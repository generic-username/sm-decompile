package org.schema.game.server.controller;

public class RequestDataStructureGen extends RequestData {
   public TerrainChunkCacheElement[] rawChunks = new TerrainChunkCacheElement[27];
   public TerrainChunkCacheElement currentChunkCache = new TerrainChunkCacheElement();

   public void reset() {
      if (this.currentChunkCache != null) {
         this.currentChunkCache.clear();
      }

   }
}
