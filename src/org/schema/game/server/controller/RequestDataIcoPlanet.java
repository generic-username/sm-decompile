package org.schema.game.server.controller;

import java.util.Random;
import javax.vecmath.Vector3f;
import obfuscated.H;
import org.schema.game.server.controller.world.factory.planet.FastNoiseSIMD;

public class RequestDataIcoPlanet extends RequestDataStructureGen {
   public final float[] noiseSet0 = FastNoiseSIMD.a(32, 32, 33);
   public final float[] noiseSet1 = FastNoiseSIMD.a(32, 32, 33);
   public final float[] noiseSet2 = FastNoiseSIMD.a(32, 32, 33);
   public H blockBiomeData = new H();
   public Random random = new Random(0L);
   public Vector3f vector3f = new Vector3f();
}
