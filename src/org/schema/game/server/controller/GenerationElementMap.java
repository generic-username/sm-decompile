package org.schema.game.server.controller;

import com.google.common.reflect.Reflection;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.Int2IntMap.Entry;
import java.util.Iterator;
import obfuscated.C;
import obfuscated.E;
import obfuscated.F;
import obfuscated.G;
import obfuscated.J;
import obfuscated.K;
import obfuscated.L;
import obfuscated.M;
import obfuscated.N;
import obfuscated.O;
import obfuscated.P;
import obfuscated.e;
import obfuscated.f;
import obfuscated.g;
import obfuscated.h;
import obfuscated.i;
import obfuscated.j;
import obfuscated.k;
import obfuscated.l;
import obfuscated.x;
import obfuscated.y;
import obfuscated.z;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.SegmentData;

public class GenerationElementMap {
   private static final Class[] blockRegisteringClasses = new Class[]{GenerationElementMap.class, K.class, J.class, L.class, N.class, M.class, P.class, O.class, C.class, E.class, G.class, x.class, F.class, y.class, z.class, e.class, f.class, g.class, h.class, i.class, j.class, k.class, l.class};
   private static final Int2IntOpenHashMap indexMap;
   public static int[] blockDataLookup;
   private boolean[] containsBlockIndex;
   public IntArrayList containsBlockIndexList;

   public GenerationElementMap() {
      this.containsBlockIndex = new boolean[blockDataLookup.length];
      this.containsBlockIndexList = new IntArrayList();
   }

   public static int getBlockDataIndex(int var0) {
      assert classRegistered((new Exception()).getStackTrace()) : "Class not registered, check stack trace for <clinit>";

      int var1;
      if ((var1 = indexMap.get(var0)) == -1) {
         assert blockDataLookup == null : "New block data added after init: " + var0 + " Type: " + ElementKeyMap.toString(SegmentData.getTypeFromIntData(var0));

         var1 = indexMap.size();
         indexMap.put(var0, var1);
      }

      return var1;
   }

   public static boolean classRegistered(StackTraceElement... var0) {
      boolean var1 = false;
      int var2 = (var0 = var0).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         StackTraceElement var4;
         if ((var4 = var0[var3]).getMethodName().contains("<clinit>")) {
            Class[] var7;
            int var5 = (var7 = blockRegisteringClasses).length;

            for(int var6 = 0; var6 < var5; ++var6) {
               if (var7[var6].getName().equals(var4.getClassName())) {
                  return true;
               }
            }

            var1 = true;
         }
      }

      if (!var1) {
         return true;
      } else {
         return false;
      }
   }

   public static void updateLookupArray() {
      Reflection.initialize(blockRegisteringClasses);
      blockDataLookup = new int[indexMap.size()];

      Entry var1;
      for(Iterator var0 = indexMap.int2IntEntrySet().iterator(); var0.hasNext(); blockDataLookup[var1.getIntValue()] = var1.getIntKey()) {
         var1 = (Entry)var0.next();
      }

   }

   public void addBlock(int var1) {
      try {
         if (!this.containsBlockIndex[var1]) {
            this.containsBlockIndex[var1] = true;
            this.containsBlockIndexList.add(var1);
         }

      } catch (ArrayIndexOutOfBoundsException var3) {
         System.err.println("INDEX OUT OF BOUNDS: " + var1 + " of total array length: " + this.containsBlockIndex.length);
         var3.printStackTrace();
      }
   }

   public void clear() {
      int var2;
      for(Iterator var1 = this.containsBlockIndexList.iterator(); var1.hasNext(); this.containsBlockIndex[var2] = false) {
         var2 = (Integer)var1.next();
      }

      this.containsBlockIndexList.clear();
   }

   public int getBlockDataFromList(int var1) {
      return blockDataLookup[this.containsBlockIndexList.getInt(var1)];
   }

   static {
      (indexMap = new Int2IntOpenHashMap()).defaultReturnValue(-1);
      getBlockDataIndex(0);

      assert getBlockDataIndex(0) == 0;

   }
}
