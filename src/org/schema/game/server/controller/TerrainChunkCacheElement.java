package org.schema.game.server.controller;

import obfuscated.A;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentDataWriteException;

public class TerrainChunkCacheElement {
   public RemoteSegment preData;
   public GenerationElementMap generationElementMap = new GenerationElementMap();
   private A structureList;
   public boolean allInSide = true;

   public boolean isEmpty() {
      assert this.generationElementMap.containsBlockIndexList.size() != 0 : "Empty type list";

      return this.generationElementMap.containsBlockIndexList.size() == 1 && this.generationElementMap.containsBlockIndexList.getInt(0) == 0;
   }

   public boolean isFullyFilledWithOneType() {
      return this.generationElementMap.containsBlockIndexList.size() == 1;
   }

   public A getStructureList() {
      return this.structureList;
   }

   public void setStructureList(A var1) {
      this.structureList = var1;
   }

   public int placeBlock(int var1, byte var2, byte var3, byte var4, SegmentData var5) throws SegmentDataWriteException {
      this.generationElementMap.addBlock(var1);
      var1 = GenerationElementMap.blockDataLookup[var1];
      var5.setInfoElementForcedAddUnsynched(SegmentData.getInfoIndex(var2, var3, var4), var1);
      return var1;
   }

   public void placeAir() {
      this.generationElementMap.addBlock(0);
   }

   public void outOfSide() {
      this.allInSide = false;
   }

   public void clear() {
      this.generationElementMap.clear();
      this.allInSide = true;
   }
}
