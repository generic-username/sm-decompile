package org.schema.game.server.controller;

import java.util.Arrays;
import java.util.Random;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.world.Chunk16SegmentData;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentDataWriteException;

public class RequestDataPlanet extends RequestData {
   public final Vector3i cachePos = new Vector3i();
   public RequestDataPlanet.Seg[] segs = new RequestDataPlanet.Seg[9];
   private int[] total = new int[294912];
   public int index;
   public boolean done;

   public int get48InfoIndex(int var1, int var2, int var3) {
      return var3 * 6144 + var2 * 48 + var1;
   }

   public RequestDataPlanet.Seg getR() {
      return this.segs[this.index];
   }

   public RequestDataPlanet() {
      for(int var1 = 0; var1 < this.segs.length; ++var1) {
         this.segs[var1] = new RequestDataPlanet.Seg();
      }

   }

   public void reset() {
      for(int var1 = 0; var1 < this.segs.length; ++var1) {
         this.segs[var1].reset();
      }

      Arrays.fill(this.total, 0);
      this.done = false;
   }

   public void initWith(Segment var1) {
      this.segs[0].set(var1.pos.x - 16, 0, var1.pos.z - 16);
      this.segs[1].set(var1.pos.x, 0, var1.pos.z - 16);
      this.segs[2].set(var1.pos.x + 16, 0, var1.pos.z - 16);
      this.segs[3].set(var1.pos.x - 16, 0, var1.pos.z);
      this.segs[4].set(var1.pos.x, 0, var1.pos.z);
      this.segs[5].set(var1.pos.x + 16, 0, var1.pos.z);
      this.segs[6].set(var1.pos.x - 16, 0, var1.pos.z + 16);
      this.segs[7].set(var1.pos.x, 0, var1.pos.z + 16);
      this.segs[8].set(var1.pos.x + 16, 0, var1.pos.z + 16);
   }

   public void applyTo(Segment var1) throws SegmentDataWriteException {
      if (var1.pos.y < 96) {
         for(int var2 = 0; var2 < 8; ++var2) {
            this.fillColumn(var1, 0, var2, 0, 0);
            this.fillColumn(var1, 1, var2, 16, 0);
            this.fillColumn(var1, 2, var2, 32, 0);
            this.fillColumn(var1, 3, var2, 0, 16);
            this.fillColumn(var1, 4, var2, 16, 16);
            this.fillColumn(var1, 5, var2, 32, 16);
            this.fillColumn(var1, 6, var2, 0, 32);
            this.fillColumn(var1, 7, var2, 16, 32);
            this.fillColumn(var1, 8, var2, 32, 32);
         }

         this.create32Chunk(var1);
      }
   }

   private void create32Chunk(Segment var1) throws SegmentDataWriteException {
      boolean var2;
      int var3 = (var2 = var1.pos.y == 0) ? 24 : 32;
      int var4 = var2 ? 0 : -8;
      int var12 = var2 ? 8 : 0;
      var1.getSegmentData().checkWritable();

      for(int var5 = 0; var5 < 32; ++var5) {
         for(int var6 = 0; var6 < var3; ++var6) {
            for(int var7 = 0; var7 < 32; ++var7) {
               int var8 = var7 + 8;
               int var9 = var1.pos.y + var6 + var4;
               int var10 = var5 + 8;
               int var11 = this.get48InfoIndex(var8, var9, var10);

               assert var11 >= 0 && var11 < this.total.length : var11 + "; " + var8 + ", " + var9 + ", " + var10 + "; " + var1.pos;

               var8 = SegmentData.getInfoIndex(var7, var6 + var12, var5);
               var1.getSegmentData().getAsIntBuffer()[var8] = this.total[var11];
               if (!var1.getSegmentData().isBlockAddedForced() && var1.getSegmentData().getType(var8) > 0) {
                  var1.getSegmentData().setBlockAddedForced(true);
               }
            }
         }
      }

   }

   private void fillColumn(Segment var1, int var2, int var3, int var4, int var5) {
      RequestDataPlanet.Seg var7 = this.segs[var2];
      int var6 = var3 << 4;
      this.fillRange(var1, var7.segmentData[var3], var4, var6, var5, var4 + 16, var6 + 16, var5 + 16);
   }

   private void fillRange(Segment var1, Chunk16SegmentData var2, int var3, int var4, int var5, int var6, int var7, int var8) {
      for(int var12 = 0; var5 < var8; ++var5) {
         for(int var9 = var4; var9 < var7; ++var9) {
            for(int var10 = var3; var10 < var6; ++var10) {
               int var11 = this.get48InfoIndex(var10, var9, var5);
               this.total[var11] = SegmentData.convertIntValue(var2, var12);
               ++var12;
            }
         }
      }

      var2.setBlockAddedForced(false);
   }

   public class Seg {
      public Chunk16SegmentData[] segmentData = new Chunk16SegmentData[8];
      public final Vector3i cachePos = new Vector3i();
      public short[] data = new short[16384];
      public boolean created = false;
      public Random rand = new Random();
      public double[] noiseSmall8;
      public double[] noise1Big16;
      public double[] noise2Big16;
      public double[] noise2DMid16;
      public final Vector3i p = new Vector3i();
      public final Vector3i pFac = new Vector3i();
      public double[] noiseArray;
      public float mar;
      public int miniblock;
      public double rScale = 1.0D;
      public float normMax = 4.0F;
      public float normMin = -0.55F;
      public float radius;

      public Seg() {
         for(int var2 = 0; var2 < this.segmentData.length; ++var2) {
            this.segmentData[var2] = new Chunk16SegmentData();
         }

      }

      public void reset() {
         this.created = false;

         for(int var1 = 0; var1 < this.segmentData.length; ++var1) {
            this.segmentData[var1].resetFast();
         }

      }

      public void set(int var1, int var2, int var3) {
         this.cachePos.set(var1, var2, var3);

         for(var2 = 0; var2 < this.segmentData.length; ++var2) {
            this.segmentData[var2].segmentPos.set(var1, var2 << 4, var3);
            this.segmentData[var2].setBlockAddedForced(false);
         }

      }
   }
}
