package org.schema.game.server.controller.pathfinding;

import it.unimi.dsi.fastutil.longs.LongArrayList;
import java.util.List;
import org.schema.game.common.controller.pathfinding.SegmentPathCalculator;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.server.data.GameServerState;

public class SegmentPathFindingHandler extends AbstractPathFindingHandler {
   private boolean foundCore;
   private SegmentPathRequest currentPathRequest;
   private LongArrayList deepCopy;

   public SegmentPathFindingHandler(GameServerState var1) {
      super(var1, "SegmentPF", new SegmentPathCalculator());
   }

   protected void init() {
      this.start();
   }

   protected boolean canProcess(SegmentPathRequest var1) {
      return var1.getType() == 0 || !ElementKeyMap.getInfo(var1.getType()).isPhysical(var1.isActive());
   }

   protected void afterCalculate(boolean var1, SegmentPathRequest var2) {
      this.foundCore = var1;
      this.currentPathRequest = var2;
      if (var1) {
         this.deepCopy = new LongArrayList(((SegmentPathCalculator)this.getIc()).getPath().size());

         for(int var3 = ((SegmentPathCalculator)this.getIc()).getPath().size() - 1; var3 >= 0; --var3) {
            this.deepCopy.add(((SegmentPathCalculator)this.getIc()).getPath().get(var3));
         }

         ((SegmentPathCalculator)this.getIc()).optimizePath(this.deepCopy);
      }

      this.enqueueSynchedResponse();
   }

   public void handleReturn() {
      if (this.foundCore) {
         this.currentPathRequest.getCallback().pathFinished(this.foundCore, this.deepCopy);
      } else {
         System.err.println("[SegmentPathFinder] NO PATH FOUND");
         this.currentPathRequest.getCallback().pathFinished(this.foundCore, (List)null);
      }
   }
}
