package org.schema.game.server.controller.pathfinding;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.graphicsengine.forms.BoundingBox;

public class BreakTestRequest extends AbstractPathRequest {
   private static Vector3i to = new Vector3i(16, 16, 16);
   private SegmentPiece from;

   public BreakTestRequest(SegmentPiece var1) {
      this.from = var1;
   }

   public SegmentController getSegmentController() {
      return this.from.getSegment().getSegmentController();
   }

   public Vector3i getFrom(Vector3i var1) {
      return this.from.getAbsolutePos(var1);
   }

   public Vector3i getTo(Vector3i var1) {
      var1.set(to);
      return to;
   }

   public void refresh() {
      this.from.refresh();
   }

   public Vector3i randomOrigin() {
      assert false;

      return null;
   }

   public BoundingBox randomRoamBB() {
      assert false;

      return null;
   }

   public Vector3i randomPathPrefferedDir() {
      assert false;

      return null;
   }

   public boolean random() {
      return false;
   }

   public int getType() {
      return this.from.getType();
   }
}
