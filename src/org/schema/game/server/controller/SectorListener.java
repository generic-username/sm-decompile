package org.schema.game.server.controller;

import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;

public interface SectorListener {
   void onSectorAdded(Sector var1);

   void onSectorRemoved(Sector var1);

   void onSectorEntityAdded(SimpleTransformableSendableObject var1, Sector var2);

   void onSectorEntityRemoved(SimpleTransformableSendableObject var1, Sector var2);
}
