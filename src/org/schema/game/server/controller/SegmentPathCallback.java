package org.schema.game.server.controller;

import java.util.List;
import org.schema.common.util.linAlg.Vector3i;

public interface SegmentPathCallback {
   void pathFinished(boolean var1, List var2);

   Vector3i getObjectSize();
}
