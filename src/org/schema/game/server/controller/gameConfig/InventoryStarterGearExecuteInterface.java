package org.schema.game.server.controller.gameConfig;

import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.inventory.Inventory;

public interface InventoryStarterGearExecuteInterface {
   void execute(InventoryStarterGearFactory.SlotType var1, int var2, int var3, Inventory var4, PlayerState var5);
}
