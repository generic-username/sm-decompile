package org.schema.game.server.controller.gameConfig;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.Short2IntOpenHashMap;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Locale;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.common.XMLTools;
import org.schema.common.config.ConfigParserException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.element.meta.BlueprintMetaItem;
import org.schema.game.common.data.element.meta.Logbook;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.updater.FileUtil;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.data.BlueprintInterface;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.game.server.data.blueprintnw.BlueprintType;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.resource.FileExt;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class GameConfig {
   private static final String defaultConfigPath;
   private static final String configPath;
   private final ObjectArrayList starterGearList = new ObjectArrayList();
   public final Short2IntOpenHashMap groupLimits = new Short2IntOpenHashMap();
   public final Short2IntOpenHashMap controllerLimits = new Short2IntOpenHashMap();
   public double massLimitShip = -1.0D;
   public double massLimitPlanet = -1.0D;
   public double massLimitStation = -1.0D;
   public int blockLimitShip = -1;
   public int blockLimitPlanet = -1;
   public int blockLimitStation = -1;
   public float sunDamagePerBlock = 1.0F;
   public float sunMinIntensityDamageRange = 1.42F;
   public float sunMaxDelayBetweenHits = 0.2F;
   public float sunDamageRadius = 8.0F;
   public float sunDamageDelay = 5.0F;
   public float sunDamageMin = 100.0F;
   public float sunDamageMax = 1000000.0F;
   public Vector3i stationSize = new Vector3i(-1, -1, -1);
   public Vector3i shipSize = new Vector3i(-1, -1, -1);
   private boolean hasMaxDim;
   private static int defaultVersion;
   private static int version;

   public GameConfig(GameServerState var1) {
      float var2;
      if ((var2 = (Float)ServerConfig.RESTRICT_BUILDING_SIZE.getCurrentState()) > 0.0F) {
         int var3 = (int)((float)(Integer)ServerConfig.SECTOR_SIZE.getCurrentState() * var2);
         this.stationSize.set(var3, var3, var3);
         this.shipSize.set(var3, var3, var3);
      }

   }

   public static void removeConfig() throws IOException {
      FileExt var0 = new FileExt(configPath);
      FileExt var1 = new FileExt(configPath + "." + System.currentTimeMillis() + ".backup.xml");
      FileUtil.copyFile(var0, var1);
      var0.delete();
   }

   public static boolean isLatestVersion() throws IOException, SAXException, ParserConfigurationException {
      FileExt var0 = new FileExt(configPath);
      FileExt var1 = new FileExt(defaultConfigPath);
      if (!var0.exists()) {
         FileUtil.copyFile(var1, var0);
         XMLTools.loadXML(var0);
      } else {
         String var4;
         if ((var4 = XMLTools.loadXML(var1).getDocumentElement().getAttribute("version")) != null) {
            try {
               defaultVersion = Integer.parseInt(var4);
            } catch (Exception var3) {
            }
         }

         if ((var4 = XMLTools.loadXML(var0).getDocumentElement().getAttribute("version")) != null) {
            try {
               version = Integer.parseInt(var4);
            } catch (Exception var2) {
            }
         }
      }

      return version >= defaultVersion;
   }

   public void parse() throws IOException, SAXException, ParserConfigurationException, ConfigParserException {
      if (!isLatestVersion()) {
         System.err.println("[GAMECONFIG] old game config version detected: current: " + version + "; required version: " + defaultVersion + "; backing up and using new default config!");
         removeConfig();
      }

      FileExt var1 = new FileExt(configPath);
      FileExt var2 = new FileExt(defaultConfigPath);
      if (!var1.exists()) {
         FileUtil.copyFile(var2, var1);
      }

      Document var3 = XMLTools.loadXML(var1);
      this.parse(var3);
   }

   private void parse(Document var1) throws ConfigParserException {
      NodeList var5 = var1.getDocumentElement().getChildNodes();
      this.starterGearList.clear();
      this.groupLimits.clear();
      this.controllerLimits.clear();

      for(int var2 = 0; var2 < var5.getLength(); ++var2) {
         Node var3;
         if ((var3 = var5.item(var2)).getNodeType() == 1) {
            String var4;
            if ((var4 = var3.getNodeName().toLowerCase(Locale.ENGLISH)).equals("startinggear")) {
               this.parseStartingGear(var3);
            }

            if (var4.equals("grouplimits")) {
               this.parseGroupLimits(var3);
            }

            if (var4.equals("shiplimits") || var4.equals("planetlimits") || var4.equals("stationlimits")) {
               this.oarseControllerLimits(var4, var3);
            }

            if (var4.equals("sunheatdamage")) {
               this.parseHeatDamage(var3);
            }

            if (var4.equals("maxdimensionship")) {
               this.parseMaxDimension(var3, this.shipSize);
            }

            if (var4.equals("maxdimensionstation")) {
               this.parseMaxDimension(var3, this.stationSize);
            }
         }
      }

   }

   private void parseHeatDamage(Node var1) {
      NodeList var5 = var1.getChildNodes();

      for(int var2 = 0; var2 < var5.getLength(); ++var2) {
         Node var3;
         if ((var3 = var5.item(var2)).getNodeType() == 1) {
            String var4;
            if ((var4 = var3.getNodeName()).toLowerCase(Locale.ENGLISH).equals("damageperblock")) {
               this.sunDamagePerBlock = Float.parseFloat(var3.getTextContent());
            }

            if (var4.toLowerCase(Locale.ENGLISH).equals("sunminintensitydamagerange")) {
               this.sunMinIntensityDamageRange = Float.parseFloat(var3.getTextContent());
            }

            if (var4.toLowerCase(Locale.ENGLISH).equals("maxdelaybetweenhits")) {
               this.sunMaxDelayBetweenHits = Float.parseFloat(var3.getTextContent());
            }

            if (var4.toLowerCase(Locale.ENGLISH).equals("sundamageradius")) {
               this.sunDamageRadius = Float.parseFloat(var3.getTextContent());
            }

            if (var4.toLowerCase(Locale.ENGLISH).equals("sundamagedelayinsecs")) {
               this.sunDamageDelay = Float.parseFloat(var3.getTextContent());
            }

            if (var4.toLowerCase(Locale.ENGLISH).equals("sundamagemax")) {
               this.sunDamageMax = Float.parseFloat(var3.getTextContent());
            }

            if (var4.toLowerCase(Locale.ENGLISH).equals("sundamagemin")) {
               this.sunDamageMax = Float.parseFloat(var3.getTextContent());
            }
         }
      }

   }

   private void parseMaxDimension(Node var1, Vector3i var2) {
      this.setHasMaxDim(true);
      NodeList var7 = var1.getChildNodes();

      for(int var3 = 0; var3 < var7.getLength(); ++var3) {
         Node var4;
         if ((var4 = var7.item(var3)).getNodeType() == 1) {
            String var5 = var4.getNodeName().toLowerCase(Locale.ENGLISH);

            try {
               int var8 = Integer.parseInt(var4.getTextContent());
               if (var5.equals("x")) {
                  var2.x = var8;
               }

               if (var5.equals("y")) {
                  var2.y = var8;
               }

               if (var5.equals("z")) {
                  var2.z = var8;
               }
            } catch (NumberFormatException var6) {
               var6.printStackTrace();
            }
         }
      }

   }

   private void oarseControllerLimits(String var1, Node var2) throws ConfigParserException {
      NodeList var9 = var2.getChildNodes();

      for(int var3 = 0; var3 < var9.getLength(); ++var3) {
         Node var4;
         if ((var4 = var9.item(var3)).getNodeType() == 1) {
            String var5;
            if ((var5 = var4.getNodeName().toLowerCase(Locale.ENGLISH)).equals("mass")) {
               try {
                  float var6 = Float.parseFloat(var4.getTextContent());
                  if (var1.contains("ship")) {
                     this.massLimitShip = (double)var6;
                  } else if (var1.contains("planet")) {
                     this.massLimitPlanet = (double)var6;
                  } else if (var1.contains("station")) {
                     this.massLimitStation = (double)var6;
                  }
               } catch (NumberFormatException var8) {
                  var8.printStackTrace();
               }
            }

            if (var5.equals("blocks")) {
               try {
                  int var10 = Integer.parseInt(var4.getTextContent());
                  if (var1.contains("ship")) {
                     this.blockLimitShip = var10;
                  } else if (var1.contains("planet")) {
                     this.blockLimitPlanet = var10;
                  } else if (var1.contains("station")) {
                     this.blockLimitStation = var10;
                  }
               } catch (NumberFormatException var7) {
                  var7.printStackTrace();
               }
            }
         }
      }

   }

   private void parseGroupLimits(Node var1) throws ConfigParserException {
      NodeList var13 = var1.getChildNodes();

      for(int var2 = 0; var2 < var13.getLength(); ++var2) {
         Node var3;
         if ((var3 = var13.item(var2)).getNodeType() == 1 && var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("controller")) {
            Integer var4 = null;
            Integer var5 = null;
            Integer var6 = null;
            NodeList var14 = var3.getChildNodes();

            for(int var7 = 0; var7 < var14.getLength(); ++var7) {
               Node var8;
               if ((var8 = var14.item(var7)).getNodeType() == 1) {
                  String var9;
                  if ((var9 = var8.getNodeName().toLowerCase(Locale.ENGLISH)).equals("id")) {
                     try {
                        var4 = Integer.parseInt(var8.getTextContent());
                     } catch (NumberFormatException var10) {
                        var10.printStackTrace();
                     }
                  }

                  if (var9.equals("groupmax")) {
                     try {
                        var5 = Integer.parseInt(var8.getTextContent());
                     } catch (NumberFormatException var11) {
                        var11.printStackTrace();
                     }
                  }

                  if (var9.equals("computermax")) {
                     try {
                        var6 = Integer.parseInt(var8.getTextContent());
                     } catch (NumberFormatException var12) {
                        var12.printStackTrace();
                     }
                  }
               }
            }

            if (var4 == null) {
               throw new ConfigParserException("Error parsing GroupLimits; 'id' missing");
            }

            if (var5 == null && var6 == null) {
               throw new ConfigParserException("Error parsing GroupLimits; 'GroupMax' or 'ComputerMax' missing");
            }

            if (var5 != null) {
               this.groupLimits.put(var4.shortValue(), var5);
            }

            if (var6 != null) {
               this.controllerLimits.put(var4.shortValue(), var6);
            }
         }
      }

   }

   private void parseStartingGear(Node var1) throws ConfigParserException {
      NodeList var5 = var1.getChildNodes();

      for(int var2 = 0; var2 < var5.getLength(); ++var2) {
         Node var3;
         if ((var3 = var5.item(var2)).getNodeType() == 1) {
            String var4;
            if ((var4 = var3.getNodeName()).toLowerCase(Locale.ENGLISH).equals("credits")) {
               this.parseCredits(var3);
            } else if (var4.toLowerCase(Locale.ENGLISH).equals("block")) {
               this.parseBlock(var3);
            } else if (var4.toLowerCase(Locale.ENGLISH).equals("tool")) {
               this.parseTool(var3);
            } else if (var4.toLowerCase(Locale.ENGLISH).equals("helmet")) {
               this.parseHelmet(var3);
            } else if (var4.toLowerCase(Locale.ENGLISH).equals("buildinhibiter")) {
               this.parseInhibiter(var3);
            } else if (var4.toLowerCase(Locale.ENGLISH).equals("flashlight")) {
               this.parseFlashlight(var3);
            } else if (var4.toLowerCase(Locale.ENGLISH).equals("logbook")) {
               this.parseLogbook(var3);
            } else if (var4.toLowerCase(Locale.ENGLISH).equals("blueprint")) {
               this.parseBlueprint(var3);
            }
         }
      }

   }

   private void parseCredits(Node var1) throws ConfigParserException {
      final Integer var2;
      try {
         var2 = Integer.parseInt(var1.getTextContent().trim());
      } catch (Exception var3) {
         throw new ConfigParserException("Error parsing " + var1.getNodeName() + "; one or more fields missing or wrong", var3);
      }

      this.starterGearList.add(new InventoryStarterGearFactory((InventoryStarterGearFactory.SlotType)null, new InventoryStarterGearExecuteInterface() {
         public void execute(InventoryStarterGearFactory.SlotType var1, int var2x, int var3, Inventory var4, PlayerState var5) {
            var5.setCredits(var2);
         }
      }));
   }

   private void parseBlueprint(Node var1) throws ConfigParserException {
      NodeList var2 = var1.getChildNodes();
      InventoryStarterGearFactory.SlotType var3 = null;
      final String var4 = null;
      Boolean var5 = null;

      for(int var6 = 0; var6 < var2.getLength(); ++var6) {
         Node var7;
         if ((var7 = var2.item(var6)).getNodeType() == 1) {
            try {
               if (var7.getNodeName().toLowerCase(Locale.ENGLISH).equals("slot")) {
                  var3 = InventoryStarterGearFactory.SlotType.valueOf(var7.getTextContent().trim().toUpperCase(Locale.ENGLISH));
               } else if (var7.getNodeName().toLowerCase(Locale.ENGLISH).equals("name")) {
                  var4 = var7.getTextContent();
               } else if (var7.getNodeName().toLowerCase(Locale.ENGLISH).equals("filled")) {
                  var5 = Boolean.parseBoolean(var7.getTextContent().trim());
               }
            } catch (Exception var8) {
               throw new ConfigParserException("Error parsing " + var1.getNodeName() + "; one or more fields missing or wrong", var8);
            }
         }
      }

      if (var3 != null && var4 != null && var5 != null) {
         final boolean var9 = var5;
         this.starterGearList.add(new InventoryStarterGearFactory(var3, new InventoryStarterGearExecuteInterface() {
            public void execute(InventoryStarterGearFactory.SlotType var1, int var2, int var3, Inventory var4x, PlayerState var5) {
               try {
                  BlueprintEntry var9x = BluePrintController.active.getBlueprint(var4);
                  MetaObject var6;
                  BlueprintMetaItem var7;
                  (var7 = (BlueprintMetaItem)(var6 = MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.BLUEPRINT, (short)-1, true))).blueprintName = var4;
                  var7.goal = new ElementCountMap(var9x.getElementCountMapWithChilds());
                  if (!var9) {
                     var7.progress = new ElementCountMap();
                  } else {
                     var7.progress = new ElementCountMap(var7.goal);
                  }

                  if (var1 == InventoryStarterGearFactory.SlotType.HOTBAR) {
                     var4x.put(var2, var6);
                  } else {
                     var4x.put(var3, var6);
                  }
               } catch (EntityNotFountException var8) {
                  var8.printStackTrace();
               }
            }
         }));
      } else {
         throw new ConfigParserException("Error parsing " + var1.getNodeName() + "; one or more fields missing or wrong. needs Slot, Name, and Filled");
      }
   }

   private void parseLogbook(Node var1) throws ConfigParserException {
      NodeList var2 = var1.getChildNodes();
      InventoryStarterGearFactory.SlotType var3 = null;
      final String var4 = null;

      for(int var5 = 0; var5 < var2.getLength(); ++var5) {
         Node var6;
         if ((var6 = var2.item(var5)).getNodeType() == 1) {
            try {
               if (var6.getNodeName().toLowerCase(Locale.ENGLISH).equals("slot")) {
                  var3 = InventoryStarterGearFactory.SlotType.valueOf(var6.getTextContent().trim().toUpperCase(Locale.ENGLISH));
               } else if (var6.getNodeName().toLowerCase(Locale.ENGLISH).equals("message")) {
                  var4 = var6.getTextContent();
               }
            } catch (Exception var7) {
               throw new ConfigParserException("Error parsing " + var1.getNodeName() + "; one or more fields missing or wrong", var7);
            }
         }
      }

      if (var3 != null && var4 != null) {
         this.starterGearList.add(new InventoryStarterGearFactory(var3, new InventoryStarterGearExecuteInterface() {
            public void execute(InventoryStarterGearFactory.SlotType var1, int var2, int var3, Inventory var4x, PlayerState var5) {
               MetaObject var6;
               ((Logbook)(var6 = MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.LOG_BOOK, (short)-1, true))).setTxt(var4);
               if (var1 == InventoryStarterGearFactory.SlotType.HOTBAR) {
                  var4x.put(var2, var6);
               } else {
                  var4x.put(var3, var6);
               }
            }
         }));
      } else {
         throw new ConfigParserException("Error parsing " + var1.getNodeName() + "; one or more fields missing or wrong");
      }
   }

   private void parseHelmet(Node var1) throws ConfigParserException {
      NodeList var2 = var1.getChildNodes();
      InventoryStarterGearFactory.SlotType var3 = null;

      for(int var4 = 0; var4 < var2.getLength(); ++var4) {
         Node var5;
         if ((var5 = var2.item(var4)).getNodeType() == 1) {
            try {
               if (var5.getNodeName().toLowerCase(Locale.ENGLISH).equals("slot")) {
                  var3 = InventoryStarterGearFactory.SlotType.valueOf(var5.getTextContent().trim().toUpperCase(Locale.ENGLISH));
               }
            } catch (Exception var6) {
               throw new ConfigParserException("Error parsing " + var1.getNodeName() + "; one or more fields missing or wrong", var6);
            }
         }
      }

      if (var3 == null) {
         throw new ConfigParserException("Error parsing " + var1.getNodeName() + "; one or more fields missing or wrong");
      } else {
         this.starterGearList.add(new InventoryStarterGearFactory(var3, new InventoryStarterGearExecuteInterface() {
            public void execute(InventoryStarterGearFactory.SlotType var1, int var2, int var3, Inventory var4, PlayerState var5) {
               MetaObject var6 = MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.HELMET, (short)-1, true);
               if (var1 == InventoryStarterGearFactory.SlotType.HOTBAR) {
                  var4.put(var2, var6);
               } else {
                  var4.put(var3, var6);
               }
            }
         }));
      }
   }

   private void parseFlashlight(Node var1) throws ConfigParserException {
      NodeList var2 = var1.getChildNodes();
      InventoryStarterGearFactory.SlotType var3 = null;

      for(int var4 = 0; var4 < var2.getLength(); ++var4) {
         Node var5;
         if ((var5 = var2.item(var4)).getNodeType() == 1) {
            try {
               if (var5.getNodeName().toLowerCase(Locale.ENGLISH).equals("slot")) {
                  var3 = InventoryStarterGearFactory.SlotType.valueOf(var5.getTextContent().trim().toUpperCase(Locale.ENGLISH));
               }
            } catch (Exception var6) {
               throw new ConfigParserException("Error parsing " + var1.getNodeName() + "; one or more fields missing or wrong", var6);
            }
         }
      }

      if (var3 == null) {
         throw new ConfigParserException("Error parsing " + var1.getNodeName() + "; one or more fields missing or wrong");
      } else {
         this.starterGearList.add(new InventoryStarterGearFactory(var3, new InventoryStarterGearExecuteInterface() {
            public void execute(InventoryStarterGearFactory.SlotType var1, int var2, int var3, Inventory var4, PlayerState var5) {
               MetaObject var6 = MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.FLASH_LIGHT, (short)-1, true);
               if (var1 == InventoryStarterGearFactory.SlotType.HOTBAR) {
                  var4.put(var2, var6);
               } else {
                  var4.put(var3, var6);
               }
            }
         }));
      }
   }

   private void parseInhibiter(Node var1) throws ConfigParserException {
      NodeList var2 = var1.getChildNodes();
      InventoryStarterGearFactory.SlotType var3 = null;

      for(int var4 = 0; var4 < var2.getLength(); ++var4) {
         Node var5;
         if ((var5 = var2.item(var4)).getNodeType() == 1) {
            try {
               if (var5.getNodeName().toLowerCase(Locale.ENGLISH).equals("slot")) {
                  var3 = InventoryStarterGearFactory.SlotType.valueOf(var5.getTextContent().trim().toUpperCase(Locale.ENGLISH));
               }
            } catch (Exception var6) {
               throw new ConfigParserException("Error parsing " + var1.getNodeName() + "; one or more fields missing or wrong", var6);
            }
         }
      }

      if (var3 == null) {
         throw new ConfigParserException("Error parsing " + var1.getNodeName() + "; one or more fields missing or wrong");
      } else {
         this.starterGearList.add(new InventoryStarterGearFactory(var3, new InventoryStarterGearExecuteInterface() {
            public void execute(InventoryStarterGearFactory.SlotType var1, int var2, int var3, Inventory var4, PlayerState var5) {
               MetaObject var6 = MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.BUILD_PROHIBITER, (short)-1, true);
               if (var1 == InventoryStarterGearFactory.SlotType.HOTBAR) {
                  var4.put(var2, var6);
               } else {
                  var4.put(var3, var6);
               }
            }
         }));
      }
   }

   private void parseTool(Node var1) throws ConfigParserException {
      NodeList var2 = var1.getChildNodes();
      InventoryStarterGearFactory.SlotType var3 = null;
      Integer var4 = null;

      final int var5;
      for(var5 = 0; var5 < var2.getLength(); ++var5) {
         Node var6;
         if ((var6 = var2.item(var5)).getNodeType() == 1) {
            try {
               if (var6.getNodeName().toLowerCase(Locale.ENGLISH).equals("slot")) {
                  var3 = InventoryStarterGearFactory.SlotType.valueOf(var6.getTextContent().trim().toUpperCase(Locale.ENGLISH));
               } else if (var6.getNodeName().toLowerCase(Locale.ENGLISH).equals("subid")) {
                  var4 = Integer.parseInt(var6.getTextContent().trim());
               }
            } catch (Exception var7) {
               throw new ConfigParserException("Error parsing " + var1.getNodeName() + "; one or more fields missing or wrong", var7);
            }
         }
      }

      if (var3 != null && var4 != null) {
         var5 = var4;
         this.starterGearList.add(new InventoryStarterGearFactory(var3, new InventoryStarterGearExecuteInterface() {
            public void execute(InventoryStarterGearFactory.SlotType var1, int var2, int var3, Inventory var4, PlayerState var5x) {
               MetaObject var6 = MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.WEAPON, (short)var5, true);
               if (var1 == InventoryStarterGearFactory.SlotType.HOTBAR) {
                  var4.put(var2, var6);
               } else {
                  var4.put(var3, var6);
               }
            }
         }));
      } else {
         throw new ConfigParserException("Error parsing " + var1.getNodeName() + "; one or more fields missing or wrong");
      }
   }

   private void parseBlock(Node var1) throws ConfigParserException {
      NodeList var2 = var1.getChildNodes();
      InventoryStarterGearFactory.SlotType var3 = null;
      Integer var4 = null;
      Integer var5 = null;

      for(int var6 = 0; var6 < var2.getLength(); ++var6) {
         Node var7;
         if ((var7 = var2.item(var6)).getNodeType() == 1) {
            try {
               if (var7.getNodeName().toLowerCase(Locale.ENGLISH).equals("slot")) {
                  var3 = InventoryStarterGearFactory.SlotType.valueOf(var7.getTextContent().trim().toUpperCase(Locale.ENGLISH));
               } else if (var7.getNodeName().toLowerCase(Locale.ENGLISH).equals("id")) {
                  var4 = Integer.parseInt(var7.getTextContent().trim());
               } else if (var7.getNodeName().toLowerCase(Locale.ENGLISH).equals("count")) {
                  var5 = Integer.parseInt(var7.getTextContent().trim());
               }
            } catch (Exception var8) {
               throw new ConfigParserException("Error parsing " + var1.getNodeName() + "; one or more fields missing or wrong", var8);
            }
         }
      }

      if (var3 != null && var4 != null && var5 != null) {
         final short var9 = var4.shortValue();
         final int var10 = var5;
         this.starterGearList.add(new InventoryStarterGearFactory(var3, new InventoryStarterGearExecuteInterface() {
            public void execute(InventoryStarterGearFactory.SlotType var1, int var2, int var3, Inventory var4, PlayerState var5) {
               if (var1 == InventoryStarterGearFactory.SlotType.HOTBAR) {
                  var4.put(var2, var9, var10, -1);
               } else {
                  var4.put(var3, var9, var10, -1);
               }
            }
         }));
      } else {
         throw new ConfigParserException("Error parsing " + var1.getNodeName() + "; one or more fields missing or wrong");
      }
   }

   public void fillInventory(PlayerState var1) {
      int var2 = 0;
      int var3 = 10;
      Iterator var4 = this.starterGearList.iterator();

      while(var4.hasNext()) {
         InventoryStarterGearFactory var5;
         (var5 = (InventoryStarterGearFactory)var4.next()).executor.execute(var5.slotType, var2, var3, var1.getInventory(), var1);
         if (var5.slotType == InventoryStarterGearFactory.SlotType.HOTBAR) {
            ++var2;
         } else if (var5.slotType == InventoryStarterGearFactory.SlotType.INVENTORY) {
            ++var3;
         }
      }

   }

   public int getGroupLimit(short var1) {
      return this.groupLimits.get(var1);
   }

   public boolean hasGroupLimit(short var1, int var2) {
      return this.groupLimits.containsKey(var1) && var2 > this.groupLimits.get(var1);
   }

   public int getControllerLimit(short var1) {
      return this.controllerLimits.get(var1);
   }

   public boolean hasControllerLimit(short var1, int var2) {
      return this.controllerLimits.containsKey(var1) && var2 > this.controllerLimits.get(var1);
   }

   public String toStringAllowedSize(SegmentController var1) {
      return var1.getType() == SimpleTransformableSendableObject.EntityType.SHIP ? this.toStringSize(this.shipSize) : this.toStringSize(this.stationSize);
   }

   private String toStringSize(Vector3i var1) {
      return "X: " + (var1.x > 0 ? var1.x : "~") + ", Y: " + (var1.y > 0 ? var1.y : "~") + ", Z: " + (var1.z > 0 ? var1.z : "~");
   }

   private boolean isOk(BoundingBox var1, Vector3i var2) {
      return (var2.x <= 0 || var1.max.x - var1.min.x <= (float)(var2.x + 2)) && (var2.y <= 0 || var1.max.y - var1.min.y <= (float)(var2.y + 2)) && (var2.z <= 0 || var1.max.z - var1.min.z <= (float)(var2.z + 2));
   }

   public boolean isOk(BoundingBox var1, SegmentController var2) {
      return !this.hasMaxDim || var2.getType() != SimpleTransformableSendableObject.EntityType.SHIP && var2.getType() != SimpleTransformableSendableObject.EntityType.SPACE_STATION || var2.getType() == SimpleTransformableSendableObject.EntityType.SHIP && this.isOk(var1, this.shipSize) || var2.getType() == SimpleTransformableSendableObject.EntityType.SPACE_STATION && this.isOk(var1, this.stationSize);
   }

   public boolean isBBOk(BlueprintInterface var1) {
      return !this.hasMaxDim || var1.getType() != BlueprintType.SHIP || var1.getType() != BlueprintType.SPACE_STATION || var1.getType() == BlueprintType.SHIP && this.isOk(var1.getBb(), this.shipSize) || var1.getType() == BlueprintType.SPACE_STATION && this.isOk(var1.getBb(), this.stationSize);
   }

   public String toStringAllowedSize(BlueprintInterface var1) {
      return var1.getType() == BlueprintType.SHIP ? this.toStringSize(this.shipSize) : this.toStringSize(this.stationSize);
   }

   public boolean isHasMaxDim() {
      return this.hasMaxDim;
   }

   public void setHasMaxDim(boolean var1) {
      this.hasMaxDim = var1;
   }

   static {
      defaultConfigPath = "." + File.separator + "data" + File.separator + "config" + File.separator + "GameConfigDefault.xml";
      configPath = "." + File.separator + "GameConfig.xml";
   }
}
