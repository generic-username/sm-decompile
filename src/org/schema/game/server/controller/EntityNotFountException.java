package org.schema.game.server.controller;

public class EntityNotFountException extends Exception {
   private static final long serialVersionUID = 1L;

   public EntityNotFountException(String var1) {
      super(var1);
   }
}
