package org.schema.game.server.controller;

import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import obfuscated.w;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.element.world.FastValidationContainer;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentAABBCalculator;
import org.schema.game.common.controller.SegmentProvider;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentDataIntArray;
import org.schema.game.network.objects.NetworkSegmentProvider;
import org.schema.game.server.data.GameServerState;

public class ServerSegmentProvider extends SegmentProvider {
   public static final Object GENERAL_GENERATOR_LOCK = new Object();
   public static final Object PLANET_GENERATOR_LOCK = new Object();
   public static final Object ASTEROID_GENERATOR_LOCK = new Object();
   public static final Object STATION_GENERATOR_LOCK = new Object();
   public final Object GRID_LOCK = new Object();
   public static int dbJobs;
   private final GeneratorGridLock gridLock = new GeneratorGridLock();
   private Map waitingRequests = new HashMap();
   public static int predeteminedEmpty;
   public static int firstStages;
   private static final SegmentAABBCalculator segmentAABBCalculator;
   private static ObjectArrayFIFOQueue fastValidationPool;

   public ServerSegmentProvider(SendableSegmentController var1) {
      super(var1);
      w.a();
   }

   protected void finishAllWritingJobsFor(RemoteSegment var1) {
      ((GameServerState)var1.getSegmentController().getState()).getThreadedSegmentWriter().finish(var1);
   }

   public void onAddRequestedSegment(Segment var1) {
   }

   protected boolean readyToRequestElements() {
      return true;
   }

   public void enqueueHightPrio(int var1, int var2, int var3, boolean var4) {
      System.err.println("[SERVER) ENQUEUE HIGH PRIORITY CHUNK: " + var1 + ", " + var2 + ", " + var3 + " for " + this.segmentController);
      ((GameServerState)this.segmentController.getState()).getController().scheduleSegmentRequest(this.getSegmentController(), new Vector3i(var1, var2, var3), (NetworkSegmentProvider)null, -1L, (short)-1, false, true);
   }

   public void writeToDiskQueued(Segment var1) throws IOException {
      RemoteSegment var2;
      ((GameServerState)(var2 = (RemoteSegment)var1).getSegmentController().getState()).getThreadedSegmentWriter().queueWrite(var2);
   }

   private void generate(RemoteSegment var1, RequestData var2, boolean var3) {
      boolean var4 = false;
      if (!var3) {
         var4 = this.getSegmentController().getCreatorThread().predictEmpty(var1.pos);
      }

      if (!var4) {
         if (var1.getSegmentData() != null && var1.getSegmentData() instanceof SegmentDataIntArray) {
            System.err.println("WARNING. Generating in already existing element: " + var1);
         } else {
            this.getSegmentController().getSegmentProvider().getFreeSegmentData().assignData(var1);
         }

         var1.setSize(0);
         var1.getSegmentData().setNeedsRevalidate(false);
         this.getSegmentController().getCreatorThread().onNoExistingSegmentFound(var1, var2);
         if (!var1.getSegmentData().isBlockAddedForced()) {
            this.purgeSegmentData(var1, var1.getSegmentData(), true);
            return;
         }

         if (var1.getSegmentData() != null) {
            var1.getSegmentData().setNeedsRevalidate(true);
            var1.getSegmentData().setBlockAddedForced(false);
            return;
         }
      } else {
         if (var1.getSegmentData() != null) {
            this.purgeSegmentData(var1, var1.getSegmentData(), false);
         }

         var1.setSize(0);
      }

   }

   public RemoteSegment addToBufferIfNecessary(RemoteSegment var1, int var2, int var3, int var4) {
      assert var1.pos.equals(var2, var3, var4) : this.getSegmentController() + "; " + var1.pos + "; " + var2 + ", " + var3 + ", " + var4;

      synchronized(this.getSegmentController().getSegmentBuffer()) {
         Segment var7;
         if ((var7 = this.segmentController.getSegmentBuffer().get(var2, var3, var4)) != null) {
            return (RemoteSegment)var7;
         }
      }

      this.addSegmentToBuffer(var1);
      return var1;
   }

   protected RemoteSegment createSegment(long var1) {
      int var3 = ElementCollection.getPosX(var1);
      int var4 = ElementCollection.getPosY(var1);
      int var5 = ElementCollection.getPosZ(var1);

      assert var3 % 32 == 0 : "Invalid request: " + var3 + ", " + var4 + ", " + var5;

      assert var4 % 32 == 0 : "Invalid request: " + var3 + ", " + var4 + ", " + var5;

      assert var5 % 32 == 0 : "Invalid request: " + var3 + ", " + var4 + ", " + var5;

      RemoteSegment var9;
      synchronized(this.getSegmentController().getSegmentBuffer()) {
         var9 = (RemoteSegment)this.segmentController.getSegmentBuffer().get(var1);
      }

      if (var9 != null) {
         var9.needsGeneration = false;
         return var9;
      } else {
         (var9 = new RemoteSegment(this.segmentController)).setPos(var3, var4, var5);
         synchronized(this.getSegmentController().getSegmentBuffer()) {
            if (this.getSegmentController().getSegmentBuffer().containsKey(var9.pos)) {
               var9.needsGeneration = false;
               return var9;
            }
         }

         var9.needsGeneration = true;
         return var9;
      }
   }

   public RemoteSegment doRequest(long var1, RequestData var3) throws IOException {
      RemoteSegment var4;
      if (!(var4 = this.createSegment(var1)).needsGeneration) {
         return var4;
      } else {
         int var2 = this.requestSegmentFromDatabase(var4);
         this.requestSegmentGeneration(var4, var2, var3);
         if (var2 == 1) {
            if (var4.getSegmentData() != null) {
               this.purgeSegmentData(var4, var4.getSegmentData(), false);
            }

            var4.setSize(0);
         }

         return var4;
      }
   }

   public RemoteSegment doRequestStaged(long var1, RequestDataIcoPlanet var3) throws IOException {
      RemoteSegment var17 = this.createSegment(var1);
      int var2;
      if ((var2 = this.requestSegmentFromDatabase(var17)) == 2) {
         long[] var4 = new long[27];
         int var5 = 0;

         int var8;
         for(int var6 = -32; var6 < 33; var6 += 32) {
            for(int var7 = -32; var7 < 33; var7 += 32) {
               for(var8 = -32; var8 < 33; var8 += 32) {
                  int var10 = var17.pos.x + var8;
                  int var11 = var17.pos.y + var7;
                  int var9 = var17.pos.z + var6;
                  long var12 = ElementCollection.getIndex(var10, var11, var9);
                  var4[var5] = var12;
                  ++var5;
               }
            }
         }

         synchronized(this.GRID_LOCK) {
            while(this.gridLock.isGridLocked(var4)) {
               try {
                  this.GRID_LOCK.wait();
               } catch (InterruptedException var15) {
                  var15.printStackTrace();
               }
            }

            this.gridLock.lockGrid(var4);
         }

         TerrainChunkCacheElement[] var18 = var3.rawChunks;
         Vector3i var19 = new Vector3i();

         for(var8 = 0; var8 < var4.length; ++var8) {
            long var21 = var4[var8];
            TerrainChunkCacheElement var20 = this.gridLock.getFromCache(var21);
            var19.set(ElementCollection.getPosX(var4[var8]), ElementCollection.getPosY(var4[var8]), ElementCollection.getPosZ(var4[var8]));
            if (var20 == null) {
               var20 = new TerrainChunkCacheElement();
               RemoteSegment var22;
               (var22 = new RemoteSegment(this.getSegmentController())).setPos(var19.x, var19.y, var19.z);
               var20.preData = var22;
               if (!this.getSegmentController().getCreatorThread().predictFirstStageEmpty(var19)) {
                  this.getSegmentController().getSegmentProvider().getFreeSegmentData().assignData(var22);
                  ++firstStages;
                  this.getSegmentController().getCreatorThread().doFirstStage(var22, var20, var3);

                  assert var20.preData.getSegmentData() instanceof SegmentDataIntArray : "Must be int array";

                  if (!var20.preData.getSegmentData().isBlockAddedForced()) {
                     this.getSegmentDataManager().addToFreeSegmentData(var20.preData.getSegmentData(), true, true);
                     var20.preData.setSegmentData((SegmentData)null);
                  }
               } else {
                  var20.placeAir();
                  ++predeteminedEmpty;
               }

               this.gridLock.putInCache(var21, var20);
            }

            var18[var8] = var20;
         }

         this.generate(var17, var3, true);
         synchronized(this.GRID_LOCK) {
            this.gridLock.unlockGrid(var4);
            this.GRID_LOCK.notifyAll();
         }
      }

      if (var2 == 1) {
         if (var17.getSegmentData() != null) {
            this.purgeSegmentData(var17, var17.getSegmentData(), false);
         }

         var17.setSize(0);
      }

      return var17;
   }

   public int requestSegmentFromDatabase(RemoteSegment var1) throws IOException {
      int var2;
      try {
         var2 = this.getSegmentDataIO().request(var1.pos.x, var1.pos.y, var1.pos.z, var1);
      } catch (IOException var3) {
         var3.printStackTrace();
         ((GameServerState)this.getSegmentController().getState()).getController().broadcastMessage(new Object[]{471, var1.getSegmentController()}, 3);
         System.err.println(var3.getClass().getSimpleName() + " WARNING: COULD NOT READ " + var1.pos + ": " + var1 + ", " + var1.getSegmentController() + ":  RECREATING -- In case of EOF");
         var2 = 2;
      }

      return var2;
   }

   public void requestSegmentGeneration(RemoteSegment var1, int var2, RequestData var3) throws IOException {
      if (var2 == 2) {
         if (this.getSegmentController() instanceof Planet) {
            this.generate(var1, var3, false);
         } else if (this.getSegmentController().getCreatorThread().isConcurrent() == 2) {
            this.generate(var1, var3, false);
         } else if (this.getSegmentController().getCreatorThread().isConcurrent() == 1) {
            synchronized(this.getSegmentController().getCreatorThread()) {
               this.generate(var1, var3, false);
            }
         } else {
            synchronized(GENERAL_GENERATOR_LOCK) {
               this.generate(var1, var3, false);
            }
         }

         var1.setLastChanged(System.currentTimeMillis());
      }

   }

   public static void freeFastValidationList(FastValidationContainer var0) {
      synchronized(fastValidationPool) {
         fastValidationPool.enqueue(var0);
      }
   }

   public static FastValidationContainer getFastValidationList() {
      synchronized(fastValidationPool) {
         if (!fastValidationPool.isEmpty()) {
            return (FastValidationContainer)fastValidationPool.dequeue();
         }
      }

      return new FastValidationContainer();
   }

   public void freeValidationList(FastValidationContainer var1) {
      freeFastValidationList(var1);
   }

   public SegmentAABBCalculator getSegmentAABBCalculator() {
      return segmentAABBCalculator;
   }

   static {
      (segmentAABBCalculator = new SegmentAABBCalculator()).start();
      fastValidationPool = new ObjectArrayFIFOQueue();
   }
}
