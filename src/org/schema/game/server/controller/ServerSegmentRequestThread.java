package org.schema.game.server.controller;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import org.schema.game.common.controller.SegmentOutOfBoundsException;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;

public class ServerSegmentRequestThread extends Thread {
   private GameServerState state;
   private ExecutorService executor;
   private boolean shutdown;
   public static int num;

   public ServerSegmentRequestThread(GameServerState var1) {
      super("ServerSegmentRequestThread");
      this.state = var1;
      int var3 = (Integer)ServerConfig.CHUNK_REQUEST_THREAD_POOL_SIZE_TOTAL.getCurrentState();
      this.executor = Executors.newFixedThreadPool(var3, new ThreadFactory() {
         public Thread newThread(Runnable var1) {
            return new Thread(var1, "ServerSegmentGeneratorThread_" + ServerSegmentRequestThread.num++);
         }
      });

      for(int var2 = 0; var2 < var3; ++var2) {
         this.executor.execute(new Runnable() {
            public void run() {
               try {
                  Thread.sleep(2000L);
               } catch (InterruptedException var1) {
                  var1.printStackTrace();
               }
            }
         });
      }

      System.err.println("[SERVER] Request Thread pool size is: " + ServerConfig.CHUNK_REQUEST_THREAD_POOL_SIZE_TOTAL.getCurrentState());
      this.setDaemon(true);
   }

   private ServerSegmentRequest getReq() throws InterruptedException {
      synchronized(this.state.getSegmentRequests()) {
         do {
            if (!this.state.getSegmentRequests().isEmpty()) {
               return (ServerSegmentRequest)this.state.getSegmentRequests().dequeue();
            }

            this.state.getSegmentRequests().wait(5000L);
         } while(!this.shutdown);

         return null;
      }
   }

   public void run() {
      while(true) {
         try {
            if (!this.shutdown) {
               final ServerSegmentRequest var1 = this.getReq();
               if (this.shutdown) {
                  return;
               }

               synchronized(this.state) {
                  if (!this.state.getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(var1.getSegmentController().getId())) {
                     if (var1.highPrio) {
                        synchronized(this.state.getSegmentRequests()) {
                           System.err.println("[SERVER] Segment Controller doesn't exist yet for high prio chunk request " + var1.getSegmentController());
                           this.state.getSegmentRequests().enqueue(var1);
                        }
                     }
                     continue;
                  }
               }

               this.executor.execute(new Runnable() {
                  public void run() {
                     try {
                        ServerSegmentRequestThread.this.state.getController().handleSegmentRequest(var1);
                     } catch (IOException var1x) {
                        var1x.printStackTrace();
                     } catch (InterruptedException var2) {
                        var2.printStackTrace();
                     } catch (SegmentOutOfBoundsException var3) {
                        var3.printStackTrace();
                     } catch (Exception var4) {
                        var4.printStackTrace();
                     }
                  }
               });
               continue;
            }
         } catch (InterruptedException var6) {
            var6.printStackTrace();
         }

         assert false : "Thread stopped";

         return;
      }
   }

   public void shutdown() {
      this.shutdown = true;
      synchronized(this.state.getSegmentRequests()) {
         this.state.getSegmentRequests().notifyAll();
      }
   }
}
