package org.schema.game.server.ai;

import org.schema.game.common.controller.Planet;
import org.schema.schine.graphicsengine.core.Timer;

public class PlanetAIEntity extends SegmentControllerAIEntity {
   public PlanetAIEntity(String var1, Planet var2) {
      super(var1, var2);
   }

   public void updateAIClient(Timer var1) {
   }

   public void updateAIServer(Timer var1) {
   }
}
