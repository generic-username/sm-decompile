package org.schema.game.server.ai.program.searchanddestroy.states;

import org.schema.game.server.ai.program.common.states.ShootAtTarget;
import org.schema.schine.ai.AiEntityStateInterface;

public class ShipShootAtTarget extends ShootAtTarget {
   public ShipShootAtTarget(AiEntityStateInterface var1) {
      super(var1);
   }
}
