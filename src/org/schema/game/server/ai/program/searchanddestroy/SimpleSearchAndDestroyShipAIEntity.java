package org.schema.game.server.ai.program.searchanddestroy;

import javax.vecmath.Vector3f;
import org.schema.game.common.controller.Ship;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.ShootingStateInterface;
import org.schema.game.server.ai.program.common.states.ShipGameState;
import org.schema.game.server.ai.program.searchanddestroy.states.ShipEngagingTarget;
import org.schema.game.server.ai.program.searchanddestroy.states.ShipGettingToTarget;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.core.Timer;

public class SimpleSearchAndDestroyShipAIEntity extends ShipAIEntity {
   private static final long CHECK_PROXIMITY_DELAY = 10000L;
   private long lastProximityTest;
   private Vector3f dist = new Vector3f();

   public SimpleSearchAndDestroyShipAIEntity(Ship var1, boolean var2) {
      super("S&D_ENT", var1);
      if (var1.isOnServer()) {
         this.setCurrentProgram(new SimpleSearchAndDestroyProgram(this, var2));
      }

      this.lastProximityTest = (long)((double)System.currentTimeMillis() + Math.random() * 10000.0D);
   }

   public void updateAIServer(Timer var1) throws FSMException {
      super.updateAIServer(var1);
      if (this.getStateCurrent() instanceof ShipGameState) {
         ((ShipGameState)this.getStateCurrent()).updateAI(this.unit, var1, this.getEntity(), this);
      }

      if (this.getEntity().getProximityObjects().size() > 0 && (this.getStateCurrent() instanceof ShipEngagingTarget || this.getStateCurrent() instanceof ShipGettingToTarget || this.getStateCurrent() instanceof ShootingStateInterface)) {
         try {
            if (!this.getEntity().getDockingController().isDocked()) {
               this.getStateCurrent().stateTransition(Transition.ENEMY_PROXIMITY);
            }

            return;
         } catch (FSMException var2) {
            var2.printStackTrace();
         }
      }

   }
}
