package org.schema.game.server.ai.program.turret;

import javax.vecmath.Vector3f;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.ai.AIConfiguationElements;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.common.states.ShipGameState;
import org.schema.game.server.data.FactionState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.graphicsengine.core.Timer;

public class TurretShipAIEntity extends ShipAIEntity {
   private static final long TARGET_ATTACKED_CHANGE_DELAY = 5000L;
   public Vector3f orientateDir = new Vector3f();
   private long lastTargetChangeDueToAttack;

   public TurretShipAIEntity(Ship var1, boolean var2) {
      super("Turret_Ent", var1);
      if (var1.isOnServer()) {
         this.setCurrentProgram(new TurretProgram(this, var2));
      }

   }

   public float getShootingDifficulty(SimpleGameObject var1) {
      return ((String)this.getEntity().getAiConfiguration().get(Types.AIM_AT).getCurrentState()).equals("Missiles") ? this.getEntity().getConfigManager().apply(StatusEffectType.AI_ACCURACY_POINT_DEFENSE, this.getShootingDifficultyRaw(var1)) : this.getEntity().getConfigManager().apply(StatusEffectType.AI_ACCURACY_TURRET, this.getShootingDifficultyRaw(var1));
   }

   public void updateAIServer(Timer var1) throws FSMException {
      super.updateAIServer(var1);
      if (this.getStateCurrent() instanceof ShipGameState) {
         ((ShipGameState)this.getStateCurrent()).updateAI(this.unit, var1, this.getEntity(), this);
      }

   }

   public void handleHitBy(float var1, Damager var2) {
      if (this.getState().getUpdateTime() - this.lastTargetChangeDueToAttack >= 5000L) {
         AIConfiguationElements var3;
         if (((String)(var3 = this.getEntity().getAiConfiguration().get(Types.AIM_AT)).getCurrentState()).equals("Any") || ((String)var3.getCurrentState()).equals("Ships") && var2 instanceof Ship || ((String)var3.getCurrentState()).equals("Stations") && (var2 instanceof SpaceStation || var2 instanceof Planet)) {
            if (var2 instanceof SimpleTransformableSendableObject) {
               SimpleTransformableSendableObject var4 = (SimpleTransformableSendableObject)var2;
               FactionRelation.RType var5;
               if ((var5 = ((FactionState)this.getState()).getFactionManager().getRelation(var4, this.getEntity())) == FactionRelation.RType.ENEMY || var5 == FactionRelation.RType.NEUTRAL) {
                  this.lastTargetChangeDueToAttack = System.currentTimeMillis();
                  ((TargetProgram)this.getCurrentProgram()).setTarget(var4);
               }
            }

         }
      }
   }
}
