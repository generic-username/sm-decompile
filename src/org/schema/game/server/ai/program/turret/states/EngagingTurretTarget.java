package org.schema.game.server.ai.program.turret.states;

import com.bulletphysics.linearmath.Transform;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.ai.AIShipControllerStateUnit;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.common.states.ShipGameState;
import org.schema.game.server.ai.program.turret.TurretShipAIEntity;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;

public class EngagingTurretTarget extends ShipGameState implements ShootingProcessInterface {
   private static final long MAX_ENGAGE_TIME = 5000L;
   private final Vector3f rotDir = new Vector3f();
   private long startedToEngage;
   private Vector3f tmp = new Vector3f();
   private String descString = "";

   public EngagingTurretTarget(AiEntityStateInterface var1) {
      super(var1);
   }

   public Vector3f getRotDir() {
      return this.rotDir;
   }

   public boolean onEnter() {
      this.getRotDir().set(0.0F, 0.0F, 0.0F);
      this.startedToEngage = System.currentTimeMillis();
      this.descString = "STARTENGAGE";
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      if (this.getEntityState().getState().getUpdateTime() - this.startedToEngage > 5000L) {
         this.stateTransition(Transition.RESTART);
         return false;
      } else {
         if (this.getAIConfig().get(Types.MANUAL).isOn()) {
            SegmentController var1;
            ManagerContainer var2 = ((ManagedSegmentController)(var1 = ((Ship)this.getEntity()).railController.getRoot())).getManagerContainer();
            if (var1 instanceof PlayerControllable && !((PlayerControllable)var1).getAttachedPlayers().isEmpty()) {
               Iterator var3 = ((PlayerState)((PlayerControllable)var1).getAttachedPlayers().get(0)).getControllerState().getUnits().iterator();

               while(var3.hasNext()) {
                  ControllerStateUnit var4 = (ControllerStateUnit)var3.next();
                  PlayerUsableInterface var5;
                  if ((var5 = var2.getPlayerUsable(-9223372036854775779L)) != null && var4.isSelected(var5, ((ManagedSegmentController)var1).getManagerContainer()) && var4.playerControllable == var1) {
                     this.stateTransition(Transition.RESTART);
                     return false;
                  }
               }
            }
         }

         SimpleGameObject var6;
         if ((var6 = ((TargetProgram)this.getEntityState().getCurrentProgram()).getTarget()) != null) {
            var6.calcWorldTransformRelative(((Ship)this.getEntity()).getSectorId(), ((GameServerState)((Ship)this.getEntity()).getState()).getUniverse().getSector(((Ship)this.getEntity()).getSectorId()).pos);
            if (!checkTarget(var6, this)) {
               this.stateTransition(Transition.RESTART);
               return false;
            }

            Vector3f var7 = ((Ship)this.getEntity()).getWorldTransform().origin;
            Vector3f var8 = new Vector3f(var6.getClientTransformCenterOfMass(serverTmp).origin);
            this.getRotDir().sub(var8, var7);
            Vector3f var12;
            if (var6.getLinearVelocity(this.tmp).lengthSquared() > 0.0F) {
               boolean var9 = true;
               if (this.getEntity() instanceof Ship) {
                  ShipManagerContainer var11;
                  var9 = !(var11 = ((Ship)this.getEntity()).getManagerContainer()).getBeam().hasAtLeastOneCoreUnit() || var11.getWeapon().hasAtLeastOneCoreUnit() || var11.getMissile().hasAtLeastOneCoreUnit();
               }

               if (var9) {
                  var12 = Vector3fTools.predictPoint(var8, var6.getLinearVelocity(this.tmp), this.getEntityState().getAntiMissileShootingSpeed(), var7);
                  this.getRotDir().set(var12);
               }
            }

            Vector3f var10;
            (var10 = new Vector3f(this.getRotDir())).normalize();
            var12 = GlUtil.getForwardVector(new Vector3f(), (Transform)((Ship)this.getEntity()).getWorldTransform());
            (new Vector3f()).sub(var12, var10);
            if (var10.epsilonEquals(var12, 0.08F)) {
               this.stateTransition(Transition.IN_SHOOTING_POSITION);
               this.descString = "INRANGE: " + var10 + "; " + var12;
               return true;
            }

            this.descString = "OORANGE: " + var10 + "; " + var12;
         } else {
            System.err.println("[AI] " + this.getEntity() + " HAS NO TARGET: resetting");
            this.stateTransition(Transition.RESTART);
         }

         return false;
      }
   }

   public String getDescString() {
      return this.descString;
   }

   public void updateAI(AIShipControllerStateUnit var1, Timer var2, Ship var3, ShipAIEntity var4) throws FSMException {
      ((Ship)this.getEntity()).getNetworkObject().moveDir.set(new Vector3f(0.0F, 0.0F, 0.0F));
      ((Ship)this.getEntity()).getNetworkObject().targetPosition.set(new Vector3f(0.0F, 0.0F, 0.0F));
      ((Ship)this.getEntity()).getNetworkObject().targetVelocity.set(new Vector3f(0.0F, 0.0F, 0.0F));
      ((TurretShipAIEntity)var4).orientateDir.set(this.getRotDir());
      ((Ship)this.getEntity()).getNetworkObject().orientationDir.set(((TurretShipAIEntity)var4).orientateDir, 0.0F);
      var4.orientate(var2, Quat4fTools.getNewQuat(((TurretShipAIEntity)var4).orientateDir.x, ((TurretShipAIEntity)var4).orientateDir.y, ((TurretShipAIEntity)var4).orientateDir.z, 0.0F));
   }
}
