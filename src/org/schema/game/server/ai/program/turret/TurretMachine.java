package org.schema.game.server.ai.program.turret;

import org.schema.game.server.ai.program.common.states.Waiting;
import org.schema.game.server.ai.program.turret.states.EngagingTurretTarget;
import org.schema.game.server.ai.program.turret.states.SeachingForTurretTarget;
import org.schema.game.server.ai.program.turret.states.TurretShootAtTarget;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.MachineProgram;
import org.schema.schine.ai.stateMachines.FiniteStateMachine;
import org.schema.schine.ai.stateMachines.Message;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public class TurretMachine extends FiniteStateMachine {
   public TurretMachine(AiEntityStateInterface var1, MachineProgram var2) {
      super(var1, var2, "");
   }

   public void addTransition(State var1, Transition var2, State var3) {
      var1.addTransition(var2, var3);
   }

   public void createFSM(String var1) {
      Transition var15 = Transition.SEARCH_FOR_TARGET;
      Transition var2 = Transition.ENEMY_FIRE;
      Transition var3 = Transition.RESTART;
      Transition var4 = Transition.TARGET_AQUIRED;
      Transition var5 = Transition.STOP;
      Transition var6 = Transition.TARGET_OUT_OF_RANGE;
      Transition var7 = Transition.TARGET_DESTROYED;
      Transition var8 = Transition.IN_SHOOTING_POSITION;
      Transition var9 = Transition.SHOOTING_COMPLETED;
      AiEntityStateInterface var10 = this.getObj();
      SeachingForTurretTarget var11 = new SeachingForTurretTarget(var10);
      EngagingTurretTarget var12 = new EngagingTurretTarget(var10);
      Waiting var13 = new Waiting(var10);
      TurretShootAtTarget var14 = new TurretShootAtTarget(var10);
      var13.addTransition(var15, var11);
      var13.addTransition(var5, var13);
      var13.addTransition(var3, var13);
      var11.addTransition(var4, var12);
      var11.addTransition(var5, var13);
      var11.addTransition(var3, var13);
      var12.addTransition(var7, var11);
      var12.addTransition(var8, var14);
      var12.addTransition(var6, var11);
      var12.addTransition(var2, var11);
      var12.addTransition(var5, var13);
      var12.addTransition(var3, var13);
      var14.addTransition(var2, var11);
      var14.addTransition(var9, var12);
      var14.addTransition(var5, var13);
      var14.addTransition(var3, var13);
      this.setStartingState(var13);
   }

   public void onMsg(Message var1) {
   }
}
