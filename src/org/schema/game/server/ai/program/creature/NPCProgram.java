package org.schema.game.server.ai.program.creature;

import java.util.HashMap;
import org.schema.game.common.controller.ai.AIConfiguationElements;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.ai.CreatureAIEntity;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.creature.character.AICreatureProgramInterface;
import org.schema.schine.ai.stateMachines.AIConfiguationElementsInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.FiniteStateMachine;
import org.schema.schine.ai.stateMachines.Transition;

public class NPCProgram extends TargetProgram implements AICreatureProgramInterface {
   private static final String CHAR = "CHAR";
   private static final String MOVE = "MOVE";
   private static final String ATT = "ATT";

   public NPCProgram(CreatureAIEntity var1, boolean var2) {
      super(var1, var2);

      assert super.getMachine() instanceof NPCMachine : super.getMachine() + "; " + this.getStartMachine() + "; " + this.machines;

   }

   public void onAISettingChanged(AIConfiguationElementsInterface var1) throws FSMException {
      if (((AIConfiguationElements)var1).getType() == Types.ORDER) {
         this.changedOrder((SimpleTransformableSendableObject)null);
      }

   }

   public FiniteStateMachine getMachine() {
      assert super.getMachine() instanceof NPCMachine : super.getMachine();

      return super.getMachine();
   }

   protected String getStartMachine() {
      return "CHAR";
   }

   protected void initializeMachines(HashMap var1) {
      var1.put("CHAR", new NPCMachine(this.getEntityState(), this));
      var1.put("MOVE", new NPCMoveMachine(this.getEntityState(), this));
      var1.put("ATT", new NPCAttackMachine(this.getEntityState(), this));
   }

   public void updateOtherMachines() throws FSMException {
      super.updateOtherMachines();
      this.getOtherMachine("ATT").update();
      this.getOtherMachine("MOVE").update();
   }

   public void underFire(SimpleTransformableSendableObject var1) throws FSMException {
      if (((AICreature)((CreatureAIEntity)this.getEntityState()).getEntity()).getAiConfiguration().isAttackOnAttacked() && this.getTarget() == null) {
         this.setTarget(var1);
         this.getOtherMachine("ATT").getFsm().getCurrentState().stateTransition(Transition.ENEMY_FIRE);
      }

   }

   public void changedOrder(SimpleTransformableSendableObject var1) throws FSMException {
      this.getOtherMachine("CHAR").getFsm().getCurrentState().stateTransition(Transition.STOP);
   }

   public void enemyProximity(SimpleTransformableSendableObject var1) throws FSMException {
      if (((AICreature)((CreatureAIEntity)this.getEntityState()).getEntity()).getAiConfiguration().isAttackOnProximity() && this.getTarget() == null) {
         this.setTarget(var1);
         this.getOtherMachine("ATT").getFsm().getCurrentState().stateTransition(Transition.ENEMY_PROXIMITY);
      }

   }

   public void stopCurrent(SimpleTransformableSendableObject var1) throws FSMException {
      this.setTarget(var1);
      this.getMachine().getFsm().getCurrentState().stateTransition(Transition.STOP);
   }

   public void onNoPath() throws FSMException {
      try {
         this.getOtherMachine("MOVE").getFsm().getCurrentState().stateTransition(Transition.PATH_FAILED);
      } catch (FSMException var1) {
         var1.printStackTrace();
      }
   }

   public void attack(SimpleTransformableSendableObject var1) throws FSMException {
      this.setTarget(var1);
      this.getOtherMachine("ATT").getFsm().getCurrentState().stateTransition(Transition.ENEMY_FIRE);
   }
}
