package org.schema.game.server.ai.program.creature.character.states;

import javax.vecmath.Vector3f;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.ShootingStateInterface;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.creature.character.AICreatureMachineInterface;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.AiInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class CharacterShooting extends CharacterState implements ShootingStateInterface {
   private final Vector3f targetPosition = new Vector3f();
   private final Vector3f targetVelocity = new Vector3f();
   private Vector3f dist = new Vector3f();
   private int targetid;
   private byte targetType;
   private Vector3f tmp = new Vector3f();

   public CharacterShooting(AiEntityStateInterface var1, AICreatureMachineInterface var2) {
      super(var1, var2);
   }

   public boolean onEnter() {
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      SimpleGameObject var1;
      if ((var1 = ((TargetProgram)this.getEntityState().getCurrentProgram()).getTarget()) != null) {
         var1.calcWorldTransformRelative(((AICreature)this.getEntity()).getSectorId(), ((GameServerState)((AICreature)this.getEntity()).getState()).getUniverse().getSector(((AICreature)this.getEntity()).getSectorId()).pos);
         if (var1 == this.getEntity() || !this.checkTarget(var1)) {
            this.stateTransition(Transition.RESTART);
            return false;
         }

         Vector3f var2;
         if ((var2 = var1.getLinearVelocity(this.tmp)) == null) {
            this.stateTransition(Transition.RESTART);
            return false;
         }

         Vector3f var3 = new Vector3f(var1.getClientTransformCenterOfMass(serverTmp).origin);
         Vector3f var4;
         (var4 = new Vector3f(((AICreature)this.getEntity()).getWorldTransform().origin)).sub(var3);
         AiEntityStateInterface var5 = ((AiInterface)this.getEntity()).getAiConfiguration().getAiEntityState();
         float var6 = 10.0F;
         if (var5 instanceof ShipAIEntity) {
            var6 = ((ShipAIEntity)var5).getShootingDifficulty(var1);
         }

         if (var1 instanceof Ship && ((Ship)var1).isJammingFor((SimpleTransformableSendableObject)this.getEntity())) {
            var6 = Math.max(1.0F, var6 * 0.1F);
         }

         var3.x = (float)((Math.random() - 0.5D) * (double)(var4.length() / var6));
         var3.y = (float)((Math.random() - 0.5D) * (double)(var4.length() / var6));
         var3.z = (float)((Math.random() - 0.5D) * (double)(var4.length() / var6));
         this.getTargetPosition().set(var3);
         this.getTargetVelocity().set(var2);
         this.targetid = var1.getAsTargetId();
         this.targetType = var1.getTargetType();
      } else {
         System.err.println("SHOOTING null");
      }

      return false;
   }

   public boolean checkTarget(SimpleGameObject var1) {
      if (!this.checkAsTarget(var1)) {
         return false;
      } else {
         this.dist.sub(var1.getClientTransform().origin, ((AICreature)this.getEntity()).getWorldTransform().origin);
         if (var1 instanceof Ship && ((Ship)var1).isCloakedFor((SimpleTransformableSendableObject)this.getEntity())) {
            return false;
         } else if (var1 instanceof Ship && ((Ship)var1).isJammingFor((SimpleTransformableSendableObject)this.getEntity()) && this.dist.length() > this.getEntityState().getShootingRange() / 4.0F) {
            return false;
         } else {
            return this.dist.length() <= this.getEntityState().getShootingRange();
         }
      }
   }

   private boolean checkAsTarget(SimpleGameObject var1) {
      if (!var1.isInPhysics()) {
         return false;
      } else {
         return !var1.isHidden();
      }
   }

   public int getTargetId() {
      return this.targetid;
   }

   public byte getTargetType() {
      return this.targetType;
   }

   public Vector3f getTargetPosition() {
      return this.targetPosition;
   }

   public Vector3f getTargetVelocity() {
      return this.targetVelocity;
   }
}
