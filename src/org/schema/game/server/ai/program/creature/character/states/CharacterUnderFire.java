package org.schema.game.server.ai.program.creature.character.states;

import org.schema.game.server.ai.program.creature.character.AICreatureMachineInterface;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class CharacterUnderFire extends CharacterState {
   public CharacterUnderFire(AiEntityStateInterface var1, AICreatureMachineInterface var2) {
      super(var1, var2);
   }

   public boolean onEnter() {
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      this.stateTransition(Transition.ATTACK);
      return false;
   }
}
