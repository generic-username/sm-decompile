package org.schema.game.server.ai.program.creature.character.states;

import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.server.ai.program.creature.character.AICreatureMachineInterface;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;

public class CharacterFollowing extends CharacterState {
   public CharacterFollowing(AiEntityStateInterface var1, AICreatureMachineInterface var2) {
      super(var1, var2);
   }

   public boolean onEnter() {
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      if (this.getEntityState().getFollowTarget() != null) {
         if (this.getEntityState().getFollowTarget().isHidden()) {
            try {
               ((AICreature)this.getEntity()).getAiConfiguration().get(Types.ORDER).switchSetting("Idling", true);
            } catch (StateParameterNotFoundException var2) {
               var2.printStackTrace();
            }

            this.stateTransition(Transition.RESTART);
         } else if (this.getEntityState().canPlotPath()) {
            try {
               this.getEntityState().plotSecondaryAbsolutePath(this.getEntityState().getFollowTarget().getWorldTransform().origin);
            } catch (FSMException var1) {
               var1.printStackTrace();
            }
         }
      }

      return false;
   }
}
