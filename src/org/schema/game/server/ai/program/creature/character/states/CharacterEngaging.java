package org.schema.game.server.ai.program.creature.character.states;

import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.creature.character.AICreatureMachineInterface;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class CharacterEngaging extends CharacterState {
   private int shootTime;

   public CharacterEngaging(AiEntityStateInterface var1, AICreatureMachineInterface var2) {
      super(var1, var2);
   }

   public boolean onEnter() {
      this.shootTime = 3000;
      if (this.getEntityState().canPlotPath() && this.getEntityState().isMoveRandomlyWhenEngaging()) {
         try {
            this.getEntityState().plotSecondaryPath();
         } catch (FSMException var1) {
            var1.printStackTrace();
         }
      }

      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      SimpleGameObject var1;
      if ((var1 = ((TargetProgram)this.getEntityState().getCurrentProgram()).getTarget()) == null) {
         this.stateTransition(Transition.RESTART);
         return false;
      } else {
         synchronized(var1.getState().getLocalAndRemoteObjectContainer().getLocalObjects()) {
            if (!var1.existsInState()) {
               ((TargetProgram)this.getEntityState().getCurrentProgram()).setTarget((SimpleGameObject)null);
               System.err.println("RESET TARGET: didn't exist anymore: " + var1);
               this.stateTransition(Transition.RESTART);
               return false;
            }
         }

         if (((AICreature)this.getEntityState().getEntity()).getAiConfiguration().isStopAttacking() && System.currentTimeMillis() - ((TargetProgram)this.getEntityState().getCurrentProgram()).getTargetAquiredTime() > (long)(this.shootTime + ((TargetProgram)this.getEntityState().getCurrentProgram()).getTargetHoldTime())) {
            ((TargetProgram)this.getEntityState().getCurrentProgram()).setTarget((SimpleGameObject)null);
            this.stateTransition(Transition.RESTART);
            return false;
         } else if (((AICreature)this.getEntity()).isInShootingRange(var1)) {
            if (var1.isHidden()) {
               this.stateTransition(Transition.RESTART);
               ((TargetProgram)this.getEntityState().getCurrentProgram()).setTarget((SimpleGameObject)null);
               return false;
            } else {
               if (this.getEntityState().canPlotPath() && this.getEntityState().isMoveUpToTargetWhenEngaging()) {
                  try {
                     this.getEntityState().plotSecondaryAbsolutePath(var1.getWorldTransform().origin);
                  } catch (FSMException var3) {
                     var3.printStackTrace();
                  }
               }

               this.stateTransition(Transition.TARGET_IN_RANGE);
               return false;
            }
         } else {
            this.stateTransition(Transition.TARGET_OUT_OF_RANGE);
            return false;
         }
      }
   }
}
