package org.schema.game.server.ai.program.creature.character.states;

import org.schema.game.common.data.world.Universe;
import org.schema.game.server.ai.program.creature.character.AICreatureMachineInterface;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class CharacterRandomWaiting extends CharacterState {
   private int min;
   private int max;
   private int waitingTime;
   private long waitingStarted;

   public CharacterRandomWaiting(AiEntityStateInterface var1, int var2, int var3, AICreatureMachineInterface var4) {
      super(var1, var4);
      this.min = var2;
      this.max = Math.max(var3, var2);
   }

   public boolean onEnter() {
      this.waitingTime = this.min + Universe.getRandom().nextInt(this.max - this.min);
      this.waitingStarted = System.currentTimeMillis();
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      if (System.currentTimeMillis() - this.waitingStarted > (long)this.waitingTime) {
         this.stateTransition(Transition.WAIT_COMPLETED);
      }

      return false;
   }
}
