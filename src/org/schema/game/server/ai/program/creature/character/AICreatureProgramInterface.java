package org.schema.game.server.ai.program.creature.character;

import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.ai.stateMachines.FSMException;

public interface AICreatureProgramInterface {
   void underFire(SimpleTransformableSendableObject var1) throws FSMException;

   void changedOrder(SimpleTransformableSendableObject var1) throws FSMException;

   void enemyProximity(SimpleTransformableSendableObject var1) throws FSMException;

   void stopCurrent(SimpleTransformableSendableObject var1) throws FSMException;

   void onNoPath() throws FSMException;
}
