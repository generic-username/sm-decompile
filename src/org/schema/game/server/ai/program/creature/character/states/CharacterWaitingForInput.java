package org.schema.game.server.ai.program.creature.character.states;

import javax.vecmath.Vector3f;
import org.schema.game.common.controller.ai.AIConfiguationElements;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.server.ai.AlreadyAtTargetException;
import org.schema.game.server.ai.CannotReachTargetException;
import org.schema.game.server.ai.program.creature.character.AICreatureMachineInterface;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.network.server.ServerMessage;

public class CharacterWaitingForInput extends CharacterState {
   private Vector3f currentlyPlotted;

   public CharacterWaitingForInput(AiEntityStateInterface var1, AICreatureMachineInterface var2) {
      super(var1, var2);
   }

   public boolean onEnter() {
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      AIConfiguationElements var1 = ((AICreature)this.getEntity()).getAiConfiguration().get(Types.ORDER);
      ((AICreature)this.getEntity()).getAiConfiguration().get(Types.ORIGIN_X).getCurrentState();
      ((AICreature)this.getEntity()).getAiConfiguration().get(Types.ORIGIN_Y).getCurrentState();
      ((AICreature)this.getEntity()).getAiConfiguration().get(Types.ORIGIN_Z).getCurrentState();
      String var7;
      if ((var7 = (String)var1.getCurrentState()).equals("Roaming")) {
         this.stateTransition(Transition.ROAM);
         return true;
      } else if (var7.equals("Attacking")) {
         this.getEntityState().setAttackTarget((String)((AICreature)this.getEntity()).getAiConfiguration().get(Types.ATTACK_TARGET).getCurrentState());
         this.stateTransition(Transition.ATTACK);
         return true;
      } else if (var7.equals("Idling")) {
         return true;
      } else if (var7.equals("Following")) {
         this.getEntityState().setFollowTarget((String)((AICreature)this.getEntity()).getAiConfiguration().get(Types.FOLLOW_TARGET).getCurrentState());
         this.stateTransition(Transition.FOLLOW);
         return true;
      } else if (var7.equals("GoTo")) {
         try {
            this.getEntityState().makeMoveTarget();
            Vector3f var8 = this.getEntityState().getGotoTarget();
            this.stateTransition(Transition.MOVE);
            this.currentlyPlotted = new Vector3f(var8);
         } catch (CannotReachTargetException var5) {
            var5.printStackTrace();

            try {
               ((GameServerState)((AICreature)this.getEntity()).getState()).getPlayerFromName(((String)((AICreature)this.getEntity()).getAiConfiguration().get(Types.OWNER).getCurrentState()).replaceAll("ENTITY_PLAYERSTATE_", "")).sendServerMessage(new ServerMessage(new Object[]{454}, 3));
            } catch (PlayerNotFountException var4) {
               var4.printStackTrace();
            }

            try {
               ((AICreature)this.getEntity()).getAiConfiguration().get(Types.ORDER).switchSetting("Idling", true);
            } catch (StateParameterNotFoundException var3) {
               var3.printStackTrace();
            }
         } catch (AlreadyAtTargetException var6) {
            try {
               ((AICreature)this.getEntity()).getAiConfiguration().get(Types.ORDER).switchSetting("Idling", true);
            } catch (StateParameterNotFoundException var2) {
               var2.printStackTrace();
            }
         }

         return true;
      } else {
         throw new NullPointerException();
      }
   }
}
