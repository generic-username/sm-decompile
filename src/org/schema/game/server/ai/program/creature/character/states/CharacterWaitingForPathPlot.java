package org.schema.game.server.ai.program.creature.character.states;

import org.schema.game.common.data.creature.AICreature;
import org.schema.game.server.ai.program.creature.character.AICreatureMachineInterface;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class CharacterWaitingForPathPlot extends CharacterState {
   public CharacterWaitingForPathPlot(AiEntityStateInterface var1, AICreatureMachineInterface var2) {
      super(var1, var2);
   }

   public String toString() {
      return "CharacterWaitingForPathPlot lastPath(" + this.getEntityState().lastPath + ")";
   }

   public boolean onEnter() {
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      if (((AICreature)this.getEntity()).getOwnerState().getCurrentPath() != null && ((AICreature)this.getEntity()).getOwnerState().getCurrentPath().size() > 0) {
         this.stateTransition(Transition.MOVE);
      } else {
         ((AICreature)this.getEntity()).getOwnerState().resetTargetToMove();
      }

      return false;
   }
}
