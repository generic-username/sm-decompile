package org.schema.game.server.ai.program.creature.character.states;

import org.schema.game.server.ai.CreatureAIEntity;
import org.schema.game.server.ai.program.common.states.GameState;
import org.schema.game.server.ai.program.creature.character.AICreatureMachineInterface;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FiniteStateMachine;
import org.schema.schine.ai.stateMachines.Transition;

public abstract class CharacterState extends GameState {
   public CharacterState(AiEntityStateInterface var1, AICreatureMachineInterface var2) {
      super(var1);
   }

   public void init(FiniteStateMachine var1) {
      super.init(var1);
      this.init((AICreatureMachineInterface)var1);
   }

   protected void init(AICreatureMachineInterface var1) {
      assert var1.getUnderFireState(this) != null;

      this.addTransition(Transition.ENEMY_FIRE, var1.getUnderFireState(this));
      this.addTransition(Transition.ENEMY_PROXIMITY, var1.getEnemyProximityState(this));
      this.addTransition(Transition.STOP, var1.getStopState(this));
      this.addTransition(Transition.RESTART, var1.getStopState(this));
   }

   public CreatureAIEntity getEntityState() {
      return (CreatureAIEntity)super.getEntityState();
   }
}
