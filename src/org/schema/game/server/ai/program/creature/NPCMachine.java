package org.schema.game.server.ai.program.creature;

import org.schema.game.server.ai.program.creature.character.AICreatureMachineInterface;
import org.schema.game.server.ai.program.creature.character.states.CharacterEngaging;
import org.schema.game.server.ai.program.creature.character.states.CharacterFollowing;
import org.schema.game.server.ai.program.creature.character.states.CharacterGoingToEnemy;
import org.schema.game.server.ai.program.creature.character.states.CharacterInEnemyProximity;
import org.schema.game.server.ai.program.creature.character.states.CharacterMovingToPosition;
import org.schema.game.server.ai.program.creature.character.states.CharacterOnAttackingOrder;
import org.schema.game.server.ai.program.creature.character.states.CharacterRallying;
import org.schema.game.server.ai.program.creature.character.states.CharacterRandomWaiting;
import org.schema.game.server.ai.program.creature.character.states.CharacterRoaming;
import org.schema.game.server.ai.program.creature.character.states.CharacterSearchingForTarget;
import org.schema.game.server.ai.program.creature.character.states.CharacterShooting;
import org.schema.game.server.ai.program.creature.character.states.CharacterState;
import org.schema.game.server.ai.program.creature.character.states.CharacterUnderFire;
import org.schema.game.server.ai.program.creature.character.states.CharacterWaiting;
import org.schema.game.server.ai.program.creature.character.states.CharacterWaitingForInput;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FiniteStateMachine;
import org.schema.schine.ai.stateMachines.Message;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public class NPCMachine extends FiniteStateMachine implements AICreatureMachineInterface {
   private CharacterUnderFire characterUnderFire;
   private CharacterInEnemyProximity characterInEnemyProximity;
   private CharacterWaiting waiting;

   public NPCMachine(AiEntityStateInterface var1, NPCProgram var2) {
      super(var1, var2, "");
   }

   public void addTransition(State var1, Transition var2, State var3) {
      var1.addTransition(var2, var3);
   }

   public void createFSM(String var1) {
      AiEntityStateInterface var14 = this.getObj();
      this.characterUnderFire = new CharacterUnderFire(this.getObj(), this);
      this.characterInEnemyProximity = new CharacterInEnemyProximity(this.getObj(), this);
      this.waiting = new CharacterWaiting(var14, this);
      Transition var2 = Transition.MOVE;
      Transition var10000 = Transition.SEARCH_FOR_TARGET;
      var10000 = Transition.ENEMY_FIRE;
      Transition var3 = Transition.RESTART;
      var10000 = Transition.HEALTH_LOW;
      var10000 = Transition.TARGET_AQUIRED;
      var10000 = Transition.STOP;
      var10000 = Transition.ENEMY_PROXIMITY;
      var10000 = Transition.TARGET_IN_RANGE;
      var10000 = Transition.TARGET_OUT_OF_RANGE;
      var10000 = Transition.TARGET_DESTROYED;
      var10000 = Transition.IN_SHOOTING_POSITION;
      var10000 = Transition.SHOOTING_COMPLETED;
      Transition var4 = Transition.WAIT_COMPLETED;
      Transition var5 = Transition.ROAM;
      Transition var6 = Transition.FOLLOW;
      Transition var7 = Transition.ATTACK;
      CharacterRandomWaiting var8 = new CharacterRandomWaiting(var14, 100, 500, this);
      CharacterRandomWaiting var9 = new CharacterRandomWaiting(var14, 2000, 12000, this);
      new CharacterSearchingForTarget(var14, this);
      CharacterMovingToPosition var10 = new CharacterMovingToPosition(var14, this);
      CharacterOnAttackingOrder var11 = new CharacterOnAttackingOrder(var14, this);
      CharacterRoaming var12 = new CharacterRoaming(var14, this);
      CharacterFollowing var13 = new CharacterFollowing(var14, this);
      new CharacterEngaging(var14, this);
      new CharacterRallying(var14, this);
      new CharacterGoingToEnemy(var14, this);
      new CharacterShooting(var14, this);
      CharacterWaitingForInput var15 = new CharacterWaitingForInput(var14, this);
      this.waiting.addTransition(var4, var8);
      var8.addTransition(var4, var15);
      var15.addTransition(var5, var12);
      var15.addTransition(var6, var13);
      var15.addTransition(var2, var10);
      var15.addTransition(var7, var11);
      var12.addTransition(var3, var9);
      var13.addTransition(var3, this.waiting);
      var10.addTransition(var3, this.waiting);
      var9.addTransition(var4, var8);
      this.characterUnderFire.addTransition(Transition.RALLY, this.waiting);
      this.characterUnderFire.addTransition(Transition.ATTACK, this.waiting);
      this.characterInEnemyProximity.addTransition(Transition.RALLY, this.waiting);
      this.characterInEnemyProximity.addTransition(Transition.ATTACK, this.waiting);
      this.setStartingState(this.waiting);
   }

   public void onMsg(Message var1) {
      var1.execute(this);
   }

   public State getUnderFireState(CharacterState var1) {
      return this.characterUnderFire;
   }

   public State getEnemyProximityState(CharacterState var1) {
      return this.characterInEnemyProximity;
   }

   public State getStopState(CharacterState var1) {
      return this.waiting;
   }
}
