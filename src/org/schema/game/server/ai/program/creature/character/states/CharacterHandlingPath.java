package org.schema.game.server.ai.program.creature.character.states;

import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.server.ai.program.creature.character.AICreatureMachineInterface;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class CharacterHandlingPath extends CharacterState {
   private Vector3i currentTarget;
   private Vector3i lastTarget;

   public CharacterHandlingPath(AiEntityStateInterface var1, AICreatureMachineInterface var2) {
      super(var1, var2);
   }

   public boolean onEnter() {
      this.currentTarget = null;
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      if (((AICreature)this.getEntity()).getOwnerState().getCurrentPath() == null) {
         this.stateTransition(Transition.PATH_FINISHED);
         return false;
      } else if (((AICreature)this.getEntity()).getOwnerState().getCurrentPath().isEmpty()) {
         this.stateTransition(Transition.PATH_FINISHED);
         return false;
      } else {
         if (((AICreature)this.getEntity()).getAffinity() != null) {
            long var1 = (Long)((AICreature)this.getEntity()).getOwnerState().getCurrentPath().remove(0);
            this.currentTarget = ElementCollection.getPosFromIndex(var1, new Vector3i());
            boolean var3;
            if (this.lastTarget != null && this.currentTarget != null) {
               do {
                  var3 = false;

                  for(int var4 = 0; var4 < 5; ++var4) {
                     if (this.currentTarget.equals(this.lastTarget.x, this.lastTarget.y + var4, this.lastTarget.z)) {
                        var3 = true;
                        break;
                     }
                  }

                  if (var3 && !((AICreature)this.getEntity()).getOwnerState().getCurrentPath().isEmpty()) {
                     var1 = (Long)((AICreature)this.getEntity()).getOwnerState().getCurrentPath().remove(0);
                     this.currentTarget = ElementCollection.getPosFromIndex(var1, new Vector3i());
                  }
               } while(!((AICreature)this.getEntity()).getOwnerState().getCurrentPath().isEmpty() && var3);
            }

            this.lastTarget = this.currentTarget;
            Vector3f var5;
            Vector3f var10000 = var5 = new Vector3f((float)(this.currentTarget.x - 16), (float)(this.currentTarget.y - 16), (float)(this.currentTarget.z - 16));
            var10000.y += ((AICreature)this.getEntity()).getCharacterHeight() * 0.5F - 0.1F;
            this.getEntityState().getCurrentMoveTarget().set(var5);
            this.stateTransition(Transition.MOVE);
         }

         return false;
      }
   }

   public boolean isOkToWalk(Vector3i var1, SegmentController var2) {
      SegmentPiece var3;
      if ((var3 = var2.getSegmentBuffer().getPointUnsave(var1)) == null) {
         return false;
      } else {
         if (var3.getType() == 0 || !ElementKeyMap.getInfo(var3.getType()).isPhysical(var3.isActive())) {
            Vector3i var5;
            ++(var5 = new Vector3i(var1)).y;
            if ((var3 = var2.getSegmentBuffer().getPointUnsave(var5)) == null) {
               return false;
            }

            if (var3.getType() == 0 || !ElementKeyMap.getInfo(var3.getType()).isPhysical(var3.isActive())) {
               --(var1 = new Vector3i(var1)).y;
               SegmentPiece var4;
               if ((var4 = var2.getSegmentBuffer().getPointUnsave(var1)) == null) {
                  return false;
               }

               if (var4.getType() != 0 && ElementKeyMap.getInfo(var4.getType()).isPhysical(var4.isActive())) {
                  return true;
               }
            }
         }

         return false;
      }
   }
}
