package org.schema.game.server.ai.program.creature.character.states;

import org.schema.game.server.ai.program.creature.character.AICreatureMachineInterface;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;

public class CharacterOnAttackingOrder extends CharacterState {
   public CharacterOnAttackingOrder(AiEntityStateInterface var1, AICreatureMachineInterface var2) {
      super(var1, var2);
   }

   public boolean onEnter() {
      if (this.getEntityState().getAttackTarget() != null) {
         this.getEntityState().attackSecondary(this.getEntityState().getAttackTarget());
      }

      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      return false;
   }
}
