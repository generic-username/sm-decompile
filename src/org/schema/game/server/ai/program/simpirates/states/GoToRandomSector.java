package org.schema.game.server.ai.program.simpirates.states;

import java.sql.SQLException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.data.simulation.groups.SimulationGroup;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class GoToRandomSector extends SimulationGroupState {
   public GoToRandomSector(AiEntityStateInterface var1) {
      super(var1);
   }

   public boolean onEnter() {
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      SimulationGroup var1;
      if ((var1 = this.getSimGroup()).getMembers().size() > 0) {
         try {
            Vector3i var4;
            (var4 = var1.getSector((String)var1.getMembers().get(0), new Vector3i())).add(Universe.getRandom().nextInt(20) - 10, Universe.getRandom().nextInt(20) - 10, Universe.getRandom().nextInt(20) - 10);
            ((TargetProgram)this.getEntityState().getCurrentProgram()).setSectorTarget(var4);
         } catch (EntityNotFountException var2) {
            var2.printStackTrace();
            this.stateTransition(Transition.DISBAND);
         } catch (SQLException var3) {
            var3.printStackTrace();
            this.stateTransition(Transition.DISBAND);
         }

         this.stateTransition(Transition.MOVE_TO_SECTOR);
      } else {
         System.err.println("[SIM][AI] RETURNING HOME TO (NO MEMBERS) " + var1.getStartSector());
         ((TargetProgram)this.getEntityState().getCurrentProgram()).setSectorTarget(new Vector3i(var1.getStartSector()));
         this.stateTransition(Transition.MOVE_TO_SECTOR);
      }

      return false;
   }
}
