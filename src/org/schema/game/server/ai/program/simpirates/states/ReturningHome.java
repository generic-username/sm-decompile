package org.schema.game.server.ai.program.simpirates.states;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.data.simulation.groups.SimulationGroup;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class ReturningHome extends SimulationGroupState {
   public ReturningHome(AiEntityStateInterface var1) {
      super(var1);
   }

   public boolean onEnter() {
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      SimulationGroup var1 = this.getSimGroup();
      System.err.println("[SIM][AI] RETURNING HOME TO " + var1.getStartSector());
      ((TargetProgram)this.getEntityState().getCurrentProgram()).setSectorTarget(new Vector3i(var1.getStartSector()));
      this.stateTransition(Transition.MOVE_TO_SECTOR);
      return false;
   }
}
