package org.schema.game.server.ai.program.simpirates.states;

import java.sql.SQLException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class MovingToSector extends SimulationGroupState {
   private long lastMove;

   public MovingToSector(AiEntityStateInterface var1) {
      super(var1);
   }

   public boolean onEnter() {
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      TargetProgram var1;
      if ((var1 = (TargetProgram)this.getEntityState().getCurrentProgram()).getSectorTarget() == null) {
         this.stateTransition(Transition.RESTART);
         return true;
      } else {
         Vector3i var2 = new Vector3i();

         for(int var3 = 0; var3 < this.getSimGroup().getMembers().size(); ++var3) {
            try {
               this.getSimGroup().getSector((String)this.getSimGroup().getMembers().get(var3), var2);
               if (var2.equals(var1.getSectorTarget())) {
                  this.stateTransition(Transition.TARGET_SECTOR_REACHED);
                  return true;
               }
            } catch (EntityNotFountException var11) {
               var11.printStackTrace();
            } catch (SQLException var12) {
               var12.printStackTrace();
            }
         }

         if (System.currentTimeMillis() - this.lastMove > 1000L) {
            boolean var14 = this.getSimGroup().getState().getSimulationManager().existsGroupInSector(var1.getSectorTarget());
            boolean var4 = false;

            for(int var13 = 0; var13 < this.getSimGroup().getMembers().size(); ++var13) {
               boolean var5;
               if (this.getSimGroup().getState().getUniverse().isSectorLoaded(var1.getSectorTarget())) {
                  var5 = this.getSimGroup().moveToTarget((String)this.getSimGroup().getMembers().get(var13), var1.getSectorTarget());
               } else if (var14) {
                  var4 = true;
                  var5 = true;
                  new Vector3i();
                  Vector3i var6 = Element.DIRECTIONSi[Universe.getRandom().nextInt(Element.DIRECTIONSi.length)];

                  try {
                     Vector3i var7;
                     (var7 = this.getSimGroup().getSector((String)this.getSimGroup().getMembers().get(var13), new Vector3i())).add(var6);
                     this.getSimGroup().moveToTarget((String)this.getSimGroup().getMembers().get(var13), var7);
                  } catch (EntityNotFountException var8) {
                     var8.printStackTrace();
                  } catch (SQLException var9) {
                     var9.printStackTrace();
                  } catch (Exception var10) {
                     var10.printStackTrace();
                  }
               } else {
                  var5 = this.getSimGroup().moveToTarget((String)this.getSimGroup().getMembers().get(var13), var1.getSectorTarget());
               }

               if (!var5) {
                  System.err.println("[SIMULATION] Exception while moving entity: REMOVING FROM MEMBERS: " + (String)this.getSimGroup().getMembers().get(var13));
                  this.getSimGroup().getMembers().remove(var13);
                  --var13;
               }

               this.lastMove = System.currentTimeMillis();
            }

            if (var4) {
               System.err.println("[MOVING TO SECTOR] Position " + var1.getSectorTarget() + " occupied for " + this.getSimGroup().getMembers());
            }
         }

         return false;
      }
   }
}
