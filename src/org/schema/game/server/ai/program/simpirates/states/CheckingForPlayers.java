package org.schema.game.server.ai.program.simpirates.states;

import java.util.Iterator;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.data.simulation.groups.SimulationGroup;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class CheckingForPlayers extends SimulationGroupState {
   private long startTime;

   public CheckingForPlayers(AiEntityStateInterface var1) {
      super(var1);
   }

   public boolean onEnter() {
      this.startTime = System.currentTimeMillis();
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      SimulationGroup var1 = this.getSimGroup();
      Vector3i var2 = new Vector3i();
      Iterator var3 = var1.getState().getPlayerStatesByName().values().iterator();

      PlayerState var4;
      do {
         if (!var3.hasNext()) {
            if (System.currentTimeMillis() - this.startTime > 60000L) {
               this.stateTransition(Transition.DISBAND);
               return true;
            }

            return false;
         }

         var4 = (PlayerState)var3.next();
         var2.sub(var4.getCurrentSector(), var1.getStartSector());
      } while(var2.length() >= 6.0F);

      ((TargetProgram)this.getEntityState().getCurrentProgram()).setSectorTarget(new Vector3i(var4.getCurrentSector()));
      var1.sendInvestigationMessage(var4);
      this.stateTransition(Transition.MOVE_TO_SECTOR);
      return true;
   }
}
