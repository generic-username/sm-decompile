package org.schema.game.server.ai.program.simpirates.states;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.data.simulation.groups.TargetSectorSimulationGroup;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class MakingMoveDesicion extends SimulationGroupState {
   private TargetSectorSimulationGroup targetGroup;

   public MakingMoveDesicion(AiEntityStateInterface var1, TargetSectorSimulationGroup var2) {
      super(var1);
      this.targetGroup = var2;

      assert this.targetGroup != null;

   }

   public boolean onEnter() {
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      assert this.targetGroup.targetSector != null;

      ((TargetProgram)this.getEntityState().getCurrentProgram()).setSectorTarget(new Vector3i(this.targetGroup.targetSector));
      this.stateTransition(Transition.MOVE_TO_SECTOR);
      return false;
   }
}
