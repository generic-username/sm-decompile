package org.schema.game.server.ai.program.simpirates.states;

import java.util.Iterator;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.simulation.groups.SimulationGroup;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;

public class Planning extends SimulationGroupState {
   public Planning(AiEntityStateInterface var1) {
      super(var1);
   }

   public boolean onEnter() {
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      SimulationGroup var1 = this.getSimGroup();
      Vector3i var2 = new Vector3i();
      Iterator var3 = var1.getState().getPlayerStatesByName().values().iterator();

      while(var3.hasNext()) {
         PlayerState var4 = (PlayerState)var3.next();
         var2.sub(var4.getCurrentSector(), var1.getStartSector());
      }

      return false;
   }
}
