package org.schema.game.server.ai.program.fleetcontrollable.states;

import com.bulletphysics.linearmath.Transform;
import java.util.Iterator;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.FleetMember;
import org.schema.game.common.data.world.SectorTransformation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.TransformaleObjectTmpVars;
import org.schema.game.server.ai.AIShipControllerStateUnit;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.common.states.ShipGameState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.core.Timer;

public abstract class FleetFormationingAbstract extends ShipGameState {
   private Vector3f movingDir = new Vector3f();
   private Quat4f orientationDir = new Quat4f();
   private float dist;
   private boolean evading;
   private boolean orientating;
   private static final TransformaleObjectTmpVars v = new TransformaleObjectTmpVars();
   int evadingCounter = 0;
   long lastEvading;

   public FleetFormationingAbstract(AiEntityStateInterface var1) {
      super(var1);
   }

   public Vector3f getMovingDir() {
      return this.movingDir;
   }

   public boolean onEnter() {
      this.movingDir.set(0.0F, 0.0F, 0.0F);
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      if (((Ship)this.getEntity()).railController.isDockedAndExecuted()) {
         return false;
      } else {
         this.orientating = false;
         Fleet var1;
         if ((var1 = ((Ship)this.getEntity()).getFleet()) != null && !var1.isEmpty() && !var1.isFlagShip((SegmentController)this.getEntity())) {
            this.orientationDir.set(0.0F, 0.0F, 0.0F, 0.0F);
            this.getMovingDir().set(0.0F, 0.0F, 0.0F);
            if (!((Ship)this.getEntity()).getSegmentBuffer().isFullyLoaded()) {
               this.getMovingDir().set(0.0F, 0.0F, 0.1F);
               return false;
            } else if (((Ship)this.getEntity()).isCoreOverheating()) {
               this.stateTransition(Transition.RESTART);
               return false;
            } else {
               FleetMember var10;
               if (!(var10 = var1.getFlagShip()).isLoaded()) {
                  ((TargetProgram)this.getEntityState().getCurrentProgram()).setSectorTarget(new Vector3i(var10.getSector()));
                  this.stateTransition(Transition.FLEET_MOVE_TO_FLAGSHIP_SECTOR);
                  return true;
               } else {
                  SegmentController var11 = var10.getLoaded();
                  boolean var2 = false;
                  if (var11 != null) {
                     if (((ShipAIEntity)this.getEntityState()).fleetFormationPos.isEmpty()) {
                        return false;
                     }

                     Vector3f var3 = ((Ship)this.getEntity()).getWorldTransform().origin;
                     SectorTransformation var4 = (SectorTransformation)((ShipAIEntity)this.getEntityState()).fleetFormationPos.get(0);
                     float var5 = 100.0F;
                     if (var11.getSectorId() == ((Ship)this.getEntity()).getSectorId()) {
                        var5 = Vector3fTools.diffLength(((Ship)this.getEntity()).getWorldTransform().origin, ((SectorTransformation)((ShipAIEntity)this.getEntityState()).fleetFormationPos.get(((ShipAIEntity)this.getEntityState()).fleetFormationPos.size() - 1)).t.origin);
                     }

                     Transform var6;
                     (var6 = new Transform()).setIdentity();
                     var6.origin.set(var4.t.origin);
                     Transform var7;
                     (var7 = new Transform()).setIdentity();
                     int var8 = ((Ship)this.getEntity()).getSectorId();
                     Vector3i var9 = ((GameServerState)((Ship)this.getEntity()).getState()).getUniverse().getSector(var8).pos;
                     SimpleTransformableSendableObject.calcWorldTransformRelative(var8, var9, var4.sectorId, var6, var11.getState(), var11.isOnServer(), var7, v);

                     assert !Float.isNaN(var7.origin.x) : var7.origin;

                     this.getMovingDir().sub(var7.origin, var3);

                     assert !Float.isNaN(this.getMovingDir().x) : this.getMovingDir();

                     this.dist = this.getMovingDir().length();
                     this.evading = false;
                     if (((Ship)this.getEntity()).getProximityObjects().isEmpty()) {
                        if (this.dist < 3.0F && ((ShipAIEntity)this.getEntityState()).fleetFormationPos.size() > 1) {
                           ((ShipAIEntity)this.getEntityState()).fleetFormationPos.remove(0);
                           Quat4fTools.set(var11.getWorldTransform().basis, this.orientationDir);

                           assert !Float.isNaN(this.orientationDir.x) : this.orientationDir;

                           this.orientating = true;
                           return this.onUpdate();
                        }

                        if (((ShipAIEntity)this.getEntityState()).fleetFormationPos.size() == 1 && this.dist < 0.3F) {
                           this.getMovingDir().set(0.0F, 0.0F, 0.01F);
                           Quat4fTools.set(var11.getWorldTransform().basis, this.orientationDir);

                           assert !Float.isNaN(this.orientationDir.x) : this.orientationDir;

                           this.orientating = true;
                        } else if (var5 < 15.0F && var11.getSectorId() == ((Ship)this.getEntity()).getSectorId()) {
                           Quat4fTools.set(var11.getWorldTransform().basis, this.orientationDir);

                           assert !Float.isNaN(this.orientationDir.x) : this.orientationDir;

                           this.orientating = true;
                        }
                     } else {
                        Vector3f var12 = new Vector3f(((Ship)this.getEntity()).proximityVector);
                        boolean var13 = false;
                        Iterator var14 = ((Ship)this.getEntity()).getProximityObjects().iterator();

                        label136:
                        while(true) {
                           SegmentController var16;
                           do {
                              int var15;
                              do {
                                 if (!var14.hasNext()) {
                                    if (var2 && this.dist < 10.0F) {
                                       this.getMovingDir().set(0.0F, 0.0F, 0.1F);
                                       break label136;
                                    }

                                    if (var13) {
                                       if ((new Vector3f(0.0F, 1.0F, 0.0F)).dot(var12) > 0.0F) {
                                          var12.y += 0.5F;
                                       } else {
                                          var12.y -= 0.5F;
                                       }

                                       var12.normalize();
                                       var12.scale(10.0F);
                                       this.getMovingDir().normalize();
                                       this.getMovingDir().scale(0.9F);
                                       this.getMovingDir().add(var12);

                                       assert !Float.isNaN(this.getMovingDir().x) : this.getMovingDir();

                                       this.evading = true;
                                    }
                                    break label136;
                                 }

                                 var15 = (Integer)var14.next();
                              } while((var16 = (SegmentController)((Ship)this.getEntity()).getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var15)).getSectorId() != ((Ship)this.getEntity()).getSectorId());

                              var13 = true;
                              Vector3f var17;
                              (var17 = new Vector3f()).sub(var3, var16.getWorldTransform().origin);
                              var17.normalize();
                              var12.add(var17);
                           } while(var16 instanceof Ship && ((Ship)var16).getFleet() == ((Ship)this.getEntity()).getFleet());

                           var2 = true;
                        }
                     }

                     assert !Float.isNaN(this.orientationDir.x) : this.orientationDir;

                     if (System.currentTimeMillis() - this.lastEvading > 1000L) {
                        this.evadingCounter = 0;
                     }

                     if (this.evading) {
                        if (!((ShipAIEntity)this.getEntityState()).isBigEvading()) {
                           ++this.evadingCounter;
                           this.lastEvading = System.currentTimeMillis();
                        }

                        if (this.evadingCounter > 130) {
                           ((ShipAIEntity)this.getEntityState()).setEvadingTime(3000L);
                           this.evadingCounter = 0;
                        }
                     }

                     this.onDistance(this.dist);
                  } else {
                     this.stateTransition(Transition.RESTART);
                  }

                  return false;
               }
            }
         } else {
            this.stateTransition(Transition.RESTART);
            return false;
         }
      }
   }

   protected abstract void onDistance(float var1) throws FSMException;

   public String getDescString() {
      return "(evade: " + this.evading + ", Orient: " + this.orientating + "; Dist: " + this.dist + "; moveDir: " + ((Ship)this.getEntity()).getNetworkObject().moveDir + ", orient: " + this.orientationDir + ")";
   }

   public void updateAI(AIShipControllerStateUnit var1, Timer var2, Ship var3, ShipAIEntity var4) throws FSMException {
      ((Ship)this.getEntity()).getNetworkObject().orientationDir.set(this.orientationDir);
      ((Ship)this.getEntity()).getNetworkObject().targetPosition.set(new Vector3f(0.0F, 0.0F, 0.0F));
      ((Ship)this.getEntity()).getNetworkObject().moveDir.set(this.getMovingDir());

      assert !Float.isNaN(this.orientationDir.x) : this.orientationDir;

      boolean var5 = Quat4fTools.isZero(this.orientationDir);
      if (this.getMovingDir().lengthSquared() > 0.0F) {
         var4.moveTo(var2, this.getMovingDir(), var5);
      }

      if (!var5) {
         assert !Float.isNaN(this.orientationDir.x) : this.orientationDir;

         var4.orientate(var2, this.orientationDir);
      }

   }
}
