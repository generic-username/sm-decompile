package org.schema.game.server.ai.program.fleetcontrollable;

import org.schema.game.server.ai.program.fleetcontrollable.states.FleetBreaking;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetEngagingTarget;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetEvadingTarget;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetFormationing;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetFormationingMining;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetGettingToTarget;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetIdleWaiting;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetMining;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetMovingToSector;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetRally;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetSeachingForTarget;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetShootAtTarget;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FiniteStateMachine;
import org.schema.schine.ai.stateMachines.Message;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public class FleetControllableMachine extends FiniteStateMachine {
   public FleetControllableMachine(AiEntityStateInterface var1, FleetControllableProgram var2) {
      super(var1, var2, "");
   }

   public void addTransition(State var1, Transition var2, State var3) {
      var1.addTransition(var2, var3);
   }

   public void createFSM(String var1) {
      AiEntityStateInterface var13 = this.getObj();
      FleetSeachingForTarget var2 = new FleetSeachingForTarget(var13);
      FleetMovingToSector var3 = new FleetMovingToSector(var13);
      FleetEngagingTarget var4 = new FleetEngagingTarget(var13);
      FleetFormationing var5 = new FleetFormationing(var13);
      FleetGettingToTarget var6 = new FleetGettingToTarget(var13);
      FleetEvadingTarget var7 = new FleetEvadingTarget(var13);
      FleetRally var8 = new FleetRally(var13);
      FleetBreaking var9 = new FleetBreaking(var13);
      FleetIdleWaiting var10 = new FleetIdleWaiting(var13);
      FleetShootAtTarget var11 = new FleetShootAtTarget(var13);
      FleetFormationingMining var12 = new FleetFormationingMining(var13);
      FleetMining var14 = new FleetMining(var13);
      var10.addTransition(Transition.STOP, var10);
      var10.addTransition(Transition.RESTART, var10);
      var10.addTransition(Transition.FLEET_FORMATION, var5);
      var10.addTransition(Transition.MOVE_TO_SECTOR, var3);
      var10.addTransition(Transition.SEARCH_FOR_TARGET, var2);
      var10.addTransition(Transition.HEALTH_LOW, var8);
      var10.addTransition(Transition.FLEET_BREAKING, var9);
      var10.addTransition(Transition.FLEET_GET_TO_MINING_POS, var12);
      var5.addTransition(Transition.FLEET_MOVE_TO_FLAGSHIP_SECTOR, var3);
      var5.addTransition(Transition.MOVE_TO_SECTOR, var3);
      var5.addTransition(Transition.RESTART, var10);
      var5.addTransition(Transition.SEARCH_FOR_TARGET, var2);
      var5.addTransition(Transition.FLEET_BREAKING, var9);
      var12.addTransition(Transition.FLEET_MOVE_TO_FLAGSHIP_SECTOR, var3);
      var12.addTransition(Transition.MOVE_TO_SECTOR, var3);
      var12.addTransition(Transition.RESTART, var10);
      var12.addTransition(Transition.SEARCH_FOR_TARGET, var2);
      var12.addTransition(Transition.FLEET_BREAKING, var9);
      var12.addTransition(Transition.FLEET_MINE, var14);
      var12.addTransition(Transition.FLEET_FORMATION, var5);
      var14.addTransition(Transition.FLEET_MOVE_TO_FLAGSHIP_SECTOR, var3);
      var14.addTransition(Transition.MOVE_TO_SECTOR, var3);
      var14.addTransition(Transition.RESTART, var10);
      var14.addTransition(Transition.FLEET_FORMATION, var5);
      var14.addTransition(Transition.SEARCH_FOR_TARGET, var2);
      var14.addTransition(Transition.FLEET_BREAKING, var9);
      var14.addTransition(Transition.FLEET_GET_TO_MINING_POS, var12);
      var2.addTransition(Transition.TARGET_AQUIRED, var6);
      var2.addTransition(Transition.STOP, var10);
      var2.addTransition(Transition.RESTART, var10);
      var2.addTransition(Transition.HEALTH_LOW, var8);
      var2.addTransition(Transition.MOVE_TO_SECTOR, var3);
      var2.addTransition(Transition.NO_TARGET_FOUND, var10);
      var2.addTransition(Transition.FLEET_FORMATION, var5);
      var2.addTransition(Transition.FLEET_BREAKING, var9);
      var3.addTransition(Transition.SEARCH_FOR_TARGET, var2);
      var3.addTransition(Transition.STOP, var10);
      var3.addTransition(Transition.RESTART, var10);
      var3.addTransition(Transition.HEALTH_LOW, var8);
      var3.addTransition(Transition.FLEET_FORMATION, var5);
      var3.addTransition(Transition.TARGET_SECTOR_REACHED, var10);
      var3.addTransition(Transition.FLEET_BREAKING, var9);
      var3.addTransition(Transition.FLEET_GET_TO_MINING_POS, var12);
      var6.addTransition(Transition.TARGET_IN_RANGE, var4);
      var6.addTransition(Transition.ENEMY_PROXIMITY, var7);
      var6.addTransition(Transition.IN_SHOOTING_POSITION, var11);
      var6.addTransition(Transition.STOP, var10);
      var6.addTransition(Transition.MOVE_TO_SECTOR, var3);
      var6.addTransition(Transition.RESTART, var10);
      var6.addTransition(Transition.HEALTH_LOW, var8);
      var6.addTransition(Transition.FLEET_FORMATION, var5);
      var6.addTransition(Transition.FLEET_BREAKING, var9);
      var7.addTransition(Transition.STOP, var10);
      var7.addTransition(Transition.RESTART, var6);
      var7.addTransition(Transition.HEALTH_LOW, var8);
      var7.addTransition(Transition.MOVE_TO_SECTOR, var3);
      var7.addTransition(Transition.IN_SHOOTING_POSITION, var11);
      var7.addTransition(Transition.FLEET_FORMATION, var5);
      var7.addTransition(Transition.FLEET_BREAKING, var9);
      var4.addTransition(Transition.TARGET_DESTROYED, var2);
      var4.addTransition(Transition.ENEMY_PROXIMITY, var7);
      var4.addTransition(Transition.IN_SHOOTING_POSITION, var11);
      var4.addTransition(Transition.TARGET_OUT_OF_RANGE, var6);
      var4.addTransition(Transition.MOVE_TO_SECTOR, var3);
      var4.addTransition(Transition.ENEMY_FIRE, var2);
      var4.addTransition(Transition.HEALTH_LOW, var8);
      var4.addTransition(Transition.STOP, var10);
      var4.addTransition(Transition.RESTART, var10);
      var4.addTransition(Transition.FLEET_FORMATION, var5);
      var4.addTransition(Transition.FLEET_BREAKING, var9);
      var11.addTransition(Transition.ENEMY_PROXIMITY, var7);
      var11.addTransition(Transition.ENEMY_FIRE, var2);
      var11.addTransition(Transition.SHOOTING_COMPLETED, var6);
      var11.addTransition(Transition.MOVE_TO_SECTOR, var3);
      var11.addTransition(Transition.HEALTH_LOW, var8);
      var11.addTransition(Transition.STOP, var10);
      var11.addTransition(Transition.RESTART, var10);
      var11.addTransition(Transition.FLEET_FORMATION, var5);
      var11.addTransition(Transition.FLEET_BREAKING, var9);
      var8.addTransition(Transition.STOP, var10);
      var8.addTransition(Transition.RESTART, var10);
      var8.addTransition(Transition.FLEET_FORMATION, var5);
      var8.addTransition(Transition.FLEET_BREAKING, var9);
      var9.addTransition(Transition.STOP, var10);
      var9.addTransition(Transition.RESTART, var10);
      var9.addTransition(Transition.FLEET_FORMATION, var5);
      var9.addTransition(Transition.MOVE_TO_SECTOR, var3);
      var9.addTransition(Transition.SEARCH_FOR_TARGET, var2);
      var9.addTransition(Transition.HEALTH_LOW, var8);
      var9.addTransition(Transition.FLEET_GET_TO_MINING_POS, var12);
      this.setStartingState(var10);
   }

   public void onMsg(Message var1) {
   }
}
