package org.schema.game.server.ai.program.fleetcontrollable.states;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.game.common.controller.SegmentBufferIteratorInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.server.ai.AIShipControllerStateUnit;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.common.states.ShipGameState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;

public class FleetMining extends ShipGameState implements SegmentBufferIteratorInterface {
   private Vector3f rotDir = new Vector3f();
   private Vector3f targetVelocity = new Vector3f();
   private Vector3f targetPosition = new Vector3f();
   private Vector3f fromPos = new Vector3f();
   private int targetid;
   private byte targetType;
   private Vector3f moveDir = new Vector3f();
   Vector3f posTmp = new Vector3f();
   private Vector3f currentTo = new Vector3f();
   boolean found = false;
   long lastSegPos = -1L;
   int lastBlockPos;
   private SegmentPiece currentTarget = new SegmentPiece();
   private boolean foundBlock;

   public FleetMining(AiEntityStateInterface var1) {
      super(var1);
   }

   public boolean onEnter() {
      this.targetVelocity.set(0.0F, 0.0F, 0.0F);
      this.targetPosition.set(0.0F, 0.0F, 0.0F);
      this.targetid = -1;
      this.moveDir.set(0.0F, 0.0F, 0.0F);
      this.rotDir.set(0.0F, 0.0F, 0.0F);
      this.fromPos.set(((Ship)this.getEntity()).getWorldTransform().origin);
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      SimpleGameObject var1;
      if ((var1 = ((TargetProgram)this.getEntityState().getCurrentProgram()).getTarget()) != null && var1 instanceof SegmentController && ((SegmentController)var1).getTotalElements() > 0) {
         if (!this.getEntityState().canSalvage()) {
            this.stateTransition(Transition.RESTART);
            return false;
         } else {
            boolean var2 = this.findTargetBlock((SegmentController)var1);
            this.targetVelocity.set(0.0F, 0.0F, 0.0F);
            this.targetPosition.set(0.0F, 0.0F, 0.0F);
            this.targetid = -1;
            this.moveDir.set(0.0F, 0.0F, 0.0F);
            this.rotDir.set(0.0F, 0.0F, 0.0F);
            Vector3f var3;
            (var3 = new Vector3f()).sub(this.fromPos, ((Ship)this.getEntity()).getWorldTransform().origin);
            if (var3.length() > this.getEntityState().getSalvageRange() - 3.0F) {
               this.stateTransition(Transition.RESTART);
               return false;
            } else {
               if (var2) {
                  if (this.currentTarget.getSegment() != null) {
                     this.currentTarget.refresh();
                     if (this.findRotDir((SegmentController)var1, this.currentTarget)) {
                        Vector3f var4;
                        GlUtil.getForwardVector(var4 = new Vector3f(), (Transform)((Ship)this.getEntity()).getWorldTransform());
                        (var3 = new Vector3f(this.rotDir)).normalize();
                        if (var3.epsilonEquals(var4, 0.23F)) {
                           if (((SegmentController)var1).getPhysicsObject() != null && !((SegmentController)var1).getPhysicsObject().isStaticOrKinematicObject()) {
                              ((SegmentController)var1).getPhysicsObject().getLinearVelocity(this.targetVelocity);
                           } else {
                              this.targetVelocity.set(0.0F, 0.0F, 0.0F);
                           }

                           this.targetPosition.set(this.currentTo);
                           this.targetid = var1.getAsTargetId();
                           this.targetType = 2;
                        }
                     } else {
                        if (this.rotDir.length() > 1200.0F) {
                           this.stateTransition(Transition.RESTART);
                           return false;
                        }

                        this.moveDir.set(this.rotDir);
                        this.rotDir.set(0.0F, 0.0F, 0.0F);
                     }
                  }
               } else if (!var2) {
                  this.stateTransition(Transition.RESTART);
               }

               return false;
            }
         }
      } else {
         System.err.println("[MINING] no target!");
         this.stateTransition(Transition.RESTART);
         return false;
      }
   }

   public boolean findRotDir(SegmentController var1, SegmentPiece var2) {
      if (var1 != null) {
         var1.calcWorldTransformRelative(((Ship)this.getEntity()).getSectorId(), ((GameServerState)((Ship)this.getEntity()).getState()).getUniverse().getSector(((Ship)this.getEntity()).getSectorId()).pos);
         var2.getAbsolutePos(this.posTmp);
         Vector3f var10000 = this.posTmp;
         var10000.x -= 16.0F;
         var10000 = this.posTmp;
         var10000.y -= 16.0F;
         var10000 = this.posTmp;
         var10000.z -= 16.0F;
         var1.getClientTransform().transform(this.posTmp);
         Vector3f var3 = ((Ship)this.getEntity()).getWorldTransform().origin;
         this.currentTo.set(this.posTmp);
         this.rotDir.sub(this.currentTo, var3);
         return this.checkTargetinRangeSalvage(var1, 0.0F);
      } else {
         this.rotDir.set(0.0F, 0.0F, 0.0F);
         return false;
      }
   }

   public boolean findTargetBlock(SegmentController var1) {
      this.found = false;
      this.foundBlock = false;
      var1.getSegmentBuffer().iterateOverNonEmptyElement(this, true);
      return this.foundBlock;
   }

   public boolean handle(Segment var1, long var2) {
      SegmentData var7;
      if ((var7 = var1.getSegmentData()) != null) {
         long var5;
         if ((var5 = ElementCollection.getIndex(var1.pos)) != this.lastSegPos) {
            this.lastBlockPos = 0;
            this.lastSegPos = var5;
         }

         for(int var3 = 0; var3 < 256 && !this.foundBlock; ++var3) {
            if (this.lastBlockPos < 32768) {
               if (ElementKeyMap.isValidType(var7.getType(this.lastBlockPos))) {
                  int var4;
                  byte var9 = (byte)((var4 = this.lastBlockPos) / 1024);
                  byte var6 = (byte)((var4 -= var9 << 10) / 32);
                  byte var8 = (byte)(var4 - (var6 << 5));
                  this.currentTarget.setByReference(var1, var8, var6, var9);
                  this.foundBlock = true;
               } else {
                  ++this.lastBlockPos;
               }
            } else {
               this.lastBlockPos = 0;
            }
         }

         this.found = true;
      }

      return false;
   }

   public void updateAI(AIShipControllerStateUnit var1, Timer var2, Ship var3, ShipAIEntity var4) throws FSMException {
      Vector3f var8 = new Vector3f();
      Vector3f var5 = new Vector3f();
      var8.set(this.targetPosition);
      var5.set(this.targetVelocity);
      int var6 = this.targetid;
      byte var7 = this.targetType;
      ((Ship)this.getEntity()).getNetworkObject().moveDir.set(this.moveDir);
      if (var8.length() > 0.0F) {
         ((Ship)this.getEntity()).getNetworkObject().targetPosition.set(var8);
         ((Ship)this.getEntity()).getNetworkObject().targetVelocity.set(var5);
         ((Ship)this.getEntity()).getNetworkObject().targetId.set(var6);
         ((Ship)this.getEntity()).getNetworkObject().targetType.set(var7);
         var4.doShooting(var1, var2);
         this.targetPosition.set(0.0F, 0.0F, 0.0F);
      }

      ((Ship)this.getEntity()).getNetworkObject().orientationDir.set(this.rotDir, 0.0F);
      if (this.rotDir.lengthSquared() > 0.0F) {
         var4.orientate(var2, Quat4fTools.getNewQuat(this.rotDir.x, this.rotDir.y, this.rotDir.z, 0.0F));
      }

   }
}
