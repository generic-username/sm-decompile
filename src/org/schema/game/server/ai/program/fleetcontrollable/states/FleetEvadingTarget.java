package org.schema.game.server.ai.program.fleetcontrollable.states;

import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.server.ai.AIShipControllerStateUnit;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.common.states.ShipGameState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.core.Timer;

public class FleetEvadingTarget extends ShipGameState {
   private final Vector3f movingDir = new Vector3f();
   private long start = 0L;

   public FleetEvadingTarget(AiEntityStateInterface var1) {
      super(var1);
   }

   public Vector3f getMovingDir() {
      return this.movingDir;
   }

   public boolean onEnter() {
      this.getMovingDir().set(0.0F, 0.0F, 0.0F);
      this.start = System.currentTimeMillis();
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      if (System.currentTimeMillis() - this.start > 30000L) {
         this.stateTransition(Transition.RESTART);
      }

      if (((Ship)this.getEntity()).getProximityObjects().size() > 0) {
         ((TargetProgram)this.getEntityState().getCurrentProgram()).setTarget((SimpleGameObject)null);
         Vector3f var1 = new Vector3f();
         Vector3f var2 = new Vector3f();
         boolean var3 = false;

         SegmentController var8;
         for(Iterator var4 = ((Ship)this.getEntity()).getProximityObjects().iterator(); var4.hasNext(); var3 = var3 || ((Ship)this.getEntity()).overlapsAABB(var8, var8.getClientTransform(), 30.0F)) {
            int var5 = (Integer)var4.next();
            var8 = (SegmentController)((Ship)this.getEntity()).getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var5);
            Vector3f var6 = ((Ship)this.getEntity()).getWorldTransform().origin;
            Vector3f var7 = var8.getWorldTransform().origin;
            var2.sub(var6, var7);
            var1.add(var2);
            var8.calcWorldTransformRelative(((Ship)this.getEntity()).getSectorId(), ((GameServerState)((Ship)this.getEntity()).getState()).getUniverse().getSector(((Ship)this.getEntity()).getSectorId()).pos);
         }

         var1.scale(10.0F);
         this.getMovingDir().set(var1);
         if (!var3) {
            this.stateTransition(Transition.RESTART);
         }
      } else {
         this.stateTransition(Transition.RESTART);
      }

      return false;
   }

   public void updateAI(AIShipControllerStateUnit var1, Timer var2, Ship var3, ShipAIEntity var4) throws FSMException {
      ((Ship)this.getEntity()).getNetworkObject().orientationDir.set(0.0F, 0.0F, 0.0F, 0.0F);
      ((Ship)this.getEntity()).getNetworkObject().targetPosition.set(new Vector3f(0.0F, 0.0F, 0.0F));
      Vector3f var5;
      (var5 = new Vector3f()).set(this.getMovingDir());
      ((Ship)this.getEntity()).getNetworkObject().moveDir.set(var5);
      var4.moveTo(var2, var5, false);
      if ((var5 = new Vector3f(var5)).lengthSquared() > 0.0F) {
         var5.normalize();
         var5.negate();
         ((Ship)this.getEntity()).getNetworkObject().orientationDir.set(var5, 0.0F);

         assert !Float.isNaN(var5.x) : var5;

         var4.orientate(var2, Quat4fTools.getNewQuat(var5.x, var5.y, var5.z, 0.0F));
      }

   }
}
