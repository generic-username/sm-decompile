package org.schema.game.server.ai.program.common.states;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ai.AIGameConfiguration;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.missile.Missile;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.AIGameEntityState;
import org.schema.schine.ai.stateMachines.AiInterface;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.network.objects.Sendable;

public abstract class GameState extends State {
   protected static Transform serverTmp = new Transform();
   Vector3f dist = new Vector3f();

   public GameState(AiEntityStateInterface var1) {
      super(var1);
   }

   public static boolean isDockedOn(SegmentController var0, SegmentController var1) {
      return var0.getDockingController().getDockedOn() != null && var0.getDockingController().getDockedOn().to.getSegment().getSegmentController() == var1;
   }

   public static boolean checkTarget(SimpleGameObject var0, ShipGameState var1) {
      Ship var2 = (Ship)var1.getEntity();
      Sector var3;
      if ((var3 = ((GameServerState)((Ship)var1.getEntity()).getState()).getUniverse().getSector(var0.getSectorId())) == null || !var3.isProtected() && !var3.isPeace()) {
         boolean var6 = var0 instanceof AiInterface && ((AiInterface)var0).getAiConfiguration().getAiEntityState().isActive();
         boolean var4 = var1.getAIConfig().get(Types.AIM_AT).getCurrentState().equals("Selected Target");
         boolean var5;
         if ((var5 = var0 instanceof PlayerControllable && !((PlayerControllable)var0).getAttachedPlayers().isEmpty()) && ((PlayerState)((PlayerControllable)var0).getAttachedPlayers().get(0)).isInvisibilityMode()) {
            if (GameClientState.isDebugObject(var2)) {
               System.err.println("[AI][DEBUG][CHECK] " + var1.getEntity() + " dismissed " + var0 + " as target: invisible");
            }

            return false;
         } else if (!var4 && !var5 && !var6 && !(var0 instanceof Missile) && var0.getFactionId() != -1) {
            if (GameClientState.isDebugObject(var2)) {
               System.err.println("[AI][DEBUG][CHECK] " + var1.getEntity() + " TARGET FAILED BECAUSE OF lack of attached players or not active AI " + var1.getEntity() + "; " + var0);
            }

            return false;
         } else {
            AbstractOwnerState var7;
            if (var4 && (var7 = ((Ship)var1.getEntity()).railController.getRoot().getOwnerState()) != null && var7 instanceof PlayerState) {
               int var8 = ((PlayerState)var7).getNetworkObject().selectedAITargetId.get();
               if (var0.getAsTargetId() != var8) {
                  if (GameClientState.isDebugObject(var2)) {
                     System.err.println("[AI][DEBUG][CHECK] " + var1.getEntity() + " TARGET FAILED BECAUSE 'SELECTED TARGET' MODE AND SELECTED IS NOT THE TARGET. " + var1.getEntity() + "; " + var0 + "; " + var0.getAsTargetId() + "; " + var8);
                  }

                  return false;
               }

               Sendable var9;
               if ((var9 = (Sendable)((Ship)var1.getEntity()).getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var8)) == null || !(var9 instanceof SimpleTransformableSendableObject)) {
                  if (GameClientState.isDebugObject(var2)) {
                     System.err.println("[AI][DEBUG][CHECK] " + var1.getEntity() + " TARGET FAILED BECAUSE DESELECTION WITH 'SELECTED TARGET' MODE. " + var1.getEntity() + "; " + var0);
                  }

                  return false;
               }
            }

            if (!(var0 instanceof SegmentController) || !isDockedOn((SegmentController)var0, (SegmentController)var1.getEntity()) && !isDockedOn(var2, (SegmentController)var0)) {
               if (var0 instanceof Ship && ((Ship)var0).isCloakedFor(var2)) {
                  if (GameClientState.isDebugObject(var2)) {
                     System.err.println("[AI][DEBUG][CHECK] " + var1.getEntity() + " TARGET FAILED BECAUSE OF CLOACKED " + var1.getEntity() + "; " + var0);
                  }

                  return false;
               } else if (var0 instanceof Ship && ((Ship)var0).isCoreOverheating()) {
                  if (GameClientState.isDebugObject(var2)) {
                     System.err.println("[AI][DEBUG][CHECK] " + var1.getEntity() + " TARGET FAILED BECAUSE OF CORE OVERHEATING " + var1.getEntity() + "; " + var0);
                  }

                  return false;
               } else if (var0 instanceof AbstractCharacter && ((AbstractCharacter)var0).isHidden()) {
                  if (GameClientState.isDebugObject(var2)) {
                     System.err.println("[AI][DEBUG][CHECK] " + var1.getEntity() + " TARGET FAILED BECAUSE OF HIDDEN CHAR " + var1.getEntity() + "; " + var0);
                  }

                  return false;
               } else if (!var4 && var0 instanceof PlayerControllable && !(var0 instanceof AbstractCharacter) && ((PlayerControllable)var0).getAttachedPlayers().isEmpty() && !((AiInterface)var0).getAiConfiguration().isActiveAI() && var0.getFactionId() != -1) {
                  System.err.println("[AI][TURRET] Dead Entity. Getting new Target");
                  return false;
               } else {
                  var0.calcWorldTransformRelative(var2.getSectorId(), ((GameServerState)var2.getState()).getUniverse().getSector(var2.getSectorId()).pos);
                  var1.dist.sub(var0.getClientTransform().origin, ((SimpleTransformableSendableObject)var1.getEntityState().getEntity()).getWorldTransform().origin);
                  if (var0 instanceof Ship && ((Ship)var0).isJammingFor(var2) && var1.dist.length() > var1.getEntityState().getShootingRange() - 100.0F) {
                     if (GameClientState.isDebugObject(var2)) {
                        System.err.println("[AI][DEBUG][CHECK] " + var1.getEntity() + " TARGET FAILED BECAUSE OF TOO FAR AND JAMMING " + var1.getEntity() + "; " + var0 + "; " + var1.dist + "/" + var1.getEntityState().getShootingRange() / 2.0F);
                     }

                     return false;
                  } else if (var1.dist.length() > var1.getEntityState().getShootingRange()) {
                     if (GameClientState.isDebugObject(var2)) {
                        System.err.println("[AI][DEBUG][CHECK] " + var1.getEntity() + " dismissed " + var0 + " as target: out of range: dist: " + var1.dist.length() + " / range: " + var1.getEntityState().getShootingRange());
                     }

                     return false;
                  } else {
                     return true;
                  }
               }
            } else {
               if (GameClientState.isDebugObject(var2)) {
                  System.err.println("[AI][DEBUG][CHECK] " + var1.getEntity() + " TARGET FAILED BECAUSE OF DOCKED " + var1.getEntity() + "; " + var0);
               }

               return false;
            }
         }
      } else {
         if (GameClientState.isDebugObject(var2)) {
            System.err.println("[AI][DEBUG][CHECK] " + var1.getEntity() + " dismissed " + var0 + " as target: sector protection or peace");
         }

         return false;
      }
   }

   public AIGameConfiguration getAIConfig() {
      return (AIGameConfiguration)((AiInterface)this.getEntityState().getEntity()).getAiConfiguration();
   }

   public Sendable getEntity() {
      return ((AIGameEntityState)super.getEntityState()).getEntity();
   }

   public AIGameEntityState getEntityState() {
      return (AIGameEntityState)super.getEntityState();
   }
}
