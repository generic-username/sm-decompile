package org.schema.game.server.ai.program.common;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.world.Universe;
import org.schema.schine.ai.MachineProgram;
import org.schema.schine.ai.stateMachines.AiEntityState;

public abstract class TargetProgram extends MachineProgram {
   private Vector3i sectorTarget;
   private SimpleGameObject target;
   private int specificTargetId = -1;
   private long targetAquiredTime;
   private int targetHoldTime;

   public TargetProgram(AiEntityState var1, boolean var2) {
      super(var1, var2);
   }

   public Vector3i getSectorTarget() {
      return this.sectorTarget;
   }

   public void setSectorTarget(Vector3i var1) {
      this.sectorTarget = var1;
   }

   public int getSpecificTargetId() {
      return this.specificTargetId;
   }

   public void setSpecificTargetId(int var1) {
      this.specificTargetId = var1;
   }

   public SimpleGameObject getTarget() {
      return this.target;
   }

   public void setTarget(SimpleGameObject var1) {
      if (this.target != var1) {
         this.targetAquiredTime = System.currentTimeMillis();
         this.targetHoldTime = Universe.getRandom().nextInt(3000);
      }

      this.target = var1;
   }

   public long getTargetAquiredTime() {
      return this.targetAquiredTime;
   }

   public int getTargetHoldTime() {
      return this.targetHoldTime;
   }
}
