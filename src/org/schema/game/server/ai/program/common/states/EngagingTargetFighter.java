package org.schema.game.server.ai.program.common.states;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.server.ai.AIShipControllerStateUnit;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.turret.states.ShootingProcessInterface;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;

public class EngagingTargetFighter extends ShipGameState implements ShootingProcessInterface {
   private final Vector3f rotDir = new Vector3f();

   public EngagingTargetFighter(AiEntityStateInterface var1) {
      super(var1);
   }

   public Vector3f getRotDir() {
      return this.rotDir;
   }

   public boolean onEnter() {
      SimpleGameObject var1 = ((TargetProgram)this.getEntityState().getCurrentProgram()).getTarget();
      this.findRotDir(var1);
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean findRotDir(SimpleGameObject var1) {
      if (var1 != null) {
         var1.calcWorldTransformRelative(((Ship)this.getEntity()).getSectorId(), ((GameServerState)((Ship)this.getEntity()).getState()).getUniverse().getSector(((Ship)this.getEntity()).getSectorId()).pos);
         if (!this.checkTargetinRange(var1, 0.0F, false)) {
            this.getRotDir().set(0.0F, 0.0F, 0.0F);
            return false;
         } else {
            Vector3f var2 = ((Ship)this.getEntity()).getWorldTransform().origin;
            Vector3f var3 = new Vector3f(var1.getClientTransformCenterOfMass(serverTmp).origin);
            this.getRotDir().sub(var3, var2);
            return true;
         }
      } else {
         this.getRotDir().set(0.0F, 0.0F, 0.0F);
         return false;
      }
   }

   public boolean onUpdate() throws FSMException {
      if (this.getEntityState().isTimeout()) {
         System.err.println("[AI] TIMEOUT ENGAGING (too far from set sector) for " + this.getEntity());
         this.stateTransition(Transition.RESTART);
         return false;
      } else {
         SimpleGameObject var1 = ((TargetProgram)this.getEntityState().getCurrentProgram()).getTarget();
         if (((Ship)this.getEntity()).isCoreOverheating()) {
            this.stateTransition(Transition.RESTART);
            return false;
         } else {
            if (this.findRotDir(var1)) {
               assert !Float.isNaN(this.getRotDir().x);

               Vector3f var4;
               (var4 = new Vector3f(this.getRotDir())).normalize();
               Vector3f var2 = GlUtil.getForwardVector(new Vector3f(), (Transform)((Ship)this.getEntity()).getWorldTransform());
               Vector3f var3;
               (var3 = new Vector3f(var2)).negate();
               var3.sub(var4);
               (new Vector3f(var2)).sub(var4);
               if (var4.epsilonEquals(var2, 0.23F)) {
                  this.stateTransition(Transition.IN_SHOOTING_POSITION);
                  return true;
               }
            } else {
               this.stateTransition(Transition.RESTART);
            }

            assert !Float.isNaN(this.getRotDir().x);

            return false;
         }
      }
   }

   public void updateAI(AIShipControllerStateUnit var1, Timer var2, Ship var3, ShipAIEntity var4) throws FSMException {
      ((Ship)this.getEntity()).getNetworkObject().targetPosition.set(new Vector3f(0.0F, 0.0F, 0.0F));
      Vector3f var5;
      Vector3f var10000 = var5 = GlUtil.getUpVector(new Vector3f(), ((Ship)this.getEntity()).getWorldTransform().basis);
      var10000.cross(var10000, this.getRotDir());
      if (var5.lengthSquared() > 0.0F) {
         var5.normalize();
         var5.scale(((ShipAIEntity)this.getEntityState()).getForceMoveWhileShooting());
         if ((double)this.getRotDir().length() > (double)this.getEntityState().getShootingRange() * 0.75D) {
            Vector3f var6;
            (var6 = new Vector3f(this.getRotDir())).normalize();
            var6.scale(10.0F);
            var5.add(var6);
         }

         ((Ship)this.getEntity()).getNetworkObject().moveDir.set(var5);
         var4.moveTo(var2, var5, false);
      } else {
         ((Ship)this.getEntity()).getNetworkObject().moveDir.set(0.0F, 0.0F, 0.0F);
      }

      var4.moveTo(var2, var5, false);

      assert !Float.isNaN(this.getRotDir().x);

      var4.orientate(var2, Quat4fTools.getNewQuat(this.getRotDir().x, this.getRotDir().y, this.getRotDir().z, 0.0F));
      ((Ship)this.getEntity()).getNetworkObject().orientationDir.set(this.getRotDir().x, this.getRotDir().y, this.getRotDir().z, 0.0F);
   }
}
