package org.schema.game.server.ai.program.common.states;

import org.schema.game.server.ai.program.simpirates.states.SimulationGroupState;
import org.schema.game.server.data.simulation.groups.SimulationGroup;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class WaitingTimed extends SimulationGroupState {
   private long startTime;
   private int seconds;

   public WaitingTimed(AiEntityStateInterface var1, int var2) {
      super(var1);
      this.seconds = var2;
   }

   public boolean onEnter() {
      this.startTime = System.currentTimeMillis();
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      if (this.getEntityState() instanceof SimulationGroup) {
         ((SimulationGroup)this.getEntityState()).onWait();
      }

      if (System.currentTimeMillis() - this.startTime > (long)(this.seconds * 1000)) {
         this.stateTransition(Transition.WAIT_COMPLETED);
      }

      return false;
   }
}
