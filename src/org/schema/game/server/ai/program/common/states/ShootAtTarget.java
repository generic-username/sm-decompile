package org.schema.game.server.ai.program.common.states;

import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ai.SegmentControllerAIInterface;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.ai.AIShipControllerStateUnit;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.ShootingStateInterface;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.turret.states.ShootingProcessInterface;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;

public abstract class ShootAtTarget extends ShipGameState implements ShootingStateInterface, ShootingProcessInterface {
   private final Vector3f targetPosition = new Vector3f();
   private final Vector3f targetVelocity = new Vector3f();
   private Vector3f dist = new Vector3f();
   private int targetid;
   private byte targetType;
   private Vector3f tmp = new Vector3f();
   protected final Vector3f fromTo = new Vector3f();

   public ShootAtTarget(AiEntityStateInterface var1) {
      super(var1);
   }

   public int getTargetId() {
      return this.targetid;
   }

   public byte getTargetType() {
      return this.targetType;
   }

   public void updateAI(AIShipControllerStateUnit var1, Timer var2, Ship var3, ShipAIEntity var4) throws FSMException {
      Vector3f var9 = new Vector3f();
      Vector3f var5 = new Vector3f();
      var9.set(this.getTargetPosition());
      var5.set(this.getTargetVelocity());
      int var6 = this.getTargetId();
      byte var7 = this.getTargetType();
      if (var9.length() > 0.0F) {
         ((Ship)this.getEntity()).getNetworkObject().targetPosition.set(var9);
         ((Ship)this.getEntity()).getNetworkObject().targetVelocity.set(var5);
         ((Ship)this.getEntity()).getNetworkObject().targetId.set(var6);
         ((Ship)this.getEntity()).getNetworkObject().targetType.set(var7);
         var4.doShooting(var1, var2);
         this.getEntityState().onShot(this);
         this.getTargetPosition().set(0.0F, 0.0F, 0.0F);
         this.stateTransition(Transition.SHOOTING_COMPLETED);
      }

      Vector3f var8;
      (var8 = new Vector3f()).set(this.fromTo);
      ((Ship)this.getEntity()).getNetworkObject().orientationDir.set(var8, 0.0F);
      Vector3f var10000 = var9 = GlUtil.getUpVector(new Vector3f(), ((Ship)this.getEntity()).getWorldTransform().basis);
      var10000.cross(var10000, var8);
      if (var9.lengthSquared() > 0.0F) {
         var9.normalize();
         var9.scale(((ShipAIEntity)this.getEntityState()).getForceMoveWhileShooting());
         if ((double)var8.length() > (double)this.getEntityState().getShootingRange() * 0.75D) {
            (var5 = new Vector3f(var8)).normalize();
            var5.scale(10.0F);
            var9.add(var5);
         }

         ((Ship)this.getEntity()).getNetworkObject().moveDir.set(var9);
         var4.moveTo(var2, var9, false);
      } else {
         ((Ship)this.getEntity()).getNetworkObject().moveDir.set(0.0F, 0.0F, 0.0F);
      }

      ((Ship)this.getEntity()).getNetworkObject().orientationDir.set(var8.x, var8.y, var8.z, 0.0F);
      var4.moveTo(var2, var9, false);

      assert !Float.isNaN(this.fromTo.x);

      assert !Float.isNaN(var8.x);

      var4.orientate(var2, Quat4fTools.getNewQuat(var8.x, var8.y, var8.z, 0.0F));
   }

   public Vector3f getTargetPosition() {
      return this.targetPosition;
   }

   public Vector3f getTargetVelocity() {
      return this.targetVelocity;
   }

   public boolean onEnter() {
      this.getTargetPosition().set(0.0F, 0.0F, 0.0F);

      try {
         this.onUpdate();
      } catch (FSMException var1) {
         var1.printStackTrace();
      }

      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      SimpleGameObject var1 = ((TargetProgram)this.getEntityState().getCurrentProgram()).getTarget();
      if (((Ship)this.getEntity()).isCoreOverheating()) {
         this.stateTransition(Transition.RESTART);
         this.getTargetPosition().set(0.0F, 0.0F, 0.0F);
         return false;
      } else {
         if (var1 != null) {
            var1.calcWorldTransformRelative(((Ship)this.getEntity()).getSectorId(), ((GameServerState)((Ship)this.getEntity()).getState()).getUniverse().getSector(((Ship)this.getEntity()).getSectorId()).pos);
            if (var1 == this.getEntity() || !this.checkTargetinRange(var1, 0.0F, false)) {
               this.stateTransition(Transition.RESTART);
               return false;
            }

            Vector3f var2 = var1.getLinearVelocity(this.tmp);
            Vector3f var3 = new Vector3f(var1.getClientTransformCenterOfMass(serverTmp).origin);
            this.fromTo.sub(var3, ((Ship)this.getEntity()).getWorldTransform().origin);
            AiEntityStateInterface var4 = ((SegmentControllerAIInterface)this.getEntity()).getAiConfiguration().getAiEntityState();
            float var5 = 10.0F;
            if (var4 instanceof ShipAIEntity) {
               var5 = ((ShipAIEntity)var4).getShootingDifficulty(var1);
            }

            if (var1 instanceof Ship && ((Ship)var1).isJammingFor((SimpleTransformableSendableObject)this.getEntity())) {
               var5 = Math.max(1.0F, var5 * 0.1F);
            }

            float var6 = this.fromTo.length() / var5;
            this.getEntityState().random.setSeed(this.getEntityState().seed);
            var1.transformAimingAt(var3, (Damager)this.getEntityState().getEntity(), var1, this.getEntityState().random, var6);
            this.getTargetPosition().set(var3);
            this.getTargetVelocity().set(var2);
            this.targetid = var1.getAsTargetId();
            this.targetType = var1.getTargetType();
         }

         return false;
      }
   }
}
