package org.schema.game.server.ai.program.common.states;

import org.schema.schine.ai.AiEntityStateInterface;

public abstract class SegmentControllerGameState extends GameState {
   public SegmentControllerGameState(AiEntityStateInterface var1) {
      super(var1);
   }
}
