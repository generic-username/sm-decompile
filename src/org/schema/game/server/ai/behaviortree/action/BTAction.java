package org.schema.game.server.ai.behaviortree.action;

import org.schema.game.server.ai.behaviortree.BTNode;

public abstract class BTAction {
   private BTAction.State state;

   public BTAction() {
      this.state = BTAction.State.STOP;
   }

   public abstract void onStart(BTNode var1);

   public abstract BTAction.State onUpdate(BTNode var1);

   public abstract void onEnd(BTNode var1);

   public abstract void resetAction(BTNode var1);

   public void update(BTNode var1) {
      assert this.state != BTAction.State.STOP;

      switch(this.state) {
      case START:
         this.onStart(var1);
         this.state = BTAction.State.RUNNING;
         return;
      case RUNNING:
         this.state = this.onUpdate(var1);

         assert this.state != BTAction.State.START;

         assert this.state != BTAction.State.STOP;

         return;
      case ENDED:
         this.onEnd(var1);
         this.resetAction(var1);
         this.state = BTAction.State.STOP;
         return;
      default:
         throw new IllegalArgumentException();
      }
   }

   public static enum State {
      STOP,
      START,
      RUNNING,
      ENDED;
   }
}
