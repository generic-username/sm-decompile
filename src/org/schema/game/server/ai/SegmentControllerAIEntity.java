package org.schema.game.server.ai;

import java.util.Iterator;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.ai.AIGameConfiguration;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.fleet.missions.machines.states.Timeout;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.ai.program.common.states.SegmentControllerGameState;
import org.schema.schine.ai.stateMachines.AIGameEntityState;

public abstract class SegmentControllerAIEntity extends AIGameEntityState {
   public Timeout engagingTimeoutQueued;
   private Timeout engagingTimeoutEngaged;

   public SegmentControllerAIEntity(String var1, SegmentController var2) {
      super(var1, var2);
   }

   public boolean isActive() {
      return super.isActive() && !((SegmentController)this.getEntity()).isVirtualBlueprint();
   }

   public SegmentControllerAIEntity.ManualUsable getManualPlayerUsable() {
      if (((AIGameConfiguration)this.getAIConfig()).get(Types.MANUAL).isOn() && ((SegmentController)this.getEntity()).railController.isDockedAndExecuted()) {
         SegmentController var1;
         ManagerContainer var2 = ((ManagedSegmentController)(var1 = ((SegmentController)this.getEntity()).railController.getRoot())).getManagerContainer();
         if (var1 instanceof PlayerControllable && !((PlayerControllable)var1).getAttachedPlayers().isEmpty()) {
            Iterator var3 = ((PlayerState)((PlayerControllable)var1).getAttachedPlayers().get(0)).getControllerState().getUnits().iterator();

            while(var3.hasNext()) {
               ControllerStateUnit var4 = (ControllerStateUnit)var3.next();
               PlayerUsableInterface var5;
               if ((var5 = var2.getPlayerUsable(-9223372036854775779L)) != null && var4.isSelected(var5, ((ManagedSegmentController)var1).getManagerContainer()) && var4.playerControllable == var1) {
                  return new SegmentControllerAIEntity.ManualUsable(var5, var4);
               }
            }
         }
      }

      return null;
   }

   public boolean isTimeout() {
      if (this.engagingTimeoutEngaged != null) {
         Vector3i var1 = ((SegmentController)this.getEntity()).getSector(new Vector3i());
         return this.engagingTimeoutEngaged.isTimeout(var1);
      } else {
         return false;
      }
   }

   public void engageTimeout() {
      this.engagingTimeoutEngaged = this.engagingTimeoutQueued;
      this.engagingTimeoutQueued = null;
   }

   public boolean hadTimeout() {
      return this.engagingTimeoutEngaged != null && this.engagingTimeoutEngaged.wasTimedOut;
   }

   public void onShot(SegmentControllerGameState var1) {
      if (this.engagingTimeoutEngaged != null) {
         this.engagingTimeoutEngaged.onShot(var1);
      }

   }

   public void onTimeout(SegmentControllerGameState var1) {
      if (this.engagingTimeoutEngaged != null) {
         this.engagingTimeoutEngaged.onTimeout(var1);
      }

   }

   public void onNoTargetFound(SegmentControllerGameState var1) {
      if (this.engagingTimeoutEngaged != null) {
         this.engagingTimeoutEngaged.onNoTargetFound(var1);
      }

   }

   public static class ManualUsable {
      public final PlayerUsableInterface p;
      public final ControllerStateUnit u;

      public ManualUsable(PlayerUsableInterface var1, ControllerStateUnit var2) {
         this.p = var1;
         this.u = var2;
      }
   }
}
