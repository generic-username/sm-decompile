package org.schema.game.server.ai;

import org.schema.game.common.controller.TeamDeathStar;
import org.schema.schine.ai.MachineProgram;
import org.schema.schine.graphicsengine.core.Timer;

public class DeathStarAIEntity extends SegmentControllerAIEntity {
   public DeathStarAIEntity(String var1, MachineProgram var2, TeamDeathStar var3) {
      super(var1, var3);
   }

   public void updateAIClient(Timer var1) {
   }

   public void updateAIServer(Timer var1) {
   }
}
