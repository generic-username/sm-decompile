package org.schema.game.server.ai;

import org.schema.game.common.controller.Vehicle;
import org.schema.schine.ai.MachineProgram;
import org.schema.schine.graphicsengine.core.Timer;

public class VehicleAIEntity extends SegmentControllerAIEntity {
   public VehicleAIEntity(String var1, MachineProgram var2, Vehicle var3) {
      super(var1, var3);
   }

   public void updateAIClient(Timer var1) {
   }

   public void updateAIServer(Timer var1) {
   }
}
