package org.schema.game.server.data;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.Short2IntArrayMap;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.EntityIndexScore;
import org.schema.game.common.data.element.ControlElementMap;
import org.schema.game.common.data.element.ControlElementMapper;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.server.data.blueprintnw.BlueprintType;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.resource.tag.Tag;

public class SegmentControllerBluePrintEntryOld implements BlueprintInterface {
   public static final int PRIVACY_SHARED = 0;
   public static final int PRIVACY_PUBLIC = 0;
   public static final String MIGRATION_V0061_TO_V0062 = "v0.061 to v0.062";
   public static final String MIGRATION_V0078_TO_V0079 = "v0.078 to v0.079";
   public static final String MIGRATION_V00897_TO_V00898 = "v0.0897 to v0.0898";
   private static int currentVersion = 6;
   public BoundingBox bb;
   public Vector3i minPos;
   public Vector3i maxPos;
   public String name;
   public int entityType = -1;
   public ControlElementMapper controllingMap;
   public long price;
   public Short2IntArrayMap elementMap;
   public boolean buyable = true;
   public boolean useInWaves;
   public String ownerId;
   public int privacy;
   public int version;
   public ArrayList needsMigration;
   private ElementCountMap elementCountMap;

   public SegmentControllerBluePrintEntryOld(BoundingBox var1, String var2) {
      this.useInWaves = ServerConfig.DEFAULT_BLUEPRINT_ENEMY_USE.isOn();
      this.ownerId = "";
      this.privacy = 0;
      this.needsMigration = new ArrayList();
      this.bb = var1;
      this.name = var2;
   }

   public SegmentControllerBluePrintEntryOld(DataInputStream var1) throws IOException {
      this.useInWaves = ServerConfig.DEFAULT_BLUEPRINT_ENEMY_USE.isOn();
      this.ownerId = "";
      this.privacy = 0;
      this.needsMigration = new ArrayList();
      String var2;
      if (!(var2 = var1.readUTF()).contains(":")) {
         this.name = var2;
         this.version = 0;
      } else {
         String[] var6 = var2.split(":");
         this.version = Integer.parseInt(var6[1]);
         this.name = var6[0];
      }

      if (this.version < 3) {
         this.needsMigration.add("v0.061 to v0.062");
      }

      if (this.version < 4) {
         this.needsMigration.add("v0.078 to v0.079");
      }

      if (this.version < 6) {
         this.needsMigration.add("v0.0897 to v0.0898");
      }

      if (this.version > 4) {
         this.buyable = var1.readBoolean();
         this.useInWaves = var1.readBoolean();
         this.ownerId = var1.readUTF();
         this.privacy = var1.readInt();
         this.entityType = var1.readInt();
      } else if (this.version > 1) {
         this.buyable = var1.readBoolean();
         this.useInWaves = var1.readBoolean();
         this.ownerId = var1.readUTF();
         this.privacy = var1.readInt();
         this.entityType = BlueprintType.SHIP.ordinal();
      } else if (this.version > 0) {
         this.buyable = var1.readBoolean();
         this.useInWaves = var1.readBoolean();
         this.ownerId = "";
         this.entityType = BlueprintType.SHIP.ordinal();
      } else {
         this.buyable = true;
         this.useInWaves = true;
         this.ownerId = "";
         this.entityType = BlueprintType.SHIP.ordinal();
      }

      this.bb = new BoundingBox(new Vector3f(var1.readFloat(), var1.readFloat(), var1.readFloat()), new Vector3f(var1.readFloat(), var1.readFloat(), var1.readFloat()));
      int var7 = var1.readInt();
      this.price = 0L;
      this.elementMap = new Short2IntArrayMap();

      for(int var3 = 0; var3 < var7; ++var3) {
         short var4 = (short)Math.abs(var1.readShort());
         int var5 = var1.readInt();
         if (ElementKeyMap.exists(var4)) {
            if (this.elementMap.get(var4) == 0) {
               this.elementMap.put(var4, 0);
            } else {
               this.elementMap.put(var4, this.elementMap.get(var4) + var5);
               this.price += ElementKeyMap.getInfo(var4).getPrice(false) * (long)var5;
            }
         }
      }

      this.elementCountMap = new ElementCountMap();
      this.elementCountMap.load(this.elementMap);
      Tag var8 = Tag.readFrom(var1, false, false);
      this.controllingMap = new ControlElementMapper();
      this.controllingMap = ControlElementMap.mapFromTag(var8, this.controllingMap, true);
   }

   public SegmentControllerBluePrintEntryOld(String var1) {
      this.useInWaves = ServerConfig.DEFAULT_BLUEPRINT_ENEMY_USE.isOn();
      this.ownerId = "";
      this.privacy = 0;
      this.needsMigration = new ArrayList();
      this.name = var1;
   }

   public void calculatePrice(ElementCountMap var1) {
      try {
         this.price = var1.getPrice();
      } catch (Exception var2) {
         var2.printStackTrace();
      }
   }

   public ControlElementMapper getControllingMap() {
      return this.controllingMap;
   }

   public ElementCountMap getElementMap() {
      return this.elementCountMap;
   }

   public ElementCountMap getElementCountMapWithChilds() {
      return this.getElementMap();
   }

   public String getName() {
      return this.name;
   }

   public long getPrice() {
      return this.price;
   }

   public EntityIndexScore getScore() {
      return null;
   }

   public BlueprintType getType() {
      return BlueprintType.SHIP;
   }

   public Tag getAiTag() {
      return null;
   }

   public int hashCode() {
      return this.name.hashCode();
   }

   public boolean equals(Object var1) {
      return ((SegmentControllerBluePrintEntryOld)var1).name.equals(this.name);
   }

   public String toString() {
      return this.name;
   }

   public void write(DataOutputStream var1, SegmentController var2) throws IOException, CannotWriteExeption {
      ByteArrayOutputStream var3 = new ByteArrayOutputStream(4096);
      DataOutputStream var4 = new DataOutputStream(var3);
      if (this.bb == null) {
         this.bb = new BoundingBox();
         this.bb.min.set(0.0F, 0.0F, 0.0F);
         this.bb.max.set(0.0F, 0.0F, 0.0F);
      }

      var4.writeUTF(this.name + ":" + currentVersion);
      var4.writeBoolean(this.buyable);
      var4.writeBoolean(this.useInWaves);
      var4.writeUTF(this.ownerId);
      var4.writeInt(this.privacy);
      if (var2 != null) {
         this.entityType = BlueprintType.getType(var2.getClass()).ordinal();
      }

      var4.writeInt(this.entityType);
      var4.writeFloat(this.bb.min.x);
      var4.writeFloat(this.bb.min.y);
      var4.writeFloat(this.bb.min.z);
      var4.writeFloat(this.bb.max.x);
      var4.writeFloat(this.bb.max.y);
      var4.writeFloat(this.bb.max.z);
      if (var2 != null) {
         var4.writeInt(ElementKeyMap.typeList().length);
         short[] var5;
         int var6 = (var5 = ElementKeyMap.typeList()).length;

         for(int var7 = 0; var7 < var6; ++var7) {
            Short var8 = var5[var7];
            var4.writeShort(var8);
            var4.writeInt(var2.getElementClassCountMap().get(var8));
         }

         var2.getControlElementMap().toTagStructure().writeTo(var4, false);
      } else {
         if (this.elementMap == null) {
            throw new CannotWriteExeption();
         }

         assert this.elementMap != null;

         var4.writeInt(this.elementMap.size());
         Iterator var9 = this.elementMap.entrySet().iterator();

         while(var9.hasNext()) {
            Entry var10 = (Entry)var9.next();
            var4.writeShort((Short)var10.getKey());
            var4.writeInt((Integer)var10.getValue());
         }

         assert this.controllingMap != null;

         ControlElementMap.mapToTag(this.controllingMap).writeTo(var4, true);
      }

      var1.writeInt(var3.size());
      var3.writeTo(var1);
      var4.close();
   }

   public boolean isChunk16() {
      return true;
   }

   public double getCapacitySingle() {
      return 0.0D;
   }

   public BoundingBox getBb() {
      return null;
   }

   public boolean isOldPowerFlag() {
      return true;
   }

   public Long2ObjectOpenHashMap getDockerPoints() {
      return null;
   }
}
