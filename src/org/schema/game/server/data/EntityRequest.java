package org.schema.game.server.data;

import java.io.IOException;
import java.util.Locale;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.FloatingRock;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.Vehicle;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.SegmentDataIntArray;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.controller.EntityAlreadyExistsException;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.resource.FileExt;

public class EntityRequest {
   private final Class entityClass;
   private final int clientId;
   private Object[] data;

   public EntityRequest(int var1, Object[] var2, Class var3) {
      this.clientId = var1;
      this.data = var2;
      this.entityClass = var3;
   }

   public static String convertAsteroidEntityName(String var0, boolean var1) {
      return (var1 ? SimpleTransformableSendableObject.EntityType.ASTEROID_MANAGED.dbPrefix : SimpleTransformableSendableObject.EntityType.ASTEROID.dbPrefix) + var0;
   }

   public static String convertPlanetEntityName(String var0) {
      return SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT.dbPrefix + var0;
   }

   public static String convertShipEntityName(String var0) {
      return SimpleTransformableSendableObject.EntityType.SHIP.dbPrefix + var0;
   }

   public static String convertShopEntityName(String var0) {
      return SimpleTransformableSendableObject.EntityType.SHOP.dbPrefix + var0;
   }

   public static String convertStationEntityName(String var0) {
      return SimpleTransformableSendableObject.EntityType.SPACE_STATION.dbPrefix + var0;
   }

   public static String convertVehicleEntityName(String var0) {
      return "ENTITY_VEHICLE_" + var0;
   }

   public static boolean caseSensitiveFileExists(String var0) throws IOException {
      FileExt var1;
      return (var1 = new FileExt(var0)).exists() && var1.getCanonicalPath().endsWith(var1.getName());
   }

   public static boolean existsIdentifier(StateInterface var0, String var1) throws EntityAlreadyExistsException {
      FileExt var2 = new FileExt(GameServerState.ENTITY_DATABASE_PATH + var1 + ".ent");
      if (var0 instanceof GameServerState && ((GameServerState)var0).getSegmentControllersByNameLowerCase().containsKey(var1.toLowerCase(Locale.ENGLISH))) {
         throw new EntityAlreadyExistsException(var1);
      } else if (var2.exists()) {
         throw new EntityAlreadyExistsException(var1);
      } else {
         return false;
      }
   }

   public static boolean existsIdentifierWOExc(StateInterface var0, String var1) {
      FileExt var2 = new FileExt(GameServerState.ENTITY_DATABASE_PATH + var1 + ".ent");
      if (var0 instanceof GameServerState && ((GameServerState)var0).getSegmentControllersByNameLowerCase().containsKey(var1.toLowerCase(Locale.ENGLISH))) {
         return true;
      } else {
         return var2.exists();
      }
   }

   public static FloatingRock getNewAsteroid(StateInterface var0, String var1, int var2, String var3, float[] var4, int var5, int var6, int var7, int var8, int var9, int var10, String var11, boolean var12) {
      FloatingRock var13;
      (var13 = new FloatingRock(var0)).setUniqueIdentifier(var1);
      var13.getMinPos().set(new Vector3i(var5, var6, var7));
      var13.getMaxPos().set(new Vector3i(var8, var9, var10));
      var13.setId(var0.getNextFreeObjectId());
      var13.setSectorId(var2);
      var13.setRealName(var3);
      var13.setLoadedFromChunk16(var12);
      var13.initialize();
      var13.getInitialTransform().setFromOpenGLMatrix(var4);
      var13.setSpawner(var11);
      return var13;
   }

   public static Ship getNewShip(StateInterface var0, String var1, int var2, String var3, float[] var4, int var5, int var6, int var7, int var8, int var9, int var10, String var11, boolean var12) {
      Ship var13;
      (var13 = new Ship(var0)).setUniqueIdentifier(var1);
      var13.getMinPos().set(new Vector3i(var5, var6, var7));
      var13.getMaxPos().set(new Vector3i(var8, var9, var10));
      var13.setId(var0.getNextFreeObjectId());
      var13.setSectorId(var2);
      var13.setRealName(var3);
      var13.setLoadedFromChunk16(var12);
      var13.initialize();
      var13.getInitialTransform().setFromOpenGLMatrix(var4);
      var13.setTouched(true, false);
      var13.setSpawner(var11);
      return var13;
   }

   public static ShopSpaceStation getNewShop(StateInterface var0, String var1, int var2, String var3, float[] var4, int var5, int var6, int var7, int var8, int var9, int var10, String var11, boolean var12) {
      ShopSpaceStation var13;
      (var13 = new ShopSpaceStation(var0)).setUniqueIdentifier(var1);
      var13.getMinPos().set(new Vector3i(var5, var6, var7));
      var13.getMaxPos().set(new Vector3i(var8, var9, var10));
      var13.setId(var0.getNextFreeObjectId());
      var13.setSectorId(var2);
      var13.setRealName(var3);
      var13.setLoadedFromChunk16(var12);
      var13.initialize();
      var13.getInitialTransform().setFromOpenGLMatrix(var4);
      var13.setSpawner(var11);
      return var13;
   }

   public static SpaceStation getNewSpaceStation(StateInterface var0, String var1, int var2, String var3, float[] var4, int var5, int var6, int var7, int var8, int var9, int var10, boolean var11) {
      SpaceStation var12;
      (var12 = new SpaceStation(var0)).setUniqueIdentifier(var1);
      var12.getMinPos().set(new Vector3i(var5, var6, var7));
      var12.getMaxPos().set(new Vector3i(var8, var9, var10));
      var12.setCreatorId(SpaceStation.SpaceStationType.EMPTY.ordinal());
      var12.setId(var0.getNextFreeObjectId());
      var12.setSectorId(var2);
      var12.setRealName(var3);
      var12.setLoadedFromChunk16(var11);
      var12.initialize();
      var12.getInitialTransform().setFromOpenGLMatrix(var4);
      return var12;
   }

   public static Vehicle getNewVehicle(StateInterface var0, String var1, int var2, String var3, float[] var4, int var5, int var6, int var7, int var8, int var9, int var10, String var11) {
      Vehicle var12;
      (var12 = new Vehicle(var0)).setUniqueIdentifier(var1);
      var12.getMinPos().set(new Vector3i(var5, var6, var7));
      var12.getMaxPos().set(new Vector3i(var8, var9, var10));
      var12.setId(var0.getNextFreeObjectId());
      var12.setSectorId(var2);
      var12.setRealName(var3);
      var12.initialize();
      var12.getInitialTransform().setFromOpenGLMatrix(var4);
      var12.setSpawner(var11);
      return var12;
   }

   public static boolean isShipNameValid(String var0) {
      return var0.length() > 0 && var0.matches("[a-zA-Z0-9_-]+[ a-zA-Z0-9_-]*") && !var0.endsWith(" ");
   }

   public int getClientId() {
      return this.clientId;
   }

   public Class getEntityClass() {
      return this.entityClass;
   }

   public Ship getShip(GameServerState var1, boolean var2) throws EntityNotFountException, IOException, EntityAlreadyExistsException {
      float[] var3 = new float[16];
      int var4 = 0;

      int var5;
      for(var5 = 0; var5 < 16; ++var5) {
         var3[var5] = (Float)this.data[var4++];
      }

      var5 = (Integer)this.data[var4++];
      int var6 = (Integer)this.data[var4++];
      int var7 = (Integer)this.data[var4++];
      int var8 = (Integer)this.data[var4++];
      int var9 = (Integer)this.data[var4++];
      int var10 = (Integer)this.data[var4++];
      int var11 = (Integer)this.data[var4++];
      String var12 = (String)this.data[var4++];
      String var16 = (String)this.data[var4];
      PlayerState var17 = (PlayerState)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var11);
      existsIdentifier(var1, var12);
      Ship var15 = getNewShip(var1, var12, var17.getCurrentSectorId(), var16, var3, var5, var6, var7, var8, var9, var10, var17.getUniqueIdentifier(), false);
      if (var2) {
         var15.setTouched(true, false);
      }

      RemoteSegment var14;
      (var14 = new RemoteSegment(var15)).setSegmentData(new SegmentDataIntArray(var1 instanceof ClientStateInterface));
      var14.getSegmentData().setSegment(var14);

      try {
         var14.getSegmentData().setInfoElementUnsynched((byte)Ship.core.x, (byte)Ship.core.y, (byte)Ship.core.z, (short)1, true, var14.getAbsoluteIndex((byte)Ship.core.x, (byte)Ship.core.y, (byte)Ship.core.z), var1.getUpdateTime());
      } catch (SegmentDataWriteException var13) {
         throw new RuntimeException("Should be a normal data chunk", var13);
      }

      var14.setLastChanged(System.currentTimeMillis());
      var15.getSegmentBuffer().addImmediate(var14);
      var15.getSegmentBuffer().updateBB(var14);
      return var15;
   }

   public SpaceStation getSpaceStation(GameServerState var1, boolean var2) throws EntityAlreadyExistsException {
      float[] var3 = new float[16];
      int var4 = 0;

      int var5;
      for(var5 = 0; var5 < 16; ++var5) {
         var3[var5] = (Float)this.data[var4++];
      }

      var5 = (Integer)this.data[var4++];
      int var6 = (Integer)this.data[var4++];
      int var7 = (Integer)this.data[var4++];
      int var8 = (Integer)this.data[var4++];
      int var9 = (Integer)this.data[var4++];
      int var10 = (Integer)this.data[var4++];
      int var11 = (Integer)this.data[var4++];
      String var12 = (String)this.data[var4++];
      String var16 = (String)this.data[var4];
      existsIdentifier(var1, var12);
      PlayerState var18 = (PlayerState)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var11);
      SpaceStation var15 = getNewSpaceStation(var1, var12, var18.getCurrentSectorId(), var16, var3, var5, var6, var7, var8, var9, var10, false);
      if (var2) {
         var15.setTouched(true, false);
      }

      SegmentDataIntArray var14 = new SegmentDataIntArray(var1 instanceof ClientStateInterface);
      RemoteSegment var17 = new RemoteSegment(var15);
      var14.assignData(var17);

      try {
         var14.setInfoElementUnsynched((byte)16, (byte)16, (byte)16, (short)5, true, var17.getAbsoluteIndex((byte)16, (byte)16, (byte)16), var1.getUpdateTime());
      } catch (SegmentDataWriteException var13) {
         throw new RuntimeException("Should be a normal data chunk", var13);
      }

      var17.lastLocalTimeStamp = System.currentTimeMillis();
      var15.getSegmentBuffer().addImmediate(var17);
      var17.setLastChanged(System.currentTimeMillis());
      var15.getSegmentBuffer().setLastChanged(var17.pos, var17.lastLocalTimeStamp);

      assert var15.getTotalElements() == 1 : var15.getTotalElements();

      return var15;
   }

   public Vehicle getVehicle(GameServerState var1) throws EntityAlreadyExistsException {
      float[] var2 = new float[16];
      int var3 = 0;

      int var4;
      for(var4 = 0; var4 < 16; ++var4) {
         var2[var4] = (Float)this.data[var3++];
      }

      var4 = (Integer)this.data[var3++];
      int var5 = (Integer)this.data[var3++];
      int var6 = (Integer)this.data[var3++];
      int var7 = (Integer)this.data[var3++];
      int var8 = (Integer)this.data[var3++];
      int var9 = (Integer)this.data[var3++];
      int var10 = (Integer)this.data[var3++];
      String var11 = (String)this.data[var3++];
      String var14 = (String)this.data[var3];
      existsIdentifier(var1, var11);
      PlayerState var16 = (PlayerState)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var10);
      Vehicle var13 = getNewVehicle(var1, var11, var16.getCurrentSectorId(), var14, var2, var4, var5, var6, var7, var8, var9, var16.getUniqueIdentifier());
      RemoteSegment var15;
      (var15 = new RemoteSegment(var13)).setSegmentData(new SegmentDataIntArray(var1 instanceof ClientStateInterface));
      var15.getSegmentData().setSegment(var15);

      try {
         var15.getSegmentData().setInfoElementUnsynched((byte)16, (byte)16, (byte)16, (short)1, true, var15.getAbsoluteIndex((byte)16, (byte)16, (byte)16), var1.getUpdateTime());
      } catch (SegmentDataWriteException var12) {
         throw new RuntimeException("Should be a normal data chunk", var12);
      }

      var15.setLastChanged(System.currentTimeMillis());
      var13.getSegmentBuffer().addImmediate(var15);
      return var13;
   }
}
