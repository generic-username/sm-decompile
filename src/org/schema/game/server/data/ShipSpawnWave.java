package org.schema.game.server.data;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map.Entry;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.game.common.data.player.catalog.CatalogWavePermission;
import org.schema.game.server.controller.BluePrintController;

public class ShipSpawnWave {
   public static final int MAX_LEVEL = 10;
   public BluePrintController bluePrintController;
   public Vector3i sectorId;
   private int waveFaction;
   private int level;
   private int timeInSecs;
   private long timeInitiatedInMS;
   private ArrayList printsToSpawn;

   public ShipSpawnWave(int var1, int var2, BluePrintController var3, int var4, Vector3i var5) {
      this.waveFaction = var1;
      this.level = var2;
      this.sectorId = var5;
      this.timeInSecs = var4;
      this.bluePrintController = var3;
      this.setTimeInitiatedInMS(System.currentTimeMillis());
   }

   public void createWave(GameServerState var1, int var2) {
      Collection var3 = var1.getCatalogManager().getCatalog();
      short var4 = -1;
      int var5 = Integer.MAX_VALUE;
      Object2ObjectOpenHashMap var6 = new Object2ObjectOpenHashMap();
      Iterator var13 = var3.iterator();

      int var9;
      while(var13.hasNext()) {
         CatalogPermission var7 = (CatalogPermission)var13.next();
         CatalogWavePermission var8 = null;
         var9 = Integer.MAX_VALUE;
         Iterator var10 = var7.wavePermissions.iterator();

         while(var10.hasNext()) {
            CatalogWavePermission var11;
            int var12;
            if ((var11 = (CatalogWavePermission)var10.next()).factionId == this.waveFaction && (var12 = Math.abs(var11.difficulty - this.level)) < var9) {
               System.err.println("[AI] closest difficulty now: " + var11.difficulty + "; distance " + var12 + "; clsoest bef: " + var9 + "; level asked: " + this.level);
               if (var12 < var5) {
                  var4 = var11.difficulty;
                  var5 = var12;
               }

               var8 = var11;
               var9 = var12;
            }
         }

         if (var8 != null) {
            var6.put(var7, var8);
         }
      }

      ObjectIterator var14 = var6.entrySet().iterator();
      ArrayList var15 = new ArrayList();

      while(true) {
         while(var14.hasNext()) {
            Entry var16;
            if (((CatalogWavePermission)(var16 = (Entry)var14.next()).getValue()).difficulty != var4) {
               var14.remove();
            } else {
               for(var9 = 0; var9 < ((CatalogWavePermission)var16.getValue()).amount; ++var9) {
                  var15.add(var16.getKey());
               }
            }
         }

         if (var6.size() > 0) {
            this.setPrintsToSpawn(var15);
            return;
         }

         this.createWaveOld(var1, var2);
         return;
      }
   }

   public void createWaveOld(GameServerState var1, int var2) {
      var1.getController();
      Collection var6 = var1.getCatalogManager().getCatalog();
      ArrayList var3 = new ArrayList();
      Iterator var7 = var6.iterator();

      while(var7.hasNext()) {
         CatalogPermission var4;
         if ((var4 = (CatalogPermission)var7.next()).enemyUsable()) {
            var3.add(var4);
         }
      }

      if (var3.isEmpty()) {
         System.err.println("[WAVE] Server will not spawn any waves, the catalog is empty");
      } else {
         Collections.sort(var3, new Comparator() {
            public int compare(CatalogPermission var1, CatalogPermission var2) {
               return (int)(var1.price - var2.price);
            }
         });
         float var8 = (float)var3.size() / 10.0F;
         int var10 = (int)Math.min((double)(var3.size() - 1), Math.ceil((double)(var8 * (float)this.level)));
         this.setPrintsToSpawn(new ArrayList());

         for(int var9 = 0; var9 < var2; ++var9) {
            int var5 = Math.min(var3.size() - 1, Math.max(0, var10 - 2 + var9));
            this.getPrintsToSpawn().add(var3.get(var5));
         }

      }
   }

   public int getLevel() {
      return this.level;
   }

   public void setLevel(int var1) {
      this.level = var1;
   }

   public ArrayList getPrintsToSpawn() {
      return this.printsToSpawn;
   }

   public void setPrintsToSpawn(ArrayList var1) {
      this.printsToSpawn = var1;
   }

   public long getTimeInitiatedInMS() {
      return this.timeInitiatedInMS;
   }

   public void setTimeInitiatedInMS(long var1) {
      this.timeInitiatedInMS = var1;
   }

   public int getTimeInSecs() {
      return this.timeInSecs;
   }

   public void setTimeInSecs(int var1) {
      this.timeInSecs = var1;
   }

   public int getWaveTeam() {
      return this.waveFaction;
   }

   public void setWaveFaction(int var1) {
      this.waveFaction = var1;
   }
}
