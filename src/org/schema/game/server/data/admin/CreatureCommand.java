package org.schema.game.server.data.admin;

public enum CreatureCommand {
   GOTO,
   GRAVITY,
   SIT,
   STAND_UP,
   ROAM,
   IDLE;
}
