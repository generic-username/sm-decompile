package org.schema.game.server.data.admin;

public class AdminParamters {
   public static final Class[] longFloat3 = new Class[]{Long.class, Float.class, Float.class, Float.class};
   public static final Class[] longOnly = new Class[]{Long.class};
   public static final Class[] longShortInt = new Class[]{Long.class, Short.class, Integer.class};
}
