package org.schema.game.server.data.admin;

import com.bulletphysics.collision.dispatch.CollisionWorld.ClosestRayResultCallback;
import com.bulletphysics.linearmath.Transform;
import com.google.common.collect.Lists;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.LogUtil;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.ParticleUtil;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.ParticleEntry;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.TransientSegmentController;
import org.schema.game.common.controller.ai.AIGameCreatureConfiguration;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.controller.database.DatabaseIndex;
import org.schema.game.common.controller.database.tables.EntityTable;
import org.schema.game.common.controller.elements.EntityIndexScore;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.controller.elements.ShieldAddOn;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.controller.elements.SpaceStationManagerContainer;
import org.schema.game.common.controller.elements.StationaryManagerContainer;
import org.schema.game.common.controller.elements.power.PowerAddOn;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.controller.elements.warpgate.WarpgateCollectionManager;
import org.schema.game.common.controller.rails.RailRelation;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementClassNotFoundException;
import org.schema.game.common.data.element.ElementDocking;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.InvalidFactoryParameterException;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.element.meta.Recipe;
import org.schema.game.common.data.element.meta.RecipeCreator;
import org.schema.game.common.data.element.meta.weapon.GrappleBeam;
import org.schema.game.common.data.element.meta.weapon.LaserWeapon;
import org.schema.game.common.data.element.meta.weapon.RocketLauncherWeapon;
import org.schema.game.common.data.element.meta.weapon.SniperRifle;
import org.schema.game.common.data.element.meta.weapon.TorchBeam;
import org.schema.game.common.data.element.meta.weapon.Weapon;
import org.schema.game.common.data.fleet.FleetMember;
import org.schema.game.common.data.missile.TargetChasingMissile;
import org.schema.game.common.data.mission.spawner.DefaultSpawner;
import org.schema.game.common.data.mission.spawner.SpawnMarker;
import org.schema.game.common.data.mission.spawner.component.SpawnComponentCreature;
import org.schema.game.common.data.mission.spawner.component.SpawnComponentDestroySpawnerAfterCount;
import org.schema.game.common.data.mission.spawner.condition.SpawnConditionCreatureCountOnAffinity;
import org.schema.game.common.data.mission.spawner.condition.SpawnConditionTime;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.game.common.data.physics.PhysicsExt;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.PlayerControlledTransformableNotFound;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.PlayerStateSpawnData;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionAddCallback;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionNotFoundException;
import org.schema.game.common.data.player.faction.FactionPermission;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.player.faction.FactionRoles;
import org.schema.game.common.data.player.faction.FactionSystemOwnerChange;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventorySlot;
import org.schema.game.common.data.player.inventory.NoSlotFreeException;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.common.data.world.Universe;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.common.data.world.space.PlanetCore;
import org.schema.game.network.objects.remote.RemoteNPCSystem;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.controller.EntityAlreadyExistsException;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.controller.NoIPException;
import org.schema.game.server.data.Admin;
import org.schema.game.server.data.CreatureSpawn;
import org.schema.game.server.data.CreatureType;
import org.schema.game.server.data.EntityRequest;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.game.server.data.ProtectedUplinkName;
import org.schema.game.server.data.ServerConfig;
import org.schema.game.server.data.blueprint.ChildStats;
import org.schema.game.server.data.blueprint.SegmentControllerOutline;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.game.server.data.simulation.jobs.SpawnTradingPartyJob;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.game.server.data.simulation.npc.NPCFactionSpawn;
import org.schema.game.server.data.simulation.npc.NPCSpawnException;
import org.schema.game.server.data.simulation.npc.NPCStartupConfig;
import org.schema.game.server.data.simulation.npc.geo.NPCEntityContainer;
import org.schema.game.server.data.simulation.npc.geo.NPCSystem;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.graphicsengine.forms.debug.DebugPoint;
import org.schema.schine.network.RegisteredClientInterface;
import org.schema.schine.network.objects.LocalSectorTransition;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.objects.container.TransformTimed;
import org.schema.schine.network.server.AdminLocalClient;
import org.schema.schine.network.server.AdminRemoteClient;
import org.schema.schine.network.server.ServerMessage;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.tag.Tag;

public class AdminCommandQueueElement {
   private Object[] commandParams;
   private AdminCommands command;
   private RegisteredClientInterface client;
   private int sqlT;

   public AdminCommandQueueElement(RegisteredClientInterface var1, AdminCommands var2, Object[] var3) {
      this.client = var1;
      this.command = var2;
      this.commandParams = var3;
   }

   public static SimpleTransformableSendableObject getControllerRoot(SimpleTransformableSendableObject var0) {
      if (var0 instanceof SegmentController) {
         if (((SegmentController)var0).getDockingController().isDocked()) {
            SegmentController var1;
            return (var1 = ((SegmentController)var0).getDockingController().getDockedOn().to.getSegment().getSegmentController()).getDockingController().isDocked() ? var1.getDockingController().getDockedOn().to.getSegment().getSegmentController() : var1;
         } else {
            return var0;
         }
      } else {
         return var0;
      }
   }

   private void activateWhiteList(GameServerState var1) throws IOException {
      Boolean var2 = (Boolean)this.commandParams[0];
      ServerConfig.USE_WHITELIST.changeBooleanSetting(var2);
      ServerConfig.write();
      this.client.serverMessage("[ADMIN COMMAND] Set WHITELIST activation to: " + var2);
   }

   private void addAdmin(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         PlayerState var3 = var1.getPlayerFromNameIgnoreCase(var2);
         var1.getController().addAdmin(this.client.getPlayerName(), var3.getName());
      } catch (PlayerNotFountException var4) {
         var1.getController().addAdmin(this.client.getPlayerName(), var2);
         this.client.serverMessage("[ADMIN COMMAND] [WARNING] '" + var2 + "' is NOT online. Please make sure you have the correct name. Name was still added to admin list");
      }
   }

   private void addAllItems(GameServerState var1) throws IOException {
      try {
         String var2 = (String)this.commandParams[0];
         Integer var3 = (Integer)this.commandParams[1];
         PlayerState var10;
         Inventory var11 = (var10 = var1.getPlayerFromName(var2)).getInventory(new Vector3i());
         IntOpenHashSet var4 = new IntOpenHashSet();
         short[] var5;
         int var6 = (var5 = ElementKeyMap.typeList()).length;

         for(int var7 = 0; var7 < var6; ++var7) {
            short var8 = var5[var7];
            System.err.println("ADDING ITEM " + var8 + " " + var3);
            int var12 = var11.incExistingOrNextFreeSlot(var8, var3);
            var4.add(var12);
         }

         var10.sendInventoryModification(var4, Long.MIN_VALUE);
      } catch (PlayerNotFountException var9) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player not found for your client");
      }
   }

   private void addAllItemsCategory(GameServerState var1) throws IOException {
      try {
         String var2 = (String)this.commandParams[0];
         Integer var3 = (Integer)this.commandParams[1];
         String var4 = (String)this.commandParams[2];
         PlayerState var12 = var1.getPlayerFromName(var2);
         IntOpenHashSet var13 = new IntOpenHashSet();
         Inventory var5 = var12.getInventory(new Vector3i());
         short[] var6;
         int var7 = (var6 = ElementKeyMap.typeList()).length;

         for(int var8 = 0; var8 < var7; ++var8) {
            short var9 = var6[var8];
            if (this.belongsToCategory(var4, var9)) {
               System.err.println("ADDING ITEM " + var9 + " " + var3 + " " + var4);
               int var14 = var5.incExistingOrNextFreeSlot(var9, var3);
               var13.add(var14);
            }
         }

         var12.sendInventoryModification(var13, Long.MIN_VALUE);
      } catch (PlayerNotFountException var10) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player not found for your client");
      } catch (UnknownCategoryException var11) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] unknown category: " + var11.getCat());
      }
   }

   private void addFaction(final GameServerState var1, boolean var2) throws IOException {
      final int var3;
      final String var4;
      String var5;
      if (var2) {
         var3 = (Integer)this.commandParams[0];
         var5 = ((String)this.commandParams[1]).trim();
         var4 = ((String)this.commandParams[2]).trim();
      } else {
         var3 = FactionManager.getNewId();
         var5 = ((String)this.commandParams[0]).trim();
         var4 = ((String)this.commandParams[1]).trim();
      }

      if (var5.length() > 0) {
         Faction var6;
         (var6 = new Faction(var1, var3, var5, "description goes here")).setOpenToJoin(true);
         var6.addOrModifyMember("ADMIN", var4, (byte)4, System.currentTimeMillis(), var1.getGameState(), false);
         var1.getGameState().getFactionManager().addFaction(var6);
         var6.addHook = new FactionAddCallback() {
            public void callback() {
               try {
                  var1.getPlayerFromName(var4).getFactionController().setFactionId(var3);
               } catch (PlayerNotFountException var6) {
                  PlayerNotFountException var1x = var6;

                  try {
                     FileExt var2;
                     if ((var2 = new FileExt(GameServerState.ENTITY_DATABASE_PATH + "ENTITY_PLAYERSTATE_" + var4 + ".ent")).exists()) {
                        Tag var3x;
                        ((Tag[])((Tag[])(var3x = Tag.readFrom(new FileInputStream(var2), true, false)).getValue())[6].getValue())[0].setValue(var3);
                        var3x.writeTo(new FileOutputStream(var2), true);
                     } else {
                        AdminCommandQueueElement.this.client.serverMessage("[ADMIN COMMAND] [ERROR] File not found: " + var2.getAbsolutePath());
                     }
                  } catch (Exception var5) {
                     try {
                        AdminCommandQueueElement.this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var1x.getClass().getSimpleName());
                     } catch (IOException var4x) {
                        var4x.printStackTrace();
                     }

                     var5.printStackTrace();
                  }
               }
            }
         };
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] added new faction!");
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] name empty!");
      }
   }

   private void addFactionAmount(GameServerState var1, boolean var2) throws IOException {
      int var5 = (Integer)this.commandParams[1];

      for(int var6 = 0; var6 < var5; ++var6) {
         int var3 = FactionManager.getNewId();
         String var7 = ((String)this.commandParams[0]).trim() + var6;
         String var4 = "Random" + var6;
         if (var7.length() > 0) {
            Faction var8;
            (var8 = new Faction(var1, var3, var7, "description goes here")).setOpenToJoin(true);
            var8.addOrModifyMember("ADMIN", var4, (byte)4, System.currentTimeMillis(), var1.getGameState(), false);
            var1.getGameState().getFactionManager().addFaction(var8);
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] added new faction!");
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] name empty!");
         }
      }

   }

   private void banIp(GameServerState var1, boolean var2, boolean var3) throws IOException {
      long var4 = -1L;
      if (var3) {
         var4 = System.currentTimeMillis() + ((Integer)this.commandParams[1]).longValue() * 60000L;
      }

      String var11;
      if (!var2) {
         var11 = (String)this.commandParams[0];
         Iterator var6 = var1.getPlayerStatesByName().values().iterator();

         while(var6.hasNext()) {
            PlayerState var12;
            if ((var12 = (PlayerState)var6.next()).getIp().replace("/", "").equals(var11)) {
               this.kick(var1, false, var3 ? 2 : 1, var12.getName());
            }
         }

         try {
            var1.getController().addBannedIp(this.client.getPlayerName(), var11, var4);
            this.client.serverMessage("[ADMIN COMMAND] successfully banned: " + var11);
         } catch (NoIPException var8) {
            var8.printStackTrace();
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] not an IP: " + var11);
         }
      } else {
         var11 = (String)this.commandParams[0];

         try {
            this.kick(var1, false, var3 ? 2 : 1, var11);
            String var7 = var1.getPlayerFromName(var11).getIp().replaceAll("/", "");

            try {
               var1.getController().addBannedIp(this.client.getPlayerName(), var7, var4);
               this.client.serverMessage("[ADMIN COMMAND] successfully banned IP: " + var7 + " of " + var11);
            } catch (NoIPException var9) {
               var9.printStackTrace();
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] not an IP: " + var7 + " of " + var11);
            }
         } catch (PlayerNotFountException var10) {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] ban failed: " + var11 + " does not exist");
         }
      }
   }

   private void unBanAccount(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      var1.getController().removeBannedAccount(this.client.getPlayerName(), var2);
      this.client.serverMessage("[ADMIN COMMAND] successfully unbanned account: " + var2);
   }

   private void banAccount(GameServerState var1, boolean var2, boolean var3) throws IOException {
      String var4 = (String)this.commandParams[0];
      long var5 = -1L;
      if (var3) {
         var5 = System.currentTimeMillis() + ((Integer)this.commandParams[1]).longValue() * 60000L;
      }

      if (!var2) {
         Iterator var10 = var1.getPlayerStatesByName().values().iterator();

         while(var10.hasNext()) {
            PlayerState var7;
            if ((var7 = (PlayerState)var10.next()).getStarmadeName() != null && var7.getStarmadeName().equals(var4)) {
               this.kick(var1, false, var3 ? 2 : 1, var7.getName());
            }
         }

         var1.getController().addBannedAccount(this.client.getPlayerName(), var4, var5);
         this.client.serverMessage("[ADMIN COMMAND] successfully banned account: " + var4);
      } else {
         try {
            PlayerState var9;
            if ((var9 = var1.getPlayerFromName(var4)).getStarmadeName() != null) {
               this.kick(var1, false, var3 ? 2 : 1, var4);
               var1.getController().addBannedAccount(this.client.getPlayerName(), var9.getStarmadeName(), var5);
               this.client.serverMessage("[ADMIN COMMAND] successfully banned account of: " + var4 + " -> " + var9.getStarmadeName());
            } else {
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] ban failed: " + var4 + " not uplinked");
            }
         } catch (PlayerNotFountException var8) {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] ban failed: " + var4 + " does not exist");
         }
      }
   }

   private void banName(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      boolean var3 = (Boolean)this.commandParams[1];
      String var4 = this.commandParams.length >= 3 ? (String)this.commandParams[2] : "";
      long var5 = -1L;
      String var7 = "";
      if (this.commandParams.length >= 4) {
         var5 = System.currentTimeMillis() + ((Integer)this.commandParams[3]).longValue() * 60000L;
         var7 = " (Temporary Ban for " + StringTools.formatTimeFromMS(var5 - System.currentTimeMillis()) + ")";
         if (var4.length() > 0) {
            var4 = var4 + var7;
         }
      }

      var1.getController().addBannedName(this.client.getPlayerName(), var2, var5);
      if (var3) {
         if (var4.length() > 0) {
            this.kick(var1, var4);
         } else {
            this.kick(var1, "You have been banned by an admin for " + var7);
         }
      }

      this.client.serverMessage("[ADMIN COMMAND] successfully banned: " + var2);
   }

   private boolean belongsToCategory(String var1, short var2) throws UnknownCategoryException {
      return ElementKeyMap.getInfo(var2).getType().hasParent(var1);
   }

   private ClosestRayResultCallback getCollision(Vector3f var1, Vector3f var2, GameServerState var3, SimpleTransformableSendableObject var4) throws NoCollisioinFountException, IOException {
      Vector3f var6 = new Vector3f(var1);
      var1 = new Vector3f(var1);
      var2.scale(8000.0F);
      var1.add(var2);
      CubeRayCastResult var5;
      (var5 = new CubeRayCastResult(var6, var1, false, new SegmentController[0])).setIgnoereNotPhysical(true);
      var5.setOnlyCubeMeshes(true);
      return var4.getPhysics().testRayCollisionPoint(var6, var1, var5, false);
   }

   private SegmentPiece getPiece(Vector3f var1, Vector3f var2, GameServerState var3, SimpleTransformableSendableObject var4, Vector3i var5) throws NoCollisioinFountException, IOException {
      Vector3f var6 = new Vector3f(var1);
      var2.scale(8000.0F);
      var6.add(var2);
      ClosestRayResultCallback var9;
      if ((var9 = ((PhysicsExt)var3.getUniverse().getSector(var4.getSectorId()).getPhysics()).testRayCollisionPoint(var1, var6, false, (SimpleTransformableSendableObject)null, (SegmentController)null, false, true, false)).hasHit()) {
         CubeRayCastResult var7;
         if ((var7 = (CubeRayCastResult)var9).getSegment() != null) {
            SegmentPiece var8;
            (var8 = new SegmentPiece(var7.getSegment(), var7.getCubePos())).getAbsolutePos(var5);
            Vector3i var11;
            (var11 = new Vector3i(var5)).add(-16, -16, -16);
            var7.getSegment().getSegmentController().getWorldTransformInverse().transform(var9.hitPointWorld);
            SegmentPiece var10 = var8.getSegment().getSegmentController().getSegmentBuffer().getPointUnsave(var5, var8);
            switch(Element.getSide(var7.hitPointWorld, var10 == null ? null : var10.getAlgorithm(), var11, var10 != null ? var10.getType() : 0, var10 != null ? var10.getOrientation() : 0)) {
            case 0:
               var5.z = (int)((float)var5.z + 1.0F);
               break;
            case 1:
               var5.z = (int)((float)var5.z - 1.0F);
               break;
            case 2:
               var5.y = (int)((float)var5.y + 1.0F);
               break;
            case 3:
               var5.y = (int)((float)var5.y - 1.0F);
               break;
            case 4:
               var5.x = (int)((float)var5.x + 1.0F);
               break;
            case 5:
               var5.x = (int)((float)var5.x - 1.0F);
               break;
            default:
               System.err.println("[BUILDMODEDRAWER] WARNING: NO SIDE recognized!!!");
            }

            return var8.getSegment().getSegmentController().getSegmentBuffer().getPointUnsave(var5, var8);
         }
      } else {
         System.err.println("[ADMIN] no collision point: " + var1 + " + " + var2 + " -> " + var6);
      }

      throw new NoCollisioinFountException();
   }

   private void testPath(GameServerState var1, CreatureCommand var2) throws IOException {
      try {
         PlayerState var3;
         SimpleTransformableSendableObject var4 = (var3 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable();
         Sendable var5;
         if ((var5 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var3.getNetworkObject().selectedEntityId.get())) != null && var5 instanceof AICreature) {
            AICreature var17 = (AICreature)var5;
            Vector3f var6;
            if (var4 instanceof AbstractCharacter) {
               var6 = new Vector3f(((AbstractCharacter)var4).getHeadWorldTransform().origin);
            } else {
               var6 = new Vector3f(var4.getWorldTransform().origin);
            }

            Vector3f var16 = var3.getForward(new Vector3f());
            Vector3i var7 = new Vector3i();
            SegmentPiece var8 = this.getPiece(var6, var16, var1, var4, var7);
            switch(var2) {
            case IDLE:
               var17.getAiConfiguration().get(Types.ORDER).switchSetting("Idling", true);
               var17.getOwnerState().standUp();
               this.client.serverMessage("[ADMIN COMMAND] creature " + var17 + " idling");
               break;
            case ROAM:
               Vector3i var13 = var8.getAbsolutePos(new Vector3i());
               var17.getAiConfiguration().get(Types.ORIGIN_X).switchSetting(String.valueOf(var13.x), true);
               var17.getAiConfiguration().get(Types.ORIGIN_Y).switchSetting(String.valueOf(var13.y), true);
               var17.getAiConfiguration().get(Types.ORIGIN_Z).switchSetting(String.valueOf(var13.z), true);
               var17.getAiConfiguration().get(Types.ORDER).switchSetting("Roaming", true);
               var17.getOwnerState().standUp();
               this.client.serverMessage("[ADMIN COMMAND] creature " + var17 + " roaming");
               break;
            case STAND_UP:
               var17.getOwnerState().standUp();
               this.client.serverMessage("[ADMIN COMMAND] creature " + var17 + " standing up");
               break;
            case SIT:
               ClosestRayResultCallback var12;
               if (!(var12 = this.getCollision(var6, var16, var1, var4)).hasHit()) {
                  this.client.serverMessage("[ERROR] " + var17 + " Cannot sit. Your line of sight has no collision");
               } else {
                  var17.sitDown(var12, var6, var16, 8000.0F);
                  this.client.serverMessage("[ADMIN COMMAND] sit down: " + var17 + ": " + var17.getOwnerState().isSitting());
               }
               break;
            case GOTO:
               var17.getOwnerState().plotPath(var8, true);
               this.client.serverMessage("[ADMIN COMMAND] testing path plotted");
               break;
            case GRAVITY:
               if (var8 != null) {
                  var17.enableGravityOnAI(var8.getSegment().getSegmentController(), new Vector3f(0.0F, -9.89F, 0.0F));
                  this.client.serverMessage("[ADMIN COMMAND] gravity engaged");
               } else {
                  var17.enableGravityOnAI((SegmentController)null, new Vector3f(0.0F, 0.0F, 0.0F));
                  this.client.serverMessage("[ADMIN COMMAND] gravity disengaged");
               }
            }

            if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
               Vector3f var14 = new Vector3f((float)(var7.x - 16), (float)(var7.y - 16), (float)(var7.z - 16));
               var8.getSegment().getSegmentController().getWorldTransform().transform(var14);
               DebugPoint var15;
               (var15 = new DebugPoint(var14, new Vector4f(1.0F, 0.0F, 0.0F, 1.0F))).size = 0.3F;
               var15.LIFETIME = 10000L;
               DebugDrawer.points.add(var15);
            }

         } else {
            this.client.serverMessage("[ERROR][ADMIN COMMAND] OBJECT TO DESTROY NOT FOUND");
         }
      } catch (PlayerNotFountException var9) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player not found for your client");
      } catch (NoCollisioinFountException var10) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] no collision found");
      } catch (Exception var11) {
         var11.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] server could not load sector");
      }
   }

   private void testBreaking(GameServerState var1) throws IOException {
      try {
         PlayerState var2;
         SimpleTransformableSendableObject var3;
         Vector3f var4;
         if ((var3 = (var2 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable()) instanceof AbstractCharacter) {
            var4 = new Vector3f(((AbstractCharacter)var3).getHeadWorldTransform().origin);
         } else {
            var4 = new Vector3f(var3.getWorldTransform().origin);
         }

         Vector3f var5 = new Vector3f(var4);
         Vector3f var8;
         (var8 = var2.getForward(new Vector3f())).scale(5000.0F);
         var5.add(var8);
         ClosestRayResultCallback var9;
         if ((var9 = ((PhysicsExt)var1.getUniverse().getSector(var3.getSectorId()).getPhysics()).testRayCollisionPoint(var4, var5, false, (SimpleTransformableSendableObject)null, (SegmentController)null, false, true, false)).hasHit()) {
            CubeRayCastResult var10;
            if ((var10 = (CubeRayCastResult)var9).getSegment() != null) {
               SegmentPiece var11 = new SegmentPiece(var10.getSegment(), var10.getCubePos());
               var1.getController().queueSegmentControllerBreak(var11);
            }

            this.client.serverMessage("[ADMIN COMMAND] testing break ");
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] no object in line of sight");
         }
      } catch (PlayerNotFountException var6) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player not found for your client");
      } catch (Exception var7) {
         var7.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] server could not load sector");
      }
   }

   private void changeSector(GameServerState var1, boolean var2) throws IOException {
      try {
         PlayerState var3 = var1.getPlayerFromStateId(this.client.getId());
         Vector3i var4 = new Vector3i((Integer)this.commandParams[0], (Integer)this.commandParams[1], (Integer)this.commandParams[2]);
         Sector var5;
         if ((var5 = var1.getUniverse().getSector(var4)) != null) {
            Iterator var8 = var3.getControllerState().getUnits().iterator();

            while(var8.hasNext()) {
               ControllerStateUnit var9;
               if ((var9 = (ControllerStateUnit)var8.next()).playerControllable instanceof SimpleTransformableSendableObject) {
                  var1.getController().queueSectorSwitch(getControllerRoot((SimpleTransformableSendableObject)var9.playerControllable), var5.pos, 1, var2, true, true);
               }
            }

         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] sector not found: " + var4 + ": " + var1.getUniverse().getSectorSet());
         }
      } catch (PlayerNotFountException var6) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player not found for your client");
      } catch (Exception var7) {
         var7.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] server could not load sector");
      }
   }

   private void changeSectorPlayer(GameServerState var1, boolean var2) throws IOException {
      try {
         PlayerState var3 = var1.getPlayerFromName(((String)this.commandParams[0]).trim());
         Vector3i var4 = new Vector3i((Integer)this.commandParams[1], (Integer)this.commandParams[2], (Integer)this.commandParams[3]);
         Sector var5;
         if ((var5 = var1.getUniverse().getSector(var4)) != null) {
            boolean var6 = false;
            Iterator var7 = var3.getControllerState().getUnits().iterator();

            while(var7.hasNext()) {
               ControllerStateUnit var8;
               if ((var8 = (ControllerStateUnit)var7.next()).playerControllable instanceof SimpleTransformableSendableObject) {
                  var1.getController().queueSectorSwitch(getControllerRoot((SimpleTransformableSendableObject)var8.playerControllable), var5.pos, 1, var2, true, true);
                  var6 = true;
               }
            }

            if (var6) {
               this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] changed sector for " + var3.getName() + " to " + var4);
            } else {
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] not changed sector for " + var3.getName() + " to " + var4 + ": Player is not bound to any entity");
            }
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] sector not found: " + var4 + ": " + var1.getUniverse().getSectorSet());
         }
      } catch (PlayerNotFountException var9) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player not found for your client " + (String)this.commandParams[0]);
      } catch (Exception var10) {
         var10.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] server could not load sector");
      }
   }

   private void changeSectorUid(GameServerState var1, boolean var2) throws IOException {
      try {
         String var3 = (String)this.commandParams[0];
         Sendable var4;
         if ((var4 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var3)) != null && var4 instanceof SimpleTransformableSendableObject) {
            Vector3i var5 = new Vector3i((Integer)this.commandParams[1], (Integer)this.commandParams[2], (Integer)this.commandParams[3]);
            Sector var6;
            if ((var6 = var1.getUniverse().getSector(var5)) != null) {
               var1.getController().queueSectorSwitch(getControllerRoot((SimpleTransformableSendableObject)var4), var6.pos, 1, var2, true, true);
               this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] changed sector for " + var3 + " to " + var5);
            } else {
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] sector not found: " + var5 + ": " + var1.getUniverse().getSectorSet());
            }
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] sector not found: " + var3 + " not found or loaded");
         }
      } catch (Exception var7) {
         var7.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] server could not load sector");
      }
   }

   private void changeSectorSelected(GameServerState var1, boolean var2) throws IOException {
      try {
         PlayerState var3 = var1.getPlayerFromStateId(this.client.getId());
         Sendable var7;
         if ((var7 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var3.getNetworkObject().selectedEntityId.get())) != null && var7 instanceof SimpleTransformableSendableObject) {
            Vector3i var4 = new Vector3i((Integer)this.commandParams[0], (Integer)this.commandParams[1], (Integer)this.commandParams[2]);
            Sector var5;
            if ((var5 = var1.getUniverse().getSector(var4)) != null) {
               var1.getController().queueSectorSwitch(getControllerRoot((SimpleTransformableSendableObject)var7), var5.pos, 1, var2, true, true);
               this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] changed sector for " + var7 + " to " + var4);
            } else {
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] sector not found: " + var7 + ": " + var1.getUniverse().getSectorSet());
            }
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] sector not found: " + var7 + " not found or loaded");
         }
      } catch (Exception var6) {
         var6.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] server could not load sector");
      }
   }

   private void creatureAnimation(GameServerState var1, boolean var2) throws IOException {
      try {
         PlayerState var3 = var1.getPlayerFromStateId(this.client.getId());
         System.err.println("[ADMIN COMMAND] checking selected");
         Sendable var7;
         if ((var7 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var3.getNetworkObject().selectedEntityId.get())) != null && var7 instanceof AICreature) {
            AICreature var8 = (AICreature)var7;
            if (var2) {
               String var9 = (String)this.commandParams[0];
               String var10 = (String)this.commandParams[1];
               float var4 = (Float)this.commandParams[2];
               boolean var5 = (Boolean)this.commandParams[3];
               var8.forceAnimation(var9, var10, var4, var5);
               this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Started animation");
               return;
            }

            var8.stopForcedAnimation();
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Stopped animation");
         }

      } catch (Exception var6) {
         var6.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var6.getMessage());
      }
   }

   private void creatureScript(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         PlayerState var3 = var1.getPlayerFromStateId(this.client.getId());
         System.err.println("[ADMIN COMMAND]checking selected");
         Sendable var5;
         SimpleTransformableSendableObject var6;
         if ((var5 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var3.getNetworkObject().selectedEntityId.get())) != null && var5 instanceof SimpleTransformableSendableObject && (var6 = (SimpleTransformableSendableObject)var5) instanceof AICreature) {
            if ((new FileExt("./data/script/" + var2)).exists()) {
               ((AICreature)var6).getOwnerState().setConversationScript(var2);
               this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] script of " + var6 + " set to " + var2);
               return;
            }

            this.client.serverMessage("[ADMIN COMMAND] [ERROR] script not found: " + (new FileExt("./data/script/" + var2)).getAbsolutePath());
         }

      } catch (PlayerNotFountException var4) {
         var4.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      }
   }

   private void creatureRename(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         PlayerState var3 = var1.getPlayerFromStateId(this.client.getId());
         System.err.println("[ADMIN COMMAND]checking selected");
         Sendable var5;
         SimpleTransformableSendableObject var6;
         if ((var5 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var3.getNetworkObject().selectedEntityId.get())) != null && var5 instanceof SimpleTransformableSendableObject && (var6 = (SimpleTransformableSendableObject)var5) instanceof AICreature) {
            ((AICreature)var6).setRealName(var2);
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] renamed object to" + var2);
         }

      } catch (PlayerNotFountException var4) {
         var4.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      }
   }

   private void checkFactionMembers(GameServerState var1) throws IOException {
      Integer var2;
      if ((var2 = (Integer)this.commandParams[0]) != null) {
         Faction var3;
         if ((var3 = var1.getGameState().getFactionManager().getFaction(var2)) != null) {
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] " + var3.getName() + ": " + var3.getMembersUID());
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] Faction Not Found: " + var2);
         }
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Faction Not Found (must be ID, list with /faction_list: " + var2);
      }
   }

   private void checkFactions(GameServerState var1) {
      var1.getGameState().getFactionManager().flagCheckFactions();
   }

   private void chmodSector(GameServerState var1) throws IOException {
      try {
         Vector3i var2 = new Vector3i((Integer)this.commandParams[0], (Integer)this.commandParams[1], (Integer)this.commandParams[2]);
         Sector var5 = var1.getUniverse().getSector(var2);
         String var6;
         if (!(var6 = (String)this.commandParams[3]).equals("+") && !var6.equals("-")) {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] operator must be either + or -");
         } else {
            String var3;
            if (!(var3 = (String)this.commandParams[4]).toLowerCase(Locale.ENGLISH).equals("peace") && !var3.toLowerCase(Locale.ENGLISH).equals("protected") && !var3.toLowerCase(Locale.ENGLISH).equals("noenter") && !var3.toLowerCase(Locale.ENGLISH).equals("noindications") && !var3.toLowerCase(Locale.ENGLISH).equals("noexit") && !var3.toLowerCase(Locale.ENGLISH).equals("nofploss")) {
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] mode must be either 'peace' or 'protected' or 'noenter' or 'noexit' or 'noindications' or 'nofploss' (without quotes)");
            } else {
               if (var3.toLowerCase(Locale.ENGLISH).equals("protected")) {
                  var5.protect(var6.toLowerCase(Locale.ENGLISH).equals("+"));
               } else if (var3.toLowerCase(Locale.ENGLISH).equals("peace")) {
                  var5.peace(var6.toLowerCase(Locale.ENGLISH).equals("+"));
               } else if (var3.toLowerCase(Locale.ENGLISH).equals("noenter")) {
                  var5.noEnter(var6.toLowerCase(Locale.ENGLISH).equals("+"));
               } else if (var3.toLowerCase(Locale.ENGLISH).equals("noexit")) {
                  var5.noExit(var6.toLowerCase(Locale.ENGLISH).equals("+"));
               } else if (var3.toLowerCase(Locale.ENGLISH).equals("noindications")) {
                  var5.noIndications(var6.toLowerCase(Locale.ENGLISH).equals("+"));
               } else if (var3.toLowerCase(Locale.ENGLISH).equals("nofploss")) {
                  var5.noFpLoss(var6.toLowerCase(Locale.ENGLISH).equals("+"));
               }

               this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] set " + var3 + " on " + var5.pos + " to '" + var6.toLowerCase(Locale.ENGLISH).equals("+") + "'");
            }
         }
      } catch (Exception var4) {
         var4.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] server could not load sector");
      }
   }

   private void countdown(GameServerState var1) {
      int var2 = (Integer)this.commandParams[0];
      StringBuffer var3 = new StringBuffer();

      for(int var4 = 1; var4 < this.commandParams.length; ++var4) {
         var3.append(this.commandParams[var4]);
         var3.append(" ");
      }

      var1.addCountdownMessage(var2, var3.toString());
   }

   private void dayTime(GameServerState var1) throws IOException {
      int var2;
      if ((var2 = (Integer)this.commandParams[0]) >= 0) {
         float var3 = (float)(var2 % 24) / 24.0F;
         var1.setServerTimeMod(var1.getController().getUniverseDayInMs() - (long)(var3 * (float)var1.getController().getUniverseDayInMs()));
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] daytime must be >= 0");
      }
   }

   private void debugFSMInfo() throws IOException {
      Boolean var1 = (Boolean)this.commandParams[0];
      ServerConfig.DEBUG_FSM_STATE.changeBooleanSetting(var1);
      ServerConfig.write();
      this.client.serverMessage("[ADMIN COMMAND] Set FSM dbug to: " + var1);
   }

   private void deleteFaction(GameServerState var1) throws IOException {
      Integer var2 = (Integer)this.commandParams[0];

      try {
         var1.getGameState().getFactionManager().removeFaction(var2);
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] deleted faction " + var2);
      } catch (FactionNotFoundException var3) {
         var3.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Faction Not Found: " + var2);
      }
   }

   private void destroyUID(GameServerState var1, boolean var2, boolean var3) throws IOException {
      String var4 = (String)this.commandParams[0];
      Sendable var10;
      if ((var10 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var4)) != null) {
         Iterator var5;
         if (var2 && var10 instanceof SegmentController) {
            SegmentController var8;
            (var8 = (SegmentController)var10).railController.destroyDockedRecursive();
            var5 = var8.getDockingController().getDockedOnThis().iterator();

            while(var5.hasNext()) {
               ElementDocking var6 = (ElementDocking)var5.next();
               this.client.serverMessage("[ADMIN COMMAND] DESTROY DOCK: " + var6.from.getSegment().getSegmentController());
               LogUtil.log().fine("[ADMIN COMMAND] destroying entity: " + var6);
               var6.from.getSegment().getSegmentController().markForPermanentDelete(true);
               var6.from.getSegment().getSegmentController().setMarkedForDeleteVolatile(true);
            }
         }

         if (var10 instanceof PlanetCore) {
            synchronized(var1.getLocalAndRemoteObjectContainer().getLocalObjects()) {
               var5 = var1.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

               while(var5.hasNext()) {
                  Sendable var11;
                  if ((var11 = (Sendable)var5.next()) instanceof Planet && ((Planet)var11).getCore() == var10) {
                     var10.markForPermanentDelete(true);
                     var10.setMarkedForDeleteVolatile(true);
                  }
               }
            }
         }

         Sector var9;
         if (var10 instanceof SimpleTransformableSendableObject && var10 instanceof TransientSegmentController && (var9 = var1.getUniverse().getSector(((SimpleTransformableSendableObject)var10).getSectorId())) != null) {
            var9.setTransientSector(false);
         }

         if (!var3) {
            this.client.serverMessage("[ADMIN COMMAND] DESTROY: " + var10);
            LogUtil.log().fine("[ADMIN COMMAND] destroying entity: " + var10);
            var10.markForPermanentDelete(true);
            var10.setMarkedForDeleteVolatile(true);
            return;
         }
      } else {
         this.client.serverMessage("[ERROR][ADMIN COMMAND] OBJECT TO DESTROY NOT FOUND");
      }

   }

   private void despawnAll(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      String var3 = ((String)this.commandParams[1]).trim().toLowerCase(Locale.ENGLISH);
      boolean var4 = (Boolean)this.commandParams[2];
      EntityTable.Despawn var5 = null;
      if (var3.equals("all")) {
         var5 = EntityTable.Despawn.ALL;
      }

      if (var3.equals("unused")) {
         var5 = EntityTable.Despawn.UNUSED;
      }

      if (var3.equals("used")) {
         var5 = EntityTable.Despawn.USED;
      }

      if (var5 != null) {
         try {
            Iterator var8 = var1.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

            while(true) {
               Sendable var6;
               do {
                  do {
                     if (!var8.hasNext()) {
                        var3 = DatabaseIndex.escape(var2) + "%";
                        System.err.println("[DESPAWN] using escaped matching string '" + var3 + "'");
                        int var9 = var1.getDatabaseIndex().getTableManager().getEntityTable().despawn(var3, var5, (Vector3i)null, var4 ? SimpleTransformableSendableObject.EntityType.SHIP : null);
                        this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] DESPAWNED " + var9 + " ENTITIES (MODE: " + var5.name() + ")");
                        return;
                     }
                  } while(!((var6 = (Sendable)var8.next()) instanceof SegmentController));
               } while(var4 && !(var6 instanceof Ship));

               if (DatabaseEntry.removePrefix(((SegmentController)var6).getUniqueIdentifier()).startsWith(var2)) {
                  var6.markForPermanentDelete(true);
                  var6.setMarkedForDeleteVolatile(true);
                  this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] ACTIVE DESPAWNED " + var6 + " ");
               }
            }
         } catch (SQLException var7) {
            var7.printStackTrace();
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] SQL EXCEPTION");
         }
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] mode '" + var5 + "' unknown. mist be 'used', 'unused', or 'all'");
      }
   }

   private void despawnSector(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      String var3 = ((String)this.commandParams[1]).trim().toLowerCase(Locale.ENGLISH);
      boolean var4 = (Boolean)this.commandParams[2];
      int var5 = (Integer)this.commandParams[3];
      int var6 = (Integer)this.commandParams[4];
      int var7 = (Integer)this.commandParams[5];
      EntityTable.Despawn var8 = null;
      if (var3.equals("all")) {
         var8 = EntityTable.Despawn.ALL;
      }

      if (var3.equals("unused")) {
         var8 = EntityTable.Despawn.UNUSED;
      }

      if (var3.equals("used")) {
         var8 = EntityTable.Despawn.USED;
      }

      Vector3i var11 = new Vector3i(var5, var6, var7);
      if (var8 != null) {
         try {
            Iterator var12 = var1.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

            while(true) {
               Sendable var14;
               do {
                  do {
                     do {
                        do {
                           if (!var12.hasNext()) {
                              String var13 = DatabaseIndex.escape(var2) + "%";
                              System.err.println("[DESPAWN] using escaped matching string '" + var13 + "'");
                              var6 = var1.getDatabaseIndex().getTableManager().getEntityTable().despawn(var13, var8, var11, var4 ? SimpleTransformableSendableObject.EntityType.SHIP : null);
                              this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] DESPAWNED " + var6 + " ENTITIES IN SECTOR: " + var11 + " (MODE: " + var8.name() + ")");
                              return;
                           }
                        } while(!((var14 = (Sendable)var12.next()) instanceof SegmentController));
                     } while(var4 && !(var14 instanceof Ship));
                  } while(var8 == EntityTable.Despawn.UNUSED && ((SegmentController)var14).getLastModifier().length() == 0);
               } while(var8 == EntityTable.Despawn.USED && ((SegmentController)var14).getLastModifier().length() > 0);

               if (DatabaseEntry.removePrefix(((SegmentController)var14).getUniqueIdentifier()).startsWith(var2) && ((SegmentController)var14).getSectorId() == var1.getUniverse().getSector(var11).getId()) {
                  var14.markForPermanentDelete(true);
                  var14.setMarkedForDeleteVolatile(true);
                  this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] ACTIVE DESPAWNED " + var14);
               }
            }
         } catch (SQLException var9) {
            var9.printStackTrace();
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] SQL EXCEPTION");
         } catch (Exception var10) {
            var10.printStackTrace();
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] COULD NOT LOAD SECTOR: " + var11);
         }
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] mode '" + var8 + "' unknown. mist be 'used', 'unused', or 'all'");
      }
   }

   private void removeSpawners(GameServerState var1) throws IOException {
      try {
         PlayerState var2 = var1.getPlayerFromStateId(this.client.getId());
         Sendable var4;
         if ((var4 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var2.getNetworkObject().selectedEntityId.get())) != null && var4 instanceof SimpleTransformableSendableObject) {
            ((SimpleTransformableSendableObject)var4).getSpawnController().removeAll();
         }

      } catch (PlayerNotFountException var3) {
         var3.printStackTrace();
         this.client.serverMessage("[ERROR][ADMIN COMMAND] Player not found for client " + this.client);
      }
   }

   private void softDespawn(GameServerState var1, Int2ObjectOpenHashMap var2, boolean var3, boolean var4) throws IOException {
      SegmentController var5;
      try {
         PlayerState var10 = var1.getPlayerFromStateId(this.client.getId());
         var5 = null;
         Sendable var11;
         if ((var11 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var10.getNetworkObject().selectedEntityId.get())) != null) {
            Iterator var6;
            if (var3 && var11 instanceof SegmentController) {
               (var5 = (SegmentController)var11).railController.destroyDockedRecursive();
               var6 = var5.getDockingController().getDockedOnThis().iterator();

               while(var6.hasNext()) {
                  ElementDocking var7 = (ElementDocking)var6.next();
                  this.client.serverMessage("[ADMIN COMMAND] DESTROY DOCK: " + var7.from.getSegment().getSegmentController());
                  LogUtil.log().fine("[ADMIN COMMAND] destroying entity: " + var7);
                  var7.from.getSegment().getSegmentController().setMarkedForDeleteVolatile(true);
               }
            }

            if (var11 instanceof PlanetCore) {
               synchronized(var1.getLocalAndRemoteObjectContainer().getLocalObjects()) {
                  var6 = var1.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

                  while(var6.hasNext()) {
                     Sendable var13;
                     if ((var13 = (Sendable)var6.next()) instanceof Planet && ((Planet)var13).getCore() == var11) {
                        var13.setMarkedForDeleteVolatile(true);
                     }
                  }
               }
            }

            Sector var12;
            if (var11 instanceof SimpleTransformableSendableObject && var11 instanceof TransientSegmentController && (var12 = var1.getUniverse().getSector(((SimpleTransformableSendableObject)var11).getSectorId())) != null) {
               var12.setTransientSector(false);
            }

            if (!var4) {
               this.client.serverMessage("[ADMIN COMMAND] DESTROY: " + var11);
               LogUtil.log().fine("[ADMIN COMMAND] destroying entity: " + var11);
               var11.setMarkedForDeleteVolatile(true);
               if (!var3 && var11 instanceof SegmentController) {
                  (var5 = (SegmentController)var11).railController.undockAllServer();
                  var6 = var5.railController.next.iterator();

                  while(var6.hasNext()) {
                     ((RailRelation)var6.next()).docked.getSegmentController().railController.disconnect();
                  }

                  return;
               }
            }
         } else {
            this.client.serverMessage("[ERROR][ADMIN COMMAND] OBJECT TO DESTROY NOT FOUND");
         }

      } catch (PlayerNotFountException var9) {
         var5 = null;
         var9.printStackTrace();
         this.client.serverMessage("[ERROR][ADMIN COMMAND] Player not found for client " + this.client);
      }
   }

   private void destroyEntity(GameServerState var1, Int2ObjectOpenHashMap var2, boolean var3, boolean var4) throws IOException {
      SegmentController var5;
      try {
         PlayerState var10 = var1.getPlayerFromStateId(this.client.getId());
         var5 = null;
         Sendable var11;
         if ((var11 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var10.getNetworkObject().selectedEntityId.get())) != null) {
            Iterator var6;
            if (var3 && var11 instanceof SegmentController) {
               (var5 = (SegmentController)var11).railController.destroyDockedRecursive();
               var6 = var5.getDockingController().getDockedOnThis().iterator();

               while(var6.hasNext()) {
                  ElementDocking var7 = (ElementDocking)var6.next();
                  this.client.serverMessage("[ADMIN COMMAND] DESTROY DOCK: " + var7.from.getSegment().getSegmentController());
                  LogUtil.log().fine("[ADMIN COMMAND] destroying entity: " + var7);
                  var7.from.getSegment().getSegmentController().markForPermanentDelete(true);
                  var7.from.getSegment().getSegmentController().setMarkedForDeleteVolatile(true);
               }
            }

            if (var11 instanceof PlanetCore) {
               synchronized(var1.getLocalAndRemoteObjectContainer().getLocalObjects()) {
                  var6 = var1.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

                  while(var6.hasNext()) {
                     Sendable var13;
                     if ((var13 = (Sendable)var6.next()) instanceof Planet && ((Planet)var13).getCore() == var11) {
                        var13.markForPermanentDelete(true);
                        var13.setMarkedForDeleteVolatile(true);
                     }
                  }
               }
            }

            Sector var12;
            if (var11 instanceof SimpleTransformableSendableObject && var11 instanceof TransientSegmentController && (var12 = var1.getUniverse().getSector(((SimpleTransformableSendableObject)var11).getSectorId())) != null) {
               var12.setTransientSector(false);
            }

            if (!var4) {
               if (!var3 && var11 instanceof SegmentController) {
                  ((SegmentController)var11).railController.undockAllServer();
               }

               this.client.serverMessage("[ADMIN COMMAND] DESTROY: " + var11);
               LogUtil.log().fine("[ADMIN COMMAND] destroying entity: " + var11);
               var11.markForPermanentDelete(true);
               var11.setMarkedForDeleteVolatile(true);
               return;
            }
         } else {
            this.client.serverMessage("[ERROR][ADMIN COMMAND] OBJECT TO DESTROY NOT FOUND");
         }

      } catch (PlayerNotFountException var9) {
         var5 = null;
         var9.printStackTrace();
         this.client.serverMessage("[ERROR][ADMIN COMMAND] Player not found for client " + this.client);
      }
   }

   private void enableDockingValidation(GameServerState var1) throws IOException {
      Boolean var2 = (Boolean)this.commandParams[0];
      ServerConfig.IGNORE_DOCKING_AREA.changeBooleanSetting(var2);
      ServerConfig.write();
      var1.getGameState().setIgnoreDockingArea(var2);
      this.client.serverMessage("[ADMIN COMMAND] Set DockingValidationIgnore to: " + var2);
   }

   private void enableSimulation(GameServerState var1) throws IOException {
      Boolean var2 = (Boolean)this.commandParams[0];
      ServerConfig.ENABLE_SIMULATION.changeBooleanSetting(var2);
      ServerConfig.write();
      this.client.serverMessage("[ADMIN COMMAND] Set SIMULATION to: " + var2);
   }

   public void execute(GameServerState var1) throws IOException, AdminCommandNotFoundException {
      synchronized(var1) {
         try {
            var1.setSynched();
            Int2ObjectOpenHashMap var3 = var1.getLocalAndRemoteObjectContainer().getLocalObjects();
            System.err.println("[ADMIN COMMAND] " + this.command.name() + " from " + this.client + " params: " + Arrays.toString(this.commandParams));
            LogUtil.log().fine("[ADMINCOMMAND] " + this.client.getPlayerName() + " used: '" + this.command.name() + "' with args " + Arrays.toString(this.commandParams));

            try {
               switch(this.command) {
               case FACTION_LIST:
                  this.listFactions(var1);
                  break;
               case FACTION_CREATE_AS:
                  this.addFaction(var1, true);
                  break;
               case FACTION_CREATE_AMOUNT:
                  this.addFactionAmount(var1, true);
                  break;
               case FACTION_CREATE:
                  this.addFaction(var1, false);
                  break;
               case SECTOR_SIZE:
                  this.sectorSize(var1);
                  break;
               case GOD_MODE:
                  this.godMode(var1);
                  break;
               case CREATIVE_MODE:
                  this.creativeMode(var1);
                  break;
               case INVISIBILITY_MODE:
                  this.invisibilityMode(var1);
                  break;
               case LIST_CONTROL_UNITS:
                  this.listControlUnits(var1);
                  break;
               case TINT:
                  this.tint(var1, false);
                  break;
               case TINT_NAME:
                  this.tint(var1, true);
                  break;
               case FACTION_DELETE:
                  this.deleteFaction(var1);
                  break;
               case FACTION_DEL_MEMBER:
                  this.factionRemoveMember(var1);
                  break;
               case FACTION_MOD_RELATION:
                  this.factionModRelation(var1);
                  break;
               case FACTION_EDIT:
                  this.factionEdit(var1);
                  break;
               case LOAD_SYSTEM:
                  this.loadSystem(var1);
                  break;
               case IGNORE_DOCKING_AREA:
                  this.enableDockingValidation(var1);
                  break;
               case START_COUNTDOWN:
                  this.countdown(var1);
                  break;
               case LOAD_SECTOR_RANGE:
                  this.loadRange(var1);
                  break;
               case DESTROY_ENTITY:
                  this.destroyEntity(var1, var3, false, false);
                  break;
               case DESTROY_ENTITY_DOCK:
                  this.destroyEntity(var1, var3, true, false);
                  break;
               case DESTROY_ENTITY_ONLY_DOCK:
                  this.destroyEntity(var1, var3, true, true);
                  break;
               case DESTROY_UID:
                  this.destroyUID(var1, false, false);
                  break;
               case DESTROY_UID_DOCKED:
                  this.destroyUID(var1, true, false);
                  break;
               case DESTROY_UID_ONLY_DOCKED:
                  this.destroyUID(var1, true, true);
                  break;
               case SHUTDOWN:
                  this.shutdown(var1);
                  break;
               case DAYTIME:
                  this.dayTime(var1);
                  break;
               case SIMULATION_AI_ENABLE:
                  this.enableSimulation(var1);
                  break;
               case SIMULATION_SPAWN_DELAY:
                  this.setSimulationDelay(var1);
                  break;
               case SIMULATION_INVOKE:
                  this.triggerSimulationPlanning(var1);
                  break;
               case SIMULATION_SEND_RESPONSE_FLEET:
                  this.triggerResponseFleet(var1);
                  break;
               case SEARCH:
                  this.search(var1);
                  break;
               case DESPAWN_ALL:
                  this.despawnAll(var1);
                  break;
               case SOFT_DESPAWN:
                  this.softDespawn(var1, var3, false, false);
                  break;
               case SOFT_DESPAWN_DOCK:
                  this.softDespawn(var1, var3, false, false);
                  break;
               case DESPAWN_SECTOR:
                  this.despawnSector(var1);
                  break;
               case SHOP_RESTOCK_UID:
                  this.restockUid(var1, false);
                  break;
               case SHOP_RESTOCK_FULL_UID:
                  this.restockUid(var1, true);
                  break;
               case FACTION_SET_ENTITY:
                  this.setFactionIdEntity(var1, false);
                  break;
               case FACTION_SET_ENTITY_UID:
                  this.setFactionIdEntity(var1, true);
                  break;
               case FACTION_SET_ALL_RELATIONS:
                  this.setAllRelations(var1);
                  break;
               case POPULATE_SECTOR:
                  this.populateSector(var1);
                  break;
               case CREATE_TRADE_PARTY:
                  this.createTradeParty(var1);
                  break;
               case SET_DEBUG_MODE:
                  this.setDebugMode(var1);
                  break;
               case CREATE_SPAWNER_TEST:
                  this.createCreatureSpawnerTest(var1);
                  break;
               case REMOVE_SPAWNERS:
                  this.removeSpawners(var1);
                  break;
               case GIVEID:
                  this.giveId(var1);
                  break;
               case SCAN:
                  this.scan(var1);
                  break;
               case FOG_OF_WAR:
                  this.fogOfWar(var1);
                  break;
               case MANAGER_CALCULATIONS:
                  this.manCalcs(var1);
                  break;
               case TP:
                  this.tp(var1);
                  break;
               case TP_TO:
                  this.tpTo(var1);
                  break;
               case CREATURE_RENAME:
                  this.creatureRename(var1);
                  break;
               case CREATURE_SCRIPT:
                  this.creatureScript(var1);
                  break;
               case CREATURE_ANIMATION_START:
                  this.creatureAnimation(var1, true);
                  break;
               case CREATURE_ANIMATION_STOP:
                  this.creatureAnimation(var1, false);
                  break;
               case CREATURE_IDLE:
                  this.testPath(var1, CreatureCommand.IDLE);
                  break;
               case CREATURE_GOTO:
                  this.testPath(var1, CreatureCommand.GOTO);
                  break;
               case CREATURE_SIT:
                  this.testPath(var1, CreatureCommand.SIT);
                  break;
               case CREATURE_STAND_UP:
                  this.testPath(var1, CreatureCommand.STAND_UP);
                  break;
               case CREATURE_ENTER_GRAVITY:
                  this.testPath(var1, CreatureCommand.GRAVITY);
                  break;
               case INITIATE_WAVE:
                  this.initiateWave(var1, var3, 5);
                  break;
               case GIVE:
                  this.give(var1);
                  break;
               case GIVE_METAITEM:
                  this.giveMetaItem(var1);
                  break;
               case GIVE_LASER_WEAPON:
                  this.giveLaserWeapon(var1, false);
                  break;
               case GIVE_LASER_WEAPON_OP:
                  this.giveLaserWeapon(var1, true);
                  break;
               case GIVE_HEAL_WEAPON:
                  this.giveHealWeapon(var1);
                  break;
               case PLAYER_PUT_INTO_ENTITY_UID:
                  this.putPlayerIntoEntity(var1);
                  break;
               case GIVE_MARKER_WEAPON:
                  this.giveMarkerWeapon(var1);
                  break;
               case GIVE_TRANSPORTER_MARKER_WEAPON:
                  this.giveTransporterMarkerWeapon(var1);
                  break;
               case GIVE_SNIPER_WEAPON:
                  this.giveSniperWeapon(var1, false);
                  break;
               case GIVE_SNIPER_WEAPON_OP:
                  this.giveSniperWeapon(var1, true);
                  break;
               case GIVE_TORCH_WEAPON:
                  this.giveTorchWeapon(var1, false);
                  break;
               case GIVE_TORCH_WEAPON_OP:
                  this.giveTorchWeapon(var1, true);
                  break;
               case GIVE_GRAPPLE_ITEM:
                  this.giveGrappleWeapon(var1, false);
                  break;
               case GIVE_GRAPPLE_ITEM_OP:
                  this.giveGrappleWeapon(var1, true);
                  break;
               case GIVE_ROCKET_LAUNCHER_OP:
                  this.giveRocketLauncherWeapon(var1, 2);
                  break;
               case GIVE_ROCKET_LAUNCHER_TEST:
                  this.giveRocketLauncherWeapon(var1, 1);
                  break;
               case GIVE_ROCKET_LAUNCHER_WEAPON:
                  this.giveRocketLauncherWeapon(var1, 0);
                  break;
               case GIVE_POWER_SUPPLY_WEAPON:
                  this.givePowerSupplyWeapon(var1);
                  break;
               case GIVE_RECIPE:
                  this.giveRecipe(var1);
                  break;
               case SET_SPAWN:
                  this.setSpawn(var1);
                  break;
               case SET_SPAWN_PLAYER:
                  this.setSpawnPlayer(var1);
                  break;
               case FACTION_RESET_ACTIVITY:
                  this.factionResetActivity(var1);
                  break;
               case SHIELD_DAMAGE:
                  this.shieldDamage(var1);
                  break;
               case DECAY:
                  this.decay(var1);
                  break;
               case STRUCTURE_SET_MINABLE:
                  this.minable(var1);
                  break;
               case STRUCTURE_SET_VULNERABLE:
                  this.vulnerable(var1);
                  break;
               case STRUCTURE_SET_MINABLE_UID:
                  this.minableUid(var1);
                  break;
               case STRUCTURE_SET_VULNERABLE_UID:
                  this.vulnerableUid(var1);
                  break;
               case FACTION_POINT_TURN:
                  this.factionPointTurn(var1);
                  break;
               case FACTION_POINT_SET:
                  this.factionPointSet(var1);
                  break;
               case FACTION_POINT_ADD:
                  this.factionPointAdd(var1);
                  break;
               case FACTION_POINT_PROTECT_PLAYER:
                  this.factionPointProtectPlayer(var1);
                  break;
               case PLAYER_SET_SPAWN_TO:
                  this.setPlayerSpawnTo(var1);
                  break;
               case PLAYER_GET_SPAWN:
                  this.getPlayerSpawn(var1);
                  break;
               case FACTION_POINT_GET:
                  this.factionPointGet(var1);
                  break;
               case GIVE_CREDITS:
                  this.giveCredits(var1);
                  break;
               case SET_INFINITE_INVENTORY_VOLUME:
                  this.giveInfiniteVolume(var1);
                  break;
               case GATE_DEST:
                  this.setJumpGateDest(var1);
                  break;
               case START_SHIP_AI:
                  this.startShipAI(var1);
                  break;
               case STOP_SHIP_AI:
                  this.stopShipAI(var1);
                  break;
               case CREATURE_ROAM:
                  this.testPath(var1, CreatureCommand.ROAM);
                  break;
               case GIVE_ALL_ITEMS:
                  this.addAllItems(var1);
                  break;
               case SHOP_RESTOCK:
                  this.restock(var1, false);
                  break;
               case SHOP_INFINITE:
                  this.shopSetInfinite(var1);
                  break;
               case SHOP_RESTOCK_FULL:
                  this.restock(var1, true);
                  break;
               case EXPLODE_PLANET_SECTOR:
                  this.explodePlanetSector(var1, true);
                  break;
               case PLAYER_SUSPEND_FACTION:
                  this.suspendFaction(var1);
                  break;
               case PLAYER_UNSUSPEND_FACTION:
                  this.unsuspendFaction(var1);
                  break;
               case EXPLODE_PLANET_SECTOR_NOT_CORE:
                  this.explodePlanetSector(var1, false);
                  break;
               case UPDATE_SHOP_PRICES:
                  this.updateAllShopPrices(var1);
                  break;
               case GIVE_CATEGORY_ITEMS:
                  this.addAllItemsCategory(var1);
                  break;
               case NPC_TURN_ALL:
                  this.npcTurn(var1, 0);
                  break;
               case FLEET_DEBUG_MOVE:
                  this.sendFleetDebug(var1);
                  break;
               case FLEET_DEBUG_STOP:
                  this.stopFleetDebug(var1);
                  break;
               case FLEET_SPEED:
                  this.fleetSpeed(var1);
                  break;
               case NPC_DEBUG_MODE:
                  this.npcDebugMode(var1);
                  break;
               case NPC_FLEET_LOADED_SPEED:
                  this.npcFleetLoadedSpeed(var1);
                  break;
               case NPC_KILL_RANDOM_IN_SYSTEM:
                  this.killRandomNPC(var1);
                  break;
               case NPC_BRING_DOWN_SYSTEM_STATUS:
                  this.killUntilStatusNPC(var1);
                  break;
               case NPC_SPAWN_FACTION:
                  this.npcSpawnFaction(var1, false);
                  break;
               case NPC_ADD_SHOP_OWNER:
                  this.npcModShopOwner(var1, true);
                  break;
               case NPC_REMOVE_SHOP_OWNER:
                  this.npcModShopOwner(var1, false);
                  break;
               case NPC_SPAWN_FACTION_POS_FIXED:
                  this.npcSpawnFaction(var1, true);
                  break;
               case NPC_REMOVE_FACTION:
                  this.npcRemoveFaction(var1);
                  break;
               case LIST_ADMINS:
                  this.listAdmins(var1);
                  break;
               case LIST_BANNED_IP:
                  this.listBannedIps(var1);
                  break;
               case LIST_BANNED_NAME:
                  this.listBannedNames(var1);
                  break;
               case LIST_BANNED_ACCOUNTS:
                  this.listBannedAccounts(var1);
                  break;
               case LIST_WHITELIST_ACCOUNTS:
                  this.listWhiteAccounts(var1);
                  break;
               case LIST_WHITELIST_IP:
                  this.listWhiteIps(var1);
                  break;
               case KICK_PLAYERS_OUT_OF_ENTITY:
                  this.forcePlayerExit(var1);
                  break;
               case SHIELD_OUTAGE:
                  this.shieldOutage(var1);
                  break;
               case ENTITY_SET_STRUCTURE_HP_PERCENT:
                  this.setStructureHP(var1);
                  break;
               case ENTITY_REBOOT:
                  this.resetStructureHP(var1);
                  break;
               case ENTITY_TRACK:
                  this.trackSelected(var1);
                  break;
               case ENTITY_TRACK_UID:
                  this.trackSelectedUID(var1);
                  break;
               case ENTITY_SET_CHECK_FLAG:
                  this.flagSelected(var1);
                  break;
               case ENTITY_SET_CHECK_FLAG_UID:
                  this.flagSelectedUID(var1);
                  break;
               case ENTITY_IS_CHECK_FLAG:
                  this.isFlagSelected(var1);
                  break;
               case ENTITY_IS_CHECK_FLAG_UID:
                  this.isFlagSelectedUID(var1);
                  break;
               case TEST_STATISTICS_SCRIPT:
                  this.testStatisticsScript(var1);
                  break;
               case POWER_OUTAGE:
                  this.powerOutage(var1);
                  break;
               case LIST_WHITELIST_NAME:
                  this.listWhiteNames(var1);
                  break;
               case ENTITY_INFO:
                  this.getEntityInfo(var1);
                  break;
               case SAVE:
                  this.saveShip(var1);
                  break;
               case SAVE_AS:
                  this.saveShipAs(var1);
                  break;
               case SAVE_UID:
                  this.saveShipUid(var1);
                  break;
               case SERVER_MESSAGE_BROADCAST:
                  this.serverMessageBroadcast(var1);
                  break;
               case SERVER_MESSAGE_TO:
                  this.serverMessageTo(var1);
                  break;
               case LIST_BLUEPRINTS:
                  this.listBlueprints(var1, false, false);
                  break;
               case LIST_BLUEPRINTS_VERBOSE:
                  this.listBlueprints(var1, false, true);
                  break;
               case LIST_BLUEPRINTS_BY_OWNER:
                  this.listBlueprints(var1, true, false);
                  break;
               case LIST_BLUEPRINTS_BY_OWNER_VERBOSE:
                  this.listBlueprints(var1, true, true);
                  break;
               case BREAK_SHIP:
                  this.testBreaking(var1);
                  break;
               case SPAWN_MOBS_LINE:
                  this.spawnMobsLine(var1);
                  break;
               case SPAWN_ITEM:
                  this.spawnItem(var1);
                  break;
               case SPAWN_MOBS:
                  this.spawnMobs(var1);
                  break;
               case SECTOR_CHMOD:
                  this.chmodSector(var1);
                  break;
               case TERRITORY_MAKE_UNCLAIMABLE:
                  this.territoryUnclaimable(var1);
                  break;
               case TERRITORY_RESET:
                  this.territoryReset(var1);
                  break;
               case DEBUG_FSM_INFO:
                  this.debugFSMInfo();
                  break;
               case LOAD:
                  this.loadShip(var1, BluePrintController.active, false, false);
                  break;
               case LOAD_DOCKED:
                  this.loadShip(var1, BluePrintController.active, true, false);
                  break;
               case LOAD_AS_FACTION:
                  this.loadShip(var1, BluePrintController.active, false, true);
                  break;
               case LOAD_AS_FACTION_DOCKED:
                  this.loadShip(var1, BluePrintController.active, true, true);
                  break;
               case LOAD_STATION_NEUTRAL:
                  this.loadShip(var1, BluePrintController.stationsNeutral, false, false);
                  break;
               case LOAD_STATION_PIRATE:
                  this.loadShip(var1, BluePrintController.stationsPirate, false, false);
                  break;
               case LOAD_STATION_TRADING_GUILD:
                  this.loadShip(var1, BluePrintController.stationsPirate, false, false);
                  break;
               case JUMP:
                  this.jump(var1);
                  break;
               case FLEET_INFO:
                  this.fleetInfo(var1);
                  break;
               case SIMULATION_INFO:
                  this.simulationInfo(var1);
                  break;
               case SIMULATION_CLEAR_ALL:
                  this.simulationClearAll(var1);
                  break;
               case FORCE_SAVE:
                  this.save(var1);
                  break;
               case EXPORT_SECTOR:
                  this.exportSectorNorm(var1);
                  break;
               case DELAY_SAVE:
                  this.delayAutosave(var1);
                  break;
               case IMPORT_SECTOR:
                  this.importSectorNorm(var1);
                  break;
               case EXPORT_SECTOR_BULK:
                  this.exportSectorBulk(var1);
                  break;
               case IMPORT_SECTOR_BULK:
                  this.importSectorBulk(var1);
                  break;
               case ADD_ADMIN:
                  this.addAdmin(var1);
                  break;
               case LIST_ADMIN_DENIED_COMMANDS:
                  this.listDeniedCommands(var1);
                  break;
               case ADD_ADMIN_DENIED_COMAND:
                  this.addOrRemoveAdminDeniedCommand(var1, true);
                  break;
               case REMOVE_ADMIN_DENIED_COMAND:
                  this.addOrRemoveAdminDeniedCommand(var1, false);
                  break;
               case REMOVE_ADMIN:
                  this.removeAdmin(var1);
                  break;
               case STATUS:
                  this.status(var1);
                  break;
               case SQL_INSERT_RETURN_GENERATED_KEYS:
                  this.sql(var1, true, true);
                  break;
               case SQL_QUERY:
                  this.sql(var1, false, false);
                  break;
               case SQL_UPDATE:
                  this.sql(var1, true, false);
                  break;
               case GIVE_LOOK:
                  this.giveLook(var1);
                  break;
               case AI_WEAPON_SWITCH_DELAY:
                  this.setAIWeaponSwitchDelay(var1);
                  break;
               case SHIP_INFO_UID:
                  this.shipInfoUid(var1);
                  break;
               case SHIP_INFO_NAME:
                  this.shipInfoName(var1);
                  break;
               case SHIP_INFO_SELECTED:
                  this.shipInfoSelected(var1);
                  break;
               case SECTOR_INFO:
                  this.sectorInfo(var1);
                  break;
               case GIVE_SLOT:
                  this.giveSlot(var1);
                  break;
               case BAN:
                  this.banName(var1);
                  break;
               case WHITELIST_ACTIVATE:
                  this.activateWhiteList(var1);
                  break;
               case BAN_IP:
                  this.banIp(var1, false, false);
                  break;
               case FACTION_SET_ENTITY_RANK:
                  this.setEntityPermission(var1);
                  break;
               case FACTION_SET_ENTITY_RANK_UID:
                  this.setEntityPermissionUID(var1);
                  break;
               case PLAYER_GET_INVENTORY:
                  this.getPlayerInventory(var1);
                  break;
               case PLAYER_GET_BLOCK_AMOUNT:
                  this.getPlayerBlockAmount(var1);
                  break;
               case BAN_IP_BY_PLAYERNAME:
                  this.banIp(var1, true, false);
                  break;
               case BAN_ACCOUNT:
                  this.banAccount(var1, false, false);
                  break;
               case BAN_ACCOUNT_BY_PLAYERNAME:
                  this.banAccount(var1, true, false);
                  break;
               case BAN_IP_TEMP:
                  this.banIp(var1, false, true);
                  break;
               case BAN_IP_BY_PLAYERNAME_TEMP:
                  this.banIp(var1, true, true);
                  break;
               case BAN_ACCOUNT_TEMP:
                  this.banAccount(var1, false, true);
                  break;
               case BAN_ACCOUNT_BY_PLAYERNAME_TEMP:
                  this.banAccount(var1, true, true);
                  break;
               case CLEAR_MINES_HERE:
                  this.clearMinesHere(var1);
                  break;
               case CLEAR_MINES_SECTOR:
                  this.clearMinesSector(var1, true, true);
                  break;
               case UNBAN_ACCOUNT:
                  this.unBanAccount(var1);
                  break;
               case WHITELIST_ACCOUNT:
                  this.whitelistAccount(var1, false);
                  break;
               case WHITELIST_NAME:
                  this.whitelistName(var1, false);
                  break;
               case WHITELIST_IP:
                  this.whitelistIp(var1, false);
               case SIM_FACTION_SPAWN_TEST:
                  break;
               case WHITELIST_ACCOUNT_TEMP:
                  this.whitelistAccount(var1, true);
                  break;
               case WHITELIST_NAME_TEMP:
                  this.whitelistName(var1, true);
                  break;
               case WHITELIST_IP_TEMP:
                  this.whitelistIp(var1, true);
                  break;
               case FACTION_SET_ID_MEMBER:
                  this.setFactionId(var1);
                  break;
               case PLAYER_INFO:
                  this.playerInfo(var1);
                  break;
               case PLAYER_LIST:
                  this.playerList(var1);
                  break;
               case PLAYER_PROTECT:
                  this.playerProtect(var1);
                  break;
               case PLAYER_UNPROTECT:
                  this.playerUnprotect(var1);
                  break;
               case REFRESH_SERVER_MSG:
                  this.refreshServerMessage(var1);
                  break;
               case SPAWN_CREATURE:
                  this.spawnCreature(var1);
                  break;
               case SPAWN_CREATURE_MASS:
                  this.spawnMassCreature(var1);
                  break;
               case UNBAN_NAME:
                  this.unBanName(var1);
                  break;
               case CLEAR_OVERHEATING:
                  this.clearOverheating(var1, false);
                  break;
               case CLEAR_OVERHEATING_SECTOR:
                  this.clearOverheatingSector(var1);
                  break;
               case CLEAR_SYSTEM_SHIP_SPAWNS:
                  this.clearSystemShipSpawns(var1, false);
                  break;
               case CLEAR_SYSTEM_SHIP_SPAWNS_ALL:
                  this.clearSystemShipSpawns(var1, true);
                  break;
               case CLEAR_OVERHEATING_ALL:
                  this.clearOverheating(var1, true);
                  break;
               case UNBAN_IP:
                  this.unBanIp(var1);
                  break;
               case KICK:
                  this.kick(var1, false, 0);
                  break;
               case KICK_REASON:
                  this.kick(var1, true, 1);
                  break;
               case RESTRUCT_AABB:
                  this.resAABB(var1);
                  break;
               case EXECUTE_ENTITY_EFFECT:
                  this.executeGraphicsEffect(var1);
                  break;
               case KILL_CHARACTER:
                  this.killCharacter(var1);
                  break;
               case TELEPORT_SELF_HOME:
                  this.teleportSelfHome(var1);
                  break;
               case TELEPORT_SELF_TO:
                  this.teleportSelfTo(var1);
                  break;
               case TELEPORT_UID_TO:
                  this.teleportUidTo(var1, var3);
                  break;
               case TELEPORT_TO:
                  this.teleportTo(var1, var3);
                  break;
               case TELEPORT_SELECTED_TO:
                  this.teleportSelectedTo(var1, var3);
                  break;
               case CHANGE_SECTOR:
                  this.changeSector(var1, false);
                  break;
               case CHANGE_SECTOR_COPY:
                  this.changeSector(var1, true);
                  break;
               case CHANGE_SECTOR_FOR:
                  this.changeSectorPlayer(var1, false);
                  break;
               case CHANGE_SECTOR_FOR_UID:
                  this.changeSectorUid(var1, false);
                  break;
               case CHANGE_SECTOR_SELECTED:
                  this.changeSectorSelected(var1, false);
                  break;
               case CHANGE_SECTOR_FOR_COPY:
                  this.changeSectorPlayer(var1, true);
                  break;
               case SET_GLOBAL_SPAWN:
                  this.setGlobalSpawn(var1);
                  break;
               case SPAWN_ENTITY:
                  this.spawnEntity(var1);
                  break;
               case SPAWN_ENTITY_POS:
                  this.spawnEntityPos(var1);
                  break;
               case REPAIR_SECTOR:
                  this.repairSector(var1);
                  break;
               case FACTION_CHECK:
                  this.checkFactions(var1);
                  break;
               case FACTION_REINSTITUTE:
                  this.factionsReinstitude(var1);
                  break;
               case FACTION_MOD_MEMBER:
                  this.factionModMember(var1);
                  break;
               case MISSILE_DEFENSE_FRIENDLY_FIRE:
                  this.handleServerConfigBoolean(var1, ServerConfig.MISSILE_DEFENSE_FRIENDLY_FIRE);
                  break;
               case FACTION_JOIN_ID:
                  this.joinFaction(var1);
                  break;
               case FACTION_LIST_MEMBERS:
                  this.checkFactionMembers(var1);
                  break;
               case LAST_CHANGED:
                  this.showModifierAndSpawner(var1);
                  break;
               case SHIELD_REGEN:
                  this.entitySetShieldRegen(var1);
                  break;
               case POWER_REGEN:
                  this.entitySetPowerRegen(var1);
                  break;
               case POWER_DRAIN:
                  this.powerDrain(var1);
                  break;
               case SPAWN_PARTICLE:
                  this.spawnParticle(var1);
                  break;
               case BLUEPRINT_DELETE:
                  this.blueprintDelete(var1);
                  break;
               case BLUEPRINT_INFO:
                  this.blueprintInfo(var1);
                  break;
               case BLUEPRINT_SET_OWNER:
                  this.blueprintSetOwner(var1);
                  break;
               case ENTITY_INFO_BY_PLAYER_UID:
                  this.entityInfoByPlayerUID(var1);
                  break;
               case ENTITY_INFO_NAME:
                  this.shipInfoName(var1);
                  break;
               case ENTITY_INFO_UID:
                  this.shipInfoUid(var1);
                  break;
               case KICK_PLAYERS_OUT_OF_ENTITY_UID:
                  this.kickPlayersOutOfEntityByUID(var1, false);
                  break;
               case KICK_PLAYERS_OUT_OF_ENTITY_UID_DOCK:
                  this.kickPlayersOutOfEntityByUID(var1, true);
                  break;
               case KICK_PLAYER_NAME_OUT_OF_ENTITY:
                  this.kickPlayerUIDOutOfEntity(var1);
                  break;
               case RAIL_RESET_ALL:
                  this.resetDock(var1, true);
                  break;
               case RAIL_RESET:
                  this.resetDock(var1, false);
                  break;
               case SET_WEAPON_RANGE_REFERENCE:
                  this.setWeaponRangeReference(var1);
                  break;
               case RESET_REPRAIR_DELAY:
                  this.resetRepairTimeout(var1);
                  break;
               case RESET_INTEGRITY_DELAY:
                  this.resetIntegrityTimeout(var1);
                  break;
               case MISSILE_TARGET_PREDICTION:
                  this.setMissileTargetPrediction(var1);
                  break;
               case GIVE_UID_STORAGE_ID:
                  this.putIntoInventory(var1);
                  break;
               case ENTITY_GET_INVENTORY:
                  this.readFromInventory(var1);
                  break;
               default:
                  throw new AdminCommandNotFoundException(this.command.name());
               }
            } catch (IndexOutOfBoundsException var8) {
               String var12 = "Needed: ";
               if (this.command.getRequiredParameterCount() != this.command.getTotalParameterCount()) {
                  var12 = var12 + "Minimum of " + this.command.getRequiredParameterCount();
               } else {
                  var12 = var12 + this.command.getTotalParameterCount();
               }

               this.client.serverMessage("[ADMIN COMMAND] [ERROR] Wrong amount of arguments. " + var12);
            } catch (AdminCommandNotFoundException var9) {
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] Command known, but not executable: " + var9.getMessage() + ". Please send in report!");
            }
         } finally {
            var1.setUnsynched();
         }
      }

      this.client.executedAdminCommand();
   }

   private void setMissileTargetPrediction(GameServerState var1) throws IOException {
      float var2 = (Float)this.commandParams[0];
      ServerConfig.MISSILE_TARGET_PREDICTION_SEC.setCurrentState(var2);
      ServerConfig.write();
      this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Missile target prediction set to " + ServerConfig.MISSILE_TARGET_PREDICTION_SEC.getCurrentState() + " ticks (" + (int)((Float)ServerConfig.MISSILE_TARGET_PREDICTION_SEC.getCurrentState() * (float)TargetChasingMissile.UPDATE_MS) + "ms)");
   }

   private void setAIWeaponSwitchDelay(GameServerState var1) throws IOException {
      int var2 = (Integer)this.commandParams[0];
      var1.getGameState().setAIWeaponSwitchDelayMS(var2);
      ServerConfig.AI_WEAPON_SWITCH_DELAY.setCurrentState(var2);
      ServerConfig.write();
      this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] AI Weapon switch delay set to " + var2 + "ms");
   }

   private void putPlayerIntoEntity(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      String var3 = (String)this.commandParams[1];
      Sector var4;
      if ((var4 = this.loadUID(var1, var3)) == null) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Either UID doesn't exist or UID sector is not spawned anywhere in the universe");
      }

      try {
         PlayerState var5 = var1.getPlayerFromName(var2);

         try {
            SimpleTransformableSendableObject var6;
            if ((var6 = var5.getFirstControlledTransformable()) instanceof SegmentController) {
               this.kickPlayersOut(var1, (SegmentController)var6, false, var5);
            }
         } catch (PlayerControlledTransformableNotFound var7) {
         }

         var5.forcePlayerIntoEntity(var4, var3);
      } catch (PlayerNotFountException var8) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player " + var2 + " not online");
      }
   }

   private Sector loadUID(GameServerState var1, String var2) {
      Sendable var3;
      if ((var3 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var2)) instanceof SimpleTransformableSendableObject) {
         return var1.getUniverse().getSector(((SimpleTransformableSendableObject)var3).getSectorId());
      } else {
         DatabaseEntry var5;
         if ((var5 = var1.getDatabaseIndex().getTableManager().getEntityTable().getEntryForFullUID(var2)) != null) {
            try {
               return var1.getUniverse().getSector(var5.sectorPos);
            } catch (IOException var4) {
               var4.printStackTrace();
            }
         }

         return null;
      }
   }

   private void getPlayerBlockAmount(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      int var3 = (Integer)this.commandParams[1];

      PlayerState var5;
      try {
         var5 = var1.getPlayerFromName(var2);
      } catch (PlayerNotFountException var4) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player " + var2 + " not online");
         return;
      }

      Inventory var6 = var5.getInventory();
      if (ElementKeyMap.isValidType(var3)) {
         int var7 = var6.getOverallQuantity((short)var3);
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] " + var2 + ": " + var7 + " of " + ElementKeyMap.toString(var3));
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] invalid block type");
      }
   }

   private void getPlayerInventory(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      PlayerState var6;
      try {
         var6 = var1.getPlayerFromName(var2);
      } catch (PlayerNotFountException var5) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player " + var2 + " not online");
         return;
      }

      Inventory var7 = var6.getInventory();
      this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Listing player " + var2 + " personal inventory START");
      Iterator var3 = var7.getSlots().iterator();

      while(var3.hasNext()) {
         int var4 = (Integer)var3.next();
         InventorySlot var8;
         if ((var8 = var7.getSlot(var4)) != null) {
            this.printSlot(var2, 0, var8);
         }
      }

      this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Listing player " + var2 + " personal inventory END.");
   }

   private void printSlot(String var1, int var2, InventorySlot var3) throws IOException {
      if (var3 != null) {
         StringBuffer var4 = new StringBuffer();

         for(int var5 = 0; var5 < var2; ++var5) {
            var4.append("-");
         }

         this.client.serverMessage("[INVENTORY] " + var1 + ": " + var4 + " SLOT: " + var3.slot + "; MULTI: " + var3.isMultiSlot() + "; TYPE: " + var3.getType() + "; META: " + var3.metaId + "; COUNT: " + var3.count());
         if (var3.isMultiSlot()) {
            Iterator var6 = var3.getSubSlots().iterator();

            while(var6.hasNext()) {
               var3 = (InventorySlot)var6.next();
               this.printSlot(var1, var2 + 1, var3);
            }
         }
      }

   }

   private void setWeaponRangeReference(GameServerState var1) throws IOException {
      float var2 = (Float)this.commandParams[0];
      var1.getGameState().setWeaponRangeReference(var2);
      this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Weapon Reference Range set to " + (int)var2 + " meters");
   }

   private void kickPlayerUIDOutOfEntity(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      PlayerState var3;
      try {
         var3 = var1.getPlayerFromName(var2);
      } catch (PlayerNotFountException var6) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player " + var2 + " not online");
         return;
      }

      try {
         SimpleTransformableSendableObject var4;
         if ((var4 = var3.getFirstControlledTransformable()) instanceof SegmentController) {
            this.kickPlayersOut(var1, (SegmentController)var4, false, var3);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] Player " + var2 + " is currently not controlling any entity");
         }
      } catch (PlayerControlledTransformableNotFound var5) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Player " + var2 + " is currently not controlling any entity");
      }
   }

   private void kickPlayersOutOfEntityByUID(GameServerState var1, boolean var2) throws IOException {
      String var3 = (String)this.commandParams[0];
      Sendable var4;
      if ((var4 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var3)) instanceof SegmentController) {
         this.kickPlayersOut(var1, (SegmentController)var4, var2, (PlayerState)null);
         if (var2) {
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Kicking players out of entity " + var3);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Kicking players out of entity and all related docks and mothership of " + var3);
         }
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] entity " + var3 + " not found in currently loaded objects");
      }
   }

   public void kickPlayersOut(GameServerState var1, SegmentController var2, boolean var3, PlayerState var4) {
      if (var4 == null) {
         var2.kickAllPlayersOutServer();
      } else {
         var2.kickPlayerOutServer(var4);
      }

      if (var3) {
         Iterator var6 = var2.railController.next.iterator();

         while(var6.hasNext()) {
            RailRelation var5 = (RailRelation)var6.next();
            this.kickPlayersOut(var1, var5.docked.getSegmentController(), var3, var4);
         }
      }

   }

   private void entityInfoByPlayerUID(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         PlayerState var3;
         try {
            var3 = var1.getPlayerFromName(var2);
         } catch (PlayerNotFountException var4) {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] player " + var2 + " not online");
            return;
         }

         SimpleTransformableSendableObject var6;
         if ((var6 = var3.getFirstControlledTransformable()) != null) {
            this.printInfo(var1, var6);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] Player is not controlling any entity");
         }
      } catch (PlayerControlledTransformableNotFound var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      }
   }

   private void clearMinesSector(GameServerState var1, boolean var2, boolean var3) throws IOException {
      int var5 = (Integer)this.commandParams[0];
      int var6 = (Integer)this.commandParams[1];
      int var4 = (Integer)this.commandParams[2];
      this.clearMines(var1, var5, var6, var4);
   }

   private void clearMines(GameServerState var1, int var2, int var3, int var4) throws IOException {
      var1.getController().getMineController().clearMinesInSectorServer(var2, var3, var4);
      this.client.serverMessage("Mines cleared in " + var2 + ", " + var3 + ", " + var4 + "!");
   }

   private void clearMinesHere(GameServerState var1) throws IOException {
      try {
         PlayerState var2 = var1.getPlayerFromName(this.client.getPlayerName());
         this.clearMines(var1, var2.getCurrentSector().x, var2.getCurrentSector().y, var2.getCurrentSector().z);
      } catch (PlayerNotFountException var3) {
         var3.printStackTrace();
         this.client.serverMessage("Player not found!");
      }
   }

   private void fleetInfo(GameServerState var1) throws IOException {
      this.client.serverMessage("[ADMIN COMMAND] [FLEETINFO] Cached Fleets: " + var1.getFleetManager().fleetCache.size());
   }

   private void triggerResponseFleet(GameServerState var1) throws IOException {
      try {
         PlayerState var2 = var1.getPlayerFromStateId(this.client.getId());
         var1.getSimulationManager().sendToAttackSpecific(var2.getFirstControlledTransformable(), -2, 1);
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] SENT RESPONSE FLEET TO YOUR POSITION");
      } catch (PlayerControlledTransformableNotFound var3) {
         var3.printStackTrace();
      } catch (PlayerNotFountException var4) {
         var4.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Player not found (client has no player attached)");
      }
   }

   private void sql(final GameServerState var1, boolean var2, boolean var3) throws IOException {
      String[] var4 = ServerConfig.SQL_PERMISSION.getCurrentState().toString().split(",");
      ObjectOpenHashSet var5 = new ObjectOpenHashSet();
      int var6 = (var4 = var4).length;

      for(int var7 = 0; var7 < var6; ++var7) {
         String var8 = var4[var7];
         var5.add(var8.toLowerCase(Locale.ENGLISH).trim());
      }

      if (!var5.contains(this.client.getPlayerName().toLowerCase(Locale.ENGLISH)) && !(this.client instanceof AdminLocalClient) && !(this.client instanceof AdminRemoteClient)) {
         this.client.serverMessage("ERROR: YOU DO NOT HAVE PERMISSIONS TO DO SQL QUERIES!");
      } else {
         String var10 = (String)this.commandParams[0];
         final StringBuffer var11 = new StringBuffer();
         final int var12;
         synchronized(var1) {
            var1.getDatabaseIndex().adminSql(var10, var2, var3, var1, this.client, var11);
            var12 = this.sqlT++;
         }

         this.client.blockFromLogout();
         System.err.println("NOW BLOCKED :::: ");
         (new Thread() {
            public void run() {
               try {
                  System.err.println("NOW BLOCKED :::: THREAD START");
                  BufferedReader var1x = new BufferedReader(new StringReader(var11.toString()));
                  synchronized(var1) {
                     AdminCommandQueueElement.this.client.serverMessage("---------- SQL QUERY " + var12 + " BEGIN ----------");
                  }

                  String var2;
                  while((var2 = var1x.readLine()) != null) {
                     synchronized(var1) {
                        AdminCommandQueueElement.this.client.serverMessage("SQL#" + var12 + ": " + var2);
                     }
                  }

                  synchronized(var1) {
                     AdminCommandQueueElement.this.client.serverMessage("---------- SQL QUERY " + var12 + " END ----------");
                  }

                  var1x.close();
                  System.err.println("NOW BLOCKED :::: THREAD END");
                  return;
               } catch (Exception var12x) {
                  var12x.printStackTrace();
               } finally {
                  AdminCommandQueueElement.this.client.disconnectNormal();
               }

            }
         }).start();
      }
   }

   private void npcModShopOwner(GameServerState var1, boolean var2) throws IOException {
      String var6 = (String)this.commandParams[0];
      String var3 = ServerConfig.NPC_DEBUG_SHOP_OWNERS.getCurrentState().toString().trim();
      ObjectOpenHashSet var4 = new ObjectOpenHashSet();
      if (var3.length() > 0) {
         String[] var7 = var3.split(",");

         for(int var5 = 0; var5 < var7.length; ++var5) {
            var4.add(var7[var5].trim().toLowerCase(Locale.ENGLISH));
         }
      }

      if (var2) {
         var4.add(var6.toLowerCase(Locale.ENGLISH).trim());
      } else {
         var4.remove(var6.toLowerCase(Locale.ENGLISH).trim());
      }

      Iterator var8 = var4.iterator();
      StringBuffer var9 = new StringBuffer();

      while(var8.hasNext()) {
         var6 = (String)var8.next();
         var9.append(var6);
         if (var8.hasNext()) {
            var9.append(", ");
         }
      }

      ServerConfig.write();
      this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Debug Shop Owners Now: " + var4);
   }

   private void manCalcs(GameServerState var1) throws IOException {
      boolean var2 = (Boolean)this.commandParams[0];
      ServerConfig.MANAGER_CALC_CANCEL_ON.setCurrentState(var2);
      ServerConfig.write();
      this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Manager Calculations set to: " + ServerConfig.MANAGER_CALC_CANCEL_ON.getCurrentState() + " (reload sector to take effect)");
   }

   private void fogOfWar(GameServerState var1) throws IOException {
      boolean var2 = (Boolean)this.commandParams[0];
      ServerConfig.USE_FOW.setCurrentState(var2);
      ServerConfig.write();
      this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Fog of war set to: " + ServerConfig.USE_FOW.getCurrentState());
   }

   private void scan(GameServerState var1) throws IOException {
      int var2 = (Integer)this.commandParams[0];
      int var3 = (Integer)this.commandParams[1];
      int var4 = (Integer)this.commandParams[2];

      try {
         PlayerState var6 = var1.getPlayerFromStateId(this.client.getId());
         Vector3i var7 = new Vector3i(var2, var3, var4);
         var6.getFogOfWar().scan(var7);
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Scanned System: " + var7 + " for " + var6.getName());
      } catch (PlayerNotFountException var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Player not found (client has no player attached)");
      }
   }

   private void npcDebugMode(GameServerState var1) throws IOException {
      boolean var2 = (Boolean)this.commandParams[0];
      ServerConfig.NPC_DEBUG_MODE.setCurrentState(var2);
      ServerConfig.write();
      this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] NPC Debug Mode set to: " + ServerConfig.NPC_DEBUG_MODE.getCurrentState());
   }

   private void npcFleetLoadedSpeed(GameServerState var1) throws IOException {
      float var2 = (Float)this.commandParams[0];
      ServerConfig.NPC_LOADED_SHIP_MAX_SPEED_MULT.setCurrentState(var2);
      ServerConfig.write();
      this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] NPC Loaded Ship Speed set to: " + ServerConfig.NPC_LOADED_SHIP_MAX_SPEED_MULT.getCurrentState());
   }

   private void fleetSpeed(GameServerState var1) throws IOException {
      int var2 = (Integer)this.commandParams[0];
      ServerConfig.FLEET_OUT_OF_SECTOR_MOVEMENT.setCurrentState(var2);
      ServerConfig.write();
      this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Fleet speed set to: " + ServerConfig.FLEET_OUT_OF_SECTOR_MOVEMENT.getCurrentState() + " milliseconds / sector");
   }

   private void npcRemoveFaction(GameServerState var1) throws IOException {
      int var2 = (Integer)this.commandParams[0];
      Faction var4;
      if ((var4 = var1.getFactionManager().getFaction(var2)) != null && var4.isNPC()) {
         NPCFaction var5;
         (var5 = (NPCFaction)var4).removeCompletely();

         try {
            var1.getGameState().getFactionManager().removeFaction(var5.getIdFaction());
         } catch (FactionNotFoundException var3) {
            var3.printStackTrace();
         }

         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] faction removed!");
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] faction not found!");
      }
   }

   private void npcSpawnFaction(GameServerState var1, boolean var2) throws IOException {
      String var3 = (String)this.commandParams[0];
      String var4 = (String)this.commandParams[1];
      String var5 = (String)this.commandParams[2];
      int var6 = (Integer)this.commandParams[3];
      Vector3i var7 = null;
      if (var2) {
         int var8 = (Integer)this.commandParams[4];
         int var11 = (Integer)this.commandParams[5];
         int var9 = (Integer)this.commandParams[6];
         var7 = new Vector3i(var8, var11, var9);
      }

      NPCFactionSpawn var12 = NPCStartupConfig.createSpawn(var3, var4, !var2, var6, var7, var5);
      Galaxy var13;
      if (var2) {
         var13 = var1.getUniverse().getGalaxyFromSystemPos(var7);
      } else {
         var13 = var1.getUniverse().getGalaxyFromSystemPos(new Vector3i(0, 0, 0));
      }

      try {
         var13.getNpcFactionManager().add(var13, var12);
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Spawned new faction!");
      } catch (NPCSpawnException var10) {
         var10.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Exception: " + var10.getMessage() + "! Check logs for exact error!");
      }
   }

   private void killUntilStatusNPC(GameServerState var1) throws IOException {
      try {
         float var3 = (Float)this.commandParams[0] / 100.0F;
         Vector3i var2 = var1.getPlayerFromStateId(this.client.getId()).getCurrentSystem();
         StellarSystem var4 = var1.getUniverse().getStellarSystemFromStellarPos(var2);
         Faction var5 = var1.getFactionManager().getFaction(var4.getOwnerFaction());

         assert var4.getPos().equals(var2);

         if (var5 != null && var5 instanceof NPCFaction) {
            NPCSystem var8 = ((NPCFaction)var5).structure.getSystem(var2);

            assert var8.system.equals(var2);

            int var7;
            for(var7 = 0; (double)var3 < var8.status; ++var7) {
               if (var8.getContingent().killRandomSpawned() == null) {
                  var8.getContingent().killRandomNonSpanwed();
               }
            }

            var1.getGameState().getNetworkObject().npcSystemBuffer.add(new RemoteNPCSystem(var8, true));
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] killed: " + var7 + " in " + var8.system + "; New Status: " + var8.status * 100.0D + "% ");
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] System not owned by NPC faction");
         }
      } catch (PlayerNotFountException var6) {
         var6.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var6.getMessage());
      }
   }

   private void killRandomNPC(GameServerState var1) throws IOException {
      try {
         Vector3i var2 = var1.getPlayerFromStateId(this.client.getId()).getCurrentSystem();
         StellarSystem var3 = var1.getUniverse().getStellarSystemFromStellarPos(var2);
         Faction var5;
         if ((var5 = var1.getFactionManager().getFaction(var3.getOwnerFaction())) != null && var5 instanceof NPCFaction) {
            NPCEntityContainer.NPCEntity var6 = ((NPCFaction)var5).structure.getSystem(var2).getContingent().killRandomSpawned();
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] killed: " + var6);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] System not owned by NPC faction");
         }
      } catch (PlayerNotFountException var4) {
         var4.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      }
   }

   private void npcTurn(GameServerState var1, int var2) throws IOException {
      if (var2 == 0) {
         Iterator var6 = var1.getFactionManager().getFactionMap().values().iterator();

         while(var6.hasNext()) {
            Faction var5;
            if ((var5 = (Faction)var6.next()) instanceof NPCFaction) {
               this.npcTurn(var1, var5.getIdFaction());
            }
         }

      } else {
         Faction var3;
         if ((var3 = var1.getFactionManager().getFaction(var2)) != null && var3 instanceof NPCFaction) {
            NPCFaction var4 = (NPCFaction)var3;
            var1.getFactionManager().scheduleTurn(var4);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] faction unknown or not an NPC " + var2 + " (" + var3 + ")");
         }
      }
   }

   private void setStructureHP(GameServerState var1) throws IOException {
      SimpleTransformableSendableObject var2;
      if ((var2 = this.getSelectedObject(var1)) != null && var2 instanceof SegmentController) {
         SegmentController var3;
         (var3 = (SegmentController)var2).getHpController().setHpPercent((Float)this.commandParams[0]);
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] set hp to " + var3.getHpController().getHp() + "/" + var3.getHpController().getMaxHp());
      }

   }

   private void testStatisticsScript(GameServerState var1) throws IOException {
      SimpleTransformableSendableObject var6;
      if ((var6 = this.getSelectedObject(var1)) != null && var6 instanceof Ship) {
         Ship var7 = (Ship)var6;

         try {
            EntityIndexScore var2 = var7.getManagerContainer().getStatisticsManager().calculateIndex();
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] script executed: index (long version): " + var2.toString());
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] script executed: index: " + var2.toShortString());
            return;
         } catch (Exception var5) {
            var5.printStackTrace();
            StringWriter var3 = new StringWriter();
            PrintWriter var4 = new PrintWriter(var3);
            var5.printStackTrace(var4);
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] script failed: " + var7.getManagerContainer().getStatisticsManager().getScript() + "\n" + var3.toString());
         }
      }

   }

   private void resetStructureHP(GameServerState var1) throws IOException {
      SimpleTransformableSendableObject var2;
      if ((var2 = this.getSelectedObject(var1)) != null && var2 instanceof SegmentController) {
         ((SegmentController)var2).getHpController().reboot(true);
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] rebooted");
      }

   }

   private void trackSelectedUID(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      boolean var3 = (Boolean)this.commandParams[1];
      Sendable var4;
      if ((var4 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var2)) instanceof SimpleTransformableSendableObject) {
         this.track(var1, var3, (SimpleTransformableSendableObject)var4);
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] entity " + var2 + " not found in currently loaded objects");
      }
   }

   private void trackSelected(GameServerState var1) throws IOException {
      (Boolean)this.commandParams[0];
      SimpleTransformableSendableObject var2;
      if ((var2 = this.getSelectedObject(var1)) instanceof SegmentController) {
         if (((SegmentController)var2).lastAdminCheckFlag != 0L) {
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] entity " + ((SegmentController)var2).getName() + " was flagged: " + new Date(((SegmentController)var2).lastAdminCheckFlag));
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] entity " + ((SegmentController)var2).getName() + " not flagged");
         }
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] invalid selected object");
      }
   }

   private void isFlagSelectedUID(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      (Boolean)this.commandParams[1];
      Sendable var3;
      if ((var3 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var2)) instanceof SegmentController) {
         if (((SegmentController)var3).lastAdminCheckFlag != 0L) {
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] entity " + var2 + " was flagged: " + new Date(((SegmentController)var3).lastAdminCheckFlag));
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] entity " + var2 + " not flagged");
         }
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] entity " + var2 + " not found in currently loaded objects");
      }
   }

   private void isFlagSelected(GameServerState var1) throws IOException {
      boolean var2 = (Boolean)this.commandParams[0];
      SimpleTransformableSendableObject var3;
      if ((var3 = this.getSelectedObject(var1)) instanceof SegmentController) {
         this.flag(var1, var2, (SegmentController)var3);
      }

   }

   private void flagSelectedUID(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      boolean var3 = (Boolean)this.commandParams[1];
      Sendable var4;
      if ((var4 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var2)) instanceof SegmentController) {
         this.flag(var1, var3, (SegmentController)var4);
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] entity " + var2 + " not found in currently loaded objects");
      }
   }

   private void flagSelected(GameServerState var1) throws IOException {
      boolean var2 = (Boolean)this.commandParams[0];
      SimpleTransformableSendableObject var3;
      if ((var3 = this.getSelectedObject(var1)) instanceof SegmentController) {
         this.flag(var1, var2, (SegmentController)var3);
      }

   }

   private void flag(GameServerState var1, boolean var2, SegmentController var3) throws IOException {
      if (var3 != null) {
         var3.lastAdminCheckFlag = var2 ? System.currentTimeMillis() : 0L;
         var3.getRuleEntityManager().triggerAdminFlagChanged();
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] " + (var2 ? "FLAGGED" : "NOT FLAGGED") + " ENTITY " + var3);
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] entity not flaggable or not found in currently loaded objects");
      }
   }

   private void track(GameServerState var1, boolean var2, SimpleTransformableSendableObject var3) throws IOException {
      if (var3 != null) {
         var3.setTracked(var2);
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] " + (var2 ? "TRACKING" : "NOT TRACKING") + " ENTITY " + var3);
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] entity not trackable or not found in currently loaded objects");
      }
   }

   private void forcePlayerExit(GameServerState var1) throws IOException {
      SimpleTransformableSendableObject var2;
      if ((var2 = this.getSelectedObject(var1)) != null && var2 instanceof EditableSendableSegmentController) {
         EditableSendableSegmentController var3;
         (var3 = (EditableSendableSegmentController)var2).forceAllCharacterExit();
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] forced all player out of " + var3.toNiceString());
      }

   }

   private void createTradeParty(GameServerState var1) throws IOException {
      try {
         PlayerState var2 = var1.getPlayerFromStateId(this.client.getId());
         Sendable var3 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var2.getNetworkObject().selectedEntityId.get());
         ShopInterface var4 = null;
         if (var3 != null) {
            if (var3 instanceof ShopInterface) {
               var4 = (ShopInterface)var3;
            } else if (var3 instanceof ManagedSegmentController && ((ManagedSegmentController)var3).getManagerContainer() instanceof ShopInterface) {
               var4 = (ShopInterface)((ManagedSegmentController)var3).getManagerContainer();
            }
         }

         if (var4 != null) {
            Vector3i var6 = var1.getSimulationManager().getUnloadedSectorAround(var2.getCurrentSector(), new Vector3i());
            var1.getSimulationManager().addJob(new SpawnTradingPartyJob(var6, new Vector3i(var2.getCurrentSector()), 3));
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Trade party created from the nearest unloaded sector");
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] No Shop Selected: " + var2.getNetworkObject().selectedEntityId.get() + "->(" + var3 + ")");
         }
      } catch (PlayerNotFountException var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      }
   }

   private void territoryUnclaimable(GameServerState var1) throws IOException {
      int var2 = (Integer)this.commandParams[0];
      int var3 = (Integer)this.commandParams[1];
      int var4 = (Integer)this.commandParams[2];
      VoidSystem var5 = new VoidSystem();
      var1.getDatabaseIndex().getTableManager().getSystemTable().loadSystem(var1, new Vector3i(var2, var3, var4), var5);
      FactionSystemOwnerChange var6 = new FactionSystemOwnerChange("ADMIN", -2, "UNCLAIMABLE", new Vector3i((var2 << 4) + 8, (var3 << 4) + 8, (var4 << 4) + 8), new Vector3i(var2, var3, var4), var5.getName());
      var1.getFactionManager().addFactionSystemOwnerChangeServer(var6);
      this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] made system unclaimable: " + var2 + ", " + var3 + ", " + var4);
   }

   private void territoryReset(GameServerState var1) throws IOException {
      int var2 = (Integer)this.commandParams[0];
      int var3 = (Integer)this.commandParams[1];
      int var4 = (Integer)this.commandParams[2];
      VoidSystem var5 = new VoidSystem();
      var1.getDatabaseIndex().getTableManager().getSystemTable().loadSystem(var1, new Vector3i(var2, var3, var4), var5);
      FactionSystemOwnerChange var6 = new FactionSystemOwnerChange("ADMIN", 0, "", var5.getOwnerPos(), new Vector3i(var2, var3, var4), var5.getName());
      var1.getFactionManager().addFactionSystemOwnerChangeServer(var6);
      this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] unclaimed system: " + var2 + ", " + var3 + ", " + var4);
   }

   private void factionPointProtectPlayer(GameServerState var1) throws IOException {
      try {
         PlayerState var3;
         (var3 = var1.getPlayerFromName((String)this.commandParams[0])).setFactionPointProtected((Boolean)this.commandParams[1]);
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] player " + var3.getName() + " FP protected: " + var3.isFactionPointProtected());
      } catch (PlayerNotFountException var2) {
         var2.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Player not found or not online'" + this.commandParams[0] + "'");
      }
   }

   private void factionPointGet(GameServerState var1) throws IOException {
      Integer var2 = (Integer)this.commandParams[0];
      Faction var3;
      if ((var3 = var1.getGameState().getFactionManager().getFaction(var2)) != null) {
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] faction points of " + var3.getName() + " now: " + var3.factionPoints);
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Faction Not Found: " + var2 + " (use /list_factions, or check the faction hub)");
      }
   }

   private void factionPointAdd(GameServerState var1) throws IOException {
      Integer var2 = (Integer)this.commandParams[0];
      Integer var3 = (Integer)this.commandParams[1];
      Faction var4;
      if ((var4 = var1.getGameState().getFactionManager().getFaction(var2)) != null) {
         var4.factionPoints += (float)var3;
         var4.sendFactionPointUpdate(var1.getGameState());
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] faction points of " + var4.getName() + " now: " + var4.factionPoints);
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Faction Not Found: " + var2 + " (use /list_factions, or check the faction hub)");
      }
   }

   private void factionPointSet(GameServerState var1) throws IOException {
      Integer var2 = (Integer)this.commandParams[0];
      Integer var3 = (Integer)this.commandParams[1];
      Faction var4;
      if ((var4 = var1.getGameState().getFactionManager().getFaction(var2)) != null) {
         var4.factionPoints = (float)var3;
         var4.sendFactionPointUpdate(var1.getGameState());
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] faction points of " + var4.getName() + " now: " + var4.factionPoints);
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Faction Not Found: " + var2 + " (use /list_factions, or check the faction hub)");
      }
   }

   private void factionResetActivity(GameServerState var1) throws IOException {
      Integer var2 = (Integer)this.commandParams[0];

      try {
         var1.getGameState().getFactionManager().resetAllActivity(var2);
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] reset faction activity of " + var2);
      } catch (FactionNotFoundException var3) {
         var3.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Faction Not Found: " + var2);
      }
   }

   private void listDeniedCommands(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      if (!var1.isAdmin(var2)) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Player " + var2 + " is not an admin");
      } else {
         Admin var3;
         if ((var3 = (Admin)var1.getAdmins().get(var2.toLowerCase(Locale.ENGLISH).trim())).deniedCommands.size() == 0) {
            this.client.serverMessage("Player " + var2 + " has no denied commands");
         } else {
            this.client.serverMessage("Denied Commands for " + var2 + ":");
            Iterator var4 = var3.deniedCommands.iterator();

            while(var4.hasNext()) {
               AdminCommands var5 = (AdminCommands)var4.next();
               this.client.serverMessage(var5.name().toLowerCase(Locale.ENGLISH));
            }

         }
      }
   }

   private void addOrRemoveAdminDeniedCommand(GameServerState var1, boolean var2) throws IOException {
      String var3 = (String)this.commandParams[0];
      String var4 = (String)this.commandParams[1];

      try {
         AdminCommands var5 = AdminCommands.valueOf(var4.toUpperCase(Locale.ENGLISH).trim());
         if (var1.isAdmin(var3)) {
            if (var2) {
               var1.getController().addAdminDeniedCommand(this.client.getPlayerName(), var3, var5);
               this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Player " + var3 + " is now forbidden to use " + var5.name().toLowerCase(Locale.ENGLISH));
            } else {
               var1.getController().removeAdminDeniedCommand(this.client.getPlayerName(), var3, var5);
               this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Player " + var3 + " is no longer forbidden to use " + var5.name().toLowerCase(Locale.ENGLISH));
            }
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] Player " + var3 + " is not an admin");
         }
      } catch (Exception var6) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Admin Command unknown '" + var4 + "'");
      }
   }

   private void handleServerConfigBoolean(GameServerState var1, ServerConfig var2) throws IOException {
      boolean var3 = (Boolean)this.commandParams[0];
      var2.changeBooleanSetting(var3);
      ServerConfig.write();
      this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] " + var2.name() + " set to '" + var3 + "'");
   }

   private void tint(GameServerState var1, boolean var2) throws IOException {
      float var3 = (Float)this.commandParams[0];
      float var4 = (Float)this.commandParams[1];
      float var5 = (Float)this.commandParams[2];
      float var6 = (Float)this.commandParams[3];

      try {
         PlayerState var8;
         if (var2) {
            var8 = var1.getPlayerFromName((String)this.commandParams[4]);
         } else {
            PlayerState var10 = var1.getPlayerFromStateId(this.client.getId());
            Sendable var9;
            if ((var9 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var10.getNetworkObject().selectedEntityId.get())) == null || !(var9 instanceof PlayerControllable) || ((PlayerControllable)var9).getAttachedPlayers().isEmpty()) {
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] Player not found");
               return;
            }

            var8 = (PlayerState)((PlayerControllable)var9).getAttachedPlayers().get(0);
         }

         var8.getTint().set(var3, var4, var5, var6);
      } catch (PlayerNotFountException var7) {
         var7.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Player not found");
      }
   }

   private void sectorInfo(GameServerState var1) throws IOException {
      int var2 = (Integer)this.commandParams[0];
      int var3 = (Integer)this.commandParams[1];
      int var4 = (Integer)this.commandParams[2];

      try {
         Sector var5;
         if ((var5 = var1.getUniverse().getSectorWithoutLoading(new Vector3i(var2, var3, var4))) != null) {
            Iterator var12 = var1.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

            while(var12.hasNext()) {
               Sendable var10;
               if ((var10 = (Sendable)var12.next()) instanceof SimpleTransformableSendableObject && ((SimpleTransformableSendableObject)var10).getSectorId() == var5.getId()) {
                  this.client.serverMessage(((SimpleTransformableSendableObject)var10).getInfo());
               }
            }

            this.client.serverMessage("LOADED SECTOR INFO: " + var5.toDetailString());
         } else {
            Sector var6 = new Sector(var1);
            if (!var1.getDatabaseIndex().getTableManager().getSectorTable().loadSector(new Vector3i(var2, var3, var4), var6)) {
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] Sector " + new Vector3i(var2, var3, var4) + " not in database");
            } else {
               List var8;
               if ((var8 = var1.getDatabaseIndex().getTableManager().getEntityTable().getBySector(new Vector3i(var2, var3, var4), 0)).size() == 0) {
                  this.client.serverMessage("[ADMIN COMMAND] No entity record for sector");
               } else {
                  System.err.println("[ADMIN COMMAND] [SUCCESS] Displaying " + var8.size() + " entities for Sector " + new Vector3i(var2, var3, var4));
               }

               Iterator var9 = var8.iterator();

               while(var9.hasNext()) {
                  DatabaseEntry var11 = (DatabaseEntry)var9.next();
                  this.client.serverMessage(var11.toString());
               }

               this.client.serverMessage("SECTOR INFO: " + var6.toDetailString());
            }
         }
      } catch (Exception var7) {
         var7.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Command known, but not executable: " + var7.getClass().getSimpleName() + ": " + var7.getMessage() + ". Please send in report!");
      }
   }

   private void printInfo(GameServerState var1, Ship var2) throws IOException {
      this.client.serverMessage("ReactorHP: " + var2.getReactorHp() + " / " + var2.getReactorHpMax());
      this.client.serverMessage("MissileCapacity: " + var2.getMissileCapacity() + " / " + var2.getMissileCapacityMax());
      this.client.serverMessage("Attached: " + var2.getAttachedPlayers());
      this.client.serverMessage("DockedUIDs: " + this.getDockedList(var2));
      this.client.serverMessage("Blocks: " + var2.getTotalElements());
      this.client.serverMessage("Mass: " + var2.getMass());
      this.client.serverMessage("LastModified: " + var2.getLastModifier());
      this.client.serverMessage("Creator: " + var2.getSpawner());
      this.client.serverMessage("Sector: " + var2.getSectorId() + " -> " + var1.getUniverse().getSector(var2.getSectorId()));
      this.client.serverMessage("Name: " + var2.getRealName());
      this.client.serverMessage("UID: " + var2.getUniqueIdentifier());
      this.client.serverMessage("MinBB(chunks): " + var2.getMinPos());
      this.client.serverMessage("MaxBB(chunks): " + var2.getMaxPos());
      this.client.serverMessage("Local-Pos: " + var2.getWorldTransform().origin);
      this.client.serverMessage("Orientation: " + Quat4fTools.set(var2.getWorldTransform().basis, new Quat4f()));
      this.client.serverMessage("FactionID: " + var2.getFactionId());
      this.client.serverMessage("Ship");
   }

   private String getDockedList(SegmentController var1) {
      StringBuffer var2 = new StringBuffer();

      for(int var3 = 0; var3 < var1.getDockingController().getDockedOnThis().size(); ++var3) {
         ElementDocking var4 = (ElementDocking)var1.getDockingController().getDockedOnThis().get(var3);
         var2.append(var4.from.getSegment().getSegmentController().getUniqueIdentifier());
         if (var3 < var1.getDockingController().getDockedOnThis().size()) {
            var2.append(", ");
         }
      }

      return var2.toString();
   }

   private void printInfo(GameServerState var1, Planet var2) throws IOException {
      this.client.serverMessage("Attached: " + var2.getAttachedPlayers());
      this.client.serverMessage("DockedUIDs: " + this.getDockedList(var2));
      this.client.serverMessage("Blocks: " + var2.getTotalElements());
      this.client.serverMessage("Mass: " + var2.getMass());
      this.client.serverMessage("LastModified: " + var2.getLastModifier());
      this.client.serverMessage("Creator: " + var2.getSpawner());
      this.client.serverMessage("Sector: " + var2.getSectorId() + " -> " + var1.getUniverse().getSector(var2.getSectorId()));
      this.client.serverMessage("Name: " + var2.getRealName());
      this.client.serverMessage("UID: " + var2.getUniqueIdentifier());
      this.client.serverMessage("MinBB(chunks): " + var2.getMinPos());
      this.client.serverMessage("MaxBB(chunks): " + var2.getMaxPos());
      this.client.serverMessage("Local-Pos: " + var2.getWorldTransform().origin);
      this.client.serverMessage("Orientation: " + Quat4fTools.set(var2.getWorldTransform().basis, new Quat4f()));
      this.client.serverMessage("FactionID: " + var2.getFactionId());
      this.client.serverMessage("Planet");
   }

   private void printInfo(GameServerState var1, SpaceStation var2) throws IOException {
      this.client.serverMessage("Attached: " + var2.getAttachedPlayers());
      this.client.serverMessage("DockedUIDs: " + this.getDockedList(var2));
      this.client.serverMessage("Blocks: " + var2.getTotalElements());
      this.client.serverMessage("Mass: " + var2.getMass());
      this.client.serverMessage("LastModified: " + var2.getLastModifier());
      this.client.serverMessage("Creator: " + var2.getSpawner());
      this.client.serverMessage("Sector: " + var2.getSectorId() + " -> " + var1.getUniverse().getSector(var2.getSectorId()));
      this.client.serverMessage("Name: " + var2.getRealName());
      this.client.serverMessage("UID: " + var2.getUniqueIdentifier());
      this.client.serverMessage("MinBB(chunks): " + var2.getMinPos());
      this.client.serverMessage("MaxBB(chunks): " + var2.getMaxPos());
      this.client.serverMessage("Local-Pos: " + var2.getWorldTransform().origin);
      this.client.serverMessage("Orientation: " + Quat4fTools.set(var2.getWorldTransform().basis, new Quat4f()));
      this.client.serverMessage("FactionID: " + var2.getFactionId());
      this.client.serverMessage("Station");
   }

   private void printInfo(GameServerState var1, SimpleTransformableSendableObject var2) throws IOException {
      if (var2 != null) {
         if (var2 instanceof Ship) {
            this.printInfo(var1, (Ship)var2);
         }

         if (var2 instanceof SpaceStation) {
            this.printInfo(var1, (SpaceStation)var2);
         }

         if (var2 instanceof Planet) {
            this.printInfo(var1, (Planet)var2);
         }
      }

   }

   private void shipInfoSelected(GameServerState var1) throws IOException {
      try {
         PlayerState var2;
         SimpleTransformableSendableObject var3;
         if ((var3 = (var2 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable()) == null || !(var3 instanceof SegmentController)) {
            System.err.println("[ADMIN COMMAND]checking selected");
            Sendable var6;
            if ((var6 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var2.getNetworkObject().selectedEntityId.get())) != null && var6 instanceof SimpleTransformableSendableObject) {
               var3 = (SimpleTransformableSendableObject)var6;
            }
         }

         this.printInfo(var1, var3);
      } catch (PlayerNotFountException var4) {
         var4.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      } catch (PlayerControlledTransformableNotFound var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      }
   }

   private void shipInfoName(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         synchronized(var1.getLocalAndRemoteObjectContainer().getLocalObjects()) {
            boolean var4 = false;
            Iterator var5 = var1.getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

            while(var5.hasNext()) {
               Sendable var6;
               if ((var6 = (Sendable)var5.next()) instanceof SegmentController && ((SimpleTransformableSendableObject)var6).getRealName().equals(var2)) {
                  this.client.serverMessage("[INFO] " + ((SimpleTransformableSendableObject)var6).getRealName() + " found in loaded objects");
                  this.printInfo(var1, (SimpleTransformableSendableObject)((SegmentController)var6));
                  var4 = true;
               }
            }

            if (var4) {
               return;
            }
         }

         this.client.serverMessage("[INFO] " + var2 + " not found in loaded objects. Checking Database...");
         List var3;
         if ((var3 = var1.getDatabaseIndex().getTableManager().getEntityTable().getByNameExact(var2, 1)).size() <= 0) {
            this.client.serverMessage("[INFO] " + var2 + " not found in database");
         } else {
            Iterator var9 = var3.iterator();

            while(var9.hasNext()) {
               DatabaseEntry var10 = (DatabaseEntry)var9.next();
               this.client.serverMessage(var10.toString());
            }

         }
      } catch (SQLException var8) {
         var8.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var8.getMessage());
      }
   }

   private void shipInfoUid(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         List var3 = var1.getDatabaseIndex().getTableManager().getEntityTable().getByUIDExact(DatabaseEntry.removePrefix(var2), 1);
         this.client.serverMessage("Loaded: " + var1.getLocalAndRemoteObjectContainer().getUidObjectMap().containsKey(var2));
         if (var3.isEmpty()) {
            this.client.serverMessage("UID Not Found in DB: " + var2 + "; checking unsaved objects");
            if (var1.getLocalAndRemoteObjectContainer().getUidObjectMap().containsKey(var2)) {
               Sendable var8;
               if ((var8 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var2)) != null && var8 instanceof SimpleTransformableSendableObject) {
                  this.printInfo(var1, (SimpleTransformableSendableObject)var8);
               }

            } else {
               this.client.serverMessage("UID also not found in unsaved objects");
            }
         } else {
            Iterator var7 = var3.iterator();

            while(var7.hasNext()) {
               DatabaseEntry var4 = (DatabaseEntry)var7.next();
               this.client.serverMessage(var4.toString());
               Sendable var9;
               if (var1.getLocalAndRemoteObjectContainer().getUidObjectMap().containsKey(var2) && (var9 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var2)) != null && var9 instanceof SimpleTransformableSendableObject) {
                  this.printInfo(var1, (SimpleTransformableSendableObject)var9);
               }
            }

         }
      } catch (SQLException var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      } catch (IllegalArgumentException var6) {
         var6.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] you need to provide the full UID (e.g. ENTITY_SHIP_RESTOFUID)");
      }
   }

   private void factionPointTurn(GameServerState var1) throws IOException {
      var1.getFactionManager().forceFactionPointTurn();
      this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] faction point turn forced");
   }

   private void setEntityPermissionUID(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      Sendable var3;
      if ((var3 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var2)) != null && var3 instanceof SegmentController) {
         byte var5 = ((Integer)this.commandParams[1]).byteValue();
         var5 = (byte)Math.max(-2, Math.min(4, var5));
         SegmentController var4;
         (var4 = (SegmentController)var3).setFactionRights(var5);
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Faction Rank of " + var4 + " set to " + var5 + " = " + FactionRoles.getRoleName(var5));
      }

   }

   private void setEntityPermission(GameServerState var1) throws IOException {
      SegmentController var3 = this.getSelectedOrEnteredStructure(var1);
      byte var2 = ((Integer)this.commandParams[0]).byteValue();
      var2 = (byte)Math.max(-2, Math.min(4, var2));
      var3.setFactionRights(var2);
      this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Faction Rank of " + var3 + " set to " + var2 + " = " + FactionRoles.getRoleName(var2));
   }

   private void decay(GameServerState var1) throws IOException {
      boolean var2 = (Boolean)this.commandParams[0];

      try {
         PlayerState var3 = var1.getPlayerFromStateId(this.client.getId());
         Sendable var5;
         if ((var5 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var3.getNetworkObject().selectedEntityId.get())) != null && var5 instanceof SegmentController) {
            ((SegmentController)var5).setScrap(var2);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] nothing valid selected");
         }
      } catch (PlayerNotFountException var4) {
         var4.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      }
   }

   private void minable(GameServerState var1) throws IOException {
      boolean var2 = (Boolean)this.commandParams[0];

      try {
         PlayerState var3 = var1.getPlayerFromStateId(this.client.getId());
         Sendable var5;
         if ((var5 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var3.getNetworkObject().selectedEntityId.get())) != null && var5 instanceof SegmentController) {
            ((SegmentController)var5).setMinable(var2);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] nothing valid selected");
         }
      } catch (PlayerNotFountException var4) {
         var4.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      }
   }

   private void vulnerable(GameServerState var1) throws IOException {
      boolean var2 = (Boolean)this.commandParams[0];

      try {
         PlayerState var3 = var1.getPlayerFromStateId(this.client.getId());
         Sendable var5;
         if ((var5 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var3.getNetworkObject().selectedEntityId.get())) != null && var5 instanceof SegmentController) {
            ((SegmentController)var5).setVulnerable(var2);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] nothing valid selected");
         }
      } catch (PlayerNotFountException var4) {
         var4.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      }
   }

   private void minableUid(GameServerState var1) throws IOException {
      boolean var2 = (Boolean)this.commandParams[1];
      String var3 = (String)this.commandParams[0];
      Sendable var4;
      if ((var4 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var3)) != null && var4 instanceof SegmentController) {
         ((SegmentController)var4).setMinable(var2);
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] nothing valid selected");
      }
   }

   private void vulnerableUid(GameServerState var1) throws IOException {
      boolean var2 = (Boolean)this.commandParams[1];
      String var3 = (String)this.commandParams[0];
      Sendable var4;
      if ((var4 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var3)) != null && var4 instanceof SegmentController) {
         ((SegmentController)var4).setVulnerable(var2);
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] nothing valid selected");
      }
   }

   private void shieldDamage(GameServerState var1) throws IOException {
      int var2 = (Integer)this.commandParams[0];

      try {
         PlayerState var3;
         SimpleTransformableSendableObject var4;
         if ((var4 = (var3 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable()) == null || !(var4 instanceof SegmentController)) {
            System.err.println("[ADMIN COMMAND]checking selected");
            Sendable var7;
            if ((var7 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var3.getNetworkObject().selectedEntityId.get())) != null && var7 instanceof SimpleTransformableSendableObject) {
               var4 = (SimpleTransformableSendableObject)var7;
            }
         }

         boolean var8 = false;
         if (var4 != null && var4 instanceof ManagedSegmentController && ((ManagedSegmentController)var4).getManagerContainer() instanceof ShieldContainerInterface) {
            ShieldAddOn var9 = ((ShieldContainerInterface)((ManagedSegmentController)var4).getManagerContainer()).getShieldAddOn();
            var8 = true;
            var9.onHit(0L, (short)0, (double)var2, DamageDealerType.GENERAL);
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] took out shields of " + var4);
            var9.getShieldLocalAddOn().hitAllShields((double)var2);
         }

         if (!var8) {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] object " + var4 + " has no shield capability");
         }

      } catch (PlayerNotFountException var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      } catch (PlayerControlledTransformableNotFound var6) {
         var6.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var6.getMessage());
      }
   }

   private void shieldOutage(GameServerState var1) throws IOException {
      try {
         PlayerState var2;
         SimpleTransformableSendableObject var3;
         if ((var3 = (var2 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable()) == null || !(var3 instanceof SegmentController)) {
            System.err.println("[ADMIN COMMAND]checking selected");
            Sendable var6;
            if ((var6 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var2.getNetworkObject().selectedEntityId.get())) != null && var6 instanceof SimpleTransformableSendableObject) {
               var3 = (SimpleTransformableSendableObject)var6;
            }
         }

         boolean var7 = false;
         if (var3 != null && var3 instanceof ManagedSegmentController && ((ManagedSegmentController)var3).getManagerContainer() instanceof ShieldContainerInterface) {
            ShieldAddOn var8 = ((ShieldContainerInterface)((ManagedSegmentController)var3).getManagerContainer()).getShieldAddOn();
            var7 = true;
            var8.onHit(0L, (short)0, (double)((long)Math.ceil(var8.getShields())), DamageDealerType.GENERAL);
            var8.getShieldLocalAddOn().dischargeAllShields();
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] took out shields of " + var3);
         }

         if (!var7) {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] object " + var3 + " has no shield capability");
         }

      } catch (PlayerNotFountException var4) {
         var4.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      } catch (PlayerControlledTransformableNotFound var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      }
   }

   private void powerOutage(GameServerState var1) throws IOException {
      try {
         PlayerState var2;
         SimpleTransformableSendableObject var3;
         if ((var3 = (var2 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable()) == null || !(var3 instanceof SegmentController)) {
            System.err.println("[ADMIN COMMAND]checking selected");
            Sendable var6;
            if ((var6 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var2.getNetworkObject().selectedEntityId.get())) != null && var6 instanceof SimpleTransformableSendableObject) {
               var3 = (SimpleTransformableSendableObject)var6;
            }
         }

         boolean var7 = false;
         if (var3 != null && var3 instanceof ManagedSegmentController && ((ManagedSegmentController)var3).getManagerContainer() instanceof PowerManagerInterface) {
            PowerAddOn var8 = ((PowerManagerInterface)((ManagedSegmentController)var3).getManagerContainer()).getPowerAddOn();
            var7 = true;
            var8.consumePowerInstantly(var8.getPower());
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] took out power of " + var3);
         }

         if (!var7) {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] object " + var3 + " has no shield capability");
         }

      } catch (PlayerNotFountException var4) {
         var4.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      } catch (PlayerControlledTransformableNotFound var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      }
   }

   private void serverMessageTo(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      String var3 = (String)this.commandParams[1];
      String var4 = ((String)this.commandParams[2]).replaceAll("\\\\n", "\n");
      byte var7;
      if (var2.toLowerCase(Locale.ENGLISH).equals("plain")) {
         var7 = 0;
      } else if (var2.toLowerCase(Locale.ENGLISH).equals("info")) {
         var7 = 1;
      } else if (var2.toLowerCase(Locale.ENGLISH).equals("warning")) {
         var7 = 2;
      } else {
         if (!var2.toLowerCase(Locale.ENGLISH).equals("error")) {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] message type must be either plain/info/warning/error");
            return;
         }

         var7 = 3;
      }

      PlayerState var5 = null;

      try {
         (var5 = var1.getPlayerFromName(var3)).sendServerMessage(new ServerMessage(new Object[]{var4}, var7, var5.getId()));
      } catch (PlayerNotFountException var6) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player " + var3 + " not online");
      }
   }

   private void serverMessageBroadcast(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      String var3 = ((String)this.commandParams[1]).replaceAll("\\\\n", "\n");
      byte var4;
      if (var2.toLowerCase(Locale.ENGLISH).equals("plain")) {
         var4 = 0;
      } else if (var2.toLowerCase(Locale.ENGLISH).equals("info")) {
         var4 = 1;
      } else if (var2.toLowerCase(Locale.ENGLISH).equals("warning")) {
         var4 = 2;
      } else {
         if (!var2.toLowerCase(Locale.ENGLISH).equals("error")) {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] message type must be either plain/info/warning/error");
            return;
         }

         var4 = 3;
      }

      var1.getController().broadcastMessage(new Object[]{var3}, var4);
   }

   private void explodePlanetSector(GameServerState var1, boolean var2) {
      try {
         SimpleTransformableSendableObject var3 = var1.getPlayerFromStateId(this.client.getId()).getFirstControlledTransformable();
         synchronized(var1.getLocalAndRemoteObjectContainer().getLocalObjects()) {
            Iterator var9 = var1.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

            while(var9.hasNext()) {
               Sendable var5 = (Sendable)var9.next();
               if (!var2 && var5 instanceof Planet && ((Planet)var5).getSectorId() == var3.getSectorId()) {
                  ((Planet)var5).setPlanetCore((PlanetCore)null);
                  ((Planet)var5).setPlanetCoreUID("none");
                  ((Planet)var5).setBlownOff(new Vector3f(0.0F, 0.0F, 0.0F));
               }

               if (var2 && var5 instanceof PlanetCore && ((PlanetCore)var5).getSectorId() == var3.getSectorId()) {
                  ((PlanetCore)var5).setDestroyed(true);
               }
            }

         }
      } catch (PlayerNotFountException var7) {
         var7.printStackTrace();
      } catch (PlayerControlledTransformableNotFound var8) {
         var8.printStackTrace();
      }
   }

   private void clearOverheatingSector(GameServerState var1) throws IOException {
      try {
         int var2 = (Integer)this.commandParams[0];
         int var3 = (Integer)this.commandParams[1];
         int var4 = (Integer)this.commandParams[2];
         Sector var5;
         if ((var5 = var1.getUniverse().getSectorWithoutLoading(new Vector3i(var2, var3, var4))) == null) {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] sector " + var2 + ", " + var3 + ", " + var4 + " unloaded");
         } else {
            var1.getPlayerFromStateId(this.client.getId());
            synchronized(var1.getLocalAndRemoteObjectContainer().getLocalObjects()) {
               int var7 = 0;
               Iterator var11 = var1.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

               while(var11.hasNext()) {
                  Sendable var8;
                  if ((var8 = (Sendable)var11.next()) instanceof SegmentController && ((SegmentController)var8).getSectorId() == var5.getSectorId() && ((SegmentController)var8).isCoreOverheating()) {
                     if (var8 instanceof SegmentController) {
                        ((SegmentController)var8).railController.undockAllServer();
                     }

                     var8.markForPermanentDelete(true);
                     var8.setMarkedForDeleteVolatile(true);
                     ++var7;
                  }
               }

               this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] cleared overheating in sector " + var2 + ", " + var3 + ", " + var4 + ": " + var7);
            }
         }
      } catch (PlayerNotFountException var10) {
         var10.printStackTrace();
      }
   }

   private void clearOverheating(GameServerState var1, boolean var2) throws IOException {
      try {
         SimpleTransformableSendableObject var3 = var1.getPlayerFromStateId(this.client.getId()).getFirstControlledTransformable();
         synchronized(var1.getLocalAndRemoteObjectContainer().getLocalObjects()) {
            int var5 = 0;

            for(Iterator var10 = var1.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator(); var10.hasNext(); this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] cleared overheating: " + var5)) {
               Sendable var6;
               if ((var6 = (Sendable)var10.next()) instanceof SegmentController && (var2 || ((SegmentController)var6).getSectorId() == var3.getSectorId() || ((SegmentController)var6).isNeighbor(var3.getSectorId(), ((SegmentController)var6).getSectorId())) && ((SegmentController)var6).isCoreOverheating()) {
                  if (var6 instanceof SegmentController) {
                     ((SegmentController)var6).railController.undockAllServer();
                  }

                  var6.markForPermanentDelete(true);
                  var6.setMarkedForDeleteVolatile(true);
                  ++var5;
               }
            }

         }
      } catch (PlayerNotFountException var8) {
         var8.printStackTrace();
      } catch (PlayerControlledTransformableNotFound var9) {
         var9.printStackTrace();
      }
   }

   private void clearSystemShipSpawns(GameServerState var1, boolean var2) {
      try {
         SimpleTransformableSendableObject var3 = var1.getPlayerFromStateId(this.client.getId()).getFirstControlledTransformable();
         synchronized(var1.getLocalAndRemoteObjectContainer().getLocalObjects()) {
            Iterator var9 = var1.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

            while(true) {
               Sendable var5;
               do {
                  do {
                     if (!var9.hasNext()) {
                        return;
                     }
                  } while(!((var5 = (Sendable)var9.next()) instanceof Ship));
               } while(!var2 && ((Ship)var5).getSectorId() != var3.getSectorId() && !((Ship)var5).isNeighbor(var3.getSectorId(), ((Ship)var5).getSectorId()));

               if (((Ship)var5).getSpawner().toLowerCase(Locale.ENGLISH).equals("<system>")) {
                  var5.markForPermanentDelete(true);
                  var5.setMarkedForDeleteVolatile(true);
               }
            }
         }
      } catch (PlayerNotFountException var7) {
         var7.printStackTrace();
      } catch (PlayerControlledTransformableNotFound var8) {
         var8.printStackTrace();
      }
   }

   private void createCreatureSpawnerTest(GameServerState var1) throws IOException {
      try {
         PlayerState var2;
         SimpleTransformableSendableObject var3;
         Vector3f var4;
         if ((var3 = (var2 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable()) instanceof AbstractCharacter) {
            var4 = new Vector3f(((AbstractCharacter)var3).getHeadWorldTransform().origin);
         } else {
            var4 = new Vector3f(var3.getWorldTransform().origin);
         }

         Vector3f var5 = new Vector3f(var4);
         Vector3f var10;
         (var10 = var2.getForward(new Vector3f())).scale(5000.0F);
         var5.add(var10);
         Sector var10000 = var1.getUniverse().getSector(var3.getSectorId());
         var1 = null;
         ClosestRayResultCallback var8;
         if ((var8 = ((PhysicsExt)var10000.getPhysics()).testRayCollisionPoint(var4, var5, false, (SimpleTransformableSendableObject)null, (SegmentController)null, false, true, false)).hasHit() && var8 instanceof CubeRayCastResult) {
            CubeRayCastResult var9;
            if ((var9 = (CubeRayCastResult)var8).getSegment() != null) {
               DefaultSpawner var11 = new DefaultSpawner();
               SpawnMarker var12 = new SpawnMarker(var9.getNextToAbsolutePosition(), var9.getSegment().getSegmentController(), var11);
               SpawnComponentCreature var13;
               (var13 = new SpawnComponentCreature()).setBottom("LegsArag");
               var13.setMiddle("TorsoShell");
               var13.setName("Spider");
               var13.setCreatureType(CreatureType.CREATURE_SPECIFIC);
               var13.setFactionId(FactionManager.FAUNA_GROUP_ENEMY[0]);
               var11.getComponents().add(var13);
               var11.getComponents().add(new SpawnComponentDestroySpawnerAfterCount(5));
               var11.getConditions().add(new SpawnConditionTime(5000L));
               var11.getConditions().add(new SpawnConditionCreatureCountOnAffinity(1));
               var9.getSegment().getSegmentController().getSpawnController().getSpawnMarker().add(var12);
               this.client.serverMessage("[ADMIN COMMAND] Spawner Spawned");
            } else {
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] no object in line of sight");
            }
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] no object in line of sight");
         }
      } catch (PlayerNotFountException var6) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var6.getMessage());
      } catch (PlayerControlledTransformableNotFound var7) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var7.getMessage());
      }
   }

   private void spawnCreature(GameServerState var1) throws IOException {
      try {
         PlayerState var2 = var1.getPlayerFromStateId(this.client.getId());
         CreatureSpawn var5 = new CreatureSpawn(new Vector3i(var2.getCurrentSector()), new Transform(var2.getFirstControlledTransformable().getWorldTransform()), "NoName", CreatureType.CHARACTER) {
            public void initAI(AIGameCreatureConfiguration var1) {
               try {
                  assert var1 != null;

                  var1.get(Types.ORIGIN_X).switchSetting("-2147483648", false);
                  var1.get(Types.ORIGIN_Y).switchSetting("-2147483648", false);
                  var1.get(Types.ORIGIN_Z).switchSetting("-2147483648", false);
                  var1.get(Types.ROAM_X).switchSetting("22", false);
                  var1.get(Types.ROAM_Y).switchSetting("3", false);
                  var1.get(Types.ROAM_Z).switchSetting("22", false);
               } catch (StateParameterNotFoundException var2) {
                  var2.printStackTrace();
               }
            }
         };
         var1.getController().queueCreatureSpawn(var5);
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Spawning creature");
      } catch (PlayerNotFountException var3) {
         var3.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] No player");
      } catch (PlayerControlledTransformableNotFound var4) {
         var4.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] No player position found");
      }
   }

   private void spawnMassCreature(GameServerState var1) throws IOException {
      int var2 = (Integer)this.commandParams[0];

      try {
         PlayerState var3;
         SimpleTransformableSendableObject var4 = (var3 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable();

         for(int var5 = 0; var5 < var2; ++var5) {
            Transform var6 = new Transform(var4.getWorldTransform());
            Vector3f var7 = new Vector3f(Universe.getRandom().nextFloat() * 50.0F - 25.0F, 20.0F, Universe.getRandom().nextFloat() * 50.0F - 25.0F);
            var6.basis.transform(var7);
            var6.origin.add(var7);
            Vector3i var11 = new Vector3i();

            try {
               this.getPiece(var6.origin, new Vector3f(0.0F, -1.0F, 0.0F), var1, var4, var11).getTransform(var6);
               CreatureSpawn var12 = new CreatureSpawn(new Vector3i(var3.getCurrentSector()), new Transform(var6), "NoName" + System.currentTimeMillis(), CreatureType.CHARACTER) {
                  public void initAI(AIGameCreatureConfiguration var1) {
                     try {
                        assert var1 != null;

                        var1.get(Types.ORIGIN_X).switchSetting("-2147483648", false);
                        var1.get(Types.ORIGIN_Y).switchSetting("-2147483648", false);
                        var1.get(Types.ORIGIN_Z).switchSetting("-2147483648", false);
                        var1.get(Types.ROAM_X).switchSetting("22", false);
                        var1.get(Types.ROAM_Y).switchSetting("3", false);
                        var1.get(Types.ROAM_Z).switchSetting("22", false);
                     } catch (StateParameterNotFoundException var2) {
                        var2.printStackTrace();
                     }
                  }
               };
               var1.getController().queueCreatureSpawn(var12);
               System.err.println("SPAWNING CREATURE AT: " + var6.origin);
            } catch (NoCollisioinFountException var8) {
               var8.printStackTrace();
            }
         }

         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Spawning mass creature");
      } catch (PlayerNotFountException var9) {
         var9.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] No player");
      } catch (PlayerControlledTransformableNotFound var10) {
         var10.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] No player position found");
      }
   }

   private void playerUnprotect(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      ProtectedUplinkName var3;
      if ((var3 = var1.getController().removeProtectedUser(var2.trim())) != null) {
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] removed account protection " + var3.uplinkname + " from playerName " + var3.playername);
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] no protection for player name " + var2 + " existed");
      }
   }

   private void playerProtect(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      String var3 = (String)this.commandParams[1];
      var1.getController().addProtectedUser(var3.trim(), var2.trim());
      this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] added account protection '" + var3 + "' to playerName " + var2);
   }

   private void playerList(GameServerState var1) throws IOException {
      Iterator var2 = var1.getPlayerStatesByName().values().iterator();

      while(var2.hasNext()) {
         PlayerState var3 = (PlayerState)var2.next();
         this.outputPlayer(var1, var3);
      }

   }

   private void playerInfo(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      PlayerState var3 = null;

      try {
         var3 = var1.getPlayerFromName(var2);
      } catch (PlayerNotFountException var8) {
         System.err.println("[ADMIN] Player not online: " + var2 + "; checking logged off");
         FileExt var4 = new FileExt(GameServerState.ENTITY_DATABASE_PATH + File.separator + "ENTITY_PLAYERSTATE_" + var2.trim() + ".ent");

         try {
            Tag var5 = Tag.readFrom(new BufferedInputStream(new FileInputStream(var4)), true, false);
            (var3 = new PlayerState(var1)).initialize();
            var3.fromTagStructure(var5);
            String var9 = var4.getName();
            var3.setName(var9.substring(19, var9.lastIndexOf(".")));
         } catch (FileNotFoundException var6) {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] player " + var2 + " not online, and no offline save state found");
            var6.printStackTrace();
         } catch (IOException var7) {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] player " + var2 + " not online, and no offline save state reading failed");
            var7.printStackTrace();
         }
      }

      if (var3 != null) {
         this.outputPlayer(var1, var3);
      }

   }

   private void outputPlayer(GameServerState var1, PlayerState var2) throws IOException {
      ArrayList var3;
      (var3 = new ArrayList()).addAll(var2.getHosts());

      for(int var4 = 0; var4 < var3.size(); ++var4) {
         this.client.serverMessage("[PL] LOGIN: " + var3.get(var4));
      }

      this.client.serverMessage("[PL] PERSONAL-TEST-SECTOR: " + var2.testSector);
      this.client.serverMessage("[PL] PERSONAL-BATTLE_MODE-SECTOR: " + var2.testSector);

      try {
         this.client.serverMessage("[PL] CONTROLLING-POS: " + var2.getFirstControlledTransformable().getWorldTransform().origin);
         this.client.serverMessage("[PL] CONTROLLING: " + var2.getFirstControlledTransformable());
      } catch (PlayerControlledTransformableNotFound var5) {
         this.client.serverMessage("[PL] CONTROLLING-POS: <not spawned>");
         this.client.serverMessage("[PL] CONTROLLING: <not spawned>");
      }

      this.client.serverMessage("[PL] SECTOR: " + var2.getCurrentSector());
      this.client.serverMessage("[PL] FACTION: " + var1.getFactionManager().getFaction(var2.getFactionId()));
      this.client.serverMessage("[PL] CREDITS: " + var2.getCredits());
      this.client.serverMessage("[PL] UPGRADED: " + var2.isUpgradedAccount());
      this.client.serverMessage("[PL] SM-NAME: " + var2.getStarmadeName());
      this.client.serverMessage("[PL] IP: " + var2.getIp());
      this.client.serverMessage("[PL] Name: " + var2.getName());
   }

   private void exportSectorBulk(GameServerState var1) {
      String var2 = (String)this.commandParams[0];
      var1.scheduleSectorBulkExport(this.client, var2);
   }

   private void exportSectorNorm(GameServerState var1) {
      Vector3i var2 = new Vector3i((Integer)this.commandParams[0], (Integer)this.commandParams[1], (Integer)this.commandParams[2]);
      String var3 = (String)this.commandParams[3];
      var1.scheduleSectorExport(var2, this.client, var3);
   }

   private void factionEdit(GameServerState var1) throws IOException {
      Integer var2 = (Integer)this.commandParams[0];
      String var3 = (String)this.commandParams[1];
      String var4 = (String)this.commandParams[2];
      Faction var5;
      if ((var5 = var1.getGameState().getFactionManager().getFaction(var2)) != null) {
         var5.setDescription(var4);
         var5.setName(var3);
         var5.sendDescriptionMod("ADMIN", var4, var1.getGameState());
         var5.sendNameMod("ADMIN", var3, var1.getGameState());
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Faction Not Found (must be ID, list with /faction_list: " + var2);
      }

      this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] added new faction!");
   }

   private void factionModMember(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      int var3 = (Integer)this.commandParams[1];

      try {
         PlayerState var6 = var1.getPlayerFromName(var2);
         Faction var4;
         if ((var4 = var1.getFactionManager().getFaction(var6.getFactionId())) != null) {
            if ((FactionPermission)var4.getMembersUID().get(var6.getName()) != null) {
               if (var3 > 0 && var3 < 6) {
                  var4.addOrModifyMember("ADMIN", var6.getName(), (byte)(var3 - 1), System.currentTimeMillis(), var1.getGameState(), true);
               } else {
                  this.client.serverMessage("[ADMIN COMMAND] [ERROR] role id must be between 1 and 5 ");
               }
            } else {
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] player is not part of the faction " + var4.getName());
            }
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] player is not in a faction; fid: " + var6.getFactionId());
         }
      } catch (PlayerNotFountException var5) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player not found for your client");
      }
   }

   private void factionModRelation(GameServerState var1) throws IOException {
      int var2 = (Integer)this.commandParams[0];
      int var3 = (Integer)this.commandParams[1];
      String var4;
      if (!(var4 = ((String)this.commandParams[2]).trim().toLowerCase(Locale.ENGLISH)).equals("enemy") && !var4.equals("ally") && !var4.equals("neutral")) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] relation must either be enemy, ally, or neutral");
      } else {
         Faction var5 = var1.getFactionManager().getFaction(var2);
         Faction var6 = var1.getFactionManager().getFaction(var3);
         if (var5 != null && var6 != null) {
            var1.getFactionManager().setRelationServer(var2, var3, (byte)(var4.equals("neutral") ? FactionRelation.RType.NEUTRAL.ordinal() : (var4.equals("enemy") ? FactionRelation.RType.ENEMY.ordinal() : FactionRelation.RType.FRIEND.ordinal())));
         } else {
            if (var5 == null) {
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] faction ID not found: " + var2);
            }

            if (var6 == null) {
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] faction ID not found: " + var3);
            }

         }
      }
   }

   private void factionRemoveMember(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         PlayerState var3 = var1.getPlayerFromName(var2);
         Faction var4;
         if ((var4 = var1.getFactionManager().getFaction(var3.getFactionId())) != null) {
            if ((FactionPermission)var4.getMembersUID().get(var3.getName()) != null) {
               var4.removeMember(var2, var1.getGameState());
            } else {
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] player is not part of the faction " + var4.getName());
            }
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] player is not in a faction; fid: " + var3.getFactionId());
         }
      } catch (PlayerNotFountException var5) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player not found for your client");
      }
   }

   private void factionsReinstitude(GameServerState var1) throws IOException {
      var1.setFactionReinstitudeFlag(true);
   }

   private void giveSlot(GameServerState var1) throws IOException {
      try {
         Integer var2 = (Integer)this.commandParams[0];
         short var3;
         PlayerState var8;
         if (ElementKeyMap.isValidType(var3 = (var8 = var1.getPlayerFromStateId(this.client.getId())).getInventory().getType(var8.getSelectedBuildSlot()))) {
            int var10 = var8.getInventory().incExistingOrNextFreeSlot(var3, var2);
            var8.sendInventoryModification(var10, Long.MIN_VALUE);
         } else {
            InventorySlot var9;
            if (var3 == -32768 && (var9 = var8.getInventory().getSlot(var8.getSelectedBuildSlot())) != null) {
               for(int var4 = 0; var4 < var9.getSubSlots().size(); ++var4) {
                  InventorySlot var5 = (InventorySlot)var9.getSubSlots().get(var4);
                  int var6 = var8.getInventory().incExistingOrNextFreeSlot(var5.getType(), var2);
                  var8.sendInventoryModification(var6, Long.MIN_VALUE);
                  if (var4 < var9.getSubSlots().size() && var5 != var9.getSubSlots().get(var4)) {
                     --var4;
                  }
               }
            }

         }
      } catch (PlayerNotFountException var7) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var7.getMessage());
      }
   }

   private void giveLook(GameServerState var1) throws IOException {
      try {
         Integer var3 = (Integer)this.commandParams[0];
         PlayerState var2;
         SimpleTransformableSendableObject var4;
         Vector3f var5;
         if ((var4 = (var2 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable()) instanceof AbstractCharacter) {
            var5 = new Vector3f(((AbstractCharacter)var4).getHeadWorldTransform().origin);
         } else {
            var5 = new Vector3f(var4.getWorldTransform().origin);
         }

         Vector3f var6 = new Vector3f(var5);
         Vector3f var7;
         (var7 = var2.getForward(new Vector3f())).scale(5000.0F);
         var6.add(var7);
         ClosestRayResultCallback var10;
         if ((var10 = ((PhysicsExt)var1.getUniverse().getSector(var4.getSectorId()).getPhysics()).testRayCollisionPoint(var5, var6, false, (SimpleTransformableSendableObject)null, (SegmentController)null, false, true, false)).hasHit() && var10 instanceof CubeRayCastResult) {
            CubeRayCastResult var11;
            if ((var11 = (CubeRayCastResult)var10).getSegment() != null) {
               short var12 = (new SegmentPiece(var11.getSegment(), var11.getCubePos())).getType();
               this.client.serverMessage("[ADMIN COMMAND] Given object " + ElementKeyMap.toString(var12));
               int var13 = var2.getInventory().incExistingOrNextFreeSlot(var12, var3);
               var2.sendInventoryModification(var13, Long.MIN_VALUE);
            } else {
               System.err.println("[ADMINCOMMAND] giveLook: segment null");
               this.client.serverMessage("[ADMIN COMMAND] ERROR: giveLook: segment null");
            }
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] no object in line of sight");
         }
      } catch (PlayerNotFountException var8) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var8.getMessage());
      } catch (PlayerControlledTransformableNotFound var9) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var9.getMessage());
      }
   }

   private void give(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      StringBuilder var3 = new StringBuilder();

      for(int var4 = 1; var4 < this.commandParams.length - 1; ++var4) {
         var3.append((String)this.commandParams[var4]);
         if (var4 < this.commandParams.length - 2) {
            var3.append(" ");
         }
      }

      String var14 = var3.toString();
      Integer var13 = (Integer)this.commandParams[this.commandParams.length - 1];

      try {
         ArrayList var5 = new ArrayList();
         short[] var6;
         int var7 = (var6 = ElementKeyMap.typeList()).length;

         for(int var8 = 0; var8 < var7; ++var8) {
            Short var9;
            if (ElementKeyMap.getInfo(var9 = var6[var8]).getName().toLowerCase(Locale.ENGLISH).contains(var14.trim().toLowerCase(Locale.ENGLISH))) {
               if (ElementKeyMap.getInfo(var9).getName().toLowerCase(Locale.ENGLISH).equals(var14.trim().toLowerCase(Locale.ENGLISH))) {
                  var5.clear();
                  var5.add(var9);
                  break;
               }

               var5.add(var9);
            }
         }

         if (var5.size() == 1) {
            PlayerState var16;
            var7 = (var16 = var1.getPlayerFromName(var2)).getInventory().incExistingOrNextFreeSlot((Short)var5.get(0), var13);
            var16.sendInventoryModification(var7, Long.MIN_VALUE);
         } else if (var5.isEmpty()) {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] no element starts with the string: \"" + var14 + "\"");
         } else {
            Iterator var15 = var5.iterator();

            while(var15.hasNext()) {
               short var17 = (Short)var15.next();
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] ambigous string: \"" + var14 + "\": (" + ElementKeyMap.getInfo(var17).getName() + " [" + var17 + "])" + (var15.hasNext() ? ", " : ""));
            }

            this.client.serverMessage("[ADMIN COMMAND] [ERROR] use either the classified name or the one in the parenthesis");
         }
      } catch (PlayerNotFountException var10) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var10.getMessage());
      } catch (IndexOutOfBoundsException var11) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Too many arguments");
      } catch (ElementClassNotFoundException var12) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Unknown Element" + var14);
      }
   }

   private void giveCredits(GameServerState var1) throws IOException {
      try {
         String var2 = (String)this.commandParams[0];
         Integer var3 = (Integer)this.commandParams[1];
         var1.getPlayerFromName(var2).modCreditsServer((long)var3);
      } catch (PlayerNotFountException var4) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      }
   }

   private void giveInfiniteVolume(GameServerState var1) throws IOException {
      try {
         String var2 = (String)this.commandParams[0];
         boolean var3 = (Boolean)this.commandParams[1];
         var1.getPlayerFromName(var2).setInfiniteInventoryVolume(var3);
      } catch (PlayerNotFountException var4) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      }
   }

   private void giveId(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      short var3 = ((Integer)this.commandParams[1]).shortValue();
      Integer var4 = (Integer)this.commandParams[2];

      try {
         if (ElementKeyMap.isValidType(var3)) {
            PlayerState var8;
            int var9 = (var8 = var1.getPlayerFromName(var2)).getInventory().incExistingOrNextFreeSlot(var3, var4);
            var8.sendInventoryModification(var9, Long.MIN_VALUE);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] invalid type " + var3);
         }
      } catch (PlayerNotFountException var5) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      } catch (IndexOutOfBoundsException var6) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Unknown Element" + var3);
      } catch (ElementClassNotFoundException var7) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Unknown Element" + var3);
      }
   }

   private void putIntoInventory(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      int var3 = (Integer)this.commandParams[1];
      int var4 = (Integer)this.commandParams[2];
      int var5 = (Integer)this.commandParams[3];
      int var6 = (Integer)this.commandParams[4];
      int var7 = (Integer)this.commandParams[5];
      if (!ElementKeyMap.isValidType(var6)) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Block ID invalid");
      } else {
         Sendable var8;
         if ((var8 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var2)) instanceof ManagedSegmentController) {
            ManagedSegmentController var9;
            Inventory var10;
            if ((var10 = (var9 = (ManagedSegmentController)var8).getManagerContainer().getInventory(new Vector3i(var3, var4, var5))) != null) {
               if (var7 < 0) {
                  IntOpenHashSet var11 = new IntOpenHashSet();
                  var10.decreaseBatch((short)var6, var7, var11);
                  var10.sendInventoryModification(var11);
                  this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Removed " + Math.abs(var7) + " of " + ElementKeyMap.toString(var6) + " from " + var9 + "; inventory " + var10 + "; affected slots " + var11);
               } else if (!var10.canPutIn((short)var6, var7)) {
                  this.client.serverMessage("[ADMIN COMMAND] [ERROR] Can't put amount of that ID into inventory");
               } else {
                  var3 = var10.incExistingOrNextFreeSlot((short)var6, var7, -1);
                  var10.sendInventoryModification(var3);
                  this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Put " + var7 + " of " + ElementKeyMap.toString(var6) + " into " + var9 + "; inventory " + var10 + "; slot " + var3);
               }
            } else {
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] No inventory found at " + new Vector3i(var3, var4, var5) + ". (hold RShift to check block coordinates of looked at block)");
            }
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] No Entity found for UID '" + var2 + "'");
         }
      }
   }

   private void readFromInventory(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      int var3 = (Integer)this.commandParams[1];
      int var4 = (Integer)this.commandParams[2];
      int var5 = (Integer)this.commandParams[3];
      Sendable var6;
      if ((var6 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var2)) instanceof ManagedSegmentController) {
         ManagedSegmentController var7;
         Inventory var9;
         if ((var9 = (var7 = (ManagedSegmentController)var6).getManagerContainer().getInventory(new Vector3i(var3, var4, var5))) != null) {
            String var8 = var7.toString() + "[" + new Vector3i(var3, var4, var5) + "]";
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Listing entity " + var8 + " inventory START");
            Iterator var10 = var9.getSlots().iterator();

            while(var10.hasNext()) {
               var4 = (Integer)var10.next();
               InventorySlot var11;
               if ((var11 = var9.getSlot(var4)) != null) {
                  this.printSlot(var8, 0, var11);
               }
            }

            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Listing entity " + var8 + " inventory END.");
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] No inventory found at " + new Vector3i(var3, var4, var5) + ". (hold RShift to check block coordinates of looked at block)");
         }
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] No Entity found for UID '" + var2 + "'");
      }
   }

   private void giveLaserWeapon(GameServerState var1, boolean var2) throws IOException {
      String var3 = (String)this.commandParams[0];

      try {
         LaserWeapon var4 = (LaserWeapon)MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.LASER.type, true);
         if (var2) {
            var4.damage = 100000000;
         }

         PlayerState var7;
         int var8 = (var7 = var1.getPlayerFromName(var3)).getInventory().getFreeSlot();
         var7.getInventory().put(var8, var4);
         var7.sendInventoryModification(var8, Long.MIN_VALUE);
      } catch (PlayerNotFountException var5) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      } catch (NoSlotFreeException var6) {
         var6.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var6.getMessage());
      }
   }

   private void giveHealWeapon(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         MetaObject var3 = MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.HEAL.type, true);
         PlayerState var6;
         int var7 = (var6 = var1.getPlayerFromName(var2)).getInventory().getFreeSlot();
         var6.getInventory().put(var7, var3);
         var6.sendInventoryModification(var7, Long.MIN_VALUE);
      } catch (PlayerNotFountException var4) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      } catch (NoSlotFreeException var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      }
   }

   private void giveMarkerWeapon(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         MetaObject var3 = MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.MARKER.type, true);
         PlayerState var6;
         int var7 = (var6 = var1.getPlayerFromName(var2)).getInventory().getFreeSlot();
         var6.getInventory().put(var7, var3);
         var6.sendInventoryModification(var7, Long.MIN_VALUE);
      } catch (PlayerNotFountException var4) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      } catch (NoSlotFreeException var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      }
   }

   private void giveTransporterMarkerWeapon(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         MetaObject var3 = MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.TRANSPORTER_MARKER.type, true);
         PlayerState var6;
         int var7 = (var6 = var1.getPlayerFromName(var2)).getInventory().getFreeSlot();
         var6.getInventory().put(var7, var3);
         var6.sendInventoryModification(var7, Long.MIN_VALUE);
      } catch (PlayerNotFountException var4) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      } catch (NoSlotFreeException var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      }
   }

   private void giveSniperWeapon(GameServerState var1, boolean var2) throws IOException {
      String var3 = (String)this.commandParams[0];

      try {
         SniperRifle var4 = (SniperRifle)MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.SNIPER_RIFLE.type, true);
         PlayerState var7 = var1.getPlayerFromName(var3);
         if (var2) {
            var4.damage = 100000000;
            var4.speed = 220.0F;
            var4.reload = 0.5F;
            var4.distance = 2000.0F;
         }

         int var8 = var7.getInventory().getFreeSlot();
         var7.getInventory().put(var8, var4);
         var7.sendInventoryModification(var8, Long.MIN_VALUE);
      } catch (PlayerNotFountException var5) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      } catch (NoSlotFreeException var6) {
         var6.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var6.getMessage());
      }
   }

   private void giveGrappleWeapon(GameServerState var1, boolean var2) throws IOException {
      String var3 = (String)this.commandParams[0];

      try {
         GrappleBeam var4 = (GrappleBeam)MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.GRAPPLE.type, true);
         PlayerState var7 = var1.getPlayerFromName(var3);
         if (var2) {
            var4.reload = 0.5F;
            var4.distance = 2000.0F;
         }

         int var8 = var7.getInventory().getFreeSlot();
         var7.getInventory().put(var8, var4);
         var7.sendInventoryModification(var8, Long.MIN_VALUE);
      } catch (PlayerNotFountException var5) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      } catch (NoSlotFreeException var6) {
         var6.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var6.getMessage());
      }
   }

   private void giveTorchWeapon(GameServerState var1, boolean var2) throws IOException {
      String var3 = (String)this.commandParams[0];

      try {
         TorchBeam var4 = (TorchBeam)MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.TORCH.type, true);
         PlayerState var7 = var1.getPlayerFromName(var3);
         if (var2) {
            var4.damage = 100000000;
            var4.reload = 0.5F;
            var4.distance = 2000.0F;
         }

         int var8 = var7.getInventory().getFreeSlot();
         var7.getInventory().put(var8, var4);
         var7.sendInventoryModification(var8, Long.MIN_VALUE);
      } catch (PlayerNotFountException var5) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      } catch (NoSlotFreeException var6) {
         var6.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var6.getMessage());
      }
   }

   private void giveRocketLauncherWeapon(GameServerState var1, int var2) throws IOException {
      String var3 = (String)this.commandParams[0];

      try {
         RocketLauncherWeapon var4 = (RocketLauncherWeapon)MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.ROCKET_LAUNCHER.type, true);
         PlayerState var7 = var1.getPlayerFromName(var3);
         if (var2 == 1) {
            var4.damage = 100000;
            var4.radius = 8.0F;
            var4.speed = 220.0F;
            var4.reload = 1000;
            var4.distance = 2000.0F;
         } else if (var2 == 2) {
            var4.damage = 100000000;
            var4.radius = 50.0F;
            var4.speed = 220.0F;
            var4.reload = 1000;
            var4.distance = 2000.0F;
         }

         var2 = var7.getInventory().getFreeSlot();
         var7.getInventory().put(var2, var4);
         var7.sendInventoryModification(var2, Long.MIN_VALUE);
      } catch (PlayerNotFountException var5) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      } catch (NoSlotFreeException var6) {
         var6.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var6.getMessage());
      }
   }

   private void givePowerSupplyWeapon(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         MetaObject var3 = MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.POWER_SUPPLY.type, true);
         PlayerState var6;
         int var7 = (var6 = var1.getPlayerFromName(var2)).getInventory().getFreeSlot();
         var6.getInventory().put(var7, var3);
         var6.sendInventoryModification(var7, Long.MIN_VALUE);
      } catch (PlayerNotFountException var4) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      } catch (NoSlotFreeException var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      }
   }

   private void giveMetaItem(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      String var3 = (String)this.commandParams[1];

      try {
         MetaObject var4 = null;
         var3 = var3.trim().toLowerCase(Locale.ENGLISH);
         MetaObjectManager.MetaObjectType[] var5;
         int var6 = (var5 = MetaObjectManager.MetaObjectType.values()).length;

         int var7;
         for(var7 = 0; var7 < var6; ++var7) {
            MetaObjectManager.MetaObjectType var8;
            if ((var8 = var5[var7]).name().toLowerCase(Locale.ENGLISH).equals(var3)) {
               var4 = MetaObjectManager.instantiate(var8, (short)-1, true);
            }
         }

         if (var4 == null) {
            MetaObjectManager.MetaObjectType var12 = MetaObjectManager.MetaObjectType.WEAPON;
            Weapon.WeaponSubType[] var14;
            var7 = (var14 = Weapon.WeaponSubType.values()).length;

            for(int var15 = 0; var15 < var7; ++var15) {
               Weapon.WeaponSubType var9;
               if ((var9 = var14[var15]).name().toLowerCase(Locale.ENGLISH).equals(var3)) {
                  var4 = MetaObjectManager.instantiate(var12, var9.type, true);
               }
            }
         }

         if (var4 != null) {
            PlayerState var13;
            var6 = (var13 = var1.getPlayerFromName(var2)).getInventory().getFreeSlot();
            var13.getInventory().put(var6, var4);
            var13.sendInventoryModification(var6, Long.MIN_VALUE);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] meta item type not known");
         }
      } catch (PlayerNotFountException var10) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var10.getMessage());
      } catch (NoSlotFreeException var11) {
         var11.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var11.getMessage());
      }
   }

   private void giveRecipe(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      int var3 = (Integer)this.commandParams[1];

      try {
         if (ElementKeyMap.isValidType((short)var3)) {
            Recipe var9 = RecipeCreator.getRecipeFor((short)var3);
            System.err.println("Admin created recipe: " + var9);
            PlayerState var7;
            int var8 = (var7 = var1.getPlayerFromName(var2)).getInventory().getFreeSlot();
            var7.getInventory().put(var8, var9);
            var7.sendInventoryModification(var8, Long.MIN_VALUE);
         } else {
            System.err.println("[ADMIN COMMAND] [ERROR] " + var3 + " is not a valid type");
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var3 + " is not a valid type");
         }
      } catch (PlayerNotFountException var4) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      } catch (NoSlotFreeException var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      } catch (InvalidFactoryParameterException var6) {
         var6.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var6.getMessage());
      }
   }

   private void godMode(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      Boolean var3 = (Boolean)this.commandParams[1];

      try {
         PlayerState var10000 = var1.getPlayerFromName(var2);
         var1 = null;
         var10000.setGodMode(var3);
         if (var3) {
            this.client.serverMessage("[ADMIN COMMAND] activated godmode for " + var2);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] deactivated godmode for " + var2);
         }
      } catch (PlayerNotFountException var4) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      }
   }

   private void creativeMode(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      Boolean var3 = (Boolean)this.commandParams[1];

      try {
         PlayerState var10000 = var1.getPlayerFromName(var2);
         var1 = null;
         var10000.setHasCreativeMode(var3);
         if (var3) {
            this.client.serverMessage("[ADMIN COMMAND] activated creative mode for " + var2);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] deactivated creative mode for " + var2);
         }
      } catch (PlayerNotFountException var4) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      }
   }

   private void importSectorBulk(GameServerState var1) {
      String var2 = (String)this.commandParams[0];
      var1.scheduleSectorBulkImport(this.client, var2);
   }

   private void importSectorNorm(GameServerState var1) {
      Vector3i var2 = new Vector3i((Integer)this.commandParams[0], (Integer)this.commandParams[1], (Integer)this.commandParams[2]);
      String var3 = (String)this.commandParams[3];
      var1.scheduleSectorImport(var2, this.client, var3);
   }

   private void delayAutosave(GameServerState var1) {
      int var2 = (Integer)this.commandParams[0];
      var1.delayAutosave = System.currentTimeMillis() + (long)(var2 * 1000);
   }

   private void initiateWave(GameServerState var1, Int2ObjectOpenHashMap var2, int var3) throws IOException {
      int var11 = (Integer)this.commandParams[0];
      int var4 = (Integer)this.commandParams[1];
      int var5 = (Integer)this.commandParams[2];

      try {
         PlayerState var6;
         (var6 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable();
         var1.getController().initiateWave(var3, var5, var11, var4, BluePrintController.active, var6.getCurrentSector());
      } catch (PlayerNotFountException var7) {
         var7.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var7.getMessage());
      } catch (PlayerControlledTransformableNotFound var8) {
         var8.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var8.getMessage());
      } catch (EntityNotFountException var9) {
         var9.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var9.getMessage());
      } catch (EntityAlreadyExistsException var10) {
         var10.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var10.getMessage());
      }
   }

   private void invisibilityMode(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      Boolean var3 = (Boolean)this.commandParams[1];

      try {
         PlayerState var10000 = var1.getPlayerFromName(var2);
         var1 = null;
         var10000.setInvisibilityMode(var3);
         if (var3) {
            this.client.serverMessage("[ADMIN COMMAND] activated invisibility for " + var2);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] deactivated invisibility for " + var2);
         }
      } catch (PlayerNotFountException var4) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      }
   }

   private void sectorSize(GameServerState var1) throws IOException {
      int var2 = (Integer)this.commandParams[0];
      ServerConfig.SECTOR_SIZE.setCurrentState(var2);
      var1.getGameState().setSectorSize((float)var2);
      this.client.serverMessage("[ADMIN COMMAND] set sector size to " + var2);
      ServerConfig.write();
   }

   private void setJumpGateDest(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         PlayerState var3 = var1.getPlayerFromStateId(this.client.getId());
         Sendable var5;
         if ((var5 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var3.getNetworkObject().selectedEntityId.get())) != null && var5 instanceof ManagedSegmentController && ((ManagedSegmentController)var5).getManagerContainer() instanceof StationaryManagerContainer) {
            ManagerModuleCollection var6 = ((StationaryManagerContainer)((ManagedSegmentController)var5).getManagerContainer()).getWarpgate();

            for(int var7 = 0; var7 < var6.getCollectionManagers().size(); ++var7) {
               ((WarpgateCollectionManager)var6.getCollectionManagers().get(var7)).setDestination(var2, new Vector3i());
            }
         }

      } catch (PlayerNotFountException var4) {
         var4.printStackTrace();
      }
   }

   private void stopFleetDebug(GameServerState var1) throws IOException {
      try {
         PlayerState var2 = var1.getPlayerFromStateId(this.client.getId());
         Sendable var5;
         if ((var5 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var2.getNetworkObject().selectedEntityId.get())) != null && var5 instanceof Ship) {
            Ship var6;
            if ((var6 = (Ship)var5).getFleet() == null) {
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] ship " + var6 + " not in a fleet");
            } else {
               Iterator var7 = var6.getFleet().getMembers().iterator();

               FleetMember var3;
               do {
                  if (!var7.hasNext()) {
                     var6.getFleet().debugStop();
                     this.client.serverMessage("[ADMIN COMMAND] Success: Debugging Stopped for fleet");
                     return;
                  }
               } while((var3 = (FleetMember)var7.next()).isLoaded());

               this.client.serverMessage("[ADMIN COMMAND] [ERROR] All ships in fleet must be loaded. (found one not loaded: " + var3.UID + ")");
            }
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] must select a fleet ship");
         }
      } catch (PlayerNotFountException var4) {
         var4.printStackTrace();
      }
   }

   private void sendFleetDebug(GameServerState var1) throws IOException {
      int var2 = (Integer)this.commandParams[0];
      int var3 = (Integer)this.commandParams[1];
      int var4 = (Integer)this.commandParams[2];

      try {
         PlayerState var5 = var1.getPlayerFromStateId(this.client.getId());
         Sendable var7;
         if ((var7 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var5.getNetworkObject().selectedEntityId.get())) != null && var7 instanceof Ship) {
            Ship var8;
            if ((var8 = (Ship)var7).getFleet() == null) {
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] ship " + var8 + " not in a fleet");
            } else {
               var8.getFleet().debugMoveBetween(var5.getCurrentSector(), new Vector3i(var2, var3, var4));
               this.client.serverMessage("[ADMIN COMMAND] Success: Debugging started for fleet");
            }
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] must select a fleet ship");
         }
      } catch (PlayerNotFountException var6) {
         var6.printStackTrace();
      }
   }

   private void joinFaction(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      int var3 = (Integer)this.commandParams[1];

      try {
         PlayerState var5 = var1.getPlayerFromName(var2);
         this.client.serverMessage("[ADMIN COMMAND] joining factionID of " + var2 + " to " + var3);
         var5.getFactionController().forceJoinOnServer(var3);
      } catch (PlayerNotFountException var4) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player not found for your client");
      }
   }

   public ClosestRayResultCallback getPlayerBlock(GameServerState var1) throws IOException, PlayerNotFountException, PlayerControlledTransformableNotFound {
      PlayerState var2;
      SimpleTransformableSendableObject var5 = (var2 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable();
      Vector3f var3;
      Vector3f var4;
      Vector3f var6;
      if (var2.getNetworkObject().isInBuildMode.getBoolean()) {
         var3 = new Vector3f(var2.getBuildModePosition().getWorldTransform().origin);
         (var6 = GlUtil.getForwardVector(new Vector3f(), (Transform)var2.getBuildModePosition().getWorldTransform())).scale(5000.0F);
         (var4 = new Vector3f(var3)).add(var6);
      } else {
         if (var5 instanceof AbstractCharacter) {
            var3 = new Vector3f(((AbstractCharacter)var5).getHeadWorldTransform().origin);
         } else {
            var3 = new Vector3f(var5.getWorldTransform().origin);
         }

         var4 = new Vector3f(var3);
         (var6 = var2.getForward(new Vector3f())).scale(5000.0F);
         var4.add(var6);
      }

      return ((PhysicsExt)var1.getUniverse().getSector(var5.getSectorId()).getPhysics()).testRayCollisionPoint(var3, var4, false, (SimpleTransformableSendableObject)null, (SegmentController)null, false, true, false);
   }

   private void jump(GameServerState var1) throws IOException {
      try {
         PlayerState var2;
         SimpleTransformableSendableObject var3 = (var2 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable();
         ClosestRayResultCallback var6;
         if ((var6 = this.getPlayerBlock(var1)).hasHit()) {
            Vector3f var7;
            (var7 = new Vector3f(var6.hitPointWorld)).sub(var2.getForward(new Vector3f()));
            this.warpTransformable(var3, var7.x, var7.y, var7.z);
            this.client.serverMessage("[ADMIN COMMAND] Object successfully jumped to " + var7);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] no object in line of sight");
         }
      } catch (PlayerNotFountException var4) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      } catch (PlayerControlledTransformableNotFound var5) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      }
   }

   private void kick(GameServerState var1, boolean var2, int var3) throws IOException {
      this.kick(var1, var2, var3, (String)this.commandParams[0]);
   }

   private void kick(GameServerState var1, boolean var2, int var3, String var4) throws IOException {
      if (var2) {
         this.kick(var1, (String)this.commandParams[var3], var4);
      } else {
         this.kick(var1, "You have been kicked by an admin", var4);
      }
   }

   private void kick(GameServerState var1, String var2) throws IOException {
      this.kick(var1, var2, (String)this.commandParams[0]);
   }

   private void kick(GameServerState var1, String var2, String var3) throws IOException {
      try {
         PlayerState var5 = var1.getPlayerFromName(var3);
         var1.getController().sendLogout(var5.getClientId(), var2);
      } catch (PlayerNotFountException var4) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      }
   }

   private void resetIntegrityTimeout(GameServerState var1) throws IOException {
      SegmentController var2;
      if ((var2 = this.getSelectedOrEnteredStructure(var1)) instanceof ManagedSegmentController) {
         ((ManagedSegmentController)var2).getManagerContainer().resetIntegrityDelay();
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] reset integrity timer for " + var2);
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] no selected or entered entity");
      }
   }

   private void resetRepairTimeout(GameServerState var1) throws IOException {
      SegmentController var2;
      if ((var2 = this.getSelectedOrEnteredStructure(var1)) instanceof ManagedSegmentController) {
         ((ManagedSegmentController)var2).getManagerContainer().resetRepairDelay();
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] reset block repair timer for " + var2);
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] no selected or entered entity");
      }
   }

   private void killCharacter(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         PlayerState var4;
         (var4 = var1.getPlayerFromName(var2)).suicideOnServer();
         var4.sendServerMessage(new ServerMessage(new Object[]{472}, 3, var4.getId()));
      } catch (PlayerNotFountException var3) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var3.getMessage());
      }
   }

   private void listAdmins(GameServerState var1) throws IOException {
      String var2 = null;
      synchronized(var1.getAdmins()) {
         var2 = var1.getAdmins().toString();
      }

      this.client.serverMessage("Admins: " + var2);
   }

   private void listBannedIps(GameServerState var1) throws IOException {
      String var2 = null;
      synchronized(var1.getBlackListedIps()) {
         var2 = var1.getBlackListedIps().toString();
      }

      this.client.serverMessage("Banned: " + var2);
   }

   private void listBannedNames(GameServerState var1) throws IOException {
      String var2 = null;
      synchronized(var1.getBlackListedNames()) {
         var2 = var1.getBlackListedNames().toString();
      }

      this.client.serverMessage("Banned: " + var2);
   }

   private void listBannedAccounts(GameServerState var1) throws IOException {
      String var2 = null;
      synchronized(var1.getBlackListedAccounts()) {
         var2 = var1.getBlackListedAccounts().toString();
      }

      this.client.serverMessage("Banned: " + var2);
   }

   private void listControlUnits(GameServerState var1) throws IOException {
      try {
         synchronized(var1.getLocalAndRemoteObjectContainer().getLocalObjects()) {
            Iterator var3 = var1.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

            while(var3.hasNext()) {
               Sendable var4;
               if ((var4 = (Sendable)var3.next()) instanceof PlayerControllable && ((PlayerControllable)var4).getAttachedPlayers().size() > 0) {
                  this.client.serverMessage(var4 + ": " + ((PlayerControllable)var4).getAttachedPlayers());
               }
            }

            this.client.serverMessage("----BY CONTROLLABLE----");
            var3 = var1.getPlayerStatesByName().values().iterator();

            while(var3.hasNext()) {
               PlayerState var7 = (PlayerState)var3.next();
               this.client.serverMessage(var7.getControllerState().getUnits().toString());
            }

            this.client.serverMessage("----BY UNIT----");
         }
      } catch (Exception var6) {
         var6.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR]: " + var6.getClass() + ": " + var6.getMessage() + ". ");
      }
   }

   private void getPlayerSpawn(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         PlayerState var4;
         PlayerStateSpawnData var5;
         if (!(var5 = (var4 = var1.getPlayerFromName(var2)).getSpawn()).deathSpawn.UID.equals("")) {
            this.client.serverMessage("[ADMINCOMMAND][SPAWN][SUCCESS] " + var4 + " spawn currently relative; UID: " + var5.deathSpawn.UID + "; local position: " + var5.deathSpawn.localPos);
         } else {
            this.client.serverMessage("[ADMINCOMMAND][SPAWN][SUCCESS] " + var4 + " spawn currently absolute; sector: " + var5.deathSpawn.absoluteSector + "; local position: " + var5.deathSpawn.localPos);
         }
      } catch (PlayerNotFountException var3) {
         var3.printStackTrace();
         this.client.serverMessage("[ADMINCOMMAND][SPAWN] Player not found");
      }
   }

   private void setPlayerSpawnTo(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      Vector3i var3 = new Vector3i((Integer)this.commandParams[1], (Integer)this.commandParams[2], (Integer)this.commandParams[3]);
      Vector3f var4 = new Vector3f((Float)this.commandParams[4], (Float)this.commandParams[5], (Float)this.commandParams[6]);

      try {
         PlayerState var6;
         PlayerStateSpawnData var7;
         (var7 = (var6 = var1.getPlayerFromName(var2)).getSpawn()).deathSpawn.UID = "";
         var7.deathSpawn.absoluteSector.set(var3);
         var7.deathSpawn.localPos.set(var4);
         var7.deathSpawn.gravityAcceleration.set(0.0F, 0.0F, 0.0F);
         this.client.serverMessage("[ADMINCOMMAND][SPAWN][SUCCESS] set spawn of player " + var6 + " to sector " + var3 + "; local position: " + var4);
      } catch (PlayerNotFountException var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMINCOMMAND][SPAWN] Player not found");
      }
   }

   private void listFactions(GameServerState var1) throws IOException {
      Int2ObjectOpenHashMap var2 = var1.getGameState().getFactionManager().getFactionMap();
      this.client.serverMessage("FACTION_LIST START");
      ObjectArrayList var3 = new ObjectArrayList();
      Iterator var5 = var2.values().iterator();

      while(var5.hasNext()) {
         Faction var4 = (Faction)var5.next();
         var1.getDatabaseIndex().getTableManager().getSystemTable().getSystemsByFaction(var4.getIdFaction(), var3);
         this.client.serverMessage("FACTION: " + var4 + "; HomeBaseName: " + var4.getHomebaseRealName() + "; HomeBaseUID: " + var4.getHomebaseUID() + "; HomeBaseLocation: " + var4.getHomeSector() + "; Owned: " + var3);
         var3.clear();
      }

      this.client.serverMessage("FACTION_LIST END");
   }

   private void listBlueprints(GameServerState var1, boolean var2, boolean var3) throws IOException {
      String var4 = null;
      if (var2) {
         var4 = ((String)this.commandParams[0]).toLowerCase(Locale.ENGLISH);
      }

      Collection var10 = var1.getCatalogManager().getCatalog();
      this.client.serverMessage("[CATALOG] START");
      int var5 = 0;
      Iterator var11 = var10.iterator();

      while(true) {
         CatalogPermission var6;
         do {
            if (!var11.hasNext()) {
               this.client.serverMessage("[CATALOG] END");
               return;
            }

            var6 = (CatalogPermission)var11.next();
         } while(var2 && !var4.equals(var6.ownerUID.toLowerCase(Locale.ENGLISH)));

         if (var3) {
            try {
               BlueprintEntry var7 = BluePrintController.active.getBlueprint(var6.getUid());
               BoundingBox var8 = new BoundingBox();
               var7.calculateTotalBb(var8);
               this.client.serverMessage("[CATALOG] INDEX " + var5 + ": " + var6.getUid() + "; Owner: " + var6.ownerUID + "; Type: " + var6.type.name() + "; Mass: " + var7.getMass() + "; DimensionInclChields: " + var8 + "; BlocksInclChilds: " + var7.getElementCountMapWithChilds().getTotalAmount());
            } catch (EntityNotFountException var9) {
               var9.printStackTrace();
               this.client.serverMessage("[CATALOG] INDEX " + var5 + ": " + var6.getUid() + " ERROR. BLUEPRINT MISSING");
            }
         } else {
            this.client.serverMessage("[CATALOG] INDEX " + var5 + ": " + var6.getUid());
         }

         ++var5;
      }
   }

   private void listWhiteIps(GameServerState var1) throws IOException {
      String var2 = null;
      synchronized(var1.getWhiteListedIps()) {
         var2 = var1.getWhiteListedIps().toString();
      }

      this.client.serverMessage("Whitelisted: " + var2);
   }

   private void listWhiteAccounts(GameServerState var1) throws IOException {
      String var2 = null;
      synchronized(var1.getWhiteListedAccounts()) {
         var2 = var1.getWhiteListedAccounts().toString();
      }

      this.client.serverMessage("Whitelisted: " + var2);
   }

   private void listWhiteNames(GameServerState var1) throws IOException {
      String var2 = null;
      synchronized(var1.getWhiteListedNames()) {
         var2 = var1.getWhiteListedNames().toString();
      }

      this.client.serverMessage("Whitelisted: " + var2);
   }

   private void loadRange(GameServerState var1) throws IOException {
      Vector3i var2 = new Vector3i((Integer)this.commandParams[0], (Integer)this.commandParams[1], (Integer)this.commandParams[2]);
      Vector3i var3 = new Vector3i((Integer)this.commandParams[3], (Integer)this.commandParams[4], (Integer)this.commandParams[5]);

      try {
         ObjectArrayFIFOQueue var4 = new ObjectArrayFIFOQueue(4096);

         for(int var5 = var2.z; var5 <= var3.z; ++var5) {
            for(int var6 = var2.y; var6 <= var3.y; ++var6) {
               for(int var7 = var2.x; var7 <= var3.x; ++var7) {
                  Vector3i var8 = new Vector3i(var7, var6, var5);
                  var4.enqueue(var8);
               }
            }
         }

         var1.toLoadSectorsQueue = var4;
      } catch (Exception var9) {
         var9.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] critical: " + var9.getClass().getSimpleName() + ": " + var9.getMessage());
      }
   }

   private void loadShip(GameServerState var1, BluePrintController var2, boolean var3, boolean var4) throws IOException {
      String var5 = (String)this.commandParams[0];
      String var6 = (String)this.commandParams[1];
      int var7 = 0;
      if (var4) {
         var7 = (Integer)this.commandParams[2];
      }

      if (!EntityRequest.isShipNameValid(var6)) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Invalid Ship name (Only Characters And numbers and -_ allowed)");
      } else {
         Transform var22;
         (var22 = new Transform()).setIdentity();

         try {
            PlayerState var8;
            SimpleTransformableSendableObject var9 = (var8 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable();
            var22.origin.set(var9.getWorldTransform().origin);
            ByteBuffer var23 = ByteBuffer.allocate(10240);
            if (var2 == BluePrintController.stationsTradingGuild) {
               var7 = -2;
            }

            if (var2 == BluePrintController.stationsPirate) {
               var7 = -1;
            }

            SegmentPiece var10 = null;
            if (var3) {
               try {
                  label77: {
                     ClosestRayResultCallback var19;
                     if ((var19 = this.getPlayerBlock(var1)).hasHit() && var19 instanceof CubeRayCastResult && ((CubeRayCastResult)var19).getSegment() != null) {
                        CubeRayCastResult var11 = (CubeRayCastResult)var19;
                        SegmentPiece var20;
                        if ((var20 = new SegmentPiece(var11.getSegment(), var11.getCubePos())).isValid() && var20.getInfo().isRailDockable()) {
                           var10 = var20;
                           break label77;
                        }

                        this.client.serverMessage("[ADMIN COMMAND] [ERROR] selected block is not dockable");
                        return;
                     }

                     this.client.serverMessage("[ADMIN COMMAND] [ERROR] no object in line of sight");
                     return;
                  }
               } catch (PlayerNotFountException var13) {
                  this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var13.getMessage());
               } catch (PlayerControlledTransformableNotFound var14) {
                  this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var14.getMessage());
               }
            }

            SegmentControllerOutline var21;
            (var21 = var2.loadBluePrint(var1, var5, var6, var22, -1, var7, var8.getCurrentSector(), "<admin>", var23, var10, false, new ChildStats(false))).scrap = var2 == BluePrintController.stationsNeutral;
            var21.shop = var2 == BluePrintController.stationsTradingGuild;
            synchronized(var1.getBluePrintsToSpawn()) {
               var1.getBluePrintsToSpawn().add(var21);
            }

            System.err.println("[ADMIN] LOADING " + var21.getClass());
         } catch (EntityNotFountException var15) {
            var15.printStackTrace();
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var15.getMessage());
         } catch (EntityAlreadyExistsException var16) {
            var16.printStackTrace();
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] Entity already exists: " + var16.getMessage());
         } catch (PlayerNotFountException var17) {
            var17.printStackTrace();
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var17.getMessage());
         } catch (PlayerControlledTransformableNotFound var18) {
            var18.printStackTrace();
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var18.getMessage());
         }
      }
   }

   private void loadSystem(GameServerState var1) throws IOException {
      Vector3i var2 = new Vector3i((Integer)this.commandParams[0], (Integer)this.commandParams[1], (Integer)this.commandParams[2]);

      try {
         ObjectArrayFIFOQueue var3 = new ObjectArrayFIFOQueue(4096);

         for(int var4 = var2.z << 4; var4 < (var2.z << 4) + 16; ++var4) {
            for(int var5 = var2.y << 4; var5 < (var2.y << 4) + 16; ++var5) {
               for(int var6 = var2.x << 4; var6 < (var2.x << 4) + 16; ++var6) {
                  Vector3i var7 = new Vector3i(var6, var5, var4);
                  var3.enqueue(var7);
               }
            }
         }

         var1.toLoadSectorsQueue = var3;
      } catch (Exception var8) {
         var8.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] critical: " + var8.getClass().getSimpleName() + ": " + var8.getMessage());
      }
   }

   private void populateSector(GameServerState var1) throws IOException {
      Vector3i var2 = new Vector3i((Integer)this.commandParams[0], (Integer)this.commandParams[1], (Integer)this.commandParams[2]);

      try {
         Sector var3;
         if ((var3 = var1.getUniverse().getSector(var2)) != null) {
            var3.populate(var1);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] sector not found: " + var2 + ": " + var1.getUniverse().getSectorSet());
         }
      } catch (Exception var4) {
         var4.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] critical: " + var4.getClass().getSimpleName() + ": " + var4.getMessage());
      }
   }

   private void refreshServerMessage(GameServerState var1) throws IOException {
      var1.getGameState().readServerMessage();
   }

   private void removeAdmin(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      if (!var1.getController().removeAdmin(this.client.getPlayerName(), var2)) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] '" + var2 + "' not found in admin list (use /list_admins to check the name)");
      }

   }

   private void simulationInfo(GameServerState var1) throws IOException {
      var1.getSimulationManager().print(this.client);
   }

   private void simulationClearAll(GameServerState var1) throws IOException {
      var1.getSimulationManager().clearAll();
   }

   private void repairSector(GameServerState var1) throws IOException {
      try {
         var1.getPlayerFromStateId(this.client.getId());
         Vector3i var2 = new Vector3i((Integer)this.commandParams[0], (Integer)this.commandParams[1], (Integer)this.commandParams[2]);
         Sector var3;
         if ((var3 = var1.getUniverse().getSector(var2)) != null) {
            var3.queueRepairRequest();
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] sector repair queued: " + var2);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] sector not found: " + var2 + ": " + var1.getUniverse().getSectorSet());
         }
      } catch (PlayerNotFountException var4) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player not found for your client");
      } catch (Exception var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] server could not load sector");
      }
   }

   private void resAABB(GameServerState var1) {
      synchronized(var1.getLocalAndRemoteObjectContainer().getLocalObjects()) {
         Iterator var5 = var1.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

         while(var5.hasNext()) {
            Sendable var3;
            if ((var3 = (Sendable)var5.next()) instanceof SegmentController) {
               ((SegmentController)var3).getSegmentBuffer().restructBB();
            }
         }

      }
   }

   private void restockUid(GameServerState var1, boolean var2) throws IOException {
      String var3 = (String)this.commandParams[0];
      Object var4 = null;
      synchronized(var1.getLocalAndRemoteObjectContainer().getLocalObjects()) {
         Sendable var6;
         if ((var6 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var3)) != null) {
            if (var6 instanceof ShopSpaceStation) {
               var4 = (ShopInterface)var6;
            } else if (var6 instanceof SpaceStation) {
               var4 = ((SpaceStation)var6).getManagerContainer();
            }
         }
      }

      if (var4 == null) {
         System.err.println("[ADMIN] Shop not online: " + var3 + "; checking logged off");
         FileExt var5 = new FileExt(GameServerState.ENTITY_DATABASE_PATH + File.separator + var3 + ".ent");

         try {
            Tag var15 = Tag.readFrom(new BufferedInputStream(new FileInputStream(var5)), true, false);
            Tag var7 = null;
            if ("ShopSpaceStation2".equals(var15.getName())) {
               ((ShopSpaceStation)(var4 = new ShopSpaceStation(var1))).initialize();
               ((ShopSpaceStation)var4).fromTagStructure(var15);
               ((ShopInterface)var4).fillInventory(false, var2);
               var7 = ((ShopSpaceStation)var4).toTagStructure();
            } else if ("SpaceStation".equals(var15.getName())) {
               SpaceStation var13;
               (var13 = new SpaceStation(var1)).initialize();
               var13.fromTagStructure(var15);
               ((ShopInterface)(var4 = var13.getManagerContainer())).fillInventory(false, var2);
               var7 = ((SpaceStation)((SpaceStationManagerContainer)var4).getSegmentController()).toTagStructure();
            } else {
               System.err.println("[ADMIN] Tag type not found " + var15.getName());
            }

            if (var7 != null) {
               BufferedOutputStream var14 = new BufferedOutputStream(new FileOutputStream(var5));
               var7.writeTo(var14, true);
               this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Restocked: " + var4);
            }

         } catch (FileNotFoundException var8) {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] shop " + var3 + " not online, and no offline save state found");
            var8.printStackTrace();
         } catch (IOException var9) {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] shop " + var3 + " not online, and no offline save state reading failed");
            var9.printStackTrace();
         } catch (Exception var10) {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] shop " + var3 + ", " + var10.getClass() + ": " + var10.getMessage());
            var10.printStackTrace();
         }
      } else {
         try {
            ((ShopInterface)var4).fillInventory(true, var2);
         } catch (NoSlotFreeException var11) {
            var11.printStackTrace();
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] No more slots free " + var11.getMessage());
         }

         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Restocked: " + var4);
      }
   }

   private void restock(GameServerState var1, boolean var2) throws IOException {
      try {
         PlayerState var3 = var1.getPlayerFromStateId(this.client.getId());
         Sendable var7 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var3.getNetworkObject().selectedEntityId.get());
         ShopInterface var4 = null;
         if (var7 != null) {
            if (var7 instanceof ShopInterface) {
               var4 = (ShopInterface)var7;
            } else if (var7 instanceof ManagedSegmentController && ((ManagedSegmentController)var7).getManagerContainer() instanceof ShopInterface) {
               var4 = (ShopInterface)((ManagedSegmentController)var7).getManagerContainer();
            }
         }

         if (var4 != null) {
            var4.fillInventory(true, var2);
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Restocked: " + var7);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] No Shop Selected: " + var3.getNetworkObject().selectedEntityId.get() + "->(" + var7 + ")");
         }
      } catch (PlayerNotFountException var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      } catch (NoSlotFreeException var6) {
         var6.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] No more slots free " + var6.getMessage());
      }
   }

   private void shopSetInfinite(GameServerState var1) throws IOException {
      try {
         PlayerState var2 = var1.getPlayerFromStateId(this.client.getId());
         Sendable var5 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var2.getNetworkObject().selectedEntityId.get());
         ShopInterface var3 = null;
         if (var5 != null) {
            if (var5 instanceof ShopInterface) {
               var3 = (ShopInterface)var5;
            } else if (var5 instanceof ManagedSegmentController && ((ManagedSegmentController)var5).getManagerContainer() instanceof ShopInterface) {
               var3 = (ShopInterface)((ManagedSegmentController)var5).getManagerContainer();
            }
         }

         if (var3 != null) {
            var3.getShoppingAddOn().setInfiniteSupply(!var3.getShoppingAddOn().isInfiniteSupply());
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Shop infinite flag set to: " + var3.getShoppingAddOn().isInfiniteSupply() + " on " + var5);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] No Shop Selected: " + var2.getNetworkObject().selectedEntityId.get() + "->(" + var5 + ")");
         }
      } catch (PlayerNotFountException var4) {
         var4.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      }
   }

   private void save(GameServerState var1) {
      var1.getController().triggerForcedSave();
   }

   private void executeGraphicsEffect(GameServerState var1) throws IOException {
      byte var2 = ((Integer)this.commandParams[0]).byteValue();

      try {
         PlayerState var3;
         SimpleTransformableSendableObject var4;
         if ((var4 = (var3 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable()) == null || !(var4 instanceof SegmentController)) {
            System.err.println("[ADMIN COMMAND]checking selected");
            Sendable var7;
            if ((var7 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var3.getNetworkObject().selectedEntityId.get())) != null && var7 instanceof SimpleTransformableSendableObject) {
               var4 = (SimpleTransformableSendableObject)var7;
            }
         }

         var4.executeGraphicalEffectServer(var2);
      } catch (PlayerNotFountException var5) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      } catch (PlayerControlledTransformableNotFound var6) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var6.getMessage());
      }
   }

   public SimpleTransformableSendableObject getSelfOrSelectedObject(GameServerState var1) throws PlayerNotFountException {
      PlayerState var2;
      SimpleTransformableSendableObject var3;
      if ((var3 = (var2 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformableWOExc()) == null || !(var3 instanceof SegmentController)) {
         System.err.println("[ADMIN COMMAND]checking selected");
         Sendable var4;
         if ((var4 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var2.getNetworkObject().selectedEntityId.get())) != null && var4 instanceof SimpleTransformableSendableObject) {
            var3 = (SimpleTransformableSendableObject)var4;
         }
      }

      return var3;
   }

   public SimpleTransformableSendableObject getSelectedObject(GameServerState var1) throws IOException {
      try {
         PlayerState var2 = var1.getPlayerFromStateId(this.client.getId());
         System.err.println("[ADMIN COMMAND]checking selected");
         Sendable var4;
         if ((var4 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var2.getNetworkObject().selectedEntityId.get())) != null && var4 instanceof SimpleTransformableSendableObject) {
            return (SimpleTransformableSendableObject)var4;
         }
      } catch (PlayerNotFountException var3) {
         var3.printStackTrace();
      }

      this.client.serverMessage("[ADMIN COMMAND] [ERROR] Nothing selected");
      return null;
   }

   private void getEntityInfo(GameServerState var1) throws IOException {
      SimpleTransformableSendableObject var2;
      if ((var2 = this.getSelectedObject(var1)) != null) {
         this.client.serverMessage(var2.getInfo());
         this.printInfo(var1, var2);
      }

   }

   private void saveShipAs(GameServerState var1) throws IOException {
      String var2;
      if (EntityRequest.isShipNameValid(var2 = (String)this.commandParams[0]) && var2.length() <= 48) {
         BlueprintClassification var3;
         if ((var3 = BlueprintClassification.valueOf(((String)this.commandParams[1]).trim().toUpperCase(Locale.ENGLISH))) == null) {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] Invalid classification (list in the catalog save dialog)");
         } else {
            this.saveShip(var1, var2, var3);
         }
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Invalid Ship name (Only Characters And numbers and -_ allowed) (max 48)");
      }
   }

   private void blueprintInfo(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      String var3;
      if ((var3 = var1.getCatalogManager().serverGetInfo(var2)) != null) {
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Blueprint info on: " + var2 + "\n" + var3);
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] blueprint not found (name is case sensitive): " + var2);
      }
   }

   private void blueprintDelete(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      if (var1.getCatalogManager().serverDeletEntry(var2)) {
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Removing blueprint: " + var2);
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] blueprint not found (name is case sensitive): " + var2);
      }
   }

   private void blueprintSetOwner(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      String var3 = (String)this.commandParams[1];
      if (var1.getCatalogManager().serverChangeOwner(var2, var3)) {
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Changing blueprint owner for " + var2 + " to " + var3);
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] blueprint not found (name is case sensitive): " + var2);
      }
   }

   private void saveShip(GameServerState var1) throws IOException {
      String var2;
      if (EntityRequest.isShipNameValid(var2 = (String)this.commandParams[0]) && var2.length() <= 48) {
         this.saveShip(var1, var2, (BlueprintClassification)null);
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Invalid Ship name (Only Characters And numbers and -_ allowed) (max 48)");
      }
   }

   private void saveShip(GameServerState var1, String var2, BlueprintClassification var3) throws IOException {
      try {
         PlayerState var4;
         SimpleTransformableSendableObject var5;
         if ((var5 = (var4 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable()) == null || !(var5 instanceof SegmentController)) {
            System.err.println("[ADMIN COMMAND]checking selected");
            Sendable var6;
            if ((var6 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var4.getNetworkObject().selectedEntityId.get())) != null && var6 instanceof SimpleTransformableSendableObject) {
               var5 = (SimpleTransformableSendableObject)var6;
            }
         }

         if (var5 instanceof SegmentController) {
            SegmentController var9;
            (var9 = (SegmentController)var5).writeAllBufferedSegmentsToDatabase(true, false, false);
            BluePrintController.active.writeBluePrint(var9, var2, false, var3 != null ? var3 : var9.getType().getDefaultClassification());
            if (var1.getCatalogManager().writeEntryAdmin(var9, var2, var4.getName(), var9.getType().getDefaultClassification(), true)) {
               this.client.serverMessage("[ADMIN COMMAND] successfully saved ship in catalog as \"" + var2 + "\"\n");
            } else {
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] FAILED saving ship in catalog as \"" + var2 + "\"\n");
            }
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR]  not inside or selected any entity");
         }
      } catch (PlayerNotFountException var7) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var7.getMessage());
      } catch (PlayerControlledTransformableNotFound var8) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var8.getMessage());
      }
   }

   public SegmentController getSelectedOrEnteredStructure(GameServerState var1) {
      return getSelectedOrEnteredStructure(var1, this.client);
   }

   public static SegmentController getSelectedOrEnteredStructure(GameServerState var0, RegisteredClientInterface var1) {
      try {
         PlayerState var6 = var0.getPlayerFromStateId(var1.getId());
         SimpleTransformableSendableObject var2 = null;
         Sendable var5;
         if ((var5 = (Sendable)var0.getLocalAndRemoteObjectContainer().getLocalObjects().get(var6.getNetworkObject().selectedEntityId.get())) != null && var5 instanceof SimpleTransformableSendableObject) {
            var2 = (SimpleTransformableSendableObject)var5;
         }

         if (var2 == null || !(var2 instanceof SegmentController)) {
            var2 = var6.getFirstControlledTransformable();
         }

         if (var2 instanceof SegmentController) {
            return (SegmentController)var2;
         }
      } catch (PlayerNotFountException var3) {
      } catch (PlayerControlledTransformableNotFound var4) {
      }

      return null;
   }

   private void resetDock(GameServerState var1, boolean var2) throws IOException {
      try {
         PlayerState var3;
         SimpleTransformableSendableObject var4;
         if ((var4 = (var3 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable()) == null || !(var4 instanceof SegmentController)) {
            System.err.println("[ADMIN COMMAND]checking selected");
            Sendable var7;
            if ((var7 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var3.getNetworkObject().selectedEntityId.get())) != null && var7 instanceof SimpleTransformableSendableObject) {
               var4 = (SimpleTransformableSendableObject)var7;
            }
         }

         if (var4 instanceof SegmentController) {
            if (var2) {
               ((SegmentController)var4).railController.resetRailAll();
            } else {
               ((SegmentController)var4).railController.resetRail();
            }
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] not inside or selected any entity");
         }
      } catch (PlayerNotFountException var5) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      } catch (PlayerControlledTransformableNotFound var6) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var6.getMessage());
      }
   }

   private void saveShipUid(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      String var3;
      if (EntityRequest.isShipNameValid(var3 = (String)this.commandParams[1]) && var3.length() <= 48) {
         Sendable var4;
         if ((var4 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var2)) != null && var4 instanceof SegmentController) {
            SegmentController var5;
            (var5 = (SegmentController)var4).writeAllBufferedSegmentsToDatabase(true, false, false);
            BluePrintController.active.writeBluePrint(var5, var3, false, var5.getType().getDefaultClassification());
            if (var1.getCatalogManager().writeEntryAdmin(var5, var3, "ADMIN", var5.getType().getDefaultClassification(), true)) {
               this.client.serverMessage("[ADMIN COMMAND] successfully saved entity in catalog as \"" + var3 + "\"\n");
            } else {
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] FAILED saving ship in catalog as \"" + var3 + "\"\n");
            }
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] uid " + var2 + " not found or loaded");
         }
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Invalid Ship name (Only Characters And numbers and -_ allowed) (max 48)");
      }
   }

   private void search(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      ArrayList var3 = Lists.newArrayList();
      Iterator var4 = var1.getSegmentControllersByName().entrySet().iterator();

      while(true) {
         Entry var5;
         do {
            do {
               if (!var4.hasNext()) {
                  try {
                     Iterator var11 = var1.getDatabaseIndex().getTableManager().getEntityTable().getByName("%" + DatabaseIndex.escape(var2) + "%", 20).iterator();

                     label51:
                     while(var11.hasNext()) {
                        DatabaseEntry var9 = (DatabaseEntry)var11.next();
                        Iterator var6 = var1.getSegmentControllersByName().entrySet().iterator();

                        while(true) {
                           Entry var7;
                           do {
                              if (!var6.hasNext()) {
                                 var3.add(new AdminCommandQueueElement.SearchResult(var9.realName, var9.uid, var9.sectorPos));
                                 continue label51;
                              }
                           } while(!((var7 = (Entry)var6.next()).getValue() instanceof Ship) && !(var7.getValue() instanceof SpaceStation));

                           if (((SegmentController)var7.getValue()).getUniqueIdentifier().equals(var9.uid)) {
                              break label51;
                           }
                        }
                     }

                     if (var3.isEmpty()) {
                        this.client.serverMessage("[ADMIN COMMAND] No matches found for '" + var2 + "'");
                        return;
                     }

                     var11 = var3.iterator();

                     while(var11.hasNext()) {
                        AdminCommandQueueElement.SearchResult var10 = (AdminCommandQueueElement.SearchResult)var11.next();
                        this.client.serverMessage("FOUND: " + var10.realName + " -> " + var10.position);
                     }

                     return;
                  } catch (SQLException var8) {
                     var8.printStackTrace();
                     this.client.serverMessage("[ADMIN COMMAND] [ERROR] SQL EXCEPTION");
                     return;
                  }
               }
            } while(!((SegmentController)(var5 = (Entry)var4.next()).getValue()).getRealName().toUpperCase(Locale.ENGLISH).contains(var2.toUpperCase(Locale.ENGLISH)));
         } while(!(var5.getValue() instanceof Ship) && !(var5.getValue() instanceof SpaceStation));

         var3.add(new AdminCommandQueueElement.SearchResult(((SegmentController)var5.getValue()).getRealName(), ((SegmentController)var5.getValue()).getUniqueIdentifier(), var1.getUniverse().getSector(((SegmentController)var5.getValue()).getSectorId()).pos));
      }
   }

   private void setAllRelations(GameServerState var1) {
      String var2;
      if ((var2 = (String)this.commandParams[0]).toLowerCase(Locale.ENGLISH).trim().equals("ally")) {
         var1.getFactionManager().setAllRelations(FactionRelation.RType.FRIEND.code);
      } else if (var2.toLowerCase(Locale.ENGLISH).trim().equals("neutral")) {
         var1.getFactionManager().setAllRelations(FactionRelation.RType.NEUTRAL.code);
      } else {
         if (var2.toLowerCase(Locale.ENGLISH).trim().equals("enemy")) {
            var1.getFactionManager().setAllRelations(FactionRelation.RType.ENEMY.code);
         }

      }
   }

   private void setDebugMode(GameServerState var1) throws IOException {
      int var3 = (Integer)this.commandParams[0];

      try {
         PlayerState var2 = var1.getPlayerFromStateId(this.client.getId());
         Sendable var5;
         if ((var5 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var2.getNetworkObject().selectedEntityId.get())) != null && var5 instanceof SimpleTransformableSendableObject) {
            ((SimpleTransformableSendableObject)var5).setDebugMode((byte)var3);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] No Entity Selected: " + var2.getNetworkObject().selectedEntityId.get() + "->(" + var5 + ")");
         }
      } catch (PlayerNotFountException var4) {
         var4.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      }
   }

   private void unsuspendFaction(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         PlayerState var4;
         (var4 = var1.getPlayerFromName(var2)).getFactionController().unsuspendFaction(var1, var4);
         this.client.serverMessage("[ADMIN COMMAND] unsuspended faction for " + var4);
      } catch (PlayerNotFountException var3) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player not found for your client");
      }
   }

   private void suspendFaction(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         PlayerState var4;
         (var4 = var1.getPlayerFromName(var2)).getFactionController().suspendFaction(var1, var4);
         this.client.serverMessage("[ADMIN COMMAND] suspended faction for " + var4);
      } catch (PlayerNotFountException var3) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player not found for your client");
      }
   }

   private void setFactionId(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      int var3 = (Integer)this.commandParams[1];

      try {
         PlayerState var5 = var1.getPlayerFromName(var2);
         this.client.serverMessage("[ADMIN COMMAND] set factionID of " + var2 + " to " + var3);
         var5.getFactionController().setFactionId(var3);
      } catch (PlayerNotFountException var4) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player not found for your client");
      }
   }

   private void setFactionIdEntity(GameServerState var1, boolean var2) throws IOException {
      int var3;
      Object var4;
      if (var2) {
         Object[] var10000 = this.commandParams;
         var3 = (Integer)this.commandParams[1];
         var4 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var2);
      } else {
         var3 = (Integer)this.commandParams[0];
         var4 = this.getSelectedOrEnteredStructure(var1);
      }

      if (var4 instanceof SimpleTransformableSendableObject) {
         ((SimpleTransformableSendableObject)var4).setFactionId(var3);
         this.client.serverMessage("[ADMIN COMMAND] SET FACTION ID: " + var4 + " -> " + var3);
      } else {
         this.client.serverMessage("[ERROR][ADMIN COMMAND] OBJECT TO SET FACTION ID FOR NOT FOUND");
      }
   }

   private void setGlobalSpawn(GameServerState var1) throws IOException {
      Transform var2;
      (var2 = new Transform()).setIdentity();

      try {
         PlayerState var6;
         SimpleTransformableSendableObject var3 = (var6 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable();
         var2.set(var3.getWorldTransform());
         ServerConfig.DEFAULT_SPAWN_SECTOR_X.setCurrentState(var6.getCurrentSector().x);
         ServerConfig.DEFAULT_SPAWN_SECTOR_Y.setCurrentState(var6.getCurrentSector().y);
         ServerConfig.DEFAULT_SPAWN_SECTOR_Z.setCurrentState(var6.getCurrentSector().z);
         ServerConfig.DEFAULT_SPAWN_POINT_X_1.setCurrentState(var2.origin.x);
         ServerConfig.DEFAULT_SPAWN_POINT_Y_1.setCurrentState(var2.origin.y);
         ServerConfig.DEFAULT_SPAWN_POINT_Z_1.setCurrentState(var2.origin.z);
         ServerConfig.DEFAULT_SPAWN_POINT_X_2.setCurrentState(var2.origin.x);
         ServerConfig.DEFAULT_SPAWN_POINT_Y_2.setCurrentState(var2.origin.y);
         ServerConfig.DEFAULT_SPAWN_POINT_Z_2.setCurrentState(var2.origin.z);
         ServerConfig.DEFAULT_SPAWN_POINT_X_3.setCurrentState(var2.origin.x);
         ServerConfig.DEFAULT_SPAWN_POINT_Y_3.setCurrentState(var2.origin.y);
         ServerConfig.DEFAULT_SPAWN_POINT_Z_3.setCurrentState(var2.origin.z);
         ServerConfig.DEFAULT_SPAWN_POINT_X_4.setCurrentState(var2.origin.x);
         ServerConfig.DEFAULT_SPAWN_POINT_Y_4.setCurrentState(var2.origin.y);
         ServerConfig.DEFAULT_SPAWN_POINT_Z_4.setCurrentState(var2.origin.z);
         ServerConfig.write();
         this.client.serverMessage("[ADMIN COMMAND] SET DEFAULT SPAWN TO Sector" + var6.getCurrentSector() + " LocalPos" + var2.origin);
      } catch (PlayerNotFountException var4) {
         var4.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      } catch (PlayerControlledTransformableNotFound var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      }
   }

   private void setSimulationDelay(GameServerState var1) throws IOException {
      int var2;
      if ((var2 = (Integer)this.commandParams[0]) >= 0) {
         ServerConfig.SIMULATION_SPAWN_DELAY.setCurrentState(var2);
         this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] simulation delay is now " + var2 + " secs");
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] time must be >= 0");
      }
   }

   private void setSpawn(GameServerState var1) throws IOException {
      try {
         PlayerState var10000 = var1.getPlayerFromStateId(this.client.getId());
         var1 = null;
         var10000.spawnData.setDeathSpawnToPlayerPos();
      } catch (PlayerNotFountException var2) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player not found for your client");
         var2.printStackTrace();
      }
   }

   private void setSpawnPlayer(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         PlayerState var10000 = var1.getPlayerFromName(var2);
         var1 = null;
         var10000.spawnData.setDeathSpawnToPlayerPos();
      } catch (PlayerNotFountException var3) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player not found for your client");
         var3.printStackTrace();
      }
   }

   private void showModifierAndSpawner(GameServerState var1) throws IOException {
      try {
         PlayerState var2 = var1.getPlayerFromStateId(this.client.getId());
         Sendable var6;
         SimpleTransformableSendableObject var7;
         if ((var6 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var2.getNetworkObject().selectedEntityId.get())) != null && var6 instanceof SegmentController) {
            var7 = (SimpleTransformableSendableObject)var6;
         } else {
            var7 = var2.getFirstControlledTransformable();
         }

         if (var7 instanceof SegmentController) {
            String var8 = ((SegmentController)var7).getSpawner();
            String var3 = ((SegmentController)var7).getLastModifier();
            var8 = var8 != null ? (var8.length() > 0 ? var8 : "unknown") : "unknown";
            var3 = var3 != null ? (var3.length() > 0 ? var3 : "unknown") : "unknown";
            this.client.serverMessage("[ADMIN COMMAND] " + var7.toNiceString() + " spawned by " + var8 + "; last modified by " + var3);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] you are not inside or selected a ship");
         }
      } catch (PlayerNotFountException var4) {
         var4.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      } catch (PlayerControlledTransformableNotFound var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      }
   }

   private void shutdown(GameServerState var1) {
      int var2 = (Integer)this.commandParams[0];
      var1.addTimedShutdown(var2);
   }

   private void spawnEntity(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      String var3 = (String)this.commandParams[1];
      int var4 = (Integer)this.commandParams[2];
      int var5 = (Integer)this.commandParams[3];
      int var6 = (Integer)this.commandParams[4];
      int var7 = (Integer)this.commandParams[5];
      boolean var8 = (Boolean)this.commandParams[6];
      Transform var9;
      (var9 = new Transform()).setIdentity();

      try {
         SimpleTransformableSendableObject var10 = var1.getPlayerFromStateId(this.client.getId()).getFirstControlledTransformable();
         var9.set(var10.getWorldTransform());
      } catch (PlayerNotFountException var14) {
         var14.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [WARNING] " + var14.getMessage() + " Assuming 0,0,0 as local position within sector");
         var9.setIdentity();
      } catch (PlayerControlledTransformableNotFound var15) {
         var15.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [WARNING] " + var15.getMessage() + " Assuming 0,0,0 as local position within sector");
         var9.setIdentity();
      }

      try {
         ByteBuffer var17 = ByteBuffer.allocate(10240);
         SegmentControllerOutline var16 = BluePrintController.active.loadBluePrint(var1, var2, var3, var9, -1, var7, new Vector3i(var4, var5, var6), "<admin>", var17, (SegmentPiece)null, var8, new ChildStats(false));
         synchronized(var1.getBluePrintsToSpawn()) {
            var1.getBluePrintsToSpawn().add(var16);
         }
      } catch (EntityNotFountException var12) {
         var12.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var12.getMessage());
      } catch (EntityAlreadyExistsException var13) {
         var13.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var13.getMessage());
      }
   }

   private void spawnEntityPos(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      String var3 = (String)this.commandParams[1];
      int var4 = (Integer)this.commandParams[2];
      int var5 = (Integer)this.commandParams[3];
      int var6 = (Integer)this.commandParams[4];
      float var7 = (Float)this.commandParams[5];
      float var8 = (Float)this.commandParams[6];
      float var9 = (Float)this.commandParams[7];
      int var10 = (Integer)this.commandParams[8];
      boolean var11 = (Boolean)this.commandParams[9];
      Transform var12;
      (var12 = new Transform()).setIdentity();
      var12.origin.set(var7, var8, var9);

      try {
         ByteBuffer var17 = ByteBuffer.allocate(10240);
         SegmentControllerOutline var16 = BluePrintController.active.loadBluePrint(var1, var2, var3, var12, -1, var10, new Vector3i(var4, var5, var6), "<admin>", var17, (SegmentPiece)null, var11, new ChildStats(false));
         synchronized(var1.getBluePrintsToSpawn()) {
            var1.getBluePrintsToSpawn().add(var16);
         }
      } catch (EntityNotFountException var14) {
         var14.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var14.getMessage());
      } catch (EntityAlreadyExistsException var15) {
         var15.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var15.getMessage());
      }
   }

   private void spawnItem(GameServerState var1) throws IOException {
      StringBuilder var2 = new StringBuilder();

      for(int var3 = 0; var3 < this.commandParams.length - 1; ++var3) {
         var2.append((String)this.commandParams[var3]);
         if (var3 < this.commandParams.length - 2) {
            var2.append(" ");
         }
      }

      String var16 = var2.toString();
      Integer var15 = (Integer)this.commandParams[this.commandParams.length - 1];

      try {
         ArrayList var4 = new ArrayList();
         short[] var5;
         int var6 = (var5 = ElementKeyMap.typeList()).length;

         for(int var7 = 0; var7 < var6; ++var7) {
            Short var8;
            String var9;
            if ((var9 = ElementKeyMap.getInfo(var8 = var5[var7]).getName().toLowerCase(Locale.ENGLISH)).startsWith(var16.trim().toLowerCase(Locale.ENGLISH))) {
               var4.add(var8);
               if (var9.toLowerCase(Locale.ENGLISH).equals(var16.toLowerCase(Locale.ENGLISH).trim())) {
                  var4.clear();
                  var4.add(var8);
                  break;
               }
            }
         }

         short var19;
         if (var4.size() == 1) {
            PlayerState var18 = var1.getPlayerFromStateId(this.client.getId());
            var19 = (Short)var4.get(0);
            Sector var20 = var1.getUniverse().getSector(var18.getCurrentSectorId());
            TransformTimed var21 = var18.getFirstControlledTransformable().getWorldTransform();
            Vector3f var22 = new Vector3f(var21.origin);
            Vector3f var14;
            (var14 = var18.getForward(new Vector3f())).scale(2.0F);
            var22.add(var14);
            var20.getRemoteSector().addItem(var22, var19, -1, var15);
            this.client.serverMessage("[ADMIN COMMAND] sucessfully spawned item at " + var22);
         } else if (var4.isEmpty()) {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] no element starts with the string: \"" + var16 + "\"");
         } else {
            Iterator var17 = var4.iterator();

            while(var17.hasNext()) {
               var19 = (Short)var17.next();
               this.client.serverMessage("[ADMIN COMMAND] [ERROR] ambigous string: \"" + var16 + "\": (" + ElementKeyMap.getInfo(var19).getName() + " [" + var19 + "])" + (var17.hasNext() ? ", " : ""));
            }

            this.client.serverMessage("[ADMIN COMMAND] [ERROR] use either the classified name or the one in the parenthesis");
         }
      } catch (PlayerNotFountException var10) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var10.getMessage());
      } catch (IndexOutOfBoundsException var11) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Too many arguments");
      } catch (ElementClassNotFoundException var12) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] Unknown Element" + var16);
      } catch (PlayerControlledTransformableNotFound var13) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var13.getMessage());
      }
   }

   public void spawnMobs(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      int var3 = (Integer)this.commandParams[1];
      int var4 = (Integer)this.commandParams[2];
      System.err.println("Spawning " + var4 + " mobs of type: " + var2);
      Transform var5;
      (var5 = new Transform()).setIdentity();

      try {
         PlayerState var6;
         SimpleTransformableSendableObject var7 = (var6 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable();
         var5.set(var7.getWorldTransform());
         var1.spawnMobs(var4, var2, var6.getCurrentSector(), var5, var3, BluePrintController.active);
      } catch (PlayerNotFountException var8) {
         var8.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var8.getMessage());
      } catch (PlayerControlledTransformableNotFound var9) {
         var9.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var9.getMessage());
      } catch (EntityNotFountException var10) {
         var10.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var10.getMessage());
      } catch (EntityAlreadyExistsException var11) {
         var11.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var11.getMessage());
      }
   }

   private void spawnMobsLine(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      int var3 = (Integer)this.commandParams[1];
      int var4 = (Integer)this.commandParams[2];

      try {
         PlayerState var5;
         SimpleTransformableSendableObject var6 = (var5 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable();
         Vector3f var7 = new Vector3f(var6.getWorldTransform().origin);
         Vector3f var8 = new Vector3f(var7);
         Vector3f var9;
         (var9 = new Vector3f(var5.getForward(new Vector3f()))).scale(5000.0F);
         var8.add(var9);
         Transform var15;
         (var15 = new Transform()).setIdentity();
         ClosestRayResultCallback var14;
         if ((var14 = ((PhysicsExt)var1.getUniverse().getSector(var6.getSectorId()).getPhysics()).testRayCollisionPoint(var7, var8, false, (SimpleTransformableSendableObject)null, (SegmentController)null, false, true, false)).hasHit()) {
            var15.origin.set(var14.hitPointWorld);
            System.err.println("Spawning " + var4 + " mobs of type: " + var2 + " at " + var14.hitPointWorld);
            var1.spawnMobs(var4, var2, var5.getCurrentSector(), var15, var3, BluePrintController.active);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] no object in line of sight");
         }
      } catch (PlayerNotFountException var10) {
         var10.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var10.getMessage());
      } catch (PlayerControlledTransformableNotFound var11) {
         var11.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var11.getMessage());
      } catch (EntityNotFountException var12) {
         var12.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var12.getMessage());
      } catch (EntityAlreadyExistsException var13) {
         var13.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var13.getMessage());
      }
   }

   private void startShipAI(GameServerState var1) throws IOException {
      try {
         int var3 = (Integer)this.commandParams[0];
         PlayerState var2 = var1.getPlayerFromStateId(this.client.getId());
         Sendable var6;
         SimpleTransformableSendableObject var7;
         if ((var6 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var2.getNetworkObject().selectedEntityId.get())) != null && var6 instanceof Ship) {
            var7 = (SimpleTransformableSendableObject)var6;
         } else {
            var7 = var2.getFirstControlledTransformable();
         }

         if (var7 instanceof SegmentController) {
            ((SegmentController)var7).railController.activateAllAIServer(true, true, true, true);
         }

         if (var7 instanceof Ship) {
            ((Ship)var7).setFactionId(var3);
            ((Ship)var7).getAiConfiguration().get(Types.TYPE).setCurrentState("Ship", true);
            ((Ship)var7).getAiConfiguration().get(Types.ACTIVE).setCurrentState(true, true);
            ((Ship)var7).getAiConfiguration().applyServerSettings();
            this.client.serverMessage("[ADMIN COMMAND] activated " + var7 + " with faction " + var3);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] you are not inside or selected a ship");
         }
      } catch (PlayerNotFountException var4) {
         var4.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      } catch (PlayerControlledTransformableNotFound var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      }
   }

   private void status(GameServerState var1) throws IOException {
      long var2 = Runtime.getRuntime().totalMemory() / 1024L / 1024L;
      long var4 = Runtime.getRuntime().freeMemory() / 1024L / 1024L;
      long var6 = var2 - var4;
      this.client.serverMessage("PhysicsInMem: " + GameServerState.axisSweepsInMemory + "; Rep: " + var1.getUniverse().getPhysicsRepository().size());
      this.client.serverMessage("Total queued NT Packages: " + ServerProcessor.totalPackagesQueued);
      this.client.serverMessage("Loaded !empty Segs / free: " + GameServerState.lastAllocatedSegmentData + " / " + GameServerState.lastFreeSegmentData);
      this.client.serverMessage("Loaded Objects: " + var1.getLocalAndRemoteObjectContainer().getLocalObjects().size());
      this.client.serverMessage("Players: " + var1.getClients().size() + " / " + var1.getMaxClients());
      this.client.serverMessage("Mem (MB)[free, taken, total]: [" + var4 + ", " + var6 + ", " + var2 + "]");
      this.client.serverMessage("---------SERVER STATUS---------");
   }

   private void stopShipAI(GameServerState var1) throws IOException {
      try {
         PlayerState var2 = var1.getPlayerFromStateId(this.client.getId());
         Sendable var5;
         SimpleTransformableSendableObject var6;
         if ((var5 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var2.getNetworkObject().selectedEntityId.get())) != null && var5 instanceof Ship) {
            var6 = (SimpleTransformableSendableObject)var5;
         } else {
            var6 = var2.getFirstControlledTransformable();
         }

         if (var6 instanceof Ship) {
            ((Ship)var6).getAiConfiguration().get(Types.ACTIVE).setCurrentState(false, true);
            ((Ship)var6).getAiConfiguration().applyServerSettings();
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] you are not inside or selected a ship");
         }
      } catch (PlayerNotFountException var3) {
         var3.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var3.getMessage());
      } catch (PlayerControlledTransformableNotFound var4) {
         var4.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var4.getMessage());
      }
   }

   private void teleportSelfHome(GameServerState var1) throws IOException {
      try {
         PlayerState var4 = var1.getPlayerFromStateId(this.client.getId());
         this.warpTransformable(var4.getFirstControlledTransformable(), 0.0F, 0.0F, 0.0F);
      } catch (PlayerNotFountException var2) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var2.getMessage());
      } catch (PlayerControlledTransformableNotFound var3) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var3.getMessage());
      }
   }

   private void teleportSelfTo(GameServerState var1) throws IOException {
      try {
         PlayerState var4 = var1.getPlayerFromStateId(this.client.getId());
         this.warpTransformable(var4.getFirstControlledTransformable(), (Float)this.commandParams[0], (Float)this.commandParams[1], (Float)this.commandParams[2]);
      } catch (PlayerControlledTransformableNotFound var2) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var2.getMessage());
      } catch (PlayerNotFountException var3) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var3.getMessage());
      }
   }

   private void teleportUidTo(GameServerState var1, Int2ObjectOpenHashMap var2) throws IOException {
      String var8 = (String)this.commandParams[0];
      float var3 = (Float)this.commandParams[1];
      float var4 = (Float)this.commandParams[2];
      float var5 = (Float)this.commandParams[3];
      Sendable var6;
      if ((var6 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var8)) != null && var6 instanceof SimpleTransformableSendableObject) {
         SimpleTransformableSendableObject var7 = (SimpleTransformableSendableObject)var6;
         this.warpTransformable(var7, var3, var4, var5);
         this.client.serverMessage("[ADMIN COMMAND] teleported " + var8 + " to ");
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] UID not found or not loaded");
      }
   }

   private void teleportTo(GameServerState var1, Int2ObjectOpenHashMap var2) throws IOException {
      String var9 = (String)this.commandParams[0];
      float var3 = (Float)this.commandParams[1];
      float var4 = (Float)this.commandParams[2];
      float var5 = (Float)this.commandParams[3];

      try {
         PlayerState var10000 = var1.getPlayerFromName(var9);
         var1 = null;
         SimpleTransformableSendableObject var8 = var10000.getFirstControlledTransformable();
         this.warpTransformable(var8, var3, var4, var5);
         this.client.serverMessage("[ADMIN COMMAND] teleported " + var9 + " to ");
      } catch (PlayerNotFountException var6) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player not found for your client");
      } catch (PlayerControlledTransformableNotFound var7) {
         var7.printStackTrace();
      }
   }

   private void teleportSelectedTo(GameServerState var1, Int2ObjectOpenHashMap var2) throws IOException {
      float var8 = (Float)this.commandParams[0];
      float var3 = (Float)this.commandParams[1];
      float var4 = (Float)this.commandParams[2];

      try {
         PlayerState var5 = var1.getPlayerFromStateId(this.client.getId());
         Sendable var7;
         if ((var7 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var5.getNetworkObject().selectedEntityId.get())) != null && var7 instanceof SimpleTransformableSendableObject) {
            this.warpTransformable((SimpleTransformableSendableObject)var7, var8, var3, var4);
         } else {
            this.client.serverMessage("[ERROR][ADMIN COMMAND] OBJECT TO WARP NOT FOUND");
         }

         this.client.serverMessage("[ADMIN COMMAND] teleported " + var7 + " to ");
      } catch (PlayerNotFountException var6) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player not found for your client");
      }
   }

   private void tp(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         PlayerState var3 = var1.getPlayerFromName(var2);
         PlayerState var8;
         SimpleTransformableSendableObject var4 = (var8 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable();
         ++(new Vector3f(var4.getWorldTransform().origin)).x;
         Vector3i var9 = new Vector3i(var8.getCurrentSector());
         Sector var5;
         if ((var5 = var1.getUniverse().getSector(var9)) != null) {
            if (var3.getCurrentSectorId() != var5.getId()) {
               Iterator var10 = var3.getControllerState().getUnits().iterator();

               while(var10.hasNext()) {
                  ControllerStateUnit var11;
                  if ((var11 = (ControllerStateUnit)var10.next()).playerControllable instanceof SimpleTransformableSendableObject) {
                     var1.getController().queueSectorSwitch(getControllerRoot((SimpleTransformableSendableObject)var11.playerControllable), var5.pos, 1, false, true, true);
                  }
               }

            } else {
               this.client.serverMessage("[ADMIN COMMAND] not changing sector for object " + var4.getSectorId() + "/" + var5.getId());
            }
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] sector not found: " + var9 + ": " + var1.getUniverse().getSectorSet());
         }
      } catch (PlayerNotFountException var6) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player not found for your client");
      } catch (Exception var7) {
         var7.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] server could not load sector");
      }
   }

   private void tpTo(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         PlayerState var3 = var1.getPlayerFromName(var2);
         PlayerState var9 = var1.getPlayerFromStateId(this.client.getId());
         SimpleTransformableSendableObject var4 = var3.getFirstControlledTransformable();
         ++(new Vector3f(var4.getWorldTransform().origin)).x;
         Vector3i var5 = new Vector3i(var9.getCurrentSector());
         Sector var6;
         if ((var6 = var1.getUniverse().getSector(var5)) != null) {
            if (var3.getCurrentSectorId() != var6.getId()) {
               Iterator var10 = var9.getControllerState().getUnits().iterator();

               while(var10.hasNext()) {
                  ControllerStateUnit var11;
                  if ((var11 = (ControllerStateUnit)var10.next()).playerControllable instanceof SimpleTransformableSendableObject) {
                     var1.getController().queueSectorSwitch(getControllerRoot((SimpleTransformableSendableObject)var11.playerControllable), var3.getCurrentSector(), 1, false, true, true);
                  }
               }

            } else {
               this.client.serverMessage("[ADMIN COMMAND] not changing sector for object " + var4.getSectorId() + "/" + var6.getId());
            }
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] sector not found: " + var5 + ": " + var1.getUniverse().getSectorSet());
         }
      } catch (PlayerNotFountException var7) {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] player not found for your client");
      } catch (Exception var8) {
         var8.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] server could not load sector");
      }
   }

   private void triggerSimulationPlanning(GameServerState var1) {
      try {
         var1.getSimulationManager().getPlanner().interrupt();
      } catch (Exception var2) {
         var2.printStackTrace();
      }
   }

   private void unBanIp(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      if (var1.getController().removeBannedIp(this.client.getPlayerName(), var2)) {
         this.client.serverMessage("[ADMIN COMMAND] successfully unbanned: " + var2);
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] ip not found in blacklist: " + var2 + " -> " + var1.getBlackListedIps());
      }
   }

   private void unBanName(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];
      if (var1.getController().removeBannedName(this.client.getPlayerName(), var2)) {
         this.client.serverMessage("[ADMIN COMMAND] successfully unbanned: " + var2);
      } else {
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] name not found in blacklist: " + var2 + " -> " + var1.getBlackListedNames());
      }
   }

   private void updateAllShopPrices(GameServerState var1) {
      GameServerState.updateAllShopPricesFlag = true;
   }

   private void warpTransformable(SimpleTransformableSendableObject var1, float var2, float var3, float var4) {
      var1.warpTransformable(var2, var3, var4, true, (LocalSectorTransition)null);
   }

   private void whitelistIp(GameServerState var1, boolean var2) throws IOException {
      String var3 = (String)this.commandParams[0];
      long var4 = -1L;
      if (var2) {
         var4 = System.currentTimeMillis() + ((Integer)this.commandParams[1]).longValue() * 60000L;
      }

      try {
         var1.getController().addWitelistedIp(var3, var4);
         this.client.serverMessage("[ADMIN COMMAND] successfully whitelisted: " + var3);
      } catch (NoIPException var6) {
         var6.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] not an IP: " + var3);
      }
   }

   private void whitelistName(GameServerState var1, boolean var2) throws IOException {
      String var3 = (String)this.commandParams[0];
      long var4 = -1L;
      if (var2) {
         var4 = System.currentTimeMillis() + ((Integer)this.commandParams[1]).longValue() * 60000L;
      }

      var1.getController().addWitelistedName(var3, var4);
      this.client.serverMessage("[ADMIN COMMAND] successfully whitelisted: " + var3);
   }

   private void whitelistAccount(GameServerState var1, boolean var2) throws IOException {
      long var3 = -1L;
      if (var2) {
         var3 = System.currentTimeMillis() + ((Integer)this.commandParams[1]).longValue() * 60000L;
      }

      String var5 = (String)this.commandParams[0];
      var1.getController().addWitelistedAccount(var5, var3);
      this.client.serverMessage("[ADMIN COMMAND] successfully whitelisted account: " + var5);
   }

   private void entitySetShieldRegen(GameServerState var1) throws IOException {
      try {
         PlayerState var2 = var1.getPlayerFromStateId(this.client.getId());
         Sendable var6 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var2.getNetworkObject().selectedEntityId.get());
         ShieldAddOn var3 = null;
         if (var6 != null) {
            if (var6 instanceof ShieldAddOn) {
               var3 = (ShieldAddOn)var6;
            } else if (var6 instanceof ManagedSegmentController) {
               if (((ManagedSegmentController)var6).getManagerContainer() instanceof ShipManagerContainer) {
                  var3 = ((ShipManagerContainer)((ManagedSegmentController)var6).getManagerContainer()).getShieldAddOn();
               } else if (((ManagedSegmentController)var6).getManagerContainer() instanceof StationaryManagerContainer) {
                  var3 = ((StationaryManagerContainer)((ManagedSegmentController)var6).getManagerContainer()).getShieldAddOn();
               }
            }
         }

         Boolean var4 = (Boolean)this.commandParams[0];
         if (var3 != null) {
            var3.setRegenEnabled(var4);
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Shield addon regen set to: " + var3.isRegenEnabled() + " on " + var6);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] No entity selected: " + var2.getNetworkObject().selectedEntityId.get() + "->(" + var6 + ")");
         }
      } catch (PlayerNotFountException var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      }
   }

   private void entitySetPowerRegen(GameServerState var1) throws IOException {
      try {
         PlayerState var2 = var1.getPlayerFromStateId(this.client.getId());
         Sendable var6 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var2.getNetworkObject().selectedEntityId.get());
         PowerAddOn var3 = null;
         if (var6 != null) {
            if (var6 instanceof PowerAddOn) {
               var3 = (PowerAddOn)var6;
            } else if (var6 instanceof ManagedSegmentController) {
               if (((ManagedSegmentController)var6).getManagerContainer() instanceof ShipManagerContainer) {
                  var3 = ((ShipManagerContainer)((ManagedSegmentController)var6).getManagerContainer()).getPowerAddOn();
               } else if (((ManagedSegmentController)var6).getManagerContainer() instanceof StationaryManagerContainer) {
                  var3 = ((StationaryManagerContainer)((ManagedSegmentController)var6).getManagerContainer()).getPowerAddOn();
               }
            }
         }

         Boolean var4 = (Boolean)this.commandParams[0];
         if (var3 != null) {
            var3.setRechargeEnabled(var4);
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] Power addon regen set to: " + var3.isRechargeEnabled() + " on " + var6);
         } else {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] No entity selected: " + var2.getNetworkObject().selectedEntityId.get() + "->(" + var6 + ")");
         }
      } catch (PlayerNotFountException var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      }
   }

   private void powerDrain(GameServerState var1) throws IOException {
      int var2 = (Integer)this.commandParams[0];

      try {
         PlayerState var3;
         SimpleTransformableSendableObject var4;
         if ((var4 = (var3 = var1.getPlayerFromStateId(this.client.getId())).getFirstControlledTransformable()) == null || !(var4 instanceof SegmentController)) {
            System.err.println("[ADMIN COMMAND]checking selected");
            Sendable var7;
            if ((var7 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var3.getNetworkObject().selectedEntityId.get())) != null && var7 instanceof SimpleTransformableSendableObject) {
               var4 = (SimpleTransformableSendableObject)var7;
            }
         }

         boolean var8 = false;
         if (var4 != null && var4 instanceof ManagedSegmentController && ((ManagedSegmentController)var4).getManagerContainer() instanceof PowerManagerInterface) {
            PowerAddOn var9 = ((PowerManagerInterface)((ManagedSegmentController)var4).getManagerContainer()).getPowerAddOn();
            var8 = true;
            var9.setPower(var9.getPower() - (double)var2);
            var9.sendPowerUpdate();
            this.client.serverMessage("[ADMIN COMMAND] [SUCCESS] drained " + var2 + " power from " + var4);
         }

         if (!var8) {
            this.client.serverMessage("[ADMIN COMMAND] [ERROR] object " + var4 + " has no power capability");
         }

      } catch (PlayerNotFountException var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      } catch (PlayerControlledTransformableNotFound var6) {
         var6.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var6.getMessage());
      }
   }

   private void spawnParticle(GameServerState var1) throws IOException {
      String var2 = (String)this.commandParams[0];

      try {
         PlayerState var3 = var1.getPlayerFromStateId(this.client.getId());
         Transform var4;
         (var4 = new Transform()).origin.set(var3.getAssingedPlayerCharacter().getWorldTransform().origin);
         ParticleUtil.sendToAllPlayers(var1, new ParticleEntry(var2, var4.origin));
      } catch (PlayerNotFountException var5) {
         var5.printStackTrace();
         this.client.serverMessage("[ADMIN COMMAND] [ERROR] " + var5.getMessage());
      }
   }

   static class SearchResult {
      String realName;
      Vector3i position;

      public SearchResult(String var1, String var2, Vector3i var3) {
         this.realName = var1;
         this.position = var3;
      }
   }
}
