package org.schema.game.server.data.admin;

import java.util.Comparator;

public class AdminCommandLengthComparator implements Comparator {
   public int compare(AdminCommands var1, AdminCommands var2) {
      if (var1.getDescription().length() < var2.getDescription().length()) {
         return -1;
      } else {
         return var1.getDescription().length() > var2.getDescription().length() ? 1 : 0;
      }
   }
}
