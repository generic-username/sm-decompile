package org.schema.game.server.data.admin;

public class AdminCommandNotFoundException extends Exception {
   private static final long serialVersionUID = 1L;

   public AdminCommandNotFoundException(String var1) {
      super(var1);
   }
}
