package org.schema.game.server.data;

import java.util.Comparator;

public class ServerConfigComperator implements Comparator {
   public int compare(ServerConfig var1, ServerConfig var2) {
      if (var1.getDescription().length() < var2.getDescription().length()) {
         return -1;
      } else {
         return var1.getDescription().length() > var2.getDescription().length() ? 1 : 0;
      }
   }
}
