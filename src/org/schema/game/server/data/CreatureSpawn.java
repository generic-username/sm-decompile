package org.schema.game.server.data;

import com.bulletphysics.linearmath.Transform;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ai.AIGameCreatureConfiguration;
import org.schema.game.common.data.creature.AICharacter;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.common.data.creature.AIRandomCompositeCreature;
import org.schema.game.common.data.creature.CreaturePartNode;
import org.schema.game.common.data.world.Sector;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.CreatureStructure;

public class CreatureSpawn {
   private static int count;
   private Vector3i spawnSectorPos;
   private Transform localPos;
   private String realName;
   private CreatureType type;
   private CreaturePartNode node;
   private float speed;
   private float scale;
   private float height;
   private float width;
   private Vector3i blockDim;

   public CreatureSpawn() {
      this.spawnSectorPos = new Vector3i();
      this.localPos = new Transform();
      this.realName = "noName";
      this.type = CreatureType.CHARACTER;
      this.speed = -1.0F;
      this.scale = 1.0F;
      this.height = 0.2F;
      this.width = 0.3F;
      this.blockDim = new Vector3i(1, 1, 1);
      this.localPos.setIdentity();
   }

   public CreatureSpawn(Vector3i var1, Transform var2, String var3, CreatureType var4) {
      this();

      assert var4 != null;

      this.spawnSectorPos = new Vector3i(var1);
      this.localPos = new Transform(var2);
      this.realName = var3;
      this.type = var4;
   }

   public void serialize(DataOutput var1) throws IOException {
      var1.writeInt(this.type.ordinal());
      var1.writeUTF(this.realName);
      var1.writeInt(this.spawnSectorPos.x);
      var1.writeInt(this.spawnSectorPos.y);
      var1.writeInt(this.spawnSectorPos.z);
      var1.writeFloat(this.localPos.origin.x);
      var1.writeFloat(this.localPos.origin.y);
      var1.writeFloat(this.localPos.origin.z);
      var1.writeInt(this.blockDim.x);
      var1.writeInt(this.blockDim.y);
      var1.writeInt(this.blockDim.z);
      var1.writeFloat(this.speed);
      var1.writeFloat(this.scale);
      var1.writeFloat(this.width);
      var1.writeFloat(this.height);
      if (this.type == CreatureType.CREATURE_SPECIFIC) {
         this.node.serialize(var1);
      }

   }

   public void deserialize(DataInput var1) throws IOException {
      this.type = CreatureType.values()[var1.readInt()];
      this.realName = var1.readUTF();
      this.spawnSectorPos.x = var1.readInt();
      this.spawnSectorPos.y = var1.readInt();
      this.spawnSectorPos.z = var1.readInt();
      this.localPos.origin.x = var1.readFloat();
      this.localPos.origin.y = var1.readFloat();
      this.localPos.origin.z = var1.readFloat();
      this.blockDim.x = var1.readInt();
      this.blockDim.y = var1.readInt();
      this.blockDim.z = var1.readInt();
      this.speed = var1.readFloat();
      this.scale = var1.readFloat();
      this.width = var1.readFloat();
      this.height = var1.readFloat();
      if (this.type == CreatureType.CREATURE_SPECIFIC) {
         this.node = new CreaturePartNode(CreatureStructure.PartType.BOTTOM);
         this.node.deserialize(var1);
      }

   }

   public void execute(GameServerState var1) throws IOException {
      String var2 = "ENTITY_CREATURE_" + System.currentTimeMillis() + "_" + count++;

      assert this.type != null;

      Object var3;
      switch(this.type) {
      case CHARACTER:
         var3 = new AICharacter(var1);
         break;
      case CREATURE_RANDOM:
         var3 = AIRandomCompositeCreature.random(var1);
         break;
      case CREATURE_SPECIFIC:
         assert this.node != null;

         var3 = AIRandomCompositeCreature.instantiate(var1, 4.0F, this.scale, this.width, this.height, this.blockDim, this.node);
         break;
      default:
         throw new IllegalArgumentException();
      }

      Sector var4 = var1.getUniverse().getSector(this.spawnSectorPos);
      ((AICreature)var3).setSectorId(var4.getId());
      ((AICreature)var3).initialize();
      ((AICreature)var3).setRealName(this.realName);
      ((AICreature)var3).getInitialTransform().setIdentity();
      ((AICreature)var3).getInitialTransform().set(this.localPos);
      ((AICreature)var3).initialFillInventory();
      ((AICreature)var3).setId(var1.getNextFreeObjectId());
      ((AICreature)var3).setUniqueIdentifier(var2);
      if (this.speed >= 0.0F) {
         ((AICreature)var3).setSpeed(this.speed);
      }

      AIGameCreatureConfiguration var5 = ((AICreature)var3).getAiConfiguration();
      this.initAI(var5);
      var5.applyServerSettings();
      var1.getController().getSynchController().addNewSynchronizedObjectQueued((Sendable)var3);
   }

   public void initAI(AIGameCreatureConfiguration var1) {
   }

   public CreaturePartNode getNode() {
      return this.node;
   }

   public void setNode(CreaturePartNode var1) {
      this.node = var1;
   }

   public float getSpeed() {
      return this.speed;
   }

   public void setSpeed(float var1) {
      this.speed = var1;
   }
}
