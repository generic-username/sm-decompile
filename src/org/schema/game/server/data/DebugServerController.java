package org.schema.game.server.data;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.controller.rails.RailRelation;
import org.schema.schine.network.objects.Sendable;

public class DebugServerController {
   private GameServerState state;
   private Long2ObjectOpenHashMap railSaves = new Long2ObjectOpenHashMap();
   private ObjectOpenHashSet railSaveUID = new ObjectOpenHashSet();

   public DebugServerController(GameServerState var1) {
      this.state = var1;
   }

   private DebugServerController.RailSave getRailSaveRec(SegmentController var1) {
      DebugServerController.RailSave var2 = new DebugServerController.RailSave(var1.getUniqueIdentifier(), var1.dbId);
      Iterator var4 = var1.railController.next.iterator();

      while(var4.hasNext()) {
         RailRelation var3 = (RailRelation)var4.next();
         var2.childs.add(this.getRailSaveRec(var3.docked.getSegmentController()));
      }

      return var2;
   }

   public void check(SegmentController var1) {
      if (this.railSaves.containsKey(var1.dbId)) {
         if (!this.railSaveUID.contains(var1.getUniqueIdentifier())) {
            throw new RuntimeException("ENTITY " + var1 + " DIDN'T HAVE UID: " + var1.dbId + "; " + var1.getUniqueIdentifier() + "; " + this.railSaveUID);
         }

         DebugServerController.RailSave var2;
         if (!(var2 = (DebugServerController.RailSave)this.railSaves.get(var1.dbId)).UID.equals(var1.getUniqueIdentifier())) {
            throw new RuntimeException("ENTITY " + var1 + " DIDN'T HAS DIFFERENT UID: " + var1.dbId + "; " + var1.getUniqueIdentifier() + " != " + var2.UID);
         }

         if (var1.isFullyLoadedWithDock() && !this.checkRailSaveWith(var1, var2)) {
            throw new RuntimeException("ENTITY " + var1 + " LOST DOCKS: NOW: " + var1.railController.next + "; SHOULD BE: " + var2.childs);
         }
      }

   }

   private boolean checkRailSaveWith(SegmentController var1, DebugServerController.RailSave var2) {
      if (var1.railController.next.size() != var2.childs.size()) {
         throw new RuntimeException("ENTITY " + var1 + " LOST DOCKS (BY CONTROLLER): NOW: " + var1.railController.next + "; SHOULD BE: " + var2.childs);
      } else {
         boolean var3 = this.checkRailChildrenByController(var1, var2);
         return this.checkRailChildrenBySave(var1, var2) && var3;
      }
   }

   private boolean checkRailChildrenBySave(SegmentController var1, DebugServerController.RailSave var2) {
      if (var2.childs.isEmpty()) {
         return true;
      } else {
         boolean var3 = false;
         Iterator var6;
         if ((var6 = var2.childs.iterator()).hasNext()) {
            var2 = (DebugServerController.RailSave)var6.next();
            Iterator var4 = var1.railController.next.iterator();

            while(true) {
               SegmentController var5;
               do {
                  if (!var4.hasNext()) {
                     if (!var3) {
                        throw new RuntimeException("ENTITY " + var1 + " LOST DOCKS (BY RAILSAVE): SPECIFICALLY: " + var2 + "; NOT FOUND IN RAIL CHILDREN: " + var1.railController.next + "; ");
                     }

                     return var3;
                  }

                  var5 = ((RailRelation)var4.next()).docked.getSegmentController();
               } while(var2.dbId != var5.dbId && !var2.UID.equals(var5.getUniqueIdentifier()));

               var3 = this.checkRailChildrenBySave(var5, var2);
            }
         } else {
            return var3;
         }
      }
   }

   private boolean checkRailChildrenByController(SegmentController var1, DebugServerController.RailSave var2) {
      if (var1.railController.next.isEmpty()) {
         return true;
      } else {
         boolean var3 = false;
         Iterator var4;
         if ((var4 = var1.railController.next.iterator()).hasNext()) {
            RailRelation var8;
            SegmentController var5 = (var8 = (RailRelation)var4.next()).docked.getSegmentController();
            Iterator var6 = var2.childs.iterator();

            while(true) {
               DebugServerController.RailSave var7;
               do {
                  if (!var6.hasNext()) {
                     if (!var3) {
                        throw new RuntimeException("ENTITY " + var1 + " LOST DOCKS: SPECIFICALLY: " + var8.docked.getSegmentController() + "; NOT FOUND IN RAILSAVE CHILDS: " + var2.childs);
                     }

                     return var3;
                  }
               } while((var7 = (DebugServerController.RailSave)var6.next()).dbId != var5.dbId && !var7.UID.equals(var5.getUniqueIdentifier()));

               var3 = this.checkRailChildrenByController(var5, var7);
            }
         } else {
            return var3;
         }
      }
   }

   public void saveRail(SegmentController var1) {
      DebugServerController.RailSave var2 = this.getRailSaveRec(var1);
      this.railSaves.put(var1.dbId, var2);
      this.railSaveUID.add(var1.getUniqueIdentifier());
   }

   public void removeRailSave(long var1) {
      if (this.railSaves.containsKey(var1)) {
         DebugServerController.RailSave var3 = (DebugServerController.RailSave)this.railSaves.remove(var1);
         if (!this.railSaveUID.remove(var3.UID)) {
            throw new RuntimeException("Couldnt remove " + var3 + "; not in UID");
         }
      }

   }

   class RailSave {
      private List childs = new ObjectArrayList();
      public final String UID;
      public long dbId;

      public RailSave(String var2, long var3) {
         this.UID = var2;
         this.dbId = var3;
      }

      public String toString() {
         return "RailSave [UID=" + this.UID + ", dbId=" + this.dbId + ", " + this.getStatus() + "]";
      }

      public String getStatus() {
         Sendable var1;
         if ((var1 = (Sendable)DebugServerController.this.state.getLocalAndRemoteObjectContainer().getDbObjects().get(this.dbId)) != null) {
            SegmentController var4 = (SegmentController)var1;
            return "###LOADED SECID: " + var4.getSectorId() + "; Sector: " + DebugServerController.this.state.getUniverse().getSector(var4.getSectorId());
         } else {
            DatabaseEntry var3 = null;

            try {
               var3 = DebugServerController.this.state.getDatabaseIndex().getTableManager().getEntityTable().getById(this.dbId);
            } catch (SQLException var2) {
               var2.printStackTrace();
            }

            return var3 != null ? "UNLOADED (" + var3.sectorPos.toStringPure() + ")" : "!!!!NOT IN DATABASE!!!!";
         }
      }
   }
}
