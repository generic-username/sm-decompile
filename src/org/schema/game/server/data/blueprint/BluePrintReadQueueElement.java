package org.schema.game.server.data.blueprint;

import com.bulletphysics.linearmath.Transform;

public class BluePrintReadQueueElement {
   public String catalogname;
   public String name;
   public Transform t;
   public boolean activeAI;

   public BluePrintReadQueueElement(String var1, String var2, Transform var3, boolean var4) {
      this.catalogname = var1;
      this.name = var2;
      this.t = var3;
      this.activeAI = var4;
   }
}
