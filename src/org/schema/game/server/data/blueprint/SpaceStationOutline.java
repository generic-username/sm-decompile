package org.schema.game.server.data.blueprint;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import javax.vecmath.Quat4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.controller.EntityAlreadyExistsException;
import org.schema.game.server.controller.SectorUtil;
import org.schema.game.server.data.BlueprintInterface;
import org.schema.game.server.data.EntityRequest;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.resource.tag.Tag;

public class SpaceStationOutline extends SegmentControllerOutline {
   private int factionId;

   public SpaceStationOutline(GameServerState var1, BlueprintInterface var2, String var3, String var4, float[] var5, int var6, Vector3i var7, Vector3i var8, String var9, boolean var10, Vector3i var11, ChildStats var12) {
      super(var1, var2, var3, var4, var5, var7, var8, var9, var10, var11, var12);
      this.factionId = var6;
      this.checkForChilds(this.factionId);
   }

   public SpaceStation spawn(Vector3i var1, boolean var2, ChildStats var3, SegmentControllerSpawnCallback var4) throws EntityAlreadyExistsException, StateParameterNotFoundException {
      SpaceStation var5 = null;

      Sector var6;
      try {
         if ((var6 = var4.sector) != null && var6.isActive()) {
            EntityRequest.existsIdentifier(this.state, this.uniqueIdentifier);
            (var5 = EntityRequest.getNewSpaceStation(this.state, this.uniqueIdentifier, var6.getId(), this.realName, this.mat, this.min.x, this.min.y, this.min.z, this.max.x, this.max.y, this.max.z, this.en.isChunk16())).npcSystem = this.npcSystem;
            var5.npcSpec = this.npcSpec;
            if (var3.usedNamed.contains(this.uniqueIdentifier)) {
               assert false : "Already exists: " + this.uniqueIdentifier + "; USED: " + var3.usedNamed;

               throw new EntityAlreadyExistsException(this.uniqueIdentifier);
            }

            var3.usedNamed.add(this.uniqueIdentifier);
            BlueprintEntry var9;
            if (this.en instanceof BlueprintEntry) {
               if ((var9 = (BlueprintEntry)this.en).rootRead) {
                  var3.rootName = this.uniqueIdentifier;
               }

               if (var9.getManagerTag() != null) {
                  var5.getManagerContainer().loadInventoriesFromTag = false;
                  var5.getManagerContainer().fromTagStructure(var9.getManagerTag());
                  var5.getManagerContainer().handleBlueprintWireless(var3.rootName, var9.wirelessToOwnRail);
                  var5.getManagerContainer().getShoppingAddOn().setCredits(0L);
                  var5.getManagerContainer().loadInventoriesFromTag = true;
               }
            }

            var5.setTouched(true, false);
            ElementCountMap var10 = null;
            if (this.itemsToSpawnWith != null && this.en.getCapacitySingle() > 0.0D) {
               (var10 = new ElementCountMap()).transferFrom(this.itemsToSpawnWith, this.en.getCapacitySingle());
            }

            var5.itemsToSpawnWith = var10;
            if (this.childs != null) {
               SegmentControllerOutline var7;
               Iterator var12;
               for(var12 = this.childs.iterator(); var12.hasNext(); var7.parentOutline = this) {
                  Tag var10000 = (var7 = (SegmentControllerOutline)var12.next()).railTag;
                  var7.parent = var5;
               }

               var12 = this.childs.iterator();

               while(var12.hasNext()) {
                  (var7 = (SegmentControllerOutline)var12.next()).scrap = this.scrap;
                  var7.dockTo = this.uniqueIdentifier;
                  var7.itemsToSpawnWith = this.itemsToSpawnWith;

                  assert var7.dockTo != null;

                  var7.spawn(var1, false, var3, var4);
               }
            }

            var5.usedOldPowerFromTag = this.en.isOldPowerFlag();
            var5.oldPowerBlocksFromBlueprint = this.en.getElementMap().get((short)2);
            var5.setFactionId(this.factionId);
            var5.getControlElementMap().getControllingMap().setFromMap(this.en.getControllingMap());
            if (this.factionId != 0) {
               var5.getAiConfiguration().get(Types.AIM_AT).switchSetting("Any", true);
               var5.getAiConfiguration().get(Types.TYPE).switchSetting("Ship", true);
               var5.getAiConfiguration().get(Types.ACTIVE).switchSetting("true", true);
               var5.getAiConfiguration().applyServerSettings();
            }

            if (this.railTag != null) {
               assert this.dockTo != null;

               var5.railController.fromTag(this.railTag, this.en.isChunk16() ? 8 : 0, false);
               var5.railController.modifyDockingRequestNames(this.uniqueIdentifier, this.dockTo);

               assert this.parent != null;

               if (this.parent != null) {
                  assert this.uniqueIdentifier.startsWith(this.en.getType().type.dbPrefix);

                  this.parent.railController.addExpectedToDock(var5.railController.getCurrentRailRequest());
               }
            } else if (this.dockTo != null) {
               var9 = (BlueprintEntry)this.en;
               System.err.println("[STATIONOUTLINE] ADDING DELAYED DOCK " + this.uniqueIdentifier + " to " + this.dockTo);
               var5.getDockingController().getSize().set(var9.getDockingSize());
               Quat4f var11 = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
               var5.getDockingController().requestDelayedDock(new String(this.dockTo), new Vector3i(var9.getDockingPos()), var11, var9.getDockingOrientation());
               var5.setHidden(true);
            }

            if (var2) {
               var5.setProspectedElementCount(this.en.getElementMap());
            }

            var5.getShoppingAddOn().setTradeNodeOn(this.tradeNode);
            var5.blueprintIdentifier = this.blueprintUID;
            var5.blueprintSegmentDataPath = this.blueprintFolder;
            System.err.println("[BLUEPRINT] UID: " + this.uniqueIdentifier + "; writeUID: " + var5.getWriteUniqueIdentifier());
            var5.setScrap(this.scrap);
            var5.getManagerContainer().getShoppingAddOn().clearInventory(false);
            if (this.shop) {
               var5.getManagerContainer().getShoppingAddOn().getOwnerPlayers().clear();
               var5.getManagerContainer().getShoppingAddOn().fillInventory(false, false);
            }

            var5.getManagerContainer().getShoppingAddOn().setInfiniteSupply(false);
            var4.onSpawn(var5);
         }
      } catch (Exception var8) {
         var6 = null;
         var8.printStackTrace();
      }

      return var5;
   }

   public long spawnInDatabase(Vector3i var1, GameServerState var2, int var3, ObjectArrayList var4, ChildStats var5, boolean var6) throws SQLException, EntityAlreadyExistsException, StateParameterNotFoundException, IOException {
      Transform var7;
      (var7 = new Transform()).setFromOpenGLMatrix(this.mat);
      if (var6) {
         try {
            SectorUtil.removeFromDatabaseAndEntityFile(var2, SimpleTransformableSendableObject.EntityType.SPACE_STATION.dbPrefix + DatabaseEntry.removePrefixWOException(this.uniqueIdentifier));
         } catch (SQLException var12) {
            var12.printStackTrace();
         }
      }

      var4.add(this.uniqueIdentifier);
      EntityRequest.existsIdentifier(var2, this.uniqueIdentifier);
      SpaceStation var8;
      (var8 = EntityRequest.getNewSpaceStation(var2, this.uniqueIdentifier, -123, this.realName, this.mat, this.min.x, this.min.y, this.min.z, this.max.x, this.max.y, this.max.z, this.en.isChunk16())).setFactionId(this.factionId);
      var8.getControlElementMap().setFromMap(this.en.getControllingMap());
      var8.getWorldTransform().set(var7);
      var8.getInitialTransform().set(var7);
      var8.npcSystem = this.npcSystem;
      var8.npcSpec = this.npcSpec;
      var8.setSpawnedInDatabaseAsChunk16(this.en.isChunk16());
      var8.usedOldPowerFromTagForcedWrite = this.en.isOldPowerFlag();
      var8.usedOldPowerFromTag = this.en.isOldPowerFlag();
      var8.oldPowerBlocksFromBlueprint = this.en.getElementMap().get((short)2);
      if (this.factionId != 0 && var8.getType() == SimpleTransformableSendableObject.EntityType.SHIP) {
         var8.getAiConfiguration().get(Types.AIM_AT).switchSetting("Any", true);
         var8.getAiConfiguration().get(Types.TYPE).switchSetting("Ship", true);
         var8.getAiConfiguration().get(Types.ACTIVE).switchSetting("true", true);
         var8.getAiConfiguration().applyServerSettings();
      }

      var8.blueprintIdentifier = this.blueprintUID;
      var8.blueprintSegmentDataPath = this.blueprintFolder;
      var8.transientSectorPos.set(var1);
      ElementCountMap var9 = null;
      if (this.itemsToSpawnWith != null && this.en.getCapacitySingle() > 0.0D) {
         (var9 = new ElementCountMap()).transferFrom(this.itemsToSpawnWith, this.en.getCapacitySingle());
      }

      var8.itemsToSpawnWith = var9;
      if (this.railTag != null) {
         assert this.dockTo != null;

         var8.railController.fromTag(this.railTag, 0, false);
         var8.railController.modifyDockingRequestNames(this.uniqueIdentifier, this.dockTo);
      } else if (this.dockTo != null) {
         BlueprintEntry var10 = (BlueprintEntry)this.en;
         System.err.println("[SHIPOUTLINE] DATABASE ADDING DELAYED DOCK " + this.uniqueIdentifier + " to " + this.dockTo);
         var8.getDockingController().getSize().set(var10.getDockingSize());
         Quat4f var11 = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
         var8.getDockingController().tagOverwrite = new DockingTagOverwrite(this.dockTo, new Vector3i(var10.getDockingPos()), var11, var10.getDockingOrientation());
      }

      var8.getShoppingAddOn().setTradeNodeOn(this.tradeNode);
      var2.getController().writeEntity(var8, false);
      var8.getDockingController().tagOverwrite = null;
      long var15 = var2.getDatabaseIndex().getTableManager().getEntityTable().updateOrInsertSegmentController(DatabaseEntry.removePrefixWOException(this.uniqueIdentifier), var1, var8.getType().dbTypeId, Universe.getRandom().nextLong(), "<sim>", "<sim>", this.realName, false, this.factionId, var7.origin, this.min, this.max, var3, true, this.parentDbId, this.rootDb, false);
      if (var8.npcSystem != null) {
         assert var8.npcSpec != null;

         var8.npcSystem.getContingent().spawn(var8.npcSpec, var15);
         var8.npcSystem = null;
         var8.npcSpec = null;
      }

      if (this.childs != null) {
         Iterator var13 = this.childs.iterator();

         while(var13.hasNext()) {
            SegmentControllerOutline var14;
            (var14 = (SegmentControllerOutline)var13.next()).dockTo = this.uniqueIdentifier;
            var14.parentDbId = var15;
            if (this.parentDbId <= 0L) {
               var14.rootDb = var15;
            } else {
               var14.rootDb = this.rootDb;
            }

            var14.itemsToSpawnWith = this.itemsToSpawnWith;
            var14.spawnInDatabase(var1, var2, var3, var4, var5, var6);
         }
      }

      return var15;
   }
}
