package org.schema.game.server.data.blueprint;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.Planet;
import org.schema.game.server.controller.EntityAlreadyExistsException;
import org.schema.game.server.data.BlueprintInterface;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;

public class PlanetOutline extends SegmentControllerOutline {
   public PlanetOutline(GameServerState var1, BlueprintInterface var2, String var3, String var4, float[] var5, int var6, Vector3i var7, Vector3i var8, String var9, boolean var10, Vector3i var11, ChildStats var12) {
      super(var1, var2, var3, var4, var5, var7, var8, var9, var10, var11, var12);
   }

   public Planet spawn(Vector3i var1, boolean var2, ChildStats var3, SegmentControllerSpawnCallback var4) throws EntityAlreadyExistsException, StateParameterNotFoundException {
      throw new IllegalAccessError();
   }

   public long spawnInDatabase(Vector3i var1, GameServerState var2, int var3, ObjectArrayList var4, ChildStats var5, boolean var6) {
      throw new IllegalArgumentException();
   }
}
