package org.schema.game.server.data.blueprint;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.game.common.data.world.Sector;
import org.schema.game.server.controller.EntityAlreadyExistsException;
import org.schema.game.server.data.BlueprintInterface;
import org.schema.game.server.data.EntityRequest;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;

public class ShopOutline extends SegmentControllerOutline {
   private int sentinelTeam;

   public ShopOutline(GameServerState var1, BlueprintInterface var2, String var3, String var4, float[] var5, int var6, Vector3i var7, Vector3i var8, String var9, boolean var10, Vector3i var11, ChildStats var12) {
      super(var1, var2, var3, var4, var5, var7, var8, var9, var10, var11, var12);
      this.sentinelTeam = var6;
   }

   public ShopSpaceStation spawn(Vector3i var1, boolean var2, ChildStats var3, SegmentControllerSpawnCallback var4) throws EntityAlreadyExistsException, StateParameterNotFoundException {
      try {
         Sector var6 = var4.sector;
         EntityRequest.existsIdentifier(this.state, this.uniqueIdentifier);
         ShopSpaceStation var7;
         (var7 = EntityRequest.getNewShop(this.state, this.uniqueIdentifier, var6.getId(), this.realName, this.mat, this.min.x, this.min.y, this.min.z, this.max.x, this.max.y, this.max.z, this.playerUID, this.en.isChunk16())).setFactionId(this.sentinelTeam);
         var7.getControlElementMap().setFromMap(this.en.getControllingMap());
         if (var6 != null && var6.isActive()) {
            var4.onSpawn(var7);
            return var7;
         }

         var4.onNullSector(var7);
      } catch (Exception var5) {
         var5.printStackTrace();
      }

      return null;
   }

   public long spawnInDatabase(Vector3i var1, GameServerState var2, int var3, ObjectArrayList var4, ChildStats var5, boolean var6) {
      throw new IllegalArgumentException();
   }
}
