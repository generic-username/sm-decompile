package org.schema.game.server.data.blueprint;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;

public class BluePrintWriteQueueElement {
   public final SegmentController segmentController;
   public final boolean local;
   public String name;
   public boolean requestedAdditionalBlueprintData;
   public final BlueprintClassification classification;

   public BluePrintWriteQueueElement(SegmentController var1, String var2, BlueprintClassification var3, boolean var4) {
      this.segmentController = var1;
      this.name = var2;
      this.local = var4;
      this.classification = var3;
   }
}
