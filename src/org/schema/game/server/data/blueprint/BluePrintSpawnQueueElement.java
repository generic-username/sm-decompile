package org.schema.game.server.data.blueprint;

import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.inventory.Inventory;

public class BluePrintSpawnQueueElement {
   public final String catalogName;
   public final String shipName;
   public boolean activeAI;
   public boolean infiniteShop;
   public boolean directBuy;
   public int factionId;
   public int metaItem = -1;
   public Inventory inv;
   public SegmentPiece toDockOn;

   public BluePrintSpawnQueueElement(String var1, String var2, int var3, boolean var4, boolean var5, boolean var6, SegmentPiece var7) {
      this.catalogName = var1;
      this.shipName = var2;
      this.activeAI = var5;
      this.infiniteShop = var4;
      this.directBuy = var6;
      this.factionId = var3;
      this.toDockOn = var7;
   }
}
