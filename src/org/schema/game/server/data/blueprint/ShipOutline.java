package org.schema.game.server.data.blueprint;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import javax.vecmath.Quat4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.data.VoidSegmentPiece;
import org.schema.game.common.data.VoidUniqueSegmentPiece;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.controller.EntityAlreadyExistsException;
import org.schema.game.server.controller.SectorUtil;
import org.schema.game.server.data.BlueprintInterface;
import org.schema.game.server.data.EntityRequest;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;

public class ShipOutline extends SegmentControllerOutline {
   public ShipOutline(GameServerState var1, BlueprintInterface var2, String var3, String var4, float[] var5, int var6, Vector3i var7, Vector3i var8, String var9, boolean var10, Vector3i var11, ChildStats var12) {
      super(var1, var2, var3, var4, var5, var7, var8, var9, var10, var11, var12);
      this.factionId = var6;
      this.checkForChilds(this.factionId);
   }

   public Ship spawn(Vector3i var1, boolean var2, ChildStats var3, SegmentControllerSpawnCallback var4) throws EntityAlreadyExistsException {
      assert var1 != null;

      this.checkOkName();
      Sector var5 = var4.sector;
      Ship var6 = null;

      try {
         if (var5 == null || var5.isActive()) {
            (var6 = EntityRequest.getNewShip(this.state, this.uniqueIdentifier, var5 != null ? var5.getId() : -1, this.realName, this.mat, this.min.x, this.min.y, this.min.z, this.max.x, this.max.y, this.max.z, this.playerUID, this.en.isChunk16())).setFactionId(this.factionId);
            var6.setFactionFromBlueprint(true);
            var6.npcSystem = this.npcSystem;
            var6.npcSpec = this.npcSpec;
            ElementCountMap var7 = null;
            if (this.itemsToSpawnWith != null && this.en.getCapacitySingle() > 0.0D) {
               (var7 = new ElementCountMap()).transferFrom(this.itemsToSpawnWith, this.en.getCapacitySingle());
            }

            var6.itemsToSpawnWith = var7;
            if (var3.usedNamed.contains(this.uniqueIdentifier)) {
               assert false : "Already exists: " + this.uniqueIdentifier + "; USED: " + var3.usedNamed;

               throw new EntityAlreadyExistsException(this.uniqueIdentifier);
            }

            var3.usedNamed.add(this.uniqueIdentifier);
            System.err.println("[BB] SETTING FACTION OF " + var6 + " to " + this.factionId);
            long var8 = System.currentTimeMillis();
            var6.usedOldPowerFromTag = this.en.isOldPowerFlag();
            var6.oldPowerBlocksFromBlueprint = this.en.getElementMap().get((short)2);
            var6.getControlElementMap().setFromMap(this.en.getControllingMap());
            long var10 = System.currentTimeMillis() - var8;
            long var12 = 0L;
            BlueprintEntry var16;
            if (this.en instanceof BlueprintEntry) {
               if ((var16 = (BlueprintEntry)this.en).rootRead) {
                  var3.rootName = this.uniqueIdentifier;
               }

               if (var16.getManagerTag() != null) {
                  var6.getManagerContainer().loadInventoriesFromTag = false;
                  System.err.println("[OUTLINE) BLUEPRINT MANAGER TAG EXISTS");
                  var8 = System.currentTimeMillis();
                  var6.getManagerContainer().fromTagStructure(var16.getManagerTag());
                  var6.getManagerContainer().handleBlueprintWireless(var3.rootName, var16.wirelessToOwnRail);
                  var12 = System.currentTimeMillis() - var8;
                  var6.getManagerContainer().loadInventoriesFromTag = true;
               }
            }

            var6.setTouched(true, false);
            if (this.childs != null) {
               Iterator var17;
               SegmentControllerOutline var18;
               for(var17 = this.childs.iterator(); var17.hasNext(); var18.parentOutline = this) {
                  (var18 = (SegmentControllerOutline)var17.next()).parent = var6;
               }

               var17 = this.childs.iterator();

               while(var17.hasNext()) {
                  (var18 = (SegmentControllerOutline)var17.next()).scrap = this.scrap;
                  var18.dockTo = this.uniqueIdentifier;
                  var18.itemsToSpawnWith = this.itemsToSpawnWith;

                  assert var18.dockTo != null;

                  var18.spawn(var1, false, var3, var4);
               }
            }

            if (this.en.getAiTag() != null) {
               var6.getAiConfiguration().fromTagStructure(this.en.getAiTag());
            }

            if (this.factionId != 0 && this.en.getAiTag() == null) {
               var6.getAiConfiguration().get(Types.AIM_AT).switchSetting("Any", false);
               if (this.dockTo != null && ((BlueprintEntry)this.en).getDockingStyle() == 7) {
                  var6.getAiConfiguration().get(Types.TYPE).switchSetting("Turret", false);
               } else {
                  var6.getAiConfiguration().get(Types.TYPE).switchSetting("Ship", false);
               }
            }

            if (!var6.getAiConfiguration().get(Types.ACTIVE).isOn()) {
               var6.getAiConfiguration().get(Types.ACTIVE).switchSetting(this.activeAI ? "true" : "false", false);
            }

            var6.getAiConfiguration().applyServerSettings();
            var6.blueprintIdentifier = this.blueprintUID;
            var6.blueprintSegmentDataPath = this.blueprintFolder;
            var6.setScrap(this.scrap);
            if (this.railTag != null) {
               assert this.dockTo != null;

               var6.railController.fromTag(this.railTag, this.en.isChunk16() ? 8 : 0, false);
               var6.railController.modifyDockingRequestNames(this.uniqueIdentifier, this.dockTo);

               assert this.parent != null;

               if (this.parent != null) {
                  assert this.uniqueIdentifier.startsWith(this.en.getType().type.dbPrefix);

                  this.parent.railController.addExpectedToDock(var6.railController.getCurrentRailRequest());
               }

               assert var6.railController.railRequestCurrent.docked.uniqueIdentifierSegmentController.equals(var6.getUniqueIdentifier());

               assert !var6.railController.railRequestCurrent.rail.uniqueIdentifierSegmentController.equals(var6.getUniqueIdentifier());

               assert !this.dockTo.equals(this.uniqueIdentifier) : "WARNING: DOCK TO SELF: " + this.dockTo;

               assert !this.dockTo.equals(var6.getUniqueIdentifier()) : "WARNING: DOCK TO SELF: " + this.dockTo;
            } else if (this.dockTo != null) {
               var16 = (BlueprintEntry)this.en;
               System.err.println("[SHIPOUTLINE] ADDING DELAYED DOCK " + this.uniqueIdentifier + " to " + this.dockTo);
               var6.getDockingController().getSize().set(var16.getDockingSize());
               Quat4f var19 = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
               var1 = new Vector3i(var16.getDockingPos());
               var6.getDockingController().requestDelayedDock(new String(this.dockTo), var1, var19, var16.getDockingOrientation());
               var6.setHidden(true);
            }

            if (this.railToSpawnOn != null) {
               if (this.en.getDockerPoints() != null && !this.en.getDockerPoints().isEmpty()) {
                  VoidSegmentPiece var20;
                  ((VoidUniqueSegmentPiece)(var20 = (VoidSegmentPiece)this.en.getDockerPoints().values().iterator().next())).setSegmentController(var6);
                  System.err.println("[SHIPOUTLINE] Connecting docker to rail \n" + this.railToSpawnOn + " \n-> \n" + this.railToSpawnOn);
                  var6.railController.connectServer(var20, this.railToSpawnOn);
               } else {
                  var4.onNoDocker();
               }
            }

            if (var2) {
               String var10000 = this.realName;
               var6.setProspectedElementCount(this.en, this.playerUID);
            }

            if (var5 != null) {
               var4.onSpawn(var6);
            } else {
               var4.onNullSector(var6);
            }

            if (var10 > 500L || var12 > 500L) {
               System.err.println("[SHIPOUTLINE] WARNING: spawning of " + this.uniqueIdentifier + " took long -- tControlMap: " + var10 + "ms; tTag: " + var12 + "ms");
            }
         }
      } catch (Exception var15) {
         var15.printStackTrace();
      }

      if (this.removeAfterSpawn != null && this.en instanceof BlueprintEntry) {
         try {
            this.removeAfterSpawn.removeBluePrint((BlueprintEntry)this.en);
         } catch (IOException var14) {
            var14.printStackTrace();
         }
      }

      return var6;
   }

   public long spawnInDatabase(Vector3i var1, GameServerState var2, int var3, ObjectArrayList var4, ChildStats var5, boolean var6) throws SQLException, EntityAlreadyExistsException, StateParameterNotFoundException, IOException {
      Transform var7;
      (var7 = new Transform()).setFromOpenGLMatrix(this.mat);
      if (var6) {
         try {
            SectorUtil.removeFromDatabaseAndEntityFile(var2, SimpleTransformableSendableObject.EntityType.SHIP.dbPrefix + DatabaseEntry.removePrefixWOException(this.uniqueIdentifier));
         } catch (SQLException var12) {
            var12.printStackTrace();
         }
      }

      EntityRequest.existsIdentifier(var2, this.uniqueIdentifier);
      Ship var8;
      (var8 = EntityRequest.getNewShip(var2, this.uniqueIdentifier, -123, this.realName, this.mat, this.min.x, this.min.y, this.min.z, this.max.x, this.max.y, this.max.z, this.playerUID, this.en.isChunk16())).npcSystem = this.npcSystem;
      var8.npcSpec = this.npcSpec;
      var8.setFactionId(this.factionId);
      var8.getControlElementMap().setFromMap(this.en.getControllingMap());
      var8.getWorldTransform().set(var7);
      var8.getInitialTransform().set(var7);
      var8.blueprintIdentifier = this.blueprintUID;
      var8.blueprintSegmentDataPath = this.blueprintFolder;
      var8.setSpawnedInDatabaseAsChunk16(this.en.isChunk16());
      var8.usedOldPowerFromTagForcedWrite = this.en.isOldPowerFlag();
      var8.usedOldPowerFromTag = this.en.isOldPowerFlag();
      var8.oldPowerBlocksFromBlueprint = this.en.getElementMap().get((short)2);
      if (this.factionId != 0) {
         var8.getAiConfiguration().get(Types.AIM_AT).switchSetting("Any", true);
         var8.getAiConfiguration().get(Types.TYPE).switchSetting("Ship", true);
         var8.getAiConfiguration().get(Types.ACTIVE).switchSetting("true", true);
         var8.getAiConfiguration().applyServerSettings();
      }

      var8.transientSectorPos.set(var1);
      if (this.railTag != null) {
         assert this.dockTo != null;

         var8.railController.fromTag(this.railTag, this.en.isChunk16() ? 8 : 0, false);
         var8.railController.modifyDockingRequestNames(this.uniqueIdentifier, this.dockTo);
      } else if (this.dockTo != null) {
         BlueprintEntry var9 = (BlueprintEntry)this.en;
         System.err.println("[SHIPOUTLINE] DATABASE ADDING DELAYED DOCK " + this.uniqueIdentifier + " to " + this.dockTo);
         var8.getDockingController().getSize().set(var9.getDockingSize());
         Quat4f var10 = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
         var8.getDockingController().tagOverwrite = new DockingTagOverwrite(this.dockTo, new Vector3i(var9.getDockingPos()), var10, var9.getDockingOrientation());
      }

      ElementCountMap var15 = null;
      if (this.itemsToSpawnWith != null && this.en.getCapacitySingle() > 0.0D) {
         (var15 = new ElementCountMap()).transferFrom(this.itemsToSpawnWith, this.en.getCapacitySingle());
      }

      var8.itemsToSpawnWith = var15;
      var2.getController().writeEntity(var8, false);
      var8.getDockingController().tagOverwrite = null;
      System.err.println("[BLUEPRINT][SPAWNINDB] added to members: " + this.uniqueIdentifier + " in sector " + var1);
      var4.add(new String(this.uniqueIdentifier));
      long var16 = var2.getDatabaseIndex().getTableManager().getEntityTable().updateOrInsertSegmentController(DatabaseEntry.removePrefixWOException(this.uniqueIdentifier), var1, var8.getType().dbTypeId, Universe.getRandom().nextLong(), "<sim>", "<sim>", this.realName, false, this.factionId, var7.origin, this.min, this.max, var3, true, this.parentDbId, this.rootDb, false);
      if (var8.npcSystem != null) {
         assert var8.npcSpec != null;

         var8.npcSystem.getContingent().spawn(var8.npcSpec, var16);
         var8.npcSystem = null;
         var8.npcSpec = null;
      }

      if (this.childs != null) {
         Iterator var13 = this.childs.iterator();

         while(var13.hasNext()) {
            SegmentControllerOutline var14;
            (var14 = (SegmentControllerOutline)var13.next()).dockTo = this.uniqueIdentifier;
            var14.parentDbId = var16;
            if (this.parentDbId <= 0L) {
               var14.rootDb = var16;
            } else {
               var14.rootDb = this.rootDb;
            }

            var14.itemsToSpawnWith = this.itemsToSpawnWith;
            var14.spawnInDatabase(var1, var2, var3, var4, var5, var6);
         }
      }

      return var16;
   }
}
