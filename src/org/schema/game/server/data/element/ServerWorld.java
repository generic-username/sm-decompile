package org.schema.game.server.data.element;

public abstract class ServerWorld {
   protected int segmentWidth;
   protected int segmentHeight;
   protected int segmentDepth;
}
