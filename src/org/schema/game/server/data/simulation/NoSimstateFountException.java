package org.schema.game.server.data.simulation;

public class NoSimstateFountException extends Exception {
   private static final long serialVersionUID = 1L;

   public NoSimstateFountException(String var1) {
      super(var1);
   }
}
