package org.schema.game.server.data.simulation;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map.Entry;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.game.common.data.player.catalog.CatalogWavePermission;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.ai.program.simpirates.PirateSimulationProgram;
import org.schema.game.server.ai.program.simpirates.TradingRouteSimulationProgram;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.game.server.data.blueprintnw.BlueprintType;
import org.schema.game.server.data.simulation.groups.AttackSingleEntitySimulationGroup;
import org.schema.game.server.data.simulation.groups.RavegingSimulationGroup;
import org.schema.game.server.data.simulation.groups.SimulationGroup;
import org.schema.game.server.data.simulation.groups.TargetSectorSimulationGroup;
import org.schema.game.server.data.simulation.jobs.SimulationJob;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.RegisteredClientInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.DiskWritable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class SimulationManager implements DiskWritable {
   private final ObjectArrayList simulationGroups = new ObjectArrayList();
   private final GameServerState state;
   private final SimulationPlanner planner;
   private final ObjectArrayFIFOQueue jobs = new ObjectArrayFIFOQueue();
   private long startTime = 0L;
   private long runningTime = 0L;
   private long lastupdateTime = 0L;
   private long uniquegroups;

   public SimulationManager(GameServerState var1) {
      this.state = var1;
      this.startTime = System.currentTimeMillis();
      this.lastupdateTime = this.startTime;
      this.planner = new SimulationPlanner(this);
   }

   public void addGroup(SimulationGroup var1) {
      synchronized(this.simulationGroups) {
         if (this.simulationGroups.size() < (Integer)ServerConfig.CONCURRENT_SIMULATION.getCurrentState()) {
            this.simulationGroups.add(var1);
         } else {
            System.err.println("[SIMUALTION] WARNING: Simulation group " + var1 + " ignored: MAX GROUPS REACHED " + this.simulationGroups.size() + "/" + ServerConfig.CONCURRENT_SIMULATION.getCurrentState());
         }

      }
   }

   public void addJob(SimulationJob var1) {
      synchronized(this.jobs) {
         this.jobs.enqueue(var1);
      }
   }

   public void aggressive(Ship var1, SimpleTransformableSendableObject var2, float var3) {
      for(int var4 = 0; var4 < this.simulationGroups.size(); ++var4) {
         SimulationGroup var5;
         if ((var5 = (SimulationGroup)this.simulationGroups.get(var4)).getMembers().contains(var1.getUniqueIdentifier())) {
            var5.aggro(var2, var3);
         }
      }

   }

   public void createRandomPirateGroup(Vector3i var1, int var2) {
      if (this.existsGroupInSector(var1)) {
         System.err.println("[SIM] cannot spawn group: collision at " + var1);
      } else {
         synchronized(this.simulationGroups) {
            if (this.simulationGroups.size() >= (Integer)ServerConfig.CONCURRENT_SIMULATION.getCurrentState()) {
               System.err.println("[SIM] cannot spawn group: LIMIT REACHED " + this.simulationGroups.size());
               return;
            }
         }

         RavegingSimulationGroup var3;
         (var3 = new RavegingSimulationGroup(this.getState())).createFromBlueprints(var1, this.getUniqueGroupUId(), -1, this.getBlueprintList(var2, 1, -1));
         var3.setCurrentProgram(new PirateSimulationProgram(var3, false));
         this.addGroup(var3);
      }
   }

   public boolean createRandomPiratePatrolGroup(Vector3i var1, Vector3i var2, int var3) {
      if (this.existsGroupInSector(var1)) {
         System.err.println("[SIM] cannot spawn group: collision at " + var1);
         return false;
      } else {
         synchronized(this.simulationGroups) {
            if (this.simulationGroups.size() >= (Integer)ServerConfig.CONCURRENT_SIMULATION.getCurrentState()) {
               System.err.println("[SIM] cannot spawn group: LIMIT REACHED " + this.simulationGroups.size());
               return false;
            }
         }

         TargetSectorSimulationGroup var4;
         (var4 = new TargetSectorSimulationGroup(this.getState(), new Vector3i(var2))).createFromBlueprints(var1, this.getUniqueGroupUId(), -1, this.getBlueprintList(var3, 1, -1));
         var4.setCurrentProgram(new TradingRouteSimulationProgram(var4, false));
         this.addGroup(var4);
         return true;
      }
   }

   public boolean createRandomTradigRouteGroup(Vector3i var1, Vector3i var2, int var3) {
      if (this.existsGroupInSector(var1)) {
         System.err.println("[SIM] cannot spawn group: collision at " + var1);
         return false;
      } else {
         synchronized(this.simulationGroups) {
            if (this.simulationGroups.size() >= (Integer)ServerConfig.CONCURRENT_SIMULATION.getCurrentState()) {
               System.err.println("[SIM] cannot spawn group: LIMIT REACHED " + this.simulationGroups.size());
               return false;
            }
         }

         TargetSectorSimulationGroup var4 = new TargetSectorSimulationGroup(this.getState(), new Vector3i(var2));
         if (ServerConfig.SIMULATION_TRADING_FILLS_SHOPS.isOn()) {
            var4.hasStockToDeliver = true;
         }

         boolean var5 = false;

         try {
            Sector var11 = var4.getState().getUniverse().getSector(new Vector3i(var2));
            Iterator var6 = var4.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

            while(var6.hasNext()) {
               Sendable var7;
               if ((var7 = (Sendable)var6.next()) instanceof ShopSpaceStation && ((ShopSpaceStation)var7).getSectorId() == var11.getId() && ((ShopSpaceStation)var7).isAiShop()) {
                  var4.createFromBlueprints(var1, this.getUniqueGroupUId(), -2, this.getBlueprintList(var3, 1, -2));
                  TradingRouteSimulationProgram var10 = new TradingRouteSimulationProgram(var4, false);
                  var4.setCurrentProgram(var10);
                  this.addGroup(var4);
                  var5 = true;
                  break;
               }
            }
         } catch (IOException var9) {
            var9.printStackTrace();
         }

         return var5;
      }
   }

   public void disband(SimulationGroup var1) {
      System.err.println("[Simulation] disbanding sim group: " + var1);
      var1.deleteMembers();
      this.removeGroup(var1);
   }

   public boolean existsGroupInSector(Vector3i var1) {
      Vector3i var2 = new Vector3i();

      for(int var3 = 0; var3 < this.simulationGroups.size(); ++var3) {
         SimulationGroup var4;
         Iterator var5 = (var4 = (SimulationGroup)this.simulationGroups.get(var3)).getMembers().iterator();

         while(var5.hasNext()) {
            String var6 = (String)var5.next();

            try {
               if (var4.getSector(var6, var2).equals(var1)) {
                  return true;
               }
            } catch (EntityNotFountException var7) {
               var7.printStackTrace();
               System.err.println("Exception REMOVING GROUP NOW " + var4.getMembers());
               this.simulationGroups.remove(var3);
               --var3;
               break;
            } catch (SQLException var8) {
               var8.printStackTrace();
            }
         }
      }

      return false;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var10;
      (Byte)(var10 = (Tag[])var1.getValue())[0].getValue();
      Tag[] var2 = (Tag[])var10[1].getValue();

      for(int var3 = 0; var3 < var2.length && var2[var3].getType() != Tag.Type.FINISH; ++var3) {
         synchronized(this.simulationGroups) {
            int var5 = (Integer)((Tag[])var2[var3].getValue())[1].getValue();

            SimulationGroup var6;
            try {
               var6 = SimulationGroup.GroupType.values()[var5].clazz.instantiate(this.getState());
               System.err.println("[SIM] loading simulation group type: " + var5 + ": " + var6);
               var6.fromTagStructure(var2[var3]);
               this.addGroup(var6);
            } catch (SecurityException var7) {
               var6 = null;
               var7.printStackTrace();

               assert false;
            } catch (IllegalArgumentException var8) {
               var6 = null;
               var8.printStackTrace();
            }
         }
      }

      if (var10.length > 2 && var10[2].getType() == Tag.Type.LONG) {
         this.uniquegroups = (Long)var10[2].getValue();
      }

   }

   public Tag toTagStructure() {
      Tag var1 = new Tag(Tag.Type.BYTE, (String)null, (byte)0);
      Tag var6;
      synchronized(this.simulationGroups) {
         Tag[] var2 = new Tag[this.simulationGroups.size() + 1];
         int var4 = 0;

         while(true) {
            if (var4 >= this.simulationGroups.size()) {
               var2[this.simulationGroups.size()] = FinishTag.INST;
               var6 = new Tag(Tag.Type.STRUCT, (String)null, var2);
               break;
            }

            var2[var4] = ((SimulationGroup)this.simulationGroups.get(var4)).toTagStructure();
            ++var4;
         }
      }

      Tag var3 = new Tag(Tag.Type.LONG, (String)null, this.uniquegroups);
      return new Tag(Tag.Type.STRUCT, "SimulationState", new Tag[]{var1, var6, var3, FinishTag.INST});
   }

   public CatalogPermission[] getBlueprintList(int var1, int var2, int var3, BlueprintType... var4) {
      Collection var5 = this.state.getCatalogManager().getCatalog();
      short var6 = -1;
      int var7 = Integer.MAX_VALUE;
      Object2ObjectOpenHashMap var8 = new Object2ObjectOpenHashMap();
      Iterator var14 = var5.iterator();

      while(true) {
         CatalogPermission var9;
         boolean var10;
         do {
            if (!var14.hasNext()) {
               ObjectIterator var15 = var8.entrySet().iterator();
               ArrayList var16 = new ArrayList();

               while(true) {
                  int var21;
                  while(var15.hasNext()) {
                     Entry var18;
                     if (((CatalogWavePermission)(var18 = (Entry)var15.next()).getValue()).difficulty != var6) {
                        var15.remove();
                     } else {
                        for(var21 = 0; var21 < ((CatalogWavePermission)var18.getValue()).amount; ++var21) {
                           var16.add(var18.getKey());
                        }
                     }
                  }

                  if (var8.size() <= 0) {
                     return this.getBlueprintListOld(var1, var2, var3, var4);
                  }

                  CatalogPermission[] var20 = new CatalogPermission[var16.size()];

                  for(var21 = 0; var21 < var16.size(); ++var21) {
                     var20[var21] = (CatalogPermission)var16.get(var21);
                  }

                  return var20;
               }
            }

            var9 = (CatalogPermission)var14.next();
            var10 = false;
            BlueprintType[] var11 = var4;
            int var12 = var4.length;

            for(int var13 = 0; var13 < var12; ++var13) {
               if (var11[var13] == var9.type) {
                  var10 = true;
                  break;
               }
            }
         } while(!var10);

         CatalogWavePermission var19 = null;
         Iterator var22 = var9.wavePermissions.iterator();

         while(var22.hasNext()) {
            int var17;
            CatalogWavePermission var23;
            if ((var23 = (CatalogWavePermission)var22.next()).factionId == var3 && (var17 = Math.abs(var23.difficulty - var2)) < Integer.MAX_VALUE) {
               var6 = var23.difficulty;
               if (var17 < var7) {
                  var6 = var23.difficulty;
                  var7 = var17;
               }

               var19 = var23;
            }
         }

         if (var19 != null) {
            var8.put(var9, var19);
         }
      }
   }

   public CatalogPermission[] getBlueprintList(int var1, int var2, int var3) {
      return this.getBlueprintList(var1, var2, var3, BlueprintType.SHIP);
   }

   public CatalogPermission[] getBlueprintListOld(int var1, int var2, int var3, BlueprintType... var4) {
      Collection var11 = this.getState().getCatalogManager().getCatalog();
      ArrayList var5 = new ArrayList();
      Iterator var12 = var11.iterator();

      int var9;
      while(var12.hasNext()) {
         CatalogPermission var6 = (CatalogPermission)var12.next();
         boolean var7 = false;
         BlueprintType[] var8 = var4;
         var9 = var4.length;

         for(int var10 = 0; var10 < var9; ++var10) {
            if (var8[var10] == var6.type) {
               var7 = true;
               break;
            }
         }

         if (var7 && var6.enemyUsable()) {
            var5.add(var6);
         }
      }

      if (var5.isEmpty()) {
         System.err.println("[WAVE] Server will not spawn any waves, the catalog is empty");
         return new CatalogPermission[0];
      } else {
         Collections.sort(var5, new Comparator() {
            public int compare(CatalogPermission var1, CatalogPermission var2) {
               if (var1.price > var2.price) {
                  return 1;
               } else {
                  return var1.price < var2.price ? -1 : 0;
               }
            }
         });
         float var13 = (float)var5.size() / 10.0F;
         int var14 = (int)Math.min((double)(var5.size() - 1), Math.ceil((double)(var13 * (float)var2)));
         CatalogPermission[] var15 = new CatalogPermission[var1];

         for(int var16 = 0; var16 < var1; ++var16) {
            var9 = Math.min(var5.size() - 1, Math.max(0, var14 - 2 + var16));
            var15[var16] = (CatalogPermission)var5.get(var9);
         }

         return var15;
      }
   }

   public String getDebugStringFor(SimpleTransformableSendableObject var1) {
      for(int var2 = 0; var2 < this.simulationGroups.size(); ++var2) {
         SimulationGroup var3;
         if ((var3 = (SimulationGroup)this.simulationGroups.get(var2)).getMembers().contains(var1.getUniqueIdentifier())) {
            return var3.getDebugString();
         }
      }

      return "NOSIM";
   }

   public SimulationPlanner getPlanner() {
      return this.planner;
   }

   public GameServerState getState() {
      return this.state;
   }

   public String getUniqueIdentifier() {
      return "SIMULATION_STATE";
   }

   public boolean isVolatile() {
      return false;
   }

   public Vector3i getUnloadedSectorAround(Vector3i var1, Vector3i var2) {
      int var3 = 2;
      var2.set(var1);

      for(; this.getState().getUniverse().isSectorLoaded(var2); ++var3) {
         int var4;
         if ((var4 = Universe.getRandom().nextInt(3)) == 0) {
            var2.set(var1.x + var3, var1.y, var1.z);
            if (!this.getState().getUniverse().isSectorLoaded(var2)) {
               break;
            }

            var2.set(var1.x - var3, var1.y, var1.z);
            if (!this.getState().getUniverse().isSectorLoaded(var2)) {
               break;
            }

            var2.set(var1.x, var1.y + var3, var1.z);
            if (!this.getState().getUniverse().isSectorLoaded(var2)) {
               break;
            }

            var2.set(var1.x, var1.y - var3, var1.z);
            if (!this.getState().getUniverse().isSectorLoaded(var2)) {
               break;
            }

            var2.set(var1.x, var1.y, var1.z + var3);
            if (!this.getState().getUniverse().isSectorLoaded(var2)) {
               break;
            }

            var2.set(var1.x, var1.y, var1.z - var3);
         } else if (var4 == 1) {
            var2.set(var1.x, var1.y + var3, var1.z);
            if (!this.getState().getUniverse().isSectorLoaded(var2)) {
               break;
            }

            var2.set(var1.x, var1.y - var3, var1.z);
            if (!this.getState().getUniverse().isSectorLoaded(var2)) {
               break;
            }

            var2.set(var1.x + var3, var1.y, var1.z);
            if (!this.getState().getUniverse().isSectorLoaded(var2)) {
               break;
            }

            var2.set(var1.x - var3, var1.y, var1.z);
            if (!this.getState().getUniverse().isSectorLoaded(var2)) {
               break;
            }

            var2.set(var1.x, var1.y, var1.z + var3);
            if (!this.getState().getUniverse().isSectorLoaded(var2)) {
               break;
            }

            var2.set(var1.x, var1.y, var1.z - var3);
         } else {
            var2.set(var1.x, var1.y, var1.z + var3);
            if (!this.getState().getUniverse().isSectorLoaded(var2)) {
               break;
            }

            var2.set(var1.x, var1.y, var1.z - var3);
            if (!this.getState().getUniverse().isSectorLoaded(var2)) {
               break;
            }

            var2.set(var1.x + var3, var1.y, var1.z);
            if (!this.getState().getUniverse().isSectorLoaded(var2)) {
               break;
            }

            var2.set(var1.x - var3, var1.y, var1.z);
            if (!this.getState().getUniverse().isSectorLoaded(var2)) {
               break;
            }

            var2.set(var1.x, var1.y + var3, var1.z);
            if (!this.getState().getUniverse().isSectorLoaded(var2)) {
               break;
            }

            var2.set(var1.x, var1.y - var3, var1.z);
            if (!this.getState().getUniverse().isSectorLoaded(var2)) {
               break;
            }
         }
      }

      return var2;
   }

   public void initialize() {
      try {
         Tag var1 = this.getState().getController().readEntity("SIMULATION_STATE", "sim");
         this.fromTagStructure(var1);
      } catch (IOException var2) {
         System.err.println("ERROR LOADING SIMULATION_STATE.sim");
         var2.printStackTrace();
      } catch (EntityNotFountException var3) {
         System.err.println("[SIMULATION] no simulation state found on disk. creating new...");
      } catch (Exception var4) {
         System.err.println("ERROR LOADING SIMULATION_STATE.sim");
         var4.printStackTrace();
      }

      this.planner.start();
   }

   public void onAIDeactivated(SimpleTransformableSendableObject var1) {
      if (var1.getUniqueIdentifier().startsWith("ENTITY_SHIP_MOB_SIM")) {
         this.removeMemberFromGroups(var1.getUniqueIdentifier());
      }

   }

   public void removeGroup(SimulationGroup var1) {
      synchronized(this.simulationGroups) {
         this.simulationGroups.remove(var1);
      }
   }

   public void removeMemberFromGroups(String var1) {
      for(int var2 = 0; var2 < this.simulationGroups.size(); ++var2) {
         ((SimulationGroup)this.simulationGroups.get(var2)).getMembers().remove(var1);
      }

   }

   public SimulationGroup sendToAttackSpecific(SimpleTransformableSendableObject var1, int var2, int var3) {
      synchronized(this.simulationGroups) {
         if (this.simulationGroups.size() >= (Integer)ServerConfig.CONCURRENT_SIMULATION.getCurrentState()) {
            System.err.println("[SIM] cannot spawn group: LIMIT REACHED " + this.simulationGroups.size());
            return null;
         }
      }

      Sector var4;
      if ((var4 = this.getState().getUniverse().getSector(var1.getSectorId())) != null) {
         AttackSingleEntitySimulationGroup var5 = new AttackSingleEntitySimulationGroup(this.getState(), new Vector3i(var4.pos), var1.getUniqueIdentifier());
         Vector3i var6 = this.getUnloadedSectorAround(var4.pos, new Vector3i());
         if (this.existsGroupInSector(var6)) {
            System.err.println("[SIM] cannot spawn group: collision at " + var6);
            return null;
         } else {
            System.err.println("[SIM] Sending group from: " + var6 + " (position of attack target: " + var4.pos + ")");
            CatalogPermission[] var11;
            CatalogPermission[] var12;
            int var7 = (var12 = var11 = this.getBlueprintList(var3, 1, var2)).length;

            for(int var8 = 0; var8 < var7; ++var8) {
               CatalogPermission var9 = var12[var8];

               assert var9.type == BlueprintType.SHIP;
            }

            var5.createFromBlueprints(var6, this.getUniqueGroupUId(), var2, var11);
            TradingRouteSimulationProgram var13;
            (var13 = new TradingRouteSimulationProgram(var5, false)).setSpecificTargetId(var1.getId());
            var5.setCurrentProgram(var13);
            this.addGroup(var5);
            return var5;
         }
      } else {
         return null;
      }
   }

   private synchronized long getUniqueGroupUId() {
      return (long)(this.uniquegroups++);
   }

   public void update(Timer var1) {
      if (!this.jobs.isEmpty()) {
         synchronized(this.jobs) {
            while(!this.jobs.isEmpty()) {
               ((SimulationJob)this.jobs.dequeue()).execute(this);
            }
         }
      }

      long var2 = var1.currentTime;
      this.runningTime += var2 - this.lastupdateTime;
      long var4 = this.runningTime / 30L;
      this.runningTime -= var4 * 30L;
      this.lastupdateTime = var2;
      if (var4 > 0L) {
         for(int var8 = 0; var8 < this.simulationGroups.size(); ++var8) {
            if (((SimulationGroup)this.simulationGroups.get(var8)).getMembers().isEmpty()) {
               synchronized(this.simulationGroups) {
                  System.err.println("[SIMULATION] Removing Sim Group for lack of members");
                  this.simulationGroups.remove(var8);
                  --var8;
               }
            } else {
               ((SimulationGroup)this.simulationGroups.get(var8)).updateTicks(var4);
            }
         }
      }

   }

   public void writeToDatabase() {
      synchronized(this.simulationGroups) {
         for(int var2 = 0; var2 < this.simulationGroups.size(); ++var2) {
            ((SimulationGroup)this.simulationGroups.get(var2)).writeToDatabase();
         }

      }
   }

   public void print(RegisteredClientInterface var1) throws IOException {
      var1.serverMessage("-------SIMULATION INFO START----------");
      var1.serverMessage("Current Total Groups: " + this.simulationGroups.size());
      synchronized(this.simulationGroups) {
         for(int var3 = 0; var3 < this.simulationGroups.size(); ++var3) {
            ((SimulationGroup)this.simulationGroups.get(var3)).print(var1);
         }

      }
   }

   public void clearAll() {
      synchronized(this.simulationGroups) {
         for(int var2 = 0; var2 < this.simulationGroups.size(); ++var2) {
            ((SimulationGroup)this.simulationGroups.get(var2)).despawn();
         }

      }
   }

   public void shutdown() {
      if (this.planner != null) {
         this.planner.shutdown();
      }

   }
}
