package org.schema.game.server.data.simulation.npc.news;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.server.data.FactionState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.resource.tag.SerializableTagElement;

public abstract class NPCFactionNewsEvent implements SerialializationInterface, SerializableTagElement {
   public long time;
   public int factionId;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeLong(this.time);
      var1.writeInt(this.factionId);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.time = var1.readLong();
      this.factionId = var1.readInt();
   }

   public byte getFactoryId() {
      return 2;
   }

   public void writeToTag(DataOutput var1) throws IOException {
      var1.writeByte(this.getType().ordinal());
      this.serialize(var1, true);
   }

   public String getOwnName(FactionState var1) {
      Faction var2;
      return (var2 = var1.getFactionManager().getFaction(this.factionId)) != null ? var2.getName() : Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_NEWS_NPCFACTIONNEWSEVENT_0;
   }

   public abstract NPCFactionNews.NPCFactionNewsEventType getType();

   public abstract String getMessage(FactionState var1);
}
