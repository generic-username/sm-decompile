package org.schema.game.server.data.simulation.npc.news;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;

public abstract class NPCFactionNewsEventSystem extends NPCFactionNewsEvent {
   public Vector3i system = new Vector3i();

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      super.serialize(var1, var2);
      this.system.serialize(var1);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.system.deserialize(var1);
   }
}
