package org.schema.game.server.data.simulation.npc.diplomacy;

import it.unimi.dsi.fastutil.bytes.Byte2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2BooleanOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import org.schema.common.util.LogInterface;
import org.schema.common.util.StringTools;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.player.faction.FactionRelationOffer;
import org.schema.game.common.data.player.faction.FactionRelationOfferAcceptOrDecline;
import org.schema.game.common.data.player.faction.PersonalEnemyMod;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.game.server.data.simulation.npc.NPCFactionConfig;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;
import org.schema.schine.network.server.ServerStateInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class NPCDiplomacyEntity implements LogInterface {
   private int points;
   private int pointCached;
   private int fid;
   private long dbId;
   private final Byte2ObjectOpenHashMap actions;
   private final FactionState state;
   private NPCDiplomacy dCache;
   private boolean changed;
   private final Object2ObjectOpenHashMap dynamicMap;
   private final Object2ObjectOpenHashMap staticMap;
   private Int2BooleanOpenHashMap reactionsFired;

   public NPCDiplomacyEntity(FactionState var1, int var2, long var3) {
      this(var1, var2);
      this.dbId = var3;
   }

   public NPCDiplomacyEntity(FactionState var1, int var2) {
      this.actions = new Byte2ObjectOpenHashMap();
      this.dynamicMap = new Object2ObjectOpenHashMap() {
         private static final long serialVersionUID = 1L;

         public NPCDiplTurnModifier remove(Object var1) {
            if (var1 != null && !(var1 instanceof DiplomacyAction.DiplActionType)) {
               throw new IllegalArgumentException();
            } else {
               return (NPCDiplTurnModifier)super.remove(var1);
            }
         }

         public NPCDiplTurnModifier get(Object var1) {
            if (var1 != null && !(var1 instanceof DiplomacyAction.DiplActionType)) {
               throw new IllegalArgumentException();
            } else {
               return (NPCDiplTurnModifier)super.get(var1);
            }
         }

         public boolean containsKey(Object var1) {
            if (var1 != null && !(var1 instanceof DiplomacyAction.DiplActionType)) {
               throw new IllegalArgumentException();
            } else {
               return super.containsKey(var1);
            }
         }
      };
      this.staticMap = new Object2ObjectOpenHashMap() {
         private static final long serialVersionUID = 1L;

         public NPCDiplStaticModifier remove(Object var1) {
            if (var1 != null && !(var1 instanceof NPCDiplomacyEntity.DiplStatusType)) {
               throw new IllegalArgumentException();
            } else {
               return (NPCDiplStaticModifier)super.remove(var1);
            }
         }

         public NPCDiplStaticModifier get(Object var1) {
            if (var1 != null && !(var1 instanceof NPCDiplomacyEntity.DiplStatusType)) {
               throw new IllegalArgumentException();
            } else {
               return (NPCDiplStaticModifier)super.get(var1);
            }
         }

         public boolean containsKey(Object var1) {
            if (var1 != null && !(var1 instanceof NPCDiplomacyEntity.DiplStatusType)) {
               throw new IllegalArgumentException();
            } else {
               return super.containsKey(var1);
            }
         }
      };
      this.reactionsFired = new Int2BooleanOpenHashMap();
      this.fid = var2;
      this.state = var1;
   }

   public boolean isSinglePlayer() {
      return this.dbId >= 2147483647L;
   }

   public boolean isFaction() {
      return this.dbId < 2147483647L;
   }

   public NPCDiplomacy getDiplomacy() {
      if (this.dCache == null) {
         this.dCache = ((NPCFaction)this.state.getFactionManager().getFaction(this.fid)).getDiplomacy();
      }

      return this.dCache;
   }

   public boolean isFactionLoaded() {
      return (NPCFaction)this.state.getFactionManager().getFaction(this.fid) != null;
   }

   public void setChanged() {
      this.changed = true;
   }

   public int getPoints() {
      if (!this.getFaction().isOnServer()) {
         return this.pointCached;
      } else {
         if (this.changed) {
            this.pointCached = this.recalcPoints();
            this.setNTChanged();
            this.changed = false;
         }

         return this.pointCached;
      }
   }

   public int getRawPoints() {
      return this.points;
   }

   private void setNTChanged() {
      if (this.state instanceof ServerStateInterface) {
         this.getDiplomacy().ntChanged(this.dbId);
      }

   }

   private int recalcPoints() {
      int var1 = this.points;

      NPCDiplStaticModifier var3;
      for(Iterator var2 = this.staticMap.values().iterator(); var2.hasNext(); var1 += var3.value) {
         var3 = (NPCDiplStaticModifier)var2.next();
      }

      return Math.max(this.getConfig().diplomacyMinPoints, Math.min(this.getConfig().diplomacyMaxPoints, var1));
   }

   public void calculateStaticModifiers(long var1) {
      NPCDiplomacyEntity.DiplStatusType[] var3;
      int var4 = (var3 = NPCDiplomacyEntity.DiplStatusType.values()).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         NPCDiplomacyEntity.DiplStatusType var6 = var3[var5];
         if (this.getConfig().existsStatus(var6)) {
            this.calculateStaticModifier(var1, this.staticMap, var6);
         }
      }

      Iterator var7 = this.staticMap.values().iterator();

      while(var7.hasNext()) {
         NPCDiplStaticModifier var8 = (NPCDiplStaticModifier)var7.next();
         int[] var10000 = null.$SwitchMap$org$schema$game$server$data$simulation$npc$diplomacy$NPCDiplomacyEntity$DiplStatusType;
         var8.type.ordinal();
      }

      this.checkReactions();
      this.setChanged();
      this.setNTChanged();
   }

   private void checkReactions() {
      Iterator var1 = this.getConfig().getDiplomacyReactions().iterator();

      while(var1.hasNext()) {
         DiplomacyReaction var2;
         if ((var2 = (DiplomacyReaction)var1.next()).isSatisfied(this)) {
            if (this.executeReaction(var2)) {
               this.reactionsFired.put(var2.index, true);
            } else {
               this.reactionsFired.remove(var2.index);
            }
         } else {
            this.reactionsFired.remove(var2.index);
         }
      }

   }

   private boolean executeReaction(DiplomacyReaction var1) {
      FactionRelationOffer var3;
      Iterator var6;
      FactionRelationOffer var7;
      FactionRelationOfferAcceptOrDecline var9;
      switch(var1.reaction) {
      case ACCEPT_ALLIANCE_OFFER:
         if (this.isFaction()) {
            var6 = ((GameServerState)this.getFaction().getState()).getFactionManager().getRelationShipOffers().values().iterator();

            while(var6.hasNext()) {
               if ((var3 = (FactionRelationOffer)var6.next()).rel == FactionRelation.RType.FRIEND.code && var3.b == this.getFaction().getIdFaction()) {
                  var9 = new FactionRelationOfferAcceptOrDecline("ADMIN", var3.getCode(), true);
                  this.state.getFactionManager().getToAddFactionRelationOfferAccepts().add(var9);
                  this.log("Faction accepted alliance offer", LogInterface.LogLevel.NORMAL);
                  return true;
               }
            }
         }

         return false;
      case REJECT_ALLIANCE_OFFER:
         if (this.isFaction()) {
            var6 = ((GameServerState)this.getFaction().getState()).getFactionManager().getRelationShipOffers().values().iterator();

            while(var6.hasNext()) {
               if ((var3 = (FactionRelationOffer)var6.next()).rel == FactionRelation.RType.FRIEND.code && var3.b == this.getFaction().getIdFaction()) {
                  var9 = new FactionRelationOfferAcceptOrDecline("ADMIN", var3.getCode(), false);
                  this.state.getFactionManager().getToAddFactionRelationOfferAccepts().add(var9);
                  this.log("Faction rejected alliance offer", LogInterface.LogLevel.NORMAL);
                  return true;
               }
            }
         }

         return false;
      case ACCEPT_PEACE_OFFER:
         if (this.isFaction()) {
            var6 = ((GameServerState)this.getFaction().getState()).getFactionManager().getRelationShipOffers().values().iterator();

            while(var6.hasNext()) {
               if ((var3 = (FactionRelationOffer)var6.next()).rel == FactionRelation.RType.NEUTRAL.code && var3.b == this.getFaction().getIdFaction()) {
                  var9 = new FactionRelationOfferAcceptOrDecline("ADMIN", var3.getCode(), true);
                  this.state.getFactionManager().getToAddFactionRelationOfferAccepts().add(var9);
                  this.log("Faction accepted peace offer", LogInterface.LogLevel.NORMAL);
                  return true;
               }
            }
         }

         return true;
      case REJECT_PEACE_OFFER:
         if (this.isFaction()) {
            var6 = ((GameServerState)this.getFaction().getState()).getFactionManager().getRelationShipOffers().values().iterator();

            while(var6.hasNext()) {
               if ((var3 = (FactionRelationOffer)var6.next()).rel == FactionRelation.RType.FRIEND.code && var3.b == this.getFaction().getIdFaction()) {
                  var9 = new FactionRelationOfferAcceptOrDecline("ADMIN", var3.getCode(), false);
                  this.state.getFactionManager().getToAddFactionRelationOfferAccepts().add(var9);
                  this.log("Faction rejected peace offer", LogInterface.LogLevel.NORMAL);
                  return true;
               }
            }
         }

         return false;
      case DECLARE_WAR:
         if (this.getFaction().getRelationshipWithFactionOrPlayer(this.dbId) != FactionRelation.RType.ENEMY) {
            this.getFaction().declareWarAgainstEntity(this.dbId);
            return true;
         }

         return false;
      case OFFER_ALLIANCE:
         if (this.isFaction()) {
            var6 = ((GameServerState)this.getFaction().getState()).getFactionManager().getRelationShipOffers().values().iterator();

            while(var6.hasNext()) {
               if ((var3 = (FactionRelationOffer)var6.next()).rel == FactionRelation.RType.FRIEND.code && var3.b == this.getFaction().getIdFaction()) {
                  return false;
               }
            }

            if (this.getFaction().getRelationshipWithFactionOrPlayer(this.dbId) != FactionRelation.RType.ENEMY && this.getFaction().getRelationshipWithFactionOrPlayer(this.dbId) != FactionRelation.RType.FRIEND) {
               var6 = ((GameServerState)this.getFaction().getState()).getFactionManager().getRelationShipOffers().values().iterator();

               while(var6.hasNext()) {
                  if ((var3 = (FactionRelationOffer)var6.next()).rel == FactionRelation.RType.FRIEND.code && var3.b == this.getFaction().getIdFaction()) {
                     return false;
                  }
               }

               (var7 = new FactionRelationOffer()).set("ADMIN", this.getFaction().getIdFaction(), (int)this.dbId, FactionRelation.RType.NEUTRAL.code, "Since you haven't been attacking for some time, we wanted to make peace", false);
               if ((var3 = (FactionRelationOffer)this.state.getFactionManager().getRelationShipOffers().get(var7.getCode())) == null || !var3.isNeutral()) {
                  this.state.getFactionManager().getRelationOffersToAdd().add(var7);
               }

               return true;
            }
         }

         return false;
      case OFFER_PEACE_DEAL:
         if (this.isFaction()) {
            var6 = ((GameServerState)this.getFaction().getState()).getFactionManager().getRelationShipOffers().values().iterator();

            while(var6.hasNext()) {
               if ((var3 = (FactionRelationOffer)var6.next()).rel == FactionRelation.RType.NEUTRAL.code && var3.b == this.getFaction().getIdFaction()) {
                  return false;
               }
            }
         }

         if (this.getFaction().getRelationshipWithFactionOrPlayer(this.dbId) == FactionRelation.RType.ENEMY) {
            if (this.isSinglePlayer()) {
               String var8 = ((GameServerState)this.state).getPlayerNameFromDbIdLowerCase(this.dbId);
               PersonalEnemyMod var5 = new PersonalEnemyMod("npc", var8, this.getFaction().getIdFaction(), false);
               if (!this.state.getFactionManager().getToModPersonalEnemies().contains(var5)) {
                  this.state.getFactionManager().getToModPersonalEnemies().add(var5);
                  ((GameServerState)this.state).getServerPlayerMessager().send(this.getFaction().getName(), var8, Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_NPCDIPLOMACYENTITY_0, Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_NPCDIPLOMACYENTITY_1);
               }
            } else if (this.isFaction()) {
               (var7 = new FactionRelationOffer()).set("ADMIN", this.getFaction().getIdFaction(), (int)this.dbId, FactionRelation.RType.NEUTRAL.code, "Since you haven't been attacking for some time, we wanted to make peace", false);
               if (((var3 = (FactionRelationOffer)this.state.getFactionManager().getRelationShipOffers().get(var7.getCode())) == null || !var3.isNeutral()) && !this.state.getFactionManager().getRelationOffersToAdd().contains(var7)) {
                  this.state.getFactionManager().getRelationOffersToAdd().add(var7);
               }
            }
         }

         return true;
      case REMOVE_ALLIANCE_OFFER:
         if (this.isFaction()) {
            var6 = ((GameServerState)this.getFaction().getState()).getFactionManager().getRelationShipOffers().values().iterator();

            while(var6.hasNext()) {
               if ((var3 = (FactionRelationOffer)var6.next()).rel == FactionRelation.RType.FRIEND.code && var3.b == this.getFaction().getIdFaction()) {
                  (var7 = new FactionRelationOffer()).set("ADMIN", var3.a, var3.b, var3.rel, "We revoke our offer!", true);
                  ((GameServerState)this.getFaction().getState()).getFactionManager().relationShipOfferServer(var7);
                  return true;
               }
            }
         }

         return false;
      case REMOVE_PEACE_DEAL_OFFER:
         if (this.isFaction()) {
            var6 = ((GameServerState)this.getFaction().getState()).getFactionManager().getRelationShipOffers().values().iterator();

            while(var6.hasNext()) {
               if ((var3 = (FactionRelationOffer)var6.next()).rel == FactionRelation.RType.NEUTRAL.code && var3.b == this.getFaction().getIdFaction()) {
                  (var7 = new FactionRelationOffer()).set("ADMIN", var3.a, var3.b, var3.rel, "We revoke our offer!", true);
                  ((GameServerState)this.getFaction().getState()).getFactionManager().relationShipOfferServer(var7);
                  return true;
               }
            }
         }

         return false;
      case SEND_POPUP_MESSAGE:
         if (!this.reactionsFired.containsKey(var1.index)) {
            if (this.isSinglePlayer()) {
               PlayerState var2;
               if ((var2 = (PlayerState)((GameServerState)this.state).getPlayerStatesByDbId().get(this.getDbId())) != null) {
                  var2.sendServerMessagePlayerError(new Object[]{481, var1.message});
               }
            } else {
               Faction var4;
               if ((int)this.getDbId() > 0 && (var4 = this.state.getFactionManager().getFaction((int)this.getDbId())) != null && var1.message != null) {
                  var4.broadcastMessage(new Object[]{482, var1.message}, 3, (GameServerState)this.state);
               }
            }
         }

         return true;
      default:
         return false;
      }
   }

   public void applyDynamicModifiers(long var1) {
      int var4 = this.getPoints();
      Iterator var2 = this.dynamicMap.values().iterator();

      while(var2.hasNext()) {
         NPCDiplTurnModifier var3 = (NPCDiplTurnModifier)var2.next();
         this.modPoints(var3.pointsPerTurn);
      }

      this.setChanged();
      if (var4 != this.getPoints()) {
         this.getPoints();
         this.setNTChanged();
      }

   }

   private void onPointsChanged(int var1, int var2) {
      this.setNTChanged();
   }

   public boolean existsStatusModifier(NPCDiplomacyEntity.DiplStatusType var1) {
      NPCDiplStaticModifier var2;
      return (var2 = (NPCDiplStaticModifier)this.staticMap.get(var1)) != null && var2.value != 0;
   }

   public long persistedStatusModifier(NPCDiplomacyEntity.DiplStatusType var1) {
      NPCDiplStaticModifier var2;
      return (var2 = (NPCDiplStaticModifier)this.staticMap.get(var1)) == null ? -1L : var2.totalTimeApplied;
   }

   public long persistedActionModifier(DiplomacyAction.DiplActionType var1) {
      NPCDiplTurnModifier var2;
      return (var2 = (NPCDiplTurnModifier)this.dynamicMap.get(var1)) == null ? -1L : var2.totalElapsedTime;
   }

   private void calculateStaticModifier(long var1, Object2ObjectOpenHashMap var3, NPCDiplomacyEntity.DiplStatusType var4) {
      float var5 = this.calculateStatus(var4);
      int var6 = this.getConfig().getDiplomacyValue(var4);
      NPCDiplStaticModifier var7 = (NPCDiplStaticModifier)var3.get(var4);
      if (var5 > 0.0F) {
         if (var7 == null) {
            (var7 = new NPCDiplStaticModifier()).type = var4;
            var7.value = (int)Math.ceil((double)((float)var6 * var5));
            var7.totalTimeApplied = 0L;
            var3.put(var4, var7);
         } else {
            var7.elapsedTimeInactive = 0L;
            var7.totalTimeApplied += var1;
         }
      } else if (var7 != null) {
         var7.elapsedTimeInactive += var1;
         if (var7.type == NPCDiplomacyEntity.DiplStatusType.NON_AGGRESSION || var7.elapsedTimeInactive > this.getConfig().getDiplomacyStaticTimpout(var4)) {
            var3.remove(var4);
         }
      }

      if (var7 != null) {
         this.log("Faction static modifier: " + var7.type.name() + "; Value: " + var6, LogInterface.LogLevel.DEBUG);
      }

   }

   public float getStatusPoints(NPCDiplomacyEntity.DiplStatusType var1) {
      NPCDiplStaticModifier var2;
      return (var2 = (NPCDiplStaticModifier)this.staticMap.get(var1)) == null ? -1.0F : (float)var2.value;
   }

   public float calculateStatus(NPCDiplomacyEntity.DiplStatusType var1) {
      FactionRelation.RType var2 = this.getDiplomacy().faction.getRelationshipWithFactionOrPlayer(this.dbId);
      List var3;
      int var4;
      Faction var5;
      Iterator var9;
      switch(var1) {
      case ALLIANCE:
         if (var2 == FactionRelation.RType.FRIEND) {
            return 1.0F;
         }

         return 0.0F;
      case ALLIANCE_WITH_ENEMY:
         if (this.isFaction()) {
            List var11 = this.getFaction().getEnemies();
            int var14 = 0;
            Iterator var13 = var11.iterator();

            while(var13.hasNext()) {
               Faction var15;
               if ((long)(var15 = (Faction)var13.next()).getIdFaction() != this.dbId && var15.getRelationshipWithFactionOrPlayer(this.dbId) == FactionRelation.RType.FRIEND) {
                  ++var14;
               }
            }

            return (float)var14;
         }

         return 0.0F;
      case ALLIANCE_WITH_FRIENDS:
         if (!this.isFaction()) {
            return 0.0F;
         } else {
            var3 = this.getFaction().getFriends();
            var4 = 0;
            var9 = var3.iterator();

            while(true) {
               do {
                  if (!var9.hasNext()) {
                     return (float)var4;
                  }
               } while(!(var5 = (Faction)var9.next()).isNPC() && !var5.isPlayerFaction());

               if ((long)var5.getIdFaction() != this.dbId && var5.getRelationshipWithFactionOrPlayer(this.dbId) == FactionRelation.RType.FRIEND) {
                  ++var4;
               }
            }
         }
      case CLOSE_TERRITORY:
         DiplomacyAction var7;
         if ((var7 = (DiplomacyAction)this.actions.get((byte)DiplomacyAction.DiplActionType.TERRITORY.ordinal())) != null && var7.counter > 0) {
            return 1.0F;
         }

         return 0.0F;
      case IN_WAR:
         if (var2 == FactionRelation.RType.ENEMY) {
            return 1.0F;
         }

         return 0.0F;
      case IN_WAR_WITH_ENEMY:
         List var10 = this.getFaction().getEnemies();
         int var6 = 0;
         Iterator var12 = var10.iterator();

         while(true) {
            Faction var16;
            do {
               if (!var12.hasNext()) {
                  return (float)var6;
               }
            } while(!(var16 = (Faction)var12.next()).isNPC() && !var16.isPlayerFaction());

            if ((long)var16.getIdFaction() != this.dbId && var16.getRelationshipWithFactionOrPlayer(this.dbId) == FactionRelation.RType.ENEMY) {
               ++var6;
            }
         }
      case IN_WAR_WITH_FRIENDS:
         var3 = this.getFaction().getFriends();
         var4 = 0;
         var9 = var3.iterator();

         while(true) {
            do {
               if (!var9.hasNext()) {
                  return (float)var4;
               }
            } while(!(var5 = (Faction)var9.next()).isNPC() && !var5.isPlayerFaction());

            if ((long)var5.getIdFaction() != this.dbId && var5.getRelationshipWithFactionOrPlayer(this.dbId) == FactionRelation.RType.ENEMY) {
               ++var4;
            }
         }
      case NON_AGGRESSION:
         DiplomacyAction var8;
         if ((var8 = (DiplomacyAction)this.actions.get((byte)DiplomacyAction.DiplActionType.ATTACK.ordinal())) != null && var8.counter > 0) {
            return 0.0F;
         }

         return 1.0F;
      case PIRATE:
         return 1.0F;
      case POWER:
         return 0.0F;
      default:
         return 0.0F;
      }
   }

   private NPCFaction getFaction() {
      return this.getDiplomacy().faction;
   }

   private void calculateDynamicModifier(long var1, Object2ObjectOpenHashMap var3, DiplomacyAction.DiplActionType var4) {
      int var5 = this.getConfig().getDiplomacyUpperLimit(var4);
      int var6 = this.getConfig().getDiplomacyLowerLimit(var4);
      if (var5 != var6 || var6 != 0) {
         int var7;
         int var8 = (var7 = var5 > var6 ? 1 : -1) > 0 ? var5 : var6;
         int var9 = var7 > 0 ? var6 : var5;
         long var10 = this.getConfig().getDiplomacyTurnTimeout(var4);
         if (Math.abs(var8 - var9) > 0) {
            var10 = 0L;
         }

         int var12 = this.getConfig().getDiplomacyValue(var4);
         int var13 = this.getActionCount(var4);
         NPCDiplTurnModifier var14 = (NPCDiplTurnModifier)var3.get(var4);
         long var15 = this.getConfig().diplomacyTurnEffectChangeDelay;
         int var2;
         int var17;
         if (var13 > 0) {
            if (var14 == null) {
               (var14 = new NPCDiplTurnModifier()).type = var4;
               var14.pointsPerTurn = var12;
               var3.put(var4, var14);
            } else {
               var14.elapsedTime += var1;
               var14.totalElapsedTime += var1;
               if (var14.elapsedTime > var15) {
                  var17 = var14.pointsPerTurn;
                  var2 = var7 * this.getConfig().getDiplomacyExistingActionModifier(var4);
                  var14.pointsPerTurn = Math.max(var9, Math.min(var8, var14.pointsPerTurn + var2));
                  this.log("PERSISTING ACTION " + var17 + " -> " + var14.pointsPerTurn + " [" + var9 + ", " + var8 + "] mod: " + var2, LogInterface.LogLevel.DEBUG);
                  var14.elapsedTime -= var15;
               } else {
                  this.log("PERSISTING ACTION not triggering yet " + var14.elapsedTime + "/" + var15 + " -> " + var14.pointsPerTurn, LogInterface.LogLevel.DEBUG);
               }
            }
         } else if (var14 != null) {
            var14.elapsedTime += var1;
            var14.totalElapsedTime += var1;
            if (var14.elapsedTime > var15) {
               var17 = var14.pointsPerTurn;
               var2 = var7 * this.getConfig().getDiplomacyNonExistingActionModifier(var4);
               var14.pointsPerTurn = Math.max(var9, Math.min(var8, var14.pointsPerTurn - var2));
               var14.elapsedTime -= var15;
               this.log("NON PERSISTING ACTION " + var17 + " -> " + var14.pointsPerTurn + " [" + var9 + ", " + var8 + "] mod: " + var2 + ";", LogInterface.LogLevel.DEBUG);
               if (var10 <= 0L && (var7 > 0 && var14.pointsPerTurn == var5 || var7 < 0 && var14.pointsPerTurn == var6) || var10 > 0L && var14.totalElapsedTime > var10) {
                  if (var10 > 0L && var14.totalElapsedTime > var10) {
                     this.log("ACTION TIMEOUT! REMOVING; " + var14.totalElapsedTime + " / " + var10, LogInterface.LogLevel.DEBUG);
                  } else {
                     this.log("LIMIT REACHED! REMOVING; ppt " + var14.pointsPerTurn + " [" + var6 + ", " + var5 + "]", LogInterface.LogLevel.DEBUG);
                  }

                  var3.remove(var4);
               }
            }
         }

         this.checkReactions();
      }
   }

   public NPCFactionConfig getConfig() {
      return this.getDiplomacy().faction.getConfig();
   }

   void calculateDiplomacyModifiersFromActions(long var1) {
      ObjectIterator var3 = this.actions.values().iterator();

      while(var3.hasNext()) {
         DiplomacyAction var4 = (DiplomacyAction)var3.next();
         if (this.getConfig().existsAction(var4.type)) {
            DiplomacyReaction var5;
            if ((var5 = this.getConfig().getDiplomacyActionRequired(var4.type)) != null) {
               if (var5.isSatisfied(this)) {
                  this.executeReaction(var5);
               }
            } else {
               this.calculateDynamicModifier(0L, this.dynamicMap, var4.type);
            }
         }

         this.log("ACTION TIMEOUT: " + var4.type.name() + " " + var4.timeDuration + " - " + var1 + " = " + (var4.timeDuration - var1), LogInterface.LogLevel.DEBUG);
         var4.timeDuration -= var1;
         if (var4.timeDuration <= 0L) {
            var3.remove();
         }
      }

      Iterator var6 = this.dynamicMap.values().iterator();

      while(var6.hasNext()) {
         NPCDiplTurnModifier var7 = (NPCDiplTurnModifier)var6.next();
         if (this.getConfig().existsAction(var7.type)) {
            this.calculateDynamicModifier(var1, this.dynamicMap, var7.type);
         }
      }

      this.setNTChanged();
   }

   public int getActionCount(DiplomacyAction.DiplActionType var1) {
      DiplomacyAction var2;
      return (var2 = (DiplomacyAction)this.actions.get((byte)var1.ordinal())) == null ? 0 : var2.counter;
   }

   public void diplomacyAction(DiplomacyAction.DiplActionType var1) {
      DiplomacyAction var2;
      if ((var2 = (DiplomacyAction)this.actions.get((byte)var1.ordinal())) == null) {
         (var2 = new DiplomacyAction()).type = var1;
         this.actions.put((byte)var1.ordinal(), var2);
      }

      this.log("Faction action modifier triggered: " + var2.type.name(), LogInterface.LogLevel.DEBUG);
      FactionRelationOffer var3;
      if (this.isFaction() && (var2.type == DiplomacyAction.DiplActionType.ATTACK || var2.type == DiplomacyAction.DiplActionType.DECLARATION_OF_WAR) && (var3 = (FactionRelationOffer)this.state.getFactionManager().getRelationShipOffers().get(FactionRelationOffer.getCode(this.getFaction().getIdFaction(), (int)this.dbId))) != null) {
         FactionRelationOffer var4;
         (var4 = new FactionRelationOffer()).set("npc", this.getFaction().getIdFaction(), (int)this.dbId, (byte)var3.getRelation().ordinal(), "msg", true);
         this.state.getFactionManager().getRelationOffersToAdd().add(var4);
         this.log("Faction revoked " + var3.getRelation().name() + " offer because of aggresion", LogInterface.LogLevel.NORMAL);
      }

      var2.timeDuration = this.getConfig().getDiplomacyActionTimeout(var1);
      ++var2.counter;
      this.setChanged();
   }

   public void log(String var1, LogInterface.LogLevel var2) {
      this.getDiplomacy().log("[TO_" + this.dbId + "]" + var1, var2);
   }

   public void setPoints(int var1) {
      this.points = Math.max(this.getConfig().diplomacyMinPoints, Math.min(this.getConfig().diplomacyMaxPoints, var1));
   }

   public long getDbId() {
      return this.dbId;
   }

   public void setDbId(long var1) {
      this.dbId = var1;
   }

   public Tag toTag() {
      this.getPoints();
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.LONG, (String)null, this.dbId), new Tag(Tag.Type.INT, (String)null, this.fid), new Tag(Tag.Type.INT, (String)null, this.points), new Tag(Tag.Type.INT, (String)null, this.pointCached), this.getActionsTag(), this.getDynamicMapTag(), this.getStaticMapTag(), this.getReactionsFiredMapTag(), FinishTag.INST});
   }

   public void fromTag(Tag var1) {
      Tag[] var2;
      (var2 = var1.getStruct())[0].getByte();
      this.dbId = var2[1].getLong();
      this.fid = var2[2].getInt();
      this.points = var2[3].getInt();
      this.pointCached = var2[4].getInt();
      this.fromActionsTag(var2[5]);
      this.fromDynamicMapTag(var2[6]);
      this.fromStaticMapTag(var2[7]);
      this.fromReactionsFiredMap(var2[8]);
   }

   private void fromStaticMapTag(Tag var1) {
      Tag[] var6;
      if ((var6 = var1.getStruct()).length - 1 > 300) {
         System.err.println("[SERVER][FACTION][NPC][DIPLOMACY] FID: " + this.fid + "; StaticMap: " + (var6.length - 1));
      }

      for(int var2 = 0; var2 < var6.length - 1; ++var2) {
         Tag[] var3;
         byte var4 = (var3 = var6[var2].getStruct())[0].getByte();
         NPCDiplStaticModifier var5;
         (var5 = new NPCDiplStaticModifier()).fromTag(var3[1]);
         this.staticMap.put(NPCDiplomacyEntity.DiplStatusType.values()[var4], var5);
      }

   }

   private void fromReactionsFiredMap(Tag var1) {
      Tag[] var5;
      if ((var5 = var1.getStruct()).length - 1 > 300) {
         System.err.println("[SERVER][FACTION][NPC][DIPLOMACY] FID: " + this.fid + "; ReActions: " + (var5.length - 1));
      }

      for(int var2 = 0; var2 < var5.length - 1; ++var2) {
         Tag[] var3;
         short var4 = (var3 = var5[var2].getStruct())[0].getShort();
         boolean var6 = var3[1].getByte() != 0;
         this.reactionsFired.put(var4, var6);
      }

   }

   private void fromDynamicMapTag(Tag var1) {
      Tag[] var6;
      if ((var6 = var1.getStruct()).length - 1 > 300) {
         System.err.println("[SERVER][FACTION][NPC][DIPLOMACY] FID: " + this.fid + "; DynMap: " + (var6.length - 1));
      }

      for(int var2 = 0; var2 < var6.length - 1; ++var2) {
         Tag[] var3;
         byte var4 = (var3 = var6[var2].getStruct())[0].getByte();
         NPCDiplTurnModifier var5;
         (var5 = new NPCDiplTurnModifier()).fromTag(var3[1]);
         this.dynamicMap.put(DiplomacyAction.DiplActionType.values()[var4], var5);
      }

   }

   private void fromActionsTag(Tag var1) {
      Tag[] var6;
      if ((var6 = var1.getStruct()).length - 1 > 300) {
         System.err.println("[SERVER][FACTION][NPC][DIPLOMACY] FID: " + this.fid + "; Actions: " + (var6.length - 1));
      }

      for(int var2 = 0; var2 < var6.length - 1; ++var2) {
         Tag[] var3;
         byte var4 = (var3 = var6[var2].getStruct())[0].getByte();
         DiplomacyAction var5;
         (var5 = new DiplomacyAction()).fromTag(var3[1]);
         this.actions.put(var4, var5);
      }

   }

   private Tag getActionsTag() {
      Tag[] var1;
      Tag[] var10000 = var1 = new Tag[this.actions.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;
      int var2 = 0;

      Entry var4;
      for(Iterator var3 = this.actions.entrySet().iterator(); var3.hasNext(); var1[var2++] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, var4.getKey()), ((DiplomacyAction)var4.getValue()).toTag(), FinishTag.INST})) {
         var4 = (Entry)var3.next();
      }

      return new Tag(Tag.Type.STRUCT, (String)null, var1);
   }

   private Tag getStaticMapTag() {
      Tag[] var1;
      Tag[] var10000 = var1 = new Tag[this.staticMap.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;
      int var2 = 0;

      Entry var4;
      for(Iterator var3 = this.staticMap.entrySet().iterator(); var3.hasNext(); var1[var2++] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)((NPCDiplomacyEntity.DiplStatusType)var4.getKey()).ordinal()), ((NPCDiplStaticModifier)var4.getValue()).toTag(), FinishTag.INST})) {
         var4 = (Entry)var3.next();
      }

      return new Tag(Tag.Type.STRUCT, (String)null, var1);
   }

   private Tag getReactionsFiredMapTag() {
      Tag[] var1;
      Tag[] var10000 = var1 = new Tag[this.reactionsFired.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;
      int var2 = 0;

      Entry var4;
      for(Iterator var3 = this.reactionsFired.entrySet().iterator(); var3.hasNext(); var1[var2++] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.SHORT, (String)null, ((Integer)var4.getKey()).shortValue()), new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)((Boolean)var4.getValue() ? 1 : 0))), FinishTag.INST})) {
         var4 = (Entry)var3.next();
      }

      return new Tag(Tag.Type.STRUCT, (String)null, var1);
   }

   private Tag getDynamicMapTag() {
      Tag[] var1;
      Tag[] var10000 = var1 = new Tag[this.dynamicMap.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;
      int var2 = 0;

      Entry var4;
      for(Iterator var3 = this.dynamicMap.entrySet().iterator(); var3.hasNext(); var1[var2++] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)((DiplomacyAction.DiplActionType)var4.getKey()).ordinal()), ((NPCDiplTurnModifier)var4.getValue()).toTag(), FinishTag.INST})) {
         var4 = (Entry)var3.next();
      }

      return new Tag(Tag.Type.STRUCT, (String)null, var1);
   }

   public void deserialize(DataInputStream var1, int var2, boolean var3) throws IOException {
      this.actions.clear();
      this.dynamicMap.clear();
      this.staticMap.clear();
      Tag var4 = Tag.deserializeNT(var1);
      this.fromTag(var4);
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      this.toTag().serializeNT(var1);
   }

   public String toString() {
      StringBuffer var1;
      (var1 = new StringBuffer()).append("ENTITY: " + this.dbId + "\n");
      var1.append("Points: " + this.pointCached + " (Raw: " + this.points + ")\n");
      var1.append("Actions: \n");
      Iterator var2 = this.actions.values().iterator();

      while(var2.hasNext()) {
         DiplomacyAction var3 = (DiplomacyAction)var2.next();
         var1.append("  " + var3.type.name() + "x" + var3.counter + "; Duration " + var3.timeDuration + "\n");
      }

      var1.append("Dynamic: \n");
      var2 = this.dynamicMap.values().iterator();

      while(var2.hasNext()) {
         NPCDiplTurnModifier var4 = (NPCDiplTurnModifier)var2.next();
         var1.append("  " + var4.type.name() + "; PPT: " + var4.pointsPerTurn + ";\n");
      }

      var1.append("Static: \n");
      var2 = this.staticMap.values().iterator();

      while(var2.hasNext()) {
         NPCDiplStaticModifier var5 = (NPCDiplStaticModifier)var2.next();
         var1.append("  " + var5.type.name() + "; MOD: " + var5.value + ";\n");
      }

      return var1.toString();
   }

   public Object2ObjectOpenHashMap getDynamicMap() {
      return this.dynamicMap;
   }

   public Object2ObjectOpenHashMap getStaticMap() {
      return this.staticMap;
   }

   public void triggerReactionManually(DiplomacyReaction var1) {
      this.log("Triggering reaction manually", LogInterface.LogLevel.NORMAL);
      this.executeReaction(var1);
   }

   public void modPoints(int var1) {
      this.getPoints();
      this.setPoints(this.getRawPoints() + var1);
      this.pointCached = this.recalcPoints();
      this.setNTChanged();
   }

   public static enum DiplStatusType {
      IN_WAR(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_NPCDIPLOMACYENTITY_2;
         }
      }),
      IN_WAR_WITH_ENEMY(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_NPCDIPLOMACYENTITY_3;
         }
      }),
      CLOSE_TERRITORY(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_NPCDIPLOMACYENTITY_4;
         }
      }),
      POWER(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_NPCDIPLOMACYENTITY_5;
         }
      }),
      PIRATE(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_NPCDIPLOMACYENTITY_6;
         }
      }),
      ALLIANCE(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_NPCDIPLOMACYENTITY_7;
         }
      }),
      ALLIANCE_WITH_ENEMY(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_NPCDIPLOMACYENTITY_8;
         }
      }),
      NON_AGGRESSION(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_NPCDIPLOMACYENTITY_9;
         }
      }),
      ALLIANCE_WITH_FRIENDS(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_NPCDIPLOMACYENTITY_10;
         }
      }),
      IN_WAR_WITH_FRIENDS(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_NPCDIPLOMACYENTITY_11;
         }
      }),
      FACTION_MEMBER_AT_WAR_WITH_US(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_NPCDIPLOMACYENTITY_12;
         }
      }),
      FACTION_MEMBER_WE_DONT_LIKE(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_NPCDIPLOMACYENTITY_13;
         }
      });

      private Translatable description;

      private DiplStatusType(Translatable var3) {
         this.description = var3;
      }

      public final String getDescription() {
         return this.description.getName(this);
      }

      public static String list() {
         return StringTools.listEnum(values());
      }
   }
}
