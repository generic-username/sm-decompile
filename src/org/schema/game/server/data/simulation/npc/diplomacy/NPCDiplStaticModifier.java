package org.schema.game.server.data.simulation.npc.diplomacy;

import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class NPCDiplStaticModifier extends NPCDiplModifier {
   public int value;
   public long elapsedTimeInactive;
   public NPCDiplomacyEntity.DiplStatusType type;
   public long totalTimeApplied;

   public Tag toTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.INT, (String)null, this.value), new Tag(Tag.Type.LONG, (String)null, this.elapsedTimeInactive), new Tag(Tag.Type.BYTE, (String)null, (byte)this.type.ordinal()), FinishTag.INST});
   }

   public void fromTag(Tag var1) {
      Tag[] var2;
      (var2 = var1.getStruct())[0].getByte();
      this.value = var2[1].getInt();
      this.elapsedTimeInactive = var2[2].getLong();
      this.type = NPCDiplomacyEntity.DiplStatusType.values()[var2[3].getByte()];
   }

   public String getName() {
      return this.type.getDescription();
   }

   public boolean isStatic() {
      return true;
   }

   public int getValue() {
      return this.value;
   }
}
