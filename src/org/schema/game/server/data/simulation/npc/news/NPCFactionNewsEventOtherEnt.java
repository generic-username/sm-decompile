package org.schema.game.server.data.simulation.npc.news;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.server.data.FactionState;

public abstract class NPCFactionNewsEventOtherEnt extends NPCFactionNewsEvent {
   public String otherEnt;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      super.serialize(var1, var2);
      var1.writeUTF(this.otherEnt);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.otherEnt = var1.readUTF();
   }

   public String getOtherName(FactionState var1) {
      return this.otherEnt;
   }
}
