package org.schema.game.server.data.simulation.npc;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import javax.vecmath.Vector3f;
import org.schema.common.util.LogInterface;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.FleetCommandTypes;
import org.schema.game.common.data.fleet.FleetManager;
import org.schema.game.common.data.fleet.FleetMember;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.network.objects.remote.FleetCommand;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.controller.EntityAlreadyExistsException;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.blueprint.ChildStats;
import org.schema.game.server.data.blueprint.SegmentControllerOutline;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.game.server.data.simulation.npc.geo.NPCSystem;
import org.schema.game.server.data.simulation.npc.geo.NPCSystemFleetManager;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class NPCFleetManager implements LogInterface {
   public static final String IDPREFIX_FLEET = "GNPCFLT#";
   public static final String IDPREFIX_OWNER = "GNPC#";
   private final NPCFaction faction;
   private final NPCFleetManager.NPCGlobalStats stats;
   private boolean loaded;
   public List fleets = new ObjectArrayList();

   public NPCFleetManager(NPCFaction var1) {
      this.faction = var1;
      this.stats = new NPCFleetManager.NPCGlobalStats();
   }

   public String getOwnerName() {
      return "GNPC#" + this.faction.getIdFaction();
   }

   public Tag toTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), FinishTag.INST});
   }

   public void fromTag(Tag var1) {
      var1.getStruct()[0].getByte();
   }

   private NPCFaction getFaction() {
      return this.faction;
   }

   public boolean isType(Fleet var1, NPCSystemFleetManager.FleetType var2) {
      return var1.getName().toLowerCase(Locale.ENGLISH).startsWith(this.getFleetPrefix(var2).toLowerCase(Locale.ENGLISH));
   }

   private String getFleetPrefix(NPCSystemFleetManager.FleetType var1) {
      return "GNPCFLT#" + var1.name() + "#";
   }

   public String getName(int var1, NPCSystemFleetManager.FleetType var2) {
      return this.getFleetPrefix(var2) + this.faction.getIdFaction() + "#" + var1;
   }

   public FleetManager getFleetManager() {
      return this.getState().getFleetManager();
   }

   public GameServerState getState() {
      return this.faction.structure.getState();
   }

   private void load() {
      if (!this.loaded) {
         this.getFleetManager().loadByOwner(this.getOwnerName());
         this.recalcFleets();
         this.loaded = true;
      }

   }

   public void onUncachedFleet(Fleet var1) {
      this.fleets.remove(var1);
      this.loaded = false;
      this.log("UNCACHED FLEET " + var1.getName() + "; Total System Fleets " + this.getState().getFleetManager().getAvailableFleets(this.getOwnerName()).size(), LogInterface.LogLevel.DEBUG);
   }

   public BlueprintClassification[] getClasses(NPCSystemFleetManager.FleetType var1) {
      return this.getFaction().getConfig().getFleetClasses(var1);
   }

   public void log(String var1, LogInterface.LogLevel var2) {
      this.faction.log("[FLEETS]" + var1, var2);
   }

   public void lostEntity(long var1, SegmentController var3) {
      Iterator var5 = this.fleets.iterator();

      while(var5.hasNext()) {
         Fleet var4;
         if ((var4 = (Fleet)var5.next()).isMember(var1)) {
            var4.removeMemberByDbIdUID(var1, false);
            if (var4.isEmpty()) {
               var4.removeFleet(true);
               this.fleets.remove(var4);
               return;
            }
         }
      }

   }

   public static NPCSystemFleetManager.FleetType getTypeFromFleetName(String var0) {
      String[] var1;
      if ((var1 = var0.split("#")).length > 2) {
         try {
            return NPCSystemFleetManager.FleetType.valueOf(var1[1].trim());
         } catch (Exception var2) {
            System.err.println("Exception STR: '" + var0 + "'");
            var2.printStackTrace();
         }
      }

      return null;
   }

   public static int getFactionIdFromFleetName(String var0) {
      String[] var1;
      if ((var1 = var0.split("#")).length > 3) {
         try {
            return Integer.parseInt(var1[2]);
         } catch (NumberFormatException var2) {
            System.err.println("Exception STR: '" + var0 + "'");
            var2.printStackTrace();
         }
      }

      return 0;
   }

   public Fleet spawnTradingFleet(ElementCountMap var1, Vector3i var2, Vector3i var3) {
      String var4 = this.getName(this.stats.getFleetCreatorNumber(), NPCSystemFleetManager.FleetType.TRADING);
      this.getState().getFleetManager().requestCreateFleet(var4, this.getOwnerName());
      this.stats.incFleetNumber();
      this.recalcFleets();
      Iterator var5 = this.fleets.iterator();

      Fleet var6;
      do {
         if (!var5.hasNext()) {
            throw new IllegalArgumentException("Couldn't spawn fleet " + var4);
         }
      } while(!(var6 = (Fleet)var5.next()).getName().equals(var4));

      var2 = this.findUnloadedOnWay(var2, var3);
      this.fillTradeFleet(var6, var2, var1);
      return var6;
   }

   public void spawnAttackFleet(Vector3i var1, int var2) {
      NPCSystemFleetManager.FleetType var3 = NPCSystemFleetManager.FleetType.ATTACKING;
      String var4 = this.getName(this.stats.getFleetCreatorNumber(), var3);
      this.getState().getFleetManager().requestCreateFleet(var4, this.getOwnerName());
      this.stats.incFleetNumber();
      this.recalcFleets();
      Iterator var5 = this.fleets.iterator();

      Fleet var6;
      do {
         if (!var5.hasNext()) {
            throw new IllegalArgumentException("Couldn't spawn fleet " + var4);
         }
      } while(!(var6 = (Fleet)var5.next()).getName().equals(var4));

      Vector3i var8 = this.findUnloadedOnWay(this.faction.getHomeSector(), var1);
      if (this.fillFleets(var6, var3, var2, var8)) {
         FleetCommand var7 = new FleetCommand(FleetCommandTypes.PATROL_FLEET, var6, new Object[]{var1});
         this.getFleetManager().executeCommand(var7);
      } else {
         this.removeFleetAndPurgeShips(var6);
         this.recalcFleets();
      }
   }

   public void spawnScavengingFleet(Vector3i var1, int var2) {
      NPCSystemFleetManager.FleetType var3 = NPCSystemFleetManager.FleetType.SCAVENGE;
      String var4 = this.getName(this.stats.getFleetCreatorNumber(), var3);
      this.getState().getFleetManager().requestCreateFleet(var4, this.getOwnerName());
      this.stats.incFleetNumber();
      this.recalcFleets();
      Iterator var5 = this.fleets.iterator();

      Fleet var6;
      do {
         if (!var5.hasNext()) {
            throw new IllegalArgumentException("Couldn't spawn fleet " + var4);
         }
      } while(!(var6 = (Fleet)var5.next()).getName().equals(var4));

      Vector3i var8 = this.findUnloadedOnWay(this.faction.getHomeSector(), var1);
      if (this.fillFleets(var6, var3, var2, var8)) {
         FleetCommand var7 = new FleetCommand(FleetCommandTypes.PATROL_FLEET, var6, new Object[]{var1});
         this.getFleetManager().executeCommand(var7);
      } else {
         this.removeFleetAndPurgeShips(var6);
         this.recalcFleets();
      }
   }

   private boolean fillFleets(Fleet var1, NPCSystemFleetManager.FleetType var2, int var3, Vector3i var4) {
      BluePrintController var5 = this.faction.getConfig().getPreset().blueprintController;
      this.log("Checking classes use for " + var2.name() + ": " + this.faction.getConfig().getPreset().confFile.getParentFile().getName() + "; " + this.faction.getConfig().getPreset().blueprintController.entityBluePrintPath, LogInterface.LogLevel.DEBUG);
      List var13 = var5.readBluePrints();
      BlueprintClassification[] var6 = this.getClasses(var2);
      ObjectArrayList var7 = new ObjectArrayList();
      int var8 = (var6 = var6).length;

      for(int var9 = 0; var9 < var8; ++var9) {
         BlueprintClassification var11 = var6[var9];
         this.log("Checking class use for " + var2.name() + ": " + var11.name(), LogInterface.LogLevel.DEBUG);
         Iterator var12 = var13.iterator();

         while(var12.hasNext()) {
            BlueprintEntry var10 = (BlueprintEntry)var12.next();
            this.log("Checking blueprint use for trading: " + var10.getClassification().name() + "; " + var10.getName(), LogInterface.LogLevel.DEBUG);
            if (var11 == var10.getClassification()) {
               var7.add(var10);
            }
         }
      }

      if (var7.size() > 0) {
         for(int var14 = 0; var14 < var3; ++var14) {
            BlueprintEntry var15 = (BlueprintEntry)var7.get(var14 % var7.size());
            String var16 = "GFLTSHP_" + this.getFaction().getIdFaction() + "_" + this.stats.getShipCreatorNumber();
            this.stats.incShipNumber();
            long var17;
            if ((var17 = this.spawnInDB(this.faction.structure.getState(), var15, (ElementCountMap)null, var4, var16, "Faction Attack Ship")) > 0L) {
               this.getFleetManager().requestShipAdd(var1, var17);
            } else {
               System.err.println("Exception: FACTIONFLEETMANAGER: NOT ADDING SHIP " + var16);
            }
         }

         return true;
      } else {
         this.log("NO SHIPS FOR A FLEET OF TYPE " + var2.name() + "; REMOVING FLEET", LogInterface.LogLevel.ERROR);
         return false;
      }
   }

   private Vector3i findUnloadedOnWay(Vector3i var1, Vector3i var2) {
      if (var1.equals(var2)) {
         for(Vector3i var3 = new Vector3i(var1); this.getState().getUniverse().isSectorLoaded(var3); ++var3.x) {
         }
      }

      Vector3f var7 = new Vector3f((float)var1.x, (float)var1.y, (float)var1.z);
      Vector3f var4;
      (var4 = new Vector3f((float)var2.x, (float)var2.y, (float)var2.z)).sub(var7);
      var4.normalize();
      int var5 = 0;
      Vector3i var6 = new Vector3i(var1);

      do {
         if (!this.getState().getUniverse().isSectorLoaded(var6)) {
            return var6;
         }

         var7.add(var4);
         var6.set(Math.round(var7.x), Math.round(var7.y), Math.round(var7.z));
         ++var5;
      } while(var5 <= 1000);

      throw new IllegalArgumentException("NO UNLOADED SECTOR WITHIN 1000 tries: " + var1 + " -> " + var2 + "; f: " + var7 + "; t: " + var4);
   }

   private void recalcFleets() {
      ObjectArrayList var1 = this.getState().getFleetManager().getAvailableFleets(this.getOwnerName());
      this.fleets.clear();
      this.fleets.addAll(var1);
   }

   private void fillTradeFleet(Fleet var1, Vector3i var2, ElementCountMap var3) {
      List var4 = this.faction.getConfig().getPreset().blueprintController.readBluePrints();
      this.log("Checking trading classes use for Trading: " + this.faction.getConfig().getPreset().confFile.getParentFile().getName() + "; " + this.faction.getConfig().getPreset().blueprintController.entityBluePrintPath, LogInterface.LogLevel.DEBUG);
      BlueprintClassification[] var5 = this.getClasses(NPCSystemFleetManager.FleetType.TRADING);
      boolean var6 = false;

      assert var5.length > 0;

      double var8 = 0.0D;
      ObjectArrayList var7 = new ObjectArrayList();
      BlueprintClassification[] var11 = var5;
      int var12 = var5.length;

      label100:
      for(int var13 = 0; var13 < var12; ++var13) {
         BlueprintClassification var14 = var11[var13];
         this.log("Checking trading class use for trading: " + var14.name(), LogInterface.LogLevel.DEBUG);
         Iterator var25 = var4.iterator();

         while(true) {
            BlueprintEntry var10;
            do {
               if (!var25.hasNext()) {
                  continue label100;
               }

               var10 = (BlueprintEntry)var25.next();
               this.log("Checking trading class blueprint use for trading: " + var10.getClassification().name() + "; " + var10.getName(), LogInterface.LogLevel.DEBUG);
               if (var14 == var10.getClassification()) {
                  var7.add(var10);
                  var8 += var10.getTotalCapacity();
               }
            } while(var10.getClassification() != BlueprintClassification.ATTACK && var10.getClassification() != BlueprintClassification.DEFENSE);

            var6 = true;
         }
      }

      double var28 = var3.getVolume();
      double var29 = 0.0D;
      int var26 = 0;
      int var27 = 0;
      int var24 = 0;
      double var17 = var3.getMass();

      BlueprintEntry var15;
      while(var7.size() > 0 && var28 > 0.0D) {
         double var19 = (var15 = (BlueprintEntry)var7.get(var24 % var7.size())).getTotalCapacity();
         ElementCountMap var21 = null;
         if (var19 > 0.0D) {
            (var21 = new ElementCountMap()).transferFrom(var3, var19);
         }

         String var16 = "GFLTSHP_" + this.getFaction().getIdFaction() + "_" + this.stats.getShipCreatorNumber();
         this.stats.incShipNumber();
         this.log("Spawning trading fleet member " + var15.getName() + " for " + var1.getName(), LogInterface.LogLevel.DEBUG);
         long var22;
         if ((var22 = this.spawnInDB(this.faction.structure.getState(), var15, var21, var2, var16, this.faction.getName() + " Trade Fleet " + var15.getClassification().getName() + " " + var15.getName())) < 0L) {
            System.err.println("Exception: NPC FLEET MANAGER: NOT ADDING SHIP TO FLEET BECAUSE SPAWN DIDNT WORK: " + var16);
         } else {
            this.getFleetManager().requestShipAdd(var1, var22);
            var28 -= var19;
            ++var24;
            if (var15.getClassification() != BlueprintClassification.ATTACK && var15.getClassification() != BlueprintClassification.DEFENSE) {
               if (var15.getClassification() == BlueprintClassification.CARGO) {
                  ++var26;
               }
            } else {
               ++var27;
               var29 += var15.getMass();
            }

            if (var8 == 0.0D) {
               break;
            }
         }
      }

      if (var6) {
         while((var27 < this.getConfig().tradingMinDefenseShips || var29 < var17 * this.getConfig().tradingCargoMassVersusDefenseMass) && var27 < var26 * this.getConfig().tradingMaxDefenseShipsPerCargo) {
            if ((var15 = (BlueprintEntry)var7.get(var24 % var7.size())).getClassification() == BlueprintClassification.ATTACK || var15.getClassification() == BlueprintClassification.DEFENSE) {
               String var30 = "GFLTSHP_" + this.getFaction().getIdFaction() + "_" + this.stats.getShipCreatorNumber();
               this.stats.incShipNumber();
               this.log("Spawning additional trading fleet defense member " + var15.getName() + " for " + var1.getName(), LogInterface.LogLevel.DEBUG);
               long var20 = this.spawnInDB(this.faction.structure.getState(), var15, (ElementCountMap)null, var2, var30, this.faction.getName() + " Trade Fleet " + var15.getClassification().getName() + " " + var15.getName());
               this.getFleetManager().requestShipAdd(var1, var20);
               ++var27;
               var29 += var15.getMass();
            }

            ++var24;
            if (var8 < 1.0D) {
               break;
            }
         }
      }

      assert var24 > 0 : var8 + "; " + var7.size();

   }

   public NPCFactionConfig getConfig() {
      return this.faction.getConfig();
   }

   private long spawnInDB(GameServerState var1, BlueprintEntry var2, ElementCountMap var3, Vector3i var4, String var5, String var6) {
      BluePrintController var7;
      List var8 = (var7 = this.faction.getConfig().getPreset().blueprintController).readBluePrints();
      long var9 = -1L;
      if (var2 != null) {
         Transform var11;
         (var11 = new Transform()).setIdentity();

         try {
            if (var3 != null) {
               this.log("SPAWN IN DATABASE WITH BLOCKS: " + var3.getTotalAmount() + "; UID: " + var5, LogInterface.LogLevel.NORMAL);
            } else {
               this.log("SPAWN IN DATABASE WITHOUT BLOCKS; UID: " + var5, LogInterface.LogLevel.NORMAL);
            }

            SegmentControllerOutline var20;
            (var20 = var7.loadBluePrint(var1, var2.getName(), var5, var11, -1, this.faction.getIdFaction(), var8, var4, (List)null, "<system>", NPCFaction.buffer, true, (SegmentPiece)null, new ChildStats(true))).spawnSectorId = new Vector3i(var4);
            var20.realName = var6;
            var20.tradeNode = DatabaseEntry.removePrefixWOException(var5).equals(DatabaseEntry.removePrefixWOException(this.faction.getHomebaseUID()));
            var20.itemsToSpawnWith = var3;
            ChildStats var18 = new ChildStats(false);
            ObjectArrayList var19 = new ObjectArrayList();
            var9 = var20.spawnInDatabase(var4, var1, 0, var19, var18, true);
         } catch (EntityNotFountException var12) {
            var12.printStackTrace();
         } catch (IOException var13) {
            var13.printStackTrace();
         } catch (EntityAlreadyExistsException var14) {
            var14.printStackTrace();
            String var17 = var2.getEntityType().type.dbPrefix + DatabaseEntry.removePrefixWOException(var5);
            var9 = var1.getDatabaseIndex().getTableManager().getEntityTable().getIdForFullUID(var17);
            System.err.println("[SERVER] Exception caught sucessfully. DB entry: " + var17 + "; returned id: " + var9);
         } catch (SQLException var15) {
            var15.printStackTrace();
         } catch (StateParameterNotFoundException var16) {
            var16.printStackTrace();
         }
      }

      return var9;
   }

   public void onFinishedTrade(Fleet var1) {
      this.removeFleetAndPurgeShips(var1);
   }

   private void removeFleetAndPurgeShips(Fleet var1) {
      Iterator var2 = (new ObjectArrayList(var1.getMembers())).iterator();

      while(var2.hasNext()) {
         FleetMember var3 = (FleetMember)var2.next();
         var1.removeMemberByDbIdUID(var3.entityDbId, true);
      }

      this.faction.structure.getState().getFleetManager().removeFleet(var1);
      this.fleets.remove(var1);
   }

   public void update(long var1) {
      if (!this.loaded) {
         this.load();

         for(int var3 = 0; var3 < this.fleets.size(); ++var3) {
            Fleet var2 = (Fleet)this.fleets.get(var3);
            switch(var2.getNpcType()) {
            case ATTACKING:
               this.log("removing inactive attack fleet", LogInterface.LogLevel.NORMAL);
               this.removeFleetAndPurgeShips(var2);
               --var3;
            case DEFENDING:
            case MINING:
            default:
               break;
            case SCAVENGE:
               this.log("removing inactive attack fleet", LogInterface.LogLevel.NORMAL);
               this.removeFleetAndPurgeShips(var2);
               --var3;
               break;
            case TRADING:
               if (!this.getState().getGameState().getTradeManager().getTradeActiveMap().getFleetsInTrades().contains(var2.dbid)) {
                  this.log("removing trading fleet because no active trade found for it (probably due to crash before save)", LogInterface.LogLevel.ERROR);
                  this.removeFleetAndPurgeShips(var2);
                  --var3;
               } else {
                  this.log("NOT removing trading fleet because active trade found for it", LogInterface.LogLevel.DEBUG);
               }
            }
         }
      }

   }

   public void fleetTurn() {
      List var1 = this.faction.getEnemies();
      Random var2 = new Random();
      Iterator var10 = var1.iterator();

      while(true) {
         while(var10.hasNext()) {
            Faction var3 = (Faction)var10.next();
            int var4 = 3 + var2.nextInt(8);
            if (var3.isPlayerFaction()) {
               List var14;
               if ((var14 = var3.getOnlinePlayers()).size() > 0) {
                  Collections.shuffle(var14);
                  this.spawnAttackFleet(new Vector3i(((PlayerState)var14.get(0)).getCurrentSector()), var4);
               }
            } else if (var3.isNPC()) {
               NPCFaction var5 = (NPCFaction)var3;
               ObjectArrayList var6;
               Collections.shuffle(var6 = new ObjectArrayList(this.getState().getPlayerStatesByDbId().values()));
               boolean var7 = false;
               Iterator var8 = var6.iterator();

               while(var8.hasNext()) {
                  PlayerState var16 = (PlayerState)var8.next();

                  try {
                     if (this.getState().getUniverse().getStellarSystemFromStellarPos(var16.getCurrentSystem()).getOwnerFaction() == var3.getIdFaction()) {
                        var7 = true;
                        this.spawnAttackFleet(new Vector3i(var16.getCurrentSector()), var4);
                        break;
                     }
                  } catch (IOException var9) {
                     var9.printStackTrace();
                  }
               }

               if (!var7) {
                  NPCSystem var20 = var5.structure.findClosestFrom(this.faction.structure.getRoot(), 2);
                  this.spawnAttackFleet(var20.getSystemBaseSector(), var4);
               }
            }
         }

         ObjectOpenHashSet var11 = new ObjectOpenHashSet(this.getState().getUniverse().attackSector.keySet());

         label56:
         for(int var12 = 0; var12 < this.getFaction().getConfig().scvengingFleetsPerTurn; ++var12) {
            Vector3i var13 = null;
            float var15 = 0.0F;
            Iterator var17 = var11.iterator();

            while(true) {
               Vector3i var19;
               float var21;
               do {
                  if (!var17.hasNext()) {
                     if (var13 != null) {
                        var11.remove(var13);
                        float var18 = Math.max(this.getFaction().getConfig().minScavenginRange, (float)this.getFaction().structure.getLastHabitatedLevel() * this.getFaction().getConfig().scavengingRangePerFactionLevel);
                        if (Vector3i.getDisatance(this.getFaction().getHomeSector(), var13) / 16.0F < var18) {
                           this.spawnScavengingFleet(var13, 5);
                        }
                     }
                     continue label56;
                  }

                  var19 = (Vector3i)var17.next();
                  var21 = Vector3i.getDisatance(this.getFaction().getHomeSector(), var19);
               } while(var13 != null && var21 >= var15);

               var13 = var19;
               var15 = var21;
            }
         }

         return;
      }
   }

   class NPCGlobalStats extends NPCStats {
      private Vector3i idSys;

      private NPCGlobalStats() {
         this.idSys = new Vector3i(NPCFleetManager.this.faction.getIdFaction(), Integer.MIN_VALUE, Integer.MIN_VALUE);
      }

      public void incShipNumber() {
         super.incShipNumber((GameServerState)NPCFleetManager.this.getFaction().getState(), NPCFleetManager.this.getFaction().getIdFaction(), this.idSys);
      }

      public void incFleetNumber() {
         super.incFleetNumber((GameServerState)NPCFleetManager.this.getFaction().getState(), NPCFleetManager.this.getFaction().getIdFaction(), this.idSys);
      }

      public int getFleetCreatorNumber() {
         return super.getFleetCreatorNumber((GameServerState)NPCFleetManager.this.getFaction().getState(), NPCFleetManager.this.getFaction().getIdFaction(), this.idSys);
      }

      public int getShipCreatorNumber() {
         return super.getShipCreatorNumber((GameServerState)NPCFleetManager.this.getFaction().getState(), NPCFleetManager.this.getFaction().getIdFaction(), this.idSys);
      }

      // $FF: synthetic method
      NPCGlobalStats(Object var2) {
         this();
      }
   }
}
