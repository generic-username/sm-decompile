package org.schema.game.server.data.simulation.npc.diplomacy;

import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class NPCDiplTurnModifier extends NPCDiplModifier {
   public DiplomacyAction.DiplActionType type;
   public int pointsPerTurn;
   public long elapsedTime;
   public long totalElapsedTime;

   public Tag toTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.INT, (String)null, this.pointsPerTurn), new Tag(Tag.Type.LONG, (String)null, this.totalElapsedTime), new Tag(Tag.Type.BYTE, (String)null, (byte)this.type.ordinal()), FinishTag.INST});
   }

   public void fromTag(Tag var1) {
      Tag[] var2;
      (var2 = var1.getStruct())[0].getByte();
      this.pointsPerTurn = var2[1].getInt();
      this.totalElapsedTime = var2[2].getLong();
      this.type = DiplomacyAction.DiplActionType.values()[var2[3].getByte()];
   }

   public String getName() {
      return this.type.getDescription();
   }

   public boolean isStatic() {
      return false;
   }

   public int getValue() {
      return this.pointsPerTurn;
   }
}
