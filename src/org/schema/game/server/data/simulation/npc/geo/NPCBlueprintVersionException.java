package org.schema.game.server.data.simulation.npc.geo;

public class NPCBlueprintVersionException extends Exception {
   private static final long serialVersionUID = 1L;

   public NPCBlueprintVersionException() {
   }

   public NPCBlueprintVersionException(String var1, Throwable var2, boolean var3, boolean var4) {
      super(var1, var2, var3, var4);
   }

   public NPCBlueprintVersionException(String var1, Throwable var2) {
      super(var1, var2);
   }

   public NPCBlueprintVersionException(String var1) {
      super(var1);
   }

   public NPCBlueprintVersionException(Throwable var1) {
      super(var1);
   }
}
