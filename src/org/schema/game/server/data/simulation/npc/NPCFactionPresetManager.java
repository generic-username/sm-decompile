package org.schema.game.server.data.simulation.npc;

import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Map.Entry;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.common.util.data.DataUtil;
import org.schema.game.server.data.simulation.npc.geo.NPCFactionPreset;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.LoadingScreen;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.xml.sax.SAXException;

public class NPCFactionPresetManager {
   public static String npcPathReal;
   public static final String npcPathPre;
   private static final byte TAG_VERSION = 0;
   private final List npcPresets = new ObjectArrayList();
   private final Object2IntOpenHashMap takenNPCPresets = new Object2IntOpenHashMap();

   public static void importPresets(String var0) {
      File[] var1 = (new File(npcPathPre)).listFiles();
      npcPathReal = var0 + File.separator + "npcFactions" + File.separator;
      File var6;
      (var6 = new File(npcPathReal)).mkdir();
      File[] var7 = var6.listFiles();
      ObjectOpenHashSet var2 = new ObjectOpenHashSet();
      int var3 = (var7 = var7).length;

      int var4;
      for(var4 = 0; var4 < var3; ++var4) {
         File var5;
         if ((var5 = var7[var4]).isDirectory()) {
            var2.add(var5.getName().toLowerCase(Locale.ENGLISH));
         }
      }

      File[] var8 = var1;
      var4 = var1.length;

      for(int var9 = 0; var9 < var4; ++var9) {
         if ((var6 = var8[var9]).isDirectory() && !var2.contains(var6.getName().toLowerCase(Locale.ENGLISH))) {
            LoadingScreen.serverMessage = Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_NPCFACTIONPRESETMANAGER_0;
            NPCFactionPreset.importPreset(npcPathPre + var6.getName() + File.separator, npcPathReal + var6.getName() + File.separator);
         }
      }

   }

   public void readNpcPresets() {
      File[] var1;
      int var2 = (var1 = (new File(npcPathReal)).listFiles()).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         File var4;
         if ((var4 = var1[var3]).isDirectory()) {
            try {
               NPCFactionPreset var8 = NPCFactionPreset.readFromDir(npcPathReal + var4.getName() + File.separator);
               this.getNpcPresets().add(var8);
            } catch (IOException var5) {
               var5.printStackTrace();
            } catch (SAXException var6) {
               var6.printStackTrace();
            } catch (ParserConfigurationException var7) {
               var7.printStackTrace();
            }
         }
      }

      assert this.getNpcPresets().size() > 0;

   }

   public NPCFactionPreset getRandomLeastUsedPreset(long var1, int var3) {
      assert this.getNpcPresets() != null;

      Random var6 = new Random(var1 + (long)var3);
      int var2 = Integer.MAX_VALUE;
      Iterator var8 = this.getNpcPresets().iterator();

      while(var8.hasNext()) {
         NPCFactionPreset var4 = (NPCFactionPreset)var8.next();

         assert var4.factionPresetName.equals(var4.factionPresetName.toLowerCase(Locale.ENGLISH));

         if (!this.takenNPCPresets.containsKey(var4.factionPresetName.toLowerCase(Locale.ENGLISH))) {
            this.takenNPCPresets.put(var4.factionPresetName, 0);
         }
      }

      for(var8 = this.takenNPCPresets.entrySet().iterator(); var8.hasNext(); var2 = Math.min((Integer)((Entry)var8.next()).getValue(), var2)) {
      }

      ObjectArrayList var9 = new ObjectArrayList();
      Iterator var10 = this.takenNPCPresets.entrySet().iterator();

      while(var10.hasNext()) {
         Entry var5;
         if ((Integer)(var5 = (Entry)var10.next()).getValue() == var2) {
            var9.add(var5.getKey());
         }
      }

      assert var9.size() > 0 : var9 + "; " + this.takenNPCPresets;

      String var11 = (String)var9.get(var6.nextInt(var9.size()));
      Iterator var12 = this.getNpcPresets().iterator();

      NPCFactionPreset var7;
      do {
         if (!var12.hasNext()) {
            throw new IllegalArgumentException("No faction preset found");
         }
      } while(!(var7 = (NPCFactionPreset)var12.next()).factionPresetName.equals(var11));

      this.takenNPCPresets.add(var7.factionPresetName, 1);
      return var7;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2;
      (Byte)(var2 = (Tag[])var1.getValue())[0].getValue();
      Tag.fromString2IntMapToTagStruct(var2[1], this.takenNPCPresets);
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), Tag.string2IntMapToTagStruct(this.takenNPCPresets), FinishTag.INST});
   }

   public List getNpcPresets() {
      return this.npcPresets;
   }

   static {
      npcPathPre = "." + File.separator + DataUtil.dataPath + "npcFactions" + File.separator;
   }
}
