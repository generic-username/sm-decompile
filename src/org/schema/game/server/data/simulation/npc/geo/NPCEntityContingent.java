package org.schema.game.server.data.simulation.npc.geo;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import org.schema.common.util.LogInterface;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class NPCEntityContingent implements LogInterface, SerialializationInterface {
   private static final byte VERSION = 0;
   public final List entities = new ObjectArrayList();
   public final NPCEntityContainer spawnedEntities = new NPCEntityContainer(this);
   public final NPCSystemStub system;
   public final ElementCountMap contingentValueServer = new ElementCountMap();
   private double minerScore = -1.0D;
   private float consumed = 0.0F;

   public NPCEntityContingent(NPCSystemStub var1) {
      this.system = var1;
   }

   public int getSpawned(NPCEntityContingent.NPCEntitySpecification var1) {
      return this.getSpawned(var1.bbName);
   }

   public int getSpawned(String var1) {
      return this.spawnedEntities.getSpanwed(var1);
   }

   public void spawn(NPCEntityContingent.NPCEntitySpecification var1, long var2) {
      this.spawnedEntities.spawn(var1, var2);
      ((NPCSystem)this.system).setChangedNT();
   }

   public NPCEntityContingent.NPCEntitySpecification getMainStation() {
      NPCEntityContingent.NPCEntitySpecification var1 = null;

      assert !this.entities.isEmpty();

      Iterator var2 = this.entities.iterator();

      while(var2.hasNext()) {
         NPCEntityContingent.NPCEntitySpecification var3;
         if ((var3 = (NPCEntityContingent.NPCEntitySpecification)var2.next()).hasLeft() && var3.type == SimpleTransformableSendableObject.EntityType.SPACE_STATION && var3.mass > -1.0F) {
            var1 = var3;
         }
      }

      assert var1 != null;

      return var1;
   }

   public NPCEntityContingent.NPCEntitySpecification getStation(long var1, Random var3) {
      var3.setSeed(var1);
      ObjectArrayList var4;
      Collections.shuffle(var4 = new ObjectArrayList(this.entities), var3);
      NPCEntityContingent.NPCEntitySpecification var2 = null;
      Iterator var5 = var4.iterator();

      while(var5.hasNext()) {
         NPCEntityContingent.NPCEntitySpecification var6;
         if ((var6 = (NPCEntityContingent.NPCEntitySpecification)var5.next()).hasLeft() && var6.type == SimpleTransformableSendableObject.EntityType.SPACE_STATION) {
            var2 = var6;
            break;
         }
      }

      return var2;
   }

   public List getShips(Fleet var1, BlueprintClassification... var2) {
      ObjectArrayList var3 = new ObjectArrayList(this.entities.size());
      Iterator var4 = this.entities.iterator();

      while(true) {
         while(true) {
            while(true) {
               NPCEntityContingent.NPCEntitySpecification var5;
               do {
                  if (!var4.hasNext()) {
                     if (var2 == null || var2.length == 0) {
                        Collections.shuffle(var3);
                     }

                     return var3;
                  }
               } while((var5 = (NPCEntityContingent.NPCEntitySpecification)var4.next()).type != SimpleTransformableSendableObject.EntityType.SHIP);

               if (var2 != null && var2.length != 0) {
                  for(int var6 = 0; var6 < var2.length; ++var6) {
                     BlueprintClassification var7 = var2[(var1.getMembers().size() + var6) % var2.length];
                     if (var5.c == var7) {
                        var3.add(var5);
                        break;
                     }
                  }
               } else {
                  var3.add(var5);
               }
            }
         }
      }
   }

   public void add(String var1, SimpleTransformableSendableObject.EntityType var2, float var3, BlueprintClassification var4, int var5, ElementCountMap var6) {
      NPCEntityContingent.NPCEntitySpecification var7;
      (var7 = new NPCEntityContingent.NPCEntitySpecification()).bbName = var1;
      var7.type = var2;
      var7.c = var4;
      var7.mass = var3;
      var7.count = var5;
      this.entities.add(var7);
      this.contingentValueServer.add(var6);
      if (this.isOnServer()) {
         this.minerScore = -1.0D;
      }

   }

   public String toString() {
      StringBuffer var1 = new StringBuffer();
      Iterator var2 = this.entities.iterator();

      while(var2.hasNext()) {
         NPCEntityContingent.NPCEntitySpecification var3 = (NPCEntityContingent.NPCEntitySpecification)var2.next();
         var1.append(var3.bbName + "; " + var3.type.name() + "; " + var3.c.name() + " x " + var3.count + "; Spawned(" + this.spawnedEntities.getSpanwed(var3.bbName) + ")\n");
      }

      var1.append("SPAWNED: \n" + this.spawnedEntities.toString());
      return var1.toString();
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeInt(this.entities.size());
      Iterator var3 = this.entities.iterator();

      while(var3.hasNext()) {
         ((NPCEntityContingent.NPCEntitySpecification)var3.next()).serialize(var1, var2);
      }

      this.spawnedEntities.serialize(var1);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.entities.clear();
      int var4 = var1.readInt();

      for(int var5 = 0; var5 < var4; ++var5) {
         NPCEntityContingent.NPCEntitySpecification var6;
         (var6 = new NPCEntityContingent.NPCEntitySpecification()).deserialize(var1, var2, var3);
         this.entities.add(var6);
      }

      this.spawnedEntities.deserialize(var1);
   }

   public int getTotalAmountClass(BlueprintClassification... var1) {
      int var2 = 0;
      Iterator var3 = this.entities.iterator();

      while(true) {
         while(var3.hasNext()) {
            NPCEntityContingent.NPCEntitySpecification var4 = (NPCEntityContingent.NPCEntitySpecification)var3.next();
            BlueprintClassification[] var5 = var1;
            int var6 = var1.length;

            for(int var7 = 0; var7 < var6; ++var7) {
               BlueprintClassification var8 = var5[var7];
               if (var4.c == var8) {
                  var2 += var4.count;
                  break;
               }
            }
         }

         return var2;
      }
   }

   public Tag toTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), this.spawnedEntities.toTag(), new Tag(Tag.Type.FLOAT, (String)null, this.consumed), FinishTag.INST});
   }

   public void fromTag(Tag var1) {
      Tag[] var2;
      (var2 = var1.getStruct())[0].getByte();
      this.spawnedEntities.fromTag(var2[1]);
      this.consumed = var2[2].getFloat();
   }

   public void log(String var1, LogInterface.LogLevel var2) {
      this.system.log(var1, var2);
   }

   public void clearContignet() {
      this.entities.clear();
   }

   public NPCEntityContingent.NPCEntitySpecification getSpec(String var1) {
      Iterator var2 = this.entities.iterator();

      NPCEntityContingent.NPCEntitySpecification var3;
      do {
         if (!var2.hasNext()) {
            return null;
         }
      } while(!(var3 = (NPCEntityContingent.NPCEntitySpecification)var2.next()).bbName.toLowerCase(Locale.ENGLISH).equals(var1.toLowerCase(Locale.ENGLISH)));

      return var3;
   }

   public NPCEntityContainer.NPCEntity killRandomSpawned() {
      assert this.isOnServer();

      Iterator var1;
      if ((var1 = this.spawnedEntities.getSpanwed().iterator()).hasNext()) {
         NPCEntityContainer.NPCEntity var2;
         if ((var2 = (NPCEntityContainer.NPCEntity)var1.next()).entityId != ((NPCSystem)this.system).getFaction().getTradeNode().getEntityDBId()) {
            ((NPCSystem)this.system).state.destroyEntity(var2.entityId);
            return var2;
         }

         System.err.println("[SERVER][NPC][KILLRANDOM] not killing homebase");
      }

      return null;
   }

   private boolean isOnServer() {
      return this.system instanceof NPCSystem;
   }

   public void despawnAllSpawned(NPCSystem var1) {
      this.spawnedEntities.onRemovedCleanUp(var1);
   }

   public void killRandomNonSpanwed() {
      this.spawnedEntities.loseRandomResources();
   }

   public double getMinerScore() {
      if (this.isOnServer() && this.minerScore < 0.0D) {
         double var1 = this.minerScore;
         this.minerScore = this.recalcMinerScore();
         if (var1 != this.minerScore) {
            ((NPCSystem)this.system).setChangedNT();
         }
      }

      return this.minerScore;
   }

   private double recalcMinerScore() {
      assert this.isOnServer();

      BluePrintController var3 = ((NPCSystem)this.system).getFaction().getConfig().getPreset().blueprintController;
      double var1 = 0.0D + ((NPCSystem)this.system).getFaction().getConfig().basicMiningScorePerSystem;
      Iterator var4 = this.entities.iterator();

      while(var4.hasNext()) {
         NPCEntityContingent.NPCEntitySpecification var5;
         if ((var5 = (NPCEntityContingent.NPCEntitySpecification)var4.next()).c == BlueprintClassification.MINING) {
            try {
               BlueprintEntry var6 = var3.getBlueprint(var5.bbName);
               double var7 = 0.0D;
               if (var6.getScore() != null) {
                  var7 = var6.getScore().miningIndex;
               } else {
                  System.err.println("[NPCFACTION] ERROR: no score attached for blueprint: " + var6);
                  this.log("ERROR: no score attached for blueprint: " + var6, LogInterface.LogLevel.ERROR);
               }

               var1 += var7 * (double)var5.count;
            } catch (EntityNotFountException var9) {
               var9.printStackTrace();
            }
         } else if (var5.c == BlueprintClassification.MINING_STATION) {
            var1 += ((NPCSystem)this.system).getFaction().getConfig().miningStationMiningScore;
         }
      }

      return var1;
   }

   public void resupply(NPCSystem var1, long var2) {
      this.spawnedEntities.resupply(var1, var2);
   }

   public void consume(NPCSystem var1, long var2) {
      if (this.consumed < 1.0F) {
         BluePrintController var16 = var1.getFaction().getConfig().getPreset().blueprintController;
         IntOpenHashSet var3 = new IntOpenHashSet();
         ElementCountMap var4 = new ElementCountMap();
         Iterator var5 = this.entities.iterator();

         while(var5.hasNext()) {
            NPCEntityContingent.NPCEntitySpecification var6 = (NPCEntityContingent.NPCEntitySpecification)var5.next();

            try {
               double var9 = (double)(var1.getFaction().getConfig().resourcesConsumeStep * var1.distanceFactor * var1.getFaction().getConfig().resourcesConsumedPerDistance);
               ElementCountMap var7 = var16.getBlueprint(var6.bbName).getElementCountMapWithChilds();
               short[] var8;
               int var11 = (var8 = ElementKeyMap.typeList()).length;

               for(int var12 = 0; var12 < var11; ++var12) {
                  short var13 = var8[var12];
                  int var14 = (int)((double)var7.get(var13) * (double)var6.count * var9);
                  var4.inc(var13, var14);
               }
            } catch (EntityNotFountException var15) {
               var15.printStackTrace();
            }
         }

         if (!var4.isEmpty()) {
            var1.getFaction().getInventory().decreaseBatchIgnoreAmount(var4, var3);
         }

         if (var3.size() > 0) {
            var1.getFaction().getInventory().sendInventoryModification(var3);
         }

         this.consumed += var1.getFaction().getConfig().resourcesConsumeStep;
         this.log("CONSUME: consumed: " + var4.getTotalAmount() + " Blocks (maintenance); consumed status: " + this.consumed + " / 1", LogInterface.LogLevel.DEBUG);
      }

   }

   public int getTotalAmount() {
      int var1 = 0;

      NPCEntityContingent.NPCEntitySpecification var3;
      for(Iterator var2 = this.entities.iterator(); var2.hasNext(); var1 += var3.count) {
         var3 = (NPCEntityContingent.NPCEntitySpecification)var2.next();
      }

      return var1;
   }

   public boolean isEqualTo(List var1) {
      if (var1.size() != this.entities.size()) {
         return false;
      } else {
         Collections.sort(var1);
         Collections.sort(this.entities);

         for(int var2 = 0; var2 < var1.size(); ++var2) {
            if (((NPCEntityContingent.NPCEntitySpecification)var1.get(var2)).hashCode() != this.entities.hashCode() || !this.entities.equals(var1.get(var2))) {
               return false;
            }
         }

         return true;
      }
   }

   public class NPCEntitySpecification implements Comparable, SerialializationInterface {
      public String bbName;
      public SimpleTransformableSendableObject.EntityType type;
      public BlueprintClassification c;
      public int count;
      public float mass;

      public void serialize(DataOutput var1, boolean var2) throws IOException {
         assert this.bbName != null;

         var1.writeUTF(this.bbName);
         var1.writeByte(this.type.ordinal());
         var1.writeByte(this.c.ordinal());
         var1.writeInt(this.count);
         var1.writeFloat(this.mass);
      }

      public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
         this.bbName = var1.readUTF();
         this.type = SimpleTransformableSendableObject.EntityType.values()[var1.readByte()];
         this.c = BlueprintClassification.values()[var1.readByte()];
         this.count = var1.readInt();
         this.mass = var1.readFloat();
      }

      public boolean hasLeft() {
         return this.getLeft() > 0;
      }

      public int getLeft() {
         return this.count - NPCEntityContingent.this.getSpawned(this);
      }

      public String toString() {
         return "NPCEntitySpecification [bbName=" + this.bbName + ", type=" + this.type + ", c=" + this.c + ", count=" + this.count + ", mass=" + this.mass + "]";
      }

      public int hashCode() {
         return (((((31 + this.getOuterType().hashCode()) * 31 + (this.bbName == null ? 0 : this.bbName.hashCode())) * 31 + (this.c == null ? 0 : this.c.hashCode())) * 31 + this.count) * 31 + Float.floatToIntBits(this.mass)) * 31 + (this.type == null ? 0 : this.type.hashCode());
      }

      public boolean equals(Object var1) {
         if (this == var1) {
            return true;
         } else if (var1 == null) {
            return false;
         } else if (!(var1 instanceof NPCEntityContingent.NPCEntitySpecification)) {
            return false;
         } else {
            NPCEntityContingent.NPCEntitySpecification var2 = (NPCEntityContingent.NPCEntitySpecification)var1;
            if (!this.getOuterType().equals(var2.getOuterType())) {
               return false;
            } else {
               if (this.bbName == null) {
                  if (var2.bbName != null) {
                     return false;
                  }
               } else if (!this.bbName.equals(var2.bbName)) {
                  return false;
               }

               if (this.c != var2.c) {
                  return false;
               } else if (this.count != var2.count) {
                  return false;
               } else if (Float.floatToIntBits(this.mass) != Float.floatToIntBits(var2.mass)) {
                  return false;
               } else {
                  return this.type == var2.type;
               }
            }
         }
      }

      private NPCEntityContingent getOuterType() {
         return NPCEntityContingent.this;
      }

      public int compareTo(NPCEntityContingent.NPCEntitySpecification var1) {
         int var2;
         if ((var2 = this.bbName.compareTo(var1.bbName)) == 0) {
            return (var2 = this.c.name().compareTo(var1.c.name())) == 0 ? this.count - var1.count : var2;
         } else {
            return var2;
         }
      }
   }
}
