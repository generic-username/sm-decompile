package org.schema.game.server.data.simulation.npc;

import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.shorts.Short2FloatOpenHashMap;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Map.Entry;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.schema.common.config.ConfigParserException;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.CompareTools;
import org.schema.common.util.LogInterface;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.game.server.data.simulation.npc.diplomacy.DiplomacyAction;
import org.schema.game.server.data.simulation.npc.diplomacy.DiplomacyReaction;
import org.schema.game.server.data.simulation.npc.diplomacy.NPCDiplomacyEntity;
import org.schema.game.server.data.simulation.npc.geo.NPCEntityContingent;
import org.schema.game.server.data.simulation.npc.geo.NPCFactionPreset;
import org.schema.game.server.data.simulation.npc.geo.NPCSystemFleetManager;
import org.schema.game.server.data.simulation.npc.geo.NPCSystemStructure;
import org.schema.schine.resource.tag.Tag;
import org.w3c.dom.Comment;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class NPCFactionConfig {
   public static final byte XML_VERSION = 0;
   public static boolean recreate = false;
   @ConfigurationElement(
      name = "Outer"
   )
   public NPCFactionConfig.EntityPriority outer = new NPCFactionConfig.EntityPriority();
   @ConfigurationElement(
      name = "Inner"
   )
   public NPCFactionConfig.EntityPriority inner = new NPCFactionConfig.EntityPriority();
   @ConfigurationElement(
      name = "BaseCredits"
   )
   public int baseCredits = 10000;
   @ConfigurationElement(
      name = "RandomCredits"
   )
   public int randomCredits = 1000000;
   @ConfigurationElement(
      name = "HomeSystemEntitiesStart",
      description = "count of entities in home system. the amount of other systems adapts depending on distance and fill"
   )
   private int homeSystemEntitiesStart = 25;
   @ConfigurationElement(
      name = "ShipCountModifier",
      description = "Must be at least 1. Changes the HomeSystemEntitiesStart for ships"
   )
   private float shipCountModifier = 1.0F;
   @ConfigurationElement(
      name = "StationCountModifier",
      description = "Must be at least 1. Changes the HomeSystemEntitiesStart for stations"
   )
   private float stationCountModifier = 1.0F;
   @ConfigurationElement(
      name = "FactionName"
   )
   private String factionName = "TestFaction";
   public int[] baseTypeCount;
   public int[] randomTypeCount;
   @ConfigurationElement(
      name = "DefaultBaseTypeCountRaw",
      description = "for initial inventory filling (raw stuff)"
   )
   public int defaultBaseTypeCountRaw;
   @ConfigurationElement(
      name = "DefaultRandomTypeCountRaw",
      description = "for initial inventory filling (raw stuff)"
   )
   public int defaultRandomTypeCountRaw;
   @ConfigurationElement(
      name = "DefaultBaseTypeCountLevel1",
      description = "for initial inventory filling (basic factory stuff)"
   )
   public int defaultBaseTypeCountLevel1;
   @ConfigurationElement(
      name = "DefaultRandomTypeCountLevel1",
      description = "for initial inventory filling (basic factory stuff)"
   )
   public int defaultRandomTypeCountLevel1;
   @ConfigurationElement(
      name = "DefaultBaseTypeCountLevel2",
      description = "for initial inventory filling (std factory stuff)"
   )
   public int defaultBaseTypeCountLevel2;
   @ConfigurationElement(
      name = "DefaultRandomTypeCountLevel2",
      description = "for initial inventory filling (std factory stuff)"
   )
   public int defaultRandomTypeCountLevel2;
   @ConfigurationElement(
      name = "DefaultBaseTypeCountLevel3",
      description = "for initial inventory filling (adv factory stuff)"
   )
   public int defaultBaseTypeCountLevel3;
   @ConfigurationElement(
      name = "DefaultRandomTypeCountLevel3",
      description = "for initial inventory filling (adv factory stuff)"
   )
   public int defaultRandomTypeCountLevel3;
   public final Short2FloatOpenHashMap typeWeightsWithProduction;
   public final Short2FloatOpenHashMap typeWeights;
   @ConfigurationElement(
      name = "MinimumNormalizationModifier",
      description = "When creating basic demand from blueprints. This is used to set a minimum weight, so that smaller ships become similary important than bigger things (since an npc faction is going to use higher numbers of smaller ships mostly)"
   )
   private float minimumNormalizationModifier;
   private long totalElements;
   @ConfigurationElement(
      name = "LevelsFilled",
      description = "levels (distance from homebase) filled until entity count falloff. The higher, the further away systems will have the same amount of entities as the home system. Use to make more defensive slow expending factions, since they need take longer to assemble resources toexpand as they are building much more stuff in the already taken territory."
   )
   private int levelsFilled;
   private NPCFactionConfig.BlueprintTypeAvailability bta;
   @ConfigurationElement(
      name = "ProductionRawConversion",
      description = "How many materials (in 0..1%) to use in production turn"
   )
   public float productionRawConversion;
   @ConfigurationElement(
      name = "DemandAdditionalPerSystemMultiplier",
      description = "Adds percentage of demand per system to total demand"
   )
   public float demandMultPerSystem;
   @ConfigurationElement(
      name = "MinDemandPerSystem",
      description = "Minimum demand for every resource per system"
   )
   public int minDemandPerSystem;
   @ConfigurationElement(
      name = "TradeDemandMult",
      description = "Demand multiplier for trading"
   )
   public float tradeDemandMult;
   @ConfigurationElement(
      name = "ExpensionDemandMult",
      description = "Demand multiplier for faction to grow"
   )
   public float expensionDemandMult;
   @ConfigurationElement(
      name = "ExpensionDemandConsumeMult",
      description = "Demand consumed on actual expansion"
   )
   public float expensionDemandConsumeMult;
   @ConfigurationElement(
      name = "TradeBuyLowerPercentage",
      description = "minimum Inventory/Demand percentage on which to start buying"
   )
   public float tradeBuyLowerPercentage;
   @ConfigurationElement(
      name = "TradeBuyUpperPercentage",
      description = "maximum Inventory/Demand percentage on which to stop buying"
   )
   public float tradeBuyUpperPercentage;
   @ConfigurationElement(
      name = "TradeBuyLowerPricePercentage",
      description = "Price to buy at on TradeBuyUpperPercentage"
   )
   public float tradeBuyLowerPricePercentage;
   @ConfigurationElement(
      name = "TradeBuyUpperPricePercentage",
      description = "Price to buy at on TradeBuyLowerPercentage"
   )
   public float tradeBuyUpperPricePercentage;
   @ConfigurationElement(
      name = "TradeSellLowerPercentage",
      description = "maximum Inventory/Demand percentage on which to stop selling"
   )
   public float tradeSellLowerPercentage;
   @ConfigurationElement(
      name = "TradeSellUpperPercentage",
      description = "minimum Inventory/Demand percentage on which to start selling"
   )
   public float tradeSellUpperPercentage;
   @ConfigurationElement(
      name = "TradeSellLowerPricePercentage",
      description = "Price to buy at on TradeSellUpperPercentage"
   )
   public float tradeSellLowerPricePercentage;
   @ConfigurationElement(
      name = "TradeSellUpperPricePercentage",
      description = "Price to sell at on TradeSellLowerPercentage"
   )
   public float tradeSellUpperPricePercentage;
   @ConfigurationElement(
      name = "TradePricePerSystem",
      description = "Trade Value: additional price per system (transport cost)"
   )
   public float tradePricePerSystem;
   @ConfigurationElement(
      name = "MaxOwnCreditsToUseForTrade",
      description = "Credits to use to buy stuff"
   )
   public float maxOwnCreditsToUseForTrade;
   @ConfigurationElement(
      name = "MaxOtherCreditsToUseForTrade",
      description = "Credits of trading partner to use for selling"
   )
   public float maxOtherCreditsToUseForTrade;
   private NPCFactionPreset preset;
   @ConfigurationElement(
      name = "AbandonSystemOnStatus",
      description = "Abanond system when status (lost resources) are percentage of contingent"
   )
   public double abandonSystemOnStatus;
   @ConfigurationElement(
      name = "AbandonSystemOnStatusAfterResupply",
      description = "Abanond system when status (lost resources) are percentage of contingent. This is checked after resupply"
   )
   public double abandonSystemOnStatusAfterResupply;
   @ConfigurationElement(
      name = "MinimumResourceMining",
      description = "Minimum mining factor (for raw resources)"
   )
   public float minimumResourceMining;
   @ConfigurationElement(
      name = "ResourcesPerMinerScore",
      description = "Multiplies score of all available Miners in a System with their mining score. Then that is multiplied with the mining factor (between 0 and 1 depending on resource richness of the system)"
   )
   public float resourcesPerMinerScore;
   @ConfigurationElement(
      name = "MinedResourcesAddedFromMisc",
      description = "Percentage of mined misc materials (flowers, stone, etc) to be added to mined resources (indication for mined-out system)"
   )
   public float minedResourcesAddedFromMisc;
   @ConfigurationElement(
      name = "ResourceAvaililityMultiplicator",
      description = "How much resources is richness worth. Basically the simulated amount of resources in a system in combination with richness"
   )
   public float resourceAvaililityMultiplicator;
   @ConfigurationElement(
      name = "BasicMiningScorePerSystem",
      description = "Basic mining score per system"
   )
   public double basicMiningScorePerSystem;
   @ConfigurationElement(
      name = "MiningStationMiningScore",
      description = "A mining station's mining score"
   )
   public double miningStationMiningScore;
   @ConfigurationElement(
      name = "ResourcesConsumeStep",
      description = "Step size to consume per turn (added per turn until sum is 1)"
   )
   public float resourcesConsumeStep;
   @ConfigurationElement(
      name = "ResourcesConsumedPerDistance",
      description = "Step size to consume per turn (added per turn until sum is 1)"
   )
   public float resourcesConsumedPerDistance;
   @ConfigurationElement(
      name = "ReplenishResourceRate",
      description = "Percentage of Minable Resources in system replenished per turn"
   )
   public float replenishResourceRate;
   @ConfigurationElement(
      name = "MinimumMinableResources",
      description = "Minimum Resource percentage in system on which faction will do mining"
   )
   public float minimumMinableResources;
   @ConfigurationElement(
      name = "InitialGrowBaseDefault",
      description = "(only used if setting in spawn config is -1) Fixed minimum initial grown systems (additional to home system)"
   )
   public int initialGrowBaseDefault;
   @ConfigurationElement(
      name = "InitialGrowAddedDefaultRandom",
      description = "(only used if setting in spawn config is -1) Randomly added initial grown systems (additional to home system and minimum fixed amount)"
   )
   public int initialGrowAddedDefaultRandom;
   @ConfigurationElement(
      name = "TimeBetweenTurnsMS",
      description = "Time between faction turns in milliseconds"
   )
   public long timeBetweenTurnsMS;
   @ConfigurationElement(
      name = "Diplomacy"
   )
   public DiplomacyConfig diplomacy;
   @ConfigurationElement(
      name = "DiplomacyStatusCheckDelay",
      description = "Check for status effects (war, etc)"
   )
   public long diplomacyStatusCheckDelay;
   @ConfigurationElement(
      name = "DiplomacyTurnEffectDelay",
      description = "Time for turn effects to apply to points"
   )
   public long diplomacyTurnEffectDelay;
   @ConfigurationElement(
      name = "DiplomacyTurnEffectChangeDelay",
      description = "Time for turn effects to change (falloff or get stronger depending on direction)"
   )
   public long diplomacyTurnEffectChangeDelay;
   @ConfigurationElement(
      name = "FleetClasses"
   )
   public final NPCFactionConfig.FleetClasses fleetClasses;
   @ConfigurationElement(
      name = "DiplomacyStartPoints"
   )
   public int diplomacyStartPoints;
   @ConfigurationElement(
      name = "DiplomacyMaxPoints"
   )
   public int diplomacyMaxPoints;
   @ConfigurationElement(
      name = "DiplomacyMinPoints"
   )
   public int diplomacyMinPoints;
   @ConfigurationElement(
      name = "MinimumTradeValue"
   )
   public int minimumTradeValue;
   @ConfigurationElement(
      name = "AmountOfBlocksToTradeMax"
   )
   public int amountOfBlocksToTradeMax;
   @ConfigurationElement(
      name = "ProductionMultiplier"
   )
   public int productionMultiplier;
   @ConfigurationElement(
      name = "ProductionStepts"
   )
   public int productionStepts;
   @ConfigurationElement(
      name = "DoesAbandonSystems",
      description = "Should faction ever lose any system"
   )
   public boolean doesAbandonSystems;
   @ConfigurationElement(
      name = "DoesAbandonHome",
      description = "Should faction ever lose its home system (DoesAbandonSystems must be true for this to be considered)"
   )
   public boolean doesAbandonHome;
   @ConfigurationElement(
      name = "MinDefendFleetCount",
      description = "Minimum fleets per system"
   )
   public int minDefendFleetCount;
   @ConfigurationElement(
      name = "MaxDefendFleetCount",
      description = "Maximum fleets per system"
   )
   public int maxDefendFleetCount;
   @ConfigurationElement(
      name = "DefendFleetsPerStation",
      description = "Fleets per station"
   )
   public float defendFleetsPerStation;
   @ConfigurationElement(
      name = "MaxMiningFleetCount",
      description = "Maximum fleets per system"
   )
   public int maxMiningFleetCount;
   @ConfigurationElement(
      name = "MiningShipsPerFleet",
      description = "Amount of mining contingent to put in a mining fleet (no mining fleets without any contingent)"
   )
   public float miningShipsPerFleet;
   @ConfigurationElement(
      name = "CanAttackStations",
      description = ""
   )
   public boolean canAttackStations;
   @ConfigurationElement(
      name = "CanAttackShips",
      description = ""
   )
   public boolean canAttackShips;
   @ConfigurationElement(
      name = "DamageTaken",
      description = ""
   )
   public float damageTaken;
   @ConfigurationElement(
      name = "DamageGiven",
      description = ""
   )
   public float damageGiven;
   @ConfigurationElement(
      name = "MinScavenginRange",
      description = "Range in systems"
   )
   public float minScavenginRange;
   @ConfigurationElement(
      name = "ScavengingRangePerFactionLevel",
      description = "(level is max distance of territory in one dimension)"
   )
   public float scavengingRangePerFactionLevel;
   @ConfigurationElement(
      name = "ScvengingFleetsPerTurn",
      description = "Amount of sectors to target with scavengin fleets (sectors where an attack happened in range)"
   )
   public int scvengingFleetsPerTurn;
   @ConfigurationElement(
      name = "DefendFleetsToSendToAttacker",
      description = "Percent of defend fleets of total defend fleets available to send to sector in which attack happens. This is only called once every 5 minutes per sector. Already close fleets are not considered"
   )
   public float defendFleetsToSendToAttacker;
   @ConfigurationElement(
      name = "MinDefendFleetsToSendToAttacker",
      description = "Minimum fleets to send to an attack in the system"
   )
   public int minDefendFleetsToSendToAttacker;
   @ConfigurationElement(
      name = "TradingMinDefenseShips",
      description = "minimum defense/attack ships (if existing in the fleet type) to send on a trading mission"
   )
   public int tradingMinDefenseShips;
   @ConfigurationElement(
      name = "TradingCargoMassVersusDefenseMass",
      description = "defense/attack ships to send compared to mass of cargo (more cargo, more ships) to send on a trding mission"
   )
   public double tradingCargoMassVersusDefenseMass;
   @ConfigurationElement(
      name = "TradingMaxDefenseShipsPerCargo",
      description = "maximum defense/attack ships per cargo ship to send on a trding mission"
   )
   public int tradingMaxDefenseShipsPerCargo;
   private static byte VERSION = 0;

   public NPCFactionConfig() {
      this.baseTypeCount = new int[ElementKeyMap.highestType + 1];
      this.randomTypeCount = new int[ElementKeyMap.highestType + 1];
      this.defaultBaseTypeCountRaw = 3;
      this.defaultRandomTypeCountRaw = 10;
      this.defaultBaseTypeCountLevel1 = 2;
      this.defaultRandomTypeCountLevel1 = 6;
      this.defaultBaseTypeCountLevel2 = 2;
      this.defaultRandomTypeCountLevel2 = 4;
      this.defaultBaseTypeCountLevel3 = 1;
      this.defaultRandomTypeCountLevel3 = 5;
      this.typeWeightsWithProduction = new Short2FloatOpenHashMap();
      this.typeWeights = new Short2FloatOpenHashMap();
      this.minimumNormalizationModifier = 0.1F;
      this.levelsFilled = 2;
      this.bta = new NPCFactionConfig.BlueprintTypeAvailability();
      this.productionRawConversion = 0.5F;
      this.demandMultPerSystem = 0.01F;
      this.minDemandPerSystem = 500;
      this.tradeDemandMult = 3.0F;
      this.expensionDemandMult = 0.2F;
      this.expensionDemandConsumeMult = 0.75F;
      this.tradeBuyLowerPercentage = 0.0F;
      this.tradeBuyUpperPercentage = 0.6F;
      this.tradeBuyLowerPricePercentage = 0.95F;
      this.tradeBuyUpperPricePercentage = 1.1F;
      this.tradeSellLowerPercentage = 0.4F;
      this.tradeSellUpperPercentage = 1.0F;
      this.tradeSellLowerPricePercentage = 0.9F;
      this.tradeSellUpperPricePercentage = 1.05F;
      this.tradePricePerSystem = 0.005F;
      this.maxOwnCreditsToUseForTrade = 0.35F;
      this.maxOtherCreditsToUseForTrade = 0.75F;
      this.abandonSystemOnStatus = 0.25D;
      this.abandonSystemOnStatusAfterResupply = 0.6D;
      this.minimumResourceMining = 0.01F;
      this.resourcesPerMinerScore = 100.0F;
      this.minedResourcesAddedFromMisc = 0.1F;
      this.resourceAvaililityMultiplicator = 1000.0F;
      this.basicMiningScorePerSystem = 300.0D;
      this.miningStationMiningScore = 10000.0D;
      this.resourcesConsumeStep = 0.05F;
      this.resourcesConsumedPerDistance = 0.2F;
      this.replenishResourceRate = 0.02F;
      this.minimumMinableResources = 0.1F;
      this.initialGrowBaseDefault = 0;
      this.initialGrowAddedDefaultRandom = 5;
      this.timeBetweenTurnsMS = 900000L;
      this.diplomacy = new DiplomacyConfig();
      this.diplomacyStatusCheckDelay = 60000L;
      this.diplomacyTurnEffectDelay = 60000L;
      this.diplomacyTurnEffectChangeDelay = 60000L;
      this.fleetClasses = new NPCFactionConfig.FleetClasses();
      this.diplomacyStartPoints = 0;
      this.diplomacyMaxPoints = 5000;
      this.diplomacyMinPoints = -5000;
      this.minimumTradeValue = 10;
      this.amountOfBlocksToTradeMax = 50000;
      this.productionMultiplier = 1000;
      this.productionStepts = 10;
      this.doesAbandonSystems = true;
      this.doesAbandonHome = false;
      this.minDefendFleetCount = 1;
      this.maxDefendFleetCount = 5;
      this.defendFleetsPerStation = 0.3F;
      this.maxMiningFleetCount = 5;
      this.miningShipsPerFleet = 5.0F;
      this.canAttackStations = true;
      this.canAttackShips = true;
      this.damageTaken = 1.0F;
      this.damageGiven = 1.0F;
      this.minScavenginRange = 7.0F;
      this.scavengingRangePerFactionLevel = 2.0F;
      this.scvengingFleetsPerTurn = 3;
      this.defendFleetsToSendToAttacker = 0.5F;
      this.minDefendFleetsToSendToAttacker = 1;
      this.tradingMinDefenseShips = 1;
      this.tradingCargoMassVersusDefenseMass = 0.75D;
      this.tradingMaxDefenseShipsPerCargo = 5;
      this.typeWeightsWithProduction.defaultReturnValue(0.0F);
   }

   public void initialize() {
      this.bta.create();
   }

   public void getWeightedContingent(int var1, int var2, float var3, NPCEntityContingent var4) throws EntityNotFountException {
      this.getWeightedContingent(var1, var2, var3, SimpleTransformableSendableObject.EntityType.SHIP, Math.max(1.0F, this.shipCountModifier), var4);
      this.getWeightedContingent(var1, var2, var3, SimpleTransformableSendableObject.EntityType.SPACE_STATION, Math.max(1.0F, this.stationCountModifier), var4);
   }

   private void getWeightedContingent(int var1, int var2, float var3, SimpleTransformableSendableObject.EntityType var4, float var5, NPCEntityContingent var6) throws EntityNotFountException {
      Object2IntOpenHashMap var7 = new Object2IntOpenHashMap();
      Object2IntOpenHashMap var8 = new Object2IntOpenHashMap();

      assert this.homeSystemEntitiesStart > 0;

      int var12 = (int)((float)this.homeSystemEntitiesStart * var5 * var3);
      this.getWeightedEntityCount(var1, var12, this.inner, var4, var8);
      this.getWeightedEntityCount(var1, var12, this.outer, var4, var7);
      var3 = Math.min(1.0F, NPCSystemStructure.getLevelWeight(var1) * (float)this.levelsFilled);
      float var10 = (float)var1 / (float)var2;
      Iterator var11 = var7.keySet().iterator();

      while(var11.hasNext()) {
         String var13 = (String)var11.next();
         int var14 = var8.getInt(var13);
         float var9 = (float)(var7.getInt(var13) - var14);
         var14 = Math.max(1, (int)(((float)var14 + var9 * var10) * var3));
         BlueprintEntry var15 = this.getPreset().blueprintController.getBlueprint(var13);
         var6.add(var13, var15.getEntityType().type, (float)var15.getMass(), var15.getClassification(), var14, var15.getElementCountMapWithChilds());
      }

   }

   private void getWeightedEntityCount(int var1, int var2, NPCFactionConfig.EntityPriority var3, SimpleTransformableSendableObject.EntityType var4, Object2IntOpenHashMap var5) {
      assert this.bta.created : "Faction config not initialized";

      float var14 = 0.0F;
      Iterator var6 = this.bta.mp.entrySet().iterator();

      Entry var7;
      while(var6.hasNext()) {
         if (((BlueprintClassification)(var7 = (Entry)var6.next()).getKey()).type == var4) {
            float var8 = var3.getVal((BlueprintClassification)var7.getKey());
            var14 += var8;
         }
      }

      var6 = this.bta.mp.entrySet().iterator();

      while(true) {
         do {
            if (!var6.hasNext()) {
               return;
            }
         } while(((BlueprintClassification)(var7 = (Entry)var6.next()).getKey()).type != var4);

         int var15 = (int)(var3.getVal((BlueprintClassification)var7.getKey()) / var14 * (float)var2);
         Iterator var9 = ((NPCFactionConfig.BlueprintTypeAvailability.BlueprintTypeScale)var7.getValue()).entries.iterator();

         while(var9.hasNext()) {
            NPCFactionConfig.BlueprintTypeAvailability.BlueprintTypeScale.BBScale var10 = (NPCFactionConfig.BlueprintTypeAvailability.BlueprintTypeScale.BBScale)var9.next();
            float var11 = 0.0F;

            NPCFactionConfig.BlueprintTypeAvailability.BlueprintTypeScale.BBScale var13;
            for(Iterator var12 = ((NPCFactionConfig.BlueprintTypeAvailability.BlueprintTypeScale)var7.getValue()).entries.iterator(); var12.hasNext(); var11 += var3.getWeight((BlueprintClassification)var7.getKey(), var13.normalizedScale)) {
               var13 = (NPCFactionConfig.BlueprintTypeAvailability.BlueprintTypeScale.BBScale)var12.next();
            }

            int var16 = (int)(var3.getWeight((BlueprintClassification)var7.getKey(), var10.normalizedScale) / var11 * (float)var15);
            var5.add(var10.name, var16);
         }
      }
   }

   public static void main(String[] var0) throws IllegalArgumentException, IllegalAccessException, ConfigParserException, IOException, SAXException, ParserConfigurationException {
      recreate = true;
      NPCFactionConfig var5 = new NPCFactionConfig();
      writeDocument(new File("./data/npcFactions/npcConfigDefault.xml"), var5);
      File[] var6;
      int var1 = (var6 = (new File("./data/npcFactions/")).listFiles()).length;

      for(int var2 = 0; var2 < var1; ++var2) {
         File var3;
         if ((var3 = var6[var2]).isDirectory()) {
            File var4;
            if ((var4 = new File(var3, "npcConfig.xml")).exists()) {
               System.err.println("WRITING TESTING " + var4.getAbsolutePath());
               NPCFactionPreset var7 = NPCFactionPreset.readFromDirOnlyConf(var3.getAbsolutePath());
               (new NPCFactionConfig()).setPreset(var7);
            } else {
               System.err.println("ERROR NO NPC CONFIG FOUND " + var4.getAbsolutePath());
            }
         }
      }

   }

   public String getSystemStationBlueprintName() {
      Iterator var1 = this.getPreset().blueprintController.readBluePrints().iterator();

      BlueprintEntry var2;
      do {
         if (!var1.hasNext()) {
            return null;
         }
      } while((var2 = (BlueprintEntry)var1.next()).getType().type != SimpleTransformableSendableObject.EntityType.SPACE_STATION);

      return var2.getName();
   }

   public void generate(LogInterface var1) {
      this.createWeightTableFromBlueprints();
      Iterator var2 = ElementKeyMap.keySet.iterator();

      while(true) {
         while(var2.hasNext()) {
            short var3 = (Short)var2.next();
            var1.log(String.format("WEIGHT          : %-40s %f", ElementKeyMap.toString(var3), this.typeWeights.get(var3)), LogInterface.LogLevel.DEBUG);
            var1.log(String.format("WEIGHT WITH PROD: %-40s %f", ElementKeyMap.toString(var3), this.typeWeightsWithProduction.get(var3)), LogInterface.LogLevel.DEBUG);
            ElementInformation var4;
            if (!(var4 = ElementKeyMap.getInfoFast(var3)).isOre() && !var4.isCapsule() && (!var4.isShoppable() || !var4.getConsistence().isEmpty())) {
               int var5 = 0;
               int var6 = 1;
               switch(var4.getProducedInFactoryType()) {
               case 211:
                  var5 = this.defaultBaseTypeCountLevel1;
                  var6 = this.defaultRandomTypeCountLevel1;
                  break;
               case 213:
                  var5 = this.defaultBaseTypeCountRaw;
                  var6 = this.defaultRandomTypeCountRaw;
                  break;
               case 215:
                  var5 = this.defaultBaseTypeCountRaw;
                  var6 = this.defaultRandomTypeCountRaw;
                  break;
               case 217:
                  var5 = this.defaultBaseTypeCountLevel2;
                  var6 = this.defaultRandomTypeCountLevel2;
                  break;
               case 259:
                  var5 = this.defaultBaseTypeCountLevel3;
                  var6 = this.defaultRandomTypeCountLevel3;
               }

               this.baseTypeCount[var3] = var5;
               this.randomTypeCount[var3] = var6;
            } else {
               this.baseTypeCount[var3] = this.defaultBaseTypeCountRaw;
               this.randomTypeCount[var3] = this.defaultRandomTypeCountRaw;
            }
         }

         return;
      }
   }

   private void createWeightTableFromBlueprints() {
      List var1 = this.getPreset().blueprintController.readBluePrints();
      long var2 = 0L;

      BlueprintEntry var5;
      for(Iterator var4 = var1.iterator(); var4.hasNext(); var2 = Math.max(var2, var5.getElementCountMapWithChilds().getTotalAmount())) {
         var5 = (BlueprintEntry)var4.next();
      }

      ElementCountMap var10 = new ElementCountMap();
      Iterator var8 = var1.iterator();

      while(var8.hasNext()) {
         BlueprintEntry var9 = (BlueprintEntry)var8.next();
         double var6 = Math.max((double)this.minimumNormalizationModifier, (double)var9.getElementCountMapWithChilds().getTotalAmount() / (double)var2);
         var10.add(var9.getElementCountMapWithChilds(), var6);
      }

      this.totalElements = var10.getTotalAmount();
      var10.getWeights(this.typeWeights);
      var10.spikeWithProduction();
      var10.getWeights(this.typeWeightsWithProduction);
   }

   public int getDemandAmount(short var1) {
      return (int)Math.min(2147483647L, (long)((double)this.typeWeights.get(var1) * (double)this.totalElements * (double)this.homeSystemEntitiesStart));
   }

   public int getInitialAmount(short var1, Random var2, long var3) {
      var2.setSeed(var3 * 234234222L + (long)var1);
      return !ElementKeyMap.isOre(var1) && !ElementKeyMap.isShard(var1) ? (int)((double)(this.baseTypeCount[var1] + (this.randomTypeCount[var1] > 0 ? var2.nextInt(this.randomTypeCount[var1]) : 0)) * (double)this.getWeightedAmountWithProduction(var1)) : 10000 + var2.nextInt(10000);
   }

   public int getWeightedAmountWithProduction(short var1) {
      return (int)Math.min(2147483647L, (long)((double)this.typeWeightsWithProduction.get(var1) * (double)this.totalElements * (double)this.homeSystemEntitiesStart));
   }

   public void getWeightedDemand(ElementCountMap var1, float var2) {
      Iterator var3 = this.typeWeights.entrySet().iterator();

      while(var3.hasNext()) {
         Entry var4 = (Entry)var3.next();
         var1.inc((Short)var4.getKey(), (int)((float)this.getWeightedAmountWithProduction((Short)var4.getKey()) * var2));
      }

   }

   public int getBuyPrice(float var1, long var2) {
      return this.getPrice(var1, var2, this.tradeBuyLowerPercentage, this.tradeBuyUpperPercentage, this.tradeBuyLowerPricePercentage, this.tradeBuyUpperPricePercentage, true);
   }

   public int getSellPrice(float var1, long var2) {
      return this.getPrice(var1, var2, this.tradeSellLowerPercentage, this.tradeSellUpperPercentage, this.tradeSellLowerPricePercentage, this.tradeSellUpperPricePercentage, false);
   }

   public int getPrice(float var1, long var2, float var4, float var5, float var6, float var7, boolean var8) {
      int var9 = 0;
      if ((var1 >= var4 || var8) && (var1 <= var5 || !var8)) {
         if (var8) {
            var1 = Math.max(var4, var1);
         } else {
            var1 = Math.min(var5, var1);
         }

         var5 -= var4;
         var1 = (var1 - var4) / var5;
         var4 = var7 - var6;
         var1 = var7 - var4 * var1;
         var9 = (int)Math.round((double)var2 * (double)var1);
      }

      return var9;
   }

   public NPCFactionPreset getPreset() {
      return this.preset;
   }

   public void setPreset(NPCFactionPreset var1) throws IllegalArgumentException, IllegalAccessException, ConfigParserException {
      this.preset = var1;

      try {
         this.parse(var1.doc);
      } catch (RuntimeException var3) {
         throw new ConfigParserException("EXCEPTION WHEN PARSING FACTION CONFIG FOR: " + var1.factionPresetName, var3);
      }
   }

   public void fromTagStructure(Tag var1, NPCFactionPresetManager var2) throws IllegalArgumentException, IllegalAccessException, ConfigParserException {
      Tag[] var5;
      (var5 = var1.getStruct())[0].getByte();
      String var6 = var5[1].getString();
      Iterator var3 = var2.getNpcPresets().iterator();

      while(var3.hasNext()) {
         NPCFactionPreset var4;
         if ((var4 = (NPCFactionPreset)var3.next()).factionPresetName.equals(var6)) {
            this.setPreset(var4);
            break;
         }
      }

      if (this.preset == null) {
         this.setPreset(var2.getRandomLeastUsedPreset(Universe.getRandom().nextLong(), var6.hashCode()));
      }

   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, VERSION), new Tag(Tag.Type.STRING, (String)null, this.preset.factionPresetName), new Tag(Tag.Type.FINISH, (String)null, (Tag[])null)});
   }

   public void create(Document var1) throws DOMException, IllegalArgumentException, IllegalAccessException {
      Element var2 = var1.createElement("NPCFactionConfig");
      var1.appendChild(var2);
      Element var3;
      (var3 = var1.createElement("Version")).setTextContent("0");
      Comment var4 = var1.createComment("autocreated");
      var3.appendChild(var4);
      var2.appendChild(var3);
      var3 = var1.createElement("Config");
      var2.appendChild(var3);
      Field[] var9;
      int var10 = (var9 = this.getClass().getDeclaredFields()).length;

      for(int var5 = 0; var5 < var10; ++var5) {
         Field var6;
         (var6 = var9[var5]).setAccessible(true);
         ConfigurationElement var7;
         if ((var7 = (ConfigurationElement)var6.getAnnotation(ConfigurationElement.class)) != null) {
            Element var8 = var1.createElement(var7.name());
            if (var6.getType().equals(NPCFactionConfig.FleetClasses.class)) {
               ((NPCFactionConfig.FleetClasses)var6.get(this)).appendXML(var1, var8);
            } else if (var6.getType().equals(DiplomacyConfig.class)) {
               ((DiplomacyConfig)var6.get(this)).appendXML(var1, var8);
            } else if (var6.getType().equals(NPCFactionConfig.EntityPriority.class)) {
               ((NPCFactionConfig.EntityPriority)var6.get(this)).appendXML(var1, var8);
            } else {
               var8.setTextContent(var6.get(this).toString());
            }

            if (var7.description() != null && var7.description().trim().length() > 0) {
               Comment var11 = var1.createComment(var7.description());
               var8.appendChild(var11);
            }

            var3.appendChild(var8);
         }
      }

   }

   public static File writeDocument(File var0, NPCFactionConfig var1) {
      try {
         Document var2 = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
         var1.create(var2);
         var2.setXmlVersion("1.0");
         TransformerFactory var10000 = TransformerFactory.newInstance();
         var1 = null;
         Transformer var5;
         (var5 = var10000.newTransformer()).setOutputProperty("omit-xml-declaration", "yes");
         var5.setOutputProperty("indent", "yes");
         var5.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
         new StringWriter();
         StreamResult var3 = new StreamResult(var0);
         DOMSource var6 = new DOMSource(var2);
         var5.transform(var6, var3);
         return var0;
      } catch (Exception var4) {
         var4.printStackTrace();
         return null;
      }
   }

   public void parse(Document var1) throws IllegalArgumentException, IllegalAccessException, ConfigParserException {
      NodeList var21 = var1.getDocumentElement().getChildNodes();
      Field[] var2 = this.getClass().getDeclaredFields();
      boolean var3 = false;
      ObjectOpenHashSet var4 = new ObjectOpenHashSet();

      int var8;
      for(int var5 = 0; var5 < var21.getLength(); ++var5) {
         Node var6;
         if ((var6 = var21.item(var5)).getNodeType() == 1 && var6.getNodeName().toLowerCase(Locale.ENGLISH).equals("version")) {
            try {
               Byte.parseByte(var6.getTextContent());
            } catch (Exception var19) {
               var19.printStackTrace();
               throw new ConfigParserException("malformed version in xml", var19);
            }
         } else if (var6.getNodeType() == 1 && var6.getNodeName().toLowerCase(Locale.ENGLISH).equals(this.getTag().toLowerCase(Locale.ENGLISH))) {
            NodeList var7 = var6.getChildNodes();
            var3 = true;

            for(var8 = 0; var8 < var7.getLength(); ++var8) {
               Node var9;
               if ((var9 = var7.item(var8)).getNodeType() == 1) {
                  boolean var10 = false;
                  boolean var11 = false;
                  Field[] var12 = var2;
                  int var13 = var2.length;

                  for(int var14 = 0; var14 < var13; ++var14) {
                     Field var15;
                     (var15 = var12[var14]).setAccessible(true);
                     ConfigurationElement var16;
                     if ((var16 = (ConfigurationElement)var15.getAnnotation(ConfigurationElement.class)) != null) {
                        var10 = true;
                        if (var16.name().toLowerCase(Locale.ENGLISH).equals(var9.getNodeName().toLowerCase(Locale.ENGLISH))) {
                           try {
                              if (var15.getType() == Boolean.TYPE) {
                                 var15.setBoolean(this, Boolean.parseBoolean(var9.getTextContent()));
                                 var11 = true;
                              } else if (var15.getType() == Integer.TYPE) {
                                 var15.setInt(this, Integer.parseInt(var9.getTextContent()));
                                 var11 = true;
                              } else if (var15.getType() == Short.TYPE) {
                                 var15.setShort(this, Short.parseShort(var9.getTextContent()));
                                 var11 = true;
                              } else if (var15.getType() == Byte.TYPE) {
                                 var15.setByte(this, Byte.parseByte(var9.getTextContent()));
                                 var11 = true;
                              } else if (var15.getType() == Float.TYPE) {
                                 var15.setFloat(this, Float.parseFloat(var9.getTextContent()));
                                 var11 = true;
                              } else if (var15.getType() == Double.TYPE) {
                                 var15.setDouble(this, Double.parseDouble(var9.getTextContent()));
                                 var11 = true;
                              } else if (var15.getType() == Long.TYPE) {
                                 var15.setLong(this, Long.parseLong(var9.getTextContent()));
                                 var11 = true;
                              } else if (var15.getType().equals(NPCFactionConfig.EntityPriority.class)) {
                                 ((NPCFactionConfig.EntityPriority)var15.get(this)).parse(var9);
                                 var11 = true;
                              } else if (var15.getType().equals(DiplomacyConfig.class)) {
                                 DiplomacyConfig var26;
                                 (var26 = (DiplomacyConfig)var15.get(this)).parse(var9);
                                 if (!(var11 = var26.version == 2)) {
                                    var26.reactions.clear();
                                    var26.addDefaultActions();
                                 }
                              } else if (var15.getType().equals(NPCFactionConfig.FleetClasses.class)) {
                                 ((NPCFactionConfig.FleetClasses)var15.get(this)).parse(var9);
                                 var11 = true;
                              } else {
                                 if (!var15.getType().equals(String.class)) {
                                    throw new ConfigParserException("Cannot parse field: " + var15.getName() + "; " + var15.getType());
                                 }

                                 String var27 = var9.getTextContent().replaceAll("\\r\\n|\\r|\\n", "").replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r").replaceAll("\\\\t", "\t");
                                 var15.set(this, var27);
                                 var11 = true;
                              }
                           } catch (NumberFormatException var18) {
                              throw new ConfigParserException("Cannot parse field: " + var15.getName() + "; " + var15.getType() + "; with " + var9.getTextContent(), var18);
                           }
                        }

                        if (var11) {
                           var4.add(var15);
                           break;
                        }
                     }
                  }

                  if (var10 && !var11) {
                     try {
                        throw new ConfigParserException(var6.getNodeName() + " -> " + var9.getNodeName() + ": No appropriate field found for tag: " + var9.getNodeName() + "\nVALUE WILL BE OMITTED");
                     } catch (ConfigParserException var20) {
                        var20.printStackTrace();
                     }
                  }
               }
            }
         }
      }

      if (!var3) {
         throw new ConfigParserException("Block module Tag \"" + this.getTag() + "\" not found in block behavior configuation. Please create it (case insensitive)");
      } else {
         this.getClass().getAnnotations();
         Field[] var22 = var2;
         int var23 = var2.length;

         for(var8 = 0; var8 < var23; ++var8) {
            Field var24;
            (var24 = var22[var8]).setAccessible(true);
            ConfigurationElement var25;
            if ((var25 = (ConfigurationElement)var24.getAnnotation(ConfigurationElement.class)) != null && !var4.contains(var24)) {
               try {
                  throw new ConfigParserException("virtual field " + var24.getName() + " (" + var25.name() + ") not found. Please define a tag \"" + var25.name() + "\" inside  of \"" + this.getTag() + "\"");
               } catch (Exception var17) {
                  var17.printStackTrace();
                  System.err.println("CATCHED EXCEPTION. WRITING MISSING TAGS TO " + this.getPreset().confFile.getAbsolutePath());
                  writeDocument(this.getPreset().confFile, this);
                  return;
               }
            }
         }

      }
   }

   private String getTag() {
      return "Config";
   }

   public boolean existsAction(DiplomacyAction.DiplActionType var1) {
      return this.diplomacy.get(var1) != null;
   }

   public boolean existsStatus(NPCDiplomacyEntity.DiplStatusType var1) {
      return this.diplomacy.get(var1) != null;
   }

   public int getDiplomacyUpperLimit(DiplomacyAction.DiplActionType var1) {
      return this.diplomacy.get(var1).upperLimit;
   }

   public int getDiplomacyLowerLimit(DiplomacyAction.DiplActionType var1) {
      return this.diplomacy.get(var1).lowerLimit;
   }

   public int getDiplomacyValue(DiplomacyAction.DiplActionType var1) {
      return this.diplomacy.get(var1).value;
   }

   public int getDiplomacyValue(NPCDiplomacyEntity.DiplStatusType var1) {
      return this.diplomacy.get(var1).value;
   }

   public DiplomacyReaction getDiplomacyActionRequired(DiplomacyAction.DiplActionType var1) {
      return this.diplomacy.get(var1).reaction;
   }

   public int getDiplomacyExistingActionModifier(DiplomacyAction.DiplActionType var1) {
      return this.diplomacy.get(var1).existingModifier;
   }

   public int getDiplomacyNonExistingActionModifier(DiplomacyAction.DiplActionType var1) {
      return this.diplomacy.get(var1).nonExistingModifier;
   }

   public long getDiplomacyTurnTimeout(DiplomacyAction.DiplActionType var1) {
      return (long)((double)this.diplomacy.get(var1).turnsActionDuration * (double)this.timeBetweenTurnsMS);
   }

   public long getDiplomacyStaticTimpout(NPCDiplomacyEntity.DiplStatusType var1) {
      return (long)((double)this.diplomacy.get(var1).staticTimeoutTurns * (double)this.timeBetweenTurnsMS);
   }

   public BlueprintClassification[] getFleetClasses(NPCSystemFleetManager.FleetType var1) {
      return (BlueprintClassification[])this.fleetClasses.classes.get(var1);
   }

   public List getDiplomacyReactions() {
      return this.diplomacy.reactions;
   }

   public long getDiplomacyActionTimeout(DiplomacyAction.DiplActionType var1) {
      return this.diplomacy.actionTimeoutMap.containsKey(var1) ? this.diplomacy.actionTimeoutMap.getLong(var1) : this.diplomacyTurnEffectChangeDelay * 3L;
   }

   public class FleetClasses {
      Object2ObjectOpenHashMap classes = new Object2ObjectOpenHashMap();

      public FleetClasses() {
         NPCSystemFleetManager.FleetType[] var5;
         int var2 = (var5 = NPCSystemFleetManager.FleetType.values()).length;

         for(int var3 = 0; var3 < var2; ++var3) {
            NPCSystemFleetManager.FleetType var4 = var5[var3];
            switch(var4) {
            case DEFENDING:
               this.classes.put(var4, new BlueprintClassification[]{BlueprintClassification.NONE, BlueprintClassification.DEFENSE, BlueprintClassification.ATTACK});
               break;
            default:
               this.classes.put(var4, new BlueprintClassification[]{BlueprintClassification.NONE});
            }
         }

      }

      public void appendXML(Document var1, Node var2) {
         NPCSystemFleetManager.FleetType[] var3;
         int var4 = (var3 = NPCSystemFleetManager.FleetType.values()).length;

         for(int var5 = 0; var5 < var4; ++var5) {
            NPCSystemFleetManager.FleetType var6 = var3[var5];
            BlueprintClassification[] var7 = (BlueprintClassification[])this.classes.get(var6);
            Element var8 = var1.createElement("FleetClass");
            Element var9 = var1.createElement("Type");
            Element var10 = var1.createElement("Classes");
            var8.appendChild(var1.createComment("Blueprint classes going to be added to a fleet of this class in looping order"));
            var8.appendChild(var9);
            var8.appendChild(var10);
            var9.setTextContent(var6.name());
            StringBuffer var11 = new StringBuffer();

            for(int var12 = 0; var12 < var7.length; ++var12) {
               var11.append(var7[var12].name());
               if (var12 < var7.length - 1) {
                  var11.append(", ");
               }
            }

            var10.setTextContent(var11.toString());
            var2.appendChild(var8);
         }

      }

      public void parse(Node var1) throws ConfigParserException {
         NodeList var11 = var1.getChildNodes();

         for(int var2 = 0; var2 < var11.getLength(); ++var2) {
            Node var3;
            if ((var3 = var11.item(var2)).getNodeType() == 1 && var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("fleetclass")) {
               NodeList var4 = var3.getChildNodes();
               NPCSystemFleetManager.FleetType var5 = null;
               BlueprintClassification[] var6 = null;

               for(int var7 = 0; var7 < var4.getLength(); ++var7) {
                  Node var8;
                  if ((var8 = var4.item(var7)).getNodeType() == 1 && var8.getNodeName().toLowerCase(Locale.ENGLISH).equals("type")) {
                     var5 = NPCSystemFleetManager.FleetType.valueOf(var8.getTextContent());
                  } else if (var8.getNodeType() == 1 && var8.getNodeName().toLowerCase(Locale.ENGLISH).equals("classes")) {
                     String[] var9;
                     if ((var9 = var8.getTextContent().split(",")).length <= 0) {
                        throw new ConfigParserException("Blueprint Classification Classes Empty for fleet type; " + var8.getNodeName() + "; " + var3.getNodeName() + "; Content: " + var8.getTextContent() + "; parse: " + Arrays.toString(var9));
                     }

                     var6 = new BlueprintClassification[var9.length];

                     for(int var10 = 0; var10 < var9.length; ++var10) {
                        var6[var10] = BlueprintClassification.valueOf(var9[var10].trim().toUpperCase(Locale.ENGLISH));
                        if (var6[var10] == null) {
                           throw new ConfigParserException("Blueprint Classification unknown: " + var9[var2] + "; " + var8.getNodeName() + "; " + var3.getNodeName());
                        }
                     }
                  }
               }

               if (var5 == null) {
                  throw new ConfigParserException("Missing 'Type' node; " + var3.getNodeName() + "; " + var3.getParentNode().getNodeName());
               }

               if (var6 == null) {
                  throw new ConfigParserException("Missing 'Classes' node; " + var3.getNodeName() + "; " + var3.getParentNode().getNodeName());
               }

               this.classes.put(var5, var6);
            }
         }

      }
   }

   public class BlueprintTypeAvailability {
      final Object2ObjectOpenHashMap mp = new Object2ObjectOpenHashMap();
      private boolean created;

      void create() {
         if (!this.created) {
            assert NPCFactionConfig.this.getPreset() != null;

            assert NPCFactionConfig.this.getPreset().blueprintController != null;

            Iterator var1;
            BlueprintEntry var2;
            NPCFactionConfig.BlueprintTypeAvailability.BlueprintTypeScale var4;
            for(var1 = NPCFactionConfig.this.getPreset().blueprintController.readBluePrints().iterator(); var1.hasNext(); var4.add(var2.getName(), (float)var2.getMass())) {
               BlueprintClassification var3 = (var2 = (BlueprintEntry)var1.next()).getClassification();
               if ((var4 = (NPCFactionConfig.BlueprintTypeAvailability.BlueprintTypeScale)this.mp.get(var3)) == null) {
                  var4 = new NPCFactionConfig.BlueprintTypeAvailability.BlueprintTypeScale();
                  this.mp.put(var3, var4);
               }
            }

            var1 = this.mp.values().iterator();

            while(var1.hasNext()) {
               ((NPCFactionConfig.BlueprintTypeAvailability.BlueprintTypeScale)var1.next()).calcNormal();
            }

            this.created = true;
         }

      }

      class BlueprintTypeScale {
         private List entries;
         private int totalMass;

         private BlueprintTypeScale() {
            this.entries = new ObjectArrayList();
         }

         void add(String var1, float var2) {
            NPCFactionConfig.BlueprintTypeAvailability.BlueprintTypeScale.BBScale var3;
            (var3 = new NPCFactionConfig.BlueprintTypeAvailability.BlueprintTypeScale.BBScale()).name = var1;
            var3.mass = var2;
            this.entries.add(var3);
         }

         public void calcNormal() {
            this.totalMass = 0;

            Iterator var1;
            NPCFactionConfig.BlueprintTypeAvailability.BlueprintTypeScale.BBScale var2;
            for(var1 = this.entries.iterator(); var1.hasNext(); this.totalMass = (int)((float)this.totalMass + var2.mass)) {
               var2 = (NPCFactionConfig.BlueprintTypeAvailability.BlueprintTypeScale.BBScale)var1.next();
            }

            NPCFactionConfig.BlueprintTypeAvailability.BlueprintTypeScale.BBScale var10000;
            for(var1 = this.entries.iterator(); var1.hasNext(); var10000.normalizedScale = var10000.mass / (float)this.totalMass) {
               var10000 = (NPCFactionConfig.BlueprintTypeAvailability.BlueprintTypeScale.BBScale)var1.next();
            }

            Collections.sort(this.entries);
         }

         // $FF: synthetic method
         BlueprintTypeScale(Object var2) {
            this();
         }

         class BBScale implements Comparable {
            String name;
            float mass;
            float normalizedScale;

            private BBScale() {
            }

            public int compareTo(NPCFactionConfig.BlueprintTypeAvailability.BlueprintTypeScale.BBScale var1) {
               return CompareTools.compare(this.normalizedScale, var1.normalizedScale);
            }

            // $FF: synthetic method
            BBScale(Object var2) {
               this();
            }
         }
      }
   }

   public class EntityPriority {
      private float[] vals = new float[BlueprintClassification.values().length];
      private float[][] valsMassDist = new float[BlueprintClassification.values().length][4];
      private float[] valsMassDistTotals = new float[BlueprintClassification.values().length];

      public EntityPriority() {
         Arrays.fill(this.vals, 1.0F);

         for(int var2 = 0; var2 < BlueprintClassification.values().length; ++var2) {
            Arrays.fill(this.valsMassDist[var2], 1.0F);
            this.calcTotalWeightDistribution(BlueprintClassification.values()[var2]);
         }

      }

      private void calcTotalWeightDistribution(BlueprintClassification var1) {
         this.valsMassDistTotals[var1.ordinal()] = 0.0F;

         for(int var2 = 0; var2 < this.valsMassDist[var1.ordinal()].length; ++var2) {
            float[] var10000 = this.valsMassDistTotals;
            int var10001 = var1.ordinal();
            var10000[var10001] += this.valsMassDist[var1.ordinal()][var2];
         }

      }

      private void calcAllTotalWeightDistribution() {
         for(int var1 = 0; var1 < BlueprintClassification.values().length; ++var1) {
            this.calcTotalWeightDistribution(BlueprintClassification.values()[var1]);
         }

      }

      public float getVal(BlueprintClassification var1) {
         return this.vals[var1.ordinal()];
      }

      public float getWeight(BlueprintClassification var1, float var2) {
         assert this.valsMassDistTotals[var1.ordinal()] > 0.0F;

         float[] var3 = this.valsMassDist[var1.ordinal()];

         assert var3.length > 0;

         assert var2 >= 0.0F;

         int var4 = Math.min(var3.length - 1, Math.round(var2 * ((float)var3.length - 1.0F)));

         assert var4 >= 0 : Math.round(var2 * ((float)var3.length - 1.0F)) + "; " + var3.length + "; " + var2;

         return var3[var4] / this.valsMassDistTotals[var1.ordinal()];
      }

      public void setWeight(BlueprintClassification var1, float... var2) {
         assert var2.length == this.valsMassDist[var1.ordinal()].length;

         for(int var3 = 0; var3 < var2.length && var3 < this.valsMassDist[var1.ordinal()].length; ++var3) {
            this.valsMassDist[var1.ordinal()][var3] = var2[var3];
         }

         this.calcTotalWeightDistribution(var1);

         assert this.valsMassDistTotals[var1.ordinal()] > 0.0F;

      }

      public void parse(Node var1) throws ConfigParserException {
         NodeList var2 = var1.getChildNodes();

         for(int var3 = 0; var3 < var2.getLength(); ++var3) {
            Node var4;
            if ((var4 = var2.item(var3)).getNodeType() == 1) {
               if (!var4.getNodeName().toLowerCase(Locale.ENGLISH).equals("classificationweight")) {
                  assert false : var4.getNodeName() + " -> " + var1.getNodeName() + ": Tag unknown";

                  throw new ConfigParserException(var4.getNodeName() + " -> " + var1.getNodeName() + ": Tag unknown");
               }

               NodeList var5 = var4.getChildNodes();
               BlueprintClassification var6 = null;
               float[] var7 = null;
               float var8 = -1.0F;

               for(int var9 = 0; var9 < var5.getLength(); ++var9) {
                  Node var10;
                  if ((var10 = var5.item(var9)).getNodeType() == 1) {
                     if (var10.getNodeName().toLowerCase(Locale.ENGLISH).equals("massdistribution")) {
                        String[] var11;
                        if ((var11 = var10.getTextContent().split(",")) == null) {
                           throw new ConfigParserException(var4.getNodeName() + "-> " + var1.getNodeName() + " -> " + var1.getNodeName() + ": mass distribution has to be at least one value");
                        }

                        try {
                           var7 = new float[var11.length];

                           for(int var12 = 0; var12 < var11.length; ++var12) {
                              float var13;
                              if ((var13 = Float.parseFloat(var11[var12].trim())) < 0.0F) {
                                 throw new ConfigParserException(var4.getNodeName() + " -> " + var4.getNodeName() + " -> " + var1.getNodeName() + ": malformed mass distribution (need to all positive values)");
                              }

                              var7[var12] = var13;
                           }
                        } catch (Exception var16) {
                           var16.printStackTrace();
                           throw new ConfigParserException(var4.getNodeName() + " -> " + var4.getNodeName() + " -> " + var1.getNodeName() + ": malformed mass distribution (need to be at least one floating point values)");
                        }

                        if (var7.length == 0) {
                           throw new ConfigParserException(var4.getNodeName() + " -> " + var4.getNodeName() + " -> " + var1.getNodeName() + ": malformed mass distribution (need to be at least one floating point values)");
                        }
                     }

                     if (var10.getNodeName().toLowerCase(Locale.ENGLISH).equals("classification")) {
                        BlueprintClassification var17 = null;

                        try {
                           var17 = BlueprintClassification.valueOf(var10.getTextContent().toUpperCase(Locale.ENGLISH));
                        } catch (IllegalArgumentException var15) {
                           var15.printStackTrace();
                        }

                        if (var17 == null && var10.getTextContent().toUpperCase(Locale.ENGLISH).endsWith("AVENGER")) {
                           var17 = BlueprintClassification.SCAVENGER;
                        }

                        if (var17 == null) {
                           throw new ConfigParserException(var4.getNodeName() + " -> " + var1.getNodeName() + ": classification unknown: '" + var10.getTextContent() + "'; Allowed: " + Arrays.toString(BlueprintClassification.values()));
                        }

                        var6 = var17;
                     }

                     if (var10.getNodeName().toLowerCase(Locale.ENGLISH).equals("weight")) {
                        try {
                           float var18;
                           if ((var18 = Float.parseFloat(var10.getTextContent())) < 0.0F) {
                              throw new ConfigParserException(var4.getNodeName() + " -> " + var1.getNodeName() + ": malformed classification weight (need to a positive value)");
                           }

                           var8 = var18;
                        } catch (Exception var14) {
                           var14.printStackTrace();
                           throw new ConfigParserException(var4.getNodeName() + " -> " + var1.getNodeName() + ": malformed classification weight  (need to be a floating point value)");
                        }
                     }
                  }
               }

               if (var6 == null || var8 < 0.0F || var7 == null) {
                  throw new ConfigParserException(var4.getNodeName() + " -> " + var1.getNodeName() + ": Invalid ClassificationWeight tag. need 'classification', 'Weight', and 'MassDistribution' sub tags");
               }

               this.vals[var6.ordinal()] = var8;
               this.valsMassDist[var6.ordinal()] = var7;
            }
         }

         this.calcAllTotalWeightDistribution();
      }

      public void appendXML(Document var1, Element var2) {
         Comment var3 = var1.createComment("Classification Weight: A weighted distribution of blueprint classes. This will normalize on existing classes. Example if we have available FIGHTER(weight 3), MINING(weight 1), TRADING(weight 5), then with 9 spawned ships, there would be 3 fighters, 1 miner, and 5 traders. Same happens with mass distribution of every class from index 0 (lightest ship) to 15 (heaviest ship), so if you set 0,0,0,0,0,0,0,0,0,0,0,0,0,0,1, only the biggest mass ships will be spawned (but nothing else). if you set 1,0,0,0,0,0,0,0,0,0,0,0,0,0,1, the smalled and biggest mass ships will spawn equally (but nothing inbetween)");
         var2.appendChild(var3);
         BlueprintClassification[] var11;
         int var4 = (var11 = BlueprintClassification.values()).length;

         for(int var5 = 0; var5 < var4; ++var5) {
            BlueprintClassification var6 = var11[var5];
            Element var7 = var1.createElement("ClassificationWeight");
            Element var8 = var1.createElement("Classification");
            Element var9 = var1.createElement("Weight");
            var8.setTextContent(var6.name());
            var9.setTextContent(String.valueOf(this.vals[var6.ordinal()]));
            var7.appendChild(var8);
            var7.appendChild(var9);
            var8 = var1.createElement("MassDistribution");
            StringBuffer var12 = new StringBuffer();

            for(int var10 = 0; var10 < this.valsMassDist[var6.ordinal()].length; ++var10) {
               var12.append(this.valsMassDist[var6.ordinal()][var10]);
               if (var10 < this.valsMassDist[var6.ordinal()].length - 1) {
                  var12.append(", ");
               }
            }

            var8.setTextContent(var12.toString());
            var7.appendChild(var8);
            var2.appendChild(var7);
         }

      }
   }
}
