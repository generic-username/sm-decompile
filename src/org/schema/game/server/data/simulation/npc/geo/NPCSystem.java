package org.schema.game.server.data.simulation.npc.geo;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import it.unimi.dsi.fastutil.shorts.ShortSet;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.CompareTools;
import org.schema.common.util.LogInterface;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.SendableGameState;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.missions.machines.states.FleetState;
import org.schema.game.common.data.world.EntityUID;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SectorInformation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.common.data.world.SystemRange;
import org.schema.game.network.objects.remote.RemoteNPCSystem;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.controller.EntityAlreadyExistsException;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GalaxyTmpVars;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.game.server.data.blueprint.ChildStats;
import org.schema.game.server.data.blueprint.SegmentControllerOutline;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.game.server.data.simulation.npc.NPCFactionConfig;
import org.schema.schine.network.server.ServerStateInterface;

public class NPCSystem extends NPCSystemStub {
   protected final NPCSystemStructure structure;
   protected final NPCCreator creator;
   protected final GameServerState state;
   private boolean changed;
   private short level;
   private final NPCSystemFleetManager fleetManager;
   private Vector3i tmp = new Vector3i();
   private int loadedSecs;
   private int total = -1;
   public ShortSet takenSecs;

   public void onStatusChanged(double var1) {
      if (this.status < this.getFaction().getConfig().abandonSystemOnStatus) {
         this.log("NPC System status low: " + var1 + " -> " + this.status + " (min " + this.getFaction().getConfig().abandonSystemOnStatus, LogInterface.LogLevel.NORMAL);
         this.abandon();
      }

      this.getFaction().sendCommand(NPCFaction.NPCFactionControlCommandType.AWNSER_STATUS, this.system.x, this.system.y, this.system.z, (float)this.status);
   }

   public void removeSystem() {
      this.abandon();
   }

   public void abandon() {
      if (!this.getConfig().doesAbandonSystems) {
         this.log("not abandoning system due to config value...", LogInterface.LogLevel.NORMAL);
      } else if (this.isRoot() && !this.getConfig().doesAbandonHome) {
         this.log("not abandoning home system due to config value...", LogInterface.LogLevel.NORMAL);
      } else {
         this.state.getFactionManager().getNpcFactionNews().lostSystem(this.getFaction().getIdFaction(), this.system);
         this.log("abandoning...", LogInterface.LogLevel.NORMAL);
         this.abandoned = true;
         this.structure.removeSystem(this);
      }
   }

   public NPCFactionConfig getConfig() {
      return this.getFaction().getConfig();
   }

   public Vector3i getHomeBase() {
      return this.structure.getHomeBase();
   }

   public NPCSystem(GameServerState var1, short var2, NPCSystemStructure var3) {
      this.level = var2;
      this.state = var1;
      this.structure = var3;
      this.creator = new NPCCreator(this);
      this.fleetManager = new NPCSystemFleetManager(this);
   }

   public float getWeight() {
      assert this.structure.totalWeight > 0.0F;

      return NPCSystemStructure.getLevelWeight(this.level) / this.structure.totalWeight;
   }

   public void setChangedNT() {
      this.changed = true;
      this.structure.setChangedNT();
   }

   public static short getLocalIndexFromSector(int var0, int var1, int var2) {
      return getLocalIndex(ByteUtil.modU16(var0), ByteUtil.modU16(var1), ByteUtil.modU16(var2));
   }

   public static short getLocalIndex(int var0, int var1, int var2) {
      assert var0 >= 0 && var0 < 16 && var1 >= 0 && var1 < 16 && var2 >= 0 && var2 < 16 : var0 + ", " + var1 + ", " + var2;

      return (short)((var2 << 8) + (var1 << 4) + var0);
   }

   public void log(String var1, LogInterface.LogLevel var2) {
      this.structure.log("[" + this.system.toStringPure() + "]" + var1, var2);
   }

   public void generate() {
      assert this.system != null : "System null!!";

      if (this.level == 0) {
         this.distanceFactor = 0.25F;
      } else {
         this.distanceFactor = Math.max(0.25F, Vector3i.getDisatance(this.structure.getRoot(), this.system));
      }

      this.state.getUniverse().getGalaxy(this.getFaction().serverGalaxyPos).getSystemResources(this.system, this.resources, new GalaxyTmpVars());
      this.seed = this.calcSeed();
   }

   public long getResourcesTotal() {
      Galaxy var1 = this.getState().getUniverse().getGalaxyFromSystemPos(this.system);
      if (this.total < 0) {
         this.total = 0;
         if (!var1.isVoidAbs(this.system)) {
            for(int var4 = 0; var4 < 16; ++var4) {
               byte var2 = this.resources.res[var4];
               this.log("System resources per type: " + var2, LogInterface.LogLevel.DEBUG);
               float var3 = this.getFaction().getConfig().minimumResourceMining;
               if (var2 > 0) {
                  var3 = Math.max(var3, (float)var2 / 127.0F);
               }

               this.total = (int)((float)this.total + var3 * this.getFaction().getConfig().resourceAvaililityMultiplicator);
            }
         }
      }

      return (long)this.total;
   }

   public long getResourcesAvailable() {
      return this.getResourcesTotal() - this.minedResources;
   }

   public NPCCreator getCreator() {
      return this.creator;
   }

   public GameServerState getState() {
      return this.state;
   }

   public NPCSystemStructure getStructure() {
      return this.structure;
   }

   public long getLastApplied() {
      return this.lastApplied;
   }

   public void applyToDatabase(boolean var1) {
      if (this.lastApplied <= 0L || var1) {
         try {
            StellarSystem var5;
            int var2 = (var5 = this.state.getUniverse().overwriteSystem(this.system, this.creator, true)).getOwnerFaction();

            assert this.getFactionId() != 0;

            var5.setOwnerFaction(this.getFactionId());
            var5.setOwnerPos(this.systemBase);
            var5.setOwnerUID(this.systemBaseUID);
            this.getState().getUniverse().getGalaxyFromSystemPos(this.system).getSystemResources(this.system, var5.systemResources, new GalaxyTmpVars());
            System.err.println("[SERVER][NPCSYSTEM] taken system: " + this.system + ": OwnerPos " + this.systemBase + "; UID " + this.systemBaseUID + "; Faction: " + this.getFactionId());
            this.state.getDatabaseIndex().getTableManager().getSystemTable().updateOrInsertSystemIfChanged(var5, true);
            this.state.getUniverse().getGalaxyFromSystemPos(this.system).getNpcFactionManager().onSystemOwnershipChanged(var2, this.getFactionId(), this.system);
            this.state.getGameState().sendGalaxyModToClients(var5, this.systemBase);
            this.lastApplied = System.currentTimeMillis();
            return;
         } catch (IOException var3) {
            var3.printStackTrace();
            return;
         } catch (SQLException var4) {
            var4.printStackTrace();
         }
      }

   }

   public void calculateContingent(int var1, int var2, float var3) {
      try {
         boolean var4 = this.lastContingentMaxLevel >= 0 && (this.lastContingentMaxLevel != var2 || Math.abs(this.lastContingentTotalLevelFill - var3) > 0.01F);
         ObjectArrayList var5 = new ObjectArrayList(this.getContingent().entities);
         this.lastContingentMaxLevel = var2;
         this.lastContingentTotalLevelFill = var3;
         this.getContingent().clearContignet();
         this.structure.faction.getConfig().getWeightedContingent(var1, var2, var3, this.getContingent());
         this.log("ReCalculated contingent... Total Entities: " + this.getContingent().getTotalAmount(), LogInterface.LogLevel.NORMAL);
         if (var4 || this.markedChangedContingent) {
            if (this.getContingent().isEqualTo(var5)) {
               this.markedChangedContingent = true;
               this.getFaction().structure.scheduleSystemContingentChange(this);
               return;
            }

            this.markedChangedContingent = false;
         }

      } catch (EntityNotFountException var6) {
         var6.printStackTrace();
      }
   }

   public Vector3i getSystemBaseSector() {
      return this.systemBase;
   }

   public String getSystemBaseUID() {
      return this.systemBaseUID;
   }

   public int getFactionId() {
      return this.structure.faction.getIdFaction();
   }

   public void checkNPCFactionSending(SendableGameState var1, boolean var2) {
      if (this.changed || var2) {
         var1.getNetworkObject().npcSystemBuffer.add(new RemoteNPCSystem(this, var1.getNetworkObject()));
         if (!var2) {
            this.changed = false;
         }
      }

   }

   public short getLevel() {
      return this.level;
   }

   public int getTotalAmountClass(BlueprintClassification... var1) {
      return this.getContingent().getTotalAmountClass(var1);
   }

   private boolean isTaken(int var1, int var2, int var3) {
      return this.takenSecs != null && this.takenSecs.contains(getLocalIndexFromSector(var1, var2, var3));
   }

   public int getTotalStationsProjected() {
      int var1 = 0;
      Iterator var2 = this.getContingent().entities.iterator();

      while(var2.hasNext()) {
         NPCEntityContingent.NPCEntitySpecification var3;
         if ((var3 = (NPCEntityContingent.NPCEntitySpecification)var2.next()).type == SimpleTransformableSendableObject.EntityType.SPACE_STATION) {
            var1 += var3.count;
         }
      }

      return var1;
   }

   public ShortArrayList createStationPositions(Random var1, int var2) {
      ShortArrayList var3 = null;
      SystemRange var4 = SystemRange.get(this.system);

      try {
         this.takenSecs = this.state.getDatabaseIndex().getTableManager().getEntityTable().getPlayerTakenSectorsLocalCoords(var4.start, var4.end);
      } catch (SQLException var12) {
         var12.printStackTrace();
      }

      if (this.takenSecs == null) {
         this.takenSecs = new ShortOpenHashSet();
      }

      boolean var14 = this.takenSecs.size() >= 4032;
      var1.setSeed((long)this.getFactionId() * this.structure.faction.getSeed() + (long)this.system.hashCode());
      int var5;
      boolean var13;
      if (var2 > 0) {
         var3 = new ShortArrayList(var2);
         var5 = var2;
         var13 = false;
      } else {
         var5 = this.getTotalStationsProjected();
         var13 = true;
      }

      if (var5 == 0) {
         throw new RuntimeException("NPC Faction has not available Stations to spawn: " + this.getFaction().getConfig().getPreset().factionPresetName);
      } else {
         Collections.sort(this.getContingent().entities, new Comparator() {
            public int compare(NPCEntityContingent.NPCEntitySpecification var1, NPCEntityContingent.NPCEntitySpecification var2) {
               int var3 = var1.c == BlueprintClassification.TRADE_STATION ? 1 : 0;
               return CompareTools.compare(var2.c == BlueprintClassification.TRADE_STATION ? 1 : 0, var3);
            }
         });
         Iterator var15 = this.getContingent().entities.iterator();

         while(true) {
            NPCEntityContingent.NPCEntitySpecification var6;
            do {
               if (!var15.hasNext()) {
                  return var3;
               }
            } while((var6 = (NPCEntityContingent.NPCEntitySpecification)var15.next()).type != SimpleTransformableSendableObject.EntityType.SPACE_STATION);

            for(int var7 = 0; var7 < var6.getLeft(); ++var7) {
               boolean var11 = false;

               byte var8;
               byte var9;
               byte var10;
               do {
                  do {
                     if (!var11 && var13 && this.system.equals(0, 0, 0)) {
                        var8 = 4;
                        var9 = 4;
                        var10 = 4;
                        var11 = true;
                     } else {
                        var8 = (byte)(1 + var1.nextInt(14));
                        var9 = (byte)(1 + var1.nextInt(14));
                        var10 = (byte)(1 + var1.nextInt(14));
                        if (Math.abs(var8 - 8) < 2 && Math.abs(var9 - 8) < 2 && Math.abs(var10 - 8) < 2) {
                           switch(var1.nextInt(3)) {
                           case 0:
                              var8 = (byte)(var8 + (var1.nextBoolean() ? 3 : -3));
                              break;
                           case 1:
                              var9 = (byte)(var9 + (var1.nextBoolean() ? 3 : -3));
                              break;
                           case 2:
                              var10 = (byte)(var10 + (var1.nextBoolean() ? 3 : -3));
                           }
                        }
                     }
                  } while(!var14 && this.isTaken(var8, var9, var10));
               } while(this.stationMap.containsKey(getLocalIndex(var8, var9, var10)));

               if (var13) {
                  this.systemBase = new Vector3i(this.system);
                  this.systemBase.scale(16);
                  this.systemBase.add(var8, var9, var10);
                  if (this.isRoot()) {
                     this.systemBaseUID = "NPC-HOMEBASE_" + this.systemBase.x + "_" + this.systemBase.y + "_" + this.systemBase.z;
                  } else {
                     this.systemBaseUID = "NPC-SYSTEMBASE_" + this.systemBase.x + "_" + this.systemBase.y + "_" + this.systemBase.z;
                  }

                  var13 = false;
               }

               short var16 = getLocalIndex(var8, var9, var10);
               this.stationMap.put(var16, var6.bbName);
               if (var3 != null) {
                  var3.add(var16);
               }

               if (this.takenSecs.size() > 2048) {
                  this.log("ERROR: SYSTEM TO OVERCROWDED WITH PLAYER STATIONS AND SHIPS" + this.getContingent().toString(), LogInterface.LogLevel.ERROR);
                  return var3;
               }

               if (this.stationMap.size() > 2048) {
                  this.log("ERROR: TOO MANY STATIONS IN THIS SYSTEM " + this.getContingent().toString(), LogInterface.LogLevel.ERROR);
                  return var3;
               }
            }
         }
      }
   }

   public boolean isRoot() {
      return this.system.equals(this.structure.getRoot());
   }

   public void populateSpaceStation(Sector var1, Random var2) {
      if (var1.pos.equals(this.getHomeBase())) {
         var1.loadUIDs(var1.getState());
         boolean var14 = false;
         Iterator var15 = var1.entityUids.iterator();

         while(var15.hasNext()) {
            EntityUID var13;
            if ((var13 = (EntityUID)var15.next()).id == this.structure.faction.getTradeNode().getEntityDBId()) {
               var14 = true;

               assert DatabaseEntry.removePrefixWOException(var13.uid).equals(DatabaseEntry.removePrefixWOException(this.structure.faction.getHomebaseUID())) : DatabaseEntry.removePrefixWOException(var13.uid) + "; " + this.structure.faction.getHomebaseUID();
               break;
            }
         }

         assert var14 : var1.entityUids;

      } else {
         BluePrintController var3;
         List var4 = (var3 = this.structure.faction.getConfig().getPreset().blueprintController).readBluePrints();
         NPCEntityContingent.NPCEntitySpecification var12 = this.getContingent().getStation(var1.getSeed(), var2);
         BlueprintEntry var5 = null;
         if (var12 != null) {
            Iterator var6 = var4.iterator();

            while(var6.hasNext()) {
               BlueprintEntry var7 = (BlueprintEntry)var6.next();
               if (var12.bbName.toLowerCase(Locale.ENGLISH).equals(var7.getName().toLowerCase(Locale.ENGLISH))) {
                  var5 = var7;
                  break;
               }
            }
         }

         if (var5 != null) {
            Transform var16;
            (var16 = new Transform()).setIdentity();

            try {
               StellarSystem var17 = var1.getState().getUniverse().getStellarSystemFromSecPos(var1.pos);
               String var18 = var17.getName() + "_" + var1.pos.x + "_" + var1.pos.y + "_" + var1.pos.z + "_ID";
               if (var1.pos.equals(this.getSystemBaseSector())) {
                  var18 = this.getSystemBaseUID();
               }

               SegmentControllerOutline var19 = var3.loadBluePrint(this.state, var5.getName(), var18, var16, -1, this.structure.faction.getIdFaction(), var4, var1.pos, (List)null, "<system>", Sector.buffer, true, (SegmentPiece)null, new ChildStats(true));
               if (var1.pos.equals(this.getSystemBaseSector())) {
                  var19.realName = var12.bbName + " Main ";
               } else {
                  var19.realName = var12.bbName + " " + this.spawedStations + " ";
                  ++this.spawedStations;
               }

               var19.checkForChilds(this.structure.faction.getIdFaction());
               var19.scrap = false;
               var19.spawnSectorId = new Vector3i(var1.pos);
               var19.npcSystem = this;
               var19.npcSpec = var12;
               synchronized(this.state.getBluePrintsToSpawn()) {
                  this.state.getBluePrintsToSpawn().add(var19);
                  return;
               }
            } catch (EntityNotFountException var9) {
               var9.printStackTrace();
               return;
            } catch (IOException var10) {
               var10.printStackTrace();
               return;
            } catch (EntityAlreadyExistsException var11) {
               var11.printStackTrace();
            }
         }

      }
   }

   public boolean isLocalCoordinateSectorLoadedServer(short var1) {
      assert this.getState() instanceof ServerStateInterface;

      Vector3i var2 = this.getPosSectorFromLocalCoordinate(var1, this.tmp);
      return this.getState().getUniverse().isSectorLoaded(var2);
   }

   public Vector3i getPosSectorFromLocalCoordinate(short var1, Vector3i var2) {
      int var3 = var1 >> 8 & 15;
      int var4 = var1 >> 4 & 15;
      int var5 = var1 & 15;
      var2.set(var5, var4, var3);
      var2.add(this.system.x << 4, this.system.y << 4, this.system.z << 4);
      return var2;
   }

   private void onActiveSystem() {
      this.log("-> Became active", LogInterface.LogLevel.NORMAL);
      this.fleetManager.onAciveSystem();
   }

   private void onInactiveSystem() {
      this.log("<- Became inactive", LogInterface.LogLevel.NORMAL);
      this.fleetManager.onInactiveSystem();
   }

   public void onLoadedSec(Sector var1) {
      boolean var2 = this.isActive();
      ++this.loadedSecs;

      assert this.loadedSecs > 0 : this;

      if (!var2 && this.isActive()) {
         this.onActiveSystem();
      }

   }

   public boolean isActive() {
      return this.loadedSecs > 0;
   }

   public void onUnloadedSec(Sector var1) {
      boolean var2 = this.isActive();
      --this.loadedSecs;

      assert this.loadedSecs >= 0 : this;

      if (!var2 && this.isActive()) {
         this.onInactiveSystem();
      }

   }

   public void onCommandPartFinished(Fleet var1, FleetState var2) {
      this.fleetManager.onCommandPartFinished(var1, var2);
   }

   public void lostEntity(BlueprintClassification var1, String var2, ElementCountMap var3, long var4, SegmentController var6) {
      try {
         this.getFaction().getConfig().getPreset().blueprintController.getBlueprint(var2);
         if (var3 != null) {
            this.getFaction().lostResources(var3);
         }

         this.fleetManager.lostEntity(var4, var6);
         this.setChangedNT();
      } catch (EntityNotFountException var7) {
         var7.printStackTrace();
      }
   }

   public NPCFaction getFaction() {
      return this.structure.faction;
   }

   public void onRemovedCleanUp() {
      this.log("cleaning up after losing control " + this.system, LogInterface.LogLevel.NORMAL);

      try {
         this.state.getDatabaseIndex().getTableManager().getSystemTable().setSystemOwnership(this.system, 0, "", 0, 0, 0);
         this.state.getUniverse().getGalaxyFromSystemPos(this.system).getNpcFactionManager().onSystemOwnershipChanged(this.getFactionId(), 0, this.system);
         this.state.getGameState().sendGalaxyModToClients(0, "", this.systemBase);
      } catch (SQLException var1) {
         var1.printStackTrace();
      }

      this.fleetManager.cleanUpAllFleets();
      this.getContingent().despawnAllSpawned(this);
   }

   public void clearFleetsAndContingentsOnChange() {
      assert !this.isActive();

      this.log("cleaning up since contingent changed (this sys is inactive) " + this.system, LogInterface.LogLevel.NORMAL);
      this.fleetManager.cleanUpAllFleets();
      this.getContingent().despawnAllSpawned(this);
   }

   public void replenish(long var1) {
      long var3 = (long)((double)this.getFaction().getConfig().replenishResourceRate * (double)this.getResourcesTotal());
      double var5 = (double)this.resourcesAvailable;
      this.minedResources = Math.max(0L, this.minedResources - var3);
      if (this.getResourcesTotal() <= 0L) {
         this.resourcesAvailable = 0.0F;
      } else {
         this.resourcesAvailable = (float)(1.0D - Math.min(1.0D, (double)this.minedResources / (double)this.getResourcesTotal()));
      }

      this.log("Replenished Resources: " + var5 + " -> " + this.resourcesAvailable + "; Replenished Resources: " + var3 + "; available now: " + this.getResourcesAvailable() + " / " + this.getResourcesTotal(), LogInterface.LogLevel.NORMAL);
   }

   public void mine(long var1) {
      this.replenish(var1);
      if (this.resourcesAvailable < this.getFaction().getConfig().minimumMinableResources) {
         this.log("MINE DONE: Not mining because available resources too low: " + this.resourcesAvailable + " < " + this.getFaction().getConfig().minimumMinableResources + "; (mined: " + this.minedResources + ")", LogInterface.LogLevel.DEBUG);
      } else {
         double var3 = this.getContingent().getMinerScore();
         IntOpenHashSet var5 = new IntOpenHashSet();
         long var6 = this.getResourcesAvailable();
         float var8 = 0.0F;
         if (var6 > 0L) {
            int var12;
            for(int var9 = 0; var9 < 16; ++var9) {
               float var11 = (float)this.resources.res[var9] / 127.0F;
               var8 += var11;
               var11 = Math.max(this.getFaction().getConfig().minimumResourceMining, var11);
               var12 = (int)Math.min(2147483647L, (long)Math.min((double)var6, (double)(var11 * this.getFaction().getConfig().resourcesPerMinerScore) * var3));
               this.log(String.format("MINE (raw ress): %-25s x %-8d value: %5.2f resourceByte: %4d", ElementKeyMap.toString(ElementKeyMap.resources[var9]), var12, var11, this.resources.res[var9]), LogInterface.LogLevel.DEBUG);
               var5.add(this.getFaction().getInventory().incExistingOrNextFreeSlotWithoutException(ElementKeyMap.resources[var9], var12));
               this.minedResources += (long)var12;
            }

            Math.abs(var8);
            this.log("MINE (misc): Total value: " + var8, LogInterface.LogLevel.DEBUG);
            Random var16 = new Random(var1);
            short[] var10;
            int var17 = (var10 = ElementKeyMap.typeList()).length;

            for(var12 = 0; var12 < var17; ++var12) {
               ElementInformation var2;
               short var14;
               int var15;
               if (!(var2 = ElementKeyMap.getInfoFast(var14 = var10[var12])).isOre() && !var2.isCapsule() && var2.isShoppable() && var2.getConsistence().isEmpty() && (var15 = (int)Math.min(2147483647L, (long)Math.min((double)var6, (double)(var8 * this.getFaction().getConfig().resourcesPerMinerScore) * var3))) > 0) {
                  var15 /= 3;
                  int var13 = 2 * var15;
                  var15 += var13 > 0 ? var16.nextInt(var13 + 1) : 0;
                  this.log(String.format("MINE (misc): %-25s x %-8d", ElementKeyMap.toString(var14), var15), LogInterface.LogLevel.DEBUG);
                  var5.add(this.getFaction().getInventory().incExistingOrNextFreeSlotWithoutException(var14, var15));
                  this.minedResources = (long)((float)this.minedResources + (float)var15 * this.getFaction().getConfig().minedResourcesAddedFromMisc);
               }
            }

            if (var5.size() > 0) {
               this.getFaction().getInventory().sendInventoryModification(var5);
            }

            this.minedResources = Math.min(this.minedResources, this.getResourcesTotal());
            this.resourcesAvailable = (float)(1.0D - Math.min(1.0D, (double)this.minedResources / (double)this.getResourcesTotal()));
            if (var8 == 0.0F) {
               this.log("WARNING; MINING: RAW RES 0: " + Arrays.toString(this.resources.res), LogInterface.LogLevel.DEBUG);
            }
         }

         this.log("MINE DONE: (MiningScore: " + var3 + ") Resources Available: " + this.resourcesAvailable + " (totAvailable " + var6 + ") (resourcesRaw: " + var8 + "); (mined: " + this.minedResources + ")", LogInterface.LogLevel.DEBUG);
         this.getFaction().setChangedNT();
      }
   }

   public void resupply(long var1) {
      if (!this.isActive()) {
         this.getContingent().resupply(this, var1);
      } else {
         this.log("Not Resupplying System since it's active", LogInterface.LogLevel.NORMAL);
      }
   }

   public void consume(long var1) {
      this.getContingent().consume(this, var1);
   }

   public void populateAsteroids(Sector var1, SectorInformation.SectorType var2, Random var3) throws IOException {
      var3 = new Random(this.seed + var1.getSeed());
      Random var4 = new Random(this.seed + var1.getSeed());
      int var5;
      if ((var5 = Math.round((float)var2.getAsteroidCountMax() * this.resourcesAvailable)) > 0) {
         var5 = var3.nextInt(var5);
         if (Galaxy.USE_GALAXY && var2 == SectorInformation.SectorType.ASTEROID) {
            ++var5;
         }

         int var12 = Math.round((float)(Integer)ServerConfig.ASTEROID_RADIUS_MAX.getCurrentState() * this.resourcesAvailable);

         for(int var6 = 0; var6 < var5; ++var6) {
            long var10 = var3.nextLong();
            var4.setSeed(var10);
            int var7 = var4.nextInt(var12) + Sector.rockSize;
            int var8 = var4.nextInt(var12) + Sector.rockSize;
            int var9 = var4.nextInt(var12) + Sector.rockSize;
            var1.addRandomRock(var1.getState(), var10, var7, var8, var9, var4, var6);
         }
      }

   }

   public void onUnachedFleet(Fleet var1) {
      this.fleetManager.onUncachedFleet(var1);
   }

   public NPCSystemFleetManager getFleetManager() {
      return this.fleetManager;
   }

   public void onAttackedFaction(Fleet var1, EditableSendableSegmentController var2, Damager var3) {
      this.getFleetManager().onAttacked(var1, var2, var3);
   }
}
