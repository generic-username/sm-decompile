package org.schema.game.server.data.simulation.npc.geo;

import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.schema.game.common.controller.ShoppingAddOn;
import org.schema.game.common.controller.trade.TradeNodeStub;
import org.schema.game.network.objects.TradePrices;
import org.schema.game.server.data.simulation.npc.NPCFaction;

public class NPCTradeNode extends TradeNodeStub {
   private final NPCFaction faction;
   private final TradePrices prices = new TradePrices(32);

   public NPCTradeNode(NPCFaction var1) {
      this.faction = var1;
   }

   public InputStream getPricesInputStream() throws IOException {
      FastByteArrayOutputStream var1 = new FastByteArrayOutputStream(10240);
      ShoppingAddOn.serializeTradePrices(new DataOutputStream(var1), true, this.getPrices(), this.getEntityDBId());
      return new FastByteArrayInputStream(var1.array, 0, (int)var1.length());
   }

   public TradePrices getPrices() {
      return this.prices;
   }

   public double getVolume() {
      this.setVolume(this.faction.getInventory().getVolume());
      return super.getVolume();
   }

   public double getCapacity() {
      this.setCapacity(this.faction.getInventory().getCapacity());
      return super.getCapacity();
   }
}
