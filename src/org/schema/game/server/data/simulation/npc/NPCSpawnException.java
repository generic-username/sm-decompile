package org.schema.game.server.data.simulation.npc;

public class NPCSpawnException extends Exception {
   private static final long serialVersionUID = 1L;

   public NPCSpawnException() {
   }

   public NPCSpawnException(String var1, Throwable var2, boolean var3, boolean var4) {
      super(var1, var2, var3, var4);
   }

   public NPCSpawnException(String var1, Throwable var2) {
      super(var1, var2);
   }

   public NPCSpawnException(String var1) {
      super(var1);
   }

   public NPCSpawnException(Throwable var1) {
      super(var1);
   }
}
