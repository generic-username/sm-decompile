package org.schema.game.server.data.simulation.npc.geo;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Locale;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.game.common.updater.FileUtil;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class NPCFactionPreset {
   public String factionPresetName;
   public BluePrintController blueprintController;
   public Document doc;
   public File confFile;

   public static boolean importPreset(String var0, String var1) {
      System.err.println("[SERVER][NPC] importing default preset: " + var0 + " -> " + var1);
      File var13 = new File(var0);
      File var2 = new File(var1);
      File[] var3 = var13.listFiles();
      boolean var4 = false;
      boolean var5 = false;
      int var6 = (var3 = var3).length;

      for(int var7 = 0; var7 < var6; ++var7) {
         File var8;
         if ((var8 = var3[var7]).getName().toLowerCase(Locale.ENGLISH).equals("blueprints.zip")) {
            try {
               assert var1.endsWith(File.separator);

               String var9 = var1 + "blueprints";
               (new File(var9)).mkdirs();
               FileUtil.extract(var8, var9);
               var4 = true;
            } catch (IOException var11) {
               var11.printStackTrace();
            }
         }

         if (var8.getName().toLowerCase(Locale.ENGLISH).equals("npcconfig.xml")) {
            File var14 = new File(var1, "npcConfig.xml");

            try {
               FileUtil.copyFile(var8, var14);
               var5 = true;
            } catch (IOException var12) {
               var12.printStackTrace();
            }
         }
      }

      if (var4 && var5) {
         return true;
      } else {
         System.err.println("Exception: could not import NPC faction preset " + var13.getAbsolutePath() + "; hasBlueprints: " + var4 + "; hasConfig: " + var5);

         try {
            FileUtil.deleteRecursive(var2);
         } catch (IOException var10) {
            var10.printStackTrace();
         }

         return false;
      }
   }

   public static NPCFactionPreset readFromDirOnlyConf(String var0) throws IOException, SAXException, ParserConfigurationException {
      assert var0.endsWith(File.separator);

      File var3;
      (var3 = new File(var0)).listFiles();
      NPCFactionPreset var1;
      (var1 = new NPCFactionPreset()).factionPresetName = var3.getName().toLowerCase(Locale.ENGLISH);
      File var2;
      if ((var2 = new File(var3, "npcConfig.xml")).exists() && !var2.isDirectory()) {
         var1.parseNPCConfigPreset(var2);
         return var1;
      } else {
         throw new IOException("no config file (npcConfig.xml) in " + var3.getAbsolutePath());
      }
   }

   public static NPCFactionPreset readFromDir(String var0) throws IOException, SAXException, ParserConfigurationException {
      assert var0.endsWith(File.separator);

      File var1;
      (var1 = new File(var0)).listFiles();
      NPCFactionPreset var2;
      (var2 = new NPCFactionPreset()).factionPresetName = var1.getName().toLowerCase(Locale.ENGLISH);
      File var3;
      if ((var3 = new File(var1, "npcConfig.xml")).exists() && !var3.isDirectory()) {
         var2.parseNPCConfigPreset(var3);
         if ((var3 = new File(var1, "blueprints")).exists() && var3.isDirectory()) {
            var2.blueprintController = new BluePrintController(var0 + "blueprints", GameServerState.SEGMENT_DATA_BLUEPRINT_PATH);
            if (var2.blueprintController.readBluePrints().isEmpty()) {
               throw new IOException("no valid blueprints found in blueprints dir of " + var1.getAbsolutePath());
            } else {
               Iterator var6 = var2.blueprintController.readBluePrints().iterator();

               while(var6.hasNext()) {
                  BlueprintEntry var7;
                  if ((var7 = (BlueprintEntry)var6.next()).getHeaderVersion() >= 3 && var7.hadCargoByteRead()) {
                     if (var7.getClassification() == BlueprintClassification.CARGO && var7.getTotalCapacity() == 0.0D) {
                        try {
                           throw new NPCBlueprintVersionException("WARNING for NPC preset " + var1.getName() + ": Cargo ship has no capacity although it has CARGO classification: " + var7.getName() + "; Please add some cargo to it for NPC use!");
                        } catch (NPCBlueprintVersionException var4) {
                           var4.printStackTrace();
                        }
                     }
                  } else {
                     try {
                        throw new NPCBlueprintVersionException("WARNING for NPC preset " + var1.getName() + ": Old blueprint version detected: " + var7.getName() + "; Please load the blueprint and resave it for NPC use!");
                     } catch (NPCBlueprintVersionException var5) {
                        var5.printStackTrace();
                     }
                  }
               }

               return var2;
            }
         } else {
            throw new IOException("no blueprints dir in " + var1.getAbsolutePath());
         }
      } else {
         throw new IOException("no config file (npcConfig.xml) in " + var1.getAbsolutePath());
      }
   }

   private void parseNPCConfigPreset(File var1) throws SAXException, IOException, ParserConfigurationException {
      this.confFile = var1;
      System.err.println("[SERVER][NPCFACTIONS] Using Faction Config File " + var1.getAbsolutePath());
      DocumentBuilder var2 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      this.doc = var2.parse(var1);
   }
}
