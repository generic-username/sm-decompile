package org.schema.game.server.data.simulation.npc;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.lang.reflect.Field;
import java.util.Locale;
import org.schema.common.config.ConfigParserException;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.linAlg.Vector3i;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class NPCFactionSpawn {
   @ConfigurationElement(
      name = "RandomSpawn",
      description = "Faction will spawn in a random star system"
   )
   public boolean randomSpawn;
   @ConfigurationElement(
      name = "FixedSpawnSystem",
      description = "If random spawn is off, set the location. Will switch to random if system is already taken"
   )
   public Vector3i fixedSpawnSystem;
   @ConfigurationElement(
      name = "Name",
      description = "Name of the faction"
   )
   public String name;
   @ConfigurationElement(
      name = "Description",
      description = "Description of the faction"
   )
   public String description;
   @ConfigurationElement(
      name = "PossiblePresets",
      description = "Possible Faction Config presets. Seperate with comma"
   )
   public String possiblePresets;
   @ConfigurationElement(
      name = "InitialGrowth",
      description = "How many systems are initially taken"
   )
   public int initialGrowth;

   public void append(Document var1, Node var2) throws IllegalArgumentException, IllegalAccessException {
      Field[] var3;
      int var4 = (var3 = this.getClass().getDeclaredFields()).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         Field var6;
         (var6 = var3[var5]).setAccessible(true);
         ConfigurationElement var7;
         if ((var7 = (ConfigurationElement)var6.getAnnotation(ConfigurationElement.class)) != null) {
            Element var8 = var1.createElement(var7.name());
            if (var6.get(this) != null) {
               if (var6.getType().equals(Vector3i.class)) {
                  var8.setTextContent(((Vector3i)var6.get(this)).toStringPure());
               } else {
                  var8.setTextContent(var6.get(this).toString());
               }
            }

            if (var7.description() != null && var7.description().trim().length() > 0) {
               Comment var9 = var1.createComment(var7.description());
               var8.appendChild(var9);
            }

            var2.appendChild(var8);
         }
      }

   }

   public void parse(Node var1) throws IllegalArgumentException, IllegalAccessException, ConfigParserException {
      Field[] var2 = this.getClass().getDeclaredFields();
      ObjectOpenHashSet var3 = new ObjectOpenHashSet();
      NodeList var4 = var1.getChildNodes();

      for(int var5 = 0; var5 < var4.getLength(); ++var5) {
         Node var6;
         if ((var6 = var4.item(var5)).getNodeType() == 1) {
            boolean var7 = false;
            boolean var8 = false;
            Field[] var9 = var2;
            int var10 = var2.length;

            for(int var11 = 0; var11 < var10; ++var11) {
               Field var12;
               (var12 = var9[var11]).setAccessible(true);
               ConfigurationElement var13;
               if ((var13 = (ConfigurationElement)var12.getAnnotation(ConfigurationElement.class)) != null) {
                  var7 = true;
                  if (var13.name().toLowerCase(Locale.ENGLISH).equals(var6.getNodeName().toLowerCase(Locale.ENGLISH))) {
                     try {
                        if (var12.getType() == Boolean.TYPE) {
                           var12.setBoolean(this, Boolean.parseBoolean(var6.getTextContent()));
                           var8 = true;
                        } else if (var12.getType() == Integer.TYPE) {
                           var12.setInt(this, Integer.parseInt(var6.getTextContent()));
                           var8 = true;
                        } else if (var12.getType() == Short.TYPE) {
                           var12.setShort(this, Short.parseShort(var6.getTextContent()));
                           var8 = true;
                        } else if (var12.getType() == Byte.TYPE) {
                           var12.setByte(this, Byte.parseByte(var6.getTextContent()));
                           var8 = true;
                        } else if (var12.getType() == Float.TYPE) {
                           var12.setFloat(this, Float.parseFloat(var6.getTextContent()));
                           var8 = true;
                        } else if (var12.getType() == Double.TYPE) {
                           var12.setDouble(this, Double.parseDouble(var6.getTextContent()));
                           var8 = true;
                        } else if (var12.getType() == Long.TYPE) {
                           var12.setLong(this, Long.parseLong(var6.getTextContent()));
                           var8 = true;
                        } else if (var12.getType().equals(Vector3i.class)) {
                           if (var6.getTextContent().trim().length() > 0) {
                              var12.set(this, Vector3i.parseVector3i(var6.getTextContent()));
                           }

                           var8 = true;
                        } else {
                           if (!var12.getType().equals(String.class)) {
                              throw new ConfigParserException("Cannot parse field: " + var12.getName() + "; " + var12.getType());
                           }

                           String var15 = var6.getTextContent().replaceAll("\\r\\n|\\r|\\n", "").replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r").replaceAll("\\\\t", "\t");
                           var12.set(this, var15);
                           var8 = true;
                        }
                     } catch (NumberFormatException var14) {
                        throw new ConfigParserException("Cannot parse field: " + var12.getName() + "; " + var12.getType() + "; with " + var6.getTextContent(), var14);
                     }
                  }

                  if (var8) {
                     var3.add(var12);
                     break;
                  }
               }
            }

            if (var7 && !var8) {
               assert false : var1.getNodeName() + " -> " + var6.getNodeName() + ": No appropriate field found for tag: " + var6.getNodeName();

               throw new ConfigParserException(var1.getNodeName() + " -> " + var6.getNodeName() + ": No appropriate field found for tag: " + var6.getNodeName());
            }
         }
      }

   }
}
