package org.schema.game.server.data.simulation.npc;

import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.shorts.Short2IntOpenHashMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Map.Entry;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.schema.common.config.ConfigParserException;
import org.schema.common.util.LogInterface;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.trade.TradeManager;
import org.schema.game.common.controller.trade.TradeNodeStub;
import org.schema.game.common.data.SendableGameState;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.missions.machines.states.FleetState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionAddCallback;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.NPCFactionInventory;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SectorInformation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.network.objects.TradePrice;
import org.schema.game.network.objects.TradePriceInterface;
import org.schema.game.network.objects.remote.RemoteSimpelCommand;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.game.server.data.simulation.npc.diplomacy.DiplomacyAction;
import org.schema.game.server.data.simulation.npc.diplomacy.DiplomacyReaction;
import org.schema.game.server.data.simulation.npc.diplomacy.NPCDiplomacy;
import org.schema.game.server.data.simulation.npc.diplomacy.NPCDiplomacyEntity;
import org.schema.game.server.data.simulation.npc.geo.NPCFactionSystemEvaluator;
import org.schema.game.server.data.simulation.npc.geo.NPCSystem;
import org.schema.game.server.data.simulation.npc.geo.NPCSystemStructure;
import org.schema.game.server.data.simulation.npc.geo.NPCTradeNode;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.server.ServerStateInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class NPCFaction extends Faction implements LogInterface {
   private static final byte VERSION = 0;
   public static final ByteBuffer buffer = ByteBuffer.allocate(10240);
   public Vector3i npcFactionHomeSystem = new Vector3i(0, 0, 0);
   public NPCSystemStructure structure;
   private NPCFactionSystemEvaluator systemEvaluator;
   private NPCFactionInventory inventory;
   private long seed;
   private Random random;
   private NPCFactionConfig config;
   private boolean changedNT;
   private long baseStationId;
   private NPCTradeController tradeController;
   private boolean initialized;
   public long accumulatedTimeNPCFactionTurn;
   public Vector3i serverGalaxyPos;
   private NPCDiplomacy diplomacy;
   private Int2IntOpenHashMap closeTerritoryFactions = new Int2IntOpenHashMap();
   private NPCFleetManager fleetManager;
   private NPCFaction.Turn[] turns;
   private int turnPoint = 0;
   private FileHandler fileHandler;
   private Logger logger;
   private Int2ObjectOpenHashMap clientReactions = new Int2ObjectOpenHashMap();
   private final IntOpenHashSet sentFullDebugUpdate = new IntOpenHashSet();
   public int initialSpawn = -1;

   public NPCFaction(StateInterface var1, int var2) {
      super(var1);
      this.setIdFaction(var2);
      if (!this.isOnServer()) {
         if (((GameClientState)var1).getGameState() != null) {
            Inventory var3;
            if ((var3 = (Inventory)((GameClientState)var1).getGameState().getInventories().get((long)var2)) != null) {
               this.inventory = (NPCFactionInventory)var3;
            } else {
               this.inventory = new NPCFactionInventory((GameStateInterface)var1, (long)var2);
            }
         } else {
            this.inventory = new NPCFactionInventory((GameStateInterface)var1, (long)var2);
         }
      } else {
         this.inventory = new NPCFactionInventory((GameStateInterface)var1, (long)var2);
      }

      this.setAutoDeclareWar(true);
      this.diplomacy = new NPCDiplomacy(this);
      this.createTurns();
      if (this.isOnServer()) {
         this.fleetManager = new NPCFleetManager(this);
      }

   }

   public NPCFaction(StateInterface var1, int var2, String var3, String var4, Vector3i var5) {
      super(var1, var2, var3, var4);
      if (!this.isOnServer()) {
         if (((GameClientState)var1).getGameState() != null) {
            Inventory var6;
            if ((var6 = (Inventory)((GameClientState)var1).getGameState().getInventories().get((long)var2)) != null) {
               this.inventory = (NPCFactionInventory)var6;
            } else {
               this.inventory = new NPCFactionInventory((GameStateInterface)var1, (long)var2);
            }
         } else {
            this.inventory = new NPCFactionInventory((GameStateInterface)var1, (long)var2);
         }
      } else {
         this.inventory = new NPCFactionInventory((GameStateInterface)var1, (long)var2);
      }

      this.npcFactionHomeSystem.set(var5);
      this.setAutoDeclareWar(true);
      this.diplomacy = new NPCDiplomacy(this);
      this.createTurns();
      if (this.isOnServer()) {
         this.fleetManager = new NPCFleetManager(this);
      }

   }

   public void initialize() {
      this.initialized = true;
      this.seed = (long)this.getName().hashCode() * (long)this.getIdFaction() + (long)this.npcFactionHomeSystem.hashCode();
      this.random = new Random(this.seed);

      assert this.getState() instanceof GameClientState || this.getConfig() != null;

      if (this.getConfig() != null) {
         this.getConfig().initialize();
      }

   }

   public void getDemandDiff(Short2IntOpenHashMap var1) {
      var1.clear();
      Iterator var2 = ElementKeyMap.keySet.iterator();

      while(var2.hasNext()) {
         short var3 = (Short)var2.next();
         int var4 = this.inventory.getOverallQuantity(var3);
         int var5 = Math.max(this.getProdWeightedDemand(var3, this.getConfig().expensionDemandMult), this.getDemand(var3, this.getConfig().expensionDemandMult));
         var1.add(var3, var5 - var4);
      }

   }

   public int getDemand(short var1, float var2) {
      if (ElementKeyMap.getInfoFast(var1).isShoppable()) {
         int var3 = this.getConfig().getDemandAmount(var1);
         return (int)(Math.max(0.0F, (float)var3 + (float)var3) * var2);
      } else {
         return 0;
      }
   }

   public int getProdWeightedDemand(short var1, float var2) {
      if (ElementKeyMap.getInfoFast(var1).isShoppable()) {
         int var3 = this.getConfig().getWeightedAmountWithProduction(var1);
         return (int)Math.max(0.0F, (float)var3 + (float)var3 * var2);
      } else {
         return 0;
      }
   }

   public int getTradeDemand(short var1, float var2) {
      if (ElementKeyMap.getInfoFast(var1).isShoppable()) {
         int var3 = this.getConfig().getDemandAmount(var1);
         int var4 = this.getConfig().getWeightedAmountWithProduction(var1);
         var4 = Math.max(var3, var4);
         return (int)Math.max((float)(this.structure.getTotalSystems() * this.getConfig().minDemandPerSystem), (float)var4 + (float)var4 * var2);
      } else {
         return 0;
      }
   }

   public boolean canExpand() {
      boolean var1 = true;
      short[] var2;
      int var3 = (var2 = ElementKeyMap.typeList()).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         short var5 = var2[var4];
         int var6 = this.getDemand(var5, this.getConfig().expensionDemandMult);
         int var7;
         if ((var7 = this.getInventory().getOverallQuantity(var5)) < var6) {
            this.log(String.format("Cannot expand, need resource: %-40s current: %8d demand: %8d --> %8d", ElementKeyMap.toString(var5), var7, var6, var6 - var7), LogInterface.LogLevel.NORMAL);
            var1 = false;
         }
      }

      return var1;
   }

   public void inventoryToDatabase() {
      List var1 = this.getTradeNode().getTradePricesInstance((GameServerState)this.getState()).getPrices();
      Short2ObjectOpenHashMap var2 = new Short2ObjectOpenHashMap();
      Short2ObjectOpenHashMap var3 = new Short2ObjectOpenHashMap();
      Iterator var4 = var1.iterator();

      while(var4.hasNext()) {
         TradePriceInterface var5;
         if ((var5 = (TradePriceInterface)var4.next()).isBuy()) {
            var2.put(var5.getType(), var5);
         } else {
            var3.put(var5.getType(), var5);
         }
      }

      short[] var11;
      int var12 = (var11 = ElementKeyMap.typeList()).length;

      for(int var6 = 0; var6 < var12; ++var6) {
         short var7 = var11[var6];
         int var8;
         TradePriceInterface var9;
         if ((var8 = this.getInventory().getOverallQuantity(var7)) == 0) {
            if ((var9 = (TradePriceInterface)var2.get(var7)) != null) {
               var9.setAmount(0);
            }

            if ((var9 = (TradePriceInterface)var3.get(var7)) != null) {
               var9.setAmount(0);
            }
         } else {
            if ((var9 = (TradePriceInterface)var2.get(var7)) != null) {
               var9.setAmount(var8);
            } else {
               var1.add(new TradePrice(var7, var8, -1, -1, false));
            }

            if ((var9 = (TradePriceInterface)var3.get(var7)) != null) {
               var9.setAmount(var8);
            } else {
               var1.add(new TradePrice(var7, var8, -1, -1, true));
            }
         }
      }

      try {
         ((GameServerState)this.getState()).getDatabaseIndex().getTableManager().getTradeNodeTable().setTradePrices(this.getTradeNode().getEntityDBId(), this.getInventory().getVolume(), this.getCredits(), var1);
      } catch (SQLException var10) {
         var10.printStackTrace();
      }
   }

   public void expandTurn() {
      if (this.canExpand()) {
         this.log("EXPANDING! (will consume " + StringTools.formatPointZero(this.getConfig().expensionDemandConsumeMult * 100.0F) + "% of demand)", LogInterface.LogLevel.NORMAL);
         IntOpenHashSet var1 = new IntOpenHashSet();
         short[] var2;
         int var3 = (var2 = ElementKeyMap.typeList()).length;

         for(int var4 = 0; var4 < var3; ++var4) {
            short var5 = var2[var4];
            int var6 = this.getDemand(var5, this.getConfig().expensionDemandConsumeMult);
            if (this.getInventory().getOverallQuantity(var5) > 0) {
               this.getInventory().decreaseBatch(var5, var6, var1);
               if (var6 != 0) {
                  this.log(String.format("EXPANDING! Removing from inventory: %-40s removing %8d", ElementKeyMap.toString(var5), var6), LogInterface.LogLevel.NORMAL);
               }
            }
         }

         this.getInventory().sendInventoryModification(var1);
         this.inventoryToDatabase();
         this.structure.grow(true);
      } else {
         this.log("Cannot expand, Not enough resources", LogInterface.LogLevel.NORMAL);
      }
   }

   private void createTurns() {
      this.turns = new NPCFaction.Turn[NPCFaction.TurnType.values().length];

      for(int var1 = 0; var1 < NPCFaction.TurnType.values().length; ++var1) {
         this.turns[var1] = new NPCFaction.Turn();
         this.turns[var1].active = true;
         this.turns[var1].type = NPCFaction.TurnType.values()[var1];
      }

   }

   public NPCFaction.Turn getTurn(NPCFaction.TurnType var1) {
      for(int var2 = 0; var2 < this.turns.length; ++var2) {
         if (this.turns[var2].type == var1) {
            return this.turns[var2];
         }
      }

      return null;
   }

   public Tag getTurnsTag() {
      Tag[] var1;
      Tag[] var10000 = var1 = new Tag[this.turns.length + 1];
      var10000[var10000.length - 1] = FinishTag.INST;

      for(int var2 = 0; var2 < this.turns.length; ++var2) {
         var1[var2] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.STRING, (String)null, this.turns[var2].type.name()), new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.turns[var2].active ? 1 : 0))), FinishTag.INST});
      }

      return new Tag(Tag.Type.STRUCT, (String)null, (Tag[])null);
   }

   public void getTurnsTag(Tag var1) {
      Tag[] var5 = var1.getStruct();

      for(int var2 = 0; var2 < var5.length - 1; ++var2) {
         Tag[] var3;
         NPCFaction.TurnType var4;
         NPCFaction.Turn var6;
         if ((var4 = NPCFaction.TurnType.valueOf((var3 = var5[var2].getStruct())[0].getString())) != null && (var6 = this.getTurn(var4)) != null) {
            var6.active = var3[1].getByte() != 0;
         }
      }

   }

   public boolean turn(long var1) {
      assert this.initialized;

      if (!this.initialized) {
         System.err.println("Exception: Faction " + this + " not initialized");
      }

      if (this.getTradeNode() == null) {
         try {
            throw new Exception("Can't do turn because trade node missing on " + this);
         } catch (Exception var3) {
            var3.printStackTrace();
            return true;
         }
      } else {
         if (this.turnPoint == 0) {
            this.log("----------STARTING TURN", LogInterface.LogLevel.NORMAL);
         } else {
            this.log("----------CONTINUING TURN", LogInterface.LogLevel.NORMAL);
         }

         if (this.turnPoint < this.turns.length) {
            this.turns[this.turnPoint].execute(var1);
            this.turnPoint = (this.turnPoint + 1) % this.turns.length;
         }

         if (this.turnPoint == 0) {
            this.log("----------ENDED TURN", LogInterface.LogLevel.NORMAL);
            return true;
         } else {
            return false;
         }
      }
   }

   public void turnFull(long var1) {
      assert this.initialized;

      this.log("----------STARTING TURN", LogInterface.LogLevel.NORMAL);
      NPCFaction.Turn[] var3;
      int var4 = (var3 = this.turns).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         var3[var5].execute(var1);
      }

      this.log("----------ENDED TURN", LogInterface.LogLevel.NORMAL);
   }

   public long getCredits() {
      return this.getTradeNode().getCredits();
   }

   public void createTradeNode(GameServerState var1, long var2) throws SQLException, IOException {
      NPCTradeNode var4;
      (var4 = new NPCTradeNode(this)).setCapacity(this.getInventory().getCapacity());
      var4.setVolume(this.getInventory().getVolume());
      var4.setCredits((long)(this.random.nextInt(Math.max(1, this.getConfig().randomCredits + 1)) + this.getConfig().baseCredits));
      var4.setEntityDBId(var2);
      var4.setFactionId(this.getIdFaction());
      var4.setOwners(new ObjectOpenHashSet());
      var4.setTradePermission(TradeManager.PERM_ALL_BUT_ENEMY);
      var4.setSector(this.getHomeSector());
      var4.setStationName(this.getHomebaseRealName());
      var4.setSystem(VoidSystem.getContainingSystem(this.getHomeSector(), new Vector3i()));
      var1.getDatabaseIndex().getTableManager().getTradeNodeTable().insertOrUpdateTradeNode(var4);

      assert var4.getEntityDBId() != Long.MIN_VALUE;

      var1.getUniverse().tradeNodesDirty.enqueue(var2);
      this.baseStationId = var2;
      var1.getUniverse().getGalaxyManager().sendDirectTradeUpdateOnServer(var2);
   }

   public boolean isNPC() {
      return true;
   }

   protected Tag toTagAdditionalInfo() {
      Tag var1 = new Tag(Tag.Type.BYTE, (String)null, (byte)0);
      Tag var2 = new Tag(Tag.Type.VECTOR3i, (String)null, this.npcFactionHomeSystem);
      Tag var3 = this.structure.toTagStructure();
      Tag var4 = this.inventory.toTagStructure();
      Tag var5 = new Tag(Tag.Type.LONG, (String)null, this.getTradeNode() != null ? this.getTradeNode().getEntityDBId() : Long.MIN_VALUE);
      Tag var6 = new Tag(Tag.Type.LONG, (String)null, this.accumulatedTimeNPCFactionTurn);
      Tag var7 = new Tag(Tag.Type.VECTOR3i, (String)null, this.serverGalaxyPos);
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, var2, var3, var4, var5, var6, var7, new Tag(Tag.Type.BYTE, (String)null, (byte)0), this.diplomacy.toTag(), FinishTag.INST});
   }

   public TradeNodeStub getTradeNode() {
      if ((TradeNodeStub)((GameServerState)this.getState()).getUniverse().getGalaxyManager().getTradeNodeDataById().get(this.baseStationId) == null) {
         try {
            throw new Exception("NPC TRADE NODE OF " + this + " NOT FOUND: " + this.baseStationId + "; ALL NODES; " + ((GameServerState)this.getState()).getUniverse().getGalaxyManager().getTradeNodeDataById());
         } catch (Exception var1) {
            var1.printStackTrace();
         }
      }

      return (TradeNodeStub)((GameServerState)this.getState()).getUniverse().getGalaxyManager().getTradeNodeDataById().get(this.baseStationId);
   }

   protected void fromTagAdditionalInfo(Tag var1) throws IllegalArgumentException, IllegalAccessException, ConfigParserException {
      Tag[] var2;
      (var2 = var1.getStruct())[0].getByte();
      this.npcFactionHomeSystem = var2[1].getVector3i();
      this.structure.fromTagStructure(var2[2]);
      this.inventory.fromTagStructure(var2[3]);
      this.baseStationId = var2[4].getLong();
      this.accumulatedTimeNPCFactionTurn = var2[5].getLong();
      this.serverGalaxyPos = var2[6].getVector3i();
      this.diplomacy.fromTag(var2[8]);
   }

   public void initializeWithState(GameServerState var1) throws IllegalArgumentException, IllegalAccessException, ConfigParserException {
      super.initializeWithState(var1);
      this.structure = new NPCSystemStructure(var1, this);
      this.systemEvaluator = new NPCFactionSystemEvaluator(this);
      this.tradeController = new NPCTradeController(this, var1);
   }

   public void onCreated() {
      this.addHook = new FactionAddCallback() {
         public void callback() {
            if (!NPCFaction.this.structure.isCreated()) {
               NPCFaction.this.config.generate(NPCFaction.this);
               NPCFaction.this.structure.createNew();
               NPCFaction.this.structure.recalc();
               NPCFaction.this.structure.applyNonExisting();

               assert NPCFaction.this.getTradeNode().getEntityDBId() != Long.MIN_VALUE;

               NPCFaction.this.tradeController.fillInitialInventoryAndTrading(NPCFaction.this.random);
            } else {
               NPCFaction.this.structure.recalc();
            }
         }
      };
   }

   public void onEntityDestroyedServer(SegmentController var1) {
      this.onLostEntity(var1.dbId, var1, true);
   }

   public void onEntityOverheatingServer(SegmentController var1) {
      this.onLostEntity(var1.dbId, var1, true);
   }

   public void onLostEntity(long var1, SegmentController var3, boolean var4) {
      this.structure.removeEntity(var1, var3, var4);
   }

   public NPCFactionSystemEvaluator getSystemEvaluator() {
      assert this.getState() instanceof ServerStateInterface;

      return this.systemEvaluator;
   }

   public Inventory getInventory() {
      return this.inventory;
   }

   public NPCFactionConfig getConfig() {
      return this.config;
   }

   public void setChangedNT() {
      assert this.getState() instanceof ServerStateInterface;

      this.changedNT = true;
      if (((FactionState)this.getState()).getFactionManager() != null) {
         ((FactionState)this.getState()).getFactionManager().setNPCFactionChanged();
      }

   }

   public void checkNPCFactionSendingDebug(SendableGameState var1, boolean var2) {
      if (this.changedNT || var2) {
         this.structure.checkNPCFactionSending(var1, var2);
         if (!var2) {
            this.changedNT = false;
         }
      }

   }

   public void checkNPCDiplomacyNT(SendableGameState var1, boolean var2) {
      this.diplomacy.checkNPCFactionSending(var1, var2);
   }

   public boolean isLogMode(LogInterface.LogMode var1) {
      return (Integer)ServerConfig.NPC_LOG_MODE.getCurrentState() == var1.ordinal();
   }

   public void log(String var1, LogInterface.LogLevel var2) {
      if (this.getName() != null && this.isOnServer()) {
         if (this.isLogMode(LogInterface.LogMode.FILE) && this.fileHandler == null) {
            File var10000 = new File("./logs/npc/");
            final SimpleDateFormat var3 = null;
            var10000.mkdirs();

            try {
               var3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
               final Date var4 = new Date();
               String var5 = this.getName() + "_" + this.getIdFaction();
               this.fileHandler = new FileHandler("./logs/npc/lognpc_" + var5 + ".%g.log", 4194304, 20);
               this.fileHandler.setFormatter(new Formatter() {
                  public String format(LogRecord var1) {
                     var4.setTime(var1.getMillis());
                     return "[" + var3.format(var4) + "] " + var1.getMessage() + "\n";
                  }
               });
               LogManager var8 = LogManager.getLogManager();
               this.logger = Logger.getLogger(var5);

               assert this.logger != null;

               this.logger.addHandler(this.fileHandler);
               this.logger.setLevel(Level.ALL);
               var8.addLogger(this.logger);
            } catch (SecurityException var6) {
               var6.printStackTrace();
            } catch (IOException var7) {
               var7.printStackTrace();
            }
         }

         if (var2 == LogInterface.LogLevel.ERROR || var2 == LogInterface.LogLevel.NORMAL) {
            if (this.logger == null) {
               System.err.println("[NPC][" + this.getName() + "]" + var1);
               return;
            }

            this.logger.log(Level.INFO, var1);
         }

      } else {
         if (var2 == LogInterface.LogLevel.ERROR || var2 == LogInterface.LogLevel.NORMAL || var2 == LogInterface.LogLevel.DEBUG) {
            System.err.println(!this.isOnServer() ? "[CLIENT]" : "[SERVER][NPC][" + this.getName() + "]" + var1);
         }

      }
   }

   public NPCTradeController getTradeController() {
      assert this.getState() instanceof GameServerState;

      return this.tradeController;
   }

   public long getSeed() {
      return this.seed;
   }

   public void populateAsteroids(Sector var1, SectorInformation.SectorType var2, Random var3) throws IOException {
      this.structure.populateAsteroids(var1, var2, var3);
   }

   public void populateSpaceStation(Sector var1, Random var2) {
      this.structure.populateSpaceStation(var1, var2);
   }

   public void populateAfterAsteroids(Sector var1, SectorInformation.SectorType var2, Random var3) {
   }

   public void populatePlanet(Sector var1, SectorInformation.SectorType var2, SectorInformation.PlanetType var3, Random var4) {
      var1.populatePlanetSector(var1.getState(), var3);
   }

   public void getWeightedInventory(ElementCountMap var1, float var2) {
   }

   public void onAddedSectorSynched(Sector var1) {
      this.structure.onLoadedSector(var1);
   }

   public void onRemovedSectorSynched(Sector var1) {
      this.structure.onUnloadedSector(var1);
   }

   public void onCommandPartFinished(Fleet var1, FleetState var2) {
      assert this.isOnServer();

      this.structure.onCommandPartFinished(var1, var2);
   }

   public void lostResources(ElementCountMap var1) {
      IntOpenHashSet var2 = new IntOpenHashSet();
      this.getInventory().decreaseBatchIgnoreAmount(var1, var2);
      this.getInventory().sendInventoryModification(var2);
      this.log("Lost resources: " + var1.getTotalAmount(), LogInterface.LogLevel.NORMAL);
   }

   public long getTimeBetweenFactionTurns() {
      return this.getConfig().timeBetweenTurnsMS;
   }

   public void setConfig(NPCFactionConfig var1) {
      assert this.config == null;

      this.config = var1;
   }

   public boolean isSystemActive(Vector3i var1) {
      assert this.getState() instanceof ServerStateInterface;

      NPCSystem var2;
      return (var2 = this.structure.getSystem(var1)) != null && var2.isActive();
   }

   public void onUncachedFleet(Fleet var1) {
      NPCSystem var2;
      if (this.isOnServer() && (var2 = this.structure.getSystem(var1.getNpcSystem())) != null) {
         var2.onUnachedFleet(var1);
      }

   }

   public void diplomacyAction(DiplomacyAction.DiplActionType var1, long var2) {
      this.diplomacy.diplomacyAction(var1, var2);
   }

   public Tag getCloseTerritoryTag() {
      Tag[] var1;
      Tag[] var10000 = var1 = new Tag[this.closeTerritoryFactions.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;
      int var2 = 0;

      for(Iterator var3 = this.closeTerritoryFactions.entrySet().iterator(); var3.hasNext(); ++var2) {
         Entry var4 = (Entry)var3.next();
         var1[var2] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.INT, (String)null, (Integer)var4.getKey()), new Tag(Tag.Type.INT, (String)null, (Integer)var4.getValue()), FinishTag.INST});
      }

      return new Tag(Tag.Type.STRUCT, (String)null, var1);
   }

   public NPCDiplomacy getDiplomacy() {
      return this.diplomacy;
   }

   public void setDiplomacy(NPCDiplomacy var1) {
      this.diplomacy = var1;
   }

   public void serializeExtra(DataOutputStream var1) throws IOException {
      NPCFaction.Turn[] var2;
      int var3 = (var2 = this.turns).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         NPCFaction.Turn var5 = var2[var4];
         var1.writeBoolean(var5.active);
      }

      var1.writeShort(this.getConfig().getDiplomacyReactions().size());
      Iterator var6 = this.getConfig().getDiplomacyReactions().iterator();

      while(var6.hasNext()) {
         DiplomacyReaction var7 = (DiplomacyReaction)var6.next();
         var1.writeShort((short)var7.index);
         var1.writeUTF(var7.name);
      }

   }

   public void deserializeExtra(DataInputStream var1) throws IOException {
      NPCFaction.Turn[] var2;
      int var3 = (var2 = this.turns).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         var2[var4].active = var1.readBoolean();
      }

      short var6 = var1.readShort();

      for(var3 = 0; var3 < var6; ++var3) {
         short var7 = var1.readShort();
         String var5 = var1.readUTF();
         this.clientReactions.put(var7, var5);
      }

   }

   public void sendCommand(NPCFaction.NPCFactionControlCommandType var1, Object... var2) {
      NPCFactionControlCommand var3 = new NPCFactionControlCommand(this.getIdFaction(), var1, var2);
      this.sendCommand(var3);
   }

   private void sendCommand(NPCFactionControlCommand var1) {
      ((GameStateInterface)this.getState()).getGameState().getNetworkObject().simpleCommandQueue.add(new RemoteSimpelCommand(var1, this.isOnServer()));
   }

   public void executeFactionCommand(NPCFactionControlCommand var1) {
      NPCFaction.NPCFactionControlCommandType var2 = NPCFaction.NPCFactionControlCommandType.values()[var1.getCommand()];
      NPCDiplomacyEntity var7;
      Vector3i var9;
      int var10;
      long var13;
      switch(var2) {
      case REQUEST_ALL:
         if (!this.sentFullDebugUpdate.contains(var1.getUpdateSenderStateId())) {
            this.sentFullDebugUpdate.add(var1.getUpdateSenderStateId());
            this.checkNPCDiplomacyNT(this.structure.getState().getGameState(), true);
            return;
         }

         this.checkNPCDiplomacyNT(this.structure.getState().getGameState(), false);
         return;
      case REQUEST_STATUS:
         var9 = new Vector3i((Integer)var1.getArgs()[0], (Integer)var1.getArgs()[1], (Integer)var1.getArgs()[2]);
         NPCSystem var14;
         if ((var14 = this.structure.getSystem(var9)) != null) {
            this.sendCommand(NPCFaction.NPCFactionControlCommandType.AWNSER_STATUS, var9.x, var9.y, var9.z, (float)var14.status);
            return;
         }
         break;
      case DIPLOMACY_ACTION:
         DiplomacyAction.DiplActionType var11 = DiplomacyAction.DiplActionType.values()[(Integer)var1.getArgs()[0]];
         var13 = (Long)var1.getArgs()[1];
         this.getDiplomacy().diplomacyAction(var11, var13);
         this.log("Manually triggered action " + var11.name(), LogInterface.LogLevel.NORMAL);
         this.getDiplomacy().ntChanged(var13);
         return;
      case MOD_POINTS:
         var10 = (Integer)var1.getArgs()[0];
         var13 = (Long)var1.getArgs()[1];
         if ((var7 = (NPCDiplomacyEntity)this.getDiplomacy().entities.get(var13)) != null) {
            var7.modPoints(var10);
            return;
         }

         this.log("no entity found in diplomacy (maybe not created yet): " + var13, LogInterface.LogLevel.ERROR);
         return;
      case DIPLOMACY_REACTION:
         var10 = (Integer)var1.getArgs()[0];
         var13 = (Long)var1.getArgs()[1];
         Iterator var6 = this.getConfig().getDiplomacyReactions().iterator();

         DiplomacyReaction var3;
         do {
            if (!var6.hasNext()) {
               return;
            }
         } while((var3 = (DiplomacyReaction)var6.next()).index != var10);

         if ((var7 = (NPCDiplomacyEntity)this.getDiplomacy().entities.get(var13)) != null) {
            var7.triggerReactionManually(var3);
            this.getDiplomacy().ntChanged(var13);
            return;
         }

         this.log("no entity found in diplomacy (maybe not created yet): " + var13, LogInterface.LogLevel.ERROR);
         return;
      case AWNSER_STATUS:
         var9 = new Vector3i((Integer)var1.getArgs()[0], (Integer)var1.getArgs()[1], (Integer)var1.getArgs()[2]);
         float var12 = (Float)var1.getArgs()[3];
         ((GameStateInterface)this.getState()).getGameState().putClientNPCSystemStatus(var9, var12);
         return;
      case DIPLOMACY_TRIGGER:
         this.getDiplomacy().trigger(NPCDiplomacy.NPCDipleExecType.values()[(Integer)var1.getArgs()[0]]);
         return;
      case TURN_MOD:
         NPCFaction.TurnType var8 = NPCFaction.TurnType.values()[(Integer)var1.getArgs()[0]];
         this.getTurn(var8).active = (Boolean)var1.getArgs()[1];
         if (this.isOnServer()) {
            this.sendCommand(var1);
            return;
         }
         break;
      case TURN_TRIGGER:
         ((FactionState)this.getState()).getFactionManager().scheduleTurn(this);
         return;
      case LOG_CREDIT_STATUS:
         String var4 = "CREDITS: " + this.getCredits();
         this.log(var4, LogInterface.LogLevel.NORMAL);
      }

   }

   public NPCFleetManager getFleetManager() {
      assert this.isOnServer();

      return this.fleetManager;
   }

   public void onAttackedFaction(Fleet var1, EditableSendableSegmentController var2, Damager var3) {
      NPCSystem var4;
      if ((var4 = this.structure.getSystem(var1.getNpcSystem())) != null) {
         var4.onAttackedFaction(var1, var2, var3);
      }

   }

   public void removeCompletely() {
      this.log("REMOVING FACTION COMPLETELY", LogInterface.LogLevel.NORMAL);
      long var1 = this.getTradeNode().getEntityDBId();
      GameServerState var3;
      Iterator var4 = (var3 = this.structure.getState()).getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

      while(var4.hasNext()) {
         Sendable var5;
         SimpleTransformableSendableObject var6;
         if ((var5 = (Sendable)var4.next()) instanceof SimpleTransformableSendableObject && (var6 = (SimpleTransformableSendableObject)var5).getFactionId() == this.getIdFaction()) {
            var6.markForPermanentDelete(true);
         }
      }

      var3.getDatabaseIndex().getTableManager().getTradeNodeTable().removeTradeNode(var1);
      var3.getUniverse().getGalaxyManager().sendDirectTradeUpdateOnServer(var1);
      var3.getDatabaseIndex().getTableManager().getEntityTable().removeFactionCompletely(this);
      this.log("FACTION HAS BEEN REMOVED COMPLETELY", LogInterface.LogLevel.NORMAL);
   }

   public boolean canAttackShips() {
      return this.isOnServer() ? this.getConfig().canAttackShips : true;
   }

   public boolean canAttackStations() {
      return this.isOnServer() ? this.getConfig().canAttackStations : true;
   }

   public void setInventory(NPCFactionInventory var1) {
      this.inventory = var1;
   }

   public Int2ObjectOpenHashMap getClientReactions() {
      return this.clientReactions;
   }

   public void setClientReactions(Int2ObjectOpenHashMap var1) {
      this.clientReactions = var1;
   }

   public class Turn {
      NPCFaction.TurnType type;
      public boolean active;

      public void execute(long var1) {
         if (this.active) {
            NPCFaction.this.log("-----" + this.type.name() + " START", LogInterface.LogLevel.NORMAL);
            switch(this.type) {
            case CONSUME:
               NPCFaction.this.structure.consume(var1);
               break;
            case EXPAND:
               NPCFaction.this.expandTurn();
               break;
            case MINING:
               NPCFaction.this.structure.mine(var1);
               break;
            case PRODUCTION:
               NPCFaction.this.structure.produce();
               break;
            case RESUPPLY:
               NPCFaction.this.structure.resupply(var1);
               break;
            case TRADE:
               NPCFaction.this.tradeController.tradeTurn();
               break;
            case FLEETS:
               NPCFaction.this.fleetManager.fleetTurn();
               break;
            case TRANSFER_TO_DB:
               NPCFaction.this.inventoryToDatabase();
               break;
            case RECALC_PRICES:
               try {
                  NPCFaction.this.tradeController.recalcPrices();
               } catch (SQLException var3) {
                  var3.printStackTrace();
               } catch (IOException var4) {
                  var4.printStackTrace();
               }
               break;
            default:
               throw new NullPointerException("UNKNOWN TURN");
            }

            NPCFaction.this.log("-----" + this.type.name() + " END", LogInterface.LogLevel.NORMAL);
         } else {
            NPCFaction.this.log("----------" + this.type.name() + " TURN INACTIVE", LogInterface.LogLevel.NORMAL);
         }
      }
   }

   public static enum TurnType {
      MINING,
      PRODUCTION,
      RESUPPLY,
      CONSUME,
      TRADE,
      EXPAND,
      FLEETS,
      TRANSFER_TO_DB,
      RECALC_PRICES;
   }

   public static enum NPCFactionControlCommandType {
      TURN_MOD(new Class[]{Integer.class, Boolean.class}),
      TURN_TRIGGER(new Class[0]),
      DIPLOMACY_TRIGGER(new Class[]{Integer.class}),
      LOG_CREDIT_STATUS(new Class[0]),
      REQUEST_STATUS(new Class[]{Integer.class, Integer.class, Integer.class}),
      AWNSER_STATUS(new Class[]{Integer.class, Integer.class, Integer.class, Float.class}),
      REQUEST_ALL(new Class[0]),
      DIPLOMACY_ACTION(new Class[]{Integer.class, Long.class}),
      DIPLOMACY_REACTION(new Class[]{Integer.class, Long.class}),
      MOD_POINTS(new Class[]{Integer.class, Long.class});

      private Class[] args;

      private NPCFactionControlCommandType(Class... var3) {
         this.args = var3;
      }

      public final void checkMatches(Object[] var1) {
         if (this.args.length != var1.length) {
            throw new IllegalArgumentException("Invalid argument count: Provided: " + Arrays.toString(var1) + ", but needs: " + Arrays.toString(this.args));
         } else {
            for(int var2 = 0; var2 < this.args.length; ++var2) {
               if (!var1[var2].getClass().equals(this.args[var2])) {
                  System.err.println("Not Equal: " + var1[var2] + " and " + this.args[var2]);
                  throw new IllegalArgumentException("Invalid argument on index " + var2 + ": Provided: " + Arrays.toString(var1) + "; cannot take " + var1[var2] + ":" + var1[var2].getClass() + ", it has to be type: " + this.args[var2].getClass());
               }
            }

         }
      }
   }
}
