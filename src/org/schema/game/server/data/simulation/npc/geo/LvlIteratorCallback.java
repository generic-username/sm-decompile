package org.schema.game.server.data.simulation.npc.geo;

public interface LvlIteratorCallback {
   void handleFree(int var1, int var2, int var3, short var4);

   void handleTaken(int var1, int var2, int var3, short var4);
}
