package org.schema.game.server.data.simulation.npc.geo;

import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.common.util.LogInterface;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class NPCSystemStub implements LogInterface, SerialializationInterface {
   int id = -1;
   private static final byte VERSION = 0;
   public long minedResources;
   protected long seed;
   public Vector3i system;
   public Vector3i systemBase;
   public short totalAsteroidSectors;
   public short totalPlanetSectors;
   protected int lastContingentMaxLevel;
   protected float lastContingentTotalLevelFill;
   public float distanceFactor = -1.0F;
   public long takenTime;
   public long lastUpdate;
   public long lastApplied = -1L;
   public String systemBaseUID;
   public boolean abandoned;
   public short spawedStations = 1;
   public final FactionResourceRequestContainer resources = new FactionResourceRequestContainer();
   public float resourcesAvailable = 1.0F;
   public final Short2ObjectOpenHashMap stationMap = new Short2ObjectOpenHashMap();
   public boolean markedChangedContingent;
   private final NPCEntityContingent contingent = new NPCEntityContingent(this);
   public double status = 1.0D;
   private int clientFactionId;
   private float clientWeight;
   private short clientLevel;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeBoolean(this.abandoned);
      if (this.abandoned) {
         this.system.serialize(var1);
      } else {
         Tag var3 = this.toTagStructure();

         assert var3.getType() == Tag.Type.STRUCT;

         var3.serializeNT(var1);
         this.contingent.serialize(var1, var2);
         var1.writeDouble(this.status);
      }
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.abandoned = var1.readBoolean();
      if (this.abandoned) {
         this.system = Vector3i.deserializeStatic(var1);
      } else {
         Tag var4 = Tag.deserializeNT(var1);
         this.fromTagStructure(var4, this.getLevel());
         this.contingent.deserialize(var1, var2, var3);
         this.status = var1.readDouble();
      }
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.INT, (String)null, this.id), new Tag(Tag.Type.SHORT, (String)null, this.getLevel()), new Tag(Tag.Type.INT, (String)null, this.getFactionId()), new Tag(Tag.Type.VECTOR3i, (String)null, this.systemBase), new Tag(Tag.Type.STRING, (String)null, this.systemBaseUID), new Tag(Tag.Type.FLOAT, (String)null, this.distanceFactor), new Tag(Tag.Type.INT, (String)null, this.lastContingentMaxLevel), new Tag(Tag.Type.FLOAT, (String)null, this.lastContingentTotalLevelFill), new Tag(Tag.Type.LONG, (String)null, this.takenTime), new Tag(Tag.Type.LONG, (String)null, this.lastUpdate), new Tag(Tag.Type.LONG, (String)null, this.lastApplied), new Tag(Tag.Type.VECTOR3i, (String)null, this.system), new Tag(Tag.Type.BYTE_ARRAY, (String)null, this.resources.res), new Tag(Tag.Type.FLOAT, (String)null, this.resourcesAvailable), this.getContingent().toTag(), new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.markedChangedContingent ? 1 : 0))), new Tag(Tag.Type.FLOAT, (String)null, this.getWeight()), Tag.short2StringMapToTagStruct(this.stationMap), new Tag(Tag.Type.SHORT, (String)null, this.totalAsteroidSectors), new Tag(Tag.Type.SHORT, (String)null, this.totalPlanetSectors), new Tag(Tag.Type.SHORT, (String)null, this.spawedStations), new Tag(Tag.Type.LONG, (String)null, this.minedResources), this instanceof NPCSystem ? ((NPCSystem)this).getFleetManager().toTag() : new Tag(Tag.Type.BYTE, (String)null, (byte)0), FinishTag.INST});
   }

   public void fromTagStructure(Tag var1, int var2) {
      assert var1 != null;

      assert var1.getType() == Tag.Type.STRUCT;

      Tag[] var3 = var1.getStruct();

      assert var3 != null;

      var3[0].getByte();
      this.id = var3[1].getInt();
      this.clientLevel = var3[2].getShort();
      this.clientFactionId = var3[3].getInt();
      this.systemBase = var3[4].getVector3i();
      this.systemBaseUID = var3[5].getString();
      this.distanceFactor = var3[6].getFloat();
      this.lastContingentMaxLevel = var3[7].getInt();
      this.lastContingentTotalLevelFill = var3[8].getFloat();
      this.takenTime = var3[9].getLong();
      this.lastUpdate = var3[10].getLong();
      this.lastApplied = var3[11].getLong();
      this.system = var3[12].getVector3i();
      this.seed = this.calcSeed();

      assert var3[13].getByteArray() != null;

      assert this.resources != null;

      assert this.resources.res != null;

      System.arraycopy(var3[13].getByteArray(), 0, this.resources.res, 0, this.resources.res.length);
      this.resourcesAvailable = var3[14].getFloat();
      if (this instanceof NPCSystem) {
         ((NPCSystem)this).calculateContingent(var2, this.lastContingentMaxLevel, this.lastContingentTotalLevelFill);
      }

      this.getContingent().fromTag(var3[15]);
      this.markedChangedContingent = var3[16].getByte() == 1;
      this.clientWeight = var3[17].getFloat();
      Tag.fromShort2StringMapToTagStruct(var3[18], this.stationMap);
      this.totalAsteroidSectors = var3[19].getShort();
      this.totalPlanetSectors = var3[20].getShort();
      this.spawedStations = var3[21].getShort();
      this.minedResources = var3[22].getLong();
      if (this instanceof NPCSystem) {
         ((NPCSystem)this).getFleetManager().fromTag(var3[23]);
      }

   }

   protected long calcSeed() {
      return (long)(this.system.hashCode() + this.getFactionId() * this.getLevel());
   }

   public int getFactionId() {
      return this.clientFactionId;
   }

   public NPCEntityContingent getContingent() {
      return this.contingent;
   }

   public float getWeight() {
      return this.clientWeight;
   }

   public short getLevel() {
      return this.clientLevel;
   }

   public void log(String var1, LogInterface.LogLevel var2) {
   }
}
