package org.schema.game.server.data.simulation.npc.geo;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.longs.Long2DoubleOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.shorts.Short2IntOpenHashMap;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.LogInterface;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.SendableGameState;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;
import org.schema.game.common.data.element.FixedRecipeProduct;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.missions.machines.states.FleetState;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SectorInformation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.controller.EntityAlreadyExistsException;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GalaxyTmpVars;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.blueprint.ChildStats;
import org.schema.game.server.data.blueprint.SegmentControllerOutline;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.game.server.data.simulation.npc.diplomacy.DiplomacyAction;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class NPCSystemStructure implements LogInterface {
   private GameServerState state;
   private NPCSystemStructure.NPCSystemLevel[] levels;
   private int totalSystems;
   public float totalDistanceWeight;
   private int idGen;
   private final Set taken = new ObjectOpenHashSet();
   public final Long2ObjectOpenHashMap spawnedEntitiesPerSystem = new Long2ObjectOpenHashMap();
   NPCFaction faction;
   private static final byte VERSION = 0;
   private static final int EXPANSION_SEARCH_SYSTEMS = 5;
   private Long2DoubleOpenHashMap valueCache = new Long2DoubleOpenHashMap();
   private Vector3i tmp3 = new Vector3i();
   public final long[] totalResources = new long[16];
   public float totalWeight;
   private Set scheduledChanges = new ObjectOpenHashSet();
   private long lastChangeUpdate;
   private boolean systemsGrown;

   public NPCSystemStructure(GameServerState var1, NPCFaction var2) {
      this.state = var1;
      this.faction = var2;
   }

   public boolean isCreated() {
      return this.levels != null;
   }

   public void createNew() {
      this.levels = new NPCSystemStructure.NPCSystemLevel[1];
      this.levels[0] = new NPCSystemStructure.NPCSystemLevel((short)0);
      NPCSystem var1;
      (var1 = new NPCSystem(this.state, (short)0, this)).system = new Vector3i(this.faction.npcFactionHomeSystem);
      var1.generate();
      this.levels[0].add(var1);
      this.recalc();
      this.faction.setHomebaseRealName(this.faction.getName() + " Home");
      var1.createStationPositions(new Random(), -1);
      if (var1.getHomeBase() == null) {
         throw new NullPointerException("Home system null " + this.faction);
      } else {
         this.faction.setHomebaseUID(SimpleTransformableSendableObject.EntityType.SPACE_STATION.dbPrefix + DatabaseEntry.removePrefixWOException(var1.getSystemBaseUID()));
         this.faction.getHomeSector().set(this.getHomeBase());
         String var2 = (String)var1.stationMap.get(NPCSystem.getLocalIndex(ByteUtil.modU16(var1.getHomeBase().x), ByteUtil.modU16(var1.getHomeBase().y), ByteUtil.modU16(var1.getHomeBase().z)));
         NPCEntityContingent.NPCEntitySpecification var9 = var1.getContingent().getSpec(var2);
         long var3 = this.spawnInDB(this.state, var9, var1, this.faction.getHomeSector(), DatabaseEntry.removePrefixWOException(this.faction.getHomebaseUID()), this.faction.getHomebaseRealName());

         try {
            if (var3 >= 0L) {
               System.err.println("[SERVER][NPC] Creating tradenode for home system: " + var1);
               this.faction.createTradeNode(this.state, var3);
            }
         } catch (SQLException var5) {
            var5.printStackTrace();
         } catch (IOException var6) {
            var6.printStackTrace();
         }

         Random var7 = new Random((long)this.faction.getIdFaction());
         int var8;
         if (this.faction.initialSpawn >= 0) {
            var8 = this.faction.initialSpawn;
         } else {
            var8 = Math.max(0, this.faction.getConfig().initialGrowBaseDefault + (this.faction.getConfig().initialGrowAddedDefaultRandom > 0 ? var7.nextInt(this.faction.getConfig().initialGrowAddedDefaultRandom) : 0));
         }

         this.log("INITIAL SYTEM COUNT: " + var8, LogInterface.LogLevel.NORMAL);

         for(int var10 = 0; var10 < var8; ++var10) {
            this.grow(false);
         }

      }
   }

   public void populateSpaceStation(Sector var1, Random var2) {
      NPCSystem var3;
      if ((var3 = this.getSystem(VoidSystem.getContainingSystem(var1.pos, new Vector3i()))) != null) {
         var3.populateSpaceStation(var1, var2);
      }

   }

   public void populateAsteroids(Sector var1, SectorInformation.SectorType var2, Random var3) throws IOException {
      NPCSystem var4;
      if ((var4 = this.getSystem(VoidSystem.getContainingSystem(var1.pos, new Vector3i()))) != null) {
         var4.populateAsteroids(var1, var2, var3);
      }

   }

   public long spawnInDB(GameServerState var1, NPCEntityContingent.NPCEntitySpecification var2, NPCSystem var3, Vector3i var4, String var5, String var6) {
      BluePrintController var7;
      List var8 = (var7 = this.faction.getConfig().getPreset().blueprintController).readBluePrints();
      BlueprintEntry var9 = null;

      assert var2 != null;

      assert var2.bbName != null;

      Iterator var10 = var8.iterator();

      while(var10.hasNext()) {
         BlueprintEntry var11 = (BlueprintEntry)var10.next();
         if (var2.bbName.toLowerCase(Locale.ENGLISH).equals(var11.getName().toLowerCase(Locale.ENGLISH))) {
            var9 = var11;
            break;
         }
      }

      long var22 = -1L;
      if (var9 != null) {
         Transform var12;
         (var12 = new Transform()).setIdentity();

         try {
            SegmentControllerOutline var19;
            (var19 = var7.loadBluePrint(var1, var9.getName(), var5, var12, -1, this.faction.getIdFaction(), var8, var4, (List)null, "<system>", NPCFaction.buffer, true, (SegmentPiece)null, new ChildStats(true))).spawnSectorId = new Vector3i(var4);
            var19.realName = var6;
            var19.checkForChilds(this.faction.getIdFaction());
            var19.tradeNode = DatabaseEntry.removePrefixWOException(var5).equals(DatabaseEntry.removePrefixWOException(this.faction.getHomebaseUID()));
            ChildStats var18 = new ChildStats(false);
            ObjectArrayList var21 = new ObjectArrayList();
            var22 = var19.spawnInDatabase(var4, var1, 0, var21, var18, true);
            System.err.println("SPAWN IN DB: ADDED: " + var21);
         } catch (EntityNotFountException var13) {
            var13.printStackTrace();
         } catch (IOException var14) {
            var14.printStackTrace();
         } catch (EntityAlreadyExistsException var15) {
            var15.printStackTrace();
            String var20 = var2.type.dbPrefix + DatabaseEntry.removePrefixWOException(var5);
            var22 = var1.getDatabaseIndex().getTableManager().getEntityTable().getIdForFullUID(var20);
            System.err.println("[SERVER] Exception caught sucessfully. DB entry: " + var20 + "; returned id: " + var22);
         } catch (SQLException var16) {
            var16.printStackTrace();
         } catch (StateParameterNotFoundException var17) {
            var17.printStackTrace();
         }
      }

      if (var22 > 0L) {
         var3.getContingent().spawn(var2, var22);
      }

      return var22;
   }

   private long getLocalKey(Vector3i var1) {
      this.tmp3.sub(this.faction.npcFactionHomeSystem, var1);
      return ElementCollection.getIndex(this.tmp3);
   }

   public void iterateTaken(LvlIteratorTakenCallback var1) {
      for(int var2 = 0; var2 < this.levels.length; ++var2) {
         this.levels[var2].iterateTaken(var1);
      }

   }

   public void iterateAllNonFull(LvlIteratorCallback var1) {
      for(int var2 = 0; var2 < this.levels.length; ++var2) {
         if (!this.levels[var2].isFull()) {
            this.levels[var2].iterate(var1);
         }
      }

   }

   public void grow(boolean var1) {
      int var2 = -1;

      for(int var3 = 0; var3 < this.levels.length; ++var3) {
         if (!this.levels[var3].isEmpty()) {
            var2 = var3;
         }
      }

      while(var2 > this.levels.length - 5) {
         this.addLevel();
      }

      NPCSystemStructure.BestExpansionIterator var7 = new NPCSystemStructure.BestExpansionIterator();
      this.iterateAllNonFull(var7);
      if (var7.found()) {
         NPCSystemStructure.NPCSystemLevel var5 = this.levels[var7.bestLvl];
         this.log("GROWING TO " + var7.bestPos, LogInterface.LogLevel.NORMAL);
         NPCSystem var6 = this.addSystemOnGrow(var5, var7.bestPos);
         if (var1) {
            var6.applyToDatabase(true);

            try {
               assert this.state.getUniverse().getStellarSystemFromStellarPos(var7.bestPos).getOwnerFaction() == this.faction.getIdFaction();
            } catch (IOException var4) {
               var4.printStackTrace();
            }

            this.systemsGrown = true;
            this.getState().getFactionManager().markedChangedContingentFactions.add(this.faction);
            this.getState().getUniverse().getGalaxyManager().markZoneDirty(var6.system);
         }

         this.state.getFactionManager().getNpcFactionNews().grown(this.faction.getIdFaction(), var6.system);
      } else {
         this.onNoSystemFoundToGrow();
      }
   }

   private void onNoSystemFoundToGrow() {
   }

   protected double getExpansionValue(Vector3i var1, short var2) {
      return this.faction.getSystemEvaluator().getSystemValue(var1, var2);
   }

   public int getLevel(Vector3i var1) {
      int var2 = var1.x - this.getRoot().x;
      int var3 = var1.y - this.getRoot().y;
      int var4 = var1.z - this.getRoot().z;
      return Math.max(Math.max(Math.abs(var2), Math.abs(var3)), Math.abs(var4));
   }

   private void addLevel() {
      short var1 = this.getLast().lvl;
      NPCSystemStructure.NPCSystemLevel[] var2 = this.levels;
      this.levels = new NPCSystemStructure.NPCSystemLevel[var2.length + 1];
      System.arraycopy(var2, 0, this.levels, 0, var2.length);
      this.levels[this.levels.length - 1] = new NPCSystemStructure.NPCSystemLevel((short)(var1 + 1));
   }

   private NPCSystem addSystemOnGrow(NPCSystemStructure.NPCSystemLevel var1, Vector3i var2) {
      NPCSystem var3;
      (var3 = new NPCSystem(this.state, var1.lvl, this)).system = new Vector3i(var2);
      var3.generate();
      var3.calculateContingent(var1.lvl, this.getLastHabitatedLevel() + 1, this.getTotalLevelFill());
      var1.add(var3);
      var3.createStationPositions(new Random((long)(var3.system.hashCode() * var1.lvl)), -1);
      return var3;
   }

   public void recalcSystem(NPCSystem var1) {
      var1.calculateContingent(var1.getLevel(), this.getLastHabitatedLevel() + 1, this.getTotalLevelFill());
   }

   private void onChangedSystem(NPCSystem var1) {
      assert !var1.isActive() : "System active while updating!";

      int var2;
      if ((var2 = var1.getTotalStationsProjected() - var1.stationMap.size()) > 0) {
         ShortArrayList var6 = var1.createStationPositions(new Random((long)(var1.system.hashCode() * var1.getLevel())), var2);

         try {
            StellarSystem var3 = this.getState().getUniverse().getStellarSystemFromStellarPos(var1.system);
            Iterator var7 = var6.iterator();

            while(var7.hasNext()) {
               short var4 = (Short)var7.next();
               Vector3i var8 = var1.getPosSectorFromLocalCoordinate(var4, new Vector3i());
               this.getState().getDatabaseIndex().getTableManager().getSectorTable().removeSector(var8);
               var8 = VoidSystem.getLocalCoordinates(var8, new Vector3i());
               int var9 = var3.getIndex(var8);
               var3.setSectorType(var9, SectorInformation.SectorType.SPACE_STATION);
               var3.setStationType(var9, SpaceStation.SpaceStationType.FACTION);
            }
         } catch (IOException var5) {
            var5.printStackTrace();
         }

         this.state.getGameState().sendGalaxyModToClients(this.faction.getIdFaction(), var1.systemBaseUID, var1.systemBase);
      }

      var1.clearFleetsAndContingentsOnChange();
      var1.setChangedNT();
   }

   private NPCSystem removeSystemFromLast() {
      for(int var1 = this.levels.length - 1; var1 >= 0; ++var1) {
         if (!this.levels[var1].isEmpty()) {
            NPCSystem var4 = this.levels[var1].getLastAdded();
            this.levels[var1].remove(var4);
            if (this.levels[var1].isEmpty()) {
               NPCSystemStructure.NPCSystemLevel[] var3 = this.levels;
               this.levels = new NPCSystemStructure.NPCSystemLevel[var3.length - 1];
               System.arraycopy(var3, 0, this.levels, 0, this.levels.length);
            }

            var4.onRemovedCleanUp();
            return var4;
         }

         NPCSystemStructure.NPCSystemLevel[] var2 = this.levels;
         this.levels = new NPCSystemStructure.NPCSystemLevel[var2.length - 1];
         System.arraycopy(var2, 0, this.levels, 0, this.levels.length);
      }

      throw new IllegalArgumentException("NPC faction is empty");
   }

   public void removeSystem(NPCSystem var1) {
      this.log("Removing NPC System: " + var1, LogInterface.LogLevel.NORMAL);
      this.levels[var1.getLevel()].remove(var1);
      if (this.levels[var1.getLevel()].isEmpty() && this.levels.length - 1 == var1.getLevel()) {
         NPCSystemStructure.NPCSystemLevel[] var2 = this.levels;
         this.levels = new NPCSystemStructure.NPCSystemLevel[var2.length - 1];
         System.arraycopy(var2, 0, this.levels, 0, this.levels.length);
      }

      var1.onRemovedCleanUp();
   }

   public void shrink() {
      this.removeSystemFromLast();
   }

   public NPCSystemStructure.NPCSystemLevel getLast() {
      return this.levels[this.levels.length - 1];
   }

   public NPCSystemStructure.NPCSystemLevel getOneBeforeLast() {
      return this.levels[this.levels.length - 2];
   }

   public void applyNonExisting() {
      NPCSystemStructure.NPCSystemLevel[] var1;
      int var2 = (var1 = this.levels).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         var1[var3].applyNonExisting();
      }

   }

   public void recalc() {
      NPCSystemStructure.NPCSystemLevel[] var1;
      int var2 = (var1 = this.levels).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         var1[var3].recalc();
      }

   }

   public Vector3i getRoot() {
      return this.levels[0].s[0].system;
   }

   public Tag toTagStructure() {
      Tag[] var1 = new Tag[this.levels.length + 1];

      for(int var2 = 0; var2 < this.levels.length; ++var2) {
         var1[var2] = this.levels[var2].toTagStructure();
      }

      var1[this.levels.length] = FinishTag.INST;
      Tag var3 = new Tag(Tag.Type.STRUCT, (String)null, var1);
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.INT, (String)null, this.idGen), new Tag(Tag.Type.INT, (String)null, this.getLastHabitatedLevel() + 1), new Tag(Tag.Type.FLOAT, (String)null, this.getTotalLevelFill()), var3, new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.systemsGrown ? 1 : 0))), FinishTag.INST});
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var4;
      (var4 = (Tag[])var1.getValue())[0].getByte();
      this.idGen = var4[1].getInt();
      var4[2].getInt();
      var4[3].getFloat();
      Tag[] var2 = var4[4].getStruct();
      this.levels = new NPCSystemStructure.NPCSystemLevel[var2.length - 1];

      for(int var3 = 0; var3 < var2.length - 1; ++var3) {
         this.levels[var3] = new NPCSystemStructure.NPCSystemLevel((short)var3);
         this.levels[var3].fromTagStructure(var2[var3]);
      }

      this.systemsGrown = var4[5].getByte() != 0;
   }

   public int getLastLevel() {
      return this.levels.length - 1;
   }

   public static float getLevelWeight(int var0) {
      return 1.0F / ((float)var0 + 1.0F);
   }

   private static int getLevelSize(int var0) {
      assert var0 >= 0;

      if (var0 == 0) {
         return 1;
      } else {
         int var1 = var0 - 1;
         var0 = (var0 << 1) + 1;
         var1 = (var1 << 1) + 1;
         return var0 * var0 * var0 - var1 * var1 * var1;
      }
   }

   public int getTotalSystems() {
      return this.totalSystems;
   }

   public Vector3i getHomeBase() {
      assert this.levels[0] != null;

      assert this.levels[0].s[0] != null;

      assert this.levels[0].s[0].getSystemBaseSector() != null;

      return this.levels[0].s[0].getSystemBaseSector();
   }

   public int getLastHabitatedLevel() {
      for(int var1 = this.levels.length - 1; var1 >= 0; --var1) {
         if (this.levels[var1].size > 0) {
            return var1;
         }
      }

      return 0;
   }

   public void setChangedNT() {
      this.faction.setChangedNT();
   }

   public void checkNPCFactionSending(SendableGameState var1, boolean var2) {
      NPCSystemStructure.NPCSystemLevel[] var3;
      int var4 = (var3 = this.levels).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         var3[var5].checkNPCFactionSending(var1, var2);
      }

   }

   public float getTotalLevelFill() {
      float var1 = 0.0F;
      NPCSystemStructure.NPCSystemLevel[] var2;
      int var3 = (var2 = this.levels).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         NPCSystemStructure.NPCSystemLevel var5 = var2[var4];
         var1 += var5.getFill();
      }

      return var1;
   }

   public void log(String var1, LogInterface.LogLevel var2) {
      this.faction.log("[GEO]" + var1, var2);
   }

   public void onLoadedSector(Sector var1) {
      Vector3i var2 = VoidSystem.getContainingSystem(var1.pos, this.tmp3);
      NPCSystem var3;
      if (this.taken.contains(var2) && (var3 = this.getSystem(var2)) != null) {
         var3.onLoadedSec(var1);
      }

   }

   public void onUnloadedSector(Sector var1) {
      Vector3i var2 = VoidSystem.getContainingSystem(var1.pos, this.tmp3);
      NPCSystem var3;
      if (this.taken.contains(var2) && (var3 = this.getSystem(var2)) != null) {
         var3.onUnloadedSec(var1);
      }

   }

   public void produce() {
      int var1 = this.getTotalAmountClass(BlueprintClassification.FACTORY_STATION, BlueprintClassification.NONE);
      this.log("Factory entities: " + var1, LogInterface.LogLevel.DEBUG);
      IntOpenHashSet var2 = new IntOpenHashSet();
      this.produceRaw(var2);
      Short2IntOpenHashMap var3 = new Short2IntOpenHashMap();
      this.faction.getDemandDiff(var3);
      this.produceFac(var3, var1, var2);
      if (var2.size() > 0) {
         this.faction.getInventory().sendInventoryModification(var2);
      }

   }

   private int getTotalAmountClass(BlueprintClassification... var1) {
      int var2 = 0;
      NPCSystemStructure.NPCSystemLevel[] var3;
      int var4 = (var3 = this.levels).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         NPCSystemStructure.NPCSystemLevel var6 = var3[var5];
         var2 += var6.getTotalAmountClass(var1);
      }

      return var2;
   }

   private void produceFac(Short2IntOpenHashMap var1, int var2, IntOpenHashSet var3) {
      ShortArrayList var4 = new ShortArrayList(ElementKeyMap.keySet);
      Inventory var5 = this.faction.getInventory();
      int var6 = this.faction.getConfig().productionMultiplier;
      int var7 = this.faction.getConfig().productionStepts;
      var6 = (var2 *= var6) / var7;
      this.log("Number of Factory Steps: " + var7 + ", production amount limit: " + var2 + ", amount per step: " + var6, LogInterface.LogLevel.DEBUG);

      for(var2 = 0; var2 < var7; ++var2) {
         this.log("Step " + var2, LogInterface.LogLevel.DEBUG);
         if (this.produceFacStep(var1, var6, var4, var5, var3) == var6) {
            this.log("Step: Nothing produced previous step so cancelling other steps", LogInterface.LogLevel.DEBUG);
            return;
         }
      }

   }

   private int produceFacStep(Short2IntOpenHashMap var1, int var2, ShortArrayList var3, Inventory var4, IntOpenHashSet var5) {
      Collections.shuffle(var3);
      Iterator var13 = var3.iterator();

      while(var13.hasNext()) {
         short var6;
         ElementInformation var7;
         if (!(var7 = ElementKeyMap.getInfoFast(var6 = (Short)var13.next())).isOre() && !var7.isCapsule() && !var7.getConsistence().isEmpty()) {
            int var8 = -1;
            int var9;
            if ((var9 = var1.get(var6)) > 0) {
               Iterator var10 = var7.getConsistence().iterator();

               label68:
               while(true) {
                  int var12;
                  do {
                     if (!var10.hasNext()) {
                        this.log("In Demand of: " + ElementKeyMap.toString(var6) + " x " + var9 + ", max able to produce: " + var8 + ", prodLim: " + var2, LogInterface.LogLevel.DEBUG);
                        break label68;
                     }

                     FactoryResource var11 = (FactoryResource)var10.next();
                     var12 = Math.max(0, var4.getOverallQuantity(var11.type) - Math.min(this.faction.getDemand(var11.type, this.faction.getConfig().expensionDemandMult), this.faction.getProdWeightedDemand(var11.type, this.faction.getConfig().expensionDemandMult))) / var11.count;
                  } while(var8 >= 0 && var12 >= var8);

                  var8 = var12;
               }
            }

            if (var8 > 0 && var2 > 0 && var9 > 0) {
               int var15 = Math.min(var2, Math.min(var9, var8));
               this.log("PRODUCING FAC: " + ElementKeyMap.toString(var6) + "x" + var15, LogInterface.LogLevel.DEBUG);
               if (var15 > 0) {
                  StringBuffer var16 = new StringBuffer();
                  Iterator var14 = var7.getConsistence().iterator();

                  while(var14.hasNext()) {
                     FactoryResource var17 = (FactoryResource)var14.next();
                     var4.decreaseBatch(var17.type, var17.count * var15, var5);
                     var16.append(ElementKeyMap.toString(var17.type) + "x" + var17.count * var15 + ", ");
                  }

                  this.log("Produced FAC: " + ElementKeyMap.toString(var6) + "x" + var15 + " from " + var16 + "; demand(" + var9 + ", maxProd: " + var8 + ")", LogInterface.LogLevel.FINE);
                  var5.add(var4.incExistingOrNextFreeSlot(var6, var15));
               }

               if ((var2 -= var15) == 0) {
                  break;
               }

               if (var2 < 0) {
                  throw new IllegalArgumentException();
               }
            }
         }
      }

      if (var2 > 0) {
         this.log("----- Remaining limit at the end of step: " + var2, LogInterface.LogLevel.DEBUG);
      }

      return var2;
   }

   private void produceRaw(IntOpenHashSet var1) {
      int var2 = this.getTotalAmountClass(BlueprintClassification.FACTORY_STATION, BlueprintClassification.NONE);
      Inventory var3 = this.faction.getInventory();
      short[] var4;
      int var5 = (var4 = ElementKeyMap.typeList()).length;

      for(int var6 = 0; var6 < var5; ++var6) {
         short var7;
         ElementInformation var8;
         if ((var8 = ElementKeyMap.getInfoFast(var7 = var4[var6])).consistence == null || var8.consistence.isEmpty()) {
            this.produceRaw(var1, var7, var3, var2);
         }
      }

   }

   private void produceRaw(IntOpenHashSet var1, short var2, Inventory var3, int var4) {
      FixedRecipeProduct[] var5;
      int var6 = (var5 = ElementKeyMap.capsuleRecipe.recipeProducts).length;

      for(int var7 = 0; var7 < var6; ++var7) {
         FixedRecipeProduct var8;
         if ((var8 = var5[var7]).input[0].type == var2) {
            int var9 = var3.getOverallQuantity(var2);
            this.log("Raw Res available: " + ElementKeyMap.toString(var2) + "x" + var9, LogInterface.LogLevel.FINE);
            if ((var9 = Math.min(var4 * this.faction.getConfig().productionMultiplier / 16, var9)) > 0) {
               FactoryResource[] var14;
               int var10 = (var14 = var8.output).length;

               for(int var11 = 0; var11 < var10; ++var11) {
                  FactoryResource var12;
                  int var13 = (var12 = var14[var11]).count * var9;
                  var1.add(var3.incExistingOrNextFreeSlot(var12.type, var13));
                  this.log("Produced from RAW: " + ElementKeyMap.toString(var12.type) + "x" + var13 + " from " + ElementKeyMap.toString(var2) + "x" + var9, LogInterface.LogLevel.DEBUG);
               }

               var3.decreaseBatch(var2, var9, var1);
            }
         }
      }

   }

   public NPCSystem getSystem(Vector3i var1) {
      int var2;
      if (var1 != null && this.taken.contains(var1) && (var2 = this.getLevel(var1)) < this.levels.length) {
         return this.levels[var2].get(var1);
      } else {
         this.log("ERROR: System not found: " + var1, LogInterface.LogLevel.DEBUG);
         return null;
      }
   }

   public boolean removeEntity(long var1, SegmentController var3, boolean var4) {
      Vector3i var5;
      if ((var5 = (Vector3i)this.spawnedEntitiesPerSystem.get(var1)) != null) {
         NPCSystem var6;
         return (var6 = this.getSystem(var5)) != null ? var6.getContingent().spawnedEntities.remove(var1, var3, var4) : false;
      } else {
         return false;
      }
   }

   public GameServerState getState() {
      return this.state;
   }

   public void onCommandPartFinished(Fleet var1, FleetState var2) {
      Vector3i var3 = var1.getNpcSystem();
      NPCSystem var4;
      if ((var4 = this.getSystem(var3)) != null) {
         var4.onCommandPartFinished(var1, var2);
      } else {
         this.log("Patrol ERROR: fleet system not found: " + var4 + " for " + var1, LogInterface.LogLevel.ERROR);
      }
   }

   public void mine(long var1) {
      NPCSystemStructure.NPCSystemLevel[] var3;
      int var4 = (var3 = this.levels).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         var3[var5].mine(var1);
      }

   }

   public void resupply(long var1) {
      NPCSystemStructure.NPCSystemLevel[] var3;
      int var4 = (var3 = this.levels).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         var3[var5].resupply(var1);
      }

   }

   public void consume(long var1) {
      NPCSystemStructure.NPCSystemLevel[] var3;
      int var4 = (var3 = this.levels).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         var3[var5].consume(var1);
      }

   }

   public void scheduleSystemContingentChange(NPCSystem var1) {
      this.scheduledChanges.add(var1);
      this.getState().getFactionManager().markedChangedContingentFactions.add(this.faction);
   }

   public boolean updateChangedSystems(long var1) {
      if (this.systemsGrown) {
         this.iterateTaken(new LvlIteratorTakenCallback() {
            public void it(NPCSystem var1) {
               NPCSystemStructure.this.recalcSystem(var1);
            }
         });
         this.systemsGrown = false;
         return true;
      } else if (this.scheduledChanges.isEmpty()) {
         return false;
      } else {
         if ((double)(var1 - this.lastChangeUpdate) > 60000.0D + Math.random() * 5000.0D - 2500.0D) {
            for(Iterator var3 = this.scheduledChanges.iterator(); var3.hasNext(); this.lastChangeUpdate = var1) {
               NPCSystem var4;
               if (!(var4 = (NPCSystem)var3.next()).isActive()) {
                  this.onChangedSystem(var4);
                  this.lastChangeUpdate = var1;
                  break;
               }
            }
         }

         return !this.scheduledChanges.isEmpty();
      }
   }

   public NPCSystem findClosestFrom(Vector3i var1, int var2) {
      int var3 = this.getLastHabitatedLevel();
      var2 = Math.max(0, var2);
      NPCSystem var4 = null;
      float var5 = 0.0F;

      for(int var6 = var3; var6 >= Math.max(0, var3 - var2); --var6) {
         NPCSystemStructure.NPCSystemLevel var7 = this.levels[var6];

         for(int var8 = 0; var8 < var7.size; ++var8) {
            NPCSystem var9;
            float var10 = Vector3i.getDisatance((var9 = var7.s[var8]).system, var1);
            if (var4 == null || var10 < var5) {
               var5 = var10;
               var4 = var9;
            }
         }
      }

      return var4;
   }

   class NPCSystemLevel {
      private short lvl;
      private int size;
      private NPCSystem[] s;
      private final int maxSize;

      public NPCSystemLevel(short var2) {
         this.lvl = var2;
         this.maxSize = NPCSystemStructure.getLevelSize(var2);
         this.s = new NPCSystem[this.maxSize];
      }

      public void iterateTaken(LvlIteratorTakenCallback var1) {
         for(int var2 = 0; var2 < this.size; ++var2) {
            var1.it(this.s[var2]);
         }

      }

      public void applyNonExisting() {
         for(int var1 = 0; var1 < this.size; ++var1) {
            this.s[var1].applyToDatabase(false);
         }

      }

      public void recalc() {
         for(int var1 = 0; var1 < this.size; ++var1) {
            this.s[var1].calculateContingent(this.lvl, NPCSystemStructure.this.getLastHabitatedLevel() + 1, NPCSystemStructure.this.getTotalLevelFill());
         }

      }

      public float getFill() {
         return (float)this.size / (float)this.maxSize;
      }

      public NPCSystem getLastAdded() {
         return this.s[this.size - 1];
      }

      public void iterate(LvlIteratorCallback var1) {
         if (this.lvl == 0) {
            if (NPCSystemStructure.this.taken.contains(NPCSystemStructure.this.getRoot())) {
               var1.handleTaken(NPCSystemStructure.this.getRoot().x, NPCSystemStructure.this.getRoot().y, NPCSystemStructure.this.getRoot().z, this.lvl);
            } else {
               var1.handleFree(NPCSystemStructure.this.getRoot().x, NPCSystemStructure.this.getRoot().y, NPCSystemStructure.this.getRoot().z, this.lvl);
            }
         } else {
            int var2 = 0;
            Vector3i var3 = new Vector3i();

            int var4;
            int var5;
            short var6;
            int var7;
            for(var4 = -this.lvl; var4 <= this.lvl; ++var4) {
               for(var5 = -this.lvl; var5 <= this.lvl; ++var5) {
                  var6 = this.lvl;
                  var3.set(var4, var6, var5);
                  var3.add(NPCSystemStructure.this.getRoot());
                  if (NPCSystemStructure.this.taken.contains(var3)) {
                     var1.handleTaken(var3.x, var3.y, var3.z, this.lvl);
                  } else {
                     var1.handleFree(var3.x, var3.y, var3.z, this.lvl);
                  }

                  ++var2;
                  var7 = -this.lvl;
                  var3.set(var4, var7, var5);
                  var3.add(NPCSystemStructure.this.getRoot());
                  if (NPCSystemStructure.this.taken.contains(var3)) {
                     var1.handleTaken(var3.x, var3.y, var3.z, this.lvl);
                  } else {
                     var1.handleFree(var3.x, var3.y, var3.z, this.lvl);
                  }

                  ++var2;
               }
            }

            for(var4 = -this.lvl + 1; var4 <= this.lvl - 1; ++var4) {
               for(var5 = -this.lvl; var5 <= this.lvl; ++var5) {
                  var6 = this.lvl;
                  var3.set(var5, var4, var6);
                  var3.add(NPCSystemStructure.this.getRoot());
                  if (NPCSystemStructure.this.taken.contains(var3)) {
                     var1.handleTaken(var3.x, var3.y, var3.z, this.lvl);
                  } else {
                     var1.handleFree(var3.x, var3.y, var3.z, this.lvl);
                  }

                  ++var2;
                  var7 = -this.lvl;
                  var3.set(var5, var4, var7);
                  var3.add(NPCSystemStructure.this.getRoot());
                  if (NPCSystemStructure.this.taken.contains(var3)) {
                     var1.handleTaken(var3.x, var3.y, var3.z, this.lvl);
                  } else {
                     var1.handleFree(var3.x, var3.y, var3.z, this.lvl);
                  }

                  ++var2;
               }

               for(var5 = -this.lvl + 1; var5 <= this.lvl - 1; ++var5) {
                  var6 = this.lvl;
                  var3.set(var6, var4, var5);
                  var3.add(NPCSystemStructure.this.getRoot());
                  if (NPCSystemStructure.this.taken.contains(var3)) {
                     var1.handleTaken(var3.x, var3.y, var3.z, this.lvl);
                  } else {
                     var1.handleFree(var3.x, var3.y, var3.z, this.lvl);
                  }

                  ++var2;
                  var7 = -this.lvl;
                  var3.set(var7, var4, var5);
                  var3.add(NPCSystemStructure.this.getRoot());
                  if (NPCSystemStructure.this.taken.contains(var3)) {
                     var1.handleTaken(var3.x, var3.y, var3.z, this.lvl);
                  } else {
                     var1.handleFree(var3.x, var3.y, var3.z, this.lvl);
                  }

                  ++var2;
               }
            }

            assert var2 == this.maxSize : this.lvl + " -> " + var2 + "/" + this.maxSize;

         }
      }

      public boolean isFull() {
         return this.maxSize == this.size;
      }

      public boolean isEmpty() {
         return this.size == 0;
      }

      private void add(NPCSystem var1) {
         if (this.size == this.s.length) {
            throw new IndexOutOfBoundsException("Index: " + this.size);
         } else {
            this.s[this.size] = var1;
            if (var1.id < 0) {
               var1.id = ++NPCSystemStructure.this.idGen;
            }

            NPCSystemStructure.this.taken.add(var1.system);
            this.addResourcesToTotal(var1);
            ++this.size;
            NPCSystemStructure.this.totalSystems++;
            NPCSystemStructure var10000 = NPCSystemStructure.this;
            var10000.totalWeight += NPCSystemStructure.getLevelWeight(this.lvl);

            assert var1.distanceFactor >= 0.0F;

            var10000 = NPCSystemStructure.this;
            var10000.totalDistanceWeight += var1.distanceFactor;
         }
      }

      private void addResourcesToTotal(NPCSystem var1) {
         for(int var2 = 0; var2 < 16; ++var2) {
            long[] var10000 = NPCSystemStructure.this.totalResources;
            var10000[var2] += (long)var1.resources.res[var2];
         }

      }

      private void removeResourcesFromTotal(NPCSystem var1) {
         for(int var2 = 0; var2 < 16; ++var2) {
            long[] var10000 = NPCSystemStructure.this.totalResources;
            var10000[var2] += (long)var1.resources.res[var2];
         }

      }

      public void remove(NPCSystem var1) {
         if (this.size == 0) {
            throw new IndexOutOfBoundsException("Index: " + this.size);
         } else {
            NPCSystem[] var2 = this.s;
            this.s = new NPCSystem[var2.length];
            int var3 = 0;

            for(int var4 = 0; var4 < this.size; ++var4) {
               if (!var1.equals(var2[var4])) {
                  this.s[var3] = var2[var4];
                  ++var3;
               }
            }

            this.removeResourcesFromTotal(var1);
            NPCSystemStructure.this.taken.remove(var1.system);
            NPCSystemStructure var10000 = NPCSystemStructure.this;
            var10000.totalWeight -= NPCSystemStructure.getLevelWeight(this.lvl);
            var10000 = NPCSystemStructure.this;
            var10000.totalDistanceWeight -= var1.distanceFactor;
            --this.size;
            NPCSystemStructure.this.totalSystems--;
         }
      }

      public void fromTagStructure(Tag var1) {
         Tag[] var4 = (Tag[])var1.getValue();
         this.lvl = (Short)var4[0].getValue();
         this.size = (Integer)var4[1].getValue();
         var4 = var4[2].getStruct();

         assert var4 != null;

         for(int var2 = 0; var2 < this.size; ++var2) {
            assert var4[var2] != null;

            NPCSystem var3;
            (var3 = new NPCSystem(NPCSystemStructure.this.state, this.lvl, NPCSystemStructure.this)).fromTagStructure(var4[var2], this.lvl);
            this.s[var2] = var3;
            this.addResourcesToTotal(this.s[var2]);
            NPCSystemStructure.this.taken.add(this.s[var2].system);
            NPCSystemStructure.this.totalSystems++;
            NPCSystemStructure var10000 = NPCSystemStructure.this;
            var10000.totalWeight += NPCSystemStructure.getLevelWeight(this.lvl);

            assert var3.distanceFactor >= 0.0F;

            var10000 = NPCSystemStructure.this;
            var10000.totalDistanceWeight += var3.distanceFactor;
         }

      }

      public Tag toTagStructure() {
         Tag[] var1 = new Tag[this.size + 1];

         for(int var2 = 0; var2 < this.size; ++var2) {
            var1[var2] = this.s[var2].toTagStructure();
         }

         var1[this.size] = FinishTag.INST;
         Tag var3 = new Tag(Tag.Type.STRUCT, (String)null, var1);
         return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.SHORT, (String)null, this.lvl), new Tag(Tag.Type.INT, (String)null, this.size), var3, FinishTag.INST});
      }

      public void checkNPCFactionSending(SendableGameState var1, boolean var2) {
         for(int var3 = 0; var3 < this.size; ++var3) {
            this.s[var3].checkNPCFactionSending(var1, var2);
         }

      }

      public int getTotalAmountClass(BlueprintClassification... var1) {
         int var2 = 0;

         for(int var3 = 0; var3 < this.size; ++var3) {
            var2 += this.s[var3].getTotalAmountClass(var1);
         }

         return var2;
      }

      public NPCSystem get(Vector3i var1) {
         for(int var2 = 0; var2 < this.size; ++var2) {
            if (this.s[var2].system.equals(var1)) {
               return this.s[var2];
            }
         }

         return null;
      }

      public void mine(long var1) {
         for(int var3 = 0; var3 < this.size; ++var3) {
            this.s[var3].mine(var1);
         }

      }

      public void resupply(long var1) {
         for(int var3 = 0; var3 < this.size; ++var3) {
            this.s[var3].resupply(var1);
         }

      }

      public void consume(long var1) {
         for(int var3 = 0; var3 < this.size; ++var3) {
            this.s[var3].consume(var1);
         }

      }
   }

   class BestExpansionIterator implements LvlIteratorCallback {
      Vector3i bestPos = new Vector3i();
      double best = Double.NEGATIVE_INFINITY;
      short bestLvl = 0;
      private Vector3i tmp = new Vector3i();
      private Vector3i tmp2 = new Vector3i();

      public BestExpansionIterator() {
         assert -1.23894398234E11D > this.best;

      }

      public void handleTaken(int var1, int var2, int var3, short var4) {
      }

      private boolean containsLvl(Vector3i var1) {
         this.tmp2.set(1, 0, 0);
         this.tmp2.add(var1);
         if (NPCSystemStructure.this.taken.contains(this.tmp2)) {
            return true;
         } else {
            this.tmp2.set(-1, 0, 0);
            this.tmp2.add(var1);
            if (NPCSystemStructure.this.taken.contains(this.tmp2)) {
               return true;
            } else {
               this.tmp2.set(0, 1, 0);
               this.tmp2.add(var1);
               if (NPCSystemStructure.this.taken.contains(this.tmp2)) {
                  return true;
               } else {
                  this.tmp2.set(0, -1, 0);
                  this.tmp2.add(var1);
                  if (NPCSystemStructure.this.taken.contains(this.tmp2)) {
                     return true;
                  } else {
                     this.tmp2.set(0, 0, 1);
                     this.tmp2.add(var1);
                     if (NPCSystemStructure.this.taken.contains(this.tmp2)) {
                        return true;
                     } else {
                        this.tmp2.set(0, 0, -1);
                        this.tmp2.add(var1);
                        return NPCSystemStructure.this.taken.contains(this.tmp2);
                     }
                  }
               }
            }
         }
      }

      public void handleFree(int var1, int var2, int var3, short var4) {
         this.tmp.set(var1, var2, var3);
         if (this.containsLvl(this.tmp)) {
            this.tmp.set(var1, var2, var3);
            long var7 = NPCSystemStructure.this.getLocalKey(this.tmp);
            double var5;
            if (!NPCSystemStructure.this.valueCache.containsKey(var7)) {
               var5 = NPCSystemStructure.this.getExpansionValue(this.tmp, var4);
               NPCSystemStructure.this.valueCache.put(var7, var5);
            } else {
               var5 = NPCSystemStructure.this.valueCache.get(var7);
            }

            if (var5 == -9.99988887777E11D) {
               Galaxy var9 = NPCSystemStructure.this.getState().getUniverse().getGalaxyFromSystemPos(this.tmp);
               FactionResourceRequestContainer var10;
               if ((var10 = NPCSystemStructure.this.state.getUniverse().updateSystemResourcesWithDatabaseValues(this.tmp, var9, new FactionResourceRequestContainer(), new GalaxyTmpVars())).factionId != 0 && var10.factionId != NPCSystemStructure.this.faction.getIdFaction()) {
                  NPCSystemStructure.this.state.getFactionManager().diplomacyAction(DiplomacyAction.DiplActionType.TERRITORY, NPCSystemStructure.this.faction.getIdFaction(), (long)var10.factionId);
                  NPCSystemStructure.this.log("someone has territory close to us. we might not like that (TERRITORY diplomacy action triggered)", LogInterface.LogLevel.NORMAL);
               }
            }

            if (var5 > this.best) {
               this.best = var5;
               this.bestPos.set(this.tmp);
               this.bestLvl = var4;
            }
         }

      }

      public boolean found() {
         return this.best != Double.NEGATIVE_INFINITY && this.best != -9.99988887777E11D;
      }
   }
}
