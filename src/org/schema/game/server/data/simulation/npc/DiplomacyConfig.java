package org.schema.game.server.data.simulation.npc;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2LongOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.schema.common.config.ConfigParserException;
import org.schema.game.server.data.simulation.npc.diplomacy.DiplomacyAction;
import org.schema.game.server.data.simulation.npc.diplomacy.DiplomacyCondition;
import org.schema.game.server.data.simulation.npc.diplomacy.DiplomacyConditionGroup;
import org.schema.game.server.data.simulation.npc.diplomacy.DiplomacyReaction;
import org.schema.game.server.data.simulation.npc.diplomacy.NPCDiplomacyEntity;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DiplomacyConfig {
   public static final byte VERSION = 2;
   private final Int2ObjectOpenHashMap map = new Int2ObjectOpenHashMap();
   public final List reactions = new ObjectArrayList();
   public byte version = 0;
   public final Object2LongOpenHashMap actionTimeoutMap = new Object2LongOpenHashMap();

   public void appendXML(Document var1, Element var2) {
      Comment var3 = var1.createComment("Diplomacy configuration. There are actions and states. States will add a constant effect on points as long as its active. Actions add/remove over time as long as the action is active. Actions get reset if repeated. Reactions can exist both for actions (checked and executed when that action happens) or general, which are executed on action/status turn.");
      var2.appendChild(var3);
      var2.appendChild(var1.createComment("Status Types: " + NPCDiplomacyEntity.DiplStatusType.list()));
      var2.appendChild(var1.createComment("Action Types: " + DiplomacyAction.DiplActionType.list()));
      var2.appendChild(var1.createComment("Reaction Types: " + DiplomacyReaction.ConditionReaction.list()));
      var2.appendChild(var1.createComment("Condition Types: " + DiplomacyCondition.ConditionType.list()));
      Element var10;
      (var10 = var1.createElement("Version")).setTextContent("2");
      Comment var4 = var1.createComment("To ensure compatibility on updates, the Diplomacy config will reset to a new default, should the version differ with what the game considers to be the latest diplomacy config format version");
      var10.appendChild(var4);
      var2.appendChild(var10);
      (var10 = var1.createElement("ActionTimeouts")).appendChild(var1.createComment("How long actions are valid in milliseconds."));
      DiplomacyAction.DiplActionType[] var12;
      int var5 = (var12 = DiplomacyAction.DiplActionType.values()).length;

      for(int var6 = 0; var6 < var5; ++var6) {
         DiplomacyAction.DiplActionType var7 = var12[var6];
         Element var8;
         (var8 = var1.createElement(var7.name().substring(0, 1) + var7.name().substring(1).toLowerCase(Locale.ENGLISH))).setTextContent(String.valueOf(this.actionTimeoutMap.get(var7)));
         var10.appendChild(var8);
      }

      var2.appendChild(var10);
      Element var14 = var1.createElement("DiplomacyElement");
      boolean var15 = false;
      DiplomacyAction.DiplActionType[] var16;
      int var18 = (var16 = DiplomacyAction.DiplActionType.values()).length;

      DiplomacyConfig.DiplomacyConfigElement var9;
      int var21;
      for(var21 = 0; var21 < var18; ++var21) {
         DiplomacyAction.DiplActionType var11 = var16[var21];
         if ((var9 = this.get(var11)) != null) {
            var9.appendXML(var1, var14, var11);
            var15 = true;
         }
      }

      NPCDiplomacyEntity.DiplStatusType[] var17;
      var18 = (var17 = NPCDiplomacyEntity.DiplStatusType.values()).length;

      for(var21 = 0; var21 < var18; ++var21) {
         NPCDiplomacyEntity.DiplStatusType var13 = var17[var21];
         if ((var9 = this.get(var13)) != null) {
            var9.appendXML(var1, var14, var13);
            var15 = true;
         }
      }

      if (var15) {
         var2.appendChild(var14);
      }

      if (this.reactions.size() == 0) {
         this.addDefaultActions();
      }

      if (this.reactions.size() > 0) {
         Element var19 = var1.createElement("Reactions");
         Iterator var20 = this.reactions.iterator();

         while(var20.hasNext()) {
            ((DiplomacyReaction)var20.next()).appendXML(var1, var19);
         }

         var2.appendChild(var19);
      }

   }

   public void parse(Node var1) throws ConfigParserException {
      if (!NPCFactionConfig.recreate) {
         this.reactions.clear();
      }

      NodeList var11 = var1.getChildNodes();

      for(int var2 = 0; var2 < var11.getLength(); ++var2) {
         Node var3;
         if ((var3 = var11.item(var2)).getNodeType() == 1 && var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("version")) {
            this.version = Byte.parseByte(var3.getTextContent());
         }

         NodeList var4;
         int var5;
         Node var6;
         if (var3.getNodeType() == 1 && var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("actiontimeouts")) {
            var4 = var3.getChildNodes();

            for(var5 = 0; var5 < var4.getLength(); ++var5) {
               if ((var6 = var4.item(var5)).getNodeType() == 1) {
                  try {
                     DiplomacyAction.DiplActionType var7 = DiplomacyAction.DiplActionType.valueOf(var6.getNodeName().toUpperCase(Locale.ENGLISH));
                     long var8 = Long.parseLong(var6.getTextContent());
                     this.actionTimeoutMap.put(var7, var8);
                  } catch (Exception var10) {
                     var10.printStackTrace();
                  }
               }
            }
         }

         if (var3.getNodeType() == 1 && var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("diplomacyelement")) {
            var4 = var3.getChildNodes();

            for(var5 = 0; var5 < var4.getLength(); ++var5) {
               if ((var6 = var4.item(var5)).getNodeType() == 1 && (var6.getNodeName().toLowerCase(Locale.ENGLISH).equals("diplaction") || var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("diplstatus"))) {
                  (new DiplomacyConfig.DiplomacyConfigElement()).parse(var6);
               }
            }
         }

         if (var3.getNodeType() == 1 && var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("reactions")) {
            this.parseReactions(var3);
         }
      }

   }

   private void parseReactions(Node var1) {
      NodeList var6 = var1.getChildNodes();
      int var2 = 0;

      for(int var3 = 0; var3 < var6.getLength(); ++var3) {
         Node var4;
         if ((var4 = var6.item(var3)).getNodeType() == 1) {
            try {
               if (var4.getNodeName().toLowerCase(Locale.ENGLISH).equals("reaction")) {
                  this.reactions.add(DiplomacyReaction.parse(var4, var2++));
               }
            } catch (ConfigParserException var5) {
               var5.printStackTrace();
               System.err.println("NOT USING REACTION BECAUSE OF EXCEPTION");
            }
         }
      }

   }

   public DiplomacyConfig() {
      DiplomacyAction.DiplActionType[] var1;
      int var2 = (var1 = DiplomacyAction.DiplActionType.values()).length;

      int var3;
      DiplomacyAction.DiplActionType var4;
      for(var3 = 0; var3 < var2; ++var3) {
         var4 = var1[var3];
         this.actionTimeoutMap.put(var4, 300000L);
      }

      var2 = (var1 = DiplomacyAction.DiplActionType.values()).length;

      DiplomacyConfig.DiplomacyConfigElement var5;
      for(var3 = 0; var3 < var2; ++var3) {
         var4 = var1[var3];
         (var5 = new DiplomacyConfig.DiplomacyConfigElement()).actionType = var4;
         DiplomacyReaction var6;
         DiplomacyCondition var7;
         switch(var4) {
         case ALLIANCE_WITH_ENEMY:
            var5.upperLimit = -10;
            var5.lowerLimit = 0;
            var5.value = -10;
            break;
         case ALLIANCE_CANCEL:
            var5.upperLimit = -10;
            var5.lowerLimit = 0;
            var5.value = -10;
            break;
         case ALLIANCE_REQUEST:
            var5.upperLimit = 0;
            var5.lowerLimit = 0;
            (var6 = new DiplomacyReaction(-1)).reaction = DiplomacyReaction.ConditionReaction.ACCEPT_ALLIANCE_OFFER;
            var6.condition = new DiplomacyConditionGroup();
            var6.condition.mod = DiplomacyConditionGroup.ConditionMod.AND;
            (var7 = new DiplomacyCondition()).type = DiplomacyCondition.ConditionType.RAW_POINTS;
            var7.argumentValue = 1000.0D;
            var6.condition.conditions.add(var7);
            (var7 = new DiplomacyCondition()).type = DiplomacyCondition.ConditionType.STATUS_PERSISTED;
            var7.argumentStatus = NPCDiplomacyEntity.DiplStatusType.NON_AGGRESSION;
            var7.argumentValue = 1800000.0D;
            var6.condition.conditions.add(var7);
            var5.reaction = var6;
            break;
         case ATTACK:
            var5.upperLimit = -500;
            var5.lowerLimit = 0;
            var5.value = -50;
            break;
         case ATTACK_ENEMY:
            var5.upperLimit = 500;
            var5.lowerLimit = 0;
            var5.value = 50;
            break;
         case DECLARATION_OF_WAR:
            var5.upperLimit = -1000;
            var5.lowerLimit = 0;
            var5.value = -1000;
            break;
         case MINING:
            var5.upperLimit = -30;
            var5.lowerLimit = 0;
            var5.value = -30;
            break;
         case PEACE_OFFER:
            var5.upperLimit = 0;
            var5.lowerLimit = 0;
            (var6 = new DiplomacyReaction(-1)).reaction = DiplomacyReaction.ConditionReaction.ACCEPT_PEACE_OFFER;
            var6.condition = new DiplomacyConditionGroup();
            var6.condition.mod = DiplomacyConditionGroup.ConditionMod.AND;
            (var7 = new DiplomacyCondition()).type = DiplomacyCondition.ConditionType.RAW_POINTS;
            var7.argumentValue = -500.0D;
            var6.condition.conditions.add(var7);
            (var7 = new DiplomacyCondition()).type = DiplomacyCondition.ConditionType.STATUS_PERSISTED;
            var7.argumentStatus = NPCDiplomacyEntity.DiplStatusType.NON_AGGRESSION;
            var7.argumentValue = 1800000.0D;
            var6.condition.conditions.add(var7);
            var5.reaction = var6;
            break;
         case TERRITORY:
            var5.upperLimit = -300;
            var5.lowerLimit = 0;
            var5.value = -1;
            break;
         case TRADING_WITH_US:
            var5.upperLimit = 300;
            var5.lowerLimit = 0;
            var5.value = 1;
            break;
         case TRADING_WITH_ENEMY:
            var5.upperLimit = 0;
            var5.lowerLimit = -200;
            var5.value = -1;
         }

         this.put(var4, var5);
      }

      NPCDiplomacyEntity.DiplStatusType[] var8;
      var2 = (var8 = NPCDiplomacyEntity.DiplStatusType.values()).length;

      for(var3 = 0; var3 < var2; ++var3) {
         NPCDiplomacyEntity.DiplStatusType var9 = var8[var3];
         (var5 = new DiplomacyConfig.DiplomacyConfigElement()).statusType = var9;
         switch(var9) {
         case ALLIANCE:
            var5.value = 1000;
            break;
         case ALLIANCE_WITH_ENEMY:
            var5.value = -500;
            break;
         case CLOSE_TERRITORY:
            var5.value = -100;
            break;
         case IN_WAR:
            var5.value = -10000;
            break;
         case IN_WAR_WITH_ENEMY:
            var5.value = 500;
            break;
         case PIRATE:
            continue;
         case POWER:
            var5.value = 1;
            break;
         case NON_AGGRESSION:
            var5.value = 1;
            break;
         case ALLIANCE_WITH_FRIENDS:
            var5.value = 10;
            break;
         case IN_WAR_WITH_FRIENDS:
            var5.value = -10;
            break;
         case FACTION_MEMBER_AT_WAR_WITH_US:
            var5.value = -500;
            break;
         case FACTION_MEMBER_WE_DONT_LIKE:
            var5.value = -50;
            break;
         default:
            var5.value = -123;
         }

         this.put(var9, var5);
      }

   }

   public void addDefaultActions() {
      DiplomacyReaction var1;
      (var1 = new DiplomacyReaction(0)).reaction = DiplomacyReaction.ConditionReaction.DECLARE_WAR;
      var1.name = "Declare War on bad score";
      var1.condition = new DiplomacyConditionGroup();
      var1.condition.mod = DiplomacyConditionGroup.ConditionMod.NOT;
      DiplomacyCondition var2;
      (var2 = new DiplomacyCondition()).type = DiplomacyCondition.ConditionType.TOTAL_POINTS;
      var2.argumentValue = -1000.0D;
      var1.condition.conditions.add(var2);
      this.reactions.add(var1);
      (var1 = new DiplomacyReaction(1)).reaction = DiplomacyReaction.ConditionReaction.OFFER_PEACE_DEAL;
      var1.name = "Offer Peace Deal";
      var1.condition = new DiplomacyConditionGroup();
      var1.condition.mod = DiplomacyConditionGroup.ConditionMod.AND;
      (var2 = new DiplomacyCondition()).type = DiplomacyCondition.ConditionType.TOTAL_POINTS;
      var2.argumentValue = -500.0D;
      var1.condition.conditions.add(var2);
      (var2 = new DiplomacyCondition()).type = DiplomacyCondition.ConditionType.STATUS_PERSISTED;
      var2.argumentStatus = NPCDiplomacyEntity.DiplStatusType.NON_AGGRESSION;
      var2.argumentValue = 900000.0D;
      var1.condition.conditions.add(var2);
      this.reactions.add(var1);
      (var1 = new DiplomacyReaction(2)).name = "Offer Peace Deal on non agression only";
      var1.reaction = DiplomacyReaction.ConditionReaction.OFFER_PEACE_DEAL;
      var1.condition = new DiplomacyConditionGroup();
      var1.condition.mod = DiplomacyConditionGroup.ConditionMod.AND;
      (var2 = new DiplomacyCondition()).type = DiplomacyCondition.ConditionType.STATUS_PERSISTED;
      var2.argumentStatus = NPCDiplomacyEntity.DiplStatusType.NON_AGGRESSION;
      var2.argumentValue = 7200000.0D;
      var1.condition.conditions.add(var2);
      this.reactions.add(var1);
      (var1 = new DiplomacyReaction(3)).name = "Offer Alliance";
      var1.reaction = DiplomacyReaction.ConditionReaction.OFFER_ALLIANCE;
      var1.condition = new DiplomacyConditionGroup();
      var1.condition.mod = DiplomacyConditionGroup.ConditionMod.AND;
      (var2 = new DiplomacyCondition()).type = DiplomacyCondition.ConditionType.TOTAL_POINTS;
      var2.argumentValue = 1000.0D;
      var1.condition.conditions.add(var2);
      (var2 = new DiplomacyCondition()).type = DiplomacyCondition.ConditionType.STATUS_PERSISTED;
      var2.argumentStatus = NPCDiplomacyEntity.DiplStatusType.NON_AGGRESSION;
      var2.argumentValue = 7200000.0D;
      var1.condition.conditions.add(var2);
      this.reactions.add(var1);
      (var1 = new DiplomacyReaction(4)).reaction = DiplomacyReaction.ConditionReaction.REMOVE_ALLIANCE_OFFER;
      var1.name = "Remove Alliance Offer";
      var1.condition = new DiplomacyConditionGroup();
      var1.condition.mod = DiplomacyConditionGroup.ConditionMod.AND;
      (var2 = new DiplomacyCondition()).type = DiplomacyCondition.ConditionType.ACTION_COUNTER;
      var2.argumentAction = DiplomacyAction.DiplActionType.ATTACK;
      var2.argumentValue = 1.0D;
      var1.condition.conditions.add(var2);
      this.reactions.add(var1);
      (var1 = new DiplomacyReaction(5)).name = "Remove Peace Deal Offer";
      var1.reaction = DiplomacyReaction.ConditionReaction.REMOVE_PEACE_DEAL_OFFER;
      var1.condition = new DiplomacyConditionGroup();
      var1.condition.mod = DiplomacyConditionGroup.ConditionMod.AND;
      (var2 = new DiplomacyCondition()).type = DiplomacyCondition.ConditionType.ACTION_COUNTER;
      var2.argumentAction = DiplomacyAction.DiplActionType.ATTACK;
      var2.argumentValue = 1.0D;
      var1.condition.conditions.add(var2);
      this.reactions.add(var1);
      (var1 = new DiplomacyReaction(6)).reaction = DiplomacyReaction.ConditionReaction.REJECT_ALLIANCE_OFFER;
      var1.name = "Reject Alliance Offer";
      var1.condition = new DiplomacyConditionGroup();
      var1.condition.mod = DiplomacyConditionGroup.ConditionMod.NOT;
      (var2 = new DiplomacyCondition()).type = DiplomacyCondition.ConditionType.TOTAL_POINTS;
      var2.argumentValue = 990.0D;
      var1.condition.conditions.add(var2);
      this.reactions.add(var1);
      (var1 = new DiplomacyReaction(7)).reaction = DiplomacyReaction.ConditionReaction.REJECT_PEACE_OFFER;
      var1.name = "Reject Peace Deal Offer";
      var1.condition = new DiplomacyConditionGroup();
      var1.condition.mod = DiplomacyConditionGroup.ConditionMod.NOT;
      (var2 = new DiplomacyCondition()).type = DiplomacyCondition.ConditionType.TOTAL_POINTS;
      var2.argumentValue = -500.0D;
      var1.condition.conditions.add(var2);
      this.reactions.add(var1);
      (var1 = new DiplomacyReaction(8)).reaction = DiplomacyReaction.ConditionReaction.SEND_POPUP_MESSAGE;
      var1.name = "Send message on one Attack";
      var1.condition = new DiplomacyConditionGroup();
      var1.condition.mod = DiplomacyConditionGroup.ConditionMod.AND;
      var1.message = "Attacks will not be tolerated!";
      (var2 = new DiplomacyCondition()).type = DiplomacyCondition.ConditionType.ACTION_COUNTER;
      var2.argumentAction = DiplomacyAction.DiplActionType.ATTACK;
      var2.argumentValue = 1.0D;
      var1.condition.conditions.add(var2);
      this.reactions.add(var1);
      (var1 = new DiplomacyReaction(9)).reaction = DiplomacyReaction.ConditionReaction.DECLARE_WAR;
      var1.name = "Declare War on three hostile actions";
      var1.condition = new DiplomacyConditionGroup();
      var1.condition.mod = DiplomacyConditionGroup.ConditionMod.AND;
      (var2 = new DiplomacyCondition()).type = DiplomacyCondition.ConditionType.ACTION_COUNTER;
      var2.argumentAction = DiplomacyAction.DiplActionType.ATTACK;
      var2.argumentValue = 3.0D;
      var1.condition.conditions.add(var2);
      this.reactions.add(var1);
   }

   public int getIndex(DiplomacyAction.DiplActionType var1) {
      return 1000 * (var1.ordinal() + 1);
   }

   public int getIndex(NPCDiplomacyEntity.DiplStatusType var1) {
      return 1000000 * (var1.ordinal() + 1);
   }

   public DiplomacyConfig.DiplomacyConfigElement get(DiplomacyAction.DiplActionType var1) {
      return (DiplomacyConfig.DiplomacyConfigElement)this.map.get(this.getIndex(var1));
   }

   public DiplomacyConfig.DiplomacyConfigElement get(NPCDiplomacyEntity.DiplStatusType var1) {
      return (DiplomacyConfig.DiplomacyConfigElement)this.map.get(this.getIndex(var1));
   }

   public DiplomacyConfig.DiplomacyConfigElement put(DiplomacyAction.DiplActionType var1, DiplomacyConfig.DiplomacyConfigElement var2) {
      return (DiplomacyConfig.DiplomacyConfigElement)this.map.put(this.getIndex(var1), var2);
   }

   public DiplomacyConfig.DiplomacyConfigElement put(NPCDiplomacyEntity.DiplStatusType var1, DiplomacyConfig.DiplomacyConfigElement var2) {
      return (DiplomacyConfig.DiplomacyConfigElement)this.map.put(this.getIndex(var1), var2);
   }

   public class DiplomacyConfigElement {
      public NPCDiplomacyEntity.DiplStatusType statusType;
      public DiplomacyAction.DiplActionType actionType;
      public int upperLimit = 30;
      public int lowerLimit = 0;
      public int value = 0;
      public int existingModifier = 1;
      public int nonExistingModifier = 1;
      public float turnsActionDuration = 1.0F;
      public float staticTimeoutTurns = 1.0F;
      public DiplomacyReaction reaction;

      public void parse(Node var1) throws ConfigParserException {
         if (var1.getNodeType() == 1) {
            NodeList var2;
            Integer var3;
            Float var4;
            if (var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("diplaction")) {
               var2 = var1.getChildNodes();
               var3 = null;
               var4 = null;
               Float var21 = null;
               Float var23 = null;
               Float var24 = null;
               Float var8 = null;
               String var9 = null;

               for(int var10 = 0; var10 < var2.getLength(); ++var10) {
                  Node var11;
                  if ((var11 = var2.item(var10)).getNodeType() == 1) {
                     if (var11.getNodeName().toLowerCase(Locale.ENGLISH).equals("name")) {
                        var9 = var11.getTextContent();
                     }

                     if (var11.getNodeName().toLowerCase(Locale.ENGLISH).equals("upperlimit")) {
                        try {
                           var4 = Float.parseFloat(var11.getTextContent());
                        } catch (NumberFormatException var12) {
                           var12.printStackTrace();
                           throw new ConfigParserException("Invalid Number Format: " + var11.getTextContent() + "; " + var11.getNodeName() + "; " + var11.getParentNode().getNodeName() + "; " + var11.getParentNode().getParentNode().getNodeName());
                        }
                     }

                     try {
                        if (var11.getNodeName().toLowerCase(Locale.ENGLISH).equals("reaction")) {
                           this.reaction = DiplomacyReaction.parse(var11, -1);
                        }
                     } catch (ConfigParserException var18) {
                        var18.printStackTrace();
                        System.err.println("NOT USING REACTION BECAUSE OF EXCEPTION");
                     }

                     if (var11.getNodeName().toLowerCase(Locale.ENGLISH).equals("lowerlimit")) {
                        try {
                           var21 = Float.parseFloat(var11.getTextContent());
                        } catch (NumberFormatException var13) {
                           var13.printStackTrace();
                           throw new ConfigParserException("Invalid Number Format: " + var11.getTextContent() + "; " + var11.getNodeName() + "; " + var11.getParentNode().getNodeName() + "; " + var11.getParentNode().getParentNode().getNodeName());
                        }
                     }

                     if (var11.getNodeName().toLowerCase(Locale.ENGLISH).equals("nonexistingmodifier")) {
                        try {
                           var23 = Float.parseFloat(var11.getTextContent());
                        } catch (NumberFormatException var14) {
                           var14.printStackTrace();
                           throw new ConfigParserException("Invalid Number Format: " + var11.getTextContent() + "; " + var11.getNodeName() + "; " + var11.getParentNode().getNodeName() + "; " + var11.getParentNode().getParentNode().getNodeName());
                        }
                     }

                     if (var11.getNodeName().toLowerCase(Locale.ENGLISH).equals("existingmodifier")) {
                        try {
                           var24 = Float.parseFloat(var11.getTextContent());
                        } catch (NumberFormatException var15) {
                           var15.printStackTrace();
                           throw new ConfigParserException("Invalid Number Format: " + var11.getTextContent() + "; " + var11.getNodeName() + "; " + var11.getParentNode().getNodeName() + "; " + var11.getParentNode().getParentNode().getNodeName());
                        }
                     }

                     if (var11.getNodeName().toLowerCase(Locale.ENGLISH).equals("actiontimeoutduration")) {
                        try {
                           var8 = Float.parseFloat(var11.getTextContent());
                        } catch (NumberFormatException var16) {
                           var16.printStackTrace();
                           throw new ConfigParserException("Invalid Number Format: " + var11.getTextContent() + "; " + var11.getNodeName() + "; " + var11.getParentNode().getNodeName() + "; " + var11.getParentNode().getParentNode().getNodeName());
                        }
                     }

                     if (var11.getNodeName().toLowerCase(Locale.ENGLISH).equals("value")) {
                        try {
                           var3 = (int)Float.parseFloat(var11.getTextContent());
                        } catch (NumberFormatException var17) {
                           var17.printStackTrace();
                           throw new ConfigParserException("Invalid Number Format: " + var11.getTextContent() + "; " + var11.getNodeName() + "; " + var11.getParentNode().getNodeName() + "; " + var11.getParentNode().getParentNode().getNodeName());
                        }
                     }
                  }
               }

               if (var4 == null) {
                  throw new ConfigParserException("'UpperLimit' Tag needed. " + var1.getNodeName() + "; " + var1.getParentNode().getNodeName());
               } else if (var21 == null) {
                  throw new ConfigParserException("'LowerLimit' Tag needed. " + var1.getNodeName() + "; " + var1.getParentNode().getNodeName());
               } else if (var23 == null) {
                  throw new ConfigParserException("'NonExistingModifier' Tag needed. " + var1.getNodeName() + "; " + var1.getParentNode().getNodeName());
               } else if (var24 == null) {
                  throw new ConfigParserException("'ExistingModifier' Tag needed. " + var1.getNodeName() + "; " + var1.getParentNode().getNodeName());
               } else if (var9 == null) {
                  throw new ConfigParserException("'Name' Tag needed. " + var1.getNodeName() + "; " + var1.getParentNode().getNodeName());
               } else if (var8 == null) {
                  throw new ConfigParserException("'ActionTimeoutDuration' Tag needed. " + var1.getNodeName() + "; " + var1.getParentNode().getNodeName());
               } else if (var3 == null) {
                  throw new ConfigParserException("'Value' Tag needed. " + var1.getNodeName() + "; " + var1.getParentNode().getNodeName());
               } else {
                  DiplomacyAction.DiplActionType var25;
                  if ((var25 = DiplomacyAction.DiplActionType.valueOf(var9.toUpperCase(Locale.ENGLISH))) == null) {
                     throw new ConfigParserException("Unknown DiplActionType: " + var9 + "; " + var1.getNodeName() + "; " + var1.getParentNode().getNodeName());
                  } else {
                     this.actionType = var25;
                     this.upperLimit = var4.intValue();
                     this.lowerLimit = var21.intValue();
                     this.nonExistingModifier = var23.intValue();
                     this.existingModifier = var24.intValue();
                     this.turnsActionDuration = var8;
                     this.value = var3;
                     DiplomacyConfig.this.put(this.actionType, this);
                  }
               }
            } else if (var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("diplstatus")) {
               var2 = var1.getChildNodes();
               var3 = null;
               var4 = null;
               String var5 = null;

               for(int var6 = 0; var6 < var2.getLength(); ++var6) {
                  Node var7;
                  if ((var7 = var2.item(var6)).getNodeType() == 1) {
                     if (var7.getNodeName().toLowerCase(Locale.ENGLISH).equals("name")) {
                        var5 = var7.getTextContent();
                     }

                     if (var7.getNodeName().toLowerCase(Locale.ENGLISH).equals("timeout")) {
                        try {
                           var4 = Float.parseFloat(var7.getTextContent());
                        } catch (NumberFormatException var19) {
                           var19.printStackTrace();
                           throw new ConfigParserException("Invalid Number Format: " + var7.getTextContent() + "; " + var7.getNodeName() + "; " + var7.getParentNode().getNodeName() + "; " + var7.getParentNode().getParentNode().getNodeName());
                        }
                     }

                     if (var7.getNodeName().toLowerCase(Locale.ENGLISH).equals("value")) {
                        try {
                           var3 = (int)Float.parseFloat(var7.getTextContent());
                        } catch (NumberFormatException var20) {
                           var20.printStackTrace();
                           throw new ConfigParserException("Invalid Number Format: " + var7.getTextContent() + "; " + var7.getNodeName() + "; " + var7.getParentNode().getNodeName() + "; " + var7.getParentNode().getParentNode().getNodeName());
                        }
                     }
                  }
               }

               if (var5 == null) {
                  throw new ConfigParserException("'Name' Tag needed. " + var1.getNodeName() + "; " + var1.getParentNode().getNodeName());
               } else if (var4 == null) {
                  throw new ConfigParserException("'Timeout' Tag needed. " + var1.getNodeName() + "; " + var1.getParentNode().getNodeName());
               } else if (var3 == null) {
                  throw new ConfigParserException("'Value' Tag needed. " + var1.getNodeName() + "; " + var1.getParentNode().getNodeName());
               } else {
                  NPCDiplomacyEntity.DiplStatusType var22;
                  if ((var22 = NPCDiplomacyEntity.DiplStatusType.valueOf(var5.toUpperCase(Locale.ENGLISH))) == null) {
                     throw new ConfigParserException("Unknown DiplStatusType: " + var5 + "; " + var1.getNodeName() + "; " + var1.getParentNode().getNodeName());
                  } else {
                     this.statusType = var22;
                     this.value = var3;
                     this.staticTimeoutTurns = var4;
                     DiplomacyConfig.this.put(this.statusType, this);
                  }
               }
            } else {
               throw new ConfigParserException("Diplomacy Config Error " + var1.getNodeName() + "; " + var1.getParentNode().getNodeName());
            }
         }
      }

      public void appendXML(Document var1, Element var2, NPCDiplomacyEntity.DiplStatusType var3) {
         Element var4 = var1.createElement("DiplStatus");
         Element var5 = var1.createElement("Name");
         Element var6 = var1.createElement("Timeout");
         Element var7 = var1.createElement("Value");
         var5.setTextContent(var3.name());
         var6.setTextContent(String.valueOf(this.staticTimeoutTurns));
         var6.appendChild(var1.createComment("In Faction Turns"));
         var7.setTextContent(String.valueOf(this.value));
         var7.appendChild(var1.createComment("In diplomacy points (no float)"));
         var4.appendChild(var5);
         var4.appendChild(var6);
         var4.appendChild(var7);
         var2.appendChild(var4);
      }

      public void appendXML(Document var1, Element var2, DiplomacyAction.DiplActionType var3) {
         Element var4 = var1.createElement("DiplAction");
         Element var5 = var1.createElement("Name");
         Element var6 = var1.createElement("UpperLimit");
         Element var7 = var1.createElement("LowerLimit");
         Element var8 = var1.createElement("NonExistingModifier");
         Element var9 = var1.createElement("ExistingModifier");
         Element var10 = var1.createElement("ActionTimeoutDuration");
         Element var11 = var1.createElement("Value");
         var5.setTextContent(var3.name());
         var6.setTextContent(String.valueOf(this.upperLimit));
         var6.appendChild(var1.createComment("If lower than lower limit, it's a minus effect on diplomacy"));
         var7.setTextContent(String.valueOf(this.lowerLimit));
         var9.setTextContent(String.valueOf(this.existingModifier));
         var9.appendChild(var1.createComment("How much the modifier changes towards upper limit each diplomacy round"));
         var8.setTextContent(String.valueOf(this.nonExistingModifier));
         var8.appendChild(var1.createComment("How much the modifier changes towards lower limit each diplomacy round"));
         var11.setTextContent(String.valueOf(this.value));
         var11.appendChild(var1.createComment("In diplomacy points (no float)"));
         var10.setTextContent(String.valueOf(this.turnsActionDuration));
         var10.appendChild(var1.createComment("Time for Action to timeout (in faction turns)"));
         var4.appendChild(var5);
         var4.appendChild(var6);
         var4.appendChild(var7);
         var4.appendChild(var9);
         var4.appendChild(var8);
         var4.appendChild(var10);
         var4.appendChild(var11);
         if (this.reaction != null) {
            this.reaction.appendXML(var1, var4);
         }

         var2.appendChild(var4);
      }
   }
}
