package org.schema.game.server.data.simulation.npc.diplomacy;

import org.schema.common.util.StringTools;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class DiplomacyAction {
   public DiplomacyAction.DiplActionType type;
   public int counter;
   public long timeDuration;

   public Tag toTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.INT, (String)null, this.counter), new Tag(Tag.Type.LONG, (String)null, this.timeDuration), new Tag(Tag.Type.BYTE, (String)null, (byte)this.type.ordinal()), FinishTag.INST});
   }

   public void fromTag(Tag var1) {
      Tag[] var2;
      (var2 = var1.getStruct())[0].getByte();
      this.counter = var2[1].getInt();
      this.timeDuration = var2[2].getLong();
      this.type = DiplomacyAction.DiplActionType.values()[var2[3].getByte()];
   }

   public static enum DiplActionType {
      ATTACK(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_DIPLOMACYACTION_0;
         }
      }),
      ATTACK_ENEMY(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_DIPLOMACYACTION_1;
         }
      }),
      MINING(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_DIPLOMACYACTION_2;
         }
      }),
      TERRITORY(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_DIPLOMACYACTION_3;
         }
      }),
      PEACE_OFFER(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_DIPLOMACYACTION_4;
         }
      }),
      DECLARATION_OF_WAR(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_DIPLOMACYACTION_5;
         }
      }),
      ALLIANCE_REQUEST(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_DIPLOMACYACTION_6;
         }
      }),
      ALLIANCE_CANCEL(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_DIPLOMACYACTION_7;
         }
      }),
      TRADING_WITH_US(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_DIPLOMACYACTION_8;
         }
      }),
      TRADING_WITH_ENEMY(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_DIPLOMACYACTION_9;
         }
      }),
      ALLIANCE_WITH_ENEMY(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_DIPLOMACY_DIPLOMACYACTION_10;
         }
      });

      private Translatable description;

      private DiplActionType(Translatable var3) {
         this.description = var3;
      }

      public final String getDescription() {
         return this.description.getName(this);
      }

      public static String list() {
         return StringTools.listEnum(values());
      }
   }
}
