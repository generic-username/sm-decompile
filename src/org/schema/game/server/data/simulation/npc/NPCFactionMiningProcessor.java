package org.schema.game.server.data.simulation.npc;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.server.data.GameServerState;

public class NPCFactionMiningProcessor {
   public final NPCFaction faction;
   public final GameServerState state;

   public NPCFactionMiningProcessor(NPCFaction var1, GameServerState var2) {
      this.faction = var1;
      this.state = var2;
   }

   public void processMining(double var1) {
      IntOpenHashSet var3 = new IntOpenHashSet();
      Inventory var4 = this.faction.getInventory();

      for(int var5 = 0; var5 < ElementKeyMap.resources.length; ++var5) {
         short var6 = ElementKeyMap.resources[var5];
         int var7 = (int)Math.max(0.0D, (double)this.faction.structure.totalResources[var5] * var1);
         int var8 = var4.incExistingOrNextFreeSlot(var6, var7);
         var3.add(var8);
      }

      var4.sendInventoryModification(var3);
   }

   public void processProduction(double var1) {
   }
}
