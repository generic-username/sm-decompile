package org.schema.game.server.data.simulation.npc;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.common.config.ConfigParserException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.database.SystemInDatabase;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.game.server.data.simulation.npc.geo.NPCFactionPreset;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.resource.DiskWritable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class NPCFactionManager implements DiskWritable {
   private static final byte TAG_VERSION = 0;
   private static final String TAG_FILE_NAME = "NPCFACTIONS";
   private final GameServerState state;
   private List factions = new ObjectArrayList();
   private final Galaxy galaxy;
   private long lastUpdate;

   public NPCFactionManager(GameServerState var1, Galaxy var2) {
      this.state = var1;
      this.galaxy = var2;
      this.lastUpdate = System.currentTimeMillis();
      if (var2.galaxyPos.equals(0, 0, 0) && !EngineSettings.SECRET.getCurrentState().toString().toLowerCase(Locale.ENGLISH).contains("nonpc")) {
         Iterator var15 = var1.getFactionManager().getFactionCollection().iterator();

         while(var15.hasNext()) {
            Faction var3;
            if ((var3 = (Faction)var15.next()) instanceof NPCFaction && ((NPCFaction)var3).serverGalaxyPos.equals(var2.galaxyPos)) {
               this.factions.add((NPCFaction)var3);
            }
         }

         try {
            Tag var18 = Tag.readFrom(new BufferedInputStream(new FileInputStream(this.getFileNameTag())), true, false);
            this.fromTagStructure(var18);
         } catch (FileNotFoundException var13) {
            System.err.println("[SERVER][NPCFACTIONS] no previous file existed. Generating NPC factions freshly");

            try {
               if (this.factions.isEmpty()) {
                  NPCStartupConfig var19 = new NPCStartupConfig();
                  DocumentBuilder var16 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                  File var4;
                  if (!(var4 = new File("./customNPCConfig/npcSpawnConfig.xml")).getParentFile().exists()) {
                     var4.getParentFile().mkdirs();
                     File var5 = new File(var4.getParentFile(), "HOWTO.txt");
                     BufferedWriter var20;
                     (var20 = new BufferedWriter(new FileWriter(var5))).write("Copy the startupConfig and folders over from ./data/npcConfigs to use them as your default npc configurations");
                     var20.close();
                  }

                  if (!var4.exists()) {
                     var4 = new File("./data/npcFactions/npcSpawnConfig.xml");
                     System.err.println("[SERVER] USING DEFAULT NPC FACTION STARTUP CONFIG: " + var4.getAbsolutePath());
                  } else {
                     System.err.println("[SERVER] USING CUSTOM NPC FACTION STARTUP CONFIG: " + var4.getAbsolutePath());
                  }

                  Document var21 = var16.parse(var4);
                  var19.parse(var21);

                  assert var19.spawns.size() > 0 : "No Spawns";

                  Iterator var22 = var19.spawns.iterator();

                  while(var22.hasNext()) {
                     NPCFactionSpawn var17 = (NPCFactionSpawn)var22.next();

                     try {
                        this.add(var2, var17);
                     } catch (NPCSpawnException var6) {
                        var6.printStackTrace();
                     }
                  }
               }

            } catch (IllegalArgumentException var7) {
               var7.printStackTrace();
            } catch (IllegalAccessException var8) {
               var8.printStackTrace();
            } catch (ConfigParserException var9) {
               var9.printStackTrace();
            } catch (IOException var10) {
               var10.printStackTrace();
            } catch (SAXException var11) {
               var11.printStackTrace();
            } catch (ParserConfigurationException var12) {
               var12.printStackTrace();
            }
         } catch (IOException var14) {
            var14.printStackTrace();
         }
      } else {
         System.err.println("[SERVER][GALAXY] not creating factions for " + var2.galaxyPos);
      }
   }

   public void write() {
   }

   public static String getFileNameTag(Vector3i var0) {
      return "NPCFACTIONS_" + var0.x + "_" + var0.y + "_" + var0.z + ".tag";
   }

   public void removeFaction(Faction var1) {
      this.factions.remove(var1);
   }

   public void updateLocal(long var1) {
      if (var1 - this.lastUpdate > 3000L) {
         for(int var3 = 0; var3 < this.factions.size(); ++var3) {
            NPCFaction var4;
            (var4 = (NPCFaction)this.factions.get(var3)).getFleetManager().update(var1);
            var4.accumulatedTimeNPCFactionTurn += var1 - this.lastUpdate;
            if (var4.accumulatedTimeNPCFactionTurn > var4.getTimeBetweenFactionTurns()) {
               System.err.println("[SERVER][NPC] scheduling faction turn for " + var4);
               this.state.getFactionManager().scheduleTurn(var4);
               var4.accumulatedTimeNPCFactionTurn = 0L;
            }

            var4.getDiplomacy().update(var1);
         }

         this.lastUpdate = var1;
      }

   }

   public boolean checkTradeNodes() {
      Iterator var1 = this.factions.iterator();

      NPCFaction var2;
      do {
         if (!var1.hasNext()) {
            return true;
         }

         var2 = (NPCFaction)var1.next();

         assert var2.getTradeNode() != null : var2;
      } while(var2.getTradeNode() != null);

      return false;
   }

   private boolean isLimited() {
      return (Integer)ServerConfig.NPC_FACTION_SPAWN_LIMIT.getCurrentState() >= 0 && this.factions.size() >= (Integer)ServerConfig.NPC_FACTION_SPAWN_LIMIT.getCurrentState();
   }

   public void add(Galaxy var1, NPCFactionSpawn var2) throws NPCSpawnException {
      if (this.isLimited()) {
         throw new NPCSpawnException("Cannot spawn more faction. limited by server.cfg");
      } else {
         Random var3 = new Random(var1.getSeed() * (long)this.getFactionManager().currentFactionIdCreator);
         Vector3i var4 = null;
         Vector3i var5 = new Vector3i();
         SystemInDatabase var6;
         if (!var2.randomSpawn && ((var6 = this.state.getDatabaseIndex().getTableManager().getSystemTable().getSystem(var2.fixedSpawnSystem)) == null || var6.factionUID == 0 || var6.factionUID == this.getFactionManager().currentFactionIdCreator)) {
            var4 = new Vector3i(var2.fixedSpawnSystem);
         }

         while(true) {
            SystemInDatabase var20;
            do {
               if (var4 != null) {
                  try {
                     NPCFactionPreset var18;
                     if (var2.possiblePresets != null && var2.possiblePresets.trim().length() != 0) {
                        List var21 = this.state.getFactionManager().npcFactionPresetManager.getNpcPresets();
                        ObjectArrayList var19 = new ObjectArrayList();
                        String[] var16;
                        if (var2.possiblePresets.contains(",")) {
                           var16 = var2.possiblePresets.split(",");
                        } else {
                           var16 = new String[]{var2.possiblePresets};
                        }

                        int var8 = 0;

                        while(true) {
                           if (var8 >= var21.size()) {
                              if (var19.isEmpty()) {
                                 try {
                                    throw new Exception("[ERROR] Exception: No NPC Faction Preset found for " + var2.name + "; But should be: " + var2.possiblePresets + "; Picking random one from existing");
                                 } catch (Exception var14) {
                                    var14.printStackTrace();
                                    var18 = this.state.getFactionManager().npcFactionPresetManager.getRandomLeastUsedPreset(var1.getSeed(), this.getFactionManager().currentFactionIdCreator);
                                    break;
                                 }
                              } else {
                                 var18 = (NPCFactionPreset)var19.get(var3.nextInt(var19.size()));
                                 System.err.println("[SERVER][NPCFACTION] picked preset " + var18.factionPresetName + " for Faction " + var2.name);
                                 break;
                              }
                           }

                           NPCFactionPreset var9 = (NPCFactionPreset)var21.get(var8);
                           String[] var10 = var16;
                           int var11 = var16.length;

                           for(int var12 = 0; var12 < var11; ++var12) {
                              String var13 = var10[var12];
                              if (var9.factionPresetName.trim().toLowerCase(Locale.ENGLISH).equals(var13.trim().toLowerCase(Locale.ENGLISH))) {
                                 var19.add(var9);
                              }
                           }

                           ++var8;
                        }
                     } else {
                        var18 = this.state.getFactionManager().npcFactionPresetManager.getRandomLeastUsedPreset(var1.getSeed(), this.getFactionManager().currentFactionIdCreator);
                     }

                     System.err.println("[SERVER][NPCFACTIONS] creating NPC faction. Created: " + this.factions.size() + "; position: " + var4);
                     NPCFaction var22;
                     (var22 = new NPCFaction(this.state, this.getFactionManager().currentFactionIdCreator, var2.name, var2.description, var4)).serverGalaxyPos = new Vector3i(var1.galaxyPos);
                     var22.initializeWithState(this.state);
                     var22.setConfig(new NPCFactionConfig());
                     var22.getConfig().setPreset(var18);
                     var22.initialize();
                     var22.initialSpawn = var2.initialGrowth;

                     assert var18.blueprintController != null;

                     var22.onCreated();
                     this.getFactionManager().addFaction(var22);
                     this.factions.add(var22);
                  } catch (Exception var15) {
                     throw new NPCSpawnException(var15);
                  }

                  ++this.getFactionManager().currentFactionIdCreator;
                  return;
               }

               int var17 = var1.getStarIndices().get(var3.nextInt(var1.getStarIndices().size()));
               var1.getNormalizedPosFromIndex(var17, var5);
               Iterator var7 = this.factions.iterator();

               while(var7.hasNext()) {
                  if (!((NPCFaction)((Faction)var7.next())).npcFactionHomeSystem.equals(var5)) {
                  }
               }
            } while((var20 = this.state.getDatabaseIndex().getTableManager().getSystemTable().getSystem(var5)) != null && var20.factionUID != 0 && var20.factionUID != this.getFactionManager().currentFactionIdCreator);

            var4 = new Vector3i(var5);
         }
      }
   }

   public String getUniqueIdentifier() {
      return null;
   }

   public FactionManager getFactionManager() {
      return this.state.getFactionManager();
   }

   public boolean isVolatile() {
      return false;
   }

   public void fromTagStructure(Tag var1) {
      (Byte)((Tag[])var1.getValue())[0].getValue();
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), FinishTag.INST});
   }

   public String getFileNameTag() {
      return getFileNameTag(this.galaxy.galaxyPos);
   }

   public void onSystemOwnershipChanged(int var1, int var2, Vector3i var3) {
   }
}
