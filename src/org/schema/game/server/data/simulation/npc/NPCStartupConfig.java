package org.schema.game.server.data.simulation.npc;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.File;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.Locale;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.schema.common.config.ConfigParserException;
import org.schema.common.util.linAlg.Vector3i;
import org.w3c.dom.Comment;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class NPCStartupConfig {
   public static final byte XML_VERSION = 0;
   public static final String dataPath = "./data/npcFactions/npcSpawnConfig.xml";
   public static final String customPath = "./customNPCConfig/npcSpawnConfig.xml";
   public final ObjectArrayList spawns = new ObjectArrayList();

   private String getTag() {
      return "Config";
   }

   public static void main(String[] var0) {
      NPCStartupConfig var1;
      (var1 = new NPCStartupConfig()).add("Trading Guild", "A Faction", false, 9, new Vector3i(0, 0, 0), "Trading Guild");
      var1.add("Outcasts Alpha", "A Faction", true, 33, (Vector3i)null, "Outcasts");
      var1.add("Outcasts Beta", "A Faction", true, 6, (Vector3i)null, "Outcasts");
      var1.add("Outcasts Gamma", "A Faction", true, 8, (Vector3i)null, "Outcasts");
      var1.add("Scavengers Alpha", "A Faction", true, 44, (Vector3i)null, "Scavengers");
      var1.add("Scavengers Beta", "A Faction", true, 22, (Vector3i)null, "Scavengers");
      var1.add("Scavengers Gamma", "A Faction", true, 12, (Vector3i)null, "Scavengers");
      writeDocument(new File("./data/npcFactions/npcSpawnConfig.xml"), var1);
   }

   public static NPCFactionSpawn createSpawn(String var0, String var1, boolean var2, int var3, Vector3i var4, String... var5) {
      NPCFactionSpawn var6;
      (var6 = new NPCFactionSpawn()).randomSpawn = var2;
      var6.initialGrowth = var3;
      var6.fixedSpawnSystem = var4;
      var6.description = var1;
      StringBuffer var7 = new StringBuffer();

      for(int var8 = 0; var8 < var5.length; ++var8) {
         var7.append(var5[var8].trim());
         if (var8 < var5.length - 1) {
            var7.append(", ");
         }
      }

      var6.possiblePresets = var7.toString();
      var6.name = var0;
      return var6;
   }

   public NPCFactionSpawn add(String var1, String var2, boolean var3, int var4, Vector3i var5, String... var6) {
      NPCFactionSpawn var7 = createSpawn(var1, var2, var3, var4, var5, var6);
      this.spawns.add(var7);
      return var7;
   }

   public void create(Document var1) throws DOMException, IllegalArgumentException, IllegalAccessException {
      Element var2 = var1.createElement("NPCConfig");
      var1.appendChild(var2);
      Element var3;
      (var3 = var1.createElement("Version")).setTextContent("0");
      Comment var4 = var1.createComment("autocreated");
      var3.appendChild(var4);
      var2.appendChild(var3);
      var3 = var1.createElement("Config");
      var2.appendChild(var3);
      Iterator var6 = this.spawns.iterator();

      while(var6.hasNext()) {
         NPCFactionSpawn var7 = (NPCFactionSpawn)var6.next();
         Element var5 = var1.createElement("Faction");
         var7.append(var1, var5);
         var3.appendChild(var5);
      }

   }

   public void parse(Document var1) throws IllegalArgumentException, IllegalAccessException, ConfigParserException {
      NodeList var8 = var1.getDocumentElement().getChildNodes();
      this.getClass().getDeclaredFields();
      new ObjectOpenHashSet();

      for(int var2 = 0; var2 < var8.getLength(); ++var2) {
         Node var3;
         if ((var3 = var8.item(var2)).getNodeType() == 1 && var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("version")) {
            try {
               Byte.parseByte(var3.getTextContent());
            } catch (Exception var7) {
               var7.printStackTrace();
               throw new ConfigParserException("malformed version in xml", var7);
            }
         } else if (var3.getNodeType() == 1 && var3.getNodeName().toLowerCase(Locale.ENGLISH).equals(this.getTag().toLowerCase(Locale.ENGLISH))) {
            NodeList var9 = var3.getChildNodes();

            for(int var4 = 0; var4 < var9.getLength(); ++var4) {
               Node var5;
               if ((var5 = var9.item(var4)).getNodeType() == 1 && var5.getNodeName().toLowerCase(Locale.ENGLISH).equals("faction")) {
                  NPCFactionSpawn var6;
                  (var6 = new NPCFactionSpawn()).parse(var5);
                  this.spawns.add(var6);
               }
            }
         }
      }

   }

   public static File writeDocument(File var0, NPCStartupConfig var1) {
      try {
         Document var2 = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
         var1.create(var2);
         var2.setXmlVersion("1.0");
         TransformerFactory var10000 = TransformerFactory.newInstance();
         var1 = null;
         Transformer var5;
         (var5 = var10000.newTransformer()).setOutputProperty("omit-xml-declaration", "yes");
         var5.setOutputProperty("indent", "yes");
         var5.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
         new StringWriter();
         StreamResult var3 = new StreamResult(var0);
         DOMSource var6 = new DOMSource(var2);
         var5.transform(var6, var3);
         return var0;
      } catch (Exception var4) {
         var4.printStackTrace();
         return null;
      }
   }
}
