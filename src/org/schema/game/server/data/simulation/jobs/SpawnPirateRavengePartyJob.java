package org.schema.game.server.data.simulation.jobs;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.data.simulation.SimulationManager;

public class SpawnPirateRavengePartyJob extends SimulationJob {
   private final Vector3i from;
   private int count;

   public SpawnPirateRavengePartyJob(Vector3i var1, int var2) {
      this.from = var1;
      this.count = var2;
   }

   public void execute(SimulationManager var1) {
      var1.createRandomPirateGroup(this.from, this.count);
   }
}
