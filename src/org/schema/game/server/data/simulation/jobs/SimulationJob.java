package org.schema.game.server.data.simulation.jobs;

import org.schema.game.server.data.simulation.SimulationManager;

public abstract class SimulationJob {
   public abstract void execute(SimulationManager var1);
}
