package org.schema.game.server.data.simulation.groups;

import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.server.ServerMessage;

public class RavegingSimulationGroup extends ShipSimulationGroup {
   public RavegingSimulationGroup(GameServerState var1) {
      super(var1);
   }

   public SimulationGroup.GroupType getType() {
      return SimulationGroup.GroupType.RAVEGING;
   }

   public void returnHomeMessage(PlayerState var1) {
      var1.sendServerMessage(new ServerMessage(new Object[]{477}, 2, var1.getId()));
   }

   public void sendInvestigationMessage(PlayerState var1) {
      var1.sendServerMessage(new ServerMessage(new Object[]{478, this.getStartSector(), var1.getCurrentSector()}, 2, var1.getId()));
   }
}
