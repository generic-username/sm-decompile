package org.schema.game.server.data.simulation.groups;

import org.schema.game.server.data.GameServerState;

public interface SimGroupFactory {
   SimulationGroup instantiate(GameServerState var1);
}
