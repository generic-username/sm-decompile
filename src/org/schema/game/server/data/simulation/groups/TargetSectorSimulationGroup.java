package org.schema.game.server.data.simulation.groups;

import java.util.Iterator;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.controller.ShopperInterface;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.inventory.NoSlotFreeException;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.server.ServerMessage;
import org.schema.schine.resource.tag.Tag;

public class TargetSectorSimulationGroup extends ShipSimulationGroup {
   public Vector3i targetSector;
   public boolean hasStockToDeliver;

   public TargetSectorSimulationGroup(GameServerState var1) {
      super(var1);
   }

   public TargetSectorSimulationGroup(GameServerState var1, Vector3i var2) {
      super(var1);
      this.targetSector = var2;
   }

   private void deliverStock() {
      Iterator var1 = this.getMembers().iterator();

      while(var1.hasNext()) {
         String var2 = (String)var1.next();
         SegmentController var4;
         ShopperInterface var5;
         if (this.isLoaded(var2) && (var4 = (SegmentController)this.getState().getSegmentControllersByName().get(var2)) != null && var4 instanceof ShopperInterface && !(var5 = (ShopperInterface)var4).getShopsInDistance().isEmpty()) {
            ShopInterface var6 = (ShopInterface)var5.getShopsInDistance().iterator().next();
            System.err.println("[SIMULATION] " + this + " filling stock of: " + var6);

            try {
               var6.fillInventory(true, false);
            } catch (NoSlotFreeException var3) {
               var3.printStackTrace();
            }

            this.hasStockToDeliver = false;
         }
      }

   }

   protected Tag getMetaData() {
      return new Tag(Tag.Type.VECTOR3i, (String)null, this.targetSector);
   }

   public SimulationGroup.GroupType getType() {
      return SimulationGroup.GroupType.TARGET_SECTOR;
   }

   protected void handleMetaData(Tag var1) {
      this.targetSector = (Vector3i)var1.getValue();
      if (this.getCurrentProgram() != null) {
         ((TargetProgram)this.getCurrentProgram()).setSectorTarget(this.targetSector);
      }

   }

   public void onWait() {
      super.onWait();
      if (this.hasStockToDeliver) {
         this.deliverStock();
      }

   }

   public void returnHomeMessage(PlayerState var1) {
      var1.sendServerMessage(new ServerMessage(new Object[]{479}, 2, var1.getId()));
   }

   public void sendInvestigationMessage(PlayerState var1) {
      var1.sendServerMessage(new ServerMessage(new Object[]{480}, 2, var1.getId()));
   }
}
