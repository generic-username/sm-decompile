package org.schema.game.server.data.simulation;

import org.schema.game.server.ai.program.simpirates.PirateSimulationProgram;
import org.schema.game.server.ai.program.simpirates.SimulationProgramInterface;
import org.schema.game.server.ai.program.simpirates.TradingRouteSimulationProgram;
import org.schema.game.server.data.simulation.groups.SimulationGroup;
import org.schema.schine.ai.MachineProgram;

public enum SimPrograms {
   VISIT_SECTOR(new SimulationProgramFactory() {
      public final TradingRouteSimulationProgram getInstance(SimulationGroup var1, boolean var2) {
         return new TradingRouteSimulationProgram(var1, var2);
      }
   }),
   SCAN_AND_ATTACK(new SimulationProgramFactory() {
      public final PirateSimulationProgram getInstance(SimulationGroup var1, boolean var2) {
         return new PirateSimulationProgram(var1, var2);
      }
   });

   private SimulationProgramFactory factory;

   private SimPrograms(SimulationProgramFactory var3) {
      this.factory = var3;
   }

   public static SimPrograms getFromClass(SimulationGroup var0) throws NoSimstateFountException {
      MachineProgram var1;
      if ((var1 = var0.getCurrentProgram()) instanceof SimulationProgramInterface) {
         return ((SimulationProgramInterface)var1).getProgram();
      } else {
         throw new NoSimstateFountException("Could not find simProgram for " + var1);
      }
   }

   public static MachineProgram getProgram(SimPrograms var0, SimulationGroup var1, boolean var2) {
      return var0.factory.getInstance(var1, var2);
   }
}
