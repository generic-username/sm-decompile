package org.schema.game.server.data.blueprintnw;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.longs.Long2DoubleOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.ClientStatics;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.FloatingRockManaged;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.ShoppingAddOn;
import org.schema.game.common.controller.ai.SegmentControllerAIInterface;
import org.schema.game.common.controller.elements.ActivationManagerInterface;
import org.schema.game.common.controller.elements.EntityIndexScore;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.controller.elements.StationaryManagerContainer;
import org.schema.game.common.controller.elements.activation.ActivationCollectionManager;
import org.schema.game.common.controller.elements.cargo.CargoCollectionManager;
import org.schema.game.common.controller.elements.railbeam.RailBeamElementManager;
import org.schema.game.common.controller.io.SegmentDataFileUtils;
import org.schema.game.common.controller.rails.RailRelation;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.VoidUniqueSegmentPiece;
import org.schema.game.common.data.element.ControlElementMap;
import org.schema.game.common.data.element.ControlElementMapper;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementDocking;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Chunk16SegmentData;
import org.schema.game.common.data.world.migration.Chunk32Migration;
import org.schema.game.common.updater.FileUtil;
import org.schema.game.common.util.FolderZipper;
import org.schema.game.common.version.Version;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.controller.EntityAlreadyExistsException;
import org.schema.game.server.controller.gameConfig.GameConfig;
import org.schema.game.server.data.BlueprintInterface;
import org.schema.game.server.data.EntityRequest;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.SegmentControllerBluePrintEntryOld;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.tag.Tag;

public class BlueprintEntry implements BlueprintInterface {
   public static final int dataVersion = 5;
   public static final int structureVersion = 0;
   public static final int metaVersion = 5;
   private static final byte FINISH_BYTE = 1;
   private static final byte SEG_MANAGER_BYTE = 2;
   private static final byte DOCKING_BYTE = 3;
   private static final byte RAIL_BYTE = 4;
   private static final byte AI_CONFIG_BYTE = 5;
   private static final byte RAIL_DOCKER_BYTE = 6;
   private static final byte CARGO_BYTE = 7;
   public final List wirelessToOwnRail;
   private final BluePrintController bbController;
   private final Vector3f railRootMinTotal;
   private final Vector3f railRootMaxTotal;
   public boolean railDock;
   public Tag railTag;
   public boolean rootRead;
   public int metaVersionRead;
   private BoundingBox bb;
   private String name;
   private BlueprintType entityType;
   private long price;
   private ElementCountMap elementMap;
   private File header;
   private File structure;
   private File meta;
   private Tag managerTag;
   private File rawBlockDir;
   private BlueprintClassification classification;
   private ControlElementMapper controlElementMap;
   private List childsToWrite;
   private List childs;
   private BlueprintEntry parent;
   private SegmentController delayedWriteSegCon;
   private Vector3i dockingPos;
   private Vector3f dockingSize;
   private short dockingStyle;
   private Tag aiTag;
   private byte dockingOrientation;
   private double mass;
   private String railUID;
   private EntityIndexScore score;
   private boolean loadedChunk16;
   private boolean needsChunkMigration;
   private Long2ObjectOpenHashMap dockerPoints;
   private Long2DoubleOpenHashMap cargoPoints;
   private ElementCountMap countWithChilds;
   private double totalCargo;
   private int headerVersion;
   private boolean hadCargoByte;
   private double singleCargo;
   private boolean oldPowerFlag;
   private long tookHeaderRead;
   private long tookMetaRead;
   private long tookStructureRead;
   private long headerModified;
   private long metaModified;
   private long structModified;
   private String blueprintSavedInGameVersion;

   public BlueprintEntry(String var1) {
      this(var1, BluePrintController.active);
   }

   public BlueprintEntry(String var1, BluePrintController var2) {
      this.wirelessToOwnRail = new ObjectArrayList();
      this.railRootMinTotal = new Vector3f();
      this.railRootMaxTotal = new Vector3f();
      this.railDock = false;
      this.classification = null;
      this.totalCargo = -1.0D;
      this.singleCargo = -1.0D;
      this.headerModified = -1L;
      this.metaModified = -1L;
      this.structModified = -1L;
      this.blueprintSavedInGameVersion = "unknown";
      this.bbController = var2;
      this.init(var1, var2);
   }

   public double getTotalCapacity() {
      if (this.totalCargo < 0.0D) {
         this.totalCargo = 0.0D;
         double var2;
         if (this.cargoPoints != null) {
            for(Iterator var1 = this.cargoPoints.values().iterator(); var1.hasNext(); this.totalCargo += var2) {
               var2 = (Double)var1.next();
            }
         }

         if (this.childs != null) {
            for(int var4 = 0; var4 < this.childs.size(); ++var4) {
               BlueprintEntry var5 = (BlueprintEntry)this.childs.get(var4);
               this.totalCargo += var5.getTotalCapacity();
            }
         }
      }

      return this.totalCargo;
   }

   public double getCapacitySingle() {
      if (this.singleCargo < 0.0D) {
         this.singleCargo = 0.0D;
         double var2;
         if (this.cargoPoints != null) {
            for(Iterator var1 = this.cargoPoints.values().iterator(); var1.hasNext(); this.singleCargo += var2) {
               var2 = (Double)var1.next();
            }
         }
      }

      return this.singleCargo;
   }

   public boolean hadCargoByteRead() {
      return this.hadCargoByte;
   }

   public static String importFile(File var0, BluePrintController var1) throws IOException {
      FileExt var2;
      if ((var2 = new FileExt("./bbtmp/")).exists()) {
         FileUtil.deleteRecursive(var2);
      }

      if (!var2.exists()) {
         var2.mkdir();
      }

      long var3 = FileUtil.getExtractedFilesSize(var0);
      System.err.println("[IMPORT][ZIP] TOTAL FILE SIZE EXTRACTED: " + var0 + ": " + var3 / 1024L / 1024L + " MB");
      long var5 = 1048576L * ((Integer)ServerConfig.ALLOWED_UNPACKED_FILE_UPLOAD_IN_MB.getCurrentState()).longValue();
      if (var3 > var5) {
         throw new IOException("Extracted files too big (possible zip bomb through sparse files) for " + var0.getAbsolutePath() + ": " + var3 / 1024L / 1024L + " MB");
      } else {
         FileUtil.extract(var0, "./bbtmp/");
         boolean var7 = false;
         File[] var11;
         if ((var11 = var2.listFiles()).length != 1) {
            throw new IOException("wrong file format to import. Must be exctly one dir, but found " + Arrays.toString(var2.list()));
         } else {
            File[] var4;
            File var12;
            if ((var4 = (var12 = var11[0]).listFiles()).length <= 0) {
               throw new IOException("wrong file format to import. found " + Arrays.toString(var12.list()));
            } else {
               for(int var16 = 0; var16 < var4.length; ++var16) {
                  if (var4[var16].getName().toLowerCase(Locale.ENGLISH).endsWith(".txt")) {
                     System.err.println("[BLUEPRINT][IMPORT] Found Old Data " + var4[var16].getName());
                     var1.convert("./bbtmp/" + var12.getName() + "/", false, var4[var16].getName());
                     var7 = true;
                     break;
                  }
               }

               if (!var7) {
                  String[] var17 = var12.list();
                  System.err.println("failed to import old method. no Catalog.txt found in " + Arrays.toString(var17) + " trying new!");

                  for(int var13 = 0; var13 < var17.length; ++var13) {
                     if (!var17[var13].equals("header.smbph")) {
                        var7 = true;
                        break;
                     }
                  }

                  if (!var7) {
                     throw new IOException("ERROR: No blueprint data found to import: " + Arrays.toString(var17));
                  }
               }

               File[] var18;
               if (!(var18 = var2.listFiles())[0].isDirectory()) {
                  throw new IOException("not a directory: " + var18[0].getAbsolutePath());
               } else {
                  FileExt var15 = new FileExt(var1.entityBluePrintPath + "/" + var18[0].getName());
                  System.err.println("IMPORT: " + var18[0].getAbsolutePath());
                  String var10 = var1.entityBluePrintPath + "/" + var18[0].getName();

                  for(int var14 = 0; var15.exists(); ++var14) {
                     var15 = new FileExt(var10 + "_" + var14);
                  }

                  FileUtil.copyDirectory(var18[0], var15);
                  var10 = var1.entityBluePrintPath + "/" + var15.getName() + "/DATA";
                  FileExt var8;
                  File[] var9;
                  if ((var8 = new FileExt(var10)).exists() && var8.isDirectory() && (var9 = var8.listFiles()).length > 0 && var9[0].getName().endsWith(".smd") && !var9[0].getName().endsWith(".smd2")) {
                     BluePrintController.migrateCatalogV00898(0, var10);
                  }

                  FileUtil.deleteRecursive(var2);
                  return var15.getName();
               }
            }
         }
      }
   }

   public static String removePoints(String var0) {
      int var1 = 0;

      int var2;
      for(var2 = -1; (var2 = var0.indexOf(".", var2 + 1)) >= 0; ++var1) {
      }

      if (var1 <= 4) {
         return var0;
      } else {
         var1 -= 4;
         StringBuffer var3 = new StringBuffer(var0);

         for(int var4 = 0; var4 < var1; ++var4) {
            for(var2 = -1; var1 > 0 && (var2 = var0.indexOf(".", var2 + 1)) >= 0; --var1) {
               var3.replace(var2, var2 + 1, "_");
            }
         }

         return var3.toString();
      }
   }

   private void calculatePrice() {
      this.price = this.elementMap.getPrice();
      BlueprintEntry var2;
      if (this.getChilds() != null) {
         for(Iterator var1 = this.getChilds().iterator(); var1.hasNext(); this.price += var2.getPrice()) {
            var2 = (BlueprintEntry)var1.next();
         }
      }

   }

   public double getMass() {
      return this.mass;
   }

   private void calculateMass() {
      this.mass = this.elementMap.getMass();
      BlueprintEntry var2;
      if (this.getChilds() != null) {
         for(Iterator var1 = this.getChilds().iterator(); var1.hasNext(); this.mass += var2.getMass()) {
            var2 = (BlueprintEntry)var1.next();
         }
      }

   }

   private void addElementCountMap(ElementCountMap var1) {
      var1.add(this.elementMap);
      if (this.getChilds() != null) {
         Iterator var2 = this.getChilds().iterator();

         while(var2.hasNext()) {
            ((BlueprintEntry)var2.next()).addElementCountMap(var1);
         }
      }

   }

   private void copyDataFilesToBlueprint(final SegmentController var1) throws IOException {
      File[] var7;
      int var2 = (var7 = (new FileExt(GameServerState.SEGMENT_DATA_DATABASE_PATH)).listFiles(new FilenameFilter() {
         public boolean accept(File var1x, String var2) {
            return var2.startsWith(var1.getUniqueIdentifier() + ".");
         }
      })).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         File var4 = var7[var3];
         FileExt var5;
         if (!(var5 = new FileExt(this.bbController.entityBluePrintPath + "/" + this.name + "/DATA/")).exists()) {
            var5.mkdirs();
         }

         String var6 = "";
         if (this.parent == null) {
            var5 = new FileExt(this.bbController.entityBluePrintPath + "/" + this.name + "/DATA/" + var4.getName());
         } else {
            var6 = var4.getName();
            var6 = this.name.substring(this.name.lastIndexOf("/")) + var6.substring(var6.indexOf("."));
            var5 = new FileExt(this.bbController.entityBluePrintPath + "/" + this.name + "/DATA/" + var6);
         }

         if (!var5.getParentFile().exists()) {
            var5.getParentFile().mkdirs();
         }

         System.err.println("[BLUEPRINT] NAME: " + this.name + " fName(" + var6 + ") COPY FILE " + var4.getName() + " TO: " + var5.getAbsolutePath());
         FileUtil.copyFile(var4, var5);
      }

   }

   private void copyLocalDataFilesToBlueptint(SegmentController var1) throws IOException {
      FileExt var2 = new FileExt(ClientStatics.SEGMENT_DATA_DATABASE_PATH);
      StringBuilder var3 = new StringBuilder();
      SegmentDataFileUtils.convertUID(var1.getUniqueIdentifier(), var1.getObfuscationString(), var3);
      final String var9 = var3.toString();
      File[] var7;
      if ((var7 = var2.listFiles(new FilenameFilter() {
         public boolean accept(File var1, String var2) {
            return var2.startsWith(var9 + ".");
         }
      })).length == 0) {
         System.err.println("[BLUEPRINT] ERROR, NO DATA FOUND FOR: " + var1 + "; UID: " + var1.getUniqueIdentifier() + "; Pattern: " + var9);
      }

      File[] var6 = var7;
      int var8 = var7.length;

      for(int var10 = 0; var10 < var8; ++var10) {
         File var4;
         String var5 = removePoints((var4 = var6[var10]).getName());
         FileExt var11;
         if (this.parent == null) {
            var5 = this.name + var5.substring(var5.indexOf("."));
            System.err.println("[BLUEPRINT] localSave pN: FNAME: " + var5);
            var11 = new FileExt(this.bbController.entityBluePrintPath + "/" + this.name + "/DATA/" + (new FileExt(var5)).getName());
         } else {
            var5 = this.name.substring(this.name.indexOf("/")) + var5.substring(var5.indexOf("."));
            System.err.println("[BLUEPRINT] localSave: FNAME: " + var5);
            var11 = new FileExt(this.bbController.entityBluePrintPath + "/" + this.name + "/DATA/" + (new FileExt(var5)).getName());
         }

         if (!var11.getParentFile().exists()) {
            var11.getParentFile().mkdirs();
         }

         FileUtil.copyFile(var4, var11);
      }

   }

   private void copyOldDataFilesToBlueprint(final SegmentControllerBluePrintEntryOld var1, String var2) throws IOException {
      FileExt var10 = new FileExt(var2 + "/DATA/");

      assert var10.isDirectory() : var10.getAbsolutePath();

      File[] var11;
      int var3 = (var11 = var10.listFiles(new FileFilter() {
         public boolean accept(File var1x) {
            return var1x.getName().startsWith(var1.name + ".");
         }
      })).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         File var5;
         if ((var5 = var11[var4]).getName().startsWith(var1.name + ".")) {
            FileExt var6;
            if (!(var6 = new FileExt(this.bbController.entityBluePrintPath + "/" + this.name + "/DATA/" + var5.getName())).getParentFile().exists()) {
               var6.getParentFile().mkdirs();
            }

            FileInputStream var14 = new FileInputStream(var5);
            FileOutputStream var15 = new FileOutputStream(var6);
            FileChannel var7 = var14.getChannel();
            FileChannel var8 = var15.getChannel();
            ByteBuffer var9 = PlayerState.buffer;

            while(true) {
               var9.clear();
               if (var7.read(var9) == -1) {
                  var7.close();
                  var8.close();
                  var14.close();
                  var15.close();
                  break;
               }

               var9.flip();
               var8.write(var9);
            }
         }
      }

      var2 = this.bbController.entityBluePrintPath + "/" + this.name + "/DATA/";
      FileExt var12;
      File[] var13;
      if ((var12 = new FileExt(var2)).exists() && var12.isDirectory() && (var13 = var12.listFiles()).length > 0 && var13[0].getName().endsWith(".smd") && !var13[0].getName().endsWith(".smd2")) {
         BluePrintController.migrateCatalogV00898(0, var2);
      }

   }

   public boolean existRawData() {
      return this.rawBlockDir.exists();
   }

   public File export() throws IOException {
      String var1 = this.bbController.entityBluePrintPath + File.separator + this.getName() + "/";
      String var2 = this.bbController.entityBluePrintPath + File.separator + "exported" + File.separator + this.name + ".sment";
      FileExt var3;
      if (!(var3 = new FileExt(var2)).getParentFile().exists()) {
         var3.getParentFile().mkdirs();
      }

      FolderZipper.zipFolder(var1, var2, (String)null, (FileFilter)null);
      return new FileExt(var2);
   }

   public BoundingBox getBb() {
      return this.bb;
   }

   public void calculateTotalBb(BoundingBox var1) {
      this.calculateTotalBb(var1, new Vector3f());
   }

   public boolean isOkWithConfig(GameConfig var1) {
      if (!var1.isBBOk(this)) {
         return false;
      } else {
         Iterator var2 = this.getChilds().iterator();

         do {
            if (!var2.hasNext()) {
               return true;
            }
         } while(((BlueprintEntry)var2.next()).isOkWithConfig(var1));

         return false;
      }
   }

   private void calculateTotalBb(BoundingBox var1, Vector3f var2) {
      var1.expand(this.railRootMinTotal, this.railRootMaxTotal);
      Vector3f var3 = new Vector3f();
      Vector3f var4 = new Vector3f();
      var3.add(var2, this.bb.min);
      var4.add(var2, this.bb.max);
      var1.expand(var3, var4);
      if (this.childs != null) {
         for(int var6 = 0; var6 < this.childs.size(); ++var6) {
            BlueprintEntry var7;
            if (!(var7 = (BlueprintEntry)this.childs.get(var6)).railDock) {
               Vector3f var5;
               Vector3f var10000 = var5 = new Vector3f(var2);
               var10000.x += (float)var7.dockingPos.x;
               var5.y += (float)var7.dockingPos.y;
               var5.z += (float)var7.dockingPos.z;
               var7.calculateTotalBb(var1, var5);
            }
         }
      }

   }

   public BluePrintController getBbController() {
      return this.bbController;
   }

   public List getChilds() {
      return this.childs;
   }

   public ControlElementMapper getControllingMap() {
      try {
         return this.readStructure(false);
      } catch (IOException var1) {
         var1.printStackTrace();
         return new ControlElementMapper();
      }
   }

   public ElementCountMap getElementMap() {
      return this.elementMap;
   }

   public ElementCountMap getElementCountMapWithChilds() {
      if (this.countWithChilds == null) {
         this.countWithChilds = new ElementCountMap(this.elementMap);
         if (this.getChilds() != null) {
            Iterator var1 = this.getChilds().iterator();

            while(var1.hasNext()) {
               ((BlueprintEntry)var1.next()).addElementCountMap(this.countWithChilds);
            }
         }
      }

      return this.countWithChilds;
   }

   public String getName() {
      return this.name;
   }

   public long getPrice() {
      return this.price;
   }

   public EntityIndexScore getScore() {
      return this.score;
   }

   public BlueprintType getType() {
      return this.entityType;
   }

   public Tag getAiTag() {
      return this.aiTag;
   }

   public byte getDockingOrientation() {
      return this.dockingOrientation;
   }

   public Vector3i getDockingPos() {
      return this.dockingPos;
   }

   public Vector3f getDockingSize() {
      return this.dockingSize;
   }

   public short getDockingStyle() {
      return this.dockingStyle;
   }

   public BlueprintType getEntityType() {
      return this.entityType;
   }

   public void setEntityType(BlueprintType var1) {
      this.entityType = var1;
   }

   public Tag getManagerTag() {
      if (this.managerTag == null) {
         try {
            this.readManagerTagOnly(this.meta);
         } catch (IOException var1) {
            var1.printStackTrace();
         }
      }

      return this.managerTag;
   }

   public File[] getRawBlockData() {
      return this.rawBlockDir.listFiles();
   }

   public void init(String var1, BluePrintController var2) {
      if (var1.contains(".")) {
         System.err.println("Exception: WARNING: Blueprint name contained irregular characters: \"" + var1 + "\"! Removing characters...");
      }

      this.name = var1.replaceAll("\\.", "");
      this.header = new FileExt(var2.entityBluePrintPath + "/" + var1 + "/header.smbph");
      this.structure = new FileExt(var2.entityBluePrintPath + "/" + var1 + "/logic.smbpl");
      this.meta = new FileExt(var2.entityBluePrintPath + "/" + var1 + "/meta.smbpm");
      this.rawBlockDir = new FileExt(var2.entityBluePrintPath + "/" + var1 + "/DATA/");
   }

   public void read() throws IOException {
      this.headerModified = this.header.lastModified();
      this.metaModified = this.meta.lastModified();
      this.structModified = this.structure.lastModified();
      long var1 = System.currentTimeMillis();
      this.readHeader(this.header);
      this.setTookHeaderRead(System.currentTimeMillis() - var1);
      var1 = System.currentTimeMillis();
      synchronized(this.getClass()) {
         if (this.needsChunkMigration) {
            BluePrintController.migrating = true;
            String var4 = this.bbController.entityBluePrintPath + "/" + this.name + "/";
            this.migrateChunk16To32(var4);
            System.err.println("[BLUEPRINT] Chunk16 blueprint detected! Doing migration to chunk32...");
            DataOutputStream var6 = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(this.header)));
            writeHeader(this.bb.min, this.bb.max, this.entityType, this.score, this.classification, this.elementMap, var6);
            var6.close();
         }
      }

      this.readMeta(this.meta, false, true);
      this.setTookMetaRead(System.currentTimeMillis() - var1);
      var1 = System.currentTimeMillis();
      this.elementMap.convertNonPlaceableBlocks();
      this.calculatePrice();
      this.calculateMass();
      this.setTookStructureRead(System.currentTimeMillis() - var1);
      System.currentTimeMillis();
   }

   private void migrateChunk16To32(String var1) {
      var1 = var1 + "DATA/";
      FileExt var2;
      if ((var2 = new FileExt(var1)).exists() && var2.isDirectory()) {
         Chunk32Migration.processFolder(var1, true, (FolderZipper.ZipCallback)null, false);
      }

   }

   public void readManagerTagOnly(File var1) throws IOException {
      DataInputStream var12;
      int var2 = (var12 = new DataInputStream(new BufferedInputStream(new FileInputStream(var1), 65536))).readInt();
      boolean var3 = false;
      if (var2 < 4) {
         var3 = true;
      }

      boolean var5 = false;

      byte var4;
      label115:
      while(!var5 && (var4 = var12.readByte()) != 1) {
         int var6;
         int var7;
         boolean var13;
         int var14;
         switch(var4) {
         case 2:
            this.managerTag = Tag.readFrom(var12, false, false);
            var5 = true;
            break;
         case 3:
            var14 = var12.readInt();
            var6 = 0;

            while(true) {
               if (var6 >= var14) {
                  continue label115;
               }

               String var19 = var12.readUTF();
               BlueprintEntry var21;
               (var21 = new BlueprintEntry(var19, this.bbController)).dockingPos = new Vector3i(var12.readInt(), var12.readInt(), var12.readInt());
               if (this.isChunk16()) {
                  var21.dockingPos.add(Chunk16SegmentData.SHIFT);
               }

               var21.dockingSize = new Vector3f(var12.readFloat(), var12.readFloat(), var12.readFloat());
               var21.dockingStyle = var12.readShort();
               var21.dockingOrientation = var12.readByte();
               var21.parent = this;
               var21.railDock = false;
               ++var6;
            }
         case 4:
            Vector3f var15 = new Vector3f();
            Vector3f var18 = new Vector3f();
            var15.set(var12.readFloat(), var12.readFloat(), var12.readFloat());
            var18.set(var12.readFloat(), var12.readFloat(), var12.readFloat());
            int var9;
            int var20;
            if (var2 >= 2) {
               var12.readUTF();
               var20 = var12.readInt();

               for(var9 = 0; var9 < var20; ++var9) {
                  (new BBWirelessLogicMarker()).deserialize(var12, var3 ? 8 : 0);
               }
            }

            var20 = var12.readInt();
            var9 = 0;

            while(true) {
               if (var9 >= var20) {
                  continue label115;
               }

               String var22 = var12.readUTF();
               new BlueprintEntry(var22, this.bbController);
               if ((var14 = var12.readInt()) < 0 || var14 > 1000000000) {
                  throw new IOException("Invalid tag size: " + var14);
               }

               byte[] var16 = new byte[var14];
               var12.readFully(var16);
               Tag.readFrom(new FastByteArrayInputStream(var16), true, false);
               ++var9;
            }
         case 5:
            if ((var14 = var12.readInt()) >= 0 && var14 <= 1000000000) {
               byte[] var17 = new byte[var14];
               var12.readFully(var17);
               Tag.readFrom(new FastByteArrayInputStream(var17), true, false);
               break;
            }

            throw new IOException("Invalid tag size: " + var14);
         case 6:
            if (!(var13 = var12.readByte() > 0) || !var13) {
               break;
            }

            var6 = var12.readInt();
            var7 = 0;

            while(true) {
               if (var7 >= var6) {
                  continue label115;
               }

               VoidUniqueSegmentPiece.deserizalizeWithoutUID(var12);
               ++var7;
            }
         case 7:
            var13 = var12.readByte() > 0;
            this.cargoPoints = new Long2DoubleOpenHashMap();
            if (var13) {
               var6 = var12.readInt();

               for(var7 = 0; var7 < var6; ++var7) {
                  long var8 = var12.readLong();
                  double var10 = var12.readDouble();
                  this.cargoPoints.put(var8, var10);
               }

               if (this.cargoPoints.size() > 0) {
                  this.cargoPoints.trim();
               }
            }

            this.hadCargoByte = true;
            break;
         default:
            throw new IOException("Unknown data type: " + var4);
         }
      }

      var12.close();
   }

   public void readMeta(File var1, boolean var2, boolean var3) throws IOException {
      DataInputStream var12 = new DataInputStream(new BufferedInputStream(new FileInputStream(var1), 65536));
      this.metaVersionRead = var12.readInt();
      if (this.metaVersionRead < 4) {
         this.loadedChunk16 = true;
      }

      int var4 = 0;
      boolean var5 = false;

      label139:
      while(!var5 && (var4 = var12.readByte()) != 1) {
         int var6;
         int var7;
         String var16;
         BlueprintEntry var17;
         switch(var4) {
         case 2:
            if (var2) {
               this.managerTag = Tag.readFrom(var12, false, false);
            }

            var5 = true;
            break;
         case 3:
            if (this.childs == null) {
               this.childs = new ObjectArrayList();
            }

            var4 = var12.readInt();
            var6 = 0;

            while(true) {
               if (var6 >= var4) {
                  continue label139;
               }

               var16 = var12.readUTF();
               (var17 = new BlueprintEntry(var16, this.bbController)).dockingPos = new Vector3i(var12.readInt(), var12.readInt(), var12.readInt());
               if (this.isChunk16()) {
                  var17.dockingPos.add(Chunk16SegmentData.SHIFT);
               }

               var17.dockingSize = new Vector3f(var12.readFloat(), var12.readFloat(), var12.readFloat());
               var17.dockingStyle = var12.readShort();
               var17.dockingOrientation = var12.readByte();
               var17.parent = this;
               var17.railDock = false;
               this.childs.add(var17);
               ++var6;
            }
         case 4:
            this.railRootMinTotal.set(var12.readFloat(), var12.readFloat(), var12.readFloat());
            this.railRootMaxTotal.set(var12.readFloat(), var12.readFloat(), var12.readFloat());
            if (this.childs == null) {
               this.childs = new ObjectArrayList();
            }

            this.railUID = null;
            if (this.metaVersionRead >= 2) {
               this.railUID = var12.readUTF();
               var4 = var12.readInt();

               for(var6 = 0; var6 < var4; ++var6) {
                  BBWirelessLogicMarker var15;
                  (var15 = new BBWirelessLogicMarker()).deserialize(var12, this.loadedChunk16 ? 8 : 0);
                  this.wirelessToOwnRail.add(var15);
               }
            }

            var4 = var12.readInt();
            var6 = 0;

            while(true) {
               if (var6 >= var4) {
                  continue label139;
               }

               var16 = var12.readUTF();
               var17 = new BlueprintEntry(var16, this.bbController);
               int var9;
               if ((var9 = var12.readInt()) < 0 || var9 > 1000000000) {
                  throw new IOException("Invalid tag size: " + var9);
               }

               byte[] var18 = new byte[var9];
               var12.readFully(var18);
               var17.railTag = Tag.readFrom(new FastByteArrayInputStream(var18), true, false);
               var17.parent = this;
               var17.railDock = true;
               this.childs.add(var17);
               ++var6;
            }
         case 5:
            if ((var4 = var12.readInt()) >= 0 && var4 <= 1000000000) {
               byte[] var13 = new byte[var4];
               var12.readFully(var13);
               this.aiTag = Tag.readFrom(new FastByteArrayInputStream(var13), true, false);
               break;
            }

            throw new IOException("Invalid tag size: " + var4);
         case 6:
            if ((var4 = var12.readByte() > 0 ? 1 : 0) == 0 || var4 == 0) {
               break;
            }

            this.dockerPoints = new Long2ObjectOpenHashMap();
            var6 = var12.readInt();

            for(var7 = 0; var7 < var6; ++var7) {
               VoidUniqueSegmentPiece var14 = VoidUniqueSegmentPiece.deserizalizeWithoutUID(var12);
               this.dockerPoints.put(var14.getAbsoluteIndex(), var14);
            }

            this.dockerPoints.trim();
            break;
         case 7:
            if (var12.readByte() > 0) {
               this.cargoPoints = new Long2DoubleOpenHashMap();
               var6 = var12.readInt();

               for(var7 = 0; var7 < var6; ++var7) {
                  long var8 = var12.readLong();
                  double var10 = var12.readDouble();
                  this.cargoPoints.put(var8, var10);
               }

               if (this.cargoPoints.size() > 0) {
                  this.cargoPoints.trim();
               }
            }

            this.hadCargoByte = true;
            break;
         default:
            throw new IOException("Unknown data type: " + var4);
         }
      }

      var12.close();
      if (var3 && this.childs != null) {
         for(var4 = 0; var4 < this.childs.size(); ++var4) {
            ((BlueprintEntry)this.childs.get(var4)).read();
         }
      }

   }

   public ControlElementMapper readStructure(boolean var1) throws IOException {
      return this.readStructure(this.structure, var1);
   }

   public ControlElementMapper readStructure(File var1, boolean var2) throws IOException {
      if (this.controlElementMap == null || var2) {
         DataInputStream var3;
         (var3 = new DataInputStream(new BufferedInputStream(new FileInputStream(var1), 65536))).readInt();
         ControlElementMapper var4 = new ControlElementMapper();
         ControlElementMap.deserialize(var3, var4);
         this.controlElementMap = var4;
         var3.close();
      }

      return this.controlElementMap;
   }

   public String toString() {
      return this.name;
   }

   public void update() {
   }

   public void write(SegmentController var1, boolean var2) throws IOException {
      if (var1 instanceof FloatingRockManaged) {
         throw new IOException("Asteroids cannot be saved in the catalog currently to prevent duping");
      } else {
         FileExt var3 = new FileExt(this.bbController.entityBluePrintPath + "/" + this.name + "Tmp/");
         this.header.getParentFile().renameTo(var3);
         if (!this.header.getParentFile().exists()) {
            this.header.getParentFile().mkdirs();
         }

         if (this.classification == null) {
            System.err.println("[BLUEPRINT] use default classification");
            this.classification = var1.getType().getDefaultClassification();
         }

         DataOutputStream var4 = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(this.header)));
         this.writeHeader(var1, var4);
         var4.close();
         var4 = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(this.structure)));
         this.writeStructure(var1, var4);
         var4.close();
         FileOutputStream var7 = new FileOutputStream(this.meta);
         DataOutputStream var5 = new DataOutputStream(new BufferedOutputStream(var7));
         this.writeMeta(var1, var5, var7);
         var5.close();
         if (var2) {
            System.err.println("[CLIENT][BLUEPRINT][LOCAL] " + var1 + "; childs: " + this.childsToWrite.size());
            this.copyLocalDataFilesToBlueptint(var1);
         } else {
            this.copyDataFilesToBlueprint(var1);
         }

         if (this.childsToWrite.size() > 0) {
            for(int var6 = 0; var6 < this.childsToWrite.size(); ++var6) {
               ((BlueprintEntry)this.childsToWrite.get(var6)).write(((BlueprintEntry)this.childsToWrite.get(var6)).delayedWriteSegCon, var2);
               ((BlueprintEntry)this.childsToWrite.get(var6)).delayedWriteSegCon = null;
            }
         }

         this.childsToWrite = null;
         if (this.parent == null) {
            FileUtil.deleteRecursive(var3);
         }

      }
   }

   public void write(SegmentControllerBluePrintEntryOld var1, String var2) throws IOException {
      FileExt var3 = new FileExt(this.bbController.entityBluePrintPath + "/" + this.name + "Tmp/");
      this.header.getParentFile().renameTo(var3);
      if (!this.header.getParentFile().exists()) {
         this.header.getParentFile().mkdirs();
      }

      try {
         this.copyOldDataFilesToBlueprint(var1, var2);
         DataOutputStream var5 = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(this.header)));
         this.writeHeader(var1, var5);
         var5.close();
         var5 = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(this.structure)));
         this.writeStructure(var1, var5);
         var5.close();
         var5 = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(this.meta)));
         this.writeMeta(var1, var5);
         var5.close();
         FileUtil.deleteRecursive(var3);
      } catch (Exception var4) {
         FileUtil.deleteRecursive(this.header.getParentFile());
      }
   }

   public static void writeHeader(Vector3f var0, Vector3f var1, BlueprintType var2, EntityIndexScore var3, BlueprintClassification var4, ElementCountMap var5, DataOutputStream var6) throws IOException {
      var6.writeInt(5);
      String var7 = Version.VERSION + "_" + Version.build;
      var6.writeUTF(var7);
      var6.writeInt(var2.ordinal());
      var6.writeInt(var4.ordinal());
      var0 = new Vector3f(var0);
      var1 = new Vector3f(var1);
      var6.writeFloat(var0.x);
      var6.writeFloat(var0.y);
      var6.writeFloat(var0.z);
      var6.writeFloat(var1.x);
      var6.writeFloat(var1.y);
      var6.writeFloat(var1.z);
      var5.serialize(var6);
      var6.writeBoolean(var3 != null);
      if (var3 != null) {
         var3.serialize(var6, true);
      }

   }

   public void readHeader(File var1) throws IOException {
      try {
         DataInputStream var5 = new DataInputStream(new BufferedInputStream(new FileInputStream(var1), 65536));
         this.headerVersion = var5.readInt();
         if (this.headerVersion >= 5) {
            this.blueprintSavedInGameVersion = var5.readUTF();
         }

         if (this.headerVersion < 4) {
            this.setOldPowerFlag(true);
         }

         if (this.headerVersion < 2) {
            this.needsChunkMigration = true;
         }

         Vector3f var2;
         Vector3f var3;
         if (this.headerVersion == 0) {
            this.entityType = BlueprintType.values()[var5.readInt()];
            var2 = new Vector3f(var5.readFloat(), var5.readFloat(), var5.readFloat());
            var3 = new Vector3f(var5.readFloat(), var5.readFloat(), var5.readFloat());
            this.bb = new BoundingBox(var2, var3);
            this.elementMap = new ElementCountMap();
            this.elementMap.deserialize(var5);
            this.classification = this.entityType.type.getDefaultClassification();
         } else {
            this.entityType = BlueprintType.values()[var5.readInt()];
            if (this.headerVersion >= 3) {
               this.classification = BlueprintClassification.values()[var5.readInt()];
            } else {
               this.classification = this.entityType.type.getDefaultClassification();
            }

            var2 = new Vector3f(var5.readFloat(), var5.readFloat(), var5.readFloat());
            var3 = new Vector3f(var5.readFloat(), var5.readFloat(), var5.readFloat());
            this.bb = new BoundingBox(var2, var3);
            this.elementMap = new ElementCountMap();
            this.elementMap.deserialize(var5);
            if (var5.readBoolean()) {
               this.score = new EntityIndexScore();
               this.score.deserialize(var5, 0, true);
            }
         }

         var5.close();
      } catch (RuntimeException var4) {
         System.err.println("ERROR: " + this.headerVersion + "; " + this.getName() + "; " + this.blueprintSavedInGameVersion);
         throw var4;
      }
   }

   public void writeHeader(SegmentController var1, DataOutputStream var2) throws IOException {
      if (var1 instanceof ManagedSegmentController) {
         ManagerContainer var3;
         if ((var3 = ((ManagedSegmentController)var1).getManagerContainer()).getStatisticsManager() != null) {
            try {
               this.score = var3.getStatisticsManager().calculateIndex();
            } catch (Exception var4) {
               var4.printStackTrace();
            }
         }

         if (var3 instanceof ShipManagerContainer) {
            ((ShipManagerContainer)var3).getRailBeam();
         }

         var3.getCargo();
      }

      writeHeader(var1.getBoundingBox().min, var1.getBoundingBox().max, BlueprintType.getType(var1.getClass()), this.score, this.classification, var1.getElementClassCountMap(), var2);
   }

   private void writeHeader(SegmentControllerBluePrintEntryOld var1, DataOutputStream var2) throws IOException {
      var2.writeInt(5);
      var2.writeUTF(Version.VERSION + "_" + Version.build);
      var2.writeInt(var1.entityType);
      if (BlueprintType.values()[var1.entityType] == BlueprintType.SPACE_STATION) {
         var2.writeInt(BlueprintClassification.NONE_STATION.ordinal());
      } else {
         var2.writeInt(BlueprintClassification.NONE.ordinal());
      }

      Vector3i var3 = new Vector3i(var1.bb.min);
      Vector3i var4 = new Vector3i(var1.bb.max);
      var2.writeFloat((float)var3.x);
      var2.writeFloat((float)var3.y);
      var2.writeFloat((float)var3.z);
      var2.writeFloat((float)var4.x);
      var2.writeFloat((float)var4.y);
      var2.writeFloat((float)var4.z);
      ElementCountMap var5;
      (var5 = new ElementCountMap()).load(var1.elementMap);
      var5.serialize(var2);
   }

   public void writeMeta(SegmentController var1, DataOutputStream var2, FileOutputStream var3) throws IOException {
      ManagerModuleCollection var4 = null;
      RailBeamElementManager var5 = null;
      if (var1 instanceof ManagedSegmentController) {
         ManagerContainer var6;
         if ((var6 = ((ManagedSegmentController)var1).getManagerContainer()) instanceof ShipManagerContainer) {
            var5 = ((ShipManagerContainer)var6).getRailBeam();
         }

         var4 = var6.getCargo();
      }

      var2.writeInt(5);
      var2.writeByte(3);
      var2.writeInt(var1.getDockingController().getDockedOnThis().size());
      this.childsToWrite = new ObjectArrayList();
      int var20 = 0;
      Iterator var7 = var1.getDockingController().getDockedOnThis().iterator();

      while(var7.hasNext()) {
         ElementDocking var8;
         SegmentController var9 = (var8 = (ElementDocking)var7.next()).from.getSegment().getSegmentController();
         Vector3i var10 = var8.to.getAbsolutePos(new Vector3i());
         String var11 = this.name + "/ATTACHED_" + var20;
         var2.writeUTF(var11);
         var2.writeInt(var10.x);
         var2.writeInt(var10.y);
         var2.writeInt(var10.z);
         var2.writeFloat(var9.getDockingController().getSize().x);
         var2.writeFloat(var9.getDockingController().getSize().y);
         var2.writeFloat(var9.getDockingController().getSize().z);
         var2.writeShort(var8.to.getType());
         var2.writeByte(-1);
         BlueprintEntry var12;
         (var12 = new BlueprintEntry(var11, this.bbController)).parent = this;
         var12.delayedWriteSegCon = var9;
         this.childsToWrite.add(var12);
         ++var20;
         var2.flush();
      }

      var2.writeByte(6);
      if (var5 == null) {
         var2.writeByte(0);
      } else {
         var2.writeByte(1);
         ObjectArrayList var22 = new ObjectArrayList();
         Iterator var24 = var5.getRailDockers().iterator();

         while(var24.hasNext()) {
            long var26 = (Long)var24.next();
            SegmentPiece var33;
            if ((var33 = var1.getSegmentBuffer().getPointUnsave(var26)) != null) {
               var22.add(new VoidUniqueSegmentPiece(var33));
            }
         }

         var2.writeInt(var22.size());

         for(int var25 = 0; var25 < var22.size(); ++var25) {
            ((VoidUniqueSegmentPiece)var22.get(var25)).serializeWithoutUID(var2);
         }
      }

      var2.writeByte(7);
      if (var4 == null) {
         var2.writeByte(0);
      } else {
         var2.writeByte(1);
         var2.writeInt(var4.getCollectionManagersMap().size());
         Iterator var28 = var4.getCollectionManagersMap().entrySet().iterator();

         while(var28.hasNext()) {
            Entry var30 = (Entry)var28.next();
            var2.writeLong((Long)var30.getKey());
            double var35 = ((CargoCollectionManager)var30.getValue()).getCapacity();
            var2.writeDouble(var35);
         }
      }

      var2.writeByte(4);
      Vector3f var23 = new Vector3f();
      Vector3f var27 = new Vector3f();
      if (var1.railController.isRoot() && var1.getPhysicsDataContainer() != null && var1.getPhysicsDataContainer().getShape() != null) {
         Transform var29;
         (var29 = new Transform()).setIdentity();
         var1.getPhysicsDataContainer().getShape().getAabb(var29, var23, var27);
      }

      var2.writeFloat(var23.x);
      var2.writeFloat(var23.y);
      var2.writeFloat(var23.z);
      var2.writeFloat(var27.x);
      var2.writeFloat(var27.y);
      var2.writeFloat(var27.z);
      var2.writeUTF(var1.railController.getRailUID());
      ObjectArrayList var31 = new ObjectArrayList();
      if (var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof ActivationManagerInterface) {
         Iterator var36 = ((ActivationManagerInterface)((ManagedSegmentController)var1).getManagerContainer()).getActivation().getCollectionManagers().iterator();

         while(var36.hasNext()) {
            SegmentController var15;
            ActivationCollectionManager var37;
            if ((var37 = (ActivationCollectionManager)var36.next()).getDestination() != null && (var15 = var1.railController.getRoot().railController.getChainElementByUID(var37.getDestination().marking)) != null) {
               BBWirelessLogicMarker var13;
               (var13 = new BBWirelessLogicMarker()).fromLocation = ElementCollection.getIndex(var37.getControllerPos());
               var13.markerLocation = var37.getDestination().markerLocation;
               var13.marking = var15.railController.getRailUID();
               var31.add(var13);
            }
         }
      }

      var2.writeInt(var31.size());

      for(int var32 = 0; var32 < var31.size(); ++var32) {
         ((BBWirelessLogicMarker)var31.get(var32)).serialize(var2);
      }

      var2.writeInt(var1.railController.next.size());

      for(Iterator var34 = var1.railController.next.iterator(); var34.hasNext(); ++var20) {
         SegmentController var39 = ((RailRelation)var34.next()).docked.getSegmentController();
         String var16 = this.name + "/ATTACHED_" + var20;
         var2.writeUTF(var16);
         Tag var41 = var39.railController.getTag();
         ByteArrayOutputStream var14 = new ByteArrayOutputStream(1024);
         var41.writeTo(var14, false);
         int var18 = var14.size();
         var2.writeInt(var18);
         var14.writeTo(var2);
         BlueprintEntry var17;
         (var17 = new BlueprintEntry(var16, this.bbController)).parent = this;
         var17.delayedWriteSegCon = var39;
         var17.railDock = true;
         this.childsToWrite.add(var17);
      }

      if (var1 instanceof SegmentControllerAIInterface) {
         var2.writeByte(5);
         Tag var38 = ((SegmentControllerAIInterface)var1).getAiConfiguration().toTagStructure();
         ByteArrayOutputStream var40 = new ByteArrayOutputStream(1024);
         var38.writeTo(var40, false);
         int var19 = var40.size();
         var2.writeInt(var19);
         var40.writeTo(var2);
      }

      if (var1 instanceof ManagedSegmentController) {
         var2.writeByte(2);
         var3.getChannel().position();
         Tag var42;
         if (((ManagedSegmentController)var1).getManagerContainer() instanceof StationaryManagerContainer) {
            ShoppingAddOn var21;
            long var43 = (var21 = ((StationaryManagerContainer)((ManagedSegmentController)var1).getManagerContainer()).getShoppingAddOn()).getCredits();
            var21.setCredits(0L);
            var42 = ((ManagedSegmentController)var1).getManagerContainer().toTagStructure();
            var21.setCredits(var43);
         } else {
            var42 = ((ManagedSegmentController)var1).getManagerContainer().toTagStructure();
         }

         var42.writeTo(var2, false);
         var2.flush();
      } else {
         var2.writeByte(1);
      }

      var2.flush();
   }

   private void writeMeta(SegmentControllerBluePrintEntryOld var1, DataOutputStream var2) throws IOException {
      var2.writeInt(5);
      var2.writeByte(1);
   }

   public void writeStructure(SegmentController var1, DataOutputStream var2) throws IOException {
      var2.writeInt(0);
      ControlElementMap.serializeForDisk(var2, var1.getControlElementMap().getControllingMap());
   }

   private void writeStructure(SegmentControllerBluePrintEntryOld var1, DataOutputStream var2) throws IOException {
      var2.writeInt(0);
      ControlElementMap.serializeForDisk(var2, var1.getControllingMap());
   }

   public void canSpawn(StateInterface var1, String var2) throws EntityAlreadyExistsException {
      canSpawn(var1, var2, this.getEntityType(), false);
   }

   public static void canSpawn(StateInterface var0, String var1, BlueprintType var2, boolean var3) throws EntityAlreadyExistsException {
      String var4 = null;
      if (var2 == null) {
         throw new NullPointerException("Entity has no type...");
      } else {
         switch(var2) {
         case SHIP:
            var4 = EntityRequest.convertShipEntityName(var1);
            break;
         case ASTEROID:
            var4 = EntityRequest.convertAsteroidEntityName(var1, false);
            break;
         case SHOP:
            var4 = EntityRequest.convertShopEntityName(var1);
            break;
         case SPACE_STATION:
            var4 = EntityRequest.convertStationEntityName(var1);
            break;
         case PLANET:
            var4 = EntityRequest.convertPlanetEntityName(var1);
            break;
         case MANAGED_ASTEROID:
            var4 = EntityRequest.convertAsteroidEntityName(var1, false);
         }

         if (var4 == null || EntityRequest.existsIdentifierWOExc(var0, var4)) {
            try {
               int var5 = 0;
               if (var3) {
                  System.err.println("[PURGE] Removing file " + var4);
                  (new FileExt(var4)).delete();
                  if (((GameServerState)var0).getDatabaseIndex().getTableManager().getEntityTable().getIdForFullUID(var2.type.dbPrefix + var1) >= 0L) {
                     ((GameServerState)var0).getDatabaseIndex().getTableManager().getEntityTable().removeSegmentController(var2.type.dbPrefix + var1, (GameServerState)var0);
                  }

                  return;
               }

               if (var1.startsWith("MOB_SIM") && (var5 = ((GameServerState)var0).getDatabaseIndex().getTableManager().getEntityTable().getByUIDExact(var1, 1).size()) <= 0) {
                  System.err.println("[MOB] File exists but is not in database: Removing file " + var4);
                  (new FileExt(var4)).delete();
                  return;
               }

               if (var5 > 0) {
                  System.err.println("ENTITY WAS FOUND IN DATABSASE");
               }

               throw new EntityAlreadyExistsException("FILE: " + var4 + "; Name-Only: " + var1);
            } catch (SQLException var6) {
               var6.printStackTrace();
            }
         }

      }
   }

   public String getRailUID() {
      return this.railUID;
   }

   private boolean isOldDockingRec() {
      if (this.getChilds() != null) {
         Iterator var1 = this.getChilds().iterator();

         while(var1.hasNext()) {
            if (((BlueprintEntry)var1.next()).isOldDockingRec()) {
               return true;
            }
         }
      }

      return !this.railDock;
   }

   public boolean hasOldDocking() {
      if (this.getChilds() != null) {
         Iterator var1 = this.getChilds().iterator();

         while(var1.hasNext()) {
            if (((BlueprintEntry)var1.next()).isOldDockingRec()) {
               return true;
            }
         }
      }

      return false;
   }

   public boolean isChunk16() {
      return this.loadedChunk16;
   }

   public BlueprintClassification getClassification() {
      return this.classification;
   }

   public void setClassification(BlueprintClassification var1) {
      if (var1 != null) {
         this.classification = var1;
      } else {
         System.err.println("[BLUEPRINTENTRY] NOT SETTING CLASSIFICATION (provided null)");
      }
   }

   public int getHeaderVersion() {
      return this.headerVersion;
   }

   public boolean isOldPowerFlag() {
      return this.oldPowerFlag;
   }

   public void setOldPowerFlag(boolean var1) {
      this.oldPowerFlag = var1;
   }

   public long getTookHeaderRead() {
      return this.tookHeaderRead;
   }

   public void setTookHeaderRead(long var1) {
      this.tookHeaderRead = var1;
   }

   public long getTookMetaRead() {
      return this.tookMetaRead;
   }

   public void setTookMetaRead(long var1) {
      this.tookMetaRead = var1;
   }

   public long getTookStructureRead() {
      return this.tookStructureRead;
   }

   public void setTookStructureRead(long var1) {
      this.tookStructureRead = var1;
   }

   public boolean isFilesDirty() {
      return this.headerModified != this.header.lastModified() || this.metaModified != this.meta.lastModified() || this.structModified != this.structure.lastModified();
   }

   public Long2ObjectOpenHashMap getDockerPoints() {
      return this.dockerPoints;
   }
}
