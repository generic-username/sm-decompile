package org.schema.game.server.data.blueprintnw;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.FloatingRock;
import org.schema.game.common.controller.FloatingRockManaged;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.BlueprintInterface;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.SpawnInterface;
import org.schema.game.server.data.blueprint.AsteroidOutline;
import org.schema.game.server.data.blueprint.ChildStats;
import org.schema.game.server.data.blueprint.PlanetOutline;
import org.schema.game.server.data.blueprint.ShipOutline;
import org.schema.game.server.data.blueprint.ShopOutline;
import org.schema.game.server.data.blueprint.SpaceStationOutline;

public enum BlueprintType {
   SHIP(SimpleTransformableSendableObject.EntityType.SHIP, Ship.class, new SpawnInterface() {
      public final ShipOutline inst(GameServerState var1, BlueprintInterface var2, String var3, String var4, float[] var5, int var6, Vector3i var7, Vector3i var8, String var9, boolean var10, Vector3i var11, ChildStats var12) {
         return new ShipOutline(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12);
      }
   }),
   SHOP(SimpleTransformableSendableObject.EntityType.SHOP, ShopSpaceStation.class, new SpawnInterface() {
      public final ShopOutline inst(GameServerState var1, BlueprintInterface var2, String var3, String var4, float[] var5, int var6, Vector3i var7, Vector3i var8, String var9, boolean var10, Vector3i var11, ChildStats var12) {
         return new ShopOutline(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12);
      }
   }),
   SPACE_STATION(SimpleTransformableSendableObject.EntityType.SPACE_STATION, SpaceStation.class, new SpawnInterface() {
      public final SpaceStationOutline inst(GameServerState var1, BlueprintInterface var2, String var3, String var4, float[] var5, int var6, Vector3i var7, Vector3i var8, String var9, boolean var10, Vector3i var11, ChildStats var12) {
         return new SpaceStationOutline(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12);
      }
   }),
   MANAGED_ASTEROID(SimpleTransformableSendableObject.EntityType.ASTEROID_MANAGED, FloatingRockManaged.class, new SpawnInterface() {
      public final SpaceStationOutline inst(GameServerState var1, BlueprintInterface var2, String var3, String var4, float[] var5, int var6, Vector3i var7, Vector3i var8, String var9, boolean var10, Vector3i var11, ChildStats var12) {
         return new SpaceStationOutline(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12);
      }
   }),
   ASTEROID(SimpleTransformableSendableObject.EntityType.ASTEROID, FloatingRock.class, new SpawnInterface() {
      public final AsteroidOutline inst(GameServerState var1, BlueprintInterface var2, String var3, String var4, float[] var5, int var6, Vector3i var7, Vector3i var8, String var9, boolean var10, Vector3i var11, ChildStats var12) {
         return new AsteroidOutline(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12);
      }
   }),
   PLANET(SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT, Planet.class, new SpawnInterface() {
      public final PlanetOutline inst(GameServerState var1, BlueprintInterface var2, String var3, String var4, float[] var5, int var6, Vector3i var7, Vector3i var8, String var9, boolean var10, Vector3i var11, ChildStats var12) {
         return new PlanetOutline(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12);
      }
   });

   public SpawnInterface iFace;
   private Class clazz;
   public final SimpleTransformableSendableObject.EntityType type;

   private BlueprintType(SimpleTransformableSendableObject.EntityType var3, Class var4, SpawnInterface var5) {
      this.clazz = var4;
      this.iFace = var5;
      this.type = var3;
   }

   public static BlueprintType getType(Class var0) {
      for(int var1 = 0; var1 < values().length; ++var1) {
         if (values()[var1].clazz == var0) {
            return values()[var1];
         }
      }

      throw new NullPointerException("SegControllerType Not Found: " + var0.getClass() + "; " + var0);
   }

   public final boolean enemySpawnable() {
      return this == SHIP;
   }
}
