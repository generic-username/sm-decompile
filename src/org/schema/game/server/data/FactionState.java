package org.schema.game.server.data;

import org.schema.game.common.data.player.faction.FactionManager;

public interface FactionState {
   FactionManager getFactionManager();
}
