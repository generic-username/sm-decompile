package org.schema.game.server.data;

public class ProtectedUplinkName implements Comparable {
   public final String uplinkname;
   public final String playername;
   public final long timeProtected;

   public ProtectedUplinkName(String var1, String var2, long var3) {
      this.uplinkname = var1;
      this.timeProtected = var3;
      this.playername = var2;
   }

   public int hashCode() {
      return this.uplinkname.hashCode();
   }

   public boolean equals(Object var1) {
      return this.uplinkname.equals(var1);
   }

   public String toString() {
      return "ProtectedUplinkName [username=" + this.uplinkname + ", timeProtected=" + this.timeProtected + "]";
   }

   public int compareTo(ProtectedUplinkName var1) {
      return (int)(this.timeProtected - var1.timeProtected);
   }
}
