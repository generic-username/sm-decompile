package org.schema.game.server.data;

public class PlayerNotFountException extends Exception {
   private static final long serialVersionUID = 1L;

   public PlayerNotFountException(String var1) {
      super("Player not found: \"" + var1 + "\"");
   }
}
