package org.schema.game.server.data;

import java.io.IOException;
import java.sql.SQLException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.controller.SectorUtil;
import org.schema.schine.network.RegisteredClientInterface;

public class SectorExportRequest implements ServerExecutionJob {
   public Vector3i pos;
   public RegisteredClientInterface client;
   private String name;

   public SectorExportRequest(Vector3i var1, RegisteredClientInterface var2, String var3) {
      this.pos = var1;
      this.client = var2;
      this.name = var3;
   }

   public boolean execute(GameServerState var1) {
      try {
         SectorUtil.exportSector(this.pos, this.name, var1);
         if (this.client != null) {
            this.client.serverMessage("sector " + this.pos + " exporting successful to " + this.name);
         }

         return true;
      } catch (SQLException var4) {
         SQLException var7 = var4;

         try {
            if (this.client != null) {
               this.client.serverMessage("sector exporting failed: " + var7.getClass().getSimpleName() + ": " + var7.getMessage());
            }
         } catch (IOException var3) {
            var3.printStackTrace();
         }

         var4.printStackTrace();
      } catch (IOException var5) {
         IOException var6 = var5;

         try {
            if (this.client != null) {
               this.client.serverMessage("sector exporting failed: " + var6.getClass().getSimpleName() + ": " + var6.getMessage());
            }
         } catch (IOException var2) {
            var2.printStackTrace();
         }

         var5.printStackTrace();
      }

      return false;
   }
}
