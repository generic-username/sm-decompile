package org.schema.game.server.data;

import java.io.IOException;
import java.sql.SQLException;
import org.schema.game.server.controller.SectorUtil;
import org.schema.schine.network.RegisteredClientInterface;

public class SectorBulkRequest implements ServerExecutionJob {
   public RegisteredClientInterface client;
   private String name;
   private boolean export;

   public SectorBulkRequest(RegisteredClientInterface var1, String var2, boolean var3) {
      this.client = var1;
      this.name = var2;
      this.export = var3;
   }

   public boolean execute(GameServerState var1) {
      try {
         SectorUtil.bulk(var1, this.name, this.export);
         if (this.client != null) {
            this.client.serverMessage("bulk " + (this.export ? "export" : "import") + " successful to " + this.name);
         }

         return true;
      } catch (SQLException var4) {
         SQLException var7 = var4;

         try {
            if (this.client != null) {
               this.client.serverMessage("sector exporting failed: " + var7.getClass().getSimpleName() + ": " + var7.getMessage());
            }
         } catch (IOException var3) {
            var3.printStackTrace();
         }

         var4.printStackTrace();
      } catch (IOException var5) {
         IOException var6 = var5;

         try {
            if (this.client != null) {
               this.client.serverMessage("sector exporting failed: " + var6.getClass().getSimpleName() + ": " + var6.getMessage());
            }
         } catch (IOException var2) {
            var2.printStackTrace();
         }

         var5.printStackTrace();
      }

      return false;
   }
}
