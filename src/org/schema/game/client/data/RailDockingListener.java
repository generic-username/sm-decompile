package org.schema.game.client.data;

import org.schema.game.common.controller.SegmentController;

public interface RailDockingListener {
   void dockingChanged(SegmentController var1, boolean var2);
}
