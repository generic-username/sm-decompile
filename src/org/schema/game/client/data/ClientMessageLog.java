package org.schema.game.client.data;

import com.bulletphysics.util.ObjectArrayList;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.util.Iterator;
import java.util.Observable;
import org.newdawn.slick.Color;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.network.objects.Sendable;

public class ClientMessageLog extends Observable {
   public static final Color flashingColor = new Color(0.7F, 0.3F, 8.0F, 1.0F);
   public static final Color tipColor = new Color(0.3F, 0.3F, 0.0F, 1.0F);
   public static final Color infoColor = new Color(0.0F, 0.3F, 0.0F, 1.0F);
   public static final Color gameColor = new Color(0.0F, 0.0F, 0.3F, 1.0F);
   public static final Color errorColor = new Color(0.5F, 0.0F, 0.0F, 1.0F);
   public static final Color chatColor = new Color(0.0F, 0.3F, 0.3F, 1.0F);
   private final ObjectArrayList log = new ObjectArrayList();
   private final ObjectArrayList factionLog = new ObjectArrayList();
   private final Object2ObjectOpenHashMap privateLog = new Object2ObjectOpenHashMap();
   private boolean changed;
   private String currentFilter;

   public void log(ClientMessageLogEntry var1) {
      this.getLog().add(var1);
      this.setChanged(true);
   }

   public void logFaction(ClientMessageLogEntry var1) {
      this.getFactionLog().add(var1);
      this.setChanged(true);
   }

   public void logPrivate(String var1, ClientMessageLogEntry var2) {
      ObjectArrayList var3;
      if ((var3 = (ObjectArrayList)this.getPrivateLog().get(var1)) == null) {
         var3 = new ObjectArrayList();
         this.getPrivateLog().put(var1, var3);
      }

      var3.add(var2);
      this.setChanged(true);
   }

   public boolean isChanged() {
      return this.changed;
   }

   public void setChanged(boolean var1) {
      this.changed = var1;
      this.setChanged();
      this.notifyObservers();
   }

   public String getCurrentChatPrefix() {
      if (this.currentFilter == null) {
         return "";
      } else if (this.currentFilter.equals("*all")) {
         return "";
      } else if (this.currentFilter.equals("*faction")) {
         return "/f ";
      } else if (this.currentFilter.equals("*private")) {
         return "";
      } else {
         return this.currentFilter.equals("*system") ? "" : "/pm " + this.currentFilter + " ";
      }
   }

   public GUIDropDownList makeGUIList(GUIElementList var1, boolean var2, boolean var3) {
      GUITextOverlay var4;
      (var4 = new GUITextOverlay(200, 16, var1.getState())).setTextSimple("*all");
      var4.setUserPointer("*all");
      GUITextOverlay var5;
      (var5 = new GUITextOverlay(200, 16, var1.getState())).setTextSimple("*faction");
      var5.setUserPointer("*faction");
      GUITextOverlay var6;
      (var6 = new GUITextOverlay(200, 16, var1.getState())).setTextSimple("*private");
      var6.setUserPointer("*private");
      GUITextOverlay var7;
      (var7 = new GUITextOverlay(200, 16, var1.getState())).setTextSimple("*system");
      var7.setUserPointer("*system");
      GUITextOverlay[] var8 = new GUITextOverlay[this.getPrivateLog().size()];
      int var9 = 0;

      for(Iterator var10 = this.getPrivateLog().keySet().iterator(); var10.hasNext(); ++var9) {
         String var11 = (String)var10.next();
         var8[var9] = new GUITextOverlay(200, 16, var1.getState());
         var8[var9].setTextSimple("PM " + var11);
         var8[var9].setUserPointer(var11);
      }

      ObjectArrayList var22 = new ObjectArrayList();
      this.add(var22, var4);
      this.add(var22, var5);
      this.add(var22, var6);
      this.add(var22, var7);
      GUITextOverlay[] var23 = var8;
      int var13 = var8.length;

      for(int var15 = 0; var15 < var13; ++var15) {
         var6 = var23[var15];
         this.add(var22, var6);
      }

      Int2ObjectOpenHashMap var24;
      synchronized(var24 = ((GameClientState)var1.getState()).getLocalAndRemoteObjectContainer().getLocalObjects()) {
         Iterator var16 = var24.values().iterator();

         while(true) {
            if (!var16.hasNext()) {
               break;
            }

            Sendable var18;
            PlayerState var20;
            if ((var18 = (Sendable)var16.next()) instanceof PlayerState && (var20 = (PlayerState)var18) != ((GameClientState)var1.getState()).getPlayer() && !this.getPrivateLog().containsKey(var20.getName())) {
               (var6 = new GUITextOverlay(200, 16, var1.getState())).setTextSimple("PM " + var20.getName());
               var6.setUserPointer(var20.getName());
               this.add(var22, var6);
            }
         }
      }

      ClientMessageLog.CB var14 = new ClientMessageLog.CB(var1, var2, var3);
      GUIDropDownList var17 = new GUIDropDownList(var1.getState(), 200, 16, 400, var14, var22);
      var14.first = false;

      for(int var19 = 0; var19 < var17.size(); ++var19) {
         if (var17.get(var19).getUserPointer().equals(this.currentFilter)) {
            GUIListElement var21 = var17.get(var19);
            var17.setSelectedElement(var21);
         }
      }

      return var17;
   }

   private void add(ObjectArrayList var1, GUIElement var2) {
      GUIListElement var3;
      (var3 = new GUIListElement(var2, var2, var2.getState())).setUserPointer(var2.getUserPointer());
      var1.add(var3);
   }

   public ObjectArrayList getLog() {
      return this.log;
   }

   public ObjectArrayList getFactionLog() {
      return this.factionLog;
   }

   public Object2ObjectOpenHashMap getPrivateLog() {
      return this.privateLog;
   }

   class CB implements DropDownCallback {
      boolean first = true;
      private GUIElementList list;
      private boolean dateTag;
      private boolean typeTag;

      public CB(GUIElementList var2, boolean var3, boolean var4) {
         this.list = var2;
         this.dateTag = var3;
         this.typeTag = var4;
      }

      public void onSelectionChanged(GUIListElement var1) {
         this.list.clear();
         if (ClientMessageLog.this.currentFilter == null) {
            ClientMessageLog.this.currentFilter = (String)var1.getUserPointer();
            this.first = false;
         } else if (this.first) {
            this.first = false;
         } else {
            ClientMessageLog.this.currentFilter = (String)var1.getUserPointer();
         }

         int var2;
         ObjectArrayList var3;
         if (ClientMessageLog.this.currentFilter.equals("*all")) {
            for(var2 = (var3 = ClientMessageLog.this.getLog()).size() - 1; var2 >= 0; --var2) {
               this.list.add(((ClientMessageLogEntry)var3.get(var2)).getGUIListEntry((GameClientState)this.list.getState(), this.dateTag, this.typeTag));
            }

         } else if (ClientMessageLog.this.currentFilter.equals("*faction")) {
            for(var2 = (var3 = ClientMessageLog.this.getFactionLog()).size() - 1; var2 >= 0; --var2) {
               this.list.add(((ClientMessageLogEntry)var3.get(var2)).getGUIListEntry((GameClientState)this.list.getState(), this.dateTag, this.typeTag));
            }

         } else {
            int var4;
            if (ClientMessageLog.this.currentFilter.equals("*private")) {
               for(var4 = ClientMessageLog.this.getLog().size() - 1; var4 >= 0; --var4) {
                  if (((ClientMessageLogEntry)ClientMessageLog.this.getLog().get(var4)).getType() == ClientMessageLogType.CHAT_PRIVATE) {
                     this.list.add(((ClientMessageLogEntry)ClientMessageLog.this.getLog().get(var4)).getGUIListEntry((GameClientState)this.list.getState(), this.dateTag, this.typeTag));
                  }
               }

            } else if (ClientMessageLog.this.currentFilter.equals("*system")) {
               for(var4 = ClientMessageLog.this.getLog().size() - 1; var4 >= 0; --var4) {
                  if (((ClientMessageLogEntry)ClientMessageLog.this.getLog().get(var4)).getType() == ClientMessageLogType.ERROR || ((ClientMessageLogEntry)ClientMessageLog.this.getLog().get(var4)).getType() == ClientMessageLogType.INFO || ((ClientMessageLogEntry)ClientMessageLog.this.getLog().get(var4)).getType() == ClientMessageLogType.GAME || ((ClientMessageLogEntry)ClientMessageLog.this.getLog().get(var4)).getType() == ClientMessageLogType.TIP) {
                     this.list.add(((ClientMessageLogEntry)ClientMessageLog.this.getLog().get(var4)).getGUIListEntry((GameClientState)this.list.getState(), this.dateTag, this.typeTag));
                  }
               }

            } else {
               if ((var3 = (ObjectArrayList)ClientMessageLog.this.getPrivateLog().get(ClientMessageLog.this.currentFilter)) != null) {
                  for(var2 = var3.size() - 1; var2 >= 0; --var2) {
                     this.list.add(((ClientMessageLogEntry)var3.get(var2)).getGUIListEntry((GameClientState)this.list.getState(), this.dateTag, this.typeTag));
                  }
               }

            }
         }
      }
   }
}
