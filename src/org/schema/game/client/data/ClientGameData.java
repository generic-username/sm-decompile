package org.schema.game.client.data;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.world.RemoteSector;

public class ClientGameData {
   private final GameClientState state;
   private Vector3i waypoint;
   private Vector3i nearestToWayPoint;
   private Vector3i tmp = new Vector3i();
   private Vector3i ltmp = new Vector3i();

   public ClientGameData(GameClientState var1) {
      this.state = var1;
   }

   public Vector3i getNearestToWayPoint() {
      return this.nearestToWayPoint;
   }

   public Vector3i getWaypoint() {
      return this.waypoint;
   }

   public void setWaypoint(Vector3i var1) {
      System.err.println("SETTING WAYPOINT: " + var1);
      this.waypoint = var1;
      this.nearestToWayPoint = null;
      this.updateNearest(this.state.getCurrentSectorId());
   }

   public void updateNearest(int var1) {
      if (this.getWaypoint() != null) {
         RemoteSector var3;
         if ((var3 = (RemoteSector)this.state.getLocalAndRemoteObjectContainer().getLocalObjects().get(var1)) == null) {
            this.state.getController().flagWaypointUpdate = true;
            return;
         }

         Vector3i var4 = var3.clientPos();
         if (this.nearestToWayPoint == null) {
            this.nearestToWayPoint = new Vector3i();
         }

         if (var4.equals(this.getWaypoint())) {
            this.setWaypoint((Vector3i)null);
            return;
         }

         this.ltmp.set(0, 0, 0);

         for(int var2 = 0; var2 < Element.DIRECTIONSi.length; ++var2) {
            this.tmp.add(var4, Element.DIRECTIONSi[var2]);
            if (this.tmp.equals(this.getWaypoint())) {
               this.nearestToWayPoint.add(var4, Element.DIRECTIONSi[var2]);
               break;
            }

            this.tmp.sub(this.getWaypoint());
            if (this.ltmp.length() != 0.0F && this.tmp.length() >= this.ltmp.length()) {
               System.err.println("NOT TAKING: " + this.tmp.length() + " / " + this.ltmp.length());
            } else {
               this.nearestToWayPoint.add(var4, Element.DIRECTIONSi[var2]);
               this.ltmp.set(this.tmp);
            }
         }

         System.err.println("NEAREST WAYPOINT " + this.nearestToWayPoint);
      }

   }
}
