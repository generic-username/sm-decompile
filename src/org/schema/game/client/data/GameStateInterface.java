package org.schema.game.client.data;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import org.schema.game.common.data.SendableGameState;
import org.schema.game.common.data.chat.ChannelRouter;
import org.schema.game.common.data.element.ControlElementMapOptimizer;
import org.w3c.dom.Document;

public interface GameStateInterface {
   SendableGameState getGameState();

   boolean isPhysicalAsteroids();

   Document getBlockBehaviorConfig();

   float getSectorSize();

   Short2ObjectOpenHashMap getAllTechs();

   boolean getMaterialPrice();

   int getSegmentPieceQueueSize();

   ControlElementMapOptimizer getControlOptimizer();

   ChannelRouter getChannelRouter();

   Long2ObjectOpenHashMap getPlayerStatesByDbId();
}
