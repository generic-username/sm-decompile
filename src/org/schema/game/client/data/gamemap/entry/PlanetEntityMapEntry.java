package org.schema.game.client.data.gamemap.entry;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.data.world.SectorInformation;
import org.schema.schine.graphicsengine.forms.Sprite;

public class PlanetEntityMapEntry extends TransformableEntityMapEntry {
   public SectorInformation.PlanetType planetType;

   protected void decodeEntryImpl(DataInputStream var1) throws IOException {
      super.decodeEntryImpl(var1);
      this.planetType = SectorInformation.PlanetType.values()[var1.readByte()];
   }

   public void encodeEntryImpl(DataOutputStream var1) throws IOException {
      super.encodeEntryImpl(var1);
      var1.writeByte(this.planetType.ordinal());
   }

   public int getSubSprite(Sprite var1) {
      switch(this.planetType) {
      case DESERT:
         return 1;
      case EARTH:
         return 2;
      case ICE:
         return 0;
      case MARS:
         return 5;
      case PURPLE:
         return 3;
      default:
         return super.getSubSprite(var1);
      }
   }
}
