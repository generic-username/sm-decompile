package org.schema.game.client.data.gamemap.entry;

import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.effects.Indication;
import org.schema.schine.graphicsengine.forms.PositionableSubColorSprite;

public interface MapEntryInterface extends SelectableMapEntry, PositionableSubColorSprite {
   void drawPoint(boolean var1, int var2, Vector3i var3);

   void encodeEntryImpl(DataOutputStream var1) throws IOException;

   Indication getIndication(Vector3i var1);

   int getType();

   void setType(byte var1);

   boolean include(int var1, Vector3i var2);
}
