package org.schema.game.client.data.gamemap.entry;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;

public abstract class AbstractMapEntry implements MapEntryInterface {
   // $FF: synthetic field
   static final boolean $assertionsDisabled = !AbstractMapEntry.class.desiredAssertionStatus();

   public static MapEntryInterface[] decode(DataInputStream var0) throws IOException {
      int var1;
      MapEntryInterface[] var2 = new MapEntryInterface[var1 = var0.readInt()];

      for(int var3 = 0; var3 < var1; ++var3) {
         var2[var3] = decodeEntry(var0);
      }

      return var2;
   }

   private static MapEntryInterface decodeEntry(DataInputStream var0) throws IOException {
      byte var2 = var0.readByte();
      SimpleTransformableSendableObject.EntityType var1;
      Object var3;
      if ((var1 = SimpleTransformableSendableObject.EntityType.values()[var2]) != SimpleTransformableSendableObject.EntityType.PLANET_CORE && var1 != SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT) {
         var3 = new TransformableEntityMapEntry();
      } else {
         var3 = new PlanetEntityMapEntry();
      }

      boolean var10000 = $assertionsDisabled;
      ((AbstractMapEntry)var3).setType(var2);
      ((AbstractMapEntry)var3).decodeEntryImpl(var0);
      return (MapEntryInterface)var3;
   }

   public static void encode(DataOutputStream var0, MapEntryInterface[] var1) throws IOException {
      var0.writeInt(var1.length);

      for(int var2 = 0; var2 < var1.length; ++var2) {
         var0.writeByte(var1[var2].getType());
         var1[var2].encodeEntryImpl(var0);
      }

   }

   protected abstract void decodeEntryImpl(DataInputStream var1) throws IOException;
}
