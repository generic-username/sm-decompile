package org.schema.game.client.data.gamemap;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.gamemap.entry.MapEntryInterface;

public abstract class GameMap {
   public static final int STATUS_UNLOADED = 0;
   public static final int STATUS_LOADING = 1;
   public static final int STATUS_LOADED = 2;
   public static final byte TYPE_REGION = 1;
   public static final byte TYPE_SYSTEM = 2;
   public static final byte TYPE_SECTOR = 3;
   public static final byte TYPE_SYSTEM_SIMPLE = 4;
   protected MapEntryInterface parent;
   protected int loadStatus;
   private Vector3i pos;
   private MapEntryInterface[] entries;

   public MapEntryInterface[] getEntries() {
      return this.entries;
   }

   public void setEntries(MapEntryInterface[] var1) {
      this.entries = var1;
   }

   public Vector3i getPos() {
      return this.pos;
   }

   public void setPos(Vector3i var1) {
      this.pos = var1;
   }

   public void update(MapEntryInterface[] var1) {
      this.entries = var1;
   }
}
