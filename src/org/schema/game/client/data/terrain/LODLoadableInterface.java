package org.schema.game.client.data.terrain;

import java.util.Vector;

public interface LODLoadableInterface {
   Vector getGeoMapsToLoad();

   void load();
}
