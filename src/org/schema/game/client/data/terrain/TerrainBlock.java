package org.schema.game.client.data.terrain;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.schine.graphicsengine.forms.Mesh;

public class TerrainBlock extends Mesh {
   private static Vector3f calcVec1 = new Vector3f();
   private static Vector3f calcVec2 = new Vector3f();
   private static Vector3f calcVec3 = new Vector3f();
   private int size;
   private int totalSize;
   private short quadrant;
   private int triangleQuantity;
   private FloatBuffer normalBuffer;
   private FloatBuffer vertexBuffer;
   private IntBuffer indexBuffer;
   private int vertexCount;
   private int triangleCount;
   private TexCoords textureCoords;
   private Vector3f stepScale;
   private Vector2f offset;
   private float offsetAmount;
   private float[] heightMap;
   private float[] oldHeightMap;
   private VBOInfo VBOInfo;

   public TerrainBlock() {
      this.quadrant = 1;
   }

   public TerrainBlock(String var1) {
      this.quadrant = 1;
      this.setName(var1);
   }

   public TerrainBlock(String var1, int var2, Vector3f var3, float[] var4, Vector3f var5) {
      this(var1, var2, var3, var4, var5, var2, new Vector2f(), 0.0F);
   }

   public TerrainBlock(String var1, int var2, Vector3f var3, float[] var4, Vector3f var5, int var6, Vector2f var7, float var8) {
      this(var1);
      this.size = var2;
      this.stepScale = var3;
      this.totalSize = var6;
      this.offsetAmount = var8;
      this.offset = var7;
      this.heightMap = var4;
      this.setPos(var5);
      this.buildVertices();
      this.buildTextureCoordinates();
      this.buildNormals();
      VBOInfo var9 = new VBOInfo(true);
      this.setVBOInfo(var9);
   }

   public void addHeightMapValue(int var1, int var2, float var3) {
      float[] var10000 = this.heightMap;
      int var10001 = var1 + var2 * this.size;
      var10000[var10001] += var3;
   }

   private void buildNormals() {
      this.setNormalBuffer(BufferUtils.createVector3Buffer(this.getNormalBuffer(), this.getVertexCount()));
      Vector3f var1 = new Vector3f();
      Vector3f var2 = new Vector3f();
      Vector3f var3 = new Vector3f();
      Vector3f var4 = new Vector3f();
      boolean var5 = false;
      boolean var6 = false;
      int var7 = 0;

      for(int var8 = 0; var8 < this.size; ++var8) {
         for(int var9 = 0; var9 < this.size; ++var9) {
            BufferUtils.populateFromBuffer(var3, this.getVertexBuffer(), var7);
            int var10;
            int var11;
            if (var8 == this.size - 1) {
               if (var9 == this.size - 1) {
                  var10 = var7 - this.size;
                  var11 = var7 - 1;
               } else {
                  var10 = var7 + 1;
                  var11 = var7 - this.size;
               }
            } else if (var9 == this.size - 1) {
               var10 = var7 - 1;
               var11 = var7 + this.size;
            } else {
               var10 = var7 + this.size;
               var11 = var7 + 1;
            }

            BufferUtils.populateFromBuffer(var2, this.getVertexBuffer(), var10);
            BufferUtils.populateFromBuffer(var1, this.getVertexBuffer(), var11);
            var4.set(var2);
            var4.sub(var3);
            var1.sub(var3);
            var4.cross(var4, var1);
            var4.normalize();
            BufferUtils.setInBuffer(var4, this.getNormalBuffer(), var7);
            ++var7;
         }
      }

   }

   public void buildTextureCoordinates() {
      float var1 = this.offset.x + this.offsetAmount * this.stepScale.x;
      float var2 = this.offset.y + this.offsetAmount * this.stepScale.z;
      this.setTextureCoords(new TexCoords(BufferUtils.createVector2Buffer(this.getVertexCount())));
      FloatBuffer var3;
      (var3 = this.getTextureCoords(0).coords).clear();
      this.getVertexBuffer().rewind();

      for(int var4 = 0; var4 < this.getVertexCount(); ++var4) {
         var3.put((this.getVertexBuffer().get() + var1) / (this.stepScale.x * (float)(this.totalSize - 1)));
         this.getVertexBuffer().get();
         var3.put((this.getVertexBuffer().get() + var2) / (this.stepScale.z * (float)(this.totalSize - 1)));
      }

   }

   private void buildVertices() {
      this.setVertexCount(this.heightMap.length);
      this.setVertexBuffer(BufferUtils.createVector3Buffer(this.getVertexBuffer(), this.getVertexCount()));
      Vector3f var1 = new Vector3f();

      int var2;
      for(var2 = 0; var2 < this.size; ++var2) {
         for(int var3 = 0; var3 < this.size; ++var3) {
            var1.set((float)var2 * this.stepScale.x, this.heightMap[var2 + var3 * this.size] * this.stepScale.y, (float)var3 * this.stepScale.z);
            BufferUtils.setInBuffer(var1, this.getVertexBuffer(), var2 + var3 * this.size);
         }
      }

      this.setTriangleQuantity((this.size - 1) * (this.size - 1) << 1);
      this.setIndexBuffer(BufferUtils.createIntBuffer(this.getTriangleCount() * 3));

      for(var2 = 0; var2 < this.size * (this.size - 1); ++var2) {
         if (var2 % (this.size * (var2 / this.size + 1) - 1) != 0 || var2 == 0) {
            this.getIndexBuffer().put(var2);
            this.getIndexBuffer().put(1 + this.size + var2);
            this.getIndexBuffer().put(var2 + 1);
            this.getIndexBuffer().put(var2);
            this.getIndexBuffer().put(this.size + var2);
            this.getIndexBuffer().put(1 + this.size + var2);
         }
      }

   }

   public float getHeight(float var1, float var2) {
      var1 /= this.stepScale.x;
      var2 /= this.stepScale.z;
      float var3 = FastMath.floor(var1);
      float var4 = FastMath.floor(var2);
      if (var3 >= 0.0F && var4 >= 0.0F && var3 < (float)(this.size - 1) && var4 < (float)(this.size - 1)) {
         var1 -= var3;
         var2 -= var4;
         int var6 = (int)(var3 + var4 * (float)this.size);
         var3 = this.heightMap[var6] * this.stepScale.y;
         var4 = this.heightMap[var6 + 1] * this.stepScale.y;
         float var5 = this.heightMap[var6 + this.size] * this.stepScale.y;
         float var7 = this.heightMap[var6 + this.size + 1] * this.stepScale.y;
         return var1 > var2 ? (1.0F - var1) * var3 + (var1 - var2) * var4 + var2 * var7 : (1.0F - var2) * var3 + (var2 - var1) * var5 + var1 * var7;
      } else {
         return Float.NaN;
      }
   }

   public float getHeight(Vector2f var1) {
      return this.getHeight(var1.x, var1.y);
   }

   public float getHeight(Vector3f var1) {
      return this.getHeight(var1.x, var1.z);
   }

   public float getHeightFromWorld(Vector3f var1) {
      calcVec1.set(var1);
      (var1 = new Vector3f(calcVec1)).sub(this.getPos());
      return this.getHeight(var1.x, var1.z);
   }

   public float[] getHeightMap() {
      return this.heightMap;
   }

   public void setHeightMap(float[] var1) {
      this.heightMap = var1;
   }

   public IntBuffer getIndexBuffer() {
      return this.indexBuffer;
   }

   public void setIndexBuffer(IntBuffer var1) {
      this.indexBuffer = var1;
   }

   public FloatBuffer getNormalBuffer() {
      return this.normalBuffer;
   }

   public void setNormalBuffer(FloatBuffer var1) {
      this.normalBuffer = var1;
   }

   public Vector2f getOffset() {
      return this.offset;
   }

   public void setOffset(Vector2f var1) {
      this.offset = var1;
   }

   public float getOffsetAmount() {
      return this.offsetAmount;
   }

   public void setOffsetAmount(float var1) {
      this.offsetAmount = var1;
   }

   public short getQuadrant() {
      return this.quadrant;
   }

   public void setQuadrant(short var1) {
      this.quadrant = var1;
   }

   public int getSize() {
      return this.size;
   }

   public void setSize(int var1) {
      this.size = var1;
   }

   public Vector3f getStepScale() {
      return this.stepScale;
   }

   public void setStepScale(Vector3f var1) {
      this.stepScale = var1;
   }

   public Vector3f getSurfaceNormal(float var1, float var2, Vector3f var3) {
      var1 /= this.stepScale.x;
      var2 /= this.stepScale.z;
      float var4 = FastMath.floor(var1);
      float var5 = FastMath.floor(var2);
      if (var4 >= 0.0F && var5 >= 0.0F && var4 < (float)(this.size - 1) && var5 < (float)(this.size - 1)) {
         var1 -= var4;
         var2 -= var5;
         if (var3 == null) {
            var3 = new Vector3f();
         }

         Vector3f var6 = calcVec1;
         Vector3f var7 = calcVec2;
         Vector3f var8 = calcVec3;
         int var9 = (int)(var4 + var5 * (float)this.size);
         BufferUtils.populateFromBuffer(var3, this.getNormalBuffer(), var9);
         BufferUtils.populateFromBuffer(var6, this.getNormalBuffer(), var9 + 1);
         BufferUtils.populateFromBuffer(var7, this.getNormalBuffer(), var9 + this.size);
         BufferUtils.populateFromBuffer(var8, this.getNormalBuffer(), var9 + this.size + 1);
         var3.interpolate(var6, var1);
         var7.interpolate(var8, var1);
         var3.interpolate(var7, var2);
         var3.normalize();
         return var3;
      } else {
         return null;
      }
   }

   public Vector3f getSurfaceNormal(Vector2f var1, Vector3f var2) {
      return this.getSurfaceNormal(var1.x, var1.y, var2);
   }

   public Vector3f getSurfaceNormal(Vector3f var1, Vector3f var2) {
      return this.getSurfaceNormal(var1.x, var1.z, var2);
   }

   public TexCoords getTextureCoords(int var1) {
      return this.textureCoords;
   }

   public int getTotalSize() {
      return this.totalSize;
   }

   public void setTotalSize(int var1) {
      this.totalSize = var1;
   }

   public int getTriangleCount() {
      return this.triangleCount;
   }

   public void setTriangleCount(int var1) {
      this.triangleCount = var1;
   }

   public int getTriangleQuantity() {
      return this.triangleQuantity;
   }

   public void setTriangleQuantity(int var1) {
      this.triangleQuantity = var1;
   }

   public VBOInfo getVBOInfo() {
      return this.VBOInfo;
   }

   public void setVBOInfo(VBOInfo var1) {
      this.VBOInfo = var1;
   }

   public FloatBuffer getVertexBuffer() {
      return this.vertexBuffer;
   }

   public void setVertexBuffer(FloatBuffer var1) {
      this.vertexBuffer = var1;
   }

   public int getVertexCount() {
      return this.vertexCount;
   }

   public void setVertexCount(int var1) {
      this.vertexCount = var1;
   }

   public boolean hasChanged() {
      boolean var1 = false;
      if (this.oldHeightMap == null) {
         this.oldHeightMap = new float[this.heightMap.length];
         var1 = true;
      }

      for(int var2 = 0; var2 < this.oldHeightMap.length; ++var2) {
         if (this.oldHeightMap[var2] != this.heightMap[var2] || var1) {
            var1 = true;
            this.oldHeightMap[var2] = this.heightMap[var2];
         }
      }

      return var1;
   }

   public void multHeightMapValue(int var1, int var2, float var3) {
      float[] var10000 = this.heightMap;
      int var10001 = var1 + var2 * this.size;
      var10000[var10001] *= var3;
   }

   public void setDetailTexture(int var1, float var2) {
   }

   public void setHeightMapValue(int var1, int var2, float var3) {
      this.heightMap[var1 + var2 * this.size] = var3;
   }

   public void setTextureCoords(TexCoords var1) {
      this.textureCoords = var1;
   }

   public void updateFromHeightMap() {
      if (this.hasChanged()) {
         Vector3f var1 = new Vector3f();

         for(int var2 = 0; var2 < this.size; ++var2) {
            for(int var3 = 0; var3 < this.size; ++var3) {
               var1.set((float)var2 * this.stepScale.x, this.heightMap[var2 + var3 * this.size] * this.stepScale.y, (float)var3 * this.stepScale.z);
               BufferUtils.setInBuffer(var1, this.getVertexBuffer(), var2 + var3 * this.size);
            }
         }

         this.buildNormals();
         if (this.getVBOInfo() != null) {
            this.getVBOInfo().setVBOVertexID(-1);
            this.getVBOInfo().setVBONormalID(-1);
         }

      }
   }

   public void updateModelBound() {
      throw new NullPointerException("cannot update: not implemented");
   }
}
