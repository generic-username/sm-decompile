package org.schema.game.client.data.terrain;

public class VBOInfo {
   private boolean useVBOVertex;
   private boolean useVBOTexture;
   private boolean useVBOColor;
   private boolean useVBONormal;
   private boolean useVBOFogCoords;
   private boolean useVBOIndex;
   private int vboVertexID;
   private int vboColorID;
   private int vboNormalID;
   private int vboFogCoordsID;
   private int[] vboTextureIDs;
   private int vboIndexID;

   public VBOInfo() {
      this(false);
   }

   public VBOInfo(boolean var1) {
      this.useVBOVertex = false;
      this.useVBOTexture = false;
      this.useVBOColor = false;
      this.useVBONormal = false;
      this.useVBOFogCoords = false;
      this.useVBOIndex = false;
      this.vboVertexID = -1;
      this.vboColorID = -1;
      this.vboNormalID = -1;
      this.vboFogCoordsID = -1;
      this.vboTextureIDs = null;
      this.vboIndexID = -1;
      this.useVBOColor = var1;
      this.useVBOTexture = var1;
      this.useVBOVertex = var1;
      this.useVBONormal = var1;
      this.useVBOFogCoords = var1;
      this.useVBOIndex = false;
      this.vboTextureIDs = new int[2];
   }

   public VBOInfo copy() {
      VBOInfo var1;
      (var1 = new VBOInfo()).useVBOVertex = this.useVBOVertex;
      var1.useVBOTexture = this.useVBOTexture;
      var1.useVBOColor = this.useVBOColor;
      var1.useVBONormal = this.useVBONormal;
      var1.useVBOIndex = this.useVBOIndex;
      var1.useVBOFogCoords = this.useVBOFogCoords;
      return var1;
   }

   public Class getClassTag() {
      return this.getClass();
   }

   public int getVBOColorID() {
      return this.vboColorID;
   }

   public void setVBOColorID(int var1) {
      this.vboColorID = var1;
   }

   public int getVBOFogCoordsID() {
      return this.vboFogCoordsID;
   }

   public void setVBOFogCoordsID(int var1) {
      this.vboFogCoordsID = var1;
   }

   public int getVBOIndexID() {
      return this.vboIndexID;
   }

   public void setVBOIndexID(int var1) {
      this.vboIndexID = var1;
   }

   public int getVBONormalID() {
      return this.vboNormalID;
   }

   public void setVBONormalID(int var1) {
      this.vboNormalID = var1;
   }

   public int getVBOTextureID(int var1) {
      return var1 >= this.vboTextureIDs.length ? -1 : this.vboTextureIDs[var1];
   }

   public int getVBOVertexID() {
      return this.vboVertexID;
   }

   public void setVBOVertexID(int var1) {
      this.vboVertexID = var1;
   }

   public boolean isVBOColorEnabled() {
      return this.useVBOColor;
   }

   public void setVBOColorEnabled(boolean var1) {
      this.useVBOColor = var1;
   }

   public boolean isVBOFogCoordsEnabled() {
      return this.useVBOFogCoords;
   }

   public void setVBOFogCoordsEnabled(boolean var1) {
      this.useVBOFogCoords = var1;
   }

   public boolean isVBOIndexEnabled() {
      return this.useVBOIndex;
   }

   public void setVBOIndexEnabled(boolean var1) {
      this.useVBOIndex = var1;
   }

   public boolean isVBONormalEnabled() {
      return this.useVBONormal;
   }

   public void setVBONormalEnabled(boolean var1) {
      this.useVBONormal = var1;
   }

   public boolean isVBOTextureEnabled() {
      return this.useVBOTexture;
   }

   public void setVBOTextureEnabled(boolean var1) {
      this.useVBOTexture = var1;
   }

   public boolean isVBOVertexEnabled() {
      return this.useVBOVertex;
   }

   public void setVBOVertexEnabled(boolean var1) {
      this.useVBOVertex = var1;
   }

   public void resizeTextureIds(int var1) {
      if (this.vboTextureIDs.length != var1 && var1 != 0) {
         int[] var2 = new int[var1];
         var1 = Math.min(var1, this.vboTextureIDs.length);

         while(true) {
            --var1;
            if (var1 < 0) {
               this.vboTextureIDs = var2;
               return;
            }

            var2[var1] = this.vboTextureIDs[var1];
         }
      }
   }

   public void setVBOTextureID(int var1, int var2) {
      if (var1 >= this.vboTextureIDs.length) {
         this.resizeTextureIds(var1 + 1);
      }

      this.vboTextureIDs[var1] = var2;
   }
}
