package org.schema.game.client.data.terrain;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import javax.vecmath.Vector3f;

public class BufferGeomap extends AbstractGeomap implements Geomap {
   protected FloatBuffer hdata;
   protected ByteBuffer ndata;
   protected int width;
   protected int height;
   protected int maxval;

   public BufferGeomap(FloatBuffer var1, ByteBuffer var2, int var3, int var4, int var5) {
      this.hdata = var1;
      this.ndata = var2;
      this.width = var3;
      this.height = var4;
      this.maxval = var5;
   }

   public BufferGeomap(int var1, int var2, int var3) {
      this(ByteBuffer.allocateDirect(var1 * var2 << 2).asFloatBuffer(), (ByteBuffer)null, var1, var2, var3);
   }

   public Geomap copySubGeomap(int var1, int var2, int var3, int var4) {
      FloatBuffer var5 = ByteBuffer.allocateDirect(var3 * var4 << 2).asFloatBuffer();
      this.hdata.position(var2 * this.width + var1);

      for(int var6 = 0; var6 < this.height; ++var6) {
         this.hdata.limit(this.hdata.position() + var3);
         var5.put(this.hdata);
         this.hdata.limit(this.hdata.capacity());
         this.hdata.position(this.hdata.position() + this.width);
      }

      var5.flip();
      ByteBuffer var7 = null;
      if (this.ndata != null) {
         var7 = ByteBuffer.allocateDirect(var3 * var4 * 3);
         this.ndata.position((var2 * this.width + var1) * 3);

         for(var1 = 0; var1 < this.height; ++var1) {
            this.ndata.limit(this.ndata.position() + var3 * 3);
            var7.put(this.ndata);
            this.ndata.limit(this.ndata.capacity());
            this.ndata.position(this.ndata.position() + this.width * 3);
         }

         var7.flip();
      }

      return new BufferGeomap(var5, var7, var3, var4, this.maxval);
   }

   public int getHeight() {
      return this.height;
   }

   public int getMaximumValue() {
      return this.maxval;
   }

   public Vector3f getNormal(int var1, int var2, Vector3f var3) {
      return this.getNormal(var2 * this.width + var1, var3);
   }

   public Vector3f getNormal(int var1, Vector3f var2) {
      this.ndata.position(var1 * 3);
      if (var2 == null) {
         var2 = new Vector3f();
      }

      var2.x = ((float)(this.ndata.get() & 255) / 255.0F - 0.5F) * 2.0F;
      var2.y = ((float)(this.ndata.get() & 255) / 255.0F - 0.5F) * 2.0F;
      var2.z = ((float)(this.ndata.get() & 255) / 255.0F - 0.5F) * 2.0F;
      return var2;
   }

   public SharedGeomap getSubGeomap(int var1, int var2, int var3, int var4) {
      if (var3 + var1 > this.width) {
         var3 = this.width - var1;
      }

      if (var4 + var2 > this.height) {
         var4 = this.height - var2;
      }

      return new SharedBufferGeomap(this, var1, var2, var3, var4);
   }

   public int getValue(int var1) {
      return (int)this.hdata.get(var1);
   }

   public int getValue(int var1, int var2) {
      return (int)this.hdata.get(var2 * this.width + var1);
   }

   public int getWidth() {
      return this.width;
   }

   public boolean hasNormalmap() {
      return this.ndata != null;
   }

   public boolean isLoaded() {
      return true;
   }

   public FloatBuffer getHeightData() {
      return !this.isLoaded() ? null : this.hdata;
   }

   public ByteBuffer getNormalData() {
      return this.isLoaded() && this.hasNormalmap() ? this.ndata : null;
   }

   public FloatBuffer writeVertexArray(FloatBuffer var1, Vector3f var2, boolean var3) {
      if (!this.isLoaded()) {
         throw new NullPointerException();
      } else {
         if (var1 != null) {
            if (var1.remaining() < this.width * this.height * 3) {
               throw new BufferUnderflowException();
            }
         } else {
            var1 = BufferUtils.createFloatBuffer(this.width * this.height * 3);
         }

         this.hdata.rewind();

         assert this.hdata.limit() == this.height * this.width;

         Vector3f var4 = new Vector3f((float)(-this.getWidth()) * var2.x * 0.5F, 0.0F, (float)(-this.getWidth()) * var2.z * 0.5F);
         if (!var3) {
            var4 = new Vector3f();
         }

         for(int var6 = 0; var6 < this.height; ++var6) {
            for(int var5 = 0; var5 < this.width; ++var5) {
               var1.put((float)var5 * var2.x + var4.x);
               var1.put(this.hdata.get() * var2.y);
               var1.put((float)var6 * var2.z + var4.z);
            }
         }

         return var1;
      }
   }
}
