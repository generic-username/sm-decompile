package org.schema.game.client.data.terrain;

import java.io.IOException;
import java.util.Vector;
import javax.vecmath.Vector3f;
import org.lwjgl.opengl.GL11;
import org.schema.common.util.data.DataUtil;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.terrain.geimipmap.LODGeomap;
import org.schema.game.client.data.terrain.geimipmap.TerrainQuad;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.shader.HoffmannSky;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.texture.Texture;

public class Terrain extends AbstractSceneNode implements LODLoadableInterface {
   public static int currently_drawn;
   private static Terrain.TerrainRegion[] regions;
   private static Terrain.TerrainShaderable terrainShaderable;
   private int specularMapPointer;
   private int normalMapPointer;
   private int heightMapPointer;
   private Vector geoMapsToLoad;
   private float tiling_factor;
   private TerrainQuad[] root;
   private boolean firstDraw;
   private float[] heightMap;
   private int blocksize;
   private Vector3f stepScale;
   private int size;
   private Vector locations;
   private GameClientState state;

   public Terrain(GameClientState var1) {
      this.tiling_factor = 20.0F;
      this.firstDraw = true;
      this.state = var1;
      this.geoMapsToLoad = new Vector();
   }

   public Terrain(GameClientState var1, int var2, int var3, int var4, int[] var5, int var6, int var7, Vector3f var8) {
      this(var1);
      this.heightMapPointer = var2;
      this.blocksize = var7;
      this.stepScale = var8;
      this.size = var6;
      this.locations = new Vector();
      this.locations.add(new Vector3f(Controller.getCamera().getPos()));
      float[] var9 = new float[var6 * var6];

      for(var2 = 0; var2 < var6; ++var2) {
         for(var3 = 0; var3 < var6; ++var3) {
            var4 = var2 * (var6 - 1) % var5.length;
            var7 = var3 % (var6 - 1);
            var9[var2 * var6 + var3] = (float)var5[var4 + var7];
         }
      }

      this.heightMap = var9;
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.isFirstDraw()) {
         this.onInit();
      }

      currently_drawn = 0;
      Vector3f var1 = new Vector3f(Controller.getCamera().getPos());
      this.locations.set(0, var1);
      int var2 = Math.round(var1.x / (float)((this.size - 1) * 100));
      int var7 = Math.round(var1.z / (float)((this.size - 1) * 100));
      Vector3f var3 = new Vector3f(this.getPos());
      int var4 = 0;
      ShaderLibrary.hoffmanTerrainShader.setShaderInterface(terrainShaderable);
      ShaderLibrary.hoffmanTerrainShader.load();
      GlUtil.glEnable(2884);
      GL11.glCullFace(1029);

      for(int var5 = -1; var5 <= 1; ++var5) {
         for(int var6 = -1; var6 <= 1; ++var6) {
            this.root[var4].setPos((float)((var5 + var2) * (this.size - 1) * 100), var3.y, (float)((var6 + var7) * (this.size - 1) * 100));
            this.root[var4].update(this.locations);
            this.root[var4].draw();
            ++var4;
         }
      }

      GlUtil.glDisable(2884);
      ShaderLibrary.hoffmanTerrainShader.unload();
      AbstractScene.infoList.add("currently drawn terrain patches: " + currently_drawn + ", surfPos: " + var2 + ", " + var7 + ";");
   }

   public void onInit() {
      if (terrainShaderable == null) {
         terrainShaderable = new Terrain.TerrainShaderable();
      }

      if (regions == null) {
         this.makeRegions();
      }

      this.normalMapPointer = Controller.getResLoader().getSprite("normalmap2").getMaterial().getTexture().getTextureId();
      this.specularMapPointer = Controller.getResLoader().getSprite("specmap2").getMaterial().getTexture().getTextureId();
      this.root = new TerrainQuad[9];

      for(int var1 = 0; var1 < 9; ++var1) {
         this.root[var1] = new TerrainQuad(this.getName(), this.blocksize, this.size, this.stepScale, this.heightMap, this);
      }

      this.setFirstDraw(false);
   }

   public Terrain clone() {
      Terrain var1;
      (var1 = new Terrain(this.state)).specularMapPointer = this.specularMapPointer;
      var1.normalMapPointer = this.normalMapPointer;
      var1.heightMapPointer = this.heightMapPointer;
      var1.root = (TerrainQuad[])this.root.clone();
      var1.firstDraw = this.firstDraw;
      var1.heightMap = this.heightMap;
      var1.blocksize = this.blocksize;
      var1.stepScale = this.stepScale;
      var1.size = this.size;
      var1.locations = new Vector(this.locations);
      return var1;
   }

   public Vector getGeoMapsToLoad() {
      return this.geoMapsToLoad;
   }

   public void load() {
      if (this.isFirstDraw()) {
         this.onInit();
      }

      if (this.geoMapsToLoad.size() > 0) {
         ((LODGeomap)this.geoMapsToLoad.get(0)).doLoadStep();
         if (((LODGeomap)this.geoMapsToLoad.get(0)).isMeshLoaded()) {
            this.geoMapsToLoad.remove(0);
            System.err.println("LOADING MAPS: " + this.geoMapsToLoad.size());
         }

      } else {
         this.setLoaded(true);
      }
   }

   public void setGeoMapsToLoad(Vector var1) {
      this.geoMapsToLoad = var1;
   }

   public boolean isFirstDraw() {
      return this.firstDraw;
   }

   public void setFirstDraw(boolean var1) {
      this.firstDraw = var1;
   }

   private void makeRegions() {
      if (regions == null) {
         regions = new Terrain.TerrainRegion[4];

         try {
            regions[0] = new Terrain.TerrainRegion(Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "/heightmaps/dirt.png", true), 0.0F, 80.0F);
            regions[1] = new Terrain.TerrainRegion(Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "/heightmaps/grass.png", true), 80.0F, 130.0F);
            regions[2] = new Terrain.TerrainRegion(Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "/heightmaps/rock.png", true), 130.0F, 180.0F);
            regions[3] = new Terrain.TerrainRegion(Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "/heightmaps/snow.png", true), 180.0F, 255.0F);
            return;
         } catch (IOException var1) {
            var1.printStackTrace();
         }
      }

   }

   public class TerrainShaderable extends HoffmannSky {
      public void onExit() {
         if (!Terrain.this.isFirstDraw()) {
            Terrain.regions[0].getTexture().unbindFromIndex();
            Terrain.regions[1].getTexture().unbindFromIndex();
            Terrain.regions[2].getTexture().unbindFromIndex();
            Terrain.regions[3].getTexture().unbindFromIndex();
            GlUtil.glActiveTexture(33988);
            GlUtil.glDisable(3553);
            GlUtil.glBindTexture(3553, 0);
            GlUtil.glActiveTexture(33989);
            GlUtil.glDisable(3553);
            GlUtil.glBindTexture(3553, 0);
            GlUtil.glActiveTexture(33990);
            GlUtil.glDisable(3553);
            GlUtil.glBindTexture(3553, 0);
            GlUtil.glActiveTexture(33984);
         }
      }

      public void updateShaderParameters(Shader var1) {
         if (!Terrain.this.isFirstDraw()) {
            super.updateShaderParameters(var1);
            GlUtil.updateShaderFloat(var1, "terrainTilingFactor", Terrain.this.tiling_factor);
            GlUtil.updateShaderFloat(var1, "heightMapScale", 1.0F);
            GlUtil.updateShaderFloat(var1, "dirtRegion.max", Terrain.regions[0].getMax());
            GlUtil.updateShaderFloat(var1, "dirtRegion.min", Terrain.regions[0].getMin());
            GlUtil.updateShaderFloat(var1, "grassRegion.max", Terrain.regions[1].getMax());
            GlUtil.updateShaderFloat(var1, "grassRegion.min", Terrain.regions[1].getMin());
            GlUtil.updateShaderFloat(var1, "rockRegion.max", Terrain.regions[2].getMax());
            GlUtil.updateShaderFloat(var1, "rockRegion.min", Terrain.regions[2].getMin());
            GlUtil.updateShaderFloat(var1, "snowRegion.max", Terrain.regions[3].getMax());
            GlUtil.updateShaderFloat(var1, "snowRegion.min", Terrain.regions[3].getMin());
            GlUtil.updateShaderVector4f(var1, "light.ambient", AbstractScene.mainLight.getAmbience());
            GlUtil.updateShaderVector4f(var1, "light.diffuse", AbstractScene.mainLight.getDiffuse());
            GlUtil.updateShaderVector4f(var1, "light.specular", AbstractScene.mainLight.getSpecular());
            GlUtil.updateShaderFloat(var1, "shininess", AbstractScene.mainLight.getShininess()[0]);
            Terrain.regions[0].getTexture().bindOnShader(33985, 1, "dirtColorMap", var1);
            Terrain.regions[1].getTexture().bindOnShader(33986, 2, "grassColorMap", var1);
            Terrain.regions[2].getTexture().bindOnShader(33987, 3, "rockColorMap", var1);
            Terrain.regions[3].getTexture().bindOnShader(33988, 4, "snowColorMap", var1);
            GlUtil.glActiveTexture(33989);
            GlUtil.glBindTexture(3553, Terrain.this.heightMapPointer);
            GlUtil.updateShaderInt(var1, "heightMap", 5);
            GlUtil.glActiveTexture(33990);
            GlUtil.glBindTexture(3553, Terrain.this.normalMapPointer);
            GL11.glTexParameteri(3553, 10242, 10497);
            GL11.glTexParameteri(3553, 10243, 10497);
            GlUtil.updateShaderInt(var1, "heightMap", 6);
            GlUtil.glActiveTexture(33991);
            GlUtil.glBindTexture(3553, Terrain.this.specularMapPointer);
            GL11.glTexParameteri(3553, 10242, 10497);
            GL11.glTexParameteri(3553, 10243, 10497);
            GlUtil.updateShaderInt(var1, "specularMap", 7);
            GlUtil.glActiveTexture(33992);
         }
      }
   }

   class TerrainRegion {
      private static final int DIRT = 0;
      private static final int GRASS = 1;
      private static final int ROCK = 2;
      private static final int SNOW = 3;
      private Texture texture;
      private float min;
      private float max;

      public TerrainRegion(Texture var2, float var3, float var4) {
         this.texture = var2;
         this.min = var3;
         this.max = var4;
      }

      public float getMax() {
         return this.max;
      }

      public float getMin() {
         return this.min;
      }

      public Texture getTexture() {
         return this.texture;
      }
   }
}
