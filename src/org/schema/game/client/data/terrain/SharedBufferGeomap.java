package org.schema.game.client.data.terrain;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;

public class SharedBufferGeomap extends AbstractGeomap implements SharedGeomap {
   protected final BufferGeomap parent;
   protected final FloatBuffer hdata;
   protected final ByteBuffer ndata;
   protected final int startX;
   protected final int startY;
   protected final int width;
   protected final int height;

   public SharedBufferGeomap(BufferGeomap var1, int var2, int var3, int var4, int var5) {
      this.parent = var1;
      this.hdata = var1.getHeightData();
      this.ndata = var1.getNormalData();
      this.startX = var2;
      this.startY = var3;
      this.width = var4;
      this.height = var5;
   }

   public Geomap copy() {
      return this.parent.copySubGeomap(this.startX, this.startY, this.width, this.height);
   }

   public Geomap getParent() {
      return this.parent;
   }

   public int getXOffset() {
      return this.startX;
   }

   public int getYOffset() {
      return this.startY;
   }

   public Geomap copySubGeomap(int var1, int var2, int var3, int var4) {
      if (var1 >= 0 && var2 >= 0 && var1 <= this.width && var2 <= this.height && var3 <= this.width && var4 <= this.height) {
         return this.parent.copySubGeomap(this.startX + var1, this.startY + var2, var3, var4);
      } else {
         throw new IndexOutOfBoundsException();
      }
   }

   public int getHeight() {
      return this.height;
   }

   public int getMaximumValue() {
      return this.parent.getMaximumValue();
   }

   public Vector3f getNormal(int var1, int var2, Vector3f var3) {
      return this.parent.getNormal(this.startX + var1, this.startY + var2, var3);
   }

   public Vector3f getNormal(int var1, Vector3f var2) {
      int var3 = var1 % this.width;
      return this.getNormal(var3, (var1 - var3) / this.width, var2);
   }

   public SharedGeomap getSubGeomap(int var1, int var2, int var3, int var4) {
      if (var1 >= 0 && var2 >= 0 && var1 <= this.width && var2 <= this.height && var3 + var1 <= this.width && var4 + var2 <= this.height) {
         return this.parent.getSubGeomap(this.startX + var1, this.startY + var2, var3, var4);
      } else {
         throw new IndexOutOfBoundsException();
      }
   }

   public int getValue(int var1) {
      int var2 = var1 % this.width;
      return this.getValue(var2, (var1 - var2) / this.width);
   }

   public int getValue(int var1, int var2) {
      return this.parent.getValue(this.startX + var1, this.startY + var2);
   }

   public int getWidth() {
      return this.width;
   }

   public boolean hasNormalmap() {
      return this.parent.hasNormalmap();
   }

   public boolean isLoaded() {
      return this.parent.isLoaded();
   }

   public Vector2f getUV(int var1, int var2, Vector2f var3) {
      return this.parent.getUV(this.startX + var1, this.startY + var2, var3);
   }

   public Vector2f getUV(int var1, Vector2f var2) {
      int var3 = var1 % this.width;
      return this.getUV(var3, (var1 - var3) / this.width, var2);
   }
}
