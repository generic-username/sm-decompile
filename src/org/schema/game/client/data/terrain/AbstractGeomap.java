package org.schema.game.client.data.terrain;

import java.nio.BufferUnderflowException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.forms.Mesh;

public abstract class AbstractGeomap implements Geomap {
   public Mesh createMesh(Vector3f var1, Vector2f var2, boolean var3) {
      return new Mesh();
   }

   public Vector2f getUV(int var1, int var2, Vector2f var3) {
      var3.set((float)var1 / (float)this.getWidth(), (float)var2 / (float)this.getHeight());
      return var3;
   }

   public Vector2f getUV(int var1, Vector2f var2) {
      return var2;
   }

   public IntBuffer writeIndexArray(IntBuffer var1) {
      int var2 = (this.getWidth() - 1) * (this.getHeight() - 1) << 1;
      if (var1 != null) {
         if (var1.remaining() < var2 * 3) {
            throw new BufferUnderflowException();
         }
      } else {
         var1 = BufferUtils.createIntBuffer(var2 * 3);
      }

      var2 = 0;

      for(int var3 = 0; var3 < this.getHeight() - 1; ++var3) {
         for(int var4 = 0; var4 < this.getWidth() - 1; ++var4) {
            var1.put(var2).put(var2 + this.getWidth()).put(var2 + this.getWidth() + 1);
            var1.put(var2 + this.getWidth() + 1).put(var2 + 1).put(var2);
            ++var2;
            if (var4 == this.getWidth() - 2) {
               ++var2;
            }
         }
      }

      var1.flip();
      return var1;
   }

   public FloatBuffer writeNormalArray(FloatBuffer var1, Vector3f var2) {
      if (!this.isLoaded()) {
         throw new NullPointerException();
      } else {
         if (var1 != null) {
            if (var1.remaining() < this.getWidth() * this.getHeight() * 3) {
               throw new BufferUnderflowException();
            }
         } else {
            var1 = BufferUtils.createFloatBuffer(this.getWidth() * this.getHeight() * 3);
         }

         var1.rewind();
         Vector3f var3;
         if (!this.hasNormalmap()) {
            var3 = new Vector3f();
            Vector3f var4 = new Vector3f();
            Vector3f var5 = new Vector3f();
            Vector3f var6 = new Vector3f();
            int var7 = 0;

            for(int var8 = 0; var8 < this.getHeight(); ++var8) {
               for(int var9 = 0; var9 < this.getWidth(); ++var9) {
                  var5.set((float)var9, (float)this.getValue(var9, var8), (float)var8);
                  if (var8 == this.getHeight() - 1) {
                     if (var9 == this.getWidth() - 1) {
                        var4.set((float)var9, (float)this.getValue(var9, var8 - 1), (float)(var8 - 1));
                        var3.set((float)(var9 - 1), (float)this.getValue(var9 - 1, var8), (float)var8);
                     } else {
                        var4.set((float)(var9 + 1), (float)this.getValue(var9 + 1, var8), (float)var8);
                        var3.set((float)var9, (float)this.getValue(var9, var8 - 1), (float)(var8 - 1));
                     }
                  } else if (var9 == this.getWidth() - 1) {
                     var4.set((float)(var9 - 1), (float)this.getValue(var9 - 1, var8), (float)var8);
                     var3.set((float)var9, (float)this.getValue(var9, var8 + 1), (float)(var8 + 1));
                  } else {
                     var4.set((float)var9, (float)this.getValue(var9, var8 + 1), (float)(var8 + 1));
                     var3.set((float)(var9 + 1), (float)this.getValue(var9 + 1, var8), (float)var8);
                  }

                  var6.set(var4);
                  var6.sub(var5);
                  var3.sub(var5);
                  var6.cross(var6, var3);
                  var6.x *= var2.x;
                  var6.y *= var2.y;
                  var6.z *= var2.z;
                  var6.normalize();
                  BufferUtils.setInBuffer(var6, var1, var7);
                  ++var7;
               }
            }
         } else {
            var3 = new Vector3f();

            for(int var10 = 0; var10 < this.getHeight(); ++var10) {
               for(int var11 = 0; var11 < this.getWidth(); ++var11) {
                  this.getNormal(var11, var10, var3);
                  var1.put(var3.x).put(var3.y).put(var3.z);
               }
            }
         }

         return var1;
      }
   }

   public FloatBuffer writeTexCoordArray(FloatBuffer var1, Vector2f var2, Vector2f var3) {
      if (var1 != null) {
         if (var1.remaining() < this.getWidth() * this.getHeight() << 1) {
            throw new BufferUnderflowException();
         }
      } else {
         var1 = BufferUtils.createFloatBuffer(this.getWidth() * this.getHeight() << 1);
      }

      if (var2 == null) {
         var2 = new Vector2f();
      }

      Vector2f var4 = new Vector2f();

      for(int var5 = 0; var5 < this.getHeight(); ++var5) {
         for(int var6 = 0; var6 < this.getWidth(); ++var6) {
            this.getUV(var6, var5, var4);
            var1.put(var2.x + var4.x * var3.x);
            var1.put(var2.y + var4.y * var3.y);
         }
      }

      return var1;
   }

   public FloatBuffer writeVertexArray(FloatBuffer var1, Vector3f var2, boolean var3) {
      if (!this.isLoaded()) {
         throw new NullPointerException();
      } else {
         if (var1 != null) {
            if (var1.remaining() < this.getWidth() * this.getHeight() * 3) {
               throw new BufferUnderflowException();
            }
         } else {
            var1 = BufferUtils.createFloatBuffer(this.getWidth() * this.getHeight() * 3);
         }

         Vector3f var4 = new Vector3f((float)(-this.getWidth()) * var2.x, 0.0F, (float)(-this.getWidth()) * var2.z);
         if (!var3) {
            var4 = new Vector3f();
         }

         for(int var6 = 0; var6 < this.getHeight(); ++var6) {
            for(int var5 = 0; var5 < this.getWidth(); ++var5) {
               var1.put((float)var5 * var2.x + var4.x);
               var1.put((float)this.getValue(var5, var6) * var2.y);
               var1.put((float)var6 * var2.z + var4.y);
            }
         }

         return var1;
      }
   }

   public IntBuffer writeIndexArray2(IntBuffer var1) {
      int var2 = (this.getWidth() - 1) * (this.getHeight() - 1) << 1;
      if (var1 != null) {
         if (var1.remaining() < var2 * 3) {
            throw new BufferUnderflowException();
         }
      } else {
         var1 = BufferUtils.createIntBuffer(var2 * 3);
      }

      int var3;
      for(var2 = 0; var2 < this.getHeight() - 1; ++var2) {
         for(var3 = 0; var3 < this.getWidth() - 1; ++var3) {
            int var4 = var3 + var2 * this.getWidth();
            var1.put(var4).put(var4 + this.getWidth()).put(var4 + this.getWidth() + 1);
            var1.put(var4 + this.getWidth() + 1).put(var4 + 1).put(var4);
         }
      }

      var1.flip();

      for(var2 = 0; var2 < var1.limit(); ++var2) {
         if ((var3 = var1.get()) < this.getWidth() - 3 && var3 % 32 != 0) {
            var1.position(var1.position() - 1);
            var1.put(var3 - var3 % 32);
         }
      }

      return var1;
   }
}
