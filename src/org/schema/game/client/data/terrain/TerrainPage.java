package org.schema.game.client.data.terrain;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.graphicsengine.forms.SceneNode;

public class TerrainPage extends SceneNode {
   private static Vector3f calcVec1 = new Vector3f();
   private Vector2f offset;
   private int totalSize;
   private int size;
   private Vector3f stepScale;
   private float offsetAmount;
   private short quadrant;

   public TerrainPage() {
      this.quadrant = 1;
   }

   public TerrainPage(String var1) {
      this.quadrant = 1;
      this.setName(var1);
   }

   public TerrainPage(String var1, int var2, int var3, Vector3f var4, float[] var5) {
      this(var1, var2, var3, var4, var5, var3, new Vector2f(), 0.0F);
      this.fixNormals();
   }

   protected TerrainPage(String var1, int var2, int var3, Vector3f var4, float[] var5, int var6, Vector2f var7, float var8) {
      this(var1);
      if (!FastMath.isPowerOfTwo(var3 - 1)) {
         throw new RuntimeException("size given: " + var3 + "  OldTerrain page sizes may only be (2^N + 1)");
      } else {
         this.offset = var7;
         this.offsetAmount = var8;
         this.totalSize = var6;
         this.size = var3;
         this.stepScale = var4;
         this.split(var2, var5);
      }
   }

   public static final float[] createHeightSubBlock(float[] var0, int var1, int var2, int var3) {
      float[] var4 = new float[var3 * var3];
      int var5 = (int)FastMath.sqrt((float)var0.length);
      int var6 = 0;

      for(int var7 = var2; var7 < var3 + var2; ++var7) {
         for(int var8 = var1; var8 < var3 + var1; ++var8) {
            if (var8 < var5 && var7 < var5) {
               var4[var6] = var0[var8 + var7 * var5];
            }

            ++var6;
         }
      }

      return var4;
   }

   private TerrainBlock _findDownBlock(TerrainBlock var1) {
      if (var1.getQuadrant() == 1) {
         return this.getBlock(2);
      } else if (var1.getQuadrant() == 3) {
         return this.getBlock(4);
      } else {
         TerrainPage var2;
         if (var1.getQuadrant() == 2) {
            if ((var2 = this._findDownPage()) != null) {
               return var2.getBlock(1);
            }
         } else if (var1.getQuadrant() == 4 && (var2 = this._findDownPage()) != null) {
            return var2.getBlock(3);
         }

         return null;
      }
   }

   private TerrainPage _findDownPage() {
      if (this.getParent() != null && this.getParent() instanceof TerrainPage) {
         TerrainPage var1 = (TerrainPage)this.getParent();
         if (this.quadrant == 1) {
            return var1.getPage(2);
         } else if (this.quadrant == 3) {
            return var1.getPage(4);
         } else {
            if (this.quadrant == 2) {
               if ((var1 = var1._findDownPage()) != null) {
                  return var1.getPage(1);
               }
            } else if (this.quadrant == 4 && (var1 = var1._findDownPage()) != null) {
               return var1.getPage(3);
            }

            return null;
         }
      } else {
         return null;
      }
   }

   private TerrainBlock _findRightBlock(TerrainBlock var1) {
      if (var1.getQuadrant() == 1) {
         return this.getBlock(3);
      } else if (var1.getQuadrant() == 2) {
         return this.getBlock(4);
      } else {
         TerrainPage var2;
         if (var1.getQuadrant() == 3) {
            if ((var2 = this._findRightPage()) != null) {
               return var2.getBlock(1);
            }
         } else if (var1.getQuadrant() == 4 && (var2 = this._findRightPage()) != null) {
            return var2.getBlock(2);
         }

         return null;
      }
   }

   private TerrainPage _findRightPage() {
      if (this.getParent() != null && this.getParent() instanceof TerrainPage) {
         TerrainPage var1 = (TerrainPage)this.getParent();
         if (this.quadrant == 1) {
            return var1.getPage(3);
         } else if (this.quadrant == 2) {
            return var1.getPage(4);
         } else {
            if (this.quadrant == 3) {
               if ((var1 = var1._findRightPage()) != null) {
                  return var1.getPage(1);
               }
            } else if (this.quadrant == 4 && (var1 = var1._findRightPage()) != null) {
               return var1.getPage(2);
            }

            return null;
         }
      } else {
         return null;
      }
   }

   public void addHeightMapValue(int var1, int var2, float var3) {
      int var4 = this.findQuadrant(var1, var2);
      int var5 = this.size + 1 >> 1;
      if (this.getChilds() != null) {
         int var6 = this.getChilds().size();

         while(true) {
            --var6;
            if (var6 < 0) {
               break;
            }

            AbstractSceneNode var7 = (AbstractSceneNode)this.getChilds().get(var6);
            int var8 = var1;
            int var9 = var2;
            boolean var10 = false;
            short var11 = 0;
            if (var7 instanceof TerrainPage) {
               var11 = ((TerrainPage)var7).getQuadrant();
            } else if (var7 instanceof TerrainBlock) {
               var11 = ((TerrainBlock)var7).getQuadrant();
            }

            if (var11 == 1 && (var4 & 1) != 0) {
               var10 = true;
            } else if (var11 == 2 && (var4 & 2) != 0) {
               var9 = var2 - var5 + 1;
               var10 = true;
            } else if (var11 == 3 && (var4 & 4) != 0) {
               var8 = var1 - var5 + 1;
               var10 = true;
            } else if (var11 == 4 && (var4 & 8) != 0) {
               var8 = var1 - var5 + 1;
               var9 = var2 - var5 + 1;
               var10 = true;
            }

            if (var10) {
               if (var7 instanceof TerrainPage) {
                  ((TerrainPage)var7).addHeightMapValue(var8, var9, var3);
               } else if (var7 instanceof TerrainBlock) {
                  ((TerrainBlock)var7).addHeightMapValue(var8, var9, var3);
               }
            }
         }
      }

   }

   private void createQuadBlock(float[] var1) {
      int var2 = this.size >> 2;
      int var3 = this.size >> 1;
      int var4 = this.size + 1 >> 1;
      Vector2f var5 = new Vector2f();
      this.offsetAmount += (float)var2;
      float[] var6 = createHeightSubBlock(var1, 0, 0, var4);
      Vector3f var7 = new Vector3f((float)(-var3) * this.stepScale.x, 0.0F, (float)(-var3) * this.stepScale.z);
      var5.x = this.offset.x;
      var5.y = this.offset.y;
      var5.x += var7.x / 2.0F;
      var5.y += var7.z / 2.0F;
      TerrainBlock var12;
      (var12 = new TerrainBlock(this.getName() + "Block1", var4, this.stepScale, var6, var7, this.totalSize, var5, this.offsetAmount)).setQuadrant((short)1);
      this.attach(var12);
      var12.setBoundingBox(new BoundingBox());
      var12.updateModelBound();
      var6 = createHeightSubBlock(var1, 0, var4 - 1, var4);
      Vector3f var8 = new Vector3f((float)(-var3) * this.stepScale.x, 0.0F, 0.0F);
      var5.x = this.offset.x;
      var5.y = this.offset.y;
      var5.x += var7.x / 2.0F;
      var5.y += (float)var2 * this.stepScale.z;
      (var12 = new TerrainBlock(this.getName() + "Block2", var4, this.stepScale, var6, var8, this.totalSize, var5, this.offsetAmount)).setQuadrant((short)2);
      this.attach(var12);
      var12.setBoundingBox(new BoundingBox());
      var12.updateModelBound();
      var6 = createHeightSubBlock(var1, var4 - 1, 0, var4);
      Vector3f var10 = new Vector3f(0.0F, 0.0F, (float)(-var3) * this.stepScale.z);
      var5.x = this.offset.x;
      var5.y = this.offset.y;
      var5.x += (float)var2 * this.stepScale.x;
      var5.y += var10.z / 2.0F;
      TerrainBlock var11;
      (var11 = new TerrainBlock(this.getName() + "Block3", var4, this.stepScale, var6, var10, this.totalSize, var5, this.offsetAmount)).setQuadrant((short)3);
      this.attach(var11);
      var11.setBoundingBox(new BoundingBox());
      var11.updateModelBound();
      var1 = createHeightSubBlock(var1, var4 - 1, var4 - 1, var4);
      var10 = new Vector3f(0.0F, 0.0F, 0.0F);
      var5.x = this.offset.x;
      var5.y = this.offset.y;
      var5.x += (float)var2 * this.stepScale.x;
      var5.y += (float)var2 * this.stepScale.z;
      TerrainBlock var9;
      (var9 = new TerrainBlock(this.getName() + "Block4", var4, this.stepScale, var1, var10, this.totalSize, var5, this.offsetAmount)).setQuadrant((short)4);
      this.attach(var9);
      var9.setBoundingBox(new BoundingBox());
      var9.updateModelBound();
   }

   private void createQuadPage(int var1, float[] var2) {
      int var3 = this.size >> 2;
      int var4 = this.size + 1 >> 1;
      Vector2f var5 = new Vector2f();
      this.offsetAmount += (float)var3;
      float[] var6 = createHeightSubBlock(var2, 0, 0, var4);
      Vector3f var7 = new Vector3f((float)(-var3) * this.stepScale.x, 0.0F, (float)(-var3) * this.stepScale.z);
      var5.x = this.offset.x;
      var5.y = this.offset.y;
      var5.x += var7.x;
      var5.y += var7.z;
      TerrainPage var10;
      (var10 = new TerrainPage(this.getName() + "Page1", var1, var4, this.stepScale, var6, this.totalSize, var5, this.offsetAmount)).setPos(var7);
      var10.quadrant = 1;
      this.attach(var10);
      var6 = createHeightSubBlock(var2, 0, var4 - 1, var4);
      var7 = new Vector3f((float)(-var3) * this.stepScale.x, 0.0F, (float)var3 * this.stepScale.z);
      var5.x = this.offset.x;
      var5.y = this.offset.y;
      var5.x += var7.x;
      var5.y += var7.z;
      (var10 = new TerrainPage(this.getName() + "Page2", var1, var4, this.stepScale, var6, this.totalSize, var5, this.offsetAmount)).setPos(var7);
      var10.quadrant = 2;
      this.attach(var10);
      var6 = createHeightSubBlock(var2, var4 - 1, 0, var4);
      var7 = new Vector3f((float)var3 * this.stepScale.x, 0.0F, (float)(-var3) * this.stepScale.z);
      var5.x = this.offset.x;
      var5.y = this.offset.y;
      var5.x += var7.x;
      var5.y += var7.z;
      (var10 = new TerrainPage(this.getName() + "Page3", var1, var4, this.stepScale, var6, this.totalSize, var5, this.offsetAmount)).setPos(var7);
      var10.quadrant = 3;
      this.attach(var10);
      var2 = createHeightSubBlock(var2, var4 - 1, var4 - 1, var4);
      Vector3f var9 = new Vector3f((float)var3 * this.stepScale.x, 0.0F, (float)var3 * this.stepScale.z);
      var5.x = this.offset.x;
      var5.y = this.offset.y;
      var5.x += var9.x;
      var5.y += var9.z;
      TerrainPage var8;
      (var8 = new TerrainPage(this.getName() + "Page4", var1, var4, this.stepScale, var2, this.totalSize, var5, this.offsetAmount)).setPos(var9);
      var8.quadrant = 4;
      this.attach(var8);
   }

   private void deleteNormalVBO(TerrainBlock var1) {
      if (var1.getVBOInfo() != null && var1.getVBOInfo().getVBOIndexID() > 0) {
         var1.getVBOInfo().setVBONormalID(-1);
      }

   }

   private int findQuadrant(int var1, int var2) {
      int var3 = this.size + 1 >> 1;
      int var4 = 0;
      if (var1 < var3 && var2 < var3) {
         var4 = 1;
      }

      if (var1 < var3 && var2 >= var3 - 1) {
         var4 |= 2;
      }

      if (var1 >= var3 - 1 && var2 < var3) {
         var4 |= 4;
      }

      if (var1 >= var3 - 1 && var2 >= var3 - 1) {
         var4 |= 8;
      }

      return var4;
   }

   public void fixNormals() {
      if (this.getChilds() != null) {
         int var1 = this.getChilds().size();

         while(true) {
            while(true) {
               --var1;
               if (var1 < 0) {
                  return;
               }

               AbstractSceneNode var2;
               if ((var2 = (AbstractSceneNode)this.getChilds().get(var1)) instanceof TerrainPage) {
                  ((TerrainPage)var2).fixNormals();
               } else if (var2 instanceof TerrainBlock) {
                  TerrainBlock var10 = (TerrainBlock)var2;
                  TerrainBlock var3 = this._findRightBlock(var10);
                  TerrainBlock var4 = this._findDownBlock(var10);
                  int var5 = var10.getSize();
                  int var8;
                  int var9;
                  if (var3 != null) {
                     float[] var6 = new float[3];

                     for(int var7 = 0; var7 < var5; ++var7) {
                        var8 = (var7 + 1) * var5 - 1;
                        var9 = var7 * var5;
                        var3.getNormalBuffer().position(var9 * 3);
                        var3.getNormalBuffer().get(var6);
                        var10.getNormalBuffer().position(var8 * 3);
                        var10.getNormalBuffer().put(var6);
                     }

                     this.deleteNormalVBO(var3);
                  }

                  if (var4 != null) {
                     int var11 = (var5 - 1) * var5;
                     float[] var12 = new float[3];

                     for(var8 = 0; var8 < var5; ++var8) {
                        var9 = var11 + var8;
                        var4.getNormalBuffer().position(var8 * 3);
                        var4.getNormalBuffer().get(var12);
                        var10.getNormalBuffer().position(var9 * 3);
                        var10.getNormalBuffer().put(var12);
                     }

                     this.deleteNormalVBO(var4);
                  }

                  this.deleteNormalVBO(var10);
               }
            }
         }
      }
   }

   private TerrainBlock getBlock(int var1) {
      if (this.getChilds() != null) {
         int var2 = this.getChilds().size();

         while(true) {
            --var2;
            if (var2 < 0) {
               break;
            }

            AbstractSceneNode var3;
            TerrainBlock var4;
            if ((var3 = (AbstractSceneNode)this.getChilds().get(var2)) instanceof TerrainBlock && (var4 = (TerrainBlock)var3).getQuadrant() == var1) {
               return var4;
            }
         }
      }

      return null;
   }

   public float getHeight(float var1, float var2) {
      int var3;
      float var4 = (float)(var3 = this.size - 1 >> 1) * this.stepScale.x;
      float var5 = (float)var3 * this.stepScale.z;
      if (var1 == 0.0F) {
         var1 += 0.001F;
      }

      if (var2 == 0.0F) {
         var2 += 0.001F;
      }

      AbstractSceneNode var6;
      if (var1 > 0.0F) {
         if (var2 > 0.0F) {
            var6 = (AbstractSceneNode)this.getChilds().get(3);
            var4 = var1;
            var5 = var2;
         } else {
            var6 = (AbstractSceneNode)this.getChilds().get(2);
            var4 = var1;
            var5 += var2;
         }
      } else if (var2 > 0.0F) {
         var6 = (AbstractSceneNode)this.getChilds().get(1);
         var4 += var1;
         var5 = var2;
      } else {
         var6 = (AbstractSceneNode)this.getChilds().get(0);
         if (var1 == 0.0F) {
            var1 -= 0.1F;
         }

         if (var2 == 0.0F) {
            var2 -= 0.1F;
         }

         var4 += var1;
         var5 += var2;
      }

      if (var6 instanceof TerrainBlock) {
         return ((TerrainBlock)var6).getHeight(var4, var5);
      } else {
         return var6 instanceof TerrainPage ? ((TerrainPage)var6).getHeight(var1 - ((TerrainPage)var6).getPos().x, var2 - ((TerrainPage)var6).getPos().z) : Float.NaN;
      }
   }

   public float getHeight(Vector2f var1) {
      return this.getHeight(var1.x, var1.y);
   }

   public float getHeight(Vector3f var1) {
      return this.getHeight(var1.x, var1.z);
   }

   public float getHeightFromWorld(Vector3f var1) {
      calcVec1.set(var1);
      (var1 = new Vector3f(calcVec1)).sub(this.getPos());
      var1.x /= this.stepScale.x;
      var1.y /= this.stepScale.y;
      var1.z /= this.stepScale.z;
      var1.x *= this.getStepScale().x;
      var1.y *= this.getStepScale().y;
      var1.z *= this.getStepScale().z;
      return this.getHeight(var1.x, var1.z);
   }

   public Vector2f getOffset() {
      return this.offset;
   }

   public void setOffset(Vector2f var1) {
      this.offset = var1;
   }

   public float getOffsetAmount() {
      return this.offsetAmount;
   }

   public void setOffsetAmount(float var1) {
      this.offsetAmount = var1;
   }

   private TerrainPage getPage(int var1) {
      if (this.getChilds() != null) {
         int var2 = this.getChilds().size();

         while(true) {
            --var2;
            if (var2 < 0) {
               break;
            }

            AbstractSceneNode var3;
            TerrainPage var4;
            if ((var3 = (AbstractSceneNode)this.getChilds().get(var2)) instanceof TerrainPage && (var4 = (TerrainPage)var3).getQuadrant() == var1) {
               return var4;
            }
         }
      }

      return null;
   }

   public short getQuadrant() {
      return this.quadrant;
   }

   public void setQuadrant(short var1) {
      this.quadrant = var1;
   }

   public int getSize() {
      return this.size;
   }

   public void setSize(int var1) {
      this.size = var1;
   }

   public Vector3f getStepScale() {
      return this.stepScale;
   }

   public void setStepScale(Vector3f var1) {
      this.stepScale = var1;
   }

   public Vector3f getSurfaceNormal(float var1, float var2, Vector3f var3) {
      int var4;
      float var5 = (float)(var4 = this.size - 1 >> 1) * this.stepScale.x;
      float var6 = (float)var4 * this.stepScale.z;
      if (var1 == 0.0F) {
         var1 += 0.001F;
      }

      if (var2 == 0.0F) {
         var2 += 0.001F;
      }

      AbstractSceneNode var7;
      if (var1 > 0.0F) {
         if (var2 > 0.0F) {
            var7 = (AbstractSceneNode)this.getChilds().get(3);
            var5 = var1;
            var6 = var2;
         } else {
            var7 = (AbstractSceneNode)this.getChilds().get(2);
            var5 = var1;
            var6 += var2;
         }
      } else if (var2 > 0.0F) {
         var7 = (AbstractSceneNode)this.getChilds().get(1);
         var5 += var1;
         var6 = var2;
      } else {
         var7 = (AbstractSceneNode)this.getChilds().get(0);
         if (var1 == 0.0F) {
            var1 -= 0.1F;
         }

         if (var2 == 0.0F) {
            var2 -= 0.1F;
         }

         var5 += var1;
         var6 += var2;
      }

      if (var7 instanceof TerrainBlock) {
         return ((TerrainBlock)var7).getSurfaceNormal(var5, var6, var3);
      } else {
         return var7 instanceof TerrainPage ? ((TerrainPage)var7).getSurfaceNormal(var1 - ((TerrainPage)var7).getPos().x, var2 - ((TerrainPage)var7).getPos().z, var3) : null;
      }
   }

   public Vector3f getSurfaceNormal(Vector2f var1, Vector3f var2) {
      return this.getSurfaceNormal(var1.x, var1.y, var2);
   }

   public Vector3f getSurfaceNormal(Vector3f var1, Vector3f var2) {
      return this.getSurfaceNormal(var1.x, var1.z, var2);
   }

   public int getTotalSize() {
      return this.totalSize;
   }

   public void setTotalSize(int var1) {
      this.totalSize = var1;
   }

   public void multHeightMapValue(int var1, int var2, float var3) {
      int var4 = this.findQuadrant(var1, var2);
      int var5 = this.size + 1 >> 1;
      if (this.getChilds() != null) {
         int var6 = this.getChilds().size();

         while(true) {
            --var6;
            if (var6 < 0) {
               break;
            }

            AbstractSceneNode var7 = (AbstractSceneNode)this.getChilds().get(var6);
            int var8 = var1;
            int var9 = var2;
            boolean var10 = false;
            short var11 = 0;
            if (var7 instanceof TerrainPage) {
               var11 = ((TerrainPage)var7).getQuadrant();
            } else if (var7 instanceof TerrainBlock) {
               var11 = ((TerrainBlock)var7).getQuadrant();
            }

            if (var11 == 1 && (var4 & 1) != 0) {
               var10 = true;
            } else if (var11 == 2 && (var4 & 2) != 0) {
               var9 = var2 - var5 + 1;
               var10 = true;
            } else if (var11 == 3 && (var4 & 4) != 0) {
               var8 = var1 - var5 + 1;
               var10 = true;
            } else if (var11 == 4 && (var4 & 8) != 0) {
               var8 = var1 - var5 + 1;
               var9 = var2 - var5 + 1;
               var10 = true;
            }

            if (var10) {
               if (var7 instanceof TerrainPage) {
                  ((TerrainPage)var7).multHeightMapValue(var8, var9, var3);
               } else if (var7 instanceof TerrainBlock) {
                  ((TerrainBlock)var7).multHeightMapValue(var8, var9, var3);
               }
            }
         }
      }

   }

   public void setDetailTexture(int var1, int var2) {
      for(int var3 = 0; var3 < this.getChilds().size(); ++var3) {
         if (this.getChilds().get(var3) instanceof TerrainPage) {
            ((TerrainPage)this.getChilds().get(var3)).setDetailTexture(var1, var2);
         } else if (this.getChilds().get(var3) instanceof TerrainBlock) {
            ((TerrainBlock)this.getChilds().get(var3)).setDetailTexture(var1, (float)var2);
         }
      }

   }

   public void setHeightMapValue(int var1, int var2, float var3) {
      int var4 = this.findQuadrant(var1, var2);
      int var5 = this.size + 1 >> 1;
      if (this.getChilds() != null) {
         int var6 = this.getChilds().size();

         while(true) {
            --var6;
            if (var6 < 0) {
               break;
            }

            AbstractSceneNode var7 = (AbstractSceneNode)this.getChilds().get(var6);
            int var8 = var1;
            int var9 = var2;
            boolean var10 = false;
            short var11 = 0;
            if (var7 instanceof TerrainPage) {
               var11 = ((TerrainPage)var7).getQuadrant();
            } else if (var7 instanceof TerrainBlock) {
               var11 = ((TerrainBlock)var7).getQuadrant();
            }

            if (var11 == 1 && (var4 & 1) != 0) {
               var10 = true;
            } else if (var11 == 2 && (var4 & 2) != 0) {
               var9 = var2 - var5 + 1;
               var10 = true;
            } else if (var11 == 3 && (var4 & 4) != 0) {
               var8 = var1 - var5 + 1;
               var10 = true;
            } else if (var11 == 4 && (var4 & 8) != 0) {
               var8 = var1 - var5 + 1;
               var9 = var2 - var5 + 1;
               var10 = true;
            }

            if (var10) {
               if (var7 instanceof TerrainPage) {
                  ((TerrainPage)var7).setHeightMapValue(var8, var9, var3);
               } else if (var7 instanceof TerrainBlock) {
                  ((TerrainBlock)var7).setHeightMapValue(var8, var9, var3);
               }
            }
         }
      }

   }

   private void split(int var1, float[] var2) {
      if ((this.size >> 1) + 1 <= var1) {
         this.createQuadBlock(var2);
      } else {
         this.createQuadPage(var1, var2);
      }
   }

   public void updateFromHeightMap() {
      for(int var1 = 0; var1 < this.getChilds().size(); ++var1) {
         if (this.getChilds().get(var1) instanceof TerrainPage) {
            ((TerrainPage)this.getChilds().get(var1)).updateFromHeightMap();
         } else if (this.getChilds().get(var1) instanceof TerrainBlock) {
            ((TerrainBlock)this.getChilds().get(var1)).updateFromHeightMap();
         }
      }

   }

   public void updateModelBound() {
      for(int var1 = 0; var1 < this.getChilds().size(); ++var1) {
         if (this.getChilds().get(var1) instanceof TerrainPage) {
            ((TerrainPage)this.getChilds().get(var1)).updateModelBound();
         } else if (this.getChilds().get(var1) instanceof TerrainBlock) {
            ((TerrainBlock)this.getChilds().get(var1)).updateModelBound();
         }
      }

   }
}
