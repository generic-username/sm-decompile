package org.schema.game.client.data.terrain.geimipmap;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.List;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.game.client.data.terrain.BufferUtils;
import org.schema.game.client.data.terrain.LODLoadableInterface;
import org.schema.game.client.data.terrain.Terrain;
import org.schema.game.client.data.terrain.geimipmap.lodcalc.LodCalculator;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.SceneNode;

public class TerrainPatch extends SceneNode {
   protected LODGeomap geomap;
   protected int lod;
   protected int previousLod;
   protected int lodLeft;
   protected int lodTop;
   protected int lodRight;
   protected int lodBottom;
   protected int size;
   protected int totalSize;
   protected short quadrant;
   protected Vector3f stepScale;
   protected Vector2f offset;
   protected float offsetAmount;
   protected LodCalculator lodCalculator;
   protected TerrainPatch leftNeighbour;
   protected TerrainPatch topNeighbour;
   protected TerrainPatch rightNeighbour;
   protected TerrainPatch bottomNeighbour;
   protected boolean searchedForNeighboursAlready;
   private int maxLod;
   private Mesh mesh;

   public TerrainPatch(String var1) {
      this.lod = -1;
      this.previousLod = -1;
      this.quadrant = 1;
      this.searchedForNeighboursAlready = false;
      this.maxLod = -1;
      this.setName(var1);
   }

   public TerrainPatch(String var1, int var2, LODLoadableInterface var3) {
      this(var1, var2, new Vector3f(1.0F, 1.0F, 1.0F), (float[])null, new Vector3f(0.0F, 0.0F, 0.0F), var3);
   }

   public TerrainPatch(String var1, int var2, Vector3f var3, float[] var4, Vector3f var5, int var6, Vector2f var7, float var8, LODLoadableInterface var9) {
      this(var1);
      this.size = var2;
      this.stepScale = var3;
      this.totalSize = var6;
      this.offsetAmount = var8;
      this.offset = var7;
      this.setPos(var5);
      System.err.println("generating heightbuffer");
      FloatBuffer var10;
      (var10 = BufferUtils.createFloatBuffer(var2 * var2)).put(var4);
      System.err.println("creating geomap");
      this.geomap = new LODGeomap(var2, var10, var9);
      System.err.println("creating cubeMeshes");
      Mesh var11 = this.geomap.createMesh(var3, new Vector2f(1.0F, 1.0F), var7, var3, var8, var6, false, this);
      this.setMesh(var11);
   }

   public TerrainPatch(String var1, int var2, Vector3f var3, float[] var4, Vector3f var5, LODLoadableInterface var6) {
      this(var1, var2, var3, var4, var5, var2, new Vector2f(), 0.0F, var6);
   }

   public TerrainPatch(String var1, int var2, Vector3f var3, Vector3f var4, int var5, Vector2f var6, float var7, Mesh var8, LODGeomap var9, LODLoadableInterface var10) {
      this.lod = -1;
      this.previousLod = -1;
      this.quadrant = 1;
      this.searchedForNeighboursAlready = false;
      this.maxLod = -1;
      this.setName(var1);
      this.size = var2;
      this.stepScale = var3;
      this.totalSize = var5;
      this.offsetAmount = var7;
      this.offset = var6;
      this.setPos(var4);
      System.err.println("creating geomap");
      this.geomap = var9;
      System.err.println("creating cubeMeshes");
      this.setMesh(var8);
   }

   protected boolean calculateLod(List var1, HashMap var2) {
      return this.lodCalculator.calculateLod(var1, var2);
   }

   public void draw() {
      if (this.mesh.isLoaded()) {
         GlUtil.glPushMatrix();
         this.transform();
         this.mesh.draw();
         GlUtil.glPopMatrix();
         ++Terrain.currently_drawn;
      }

   }

   public TerrainPatch clone() {
      TerrainPatch var1 = new TerrainPatch(this.getName(), this.size, new Vector3f(this.stepScale), new Vector3f(this.getPos()), this.totalSize, new Vector2f(this.offset), this.offsetAmount, this.getMesh(), this.geomap, (LODLoadableInterface)null);
      System.err.println("size of clone " + var1.getSize());
      var1.lodCalculator = ((TerrainQuad)this.getParent()).lodCalculatorFactory.createCalculator(var1);
      var1.setBoundingBox(this.mesh.getBoundingBox());
      return var1;
   }

   public float getHeight(float var1, float var2) {
      return 0.0F;
   }

   public float getHeight(Vector2f var1) {
      return this.getHeight(var1.x, var1.y);
   }

   public float getHeight(Vector3f var1) {
      return this.getHeight(var1.x, var1.z);
   }

   public int getLod() {
      return this.lod;
   }

   public void setLod(int var1) {
      this.lod = var1;
   }

   protected int getLodBottom() {
      return this.lodBottom;
   }

   protected void setLodBottom(int var1) {
      this.lodBottom = var1;
   }

   public LodCalculator getLodCalculator() {
      return this.lodCalculator;
   }

   public void setLodCalculator(LodCalculator var1) {
      this.lodCalculator = var1;
   }

   protected int getLodLeft() {
      return this.lodLeft;
   }

   protected void setLodLeft(int var1) {
      this.lodLeft = var1;
   }

   protected int getLodRight() {
      return this.lodRight;
   }

   protected void setLodRight(int var1) {
      this.lodRight = var1;
   }

   protected int getLodTop() {
      return this.lodTop;
   }

   protected void setLodTop(int var1) {
      this.lodTop = var1;
   }

   public int getMaxLod() {
      if (this.maxLod < 0) {
         this.maxLod = Math.max(1, (int)(FastMath.log((float)(this.size - 1)) / FastMath.log(2.0F)) - 1);
      }

      return this.maxLod;
   }

   public Mesh getMesh() {
      return this.mesh;
   }

   public void setMesh(Mesh var1) {
      this.mesh = var1;
   }

   public Vector2f getOffset() {
      return this.offset;
   }

   public void setOffset(Vector2f var1) {
      this.offset = var1;
   }

   public float getOffsetAmount() {
      return this.offsetAmount;
   }

   public void setOffsetAmount(float var1) {
      this.offsetAmount = var1;
   }

   public int getPreviousLod() {
      return this.previousLod;
   }

   public void setPreviousLod(int var1) {
      this.previousLod = var1;
   }

   public short getQuadrant() {
      return this.quadrant;
   }

   public void setQuadrant(short var1) {
      this.quadrant = var1;
   }

   public int getSize() {
      return this.size;
   }

   public void setSize(int var1) {
      this.size = var1;
      this.maxLod = -1;
   }

   public Vector3f getStepScale() {
      return this.stepScale;
   }

   public void setStepScale(Vector3f var1) {
      this.stepScale = var1;
   }

   public int getTotalSize() {
      return this.totalSize;
   }

   public void setTotalSize(int var1) {
      this.totalSize = var1;
   }

   public boolean isLoaded() {
      return this.mesh.isLoaded();
   }

   protected void reIndexGeometry(HashMap var1) {
      UpdatedTerrainPatch var7;
      if ((var7 = (UpdatedTerrainPatch)var1.get(this.getName())) != null && (var7.isReIndexNeeded() || var7.isFixEdges())) {
         int var2 = (int)Math.pow(2.0D, (double)var7.getNewLod());
         boolean var3 = var7.getLeftLod() > var7.getNewLod();
         boolean var4 = var7.getTopLod() > var7.getNewLod();
         boolean var5 = var7.getRightLod() > var7.getNewLod();
         boolean var6 = var7.getBottomLod() > var7.getNewLod();
         IntBuffer var8 = this.geomap.writeIndexArrayLodDiff((IntBuffer)null, var2, var5, var4, var3, var6);
         var7.setNewIndexBuffer(var8);
      }

   }

   public void updateModelBound() {
      if (this.mesh != null) {
         this.mesh.updateBound();
         this.setBoundingBox(this.mesh.getBoundingBox());
      }

   }
}
