package org.schema.game.client.data.terrain.geimipmap.lodcalc;

public interface LodThreshold {
   float getLodDistanceThreshold();
}
