package org.schema.game.client.data.terrain.geimipmap;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.game.client.data.terrain.BufferGeomap;
import org.schema.game.client.data.terrain.BufferUtils;
import org.schema.game.client.data.terrain.LODLoadableInterface;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Mesh;

public class LODGeomap extends BufferGeomap {
   public static boolean LOAD_IMIDIATELY = false;
   private static int count;
   private final int maxLod;
   private LODGeomap.TerrainPatchLoader terrainLoader;
   private LODLoadableInterface terrain;

   public LODGeomap(int var1, FloatBuffer var2, LODLoadableInterface var3) {
      super(var2, (ByteBuffer)null, var1, var1, 1);
      this.terrain = var3;
      this.maxLod = Math.max(1, (int)(FastMath.log((float)(var1 - 1)) / FastMath.log(2.0F)) - 1);
   }

   private int calculateNumIndexesLodDiff(int var1) {
      int var2;
      int var10000 = var2 = (this.getWidth() - 1) / var1 + 1 - 2;
      int var3 = (var10000 * var10000 << 1) - 2 * var2;
      var2 = 2 * (var2 - 2);
      var3 = var3 + var2 + (this.getWidth() / var1 << 1 << 2);
      ++var3;
      var3 += 10;
      return var3;
   }

   public Mesh createMesh(Vector3f var1, Vector2f var2, Vector2f var3, Vector3f var4, float var5, int var6, boolean var7, int var8, boolean var9, boolean var10, boolean var11, boolean var12, TerrainPatch var13) {
      System.err.println("loading cubeMeshes for patch " + count++);
      Mesh var14 = new Mesh();
      if (LOAD_IMIDIATELY) {
         this.terrainLoader = new LODGeomap.TerrainPatchLoader(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var14, var13);
         this.terrainLoader.loadMesh();
      } else {
         this.terrainLoader = new LODGeomap.TerrainPatchLoader(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var14, var13);
         this.terrain.getGeoMapsToLoad().add(this);
      }

      return var14;
   }

   public Mesh createMesh(Vector3f var1, Vector2f var2, Vector2f var3, Vector3f var4, float var5, int var6, boolean var7, TerrainPatch var8) {
      return this.createMesh(var1, var2, var3, var4, var5, var6, var7, 1, false, false, false, false, var8);
   }

   public void doLoadStep() {
      this.terrainLoader.loadMesh();
   }

   public Vector2f getUV(int var1, int var2, Vector2f var3, Vector2f var4, float var5, int var6, Vector3f var7) {
      float var8 = var4.x / var7.x + var5;
      float var9 = var4.y / var7.z + var5;
      var3.set(((float)var1 + var8) / (float)var6, ((float)var2 + var9) / (float)var6);
      return var3;
   }

   public boolean isMeshLoaded() {
      return this.terrainLoader.m.isLoaded();
   }

   public IntBuffer writeIndexArrayLodDiff(IntBuffer var1, int var2, boolean var3, boolean var4, boolean var5, boolean var6) {
      IntBuffer var7 = var1;
      int var8 = this.calculateNumIndexesLodDiff(var2);
      if (var1 == null) {
         var7 = BufferUtils.createIntBuffer(var8);
      }

      LODGeomap.VerboseIntBuffer var13 = new LODGeomap.VerboseIntBuffer(var7);

      int var9;
      int var10;
      int var11;
      int var14;
      for(var14 = var2; var14 < this.getWidth() - 2 * var2; var14 += var2) {
         var9 = var14 * this.getWidth();
         var10 = (var14 + 1 * var2) * this.getWidth();

         for(var11 = var2; var11 < this.getWidth() - 1 * var2; var11 += var2) {
            int var12 = var9 + var11;
            var13.put(var12);
            var12 = var10 + var11;
            var13.put(var12);
         }

         if (var14 < this.getWidth() - 3 * var2) {
            var11 = var10 + this.getWidth() - 1 * var2 - 1;
            var13.put(var11);
            var11 = var10 + 1 * var2;
            var13.put(var11);
         }
      }

      var14 = this.getWidth() * (this.getWidth() - var2) - 1 - var2;
      var13.put(var14);
      var9 = this.getWidth() * this.getWidth() - 1;
      var13.put(var9);
      if (var3) {
         for(var10 = this.getWidth() - var2; var10 >= var2 + 1; var10 -= 2 * var2) {
            var11 = var10 * this.getWidth() - 1 - var2;
            var13.put(var11);
            var11 = (var10 - var2) * this.getWidth() - 1;
            var13.put(var11);
            if (var10 > var2 + 1) {
               var11 = (var10 - var2) * this.getWidth() - 1 - var2;
               var13.put(var11);
               var11 = (var10 - var2) * this.getWidth() - 1;
               var13.put(var11);
            }
         }
      } else {
         var13.put(var9);

         for(var10 = this.getWidth() - var2; var10 > var2; var10 -= var2) {
            var11 = var10 * this.getWidth() - 1;
            var13.put(var11);
            var13.put(var11 - var2);
         }
      }

      var13.put(this.getWidth() - 1);
      if (var4) {
         if (var3) {
            var13.put(this.getWidth() - 1);
         }

         for(var10 = this.getWidth() - 1; var10 >= var2; var10 -= 2 * var2) {
            var11 = var2 * this.getWidth() + var10 - var2;
            var13.put(var11);
            var11 = var10 - 2 * var2;
            var13.put(var11);
            if (var10 > var2 << 1) {
               var11 = var2 * this.getWidth() + var10 - 2 * var2;
               var13.put(var11);
               var11 = var10 - 2 * var2;
               var13.put(var11);
            }
         }
      } else {
         if (var3) {
            var13.put(this.getWidth() - 1);
         }

         for(var10 = this.getWidth() - 1 - var2; var10 > 0; var10 -= var2) {
            var11 = var10 + var2 * this.getWidth();
            var13.put(var11);
            var13.put(var10);
         }

         var13.put(0);
      }

      var13.put(0);
      if (var5) {
         if (var4) {
            var13.put(0);
         }

         for(var10 = 0; var10 < this.getWidth() - var2; var10 += 2 * var2) {
            var11 = (var10 + var2) * this.getWidth() + var2;
            var13.put(var11);
            var11 = (var10 + 2 * var2) * this.getWidth();
            var13.put(var11);
            if (var10 < this.getWidth() - var2 - 2 - 1) {
               var11 = (var10 + 2 * var2) * this.getWidth() + var2;
               var13.put(var11);
               var11 = (var10 + 2 * var2) * this.getWidth();
               var13.put(var11);
            }
         }
      } else {
         if (!var4) {
            var13.put(0);
         }

         for(var10 = var2; var10 < this.getWidth() - var2; var10 += var2) {
            var11 = var10 * this.getWidth();
            var13.put(var11);
            var11 = var10 * this.getWidth() + var2;
            var13.put(var11);
         }
      }

      var13.put(this.getWidth() * (this.getWidth() - 1));
      if (var6) {
         if (var5) {
            var13.put(this.getWidth() * (this.getWidth() - 1));
         }

         for(var10 = 0; var10 < this.getWidth() - var2; var10 += 2 * var2) {
            var11 = this.getWidth() * (this.getWidth() - 1 - var2) + var10 + var2;
            var13.put(var11);
            var11 = this.getWidth() * (this.getWidth() - 1) + var10 + 2 * var2;
            var13.put(var11);
            if (var10 < this.getWidth() - 1 - 2 * var2) {
               var11 = this.getWidth() * (this.getWidth() - 1 - var2) + var10 + 2 * var2;
               var13.put(var11);
               var11 = this.getWidth() * (this.getWidth() - 1) + var10 + 2 * var2;
               var13.put(var11);
            }
         }
      } else {
         if (var5) {
            var13.put(this.getWidth() * (this.getWidth() - 1));
         }

         for(var10 = var2; var10 < this.getWidth() - var2; var10 += var2) {
            var11 = this.getWidth() * (this.getWidth() - 1 - var2) + var10;
            var13.put(var11);
            var11 = this.getWidth() * (this.getWidth() - 1) + var10;
            var13.put(var11);
         }
      }

      var13.put(this.getWidth() * this.getWidth() - 1);

      for(var10 = var13.getCount(); var10 < var8; ++var10) {
         var13.put(this.getWidth() * this.getWidth() - 1);
      }

      return var13.delegate;
   }

   public FloatBuffer writeTexCoordArray(FloatBuffer var1, Vector2f var2, Vector2f var3, float var4, int var5, Vector3f var6) {
      if (var1 != null) {
         if (var1.remaining() < this.getWidth() * this.getHeight() << 1) {
            throw new BufferUnderflowException();
         }
      } else {
         var1 = BufferUtils.createFloatBuffer(this.getWidth() * this.getHeight() << 1);
      }

      if (var2 == null) {
         var2 = new Vector2f();
      }

      Vector2f var7 = new Vector2f();
      System.err.println(var2 + ", " + var4 + ", " + var3);

      for(int var8 = 0; var8 < this.getHeight(); ++var8) {
         for(int var9 = 0; var9 < this.getWidth(); ++var9) {
            this.getUV(var9, var8, var7, var2, var4, var5, var6);
            float var10 = var7.x * var3.x;
            float var11 = var7.y * var3.y;
            var1.put(var10);
            var1.put(var11);
         }
      }

      return var1;
   }

   public class VerboseIntBuffer {
      int count = 0;
      private IntBuffer delegate;

      public VerboseIntBuffer(IntBuffer var2) {
         this.delegate = var2;
      }

      public int getCount() {
         return this.count;
      }

      public void put(int var1) {
         try {
            this.delegate.put(var1);
            ++this.count;
         } catch (BufferOverflowException var2) {
         }
      }
   }

   class TerrainPatchLoader {
      int phase = 0;
      private Mesh m;
      private FloatBuffer pb;
      private FloatBuffer tb;
      private FloatBuffer nb;
      private IntBuffer ib;
      private TerrainPatch patch;
      private Vector3f scale;
      private Vector2f tcScale;
      private Vector2f tcOffset;
      private Vector3f stepScale;
      private float offsetAmount;
      private int totalSize;
      private boolean center;
      private int lod;
      private boolean rightLod;
      private boolean topLod;
      private boolean leftLod;
      private boolean bottomLod;

      public TerrainPatchLoader(Vector3f var2, Vector2f var3, Vector2f var4, Vector3f var5, float var6, int var7, boolean var8, int var9, boolean var10, boolean var11, boolean var12, boolean var13, Mesh var14, TerrainPatch var15) {
         this.scale = var2;
         this.tcScale = var3;
         this.tcOffset = var4;
         this.stepScale = var5;
         this.offsetAmount = var6;
         this.totalSize = var7;
         this.center = var8;
         this.lod = var9;
         this.rightLod = var10;
         this.topLod = var11;
         this.leftLod = var12;
         this.bottomLod = var13;
         this.m = var14;
         this.patch = var15;
      }

      public void loadMesh() {
         switch(this.phase) {
         case 0:
            System.err.println("writingToDiskLock vertex array");
            this.pb = LODGeomap.this.writeVertexArray((FloatBuffer)null, this.scale, this.center);
            break;
         case 1:
            System.err.println("writingToDiskLock noiseVolume array");
            this.tb = LODGeomap.this.writeTexCoordArray((FloatBuffer)null, this.tcOffset, this.tcScale, this.offsetAmount, this.totalSize, this.stepScale);
            break;
         case 2:
            System.err.println("writingToDiskLock normal array");
            this.nb = LODGeomap.this.writeNormalArray((FloatBuffer)null, this.scale);
            break;
         case 3:
            System.err.println("writingToDiskLock index array");
            this.ib = LODGeomap.this.writeIndexArrayLodDiff((IntBuffer)null, this.lod, this.rightLod, this.topLod, this.leftLod, this.bottomLod);
            break;
         case 4:
            System.err.println("setting vertex array");
            this.m.setBuffer(2, 3, this.pb);
            break;
         case 5:
            System.err.println("setting normal array");
            this.m.setBuffer(3, 3, this.nb);
            break;
         case 6:
            System.err.println("setting noiseVolume array");
            this.m.setBuffer(6, 2, this.tb);
            break;
         case 7:
            System.err.println("setting index array");
            this.m.setBuffer(1, 3, this.ib);
            break;
         case 8:
            System.err.println("updating bounds");
            this.m.updateBound();
            this.m.setFaceCount(this.ib.capacity() / 3);
            this.m.setType(3);
            this.m.setDrawMode(5);
            this.m.setLoaded(true);
            LODGeomap.this.hdata = null;
            LODGeomap.this.ndata = null;
            this.pb = null;
            this.nb = null;
            this.tb = null;
            this.ib = null;
            this.patch.updateModelBound();
            GlUtil.glBindBuffer(34963, 0);
         }

         ++this.phase;
      }
   }
}
