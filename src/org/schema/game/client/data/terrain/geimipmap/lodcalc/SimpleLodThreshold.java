package org.schema.game.client.data.terrain.geimipmap.lodcalc;

public class SimpleLodThreshold implements LodThreshold {
   private int size;
   private float lodMultiplier = 2.0F;

   public SimpleLodThreshold(int var1, float var2) {
      this.size = var1;
      this.lodMultiplier = var2;
   }

   public float getLodDistanceThreshold() {
      return (float)this.size * this.lodMultiplier;
   }
}
