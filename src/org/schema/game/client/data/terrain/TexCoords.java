package org.schema.game.client.data.terrain;

import java.nio.FloatBuffer;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;

public class TexCoords {
   public FloatBuffer coords;
   public int perVert;

   public TexCoords() {
   }

   public TexCoords(FloatBuffer var1) {
      this(var1, 2);
   }

   public TexCoords(FloatBuffer var1, int var2) {
      this.coords = var1;
      this.perVert = var2;
   }

   public static TexCoords ensureSize(TexCoords var0, int var1, int var2) {
      if (var0 == null) {
         return new TexCoords(BufferUtils.createFloatBuffer(var1 * var2), var2);
      } else if (var0.coords.limit() == var2 * var1 && var0.perVert == var2) {
         var0.coords.rewind();
         return var0;
      } else {
         if (var0.coords.limit() == var2 * var1) {
            var0.perVert = var2;
         } else {
            var0.coords = BufferUtils.createFloatBuffer(var1 * var2);
            var0.perVert = var2;
         }

         return var0;
      }
   }

   public static TexCoords makeNew(float[] var0) {
      return var0 == null ? null : new TexCoords(BufferUtils.createFloatBuffer(var0), 1);
   }

   public static TexCoords makeNew(Vector2f[] var0) {
      return var0 == null ? null : new TexCoords(BufferUtils.createFloatBuffer(var0), 2);
   }

   public static TexCoords makeNew(Vector3f[] var0) {
      return var0 == null ? null : new TexCoords(BufferUtils.createFloatBuffer(var0), 3);
   }

   public Class getClassTag() {
      return TexCoords.class;
   }
}
