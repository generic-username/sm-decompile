package org.schema.game.client.data;

import org.schema.game.common.controller.SegmentController;

public interface PowerChangeListener {
   void powerChanged(SegmentController var1, PowerChangeListener.PowerChangeType var2);

   public static enum PowerChangeType {
      REACTOR,
      STABILIZER,
      CHAMBER,
      STABILIZER_PATH;
   }
}
