package org.schema.game.client.data.city;

import java.awt.Color;
import java.nio.IntBuffer;
import java.util.Random;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.texture.TextureLoader;

public class BuildingTexture {
   public static final int SEGMENTS_PER_TEXTURE = 32;
   public static final float ONE_SEGMENT = 0.03125F;
   public static final int LANES_PER_TEXTURE = 8;
   public static final float LANE_SIZE = 0.125F;
   public static final int TRIM_RESOLUTION = 256;
   public static final int TRIM_ROWS = 4;
   public static final float TRIM_SIZE = 0.25F;
   public static final int TRIM_PIXELS = 64;
   public static final int LOGO_RESOLUTION = 512;
   public static final int LOGO_ROWS = 16;
   public static final float LOGO_SIZE = 0.0625F;
   public static final int LOGO_PIXELS = 32;
   public static final int TEXTURE_LIGHT = 0;
   public static final int TEXTURE_SOFT_CIRCLE = 1;
   public static final int TEXTURE_SKY = 2;
   public static final int TEXTURE_LOGOS = 3;
   public static final int TEXTURE_TRIM = 4;
   public static final int TEXTURE_BLOOM = 5;
   public static final int TEXTURE_HEADLIGHT = 6;
   public static final int TEXTURE_LATTICE = 7;
   public static final int TEXTURE_BUILDING1 = 8;
   public static final int TEXTURE_BUILDING2 = 9;
   public static final int TEXTURE_BUILDING3 = 10;
   public static final int TEXTURE_BUILDING4 = 11;
   public static final int TEXTURE_BUILDING5 = 12;
   public static final int TEXTURE_BUILDING6 = 13;
   public static final int TEXTURE_BUILDING7 = 14;
   public static final int TEXTURE_BUILDING8 = 15;
   public static final int TEXTURE_BUILDING9 = 16;
   public static final int TEXTURE_COUNT = 17;
   public static final int BUILDING_COUNT = 9;
   public static float SKY_BANDS = 1.0F;
   static String[] prefix;
   public static int PREFIX_COUNT;
   static String[] name;
   public static int NAME_COUNT;
   static String[] suffix;
   public static int SUFFIX_COUNT;
   static BuildingTexture head;
   static boolean textures_done;
   static boolean[] prefix_used;
   static boolean[] name_used;
   static boolean[] suffix_used;
   static int build_time;
   static IntBuffer viewport;
   private static Random random;
   public static float RANDOM_COLOR_SHIFT;
   public static float RANDOM_COLOR_VAL;
   public static float RANDOM_COLOR_LIGHT;
   int _my_id;
   int _glid;
   int _desired_size;
   int _size;
   int _half;
   int _segment_size;
   boolean _ready;
   boolean _masked;
   boolean _mipmap;
   boolean _clamp;
   private BuildingTexture _next;

   public BuildingTexture(int var1, int var2, boolean var3, boolean var4, boolean var5) {
      this._my_id = var1;
      this._mipmap = var3;
      this._clamp = var4;
      this._masked = var5;
      this._desired_size = var2;
      this._size = var2;
      this._half = var2 / 2;
      this._segment_size = var2 / 32;
      this._ready = false;
      this._next = head;
      head = this;
   }

   static void do_bloom(BuildingTexture var0) {
      GlUtil.glBindTexture(3553, 0);
      GL11.glViewport(0, 0, var0._size, var0._size);
      GL11.glCullFace(1029);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glDepthMask(true);
      GL11.glPolygonMode(1032, 6914);
      GlUtil.glEnable(2929);
      GlUtil.glEnable(2884);
      GL11.glCullFace(1029);
      GlUtil.glBlendFunc(770, 771);
      GL11.glPolygonMode(1032, 6914);
      GL11.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
      GL11.glClear(16640);
      GlUtil.glEnable(3553);
      GlUtil.glBindTexture(3553, var0._glid);
      GL11.glTexParameteri(3553, 10240, 9729);
      GL11.glCopyTexImage2D(3553, 0, 6408, 0, 0, var0._size, var0._size, 0);
   }

   static void drawrect(int var0, int var1, int var2, int var3, Vector4f var4) {
      GlUtil.glDisable(2884);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      GL11.glLineWidth(1.0F);
      GL11.glPolygonMode(1032, 6914);
      GL11.glColor4f(var4.x, var4.y, var4.z, var4.w);
      if (var0 == var2) {
         GL11.glBegin(1);
         GL11.glVertex2i(var0, var1);
         GL11.glVertex2i(var0, var3);
         GL11.glEnd();
      }

      if (var1 == var3) {
         GL11.glBegin(1);
         GL11.glVertex2i(var0, var1);
         GL11.glVertex2i(var2, var1);
         GL11.glEnd();
      } else {
         GL11.glBegin(7);
         GL11.glVertex2i(var0, var1);
         GL11.glVertex2i(var2, var1);
         GL11.glVertex2i(var2, var3);
         GL11.glVertex2i(var0, var3);
         GL11.glEnd();
         float var8;
         boolean var6 = (var8 = (var4.x + var4.y + var4.z) / 3.0F) > 0.5F;
         int var5 = (int)(var8 * 255.0F);
         int var7;
         int var11;
         if (var6) {
            GlUtil.glBlendFunc(770, 771);
            GL11.glBegin(0);

            for(var11 = var0 + 1; var11 < var2 - 1; ++var11) {
               for(var7 = var1 + 1; var7 < var3 - 1; ++var7) {
                  GL11.glColor4f(255.0F, 0.0F, (float)random.nextInt(var5), 255.0F);
                  Color var9 = Color.getHSBColor(0.2F + (float)random.nextInt(100) / 300.0F + (float)random.nextInt(100) / 300.0F + (float)random.nextInt(100) / 300.0F, 0.3F, 0.5F);
                  (var4 = new Vector4f((float)var9.getRed(), (float)var9.getGreen(), (float)var9.getBlue(), (float)var9.getAlpha())).w = (float)random.nextInt(var5) / 144.0F;
                  GL11.glColor4f(RANDOM_COLOR_VAL, RANDOM_COLOR_VAL, RANDOM_COLOR_VAL, (float)random.nextInt(var5) / 144.0F);
                  GL11.glColor4f(var4.x, var4.y, var4.z, var4.w);
                  GL11.glVertex2i(var11, var7);
               }
            }

            GL11.glEnd();
         }

         random.nextInt(6);
         int var10 = var3 - var1 + (random.nextInt(3) - 1) + (random.nextInt(3) - 1);

         for(var11 = var0; var11 < var2; ++var11) {
            if (random.nextInt(3) == 0) {
               random.nextInt(4);
            }

            if (random.nextInt(6) == 0) {
               var10 = var3 - var1;
               var10 = (int)((float)random.nextInt(Math.abs(var10 + 1)) * Math.signum((float)var10));
               var10 = (int)((float)random.nextInt(Math.abs(var10 + 1)) * Math.signum((float)var10));
               var10 = (int)((float)random.nextInt(Math.abs(var10 + 1)) * Math.signum((float)var10));
               var10 = (var3 - var1 + var10) / 2;
            }

            for(var7 = 0; var7 <= 0; ++var7) {
               GL11.glBegin(1);
               GL11.glColor4f(0.0F, 0.0F, 0.0F, (float)random.nextInt(256) / 256.0F);
               GL11.glVertex2i(var11, var3 - var10);
               GL11.glColor4f(0.0F, 0.0F, 0.0F, (float)random.nextInt(256) / 256.0F);
               GL11.glVertex2i(var11, var3);
               GL11.glEnd();
            }
         }

      }
   }

   static void drawrect_simple(int var0, int var1, int var2, int var3, Vector4f var4) {
      GL11.glColor4f(var4.x, var4.y, var4.z, var4.w);
      GL11.glBegin(7);
      GL11.glVertex2i(var0, var1);
      GL11.glVertex2i(var2, var1);
      GL11.glVertex2i(var2, var3);
      GL11.glVertex2i(var0, var3);
      GL11.glEnd();
   }

   static void drawrect_simple(int var0, int var1, int var2, int var3, Vector4f var4, Vector4f var5) {
      GL11.glColor4f(var4.x, var4.y, var4.z, var4.w);
      GL11.glBegin(6);
      GL11.glVertex2i((var0 + var2) / 2, (var1 + var3) / 2);
      GL11.glColor4f(var5.x, var5.y, var5.z, var5.w);
      GL11.glVertex2i(var0, var1);
      GL11.glVertex2i(var2, var1);
      GL11.glVertex2i(var2, var3);
      GL11.glVertex2i(var0, var3);
      GL11.glVertex2i(var0, var1);
      GL11.glEnd();
   }

   private static int RenderBloom() {
      return 0;
   }

   public static int TextureId(int var0) {
      for(BuildingTexture var1 = head; var1 != null; var1 = var1._next) {
         if (var1._my_id == var0) {
            return var1._glid;
         }
      }

      return 0;
   }

   public static void TextureInit() {
      new BuildingTexture(7, 128, true, true, true);
      new BuildingTexture(0, 128, false, false, true);
      new BuildingTexture(1, 128, false, false, true);
      new BuildingTexture(6, 128, false, false, true);
      new BuildingTexture(4, 256, true, false, false);
      new BuildingTexture(3, 512, true, false, true);

      for(int var0 = 8; var0 <= 16; ++var0) {
         new BuildingTexture(var0, 512, true, false, false);
      }

      new BuildingTexture(5, 512, true, false, false);
      int var10000 = PREFIX_COUNT;
      var10000 = NAME_COUNT;
      var10000 = SUFFIX_COUNT;
      var10000 = NAME_COUNT;
      System.out.println("BUILDING TEXTURES DONE");
   }

   public static void TextureUpdate() {
      BuildingTexture var0;
      if (textures_done) {
         if (RenderBloom() == 0) {
            return;
         }

         for(var0 = head; var0 != null; var0 = var0._next) {
            if (var0._my_id == 5) {
               do_bloom(var0);
               return;
            }
         }
      }

      for(var0 = head; var0 != null; var0 = var0._next) {
         if (!var0._ready) {
            var0.Rebuild();
            GlUtil.printGlErrorCritical();
         }
      }

   }

   static void window(int var0, int var1, int var2, int var3, Vector4f var4) {
      int var5 = var2 / 3;
      int var6 = var2 / 2;
      switch(var3) {
      case 8:
         drawrect(var0 + 1, var1 + 1, var0 + var2 - 1, var1 + var2 - 1, var4);
         return;
      case 9:
         drawrect(var0 + var5, var1 + 1, var0 + var2 - var5, var1 + var2 - 1, var4);
         return;
      case 10:
         drawrect(var0 + 1, var1 + 1, var0 + var6 - 1, var1 + var2 - var5, var4);
         drawrect(var0 + var6 + 1, var1 + 1, var0 + var2 - 1, var1 + var2 - var5, var4);
         return;
      case 11:
         drawrect(var0 + 1, var1 + 1, var0 + var2 - 1, var1 + var2 - 1, var4);
         var3 = random.nextInt(var2 - 2);
         var4.scale(0.3F);
         drawrect(var0 + 1, var1 + 1, var0 + var2 - 1, var1 + var3 + 1, var4);
         return;
      case 12:
         drawrect(var0 + 1, var1 + 1, var0 + var2 - 1, var1 + var2 - 1, var4);
         var4.scale(0.7F);
         drawrect(var0 + var5, var1 + 1, var0 + var5, var1 + var2 - 1, var4);
         var4.scale(0.3F);
         drawrect(var0 + var2 - var5 - 1, var1 + 1, var0 + var2 - var5 - 1, var1 + var2 - 1, var4);
         return;
      case 13:
         drawrect(var0 + 1, var1 + 1, var0 + var2 - 1, var1 + var2 - var5, var4);
         return;
      case 14:
         drawrect(var0 + 2, var1 + 1, var0 + var2 - 1, var1 + var2 - 1, var4);
         var4.scale(0.2F);
         drawrect(var0 + 2, var1 + var6, var0 + var2 - 1, var1 + var6, var4);
         var4.scale(0.2F);
         drawrect(var0 + var6, var1 + 1, var0 + var6, var1 + var2 - 1, var4);
         return;
      case 15:
         drawrect(var0 + var6 - 1, var1 + 1, var0 + var6 + 1, var1 + var2 - var5, var4);
         return;
      case 16:
         drawrect(var0 + 1, var1 + var5, var0 + var2 - 1, var1 + var2 - var5 - 1, var4);
      default:
      }
   }

   private void cleanUp() {
      System.err.println("cleanup not implemented");
   }

   void DrawHeadlight() {
      Vector2f var5 = new Vector2f();
      float var1 = (float)this._half - 20.0F;
      int var3 = this._half - 20;
      int var4 = this._half;
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      GL11.glBegin(6);
      GL11.glColor4f(0.8F, 0.8F, 0.8F, 0.6F);
      GL11.glVertex2i(this._half - 5, var4);
      GL11.glColor4f(0.0F, 0.0F, 0.0F, 0.0F);

      int var2;
      float var6;
      for(var2 = 0; var2 <= 360; var2 += 36) {
         var6 = (float)Math.toRadians((double)(var2 % 360));
         var5.x = (float)Math.sin((double)(var6 * var1));
         var5.y = (float)Math.cos((double)(var6 * var1));
         GL11.glVertex2i(var3 + (int)var5.x, this._half + (int)var5.y);
      }

      GL11.glEnd();
      var3 = this._half + 20;
      GL11.glBegin(6);
      GL11.glColor4f(0.8F, 0.8F, 0.8F, 0.6F);
      GL11.glVertex2i(this._half + 5, var4);
      GL11.glColor4f(0.0F, 0.0F, 0.0F, 0.0F);

      for(var2 = 0; var2 <= 360; var2 += 36) {
         var6 = (float)Math.toRadians((double)(var2 % 360));
         var5.x = (float)Math.sin((double)(var6 * var1));
         var5.y = (float)Math.cos((double)(var6 * var1));
         GL11.glVertex2i(var3 + (int)var5.x, this._half + (int)var5.y);
      }

      GL11.glEnd();
      var3 = this._half - 6;
      Vector4f var7 = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
      drawrect_simple(var3 - 3, var4 - 2, var3 + 2, var4 + 2, var7);
      drawrect_simple((var3 = this._half + 6) - 2, var4 - 2, var3 + 3, var4 + 2, var7);
   }

   void DrawSky() {
   }

   void DrawWindows() {
      int var3 = 0;
      int var4 = 0;
      int var5 = 0;
      boolean var7 = false;

      for(int var2 = 0; var2 < 32; ++var2) {
         if (var2 % 8 == 0) {
            var3 = 0;
            var4 = random.nextInt(9) + 2;
            var5 = 2 + random.nextInt(2) + random.nextInt(2);
            var7 = false;
         }

         for(int var1 = 0; var1 < 32; ++var1) {
            if (var3 <= 0) {
               var3 = random.nextInt(var4);
               var7 = random.nextInt(var5) == 0;
            }

            float var6;
            Vector4f var8;
            if (var7) {
               var6 = 0.5F + (float)random.nextInt() % 128.0F / 256.0F;
               (var8 = new Vector4f(var6, var6, var6, 1.0F)).add(new Vector4f((float)random.nextInt(10) / 50.0F, (float)random.nextInt(10) / 50.0F, (float)random.nextInt(10) / 50.0F, 0.0F));
            } else {
               var6 = (float)(random.nextInt() % 40) / 256.0F;
               var8 = new Vector4f(var6, var6, var6, 1.0F);
            }

            window(var1 * this._segment_size, var2 * this._segment_size, this._segment_size, this._my_id, var8);
            --var3;
         }
      }

   }

   void Rebuild() {
      viewport.rewind();
      viewport.rewind();
      System.err.println("stated texture creation " + this._size + " x " + this._size);
      Vector2f var4 = new Vector2f();
      long var6 = System.currentTimeMillis();
      this._size = this._desired_size;

      int var1;
      for(var1 = this.RenderMaxTextureSize(); this._size > var1; this._size /= 2) {
      }

      int var10000;
      this._glid = TextureLoader.getEmptyTexture(this._size, this._size).getTextureId();
      System.err.println("binding to " + this._glid);
      GlUtil.glEnable(3553);
      GlUtil.glBindTexture(3553, this._glid);
      GlUtil.glDisable(2896);
      GlUtil.printGlErrorCritical();
      GL11.glViewport(0, 0, this._size, this._size);
      GlUtil.glMatrixMode(5889);
      GlUtil.printGlErrorCritical();
      GL11.glLoadIdentity();
      System.err.println("ortho " + this._size);
      GL11.glOrtho(0.0D, (double)this._size, (double)this._size, 0.0D, 0.10000000149011612D, 2048.0D);
      GlUtil.printGlErrorCritical();
      GlUtil.glMatrixMode(5888);
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      GlUtil.printGlErrorCritical();
      GlUtil.glDisable(2884);
      GlUtil.glDisable(2912);
      GlUtil.glBindTexture(3553, 0);
      GL11.glTranslatef(0.0F, 0.0F, -10.0F);
      GL11.glClearColor(0.0F, 0.0F, 0.0F, this._masked ? 0.0F : 1.0F);
      GL11.glClear(16640);
      GL11.glPolygonMode(1032, 6914);
      int var10;
      float var12;
      float var14;
      label125:
      switch(this._my_id) {
      case 0:
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         var10000 = this._half;
         var12 = 0.0F;
         var10 = 0;

         while(true) {
            if (var10 >= 2) {
               break label125;
            }

            GL11.glBegin(6);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            GL11.glVertex2i(this._half, this._half);
            if (var10 == 0) {
               var12 = (float)this._half / 2.0F;
            } else {
               var12 = 8.0F;
            }

            GL11.glColor4f(1.0F, 1.0F, 1.0F, 0.0F);

            for(var1 = 0; var1 <= 360; ++var1) {
               var14 = (float)Math.toRadians((double)var1);
               var4.x = (float)Math.sin((double)(var14 * var12));
               var4.y = (float)Math.cos((double)(var14 * var12));
               GL11.glVertex2i(this._half + (int)var4.x, this._half + (int)var4.y);
            }

            GL11.glEnd();
            ++var10;
         }
      case 1:
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         var12 = (float)this._half - 3.0F;
         GL11.glBegin(6);
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GL11.glVertex2i(this._half, this._half);
         GL11.glColor4f(0.0F, 0.0F, 0.0F, 0.0F);

         for(var1 = 0; var1 <= 360; ++var1) {
            var14 = (float)Math.toRadians((double)var1);
            var4.x = (float)Math.sin((double)(var14 * var12));
            var4.y = (float)Math.cos((double)(var14 * var12));
            GL11.glVertex2i(this._half + (int)var4.x, this._half + (int)var4.y);
         }

         GL11.glEnd();
         break;
      case 2:
         this.DrawSky();
         break;
      case 3:
         var1 = 0;
         GlUtil.glDepthMask(false);
         GlUtil.glDisable(3042);
         var10 = random.nextInt(NAME_COUNT);
         int var11 = random.nextInt(PREFIX_COUNT);
         int var13 = random.nextInt(SUFFIX_COUNT);
         GL11.glColor3f(1.0F, 1.0F, 1.0F);

         while(true) {
            if (var1 >= this._size) {
               break label125;
            }

            if (Math.random() > 0.5D) {
               this.RenderPrint(2, this._size - var1 - 8, random.nextInt(), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), "%s%s", prefix[var11], name[var10]);
            } else {
               this.RenderPrint(2, this._size - var1 - 8, random.nextInt(), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), "%s%s", name[var10], suffix[var13]);
            }

            var10 = (var10 + 1) % NAME_COUNT;
            var11 = (var11 + 1) % PREFIX_COUNT;
            var13 = (var13 + 1) % SUFFIX_COUNT;
            var1 += 32;
         }
      case 4:
         int var5 = Math.max(16, 1);
         Vector4f var2 = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
         Vector4f var3 = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);

         for(var1 = 0; var1 < this._size; var1 += 64) {
            drawrect_simple(var1 + var5, var5, var1 + 64 - var5, 64 - var5, var2, var3);
         }

         for(var1 = 0; var1 < this._size; var1 += 128) {
            drawrect_simple(var1 + var5, var5 + 64, var1 + 64 - var5, 128 - var5, var2, var3);
         }

         for(var1 = 0; var1 < this._size; var1 += 192) {
            drawrect_simple(var1 + var5, var5 + 128, var1 + 64 - var5, 192 - var5, var2, var3);
         }

         var1 = 0;

         while(true) {
            if (var1 >= this._size) {
               break label125;
            }

            drawrect_simple(var1 + var5, 192 + (var5 << 1), var1 + 64 - var5, 256 - var5, var2, var3);
            var1 += 64;
         }
      case 5:
      default:
         GlUtil.glDepthMask(false);
         this.DrawWindows();
         break;
      case 6:
         this.DrawHeadlight();
         break;
      case 7:
         System.err.println("LATTICE is " + this._glid);
         GL11.glLineWidth(2.0F);
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GL11.glBegin(1);
         GL11.glVertex2i(0, 0);
         GL11.glVertex2i(this._size, this._size);
         GL11.glVertex2i(0, 0);
         GL11.glVertex2i(0, this._size);
         GL11.glVertex2i(0, 0);
         GL11.glVertex2i(this._size, 0);
         GL11.glEnd();
         GL11.glBegin(3);
         GL11.glVertex2i(0, 0);

         for(var1 = 0; var1 < this._size; var1 += 9) {
            if (var1 % 2 != 0) {
               GL11.glVertex2i(0, var1);
            } else {
               GL11.glVertex2i(var1, var1);
            }
         }

         for(var1 = 0; var1 < this._size; var1 += 9) {
            if (var1 % 2 != 0) {
               GL11.glVertex2i(var1, 0);
            } else {
               GL11.glVertex2i(var1, var1);
            }
         }

         GL11.glEnd();
      }

      GL11.glPopMatrix();
      GlUtil.glBindTexture(3553, this._glid);
      GL11.glCopyTexImage2D(3553, 0, 6408, 0, 0, this._size, this._size, 0);
      GL11.glCopyTexImage2D(3553, 0, 6408, 0, 0, this._size, this._size, 0);
      var10000 = this._my_id;
      GL11.glTexParameteri(3553, 10240, 9728);
      GL11.glViewport(viewport.get(0), viewport.get(1), viewport.get(2), viewport.get(3));
      GlUtil.glEnable(2929);
      GlUtil.glDisable(3553);
      GlUtil.glDisable(3042);
      GlUtil.glDepthMask(true);
      GlUtil.glEnable(2896);
      GL11.glPolygonMode(1028, 6914);
      GL11.glClearColor(0.5F, 0.5F, 0.5F, 1.0F);
      this._ready = true;
      long var8 = System.currentTimeMillis() - var6;
      build_time = (int)((long)build_time + var8);
      GlUtil.printGlErrorCritical();
   }

   private int RenderMaxTextureSize() {
      return 512;
   }

   private void RenderPrint(int var1, int var2, int var3, Vector4f var4, String var5, String var6, String var7) {
      System.err.println("RenderPrint not implemented");
   }

   public int TextureRandomBuilding(int var1) {
      return TextureId(Math.abs(var1) % 9 + 8);
   }

   boolean TextureReady() {
      return textures_done;
   }

   void TextureReset() {
      textures_done = false;
      build_time = 0;
      BuildingTexture var1 = head;

      while(true) {
         var1.cleanUp();
         var1 = var1._next;
      }
   }

   void TextureTerm() {
      while(head != null) {
         BuildingTexture var1 = head._next;
         System.err.println("impement free");
         head = var1;
      }

   }

   static {
      PREFIX_COUNT = (prefix = new String[]{"i", "Green ", "Mega", "Super ", "Omni", "e", "Hyper", "Global ", "Vital", "Next ", "Pacific ", "Metro", "Unity ", "G-", "Trans", "Infinity ", "Superior ", "Monolith ", "Best ", "Atlantic ", "First ", "Union ", "National "}).length;
      NAME_COUNT = (name = new String[]{"Biotic", "Info", "Data", "Solar", "Aerospace", "Motors", "Nano", "Online", "Circuits", "Energy", "Med", "Robotic", "Exports", "Security", "Systems", "Financial", "Industrial", "Media", "Materials", "Foods", "Networks", "Shipping", "Tools", "Medical", "Publishing", "Enterprises", "Audio", "Health", "Bank", "Imports", "Apparel", "Petroleum", "Studios"}).length;
      SUFFIX_COUNT = (suffix = new String[]{"Corp", " Inc.", "Co", "PlanetSurface", ".Com", " USA", " Ltd.", "Net", " Tech", " Labs", " Mfg.", " UK", " Unlimited", " One", " LLC"}).length;
      prefix_used = new boolean[PREFIX_COUNT];
      name_used = new boolean[NAME_COUNT];
      suffix_used = new boolean[SUFFIX_COUNT];
      viewport = BufferUtils.createIntBuffer(16);
      RANDOM_COLOR_SHIFT = (float)(random = new Random()).nextInt(10) / 50.0F;
      RANDOM_COLOR_VAL = (float)random.nextInt(256) / 256.0F;
      RANDOM_COLOR_LIGHT = (float)(200 + random.nextInt(56)) / 256.0F;
   }
}
