package org.schema.game.client.data.city;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.forms.BoundingBox;

public class BuildingWorld {
   public static final int WORLD_EDGE = 200;
   public static final int WORLD_SIZE = 1024;
   public static final int WORLD_HALF = 512;
   public static final int CLAIM_ROAD = 1;
   public static final int CLAIM_WALK = 2;
   public static final int CLAIM_BUILDING = 4;
   public static final int MAP_ROAD_NORTH = 8;
   public static final int MAP_ROAD_SOUTH = 16;
   public static final int MAP_ROAD_EAST = 32;
   public static final int MAP_ROAD_WEST = 64;
   static int[][] world = new int[1024][1024];
   static Random random = new Random();
   public ArrayList buildings = new ArrayList();
   public ArrayList decos = new ArrayList();
   private int modern_count;
   private int skyscrapers;
   private int tower_count;
   private int blocky_count;
   private int logo_index;
   private int scene_begin;
   private BoundingBox hot_zone;

   static int build_light_strip(int var0, int var1, int var2) {
      byte var7 = 0;
      byte var8 = 0;
      Color var3 = Color.getHSBColor(0.09F, 0.99F, 0.85F);
      Vector4f var4 = new Vector4f((float)var3.getRed(), (float)var3.getGreen(), (float)var3.getBlue(), (float)var3.getAlpha());
      switch(var2) {
      case 0:
         var8 = 1;
         var7 = 0;
         break;
      case 1:
         var8 = 1;
         var7 = 0;
         break;
      case 2:
         var8 = 0;
         var7 = 1;
         break;
      case 3:
         var8 = 0;
         var7 = 1;
      }

      int var9 = var0;
      int var5 = var1;

      int var6;
      for(var6 = 0; var9 > 0 && var9 < 1024 && var5 > 0 && var5 < 1024 && (world[var9][var5] & 1) == 0; var5 += var8) {
         ++var6;
         var9 += var7;
      }

      if (var6 < 10) {
         return var6;
      } else {
         int var11 = Math.max(Math.abs(var9 - var0), 1);
         var5 = Math.max(Math.abs(var5 - var1), 1);
         CDeco var10 = new CDeco();
         if (var2 == 2) {
            var10.CreateLightStrip((float)var0, (float)var1 - 2.5F, (float)var11, (float)var5 + 2.5F, 2.0F, var4);
         } else if (var2 == 3) {
            var10.CreateLightStrip((float)var0, (float)var1, (float)var11, (float)var5 + 2.5F, 2.0F, var4);
         } else if (var2 == 0) {
            var10.CreateLightStrip((float)var0, (float)var1, (float)var11 + 2.5F, (float)var5, 2.0F, var4);
         } else {
            var10.CreateLightStrip((float)var0 - 2.5F, (float)var1, (float)var11 + 2.5F, (float)var5, 2.0F, var4);
         }

         return var6;
      }
   }

   static void build_road(int var0, int var1, int var2, int var3) {
      int var4;
      if (var2 > var3) {
         var4 = var3;
      } else {
         var4 = var2;
      }

      if (var4 >= 4) {
         byte var5;
         if (var4 % 2 != 0) {
            --var4;
            var5 = 1;
         } else {
            var5 = 0;
         }

         int var6 = Math.max(2, var4 - 10);
         var4 -= var6;
         var6 /= 2;
         var4 /= 2;
         claim(var0, var1, var2, var3, 2);
         if (var2 > var3) {
            claim(var0, var1 + var6, var2, var4, 65);
            claim(var0, var1 + var6 + var4 + var5, var2, var4, 33);
         } else {
            claim(var0 + var6, var1, var4, var3, 17);
            claim(var0 + var6 + var4 + var5, var1, var4, var3, 9);
         }
      }
   }

   static void claim(int var0, int var1, int var2, int var3, int var4) {
      for(int var5 = var0; var5 < var0 + var2; ++var5) {
         for(int var6 = var1; var6 < var1 + var3; ++var6) {
            int[] var10000 = world[CLAMP(var5, 0, 1023)];
            int var10001 = CLAMP(var6, 0, 1023);
            var10000[var10001] |= var4;
         }
      }

   }

   static boolean claimed(int var0, int var1, int var2, int var3) {
      for(int var4 = var0; var4 < var0 + var2; ++var4) {
         for(int var5 = var1; var5 < var1 + var3; ++var5) {
            if (world[CLAMP(var4, 0, 1023)][CLAMP(var5, 0, 1023)] != 0) {
               return true;
            }
         }
      }

      return false;
   }

   public static int CLAMP(int var0, int var1, int var2) {
      if (var0 < var1) {
         return var1;
      } else {
         return var0 > var2 ? var2 : var0;
      }
   }

   public static boolean coinflip() {
      return Math.random() > 0.5D;
   }

   static Plot find_plot(int var0, int var1) {
      Plot var2 = new Plot(0, 0, 0, 0);
      int var4 = var0;

      int var3;
      for(var3 = var0; !claimed(var3 - 1, var1, 1, 1) && var3 > 0; --var3) {
      }

      while(!claimed(var4 + 1, var1, 1, 1) && var4 < 1024) {
         ++var4;
      }

      int var5 = var1;

      for(var1 = var1; !claimed(var0, var1 - 1, 1, 1) && var1 > 0; --var1) {
      }

      while(!claimed(var0, var5 + 1, 1, 1) && var5 < 1024) {
         ++var5;
      }

      var2.width = var4 - var3;
      var2.depth = var5 - var1;
      var2.x = var3;
      var2.z = var1;
      return var2;
   }

   static Plot make_plot(int var0, int var1, int var2, int var3) {
      return new Plot(var0, var1, var2, var3);
   }

   void do_building(Plot var1) {
      int var2 = var1.width * var1.depth;
      Vector4f var5 = new Vector4f(0.9F, 0.9F, 0.9F, 1.0F);
      int var3 = random.nextInt();
      if (var1.width >= 10 && var1.depth >= 10) {
         if (var2 > 800) {
            if (coinflip()) {
               var1.width /= 2;
               if (coinflip()) {
                  this.do_building(make_plot(var1.x, var1.z, var1.width, var1.depth));
               } else {
                  this.do_building(make_plot(var1.x + var1.width, var1.z, var1.width, var1.depth));
               }
            } else {
               var1.depth /= 2;
               if (coinflip()) {
                  this.do_building(make_plot(var1.x, var1.z, var1.width, var1.depth));
               } else {
                  this.do_building(make_plot(var1.x, var1.z + var1.depth, var1.width, var1.depth));
               }
            }
         } else if (var2 >= 100) {
            boolean var6 = Math.abs(var1.width - var1.depth) < 10;
            claim(var1.x, var1.z, var1.width, var1.depth, 4);
            if (var6 && var1.width > 20) {
               var2 = 45 + random.nextInt(10);
               ++this.modern_count;
               ++this.skyscrapers;
               this.buildings.add(new Building(1, var1.x, var1.z, var2, var1.width, var1.depth, var3, var5));
            } else {
               byte var4;
               if (this.tower_count < this.modern_count && this.tower_count < this.blocky_count) {
                  var4 = 2;
                  ++this.tower_count;
               } else if (this.blocky_count < this.modern_count) {
                  var4 = 3;
                  ++this.blocky_count;
               } else {
                  var4 = 1;
                  ++this.modern_count;
               }

               var2 = 45 + random.nextInt(10);
               this.buildings.add(new Building(var4, var1.x, var1.z, var2, var1.width, var1.depth, var3, var5));
               ++this.skyscrapers;
            }
         }
      }
   }

   public void do_reset() {
      float var6 = 0.0F;
      float var7 = 0.0F;
      float var8 = 0.0F;
      float var9 = 0.0F;
      boolean var5 = false;
      this.skyscrapers = 0;
      this.logo_index = 0;
      this.scene_begin = 0;
      this.tower_count = this.blocky_count = this.modern_count = 0;

      int var2;
      int var4;
      for(var2 = 200; var2 < 824; var2 += random.nextInt(25) + 25) {
         if (!var5 && var2 > 492) {
            build_road(0, var2, 1024, 19);
            var2 += 20;
            var5 = true;
         } else {
            var4 = 6 + random.nextInt(6);
            if (var2 < 256) {
               var7 = (float)(var2 + var4 / 2);
            }

            if (var2 < 768) {
               var9 = (float)(var2 + var4 / 2);
            }

            build_road(0, var2, 1024, var4);
         }
      }

      var5 = false;

      int var1;
      int var3;
      for(var1 = 200; var1 < 824; var1 += random.nextInt(25) + 25) {
         if (!var5 && var1 > 492) {
            build_road(var1, 0, 19, 1024);
            var1 += 20;
            var5 = true;
         } else {
            var3 = 6 + random.nextInt(6);
            if (var1 <= 256) {
               var6 = (float)(var1 + var3 / 2);
            }

            if (var1 <= 768) {
               var8 = (float)(var1 + var3 / 2);
            }

            build_road(var1, 0, var3, 1024);
         }
      }

      this.hot_zone = new BoundingBox(new Vector3f(var6, 0.0F, var7), new Vector3f(var8, 0.0F, var9));

      boolean var10;
      boolean var11;
      for(var1 = 1; var1 < 1023; ++var1) {
         for(var2 = 0; var2 < 1024; ++var2) {
            if ((world[var1][var2] & 2) != 0 && (world[var1][var2] & 1) == 0) {
               var10 = (world[var1 + 1][var2] & 1) != 0;
               var11 = (world[var1 - 1][var2] & 1) != 0;
               if ((var10 || var11) && (!var10 || !var11)) {
                  var2 += build_light_strip(var1, var2, var11 ? 1 : 0);
               }
            }
         }
      }

      for(var2 = 1; var2 < 1023; ++var2) {
         for(var1 = 1; var1 < 1023; ++var1) {
            if ((world[var1][var2] & 2) != 0 && (world[var1][var2] & 1) == 0) {
               var10 = (world[var1][var2 + 1] & 1) != 0;
               var11 = (world[var1][var2 - 1] & 1) != 0;
               if ((!var10 || !var11) && (var10 || var11)) {
                  var1 += build_light_strip(var1, var2, var11 ? 2 : 3);
               }
            }
         }
      }

      for(var3 = 0; this.skyscrapers < 50 && var3 < 350; ++var3) {
         var1 = 256 + random.nextInt() % 512;
         var2 = 256 + random.nextInt() % 512;
         if (!claimed(var1, var2, 1, 1)) {
            this.do_building(find_plot(var1, var2));
            ++this.skyscrapers;
         }
      }

      for(var1 = 0; var1 < 1024; ++var1) {
         for(var2 = 0; var2 < 1024; ++var2) {
            if (world[CLAMP(var1, 0, 1024)][CLAMP(var2, 0, 1024)] == 0) {
               var3 = 12 + random.nextInt(20);
               var4 = 12 + random.nextInt(20);
               int var12 = Math.min(var3, var4);
               if (var1 >= 30 && var2 >= 30 && var1 <= 994 && var2 <= 994) {
                  if (var1 < 256) {
                     var12 /= 2;
                  }
               } else {
                  var12 = random.nextInt(15) + 20;
               }

               while(var3 > 8 && var4 > 8) {
                  if (!claimed(var1, var2, var3, var4)) {
                     claim(var1, var2, var3, var4, 4);
                     Vector4f var13 = new Vector4f(0.5F, 0.5F, 0.5F, 1.0F);
                     if ((float)var1 >= this.hot_zone.min.x && (float)var1 <= this.hot_zone.max.x && (float)var2 >= this.hot_zone.min.z && (float)var2 <= this.hot_zone.max.z) {
                        var12 = 15 + random.nextInt(15);
                        var3 -= 2;
                        var4 -= 2;
                        if (coinflip()) {
                           this.buildings.add(new Building(2, var1 + 1, var2 + 1, var12, var3, var4, random.nextInt(), var13));
                        } else {
                           this.buildings.add(new Building(3, var1 + 1, var2 + 1, var12, var3, var4, random.nextInt(), var13));
                        }
                     } else {
                        var12 = 5 + random.nextInt(var12) + random.nextInt(var12);
                        this.buildings.add(new Building(0, var1 + 1, var2 + 1, var12, var3 - 2, var4 - 2, random.nextInt(), var13));
                     }
                     break;
                  }

                  --var3;
                  --var4;
               }

               if (var2 < 200 || var2 > 824) {
                  var2 += 32;
               }
            }
         }

         if (var1 < 200 || var1 > 824) {
            var1 += 28;
         }
      }

   }

   BoundingBox WorldHotZone() {
      return this.hot_zone;
   }

   int WorldLogoIndex() {
      return this.logo_index++;
   }

   int WorldSceneBegin() {
      return this.scene_begin;
   }
}
