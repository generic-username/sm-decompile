package org.schema.game.client.data.city;

import java.util.ArrayList;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.GlUtil;

public class CDeco {
   private static final int TRIM_ROWS = 3;
   private static final float TRIM_SIZE = 3.0F;
   private static final float LOGO_OFFSET = 1.0F;
   private Vector3f center;
   private ArrayList vertices = new ArrayList();
   private ArrayList texCoords = new ArrayList();
   private ArrayList normals = new ArrayList();
   private boolean use_alpha;
   private int texture;

   public void CreateLightStrip(float var1, float var2, float var3, float var4, float var5, Vector4f var6) {
      this.use_alpha = true;
      this.center = new Vector3f(var1 + var3 / 2.0F, var5, var2 + var4 / 2.0F);
      float var7;
      float var8;
      if (var3 < var4) {
         var8 = 1.0F;
         var7 = (float)((int)(var4 / var3));
      } else {
         var7 = 1.0F;
         var8 = (float)((int)(var3 / var4));
      }

      this.texture = BuildingTexture.TextureId(0);
      this.vertices.add(new Vector3f(var1, var5, var2));
      this.texCoords.add(new Vector2f(0.0F, 0.0F));
      this.vertices.add(new Vector3f(var1, var5, var2 + var4));
      this.texCoords.add(new Vector2f(0.0F, var7));
      this.vertices.add(new Vector3f(var1 + var3, var5, var2 + var4));
      this.texCoords.add(new Vector2f(var8, var7));
      this.vertices.add(new Vector3f(var1 + var3, var5, var2 + var4));
      this.texCoords.add(new Vector2f(var8, var7));
      this.vertices.add(new Vector3f(var1 + var3, var5, var2));
      this.texCoords.add(new Vector2f(var8, 0.0F));
      this.vertices.add(new Vector3f(var1, var5, var2 + var4));
      this.texCoords.add(new Vector2f(0.0F, var7));
   }

   public void CreateLightTrim(Vector3f[] var1, int var2, float var3, int var4, Vector4f var5) {
      ArrayList var16 = new ArrayList();
      Vector3f var6 = new Vector3f();
      this.center = new Vector3f(0.0F, 0.0F, 0.0F);

      int var8;
      for(var8 = 0; var8 < var2; ++var8) {
         this.center.add(var1[var8]);
      }

      this.center.scale(1.0F / (float)var2);
      float var14;
      float var10 = (var14 = (float)(var4 % 3)) * 3.0F;
      float var11 = (var14 + 1.0F) * 3.0F;
      float var9 = 0.0F;

      for(var8 = 0; var8 < var2 + 1; ++var8) {
         Vector3f var12;
         if (var8 != 0) {
            (var12 = new Vector3f(var1[var8 % var2])).sub(var6);
            var9 += var12.length() * 0.1F;
         }

         int var17;
         if ((var17 = var8 - 1) < 0) {
            var17 += var2;
         }

         var4 = (var8 + 1) % var2;
         Vector3f var15;
         (var15 = new Vector3f(var1[var4])).sub(var1[var17]);
         var15.normalize();
         var12 = new Vector3f(0.0F, 1.0F, 0.0F);
         Vector3f var7;
         (var7 = new Vector3f()).cross(var12, var15);
         var7.scale(1.0F);
         (var6 = new Vector3f(var1[var8 % var2])).add(var7);
         new Vector2f(var9, var11);
         var16.add(new Vector3f(var6));
         var6.y += var3;
         new Vector2f(var9, var10);
         var16.add(new Vector3f(var6));
      }

      this.texture = BuildingTexture.TextureId(4);
      if (var16.size() >= 2) {
         float var18 = 1.0F / (float)var16.size();

         for(int var13 = 0; var13 < var16.size() - 2; var13 += 2) {
            this.vertices.add(new Vector3f((Vector3f)var16.get(var13)));
            this.texCoords.add(new Vector2f((float)var13 * var18, 0.0F));
            this.vertices.add(new Vector3f((Vector3f)var16.get(var13 + 1)));
            this.texCoords.add(new Vector2f((float)var13 * var18, 1.0F));
            this.vertices.add(new Vector3f((Vector3f)var16.get(var13 + 2)));
            this.texCoords.add(new Vector2f((float)(var13 + 1) * var18, 0.0F));
            this.vertices.add(new Vector3f((Vector3f)var16.get(var13 + 2)));
            this.texCoords.add(new Vector2f((float)(var13 + 1) * var18, 0.0F));
            this.vertices.add(new Vector3f((Vector3f)var16.get(var13 + 1)));
            this.texCoords.add(new Vector2f((float)var13 * var18, 1.0F));
            this.vertices.add(new Vector3f((Vector3f)var16.get(var13 + 3)));
            this.texCoords.add(new Vector2f((float)(var13 + 1) * var18, 1.0F));
         }
      }

   }

   public void CreateLogo(Vector2f var1, Vector2f var2, float var3, Object var4, Vector4f var5) {
   }

   void CreateRadioTower(Vector3f var1, float var2) {
      float var3 = var2 / 15.0F;
      this.center = var1;
      this.use_alpha = true;
      this.vertices.add(new Vector3f(this.center.x, this.center.y + var2, this.center.z));
      this.texCoords.add(new Vector2f(0.0F, 1.0F));
      this.vertices.add(new Vector3f(this.center.x - var3, this.center.y, this.center.z - var3));
      this.texCoords.add(new Vector2f(1.0F, 0.0F));
      this.vertices.add(new Vector3f(this.center.x + var3, this.center.y, this.center.z - var3));
      this.texCoords.add(new Vector2f(0.0F, 0.0F));
      this.vertices.add(new Vector3f(this.center.x, this.center.y + var2, this.center.z));
      this.texCoords.add(new Vector2f(0.0F, 1.0F));
      this.vertices.add(new Vector3f(this.center.x + var3, this.center.y, this.center.z - var3));
      this.texCoords.add(new Vector2f(1.0F, 0.0F));
      this.vertices.add(new Vector3f(this.center.x + var3, this.center.y, this.center.z + var3));
      this.texCoords.add(new Vector2f(0.0F, 0.0F));
      this.vertices.add(new Vector3f(this.center.x, this.center.y + var2, this.center.z));
      this.texCoords.add(new Vector2f(0.0F, 1.0F));
      this.vertices.add(new Vector3f(this.center.x + var3, this.center.y, this.center.z + var3));
      this.texCoords.add(new Vector2f(1.0F, 0.0F));
      this.vertices.add(new Vector3f(this.center.x - var3, this.center.y, this.center.z + var3));
      this.texCoords.add(new Vector2f(0.0F, 0.0F));
      this.vertices.add(new Vector3f(this.center.x, this.center.y + var2, this.center.z));
      this.texCoords.add(new Vector2f(0.0F, 1.0F));
      this.vertices.add(new Vector3f(this.center.x - var3, this.center.y, this.center.z + var3));
      this.texCoords.add(new Vector2f(1.0F, 0.0F));
      this.vertices.add(new Vector3f(this.center.x - var3, this.center.y, this.center.z - var3));
      this.texCoords.add(new Vector2f(0.0F, 0.0F));
      new CLight(new Vector3f(this.center.x, this.center.y + var2 + 1.0F, this.center.z), new Vector4f(255.0F, 192.0F, 160.0F, 1.0F), 1);
      this.texture = BuildingTexture.TextureId(7);
      System.err.println("bunding texture to tower " + this.texture);
   }

   public void testDraw() {
      GlUtil.glEnable(2896);
      GlUtil.glBindTexture(3553, this.texture);
      GlUtil.glEnable(3553);
      if (this.use_alpha) {
         GlUtil.glBlendFunc(770, 772);
         GlUtil.glEnable(3042);
         GlUtil.glDisable(2884);
      }

      GL11.glBegin(4);

      for(int var1 = 0; var1 < this.vertices.size(); ++var1) {
         Vector3f var2 = (Vector3f)this.vertices.get(var1);
         Vector2f var3;
         GL11.glTexCoord2f((var3 = (Vector2f)this.texCoords.get(var1)).x, var3.y);
         GL11.glVertex3f(var2.x, var2.y, var2.z);
      }

      GL11.glEnd();
      GlUtil.glBindTexture(3553, 0);
      if (this.use_alpha) {
         GlUtil.glDepthMask(true);
         GlUtil.glDisable(3042);
         GlUtil.glEnable(2929);
      }

   }
}
