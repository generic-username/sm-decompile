package org.schema.game.client.data;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrayFIFOQueue;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observer;
import java.util.Set;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.BufferUtils;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.GUIController;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.controller.GameMainMenuController;
import org.schema.game.client.controller.PlayerLagStatsInput;
import org.schema.game.client.controller.manager.GlobalGameControlManager;
import org.schema.game.client.controller.manager.ingame.BlockSyleSubSlotController;
import org.schema.game.client.controller.manager.ingame.BuildToolsManager;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.view.MainGameGraphics;
import org.schema.game.client.view.Segment2ObjWriter;
import org.schema.game.client.view.SegmentDrawer;
import org.schema.game.client.view.WorldDrawer;
import org.schema.game.client.view.camera.ObjectViewerCamera;
import org.schema.game.client.view.gui.PlayerPanel;
import org.schema.game.client.view.gui.lagStats.LagDataStatsList;
import org.schema.game.common.Starter;
import org.schema.game.common.ThreadedSegmentWriter;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.RaceManagerState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SegmentDataManager;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.controller.ShopperInterface;
import org.schema.game.common.controller.activities.RaceManager;
import org.schema.game.common.controller.damage.projectile.ProjectileController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ParticleHandler;
import org.schema.game.common.controller.elements.PulseController;
import org.schema.game.common.controller.elements.PulseHandler;
import org.schema.game.common.controller.gamemodes.GameModes;
import org.schema.game.common.controller.generator.ClientCreatorThread;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.game.common.crashreporter.CrashReporter;
import org.schema.game.common.data.MetaObjectState;
import org.schema.game.common.data.SendableGameState;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.blockeffects.config.ConfigPool;
import org.schema.game.common.data.blockeffects.config.ConfigPoolProvider;
import org.schema.game.common.data.chat.ChannelRouter;
import org.schema.game.common.data.element.ControlElementMapOptimizer;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.event.EventFactory;
import org.schema.game.common.data.fleet.FleetManager;
import org.schema.game.common.data.fleet.FleetStateInterface;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.catalog.CatalogManager;
import org.schema.game.common.data.player.catalog.PlayerCatalogManager;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.player.faction.FactionRelationOffer;
import org.schema.game.common.data.world.GravityStateInterface;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.common.data.world.space.PlanetCore;
import org.schema.game.common.version.Version;
import org.schema.game.network.objects.ChatMessage;
import org.schema.game.server.data.CatalogState;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.schine.ai.stateMachines.AiEntityState;
import org.schema.schine.common.DebugTimer;
import org.schema.schine.common.TextCallback;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.ChatSystem;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.network.client.HostPortLoginName;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.server.ServerMessage;
import org.schema.schine.physics.Physics;
import org.schema.schine.physics.PhysicsState;
import org.schema.schine.resource.ResourceMap;
import org.w3c.dom.Document;

public class GameClientState extends ClientState implements GameStateInterface, SegmentManagerInterface, RaceManagerState, ParticleHandler, PulseHandler, DrawerObserver, MetaObjectState, ConfigPoolProvider, FleetStateInterface, GravityStateInterface, CatalogState, FactionState, PhysicsState {
   public static final ByteArrayOutputStream SEGMENT_BYTE_ARRAY_BUFFER = new ByteArrayOutputStream(102400);
   private static final String[] prefixes = new String[]{"//", "/"};
   public static int SERVER_BLOCK_QUEUE_SIZE = -1;
   public static long totalMemory;
   public static long freeMemory;
   public static long takenMemory;
   public static int allocatedSegmentData;
   public static int lastAllocatedSegmentData;
   public static int lastFreeSegmentData;
   public static int requestQueue;
   public static int requestedSegments;
   public static int returnedRequests;
   public static int clientCreatorThreadIterations;
   public static Long dataReceived = new Long(0L);
   private final List dockingListeners;
   private final List powerChangeListeners;
   private final EventFactory eventFactory;
   private final Long2ObjectOpenHashMap playerStatesByDbId;
   public final ObjectArrayList laggyList;
   public static int singleplayerCreativeMode = -1;
   public static final int CREATIVE_MODE_ON = 1;
   public static final int CREATIVE_MODE_OFF = 2;
   public static int collectionUpdates;
   public static GameClientState instance;
   public static int drawnSegements;
   public static int realVBOSize;
   public static int prospectedVBOSize;
   public static int debugSelectedObject = -1;
   public static float avgBlockLightTime;
   public static float avgBlockLightLockTime;
   public static int staticSector;
   public static boolean instanced;
   public static boolean smoothDisableDebug;
   public Segment2ObjWriter exportingShip;
   private final MetaObjectManager metaObjectManager;
   private final Object2ObjectOpenHashMap loadedSectors;
   private final Map onlinePlayersLowerCaseMap;
   private final ClientMessageLog messageLog;
   private final IntArrayFIFOQueue toRequestMetaObjects;
   private final ThreadedSegmentWriter threadedSegmentWriter;
   private final ArrayList serverMessages;
   private final HashSet flaggedAddedObjects;
   private final HashSet flaggedRemovedObjects;
   private final HashSet needsNotifyObjects;
   private final HashSet otherSendables;
   private final ChannelRouter channelRouter;
   private final ArrayList needsNotify;
   private final AiEntityState tutorialAIState;
   private final ObjectArrayFIFOQueue bufferPool;
   private final Int2ObjectOpenHashMap currentSectorEntities;
   private final ObjectArrayList currentGravitySources;
   private final Short2ObjectOpenHashMap techs;
   private final Object2ObjectOpenHashMap surrounding;
   public String receivedBlockConfigPropertiesChecksum;
   public String receivedBlockConfigChecksum;
   public String receivedFactionConfigChecksum;
   public String receivedBlockBehaviorChecksum;
   public SegmentController currentEnterTry;
   public long currentEnterTryTime;
   public String receivedCustomTexturesChecksum;
   public long updateTime;
   public List spotlights;
   byte[] buffer;
   boolean wasInShopDistance;
   private Document blockBehaviorConfig;
   private float highestSubStep;
   private Physics physics;
   private WorldDrawer worldDrawer;
   private GameClientController controller;
   private GLFrame glFrame;
   private MainGameGraphics scene;
   private GUIController guiController;
   private GlobalGameControlManager globalGameControlManager;
   private Map requestingSegments;
   private SendableGameState gameState;
   private long lastServerTimeRequest;
   private Ship ship;
   private PlayerState player;
   private SimpleTransformableSendableObject currentPlayerObject;
   private PlayerCharacter character;
   private ChatSystem chat;
   private EditableSendableSegmentController planetTestSurface;
   private SegmentDataManager segmentDataManager;
   private String playerName;
   private boolean waitingForPlayerActivate;
   private GameModes gameMode;
   private boolean requestServerTimeFlag;
   private boolean dbPurgeRequested;
   private Integer initialSectorId;
   private Vector3i initialSectorPos;
   private int flagSectorChange;
   private ProjectileController particleController;
   private PulseController pulseController;
   private ShopInterface currentClosestShop;
   private boolean playerSpawned;
   private boolean flagPlayerReceived;
   private String configPropertiesCheckSum;
   private String configCheckSum;
   private String factionConfigCheckSum;
   private boolean warped;
   private short warpedUpdateNr;
   private boolean physicalAsteroids;
   private int hinderedInput;
   private long hinderedInputTime;
   private String blockBehaviorCheckSum;
   private Vector3i tmpStellar;
   private Vector3i tmpGalaxy;
   private Galaxy currentGalaxy;
   private String customTexturesCheckSum;
   private boolean synched;
   private ControlElementMapOptimizer controlOptimizer;
   public final LagDataStatsList lagStats;
   private final FleetManager fleetManager;
   public final ObjectArrayFIFOQueue unloadedInventoryUpdates;
   private List activeSubtitles;
   private final BlockSyleSubSlotController blockSyleSubSlotController;
   private Exception currentE;
   private Vector3i lastSector;
   private VoidSystem cached;
   private final DebugTimer debugTimer;

   public GameClientState() {
      this(false);
   }

   public GameClientState(boolean var1) {
      super(var1);
      this.dockingListeners = new ObjectArrayList();
      this.powerChangeListeners = new ObjectArrayList();
      this.playerStatesByDbId = new Long2ObjectOpenHashMap();
      this.laggyList = new ObjectArrayList();
      this.loadedSectors = new Object2ObjectOpenHashMap();
      this.onlinePlayersLowerCaseMap = new Object2ObjectOpenHashMap();
      this.messageLog = new ClientMessageLog();
      this.toRequestMetaObjects = new IntArrayFIFOQueue();
      this.threadedSegmentWriter = new ThreadedSegmentWriter("CLIENT");
      this.serverMessages = new ArrayList();
      this.flaggedAddedObjects = new HashSet();
      this.flaggedRemovedObjects = new HashSet();
      this.needsNotifyObjects = new HashSet();
      this.otherSendables = new HashSet();
      this.needsNotify = new ArrayList();
      this.bufferPool = new ObjectArrayFIFOQueue(5);
      this.currentSectorEntities = new Int2ObjectOpenHashMap();
      this.currentGravitySources = new ObjectArrayList();
      this.techs = new Short2ObjectOpenHashMap();
      this.surrounding = new Object2ObjectOpenHashMap();
      this.spotlights = new ObjectArrayList();
      this.buffer = new byte[1048576];
      this.wasInShopDistance = false;
      this.gameMode = null;
      this.initialSectorId = -2;
      this.initialSectorPos = new Vector3i(-1, -1, -1);
      this.flagSectorChange = -1;
      this.tmpStellar = new Vector3i();
      this.tmpGalaxy = new Vector3i();
      this.controlOptimizer = new ControlElementMapOptimizer();
      this.lagStats = new LagDataStatsList(new Vector4f(1.0F, 0.0F, 0.0F, 1.0F));
      this.unloadedInventoryUpdates = new ObjectArrayFIFOQueue();
      this.blockSyleSubSlotController = new BlockSyleSubSlotController();
      this.lastSector = new Vector3i(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
      this.debugTimer = new DebugTimer();

      for(int var3 = 0; var3 < 5; ++var3) {
         ByteBuffer var2 = BufferUtils.createByteBuffer(1048576);
         this.bufferPool.enqueue(var2);
      }

      this.segmentDataManager = new SegmentDataManager(this);
      this.requestingSegments = new HashMap();
      this.metaObjectManager = new MetaObjectManager(this);
      this.tutorialAIState = new AiEntityState("tutorialAIState", this);
      this.channelRouter = new ChannelRouter(this);
      this.fleetManager = new FleetManager(this);
      ManagerContainer.onClientStartStatic();
      instance = this;
      this.blockSyleSubSlotController.load();
      this.eventFactory = new EventFactory(this);
   }

   public static boolean isDebugObject(SimpleGameObject var0) {
      return var0 != null && debugSelectedObject > 0 && var0.getAsTargetId() == debugSelectedObject;
   }

   public static boolean isCreated() {
      return instanced;
   }

   public synchronized void addObserver(Observer var1) {
      super.addObserver(var1);
   }

   public void addRequestingSegment(short var1, RemoteSegment var2) {
      synchronized(this.requestingSegments) {
         assert !this.requestingSegments.containsKey(var1) : "double packetId";

         this.requestingSegments.put(var1, var2);
      }
   }

   public SimpleTransformableSendableObject getSelectedEntity() {
      return this.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity();
   }

   public void chat(ChatSystem var1, String var2, String var3, boolean var4) {
      Vector4f var5 = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);

      String var6;
      try {
         var6 = this.getPlayerFromClientId((long)var1.getOwnerStateId()).getName();
      } catch (PlayerNotFountException var9) {
         var6 = "unknown(" + var1.getOwnerStateId() + ")";
      }

      ClientMessageLogEntry var7;
      String var10;
      if (var3.startsWith("[PM to")) {
         try {
            var10 = var3.replaceAll("\\[PM to", "").replaceAll("\\]", "").trim();
            System.err.println("PM SENDING FROM: " + var6 + " to " + var10);
            var7 = new ClientMessageLogEntry(this.getPlayerName(), var10, var2, System.currentTimeMillis(), ClientMessageLogType.CHAT_PRIVATE_SEND);
            this.getMessageLog().log(var7);
            this.getMessageLog().logPrivate(var10, var7);
         } catch (Exception var8) {
            var8.printStackTrace();
         }
      } else if (var3.startsWith("[PM]")) {
         var10 = var2.replaceFirst("\\[", "").replaceFirst("\\]", " ").split(" ")[0].trim();
         var7 = new ClientMessageLogEntry(var6, this.getPlayerName(), var2, System.currentTimeMillis(), ClientMessageLogType.CHAT_PRIVATE);
         this.getMessageLog().log(var7);
         System.err.println("PM RECEIVED: " + var3 + "; " + var10 + " " + var6 + "; " + this.getPlayerName());
         this.getMessageLog().logPrivate(var10, var7);
         var5.set(0.5F, 0.5F, 1.0F, 1.0F);
      } else {
         ClientMessageLogEntry var11;
         if (var3.startsWith("[FACTION]")) {
            var11 = new ClientMessageLogEntry(var6, this.getPlayerName(), var2, System.currentTimeMillis(), ClientMessageLogType.CHAT_FACTION);
            this.getMessageLog().log(var11);
            this.getMessageLog().logFaction(var11);
            var5.set(0.5F, 1.0F, 0.5F, 1.0F);
         } else if (!var3.startsWith("[SERVER]") && !var3.startsWith("[MESSAGE]")) {
            var11 = new ClientMessageLogEntry(var6, this.getPlayerName(), var2, System.currentTimeMillis(), ClientMessageLogType.CHAT_PUBLIC);
            this.getMessageLog().log(var11);
         } else {
            var11 = new ClientMessageLogEntry(var6, this.getPlayerName(), var2, System.currentTimeMillis(), ClientMessageLogType.INFO);
            this.getMessageLog().log(var11);
            var5.set(1.0F, 0.5F, 0.5F, 1.0F);
         }
      }

      StringBuilder var14 = new StringBuilder();
      if (var3.length() > 0) {
         var14.append(var3).append(" ");
      }

      if (var4) {
         var14.append(var6).append(": ");
      }

      var14.append(var2);
      var10 = StringTools.wrap(var14.toString(), 56);
      System.err.println("[CLIENT][CHAT] " + var10);
      ChatMessage var13;
      (var13 = new ChatMessage()).receiverType = ChatMessage.ChatMessageType.SYSTEM;
      var13.receiver = this.playerName;
      if (var4) {
         var13.sender = var6;
      } else {
         var13.sender = "";
      }

      var13.text = var2;
      var13.reset();
      this.getGeneralChatLog().add(var13);
      List var12;
      (var12 = this.getVisibleChatLog()).add(0, new ChatMessage(var13));

      while(var12.size() > 8) {
         var12.remove(var12.size() - 1);
      }

   }

   public ChatSystem getChat() {
      return this.chat;
   }

   public String[] getCommandPrefixes() {
      return prefixes;
   }

   public byte[] getDataBuffer() {
      return this.buffer;
   }

   public ByteBuffer getDataByteBuffer() {
      synchronized(this.bufferPool) {
         while(this.bufferPool.isEmpty()) {
            try {
               this.bufferPool.wait(20000L);
            } catch (InterruptedException var3) {
               var3.printStackTrace();
            }
         }

         return (ByteBuffer)this.bufferPool.dequeue();
      }
   }

   public String getVersion() {
      return Version.VERSION;
   }

   public void needsNotify(Sendable var1) {
      synchronized(this.needsNotifyObjects) {
         this.needsNotifyObjects.add(var1);
      }
   }

   public void notifyOfAddedObject(Sendable var1) {
      SegmentController var2;
      if (var1 instanceof SegmentController && (var2 = (SegmentController)var1).getCreatorThread() == null) {
         var2.setCreatorThread(new ClientCreatorThread(var2));
      }

      synchronized(this.flaggedAddedObjects) {
         this.flaggedAddedObjects.add(var1);
      }
   }

   public void notifyOfRemovedObject(Sendable var1) {
      synchronized(this.flaggedRemovedObjects) {
         this.flaggedRemovedObjects.add(var1);
      }
   }

   public String onAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
      return this.controller.onAutoComplete(var1, var2, var3);
   }

   public boolean onChatTextEnterHook(ChatSystem var1, String var2, boolean var3) {
      int var4;
      int var5;
      String[] var8;
      if (var2.startsWith("/offer ") && this.getPlayer().getNetworkObject().isAdminClient.get()) {
         try {
            var4 = Integer.parseInt((var8 = var2.split(" "))[1]);
            var5 = Integer.parseInt(var8[2]);
            this.getFactionManager().sendRelationshipOffer("admin", var4, var5, FactionRelation.getRelationFromString(var8[3]), "AUTOMATED TEST", false);
         } catch (Exception var7) {
            var7.printStackTrace();
         }
      }

      if (var2.startsWith("/accept ") && this.getPlayer().getNetworkObject().isAdminClient.get()) {
         try {
            var4 = Integer.parseInt((var8 = var2.split(" "))[1]);
            var5 = Integer.parseInt(var8[2]);
            FactionRelationOffer var9 = (FactionRelationOffer)this.getFactionManager().getRelationShipOffers().get(FactionRelationOffer.getOfferCode(var4, var5));
            this.getFactionManager().sendRelationshipAccept("admin", var9, true);
         } catch (Exception var6) {
            var6.printStackTrace();
         }
      }

      Faction var10;
      if (var2.startsWith("/f ") && (var10 = this.getFactionManager().getFaction(this.getPlayer().getFactionId())) != null) {
         String var11 = var2.substring(3);
         Iterator var13 = var10.getMembersUID().keySet().iterator();

         while(var13.hasNext()) {
            String var12;
            if (!(var12 = (String)var13.next()).equals(this.player.getName())) {
               var1.whisper(var12, var11, false, (byte)1);
            }
         }

         var1.addToVisibleChat(var11, "[FACTION][" + this.player.getName() + "] ", false);
         return true;
      } else {
         return false;
      }
   }

   public void onStringCommand(String var1, TextCallback var2, String var3) {
      this.controller.onStringCommand(var1, var2, var3);
   }

   public void releaseDataByteBuffer(ByteBuffer var1) {
      synchronized(this.bufferPool) {
         this.bufferPool.enqueue(var1);
         this.bufferPool.notify();
      }
   }

   public ResourceMap getResourceMap() {
      return Controller.getResLoader().getResourceMap();
   }

   public long getUpdateTime() {
      return this.updateTime;
   }

   public void setSynched() {
      assert !this.synched : this.printLast();

      this.synched = true;
   }

   private String printLast() {
      if (this.currentE != null) {
         System.err.println("CURRENTLY SYNCHED BY:");
         this.currentE.printStackTrace();
      }

      return this.currentE != null ? this.currentE.getMessage() : "null";
   }

   public void setUnsynched() {
      assert this.synched;

      this.synched = false;
   }

   public boolean isSynched() {
      return this.synched;
   }

   public long getUploadBlockSize() {
      return 256L;
   }

   public void setChat(ChatSystem var1) {
      this.chat = var1;
   }

   public void doUpdates() {
      if (!this.needsNotify.isEmpty()) {
         while(!this.needsNotify.isEmpty()) {
            synchronized(this.needsNotify) {
               DrawerObservable var2 = (DrawerObservable)this.needsNotify.remove(0);
               this.notifyObservers(var2);
            }
         }
      }

   }

   public void flagRequestServerTime() {
      this.requestServerTimeFlag = true;
   }

   public void flagWarped() {
      this.warped = true;
      this.warpedUpdateNr = this.getNumberOfUpdate();
   }

   public RemoteSegment getAndRemoveRequestingSegment(short var1) {
      synchronized(this.requestingSegments) {
         return (RemoteSegment)this.requestingSegments.remove(var1);
      }
   }

   public PlayerCatalogManager getCatalog() {
      return this.getPlayer().getCatalog();
   }

   public CatalogManager getCatalogManager() {
      return this.gameState.getCatalogManager();
   }

   public PlayerCharacter getCharacter() {
      return this.character;
   }

   public void setCharacter(PlayerCharacter var1) {
      this.character = var1;
   }

   public String getBlockConfigCheckSum() {
      return this.configCheckSum;
   }

   public String getConfigPropertiesCheckSum() {
      return this.configPropertiesCheckSum;
   }

   public void setConfigPropertiesCheckSum(String var1) {
      this.configPropertiesCheckSum = var1;
   }

   public GameClientController getController() {
      return this.controller;
   }

   public String getPlayerName() {
      return this.playerName;
   }

   public void setPlayerName(String var1) {
      this.playerName = var1;
   }

   public void message(Object[] var1, Integer var2) {
      ServerMessage var4 = new ServerMessage(var1, var2);
      synchronized(this.getServerMessages()) {
         this.getServerMessages().add(var4);
      }
   }

   public boolean allowMouseWheel() {
      PlayerInteractionControlManager var1;
      BuildToolsManager var2;
      boolean var3 = (var2 = (var1 = this.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager()).getBuildToolsManager()).getCopyArea() != null && var2.isPasteMode() && var1.isTreeActive();
      return !PlayerPanel.mouseInInfoScroll && !var3 && (EngineSettings.S_ZOOM_MOUSEWHEEL.getCurrentState().equals("ZOOM") || Keyboard.isKeyDown(KeyboardMappings.SCROLL_MOUSE_ZOOM.getMapping()) || Controller.getCamera() != null && Controller.getCamera() instanceof ObjectViewerCamera);
   }

   public String getClientVersion() {
      return Version.VERSION;
   }

   public void setController(GameClientController var1) {
      this.controller = var1;
   }

   public ShopInterface getCurrentClosestShop() {
      return this.currentClosestShop;
   }

   public ObjectArrayList getCurrentGravitySources() {
      return this.currentGravitySources;
   }

   public SimpleTransformableSendableObject getCurrentPlayerObject() {
      return this.currentPlayerObject;
   }

   public void setCurrentPlayerObject(SimpleTransformableSendableObject var1) {
      this.currentPlayerObject = var1;
   }

   public RemoteSector getCurrentRemoteSector() {
      Sendable var1;
      return (var1 = (Sendable)this.getLocalAndRemoteObjectContainer().getLocalObjects().get(this.getPlayer().getCurrentSectorId())) != null && var1 instanceof RemoteSector ? (RemoteSector)var1 : null;
   }

   public Int2ObjectOpenHashMap getCurrentSectorEntities() {
      return this.currentSectorEntities;
   }

   public final int getCurrentSectorId() {
      if (this.getPlayer() == null) {
         assert this.getInitialSectorId() != -2;

         return this.getInitialSectorId();
      } else {
         assert this.getPlayer().getCurrentSectorId() != -2;

         return this.getPlayer().getCurrentSectorId();
      }
   }

   public Faction getFaction() {
      return this.getFactionManager().getFaction(this.getPlayer().getFactionId());
   }

   public FactionManager getFactionManager() {
      return this.gameState == null ? null : this.gameState.getFactionManager();
   }

   public String getFactionString() {
      Faction var1;
      return (var1 = this.getFaction()) == null ? "neutral" : var1.getName();
   }

   public GameModes getGameMode() {
      return this.gameMode;
   }

   public void setGameMode(GameModes var1) {
      this.gameMode = var1;
   }

   public SendableGameState getGameState() {
      return this.gameState;
   }

   public void setGameState(SendableGameState var1) {
      this.gameState = var1;
   }

   public boolean isPhysicalAsteroids() {
      return this.physicalAsteroids;
   }

   public void setPhysicalAsteroids(boolean var1) {
      this.physicalAsteroids = var1;
   }

   public Document getBlockBehaviorConfig() {
      return this.blockBehaviorConfig;
   }

   public void setBlockBehaviorConfig(Document var1) {
      this.blockBehaviorConfig = var1;
   }

   public float getSectorSize() {
      return this.getGameState().getSectorSize() + 300.0F;
   }

   public Short2ObjectOpenHashMap getAllTechs() {
      return this.techs;
   }

   public boolean getMaterialPrice() {
      return this.getGameState().isDynamicPrices();
   }

   public int getSegmentPieceQueueSize() {
      assert SERVER_BLOCK_QUEUE_SIZE > 0 : SERVER_BLOCK_QUEUE_SIZE;

      return SERVER_BLOCK_QUEUE_SIZE;
   }

   public ControlElementMapOptimizer getControlOptimizer() {
      return this.controlOptimizer;
   }

   public ChannelRouter getChannelRouter() {
      return this.channelRouter;
   }

   public GLFrame getGlFrame() {
      return this.glFrame;
   }

   public void setGlFrame(GLFrame var1) {
      this.glFrame = var1;
   }

   public GlobalGameControlManager getGlobalGameControlManager() {
      return this.globalGameControlManager;
   }

   public void setGlobalGameControlManager(GlobalGameControlManager var1) {
      this.globalGameControlManager = var1;
   }

   public GUIController getGUIController() {
      return this.guiController;
   }

   public void setGUIController(GUIController var1) {
      this.guiController = var1;
   }

   public float getHighestSubStep() {
      return this.highestSubStep;
   }

   public void setHighestSubStep(float var1) {
      this.highestSubStep = var1;
   }

   public Integer getInitialSectorId() {
      return this.initialSectorId;
   }

   public void setInitialSectorId(Integer var1) {
      System.err.println("SET INITIAL SECTOR ID TO " + var1);

      assert var1 != -2;

      this.initialSectorId = var1;
   }

   public Vector3i getInitialSectorPos() {
      return this.initialSectorPos;
   }

   public void setInitialSectorPos(Vector3i var1) {
      this.initialSectorPos = var1;
   }

   public long getLastServerTimeRequest() {
      return this.lastServerTimeRequest;
   }

   public void setLastServerTimeRequest(long var1) {
      this.lastServerTimeRequest = var1;
   }

   public float getLinearDamping() {
      return this.getGameState() != null ? this.getGameState().getLinearDamping() : 0.09F;
   }

   public Physics getPhysics() {
      return this.physics;
   }

   public String getPhysicsSlowMsg() {
      return "[PHYSICS][CLIENT] WARNING: PHYSICS SYNC IN DANGER";
   }

   public float getRotationalDamping() {
      return this.getGameState() != null ? this.getGameState().getRotationalDamping() : 0.09F;
   }

   public void handleNextPhysicsSubstep(float var1) {
      this.setHighestSubStep(Math.max(this.getHighestSubStep(), var1));
   }

   public String toStringDebug() {
      return "CLIENT: " + this.toString();
   }

   public void setPhysics(Physics var1) {
      this.physics = var1;
   }

   public MetaObjectManager getMetaObjectManager() {
      return this.metaObjectManager;
   }

   public void requestMetaObject(int var1) {
      synchronized(this.getToRequestMetaObjects()) {
         assert var1 >= 0 : var1;

         this.getToRequestMetaObjects().enqueue(var1);
      }
   }

   public ArrayList getNeedsNotify() {
      return this.needsNotify;
   }

   public HashSet getOtherSendables() {
      return this.otherSendables;
   }

   public ProjectileController getParticleController() {
      return this.particleController;
   }

   public void setParticleController(ProjectileController var1) {
      this.particleController = var1;
   }

   public EditableSendableSegmentController getPlanetTestSurface() {
      return this.planetTestSurface;
   }

   public void setPlanetTestSurface(EditableSendableSegmentController var1) {
      this.planetTestSurface = var1;
   }

   public PlayerState getPlayer() {
      return this.player;
   }

   public void setPlayer(PlayerState var1) {
      this.player = var1;
      this.setFlagPlayerReceived(true);
   }

   public PlayerState getPlayerFromClientId(long var1) throws PlayerNotFountException {
      synchronized(this.getLocalAndRemoteObjectContainer().getLocalObjects()){}

      Throwable var10000;
      label98: {
         boolean var10001;
         Iterator var4;
         try {
            var4 = this.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();
         } catch (Throwable var11) {
            var10000 = var11;
            var10001 = false;
            break label98;
         }

         while(true) {
            try {
               if (!var4.hasNext()) {
                  throw new PlayerNotFountException("clientID: " + var1);
               }

               Sendable var5;
               PlayerState var13;
               if ((var5 = (Sendable)var4.next()) instanceof PlayerState && (long)(var13 = (PlayerState)var5).getClientId() == var1) {
                  return var13;
               }
            } catch (Throwable var10) {
               var10000 = var10;
               var10001 = false;
               break;
            }
         }
      }

      Throwable var12 = var10000;
      throw var12;
   }

   public List getPlayerInputs() {
      return this.getController().getInputController().getPlayerInputs();
   }

   public void exit() {
      try {
         if (this.getPlayer() != null) {
            this.getPlayer().getPlayerChannelManager().saveChannelsClient();
         }
      } catch (Exception var5) {
         System.err.println("Exception: CHAT CHANNELS COULD NOT BE SAVED");
         var5.printStackTrace();
      }

      try {
         CrashReporter.createThreadDump();
         return;
      } catch (IOException var6) {
         var6.printStackTrace();
      } finally {
         GLFrame.setFinished(true);
      }

   }

   public List getGeneralChatLog() {
      return this.getChannelRouter().getDefaultChatLog();
   }

   public List getVisibleChatLog() {
      return this.getChannelRouter().getDefaultVisibleChatLog();
   }

   public Transform getCurrentPosition() {
      return this.getCurrentPlayerObject().getWorldTransform();
   }

   public void onSwitchedSetting(EngineSettings var1) {
      switch(var1) {
      case LIGHT_RAY_COUNT:
         SegmentDrawer.forceFullLightingUpdate = true;
      default:
      }
   }

   public PulseController getPulseController() {
      return this.pulseController;
   }

   public void setPulseController(PulseController var1) {
      this.pulseController = var1;
   }

   public Map getRequestingLocks() {
      return this.requestingSegments;
   }

   public MainGameGraphics getScene() {
      return this.scene;
   }

   public void setScene(MainGameGraphics var1) {
      this.scene = var1;
   }

   public SegmentDataManager getSegmentDataManager() {
      return this.segmentDataManager;
   }

   public ArrayList getServerMessages() {
      return this.serverMessages;
   }

   public Ship getShip() {
      return this.ship;
   }

   public void setShip(Ship var1) {
      this.ship = var1;
   }

   public ThreadedSegmentWriter getThreadedSegmentWriter() {
      return this.threadedSegmentWriter;
   }

   public IntArrayFIFOQueue getToRequestMetaObjects() {
      return this.toRequestMetaObjects;
   }

   public AiEntityState getTutorialAIState() {
      return this.tutorialAIState;
   }

   public short getWarpedUpdateNr() {
      return this.warpedUpdateNr;
   }

   public WorldDrawer getWorldDrawer() {
      return this.worldDrawer;
   }

   public void setWorldDrawer(WorldDrawer var1) {
      this.worldDrawer = var1;
   }

   public void handleFlaggedAddedOrRemovedObjects() {
      HashSet var1;
      Iterator var2;
      Sendable var5;
      if (!this.flaggedAddedObjects.isEmpty()) {
         var1 = new HashSet(this.flaggedAddedObjects.size());
         synchronized(this.flaggedAddedObjects) {
            var1.addAll(this.flaggedAddedObjects);
         }

         for(var2 = var1.iterator(); var2.hasNext(); this.onSendableAdded(var5)) {
            if ((var5 = (Sendable)var2.next()) instanceof PlanetCore) {
               System.err.println("[CLIENT] ADDED PLANET CORE OBJECT: " + var5);
            }
         }

         this.flaggedAddedObjects.clear();
      }

      if (!this.flaggedRemovedObjects.isEmpty()) {
         var1 = new HashSet(this.flaggedRemovedObjects.size());
         synchronized(this.flaggedRemovedObjects) {
            var1.addAll(this.flaggedRemovedObjects);
         }

         var2 = var1.iterator();

         while(var2.hasNext()) {
            var5 = (Sendable)var2.next();
            this.onSendableRemoved(var5);
         }

         this.flaggedRemovedObjects.clear();
      }

   }

   private void onSendableRemoved(Sendable var1) {
      if (var1 instanceof PlayerState) {
         this.playerStatesByDbId.put(((PlayerState)var1).getDbId(), (PlayerState)var1);
      }

      if (var1.isPrivateNetworkObject()) {
         this.controller.onPrivateSendableAdded(var1);
      } else {
         this.controller.onSendableRemoved(var1);
      }

      this.setChanged();
      this.notifyObservers(var1);
   }

   private void onSendableAdded(Sendable var1) {
      if (var1 instanceof PlayerState) {
         this.playerStatesByDbId.remove(((PlayerState)var1).getDbId());
      }

      if (var1.isPrivateNetworkObject()) {
         this.controller.onPrivateSendableAdded(var1);
      } else {
         this.controller.onSendableAdded(var1);
      }

      this.setChanged();
      this.notifyObservers(var1);
   }

   public void handleNeedsNotifyObjects() {
      if (!this.needsNotifyObjects.isEmpty()) {
         HashSet var1 = new HashSet();
         synchronized(this.needsNotifyObjects) {
            var1.addAll(this.needsNotifyObjects);
         }

         Iterator var2 = var1.iterator();

         while(var2.hasNext()) {
            Sendable var4 = (Sendable)var2.next();
            this.setChanged();
            this.notifyObservers(var4);
         }

         this.needsNotifyObjects.clear();
      }

   }

   public boolean isControllManagerInitialized() {
      return this.getGlobalGameControlManager() != null && this.getGlobalGameControlManager().isInitialized();
   }

   public boolean isDbPurgeRequested() {
      return this.dbPurgeRequested;
   }

   public void setDbPurgeRequested(boolean var1) {
      this.dbPurgeRequested = var1;
   }

   public boolean isFlagPlayerReceived() {
      return this.flagPlayerReceived;
   }

   public void setFlagPlayerReceived(boolean var1) {
      this.flagPlayerReceived = var1;
   }

   public boolean isFlagRequestServerTime() {
      return this.requestServerTimeFlag;
   }

   public int isFlagSectorChange() {
      return this.flagSectorChange;
   }

   public boolean isInShopDistance() {
      this.currentClosestShop = null;
      if (!this.isPlayerSpawned()) {
         return false;
      } else {
         try {
            boolean var1;
            if ((var1 = this.currentPlayerObject != null && this.currentPlayerObject instanceof ShopperInterface && ((ShopperInterface)this.currentPlayerObject).getShopsInDistance().size() > 0) != this.wasInShopDistance && this.getController().getTutorialMode() != null) {
               this.getController().getTutorialMode().shopDistanceChanged(var1);
               this.wasInShopDistance = var1;
            }

            if (var1) {
               Set var2 = ((ShopperInterface)this.currentPlayerObject).getShopsInDistance();
               float var3 = -1.0F;
               Iterator var7 = var2.iterator();

               while(true) {
                  ShopInterface var4;
                  Vector3f var5;
                  do {
                     do {
                        if (!var7.hasNext()) {
                           return var1;
                        }
                     } while((var4 = (ShopInterface)var7.next()).getSectorId() != this.currentPlayerObject.getSectorId());

                     (var5 = new Vector3f(var4.getWorldTransform().origin)).sub(this.currentPlayerObject.getWorldTransform().origin);
                  } while(var3 >= 0.0F && var5.lengthSquared() >= var3);

                  this.currentClosestShop = var4;
                  var3 = var5.lengthSquared();
               }
            } else {
               return var1;
            }
         } catch (NullPointerException var6) {
            System.err.println("EXCEPTION HAS BEEN CATCHED. CURRENT OBJECT PROBABLY BECAME NULL: " + this.currentPlayerObject);
            var6.printStackTrace();
            return false;
         }
      }
   }

   public boolean isInWarp() {
      return this.warped;
   }

   public boolean isPlayerSpawned() {
      return this.playerSpawned;
   }

   public void setPlayerSpawned(boolean var1) {
      if (this.playerSpawned && !var1 && this.controller.getTutorialMode() != null) {
         this.controller.getTutorialMode().repeat();
      }

      this.playerSpawned = var1;
   }

   public boolean isWaitingForPlayerActivate() {
      return this.waitingForPlayerActivate;
   }

   public void setWaitingForPlayerActivate(boolean var1) {
      this.waitingForPlayerActivate = var1;
   }

   public void notifyOfCatalogChange() {
      this.setChanged();
      this.notifyObservers("CATALOG_UPDATE");
   }

   public void resetFlagRequestServerTime() {
      this.requestServerTimeFlag = false;
   }

   public void setConfigCheckSum(String var1) {
      this.configCheckSum = var1;
   }

   public void setFlagSectorChange(int var1) {
      this.flagSectorChange = var1;
   }

   public void setWarped(boolean var1) {
      this.warped = var1;
   }

   public void update(DrawerObservable var1, Object var2, Object var3) {
      synchronized(this.getNeedsNotify()) {
         this.getNeedsNotify().add(var1);
      }
   }

   public ClientMessageLog getMessageLog() {
      return this.messageLog;
   }

   public int getHinderedInput() {
      return this.hinderedInput;
   }

   public void setHinderedInput(int var1) {
      this.hinderedInput = var1;
      this.hinderedInputTime = System.currentTimeMillis();
   }

   public long getHinderedInputTime() {
      return this.hinderedInputTime;
   }

   public int getReceiveQueue() {
      return this.controller.getConnection().getReceiveQueue();
   }

   public String getBlockBehaviorCheckSum() {
      return this.blockBehaviorCheckSum;
   }

   public void setBlockBehaviorCheckSum(String var1) {
      this.blockBehaviorCheckSum = var1;
   }

   public String getCustomTexturesCheckSum() {
      return this.customTexturesCheckSum;
   }

   public void setCustomTexturesCheckSum(String var1) {
      this.customTexturesCheckSum = var1;
   }

   public Object2ObjectOpenHashMap getLoadedSectors() {
      return this.loadedSectors;
   }

   public float getMaxBuildArea() {
      return (float)this.gameState.getMaxBuildArea();
   }

   public Galaxy getCurrentGalaxyNeighbor(Vector3i var1) {
      Vector3i var2;
      (var2 = Galaxy.getContainingGalaxyFromSystemPos(VoidSystem.getContainingSystem(!this.getPlayer().isInTutorial() && !this.getPlayer().isInPersonalSector() && !this.getPlayer().isInTestSector() ? this.getPlayer().getCurrentSector() : new Vector3i(0, 0, 0), this.tmpStellar), this.tmpGalaxy)).add(var1);
      Galaxy var7;
      if ((var7 = (Galaxy)this.surrounding.get(var2)) == null) {
         long var3 = System.currentTimeMillis();
         long var5 = this.getGameState().getUniverseSeed() + (long)var2.hashCode();
         (var7 = new Galaxy(var5, new Vector3i(var2))).generate();
         System.err.println("[CLIENT] creating galaxy " + this.tmpGalaxy + "; Stars: " + var7.getNumberOfStars() + "; created in: " + (System.currentTimeMillis() - var3) + "ms");
         this.surrounding.put(var7.galaxyPos, var7);
      }

      return var7;
   }

   public Galaxy getCurrentGalaxy() {
      Vector3i var1 = Galaxy.getContainingGalaxyFromSystemPos(VoidSystem.getContainingSystem(!this.getPlayer().isInTutorial() && !this.getPlayer().isInPersonalSector() && !this.getPlayer().isInTestSector() ? this.getPlayer().getCurrentSector() : new Vector3i(0, 0, 0), this.tmpStellar), this.tmpGalaxy);
      long var2 = this.getGameState().getUniverseSeed() + (long)var1.hashCode();

      assert var2 != 0L;

      if (this.currentGalaxy == null || this.currentGalaxy.getSeed() != var2) {
         this.surrounding.clear();
         long var4 = System.currentTimeMillis();
         this.currentGalaxy = new Galaxy(var2, new Vector3i(var1));
         this.currentGalaxy.generate();
         System.err.println("[CLIENT] creating galaxy " + this.tmpGalaxy + "; Stars: " + this.currentGalaxy.getNumberOfStars() + "; created in: " + (System.currentTimeMillis() - var4) + "ms");
      }

      return this.currentGalaxy;
   }

   public VoidSystem getCurrentClientSystem() {
      if (!this.lastSector.equals(this.getPlayer().getCurrentSector())) {
         if (this.getController().getClientChannel() != null && this.getController().getClientChannel().isConnectionReady() && this.getPlayer() != null) {
            this.cached = this.getController().getClientChannel().getGalaxyManagerClient().getSystemOnClient(this.getPlayer().getCurrentSector());
            this.lastSector.set(this.getPlayer().getCurrentSector());
         } else {
            this.cached = null;
         }
      }

      return this.cached;
   }

   public String getFactionConfigCheckSum() {
      return this.factionConfigCheckSum;
   }

   public void setFactionConfigCheckSum(String var1) {
      this.factionConfigCheckSum = var1;
   }

   public Map getOnlinePlayersLowerCaseMap() {
      return this.onlinePlayersLowerCaseMap;
   }

   public void notifyLaggyListChanged() {
      Iterator var1 = this.getPlayerInputs().iterator();

      while(var1.hasNext()) {
         DialogInterface var2;
         if ((var2 = (DialogInterface)var1.next()) instanceof PlayerLagStatsInput) {
            ((PlayerLagStatsInput)var2).flagChanged();
         }
      }

   }

   public boolean isAdmin() {
      return this.getPlayer() != null && this.getPlayer().getNetworkObject() != null && this.getPlayer().getNetworkObject().isAdminClient.getBoolean();
   }

   public RaceManager getRaceManager() {
      return this.getGameState().getRaceManager();
   }

   public boolean isInAnyBuildMode() {
      return this.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().isInAnyBuildMode();
   }

   public boolean isInCharacterBuildMode() {
      return this.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().isInAnyCharacterBuildMode();
   }

   public boolean isInAnyStructureBuildMode() {
      return this.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().isInAnyStructureBuildMode();
   }

   public boolean isDebugKeyDown() {
      return KeyboardMappings.PLAYER_LIST.isDown(this);
   }

   public FleetManager getFleetManager() {
      return this.fleetManager;
   }

   public void stopClient() {
      Starter.stopClient(this.getController().graphicsContext);
   }

   public void startClient(HostPortLoginName var1, boolean var2) {
      Starter.startClient(var1, var2, this.getController().graphicsContext);
   }

   public void startLocalServer() {
      Starter.startServer(false);
   }

   public void flagChangedForObservers(Object var1) {
      this.setChanged();
      this.notifyObservers(var1);
   }

   public void handleExceptionGraphically(Exception var1) {
      if (GameMainMenuController.currentMainMenu != null) {
         GameMainMenuController.currentMainMenu.switchFrom(this, var1);
      }

   }

   public String getGUIPath() {
      return "gui/ingame/";
   }

   public GraphicsContext getGraphicsContext() {
      return this.controller.graphicsContext;
   }

   public void setActiveSubtitles(List var1) {
      this.activeSubtitles = var1;
   }

   public List getActiveSubtitles() {
      return this.activeSubtitles;
   }

   public BlockSyleSubSlotController getBlockSyleSubSlotController() {
      return this.blockSyleSubSlotController;
   }

   public ConfigPool getConfigPool() {
      return this.getGameState() == null ? null : this.getGameState().getConfigPool();
   }

   public EventFactory getEventFactory() {
      return this.eventFactory;
   }

   public void onPowerChanged(SegmentController var1, PowerChangeListener.PowerChangeType var2) {
      Iterator var3 = this.powerChangeListeners.iterator();

      while(var3.hasNext()) {
         ((PowerChangeListener)var3.next()).powerChanged(var1, var2);
      }

   }

   public void onDockChanged(SegmentController var1, boolean var2) {
      Iterator var3 = this.dockingListeners.iterator();

      while(var3.hasNext()) {
         ((RailDockingListener)var3.next()).dockingChanged(var1, var2);
      }

   }

   public List getDockingListeners() {
      return this.dockingListeners;
   }

   public List getPowerChangeListeners() {
      return this.powerChangeListeners;
   }

   public Long2ObjectOpenHashMap getPlayerStatesByDbId() {
      return this.playerStatesByDbId;
   }

   public DebugTimer getDebugTimer() {
      return this.debugTimer;
   }

   public boolean isInFlightMode() {
      return this.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController().isTreeActiveInFlight();
   }
}
