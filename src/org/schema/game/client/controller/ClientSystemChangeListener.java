package org.schema.game.client.controller;

import org.schema.common.util.linAlg.Vector3i;

public interface ClientSystemChangeListener {
   void onSystemChanged(Vector3i var1, Vector3i var2);
}
