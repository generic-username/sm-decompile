package org.schema.game.client.controller;

import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.TreeSet;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.data.gamemap.requests.GameMapRequestManager;
import org.schema.game.common.controller.ParticleEntry;
import org.schema.game.common.controller.trade.TradeNodeClient;
import org.schema.game.common.controller.trade.TradeNodeStub;
import org.schema.game.common.controller.trade.TradeOrder;
import org.schema.game.common.controller.trade.TradeTypeRequestAwnser;
import org.schema.game.common.data.MetaObjectState;
import org.schema.game.common.data.ScanData;
import org.schema.game.common.data.mines.updates.MineUpdate;
import org.schema.game.common.data.player.ClientFileDownloadController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.SavedCoordinate;
import org.schema.game.common.data.player.faction.FactionNewsPost;
import org.schema.game.common.data.player.playermessage.PlayerMessageController;
import org.schema.game.common.data.world.GalaxyManager;
import org.schema.game.network.objects.ChatChannelModification;
import org.schema.game.network.objects.ChatMessage;
import org.schema.game.network.objects.CreateDockRequest;
import org.schema.game.network.objects.NetworkClientChannel;
import org.schema.game.network.objects.StringLongLongPair;
import org.schema.game.network.objects.TradePrices;
import org.schema.game.network.objects.remote.RemoteChatChannel;
import org.schema.game.network.objects.remote.RemoteChatMessage;
import org.schema.game.network.objects.remote.RemoteCreateDock;
import org.schema.game.network.objects.remote.RemoteFactionNewsPost;
import org.schema.game.network.objects.remote.RemoteMineUpdate;
import org.schema.game.network.objects.remote.RemoteNPCDiplomacy;
import org.schema.game.network.objects.remote.RemoteParticleEntry;
import org.schema.game.network.objects.remote.RemoteSavedCoordinate;
import org.schema.game.network.objects.remote.RemoteScanData;
import org.schema.game.network.objects.remote.RemoteStringLongPair;
import org.schema.game.network.objects.remote.RemoteTradeOrder;
import org.schema.game.network.objects.remote.RemoteTradePrice;
import org.schema.game.network.objects.remote.RemoteTradeTypeRequest;
import org.schema.game.server.controller.GameServerController;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.simulation.npc.diplomacy.NPCDiplomacyEntity;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.TopLevelType;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.objects.remote.RemoteString;
import org.schema.schine.network.objects.remote.Streamable;
import org.schema.schine.network.server.ServerStateInterface;
import org.schema.schine.resource.FileExt;

public class ClientChannel extends Observable implements Sendable {
   private final boolean onServer;
   private final TreeSet factionNews = new TreeSet();
   private final ArrayList toAddFactionNewsPosts = new ArrayList();
   private final ClientFileDownloadController clientFileDownloadController;
   private final GameMapRequestManager clientMapRequestManager;
   private final ObjectArrayFIFOQueue toAddScanData = new ObjectArrayFIFOQueue();
   int id = -12312323;
   private StateInterface state;
   private int playerId = -121212;
   private NetworkClientChannel networkClientChannel;
   private String clientBlockBehaviorToUpload = null;
   private PlayerMessageController playerMessageController;
   private boolean markedForDeleteVolatile;
   private boolean markedForDeleteSent;
   private int factionId;
   private boolean writtenForUnload;
   private PlayerState player;
   private GalaxyManager galaxyManagerClient;
   private final ObjectArrayFIFOQueue receivedTradePrices = new ObjectArrayFIFOQueue();
   private ClientChannel.ReceivedTradeSearchResult receivedTradeSearchResults = new ClientChannel.ReceivedTradeSearchResult();
   private ObjectArrayFIFOQueue tradeSearchAwnseresToAdd = new ObjectArrayFIFOQueue();
   private ObjectArrayList diplomacyEntitiesToAddClient = new ObjectArrayList();

   public ClientChannel(StateInterface var1) {
      this.state = var1;
      this.onServer = var1 instanceof ServerStateInterface;
      if (!this.onServer) {
         this.galaxyManagerClient = new GalaxyManager(var1);
      }

      this.clientMapRequestManager = new GameMapRequestManager(this);
      this.clientFileDownloadController = new ClientFileDownloadController(this);
      this.setPlayerMessageController(new PlayerMessageController(this));
      if (!this.isOnServer()) {
         this.setPlayer(((GameClientState)var1).getPlayer());
         this.getPlayer().setClientChannel(this);
      }

   }

   public void cleanUpOnEntityDelete() {
   }

   public ClientChannel.ReceivedTradeSearchResult getReceivedTradeSearchResults() {
      return this.receivedTradeSearchResults;
   }

   public void requestTradeNodesFor(short var1) {
      this.receivedTradeSearchResults.o.clear();
      this.getNetworkObject().tradeTypeRequestBuffer.add(var1);
      this.receivedTradeSearchResults.changed();
   }

   public void destroyPersistent() {
   }

   public NetworkClientChannel getNetworkObject() {
      return this.networkClientChannel;
   }

   public StateInterface getState() {
      return this.state;
   }

   public void initFromNetworkObject(NetworkObject var1) {
      this.setId(this.networkClientChannel.id.get());
      this.setPlayerId((Integer)this.networkClientChannel.playerId.get());
      if (this.isOnServer()) {
         this.setPlayer((PlayerState)((GameServerState)this.state).getLocalAndRemoteObjectContainer().getLocalObjects().get(this.getPlayerId()));
         this.getPlayer().setClientChannel(this);
      }

   }

   public void initialize() {
   }

   public boolean isMarkedForDeleteVolatile() {
      return this.markedForDeleteVolatile;
   }

   public void setMarkedForDeleteVolatile(boolean var1) {
      this.markedForDeleteVolatile = var1;
   }

   public boolean isMarkedForDeleteVolatileSent() {
      return this.markedForDeleteSent;
   }

   public void setMarkedForDeleteVolatileSent(boolean var1) {
      this.markedForDeleteSent = var1;
   }

   public boolean isMarkedForPermanentDelete() {
      return false;
   }

   public boolean isOkToAdd() {
      return true;
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public boolean isUpdatable() {
      return true;
   }

   public void markForPermanentDelete(boolean var1) {
   }

   public void newNetworkObject() {
      this.networkClientChannel = new NetworkClientChannel(this.state, ((MetaObjectState)this.state).getMetaObjectManager());
   }

   public void updateFromNetworkObject(NetworkObject var1, int var2) {
      this.setId(this.networkClientChannel.id.get());
      this.clientFileDownloadController.handleUploadNT(var2);
      this.getPlayerMessageController().handleReceived(this.networkClientChannel.playerMessageBuffer, this.getNetworkObject().playerMessageRequests);

      int var14;
      for(var14 = 0; var14 < this.getNetworkObject().savedCoordinates.getReceiveBuffer().size(); ++var14) {
         RemoteSavedCoordinate var3 = (RemoteSavedCoordinate)this.getNetworkObject().savedCoordinates.getReceiveBuffer().get(var14);
         this.getPlayer().getSavedCoordinatesToAdd().enqueue(var3.get());
      }

      ((GameStateInterface)this.getState()).getGameState().getManualTradeManager().updateFromNetworkObject(this.networkClientChannel);

      for(var14 = 0; var14 < this.networkClientChannel.mineUpdateBuffer.getReceiveBuffer().size(); ++var14) {
         MineUpdate var15 = (MineUpdate)((RemoteMineUpdate)this.networkClientChannel.mineUpdateBuffer.getReceiveBuffer().get(var14)).get();
         ((MineInterface)this.getState().getController()).getMineController().receivedUpdate(this, var15);
      }

      long var23;
      if (!this.isOnServer()) {
         ((GameStateInterface)this.getState()).getGameState().getConfigPool().clientReceive(this);

         for(var14 = 0; var14 < this.networkClientChannel.scanDataUpdates.getReceiveBuffer().size(); ++var14) {
            ScanData var16 = (ScanData)((RemoteScanData)this.networkClientChannel.scanDataUpdates.getReceiveBuffer().get(var14)).get();
            synchronized(this.toAddScanData) {
               this.toAddScanData.enqueue(var16);
            }
         }

         for(var14 = 0; var14 < this.getNetworkObject().npcDiplomacyBuffer.getReceiveBuffer().size(); ++var14) {
            this.diplomacyEntitiesToAddClient.add(((RemoteNPCDiplomacy)this.getNetworkObject().npcDiplomacyBuffer.getReceiveBuffer().get(var14)).get());
         }

         for(var14 = 0; var14 < this.networkClientChannel.tradeTypeBuffer.getReceiveBuffer().size(); ++var14) {
            TradeTypeRequestAwnser var17 = (TradeTypeRequestAwnser)((RemoteTradeTypeRequest)this.networkClientChannel.tradeTypeBuffer.getReceiveBuffer().get(var14)).get();
            this.tradeSearchAwnseresToAdd.enqueue(var17);
         }

         for(var14 = 0; var14 < this.networkClientChannel.factionNewsPosts.getReceiveBuffer().size(); ++var14) {
            RemoteFactionNewsPost var18 = (RemoteFactionNewsPost)this.networkClientChannel.factionNewsPosts.getReceiveBuffer().get(var14);
            synchronized(this.toAddFactionNewsPosts) {
               System.err.println("[FACTIONMANAGER] received news on vlient channel " + this.state + ": " + var18.get());
               this.toAddFactionNewsPosts.add(var18.get());
            }
         }

         this.getClientMapRequestManager().updateFromNetworkObject(this.networkClientChannel);
         this.galaxyManagerClient.updateFromNetwork(this);

         for(var14 = 0; var14 < this.getNetworkObject().particles.getReceiveBuffer().size(); ++var14) {
            ((ParticleEntry)((RemoteParticleEntry)this.getNetworkObject().particles.getReceiveBuffer().get(var14)).get()).handleClient((GameClientState)this.state);
         }

         for(var14 = 0; var14 < this.getNetworkObject().pricesOfShopAwnser.getReceiveBuffer().size(); ++var14) {
            TradePrices var20 = (TradePrices)((RemoteTradePrice)this.getNetworkObject().pricesOfShopAwnser.getReceiveBuffer().get(var14)).get();
            synchronized(this.receivedTradePrices) {
               this.receivedTradePrices.enqueue(var20);
            }
         }
      } else {
         ((GameStateInterface)this.getState()).getGameState().getConfigPool().checkRequestReceived(this);
         ((GameServerState)this.getState()).getUniverse().getGalaxyManager().updateFromNetwork(this);

         for(var14 = 0; var14 < this.networkClientChannel.tradeTypeRequestBuffer.getReceiveBuffer().size(); ++var14) {
            short var21 = this.networkClientChannel.tradeTypeRequestBuffer.getReceiveBuffer().getShort(var14);
            ((GameServerState)this.getState()).getGameState().getTradeManager().requestTradeTypePrices(this.networkClientChannel, var21);
         }

         for(var14 = 0; var14 < this.getNetworkObject().tradeOrderRequests.getReceiveBuffer().size(); ++var14) {
            TradeOrder var22;
            (var22 = (TradeOrder)((RemoteTradeOrder)this.getNetworkObject().tradeOrderRequests.getReceiveBuffer().get(var14)).get()).clientChannelReceivedOn = this;
            ((GameServerState)this.getState()).getGameState().getTradeManager().receivedOrderOnServer(var22);
         }

         for(var14 = 0; var14 < this.getNetworkObject().requestPricesOfShop.getReceiveBuffer().size(); ++var14) {
            var23 = this.getNetworkObject().requestPricesOfShop.getReceiveBuffer().get(var14);
            ((GameServerState)this.getState()).getUniverse().getGalaxyManager().enqueuePricesRequest(var23, this);
         }

         for(var14 = 0; var14 < this.getNetworkObject().createDockBuffer.getReceiveBuffer().size(); ++var14) {
            CreateDockRequest var24 = (CreateDockRequest)((RemoteCreateDock)this.getNetworkObject().createDockBuffer.getReceiveBuffer().get(var14)).get();
            if (this.getPlayer() != null) {
               synchronized(this.getPlayer().getCreateDockRequests()) {
                  this.getPlayer().getCreateDockRequests().enqueue(var24);
               }
            }
         }

         for(var14 = 0; var14 < this.getNetworkObject().blockBehaviorUploads.getReceiveBuffer().size(); ++var14) {
            PlayerState var25;
            if ((var25 = this.getPlayer()).getNetworkObject().isAdminClient.get()) {
               System.err.println("[SERVER] received block behavior");
               String var4 = (String)((RemoteString)this.getNetworkObject().blockBehaviorUploads.getReceiveBuffer().get(var14)).get();

               try {
                  ((GameServerState)this.getState()).getController().setBlockBehaviorChanged(var4.getBytes("UTF-8"));
               } catch (UnsupportedEncodingException var9) {
                  var9.printStackTrace();
                  ((GameServerState)this.getState()).getController().broadcastMessage(new Object[]{0}, 3);
               }
            } else {
               System.err.println("[SERVER] " + var25 + " tried to upload block behavior but is not admin (hacked client)");
            }
         }

         for(var14 = 0; var14 < this.networkClientChannel.metaObjectRequests.getReceiveBuffer().size(); ++var14) {
            int var26 = this.networkClientChannel.metaObjectRequests.getReceiveBuffer().get(var14);
            ((MetaObjectState)this.getState()).getMetaObjectManager().awnserRequestTo(var26, this.networkClientChannel);
         }
      }

      for(var14 = 0; var14 < this.networkClientChannel.chatBuffer.getReceiveBuffer().size(); ++var14) {
         ChatMessage var27 = (ChatMessage)((RemoteChatMessage)this.networkClientChannel.chatBuffer.getReceiveBuffer().get(var14)).get();
         if (this.isOnServer()) {
            ((GameServerState)this.getState()).getChannelRouter().receivedChat.enqueue(var27);
         } else {
            ((GameClientState)this.getState()).getChannelRouter().receivedChat.enqueue(var27);
         }
      }

      for(var14 = 0; var14 < this.networkClientChannel.chatChannelBuffer.getReceiveBuffer().size(); ++var14) {
         ChatChannelModification var28 = (ChatChannelModification)((RemoteChatChannel)this.networkClientChannel.chatChannelBuffer.getReceiveBuffer().get(var14)).get();
         this.getPlayer().getPlayerChannelManager().receivedChannelMods.enqueue(var28);
      }

      if (this.isOnServer()) {
         ((GameServerState)this.state).getGameMapProvider().updateFromNetworkObject(this);

         for(var14 = 0; var14 < this.networkClientChannel.factionNewsRequests.getReceiveBuffer().size(); ++var14) {
            var23 = this.networkClientChannel.factionNewsRequests.getReceiveBuffer().get(var14);
            this.getPlayer().getFactionController().queueNewsRequestOnServer(this.networkClientChannel, var23);
         }

         String var30;
         for(var14 = 0; var14 < this.networkClientChannel.timeStampRequests.getReceiveBuffer().size(); ++var14) {
            var30 = (String)((RemoteString)this.networkClientChannel.timeStampRequests.getReceiveBuffer().get(var14)).get();
            FileExt var19 = new FileExt("./server-skins/" + var30 + ".smskin");
            long var5 = 0L;
            long var7 = 0L;
            if (var19.exists()) {
               var5 = var19.lastModified();
               var7 = var19.length();
            }

            this.networkClientChannel.timeStampResponses.add(new RemoteStringLongPair(new StringLongLongPair(var30, var5, var7), this.isOnServer()));
         }

         for(var14 = 0; var14 < this.networkClientChannel.fileRequests.getReceiveBuffer().size(); ++var14) {
            var30 = (String)((RemoteString)this.networkClientChannel.fileRequests.getReceiveBuffer().get(var14)).get();
            System.err.println("[SERVER] received File request for " + var30);
            ((GameServerState)this.state).addServerFileRequest(this, var30);
         }

         ((GameServerController)this.getState().getController()).getMissileController().fromNetwork(this.networkClientChannel);
      } else {
         for(var14 = 0; var14 < this.networkClientChannel.timeStampResponses.getReceiveBuffer().size(); ++var14) {
            StringLongLongPair var29 = (StringLongLongPair)((RemoteStringLongPair)this.networkClientChannel.timeStampResponses.getReceiveBuffer().get(var14)).get();
            ((GameClientState)this.state).getController().getTextureSynchronizer().handleTimeStampResponse(var29);
         }

         ((GameClientController)this.getState().getController()).getClientMissileManager().fromNetwork(this.networkClientChannel);
      }
   }

   public void updateLocal(Timer var1) {
      if (!this.onServer) {
         GameClientState var2;
         if ((var2 = (GameClientState)this.getState()).getPlayer().getFactionId() != 0 && var2.getPlayer().getFactionId() != this.factionId) {
            this.factionNews.clear();
            this.requestNextNews();
            this.factionId = var2.getPlayer().getFactionId();
         }

         if (this.clientBlockBehaviorToUpload != null) {
            if (((GameClientState)this.getState()).getPlayer().getNetworkObject().isAdminClient.get()) {
               ((GameClientState)this.getState()).getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_CLIENTCHANNEL_1, 0.0F);
               this.getNetworkObject().blockBehaviorUploads.add((Streamable)(new RemoteString(this.clientBlockBehaviorToUpload, this.onServer)));
            } else {
               ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_CLIENTCHANNEL_2, 0.0F);
            }

            this.clientBlockBehaviorToUpload = null;
         }

         for(int var8 = 0; var8 < this.diplomacyEntitiesToAddClient.size(); ++var8) {
            assert !this.isOnServer();

            NPCDiplomacyEntity var3;
            if ((var3 = (NPCDiplomacyEntity)this.diplomacyEntitiesToAddClient.get(var8)).isFactionLoaded()) {
               var3.getDiplomacy().entities.put(var3.getDbId(), var3);
               this.diplomacyEntitiesToAddClient.remove(var8);
               --var8;
               var3.getDiplomacy().onClientChanged();
            }
         }

         ((GameStateInterface)this.getState()).getGameState().getConfigPool().checkClientRequest(this);
         if (!this.getPlayerMessageController().initialRequest) {
            this.getNetworkObject().playerMessageRequests.add(10);
            this.getPlayerMessageController().initialRequest = true;
         }

         while(!this.tradeSearchAwnseresToAdd.isEmpty()) {
            TradeTypeRequestAwnser var10 = (TradeTypeRequestAwnser)this.tradeSearchAwnseresToAdd.dequeue();
            TradeNodeStub var9;
            if ((var9 = (TradeNodeStub)this.getGalaxyManagerClient().getTradeNodeDataById().get(var10.node)) != null) {
               var10.nodeClient = (TradeNodeClient)var9;
               this.receivedTradeSearchResults.o.add(var10);
               this.receivedTradeSearchResults.changed();
            }
         }

         this.getClientMapRequestManager().update(var1);
         this.galaxyManagerClient.updateClient();
         if (!this.toAddScanData.isEmpty()) {
            synchronized(this.toAddScanData) {
               while(!this.toAddScanData.isEmpty()) {
                  ScanData var11 = (ScanData)this.toAddScanData.dequeue();
                  this.getPlayer().addScanHistory(var11);
                  if (System.currentTimeMillis() - var11.time < 60000L) {
                     ((GameClientState)this.getState()).getController().popupGameTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_CLIENTCHANNEL_3, 0.0F);
                  }
               }
            }
         }

         if (!this.receivedTradePrices.isEmpty()) {
            synchronized(this.receivedTradePrices) {
               while(!this.receivedTradePrices.isEmpty()) {
                  TradePrices var12 = (TradePrices)this.receivedTradePrices.dequeue();
                  this.getGalaxyManagerClient().updatedPricesOnClient(var12);
               }
            }
         }

         if (!this.toAddFactionNewsPosts.isEmpty()) {
            synchronized(this.toAddFactionNewsPosts) {
               while(!this.toAddFactionNewsPosts.isEmpty()) {
                  FactionNewsPost var13 = (FactionNewsPost)this.toAddFactionNewsPosts.remove(0);
                  if (!this.factionNews.isEmpty() && ((FactionNewsPost)this.factionNews.iterator().next()).getFactionId() != ((GameClientState)this.state).getPlayer().getFactionId()) {
                     System.err.println("[CLIENT] RECEIVED NEWS OF OTHER FACTION -> cleaning news");
                     this.factionNews.clear();
                  }

                  this.factionNews.add(var13);
                  this.setChanged();
                  this.notifyObservers();
               }
            }
         }
      }

      try {
         this.clientFileDownloadController.updateLocal();
      } catch (IOException var4) {
         var4.printStackTrace();
         this.clientFileDownloadController.setNeedsUpdate(false);
      }

      if (!this.onServer && !this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(this.playerId)) {
         this.setMarkedForDeleteVolatile(true);
         System.err.println("[SERVER][CLIENTCHANNEL] DELETING: no more player attached");
         this.getClientFileDownloadController().setNeedsUpdate(false);
      }

   }

   public void updateToFullNetworkObject() {
      this.networkClientChannel.playerId.set(this.getPlayerId());
      this.networkClientChannel.id.set(this.getId());
      this.updateToNetworkObject();
   }

   public void updateToNetworkObject() {
      if (this.isOnServer()) {
         this.networkClientChannel.connectionReady.set(true);
      }

   }

   public boolean isWrittenForUnload() {
      return this.writtenForUnload;
   }

   public void setWrittenForUnload(boolean var1) {
      this.writtenForUnload = var1;
   }

   public ClientFileDownloadController getClientFileDownloadController() {
      return this.clientFileDownloadController;
   }

   public GameMapRequestManager getClientMapRequestManager() {
      return this.clientMapRequestManager;
   }

   public TreeSet getFactionNews() {
      return this.factionNews;
   }

   public int getId() {
      return this.id;
   }

   public void setId(int var1) {
      this.id = var1;
   }

   public int getPlayerId() {
      return this.playerId;
   }

   public void setPlayerId(int var1) {
      this.playerId = var1;
   }

   public boolean isConnectionReady() {
      return this.getNetworkObject() != null && (Boolean)this.getNetworkObject().connectionReady.get();
   }

   public void requestAllNews() {
      this.networkClientChannel.factionNewsRequests.add(-1L);
   }

   public void requestFile(String var1) {
      this.clientFileDownloadController.setCurrentDownloadName(var1);
      this.networkClientChannel.fileRequests.add((Streamable)(new RemoteString(var1, this.getNetworkObject())));
   }

   public void requestFileTimestamp(String var1) {
      this.networkClientChannel.timeStampRequests.add((Streamable)(new RemoteString(var1, this.getNetworkObject())));
   }

   public void requestMetaObject(int var1) {
      this.getNetworkObject().metaObjectRequests.add(var1);
   }

   public void requestNextNews() {
      if (this.factionNews.isEmpty()) {
         this.networkClientChannel.factionNewsRequests.add(System.currentTimeMillis());
      } else {
         this.networkClientChannel.factionNewsRequests.add(((FactionNewsPost)this.factionNews.iterator().next()).getDate());
      }
   }

   public void setConnectionReady() {
      this.getNetworkObject().connectionReady.set(true);
   }

   public void uploadClientBlockBehavior(String var1) throws IOException {
      assert !this.onServer;

      if (!this.onServer) {
         FileExt var6 = new FileExt(var1);
         BufferedReader var7 = new BufferedReader(new FileReader(var6));

         try {
            StringBuffer var2 = new StringBuffer();

            String var3;
            while((var3 = var7.readLine()) != null) {
               var2.append(var3);
               var2.append("\n");
            }

            this.clientBlockBehaviorToUpload = var2.toString();
         } finally {
            var7.close();
         }
      } else {
         throw new IllegalArgumentException("Client only");
      }
   }

   public PlayerState getPlayer() {
      return this.player;
   }

   public void setPlayer(PlayerState var1) {
      this.player = var1;
   }

   public PlayerMessageController getPlayerMessageController() {
      return this.playerMessageController;
   }

   public void setPlayerMessageController(PlayerMessageController var1) {
      this.playerMessageController = var1;
   }

   public GalaxyManager getGalaxyManagerClient() {
      return this.galaxyManagerClient;
   }

   public void sendSavedCoordinateToServer(String var1, Vector3i var2) {
      this.getNetworkObject().savedCoordinates.add(new RemoteSavedCoordinate(new SavedCoordinate(new Vector3i(var2), var1, false), false));
   }

   public void removeSavedCoordinateToServer(SavedCoordinate var1) {
      this.getNetworkObject().savedCoordinates.add(new RemoteSavedCoordinate(new SavedCoordinate(new Vector3i(var1.getSector()), var1.getName(), true), false));
   }

   public void announceLag(long var1) {
   }

   public long getCurrentLag() {
      return 0L;
   }

   public void onStopClient() {
      if (this.galaxyManagerClient != null) {
         this.galaxyManagerClient.shutdown();
      }

   }

   public void requestTradePrices(long var1) {
      this.getNetworkObject().requestPricesOfShop.add(var1);
   }

   public TopLevelType getTopLevelType() {
      return TopLevelType.GENERAL;
   }

   public boolean isPrivateNetworkObject() {
      return true;
   }

   public class ReceivedTradeSearchResult extends Observable {
      public ObjectArrayList o = new ObjectArrayList();

      public void changed() {
         this.setChanged();
         this.notifyObservers();
      }
   }
}
