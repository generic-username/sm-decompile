package org.schema.game.client.controller;

import org.schema.schine.network.objects.Sendable;

public interface SendableAddedRemovedListener {
   void onAddedSendable(Sendable var1);

   void onRemovedSendable(Sendable var1);
}
