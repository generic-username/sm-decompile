package org.schema.game.client.controller;

public class LogoutException extends Exception {
   private static final long serialVersionUID = 1L;

   public LogoutException(String var1) {
      super(var1);
   }
}
