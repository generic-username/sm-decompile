package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.inventory.inventorynew.InventoryPanelNew;
import org.schema.game.client.view.gui.inventory.inventorynew.SecondaryInventoryPanelNew;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.PersonalFactoryInventory;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.KeyEventInterface;

public class PlayerInventoryInput extends PlayerInput {
   private SecondaryInventoryPanelNew panel;
   private GUIActiveInterface actInterface;
   private InventoryPanelNew invPanel;

   public PlayerInventoryInput(String var1, GameClientState var2, Object var3, GUIActiveInterface var4, InventoryPanelNew var5, Inventory var6, InventoryPanelNew var7) {
      super(var2);
      this.actInterface = var4;
      this.invPanel = var5;
      if (this.panel == null) {
         short var9 = 520;
         short var8 = 510;
         if (var6 instanceof PersonalFactoryInventory) {
            var9 = 370;
            var8 = 191;
         }

         this.panel = new SecondaryInventoryPanelNew(var1, var2, var3, var9, var8, var6, this, var4, var7);
      }

      this.panel.setCallback(this);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var1.getUserPointer() != null && !var1.wasInside() && var1.isInside()) {
         this.getState().getController().queueUIAudio("0022_action - buttons push small");
      }

      if (var2.getEventButtonState() && var2.getEventButton() == 0) {
         if (var1.getUserPointer().equals("OK")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - enter");
            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("CANCEL")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - back");
            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("X")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - back");
            this.deactivate();
            return;
         }

         assert false : "not known command: '" + var1.getUserPointer() + "'";
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
   }

   public void deactivate() {
      this.invPanel.deactivate(this);
   }

   public SecondaryInventoryPanelNew getInputPanel() {
      return this.panel;
   }

   public void onDeactivate() {
      this.invPanel.deactivateSecondary(this.panel);
   }

   public boolean isOccluded() {
      return !this.actInterface.isActive();
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public void setErrorMessage(String var1) {
      System.err.println(var1);
   }

   public void clearFilter() {
      this.panel.getInventoryIcons().clearFilter();
   }
}
