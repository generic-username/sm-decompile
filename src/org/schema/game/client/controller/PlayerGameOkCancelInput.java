package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.forms.font.FontStyle;

public abstract class PlayerGameOkCancelInput extends PlayerOkCancelInput {
   public PlayerGameOkCancelInput(String var1, GameClientState var2, int var3, int var4, Object var5, Object var6, FontStyle var7) {
      super(var1, var2, var3, var4, var5, var6, var7);
   }

   public PlayerGameOkCancelInput(String var1, GameClientState var2, int var3, int var4, Object var5, Object var6) {
      super(var1, var2, var3, var4, var5, var6);
   }

   public PlayerGameOkCancelInput(String var1, GameClientState var2, Object var3, Object var4, FontStyle var5) {
      super(var1, var2, var3, var4, var5);
   }

   public PlayerGameOkCancelInput(String var1, GameClientState var2, Object var3, Object var4) {
      super(var1, var2, var3, var4);
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }
}
