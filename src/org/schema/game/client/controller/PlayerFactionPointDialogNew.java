package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.faction.newfaction.FactionPointRevenueScrollableListNew;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;

public class PlayerFactionPointDialogNew extends PlayerGameOkCancelInput {
   public PlayerFactionPointDialogNew(GameClientState var1) {
      super("PlayerFactionPointDialogNew", var1, 640, 480, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERFACTIONPOINTDIALOGNEW_0, "");
      this.getInputPanel().setOkButton(false);
      this.getInputPanel().setCancelButtonText(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERFACTIONPOINTDIALOGNEW_1);
      this.getInputPanel().onInit();
      FactionPointRevenueScrollableListNew var2;
      (var2 = new FactionPointRevenueScrollableListNew(var1, ((GUIDialogWindow)this.getInputPanel().getBackground()).getMainContentPane().getContent(0), var1.getFaction())).onInit();
      ((GUIDialogWindow)this.getInputPanel().getBackground()).getMainContentPane().getContent(0).attach(var2);
   }

   public void onDeactivate() {
   }

   public void pressedOK() {
   }
}
