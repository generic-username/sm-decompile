package org.schema.game.client.controller;

import org.schema.game.common.data.player.PlayerState;

public class TextureSynchronizationState {
   private final PlayerState player;
   public boolean requested;
   private long clientTimeStamp = -1L;
   private long serverTimeStamp = -1L;
   private long clientSize = -1L;
   private long serverSize = -1L;
   private boolean timeStampRequested = false;
   private boolean received;

   public TextureSynchronizationState(PlayerState var1) {
      this.player = var1;
      this.clientTimeStamp = var1.getSkinManager().getTimeStamp();
   }

   public long getClientSize() {
      return this.clientSize;
   }

   public void setClientSize(long var1) {
      this.clientSize = var1;
   }

   public long getClientTimeStamp() {
      return this.clientTimeStamp;
   }

   public void setClientTimeStamp(long var1) {
      this.clientTimeStamp = var1;
   }

   public PlayerState getPlayer() {
      return this.player;
   }

   public long getServerSize() {
      return this.serverSize;
   }

   public void setServerSize(long var1) {
      this.serverSize = var1;
   }

   public long getServerTimeStamp() {
      return this.serverTimeStamp;
   }

   public void setServerTimeStamp(long var1) {
      this.serverTimeStamp = var1;
   }

   public boolean isReceived() {
      return this.received;
   }

   public void setReceived(boolean var1) {
      this.received = var1;
   }

   public boolean isTimeStampRequested() {
      return this.timeStampRequested;
   }

   public void setTimeStampRequested(boolean var1) {
      this.timeStampRequested = var1;
   }
}
