package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.common.JoystickInputHandler;

public abstract class PlayerJoystickInput extends PlayerInput implements JoystickInputHandler {
   public PlayerJoystickInput(GameClientState var1) {
      super(var1);
   }
}
