package org.schema.game.client.controller;

import java.util.Iterator;
import org.schema.game.common.controller.ParticleEntry;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.network.objects.remote.RemoteParticleEntry;
import org.schema.game.server.data.GameServerState;

public class ParticleUtil {
   public static void sendToAllPlayers(GameServerState var0, ParticleEntry var1) {
      Iterator var2 = var0.getPlayerStatesByName().values().iterator();

      while(var2.hasNext()) {
         sendToPlayer((PlayerState)var2.next(), var1);
      }

   }

   public static void sendToPlayer(PlayerState var0, ParticleEntry var1) {
      var0.getClientChannel().getNetworkObject().particles.add(new RemoteParticleEntry(var1, true));
   }
}
