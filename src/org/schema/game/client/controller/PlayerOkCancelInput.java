package org.schema.game.client.controller;

import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontStyle;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public abstract class PlayerOkCancelInput extends DialogInput {
   private final GUIInputPanel inputPanel;

   public PlayerOkCancelInput(String var1, InputState var2, Object var3, Object var4) {
      super(var2);
      this.inputPanel = new GUIInputPanel(var1, var2, this, var3, var4);
      this.inputPanel.setCallback(this);
   }

   public PlayerOkCancelInput(String var1, InputState var2, int var3, int var4, Object var5, Object var6) {
      super(var2);
      this.inputPanel = new GUIInputPanel(var1, var2, var3, var4, this, var5, var6);
      this.inputPanel.setCallback(this);
   }

   public PlayerOkCancelInput(String var1, InputState var2, Object var3, Object var4, FontStyle var5) {
      super(var2);
      this.inputPanel = new GUIInputPanel(var1, var2, this, var3, var4, var5);
      this.inputPanel.setCallback(this);
   }

   public PlayerOkCancelInput(String var1, InputState var2, int var3, int var4, Object var5, Object var6, FontStyle var7) {
      super(var2);
      this.inputPanel = new GUIInputPanel(var1, var2, var3, var4, this, var5, var6, var7);
      this.inputPanel.setCallback(this);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (!this.isOccluded() && var2.pressedLeftMouse()) {
         if (var1.getUserPointer().equals("OK")) {
            this.pressedOK();
         }

         if (var1.getUserPointer().equals("SECOND_OPTION")) {
            this.pressedSecondOption();
         }

         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            this.cancel();
         }
      }

   }

   public void pressedSecondOption() {
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, this.getState())) {
         if (this.isDeactivateOnEscape() && KeyboardMappings.getEventKeyRaw(var1) == 1) {
            this.deactivate();
            return;
         }

         if (!this.allowChat() && (KeyboardMappings.getEventKeyRaw(var1) == 28 || KeyboardMappings.getEventKeyRaw(var1) == 156)) {
            this.pressedOK();
            return;
         }
      }

   }

   public GUIInputPanel getInputPanel() {
      return this.inputPanel;
   }

   public abstract void onDeactivate();

   public void handleMouseEvent(MouseEvent var1) {
   }

   public abstract void pressedOK();

   public void setErrorMessage(String var1) {
      this.inputPanel.setErrorMessage(var1, 2000L);
   }
}
