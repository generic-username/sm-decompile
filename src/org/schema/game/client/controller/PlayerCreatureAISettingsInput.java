package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.ai.CreatureAiSettingsPanel;
import org.schema.schine.ai.stateMachines.AiInterface;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;

public class PlayerCreatureAISettingsInput extends PlayerInput {
   private CreatureAiSettingsPanel panel;

   public PlayerCreatureAISettingsInput(GameClientState var1, AiInterface var2) {
      super(var1);
      this.panel = new CreatureAiSettingsPanel(var1, var2, this, "AISettings", "");
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.getEventButtonState() && var2.getEventButton() == 0 && (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X"))) {
         System.err.println("CANCEL");
         this.cancel();
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
   }

   public GUIElement getInputPanel() {
      return this.panel;
   }

   public void onDeactivate() {
   }

   public boolean isOccluded() {
      return false;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }
}
