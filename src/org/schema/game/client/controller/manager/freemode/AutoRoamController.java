package org.schema.game.client.controller.manager.freemode;

import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.game.client.controller.JoinMenu;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.camera.AutoViewerCamera;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.camera.viewer.FixedViewer;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.network.objects.container.TransformTimed;

public class AutoRoamController extends AbstractControlManager {
   static final float minSpeed = 5.0F;
   static final float fastSpeed = 50.0F;
   private AutoViewerCamera camera;

   public AutoRoamController(GameClientState var1) {
      super(var1);
   }

   private void initialize() {
   }

   public void onSwitch(boolean var1) {
      CameraMouseState.setGrabbed(!var1);
      if (var1) {
         System.err.println("[CLIENT][CONTROLLER] Switched to auto roam (spawn screen)");
         this.getState().setPlayerSpawned(false);
         boolean var2 = false;
         Iterator var3 = this.getState().getController().getPlayerInputs().iterator();

         while(var3.hasNext()) {
            if ((DialogInterface)var3.next() instanceof JoinMenu) {
               var2 = true;
               break;
            }
         }

         if (!var2) {
            JoinMenu var4 = new JoinMenu(this.getState());
            this.getState().getController().getPlayerInputs().add(var4);
         }
      }

      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      super.update(var1);
      CameraMouseState.setGrabbed(false);
      if (this.camera == null) {
         this.setCamera();
      }

      if (Controller.getCamera() != this.camera) {
         Controller.setCamera(this.camera);
         this.camera.update(var1, false);
      }

   }

   private void setCamera() {
      final TransformTimed var1;
      (var1 = new TransformTimed()).setIdentity();
      FixedViewer var3 = new FixedViewer(new Transformable() {
         public TransformTimed getWorldTransform() {
            return var1;
         }
      });
      Vector3f var2;
      if ((var2 = new Vector3f(this.getState().getScene().getLight().getPos())).lengthSquared() == 0.0F) {
         var2.set(0.0F, 0.0F, 1.0F);
      }

      var2.negate();
      var2.normalize();
      this.camera = new AutoViewerCamera(this.getState(), var3, new Vector3f(125.0F, 70.0F, 223.0F), var2);
   }
}
