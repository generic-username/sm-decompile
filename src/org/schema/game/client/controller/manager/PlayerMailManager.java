package org.schema.game.client.controller.manager;

import java.util.Iterator;
import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.controller.PlayerMessagesPlayerInput;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.input.KeyEventInterface;

public class PlayerMailManager extends AbstractControlManager {
   public PlayerMailManager(GameClientState var1) {
      super(var1);
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public void handleMouseEvent(MouseEvent var1) {
      super.handleMouseEvent(var1);
      PlayerInput.lastDialougeClick = System.currentTimeMillis();
   }

   public void onSwitch(boolean var1) {
      boolean var2 = false;
      if (var1) {
         synchronized(this.getState().getController().getPlayerInputs()) {
            Iterator var4 = this.getState().getController().getPlayerInputs().iterator();

            while(true) {
               if (!var4.hasNext()) {
                  break;
               }

               if ((DialogInterface)var4.next() instanceof PlayerMessagesPlayerInput) {
                  var2 = true;
               }
            }
         }

         if (!var2) {
            PlayerMessagesPlayerInput var3 = new PlayerMessagesPlayerInput(this.getState());
            this.getState().getController().getPlayerInputs().add(var3);
         }
      }

      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      CameraMouseState.setGrabbed(false);
      super.update(var1);
   }
}
