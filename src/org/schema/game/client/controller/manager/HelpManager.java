package org.schema.game.client.controller.manager;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public class HelpManager extends AbstractControlManager {
   private int page;
   private int pages = 3;

   public HelpManager(GameClientState var1) {
      super(var1);
   }

   public int getPage() {
      return this.page;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
      if (KeyboardMappings.getEventKeyState(var1, this.getState())) {
         switch(KeyboardMappings.getEventKeySingle(var1)) {
         case 203:
            --this.page;
            if (this.page < 0) {
               this.page = this.pages - 1;
               return;
            }
            break;
         case 205:
            this.page = (this.page + 1) % this.pages;
         }
      }

   }
}
