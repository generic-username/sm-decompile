package org.schema.game.client.controller.manager;

import java.util.Iterator;
import org.schema.game.client.controller.MainMenu;
import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.input.KeyEventInterface;

public class MainMenuManager extends AbstractControlManager {
   public MainMenuManager(GameClientState var1) {
      super(var1);
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public void handleMouseEvent(MouseEvent var1) {
      super.handleMouseEvent(var1);
      PlayerInput.lastDialougeClick = System.currentTimeMillis();
   }

   public void onSwitch(boolean var1) {
      boolean var2 = false;
      if (var1) {
         synchronized(this.getState().getController().getPlayerInputs()) {
            Iterator var4 = this.getState().getController().getPlayerInputs().iterator();

            while(true) {
               if (!var4.hasNext()) {
                  break;
               }

               if ((DialogInterface)var4.next() instanceof MainMenu) {
                  var2 = true;
               }
            }
         }

         if (!var2) {
            (new MainMenu(this.getState())).activate();
         }
      }

      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController().suspend(var1);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().suspend(var1);
      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      CameraMouseState.setGrabbed(false);
      super.update(var1);
   }
}
