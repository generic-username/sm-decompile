package org.schema.game.client.controller.manager.ingame.shop;

import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.common.TextCallback;

public abstract class QuantityDialog extends PlayerGameTextInput {
   protected final Object descriptionObject;
   protected short element;

   public QuantityDialog(GameClientState var1, short var2, String var3, Object var4, int var5) {
      super("QuantityDialog", var1, 10, var3, var4, String.valueOf(var5));
      this.descriptionObject = var4;
      this.element = var2;
   }

   public QuantityDialog(GameClientState var1, short var2, String var3, Object var4, int var5, int var6) {
      super("QuantityDialog", var1, var6, var3, var4, String.valueOf(var5));
      this.descriptionObject = var4;
      this.element = var2;
   }

   public String[] getCommandPrefixes() {
      return null;
   }

   public String handleAutoComplete(String var1, TextCallback var2, String var3) {
      return var1;
   }

   public void onFailedTextCheck(String var1) {
      this.setErrorMessage(var1);
   }

   public Object getDescriptionObject() {
      return this.descriptionObject;
   }

   public boolean isOccluded() {
      return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
   }

   public void onDeactivate() {
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getShopControlManager().suspend(false);
   }
}
