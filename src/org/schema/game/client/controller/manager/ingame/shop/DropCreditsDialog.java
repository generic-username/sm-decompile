package org.schema.game.client.controller.manager.ingame.shop;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;

public class DropCreditsDialog extends QuantityDialog {
   public DropCreditsDialog(GameClientState var1, int var2) {
      super(var1, (short)-2, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_DROPCREDITSDIALOG_0, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_DROPCREDITSDIALOG_1, var2, 10);
      this.setInputChecker(new InputChecker() {
         public boolean check(String var1, TextCallback var2) {
            try {
               if (var1.length() > 0) {
                  if (Integer.parseInt(var1) <= 0) {
                     DropCreditsDialog.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_DROPCREDITSDIALOG_2, 0.0F);
                     return false;
                  }

                  DropCreditsDialog.this.getState().getController().queueUIAudio("0022_action - purchase with credits");
                  return true;
               }
            } catch (NumberFormatException var3) {
            }

            var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_DROPCREDITSDIALOG_3);
            return false;
         }
      });
      this.getTextInput().setOnInputChangedCallback(new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            try {
               ((BuyDesc)DropCreditsDialog.this.descriptionObject).wantedQuantity = Integer.parseInt(var1);
            } catch (Exception var2) {
            }

            return var1;
         }
      });
   }

   public boolean onInput(String var1) {
      int var2 = Integer.parseInt(var1);
      this.getState().getPlayer().getNetworkObject().creditsDropBuffer.add(var2);
      return true;
   }
}
