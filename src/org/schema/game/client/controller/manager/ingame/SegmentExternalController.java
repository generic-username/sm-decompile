package org.schema.game.client.controller.manager.ingame;

import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.controller.manager.ingame.navigation.NavigationControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.camera.InShipCamera;
import org.schema.game.client.view.camera.ShipOffsetCameraViewable;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.camera.Camera;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.objects.container.PhysicsDataContainer;

public class SegmentExternalController extends AbstractControlManager {
   private final TargetAquireHelper aquire = new TargetAquireHelper();
   Vector3f force = new Vector3f();
   Vector3i cockpitTmp = new Vector3i();
   private Camera shipCamera;

   public SegmentExternalController(GameClientState var1) {
      super(var1);
   }

   public SegmentPiece getEntered() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSegmentControlManager().getEntered();
   }

   public Vector3i getEntered(Vector3i var1) {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSegmentControlManager().getEntered().getAbsolutePos(var1);
   }

   public PhysicsDataContainer getPhysicsData() {
      return this.getEntered().getSegment().getSegmentController().getPhysicsDataContainer();
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, this.getState()) && KeyboardMappings.getEventKeyRaw(var1) >= 2 && KeyboardMappings.getEventKeyRaw(var1) <= 11) {
         if (Keyboard.isKeyDown(KeyboardMappings.SCROLL_BOTTOM_BAR.getMapping())) {
            this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().setSelectedWeaponBottomBar((byte)(KeyboardMappings.getEventKeyRaw(var1) - 2));
            return;
         }

         this.numberKeyPressed((byte)(this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().getSelectedWeaponBottomBar() * 10 + KeyboardMappings.getEventKeyRaw(var1) - 2));
      }

   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public void onSwitch(boolean var1) {
      this.aquire.flagSlotChange();
      SegmentControlManager var2 = this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSegmentControlManager();
      if (this.getState().getPlayer() != null) {
         this.getState().getPlayer().setAquiredTarget((SimpleTransformableSendableObject)null);
      }

      if (var1) {
         this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getBuildToolsManager().load(this.getEntered().getSegment().getSegmentController().getUniqueIdentifier());
         this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getBuildToolsManager().user = this.getEntered().getSegment().getSegmentController().getUniqueIdentifier();
         System.err.println("[CLIENT][SEGMENTEXTERNALEFT_CONTROLLER] ENTERED: " + this.getEntered());
         this.shipCamera = new InShipCamera(var2, Controller.getCamera(), this.getEntered());
         this.shipCamera.setCameraStartOffset(0.0F);
         Vector3i var3;
         (var3 = var2.getEntered().getAbsolutePos(new Vector3i())).sub(Ship.core);
         ((ShipOffsetCameraViewable)this.shipCamera.getViewable()).getPosMod().set(var3);
         ((ShipOffsetCameraViewable)this.shipCamera.getViewable()).setJumpToBlockSpeed(50.0F);
         Controller.setCamera(this.shipCamera);
      } else {
         this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getBuildToolsManager().save(this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getBuildToolsManager().user);
         this.shipCamera = null;
      }

      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      super.update(var1);
      CameraMouseState.setGrabbed(true);
      this.aquire.update(this.getEntered(), var1);
   }

   private void numberKeyPressed(byte var1) {
      this.getState().getPlayer().setCurrentShipControllerSlot(var1, 0.0F);
      this.aquire.flagSlotChange();
   }

   public NavigationControllerManager getNavigationControllerManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getNavigationControlManager();
   }

   public TargetAquireHelper getAquire() {
      return this.aquire;
   }
}
