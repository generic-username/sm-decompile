package org.schema.game.client.controller.manager.ingame.ship;

import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.client.view.buildhelper.BuildHelper;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SegmentControllerHpControllerInterface;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public class InShipControlManager extends AbstractControlManager {
   long lastFreeCamKey;
   private ShipControllerManager shipControlManager;
   private Vector3i eneteredFromShipyard;

   public InShipControlManager(GameClientState var1) {
      super(var1);
      this.initialize();
   }

   public SegmentPiece getEntered() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getEntered();
   }

   public void setEntered(SegmentPiece var1) {
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().setEntered(var1);
   }

   public static void switchEntered(SegmentController var0) {
      GameClientState var1;
      PlayerInteractionControlManager var2;
      SegmentPiece var4;
      (var4 = (var2 = (var1 = (GameClientState)var0.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager()).getEntered()).getSegment().getSegmentController();
      var2.getEntered().getAbsolutePos(new Vector3i());
      SegmentPiece var3;
      if ((var3 = var0.getSegmentBuffer().getPointUnsave(Ship.core)) != null) {
         LongOpenHashSet var5;
         if ((!ElementKeyMap.isValidType(var3.getType()) || !ElementKeyMap.getInfoFast(var3.getType()).isEnterable()) && !(var0 instanceof Ship) && var0 instanceof ManagedSegmentController && (var5 = ((ManagedSegmentController)var0).getManagerContainer().getBuildBlocks()).size() > 0) {
            var3 = var0.getSegmentBuffer().getPointUnsave(var5.iterator().nextLong());
         }

         if (var3 != null) {
            assert var4.getSegmentController() != var0 : var4.getSegmentController();

            var2.getInShipControlManager().setEntered(var3);
            var1.getController().requestControlChange((PlayerControllable)var4.getSegmentController(), (PlayerControllable)var2.getEntered().getSegmentController(), var4.getAbsolutePos(new Vector3i()), var2.getEntered().getAbsolutePos(new Vector3i()), true);
            return;
         }
      } else {
         var1.getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHIP_INSHIPCONTROLMANAGER_1, 0.0F);
      }

   }

   public static boolean checkEnter(SegmentPiece var0) {
      return var0 != null ? ((GameClientState)var0.getSegmentController().getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getPlayerCharacterManager().checkEnter(var0) : false;
   }

   public static boolean checkEnterDry(SegmentPiece var0, boolean var1) {
      return var0 != null ? ((GameClientState)var0.getSegmentController().getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getPlayerCharacterManager().checkEnterDry(var0, var1) : false;
   }

   public static boolean checkEnter(SegmentController var0) {
      PlayerInteractionControlManager var1;
      SegmentPiece var10000 = (var1 = ((GameClientState)var0.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager()).getEntered();
      LongOpenHashSet var2 = null;
      var10000.getSegment().getSegmentController();
      var1.getEntered().getAbsolutePos(new Vector3i());
      SegmentPiece var3;
      if ((var3 = var0.getSegmentBuffer().getPointUnsave(Ship.core)) != null) {
         if ((!ElementKeyMap.isValidType(var3.getType()) || !ElementKeyMap.getInfoFast(var3.getType()).isEnterable()) && !(var0 instanceof Ship) && var0 instanceof ManagedSegmentController && (var2 = ((ManagedSegmentController)var0).getManagerContainer().getBuildBlocks()).size() > 0) {
            var3 = var0.getSegmentBuffer().getPointUnsave(var2.iterator().nextLong());
         }

         if (var3 != null && ElementKeyMap.isValidType(var3.getType()) && ElementKeyMap.getInfoFast(var3.getType()).isEnterable()) {
            return checkEnterDry(var3, true);
         }
      }

      return false;
   }

   public void exitShip(boolean var1) {
      if (this.getEntered() != null) {
         PlayerInteractionControlManager var2;
         if ((var2 = this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager()).getBuildToolsManager().getBuildHelper() != null) {
            var2.getBuildToolsManager().getBuildHelper().clean();
            var2.getBuildToolsManager().setBuildHelper((BuildHelper)null);
         }

         final SegmentController var6 = this.getEntered().getSegment().getSegmentController();
         final Vector3i var4 = new Vector3i();
         final Vector3i var5 = this.getEntered().getAbsolutePos(new Vector3i());
         if (this.eneteredFromShipyard != null) {
            this.eneteredFromShipyard = null;
         }

         final PlayerControllable var3 = (PlayerControllable)var6;
         if (!var1 && (Float)EngineSettings.G_MUST_CONFIRM_DETACHEMENT_AT_SPEED.getCurrentState() >= 0.0F && (double)var6.getSpeedPercentServerLimitCurrent() >= (double)(Float)EngineSettings.G_MUST_CONFIRM_DETACHEMENT_AT_SPEED.getCurrentState() * 0.01D) {
            (new PlayerGameOkCancelInput("CONFIRM_Exit", this.getState(), "Exit", "Do you really want to do that.\nThe current object was flying at " + StringTools.formatPointZero(var6.getSpeedCurrent()) + " speed\n\n(this message can be customized or\nturned off in the game option\n'Popup Detach Warning' in the ingame options menu)") {
               public boolean isOccluded() {
                  return false;
               }

               public void onDeactivate() {
               }

               public void pressedOK() {
                  if (InShipControlManager.this.getEntered() != null) {
                     System.err.println("[CLIENT] EXIT SHIP FROM EXTRYPOINT " + var5 + " OF " + var6 + "; segSec: " + var6.getSectorId() + "; ");
                     this.getState().getController().requestControlChange(var3, this.getState().getCharacter(), var5, var4, true);
                     InShipControlManager.this.setEntered((SegmentPiece)null);
                     InShipControlManager.this.setActive(false);
                  }

                  this.deactivate();
               }
            }).activate();
            return;
         }

         System.err.println("[CLIENT] EXIT SHIP FROM EXTRYPOINT " + var5 + " OF " + var6 + "; segSec: " + var6.getSectorId() + "; ");
         this.getState().getController().requestControlChange(var3, this.getState().getCharacter(), var5, var4, true);
         this.setEntered((SegmentPiece)null);
         this.setActive(false);
      }

   }

   public ShipControllerManager getShipControlManager() {
      return this.shipControlManager;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
      if (KeyboardMappings.getEventKeyState(var1, this.getState())) {
         if (KeyboardMappings.REBOOT_SYSTEMS.isEventKey(var1, this.getState()) && this.getState().getShip() != null) {
            this.popupShipRebootDialog(this.getState().getShip());
         }

         if (KeyboardMappings.FREE_CAM.isEventKey(var1, this.getState())) {
            if (!Controller.FREE_CAM_STICKY) {
               if (System.currentTimeMillis() - this.lastFreeCamKey < 300L) {
                  Controller.FREE_CAM_STICKY = true;
                  this.getState().getController().popupInfoTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHIP_INSHIPCONTROLMANAGER_0, KeyboardMappings.FREE_CAM.getKeyChar()), 0.0F);
               } else {
                  this.lastFreeCamKey = System.currentTimeMillis();
               }
            } else {
               Controller.FREE_CAM_STICKY = false;
            }
         }

         SegmentPiece var2;
         if (KeyboardMappings.ENTER_SHIP.isEventKey(var1, this.getState())) {
            if (KeyboardMappings.ENTER_SHIP.getMapping() == KeyboardMappings.ACTIVATE.getMapping() && this.shipControlManager.getSegmentBuildController().isActive()) {
               if ((var2 = this.shipControlManager.getSegmentBuildController().getCurrentSegmentPiece()) != null && ElementKeyMap.isValidType(var2.getType()) && (ElementKeyMap.getInfo(var2.getType()).canActivate() || var2.getType() == 978 || var2.getType() == 2 || var2.getType() == 331 || var2.getType() == 3 || var2.getType() == 478) && !ElementKeyMap.getInfo(var2.getType()).isEnterable()) {
                  System.err.println("[CLIENT] CHECKING ACTIVATE OF " + var2 + " FROM INSIDE BUILD MODE");
                  this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().checkActivate(var2);
                  return;
               }

               this.exitShip(false);
               return;
            }

            this.exitShip(false);
            return;
         }

         if (KeyboardMappings.ACTIVATE.isEventKey(var1, this.getState()) && this.shipControlManager.getSegmentBuildController().isActive() && (var2 = this.shipControlManager.getSegmentBuildController().getCurrentSegmentPiece()) != null) {
            this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().checkActivate(var2);
         }
      }

   }

   public void onSwitch(boolean var1) {
      if (var1 && this.getEntered() != null) {
         Ship var2 = (Ship)this.getEntered().getSegmentController();
         this.getState().setShip(var2);

         assert this.getEntered() != null;

         this.shipControlManager.setActive(true);
         this.getEntered().getSegmentController();
         this.getEntered().getAbsolutePos(new Vector3i());
      } else {
         if (this.getState().getShip() == this.getState().getCurrentPlayerObject()) {
            this.getState().setCurrentPlayerObject((SimpleTransformableSendableObject)null);
         }

         this.getState().setShip((Ship)null);
      }

      assert !var1 || this.getState().getShip() != null : ": Entered: " + (this.getEntered() != null ? this.getEntered().getSegment().getSegmentController() : "null") + " -> " + (this.getEntered() != null ? this.getEntered().getAbsolutePos(new Vector3i()) : "null");

      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      super.update(var1);
      if (this.getEntered() == null) {
         System.err.println("[CLIENT] CANNOT UPDATE inShipControlManager: entered is null");
      } else if (!this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(this.getEntered().getSegment().getSegmentController().getId())) {
         System.err.println("[CLIENT][InShipControlManager] Entered object no longer exists");
         this.exitShip(true);
         this.setActive(false);
      } else {
         if (this.getEntered() != null) {
            this.getEntered().refresh();
            if (this.getEntered().getSegment().getSegmentController().getSegmentBuffer().containsKey(this.getEntered().getSegment().pos) && ((RemoteSegment)this.getEntered().getSegment()).getLastChanged() > 0L && this.getEntered().getType() == 0) {
               Vector3i var2 = this.getEntered().getAbsolutePos(new Vector3i());
               SegmentPiece var3;
               if ((var3 = this.getEntered().getSegment().getSegmentController().getSegmentBuffer().getPointUnsave(var2)) != null) {
                  if (var3.getType() == 0) {
                     System.err.println("[CLIENT][InShipControlManager] POINT BECAME AIR -> exiting ship; lastChanged: " + ((RemoteSegment)this.getEntered().getSegment()).getLastChanged());
                     this.exitShip(true);
                     this.setActive(false);
                     return;
                  }

                  this.setEntered(var3);
               }
            }

            if (!this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(this.getEntered().getSegment().getSegmentController().getId())) {
               this.exitShip(true);
               this.setActive(false);
               return;
            }
         }

      }
   }

   public void initialize() {
      this.shipControlManager = new ShipControllerManager(this.getState());
      this.getControlManagers().add(this.shipControlManager);
   }

   public void popupShipRebootDialog(final SegmentController var1) {
      final SegmentControllerHpControllerInterface var2;
      if (!(var2 = var1.getHpController()).isRebooting()) {
         var2.setRequestedTimeClient(false);
         Object var3 = new Object() {
            public String toString() {
               String var1x = "calculating...";
               if (var2.getRebootTimeMS() > 0L) {
                  var1x = StringTools.formatTimeFromMS(var2.getRebootTimeMS());
               }

               PowerInterface var2x;
               if (var1 instanceof ManagedSegmentController && (var2x = ((ManagedSegmentController)var1).getManagerContainer().getPowerInterface()).isUsingPowerReactors()) {
                  float var3;
                  if ((var3 = Math.max(var2x.getReactorRebootCooldown(), var2x.getReactorSwitchCooldown())) > 0.0F) {
                     var1x = StringTools.formatTimeFromMS((long)(var3 * 1000.0F));
                  } else {
                     var1x = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHIP_INSHIPCONTROLMANAGER_16;
                  }
               }

               return Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHIP_INSHIPCONTROLMANAGER_6 + "and it will set the HP according to your current blocks.\n\n" + (var1 instanceof SpaceStation ? Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHIP_INSHIPCONTROLMANAGER_8 : "") + Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHIP_INSHIPCONTROLMANAGER_17 + var1x;
            }
         };
         PlayerGameOkCancelInput var4;
         (var4 = new PlayerGameOkCancelInput("CONFIRM", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHIP_INSHIPCONTROLMANAGER_11, var3) {
            public boolean isOccluded() {
               return false;
            }

            public void onDeactivate() {
            }

            public void pressedOK() {
               float var1x = 0.0F;
               if (var1 instanceof ManagedSegmentController) {
                  PowerInterface var2;
                  var1x = Math.max((var2 = ((ManagedSegmentController)var1).getManagerContainer().getPowerInterface()).getReactorRebootCooldown(), var2.getReactorSwitchCooldown());
               }

               if (var1 instanceof SegmentController && var1x <= 0.0F) {
                  this.getState().getController().queueUIAudio("0022_action - buttons push medium");
                  var1.getHpController().reboot(false);
                  this.deactivate();
               }

            }
         }).getInputPanel().getButtonOK().setText(new Object() {
            public String toString() {
               InShipControlManager.this.getState().getController();
               float var1x = 0.0F;
               if (var1 instanceof ManagedSegmentController) {
                  PowerInterface var2;
                  var1x = Math.max((var2 = ((ManagedSegmentController)var1).getManagerContainer().getPowerInterface()).getReactorRebootCooldown(), var2.getReactorSwitchCooldown());
               }

               if (var1x <= 0.0F) {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHIP_INSHIPCONTROLMANAGER_14;
               } else {
                  InShipControlManager.this.getState().getController();
                  return Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHIP_INSHIPCONTROLMANAGER_15 + "(" + (int)Math.ceil((double)var1x) + ")";
               }
            }
         });
         var4.activate();
      }

   }

   public void popupShipRebootBuyDialog() {
      SimpleTransformableSendableObject var1;
      if ((var1 = this.getState().getCurrentPlayerObject()) instanceof SegmentController && !((SegmentController)var1).getHpController().isRebooting()) {
         final SegmentControllerHpControllerInterface var2;
         (var2 = ((SegmentController)var1).getHpController()).setRequestedTimeClient(false);
         Object var3 = new Object() {
            public String toString() {
               String var10000 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHIP_INSHIPCONTROLMANAGER_2;
               if (var2.getRebootTimeMS() > 0L) {
                  StringTools.formatTimeFromMS(var2.getRebootTimeMS());
               }

               SimpleTransformableSendableObject var1;
               PowerInterface var2x;
               if ((var1 = InShipControlManager.this.getState().getCurrentPlayerObject()) instanceof ManagedSegmentController && (var2x = ((ManagedSegmentController)var1).getManagerContainer().getPowerInterface()).isUsingPowerReactors() && var2x.isUsingPowerReactors()) {
                  float var3;
                  if ((var3 = Math.max(var2x.getReactorRebootCooldown(), var2x.getReactorSwitchCooldown())) > 0.0F) {
                     StringTools.formatTimeFromMS((long)(var3 * 1000.0F));
                  } else {
                     var10000 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHIP_INSHIPCONTROLMANAGER_9;
                  }
               }

               return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHIP_INSHIPCONTROLMANAGER_10, var2.getShopRebootCost(), InShipControlManager.this.getState().getPlayer().getCredits());
            }
         };
         PlayerGameOkCancelInput var4;
         (var4 = new PlayerGameOkCancelInput("CONFIRM", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHIP_INSHIPCONTROLMANAGER_4, var3) {
            public boolean isOccluded() {
               return false;
            }

            public void onDeactivate() {
            }

            public void pressedOK() {
               SimpleTransformableSendableObject var1;
               if ((var1 = this.getState().getCurrentPlayerObject()) instanceof SegmentController && !((SegmentController)var1).getHpController().isRebooting()) {
                  this.getState().getController().queueUIAudio("0022_action - buttons push medium");
                  ((SegmentController)var1).getHpController().reboot(true);
                  this.deactivate();
               } else {
                  System.err.println("REBOOTING ALREADY!");
               }
            }
         }).getInputPanel().getButtonOK().setText(new Object() {
            public String toString() {
               InShipControlManager.this.getState().getController();
               float var1 = 0.0F;
               SimpleTransformableSendableObject var2;
               if ((var2 = InShipControlManager.this.getState().getCurrentPlayerObject()) instanceof ManagedSegmentController) {
                  PowerInterface var3;
                  var1 = Math.max((var3 = ((ManagedSegmentController)var2).getManagerContainer().getPowerInterface()).getReactorRebootCooldown(), var3.getReactorSwitchCooldown());
               }

               if (var1 <= 0.0F) {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHIP_INSHIPCONTROLMANAGER_12;
               } else {
                  InShipControlManager.this.getState().getController();
                  return Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHIP_INSHIPCONTROLMANAGER_13 + "(" + (int)Math.ceil((double)var1) + ")";
               }
            }
         });
         var4.activate();
      }

   }

   public void enteredFromShipyard(Vector3i var1) {
      this.eneteredFromShipyard = var1;
   }
}
