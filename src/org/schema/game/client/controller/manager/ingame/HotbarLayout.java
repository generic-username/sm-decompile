package org.schema.game.client.controller.manager.ingame;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.Map.Entry;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.schine.resource.FileExt;

public class HotbarLayout {
   private Object2ObjectOpenHashMap hotbars = new Object2ObjectOpenHashMap();
   private GameClientState state;
   private int version = 0;
   public static String PATH = "./hotbarLayouts";

   public HotbarLayout(GameClientState var1) {
      this.state = var1;
      this.readHotbars();
   }

   public Object2ObjectOpenHashMap getLayouts() {
      return this.hotbars;
   }

   private PlayerInteractionControlManager getPIM() {
      return this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager();
   }

   public void setCurrentHotbar(String var1) {
      short[] var7 = (short[])this.hotbars.get(var1);
      Inventory var2 = this.state.getPlayer().getInventory((Vector3i)null);
      if (var7 != null) {
         for(int var3 = 0; var3 < var7.length; ++var3) {
            int var4 = this.getPIM().getSlotKey(var3 + 2);
            short var5 = var7[var3];
            int var6 = var2.getFirstSlot(var5, true);
            if (ElementKeyMap.isValidType(var5) && var6 >= 0 && var2.getSlot(var6) != null) {
               var2.switchSlotsOrCombineClient(var4, var6, var2.getSlot(var6).count());
            }
         }
      }

   }

   public void removeHotbarLayout(String var1) {
      this.hotbars.remove(var1);
   }

   public boolean containsHotbar(String var1) {
      return this.hotbars.containsKey(var1);
   }

   public void addHotbarLayout(String var1) {
      short[] var2 = new short[10];

      for(int var3 = 0; var3 < var2.length; ++var3) {
         int var4 = this.getPIM().getSlotKey(var3 + 2);
         short var5;
         if (ElementKeyMap.isValidType(var5 = this.state.getPlayer().getInventory((Vector3i)null).getType(var4))) {
            var2[var3] = var5;
         }
      }

      this.hotbars.put(var1, var2);
   }

   public void writeHotbars() {
      try {
         FileExt var1 = new FileExt(PATH);
         ObjectOutputStream var2 = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(var1)));

         try {
            if (!var1.exists()) {
               var1.createNewFile();
            }

            var2.writeInt(this.version);
            var2.writeInt(this.hotbars.entrySet().size());
            Iterator var9 = this.hotbars.entrySet().iterator();

            while(var9.hasNext()) {
               Entry var3 = (Entry)var9.next();
               var2.writeUTF((String)var3.getKey());
               var2.writeInt(((short[])var3.getValue()).length);

               for(int var4 = 0; var4 < ((short[])var3.getValue()).length; ++var4) {
                  var2.writeShort(((short[])var3.getValue())[var4]);
               }
            }
         } finally {
            var2.close();
         }

      } catch (IOException var8) {
         var8.printStackTrace();
      }
   }

   private void readHotbars() {
      try {
         FileExt var1 = new FileExt(PATH);
         ObjectInputStream var12 = new ObjectInputStream(new BufferedInputStream(new FileInputStream(var1)));

         try {
            this.hotbars = new Object2ObjectOpenHashMap();
            var12.readInt();
            int var2 = var12.readInt();

            for(int var5 = 0; var5 < var2; ++var5) {
               String var3 = var12.readUTF();
               int var6;
               short[] var4 = new short[var6 = var12.readInt()];

               for(int var7 = 0; var7 < var6; ++var7) {
                  var4[var7] = var12.readShort();
               }

               this.hotbars.put(var3, var4);
            }
         } finally {
            var12.close();
         }

      } catch (IOException var11) {
         var11.printStackTrace();
      }
   }
}
