package org.schema.game.client.controller.manager.ingame;

import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public class SegmentControlManager extends AbstractControlManager implements EditSegmentInterface {
   private SegmentExternalController segmentExternalController;
   private SegmentBuildController segmentBuildController;
   private Vector3i lastEntered;

   public SegmentControlManager(GameClientState var1) {
      super(var1);
      this.initialize();
   }

   public void exit() {
      if (this.getEntered() != null) {
         final SegmentController var1 = this.getEntered().getSegment().getSegmentController();
         if ((Float)EngineSettings.G_MUST_CONFIRM_DETACHEMENT_AT_SPEED.getCurrentState() >= 0.0F && (double)var1.getSpeedPercentServerLimitCurrent() >= (double)(Float)EngineSettings.G_MUST_CONFIRM_DETACHEMENT_AT_SPEED.getCurrentState() * 0.01D) {
            (new PlayerGameOkCancelInput("CONFIRM_Exit", this.getState(), "Exit", "Do you really want to do that.\nThe current object was flying at " + StringTools.formatPointZero(var1.getSpeedCurrent()) + " speed\n\n(this message can be customized or\nturned off in the game option\n'Popup Detach Warning' in the ingame options menu)") {
               public boolean isOccluded() {
                  return false;
               }

               public void onDeactivate() {
               }

               public void pressedOK() {
                  if (SegmentControlManager.this.getEntered() != null) {
                     Vector3i var1x = SegmentControlManager.this.getEntered().getAbsolutePos(new Vector3i());
                     System.err.println("[SegmentConrolManager] EXIT SHIP FROM EXTRYPOINT " + var1x);
                     this.getState().getController().requestControlChange((PlayerControllable)var1, this.getState().getCharacter(), var1x, new Vector3i(), true);
                     SegmentControlManager.this.lastEntered = SegmentControlManager.this.getEntered().getAbsolutePos(new Vector3i());
                     SegmentControlManager.this.setEntered((SegmentPiece)null);
                  }

                  this.deactivate();
               }
            }).activate();
            return;
         }

         Vector3i var2 = this.getEntered().getAbsolutePos(new Vector3i());
         System.err.println("[SegmentConrolManager] EXIT SHIP FROM EXTRYPOINT " + var2);
         this.getState().getController().requestControlChange((PlayerControllable)var1, this.getState().getCharacter(), var2, new Vector3i(), true);
         this.lastEntered = this.getEntered().getAbsolutePos(new Vector3i());
         this.setEntered((SegmentPiece)null);
         this.setActive(false);
      }

   }

   public Vector3i getCore() {
      if (this.getEntered() != null) {
         return this.getEntered().getAbsolutePos(new Vector3i());
      } else {
         return this.lastEntered != null ? this.lastEntered : new Vector3i(16, 16, 16);
      }
   }

   public SegmentPiece getEntered() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getEntered();
   }

   public void setEntered(SegmentPiece var1) {
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().setEntered(var1);
   }

   public EditableSendableSegmentController getSegmentController() {
      return this.getEntered() == null ? null : (EditableSendableSegmentController)this.getEntered().getSegment().getSegmentController();
   }

   public SegmentBuildController getSegmentBuildController() {
      return this.segmentBuildController;
   }

   public SegmentExternalController getSegmentExternalController() {
      return this.segmentExternalController;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
      if (KeyboardMappings.getEventKeyState(var1, this.getState())) {
         SegmentPiece var2;
         if (KeyboardMappings.ENTER_SHIP.isEventKey(var1, this.getState())) {
            if (KeyboardMappings.ENTER_SHIP.getMapping() == KeyboardMappings.ACTIVATE.getMapping() && this.isActive()) {
               if ((var2 = this.getSegmentBuildController().getCurrentSegmentPiece()) != null && ElementKeyMap.isValidType(var2.getType()) && ElementKeyMap.getInfo(var2.getType()).canActivate() && !ElementKeyMap.getInfo(var2.getType()).isEnterable()) {
                  System.err.println("[CLIENT] CHECKING ACTIVATE OF " + var2 + " FROM INSIDE BUILD MODE");
                  this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().checkActivate(var2);
                  return;
               }

               this.exit();
               return;
            }

            this.exit();
            return;
         }

         if (KeyboardMappings.ACTIVATE.isEventKey(var1, this.getState())) {
            if (this.getSegmentBuildController().isActive()) {
               if ((var2 = this.getSegmentBuildController().getCurrentSegmentPiece()) != null) {
                  this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().checkActivate(var2);
               }

               return;
            }
         } else if (KeyboardMappings.REBOOT_SYSTEMS.isEventKey(var1, this.getState()) && this.getState().getCurrentPlayerObject() != null && this.getState().getCurrentPlayerObject() instanceof SpaceStation) {
            this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().popupShipRebootDialog((SegmentController)this.getState().getCurrentPlayerObject());
         }
      }

   }

   public void onSwitch(boolean var1) {
      if (var1) {
         assert this.getEntered() != null;

         if (this.getEntered() == null || this.getEntered().getSegment() == null) {
            System.err.println("Exception: entered has been null");
            return;
         }

         this.getEntered().getSegment().getSegmentController().getControlElementMap().getControlledElements((short)32767, this.getEntered().getAbsolutePos(new Vector3i()));
         if (this.getEntered().getType() == 123) {
            this.segmentBuildController.setActive(true);
         } else {
            this.segmentExternalController.setActive(true);
         }

         this.getEntered().getSegment().getSegmentController();
         this.getEntered().getAbsolutePos(new Vector3i());
      } else {
         System.err.println("[INSHIP] EXIT setting ship to null. " + this.getState());
         if (this.getEntered() != null && this.getEntered().getSegment().getSegmentController() == this.getState().getCurrentPlayerObject()) {
            this.getState().setCurrentPlayerObject((SimpleTransformableSendableObject)null);
         }

         this.segmentExternalController.setActive(false);
         this.segmentBuildController.setActive(false);
      }

      assert !var1 || this.getEntered() != null : ": Entered: " + this.getEntered().getSegment().getSegmentController() + " -> " + this.getEntered().getAbsolutePos(new Vector3i());

      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      super.update(var1);
      if (this.getEntered() != null && !this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(this.getEntered().getSegment().getSegmentController().getId())) {
         System.err.println("[CLIENT][SegmentControlManager] Entered object no longer exists");
         this.exit();
      }

      if (this.getEntered() != null) {
         this.getEntered().refresh();
         if (this.getEntered().getType() == 0) {
            Vector3i var2 = this.getEntered().getAbsolutePos(new Vector3i());
            SegmentPiece var3;
            if ((var3 = this.getEntered().getSegment().getSegmentController().getSegmentBuffer().getPointUnsave(var2)) != null) {
               if (var3.getType() == 0) {
                  this.exit();
               } else {
                  this.setEntered(var3);
               }
            }
         }

         if (this.getEntered() != null && !this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(this.getEntered().getSegment().getSegmentController().getId())) {
            this.exit();
         }
      }

   }

   public void initialize() {
      this.segmentExternalController = new SegmentExternalController(this.getState());
      this.segmentBuildController = new SegmentBuildController(this.getState(), this);
      this.getControlManagers().add(this.segmentExternalController);
      this.getControlManagers().add(this.segmentBuildController);
   }
}
