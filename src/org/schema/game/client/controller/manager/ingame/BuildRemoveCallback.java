package org.schema.game.client.controller.manager.ingame;

public interface BuildRemoveCallback extends BuildSelectionCallback {
   void onRemove(long var1, short var3);

   boolean canRemove(short var1);
}
