package org.schema.game.client.controller.manager.ingame.ship;

import java.util.Iterator;
import java.util.Observer;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SlotAssignment;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public class WeaponAssignControllerManager extends AbstractControlManager implements GUICallback {
   public static final int LIST_CHANGED = -1;
   public static final int SELECTED_CHANGED = -2;
   private long selectedPiece;
   private boolean needsUpdate;

   public WeaponAssignControllerManager(GameClientState var1) {
      super(var1);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (this.isTreeActive() && !this.isSuspended()) {
         Iterator var6 = this.getState().getController().getInputController().getMouseEvents().iterator();

         while(var6.hasNext()) {
            MouseEvent var3;
            if ((var3 = (MouseEvent)var6.next()).button == 0 && !var3.state && var1 instanceof GUIListElement) {
               GUIElementList var4;
               GUIListElement var7;
               int var5 = (var4 = (GUIElementList)(var7 = (GUIListElement)var1).getParent()).indexOf(var7);
               var4.deselectAll();
               var7.setSelected(true);
               System.err.println("Controller manager call back: index: " + var5);
               this.selectedPiece = (Long)var7.getContent().getUserPointer();
               this.selectionUpdate();
            }
         }
      }

   }

   public boolean isOccluded() {
      return !this.getState().getController().getPlayerInputs().isEmpty();
   }

   public synchronized void addObserver(Observer var1) {
      super.addObserver(var1);
   }

   public void flagUpdate() {
      this.setNeedsUpdate(true);
   }

   public void forceUpdate() {
      this.setChanged();
      this.notifyObservers(new NotifySegmentPiece(this.selectedPiece, -1));
   }

   public PlayerInteractionControlManager getInteractionManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager();
   }

   public long getSelectedPiece() {
      return this.selectedPiece;
   }

   public void setSelectedPiece(long var1) {
      this.selectedPiece = var1;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
      if (KeyboardMappings.getEventKeyState(var1, this.getState()) && KeyboardMappings.getEventKeyRaw(var1) >= 2 && KeyboardMappings.getEventKeyRaw(var1) <= 11) {
         this.numberKeyPressed(KeyboardMappings.getEventKeyRaw(var1) - 2);
      }

   }

   public void handleMouseEvent(MouseEvent var1) {
      super.handleMouseEvent(var1);
   }

   public void onSwitch(boolean var1) {
      CameraMouseState.setGrabbed(!var1);
      if (var1) {
         this.needsUpdate = true;
         this.getState().getController().queueUIAudio("0022_menu_ui - swoosh scroll large");
      } else {
         this.getState().getController().queueUIAudio("0022_menu_ui - swoosh scroll small");
      }

      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController().suspend(var1);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().suspend(var1);
      this.setChanged();
      this.notifyObservers();
      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      CameraMouseState.setGrabbed(false);
      this.getInteractionManager().suspend(true);
   }

   public boolean isNeedsUpdate() {
      return this.needsUpdate;
   }

   public void setNeedsUpdate(boolean var1) {
      this.needsUpdate = var1;
   }

   private void numberKeyPressed(int var1) {
      if (this.selectedPiece != Long.MIN_VALUE) {
         assert this.getState().getShip() != null;

         SlotAssignment var2 = this.getState().getShip().getSlotAssignment();
         byte var3 = -1;
         if (var2.hasConfigForPos(this.selectedPiece)) {
            var3 = var2.removeByPosAndSend(this.selectedPiece);
            this.setChanged();
         }

         System.err.println("[CLIENT][SHIPKEYCONFIG] PRESSED " + var1 + " (key " + (var1 + 1) + "); WEAPON BAR: " + this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().getSelectedWeaponBottomBar() + ": REMOVE: " + var3 + " (" + this.selectedPiece + ") index: " + this.selectedPiece);
         var1 += this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().getSelectedWeaponBottomBar() * 10;
         if (var3 != var1) {
            System.err.println("[CLIENT][SHIPKEYCONFIG] ASSINGING: " + var1 + " to " + this.selectedPiece);
            var2.modAndSend((byte)var1, this.selectedPiece);
            this.setChanged();
         }

         this.notifyObservers(this.selectedPiece);
      }
   }

   public void selectionUpdate() {
      this.setChanged();
      this.notifyObservers(new NotifySegmentPiece(this.selectedPiece, -2));
   }
}
