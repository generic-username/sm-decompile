package org.schema.game.client.controller.manager.ingame;

import org.schema.common.util.linAlg.Vector3i;

public interface BuildCallback extends BuildSelectionCallback {
   void onBuild(Vector3i var1, Vector3i var2, short var3);
}
