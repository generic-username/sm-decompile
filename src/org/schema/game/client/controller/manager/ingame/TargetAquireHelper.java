package org.schema.game.client.controller.manager.ingame;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.SlotAssignment;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.controller.elements.MissileModuleInterface;
import org.schema.game.common.controller.elements.missile.dumb.DumbMissileCollectionManager;
import org.schema.game.common.controller.elements.missile.dumb.DumbMissileElementManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.config.ConfigManagerInterface;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.KeyboardMappings;

public class TargetAquireHelper {
   private float targetTime = 0.0F;
   private float targetHoldTime = 0.0F;
   private SimpleTransformableSendableObject target;
   private boolean targetMode = false;
   private boolean flagSlotChange = true;
   private long currentSlotUsableIndex;

   public void checkSlot(SegmentPiece var1) {
      long var2 = this.currentSlotUsableIndex;
      GameClientState var4;
      if (this.flagSlotChange && var1 != null) {
         var4 = (GameClientState)var1.getSegmentController().getState();
         SlotAssignment var5;
         if ((var5 = var1.getSegmentController().getSlotAssignment()).hasConfigForSlot(var4.getPlayer().getCurrentShipControllerSlot())) {
            this.currentSlotUsableIndex = var5.getAsIndex(var4.getPlayer().getCurrentShipControllerSlot());
            if (this.currentSlotUsableIndex != Long.MIN_VALUE) {
               SegmentPiece var6;
               if ((var6 = var1.getSegmentController().getSegmentBuffer().getPointUnsave(this.currentSlotUsableIndex)) == null) {
                  return;
               }

               this.targetMode = false;
               if (var1.getSegment().getSegmentController() instanceof ManagedSegmentController) {
                  this.targetMode = ((ManagedSegmentController)var1.getSegment().getSegmentController()).getManagerContainer().isTargetLocking(var6);
               }

               if (!this.targetMode && var4.getPlayer().getAquiredTarget() != null) {
                  System.err.println("[CLIENT][SLOT] no longer in target mode");
                  var4.getPlayer().setAquiredTarget((SimpleTransformableSendableObject)null);
               }
            }
         } else {
            this.targetMode = false;
         }
      }

      if (var2 != this.currentSlotUsableIndex) {
         this.targetTime = 0.0F;
         this.targetHoldTime = 0.0F;
         this.flagSlotChange = false;
         if (var1 != null) {
            if ((var4 = (GameClientState)var1.getSegmentController().getState()).getPlayer().getAquiredTarget() != null) {
               System.err.println("[CLIENT] Aquired target reset (slot changed) from " + var4.getPlayer().getAquiredTarget());
            }

            var4.getPlayer().setAquiredTarget((SimpleTransformableSendableObject)null);
         }
      }

   }

   private void checkTargetLockOnMode(SegmentPiece var1, Timer var2) {
      GameClientState var3 = (GameClientState)var1.getSegmentController().getState();
      boolean var4 = KeyboardMappings.FREE_CAM.isDownOrSticky(var3);
      if (this.targetMode && !var4) {
         if (var3.getPlayer().getAquiredTarget() != null && var3.getPlayer().getAquiredTarget() instanceof Ship && ((Ship)var3.getPlayer().getAquiredTarget()).isJammingFor(var1.getSegmentController())) {
            if (var3.getPlayer().getAquiredTarget() != null) {
               System.err.println("[CLIENT] Aquired target reset (jamming) from " + var3.getPlayer().getAquiredTarget());
            }

            var3.getPlayer().setAquiredTarget((SimpleTransformableSendableObject)null);
            this.targetTime = 0.0F;
            this.targetHoldTime = 0.0F;
         }

         if (var3.getPlayer().getAquiredTarget() != null) {
            this.targetHoldTime += var2.getDelta();
         }

         if (this.targetHoldTime > 0.0F && this.targetHoldTime < 3.0F) {
            if (PlayerInteractionControlManager.getLookingAt(var3, true, 90.0F, false, 0.9F, true) == var3.getPlayer().getAquiredTarget()) {
               this.targetHoldTime = 0.0F;
               return;
            }
         } else {
            this.targetHoldTime = 0.0F;
            SimpleTransformableSendableObject var5;
            if ((var5 = PlayerInteractionControlManager.getLookingAt(var3, true, 90.0F, false, 0.9F, true)) != null && var5.isInAdminInvisibility()) {
               var5 = null;
            }

            if (!var3.isInWarp() && (this.target != var5 || var5 instanceof Ship && ((Ship)var5).isJammingFor(var1.getSegmentController()))) {
               if (var3.getPlayer().getAquiredTarget() != null) {
                  System.err.println("[CLIENT] Aquired target reset from " + var3.getPlayer().getAquiredTarget());
               }

               this.target = var5;
               this.targetTime = 0.0F;
               var3.getPlayer().setAquiredTarget((SimpleTransformableSendableObject)null);
               return;
            }

            if (this.target != null) {
               this.targetTime += var2.getDelta();
               if (this.targetTime > this.getAcquireTime(var1.getSegmentController(), this.target)) {
                  var3.getPlayer().setAquiredTarget(this.target);
               }
            }
         }

      } else {
         if (var3.getPlayer().getAquiredTarget() != null) {
            System.err.println("[CLIENT] Aquired target reset (no longer in target mode) from " + var3.getPlayer().getAquiredTarget());
         }

         var3.getPlayer().setAquiredTarget((SimpleTransformableSendableObject)null);
         this.targetTime = 0.0F;
         this.targetHoldTime = 0.0F;
      }
   }

   public void update(SegmentPiece var1, Timer var2) {
      if (var1 != null) {
         this.checkSlot(var1);
         this.checkTargetLockOnMode(var1, var2);
      }

   }

   public float getAcquireTime(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2) {
      float var3 = DumbMissileElementManager.LOCK_ON_TIME_SEC;
      if (var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof MissileModuleInterface) {
         SegmentController var4;
         ManagerModuleCollection var8;
         SlotAssignment var5 = (var4 = ((DumbMissileElementManager)(var8 = ((MissileModuleInterface)((ManagedSegmentController)var1).getManagerContainer()).getMissile()).getElementManager()).getSegmentController()).getSlotAssignment();
         GameClientState var10 = (GameClientState)var4.getState();
         if (var5.hasConfigForSlot(var10.getPlayer().getCurrentShipControllerSlot())) {
            long var6 = var5.getAsIndex(var10.getPlayer().getCurrentShipControllerSlot());
            DumbMissileCollectionManager var9;
            if ((var9 = (DumbMissileCollectionManager)var8.getCollectionManagersMap().get(var6)) != null) {
               var3 = var9.getLockOnTime();
            }
         }
      }

      return var2 instanceof ConfigManagerInterface ? ((ConfigManagerInterface)var2).getConfigManager().apply(StatusEffectType.STEALTH_MISSILE_LOCK_ON_TIME, var3) : var3;
   }

   public SimpleTransformableSendableObject getTarget() {
      return this.target;
   }

   public float getTargetTime() {
      return this.targetTime;
   }

   public boolean isTargetMode() {
      return this.targetMode;
   }

   public void setTargetMode(boolean var1) {
      this.targetMode = var1;
   }

   public void flagSlotChange() {
      this.flagSlotChange = true;
   }
}
