package org.schema.game.client.controller.manager.ingame.faction;

import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.client.view.gui.faction.FactionBlockGUIDialog;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.EntityRequest;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;

public class FactionBlockDialog extends PlayerGameOkCancelInput {
   private final SimpleTransformableSendableObject obj;
   private FactionBlockGUIDialog panel;
   private AbstractControlManager pc;

   public FactionBlockDialog(GameClientState var1, SimpleTransformableSendableObject var2, AbstractControlManager var3) {
      super("FactionBlockDialog", var1, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FACTION_FACTIONBLOCKDIALOG_8, "");
      this.obj = var2;
      this.pc = var3;
      var3.suspend(true);
      this.panel = new FactionBlockGUIDialog(this.getState(), this, var2);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      super.callback(var1, var2);
      if (var2.pressedLeftMouse()) {
         if (var1.getUserPointer().equals("NEUTRAL")) {
            if (this.obj instanceof SegmentController && ((SegmentController)this.obj).isSufficientFactionRights(this.getState().getPlayer())) {
               this.getState().getPlayer().getFactionController().sendEntityFactionIdChangeRequest(0, this.obj);
            } else {
               this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FACTION_FACTIONBLOCKDIALOG_5, 0.0F);
            }

            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("FACTION")) {
            System.err.println("[CLIENT] trying to set faction to " + this.getState().getPlayer().getFactionId() + " on " + this.obj);
            this.getState().getPlayer().getFactionController().sendEntityFactionIdChangeRequest(this.getState().getPlayer().getFactionId(), this.obj);
            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("HOMEBASE")) {
            String var5 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FACTION_FACTIONBLOCKDIALOG_6;
            boolean var6 = this.getState().getPlayer().getFactionController().hasHomebase();
            (new PlayerGameOkCancelInput("CONFIRM", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FACTION_FACTIONBLOCKDIALOG_11, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FACTION_FACTIONBLOCKDIALOG_7 + "\n" + (var6 ? var5 : "")) {
               public boolean isOccluded() {
                  return false;
               }

               public void onDeactivate() {
               }

               public void pressedOK() {
                  this.deactivate();
                  this.getState().getFactionManager().sendClientHomeBaseChange(this.getState().getPlayer().getName(), this.getState().getPlayer().getFactionId(), FactionBlockDialog.this.obj.getUniqueIdentifier());
               }
            }).activate();
            this.deactivate();
            return;
         }

         final SegmentController var3;
         if (var1.getUserPointer().equals("SYSTEM_OWN")) {
            var3 = (SegmentController)this.obj;
            if (this.getState().getPlayer().getFactionId() != 0 && this.getState().getPlayer().getFactionId() == var3.getFactionId() && !var3.isSufficientFactionRights(this.getState().getPlayer())) {
               this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FACTION_FACTIONBLOCKDIALOG_2, 0.0F);
               return;
            }

            this.getState().getFactionManager().sendClientSystemOwnerChange(this.getState().getPlayer().getName(), this.getState().getPlayer().getFactionId(), this.obj);
            this.deactivate();
            return;
         }

         if (!var1.getUserPointer().equals("SYSTEM_NONE")) {
            if (var1.getUserPointer().equals("SYSTEM_REVOKE")) {
               var3 = (SegmentController)this.obj;
               if (this.getState().getPlayer().getFactionId() != 0 && this.getState().getPlayer().getFactionId() == var3.getFactionId() && !var3.isSufficientFactionRights(this.getState().getPlayer())) {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FACTION_FACTIONBLOCKDIALOG_1, 0.0F);
                  return;
               }

               this.getState().getFactionManager().sendClientSystemOwnerChange(this.getState().getPlayer().getName(), 0, this.obj);
               this.deactivate();
               return;
            }

            if (var1.getUserPointer().equals("RENAME")) {
               var3 = (SegmentController)this.obj;
               if (this.getState().getPlayer().getFactionId() != 0 && this.getState().getPlayer().getFactionId() == var3.getFactionId() && !var3.isSufficientFactionRights(this.getState().getPlayer())) {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FACTION_FACTIONBLOCKDIALOG_0, 0.0F);
                  return;
               }

               PlayerGameTextInput var4;
               (var4 = new PlayerGameTextInput("FactionBlockDialog_CHANGE_NAME", this.getState(), 50, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FACTION_FACTIONBLOCKDIALOG_9, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FACTION_FACTIONBLOCKDIALOG_10, var3.getRealName()) {
                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3x) throws PrefixNotFoundException {
                     return null;
                  }

                  public void onFailedTextCheck(String var1) {
                  }

                  public boolean isOccluded() {
                     return false;
                  }

                  public void onDeactivate() {
                     FactionBlockDialog.this.pc.hinderInteraction(400);
                     FactionBlockDialog.this.pc.suspend(false);
                  }

                  public boolean onInput(String var1) {
                     if (!var3.getRealName().equals(var1.trim())) {
                        System.err.println("[CLIENT] sending name for object: " + var3 + ": " + var1.trim());
                        var3.getNetworkObject().realName.set(var1.trim(), true);

                        assert var3.getNetworkObject().realName.hasChanged();

                        assert var3.getNetworkObject().isChanged();
                     }

                     return true;
                  }
               }).setInputChecker(new InputChecker() {
                  public boolean check(String var1, TextCallback var2) {
                     if (EntityRequest.isShipNameValid(var1)) {
                        return true;
                     } else {
                        var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FACTION_FACTIONBLOCKDIALOG_3);
                        return false;
                     }
                  }
               });
               var4.activate();
               this.deactivate();
               return;
            }

            if (var1.getUserPointer().equals("HOMEBASE_REVOKE")) {
               var3 = (SegmentController)this.obj;
               if (this.getState().getPlayer().getFactionId() != 0 && this.getState().getPlayer().getFactionId() == var3.getFactionId() && this.getState().getPlayer().getFactionRights() < var3.getFactionRights()) {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FACTION_FACTIONBLOCKDIALOG_4, 0.0F);
                  return;
               }

               this.getState().getFactionManager().sendClientHomeBaseChange(this.getState().getPlayer().getName(), this.getState().getPlayer().getFactionId(), "");
               this.deactivate();
            }
         }
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public GUIInputPanel getInputPanel() {
      return this.panel;
   }

   public void onDeactivate() {
      if (this.getState().getController().getPlayerInputs().isEmpty()) {
         this.pc.hinderInteraction(400);
         this.pc.suspend(false);
      }

   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public boolean isOccluded() {
      return false;
   }

   public void pressedOK() {
      this.deactivate();
   }
}
