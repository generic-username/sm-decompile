package org.schema.game.client.controller.manager.ingame;

public interface RemoveCallback extends BuildSelectionCallback {
   void onRemove(long var1, short var3);
}
