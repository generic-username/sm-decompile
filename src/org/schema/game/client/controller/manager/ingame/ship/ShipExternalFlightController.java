package org.schema.game.client.controller.manager.ingame.ship;

import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.controller.manager.ingame.TargetAquireHelper;
import org.schema.game.client.controller.manager.ingame.navigation.NavigationControllerManager;
import org.schema.game.client.view.camera.InShipCamera;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.CockpitManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.camera.Camera;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.camera.viewer.FixedViewer;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.objects.container.PhysicsDataContainer;

public class ShipExternalFlightController extends AbstractControlManager {
   private final TargetAquireHelper aquire = new TargetAquireHelper();
   ControllerStateUnit unit = new ControllerStateUnit();
   Vector3f force = new Vector3f();
   boolean cancelledMov = false;
   public Camera shipCamera;
   private boolean lastDocked;

   public ShipExternalFlightController(ShipControllerManager var1) {
      super(var1.getState());
   }

   public SegmentPiece getEntered() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getEntered();
   }

   public Vector3i getEntered(Vector3i var1) {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getEntered().getAbsolutePos(var1);
   }

   public PhysicsDataContainer getPhysicsData() {
      return this.getShip().getPhysicsDataContainer();
   }

   public Ship getShip() {
      return this.getState().getShip();
   }

   public CockpitManager getCockpitManager() {
      return this.getShip().getManagerContainer().getCockpitManager();
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, this.getState())) {
         if (this.shipCamera instanceof InShipCamera && KeyboardMappings.ADJUST_COCKPIT.isEventKey(var1, this.getState())) {
            Controller.FREE_CAM_STICKY = false;
            ((InShipCamera)this.shipCamera).switchAdjustMode();
         }

         if (this.shipCamera instanceof InShipCamera && KeyboardMappings.ADJUST_COCKPIT_RESET.isEventKey(var1, this.getState())) {
            ((InShipCamera)this.shipCamera).resetAdjustMode();
         }

         if (KeyboardMappings.getEventKeyRaw(var1) >= 2 && KeyboardMappings.getEventKeyRaw(var1) <= 11) {
            if (Keyboard.isKeyDown(KeyboardMappings.SCROLL_BOTTOM_BAR.getMapping())) {
               this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().setSelectedWeaponBottomBar((byte)(KeyboardMappings.getEventKeyRaw(var1) - 2));
            } else {
               this.numberKeyPressed((byte)(this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().getSelectedWeaponBottomBar() * 10 + KeyboardMappings.getEventKeyRaw(var1) - 2));
            }
         }

         this.getCockpitManager().handleKeyEvent(var1, this);
      }

   }

   public void handleMouseEvent(MouseEvent var1) {
      if (var1.pressedLeftMouse()) {
         this.aquire.flagSlotChange();
      }

      if (EngineSettings.S_ZOOM_MOUSEWHEEL.getCurrentState().equals("SLOTS") && !KeyboardMappings.SCROLL_MOUSE_ZOOM.isDown(this.getState()) && !KeyboardMappings.PLAYER_LIST.isDown(this.getState())) {
         int var2 = this.getState().getPlayer().getCurrentShipControllerSlot();
         if (Keyboard.isKeyDown(KeyboardMappings.SCROLL_BOTTOM_BAR.getMapping())) {
            if (var1.dWheel > 0) {
               this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().modSelectedWeaponBottomBar(1);
            } else if (var1.dWheel < 0) {
               this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().modSelectedWeaponBottomBar(-1);
            }

            this.getState().getPlayer().setCurrentShipControllerSlot((byte)(FastMath.cyclicModulo((int)var2, (int)10) + this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().getSelectedWeaponBottomBar() * 10), 0.1F);
         } else {
            int var3;
            int var4;
            if (EngineSettings.S_INVERT_MOUSEWHEEL_HOTBAR.isOn()) {
               var3 = var2 - var1.dWheel / 120;
               var4 = FastMath.cyclicModulo((int)var2, (int)10) - var1.dWheel / 120;
            } else {
               var3 = var2 + var1.dWheel / 120;
               var4 = FastMath.cyclicModulo((int)var2, (int)10) + var1.dWheel / 120;
            }

            var3 = FastMath.cyclicModulo((int)var3, (int)10);
            if (!EngineSettings.S_FLIP_HOTBAR_MOUSEWHEEL.isOn() && var4 > 9) {
               this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().modSelectedWeaponBottomBar(1);
            } else if (!EngineSettings.S_FLIP_HOTBAR_MOUSEWHEEL.isOn() && var4 < 0) {
               this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().modSelectedWeaponBottomBar(-1);
            }

            if (var2 != var3) {
               this.getState().getPlayer().setCurrentShipControllerSlot((byte)(var3 + this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().getSelectedWeaponBottomBar() * 10), 0.1F);
            }
         }

         this.aquire.flagSlotChange();
      }

      this.getAquire().checkSlot(this.getEntered());
   }

   public void onSwitch(boolean var1) {
      System.out.println("SHIPCAMERA: onSwitch called");
      InShipControlManager var2 = this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager();
      if (this.getState().getPlayer() != null) {
         this.getState().getPlayer().setAquiredTarget((SimpleTransformableSendableObject)null);
      }

      this.aquire.flagSlotChange();
      if (!var1 && this.shipCamera instanceof InShipCamera) {
         ((InShipCamera)this.shipCamera).setAdjustMode(false);
      }

      if (var1) {
         if (this.shipCamera != null && ((FixedViewer)this.shipCamera.getViewable()).getEntity() == this.getShip()) {
            if (this.shipCamera != null) {
               ((InShipCamera)this.shipCamera).resetTransition(Controller.getCamera());
               this.getState().getPlayer().setLastOrientation(this.getEntered().getSegment().getSegmentController().getWorldTransform());
               ((InShipCamera)this.shipCamera).docked = this.getEntered().getSegmentController().getDockingController().isDocked() || this.getEntered().getSegmentController().railController.isDockedOrDirty();
            }
         } else {
            Camera var3 = Controller.getCamera();

            assert this.getShip() != null : "SHIP NOT FOUND ";

            System.err.println("[CLIENT] SWITCHING TO USING SHIP CAMERA (last cam: " + var3 + "); DOCKED: " + this.getEntered().getSegmentController().getDockingController().isDocked() + "; RAIL: " + this.getEntered().getSegmentController().railController.isDockedOrDirty());
            this.shipCamera = new InShipCamera(var2.getShipControlManager(), Controller.getCamera(), this.getEntered());
            this.shipCamera.setCameraStartOffset(0.0F);
            ((InShipCamera)this.shipCamera).resetTransition(var3);
            this.getState().getPlayer().setLastOrientation(this.getEntered().getSegment().getSegmentController().getWorldTransform());
            ((InShipCamera)this.shipCamera).docked = this.getEntered().getSegmentController().getDockingController().isDocked() || this.getEntered().getSegmentController().railController.isDockedOrDirty();
         }

         this.getCockpitManager().onSwitch(this);
         this.getState().getController().timeOutBigMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHIP_SHIPEXTERNALFLIGHTCONTROLLER_6);
         String var4 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHIP_SHIPEXTERNALFLIGHTCONTROLLER_7;
         this.getState().getController().showBigMessage("FLTMODE", var4, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHIP_SHIPEXTERNALFLIGHTCONTROLLER_8, KeyboardMappings.CHANGE_SHIP_MODE.getKeyChar(), KeyboardMappings.ENTER_SHIP.getKeyChar()), 0.0F);
         Controller.setCamera(this.shipCamera);
      } else {
         this.shipCamera = null;
      }

      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      super.update(var1);
      if (KeyboardMappings.ALIGN_SHIP.isDown(this.getState()) && Controller.getCamera() instanceof InShipCamera) {
         ((InShipCamera)Controller.getCamera()).align();
      }

      if (!this.cancelledMov && KeyboardMappings.CANCEL_SHIP.isDown(this.getState())) {
         if (Controller.getCamera() instanceof InShipCamera) {
            ((InShipCamera)Controller.getCamera()).cancel();
         }

         this.cancelledMov = true;
      } else {
         this.cancelledMov = KeyboardMappings.CANCEL_SHIP.isDown(this.getState());
      }

      if (this.lastDocked && this.getEntered() != null && !this.getEntered().getSegmentController().getDockingController().isDocked() && !this.getEntered().getSegmentController().railController.isDockedOrDirty()) {
         System.err.println("[CLIENT] undocked: restting camera");
         this.shipCamera = null;
         this.onSwitch(true);
      }

      this.lastDocked = this.getEntered() != null && (this.getEntered().getSegmentController().getDockingController().isDocked() || this.getEntered().getSegmentController().railController.isDockedOrDirty());
      CameraMouseState.setGrabbed(true);
      this.getCockpitManager().updateLocal(this, this.shipCamera, var1);
      this.getAquire().update(this.getEntered(), var1);
   }

   private void numberKeyPressed(byte var1) {
      this.getState().getPlayer().setCurrentShipControllerSlot(var1, 0.0F);
      this.aquire.flagSlotChange();
   }

   public void resetShipCamera() {
      this.shipCamera = null;
   }

   public NavigationControllerManager getNavigationControllerManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getNavigationControlManager();
   }

   public TargetAquireHelper getAquire() {
      return this.aquire;
   }

   public boolean isTreeActiveInFlight() {
      return this.isTreeActive() && (!(this.shipCamera instanceof InShipCamera) || !((InShipCamera)this.shipCamera).isInAdjustMode());
   }
}
