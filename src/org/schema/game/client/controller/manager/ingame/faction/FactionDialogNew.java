package org.schema.game.client.controller.manager.ingame.faction;

import java.util.regex.Pattern;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;

public class FactionDialogNew extends PlayerGameTextInput {
   public FactionDialogNew(GameClientState var1, String var2) {
      super("FactionDialogNew", var1, 420, 210, 23, var2, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FACTION_FACTIONDIALOGNEW_0, (String)null);
      this.setInputChecker(new InputChecker() {
         public boolean check(String var1, TextCallback var2) {
            if (var1.length() >= 6 && var1.length() < 24) {
               if (Pattern.matches("[a-zA-Z0-9 _-]+", var1)) {
                  return true;
               }

               var2.onFailedTextCheck("Please only alphanumeric (and space, _, -)!");
               System.err.println("MATCH FOUND ^ALPHANUMERIC");
            } else {
               var2.onFailedTextCheck("Please enter name between 6 and 24 long!");
            }

            return false;
         }
      });
   }

   public String[] getCommandPrefixes() {
      return null;
   }

   public String handleAutoComplete(String var1, TextCallback var2, String var3) {
      return var1;
   }

   public void onFailedTextCheck(String var1) {
      this.setErrorMessage(var1);
   }

   public boolean isOccluded() {
      return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
   }

   public void onDeactivate() {
   }

   public boolean onInput(String var1) {
      this.getState().getPlayer().getFactionController().clientCreateFaction(var1, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FACTION_FACTIONDIALOGNEW_1);
      return true;
   }
}
