package org.schema.game.client.controller.manager.ingame.map;

import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.controller.manager.ingame.navigation.NavigationFilter;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.navigation.NavigationFilterSettingPanel;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;

public class MapFilterEditDialog extends PlayerInput {
   private final NavigationFilterSettingPanel panel;

   public MapFilterEditDialog(GameClientState var1, NavigationFilter var2, boolean var3) {
      super(var1);
      this.panel = new NavigationFilterSettingPanel(this.getState(), var2, 0, this, var3);
      this.panel.onInit();
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         if (var1.getUserPointer().equals("OK")) {
            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            this.cancel();
         }
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
   }

   public GUIElement getInputPanel() {
      return this.panel;
   }

   public void onDeactivate() {
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getCatalogControlManager().hinderInteraction(500);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getCatalogControlManager().suspend(false);
   }

   public boolean isOccluded() {
      return false;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }
}
