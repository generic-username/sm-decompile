package org.schema.game.client.controller.manager.ingame.ship;

import java.util.Iterator;
import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.controller.PlayerThrustManagerInput;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.Ship;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.input.KeyEventInterface;

public class ThrustManager extends AbstractControlManager {
   private PlayerThrustManagerInput gameMenu;
   private Ship selectedShip;

   public ThrustManager(GameClientState var1) {
      super(var1);
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public void handleMouseEvent(MouseEvent var1) {
      super.handleMouseEvent(var1);
      PlayerInput.lastDialougeClick = System.currentTimeMillis();
   }

   public void onSwitch(boolean var1) {
      boolean var2 = false;
      if (var1) {
         synchronized(this.getState().getController().getPlayerInputs()) {
            Iterator var4 = this.getState().getController().getPlayerInputs().iterator();

            while(true) {
               if (!var4.hasNext()) {
                  break;
               }

               if ((DialogInterface)var4.next() instanceof PlayerThrustManagerInput) {
                  var2 = true;
               }
            }
         }

         if (!var2 && this.getSelectedShip() != null) {
            this.gameMenu = new PlayerThrustManagerInput(this.getState(), this.getSelectedShip());
            this.getState().getController().getPlayerInputs().add(this.gameMenu);
         }
      } else {
         PlayerInput.lastDialougeClick = System.currentTimeMillis();
         synchronized(this.getState().getController().getPlayerInputs()) {
            for(int var7 = 0; var7 < this.getState().getController().getPlayerInputs().size(); ++var7) {
               if ((DialogInterface)this.getState().getController().getPlayerInputs().get(var7) instanceof PlayerThrustManagerInput) {
                  ((DialogInterface)this.getState().getController().getPlayerInputs().get(var7)).deactivate();
                  break;
               }
            }
         }
      }

      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      CameraMouseState.setGrabbed(false);
      super.update(var1);
      if (this.gameMenu != null) {
         this.gameMenu.update(var1);
      }

   }

   public Ship getSelectedShip() {
      return this.selectedShip;
   }

   public void setSelectedShip(Ship var1) {
      this.selectedShip = var1;
   }
}
