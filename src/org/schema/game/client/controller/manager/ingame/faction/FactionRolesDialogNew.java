package org.schema.game.client.controller.manager.ingame.faction;

import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.faction.newfaction.FactionRoleSettingGUIPlayerInputNew;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;

public class FactionRolesDialogNew extends PlayerInput {
   private final FactionRoleSettingGUIPlayerInputNew panel;
   private boolean sent;

   public FactionRolesDialogNew(GameClientState var1, Faction var2) {
      super(var1);
      this.panel = new FactionRoleSettingGUIPlayerInputNew(this.getState(), this, var2);
   }

   protected void apply() {
      this.getState().getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FACTION_FACTIONROLESDIALOGNEW_0, "sndFactionRoles", 0.0F);
      if (!this.sent) {
         this.getState().getFactionManager().sendFactionRoles(this.panel.getRoles());
         this.sent = true;
      } else {
         assert false;

         try {
            throw new Exception("Exception: ERROR: TRIED TO SEND FACTION ROLES TWICE");
         } catch (Exception var1) {
            var1.printStackTrace();
         }
      }
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public GUIElement getInputPanel() {
      return this.panel;
   }

   public void onDeactivate() {
   }

   public void handleMouseEvent(MouseEvent var1) {
   }
}
