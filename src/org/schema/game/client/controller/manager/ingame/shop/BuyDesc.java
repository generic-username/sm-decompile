package org.schema.game.client.controller.manager.ingame.shop;

import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.data.element.ElementInformation;

public class BuyDesc {
   public int wantedQuantity;
   private ElementInformation info;
   private ShopInterface shop;
   private boolean shopOwner;

   public BuyDesc(ElementInformation var1, ShopInterface var2, boolean var3) {
      this.info = var1;
      this.shop = var2;
      this.shopOwner = var3;
   }

   public String toString() {
      return "How many " + this.info.getName() + " do you want to " + (this.shopOwner ? "take" : "buy") + "?\n" + (this.shopOwner ? "" : "If you enter too many, the maximal amount you can affort\nwill be displayed.\nCurrent Buying Value: " + this.shop.getShoppingAddOn().toStringForPurchase(this.info.getId(), this.wantedQuantity) + " (base " + (long)this.wantedQuantity * this.info.getPrice(((GameStateInterface)this.shop.getState()).getMaterialPrice()) + "c)");
   }
}
