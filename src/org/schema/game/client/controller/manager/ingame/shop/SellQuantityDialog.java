package org.schema.game.client.controller.manager.ingame.shop;

import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.controller.ShoppingAddOn;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;

public class SellQuantityDialog extends QuantityDialog {
   public SellQuantityDialog(final GameClientState var1, String var2, final short var3, int var4, ShopInterface var5) {
      super(var1, var3, var2, new SellDesc(ElementKeyMap.getInfo(var3), var5, var2.contains("Put")), var4);
      ((SellDesc)this.descriptionObject).wantedQuantity = -var4;
      this.setInputChecker(new InputChecker() {
         public boolean check(String var1x, TextCallback var2) {
            try {
               if (var1x.length() > 0) {
                  int var7 = Integer.parseInt(var1x);
                  int var3x = SellQuantityDialog.this.getState().getPlayer().getInventory((Vector3i)null).getOverallQuantity(var3);
                  ShopInterface var4;
                  if ((var4 = SellQuantityDialog.this.getState().getCurrentClosestShop()) == null) {
                     SellQuantityDialog.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SELLQUANTITYDIALOG_0, 0.0F);
                     return false;
                  }

                  if (var7 <= 0) {
                     SellQuantityDialog.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SELLQUANTITYDIALOG_1, 0.0F);
                     return false;
                  }

                  if (!var4.getShopInventory().canPutIn(var3, var7)) {
                     SellQuantityDialog.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SELLQUANTITYDIALOG_2, 0.0F);
                     SellQuantityDialog.this.getTextInput().clear();
                     var7 = var4.getShopInventory().canPutInHowMuch(var3, var7, -1);
                     SellQuantityDialog.this.getTextInput().append(String.valueOf(var7));
                     SellQuantityDialog.this.getTextInput().selectAll();
                     return false;
                  }

                  ElementKeyMap.getInfo(var3);
                  int var5 = var4.getShoppingAddOn().canShopAfford(var3, var7);
                  if (ShoppingAddOn.isSelfOwnedShop(var1, var4)) {
                     var5 = var7;
                  }

                  if (var5 < var7) {
                     SellQuantityDialog.this.getState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SELLQUANTITYDIALOG_3, var5), 0.0F);
                     SellQuantityDialog.this.getTextInput().clear();
                     SellQuantityDialog.this.getTextInput().append(String.valueOf(var5));
                     SellQuantityDialog.this.getTextInput().selectAll();
                     SellQuantityDialog.this.getTextInput().update();
                  } else {
                     if (var7 <= var3x) {
                        SellQuantityDialog.this.getState().getController().queueUIAudio("0022_action - receive credits");
                        return true;
                     }

                     SellQuantityDialog.this.getState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SELLQUANTITYDIALOG_4, var3x), 0.0F);
                     SellQuantityDialog.this.getTextInput().clear();
                     SellQuantityDialog.this.getTextInput().append(String.valueOf(var3x));
                     SellQuantityDialog.this.getTextInput().selectAll();
                     SellQuantityDialog.this.getTextInput().update();
                  }

                  return false;
               }
            } catch (NumberFormatException var6) {
            }

            var2.onFailedTextCheck("Please only enter numbers!");
            return false;
         }
      });
      this.getTextInput().setOnInputChangedCallback(new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            try {
               ((SellDesc)SellQuantityDialog.this.descriptionObject).wantedQuantity = -Integer.parseInt(var1);
            } catch (Exception var2) {
            }

            return var1;
         }
      });
   }

   public boolean onInput(String var1) {
      int var2 = Integer.parseInt(var1);
      this.getState().getPlayer().getInventoryController().sell(this.element, var2);
      return true;
   }
}
