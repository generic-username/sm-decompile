package org.schema.game.client.controller.manager.ingame;

import org.schema.game.client.data.GameClientState;

public class SlabSetting extends AbstractSizeSetting {
   private GameClientState state;

   public SlabSetting(GameClientState var1) {
      this.state = var1;
   }

   public void set(float var1) {
      super.set(var1);
      this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().checkOrienationForNewSelectedSlot();
   }

   public int getMax() {
      return 3;
   }

   public int getMin() {
      return 0;
   }
}
