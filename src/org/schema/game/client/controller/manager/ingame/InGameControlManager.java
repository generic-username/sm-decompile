package org.schema.game.client.controller.manager.ingame;

import java.io.File;
import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerLagStatsInput;
import org.schema.game.client.controller.PlayerMessageLogPlayerInput;
import org.schema.game.client.controller.PlayerMessagesPlayerInput;
import org.schema.game.client.controller.PlayerNetworkStatsInput;
import org.schema.game.client.controller.PlayerNotYetInitializedException;
import org.schema.game.client.controller.TeamMenu;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.controller.manager.MessageLogManager;
import org.schema.game.client.controller.manager.PlayerMailManager;
import org.schema.game.client.controller.manager.freemode.AutoRoamController;
import org.schema.game.client.controller.manager.freemode.FreeRoamController;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.util.GifEncoder;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.resource.FileExt;

public class InGameControlManager extends AbstractControlManager {
   public static final int PLAYER_MESSAGE_LOG_KEY = 62;
   private ChatControlManager chatControlManager;
   private FreeRoamController freeRoamController;
   private AutoRoamController autoRoamController;
   private MessageLogManager messageLogManager;
   private PlayerMailManager playerMailManager;
   private PlayerGameControlManager playerGameControlManager;

   public InGameControlManager(GameClientState var1) {
      super(var1);
      this.initialize();
   }

   public AutoRoamController getAutoRoamController() {
      return this.autoRoamController;
   }

   public void setAutoRoamController(AutoRoamController var1) {
      this.autoRoamController = var1;
   }

   public ChatControlManager getChatControlManager() {
      return this.chatControlManager;
   }

   public FreeRoamController getFreeRoamController() {
      return this.freeRoamController;
   }

   public PlayerGameControlManager getPlayerGameControlManager() {
      return this.playerGameControlManager;
   }

   public void activateMesssageLog() {
      boolean var1 = this.messageLogManager.isActive();
      this.messageLogManager.setActive(!var1);
      if (!this.messageLogManager.isActive()) {
         for(int var2 = 0; var2 < this.getState().getController().getPlayerInputs().size(); ++var2) {
            if ((DialogInterface)this.getState().getController().getPlayerInputs().get(var2) instanceof PlayerMessageLogPlayerInput) {
               ((DialogInterface)this.getState().getController().getPlayerInputs().get(var2)).deactivate();
               return;
            }
         }
      }

   }

   public void activatePlayerMesssages() {
      boolean var1 = this.playerMailManager.isActive();
      this.playerMailManager.setActive(!var1);
      if (!this.playerMailManager.isActive()) {
         for(int var2 = 0; var2 < this.getState().getController().getPlayerInputs().size(); ++var2) {
            if ((DialogInterface)this.getState().getController().getPlayerInputs().get(var2) instanceof PlayerMessagesPlayerInput) {
               ((DialogInterface)this.getState().getController().getPlayerInputs().get(var2)).deactivate();
               return;
            }
         }
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
      boolean var2 = this.chatControlManager.isActive();
      boolean var3 = this.getState().getController().getInputController().handleKeyEventInputPanels(var1);
      if (this.getState().getController().getPlayerInputs().size() == 1 && KeyboardMappings.getEventKeyState(var1, this.getState())) {
         if (((DialogInterface)this.getState().getController().getPlayerInputs().get(0)).allowChat()) {
            if (KeyboardMappings.CHAT.isEventKey(var1, this.getState())) {
               if (!var2) {
                  this.chatControlManager.setDelayedActive(true);
               } else if (EngineSettings.CHAT_CLOSE_ON_ENTER.isOn()) {
                  this.chatControlManager.setDelayedActive(false);
               }
            } else if (KeyboardMappings.getEventKeySingle(var1) == 1) {
               if (var2) {
                  this.chatControlManager.setDelayedActive(false);
               }
            } else if (var2) {
               this.chatControlManager.handleKeyEvent(var1);
            }
         } else if (KeyboardMappings.getEventKeyState(var1, this.getState())) {
            if (KeyboardMappings.getEventKeySingle(var1) == 62 && !this.getState().isDebugKeyDown()) {
               this.activatePlayerMesssages();
            } else {
               KeyboardMappings.getEventKeySingle(var1);
            }
         }
      }

      if (!var3) {
         if (KeyboardMappings.getEventKeyState(var1, this.getState())) {
            if (KeyboardMappings.NETWORK_STATS_PANEL.isEventKey(var1, this.getState()) && !this.getState().isDebugKeyDown() && this.getState().getController().getPlayerInputs().isEmpty()) {
               (new PlayerNetworkStatsInput(this.getState())).activate();
            }

            if (KeyboardMappings.LAG_STATS_PANEL.isEventKey(var1, this.getState()) && !this.getState().isDebugKeyDown() && this.getState().getController().getPlayerInputs().isEmpty()) {
               if (this.getState().isAdmin()) {
                  (new PlayerLagStatsInput(this.getState())).activate();
               } else {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_INGAMECONTROLMANAGER_0, 0.0F);
               }
            }

            if (KeyboardMappings.RECORD_GIF.isEventKey(var1, this.getState())) {
               if (this.getState().getWorldDrawer().gifEncoder == null) {
                  Integer var11 = new Integer((Integer)EngineSettings.GIF_FPS.getCurrentState());
                  this.getState().getWorldDrawer().gifEncoder = new GifEncoder();
                  this.getState().getWorldDrawer().gifEncoder.setFrameRate((float)var11);
                  this.getState().getWorldDrawer().gifEncoder.setRepeat(0);
                  float var9 = (float)(Integer)EngineSettings.GIF_WIDTH.getCurrentState();
                  float var4 = (float)(Integer)EngineSettings.GIF_HEIGHT.getCurrentState();
                  var4 = (float)GLFrame.getHeight() / (float)GLFrame.getWidth() * var4;
                  this.getState().getWorldDrawer().gifEncoder.setSize((int)var9, (int)var4);
                  GraphicsContext.limitFPS = var11;
                  File[] var10 = (new FileExt("./")).listFiles();
                  int var12 = 0;
                  boolean var13 = true;

                  while(true) {
                     while(var13) {
                        var13 = false;
                        File[] var5 = var10;
                        int var6 = var10.length;

                        for(int var7 = 0; var7 < var6; ++var7) {
                           if (var5[var7].getName().startsWith("starmade-gif-" + StringTools.formatFourZero(var12) + ".gif")) {
                              System.err.println("Screen Already Exists: ./starmade-gif-" + StringTools.formatFourZero(var12) + ".gif");
                              ++var12;
                              var13 = true;
                              break;
                           }
                        }
                     }

                     String var14 = "./starmade-gif-" + StringTools.formatFourZero(var12) + ".gif";
                     System.err.println("Recording GIF: " + var14);
                     this.getState().getWorldDrawer().gifEncoder.start(var14);
                     break;
                  }
               } else {
                  System.err.println("STOPPING GIF RECORDING!");
                  this.getState().getWorldDrawer().gifEncoder.finish();
                  this.getState().getWorldDrawer().gifEncoder = null;
                  GraphicsContext.limitFPS = null;
               }
            } else if (KeyboardMappings.getEventKeySingle(var1) == 62 && !this.getState().isDebugKeyDown()) {
               this.activatePlayerMesssages();
            } else if (KeyboardMappings.CHAT.isEventKey(var1, this.getState())) {
               if (!var2) {
                  this.chatControlManager.setDelayedActive(true);
               } else if (EngineSettings.CHAT_CLOSE_ON_ENTER.isOn()) {
                  this.chatControlManager.setDelayedActive(false);
               }
            } else if (KeyboardMappings.PLAYER_LIST.isDown(this.getState()) && KeyboardMappings.getEventKeyRaw(var1) == 87) {
               System.err.println("[CLIENT] F11 Pressed: lwjgl event key: " + KeyboardMappings.getEventKeyRaw(var1));
               if (!var2) {
                  if (!(var3 = this.getFreeRoamController().isActive()) && !this.getState().getController().getPlayerInputs().isEmpty()) {
                     return;
                  }

                  if (var3 && this.getState().getCharacter() == null) {
                     System.out.println("no player character: spawning");

                     try {
                        this.getState().getController().spawnAndActivatePlayerCharacter();
                     } catch (PlayerNotYetInitializedException var8) {
                        var8.printStackTrace();
                        this.getState().getController().popupAlertTextMessage("Player not yet initialized", 0.0F);
                     }
                  } else {
                     this.playerGameControlManager.setActive(var3);
                     this.getFreeRoamController().setActive(!var3);
                  }

                  if (this.autoRoamController.isActive() && !var3) {
                     this.autoRoamController.setActive(false);
                     this.getFreeRoamController().setActive(true);
                  }
               }
            }
         }

         super.handleKeyEvent(var1);
      }
   }

   public void handleMouseEvent(MouseEvent var1) {
      super.handleMouseEvent(var1);
   }

   public void update(Timer var1) {
      super.update(var1);
      if (!this.getState().getController().getPlayerInputs().isEmpty()) {
         CameraMouseState.setGrabbed(false);
      }

   }

   public void initialize() {
      this.chatControlManager = new ChatControlManager(this.getState());
      this.freeRoamController = new FreeRoamController(this.getState());
      this.autoRoamController = new AutoRoamController(this.getState());
      this.playerGameControlManager = new PlayerGameControlManager(this.getState());
      this.messageLogManager = new MessageLogManager(this.getState());
      this.playerMailManager = new PlayerMailManager(this.getState());
      this.getControlManagers().add(this.playerMailManager);
      this.getControlManagers().add(this.messageLogManager);
      this.getControlManagers().add(this.chatControlManager);
      this.getControlManagers().add(this.freeRoamController);
      this.getControlManagers().add(this.autoRoamController);
      this.getControlManagers().add(this.playerGameControlManager);
      this.getAutoRoamController().setActive(true);
   }

   public void teamChangeMenu() {
      boolean var1 = false;
      Iterator var2 = this.getState().getController().getPlayerInputs().iterator();

      while(var2.hasNext()) {
         if ((DialogInterface)var2.next() instanceof TeamMenu) {
            var1 = true;
         }
      }

      if (!var1) {
         TeamMenu var3 = new TeamMenu(this.getState());
         this.getState().getController().getPlayerInputs().add(var3);
      }

   }

   public boolean isInBuildMode() {
      return this.getPlayerGameControlManager().getPlayerIntercationManager().getSegmentControlManager().isTreeActive() || this.getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().isTreeActive();
   }

   public MessageLogManager getMessageLogManager() {
      return this.messageLogManager;
   }

   public PlayerMailManager getPlayerMailManager() {
      return this.playerMailManager;
   }

   public boolean isAnyMenuOrChatActive() {
      return this.getPlayerMailManager().isActive() || this.getMessageLogManager().isActive() || this.getChatControlManager().isActive() || this.getPlayerGameControlManager().isAnyMenuActive();
   }
}
