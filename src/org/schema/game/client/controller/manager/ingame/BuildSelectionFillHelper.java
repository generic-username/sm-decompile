package org.schema.game.client.controller.manager.ingame;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.graphicsengine.core.MouseEvent;

public class BuildSelectionFillHelper extends BuildSelection {
   protected void callback(PlayerInteractionControlManager var1, MouseEvent var2) {
      Vector3i var5 = new Vector3i(this.selectionBoxA);
      BuildToolsManager var3 = var1.getBuildToolsManager();
      SegmentPiece var4;
      if ((var4 = var1.getSegmentControlManager().getSegmentController().getSegmentBuffer().getPointUnsave(var5)) != null) {
         var3.setFillTool(new FillTool(var1.getSegmentControlManager().getSegmentController(), var5, var4.getType()));
      }

   }

   protected BuildSelection.DrawStyle getDrawStyle() {
      return BuildSelection.DrawStyle.LINE;
   }

   protected boolean canExecute(PlayerInteractionControlManager var1) {
      return true;
   }

   protected boolean isSingleSelect() {
      return true;
   }
}
