package org.schema.game.client.controller.manager.ingame.catalog;

import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public class CatalogControlManager extends AbstractControlManager implements GUICallback {
   public static final String PERSONAL = "PERSONAL";
   public static final String ACCESSIBLE = "ACCESSIBLE";
   public static final String ADMIN = "ADMIN";
   private PersonalCatalogControlManager personalCatalogControlManager;
   private AccessibleCatalogControlManager accessibleCatalogControlManager;
   private AdminCatalogControlManager adminCatalogControlManager;

   public CatalogControlManager(GameClientState var1) {
      super(var1);
      this.initialize();
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         if ("PERSONAL".equals(var1.getUserPointer())) {
            this.activate(this.personalCatalogControlManager);
            this.setChanged();
            this.notifyObservers();
            return;
         }

         if ("ACCESSIBLE".equals(var1.getUserPointer())) {
            this.activate(this.accessibleCatalogControlManager);
            this.setChanged();
            this.notifyObservers();
            return;
         }

         if ("ADMIN".equals(var1.getUserPointer()) && this.getState().getPlayer().getNetworkObject().isAdminClient.get()) {
            this.activate(this.adminCatalogControlManager);
            this.setChanged();
            this.notifyObservers();
         }
      }

   }

   public boolean isOccluded() {
      return false;
   }

   public AccessibleCatalogControlManager getAccessibleCatalogControlManager() {
      return this.accessibleCatalogControlManager;
   }

   public AdminCatalogControlManager getAdminCatalogControlManager() {
      return this.adminCatalogControlManager;
   }

   public Faction getOwnFaction() {
      int var1 = this.getState().getPlayer().getFactionId();
      return this.getState().getFactionManager().getFaction(var1);
   }

   public PersonalCatalogControlManager getPersonalCatalogControlManager() {
      return this.personalCatalogControlManager;
   }

   private void initialize() {
      this.personalCatalogControlManager = new PersonalCatalogControlManager(this.getState());
      this.accessibleCatalogControlManager = new AccessibleCatalogControlManager(this.getState());
      this.adminCatalogControlManager = new AdminCatalogControlManager(this.getState());
      this.getControlManagers().add(this.personalCatalogControlManager);
      this.getControlManagers().add(this.accessibleCatalogControlManager);
      this.getControlManagers().add(this.adminCatalogControlManager);
      this.personalCatalogControlManager.setActive(true);
   }

   public void onSwitch(boolean var1) {
      if (var1) {
         this.getState().getController().queueUIAudio("0022_menu_ui - swoosh scroll large");
         this.setChanged();
         this.notifyObservers();
      } else {
         this.getState().getController().queueUIAudio("0022_menu_ui - swoosh scroll small");
      }

      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController().suspend(var1);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().suspend(var1);
      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      CameraMouseState.setGrabbed(false);
      super.update(var1);
   }
}
