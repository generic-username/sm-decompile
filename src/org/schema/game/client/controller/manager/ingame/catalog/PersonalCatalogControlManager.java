package org.schema.game.client.controller.manager.ingame.catalog;

import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;

public class PersonalCatalogControlManager extends AbstractControlManager {
   public PersonalCatalogControlManager(GameClientState var1) {
      super(var1);
   }
}
