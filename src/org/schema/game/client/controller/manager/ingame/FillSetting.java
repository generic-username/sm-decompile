package org.schema.game.client.controller.manager.ingame;

import org.schema.game.client.data.GameClientState;

public class FillSetting extends AbstractSizeSetting {
   private GameClientState state;

   public FillSetting(GameClientState var1) {
      this.state = var1;
   }

   public int getMin() {
      return 1;
   }

   public int getMax() {
      return Math.min((int)Math.pow((double)this.state.getMaxBuildArea(), 3.0D), 5000);
   }
}
