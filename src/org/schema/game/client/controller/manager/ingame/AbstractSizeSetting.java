package org.schema.game.client.controller.manager.ingame;

import org.schema.schine.graphicsengine.forms.gui.GUISettingsElement;

public abstract class AbstractSizeSetting {
   public GUISettingsElement guiCallBack;
   public int setting = this.getMin();

   public abstract int getMin();

   public void dec() {
      this.setting = Math.max(this.getMin(), this.setting - 1);
      if (this.guiCallBack != null) {
         this.guiCallBack.settingChanged(this.setting);
      }

   }

   public void inc() {
      this.setting = Math.min(10, this.setting + 1);
      if (this.guiCallBack != null) {
         this.guiCallBack.settingChanged(this.setting);
      }

   }

   public void reset() {
      this.setting = 1;
      if (this.guiCallBack != null) {
         this.guiCallBack.settingChanged(this.setting);
      }

   }

   public String toString() {
      return String.valueOf(this.setting);
   }

   public abstract int getMax();

   public float get() {
      return (float)this.setting;
   }

   public void set(float var1) {
      this.setting = Math.min(Math.max(this.getMin(), Math.round(var1)), this.getMax());
      if (this.guiCallBack != null) {
         this.guiCallBack.settingChanged(this.setting);
      }

   }
}
