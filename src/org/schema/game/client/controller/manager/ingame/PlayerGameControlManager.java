package org.schema.game.client.controller.manager.ingame;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import org.schema.common.FastMath;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerMessageLogPlayerInput;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.controller.manager.AiConfigurationManager;
import org.schema.game.client.controller.manager.ingame.catalog.CatalogControlManager;
import org.schema.game.client.controller.manager.ingame.cubatom.CubatomControllerManager;
import org.schema.game.client.controller.manager.ingame.faction.FactionControlManager;
import org.schema.game.client.controller.manager.ingame.map.MapControllerManager;
import org.schema.game.client.controller.manager.ingame.navigation.NavigationControllerManager;
import org.schema.game.client.controller.manager.ingame.ship.FleetControlManager;
import org.schema.game.client.controller.manager.ingame.ship.InShipControlManager;
import org.schema.game.client.controller.manager.ingame.ship.ThrustManager;
import org.schema.game.client.controller.manager.ingame.ship.WeaponAssignControllerManager;
import org.schema.game.client.controller.manager.ingame.shop.ShopControllerManager;
import org.schema.game.client.controller.manager.ingame.structurecontrol.StructureControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.reactor.ReactorTreeDialog;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.rails.RailController;
import org.schema.game.common.controller.rails.RailRelation;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.schine.ai.stateMachines.AiInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public class PlayerGameControlManager extends AbstractControlManager {
   private final ObjectArrayList panelElements = new ObjectArrayList();
   short lastUpdateInventory = -1;
   long lastUpdateTime = 0L;
   private PlayerInteractionControlManager playerIntercationManager;
   private InventoryControllerManager inventoryControlManager;
   private ShopControllerManager shopControlManager;
   private WeaponAssignControllerManager weaponControlManager;
   private FleetControlManager fleetControlManager;
   private NavigationControllerManager navigationControlManager;
   private MapControllerManager mapControlManager;
   private AiConfigurationManager aiConfigurationManager;
   private FactionControlManager factionControlManager;
   private CatalogControlManager catalogControlManager;
   private CubatomControllerManager cubatomControlManager;
   private StructureControllerManager structureControlManager;
   private ThrustManager thrustManager;

   public PlayerGameControlManager(GameClientState var1) {
      super(var1);
      this.initialize();
   }

   public void activateThrustManager(Ship var1) {
      if (this.inventoryControlManager.isActive()) {
         this.inventoryControlManager.setActive(false);
      }

      if (this.shopControlManager.isActive()) {
         this.shopControlManager.setActive(false);
      }

      if (this.getFactionControlManager().isActive()) {
         this.getFactionControlManager().setActive(false);
      }

      if (this.getCatalogControlManager().isActive()) {
         this.getCatalogControlManager().setActive(false);
      }

      if (this.weaponControlManager.isActive()) {
         this.weaponControlManager.setActive(false);
      }

      if (this.fleetControlManager.isActive()) {
         this.fleetControlManager.setActive(false);
      }

      if (this.mapControlManager.isActive()) {
         this.mapControlManager.setActive(false);
      }

      if (this.aiConfigurationManager.isActive()) {
         this.aiConfigurationManager.setActive(false);
      }

      if (this.cubatomControlManager.isActive()) {
         this.cubatomControlManager.setActive(false);
      }

      if (this.structureControlManager.isActive()) {
         this.structureControlManager.setActive(false);
      }

      if (this.navigationControlManager.isActive()) {
         this.navigationControlManager.setActive(false);
      }

      boolean var2 = this.thrustManager.isActive();
      this.thrustManager.setSelectedShip(var1);
      this.thrustManager.setActive(!var2);
      if (!this.thrustManager.isActive()) {
         for(int var3 = 0; var3 < this.getState().getController().getPlayerInputs().size(); ++var3) {
            if ((DialogInterface)this.getState().getController().getPlayerInputs().get(var3) instanceof PlayerMessageLogPlayerInput) {
               ((DialogInterface)this.getState().getController().getPlayerInputs().get(var3)).deactivate();
               return;
            }
         }
      }

   }

   public void aiConfigurationAction(SegmentPiece var1) {
      if (this.getState().getShip() != null && this.getState().getShip().getAiConfiguration().getControllerBlock() != null) {
         this.aiConfigurationManager.setCanEdit(true);
         this.aiConfigurationManager.setAi(this.getState().getShip());
      } else if (var1 != null && var1.getSegment().getSegmentController() instanceof AiInterface) {
         this.aiConfigurationManager.setCanEdit(true);
         this.aiConfigurationManager.setAi((AiInterface)var1.getSegment().getSegmentController());
      } else if (this.getPlayerIntercationManager().getSelectedEntity() != null && this.getPlayerIntercationManager().getSelectedEntity() instanceof AiInterface) {
         this.aiConfigurationManager.setCanEdit(this.getState().getPlayer().getNetworkObject().isAdminClient.get());
         this.aiConfigurationManager.setAi((AiInterface)this.getPlayerIntercationManager().getSelectedEntity());
      } else {
         this.aiConfigurationManager.setCanEdit(false);
         this.aiConfigurationManager.setAi((AiInterface)null);
      }

      if (this.thrustManager.isActive()) {
         this.thrustManager.setActive(false);
      }

      if (this.inventoryControlManager.isActive()) {
         this.inventoryControlManager.setActive(false);
      }

      if (this.getFactionControlManager().isActive()) {
         this.getFactionControlManager().setActive(false);
      }

      if (this.getCatalogControlManager().isActive()) {
         this.getCatalogControlManager().setActive(false);
      }

      if (this.shopControlManager.isActive()) {
         this.shopControlManager.setActive(false);
      }

      if (this.weaponControlManager.isActive()) {
         this.weaponControlManager.setActive(false);
      }

      if (this.fleetControlManager.isActive()) {
         this.fleetControlManager.setActive(false);
      }

      if (this.navigationControlManager.isActive()) {
         this.navigationControlManager.setActive(false);
      }

      if (this.cubatomControlManager.isActive()) {
         this.cubatomControlManager.setActive(false);
      }

      if (this.structureControlManager.isActive()) {
         this.structureControlManager.setActive(false);
      }

      boolean var2 = !this.aiConfigurationManager.isActive();
      this.aiConfigurationManager.setDelayedActive(var2);
   }

   public void deactivateAll() {
      Iterator var1 = this.panelElements.iterator();

      while(var1.hasNext()) {
         ((AbstractControlManager)var1.next()).setActive(false);
      }

      this.getPlayerIntercationManager().hinderInteraction(600);
   }

   public AiConfigurationManager getAiConfigurationManager() {
      return this.aiConfigurationManager;
   }

   public CatalogControlManager getCatalogControlManager() {
      return this.catalogControlManager;
   }

   public CubatomControllerManager getCubatomControlManager() {
      return this.cubatomControlManager;
   }

   public void setCubatomControlManager(CubatomControllerManager var1) {
      this.cubatomControlManager = var1;
   }

   public StructureControllerManager getStructureControlManager() {
      return this.structureControlManager;
   }

   public FactionControlManager getFactionControlManager() {
      return this.factionControlManager;
   }

   public InventoryControllerManager getInventoryControlManager() {
      return this.inventoryControlManager;
   }

   public MapControllerManager getMapControlManager() {
      return this.mapControlManager;
   }

   public NavigationControllerManager getNavigationControlManager() {
      return this.navigationControlManager;
   }

   public PlayerInteractionControlManager getPlayerIntercationManager() {
      return this.playerIntercationManager;
   }

   public ShopControllerManager getShopControlManager() {
      return this.shopControlManager;
   }

   public WeaponAssignControllerManager getWeaponControlManager() {
      return this.weaponControlManager;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      int var2;
      if ((var2 = this.getState().getController().getPlayerInputs().size()) > 0) {
         ((DialogInterface)this.getState().getController().getPlayerInputs().get(var2 - 1)).handleKeyEvent(var1);
      } else {
         boolean var5;
         if (KeyboardMappings.getEventKeyState(var1, this.getState()) && this.getState().getController().getPlayerInputs().isEmpty() && !this.getState().isDebugKeyDown()) {
            if (this.inventoryControlManager.isActive() && KeyboardMappings.ACTIVATE.isEventKey(var1, this.getState())) {
               this.inventoryAction((Inventory)null);
            }

            if (KeyboardMappings.INVENTORY_PANEL.isEventKey(var1, this.getState())) {
               this.inventoryAction((Inventory)null);
            } else if (KeyboardMappings.SHOP_PANEL.isEventKey(var1, this.getState())) {
               this.shopAction();
            } else if (KeyboardMappings.WEAPON_PANEL.isEventKey(var1, this.getState())) {
               this.weaponAction();
            } else if (KeyboardMappings.FLEET_PANEL.isEventKey(var1, this.getState())) {
               this.fleetAction();
            } else if (KeyboardMappings.NAVIGATION_PANEL.isEventKey(var1, this.getState())) {
               this.navigationAction();
            } else if (KeyboardMappings.CATALOG_PANEL.isEventKey(var1, this.getState())) {
               this.catalogAction();
            } else if (KeyboardMappings.HELP_SCREEN.isEventKey(var1, this.getState())) {
               EngineSettings.CONTROL_HELP.changeBooleanSetting(!EngineSettings.CONTROL_HELP.isOn());
            } else if (KeyboardMappings.STRUCTURE_PANEL.isEventKey(var1, this.getState())) {
               this.structureAction();
            } else if (KeyboardMappings.REACTOR_KEY.isEventKey(var1, this.getState())) {
               if (this.getState().getCurrentPlayerObject() != null && this.getState().getCurrentPlayerObject().hasAnyReactors()) {
                  (new ReactorTreeDialog(this.getState(), (ManagedSegmentController)this.getState().getCurrentPlayerObject())).activate();
               }
            } else if (KeyboardMappings.MAP_PANEL.isEventKey(var1, this.getState())) {
               this.galaxyMapAction();
            } else if (KeyboardMappings.AI_CONFIG_PANEL.isEventKey(var1, this.getState())) {
               this.aiConfigurationAction((SegmentPiece)null);
            } else if (KeyboardMappings.FACTION_MENU.isEventKey(var1, this.getState())) {
               this.factionAction();
            } else if (KeyboardMappings.CREATIVE_MODE.isEventKey(var1, this.getState())) {
               if (this.getState().getPlayer().isHasCreativeMode()) {
                  this.getState().getPlayer().setUseCreativeMode(!this.getState().getPlayer().isUseCreativeMode());
               }
            } else if (KeyboardMappings.SWITCH_COCKPIT_SHIP_NEXT.isEventKey(var1, this.getState()) || KeyboardMappings.SWITCH_COCKPIT_SHIP_PREVIOUS.isEventKey(var1, this.getState())) {
               var5 = KeyboardMappings.SWITCH_COCKPIT_SHIP_NEXT.isEventKey(var1, this.getState());
               PlayerGameControlManager var3 = this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
               this.handleDockingAndRail(var5 ? 1 : -1, var3);
            }
         }

         var5 = false;

         AbstractControlManager var4;
         for(Iterator var6 = this.panelElements.iterator(); var6.hasNext(); var5 = var5 || var4.isActive()) {
            var4 = (AbstractControlManager)var6.next();
         }

         if (this.playerIntercationManager.isSuspended() != var5) {
            this.playerIntercationManager.suspend(var5);
         }

         if (this.getState().getController().getPlayerInputs().isEmpty()) {
            super.handleKeyEvent(var1);
         }

      }
   }

   private void handleDockingAndRail(int var1, PlayerGameControlManager var2) {
      while(this.getPlayerIntercationManager().getEntered() != null) {
         SegmentController var6 = this.getPlayerIntercationManager().getEntered().getSegmentController();
         if (KeyboardMappings.SWITCH_COCKPIT_SHIP_HOLD_FOR_CHAIN.isDown(this.getState())) {
            if (var6.railController.isDockedAndExecuted()) {
               RailController var3 = var6.railController.previous.getRailRController();
               if (Math.abs(var1) > var3.next.size() - 1) {
                  return;
               }

               int var4 = -1;

               for(int var5 = 0; var5 < var3.next.size(); ++var5) {
                  if (((RailRelation)var3.next.get(var5)).docked.getSegmentController() == var6) {
                     var4 = var5;
                  }
               }

               if ((var4 = FastMath.cyclicModulo(var4 + var1, var3.next.size())) >= 0 && var4 < var3.next.size()) {
                  SegmentController var8;
                  if ((var8 = ((RailRelation)var3.next.get(var4)).docked.getSegmentController()) != var6 && InShipControlManager.checkEnter(var8)) {
                     InShipControlManager.switchEntered(var8);
                     return;
                  }

                  if (var8 != var6) {
                     this.getState().getController().popupInfoTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_PLAYERGAMECONTROLMANAGER_2, var8.toNiceString()), 0.0F);
                  }

                  var1 = var1 > 0 ? var1 + 1 : var1 - 1;
                  this = this;
                  continue;
               }

               return;
            }
         } else {
            SegmentController var7 = null;
            if (var1 > 0) {
               if (var6.railController.isRail()) {
                  var7 = ((RailRelation)var6.railController.next.get(0)).docked.getSegmentController();
               }
            } else if (var6.railController.isDockedAndExecuted()) {
               var7 = var6.railController.previous.rail.getSegmentController();
            }

            if (var7 != null && var7 != var6 && InShipControlManager.checkEnter(var7)) {
               assert this.getPlayerIntercationManager().getEntered().getSegmentController() == var6;

               assert this.getPlayerIntercationManager().getEntered().getSegmentController() != var7;

               InShipControlManager.switchEntered(var7);
            }

            this.getState().getController().popupInfoTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_PLAYERGAMECONTROLMANAGER_3, KeyboardMappings.SWITCH_COCKPIT_SHIP_HOLD_FOR_CHAIN.getKeyChar()), 0.0F);
         }

         return;
      }

   }

   public void onSwitch(boolean var1) {
      if (var1) {
         this.cubatomControlManager.setActive(false);
         this.structureControlManager.setActive(false);
         this.navigationControlManager.setActive(false);
         this.shopControlManager.setActive(false);
         this.weaponControlManager.setActive(false);
         this.fleetControlManager.setActive(false);
         this.inventoryControlManager.setActive(false);
         this.mapControlManager.setActive(false);
         this.thrustManager.setActive(false);
         this.playerIntercationManager.setDelayedActive(true);
      } else {
         this.inventoryControlManager.setActive(false);
         this.navigationControlManager.setActive(false);
         this.shopControlManager.setActive(false);
         this.weaponControlManager.setActive(false);
         this.fleetControlManager.setActive(false);
         this.mapControlManager.setActive(false);
         this.cubatomControlManager.setActive(false);
         this.thrustManager.setActive(false);
         this.structureControlManager.setActive(false);
      }

      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      boolean var2 = this.inventoryControlManager.isActive() || this.shopControlManager.isActive() || this.weaponControlManager.isActive() || this.factionControlManager.isActive() || this.aiConfigurationManager.isActive() || this.fleetControlManager.isActive() || this.catalogControlManager.isActive();
      if (this.playerIntercationManager.isSuspended() != var2) {
         this.playerIntercationManager.suspend(var2);
      }

      super.update(var1);
   }

   public void galaxyMapAction() {
      if (this.inventoryControlManager.isActive()) {
         this.inventoryControlManager.setActive(false);
      }

      if (this.shopControlManager.isActive()) {
         this.shopControlManager.setActive(false);
      }

      if (this.getFactionControlManager().isActive()) {
         this.getFactionControlManager().setActive(false);
      }

      if (this.getCatalogControlManager().isActive()) {
         this.getCatalogControlManager().setActive(false);
      }

      if (this.weaponControlManager.isActive()) {
         this.weaponControlManager.setActive(false);
      }

      if (this.fleetControlManager.isActive()) {
         this.fleetControlManager.setActive(false);
      }

      if (this.navigationControlManager.isActive()) {
         this.navigationControlManager.setActive(false);
      }

      if (this.cubatomControlManager.isActive()) {
         this.cubatomControlManager.setActive(false);
      }

      if (this.structureControlManager.isActive()) {
         this.structureControlManager.setActive(false);
      }

      if (this.aiConfigurationManager.isActive()) {
         this.aiConfigurationManager.setActive(false);
      }

      if (this.thrustManager.isActive()) {
         this.thrustManager.setActive(false);
      }

      boolean var1 = !this.mapControlManager.isActive();
      System.err.println("ACTIVATE MAP " + var1);
      this.mapControlManager.setDelayedActive(var1);
   }

   public void navigationAction() {
      if (this.inventoryControlManager.isActive()) {
         this.inventoryControlManager.setActive(false);
      }

      if (this.shopControlManager.isActive()) {
         this.shopControlManager.setActive(false);
      }

      if (this.getFactionControlManager().isActive()) {
         this.getFactionControlManager().setActive(false);
      }

      if (this.getCatalogControlManager().isActive()) {
         this.getCatalogControlManager().setActive(false);
      }

      if (this.weaponControlManager.isActive()) {
         this.weaponControlManager.setActive(false);
      }

      if (this.fleetControlManager.isActive()) {
         this.fleetControlManager.setActive(false);
      }

      if (this.mapControlManager.isActive()) {
         this.mapControlManager.setActive(false);
      }

      if (this.aiConfigurationManager.isActive()) {
         this.aiConfigurationManager.setActive(false);
      }

      if (this.cubatomControlManager.isActive()) {
         this.cubatomControlManager.setActive(false);
      }

      if (this.structureControlManager.isActive()) {
         this.structureControlManager.setActive(false);
      }

      if (this.thrustManager.isActive()) {
         this.thrustManager.setActive(false);
      }

      boolean var1 = !this.navigationControlManager.isActive();
      this.navigationControlManager.setDelayedActive(var1);
   }

   public void initialize() {
      this.playerIntercationManager = new PlayerInteractionControlManager(this.getState());
      this.inventoryControlManager = new InventoryControllerManager(this.getState());
      this.shopControlManager = new ShopControllerManager(this.getState());
      this.weaponControlManager = new WeaponAssignControllerManager(this.getState());
      this.fleetControlManager = new FleetControlManager(this.getState());
      this.navigationControlManager = new NavigationControllerManager(this.getState());
      this.mapControlManager = new MapControllerManager(this.getState());
      this.aiConfigurationManager = new AiConfigurationManager(this.getState());
      this.factionControlManager = new FactionControlManager(this.getState());
      this.catalogControlManager = new CatalogControlManager(this.getState());
      this.cubatomControlManager = new CubatomControllerManager(this.getState());
      this.structureControlManager = new StructureControllerManager(this.getState());
      this.thrustManager = new ThrustManager(this.getState());
      this.getControlManagers().add(this.thrustManager);
      this.getControlManagers().add(this.playerIntercationManager);
      this.getControlManagers().add(this.inventoryControlManager);
      this.getControlManagers().add(this.shopControlManager);
      this.getControlManagers().add(this.weaponControlManager);
      this.getControlManagers().add(this.fleetControlManager);
      this.getControlManagers().add(this.navigationControlManager);
      this.getControlManagers().add(this.aiConfigurationManager);
      this.getControlManagers().add(this.mapControlManager);
      this.getControlManagers().add(this.getFactionControlManager());
      this.getControlManagers().add(this.getCatalogControlManager());
      this.getControlManagers().add(this.getCubatomControlManager());
      this.getControlManagers().add(this.structureControlManager);
      this.panelElements.add(this.thrustManager);
      this.panelElements.add(this.inventoryControlManager);
      this.panelElements.add(this.shopControlManager);
      this.panelElements.add(this.weaponControlManager);
      this.panelElements.add(this.fleetControlManager);
      this.panelElements.add(this.navigationControlManager);
      this.panelElements.add(this.aiConfigurationManager);
      this.panelElements.add(this.mapControlManager);
      this.panelElements.add(this.cubatomControlManager);
      this.panelElements.add(this.getFactionControlManager());
      this.panelElements.add(this.getCatalogControlManager());
      this.panelElements.add(this.structureControlManager);
   }

   public boolean isAnyMenuActive() {
      Iterator var1 = this.getControlManagers().iterator();

      AbstractControlManager var2;
      do {
         if (!var1.hasNext()) {
            return false;
         }
      } while(!(var2 = (AbstractControlManager)var1.next()).isActive() || var2 == this.playerIntercationManager);

      return true;
   }

   public ThrustManager getThrustManager() {
      return this.thrustManager;
   }

   public void inventoryAction(Inventory var1) {
      this.inventoryAction(var1, !this.inventoryControlManager.isActive(), false);
   }

   public void inventoryAction(Inventory var1, boolean var2, boolean var3) {
      if (this.getState().getNumberOfUpdate() != this.lastUpdateInventory || System.currentTimeMillis() - this.lastUpdateTime >= 1000L) {
         this.lastUpdateInventory = this.getState().getNumberOfUpdate();
         this.lastUpdateTime = System.currentTimeMillis();
         if (this.shopControlManager.isActive()) {
            this.shopControlManager.setActive(false);
         }

         if (this.getWeaponControlManager().isActive()) {
            this.getWeaponControlManager().setActive(false);
         }

         if (this.aiConfigurationManager.isActive()) {
            this.aiConfigurationManager.setActive(false);
         }

         if (this.navigationControlManager.isActive()) {
            this.navigationControlManager.setActive(false);
         }

         if (this.fleetControlManager.isActive()) {
            this.fleetControlManager.setActive(false);
         }

         if (this.cubatomControlManager.isActive()) {
            this.cubatomControlManager.setActive(false);
         }

         if (this.structureControlManager.isActive()) {
            this.structureControlManager.setActive(false);
         }

         if (this.getFactionControlManager().isActive()) {
            this.getFactionControlManager().setActive(false);
         }

         if (this.mapControlManager.isActive()) {
            this.mapControlManager.setActive(false);
         }

         if (this.getCatalogControlManager().isActive()) {
            this.getCatalogControlManager().setActive(false);
         }

         if (var3) {
            this.inventoryControlManager.setActive(false);
         }

         if (this.thrustManager.isActive()) {
            this.thrustManager.setActive(false);
         }

         if (var2) {
            this.inventoryControlManager.setSecondInventory(var1);
         }

         this.inventoryControlManager.setActive(var2);
      }
   }

   public void shopAction() {
      boolean var1 = !this.shopControlManager.isActive();
      if (!this.getState().isInShopDistance()) {
         this.getState().getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_PLAYERGAMECONTROLMANAGER_0, 0.0F);
      } else {
         if (this.inventoryControlManager.isActive()) {
            this.inventoryControlManager.setActive(false);
         }

         if (this.mapControlManager.isActive()) {
            this.mapControlManager.setActive(false);
         }

         if (this.getFactionControlManager().isActive()) {
            this.getFactionControlManager().setActive(false);
         }

         if (this.weaponControlManager.isActive()) {
            this.weaponControlManager.setActive(false);
         }

         if (this.fleetControlManager.isActive()) {
            this.fleetControlManager.setActive(false);
         }

         if (this.navigationControlManager.isActive()) {
            this.navigationControlManager.setActive(false);
         }

         if (this.cubatomControlManager.isActive()) {
            this.cubatomControlManager.setActive(false);
         }

         if (this.structureControlManager.isActive()) {
            this.structureControlManager.setActive(false);
         }

         if (this.getCatalogControlManager().isActive()) {
            this.getCatalogControlManager().setActive(false);
         }

         if (this.aiConfigurationManager.isActive()) {
            this.aiConfigurationManager.setActive(false);
         }

         if (this.thrustManager.isActive()) {
            this.thrustManager.setActive(false);
         }

         this.shopControlManager.setDelayedActive(var1);
      }
   }

   public void weaponAction() {
      if (!this.getPlayerIntercationManager().getInShipControlManager().isActive()) {
         this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_PLAYERGAMECONTROLMANAGER_1, 0.0F);
      } else {
         if (this.getFactionControlManager().isActive()) {
            this.getFactionControlManager().setActive(false);
         }

         if (this.getCatalogControlManager().isActive()) {
            this.getCatalogControlManager().setActive(false);
         }

         if (this.inventoryControlManager.isActive()) {
            this.inventoryControlManager.setActive(false);
         }

         if (this.shopControlManager.isActive()) {
            this.shopControlManager.setActive(false);
         }

         if (this.mapControlManager.isActive()) {
            this.mapControlManager.setActive(false);
         }

         if (this.fleetControlManager.isActive()) {
            this.fleetControlManager.setActive(false);
         }

         if (this.navigationControlManager.isActive()) {
            this.navigationControlManager.setActive(false);
         }

         if (this.cubatomControlManager.isActive()) {
            this.cubatomControlManager.setActive(false);
         }

         if (this.structureControlManager.isActive()) {
            this.structureControlManager.setActive(false);
         }

         if (this.aiConfigurationManager.isActive()) {
            this.aiConfigurationManager.setActive(false);
         }

         if (this.thrustManager.isActive()) {
            this.thrustManager.setActive(false);
         }

         boolean var1 = !this.weaponControlManager.isActive();
         this.weaponControlManager.setDelayedActive(var1);
      }
   }

   public void fleetAction() {
      if (this.inventoryControlManager.isActive()) {
         this.inventoryControlManager.setActive(false);
      }

      if (this.shopControlManager.isActive()) {
         this.shopControlManager.setActive(false);
      }

      if (this.getFactionControlManager().isActive()) {
         this.getFactionControlManager().setActive(false);
      }

      if (this.getCatalogControlManager().isActive()) {
         this.getCatalogControlManager().setActive(false);
      }

      if (this.weaponControlManager.isActive()) {
         this.weaponControlManager.setActive(false);
      }

      if (this.mapControlManager.isActive()) {
         this.mapControlManager.setActive(false);
      }

      if (this.aiConfigurationManager.isActive()) {
         this.aiConfigurationManager.setActive(false);
      }

      if (this.navigationControlManager.isActive()) {
         this.navigationControlManager.setActive(false);
      }

      if (this.cubatomControlManager.isActive()) {
         this.cubatomControlManager.setActive(false);
      }

      if (this.thrustManager.isActive()) {
         this.thrustManager.setActive(false);
      }

      if (this.structureControlManager.isActive()) {
         this.structureControlManager.setActive(false);
      }

      boolean var1 = !this.fleetControlManager.isActive();
      this.fleetControlManager.setDelayedActive(var1);
   }

   public void structureAction() {
      if (this.inventoryControlManager.isActive()) {
         this.inventoryControlManager.setActive(false);
      }

      if (this.shopControlManager.isActive()) {
         this.shopControlManager.setActive(false);
      }

      if (this.getFactionControlManager().isActive()) {
         this.getFactionControlManager().setActive(false);
      }

      if (this.getCatalogControlManager().isActive()) {
         this.getCatalogControlManager().setActive(false);
      }

      if (this.weaponControlManager.isActive()) {
         this.weaponControlManager.setActive(false);
      }

      if (this.fleetControlManager.isActive()) {
         this.fleetControlManager.setActive(false);
      }

      if (this.mapControlManager.isActive()) {
         this.mapControlManager.setActive(false);
      }

      if (this.aiConfigurationManager.isActive()) {
         this.aiConfigurationManager.setActive(false);
      }

      if (this.navigationControlManager.isActive()) {
         this.navigationControlManager.setActive(false);
      }

      if (this.cubatomControlManager.isActive()) {
         this.cubatomControlManager.setActive(false);
      }

      if (this.thrustManager.isActive()) {
         this.thrustManager.setActive(false);
      }

      boolean var1 = !this.structureControlManager.isActive();
      this.structureControlManager.setDelayedActive(var1);
   }

   public void catalogAction() {
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getStructureControlManager().setActive(false);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getAiConfigurationManager().setActive(false);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager().setActive(false);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getShopControlManager().setActive(false);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getNavigationControlManager().setActive(false);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getWeaponControlManager().setActive(false);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager().setActive(false);
      if (this.fleetControlManager.isActive()) {
         this.fleetControlManager.setActive(false);
      }

      if (this.thrustManager.isActive()) {
         this.thrustManager.setActive(false);
      }

      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getCatalogControlManager().setActive(true);
   }

   public void factionAction() {
      if (this.inventoryControlManager.isActive()) {
         this.inventoryControlManager.setActive(false);
      }

      if (this.shopControlManager.isActive()) {
         this.shopControlManager.setActive(false);
      }

      if (this.navigationControlManager.isActive()) {
         this.navigationControlManager.setActive(false);
      }

      if (this.fleetControlManager.isActive()) {
         this.fleetControlManager.setActive(false);
      }

      if (this.cubatomControlManager.isActive()) {
         this.cubatomControlManager.setActive(false);
      }

      if (this.structureControlManager.isActive()) {
         this.structureControlManager.setActive(false);
      }

      if (this.getCatalogControlManager().isActive()) {
         this.getCatalogControlManager().setActive(false);
      }

      if (this.mapControlManager.isActive()) {
         this.mapControlManager.setActive(false);
      }

      if (this.weaponControlManager.isActive()) {
         this.weaponControlManager.setActive(false);
      }

      if (this.aiConfigurationManager.isActive()) {
         this.aiConfigurationManager.setActive(false);
      }

      if (this.thrustManager.isActive()) {
         this.thrustManager.setActive(false);
      }

      boolean var1 = !this.getFactionControlManager().isActive();
      this.getFactionControlManager().setDelayedActive(var1);
   }

   public FleetControlManager getFleetControlManager() {
      return this.fleetControlManager;
   }
}
