package org.schema.game.client.controller.manager.ingame.ship;

public class NotifySegmentPiece {
   public long piece;
   public int type;

   public NotifySegmentPiece(long var1, int var3) {
      this.piece = var1;
      this.type = var3;
   }
}
