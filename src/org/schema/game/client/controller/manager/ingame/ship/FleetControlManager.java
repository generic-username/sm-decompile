package org.schema.game.client.controller.manager.ingame.ship;

import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.Timer;

public class FleetControlManager extends AbstractControlManager {
   public FleetControlManager(GameClientState var1) {
      super(var1);
   }

   public Faction getOwnFaction() {
      int var1 = this.getState().getPlayer().getFactionId();
      return this.getState().getFactionManager().getFaction(var1);
   }

   private void initialize() {
   }

   public void onSwitch(boolean var1) {
      if (var1) {
         this.getState().getController().queueUIAudio("0022_menu_ui - swoosh scroll large");
         this.setChanged();
         this.notifyObservers();
      } else {
         this.getState().getController().queueUIAudio("0022_menu_ui - swoosh scroll small");
      }

      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController().suspend(var1);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().suspend(var1);
      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      CameraMouseState.setGrabbed(false);
      super.update(var1);
   }
}
