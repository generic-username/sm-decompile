package org.schema.game.client.controller.manager.ingame.navigation;

import java.util.Iterator;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.navigation.NavigationPanel;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.input.KeyEventInterface;

public class NavigationControllerManager extends AbstractControlManager implements GUICallback {
   private NavigationFilter filter = new NavigationFilter() {
      public long getFilter() {
         return (Long)EngineSettings.E_NAVIGATION_FILTER.getCurrentState();
      }

      public void setFilter(long var1) {
         EngineSettings.E_NAVIGATION_FILTER.setCurrentState(var1);
      }
   };
   private static final Vector3i tmpParam = new Vector3i();

   public NavigationControllerManager(GameClientState var1) {
      super(var1);
   }

   public static boolean isPlayerInCore(SimpleTransformableSendableObject var0) {
      if (var0 instanceof PlayerCharacter) {
         Iterator var2 = ((PlayerCharacter)var0).getOwnerState().getControllerState().getUnits().iterator();

         while(var2.hasNext()) {
            ControllerStateUnit var1;
            if ((var1 = (ControllerStateUnit)var2.next()).playerControllable instanceof SegmentController && Ship.core.equals(var1.getParameter(tmpParam))) {
               return true;
            }
         }
      }

      return false;
   }

   public static boolean isPlayerInJammedShip(SimpleTransformableSendableObject var0) {
      if (var0 instanceof PlayerCharacter) {
         Iterator var1 = ((PlayerCharacter)var0).getOwnerState().getControllerState().getUnits().iterator();

         while(var1.hasNext()) {
            ControllerStateUnit var2;
            if ((var2 = (ControllerStateUnit)var1.next()).playerControllable instanceof Ship && ((Ship)var2.playerControllable).isJammingFor(((GameClientState)var0.getState()).getCurrentPlayerObject())) {
               return true;
            }
         }
      }

      return false;
   }

   public NavigationFilter getFilter() {
      return this.filter;
   }

   public void setFilter(NavigationFilter var1) {
      this.filter = var1;
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (this.isTreeActive() && !this.isSuspended() && var2.pressedLeftMouse() && var1 instanceof GUIListElement) {
         GUIListElement var3;
         GUIElementList var4;
         (var4 = (GUIElementList)(var3 = (GUIListElement)var1).getParent()).indexOf(var3);
         var4.deselectAll();
         var3.setSelected(true);
         this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().setSelectedEntity(((NavigationPanel.NavigationElement)var3.getContent()).getSendable());
      }

   }

   public boolean isOccluded() {
      return !this.getState().getController().getPlayerInputs().isEmpty();
   }

   public NavigationFilter getFilterClone() {
      return new NavigationFilter(this.filter) {
         public long getFilter() {
            return (Long)EngineSettings.E_NAVIGATION_FILTER.getCurrentState();
         }

         public void setFilter(long var1) {
            EngineSettings.E_NAVIGATION_FILTER.setCurrentState(var1);
         }
      };
   }

   public PlayerInteractionControlManager getInteractionManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager();
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public void handleMouseEvent(MouseEvent var1) {
      super.handleMouseEvent(var1);
   }

   public void onSwitch(boolean var1) {
      CameraMouseState.setGrabbed(!var1);
      if ((Long)EngineSettings.E_NAVIGATION_FILTER.getCurrentState() < 0L) {
         EngineSettings.E_NAVIGATION_FILTER.setCurrentState(869L);
      }

      if (var1) {
         this.getState().getController().queueUIAudio("0022_menu_ui - swoosh scroll large");
      } else {
         this.getState().getController().queueUIAudio("0022_menu_ui - swoosh scroll small");
      }

      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController().suspend(var1);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().suspend(var1);
      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      CameraMouseState.setGrabbed(false);
      this.getInteractionManager().suspend(true);
   }

   public boolean isDisplayed(SimpleTransformableSendableObject var1) {
      return this.filter.isDisplayed(var1) && isVisibleRadar(var1);
   }

   public boolean isFiltered(SimpleTransformableSendableObject var1) {
      return !this.filter.isDisplayed(var1) || !isVisibleRadar(var1);
   }

   public static boolean isVisibleRadar(SimpleTransformableSendableObject var0) {
      boolean var1 = var0.isInAdminInvisibility();
      boolean var2 = var0 instanceof Ship && ((Ship)var0).isJammingFor(((GameClientState)var0.getState()).getCurrentPlayerObject());
      return !var1 && !var2 && !isPlayerInCore(var0) && !isPlayerInJammedShip(var0);
   }
}
