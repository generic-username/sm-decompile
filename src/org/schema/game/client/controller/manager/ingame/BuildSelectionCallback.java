package org.schema.game.client.controller.manager.ingame;

public interface BuildSelectionCallback {
   long getSelectedControllerPos();
}
