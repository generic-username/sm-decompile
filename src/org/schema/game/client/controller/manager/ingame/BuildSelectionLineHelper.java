package org.schema.game.client.controller.manager.ingame;

import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.buildhelper.BuildHelperLine;
import org.schema.game.client.view.buildhelper.Line;
import org.schema.schine.graphicsengine.core.MouseEvent;

public class BuildSelectionLineHelper extends BuildSelection {
   private BuildHelperLine c;

   public BuildSelectionLineHelper(BuildHelperLine var1) {
      this.c = var1;
   }

   protected void callback(PlayerInteractionControlManager var1, MouseEvent var2) {
      this.c.clean();
      this.c.line = new Line();
      this.c.line.A = new Vector3i(this.selectionBoxA);
      this.c.line.B = new Vector3i(this.selectionBoxB);
      this.c.create();

      assert !this.c.isFinished();

      this.c.placedPos.set(this.selectionBoxA);
      this.c.placed = true;
      Vector3f var3 = new Vector3f((float)(this.selectionBoxA.x - 16), (float)(this.selectionBoxA.y - 16), (float)(this.selectionBoxA.z - 16));
      this.c.localTransform.origin.set(var3);
      System.err.println("PLACED ----------- " + this.selectionBoxA + " - " + this.selectionBoxB);
      this.c.showProcessingDialog(var1.getState(), var1.getBuildToolsManager(), true);
   }

   protected BuildSelection.DrawStyle getDrawStyle() {
      return BuildSelection.DrawStyle.LINE;
   }

   protected boolean canExecute(PlayerInteractionControlManager var1) {
      return true;
   }

   protected boolean isSingleSelect() {
      return false;
   }
}
