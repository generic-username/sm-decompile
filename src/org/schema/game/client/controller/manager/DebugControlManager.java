package org.schema.game.client.controller.manager;

import com.bulletphysics.linearmath.Transform;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.client.view.BuildModeDrawer;
import org.schema.game.client.view.ElementCollectionDrawer;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.client.view.Segment2ObjWriter;
import org.schema.game.client.view.SegmentDrawer;
import org.schema.game.client.view.WorldDrawer;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.cubes.shapes.pentahedron.topbottom.PentaShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.spike.SpikeShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.tetrahedron.TetrahedronShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeShapeAlgorithm;
import org.schema.game.client.view.gui.RadialMenuDialogDebug;
import org.schema.game.client.view.gui.RadialMenuDialogMain;
import org.schema.game.client.view.gui.reactor.ReactorTreeDialog;
import org.schema.game.client.view.gui.rules.RuleSetConfigDialogGame;
import org.schema.game.client.view.mainmenu.gui.effectconfig.EffectConfigEntityDialog;
import org.schema.game.client.view.mainmenu.gui.effectconfig.GUIEffectStat;
import org.schema.game.client.view.mainmenu.gui.ruleconfig.GUIRuleSetStat;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.StationaryManagerContainer;
import org.schema.game.common.controller.rules.RuleSetManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.config.ConfigManagerInterface;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.PhysicsExt;
import org.schema.game.common.data.physics.Vector3fb;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.remote.RemoteSegmentPiece;
import org.schema.game.server.controller.GameServerController;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.StateChangeRequest;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.graphicsengine.forms.debug.DebugPoint;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.objects.Sendable;

public class DebugControlManager extends AbstractControlManager {
   public static float x;
   public static float y;
   public static boolean requestPhysicsCheck;
   public static SegmentController requestShipMissalign;

   public DebugControlManager(GameClientState var1) {
      super(var1);
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);

      try {
         this.getState().getWorldDrawer().handleKeyEvent(var1);
         if (KeyboardMappings.getEventKeyState(var1, this.getState())) {
            short var2 = this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedTypeWithSub();
            int var3 = this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getBlockOrientation();
            ElementInformation var4 = null;
            if (var2 > 0) {
               var4 = ElementKeyMap.getInfo(var2);
            }

            SimpleTransformableSendableObject var13;
            SimpleTransformableSendableObject var17;
            BlockShapeAlgorithm var19;
            SegmentPiece var21;
            byte var33;
            switch(KeyboardMappings.getEventKeyRaw(var1)) {
            case 2:
               if (!Keyboard.isKeyDown(157)) {
                  return;
               }

               if (BuildModeDrawer.currentInfo != null) {
                  if (this.getState().getCurrentPlayerObject() != null && this.getState().getCurrentPlayerObject() instanceof SegmentController) {
                     if (BuildModeDrawer.currentInfo.getBlockStyle() != BlockStyle.NORMAL) {
                        (var19 = BlockShapeAlgorithm.getAlgo(BuildModeDrawer.currentInfo.getBlockStyle(), BuildModeDrawer.currentPiece.getOrientation())).modProperty(Keyboard.isKeyDown(42) ? 6 : BuildModeDrawer.currentSide, -1);
                        System.err.println("SET SIDE VERTEX ORDER (previous): " + var19.getClass().getSimpleName() + ": LOOKINGKT_SIDE: " + Element.getSideString(BuildModeDrawer.currentSide));
                     } else {
                        System.err.println("Changing a normal block: SIDE: " + BuildModeDrawer.currentSide + "; " + BuildModeDrawer.currentInfo);
                        BlockShapeAlgorithm.changeTexCoordOrder(BuildModeDrawer.currentPiece, BuildModeDrawer.currentSide, -1);
                     }

                     SegmentDrawer.forceFullLightingUpdate = true;
                     return;
                  }

                  try {
                     throw new Exception("This only works right inside build mode");
                  } catch (Exception var7) {
                     var7.printStackTrace();
                     this.getState().getController().popupAlertTextMessage("Only in build mode", 0.0F);
                     return;
                  }
               }
               break;
            case 3:
               if (!Keyboard.isKeyDown(157)) {
                  return;
               }

               if (BuildModeDrawer.currentInfo != null) {
                  if (this.getState().getCurrentPlayerObject() != null && this.getState().getCurrentPlayerObject() instanceof SegmentController) {
                     if (BuildModeDrawer.currentInfo.getBlockStyle() != BlockStyle.NORMAL) {
                        if (BuildModeDrawer.currentSide >= 0) {
                           (var19 = BlockShapeAlgorithm.getAlgo(BuildModeDrawer.currentInfo.getBlockStyle(), BuildModeDrawer.currentPiece.getOrientation())).modProperty(Keyboard.isKeyDown(42) ? 6 : BuildModeDrawer.currentSide, 1);
                           System.err.println("SET SIDE VERTEX ORDER (next): " + var19.getClass().getSimpleName() + ": LOOKING_AT_SIDE:  " + Element.getSideString(BuildModeDrawer.currentSide));
                        } else {
                           System.err.println("INVALID CURRENT SIDE: " + BuildModeDrawer.currentSide);
                        }
                     } else {
                        BlockShapeAlgorithm.changeTexCoordOrder(BuildModeDrawer.currentPiece, BuildModeDrawer.currentSide, 1);
                        System.err.println("Changing a normal block: SIDE: " + BuildModeDrawer.currentSide + "; " + BuildModeDrawer.currentInfo);
                     }

                     SegmentDrawer.forceFullLightingUpdate = true;
                     return;
                  }

                  try {
                     throw new Exception("This only works right inside build mode");
                  } catch (Exception var6) {
                     var6.printStackTrace();
                     this.getState().getController().popupAlertTextMessage("Only in build mode", 0.0F);
                     return;
                  }
               }
               break;
            case 4:
               if (!Keyboard.isKeyDown(157)) {
                  return;
               }

               if (BuildModeDrawer.currentInfo != null) {
                  if ((var33 = (byte)((var21 = new SegmentPiece(BuildModeDrawer.currentPiece)).getOrientation() - 1)) <= 0) {
                     var33 = 31;
                  }

                  var21.setOrientation(var33);
                  var21.getSegment().getSegmentController().sendBlockMod(new RemoteSegmentPiece(var21, false));
                  System.err.println("SENT (shouldbeorient " + var33 + "); " + var21.getOrientation() + "; " + var21);
                  return;
               }
               break;
            case 5:
               if (!Keyboard.isKeyDown(157)) {
                  return;
               }

               if (BuildModeDrawer.currentInfo != null) {
                  if ((var33 = (byte)((var21 = new SegmentPiece(BuildModeDrawer.currentPiece)).getOrientation() + 1)) >= 32) {
                     var33 = 0;
                  }

                  var21.setOrientation(var33);
                  var21.getSegment().getSegmentController().sendBlockMod(new RemoteSegmentPiece(var21, false));
                  System.err.println("SENT (shouldbeorient " + var33 + "); " + var21.getOrientation() + "; " + var21);
                  return;
               }
               break;
            case 6:
               System.err.println("SHATTERING");
               Vector3fb var32 = new Vector3fb(this.getState().getCurrentPlayerObject().getWorldTransform().origin);
               Vector3f var29;
               (var29 = this.getState().getPlayer().getForward(new Vector3f())).scale(3.0F);
               var32.add(var29);
               Transform var30;
               (var30 = new Transform()).setIdentity();
               var30.origin.set(var32);
               this.getState().getWorldDrawer().getShards().voronoiBBShatter((PhysicsExt)this.getState().getPhysics(), var30, (short)1, this.getState().getCurrentSectorId(), var30.origin, (SimpleTransformableSendableObject)null);
               return;
            case 7:
               System.err.println("[DEBUG] REQUESTING PHYSICS CHECK");
               requestPhysicsCheck = true;
               return;
            case 8:
               AbstractScene.setZoomFactorForRender(!this.getState().getWorldDrawer().getGameMapDrawer().isMapActive(), Math.min(1.0F, AbstractScene.getZoomFactorUnchecked() + 0.1F));
               System.err.println("ZOOM: " + AbstractScene.getZoomFactorForRender(!this.getState().getWorldDrawer().getGameMapDrawer().isMapActive()));
               return;
            case 9:
               AbstractScene.setZoomFactorForRender(!this.getState().getWorldDrawer().getGameMapDrawer().isMapActive(), Math.max(0.1F, AbstractScene.getZoomFactorUnchecked() - 0.1F));
               System.err.println("ZOOM: " + AbstractScene.getZoomFactorForRender(!this.getState().getWorldDrawer().getGameMapDrawer().isMapActive()));
               return;
            case 10:
               List var31 = GameResourceLoader.getBlockTextureResourceLoadEntry();
               Controller.getResLoader().enqueueWithReset(var31);
               EngineSettings.G_SHADER_RELOAD.switchSetting();
               System.err.println("RELOADING SHADERS");
               return;
            case 11:
               if (var4 != null && var4.getBlockStyle() != BlockStyle.NORMAL) {
                  BlockShapeAlgorithm.algorithms[var4.getBlockStyle().id - 1][var3].output();
                  return;
               }
            case 12:
            case 13:
            case 15:
            case 17:
            case 19:
            case 20:
            case 24:
            case 26:
            case 27:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 35:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 46:
            case 51:
            case 53:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 69:
            case 70:
            case 71:
            case 72:
            case 73:
            case 74:
            case 78:
            case 82:
            case 83:
            case 84:
            case 85:
            case 86:
            case 89:
            case 90:
            case 91:
            case 92:
            case 93:
            case 94:
            case 95:
            case 96:
            case 97:
            case 98:
            case 99:
            case 100:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 106:
            case 107:
            case 108:
            case 109:
            case 110:
            case 111:
            case 112:
            case 113:
            case 114:
            case 115:
            case 116:
            case 117:
            case 118:
            case 119:
            case 120:
            case 121:
            case 122:
            case 123:
            case 124:
            case 125:
            case 126:
            case 127:
            case 128:
            case 129:
            case 130:
            case 131:
            case 132:
            case 133:
            case 134:
            case 135:
            case 136:
            case 137:
            case 138:
            case 139:
            case 140:
            case 141:
            case 142:
            case 143:
            case 144:
            case 145:
            case 146:
            case 147:
            case 148:
            case 149:
            case 150:
            case 151:
            case 152:
            case 153:
            case 154:
            case 155:
            case 156:
            case 157:
            case 158:
            case 159:
            case 160:
            case 161:
            case 162:
            case 163:
            case 164:
            case 165:
            case 166:
            case 167:
            case 168:
            case 169:
            case 170:
            case 171:
            case 172:
            case 173:
            case 174:
            case 175:
            case 176:
            case 177:
            case 178:
            case 179:
            case 180:
            case 181:
            case 182:
            case 183:
            case 184:
            case 185:
            case 186:
            case 187:
            case 188:
            case 189:
            case 190:
            case 191:
            case 192:
            case 193:
            case 194:
            case 195:
            case 196:
            case 197:
            case 198:
            case 202:
            case 204:
            case 206:
            default:
               break;
            case 14:
               System.err.println("[CLIENT] RE-READING Engine Settings");
               EngineSettings.read();
               return;
            case 16:
               if ((var13 = this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity()) instanceof SegmentController && ((SegmentController)var13).railController.isRoot()) {
                  requestShipMissalign = (SegmentController)var13;
               }

               if (Keyboard.isKeyDown(42)) {
                  (new RadialMenuDialogMain(this.getState())).activate();
                  return;
               }
               break;
            case 18:
               if (Keyboard.isKeyDown(42)) {
                  (new RadialMenuDialogMain(this.getState())).activate();
                  return;
               }
               break;
            case 21:
               BlockShapeAlgorithm[] var26 = BlockShapeAlgorithm.algorithms[1];

               for(int var25 = 0; var25 < var26.length - 1; ++var25) {
                  ((SpikeShapeAlgorithm)var26[var25]).calcAngledSideVertsFromWedges();
               }

               return;
            case 22:
               System.err.println("MACHINES: " + this.getState().getController().getTutorialMode().getMachineNames());
               this.openTutorialPanel();
               return;
            case 23:
               EngineSettings.S_INFO_DRAW.switchSetting();
               System.err.println("info: " + EngineSettings.S_INFO_DRAW.isOn());
               return;
            case 25:
               if (this.getState().isAdmin()) {
                  var13 = null;
                  if ((var17 = this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity()) != null) {
                     var13 = var17;
                  }

                  if (var13 == null) {
                     var13 = this.getState().getCurrentPlayerObject();
                  }

                  if (var13 != null && var13.hasAnyReactors()) {
                     (new ReactorTreeDialog(this.getState(), (ManagedSegmentController)var13)).activate();
                     return;
                  }

                  System.err.println("NOTHING SELECTED OR STRUCTURE IS NOT USING NEW POWER SYSTEM");
                  return;
               }
               break;
            case 28:
               if (EngineSettings.T_ENABLE_TEXTURE_BAKER.isOn()) {
                  WorldDrawer.flagTextureBake = true;
               }
               break;
            case 34:
               EngineSettings.G_DRAW_NO_OVERLAYS.switchSetting();
               return;
            case 36:
               ElementCollectionDrawer.flagAllDirty = true;
               return;
            case 37:
               if (Keyboard.isKeyDown(42)) {
                  this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().debugPush();
                  return;
               }
               break;
            case 38:
               if (EngineSettings.T_ENABLE_TEXTURE_BAKER.isOn()) {
                  WorldDrawer.flagTextureBake = true;
                  return;
               }
               break;
            case 44:
               GameClientState.smoothDisableDebug = !GameClientState.smoothDisableDebug;
               System.err.println("disable smooth: " + GameClientState.smoothDisableDebug);
               this.getState().getController().popupAlertTextMessage("Smooth Debug Disabled: " + GameClientState.smoothDisableDebug);
               return;
            case 45:
               EngineSettings.G_FRAMERATE_FIXED.switchSetting();
               System.err.println("SYNC");
               return;
            case 47:
               if ((var13 = this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity()) != null && var13 instanceof SegmentController) {
                  SegmentController var22 = (SegmentController)var13;
                  if (!Segment2ObjWriter.isRunning()) {
                     Segment2ObjWriter var16 = new Segment2ObjWriter(var22, "modelexport", "testWrite");
                     (new Thread(var16, "WavefrontObjWriter")).start();
                     return;
                  }

                  ((GameClientState)var22.getState()).getController().popupAlertTextMessage("Writing in progress", 0.0F);
                  return;
               }
               break;
            case 48:
               if (this.getState().isAdmin()) {
                  (new RadialMenuDialogDebug(this.getState())).activate();
                  return;
               }
               break;
            case 49:
               if (this.getState().isAdmin()) {
                  var13 = this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity();
                  Object var18 = null;
                  if (var13 != null && var13 instanceof ConfigManagerInterface) {
                     var18 = (ConfigManagerInterface)var13;
                  } else if (this.getState().getCurrentRemoteSector() != null) {
                     var18 = this.getState().getCurrentRemoteSector();
                  }

                  if (var18 != null) {
                     (new EffectConfigEntityDialog(this.getState(), (ConfigManagerInterface)var18, new GUIEffectStat(this.getState(), this.getState().getConfigPool()))).activate();
                  }

                  return;
               }
               break;
            case 50:
               if (Keyboard.isKeyDown(42)) {
                  GameServerController.debugLogoutOnShutdown = !GameServerController.debugLogoutOnShutdown;
                  this.getState().getController().popupAlertTextMessage("DEBUG SHUTDOWN: " + GameServerController.debugLogoutOnShutdown, 0.0F);
                  return;
               }
               break;
            case 52:
               this.getState().getController().parseBlockBehavior("./data/config/blockBehaviorConfig.xml");
               this.getState().getController().reapplyBlockConfigInstantly();
               this.getState().getController().getClientChannel().uploadClientBlockBehavior("./data/config/blockBehaviorConfig.xml");
               return;
            case 54:
               this.getState().getParticleSystemManager().openGUI(this.getState());
               return;
            case 60:
               EngineSettings.G_SHADER_RELOAD.switchSetting();
               System.err.println("RELOADING SHADERS");
               return;
            case 61:
               EngineSettings.F_FRAME_BUFFER.switchSetting();
               System.err.println("fbo: " + EngineSettings.F_FRAME_BUFFER.isOn());
               return;
            case 62:
               EngineSettings.F_BLOOM.switchSetting();
               System.err.println("bloom: " + EngineSettings.F_BLOOM.isOn());
               return;
            case 63:
               BlockShapeAlgorithm.readTexOrder();
               SegmentDrawer.forceFullLightingUpdate = true;
               System.err.println("force contextSwitch: " + SegmentDrawer.forceFullLightingUpdate);
               return;
            case 64:
               BlockShapeAlgorithm.readTexOrder();
               if (Keyboard.isKeyDown(42)) {
                  SegmentDrawer.reinitializeMeshes = true;
               }

               if (!this.getState().getPlayer().getNetworkObject().isAdminClient.get() && !this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getPlayerCharacterManager().isTreeActive()) {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_DEBUGCONTROLMANAGER_0, 0.0F);
                  return;
               }

               this.getState().getController().clearAllSegmentBuffers();
               if (this.getState().getPlayer() != null) {
                  this.getState().getPlayer().hasSpawnWait = true;
                  if (this.getState().getPlayer().getAssingedPlayerCharacter() != null) {
                     this.getState().getPlayer().getAssingedPlayerCharacter().waitingForToSpawn.clear();
                     return;
                  }
               }
               break;
            case 65:
               this.getState().setDbPurgeRequested(true);
               return;
            case 66:
               if (!this.getState().getPlayer().getNetworkObject().isAdminClient.get()) {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_DEBUGCONTROLMANAGER_2, 0.0F);
                  return;
               }

               PlayerInteractionControlManager var12;
               if ((var12 = this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager()).getSegmentControlManager().getSegmentController() != null && ((ManagedSegmentController)var12.getSegmentControlManager().getSegmentController()).getManagerContainer() instanceof StationaryManagerContainer) {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_DEBUGCONTROLMANAGER_1, 0.0F);
                  return;
               }

               if ((var17 = this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity()) == null || !(var17 instanceof Ship) || (var21 = ((Ship)var17).getSegmentBuffer().getPointUnsave(Ship.core)) == null) {
                  Iterator var24 = this.getState().getCurrentSectorEntities().values().iterator();

                  Sendable var27;
                  SegmentPiece var28;
                  do {
                     if (!var24.hasNext()) {
                        return;
                     }
                  } while(!((var27 = (Sendable)var24.next()) instanceof Ship) || ((Ship)var27).getSectorId() != this.getState().getCurrentSectorId() || (var28 = ((Ship)var27).getSegmentBuffer().getPointUnsave(Ship.core)) == null);

                  var12.getSegmentControlManager().exit();
                  var12.getInShipControlManager().setEntered(var28);
                  this.getState().getController().requestControlChange(this.getState().getCharacter(), (PlayerControllable)var28.getSegment().getSegmentController(), new Vector3i(), var28.getAbsolutePos(new Vector3i()), true);
                  return;
               }

               if (var21.getSegment().getSegmentController() instanceof Ship) {
                  var12.getInShipControlManager().setEntered(var21);
                  System.err.println("[CLIENT] Debug enter used; Entered (ship) " + var21 + "; " + var21.getSegment().getSegmentController());
                  this.getState().getController().requestControlChange(this.getState().getCharacter(), (PlayerControllable)var21.getSegment().getSegmentController(), new Vector3i(), var21.getAbsolutePos(new Vector3i()), true);
                  return;
               }
               break;
            case 67:
               EngineSettings.T_ENABLE_TEXTURE_BAKER.switchSetting();
               return;
            case 68:
               if (this.getState().getPlayer().getNetworkObject().isAdminClient.get()) {
                  EngineSettings.P_PHYSICS_DEBUG_ACTIVE.switchSetting();
                  System.err.println("physics: " + EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn());
                  this.getState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_DEBUGCONTROLMANAGER_3, EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()), 0.0F);
                  return;
               }

               this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_DEBUGCONTROLMANAGER_4, 0.0F);
               return;
            case 75:
               BlockShapeAlgorithm.rearrageStat(3, Keyboard.isKeyDown(58));
               SegmentDrawer.forceFullLightingUpdate = true;
               return;
            case 76:
               BlockShapeAlgorithm.rearrageStat(4, Keyboard.isKeyDown(58));
               SegmentDrawer.forceFullLightingUpdate = true;
               return;
            case 77:
               BlockShapeAlgorithm.rearrageStat(5, Keyboard.isKeyDown(58));
               SegmentDrawer.forceFullLightingUpdate = true;
               return;
            case 79:
               BlockShapeAlgorithm.rearrageStat(0, Keyboard.isKeyDown(58));
               SegmentDrawer.forceFullLightingUpdate = true;
               return;
            case 80:
               BlockShapeAlgorithm.rearrageStat(1, Keyboard.isKeyDown(58));
               SegmentDrawer.forceFullLightingUpdate = true;
               return;
            case 81:
               BlockShapeAlgorithm.rearrageStat(2, Keyboard.isKeyDown(58));
               SegmentDrawer.forceFullLightingUpdate = true;
               return;
            case 87:
               System.err.println("[CLIENT] Tab+F11 Pressed: lwjgl event key: " + KeyboardMappings.getEventKeyRaw(var1));
               this.getState().flagRequestServerTime();
               return;
            case 88:
               System.err.println("REPARSING GUI CONFIG");
               Controller.getResLoader().enqueueConfigResources("GuiConfig.xml", false);
               Controller.getResLoader().setLoaded(false);
               if (this.getState().getPlayerInputs().isEmpty()) {
                  PlayerOkCancelInput var11;
                  (var11 = new PlayerOkCancelInput("TTEEDEBUG", this.getState(), 700, 500, "TEST (F1+F12 to Refresh Config Load)", "") {
                     public void pressedOK() {
                        this.deactivate();
                     }

                     public void onDeactivate() {
                     }
                  }).getInputPanel().onInit();
                  GUIContentPane var15;
                  (var15 = ((GUIDialogWindow)var11.getInputPanel().getBackground()).getMainContentPane()).setTextBoxHeightLast(30);
                  GUIHorizontalButtonTablePane var20;
                  (var20 = new GUIHorizontalButtonTablePane(this.getState(), 1, GUIHorizontalArea.HButtonColor.values().length, var15.getContent(0))).onInit();

                  for(int var23 = 0; var23 < GUIHorizontalArea.HButtonColor.values().length; ++var23) {
                     GUIHorizontalButton var5 = (new DebugControlManager.Cbutton(var23)).getButton(this.getState());
                     var20.addButton(var5, 0, var23);
                  }

                  var15.getContent(0).attach(var20);
                  var11.activate();
                  return;
               }
               break;
            case 199:
               WorldDrawer.flagCreatureTool = true;
               CameraMouseState.ungrabForced = !CameraMouseState.ungrabForced;
               return;
            case 200:
               if (Keyboard.isKeyDown(58)) {
                  this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().removeLayer(0, 1, 0);
                  return;
               }

               ++y;
               System.err.println("DEBUG VAL Y: " + y);
               return;
            case 201:
               if (Keyboard.isKeyDown(58)) {
                  this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().removeLayer(1, 0, 0);
                  return;
               }

               if (BuildModeDrawer.currentInfo != null) {
                  if (BuildModeDrawer.currentInfo.getBlockStyle() != BlockStyle.NORMAL) {
                     (var19 = BlockShapeAlgorithm.getAlgo(BuildModeDrawer.currentInfo.getBlockStyle(), BuildModeDrawer.currentPiece.getOrientation())).modAngledVertex(-(1 + (Keyboard.isKeyDown(42) ? 100 : 0)));
                     System.err.println("SET SIDE VERTEX ORDER (next): " + var19.getClass().getSimpleName() + ": " + Element.getSideString(BuildModeDrawer.currentSide));
                  } else {
                     System.err.println("NO CHANGE::::: Block style is not special: " + BuildModeDrawer.currentInfo.getBlockStyle());
                  }

                  SegmentDrawer.forceFullLightingUpdate = true;
                  return;
               }
               break;
            case 203:
               if (Keyboard.isKeyDown(58)) {
                  this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().removeLayer(0, 0, -1);
                  return;
               }

               --x;
               System.err.println("DEBUG VAL X: " + x);
               return;
            case 205:
               if (Keyboard.isKeyDown(58)) {
                  this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().removeLayer(0, 0, 1);
                  return;
               }

               ++x;
               System.err.println("DEBUG VAL X: " + x);
               return;
            case 207:
               if (Keyboard.isKeyDown(29) && Keyboard.isKeyDown(157) && GLFrame.stateChangeRequest == null) {
                  GLFrame.stateChangeRequest = new StateChangeRequest();
                  return;
               }
               break;
            case 208:
               if (Keyboard.isKeyDown(58)) {
                  this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().removeLayer(0, -1, 0);
                  return;
               }

               --y;
               System.err.println("DEBUG VAL Y: " + y);
               return;
            case 209:
               if (Keyboard.isKeyDown(58)) {
                  this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().removeLayer(-1, 0, 0);
                  return;
               }

               if (BuildModeDrawer.currentInfo != null) {
                  if (BuildModeDrawer.currentInfo.getBlockStyle() != BlockStyle.NORMAL) {
                     (var19 = BlockShapeAlgorithm.getAlgo(BuildModeDrawer.currentInfo.getBlockStyle(), BuildModeDrawer.currentPiece.getOrientation())).modAngledVertex(1 + (Keyboard.isKeyDown(42) ? 100 : 0));
                     System.err.println("SET SIDE VERTEX ORDER (next): " + var19.getClass().getSimpleName() + ": " + Element.getSideString(BuildModeDrawer.currentSide));
                  } else {
                     System.err.println("NO CHANGE::::: Block style is not special: " + BuildModeDrawer.currentInfo.getBlockStyle());
                  }

                  SegmentDrawer.forceFullLightingUpdate = true;
                  return;
               }
               break;
            case 210:
               if (this.getState().isAdmin()) {
                  RuleSetManager var10;
                  (var10 = new RuleSetManager(this.getState().getGameState().getRuleManager())).setState(this.getState());
                  GUIRuleSetStat var14;
                  (var14 = new GUIRuleSetStat(this.getState(), var10)).gameState = this.getState().getGameState();
                  (new RuleSetConfigDialogGame(this.getState(), var14)).activate();
                  return;
               }
            }
         }

      } catch (StateParameterNotFoundException var8) {
         var8.printStackTrace();
      } catch (IOException var9) {
         var9.printStackTrace();
      }
   }

   public void handleMouseEvent(MouseEvent var1) {
      super.handleMouseEvent(var1);
   }

   public void update(Timer var1) {
      if (Keyboard.isKeyDown(54)) {
         Transform var4;
         Vector3f var10000;
         if (BuildModeDrawer.currentInfo != null && BuildModeDrawer.currentInfo.getBlockStyle() == BlockStyle.NORMAL24) {
            Vector3f var10;
            var10000 = var10 = BuildModeDrawer.currentPiece.getAbsolutePos(new Vector3f());
            var10000.x -= 16.0F;
            var10.y -= 16.0F;
            var10.z -= 16.0F;
            Oriencube var12;
            Transform var13 = (var12 = (Oriencube)BlockShapeAlgorithm.getAlgo(BuildModeDrawer.currentInfo.getBlockStyle(), BuildModeDrawer.currentPiece.getOrientation())).getPrimaryTransform(var10, 1, new Transform());
            var4 = var12.getSecondaryTransform(new Transform());
            Transform var15;
            (var15 = new Transform()).set(BuildModeDrawer.currentPiece.getSegmentController().getClientTransform());
            var15.mul(var13);
            var15.mul(var4);
            DebugDrawer.addArrowFromTransform(var15);
            return;
         }

         if (BuildModeDrawer.currentInfo != null && BuildModeDrawer.currentInfo.getBlockStyle() != BlockStyle.NORMAL) {
            Vector3i[] var2;
            Vector3f var3;
            int var5;
            Vector3f var6;
            DebugPoint var7;
            BlockShapeAlgorithm var8;
            if ((var8 = BlockShapeAlgorithm.getAlgo(BuildModeDrawer.currentInfo.getBlockStyle(), BuildModeDrawer.currentPiece.getOrientation())) != null && var8 instanceof WedgeShapeAlgorithm) {
               var2 = ((WedgeShapeAlgorithm)var8).getAngledSideVerts();
               var10000 = var3 = BuildModeDrawer.currentPiece.getAbsolutePos(new Vector3f());
               var10000.x -= 16.0F;
               var3.y -= 16.0F;
               var3.z -= 16.0F;
               (var4 = new Transform()).set(BuildModeDrawer.currentPiece.getSegmentController().getClientTransform());

               for(var5 = 0; var5 < var2.length; ++var5) {
                  var10000 = var6 = new Vector3f(var3);
                  var10000.x += (float)var2[var5].x * 0.5F;
                  var6.y += (float)var2[var5].y * 0.5F;
                  var6.z += (float)var2[var5].z * 0.5F;
                  var4.transform(var6);
                  var7 = new DebugPoint(var6, new Vector4f(1.0F, 0.0F, 0.0F, 1.0F), 0.15F);
                  DebugDrawer.points.addElement(var7);
               }
            }

            if (var8 != null && var8 instanceof TetrahedronShapeAlgorithm) {
               var2 = ((TetrahedronShapeAlgorithm)var8).getAngledSideVerts();
               var10000 = var3 = BuildModeDrawer.currentPiece.getAbsolutePos(new Vector3f());
               var10000.x -= 16.0F;
               var3.y -= 16.0F;
               var3.z -= 16.0F;
               (var4 = new Transform()).set(BuildModeDrawer.currentPiece.getSegmentController().getClientTransform());

               for(var5 = 0; var5 < var2.length; ++var5) {
                  var10000 = var6 = new Vector3f(var3);
                  var10000.x += (float)var2[var5].x * 0.5F;
                  var6.y += (float)var2[var5].y * 0.5F;
                  var6.z += (float)var2[var5].z * 0.5F;
                  var4.transform(var6);
                  var7 = new DebugPoint(var6, new Vector4f(1.0F, 0.0F, 0.0F, 1.0F), 0.15F);
                  DebugDrawer.points.addElement(var7);
               }
            }

            if (var8 != null && var8 instanceof PentaShapeAlgorithm) {
               var2 = ((PentaShapeAlgorithm)var8).getAngledSideVerts();
               var10000 = var3 = BuildModeDrawer.currentPiece.getAbsolutePos(new Vector3f());
               var10000.x -= 16.0F;
               var3.y -= 16.0F;
               var3.z -= 16.0F;
               (var4 = new Transform()).set(BuildModeDrawer.currentPiece.getSegmentController().getClientTransform());

               for(var5 = 0; var5 < var2.length; ++var5) {
                  var10000 = var6 = new Vector3f(var3);
                  var10000.x += (float)var2[var5].x * 0.5F;
                  var6.y += (float)var2[var5].y * 0.5F;
                  var6.z += (float)var2[var5].z * 0.5F;
                  var4.transform(var6);
                  var7 = new DebugPoint(var6, new Vector4f(1.0F, 0.0F, 0.0F, 1.0F), 0.15F);
                  DebugDrawer.points.addElement(var7);
               }
            }

            if (var8 != null && var8 instanceof SpikeShapeAlgorithm) {
               Vector3i[][] var11 = ((SpikeShapeAlgorithm)var8).getAngledSideVerts();
               var10000 = var3 = BuildModeDrawer.currentPiece.getAbsolutePos(new Vector3f());
               var10000.x -= 16.0F;
               var3.y -= 16.0F;
               var3.z -= 16.0F;
               (var4 = new Transform()).set(BuildModeDrawer.currentPiece.getSegmentController().getClientTransform());

               for(var5 = 0; var5 < var11.length; ++var5) {
                  for(int var14 = 0; var14 < var11[var5].length; ++var14) {
                     Vector3f var16;
                     var10000 = var16 = new Vector3f(var3);
                     var10000.x += (float)var11[var5][var14].x * 0.5F;
                     var16.y += (float)var11[var5][var14].y * 0.5F;
                     var16.z += (float)var11[var5][var14].z * 0.5F;
                     var4.transform(var16);
                     DebugPoint var9;
                     if (var5 == 0) {
                        var9 = new DebugPoint(var16, new Vector4f(1.0F, 0.0F, 0.0F, 1.0F), 0.15F);
                        DebugDrawer.points.addElement(var9);
                     } else {
                        var9 = new DebugPoint(var16, new Vector4f(0.0F, 1.0F, 0.0F, 1.0F), 0.15F);
                        DebugDrawer.points.addElement(var9);
                     }
                  }
               }
            }
         }
      }

   }

   static class Cbutton {
      private GUIHorizontalArea.HButtonColor c;
      public int m;

      public Cbutton(int var1) {
         this.c = GUIHorizontalArea.HButtonColor.values()[var1];
      }

      public GUIHorizontalButton getButton(InputState var1) {
         return new GUIHorizontalButton(var1, this.c, this.c.name(), new GUICallback() {
            public boolean isOccluded() {
               return false;
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  Cbutton.this.m = (Cbutton.this.m + 1) % 3;
               }

            }
         }, new GUIActiveInterface() {
            public boolean isActive() {
               return true;
            }
         }, new GUIActivationHighlightCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return Cbutton.this.m != 2;
            }

            public boolean isHighlighted(InputState var1) {
               return Cbutton.this.m == 1;
            }
         });
      }
   }
}
