package org.schema.game.client.controller.manager;

import org.schema.game.client.controller.JoinMenu;
import org.schema.game.client.controller.manager.ingame.InGameControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.WorldDrawer;
import org.schema.game.client.view.gui.options.GUIKeyboardElement;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public class GlobalGameControlManager extends AbstractControlManager {
   public static final int HELP_KEY = -1;
   public static final int TUTORIAL_SKIP_KEY = 22;
   public static final int TUTORIAL_END_KEY = 211;
   public static final int TUTORIAL_END_STEP_KEY = 207;
   public static final int TUTORIAL_REPEAT_STEP_KEY = 199;
   public static final int TUTORIAL_RESTART_KEY = 210;
   public static final int MAIN_MENU_KEY = 1;
   private DebugControlManager debugControlManager;
   private InGameControlManager ingameControlManager;
   private OptionGameControlManager optionsControlManager;
   private MainMenuManager mainMenuManager;
   private HelpManager helpManager;
   private boolean initialized;

   public GlobalGameControlManager(GameClientState var1) {
      super(var1);
   }

   public void activateMainMenu() {
      if (!GUIKeyboardElement.active()) {
         boolean var1;
         if (!(var1 = this.mainMenuManager.isActive()) && this.optionsControlManager.isActive()) {
            this.optionsControlManager.setActive(false);
            this.optionsControlManager.deactivateMenu();
         }

         this.mainMenuManager.setActive(!var1);
      }
   }

   public DebugControlManager getDebugControlManager() {
      return this.debugControlManager;
   }

   public HelpManager getHelpManager() {
      return this.helpManager;
   }

   public InGameControlManager getIngameControlManager() {
      return this.ingameControlManager;
   }

   public MainMenuManager getMainMenuManager() {
      return this.mainMenuManager;
   }

   public OptionGameControlManager getOptionsControlManager() {
      return this.optionsControlManager;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      int var2;
      if (KeyboardMappings.getEventKeyState(var1, this.getState())) {
         var2 = KeyboardMappings.getEventKeySingle(var1);
         if (this.getState().getController().getTutorialController().isBackgroundVideoActive() && (var2 == 62 || var2 == 61 || var2 == 201 || var2 == 209)) {
            return;
         }
      }

      var2 = this.getState().getController().getPlayerInputs().size();
      if (KeyboardMappings.PLAYER_LIST.isDown(this.getState())) {
         this.debugControlManager.handleKeyEvent(var1);
      }

      boolean var3 = false;
      super.handleKeyEvent(var1);
      int var4;
      if (!this.ingameControlManager.isActive()) {
         var4 = this.getState().getController().getPlayerInputs().size();
         this.getState().getController().getInputController().handleKeyEventInputPanels(var1);
         var3 = var4 > this.getState().getController().getPlayerInputs().size();
      }

      if (KeyboardMappings.getEventKeyState(var1, this.getState())) {
         var4 = KeyboardMappings.getEventKeySingle(var1);
         if (this.getState().getController().getTutorialController().isBackgroundVideoActive() && (var4 == 62 || var4 == 61 || var4 == 201 || var4 == 209)) {
            return;
         }

         if (var4 == KeyboardMappings.RELEASE_MOUSE.getMapping()) {
            CameraMouseState.ungrabForced = !CameraMouseState.ungrabForced;
         } else if (var4 == KeyboardMappings.SCREENSHOT_WITH_GUI.getMapping() && !this.getState().isDebugKeyDown()) {
            WorldDrawer.flagScreenShotWithGUI = true;
         } else if (var4 == KeyboardMappings.SCREENSHOT_WITHOUT_GUI.getMapping() && !this.getState().isDebugKeyDown()) {
            WorldDrawer.flagScreenShotWithoutGUI = true;
         } else if (!KeyboardMappings.PLAYER_LIST.isDown(this.getState()) && var4 == KeyboardMappings.TUTORIAL.getMapping()) {
            if (!this.getState().getController().getTutorialController().isTutorialSelectorActive()) {
               this.getState().getController().getTutorialController().onActivateFromTopTaskBar();
            } else {
               this.getState().getController().getTutorialController().onDeactivateFromTopTaskBar();
            }
         }

         boolean var5;
         switch(var4) {
         case -1:
            var5 = this.getHelpManager().isActive();
            this.getHelpManager().setActive(!var5);
            return;
         case 1:
            if (EngineSettings.S_EXIT_ON_ESC.isOn()) {
               System.err.println("[GLOBALGAMECONTROLLER] ESCAPE: EXIT: " + EngineSettings.S_EXIT_ON_ESC.isOn());
               GLFrame.setFinished(true);
               return;
            }

            if (this.ingameControlManager.getChatControlManager().isActive()) {
               this.ingameControlManager.getChatControlManager().setActive(false);
               this.getState().getChat().getTextInput().clear();
               return;
            }

            if (!this.mainMenuManager.isActive()) {
               if (var2 != 0) {
                  break;
               }

               if (this.getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().isSuspended() || !this.getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().isActive()) {
                  if (var3) {
                     break;
                  }

                  var5 = false;

                  for(var2 = 0; var2 < this.getState().getController().getPlayerInputs().size(); ++var2) {
                     if (this.getState().getController().getPlayerInputs().get(var2) instanceof JoinMenu) {
                        var5 = true;
                     }
                  }

                  if (!var5) {
                     if (!var5 && this.getState().getController().getPlayerInputs().size() == 0 && !this.isHinderedInteraction()) {
                        this.getIngameControlManager().getPlayerGameControlManager().deactivateAll();
                     }

                     return;
                  }
               }
            }

            this.activateMainMenu();
            break;
         case 22:
            if (!this.ingameControlManager.getChatControlManager().isActive() && this.getState().getController().getTutorialMode() != null && this.getState().getController().getPlayerInputs().isEmpty()) {
               this.getState().getController().getTutorialMode().skip();
               return;
            }
         }
      }

   }

   public void handleMouseEvent(MouseEvent var1) {
      super.handleMouseEvent(var1);
      if (var1.state && CameraMouseState.ungrabForced) {
         CameraMouseState.ungrabForced = false;
      }

   }

   public void update(Timer var1) {
      if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
         this.debugControlManager.update(var1);
      }

      super.update(var1);
   }

   public void initialize() {
      this.debugControlManager = new DebugControlManager(this.getState());
      this.ingameControlManager = new InGameControlManager(this.getState());
      this.optionsControlManager = new OptionGameControlManager(this.getState());
      this.mainMenuManager = new MainMenuManager(this.getState());
      this.helpManager = new HelpManager(this.getState());
      this.getControlManagers().add(this.debugControlManager);
      this.getControlManagers().add(this.ingameControlManager);
      this.getControlManagers().add(this.optionsControlManager);
      this.getControlManagers().add(this.mainMenuManager);
      this.getControlManagers().add(this.helpManager);
      this.ingameControlManager.setActive(true);
      this.initialized = true;
   }

   public boolean isInitialized() {
      return this.initialized;
   }
}
