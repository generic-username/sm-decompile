package org.schema.game.client.controller.manager;

import java.util.Iterator;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.ai.stateMachines.AiInterface;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;

public class AiConfigurationManager extends AbstractControlManager implements GUICallback {
   private boolean needsUpdate;
   private AiInterface aiInterface;
   private boolean canEdit;

   public AiConfigurationManager(GameClientState var1) {
      super(var1);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (this.isTreeActive() && !this.isSuspended()) {
         Iterator var3 = this.getState().getController().getInputController().getMouseEvents().iterator();

         while(var3.hasNext()) {
            if ((var2 = (MouseEvent)var3.next()).button == 0) {
               boolean var10000 = var2.state;
            }
         }
      }

   }

   public boolean isOccluded() {
      return !this.getState().getController().getPlayerInputs().isEmpty();
   }

   public AiInterface getAi() {
      return this.aiInterface;
   }

   public void setAi(AiInterface var1) {
      this.aiInterface = var1;
      this.setNeedsUpdate(true);
   }

   public PlayerInteractionControlManager getInteractionManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager();
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public void onSwitch(boolean var1) {
      CameraMouseState.setGrabbed(!var1);
      if (var1) {
         this.setChanged();
         this.notifyObservers(this.getAi());
         this.getState().getController().queueUIAudio("0022_menu_ui - swoosh scroll large");
      } else {
         this.getState().getController().queueUIAudio("0022_menu_ui - swoosh scroll small");
      }

      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController().suspend(var1);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().suspend(var1);
      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      CameraMouseState.setGrabbed(false);
      this.getInteractionManager().suspend(true);
   }

   public boolean isNeedsUpdate() {
      return this.needsUpdate;
   }

   public void setNeedsUpdate(boolean var1) {
      this.needsUpdate = var1;
   }

   public boolean canEdit() {
      return this.canEdit;
   }

   public void setCanEdit(boolean var1) {
      this.canEdit = var1;
   }
}
