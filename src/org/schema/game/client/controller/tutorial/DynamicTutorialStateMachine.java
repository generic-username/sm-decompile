package org.schema.game.client.controller.tutorial;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.tutorial.states.ActivateBlockTestState;
import org.schema.game.client.controller.tutorial.states.ActivateGravityTestState;
import org.schema.game.client.controller.tutorial.states.AssignWeaponTestState;
import org.schema.game.client.controller.tutorial.states.ConnectedFromToTestState;
import org.schema.game.client.controller.tutorial.states.CreateShipTestState;
import org.schema.game.client.controller.tutorial.states.DestroyEntityTestState;
import org.schema.game.client.controller.tutorial.states.EnterLastSpawnedShipTestState;
import org.schema.game.client.controller.tutorial.states.EnterUIDSegmentControllerTestState;
import org.schema.game.client.controller.tutorial.states.InstantiateInventoyTestState;
import org.schema.game.client.controller.tutorial.states.OpenChestTestState;
import org.schema.game.client.controller.tutorial.states.PlaceElementOnLastSpawnedTestState;
import org.schema.game.client.controller.tutorial.states.PlaceElementTestState;
import org.schema.game.client.controller.tutorial.states.PutInChestTestState;
import org.schema.game.client.controller.tutorial.states.RestoreInventoyTestState;
import org.schema.game.client.controller.tutorial.states.SatisfyingCondition;
import org.schema.game.client.controller.tutorial.states.SelectProductionTestState;
import org.schema.game.client.controller.tutorial.states.ShipBuildControllerTestState;
import org.schema.game.client.controller.tutorial.states.ShipFlightControllerTestState;
import org.schema.game.client.controller.tutorial.states.SkipTestState;
import org.schema.game.client.controller.tutorial.states.TalkToNPCTestState;
import org.schema.game.client.controller.tutorial.states.TeleportToTestState;
import org.schema.game.client.controller.tutorial.states.TeleportToTutorialSector;
import org.schema.game.client.controller.tutorial.states.TextState;
import org.schema.game.client.controller.tutorial.states.TutorialEnded;
import org.schema.game.client.controller.tutorial.states.TutorialEndedTextState;
import org.schema.game.client.controller.tutorial.states.TutorialFailedState;
import org.schema.game.client.controller.tutorial.states.TypeInInventoryTestState;
import org.schema.game.client.controller.tutorial.states.TypeInPersonalCapsuleRefineryTestState;
import org.schema.game.client.controller.tutorial.states.TypeInPersonalFactoryAssemberTestState;
import org.schema.game.client.controller.tutorial.states.WaitingTextState;
import org.schema.game.client.controller.tutorial.states.WeaponPanelClosedTestState;
import org.schema.game.client.controller.tutorial.states.WeaponPanelOpenTestState;
import org.schema.game.client.controller.tutorial.states.conditions.TutorialConditionBlockExists;
import org.schema.game.client.controller.tutorial.states.conditions.TutorialConditionChestOpen;
import org.schema.game.client.controller.tutorial.states.conditions.TutorialConditionInBuildMode;
import org.schema.game.client.controller.tutorial.states.conditions.TutorialConditionInFlightMode;
import org.schema.game.client.controller.tutorial.states.conditions.TutorialConditionInLastShip;
import org.schema.game.client.controller.tutorial.states.conditions.TutorialConditionInShipUID;
import org.schema.game.client.controller.tutorial.states.conditions.TutorialConditionProductionSet;
import org.schema.game.client.controller.tutorial.states.conditions.TutorialConditionWeaponPanelOpen;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.ai.MachineProgram;
import org.schema.schine.ai.stateMachines.AiEntityState;
import org.schema.schine.ai.stateMachines.FiniteStateMachine;
import org.schema.schine.ai.stateMachines.Message;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.resource.FileExt;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DynamicTutorialStateMachine extends FiniteStateMachine {
   private State tutorialStartState;
   private State tutorialEndState;
   private TutorialEnded tutorialEnded;
   private TutorialFailedState tutorialFailed;
   private String tutorialName;

   public DynamicTutorialStateMachine(AiEntityState var1, MachineProgram var2, String var3, File var4) {
      super(var1, var2, var3);
   }

   public void createFSM(String var1) {
      DocumentBuilderFactory var2 = DocumentBuilderFactory.newInstance();
      this.tutorialName = (new FileExt(var1)).getParentFile().getName();
      GameClientState var3 = this.getMachineProgram().getState();

      try {
         Document var10000 = var2.newDocumentBuilder().parse(new FileExt(var1));
         var1 = null;
         Element var11;
         String var13 = (var11 = var10000.getDocumentElement()).getAttribute("description");
         NodeList var4 = var11.getElementsByTagName("Start");
         NodeList var5 = var11.getElementsByTagName("End");
         if (var4.getLength() == 0 || var5.getLength() == 0) {
            throw new TutorialException("Start or End state not found");
         }

         Object2ObjectOpenHashMap var14 = (Object2ObjectOpenHashMap)TutorialMode.translation.get(var13);
         this.tutorialFailed = new TutorialFailedState(this.getObj(), var3);
         this.tutorialEnded = new TutorialEnded(this.getObj());
         this.tutorialStartState = this.parseState(var4.item(0), var3, var14);
         this.tutorialEndState = this.parseState(var5.item(0), var3, var14);
         Object2ObjectOpenHashMap var15;
         (var15 = new Object2ObjectOpenHashMap()).put("start", this.tutorialStartState);
         var15.put("end", this.tutorialEndState);
         NodeList var12 = var11.getChildNodes();

         Node var6;
         int var16;
         for(var16 = 0; var16 < var12.getLength(); ++var16) {
            if ((var6 = var12.item(var16)).getNodeType() == 1 && !var6.getNodeName().toLowerCase(Locale.ENGLISH).equals("start") && !var6.getNodeName().toLowerCase(Locale.ENGLISH).equals("end")) {
               State var7 = this.parseState(var6, var3, var14);
               var15.put(var6.getNodeName().toLowerCase(Locale.ENGLISH), var7);
            }
         }

         for(var16 = 0; var16 < var12.getLength(); ++var16) {
            if ((var6 = var12.item(var16)).getNodeType() == 1) {
               this.parseTransitions(var6.getNodeName().toLowerCase(Locale.ENGLISH), var6, var15, var3);
            }
         }

         this.tutorialStartState.addTransition(Transition.TUTORIAL_END, this.tutorialEndState);
         this.tutorialStartState.addTransition(Transition.TUTORIAL_FAILED, this.tutorialFailed);
         this.tutorialStartState.addTransition(Transition.TUTORIAL_STOP, this.tutorialEnded);
         this.tutorialEndState.addTransition(Transition.TUTORIAL_RESTART, this.tutorialStartState);
         this.tutorialEndState.addTransition(Transition.TUTORIAL_FAILED, this.tutorialFailed);
         this.tutorialEndState.addTransition(Transition.CONDITION_SATISFIED, this.tutorialEnded);
         this.tutorialEnded.addTransition(Transition.TUTORIAL_RESTART, this.tutorialStartState);
         this.tutorialFailed.addTransition(Transition.TUTORIAL_FAILED, this.tutorialEnded);
      } catch (SAXException var8) {
         var8.printStackTrace();
         throw new TutorialException(var8);
      } catch (IOException var9) {
         var9.printStackTrace();
         throw new TutorialException(var9);
      } catch (ParserConfigurationException var10) {
         var10.printStackTrace();
         throw new TutorialException(var10);
      }

      this.tutorialEndState.addTransition(Transition.CONDITION_SATISFIED, this.tutorialEnded);
      this.setStartingState(this.tutorialStartState);
   }

   public TutorialMode getMachineProgram() {
      return (TutorialMode)super.getMachineProgram();
   }

   public void onMsg(Message var1) {
   }

   private void parseTransitions(String var1, Node var2, Object2ObjectOpenHashMap var3, GameClientState var4) {
      NodeList var13 = var2.getChildNodes();
      State var12;
      (var12 = (State)var3.get(var1.toLowerCase(Locale.ENGLISH))).addTransition(Transition.TUTORIAL_STOP, this.tutorialEnded);
      var12.addTransition(Transition.TUTORIAL_END, this.tutorialEndState);
      var12.addTransition(Transition.TUTORIAL_RESTART, this.tutorialStartState);
      var12.addTransition(Transition.TUTORIAL_FAILED, this.tutorialFailed);

      for(int var14 = 0; var14 < var13.getLength(); ++var14) {
         Node var5;
         if ((var5 = var13.item(var14)).getNodeType() == 1) {
            String var6;
            State var7;
            if (var5.getNodeName().toLowerCase(Locale.ENGLISH).equals("satisfied")) {
               var6 = var5.getTextContent().toLowerCase(Locale.ENGLISH);
               var7 = (State)var3.get(var6);
               if (var5.getParentNode().getNodeName().toLowerCase(Locale.ENGLISH).equals(var6)) {
                  throw new TutorialException("Transition state not found for " + var5.getNodeName() + "->" + var5.getParentNode().getNodeName() + " can't point to itself");
               }

               if (var7 == null) {
                  throw new TutorialException("Transition state not found for " + var5.getNodeName() + "->" + var5.getParentNode().getNodeName() + " searched: " + var6 + " (Available: " + var3.keySet() + ")");
               }

               var12.addTransition(Transition.CONDITION_SATISFIED, var7);
            }

            if (var5.getNodeName().toLowerCase(Locale.ENGLISH).equals("back")) {
               var6 = var5.getTextContent().toLowerCase(Locale.ENGLISH);
               var7 = (State)var3.get(var6);
               if (var5.getParentNode().getNodeName().toLowerCase(Locale.ENGLISH).equals(var6)) {
                  throw new TutorialException("Transition state not found for " + var5.getNodeName() + "->" + var5.getParentNode().getNodeName() + " can't point to itself");
               }

               if (var7 == null) {
                  throw new TutorialException("Transition state not found for " + var5.getNodeName() + "->" + var5.getParentNode().getNodeName());
               }

               var12.addTransition(Transition.BACK, var7);
            }

            if (var5.getNodeName().toLowerCase(Locale.ENGLISH).equals("backback")) {
               var6 = var5.getTextContent().toLowerCase(Locale.ENGLISH);
               var7 = (State)var3.get(var6);
               if (var5.getParentNode().getNodeName().toLowerCase(Locale.ENGLISH).equals(var6)) {
                  throw new TutorialException("Transition state not found for " + var5.getNodeName() + "->" + var5.getParentNode().getNodeName() + " can't point to itself");
               }

               if (var7 == null) {
                  throw new TutorialException("Transition state not found for " + var5.getNodeName() + "->" + var5.getParentNode().getNodeName());
               }

               var12.addTransition(Transition.BACK, var7);
            }

            if (var5.getNodeName().toLowerCase(Locale.ENGLISH).equals("condition")) {
               if (var5.getAttributes().getNamedItem("failTo") == null) {
                  throw new TutorialException("Condition needs attribute: 'failTo': '" + var5.getAttributes() + "' " + var5.getNodeName() + "->" + var5.getParentNode().getNodeName());
               }

               var6 = var5.getAttributes().getNamedItem("failTo").getNodeValue();
               var7 = (State)var3.get(var6.toLowerCase(Locale.ENGLISH));
               if (var5.getParentNode().getNodeName().toLowerCase(Locale.ENGLISH).equals(var6.toLowerCase(Locale.ENGLISH))) {
                  throw new TutorialException("Transition state not found for " + var5.getNodeName() + "->" + var5.getParentNode().getNodeName() + " can't point to itself");
               }

               if (var7 == null) {
                  throw new TutorialException("Condition fail to state doesnt exist: '" + var6 + "' " + var5.getAttributes() + "' " + var5.getNodeName() + "->" + var5.getParentNode().getNodeName());
               }

               Object var15;
               if (var5.getTextContent().toLowerCase(Locale.ENGLISH).equals("inshipuid")) {
                  if (var5.getAttributes().getNamedItem("uid") == null) {
                     throw new TutorialException("Condition needs attribute: 'uid': '" + var5.getAttributes() + "' " + var5.getNodeName() + "->" + var5.getParentNode().getNodeName());
                  }

                  var6 = var5.getAttributes().getNamedItem("uid").getNodeValue();
                  var15 = new TutorialConditionInShipUID(var12, var7, var6);
               } else if (var5.getTextContent().toLowerCase(Locale.ENGLISH).equals("inflightmode")) {
                  var15 = new TutorialConditionInFlightMode(var12, var7);
               } else if (var5.getTextContent().toLowerCase(Locale.ENGLISH).equals("inbuildmode")) {
                  var15 = new TutorialConditionInBuildMode(var12, var7);
               } else if (var5.getTextContent().toLowerCase(Locale.ENGLISH).equals("inlastspawnedship")) {
                  var15 = new TutorialConditionInLastShip(var12, var7);
               } else if (var5.getTextContent().toLowerCase(Locale.ENGLISH).equals("weaponpanelopen")) {
                  var15 = new TutorialConditionWeaponPanelOpen(var12, var7);
               } else {
                  String[] var8;
                  Vector3i var16;
                  if (var5.getTextContent().toLowerCase(Locale.ENGLISH).equals("chestopen")) {
                     if (var5.getAttributes().getNamedItem("pos") == null) {
                        throw new TutorialException("Condition needs attribute: 'pos': '" + var5.getAttributes() + "' " + var5.getNodeName() + "->" + var5.getParentNode().getNodeName());
                     }

                     try {
                        var8 = var5.getAttributes().getNamedItem("pos").getNodeValue().split(",", 3);
                        var16 = new Vector3i(Integer.parseInt(var8[0].trim()), Integer.parseInt(var8[1].trim()), Integer.parseInt(var8[2].trim()));
                        var15 = new TutorialConditionChestOpen(var12, var7, var16);
                     } catch (Exception var11) {
                        var11.printStackTrace();
                        throw new TutorialException("Failed: " + var5.getAttributes() + "' " + var5.getNodeName() + "->" + var5.getParentNode().getNodeName(), var11);
                     }
                  } else if (var5.getTextContent().toLowerCase(Locale.ENGLISH).equals("blockexists")) {
                     if (var5.getAttributes().getNamedItem("pos") == null) {
                        throw new TutorialException("Condition needs attribute: 'pos': '" + var5.getAttributes() + "' " + var5.getNodeName() + "->" + var5.getParentNode().getNodeName());
                     }

                     try {
                        var8 = var5.getAttributes().getNamedItem("pos").getNodeValue().split(",", 3);
                        var16 = new Vector3i(Integer.parseInt(var8[0].trim()), Integer.parseInt(var8[1].trim()), Integer.parseInt(var8[2].trim()));
                        var15 = new TutorialConditionBlockExists(var12, var7, var16);
                     } catch (Exception var10) {
                        var10.printStackTrace();
                        throw new TutorialException("Failed: " + var5.getAttributes() + "' " + var5.getNodeName() + "->" + var5.getParentNode().getNodeName(), var10);
                     }
                  } else {
                     if (!var5.getTextContent().toLowerCase(Locale.ENGLISH).equals("setproduction")) {
                        throw new TutorialException("Condition type not found: '" + var5.getTextContent() + "' " + var5.getNodeName() + "->" + var5.getParentNode().getNodeName());
                     }

                     if (var5.getAttributes().getNamedItem("pos") == null) {
                        throw new TutorialException("Condition needs attribute: 'pos': '" + var5.getAttributes() + "' " + var5.getNodeName() + "->" + var5.getParentNode().getNodeName());
                     }

                     if (var5.getAttributes().getNamedItem("type") == null) {
                        throw new TutorialException("Condition needs attribute: 'type': '" + var5.getAttributes() + "' " + var5.getNodeName() + "->" + var5.getParentNode().getNodeName());
                     }

                     try {
                        short var17 = Short.parseShort(var5.getAttributes().getNamedItem("type").getNodeValue());
                        var8 = var5.getAttributes().getNamedItem("pos").getNodeValue().split(",", 3);
                        var16 = new Vector3i(Integer.parseInt(var8[0].trim()), Integer.parseInt(var8[1].trim()), Integer.parseInt(var8[2].trim()));
                        var15 = new TutorialConditionProductionSet(var12, var7, var16, var17);
                     } catch (Exception var9) {
                        var9.printStackTrace();
                        throw new TutorialException("Failed: " + var5.getAttributes() + "' " + var5.getNodeName() + "->" + var5.getParentNode().getNodeName(), var9);
                     }
                  }
               }

               ((SatisfyingCondition)var12).getTutorialConditions().add(var15);
            }
         }
      }

   }

   public String getAttrib(Node var1, String var2) {
      Node var3;
      if ((var3 = var1.getAttributes().getNamedItem(var2)) == null) {
         throw new TutorialException("attribute \"" + var2 + "\" not found in " + var1.getNodeName());
      } else {
         return var3.getNodeValue();
      }
   }

   private State parseState(Node var1, GameClientState var2, Object2ObjectOpenHashMap var3) {
      String var4 = this.getAttrib(var1, "type");
      Object var5 = null;
      NodeList var6 = var1.getChildNodes();
      String var7 = var1.getNodeName();
      String var8 = null;
      int var9 = -1;
      String var10 = null;
      Vector3i var11 = null;
      Vector3i var12 = null;
      String var13 = null;
      String var14 = null;
      Boolean var15 = null;
      short var16 = -1;
      int var17 = 1;
      boolean var18 = false;
      boolean var19 = false;
      boolean var20 = false;
      int var21 = 1;

      for(int var22 = 0; var22 < var6.getLength(); ++var22) {
         Node var23;
         if ((var23 = var6.item(var22)).getNodeType() == 1) {
            try {
               if (var23.getNodeName().toLowerCase(Locale.ENGLISH).equals("content")) {
                  String var26 = null;
                  if (var3 != null) {
                     var26 = (String)var3.get(var7);
                  }

                  if (var26 != null) {
                     var8 = this.formatText(var26, var2);
                  } else {
                     var8 = this.formatText(var23.getTextContent(), var2);
                  }
               } else if (var23.getNodeName().toLowerCase(Locale.ENGLISH).equals("duration")) {
                  var9 = Integer.parseInt(var23.getTextContent());
               } else if (var23.getNodeName().toLowerCase(Locale.ENGLISH).equals("image")) {
                  var10 = this.tutorialName + "_" + var23.getTextContent();
               } else if (var23.getNodeName().toLowerCase(Locale.ENGLISH).equals("npcname")) {
                  var14 = var23.getTextContent();
               } else if (var23.getNodeName().toLowerCase(Locale.ENGLISH).equals("structureuid")) {
                  var13 = var23.getTextContent();
               } else if (var23.getNodeName().toLowerCase(Locale.ENGLISH).equals("typea")) {
                  var16 = Short.parseShort(var23.getTextContent());
               } else if (var23.getNodeName().toLowerCase(Locale.ENGLISH).equals("count")) {
                  var21 = Integer.parseInt(var23.getTextContent());
               } else if (var23.getNodeName().toLowerCase(Locale.ENGLISH).equals("clear")) {
                  var15 = Boolean.parseBoolean(var23.getTextContent());
               } else if (var23.getNodeName().toLowerCase(Locale.ENGLISH).equals("recordposition")) {
                  Boolean.parseBoolean(var23.getTextContent());
               } else if (var23.getNodeName().toLowerCase(Locale.ENGLISH).equals("skipwindowmessage")) {
                  var20 = Boolean.parseBoolean(var23.getTextContent());
               } else if (var23.getNodeName().toLowerCase(Locale.ENGLISH).equals("slot")) {
                  if ((var17 = Integer.parseInt(var23.getTextContent())) < 0 || var17 > 10) {
                     throw new TutorialException("At " + var1.getNodeName() + "; " + var1.getParentNode().getNodeName() + ": slot invalid: must be [0-10]: but was" + var17);
                  }
               } else if (var23.getNodeName().toLowerCase(Locale.ENGLISH).equals("typeb")) {
                  Short.parseShort(var23.getTextContent());
               } else if (var23.getNodeName().toLowerCase(Locale.ENGLISH).equals("limitedblocksupply")) {
                  var18 = Boolean.parseBoolean(var23.getTextContent());
               } else if (var23.getNodeName().toLowerCase(Locale.ENGLISH).equals("active")) {
                  var19 = Boolean.parseBoolean(var23.getTextContent());
               } else if (var23.getNodeName().toLowerCase(Locale.ENGLISH).equals("takeascurrentcontext")) {
                  Boolean.parseBoolean(var23.getTextContent());
               } else {
                  String[] var24;
                  if (var23.getNodeName().toLowerCase(Locale.ENGLISH).equals("blockposa")) {
                     var24 = var23.getTextContent().split(",", 3);
                     var11 = new Vector3i(Integer.parseInt(var24[0].trim()), Integer.parseInt(var24[1].trim()), Integer.parseInt(var24[2].trim()));
                  } else if (var23.getNodeName().toLowerCase(Locale.ENGLISH).equals("blockposb")) {
                     var24 = var23.getTextContent().split(",", 3);
                     var12 = new Vector3i(Integer.parseInt(var24[0].trim()), Integer.parseInt(var24[1].trim()), Integer.parseInt(var24[2].trim()));
                  }
               }
            } catch (Exception var25) {
               throw new TutorialException("At " + var1.getNodeName(), var25);
            }
         }
      }

      if (var8 == null) {
         throw new TutorialException("At " + var1.getNodeName() + "; no <Content> tag found");
      } else if (var9 < 0) {
         throw new TutorialException("At " + var1.getNodeName() + "; no <Duration> tag found or duration negative");
      } else {
         if (var4.toLowerCase(Locale.ENGLISH).equals("placeblockonlastspawned")) {
            if (!ElementKeyMap.exists(var16)) {
               throw new TutorialException("At " + var1.getNodeName() + "; <typeA> must be specified and valid");
            }

            var5 = new PlaceElementOnLastSpawnedTestState(this.getObj(), var2, var8, var16, var11, var18, var21);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("openchest")) {
            if (var11 == null) {
               throw new TutorialException("At " + var1.getNodeName() + "; <blockPosA> must be specified and valid");
            }

            var5 = new OpenChestTestState(this.getObj(), var8, var2, var11);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("placeblock")) {
            if (!ElementKeyMap.exists(var16)) {
               throw new TutorialException("At " + var1.getNodeName() + "; <typeA> must be specified and valid");
            }

            var5 = new PlaceElementTestState(this.getObj(), var2, var8, var16, var11, var18, var21);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("putinchest")) {
            if (!ElementKeyMap.exists(var16)) {
               throw new TutorialException("At " + var1.getNodeName() + "; <typeA> must be specified and valid");
            }

            if (var11 == null) {
               throw new TutorialException("At " + var1.getNodeName() + "; <blockPosA> must be specified and valid");
            }

            var5 = new PutInChestTestState(this.getObj(), var2, var8, var16, var11, var18, var21);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("selectproductionon")) {
            if (!ElementKeyMap.exists(var16)) {
               throw new TutorialException("At " + var1.getNodeName() + "; <typeA> must be specified and valid");
            }

            if (var11 == null) {
               throw new TutorialException("At " + var1.getNodeName() + "; <blockPosA> must be specified and valid");
            }

            var5 = new SelectProductionTestState(this.getObj(), var8, var2, var11, var16);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("assignweaponslot")) {
            if (!ElementKeyMap.exists(var16)) {
               throw new TutorialException("At " + var1.getNodeName() + "; <typeA> must be specified and valid");
            }

            var5 = new AssignWeaponTestState(this.getObj(), var8, var2, var16, var17);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("weaponpanelopen")) {
            var5 = new WeaponPanelOpenTestState(this.getObj(), var8, var2);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("teleporttotutorialsector")) {
            if (var11 == null) {
               throw new TutorialException("At " + var1.getNodeName() + "; <blockPosA> must be specified and valid");
            }

            if (var13 == null) {
               throw new TutorialException("At " + var1.getNodeName() + "; <ShipUID> must be specified and valid");
            }

            var5 = new TeleportToTutorialSector(this.getObj(), var8, var2, var13, var11);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("teleportto")) {
            if (var11 == null) {
               throw new TutorialException("At " + var1.getNodeName() + "; <blockPosA> must be specified and valid");
            }

            if (var13 == null) {
               throw new TutorialException("At " + var1.getNodeName() + "; <ShipUID> must be specified and valid");
            }

            var5 = new TeleportToTestState(this.getObj(), var8, var2, var13, var11);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("destroyentity")) {
            if (var13 == null) {
               throw new TutorialException("At " + var1.getNodeName() + "; <ShipUID> must be specified and valid");
            }

            var5 = new DestroyEntityTestState(this.getObj(), var8, var2, var13);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("tutorialcompleted")) {
            var5 = new TutorialEndedTextState(this.getObj(), var2, var8, var9);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("typeininventory")) {
            if (!ElementKeyMap.exists(var16)) {
               throw new TutorialException("At " + var1.getNodeName() + "; <typeA> must be specified and valid");
            }

            var5 = new TypeInInventoryTestState(this.getObj(), var8, var2, var16, var21);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("typeinpersonalcapsulerefinery")) {
            if (!ElementKeyMap.exists(var16)) {
               throw new TutorialException("At " + var1.getNodeName() + "; <typeA> must be specified and valid");
            }

            var5 = new TypeInPersonalCapsuleRefineryTestState(this.getObj(), var8, var2, var16, var21);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("typeinpersonalfactoryassember")) {
            if (!ElementKeyMap.exists(var16)) {
               throw new TutorialException("At " + var1.getNodeName() + "; <typeA> must be specified and valid");
            }

            var5 = new TypeInPersonalFactoryAssemberTestState(this.getObj(), var8, var2, var16, var21);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("activategravity")) {
            if (var13 == null) {
               throw new TutorialException("At " + var1.getNodeName() + "; <ShipUID> must be specified and valid");
            }

            var5 = new ActivateGravityTestState(this.getObj(), var8, var2, var13);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("instantiateinventory")) {
            if (var15 == null) {
               throw new TutorialException("At " + var1.getNodeName() + "; <Clear> must be specified and valid");
            }

            var5 = new InstantiateInventoyTestState(this.getObj(), var8, var2, var15);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("restoryinventory")) {
            var5 = new RestoreInventoyTestState(this.getObj(), var8, var2);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("weaponpanelclosed")) {
            var5 = new WeaponPanelClosedTestState(this.getObj(), var8, var2);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("talktonpc")) {
            if (var14 == null) {
               throw new TutorialException("At " + var1.getNodeName() + "; <NpcName> must be specified and valid");
            }

            var5 = new TalkToNPCTestState(this.getObj(), var8, var2, var14);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("spawnship")) {
            var5 = new CreateShipTestState(this.getObj(), var8, var2, var18, false);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("getintolastspawnedship")) {
            var5 = new EnterLastSpawnedShipTestState(this.getObj(), var8, var2);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("connectblocks")) {
            if (var11 == null) {
               throw new TutorialException("At " + var1.getNodeName() + "; <blockPosA> must be specified and valid");
            }

            if (var12 == null) {
               throw new TutorialException("At " + var1.getNodeName() + "; <blockPosB> must be specified and valid");
            }

            var5 = new ConnectedFromToTestState(this.getObj(), var8, var2, var11, var12);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("activateflightmode")) {
            var5 = new ShipFlightControllerTestState(this.getObj(), var8, var2);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("activatebuildmode")) {
            var5 = new ShipBuildControllerTestState(this.getObj(), var8, var2);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("activateblock")) {
            if (var11 == null) {
               throw new TutorialException("At " + var1.getNodeName() + "; <blockPosA> must be specified and valid");
            }

            var5 = new ActivateBlockTestState(this.getObj(), var8, var2, var11, var19);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("getinstructureuid")) {
            if (var13 == null) {
               throw new TutorialException("At " + var1.getNodeName() + "; <ShipUID> must be specified and valid");
            }

            if (var11 == null) {
               throw new TutorialException("At " + var1.getNodeName() + "; <blockPosA> must be specified and valid");
            }

            var5 = new EnterUIDSegmentControllerTestState(this.getObj(), var8, var2, var13, var11);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("skip")) {
            var5 = new SkipTestState(this.getObj(), var2);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("waitingtext")) {
            var5 = new WaitingTextState(this.getObj(), var2, var8, var9);
         } else if (var4.toLowerCase(Locale.ENGLISH).equals("text")) {
            var5 = new TextState(this.getObj(), var2, var8, var9);
         }

         if (var5 == null) {
            throw new TutorialException("Unknown type: " + var4);
         } else {
            if (var10 != null && Controller.getResLoader().getSprite(var10) != null) {
               ((TextState)var5).setImage(Controller.getResLoader().getSprite(var10));
            }

            ((SatisfyingCondition)var5).setSkipWindowMessage(var20);
            return (State)var5;
         }
      }
   }

   private String formatText(String var1, GameClientState var2) {
      return KeyboardMappings.formatText(var1).replaceAll("\\$PLAYER", var2.getPlayerName());
   }

   public State getTutorialEndedState() {
      return this.tutorialEnded;
   }

   public State getTutorialEndState() {
      return this.tutorialEndState;
   }

   public State getTutorialStartState() {
      return this.tutorialStartState;
   }
}
