package org.schema.game.client.controller.tutorial.states;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.input.KeyboardMappings;

public class EnterLastSpawnedShipTestState extends SatisfyingCondition {
   public EnterLastSpawnedShipTestState(AiEntityStateInterface var1, String var2, GameClientState var3) {
      super(var1, var2, var3);
      this.skipIfSatisfiedAtEnter = true;
      this.setMarkers(new ObjectArrayList(1));
      this.getMarkers().add(new TutorialMarker(new Vector3i(16, 16, 16), "Press " + KeyboardMappings.ACTIVATE.getKeyChar() + " here to enter"));
   }

   protected boolean checkSatisfyingCondition() throws FSMException {
      if (this.getGameState().getController().getTutorialMode().lastSpawnedShip == null) {
         this.getEntityState().getMachine().getFsm().stateTransition(Transition.TUTORIAL_FAILED);
         return false;
      } else {
         ((TutorialMarker)this.getMarkers().get(0)).context = this.getGameState().getController().getTutorialMode().lastSpawnedShip;
         SegmentPiece var1;
         if ((var1 = this.getGameState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getEntered()) != null && var1.getSegment().getSegmentController() != null) {
            return var1.getSegment().getSegmentController() == this.getGameState().getController().getTutorialMode().lastSpawnedShip;
         } else {
            return false;
         }
      }
   }
}
