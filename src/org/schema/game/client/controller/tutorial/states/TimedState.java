package org.schema.game.client.controller.tutorial.states;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.ai.AiEntityStateInterface;

public abstract class TimedState extends SatisfyingCondition {
   protected long entered;

   public TimedState(AiEntityStateInterface var1, String var2, GameClientState var3) {
      super(var1, var2, var3);
   }

   protected boolean checkSatisfyingCondition() {
      if (this.getGameState().getController().getTutorialMode().isBackgroundMode()) {
         return !this.getGameState().getController().getTutorialMode().isSuspended() && System.currentTimeMillis() - this.entered > (long)this.getDurationInMs();
      } else {
         return this.next;
      }
   }

   public boolean onEnter() {
      this.entered = System.currentTimeMillis();
      return super.onEnter();
   }

   public abstract int getDurationInMs();
}
