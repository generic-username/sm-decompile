package org.schema.game.client.controller.tutorial.states;

import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;
import org.schema.schine.input.KeyboardMappings;

public class ConnectedFromToTestState extends SatisfyingCondition {
   private Vector3i connectFrom;
   private Vector3i connectTo;

   public ConnectedFromToTestState(AiEntityStateInterface var1, String var2, GameClientState var3, Vector3i var4, Vector3i var5) {
      super(var1, var2, var3);
      this.connectFrom = var4;
      this.connectTo = var5;
      this.skipIfSatisfiedAtEnter = true;
      this.setMarkers(new ObjectArrayList());
      this.getMarkers().add(new TutorialMarker(var4, "First, press '" + KeyboardMappings.SELECT_MODULE.getKeyChar() + "' here to select this block"));
   }

   protected boolean checkSatisfyingCondition() throws FSMException {
      return this.isConnected();
   }

   private boolean isConnected() throws FSMException {
      EditableSendableSegmentController var1 = this.getGameState().getController().getTutorialMode().currentContext;
      this.getMarkers().set(0, new TutorialMarker(this.connectFrom, "First, press '" + KeyboardMappings.SELECT_MODULE.getKeyChar() + "' here to select this block"));
      if (var1 != null) {
         SegmentPiece var2 = var1.getSegmentBuffer().getPointUnsave(this.connectFrom);
         SegmentPiece var3 = var1.getSegmentBuffer().getPointUnsave(this.connectTo);
         SegmentPiece var4 = this.getGameState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getPlayerCharacterManager().getSelectedBlock();
         if (var2 != null && var4 != null && !var2.getAbsolutePos(new Vector3i()).equals(var4.getAbsolutePos(new Vector3i()))) {
            this.getMarkers().set(0, new TutorialMarker(this.connectFrom, "First, press '" + KeyboardMappings.SELECT_MODULE.getKeyChar() + "' here to select this block"));
            return false;
         } else if (var3 == null || var2 == null || !ElementKeyMap.isValidType(var2.getType()) || !ElementKeyMap.isValidType(var3.getType()) || !ElementInformation.canBeControlled(var2.getType(), var3.getType()) && !ElementKeyMap.getInfo(var2.getType()).getControlling().contains(var3.getType())) {
            if (var3 != null && var2 != null) {
               if (ElementKeyMap.isValidType(var2.getType()) && ElementKeyMap.isValidType(var3.getType())) {
                  System.err.println("[ERROR] from to: " + ElementInformation.canBeControlled(var2.getType(), var3.getType()) + "; " + ElementKeyMap.getInfo(var2.getType()).getControlling().contains(var3.getType()));
               }

               System.err.println("[ERROR] Blocks to connect dont exist or are incompatible: " + var2 + " -> " + var3);
               this.getEntityState().getMachine().getFsm().stateTransition(Transition.TUTORIAL_FAILED);
               return false;
            } else {
               return false;
            }
         } else {
            short var6 = var3.getType();
            LongOpenHashSet var5 = (LongOpenHashSet)var1.getControlElementMap().getControllingMap().getAll().get(ElementCollection.getIndex(this.connectFrom));
            if ((var3 = this.getGameState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedBlockByActiveController()) != null && var3.equalsPos(this.connectFrom)) {
               this.getMarkers().set(0, new TutorialMarker(this.connectTo, "Then, press '" + KeyboardMappings.CONNECT_MODULE.getKeyChar() + "' here to connect\nthis block to the one with the orange wobble"));
            }

            return var5 != null && var5.contains(ElementCollection.getIndex4((short)this.connectTo.x, (short)this.connectTo.y, (short)this.connectTo.z, var6));
         }
      } else {
         this.getEntityState().getMachine().getFsm().stateTransition(Transition.TUTORIAL_FAILED);
         return false;
      }
   }

   public boolean checkConnectBlock(SegmentPiece var1, GameClientState var2) {
      if (var1 != null && var1.equalsPos(this.connectFrom)) {
         return true;
      } else {
         var2.getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_CONNECTEDFROMTOTESTSTATE_0, 0.0F);
         return false;
      }
   }

   public boolean checkConnectToBlock(Vector3i var1, GameClientState var2) {
      if (var1 != null && var1.equals(this.connectTo)) {
         return true;
      } else {
         var2.getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_CONNECTEDFROMTOTESTSTATE_1, 0.0F);
         return false;
      }
   }
}
