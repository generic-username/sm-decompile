package org.schema.game.client.controller.tutorial.states;

import com.bulletphysics.linearmath.Transform;
import java.util.Iterator;
import java.util.Map.Entry;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;

public class TeleportToTutorialSector extends SatisfyingCondition {
   private String uidStartsWith;
   private Vector3i pos;

   public TeleportToTutorialSector(AiEntityStateInterface var1, String var2, GameClientState var3, String var4, Vector3i var5) {
      super(var1, var2, var3);
      this.skipIfSatisfiedAtEnter = true;
      this.endIfSatisfied = true;
      this.uidStartsWith = var4;
      this.pos = var5;
   }

   protected boolean checkSatisfyingCondition() throws FSMException {
      if (!this.getGameState().getPlayer().isInTutorial()) {
         System.err.println("[TUT] toTutorialSector: not yet in tutorial sector");
         return false;
      } else {
         synchronized(this.getGameState().getLocalAndRemoteObjectContainer().getLocalObjects()) {
            Iterator var2 = this.getGameState().getLocalAndRemoteObjectContainer().getUidObjectMap().entrySet().iterator();

            while(var2.hasNext()) {
               Entry var3;
               if (((String)(var3 = (Entry)var2.next()).getKey()).startsWith(this.uidStartsWith)) {
                  if (var3.getValue() instanceof SegmentController && ((SegmentController)var3.getValue()).getSectorId() == this.getGameState().getPlayer().getCurrentSectorId()) {
                     SegmentController var5;
                     if ((var5 = (SegmentController)var3.getValue()).getSegmentBuffer().getPointUnsave(new Vector3i(16, 16, 16)) != null) {
                        Transform var6;
                        (var6 = new Transform()).setIdentity();
                        var5.getAbsoluteElementWorldPositionShifted(this.pos, var6.origin);
                        this.getGameState().getPlayer().getAssingedPlayerCharacter().getGhostObject().setWorldTransform(var6);
                        return true;
                     }

                     return false;
                  }

                  return false;
               }

               System.err.println("[TUT] toTutorialSector: " + this.uidStartsWith + " NOT MATCHED: " + var3.getValue());
            }

            return false;
         }
      }
   }

   public boolean onEnter() {
      if (this.getGameState().getPlayer().getFirstControlledTransformableWOExc() != null && this.getGameState().getPlayer().getFirstControlledTransformableWOExc() instanceof PlayerCharacter) {
         this.getGameState().getPlayer().warpToTutorialSectorClient();
         return super.onEnter();
      } else {
         try {
            this.getGameState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_TELEPORTTOTUTORIALSECTOR_0, 0.0F);
            this.getEntityState().getMachine().getFsm().stateTransition(Transition.TUTORIAL_FAILED);
         } catch (FSMException var1) {
            var1.printStackTrace();
         }

         return false;
      }
   }
}
