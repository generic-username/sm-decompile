package org.schema.game.client.controller.tutorial.states;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.InventoryControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;
import org.schema.schine.input.KeyboardMappings;

public class OpenChestTestState extends SatisfyingCondition {
   private Vector3i pos;
   private SegmentPiece cc;

   public OpenChestTestState(AiEntityStateInterface var1, String var2, GameClientState var3, Vector3i var4) {
      super(var1, var2, var3);
      this.pos = var4;
      this.skipIfSatisfiedAtEnter = true;
      this.setMarkers(new ObjectArrayList());
      this.getMarkers().add(new TutorialMarker(var4, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_OPENCHESTTESTSTATE_0, KeyboardMappings.ACTIVATE.getKeyChar())));
   }

   protected boolean checkSatisfyingCondition() throws FSMException {
      EditableSendableSegmentController var1;
      if ((var1 = this.getGameState().getController().getTutorialMode().currentContext) != null) {
         if (this.cc != null) {
            this.cc.refresh();
            InventoryControllerManager var2;
            if ((var2 = this.getGameState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager()).isTreeActive() && var2.getSecondInventory() != null && var2.getSecondInventory().getParameter() == ElementCollection.getIndex(this.pos)) {
               return true;
            }

            return false;
         }

         this.cc = var1.getSegmentBuffer().getPointUnsave(this.pos);
         if (this.cc != null && (!ElementKeyMap.isValidType(this.cc.getType()) || !ElementKeyMap.getFactorykeyset().contains(this.cc.getType()) && this.cc.getType() == 120)) {
            System.err.println("[ERROR] Block invalid or cannot be opened: " + this.cc + "!");
            this.getEntityState().getMachine().getFsm().stateTransition(Transition.TUTORIAL_FAILED);
            return false;
         }
      } else {
         System.err.println("[TUTORIAL] ACTIVATE BLOCK HAS NO SegmentController CONTEXT: " + this.pos);
         this.getEntityState().getMachine().getFsm().stateTransition(Transition.TUTORIAL_FAILED);
      }

      return false;
   }
}
