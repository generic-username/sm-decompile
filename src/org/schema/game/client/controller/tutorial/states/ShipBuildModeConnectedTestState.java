package org.schema.game.client.controller.tutorial.states;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class ShipBuildModeConnectedTestState extends SatisfyingCondition {
   private short clazz;

   public ShipBuildModeConnectedTestState(AiEntityStateInterface var1, String var2, GameClientState var3, short var4) {
      super(var1, var2, var3);
      this.clazz = var4;
      this.skipIfSatisfiedAtEnter = true;
   }

   protected boolean checkSatisfyingCondition() throws FSMException {
      EditableSendableSegmentController var1;
      if ((var1 = this.getGameState().getController().getTutorialMode().currentContext) != null) {
         return !var1.getControlElementMap().getAllControlledElements(this.clazz).isEmpty();
      } else {
         this.getEntityState().getMachine().getFsm().stateTransition(Transition.TUTORIAL_NO_SHIP_CREATED);
         return false;
      }
   }
}
