package org.schema.game.client.controller.tutorial.states;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.ai.AiEntityStateInterface;

public class WaitingTextState extends TextState {
   private boolean lastNext;

   public WaitingTextState(AiEntityStateInterface var1, GameClientState var2, String var3, int var4) {
      super(var1, var2, var3, var4);
   }

   protected boolean checkSatisfyingCondition() {
      if (!this.getGameState().getController().getTutorialMode().isBackgroundMode()) {
         if (this.lastNext != this.next && this.next) {
            this.entered = System.currentTimeMillis();
         }

         this.lastNext = this.next;
      }

      if (!this.next && !this.getGameState().getController().getTutorialMode().isBackgroundMode()) {
         return false;
      } else {
         return !this.getGameState().getController().getTutorialMode().isSuspended() && System.currentTimeMillis() - this.entered > (long)this.getDurationInMs();
      }
   }

   public boolean onEnter() {
      this.lastNext = false;
      return super.onEnter();
   }

   public String getMessage() {
      return this.next && !this.getGameState().getController().getTutorialMode().isBackgroundMode() ? super.getMessage() + "(Tutorial will resume in " + ((long)this.getDurationInMs() - (System.currentTimeMillis() - this.entered)) / 1000L + " sec\nor press 'u')" : super.getMessage();
   }
}
