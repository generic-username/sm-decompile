package org.schema.game.client.controller.tutorial.states;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.SimplePlayerCommands;
import org.schema.schine.ai.AiEntityStateInterface;

public class InstantiateInventoyTestState extends TimedState {
   private boolean clear;

   public InstantiateInventoyTestState(AiEntityStateInterface var1, String var2, GameClientState var3, boolean var4) {
      super(var1, var2, var3);
      this.clear = var4;
   }

   public boolean onEnter() {
      this.getGameState().getPlayer().sendSimpleCommand(SimplePlayerCommands.BACKUP_INVENTORY, this.clear);
      return super.onEnter();
   }

   public int getDurationInMs() {
      return 5000;
   }
}
