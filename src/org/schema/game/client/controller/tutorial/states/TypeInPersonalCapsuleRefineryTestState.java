package org.schema.game.client.controller.tutorial.states;

import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.inventory.PersonalFactoryInventory;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.common.language.Lng;
import org.schema.schine.input.KeyboardMappings;

public class TypeInPersonalCapsuleRefineryTestState extends SatisfyingCondition {
   private short type;
   private int count;

   public TypeInPersonalCapsuleRefineryTestState(AiEntityStateInterface var1, String var2, GameClientState var3, short var4, int var5) {
      super(var1, var2, var3);
      this.skipIfSatisfiedAtEnter = true;
      this.type = var4;
      this.count = var5;
   }

   protected boolean checkSatisfyingCondition() {
      return this.getGameState().getPlayer().getPersonalFactoryInventoryCapsule().getOverallQuantity(this.type) >= this.count;
   }

   public boolean onEnter() {
      this.setMessage("Open up your inventory (" + KeyboardMappings.INVENTORY_PANEL.getKeyChar() + ")");
      this.getGameState().getController().getTutorialMode().highlightType = this.type;
      return super.onEnter();
   }

   public boolean onUpdate() throws FSMException {
      PersonalFactoryInventory var1;
      int var2;
      if ((var2 = (var1 = this.getGameState().getPlayer().getPersonalFactoryInventoryCapsule()).getOverallQuantity(this.type)) < this.count && this.getGameState().getPlayer().getInventory().getOverallQuantity(this.type) < this.count - var2) {
         int var3 = this.getGameState().getPlayer().getInventory().incExistingOrNextFreeSlot(this.type, this.count - var2);
         this.getGameState().getPlayer().getInventory().sendInventoryModification(var3);
      }

      String var7 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_TYPEINPERSONALCAPSULEREFINERYTESTSTATE_0;
      if (!this.getGameState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager().isTreeActive()) {
         this.setMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_TYPEINPERSONALCAPSULEREFINERYTESTSTATE_1, KeyboardMappings.INVENTORY_PANEL.getKeyChar()));
         return super.onUpdate();
      } else if (!this.getGameState().getWorldDrawer().getGuiDrawer().getPlayerPanel().getInventoryPanel().isCapsuleRefineryOpen()) {
         this.setMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_TYPEINPERSONALCAPSULEREFINERYTESTSTATE_2, var7));
         return super.onUpdate();
      } else {
         Iterator var4 = var1.getSlots().iterator();

         boolean var6;
         short var8;
         do {
            if (!var4.hasNext()) {
               this.setMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_TYPEINPERSONALCAPSULEREFINERYTESTSTATE_5, this.count - var2, ElementKeyMap.getInfo(this.type)));
               return super.onUpdate();
            }

            int var5 = (Integer)var4.next();
            var8 = var1.getType(var5);
            var6 = ElementKeyMap.isShard(this.type) && var8 == 220 || ElementKeyMap.isOre(this.type) && var8 == 440;
         } while(var8 == 0 || var8 == this.type || var6);

         if (var8 > 0) {
            this.setMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_TYPEINPERSONALCAPSULEREFINERYTESTSTATE_3, ElementKeyMap.getInfo(var8), var7));
         } else {
            this.setMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_TYPEINPERSONALCAPSULEREFINERYTESTSTATE_4, var7));
         }

         return super.onUpdate();
      }
   }
}
