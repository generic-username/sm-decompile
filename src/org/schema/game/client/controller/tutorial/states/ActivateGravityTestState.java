package org.schema.game.client.controller.tutorial.states;

import java.util.Iterator;
import java.util.Map.Entry;
import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;

public class ActivateGravityTestState extends SatisfyingCondition {
   private String uidStartsWith;

   public ActivateGravityTestState(AiEntityStateInterface var1, String var2, GameClientState var3, String var4) {
      super(var1, var2, var3);
      this.uidStartsWith = var4;
   }

   protected boolean checkSatisfyingCondition() throws FSMException {
      synchronized(this.getGameState().getLocalAndRemoteObjectContainer().getLocalObjects()){}

      Throwable var10000;
      label252: {
         boolean var10001;
         Iterator var2;
         try {
            var2 = this.getGameState().getLocalAndRemoteObjectContainer().getUidObjectMap().entrySet().iterator();
         } catch (Throwable var23) {
            var10000 = var23;
            var10001 = false;
            break label252;
         }

         while(true) {
            Entry var3;
            try {
               if (!var2.hasNext()) {
                  return false;
               }

               if (!((String)(var3 = (Entry)var2.next()).getKey()).startsWith(this.uidStartsWith)) {
                  continue;
               }

               if (this.getGameState().getPlayer().getAssingedPlayerCharacter().getGravity().source == var3.getValue()) {
                  return true;
               }
            } catch (Throwable var22) {
               var10000 = var22;
               var10001 = false;
               break;
            }

            try {
               if (var3.getValue() instanceof SegmentController && ((SegmentController)var3.getValue()).getSectorId() == this.getGameState().getPlayer().getCurrentSectorId()) {
                  SegmentController var25 = (SegmentController)var3.getValue();
                  this.getGameState().getPlayer().getAssingedPlayerCharacter().scheduleGravityWithBlockBelow(new Vector3f(0.0F, -9.89F, 0.0F), var25);
                  return super.onEnter();
               }
            } catch (Throwable var21) {
               var10000 = var21;
               var10001 = false;
               break;
            }

            try {
               if (this.getGameState().getPlayer().getAssingedPlayerCharacter().getGravity().source == var3.getValue()) {
                  return true;
               }
            } catch (Throwable var20) {
               var10000 = var20;
               var10001 = false;
               break;
            }
         }
      }

      Throwable var24 = var10000;
      throw var24;
   }

   public boolean onEnter() {
      return super.onEnter();
   }
}
