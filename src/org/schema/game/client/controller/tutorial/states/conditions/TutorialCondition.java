package org.schema.game.client.controller.tutorial.states.conditions;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public abstract class TutorialCondition {
   static int i;
   private State establishing;
   private State toObserve;
   private int id;

   public TutorialCondition(State var1, State var2) {
      this.establishing = var2;
      this.toObserve = var1;
      var1.addTransition(this.getTransition(), var2);
      this.id = i++;
   }

   protected abstract Transition getTransition();

   public abstract boolean isSatisfied(GameClientState var1);

   public abstract String getNotSactifiedText();

   public boolean checkAndStateTransitionIfMissed(GameClientState var1) throws FSMException {
      if (!this.isSatisfied(var1)) {
         this.toObserve.stateTransition(this.getTransition());
         return false;
      } else {
         return true;
      }
   }

   public State getEstablishingState() {
      return this.establishing;
   }

   public int hashCode() {
      return this.id;
   }

   public boolean equals(Object var1) {
      if (var1 instanceof TutorialCondition) {
         return this.id == ((TutorialCondition)var1).id;
      } else {
         return false;
      }
   }
}
