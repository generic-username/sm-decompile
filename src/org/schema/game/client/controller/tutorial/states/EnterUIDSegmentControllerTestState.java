package org.schema.game.client.controller.tutorial.states;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.Map.Entry;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;
import org.schema.schine.input.KeyboardMappings;

public class EnterUIDSegmentControllerTestState extends SatisfyingCondition {
   private String uidStartsWith;

   public EnterUIDSegmentControllerTestState(AiEntityStateInterface var1, String var2, GameClientState var3, String var4, Vector3i var5) {
      super(var1, var2, var3);
      this.skipIfSatisfiedAtEnter = true;
      this.uidStartsWith = var4;
      this.setMarkers(new ObjectArrayList(1));
      this.getMarkers().add(new TutorialMarker(var5, "Press " + KeyboardMappings.ACTIVATE.getKeyChar() + " here to enter"));
   }

   protected boolean checkSatisfyingCondition() throws FSMException {
      synchronized(this.getGameState().getLocalAndRemoteObjectContainer().getLocalObjects()){}

      Throwable var10000;
      label229: {
         boolean var10001;
         Iterator var2;
         try {
            var2 = this.getGameState().getLocalAndRemoteObjectContainer().getUidObjectMap().entrySet().iterator();
         } catch (Throwable var14) {
            var10000 = var14;
            var10001 = false;
            break label229;
         }

         Entry var3;
         while(true) {
            try {
               if (var2.hasNext()) {
                  if (!((String)(var3 = (Entry)var2.next()).getKey()).startsWith(this.uidStartsWith)) {
                     continue;
                  }

                  if (!(var3.getValue() instanceof SegmentController) || ((SegmentController)var3.getValue()).getSectorId() != this.getGameState().getPlayer().getCurrentSectorId()) {
                     break;
                  }

                  ((TutorialMarker)this.getMarkers().get(0)).context = (SegmentController)var3.getValue();
                  SegmentPiece var16;
                  if ((var16 = this.getGameState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getEntered()) != null && var16.getSegment().getSegmentController() != null) {
                     return var16.getSegment().getSegmentController().getUniqueIdentifier().startsWith(this.uidStartsWith);
                  }

                  return false;
               }
            } catch (Throwable var15) {
               var10000 = var15;
               var10001 = false;
               break label229;
            }

            this.getGameState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_ENTERUIDSEGMENTCONTROLLERTESTSTATE_1, this.uidStartsWith), 0.0F);
            this.stateTransition(Transition.TUTORIAL_FAILED);
            return false;
         }

         label193:
         try {
            this.getGameState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_ENTERUIDSEGMENTCONTROLLERTESTSTATE_0, var3.getValue()), 0.0F);
            this.stateTransition(Transition.TUTORIAL_FAILED);
            return false;
         } catch (Throwable var13) {
            var10000 = var13;
            var10001 = false;
            break label193;
         }
      }

      Throwable var17 = var10000;
      throw var17;
   }
}
