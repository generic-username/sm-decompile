package org.schema.game.client.controller.tutorial.states;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.input.KeyboardMappings;

public class ActivateBlockTestState extends SatisfyingCondition {
   private Boolean active;
   private Vector3i pos;
   private SegmentPiece cc;
   private boolean before;
   private SimpleTransformableSendableObject gravSource;

   public ActivateBlockTestState(AiEntityStateInterface var1, String var2, GameClientState var3, Vector3i var4, Boolean var5) {
      super(var1, var2, var3);
      this.pos = var4;
      this.skipIfSatisfiedAtEnter = true;
      this.active = var5;
      this.setMarkers(new ObjectArrayList());
      if (var5 == null) {
         this.getMarkers().add(new TutorialMarker(var4, "Press " + KeyboardMappings.ACTIVATE.getKeyChar() + " here to activate/decativate this block"));
      } else {
         this.getMarkers().add(new TutorialMarker(var4, "Press " + KeyboardMappings.ACTIVATE.getKeyChar() + " here to " + (var5 ? "activate" : "deactivate") + " this block"));
      }
   }

   protected boolean checkSatisfyingCondition() throws FSMException {
      EditableSendableSegmentController var1;
      if ((var1 = this.getGameState().getController().getTutorialMode().currentContext) != null) {
         System.err.println("CHECK " + this.cc + "; " + this.active);
         if (this.cc != null) {
            this.cc.refresh();
            if (this.cc.getType() == 56) {
               if (this.active == null) {
                  if (this.gravSource != this.getGameState().getCharacter().getGravity().source) {
                     return true;
                  }

                  return false;
               }

               if (this.active) {
                  if (this.getGameState().getCharacter().getGravity().source != null && !this.getGameState().getCharacter().getGravity().getAcceleration().equals(new Vector3f(0.0F, 0.0F, 0.0F))) {
                     return true;
                  }

                  return false;
               }

               return this.getGameState().getCharacter().getGravity().getAcceleration().equals(new Vector3f(0.0F, 0.0F, 0.0F));
            }

            if (this.active == null) {
               if (this.cc.isActive() != this.before) {
                  return true;
               }

               return false;
            }

            if (this.cc.isActive() == this.active) {
               return true;
            }

            return false;
         }

         this.cc = var1.getSegmentBuffer().getPointUnsave(this.pos);
         if (this.cc != null) {
            if (!ElementKeyMap.isValidType(this.cc.getType()) || !ElementKeyMap.getInfo(this.cc.getType()).canActivate()) {
               System.err.println("[ERROR] Block invalid or cannot be activated: " + this.cc + "!");
               this.getEntityState().getMachine().getFsm().stateTransition(Transition.RESTART);
               return false;
            }

            this.before = this.cc.isActive();
            this.gravSource = this.getGameState().getCharacter().getGravity().source;
         }
      } else {
         System.err.println("[TUTORIAL] ACTIVATE BLOCK HAS NO SegmentController CONTEXT: " + this.pos);
      }

      return false;
   }
}
