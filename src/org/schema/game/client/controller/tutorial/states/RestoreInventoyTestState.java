package org.schema.game.client.controller.tutorial.states;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.SimplePlayerCommands;
import org.schema.schine.ai.AiEntityStateInterface;

public class RestoreInventoyTestState extends TimedState {
   public RestoreInventoyTestState(AiEntityStateInterface var1, String var2, GameClientState var3) {
      super(var1, var2, var3);
   }

   public boolean onEnter() {
      this.getGameState().getPlayer().sendSimpleCommand(SimplePlayerCommands.RESTORE_INVENTORY);
      return super.onEnter();
   }

   public int getDurationInMs() {
      return 5000;
   }
}
