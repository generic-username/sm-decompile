package org.schema.game.client.controller.tutorial.states;

import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.schine.ai.AiEntityStateInterface;

public class InventoryClosedTestState extends SatisfyingCondition {
   public InventoryClosedTestState(AiEntityStateInterface var1, String var2, GameClientState var3) {
      super(var1, var2, var3);
      this.skipIfSatisfiedAtEnter = true;
   }

   protected boolean checkSatisfyingCondition() {
      return !this.getGameState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager().isActive();
   }

   public boolean onEnter() {
      PlayerGameControlManager var1;
      if (!(var1 = this.getGameState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager()).getInventoryControlManager().isActive()) {
         var1.inventoryAction((Inventory)null);
      }

      return super.onEnter();
   }
}
