package org.schema.game.client.controller.tutorial.states;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;

public class TutorialFailedState extends State {
   private GameClientState gameState;

   public TutorialFailedState(AiEntityStateInterface var1, GameClientState var2) {
      super(var1);
      this.gameState = var2;
   }

   public boolean onEnter() {
      this.gameState.getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_TUTORIALFAILEDSTATE_0, 0.0F);
      return true;
   }

   public boolean onExit() {
      return true;
   }

   public boolean onUpdate() throws FSMException {
      this.stateTransition(Transition.TUTORIAL_FAILED);
      return true;
   }
}
