package org.schema.game.client.controller.tutorial.states;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.ai.AiEntityStateInterface;

public class EntitySelectedTestState extends SatisfyingCondition {
   private Class clazz;

   public EntitySelectedTestState(AiEntityStateInterface var1, String var2, GameClientState var3, Class var4) {
      super(var1, var2, var3);
      this.clazz = var4;
      this.skipIfSatisfiedAtEnter = true;
   }

   protected boolean checkSatisfyingCondition() {
      SimpleTransformableSendableObject var1;
      return (var1 = this.getGameState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity()) != null && this.clazz.isInstance(var1);
   }
}
