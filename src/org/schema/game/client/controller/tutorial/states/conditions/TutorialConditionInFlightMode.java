package org.schema.game.client.controller.tutorial.states.conditions;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public class TutorialConditionInFlightMode extends TutorialCondition {
   public TutorialConditionInFlightMode(State var1, State var2) {
      super(var1, var2);
   }

   public boolean isSatisfied(GameClientState var1) {
      return var1.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController().isActive();
   }

   public String getNotSactifiedText() {
      return "Tutorial Condition failed\nExited flight Mode";
   }

   protected Transition getTransition() {
      return Transition.TUTORIAL_CONDITION_IN_FLIGHT_MODE;
   }
}
