package org.schema.game.client.controller.tutorial.states;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.ai.AiEntityStateInterface;

public class ShipBuildControllerTestState extends SatisfyingCondition {
   public ShipBuildControllerTestState(AiEntityStateInterface var1, String var2, GameClientState var3) {
      super(var1, var2, var3);
      this.skipIfSatisfiedAtEnter = true;
   }

   protected boolean checkSatisfyingCondition() {
      return this.getGameState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().isActive();
   }
}
