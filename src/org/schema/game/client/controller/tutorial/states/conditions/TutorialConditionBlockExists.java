package org.schema.game.client.controller.tutorial.states.conditions;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public class TutorialConditionBlockExists extends TutorialCondition {
   private Vector3i pos;

   public TutorialConditionBlockExists(State var1, State var2, Vector3i var3) {
      super(var1, var2);
      this.pos = var3;
   }

   public boolean isSatisfied(GameClientState var1) {
      if (var1.getController().getTutorialMode().currentContext == null) {
         System.err.println("[TUTORIALCONDITION] failed: no context: " + this);
         return false;
      } else {
         SegmentPiece var2;
         return (var2 = var1.getController().getTutorialMode().currentContext.getSegmentBuffer().getPointUnsave(this.pos)) != null ? ElementKeyMap.isValidType(var2.getType()) : true;
      }
   }

   public String getNotSactifiedText() {
      return "CRITICAL: block removed";
   }

   protected Transition getTransition() {
      return Transition.TUTORIAL_CONDITION_BLOCK_EXISTS;
   }
}
