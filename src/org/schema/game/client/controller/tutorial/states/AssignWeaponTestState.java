package org.schema.game.client.controller.tutorial.states;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SlotAssignment;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class AssignWeaponTestState extends SatisfyingCondition {
   private short type;
   private int slot;

   public AssignWeaponTestState(AiEntityStateInterface var1, String var2, GameClientState var3, short var4, int var5) {
      super(var1, var2, var3);
      this.skipIfSatisfiedAtEnter = true;
      this.slot = var5 - 1;
      this.type = var4;
      if (var5 == 0) {
         this.slot = 10;
      }

   }

   protected boolean checkSatisfyingCondition() throws FSMException {
      if (this.getGameState().getController().getTutorialMode().lastSpawnedShip == null) {
         this.getEntityState().getMachine().getFsm().stateTransition(Transition.TUTORIAL_FAILED);
         return false;
      } else {
         SlotAssignment var1;
         Vector3i var2;
         if ((var1 = this.getGameState().getController().getTutorialMode().lastSpawnedShip.getSlotAssignment()) != null && (var2 = var1.get(this.slot)) != null) {
            SegmentPiece var3;
            return (var3 = this.getGameState().getController().getTutorialMode().lastSpawnedShip.getSegmentBuffer().getPointUnsave(var2)) != null && var3.getType() == this.type;
         } else {
            return false;
         }
      }
   }
}
