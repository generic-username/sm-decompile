package org.schema.game.client.controller.tutorial.states;

import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;

public class TutorialMarker {
   public final Vector3i where;
   public final String markerText;
   public SegmentController context;
   public Vector3f absolute;

   public TutorialMarker(Vector3i var1, String var2) {
      this.where = var1;
      this.markerText = var2;
   }
}
