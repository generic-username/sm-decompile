package org.schema.game.client.controller.tutorial.states;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.ai.AiEntityStateInterface;

public class SkipTestState extends SatisfyingCondition {
   public SkipTestState(AiEntityStateInterface var1, GameClientState var2) {
      super(var1, "", var2);
      this.skipIfSatisfiedAtEnter = true;
   }

   protected boolean checkSatisfyingCondition() {
      return true;
   }
}
