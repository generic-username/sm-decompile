package org.schema.game.client.controller.tutorial.states.conditions;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public class TutorialConditionInLastShip extends TutorialCondition {
   public TutorialConditionInLastShip(State var1, State var2) {
      super(var1, var2);
   }

   public boolean isSatisfied(GameClientState var1) {
      SegmentPiece var2 = var1.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getEntered();
      if (var1.getController().getTutorialMode() != null && var1.getController().getTutorialMode().lastSpawnedShip != null) {
         if (var2 != null && var2.getSegment().getSegmentController() != null) {
            return var2.getSegment().getSegmentController() == var1.getController().getTutorialMode().lastSpawnedShip;
         } else {
            return false;
         }
      } else {
         return false;
      }
   }

   public String getNotSactifiedText() {
      return "CRITICAL: not in ship";
   }

   protected Transition getTransition() {
      return Transition.TUTORIAL_CONDITION_IN_LAST_SHIP;
   }
}
