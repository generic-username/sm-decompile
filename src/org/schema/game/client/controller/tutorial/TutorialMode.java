package org.schema.game.client.controller.tutorial;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import org.schema.game.client.controller.tutorial.states.SatisfyingCondition;
import org.schema.game.client.controller.tutorial.states.TextState;
import org.schema.game.client.controller.tutorial.states.TutorialEnded;
import org.schema.game.client.controller.tutorial.states.TutorialEndedTextState;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.schine.ai.MachineProgram;
import org.schema.schine.ai.stateMachines.AIConfiguationElementsInterface;
import org.schema.schine.ai.stateMachines.AiEntityState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.resource.FileExt;

public class TutorialMode extends MachineProgram {
   public static final String BASIC_TUTORIAL = "Basics Tutorial";
   public static final Object2ObjectOpenHashMap translation = new Object2ObjectOpenHashMap();
   public EditableSendableSegmentController currentContext;
   public Ship lastSpawnedShip;
   public List markers;
   public short highlightType;
   private boolean backgroundMode = false;

   public TutorialMode(AiEntityState var1) {
      super(var1, true);
      this.suspend(false);
   }

   public void onAISettingChanged(AIConfiguationElementsInterface var1) {
   }

   protected String getStartMachine() {
      return "Basics Tutorial";
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   protected void initializeMachines(HashMap var1) {
      var1.put("Basics Tutorial", new DynamicTutorialStateMachine(this.getEntityState(), this, (new FileExt("./data/tutorial/TutorialStartUp/TutorialStartUp.xml")).getAbsolutePath(), new FileExt("./data/tutorial/TutorialStartUp/")));
      File[] var2;
      int var3 = (var2 = (new FileExt("./data/tutorial/")).listFiles()).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         File var5;
         if ((var5 = var2[var4]).isDirectory()) {
            File[] var6;
            int var7 = (var6 = var5.listFiles()).length;

            for(int var8 = 0; var8 < var7; ++var8) {
               File var9;
               if ((var9 = var6[var8]).getName().endsWith(".xml")) {
                  try {
                     var1.put(var5.getName(), new DynamicTutorialStateMachine(this.getEntityState(), this, var9.getAbsolutePath(), var5));
                  } catch (TutorialException var10) {
                     System.err.println("[TUTORIAL] TUTORIAL EXCEPTION IN " + var5.getName());
                     var10.printStackTrace();
                     throw new RuntimeException(var10);
                  }
               }
            }
         }
      }

   }

   public boolean hasBack() {
      if (this.getMachine().getFsm().getCurrentState().containsTransition(Transition.BACK)) {
         try {
            State var1;
            if ((var1 = this.getMachine().getFsm().getCurrentState().getStateData().getOutput(Transition.BACK)) != this.getMachine().getFsm().getCurrentState() && var1 instanceof TextState) {
               return true;
            }

            return false;
         } catch (FSMException var2) {
            var2.printStackTrace();
         }
      }

      return false;
   }

   public void back() throws FSMException {
      this.getMachine().getFsm().stateTransition(Transition.BACK);
   }

   public void end() {
      try {
         this.getMachine().getFsm().stateTransition(Transition.TUTORIAL_END);
      } catch (FSMException var1) {
         var1.printStackTrace();
      }
   }

   public void endNow() {
      try {
         this.getMachine().getFsm().stateTransition(Transition.TUTORIAL_END);
         this.getMachine().getFsm().stateTransition(Transition.CONDITION_SATISFIED);
      } catch (FSMException var1) {
         var1.printStackTrace();
      }
   }

   public void endStep() {
      try {
         this.getMachine().getFsm().stateTransition(Transition.TUTORIAL_SKIP_PART);
      } catch (FSMException var1) {
         var1.printStackTrace();
      }
   }

   public boolean isBackgroundMode() {
      return this.backgroundMode;
   }

   public void setBackgroundMode(boolean var1) {
      this.backgroundMode = var1;
   }

   public void repeat() {
      try {
         this.getMachine().getFsm().stateTransition(Transition.TUTORIAL_RESTART);
      } catch (FSMException var1) {
         var1.printStackTrace();
      }
   }

   public void repeatStep() {
      try {
         this.getMachine().getFsm().stateTransition(Transition.TUTORIAL_RESET_PART);
      } catch (FSMException var1) {
         var1.printStackTrace();
      }
   }

   public void shopDistanceChanged(boolean var1) {
      if (!(this.getMachine().getFsm().getCurrentState() instanceof TutorialEnded) && !var1) {
         try {
            this.getMachine().getFsm().stateTransition(Transition.TUTORIAL_SHOP_DISTANCE_LOST);
            return;
         } catch (FSMException var2) {
         }
      }

   }

   public void skip() {
      if (this.getMachine().getFsm().getCurrentState() instanceof SatisfyingCondition) {
         ((SatisfyingCondition)this.getMachine().getFsm().getCurrentState()).forceSatisfied();
      }

   }

   public boolean isEndState() {
      return this.getMachine().getFsm().getCurrentState() instanceof TutorialEnded || this.getMachine().getFsm().getCurrentState() instanceof TutorialEndedTextState;
   }
}
