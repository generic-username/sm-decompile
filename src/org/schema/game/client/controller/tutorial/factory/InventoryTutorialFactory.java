package org.schema.game.client.controller.tutorial.factory;

import org.schema.game.client.controller.tutorial.states.InventoryClosedTestState;
import org.schema.game.client.controller.tutorial.states.InventoryOpenTestState;
import org.schema.game.client.controller.tutorial.states.TextState;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.input.KeyboardMappings;

public class InventoryTutorialFactory extends AbstractFSMFactory {
   public InventoryTutorialFactory(State var1, State var2, State var3, GameClientState var4) {
      super(var1, var2, var3, var4);
   }

   protected State create(State var1) {
      GameClientState var2 = (GameClientState)this.state;
      InventoryOpenTestState var3 = new InventoryOpenTestState(this.getObj(), "Press '" + KeyboardMappings.INVENTORY_PANEL.getKeyChar() + "' to enter \nyour inventory.", var2);
      TextState var4 = new TextState(this.getObj(), var2, "Good!\nHere you see all the building\nmodules you currently have.\n", 8000);
      TextState var5 = new TextState(this.getObj(), var2, "You can drag and stack item\n", 8000);
      TextState var6 = new TextState(this.getObj(), var2, "To take a custom amount of an item,\nright-click on it.", 12000);
      TextState var7 = new TextState(this.getObj(), var2, "You can also drag them in the\naction bar at the bottom to assign them\nto the respective number on your keyboard.", 12000);
      this.transition(var1, var3);
      this.transition(var3, var4);
      this.transition(var4, var5);
      this.transition(var5, var6);
      this.transition(var6, var7);
      return var7;
   }

   public void defineStartAndEnd() {
      GameClientState var1 = (GameClientState)this.state;
      this.setStartState(new TextState(this.getObj(), var1, "Let's go on to learn about the inventory", 8000));
      this.setEndState(new InventoryClosedTestState(this.getObj(), "when you are done, press '" + KeyboardMappings.INVENTORY_PANEL.getKeyChar() + "' again \nto exit from your inventory", var1));
   }
}
