package org.schema.game.client.controller.tutorial;

import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.controller.tutorial.states.SatisfyingCondition;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.tutorial.tutorialnew.GUITutorialStepPanelNew;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.KeyEventInterface;

public class TutorialDialog extends PlayerInput implements GUIActiveInterface {
   private GUITutorialStepPanelNew panelStat;
   private SatisfyingCondition condition;

   public TutorialDialog(GameClientState var1, String var2, Sprite var3, SatisfyingCondition var4) {
      super(var1);
      this.condition = var4;
      this.panelStat = new GUITutorialStepPanelNew("TUTORIAL", var1, var2, this, var3);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.getEventButtonState() && var2.getEventButton() == 0 && !isDelayedFromMainMenuDeactivation()) {
         PlayerInput.lastDialougeClick = System.currentTimeMillis();
         if (var1.getUserPointer().equals("NEXT")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - enter");
            this.condition.satisfy();
            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("BACK")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - enter");

            try {
               this.getState().getController().getTutorialMode().back();
               return;
            } catch (FSMException var3) {
               System.err.println("FSMException: " + var3.getMessage());
               return;
            }
         }

         if (var1.getUserPointer().equals("SKIP")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - cancel");
            this.getState().getController().getTutorialMode().skip();
            return;
         }

         if (var1.getUserPointer().equals("END")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - cancel");
            this.getState().getController().getTutorialMode().end();
            return;
         }

         assert false : "not known command: " + var1.getUserPointer();
      }

   }

   public SatisfyingCondition getCondition() {
      return this.condition;
   }

   public void setCondition(SatisfyingCondition var1) {
      this.condition = var1;
   }

   public GUIElement getInputPanel() {
      return this.panelStat;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
   }

   public boolean isActive() {
      return this.getState().getController().getPlayerInputs().indexOf(this) == this.getState().getController().getPlayerInputs().size() - 1;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public boolean isOccluded() {
      return false;
   }

   public void pressedNext() {
      synchronized(this.getState()) {
         boolean var2;
         if (var2 = !this.getState().isSynched()) {
            this.getState().setSynched();
         }

         this.getState().getController().queueUIAudio("0022_menu_ui - enter");
         this.condition.satisfy();

         try {
            this.getState().getController().getTutorialMode().getMachine().update();
            this.getState().getController().getTutorialMode().getMachine().update();
         } catch (FSMException var3) {
            var3.printStackTrace();
         }

         this.deactivate();
         if (var2) {
            this.getState().setUnsynched();
         }

      }
   }

   public void onDeactivate() {
   }

   public void pressedBack() {
      synchronized(this.getState()) {
         boolean var2;
         if (var2 = !this.getState().isSynched()) {
            this.getState().setSynched();
         }

         this.getState().getController().queueUIAudio("0022_menu_ui - enter");

         try {
            this.getState().getController().getTutorialMode().back();

            try {
               this.getState().getController().getTutorialMode().getMachine().update();
               this.getState().getController().getTutorialMode().getMachine().update();
            } catch (FSMException var4) {
               var4.printStackTrace();
            }
         } catch (FSMException var5) {
            System.err.println("FSMException: " + var5.getMessage());
         }

         if (var2) {
            this.getState().setUnsynched();
         }

      }
   }

   public void pressedSkip() {
      this.getState().getController().queueUIAudio("0022_menu_ui - cancel");
      this.getState().getController().getTutorialMode().skip();
   }

   public void pressedEnd() {
      this.getState().getController().queueUIAudio("0022_menu_ui - cancel");
      if (this.getState().getController().getTutorialMode().isEndState()) {
         this.pressedNext();
      } else if (this.getState().getPlayer().isInTutorial()) {
         this.getState().getGlobalGameControlManager().openExitTutorialPanel(this);
      } else {
         PlayerGameOkCancelInput var1;
         (var1 = new PlayerGameOkCancelInput("CONFIRM", this.getState(), "TUTORIAL EXIT", "Show tutorial on next start?") {
            public void onDeactivate() {
            }

            public void pressedOK() {
               this.deactivate();
            }

            public void cancel() {
               EngineSettings.TUTORIAL_NEW.changeBooleanSetting(false);
               super.cancel();
            }
         }).getInputPanel().setOkButtonText("YES");
         var1.getInputPanel().setCancelButtonText("NO");
         var1.activate();
         this.getState().getController().getTutorialMode().endNow();
         this.pressedNext();
      }
   }
}
