package org.schema.game.client.controller.tutorial.newtut;

import org.schema.game.client.view.gui.tutorial.tutorialnewvideo.GUITutorialVideoSelector;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class TutorialVideoPlayer extends DialogInput {
   private GUITutorialVideoSelector inputPanel;

   public TutorialVideoPlayer(InputState var1) {
      super(var1);
      this.inputPanel = new GUITutorialVideoSelector(var1, 850, 300, GLFrame.getWidth() / 2 - 425, GLFrame.getHeight() / 2 - 150, this, this);
      this.inputPanel.onInit();
      this.inputPanel.setCloseCallback(new GUICallback() {
         public boolean isOccluded() {
            return !TutorialVideoPlayer.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               TutorialVideoPlayer.this.deactivate();
            }

         }
      });
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public GUIElement getInputPanel() {
      return this.inputPanel;
   }

   public void onDeactivate() {
   }

   public void playIntroVideo() {
      this.inputPanel.playIntroVideo();
   }
}
