package org.schema.game.client.controller;

import org.schema.game.common.controller.elements.mines.MineController;

public interface MineInterface {
   MineController getMineController();
}
