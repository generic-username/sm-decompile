package org.schema.game.client.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.UploadInProgressException;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.updater.FileUtil;
import org.schema.game.network.objects.StringLongLongPair;
import org.schema.schine.resource.FileExt;

public class TextureSynchronizer implements Observer {
   private final ArrayList receivedFileBroadcasts = new ArrayList();
   private final ArrayList receivedFiles = new ArrayList();
   private final ArrayList playersToCheckQueue = new ArrayList();
   private final ArrayList timeStampResponses = new ArrayList();
   private final Map playerRequestMap = new HashMap();
   private GameClientState state;
   private String ownModelPath = "defaultMale";
   private boolean mapChanged = false;
   private boolean ownPlayerSatisfied = false;
   private TextureSynchronizationState fileRequest;
   private boolean ownSkinClientTimeStampSet;

   public TextureSynchronizer(GameClientState var1) {
      this.state = var1;
      var1.addObserver(this);
   }

   private void availableServerTimeStampOtherPlayer(TextureSynchronizationState var1) {
      if (var1.getPlayer().getSkinManager().getFileName().length() != 0) {
         if (!var1.getPlayer().getSkinManager().getFileName().equals("defaultMale") && (var1.getClientTimeStamp() < var1.getServerTimeStamp() || var1.getClientSize() != var1.getServerSize())) {
            this.fileRequest = var1;
            return;
         }

         var1.setReceived(true);
         var1.getPlayer().getSkinManager().flagUpdateTexture();
      }

   }

   private void availableServerTimeStampOwnPlayer(TextureSynchronizationState var1) {
      if (var1.getClientTimeStamp() <= var1.getServerTimeStamp() && var1.getClientSize() == var1.getServerSize()) {
         var1.setReceived(true);
         var1.getPlayer().getSkinManager().setFileName(this.ownModelPath);
         var1.getPlayer().getSkinManager().flagUpdateTexture();
      } else {
         if (this.ownModelPath.equals("defaultMale")) {
            this.state.getPlayer().getSkinManager().setFileName(this.ownModelPath);
         } else {
            var1.getClientTimeStamp();
            var1.getServerTimeStamp();
            var1.getClientSize();
            var1.getServerSize();

            try {
               this.state.getPlayer().getSkinManager().uploadFromClient(this.ownModelPath);
            } catch (IOException var2) {
               var2.printStackTrace();
            } catch (UploadInProgressException var3) {
               var3.printStackTrace();
            }
         }

         var1.setReceived(true);
         var1.getPlayer().getSkinManager().flagUpdateTexture();
      }

      this.ownPlayerSatisfied = true;
   }

   private void checkForNewOrChangedPlayers() {
      if (!this.playersToCheckQueue.isEmpty()) {
         synchronized(this.playersToCheckQueue) {
            while(!this.playersToCheckQueue.isEmpty()) {
               PlayerState var2 = (PlayerState)this.playersToCheckQueue.remove(0);
               this.playerRequestMap.put(var2, new TextureSynchronizationState(var2));
               this.mapChanged = true;
            }

         }
      }
   }

   public void fileChangeBroadcaseted(PlayerState var1, long var2, long var4) {
      synchronized(this.receivedFileBroadcasts) {
         this.receivedFileBroadcasts.add(new TextureSynchronizer.FileChangeBroadcast(var1, var2, var4));
      }
   }

   public String getModelPath() {
      return this.ownModelPath;
   }

   public void setModelPath(String var1) {
      System.err.println("[CLIENT] set own model path to " + var1);
      this.ownModelPath = var1;
   }

   public void handleDownloaded(File var1) {
      synchronized(this.receivedFiles) {
         this.receivedFiles.add(var1);
      }
   }

   private void handleFileRequest(TextureSynchronizationState var1) {
      if (!var1.requested) {
         String var2 = var1.getPlayer().getName() + ".smskin";
         System.err.println("[CLIENT] REQUESTING FILE FOR DOWNLOAD: " + var2);
         var1.requested = true;
         this.state.getController().getClientChannel().requestFile(var2);
      } else {
         if (!this.receivedFiles.isEmpty()) {
            synchronized(this.receivedFiles) {
               System.err.println("[TEXTURE SYNCH] RECEIVED FILES: " + this.receivedFiles);
               PlayerState var3 = var1.getPlayer();
               int var4 = 0;

               while(var4 < this.receivedFiles.size()) {
                  File var5;
                  if (!(var5 = (File)this.receivedFiles.get(var4)).getName().equals(var3.getName() + ".smskin")) {
                     ++var4;
                  } else {
                     String var6 = var3.getSkinManager().clientDB;
                     FileExt var9;
                     (var9 = new FileExt(var6 + var3.getName() + ".smskin")).delete();

                     try {
                        var9.createNewFile();
                        FileUtil.copyFile(var5, var9);
                        var5.delete();
                        var9.setLastModified(var1.getServerTimeStamp());
                     } catch (IOException var7) {
                        var7.printStackTrace();
                     }

                     this.receivedFiles.remove(var4);
                     var1.setReceived(true);
                     var3.getSkinManager().flagUpdateTexture();
                     break;
                  }
               }
            }

            this.fileRequest = null;
         }

      }
   }

   public void handleTimeStampResponse(StringLongLongPair var1) {
      synchronized(this.timeStampResponses) {
         this.timeStampResponses.add(new StringLongLongPair(var1));
      }
   }

   private void onNoServerTimeStampReceived(TextureSynchronizationState var1) {
      if (!var1.isTimeStampRequested()) {
         System.err.println("[CLIENT][TEXTURE-SYNCH] requesting tex synch of " + var1.getPlayer());
         FileExt var2;
         if ((var2 = new FileExt(this.state.getPlayer().getSkinManager().clientDB + var1.getPlayer().getName() + ".smskin")).exists()) {
            var1.setClientTimeStamp(var2.lastModified());
            var1.setClientSize(var2.length());
         }

         this.state.getController().getClientChannel().requestFileTimestamp(var1.getPlayer().getName());
         var1.setTimeStampRequested(true);
      } else {
         synchronized(this.timeStampResponses) {
            for(int var3 = 0; var3 < this.timeStampResponses.size(); ++var3) {
               StringLongLongPair var4;
               if ((var4 = (StringLongLongPair)this.timeStampResponses.get(var3)).playerName.equals(var1.getPlayer().getName())) {
                  var1.setServerTimeStamp(var4.timeStamp);
                  var1.setServerSize(var4.size);
               }
            }

         }
      }
   }

   private boolean receiveFileChaneBroadcast(TextureSynchronizer.FileChangeBroadcast var1) {
      if (!var1.player.isClientOwnPlayer()) {
         TextureSynchronizationState var2 = new TextureSynchronizationState(var1.player);
         this.playerRequestMap.put(var1.player, var2);
         FileExt var3;
         if ((var3 = new FileExt(var2.getPlayer().getSkinManager().clientDB + var2.getPlayer().getName() + ".smskin")).exists()) {
            var2.setClientTimeStamp(var3.lastModified());
            var2.setClientSize(var3.length());
         }

         var2.setServerTimeStamp(var1.fileChanged);
         var2.setServerSize(var1.fileSize);
         return true;
      } else {
         return false;
      }
   }

   public void synchronize() {
      this.checkForNewOrChangedPlayers();
      if (this.fileRequest == null) {
         this.updateSynchStates();
      } else {
         this.handleFileRequest(this.fileRequest);
      }
   }

   public void update(Observable var1, Object var2) {
      if (var2 instanceof PlayerState && this.state.getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(((PlayerState)var2).getId())) {
         synchronized(this.playersToCheckQueue) {
            System.err.println("[CLIENT][TEXTURE][SYNCHRONIZER] ADDED TO UPDATE QUEUE: " + var2 + " for " + this.state.getPlayer());
            this.playersToCheckQueue.add((PlayerState)var2);
         }
      }
   }

   private void updateSynchStates() {
      if (!this.receivedFileBroadcasts.isEmpty()) {
         synchronized(this.receivedFileBroadcasts) {
            while(!this.receivedFileBroadcasts.isEmpty()) {
               TextureSynchronizer.FileChangeBroadcast var2 = (TextureSynchronizer.FileChangeBroadcast)this.receivedFileBroadcasts.remove(0);
               this.mapChanged = this.receiveFileChaneBroadcast(var2);
            }
         }
      }

      boolean var1 = true;
      if (this.mapChanged) {
         Iterator var6 = this.playerRequestMap.values().iterator();

         label56:
         while(true) {
            TextureSynchronizationState var3;
            do {
               if (!var6.hasNext()) {
                  break label56;
               }

               var3 = (TextureSynchronizationState)var6.next();
               if (!this.ownSkinClientTimeStampSet && var3.getPlayer().isClientOwnPlayer() && !this.ownModelPath.equals("defaultMale")) {
                  FileExt var4 = new FileExt(this.ownModelPath);
                  var3.setClientTimeStamp(var4.lastModified());
                  var3.setClientSize(var4.length());
                  System.err.println("[CLIENT][TEXTURE-SYNCH] not using default skin. time of last changed skin: " + new Date(var3.getClientTimeStamp()));
                  this.ownSkinClientTimeStampSet = true;
               }
            } while(!this.ownPlayerSatisfied && !var3.getPlayer().isClientOwnPlayer());

            if (this.fileRequest != null) {
               break;
            }

            if (!var3.isReceived()) {
               if (var3.getServerTimeStamp() < 0L) {
                  this.onNoServerTimeStampReceived(var3);
               } else if (var3.getPlayer().isClientOwnPlayer()) {
                  this.availableServerTimeStampOwnPlayer(var3);
               } else {
                  this.availableServerTimeStampOtherPlayer(var3);
               }

               var1 = false;
            }
         }
      }

      this.mapChanged = !var1;
   }

   class FileChangeBroadcast {
      PlayerState player;
      long fileChanged;
      long fileSize;

      public FileChangeBroadcast(PlayerState var2, long var3, long var5) {
         this.player = var2;
         this.fileChanged = var3;
         this.fileSize = var5;
      }
   }
}
