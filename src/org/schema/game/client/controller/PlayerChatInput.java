package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.chat.ChatInputPanel;
import org.schema.game.client.view.gui.chat.ChatPanel;
import org.schema.game.common.data.chat.ChatCallback;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.KeyEventInterface;

public class PlayerChatInput extends PlayerInput {
   private ChatInputPanel panel;
   private GUIActiveInterface actInterface;
   private ChatPanel chatPanel;
   private ChatCallback chatCallback;

   public PlayerChatInput(String var1, GameClientState var2, Object var3, GUIActiveInterface var4, ChatPanel var5, ChatCallback var6) {
      super(var2);
      this.actInterface = var4;
      this.chatPanel = var5;
      this.chatCallback = var6;
      if (this.panel == null) {
         this.panel = new ChatInputPanel(var1, var2, var3, 370, 166, var6, this, var4, var5);
      }

      this.panel.setCallback(this);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var1.getUserPointer() != null && !var1.wasInside() && var1.isInside()) {
         this.getState().getController().queueUIAudio("0022_action - buttons push small");
      }

      if (var2.getEventButtonState() && var2.getEventButton() == 0) {
         if (var1.getUserPointer().equals("OK")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - enter");
            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("CANCEL")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - back");
            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("X")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - back");
            this.deactivate();
            return;
         }

         assert false : "not known command: '" + var1.getUserPointer() + "'";
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
   }

   public void deactivate() {
      this.chatPanel.deactivate(this);
   }

   public ChatInputPanel getInputPanel() {
      return this.panel;
   }

   public void onDeactivate() {
      this.chatCallback.onWindowDeactivate();
   }

   public boolean isOccluded() {
      return !this.actInterface.isActive();
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public void setErrorMessage(String var1) {
      System.err.println(var1);
   }
}
