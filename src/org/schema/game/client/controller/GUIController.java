package org.schema.game.client.controller;

import java.util.Observable;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;

public class GUIController extends Observable {
   public static final int GUI_MODE_INGAME = 0;
   public static final int GUI_MODE_INVENTORY = 1;
   public static final int GUI_MODE_SHOP = 2;
   private int guiMode = 0;
   private GameClientState state;

   public GUIController(GameClientState var1) {
      this.setState(var1);
   }

   public int getGUIMode() {
      return this.guiMode;
   }

   public void setGUIMode(int var1) {
      this.guiMode = var1;
   }

   public GameClientState getState() {
      return this.state;
   }

   public void setState(GameClientState var1) {
      this.state = var1;
   }

   public void update() {
      this.state.getPlayer().getInventory((Vector3i)null);
   }
}
