package org.schema.game.client.controller;

public interface EntityTrackingChangedListener {
   void onTrackingChanged();
}
