package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.mail.GUICreateMailPanel;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;

public class PlayerMailInputNew extends PlayerInput {
   private GUICreateMailPanel mailInputPanel;

   public PlayerMailInputNew(GameClientState var1, String var2, String var3) {
      super(var1);
      this.mailInputPanel = new GUICreateMailPanel(var1, this, var2, var3);
      this.mailInputPanel.setCallback(this);
   }

   public PlayerMailInputNew(GameClientState var1) {
      this(var1, "", "");
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (!this.isOccluded() && var2.pressedLeftMouse()) {
         if (var1.getUserPointer().equals("OK") && this.sendMail()) {
            this.deactivate();
         }

         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            this.cancel();
         }
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public GUIElement getInputPanel() {
      return this.mailInputPanel;
   }

   public void onDeactivate() {
      this.mailInputPanel.cleanUp();
   }

   public boolean isOccluded() {
      return false;
   }

   public String getTo() {
      return this.mailInputPanel.getTo().trim();
   }

   public String getSubject() {
      return this.mailInputPanel.getSubject().trim();
   }

   public String getMessage() {
      return this.mailInputPanel.getMessage().trim();
   }

   private boolean sendMail() {
      return this.getState().getController().getClientChannel().getPlayerMessageController().clientSend(this.getState().getPlayerName(), this.getTo(), this.getSubject(), this.getMessage());
   }

   public void handleMouseEvent(MouseEvent var1) {
   }
}
