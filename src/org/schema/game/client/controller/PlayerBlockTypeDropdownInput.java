package org.schema.game.client.controller;

import com.bulletphysics.collision.dispatch.CollisionWorld.ClosestRayResultCallback;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIBlockSprite;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.game.common.data.physics.PhysicsExt;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextInput;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public abstract class PlayerBlockTypeDropdownInput extends PlayerGameDropDownInput {
   public ObjectArrayList additionalElements;
   private GUITextInput guiTextInput;
   private PlayerTextInputBar playerTextInputBar;
   private Object info;

   public PlayerBlockTypeDropdownInput(String var1, GameClientState var2, Object var3, ObjectArrayList var4) {
      this(var1, var2, var3, var4, 1);
   }

   public PlayerBlockTypeDropdownInput(String var1, GameClientState var2, Object var3, ObjectArrayList var4, int var5) {
      super(var1, var2, var3, 32);
      this.additionalElements = var4;
      this.info = var3;
      this.guiTextInput = new GUITextInput(180, 20, var2);
      this.playerTextInputBar = new PlayerTextInputBar(this.getState(), 30, this.getInputPanel(), this.guiTextInput) {
         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public void onFailedTextCheck(String var1) {
         }

         public boolean isOccluded() {
            return false;
         }

         public void onDeactivate() {
         }

         public boolean onInput(String var1) {
            return false;
         }
      };
      this.guiTextInput.setPreText(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKTYPEDROPDOWNINPUT_0);
      this.guiTextInput.setDrawCarrier(true);
      this.guiTextInput.setPos(20.0F, 0.0F, 0.0F);
      this.guiTextInput.dependend = ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().getContent(0);
      this.guiTextInput.dependendWidthOffset = -40;
      this.getInputPanel().getContent().attach(this.guiTextInput);
      GUITextButton var6;
      (var6 = new GUITextButton(this.getState(), 150, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKTYPEDROPDOWNINPUT_1, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               Vector3f var4 = new Vector3f(Controller.getCamera().getPos());
               Vector3f var7 = new Vector3f(var4);
               Vector3f var3;
               if (Float.isNaN((var3 = new Vector3f(Controller.getCamera().getForward())).x)) {
                  return;
               }

               var3.normalize();
               var3.scale(300.0F);
               var7.add(var3);
               ClosestRayResultCallback var5;
               if ((var5 = ((PhysicsExt)PlayerBlockTypeDropdownInput.this.getState().getPhysics()).testRayCollisionPoint(var4, var7, false, PlayerBlockTypeDropdownInput.this.getState().getCharacter(), (SegmentController)null, true, true, false)) != null && var5.hasHit() && var5 instanceof CubeRayCastResult) {
                  CubeRayCastResult var6 = (CubeRayCastResult)var5;
                  SegmentPiece var8;
                  if (ElementKeyMap.isValidType((var8 = new SegmentPiece(var6.getSegment(), var6.getCubePos())).getType())) {
                     ElementInformation var9 = ElementKeyMap.getInfo(var8.getType());
                     PlayerBlockTypeDropdownInput.this.setSelectedUserPointer(var9);
                     System.err.println("[DROPDOWN] set type by pick: " + var8.getType());
                  }
               }
            }

         }
      })).setPos(5.0F, 85.0F, 0.0F);
      this.getInputPanel().getContent().attach(var6);
      this.updateDropdown("");
   }

   public PlayerBlockTypeDropdownInput(String var1, GameClientState var2, Object var3, int var4) {
      this(var1, var2, var3, (ObjectArrayList)null, var4);
   }

   public PlayerBlockTypeDropdownInput(String var1, GameClientState var2, Object var3) {
      this(var1, var2, var3, (ObjectArrayList)null);
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, this.getState())) {
         if (this.isDeactivateOnEscape() && KeyboardMappings.getEventKeyRaw(var1) == 1) {
            this.deactivate();
            return;
         }

         this.playerTextInputBar.getTextInput().handleKeyEvent(var1);
         this.updateDropdown(this.playerTextInputBar.getText());
      }

   }

   private void updateDropdown(String var1) {
      this.update(this.getState(), this.info, 32, "", this.getElements(this.getState(), var1, this.additionalElements));
   }

   public List getElements(GameClientState var1, String var2, ObjectArrayList var3) {
      ObjectArrayList var4 = new ObjectArrayList();
      if (var3 != null) {
         var4.addAll(var3);
      }

      Iterator var8 = ElementKeyMap.sortedByName.iterator();

      while(true) {
         ElementInformation var5;
         do {
            if (!var8.hasNext()) {
               return var4;
            }

            var5 = (ElementInformation)var8.next();
         } while(var2.trim().length() != 0 && !var5.getName().toLowerCase(Locale.ENGLISH).contains(var2.trim().toLowerCase(Locale.ENGLISH)));

         GUIAncor var6 = new GUIAncor(var1, 800.0F, 32.0F);
         var4.add(var6);
         GUITextOverlay var7;
         (var7 = new GUITextOverlay(100, 32, FontLibrary.getBoldArial12White(), var1)).setTextSimple(var5.getName());
         var6.setUserPointer(var5);
         GUIBlockSprite var9;
         (var9 = new GUIBlockSprite(var1, var5.getId())).getScale().set(0.5F, 0.5F, 0.5F);
         var6.attach(var9);
         var7.getPos().x = 50.0F;
         var7.getPos().y = 7.0F;
         var6.attach(var7);
      }
   }

   public void onDeactivate() {
   }

   public boolean isOccluded() {
      return false;
   }

   public void pressedOK(GUIListElement var1) {
      this.onOk((ElementInformation)var1.getContent().getUserPointer());
      this.deactivate();
   }

   public abstract void onOk(ElementInformation var1);
}
