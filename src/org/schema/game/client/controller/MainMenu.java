package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.gamemenus.gamemenusnew.MainMenuPanelNew;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.KeyEventInterface;

public class MainMenu extends PlayerInput implements GUIActiveInterface {
   private GUIElement menuPanel;

   public MainMenu(GameClientState var1) {
      super(var1);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var1.getUserPointer() != null && !var1.wasInside() && var1.isInside()) {
         this.getState().getController().queueUIAudio("0022_action - buttons push small");
      }

      if (var2.pressedLeftMouse()) {
         if (var1.getUserPointer().equals("TUTORIAL")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - enter");
            this.deactivate();
            this.getState().getGlobalGameControlManager().getMainMenuManager().setActive(false);
            this.getState().getGlobalGameControlManager().getIngameControlManager().setDelayedActive(true);
            this.getState().getGlobalGameControlManager().getIngameControlManager().hinderInteraction(500);
            if (this.getState().getController().getPlayerInputs().isEmpty()) {
               if (this.getState().getPlayer().isInTestSector()) {
                  this.getState().getGlobalGameControlManager().openExitTestSectorPanel((PlayerInput)null);
               }

               if (this.getState().getPlayer().isInTutorial()) {
                  this.getState().getGlobalGameControlManager().openExitTutorialPanel((PlayerInput)null);
                  return;
               }

               this.getState().getGlobalGameControlManager().openTutorialPanel();
               return;
            }
         } else {
            if (var1.getUserPointer().equals("RESUME") || var1.getUserPointer().equals("X")) {
               this.getState().getController().queueUIAudio("0022_menu_ui - enter");
               this.deactivate();
               this.getState().getGlobalGameControlManager().getMainMenuManager().setActive(false);
               this.getState().getGlobalGameControlManager().getIngameControlManager().setDelayedActive(true);
               this.getState().getGlobalGameControlManager().getIngameControlManager().hinderInteraction(500);
               return;
            }

            if (var1.getUserPointer().equals("EXIT")) {
               this.getState().getController().queueUIAudio("0022_menu_ui - back");
               this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MAINMENU_0);
               return;
            }

            if (var1.getUserPointer().equals("MESSAGELOG")) {
               this.getState().getController().queueUIAudio("0022_menu_ui - back");
               this.getState().getGlobalGameControlManager().getIngameControlManager().setDelayedActive(true);
               this.getState().getGlobalGameControlManager().getIngameControlManager().hinderInteraction(500);
               this.getState().getGlobalGameControlManager().getIngameControlManager().activateMesssageLog();
               this.getState().getGlobalGameControlManager().getMainMenuManager().setActive(false);
               return;
            }

            if (var1.getUserPointer().equals("RESPAWN")) {
               this.getState().getController().queueUIAudio("0022_menu_ui - enter");
               String var3 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MAINMENU_1;
               (new PlayerGameOkCancelInput("CONFIRM", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MAINMENU_10, var3) {
                  public void onDeactivate() {
                     this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().hinderInteraction(400);
                  }

                  public boolean isOccluded() {
                     return false;
                  }

                  public void pressedOK() {
                     this.getState().getController().queueUIAudio("0022_action - buttons push medium");
                     if (this.getState().getController().suicide()) {
                        this.deactivate();
                        this.getState().getGlobalGameControlManager().getMainMenuManager().setActive(false);
                        this.getState().getGlobalGameControlManager().getIngameControlManager().setDelayedActive(true);
                        this.getState().getGlobalGameControlManager().getIngameControlManager().hinderInteraction(500);
                     } else {
                        this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MAINMENU_3);
                     }

                     this.deactivate();
                  }
               }).activate();
               return;
            }

            if (var1.getUserPointer().equals("EXIT_TO_WINDOWS")) {
               this.pressedExit();
               return;
            }

            if (var1.getUserPointer().equals("OPTIONS")) {
               this.getState().getController().queueUIAudio("0022_menu_ui - enter");
               this.getState().getGlobalGameControlManager().getMainMenuManager().setActive(false);
               this.getState().getGlobalGameControlManager().getOptionsControlManager().setActive(true);
               return;
            }

            assert false : "not known command: " + var1.getUserPointer();
         }
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public boolean isActive() {
      return this.getState().getController().getPlayerInputs().indexOf(this) == this.getState().getController().getPlayerInputs().size() - 1;
   }

   public GUIElement getInputPanel() {
      if (this.menuPanel == null) {
         this.menuPanel = new MainMenuPanelNew(this.getState(), this);
         ((MainMenuPanelNew)this.menuPanel).setCloseCallback(new GUICallback() {
            public boolean isOccluded() {
               return !MainMenu.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  MainMenu.this.pressedResume();
               }

            }
         });
         ((MainMenuPanelNew)this.menuPanel).activeInterface = this;
      }

      return this.menuPanel;
   }

   public void onDeactivate() {
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public boolean isOccluded() {
      return false;
   }

   public void setErrorMessage(String var1) {
      this.getState().getController().popupAlertTextMessage(var1, 0.0F);
   }

   public void pressedResume() {
      this.getState().getController().queueUIAudio("0022_menu_ui - enter");
      this.deactivate();
      this.getState().getGlobalGameControlManager().getMainMenuManager().setActive(false);
      this.getState().getGlobalGameControlManager().getIngameControlManager().setDelayedActive(true);
      this.getState().getGlobalGameControlManager().getIngameControlManager().hinderInteraction(500);
   }

   public void pressedOptions() {
      this.getState().getController().queueUIAudio("0022_menu_ui - enter");
      this.getState().getGlobalGameControlManager().getMainMenuManager().setActive(false);
      this.getState().getGlobalGameControlManager().getOptionsControlManager().setActive(true);
   }

   public String getTutorialsStringObj() {
      if (this.getState().getPlayer().isInTestSector()) {
         return Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MAINMENU_6;
      } else {
         return this.getState().getPlayer().isInTutorial() ? Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MAINMENU_7 : Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MAINMENU_8;
      }
   }

   public void pressedTutorials() {
      this.getState().getController().queueUIAudio("0022_menu_ui - enter");
      this.deactivate();
      this.getState().getGlobalGameControlManager().getMainMenuManager().setActive(false);
      this.getState().getGlobalGameControlManager().getIngameControlManager().setDelayedActive(true);
      this.getState().getGlobalGameControlManager().getIngameControlManager().hinderInteraction(500);
      if (this.getState().getPlayer().isInTestSector()) {
         this.getState().getGlobalGameControlManager().openExitTestSectorPanel((PlayerInput)null);
      } else {
         if (this.getState().getController().getPlayerInputs().isEmpty()) {
            this.getState().getController().getTutorialController().onActivateFromTopTaskBar();
         }

      }
   }

   public void pressedSuicide() {
      this.getState().getController().queueUIAudio("0022_menu_ui - enter");
      String var1 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MAINMENU_9;
      (new PlayerGameOkCancelInput("CONFIRM", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MAINMENU_2, var1) {
         public boolean isOccluded() {
            return false;
         }

         public void onDeactivate() {
            this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().hinderInteraction(400);
         }

         public void pressedOK() {
            this.getState().getController().queueUIAudio("0022_action - buttons push medium");
            if (this.getState().getController().suicide()) {
               this.deactivate();
               this.getState().getGlobalGameControlManager().getMainMenuManager().setActive(false);
               this.getState().getGlobalGameControlManager().getIngameControlManager().setDelayedActive(true);
               this.getState().getGlobalGameControlManager().getIngameControlManager().hinderInteraction(500);
            } else {
               this.setErrorMessage("Those who don't exist cannot be killed!.");
            }

            this.deactivate();
         }
      }).activate();
   }

   public void pressedExit() {
      this.getState().getController().queueUIAudio("0022_menu_ui - back");
      String var1 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MAINMENU_11;
      PlayerGameOkCancelInput var2;
      (var2 = new PlayerGameOkCancelInput("CONFIRM", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MAINMENU_12, var1) {
         public boolean isOccluded() {
            return false;
         }

         public void onDeactivate() {
            this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().hinderInteraction(400);
         }

         public void pressedOK() {
            GameMainMenuController.currentMainMenu.switchFrom(this.getState());
            this.deactivate();
         }

         public void pressedSecondOption() {
            this.getState().getController().queueUIAudio("0022_action - buttons push medium");
            GLFrame.setFinished(true);
            this.deactivate();
         }
      }).getInputPanel().setOkButtonText(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MAINMENU_14);
      var2.getInputPanel().setSecondOptionButton(true);
      var2.getInputPanel().setSecondOptionButtonText(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MAINMENU_13);
      var2.activate();
   }
}
