package org.schema.game.client.controller;

import java.io.IOException;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.font.FontStyle;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.settingsnew.GUICheckBoxTextPairNew;

public class PlayerContextHelpDialog {
   private GameClientState state;
   private EngineSettings setting;
   private String desc;
   private boolean ignore;
   private FontLibrary.FontSize descText;
   private FontLibrary.FontSize checkBoxText;
   boolean checkBoxChecked;
   public boolean ignoreToggle;

   public PlayerContextHelpDialog(GameClientState var1, EngineSettings var2, String var3, boolean var4) {
      this.descText = FontLibrary.FontSize.MEDIUM;
      this.checkBoxText = FontLibrary.FontSize.MEDIUM;
      this.ignoreToggle = true;
      this.state = var1;
      this.setting = var2;
      this.desc = var3;
      this.ignore = var4;
      this.checkBoxChecked = !var2.isOn();
   }

   public void activate() {
      if (this.state.getPlayerInputs().isEmpty()) {
         final PlayerOkCancelInput var1;
         (var1 = new PlayerOkCancelInput("CONTEXT_HLP", this.state, 400, 200, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERCONTEXTHELPDIALOG_0, this.desc, new FontStyle(this.descText)) {
            public void pressedOK() {
               this.deactivate();
            }

            public void onDeactivate() {
               PlayerContextHelpDialog.this.setting.setCurrentState(!PlayerContextHelpDialog.this.checkBoxChecked);

               try {
                  EngineSettings.write();
               } catch (IOException var1) {
                  var1.printStackTrace();
               }
            }
         }).getInputPanel().setCancelButton(false);
         var1.getInputPanel().onInit();
         GUITextOverlay var2 = var1.getInputPanel().getDescriptionText();
         String var3;
         if (this.ignore) {
            var3 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERCONTEXTHELPDIALOG_1;
         } else {
            var3 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERCONTEXTHELPDIALOG_2;
         }

         GUICheckBoxTextPairNew var4 = new GUICheckBoxTextPairNew(this.state, var3, this.checkBoxText.getFont()) {
            public boolean isChecked() {
               return PlayerContextHelpDialog.this.checkBoxChecked;
            }

            public void deactivate() {
               PlayerContextHelpDialog.this.checkBoxChecked = false;
            }

            public void activate() {
               PlayerContextHelpDialog.this.checkBoxChecked = true;
            }

            public void draw() {
               if (PlayerContextHelpDialog.this.ignoreToggle) {
                  this.setPos(var1.getInputPanel().getButtonOK().getWidth() + 6.0F, 0.0F, 0.0F);
                  super.draw();
               }

            }
         };
         var2.updateTextSize();
         var4.onInit();
         var1.getInputPanel().getButtonOK().attach(var4);
         var1.activate();
      }
   }
}
