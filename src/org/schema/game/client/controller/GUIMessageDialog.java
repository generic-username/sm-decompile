package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIMessagePanel;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;

public class GUIMessageDialog extends PlayerInput {
   GUIMessagePanel panel;

   public GUIMessageDialog(GameClientState var1, String var2, String var3, boolean var4) {
      super(var1);
      this.panel = new GUIMessagePanel(var1, this, var2, var3, var4);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.getEventButtonState() && var2.getEventButton() == 0 && !isDelayedFromMainMenuDeactivation()) {
         PlayerInput.lastDialougeClick = System.currentTimeMillis();
         if (var1.getUserPointer().equals("OK")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - enter");
            this.onOK();
            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("CANCEL")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - enter");
            this.onCancel();
            this.deactivate();
            return;
         }

         assert false : "not known command: " + var1.getUserPointer();
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
   }

   public GUIElement getInputPanel() {
      return this.panel;
   }

   public void onDeactivate() {
   }

   public boolean isOccluded() {
      return false;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public void onCancel() {
   }

   public void onOK() {
   }
}
