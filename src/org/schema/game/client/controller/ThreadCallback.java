package org.schema.game.client.controller;

public interface ThreadCallback {
   float getPercent();

   boolean isFinished();

   void onFinished();
}
