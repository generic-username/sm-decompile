package org.schema.game.client.controller;

public interface ClientSectorChangeListener {
   void onSectorChangeSelf(int var1, int var2);
}
