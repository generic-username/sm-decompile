package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.thrustmanagement.ThrustManagementPanelNew;
import org.schema.game.common.controller.Ship;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.KeyEventInterface;

public class PlayerThrustManagerInput extends PlayerInput implements GUIActiveInterface {
   private ThrustManagementPanelNew panel;

   public PlayerThrustManagerInput(GameClientState var1, Ship var2) {
      super(var1);
      this.panel = new ThrustManagementPanelNew(var1, this, var2);
      this.panel.setCloseCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               PlayerThrustManagerInput.this.deactivate();
               PlayerThrustManagerInput.this.getState().getGlobalGameControlManager().getIngameControlManager().getMessageLogManager().setActive(false);
            }

         }

         public boolean isOccluded() {
            return !PlayerThrustManagerInput.this.isActive();
         }
      });
      this.panel.reset();
      this.panel.activeInterface = this;
   }

   public void update(Timer var1) {
      if (this.panel != null) {
         this.panel.update(var1);
      }

   }

   public String getCurrentChatPrefix() {
      return "";
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var1.getUserPointer() != null && !var1.wasInside() && var1.isInside()) {
         this.getState().getController().queueUIAudio("0022_action - buttons push small");
      }

      if (var2.getEventButtonState() && var2.getEventButton() == 0) {
         if (var1.getUserPointer().equals("OK")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - enter");
            this.deactivate();
            this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().activateThrustManager((Ship)null);
            return;
         }

         if (var1.getUserPointer().equals("CANCEL")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - back");
            this.deactivate();
            this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().activateThrustManager((Ship)null);
            return;
         }

         if (var1.getUserPointer().equals("X")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - back");
            this.deactivate();
            this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().activateThrustManager((Ship)null);
            return;
         }

         assert false : "not known command: '" + var1.getUserPointer() + "'";
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public boolean isActive() {
      return this.getState().getController().getPlayerInputs().isEmpty() || ((DialogInterface)this.getState().getController().getPlayerInputs().get(this.getState().getController().getPlayerInputs().size() - 1)).getInputPanel() == this.panel;
   }

   public GUIElement getInputPanel() {
      return this.panel;
   }

   public void onDeactivate() {
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getThrustManager().setDelayedActive(false);
      this.panel.cleanUp();
   }

   public boolean isOccluded() {
      return false;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public void setErrorMessage(String var1) {
      System.err.println(var1);
   }
}
