package org.schema.game.client.controller;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.swing.JDialog;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.common.ParseException;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.client.view.gui.LoadingScreenDetailed;
import org.schema.game.client.view.mainmenu.LocalUniverse;
import org.schema.game.client.view.mainmenu.MainMenuFrame;
import org.schema.game.client.view.mainmenu.gui.GameStarterState;
import org.schema.game.common.Starter;
import org.schema.game.common.updater.FileUtil;
import org.schema.game.common.util.DataUtil;
import org.schema.game.common.util.FolderZipper;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.game.common.util.ZipGUICallback;
import org.schema.game.common.version.Version;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.GraphicsMainMenuController;
import org.schema.schine.common.JoystickAxisMapping;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.ErrorHandlerInterface;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.GraphicsFrame;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.ButtonColorImpl;
import org.schema.schine.input.BasicInputController;
import org.schema.schine.input.InputController;
import org.schema.schine.input.InputState;
import org.schema.schine.input.JoystickEvent;
import org.schema.schine.input.JoystickMappingFile;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.ServerInfo;
import org.schema.schine.network.ServerListRetriever;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.network.client.HostPortLoginName;
import org.schema.schine.network.client.KBMapInterface;
import org.schema.schine.network.server.ServerState;
import org.schema.schine.resource.FileExt;
import org.xml.sax.SAXException;

public class GameMainMenuController extends GraphicsMainMenuController implements GameStarterState, ErrorHandlerInterface, InputController, InputState {
   public static GameMainMenuController currentMainMenu;
   private final BasicInputController inputController = new BasicInputController();
   private final ServerListRetriever serverListRetriever = new ServerListRetriever();
   private final List generalChatLog = new ObjectArrayList();
   private final List visibleChatLog = new ObjectArrayList();
   private short updateNumber;
   private final MainMenuFrame frame;
   private LocalUniverse selectedLocalUniverse;
   private int maxFile;
   private int file;
   private ServerInfo selectedOnlineUniverse;
   private Exception queuedException;
   private boolean showedWarning;
   private boolean inTextBox;

   public GameMainMenuController() {
      if (GUITextButton.cp == null) {
         GUITextButton.cp = new ButtonColorImpl();
      }

      if (Controller.getResLoader() == null) {
         Controller.initResLoader(new GameResourceLoader());
      }

      this.frame = new MainMenuFrame(this);
      this.setFrame(this.frame, false);
      this.graphicsContext.errorHandler = this;
      currentMainMenu = this;
   }

   public void switchFrom(GameClientState var1) {
      this.switchFrom(var1, (Exception)null);
   }

   public void switchFrom(final GameClientState var1, final Exception var2) {
      if (var1.getScene() != null) {
         GraphicsContext.cleanUpScene = var1.getScene();
      }

      (new Thread(new Runnable() {
         public void run() {
            GameMainMenuController.this.setFrame((GraphicsFrame)null, false);
            var1.stopClient();
            var1.setDoNotDisplayIOException(true);
            var1.setExitApplicationOnDisconnect(false);
            ClientState.setFinishedFrameAfterLocalServerShutdown = false;
            if (ServerState.isCreated()) {
               ServerState.setFlagShutdown(true);
            }

            String[] var1x = new String[]{"/", "-", "\\"};
            int var2x = 0;
            long var3 = System.currentTimeMillis();

            while(ServerState.isCreated()) {
               GameMainMenuController.this.graphicsContext.setLoadMessage("WAITING FOR LOCAL SERVER TO SHUT DOWN... " + var1x[var2x % var1x.length]);
               if (System.currentTimeMillis() - var3 > 30L) {
                  ++var2x;
                  var3 = System.currentTimeMillis();
               }

               if (var2x > 1000 && var2x % 500 == 0) {
                  System.err.println("[MAINMENU] SOMETHING WENT WRONG SHUTTING DOWN SERVER: " + ServerState.isCreated() + "; " + ServerState.isShutdown());
                  if (!ServerState.isShutdown()) {
                     continue;
                  }
                  break;
               }
            }

            GameMainMenuController.this.graphicsContext.setLoadMessage("RESETTING VARIABLES... ");
            ServerState.clearStatic();
            GameMainMenuController.this.graphicsContext.setLoadMessage("LOADING MAIN MENU... ");
            GameMainMenuController.this.setFrame(GameMainMenuController.this.frame, false);
            GLFrame.activeForInput = true;
            GameMainMenuController.this.queuedException = var2;
         }
      })).start();
   }

   public void update() {
      Timer var1 = this.graphicsContext.timer;
      if (this.queuedException != null) {
         this.errorDialog(this.queuedException);
         this.queuedException = null;

         assert this.inputController.getPlayerInputs().size() > 0;
      }

      if (!Version.is64Bit() && EngineSettings.SHOW_32BIT_WARNING.isOn() && !this.showedWarning) {
         PlayerOkCancelInput var2;
         (var2 = new PlayerOkCancelInput("32BIT_SSss", this.getState(), 460, 280, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_18, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_19) {
            public void pressedOK() {
               this.deactivate();
            }

            public void onDeactivate() {
            }

            public void pressedSecondOption() {
               super.pressedSecondOption();
               EngineSettings.SHOW_32BIT_WARNING.setCurrentState(false);

               try {
                  EngineSettings.write();
               } catch (IOException var1) {
                  var1.printStackTrace();
               }

               this.deactivate();
            }
         }).getInputPanel().setSecondOptionButton(true);
         var2.getInputPanel().setSecondOptionButtonWidth(120);
         var2.getInputPanel().setSecondOptionButtonText(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_20);
         var2.getInputPanel().setCancelButton(false);
         var2.activate();
         this.showedWarning = true;
      }

      this.frame.update(var1);
      this.inputController.updateInput(this, var1);
      ((LoadingScreenDetailed)this.graphicsContext.getLoadingScreen()).setMainMenu(this);
      ++this.updateNumber;
   }

   public List getPlayerInputs() {
      return this.inputController.getPlayerInputs();
   }

   public void flagChangedForObservers(Object var1) {
      this.setChanged();
      this.notifyObservers(var1);
   }

   public boolean isChatActive() {
      return false;
   }

   public static void createWorld(final String var0, final InputState var1) {
      PlayerOkCancelInput var10;
      if (var0.toLowerCase(Locale.ENGLISH).equals("old")) {
         (var10 = new PlayerOkCancelInput("CONFIRM", var1, 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_0, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_1) {
            public final void pressedOK() {
               this.deactivate();
            }

            public final void onDeactivate() {
            }
         }).getInputPanel().setCancelButton(false);
         var10.getInputPanel().onInit();
         var10.activate();
      } else if ((new FileExt(GameServerState.SERVER_DATABASE + var0)).exists()) {
         (var10 = new PlayerOkCancelInput("CONFIRM", var1, 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_2, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_3) {
            public final void pressedOK() {
               this.deactivate();
            }

            public final void onDeactivate() {
            }
         }).getInputPanel().setCancelButton(false);
         var10.getInputPanel().onInit();
         var10.activate();
      } else {
         (new FileExt(GameServerState.SERVER_DATABASE + var0 + File.separator)).mkdirs();
         List var2;
         if ((var2 = LocalUniverse.readUniverses()).size() > 0) {
            GUIElement[] var3 = new GUIElement[var2.size() - 1];
            int var4 = 0;
            Iterator var8 = var2.iterator();

            while(var8.hasNext()) {
               LocalUniverse var5;
               if (!(var5 = (LocalUniverse)var8.next()).name.equals(var0)) {
                  GUIAncor var6 = new GUIAncor(var1, 200.0F, 24.0F);
                  GUITextOverlay var7;
                  (var7 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProBook16(), var1)).setTextSimple(var5.name);
                  var7.setPos(4.0F, 4.0F, 0.0F);
                  var6.attach(var7);
                  var6.setUserPointer(var5);
                  var3[var4] = var6;
                  ++var4;
               }
            }

            String var9 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_4;
            PlayerDropDownInput var11;
            (var11 = new PlayerDropDownInput("DD", var1, 450, 240, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_5, 24, var9, var3) {
               public final void pressedOK(GUIListElement var1x) {
                  try {
                     LocalUniverse var5 = (LocalUniverse)var1x.getContent().getUserPointer();
                     FileExt var2 = new FileExt(GameServerState.SERVER_DATABASE + var0 + File.separator + "CATALOG.cat");
                     FileExt var6 = new FileExt(GameServerState.SERVER_DATABASE + var5.name + File.separator + "CATALOG.cat");

                     try {
                        FileUtil.copyFile(var2, var6);
                     } catch (IOException var3) {
                        var3.printStackTrace();
                     }
                  } catch (Exception var4) {
                     var4.printStackTrace();
                  }

                  this.deactivate();
               }

               public final void onDeactivate() {
                  var1.flagChangedForObservers((Object)null);
               }
            }).setDropdownYPos(120);
            var11.activate();
         }

      }
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      this.inputController.handleKeyEventInputPanels(var1);
      if (KeyboardMappings.getEventKeyState(var1, this.getState())) {
         switch(KeyboardMappings.getEventKeyRaw(var1)) {
         case 28:
            if (this.frame != null && this.getInputController().getPlayerInputs().isEmpty()) {
               this.startLastUsed();
               return;
            }
            break;
         case 87:
            if (this.frame != null) {
               try {
                  this.frame.enqueueFrameResources();
                  return;
               } catch (FileNotFoundException var2) {
                  var2.printStackTrace();
                  return;
               } catch (ResourceException var3) {
                  var3.printStackTrace();
                  return;
               } catch (ParseException var4) {
                  var4.printStackTrace();
                  return;
               } catch (SAXException var5) {
                  var5.printStackTrace();
                  return;
               } catch (IOException var6) {
                  var6.printStackTrace();
                  return;
               } catch (ParserConfigurationException var7) {
                  var7.printStackTrace();
               }
            }
         }
      }

   }

   public void handleJoystickEventButton(JoystickEvent var1) {
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public void handleLocalMouseInput() {
   }

   public void onMouseEvent(MouseEvent var1) {
   }

   public boolean beforeInputUpdate() {
      return true;
   }

   public BasicInputController getInputController() {
      return this.inputController;
   }

   public boolean isJoystickKeyboardButtonDown(KBMapInterface var1) {
      return this.inputController.getJoystick().isKeyboardButtonDown((KeyboardMappings)var1);
   }

   public boolean isJoystickOk() {
      return JoystickMappingFile.ok();
   }

   public boolean isJoystickMouseRigthButtonDown() {
      return this.inputController.getJoystick().getRightMouse().isDown();
   }

   public boolean isJoystickMouseLeftButtonDown() {
      return this.inputController.getJoystick().getLeftMouse().isDown();
   }

   public double getJoystickAxis(JoystickAxisMapping var1) {
      return this.inputController.getJoystick().getAxis(var1);
   }

   public void queueUIAudio(String var1) {
   }

   public InputState getState() {
      return this;
   }

   public InputController getController() {
      return this;
   }

   public short getNumberOfUpdate() {
      return this.updateNumber;
   }

   public List getGeneralChatLog() {
      return this.generalChatLog;
   }

   public List getVisibleChatLog() {
      return this.visibleChatLog;
   }

   public void onSwitchedSetting(EngineSettings var1) {
   }

   public String onAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
      return null;
   }

   public ServerListRetriever getServerListRetriever() {
      return this.serverListRetriever;
   }

   public void onSelected(Object var1) {
      assert var1 != null;

      if (var1 instanceof LocalUniverse) {
         System.err.println("[GUI] SELECTED SP INFO " + var1);
         this.selectedLocalUniverse = (LocalUniverse)var1;
      } else {
         if (var1 instanceof ServerInfo) {
            System.err.println("[GUI] SELECTED ONLINE SERVER INFO " + var1);
            this.setSelectedOnlineUniverse((ServerInfo)var1);
         }

      }
   }

   public void onDoubleClick(Object var1) {
      if (var1 instanceof LocalUniverse) {
         this.startSelectedLocalGame();
      } else {
         if (var1 instanceof ServerInfo) {
            this.startSelectedOnlineGame();
         }

      }
   }

   public void startSelectedLocalGame() {
      if (this.selectedLocalUniverse != null) {
         EngineSettings.LAST_GAME.setCurrentState("SP;" + this.selectedLocalUniverse.name + ";4242;" + EngineSettings.OFFLINE_PLAYER_NAME.getCurrentState().toString().trim());

         try {
            EngineSettings.write();
         } catch (IOException var5) {
            var5.printStackTrace();
         }

         try {
            ServerConfig.write();
         } catch (IOException var4) {
            var4.printStackTrace();
         }

         String var1 = EngineSettings.OFFLINE_PLAYER_NAME.getCurrentState().toString().trim();
         boolean var2 = true;
         boolean var3 = false;
         if (var1.trim().length() <= 0) {
            var3 = true;
            var2 = false;
         } else if (var1.length() > 32) {
            var2 = false;
         } else if (var1.length() <= 2) {
            var2 = false;
         } else if (var1.matches("[_-]+")) {
            var2 = false;
         } else if (!var1.matches("[a-zA-Z0-9_-]+")) {
            var2 = false;
         }

         if (!var2) {
            if (!var3) {
               PlayerOkCancelInput var7;
               (var7 = new PlayerOkCancelInput("ERROR", this.getState(), 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_16, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_24) {
                  public void pressedOK() {
                     this.deactivate();
                  }

                  public void onDeactivate() {
                  }
               }).getInputPanel().setCancelButton(false);
               var7.activate();
               return;
            }

            PlayerTextInput var6;
            (var6 = new PlayerTextInput("PLAYERNAME", this.getState(), 32, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_25, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_23) {
               public void onDeactivate() {
               }

               public String[] getCommandPrefixes() {
                  return null;
               }

               public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                  return null;
               }

               public void onFailedTextCheck(String var1) {
               }

               public boolean onInput(String var1) {
                  boolean var2 = true;
                  if (var1.trim().length() <= 0) {
                     var2 = false;
                  } else if (var1.length() > 32) {
                     var2 = false;
                  } else if (var1.length() <= 2) {
                     var2 = false;
                  } else if (var1.matches("[_-]+")) {
                     var2 = false;
                  } else if (!var1.matches("[a-zA-Z0-9_-]+")) {
                     var2 = false;
                  }

                  if (!var2) {
                     PlayerOkCancelInput var4;
                     (var4 = new PlayerOkCancelInput("ERROR", this.getState(), 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_29, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_28) {
                        public void pressedOK() {
                           this.deactivate();
                        }

                        public void onDeactivate() {
                        }
                     }).getInputPanel().setCancelButton(false);
                     var4.activate();
                  } else {
                     EngineSettings.OFFLINE_PLAYER_NAME.setCurrentState(var1);
                     EngineSettings.OFFLINE_PLAYER_NAME.notifyChanged();
                     GameMainMenuController.this.startSelectedLocalGame();

                     try {
                        EngineSettings.write();
                     } catch (IOException var3) {
                        var1 = null;
                        var3.printStackTrace();
                     }
                  }

                  return var2;
               }
            }).getInputPanel().setCancelButton(false);
            var6.activate();
            return;
         }

         this.setFrame((GraphicsFrame)null, false);
         (new Thread(new Runnable() {
            public void run() {
               try {
                  Controller.getResLoader().loadAll();
               } catch (FileNotFoundException var5) {
                  var5.printStackTrace();
               } catch (ResourceException var6) {
                  var6.printStackTrace();
               } catch (ParseException var7) {
                  var7.printStackTrace();
               } catch (SAXException var8) {
                  var8.printStackTrace();
               } catch (IOException var9) {
                  var9.printStackTrace();
               } catch (ParserConfigurationException var10) {
                  var10.printStackTrace();
               }

               try {
                  ServerConfig.WORLD.setCurrentState(GameMainMenuController.this.selectedLocalUniverse.name);
                  ServerConfig.write();
               } catch (IOException var4) {
                  var4.printStackTrace();
               }

               try {
                  GameMainMenuController.this.graphicsContext.setLoadMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_10);
                  GameServerState.readDatabasePosition(false);
                  Starter.initializeServer(false);

                  try {
                     Starter.initialize(false);
                  } catch (SecurityException var2) {
                     var2.printStackTrace();
                  }

                  GameMainMenuController.this.graphicsContext.setLoadMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_11);
                  Starter.doMigration(new JDialog(), false);
               } catch (Exception var3) {
                  var3.printStackTrace();
                  String var1 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_31, var3.getClass().getSimpleName());
                  GameMainMenuController.this.graphicsContext.setLoadMessage(var1);
                  GameMainMenuController.this.graphicsContext.handleError(var1);
                  return;
               }

               GameMainMenuController.this.graphicsContext.setLoadMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_12);
               HostPortLoginName var11 = new HostPortLoginName("localhost", 4242, (byte)0, EngineSettings.OFFLINE_PLAYER_NAME.getCurrentState().toString());
               Starter.startServer(false);
               GameMainMenuController.this.graphicsContext.setLoadMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_13);
               Starter.startClient(var11, false, GameMainMenuController.this.graphicsContext);
            }
         })).start();
      }

   }

   public void deleteSelectedWorld(boolean var1) {
      if (this.selectedLocalUniverse != null) {
         this.backupAndDeleteSelected(var1, true);
      }

   }

   public void importDB(final File var1, final String var2) {
      (new Thread(new Runnable() {
         public void run() {
            try {
               GameMainMenuController.this.setFrame((GraphicsFrame)null, false);
               FileUtil.extract(var1, GameServerState.SERVER_DATABASE + var2 + File.separator, "server-database", new FolderZipper.ZipCallback() {
                  public void update(File var1x) {
                     GameMainMenuController.this.graphicsContext.setLoadMessage("Extracting " + var1x.getName() + "...");
                  }
               });
               return;
            } catch (Exception var4) {
               var4.printStackTrace();
               GameMainMenuController.this.errorDialog(var4);
            } finally {
               GameMainMenuController.this.setFrame(GameMainMenuController.this.frame, false);
               GameMainMenuController.this.setChanged();
               GameMainMenuController.this.notifyObservers();
            }

         }
      })).start();
   }

   public void backupAndDeleteSelected(final boolean var1, final boolean var2) {
      this.setFrame((GraphicsFrame)null, false);
      if (this.selectedLocalUniverse == null) {
         System.err.println("[MAINMENU] ERROR: NO UNIVERSE SELECTED");
      } else {
         (new Thread(new Runnable() {
            public void run() {
               try {
                  if (var1 && GameMainMenuController.this.selectedLocalUniverse != null) {
                     System.err.println("[CLIENT] BACKING UP " + GameMainMenuController.this.selectedLocalUniverse.name);
                     final ZipGUICallback var1x = new ZipGUICallback();
                     FolderZipper.ZipCallback var11 = new FolderZipper.ZipCallback() {
                        public void update(File var1xx) {
                           GameMainMenuController.this.graphicsContext.setLoadMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_6, GameMainMenuController.this.file, GameMainMenuController.this.maxFile, var1xx.getName()));
                           var1x.f = var1xx;
                           var1x.fileMax = GameMainMenuController.this.maxFile;
                           var1x.fileIndex = GameMainMenuController.this.file;
                           GameMainMenuController.this.file++;
                        }
                     };
                     GameMainMenuController.this.backUp(GameMainMenuController.this.selectedLocalUniverse.name, var11);
                  }

                  if (!var2) {
                     return;
                  }

                  System.err.println("[CLIENT] DELETING UNIVERSE " + GameMainMenuController.this.selectedLocalUniverse.name);
                  GameMainMenuController.this.graphicsContext.setLoadMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_7, GameMainMenuController.this.selectedLocalUniverse.name));
                  if (GameMainMenuController.this.selectedLocalUniverse.name.toLowerCase(Locale.ENGLISH).equals("old")) {
                     File[] var13;
                     int var2x = (var13 = (new FileExt(GameServerState.SERVER_DATABASE)).listFiles(new FileFilter() {
                        public boolean accept(File var1x) {
                           if (!var1x.getName().toLowerCase(Locale.ENGLISH).equals("index") && !var1x.getName().toLowerCase(Locale.ENGLISH).equals("data")) {
                              return !var1x.isDirectory();
                           } else {
                              return true;
                           }
                        }
                     })).length;

                     for(int var3 = 0; var3 < var2x; ++var3) {
                        File var4;
                        if ((var4 = var13[var3]).isDirectory()) {
                           try {
                              FileUtil.deleteRecursive(var4);
                           } catch (IOException var8) {
                              var8.printStackTrace();
                              GameMainMenuController.this.errorDialog((Exception)var8);
                           }
                        } else {
                           var4.delete();
                        }
                     }

                     return;
                  }

                  try {
                     FileUtil.deleteRecursive(new FileExt(GameServerState.SERVER_DATABASE + GameMainMenuController.this.selectedLocalUniverse.name));
                     return;
                  } catch (IOException var9) {
                     var9.printStackTrace();
                     String var12 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_32, var9.getClass().getSimpleName());
                     GameMainMenuController.this.graphicsContext.setLoadMessage(var12);
                     GameMainMenuController.this.graphicsContext.handleError(var12);
                  }
               } finally {
                  GameMainMenuController.this.setFrame(GameMainMenuController.this.frame, false);
                  GameMainMenuController.this.setChanged();
                  GameMainMenuController.this.notifyObservers();
               }

            }
         })).start();
      }
   }

   public void backUp(final String var1, FolderZipper.ZipCallback var2) {
      try {
         FileFilter var3 = new FileFilter() {
            public boolean accept(File var1x) {
               System.err.println("[BACKUP] CHECKING ZIPPING " + var1x);
               if (!var1x.getName().toLowerCase(Locale.ENGLISH).equals(var1.toLowerCase(Locale.ENGLISH)) && !var1x.getName().toLowerCase(Locale.ENGLISH).equals("index") && !var1x.getName().toLowerCase(Locale.ENGLISH).equals("data") && !var1x.getName().toLowerCase(Locale.ENGLISH).equals("server-database")) {
                  return !var1x.isDirectory();
               } else {
                  return true;
               }
            }
         };
         if (var1.equals("old")) {
            this.backUp("./", GameServerState.SERVER_DATABASE, String.valueOf(System.currentTimeMillis()), ".smdb", false, true, var3, var2);
         } else {
            this.backUp("./", GameServerState.SERVER_DATABASE + var1, var1 + String.valueOf(System.currentTimeMillis()), ".smdb", false, true, var3, var2);
         }
      } catch (IOException var4) {
         var4.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var4);
      }
   }

   public void backUp(String var1, String var2, String var3, String var4, boolean var5, boolean var6, FileFilter var7, FolderZipper.ZipCallback var8) throws IOException {
      FileExt var9;
      if ((var9 = new FileExt(var1)).exists() && var9.list().length > 0) {
         this.setChanged();
         this.notifyObservers("Backing Up");
         var3 = "backup-StarMade-" + Version.VERSION + "-" + Version.build + "_" + var3 + (!var4.startsWith(".") ? "." + var4 : var4);
         System.out.println("Backing Up (archiving files)");
         this.file = 0;
         if (var6) {
            this.maxFile = FileUtil.countFilesRecusrively((new FileExt(var1)).getAbsolutePath() + File.separator + var2);
         } else {
            this.maxFile = FileUtil.countFilesRecusrively((new FileExt(var1)).getAbsolutePath());
         }

         System.err.println("[BACKUP] Total files: " + this.maxFile);
         FileExt var12;
         if (var6) {
            if ((var12 = new FileExt(var1 + File.separator + var2 + File.separator)).exists() && var12.isDirectory()) {
               FolderZipper.zipFolder(var12.getAbsolutePath(), var3 + ".tmp", "backup-StarMade-", var8, "", var7, true);
            }
         } else {
            FolderZipper.zipFolder(var1, var3 + ".tmp", "backup-StarMade-", var8, var7);
         }

         this.setChanged();
         this.notifyObservers("resetbars");
         System.out.println("Copying Backup mFile to install dir...");
         if ((var12 = new FileExt(var3 + ".tmp")).exists()) {
            FileExt var10 = new FileExt((new FileExt(var1)).getAbsolutePath() + File.separator + var3);
            System.err.println("Copy to: " + var10.getAbsolutePath());
            DataUtil.copy(var12, var10);
            var12.delete();
         } else {
            assert false : var12.getAbsolutePath() + " doesnt exist";
         }

         if (var5) {
            this.setChanged();
            this.notifyObservers("Deleting old installation");
            System.out.println("Cleaning up current installation");
            File[] var11 = var9.listFiles();

            for(int var13 = 0; var13 < var11.length; ++var13) {
               File var14;
               if ((var14 = var11[var13]).getName().equals("data") || var14.getName().equals("native") || var14.getName().startsWith("StarMade") || var14.getName().equals("MANIFEST.MF") || var14.getName().equals("version.txt")) {
                  FileUtil.deleteDir(var14);
                  var14.delete();
               }
            }
         }

         System.out.println("[BACKUP] DONE");
      }

   }

   public void startSelectedOnlineGame() {
      if (this.getSelectedOnlineUniverse() != null) {
         String var1 = EngineSettings.ONLINE_PLAYER_NAME.getCurrentState().toString().trim();
         boolean var2 = true;
         boolean var3 = false;
         if (var1.trim().length() <= 0) {
            var2 = false;
            var3 = true;
         } else if (var1.length() > 32) {
            var2 = false;
         } else if (var1.length() <= 2) {
            var2 = false;
         } else if (var1.matches("[_-]+")) {
            var2 = false;
         } else if (!var1.matches("[a-zA-Z0-9_-]+")) {
            var2 = false;
         }

         if (!var2) {
            if (!var3) {
               PlayerOkCancelInput var6;
               (var6 = new PlayerOkCancelInput("ERROR", this.getState(), 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_27, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_17) {
                  public void pressedOK() {
                     this.deactivate();
                  }

                  public void onDeactivate() {
                  }
               }).getInputPanel().setCancelButton(false);
               var6.activate();
               return;
            }

            PlayerTextInput var5;
            (var5 = new PlayerTextInput("PLAYERNAME", this.getState(), 32, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_21, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_26) {
               public void onDeactivate() {
               }

               public String[] getCommandPrefixes() {
                  return null;
               }

               public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                  return null;
               }

               public void onFailedTextCheck(String var1) {
               }

               public boolean onInput(String var1) {
                  boolean var2 = true;
                  if (var1.trim().length() <= 0) {
                     var2 = false;
                  } else if (var1.length() > 32) {
                     var2 = false;
                  } else if (var1.length() <= 2) {
                     var2 = false;
                  } else if (var1.matches("[_-]+")) {
                     var2 = false;
                  } else if (!var1.matches("[a-zA-Z0-9_-]+")) {
                     var2 = false;
                  }

                  if (!var2) {
                     PlayerOkCancelInput var4;
                     (var4 = new PlayerOkCancelInput("ERROR", this.getState(), 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_14, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_15) {
                        public void pressedOK() {
                           this.deactivate();
                        }

                        public void onDeactivate() {
                        }
                     }).getInputPanel().setCancelButton(false);
                     var4.activate();
                  } else {
                     EngineSettings.ONLINE_PLAYER_NAME.setCurrentState(var1);
                     EngineSettings.ONLINE_PLAYER_NAME.notifyChanged();
                     GameMainMenuController.this.startSelectedOnlineGame();

                     try {
                        EngineSettings.write();
                     } catch (IOException var3) {
                        var1 = null;
                        var3.printStackTrace();
                     }
                  }

                  return var2;
               }
            }).getInputPanel().setCancelButton(false);
            var5.activate();
         }

         System.err.println("[CLIENT] starting selected online universe: " + this.getSelectedOnlineUniverse());
         EngineSettings.LAST_GAME.setCurrentState("MP;" + this.getSelectedOnlineUniverse().getHost() + ";" + this.getSelectedOnlineUniverse().getPort() + ";" + EngineSettings.ONLINE_PLAYER_NAME.getCurrentState().toString().trim());

         try {
            EngineSettings.write();
         } catch (IOException var4) {
            var4.printStackTrace();
         }

         Starter.serverInitFinished = true;
         this.setFrame((GraphicsFrame)null, false);
         (new Thread(new Runnable() {
            public void run() {
               try {
                  Starter.initialize(false);
               } catch (Exception var2) {
                  var2.printStackTrace();
                  String var1 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_30, var2.getClass().getSimpleName());
                  GameMainMenuController.this.graphicsContext.setLoadMessage(var1);
                  GameMainMenuController.this.graphicsContext.handleError(var1);
               }

               Starter.startClient(new HostPortLoginName(GameMainMenuController.this.getSelectedOnlineUniverse().getHost(), GameMainMenuController.this.getSelectedOnlineUniverse().getPort(), (byte)0, EngineSettings.ONLINE_PLAYER_NAME.getCurrentState().toString()), false, GameMainMenuController.this.graphicsContext);
            }
         })).start();
      }

   }

   public boolean hasCurrentLocalSelected() {
      return this.selectedLocalUniverse != null;
   }

   public boolean hasCurrentOnlineSelected() {
      return this.getSelectedOnlineUniverse() != null;
   }

   public void connectToCustomServer(String var1) {
      Starter.serverInitFinished = true;
      this.setFrame((GraphicsFrame)null, false);
      String[] var5;
      if ((var5 = var1.split(":")).length == 2) {
         final String var2 = var5[0];

         try {
            final int var6 = Integer.parseInt(var5[1]);
            EngineSettings.LAST_GAME.setCurrentState("MP;" + var2 + ";" + var6 + ";" + EngineSettings.ONLINE_PLAYER_NAME.getCurrentState().toString().trim());
            EngineSettings.write();
            (new Thread(new Runnable() {
               public void run() {
                  try {
                     Starter.initialize(false);
                  } catch (IOException var2x) {
                     var2x.printStackTrace();
                     GameMainMenuController.this.errorDialog((Exception)var2x);
                  }

                  Starter.startClient(new HostPortLoginName(var2, var6, (byte)0, EngineSettings.ONLINE_PLAYER_NAME.getCurrentState().toString()), false, GameMainMenuController.this.graphicsContext);
               }
            })).start();
         } catch (NumberFormatException var3) {
            var3.printStackTrace();
            this.errorDialog(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_8);
         } catch (IOException var4) {
            var4.printStackTrace();
         }
      } else {
         this.errorDialog(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_9);
      }
   }

   public void errorDialog(Exception var1) {
      this.setFrame(this.frame, false);
      this.frame.handleError(var1);
   }

   public void errorDialog(String var1) {
      throw new RuntimeException(var1);
   }

   public void startGraphics() throws FileNotFoundException, ResourceException, ParseException, SAXException, IOException, ParserConfigurationException {
      System.err.println("[CLIENT][STARTUP] Initialize Open GL");
      this.graphicsContext.initializeOpenGL(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_GAMEMAINMENUCONTROLLER_22);
      System.err.println("[CLIENT][STARTUP] Initialize Open GL Done");
      this.graphicsContext.initializeLoadingScreen(new LoadingScreenDetailed());
      System.err.println("[CLIENT][STARTUP] Initialize LoadingScreen Done. Starting up");
      this.graphicsContext.startUp();
   }

   public ServerInfo getCurrentOnlineSelected() {
      return this.getSelectedOnlineUniverse();
   }

   public void handleError(String var1) {
      this.setFrame(this.frame, false);
      this.queuedException = new RuntimeException(var1);
   }

   public void handleError(Exception var1) {
      this.setFrame(this.frame, false);
      this.queuedException = var1;
   }

   public boolean hasLastUsed() {
      return EngineSettings.LAST_GAME.getCurrentState().toString().trim().length() > 0;
   }

   public void startLastUsed() {
      ServerInfo var1 = null;
      LocalUniverse var2 = null;

      String[] var3;
      try {
         boolean var4 = (var3 = EngineSettings.LAST_GAME.getCurrentState().toString().trim().split(";"))[0].equals("SP");
         String var5 = var3[1];
         int var6 = Integer.parseInt(var3[2]);
         String var9 = var3[3];
         if (!var4) {
            var1 = new ServerInfo(var5, var6);
            EngineSettings.ONLINE_PLAYER_NAME.setCurrentState(var9);
            this.setSelectedOnlineUniverse(var1);
         } else {
            var2 = new LocalUniverse(var5, 0L);
            EngineSettings.OFFLINE_PLAYER_NAME.setCurrentState(var9);
            this.selectedLocalUniverse = var2;
         }
      } catch (Exception var8) {
         var3 = null;
         var8.printStackTrace();
         EngineSettings.LAST_GAME.setCurrentState("");

         try {
            EngineSettings.write();
         } catch (IOException var7) {
            var7.printStackTrace();
         }
      }

      if (var1 != null) {
         this.startSelectedOnlineGame();
      } else {
         if (var2 != null) {
            this.startSelectedLocalGame();
         }

      }
   }

   public String getGUIPath() {
      return "gui/menu/";
   }

   public ServerInfo getSelectedOnlineUniverse() {
      return this.selectedOnlineUniverse;
   }

   public void setSelectedOnlineUniverse(ServerInfo var1) {
      this.selectedOnlineUniverse = var1;
   }

   public LocalUniverse getSelectedLocalUniverse() {
      return this.selectedLocalUniverse;
   }

   public void loadLanguage(String var1) {
      this.frame.switchLanguage(var1);
   }

   public void setActiveSubtitles(List var1) {
   }

   public void setInTextBox(boolean var1) {
      this.inTextBox = var1;
   }

   public boolean isInTextBox() {
      return this.inTextBox;
   }

   public void mouseButtonEventNetworktransmission(MouseEvent var1) {
   }

   public void popupAlertTextMessage(String var1) {
   }
}
