package org.schema.game.client.controller;

import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.catalog.newcatalog.GUIBlueprintFillScrollableList;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.BlueprintMetaItem;
import org.schema.game.common.data.player.SimplePlayerCommands;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.server.data.EntityRequest;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUICheckBoxTextPair;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.input.InputState;

public class PlayerBlueprintMetaDialog extends PlayerGameOkCancelInput {
   private BlueprintMetaItem it;
   private boolean useOwnFaction;

   public PlayerBlueprintMetaDialog(GameClientState var1, BlueprintMetaItem var2, final Inventory var3) {
      super("PlayerBlueprintMetaDialog", var1, 600, 400, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLUEPRINTMETADIALOG_0, var2.blueprintName), "");
      this.it = var2;
      this.getInputPanel().onInit();
      this.useOwnFaction = this.getState().getPlayer().getFactionId() > 0;
      ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().setTextBoxHeightLast(25);
      ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().addNewTextBox(40);
      GUIHorizontalButtonTablePane var4;
      (var4 = new GUIHorizontalButtonTablePane(var1, 2, 1, ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().getContent(0))).onInit();
      ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().getContent(0).attach(var4);
      var4.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLUEPRINTMETADIALOG_1, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               PlayerBlueprintMetaDialog.this.pressedAll();
            }

         }

         public boolean isOccluded() {
            return !PlayerBlueprintMetaDialog.this.isActive();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return !PlayerBlueprintMetaDialog.this.getItem().metGoal();
         }
      });
      var4.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLUEPRINTMETADIALOG_2, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !PlayerBlueprintMetaDialog.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               PlayerBlueprintMetaDialog.this.pressedSpawn(var3);
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return PlayerBlueprintMetaDialog.this.getItem().metGoal();
         }
      });
      GUIBlueprintFillScrollableList var5;
      (var5 = new GUIBlueprintFillScrollableList(this.getState(), ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().getContent(1), this, this.getItem())).onInit();
      ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().getContent(1).attach(var5);
      this.getInputPanel().setCancelButton(false);
      this.getInputPanel().setOkButtonText(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLUEPRINTMETADIALOG_3);
   }

   public BlueprintMetaItem getItem() {
      return (BlueprintMetaItem)this.getState().getMetaObjectManager().getObject(this.it.getId());
   }

   public void pressedAll() {
      (new PlayerGameOkCancelInput("PlayerBlueprintMetaDialog_ADD_ALL", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLUEPRINTMETADIALOG_4, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLUEPRINTMETADIALOG_5) {
         public void onDeactivate() {
         }

         public boolean isOccluded() {
            return false;
         }

         public void pressedOK() {
            this.getState().getPlayer().sendSimpleCommand(SimplePlayerCommands.ADD_BLUEPRINT_META_ALL, PlayerBlueprintMetaDialog.this.getItem().getId());
            this.deactivate();
         }
      }).activate();
   }

   public void pressedSpawn(Inventory var1) {
      final int var2 = var1.getInventoryHolder().getId();
      final long var3 = var1.getParameter();
      PlayerGameTextInput var5;
      (var5 = new PlayerGameTextInput("PlayerBlueprintMetaDialog_SPAWN", this.getState(), 50, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLUEPRINTMETADIALOG_6, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLUEPRINTMETADIALOG_7, this.getItem().blueprintName + System.currentTimeMillis()) {
         public String[] getCommandPrefixes() {
            return null;
         }

         public void onDeactivate() {
         }

         public String handleAutoComplete(String var1, TextCallback var2x, String var3x) throws PrefixNotFoundException {
            return null;
         }

         public void onFailedTextCheck(String var1) {
         }

         public boolean isOccluded() {
            return false;
         }

         public boolean onInput(String var1) {
            if (var1.length() > 0 && EntityRequest.isShipNameValid(var1)) {
               this.getState().getPlayer().sendSimpleCommand(SimplePlayerCommands.SPAWN_BLUEPRINT_META, PlayerBlueprintMetaDialog.this.getItem().getId(), var1, var2, ElementCollection.getPosX(var3), ElementCollection.getPosY(var3), ElementCollection.getPosZ(var3), PlayerBlueprintMetaDialog.this.useOwnFaction, -1, Long.MIN_VALUE);
               PlayerBlueprintMetaDialog.this.deactivate();
               return true;
            } else {
               this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLUEPRINTMETADIALOG_8, 0.0F);
               return false;
            }
         }
      }).getInputPanel().onInit();
      GUICheckBoxTextPair var6;
      (var6 = new GUICheckBoxTextPair(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLUEPRINTMETADIALOG_9, 240, FontLibrary.getBlenderProMedium14(), 24) {
         public boolean isActivated() {
            return PlayerBlueprintMetaDialog.this.useOwnFaction;
         }

         public void deactivate() {
            PlayerBlueprintMetaDialog.this.useOwnFaction = false;
         }

         public void activate() {
            if (((GameClientState)this.getState()).getPlayer().getFactionId() > 0) {
               PlayerBlueprintMetaDialog.this.useOwnFaction = true;
            } else {
               ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLUEPRINTMETADIALOG_10, 0.0F);
               PlayerBlueprintMetaDialog.this.useOwnFaction = false;
            }
         }
      }).setPos(3.0F, 35.0F, 0.0F);
      ((GUIDialogWindow)var5.getInputPanel().background).getMainContentPane().getContent(0).attach(var6);
      var5.activate();
   }

   public boolean allowChat() {
      return true;
   }

   public boolean isOccluded() {
      return false;
   }

   public void pressedAdd(final short var1) {
      (new PlayerGameTextInput("PlayerBlueprintMetaDialog_ADD", this.getState(), 20, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLUEPRINTMETADIALOG_11, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLUEPRINTMETADIALOG_12, ElementKeyMap.getInfo(var1).getName())) {
         public boolean isOccluded() {
            return false;
         }

         public void onFailedTextCheck(String var1x) {
         }

         public String handleAutoComplete(String var1x, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public String[] getCommandPrefixes() {
            return null;
         }

         public boolean onInput(String var1x) {
            if (var1x.length() <= 0) {
               this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLUEPRINTMETADIALOG_13, 0.0F);
               return false;
            } else {
               try {
                  int var4 = Integer.parseInt(var1x.trim());
                  short var2 = var1;
                  if (ElementKeyMap.getInfoFast(var1).getSourceReference() != 0) {
                     var2 = (short)ElementKeyMap.getInfoFast(var1).getSourceReference();
                  }

                  int var5;
                  if ((var5 = this.getState().getPlayer().getInventory().getOverallQuantity(var2)) < var4) {
                     this.getTextInput().clear();
                     this.getTextInput().append(String.valueOf(var5));
                     this.getState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLUEPRINTMETADIALOG_14, var5), 0.0F);
                     return false;
                  } else {
                     this.getState().getPlayer().sendSimpleCommand(SimplePlayerCommands.ADD_BLUEPRINT_META_SINGLE, PlayerBlueprintMetaDialog.this.getItem().getId(), var1, var4);
                     return true;
                  }
               } catch (NumberFormatException var3) {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLUEPRINTMETADIALOG_15, 0.0F);
                  return false;
               }
            }
         }

         public void onDeactivate() {
         }
      }).activate();
   }

   public void onDeactivate() {
   }

   public void pressedOK() {
      this.deactivate();
   }
}
