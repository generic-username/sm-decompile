package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.mainmenu.DialogInput;

public abstract class PlayerInput extends DialogInput {
   public PlayerInput(GameClientState var1) {
      super(var1);
      this.initialize();
   }

   protected void initialize() {
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }
}
