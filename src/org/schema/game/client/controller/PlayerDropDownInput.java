package org.schema.game.client.controller;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Iterator;
import org.schema.game.client.view.gui.GUIInputDropDownPanel;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.input.InputState;

public abstract class PlayerDropDownInput extends DialogInput implements DropDownCallback {
   protected GUIInputPanel inputPanel;
   protected GUIDropDownList list;
   private GUIListElement current;
   protected FontLibrary.FontSize fontSize;

   public PlayerDropDownInput(String var1, InputState var2, Object var3, int var4) {
      super(var2);
      this.fontSize = FontLibrary.FontSize.MEDIUM;
      this.init(var1, var2, 420, 180, var3, var4, "");
      this.update(var2, var3, var4, "", (Collection)(new ObjectArrayList(1)));
   }

   public PlayerDropDownInput(String var1, InputState var2, Object var3, int var4, Object var5, Collection var6) {
      super(var2);
      this.fontSize = FontLibrary.FontSize.MEDIUM;
      this.init(var1, var2, 420, 180, var3, var4, var5);
      this.update(var2, var3, var4, var5, var6);
   }

   public PlayerDropDownInput(String var1, InputState var2, Object var3, int var4, Object var5, GUIElement... var6) {
      super(var2);
      this.fontSize = FontLibrary.FontSize.MEDIUM;
      this.init(var1, var2, 420, 180, var3, var4, var5);
      this.update(var2, var3, var4, var5, var6);
   }

   public PlayerDropDownInput(String var1, InputState var2, int var3, int var4, Object var5, int var6) {
      super(var2);
      this.fontSize = FontLibrary.FontSize.MEDIUM;
      this.init(var1, var2, var3, var4, var5, var6, "");
      this.update(var2, var5, var6, "", (Collection)(new ObjectArrayList(1)));
   }

   public PlayerDropDownInput(String var1, InputState var2, int var3, int var4, Object var5, int var6, Object var7, Collection var8) {
      super(var2);
      this.fontSize = FontLibrary.FontSize.MEDIUM;
      this.init(var1, var2, var3, var4, var5, var6, var7);
      this.update(var2, var5, var6, var7, var8);
   }

   public PlayerDropDownInput(String var1, InputState var2, int var3, int var4, Object var5, int var6, Object var7, GUIElement... var8) {
      super(var2);
      this.fontSize = FontLibrary.FontSize.MEDIUM;
      this.init(var1, var2, var3, var4, var5, var6, var7);
      this.update(var2, var5, var6, var7, var8);
   }

   public PlayerDropDownInput(String var1, InputState var2, int var3, int var4, Object var5, int var6, int var7, Object var8, GUIElement... var9) {
      super(var2);
      this.fontSize = FontLibrary.FontSize.MEDIUM;
      this.init(var1, var2, var3, var4, var5, var6, var8);
      this.update(var2, var5, var6, var8, var9);
   }

   private void init(String var1, InputState var2, int var3, int var4, Object var5, int var6, Object var7) {
      assert this.list == null;

      assert this.inputPanel == null;

      this.list = new GUIDropDownList(this.getState(), 300, var6, 200, this);
      this.inputPanel = new GUIInputDropDownPanel(var1, var2, var3, var4, this, var5, var7, this.list) {
         public void draw() {
            if (GUIElement.isNewHud()) {
               PlayerDropDownInput.this.list.dependend = ((GUIDialogWindow)PlayerDropDownInput.this.inputPanel.getBackground()).getMainContentPane().getContent(0);
            }

            assert ((GUIDialogWindow)PlayerDropDownInput.this.inputPanel.getBackground()).getMainContentPane().getContent(0) == ((GUIDialogWindow)this.getBackground()).getMainContentPane().getContent(0);

            super.draw();
         }
      };
      this.inputPanel.onInit();
      this.inputPanel.setCallback(this);
   }

   public void setDropdownYPos(int var1) {
      ((GUIInputDropDownPanel)this.inputPanel).yPos = var1;
      ((GUIInputDropDownPanel)this.inputPanel).updateList(this.list);
   }

   public void update(InputState var1, Object var2, int var3, Object var4, GUIElement... var5) {
      assert var3 > 0;

      this.list.getList().clear();
      GUIElement[] var6 = var5;
      var3 = var5.length;

      for(int var7 = 0; var7 < var3; ++var7) {
         GUIElement var8 = var6[var7];

         assert var8 != null;

         assert var8.getHeight() > 0.0F;

         GUIListElement var9 = new GUIListElement(var8, var8, var1);

         assert var9.getHeight() > 0.0F;

         this.list.getList().addWithoutUpdate(var9);
      }

      this.list.getList().updateDim();
      this.list.onListChanged();
      ((GUIInputDropDownPanel)this.inputPanel).updateList(this.list);
   }

   public void setSelectedUserPointer(Object var1) {
      ((GUIInputDropDownPanel)this.inputPanel).getDropDown().setSelectedUserPointer(var1);
   }

   public void update(InputState var1, Object var2, int var3, Object var4, Collection var5) {
      this.list.getList().clear();
      Iterator var6 = var5.iterator();

      while(var6.hasNext()) {
         GUIElement var7 = (GUIElement)var6.next();

         assert var7 != null;

         assert var7.getHeight() > 0.0F;

         GUIListElement var8 = new GUIListElement(var7, var7, var1);

         assert var8.getHeight() > 0.0F;

         this.list.getList().addWithoutUpdate(var8);
      }

      this.list.getList().updateDim();
      this.list.onListChanged();
      ((GUIInputDropDownPanel)this.inputPanel).updateList(this.list);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse() && !this.list.isExpanded()) {
         if (var1.getUserPointer().equals("OK")) {
            this.pressedOK(this.current);
         }

         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            this.cancel();
         }
      }

   }

   protected GUIListElement getSelectedValue() {
      return this.current;
   }

   public GUIInputPanel getInputPanel() {
      return this.inputPanel;
   }

   public abstract void onDeactivate();

   public void handleMouseEvent(MouseEvent var1) {
   }

   public void onSelectionChanged(GUIListElement var1) {
      this.current = var1;
   }

   public abstract void pressedOK(GUIListElement var1);

   public void setErrorMessage(String var1) {
      this.inputPanel.setErrorMessage(var1, 2000L);
   }
}
