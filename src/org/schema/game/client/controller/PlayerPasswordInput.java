package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;

public abstract class PlayerPasswordInput extends PlayerGameTextInput {
   public PlayerPasswordInput(String var1, GameClientState var2, int var3, int var4, int var5, Object var6, Object var7, String var8) {
      super(var1, var2, var3, var4, var5, var6, var7, var8);
      this.inputPanel.setDisplayAsPassword(true);
   }

   public PlayerPasswordInput(String var1, GameClientState var2, int var3, int var4, int var5, Object var6, Object var7) {
      super(var1, var2, var3, var4, var5, var6, var7);
      this.inputPanel.setDisplayAsPassword(true);
   }

   public PlayerPasswordInput(String var1, GameClientState var2, int var3, Object var4, Object var5, String var6) {
      super(var1, var2, var3, var4, var5, var6);
      this.inputPanel.setDisplayAsPassword(true);
   }

   public PlayerPasswordInput(String var1, GameClientState var2, int var3, Object var4, Object var5) {
      super(var1, var2, var3, var4, var5);
      this.inputPanel.setDisplayAsPassword(true);
   }
}
