package org.schema.game.client.controller.element.world;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.world.RemoteSegment;

public class LocalSegmentRetriever extends Thread {
   private ArrayList queue = new ArrayList();
   public static final int THREADS = 5;
   private ExecutorService executor = Executors.newFixedThreadPool(5);

   public LocalSegmentRetriever() {
      super("LocalSegmentRetriever");
      this.setDaemon(true);

      for(int var1 = 0; var1 < 5; ++var1) {
         this.executor.execute(new Runnable() {
            public void run() {
               try {
                  Thread.sleep(2000L);
               } catch (InterruptedException var1) {
                  var1.printStackTrace();
               }
            }
         });
      }

   }

   public void enqueue(LocalSegmentRequest var1) {
      synchronized(this.queue) {
         assert !this.queue.contains(var1);

         this.queue.add(var1);
         this.queue.notify();
      }
   }

   public LocalSegmentRequest getNextInQueue() {
      synchronized(this.queue) {
         while(this.queue.isEmpty()) {
            try {
               this.queue.wait();
            } catch (InterruptedException var3) {
               var3.printStackTrace();
            }
         }

         return (LocalSegmentRequest)this.queue.remove(0);
      }
   }

   public void run() {
      while(true) {
         final LocalSegmentRequest var1 = this.getNextInQueue();
         this.executor.execute(new Runnable() {
            public void run() {
               RemoteSegment var1x = var1.segment;
               boolean var3 = false;

               int var5;
               try {
                  var5 = var1.provider.requestFromLocalDB(var1x);
               } catch (IOException var4) {
                  Object var2 = null;
                  var4.printStackTrace();
                  var3 = true;
                  var5 = 0;
                  System.err.println("[CLIENT] Exception: Local cache is corrupted: REQUESTING COMPLETE PURGE");
                  ((GameClientState)var1.segment.getSegmentController().getState()).setDbPurgeRequested(true);
               }

               if (!var3 && var5 != 2) {
                  if (var5 == 1 && var1x.pos.x == 0 && var1x.pos.y == 0 && var1x.pos.z == 0) {
                     System.err.println("[CLIENT][DB-READ] WARNING 0,0,0 on " + var1.provider.getSegmentController() + " IS EMPTY");
                  }

                  var1.provider.received(var1x, var1x.getIndex());
               } else {
                  if (var3) {
                     System.err.println("[CLIENT][SEGMENTPROVIDER][WARNING] Requesting corrupt segment!! " + var1x.pos + ": " + var1x.getSegmentController());
                  } else {
                     System.err.println("[CLIENT][SEGMENTPROVIDER][WARNING] VersionOK but no data in DB! Re-Requesting segment from server!! " + var1x.pos + ": " + var1x.getSegmentController());
                  }

                  var1.provider.enqueueSynched(var1x);
               }
            }
         });
      }
   }
}
