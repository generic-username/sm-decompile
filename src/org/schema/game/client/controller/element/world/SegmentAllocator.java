package org.schema.game.client.controller.element.world;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentBuffer;
import org.schema.game.common.controller.SegmentDataManager;
import org.schema.game.common.data.world.Segment;

public class SegmentAllocator {
   private final ArrayList queue;
   private final Set createBuffer;
   private final ArrayList readySegments;
   private final ArrayList overwrittenSegments;
   Vector3i simpleToAbsolute3i = new Vector3i();
   private SegmentBuffer segmentBuffer;

   public SegmentAllocator(SegmentBuffer var1) {
      this.segmentBuffer = var1;
      this.readySegments = new ArrayList();
      this.overwrittenSegments = new ArrayList();
      this.queue = new ArrayList();
      this.createBuffer = new HashSet();
   }

   public void addSegmentBuffered(Segment var1) {
      this.readySegments.add(var1);
   }

   public void buffer(int var1, int var2, int var3) {
      this.simpleToAbsolute3i.set(var1 << 5, var2 << 5, var3 << 5);
      if (!this.isInQueue(this.simpleToAbsolute3i) && !this.isCreating(this.simpleToAbsolute3i) && !this.segmentBuffer.containsKey(this.simpleToAbsolute3i)) {
         synchronized(this.queue) {
            Vector3i var6 = new Vector3i(this.simpleToAbsolute3i);
            this.queue.add(var6);
            synchronized(this.createBuffer) {
               this.createBuffer.add(var6);
               if (this.queue.size() > SegmentDataManager.MAX_BUFFERED_DATA) {
                  this.createBuffer.remove(this.queue.remove(0));
               }
            }

            this.queue.notify();
         }
      }
   }

   public void discard(Vector3i var1) {
      synchronized(this.createBuffer) {
         this.createBuffer.remove(var1);
         this.queue.remove(var1);
      }
   }

   public Vector3i getNextQueueElement() {
      synchronized(this.queue) {
         return (Vector3i)this.queue.remove(this.queue.size() - 1);
      }
   }

   boolean isCreating(Vector3i var1) {
      return this.createBuffer.contains(var1);
   }

   boolean isInQueue(Vector3i var1) {
      return this.queue.contains(var1);
   }

   public boolean isQueueEmpty() {
      return this.queue.isEmpty();
   }

   public void overrideSegment(Segment var1) {
      this.overwrittenSegments.add(var1);
   }

   public void removeSegmentBuffered(Vector3i var1) {
      Segment var2;
      if ((var2 = this.segmentBuffer.get(var1)) != null) {
         this.overwrittenSegments.add(var2);
      }

   }
}
