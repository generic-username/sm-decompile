package org.schema.game.client.controller;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.Locale;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.ElementCategory;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUISearchBar;

public abstract class PlayerBlockCategoryDropdownInputNew extends PlayerGameDropDownInput {
   public ObjectArrayList additionalElements;
   private GUISearchBar searchBar;
   private Object info;
   private int[] numberValue;
   private GUIActivatableTextBar[] numberInputBar;
   private boolean includeMeta;
   private ObjectArrayList lTmp;

   public PlayerBlockCategoryDropdownInputNew(String var1, GameClientState var2, Object var3, ObjectArrayList var4, int var5, int var6, boolean var7) {
      super(var1, var2, 480, 180 + var5 * 30, var3, 32);
      this.lTmp = new ObjectArrayList();
      this.includeMeta = var7;
      this.additionalElements = var4;
      this.info = var3;
      this.searchBar = new GUISearchBar(var2, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKCATEGORYDROPDOWNINPUTNEW_0, ((GUIDialogWindow)this.getInputPanel().getBackground()).getMainContentPane().getContent(0), new TextCallback() {
         public String[] getCommandPrefixes() {
            return null;
         }

         public void onTextEnter(String var1, boolean var2, boolean var3) {
         }

         public void onFailedTextCheck(String var1) {
         }

         public void newLine() {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            System.err.println("INPUT CGHANGED " + var1);
            PlayerBlockCategoryDropdownInputNew.this.updateDropdown(var1);
            return var1;
         }
      });
      this.searchBar.setPos(0.0F, 24.0F, 0.0F);
      ((GUIDialogWindow)this.getInputPanel().getBackground()).getMainContentPane().getContent(0).attach(this.searchBar);
      if (var5 > 0) {
         this.numberInputBar = new GUIActivatableTextBar[var5];
         this.numberValue = new int[var5];

         for(final int var8 = 0; var8 < this.numberInputBar.length; ++var8) {
            String var9;
            switch(var8) {
            case 0:
               var9 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKCATEGORYDROPDOWNINPUTNEW_1;
               break;
            case 1:
               var9 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKCATEGORYDROPDOWNINPUTNEW_2;
               break;
            default:
               var9 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKCATEGORYDROPDOWNINPUTNEW_3;
            }

            this.numberInputBar[var8] = new GUIActivatableTextBar(var2, this.fontSize, var9, ((GUIDialogWindow)this.getInputPanel().getBackground()).getMainContentPane().getContent(0), new TextCallback() {
               public void onTextEnter(String var1, boolean var2, boolean var3) {
               }

               public void onFailedTextCheck(String var1) {
               }

               public void newLine() {
               }

               public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                  return null;
               }

               public String[] getCommandPrefixes() {
                  return null;
               }
            }, new OnInputChangedCallback() {
               public String onInputChanged(String var1) {
                  try {
                     PlayerBlockCategoryDropdownInputNew.this.numberValue[var8] = Integer.parseInt(var1.trim());
                  } catch (NumberFormatException var2) {
                  }

                  return var1;
               }
            });
            if (var6 > 0) {
               this.numberInputBar[var8].appendText(String.valueOf(var6));
            }

            this.numberInputBar[var8].setPos(0.0F, (float)(98 + var8 * 30), 0.0F);
            this.numberInputBar[var8].getTextArea().setLimit(8);
            ((GUIDialogWindow)this.getInputPanel().getBackground()).getMainContentPane().getContent(0).attach(0, this.numberInputBar[var8]);
         }
      }

      this.updateDropdown("");
   }

   public PlayerBlockCategoryDropdownInputNew(String var1, GameClientState var2, Object var3, int var4, int var5, boolean var6) {
      this(var1, var2, var3, (ObjectArrayList)null, var4, var5, var6);
   }

   private void updateDropdown(String var1) {
      this.update(this.getState(), this.info, 32, "", this.getElements(this.getState(), var1, this.additionalElements));
   }

   public boolean isOccluded() {
      return false;
   }

   public ObjectArrayList getElements(GameClientState var1, String var2, ObjectArrayList var3) {
      ObjectArrayList var4 = new ObjectArrayList();
      if (var3 != null) {
         Iterator var7 = var3.iterator();

         label34:
         while(true) {
            GUIElement var5;
            do {
               if (!var7.hasNext()) {
                  break label34;
               }
            } while((var5 = (GUIElement)var7.next()).getUserPointer() != null && var2.trim().length() != 0 && !var5.getUserPointer().toString().toLowerCase(Locale.ENGLISH).contains(var2.trim().toLowerCase(Locale.ENGLISH)));

            var4.add(var5);
         }
      }

      this.addCatRecursive(ElementKeyMap.getCategoryHirarchy(), var2, var1, var4, -1);
      String var8 = "Meta Items";
      if ((var2.trim().length() == 0 || var8.toLowerCase(Locale.ENGLISH).contains(var2.trim().toLowerCase(Locale.ENGLISH))) && this.includeMeta) {
         GUIAncor var9 = new GUIAncor(var1, 800.0F, 32.0F);
         var4.add(var9);
         GUITextOverlay var6;
         (var6 = new GUITextOverlay(500, 32, FontLibrary.getBoldArial12White(), var1)).setTextSimple(var8);
         var9.setUserPointer(MetaObject.class);
         var6.getPos().x = 3.0F;
         var6.getPos().y = 7.0F;
         var9.attach(var6);
      }

      return var4;
   }

   private void addCatRecursive(ElementCategory var1, String var2, GameClientState var3, ObjectArrayList var4, int var5) {
      this.lTmp.clear();
      var1.getInfoElementsRecursive(this.lTmp);
      boolean var6 = false;
      Iterator var7 = this.lTmp.iterator();

      while(var7.hasNext()) {
         ElementInformation var8;
         if (!(var8 = (ElementInformation)var7.next()).isDeprecated() && var8.isShoppable()) {
            var6 = true;
            break;
         }
      }

      if (var6) {
         if (var5 >= 0) {
            String var10 = var1.getCategory();
            if (var2.trim().length() == 0 || var10.toLowerCase(Locale.ENGLISH).contains(var2.trim().toLowerCase(Locale.ENGLISH))) {
               GUIAncor var11 = new GUIAncor(var3, 800.0F, 32.0F);
               var4.add(var11);
               GUITextOverlay var9;
               (var9 = new GUITextOverlay(500, 32, FontLibrary.getBoldArial12White(), var3)).setTextSimple(var10);
               var11.setUserPointer(var1);
               var9.getPos().x = (float)(3 + var5 * 5);
               var9.getPos().y = 7.0F;
               var11.attach(var9);
            }
         }

         var7 = var1.getChildren().iterator();

         while(var7.hasNext()) {
            ElementCategory var12 = (ElementCategory)var7.next();
            this.addCatRecursive(var12, var2, var3, var4, var5 + 1);
         }
      }

   }

   public void onDeactivate() {
   }

   public void pressedOK(GUIListElement var1) {
      if (var1.getContent().getUserPointer() != null && var1.getContent().getUserPointer() instanceof ElementCategory) {
         this.onOk((ElementCategory)var1.getContent().getUserPointer());
      } else if (var1.getContent().getUserPointer() == MetaObject.class) {
         this.onOkMeta();
      } else {
         this.onAdditionalElementOk(var1.getContent().getUserPointer());
      }

      this.deactivate();
   }

   public abstract void onAdditionalElementOk(Object var1);

   public abstract void onOk(ElementCategory var1);

   public abstract void onOkMeta();

   public int getNumberValue(int var1) {
      return this.numberValue[var1];
   }
}
