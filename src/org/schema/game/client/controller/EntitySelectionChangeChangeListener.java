package org.schema.game.client.controller;

import org.schema.game.common.data.world.SimpleTransformableSendableObject;

public interface EntitySelectionChangeChangeListener {
   void onEntityChanged(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2);
}
