package org.schema.game.client.view;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.lang.reflect.InvocationTargetException;
import java.nio.FloatBuffer;
import java.util.Iterator;
import java.util.Map.Entry;
import javax.vecmath.Vector3f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.cubes.CubeMeshNormal;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.GlUtil;

public class SegAABBDrawer {
   private static final int VERT_COMPO = 4;
   private static final int VERTS_PER_AABB = 24;
   private static final int FLOAT_SIZE_PER_AABB = 96;
   private static final int BYTE_SIZE_PER_AABB = 384;
   private static final boolean DEBUG = false;
   FloatBuffer helpBuffer;
   private int bufferId;
   private IntArrayList segConIds = new IntArrayList();
   private LongArrayList endPointers = new LongArrayList();
   private boolean updated;

   public static void addBox(Vector3i var0, Vector3f var1, Vector3f var2, int var3, FloatBuffer var4) {
      GlUtil.putPoint4(var4, var1.x + (float)var0.x, var1.y + (float)var0.y, var1.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var1.x + (float)var0.x, var2.y + (float)var0.y, var1.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var2.x + (float)var0.x, var2.y + (float)var0.y, var1.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var2.x + (float)var0.x, var1.y + (float)var0.y, var1.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var2.x + (float)var0.x, var1.y + (float)var0.y, var2.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var2.x + (float)var0.x, var2.y + (float)var0.y, var2.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var1.x + (float)var0.x, var2.y + (float)var0.y, var2.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var1.x + (float)var0.x, var1.y + (float)var0.y, var2.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var2.x + (float)var0.x, var1.y + (float)var0.y, var1.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var2.x + (float)var0.x, var2.y + (float)var0.y, var1.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var2.x + (float)var0.x, var2.y + (float)var0.y, var2.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var2.x + (float)var0.x, var1.y + (float)var0.y, var2.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var1.x + (float)var0.x, var1.y + (float)var0.y, var2.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var1.x + (float)var0.x, var2.y + (float)var0.y, var2.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var1.x + (float)var0.x, var2.y + (float)var0.y, var1.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var1.x + (float)var0.x, var1.y + (float)var0.y, var1.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var2.x + (float)var0.x, var2.y + (float)var0.y, var2.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var2.x + (float)var0.x, var2.y + (float)var0.y, var1.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var1.x + (float)var0.x, var2.y + (float)var0.y, var1.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var1.x + (float)var0.x, var2.y + (float)var0.y, var2.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var2.x + (float)var0.x, var1.y + (float)var0.y, var1.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var2.x + (float)var0.x, var1.y + (float)var0.y, var2.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var1.x + (float)var0.x, var1.y + (float)var0.y, var2.z + (float)var0.z, (float)var3);
      GlUtil.putPoint4(var4, var1.x + (float)var0.x, var1.y + (float)var0.y, var1.z + (float)var0.z, (float)var3);
   }

   public void generate(int var1) {
      this.bufferId = GL15.glGenBuffers();
      GlUtil.glBindBuffer(34962, this.bufferId);
      GL15.glBufferData(34962, (long)(var1 * 384), CubeMeshNormal.BUFFER_FLAG);

      try {
         if (this.helpBuffer != null) {
            GlUtil.destroyDirectByteBuffer(this.helpBuffer);
         }
      } catch (IllegalArgumentException var2) {
         var2.printStackTrace();
      } catch (IllegalAccessException var3) {
         var3.printStackTrace();
      } catch (InvocationTargetException var4) {
         var4.printStackTrace();
      } catch (SecurityException var5) {
         var5.printStackTrace();
      } catch (NoSuchMethodException var6) {
         var6.printStackTrace();
      }

      this.helpBuffer = BufferUtils.createFloatBuffer(var1 * 96);
   }

   public void draw(GameClientState var1) {
      if (this.updated) {
         GlUtil.glEnableClientState(32884);
         GlUtil.glBindBuffer(34962, this.bufferId);
         GL20.glUseProgram(0);
         GlUtil.glPushMatrix();
         long var2 = 0L;

         for(int var4 = 0; var4 < this.segConIds.size(); ++var4) {
            long var5 = this.endPointers.get(var4);
            SimpleTransformableSendableObject var7 = (SimpleTransformableSendableObject)var1.getCurrentSectorEntities().get(this.segConIds.get(var4));
            GlUtil.glPushMatrix();
            GlUtil.glMultMatrix((Transform)var7.getWorldTransformOnClient());
            GL11.glVertexPointer(4, 5126, 0, var2);
            long var8 = (var5 - var2) / 16L;
            GL11.glDrawArrays(7, 0, (int)var8);
            GlUtil.glPopMatrix();
            var2 = var5;
         }

         GlUtil.glBindBuffer(34962, 0);
         GlUtil.glPopMatrix();
         GlUtil.glDisableClientState(32884);
      }

   }

   public void update(Int2ObjectOpenHashMap var1, int var2) {
      this.helpBuffer.clear();
      this.segConIds.clear();
      this.endPointers.clear();
      long var3 = 0L;
      Iterator var8 = var1.entrySet().iterator();

      while(var8.hasNext()) {
         Entry var9;
         int var5 = (Integer)(var9 = (Entry)var8.next()).getKey();
         ObjectArrayList var10 = (ObjectArrayList)var9.getValue();

         for(int var6 = 0; var6 < var10.size(); ++var6) {
            SegmentDrawer.SAABB var7;
            addBox((var7 = (SegmentDrawer.SAABB)var10.get(var6)).position, var7.min, var7.max, var5, this.helpBuffer);
            var3 += 384L;
         }

         this.segConIds.add(var5);
         this.endPointers.add(var3);
      }

      this.helpBuffer.flip();
      GlUtil.glBindBuffer(34962, this.bufferId);
      if (this.helpBuffer.limit() != 0) {
         GL15.glBufferSubData(34962, 0L, this.helpBuffer);
      }

      GlUtil.glBindBuffer(34962, 0);
      this.updated = true;
   }
}
