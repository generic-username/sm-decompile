package org.schema.game.client.view;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map.Entry;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.common.TimeStatistics;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.cubes.CubeData;
import org.schema.game.client.view.effects.DepthBufferScene;
import org.schema.game.client.view.effects.GraphicsException;
import org.schema.game.client.view.effects.Shadow;
import org.schema.game.client.view.shader.ShadowShader;
import org.schema.game.common.controller.ShopperInterface;
import org.schema.game.common.data.element.ElementCollectionMesh;
import org.schema.game.common.data.world.LightTransformable;
import org.schema.game.common.data.world.SimpleTransformable;
import org.schema.game.common.version.Version;
import org.schema.game.server.controller.ServerSegmentProvider;
import org.schema.game.server.controller.pathfinding.AbstractPathFindingHandler;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.GraphicsStateInterface;
import org.schema.schine.graphicsengine.OculusVrHelper;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GLDebugDrawer;
import org.schema.schine.graphicsengine.core.GLException;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.Light;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.graphicsengine.shader.GammaShader;
import org.schema.schine.graphicsengine.shader.OculusDistortionShader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.SilhouetteShader2D;
import org.schema.schine.graphicsengine.shader.bloom.GaussianBloomShader;
import org.schema.schine.graphicsengine.shader.bloom.GodRayShader;
import org.schema.schine.graphicsengine.shader.targetBloom.Blurrer;
import org.schema.schine.graphicsengine.util.WorldToScreenConverter;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.input.Mouse;
import org.schema.schine.network.server.ServerProcessor;

public class MainGameGraphics extends AbstractScene implements GraphicsStateInterface {
   private static final boolean MOUSE_DRAW = false;
   private final WorldToScreenConverter worldToScreenConverter = new WorldToScreenConverter();
   private final MainGameGraphics.FromMeComp fromMeDist = new MainGameGraphics.FromMeComp();
   private boolean mouseUpdate;
   private boolean reshape;
   private GameClientState state;
   private GaussianBloomShader bloomShader;
   private FrameBufferObjects backgroundFbo;
   private SilhouetteShader2D silhouetteShader2D;
   private ShadowShader shadowShader;
   private FrameBufferObjects silhouetteFbo;
   private FrameBufferObjects silhouetteFboPlumes;
   private FrameBufferObjects foregroundFbo;
   private GodRayShader godRayShader;
   private Vector3f lightPosOnScreen = new Vector3f();
   private Vector3f secondLightPosOnScreen = new Vector3f();
   private long lastUpdate;
   private String memoryInfo = "";
   private Shadow shadow;
   private FrameBufferObjects occFBO;
   private GammaShader gammaShader;
   private Blurrer bloomPointPass;
   private float tmpScale = 0.5F;
   private boolean debug = true;
   private DepthBufferScene depthBuffer;

   public MainGameGraphics(GameClientState var1) {
      super(var1);
      this.setState(var1);
   }

   public void addPhysicsDebugDrawer() {
   }

   public void applyEngineSettings() {
   }

   public void cleanUp() {
      System.err.println("[CLIENT][GRAPHICS] cleaning up main graphics");
      ShaderLibrary.cleanUp();
      this.getState().getWorldDrawer().cleanUp();
      if (this.fbo != null) {
         this.fbo.cleanUp();
      }

      if (this.bloomShader != null) {
         this.bloomShader.cleanUp();
      }

      if (this.backgroundFbo != null) {
         this.backgroundFbo.cleanUp();
      }

      if (this.foregroundFbo != null) {
         this.foregroundFbo.cleanUp();
      }

      if (this.foregroundFbo != null) {
         this.foregroundFbo.cleanUp();
      }

      if (this.occFBO != null) {
         this.occFBO.cleanUp();
      }

      if (this.silhouetteFbo != null) {
         this.silhouetteFbo.cleanUp();
      }

      if (this.silhouetteFboPlumes != null) {
         this.silhouetteFboPlumes.cleanUp();
      }

      if (this.bloomPointPass != null) {
         this.bloomPointPass.cleanUp();
      }

      if (this.shadow != null) {
         this.shadow.cleanUp();
      }

      if (this.state.getWorldDrawer() != null) {
         this.state.getWorldDrawer().cleanUp();
      }

   }

   private void bloomPass(int var1) {
      if (this.bloomShader == null) {
         this.bloomShader = new GaussianBloomShader(this.fbo);
         this.bloomShader.initialize(this.fbo.getDepthRenderBufferId());
         this.bloomShader.setSilhouetteTexture(this.silhouetteFbo.getTexture());
         this.godRayShader = new GodRayShader();
         ShaderLibrary.godRayShader.setShaderInterface(this.godRayShader);
      }

      this.fbo.enable();
      GL11.glClear(16640);
      GlUtil.glDisable(3042);
      GlUtil.glDisable(2929);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFuncSeparate(770, 771, 1, 771);

      try {
         if (!this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getMapControlManager().isTreeActive()) {
            this.getState().getWorldDrawer().getStarSky().drawBackground();
         }
      } catch (GLException var3) {
         var3.printStackTrace();
      }

      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      if (!this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getMapControlManager().isTreeActive()) {
         this.backgroundFbo.draw(0);
      }

      GlUtil.glDisable(3042);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(1, 771);
      this.foregroundFbo.draw(0);
      this.fbo.disable();
      this.state.getWorldDrawer().drawAdditional(this.foregroundFbo, this.fbo, this.depthBuffer);
      if (var1 != 0) {
         if (var1 == 2) {
            this.occFBO.enable();
            GL11.glClear(16640);
            this.occFBO.disable();
         }

         this.bloomShader.draw(this.occFBO, var1);
      } else {
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         this.foregroundFbo.enable();
         GL11.glClear(16640);
         this.foregroundFbo.disable();
         this.bloomShader.draw(this.foregroundFbo, 0);
         GlUtil.glDisable(3042);
         this.fbo.enable();
         GL11.glClear(16640);
         this.foregroundFbo.draw(var1);
         this.fbo.disable();
      }

      if (var1 != 0) {
         this.occFBO.enable();
         GLFrame.getWidth();
         GLFrame.getHeight();
         OculusVrHelper.getScaleFactor();
         OculusVrHelper.getScaleFactor();
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(1, 1);
         this.godRayShader.tint.set(this.state.getWorldDrawer().sunColor);
         if (!this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getMapControlManager().isTreeActive()) {
            this.fbo.draw(ShaderLibrary.godRayShader, var1);
         }

         GlUtil.glDisable(3042);
         this.occFBO.disable();
         if (var1 == 1) {
            GL11.glClear(16640);
            OculusDistortionShader var4;
            (var4 = new OculusDistortionShader()).setFBO(2);
            ShaderLibrary.ocDistortion.setShaderInterface(var4);
            this.occFBO.drawOC(ShaderLibrary.ocDistortion, 0);
            var4.setFBO(1);
            this.occFBO.drawOC(ShaderLibrary.ocDistortion, 0);
         }
      } else {
         FrameBufferObjects var2 = this.bloomPointPass.blur(this.fbo);
         this.godRayShader.lightPosOnScreen.set(this.lightPosOnScreen);
         this.godRayShader.update();
         this.godRayShader.tint.set(this.state.getWorldDrawer().sunColor);
         this.godRayShader.setSilouetteTexId(this.silhouetteFbo.getTexture());
         this.godRayShader.setScene(var2);
         this.fbo.enable();
         GL11.glClear(16640);
         if (!this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getMapControlManager().isTreeActive()) {
            var2.draw(ShaderLibrary.godRayShader, var1);
         } else {
            var2.draw(var1);
         }

         this.fbo.disable();
         this.fbo.draw(var1);
         this.getState().getWorldDrawer().currentFBO = this.fbo;
      }

      GlUtil.glBlendFunc(770, 771);
      this.getState().getWorldDrawer().drawGUI();
      this.getState().getWorldDrawer().currentFBO = null;
   }

   private void createFrameBuffers() {
      int var1 = GLFrame.getWidth();
      int var2 = GLFrame.getHeight();
      if (this.fbo != null) {
         this.fbo.cleanUp();
      }

      GlUtil.printGlErrorCritical();
      if (this.backgroundFbo != null) {
         this.backgroundFbo.cleanUp();
      }

      GlUtil.printGlErrorCritical();
      if (this.foregroundFbo != null) {
         this.foregroundFbo.cleanUp();
      }

      GlUtil.printGlErrorCritical();
      if (this.silhouetteFbo != null) {
         this.silhouetteFbo.cleanUp();
      }

      if (this.silhouetteFboPlumes != null) {
         this.silhouetteFboPlumes.cleanUp();
      }

      if (this.occFBO != null) {
         this.occFBO.cleanUp();
      }

      GlUtil.printGlErrorCritical();
      if (EngineSettings.G_SHADOWS.isOn() && this.shadow == null) {
         this.shadow = new Shadow(this.getState());
         this.shadow.init();
      }

      GlUtil.printGlErrorCritical();
      this.fbo = new FrameBufferObjects("MAIN", var1, var2);
      this.backgroundFbo = new FrameBufferObjects("Background", var1, var2);
      this.foregroundFbo = new FrameBufferObjects("Foreground", var1, var2);
      this.silhouetteFbo = new FrameBufferObjects("Silhouette", var1 / 2, var2 / 2);
      this.silhouetteFboPlumes = new FrameBufferObjects("SilhouettePlums", var1, var2);
      if (this.silhouetteShader2D == null) {
         this.silhouetteShader2D = new SilhouetteShader2D();
      }

      if (this.shadowShader == null) {
         this.shadowShader = new ShadowShader();
      }

      if (EngineSettings.O_OCULUS_RENDERING.isOn()) {
         this.occFBO = new FrameBufferObjects("Occulus", var1, var2);
      }

      if (this.bloomPointPass != null) {
         this.bloomPointPass.cleanUp();
      }

      this.bloomPointPass = new Blurrer(this.silhouetteFboPlumes, 0);
   }

   public static boolean isFBOOn() {
      return EngineSettings.F_FRAME_BUFFER.isOn() || EngineSettings.F_BLOOM.isOn() || EngineSettings.G_SHADOWS.isOn();
   }

   public void draw() {
      this.getState().getWorldDrawer().onStartFrame();
      boolean var1 = isFBOOn();
      if (Controller.getResLoader().isLoaded() && this.getState().isReady()) {
         if (this.firstDraw) {
            this.onInit();
            this.firstDraw = false;
            this.reshape = false;
         }

         if (this.reshape) {
            if (var1) {
               this.initializeFrameBuffers();
            }

            this.reshape = false;
         }

         if (EngineSettings.G_SHADER_RELOAD.isOn()) {
            System.err.println("RECOMPILING SHADERS");
            ShaderLibrary.reCompile(Keyboard.isKeyDown(42));

            try {
               EngineSettings.G_SHADER_RELOAD.switchSetting();
            } catch (StateParameterNotFoundException var4) {
               var4.printStackTrace();
            }
         }

         GL11.glDepthFunc(515);
         if (!AbstractSceneNode.isMirrorMode() && !EngineSettings.O_OCULUS_RENDERING.isOn()) {
            this.initProjection(0);
         }

         if (!EngineSettings.O_OCULUS_RENDERING.isOn()) {
            this.getWorldToScreenConverter().storeCurrentModelviewProjection();
            this.getWorldToScreenConverter().convert(this.state.getScene().getLight().getPos(), this.lightPosOnScreen, false);
            if (this.getState().getWorldDrawer().secondSunLightPos != null) {
               this.getWorldToScreenConverter().convert(this.getState().getWorldDrawer().secondSunLightPos, this.secondLightPosOnScreen, false);
            }
         }

         if (var1) {
            try {
               if (EngineSettings.O_OCULUS_RENDERING.isOn()) {
                  this.initProjection(2);
                  this.getWorldToScreenConverter().storeCurrentModelviewProjection();
                  this.getWorldToScreenConverter().convert(this.state.getScene().getLight().getPos(), this.lightPosOnScreen, false);
                  if (this.getState().getWorldDrawer().secondSunLightPos != null) {
                     this.getWorldToScreenConverter().convert(this.getState().getWorldDrawer().secondSunLightPos, this.secondLightPosOnScreen, false);
                  }

                  this.framebufferPass(2);
                  this.initProjection(1);
                  this.getWorldToScreenConverter().storeCurrentModelviewProjection();
                  this.getWorldToScreenConverter().convert(this.state.getScene().getLight().getPos(), this.lightPosOnScreen, false);
                  if (this.getState().getWorldDrawer().secondSunLightPos != null) {
                     this.getWorldToScreenConverter().convert(this.getState().getWorldDrawer().secondSunLightPos, this.secondLightPosOnScreen, false);
                  }

                  this.framebufferPass(1);
               } else {
                  this.framebufferPass(0);
               }
            } catch (GraphicsException var3) {
               var3.printStackTrace();
               EngineSettings.F_FRAME_BUFFER.setCurrentState(false);

               try {
                  EngineSettings.write();
               } catch (IOException var2) {
                  var2.printStackTrace();
               }

               this.normalPass();
            }
         } else {
            this.normalPass();
         }

         this.getState().getWorldDrawer().retrieveMousePosition();
         TimeStatistics.reset("INFO-DRAW");
         GlUtil.glPushMatrix();
         this.drawInfo();
         GlUtil.glPopMatrix();
         TimeStatistics.set("INFO-DRAW");
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         this.getState().getWorldDrawer().onEndFrame();
      }
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      ShaderLibrary.loadShaders();
      if (isFBOOn()) {
         this.createFrameBuffers();
         this.initializeFrameBuffers();
      }

      this.depthBuffer = new DepthBufferScene(this.state);
      this.gammaShader = new GammaShader();
      this.getState().getPhysics().getDynamicsWorld().setDebugDrawer(new GLDebugDrawer());
      this.addPhysicsDebugDrawer();
      Controller.getResLoader().getMesh("Box").setStaticMesh(true);
      this.getState().getWorldDrawer().onInit();
   }

   public void drawScene() {
      GlUtil.glPushMatrix();
      if (this.getState().getCurrentPlayerObject() != null) {
         this.fromMeDist.where = this.getState().getCurrentPlayerObject().getWorldTransform().origin;
         Collections.sort(this.state.spotlights, this.fromMeDist);
         Iterator var1 = spotLights.iterator();

         while(var1.hasNext()) {
            ((Light)var1.next()).cleanUp();
         }

         spotLights.clear();

         for(int var5 = 0; var5 < this.state.spotlights.size() && var5 < 4; ++var5) {
            LightTransformable var2;
            Light var3;
            if ((var3 = (var2 = (LightTransformable)this.state.spotlights.get(var5)).getLight()) == null) {
               var3 = new Light();
               var2.setLight(var3);
            } else {
               var3.reassign();
            }

            Vector3f var4 = var2.getWorldTransformOnClient().origin;
            var3.setPos(var4.x, var4.y, var4.z);
            if (var2.getOwnerState() != null && (var2.getOwnerState() != this.state.getPlayer() || !KeyboardMappings.FREE_CAM.isDown(this.state))) {
               var2.getOwnerState().getForward(var3.spotDirection);
               Vector3f var7;
               (var7 = var2.getOwnerState().getUp(new Vector3f())).scale(0.25F);
               var3.spotUp.set(var7);
            }

            var3.getPos().add(var3.spotUp);
            var3.setSpecular(new Vector4f(0.3F, 0.3F, 0.3F, 1.0F));
            var3.setDiffuse(new Vector4f(0.2F, 0.2F, 0.2F, 1.0F));
            var3.setShininess(new float[]{8.0F});
            var3.quadAttenuation = 0.05F;
            spotLights.add(var3);
            var3.draw();
            var3.deactivate();
         }
      }

      this.getMainLight().draw();
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      TimeStatistics.reset("WORLD-DRAW");
      this.getState().getWorldDrawer().shadow = this.shadow;
      this.getState().getWorldDrawer().draw();
      TimeStatistics.set("WORLD-DRAW");
      if ((this.state.getScene().getFbo() == null || !this.state.getScene().getFbo().isEnabled()) && EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
         GlUtil.glDisable(2896);
         GlUtil.glEnable(2929);
         GlUtil.glMatrixMode(5889);
         GlUtil.glPushMatrix();
         float var6 = (float)GLFrame.getWidth() / (float)GLFrame.getHeight();
         if (!EngineSettings.O_OCULUS_RENDERING.isOn()) {
            GlUtil.gluPerspective(Controller.projectionMatrix, (Float)EngineSettings.G_FOV.getCurrentState() * AbstractScene.getZoomFactorForRender(!this.getState().getWorldDrawer().getGameMapDrawer().isMapActive()), var6, 0.05F, this.state.getSectorSize() * 7.0F, true);
         } else {
            Matrix4f var8 = GlUtil.gluPerspective(Controller.projectionMatrix, (Float)EngineSettings.G_FOV.getCurrentState() * AbstractScene.getZoomFactorForRender(!this.getState().getWorldDrawer().getGameMapDrawer().isMapActive()), OculusVrHelper.getAspectRatio(), 0.05F, this.state.getSectorSize() * 7.0F, true);
            Matrix4f.mul(Controller.occulusProjMatrix, var8, var8);
            GlUtil.glLoadMatrix(var8);
         }

         GlUtil.glMatrixMode(5888);
         DebugDrawer.drawBoundingBoxes();
         DebugDrawer.drawBoundingXses();
         DebugDrawer.drawPoints();
         DebugDrawer.drawBoxes();
         DebugDrawer.drawLines();
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GlUtil.glMatrixMode(5889);
         GlUtil.glPopMatrix();
         GlUtil.glMatrixMode(5888);
      }

      zSortedMap.clear();
      GlUtil.glPopMatrix();
   }

   public FrameBufferObjects getFbo() {
      return this.fbo;
   }

   public Light getLight() {
      return this.getMainLight();
   }

   public void reshape() {
   }

   public void update(Timer var1) {
      System.currentTimeMillis();
      boolean var2 = EngineSettings.S_INFO_DRAW.getCurrentState().equals("SOME_INFO") || EngineSettings.S_INFO_DRAW.getCurrentState().equals("ALL_INFO");
      infoList.add(0, "v" + Version.VERSION + "   [FPS] " + var1.getFps() + (var2 ? " (" + StringTools.formatPointZeroZero(var1.lastDrawMilli) + "ms)" : ""));
      infoList.add(1, "[PING] " + this.state.getPing());
      if (GameClientState.freeMemory / 1024L / 1024L < 10L) {
         this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINGAMEGRAPHICS_1, 0.0F);
      }

      if (System.currentTimeMillis() - this.lastUpdate > 900L) {
         this.memoryInfo = "[MEMORY] free: " + StringTools.formatPointZero((double)(GameClientState.freeMemory / 1024L) / 1024.0D) + "mb, taken: " + StringTools.formatPointZero((double)(GameClientState.takenMemory / 1024L) / 1024.0D) + "mb, total " + StringTools.formatPointZero((double)(GameClientState.totalMemory / 1024L) / 1024.0D) + "mb; " + GLFrame.currentMemString;
         this.lastUpdate = System.currentTimeMillis();
      }

      infoList.add(2, this.memoryInfo + " (ECM: " + ElementCollectionMesh.meshesInUse + ")");
      if (EngineSettings.S_INFO_DRAW.getCurrentState().equals("ALL_INFO")) {
         infoList.add(3, "[CL SEG A/F; D; [VRAM-save]] " + GameClientState.lastAllocatedSegmentData + " / " + GameClientState.lastFreeSegmentData + " (CMI: " + SegmentDrawer.dataPool.stats() + "; sc" + this.state.getWorldDrawer().getSegmentDrawer().getSegmentControllers().size() + "; q" + this.state.getWorldDrawer().getSegmentDrawer().getQueueSize() + ") " + GameClientState.drawnSegements + "; [VBO: " + GameClientState.realVBOSize / 1024 / 1024 + "mb] " + CubeData.drawnMeshes + "; Res: " + StringTools.formatPointZero((float)GameClientState.dataReceived / 1000000.0F) + "MB; avgL: " + StringTools.formatPointZero(GameClientState.avgBlockLightTime) + "; occ: " + SegmentOcclusion.occluded + "/" + SegmentOcclusion.total + "[f: " + SegmentOcclusion.failed + "]");
      } else {
         infoList.add(3, "[CL SEG] " + GameClientState.lastAllocatedSegmentData + " / " + GameClientState.lastFreeSegmentData + " (CMI: " + SegmentDrawer.dataPool.stats() + "; sc" + this.state.getWorldDrawer().getSegmentDrawer().getSegmentControllers().size() + "; q" + this.state.getWorldDrawer().getSegmentDrawer().getQueueSize() + ")");
      }

      if (EngineSettings.S_INFO_DRAW.getCurrentState().equals("ALL_INFO")) {
         infoList.add(4, "[SE SEG] " + GameServerState.lastAllocatedSegmentData + " / " + GameServerState.lastFreeSegmentData + "; " + ServerProcessor.totalPackagesQueued + "; " + GameServerState.activeSectorCount + "/" + GameServerState.totalSectorCount + "; REQ: " + GameServerState.segmentRequestQueue + "; DC: " + GameServerState.totalDockingChecks + "; Res: " + (float)GameServerState.dataReceived / 1000000.0F + "MB // PathCalc: " + AbstractPathFindingHandler.path_in_calc + "; " + AbstractPathFindingHandler.currentIt);
      } else {
         infoList.add(4, "[CHUNK loaded/pool; SECTOR active/total;] " + GameServerState.lastAllocatedSegmentData + " / " + GameServerState.lastFreeSegmentData + "; " + GameServerState.activeSectorCount + "/" + GameServerState.totalSectorCount + "; \nfirstStageCalls: nonemptypretest: " + ServerSegmentProvider.firstStages + ", empty: " + ServerSegmentProvider.predeteminedEmpty);
      }

      if (EngineSettings.S_INFO_DRAW.getCurrentState().equals("ALL_INFO")) {
         infoList.add("[SHOPS] NEAREST: " + this.state.getCurrentClosestShop() + "; ");
         if (this.state.getCurrentPlayerObject() != null && this.state.getCurrentPlayerObject() instanceof ShopperInterface) {
            infoList.add("[SHOPS] IN RANGE: " + ((ShopperInterface)this.state.getCurrentPlayerObject()).getShopsInDistance() + "; ");
         }
      }

      GameClientState.drawnSegements = 0;
      CubeData.drawnMeshes = 0;
      Iterator var4 = TimeStatistics.timer.entrySet().iterator();

      while(var4.hasNext()) {
         Entry var3 = (Entry)var4.next();
         infoList.add((String)var3.getKey() + ": " + (float)(Long)var3.getValue());
      }

      if (Controller.getCamera() != null) {
         infoList.add("++CAM:   " + Controller.getCamera().getClass().getSimpleName() + ": " + Controller.getCamera().getPos());
      }

      if (Mouse.isButtonDown(0)) {
         this.tmpScale = Math.min(1.0F, this.tmpScale + var1.getDelta() * 0.07F);
      }

      if (Mouse.isButtonDown(1)) {
         this.tmpScale = Math.max(0.0F, this.tmpScale - var1.getDelta() * 0.07F);
      }

      this.getState().getWorldDrawer().update(var1);
   }

   public static boolean drawBloomedEffects() {
      return EngineSettings.PLUME_BLOOM.isOn() && isFBOOn();
   }

   private void framebufferPass(int var1) throws GraphicsException {
      GL11.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
      GL11.glClear(16640);
      GL11.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
      if (this.fbo == null || GraphicsContext.isScreenSettingChanging()) {
         this.createFrameBuffers();
         this.initializeFrameBuffers();
      }

      if (EngineSettings.G_SHADOWS.isOn()) {
         if (this.shadow == null || GraphicsContext.isScreenSettingChanging()) {
            this.shadow = new Shadow(this.getState());
            this.shadow.init();
         }

         GlUtil.glPushMatrix();
         this.getMainLight().draw();
         this.shadow.makeShadowMap(this.getState().getGraphicsContext().timer);
         GlUtil.glPopMatrix();
      }

      try {
         this.getState().getWorldDrawer().getStarSky().createField();
         GlUtil.glEnable(2929);
         GlUtil.glEnable(2884);
      } catch (GLException var4) {
         var4.printStackTrace();
      }

      this.backgroundFbo.enable();
      GL11.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
      GL11.glClear(16640);
      if (!this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getMapControlManager().isTreeActive()) {
         this.getState().getWorldDrawer().getStarSky().drawStars(false);
         this.getState().getWorldDrawer().getStarSky().drawStars(true);
      }

      this.backgroundFbo.disable();
      this.depthBuffer.createDepthTexture();
      this.foregroundFbo.enable();
      GL11.glClear(16640);
      TimeStatistics.reset("SCENE-DRAW");
      this.getState().getWorldDrawer().currentFBO = this.foregroundFbo;
      this.drawScene();
      TimeStatistics.set("SCENE-DRAW");
      this.foregroundFbo.disable();
      if (drawBloomedEffects()) {
         this.silhouetteFboPlumes.enable();
         GL11.glClear(16384);
         GlUtil.glDisable(3042);
         GlUtil.glEnable(2929);
         GL11.glDepthMask(true);
         GL11.glDepthRange(0.0D, 1.0D);
         ShaderLibrary.silhouetteAlpha.load();
         if (!this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getMapControlManager().isTreeActive()) {
            this.getState().getWorldDrawer().prepareCamera();
            this.getState().getWorldDrawer().getPlumAndMuzzleDrawer().drawRaw();
            this.getState().getWorldDrawer().getFlareDrawerManager().drawRaw();
            this.getState().getWorldDrawer().drawProjectiles(1.0F);
            this.getState().getWorldDrawer().drawPointExplosions();
            this.getState().getWorldDrawer().getBeamDrawerManager().draw(1.0F);
            GlUtil.glMatrixMode(5889);
            GlUtil.glPopMatrix();
            GlUtil.glMatrixMode(5888);
         }

         this.silhouetteFboPlumes.disable();
      }

      this.debugCheck();
      ShaderLibrary.silhouetteShader2D.setShaderInterface(this.silhouetteShader2D);
      this.debugCheck();
      this.silhouetteFbo.enable();
      GL11.glClear(16640);
      this.silhouetteShader2D.setTextureId(this.foregroundFbo.getTexture());
      this.debugCheck();
      this.silhouetteShader2D.color.set(1.0F, 1.0F, 1.0F, 1.0F);
      ShaderLibrary.silhouetteShader2D.load();
      if (!this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getMapControlManager().isTreeActive()) {
         this.getState().getWorldDrawer().drawSunBallForGodRays();
      }

      this.debugCheck();
      ShaderLibrary.silhouetteShader2D.unload();
      this.debugCheck();
      GlUtil.glEnable(3042);
      this.debugCheck();
      GlUtil.glBlendFunc(770, 771);
      this.debugCheck();
      this.silhouetteShader2D.color.set(0.0F, 0.0F, 0.0F, 1.0F);
      this.debugCheck();
      ShaderLibrary.silhouetteShader2D.load();
      this.debugCheck();
      this.foregroundFbo.draw(0);
      this.debugCheck();
      ShaderLibrary.silhouetteShader2D.unload();
      this.debugCheck();
      GlUtil.glDisable(3042);
      this.debugCheck();
      this.silhouetteFbo.disable();
      this.debugCheck();
      if (EngineSettings.G_SHADOWS.isOn() && (EngineSettings.G_SHADOW_DISPLAY_SHADOW_MAP.isOn() || this.getState().isDebugKeyDown() && Keyboard.isKeyDown(12))) {
         this.shadow.showDepthTex();
      } else if (EngineSettings.F_BLOOM.isOn()) {
         this.debugCheck();
         this.bloomPass(var1);
         this.debugCheck();
      } else {
         this.getState().getWorldDrawer().currentFBO = this.fbo;
         this.fbo.enable();
         GL11.glClear(16640);
         GlUtil.glDisable(2929);
         GlUtil.glEnable(3042);
         GlUtil.glBlendFuncSeparate(770, 771, 1, 771);

         try {
            if (!this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getMapControlManager().isTreeActive()) {
               GlUtil.glPushMatrix();
               this.getState().getWorldDrawer().getStarSky().drawBackground();
               GlUtil.glPopMatrix();
            }
         } catch (GLException var3) {
            var3.printStackTrace();
         }

         if (!this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getMapControlManager().isTreeActive()) {
            this.getState().getWorldDrawer().getStarSky().drawStars(false);
            this.getState().getWorldDrawer().getStarSky().drawStars(true);
         }

         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(1, 771);
         GlUtil.glBlendFuncSeparate(1, 771, 1, 1);
         this.debugCheck();
         this.foregroundFbo.draw(0);
         this.debugCheck();
         GlUtil.glDisable(3042);
         this.fbo.disable();
         this.state.getWorldDrawer().drawAdditional(this.foregroundFbo, this.fbo, this.depthBuffer);
         if (var1 != 0) {
            this.occFBO.enable();
            if (var1 == 2) {
               GL11.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
            }

            GLFrame.getWidth();
            GLFrame.getHeight();
            OculusVrHelper.getScaleFactor();
            OculusVrHelper.getScaleFactor();
            this.debugCheck();
            this.fbo.draw(var1);
            this.debugCheck();
            this.occFBO.disable();
            this.debugCheck();
            if (var1 == 1) {
               OculusDistortionShader var5;
               (var5 = new OculusDistortionShader()).setFBO(2);
               ShaderLibrary.ocDistortion.setShaderInterface(var5);
               this.occFBO.drawOC(ShaderLibrary.ocDistortion, 0);
               var5.setFBO(1);
               this.occFBO.drawOC(ShaderLibrary.ocDistortion, 0);
            }
         } else {
            assert this.bloomPointPass != null;

            this.debugCheck();
            FrameBufferObjects var2 = this.bloomPointPass.blur(this.fbo);
            this.debugCheck();
            this.getState().getWorldDrawer().currentFBO = var2;
            ShaderLibrary.gamma.setShaderInterface(this.gammaShader);
            ShaderLibrary.gamma.load();
            if (org.lwjgl.input.Keyboard.isKeyDown(87)) {
               this.silhouetteFboPlumes.draw(var1);
            } else {
               var2.draw(var1);
            }

            ShaderLibrary.gamma.unload();
         }

         this.getState().getWorldDrawer().drawGUI();
      }
   }

   public GameClientState getState() {
      return this.state;
   }

   public void debugCheck() {
      if (this.debug) {
         GlUtil.printGlErrorCritical();
      }

   }

   public void setState(GameClientState var1) {
      this.state = var1;
   }

   public WorldToScreenConverter getWorldToScreenConverter() {
      return this.worldToScreenConverter;
   }

   private void initializeFrameBuffers() {
      try {
         this.fbo.setWithDepthAttachment(false);
         this.fbo.initialize();
         this.backgroundFbo.setReuseDepthBuffer(this.fbo.getDepthRenderBufferId());
         this.backgroundFbo.initialize();
         this.silhouetteFbo.setReuseDepthBuffer(this.fbo.getDepthRenderBufferId());
         this.silhouetteFbo.initialize();
         this.foregroundFbo.setWithDepthTexture(true);
         this.foregroundFbo.multisampled = GraphicsContext.MULTISAMPLES > 0;
         this.foregroundFbo.initialize();

         assert this.foregroundFbo.getDepthTextureID() != 0;

         this.silhouetteFboPlumes.setReuseDepthTexture(this.foregroundFbo.getDepthTextureID());
         this.silhouetteFboPlumes.initialize();

         assert this.silhouetteFboPlumes.getDepthTextureID() == this.foregroundFbo.getDepthTextureID();

         if (this.occFBO != null) {
            this.occFBO.initialize();
         }

         this.bloomPointPass.setReuseRenderDepthBuffer(this.fbo.getDepthRenderBufferId());
         this.bloomPointPass.initialize();
         GlUtil.printGlErrorCritical();
      } catch (GLException var1) {
         var1.printStackTrace();
      }
   }

   private void normalPass() {
      GL11.glClear(16640);
      this.getState().getWorldDrawer().getStarSky().draw();
      GL11.glClear(256);
      TimeStatistics.reset("SCENE-DRAW");
      this.drawScene();
      TimeStatistics.set("SCENE-DRAW");
      this.getState().getWorldDrawer().drawGUI();
   }

   public void println(String var1) {
      System.err.println(var1);
   }

   public void updateCurrentCamera(Timer var1) {
      this.debug = Keyboard.isKeyDown(60);
      Controller.getCamera().update(var1, false);
   }

   class FromMeComp implements Comparator {
      Vector3f where;
      Vector3f aTmp;
      Vector3f bTmp;

      private FromMeComp() {
         this.aTmp = new Vector3f();
         this.bTmp = new Vector3f();
      }

      public int compare(SimpleTransformable var1, SimpleTransformable var2) {
         this.aTmp.sub(var1.getWorldTransformOnClient().origin, this.where);
         this.bTmp.sub(var2.getWorldTransformOnClient().origin, this.where);
         return Float.compare(this.aTmp.lengthSquared(), this.bTmp.lengthSquared());
      }

      // $FF: synthetic method
      FromMeComp(Object var2) {
         this();
      }
   }
}
