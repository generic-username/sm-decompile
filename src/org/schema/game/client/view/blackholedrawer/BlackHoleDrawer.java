package org.schema.game.client.view.blackholedrawer;

import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class BlackHoleDrawer implements Drawable, Shaderable {
   private float time;
   private int skyTex;
   private int skyNormal;
   private float sharp;
   private float cover;
   private Mesh planetMesh;

   public void cleanUp() {
   }

   public void draw() {
      GlUtil.glPushMatrix();
      GlUtil.translateModelview(AbstractScene.mainLight.getPos());
      GlUtil.scaleModelview(10.0F, 10.0F, 10.0F);
      GlUtil.glEnable(2884);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      ShaderLibrary.blackHoleShader.setShaderInterface(this);
      ShaderLibrary.blackHoleShader.load();
      this.planetMesh.draw();
      ShaderLibrary.blackHoleShader.unload();
      GlUtil.glDisable(3042);
      GlUtil.glPopMatrix();
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.planetMesh = (Mesh)Controller.getResLoader().getMesh("BlackHole").getChilds().iterator().next();
      this.setSharp(5.0E-7F);
      this.setCover(0.7F);
      this.skyTex = Controller.getResLoader().getSprite("detail").getMaterial().getTexture().getTextureId();
      this.skyNormal = Controller.getResLoader().getSprite("detail_normal").getMaterial().getTexture().getTextureId();
   }

   public float getCover() {
      return this.cover;
   }

   public void setCover(float var1) {
      this.cover = var1;
   }

   public float getSharp() {
      return this.sharp;
   }

   public void setSharp(float var1) {
      this.sharp = var1;
   }

   public void onExit() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33985);
      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33984);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderFloat(var1, "cSharp", this.getSharp());
      GlUtil.updateShaderFloat(var1, "cCover", this.getCover());
      GlUtil.updateShaderFloat(var1, "cMove", this.time);
      GlUtil.updateShaderVector4f(var1, "lightPos", AbstractScene.mainLight.getPos().x, AbstractScene.mainLight.getPos().y, AbstractScene.mainLight.getPos().z, 1.0F);
      GlUtil.glActiveTexture(33984);
      GlUtil.glEnable(3553);
      GlUtil.glBindTexture(3553, this.skyTex);
      GlUtil.glActiveTexture(33985);
      GlUtil.glEnable(3553);
      GlUtil.glBindTexture(3553, this.skyNormal);
      GlUtil.glActiveTexture(33986);
      GlUtil.updateShaderInt(var1, "tex", 0);
      GlUtil.updateShaderInt(var1, "nmap", 1);
   }

   public void update(Timer var1) {
      this.time += var1.getDelta() * 0.07F;
      if (this.time > 1.0F) {
         this.time = (float)((double)this.time - Math.floor((double)this.time));
      }

   }
}
