package org.schema.game.client.view.mainmenu;

import java.io.IOException;
import org.schema.common.ParseException;
import org.schema.game.client.controller.GameMainMenuController;
import org.schema.game.client.view.gui.options.newoptions.OptionsPanelNew;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public class MainMenuOptionsMenu extends MainMenuInputDialog implements GUIActiveInterface {
   private OptionsPanelNew optionPanel;

   public MainMenuOptionsMenu(GameMainMenuController var1) {
      super(var1);
      this.optionPanel = new OptionsPanelNew(var1, this) {
         private int sWidth = GLFrame.getWidth();
         private int sHeight = GLFrame.getHeight();

         public void pressedOk() {
            MainMenuOptionsMenu.this.applyGameSettings((MainMenuOptionsMenu)this.activeInterface, true);
         }

         public void pressedCancel() {
            EngineSettings.read();
            KeyboardMappings.read();
         }

         public void pressedApply() {
            MainMenuOptionsMenu.this.applyGameSettings((MainMenuOptionsMenu)this.activeInterface, false);
         }

         public void draw() {
            if (this.sWidth != GLFrame.getWidth() || this.sHeight != GLFrame.getHeight()) {
               this.sWidth = GLFrame.getWidth();
               this.sHeight = GLFrame.getHeight();
               this.setPos(435.0F, 35.0F, 0.0F);
               this.setWidth((float)(GLFrame.getWidth() - 470));
               this.setHeight((float)(GLFrame.getHeight() - 70));
            }

            super.draw();
         }
      };
      this.optionPanel.onInit();
      this.optionPanel.setCloseCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               MainMenuOptionsMenu.this.applyGameSettings(MainMenuOptionsMenu.this, true);
               MainMenuOptionsMenu.this.deactivate();
            }

         }

         public boolean isOccluded() {
            return !MainMenuOptionsMenu.this.isActive();
         }
      });
      this.optionPanel.setPos(435.0F, 35.0F, 0.0F);
      this.optionPanel.setWidth((float)(GLFrame.getWidth() - 470));
      this.optionPanel.setHeight((float)(GLFrame.getHeight() - 70));
      this.optionPanel.reset();
      this.optionPanel.setCallback(this);
      this.optionPanel.activeInterface = this;

      try {
         this.getState().getController().getInputController().getJoystick().read();
      } catch (IOException var2) {
         var2.printStackTrace();
      } catch (ParseException var3) {
         var3.printStackTrace();
      }
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var1.getUserPointer() != null && !var1.wasInside() && var1.isInside()) {
         this.getState().getController().queueUIAudio("0022_action - buttons push small");
      }

      if (var2.pressedLeftMouse() && var1.getUserPointer() != null) {
         if (var1.getUserPointer().equals("OK")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - enter");
            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("CANCEL")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - back");
            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("X")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - back");
            System.err.println("[GUI] OPTIONS CLICKED X");
            this.deactivate();
            return;
         }

         assert false : "not known command: '" + var1.getUserPointer() + "'";
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public boolean isActive() {
      return !MainMenuGUI.runningSwingDialog && (this.getState().getController().getPlayerInputs().isEmpty() || ((DialogInterface)this.getState().getController().getPlayerInputs().get(this.getState().getController().getPlayerInputs().size() - 1)).getInputPanel() == this.optionPanel);
   }

   public GUIElement getInputPanel() {
      return this.optionPanel;
   }

   public void onDeactivate() {
   }

   public boolean isOccluded() {
      return false;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public void setErrorMessage(String var1) {
      System.err.println(var1);
   }

   public boolean isInside() {
      return this.getInputPanel().isInside();
   }
}
