package org.schema.game.client.view.mainmenu;

import org.schema.game.client.controller.GameMainMenuController;
import org.schema.game.client.view.mainmenu.gui.GUILocalUniversePanel;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public class LocalUniverseDialog extends MainMenuInputDialog {
   private final GUILocalUniversePanel p;

   public LocalUniverseDialog(GameMainMenuController var1) {
      super(var1);
      this.p = new GUILocalUniversePanel(var1, this);
      this.p.onInit();
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public GUIElement getInputPanel() {
      return this.p;
   }

   public void onDeactivate() {
      this.p.cleanUp();
   }

   public boolean isInside() {
      return this.p.isInside();
   }
}
