package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.schema.game.client.controller.PlayerDropDownInput;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.client.view.mainmenu.MainMenuGUI;
import org.schema.game.common.controller.rules.rules.Rule;
import org.schema.game.common.controller.rules.rules.actions.Action;
import org.schema.game.common.controller.rules.rules.conditions.Condition;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.controller.rules.rules.conditions.RuleFieldValueInterface;
import org.schema.game.common.controller.rules.rules.conditions.seg.ConditionGroup;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;

public class GUIConditionGroupConfigPanel extends GUIElement implements RuleListUpdater, GUIActiveInterface {
   public GUIMainWindow mainPanel;
   private GUIContentPane mainTab;
   private DialogInput diag;
   private List toCleanUp = new ObjectArrayList();
   private GUIRuleStat stat;
   private boolean init;
   private GUIConditionAndActionDetailList dList;
   private GUIContentPane t;
   private final ConditionGroup g;
   private RuleListUpdater lastRSC;
   private int detailIndex;

   public GUIConditionGroupConfigPanel(InputState var1, GUIRuleStat var2, ConditionGroup var3, DialogInput var4) {
      super(var1);
      this.diag = var4;
      this.stat = var2;
      var2.selectedCondition = null;
      var2.selectedAction = null;
      this.g = var3;
      this.lastRSC = var2.rsc;
      var2.rsc = this;
   }

   public void cleanUp() {
      Iterator var1 = this.toCleanUp.iterator();

      while(var1.hasNext()) {
         ((GUIElement)var1.next()).cleanUp();
      }

      this.toCleanUp.clear();
      this.stat.rsc = this.lastRSC;
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.mainPanel.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      if (!this.init) {
         this.mainPanel = new GUIMainWindow(this.getState(), GLFrame.getWidth() - 410, GLFrame.getHeight() - 20, 400, 10, "RuleWindow");
         this.mainPanel.onInit();
         this.mainPanel.setPos(435.0F, 35.0F, 0.0F);
         this.mainPanel.setWidth((float)(GLFrame.getWidth() - 470));
         this.mainPanel.setHeight((float)(GLFrame.getHeight() - 70));
         this.mainPanel.clearTabs();
         this.mainTab = this.createRuleSetTab();
         this.mainPanel.activeInterface = this;
         this.mainPanel.setCloseCallback(new GUICallback() {
            public boolean isOccluded() {
               return !GUIConditionGroupConfigPanel.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  GUIConditionGroupConfigPanel.this.diag.deactivate();
               }

            }
         });
         this.init = true;
      }
   }

   public boolean isInside() {
      return this.mainPanel.isInside();
   }

   private GUIContentPane createRuleSetTab() {
      this.t = this.mainPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUICONDITIONGROUPCONFIGPANEL_0);
      this.t.setTextBoxHeightLast(24);
      this.t.addNewTextBox(72);
      this.addEditButtonPanel(this.t, 1);
      this.t.addNewTextBox(280);
      GUIConditionList var1;
      (var1 = new GUIConditionList(this.getState(), this.t.getContent(2), this, this.stat, this.g)).onInit();
      this.t.getContent(2).attach(var1);
      this.t.addNewTextBox(100);
      this.detailIndex = 3;
      this.dList = new GUIConditionAndActionDetailList(this.getState(), this.t.getContent(this.detailIndex), this, this.stat, (RuleFieldValueInterface)null);
      this.dList.onInit();
      this.t.getContent(this.detailIndex).attach(this.dList);
      return this.t;
   }

   public void updateDetailPanel(Rule var1, Condition var2, Action var3) {
      this.t.getContent(this.detailIndex).detach(this.dList);
      this.dList.cleanUp();
      this.dList = new GUIConditionAndActionDetailList(this.getState(), this.t.getContent(this.detailIndex), this, this.stat, (RuleFieldValueInterface)(var2 != null ? var2 : var3));
      this.dList.onInit();
      this.t.getContent(this.detailIndex).attach(this.dList);
   }

   private void addEditButtonPanel(GUIContentPane var1, int var2) {
      GUIHorizontalButtonTablePane var3;
      (var3 = new GUIHorizontalButtonTablePane(this.getState(), 3, 2, var1.getContent(var2))).onInit();
      var3.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUICONDITIONGROUPCONFIGPANEL_1, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         private ConditionTypes selected;

         public boolean isOccluded() {
            return !GUIConditionGroupConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               int var7 = 0;
               List var8;
               GUIElement[] var3 = new GUIElement[(var8 = ConditionTypes.getSortedByName(GUIConditionGroupConfigPanel.this.stat.selectedRule.getEntityType())).size()];

               for(Iterator var9 = var8.iterator(); var9.hasNext(); ++var7) {
                  ConditionTypes var4 = (ConditionTypes)var9.next();
                  GUIAncor var5 = new GUIAncor(GUIConditionGroupConfigPanel.this.getState(), 200.0F, 24.0F);
                  GUITextOverlay var6;
                  (var6 = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM, GUIConditionGroupConfigPanel.this.getState())).setTextSimple(var4.getName());
                  var6.setPos(4.0F, 4.0F, 0.0F);
                  var5.attach(var6);
                  var5.setUserPointer(var4);
                  var3[var7] = var5;
               }

               (new PlayerDropDownInput("RULESELCONDITIONTYPE", GUIConditionGroupConfigPanel.this.getState(), 400, 200, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUICONDITIONGROUPCONFIGPANEL_2, 24, new Object() {
                  public String toString() {
                     return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUICONDITIONGROUPCONFIGPANEL_3 + "\n" + (selected != null ? selected.getDesc() : "");
                  }
               }, var3) {
                  public void pressedOK(GUIListElement var1) {
                     Condition var2 = ((ConditionTypes)var1.getContent().getUserPointer()).fac.instantiateCondition();
                     GUIConditionGroupConfigPanel.this.g.addCondition(var2);
                     GUIConditionGroupConfigPanel.this.stat.selectedCondition = var2;
                     System.err.println("SELECTED CONDITION: " + var2);
                     GUIConditionGroupConfigPanel.this.stat.selectedAction = null;
                     GUIConditionGroupConfigPanel.this.stat.change();
                     this.deactivate();
                  }

                  public void onDeactivate() {
                  }

                  public void onSelectionChanged(GUIListElement var1) {
                     super.onSelectionChanged(var1);
                     if (var1 != null) {
                        selected = (ConditionTypes)var1.getUserPointer();
                     }

                  }
               }).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIConditionGroupConfigPanel.this.isActive() && GUIConditionGroupConfigPanel.this.stat.manager != null && GUIConditionGroupConfigPanel.this.stat.selectedRule != null;
         }
      });
      var3.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUICONDITIONGROUPCONFIGPANEL_4, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         public boolean isOccluded() {
            return !GUIConditionGroupConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIConditionGroupConfigPanel.this.stat.selectedCondition != null) {
               try {
                  Condition var4 = GUIConditionGroupConfigPanel.this.stat.selectedCondition.duplicate();
                  GUIConditionGroupConfigPanel.this.stat.selectedRule.addCondition(var4);
                  GUIConditionGroupConfigPanel.this.stat.selectedCondition = var4;
                  GUIConditionGroupConfigPanel.this.stat.selectedAction = null;
                  GUIConditionGroupConfigPanel.this.stat.change();
               } catch (IOException var3) {
                  var3.printStackTrace();
               }

               GUIConditionGroupConfigPanel.this.stat.change();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIConditionGroupConfigPanel.this.isActive() && GUIConditionGroupConfigPanel.this.stat.manager != null && GUIConditionGroupConfigPanel.this.stat.selectedCondition != null;
         }
      });
      var3.addButton(2, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUICONDITIONGROUPCONFIGPANEL_5, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.RED, new GUICallback() {
         public boolean isOccluded() {
            return !GUIConditionGroupConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIConditionGroupConfigPanel.this.stat.selectedCondition != null) {
               GUIConditionGroupConfigPanel.this.g.removeCondition(GUIConditionGroupConfigPanel.this.stat.selectedCondition);
               GUIConditionGroupConfigPanel.this.stat.change();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIConditionGroupConfigPanel.this.isActive() && GUIConditionGroupConfigPanel.this.stat.manager != null && GUIConditionGroupConfigPanel.this.stat.selectedCondition != null;
         }
      });
      var3.addButton(0, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUICONDITIONGROUPCONFIGPANEL_6, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         public boolean isOccluded() {
            return !GUIConditionGroupConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIConditionGroupConfigPanel.this.stat.selectedRule != null) {
               GUIConditionGroupConfigPanel.this.g.setAllTrue(!GUIConditionGroupConfigPanel.this.g.isAllTrue());
               GUIConditionGroupConfigPanel.this.stat.change();
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIConditionGroupConfigPanel.this.isActive() && GUIConditionGroupConfigPanel.this.stat.manager != null;
         }

         public boolean isHighlighted(InputState var1) {
            return GUIConditionGroupConfigPanel.this.g.isAllTrue();
         }
      });
      var3.addButton(1, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUICONDITIONGROUPCONFIGPANEL_7, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         public boolean isOccluded() {
            return !GUIConditionGroupConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIConditionGroupConfigPanel.this.stat.selectedRule != null) {
               GUIConditionGroupConfigPanel.this.g.setAllTrue(!GUIConditionGroupConfigPanel.this.g.isAllTrue());
               GUIConditionGroupConfigPanel.this.stat.change();
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIConditionGroupConfigPanel.this.isActive() && GUIConditionGroupConfigPanel.this.stat.manager != null;
         }

         public boolean isHighlighted(InputState var1) {
            return !GUIConditionGroupConfigPanel.this.g.isAllTrue();
         }
      });
      var1.getContent(var2).attach(var3);
   }

   public float getHeight() {
      return 0.0F;
   }

   public float getWidth() {
      return 0.0F;
   }

   public boolean isActive() {
      List var1 = this.getState().getController().getInputController().getPlayerInputs();
      return !MainMenuGUI.runningSwingDialog && (var1.isEmpty() || ((DialogInterface)var1.get(var1.size() - 1)).getInputPanel() == this);
   }
}
