package org.schema.game.client.view.mainmenu.gui;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observable;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.GameMainMenuController;
import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.game.client.controller.PlayerTextInput;
import org.schema.game.client.view.mainmenu.LocalUniverse;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUICheckBoxTextPair;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContextPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;
import org.schema.schine.resource.FileExt;

public class GUILocalUniverseList extends ScrollableTableList {
   private boolean createBackupOnDelete = true;
   boolean first = true;
   private long newest = 0L;

   public GUILocalUniverseList(InputState var1, GUIElement var2) {
      super(var1, 100.0F, 100.0F, var2);
      ((Observable)this.getState()).addObserver(this);
   }

   public void cleanUp() {
      ((Observable)this.getState()).deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSELIST_0, 5.0F, new Comparator() {
         public int compare(LocalUniverse var1, LocalUniverse var2) {
            return var1.name.toLowerCase(Locale.ENGLISH).compareTo(var2.name.toLowerCase(Locale.ENGLISH));
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSELIST_1, 120, new Comparator() {
         public int compare(LocalUniverse var1, LocalUniverse var2) {
            if (var1.lastChanged > var2.lastChanged) {
               return -1;
            } else {
               return var1.lastChanged < var2.lastChanged ? 1 : 0;
            }
         }
      }, true);
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, LocalUniverse var2) {
            return var2.name.toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSELIST_2, ControllerElement.FilterRowStyle.FULL, ControllerElement.FilterPos.TOP);
   }

   protected Collection getElementList() {
      return LocalUniverse.readUniverses();
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      final DateFormat var3 = DateFormat.getDateInstance(2, Locale.getDefault());
      Iterator var9 = var2.iterator();

      while(var9.hasNext()) {
         final LocalUniverse var4 = (LocalUniverse)var9.next();
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());

         assert var4.name != null;

         var5.setTextSimple(new Object() {
            public String toString() {
               return var4.name;
            }
         });
         var6.setTextSimple(new Object() {
            public String toString() {
               String var1 = var3.format(new Date(var4.lastChanged));

               assert var1 != null;

               return var1;
            }
         });
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         ScrollableTableList.GUIClippedRow var8;
         (var8 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var6);
         var5.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;
         GUILocalUniverseList.LocalUniverseRow var10 = new GUILocalUniverseList.LocalUniverseRow(this.getState(), var4, new GUIElement[]{var7, var8});
         new GUIAncor(this.getState(), 100.0F, 100.0F);
         var10.onInit();
         var1.addWithoutUpdate(var10);
      }

      var1.updateDim();
      this.first = false;
   }

   protected boolean isFiltered(LocalUniverse var1) {
      return super.isFiltered(var1);
   }

   class LocalUniverseRow extends ScrollableTableList.Row {
      protected GUIContextPane createContext() {
         GUIContextPane var1 = new GUIContextPane(this.getState(), 180.0F, 25.0F);
         GUIHorizontalButtonTablePane var2;
         (var2 = new GUIHorizontalButtonTablePane(this.getState(), 1, 3, var1)).onInit();
         var2.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSELIST_3, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  (new PlayerOkCancelInput("BACKUP", LocalUniverseRow.this.getState(), 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSELIST_4, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSELIST_5) {
                     public void pressedOK() {
                        ((GameMainMenuController)this.getState()).backupAndDeleteSelected(true, false);
                        this.deactivate();
                     }

                     public void onDeactivate() {
                     }
                  }).activate();
               }

            }

            public boolean isOccluded() {
               return false;
            }
         }, new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return true;
            }
         });
         var2.addButton(0, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSELIST_10, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  PlayerTextInput var3;
                  (var3 = new PlayerTextInput("RENAME_UNIVERSE", LocalUniverseRow.this.getState(), 32, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSELIST_11, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSELIST_13, ((LocalUniverse)LocalUniverseRow.this.f).name) {
                     public void onFailedTextCheck(String var1) {
                     }

                     public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                        return null;
                     }

                     public String[] getCommandPrefixes() {
                        return null;
                     }

                     public boolean onInput(String var1) {
                        if (var1.trim().length() > 0) {
                           try {
                              FileUtils.moveDirectory(new FileExt(GameServerState.SERVER_DATABASE + ((LocalUniverse)LocalUniverseRow.this.f).name), new FileExt(GameServerState.SERVER_DATABASE + var1.trim()));
                              ((LocalUniverse)LocalUniverseRow.this.f).name = var1.trim();
                              return true;
                           } catch (IOException var2) {
                              var2.printStackTrace();
                              return false;
                           }
                        } else {
                           return false;
                        }
                     }

                     public void onDeactivate() {
                     }
                  }).setInputChecker(new InputChecker() {
                     public boolean check(String var1, TextCallback var2) {
                        FileExt var4 = new FileExt(var1);

                        try {
                           var4.getCanonicalPath();
                           return true;
                        } catch (IOException var3) {
                           var2.onFailedTextCheck("Name must be a legal file name");
                           return false;
                        }
                     }
                  });
                  var3.activate();
               }

            }

            public boolean isOccluded() {
               return false;
            }
         }, new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return true;
            }
         });
         var2.addButton(0, 2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSELIST_6, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  PlayerOkCancelInput var3;
                  (var3 = new PlayerOkCancelInput("CONFIRM_DEL", LocalUniverseRow.this.getState(), 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSELIST_7, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSELIST_8, ((GameMainMenuController)LocalUniverseRow.this.getState()).getSelectedLocalUniverse().name)) {
                     public void pressedOK() {
                        ((GameMainMenuController)this.getState()).deleteSelectedWorld(GUILocalUniverseList.this.createBackupOnDelete);
                        this.deactivate();
                     }

                     public void onDeactivate() {
                     }
                  }).getInputPanel().onInit();
                  GUICheckBoxTextPair var4 = new GUICheckBoxTextPair(LocalUniverseRow.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSELIST_9, 100, 24) {
                     public boolean isActivated() {
                        return GUILocalUniverseList.this.createBackupOnDelete;
                     }

                     public void deactivate() {
                        GUILocalUniverseList.this.createBackupOnDelete = false;
                     }

                     public void activate() {
                        GUILocalUniverseList.this.createBackupOnDelete = true;
                     }
                  };
                  ((GUIDialogWindow)var3.getInputPanel().background).getMainContentPane().getContent(0).attach(var4);
                  var4.setPos(10.0F, 50.0F, 0.0F);
                  var3.activate();
               }

            }

            public boolean isOccluded() {
               return false;
            }
         }, new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return true;
            }
         });
         var1.attach(var2);
         return var1;
      }

      public LocalUniverseRow(InputState var2, LocalUniverse var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelectSimple = true;
         this.setAllwaysOneSelected(true);
         this.rightClickSelectsToo = true;
         if (GUILocalUniverseList.this.first && var3.lastChanged > GUILocalUniverseList.this.newest) {
            this.clickedOnRow();
            GUILocalUniverseList.this.newest = var3.lastChanged;
         }

      }
   }
}
