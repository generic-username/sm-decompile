package org.schema.game.client.view.mainmenu.gui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.text.DateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIFileChooserList extends ScrollableTableList {
   private GUIActiveInterface active;
   private final FileChooserStats stats;
   boolean first = true;
   private GUIActivatableTextBar playerNameBar;

   public GUIFileChooserList(InputState var1, GUIElement var2, GUIActiveInterface var3, FileChooserStats var4) {
      super(var1, 100.0F, 100.0F, var2);
      this.active = var3;
      var4.addObserver(this);
      this.stats = var4;
   }

   public void cleanUp() {
      super.cleanUp();
      this.stats.deleteObserver(this);
   }

   public void initColumns() {
      new StringComparator();
      this.addFixedWidthColumn("Type", 60, new Comparator() {
         public int compare(GUIFileEntry var1, GUIFileEntry var2) {
            return var1.compareTo(var2);
         }
      }, true);
      this.addColumn("Name", 2.0F, new Comparator() {
         public int compare(GUIFileEntry var1, GUIFileEntry var2) {
            return var1.getName().compareTo(var2.getName());
         }
      }, false);
   }

   protected Collection getElementList() {
      ObjectArrayList var1 = new ObjectArrayList();
      this.stats.getFiles(var1);
      return var1;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      DateFormat.getDateInstance(2, Locale.getDefault());
      Iterator var6 = var2.iterator();

      while(var6.hasNext()) {
         final GUIFileEntry var3 = (GUIFileEntry)var6.next();
         ScrollableTableList.GUIClippedRow var4 = this.getSimpleRow(new Object() {
            public String toString() {
               if (var3.isUpDir()) {
                  return "DIR";
               } else {
                  return var3.isDirectory() ? "DIR" : "FILE";
               }
            }
         }, this.active);
         ScrollableTableList.GUIClippedRow var5 = this.getSimpleRow(new Object() {
            public String toString() {
               return var3.getName();
            }
         }, this.active);
         GUIFileChooserList.GUIFileEntryRow var7;
         (var7 = new GUIFileChooserList.GUIFileEntryRow(this.getState(), var3, new GUIElement[]{var4, var5})).onInit();
         var1.addWithoutUpdate(var7);
      }

      var1.updateDim();
      this.first = false;
   }

   protected boolean isFiltered(GUIFileEntry var1) {
      return this.stats.isFilteredOut(var1) || super.isFiltered(var1);
   }

   class GUIFileEntryRow extends ScrollableTableList.Row {
      public GUIFileEntryRow(InputState var2, GUIFileEntry var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
         this.highlightSelectSimple = true;
         this.setAllwaysOneSelected(true);
         this.rightClickSelectsToo = true;
      }

      public void onDoubleClick() {
         super.onDoubleClick();
         GUIFileChooserList.this.stats.onDoubleClick((GUIFileEntry)this.f);
      }

      protected void clickedOnRow() {
         super.clickedOnRow();
         GUIFileChooserList.this.stats.onSingleClick((GUIFileEntry)this.f);
      }
   }
}
