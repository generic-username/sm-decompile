package org.schema.game.client.view.mainmenu.gui.effectconfig;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.common.data.blockeffects.config.ConfigManagerInterface;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public class EffectConfigEntityDialog extends DialogInput {
   private final GUIEffectConfigPanel p;
   private GUIEffectStat stat;

   public EffectConfigEntityDialog(GameClientState var1, ConfigManagerInterface var2, GUIEffectStat var3) {
      super(var1);
      this.p = new GUIEffectConfigPanel(var1, var2, var3, this);
      this.p.onInit();
      this.stat = var3;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public GUIElement getInputPanel() {
      return this.p;
   }

   public void onDeactivate() {
      this.p.cleanUp();
   }

   public void update(Timer var1) {
      super.update(var1);
      this.stat.updateLocal(var1);
   }
}
