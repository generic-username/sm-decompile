package org.schema.game.client.view.mainmenu.gui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;
import org.schema.game.client.controller.GameMainMenuController;
import org.schema.game.client.controller.PlayerTextInput;
import org.schema.game.client.view.mainmenu.MainMenuGUI;
import org.schema.game.client.view.mainmenu.MainMenuInputDialog;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIInnerTextbox;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITabbedContent;
import org.schema.schine.input.InputState;
import org.schema.schine.resource.FileExt;

public class GUILocalUniversePanel extends GUIElement implements GUIActiveInterface {
   public GUIMainWindow universePanel;
   private GUIContentPane localUniversesTab;
   private MainMenuInputDialog diag;
   private List toCleanUp = new ObjectArrayList();

   public GUILocalUniversePanel(InputState var1, MainMenuInputDialog var2) {
      super(var1);
      this.diag = var2;
   }

   public void cleanUp() {
      Iterator var1 = this.toCleanUp.iterator();

      while(var1.hasNext()) {
         ((GUIElement)var1.next()).cleanUp();
      }

      this.toCleanUp.clear();
   }

   public void draw() {
      GlUtil.glPushMatrix();
      this.transform();
      this.universePanel.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.universePanel = new GUIMainWindow(this.getState(), GLFrame.getWidth() - 410, GLFrame.getHeight() - 20, 400, 10, "UniversePanelWindow");
      this.universePanel.onInit();
      this.universePanel.setPos(435.0F, 35.0F, 0.0F);
      this.universePanel.setWidth((float)(GLFrame.getWidth() - 470));
      this.universePanel.setHeight((float)(GLFrame.getHeight() - 70));
      this.universePanel.clearTabs();
      this.localUniversesTab = this.createLocalTab();
      this.universePanel.activeInterface = this;
      this.universePanel.setCloseCallback(new GUICallback() {
         public boolean isOccluded() {
            return !GUILocalUniversePanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUILocalUniversePanel.this.diag.deactivate();
            }

         }
      });
   }

   public boolean isInside() {
      return this.universePanel.isInside();
   }

   private GUIContentPane createLocalTab() {
      GUIContentPane var1;
      (var1 = this.universePanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_0)).setTextBoxHeightLast(28);
      GUITabbedContent var2;
      (var2 = new GUITabbedContent(this.getState(), var1.getContent(0))).activationInterface = this;
      var2.onInit();
      var1.getContent(0).attach(var2);
      GUIContentPane var3;
      (var3 = var2.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_17)).setTextBoxHeightLast(128);
      GUISimpleSettingsList var4;
      (var4 = new GUISimpleSettingsList(this.getState(), var3.getContent(0), this)).onInit();
      this.toCleanUp.add(var4);
      var3.getContent(0).attach(var4);
      var3.addNewTextBox(28);
      GUILocalUniverseList var7;
      (var7 = new GUILocalUniverseList(this.getState(), var3.getContent(1))).selCallback = (GameStarterState)this.getState();
      this.toCleanUp.add(var7);
      var7.onInit();
      var3.getContent(1).attach(var7);
      var3.setListDetailMode((GUIInnerTextbox)var3.getTextboxes().get(1));
      var3.addNewTextBox(28);
      GUIHorizontalButtonTablePane var8;
      (var8 = new GUIHorizontalButtonTablePane(this.getState(), 3, 1, var3.getContent(2))).onInit();
      this.createLocalUniversesButtons(var8);
      var3.getContent(2).attach(var8);
      GUIContentPane var5;
      (var5 = var2.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_21)).setTextBoxHeightLast(28);
      GUITabbedContent var6;
      (var6 = new GUITabbedContent(this.getState(), var5.getContent(0))).activationInterface = this;
      var6.onInit();
      var6.setPos(0.0F, 2.0F, 0.0F);
      var5.getContent(0).attach(var6);
      this.addSettingsTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_22, var6, ServerConfig.ServerConfigCategory.GAME_SETTING);
      this.addSettingsTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_26, var6, ServerConfig.ServerConfigCategory.NPC_SETTING);
      this.addSettingsTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_23, var6, ServerConfig.ServerConfigCategory.PERFORMANCE_SETTING);
      this.addSettingsTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_24, var6, ServerConfig.ServerConfigCategory.NETWORK_SETTING);
      this.addSettingsTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_25, var6, ServerConfig.ServerConfigCategory.DATABASE_SETTING);
      return var1;
   }

   private void createLocalUniversesButtons(GUIHorizontalButtonTablePane var1) {
      var1.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_2, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_PINK_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !GUILocalUniversePanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ((GameStarterState)GUILocalUniversePanel.this.getState()).startSelectedLocalGame();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUILocalUniversePanel.this.isActive() && ((GameMainMenuController)GUILocalUniversePanel.this.getState()).hasCurrentLocalSelected();
         }
      });
      var1.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_3, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !GUILocalUniversePanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               PlayerTextInput var3;
               (var3 = new PlayerTextInput("NameDiag", GUILocalUniversePanel.this.getState(), 64, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_4, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_5) {
                  public void onFailedTextCheck(String var1) {
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public boolean onInput(String var1) {
                     if (var1.trim().length() > 0) {
                        GameMainMenuController.createWorld(var1.trim(), this.getState());
                        return true;
                     } else {
                        return false;
                     }
                  }

                  public void onDeactivate() {
                  }
               }).setInputChecker(new InputChecker() {
                  public boolean check(String var1, TextCallback var2) {
                     FileExt var4 = new FileExt(var1);

                     try {
                        var4.getCanonicalPath();
                        return true;
                     } catch (IOException var3) {
                        var2.onFailedTextCheck("Name must be a legal file name");
                        return false;
                     }
                  }
               });
               var3.activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUILocalUniversePanel.this.isActive();
         }
      });
      var1.addButton(2, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_9, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !GUILocalUniversePanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && !MainMenuGUI.runningSwingDialog) {
               MainMenuGUI.runningSwingDialog = true;
               SwingUtilities.invokeLater(new Runnable() {
                  public void run() {
                     JFileChooser var1;
                     (var1 = new JFileChooser(new FileExt("./"))).setFileSelectionMode(0);
                     var1.setAcceptAllFileFilterUsed(false);
                     var1.addChoosableFileFilter(new FileFilter() {
                        public boolean accept(File var1) {
                           if (var1.isDirectory()) {
                              return true;
                           } else {
                              return var1.getName().endsWith(".smdb");
                           }
                        }

                        public String getDescription() {
                           return "StarMade Database (.smdb)";
                        }
                     });
                     var1.setAcceptAllFileFilterUsed(false);
                     JDialog var2;
                     (var2 = new JDialog()).setAlwaysOnTop(true);
                     var2.setVisible(true);
                     if (var1.showDialog(var2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_18) == 0) {
                        final File var3 = var1.getSelectedFile();
                        PlayerTextInput var4;
                        (var4 = new PlayerTextInput("NameDiag", GUILocalUniversePanel.this.getState(), 64, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_10, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_11) {
                           public void onFailedTextCheck(String var1) {
                           }

                           public String handleAutoComplete(String var1, TextCallback var2, String var3x) throws PrefixNotFoundException {
                              return null;
                           }

                           public String[] getCommandPrefixes() {
                              return null;
                           }

                           public boolean onInput(String var1) {
                              if (var1.trim().length() > 0) {
                                 if (!(new FileExt(GameServerState.SERVER_DATABASE + var1.trim())).exists()) {
                                    ((GameMainMenuController)this.getState()).importDB(var3, var1.trim());
                                 } else {
                                    this.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_19);
                                 }

                                 return true;
                              } else {
                                 return false;
                              }
                           }

                           public void onDeactivate() {
                           }
                        }).setInputChecker(new InputChecker() {
                           public boolean check(String var1, TextCallback var2) {
                              FileExt var4 = new FileExt(var1);

                              try {
                                 var4.getCanonicalPath();
                                 return true;
                              } catch (IOException var3) {
                                 var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_20);
                                 return false;
                              }
                           }
                        });
                        var4.activate();
                     }

                     var2.dispose();
                     MainMenuGUI.runningSwingDialog = false;
                  }
               });
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUILocalUniversePanel.this.isActive();
         }
      });
   }

   private void addSettingsTab(String var1, GUITabbedContent var2, ServerConfig.ServerConfigCategory var3) {
      GUIContentPane var4 = var2.addTab(var1);
      GUIServerSettingsList var5;
      (var5 = new GUIServerSettingsList(this.getState(), var4.getContent(0), var3, this)).onInit();
      this.toCleanUp.add(var5);
      var4.getContent(0).attach(var5);
      this.addOkCancel(var4);
   }

   private void addOkCancel(GUIContentPane var1) {
      var1.setTextBoxHeightLast(10);
      var1.setListDetailMode((GUIInnerTextbox)var1.getTextboxes().get(var1.getTextboxes().size() - 1));
      var1.addNewTextBox(28);
      int var2 = var1.getTextboxes().size() - 1;
      GUIHorizontalButtonTablePane var3;
      (var3 = new GUIHorizontalButtonTablePane(this.getState(), 2, 1, var1.getContent(var2))).onInit();
      var3.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_1, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !GUILocalUniversePanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUILocalUniversePanel.access$100(GUILocalUniversePanel.this);
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUILocalUniversePanel.this.isActive();
         }
      });
      var3.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUILOCALUNIVERSEPANEL_16, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !GUILocalUniversePanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUILocalUniversePanel.this.pressedCancel();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUILocalUniversePanel.this.isActive();
         }
      });
      var1.getContent(var2).attach(var3);
   }

   private void pressedCancel() {
      ServerConfig.read();
      Iterator var1 = this.toCleanUp.iterator();

      while(var1.hasNext()) {
         GUIElement var2;
         if ((var2 = (GUIElement)var1.next()) instanceof GUIServerSettingsList) {
            ((GUIServerSettingsList)var2).clear();
         }
      }

   }

   private void pressedApply() {
      try {
         ServerConfig.write();
      } catch (IOException var1) {
         var1.printStackTrace();
      }
   }

   public float getHeight() {
      return 0.0F;
   }

   public float getWidth() {
      return 0.0F;
   }

   public boolean isActive() {
      List var1 = this.getState().getController().getInputController().getPlayerInputs();
      return !MainMenuGUI.runningSwingDialog && (var1.isEmpty() || ((DialogInterface)var1.get(var1.size() - 1)).getInputPanel() == this);
   }

   // $FF: synthetic method
   static void access$100(GUILocalUniversePanel var0) {
      try {
         ServerConfig.write();
      } catch (IOException var1) {
         var1.printStackTrace();
      }
   }
}
