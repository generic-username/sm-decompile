package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import java.util.Collection;
import java.util.Observable;
import org.schema.game.common.controller.rules.rules.Rule;
import org.schema.game.common.controller.rules.rules.actions.Action;
import org.schema.game.common.controller.rules.rules.conditions.Condition;

public abstract class GUIRuleCollection extends Observable {
   public abstract Collection getRules();

   public abstract void setSelectedRule(Rule var1);

   public abstract void setSelectedAction(Action var1);

   public abstract void setSelectedCondition(Condition var1);

   public abstract void change();

   public abstract boolean canRulesBeIgnored();
}
