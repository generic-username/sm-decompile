package org.schema.game.client.view.mainmenu.gui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import javax.swing.filechooser.FileFilter;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.client.view.mainmenu.FileChooserDialog;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public abstract class FileChooserStats extends Observable implements OnInputChangedCallback, TextCallback, DropDownCallback {
   private File currentPath;
   private String selectedName;
   private FileFilter[] filters;
   private FileFilter filter;
   private InputState state;
   public FileChooserDialog diag;
   String lastSel = null;
   File lastF;

   public FileChooserStats(InputState var1, File var2, String var3, FileFilter... var4) {
      this.currentPath = var2;
      this.selectedName = var3;
      this.filters = var4;
      this.state = var1;
   }

   public void getFiles(List var1) {
      var1.clear();
      File[] var2;
      int var3;
      int var4;
      File var5;
      if (this.currentPath.getAbsoluteFile().getParentFile() == null) {
         var3 = (var2 = File.listRoots()).length;

         for(var4 = 0; var4 < var3; ++var4) {
            var5 = var2[var4];
            var1.add(new GUIFileEntry(var5));
         }
      } else {
         var1.add(new GUIFileEntry((File)null));
      }

      var3 = (var2 = this.currentPath.listFiles()).length;

      for(var4 = 0; var4 < var3; ++var4) {
         var5 = var2[var4];
         var1.add(new GUIFileEntry(var5));
      }

      Collections.sort(var1);
   }

   public boolean isFilteredOut(GUIFileEntry var1) {
      return this.filter != null && !var1.isUpDir() && !this.filter.accept(var1.file);
   }

   public void onDoubleClick(GUIFileEntry var1) {
      if (var1.isDirectory()) {
         if (var1.isUpDir()) {
            this.currentPath = this.currentPath.getAbsoluteFile().getParentFile();
         } else {
            this.currentPath = var1.file;
         }

         this.lastF = null;
         this.lastSel = null;
         this.setChanged();
         this.notifyObservers();
      } else {
         this.onPressedOk(this.diag);
      }
   }

   public void onSingleClick(GUIFileEntry var1) {
      this.selectedName = var1.getName();
      this.diag.onSelectedChanged(this.selectedName);
   }

   public String[] getCommandPrefixes() {
      return null;
   }

   public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
      return null;
   }

   public void onFailedTextCheck(String var1) {
   }

   public void onTextEnter(String var1, boolean var2, boolean var3) {
   }

   public void newLine() {
   }

   public String onInputChanged(String var1) {
      return this.selectedName = var1;
   }

   public Collection getFileTypesGUIElements() {
      ObjectArrayList var1 = new ObjectArrayList();
      if (this.filters.length > 0) {
         FileFilter[] var2;
         int var3 = (var2 = this.filters).length;

         for(int var4 = 0; var4 < var3; ++var4) {
            FileFilter var5 = var2[var4];
            if (this.filter == null) {
               this.filter = var5;
            }

            GUITextOverlay var6;
            (var6 = new GUITextOverlay(10, 10, this.state)).setTextSimple(var5.getDescription());
            var6.getPos().x = 5.0F;
            var6.getPos().y = 2.0F;
            var1.add(var6);
            var6.setUserPointer(var5);
         }
      } else {
         GUITextOverlay var7;
         (var7 = new GUITextOverlay(10, 10, this.state)).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_FILECHOOSERSTATS_0);
         var7.getPos().x = 5.0F;
         var7.getPos().y = 2.0F;
         var1.add(var7);
         var7.setUserPointer((Object)null);
      }

      return var1;
   }

   public void onPressedOk(DialogInput var1) {
      this.onSelectedFile(this.currentPath, this.selectedName);
      var1.deactivate();
   }

   public abstract void onSelectedFile(File var1, String var2);

   public void onPressedCancel(DialogInput var1) {
      var1.deactivate();
   }

   public void onSelectionChanged(GUIListElement var1) {
      this.filter = var1.getContent().getUserPointer() != null ? (FileFilter)var1.getContent().getUserPointer() : null;
      this.setChanged();
      this.notifyObservers();
   }

   public String getCurrentSelectedName() {
      return this.selectedName;
   }

   public boolean isDirectorySelected() {
      if (this.lastSel == null || !this.selectedName.equals(this.lastSel)) {
         this.lastF = new File(this.currentPath, this.selectedName);
         this.lastSel = this.selectedName;
      }

      return this.lastF.exists() && this.lastF.isDirectory();
   }
}
