package org.schema.game.client.view.mainmenu.gui.effectconfig;

import java.text.DateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.common.data.blockeffects.config.ConfigGroup;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIEffectConfigGroupList extends ScrollableTableList {
   private GUIActiveInterface active;
   private GUIEffectStat stat;
   boolean first = true;

   public GUIEffectConfigGroupList(InputState var1, GUIElement var2, GUIActiveInterface var3, GUIEffectStat var4) {
      super(var1, 100.0F, 100.0F, var2);
      this.active = var3;
      this.stat = var4;
      var4.addObserver(this);
   }

   public void cleanUp() {
      this.stat.deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTCONFIGGROUPLIST_0, 1.0F, new Comparator() {
         public int compare(ConfigGroup var1, ConfigGroup var2) {
            return var1.id.toLowerCase(Locale.ENGLISH).compareTo(var2.id.toLowerCase(Locale.ENGLISH));
         }
      }, true);
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTCONFIGGROUPLIST_1, 40, new Comparator() {
         public int compare(ConfigGroup var1, ConfigGroup var2) {
            return var1.elements.size() - var2.elements.size();
         }
      });
   }

   protected Collection getElementList() {
      return this.stat.configPool.pool;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      DateFormat.getDateInstance(2, Locale.getDefault());
      Iterator var8 = var2.iterator();

      while(var8.hasNext()) {
         final ConfigGroup var3 = (ConfigGroup)var8.next();
         GUITextOverlayTable var4;
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return var3.id;
            }
         });
         ScrollableTableList.GUIClippedRow var5;
         (var5 = new ScrollableTableList.GUIClippedRow(this.getState())).activationInterface = this.active;
         ScrollableTableList.GUIClippedRow var6;
         (var6 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         GUITextOverlayTable var7;
         (var7 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var3.elements.size());
            }
         });
         var5.attach(var7);
         var4.getPos().y = 5.0F;
         GUIEffectConfigGroupList.ConfigGroupRow var9 = new GUIEffectConfigGroupList.ConfigGroupRow(this.getState(), var3, new GUIElement[]{var6, var5});
         new GUIAncor(this.getState(), 100.0F, 100.0F);
         var9.onInit();
         var1.addWithoutUpdate(var9);
      }

      var1.updateDim();
      this.first = false;
   }

   protected boolean isFiltered(ConfigGroup var1) {
      return super.isFiltered(var1);
   }

   class ConfigGroupRow extends ScrollableTableList.Row {
      public ConfigGroupRow(InputState var2, ConfigGroup var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
         this.highlightSelectSimple = true;
         this.setAllwaysOneSelected(true);
      }

      protected void clickedOnRow() {
         GUIEffectConfigGroupList.this.stat.selectedGroup = (ConfigGroup)this.f;
         GUIEffectConfigGroupList.this.stat.selectedElement = null;
         GUIEffectConfigGroupList.this.stat.change();
         super.clickedOnRow();
      }
   }
}
