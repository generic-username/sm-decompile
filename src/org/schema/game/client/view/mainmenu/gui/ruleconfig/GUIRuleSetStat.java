package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Observable;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.game.client.controller.PlayerTextInput;
import org.schema.game.common.controller.rules.RuleManagerProvider;
import org.schema.game.common.controller.rules.RulePropertyContainer;
import org.schema.game.common.controller.rules.RuleSet;
import org.schema.game.common.controller.rules.RuleSetManager;
import org.schema.game.common.data.SendableGameState;
import org.schema.game.network.objects.remote.RemoteRuleSetManager;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.input.InputState;

public class GUIRuleSetStat extends Observable implements RuleManagerProvider {
   public RuleSetManager manager;
   public RuleSet selectedRuleset;
   private String path;
   public SendableGameState gameState;
   private final InputState inputState;

   public RulePropertyContainer getProperties() {
      return this.gameState != null ? this.gameState.getRuleProperties() : null;
   }

   public GUIRuleSetStat(InputState var1, RuleSetManager var2) {
      this.manager = var2;
      this.inputState = var1;
   }

   public GUIRuleSetStat(InputState var1, String var2) {
      if (var2 == null) {
         this.path = RuleSetManager.rulesPath;
      }

      this.inputState = var1;
      this.load(var1, this.path, false);
   }

   public void updateLocal(Timer var1) {
   }

   public void change() {
      this.setChanged();
      this.notifyObservers(this);
   }

   public String getLoadedPath() {
      return this.path;
   }

   public void saveAs(InputState var1, File var2) {
      try {
         this.manager.writeToDisk(var2);
         PlayerOkCancelInput var3;
         (var3 = new PlayerOkCancelInput("INFO_NOTE", var1, 400, 160, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_2, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_3, var2.getAbsolutePath())) {
            public void pressedOK() {
               this.deactivate();
            }

            public void onDeactivate() {
            }
         }).getInputPanel().setCancelButton(false);
         var3.activate();
      } catch (Exception var4) {
         var4.printStackTrace();
         PlayerOkCancelInput var5;
         (var5 = new PlayerOkCancelInput("INFO_NOTE", var1, 400, 160, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_20, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_5, var2.getAbsolutePath(), var4.getClass().getSimpleName())) {
            public void pressedOK() {
               this.deactivate();
            }

            public void onDeactivate() {
            }
         }).getInputPanel().setCancelButton(false);
         var5.activate();
      }
   }

   public void save(InputState var1, String var2) {
      if (this.gameState != null) {
         System.err.println("[CLIENT] Sending all rules and properties to server");
         this.manager.includePropertiesInSendAndSaveOnServer = true;

         assert this.manager.getProperties() != null;

         this.gameState.getNetworkObject().ruleSetManagerBuffer.add(new RemoteRuleSetManager(this.manager, this.gameState.getNetworkObject()));
         PlayerOkCancelInput var4;
         (var4 = new PlayerOkCancelInput("INFO_NOTE", var1, 400, 160, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_1) {
            public void pressedOK() {
               this.deactivate();
            }

            public void onDeactivate() {
            }
         }).getInputPanel().setCancelButton(false);
         var4.activate();
      } else {
         if (var2 != null) {
            this.path = var2;
         }

         File var3 = new File(this.path);
         this.saveAs(var1, var3);
      }
   }

   public void load(InputState var1, String var2, boolean var3) {
      this.manager = new RuleSetManager();
      File var4 = new File(var2);

      try {
         this.manager.loadFromDisk(var2);
         if (var3) {
            PlayerOkCancelInput var7;
            (var7 = new PlayerOkCancelInput("INFO_NOTE", var1, 400, 160, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_6, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_7, var4.getAbsolutePath())) {
               public void pressedOK() {
                  this.deactivate();
               }

               public void onDeactivate() {
               }
            }).getInputPanel().setCancelButton(false);
            var7.activate();
         }
      } catch (Exception var5) {
         var5.printStackTrace();
         if (!(var5 instanceof FileNotFoundException) || var3) {
            PlayerOkCancelInput var6;
            (var6 = new PlayerOkCancelInput("INFO_NOTE", var1, 400, 160, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_4, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_9, var4.getAbsolutePath(), var5.getClass().getSimpleName())) {
               public void pressedOK() {
                  this.deactivate();
               }

               public void onDeactivate() {
               }
            }).getInputPanel().setCancelButton(false);
            var6.activate();
         }
      }

      this.change();
   }

   public RuleSetManager getRuleSetManager() {
      return this.manager;
   }

   public SendableGameState getGameState() {
      return this.gameState;
   }

   public void importFile(File var1) {
      try {
         RuleSetManager var6;
         (var6 = new RuleSetManager()).loadFromDisk(var1);
         System.err.println("LOADED FROM DISK. RULESETS: " + var6.getRuleSets().size());
         ObjectArrayList var7 = new ObjectArrayList(var6.getRuleSets());
         boolean var3 = true;

         for(int var4 = 0; var4 < var7.size(); ++var4) {
            var3 = this.checkNameAndAdd((RuleSet)var7.get(var4), var4 == var7.size() - 1) && var3;
         }

         if (var3) {
            PlayerOkCancelInput var8;
            (var8 = new PlayerOkCancelInput("CONFIRM", this.inputState, 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_18, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_11, var1.getName())) {
               public void pressedOK() {
                  this.deactivate();
               }

               public void onDeactivate() {
               }
            }).getInputPanel().setCancelButton(false);
            var8.getInputPanel().onInit();
            var8.activate();
         }

      } catch (Exception var5) {
         var5.printStackTrace();
         PlayerOkCancelInput var2;
         (var2 = new PlayerOkCancelInput("CONFIRM", this.inputState, 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_22, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_12, var1.getName(), var5.getClass().getSimpleName(), var5.getMessage() != null ? ": " + var5.getMessage() : "")) {
            public void pressedOK() {
               this.deactivate();
            }

            public void onDeactivate() {
            }
         }).getInputPanel().setCancelButton(false);
         var2.getInputPanel().onInit();
         var2.activate();
      }
   }

   private boolean checkNameAndAdd(final RuleSet var1, final boolean var2) {
      if (this.manager.containtsName(var1)) {
         System.err.println("NAME EXISTS: " + var1.uniqueIdentifier);
         (new PlayerTextInput("PPLExport", this.inputState, 64, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_13, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_14, var1.uniqueIdentifier)) {
            public void onFailedTextCheck(String var1x) {
            }

            public String handleAutoComplete(String var1x, TextCallback var2x, String var3) throws PrefixNotFoundException {
               return null;
            }

            public String[] getCommandPrefixes() {
               return null;
            }

            public boolean onInput(String var1x) {
               if (var1x.length() == 0) {
                  return false;
               } else {
                  var1.uniqueIdentifier = var1x;
                  GUIRuleSetStat.this.checkNameAndAdd(var1, var2);
                  return true;
               }
            }

            public void cancel() {
               var1.uniqueIdentifier = "";
               super.cancel();
            }

            public void onDeactivate() {
               if (var1.uniqueIdentifier.length() > 0 && !GUIRuleSetStat.this.manager.containtsName(var1)) {
                  System.err.println("ADDING RULESET " + var1.uniqueIdentifier);
                  GUIRuleSetStat.this.manager.addRuleSet(var1);
                  GUIRuleSetStat.this.change();
               }

            }
         }).activate();
         return false;
      } else {
         return true;
      }
   }

   public void exportSelected(File var1) {
      try {
         System.err.println("EXPORT SELECTED RULESET TO " + var1.getCanonicalPath());
         this.selectedRuleset.export(var1);
         PlayerOkCancelInput var2;
         (var2 = new PlayerOkCancelInput("CONFIRM", this.inputState, 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_15, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_19, var1.getName())) {
            public void pressedOK() {
               this.deactivate();
            }

            public void onDeactivate() {
            }
         }).getInputPanel().setCancelButton(false);
         var2.getInputPanel().onInit();
         var2.activate();
      } catch (Exception var3) {
         var3.printStackTrace();
         PlayerOkCancelInput var4;
         (var4 = new PlayerOkCancelInput("CONFIRM", this.inputState, 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_23, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_17, var1.getName(), var3.getClass().getSimpleName(), var3.getMessage() != null ? ": " + var3.getMessage() : "")) {
            public void pressedOK() {
               this.deactivate();
            }

            public void onDeactivate() {
            }
         }).getInputPanel().setCancelButton(false);
         var4.getInputPanel().onInit();
         var4.activate();
      }
   }

   public void exportAll(File var1) {
      try {
         this.manager.writeToDisk(var1);
         PlayerOkCancelInput var2;
         (var2 = new PlayerOkCancelInput("CONFIRM", this.inputState, 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_10, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_16, var1.getName())) {
            public void pressedOK() {
               this.deactivate();
            }

            public void onDeactivate() {
            }
         }).getInputPanel().setCancelButton(false);
         var2.getInputPanel().onInit();
         var2.activate();
      } catch (Exception var3) {
         var3.printStackTrace();
         PlayerOkCancelInput var4;
         (var4 = new PlayerOkCancelInput("CONFIRM", this.inputState, 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_8, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETSTAT_21, var1.getName(), var3.getClass().getSimpleName(), var3.getMessage() != null ? ": " + var3.getMessage() : "")) {
            public void pressedOK() {
               this.deactivate();
            }

            public void onDeactivate() {
            }
         }).getInputPanel().setCancelButton(false);
         var4.getInputPanel().onInit();
         var4.activate();
      }
   }
}
