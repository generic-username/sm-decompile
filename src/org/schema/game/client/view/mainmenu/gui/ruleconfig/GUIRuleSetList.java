package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import java.text.DateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.common.controller.rules.RuleSet;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICheckBox;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIRuleSetList extends ScrollableTableList {
   private GUIActiveInterface active;
   private GUIRuleSetStat stat;
   boolean first = true;

   public GUIRuleSetList(InputState var1, GUIElement var2, GUIActiveInterface var3, GUIRuleSetStat var4) {
      super(var1, 100.0F, 100.0F, var2);
      this.active = var3;
      this.stat = var4;
      var4.addObserver(this);
   }

   public void cleanUp() {
      this.stat.deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETLIST_0, 4.0F, new Comparator() {
         public int compare(RuleSet var1, RuleSet var2) {
            return var1.uniqueIdentifier.toLowerCase(Locale.ENGLISH).compareTo(var2.uniqueIdentifier.toLowerCase(Locale.ENGLISH));
         }
      }, true);
      if (this.stat.getProperties() != null) {
         this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETLIST_1, 40, new Comparator() {
            public int compare(RuleSet var1, RuleSet var2) {
               return (GUIRuleSetList.this.stat.getProperties().isGlobal(var1) ? 1 : -1) - (GUIRuleSetList.this.stat.getProperties().isGlobal(var2) ? 1 : -1);
            }
         });
      }

      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETLIST_3, 40, new Comparator() {
         public int compare(RuleSet var1, RuleSet var2) {
            return var1.size() - var2.size();
         }
      });
   }

   protected Collection getElementList() {
      return this.stat.manager.getRuleSets();
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      DateFormat.getDateInstance(2, Locale.getDefault());
      Iterator var8 = var2.iterator();

      while(var8.hasNext()) {
         final RuleSet var3 = (RuleSet)var8.next();
         GUITextOverlayTable var4;
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return var3.uniqueIdentifier;
            }
         });
         ScrollableTableList.GUIClippedRow var5;
         (var5 = new ScrollableTableList.GUIClippedRow(this.getState())).activationInterface = this.active;
         ScrollableTableList.GUIClippedRow var6;
         (var6 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         GUITextOverlayTable var7;
         (var7 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var3.size());
            }
         });
         var5.attach(var7);
         var4.getPos().y = 5.0F;
         GUIRuleSetList.RuleSetRow var9;
         if (this.stat.getProperties() != null) {
            GUICheckBox var10 = new GUICheckBox(this.getState()) {
               protected boolean isActivated() {
                  return GUIRuleSetList.this.stat.getProperties().isGlobal(var3);
               }

               protected void deactivate() throws StateParameterNotFoundException {
                  GUIRuleSetList.this.stat.getProperties().setGlobal(var3, false, false);
               }

               protected void activate() throws StateParameterNotFoundException {
                  GUIRuleSetList.this.stat.getProperties().setGlobal(var3, true, false);
               }
            };
            ScrollableTableList.GUIClippedRow var11;
            (var11 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var10);
            var9 = new GUIRuleSetList.RuleSetRow(this.getState(), var3, new GUIElement[]{var6, var11, var5});
         } else {
            var9 = new GUIRuleSetList.RuleSetRow(this.getState(), var3, new GUIElement[]{var6, var5});
         }

         new GUIAncor(this.getState(), 100.0F, 100.0F);
         var9.onInit();
         var1.addWithoutUpdate(var9);
      }

      var1.updateDim();
      this.first = false;
   }

   protected boolean isFiltered(RuleSet var1) {
      return super.isFiltered(var1);
   }

   class RuleSetRow extends ScrollableTableList.Row {
      public RuleSetRow(InputState var2, RuleSet var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
         this.highlightSelectSimple = true;
         this.setAllwaysOneSelected(true);
      }

      protected void clickedOnRow() {
         GUIRuleSetList.this.stat.selectedRuleset = (RuleSet)this.f;
         GUIRuleSetList.this.stat.change();
         super.clickedOnRow();
      }
   }
}
