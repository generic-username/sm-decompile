package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import java.text.DateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.CompareTools;
import org.schema.game.common.controller.rules.rules.Rule;
import org.schema.game.common.controller.rules.rules.actions.Action;
import org.schema.game.common.controller.rules.rules.conditions.Condition;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIRuleList extends ScrollableTableList {
   private GUIActiveInterface active;
   private GUIRuleCollection stat;
   boolean first = true;

   public GUIRuleList(InputState var1, GUIElement var2, GUIActiveInterface var3, GUIRuleCollection var4) {
      super(var1, 100.0F, 100.0F, var2);
      this.active = var3;
      this.stat = var4;
      var4.addObserver(this);
   }

   public void cleanUp() {
      this.stat.deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULELIST_0, 1.0F, new Comparator() {
         public int compare(Rule var1, Rule var2) {
            return var1.getUniqueIdentifier().toLowerCase(Locale.ENGLISH).compareTo(var2.getUniqueIdentifier().toLowerCase(Locale.ENGLISH));
         }
      }, true);
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULELIST_1, 1.0F, new Comparator() {
         public int compare(Rule var1, Rule var2) {
            return var1.ruleType.getName().toLowerCase(Locale.ENGLISH).compareTo(var2.ruleType.getName().toLowerCase(Locale.ENGLISH));
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULELIST_2, 45, new Comparator() {
         public int compare(Rule var1, Rule var2) {
            return var1.getConditionCount() - var2.getConditionCount();
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULELIST_3, 45, new Comparator() {
         public int compare(Rule var1, Rule var2) {
            return var1.getActionCount() - var2.getActionCount();
         }
      });
      if (this.stat.canRulesBeIgnored()) {
         this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULELIST_4, 40, new Comparator() {
            public int compare(Rule var1, Rule var2) {
               return CompareTools.compare(var1.ignoreRule, var2.ignoreRule);
            }
         });
      }

   }

   protected Collection getElementList() {
      return this.stat.getRules();
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      DateFormat.getDateInstance(2, Locale.getDefault());
      Iterator var10 = var2.iterator();

      while(var10.hasNext()) {
         final Rule var3 = (Rule)var10.next();
         GUITextOverlayTable var4;
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return var3.getUniqueIdentifier();
            }
         });
         ScrollableTableList.GUIClippedRow var5;
         (var5 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         ScrollableTableList.GUIClippedRow var6;
         (var6 = new ScrollableTableList.GUIClippedRow(this.getState())).activationInterface = this.active;
         GUITextOverlayTable var7;
         (var7 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var3.getConditionCount());
            }
         });
         var6.attach(var7);
         ScrollableTableList.GUIClippedRow var13;
         (var13 = new ScrollableTableList.GUIClippedRow(this.getState())).activationInterface = this.active;
         GUITextOverlayTable var8;
         (var8 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var3.getActionCount());
            }
         });
         var13.attach(var8);
         ScrollableTableList.GUIClippedRow var14;
         (var14 = new ScrollableTableList.GUIClippedRow(this.getState())).activationInterface = this.active;
         GUITextOverlayTable var9;
         (var9 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return var3.ruleType.getName();
            }
         });
         var14.attach(var9);
         var4.getPos().y = 5.0F;
         GUIRuleList.RuleRow var11;
         if (this.stat.canRulesBeIgnored()) {
            ScrollableTableList.GUIClippedRow var12;
            (var12 = new ScrollableTableList.GUIClippedRow(this.getState())).activationInterface = this.active;
            (var9 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
               public String toString() {
                  return var3.ignoreRule ? "X" : "";
               }
            });
            var12.attach(var9);
            var11 = new GUIRuleList.RuleRow(this.getState(), var3, new GUIElement[]{var5, var14, var6, var13, var12});
         } else {
            var11 = new GUIRuleList.RuleRow(this.getState(), var3, new GUIElement[]{var5, var14, var6, var13});
         }

         new GUIAncor(this.getState(), 100.0F, 100.0F);
         var11.onInit();
         var1.addWithoutUpdate(var11);
      }

      var1.updateDim();
      this.first = false;
   }

   protected boolean isFiltered(Rule var1) {
      return super.isFiltered(var1);
   }

   class RuleRow extends ScrollableTableList.Row {
      public RuleRow(InputState var2, Rule var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
         this.highlightSelectSimple = true;
         this.setAllwaysOneSelected(true);
      }

      protected void clickedOnRow() {
         GUIRuleList.this.stat.setSelectedRule((Rule)this.f);
         GUIRuleList.this.stat.setSelectedAction((Action)null);
         GUIRuleList.this.stat.setSelectedCondition((Condition)null);
         GUIRuleList.this.stat.change();
         super.clickedOnRow();
      }
   }
}
