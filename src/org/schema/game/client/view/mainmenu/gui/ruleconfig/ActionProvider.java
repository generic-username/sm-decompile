package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import java.util.List;

public interface ActionProvider {
   List getActions();

   boolean isActionAvailable();
}
