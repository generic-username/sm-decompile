package org.schema.game.client.view.mainmenu;

import java.util.Locale;
import org.schema.game.client.controller.GameMainMenuController;
import org.schema.game.client.controller.PlayerButtonTilesInput;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.input.InputState;

public class LanguageDialog extends PlayerButtonTilesInput implements MainMenuInputDialogInterface {
   public LanguageDialog(GameMainMenuController var1) {
      super((String)null, var1, 650, 510, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_LANGUAGEDIALOG_1, FontLibrary.getBlenderProBook16(), 200, 100);
   }

   public void onDeactivate() {
   }

   private void addLanguage(final String var1, String var2) {
      this.addTile(var1, var2, GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         public boolean isOccluded() {
            return !LanguageDialog.this.isActive();
         }

         public void callback(GUIElement var1x, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               LanguageDialog.this.getState().loadLanguage(var1);
               LanguageDialog.this.deactivate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1x) {
            return true;
         }

         public boolean isActive(InputState var1x) {
            return LanguageDialog.this.isActive() && (EngineSettings.LANGUAGE_PACK.getCurrentState() == null || !EngineSettings.LANGUAGE_PACK.getCurrentState().toString().toLowerCase(Locale.ENGLISH).equals(var1.toLowerCase(Locale.ENGLISH)));
         }
      });
   }

   public boolean isActive() {
      return !MainMenuGUI.runningSwingDialog && (this.getState().getController().getPlayerInputs().isEmpty() || this.getState().getController().getPlayerInputs().get(this.getState().getController().getPlayerInputs().size() - 1) == this);
   }

   public void addToolsAndModsButtons() {
      this.addLanguage("English", "English");
      this.addLanguage("Polish", "jezyk polski");
      this.addLanguage("German", "Deutsch");
      this.addLanguage("Spanish", "Espaniol");
      this.addLanguage("French", "Francais");
      this.addLanguage("Russian", "Russkij Jazyk");
      this.addLanguage("Portuguese Brazilian", "portugues brasileiro");
      this.addLanguage("Japanese", "Nihon Go");
      this.addLanguage("Chinese Traditional", "Chinese Traditional\n");
      this.addLanguage("Chinese Simplified", "Chinese Simplified\n");
      this.addLanguage("Czech", "Cestina\n");
   }

   public GameMainMenuController getState() {
      return (GameMainMenuController)super.getState();
   }
}
