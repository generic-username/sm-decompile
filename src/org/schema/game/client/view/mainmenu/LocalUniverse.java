package org.schema.game.client.view.mainmenu;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.File;
import java.util.List;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.resource.FileExt;

public class LocalUniverse {
   public String name;
   public final long lastChanged;

   public LocalUniverse(String var1, long var2) {
      this.name = var1;
      this.lastChanged = var2;
   }

   public int hashCode() {
      return this.name.hashCode();
   }

   public boolean equals(Object var1) {
      return this.name.equals(((LocalUniverse)var1).name);
   }

   public static List readUniverses() {
      ObjectArrayList var0 = new ObjectArrayList();
      FileExt var1;
      if ((var1 = new FileExt(GameServerState.SERVER_DATABASE)).exists() && var1.isDirectory()) {
         File[] var5;
         int var2 = (var5 = var1.listFiles()).length;

         for(int var3 = 0; var3 < var2; ++var3) {
            File var4;
            if ((var4 = var5[var3]).isDirectory() && !var4.getName().startsWith(".") && !var4.getName().equals("DATA") && !var4.getName().equals("index")) {
               var0.add(new LocalUniverse(var4.getName(), var4.lastModified()));
            }
         }
      }

      return var0;
   }
}
