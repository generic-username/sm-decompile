package org.schema.game.client.view.gamemap;

import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.input.Keyboard;
import org.schema.schine.network.objects.container.TransformTimed;

public class GameMapPosition implements Transformable {
   private final GameMapDrawer gameMapDrawer;
   private final TransformTimed transform;
   private final Vector3f oldPosition = new Vector3f();
   private final Vector3f newPosition = new Vector3f();
   private final Vector3f tmp = new Vector3f();
   private final Vector3i tmpSysPosA = new Vector3i();
   private Vector3i secPos = new Vector3i();
   private Vector3i currentSysPos;

   public GameMapPosition(GameClientState var1, GameMapDrawer var2) {
      this.gameMapDrawer = var2;
      this.transform = new TransformTimed();
      this.transform.setIdentity();
   }

   public void add(int var1, int var2, int var3, float var4, boolean var5) {
      if (!Keyboard.isKeyDown(42)) {
         this.secPos.add(var1, var2, var3);
      } else {
         this.secPos.add(var1 << 4, var2 << 4, var3 << 4);
      }

      this.apply(var5);
   }

   private void apply(boolean var1) {
      float var2 = (float)this.secPos.x;
      float var3 = (float)this.secPos.y;
      float var4 = (float)this.secPos.z;
      this.newPosition.set((var2 - 7.5F) * 6.25F, (var3 - 7.5F) * 6.25F, (var4 - 7.5F) * 6.25F);
      if (var1) {
         this.getWorldTransform().origin.set(this.newPosition);
         this.oldPosition.set(this.newPosition);
      }

      this.currentSysPos = VoidSystem.getPosFromSector(this.secPos, this.tmpSysPosA);
      this.gameMapDrawer.checkSystem(this.secPos);
   }

   public Vector3i get(Vector3i var1) {
      var1.set(this.secPos);
      return var1;
   }

   public Vector3i getCurrentSysPos() {
      return this.currentSysPos;
   }

   public TransformTimed getWorldTransform() {
      return this.transform;
   }

   public boolean isActive(Vector3i var1) {
      return this.currentSysPos.equals(var1);
   }

   public void set(int var1, int var2, int var3, boolean var4) {
      this.secPos.set(var1, var2, var3);
      System.err.println("[CLIENT][MAP][POS] SETTING TO " + this.secPos);
      this.apply(var4);
   }

   public void update(Timer var1) {
      label16: {
         if (!this.oldPosition.epsilonEquals(this.newPosition, 0.001F)) {
            this.tmp.sub(this.newPosition, this.oldPosition);
            float var2 = this.tmp.length();
            this.tmp.normalize();
            this.tmp.scale(var1.getDelta() * 10.0F);
            if (var2 > 1.0F) {
               this.tmp.scale(var2);
            }

            if (var2 >= this.tmp.length()) {
               this.oldPosition.add(this.tmp);
               break label16;
            }
         }

         this.oldPosition.set(this.newPosition);
      }

      this.getWorldTransform().origin.set(this.oldPosition);
   }
}
