package org.schema.game.client.view.gamemap;

import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.LongSet;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.nio.IntBuffer;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Map.Entry;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.schema.common.FastMath;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.CompareTools;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.map.MapControllerManager;
import org.schema.game.client.controller.manager.ingame.navigation.NavigationFilter;
import org.schema.game.client.controller.manager.ingame.navigation.NavigationFilterEditDialog;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.gamemap.GameMap;
import org.schema.game.client.view.camera.GameMapCamera;
import org.schema.game.client.view.effects.ConstantIndication;
import org.schema.game.client.view.gui.GalaxyOrientationElement;
import org.schema.game.client.view.gui.PlayerPanel;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.common.controller.trade.TradeActive;
import org.schema.game.common.controller.trade.TradeNodeClient;
import org.schema.game.common.controller.trade.TradeNodeStub;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.FleetMember;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.world.FTLConnection;
import org.schema.game.common.data.world.SystemRange;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GalaxyTmpVars;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.game.server.data.simulation.npc.geo.FactionResourceRequestContainer;
import org.schema.game.server.data.simulation.npc.geo.NPCSystemStub;
import org.schema.schine.common.InputHandler;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.graphicsengine.forms.PositionableSubColorSprite;
import org.schema.schine.graphicsengine.forms.PositionableSubSprite;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIProgressBar;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.util.WorldToScreenConverter;
import org.schema.schine.graphicsengine.util.timer.SinusTimerUtil;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public class GameMapDrawer implements InputHandler, Drawable {
   public static final Vector3i positionVec = new Vector3i();
   public static final int FILTER_X = 1;
   public static final int FILTER_Y = 2;
   public static final int FILTER_Z = 4;
   public static final float size = 100.0F;
   public static final float halfsize = 50.0F;
   public static final float sectorSize = 6.25F;
   public static final float sectorSizeHalf = 3.125F;
   public static final NavigationFilterEditDialog filterCallback = null;
   private static final float systemCubeAlpha = 0.45F;
   public static IntBuffer selectBuffer = BufferUtils.createIntBuffer(2048);
   public static int filterAxis = 0;
   public static boolean drawFactionTerritory = true;
   public static boolean drawFactionByRelation = false;
   public static boolean drawPlanetOrbits = true;
   public static boolean drawAsteroidBeltOrbits = true;
   public static boolean drawWormHoles = true;
   public static boolean drawWarpGates = true;
   public static boolean highlightOrbitSectors;
   protected static long filterMask = 1021L;
   public static final NavigationFilter filter = new NavigationFilter() {
      public final long getFilter() {
         return GameMapDrawer.filterMask;
      }

      public final void setFilter(long var1) {
         GameMapDrawer.filterMask = var1;
      }
   };
   private static boolean drawResources = true;
   private static boolean debug = false;
   private final GameMapPosition gameMapPosition;
   private final GameClientState state;
   private final Vector3i playerSystem = new Vector3i();
   Matrix3f mY = new Matrix3f();
   Matrix3f mYB = new Matrix3f();
   Matrix3f mYC = new Matrix3f();
   Matrix3f mX = new Matrix3f();
   Matrix3f mXB = new Matrix3f();
   Matrix3f mXC = new Matrix3f();
   Vector3i tmp = new Vector3i();
   Vector3f posBuffer = new Vector3f();
   Transform worldpos = new Transform();
   int[] orbits = new int[8];
   Transform orbitRot = new Transform();
   Transform orbitRotInv = new Transform();
   private GameMapCamera camera;
   private SinusTimerUtil sinus = new SinusTimerUtil();
   PositionableSubColorSprite[] ownPosition = new PositionableSubColorSprite[]{new PositionableSubColorSprite() {
      Vector4f c = new Vector4f();
      private Vector3f pos = new Vector3f();

      public Vector4f getColor() {
         this.c.set((1.0F - GameMapDrawer.this.sinus.getTime()) / 2.0F + 0.5F, 1.0F, (1.0F - GameMapDrawer.this.sinus.getTime()) / 2.0F + 0.5F, 1.0F);
         return this.c;
      }

      public Vector3f getPos() {
         return this.pos;
      }

      public float getScale(long var1) {
         return 0.1F + 0.07F * GameMapDrawer.this.sinus.getTime();
      }

      public int getSubSprite(Sprite var1) {
         return 10;
      }

      public boolean canDraw() {
         return true;
      }
   }};
   private WorldToScreenConverter worldToScreenConverter;
   private GalaxyOrientationElement galaxyOrientationElement;
   private int coordList;
   private GUITextOverlay[] systemWallText;
   private Galaxy lastGalaxy;
   private Vector3i sys = new Vector3i();
   private ConstantIndication unexploredIndication;
   private GUIProgressBar npcStatus;
   private final Random r = new Random();
   private float time;
   public static boolean debugSecret = true;
   public static Vector3i highlightIcon;
   private float[] param = new float[1];
   private Vector3f normal = new Vector3f();
   Vector3f fromTmp = new Vector3f();
   Vector3f toTmp = new Vector3f();
   Vector3f aabbMin = new Vector3f();
   Vector3f aabbMax = new Vector3f();
   private List quadBuffer = new ObjectArrayList();
   private ObjectArrayList quadPool = new ObjectArrayList();
   private int camInsideFactionSystem;
   private boolean checkGLErrorOnce;
   private static float quadSize = 49.99F;

   public GameMapDrawer(GameClientState var1) {
      for(int var2 = 0; var2 < 256; ++var2) {
         this.quadPool.add(new GameMapDrawer.Quad());
      }

      this.state = var1;
      this.gameMapPosition = new GameMapPosition(var1, this);
   }

   public void checkSystem(Vector3i var1) {
      if (this.state.getController().getClientChannel() != null) {
         this.state.getController().getClientChannel().getClientMapRequestManager().check(var1);
         this.state.getController().getClientChannel().getGalaxyManagerClient().checkSystemBySectorPos(var1);
      }

   }

   public void cleanUp() {
   }

   public boolean doDraw() {
      if (!this.isMapActive()) {
         return false;
      } else {
         return this.state.getController().getClientChannel() != null && this.state.getController().getClientChannel().getClientMapRequestManager() != null;
      }
   }

   public void draw() {
      if (this.doDraw()) {
         if (!this.checkGLErrorOnce) {
            this.checkGLErrorOnce = Keyboard.isKeyDown(60);
         }

         GL11.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glDisable(3553);
         GlUtil.glDisable(2896);
         GlUtil.glEnable(2848);
         GL11.glHint(3154, 4354);
         GL11.glClear(16640);
         MapControllerManager.selected.clear();
         drawResources = Keyboard.isKeyDown(54) && Keyboard.isKeyDown(12);
         if (debug || this.checkGLErrorOnce) {
            GlUtil.printGlErrorCritical("begin");
         }

         if (debug || this.checkGLErrorOnce) {
            GlUtil.printGlErrorCritical("clear");
         }

         this.drawSystemClose();
         if (debug || this.checkGLErrorOnce) {
            GlUtil.printGlErrorCritical("after sys close");
         }

         this.drawHighlights();
         if (debug || this.checkGLErrorOnce) {
            GlUtil.printGlErrorCritical("after highlights");
         }

         this.drawNavConnection();
         this.drawFTLConnections();
         this.drawTradeRoutes();
         this.drawGalaxyBack();
         this.drawSystemCloseFront();
         this.drawCoordinateSystem();
         if (debug || this.checkGLErrorOnce) {
            GlUtil.printGlErrorCritical("after close systems");
         }

         this.drawGalaxy();
         if (debug || this.checkGLErrorOnce) {
            GlUtil.printGlErrorCritical("after galaxy");
         }

         this.drawOrientation();
         if (debug || this.checkGLErrorOnce) {
            GlUtil.printGlErrorCritical("after galaxy");
         }

         GlUtil.glDisable(2848);
         this.checkGLErrorOnce = false;
      }
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.camera = new GameMapCamera(this.state, this.gameMapPosition);
      this.camera.setCameraStartOffset(88.487305F);
      this.camera.setCameraOffset(88.487305F);
      this.camera.alwaysAllowWheelZoom = true;
      this.npcStatus = new GUIProgressBar(this.getState(), 200.0F, 24.0F, new Object() {
         public String toString() {
            return GameMapDrawer.this.npcStatus.getPercent() < 0.0F ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GAMEMAP_GAMEMAPDRAWER_2 : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GAMEMAP_GAMEMAPDRAWER_3, GameMapDrawer.this.npcStatus.getPercent() * 100.0F);
         }
      }, true, (GUICallback)null);
      this.npcStatus.onInit();
      this.mY.setIdentity();
      this.mY.rotY(1.5707964F);
      this.mYB.setIdentity();
      this.mYB.rotY(-1.5707964F);
      this.mYC.setIdentity();
      this.mYC.rotY(3.1415927F);
      this.mX.setIdentity();
      this.mX.rotX(1.5707964F);
      this.mXB.setIdentity();
      this.mXB.rotX(-1.5707964F);
      this.mXC.setIdentity();
      this.mXC.rotX(3.1415927F);
      this.worldpos.setIdentity();
      this.worldToScreenConverter = new WorldToScreenConverter();
      this.state.getCurrentGalaxy().onInit();
      this.galaxyOrientationElement = new GalaxyOrientationElement(this.state);
      this.galaxyOrientationElement.onInit();
      this.systemWallText = new GUITextOverlay[12];

      for(int var1 = 0; var1 < this.systemWallText.length; ++var1) {
         this.systemWallText[var1] = new GUITextOverlay(100, 20, FontLibrary.getBoldArial32(), this.state);
         this.systemWallText[var1].setTextSimple("TEST");
      }

      this.createCoordinateSystem();
   }

   public Vector3i getPlayerSystem() {
      return VoidSystem.getContainingSystem(this.getPlayerSector(), this.sys);
   }

   public Vector3i getPlayerSector() {
      return !this.state.getPlayer().isInTutorial() && !this.state.getPlayer().isInTestSector() && !this.state.getPlayer().isInPersonalSector() ? this.state.getPlayer().getCurrentSector() : new Vector3i(0, 0, 0);
   }

   private boolean drawAllFleets() {
      return !this.getState().getGameState().isFow();
   }

   private boolean showAllTrades() {
      return !this.getState().getGameState().isFow();
   }

   private boolean seeAllFov() {
      return !this.getState().getGameState().isFow();
   }

   private boolean drawAllFleetTarget() {
      return !this.getState().getGameState().isFow();
   }

   private void drawInner() {
      Set var1 = this.state.getController().getClientChannel().getClientMapRequestManager().getSystemMap().entrySet();
      VoidSystem.getPosFromSector(this.getPlayerSector(), this.playerSystem);
      Iterator var4 = var1.iterator();

      while(var4.hasNext()) {
         Entry var2 = (Entry)var4.next();
         boolean var3;
         if (var3 = this.gameMapPosition.isActive((Vector3i)var2.getKey())) {
            HudIndicatorOverlay.toDrawMapInterfaces.add(var2.getValue());
            GlUtil.glPushMatrix();
            GlUtil.translateModelview((float)((Vector3i)var2.getKey()).x * 100.0F, (float)((Vector3i)var2.getKey()).y * 100.0F, (float)((Vector3i)var2.getKey()).z * 100.0F);
            GlUtil.translateModelview(-50.0F, -50.0F, -50.0F);
            GlUtil.glDisable(2896);
            GlUtil.glEnable(2903);
            GlUtil.glEnable(3042);
            GlUtil.glBlendFunc(770, 771);
            this.camera.updateFrustum();
            this.drawNodesSprites(var2, var3);
            GlUtil.glEnable(2896);
            GlUtil.glDisable(3553);
            GlUtil.glEnable(2884);
            GlUtil.glEnable(3042);
            GlUtil.glEnable(2903);
            GlUtil.glBlendFunc(770, 771);
            Vector3i var5;
            if (var3 && !(var5 = this.gameMapPosition.get(new Vector3i())).equals(this.getPlayerSector()) && this.isVisibleSector(var5)) {
               GlUtil.glPushMatrix();
               GlUtil.translateModelview((float)(-((Vector3i)var2.getKey()).x) * 100.0F, (float)(-((Vector3i)var2.getKey()).y) * 100.0F, (float)(-((Vector3i)var2.getKey()).z) * 100.0F);
               this.drawSelectedSector();
               GlUtil.glPopMatrix();
            }

            GlUtil.glPopMatrix();
         }
      }

      this.drawOwnPosition();
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
   }

   private void drawFleets() {
      if (this.drawAllFleets()) {
         synchronized(this.state) {
            this.drawFleet(this.getState().getFleetManager().fleetCache.keySet());
         }
      } else {
         synchronized(this.state) {
            this.drawFleet(this.getState().getFleetManager().fleetCache.keySet());
         }
      }
   }

   private void drawFleet(LongSet var1) {
      String var2 = this.getState().getPlayer().getName().toLowerCase(Locale.ENGLISH);
      if (var1 != null) {
         Iterator var8 = var1.iterator();

         while(true) {
            Fleet var3;
            boolean var5;
            FleetMember var6;
            boolean var9;
            do {
               do {
                  long var4;
                  do {
                     do {
                        if (!var8.hasNext()) {
                           return;
                        }

                        var4 = (Long)var8.next();
                     } while((var3 = (Fleet)this.getState().getFleetManager().fleetCache.get(var4)) == null);
                  } while(var3.getMembers().isEmpty());

                  var9 = var2.equals(var3.getOwner().toLowerCase(Locale.ENGLISH));
                  var5 = this.isVisibleSystem(this.getState().getPlayer().getCurrentSystem()) && SystemRange.isInSystem(var3.getFlagShip().getSector(), this.getState().getPlayer().getCurrentSystem());
                  var6 = (FleetMember)var3.getMembers().get(0);
                  if (this.drawAllFleets() || var9 || var5) {
                     GlUtil.glPushMatrix();
                     this.r.setSeed(var6.entityDbId);
                     GlUtil.translateModelview(-46.875F + (this.r.nextFloat() * 6.25F - 3.125F), -46.875F + (this.r.nextFloat() * 6.25F - 3.125F), -46.875F + (this.r.nextFloat() * 6.25F - 3.125F));
                     GlUtil.glDisable(2896);
                     GlUtil.glEnable(2903);
                     GlUtil.glEnable(3042);
                     GlUtil.glBlendFunc(770, 771);
                     Sprite var7;
                     (var7 = Controller.getResLoader().getSprite("map-sprites-8x2-c-gui-")).setBillboard(true);
                     var7.setBlend(true);
                     var7.setFlip(true);
                     HudIndicatorOverlay.toDrawFleet.add(var6.mapEntry);
                     this.gameMapPosition.get(positionVec);
                     this.camera.updateFrustum();
                     Sprite.draw3D(var7, (PositionableSubSprite[])(new PositionableSubColorSprite[]{var6.mapEntry}), this.camera);
                     var7.setBillboard(false);
                     var7.setFlip(false);
                     GlUtil.glDisable(3553);
                     GlUtil.glEnable(2896);
                     GlUtil.glEnable(2884);
                     GlUtil.glEnable(3042);
                     GlUtil.glEnable(2903);
                     GlUtil.glBlendFunc(770, 771);
                     GlUtil.glPopMatrix();
                  }
               } while(var3.getCurrentMoveTarget() == null);
            } while(!this.drawAllFleetTarget() && !var9 && !var5 && !this.canSeeTarget(var6.getSector(), var3.getCurrentMoveTarget()));

            this.startDrawDottedLine();
            this.drawDottedLine(var6.getSector(), var3.getCurrentMoveTarget(), new Vector4f(0.0F, 1.0F, 1.0F, 1.0F), this.time);
            this.endDrawDottedLine();
         }
      }
   }

   private boolean canSeeTarget(Vector3i var1, Vector3i var2) {
      if (this.isVisibleSystem(this.getState().getPlayer().getCurrentSystem())) {
         int var3 = this.getState().getPlayer().getCurrentSystem().x << 4;
         int var4 = this.getState().getPlayer().getCurrentSystem().y << 4;
         int var5 = this.getState().getPlayer().getCurrentSystem().z << 4;
         int var6 = var3 + 16;
         int var7 = var4 + 16;
         int var8 = var5 + 16;
         this.aabbMin.set((float)var3, (float)var4, (float)var5);
         this.aabbMax.set((float)var6, (float)var7, (float)var8);
         this.fromTmp.set((float)var1.x, (float)var1.y, (float)var1.z);
         this.toTmp.set((float)var2.x, (float)var2.y, (float)var2.z);
         this.param[0] = 1.0F;
         return AabbUtil2.rayAabb(this.fromTmp, this.toTmp, this.aabbMin, this.aabbMax, this.param, this.normal);
      } else {
         return false;
      }
   }

   private boolean isVisibleSector(Vector3i var1) {
      return this.seeAllFov() || this.getState().getController().getClientChannel().getGalaxyManagerClient().isSectorVisiblyClientIncludingLastVisited(var1);
   }

   private boolean isVisibleSystem(Vector3i var1) {
      return this.seeAllFov() || this.getState().getController().getClientChannel().getGalaxyManagerClient().isSystemVisiblyClient(var1);
   }

   private void drawLocalSystemBack(Map var1) {
      GlUtil.glPushMatrix();
      GlUtil.translateModelview((float)this.gameMapPosition.getCurrentSysPos().x * 100.0F, (float)this.gameMapPosition.getCurrentSysPos().y * 100.0F, (float)this.gameMapPosition.getCurrentSysPos().z * 100.0F);
      GL11.glCullFace(1028);
      VoidSystem var2;
      if ((var2 = (VoidSystem)var1.get(this.gameMapPosition.getCurrentSysPos())) == null || var2.getOwnerFaction() == 0 || !this.state.getFactionManager().existsFaction(var2.getOwnerFaction())) {
         this.drawBox(50.0F, 0.45F, true);
      }

      GL11.glCullFace(1029);
      GlUtil.glPopMatrix();
   }

   private void drawLocalSystemFront(Map var1) {
      GlUtil.glPushMatrix();
      GlUtil.translateModelview((float)this.gameMapPosition.getCurrentSysPos().x * 100.0F, (float)this.gameMapPosition.getCurrentSysPos().y * 100.0F, (float)this.gameMapPosition.getCurrentSysPos().z * 100.0F);
      VoidSystem var2;
      if ((var2 = (VoidSystem)var1.get(this.gameMapPosition.getCurrentSysPos())) == null || var2.getOwnerFaction() == 0 || !this.state.getFactionManager().existsFaction(var2.getOwnerFaction())) {
         this.drawBox(50.0F, 0.45F, true);
      }

      for(int var3 = 0; var3 < 12; ++var3) {
         this.drawBoxTexts(50.0F, 1.0F, this.gameMapPosition.getCurrentSysPos(), var3, var3 > 5);
      }

      GlUtil.glPopMatrix();
   }

   private void drawGrids() {
      System.err.println("SHADER::: " + GlUtil.loadedShader);
      GlUtil.glPushMatrix();
      GlUtil.translateModelview((float)this.gameMapPosition.getCurrentSysPos().x * 100.0F - 50.0F, (float)this.gameMapPosition.getCurrentSysPos().y * 100.0F - 50.0F, (float)this.gameMapPosition.getCurrentSysPos().z * 100.0F - 50.0F);
      GlUtil.glDisable(2896);
      GlUtil.glEnable(2903);
      GlUtil.glEnable(3553);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glDisable(3553);
      GlUtil.glColor4fForced(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      this.drawSystemGrid(-100.0F, 100.0F, 1.0F, 1.0F);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glDisable(2896);
      GlUtil.glEnable(2903);
      GlUtil.glEnable(3553);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glDisable(3553);
      GlUtil.glColor4fForced(1.0F, 1.0F, 1.0F, 1.0F);
      this.drawGrid(100.0F, 6.25F, 0.8F, 0.9F);
      GlUtil.glPopMatrix();
   }

   private void drawSystemClose() {
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      this.camera.lookAt(true);
      this.camera.updateFrustum();
      AbstractScene.mainLight.draw();
      GlUtil.glEnable(2884);
      GlUtil.glEnable(3042);
      GlUtil.glEnable(2903);
      GlUtil.glDisable(3553);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glDisable(2896);
      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glPushMatrix();
      this.worldToScreenConverter.storeCurrentModelviewProjection();
      GlUtil.glPopMatrix();
      GlUtil.glColor4fForced(1.0F, 1.0F, 1.0F, 1.0F);
      Map var1 = this.state.getController().getClientChannel().getGalaxyManagerClient().getClientData();
      this.drawLocalSystemBack(var1);
      GlUtil.glColor4fForced(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glDisable(2896);
      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(3553, 0);
      this.drawGrids();
      Transform var2;
      (var2 = new Transform()).setIdentity();
      var2.origin.set((float)this.gameMapPosition.getCurrentSysPos().x * 100.0F, (float)this.gameMapPosition.getCurrentSysPos().y * 100.0F, (float)this.gameMapPosition.getCurrentSysPos().z * 100.0F);
      this.unexploredIndication = new ConstantIndication(var2, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GAMEMAP_GAMEMAPDRAWER_0, this.getState().getGameState().getSectorsToExploreForSystemScan()));
      GlUtil.glDisable(2896);
      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(3553, 0);
      if (this.isVisibleSystem(this.gameMapPosition.getCurrentSysPos())) {
         this.drawInner();
         this.drawOrbits();
      } else {
         GlUtil.glDisable(2896);
         GlUtil.glEnable(2903);
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         GlUtil.glDisable(3553);
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glEnable(2884);
         GlUtil.glEnable(3042);
         GlUtil.glEnable(2903);
         GlUtil.glBlendFunc(770, 771);
         this.drawOwnPosition();
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GlUtil.glDisable(2896);
         GlUtil.glDisable(3553);
         GlUtil.glBindTexture(3553, 0);
         this.unexploredIndication.setText(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GAMEMAP_GAMEMAPDRAWER_1, this.getState().getPlayer().getFogOfWar().getScannedCountForSystem(this.gameMapPosition.getCurrentSysPos()), this.getState().getGameState().getSectorsToExploreForSystemScan()));
         HudIndicatorOverlay.toDrawMapTexts.add(this.unexploredIndication);
      }

      this.drawTradeNodes();
      this.drawFleets();
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glDisable(3553);
      GlUtil.glEnable(2896);
      GlUtil.glDisable(2903);
      GlUtil.glEnable(2884);
      GlUtil.glDisable(3042);
      GL11.glCullFace(1029);
      GlUtil.glDisable(3042);
      GlUtil.glEnable(2929);
      GlUtil.glDisable(2903);
      GlUtil.glEnable(2896);
      GlUtil.glPopMatrix();
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
   }

   private void drawSystemCloseFront() {
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      this.camera.lookAt(true);
      this.camera.updateFrustum();
      AbstractScene.mainLight.draw();
      GlUtil.glEnable(2884);
      GlUtil.glEnable(3042);
      GlUtil.glEnable(2903);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glPushMatrix();
      this.worldToScreenConverter.storeCurrentModelviewProjection();
      GlUtil.glPopMatrix();
      Map var1 = this.state.getController().getClientChannel().getGalaxyManagerClient().getClientData();
      this.drawLocalSystemFront(var1);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glDisable(3553);
      GlUtil.glEnable(2896);
      GlUtil.glDisable(2903);
      GlUtil.glEnable(2884);
      GlUtil.glDisable(3042);
      GL11.glCullFace(1029);
      GlUtil.glDisable(3042);
      GlUtil.glEnable(2929);
      GlUtil.glDisable(2903);
      GlUtil.glEnable(2896);
      GlUtil.glPopMatrix();
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
   }

   private void drawOrientation() {
      GL11.glClear(256);
      this.state.getCurrentGalaxy();
      GlUtil.glMatrixMode(5889);
      GlUtil.glPushMatrix();
      float var1 = (float)GLFrame.getWidth() / (float)GLFrame.getHeight();
      GlUtil.gluPerspective(Controller.projectionMatrix, (Float)EngineSettings.G_FOV.getCurrentState(), var1, 10.0F, 25000.0F, true);
      GlUtil.glMatrixMode(5888);
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      this.camera.lookAt(true);
      if (debug) {
         GlUtil.printGlErrorCritical("after galaxy intern draw");
      }

      this.galaxyOrientationElement.draw();
      GlUtil.glPopMatrix();
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glMatrixMode(5889);
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5888);
   }

   private void drawGalaxyFactions() {
      if (debug || this.checkGLErrorOnce) {
         GlUtil.printGlErrorCritical("before factions");
      }

      Map var1 = this.state.getController().getClientChannel().getGalaxyManagerClient().getClientData();
      highlightIcon = null;
      Iterator var2 = var1.entrySet().iterator();

      while(true) {
         while(var2.hasNext()) {
            Entry var3;
            Vector3i var4 = (Vector3i)(var3 = (Entry)var2.next()).getKey();
            this.bufferBoxFaction(49.99F, 0.25F, var4, var1, this.quadBuffer);
            if (!this.getState().getGameState().isNpcDebug() && this.getState().getFactionManager().getFaction(((VoidSystem)var3.getValue()).getOwnerFaction()) != null && var4.equals(this.getGameMapPosition().getCurrentSysPos())) {
               float var10 = this.getState().getGameState().getClientNPCSystemStatus(((VoidSystem)var3.getValue()).getOwnerFaction(), var4);
               ObjectArrayList var5 = new ObjectArrayList();
               Faction var6;
               if ((var6 = this.getState().getFactionManager().getFaction(((VoidSystem)var3.getValue()).getOwnerFaction())) != null) {
                  var5.add(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GAMEMAP_GAMEMAPDRAWER_6);
                  var5.add(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GAMEMAP_GAMEMAPDRAWER_7, var6.getName()));
                  FactionRelation.RType var11 = this.getState().getFactionManager().getRelation(this.getState().getPlayerName(), this.getState().getPlayer().getFactionId(), var6.getIdFaction());
                  switch(var11) {
                  case ENEMY:
                     var5.add(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GAMEMAP_GAMEMAPDRAWER_8);
                     break;
                  case FRIEND:
                     var5.add(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GAMEMAP_GAMEMAPDRAWER_9);
                     break;
                  case NEUTRAL:
                     var5.add(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GAMEMAP_GAMEMAPDRAWER_10);
                  }

                  if (FactionManager.isNPCFaction(((VoidSystem)var3.getValue()).getOwnerFaction())) {
                     if (var10 < 0.0F) {
                        var5.add(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GAMEMAP_GAMEMAPDRAWER_11, var10));
                     } else {
                        var5.add(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GAMEMAP_GAMEMAPDRAWER_4, StringTools.formatPointZero(var10 * 100.0F)));
                     }
                  }
               } else {
                  var5.add(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GAMEMAP_GAMEMAPDRAWER_5, var10));
               }

               if (((VoidSystem)var3.getValue()).getOwnerPos() != null) {
                  highlightIcon = ((VoidSystem)var3.getValue()).getOwnerPos();
               }

               StringBuffer var12 = new StringBuffer();

               for(int var7 = 0; var7 < var5.size(); ++var7) {
                  var12.append((String)var5.get(var7));
                  var12.append("\n");
               }

               PlayerPanel.infoText = var12.toString();
            } else {
               NPCSystemStub var9;
               if (this.getState().getGameState().isNpcDebug() && FactionManager.isNPCFaction(((VoidSystem)var3.getValue()).getOwnerFaction()) && var4.equals(this.getGameMapPosition().getCurrentSysPos()) && (var9 = (NPCSystemStub)this.getState().getGameState().getClientNPCSystemMap().get(var4)) != null && (NPCFaction)this.getState().getFactionManager().getFaction(var9.getFactionId()) != null) {
                  highlightIcon = var9.systemBase;
                  PlayerPanel.infoText = String.format("FACTION %s\n\nStatus: %s%%\nResAvail: %s%% (mined %s)\nDiplomacy: %s\nLevel: %s\nDistFact: %s\nBaseUID: %s\nBaseSector: %s\nWeight: %s\nLost: %s\n\nContingent:\n%s\n\n", var9.getFactionId(), StringTools.formatPointZero(var9.status * 100.0D), StringTools.formatPointZero(var9.resourcesAvailable * 100.0F), var9.minedResources, ((NPCFaction)this.getState().getFactionManager().getFaction(var9.getFactionId())).getDiplomacy().printFor(this.getState().getPlayer()), var9.getLevel(), var9.distanceFactor, var9.systemBaseUID, var9.systemBase, var9.getWeight(), var9.getContingent().spawnedEntities.getLostEntities().size(), var9.getContingent());
               }
            }
         }

         Collections.sort(this.quadBuffer);
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glDisable(3553);
         GlUtil.glDisable(2896);
         GlUtil.glEnable(2903);
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         GL11.glCullFace(1029);
         GlUtil.glBegin(7);

         GameMapDrawer.Quad var8;
         for(var2 = this.quadBuffer.iterator(); var2.hasNext(); this.freeQuad(var8)) {
            if ((var8 = (GameMapDrawer.Quad)var2.next()).userData == this.camInsideFactionSystem) {
               var8.verticesReverse();
            } else {
               var8.vertices();
            }
         }

         GlUtil.glEnd();
         if (debug || this.checkGLErrorOnce) {
            GlUtil.printGlErrorCritical("after factions aft end");
         }

         this.quadBuffer.clear();
         this.camInsideFactionSystem = 0;
         return;
      }
   }

   private void drawGalaxyResources() {
      GlUtil.glEnable(2884);
      GlUtil.glEnable(3042);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glDisable(3553);
      GlUtil.glDisable(2896);
      GlUtil.glEnable(2903);
      GlUtil.glBlendFunc(770, 771);
      this.state.getController().getClientChannel().getGalaxyManagerClient().getClientData();
      Galaxy var1 = this.state.getCurrentGalaxy();
      FactionResourceRequestContainer var2 = new FactionResourceRequestContainer();
      GalaxyTmpVars var3 = new GalaxyTmpVars();
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      Vector3i var4 = new Vector3i();

      for(int var5 = -16; var5 < 16; ++var5) {
         for(int var6 = -16; var6 < 16; ++var6) {
            for(int var7 = -16; var7 < 16; ++var7) {
               var4.set(var7, var6, var5);
               var1.getSystemResources(var4, var2, var3);
               if (var2.res[0] > 70) {
                  GlUtil.glPushMatrix();
                  GlUtil.translateModelview((float)var4.x * 100.0F, (float)var4.y * 100.0F, (float)var4.z * 100.0F);
                  this.drawBox(50.0F, (float)var2.res[0] / 127.0F, false);
                  GlUtil.glPopMatrix();
               }
            }
         }
      }

   }

   private void drawGalaxyBack() {
      GlUtil.glMatrixMode(5889);
      GlUtil.glPushMatrix();
      float var1 = (float)GLFrame.getWidth() / (float)GLFrame.getHeight();
      GlUtil.gluPerspective(Controller.projectionMatrix, (Float)EngineSettings.G_FOV.getCurrentState(), var1, 10.0F, 25000.0F, true);
      GlUtil.glMatrixMode(5888);
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      this.camera.lookAt(true);
      GL11.glCullFace(1028);
      if (drawResources) {
         this.drawGalaxyResources();
      }

      GL11.glCullFace(1029);
      GlUtil.glPopMatrix();
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glMatrixMode(5889);
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5888);
      GlUtil.glDisable(2903);
      GlUtil.glEnable(2884);
      GlUtil.glDisable(3042);
      GlUtil.glDisable(3553);
   }

   private void drawGalaxy() {
      if (!Galaxy.USE_GALAXY) {
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      } else {
         Galaxy var1 = this.state.getCurrentGalaxy();
         GlUtil.glMatrixMode(5889);
         GlUtil.glPushMatrix();
         float var2 = (float)GLFrame.getWidth() / (float)GLFrame.getHeight();
         GlUtil.gluPerspective(Controller.projectionMatrix, (Float)EngineSettings.G_FOV.getCurrentState(), var2, 10.0F, EngineSettings.G_DRAW_SURROUNDING_GALAXIES_IN_MAP.isOn() ? 75000.0F : 25000.0F, true);
         GlUtil.glMatrixMode(5888);
         GlUtil.glPushMatrix();
         GlUtil.glLoadIdentity();
         this.camera.lookAt(true);
         if (debug) {
            GlUtil.printGlErrorCritical("after proj");
         }

         AbstractScene.mainLight.draw();
         GlUtil.glEnable(2884);
         GlUtil.glEnable(3042);
         GlUtil.glEnable(2903);
         GlUtil.glBlendFunc(770, 771);
         if (debug) {
            GlUtil.printGlErrorCritical("after blend");
         }

         GlUtil.glPushMatrix();
         this.worldToScreenConverter.storeCurrentModelviewProjection();
         GlUtil.glPopMatrix();
         if (debug) {
            GlUtil.printGlErrorCritical("after convert");
         }

         this.gameMapPosition.get(positionVec);
         if (drawFactionTerritory) {
            this.drawGalaxyFactions();
         }

         if (drawResources) {
            this.drawGalaxyResources();
         }

         if (debug) {
            GlUtil.printGlErrorCritical("after Box");
         }

         if (Galaxy.USE_GALAXY) {
            if (!EngineSettings.G_DRAW_SURROUNDING_GALAXIES_IN_MAP.isOn()) {
               var1.draw(this.camera, 100, 0.25F);
            } else {
               for(int var4 = -1; var4 < 2; ++var4) {
                  for(int var5 = -1; var5 < 2; ++var5) {
                     for(int var3 = -1; var3 < 2; ++var3) {
                        this.getState().getCurrentGalaxyNeighbor(new Vector3i(var3, var5, var4)).draw(this.camera, 100, 0.25F);
                     }
                  }
               }
            }

            GlUtil.glDisable(2903);
            GlUtil.glEnable(2884);
            GlUtil.glDisable(3042);
            GlUtil.glDisable(3553);
         }

         if (debug) {
            GlUtil.printGlErrorCritical("after galaxy");
         }

         GlUtil.glDisable(2903);
         GlUtil.glEnable(2884);
         GlUtil.glDisable(3042);
         GlUtil.glDisable(3553);
         if (debug) {
            GlUtil.printGlErrorCritical("after galaxy intern draw");
         }

         GlUtil.glPopMatrix();
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GlUtil.glMatrixMode(5889);
         GlUtil.glPopMatrix();
         GlUtil.glMatrixMode(5888);
      }
   }

   private void createCoordinateSystem() {
      this.coordList = GL11.glGenLists(1);
      GL11.glNewList(this.coordList, 4864);
      GlUtil.glBegin(1);
      GlUtil.glColor4f(0.5F, 0.0F, 0.0F, 0.5F);
      GL11.glVertex3f(0.0F, 0.0F, 0.0F);
      GL11.glVertex3f(6400.0F, 0.0F, 0.0F);
      GL11.glVertex3f(0.0F, 0.0F, 0.0F);
      GL11.glVertex3f(-6400.0F, 0.0F, 0.0F);
      GlUtil.glColor4f(0.0F, 0.5F, 0.0F, 0.5F);
      GL11.glVertex3f(0.0F, 0.0F, 0.0F);
      GL11.glVertex3f(0.0F, 6400.0F, 0.0F);
      GL11.glVertex3f(0.0F, 0.0F, 0.0F);
      GL11.glVertex3f(0.0F, -6400.0F, 0.0F);
      GlUtil.glColor4f(0.0F, 0.0F, 0.5F, 0.5F);
      GL11.glVertex3f(0.0F, 0.0F, 0.0F);
      GL11.glVertex3f(0.0F, 0.0F, 6400.0F);
      GL11.glVertex3f(0.0F, 0.0F, 0.0F);
      GL11.glVertex3f(0.0F, 0.0F, -6400.0F);
      GlUtil.glEnd();
      GL11.glEndList();
   }

   private void drawCoordinateSystem() {
      GlUtil.glDisable(2896);
      GlUtil.glDisable(3553);
      GlUtil.glEnable(2903);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glMatrixMode(5889);
      GlUtil.glPushMatrix();
      float var1 = (float)GLFrame.getWidth() / (float)GLFrame.getHeight();
      GlUtil.gluPerspective(Controller.projectionMatrix, (Float)EngineSettings.G_FOV.getCurrentState(), var1, 10.0F, 25000.0F, true);
      GlUtil.glMatrixMode(5888);
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      this.camera.lookAt(true);
      GlUtil.translateModelview(-50.0F, -50.0F, -50.0F);
      GL11.glCallList(this.coordList);
      GlUtil.glPopMatrix();
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glMatrixMode(5889);
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5888);
   }

   public GameClientState getState() {
      return this.state;
   }

   private void drawNavConnection() {
      Vector3i var1;
      if ((var1 = this.state.getController().getClientGameData().getWaypoint()) != null) {
         GlUtil.glDisable(2896);
         GlUtil.glDisable(3553);
         GlUtil.glEnable(2903);
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         GlUtil.glMatrixMode(5889);
         GlUtil.glPushMatrix();
         float var2 = (float)GLFrame.getWidth() / (float)GLFrame.getHeight();
         GlUtil.gluPerspective(Controller.projectionMatrix, (Float)EngineSettings.G_FOV.getCurrentState(), var2, 10.0F, 25000.0F, true);
         GlUtil.glMatrixMode(5888);
         GlUtil.glPushMatrix();
         GlUtil.glLoadIdentity();
         this.camera.lookAt(true);
         GlUtil.translateModelview(-50.0F, -50.0F, -50.0F);
         GlUtil.glBegin(1);
         Vector3i var3 = this.getPlayerSector();
         GlUtil.glColor4f(0.1F + HudIndicatorOverlay.selectColorValue, 0.8F + HudIndicatorOverlay.selectColorValue, 0.6F + HudIndicatorOverlay.selectColorValue, 0.4F + HudIndicatorOverlay.selectColorValue);
         GL11.glVertex3f((float)var3.x * 6.25F + 3.125F, (float)var3.y * 6.25F + 3.125F, (float)var3.z * 6.25F + 3.125F);
         GL11.glVertex3f((float)var1.x * 6.25F + 3.125F, (float)var1.y * 6.25F + 3.125F, (float)var1.z * 6.25F + 3.125F);
         GlUtil.glEnd();
         GlUtil.glPopMatrix();
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GlUtil.glMatrixMode(5889);
         GlUtil.glPopMatrix();
         GlUtil.glMatrixMode(5888);
      }

   }

   public void drawTradeRoutes() {
      List var1 = this.state.getGameState().getTradeManager().getTradeActiveMap().getTradeList();
      this.startDrawDottedLine();
      Vector4f var2 = new Vector4f(0.1F, 1.0F, 0.1775F, 0.7F);
      Iterator var4 = var1.iterator();

      while(true) {
         TradeActive var3;
         do {
            do {
               if (!var4.hasNext()) {
                  this.endDrawDottedLine();
                  return;
               }
            } while((var3 = (TradeActive)var4.next()).getSectorWayPoints().isEmpty());
         } while(!this.showAllTrades() && !var3.canView(this.state.getPlayer()) && !this.canSeeTarget(var3.getStartSector(), (Vector3i)var3.getSectorWayPoints().get(0)));

         this.drawDottedLine(var3.getStartSector(), (Vector3i)var3.getSectorWayPoints().get(0), var2, this.time);
      }
   }

   public void drawTradeNodes() {
      Iterator var1 = this.state.getController().getClientChannel().getGalaxyManagerClient().getTradeNodeDataById().values().iterator();

      while(var1.hasNext()) {
         TradeNodeClient var2 = (TradeNodeClient)((TradeNodeStub)var1.next());
         if (this.isVisibleSector(var2.getSector())) {
            GlUtil.glPushMatrix();
            this.r.setSeed(var2.getEntityDBId());
            GlUtil.translateModelview(-46.875F + (this.r.nextFloat() * 6.25F - 3.125F), -46.875F + (this.r.nextFloat() * 6.25F - 3.125F), -46.875F + (this.r.nextFloat() * 6.25F - 3.125F));
            GlUtil.glDisable(2896);
            GlUtil.glEnable(2903);
            GlUtil.glEnable(3042);
            GlUtil.glBlendFunc(770, 771);
            Sprite var3;
            (var3 = Controller.getResLoader().getSprite("map-sprites-8x2-c-gui-")).setBillboard(true);
            var3.setBlend(true);
            var3.setFlip(true);
            var2.mapEntry.currentSystemInMap.set(this.gameMapPosition.getCurrentSysPos());
            HudIndicatorOverlay.toDrawTradeNodes.add(var2.mapEntry);
            this.gameMapPosition.get(positionVec);
            this.camera.updateFrustum();
            Sprite.draw3D(var3, (PositionableSubSprite[])(new PositionableSubColorSprite[]{var2.mapEntry}), this.camera);
            var3.setBillboard(false);
            var3.setFlip(false);
            GlUtil.glDisable(3553);
            GlUtil.glEnable(2896);
            GlUtil.glEnable(2884);
            GlUtil.glEnable(3042);
            GlUtil.glEnable(2903);
            GlUtil.glBlendFunc(770, 771);
            GlUtil.glPopMatrix();
         }
      }

   }

   private void endDrawDottedLine() {
      GlUtil.glEnd();
      GlUtil.glPopMatrix();
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glMatrixMode(5889);
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5888);
   }

   private void startDrawDottedLine() {
      this.state.getController().getClientChannel().getGalaxyManagerClient().getFtlData();
      GlUtil.glDisable(2896);
      GlUtil.glDisable(3553);
      GlUtil.glEnable(2903);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glMatrixMode(5889);
      GlUtil.glPushMatrix();
      float var1 = (float)GLFrame.getWidth() / (float)GLFrame.getHeight();
      GlUtil.gluPerspective(Controller.projectionMatrix, (Float)EngineSettings.G_FOV.getCurrentState(), var1, 10.0F, 25000.0F, true);
      GlUtil.glMatrixMode(5888);
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      this.camera.lookAt(true);
      GlUtil.translateModelview(-50.0F, -50.0F, -50.0F);
      GlUtil.glBegin(1);
   }

   private void drawDottedLine(Vector3i var1, Vector3i var2, Vector4f var3, float var4) {
      Vector3f var5 = new Vector3f();
      Vector3f var6 = new Vector3f();
      var5.set((float)var1.x * 6.25F + 3.125F, (float)var1.y * 6.25F + 3.125F, (float)var1.z * 6.25F + 3.125F);
      var6.set((float)var2.x * 6.25F + 3.125F, (float)var2.y * 6.25F + 3.125F, (float)var2.z * 6.25F + 3.125F);
      if (!var5.equals(var6)) {
         Vector3f var9 = new Vector3f();
         Vector3f var11 = new Vector3f();
         var9.sub(var6, var5);
         var11.set(var9);
         var11.normalize();
         float var10 = var9.length();
         var6 = new Vector3f();
         Vector3f var7 = new Vector3f();
         float var8 = Math.min(Math.max(2.0F, var10 * 0.1F), 40.0F);
         GlUtil.glColor4f(var3);
         boolean var12 = true;

         for(var4 = var4 % 1.0F * var8 * 2.0F; var4 < var10; var4 += var8 * 2.0F) {
            var6.set(var11);
            var6.scale(var4);
            if (var12) {
               var6.set(0.0F, 0.0F, 0.0F);
               var12 = false;
            }

            var7.set(var11);
            if (var4 + var8 >= var10) {
               var7.scale(var10);
               var4 = var10;
            } else {
               var7.scale(var4 + var8);
            }

            GL11.glVertex3f(var5.x + var6.x, var5.y + var6.y, var5.z + var6.z);
            GL11.glVertex3f(var5.x + var7.x, var5.y + var7.y, var5.z + var7.z);
         }

      }
   }

   private void drawFTLConnections() {
      Object2ObjectOpenHashMap var1 = this.state.getController().getClientChannel().getGalaxyManagerClient().getFtlData();
      GlUtil.glDisable(2896);
      GlUtil.glDisable(3553);
      GlUtil.glEnable(2903);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glMatrixMode(5889);
      GlUtil.glPushMatrix();
      float var2 = (float)GLFrame.getWidth() / (float)GLFrame.getHeight();
      GlUtil.gluPerspective(Controller.projectionMatrix, (Float)EngineSettings.G_FOV.getCurrentState(), var2, 10.0F, 25000.0F, true);
      GlUtil.glMatrixMode(5888);
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      this.camera.lookAt(true);
      GlUtil.translateModelview(-50.0F, -50.0F, -50.0F);
      GlUtil.glBegin(1);
      Vector3f var17 = new Vector3f();
      Vector3f var3 = new Vector3f();
      Vector3f var4 = new Vector3f();
      Vector3f var5 = new Vector3f();
      Vector3f var6 = new Vector3f();
      Vector3f var7 = new Vector3f();
      Vector3f var8 = new Vector3f();
      Vector3f var9 = new Vector3f();
      Vector3f var10 = new Vector3f();
      new Vector3f();
      new Vector3f();
      new Vector3i();
      new Vector3i();
      Iterator var16 = var1.values().iterator();

      while(var16.hasNext()) {
         FTLConnection var11;
         Vector3i var12 = (var11 = (FTLConnection)var16.next()).from;

         for(int var13 = 0; var13 < var11.to.size(); ++var13) {
            Vector3i var14 = (Vector3i)var11.to.get(var13);
            if (this.isVisibleSector(var14) || this.isVisibleSector(var12)) {
               Vector3i var15;
               if ((var15 = (Vector3i)var11.param.get(var13)).x == 0) {
                  if (!drawWarpGates) {
                     continue;
                  }
               } else if (var15.x == 1) {
                  if (!drawWormHoles) {
                     continue;
                  }
               } else if (var15.x == 2) {
                  continue;
               }

               if (var15.x == 0) {
                  GlUtil.glColor4f(0.1F, 0.1F, 1.0F, 1.0F);
               } else if (var15.x == 1) {
                  GlUtil.glColor4f(0.4F, 0.1F, 0.7F, 1.0F);
               }

               var17.set((float)var12.x * 6.25F + 3.125F, (float)var12.y * 6.25F + 3.125F, (float)var12.z * 6.25F + 3.125F);
               var3.set((float)var14.x * 6.25F + 3.125F, (float)var14.y * 6.25F + 3.125F, (float)var14.z * 6.25F + 3.125F);
               GL11.glVertex3f(var17.x, var17.y, var17.z);
               if (var15.x == 0) {
                  GlUtil.glColor4f(0.70000005F, 0.70000005F, 1.0F, 1.0F);
               } else if (var15.x == 1) {
                  GlUtil.glColor4f(1.0F, 0.70000005F, 0.9F, 1.0F);
               }

               GL11.glVertex3f(var3.x, var3.y, var3.z);
               var4.sub(var3, var17);
               var5.set(var4);
               var5.normalize();
               var6.set(0.0F, 1.0F, 0.0F);
               if (var6.equals(var5)) {
                  var6.set(1.0F, 0.0F, 0.0F);
               }

               var8.set(var5);
               var7.cross(var8, var6);
               var7.normalize();
               var6.cross(var7, var8);
               var6.normalize();
               var9.set(var5);
               var9.scale(Math.max(var4.length() / 2.0F, var4.length() - 100.0F));
               var9.add(var17);
               var8.negate();
               var8.scale(8.0F);
               var7.scale(8.0F);
               if (var15.x == 0) {
                  GlUtil.glColor4f(0.3F, 0.3F, 1.0F * this.sinus.getTime(), 1.0F);
               } else if (var15.x == 1) {
                  GlUtil.glColor4f(0.6F * this.sinus.getTime(), 0.3F, 0.7F * this.sinus.getTime(), 1.0F);
               }

               var10.set(var9);
               GL11.glVertex3f(var10.x, var10.y, var10.z);
               var10.add(var7);
               var10.add(var8);
               GL11.glVertex3f(var10.x, var10.y, var10.z);
               var10.set(var9);
               var7.negate();
               GL11.glVertex3f(var10.x, var10.y, var10.z);
               var10.add(var7);
               var10.add(var8);
               GL11.glVertex3f(var10.x, var10.y, var10.z);
            }
         }
      }

      GlUtil.glEnd();
      GlUtil.glPopMatrix();
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glMatrixMode(5889);
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5888);
   }

   private void drawHighlights() {
      GlUtil.glDisable(2896);
      GlUtil.glDisable(3553);
      GlUtil.glEnable(2903);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glMatrixMode(5889);
      GlUtil.glPushMatrix();
      float var1 = (float)GLFrame.getWidth() / (float)GLFrame.getHeight();
      GlUtil.gluPerspective(Controller.projectionMatrix, (Float)EngineSettings.G_FOV.getCurrentState(), var1, 0.1F, 5000.0F, true);
      GlUtil.glMatrixMode(5888);
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      this.camera.lookAt(true);
      this.drawHighlightSector(this.getPlayerSector(), new Vector3f(0.8F, 0.8F, 0.0F), this.sinus.getTime());
      Vector3i var2;
      if ((var2 = this.getState().getController().getClientGameData().getWaypoint()) != null) {
         this.drawHighlightSector(var2, new Vector3f(0.1F + HudIndicatorOverlay.selectColorValue, 0.8F + HudIndicatorOverlay.selectColorValue, 0.6F + HudIndicatorOverlay.selectColorValue), 0.4F + HudIndicatorOverlay.selectColorValue);
      }

      GlUtil.glPopMatrix();
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glMatrixMode(5889);
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5888);
   }

   public void freeQuad(GameMapDrawer.Quad var1) {
      this.quadPool.add(var1);
   }

   public GameMapDrawer.Quad getQuad() {
      return this.quadPool.isEmpty() ? new GameMapDrawer.Quad() : (GameMapDrawer.Quad)this.quadPool.remove(this.quadPool.size() - 1);
   }

   private void bufferBoxFaction(float var1, float var2, Vector3i var3, Map var4, List var5) {
      Faction var6;
      VoidSystem var9;
      if ((var9 = (VoidSystem)var4.get(var3)).getOwnerFaction() != 0 && var9.getOwnerUID() != null && (var6 = this.state.getFactionManager().getFaction(var9.getOwnerFaction())) != null) {
         if (this.camera.getPos().x >= (float)var3.x * 100.0F - 50.0F && this.camera.getPos().y >= (float)var3.y * 100.0F - 50.0F && this.camera.getPos().z >= (float)var3.z * 100.0F - 50.0F && this.camera.getPos().x < (float)var3.x * 100.0F + 50.0F && this.camera.getPos().y < (float)var3.y * 100.0F + 50.0F && this.camera.getPos().z < (float)var3.z * 100.0F + 50.0F) {
            this.camInsideFactionSystem = var9.getOwnerFaction();
         }

         FactionRelation.RType var7 = this.getState().getFactionManager().getRelation(this.getState().getPlayer().getFactionId(), var6.getIdFaction());
         this.tmp.set(var3);
         --this.tmp.z;
         VoidSystem var8;
         GameMapDrawer.Quad var10;
         if ((var8 = (VoidSystem)var4.get(this.tmp)) == null || var8.getOwnerFaction() != var9.getOwnerFaction()) {
            (var10 = this.getQuad()).pos((float)var3.x, (float)var3.y, (float)var3.z);
            var10.userData = var9.getOwnerFaction();
            var5.add(var10);
            var10.front();
            if (drawFactionByRelation) {
               var10.color4ff3(var7.defaultColor.x, var7.defaultColor.y, var7.defaultColor.z, var2);
            } else {
               var10.color4ff3(var6.getColor().x, var6.getColor().y, var6.getColor().z, var2);
            }
         }

         this.tmp.set(var3);
         ++this.tmp.z;
         if ((var8 = (VoidSystem)var4.get(this.tmp)) == null || var8.getOwnerFaction() != var9.getOwnerFaction()) {
            (var10 = this.getQuad()).pos((float)var3.x, (float)var3.y, (float)var3.z);
            var10.userData = var9.getOwnerFaction();
            var5.add(var10);
            var10.back();
            if (drawFactionByRelation) {
               var10.color4ff3(var7.defaultColor.x, var7.defaultColor.y, var7.defaultColor.z, var2);
            } else {
               var10.color4ff3(var6.getColor().x, var6.getColor().y, var6.getColor().z, var2);
            }
         }

         this.tmp.set(var3);
         ++this.tmp.x;
         if ((var8 = (VoidSystem)var4.get(this.tmp)) == null || var8.getOwnerFaction() != var9.getOwnerFaction()) {
            (var10 = this.getQuad()).pos((float)var3.x, (float)var3.y, (float)var3.z);
            var10.userData = var9.getOwnerFaction();
            var5.add(var10);
            var10.left();
            if (drawFactionByRelation) {
               var10.color4ff3(var7.defaultColor.x, var7.defaultColor.y, var7.defaultColor.z, var2);
            } else {
               var10.color4ff3(var6.getColor().x, var6.getColor().y, var6.getColor().z, var2);
            }
         }

         this.tmp.set(var3);
         --this.tmp.x;
         if ((var8 = (VoidSystem)var4.get(this.tmp)) == null || var8.getOwnerFaction() != var9.getOwnerFaction()) {
            (var10 = this.getQuad()).pos((float)var3.x, (float)var3.y, (float)var3.z);
            var10.userData = var9.getOwnerFaction();
            var5.add(var10);
            var10.right();
            if (drawFactionByRelation) {
               var10.color4ff3(var7.defaultColor.x, var7.defaultColor.y, var7.defaultColor.z, var2);
            } else {
               var10.color4ff3(var6.getColor().x, var6.getColor().y, var6.getColor().z, var2);
            }
         }

         this.tmp.set(var3);
         ++this.tmp.y;
         if ((var8 = (VoidSystem)var4.get(this.tmp)) == null || var8.getOwnerFaction() != var9.getOwnerFaction()) {
            (var10 = this.getQuad()).pos((float)var3.x, (float)var3.y, (float)var3.z);
            var10.userData = var9.getOwnerFaction();
            var5.add(var10);
            var10.bottom();
            if (drawFactionByRelation) {
               var10.color4ff3(var7.defaultColor.x, var7.defaultColor.y, var7.defaultColor.z, var2);
            } else {
               var10.color4ff3(var6.getColor().x, var6.getColor().y, var6.getColor().z, var2);
            }
         }

         this.tmp.set(var3);
         --this.tmp.y;
         if ((var8 = (VoidSystem)var4.get(this.tmp)) == null || var8.getOwnerFaction() != var9.getOwnerFaction()) {
            (var10 = this.getQuad()).pos((float)var3.x, (float)var3.y, (float)var3.z);
            var10.userData = var9.getOwnerFaction();
            var5.add(var10);
            var10.top();
            if (drawFactionByRelation) {
               var10.color4ff3(var7.defaultColor.x, var7.defaultColor.y, var7.defaultColor.z, var2);
               return;
            }

            var10.color4ff3(var6.getColor().x, var6.getColor().y, var6.getColor().z, var2);
         }
      }

   }

   private void drawBoxTexts(float var1, float var2, Vector3i var3, int var4, boolean var5) {
      this.posBuffer.x = 0.0F;
      this.posBuffer.y = 0.0F;
      this.posBuffer.z = 0.0F;
      GUITextOverlay var6 = this.systemWallText[var4];
      var4 %= 6;
      this.worldpos.basis.setIdentity();
      Vector3i var7 = Element.DIRECTIONSi[var5 ? Element.OPPOSITE_SIDE[var4] : var4];
      var6.getText().set(0, var3.x + var7.x + ", " + (var3.y + var7.y) + ", " + (var3.z + var7.z));
      int var8 = var5 ? -50 : 50;
      Vector3f var10000;
      switch(var4) {
      case 0:
         this.worldpos.basis.mul(this.mYC);
         var10000 = this.posBuffer;
         var10000.z += (float)var8;
         break;
      case 1:
         var10000 = this.posBuffer;
         var10000.z -= (float)var8;
         break;
      case 2:
         this.worldpos.basis.mul(this.mX);
         var10000 = this.posBuffer;
         var10000.y += (float)var8;
         break;
      case 3:
         this.worldpos.basis.mul(this.mYC);
         this.worldpos.basis.mul(this.mXB);
         var10000 = this.posBuffer;
         var10000.y -= (float)var8;
         break;
      case 4:
         this.worldpos.basis.mul(this.mY);
         var10000 = this.posBuffer;
         var10000.x += (float)var8;
         break;
      case 5:
         this.worldpos.basis.mul(this.mYB);
         var10000 = this.posBuffer;
         var10000.x -= (float)var8;
      }

      this.worldpos.origin.set(this.posBuffer);
      GlUtil.glPushMatrix();
      GlUtil.glMultMatrix(this.worldpos);
      GlUtil.translateModelview(6.0F, 0.0F, 0.0F);
      GlUtil.scaleModelview(-0.1F, -0.1F, 0.1F);
      var6.draw();
      GlUtil.glPopMatrix();
   }

   private void drawBoxColor(float var1, float var2, Vector3f var3) {
      GlUtil.glBegin(7);
      GlUtil.glColor4f(var3.x, var3.y, var3.z, var2);
      GL11.glNormal3f(0.0F, 0.0F, 1.0F);
      GL11.glVertex3f(var1, var1, -var1);
      GL11.glVertex3f(var1, -var1, -var1);
      GL11.glVertex3f(-var1, -var1, -var1);
      GL11.glVertex3f(-var1, var1, -var1);
      GL11.glNormal3f(0.0F, 0.0F, -1.0F);
      GL11.glVertex3f(var1, -var1, var1);
      GL11.glVertex3f(var1, var1, var1);
      GL11.glVertex3f(-var1, var1, var1);
      GL11.glVertex3f(-var1, -var1, var1);
      GL11.glNormal3f(-1.0F, 0.0F, 0.0F);
      GL11.glVertex3f(var1, -var1, -var1);
      GL11.glVertex3f(var1, var1, -var1);
      GL11.glVertex3f(var1, var1, var1);
      GL11.glVertex3f(var1, -var1, var1);
      GL11.glNormal3f(1.0F, 0.0F, 0.0F);
      GL11.glVertex3f(-var1, -var1, var1);
      GL11.glVertex3f(-var1, var1, var1);
      GL11.glVertex3f(-var1, var1, -var1);
      GL11.glVertex3f(-var1, -var1, -var1);
      GL11.glNormal3f(0.0F, -1.0F, 0.0F);
      GL11.glVertex3f(var1, var1, var1);
      GL11.glVertex3f(var1, var1, -var1);
      GL11.glVertex3f(-var1, var1, -var1);
      GL11.glVertex3f(-var1, var1, var1);
      GL11.glNormal3f(0.0F, 1.0F, 0.0F);
      GL11.glVertex3f(var1, -var1, -var1);
      GL11.glVertex3f(var1, -var1, var1);
      GL11.glVertex3f(-var1, -var1, var1);
      GL11.glVertex3f(-var1, -var1, -var1);
      GlUtil.glEnd();
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
   }

   private void drawBox(float var1, float var2, boolean var3) {
      GlUtil.glBegin(7);
      GlUtil.glColor4f(0.3F, 0.0F, 0.0F, var2);
      GL11.glNormal3f(0.0F, 0.0F, 1.0F);
      GL11.glVertex3f(var1, var1, -var1);
      GL11.glVertex3f(var1, -var1, -var1);
      GL11.glVertex3f(-var1, -var1, -var1);
      GL11.glVertex3f(-var1, var1, -var1);
      GlUtil.glColor4f(0.3F, 0.0F, 0.0F, var2);
      GL11.glNormal3f(0.0F, 0.0F, -1.0F);
      GL11.glVertex3f(var1, -var1, var1);
      GL11.glVertex3f(var1, var1, var1);
      GL11.glVertex3f(-var1, var1, var1);
      GL11.glVertex3f(-var1, -var1, var1);
      GlUtil.glColor4f(0.0F, 0.3F, 0.0F, var2);
      GL11.glNormal3f(-1.0F, 0.0F, 0.0F);
      GL11.glVertex3f(var1, -var1, -var1);
      GL11.glVertex3f(var1, var1, -var1);
      GL11.glVertex3f(var1, var1, var1);
      GL11.glVertex3f(var1, -var1, var1);
      GlUtil.glColor4f(0.0F, 0.3F, 0.0F, var2);
      GL11.glNormal3f(1.0F, 0.0F, 0.0F);
      GL11.glVertex3f(-var1, -var1, var1);
      GL11.glVertex3f(-var1, var1, var1);
      GL11.glVertex3f(-var1, var1, -var1);
      GL11.glVertex3f(-var1, -var1, -var1);
      GlUtil.glColor4f(0.0F, 0.0F, 0.3F, var2);
      GL11.glNormal3f(0.0F, -1.0F, 0.0F);
      GL11.glVertex3f(var1, var1, var1);
      GL11.glVertex3f(var1, var1, -var1);
      GL11.glVertex3f(-var1, var1, -var1);
      GL11.glVertex3f(-var1, var1, var1);
      GlUtil.glColor4f(0.0F, 0.0F, 0.3F, var2);
      GL11.glNormal3f(0.0F, 1.0F, 0.0F);
      GL11.glVertex3f(var1, -var1, -var1);
      GL11.glVertex3f(var1, -var1, var1);
      GL11.glVertex3f(-var1, -var1, var1);
      GL11.glVertex3f(-var1, -var1, -var1);
      GlUtil.glEnd();
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
   }

   private void drawGrid(float var1, float var2, float var3, float var4) {
      Vector3i var5;
      Vector3i var10000 = var5 = this.gameMapPosition.get(new Vector3i());
      var10000.x = ByteUtil.modU16(var10000.x);
      var5.y = ByteUtil.modU16(var5.y);
      var5.z = ByteUtil.modU16(var5.z);
      GlUtil.glBegin(1);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var3);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var4);
      GL11.glVertex3f((float)var5.x * var2, (float)var5.y * var2, 0.0F);
      GL11.glVertex3f((float)var5.x * var2, (float)var5.y * var2, var1);
      GL11.glVertex3f(0.0F, (float)var5.y * var2, (float)var5.z * var2);
      GL11.glVertex3f(var1, (float)var5.y * var2, (float)var5.z * var2);
      GL11.glVertex3f((float)var5.x * var2, 0.0F, (float)var5.z * var2);
      GL11.glVertex3f((float)var5.x * var2, var1, (float)var5.z * var2);
      GL11.glVertex3f((float)var5.x * var2, (float)(var5.y + 1) * var2, 0.0F);
      GL11.glVertex3f((float)var5.x * var2, (float)(var5.y + 1) * var2, var1);
      GL11.glVertex3f(0.0F, (float)var5.y * var2, (float)(var5.z + 1) * var2);
      GL11.glVertex3f(var1, (float)var5.y * var2, (float)(var5.z + 1) * var2);
      GL11.glVertex3f((float)var5.x * var2, 0.0F, (float)(var5.z + 1) * var2);
      GL11.glVertex3f((float)var5.x * var2, var1, (float)(var5.z + 1) * var2);
      GL11.glVertex3f((float)(var5.x + 1) * var2, (float)var5.y * var2, 0.0F);
      GL11.glVertex3f((float)(var5.x + 1) * var2, (float)var5.y * var2, var1);
      GL11.glVertex3f(0.0F, (float)(var5.y + 1) * var2, (float)var5.z * var2);
      GL11.glVertex3f(var1, (float)(var5.y + 1) * var2, (float)var5.z * var2);
      GL11.glVertex3f((float)(var5.x + 1) * var2, 0.0F, (float)var5.z * var2);
      GL11.glVertex3f((float)(var5.x + 1) * var2, var1, (float)var5.z * var2);
      GL11.glVertex3f((float)(var5.x + 1) * var2, (float)(var5.y + 1) * var2, 0.0F);
      GL11.glVertex3f((float)(var5.x + 1) * var2, (float)(var5.y + 1) * var2, var1);
      GL11.glVertex3f(0.0F, (float)(var5.y + 1) * var2, (float)(var5.z + 1) * var2);
      GL11.glVertex3f(var1, (float)(var5.y + 1) * var2, (float)(var5.z + 1) * var2);
      GL11.glVertex3f((float)(var5.x + 1) * var2, 0.0F, (float)(var5.z + 1) * var2);
      GL11.glVertex3f((float)(var5.x + 1) * var2, var1, (float)(var5.z + 1) * var2);
      GlUtil.glEnd();
   }

   private void drawOrbits() {
      if (Galaxy.USE_GALAXY) {
         Vector3i var1 = Galaxy.getRelPosInGalaxyFromAbsSystem(this.getGameMapPosition().getCurrentSysPos(), new Vector3i());
         if (this.getState().getCurrentGalaxy().isStellarSystem(var1)) {
            this.getState().getCurrentGalaxy().getSystemOrbits(var1, this.orbits);
            Vector3i var2 = this.getState().getCurrentGalaxy().getSunPositionOffset(var1, new Vector3i());
            this.orbitRot.setIdentity();
            this.getState().getCurrentGalaxy().getAxisMatrix(var1, this.orbitRot.basis);
            GlUtil.glPushMatrix();
            GlUtil.translateModelview((float)this.getGameMapPosition().getCurrentSysPos().x * 100.0F + (float)var2.x * 6.25F + 3.125F, (float)this.getGameMapPosition().getCurrentSysPos().y * 100.0F + (float)var2.y * 6.25F + 3.125F, (float)this.getGameMapPosition().getCurrentSysPos().z * 100.0F + (float)var2.z * 6.25F + 3.125F);
            GlUtil.glMultMatrix(this.orbitRot);
            GlUtil.glDisable(2896);
            GlUtil.glEnable(2903);
            GlUtil.glEnable(3042);
            GlUtil.glBlendFunc(770, 771);
            Sprite var3;
            (var3 = Controller.getResLoader().getSprite("map-sprites-8x2-c-gui-")).setBillboard(true);
            var3.setBlend(true);
            var3.setFlip(true);
            this.gameMapPosition.get(positionVec);
            this.camera.updateFrustum();

            for(int var4 = 0; var4 < this.orbits.length; ++var4) {
               int var5;
               if ((var5 = this.orbits[var4]) > 0) {
                  float var6 = (float)(var4 + 1) * 6.25F + 3.125F;
                  if (Galaxy.isPlanetOrbit(var5)) {
                     if (!drawPlanetOrbits) {
                        continue;
                     }

                     GlUtil.glColor4f(0.8F, 0.2F, 1.0F, 1.0F);
                  } else {
                     if (!drawAsteroidBeltOrbits) {
                        continue;
                     }

                     GlUtil.glColor4f(1.0F, 1.0F, 0.0F, 1.0F);
                  }

                  GlUtil.glBegin(3);

                  for(float var7 = 0.0F; var7 < 6.2831855F; var7 += 0.049087387F) {
                     GL11.glVertex3f(FastMath.cos(var7) * var6, 0.0F, FastMath.sin(var7) * var6);
                  }

                  GL11.glVertex3f(FastMath.cos(0.0F) * var6, 0.0F, FastMath.sin(0.0F) * var6);
                  GlUtil.glEnd();
               }
            }

            var3.setBillboard(false);
            var3.setFlip(false);
            GlUtil.glDisable(3553);
            GlUtil.glEnable(2896);
            GlUtil.glEnable(2884);
            GlUtil.glEnable(3042);
            GlUtil.glEnable(2903);
            GlUtil.glBlendFunc(770, 771);
            GlUtil.glPopMatrix();
            this.orbitRotInv.basis.set(this.orbitRot.basis);
            Vector3f var16 = new Vector3f((float)var2.x * 6.25F, (float)var2.y * 6.25F, (float)var2.z * 6.25F);
            this.orbitRotInv.origin.set(var16);
            this.orbitRotInv.inverse();
            Vector3f var17 = this.getState().getCurrentGalaxy().getSystemAxis(var1, new Vector3f());
            Vector3i var18 = this.getGameMapPosition().getCurrentSysPos();
            if (highlightOrbitSectors) {
               for(int var19 = 0; var19 < 16; ++var19) {
                  for(int var12 = 0; var12 < 16; ++var12) {
                     for(int var13 = 0; var13 < 16; ++var13) {
                        Vector3f var14 = new Vector3f(-50.0F + (float)var13 * 6.25F, -50.0F + (float)var12 * 6.25F, -50.0F + (float)var19 * 6.25F);
                        Vector3f var8 = new Vector3f(-50.0F + (float)var13 * 6.25F + 6.25F, -50.0F + (float)var12 * 6.25F + 6.25F, -50.0F + (float)var19 * 6.25F + 6.25F);
                        BoundingBox var15 = new BoundingBox(var14, var8);
                        if (BoundingBox.intersectsPlane(var16, var17, var15)) {
                           for(int var20 = 0; var20 < this.orbits.length; ++var20) {
                              int var9;
                              if ((var9 = this.orbits[var20]) > 0) {
                                 float var10 = (float)(var20 + 1) * 6.25F + 3.125F;
                                 Vector3f var11;
                                 (var11 = new Vector3f()).sub(var15.getCenter(new Vector3f()), var16);
                                 var11.normalize();
                                 var11.scale(var10);
                                 var11.add(var16);
                                 if (var15.isInside(var11)) {
                                    Vector3f var21;
                                    if (Galaxy.isPlanetOrbit(var9)) {
                                       if (!drawPlanetOrbits) {
                                          continue;
                                       }

                                       var21 = new Vector3f(0.8F, 0.2F, 1.0F);
                                    } else {
                                       if (!drawAsteroidBeltOrbits) {
                                          continue;
                                       }

                                       var21 = new Vector3f(1.0F, 1.0F, 0.0F);
                                    }

                                    Vector3i var22 = new Vector3i((var18.x << 4) + var13, (var18.y << 4) + var12, (var18.z << 4) + var19);
                                    this.drawHighlightSector(var22, var21, 0.6F);
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }

         }
      }
   }

   private void drawOwnPosition() {
      GlUtil.glPushMatrix();
      GlUtil.translateModelview(-50.0F + (float)this.getPlayerSector().x * 6.25F + 3.125F, -50.0F + (float)this.getPlayerSector().y * 6.25F + 3.125F, -50.0F + (float)this.getPlayerSector().z * 6.25F + 3.125F);
      GlUtil.glDisable(2896);
      GlUtil.glEnable(2903);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      Sprite var1;
      (var1 = Controller.getResLoader().getSprite("map-sprites-8x2-c-gui-")).setBillboard(true);
      var1.setBlend(true);
      var1.setFlip(true);
      this.gameMapPosition.get(positionVec);
      this.camera.updateFrustum();
      Sprite.draw3D(var1, (PositionableSubSprite[])this.ownPosition, this.camera);
      var1.setBillboard(false);
      var1.setFlip(false);
      GlUtil.glDisable(3553);
      GlUtil.glEnable(2896);
      GlUtil.glEnable(2884);
      GlUtil.glEnable(3042);
      GlUtil.glEnable(2903);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glPopMatrix();
   }

   private void drawNodesSprites(Entry var1, boolean var2) {
      Sprite var3;
      (var3 = Controller.getResLoader().getSprite("map-sprites-8x2-c-gui-")).setBillboard(true);
      var3.setBlend(true);
      var3.setFlip(true);
      this.gameMapPosition.get(positionVec);
      Sprite.draw3D(var3, (PositionableSubSprite[])((GameMap)var1.getValue()).getEntries(), this.camera);
      var3.setBillboard(false);
      var3.setFlip(false);
   }

   private void drawSelectedSector() {
      GlUtil.glPushMatrix();
      GlUtil.translateModelview(50.0F, 50.0F, 50.0F);
      this.gameMapPosition.get(new Vector3i());
      GlUtil.translateModelview(this.gameMapPosition.getWorldTransform().origin.x, this.gameMapPosition.getWorldTransform().origin.y, this.gameMapPosition.getWorldTransform().origin.z);
      this.drawBox(3.125F, 0.4F, true);
      GlUtil.glPopMatrix();
   }

   private void drawHighlightSector(Vector3i var1, Vector3f var2, float var3) {
      GlUtil.glPushMatrix();
      GlUtil.glEnable(2903);
      GlUtil.glDisable(3553);
      GlUtil.glDisable(2896);
      GlUtil.glEnable(2884);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.translateModelview(-50.0F + (float)var1.x * 6.25F + 3.125F, -50.0F + (float)var1.y * 6.25F + 3.125F, -50.0F + (float)var1.z * 6.25F + 3.125F);
      this.drawBoxColor(3.125F, var3, var2);
      GlUtil.glPopMatrix();
   }

   private void drawSystemGrid(float var1, float var2, float var3, float var4) {
      GlUtil.glMatrixMode(5889);
      GlUtil.glPushMatrix();
      var3 = (float)GLFrame.getWidth() / (float)GLFrame.getHeight();
      GlUtil.gluPerspective(Controller.projectionMatrix, (Float)EngineSettings.G_FOV.getCurrentState(), var3, 10.0F, 25000.0F, true);
      GlUtil.glMatrixMode(5888);
      Vector3i var9;
      Vector3i var10000 = var9 = new Vector3i();
      var10000.x = ByteUtil.modU16(var10000.x);
      var9.y = ByteUtil.modU16(var9.y);
      var9.z = ByteUtil.modU16(var9.z);
      GlUtil.glBegin(1);
      var4 = var2 * 3.0F;
      float var5 = var1 + 0.33333334F * var4;
      float var6 = 0.0F;
      float var7 = 0.0F;

      for(float var8 = 0.0F; var8 < 3.0F; ++var8) {
         var7 = 1.0F;
         var6 = 1.0F;
         if (var8 == 0.0F) {
            var6 = 0.0F;
            var7 = 0.6F;
         } else if (var8 == 2.0F) {
            var6 = 0.6F;
            var7 = 0.0F;
         }

         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var6);
         GL11.glVertex3f((float)var9.x * var2, (float)var9.y * var2, var1);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var7);
         GL11.glVertex3f((float)var9.x * var2, (float)var9.y * var2, var5);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var6);
         GL11.glVertex3f(var1, (float)var9.y * var2, (float)var9.z * var2);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var7);
         GL11.glVertex3f(var5, (float)var9.y * var2, (float)var9.z * var2);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var6);
         GL11.glVertex3f((float)var9.x * var2, var1, (float)var9.z * var2);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var7);
         GL11.glVertex3f((float)var9.x * var2, var5, (float)var9.z * var2);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var6);
         GL11.glVertex3f((float)var9.x * var2, (float)(var9.y + 1) * var2, var1);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var7);
         GL11.glVertex3f((float)var9.x * var2, (float)(var9.y + 1) * var2, var5);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var6);
         GL11.glVertex3f(var1, (float)var9.y * var2, (float)(var9.z + 1) * var2);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var7);
         GL11.glVertex3f(var5, (float)var9.y * var2, (float)(var9.z + 1) * var2);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var6);
         GL11.glVertex3f((float)var9.x * var2, var1, (float)(var9.z + 1) * var2);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var7);
         GL11.glVertex3f((float)var9.x * var2, var5, (float)(var9.z + 1) * var2);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var6);
         GL11.glVertex3f((float)(var9.x + 1) * var2, (float)var9.y * var2, var1);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var7);
         GL11.glVertex3f((float)(var9.x + 1) * var2, (float)var9.y * var2, var5);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var6);
         GL11.glVertex3f(var1, (float)(var9.y + 1) * var2, (float)var9.z * var2);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var7);
         GL11.glVertex3f(var5, (float)(var9.y + 1) * var2, (float)var9.z * var2);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var6);
         GL11.glVertex3f((float)(var9.x + 1) * var2, var1, (float)var9.z * var2);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var7);
         GL11.glVertex3f((float)(var9.x + 1) * var2, var5, (float)var9.z * var2);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var6);
         GL11.glVertex3f((float)(var9.x + 1) * var2, (float)(var9.y + 1) * var2, var1);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var7);
         GL11.glVertex3f((float)(var9.x + 1) * var2, (float)(var9.y + 1) * var2, var5);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var6);
         GL11.glVertex3f(var1, (float)(var9.y + 1) * var2, (float)(var9.z + 1) * var2);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var7);
         GL11.glVertex3f(var5, (float)(var9.y + 1) * var2, (float)(var9.z + 1) * var2);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var6);
         GL11.glVertex3f((float)(var9.x + 1) * var2, var1, (float)(var9.z + 1) * var2);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, var7);
         GL11.glVertex3f((float)(var9.x + 1) * var2, var5, (float)(var9.z + 1) * var2);
         var5 += 0.33333334F * var4;
         var1 += 0.33333334F * var4;
      }

      GlUtil.glEnd();
      GlUtil.glMatrixMode(5889);
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5888);
   }

   public GameMapCamera getCamera() {
      return this.camera;
   }

   public int getFilter() {
      return filterAxis;
   }

   public void setFilter(int var1) {
      filterAxis = var1;
   }

   public GameMapPosition getGameMapPosition() {
      return this.gameMapPosition;
   }

   public WorldToScreenConverter getWorldToScreenConverter() {
      return this.worldToScreenConverter;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, this.state)) {
         if (KeyboardMappings.getEventKeySingle(var1) == KeyboardMappings.FORWARD.getMapping()) {
            this.move(0, 0, 1);
         }

         if (KeyboardMappings.getEventKeySingle(var1) == KeyboardMappings.BACKWARDS.getMapping()) {
            this.move(0, 0, -1);
         }

         if (KeyboardMappings.getEventKeySingle(var1) == KeyboardMappings.STRAFE_RIGHT.getMapping()) {
            this.move(-1, 0, 0);
         }

         if (KeyboardMappings.getEventKeySingle(var1) == KeyboardMappings.STRAFE_LEFT.getMapping()) {
            this.move(1, 0, 0);
         }

         if (KeyboardMappings.getEventKeySingle(var1) == KeyboardMappings.UP.getMapping()) {
            this.move(0, 1, 0);
         }

         if (KeyboardMappings.getEventKeySingle(var1) == KeyboardMappings.DOWN.getMapping()) {
            this.move(0, -1, 0);
         }

         if (KeyboardMappings.getEventKeySingle(var1) == 2) {
            this.switchFilter(1);
         }

         if (KeyboardMappings.getEventKeySingle(var1) == 3) {
            this.switchFilter(2);
         }

         if (KeyboardMappings.getEventKeySingle(var1) == 4) {
            this.switchFilter(4);
         }

         if (KeyboardMappings.getEventKeySingle(var1) == 5) {
            filterAxis = 0;
         }
      }

   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public boolean isMapActive() {
      return this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getMapControlManager().isTreeActive() && this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getMapControlManager().isActive();
   }

   private void move(int var1, int var2, int var3) {
      Vector3f var4 = new Vector3f();
      int var5 = 0;
      if (var3 != 0) {
         var5 = var3;
         var3 = 0;
         GlUtil.getForwardVector(var4, (Transform)this.camera.getWorldTransform());
      }

      if (var2 != 0) {
         var5 = var2;
         var2 = 0;
         GlUtil.getUpVector(var4, (Transform)this.camera.getWorldTransform());
      }

      if (var1 != 0) {
         var5 = var1;
         var1 = 0;
         GlUtil.getRightVector(var4, (Transform)this.camera.getWorldTransform());
      }

      if (Math.abs(var4.x) >= Math.abs(var4.y) && Math.abs(var4.x) >= Math.abs(var4.z)) {
         if (var4.x >= 0.0F) {
            var1 = var5;
         } else {
            var1 = -var5;
         }
      } else if (Math.abs(var4.y) >= Math.abs(var4.x) && Math.abs(var4.y) >= Math.abs(var4.z)) {
         if (var4.y >= 0.0F) {
            var2 = var5;
         } else {
            var2 = -var5;
         }
      } else if (Math.abs(var4.z) >= Math.abs(var4.y) && Math.abs(var4.z) >= Math.abs(var4.x)) {
         if (var4.z >= 0.0F) {
            var3 = var5;
         } else {
            var3 = -var5;
         }
      }

      this.checkGLErrorOnce = true;
      this.gameMapPosition.add(var1, var2, var3, this.camera.getCameraOffset(), false);
   }

   public void resetToCurrentSector() {
      this.gameMapPosition.set(this.getPlayerSector().x, this.getPlayerSector().y, this.getPlayerSector().z, true);
   }

   public void setFilter(int var1, boolean var2) {
      if (var2) {
         filterAxis &= ~var1;
      } else {
         filterAxis |= var1;
      }
   }

   public void switchFilter(int var1) {
      this.setFilter(var1, (filterAxis & var1) == var1);
   }

   public void update(Timer var1) {
      if (this.isMapActive()) {
         this.time += var1.getDelta();
         if (this.lastGalaxy != null && this.lastGalaxy != this.state.getCurrentGalaxy()) {
            this.resetToCurrentSector();
         }

         this.lastGalaxy = this.state.getCurrentGalaxy();
         this.gameMapPosition.update(var1);
         this.sinus.update(var1);
         this.camera.alwaysAllowWheelZoom = !PlayerPanel.mouseInInfoScroll;
      }
   }

   public boolean updateCamera(Timer var1) {
      if (this.isMapActive()) {
         this.camera.update(var1, false);
         return true;
      } else {
         return false;
      }
   }

   public void updateMouse() {
      if (this.isMapActive()) {
         this.camera.updateMouseState();
      }

   }

   class Quad implements Comparable {
      float[] v;
      public int userData;

      private Quad() {
         this.v = new float[22];
      }

      private void pos(float var1, float var2, float var3) {
         this.v[19] = var1;
         this.v[20] = var2;
         this.v[21] = var3;
      }

      private float distance() {
         float var1 = this.v[19] * 100.0F + this.v[0] * GameMapDrawer.quadSize - GameMapDrawer.this.camera.getPos().x;
         float var2 = this.v[20] * 100.0F + this.v[1] * GameMapDrawer.quadSize - GameMapDrawer.this.camera.getPos().y;
         float var3 = this.v[21] * 100.0F + this.v[2] * GameMapDrawer.quadSize - GameMapDrawer.this.camera.getPos().z;
         return Vector3fTools.lengthSquared(var1, var2, var3);
      }

      public int compareTo(GameMapDrawer.Quad var1) {
         return CompareTools.compare(var1.distance(), this.distance());
      }

      private void verticesReverse() {
         float var1 = this.v[19] * 100.0F;
         float var2 = this.v[20] * 100.0F;
         float var3 = this.v[21] * 100.0F;
         GlUtil.glColor4f(this.v[15], this.v[16], this.v[17], this.v[18]);
         GL11.glNormal3f(-this.v[0], -this.v[1], -this.v[2]);
         GL11.glVertex3f(var1 + this.v[12], var2 + this.v[13], var3 + this.v[14]);
         GL11.glVertex3f(var1 + this.v[9], var2 + this.v[10], var3 + this.v[11]);
         GL11.glVertex3f(var1 + this.v[6], var2 + this.v[7], var3 + this.v[8]);
         GL11.glVertex3f(var1 + this.v[3], var2 + this.v[4], var3 + this.v[5]);
      }

      private void vertices() {
         float var1 = this.v[19] * 100.0F;
         float var2 = this.v[20] * 100.0F;
         float var3 = this.v[21] * 100.0F;
         GlUtil.glColor4f(this.v[15], this.v[16], this.v[17], this.v[18]);
         GL11.glNormal3f(this.v[0], this.v[1], this.v[2]);
         GL11.glVertex3f(var1 + this.v[3], var2 + this.v[4], var3 + this.v[5]);
         GL11.glVertex3f(var1 + this.v[6], var2 + this.v[7], var3 + this.v[8]);
         GL11.glVertex3f(var1 + this.v[9], var2 + this.v[10], var3 + this.v[11]);
         GL11.glVertex3f(var1 + this.v[12], var2 + this.v[13], var3 + this.v[14]);
      }

      private void normal3f(float var1, float var2, float var3) {
         this.v[0] = var1;
         this.v[1] = var2;
         this.v[2] = var3;
      }

      private void vertex3f0(float var1, float var2, float var3) {
         this.v[3] = var1;
         this.v[4] = var2;
         this.v[5] = var3;
      }

      private void vertex3f1(float var1, float var2, float var3) {
         this.v[6] = var1;
         this.v[7] = var2;
         this.v[8] = var3;
      }

      private void vertex3f2(float var1, float var2, float var3) {
         this.v[9] = var1;
         this.v[10] = var2;
         this.v[11] = var3;
      }

      private void vertex3f3(float var1, float var2, float var3) {
         this.v[12] = var1;
         this.v[13] = var2;
         this.v[14] = var3;
      }

      private void color4ff3(float var1, float var2, float var3, float var4) {
         this.v[15] = var1;
         this.v[16] = var2;
         this.v[17] = var3;
         this.v[18] = var4;
      }

      public void front() {
         this.normal3f(0.0F, 0.0F, 1.0F);
         this.vertex3f0(GameMapDrawer.quadSize, GameMapDrawer.quadSize, -GameMapDrawer.quadSize);
         this.vertex3f1(GameMapDrawer.quadSize, -GameMapDrawer.quadSize, -GameMapDrawer.quadSize);
         this.vertex3f2(-GameMapDrawer.quadSize, -GameMapDrawer.quadSize, -GameMapDrawer.quadSize);
         this.vertex3f3(-GameMapDrawer.quadSize, GameMapDrawer.quadSize, -GameMapDrawer.quadSize);
      }

      public void back() {
         this.normal3f(0.0F, 0.0F, -1.0F);
         this.vertex3f0(GameMapDrawer.quadSize, -GameMapDrawer.quadSize, GameMapDrawer.quadSize);
         this.vertex3f1(GameMapDrawer.quadSize, GameMapDrawer.quadSize, GameMapDrawer.quadSize);
         this.vertex3f2(-GameMapDrawer.quadSize, GameMapDrawer.quadSize, GameMapDrawer.quadSize);
         this.vertex3f3(-GameMapDrawer.quadSize, -GameMapDrawer.quadSize, GameMapDrawer.quadSize);
      }

      public void right() {
         this.normal3f(1.0F, 0.0F, 0.0F);
         this.vertex3f0(-GameMapDrawer.quadSize, -GameMapDrawer.quadSize, GameMapDrawer.quadSize);
         this.vertex3f1(-GameMapDrawer.quadSize, GameMapDrawer.quadSize, GameMapDrawer.quadSize);
         this.vertex3f2(-GameMapDrawer.quadSize, GameMapDrawer.quadSize, -GameMapDrawer.quadSize);
         this.vertex3f3(-GameMapDrawer.quadSize, -GameMapDrawer.quadSize, -GameMapDrawer.quadSize);
      }

      public void left() {
         this.normal3f(-1.0F, 0.0F, 0.0F);
         this.vertex3f0(GameMapDrawer.quadSize, -GameMapDrawer.quadSize, -GameMapDrawer.quadSize);
         this.vertex3f1(GameMapDrawer.quadSize, GameMapDrawer.quadSize, -GameMapDrawer.quadSize);
         this.vertex3f2(GameMapDrawer.quadSize, GameMapDrawer.quadSize, GameMapDrawer.quadSize);
         this.vertex3f3(GameMapDrawer.quadSize, -GameMapDrawer.quadSize, GameMapDrawer.quadSize);
      }

      public void bottom() {
         this.normal3f(0.0F, -1.0F, 0.0F);
         this.vertex3f0(GameMapDrawer.quadSize, GameMapDrawer.quadSize, GameMapDrawer.quadSize);
         this.vertex3f1(GameMapDrawer.quadSize, GameMapDrawer.quadSize, -GameMapDrawer.quadSize);
         this.vertex3f2(-GameMapDrawer.quadSize, GameMapDrawer.quadSize, -GameMapDrawer.quadSize);
         this.vertex3f3(-GameMapDrawer.quadSize, GameMapDrawer.quadSize, GameMapDrawer.quadSize);
      }

      public void top() {
         this.normal3f(0.0F, 1.0F, 0.0F);
         this.vertex3f0(GameMapDrawer.quadSize, -GameMapDrawer.quadSize, -GameMapDrawer.quadSize);
         this.vertex3f1(GameMapDrawer.quadSize, -GameMapDrawer.quadSize, GameMapDrawer.quadSize);
         this.vertex3f2(-GameMapDrawer.quadSize, -GameMapDrawer.quadSize, GameMapDrawer.quadSize);
         this.vertex3f3(-GameMapDrawer.quadSize, -GameMapDrawer.quadSize, -GameMapDrawer.quadSize);
      }

      // $FF: synthetic method
      Quad(Object var2) {
         this();
      }
   }
}
