package org.schema.game.client.view.gamemap;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.map.MapControllerManager;
import org.schema.game.client.data.gamemap.entry.SelectableMapEntry;
import org.schema.game.client.view.effects.ConstantIndication;
import org.schema.game.client.view.effects.Indication;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.server.data.Galaxy;
import org.schema.schine.graphicsengine.forms.PositionableSubColorSprite;
import org.schema.schine.graphicsengine.forms.SelectableSprite;
import org.schema.schine.graphicsengine.forms.Sprite;

public class StarPosition implements SelectableMapEntry, PositionableSubColorSprite, SelectableSprite {
   public static float posAdd = 0.0F;
   public static int posMult = 1;
   public static float spriteScale = 1.0F;
   public static Indication indication;
   public int starSubSprite;
   public Vector3f pos = new Vector3f();
   public Vector3f posRet = new Vector3f();
   public Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   public Vector4f colorRet = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   public Vector3i relPosInGalaxy = new Vector3i();
   public float scale = 1.0F;
   private float selectDepth;
   private boolean drawIndication;
   private float distanceToCam;

   public float getScale(long var1) {
      return spriteScale;
   }

   public int getSubSprite(Sprite var1) {
      return this.starSubSprite;
   }

   public boolean canDraw() {
      return true;
   }

   public Vector3f getPos() {
      this.posRet.set(this.pos);
      this.posRet.scale((float)posMult);
      Vector3f var10000 = this.posRet;
      var10000.x += posAdd;
      var10000 = this.posRet;
      var10000.y += posAdd;
      var10000 = this.posRet;
      var10000.z += posAdd;
      return this.posRet;
   }

   public Vector4f getColor() {
      this.colorRet.set(this.color);
      Vector4f var10000 = this.colorRet;
      var10000.w *= Math.min(1.0F, this.getDistanceToCam() / 150.0F);
      return this.colorRet;
   }

   public float getSelectionDepth() {
      return this.selectDepth;
   }

   public boolean isSelectable() {
      return true;
   }

   public void onSelect(float var1) {
      this.setDrawIndication(true);
      this.selectDepth = var1;
      MapControllerManager.selected.add(this);
      HudIndicatorOverlay.toDrawStars.add(this);
   }

   public void onUnSelect() {
      this.setDrawIndication(false);
      MapControllerManager.selected.remove(this);
      HudIndicatorOverlay.toDrawStars.remove(this);
   }

   public boolean isDrawIndication() {
      return this.drawIndication;
   }

   public void setDrawIndication(boolean var1) {
      this.drawIndication = var1;
   }

   public float getDistanceToCam() {
      return this.distanceToCam;
   }

   public void setDistanceToCam(float var1) {
      this.distanceToCam = var1;
   }

   public Indication getIndication(Galaxy var1) {
      if (indication == null) {
         Transform var2;
         (var2 = new Transform()).setIdentity();
         indication = new ConstantIndication(var2, "STAR" + this.pos);
      }

      float var10001 = this.pos.x * 100.0F;
      float var10002 = this.pos.y * 100.0F;
      indication.getCurrentTransform().origin.set(var10001, var10002, this.pos.z * 100.0F);
      indication.setText(var1.getName(this.relPosInGalaxy) + " (" + (this.relPosInGalaxy.x + var1.galaxyPos.x * Galaxy.size) + ", " + (this.relPosInGalaxy.y + var1.galaxyPos.y * Galaxy.size) + ", " + (this.relPosInGalaxy.z + var1.galaxyPos.z * Galaxy.size) + ")");
      return indication;
   }
}
