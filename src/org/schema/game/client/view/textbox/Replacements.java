package org.schema.game.client.view.textbox;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Locale;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.controller.elements.ShieldLocal;
import org.schema.game.common.controller.elements.ShieldLocalAddOn;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorTree;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.schine.common.language.Lng;

public class Replacements {
   private static final Vector3i tmp = new Vector3i();
   private static final int REPLACE_AVAILABILITY_NEW_POWER = 1;
   private static final int REPLACE_AVAILABILITY_OLD_POWER = 2;
   private static final int REPLACE_AVAILABILITY_ALL = 3;

   public static String getVariables(boolean var0) {
      StringBuffer var1 = new StringBuffer();
      ObjectArrayList var2 = new ObjectArrayList();

      int var3;
      Replacements.Type var4;
      for(var3 = 0; var3 < Replacements.Type.values().length; ++var3) {
         var4 = Replacements.Type.values()[var3];
         if (var0 && (var4.availability & 2) == 2 || !var0 && (var4.availability & 1) == 1) {
            var2.add(var4);
         }
      }

      for(var3 = 0; var3 < var2.size(); ++var3) {
         var4 = (Replacements.Type)var2.get(var3);
         var1.append("[");
         var1.append(var4.var.toLowerCase(Locale.ENGLISH));
         if (var4.takesIndex > 0) {
            var1.append("#");
         }

         var1.append("]");
         if (var3 < Replacements.Type.values().length - 1) {
            var1.append(", ");
         }
      }

      var1.append(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_14);
      return var1.toString();
   }

   public static void main(String[] var0) {
      System.out.println(getVariables(false));
   }

   public static enum Type {
      SHIELD("shield", 2, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return StringTools.formatPointZero(((ShieldContainerInterface)((ManagedSegmentController)var1).getManagerContainer()).getShieldAddOn().getShields());
         }

         public final boolean ok(SegmentController var1) {
            return var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof ShieldContainerInterface;
         }
      }),
      SHIELD_CAP("shieldCap", 2, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return StringTools.formatPointZero(((ShieldContainerInterface)((ManagedSegmentController)var1).getManagerContainer()).getShieldAddOn().getShieldCapacity());
         }

         public final boolean ok(SegmentController var1) {
            return var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof ShieldContainerInterface;
         }
      }),
      SHIELD_PERCENT("shieldPercent", 2, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            double var3 = ((ShieldContainerInterface)((ManagedSegmentController)var1).getManagerContainer()).getShieldAddOn().getShieldCapacity();
            return StringTools.formatPointZero(((ShieldContainerInterface)((ManagedSegmentController)var1).getManagerContainer()).getShieldAddOn().getShields() / Math.max(1.0E-5D, var3) * 100.0D);
         }

         public final boolean ok(SegmentController var1) {
            return var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof ShieldContainerInterface;
         }
      }),
      POWER("power", 2, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return StringTools.formatPointZero(((PowerManagerInterface)((ManagedSegmentController)var1).getManagerContainer()).getPowerAddOn().getPower());
         }

         public final boolean ok(SegmentController var1) {
            return var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof PowerManagerInterface;
         }
      }),
      POWER_CAP("powerCap", 2, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return StringTools.formatPointZero(((PowerManagerInterface)((ManagedSegmentController)var1).getManagerContainer()).getPowerAddOn().getMaxPower());
         }

         public final boolean ok(SegmentController var1) {
            return var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof PowerManagerInterface;
         }
      }),
      POWER_PERCENT("powerPercent", 2, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            double var3 = ((PowerManagerInterface)((ManagedSegmentController)var1).getManagerContainer()).getPowerAddOn().getMaxPower();
            return StringTools.formatPointZero(((PowerManagerInterface)((ManagedSegmentController)var1).getManagerContainer()).getPowerAddOn().getPower() / Math.max(1.0E-5D, var3) * 100.0D);
         }

         public final boolean ok(SegmentController var1) {
            return var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof PowerManagerInterface;
         }
      }),
      POWER_BATTERY("auxPower", 2, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return StringTools.formatPointZero(((PowerManagerInterface)((ManagedSegmentController)var1).getManagerContainer()).getPowerAddOn().getBatteryPower());
         }

         public final boolean ok(SegmentController var1) {
            return var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof PowerManagerInterface;
         }
      }),
      POWER_BATTERY_CAP("auxPowerCap", 2, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return StringTools.formatPointZero(((PowerManagerInterface)((ManagedSegmentController)var1).getManagerContainer()).getPowerAddOn().getBatteryMaxPower());
         }

         public final boolean ok(SegmentController var1) {
            return var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof PowerManagerInterface;
         }
      }),
      POWER_BATTERY_PERCENT("auxPowerPercent", 2, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            double var3 = ((PowerManagerInterface)((ManagedSegmentController)var1).getManagerContainer()).getPowerAddOn().getBatteryMaxPower();
            return StringTools.formatPointZero(((PowerManagerInterface)((ManagedSegmentController)var1).getManagerContainer()).getPowerAddOn().getBatteryPower() / Math.max(1.0E-5D, var3) * 100.0D);
         }

         public final boolean ok(SegmentController var1) {
            return var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof PowerManagerInterface;
         }
      }),
      STRUCTURE_HP("structureHp", 2, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return StringTools.formatSmallAndBig(var1.getHpController().getHp());
         }

         public final boolean ok(SegmentController var1) {
            return true;
         }
      }),
      STRUCTURE_HP_CAPACITY("structureHpCap", 2, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return StringTools.formatSmallAndBig(var1.getHpController().getMaxHp());
         }

         public final boolean ok(SegmentController var1) {
            return true;
         }
      }),
      STRUCTURE_HP_PERCENT("structureHpPercent", 2, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return StringTools.formatPointZero((double)var1.getHpController().getHp() / Math.max(1.0E-5D, (double)var1.getHpController().getMaxHp()) * 100.0D);
         }

         public final boolean ok(SegmentController var1) {
            return true;
         }
      }),
      REACTOR_HP("activeReactorHp", 1, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return StringTools.formatSmallAndBig(var1.getHpController().getHp());
         }

         public final boolean ok(SegmentController var1) {
            return true;
         }
      }),
      REACTOR_HP_CAPACITY("activeReactorMaxHp", 1, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return StringTools.formatSmallAndBig(var1.getHpController().getMaxHp());
         }

         public final boolean ok(SegmentController var1) {
            return true;
         }
      }),
      REACTOR_HP_PERCENT("activeReactorHpPercent", 1, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return StringTools.formatPointZero((double)var1.getHpController().getHp() / Math.max(1.0E-5D, (double)var1.getHpController().getMaxHp()) * 100.0D);
         }

         public final boolean ok(SegmentController var1) {
            return true;
         }
      }),
      MASS("mass", 3, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return StringTools.formatPointZero(var1.getTotalPhysicalMass());
         }

         public final boolean ok(SegmentController var1) {
            return true;
         }
      }),
      BLOCK_COUNT("blockCount", 3, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return StringTools.formatPointZero((float)var1.getTotalElements());
         }

         public final boolean ok(SegmentController var1) {
            return true;
         }
      }),
      SECTOR("sector", 3, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return var1.getClientSector().toStringPure();
         }

         public final boolean ok(SegmentController var1) {
            return true;
         }
      }),
      SYSTEM("system", 3, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return VoidSystem.getContainingSystem(var1.getClientSector(), Replacements.tmp).toStringPure();
         }

         public final boolean ok(SegmentController var1) {
            return true;
         }
      }),
      NAME("name", 3, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return var1.getRealName();
         }

         public final boolean ok(SegmentController var1) {
            return true;
         }
      }),
      DOCKED("docked", 3, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return var1.railController.isDockedAndExecuted() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_0 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_1;
         }

         public final boolean ok(SegmentController var1) {
            return true;
         }
      }),
      CLOAKED("cloaked", 3, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return ((Ship)var1).isCloakedFor((SimpleTransformableSendableObject)null) ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_2 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_3;
         }

         public final boolean ok(SegmentController var1) {
            return var1 instanceof Ship;
         }
      }),
      JAMMING("jamming", 3, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return ((Ship)var1).isJammingFor((SimpleTransformableSendableObject)null) ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_4 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_5;
         }

         public final boolean ok(SegmentController var1) {
            return var1 instanceof Ship;
         }
      }),
      SPEED("speed", 3, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return StringTools.formatPointZero(((Ship)var1).getSpeedCurrent());
         }

         public final boolean ok(SegmentController var1) {
            return var1 instanceof Ship;
         }
      }),
      REACTORIDACTIVE("activeReactorId", 1, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return var1 instanceof ManagedSegmentController ? String.valueOf(((ManagedSegmentController)var1).getManagerContainer().getPowerInterface().getActiveReactorId()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_16;
         }

         public final boolean ok(SegmentController var1) {
            return !var1.isUsingOldPower() && var1 instanceof ManagedSegmentController && var1.hasActiveReactors();
         }
      }),
      REACTORRECHARGEACTIVE("activeReactorRecharge", 1, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return var1 instanceof ManagedSegmentController ? StringTools.formatSmallAndBig(((ManagedSegmentController)var1).getManagerContainer().getPowerInterface().getRechargeRatePowerPerSec()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_17;
         }

         public final boolean ok(SegmentController var1) {
            return !var1.isUsingOldPower() && var1 instanceof ManagedSegmentController && var1.hasActiveReactors();
         }
      }),
      REACTORCONSUMPIONACTIVE("activeReactorConsumption", 1, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return var1 instanceof ManagedSegmentController ? StringTools.formatSmallAndBig(((ManagedSegmentController)var1).getManagerContainer().getPowerInterface().getCurrentConsumptionPerSec()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_15;
         }

         public final boolean ok(SegmentController var1) {
            return !var1.isUsingOldPower() && var1 instanceof ManagedSegmentController && var1.hasActiveReactors();
         }
      }),
      REACTORCONSUMPIONPERCENTACTIVE("activeReactorConsumptionPercent", 1, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return var1 instanceof ManagedSegmentController ? StringTools.formatPointZero(((ManagedSegmentController)var1).getManagerContainer().getPowerInterface().getPowerConsumptionAsPercent() * 100.0D) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_18;
         }

         public final boolean ok(SegmentController var1) {
            return !var1.isUsingOldPower() && var1 instanceof ManagedSegmentController && var1.hasActiveReactors();
         }
      }),
      REACTORIDX("reactorId", 1, 1000, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            if (var1 instanceof ManagedSegmentController) {
               PowerInterface var3 = ((ManagedSegmentController)var1).getManagerContainer().getPowerInterface();
               if (var2 >= 0 && var2 <= var3.getReactorSet().getTrees().size()) {
                  return String.valueOf(((ReactorTree)var3.getReactorSet().getTrees().get(var2)).getId());
               }
            }

            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_6;
         }

         public final boolean ok(SegmentController var1) {
            return !var1.isUsingOldPower() && var1 instanceof ManagedSegmentController && var1.hasAnyReactors();
         }
      }),
      REACTORSIZEX("reactorSize", 1, 1000, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            if (var1 instanceof ManagedSegmentController) {
               PowerInterface var3 = ((ManagedSegmentController)var1).getManagerContainer().getPowerInterface();
               if (var2 >= 0 && var2 <= var3.getReactorSet().getTrees().size()) {
                  return StringTools.formatSmallAndBig(((ReactorTree)var3.getReactorSet().getTrees().get(var2)).getActualSize());
               }
            }

            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_8;
         }

         public final boolean ok(SegmentController var1) {
            return !var1.isUsingOldPower() && var1 instanceof ManagedSegmentController && var1.hasAnyReactors();
         }
      }),
      REACTORHPX("reactorHp", 1, 1000, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            if (var1 instanceof ManagedSegmentController) {
               PowerInterface var3 = ((ManagedSegmentController)var1).getManagerContainer().getPowerInterface();
               if (var2 >= 0 && var2 <= var3.getReactorSet().getTrees().size()) {
                  return StringTools.formatSmallAndBig(((ReactorTree)var3.getReactorSet().getTrees().get(var2)).getHp());
               }
            }

            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_7;
         }

         public final boolean ok(SegmentController var1) {
            return !var1.isUsingOldPower() && var1 instanceof ManagedSegmentController && var1.hasAnyReactors();
         }
      }),
      REACTORMAXHPX("reactorHp", 1, 1000, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            if (var1 instanceof ManagedSegmentController) {
               PowerInterface var3 = ((ManagedSegmentController)var1).getManagerContainer().getPowerInterface();
               if (var2 >= 0 && var2 <= var3.getReactorSet().getTrees().size()) {
                  return StringTools.formatSmallAndBig(((ReactorTree)var3.getReactorSet().getTrees().get(var2)).getMaxHp());
               }
            }

            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_9;
         }

         public final boolean ok(SegmentController var1) {
            return !var1.isUsingOldPower() && var1 instanceof ManagedSegmentController && var1.hasAnyReactors();
         }
      }),
      SHIELDIDX("shieldId", 1, 1000, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            ShieldLocalAddOn var3 = ((ShieldContainerInterface)((ManagedSegmentController)var1).getManagerContainer()).getShieldAddOn().getShieldLocalAddOn();
            return var2 >= 0 && var2 < var3.getActiveShields().size() ? String.valueOf(((ShieldLocal)var3.getActiveShields().get(var2)).mainId) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_12;
         }

         public final boolean ok(SegmentController var1) {
            return var1.isUsingLocalShields();
         }
      }),
      SHIELDPERCENTX("shieldPercent", 1, 1000, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            ShieldLocalAddOn var3 = ((ShieldContainerInterface)((ManagedSegmentController)var1).getManagerContainer()).getShieldAddOn().getShieldLocalAddOn();
            return var2 >= 0 && var2 < var3.getActiveShields().size() ? StringTools.formatPointZero(((ShieldLocal)var3.getActiveShields().get(var2)).getPercentOne() * 100.0F) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_13;
         }

         public final boolean ok(SegmentController var1) {
            return var1.isUsingLocalShields();
         }
      }),
      SHIELDHPX("shieldHp", 1, 1000, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            ShieldLocalAddOn var3 = ((ShieldContainerInterface)((ManagedSegmentController)var1).getManagerContainer()).getShieldAddOn().getShieldLocalAddOn();
            return var2 >= 0 && var2 < var3.getActiveShields().size() ? StringTools.formatPointZero(((ShieldLocal)var3.getActiveShields().get(var2)).getShields()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_10;
         }

         public final boolean ok(SegmentController var1) {
            return var1.isUsingLocalShields();
         }
      }),
      SHIELDCAPX("shieldMaxHp", 1, 1000, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            ShieldLocalAddOn var3 = ((ShieldContainerInterface)((ManagedSegmentController)var1).getManagerContainer()).getShieldAddOn().getShieldLocalAddOn();
            return var2 >= 0 && var2 < var3.getActiveShields().size() ? StringTools.formatPointZero(((ShieldLocal)var3.getActiveShields().get(var2)).getShieldCapacity()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_11;
         }

         public final boolean ok(SegmentController var1) {
            return var1.isUsingLocalShields();
         }
      }),
      SHIELDRADIUSX("shieldRadius", 1, 1000, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            ShieldLocalAddOn var3 = ((ShieldContainerInterface)((ManagedSegmentController)var1).getManagerContainer()).getShieldAddOn().getShieldLocalAddOn();
            return var2 >= 0 && var2 < var3.getActiveShields().size() ? StringTools.formatPointZero(((ShieldLocal)var3.getActiveShields().get(var2)).radius) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_TEXTBOX_REPLACEMENTS_19;
         }

         public final boolean ok(SegmentController var1) {
            return var1.isUsingLocalShields();
         }
      }),
      MISSILE_CAPACITY("missileCapacity", 3, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return StringTools.formatPointZero(var1.getMissileCapacity());
         }

         public final boolean ok(SegmentController var1) {
            return var1.isUsingLocalShields();
         }
      }),
      MISSILE_CAPACITY_MAX("missileCapacityMax", 3, new Replacements.RFactory() {
         public final String getValue(SegmentController var1, int var2) {
            return StringTools.formatPointZero(var1.getMissileCapacityMax());
         }

         public final boolean ok(SegmentController var1) {
            return var1.isUsingLocalShields();
         }
      });

      public final String var;
      public final Replacements.RFactory fac;
      private final int availability;
      public final int takesIndex;

      private Type(String var3, int var4, Replacements.RFactory var5) {
         this(var3, var4, 0, var5);
      }

      private Type(String var3, int var4, int var5, Replacements.RFactory var6) {
         this.var = var3;
         this.fac = var6;
         this.availability = var4;
         this.takesIndex = var5;
      }
   }

   interface RFactory {
      String getValue(SegmentController var1, int var2);

      boolean ok(SegmentController var1);
   }
}
