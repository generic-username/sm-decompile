package org.schema.game.client.view;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.damage.projectile.ProjectileController;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.particle.ParticleLaserDrawerVBO;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.input.Keyboard;

public class ProjectileCannonDrawerVBO extends ParticleLaserDrawerVBO implements Shaderable {
   public static final float PROJECTILE_SPRITE_SIZE = 0.2F;
   private GameClientState state;
   private Sprite sprite;
   private Shader shader;
   private boolean initialized;
   private float zoomFac;

   public ProjectileCannonDrawerVBO(ProjectileController var1) {
      super(var1, 0.2F);
      this.state = (GameClientState)var1.getState();
      this.setBlended(false);
   }

   public void draw() {
      if (!this.initialized) {
         this.onInit();
      }

      this.shader = ShaderLibrary.projectileBeamQuadShader;
      DEBUG = Keyboard.isKeyDown(79);
      if (this.getParticleController().getParticleCount() > 0) {
         if (WorldDrawer.drawError) {
            GlUtil.printGlErrorCritical();
         }

         if (!DEBUG) {
            this.shader.setShaderInterface(this);
            this.shader.load();
         }

         if (WorldDrawer.drawError) {
            GlUtil.printGlErrorCritical();
         }

         super.draw();
         if (WorldDrawer.drawError) {
            GlUtil.printGlErrorCritical();
         }

         if (!DEBUG) {
            this.shader.unload();
         }

      }
   }

   public void onInit() {
      this.sprite = Controller.getResLoader().getSprite("starSprite");
      super.onInit();
      this.initialized = true;
   }

   public void onExit() {
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, this.sprite.getMaterial().getTexture().getTextureId());
      GlUtil.updateShaderInt(var1, "tex", 0);
      GlUtil.updateShaderFloat(var1, "zoomFac", this.zoomFac);
   }

   public String toString() {
      return "ProjectileDrawerVBO [state=" + this.state + ", count=" + this.getParticleController().getParticleCount() + "]";
   }

   public float getZoomFac() {
      return this.zoomFac;
   }

   public void setZoomFac(float var1) {
      this.zoomFac = var1;
   }
}
