package org.schema.game.client.view.tools;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import org.schema.game.common.Starter;
import org.schema.schine.resource.tag.Tag;

public class TagReader {
   public static void main(String[] var0) {
      JFrame var4 = new JFrame("Tag Reader");
      Starter.registerSerializableFactories();
      var4.setDefaultCloseOperation(3);
      var4.setSize(1024, 800);
      JFileChooser var1;
      (var1 = new JFileChooser("./server-database")).showDialog(var4, "Tag");

      try {
         Starter.initialize(true);
         Tag var5 = Tag.readFrom(new BufferedInputStream(new FileInputStream(var1.getSelectedFile())), true, true);
         var4.setContentPane(new TagExplorerPanel(var5));
      } catch (FileNotFoundException var2) {
         var2.printStackTrace();
      } catch (IOException var3) {
         var3.printStackTrace();
      }

      var4.setVisible(true);
   }
}
