package org.schema.game.client.view.tools;

import javax.vecmath.Vector4f;
import org.schema.common.FastMath;

public class ColorTools {
   public static void brighten(Vector4f var0) {
      for(var0.w = 1.0F; FastMath.carmackSqrt(var0.x * var0.x + var0.y * var0.y + var0.z * var0.z) < 0.5F; var0.z = Math.min(1.0F, var0.z * 1.33F)) {
         var0.x = Math.min(1.0F, var0.x * 1.33F);
         var0.y = Math.min(1.0F, var0.y * 1.33F);
      }

   }
}
