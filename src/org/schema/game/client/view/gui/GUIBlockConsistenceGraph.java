package org.schema.game.client.view.gui;

import java.util.Iterator;
import org.lwjgl.opengl.GL11;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GLException;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraph;
import org.schema.schine.input.InputState;
import org.schema.schine.resource.FileExt;

public class GUIBlockConsistenceGraph extends GUIAncor {
   private GUIScrollablePanel scrollPane;
   private GUIGraph graph;

   public GUIBlockConsistenceGraph(InputState var1, ElementInformation var2) {
      this(var1, var2, (GUIAncor)null);
   }

   public GUIBlockConsistenceGraph(InputState var1, ElementInformation var2, GUIAncor var3) {
      super(var1);
      if (var3 != null) {
         this.scrollPane = new GUIScrollablePanel(824.0F, 424.0F, var3, var1);
      } else {
         this.scrollPane = new GUIScrollablePanel(824.0F, 424.0F, var1);
      }

      this.graph = var2.getRecipeGraph(var1);
      this.scrollPane.setScrollable(GUIScrollablePanel.SCROLLABLE_HORIZONTAL | GUIScrollablePanel.SCROLLABLE_VERTICAL);
      this.scrollPane.setContent(this.graph);
      this.attach(this.scrollPane);
   }

   public static void bake(FrameBufferObjects var0, GameClientState var1) throws GLException {
      FrameBufferObjects var2;
      (var2 = new FrameBufferObjects("BlockConsistenceBake", 2048, 2048)).initialize();
      var2.enable();
      GL11.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
      FileExt var3;
      if (!(var3 = new FileExt("./recipeGraphs/")).exists()) {
         var3.mkdir();
      }

      Iterator var6 = ElementKeyMap.keySet.iterator();

      while(var6.hasNext()) {
         short var4 = (Short)var6.next();
         GL11.glClear(16640);
         ElementInformation var7;
         GUIGraph var5 = (var7 = ElementKeyMap.getInfo(var4)).getRecipeGraph(var1);
         GL11.glViewport(0, 0, (int)var5.getWidth(), (int)var5.getHeight());
         GlUtil.glDisable(2896);
         GlUtil.glDisable(2929);
         GlUtil.glPushMatrix();
         GlUtil.glMatrixMode(5889);
         GlUtil.glPushMatrix();
         GlUtil.glLoadIdentity();
         GlUtil.gluOrtho2D(0.0F, (float)((int)var5.getWidth()), (float)((int)var5.getHeight()), 0.0F);
         GlUtil.glMatrixMode(5888);
         GlUtil.glLoadIdentity();
         var5.draw();
         GlUtil.writeScreenToDisk("./recipeGraphs/" + var7.getId() + "_" + var7.getName(), "png", (int)var5.getWidth(), (int)var5.getHeight(), 4, var0);
         GlUtil.glMatrixMode(5889);
         GlUtil.glPopMatrix();
         GlUtil.glMatrixMode(5888);
         GlUtil.glPopMatrix();
         GlUtil.glEnable(2929);
         GlUtil.glEnable(2896);
      }

      GL11.glViewport(0, 0, GLFrame.getWidth(), GLFrame.getHeight());
      var2.disable();
      var2.cleanUp();
   }
}
