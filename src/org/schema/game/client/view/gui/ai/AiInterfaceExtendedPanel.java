package org.schema.game.client.view.gui.ai;

import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerCreatureAISettingsInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.manager.AiConfigurationManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.ai.AIGameConfiguration;
import org.schema.game.common.controller.ai.AiInterfaceContainer;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.ai.UnloadedAiEntityException;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.common.data.player.PlayerControlledTransformableNotFound;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class AiInterfaceExtendedPanel extends GUIElement implements GUICallback {
   private GameClientState state;
   private AiInterfaceContainer ai;
   private boolean showAdminSettings;

   public AiInterfaceExtendedPanel(InputState var1, AiInterfaceContainer var2, boolean var3) {
      super(var1);
      this.showAdminSettings = var3;
      this.state = (GameClientState)this.getState();
      this.ai = var2;
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         if ("Idling".equals(var1.getUserPointer())) {
            this.idle();
            return;
         }

         if ("Following".equals(var1.getUserPointer())) {
            this.follow();
            return;
         }

         if ("Roaming".equals(var1.getUserPointer())) {
            this.roam();
            return;
         }

         if ("Attacking".equals(var1.getUserPointer())) {
            this.attack();
            return;
         }

         if ("GoTo".equals(var1.getUserPointer())) {
            this.gotoPos();
            return;
         }

         if ("delete".equals(var1.getUserPointer())) {
            this.delete();
            return;
         }

         if ("rename".equals(var1.getUserPointer())) {
            this.rename();
            return;
         }

         if ("settings".equals(var1.getUserPointer())) {
            this.settings();
         }
      }

   }

   public boolean isOccluded() {
      return false;
   }

   private void settings() {
      System.err.println(this.ai + " settings ");

      try {
         if (this.ai.getType() == 0) {
            (new PlayerCreatureAISettingsInput(this.state, this.ai.getAi())).activate();
         } else {
            ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_AI_AIINTERFACEEXTENDEDPANEL_0, 0.0F);
         }
      } catch (UnloadedAiEntityException var2) {
         ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_AI_AIINTERFACEEXTENDEDPANEL_1, 0.0F);
         var2.printStackTrace();
      }
   }

   private void rename() {
      final String var1;
      try {
         var1 = this.ai.getRealName();
      } catch (UnloadedAiEntityException var2) {
         ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_AI_AIINTERFACEEXTENDEDPANEL_4, 0.0F);
         return;
      }

      (new PlayerGameTextInput("AiInterfaceExtendedPanel_RENAME", (GameClientState)this.getState(), 32, "Rename", "Enter a new name (must be at least 3 letters)", var1) {
         public boolean isOccluded() {
            return false;
         }

         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1x, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public void onFailedTextCheck(String var1x) {
         }

         public void onDeactivate() {
         }

         public boolean onInput(String var1x) {
            String var2;
            if ((var2 = var1x.trim()).length() < 3) {
               this.setErrorMessage("Name to short");
               return false;
            } else {
               System.err.println("[DIALOG] APPLYING AI NAME CHANGE: " + var1x + ": from " + var1 + " to " + var2 + "; changed? " + var1.equals(var2));

               try {
                  if (!var1.equals(var2)) {
                     if (AiInterfaceExtendedPanel.this.ai.getAi() instanceof SendableSegmentController) {
                        SendableSegmentController var4 = (SendableSegmentController)AiInterfaceExtendedPanel.this.ai.getAi();
                        System.err.println("[CLIENT] sending name for object: " + var4 + ": " + var2);
                        var4.getNetworkObject().realName.set(var2, true);

                        assert var4.getNetworkObject().realName.hasChanged();

                        assert var4.getNetworkObject().isChanged();
                     } else if (AiInterfaceExtendedPanel.this.ai.getAi() instanceof AICreature) {
                        AICreature var5 = (AICreature)AiInterfaceExtendedPanel.this.ai.getAi();
                        System.err.println("[CLIENT] sending name for object: " + var5 + ": " + var2);
                        var5.getNetworkObject().realName.set(var2, true);

                        assert var5.getNetworkObject().realName.hasChanged();

                        assert var5.getNetworkObject().isChanged();
                     }
                  }
               } catch (UnloadedAiEntityException var3) {
                  var3.printStackTrace();
               }

               return true;
            }
         }
      }).activate();
   }

   private void delete() {
      System.err.println(this.ai + " delete ");
      ((GameClientState)this.getState()).getPlayer().getPlayerAiManager().removeAI(this.ai);
   }

   private void gotoPos() {
      System.err.println(this.ai + " goto ");
   }

   private void idle() {
      System.err.println(this.ai + " idle ");

      try {
         ((AIGameConfiguration)this.ai.getAi().getAiConfiguration()).get(Types.ORDER).switchSetting("Idling", true);
      } catch (StateParameterNotFoundException var2) {
         var2.printStackTrace();
      } catch (UnloadedAiEntityException var3) {
         ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_AI_AIINTERFACEEXTENDEDPANEL_5, 0.0F);
         var3.printStackTrace();
      }
   }

   private void follow() {
      System.err.println(this.ai + " follow ");

      try {
         ((AIGameConfiguration)this.ai.getAi().getAiConfiguration()).get(Types.FOLLOW_TARGET).switchSetting(((GameClientState)this.getState()).getPlayer().getFirstControlledTransformable().getUniqueIdentifier(), true);
         ((AIGameConfiguration)this.ai.getAi().getAiConfiguration()).get(Types.ORDER).switchSetting("Following", true);
      } catch (StateParameterNotFoundException var1) {
         var1.printStackTrace();
      } catch (PlayerControlledTransformableNotFound var2) {
         var2.printStackTrace();
      } catch (UnloadedAiEntityException var3) {
         var3.printStackTrace();
         ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_AI_AIINTERFACEEXTENDEDPANEL_2, 0.0F);
      }
   }

   private void roam() {
      System.err.println(this.ai + " roam ");

      try {
         ((AIGameConfiguration)this.ai.getAi().getAiConfiguration()).get(Types.ORDER).switchSetting("Roaming", true);
      } catch (StateParameterNotFoundException var1) {
         var1.printStackTrace();
      } catch (UnloadedAiEntityException var2) {
         var2.printStackTrace();
         ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_AI_AIINTERFACEEXTENDEDPANEL_3, 0.0F);
      }
   }

   private void attack() {
      System.err.println(this.ai + " attack ");

      try {
         ((AIGameConfiguration)this.ai.getAi().getAiConfiguration()).get(Types.ORDER).switchSetting("Attacking", true);
      } catch (StateParameterNotFoundException var2) {
         var2.printStackTrace();
      } catch (UnloadedAiEntityException var3) {
         ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_AI_AIINTERFACEEXTENDEDPANEL_6, 0.0F);
         var3.printStackTrace();
      }
   }

   public void cleanUp() {
   }

   public void draw() {
      this.drawAttached();
   }

   public void onInit() {
      GUITextButton var1;
      (var1 = new GUITextButton(this.state, 90, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_AI_AIINTERFACEEXTENDEDPANEL_7, this, this.getAIControlManager())).setUserPointer("Idling");
      var1.setTextPos(5, 1);
      GUITextButton var2;
      (var2 = new GUITextButton(this.state, 90, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_AI_AIINTERFACEEXTENDEDPANEL_8, this, this.getAIControlManager())).setUserPointer("settings");
      var2.setTextPos(5, 1);
      GUITextButton var3;
      (var3 = new GUITextButton(this.state, 90, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_AI_AIINTERFACEEXTENDEDPANEL_9, this, this.getAIControlManager())).setUserPointer("Following");
      var3.setTextPos(5, 1);
      GUITextButton var4;
      (var4 = new GUITextButton(this.state, 90, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_AI_AIINTERFACEEXTENDEDPANEL_10, this, this.getAIControlManager())).setUserPointer("rename");
      var4.setTextPos(5, 1);
      GUITextButton var5;
      (var5 = new GUITextButton(this.state, 80, 20, new Vector4f(0.7F, 0.2F, 0.2F, 0.9F), new Vector4f(0.99F, 0.99F, 0.99F, 1.0F), FontLibrary.getRegularArial15White(), "Discharge", this, this.getAIControlManager())).setUserPointer("delete");
      var5.setTextPos(6, 1);
      var3.getPos().x = var1.getPos().x + 105.0F;
      var4.getPos().x = var3.getPos().x + 105.0F;
      var2.getPos().x = var4.getPos().x + 105.0F;
      var5.getPos().x = var2.getPos().x + 105.0F;
      GUITextOverlay var6;
      (var6 = new GUITextOverlay(10, 10, this.state)).getPos().y = 35.0F;
      var6.setTextSimple(new Object() {
         public String toString() {
            try {
               return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_AI_AIINTERFACEEXTENDEDPANEL_11, AiInterfaceExtendedPanel.this.ai.getRealName());
            } catch (UnloadedAiEntityException var1) {
               return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_AI_AIINTERFACEEXTENDEDPANEL_12, AiInterfaceExtendedPanel.this.ai.getUID());
            }
         }
      });
      this.setMouseUpdateEnabled(true);
      this.attach(var1);
      this.attach(var3);
      this.attach(var2);
      this.attach(var5);
      this.attach(var4);
      this.attach(var6);
      boolean var10000 = this.showAdminSettings;
   }

   public float getHeight() {
      return 80.0F;
   }

   public float getWidth() {
      return 510.0F;
   }

   public AiConfigurationManager getAIControlManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getAiConfigurationManager();
   }
}
