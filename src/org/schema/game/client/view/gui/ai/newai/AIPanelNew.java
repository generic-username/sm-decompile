package org.schema.game.client.view.gui.ai.newai;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;

public class AIPanelNew extends GUIElement implements GUIActiveInterface {
   public GUIMainWindow aiPanel;
   private GUIContentPane currentEntityTab;
   private boolean init;
   private boolean flagFactionTabRecreate;
   private AIEntityScrollableList entityAIList;

   public AIPanelNew(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.flagFactionTabRecreate) {
         this.recreateTabs();
         this.flagFactionTabRecreate = false;
      }

      this.aiPanel.draw();
   }

   public void onInit() {
      if (this.aiPanel != null) {
         this.aiPanel.cleanUp();
      }

      this.aiPanel = new GUIMainWindow(this.getState(), 750, 550, "AIPanelNew");
      this.aiPanel.onInit();
      this.aiPanel.setCloseCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               AIPanelNew.this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().deactivateAll();
            }

         }

         public boolean isOccluded() {
            return !AIPanelNew.this.getState().getController().getPlayerInputs().isEmpty();
         }
      });
      this.aiPanel.orientate(48);
      this.recreateTabs();
      this.init = true;
   }

   public void recreateTabs() {
      String var1 = null;
      if (this.aiPanel.getSelectedTab() < this.aiPanel.getTabs().size()) {
         var1 = ((GUIContentPane)this.aiPanel.getTabs().get(this.aiPanel.getSelectedTab())).getTabName().toString();
      }

      this.aiPanel.clearTabs();
      this.currentEntityTab = this.aiPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_AI_NEWAI_AIPANELNEW_0);
      this.createCurrentEntityPane();
      this.aiPanel.activeInterface = this;
      if (var1 != null) {
         for(int var2 = 0; var2 < this.aiPanel.getTabs().size(); ++var2) {
            if (((GUIContentPane)this.aiPanel.getTabs().get(var2)).getTabName().equals(var1)) {
               this.aiPanel.setSelectedTab(var2);
               return;
            }
         }
      }

   }

   public void update(Timer var1) {
      this.entityAIList.update(var1);
   }

   public void createCurrentEntityPane() {
      if (this.entityAIList != null) {
         this.entityAIList.cleanUp();
      }

      this.entityAIList = new AIEntityScrollableList(this.getState(), this.currentEntityTab.getContent(0));
      this.entityAIList.onInit();
      this.currentEntityTab.getContent(0).attach(this.entityAIList);
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFactionManager().getFaction(this.getOwnPlayer().getFactionId());
   }

   public float getHeight() {
      return this.aiPanel.getHeight();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public float getWidth() {
      return this.aiPanel.getWidth();
   }

   public boolean isActive() {
      return this.getState().getController().getPlayerInputs().isEmpty();
   }

   public void reset() {
      this.aiPanel.reset();
   }
}
