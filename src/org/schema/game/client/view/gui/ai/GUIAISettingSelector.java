package org.schema.game.client.view.gui.ai;

import java.util.ArrayList;
import org.schema.game.common.controller.ai.AIConfiguationElements;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUISettingsElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Mouse;

public class GUIAISettingSelector extends GUISettingsElement implements GUICallback {
   private GUITextOverlay settingName;
   private GUIOverlay leftArrow;
   private GUIOverlay rightArrow;
   private AIConfiguationElements setting;
   private boolean init;

   public GUIAISettingSelector(InputState var1, AIConfiguationElements var2) {
      super(var1);
      this.setMouseUpdateEnabled(true);
      this.setCallback(this);
      this.setting = var2;
      this.leftArrow = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "tools-16x16-gui-"), this.getState());
      this.rightArrow = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "tools-16x16-gui-"), this.getState());
      this.settingName = new GUITextOverlay(140, 30, FontLibrary.getBoldArial18(), this.getState());
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.getEventButtonState() && var2.getEventButton() == 0) {
         try {
            this.setting.switchSetting(true);
            return;
         } catch (StateParameterNotFoundException var3) {
            var3.printStackTrace();
            GLFrame.processErrorDialogException(var3, this.getState());
         }
      }

   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.settingName.draw();
      this.leftArrow.draw();
      this.rightArrow.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.settingName.setText(new ArrayList());
      this.settingName.getText().add(this.setting.getCurrentState().toString());
      this.settingName.setPos(0.0F, 5.0F, 0.0F);
      this.settingName.onInit();
      this.leftArrow.setMouseUpdateEnabled(true);
      this.rightArrow.setMouseUpdateEnabled(true);
      this.leftArrow.setCallback(new GUICallback() {
         private long startedL = -1L;

         public void callback(GUIElement var1, MouseEvent var2) {
            boolean var4 = false;
            if (Mouse.isButtonDown(0)) {
               if (this.startedL == -1L) {
                  this.startedL = System.currentTimeMillis();
               }

               if (System.currentTimeMillis() - this.startedL > 1000L) {
                  var4 = true;
               }
            } else {
               this.startedL = -1L;
            }

            if (var2.pressedLeftMouse() || var4) {
               try {
                  GUIAISettingSelector.this.setting.switchSettingBack(true);
                  GUIAISettingSelector.this.updateText();
                  return;
               } catch (StateParameterNotFoundException var3) {
                  var3.printStackTrace();
                  GLFrame.processErrorDialogException(var3, GUIAISettingSelector.this.getState());
               }
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.rightArrow.setCallback(new GUICallback() {
         private long startedR = -1L;

         public void callback(GUIElement var1, MouseEvent var2) {
            boolean var4 = false;
            if (Mouse.isButtonDown(0)) {
               if (this.startedR == -1L) {
                  this.startedR = System.currentTimeMillis();
               }

               if (System.currentTimeMillis() - this.startedR > 1000L) {
                  var4 = true;
               }
            } else {
               this.startedR = -1L;
            }

            if (var2.pressedLeftMouse() || var4) {
               try {
                  GUIAISettingSelector.this.setting.switchSetting(true, GUIAISettingSelector.this.getState());
                  GUIAISettingSelector.this.updateText();
                  return;
               } catch (StateParameterNotFoundException var3) {
                  var3.printStackTrace();
                  GLFrame.processErrorDialogException(var3, GUIAISettingSelector.this.getState());
               }
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.leftArrow.setSpriteSubIndex(21);
      this.rightArrow.setSpriteSubIndex(20);
      this.settingName.getPos().x = this.leftArrow.getWidth();
      this.rightArrow.getPos().x = this.leftArrow.getWidth() + this.settingName.getWidth();
      this.init = true;
   }

   protected void doOrientation() {
   }

   public float getHeight() {
      return 30.0F;
   }

   public float getWidth() {
      return this.settingName.getWidth() + this.leftArrow.getWidth() + this.rightArrow.getWidth();
   }

   public boolean isOccluded() {
      return false;
   }

   public boolean isPositionCenter() {
      return false;
   }

   private void updateText() {
      this.settingName.getText().set(0, this.setting.getCurrentState().toString());
   }
}
