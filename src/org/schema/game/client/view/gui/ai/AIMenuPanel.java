package org.schema.game.client.view.gui.ai;

import java.util.Observable;
import java.util.Observer;
import org.schema.game.client.controller.manager.AiConfigurationManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.input.InputState;

public class AIMenuPanel extends GUIElement implements Observer, GUICallback {
   private GUIOverlay background = new GUIOverlay(Controller.getResLoader().getSprite("ai-panel-gui-"), this.getState());
   private GUIAncor content;
   private AiCrewPanel aiCrewPanel;
   private AiFleetPanel aiFleetPanel;
   private AiEntityPanel aiEntityPanel;
   private GUITextButton aiCrewPanelButton;
   private GUITextButton aiFleetPanelButton;
   private GUITextButton aiEntityPanelButton;
   private boolean firstDraw = true;

   public AIMenuPanel(InputState var1) {
      super(var1);
      ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getAiConfigurationManager().addObserver(this);
   }

   public void attach(GUIElement var1) {
      this.background.attach(var1);
   }

   public void detach(GUIElement var1) {
      this.background.detach(var1);
   }

   public float getHeight() {
      return this.background.getHeight();
   }

   public float getWidth() {
      return this.background.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void cleanUp() {
      this.background.cleanUp();
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.background.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.aiCrewPanel = new AiCrewPanel(this.getState(), 522, 290);
      this.aiFleetPanel = new AiFleetPanel(this.getState(), 522, 290);
      this.aiEntityPanel = new AiEntityPanel(this.getState(), 522, 290);
      this.content = new GUIAncor(this.getState(), 522.0F, 312.0F);
      this.content.getPos().set(251.0F, 107.0F, 0.0F);
      this.aiCrewPanelButton = new GUITextButton(this.getState(), 174, 22, "Crew", this);
      this.aiFleetPanelButton = new GUITextButton(this.getState(), 174, 22, "Fleet", this);
      this.aiEntityPanelButton = new GUITextButton(this.getState(), 174, 22, "Entity", this);
      this.background.onInit();
      this.aiEntityPanel.onInit();
      this.aiFleetPanel.onInit();
      this.aiCrewPanel.onInit();
      this.aiEntityPanel.setMouseUpdateEnabled(true);
      this.aiFleetPanel.setMouseUpdateEnabled(true);
      this.aiCrewPanel.setMouseUpdateEnabled(true);
      this.aiEntityPanel.getPos().y = 22.0F;
      this.aiFleetPanel.getPos().y = 22.0F;
      this.aiCrewPanel.getPos().y = 22.0F;
      this.aiCrewPanelButton.getPos().x = 0.0F;
      this.aiFleetPanelButton.getPos().x = 174.0F;
      this.aiEntityPanelButton.getPos().x = 348.0F;
      this.content.attach(this.aiEntityPanelButton);
      this.content.attach(this.aiFleetPanelButton);
      this.content.attach(this.aiCrewPanelButton);
      this.content.attach(this.aiCrewPanel);
      this.background.attach(this.content);
      super.attach(this.background);
      this.doOrientation();
      this.firstDraw = false;
   }

   public AiConfigurationManager getAiManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getAiConfigurationManager();
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         if (var1 == this.aiCrewPanelButton) {
            this.activateCrewPanel();
         }

         if (var1 == this.aiFleetPanelButton) {
            this.activateFleetPanel();
         }

         if (var1 == this.aiEntityPanelButton) {
            this.activateEntityPanel();
         }
      }

   }

   public boolean isOccluded() {
      return false;
   }

   public void activateCrewPanel() {
      this.content.detach(this.aiFleetPanel);
      this.content.detach(this.aiEntityPanel);
      this.content.detach(this.aiCrewPanel);
      this.content.attach(this.aiCrewPanel);
   }

   public void activateFleetPanel() {
      this.content.detach(this.aiFleetPanel);
      this.content.detach(this.aiEntityPanel);
      this.content.detach(this.aiCrewPanel);
      this.content.attach(this.aiFleetPanel);
   }

   public void activateEntityPanel() {
      this.content.detach(this.aiFleetPanel);
      this.content.detach(this.aiEntityPanel);
      this.content.detach(this.aiCrewPanel);
      this.content.attach(this.aiEntityPanel);
   }

   public void update(Observable var1, Object var2) {
      if (var2 != null && var2 instanceof SegmentController) {
         this.activateEntityPanel();
      }

   }
}
