package org.schema.game.client.view.gui.trade;

public class ShopNotFoundException extends Exception {
   private static final long serialVersionUID = 1L;

   public ShopNotFoundException() {
   }

   public ShopNotFoundException(String var1, Throwable var2, boolean var3, boolean var4) {
      super(var1, var2, var3, var4);
   }

   public ShopNotFoundException(String var1, Throwable var2) {
      super(var1, var2);
   }

   public ShopNotFoundException(String var1) {
      super(var1);
   }

   public ShopNotFoundException(Throwable var1) {
      super(var1);
   }
}
