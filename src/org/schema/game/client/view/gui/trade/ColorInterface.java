package org.schema.game.client.view.gui.trade;

import javax.vecmath.Vector4f;

public interface ColorInterface {
   Vector4f getColor();
}
