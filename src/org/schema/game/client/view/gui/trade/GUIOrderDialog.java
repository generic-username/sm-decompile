package org.schema.game.client.view.gui.trade;

import java.util.Observable;
import java.util.Observer;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.controller.elements.shop.ShopElementManager;
import org.schema.game.common.controller.trade.TradeNodeClient;
import org.schema.game.common.controller.trade.TradeOrder;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIEnterableList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITabbedContent;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.ListColorPalette;
import org.schema.schine.input.InputState;

public class GUIOrderDialog extends GUIInputPanel implements Observer {
   private boolean init;
   private final TradeOrder tradeOrder;
   private final TradeNodeClient tradeNode;
   final int rowHeight = 24;
   private GUIScrollablePanel p;
   private GUIElementList mainList;
   private Vector4f red = new Vector4f(1.0F, 0.2F, 0.2F, 1.0F);
   private Vector4f green = new Vector4f(0.2F, 1.0F, 0.2F, 1.0F);
   private Vector4f white = new Vector4f(0.9F, 0.9F, 0.9F, 1.0F);
   private OrderDialog dialog;

   public boolean isActive() {
      return super.isActive() && this.dialog.isActive();
   }

   public GUIOrderDialog(InputState var1, OrderDialog var2, TradeNodeClient var3, ShopInterface var4, OrderDialog var5, TradeOrder var6) throws ShopNotFoundException {
      super("ORDER_PNL", var1, 800, 600, var5, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_0, "");
      this.tradeOrder = var6;
      this.tradeNode = var3;
      this.dialog = var2;
      this.onInit();
      GUIDialogWindow var7;
      (var7 = (GUIDialogWindow)this.getBackground()).getMainContentPane().setTextBoxHeightLast(148);
      this.createTradeStats(var7.getMainContentPane().getContent(0));
      var7.getMainContentPane().addNewTextBox(140);
      GUITradeOrderScrollableList var9;
      (var9 = new GUITradeOrderScrollableList(this.getState(), var4, var6, this.tradeNode, var7.getMainContentPane().getContent(1))).onInit();
      var7.getMainContentPane().getContent(1).attach(var9);
      var7.getMainContentPane().addNewTextBox(150);
      GUITabbedContent var10;
      (var10 = new GUITabbedContent(this.getState(), var7.getMainContentPane().getContent(2))).onInit();
      GUIContentPane var11 = var10.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_1);
      GUIContentPane var12 = var10.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_2);
      var10.activationInterface = var2;
      var11.setTextBoxHeightLast(10);
      var12.setTextBoxHeightLast(10);
      GUIPricesOfferScrollableList var8;
      (var8 = new GUIPricesOfferScrollableList(this.getState(), var11.getContent(0), var6, this.tradeNode, true)).onInit();
      var11.getContent(0).attach(var8);
      (var8 = new GUIPricesOfferScrollableList(this.getState(), var12.getContent(0), var6, this.tradeNode, false)).onInit();
      var12.getContent(0).attach(var8);
      var7.getMainContentPane().getContent(2).attach(var10);
   }

   private void createTradeStats(GUIAncor var1) {
      this.p = new GUIScrollablePanel(10.0F, 10.0F, var1, this.getState());
      this.mainList = new GUIElementList(this.getState());
      this.p.setContent(this.mainList);
      this.mainList.setScrollPane(this.p);
      this.addRow(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_14, new Object() {
         public String toString() {
            long var1 = GUIOrderDialog.this.tradeOrder.getBuyPrice() - GUIOrderDialog.this.tradeOrder.getSellPrice();
            return StringTools.formatSeperated(GUIOrderDialog.this.tradeOrder.getBuyPrice()) + " / " + StringTools.formatSeperated(GUIOrderDialog.this.tradeOrder.getSellPrice()) + StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_13, StringTools.formatSeperated(Math.abs(var1))) + (var1 < 0L ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_3 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_4);
         }
      }, var1, new ColorInterface() {
         public Vector4f getColor() {
            long var1;
            if ((var1 = GUIOrderDialog.this.tradeOrder.getBuyPrice() - GUIOrderDialog.this.tradeOrder.getSellPrice()) == 0L) {
               return GUIOrderDialog.this.white;
            } else {
               return var1 < 0L ? GUIOrderDialog.this.green : GUIOrderDialog.this.red;
            }
         }
      }, this.mainList, true);
      GUIElementList var2 = new GUIElementList(this.getState());
      this.addRow(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_6, new Object() {
         public String toString() {
            return StringTools.formatSeperated(GUIOrderDialog.this.tradeOrder.getUsedTradeShips()) + StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_7, StringTools.formatSeperated(ShopElementManager.TRADING_GUILD_COST_PER_CARGO_SHIP), StringTools.formatSeperated(GUIOrderDialog.this.tradeOrder.getTradeShipCost()));
         }
      }, var1, (ColorInterface)null, var2, true);
      this.addRow(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_8, new Object() {
         public String toString() {
            return StringTools.formatSeperated(GUIOrderDialog.this.tradeOrder.getTradeDistance()) + StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_9, StringTools.formatSeperated(ShopElementManager.TRADING_GUILD_COST_PER_SYSTEM), StringTools.formatSeperated(GUIOrderDialog.this.tradeOrder.getPricePerDistance()));
         }
      }, var1, (ColorInterface)null, var2, false);
      this.addRow(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_10, new Object() {
         public String toString() {
            return StringTools.formatSeperated(GUIOrderDialog.this.tradeOrder.getTradeingGuildProfit()) + StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_11, StringTools.formatPointZero(ShopElementManager.TRADING_GUILD_PROFIT_OF_VALUE * 100.0D));
         }
      }, var1, (ColorInterface)null, var2, true);
      this.addRow(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_18, new Object() {
         public String toString() {
            return StringTools.formatSeperated(GUIOrderDialog.this.tradeOrder.getTradeingGuildProfitPerSystem()) + StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_19, StringTools.formatPointZero(ShopElementManager.TRADING_GUILD_PROFIT_OF_VALUE_PER_SYSTEM * 100.0D));
         }
      }, var1, (ColorInterface)null, var2, false);
      this.addRow(this.mainList, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_5, new Object() {
         public String toString() {
            return StringTools.formatSeperated(GUIOrderDialog.this.tradeOrder.getTradingGuildPrice());
         }
      }, var1, (ColorInterface)null, false, var2);
      this.addRow(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_12, new Object() {
         public String toString() {
            long var1 = GUIOrderDialog.this.tradeOrder.getTotalPrice();
            return StringTools.formatSeperated(Math.abs(var1)) + (var1 < 0L ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_15 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_16);
         }
      }, var1, new ColorInterface() {
         public Vector4f getColor() {
            long var1;
            if ((var1 = GUIOrderDialog.this.tradeOrder.getTotalPrice()) == 0L) {
               return GUIOrderDialog.this.white;
            } else {
               return var1 < 0L ? GUIOrderDialog.this.green : GUIOrderDialog.this.red;
            }
         }
      }, this.mainList, true);
      this.mainList.onInit();
      this.mainList.updateDim();
      var1.attach(this.p);
   }

   private void addRow(GUIElementList var1, String var2, Object var3, final GUIAncor var4, final ColorInterface var5, boolean var6, GUIElementList var7) {
      Vector4f var16 = var6 ? ListColorPalette.buyListBackgroundColor : ListColorPalette.buyListBackgroundColorAlternate;
      GUIColoredRectangle var8;
      (var8 = new GUIColoredRectangle(this.getState(), 10.0F, 24.0F, var16) {
         public void draw() {
            this.setWidth((int)var4.getWidth());
            this.setHeight(24);
            super.draw();
         }
      }).onInit();
      var8.setName("BG");
      GUITextOverlayTable var9;
      (var9 = new GUITextOverlayTable(10, 10, this.getState()) {
         public void draw() {
            if (var5 != null) {
               this.setColor(var5.getColor());
            }

            super.draw();
         }
      }).setTextSimple("[+] " + var2);
      var9.setPos(4.0F, 4.0F, 0.0F);
      var8.attach(var9);
      GUITextOverlayTable var10;
      (var10 = new GUITextOverlayTable(10, 10, this.getState()) {
         public void draw() {
            if (var5 != null) {
               this.setColor(var5.getColor());
            }

            super.draw();
         }
      }).setTextSimple(var3);
      var10.setPos(204.0F, 4.0F, 0.0F);
      var8.attach(var10);
      GUIColoredRectangle var15;
      (var15 = new GUIColoredRectangle(this.getState(), 10.0F, 24.0F, var16) {
         public void draw() {
            this.setWidth((int)var4.getWidth());
            this.setHeight(24);
            super.draw();
         }
      }).onInit();
      GUITextOverlayTable var11;
      (var11 = new GUITextOverlayTable(10, 10, this.getState()) {
         public void draw() {
            if (var5 != null) {
               this.setColor(var5.getColor());
            }

            super.draw();
         }
      }).setTextSimple("[-] " + var2);
      var11.setPos(4.0F, 4.0F, 0.0F);
      var15.setName("BGEXT");
      var15.attach(var9);
      GUITextOverlayTable var12;
      (var12 = new GUITextOverlayTable(10, 10, this.getState()) {
         public void draw() {
            if (var5 != null) {
               this.setColor(var5.getColor());
            }

            super.draw();
         }
      }).setTextSimple(var3);
      var12.setPos(204.0F, 4.0F, 0.0F);
      var15.attach(var10);
      GUIEnterableList var13;
      (var13 = new GUIEnterableList(this.getState(), var7, var8, var15)).extendedHighlightBottomDist = 4.0F;
      var13.scrollPanel = this.p;

      assert var13.isCollapsed();

      var13.expandedBackgroundColor = var16;
      var13.addObserver(this);

      assert var13.isCollapsed();

      var13.onInit();

      assert var13.isCollapsed();

      GUIListElement var14 = new GUIListElement(var13, this.getState());
      var1.add(var14);
   }

   private void addRow(Object var1, Object var2, final GUIAncor var3, final ColorInterface var4, GUIElementList var5, boolean var6) {
      GUIColoredRectangle var9;
      (var9 = new GUIColoredRectangle(this.getState(), 10.0F, 24.0F, var6 ? ListColorPalette.buyListBackgroundColor : ListColorPalette.buyListBackgroundColorAlternate) {
         public void draw() {
            this.setWidth((int)var3.getWidth());
            this.setHeight(24);
            super.draw();
         }
      }).onInit();
      GUITextOverlayTable var10;
      (var10 = new GUITextOverlayTable(10, 10, this.getState()) {
         public void draw() {
            if (var4 != null) {
               this.setColor(var4.getColor());
            }

            super.draw();
         }
      }).setTextSimple(var1);
      var10.setPos(4.0F, 4.0F, 0.0F);
      var9.attach(var10);
      GUITextOverlayTable var7;
      (var7 = new GUITextOverlayTable(10, 10, this.getState()) {
         public void draw() {
            if (var4 != null) {
               this.setColor(var4.getColor());
            }

            super.draw();
         }
      }).setTextSimple(var2);
      var7.setPos(204.0F, 4.0F, 0.0F);
      var9.attach(var7);
      GUIListElement var8 = new GUIListElement(var9, this.getState());
      var5.add(var8);
   }

   public void onInit() {
      if (!this.init) {
         super.onInit();
         this.init = true;
      }
   }

   public void draw() {
      assert this.init;

      if ((TradeNodeClient)((GameClientState)this.getState()).getController().getClientChannel().getGalaxyManagerClient().getTradeNodeDataById().get(this.tradeNode.getEntityDBId()) != this.tradeNode) {
         this.dialog.queueDeactivate = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIORDERDIALOG_17;
      }

      super.draw();
   }

   public void update(Observable var1, Object var2) {
      if (var1 != null && var1 instanceof GUIEnterableList) {
         this.mainList.updateDim();
      }

   }
}
