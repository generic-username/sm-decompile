package org.schema.game.client.view.gui.trade;

import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.controller.ShopInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.input.InputState;

public class GUITradePanelIndependent extends GUIInputPanel {
   public GUITradePanelIndependent(InputState var1, int var2, int var3, GUICallback var4) {
      super("TradeNodePanelIndependent", var1, var2, var3, var4, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEPANELINDEPENDENT_0, "");
      this.setOkButton(false);
      this.onInit();
   }

   public void onInit() {
      super.onInit();
      ((GUIDialogWindow)this.background).getMainContentPane().setTextBoxHeightLast(10);
      GUITradeNodeScrollableList var1;
      (var1 = new GUITradeNodeScrollableList(this.getState(), (ShopInterface)null, ((GUIDialogWindow)this.background).getMainContentPane().getContent(0))).onInit();
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(0).attach(var1);
   }

   public boolean isActive() {
      return (this.getState().getController().getPlayerInputs().isEmpty() || ((DialogInterface)this.getState().getController().getPlayerInputs().get(this.getState().getController().getPlayerInputs().size() - 1)).getInputPanel() == this) && super.isActive();
   }
}
