package org.schema.game.client.view.gui.trade;

import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public class TradeNodeTypeSearchDialog extends PlayerInput implements GUICallback {
   private final GUITradeNodeSearchInputPanel orderDialog;

   public TradeNodeTypeSearchDialog(GameClientState var1, ShopInterface var2, ElementInformation var3) throws ShopNotFoundException {
      super(var1);
      this.orderDialog = new GUITradeNodeSearchInputPanel(var1, this, var3, var2, this);
      this.orderDialog.setOkButton(false);
      this.orderDialog.setCancelButtonText(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_TRADENODETYPESEARCHDIALOG_0);
      this.orderDialog.onInit();
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public GUIElement getInputPanel() {
      return this.orderDialog;
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (!this.isOccluded() && var2.getEventButtonState() && var2.getEventButton() == 0) {
         if (var1.getUserPointer().equals("OK")) {
            this.deactivate();
         }

         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            this.cancel();
         }
      }

   }

   private void pressedOK() {
      this.deactivate();
   }

   public void onDeactivate() {
   }
}
