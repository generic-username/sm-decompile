package org.schema.game.client.view.gui.trade;

import java.util.Observable;
import java.util.Observer;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIEnterableList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.input.InputState;

public class GUITradeNodeSearchInputPanel extends GUIInputPanel implements Observer {
   private boolean init;
   final int rowHeight = 24;
   private GUIElementList mainList;
   private GUIActiveInterface dialog;

   public boolean isActive() {
      return super.isActive() && this.dialog.isActive();
   }

   public GUITradeNodeSearchInputPanel(InputState var1, GUIActiveInterface var2, ElementInformation var3, ShopInterface var4, TradeNodeTypeSearchDialog var5) throws ShopNotFoundException {
      super("ORDER_PNLMA", var1, 800, 600, var5, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADENODESEARCHINPUTPANEL_0, var3.getName()), "");
      this.dialog = var2;
      this.onInit();
      GUIDialogWindow var7;
      (var7 = (GUIDialogWindow)this.getBackground()).getMainContentPane().setTextBoxHeightLast(148);
      ((GameClientState)var1).getController().getClientChannel().requestTradeNodesFor(var3.id);
      TradeTypeSearchNodeScrollableList var6;
      (var6 = new TradeTypeSearchNodeScrollableList(this.getState(), var4, var7.getMainContentPane().getContent(0))).onInit();
      var7.getMainContentPane().getContent(0).attach(var6);
   }

   public void onInit() {
      if (!this.init) {
         super.onInit();
         this.init = true;
      }
   }

   public void draw() {
      assert this.init;

      super.draw();
   }

   public void update(Observable var1, Object var2) {
      if (var1 != null && var1 instanceof GUIEnterableList) {
         this.mainList.updateDim();
      }

   }
}
