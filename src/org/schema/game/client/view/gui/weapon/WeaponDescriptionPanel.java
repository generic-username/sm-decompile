package org.schema.game.client.view.gui.weapon;

import java.util.ArrayList;
import org.newdawn.slick.Color;
import org.newdawn.slick.UnicodeFont;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.combination.CombinationAddOn;
import org.schema.game.common.controller.elements.effectblock.EffectCollectionManager;
import org.schema.game.common.controller.elements.powerbattery.PowerBatteryCollectionManager;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class WeaponDescriptionPanel extends GUIElement {
   private ArrayList text;
   private GUITextOverlay textOverlay;
   private GUIElementList weaponElementList;
   private GUIScrollablePanel scrollPanel;
   private boolean firstDraw = true;
   private GUIListElement elem;
   private GUIAncor textOverlayAncor;

   public WeaponDescriptionPanel(InputState var1, UnicodeFont var2, int var3, int var4) {
      super(var1);
      this.scrollPanel = new GUIScrollablePanel((float)var3, (float)var4, var1);
      this.weaponElementList = new GUIElementList(var1);
      if (isNewHud()) {
         this.textOverlayAncor = new GUIAncor(var1, 256.0F, 200.0F);
         this.textOverlay = new GUITextOverlay(256, 200, var2, var1);
         this.text = new ArrayList();
         this.text.add("");
         this.textOverlay.setColor(Color.green);
         this.textOverlayAncor.attach(this.textOverlay);
         this.scrollPanel.setContent(this.textOverlayAncor);
      }

   }

   public WeaponDescriptionPanel(InputState var1, UnicodeFont var2, GUIElement var3) {
      super(var1);
      this.scrollPanel = new GUIScrollablePanel(100.0F, 100.0F, var3, var1);
      this.weaponElementList = new GUIElementList(var1);
      if (isNewHud()) {
         this.textOverlayAncor = new GUIAncor(var1, 256.0F, 200.0F);
         this.textOverlay = new GUITextOverlay(256, 200, var2, var1);
         this.text = new ArrayList();
         this.text.add("");
         this.textOverlay.setColor(Color.green);
         this.textOverlayAncor.attach(this.textOverlay);
         this.scrollPanel.setContent(this.textOverlayAncor);
      }

   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      if (isNewHud()) {
         this.textOverlay.setText(this.getText());
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.scrollPanel.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.scrollPanel.onInit();
      this.firstDraw = false;
   }

   public float getHeight() {
      return this.scrollPanel.getHeight();
   }

   public float getWidth() {
      return this.scrollPanel.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public ArrayList getText() {
      return this.text;
   }

   public void setText(ArrayList var1) {
      this.text = var1;
   }

   public void reset() {
      if (isNewHud()) {
         this.text.set(0, "");
         this.scrollPanel.setContent(this.textOverlayAncor);
      }

   }

   public void update(ElementCollectionManager var1) {
      if (var1.getContainer().getSegmentController() == ((GameClientState)this.getState()).getCurrentPlayerObject()) {
         this.weaponElementList.clear();
         if (var1 instanceof PowerBatteryCollectionManager) {
            GUIElementList var8 = new GUIElementList(this.getState());
            this.loadEntries(var1, var8, (ControlBlockElementCollectionManager)null, (ControlBlockElementCollectionManager)null);
            this.scrollPanel.setContent(var8);
         } else if (var1 instanceof ControlBlockElementCollectionManager) {
            this.weaponElementList.clear();
            final ControlBlockElementCollectionManager var2 = (ControlBlockElementCollectionManager)var1;
            final GUIElementList var7 = new GUIElementList(this.getState());
            StringBuffer var3 = new StringBuffer();
            final ControlBlockElementCollectionManager var4 = var2.getSupportCollectionManager();
            final EffectCollectionManager var5 = var2.getEffectCollectionManager();
            var3.append(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONDESCRIPTIONPANEL_0, var2.getModuleName(), var2.getElementCollections().size()));
            float var6;
            if (var4 != null) {
               var6 = CombinationAddOn.getRatio(var2, var4);
               var3.append(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONDESCRIPTIONPANEL_1, var4.getModuleName(), var6 * 100.0F));
            }

            if (var5 != null) {
               var6 = CombinationAddOn.getRatio(var2, var5);
               var3.append(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONDESCRIPTIONPANEL_2, var5.getModuleName(), var6 * 100.0F));
            }

            var7.setScrollPane(this.scrollPanel);
            if (var2.getElementCollections().size() > 20) {
               GUITextButton var9 = new GUITextButton(this.getState(), 400, 20, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONDESCRIPTIONPANEL_3, var2.getElementCollections().size()), new GUICallback() {
                  public void callback(GUIElement var1, MouseEvent var2x) {
                     var7.remove(WeaponDescriptionPanel.this.elem);
                     WeaponDescriptionPanel.this.loadEntries(var2, var7, var4, var5);
                  }

                  public boolean isOccluded() {
                     return false;
                  }
               });
               this.elem = new GUIListElement(var9, var9, this.getState());
               var7.add(this.elem);
            } else {
               this.loadEntries(var2, var7, var4, var5);
            }

            this.scrollPanel.setContent(var7);
         } else {
            System.err.println("EXCEPTION: UNKNOWN MANAGER: " + var1);
         }
      }
   }

   private void loadEntries(ElementCollectionManager var1, GUIElementList var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      GUIKeyValueEntry[] var5;
      GUIListElement var12;
      if ((var5 = var1.getGUICollectionStats()) != null) {
         int var6 = (var5 = var5).length;

         for(int var7 = 0; var7 < var6; ++var7) {
            GUIAncor var8 = var5[var7].get((GameClientState)this.getState());
            var12 = new GUIListElement(var8, var8, this.getState());
            var2.add(var12);
         }
      }

      for(int var9 = 0; var9 < var1.getElementCollections().size(); ++var9) {
         ElementCollection var10;
         ControllerManagerGUI var11;
         if ((var11 = (var10 = (ElementCollection)var1.getElementCollections().get(var9)).createUnitGUI((GameClientState)this.getState(), var3, var4)) != null) {
            var12 = var11.getListEntry((GameClientState)this.getState(), var2);
            var2.add(var12);
         } else {
            System.err.println("Not creating weapon panel entry for " + var10);
         }
      }

   }

   public void update(SegmentController var1) {
      if (!isNewHud()) {
         if (var1 instanceof SegmentController) {
            StringBuffer var2;
            (var2 = new StringBuffer()).append("Undock " + var1.toNiceString() + "\nfrom you by executing this!\n");
            this.text.set(0, var2.toString());
            this.scrollPanel.setContent(this.textOverlayAncor);
         }

      }
   }

   public void update(String var1) {
      if (!isNewHud()) {
         StringBuffer var2;
         if (var1.equals("CORE")) {
            (var2 = new StringBuffer()).append("Type: \t\tDocking Beam\nLocation:\t\t" + Ship.core + "\n");
            this.text.set(0, var2.toString());
            this.scrollPanel.setContent(this.textOverlayAncor);
         } else {
            if (var1.equals("DOCK")) {
               (var2 = new StringBuffer()).append("Undock yourself by executing this!\n");
               this.text.set(0, var2.toString());
               this.scrollPanel.setContent(this.textOverlayAncor);
            }

         }
      }
   }

   public void update(Timer var1) {
      if (this.scrollPanel.getContent() == this.weaponElementList) {
         this.weaponElementList.update(var1);
      }

      super.update(var1);
   }
}
