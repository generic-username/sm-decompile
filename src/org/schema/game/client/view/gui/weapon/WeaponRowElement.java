package org.schema.game.client.view.gui.weapon;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.element.world.ClientSegmentProvider;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.SlotAssignment;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.controller.elements.combination.CombinationAddOn;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementDocking;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class WeaponRowElement implements WeaponRowElementInterface {
   private static Vector3i absPosTmp = new Vector3i();
   private final SegmentPiece piece;
   public final ElementInformation info;
   public final SegmentController segmentController;
   public final GameClientState state;
   public final List descriptionList = new ObjectArrayList();
   public SlotAssignment shipConfiguration;
   public GUIAncor weaponColumn;
   public GUIAncor mainSizeColumn;
   public GUIAncor secondaryColumn;
   public GUIAncor sizeColumn;
   public GUIAncor keyColumn;
   public GUIAncor tertiaryColumn;
   private int key = -1;
   private short supportType;
   private long supportPosL;
   private float supportRatio;
   private int effectSize;
   private float effectRatio;
   private short effectType;
   private long effectPosL;
   private int supportSize;
   private int weaponBlockSize;
   private WeaponSlotOverlayElement weaponIcon;
   private WeaponSlotOverlayElement supportIcon;
   private WeaponSlotOverlayElement effectIcon;

   public WeaponRowElement(SegmentPiece var1) {
      assert ElementKeyMap.isValidType(var1.getType());

      this.piece = var1;
      this.info = ElementKeyMap.getInfo(var1.getType());
      this.state = (GameClientState)var1.getSegment().getSegmentController().getState();
      this.segmentController = var1.getSegment().getSegmentController();
      this.shipConfiguration = var1.getSegmentController().getSlotAssignment();
      ManagerContainer var2;
      ManagerModuleCollection var3;
      if (this.segmentController instanceof ManagedSegmentController && (var3 = (var2 = ((ManagedSegmentController)var1.getSegment().getSegmentController()).getManagerContainer()).getModulesControllerMap().get(var1.getType())) != null) {
         ControlBlockElementCollectionManager var8 = (ControlBlockElementCollectionManager)var3.getElementManager().getCollectionManagersMap().get(var1.getAbsoluteIndex());
         this.weaponBlockSize = var8.getTotalSize();
         short var4;
         long var5;
         ManagerModuleCollection var9;
         ControlBlockElementCollectionManager var10;
         if (var8.getSlaveConnectedElement() != Long.MIN_VALUE) {
            var4 = (short)ElementCollection.getType(var8.getSlaveConnectedElement());
            var5 = ElementCollection.getPosIndexFrom4(var8.getSlaveConnectedElement());
            this.supportType = var4;
            this.supportPosL = var5;
            if ((var9 = var2.getModulesControllerMap().get(var4)) != null) {
               if ((var10 = (ControlBlockElementCollectionManager)var9.getCollectionManagersMap().get(var5)) != null) {
                  this.supportRatio = CombinationAddOn.getRatio(var8, var10);
                  this.supportSize = var10.getTotalSize();
               }
            } else {
               System.err.println("WARN: Something is wrong with combi");
            }
         }

         if (var8.getEffectConnectedElement() != Long.MIN_VALUE) {
            var4 = (short)ElementCollection.getType(var8.getEffectConnectedElement());
            var5 = ElementCollection.getPosIndexFrom4(var8.getEffectConnectedElement());
            this.effectType = var4;
            this.effectPosL = var5;
            if ((var9 = var2.getModulesControllerMap().get(var4)) != null) {
               if ((var10 = (ControlBlockElementCollectionManager)var9.getCollectionManagersMap().get(var5)) != null) {
                  float var7 = CombinationAddOn.getRatio(var8, var10);
                  this.effectSize = var10.getTotalSize();
                  this.effectRatio = var7;
               } else {
                  System.err.println("WARN: Something is wrong with combi");
               }
            }
         }
      }

      this.initOverlays();
      this.updateInfo(var1, this.descriptionList);
   }

   private void initOverlays() {
      this.weaponIcon = new WeaponSlotOverlayElement(this.state);
      this.weaponIcon.setScale(0.75F, 0.75F, 0.75F);
      this.weaponIcon.setType(this.piece.getType(), this.piece.getAbsoluteIndex());
      this.weaponIcon.setSpriteSubIndex(ElementKeyMap.getInfo(this.piece.getType()).getBuildIconNum());
      GUITextOverlay var1 = new GUITextOverlay(10, 10, this.state);
      if (this.info.id == 670) {
         var1.setTextSimple(new Object() {
            public String toString() {
               long var1 = WeaponRowElement.this.piece.getAbsoluteIndexWithType4();
               String var3;
               if ((var3 = (String)WeaponRowElement.this.piece.getSegmentController().getTextMap().get(var1)) == null) {
                  ((ClientSegmentProvider)WeaponRowElement.this.piece.getSegmentController().getSegmentProvider()).getSendableSegmentProvider().clientTextBlockRequest(var1);
                  var3 = "";
               }

               return var3;
            }
         });
      } else {
         var1.setTextSimple(this.info.getName());
      }

      this.supportIcon = new WeaponSlotOverlayElement(this.state);
      this.supportIcon.setScale(0.65F, 0.65F, 0.65F);
      this.supportIcon.setSlotStyle(1);
      this.supportIcon.setTiedToType(this.piece.getType());
      this.supportIcon.setTiedToPosIndex(this.piece.getAbsoluteIndex());
      if (this.supportType > 0) {
         this.supportIcon.setType(this.supportType, this.supportPosL);
         this.supportIcon.setSpriteSubIndex(ElementKeyMap.getInfo(this.supportType).getBuildIconNum());
      }

      this.effectIcon = new WeaponSlotOverlayElement(this.state);
      this.effectIcon.setScale(0.65F, 0.65F, 0.65F);
      this.effectIcon.setSlotStyle(2);
      this.effectIcon.setTiedToType(this.piece.getType());
      this.effectIcon.setTiedToPosIndex(this.piece.getAbsoluteIndex());
      if (this.effectType > 0) {
         this.effectIcon.setType(this.effectType, this.effectPosL);
         this.effectIcon.setSpriteSubIndex(ElementKeyMap.getInfo(this.effectType).getBuildIconNum());
      }

      if (this.info.isSupportCombinationControllerB()) {
         this.supportIcon.setUnloadedHighlightSupport(true);
      }

      if (this.info.isMainCombinationControllerB()) {
         this.supportIcon.setUnloadedHighlightSupport(true);
         this.effectIcon.setUnloadedHighlightEffect(true);
      }

      GUITextOverlay var2 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium19(), this.state);
      int var3;
      if ((var3 = this.shipConfiguration.getByPos(this.piece.getAbsolutePos(absPosTmp))) >= 0) {
         this.key = var3;
         var2.setTextSimple((var3 + 1) % 10 + " [" + (var3 / 10 + 1) + "]");
      } else {
         this.key = -1;
         var2.setTextSimple("");
      }

      GUITextOverlay var7;
      (var7 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium14(), this.state)).setTextSimple(String.valueOf(this.weaponBlockSize));
      GUITextOverlay var4;
      (var4 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium14(), this.state)).setTextSimple(this.supportRatio > 0.0F ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONROWELEMENT_1, StringTools.formatPointZero(this.supportRatio * 100.0F), this.supportSize) : "");
      GUITextOverlay var5;
      (var5 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium14(), this.state)).setTextSimple(this.effectRatio > 0.0F ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONROWELEMENT_0, StringTools.formatPointZero(this.effectRatio * 100.0F), this.effectSize) : "");
      GUITextOverlay var6;
      (var6 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium18(), this.state)).setTextSimple(this.weaponBlockSize + this.supportSize + this.effectSize);
      this.keyColumn = new GUIAncor(this.state, 32.0F, 52.0F);
      var2.setPos(2.0F, 8.0F, 0.0F);
      this.keyColumn.attach(var2);
      this.mainSizeColumn = new GUIAncor(this.state, 32.0F, 52.0F);
      var7.setPos(4.0F, 8.0F, 0.0F);
      this.mainSizeColumn.attach(var7);
      this.weaponColumn = new GUIAncor(this.state, 32.0F, 52.0F);
      this.weaponColumn.attach(this.weaponIcon);
      var1.setPos(16.0F, 25.0F, 0.0F);
      this.weaponColumn.attach(var1);
      this.sizeColumn = new GUIAncor(this.state, 32.0F, 52.0F);
      this.sizeColumn.attach(var6);
      this.secondaryColumn = new GUIAncor(this.state, 32.0F, 52.0F);
      this.supportIcon.setPos(4.0F, 2.0F, 0.0F);
      this.secondaryColumn.attach(this.supportIcon);
      var4.setPos(4.0F, 28.0F, 0.0F);
      this.secondaryColumn.attach(var4);
      this.tertiaryColumn = new GUIAncor(this.state, 32.0F, 52.0F);
      this.effectIcon.setPos(4.0F, 2.0F, 0.0F);
      this.tertiaryColumn.attach(this.effectIcon);
      var5.setPos(4.0F, 28.0F, 0.0F);
      this.tertiaryColumn.attach(var5);
   }

   public boolean isBlocked() {
      return this.weaponIcon.isInside() || this.supportIcon.isInside() || this.effectIcon.isInside();
   }

   public WeaponDescriptionPanel getDescriptionPanel(InputState var1, GUIElement var2) {
      WeaponDescriptionPanel var3 = new WeaponDescriptionPanel(var1, FontLibrary.getBlenderProMedium14(), var2);
      ManagerModuleCollection var4;
      ControlBlockElementCollectionManager var5;
      if (this.piece != null && this.piece.getType() != 0 && (var4 = ((ManagedSegmentController)this.segmentController).getManagerContainer().getModulesControllerMap().get(this.piece.getType())) != null && (var5 = (ControlBlockElementCollectionManager)var4.getCollectionManagersMap().get(this.piece.getAbsoluteIndex())) != null) {
         var3.update((ElementCollectionManager)var5);
      }

      return var3;
   }

   private void updateInfo(SegmentPiece var1, List var2) {
      var2.clear();
      if (var1 != null) {
         if (this.piece != null) {
            SegmentController var3;
            if ((var3 = this.segmentController) == null) {
               return;
            }

            System.err.println("[WEAPONROW] UPDATE FOR: " + var1);
            if (var1 != null && var1.getType() != 0) {
               ManagerModuleCollection var5;
               ControlBlockElementCollectionManager var6;
               if ((var5 = ((ManagedSegmentController)var3).getManagerContainer().getModulesControllerMap().get(var1.getType())) != null && (var6 = (ControlBlockElementCollectionManager)var5.getCollectionManagersMap().get(var1.getAbsoluteIndex())) != null) {
                  this.update((ElementCollectionManager)var6, var2);
               }

               if (var1.getType() == 1 && var1.getSegmentController().getDockingController().getDockedOn() != null) {
                  this.update("DOCK", var2);
               } else if (var1.getType() == 1) {
                  this.update("CORE", var2);
               } else if (var1.getType() == 670) {
                  this.update("INNER_LOGIC", var2);
               } else if (var1.getType() == 663) {
                  if (var1.getSegmentController().railController.isDocked()) {
                     this.update("UNDOCK", var2);
                  } else {
                     this.update("RAILBEAM", var2);
                  }
               }

               if (ElementKeyMap.getInfo(var1.getType()).isOldDockable()) {
                  Iterator var7 = var1.getSegment().getSegmentController().getDockingController().getDockedOnThis().iterator();

                  while(var7.hasNext()) {
                     ElementDocking var4;
                     if ((var4 = (ElementDocking)var7.next()).to.equals(var1)) {
                        this.update(var4.from.getSegment().getSegmentController(), var2);
                     }
                  }
               }
            }
         }

      }
   }

   public void update(ElementCollectionManager var1, List var2) {
      if (var1.getContainer().getSegmentController() == ((GameClientState)var1.getSegmentController().getState()).getCurrentPlayerObject()) {
         if (var1 instanceof ControlBlockElementCollectionManager) {
            ControlBlockElementCollectionManager var3 = (ControlBlockElementCollectionManager)var1;
            StringBuffer var4;
            (var4 = new StringBuffer()).append("Type: " + var3.getModuleName() + "\nGroups:\t\t" + var3.getElementCollections().size() + "\n");
            ManagerModuleCollection var5;
            float var6;
            ControlBlockElementCollectionManager var7;
            if (var3.getSlaveConnectedElement() != Long.MIN_VALUE && (var5 = ((ManagedSegmentController)var1.getContainer().getSegmentController()).getManagerContainer().getModulesControllerMap().get((short)ElementCollection.getType(var3.getSlaveConnectedElement()))) != null && (var7 = (ControlBlockElementCollectionManager)var5.getCollectionManagersMap().get(ElementCollection.getPosIndexFrom4(var3.getSlaveConnectedElement()))) != null) {
               var6 = CombinationAddOn.getRatio(var3, var7);
               var4.append("support: \n  " + var7.getModuleName() + " (" + var6 * 100.0F + "%)\n");
            }

            if (var3.getEffectConnectedElement() != Long.MIN_VALUE && (var5 = ((ManagedSegmentController)var1.getContainer().getSegmentController()).getManagerContainer().getModulesControllerMap().get((short)ElementCollection.getType(var3.getEffectConnectedElement()))) != null && (var7 = (ControlBlockElementCollectionManager)var5.getCollectionManagersMap().get(ElementCollection.getPosIndexFrom4(var3.getEffectConnectedElement()))) != null) {
               var6 = CombinationAddOn.getRatio(var3, var7);
               var4.append("effect: \n  " + var7.getModuleName() + " (" + var6 * 100.0F + "%)\n");
            }

            var2.add(var4.toString());
         } else {
            System.err.println("EXCEPTION: UNKNOWN MANAGER: " + var1);
         }
      }
   }

   public void update(SegmentController var1, List var2) {
      if (var1 instanceof SegmentController) {
         StringBuffer var3;
         (var3 = new StringBuffer()).append("Undock " + var1.toNiceString() + "\nfrom you by executing this!\n");
         var2.set(0, var3.toString());
      }

   }

   public void update(String var1, List var2) {
      StringBuffer var3;
      if (var1.equals("RAIL")) {
         (var3 = new StringBuffer()).append("Type: \t\tRail docking Beam\n");
         var2.add(var3.toString());
      } else if (var1.equals("CORE")) {
         (var3 = new StringBuffer()).append("Type: \t\tDocking Beam\nLocation:\t\t" + Ship.core + "\n");
         var2.add(var3.toString());
      } else if (var1.equals("DOCK")) {
         (var3 = new StringBuffer()).append("Undock yourself by executing this!\n");
         var2.add(var3.toString());
      } else {
         if (var1.equals("UNDOCK")) {
            (var3 = new StringBuffer()).append("Undock yourself by executing this!\n");
            var2.add(var3.toString());
         }

      }
   }

   public int getTotalSize() {
      return this.weaponBlockSize + this.supportSize + this.effectSize;
   }

   public int compareTo(WeaponRowElementInterface var1) {
      return var1.getTotalSize() - this.getTotalSize();
   }

   public GUIAncor getWeaponColumn() {
      return this.weaponColumn;
   }

   public GUIAncor getMainSizeColumn() {
      return this.mainSizeColumn;
   }

   public GUIAncor getSecondaryColumn() {
      return this.secondaryColumn;
   }

   public GUIAncor getSizeColumn() {
      return this.sizeColumn;
   }

   public GUIAncor getKeyColumn() {
      return this.keyColumn;
   }

   public GUIAncor getTertiaryColumn() {
      return this.tertiaryColumn;
   }

   public List getDescriptionList() {
      return this.descriptionList;
   }

   public int getKey() {
      return this.key;
   }

   public void setKey(int var1) {
      this.key = var1;
   }

   public SegmentPiece getPiece() {
      return this.piece;
   }

   public long getUsableId() {
      return this.piece.getAbsoluteIndex();
   }

   public int getMaxCharges() {
      return 0;
   }

   public int getCurrentCharges() {
      return 0;
   }
}
