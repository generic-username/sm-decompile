package org.schema.game.client.view.gui.weapon;

import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;
import org.newdawn.slick.Color;
import org.newdawn.slick.UnicodeFont;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.controller.manager.ingame.ship.ShipExternalFlightController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.HotbarInterface;
import org.schema.game.client.view.gui.shiphud.newhud.BottomBar;
import org.schema.game.client.view.gui.shiphud.newhud.GUIPosition;
import org.schema.game.client.view.gui.shiphud.newhud.PlayerHealthBar;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.SlotAssignment;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerActivityInterface;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.controller.elements.ManagerReloadInterface;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.BlockEffect;
import org.schema.game.common.data.blockeffects.StatusBlockEffect;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIInnerBackground;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Mouse;
import org.schema.schine.network.client.ClientState;

public class WeaponBottomBar extends BottomBar implements HotbarInterface {
   @ConfigurationElement(
      name = "StartPosIconsX"
   )
   public static int START_ICON_X;
   @ConfigurationElement(
      name = "StartPosIconsY"
   )
   public static int START_ICON_Y;
   @ConfigurationElement(
      name = "IconSpacing"
   )
   public static int ICON_SPACING = 70;
   @ConfigurationElement(
      name = "Color"
   )
   public static Vector4i COLOR;
   @ConfigurationElement(
      name = "Position"
   )
   public static GUIPosition POSITION;
   @ConfigurationElement(
      name = "Offset"
   )
   public static Vector2f OFFSET;
   private final int barIndex;
   Vector3i absPosTmp = new Vector3i();
   int repairIconNum = -1;
   int jammingIconNum = -1;
   int cloakIconNum = -1;
   private WeaponSlotOverlayElement[] icons;
   private GUIOverlay selectIcon;
   private GUIOverlay background;
   private Vector3i tmpPos = new Vector3i();
   private SegmentPiece tmp = new SegmentPiece();
   private GUITextOverlay draggingIconText;
   private WeaponSlotOverlayElement draggingIcon;
   private GUITextOverlay barIndexText;
   private long selectTime;
   private int lastSelected = 0;
   private PlayerHealthBar playerHealth;
   private GUIAncor bgIndexAnchor;
   private GUIInnerBackground bgIndex;
   private GUIOverlay upButton;
   private GUIOverlay downButton;
   private GUIOverlay reload;
   private Vector3i tmp1 = new Vector3i();
   private Vector3i tmp2 = new Vector3i();
   private long lastErrorMsg;
   private static Vector4f fadedTint = new Vector4f(1.0F, 1.0F, 1.0F, 0.3F);

   public WeaponBottomBar(InputState var1, int var2) {
      super(var1);
      this.barIndex = var2;
      this.icons = new WeaponSlotOverlayElement[10];
      float var3 = (float)this.getStartIconX();
      float var4 = (float)this.getStartIconY();
      float var5 = (float)this.getIconSpacing();
      this.barIndexText = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium20(), var1);
      this.barIndexText.setTextSimple(String.valueOf(var2 + 1));
      this.barIndexText.setPos(4.0F, 28.0F, 0.0F);
      this.draggingIconText = this.initializeTextIcon(FontLibrary.getBoldArial18(), var1);
      this.draggingIcon = new WeaponSlotOverlayElement(var1);
      this.draggingIcon.attach(this.draggingIconText);
      this.draggingIcon.setScale(1.0F, 1.0F, 1.0F);
      this.bgIndexAnchor = new GUIAncor(var1, (float)(18 + (int)Math.log10((double)(var2 + 1)) * 11), 77.0F);
      this.bgIndex = new GUIInnerBackground(var1, this.bgIndexAnchor, 0);
      this.bgIndexAnchor.attach(this.bgIndex);
      this.bgIndex.attach(this.barIndexText);
      String var6 = "gui/ingame/";
      if (var1 != null) {
         var6 = var1.getGUIPath();
      }

      this.upButton = new GUIOverlay(Controller.getResLoader().getSprite(var6 + "UI 16px-8x8-gui-"), var1);
      this.downButton = new GUIOverlay(Controller.getResLoader().getSprite(var6 + "UI 16px-8x8-gui-"), var1);
      this.upButton.setSpriteSubIndex(4);
      this.downButton.setSpriteSubIndex(5);
      this.bgIndex.attach(this.upButton);
      this.bgIndex.attach(this.downButton);
      this.upButton.setPos(1.0F, 0.0F, 0.0F);
      this.downButton.setPos(1.0F, this.bgIndexAnchor.getHeight() - this.downButton.getHeight(), 0.0F);

      for(int var7 = 0; var7 < this.icons.length; ++var7) {
         this.icons[var7] = new WeaponSlotOverlayElement(var1);
         this.icons[var7].setSlot(var2 * 10 + var7);
         this.icons[var7].getPos().x = var3 + (float)var7 * var5;
         this.icons[var7].getPos().y = var4;
      }

      this.reload = new GUIOverlay(Controller.getResLoader().getSprite("HUD_Hotbar-8x2-gui-"), var1);
      this.background = new GUIOverlay(Controller.getResLoader().getSprite("HUD_Hotbar-gui-"), var1);
      this.selectIcon = new GUIOverlay(Controller.getResLoader().getSprite("HUD_Hotbar-8x2-gui-"), var1);
      this.selectIcon.setSpriteSubIndex(1);
      this.playerHealth = new PlayerHealthBar(var1);
   }

   public int getStartIconX() {
      return START_ICON_X;
   }

   public int getStartIconY() {
      return START_ICON_Y;
   }

   public int getIconSpacing() {
      return ICON_SPACING;
   }

   private GUITextOverlay initializeTextIcon(UnicodeFont var1, InputState var2) {
      GUITextOverlay var3;
      (var3 = new GUITextOverlay(32, 32, var1, var2)).setColor(Color.white);
      var3.setText(new ArrayList());
      var3.getText().add("undefined");
      var3.setPos(2.0F, 2.0F, 0.0F);
      return var3;
   }

   public void activateDragging(boolean var1) {
      for(int var2 = 0; var2 < 10; ++var2) {
         this.icons[var2].setMouseUpdateEnabled(var1);
      }

   }

   public void drawDragging(WeaponSlotOverlayElement var1) {
      GUIElement.enableOrthogonal();
      this.draggingIcon.setPos((float)(Mouse.getX() - var1.getDragPosX()), (float)(GLFrame.getHeight() - Mouse.getY() - var1.getDragPosY()), 0.0F);
      this.draggingIcon.setType(var1.getType(), var1.getPosIndex());
      this.draggingIcon.setSpriteSubIndex(ElementKeyMap.getInfo(var1.getType()).getBuildIconNum());
      this.draggingIconText.getText().set(0, ElementKeyMap.toString(var1.getType()));
      this.draggingIcon.draw();
      GUIElement.disableOrthogonal();
   }

   private void checkController(SegmentController var1, long var2, float var4, int var5, float var6, float var7, boolean var8) {
      if (!Ship.core.equals(var2)) {
         short var21 = (short)ElementCollection.getPosX(var2);
         short var9 = (short)ElementCollection.getPosY(var2);
         short var10 = (short)ElementCollection.getPosZ(var2);
         int var11 = var5 % 10;
         SegmentPiece var12 = null;
         PlayerUsableInterface var13 = null;
         ManagerContainer var14 = ((ManagedSegmentController)var1).getManagerContainer();
         if (var2 > -9223372036854774808L) {
            var12 = var1.getSegmentBuffer().getPointUnsave(var2, this.tmp);
         } else {
            if ((var13 = var14.getPlayerUsable(var2)) == null) {
               if (((ClientState)this.getState()).getUpdateTime() - this.lastErrorMsg > 5000L) {
                  System.err.println("[CLIENT] ERROR: MISSING PLAYER USABLE ON " + var14.getSegmentController() + "; POS: " + var2);
                  this.lastErrorMsg = ((ClientState)this.getState()).getUpdateTime();
               }

               return;
            }

            if (!var13.isPlayerUsable()) {
               short var22;
               if (!ElementKeyMap.isValidType(var22 = PlayerUsableInterface.ICONS.get(var2))) {
                  System.err.println("NO disabled ICON FOR " + var2 + "; Check PlayerUsableInterface.ICONS map");
                  return;
               }

               var5 = ElementKeyMap.getInfo(var22).getBuildIconNum();
               this.icons[var11].getSprite().setTint(fadedTint);
               this.icons[var11].setType(var22, var2);
               this.icons[var11].getPos().x = var4 + (float)var11 * var6;
               this.icons[var11].getPos().y = var7;
               this.icons[var11].setSpriteSubIndex(var5);
               this.icons[var11].draw();
               this.icons[var11].getSprite().setTint((Vector4f)null);
               return;
            }
         }

         LongOpenHashSet var15 = (LongOpenHashSet)var1.getControlElementMap().getControllingMap().getAll().get(ElementCollection.getIndex(Ship.core));
         if (var12 != null && ElementKeyMap.isValidType(var12.getType()) && var15 != null && ElementKeyMap.getInfo(var12.getType()).needsCoreConnectionToWorkOnHotbar() && !var15.contains(ElementCollection.getIndex4(var21, var9, var10, var12.getType()))) {
            var1.getSlotAssignment().removeBySlotAndSend((byte)var5);
         } else {
            short var16;
            if (var12 != null) {
               var16 = var12.getType();
            } else if (!ElementKeyMap.isValidType(var16 = PlayerUsableInterface.ICONS.get(var2))) {
               System.err.println("NO ICON FOR " + var2 + "; Check PlayerUsableInterface.ICONS map");
            }

            if (ElementKeyMap.isValidType(var16)) {
               var5 = ElementKeyMap.getInfo(var16).getBuildIconNum();
               this.icons[var11].setType(var16, var2);
               this.icons[var11].getPos().x = var4 + (float)var11 * var6;
               this.icons[var11].getPos().y = var7;
               this.icons[var11].setSpriteSubIndex(var5);
               this.icons[var11].draw();
               if (var12 != null && var12.getType() == 978 && ((PowerManagerInterface)var14).getPowerAddOn().isBatteryActive()) {
                  this.reload.setSpriteSubIndex(2);
                  this.reload.getPos().y = var7;
                  this.reload.getPos().x = var4 + (float)var11 * var6;
                  this.reload.draw();
               }

               if (var12 != null && var12.getType() == 15) {
                  if (var14.getSegmentController().isJammingFor((SimpleTransformableSendableObject)null)) {
                     this.reload.setSpriteSubIndex(2);
                     this.reload.getPos().y = var7;
                     this.reload.getPos().x = var4 + (float)var11 * var6;
                     this.reload.draw();
                  } else if (var14 instanceof ShipManagerContainer) {
                     this.tmp1.set((int)(var4 + (float)var11 * var6), (int)var7, 0);
                     this.tmp2.set(64, 64, 0);
                     ((ShipManagerContainer)var14).getJammingElementManager().drawReloads(this.tmp1, this.tmp2, var2);
                  }
               }

               if (var12 != null && var12.getType() == 22) {
                  if (var14.getSegmentController().isCloakedFor((SimpleTransformableSendableObject)null)) {
                     this.reload.setSpriteSubIndex(2);
                     this.reload.getPos().y = var7;
                     this.reload.getPos().x = var4 + (float)var11 * var6;
                     this.reload.draw();
                  } else if (var14 instanceof ShipManagerContainer) {
                     this.tmp1.set((int)(var4 + (float)var11 * var6), (int)var7, 0);
                     this.tmp2.set(64, 64, 0);
                     ((ShipManagerContainer)var14).getCloakElementManager().drawReloads(this.tmp1, this.tmp2, var2);
                  }
               }

               ManagerModuleCollection var18;
               if ((var18 = var14.getModulesControllerMap().get(var16)) != null && !var18.getCollectionManagers().isEmpty()) {
                  if (var18.getElementManager() instanceof ManagerReloadInterface) {
                     this.tmp1.set((int)(var4 + (float)var11 * var6), (int)var7, 0);
                     this.tmp2.set(64, 64, 0);
                     ((ManagerReloadInterface)var18.getElementManager()).drawReloads(this.tmp1, this.tmp2, var2);
                  }

                  if (var18.getElementManager() instanceof ManagerActivityInterface && ((ManagerActivityInterface)var18.getElementManager()).isActive()) {
                     this.reload.setSpriteSubIndex(2);
                     this.reload.getPos().y = var7;
                     this.reload.getPos().x = var4 + (float)var11 * var6;
                     this.reload.draw();
                  }

                  ControlBlockElementCollectionManager var19;
                  if ((var19 = (ControlBlockElementCollectionManager)var18.getElementManager().getCollectionManagersMap().get(var2)) != null && var19 instanceof ManagerActivityInterface && ((ManagerActivityInterface)var19).isActive()) {
                     this.reload.setSpriteSubIndex(2);
                     this.reload.getPos().y = var7;
                     this.reload.getPos().x = var4 + (float)var11 * var6;
                     this.reload.draw();
                  }

                  if (var19 != null && var19.getSlaveConnectedElement() != Long.MIN_VALUE) {
                     this.icons[var11].setSlaveType((short)ElementCollection.getType(var19.getSlaveConnectedElement()));
                  } else {
                     this.icons[var11].setSlaveType((short)0);
                  }

                  if (var19 != null && var19.getEffectConnectedElement() != Long.MIN_VALUE) {
                     this.icons[var11].setEffectType((short)ElementCollection.getType(var19.getEffectConnectedElement()));
                  } else {
                     this.icons[var11].setEffectType((short)0);
                  }
               } else {
                  this.icons[var11].setEffectType((short)0);
                  this.icons[var11].setSlaveType((short)0);
               }

               if (var13 != null) {
                  ManagerReloadInterface var20;
                  if ((var20 = var13.getReloadInterface()) != null) {
                     var20.drawReloads(new Vector3i(var4 + (float)var11 * var6, var7, 0.0F), new Vector3i(64.0F, 64.0F, 0.0F), var2);
                  }

                  ManagerActivityInterface var17;
                  if ((var17 = var13.getActivityInterface()) != null && var17.isActive()) {
                     this.reload.setSpriteSubIndex(2);
                     this.reload.getPos().y = var7;
                     this.reload.getPos().x = var4 + (float)var11 * var6;
                     this.reload.draw();
                  }
               }

               if (var12 != null && var16 == 670 && var12.isActive()) {
                  this.reload.setSpriteSubIndex(2);
                  this.reload.getPos().y = var7;
                  this.reload.getPos().x = var4 + (float)var11 * var6;
                  this.reload.draw();
               }

            } else {
               if (this.getState().getController().getInputController().getDragging() != null && this.getState().getController().getInputController().getDragging() instanceof WeaponSlotOverlayElement && ((WeaponSlotOverlayElement)this.getState().getController().getInputController().getDragging()).getSlotStyle() == 0) {
                  this.icons[var11].drawHighlight();
               }

               GlUtil.glPushMatrix();
               this.icons[var11].setInside(false);
               this.icons[var11].transform();
               this.icons[var11].setType((short)0, Long.MIN_VALUE);
               this.icons[var11].setEffectType((short)0);
               this.icons[var11].setSlaveType((short)0);
               this.icons[var11].checkMouseInside();
               GlUtil.glPopMatrix();
            }
         }
      }
   }

   public void cleanUp() {
   }

   public void draw() {
      ShipExternalFlightController var1 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController();
      this.getState();
      if (var1 != null && var1.getShip() != null) {
         if (var1.getEntered() == null) {
            System.err.println("[WEAPON-SIDE-BAR] NO CONTROLLER DRAW. nothing entered");
         } else {
            GlUtil.glPushMatrix();
            Ship var2 = var1.getShip();
            this.transform();
            float var3 = (float)this.getStartIconX();
            float var4 = (float)this.getStartIconY();
            float var5 = (float)this.getIconSpacing();
            this.background.draw();
            if (var1.getEntered(this.tmpPos).equals(Ship.core)) {
               try {
                  this.drawFromCore(var2, var3, var5, var4);
                  this.drawFixedTooltipsFromCore(var2, var3, var5, var4);
               } catch (IOException var7) {
                  var7.printStackTrace();
                  throw new RuntimeException(var7);
               }
            } else {
               try {
                  this.drawFromEntered(var2, var3, var5, var1.getEntered().getAbsoluteIndex(), var4);
               } catch (IOException var6) {
                  var6.printStackTrace();
                  throw new RuntimeException(var6);
               }
            }

            this.drawEffects(var1.getShip());
            this.playerHealth.setPos(PlayerHealthBar.OFFSET.x, PlayerHealthBar.OFFSET.y, 0.0F);
            this.playerHealth.draw();
            this.icons[0].getSprite().setSelectedMultiSprite(0);
            this.bgIndexAnchor.getPos().x = this.background.getWidth() - 157.0F;
            this.bgIndexAnchor.getPos().y = 50.0F;
            this.bgIndexAnchor.draw();
            GlUtil.glPopMatrix();
         }
      } else {
         System.err.println("[WEAPON-SIDE-BAR] NO CONTROLLER DRAW");
      }
   }

   public void onInit() {
      this.background.onInit();

      for(int var1 = 0; var1 < this.icons.length; ++var1) {
         this.icons[var1].onInit();
      }

      this.upButton.setMouseUpdateEnabled(true);
      this.downButton.setMouseUpdateEnabled(true);
      this.upButton.setCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ((GameClientState)WeaponBottomBar.this.getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().modSelectedWeaponBottomBar(1);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.downButton.setCallback(new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ((GameClientState)WeaponBottomBar.this.getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().modSelectedWeaponBottomBar(-1);
            }

         }
      });
      this.upButton.onInit();
      this.downButton.onInit();
      this.playerHealth.onInit();
      this.barIndexText.onInit();
      this.selectIcon.onInit();
   }

   public float getHeight() {
      return this.background.getHeight();
   }

   public float getWidth() {
      return this.background.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void drawEffects(SendableSegmentController var1) {
      GameClientState var2 = (GameClientState)this.getState();
      Iterator var3 = var1.getBlockEffectManager().getActiveEffectsSet().iterator();

      while(true) {
         while(var3.hasNext()) {
            BlockEffect var4;
            if ((var4 = (BlockEffect)var3.next()).getBlockAndTypeId4() != Long.MIN_VALUE) {
               if (var4 instanceof StatusBlockEffect) {
                  var2.getController().popupGameTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONBOTTOMBAR_0, var4.getType().getName(), StringTools.formatPointZero(((StatusBlockEffect)var4).getRatio(var1.getBlockEffectManager().status) * 100.0F)), 0.0F);
               } else {
                  var2.getController().popupGameTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONBOTTOMBAR_1, var4.getType().getName()), 0.0F);
               }

               for(int var5 = 0; var5 < 10; ++var5) {
                  if (this.icons[var5].getPosIndex() == ElementCollection.getPosIndexFrom4(var4.getBlockAndTypeId4())) {
                     this.reload.setSpriteSubIndex(2);
                     this.reload.setPos(this.icons[var5].getPos());
                     this.reload.draw();
                  }
               }
            } else if (var4.getMessage() != null && !var4.isMessageDisplayed() && var1.isClientOwnObject() && ((GameClientState)var1.getState()).getWorldDrawer() != null) {
               ((GameClientState)var1.getState()).getWorldDrawer().getGuiDrawer().notifyEffectHit(var1, var4.getMessage());
               var4.setMessageDisplayed(System.currentTimeMillis());
            }
         }

         return;
      }
   }

   private void drawFixedTooltipsFromCore(Ship var1, float var2, float var3, float var4) throws IOException {
      PlayerState var7 = ((GameClientState)this.getState()).getPlayer();

      for(int var8 = 0; var8 < 10; ++var8) {
         if (var8 == var7.getCurrentShipControllerSlot() % 10) {
            long var5 = System.currentTimeMillis() - this.selectTime;
            GlUtil.glPushMatrix();
            this.icons[var8].drawToolTipFixed(var5, 1200L);
            GlUtil.glPopMatrix();
         }
      }

   }

   private void drawFromCore(Ship var1, float var2, float var3, float var4) throws IOException {
      PlayerState var5 = ((GameClientState)this.getState()).getPlayer();
      SlotAssignment var6 = var1.getSlotAssignment();

      for(int var7 = this.barIndex * 10; var7 < this.barIndex * 10 + 10; ++var7) {
         int var8 = var7 % 10;
         long var9 = var6.getAsIndex(var7);
         GlUtil.glPushMatrix();
         if (var7 == var5.getCurrentShipControllerSlot()) {
            GlUtil.glPushMatrix();
            if (this.lastSelected != var7) {
               this.selectTime = System.currentTimeMillis();
               this.lastSelected = var7;
            }

            this.selectIcon.getPos().set(this.icons[var8].getPos());
            this.selectIcon.getScale().set(this.icons[var8].getScale());
            this.selectIcon.draw();
            GlUtil.glPopMatrix();
         }

         GlUtil.glPopMatrix();
         if (var9 == Long.MIN_VALUE) {
            if (this.getState().getController().getInputController().getDragging() != null && this.getState().getController().getInputController().getDragging() instanceof WeaponSlotOverlayElement && ((WeaponSlotOverlayElement)this.getState().getController().getInputController().getDragging()).getSlotStyle() == 0) {
               GlUtil.glPushMatrix();
               this.icons[var8].drawHighlight();
               GlUtil.glPopMatrix();
            }

            GlUtil.glPushMatrix();
            this.icons[var8].setInside(false);
            this.icons[var8].transform();
            this.icons[var8].setType((short)0, Long.MIN_VALUE);
            this.icons[var8].setEffectType((short)0);
            this.icons[var8].setSlaveType((short)0);
            this.icons[var8].checkMouseInside();
            GlUtil.glPopMatrix();
         } else {
            GlUtil.glPushMatrix();
            this.checkController(var1, var9, var2, var7, var3, var4, true);
            GlUtil.glPopMatrix();
         }
      }

   }

   private void drawFromEntered(Ship var1, float var2, float var3, long var4, float var6) throws IOException {
      this.checkController(var1, var4, var2, 0, var3, var6, false);
   }

   public void drawToolTip() {
      GlUtil.glPushMatrix();

      for(int var1 = 0; var1 < this.icons.length; ++var1) {
         this.icons[var1].drawToolTip();
      }

      GlUtil.glPopMatrix();
   }

   public void update(Timer var1) {
      super.update(var1);
   }

   public Vector4i getConfigColor() {
      return COLOR;
   }

   public GUIPosition getConfigPosition() {
      return POSITION;
   }

   public Vector2f getConfigOffset() {
      return OFFSET;
   }

   protected String getTag() {
      return "BottomBarWeapon";
   }
}
