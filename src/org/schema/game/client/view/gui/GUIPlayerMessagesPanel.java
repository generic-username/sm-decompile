package org.schema.game.client.view.gui;

import java.text.DateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import javax.vecmath.Vector4f;
import org.newdawn.slick.UnicodeFont;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerMessagesPlayerInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.game.common.data.player.playermessage.PlayerMessage;
import org.schema.game.common.data.player.playermessage.PlayerMessageController;
import org.schema.game.network.objects.remote.RemotePlayerMessage;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIEnterableList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUIPlayerMessagesPanel extends GUIAncor implements DrawerObserver, GUICallback {
   private static DateFormat dateFormat;
   private GUIOverlay background = new GUIOverlay(Controller.getResLoader().getSprite("panel-std-gui-"), this.getState());
   private GUIScrollablePanel scrollPanel;
   private GUIElementList panelList;
   private PlayerMessageController messageController = ((GameClientState)this.getState()).getController().getClientChannel().getPlayerMessageController();
   private boolean flagChanged = true;

   public GUIPlayerMessagesPanel(InputState var1) {
      super(var1);
      this.messageController.addObserver(this);
      this.width = this.background.getWidth();
      this.height = this.background.getHeight();
      this.scrollPanel = new GUIScrollablePanel(512.0F, 366.0F, this.getState());
      this.panelList = new GUIElementList(this.getState());
      this.scrollPanel.setContent(this.panelList);
      GUITextButton var2;
      (var2 = new GUITextButton(var1, 100, 26, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIPLAYERMESSAGESPANEL_1, this)).setUserPointer("new");
      var2.setTextPos(4, 3);
      GUITextButton var3;
      (var3 = new GUITextButton(var1, 100, 26, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIPLAYERMESSAGESPANEL_2, this)).getBackgroundColor().set(0.7F, 0.0F, 0.0F, 1.0F);
      var3.getBackgroundColorMouseOverlay().set(0.4F, 0.0F, 0.0F, 1.0F);
      var3.setUserPointer("delAll");
      var3.setTextPos(17, 3);
      GUITextButton var4;
      (var4 = new GUITextButton(var1, 20, 18, " X", this)).setUserPointer("X");
      GUITextOverlay var5 = new GUITextOverlay(30, 30, var1);
      GUITextOverlay var6 = new GUITextOverlay(30, 30, var1);
      var5.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIPLAYERMESSAGESPANEL_3);
      var5.setPos(260.0F, 32.0F, 0.0F);
      var6.setPos(360.0F, 32.0F, 0.0F);
      var4.setPos(790.0F, 32.0F, 0.0F);
      var2.setPos(460.0F, 32.0F, 0.0F);
      var3.setPos(460.0F + var2.getWidth() + 5.0F, 32.0F, 0.0F);
      this.attach(this.background);
      this.background.attach(var5);
      this.background.attach(var4);
      this.background.attach(var2);
      this.background.attach(var3);
      this.background.attach(this.scrollPanel);
      this.scrollPanel.setPos(260.0F, 64.0F, 0.0F);
      this.orientate(48);
   }

   public void draw() {
      if (this.flagChanged) {
         this.reList();
      }

      super.draw();
   }

   private void reList() {
      this.panelList.clear();
      int var1 = 0;
      Iterator var2 = this.messageController.messagesReceived.iterator();

      while(var2.hasNext()) {
         PlayerMessage var3 = (PlayerMessage)var2.next();
         GUIPlayerMessagesPanel.MessageEnterableList var4;
         (var4 = new GUIPlayerMessagesPanel.MessageEnterableList(this.getState(), var3, var1)).addObserver(new Observer() {
            public void update(Observable var1, Object var2) {
               GUIPlayerMessagesPanel.this.panelList.updateDim();
            }
         });
         ++var1;
         this.panelList.add(new GUIListElement(var4, var4, this.getState()));
      }

      GUITextButton var5 = new GUITextButton(this.getState(), 300, 30, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIPLAYERMESSAGESPANEL_4, 10), new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               System.err.println("[CLIENT][GUI] REQUESTING 10 MORE MESSAGES");
               GUIPlayerMessagesPanel.this.messageController.getChannel().getNetworkObject().playerMessageRequests.add(10);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.panelList.add(new GUIListElement(var5, var5, this.getState()));
      this.flagChanged = false;
   }

   public void update(DrawerObservable var1, Object var2, Object var3) {
      this.flagChanged = true;
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      assert var1 != null && var1.getUserPointer() != null : var1;

      this.getCallback().callback(var1, var2);
   }

   public boolean isOccluded() {
      return false;
   }

   static {
      dateFormat = StringTools.getSimpleDateFormat(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIPLAYERMESSAGESPANEL_0, "MM/dd/yyyy HH:mm");
   }

   public class MessageEnterableList extends GUIEnterableList {
      private final PlayerMessage f;
      private GUIColoredRectangle p;

      public MessageEnterableList(InputState var2, PlayerMessage var3, int var4) {
         super(var2);
         this.f = var3;
         this.p = new GUIColoredRectangle(this.getState(), 510.0F, 250.0F, var4 % 2 == 0 ? new Vector4f(0.0F, 0.0F, 0.0F, 0.0F) : new Vector4f(0.2F, 0.2F, 0.2F, 0.5F));
         GUIPlayerMessagesPanel.MessageEnterableList.PlayerMessageExtendedPanel var5;
         (var5 = new GUIPlayerMessagesPanel.MessageEnterableList.PlayerMessageExtendedPanel(this.getState(), var3)).onInit();
         this.p.attach(var5);
         this.list.add(new GUIListElement(this.p, this.p, this.getState()));
         this.collapsedButton = new GUIPlayerMessagesPanel.MessageEnterableList.PlayerMessageEntryPanel(this.getState(), var3, "+", var4);
         this.backButton = new GUIPlayerMessagesPanel.MessageEnterableList.PlayerMessageEntryPanel(this.getState(), var3, "-", var4);
         this.collapsedButton.onInit();
         this.backButton.onInit();
         this.onInit();
      }

      protected boolean canClick() {
         return ((GameClientState)this.getState()).getPlayerInputs().size() == 1 && ((GameClientState)this.getState()).getPlayerInputs().get(0) instanceof PlayerMessagesPlayerInput;
      }

      public void switchCollapsed(boolean var1) {
         super.switchCollapsed(var1);
         if (this.isExpended() && !this.f.isRead()) {
            this.f.setRead(true);
            System.err.println("EXPANDED AND READ " + this.f.isRead());
            ((GameClientState)this.getState()).getController().getClientChannel().getNetworkObject().playerMessageBuffer.add(new RemotePlayerMessage(this.f, false));
         }

      }

      public void updateIndex(int var1) {
         this.p.getColor().set(var1 % 2 == 0 ? new Vector4f(0.0F, 0.0F, 0.0F, 0.0F) : new Vector4f(0.2F, 0.2F, 0.2F, 0.5F));
      }

      class PlayerMessageEntryPanel extends GUIColoredRectangle {
         boolean read;
         private PlayerMessage f;
         private String string;

         public PlayerMessageEntryPanel(InputState var2, PlayerMessage var3, String var4, int var5) {
            super(var2, 500.0F, 35.0F, var5 % 2 == 0 ? new Vector4f(0.0F, 0.0F, 0.0F, 0.0F) : new Vector4f(0.2F, 0.2F, 0.2F, 0.5F));
            this.f = var3;
            this.read = var3.isRead();
            this.string = var4;
            this.attach(var2, !var3.isRead(), var4, var3);
         }

         public void draw() {
            if (this.f.isRead() != this.read) {
               this.detachAll();
               this.read = this.f.isRead();
               this.attach(this.getState(), !this.f.isRead(), this.string, this.f);
            }

            super.draw();
         }

         public void attach(InputState var1, boolean var2, String var3, PlayerMessage var4) {
            UnicodeFont var5 = FontLibrary.getRegularArial12WhiteWithoutOutline();
            if (var2) {
               var5 = FontLibrary.getBoldArial12Yellow();
               System.err.println("DRAWING BOLD");
            }

            GUITextOverlay var6;
            (var6 = new GUITextOverlay(300, 20, var5, var1)).setTextSimple(var3);
            this.attach(var6);
            var6 = new GUITextOverlay(300, 20, var5, var1);
            Date var7 = new Date(var4.getSent());
            var6.setTextSimple(GUIPlayerMessagesPanel.dateFormat.format(var7));
            var6.getPos().x = 15.0F;
            this.attach(var6);
            (var6 = new GUITextOverlay(300, 20, var5, var1)).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIPLAYERMESSAGESPANEL_8 + var4.getFrom());
            var6.getPos().x = 120.0F;
            this.attach(var6);
            (var6 = new GUITextOverlay(300, 20, var5, var1)).setTextSimple(var4.getTopic());
            var6.getPos().x = 15.0F;
            var6.getPos().y = 15.0F;
            this.attach(var6);
         }
      }

      public class PlayerMessageExtendedPanel extends GUIAncor {
         public final PlayerMessage f;

         public PlayerMessageExtendedPanel(InputState var2, final PlayerMessage var3) {
            super(var2, 400.0F, 300.0F);
            this.f = var3;
            GUIAncor var4 = new GUIAncor(var2, 250.0F, 20.0F);
            GUITextButton var5;
            (var5 = new GUITextButton(var2, 120, 18, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIPLAYERMESSAGESPANEL_5, GUIPlayerMessagesPanel.this)).setUserPointer(var3);
            GUITextButton var6;
            (var6 = new GUITextButton(var2, 120, 18, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIPLAYERMESSAGESPANEL_6, new GUICallback() {
               public boolean isOccluded() {
                  return false;
               }

               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse()) {
                     (new PlayerGameOkCancelInput("CONFIRM", (GameClientState)PlayerMessageExtendedPanel.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIPLAYERMESSAGESPANEL_9, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIPLAYERMESSAGESPANEL_7 + "\n" + var3.getTopic()) {
                        public boolean isOccluded() {
                           return false;
                        }

                        public void onDeactivate() {
                        }

                        public void pressedOK() {
                           System.err.println("[CLIENT] sending message delete");
                           GUIPlayerMessagesPanel.this.messageController.clientDelete(var3);
                           this.deactivate();
                        }
                     }).activate();
                  }

               }
            })).getBackgroundColor().set(1.0F, 0.0F, 0.0F, 1.0F);
            var6.getPos().x = 400.0F;
            var4.attach(var5);
            var4.attach(var6);
            this.attach(var4);
            GUIAncor var7;
            (var7 = new GUIAncor(var2, this.getWidth(), this.getHeight() - 30.0F)).getPos().y = 20.0F;
            GUITextOverlay var8;
            (var8 = new GUITextOverlay(300, 20, var2)).setTextSimple(StringTools.wrap(var3.getMessage(), 90));
            var7.attach(var8);
            this.attach(var7);
         }
      }
   }
}
