package org.schema.game.client.view.gui;

import com.bulletphysics.linearmath.Transform;
import java.nio.FloatBuffer;
import java.util.Iterator;
import javax.vecmath.Matrix3f;
import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.tools.SingleBlockDrawer;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.util.timer.LinearTimerUtil;
import org.schema.schine.input.InputState;

public class GUI3DBlockElement extends GUIElement {
   public static LinearTimerUtil linearTimer = new LinearTimerUtil();
   private static Transform mView = new Transform();
   private static FloatBuffer fb = BufferUtils.createFloatBuffer(16);
   private static float[] ff = new float[16];
   private static Transform mView2 = new Transform();
   private static FloatBuffer fb2 = BufferUtils.createFloatBuffer(16);
   private static float[] ff2 = new float[16];
   private final SingleBlockDrawer drawer = new SingleBlockDrawer();
   private Transform orientation = new Transform();
   private Transform orientationTmp = new Transform();
   private Matrix3f rot = new Matrix3f();
   private short blockType = 1;
   private int shapeOrienation = 0;
   private int sidedOrienation = 0;
   private boolean init;

   public GUI3DBlockElement(InputState var1) {
      super(var1);
      this.orientation.setIdentity();
      this.orientationTmp.setIdentity();
   }

   public static void setMatrix() {
      Matrix4f var0 = Controller.modelviewMatrix;
      fb.rewind();
      var0.store(fb);
      fb.rewind();
      fb.get(ff);
      mView.setFromOpenGLMatrix(ff);
      mView.origin.set(0.0F, 0.0F, 0.0F);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.isInvisible()) {
         if (!this.init) {
            this.onInit();
         }

         GlUtil.glPushMatrix();
         this.setInside(false);
         this.transform();
         if (this.isMouseUpdateEnabled()) {
            this.checkMouseInside();
         }

         this.drawBlock(this.blockType);
         Iterator var1 = this.getChilds().iterator();

         while(var1.hasNext()) {
            ((AbstractSceneNode)var1.next()).draw();
         }

         GlUtil.glPopMatrix();
      }
   }

   public void onInit() {
      this.init = true;
   }

   public void drawBlock(short var1) {
      if (var1 != 0) {
         GlUtil.glDisable(2929);
         GlUtil.glEnable(3553);
         GlUtil.glEnable(2896);
         GlUtil.glEnable(3042);
         GlUtil.glDepthMask(false);
         Matrix4f var2 = Controller.modelviewMatrix;
         fb2.rewind();
         var2.store(fb2);
         fb2.rewind();
         fb2.get(ff2);
         mView2.setFromOpenGLMatrix(ff2);
         GUIElement.enableOrthogonal3d();
         GlUtil.glBindBuffer(34962, 0);
         GlUtil.glMultMatrix(mView2);
         GlUtil.glPushMatrix();
         GlUtil.translateModelview(0.0F, 0.0F, 100.0F);
         GlUtil.scaleModelview(32.0F, -32.0F, 32.0F);
         this.rot.set(this.orientation.basis);
         mView.basis.mul(this.rot);
         GlUtil.glMultMatrix(mView);
         Transform var3;
         (var3 = new Transform()).setIdentity();
         if (((GameClientState)this.getState()).getCurrentPlayerObject() != null) {
            var3.basis.set(((GameClientState)this.getState()).getCurrentPlayerObject().getWorldTransform().basis);
         }

         GlUtil.glMultMatrix(var3);
         this.drawer.useSpriteIcons = false;
         this.drawer.activateBlinkingOrientation(ElementKeyMap.getInfo(var1).isOrientatable());
         this.drawer.setShapeOrientation24((byte)this.shapeOrienation);
         this.drawer.setSidedOrientation((byte)this.sidedOrienation);
         if (EngineSettings.G_DRAW_ADV_BUILDMODE_BLOCK_PREVIEW.isOn()) {
            this.drawer.drawType(var1);
         }

         GlUtil.glPopMatrix();
         GUIElement.disableOrthogonal();
         GlUtil.glEnable(2896);
         GlUtil.glDisable(2977);
         GlUtil.glEnable(2929);
         GlUtil.glDepthMask(true);
         this.drawer.useSpriteIcons = true;
      }
   }

   public short getBlockType() {
      return this.blockType;
   }

   public void setBlockType(short var1) {
      this.blockType = var1;
   }

   public float getHeight() {
      return 64.0F;
   }

   public float getWidth() {
      return 64.0F;
   }

   public void setShapeOrientation(int var1) {
      this.shapeOrienation = var1;
   }

   public void setSidedOrientation(int var1) {
      this.sidedOrienation = var1;
   }
}
