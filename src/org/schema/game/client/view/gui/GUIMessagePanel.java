package org.schema.game.client.view.gui;

import java.util.ArrayList;
import javax.vecmath.Vector4f;
import org.newdawn.slick.Color;
import org.schema.game.client.controller.GUIFadingElement;
import org.schema.game.client.controller.GUIMessageDialog;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.DraggableAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUIMessagePanel extends GUIFadingElement {
   private static DraggableAncor draggableAncor;
   private GUIOverlay background;
   private GUIButton ok;
   private GUIButton cancel;
   private GUIScrollablePanel scrollPanel;
   private boolean init;
   private GUIMessageDialog dialog;
   private String message;
   private float fade = 0.0F;
   private GUITextOverlay text;
   private GUITextOverlay title;
   private GUITextOverlay titleDrag;
   private Vector4f blend;
   private String titleString;
   private boolean withCancel;

   public GUIMessagePanel(InputState var1, GUIMessageDialog var2, String var3, String var4, boolean var5) {
      super(var1);
      this.dialog = var2;
      this.titleString = var4;
      this.withCancel = var5;
      this.background = new GUIOverlay(Controller.getResLoader().getSprite("info-panel-gui-"), var1);
      this.scrollPanel = new GUIScrollablePanel(363.0F, 110.0F, var1);
      this.message = var3;
      this.blend = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F - this.fade);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.blend.set(1.0F, 1.0F, 1.0F, 1.0F - this.fade);
      this.ok.getSprite().setTint(this.blend);
      this.background.getSprite().setTint(this.blend);
      this.text.setColor(new Color(this.blend.x, this.blend.y, this.blend.z, this.blend.w));
      this.background.draw();
      this.ok.getSprite().setTint((Vector4f)null);
      this.background.getSprite().setTint((Vector4f)null);
      this.text.setColor(new Color(1, 1, 1, 1));
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.text = new GUITextOverlay(220, 100, FontLibrary.getRegularArial13White(), this.getState());
      this.text.setText(new ArrayList());
      this.text.getText().add(this.message);
      this.title = new GUITextOverlay(200, 30, FontLibrary.getBoldArial18(), this.getState());
      this.title.setText(new ArrayList());
      this.title.getText().add(this.titleString);
      this.titleDrag = new GUITextOverlay(200, 30, FontLibrary.getRegularArial11White(), this.getState());
      this.titleDrag.setText(new ArrayList());
      this.titleDrag.getText().add("(click to drag)");
      this.ok = new GUIButton(Controller.getResLoader().getSprite("buttons-8x8-gui-"), this.getState(), GameResourceLoader.StandardButtons.OK_BUTTON, "OK", this.dialog);
      this.cancel = new GUIButton(Controller.getResLoader().getSprite("buttons-8x8-gui-"), this.getState(), GameResourceLoader.StandardButtons.CANCEL_BUTTON, "CANCEL", this.dialog);
      if (draggableAncor == null) {
         draggableAncor = new DraggableAncor(this.getState(), 380.0F, 40.0F, this.background);
         this.background.orientate(48);
      } else {
         this.background.getPos().set(draggableAncor.getAffected().getPos());
         draggableAncor = new DraggableAncor(this.getState(), 380.0F, 40.0F, this.background);
      }

      this.scrollPanel.setContent(this.text);
      this.background.attach(this.title);
      this.background.attach(this.titleDrag);
      this.background.attach(this.scrollPanel);
      this.background.attach(this.ok);
      if (this.withCancel) {
         this.background.attach(this.cancel);
      }

      this.background.attach(draggableAncor);
      this.title.setPos(14.0F, 10.0F, 0.0F);
      this.titleDrag.setPos(280.0F, 15.0F, 0.0F);
      this.scrollPanel.setPos(23.0F, 42.0F, 0.0F);
      this.ok.setScale(0.5F, 0.5F, 0.5F);
      this.cancel.setScale(0.5F, 0.5F, 0.5F);
      this.ok.setPos(330.0F, 158.0F, 0.0F);
      this.cancel.setPos(220.0F, 158.0F, 0.0F);
      this.init = true;
   }

   public float getHeight() {
      return this.background.getHeight();
   }

   public float getWidth() {
      return this.background.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void setFade(float var1) {
      this.fade = var1;
   }
}
