package org.schema.game.client.view.gui;

import com.bulletphysics.linearmath.Transform;
import java.nio.FloatBuffer;
import javax.vecmath.Matrix3f;
import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.Ship;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public abstract class OrientationElement implements Drawable {
   protected GameClientState state;
   private Matrix3f rot = new Matrix3f();
   private Transform mView = new Transform();
   private FloatBuffer fb = BufferUtils.createFloatBuffer(16);
   private float[] ff = new float[16];
   private boolean init;
   private Mesh mesh;

   public OrientationElement(GameClientState var1) {
      this.state = var1;
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      try {
         if (this.canDraw()) {
            Matrix4f var1 = Controller.modelviewMatrix;
            this.fb.rewind();
            var1.store(this.fb);
            this.fb.rewind();
            this.fb.get(this.ff);
            this.mView.setFromOpenGLMatrix(this.ff);
            this.mView.origin.set(0.0F, 0.0F, 0.0F);
            GUIElement.enableOrthogonal3d();
            GlUtil.glPushMatrix();
            GlUtil.translateModelview((float)(GLFrame.getWidth() / 2), 105.0F, 0.0F);
            GlUtil.scaleModelview(10.0F, -10.0F, 10.0F);
            this.rot.set(this.getWorldTransform().basis);
            this.mView.basis.mul(this.rot);
            GlUtil.glMultMatrix(this.mView);
            GlUtil.glDisable(2896);
            GlUtil.glDisable(2929);
            GlUtil.glEnable(2977);
            GlUtil.glDisable(2884);
            GlUtil.glEnable(2929);
            this.mesh.getMaterial().attach(0);
            this.mesh.drawVBO();
            this.mesh.getMaterial().detach();
            GlUtil.glPopMatrix();
            GUIElement.disableOrthogonal();
            GlUtil.glEnable(2896);
            GlUtil.glDisable(2977);
            GlUtil.glEnable(2929);
            GlUtil.glBindBuffer(34962, 0);
         }

      } catch (NullPointerException var2) {
         System.err.println("EXCPETION HAS BEEN HANDLED");
         var2.printStackTrace();
      }
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.mesh = (Mesh)Controller.getResLoader().getMesh("Arrow").getChilds().get(0);
      this.init = true;
   }

   public abstract Transform getWorldTransform();

   public abstract boolean canDraw();

   public Ship getShip() {
      return this.state.getShip();
   }
}
