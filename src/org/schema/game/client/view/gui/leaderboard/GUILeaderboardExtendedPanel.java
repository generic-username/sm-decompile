package org.schema.game.client.view.gui.leaderboard;

import it.unimi.dsi.fastutil.objects.Object2IntAVLTreeMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map.Entry;
import org.schema.game.common.data.gamemode.battle.KillerEntity;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUILeaderboardExtendedPanel extends GUIAncor {
   public GUILeaderboardExtendedPanel(InputState var1, Entry var2) {
      super(var1, 420.0F, 300.0F);
      GUITextOverlay var3 = new GUITextOverlay(400, 300, var1);
      GUITextOverlay var8 = new GUITextOverlay(400, 300, var1);
      Object2IntAVLTreeMap var4 = new Object2IntAVLTreeMap();
      Object2IntAVLTreeMap var5 = new Object2IntAVLTreeMap();
      Iterator var9 = ((ObjectArrayList)var2.getValue()).iterator();

      while(var9.hasNext()) {
         KillerEntity var6 = (KillerEntity)var9.next();
         var5.put(var6.deadPlayerName, var5.getInt(var6.deadPlayerName) + 1);
         var4.put(var6.shipName, var4.getInt(var6.shipName) + 1);
      }

      Comparator var10 = new Comparator() {
         public int compare(Entry var1, Entry var2) {
            return ((Integer)var2.getValue()).compareTo((Integer)var1.getValue());
         }
      };
      ObjectArrayList var14;
      Collections.sort(var14 = new ObjectArrayList(var4.entrySet()), var10);
      ObjectArrayList var12;
      Collections.sort(var12 = new ObjectArrayList(var5.entrySet()), var10);
      String var11 = "";
      String var13 = "";
      int var7;
      if (var14.size() > 0) {
         var11 = var11 + "Top 3 kills by ships used\n";

         for(var7 = 0; var7 < Math.min(var14.size(), 3); ++var7) {
            var11 = var11 + " " + (var7 + 1) + ". [" + (Integer)((Entry)var14.get(var7)).getValue() + "] " + (String)((Entry)var14.get(var7)).getKey() + "\n";
         }
      }

      if (var12.size() > 0) {
         var13 = var13 + "Top 3 victims\n";

         for(var7 = 0; var7 < Math.min(var12.size(), 3); ++var7) {
            var13 = var13 + " " + (var7 + 1) + ". [" + (Integer)((Entry)var12.get(var7)).getValue() + "] " + (String)((Entry)var12.get(var7)).getKey() + "\n";
         }
      }

      var3.setTextSimple(var13);
      var8.setTextSimple(var11);
      var8.getPos().x = 220.0F;
      this.attach(var3);
      this.attach(var8);
   }
}
