package org.schema.game.client.view.gui.leaderboard;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIEnterableList;
import org.schema.schine.input.InputState;

public class GUILeaderboardEnterableList extends GUIEnterableList {
   private final GUIColoredRectangle p;

   public GUILeaderboardEnterableList(InputState var1, GUIElementList var2, GUIElement var3, GUIElement var4, GUIColoredRectangle var5) {
      super(var1, var2, var3, var4);
      this.p = var5;
   }

   public void updateIndex(int var1) {
      this.p.getColor().set(var1 % 2 == 0 ? new Vector4f(0.0F, 0.0F, 0.0F, 0.0F) : new Vector4f(0.1F, 0.1F, 0.1F, 0.5F));
      ((GUILoaderboardElement)this.collapsedButton).setIndex(var1);
      ((GUILoaderboardElement)this.backButton).setIndex(var1);
   }
}
