package org.schema.game.client.view.gui.leaderboard;

import java.util.Map.Entry;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.input.InputState;

public class GUILearderboardElementList extends GUIElementList {
   private final Entry entry;

   public GUILearderboardElementList(InputState var1, Entry var2) {
      super(var1);
      this.entry = var2;
   }

   public Entry getEntry() {
      return this.entry;
   }
}
