package org.schema.game.client.view.gui.messagelog.messagelognew;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import javax.vecmath.Vector4f;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.ship.InShipControlManager;
import org.schema.game.client.controller.manager.ingame.ship.WeaponAssignControllerManager;
import org.schema.game.client.data.ClientMessageLog;
import org.schema.game.client.data.ClientMessageLogEntry;
import org.schema.game.client.data.ClientMessageLogType;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.CreateGUIElementInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterDropdown;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableDropDown;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.GuiDateFormats;
import org.schema.schine.input.InputState;

public class MessageLogScrollableListNew extends ScrollableTableList implements Observer {
   private ClientMessageLog messageLog;

   public MessageLogScrollableListNew(InputState var1, GUIElement var2, ClientMessageLog var3) {
      super(var1, 100.0F, 100.0F, var2);
      this.messageLog = var3;
      this.messageLog.addObserver(this);
   }

   public void cleanUp() {
      super.cleanUp();
      this.messageLog.deleteObserver(this);
   }

   public void onInit() {
      super.onInit();
   }

   public void initColumns() {
      new StringComparator();
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MESSAGELOG_MESSAGELOGNEW_MESSAGELOGSCROLLABLELISTNEW_0, 80, new Comparator() {
         public int compare(ClientMessageLogEntry var1, ClientMessageLogEntry var2) {
            return var1.getType().name().compareToIgnoreCase(var2.getType().name());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MESSAGELOG_MESSAGELOGNEW_MESSAGELOGSCROLLABLELISTNEW_1, 140, new Comparator() {
         public int compare(ClientMessageLogEntry var1, ClientMessageLogEntry var2) {
            if (var1.getTimestamp() > var2.getTimestamp()) {
               return 1;
            } else {
               return var1.getTimestamp() < var2.getTimestamp() ? -1 : 0;
            }
         }
      }, true);
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MESSAGELOG_MESSAGELOGNEW_MESSAGELOGSCROLLABLELISTNEW_2, 1.0F, new Comparator() {
         public int compare(ClientMessageLogEntry var1, ClientMessageLogEntry var2) {
            return var1.getFrom().compareToIgnoreCase(var2.getFrom());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MESSAGELOG_MESSAGELOGNEW_MESSAGELOGSCROLLABLELISTNEW_3, 8.0F, new Comparator() {
         public int compare(ClientMessageLogEntry var1, ClientMessageLogEntry var2) {
            return var1.getMessage().compareToIgnoreCase(var2.getMessage());
         }
      });
      this.addDropdownFilter(new GUIListFilterDropdown(ClientMessageLogType.values()) {
         public boolean isOk(ClientMessageLogType var1, ClientMessageLogEntry var2) {
            return var2.getType() == var1;
         }
      }, new CreateGUIElementInterface() {
         public GUIElement create(ClientMessageLogType var1) {
            GUIAncor var2 = new GUIAncor(MessageLogScrollableListNew.this.getState(), 10.0F, 24.0F);
            GUITextOverlayTableDropDown var3;
            (var3 = new GUITextOverlayTableDropDown(10, 10, MessageLogScrollableListNew.this.getState())).setTextSimple(var1.getName());
            var3.setPos(4.0F, 4.0F, 0.0F);
            var2.setUserPointer(var1);
            var2.attach(var3);
            return var2;
         }

         public GUIElement createNeutral() {
            GUIAncor var1 = new GUIAncor(MessageLogScrollableListNew.this.getState(), 10.0F, 24.0F);
            GUITextOverlayTableDropDown var2;
            (var2 = new GUITextOverlayTableDropDown(10, 10, MessageLogScrollableListNew.this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MESSAGELOG_MESSAGELOGNEW_MESSAGELOGSCROLLABLELISTNEW_4);
            var2.setPos(4.0F, 4.0F, 0.0F);
            var1.attach(var2);
            return var1;
         }
      }, ControllerElement.FilterRowStyle.FULL);
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, ClientMessageLogEntry var2) {
            return var2.getFrom().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MESSAGELOG_MESSAGELOGNEW_MESSAGELOGSCROLLABLELISTNEW_5, ControllerElement.FilterRowStyle.LEFT);
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, ClientMessageLogEntry var2) {
            return var2.getMessage().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MESSAGELOG_MESSAGELOGNEW_MESSAGELOGSCROLLABLELISTNEW_6, ControllerElement.FilterRowStyle.RIGHT);
   }

   protected Collection getElementList() {
      return this.messageLog.getLog();
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      this.getState().getGameState().getFactionManager();
      this.getState().getGameState().getCatalogManager();
      this.getState().getPlayer();
      Iterator var10 = var2.iterator();

      while(var10.hasNext()) {
         ClientMessageLogEntry var3 = (ClientMessageLogEntry)var10.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState());
         ScrollableTableList.GUIClippedRow var8;
         (var8 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var6);
         ScrollableTableList.GUIClippedRow var9;
         (var9 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var7);
         new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
         var4.setColor(var3.getType().color);
         var7.setPos(4.0F, 4.0F, 0.0F);
         var6.setPos(4.0F, 4.0F, 0.0F);
         var4.setPos(4.0F, 4.0F, 0.0F);
         var5.setPos(4.0F, 4.0F, 0.0F);
         var7.setTextSimple(var3.getMessage());
         var6.setTextSimple(var3.getFrom());
         var4.setTextSimple(var3.getType().name().toLowerCase(Locale.ENGLISH));
         var5.setTextSimple(GuiDateFormats.messageLogTime.format(var3.getTimestamp()));
         MessageLogScrollableListNew.MessageRow var11;
         (var11 = new MessageLogScrollableListNew.MessageRow(this.getState(), var3, new GUIElement[]{var4, var5, var8, var9})).onInit();
         var1.addWithoutUpdate(var11);
      }

      var1.updateDim();
   }

   public boolean isPlayerAdmin() {
      return this.getState().getPlayer().getNetworkObject().isAdminClient.get();
   }

   public boolean canEdit(CatalogPermission var1) {
      return var1.ownerUID.toLowerCase(Locale.ENGLISH).equals(this.getState().getPlayer().getName().toLowerCase(Locale.ENGLISH)) || this.isPlayerAdmin();
   }

   public WeaponAssignControllerManager getAssignWeaponControllerManager() {
      return this.getPlayerGameControlManager().getWeaponControlManager();
   }

   public InShipControlManager getInShipControlManager() {
      return this.getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   private PlayerGameControlManager getPlayerGameControlManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   class MessageRow extends ScrollableTableList.Row {
      public MessageRow(InputState var2, ClientMessageLogEntry var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
