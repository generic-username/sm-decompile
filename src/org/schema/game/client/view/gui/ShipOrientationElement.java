package org.schema.game.client.view.gui;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.data.GameClientState;

public class ShipOrientationElement extends OrientationElement {
   public ShipOrientationElement(GameClientState var1) {
      super(var1);
   }

   public Transform getWorldTransform() {
      return this.state.getShip().getWorldTransform();
   }

   public boolean canDraw() {
      return this.state.getShip() != null && !this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getMapControlManager().isActive();
   }
}
