package org.schema.game.client.view.gui;

import org.schema.game.client.view.GameResourceLoader;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.input.InputState;

public class GUIButton extends GUIOverlay {
   private final GameResourceLoader.StandardButtons buttonType;

   public GUIButton(Sprite var1, InputState var2, GameResourceLoader.StandardButtons var3, String var4, GUICallback var5) {
      super(var1, var2);
      this.buttonType = var3;
      this.setMouseUpdateEnabled(true);
      this.setUserPointer(var4);
      this.setCallback(var5);
   }

   public void draw() {
      if (!this.isInvisible()) {
         this.setSpriteSubIndex(this.buttonType.getSpriteNum(this.isInside()));
         super.draw();
      }
   }

   public GameResourceLoader.StandardButtons getButtonType() {
      return this.buttonType;
   }
}
