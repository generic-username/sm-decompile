package org.schema.game.client.view.gui.advanced.tools;

import java.util.Collection;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public abstract class DropdownResult extends AdvResult {
   private Object currentValue;

   public Object getCurrentValue() {
      return this.currentValue;
   }

   public void setCurrentValue(Object var1) {
      this.currentValue = var1;
   }

   protected void initDefault() {
      this.change(this.getDefault());
   }

   public void change(Object var1) {
      Object var2 = this.getCurrentValue();
      this.setCurrentValue(var1);
      if (var2 != this.getCurrentValue() && this.callback != null) {
         ((DropdownCallback)this.callback).onChanged(this.getCurrentValue());
      }

   }

   public abstract Object getDefault();

   public void refresh() {
      this.change(this.getDefault());
   }

   public abstract Collection getDropdownElements(GUIElement var1);

   public abstract boolean needsListUpdate();

   public abstract void flagListNeedsUpdate(boolean var1);

   public int getDropdownExpendedHeight() {
      return 160;
   }

   public int getDropdownHeight() {
      return 24;
   }
}
