package org.schema.game.client.view.gui.advanced.tools;

import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.settingsnew.GUICheckBoxTextPairNew;
import org.schema.schine.input.InputState;

public class GUIAdvCheckbox extends GUIAdvTool {
   private final GUICheckBoxTextPairNew chk;

   public GUIAdvCheckbox(InputState var1, GUIElement var2, final CheckboxResult var3) {
      super(var1, var2, var3);
      this.chk = new GUICheckBoxTextPairNew(this.getState(), new Object() {
         public String toString() {
            return ((CheckboxResult)GUIAdvCheckbox.this.getRes()).getName();
         }
      }, FontLibrary.getFont(var3.getFontSize())) {
         public boolean isChecked() {
            return var3.getCurrentValue();
         }

         public void deactivate() {
            var3.change(false);
         }

         public void activate() {
            var3.change(true);
         }
      };
      this.chk.activeInterface = new GUIActiveInterface() {
         public boolean isActive() {
            return GUIAdvCheckbox.this.isActive();
         }
      };
      this.attach(this.chk);
   }

   public int getElementHeight() {
      return (int)this.chk.getHeight();
   }
}
