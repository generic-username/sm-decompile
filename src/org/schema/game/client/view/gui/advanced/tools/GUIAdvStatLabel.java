package org.schema.game.client.view.gui.advanced.tools;

import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalProgressBar;
import org.schema.schine.input.InputState;

public class GUIAdvStatLabel extends GUIAdvTool {
   private final GUITextOverlay l;
   private final GUITextOverlay ls;
   private GUIScrollablePanel scroll;

   public GUIAdvStatLabel(InputState var1, GUIElement var2, StatLabelResult var3) {
      super(var1, var2, var3);
      this.scroll = new GUIScrollablePanel(10.0F, 10.0F, var2, var1);
      this.scroll.setScrollable(0);
      GUIAncor var4 = new GUIAncor(var1, 10.0F, 10.0F);
      this.l = new GUITextOverlay(10, 10, FontLibrary.getFont(((StatLabelResult)this.getRes()).getFontSize()), this.getState()) {
         public void draw() {
            this.setColor(((StatLabelResult)GUIAdvStatLabel.this.getRes()).getFontColor());
            super.draw();
         }
      };
      this.l.setTextSimple(new Object() {
         public String toString() {
            return ((StatLabelResult)GUIAdvStatLabel.this.getRes()).getName();
         }
      });
      this.ls = new GUITextOverlay(10, 10, FontLibrary.getFont(((StatLabelResult)this.getRes()).getFontSize()), this.getState()) {
         public void draw() {
            this.setColor(((StatLabelResult)GUIAdvStatLabel.this.getRes()).getFontColor());
            super.draw();
         }
      };
      this.ls.setTextSimple(new Object() {
         public String toString() {
            return ((StatLabelResult)GUIAdvStatLabel.this.getRes()).getValue();
         }
      });
      var4.attach(this.l);
      var4.attach(this.ls);
      var4.setMouseUpdateEnabled(true);
      this.scroll.setContent(var4);
      this.scroll.onInit();
      this.attach(this.scroll);
   }

   public int calcTextWidth() {
      this.l.updateTextSize();
      return this.l.getMaxLineWidth();
   }

   public int getTextWidth() {
      return this.l.getMaxLineWidth();
   }

   public void draw() {
      this.ls.setPos(this.l.getPos().x + (float)((StatLabelResult)this.getRes()).getStatDistance(), this.l.getPos().y, 0.0F);
      super.draw();
   }

   public int getElementHeight() {
      return this.l.getTextHeight();
   }

   protected int getElementWidth() {
      return Math.max((int)(this.l.getPos().x + (float)((StatLabelResult)this.getRes()).getStatDistance() + (float)this.ls.getMaxLineWidth()), this.l.getMaxLineWidth());
   }

   public void setBackgroundProgressBar(final ProgressBarInterface var1) {
      GUIHorizontalProgressBar var2;
      (var2 = new GUIHorizontalProgressBar(this.getState(), this) {
         public float getValue() {
            return var1.getProgressPercent();
         }

         public void draw() {
            var1.getColor(this.getColor());
            super.draw();
         }
      }).setPos(0.0F, -2.0F, 0.0F);
      var2.onInit();
      this.attach(var2, 0);
   }
}
