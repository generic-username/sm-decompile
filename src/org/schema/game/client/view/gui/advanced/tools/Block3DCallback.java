package org.schema.game.client.view.gui.advanced.tools;

public interface Block3DCallback extends AdvCallback {
   void onTypeChanged(short var1);

   void onOrientationChanged(int var1);
}
