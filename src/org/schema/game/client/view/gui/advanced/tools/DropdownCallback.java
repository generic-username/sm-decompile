package org.schema.game.client.view.gui.advanced.tools;

public interface DropdownCallback extends AdvCallback {
   void onChanged(Object var1);
}
