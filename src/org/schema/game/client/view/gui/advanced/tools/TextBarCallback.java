package org.schema.game.client.view.gui.advanced.tools;

public interface TextBarCallback extends AdvCallback {
   void onValueChanged(String var1);
}
