package org.schema.game.client.view.gui.advanced;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.input.InputState;

public abstract class AdvancedGUIMinimizeCallback {
   private boolean canMinimize;
   public GUITextButton closeLashButton;
   public boolean minimized;
   float minimizeStatus = 0.0F;
   public final List additionalButtons = new ObjectArrayList();

   public abstract String getMinimizedText();

   public abstract String getMaximizedText();

   public AdvancedGUIMinimizeCallback(InputState var1, boolean var2) {
      this.canMinimize = var2;
      this.initialMinimized();
      final Object var4 = new Object() {
         public String toString() {
            return AdvancedGUIMinimizeCallback.this.getMinimizedText();
         }
      };
      final Object var3 = new Object() {
         public String toString() {
            return AdvancedGUIMinimizeCallback.this.getMaximizedText();
         }
      };
      this.closeLashButton = new GUITextButton(var1, 170, 20, this.minimized ? var4 : var3, new GUICallback() {
         public boolean isOccluded() {
            return !AdvancedGUIMinimizeCallback.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               AdvancedGUIMinimizeCallback.this.setMinimized(!AdvancedGUIMinimizeCallback.this.isMinimized());
            }

         }
      }) {
         public void draw() {
            if (AdvancedGUIMinimizeCallback.this.isCloseLash()) {
               if (!AdvancedGUIMinimizeCallback.this.isMinimized() && AdvancedGUIMinimizeCallback.this.minimizeStatus < 0.5F) {
                  if (this.getWidth() != 30.0F) {
                     this.setWidth(30);
                     this.setText(var3);
                  }
               } else if (AdvancedGUIMinimizeCallback.this.isMinimized() && AdvancedGUIMinimizeCallback.this.minimizeStatus > 0.5F && this.getWidth() != 170.0F) {
                  this.setWidth(170);
                  this.setText(var4);
               }

               super.draw();
            }

         }
      };
   }

   public abstract void initialMinimized();

   public abstract void onMinimized(boolean var1);

   public abstract boolean isActive();

   public boolean isInside() {
      return this.isCloseLash() && (this.isAnyButtonInside() || this.minimizeStatus > 1.0E-7F && this.minimizeStatus < 0.999999F);
   }

   private boolean isAnyButtonInside() {
      if (this.closeLashButton.isInside()) {
         return true;
      } else {
         Iterator var1 = this.additionalButtons.iterator();

         do {
            if (!var1.hasNext()) {
               return false;
            }
         } while(!((GUITextButton)var1.next()).isInside());

         return true;
      }
   }

   public boolean isCloseLash() {
      return this.canMinimize;
   }

   public void setButtonPosition(GUIElement var1, AdvancedGUIElement var2) {
      if (this.isCloseLashOnRight()) {
         var1.setPos(var2.getWidth() + (float)this.closeLashButtonOffsetX(), (float)((int)(var2.main.getHeight() / 2.0F - this.closeLashButton.getWidth() / 2.0F)), 0.0F);
      } else {
         var1.setPos(-(this.closeLashButton.getHeight() + (float)this.closeLashButtonOffsetX()), (float)((int)(var2.main.getHeight() / 2.0F - this.closeLashButton.getWidth() / 2.0F)), 0.0F);
      }

      var1.setRot(0.0F, 0.0F, 90.0F);
   }

   public void setButtonPosition(AdvancedGUIElement var1) {
      if (this.isCloseLash()) {
         this.setButtonPosition(this.closeLashButton, var1);
      }

   }

   public float getMinimizeSpeed() {
      return 1.666F;
   }

   protected boolean isCloseLashOnRight() {
      return true;
   }

   protected int closeLashButtonOffsetX() {
      return 20;
   }

   public void onInit() {
      this.closeLashButton.onInit();
   }

   public void setMinimized(boolean var1) {
      this.minimized = var1;
      this.onMinimized(this.minimized);
   }

   public boolean isMinimized() {
      return this.minimized;
   }

   public void setMinimizedInitial(boolean var1) {
      this.minimized = var1;
      this.minimizeStatus = this.minimized ? 1.0F : 0.0F;
      this.onMinimized(this.minimized);
   }

   public void update(Timer var1) {
      if (!this.minimized && this.minimizeStatus > 0.0F) {
         this.minimizeStatus = Math.max(0.0F, this.minimizeStatus - var1.getDelta() * this.getMinimizeSpeed());
      } else {
         if (this.minimized && this.minimizeStatus < 1.0F) {
            this.minimizeStatus = Math.min(1.0F, this.minimizeStatus + var1.getDelta() * this.getMinimizeSpeed());
         }

      }
   }

   public void draw() {
      this.closeLashButton.draw();
      Iterator var1 = this.additionalButtons.iterator();

      while(var1.hasNext()) {
         ((GUITextButton)var1.next()).draw();
      }

   }

   public int getButtonWidth() {
      return (int)this.closeLashButton.getWidth();
   }
}
