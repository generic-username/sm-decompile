package org.schema.game.client.view.gui.advanced.tools;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.input.InputState;

public abstract class AdvResult {
   private static final long TOOLTIP_DEFAULT_DELAY_MS = 2000L;
   public AdvCallback callback;
   public static final Vector4f WHITE = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   public static final Vector4f BLACK = new Vector4f(0.0F, 0.0F, 0.0F, 1.0F);
   public static final Vector4f RED = new Vector4f(1.0F, 0.3F, 0.3F, 1.0F);
   public static final Vector4f GREEN = new Vector4f(0.3F, 1.0F, 0.3F, 1.0F);
   public static final Vector4f HALF_TRANS = new Vector4f(0.0F, 0.0F, 0.0F, 0.4F);
   public static final Vector4f FULL_TRANS = new Vector4f(0.0F, 0.0F, 0.0F, 0.0F);
   private GUIActivationHighlightCallback actCallback = new GUIActivationHighlightCallback() {
      public boolean isVisible(InputState var1) {
         return AdvResult.this.isVisible();
      }

      public boolean isActive(InputState var1) {
         return AdvResult.this.isActive();
      }

      public boolean isHighlighted(InputState var1) {
         return AdvResult.this.isHighlighted();
      }
   };

   public abstract AdvCallback initCallback();

   public abstract String getName();

   public FontLibrary.FontSize getFontSize() {
      return FontLibrary.FontSize.MEDIUM;
   }

   public boolean isActive() {
      return true;
   }

   public boolean isVisible() {
      return true;
   }

   public boolean isHighlighted() {
      return false;
   }

   public final GUIActivationCallback getActCallback() {
      return this.actCallback;
   }

   public final void init() {
      this.callback = this.initCallback();
      this.initDefault();
   }

   protected abstract void initDefault();

   public Vector4f getFontColor() {
      return WHITE;
   }

   public int getInsetTop() {
      return 0;
   }

   public int getInsetBottom() {
      return 0;
   }

   public int getInsetRight() {
      return 0;
   }

   public int getInsetLeft() {
      return 0;
   }

   public AdvResult.VerticalAlignment getVerticalAlignment() {
      return AdvResult.VerticalAlignment.TOP;
   }

   public float getWeight() {
      return 1.0F;
   }

   public AdvResult.HorizontalAlignment getHorizontalAlignment() {
      return AdvResult.HorizontalAlignment.LEFT;
   }

   public void refresh() {
   }

   public void update(Timer var1) {
   }

   public abstract String getToolTipText();

   public long getToolTipDelayMs() {
      return 2000L;
   }

   public static enum HorizontalAlignment {
      MID,
      RIGHT,
      LEFT;
   }

   public static enum VerticalAlignment {
      MID,
      TOP,
      BOTTOM;
   }
}
