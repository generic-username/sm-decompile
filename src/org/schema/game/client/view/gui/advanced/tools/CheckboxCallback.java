package org.schema.game.client.view.gui.advanced.tools;

public interface CheckboxCallback extends AdvCallback {
   void onValueChanged(boolean var1);
}
