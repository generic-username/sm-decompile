package org.schema.game.client.view.gui.advanced.tools;

import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalProgressBar;
import org.schema.schine.input.InputState;

public class GUIAdvLabel extends GUIAdvTool {
   private final GUITextOverlay l = new GUITextOverlay(10, 10, FontLibrary.getFont(((LabelResult)this.getRes()).getFontSize()), this.getState()) {
      public void draw() {
         this.setColor(((LabelResult)GUIAdvLabel.this.getRes()).getFontColor());
         super.draw();
      }
   };

   public GUIAdvLabel(InputState var1, GUIElement var2, LabelResult var3) {
      super(var1, var2, var3);
      this.l.setTextSimple(new Object() {
         public String toString() {
            return ((LabelResult)GUIAdvLabel.this.getRes()).getName();
         }
      });
      this.attach(this.l);
   }

   public int getElementHeight() {
      return this.l.getTextHeight();
   }

   protected int getElementWidth() {
      return this.l.getMaxLineWidth();
   }

   public void setBackgroundProgressBar(final ProgressBarInterface var1) {
      GUIHorizontalProgressBar var2;
      (var2 = new GUIHorizontalProgressBar(this.getState(), this) {
         public float getValue() {
            return var1.getProgressPercent();
         }

         public void draw() {
            var1.getColor(this.getColor());
            super.draw();
         }
      }).setPos(0.0F, -2.0F, 0.0F);
      var2.onInit();
      this.attach(var2, 0);
   }
}
