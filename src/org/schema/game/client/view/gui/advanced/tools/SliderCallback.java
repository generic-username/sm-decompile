package org.schema.game.client.view.gui.advanced.tools;

public interface SliderCallback extends AdvCallback {
   void onValueChanged(int var1);
}
