package org.schema.game.client.view.gui.advanced.tools;

import javax.vecmath.Vector4f;

public interface ProgressBarInterface {
   float getProgressPercent();

   void getColor(Vector4f var1);
}
