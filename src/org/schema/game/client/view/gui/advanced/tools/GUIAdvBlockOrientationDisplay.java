package org.schema.game.client.view.gui.advanced.tools;

import org.schema.game.client.view.gui.buildtools.GUIOrientationSettingElementNew;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class GUIAdvBlockOrientationDisplay extends GUIAdvTool {
   private final GUIOrientationSettingElementNew blockPreview;

   public GUIAdvBlockOrientationDisplay(InputState var1, GUIElement var2, BlockOrientationResult var3) {
      super(var1, var2, var3);
      this.blockPreview = new GUIOrientationSettingElementNew(this.getState(), var3);
      this.attach(this.blockPreview);
   }

   public int getElementHeight() {
      return 64;
   }

   public boolean isInsideForTooltip() {
      return super.isInsideForTooltip() && this.blockPreview.isMouseInsideBlock();
   }
}
