package org.schema.game.client.view.gui.advanced.tools;

public abstract class TextBarResult extends AdvResult {
   protected void initDefault() {
   }

   public abstract String onTextChanged(String var1);
}
