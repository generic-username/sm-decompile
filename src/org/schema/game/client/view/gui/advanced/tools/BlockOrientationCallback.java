package org.schema.game.client.view.gui.advanced.tools;

public interface BlockOrientationCallback extends AdvCallback {
   void onTypeChanged(short var1);

   void onOrientationChanged(int var1);
}
