package org.schema.game.client.view.gui.advanced.tools;

public interface BlockSelectCallback extends AdvCallback {
   void onTypeChanged(short var1);
}
