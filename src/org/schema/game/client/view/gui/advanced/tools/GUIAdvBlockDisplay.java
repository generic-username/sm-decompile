package org.schema.game.client.view.gui.advanced.tools;

import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.IconDatabase;
import org.schema.schine.input.InputState;

public class GUIAdvBlockDisplay extends GUIAdvTool {
   private final GUIOverlay blockOverlay;

   public GUIAdvBlockDisplay(InputState var1, GUIElement var2, BlockDisplayResult var3) {
      super(var1, var2, var3);
      this.blockOverlay = new GUIOverlay(IconDatabase.getBuildIconsSprite(var3.getCurrentBuildIconNum()), var1) {
         public void draw() {
            this.setScale(GUIAdvBlockDisplay.this.getIconScale(), GUIAdvBlockDisplay.this.getIconScale(), 0.0F);
            IconDatabase.getBuildIcons(this, ((BlockDisplayResult)GUIAdvBlockDisplay.this.getRes()).getCurrentBuildIconNum());
            if (((BlockDisplayResult)GUIAdvBlockDisplay.this.getRes()).isBlockInit()) {
               ((BlockDisplayResult)GUIAdvBlockDisplay.this.getRes()).beforeBlockDraw(this);
               super.draw();
               ((BlockDisplayResult)GUIAdvBlockDisplay.this.getRes()).afterBlockDraw(this);
            }

         }
      };
      this.attach(this.blockOverlay);
      ((BlockDisplayResult)this.getRes()).afterInit(this.blockOverlay);
   }

   public int getElementHeight() {
      return (int)(this.blockOverlay.getHeight() * this.getIconScale());
   }

   protected float getIconScale() {
      return ((BlockDisplayResult)this.getRes()).getIconScale();
   }
}
