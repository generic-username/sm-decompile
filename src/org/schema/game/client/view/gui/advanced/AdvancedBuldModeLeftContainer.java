package org.schema.game.client.view.gui.advanced;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.advancedEntity.AdvancedEntity;
import org.schema.game.client.view.gui.advancedstats.AdvancedStructureStats;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;

public class AdvancedBuldModeLeftContainer {
   public final AdvancedStructureStats advancedStrctureStats;
   public final AdvancedEntity advancedEntity;
   public AdvancedGUIBuildModeLeftElement selected;
   private final List bmPanel = new ObjectArrayList();
   private GameClientState state;
   private GUITextButton entityButton;
   private GUITextButton structureButton;

   public AdvancedBuldModeLeftContainer(GameClientState var1) {
      this.state = var1;
      this.bmPanel.add(this.advancedStrctureStats = new AdvancedStructureStats(var1));
      this.bmPanel.add(this.advancedEntity = new AdvancedEntity(var1));
      this.selected = this.advancedEntity;
      final AdvancedGUIMinimizeCallback var2 = new AdvancedGUIMinimizeCallback(var1, true) {
         public boolean isActive() {
            return AdvancedBuldModeLeftContainer.this.selected.isActive() && AdvancedBuldModeLeftContainer.this.selected.main.isActive();
         }

         public void initialMinimized() {
            this.setMinimizedInitial(EngineSettings.STRUCTURE_STATS_MINIMIZED.isOn());
         }

         public void onMinimized(boolean var1) {
            EngineSettings.STRUCTURE_STATS_MINIMIZED.setCurrentState(var1);

            try {
               EngineSettings.write();
            } catch (IOException var2) {
               var2.printStackTrace();
            }
         }

         public String getMinimizedText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCED_ADVANCEDBULDMODELEFTCONTAINER_0;
         }

         public String getMaximizedText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCED_ADVANCEDBULDMODELEFTCONTAINER_1;
         }
      };
      this.entityButton = new GUITextButton(var1, 170, 20, new Object() {
         public String toString() {
            return AdvancedBuldModeLeftContainer.this.advancedEntity.getPanelName();
         }
      }, new GUICallback() {
         public boolean isOccluded() {
            return !AdvancedBuldModeLeftContainer.this.advancedEntity.isActive() || !AdvancedBuldModeLeftContainer.this.advancedEntity.main.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               AdvancedBuldModeLeftContainer.this.switchFrom(AdvancedBuldModeLeftContainer.this.advancedStrctureStats);
            }

         }
      }) {
         public void draw() {
            if (!var2.isMinimized()) {
               var2.setButtonPosition(this, AdvancedBuldModeLeftContainer.this.advancedEntity);
               Vector3f var10000 = this.getPos();
               var10000.y -= this.getWidth() + 5.0F;
               super.draw();
            }

            if (AdvancedBuldModeLeftContainer.this.selected == AdvancedBuldModeLeftContainer.this.advancedEntity) {
               this.getBackgroundColor().set(1.0F, 1.0F, 0.0F, 1.0F);
               this.getBackgroundColorMouseOverlay().set(1.0F, 1.0F, 0.0F, 1.0F);
            } else {
               this.getBackgroundColor().set(0.3F, 0.3F, 0.6F, 0.9F);
               this.getBackgroundColorMouseOverlay().set(0.3F, 0.3F, 0.6F, 0.9F);
            }
         }
      };
      this.structureButton = new GUITextButton(var1, 170, 20, new Object() {
         public String toString() {
            return AdvancedBuldModeLeftContainer.this.advancedStrctureStats.getPanelName();
         }
      }, new GUICallback() {
         public boolean isOccluded() {
            return !AdvancedBuldModeLeftContainer.this.advancedStrctureStats.isActive() || !AdvancedBuldModeLeftContainer.this.advancedStrctureStats.main.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               AdvancedBuldModeLeftContainer.this.switchFrom(AdvancedBuldModeLeftContainer.this.advancedEntity);
            }

         }
      }) {
         public void draw() {
            if (!var2.isMinimized()) {
               var2.setButtonPosition(this, AdvancedBuldModeLeftContainer.this.advancedStrctureStats);
               Vector3f var10000 = this.getPos();
               var10000.y += (float)(var2.getButtonWidth() + 5);
               super.draw();
            }

            if (AdvancedBuldModeLeftContainer.this.selected == AdvancedBuldModeLeftContainer.this.advancedStrctureStats) {
               this.getBackgroundColor().set(1.0F, 1.0F, 0.0F, 1.0F);
               this.getBackgroundColorMouseOverlay().set(1.0F, 1.0F, 0.0F, 1.0F);
            } else {
               this.getBackgroundColor().set(0.3F, 0.3F, 0.6F, 0.9F);
               this.getBackgroundColorMouseOverlay().set(0.3F, 0.3F, 0.6F, 0.9F);
            }
         }
      };
      var2.additionalButtons.add(this.entityButton);
      var2.additionalButtons.add(this.structureButton);
      Iterator var3 = this.bmPanel.iterator();

      while(var3.hasNext()) {
         ((AdvancedGUIBuildModeLeftElement)var3.next()).setMinimizeCallback(var2);
      }

   }

   public void update(Timer var1) {
      Iterator var2 = this.bmPanel.iterator();

      while(var2.hasNext()) {
         ((AdvancedGUIBuildModeLeftElement)var2.next()).update(var1);
      }

   }

   public void onInit() {
      this.entityButton.onInit();
      Iterator var1 = this.bmPanel.iterator();

      while(var1.hasNext()) {
         ((AdvancedGUIBuildModeLeftElement)var1.next()).onInit();
      }

   }

   public void draw() {
      this.selected.draw();
   }

   public void drawToolTip(long var1) {
      this.selected.drawToolTip(var1);
   }

   public boolean isInside() {
      return this.selected.isInside();
   }

   public String getSwitchToNameFrom(AdvancedGUIBuildModeLeftElement var1) {
      return this.getNext(var1).getPanelName();
   }

   public void switchFrom(AdvancedGUIBuildModeLeftElement var1) {
      this.selected = this.getNext(var1);
   }

   public AdvancedGUIBuildModeLeftElement getNext(AdvancedGUIBuildModeLeftElement var1) {
      int var2;
      return (var2 = this.bmPanel.indexOf(var1)) >= 0 ? (AdvancedGUIBuildModeLeftElement)this.bmPanel.get((var2 + 1) % this.bmPanel.size()) : var1;
   }
}
