package org.schema.game.client.view.gui.advanced.tools;

import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.input.InputState;

public class GUIAdvTextBar extends GUIAdvTool {
   private final GUIActivatableTextBar chk;

   public GUIAdvTextBar(InputState var1, GUIElement var2, TextBarResult var3) {
      super(var1, var2, var3);
      this.chk = new GUIActivatableTextBar(this.getState(), var3.getFontSize(), var2, new TextCallback() {
         public void onTextEnter(String var1, boolean var2, boolean var3) {
            GUIAdvTextBar.this.chk.deactivateBar();
         }

         public void onFailedTextCheck(String var1) {
         }

         public void newLine() {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public String[] getCommandPrefixes() {
            return null;
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            return ((TextBarResult)GUIAdvTextBar.this.getRes()).onTextChanged(var1);
         }
      });
      this.attach(this.chk);
   }

   public int getElementHeight() {
      return (int)this.chk.getHeight();
   }
}
