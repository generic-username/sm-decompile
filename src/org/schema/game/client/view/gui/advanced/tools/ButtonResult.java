package org.schema.game.client.view.gui.advanced.tools;

import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;

public abstract class ButtonResult extends AdvResult {
   public void rightClick() {
      if (this.callback != null) {
         ((ButtonCallback)this.callback).pressedRightMouse();
      }

   }

   public void leftClick() {
      if (this.callback != null) {
         ((ButtonCallback)this.callback).pressedLeftMouse();
      }

   }

   protected void initDefault() {
   }

   public String getToolTipText() {
      return null;
   }

   public abstract GUIHorizontalArea.HButtonColor getColor();
}
