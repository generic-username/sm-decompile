package org.schema.game.client.view.gui.advanced.tools;

import org.schema.schine.graphicsengine.forms.gui.newgui.settingsnew.GUIScrollSettingSelector;

public abstract class SliderResult extends AdvResult {
   private int currentValue = Integer.MIN_VALUE;

   protected void initDefault() {
      this.change(this.getDefault());
   }

   public int getCurrentValue() {
      return this.currentValue;
   }

   public boolean showLabel() {
      return true;
   }

   public void mod(int var1) {
      this.change(this.currentValue + var1);
   }

   public void change(int var1) {
      int var2 = this.currentValue;
      this.currentValue = Math.min(this.getMax(), Math.max(this.getMin(), var1));
      if (var2 != this.currentValue && this.callback != null) {
         ((SliderCallback)this.callback).onValueChanged(this.currentValue);
      }

   }

   public abstract int getDefault();

   public abstract int getMax();

   public abstract int getMin();

   public void refresh() {
      this.change(this.getDefault());
   }

   public void onInitializeScrollSetting(GUIScrollSettingSelector var1) {
   }

   public int getResetValue() {
      return this.getMin();
   }
}
