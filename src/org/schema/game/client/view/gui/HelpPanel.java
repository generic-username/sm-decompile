package org.schema.game.client.view.gui;

import javax.vecmath.Vector3f;
import org.schema.game.client.controller.manager.HelpManager;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.input.InputState;

public class HelpPanel extends GUIElement {
   private boolean expanded;
   private GUIOverlay helpButton;

   public HelpPanel(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      this.helpButton.draw();
   }

   public void onInit() {
      this.helpButton = new GUIOverlay(Controller.getResLoader().getSprite("help-gui-"), this.getState());
      this.helpButton.orientate(9);
      this.helpButton.onInit();
      Vector3f var10000 = this.helpButton.getPos();
      var10000.y -= 64.0F;
   }

   public float getHeight() {
      return 0.0F;
   }

   public float getWidth() {
      return 0.0F;
   }

   public boolean isPositionCenter() {
      return false;
   }

   public HelpManager getHelpManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getHelpManager();
   }

   public PlayerInteractionControlManager getInteractionManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager();
   }

   public boolean isExpanded() {
      return this.expanded;
   }

   public void setExpanded(boolean var1) {
      this.expanded = var1;
   }
}
