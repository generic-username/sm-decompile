package org.schema.game.client.view.gui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.ArrayList;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.newdawn.slick.Color;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.InventoryControllerManager;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.controller.manager.ingame.SegmentBuildController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.inventory.InventoryIconsNew;
import org.schema.game.client.view.gui.inventory.InventorySlotOverlayElement;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.client.view.gui.weapon.WeaponSlotOverlayElement;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.element.meta.Recipe;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventorySlot;
import org.schema.game.common.data.player.inventory.NoSlotFreeException;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.TooltipProvider;
import org.schema.schine.input.Mouse;

@Deprecated
public class BuildSideBarOld extends GUIElement implements HotbarInterface, TooltipProvider {
   private final float spacing = 70.0F;
   private InventorySlotOverlayElement[] icons = new InventorySlotOverlayElement[256];
   private InventorySlotOverlayElement multiIconHelper;
   private GUITextOverlay[] iconsTexts;
   private GUIColoredRectangle selectIcon;
   private GUIOverlay background;
   private PowerHealthBars powerHealthBars;
   private int lastSelected;
   private long selectTime;
   private boolean mouseGlobalCheck = true;
   private GUIOverlay reload;
   private int lastSelectedSub;
   private Vector4f iconTint = new Vector4f();

   public BuildSideBarOld(GameClientState var1) {
      super(var1);
      this.setIconsTexts(new GUITextOverlay[256]);

      for(int var2 = 0; var2 < this.icons.length; ++var2) {
         this.icons[var2] = new InventorySlotOverlayElement(true, var1, true, this);
         this.multiIconHelper = new InventorySlotOverlayElement(true, var1, true, this);
         this.getIconsTexts()[var2] = new GUITextOverlay(32, 32, FontLibrary.getBoldArial16White(), var1);
         this.getIconsTexts()[var2].setColor(Color.white);
         this.getIconsTexts()[var2].setText(new ArrayList());
         this.getIconsTexts()[var2].getText().add("i " + var2);
         this.getIconsTexts()[var2].setPos(2.0F, 2.0F, 0.0F);
         this.icons[var2].attach(this.getIconsTexts()[var2]);
      }

      this.reload = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "tools-16x16-gui-"), var1);
      this.background = new GUIOverlay(Controller.getResLoader().getSprite("sidebar-gui-"), var1);
      this.selectIcon = new GUIColoredRectangle(this.getState(), 64.0F, 64.0F, new Vector4f(1.0F, 1.0F, 1.0F, 0.18F));
   }

   public void activateDragging(boolean var1) {
      for(int var2 = 0; var2 < 10; ++var2) {
         this.icons[var2].setMouseUpdateEnabled(var1);
      }

   }

   public void drawDragging(WeaponSlotOverlayElement var1) {
   }

   public void cleanUp() {
   }

   public void draw() {
      GlUtil.glPushMatrix();
      SegmentBuildController var1 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController();
      PlayerInteractionControlManager var2 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager();
      if (var1 != null) {
         int var14 = var2.getSelectedSlot();
         Inventory var3 = ((GameClientState)this.getState()).getPlayer().getInventory((Vector3i)null);
         this.transform();
         if (this.mouseGlobalCheck) {
            this.checkMouseInside();
         }

         this.background.draw();

         for(int var4 = 0; var4 < 10; ++var4) {
            short var5 = 0;
            int var6 = 511;
            int var7 = var2.getSlotKey(var4 + 2);
            int var8 = -1;
            short var9 = -1;
            MetaObject var10 = null;
            if (var3.isSlotEmpty(var7)) {
               this.iconsTexts[var4].getText().set(0, "");
            } else if ((var5 = var3.getType(var7)) < 0) {
               if (var5 != -32768) {
                  var6 = Math.abs(var5);
                  if (var5 == MetaObjectManager.MetaObjectType.RECIPE.type) {
                     if ((var10 = ((GameClientState)this.getState()).getMetaObjectManager().getObject(var3.getMeta(var7))) != null) {
                        var8 = ElementKeyMap.getInfo(var9 = ((Recipe)var10).recipeProduct[0].outputResource[0].type).getBuildIconNum();
                     }
                  } else if (var5 == MetaObjectManager.MetaObjectType.WEAPON.type && (var10 = ((GameClientState)this.getState()).getMetaObjectManager().getObject(var3.getMeta(var7))) != null) {
                     var6 += var10.getExtraBuildIconIndex();
                  }
               }
            } else {
               var6 = ElementKeyMap.getInfo(var5).getBuildIconNum();
            }

            int var11 = var3.getCount(var7, var5);
            if (var5 != -32768) {
               if (var8 >= 0) {
                  this.doDraw(var6, var7, var5, var10, 0, var11, 159.0F, 21.0F, 1.0F, true, true, true, var4);
                  this.doDraw(var8, var7, var9, var10, 0, var11, 191.0F, 53.0F, 0.5F, false, true, false, var4);
                  this.doDraw(var6, var7, var5, var10, 0, var11, 159.0F, 21.0F, 1.0F, true, false, true, var4);
               } else {
                  this.doDraw(var6, var7, var5, var10, 0, var11, 159.0F, 21.0F, 1.0F, true, true, true, var4);
               }
            } else {
               ObjectArrayList var16 = new ObjectArrayList(var3.getSubSlots(var7));

               int var12;
               for(var12 = 1; var12 < var16.size(); ++var12) {
                  InventorySlot var13 = (InventorySlot)var16.get(var12);
                  if (var12 == ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedSubSlot() && var7 == ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedSlot()) {
                     var16.set(var12, var16.get(0));
                     var16.set(0, var13);
                  }
               }

               for(var12 = 1; var12 < var16.size() && var12 < 5; ++var12) {
                  float var22 = (float)((var12 - 1) % 2);
                  float var17 = (float)((var12 - 1) / 2);
                  InventorySlot var19 = (InventorySlot)var16.get(var12);
                  this.doDraw(ElementKeyMap.getInfo(var19.getType()).getBuildIconNum(), var7, var19.getType(), var10, var19.count(), var19.count(), 159.0F + var22 * 32.0F, 21.0F + var17 * 32.0F, var22 * 44.0F, var17 * 42.0F, 0.5F, true, true, false, var4);
               }

               InventorySlot var21 = (InventorySlot)var16.get(0);
               this.doDraw(ElementKeyMap.getInfo(var21.getType()).getBuildIconNum(), var7, var21.getType(), var10, var21.count(), var21.count(), 167.0F, 29.0F, 0.75F, true, true, false, var4);
               this.doDraw(ElementKeyMap.getInfo(var21.getType()).getBuildIconNum(), var7, var5, var10, var21.count(), var21.count(), 159.0F, 21.0F, 1.0F, false, false, true, var4);
            }

            var6 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedSubSlot();
            if (var7 == var14) {
               if (this.lastSelected != var7 || this.lastSelectedSub != var6) {
                  this.selectTime = System.currentTimeMillis();
                  this.lastSelected = var7;
                  this.lastSelectedSub = var6;
               }

               this.selectIcon.getPos().set(this.icons[var4].getPos());
               this.selectIcon.getScale().set(this.icons[var4].getScale());
               this.selectIcon.draw();
               long var23;
               if ((var23 = System.currentTimeMillis() - this.selectTime) < 1200L) {
                  if (this.icons[var4].getType() != -32768) {
                     GlUtil.glPushMatrix();
                     this.icons[var4].drawToolTipFixed((float)var23 / 1200.0F);
                     GlUtil.glPopMatrix();
                  } else {
                     GlUtil.glPushMatrix();
                     InventorySlot var18;
                     if ((var18 = var3.getSlot(this.icons[var4].getSlot())).isMultiSlot()) {
                        for(int var20 = 0; var20 < var18.getSubSlots().size(); ++var20) {
                           InventorySlot var15 = (InventorySlot)var18.getSubSlots().get(var20);
                           this.multiIconHelper.setPos(this.icons[var4].getPos());
                           this.multiIconHelper.setScale(this.icons[var4].getScale());
                           Vector3f var10000 = this.multiIconHelper.getPos();
                           var10000.y -= (float)(var20 + 1 << 6);
                           this.iconsTexts[var4].getText().set(0, String.valueOf(var15.count()));
                           if (var3.isInfinite()) {
                              this.iconsTexts[var4].getText().set(0, "");
                           }

                           this.iconsTexts[var4].setPos(this.multiIconHelper.getPos());
                           this.multiIconHelper.setType(var15.getType());
                           this.multiIconHelper.setLayer(ElementKeyMap.getInfo(var15.getType()).getBuildIconNum() / 256);
                           this.multiIconHelper.setSpriteSubIndex(ElementKeyMap.getInfo(var15.getType()).getBuildIconNum() % 256);
                           this.multiIconHelper.draw();
                           this.iconsTexts[var4].draw();
                           if (var6 == var20) {
                              this.selectIcon.getPos().set(this.multiIconHelper.getPos());
                              this.selectIcon.getScale().set(this.multiIconHelper.getScale());
                              this.selectIcon.draw();
                           }

                           this.iconsTexts[var4].getText().set(0, "");
                           this.iconsTexts[var4].getPos().set(0.0F, 0.0F, 0.0F);
                        }
                     }

                     GlUtil.glPopMatrix();
                  }
               }
            }
         }

         this.icons[0].getSprite().setSelectedMultiSprite(0);
         GlUtil.glPopMatrix();
      }
   }

   public void onInit() {
      this.background.onInit();

      for(int var1 = 0; var1 < this.icons.length; ++var1) {
         this.icons[var1].onInit();
      }

      this.selectIcon.onInit();
      this.powerHealthBars = new PowerHealthBars((GameClientState)this.getState());
      this.powerHealthBars.onInit();
      this.background.attach(this.powerHealthBars);
      this.reload.setScale(2.0F, 2.0F, 2.0F);
      this.reload.onInit();
   }

   private void doDraw(int var1, int var2, short var3, MetaObject var4, int var5, int var6, float var7, float var8, float var9, boolean var10, boolean var11, boolean var12, int var13) {
      this.doDraw(var1, var2, var3, var4, var5, var6, var7, var8, 0.0F, 0.0F, var9, var10, var11, var12, var13);
   }

   private void doDraw(int var1, int var2, short var3, MetaObject var4, int var5, int var6, float var7, float var8, float var9, float var10, float var11, boolean var12, boolean var13, boolean var14, int var15) {
      Inventory var16 = ((GameClientState)this.getState()).getPlayer().getInventory((Vector3i)null);
      if (this.mouseGlobalCheck) {
         this.icons[var2].setMouseUpdateEnabled(var14);
         this.checkMouseInside();
      } else {
         this.icons[var2].setMouseUpdateEnabled(false);
      }

      this.iconTint.set(1.0F, 1.0F, 1.0F, 1.0F);
      if (this.icons[var2].getSprite().getTint() == null) {
         this.icons[var2].getSprite().setTint(new Vector4f());
      }

      if (((GameClientState)this.getState()).getController().getTutorialMode() != null && ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager().isTreeActive() && var16 == ((GameClientState)this.getState()).getPlayer().getInventory() && ((GameClientState)this.getState()).getController().getTutorialMode().highlightType == var3) {
         this.icons[var2].getSprite().getTint().set(0.4F + HudIndicatorOverlay.selectColorValue, 0.9F + HudIndicatorOverlay.selectColorValue, 0.1F + HudIndicatorOverlay.selectColorValue, 0.1F + HudIndicatorOverlay.selectColorValue);
         this.iconsTexts[var2].setColor(new Vector4f(this.icons[var2].getSprite().getTint()));
      } else {
         this.icons[var2].getSprite().getTint().set(this.iconTint);
         this.iconsTexts[var2].setColor(this.iconTint);
      }

      GlUtil.glPushMatrix();
      this.icons[var2].setSlot(var2);
      this.icons[var2].setType(var3);
      this.icons[var2].setMeta(var16.getMeta(var2));
      this.icons[var2].setCount(var6);
      this.icons[var2].setScale(var11, var11, var11);
      if ((var6 = this.icons[var2].getCount(false)) <= 0 && var5 <= 0) {
         var1 = 511;
         this.iconsTexts[var2].getText().set(0, "");
      } else {
         if (var5 > 0) {
            if (this.icons[var2].getSubSlotType() == var3) {
               var5 -= this.icons[var2].getDraggingCount();
            }

            this.iconsTexts[var2].getText().set(0, String.valueOf(var5));
         } else {
            this.iconsTexts[var2].getText().set(0, String.valueOf(var6));
         }

         if (var16.isInfinite()) {
            this.iconsTexts[var2].getText().set(0, "");
         }
      }

      if (!var12) {
         this.iconsTexts[var2].getText().set(0, "");
      }

      this.iconsTexts[var2].setPos(var9, var10, 0.0F);
      this.icons[var2].setInvisible(false);
      this.icons[var2].getPos().y = var8;
      this.icons[var2].getPos().x = var7 + (float)var15 * 70.0F;
      var5 = var1 / 256;
      if (var3 > 0) {
         this.icons[var2].setLayer(var5);
      } else {
         this.icons[var2].setLayer(-1);
      }

      short var17 = (short)(var1 % 256);
      this.icons[var2].getSprite().setSelectedMultiSprite(var17);
      if (var13) {
         this.icons[var2].draw();
         if (var4 != null) {
            this.reload.setPos(this.icons[var2].getPos());
            var4.drawPossibleOverlay(this.reload, (Inventory)null);
         }
      } else if (var14 && this.mouseGlobalCheck) {
         this.icons[var2].checkMouseInsideWithTransform();
      }

      GlUtil.glPopMatrix();
      this.icons[var2].setScale(1.0F, 1.0F, 1.0F);
      this.iconsTexts[var2].setPos(0.0F, 0.0F, 0.0F);
      if (this.icons[var2].getSprite().getTint() != null) {
         this.icons[var2].getSprite().getTint().set(1.0F, 1.0F, 1.0F, 1.0F);
      }

      this.iconsTexts[var2].setColor(1.0F, 1.0F, 1.0F, 1.0F);
   }

   protected void doOrientation() {
   }

   public float getHeight() {
      return this.background.getHeight();
   }

   public float getWidth() {
      return this.background.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void drawToolTip() {
      if (!Mouse.isGrabbed()) {
         GlUtil.glPushMatrix();

         for(int var1 = 0; var1 < this.icons.length; ++var1) {
            this.icons[var1].drawToolTip();
         }

         GlUtil.glPopMatrix();
      }
   }

   public GUITextOverlay[] getIconsTexts() {
      return this.iconsTexts;
   }

   public void setIconsTexts(GUITextOverlay[] var1) {
      this.iconsTexts = var1;
   }

   public void update(Timer var1) {
      super.update(var1);
      this.powerHealthBars.update(var1);
   }

   public boolean placeInChestOrInv(InventorySlotOverlayElement var1) {
      InventoryControllerManager var2 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager();
      Inventory var3 = ((GameClientState)this.getState()).getPlayer().getInventory();
      if (var2.getSecondInventory() != null) {
         System.err.println("[CLIENT] HOTBAR FAST SWITCH TO MULTI");
         return this.switchInto(var3, var2.getSecondInventory(), var1);
      } else {
         System.err.println("[CLIENT] HOTBAR FAST SWITCH TO SINGLE");
         return this.switchToInv(var3, var1);
      }
   }

   private boolean switchInto(Inventory var1, Inventory var2, InventorySlotOverlayElement var3) {
      int var4 = var2.getFirstSlot(var3.getType(), false);
      boolean var5 = true;
      if (var4 < 0) {
         try {
            var5 = false;
            var4 = var2.getFreeSlot();
         } catch (NoSlotFreeException var6) {
            var6.printStackTrace();
            ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDSIDEBAROLD_0, 0.0F);
         }
      }

      if (var4 >= 0) {
         if (var5) {
            var2.switchSlotsOrCombineClient(var4, var3.getSlot(), var1, -1);
         } else {
            var1.switchSlotsOrCombineClient(var3.getSlot(), var4, var2, -1);
         }

         return true;
      } else {
         return false;
      }
   }

   private boolean switchToInv(Inventory var1, InventorySlotOverlayElement var2) {
      try {
         int var3;
         if ((var3 = var1.getFirstNonActiveSlot(var2.getType(), false)) < var1.getActiveSlotsMax()) {
            System.err.println("[CLIENT][INVENTORY] FastSwitch: No exisitng slot found: " + var3 + "; checking free");
            var3 = var1.getFreeNonActiveSlot();
         }

         if (var3 >= var1.getActiveSlotsMax()) {
            System.err.println("[CLIENT][INVENTORY] FastSwitch: slot found: " + var2.getSlot() + " -> " + var3);
            var1.switchSlotsOrCombineClient(var3, var2.getSlot(), var1, -1);
         } else {
            System.err.println("[CLIENT][INVENTORY] FastSwitch: No free slot found: " + var3);
         }
      } catch (NoSlotFreeException var4) {
         var4.printStackTrace();
      }

      return false;
   }

   public void displaySplitDialog(InventorySlotOverlayElement var1) {
      InventoryIconsNew.displaySplitDialog(var1, (GameClientState)this.getState(), ((GameClientState)this.getState()).getPlayer().getInventory());
   }
}
