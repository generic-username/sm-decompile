package org.schema.game.client.view.gui.gamemenus.gamemenusnew;

import java.io.IOException;
import org.schema.game.client.controller.MainMenu;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.rules.RuleSetConfigDialogGame;
import org.schema.game.client.view.mainmenu.gui.ruleconfig.GUIRuleSetStat;
import org.schema.game.common.controller.rules.RuleSetManager;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;

public class MainMenuPanelNew extends GUIMainWindow implements GUIActiveInterface {
   private MainMenu mainMenu;
   private boolean init;
   private GUIContentPane generalTab;

   public MainMenuPanelNew(InputState var1, MainMenu var2) {
      super(var1, 300, ((GameClientState)var1).isAdmin() ? 274 : 243, "MainMenuPanelNewNAAA");
      this.mainMenu = var2;
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      super.draw();
   }

   public void onInit() {
      super.onInit();
      this.recreateTabs();
      this.orientate(48);
      this.init = true;
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public void recreateTabs() {
      Object var1 = null;
      if (this.getSelectedTab() < this.getTabs().size()) {
         var1 = ((GUIContentPane)this.getTabs().get(this.getSelectedTab())).getTabName();
      }

      this.clearTabs();
      this.generalTab = this.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GAMEMENUS_GAMEMENUSNEW_MAINMENUPANELNEW_0);
      this.createGeneralPane();
      if (var1 != null) {
         for(int var2 = 0; var2 < this.getTabs().size(); ++var2) {
            if (((GUIContentPane)this.getTabs().get(var2)).getTabName().equals(var1)) {
               this.setSelectedTab(var2);
               return;
            }
         }
      }

   }

   private void addButton(GUIHorizontalArea.HButtonType var1, String var2, GUICallback var3, boolean var4) {
      if (var4) {
         this.generalTab.setTextBoxHeightLast(28);
      } else {
         this.generalTab.addNewTextBox(28);
      }

      int var6 = this.generalTab.getTextboxes().size() - 1;
      GUIHorizontalButtonTablePane var5;
      (var5 = new GUIHorizontalButtonTablePane(this.getState(), 1, 1, this.generalTab.getContent(var6))).onInit();
      var5.addButton(0, 0, var2, (GUIHorizontalArea.HButtonType)var1, var3, (GUIActivationCallback)null);
      this.generalTab.getContent(var6).attach(var5);
   }

   private void createGeneralPane() {
      this.addButton(GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GAMEMENUS_GAMEMENUSNEW_MAINMENUPANELNEW_1, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               MainMenuPanelNew.this.mainMenu.pressedResume();
            }

         }

         public boolean isOccluded() {
            return !MainMenuPanelNew.this.isActive();
         }
      }, true);
      this.addButton(GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GAMEMENUS_GAMEMENUSNEW_MAINMENUPANELNEW_2, new GUICallback() {
         public boolean isOccluded() {
            return !MainMenuPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               MainMenuPanelNew.this.mainMenu.pressedOptions();
            }

         }
      }, false);
      if (this.getState().isAdmin()) {
         this.addButton(GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GAMEMENUS_GAMEMENUSNEW_MAINMENUPANELNEW_4, new GUICallback() {
            public boolean isOccluded() {
               return !MainMenuPanelNew.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse() && MainMenuPanelNew.this.getState().isAdmin()) {
                  MainMenuPanelNew.this.mainMenu.deactivate();

                  try {
                     RuleSetManager var4;
                     (var4 = new RuleSetManager(MainMenuPanelNew.this.getState().getGameState().getRuleManager())).setState(MainMenuPanelNew.this.getState());
                     GUIRuleSetStat var5;
                     (var5 = new GUIRuleSetStat(MainMenuPanelNew.this.getState(), var4)).gameState = MainMenuPanelNew.this.getState().getGameState();
                     (new RuleSetConfigDialogGame(MainMenuPanelNew.this.getState(), var5)).activate();
                     return;
                  } catch (IOException var3) {
                     var3.printStackTrace();
                  }
               }

            }
         }, false);
      }

      this.addButton(GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, this.mainMenu.getTutorialsStringObj(), new GUICallback() {
         public boolean isOccluded() {
            return !MainMenuPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               MainMenuPanelNew.this.mainMenu.pressedTutorials();
            }

         }
      }, false);
      this.addButton(GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GAMEMENUS_GAMEMENUSNEW_MAINMENUPANELNEW_3, new GUICallback() {
         public boolean isOccluded() {
            return !MainMenuPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               MainMenuPanelNew.this.mainMenu.pressedSuicide();
            }

         }
      }, false);
      this.addButton(GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GAMEMENUS_GAMEMENUSNEW_MAINMENUPANELNEW_5, new GUICallback() {
         public boolean isOccluded() {
            return !MainMenuPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               MainMenuPanelNew.this.mainMenu.pressedExit();
            }

         }
      }, false);
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFactionManager().getFaction(this.getOwnPlayer().getFactionId());
   }
}
