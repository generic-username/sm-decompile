package org.schema.game.client.view.gui.transporter;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.elements.transporter.TransporterCollectionManager;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUITrasporterDestinationsScrollableList extends ScrollableTableList implements DrawerObserver {
   private TransporterCollectionManager transporter;
   private long lastRefresh;

   public GUITrasporterDestinationsScrollableList(InputState var1, GUIElement var2, TransporterCollectionManager var3) {
      super(var1, 100.0F, 100.0F, var2);
      this.transporter = var3;
      ((GameClientController)this.getState().getController()).sectorChangeObservable.addObserver(this);
   }

   public void cleanUp() {
      ((GameClientController)this.getState().getController()).sectorChangeObservable.deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRANSPORTER_GUITRASPORTERDESTINATIONSSCROLLABLELIST_0, 3.0F, new Comparator() {
         public int compare(TransporterDestinations var1, TransporterDestinations var2) {
            return var1.target.getName().compareTo(var2.target.getName());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRANSPORTER_GUITRASPORTERDESTINATIONSSCROLLABLELIST_1, 3.0F, new Comparator() {
         public int compare(TransporterDestinations var1, TransporterDestinations var2) {
            return var1.name.compareTo(var2.name);
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRANSPORTER_GUITRASPORTERDESTINATIONSSCROLLABLELIST_2, 80, new Comparator() {
         public int compare(TransporterDestinations var1, TransporterDestinations var2) {
            return var1.pos.compareTo(var2.pos);
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, TransporterDestinations var2) {
            return var2.name.toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRANSPORTER_GUITRASPORTERDESTINATIONSSCROLLABLELIST_3, ControllerElement.FilterRowStyle.FULL);
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, TransporterDestinations var2) {
            return var2.target.getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRANSPORTER_GUITRASPORTERDESTINATIONSSCROLLABLELIST_4, ControllerElement.FilterRowStyle.FULL);
      this.activeSortColumnIndex = 1;
   }

   protected Collection getElementList() {
      return this.transporter.getActiveTransporterDestinations(this.transporter.getSegmentController());
   }

   public void draw() {
      super.draw();
      long var1;
      if ((var1 = System.currentTimeMillis()) - this.lastRefresh > 1000L) {
         this.flagDirty();
         this.lastRefresh = var1;
      }

   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getPlayer();
      Iterator var9 = var2.iterator();

      while(var9.hasNext()) {
         final TransporterDestinations var3 = (TransporterDestinations)var9.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());

         assert var3.name != null;

         var4.setTextSimple(var3.target.getName());
         var5.setTextSimple(var3.name);
         var6.setTextSimple(var3.pos.toStringPure());
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         ScrollableTableList.GUIClippedRow var8;
         (var8 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var6);
         var4.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;
         GUITrasporterDestinationsScrollableList.TransporterDestinationsRow var11;
         (var11 = new GUITrasporterDestinationsScrollableList.TransporterDestinationsRow(this.getState(), var3, new GUIElement[]{var4, var7, var8})).expanded = new GUIElementList(this.getState());
         GUIAncor var12 = new GUIAncor(this.getState(), 100.0F, 30.0F);
         GUITextButton var10;
         (var10 = new GUITextButton(this.getState(), 140, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRANSPORTER_GUITRASPORTERDESTINATIONSSCROLLABLELIST_5, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  GUITrasporterDestinationsScrollableList.this.transporter.setDestination(var3.target.getUniqueIdentifier(), ElementCollection.getIndex(var3.pos));
                  GUITrasporterDestinationsScrollableList.this.transporter.sendDestinationUpdate();
               }

            }

            public boolean isOccluded() {
               return !GUITrasporterDestinationsScrollableList.this.isActive();
            }
         })).onInit();
         var12.attach(var10);
         var12.onInit();
         var10.setPos(2.0F, 2.0F, 0.0F);
         var11.expanded.add(new GUIListElement(var12, var12, this.getState()));
         var11.onInit();
         var1.addWithoutUpdate(var11);
      }

      var1.updateDim();
   }

   protected boolean isFiltered(TransporterDestinations var1) {
      return super.isFiltered(var1) || var1.target == this.transporter.getSegmentController() && var1.pos.equals(this.transporter.getControllerPos());
   }

   public void update(DrawerObservable var1, Object var2, Object var3) {
      this.flagDirty();
   }

   class TransporterDestinationsRow extends ScrollableTableList.Row {
      public TransporterDestinationsRow(InputState var2, TransporterDestinations var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
