package org.schema.game.client.view.gui.manualtrade;

import org.schema.game.client.controller.PlayerManualTradeInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIInnerTextbox;
import org.schema.schine.input.InputState;

public class GUIPlayerTradePanel extends GUIInputPanel implements GUIActiveInterface {
   private PlayerManualTradeInput dialog;

   public GUIPlayerTradePanel(InputState var1, int var2, int var3, PlayerManualTradeInput var4) {
      super("GUIPlayerTradePanel", var1, var2, var3, var4, "Trade", "");
      this.setOkButton(false);
      this.dialog = var4;
   }

   public void onInit() {
      super.onInit();
      GUIContentPane var1;
      (var1 = ((GUIDialogWindow)this.background).getMainContentPane()).addDivider(280);
      var1.setEqualazingHorizontalDividers(0);
      this.createDiv(0, var1, this.dialog.trade.a[0]);
      this.createDiv(1, var1, this.dialog.trade.a[0]);
   }

   private void createDiv(int var1, GUIContentPane var2, final AbstractOwnerState var3) {
      var2.setTextBoxHeightLast(var1, 49);
      GUIInnerTextbox var4 = (GUIInnerTextbox)var2.getTextboxes(var1).get(0);
      GUIHorizontalButtonTablePane var5;
      (var5 = new GUIHorizontalButtonTablePane(this.getState(), 1, 2, var4)).onInit();
      var5.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MANUALTRADE_GUIPLAYERTRADEPANEL_0, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return var3 != ((GameClientState)GUIPlayerTradePanel.this.getState()).getPlayer();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               assert var3 == ((GameClientState)GUIPlayerTradePanel.this.getState()).getPlayer();

               GUIPlayerTradePanel.this.dialog.trade.clientAccept(((GameClientState)GUIPlayerTradePanel.this.getState()).getPlayer(), !GUIPlayerTradePanel.this.dialog.trade.isReady(((GameClientState)GUIPlayerTradePanel.this.getState()).getPlayer()));
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIPlayerTradePanel.this.isActive();
         }

         public boolean isHighlighted(InputState var1) {
            return GUIPlayerTradePanel.this.dialog.trade.isReady(var3);
         }
      });
      var5.addButton(0, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MANUALTRADE_GUIPLAYERTRADEPANEL_1, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return var3 != ((GameClientState)GUIPlayerTradePanel.this.getState()).getPlayer();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               assert var3 == ((GameClientState)GUIPlayerTradePanel.this.getState()).getPlayer();

               GUIPlayerTradePanel.this.dialog.trade.clientCancel(((GameClientState)GUIPlayerTradePanel.this.getState()).getPlayer());
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return var3 == ((GameClientState)GUIPlayerTradePanel.this.getState()).getPlayer();
         }

         public boolean isActive(InputState var1) {
            return GUIPlayerTradePanel.this.isActive();
         }
      });
      var4.attach(var5);
      var2.addNewTextBox(var1, 86);
      GUIInnerTextbox var6 = (GUIInnerTextbox)var2.getTextboxes(var1).get(1);
      GUIManualTradeItemScrollableList var7;
      (var7 = new GUIManualTradeItemScrollableList(this.getState(), var6, this.dialog)).onInit();
      var6.attach(var7);
   }

   public boolean isActive() {
      return (this.getState().getController().getPlayerInputs().isEmpty() || ((DialogInterface)this.getState().getController().getPlayerInputs().get(this.getState().getController().getPlayerInputs().size() - 1)).getInputPanel() == this) && super.isActive();
   }
}
