package org.schema.game.client.view.gui.manualtrade;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Observer;
import java.util.Set;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.ship.InShipControlManager;
import org.schema.game.client.controller.manager.ingame.ship.WeaponAssignControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.trade.manualtrade.ManualTrade;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.game.common.data.world.Sector;
import org.schema.game.network.objects.remote.RemoteManualTrade;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIPlayerInRangeScrollableList extends ScrollableTableList implements Observer {
   private PlayerInput input;

   public GUIPlayerInRangeScrollableList(InputState var1, GUIAncor var2, PlayerInput var3) {
      super(var1, 100.0F, 100.0F, var2);
      this.getState().getController().sectorChangeObservable.addObserver(this);
      this.input = var3;
   }

   public void cleanUp() {
      this.getState().getController().sectorChangeObservable.deleteObserver(this);
      super.cleanUp();
   }

   public void onInit() {
      super.onInit();
   }

   public void initColumns() {
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MANUALTRADE_GUIPLAYERINRANGESCROLLABLELIST_0, 2.0F, new Comparator() {
         public int compare(PlayerState var1, PlayerState var2) {
            return var1.getName().compareToIgnoreCase(var2.getName());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MANUALTRADE_GUIPLAYERINRANGESCROLLABLELIST_1, 3.0F, new Comparator() {
         public int compare(PlayerState var1, PlayerState var2) {
            return var1.getFactionName().compareToIgnoreCase(var2.getFactionName());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MANUALTRADE_GUIPLAYERINRANGESCROLLABLELIST_2, 56, new Comparator() {
         public int compare(PlayerState var1, PlayerState var2) {
            return 0;
         }
      });
   }

   protected Collection getElementList() {
      Map var1 = this.getState().getOnlinePlayersLowerCaseMap();
      ObjectArrayList var2 = new ObjectArrayList(var1.size());
      Iterator var4 = var1.values().iterator();

      while(var4.hasNext()) {
         PlayerState var3;
         if ((var3 = (PlayerState)var4.next()) != this.getState().getPlayer() && !this.getState().getPlayer().isInTestSector() && !var3.isInTestSector() && Sector.isNeighbor(var3.getCurrentSector(), this.getState().getPlayer().getCurrentSector())) {
            var2.add(var3);
         }
      }

      return var2;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      this.getState().getGameState().getFactionManager();
      this.getState().getGameState().getCatalogManager();
      this.getState().getPlayer();
      Iterator var8 = var2.iterator();

      while(var8.hasNext()) {
         final PlayerState var3 = (PlayerState)var8.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         ScrollableTableList.GUIClippedRow var6;
         (var6 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         var4.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         var4.setTextSimple(var3.getName());
         var5.setTextSimple(new Object() {
            public String toString() {
               return var3.getFactionName();
            }
         });

         assert !var4.getText().isEmpty();

         assert !var5.getText().isEmpty();

         GUIAncor var10 = new GUIAncor(this.getState(), 50.0F, (float)this.columnsHeight);
         GUITextButton var11;
         (var11 = new GUITextButton(this.getState(), 50, 20, GUITextButton.ColorPalette.CANCEL, new Object() {
            public String toString() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MANUALTRADE_GUIPLAYERINRANGESCROLLABLELIST_3;
            }
         }, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse() && GUIPlayerInRangeScrollableList.this.getState().getPlayer().getNetworkObject().isAdminClient.get()) {
                  GUIPlayerInRangeScrollableList.this.pressedTrade(var3);
               }

            }

            public boolean isOccluded() {
               return false;
            }
         })).setPos(0.0F, 2.0F, 0.0F);
         var10.attach(var11);
         GUIPlayerInRangeScrollableList.WeaponRow var9;
         (var9 = new GUIPlayerInRangeScrollableList.WeaponRow(this.getState(), var3, new GUIElement[]{var6, var7, var10})).onInit();
         var1.addWithoutUpdate(var9);
      }

      var1.updateDim();
   }

   public boolean isPlayerAdmin() {
      return this.getState().getPlayer().getNetworkObject().isAdminClient.get();
   }

   public boolean canEdit(CatalogPermission var1) {
      return var1.ownerUID.toLowerCase(Locale.ENGLISH).equals(this.getState().getPlayer().getName().toLowerCase(Locale.ENGLISH)) || this.isPlayerAdmin();
   }

   public WeaponAssignControllerManager getAssignWeaponControllerManager() {
      return this.getPlayerGameControlManager().getWeaponControlManager();
   }

   public InShipControlManager getInShipControlManager() {
      return this.getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager();
   }

   public PlayerGameControlManager getPlayerGameControlManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   protected void pressedTrade(final PlayerState var1) {
      if (!this.getState().getPlayer().activeManualTrades.isEmpty()) {
         (new PlayerGameOkCancelInput("PlayerStatisticsPanel_PLAYER_ADMIN_OPTIONS", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MANUALTRADE_GUIPLAYERINRANGESCROLLABLELIST_4, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MANUALTRADE_GUIPLAYERINRANGESCROLLABLELIST_5) {
            public boolean isOccluded() {
               return false;
            }

            public void onDeactivate() {
            }

            public void pressedOK() {
               Iterator var1x = this.getState().getPlayer().activeManualTrades.iterator();

               while(var1x.hasNext()) {
                  ((ManualTrade)var1x.next()).clientCancel(this.getState().getPlayer());
               }

               GUIPlayerInRangeScrollableList.this.startTrade(var1);
               this.deactivate();
            }
         }).activate();
      } else {
         this.startTrade(var1);
      }
   }

   private void startTrade(PlayerState var1) {
      ManualTrade var2 = new ManualTrade(this.getState().getPlayer(), var1);
      this.getState().getController().getClientChannel().getNetworkObject().manualTradeBuffer.add(new RemoteManualTrade(var2, false));
      this.input.deactivate();
   }

   class WeaponRow extends ScrollableTableList.Row {
      public WeaponRow(InputState var2, PlayerState var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
