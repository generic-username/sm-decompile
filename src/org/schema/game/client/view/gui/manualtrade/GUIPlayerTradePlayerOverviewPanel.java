package org.schema.game.client.view.gui.manualtrade;

import org.schema.game.client.controller.PlayerManualTradeOverviewInput;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.input.InputState;

public class GUIPlayerTradePlayerOverviewPanel extends GUIInputPanel implements GUIActiveInterface {
   private PlayerManualTradeOverviewInput dialog;

   public GUIPlayerTradePlayerOverviewPanel(InputState var1, int var2, int var3, PlayerManualTradeOverviewInput var4) {
      super("GUIPlayerTradePlayerOverviewPanel", var1, var2, var3, var4, "Players in Range", "");
      this.setOkButton(false);
      this.dialog = var4;
   }

   public void onInit() {
      super.onInit();
      GUIContentPane var1;
      (var1 = ((GUIDialogWindow)this.background).getMainContentPane()).setTextBoxHeightLast(30);
      GUIPlayerInRangeScrollableList var2;
      (var2 = new GUIPlayerInRangeScrollableList(this.getState(), var1.getContent(0), this.dialog)).onInit();
      var1.getContent(0).attach(var2);
   }

   public boolean isActive() {
      return (this.getState().getController().getPlayerInputs().isEmpty() || ((DialogInterface)this.getState().getController().getPlayerInputs().get(this.getState().getController().getPlayerInputs().size() - 1)).getInputPanel() == this) && super.isActive();
   }
}
