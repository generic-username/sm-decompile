package org.schema.game.client.view.gui.rules;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.text.DateFormat;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.controller.EntityTrackingChangedListener;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;
import org.schema.schine.network.objects.Sendable;

public class GUITrackingList extends ScrollableTableList implements EntityTrackingChangedListener {
   private GUIActiveInterface active;
   List ents = new ObjectArrayList();
   boolean first = true;

   public GUITrackingList(GameClientState var1, GUIElement var2, GUIActiveInterface var3) {
      super(var1, 100.0F, 100.0F, var2);
      this.active = var3;
      var1.getController().addEntityTrackingListener(this);
   }

   public void cleanUp() {
      ((GameClientState)this.getState()).getController().removeEntityTrackingListener(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RULES_GUITRACKINGLIST_0, 1.0F, new Comparator() {
         public int compare(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2) {
            return var1.getName().toLowerCase(Locale.ENGLISH).compareTo(var2.getName().toLowerCase(Locale.ENGLISH));
         }
      }, true);
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RULES_GUITRACKINGLIST_1, 0.5F, new Comparator() {
         public int compare(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2) {
            return var1.getType().getName().toLowerCase(Locale.ENGLISH).compareTo(var2.getType().getName().toLowerCase(Locale.ENGLISH));
         }
      }, true);
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RULES_GUITRACKINGLIST_2, 1.0F, new Comparator() {
         public int compare(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2) {
            return GUITrackingList.this.getSpwner(var1).compareTo(GUITrackingList.this.getSpwner(var2));
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RULES_GUITRACKINGLIST_3, 1.0F, new Comparator() {
         public int compare(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2) {
            return GUITrackingList.this.getLastMod(var1).compareTo(GUITrackingList.this.getLastMod(var2));
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RULES_GUITRACKINGLIST_4, 1.0F, new Comparator() {
         public int compare(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2) {
            return var1.getClientSector().compareTo(var2.getClientSector());
         }
      });
   }

   protected List getElementList() {
      this.ents.clear();
      Iterator var1 = ((GameClientState)this.getState()).getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

      while(var1.hasNext()) {
         Sendable var2;
         if ((var2 = (Sendable)var1.next()) instanceof SimpleTransformableSendableObject && ((SimpleTransformableSendableObject)var2).isTracked()) {
            this.ents.add((SimpleTransformableSendableObject)var2);
         }
      }

      return this.ents;
   }

   public String getSpwner(SimpleTransformableSendableObject var1) {
      return var1 instanceof SegmentController ? ((SegmentController)var1).getSpawner() : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RULES_GUITRACKINGLIST_6;
   }

   public String getLastMod(SimpleTransformableSendableObject var1) {
      return var1 instanceof SegmentController ? ((SegmentController)var1).getLastModifier() : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RULES_GUITRACKINGLIST_5;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      DateFormat.getDateInstance(2, Locale.getDefault());
      Iterator var9 = var2.iterator();

      while(var9.hasNext()) {
         final SimpleTransformableSendableObject var3 = (SimpleTransformableSendableObject)var9.next();
         ScrollableTableList.GUIClippedRow var4 = this.getSimpleRow(new Object() {
            public String toString() {
               return var3.getName();
            }
         }, this.active);
         ScrollableTableList.GUIClippedRow var5 = this.getSimpleRow(new Object() {
            public String toString() {
               return var3.getType().getName();
            }
         }, this.active);
         ScrollableTableList.GUIClippedRow var6 = this.getSimpleRow(new Object() {
            public String toString() {
               return GUITrackingList.this.getSpwner(var3);
            }
         }, this.active);
         ScrollableTableList.GUIClippedRow var7 = this.getSimpleRow(new Object() {
            public String toString() {
               return GUITrackingList.this.getLastMod(var3);
            }
         }, this.active);
         ScrollableTableList.GUIClippedRow var8 = this.getSimpleRow(new Object() {
            public String toString() {
               return var3.getClientSector().toStringPure();
            }
         }, this.active);
         GUITrackingList.RuleRow var10 = new GUITrackingList.RuleRow(this.getState(), var3, new GUIElement[]{var4, var5, var6, var7, var8});
         new GUIAncor(this.getState(), 100.0F, 100.0F);
         var10.onInit();
         var1.addWithoutUpdate(var10);
      }

      var1.updateDim();
      this.first = false;
   }

   protected boolean isFiltered(SimpleTransformableSendableObject var1) {
      return super.isFiltered(var1);
   }

   public void onTrackingChanged() {
      this.flagDirty();
   }

   class RuleRow extends ScrollableTableList.Row {
      public RuleRow(InputState var2, SimpleTransformableSendableObject var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
         this.highlightSelectSimple = true;
         this.setAllwaysOneSelected(true);
      }

      protected void clickedOnRow() {
         super.clickedOnRow();
      }
   }
}
