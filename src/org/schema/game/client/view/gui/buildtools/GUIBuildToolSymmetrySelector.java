package org.schema.game.client.view.gui.buildtools;

import java.util.ArrayList;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.controller.manager.ingame.SymmetryPlanes;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUISettingsElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUIBuildToolSymmetrySelector extends GUISettingsElement {
   private final int mode;
   private GUITextOverlay settingName;
   private GUITextButton button;
   private boolean init;

   public GUIBuildToolSymmetrySelector(InputState var1, int var2) {
      super(var1);
      this.setMouseUpdateEnabled(true);
      this.mode = var2;
      this.settingName = new GUITextOverlay(30, 30, FontLibrary.getBoldArial24(), this.getState());
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      super.drawAttached();
   }

   public void onInit() {
      this.settingName.setText(new ArrayList());
      switch(this.mode) {
      case 1:
         this.settingName.getText().add(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_GUIBUILDTOOLSYMMETRYSELECTOR_0);
         break;
      case 2:
         this.settingName.getText().add(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_GUIBUILDTOOLSYMMETRYSELECTOR_1);
      case 3:
      default:
         break;
      case 4:
         this.settingName.getText().add(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_GUIBUILDTOOLSYMMETRYSELECTOR_2);
      }

      this.settingName.onInit();
      this.button = new GUITextButton(this.getState(), 100, 20, new Object() {
         public String toString() {
            if (GUIBuildToolSymmetrySelector.this.getSymmetryPlanes().getPlaceMode() == GUIBuildToolSymmetrySelector.this.mode) {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_GUIBUILDTOOLSYMMETRYSELECTOR_3;
            } else {
               switch(GUIBuildToolSymmetrySelector.this.mode) {
               case 1:
                  if (GUIBuildToolSymmetrySelector.this.getSymmetryPlanes().isXyPlaneEnabled()) {
                     return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_GUIBUILDTOOLSYMMETRYSELECTOR_8;
                  }

                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_GUIBUILDTOOLSYMMETRYSELECTOR_7;
               case 2:
                  if (GUIBuildToolSymmetrySelector.this.getSymmetryPlanes().isXzPlaneEnabled()) {
                     return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_GUIBUILDTOOLSYMMETRYSELECTOR_4;
                  }

                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_GUIBUILDTOOLSYMMETRYSELECTOR_9;
               case 3:
               default:
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_GUIBUILDTOOLSYMMETRYSELECTOR_10;
               case 4:
                  return GUIBuildToolSymmetrySelector.this.getSymmetryPlanes().isYzPlaneEnabled() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_GUIBUILDTOOLSYMMETRYSELECTOR_6 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_GUIBUILDTOOLSYMMETRYSELECTOR_5;
               }
            }
         }
      }, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (GUIBuildToolSymmetrySelector.this.getSymmetryPlanes().getPlaceMode() == 0) {
                  switch(GUIBuildToolSymmetrySelector.this.mode) {
                  case 1:
                     if (GUIBuildToolSymmetrySelector.this.getSymmetryPlanes().isXyPlaneEnabled()) {
                        GUIBuildToolSymmetrySelector.this.getSymmetryPlanes().setXyPlaneEnabled(false);
                        return;
                     }

                     GUIBuildToolSymmetrySelector.this.getSymmetryPlanes().setPlaceMode(GUIBuildToolSymmetrySelector.this.mode);
                     return;
                  case 2:
                     if (GUIBuildToolSymmetrySelector.this.getSymmetryPlanes().isXzPlaneEnabled()) {
                        GUIBuildToolSymmetrySelector.this.getSymmetryPlanes().setXzPlaneEnabled(false);
                        return;
                     }

                     GUIBuildToolSymmetrySelector.this.getSymmetryPlanes().setPlaceMode(GUIBuildToolSymmetrySelector.this.mode);
                     return;
                  case 4:
                     if (GUIBuildToolSymmetrySelector.this.getSymmetryPlanes().isYzPlaneEnabled()) {
                        GUIBuildToolSymmetrySelector.this.getSymmetryPlanes().setYzPlaneEnabled(false);
                        return;
                     }

                     GUIBuildToolSymmetrySelector.this.getSymmetryPlanes().setPlaceMode(GUIBuildToolSymmetrySelector.this.mode);
                  case 3:
                  default:
                     return;
                  }
               }

               GUIBuildToolSymmetrySelector.this.getSymmetryPlanes().setPlaceMode(0);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.button.getPos().x = 40.0F;
      this.attach(this.button);
      this.init = true;
   }

   protected void doOrientation() {
   }

   public float getHeight() {
      return this.settingName.getHeight();
   }

   public float getWidth() {
      return 100.0F;
   }

   public boolean isPositionCenter() {
      return false;
   }

   public SymmetryPlanes getSymmetryPlanes() {
      PlayerInteractionControlManager var1;
      return (var1 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager()).getInShipControlManager().getShipControlManager().getSegmentBuildController().isTreeActive() ? var1.getInShipControlManager().getShipControlManager().getSegmentBuildController().getSymmetryPlanes() : var1.getSegmentControlManager().getSegmentBuildController().getSymmetryPlanes();
   }
}
