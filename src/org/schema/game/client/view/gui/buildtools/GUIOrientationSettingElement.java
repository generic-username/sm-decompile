package org.schema.game.client.view.gui.buildtools;

import javax.vecmath.Vector4f;
import org.schema.common.FastMath;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.controller.manager.ingame.SegmentBuildController;
import org.schema.game.client.controller.manager.ingame.SegmentControlManager;
import org.schema.game.client.controller.manager.ingame.ship.ShipControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.client.view.gui.GUI3DBlockElement;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredGradientRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUISettingsElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUIOrientationSettingElement extends GUISettingsElement {
   private final GUI3DBlockElement blockPreview;
   private final GUITextOverlay settingNotAvailable;
   private final GUIOverlay leftArrow;
   private final GUIOverlay rightArrow;
   boolean checkError = true;
   boolean checkError2 = true;
   private boolean init;
   private int lastSlot = -1;
   private int lastOrientation;
   private GUIColoredGradientRectangle blockBg;
   private int lastSubSlot;
   private int lastSlab;
   private short lastForced;

   public GUIOrientationSettingElement(InputState var1) {
      super(var1);
      this.setMouseUpdateEnabled(true);
      this.leftArrow = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "tools-16x16-gui-"), this.getState());
      this.rightArrow = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "tools-16x16-gui-"), this.getState());
      this.blockPreview = new GUI3DBlockElement(var1);
      this.settingNotAvailable = new GUITextOverlay(10, 10, this.getState());
      this.settingNotAvailable.setTextSimple("N/A for this block");
      this.blockBg = new GUIColoredGradientRectangle(var1, 52.0F, 52.0F, new Vector4f(0.7F, 0.7F, 0.7F, 0.7F));
      this.blockBg.gradient.set(0.1F, 0.1F, 0.1F, 0.9F);
   }

   public static int getMaxRotation(PlayerInteractionControlManager var0) {
      short var1;
      return (var1 = var0.getSelectedTypeWithSub()) != 0 ? ElementKeyMap.getInfo(var1).getBlockStyle().orientations : 6;
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.checkError2) {
         GlUtil.printGlErrorCritical();
      }

      if (!this.init) {
         this.onInit();
      }

      if (this.checkError2) {
         GlUtil.printGlErrorCritical();
      }

      if (this.lastSlot != this.getPlayerInteractionControlManager().getSelectedSlot() || this.lastSubSlot != this.getPlayerInteractionControlManager().getSelectedSubSlot() || this.lastOrientation != this.getPlayerInteractionControlManager().getBlockOrientation() || this.lastForced != this.getPlayerInteractionControlManager().getForcedSelect() || this.lastSlab != this.getPlayerInteractionControlManager().getBuildToolsManager().slabSize.setting) {
         this.updateText();
         this.lastForced = this.getPlayerInteractionControlManager().getForcedSelect();
         this.lastSlot = this.getPlayerInteractionControlManager().getSelectedSlot();
         this.lastOrientation = this.getPlayerInteractionControlManager().getBlockOrientation();
         this.lastSubSlot = this.getPlayerInteractionControlManager().getSelectedSubSlot();
         this.lastSlab = this.getPlayerInteractionControlManager().getBuildToolsManager().slabSize.setting;
      }

      if (this.checkError2) {
         GlUtil.printGlErrorCritical();
      }

      this.checkError2 = false;
      short var1 = this.getPlayerInteractionControlManager().getSelectedTypeWithSub();
      boolean var2 = true;
      if (var1 <= 0 || ElementKeyMap.getInfo(var1).getBlockStyle() == BlockStyle.NORMAL && ElementKeyMap.getInfo(var1).individualSides < 4 && !ElementKeyMap.getInfo(var1).isOrientatable()) {
         var2 = false;
      }

      if (this.checkError) {
         GlUtil.printGlErrorCritical();
      }

      if (this.checkError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.glPushMatrix();
      this.transform();
      if (this.checkError) {
         GlUtil.printGlErrorCritical();
      }

      this.blockBg.draw();
      if (this.checkError) {
         GlUtil.printGlErrorCritical();
      }

      this.blockPreview.draw();
      if (this.checkError) {
         GlUtil.printGlErrorCritical();
      }

      if (var2) {
         this.leftArrow.draw();
      }

      if (this.checkError) {
         GlUtil.printGlErrorCritical();
      }

      if (var2) {
         this.rightArrow.draw();
      }

      if (this.checkError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.glPopMatrix();
      if (this.checkError) {
         GlUtil.printGlErrorCritical();
      }

      this.checkError = false;
   }

   public void onInit() {
      this.blockPreview.onInit();
      this.leftArrow.setMouseUpdateEnabled(true);
      this.rightArrow.setMouseUpdateEnabled(true);
      this.blockBg.onInit();
      this.leftArrow.setCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.getEventButtonState() && var2.getEventButton() == 0) {
               int var3 = GUIOrientationSettingElement.this.getPlayerInteractionControlManager().getBlockOrientation();
               GUIOrientationSettingElement.this.getPlayerInteractionControlManager().getSelectedTypeWithSub();
               var3 = FastMath.cyclicModulo(var3 - 1, GUIOrientationSettingElement.getMaxRotation(GUIOrientationSettingElement.this.getPlayerInteractionControlManager()));
               GUIOrientationSettingElement.this.getPlayerInteractionControlManager().setBlockOrientation(var3);
               GUIOrientationSettingElement.this.updateText();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.rightArrow.setCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.getEventButtonState() && var2.getEventButton() == 0) {
               int var3 = GUIOrientationSettingElement.this.getPlayerInteractionControlManager().getBlockOrientation();
               GUIOrientationSettingElement.this.getPlayerInteractionControlManager().getSelectedTypeWithSub();
               var3 = (var3 + 1) % GUIOrientationSettingElement.getMaxRotation(GUIOrientationSettingElement.this.getPlayerInteractionControlManager());
               GUIOrientationSettingElement.this.getPlayerInteractionControlManager().setBlockOrientation(var3);
               GUIOrientationSettingElement.this.updateText();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.leftArrow.setSpriteSubIndex(21);
      this.rightArrow.setSpriteSubIndex(20);
      this.settingNotAvailable.getPos().x = 6.0F;
      this.settingNotAvailable.getPos().y = 9.0F;
      this.blockPreview.getPos().x = this.leftArrow.getWidth() + 29.0F;
      this.blockPreview.getPos().y = 24.0F;
      this.blockBg.setPos(this.blockPreview.getPos().x - 26.0F, this.blockPreview.getPos().y - 26.0F, 0.0F);
      this.rightArrow.getPos().x = this.leftArrow.getWidth() + 60.0F;
      this.init = true;
   }

   public SegmentBuildController getActiveBuildController() {
      if (this.getSegmentControlManager().getSegmentBuildController().isTreeActive()) {
         return this.getSegmentControlManager().getSegmentBuildController();
      } else {
         return this.getShipControllerManager().getSegmentBuildController().isTreeActive() ? this.getShipControllerManager().getSegmentBuildController() : null;
      }
   }

   public float getHeight() {
      return this.blockPreview.getHeight() + 12.0F;
   }

   public float getWidth() {
      return this.blockPreview.getWidth() + this.leftArrow.getWidth() + this.rightArrow.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public PlayerInteractionControlManager getPlayerInteractionControlManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager();
   }

   public SegmentControlManager getSegmentControlManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSegmentControlManager();
   }

   public ShipControllerManager getShipControllerManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager();
   }

   private void updateText() {
      int var1 = this.getPlayerInteractionControlManager().getBlockOrientation();
      short var2;
      if ((var2 = this.getPlayerInteractionControlManager().getSelectedTypeWithSub()) > 0) {
         if (ElementKeyMap.getInfo(var2).getBlockStyle() != BlockStyle.NORMAL) {
            this.blockPreview.setBlockType(var2);
            this.blockPreview.setSidedOrientation(0);
            this.blockPreview.setShapeOrientation(var1);
         } else if (ElementKeyMap.getInfo(var2).getIndividualSides() > 3) {
            this.blockPreview.setBlockType(var2);
            this.blockPreview.setShapeOrientation(0);
            this.blockPreview.setSidedOrientation(var1);
         } else if (ElementKeyMap.getInfo(var2).orientatable) {
            this.blockPreview.setBlockType(var2);
            this.blockPreview.setShapeOrientation(0);
            this.blockPreview.setSidedOrientation(var1);
         } else {
            this.getPlayerInteractionControlManager().setBlockOrientation(ElementKeyMap.getInfo(var2).getDefaultOrientation());
            this.blockPreview.setBlockType(var2);
            this.blockPreview.setShapeOrientation(0);
            this.blockPreview.setSidedOrientation(0);
         }
      } else {
         this.blockPreview.setBlockType((short)0);
         this.blockPreview.setShapeOrientation(0);
         this.blockPreview.setSidedOrientation(0);
      }
   }
}
