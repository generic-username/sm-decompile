package org.schema.game.client.view.gui.buildtools;

import com.bulletphysics.collision.dispatch.CollisionWorld.ClosestRayResultCallback;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerBlockTypeDropdownInput;
import org.schema.game.client.controller.PlayerGameDropDownInput;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.PlayerThreadProgressInput;
import org.schema.game.client.controller.manager.ingame.BuildSelectionCopy;
import org.schema.game.client.controller.manager.ingame.BuildToolsManager;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.controller.manager.ingame.SymmetryPlanes;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.buildhelper.BuildHelper;
import org.schema.game.client.view.buildhelper.BuildHelperClass;
import org.schema.game.client.view.buildhelper.BuildHelperVar;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.client.view.gui.GUISizeSettingSelectorScroll;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.game.common.data.physics.PhysicsExt;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUICheckBox;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUISettingsListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIToolTip;
import org.schema.schine.graphicsengine.forms.gui.TooltipProvider;
import org.schema.schine.graphicsengine.forms.gui.newgui.settingsnew.GUIScrollSettingSelector;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.resource.FileExt;

public class BuildToolsPanel extends GUIElement implements TooltipProvider {
   public static int HEIGHT = 734;
   public static int HEIGHT_UNEXP = 390;
   private GUIAncor background;
   private GUIAncor buildMode;
   private GUIAncor nothing;
   private GUIScrollablePanel scrollPanel;
   private GUITextOverlay altText;
   private GUITextOverlay inModeText;
   private boolean firstDraw = true;
   private GUIElementList generalList;
   private boolean astronaut;
   private short lastReplaceTypeSelected;
   private Int2ObjectOpenHashMap mm = new Int2ObjectOpenHashMap();
   public static byte blueprintPlacementSetting = 0;

   public BuildToolsPanel(InputState var1, boolean var2) {
      super(var1);
      this.background = new GUIAncor(this.getState(), 300.0F, (float)HEIGHT);
      this.astronaut = var2;
   }

   private BuildHelper getBuildHelperCached(Class var1, SimpleTransformableSendableObject var2) {
      Object2ObjectOpenHashMap var3 = (Object2ObjectOpenHashMap)this.mm.get(var2.getId());
      BuildHelper var4 = null;
      if (var3 != null) {
         var4 = (BuildHelper)var3.get(var1);
      }

      if (var4 == null) {
         try {
            var4 = (BuildHelper)var1.getConstructor(Transformable.class).newInstance(var2);
            if (var3 == null) {
               var3 = new Object2ObjectOpenHashMap();
               this.mm.put(var2.getId(), var3);
            }

            var3.put(var1, var4);
         } catch (NoSuchMethodException var5) {
            var5.printStackTrace();
         } catch (SecurityException var6) {
            var6.printStackTrace();
         } catch (InstantiationException var7) {
            var7.printStackTrace();
         } catch (IllegalAccessException var8) {
            var8.printStackTrace();
         } catch (IllegalArgumentException var9) {
            var9.printStackTrace();
         } catch (InvocationTargetException var10) {
            var10.printStackTrace();
         }
      } else {
         var4.reset();
      }

      return var4;
   }

   public void attach(GUIElement var1) {
      this.background.attach(var1);
   }

   public void detach(GUIElement var1) {
      this.background.detach(var1);
   }

   public float getHeight() {
      return this.background.getHeight();
   }

   public float getWidth() {
      return this.background.getWidth();
   }

   public boolean isInside() {
      return this.background.isInside();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void cleanUp() {
      this.background.cleanUp();
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      String var1;
      if (!(var1 = "press " + KeyboardMappings.BUILD_MODE_FIX_CAM.getKeyChar() + "\nto enter advanced\nbuild mode\n\n").equals(this.altText.getText().get(0))) {
         this.altText.getText().set(0, var1);
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.background.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.altText = new GUITextOverlay(100, 30, FontLibrary.getBoldArial14White(), this.getState());
      this.altText.setText(new ArrayList());
      this.altText.getText().add(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_14, KeyboardMappings.BUILD_MODE_FIX_CAM.getKeyChar()));
      this.altText.getText().add(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_15, KeyboardMappings.JUMP_TO_MODULE.getKeyChar()));
      this.altText.getText().add(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_20, KeyboardMappings.BUILD_MODE_FAST_MOVEMENT.getKeyChar()));
      this.altText.getText().add(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_25, KeyboardMappings.CHANGE_SHIP_MODE.getKeyChar()));
      this.altText.getText().add(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_36);
      this.buildMode = new GUIAncor(this.getState());
      this.inModeText = new GUITextOverlay(100, 30, FontLibrary.getBoldArial18(), this.getState());
      this.inModeText.setText(new ArrayList());
      this.inModeText.getText().add("");
      this.inModeText.getPos().y = 100.0F;
      this.scrollPanel = new GUIScrollablePanel(this.background.getWidth(), this.background.getHeight(), this.getState());
      this.scrollPanel.getPos().set(0.0F, 0.0F, 0.0F);
      this.generalList = new GUIElementList(this.getState());
      this.generalList.setCallback(this.getBuildToolsManager());
      this.buildMode.attach(this.generalList);
      this.buildMode.attach(this.inModeText);
      this.nothing = new GUIAncor(this.getState(), 1.0F, 1.0F);
      this.scrollPanel.setContent(this.altText);
      this.generalList.attach(this.inModeText);
      this.background.onInit();
      if (this.astronaut) {
         this.reconstructListAstronaut();
      } else {
         this.reconstructListFull();
      }

      super.attach(this.background);
      this.background.attach(this.scrollPanel);
      this.doOrientation();
      this.background.getPos().set((float)(GLFrame.getWidth() - 270), 64.0F, 0.0F);
      this.background.setMouseUpdateEnabled(true);
      this.firstDraw = false;
   }

   public BuildToolsManager getBuildToolsManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getBuildToolsManager();
   }

   public PlayerInteractionControlManager getPlayerInteractionControlManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager();
   }

   public SymmetryPlanes getSymmetryPlanes() {
      PlayerInteractionControlManager var1;
      return (var1 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager()).getInShipControlManager().getShipControlManager().getSegmentBuildController().isTreeActive() ? var1.getInShipControlManager().getShipControlManager().getSegmentBuildController().getSymmetryPlanes() : var1.getSegmentControlManager().getSegmentBuildController().getSymmetryPlanes();
   }

   public void reconstructListAstronaut() {
      this.generalList.clear();
      GUISettingsListElement var1 = new GUISettingsListElement(this.getState(), 90, 70, "Orientation", new GUIOrientationSettingElement(this.getState()), false, false);
      this.generalList.add((GUIListElement)var1);
      Vector3f var10000 = this.generalList.getPos();
      var10000.y += 400.0F;
   }

   public void reconstructListFull() {
      this.generalList.clear();
      GUIAncor var1 = new GUIAncor(this.getState(), 240.0F, 30.0F);
      GUIAncor var2 = new GUIAncor(this.getState(), 240.0F, 30.0F);
      GUIAncor var3 = new GUIAncor(this.getState(), 240.0F, 90.0F);
      GUIAncor var4 = new GUIAncor(this.getState(), 240.0F, 30.0F);
      GUITextButton var5 = new GUITextButton(this.getState(), 100, 20, new Object() {
         public String toString() {
            return BuildToolsPanel.this.getBuildToolsManager().isCopyMode() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_0 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_1;
         }
      }, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               BuildToolsPanel.this.getBuildToolsManager().setCopyMode(!BuildToolsPanel.this.getBuildToolsManager().isCopyMode());
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }) {
         Vector4f c = new Vector4f(0.7F, 0.0F, 0.0F, 1.0F);

         public void draw() {
            super.draw();
         }

         public Vector4f getBackgroundColorMouseOverlay() {
            return BuildToolsPanel.this.getBuildToolsManager().isCopyMode() ? this.c : super.getBackgroundColorMouseOverlay();
         }

         public Vector4f getBackgroundColor() {
            return BuildToolsPanel.this.getBuildToolsManager().isCopyMode() ? this.c : super.getBackgroundColor();
         }
      };
      GUITextButton var6 = new GUITextButton(this.getState(), 100, 20, new Object() {
         public String toString() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_60;
         }
      }, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               BuildToolsPanel.this.getBuildToolsManager().setSelectMode(BuildToolsPanel.this.getBuildToolsManager().isSelectMode() ? null : new BuildSelectionCopy());
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }) {
         Vector4f c = new Vector4f(0.7F, 0.0F, 0.0F, 1.0F);

         public void draw() {
            super.draw();
         }

         public Vector4f getBackgroundColorMouseOverlay() {
            return BuildToolsPanel.this.getBuildToolsManager().isSelectMode() ? this.c : super.getBackgroundColorMouseOverlay();
         }

         public Vector4f getBackgroundColor() {
            return BuildToolsPanel.this.getBuildToolsManager().isSelectMode() ? this.c : super.getBackgroundColor();
         }
      };
      GUITextButton var7 = new GUITextButton(this.getState(), 140, 20, new Object() {
         public String toString() {
            return BuildToolsPanel.this.getBuildToolsManager().isPasteMode() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_51 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_52;
         }
      }, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               BuildToolsPanel.this.getBuildToolsManager().setPasteMode(!BuildToolsPanel.this.getBuildToolsManager().isPasteMode());
            }

         }
      }) {
         Vector4f c = new Vector4f(0.7F, 0.0F, 0.0F, 1.0F);

         public void draw() {
            if (BuildToolsPanel.this.getBuildToolsManager().canPaste()) {
               super.draw();
            }

         }

         public Vector4f getBackgroundColorMouseOverlay() {
            return BuildToolsPanel.this.getBuildToolsManager().isPasteMode() ? this.c : super.getBackgroundColorMouseOverlay();
         }

         public Vector4f getBackgroundColor() {
            return BuildToolsPanel.this.getBuildToolsManager().isPasteMode() ? this.c : super.getBackgroundColor();
         }
      };
      GUITextButton var8 = new GUITextButton(this.getState(), 100, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_53, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ArrayList var6 = new ArrayList();
               FileExt var7;
               if ((var7 = new FileExt("./templates/")).exists()) {
                  File[] var8;
                  Arrays.sort(var8 = var7.listFiles(), 0, var8.length, new Comparator() {
                     public int compare(File var1, File var2) {
                        return var1.getName().compareTo(var2.getName());
                     }
                  });

                  for(int var3 = 0; var3 < var8.length; ++var3) {
                     File var4;
                     if (!(var4 = var8[var3]).isDirectory() && var4.getName().endsWith(".smtpl")) {
                        GUITextOverlay var5;
                        (var5 = new GUITextOverlay(300, 16, BuildToolsPanel.this.getState())).setTextSimple(var4.getName().substring(0, var4.getName().lastIndexOf(".smtpl")));
                        var5.setUserPointer(var4);
                        var6.add(var5);
                     }
                  }

                  (new PlayerGameDropDownInput("BuildToolsPanel_SELECT_TMP", (GameClientState)BuildToolsPanel.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_17, 16, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_16, var6) {
                     public boolean isOccluded() {
                        return false;
                     }

                     public void onDeactivate() {
                     }

                     public void pressedOK(GUIListElement var1) {
                        File var3 = (File)var1.getContent().getUserPointer();

                        try {
                           BuildToolsPanel.this.getBuildToolsManager().loadCopyArea(var3);
                           this.getState().getController().popupInfoTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_2, var3.getName()), 0.0F);
                           this.deactivate();
                        } catch (IOException var2) {
                           var2.printStackTrace();
                           this.getState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_3, var3.getName()), 0.0F);
                        }
                     }
                  }).activate();
                  return;
               }

               ((GameClientState)BuildToolsPanel.this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_4, 0.0F);
            }

         }
      }) {
         public void draw() {
            super.draw();
         }
      };
      GUITextButton var9 = new GUITextButton(this.getState(), 140, 20, new Object() {
         public String toString() {
            return BuildToolsPanel.this.getBuildToolsManager().canPaste() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_5 : "-";
         }
      }, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerGameTextInput("BuildToolsPanel_SAVE_TMP", (GameClientState)BuildToolsPanel.this.getState(), 50, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_18, "") {
                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) {
                     return null;
                  }

                  public void onFailedTextCheck(String var1) {
                     this.setErrorMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_19, var1));
                  }

                  public boolean isOccluded() {
                     return false;
                  }

                  public void onDeactivate() {
                  }

                  public boolean onInput(String var1) {
                     if (var1.length() > 0) {
                        try {
                           BuildToolsPanel.this.getBuildToolsManager().saveCopyArea(var1);
                           this.getState().getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_6, 0.0F);
                           return true;
                        } catch (IOException var2) {
                           var2.printStackTrace();
                           this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_7, 0.0F);
                        }
                     } else {
                        this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_8, 0.0F);
                     }

                     return false;
                  }
               }).activate();
            }

         }
      }) {
         public void draw() {
            if (BuildToolsPanel.this.getBuildToolsManager().canPaste()) {
               super.draw();
            }

         }
      };
      GUITextButton var10 = new GUITextButton(this.getState(), 130, 20, new Object() {
         public String toString() {
            return BuildToolsPanel.this.getBuildToolsManager().getRemoveFilter() != 0 ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_9, ElementKeyMap.getInfo(BuildToolsPanel.this.getBuildToolsManager().getRemoveFilter()).getName()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_10;
         }
      }, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (BuildToolsPanel.this.getBuildToolsManager().getRemoveFilter() == 0) {
                  GUICheckBox var4 = new GUICheckBox(BuildToolsPanel.this.getState()) {
                     protected void activate() throws StateParameterNotFoundException {
                        BuildToolsPanel.this.getBuildToolsManager().setReplaceRemoveFilter(true);
                     }

                     protected boolean isActivated() {
                        return BuildToolsPanel.this.getBuildToolsManager().isReplaceRemoveFilter();
                     }

                     protected void deactivate() throws StateParameterNotFoundException {
                        BuildToolsPanel.this.getBuildToolsManager().setReplaceRemoveFilter(false);
                     }

                     public void draw() {
                        super.draw();
                     }
                  };
                  PlayerBlockTypeDropdownInput var5;
                  PlayerBlockTypeDropdownInput var3 = var5 = new PlayerBlockTypeDropdownInput("BuildToolsPanel_TYPE", (GameClientState)BuildToolsPanel.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_11) {
                     public void onOk(ElementInformation var1) {
                        BuildToolsPanel.this.lastReplaceTypeSelected = var1.getId();
                        BuildToolsPanel.this.getBuildToolsManager().setRemoveFilter(var1.getId());
                     }
                  };
                  var5.getInputPanel().onInit();
                  if (ElementKeyMap.isValidType(BuildToolsPanel.this.lastReplaceTypeSelected)) {
                     var5.setSelectedUserPointer(ElementKeyMap.getInfoFast(BuildToolsPanel.this.lastReplaceTypeSelected));
                  }

                  GUITextOverlay var6;
                  (var6 = new GUITextOverlay(100, 20, BuildToolsPanel.this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_12);
                  var4.setPos(10.0F, 30.0F, 0.0F);
                  var6.setPos(30.0F, 30.0F, 0.0F);
                  var3.getInputPanel().getContent().attach(var4);
                  var3.getInputPanel().getContent().attach(var6);
                  var3.activate();
                  return;
               }

               BuildToolsPanel.this.getBuildToolsManager().setRemoveFilter((short)0);
            }

         }
      }) {
         Vector4f c = new Vector4f(0.7F, 0.0F, 0.0F, 1.0F);

         public Vector4f getBackgroundColorMouseOverlay() {
            return BuildToolsPanel.this.getBuildToolsManager().getRemoveFilter() != 0 ? this.c : super.getBackgroundColorMouseOverlay();
         }

         public Vector4f getBackgroundColor() {
            return BuildToolsPanel.this.getBuildToolsManager().getRemoveFilter() != 0 ? this.c : super.getBackgroundColor();
         }

         public void draw() {
            if (BuildToolsPanel.this.getBuildToolsManager().getRemoveFilter() != 0) {
               this.setWidth(230);
            } else {
               this.setWidth(130);
            }

            super.draw();
         }
      };
      GUITextButton var11 = new GUITextButton(this.getState(), 100, 20, new Object() {
         public String toString() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_13;
         }
      }, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && BuildToolsPanel.this.getBuildToolsManager().getRemoveFilter() == 0) {
               Vector3f var4 = new Vector3f(Controller.getCamera().getPos());
               Vector3f var7 = new Vector3f(var4);
               Vector3f var3;
               if (Float.isNaN((var3 = new Vector3f(Controller.getCamera().getForward())).x)) {
                  return;
               }

               var3.scale(300.0F);
               var7.add(var3);
               ClosestRayResultCallback var5;
               if ((var5 = ((PhysicsExt)((GameClientState)BuildToolsPanel.this.getState()).getPhysics()).testRayCollisionPoint(var4, var7, false, ((GameClientState)BuildToolsPanel.this.getState()).getCharacter(), (SegmentController)null, true, true, false)) != null && var5.hasHit() && var5 instanceof CubeRayCastResult) {
                  CubeRayCastResult var6 = (CubeRayCastResult)var5;
                  SegmentPiece var8;
                  if (ElementKeyMap.isValidType((var8 = new SegmentPiece(var6.getSegment(), var6.getCubePos())).getType())) {
                     ElementKeyMap.getInfo(var8.getType());
                     BuildToolsPanel.this.getBuildToolsManager().setRemoveFilter(var8.getType());
                     System.err.println("[BUILDTOOLS] set type by pick: " + var8.getType());
                  }
               }
            }

         }
      }) {
         public void draw() {
            if (BuildToolsPanel.this.getBuildToolsManager().getRemoveFilter() == 0) {
               super.draw();
            }

         }
      };
      GUITextButton var12 = new GUITextButton(this.getState(), 100, 20, new Object() {
         public String toString() {
            return BuildToolsPanel.this.getBuildToolsManager().buildHelperReplace ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_21 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_22;
         }
      }, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               BuildToolsPanel.this.getBuildToolsManager().buildHelperReplace = !BuildToolsPanel.this.getBuildToolsManager().buildHelperReplace;
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }) {
         Vector4f c = new Vector4f(0.7F, 0.0F, 0.0F, 1.0F);

         public Vector4f getBackgroundColorMouseOverlay() {
            return BuildToolsPanel.this.getBuildToolsManager().buildHelperReplace ? this.c : super.getBackgroundColorMouseOverlay();
         }

         public Vector4f getBackgroundColor() {
            return BuildToolsPanel.this.getBuildToolsManager().buildHelperReplace ? this.c : super.getBackgroundColor();
         }

         public void draw() {
            if (BuildToolsPanel.this.getBuildToolsManager().getBuildHelper() != null) {
               super.draw();
            }

         }
      };
      GUITextButton var13 = new GUITextButton(this.getState(), 100, 20, new Object() {
         public String toString() {
            return BuildToolsPanel.this.getBuildToolsManager().getBuildHelper() != null ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_23 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_24;
         }
      }, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (BuildToolsPanel.this.getBuildToolsManager().getBuildHelper() == null) {
                  PlayerGameDropDownInput var4;
                  (var4 = new PlayerGameDropDownInput("BuildToolsPanel_HELPERS", (GameClientState)BuildToolsPanel.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_26, 24, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_27, BuildToolsPanel.this.getHelperList()) {
                     public boolean isOccluded() {
                        return false;
                     }

                     public void pressedOK(GUIListElement var1) {
                        this.deactivate();
                        if (BuildToolsPanel.this.getBuildToolsManager().getBuildHelper() != null) {
                           BuildToolsPanel.this.getBuildToolsManager().getBuildHelper().clean();
                        }

                        Class var6 = (Class)var1.getContent().getUserPointer();

                        try {
                           final BuildHelper var7 = BuildToolsPanel.this.getBuildHelperCached(var6, this.getState().getCurrentPlayerObject());
                           PlayerGameOkCancelInput var2;
                           (var2 = new PlayerGameOkCancelInput("BuildToolsPanel_SETTINGS", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_28, "") {
                              public void onDeactivate() {
                              }

                              public boolean isOccluded() {
                                 return false;
                              }

                              public void pressedOK() {
                                 this.deactivate();
                                 (new Thread(var7)).start();
                                 (new PlayerThreadProgressInput("BuildToolsPanel_CALCULATING", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_29, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_30, var7) {
                                    public void onDeactivate() {
                                       var7.onFinished();
                                       BuildToolsPanel.this.getBuildToolsManager().setBuildHelper(var7);
                                       System.err.println("[CLIENT] using build helper: " + var7);
                                    }

                                    public boolean isOccluded() {
                                       return false;
                                    }

                                    public void pressedOK() {
                                    }
                                 }).activate();
                              }
                           }).getInputPanel().onInit();
                           GUIScrollablePanel var3 = new GUIScrollablePanel(400.0F, 100.0F, this.getState());
                           GUIElementList var8 = BuildToolsPanel.this.getHelperFieldList(var7);
                           var3.setContent(var8);
                           var8.setScrollPane(var3);
                           var2.getInputPanel().getContent().attach(var3);
                           var2.activate();
                        } catch (SecurityException var4) {
                           var4.printStackTrace();
                        } catch (IllegalArgumentException var5) {
                           var5.printStackTrace();
                        }
                     }

                     public void onDeactivate() {
                     }
                  }).getInputPanel().onInit();
                  GUICheckBox var5 = new GUICheckBox(BuildToolsPanel.this.getState()) {
                     protected boolean isActivated() {
                        return BuildToolsPanel.this.getBuildToolsManager().buildHelperReplace;
                     }

                     protected void deactivate() throws StateParameterNotFoundException {
                        BuildToolsPanel.this.getBuildToolsManager().buildHelperReplace = false;
                     }

                     protected void activate() throws StateParameterNotFoundException {
                        BuildToolsPanel.this.getBuildToolsManager().buildHelperReplace = true;
                     }
                  };
                  GUITextOverlay var3;
                  (var3 = new GUITextOverlay(300, 20, BuildToolsPanel.this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_31);
                  if (GUIElement.isNewHud()) {
                     var5.getPos().x = 5.0F;
                     var5.getPos().y = 20.0F;
                     var3.getPos().x = 30.0F;
                     var3.getPos().y = 20.0F;
                  } else {
                     var5.getPos().y = 64.0F;
                     var3.getPos().x = 40.0F;
                     var3.getPos().y = 70.0F;
                  }

                  var4.getInputPanel().getContent().attach(var5);
                  var4.getInputPanel().getContent().attach(var3);
                  var4.activate();
                  return;
               }

               BuildToolsPanel.this.getBuildToolsManager().getBuildHelper().clean();
               BuildToolsPanel.this.getBuildToolsManager().setBuildHelper((BuildHelper)null);
            }

         }
      }) {
         Vector4f c = new Vector4f(0.7F, 0.0F, 0.0F, 1.0F);

         public Vector4f getBackgroundColorMouseOverlay() {
            return BuildToolsPanel.this.getBuildToolsManager().getBuildHelper() != null ? this.c : super.getBackgroundColorMouseOverlay();
         }

         public Vector4f getBackgroundColor() {
            return BuildToolsPanel.this.getBuildToolsManager().getBuildHelper() != null ? this.c : super.getBackgroundColor();
         }

         public void draw() {
            super.draw();
         }
      };
      GUITextButton var14 = new GUITextButton(this.getState(), 50, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_32, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               BuildToolsPanel.this.getBuildToolsManager().undo();
            }

         }
      }) {
         public void draw() {
            if (BuildToolsPanel.this.getBuildToolsManager().canUndo()) {
               super.draw();
            }

         }

         public boolean isActive() {
            return super.isActive() && !BuildToolsPanel.this.getBuildToolsManager().isUndoRedoOnCooldown();
         }
      };
      GUITextButton var15 = new GUITextButton(this.getState(), 50, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_33, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               BuildToolsPanel.this.getBuildToolsManager().redo();
            }

         }
      }) {
         public void draw() {
            if (BuildToolsPanel.this.getBuildToolsManager().canRedo()) {
               super.draw();
            }

         }

         public boolean isActive() {
            return super.isActive() && !BuildToolsPanel.this.getBuildToolsManager().isUndoRedoOnCooldown();
         }
      };
      GUITextButton var16 = new GUITextButton(this.getState(), 220, 20, new Object() {
         public String toString() {
            return BuildToolsPanel.this.getBuildToolsManager().getCreateDockingModeMsg();
         }
      }, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (!BuildToolsPanel.this.getBuildToolsManager().isInCreateDockingMode()) {
                  BuildToolsPanel.this.getBuildToolsManager().startCreateDockingMode();
                  return;
               }

               BuildToolsPanel.this.getBuildToolsManager().cancelCreateDockingMode();
            }

         }
      }) {
         public void draw() {
            super.draw();
         }

         public boolean isActive() {
            return super.isActive();
         }
      };
      var1.attach(var16);
      var2.attach(var14);
      var15.setPos(80.0F, 0.0F, 0.0F);
      var2.attach(var15);
      var5.setPos(0.0F, 30.0F, 0.0F);
      var6.setPos(0.0F, 60.0F, 0.0F);
      var3.attach(var5);
      var3.attach(var8);
      var3.attach(var6);
      var7.setPos(105.0F, 0.0F, 0.0F);
      var9.setPos(105.0F, 30.0F, 0.0F);
      var3.attach(var7);
      var3.attach(var9);
      var4.attach(var10);
      var4.attach(var11);
      var11.setPos(var10.getPos().x + var10.getWidth() + 5.0F, var10.getPos().y, 0.0F);
      GUIAncor var20 = new GUIAncor(this.getState(), 200.0F, 60.0F);
      GUISettingsListElement var22 = new GUISettingsListElement(this.getState(), 100, 30, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_34, new GUICheckBox(this.getState()) {
         public GUIToolTip initToolTip() {
            return EngineSettings.DRAW_TOOL_TIPS.isOn() ? new GUIToolTip(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_35, this) : null;
         }

         protected void activate() throws StateParameterNotFoundException {
            BuildToolsPanel.this.getBuildToolsManager().add = false;
         }

         protected void deactivate() throws StateParameterNotFoundException {
            BuildToolsPanel.this.getBuildToolsManager().add = true;
         }

         protected boolean isActivated() {
            return !BuildToolsPanel.this.getBuildToolsManager().add;
         }
      }, false, false);
      GUISettingsListElement var24 = new GUISettingsListElement(this.getState(), 100, 30, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_37, new GUICheckBox(this.getState()) {
         protected void activate() throws StateParameterNotFoundException {
            BuildToolsPanel.this.getBuildToolsManager().showCenterOfMass = true;
         }

         protected void deactivate() throws StateParameterNotFoundException {
            BuildToolsPanel.this.getBuildToolsManager().showCenterOfMass = false;
         }

         protected boolean isActivated() {
            return BuildToolsPanel.this.getBuildToolsManager().showCenterOfMass;
         }

         public GUIToolTip initToolTip() {
            return EngineSettings.DRAW_TOOL_TIPS.isOn() ? new GUIToolTip(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_38, this) : null;
         }
      }, false, false);
      GUISettingsListElement var25 = new GUISettingsListElement(this.getState(), 50, 30, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_39, new GUICheckBox(this.getState()) {
         protected void activate() throws StateParameterNotFoundException {
            BuildToolsPanel.this.getBuildToolsManager().lighten = true;
         }

         protected void deactivate() throws StateParameterNotFoundException {
            BuildToolsPanel.this.getBuildToolsManager().lighten = false;
         }

         protected boolean isActivated() {
            return BuildToolsPanel.this.getBuildToolsManager().lighten;
         }

         public GUIToolTip initToolTip() {
            return EngineSettings.DRAW_TOOL_TIPS.isOn() ? new GUIToolTip(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_40, this) : null;
         }
      }, false, false);
      GUISettingsListElement var26 = new GUISettingsListElement(this.getState(), 100, 30, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_54, new GUICheckBox(this.getState()) {
         protected void activate() throws StateParameterNotFoundException {
            BuildToolsPanel.this.getBuildToolsManager().buildInfo = true;
         }

         protected void deactivate() throws StateParameterNotFoundException {
            BuildToolsPanel.this.getBuildToolsManager().buildInfo = false;
         }

         protected boolean isActivated() {
            return BuildToolsPanel.this.getBuildToolsManager().buildInfo;
         }

         public GUIToolTip initToolTip() {
            return EngineSettings.DRAW_TOOL_TIPS.isOn() ? new GUIToolTip(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_55, this) : null;
         }
      }, false, false);
      var20.attach(var22);
      var25.getPos().x = 150.0F;
      var20.attach(var25);
      var20.attach(var24);
      var20.attach(var26);
      var24.getPos().y = 20.0F;
      var26.getPos().y = 40.0F;
      this.generalList.add(new GUIListElement(var20, var20, this.getState()));
      this.generalList.add((GUIListElement)(new GUISettingsListElement(this.getState(), 30, 28, "X", new GUISizeSettingSelectorScroll(this.getState(), this.getBuildToolsManager().width), false, false)));
      this.generalList.add((GUIListElement)(new GUISettingsListElement(this.getState(), 30, 28, "Y", new GUISizeSettingSelectorScroll(this.getState(), this.getBuildToolsManager().height), false, false)));
      this.generalList.add((GUIListElement)(new GUISettingsListElement(this.getState(), 30, 35, "Z", new GUISizeSettingSelectorScroll(this.getState(), this.getBuildToolsManager().depth), false, false)));
      this.generalList.add((GUIListElement)(new GUISettingsListElement(this.getState(), 30, 40, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_41, new GUISizeSettingSelectorScroll(this.getState(), this.getBuildToolsManager().slabSize), false, false) {
         public void draw() {
            short var1;
            if (ElementKeyMap.isValidType(var1 = BuildToolsPanel.this.getPlayerInteractionControlManager().getSelectedTypeWithSub()) && ElementKeyMap.getInfoFast(var1).slabIds != null && (BuildToolsPanel.this.getPlayerInteractionControlManager().isMultiSlot() || ElementKeyMap.getInfoFast(var1).slab == 0 && ElementKeyMap.getInfoFast(var1).blocktypeIds == null && ElementKeyMap.getInfoFast(var1).getSourceReference() == 0) && ElementKeyMap.getInfoFast(var1).blockStyle == BlockStyle.NORMAL) {
               super.draw();
            }

         }
      }));
      final GUISettingsListElement var21 = new GUISettingsListElement(this.getState(), 90, 70, "Orientation", new GUIOrientationSettingElement(this.getState()), false, false);
      final GUITextOverlay var23;
      (var23 = new GUITextOverlay(100, 40, FontLibrary.getBoldArial12Yellow(), this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_42);
      var20 = new GUIAncor(this.getState(), 90.0F, 70.0F) {
         public void draw() {
            super.draw();
            if (BuildToolsPanel.this.getBuildToolsManager().isPasteMode()) {
               var23.draw();
            } else {
               var21.draw();
            }
         }
      };
      this.generalList.add(new GUIListElement(var20, var20, this.getState()));
      this.generalList.add(new GUIListElement(var2, var2, this.getState()));
      this.generalList.add(new GUIListElement(var3, var3, this.getState()));
      this.generalList.add(new GUIListElement(var1, var1, this.getState()));
      GUITextOverlay var17;
      (var17 = new GUITextOverlay(100, 40, FontLibrary.getBoldArial18(), this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_43);
      this.generalList.add(new GUIListElement(var17, var17, this.getState()));
      this.generalList.add((GUIListElement)(new GUISettingsListElement(this.getState(), 60, 30, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_44, new GUIBuildToolSymmetrySelector(this.getState(), 1), false, false)));
      this.generalList.add((GUIListElement)(new GUISettingsListElement(this.getState(), 60, 30, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_45, new GUIBuildToolSymmetrySelector(this.getState(), 2), false, false)));
      this.generalList.add((GUIListElement)(new GUISettingsListElement(this.getState(), 60, 30, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_46, new GUIBuildToolSymmetrySelector(this.getState(), 4), false, false)));
      this.generalList.add((GUIListElement)(new GUISettingsListElement(this.getState(), 100, 34, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_47, new GUICheckBox(this.getState()) {
         protected void activate() throws StateParameterNotFoundException {
            BuildToolsPanel.this.getSymmetryPlanes().setExtraDist(0);
         }

         protected void deactivate() throws StateParameterNotFoundException {
            BuildToolsPanel.this.getSymmetryPlanes().setExtraDist(1);
         }

         protected boolean isActivated() {
            return BuildToolsPanel.this.getSymmetryPlanes().getExtraDist() == 0;
         }

         public GUIToolTip initToolTip() {
            return new GUIToolTip(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_48, this);
         }
      }, false, false)));
      this.generalList.add((GUIListElement)(new GUISettingsListElement(this.getState(), 100, 34, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_49, new GUICheckBox(this.getState()) {
         protected void activate() throws StateParameterNotFoundException {
            BuildToolsPanel.this.getSymmetryPlanes().setMirrorCubeShapes(true);
         }

         protected void deactivate() throws StateParameterNotFoundException {
            BuildToolsPanel.this.getSymmetryPlanes().setMirrorCubeShapes(false);
         }

         protected boolean isActivated() {
            return BuildToolsPanel.this.getSymmetryPlanes().isMirrorCubeShapes();
         }

         public GUIToolTip initToolTip() {
            return EngineSettings.DRAW_TOOL_TIPS.isOn() ? new GUIToolTip(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_50, this) : null;
         }
      }, false, false)));
      (var1 = new GUIAncor(this.getState(), 240.0F, 34.0F)).attach(var13);
      var12.getPos().x = 100.0F;
      var1.attach(var12);
      this.generalList.add(new GUIListElement(var1, var1, this.getState()));
      this.generalList.add(new GUIListElement(var4, var4, this.getState()));
      if ((Boolean)EngineSettings.BLUEPRINT_STRUCTURE_BUILD_OPTIONS.getCurrentState()) {
         var1 = new GUIAncor(this.getState(), 240.0F, 40.0F);
         GUISettingsListElement var18 = new GUISettingsListElement(this.getState(), 100, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_56, new GUICheckBox(this.getState()) {
            protected void activate() {
               BuildToolsPanel.blueprintPlacementSetting = 1;
            }

            protected void deactivate() {
               BuildToolsPanel.blueprintPlacementSetting = 0;
            }

            protected boolean isActivated() {
               return BuildToolsPanel.blueprintPlacementSetting == 1;
            }

            public GUIToolTip initToolTip() {
               return new GUIToolTip(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_57, this);
            }
         }, false, false);
         GUISettingsListElement var19 = new GUISettingsListElement(this.getState(), 100, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_58, new GUICheckBox(this.getState()) {
            protected void activate() {
               BuildToolsPanel.blueprintPlacementSetting = 2;
            }

            protected void deactivate() {
               BuildToolsPanel.blueprintPlacementSetting = 0;
            }

            protected boolean isActivated() {
               return BuildToolsPanel.blueprintPlacementSetting == 2;
            }

            public GUIToolTip initToolTip() {
               return new GUIToolTip(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDTOOLSPANEL_59, this);
            }
         }, false, false);
         var1.attach(var18);
         var19.getPos().y = 20.0F;
         var1.attach(var19);
         this.generalList.add(new GUIListElement(var1, var1, this.getState()));
      }

   }

   public GUIElementList getHelperFieldList(final BuildHelper var1) {
      Field[] var2 = var1.getClass().getFields();
      GUIElementList var3 = new GUIElementList(this.getState());

      for(int var4 = 0; var4 < var2.length; ++var4) {
         final Field var5;
         final BuildHelperVar var6;
         if ((var6 = (BuildHelperVar)(var5 = var2[var4]).getAnnotation(BuildHelperVar.class)) != null) {
            var3.add((GUIListElement)(new GUISettingsListElement(this.getState(), 100, 28, var6.name().toString(), new GUIScrollSettingSelector(this.getState(), GUIScrollablePanel.SCROLLABLE_HORIZONTAL, 200, FontLibrary.getBoldArial18()) {
               protected void decSetting() {
                  try {
                     var5.setFloat(var1, var5.getFloat(var1) - 1.0F);
                     this.settingChanged((Object)null);
                  } catch (IllegalArgumentException var1x) {
                  } catch (IllegalAccessException var2) {
                     var2.printStackTrace();
                  }
               }

               protected void incSetting() {
                  try {
                     var5.setFloat(var1, var5.getFloat(var1) + 1.0F);
                     this.settingChanged((Object)null);
                  } catch (IllegalArgumentException var1x) {
                  } catch (IllegalAccessException var2) {
                     var2.printStackTrace();
                  }
               }

               protected float getSettingX() {
                  try {
                     return var5.getFloat(var1);
                  } catch (IllegalArgumentException var1x) {
                     var1x.printStackTrace();
                  } catch (IllegalAccessException var2) {
                     var2.printStackTrace();
                  }

                  return 0.0F;
               }

               protected void setSettingX(float var1x) {
                  try {
                     var5.setFloat(var1, (float)((int)var1x));
                     this.settingChanged((Object)null);
                  } catch (IllegalArgumentException var2) {
                  } catch (IllegalAccessException var3) {
                     var3.printStackTrace();
                  }
               }

               protected float getSettingY() {
                  try {
                     return var5.getFloat(var1);
                  } catch (IllegalArgumentException var1x) {
                     var1x.printStackTrace();
                  } catch (IllegalAccessException var2) {
                     var2.printStackTrace();
                  }

                  return 0.0F;
               }

               protected void setSettingY(float var1x) {
                  try {
                     var5.setFloat(var1, (float)((int)var1x));
                     this.settingChanged((Object)null);
                  } catch (IllegalArgumentException var2) {
                  } catch (IllegalAccessException var3) {
                     var3.printStackTrace();
                  }
               }

               public void settingChanged(Object var1x) {
                  super.settingChanged(var1x);
               }

               public float getMaxX() {
                  return (float)var6.max();
               }

               public float getMaxY() {
                  return (float)var6.max();
               }

               public float getMinX() {
                  return 0.0F;
               }

               public float getMinY() {
                  return 0.0F;
               }

               public boolean isVerticalActive() {
                  return false;
               }
            }, false, false)));
         }
      }

      return var3;
   }

   private ArrayList getHelperList() {
      ArrayList var1 = new ArrayList();
      Iterator var2 = this.getBuildToolsManager().getBuildHelperClasses().iterator();

      while(var2.hasNext()) {
         Class var3 = (Class)var2.next();
         GUIAncor var4 = new GUIAncor(this.getState(), 200.0F, 24.0F);
         GUITextOverlay var5;
         (var5 = new GUITextOverlay(10, 24, this.getState())).setTextSimple(((BuildHelperClass)var3.getAnnotation(BuildHelperClass.class)).name());
         var5.setPos(5.0F, 4.0F, 0.0F);
         var4.attach(var5);
         var1.add(var4);
         var4.setUserPointer(var3);
      }

      return var1;
   }

   public void update(Timer var1) {
      super.update(var1);
      if (Keyboard.isKeyDown(KeyboardMappings.BUILD_MODE_FIX_CAM.getMapping())) {
         this.scrollPanel.setContent(this.buildMode);
      } else if (this.astronaut) {
         this.scrollPanel.setContent(this.nothing);
      } else {
         this.scrollPanel.setContent(this.altText);
      }
   }

   public void drawToolTip() {
      this.generalList.drawToolTip();
   }
}
