package org.schema.game.client.view.gui.buildtools;

import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.KeyboardMappings;

public class BuildConstructionManager {
   private final GameClientState state;
   private final ObjectArrayFIFOQueue queue = new ObjectArrayFIFOQueue();
   private BuildConstructionCommand current;

   public BuildConstructionManager(GameClientState var1) {
      this.state = var1;
   }

   public boolean isCommandQueued() {
      return this.current != null || !this.queue.isEmpty();
   }

   public void update(Timer var1) {
      if (!KeyboardMappings.BUILD_MODE_FIX_CAM.isDown(this.state)) {
         BuildConstructionCommand.issued = false;
      }

      if (this.current != null && this.current.isFinished()) {
         this.current.onEnd(this.state);
         this.current = null;
      }

      if (this.current == null && !this.queue.isEmpty()) {
         this.current = (BuildConstructionCommand)this.queue.dequeue();
         System.err.println("[BUILDCOMMANDQUREUE] starting build command: " + this.current);
         this.current.onStart(this.state);
      }

      if (this.current != null) {
         this.current.update(var1, this.state);
      }

   }

   public void onBuiltBlock(Vector3i var1, Vector3i var2, short var3) {
      if (this.current != null) {
         this.current.onBuiltBlock(var3);
      }

   }

   public void onRemovedBlock(long var1, short var3) {
      if (this.current != null) {
         this.current.onRemovedBlock(var3);
      }

   }

   public void updateOnNotInBuildmode(Timer var1) {
      BuildConstructionCommand.issued = false;
      this.reset();
   }

   private void reset() {
      this.current = null;
      this.queue.clear();
   }

   public void enqueue(BuildConstructionCommand var1) {
      this.queue.enqueue(var1);
   }

   public void onCanceled(BuildConstructionCommand var1) {
      System.err.println("[BUILDCOMMANDQUREUE] command: " + var1 + " canceled. Clearing queue");
      this.queue.clear();
   }

   public BuildConstructionCommand getCurrent() {
      return this.current;
   }

   public boolean canQueue(BuildConstructionCommand var1) {
      return var1.isExecutable(this.state);
   }

   public void resetQueue() {
      this.queue.clear();
      this.current = null;
   }
}
