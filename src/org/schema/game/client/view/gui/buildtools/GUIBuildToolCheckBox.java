package org.schema.game.client.view.gui.buildtools;

import org.schema.game.common.controller.ai.AIConfiguationElements;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUISettingsElement;
import org.schema.schine.input.InputState;

public class GUIBuildToolCheckBox extends GUISettingsElement implements GUICallback {
   private GUIOverlay checkBox;
   private GUIOverlay check;
   private AIConfiguationElements booleanSettings;

   public GUIBuildToolCheckBox(InputState var1, AIConfiguationElements var2) {
      super(var1);
      this.setMouseUpdateEnabled(true);
      this.setCallback(this);
      this.booleanSettings = var2;

      assert var2.getCurrentState() instanceof Boolean;

      this.checkBox = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "tools-16x16-gui-"), this.getState());
      this.check = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "tools-16x16-gui-"), this.getState());
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.getEventButtonState() && var2.getEventButton() == 0) {
         try {
            this.booleanSettings.switchSetting(true);
            return;
         } catch (StateParameterNotFoundException var3) {
            var3.printStackTrace();
            GLFrame.processErrorDialogException(var3, this.getState());
         }
      }

   }

   public boolean isOccluded() {
      return false;
   }

   public void cleanUp() {
   }

   public void draw() {
      GlUtil.glPushMatrix();
      this.transform();
      this.checkMouseInside();
      this.checkBox.draw();
      if (this.booleanSettings.isOn()) {
         this.check.draw();
      }

      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.checkBox.setSpriteSubIndex(18);
      this.check.setSpriteSubIndex(19);
   }

   protected void doOrientation() {
   }

   public float getHeight() {
      return this.checkBox.getHeight();
   }

   public float getWidth() {
      return this.checkBox.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }
}
