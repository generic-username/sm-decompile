package org.schema.game.client.view.gui.buildtools;

import org.schema.common.util.StringTools;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventorySlot;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class BuildConstructionCommand {
   public static boolean issued;
   public final BuildConstructionManager manager;
   public final short type;
   public final int amountToBuild;
   private int amountBuilt;
   private String instruction;
   private boolean canceled;

   public BuildConstructionCommand(BuildConstructionManager var1, short var2, int var3) {
      this.manager = var1;
      this.type = var2;
      this.amountToBuild = var3;
   }

   public String getInstruction() {
      return this.instruction != null ? this.instruction : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDCONSTRUCTIONCOMMAND_0, this.amountToBuild, this.getInfo().getName(), this.amountBuilt, this.amountToBuild);
   }

   public ElementInformation getInfo() {
      return ElementKeyMap.getInfo(this.type);
   }

   public boolean isFinished() {
      return this.canceled || this.amountBuilt >= this.amountToBuild;
   }

   public void cancel() {
      this.manager.onCanceled(this);
      this.canceled = true;
   }

   public void onStart(GameClientState var1) {
      Inventory var2;
      int var3;
      if ((var3 = (var2 = var1.getPlayer().getInventory()).getOverallQuantity(this.type)) < this.amountToBuild) {
         var1.getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDCONSTRUCTIONCOMMAND_1, this.getInfo().getName(), var3, this.amountToBuild));
         this.cancel();
      }

      if ((var3 = var2.getFirstSlot(this.type, true)) < 0) {
         System.err.println("[BUILDCOMMANDQUEUE] canceled " + this + "; no slot found: " + var3);
         this.canceled = true;
      } else {
         InventorySlot var4;
         if ((var4 = var2.getSlot(var3)) == null) {
            this.canceled = true;
         } else {
            int var5 = -1;
            if (var4.isMultiSlot()) {
               for(int var6 = 0; var6 < var4.getSubSlots().size(); ++var6) {
                  if (((InventorySlot)var4.getSubSlots().get(var6)).getType() == this.type) {
                     var5 = var6;
                     break;
                  }
               }
            }

            if (var3 != 0) {
               System.err.println("[BUILDCOMMANDQUEUE] found slot for " + this + "; putting " + var3 + " into first slot");
               if (var2.getSlot(0) != null && !var2.getSlot(0).isEmpty()) {
                  var2.switchSlotsOrCombineClient(0, var3, var5, var2, var4.count());
               } else {
                  var2.switchSlotsOrCombineClient(0, var3, var2, -1);
               }
            }

            PlayerInteractionControlManager var7;
            (var7 = var1.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager()).setSelectedSlotForced(0, this.type);
            issued = true;
            var7.hinderInteraction(300);
         }
      }
   }

   public void update(Timer var1, GameClientState var2) {
      if (!this.canceled && this.amountBuilt < this.amountToBuild) {
         PlayerInteractionControlManager var3;
         if ((var3 = var2.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager()).getSelectedSlot() != 0) {
            this.cancel();
         } else if (!var3.isInAnyStructureBuildMode()) {
            this.cancel();
         } else {
            int var4;
            if ((var4 = var2.getPlayer().getInventory().getOverallQuantity(this.type)) < this.amountToBuild) {
               var2.getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_BUILDTOOLS_BUILDCONSTRUCTIONCOMMAND_2, this.getInfo().getName(), var4, this.amountToBuild));
               this.cancel();
            }
         }
      }
   }

   public boolean isExecutable(GameClientState var1) {
      return var1.getPlayer().getInventory().getOverallQuantity(this.type) >= this.amountToBuild;
   }

   public void onEnd(GameClientState var1) {
   }

   public void onBuiltBlock(short var1) {
      if (var1 == this.type) {
         ++this.amountBuilt;
      }

   }

   public void onRemovedBlock(short var1) {
      if (var1 == this.type) {
         this.amountBuilt = Math.max(0, this.amountBuilt - 1);
      }

   }

   public void setInstruction(String var1) {
      this.instruction = var1;
   }

   public String toString() {
      return "BuildConstructionCommand [type=" + ElementKeyMap.toString(this.type) + ", amountToBuild=" + this.amountToBuild + ", instruction=" + this.instruction + "]";
   }
}
