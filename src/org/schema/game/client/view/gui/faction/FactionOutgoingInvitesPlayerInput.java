package org.schema.game.client.view.gui.faction;

import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionPermission;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyEventInterface;

public class FactionOutgoingInvitesPlayerInput extends PlayerInput {
   private FactionOutgoingInvitesPlayerInput.FactionInvitePanel panel = new FactionOutgoingInvitesPlayerInput.FactionInvitePanel(this.getState(), this, "Outgoing Invites", "");
   private AbstractControlManager caller;

   public FactionOutgoingInvitesPlayerInput(GameClientState var1, AbstractControlManager var2) {
      super(var1);
      this.caller = var2;
      var2.suspend(true);
   }

   public void handleKeyEvent(KeyEventInterface var1) {
   }

   public GUIElement getInputPanel() {
      return this.panel;
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         if (var1.getUserPointer().equals("OK")) {
            this.cancel();
         }

         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            System.err.println("CANCEL");
            this.cancel();
         }
      }

   }

   public void onDeactivate() {
      this.caller.suspend(false);
   }

   public boolean isOccluded() {
      return false;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   class FactionInvitePanel extends GUIInputPanel implements GUICallback {
      public FactionInvitePanel(InputState var2, GUICallback var3, Object var4, Object var5) {
         super("FACTION_INVITE_PANEL", var2, var3, var4, var5);
         this.setOkButton(false);
      }

      public void callback(GUIElement var1, MouseEvent var2) {
         if (var2.pressedLeftMouse()) {
            ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager().openInvitePlayerDialog();
            FactionOutgoingInvitesPlayerInput.this.deactivate();
         }

      }

      public void onInit() {
         super.onInit();
         FactionOutgoingInvitationsPanel var1;
         (var1 = new FactionOutgoingInvitationsPanel(410.0F, 140.0F, this.getState())).onInit();
         this.getContent().attach(var1);
         final GUITextOverlay var2;
         (var2 = new GUITextOverlay(100, 100, this.getState())).setTextSimple("No Permission to invite");
         GUITextButton var3;
         (var3 = new GUITextButton(this.getState(), 120, 30, "Invite Player", this) {
            public void draw() {
               int var1 = ((GameClientState)this.getState()).getPlayer().getFactionId();
               Faction var3;
               if ((var3 = ((GameClientState)this.getState()).getFactionManager().getFaction(var1)) != null) {
                  FactionPermission var2x;
                  if ((var2x = (FactionPermission)var3.getMembersUID().get(((GameClientState)this.getState()).getPlayer().getName())) != null && var2x.hasInvitePermission(var3)) {
                     super.draw();
                  } else {
                     GlUtil.glPushMatrix();
                     this.transform();
                     var2.draw();
                     GlUtil.glPopMatrix();
                  }
               } else {
                  GlUtil.glPushMatrix();
                  this.transform();
                  var2.draw();
                  GlUtil.glPopMatrix();
               }
            }
         }).setUserPointer("INVITE");
         var3.setPos(235.0F, 213.0F, 0.0F);
         this.getBackground().attach(var3);
      }

      public boolean isOccluded() {
         return false;
      }
   }
}
