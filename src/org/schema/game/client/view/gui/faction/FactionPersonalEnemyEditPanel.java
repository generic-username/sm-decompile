package org.schema.game.client.view.gui.faction;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Iterator;
import javax.vecmath.Vector4f;
import org.schema.game.client.controller.manager.ingame.faction.FactionPersonalEnemyDialog;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionPermission;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class FactionPersonalEnemyEditPanel extends GUIInputPanel {
   private final Faction from;
   private final FactionPersonalEnemyDialog dialog;
   ObjectOpenHashSet set = new ObjectOpenHashSet();
   private GUIScrollablePanel p;
   private GUIElementList l;
   private boolean init;

   public FactionPersonalEnemyEditPanel(InputState var1, Faction var2, FactionPersonalEnemyDialog var3) {
      super("FACTION_PERSONAL_ENEMY_EDIT", var1, var3, "Personal Enemies of the faction", "");
      this.from = var2;
      this.dialog = var3;
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
         this.init = true;
      }

      if (this.set.size() != this.from.getPersonalEnemies().size() || !this.set.equals(this.from.getPersonalEnemies())) {
         System.err.println("[CLIENT] updated panel for personal enemies of faction");
         this.updateList();
      }

      super.draw();
   }

   public void onInit() {
      super.onInit();
      this.p = new GUIScrollablePanel(400.0F, 110.0F, this.getState());
      this.l = new GUIElementList(this.getState());
      this.p.setContent(this.l);
      this.updateList();
      GUITextOverlay var1 = new GUITextOverlay(100, 10, this.getState());
      GUITextButton var2;
      (var2 = new GUITextButton(this.getState(), 40, 20, new Vector4f(0.7F, 0.0F, 0.7F, 1.0F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getBoldArial12White(), "Add", this.dialog) {
         public void draw() {
            FactionPermission var1;
            if ((var1 = (FactionPermission)FactionPersonalEnemyEditPanel.this.from.getMembersUID().get(((GameClientState)this.getState()).getPlayer().getName())) != null && var1.hasRelationshipPermission(FactionPersonalEnemyEditPanel.this.from)) {
               super.draw();
            }
         }
      }).setTextPos(5, 1);
      var2.getPos().y = 5.0F;
      this.p.getPos().y = 30.0F;
      this.getContent().attach(var2);
      this.getContent().attach(this.p);
      this.getContent().attach(var1);
      var2.setUserPointer("ADD");
   }

   private void updateList() {
      this.l.clear();
      this.set.clear();
      this.set.addAll(this.from.getPersonalEnemies());
      Iterator var1 = this.set.iterator();

      while(var1.hasNext()) {
         String var2 = (String)var1.next();
         GUIAncor var3 = new GUIAncor(this.getState(), 400.0F, 25.0F);
         GUITextOverlay var4;
         (var4 = new GUITextOverlay(80, 19, this.getState())).getPos().y = 3.0F;
         var4.setTextSimple(var2);
         var3.attach(var4);
         GUITextButton var5;
         (var5 = new GUITextButton(this.getState(), 80, 19, new Vector4f(0.7F, 0.0F, 0.0F, 1.0F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getBoldArial12White(), "Remove", this.dialog) {
            public void draw() {
               FactionPermission var1;
               if ((var1 = (FactionPermission)FactionPersonalEnemyEditPanel.this.from.getMembersUID().get(((GameClientState)this.getState()).getPlayer().getName())) != null && var1.hasRelationshipPermission(FactionPersonalEnemyEditPanel.this.from)) {
                  super.draw();
               }
            }
         }).setUserPointer("remove_" + var2);
         var5.getPos().x = 250.0F;
         var5.getPos().y = 3.0F;
         var3.attach(var5);
         this.l.add(new GUIListElement(var3, var3, this.getState()));
      }

   }
}
