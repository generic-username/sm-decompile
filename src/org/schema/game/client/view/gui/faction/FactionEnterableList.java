package org.schema.game.client.view.gui.faction;

import javax.vecmath.Vector4f;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIEnterableList;
import org.schema.schine.input.InputState;

public class FactionEnterableList extends GUIEnterableList {
   private final GUIColoredRectangle p;

   public FactionEnterableList(InputState var1, GUIElementList var2, GUIElement var3, GUIElement var4, GUIColoredRectangle var5) {
      super(var1, var2, var3, var4);
      this.p = var5;
   }

   protected boolean canClick() {
      return ((GameClientState)this.getState()).getPlayerInputs().isEmpty();
   }

   public void updateIndex(int var1) {
      this.p.getColor().set(var1 % 2 == 0 ? new Vector4f(0.0F, 0.0F, 0.0F, 0.0F) : new Vector4f(0.1F, 0.1F, 0.1F, 0.5F));
      ((FactionEnterableElement)this.collapsedButton).setIndex(var1);
      ((FactionEnterableElement)this.backButton).setIndex(var1);
   }
}
