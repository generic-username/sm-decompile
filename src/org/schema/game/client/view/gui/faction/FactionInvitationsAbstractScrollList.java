package org.schema.game.client.view.gui.faction;

import java.util.Observable;
import java.util.Observer;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.input.InputState;

public abstract class FactionInvitationsAbstractScrollList extends GUIScrollablePanel implements Observer {
   private boolean needsUpdate = true;
   private GUIElementList list;

   public FactionInvitationsAbstractScrollList(float var1, float var2, InputState var3) {
      super(var1, var2, var3);
      ((GameClientState)this.getState()).getPlayer().getFactionController().deleteObserver(this);
      ((GameClientState)this.getState()).getPlayer().getFactionController().addObserver(this);
   }

   public void draw() {
      if (this.needsUpdate) {
         this.updateInvitationList(this.list);
         this.needsUpdate = false;
      }

      super.draw();
   }

   public void onInit() {
      this.list = new GUIElementList(this.getState());
      this.setContent(this.list);
      super.onInit();
   }

   public void update(Observable var1, Object var2) {
      this.needsUpdate = true;
   }

   protected abstract void updateInvitationList(GUIElementList var1);
}
