package org.schema.game.client.view.gui.faction;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionPermission;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public abstract class ScrollableFactionMemberList extends GUIElement implements Observer, GUICallback {
   static DateFormat df;
   private GUIScrollablePanel memberScrollPanel;
   private GUIElementList memberList;
   private int width;
   private int height;
   private GUICallback onSelectCallBack;
   private int lastFaction;
   private boolean needFactionUpdate = true;
   private boolean displayOptions;

   public ScrollableFactionMemberList(InputState var1, int var2, int var3, GUICallback var4, boolean var5) {
      super(var1);
      this.width = var2;
      this.height = var3;
      this.onSelectCallBack = var4;
      this.displayOptions = var5;
      this.getState().getGameState().getFactionManager().addObserver(this);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.getFactionId() != this.lastFaction) {
         this.needFactionUpdate = true;
      }

      if (this.needFactionUpdate) {
         this.updateMemberList();
         this.needFactionUpdate = false;
      }

      this.drawAttached();
   }

   public void onInit() {
      this.memberScrollPanel = new GUIScrollablePanel((float)this.width, (float)this.height, this.getState());
      this.memberList = new GUIElementList(this.getState());
      this.memberList.setCallback(this);
      this.updateMemberList();
      this.memberList.setScrollPane(this.memberScrollPanel);
      this.memberScrollPanel.setContent(this.memberList);
      this.attach(this.memberScrollPanel);
   }

   public abstract int getFactionId();

   public float getHeight() {
      return (float)this.height;
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public float getWidth() {
      return (float)this.width;
   }

   public void update(Observable var1, Object var2) {
      this.needFactionUpdate = true;
   }

   private void updateMemberList() {
      FactionManager var1 = this.getState().getGameState().getFactionManager();
      int var2 = this.getFactionId();
      this.memberList.clear();
      Faction var6;
      if ((var6 = var1.getFaction(var2)) != null) {
         Map var3 = var6.getMembersUID();
         int var4 = 0;
         Iterator var7 = var3.values().iterator();

         while(var7.hasNext()) {
            FactionPermission var5 = (FactionPermission)var7.next();
            ScrollableFactionMemberList.MemberListElement var8;
            (var8 = new ScrollableFactionMemberList.MemberListElement(this.getState(), var6, var5, var4++)).onInit();
            this.memberList.add((GUIListElement)var8);
         }
      }

      this.lastFaction = var2;
   }

   static {
      df = StringTools.getSimpleDateFormat(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_SCROLLABLEFACTIONMEMBERLIST_0, "M/d/yyyy, h:mm a");
   }

   class MemberListElement extends GUIListElement {
      private static final int height = 20;
      private Faction faction;
      private FactionPermission permission;
      private GUIColoredRectangle background;
      private GUITextOverlay name;
      private GUITextOverlay active;
      private GUITextButton options;
      private GUITextOverlay role;
      private Vector4f color;

      public MemberListElement(InputState var2, Faction var3, FactionPermission var4, int var5) {
         super(var2);
         this.faction = var3;
         this.permission = var4;
         this.color = var5 % 2 == 0 ? new Vector4f(0.1F, 0.1F, 0.1F, 1.0F) : new Vector4f(0.3F, 0.3F, 0.3F, 1.0F);
         this.background = new GUIColoredRectangle(this.getState(), (float)ScrollableFactionMemberList.this.width, 20.0F, this.color);
         this.setContent(this.background);
         this.setSelectContent(this.background);
         this.setUserPointer(new Object[]{var3, var4});
      }

      public void draw() {
         if (this.permission.hasPermissionEditPermission(this.faction)) {
            this.role.setColor(1.0F, 0.7F, 0.7F, 1.0F);
         } else if (this.permission.hasKickPermission(this.faction) || this.permission.hasInvitePermission(this.faction)) {
            this.role.setColor(1.0F, 0.9F, 0.9F, 1.0F);
         }

         super.draw();
         this.role.setColor(1.0F, 1.0F, 1.0F, 1.0F);
      }

      public void onInit() {
         this.active = new GUITextOverlay(200, 20, this.getState());
         this.name = new GUITextOverlay(200, 20, this.getState());
         this.name.setTextSimple(this.permission.playerUID);
         Object var1 = new Object() {
            public String toString() {
               return MemberListElement.this.faction.getRoles().getRoles()[MemberListElement.this.permission.role].name;
            }
         };
         Object var2 = new Object() {
            public String toString() {
               return "[" + (MemberListElement.this.permission.isActiveMember() ? "active" : "inactive") + " - " + ScrollableFactionMemberList.df.format(new Date(MemberListElement.this.permission.activeMemberTime)) + "]";
            }
         };
         this.active.setTextSimple(var2);
         this.role = new GUITextOverlay(200, 20, this.getState());
         this.role.setText(new ArrayList());
         this.role.getText().add(var1);
         Vector4f var3;
         (var3 = new Vector4f(this.color)).scale(1.1F);
         this.options = new GUITextButton(this.getState(), 55, 20, var3, new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getRegularArial12WhiteWithoutOutline(), "Options", ScrollableFactionMemberList.this.onSelectCallBack);
         this.options.setTextPos(5, 1);
         this.background.attach(this.name);
         this.background.attach(this.role);
         this.background.attach(this.active);
         if (ScrollableFactionMemberList.this.displayOptions) {
            this.background.attach(this.options);
         }

         this.role.getPos().x = 160.0F;
         this.active.getPos().x = 280.0F;
         this.options.getPos().x = (float)(ScrollableFactionMemberList.this.width - 32) - this.options.getWidth();
         super.onInit();
      }
   }
}
