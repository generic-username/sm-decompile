package org.schema.game.client.view.gui.faction.newfaction;

import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.common.language.Lng;

public class FactionIncomingInvitesPlayerInputNew extends PlayerGameOkCancelInput {
   public FactionIncomingInvitesPlayerInputNew(GameClientState var1) {
      super("FactionIncomingInvitesPlayerInputNew", var1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONINCOMINGINVITESPLAYERINPUTNEW_0, "");
      this.getInputPanel().onInit();
      FactionIncomingInvitationsPanelNew var2;
      (var2 = new FactionIncomingInvitationsPanelNew(this.getInputPanel().getContent(), var1)).onInit();
      this.getInputPanel().getContent().attach(var2);
   }

   public boolean isOccluded() {
      return false;
   }

   public void onDeactivate() {
   }

   public void pressedOK() {
      this.deactivate();
   }
}
