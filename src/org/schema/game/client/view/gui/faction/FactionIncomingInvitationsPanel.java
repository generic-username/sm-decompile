package org.schema.game.client.view.gui.faction;

import java.util.Date;
import java.util.Iterator;
import javax.vecmath.Vector4f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionInvite;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class FactionIncomingInvitationsPanel extends FactionInvitationsAbstractScrollList {
   public FactionIncomingInvitationsPanel(float var1, float var2, InputState var3) {
      super(var1, var2, var3);
   }

   public boolean equals(Object var1) {
      return var1 instanceof FactionIncomingInvitationsPanel;
   }

   protected void updateInvitationList(GUIElementList var1) {
      int var2 = 0;
      var1.clear();
      System.err.println("[GUI] UPDATING LIST: " + ((GameClientState)this.getState()).getPlayer().getFactionController().getInvitesIncoming().size());

      for(Iterator var3 = ((GameClientState)this.getState()).getPlayer().getFactionController().getInvitesIncoming().iterator(); var3.hasNext(); ++var2) {
         FactionInvite var4 = (FactionInvite)var3.next();
         var1.add((GUIListElement)(new FactionIncomingInvitationsPanel.IncomingInvitationListElement(this.getState(), var4, var2)));
      }

   }

   class IncomingInvitationListElement extends GUIListElement implements GUICallback {
      GUIColoredRectangle bg;
      private FactionInvite invite;

      public IncomingInvitationListElement(InputState var2, FactionInvite var3, int var4) {
         super(var2);
         this.invite = var3;
         this.bg = new GUIColoredRectangle(this.getState(), 410.0F, 45.0F, var4 % 2 == 0 ? new Vector4f(0.1F, 0.1F, 0.1F, 1.0F) : new Vector4f(0.2F, 0.2F, 0.2F, 2.0F));
         this.setContent(this.bg);
         this.setSelectContent(this.bg);
      }

      public void callback(GUIElement var1, MouseEvent var2) {
         if (var2.pressedLeftMouse()) {
            if ("ACCEPT".equals(var1.getUserPointer())) {
               ((GameClientState)this.getState()).getPlayer().getFactionController().joinFaction(this.invite.getFactionUID());
               return;
            }

            if ("DECLINE".equals(var1.getUserPointer())) {
               ((GameClientState)this.getState()).getFactionManager().removeFactionInvitationClient(this.invite);
            }
         }

      }

      public boolean isOccluded() {
         return false;
      }

      public void onInit() {
         super.onInit();
         GUITextOverlay var1;
         (var1 = new GUITextOverlay(200, 30, this.getState())).setTextSimple((new Date(this.invite.getDate())).toString());
         Faction var2 = ((GameClientState)this.getState()).getFactionManager().getFaction(this.invite.getFactionUID());
         var1.getText().add("from: " + this.invite.getToPlayerName());
         var1.getText().add(var2 != null ? var2.getName() : "(ERROR)unknown");
         GUITextButton var4;
         (var4 = new GUITextButton(this.getState(), 50, 20, "Decline", this)).setUserPointer("DECLINE");
         GUITextButton var3;
         (var3 = new GUITextButton(this.getState(), 50, 20, "Accept", this)).setUserPointer("ACCEPT");
         this.bg.attach(var1);
         this.bg.attach(var4);
         this.bg.attach(var3);
         var3.getPos().x = 310.0F;
         var4.getPos().x = 250.0F;
      }
   }
}
