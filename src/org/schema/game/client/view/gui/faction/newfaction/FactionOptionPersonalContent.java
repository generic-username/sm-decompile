package org.schema.game.client.view.gui.faction.newfaction;

import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.manager.ingame.faction.FactionDialogNew;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.input.InputState;

public class FactionOptionPersonalContent extends GUIAncor {
   private FactionPanelNew panel;

   public FactionOptionPersonalContent(InputState var1, FactionPanelNew var2) {
      super(var1);
      this.panel = var2;
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFaction();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public void onInit() {
      GUIHorizontalButtonTablePane var1;
      (var1 = new GUIHorizontalButtonTablePane(this.getState(), 2, 2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONPERSONALCONTENT_0, this)).onInit();
      var1.activeInterface = this.panel;
      var1.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONPERSONALCONTENT_1, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && FactionOptionPersonalContent.this.getState().getController().getPlayerInputs().isEmpty()) {
               (new FactionDialogNew(FactionOptionPersonalContent.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONPERSONALCONTENT_2)).activate();
            }

         }

         public boolean isOccluded() {
            return !FactionOptionPersonalContent.this.isActive();
         }
      }, (GUIActivationCallback)null);
      var1.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONPERSONALCONTENT_3, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !FactionOptionPersonalContent.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerGameOkCancelInput("CONFIRM", FactionOptionPersonalContent.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONPERSONALCONTENT_4, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONPERSONALCONTENT_5) {
                  public void onDeactivate() {
                  }

                  public void pressedOK() {
                     System.err.println("[CLIENT][FactionControlManager] leaving Faction");
                     this.getState().getPlayer().getFactionController().leaveFaction();
                     this.deactivate();
                  }
               }).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return FactionOptionPersonalContent.this.getOwnFaction() != null;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      var1.addButton(0, 1, new Object() {
         public String toString() {
            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONPERSONALCONTENT_6, FactionOptionPersonalContent.this.getState().getPlayer().getFactionController().getInvitesIncoming().size());
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !FactionOptionPersonalContent.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new FactionIncomingInvitesPlayerInputNew(FactionOptionPersonalContent.this.getState())).activate();
            }

         }
      }, (GUIActivationCallback)null);
      this.setPos(1.0F, 0.0F, 0.0F);
      this.attach(var1);
   }
}
