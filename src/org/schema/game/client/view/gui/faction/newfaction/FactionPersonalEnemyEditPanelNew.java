package org.schema.game.client.view.gui.faction.newfaction;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Iterator;
import org.schema.game.client.controller.manager.ingame.faction.FactionPersonalEnemyDialog;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionPermission;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.input.InputState;

public class FactionPersonalEnemyEditPanelNew extends GUIInputPanel {
   private final Faction from;
   private final FactionPersonalEnemyDialog dialog;
   ObjectOpenHashSet set = new ObjectOpenHashSet();
   private GUIScrollablePanel p;
   private GUIElementList l;
   private boolean init;
   private GUITextButton add;

   public FactionPersonalEnemyEditPanelNew(InputState var1, Faction var2, FactionPersonalEnemyDialog var3) {
      super("FactionPersonalEnemyEditPanelNew", var1, 420, 180, var3, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPERSONALENEMYEDITPANELNEW_0, "");
      this.from = var2;
      this.dialog = var3;
      this.setCancelButton(false);
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
         this.init = true;
      }

      if (this.set.size() != this.from.getPersonalEnemies().size() || !this.set.equals(this.from.getPersonalEnemies())) {
         System.err.println("[CLIENT] updated panel for personal enemies of faction");
         this.updateList();
      }

      this.add.getPos().set(this.getButtonCancel().getPos().x + 30.0F, this.getButtonCancel().getPos().y, 0.0F);
      super.draw();
   }

   public void onInit() {
      super.onInit();
      this.p = new GUIScrollablePanel(100.0F, 100.0F, this.getContent(), this.getState());
      this.l = new GUIElementList(this.getState());
      this.p.setContent(this.l);
      this.updateList();
      GUITextOverlayTable var1 = new GUITextOverlayTable(100, 10, this.getState());
      this.add = new GUITextButton(this.getState(), (int)this.getButtonOK().getWidth(), (int)this.getButtonOK().getHeight(), GUITextButton.ColorPalette.OK, FontLibrary.getBlenderProMedium14(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPERSONALENEMYEDITPANELNEW_1, this.dialog) {
         public void draw() {
            FactionPermission var1;
            if ((var1 = (FactionPermission)FactionPersonalEnemyEditPanelNew.this.from.getMembersUID().get(((GameClientState)this.getState()).getPlayer().getName())) != null && var1.hasRelationshipPermission(FactionPersonalEnemyEditPanelNew.this.from)) {
               super.draw();
            }
         }
      };
      this.getBackground().attach(this.add);
      this.getContent().attach(this.p);
      this.getContent().attach(var1);
      this.add.setUserPointer("ADD");
   }

   private void updateList() {
      this.l.clear();
      this.set.clear();
      this.set.addAll(this.from.getPersonalEnemies());
      Iterator var1 = this.set.iterator();

      while(var1.hasNext()) {
         String var2 = (String)var1.next();
         GUIAncor var3 = new GUIAncor(this.getState(), 400.0F, 25.0F);
         GUITextOverlayTable var4;
         (var4 = new GUITextOverlayTable(80, 19, this.getState())).getPos().y = 3.0F;
         var4.setTextSimple(var2);
         var3.attach(var4);
         GUITextButton var5;
         (var5 = new GUITextButton(this.getState(), 80, 19, GUITextButton.ColorPalette.CANCEL, FontLibrary.getBlenderProMedium14(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPERSONALENEMYEDITPANELNEW_2, this.dialog) {
            public void draw() {
               FactionPermission var1;
               if ((var1 = (FactionPermission)FactionPersonalEnemyEditPanelNew.this.from.getMembersUID().get(((GameClientState)this.getState()).getPlayer().getName())) != null && var1.hasRelationshipPermission(FactionPersonalEnemyEditPanelNew.this.from)) {
                  super.draw();
               }
            }
         }).setUserPointer("remove_" + var2);
         var5.getPos().x = 250.0F;
         var5.getPos().y = 3.0F;
         var3.attach(var5);
         this.l.add(new GUIListElement(var3, var3, this.getState()));
      }

   }
}
