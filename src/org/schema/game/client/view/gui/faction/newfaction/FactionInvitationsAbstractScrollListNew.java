package org.schema.game.client.view.gui.faction.newfaction;

import java.util.Observable;
import java.util.Observer;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.network.client.ClientState;

public abstract class FactionInvitationsAbstractScrollListNew extends GUIScrollablePanel implements Observer {
   private boolean needsUpdate = true;
   private GUIElementList list;
   private GUIElement p;

   public FactionInvitationsAbstractScrollListNew(ClientState var1, GUIElement var2) {
      super(10.0F, 10.0F, var2, var1);
      this.p = var2;
      ((GameClientState)this.getState()).getPlayer().getFactionController().deleteObserver(this);
      ((GameClientState)this.getState()).getPlayer().getFactionController().addObserver(this);
   }

   public void draw() {
      if (this.needsUpdate) {
         this.updateInvitationList(this.list);
         this.needsUpdate = false;
      }

      this.setWidth(this.p.getWidth());
      this.setHeight(this.p.getHeight());
      super.draw();
   }

   public void onInit() {
      this.list = new GUIElementList(this.getState());
      this.setContent(this.list);
      super.onInit();
   }

   public void update(Observable var1, Object var2) {
      this.needsUpdate = true;
   }

   protected abstract void updateInvitationList(GUIElementList var1);
}
