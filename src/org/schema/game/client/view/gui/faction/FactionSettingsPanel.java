package org.schema.game.client.view.gui.faction;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionPermission;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUICheckBox;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class FactionSettingsPanel extends GUIAncor {
   private GUIScrollablePanel scrollPanel;
   private GUICallback settingsCallback;
   private GUITextButton factionPersonalEnemyButton;

   public FactionSettingsPanel(InputState var1, int var2, int var3, GUICallback var4) {
      super(var1, (float)var2, (float)var3);
      this.settingsCallback = var4;
   }

   public void draw() {
      GameClientState var1;
      Faction var2;
      if ((var1 = (GameClientState)this.getState()).getPlayer().getFactionId() != 0 && (var2 = var1.getFactionManager().getFaction(var1.getPlayer().getFactionId())) != null) {
         FactionPermission var3;
         if ((var3 = (FactionPermission)var2.getMembersUID().get(var1.getPlayer().getName())) != null && var3.hasRelationshipPermission(var2)) {
            super.draw();
            return;
         }

         GlUtil.glPushMatrix();
         this.transform();
         this.factionPersonalEnemyButton.draw();
         GlUtil.glPopMatrix();
      }

   }

   public void onInit() {
      this.scrollPanel = new GUIScrollablePanel(this.width, this.height, this.getState());
      GUIAncor var1 = new GUIAncor(this.getState(), 100.0F, 40.0F);
      GUITextOverlay var2;
      (var2 = new GUITextOverlay(100, 20, this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONSETTINGSPANEL_0);
      GUICheckBox var3 = new GUICheckBox(this.getState()) {
         protected void activate() throws StateParameterNotFoundException {
            int var1 = ((GameClientState)this.getState()).getPlayer().getFactionId();
            Faction var2;
            if ((var2 = ((GameClientState)this.getState()).getFactionManager().getFaction(var1)) != null) {
               var2.clientRequestOpenFaction(((GameClientState)this.getState()).getPlayer(), true);
            } else {
               ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONSETTINGSPANEL_4, 0.0F);
               System.err.println("[CLIENT][FactionSetting] faction not found: " + var1);
            }
         }

         protected void deactivate() throws StateParameterNotFoundException {
            int var1 = ((GameClientState)this.getState()).getPlayer().getFactionId();
            Faction var2;
            if ((var2 = ((GameClientState)this.getState()).getFactionManager().getFaction(var1)) != null) {
               var2.clientRequestOpenFaction(((GameClientState)this.getState()).getPlayer(), false);
            } else {
               ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONSETTINGSPANEL_3, 0.0F);
               System.err.println("[CLIENT][FactionSetting] faction not found: " + var1);
            }
         }

         protected boolean isActivated() {
            int var1 = ((GameClientState)this.getState()).getPlayer().getFactionId();
            Faction var2;
            return (var2 = ((GameClientState)this.getState()).getFactionManager().getFaction(var1)) != null ? var2.isOpenToJoin() : false;
         }
      };
      GUITextOverlay var4;
      (var4 = new GUITextOverlay(100, 20, this.getState())).setTextSimple("Consider neutral enemy");
      GUICheckBox var5 = new GUICheckBox(this.getState()) {
         protected void activate() throws StateParameterNotFoundException {
            int var1 = ((GameClientState)this.getState()).getPlayer().getFactionId();
            Faction var2;
            if ((var2 = ((GameClientState)this.getState()).getFactionManager().getFaction(var1)) != null) {
               var2.clientRequestAttackNeutral(((GameClientState)this.getState()).getPlayer(), true);
            } else {
               ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONSETTINGSPANEL_6, 0.0F);
               System.err.println("[CLIENT][FactionSetting] faction not found: " + var1);
            }
         }

         protected void deactivate() throws StateParameterNotFoundException {
            int var1 = ((GameClientState)this.getState()).getPlayer().getFactionId();
            Faction var2;
            if ((var2 = ((GameClientState)this.getState()).getFactionManager().getFaction(var1)) != null) {
               var2.clientRequestAttackNeutral(((GameClientState)this.getState()).getPlayer(), false);
            } else {
               ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONSETTINGSPANEL_5, 0.0F);
               System.err.println("[CLIENT][FactionSetting] faction not found: " + var1);
            }
         }

         protected boolean isActivated() {
            int var1 = ((GameClientState)this.getState()).getPlayer().getFactionId();
            Faction var2;
            return (var2 = ((GameClientState)this.getState()).getFactionManager().getFaction(var1)) != null ? var2.isAttackNeutral() : false;
         }
      };
      GUITextOverlay var6;
      (var6 = new GUITextOverlay(100, 20, this.getState())).setTextSimple("Declare war on hostile action");
      GUICheckBox var7 = new GUICheckBox(this.getState()) {
         protected void activate() throws StateParameterNotFoundException {
            int var1 = ((GameClientState)this.getState()).getPlayer().getFactionId();
            Faction var2;
            if ((var2 = ((GameClientState)this.getState()).getFactionManager().getFaction(var1)) != null) {
               var2.clientRequestAutoDeclareWar(((GameClientState)this.getState()).getPlayer(), true);
            } else {
               ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONSETTINGSPANEL_2, 0.0F);
               System.err.println("[CLIENT][FactionSetting] faction not found: " + var1);
            }
         }

         protected void deactivate() throws StateParameterNotFoundException {
            int var1 = ((GameClientState)this.getState()).getPlayer().getFactionId();
            Faction var2;
            if ((var2 = ((GameClientState)this.getState()).getFactionManager().getFaction(var1)) != null) {
               var2.clientRequestAutoDeclareWar(((GameClientState)this.getState()).getPlayer(), false);
            } else {
               ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONSETTINGSPANEL_1, 0.0F);
               System.err.println("[CLIENT][FactionSetting] faction not found: " + var1);
            }
         }

         protected boolean isActivated() {
            int var1 = ((GameClientState)this.getState()).getPlayer().getFactionId();
            Faction var2;
            return (var2 = ((GameClientState)this.getState()).getFactionManager().getFaction(var1)) != null ? var2.isAutoDeclareWar() : false;
         }
      };
      GUITextButton var8;
      (var8 = new GUITextButton(this.getState(), 80, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONSETTINGSPANEL_7, this.settingsCallback, ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager())).setUserPointer("POST_NEWS");
      var8.getPos().x = 280.0F;
      GUITextButton var9;
      (var9 = new GUITextButton(this.getState(), 45, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONSETTINGSPANEL_8, this.settingsCallback, ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager()) {
         public void draw() {
            int var1 = ((GameClientState)this.getState()).getPlayer().getFactionId();
            FactionPermission var2;
            Faction var3;
            if ((var3 = ((GameClientState)this.getState()).getFactionManager().getFaction(var1)) != null && (var2 = (FactionPermission)var3.getMembersUID().get(((GameClientState)this.getState()).getPlayer().getName())) != null && var2.hasPermissionEditPermission(var3)) {
               super.draw();
            }

         }
      }).setUserPointer("ROLES");
      var9.getPos().x = 450.0F;
      this.factionPersonalEnemyButton = new GUITextButton(this.getState(), 125, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONSETTINGSPANEL_9, this.settingsCallback, ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager());
      this.factionPersonalEnemyButton.setUserPointer("PERSONAL_ENEMIES");
      this.factionPersonalEnemyButton.getPos().x = 5.0F;
      GUITextButton var10;
      (var10 = new GUITextButton(this.getState(), 110, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONSETTINGSPANEL_10, this.settingsCallback, ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager())).setUserPointer("EDIT_DESC");
      var10.getPos().x = 150.0F;
      GUITextButton var11;
      (var11 = new GUITextButton(this.getState(), 60, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONSETTINGSPANEL_11, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ((GameClientState)FactionSettingsPanel.this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager().openOpenOffers();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }, ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager())).getPos().x = 375.0F;
      var3.getPos().y = 23.0F;
      var2.getPos().x = 35.0F;
      var2.getPos().y = 32.0F;
      var7.getPos().x = 145.0F;
      var7.getPos().y = 23.0F;
      var6.getPos().x = 180.0F;
      var6.getPos().y = 32.0F;
      var5.getPos().x = 350.0F;
      var5.getPos().y = 23.0F;
      var4.getPos().x = 385.0F;
      var4.getPos().y = 32.0F;
      var1.attach(var3);
      var1.attach(var2);
      var1.attach(var4);
      var1.attach(var5);
      var1.attach(var6);
      var1.attach(var7);
      var1.attach(var8);
      var1.attach(var10);
      var1.attach(var11);
      var1.attach(var9);
      var1.attach(this.factionPersonalEnemyButton);
      this.scrollPanel.setContent(var1);
      this.attach(this.scrollPanel);
      super.onInit();
   }
}
