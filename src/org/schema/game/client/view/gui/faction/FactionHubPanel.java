package org.schema.game.client.view.gui.faction;

import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class FactionHubPanel extends GUIElement implements GUICallback {
   private ScrollableFactionList factionList;

   public FactionHubPanel(InputState var1) {
      super(var1);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.getEventButtonState() && var2.getEventButton() == 0 && ((GameClientState)this.getState()).getPlayerInputs().isEmpty()) {
         final int var3;
         if (var1.getUserPointer() != null && var1.getUserPointer().toString().startsWith("JOIN_")) {
            var3 = Integer.parseInt(var1.getUserPointer().toString().substring(5));
            if (((GameClientState)this.getState()).getPlayer().getFactionId() != 0) {
               (new PlayerGameOkCancelInput("CONFIRM", (GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONHUBPANEL_0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONHUBPANEL_1) {
                  public boolean isOccluded() {
                     return false;
                  }

                  public void onDeactivate() {
                  }

                  public void pressedOK() {
                     this.getState().getPlayer().getFactionController().joinFaction(var3);
                     this.deactivate();
                  }
               }).activate();
            } else {
               ((GameClientState)this.getState()).getPlayer().getFactionController().joinFaction(var3);
            }
         }

         if (var1.getUserPointer() != null && var1.getUserPointer().toString().startsWith("REL_")) {
            var3 = Integer.parseInt(var1.getUserPointer().toString().substring(4));
            ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager().openRelationShipInput(var3);
         }
      }

   }

   public boolean isOccluded() {
      return false;
   }

   public void cleanUp() {
   }

   public void draw() {
      this.drawAttached();
   }

   public void onInit() {
      this.factionList = new ScrollableFactionList(this.getState(), 540, 325, this);
      this.factionList.onInit();
      this.attach(this.factionList);
   }

   public float getHeight() {
      return 0.0F;
   }

   public float getWidth() {
      return 0.0F;
   }

   public boolean isPositionCenter() {
      return false;
   }
}
