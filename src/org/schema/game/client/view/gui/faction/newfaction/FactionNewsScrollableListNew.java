package org.schema.game.client.view.gui.faction.newfaction;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Observer;
import java.util.Set;
import java.util.TreeSet;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionNewsPost;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableInnerDescription;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.GuiDateFormats;
import org.schema.schine.input.InputState;

public class FactionNewsScrollableListNew extends ScrollableTableList implements Observer {
   private Faction faction;
   private FactionManager factionManager;

   public FactionNewsScrollableListNew(InputState var1, GUIElement var2, Faction var3) {
      super(var1, 100.0F, 100.0F, var2);
      this.faction = var3;
      this.factionManager = this.getState().getFactionManager();
      this.columnsHeight = 36;
      this.factionManager.addObserver(this);
      if (this.getState().getController().getClientChannel() != null) {
         this.getState().getController().getClientChannel().addObserver(this);
      }

      this.getState().getPlayer().getFactionController().addObserver(this);
   }

   public void cleanUp() {
      this.factionManager.deleteObserver(this);
      if (this.getState().getController().getClientChannel() != null) {
         this.getState().getController().getClientChannel().deleteObserver(this);
      }

      this.getState().getPlayer().getFactionController().deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONNEWSSCROLLABLELISTNEW_1, 3.0F, new Comparator() {
         public int compare(FactionNewsPost var1, FactionNewsPost var2) {
            if (var1.getDate() > var2.getDate()) {
               return 1;
            } else {
               return var1.getDate() < var2.getDate() ? -1 : 0;
            }
         }
      });
      this.addButton(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               System.err.println("[CLIENT] Requesting News");
               FactionNewsScrollableListNew.this.getState().getController().getClientChannel().requestNextNews();
            }

         }

         public boolean isOccluded() {
            return !FactionNewsScrollableListNew.this.isActive();
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONNEWSSCROLLABLELISTNEW_0, ControllerElement.FilterRowStyle.FULL, ControllerElement.FilterPos.BOTTOM);
   }

   protected Collection getElementList() {
      TreeSet var1 = (TreeSet)this.getState().getFactionManager().getNews().get(this.faction.getIdFaction());

      assert this.getState().getController().getClientChannel() != null;

      assert this.getState().getController().getClientChannel().getFactionNews() != null;

      if (var1 != null) {
         var1.addAll(this.getState().getController().getClientChannel().getFactionNews());
      } else {
         var1 = this.getState().getController().getClientChannel().getFactionNews();
      }

      return (Collection)(var1 == null ? new HashSet() : var1.descendingSet());
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      this.getState().getGameState().getFactionManager();
      final PlayerState var3 = this.getState().getPlayer();
      Iterator var9 = var2.iterator();

      while(var9.hasNext()) {
         final FactionNewsPost var4 = (FactionNewsPost)var9.next();
         final GUITextOverlayTable var5;
         (var5 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var4.getTopic());
         final GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         boolean var7;
         String var8 = (var7 = var4.getMessage().indexOf("\n") > 0) ? var4.getMessage().substring(0, var4.getMessage().indexOf("\n") - 1) : var4.getMessage();
         var6.setTextSimple(var8.length() < 50 ? var8 + (var7 ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONNEWSSCROLLABLELISTNEW_2 : "") : var8.subSequence(0, 49) + Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONNEWSSCROLLABLELISTNEW_3);
         final GUITextOverlayTable var13;
         (var13 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(GuiDateFormats.factionNewsTime.format(var4.getDate()));
         final int var15 = var13.getFont().getWidth(var13.getText().get(0).toString());
         GUIAncor var16;
         (var16 = new GUIAncor(this.getState(), 100.0F, 32.0F) {
            public void draw() {
               this.setWidth(((ScrollableTableList.Column)FactionNewsScrollableListNew.this.columns.get(0)).bg.getWidth());
               var5.setPos(4.0F, 4.0F, 0.0F);
               var6.setPos(4.0F, 20.0F, 0.0F);
               var13.setPos(this.getWidth() - (float)var15 - 18.0F, 4.0F, 0.0F);
               super.draw();
            }
         }).attach(var5);
         var16.attach(var6);
         var16.attach(var13);
         FactionNewsScrollableListNew.FactionRow var11;
         (var11 = new FactionNewsScrollableListNew.FactionRow(this.getState(), var4, new GUIElement[]{var16}) {
            public void draw() {
               if (this.l.isExpended()) {
                  var6.setColor(0.0F, 0.0F, 0.0F, 0.0F);
               } else {
                  var6.setColor(0.8F, 0.8F, 0.8F, 1.0F);
               }

               super.draw();
            }
         }).expanded = new GUIElementList(this.getState());
         GUITextOverlayTableInnerDescription var12;
         (var12 = new GUITextOverlayTableInnerDescription(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return var4.getMessage();
            }
         });
         GUIAncor var14 = new GUIAncor(this.getState(), 100.0F, 100.0F);
         GUITextButton var10;
         (var10 = new GUITextButton(this.getState(), 80, 24, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONNEWSSCROLLABLELISTNEW_4, new GUICallback() {
            public boolean isOccluded() {
               return !FactionNewsScrollableListNew.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  System.err.println("DELETE");
                  FactionNewsScrollableListNew.this.getState().getPlayer().getFactionController().removeNewsClient(var4);
               }

            }
         }) {
            public void draw() {
               if (var3.getFactionController().hasDescriptionAndNewsPostPermission()) {
                  super.draw();
               }

            }
         }).setPos(0.0F, var14.getHeight(), 0.0F);
         var14.attach(var12);
         var14.attach(var10);
         var11.expanded.add(new GUIListElement(var14, var14, this.getState()));
         var11.onInit();
         var1.addWithoutUpdate(var11);
      }

      var1.updateDim();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   class FactionRow extends ScrollableTableList.Row {
      public FactionRow(InputState var2, FactionNewsPost var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.customColumnHeightExpanded = 16;
         this.highlightSelect = true;
      }
   }
}
