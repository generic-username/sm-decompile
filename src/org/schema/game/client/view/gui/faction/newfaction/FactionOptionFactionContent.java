package org.schema.game.client.view.gui.faction.newfaction;

import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerCreateFactionNewsInputNew;
import org.schema.game.client.controller.PlayerFactionPointDialogNew;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerTextAreaInput;
import org.schema.game.client.controller.manager.ingame.faction.FactionOfferDialog;
import org.schema.game.client.controller.manager.ingame.faction.FactionPersonalEnemyDialog;
import org.schema.game.client.controller.manager.ingame.faction.FactionRolesDialogNew;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUICheckBoxTextPair;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.input.InputState;

public class FactionOptionFactionContent extends GUIAncor {
   private FactionPanelNew panel;

   public FactionOptionFactionContent(InputState var1, FactionPanelNew var2) {
      super(var1);
      this.panel = var2;
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFaction();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public void onInit() {
      GUIElementList var1 = new GUIElementList(this.getState()) {
         public void draw() {
            if (FactionOptionFactionContent.this.getOwnFaction() != null) {
               super.draw();
            }

         }
      };
      GUIHorizontalButtonTablePane var2;
      (var2 = new GUIHorizontalButtonTablePane(this.getState(), 2, 5, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONFACTIONCONTENT_0, this)).onInit();
      var2.activeInterface = this.panel;
      var2.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONFACTIONCONTENT_1, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               FactionOptionFactionContent.this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager().openInvitePlayerDialog();
            }

         }

         public boolean isOccluded() {
            return !FactionOptionFactionContent.this.isActive();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return FactionOptionFactionContent.this.getOwnPlayer().getFactionController().hasInvitePermission();
         }
      });
      var2.addButton(1, 0, new Object() {
         public String toString() {
            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONFACTIONCONTENT_2, FactionOptionFactionContent.this.getState().getPlayer().getFactionController().getInvitesOutgoing().size());
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !FactionOptionFactionContent.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new FactionOutgoingInvitesPlayerInputNew(FactionOptionFactionContent.this.getState())).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return FactionOptionFactionContent.this.getOwnPlayer().getFactionController().hasInvitePermission();
         }
      });
      var2.addButton(0, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONFACTIONCONTENT_3, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !FactionOptionFactionContent.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new FactionOfferDialog(FactionOptionFactionContent.this.getState())).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return FactionOptionFactionContent.this.getOwnPlayer().getFactionController().hasRelationshipPermission();
         }
      });
      var2.addButton(1, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONFACTIONCONTENT_4, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !FactionOptionFactionContent.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerCreateFactionNewsInputNew(FactionOptionFactionContent.this.getState())).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return FactionOptionFactionContent.this.getOwnPlayer().getFactionController().hasDescriptionAndNewsPostPermission();
         }
      });
      var2.addButton(0, 2, new Object() {
         public String toString() {
            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONFACTIONCONTENT_5, (int)FactionOptionFactionContent.this.getState().getPlayer().getFactionController().getFactionPoints());
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !FactionOptionFactionContent.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerFactionPointDialogNew(FactionOptionFactionContent.this.getState())).activate();
            }

         }
      }, (GUIActivationCallback)null);
      var2.addButton(1, 2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONFACTIONCONTENT_6, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !FactionOptionFactionContent.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new FactionRolesDialogNew(FactionOptionFactionContent.this.getState(), FactionOptionFactionContent.this.getOwnFaction())).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return FactionOptionFactionContent.this.getOwnPlayer().getFactionController().hasPermissionEditPermission();
         }
      });
      var2.addButton(0, 3, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONFACTIONCONTENT_7, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !FactionOptionFactionContent.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               Faction var3 = FactionOptionFactionContent.this.getOwnFaction();
               PlayerTextAreaInput var4;
               (var4 = new PlayerTextAreaInput("FactionOptionFactionContent_EDIT_FACTION_DESC", FactionOptionFactionContent.this.getState(), 140, 5, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONFACTIONCONTENT_8, "", var3 != null ? var3.getDescription() : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONFACTIONCONTENT_9) {
                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public void onFailedTextCheck(String var1) {
                  }

                  public boolean isOccluded() {
                     return false;
                  }

                  public void onDeactivate() {
                  }

                  public boolean onInput(String var1) {
                     return this.getState().getPlayer().getFactionController().editDescriptionClient(var1);
                  }
               }).activate();
               var4.setInputChecker(new InputChecker() {
                  public boolean check(String var1, TextCallback var2) {
                     return true;
                  }
               });
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return FactionOptionFactionContent.this.getOwnPlayer().getFactionController().hasDescriptionAndNewsPostPermission();
         }
      });
      var2.addButton(1, 3, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONFACTIONCONTENT_10, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !FactionOptionFactionContent.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new FactionPersonalEnemyDialog(FactionOptionFactionContent.this.getState(), FactionOptionFactionContent.this.getOwnFaction())).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return FactionOptionFactionContent.this.getOwnPlayer().getFactionController().hasRelationshipPermission();
         }
      });
      var2.addButton(0, 4, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONFACTIONCONTENT_12, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !FactionOptionFactionContent.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerGameOkCancelInput("CONFIRM", FactionOptionFactionContent.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONFACTIONCONTENT_14, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONFACTIONCONTENT_13) {
                  public boolean isOccluded() {
                     return false;
                  }

                  public void onDeactivate() {
                  }

                  public void pressedOK() {
                     this.deactivate();
                     this.getState().getFactionManager().sendClientHomeBaseChange(this.getState().getPlayer().getName(), this.getState().getPlayer().getFactionId(), "");
                  }
               }).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return FactionOptionFactionContent.this.getOwnPlayer().getFactionController().hasHomebaseEditPermission() && FactionOptionFactionContent.this.getOwnPlayer().getFactionController().hasHomebase();
         }
      });
      GUICheckBoxTextPair var3 = new GUICheckBoxTextPair(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONFACTIONCONTENT_11, 208, 30) {
         public void activate() {
            if (FactionOptionFactionContent.this.isActive() && FactionOptionFactionContent.this.getOwnFaction() != null) {
               FactionOptionFactionContent.this.getOwnFaction().clientRequestAutoDeclareWar(FactionOptionFactionContent.this.getOwnPlayer(), true);
            }

         }

         public boolean isActivated() {
            return FactionOptionFactionContent.this.getOwnFaction() != null && FactionOptionFactionContent.this.getOwnFaction().isAutoDeclareWar();
         }

         public void deactivate() {
            if (FactionOptionFactionContent.this.isActive() && FactionOptionFactionContent.this.getOwnFaction() != null) {
               FactionOptionFactionContent.this.getOwnFaction().clientRequestAutoDeclareWar(FactionOptionFactionContent.this.getOwnPlayer(), false);
            }

         }
      };
      GUICheckBoxTextPair var4 = new GUICheckBoxTextPair(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONFACTIONCONTENT_15, 208, 20) {
         public boolean isActivated() {
            return FactionOptionFactionContent.this.getOwnFaction() != null && FactionOptionFactionContent.this.getOwnFaction().isAttackNeutral();
         }

         public void deactivate() {
            if (FactionOptionFactionContent.this.isActive() && FactionOptionFactionContent.this.getOwnFaction() != null) {
               FactionOptionFactionContent.this.getOwnFaction().clientRequestAttackNeutral(FactionOptionFactionContent.this.getOwnPlayer(), false);
            }

         }

         public void activate() {
            if (FactionOptionFactionContent.this.isActive() && FactionOptionFactionContent.this.getOwnFaction() != null) {
               FactionOptionFactionContent.this.getOwnFaction().clientRequestAttackNeutral(FactionOptionFactionContent.this.getOwnPlayer(), true);
            }

         }
      };
      GUICheckBoxTextPair var5 = new GUICheckBoxTextPair(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOPTIONFACTIONCONTENT_16, 208, 20) {
         public boolean isActivated() {
            return FactionOptionFactionContent.this.getOwnFaction() != null && FactionOptionFactionContent.this.getOwnFaction().isOpenToJoin();
         }

         public void deactivate() {
            if (FactionOptionFactionContent.this.isActive() && FactionOptionFactionContent.this.getOwnFaction() != null) {
               FactionOptionFactionContent.this.getOwnFaction().clientRequestOpenFaction(FactionOptionFactionContent.this.getOwnPlayer(), false);
            }

         }

         public void activate() {
            if (FactionOptionFactionContent.this.isActive() && FactionOptionFactionContent.this.getOwnFaction() != null) {
               FactionOptionFactionContent.this.getOwnFaction().clientRequestOpenFaction(FactionOptionFactionContent.this.getOwnPlayer(), true);
            }

         }
      };
      var3.setPos(4.0F, 10.0F, 0.0F);
      var4.setPos(4.0F, 0.0F, 0.0F);
      var5.setPos(4.0F, 0.0F, 0.0F);
      var1.add(new GUIListElement(var2, var2, this.getState()));
      var1.add(new GUIListElement(var3, var3, this.getState()));
      var1.add(new GUIListElement(var4, var4, this.getState()));
      var1.add(new GUIListElement(var5, var5, this.getState()));
      var1.setPos(1.0F, 0.0F, 0.0F);
      this.attach(var1);
   }
}
