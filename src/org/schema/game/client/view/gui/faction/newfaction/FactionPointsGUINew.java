package org.schema.game.client.view.gui.faction.newfaction;

import org.schema.game.client.controller.manager.ingame.faction.FactionRolesDialogNew;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionRoles;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.input.InputState;

public class FactionPointsGUINew extends GUIInputPanel {
   private Faction faction;
   private FactionRolesScrollableListNew sc;

   public FactionPointsGUINew(InputState var1, FactionRolesDialogNew var2, Faction var3) {
      super("FactionPointsGUINew", var1, 300, 450, var2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPOINTSGUINEW_0, "");
      this.faction = var3;
   }

   public void onInit() {
      super.onInit();
      this.sc = new FactionRolesScrollableListNew(this.getState(), ((GUIDialogWindow)this.background).getMainContentPane().getContent(0), this.faction);
      this.sc.onInit();
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(0).attach(this.sc);
   }

   public FactionRoles getRoles() {
      return this.sc.getRoles();
   }
}
