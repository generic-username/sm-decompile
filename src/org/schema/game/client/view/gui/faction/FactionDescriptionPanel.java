package org.schema.game.client.view.gui.faction;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public abstract class FactionDescriptionPanel extends GUIElement implements Observer {
   private int width;
   private int height;
   private GUIAncor descScrollPanel;
   private GUITextOverlay desc;
   private String currentDesc;
   private boolean updateNeeded;

   public FactionDescriptionPanel(InputState var1, int var2, int var3) {
      super(var1);
      this.width = var2;
      this.height = var3;
      ((GameClientState)this.getState()).getFactionManager().addObserver(this);
   }

   public void cleanUp() {
      ((GameClientState)this.getState()).getFactionManager().deleteObserver(this);
   }

   public void draw() {
      if (this.updateNeeded) {
         this.currentDesc = this.getCurrentDesc();
         this.desc.setText(new ArrayList());
         String[] var1 = this.currentDesc.split("\\\\n");
         this.desc.getText().clear();

         for(int var2 = 0; var2 < var1.length; ++var2) {
            this.desc.getText().add(var1[var2]);
         }

         this.updateNeeded = false;
      }

      this.drawAttached();
   }

   public void onInit() {
      this.descScrollPanel = new GUIAncor(this.getState(), (float)this.width, (float)this.height);
      this.desc = new GUITextOverlay(this.width, this.height, this.getState());
      this.currentDesc = this.getCurrentDesc();
      this.desc.setText(new ArrayList());
      String[] var1 = this.currentDesc.split("\\\\n");

      for(int var2 = 0; var2 < var1.length; ++var2) {
         this.desc.getText().add(var1[var2]);
      }

      this.descScrollPanel.attach(this.desc);
      this.attach(this.descScrollPanel);
   }

   public abstract String getCurrentDesc();

   public float getHeight() {
      return (float)this.height;
   }

   public float getWidth() {
      return (float)this.width;
   }

   public void update(Observable var1, Object var2) {
      this.updateNeeded = true;
   }
}
