package org.schema.game.client.view.gui.faction.newfaction;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerMailInputNew;
import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.game.client.controller.manager.ingame.faction.FactionRelationDialog;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.shiphud.newhud.ColorPalette;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionPermission;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIResizableElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.CreateGUIElementInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterDropdown;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableDropDown;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableInnerDescription;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class FactionScrollableListNew extends ScrollableTableList implements Observer {
   private GUIElement p;

   public FactionScrollableListNew(InputState var1, GUIElement var2) {
      super(var1, 100.0F, 100.0F, var2);
      this.p = var2;
      ((GameClientState)this.getState()).getFactionManager().addObserver(this);
   }

   public void cleanUp() {
      ((GameClientState)this.getState()).getFactionManager().deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_12, 7.0F, new Comparator() {
         public int compare(Faction var1, Faction var2) {
            return var1.getName().compareToIgnoreCase(var2.getName());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_13, 0.0F, new Comparator() {
         public int compare(Faction var1, Faction var2) {
            return var1.getMembersUID().size() - var2.getMembersUID().size();
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_14, 1.0F, new Comparator() {
         public int compare(Faction var1, Faction var2) {
            FactionRelation.RType var3 = ((GameClientState)FactionScrollableListNew.this.getState()).getFactionManager().getRelation(((GameClientState)FactionScrollableListNew.this.getState()).getPlayer().getName(), ((GameClientState)FactionScrollableListNew.this.getState()).getPlayer().getFactionId(), var1.getIdFaction());
            FactionRelation.RType var4 = ((GameClientState)FactionScrollableListNew.this.getState()).getFactionManager().getRelation(((GameClientState)FactionScrollableListNew.this.getState()).getPlayer().getName(), ((GameClientState)FactionScrollableListNew.this.getState()).getPlayer().getFactionId(), var2.getIdFaction());
            return var3.sortWeight - var4.sortWeight;
         }
      });
      this.addDropdownFilter(new GUIListFilterDropdown(FactionRelation.RType.values()) {
         public boolean isOk(FactionRelation.RType var1, Faction var2) {
            PlayerState var3 = ((GameClientState)FactionScrollableListNew.this.getState()).getPlayer();
            return ((GameClientState)FactionScrollableListNew.this.getState()).getGameState().getFactionManager().getRelation(var3.getName(), var3.getFactionId(), var2.getIdFaction()) == var1;
         }
      }, new CreateGUIElementInterface() {
         public GUIElement create(FactionRelation.RType var1) {
            GUIAncor var2 = new GUIAncor(FactionScrollableListNew.this.getState(), 10.0F, 24.0F);
            GUITextOverlayTableDropDown var3;
            (var3 = new GUITextOverlayTableDropDown(10, 10, FactionScrollableListNew.this.getState())).setTextSimple(var1.name());
            var3.setPos(4.0F, 4.0F, 0.0F);
            var2.setUserPointer(var1);
            var2.attach(var3);
            return var2;
         }

         public GUIElement createNeutral() {
            GUIAncor var1 = new GUIAncor(FactionScrollableListNew.this.getState(), 10.0F, 24.0F);
            GUITextOverlayTableDropDown var2;
            (var2 = new GUITextOverlayTableDropDown(10, 10, FactionScrollableListNew.this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_15);
            var2.setPos(4.0F, 4.0F, 0.0F);
            var1.attach(var2);
            return var1;
         }
      }, ControllerElement.FilterRowStyle.LEFT);
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, Faction var2) {
            return var2.getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, ControllerElement.FilterRowStyle.RIGHT);
   }

   protected Collection getElementList() {
      return ((GameClientState)this.getState()).getGameState().getFactionManager().getFactionCollection();
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      final FactionManager var3 = ((GameClientState)this.getState()).getGameState().getFactionManager();
      final PlayerState var4 = ((GameClientState)this.getState()).getPlayer();
      Iterator var15 = var2.iterator();

      while(var15.hasNext()) {
         final Faction var5 = (Faction)var15.next();
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var8 = new GUITextOverlayTable(10, 10, this.getState()) {
            public void draw() {
               FactionRelation.RType var1 = var3.getRelation(var4.getName(), var4.getFactionId(), var5.getIdFaction());
               this.setColor(ColorPalette.getColorDefault(var1, var5.getIdFaction() == var4.getFactionId()));
               super.draw();
            }
         };
         ScrollableTableList.GUIClippedRow var9;
         (var9 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var6);
         var6.setTextSimple(new Object() {
            public String toString() {
               return var5.getName() + (var5.isOpenToJoin() ? " " + Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_16 : "");
            }
         });
         var7.setTextSimple(new Object() {
            public String toString() {
               return var5.getIdFaction() > 0 ? String.valueOf(var5.getMembersUID().size()) : "-";
            }
         });
         var8.setTextSimple(new Object() {
            public String toString() {
               return var5.getIdFaction() == var4.getFactionId() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_17 : var3.getRelation(var4.getName(), var4.getFactionId(), var5.getIdFaction()).getName();
            }
         });
         var6.getPos().y = 4.0F;
         var7.getPos().y = 4.0F;
         var8.getPos().y = 4.0F;
         FactionScrollableListNew.FactionRow var16;
         (var16 = new FactionScrollableListNew.FactionRow(this.getState(), var5, new GUIElement[]{var9, var7, var8})).expanded = new GUIElementList(this.getState());
         GUIAncor var17 = new GUIAncor(this.getState(), 100.0F, 100.0F) {
            public void draw() {
               this.setWidth(FactionScrollableListNew.this.p.getWidth());
               super.draw();
            }
         };
         final GUIScrollablePanel var18 = new GUIScrollablePanel(80.0F, 80.0F, var17, this.getState()) {
            public void draw() {
               super.draw();
            }
         };
         GUITextOverlayTableInnerDescription var19 = new GUITextOverlayTableInnerDescription(10, 10, this.getState());
         var18.setContent(var19);
         GUIResizableElement var10 = new GUIResizableElement(this.getState()) {
            public float getWidth() {
               return var18.getWidth() - 20.0F;
            }

            public float getHeight() {
               return var18.getHeight() - 20.0F;
            }

            public void cleanUp() {
            }

            public void draw() {
            }

            public void onInit() {
            }

            public void setWidth(float var1) {
            }

            public void setHeight(float var1) {
            }
         };
         var19.autoWrapOn = var10;
         var19.setTextSimple(new Object() {
            public String toString() {
               return var5.getDescription();
            }
         });
         var19.setPos(4.0F, 2.0F, 0.0F);
         GUITextButton var20 = new GUITextButton(this.getState(), 80, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_18, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  (new PlayerMailInputNew((GameClientState)FactionScrollableListNew.this.getState(), StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_19, var5.getName()), "")).activate();
               }

            }

            public boolean isOccluded() {
               return !FactionScrollableListNew.this.isActive();
            }
         });
         GUITextButton var21 = new GUITextButton(this.getState(), 130, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_0, new GUICallback() {
            public boolean isOccluded() {
               return !FactionScrollableListNew.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  System.err.println("VIEW RELATION");
                  final PlayerGameOkCancelInput var3;
                  (var3 = new PlayerGameOkCancelInput("FactionScrollableListNew_VIEW_REL", (GameClientState)FactionScrollableListNew.this.getState(), 540, 400, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_1, var5.getName()), "") {
                     public boolean isOccluded() {
                        return !this.getState().getController().getPlayerInputs().isEmpty() && this.getState().getController().getPlayerInputs().get(this.getState().getController().getPlayerInputs().size() - 1) != this;
                     }

                     public void onDeactivate() {
                     }

                     public void pressedOK() {
                        this.deactivate();
                     }
                  }).getInputPanel().setCancelButton(false);
                  var3.getInputPanel().onInit();
                  FactionScrollableListRelation var4;
                  (var4 = new FactionScrollableListRelation(FactionScrollableListNew.this.getState(), var3.getInputPanel().getContent(), var5) {
                     public boolean isActive() {
                        return super.isActive() && (this.getState().getController().getPlayerInputs().isEmpty() || this.getState().getController().getPlayerInputs().get(this.getState().getController().getPlayerInputs().size() - 1) == var3);
                     }
                  }).onInit();
                  var3.getInputPanel().getContent().attach(var4);
                  var3.activate();
               }

            }
         });
         GUITextButton var11 = new GUITextButton(this.getState(), 80, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_2, new GUICallback() {
            public boolean isOccluded() {
               return !FactionScrollableListNew.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  if (((GameClientState)FactionScrollableListNew.this.getState()).getPlayer().getFactionId() != 0) {
                     (new PlayerGameOkCancelInput("FactionScrollableListNew_VIEW_JOIN", (GameClientState)FactionScrollableListNew.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_3, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_4) {
                        public boolean isOccluded() {
                           return false;
                        }

                        public void onDeactivate() {
                        }

                        public void pressedOK() {
                           this.getState().getPlayer().getFactionController().joinFaction(var5.getIdFaction());
                           this.deactivate();
                        }
                     }).activate();
                     return;
                  }

                  ((GameClientState)FactionScrollableListNew.this.getState()).getPlayer().getFactionController().joinFaction(var5.getIdFaction());
               }

            }
         }) {
            public void draw() {
               if (var5.isOpenToJoin() && var5.getIdFaction() > 0 && var5.getIdFaction() != var4.getFactionId()) {
                  super.draw();
               }

            }
         };
         GUITextButton var12 = new GUITextButton(this.getState(), 130, 24, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_5, new GUICallback() {
            public boolean isOccluded() {
               return !FactionScrollableListNew.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  System.err.println("CHANGE RELATION");
                  (new FactionRelationDialog((GameClientState)FactionScrollableListNew.this.getState(), ((GameClientState)FactionScrollableListNew.this.getState()).getFaction(), var5)).activate();
               }

            }
         }) {
            public void draw() {
               FactionPermission var1;
               if (((GameClientState)this.getState()).getFaction() != null && (var1 = (FactionPermission)((GameClientState)this.getState()).getFaction().getMembersUID().get(((GameClientState)this.getState()).getPlayer().getName())) != null && var1.hasRelationshipPermission(((GameClientState)this.getState()).getFaction()) && (var5.getIdFaction() > 0 || FactionManager.isNPCFaction(var5.getIdFaction())) && var5.getIdFaction() != var4.getFactionId()) {
                  super.draw();
               }

            }
         };
         GUITextButton var13 = new GUITextButton(this.getState(), 110, 24, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_9, new GUICallback() {
            public boolean isOccluded() {
               return !FactionScrollableListNew.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  (new PlayerOkCancelInput("CONFIRM", FactionScrollableListNew.this.getState(), 400, 230, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_10, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_11, var5.getName())) {
                     public void pressedOK() {
                        ((GameClientState)this.getState()).getPlayer().getFactionController().shareFow(var5.getIdFaction());
                        this.deactivate();
                     }

                     public void onDeactivate() {
                     }
                  }).activate();
               }

            }
         }) {
            public void draw() {
               FactionPermission var1;
               if (((GameClientState)this.getState()).getFaction() != null && (var1 = (FactionPermission)((GameClientState)this.getState()).getFaction().getMembersUID().get(((GameClientState)this.getState()).getPlayer().getName())) != null && var1.hasPermissionEditPermission(((GameClientState)this.getState()).getFaction()) && var5.getIdFaction() > 0 && var5.getIdFaction() != var4.getFactionId()) {
                  super.draw();
               }

            }
         };
         GUITextOverlayTableInnerDescription var14;
         (var14 = new GUITextOverlayTableInnerDescription(10, 10, this.getState()) {
            public void draw() {
               super.draw();
            }
         }).setTextSimple(new Object() {
            public String toString() {
               String var1 = var5.getHomebaseUID().length() > 0 ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_6 + var5.getHomeSector().toStringPure() : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_7;
               return ((GameClientState)FactionScrollableListNew.this.getState()).getPlayer().getNetworkObject().isAdminClient.get() ? var1 + Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTNEW_8 + var5.getIdFaction() : var1;
            }
         });
         var17.attach(var20);
         var17.attach(var21);
         var17.attach(var12);
         var17.attach(var13);
         var17.attach(var14);
         var17.attach(var11);
         var14.setPos(4.0F, var17.getHeight() - 16.0F, 0.0F);
         var20.setPos(0.0F, var17.getHeight(), 0.0F);
         var21.setPos(90.0F, var17.getHeight(), 0.0F);
         var12.setPos(var20.getWidth() + 10.0F + var21.getWidth() + 10.0F, var17.getHeight(), 0.0F);
         var13.setPos(var12.getPos().x + var12.getWidth() + 20.0F, var17.getHeight(), 0.0F);
         var11.setPos(var20.getWidth() + 10.0F + var21.getWidth() + 10.0F + var12.getWidth() + 10.0F + var13.getWidth() + 10.0F, var17.getHeight(), 0.0F);
         var17.attach(var18);
         var16.expanded.add(new GUIListElement(var17, var17, this.getState()));
         var16.onInit();
         var1.addWithoutUpdate(var16);
      }

      var1.updateDim();
   }

   protected boolean isFiltered(Faction var1) {
      return !var1.isShowInHub() || super.isFiltered(var1);
   }

   class FactionRow extends ScrollableTableList.Row {
      public FactionRow(InputState var2, Faction var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
