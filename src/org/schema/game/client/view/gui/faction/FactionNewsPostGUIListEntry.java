package org.schema.game.client.view.gui.faction;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.vecmath.Vector4f;
import org.schema.game.common.data.player.faction.FactionNewsPost;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class FactionNewsPostGUIListEntry extends GUIListElement {
   private GUIColoredRectangle bg;
   private GUIColoredRectangle bgHead;
   private FactionNewsPost newsPost;

   public FactionNewsPostGUIListEntry(InputState var1, FactionNewsPost var2, int var3) {
      super(var1);
      this.bg = new GUIColoredRectangle(this.getState(), 540.0F, 100.0F, var3 % 2 == 0 ? new Vector4f(0.1F, 0.1F, 0.1F, 1.0F) : new Vector4f(0.2F, 0.2F, 0.2F, 1.0F));
      this.setContent(this.bg);
      this.setSelectContent(this.bg);
      this.newsPost = var2;
   }

   public void onInit() {
      super.onInit();
      GUITextOverlay var1 = new GUITextOverlay(200, 20, this.getState());
      GUITextOverlay var2 = new GUITextOverlay(200, 20, this.getState());
      GUITextOverlay var3 = new GUITextOverlay(200, 20, this.getState());
      this.bgHead = new GUIColoredRectangle(this.getState(), 550.0F, 20.0F, new Vector4f(0.3F, 0.3F, 0.4F, 0.8F));
      var1.setTextSimple(this.newsPost.getOp());
      GregorianCalendar var4;
      (var4 = new GregorianCalendar()).setTime(new Date(this.newsPost.getDate()));
      var2.setTextSimple(var4.getTime().toString());
      var3.setText(new ArrayList());
      String[] var8;
      int var5 = (var8 = this.newsPost.getMessage().split("\\\\n")).length;

      for(int var6 = 0; var6 < var5; ++var6) {
         String var7 = var8[var6];
         var3.getText().add(var7);
      }

      this.bgHead.attach(var1);
      this.bgHead.attach(var2);
      var2.getPos().x = 300.0F;
      var3.getPos().y = 20.0F;
      this.bg.attach(this.bgHead);
      this.bg.attach(var3);
   }
}
