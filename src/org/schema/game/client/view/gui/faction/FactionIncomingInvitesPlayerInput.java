package org.schema.game.client.view.gui.faction;

import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyEventInterface;

public class FactionIncomingInvitesPlayerInput extends PlayerInput {
   private FactionIncomingInvitesPlayerInput.FactionInvitePanel panel;
   private AbstractControlManager caller;

   public FactionIncomingInvitesPlayerInput(GameClientState var1, AbstractControlManager var2) {
      super(var1);
      this.caller = var2;
      var2.suspend(true);
      this.panel = new FactionIncomingInvitesPlayerInput.FactionInvitePanel(this.getState(), this, "Incoming Invites", "");
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         if (var1.getUserPointer().equals("OK")) {
            this.cancel();
         }

         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            System.err.println("CANCEL");
            this.cancel();
         }
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
   }

   public GUIElement getInputPanel() {
      return this.panel;
   }

   public void onDeactivate() {
      this.caller.suspend(false);
   }

   public boolean isOccluded() {
      return false;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   class FactionInvitePanel extends GUIInputPanel {
      public FactionInvitePanel(InputState var2, GUICallback var3, Object var4, Object var5) {
         super("FACTION_INVITE_PANEL", var2, var3, var4, var5);
         this.setOkButton(false);
      }

      public void onInit() {
         super.onInit();
         FactionIncomingInvitationsPanel var1;
         (var1 = new FactionIncomingInvitationsPanel(420.0F, 140.0F, this.getState())).onInit();
         this.getContent().attach(var1);
      }
   }
}
