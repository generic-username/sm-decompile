package org.schema.game.client.view.gui.faction;

import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.manager.ingame.faction.FactionBlockDialog;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.SimplePlayerCommands;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class FactionBlockGUIDialog extends GUIInputPanel {
   private SimpleTransformableSendableObject obj;

   public FactionBlockGUIDialog(InputState var1, FactionBlockDialog var2, SimpleTransformableSendableObject var3) {
      super("FACTION_BLOCK_GUI_DIALOG_S", var1, 450, 270, var2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONBLOCKGUIDIALOG_3, "");
      this.obj = var3;
      this.setCancelButton(false);
   }

   public void onInit() {
      super.onInit();
      GUITextButton var1;
      (var1 = new GUITextButton(this.getState(), 200, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONBLOCKGUIDIALOG_4, this.getCallback())).setUserPointer("NEUTRAL");
      GUITextButton var2;
      (var2 = new GUITextButton(this.getState(), 200, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONBLOCKGUIDIALOG_7, this.getCallback())).setUserPointer("FACTION");
      var2.getPos().y = 30.0F;
      final GameClientState var3 = (GameClientState)this.getState();
      GUITextButton var5;
      (var5 = new GUITextButton(this.getState(), 200, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONBLOCKGUIDIALOG_8, this.getCallback()) {
         public void draw() {
            if (((GameClientState)this.getState()).getFaction() != null && FactionBlockGUIDialog.this.obj.getFactionId() == var3.getPlayer().getFactionId() && var3.getPlayer().getFactionController().hasHomebaseEditPermission() && !var3.getPlayer().getFactionController().isHomebase(FactionBlockGUIDialog.this.obj) && (FactionBlockGUIDialog.this.obj instanceof Planet || FactionBlockGUIDialog.this.obj instanceof SpaceStation)) {
               super.draw();
            }

         }
      }).setUserPointer("HOMEBASE");
      var5.getPos().y = 60.0F;
      this.getContent().attach(var5);
      (var5 = new GUITextButton(this.getState(), 200, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONBLOCKGUIDIALOG_9, this.getCallback()) {
         public void draw() {
            if (((GameClientState)this.getState()).getFaction() != null && FactionBlockGUIDialog.this.obj.getFactionId() == ((GameClientState)this.getState()).getPlayer().getFactionId() && (FactionBlockGUIDialog.this.obj instanceof Planet || FactionBlockGUIDialog.this.obj instanceof SpaceStation)) {
               VoidSystem var1;
               if ((var1 = ((GameClientState)this.getState()).getController().getClientChannel().getGalaxyManagerClient().getSystemOnClient(((GameClientState)this.getState()).getCurrentRemoteSector().clientPos())) != null) {
                  if (var1.getOwnerFaction() == ((GameClientState)this.getState()).getPlayer().getFactionId()) {
                     this.setText(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONBLOCKGUIDIALOG_10);
                     this.setUserPointer("SYSTEM_REVOKE");
                  } else if (var1.getOwnerFaction() != 0) {
                     Faction var2;
                     if ((var2 = ((GameClientState)this.getState()).getFactionManager().getFaction(var1.getOwnerFaction())) != null) {
                        this.setText(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONBLOCKGUIDIALOG_11, var2.getName()));
                        this.setUserPointer("SYSTEM_NONE");
                     } else {
                        this.setText(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONBLOCKGUIDIALOG_12);
                        this.setUserPointer("SYSTEM_OWN");
                     }
                  } else {
                     this.setText(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONBLOCKGUIDIALOG_13);
                     this.setUserPointer("SYSTEM_OWN");
                  }
               } else {
                  this.setText(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONBLOCKGUIDIALOG_14);
               }

               super.draw();
            }

         }
      }).getPos().x = 200.0F;
      var5.getPos().y = 60.0F;
      this.getContent().attach(var5);
      (var5 = new GUITextButton(this.getState(), 200, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONBLOCKGUIDIALOG_5, this.getCallback()) {
         public void draw() {
            if (((GameClientState)this.getState()).getFaction() != null && FactionBlockGUIDialog.this.obj.getFactionId() == ((GameClientState)this.getState()).getPlayer().getFactionId()) {
               super.draw();
            }

         }
      }).setUserPointer("RENAME");
      var5.getPos().y = 90.0F;
      this.getContent().attach(var5);
      GUITextOverlay var6;
      if (this.obj instanceof SegmentController) {
         (var6 = new GUITextOverlay(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return ((SegmentController)FactionBlockGUIDialog.this.obj).currentOwnerLowerCase.length() > 0 ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONBLOCKGUIDIALOG_15, ((SegmentController)FactionBlockGUIDialog.this.obj).currentOwnerLowerCase) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONBLOCKGUIDIALOG_16;
            }
         });
         var6.setPos(5.0F, 115.0F, 0.0F);
         this.getContent().attach(var6);
      }

      (var6 = new GUITextOverlay(10, 10, this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONBLOCKGUIDIALOG_6);
      var6.getPos().x = 5.0F;
      var6.getPos().y = 133.0F;

      for(byte var4 = -1; var4 < 5; ++var4) {
         this.addFactionRank(var4);
      }

      this.getContent().attach(var6);
      this.getContent().attach(var1);
      this.getContent().attach(var2);
   }

   private void addFactionRank(final byte var1) {
      GUITextButton var2;
      (var2 = new GUITextButton(this.getState(), 20, 20, this.getRankString(var1) + (var1 == ((GameClientState)this.getState()).getPlayer().getFactionRights() ? "*" : ""), new GUICallback() {
         public void callback(GUIElement var1x, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               SegmentController var3 = (SegmentController)FactionBlockGUIDialog.this.obj;
               final PlayerState var4 = ((GameClientState)FactionBlockGUIDialog.this.getState()).getPlayer();
               if (var3.getFactionRights() != -1 && var4.getFactionRights() == 4 || var3.isSufficientFactionRights(var4)) {
                  if (var1 > var4.getFactionRights()) {
                     (new PlayerGameOkCancelInput("CONFIRM", (GameClientState)FactionBlockGUIDialog.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONBLOCKGUIDIALOG_0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONBLOCKGUIDIALOG_1) {
                        public boolean isOccluded() {
                           return false;
                        }

                        public void onDeactivate() {
                        }

                        public void pressedOK() {
                           var4.sendSimpleCommand(SimplePlayerCommands.SET_FACTION_RANK_ON_OBJ, FactionBlockGUIDialog.this.obj.getId(), var1);
                           this.deactivate();
                        }
                     }).activate();
                     return;
                  }

                  var4.sendSimpleCommand(SimplePlayerCommands.SET_FACTION_RANK_ON_OBJ, FactionBlockGUIDialog.this.obj.getId(), var1);
                  return;
               }

               ((GameClientState)FactionBlockGUIDialog.this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONBLOCKGUIDIALOG_2, 0.0F);
               System.err.println("[CLIENT] Permission failed: " + var4 + ": Rank: " + var4.getFactionRights() + "; Target " + var3 + ": Rank: " + var3.getFactionRights() + "; toSet: " + var1);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }) {
         private Vector4f selected = new Vector4f(0.5F, 0.9F, 0.4F, 1.0F);
         private Vector4f uselected = new Vector4f(0.8F, 0.2F, 0.4F, 1.0F);
         private Vector4f blue = new Vector4f(0.2F, 0.4F, 0.8F, 1.0F);
         private Vector4f mouse = new Vector4f(0.5F, 0.4F, 0.2F, 1.0F);

         public void draw() {
            if (((GameClientState)this.getState()).getFaction() != null && FactionBlockGUIDialog.this.obj.getFactionId() == ((GameClientState)this.getState()).getPlayer().getFactionId()) {
               super.draw();
            }

         }

         public Vector4f getBackgroundColor() {
            if (((SegmentController)FactionBlockGUIDialog.this.obj).getFactionRights() == -1) {
               return var1 == -1 ? this.selected : this.uselected;
            } else if (((SegmentController)FactionBlockGUIDialog.this.obj).getFactionRights() == -2) {
               return var1 == -1 ? this.blue : this.uselected;
            } else if (var1 >= ((SegmentController)FactionBlockGUIDialog.this.obj).getFactionRights()) {
               return this.selected;
            } else {
               return var1 == -1 ? this.blue : this.uselected;
            }
         }

         public Vector4f getBackgroundColorMouseOverlay() {
            return this.mouse;
         }
      }).setTextPos(3, 1);
      var2.getPos().y = 131.0F;
      var2.getPos().x = (float)(106 + (var1 >= 0 ? var1 * 20 : 100));
      this.getContent().attach(var2);
   }

   private String getRankString(byte var1) {
      if (var1 == -1) {
         return "P";
      } else {
         return var1 == 4 ? "F" : String.valueOf(4 - var1);
      }
   }
}
