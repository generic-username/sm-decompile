package org.schema.game.client.view.gui.lagStats;

import javax.vecmath.Vector4f;
import org.schema.game.client.controller.PlayerLagStatsInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUICheckBoxTextPair;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIStatisticsGraph;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITabbedContent;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.StatisticsGraphListInterface;
import org.schema.schine.input.InputState;

public class GUILagStatsPanelNew extends GUIInputPanel implements GUIActiveInterface {
   private GUILagObjectsScrollableList lRec;

   public GUILagStatsPanelNew(InputState var1, int var2, int var3, PlayerLagStatsInput var4) {
      super("GUILagStatsPanelNew", var1, var2, var3, var4, "Lag Stats", "");
      this.setOkButton(false);
   }

   public void onInit() {
      super.onInit();
      ((GUIDialogWindow)this.background).getMainContentPane().setTextBoxHeightLast(25);
      ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(86);
      ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(86);
      GUITabbedContent var1;
      (var1 = new GUITabbedContent(this.getState(), ((GUIDialogWindow)this.background).getMainContentPane().getContent(2))).activationInterface = this;
      var1.onInit();
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(2).attach(var1);
      final GUIContentPane var2;
      (var2 = var1.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LAGSTATS_GUILAGSTATSPANELNEW_0)).getTextColorSelected().set(new Vector4f(1.0F, 0.4F, 0.4F, 0.7F));
      var2.getTextColorUnselected().set(new Vector4f(0.65F, 0.1F, 0.1F, 0.7F));
      GUIScrollablePanel var3 = new GUIScrollablePanel(100.0F, 100.0F, ((GUIDialogWindow)this.background).getMainContentPane().getContent(1), this.getState());
      GUIStatisticsGraph var4;
      (var4 = new GUIStatisticsGraph(this.getState(), var1, ((GUIDialogWindow)this.background).getMainContentPane().getContent(1), new StatisticsGraphListInterface[]{((GameClientState)this.getState()).lagStats}) {
         public String formatMax(long var1, long var3) {
            return "Max: " + var1 + "ms" + (this.ps == null ? " (F7)" : "");
         }
      }).onInit();
      var3.setContent(var4);
      GUIDropDownList var5 = new GUIDropDownList(this.getState(), 100, 24, 300, var4);
      this.addTime(var5, "10 secs", 10000L);
      this.addTime(var5, "30 secs", 30000L);
      this.addTime(var5, "1 min", 60000L);
      this.addTime(var5, "2 min", 120000L);
      this.addTime(var5, "3 min", 180000L);
      this.addTime(var5, "4 min", 240000L);
      var5.setSelectedIndex(3);
      var5.dependend = ((GUIDialogWindow)this.background).getMainContentPane().getContent(0);
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(0).attach(var5);
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(1).attach(var3);
      GUICheckBoxTextPair var10000 = new GUICheckBoxTextPair(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LAGSTATS_GUILAGSTATSPANELNEW_1, 200, 24) {
         public boolean isActivated() {
            return EngineSettings.G_DRAW_LAG_OBJECTS_IN_HUD.isOn();
         }

         public void deactivate() {
            EngineSettings.G_DRAW_LAG_OBJECTS_IN_HUD.setCurrentState(false);
         }

         public void activate() {
            EngineSettings.G_DRAW_LAG_OBJECTS_IN_HUD.setCurrentState(true);
         }
      };
      GUITextButton var6;
      (var6 = new GUITextButton(this.getState(), 95, 24, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LAGSTATS_GUILAGSTATSPANELNEW_2, new GUICallback() {
         public boolean isOccluded() {
            return !((GameClientState)GUILagStatsPanelNew.this.getState()).lagStats.isAnySelected();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ((GameClientState)GUILagStatsPanelNew.this.getState()).lagStats.deselectAll();
            }

         }
      }) {
         public boolean isActive() {
            return super.isActive() && ((GameClientState)this.getState()).lagStats.isAnySelected();
         }

         public void draw() {
            this.setPos(2.0F, var2.getContent(0).getHeight() + 5.0F, 0.0F);
            super.draw();
         }
      }).setPos(2.0F, 2.0F, 0.0F);
      var2.getContent(0).attach(var6);
      this.lRec = new GUILagObjectsScrollableList((GameClientState)this.getState(), var2.getContent(0), ((GameClientState)this.getState()).lagStats);
      this.lRec.onInit();
      ((GameClientState)this.getState()).lagStats.scollableList = this.lRec;
      var2.getContent(0).attach(this.lRec);
   }

   private void addTime(GUIDropDownList var1, String var2, long var3) {
      GUIAncor var5 = new GUIAncor(this.getState(), 10.0F, 24.0F);
      GUITextOverlayTable var6;
      (var6 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var2);
      var6.setPos(4.0F, 4.0F, 0.0F);
      var5.setUserPointer(var3);
      var5.attach(var6);
      var1.add(new GUIListElement(var5, this.getState()));
   }

   public boolean isActive() {
      return (this.getState().getController().getPlayerInputs().isEmpty() || ((DialogInterface)this.getState().getController().getPlayerInputs().get(this.getState().getController().getPlayerInputs().size() - 1)).getInputPanel() == this) && super.isActive();
   }

   public void flagChanged() {
      if (this.lRec != null) {
         this.lRec.flagDirty();
      }

   }
}
