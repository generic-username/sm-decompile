package org.schema.game.client.view.gui.lagStats;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.forms.gui.newgui.StatisticsGraphListInterface;

public class LagDataStatsList extends ObjectArrayList implements StatisticsGraphListInterface {
   private static final long serialVersionUID = 1L;
   private final Vector4f color;
   public GUILagObjectsScrollableList scollableList;

   public LagDataStatsList(Vector4f var1) {
      this.color = var1;
   }

   public int getSize() {
      return this.size();
   }

   public double getAmplitudePercentAtIndex(int var1, long var2) {
      return (double)((LagDataStatsEntry)this.get(var1)).volume / (double)var2;
   }

   public long getAmplitudeAtIndex(int var1) {
      return ((LagDataStatsEntry)this.get(var1)).volume;
   }

   public long getTimeAtIndex(int var1) {
      return ((LagDataStatsEntry)this.get(var1)).time;
   }

   public int getStartIndexFrom(long var1) {
      if (this.size() == 0) {
         return -1;
      } else if (this.size() == 1) {
         return 0;
      } else {
         for(int var3 = this.size() - 1; var3 > 0; --var3) {
            assert ((LagDataStatsEntry)this.get(var3)).time < ((LagDataStatsEntry)this.get(var3 - 1)).time;

            if (((LagDataStatsEntry)this.get(var3)).time < var1 && ((LagDataStatsEntry)this.get(var3 - 1)).time > var1) {
               return var3;
            }
         }

         return this.size() - 1;
      }
   }

   public int getClosestIndexFrom(long var1) {
      if (this.size() == 0) {
         return -1;
      } else if (this.size() == 1) {
         return 0;
      } else {
         for(int var3 = this.size() - 1; var3 > 0; --var3) {
            assert ((LagDataStatsEntry)this.get(var3)).time < ((LagDataStatsEntry)this.get(var3 - 1)).time;

            if (((LagDataStatsEntry)this.get(var3)).time < var1 && ((LagDataStatsEntry)this.get(var3 - 1)).time > var1) {
               if (Math.abs(((LagDataStatsEntry)this.get(var3 - 1)).time - var1) < Math.abs(((LagDataStatsEntry)this.get(var3)).time - var1)) {
                  return var3 - 1;
               }

               return var3;
            }
         }

         return this.size() - 1;
      }
   }

   public int getEndIndexFrom(long var1) {
      return this.getStartIndexFrom(var1);
   }

   public long getMaxAplitude(long var1, long var3) {
      int var7 = this.getStartIndexFrom(var1);
      long var5 = 0L;

      for(int var2 = 0; var2 <= var7; ++var2) {
         var5 = Math.max(var5, this.getAmplitudeAtIndex(var2));
      }

      return var5;
   }

   public Vector4f getColor() {
      return this.color;
   }

   public void select(int var1) {
      if (var1 > 0 && var1 < this.size()) {
         if (((LagDataStatsEntry)this.get(var1)).selected) {
            this.deselectAll();
            return;
         }

         this.deselectAll();
         ((LagDataStatsEntry)this.get(var1)).selected = true;
      }

   }

   public boolean isSelected(int var1) {
      return var1 > 0 && var1 < this.size() && ((LagDataStatsEntry)this.get(var1)).selected;
   }

   public void notifyGUI() {
      this.scollableList.notifyGUIFromDataStatistics();
   }

   void deselectAll() {
      for(int var1 = 0; var1 < this.size(); ++var1) {
         ((LagDataStatsEntry)this.get(var1)).selected = false;
      }

   }

   public void clearAllBefore(long var1) {
      for(int var3 = 0; var3 < this.size; ++var3) {
         if (((LagDataStatsEntry)this.get(var3)).time < var1) {
            this.remove(var3);
            --var3;
         }
      }

   }

   public boolean isAnySelected() {
      for(int var1 = 0; var1 < this.size; ++var1) {
         if (((LagDataStatsEntry)this.get(var1)).selected) {
            return true;
         }
      }

      return false;
   }

   public LagDataStatsEntry getSelected() {
      for(int var1 = 0; var1 < this.size; ++var1) {
         if (((LagDataStatsEntry)this.get(var1)).selected) {
            return (LagDataStatsEntry)this.get(var1);
         }
      }

      return null;
   }
}
