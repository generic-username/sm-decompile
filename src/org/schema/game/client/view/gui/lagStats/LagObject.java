package org.schema.game.client.view.gui.lagStats;

import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.network.objects.Sendable;

public class LagObject {
   public final Sendable s;
   private long lagTime;

   public LagObject(Sendable var1) {
      this.s = var1;
      this.lagTime = var1.getCurrentLag();
   }

   public int hashCode() {
      return this.s.hashCode() * (int)this.lagTime;
   }

   public boolean equals(Object var1) {
      LagObject var2 = (LagObject)var1;
      return this.s == var2.s && this.lagTime == var2.lagTime;
   }

   public long getLagTime() {
      return this.lagTime;
   }

   public String getType() {
      if (this.s instanceof RemoteSector) {
         return "SECTOR";
      } else {
         return this.s instanceof SimpleTransformableSendableObject ? ((SimpleTransformableSendableObject)this.s).getTypeString() : "OTHER";
      }
   }

   public String getSector() {
      String var1;
      if (this.s instanceof RemoteSector) {
         var1 = ((RemoteSector)this.s).clientPos().toStringPure();
      } else if (this.s instanceof AbstractOwnerState) {
         var1 = ((PlayerState)this.s).getCurrentSector().toStringPure();
      } else if (this.s instanceof SimpleTransformableSendableObject) {
         synchronized(this.s.getState()) {
            this.s.getState().setSynched();
            Sendable var4;
            if ((var4 = (Sendable)this.s.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(((SimpleTransformableSendableObject)this.s).getSectorId())) != null && var4 instanceof RemoteSector) {
               var1 = ((RemoteSector)var4).clientPos().toStringPure();
            } else {
               var1 = "unloadedSector(" + ((SimpleTransformableSendableObject)this.s).getSectorId() + ")";
            }

            this.s.getState().setUnsynched();
         }
      } else {
         var1 = "unknown";
      }

      return var1;
   }

   public String getName() {
      return this.s instanceof RemoteSector ? "SectorTotal" : this.s.toString();
   }
}
