package org.schema.game.client.view.gui.advancedbuildmode;

import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.ButtonCallback;
import org.schema.game.client.view.gui.advanced.tools.ButtonResult;
import org.schema.game.client.view.gui.advanced.tools.CheckboxCallback;
import org.schema.game.client.view.gui.advanced.tools.CheckboxResult;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;

public class AdvancedBuildModeSymmetry extends AdvancedBuildModeGUISGroup {
   public AdvancedBuildModeSymmetry(AdvancedGUIElement var1) {
      super(var1);
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      new GUITextOverlay(10, 10, this.getState());
      this.addButton(var1.getContent(0), 0, 0, new AdvancedBuildModeSymmetry.SymResult(1));
      this.addButton(var1.getContent(0), 1, 0, new AdvancedBuildModeSymmetry.SymResult(2));
      this.addButton(var1.getContent(0), 2, 0, new AdvancedBuildModeSymmetry.SymResult(4));
      this.addButton(var1.getContent(0), 0, 1, new ButtonResult() {
         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }

         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedLeftMouse() {
                  int var1 = AdvancedBuildModeSymmetry.this.getSymmetryPlanes().getXyExtraDist();
                  AdvancedBuildModeSymmetry.this.getSymmetryPlanes().setXyExtraDist(var1 == 0 ? 1 : 0);
               }

               public void pressedRightMouse() {
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESYMMETRY_10;
         }

         public boolean isHighlighted() {
            return AdvancedBuildModeSymmetry.this.getSymmetryPlanes().getXyExtraDist() == 1;
         }
      });
      this.addButton(var1.getContent(0), 1, 1, new ButtonResult() {
         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.GREEN;
         }

         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedLeftMouse() {
                  int var1 = AdvancedBuildModeSymmetry.this.getSymmetryPlanes().getXzExtraDist();
                  AdvancedBuildModeSymmetry.this.getSymmetryPlanes().setXzExtraDist(var1 == 0 ? 1 : 0);
               }

               public void pressedRightMouse() {
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESYMMETRY_11;
         }

         public boolean isHighlighted() {
            return AdvancedBuildModeSymmetry.this.getSymmetryPlanes().getXzExtraDist() == 1;
         }
      });
      this.addButton(var1.getContent(0), 2, 1, new ButtonResult() {
         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.RED;
         }

         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedLeftMouse() {
                  int var1 = AdvancedBuildModeSymmetry.this.getSymmetryPlanes().getYzExtraDist();
                  AdvancedBuildModeSymmetry.this.getSymmetryPlanes().setYzExtraDist(var1 == 0 ? 1 : 0);
               }

               public void pressedRightMouse() {
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESYMMETRY_12;
         }

         public boolean isHighlighted() {
            return AdvancedBuildModeSymmetry.this.getSymmetryPlanes().getYzExtraDist() == 1;
         }
      });
      var1.addNewTextBox(30);
      this.addCheckbox(var1.getContent(1), 0, 0, new CheckboxResult() {
         public CheckboxCallback initCallback() {
            return null;
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESYMMETRY_9;
         }

         public boolean getDefault() {
            return AdvancedBuildModeSymmetry.this.getSymmetryPlanes().isMirrorCubeShapes();
         }

         public boolean getCurrentValue() {
            return AdvancedBuildModeSymmetry.this.getSymmetryPlanes().isMirrorCubeShapes();
         }

         public void setCurrentValue(boolean var1) {
            AdvancedBuildModeSymmetry.this.getSymmetryPlanes().setMirrorCubeShapes(var1);
         }

         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESYMMETRY_13;
         }
      });
      this.addCheckbox(var1.getContent(1), 0, 1, new CheckboxResult() {
         public CheckboxCallback initCallback() {
            return null;
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESYMMETRY_14;
         }

         public boolean getDefault() {
            return AdvancedBuildModeSymmetry.this.getSymmetryPlanes().isMirrorNonCubicShapes();
         }

         public boolean getCurrentValue() {
            return AdvancedBuildModeSymmetry.this.getSymmetryPlanes().isMirrorNonCubicShapes();
         }

         public void setCurrentValue(boolean var1) {
            AdvancedBuildModeSymmetry.this.getSymmetryPlanes().setMirrorNonCubicShapes(var1);
         }

         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESYMMETRY_15;
         }
      });
   }

   public String getId() {
      return "BSYMMETRY";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESYMMETRY_7;
   }

   class SymResult extends ButtonResult {
      private final int mode;

      public SymResult(int var2) {
         this.mode = var2;
      }

      public GUIHorizontalArea.HButtonColor getColor() {
         switch(this.mode) {
         case 1:
            return GUIHorizontalArea.HButtonColor.BLUE;
         case 2:
            return GUIHorizontalArea.HButtonColor.GREEN;
         case 3:
         default:
            throw new RuntimeException("Mode fail: " + this.mode);
         case 4:
            return GUIHorizontalArea.HButtonColor.RED;
         }
      }

      public ButtonCallback initCallback() {
         return new ButtonCallback() {
            public void pressedRightMouse() {
               AdvancedBuildModeSymmetry.this.getSymmetryPlanes().setPlaceMode(0);
            }

            public void pressedLeftMouse() {
               if (AdvancedBuildModeSymmetry.this.getSymmetryPlanes().getPlaceMode() == 0) {
                  switch(SymResult.this.mode) {
                  case 1:
                     if (AdvancedBuildModeSymmetry.this.getSymmetryPlanes().isXyPlaneEnabled()) {
                        AdvancedBuildModeSymmetry.this.getSymmetryPlanes().setXyPlaneEnabled(false);
                        return;
                     }

                     AdvancedBuildModeSymmetry.this.getSymmetryPlanes().setPlaceMode(SymResult.this.mode);
                     return;
                  case 2:
                     if (AdvancedBuildModeSymmetry.this.getSymmetryPlanes().isXzPlaneEnabled()) {
                        AdvancedBuildModeSymmetry.this.getSymmetryPlanes().setXzPlaneEnabled(false);
                        return;
                     }

                     AdvancedBuildModeSymmetry.this.getSymmetryPlanes().setPlaceMode(SymResult.this.mode);
                     return;
                  case 4:
                     if (AdvancedBuildModeSymmetry.this.getSymmetryPlanes().isYzPlaneEnabled()) {
                        AdvancedBuildModeSymmetry.this.getSymmetryPlanes().setYzPlaneEnabled(false);
                        return;
                     } else {
                        AdvancedBuildModeSymmetry.this.getSymmetryPlanes().setPlaceMode(SymResult.this.mode);
                     }
                  case 3:
                  default:
                  }
               } else {
                  AdvancedBuildModeSymmetry.this.getSymmetryPlanes().setPlaceMode(0);
               }
            }
         };
      }

      public String getName() {
         if (AdvancedBuildModeSymmetry.this.getSymmetryPlanes().getPlaceMode() == this.mode) {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESYMMETRY_0;
         } else {
            switch(this.mode) {
            case 1:
               if (AdvancedBuildModeSymmetry.this.getSymmetryPlanes().isXyPlaneEnabled()) {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESYMMETRY_1;
               }

               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESYMMETRY_2;
            case 2:
               if (AdvancedBuildModeSymmetry.this.getSymmetryPlanes().isXzPlaneEnabled()) {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESYMMETRY_3;
               }

               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESYMMETRY_4;
            case 3:
            default:
               throw new RuntimeException("Mode fail: " + this.mode);
            case 4:
               return AdvancedBuildModeSymmetry.this.getSymmetryPlanes().isYzPlaneEnabled() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESYMMETRY_5 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESYMMETRY_6;
            }
         }
      }

      public boolean isHighlighted() {
         switch(this.mode) {
         case 1:
            if (AdvancedBuildModeSymmetry.this.getSymmetryPlanes().isXyPlaneEnabled()) {
               return true;
            }
            break;
         case 2:
            if (AdvancedBuildModeSymmetry.this.getSymmetryPlanes().isXzPlaneEnabled()) {
               return true;
            }
         case 3:
         default:
            break;
         case 4:
            if (AdvancedBuildModeSymmetry.this.getSymmetryPlanes().isYzPlaneEnabled()) {
               return true;
            }
         }

         return false;
      }
   }
}
