package org.schema.game.client.view.gui.advancedbuildmode;

import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.input.KeyboardMappings;

public class AdvancedBuildModeHelp extends AdvancedBuildModeGUISGroup {
   public AdvancedBuildModeHelp(AdvancedGUIElement var1) {
      super(var1);
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      this.addLabel(var1.getContent(0), 0, 0, new LabelResult() {
         public String getName() {
            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEHELP_0, KeyboardMappings.BUILD_MODE_FIX_CAM.getKeyChar());
         }
      });
      var1.addNewTextBox(30);
      this.addLabel(var1.getContent(1), 0, 0, new LabelResult() {
         public String getName() {
            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEHELP_1, KeyboardMappings.BUILD_MODE_FIX_CAM.getKeyChar());
         }
      });
   }

   public String getId() {
      return "BHELP";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEHELP_2;
   }
}
