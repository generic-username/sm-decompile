package org.schema.game.client.view.gui.advancedbuildmode;

import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;

public class AdvancedBuildModeBrushSize extends AdvancedBuildModeGUISGroup {
   public AdvancedBuildModeBrushSize(AdvancedGUIElement var1) {
      super(var1);
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      new GUITextOverlay(10, 10, this.getState());
      this.addLabel(var1.getContent(0), 0, 0, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEBRUSHSIZE_0;
         }
      });
      this.addSlider(var1.getContent(0), 0, 1, new AdvancedBuildModeGUISGroup.SizeSliderResult(this.getBuildToolsManager().width) {
         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEBRUSHSIZE_2;
         }

         public long getToolTipDelayMs() {
            return 800L;
         }
      });
      this.addSlider(var1.getContent(0), 0, 2, new AdvancedBuildModeGUISGroup.SizeSliderResult(this.getBuildToolsManager().height) {
         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEBRUSHSIZE_3;
         }

         public long getToolTipDelayMs() {
            return 800L;
         }
      });
      this.addSlider(var1.getContent(0), 0, 3, new AdvancedBuildModeGUISGroup.SizeSliderResult(this.getBuildToolsManager().depth) {
         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEBRUSHSIZE_5;
         }

         public long getToolTipDelayMs() {
            return 800L;
         }
      });
   }

   public String getId() {
      return "BSIZE";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEBRUSHSIZE_1;
   }
}
