package org.schema.game.client.view.gui.advancedbuildmode;

import org.schema.game.client.controller.manager.ingame.BuildInstruction;
import org.schema.game.client.controller.manager.ingame.BuildSelection;
import org.schema.game.client.controller.manager.ingame.BuildSelectionCopy;
import org.schema.game.client.controller.manager.ingame.BuildSelectionFillHelper;
import org.schema.game.client.controller.manager.ingame.FillTool;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.ButtonCallback;
import org.schema.game.client.view.gui.advanced.tools.ButtonResult;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;

public class AdvancedBuildModeFill extends AdvancedBuildModeGUISGroup {
   public AdvancedBuildModeFill(AdvancedGUIElement var1) {
      super(var1);
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      this.addButton(var1.getContent(0), 0, 0, new ButtonResult() {
         public GUIHorizontalArea.HButtonColor getColor() {
            return AdvancedBuildModeFill.this.getBuildToolsManager().getFillTool() == null && !(AdvancedBuildModeFill.this.getBuildToolsManager().getSelectMode() instanceof BuildSelectionFillHelper) ? GUIHorizontalArea.HButtonColor.BLUE : GUIHorizontalArea.HButtonColor.ORANGE;
         }

         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  if (!AdvancedBuildModeFill.this.getBuildToolsManager().isSelectMode() && AdvancedBuildModeFill.this.getBuildToolsManager().getFillTool() == null) {
                     AdvancedBuildModeFill.this.getBuildToolsManager().setSelectMode(new BuildSelectionFillHelper());
                  } else {
                     AdvancedBuildModeFill.this.getBuildToolsManager().setSelectMode((BuildSelection)null);
                     AdvancedBuildModeFill.this.getBuildToolsManager().setFillTool((FillTool)null);
                  }
               }
            };
         }

         public boolean isActive() {
            return !AdvancedBuildModeFill.this.getBuildToolsManager().isPasteMode() && !AdvancedBuildModeFill.this.getBuildToolsManager().isCopyMode() && !(AdvancedBuildModeFill.this.getBuildToolsManager().getSelectMode() instanceof BuildSelectionCopy);
         }

         public boolean isHighlighted() {
            return AdvancedBuildModeFill.this.getBuildToolsManager().getFillTool() != null || AdvancedBuildModeFill.this.getBuildToolsManager().getSelectMode() instanceof BuildSelectionFillHelper;
         }

         public String getName() {
            return AdvancedBuildModeFill.this.getBuildToolsManager().getFillTool() == null && !(AdvancedBuildModeFill.this.getBuildToolsManager().getSelectMode() instanceof BuildSelectionFillHelper) ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEFILL_0 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEFILL_1;
         }
      });
      this.addButton(var1.getContent(0), 1, 0, new ButtonResult() {
         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }

         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  if (AdvancedBuildModeFill.this.getBuildToolsManager().getFillTool() != null) {
                     short var1 = AdvancedBuildModeFill.this.getPlayerInteractionControlManager().getSelectedTypeWithSub();
                     AdvancedBuildModeFill.this.getBuildToolsManager().getFillTool().doFill(AdvancedBuildModeFill.this.getBuildToolsManager(), var1, AdvancedBuildModeFill.this.getBuildToolsManager().fill.setting);
                     if (ElementKeyMap.isValidType(var1)) {
                        int var2 = AdvancedBuildModeFill.this.getPlayerInteractionControlManager().getBlockOrientation();
                        boolean var3 = ElementKeyMap.getInfoFast(var1).activateOnPlacement();
                        BuildInstruction var4 = new BuildInstruction(AdvancedBuildModeFill.this.getPlayerInteractionControlManager().getSegmentControlManager().getSegmentController());
                        AdvancedBuildModeFill.this.getBuildToolsManager().getFillTool().place(AdvancedBuildModeFill.this.getPlayerInteractionControlManager(), var1, var2, var3, var4);
                     }
                  }

               }
            };
         }

         public boolean isActive() {
            return super.isActive() && AdvancedBuildModeFill.this.getBuildToolsManager().getFillTool() != null;
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEFILL_2;
         }

         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEFILL_3;
         }
      });
      this.addLabel(var1.getContent(0), 0, 1, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEFILL_4;
         }
      });
      this.addSlider(var1.getContent(0), 0, 2, new AdvancedBuildModeGUISGroup.SizeSliderResult(this.getBuildToolsManager().fill) {
         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEFILL_5;
         }
      });
   }

   public String getId() {
      return "BFILL";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEFILL_6;
   }
}
