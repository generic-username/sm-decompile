package org.schema.game.client.view.gui.advancedbuildmode;

import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.AdvResult;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIResizableGrabbableWindow;
import org.schema.schine.input.KeyboardMappings;

public class AdvancedBuildModeHelpTop extends AdvancedBuildModeGUISGroup {
   public AdvancedBuildModeHelpTop(AdvancedGUIElement var1) {
      super(var1);
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      new GUITextOverlay(10, 10, this.getState());
      this.addLabel(var1.getContent(0), 0, 0, new LabelResult() {
         public String getName() {
            return AdvancedBuildModeHelpTop.this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().isStickyAdvBuildMode() ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEHELPTOP_0, KeyboardMappings.BUILD_MODE_FIX_CAM.getKeyChar()) : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEHELPTOP_1, KeyboardMappings.BUILD_MODE_FIX_CAM.getKeyChar());
         }

         public AdvResult.VerticalAlignment getVerticalAlignment() {
            return AdvResult.VerticalAlignment.TOP;
         }

         public AdvResult.HorizontalAlignment getHorizontalAlignment() {
            return AdvResult.HorizontalAlignment.LEFT;
         }
      });
   }

   public String getId() {
      return "BTOPHELP";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEHELPTOP_2;
   }

   public boolean isDefaultExpanded() {
      return true;
   }

   public int getSubListIndex() {
      return 0;
   }

   public boolean isExpandable() {
      return false;
   }

   public boolean isClosable() {
      return true;
   }

   public void onClosed() {
      GUIResizableGrabbableWindow.setHidden(this.getWindowId(), true);
   }
}
