package org.schema.game.client.view.gui.mail;

import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.input.InputState;

public class GUICreateMailPanel extends GUIInputPanel {
   private GUIActivatableTextBar toTextBar;
   private GUIActivatableTextBar subjectTextBar;
   private GUIActivatableTextBar messageTextBar;
   private String predefinedTo;
   private String predefinedTopic;

   public GUICreateMailPanel(InputState var1, GUICallback var2, String var3, String var4) {
      super("GUICreateMailPanel", var1, 750, 320, var2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUICREATEMAILPANEL_0, "");
      this.predefinedTo = var3;
      this.predefinedTopic = var4;
      this.setOkButtonText(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUICREATEMAILPANEL_1);
      this.onInit();
   }

   public void onInit() {
      super.onInit();
      this.toTextBar = new GUIActivatableTextBar(this.getState(), FontLibrary.FontSize.MEDIUM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUICREATEMAILPANEL_2, ((GUIDialogWindow)this.background).getMainContentPane().getContent(0), new GUICreateMailPanel.DefaultTextCallback(), new GUICreateMailPanel.DefaultTextChangedCallback());
      this.toTextBar.onInit();
      this.toTextBar.appendText(this.predefinedTo);
      this.subjectTextBar = new GUIActivatableTextBar(this.getState(), FontLibrary.FontSize.MEDIUM, 80, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUICREATEMAILPANEL_3, ((GUIDialogWindow)this.background).getMainContentPane().getContent(0), new GUICreateMailPanel.DefaultTextCallback(), new GUICreateMailPanel.DefaultTextChangedCallback());
      this.subjectTextBar.onInit();
      this.subjectTextBar.appendText(this.predefinedTopic);
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(0).attach(this.toTextBar);
      this.subjectTextBar.setPos(0.0F, this.toTextBar.getHeight(), 0.0F);
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(0).attach(this.subjectTextBar);
      ((GUIDialogWindow)this.background).getMainContentPane().setTextBoxHeightLast(52);
      ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(1);
      this.messageTextBar = new GUIActivatableTextBar(this.getState(), FontLibrary.FontSize.MEDIUM, 420, 8, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUICREATEMAILPANEL_4, ((GUIDialogWindow)this.background).getMainContentPane().getContent(1), new GUICreateMailPanel.DefaultTextCallback(), new GUICreateMailPanel.DefaultTextChangedCallback()) {
      };
      this.messageTextBar.onInit();
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(1).attach(this.messageTextBar);
   }

   public String getTo() {
      return this.toTextBar.getText();
   }

   public String getSubject() {
      return this.subjectTextBar.getText();
   }

   public String getMessage() {
      return this.messageTextBar.getText();
   }

   class DefaultTextCallback implements TextCallback {
      private DefaultTextCallback() {
      }

      public String[] getCommandPrefixes() {
         return null;
      }

      public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
         return GUICreateMailPanel.this.getState().onAutoComplete(var1, var2, "#");
      }

      public void onFailedTextCheck(String var1) {
      }

      public void onTextEnter(String var1, boolean var2, boolean var3) {
      }

      public void newLine() {
      }

      // $FF: synthetic method
      DefaultTextCallback(Object var2) {
         this();
      }
   }

   class DefaultTextChangedCallback implements OnInputChangedCallback {
      private DefaultTextChangedCallback() {
      }

      public String onInputChanged(String var1) {
         return var1;
      }

      // $FF: synthetic method
      DefaultTextChangedCallback(Object var2) {
         this();
      }
   }
}
