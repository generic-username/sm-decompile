package org.schema.game.client.view.gui.mail;

import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerMailInputNew;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.data.player.playermessage.PlayerMessage;
import org.schema.game.common.data.player.playermessage.PlayerMessageController;
import org.schema.game.network.objects.remote.RemotePlayerMessage;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.input.InputState;

public class GUIMailPanelNew extends GUIInputPanel {
   private PlayerMessageController messageController = ((GameClientState)this.getState()).getController().getClientChannel().getPlayerMessageController();

   public GUIMailPanelNew(InputState var1, int var2, int var3, GUICallback var4) {
      super("GUIMailPanelNew", var1, var2, var3, var4, "Mail", "");
      this.setOkButton(false);
   }

   public void onInit() {
      super.onInit();
      GUIHorizontalButtonTablePane var1;
      (var1 = new GUIHorizontalButtonTablePane(this.getState(), 2, 2, ((GUIDialogWindow)this.background).getMainContentPane().getContent(0))).onInit();
      var1.activeInterface = new GUIActiveInterface() {
         public boolean isActive() {
            return GUIMailPanelNew.this.isActive();
         }
      };
      var1.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUIMAILPANELNEW_0, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerMailInputNew((GameClientState)GUIMailPanelNew.this.getState())).activate();
            }

         }

         public boolean isOccluded() {
            return !GUIMailPanelNew.this.isActive();
         }
      }, (GUIActivationCallback)null);
      var1.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUIMAILPANELNEW_1, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !GUIMailPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               for(int var3 = 0; var3 < GUIMailPanelNew.this.messageController.messagesReceived.size(); ++var3) {
                  PlayerMessage var4;
                  if (!(var4 = (PlayerMessage)GUIMailPanelNew.this.messageController.messagesReceived.get(var3)).isRead()) {
                     var4.setRead(true);
                     ((GameClientState)GUIMailPanelNew.this.getState()).getController().getClientChannel().getNetworkObject().playerMessageBuffer.add(new RemotePlayerMessage(var4, false));
                  }
               }
            }

         }
      }, (GUIActivationCallback)null);
      var1.addButton(1, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUIMAILPANELNEW_2, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !GUIMailPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerGameOkCancelInput("GUIMailPanelNew_DELETE_CONFIRM", (GameClientState)GUIMailPanelNew.this.getState(), 300, 100, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUIMAILPANELNEW_3, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUIMAILPANELNEW_4) {
                  public boolean isOccluded() {
                     return false;
                  }

                  public void onDeactivate() {
                  }

                  public void pressedOK() {
                     GUIMailPanelNew.this.messageController.deleteAllMessagesClient();
                     this.deactivate();
                  }
               }).activate();
            }

         }
      }, (GUIActivationCallback)null);
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(0).attach(var1);
      ((GUIDialogWindow)this.background).getMainContentPane().setTextBoxHeightLast(56);
      ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(1);
      GUIMailScrollableList var2;
      (var2 = new GUIMailScrollableList(this.getState(), ((GUIDialogWindow)this.background).getMainContentPane().getContent(1))).onInit();
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(1).attach(var2);
   }

   public boolean isActive() {
      return (this.getState().getController().getPlayerInputs().isEmpty() || ((DialogInterface)this.getState().getController().getPlayerInputs().get(this.getState().getController().getPlayerInputs().size() - 1)).getInputPanel() == this) && super.isActive();
   }
}
