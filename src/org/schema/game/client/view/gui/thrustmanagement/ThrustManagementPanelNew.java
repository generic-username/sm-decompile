package org.schema.game.client.view.gui.thrustmanagement;

import PolygonStatsInterface.PolygonStatsEditableInterface;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerThrustManagerInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ThrustConfiguration;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIPolygonStats;
import org.schema.schine.graphicsengine.forms.gui.newgui.PolygonStatsInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.settingsnew.GUIScrollSettingSelector;
import org.schema.schine.input.InputState;

public class ThrustManagementPanelNew extends GUIMainWindow implements PolygonStatsEditableInterface, GUIActiveInterface, PolygonStatsInterface {
   private PlayerThrustManagerInput mainMenu;
   private boolean init;
   private GUIContentPane generalTab;
   private GUIPolygonStats st;
   private final Ship ship;
   private final ThrustConfiguration tc;

   public ThrustManagementPanelNew(InputState var1, PlayerThrustManagerInput var2, Ship var3) {
      super(var1, 800, 500, "TPANNA");
      this.mainMenu = var2;
      this.ship = var3;
      if (var3 != null) {
         this.tc = var3.getManagerContainer().thrustConfiguration;
      } else {
         this.tc = null;
      }
   }

   public void cleanUp() {
      this.generalTab.cleanUp();
      this.st.cleanUp();
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      super.draw();
   }

   public void update(Timer var1) {
      if (this.st != null) {
         this.st.update();
      }

   }

   public void onInit() {
      super.onInit();
      this.recreateTabs();
      this.orientate(48);
      this.init = true;
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public void recreateTabs() {
      Object var1 = null;
      if (this.getSelectedTab() < this.getTabs().size()) {
         var1 = ((GUIContentPane)this.getTabs().get(this.getSelectedTab())).getTabName();
      }

      this.clearTabs();
      this.generalTab = this.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_THRUSTMANAGEMENT_THRUSTMANAGEMENTPANELNEW_0);
      this.createGeneralPane();
      if (var1 != null) {
         for(int var2 = 0; var2 < this.getTabs().size(); ++var2) {
            if (((GUIContentPane)this.getTabs().get(var2)).getTabName().equals(var1)) {
               this.setSelectedTab(var2);
               return;
            }
         }
      }

   }

   private void createGeneralPane() {
      this.generalTab.setTextBoxHeightLast(186);
      GUIHorizontalButtonTablePane var1;
      (var1 = new GUIHorizontalButtonTablePane(this.getState(), 2, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_THRUSTMANAGEMENT_THRUSTMANAGEMENTPANELNEW_1, this.generalTab.getContent(0))).onInit();
      this.generalTab.getContent(0).attach(var1);
      var1.addButton(0, 0, new Object() {
         public String toString() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_THRUSTMANAGEMENT_THRUSTMANAGEMENTPANELNEW_2;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && ThrustManagementPanelNew.this.ship != null) {
               ThrustManagementPanelNew.this.ship.requestAutomaticDampeners(!ThrustManagementPanelNew.this.ship.isAutomaticDampeners());
            }

         }

         public boolean isOccluded() {
            return !ThrustManagementPanelNew.this.mainMenu.isActive();
         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }

         public boolean isHighlighted(InputState var1) {
            return ThrustManagementPanelNew.this.ship != null && ThrustManagementPanelNew.this.ship.isAutomaticDampeners();
         }
      });
      var1.addButton(1, 0, new Object() {
         public String toString() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_THRUSTMANAGEMENT_THRUSTMANAGEMENTPANELNEW_3;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && ThrustManagementPanelNew.this.ship != null) {
               ThrustManagementPanelNew.this.ship.requestAutomaticDampenersReactivate(!ThrustManagementPanelNew.this.ship.isAutomaticReactivateDampeners());
            }

         }

         public boolean isOccluded() {
            return !ThrustManagementPanelNew.this.mainMenu.isActive();
         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }

         public boolean isHighlighted(InputState var1) {
            return ThrustManagementPanelNew.this.ship != null && ThrustManagementPanelNew.this.ship.isAutomaticReactivateDampeners();
         }
      });
      (var1 = new GUIHorizontalButtonTablePane(this.getState(), 4, 1, this.generalTab.getContent(0))).onInit();
      this.generalTab.getContent(0).attach(var1);
      var1.setPos(0.0F, 58.0F, 0.0F);
      var1.addButton(0, 0, new Object() {
         public String toString() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_THRUSTMANAGEMENT_THRUSTMANAGEMENTPANELNEW_4;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && ThrustManagementPanelNew.this.ship != null) {
               ThrustManagementPanelNew.this.tc.bufferThrusterBalanceChange(new Vector3f(0.33333F, 0.33333F, 0.33333F), ThrustManagementPanelNew.this.ship.getManagerContainer().getThrusterElementManager().rotationBalance, ThrustManagementPanelNew.this.ship.getManagerContainer().getRepulseManager().getThrustToRepul());
            }

         }

         public boolean isOccluded() {
            return !ThrustManagementPanelNew.this.mainMenu.isActive();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      var1.addButton(1, 0, new Object() {
         public String toString() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_THRUSTMANAGEMENT_THRUSTMANAGEMENTPANELNEW_5;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && ThrustManagementPanelNew.this.ship != null) {
               ThrustManagementPanelNew.this.tc.bufferThrusterBalanceChange(new Vector3f(0.0F, 0.0F, 1.0F), ThrustManagementPanelNew.this.ship.getManagerContainer().getThrusterElementManager().rotationBalance, ThrustManagementPanelNew.this.ship.getManagerContainer().getRepulseManager().getThrustToRepul());
            }

         }

         public boolean isOccluded() {
            return !ThrustManagementPanelNew.this.mainMenu.isActive();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      var1.addButton(2, 0, new Object() {
         public String toString() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_THRUSTMANAGEMENT_THRUSTMANAGEMENTPANELNEW_6;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && ThrustManagementPanelNew.this.ship != null) {
               ThrustManagementPanelNew.this.tc.bufferThrusterBalanceChange(new Vector3f(0.0F, 1.0F, 0.0F), ThrustManagementPanelNew.this.ship.getManagerContainer().getThrusterElementManager().rotationBalance, ThrustManagementPanelNew.this.ship.getManagerContainer().getRepulseManager().getThrustToRepul());
            }

         }

         public boolean isOccluded() {
            return !ThrustManagementPanelNew.this.mainMenu.isActive();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      var1.addButton(3, 0, new Object() {
         public String toString() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_THRUSTMANAGEMENT_THRUSTMANAGEMENTPANELNEW_7;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && ThrustManagementPanelNew.this.ship != null) {
               ThrustManagementPanelNew.this.tc.bufferThrusterBalanceChange(new Vector3f(1.0F, 0.0F, 0.0F), ThrustManagementPanelNew.this.ship.getManagerContainer().getThrusterElementManager().rotationBalance, ThrustManagementPanelNew.this.ship.getManagerContainer().getRepulseManager().getThrustToRepul());
            }

         }

         public boolean isOccluded() {
            return !ThrustManagementPanelNew.this.mainMenu.isActive();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      (var1 = new GUIHorizontalButtonTablePane(this.getState(), 1, 1, this.generalTab.getContent(0))).onInit();
      this.generalTab.getContent(0).attach(var1);
      var1.setPos(0.0F, 83.0F, 0.0F);
      var1.addButton(0, 0, new Object() {
         public String toString() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_THRUSTMANAGEMENT_THRUSTMANAGEMENTPANELNEW_8;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && ThrustManagementPanelNew.this.ship != null) {
               ThrustManagementPanelNew.this.ship.requestThrustSharing(!ThrustManagementPanelNew.this.tc.thrustSharing);
            }

         }

         public boolean isOccluded() {
            return !ThrustManagementPanelNew.this.mainMenu.isActive();
         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return ThrustManagementPanelNew.this.ship != null && !ThrustManagementPanelNew.this.ship.railController.next.isEmpty();
         }

         public boolean isHighlighted(InputState var1) {
            return ThrustManagementPanelNew.this.ship != null && !ThrustManagementPanelNew.this.ship.railController.next.isEmpty() && ThrustManagementPanelNew.this.tc.thrustSharing;
         }
      });
      GUITextOverlay var5;
      (var5 = new GUITextOverlay(10, 10, this.getState())).setTextSimple(new Object() {
         public String toString() {
            return ThrustManagementPanelNew.this.ship != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_THRUSTMANAGEMENT_THRUSTMANAGEMENTPANELNEW_9, String.valueOf(Math.round(ThrustManagementPanelNew.this.tc.queuedThrustBuffer.w * 100.0F))) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_THRUSTMANAGEMENT_THRUSTMANAGEMENTPANELNEW_14;
         }
      });
      GUIScrollSettingSelector var2 = new GUIScrollSettingSelector(this.getState(), GUIScrollablePanel.SCROLLABLE_HORIZONTAL, 180) {
         public boolean isVerticalActive() {
            return false;
         }

         public boolean showLabel() {
            return false;
         }

         public void settingChanged(Object var1) {
            super.settingChanged(var1);
            if (ThrustManagementPanelNew.this.ship != null && var1 != null) {
               ThrustManagementPanelNew.this.tc.bufferThrusterBalanceChange(new Vector3f(ThrustManagementPanelNew.this.tc.queuedThrustBuffer.x, ThrustManagementPanelNew.this.tc.queuedThrustBuffer.y, ThrustManagementPanelNew.this.tc.queuedThrustBuffer.z), ThrustManagementPanelNew.this.tc.queuedThrustBuffer.w, ThrustManagementPanelNew.this.tc.queuedRepulsorBuffer);
            }

         }

         protected void setSettingY(float var1) {
         }

         protected void setSettingX(float var1) {
            if (ThrustManagementPanelNew.this.ship != null) {
               ThrustManagementPanelNew.this.tc.queuedThrustBuffer.w = var1 * 0.01F;
               this.settingChanged(Math.round(ThrustManagementPanelNew.this.tc.queuedThrustBuffer.w * 100.0F));
            }

         }

         protected void incSetting() {
            if (ThrustManagementPanelNew.this.ship != null) {
               Vector4f var10000 = ThrustManagementPanelNew.this.tc.queuedThrustBuffer;
               var10000.w = (float)((double)var10000.w + 0.1D);
               this.settingChanged(Math.round(ThrustManagementPanelNew.this.tc.queuedThrustBuffer.w * 100.0F));
            }

         }

         protected float getSettingY() {
            return 0.0F;
         }

         protected float getSettingX() {
            return ThrustManagementPanelNew.this.ship != null ? ThrustManagementPanelNew.this.tc.queuedThrustBuffer.w * 100.0F : 0.0F;
         }

         public float getMaxY() {
            return 0.0F;
         }

         public float getMaxX() {
            return 100.0F;
         }

         protected void decSetting() {
            if (ThrustManagementPanelNew.this.ship != null) {
               Vector4f var10000 = ThrustManagementPanelNew.this.tc.queuedThrustBuffer;
               var10000.w = (float)((double)var10000.w - 0.1D);
               this.settingChanged(Math.round(ThrustManagementPanelNew.this.tc.queuedThrustBuffer.w * 100.0F));
            }

         }

         public float getMinX() {
            return 0.0F;
         }

         public float getMinY() {
            return 0.0F;
         }

         public void draw() {
            this.setWidth((int)(ThrustManagementPanelNew.this.generalTab.getContent(0).getWidth() - 194.0F));
            super.draw();
         }
      };
      var5.setPos(3.0F, 119.0F, 0.0F);
      var2.setPos(140.0F, 107.0F, 0.0F);
      GUITextOverlay var3;
      (var3 = new GUITextOverlay(10, 10, this.getState())).setTextSimple(new Object() {
         public String toString() {
            return ThrustManagementPanelNew.this.ship != null && ThrustManagementPanelNew.this.ship.getManagerContainer().hasRepulsors() ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_THRUSTMANAGEMENT_THRUSTMANAGEMENTPANELNEW_15, String.valueOf(Math.round(ThrustManagementPanelNew.this.tc.queuedRepulsorBuffer * 100.0F))) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_THRUSTMANAGEMENT_THRUSTMANAGEMENTPANELNEW_16;
         }
      });
      GUIScrollSettingSelector var4 = new GUIScrollSettingSelector(this.getState(), GUIScrollablePanel.SCROLLABLE_HORIZONTAL, 180) {
         public boolean isVerticalActive() {
            return false;
         }

         public boolean showLabel() {
            return false;
         }

         public void settingChanged(Object var1) {
            super.settingChanged(var1);
            if (ThrustManagementPanelNew.this.ship != null && var1 != null) {
               ThrustManagementPanelNew.this.tc.bufferThrusterBalanceChange(new Vector3f(ThrustManagementPanelNew.this.tc.queuedThrustBuffer.x, ThrustManagementPanelNew.this.tc.queuedThrustBuffer.y, ThrustManagementPanelNew.this.tc.queuedThrustBuffer.z), ThrustManagementPanelNew.this.tc.queuedThrustBuffer.w, ThrustManagementPanelNew.this.tc.queuedRepulsorBuffer);
            }

         }

         protected void setSettingY(float var1) {
         }

         protected void setSettingX(float var1) {
            if (ThrustManagementPanelNew.this.ship != null) {
               ThrustManagementPanelNew.this.tc.queuedRepulsorBuffer = var1 * 0.01F;
               this.settingChanged(Math.round(ThrustManagementPanelNew.this.tc.queuedRepulsorBuffer * 100.0F));
            }

         }

         protected void incSetting() {
            if (ThrustManagementPanelNew.this.ship != null) {
               ThrustConfiguration var10000 = ThrustManagementPanelNew.this.tc;
               var10000.queuedRepulsorBuffer = (float)((double)var10000.queuedRepulsorBuffer + 0.1D);
               this.settingChanged(Math.round(ThrustManagementPanelNew.this.tc.queuedRepulsorBuffer * 100.0F));
            }

         }

         protected float getSettingY() {
            return 0.0F;
         }

         protected float getSettingX() {
            return ThrustManagementPanelNew.this.ship != null ? ThrustManagementPanelNew.this.tc.queuedRepulsorBuffer * 100.0F : 0.0F;
         }

         public float getMaxY() {
            return 0.0F;
         }

         public float getMaxX() {
            return 100.0F;
         }

         protected void decSetting() {
            if (ThrustManagementPanelNew.this.ship != null) {
               ThrustConfiguration var10000 = ThrustManagementPanelNew.this.tc;
               var10000.queuedRepulsorBuffer = (float)((double)var10000.queuedRepulsorBuffer - 0.1D);
               this.settingChanged(Math.round(ThrustManagementPanelNew.this.tc.queuedRepulsorBuffer * 100.0F));
            }

         }

         public float getMinX() {
            return 0.0F;
         }

         public float getMinY() {
            return 0.0F;
         }

         public void draw() {
            this.setWidth((int)(ThrustManagementPanelNew.this.generalTab.getContent(0).getWidth() - 194.0F));
            super.draw();
         }
      };
      var3.setPos(3.0F, 147.0F, 0.0F);
      var4.setPos(140.0F, 135.0F, 0.0F);
      this.generalTab.getContent(0).attach(var4);
      this.generalTab.getContent(0).attach(var3);
      this.generalTab.getContent(0).attach(var2);
      this.generalTab.getContent(0).attach(var5);
      this.generalTab.addNewTextBox(100);
      GUIScrollablePanel var6 = new GUIScrollablePanel(10.0F, 10.0F, this.generalTab.getContent(1), this.getState());
      this.st = new GUIPolygonStats(this.getState(), this) {
         public void draw() {
            float var1 = ThrustManagementPanelNew.this.generalTab.getContent(1).getHeight();
            this.setHeight((int)var1);
            this.setWidth((int)var1);
            this.setPos((float)((int)(ThrustManagementPanelNew.this.generalTab.getContent(1).getWidth() / 2.0F - this.getWidth() / 2.0F)), (float)((int)(ThrustManagementPanelNew.this.generalTab.getContent(1).getHeight() / 2.0F - this.getHeight() / 2.0F)), 0.0F);
            super.draw();
         }
      };
      var6.setContent(this.st);
      this.st.setEditable(this);
      var6.onInit();
      this.generalTab.getContent(1).attach(var6);
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFactionManager().getFaction(this.getOwnPlayer().getFactionId());
   }

   public int getDataPointsNum() {
      return 3;
   }

   public double getPercent(int var1) {
      if (this.ship != null) {
         if (var1 == 0) {
            return (double)this.tc.queuedThrustBuffer.x;
         } else {
            return var1 == 1 ? (double)this.tc.queuedThrustBuffer.y : (double)this.tc.queuedThrustBuffer.z;
         }
      } else {
         return 0.0D;
      }
   }

   public void setPercent(int var1, float var2) {
      if (this.ship != null) {
         if (var1 == 0) {
            this.tc.queuedThrustBuffer.x = 1.0F - var2;
         } else if (var1 == 1) {
            this.tc.queuedThrustBuffer.y = 1.0F - var2;
         } else {
            this.tc.queuedThrustBuffer.z = 1.0F - var2;
         }

         this.tc.bufferThrusterBalanceChange(new Vector3f(this.tc.queuedThrustBuffer.x, this.tc.queuedThrustBuffer.y, this.tc.queuedThrustBuffer.z), this.tc.queuedThrustBuffer.w, this.tc.queuedRepulsorBuffer);
      }

   }

   public double getValue(int var1) {
      if (this.ship != null) {
         if (var1 == 0) {
            return (double)this.tc.queuedThrustBuffer.x;
         } else {
            return var1 == 1 ? (double)this.tc.queuedThrustBuffer.y : (double)this.tc.queuedThrustBuffer.z;
         }
      } else {
         return 0.0D;
      }
   }

   public String getValueName(int var1) {
      if (this.ship != null) {
         float var2 = 1.0F - this.tc.queuedThrustBuffer.w;
         float var3 = 1.0F - this.tc.queuedRepulsorBuffer;
         if (!this.ship.getManagerContainer().hasRepulsors()) {
            var3 = 1.0F;
         }

         var2 = Math.max(0.0F, Math.min(1.0F, (var3 + var2) / 2.0F));
         return (var1 == 0 ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_THRUSTMANAGEMENT_THRUSTMANAGEMENTPANELNEW_11 : (var1 == 1 ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_THRUSTMANAGEMENT_THRUSTMANAGEMENTPANELNEW_12 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_THRUSTMANAGEMENT_THRUSTMANAGEMENTPANELNEW_13)) + Math.round(this.getValue(var1) * 100.0D) + "% (" + Math.round(this.getValue(var1) * (double)var2 * 100.0D) + "%)";
      } else {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_THRUSTMANAGEMENT_THRUSTMANAGEMENTPANELNEW_10;
      }
   }
}
