package org.schema.game.client.view.gui.advancedstats;

import javax.vecmath.Vector3f;
import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.StatLabelResult;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.ManagerThrustInterface;
import org.schema.game.common.controller.elements.thrust.ThrusterCollectionManager;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;

public class AdvancedStructureStatsThruster extends AdvancedStructureStatsGUISGroup {
   public AdvancedStructureStatsThruster(AdvancedGUIElement var1) {
      super(var1);
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      this.addStatLabel(var1.getContent(0), 0, 0, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSTHRUSTER_1;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedStructureStatsThruster.this.getMan() instanceof ManagerThrustInterface ? StringTools.formatSmallAndBig(((ThrusterCollectionManager)((ManagerThrustInterface)AdvancedStructureStatsThruster.this.getMan()).getThrust().getCollectionManager()).getTotalSize()) : "";
         }

         public int getStatDistance() {
            return AdvancedStructureStatsThruster.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, 1, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSTHRUSTER_2;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            assert AdvancedStructureStatsThruster.this.getMan() != null;

            return AdvancedStructureStatsThruster.this.getMan() instanceof ManagerThrustInterface ? StringTools.formatSmallAndBig(((ManagerThrustInterface)AdvancedStructureStatsThruster.this.getMan()).getThrusterElementManager().getActualThrust()) : "";
         }

         public int getStatDistance() {
            return AdvancedStructureStatsThruster.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, 2, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSTHRUSTER_3;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedStructureStatsThruster.this.getMan() instanceof ManagerThrustInterface ? StringTools.formatPointZero(((Ship)AdvancedStructureStatsThruster.this.getSegCon()).getCurrentMaxVelocity()) : "";
         }

         public int getStatDistance() {
            return AdvancedStructureStatsThruster.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, 3, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSTHRUSTER_4;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedStructureStatsThruster.this.getMan() instanceof ManagerThrustInterface ? StringTools.formatPointZero(((ManagerThrustInterface)AdvancedStructureStatsThruster.this.getMan()).getThrusterElementManager().getPowerConsumedPerSecondCharging()) : "";
         }

         public int getStatDistance() {
            return AdvancedStructureStatsThruster.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, 4, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSTHRUSTER_6;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedStructureStatsThruster.this.getMan() instanceof ManagerThrustInterface ? StringTools.formatPointZero(((ManagerThrustInterface)AdvancedStructureStatsThruster.this.getMan()).getThrusterElementManager().getThrustMassRatio()) : "";
         }

         public int getStatDistance() {
            return AdvancedStructureStatsThruster.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, 5, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSTHRUSTER_7;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedStructureStatsThruster.this.getMan() instanceof ManagerThrustInterface) {
               Vector3f var1 = ((Ship)AdvancedStructureStatsThruster.this.getSegCon()).getOrientationForce();
               return StringTools.formatPointZero(var1.x) + ", " + StringTools.formatPointZero(var1.y) + ", " + StringTools.formatPointZero(var1.z);
            } else {
               return "";
            }
         }

         public int getStatDistance() {
            return AdvancedStructureStatsThruster.this.getTextDist();
         }
      });
      if (this.hasIntegrity()) {
         this.addStatLabel(var1.getContent(0), 0, 6, new StatLabelResult() {
            public String getName() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSTHRUSTER_5;
            }

            public FontLibrary.FontSize getFontSize() {
               return FontLibrary.FontSize.MEDIUM;
            }

            public String getValue() {
               if (AdvancedStructureStatsThruster.this.getMan() instanceof ManagerThrustInterface) {
                  double var1;
                  return (var1 = ((ManagerThrustInterface)AdvancedStructureStatsThruster.this.getMan()).getThrusterElementManager().lowestIntegrity) == Double.POSITIVE_INFINITY ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSTHRUSTER_9 : String.valueOf((int)var1);
               } else {
                  return "";
               }
            }

            public int getStatDistance() {
               return AdvancedStructureStatsThruster.this.getTextDist();
            }
         });
      }

   }

   private int getTextDist() {
      return 150;
   }

   public String getId() {
      return "ASTHRUSTER";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSTHRUSTER_8;
   }
}
