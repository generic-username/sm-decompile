package org.schema.game.client.view.gui.advancedstats;

import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.AdvResult;
import org.schema.game.client.view.gui.advanced.tools.ButtonCallback;
import org.schema.game.client.view.gui.advanced.tools.ButtonResult;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.game.client.view.gui.advanced.tools.StatLabelResult;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;

public class AdvancedStructureStatsDocks extends AdvancedStructureStatsGUISGroup {
   public AdvancedStructureStatsDocks(AdvancedGUIElement var1) {
      super(var1);
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      this.addStatLabel(var1.getContent(0), 0, 0, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_12;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            assert AdvancedStructureStatsDocks.this.getMan() != null;

            return StringTools.formatSmallAndBig(AdvancedStructureStatsDocks.this.getSegCon().railController.getNormalDockCount());
         }

         public int getStatDistance() {
            return AdvancedStructureStatsDocks.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, 1, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_2;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            assert AdvancedStructureStatsDocks.this.getMan() != null;

            return StringTools.formatSmallAndBig(AdvancedStructureStatsDocks.this.getSegCon().railController.getTurretCount());
         }

         public int getStatDistance() {
            return AdvancedStructureStatsDocks.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, 2, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_1;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            assert AdvancedStructureStatsDocks.this.getMan() != null;

            return StringTools.formatSmallAndBig(AdvancedStructureStatsDocks.this.getSegCon().railController.next.size());
         }

         public int getStatDistance() {
            return AdvancedStructureStatsDocks.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, 3, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_13;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            assert AdvancedStructureStatsDocks.this.getMan() != null;

            return StringTools.formatSmallAndBig(AdvancedStructureStatsDocks.this.getSegCon().railController.getTotalDockedFromHere());
         }

         public int getStatDistance() {
            return AdvancedStructureStatsDocks.this.getTextDist();
         }
      });
      this.addLabel(var1.getContent(0), 0, 4, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_14;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }

         public AdvResult.HorizontalAlignment getHorizontalAlignment() {
            return AdvResult.HorizontalAlignment.MID;
         }
      });
      this.addButton(var1.getContent(0), 0, 5, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  (new PlayerGameOkCancelInput("CONFIRM", AdvancedStructureStatsDocks.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_15, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_19) {
                     public boolean isOccluded() {
                        return false;
                     }

                     public void onDeactivate() {
                     }

                     public void pressedOK() {
                        this.deactivate();
                        AdvancedStructureStatsDocks.this.getSegCon().railController.undockAllClient();
                     }
                  }).activate();
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_17;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.ORANGE;
         }
      });
      this.addButton(var1.getContent(0), 1, 5, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  (new PlayerGameOkCancelInput("CONFIRM", AdvancedStructureStatsDocks.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_4, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_5) {
                     public boolean isOccluded() {
                        return false;
                     }

                     public void onDeactivate() {
                     }

                     public void pressedOK() {
                        this.deactivate();
                        AdvancedStructureStatsDocks.this.getSegCon().railController.undockAllClientTurret();
                     }
                  }).activate();
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_3;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.ORANGE;
         }
      });
      this.addButton(var1.getContent(0), 2, 5, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  (new PlayerGameOkCancelInput("CONFIRM", AdvancedStructureStatsDocks.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_18, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_16) {
                     public boolean isOccluded() {
                        return false;
                     }

                     public void onDeactivate() {
                     }

                     public void pressedOK() {
                        this.deactivate();
                        AdvancedStructureStatsDocks.this.getSegCon().railController.undockAllClientNormal();
                     }
                  }).activate();
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_6;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.ORANGE;
         }
      });
      this.addLabel(var1.getContent(0), 0, 6, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_7;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }

         public AdvResult.HorizontalAlignment getHorizontalAlignment() {
            return AdvResult.HorizontalAlignment.MID;
         }
      });
      this.addButton(var1.getContent(0), 0, 7, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  AdvancedStructureStatsDocks.this.getSegCon().railController.getRoot().railController.activateAllAIClient(true, true, false);
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_8;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.GREEN;
         }
      });
      this.addButton(var1.getContent(0), 1, 7, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  AdvancedStructureStatsDocks.this.getSegCon().railController.getRoot().railController.activateAllAIClient(false, true, false);
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_9;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.RED;
         }
      });
      this.addButton(var1.getContent(0), 0, 8, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  AdvancedStructureStatsDocks.this.getSegCon().railController.getRoot().railController.activateAllAIClient(true, false, true);
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_10;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.GREEN;
         }
      });
      this.addButton(var1.getContent(0), 1, 8, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  AdvancedStructureStatsDocks.this.getSegCon().railController.getRoot().railController.activateAllAIClient(false, false, true);
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_11;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.RED;
         }
      });
   }

   private int getTextDist() {
      return 150;
   }

   public String getId() {
      return "ASDOCKS";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSDOCKS_0;
   }
}
