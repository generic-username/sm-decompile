package org.schema.game.client.view.gui.advancedstats;

import java.util.Locale;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.ButtonCallback;
import org.schema.game.client.view.gui.advanced.tools.ButtonResult;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.game.client.view.gui.advanced.tools.StatLabelResult;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.StationaryManagerContainer;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.SimplePlayerCommands;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;

public class AdvancedStructureStatsFaction extends AdvancedStructureStatsGUISGroup {
   private Vector3i tmp = new Vector3i();
   private Vector4f selected = new Vector4f(0.5F, 0.9F, 0.4F, 1.0F);
   private Vector4f uselected = new Vector4f(0.8F, 0.2F, 0.4F, 1.0F);
   private Vector4f blue = new Vector4f(0.2F, 0.4F, 0.8F, 1.0F);
   private Vector4f mouse = new Vector4f(0.5F, 0.4F, 0.2F, 1.0F);

   public AdvancedStructureStatsFaction(AdvancedGUIElement var1) {
      super(var1);
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      this.addLabel(var1.getContent(0), 0, 0, new LabelResult() {
         public String getName() {
            return AdvancedStructureStatsFaction.this.hasFactionBlock() ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_1, ElementCollection.getPosFromIndex(AdvancedStructureStatsFaction.this.getMan().getFactionBlockPos(), AdvancedStructureStatsFaction.this.tmp).toStringPure()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_30;
         }
      });
      this.addStatLabel(var1.getContent(0), 0, 1, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_7;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return FactionManager.getFactionName(AdvancedStructureStatsFaction.this.getSegCon());
         }

         public int getStatDistance() {
            return AdvancedStructureStatsFaction.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, 2, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_26;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedStructureStatsFaction.this.getSegCon().currentOwnerLowerCase.length() > 0 ? AdvancedStructureStatsFaction.this.getSegCon().currentOwnerLowerCase : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_27;
         }

         public int getStatDistance() {
            return AdvancedStructureStatsFaction.this.getTextDist();
         }
      });
      this.addButton(var1.getContent(0), 0, 3, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  if (AdvancedStructureStatsFaction.this.canTakeOwnerShip()) {
                     AdvancedStructureStatsFaction.this.getState().getPlayer().getFactionController().sendEntityOwnerChangeRequest(AdvancedStructureStatsFaction.this.getSegCon().currentOwnerLowerCase.length() > 0 ? "" : AdvancedStructureStatsFaction.this.getState().getPlayer().getName().toLowerCase(Locale.ENGLISH), AdvancedStructureStatsFaction.this.getSegCon());
                  }
               }
            };
         }

         public boolean isActive() {
            return super.isActive() && AdvancedStructureStatsFaction.this.getSegCon() != null && AdvancedStructureStatsFaction.this.canTakeOwnerShip();
         }

         public String getName() {
            return AdvancedStructureStatsFaction.this.getSegCon().currentOwnerLowerCase.length() > 0 ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_28 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_29;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return AdvancedStructureStatsFaction.this.getSegCon() == null ? GUIHorizontalArea.HButtonColor.RED : GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
      this.addButton(var1.getContent(0), 0, 4, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  if (AdvancedStructureStatsFaction.this.getSegCon().isHomeBaseFor(AdvancedStructureStatsFaction.this.getState().getPlayer().getFactionId())) {
                     AdvancedStructureStatsFaction.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_15, 0.0F);
                  } else if (AdvancedStructureStatsFaction.this.getSegCon().getFactionId() != 0) {
                     AdvancedStructureStatsFaction.this.getState().getPlayer().getFactionController().sendEntityFactionIdChangeRequest(0, AdvancedStructureStatsFaction.this.getSegCon());
                  } else {
                     if (AdvancedStructureStatsFaction.this.getState().getPlayer().getFactionId() != 0) {
                        AdvancedStructureStatsFaction.this.getState().getPlayer().getFactionController().sendEntityFactionIdChangeRequest(AdvancedStructureStatsFaction.this.getState().getPlayer().getFactionId(), AdvancedStructureStatsFaction.this.getSegCon());
                     }

                  }
               }
            };
         }

         public boolean isActive() {
            return super.isActive() && AdvancedStructureStatsFaction.this.hasFactionBlock() && AdvancedStructureStatsFaction.this.hasRights() && (AdvancedStructureStatsFaction.this.getSegCon().getFactionId() != 0 || AdvancedStructureStatsFaction.this.getState().getPlayer().getFactionId() != 0);
         }

         public String getName() {
            if (!AdvancedStructureStatsFaction.this.hasFactionBlock()) {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_2;
            } else {
               return AdvancedStructureStatsFaction.this.getSegCon().getFactionId() != 0 ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_0 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_16;
            }
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return AdvancedStructureStatsFaction.this.getSegCon() != null && AdvancedStructureStatsFaction.this.getSegCon().getFactionId() == 0 ? GUIHorizontalArea.HButtonColor.GREEN : GUIHorizontalArea.HButtonColor.RED;
         }
      });
      this.addButton(var1.getContent(0), 0, 5, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  String var1 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_9;
                  boolean var2 = AdvancedStructureStatsFaction.this.getState().getPlayer().getFactionController().hasHomebase();
                  (new PlayerGameOkCancelInput("CONFIRM", AdvancedStructureStatsFaction.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_10, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_11 + "\n" + (var2 ? var1 : "")) {
                     public boolean isOccluded() {
                        return false;
                     }

                     public void onDeactivate() {
                     }

                     public void pressedOK() {
                        this.deactivate();
                        this.getState().getFactionManager().sendClientHomeBaseChange(this.getState().getPlayer().getName(), this.getState().getPlayer().getFactionId(), AdvancedStructureStatsFaction.this.getSegCon().getUniqueIdentifier());
                     }
                  }).activate();
               }
            };
         }

         public boolean isActive() {
            return super.isActive() && AdvancedStructureStatsFaction.this.getMan() != null && AdvancedStructureStatsFaction.this.hasRights() && AdvancedStructureStatsFaction.this.getMan() instanceof StationaryManagerContainer && AdvancedStructureStatsFaction.this.isSegConOwnFactionExists() && !AdvancedStructureStatsFaction.this.getSegCon().isHomeBaseFor(AdvancedStructureStatsFaction.this.getState().getPlayer().getFactionId());
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_17;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.GREEN;
         }
      });
      this.addButton(var1.getContent(0), 1, 5, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  (new PlayerGameOkCancelInput("CONFIRM", AdvancedStructureStatsFaction.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_12, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_13) {
                     public boolean isOccluded() {
                        return false;
                     }

                     public void onDeactivate() {
                     }

                     public void pressedOK() {
                        this.deactivate();
                        if (this.getState().getPlayer().getFactionId() != 0 && this.getState().getPlayer().getFactionId() == AdvancedStructureStatsFaction.this.getSegCon().getFactionId() && this.getState().getPlayer().getFactionRights() < AdvancedStructureStatsFaction.this.getSegCon().getFactionRights()) {
                           this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_14, 0.0F);
                        } else {
                           this.getState().getFactionManager().sendClientHomeBaseChange(this.getState().getPlayer().getName(), this.getState().getPlayer().getFactionId(), "");
                           this.deactivate();
                        }
                     }
                  }).activate();
               }
            };
         }

         public boolean isActive() {
            return super.isActive() && AdvancedStructureStatsFaction.this.getMan() != null && AdvancedStructureStatsFaction.this.hasRights() && AdvancedStructureStatsFaction.this.getSegCon().isHomeBaseFor(AdvancedStructureStatsFaction.this.getState().getPlayer().getFactionId());
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_18;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.RED;
         }
      });
      this.addLabel(var1.getContent(0), 0, 6, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_3;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }
      });

      for(byte var3 = -1; var3 < 5; ++var3) {
         this.addPermissionButton(var1.getContent(0), var3, 7);
      }

   }

   private void addPermissionButton(GUIAncor var1, final byte var2, int var3) {
      this.addButton(var1, var2, var3, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  final SegmentController var1 = AdvancedStructureStatsFaction.this.getSegCon();
                  final PlayerState var2x = AdvancedStructureStatsFaction.this.getState().getPlayer();
                  if ((var1.getFactionRights() == -1 || var2x.getFactionRights() != 4) && !var1.isSufficientFactionRights(var2x)) {
                     AdvancedStructureStatsFaction.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_6, 0.0F);
                     System.err.println("[CLIENT] Permission failed: " + var2x + ": Rank: " + var2x.getFactionRights() + "; Target " + var1 + ": Rank: " + var1.getFactionRights() + "; toSet: " + var2);
                  } else if (var2 > var2x.getFactionRights()) {
                     (new PlayerGameOkCancelInput("CONFIRM", AdvancedStructureStatsFaction.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_4, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_5) {
                        public boolean isOccluded() {
                           return false;
                        }

                        public void onDeactivate() {
                        }

                        public void pressedOK() {
                           var2x.sendSimpleCommand(SimplePlayerCommands.SET_FACTION_RANK_ON_OBJ, var1.getId(), var2);
                           this.deactivate();
                        }
                     }).activate();
                  } else {
                     var2x.sendSimpleCommand(SimplePlayerCommands.SET_FACTION_RANK_ON_OBJ, var1.getId(), var2);
                  }
               }
            };
         }

         public boolean isActive() {
            return super.isActive() && AdvancedStructureStatsFaction.this.isSegConOwnFactionExists();
         }

         public String getName() {
            return AdvancedStructureStatsFaction.this.getRankString(var2) + (var2 == AdvancedStructureStatsFaction.this.getState().getPlayer().getFactionRights() ? "*" : "");
         }

         public String getToolTipText() {
            return AdvancedStructureStatsFaction.this.getRankStringLong(var2, var2 == AdvancedStructureStatsFaction.this.getState().getPlayer().getFactionRights());
         }

         public long getToolTipDelayMs() {
            return 500L;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            if (AdvancedStructureStatsFaction.this.getSegCon() == null) {
               return GUIHorizontalArea.HButtonColor.BLUE;
            } else if (AdvancedStructureStatsFaction.this.getSegCon().getFactionRights() == -1) {
               return var2 == -1 ? GUIHorizontalArea.HButtonColor.GREEN : GUIHorizontalArea.HButtonColor.RED;
            } else if (AdvancedStructureStatsFaction.this.getSegCon().getFactionRights() == -2) {
               return var2 == -1 ? GUIHorizontalArea.HButtonColor.BLUE : GUIHorizontalArea.HButtonColor.RED;
            } else if (var2 >= AdvancedStructureStatsFaction.this.getSegCon().getFactionRights()) {
               return GUIHorizontalArea.HButtonColor.GREEN;
            } else {
               return var2 == -1 ? GUIHorizontalArea.HButtonColor.BLUE : GUIHorizontalArea.HButtonColor.RED;
            }
         }
      });
   }

   private String getRankString(byte var1) {
      if (var1 == -1) {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_19;
      } else {
         return var1 == 4 ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_20 : String.valueOf(4 - var1);
      }
   }

   private String getRankStringLong(byte var1, boolean var2) {
      String var3 = "";
      if (var2) {
         var3 = "\n" + Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_21;
      }

      if (var1 == -1) {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_22 + var3;
      } else {
         return var1 == 4 ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_23 + var3 : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_24, String.valueOf(4 - var1)) + var3;
      }
   }

   public boolean hasRights() {
      return this.getSegCon().isSufficientFactionRights(this.getState().getPlayer());
   }

   public boolean hasFactionBlock() {
      return this.getSegCon().getElementClassCountMap().get((short)291) > 0;
   }

   private boolean isSegConOwnFactionExists() {
      return this.getState().getPlayer().getFactionId() != 0 && this.isSegConOwnFaction();
   }

   private boolean isSegConOwnFactionOrNeutral() {
      return this.getState().getPlayer().getFactionId() == 0 || this.isSegConOwnFaction();
   }

   private boolean isSegConOwnFaction() {
      return this.getSegCon() != null && this.getSegCon().getFactionId() == this.getState().getPlayer().getFactionId();
   }

   private int getTextDist() {
      return 150;
   }

   private boolean canTakeOwnerShip() {
      return this.isSegConOwnFactionOrNeutral() && this.hasRights();
   }

   public String getId() {
      return "ASFACTION";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSFACTION_25;
   }
}
