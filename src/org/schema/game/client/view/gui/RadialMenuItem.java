package org.schema.game.client.view.gui;

import it.unimi.dsi.fastutil.floats.FloatArrayList;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.common.FastMath;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public abstract class RadialMenuItem extends GUIElement {
   protected final RadialMenu m;
   private int index;
   private GUIElement label;
   private GUITextOverlay tooltip;
   private boolean activateOnDeactiveRadial = true;
   private RadialMenu childMenu;
   private GUIActivationCallback activationCallback;
   private boolean subMenuParent;
   private float margin = 0.01F;
   private int list;
   private float lastRad;
   private float lastCenRad;
   private int lastTotSlices;
   private float radScale = 1.0F;
   private float alpha = 1.0F;
   protected Vector4f clrTmp = new Vector4f();
   private long lastInside;
   private long insideDuration;

   public abstract GUIElement getLabel();

   public RadialMenuItem(InputState var1, RadialMenu var2, int var3, final GUIActivationCallback var4, final GUICallback var5) {
      super(var1);
      this.m = var2;
      this.index = var3;
      this.activationCallback = var4;
      this.tooltip = new GUITextOverlay(30, 30, FontLibrary.getBlenderProHeavy20(), var1);
      if (var5 != null) {
         GUICallback var6 = var5;
         if (var4 != null) {
            var6 = new GUICallback() {
               public boolean isOccluded() {
                  return !var4.isActive(RadialMenuItem.this.getState()) || var5.isOccluded();
               }

               public void callback(GUIElement var1, MouseEvent var2) {
                  var5.callback(var1, var2);
               }
            };
         }

         this.setCallback(var6);
      } else {
         this.subMenuParent = true;
         this.setCallback(new RadialMenuItem.ParentCallback());
      }

      this.setMouseUpdateEnabled(true);
   }

   public void setToolTip(Object var1) {
      this.tooltip.setTextSimple(var1);
   }

   public Vector4f getColorCurrent(Vector4f var1) {
      if (this.isActive()) {
         if (this.isInside()) {
            this.clrTmp.set(this.isHightlighted() ? this.m.highlightSelected : this.m.colorSelected);
            return this.clrTmp;
         } else {
            this.clrTmp.set(this.isHightlighted() ? this.m.highlight : this.m.color);
            return this.clrTmp;
         }
      } else {
         this.clrTmp.set(this.m.deactivated);
         return this.clrTmp;
      }
   }

   public boolean isActive() {
      return super.isActive() && (this.activationCallback == null || this.activationCallback.isActive(this.getState()));
   }

   public boolean isHightlighted() {
      return this.activationCallback != null && this.activationCallback instanceof GUIActivationHighlightCallback && ((GUIActivationHighlightCallback)this.activationCallback).isHighlighted(this.getState());
   }

   private Vector2f getStartSector() {
      return this.getSector(this.index, this.margin);
   }

   private Vector2f getEndSector() {
      return this.getSector(this.index + 1, -this.margin);
   }

   private float getRadFract(int var1, float var2, float var3) {
      return (float)var1 / (float)this.m.getTotalSlices() * 6.2831855F + var2 * var3;
   }

   private Vector2f getSector(int var1, float var2) {
      Vector2f var3 = new Vector2f();
      float var4 = this.getRadFract(var1, var2, 1.0F);
      var3.x = FastMath.cos(var4) * this.getRadius();
      var3.y = FastMath.sin(var4) * this.getRadius();
      return var3;
   }

   public void cleanUp() {
   }

   public void drawFading(float var1, float var2, boolean var3, boolean var4) {
      this.alpha = var2;
      this.radScale = var1;
      if (this.list == 0 || this.getRadius() != this.lastRad || this.getCenterRadius() != this.lastCenRad || this.m.getTotalSlices() != this.lastTotSlices) {
         this.recalcLists();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.getColorCurrent(this.clrTmp);
      Vector4f var10000 = this.clrTmp;
      var10000.w *= this.alpha;
      GlUtil.glColor4f(this.clrTmp);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
      GlUtil.glDisable(2896);
      GlUtil.glDisable(3553);
      GL11.glCallList(this.list);
      if (var3) {
         this.drawText();
      }

      if (var4) {
         this.checkMouseInside();
      }

      GlUtil.glPopMatrix();
      this.radScale = 1.0F;
      this.alpha = 1.0F;
   }

   public void drawFadingText(float var1, float var2, boolean var3, boolean var4) {
      this.alpha = var2;
      this.radScale = var1;
      GlUtil.glPushMatrix();
      this.transform();
      this.getColorCurrent(this.clrTmp);
      Vector4f var10000 = this.clrTmp;
      var10000.w *= this.alpha;
      GlUtil.glColor4f(this.clrTmp);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
      GlUtil.glDisable(2896);
      GlUtil.glDisable(3553);
      if (var3) {
         this.drawText();
      }

      GlUtil.glPopMatrix();
      this.radScale = 1.0F;
      this.alpha = 1.0F;
   }

   public void draw() {
      if (this.label == null) {
         this.label = this.getLabel();
      }

      if (this.list == 0 || this.getRadius() != this.lastRad || this.getCenterRadius() != this.lastCenRad || this.m.getTotalSlices() != this.lastTotSlices) {
         this.recalcLists();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.getColorCurrent(this.clrTmp);
      Vector4f var10000 = this.clrTmp;
      var10000.w *= this.alpha;
      GlUtil.glColor4f(this.clrTmp);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
      GlUtil.glDisable(2896);
      GlUtil.glDisable(3553);
      GL11.glCallList(this.list);
      this.drawText();
      this.checkMouseInside();
      if (this.isInside()) {
         if (this.lastInside != 0L) {
            long var1 = System.currentTimeMillis() - this.lastInside;
            this.insideDuration += Math.max(1L, var1);
         }

         this.lastInside = System.currentTimeMillis();
      } else {
         this.insideDuration = 0L;
         this.lastInside = 0L;
      }

      GlUtil.glPopMatrix();
   }

   public void drawLabel() {
      if (this.label == null) {
         this.label = this.getLabel();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.getColorCurrent(this.clrTmp);
      Vector4f var10000 = this.clrTmp;
      var10000.w *= this.alpha;
      GlUtil.glColor4f(this.clrTmp);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
      GlUtil.glDisable(2896);
      GlUtil.glDisable(3553);
      this.drawText();
      if (this.insideDuration > this.m.getToolTipTime()) {
         this.drawToolTip();
      }

      this.checkMouseInside();
      GlUtil.glPopMatrix();
   }

   private float getRadius() {
      return this.radScale * this.m.getRadius();
   }

   private float getCenterRadius() {
      return this.radScale * this.m.getCenterRadius();
   }

   private void drawText() {
      float var1 = this.getRadFract(this.index, this.margin, 1.0F);
      float var2 = (this.getRadFract(this.index + 1, -this.margin, 1.0F) - var1) / 2.0F;
      float var3 = FastMath.cos(var1 + var2);
      var1 = FastMath.sin(var1 + var2);
      var2 = var3 * this.getRadius() * 0.766F;
      var1 = var1 * this.getRadius() * 0.766F;
      this.setColorAndPos(this.label, var2, var1, this.getColorCurrent(this.clrTmp));
      this.label.draw();
   }

   private void drawToolTip() {
      float var1 = this.getRadFract(this.index, this.margin, 1.0F);
      float var2 = (this.getRadFract(this.index + 1, -this.margin, 1.0F) - var1) / 2.0F;
      float var3 = FastMath.cos(var1 + var2);
      var1 = FastMath.sin(var1 + var2);
      var2 = var3 * this.getRadius() * 0.766F;
      var1 = var1 * this.getRadius() * 0.766F - 40.0F;
      this.tooltip.getColor().a = this.getColorCurrent(this.clrTmp).w;
      this.tooltip.setPos((float)((int)((float)this.getCenterX() + var2 - this.label.getWidth() / 2.0F)), (float)((int)((float)this.getCenterY() + var1 - this.label.getHeight() / 2.0F)), 0.0F);
      this.tooltip.setColor(this.m.textColor);
      this.tooltip.draw();
   }

   protected abstract void setColorAndPos(GUIElement var1, float var2, float var3, Vector4f var4);

   public void onInit() {
   }

   private void recalcLists() {
      int var1;
      if (this.m.getTotalSlices() >= 8) {
         var1 = 24;
      } else {
         var1 = (int)(8.0F / (float)Math.max(1, this.m.getTotalSlices()) * 24.0F);
      }

      FloatArrayList var2 = new FloatArrayList(var1);
      FloatArrayList var3 = new FloatArrayList(var1);
      float var4 = this.getRadFract(this.index, this.margin, this.getCenterRadius() > 0.0F ? this.getRadius() / this.getCenterRadius() : 1.0F);
      float var5 = this.getRadFract(this.index + 1, -this.margin, this.getCenterRadius() > 0.0F ? this.getRadius() / this.getCenterRadius() : 1.0F);
      float var6 = this.getRadFract(this.index, this.margin, 1.0F);
      float var7 = this.getRadFract(this.index + 1, -this.margin, 1.0F);
      var5 = (var5 - var4) / (float)var1;
      var7 = (var7 - var6) / (float)var1;
      float var8 = 0.0F;
      float var9 = 0.0F;

      int var10;
      float var11;
      float var12;
      float var13;
      float var14;
      float var15;
      float var16;
      for(var10 = 0; var10 < var1 + 1; ++var10) {
         var11 = var4 + var8;
         var12 = var6 + var9;
         var13 = FastMath.cos(var11);
         var14 = FastMath.sin(var11);
         var15 = FastMath.cos(var12);
         var16 = FastMath.sin(var12);
         var3.add(var13 * this.getCenterRadius());
         var3.add(var14 * this.getCenterRadius());
         var2.add(var15 * this.getRadius());
         var2.add(var16 * this.getRadius());
         var8 += var5;
         var9 += var7;
      }

      if (this.list == 0) {
         this.list = GL11.glGenLists(1);
      }

      GL11.glNewList(this.list, 4864);
      GL11.glBegin(7);

      for(var10 = 0; var10 < var1; ++var10) {
         var11 = var2.get(var10 << 1);
         var12 = var2.get((var10 << 1) + 1);
         var13 = var3.get(var10 << 1);
         var14 = var3.get((var10 << 1) + 1);
         var15 = var2.get(var10 + 1 << 1);
         var16 = var2.get((var10 + 1 << 1) + 1);
         var4 = var3.get(var10 + 1 << 1);
         var5 = var3.get((var10 + 1 << 1) + 1);
         GL11.glVertex2f((float)this.m.getCenterX() + var13, (float)this.m.getCenterY() + var14);
         GL11.glVertex2f((float)this.m.getCenterX() + var4, (float)this.m.getCenterY() + var5);
         GL11.glVertex2f((float)this.m.getCenterX() + var15, (float)this.m.getCenterY() + var16);
         GL11.glVertex2f((float)this.m.getCenterX() + var11, (float)this.m.getCenterY() + var12);
      }

      GL11.glEnd();
      GL11.glEndList();
      this.lastRad = this.getRadius();
      this.lastCenRad = this.getCenterRadius();
      this.lastTotSlices = this.m.getTotalSlices();
   }

   public float getHeight() {
      return this.m.getHeight();
   }

   public float getWidth() {
      return this.m.getWidth();
   }

   public int getCenterX() {
      return this.m.getCenterX();
   }

   public int getCenterY() {
      return this.m.getCenterY();
   }

   private static boolean areClockwise(Vector2f var0, Vector2f var1) {
      return -var0.x * var1.y + var0.y * var1.x > 0.0F;
   }

   private static boolean isOutofRadius(Vector2f var0, float var1) {
      return var0.x * var0.x + var0.y * var0.y > var1;
   }

   protected boolean isCoordsInside(Vector3f var1, float var2, float var3) {
      int var6 = (int)(var1.x - (float)this.getCenterX());
      int var4 = (int)(var1.y - (float)this.getCenterY());
      Vector2f var5 = new Vector2f((float)var6, (float)var4);
      Vector2f var7 = this.getStartSector();
      Vector2f var8 = this.getEndSector();
      return (this.m.getTotalSlices() == 1 || !areClockwise(var7, var5) && areClockwise(var8, var5)) && isOutofRadius(var5, this.getCenterRadius() * this.getCenterRadius());
   }

   public RadialMenu getChildMenu() {
      return this.childMenu;
   }

   public void setChildMenu(RadialMenu var1) {
      this.childMenu = var1;
   }

   public void activateSelected() {
      if (this.isActivateOnDeactiveRadial() && !this.subMenuParent && this.isActive() && this.getCallback() != null && !this.getCallback().isOccluded() && this.isInside()) {
         MouseEvent var1;
         (var1 = new MouseEvent()).button = 0;
         var1.state = true;
         this.getCallback().callback(this, var1);
      }

   }

   public boolean isActivateOnDeactiveRadial() {
      return this.activateOnDeactiveRadial;
   }

   public void setActivateOnDeactiveRadial(boolean var1) {
      this.activateOnDeactiveRadial = var1;
   }

   class ParentCallback implements GUICallback {
      private ParentCallback() {
      }

      public void callback(GUIElement var1, MouseEvent var2) {
         if (var2.pressedLeftMouse()) {
            if (RadialMenuItem.this.childMenu != null) {
               RadialMenuItem.this.m.getRadialMenuCallback().menuChanged(RadialMenuItem.this.childMenu);
               RadialMenuItem.this.m.getRadialMenuCallback().menuDeactivated(RadialMenuItem.this.m);
               return;
            }
         } else if (var2.pressedRightMouse() && RadialMenuItem.this.m.getParentMenu() != null) {
            RadialMenuItem.this.m.getRadialMenuCallback().menuChanged(RadialMenuItem.this.m.getParentMenu());
            RadialMenuItem.this.m.getRadialMenuCallback().menuDeactivated(RadialMenuItem.this.m);
         }

      }

      public boolean isOccluded() {
         return RadialMenuItem.this.activationCallback != null && !RadialMenuItem.this.activationCallback.isActive(RadialMenuItem.this.getState());
      }

      // $FF: synthetic method
      ParentCallback(Object var2) {
         this();
      }
   }
}
