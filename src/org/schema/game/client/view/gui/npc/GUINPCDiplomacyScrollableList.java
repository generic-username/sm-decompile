package org.schema.game.client.view.gui.npc;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.controller.manager.ingame.shop.ShopControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.game.server.data.simulation.npc.diplomacy.NPCDiplModifier;
import org.schema.game.server.data.simulation.npc.diplomacy.NPCDiplomacyEntity;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.CreateGUIElementInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterDropdown;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableDropDown;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUINPCDiplomacyScrollableList extends ScrollableTableList {
   private long diplEntityId;
   private NPCFaction toFaction;

   public GUINPCDiplomacyScrollableList(InputState var1, long var2, NPCFaction var4, GUIElement var5) {
      super(var1, 100.0F, 100.0F, var5);
      this.toFaction = var4;
      this.diplEntityId = var2;
      ((GameClientState)var1).getFactionManager().addObserver(this);
      var4.getDiplomacy().addObserver(this);
   }

   public void cleanUp() {
      super.cleanUp();
      ((GameClientState)this.getState()).getFactionManager().deleteObserver(this);
      this.toFaction.getDiplomacy().deleteObserver(this);
   }

   protected boolean isFiltered(NPCDiplModifier var1) {
      return super.isFiltered(var1);
   }

   public ShopControllerManager getShopControlManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getShopControlManager();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NPC_GUINPCDIPLOMACYSCROLLABLELIST_0, 3.0F, new Comparator() {
         public int compare(NPCDiplModifier var1, NPCDiplModifier var2) {
            return var1.getName().compareTo(var2.getName());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NPC_GUINPCDIPLOMACYSCROLLABLELIST_1, 140, new Comparator() {
         public int compare(NPCDiplModifier var1, NPCDiplModifier var2) {
            return var1.getValue() - var2.getValue();
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, NPCDiplModifier var2) {
            return var2.getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NPC_GUINPCDIPLOMACYSCROLLABLELIST_2, ControllerElement.FilterRowStyle.LEFT);
      this.addDropdownFilter(new GUIListFilterDropdown(new Integer[]{0, 1, 2}) {
         public boolean isOk(Integer var1, NPCDiplModifier var2) {
            switch(var1) {
            case 0:
               return true;
            case 1:
               if (!var2.isStatic()) {
                  return true;
               }

               return false;
            case 2:
               return var2.isStatic();
            default:
               return true;
            }
         }
      }, new CreateGUIElementInterface() {
         public GUIElement create(Integer var1) {
            GUIAncor var2 = new GUIAncor(GUINPCDiplomacyScrollableList.this.getState(), 10.0F, 24.0F);
            GUITextOverlayTableDropDown var3 = new GUITextOverlayTableDropDown(10, 10, GUINPCDiplomacyScrollableList.this.getState());
            switch(var1) {
            case 0:
               var3.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NPC_GUINPCDIPLOMACYSCROLLABLELIST_3);
               break;
            case 1:
               var3.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NPC_GUINPCDIPLOMACYSCROLLABLELIST_4);
               break;
            case 2:
               var3.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NPC_GUINPCDIPLOMACYSCROLLABLELIST_5);
            }

            var3.setPos(4.0F, 4.0F, 0.0F);
            var2.setUserPointer(var1);
            var2.attach(var3);
            return var2;
         }

         public GUIElement createNeutral() {
            return null;
         }
      }, ControllerElement.FilterRowStyle.RIGHT);
      this.activeSortColumnIndex = 0;
   }

   protected Collection getElementList() {
      ObjectArrayList var1 = new ObjectArrayList();
      NPCDiplomacyEntity var2;
      if ((var2 = (NPCDiplomacyEntity)this.toFaction.getDiplomacy().entities.get(this.diplEntityId)) != null) {
         var1.addAll(var2.getDynamicMap().values());
         var1.addAll(var2.getStaticMap().values());
      }

      return var1;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getPlayer();
      Iterator var8 = var2.iterator();

      while(var8.hasNext()) {
         final NPCDiplModifier var3 = (NPCDiplModifier)var8.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         var4.setTextSimple(new Object() {
            public String toString() {
               return var3.getName();
            }
         });
         var5.setTextSimple(new Object() {
            public String toString() {
               return var3.getValue() + (var3.isStatic() ? "" : " / turn");
            }
         });
         ScrollableTableList.GUIClippedRow var6;
         (var6 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         var4.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         GUINPCDiplomacyScrollableList.NPCDiplModifierRow var9;
         (var9 = new GUINPCDiplomacyScrollableList.NPCDiplModifierRow(this.getState(), var3, new GUIElement[]{var6, var7})).onInit();
         var1.addWithoutUpdate(var9);
      }

      var1.updateDim();
   }

   class NPCDiplModifierRow extends ScrollableTableList.Row {
      public NPCDiplModifierRow(InputState var2, NPCDiplModifier var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
