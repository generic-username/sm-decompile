package org.schema.game.client.view.gui.npc;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.controller.manager.ingame.shop.ShopControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.faction.newfaction.FactionPanelNew;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.simulation.npc.news.NPCFactionNewsEvent;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUINPCFactionNewsScrollableList extends ScrollableTableList {
   public GUINPCFactionNewsScrollableList(InputState var1, GUIElement var2, FactionPanelNew var3) {
      super(var1, 100.0F, 100.0F, var2);
      ((GameClientState)this.getState()).getFactionManager().getNpcFactionNews().addObserver(this);
   }

   public void cleanUp() {
      super.cleanUp();
      ((GameClientState)this.getState()).getFactionManager().getNpcFactionNews().deleteObserver(this);
   }

   protected boolean isFiltered(NPCFactionNewsEvent var1) {
      return super.isFiltered(var1);
   }

   public ShopControllerManager getShopControlManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getShopControlManager();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NPC_GUINPCFACTIONNEWSSCROLLABLELIST_0, 3.0F, new Comparator() {
         public int compare(NPCFactionNewsEvent var1, NPCFactionNewsEvent var2) {
            return var1.getMessage((FactionState)GUINPCFactionNewsScrollableList.this.getState()).compareTo(var2.getMessage((FactionState)GUINPCFactionNewsScrollableList.this.getState()));
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, NPCFactionNewsEvent var2) {
            return var2.getMessage((FactionState)GUINPCFactionNewsScrollableList.this.getState()).toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NPC_GUINPCFACTIONNEWSSCROLLABLELIST_1, ControllerElement.FilterRowStyle.FULL);
      this.activeSortColumnIndex = 0;
   }

   protected Collection getElementList() {
      return ((GameClientState)this.getState()).getFactionManager().getNpcFactionNews().events;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getPlayer();
      Iterator var6 = var2.iterator();

      while(var6.hasNext()) {
         final NPCFactionNewsEvent var3 = (NPCFactionNewsEvent)var6.next();
         GUITextOverlayTable var4;
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return var3.getMessage((FactionState)GUINPCFactionNewsScrollableList.this.getState());
            }
         });
         ScrollableTableList.GUIClippedRow var5;
         (var5 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         var4.getPos().y = 5.0F;
         GUINPCFactionNewsScrollableList.NPCFactionNewsEventRow var7;
         (var7 = new GUINPCFactionNewsScrollableList.NPCFactionNewsEventRow(this.getState(), var3, new GUIElement[]{var5})).onInit();
         var1.addWithoutUpdate(var7);
      }

      var1.updateDim();
   }

   class NPCFactionNewsEventRow extends ScrollableTableList.Row {
      public NPCFactionNewsEventRow(InputState var2, NPCFactionNewsEvent var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
