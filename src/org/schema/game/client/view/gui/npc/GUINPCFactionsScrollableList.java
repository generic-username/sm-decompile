package org.schema.game.client.view.gui.npc;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.controller.manager.ingame.shop.ShopControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.faction.newfaction.FactionPanelNew;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.CreateGUIElementInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterDropdown;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableDropDown;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUINPCFactionsScrollableList extends ScrollableTableList {
   private FactionPanelNew panel;

   public GUINPCFactionsScrollableList(InputState var1, GUIElement var2, FactionPanelNew var3) {
      super(var1, 100.0F, 100.0F, var2);
      this.panel = var3;
      ((GameClientState)var1).getFactionManager().addObserver(this);
   }

   public void cleanUp() {
      super.cleanUp();
      ((GameClientState)this.getState()).getFactionManager().deleteObserver(this);
   }

   protected boolean isFiltered(NPCFaction var1) {
      return super.isFiltered(var1);
   }

   public ShopControllerManager getShopControlManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getShopControlManager();
   }

   public FactionRelation.RType getOwnRelationTo(NPCFaction var1) {
      return ((GameClientState)this.getState()).getFactionManager().getRelation(((GameClientState)this.getState()).getPlayerName(), ((GameClientState)this.getState()).getPlayer().getFactionId(), var1.getIdFaction());
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NPC_GUINPCFACTIONSSCROLLABLELIST_0, 3.0F, new Comparator() {
         public int compare(NPCFaction var1, NPCFaction var2) {
            return var1.getName().compareTo(var2.getName());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NPC_GUINPCFACTIONSSCROLLABLELIST_1, 140, new Comparator() {
         public int compare(NPCFaction var1, NPCFaction var2) {
            return var1.getHomeSector().compareTo(var2.getHomeSector());
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, NPCFaction var2) {
            return var2.getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NPC_GUINPCFACTIONSSCROLLABLELIST_2, ControllerElement.FilterRowStyle.LEFT);
      this.addDropdownFilter(new GUIListFilterDropdown(new Integer[]{0, 1, 2, 3}) {
         public boolean isOk(Integer var1, NPCFaction var2) {
            switch(var1) {
            case 0:
               return true;
            case 1:
               if (GUINPCFactionsScrollableList.this.getOwnRelationTo(var2) == FactionRelation.RType.NEUTRAL) {
                  return true;
               }

               return false;
            case 2:
               if (GUINPCFactionsScrollableList.this.getOwnRelationTo(var2) == FactionRelation.RType.ENEMY) {
                  return true;
               }

               return false;
            case 3:
               if (GUINPCFactionsScrollableList.this.getOwnRelationTo(var2) == FactionRelation.RType.FRIEND) {
                  return true;
               }

               return false;
            default:
               return true;
            }
         }
      }, new CreateGUIElementInterface() {
         public GUIElement create(Integer var1) {
            GUIAncor var2 = new GUIAncor(GUINPCFactionsScrollableList.this.getState(), 10.0F, 24.0F);
            GUITextOverlayTableDropDown var3 = new GUITextOverlayTableDropDown(10, 10, GUINPCFactionsScrollableList.this.getState());
            switch(var1) {
            case 0:
               var3.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NPC_GUINPCFACTIONSSCROLLABLELIST_3);
               break;
            case 1:
               var3.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NPC_GUINPCFACTIONSSCROLLABLELIST_4);
               break;
            case 2:
               var3.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NPC_GUINPCFACTIONSSCROLLABLELIST_5);
               break;
            case 3:
               var3.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NPC_GUINPCFACTIONSSCROLLABLELIST_6);
            }

            var3.setPos(4.0F, 4.0F, 0.0F);
            var2.setUserPointer(var1);
            var2.attach(var3);
            return var2;
         }

         public GUIElement createNeutral() {
            return null;
         }
      }, ControllerElement.FilterRowStyle.RIGHT);
      this.activeSortColumnIndex = 0;
   }

   protected Collection getElementList() {
      ObjectArrayList var1 = new ObjectArrayList();
      Iterator var2 = ((GameClientState)this.getState()).getFactionManager().getFactionCollection().iterator();

      while(var2.hasNext()) {
         Faction var3;
         if ((var3 = (Faction)var2.next()).isNPC()) {
            var1.add((NPCFaction)var3);
         }
      }

      return var1;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getPlayer();
      Iterator var8 = var2.iterator();

      while(var8.hasNext()) {
         final NPCFaction var3 = (NPCFaction)var8.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         var4.setTextSimple(new Object() {
            public String toString() {
               return var3.getName();
            }
         });
         var5.setTextSimple(new Object() {
            public String toString() {
               return var3.getHomeSector().toStringPure();
            }
         });
         ScrollableTableList.GUIClippedRow var6;
         (var6 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         var4.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         GUINPCFactionsScrollableList.NPCFactionRow var9;
         (var9 = new GUINPCFactionsScrollableList.NPCFactionRow(this.getState(), var3, new GUIElement[]{var6, var7})).onInit();
         var1.addWithoutUpdate(var9);
      }

      var1.updateDim();
   }

   class NPCFactionRow extends ScrollableTableList.Row {
      public NPCFactionRow(InputState var2, NPCFaction var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
         this.highlightSelectSimple = true;
         this.setAllwaysOneSelected(true);
      }

      protected void clickedOnRow() {
         GUINPCFactionsScrollableList.this.panel.onSelectFaction((NPCFaction)this.f);
         super.clickedOnRow();
      }
   }
}
