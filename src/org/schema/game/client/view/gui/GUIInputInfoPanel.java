package org.schema.game.client.view.gui;

import java.util.ArrayList;
import java.util.Iterator;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.network.client.ClientState;

public class GUIInputInfoPanel extends GUIElement implements GUIInputInterface {
   private final GUITextButton buttonOK;
   private final GUITextButton buttonCancel;
   private GUITextOverlay infoText;
   private GUITextOverlay descriptionText;
   private GUITextOverlay errorText;
   private GUIDialogWindow background;
   private boolean okButton = false;
   private boolean cancelButton = false;
   private long timeError;
   private long timeErrorShowed;
   private boolean firstDraw = true;
   private Object[] awnsers;
   private GUIAwnserInterface actInt;

   public GUIInputInfoPanel(String var1, ClientState var2, GUICallback var3, Object var4, Object var5, Object[] var6, GUIAwnserInterface var7) {
      super(var2);
      this.setCallback(var3);
      this.actInt = var7;
      this.infoText = new GUITextOverlay(256, 64, FontLibrary.getBlenderProMedium15(), var2);
      this.descriptionText = new GUITextOverlay(256, 64, FontLibrary.getBlenderProMedium14(), var2);
      this.errorText = new GUITextOverlay(256, 64, var2);
      this.background = new GUIDialogWindow(var2, 600, 350, var1);
      this.background.activeInterface = var7;
      this.buttonOK = new GUITextButton(var2, 100, 24, GUITextButton.ColorPalette.OK, "OK", var3);
      this.buttonOK.setUserPointer("OK");
      this.buttonOK.setMouseUpdateEnabled(true);
      this.buttonCancel = new GUITextButton(var2, 100, 24, GUITextButton.ColorPalette.CANCEL, "CANCEL", var3);
      this.buttonCancel.setCallback(var3);
      this.buttonCancel.setUserPointer("CANCEL");
      this.awnsers = var6;
      this.background.setCloseCallback(var3);
      ArrayList var8;
      (var8 = new ArrayList()).add(var4);
      this.infoText.setText(var8);
      (var8 = new ArrayList()).add(var5);
      this.descriptionText.setText(var8);
      var8 = new ArrayList();
      this.errorText.setText(var8);
      this.background.orientate(48);
   }

   public void cleanUp() {
      this.background.cleanUp();
      this.infoText.cleanUp();
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      if (this.needsReOrientation()) {
         this.background.orientate(48);
      }

      GlUtil.glPushMatrix();
      this.transform();
      if (this.timeError < System.currentTimeMillis() - this.timeErrorShowed) {
         this.errorText.getText().clear();
      }

      this.buttonOK.setPos(8.0F, (float)((int)(this.background.getHeight() - (42.0F + this.buttonOK.getHeight()))), 0.0F);
      this.buttonCancel.setPos((float)((int)(this.buttonOK.getPos().x + this.buttonOK.getWidth() + 5.0F)), (float)((int)this.buttonOK.getPos().y), 0.0F);
      this.infoText.setPos((float)((int)(this.background.getWidth() / 2.0F - (float)(this.infoText.getMaxLineWidth() / 2))), -23.0F, 0.0F);
      Iterator var1 = this.getChilds().iterator();

      while(var1.hasNext()) {
         ((AbstractSceneNode)var1.next()).draw();
      }

      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.background.orientate(48);
      this.background.onInit();
      this.infoText.onInit();
      this.buttonOK.onInit();
      this.buttonCancel.onInit();
      this.descriptionText.onInit();
      this.background.getMainContentPane().setTextBoxHeightLast(200);
      this.background.getMainContentPane().addNewTextBox(70);
      GUIScrollablePanel var1;
      (var1 = new GUIScrollablePanel(10.0F, 10.0F, this.background.getMainContentPane().getContent(0), this.getState())).setScrollable(GUIScrollablePanel.SCROLLABLE_HORIZONTAL | GUIScrollablePanel.SCROLLABLE_VERTICAL);
      GUIAncor var2;
      (var2 = new GUIAncor(this.getState()) {
         public void draw() {
            this.setWidth(GUIInputInfoPanel.this.descriptionText.getMaxLineWidth());
            this.setHeight(GUIInputInfoPanel.this.descriptionText.getTextHeight());
            super.draw();
         }
      }).attach(this.descriptionText);
      var1.setContent(var2);
      var1.onInit();
      this.background.getMainContentPane().getContent(0).attach(var1);
      final GUIHorizontalButtonTablePane var4;
      (var4 = new GUIHorizontalButtonTablePane(this.getState(), 1, this.awnsers.length, this.background.getMainContentPane().getContent(1))).onInit();

      for(final int var5 = this.awnsers.length - 1; var5 >= 0; --var5) {
         var4.addButton(0, var5, this.awnsers[var5], (GUIHorizontalArea.HButtonType)(var5 == this.awnsers.length - 1 ? GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM : GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM), new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  GUIInputInfoPanel.this.actInt.pressedAwnser(var5);
               }

            }

            public boolean isOccluded() {
               return !GUIInputInfoPanel.this.actInt.isActive();
            }
         }, (GUIActivationCallback)null);
      }

      GUIScrollablePanel var6 = new GUIScrollablePanel(10.0F, 10.0F, this.background.getMainContentPane().getContent(1), this.getState());
      GUIAncor var3;
      (var3 = new GUIAncor(this.getState()) {
         public void draw() {
            this.setWidth(var4.getWidth());
            this.setHeight(var4.getHeight());
            super.draw();
         }
      }).attach(var4);
      var6.setContent(var3);
      var6.onInit();
      this.background.getMainContentPane().getContent(1).attach(var6);
      this.attach(this.background);
      this.background.attach(this.infoText);
      this.background.attach(this.errorText);
      if (this.isOkButton()) {
         this.background.attach(this.buttonOK);
      }

      if (this.isCancelButton()) {
         this.background.attach(this.buttonCancel);
      }

      this.infoText.setPos(95.0F, 11.0F, 0.0F);
      this.descriptionText.setPos(2.0F, 2.0F, 0.0F);
      this.errorText.setPos(97.0F, 300.0F, 0.0F);
      this.buttonOK.setPos(735.0F, 460.0F, 0.0F);
      this.buttonOK.setScale(0.45F, 0.45F, 0.45F);
      this.buttonCancel.setPos(800.0F, 460.0F, 0.0F);
      this.buttonCancel.setScale(0.45F, 0.45F, 0.45F);
      this.firstDraw = false;
   }

   protected void doOrientation() {
      if (isNewHud()) {
         this.background.doOrientation();
      } else {
         super.doOrientation();
      }
   }

   public float getHeight() {
      return this.background.getWidth();
   }

   public float getWidth() {
      return this.background.getHeight();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void orientate(int var1) {
      if (isNewHud()) {
         this.background.orientate(var1);
      } else {
         super.orientate(var1);
      }
   }

   public GUIDialogWindow getBackground() {
      return this.background;
   }

   public GUITextButton getButtonCancel() {
      return this.buttonCancel;
   }

   public GUITextButton getButtonOK() {
      return this.buttonOK;
   }

   public GUITextOverlay getDescriptionText() {
      return this.descriptionText;
   }

   public void setDescriptionText(GUITextOverlay var1) {
      this.descriptionText = var1;
   }

   public boolean isCancelButton() {
      return this.cancelButton;
   }

   public void setCancelButton(boolean var1) {
      this.cancelButton = var1;
   }

   public boolean isOkButton() {
      return this.okButton;
   }

   public void setOkButton(boolean var1) {
      this.okButton = var1;
   }

   public void setErrorMessage(String var1, long var2) {
      this.errorText.setTextSimple(var1);
      this.timeError = System.currentTimeMillis();
      this.timeErrorShowed = var2;
   }
}
