package org.schema.game.client.view.gui;

import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.input.Mouse;

public class RadialMenuDialogShapesMini extends RadialMenuDialog implements GUIActivationCallback {
   private final ElementInformation info;
   private short currentSelectedType;

   public RadialMenuDialogShapesMini(GameClientState var1, ElementInformation var2) {
      super(var1);
      this.info = var2;

      assert this.info.blocktypeIds != null : this.info;

   }

   public RadialMenu createMenu(RadialMenuDialog var1) {
      this.currentSelectedType = this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedTypeWithSub();
      final GameClientState var2 = this.getState();
      RadialMenu var5;
      (var5 = new RadialMenu(var2, "ShapesRadialMini", var1, 200, 200, 10, FontLibrary.getBlenderProHeavy13()) {
         public void draw() {
            super.draw();
            if (!Mouse.isButtonDown(0)) {
               ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().selectTypeForced(RadialMenuDialogShapesMini.this.currentSelectedType);
               RadialMenuDialogShapesMini.this.deactivate();
            }

         }
      }).setForceBackButton(false);
      var5.posElem = new GUIElement(var2) {
         public void onInit() {
         }

         public void draw() {
         }

         public void cleanUp() {
         }

         public float getWidth() {
            return 0.0F;
         }

         public float getHeight() {
            return 0.0F;
         }
      };
      var5.posElem.setPos((float)Mouse.getX(), (float)(GLFrame.getHeight() - Mouse.getY()), 0.0F);

      assert this.info.blocktypeIds != null : this.info;

      for(int var3 = -1; var3 < this.info.blocktypeIds.length; ++var3) {
         final short var4;
         if (var3 < 0) {
            var4 = this.info.id;
         } else {
            var4 = this.info.blocktypeIds[var3];
         }

         ElementKeyMap.getInfo(var4);
         var5.addItemBlock(var4, new GUICallback() {
            public boolean isOccluded() {
               return !RadialMenuDialogShapesMini.this.isActive(var2);
            }

            public void callback(GUIElement var1, MouseEvent var2x) {
               RadialMenuDialogShapesMini.this.currentSelectedType = var4;
            }
         }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGSHAPESMINI_0, new GUIActivationHighlightCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return RadialMenuDialogShapesMini.this.isActive();
            }

            public boolean isHighlighted(InputState var1) {
               return true;
            }
         });
      }

      return var5;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, this.getState()) && this.isDeactivateOnEscape() && KeyboardMappings.getEventKeyRaw(var1) == 1) {
         this.deactivate();
      }

   }

   public void handleMouseEvent(MouseEvent var1) {
      super.handleMouseEvent(var1);
      if (!Mouse.isButtonDown(0)) {
         this.deactivate();
      }

   }

   public PlayerGameControlManager getPGCM() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   public boolean isVisible(InputState var1) {
      return true;
   }

   public boolean isActive(InputState var1) {
      return super.isActive();
   }
}
