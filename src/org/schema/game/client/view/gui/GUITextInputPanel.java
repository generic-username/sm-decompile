package org.schema.game.client.view.gui;

import org.schema.schine.common.TextInput;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUITextInput;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.input.InputState;

public class GUITextInputPanel extends GUIInputPanel {
   private GUITextInput guiTextInput;

   public GUITextInputPanel(String var1, InputState var2, GUICallback var3, Object var4, Object var5, TextInput var6) {
      super(var1, var2, var3, var4, var5);
      this.guiTextInput = new GUITextInput(256, 32, var2);
      this.guiTextInput.setTextBox(true);
      this.guiTextInput.setPreText("");
      this.guiTextInput.setTextInput(var6);
   }

   public GUITextInputPanel(String var1, InputState var2, int var3, int var4, GUICallback var5, Object var6, Object var7, TextInput var8) {
      super(var1, var2, var3, var4, var5, var6, var7);
      this.guiTextInput = new GUITextInput(256, 32, var2);
      this.guiTextInput.setTextBox(true);
      this.guiTextInput.setPreText("");
      this.guiTextInput.setTextInput(var8);
   }

   public void setDisplayAsPassword(boolean var1) {
      this.guiTextInput.setDisplayAsPassword(var1);
   }

   public void cleanUp() {
      super.cleanUp();
      this.guiTextInput.cleanUp();
   }

   public void draw() {
      if (isNewHud() && this.background != null && ((GUIDialogWindow)this.background).getMainContentPane() != null && ((GUIDialogWindow)this.background).getMainContentPane().getContent(0) != null) {
         this.guiTextInput.setPos(12.0F, ((GUIDialogWindow)this.background).getMainContentPane().getContent(0).getHeight() - 32.0F, 0.0F);
         this.errorText.setPos(this.guiTextInput.getPos().x, this.guiTextInput.getPos().y - 16.0F, 0.0F);
      }

      super.draw();
   }

   public void onInit() {
      super.onInit();
      this.guiTextInput.onInit();
      this.getBackground().attach(this.guiTextInput);
      this.guiTextInput.setPos(55.0F, 181.0F, 0.0F);
   }

   public void setInput(TextInput var1) {
      if (this.guiTextInput == null) {
         this.guiTextInput = new GUITextInput(256, 32, this.getState());
         this.guiTextInput.setPreText("");
         this.guiTextInput.setPos(12.0F, this.background.getHeight() - 128.0F, 0.0F);
         if (this.getBackground() != null) {
            this.getBackground().attach(this.guiTextInput);
         }
      }

      this.guiTextInput.setTextInput(var1);
   }
}
