package org.schema.game.client.view.gui.playerstats;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.ship.InShipControlManager;
import org.schema.game.client.controller.manager.ingame.ship.WeaponAssignControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.game.server.data.admin.AdminCommands;
import org.schema.schine.common.TextCallback;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class PlayerStatisticsScrollableListNew extends ScrollableTableList implements Observer {
   public PlayerStatisticsScrollableListNew(InputState var1, GUIAncor var2) {
      super(var1, 100.0F, 100.0F, var2);
   }

   public void cleanUp() {
      super.cleanUp();
   }

   public void onInit() {
      super.onInit();
   }

   public void initColumns() {
      this.addColumn("Name", 2.0F, new Comparator() {
         public int compare(PlayerState var1, PlayerState var2) {
            return var1.getName().compareToIgnoreCase(var2.getName());
         }
      });
      this.addColumn("Faction", 3.0F, new Comparator() {
         public int compare(PlayerState var1, PlayerState var2) {
            return var1.getFactionName().compareToIgnoreCase(var2.getFactionName());
         }
      });
      this.addFixedWidthColumn("Ping", 50, new Comparator() {
         public int compare(PlayerState var1, PlayerState var2) {
            return var1.getPing() - var2.getPing();
         }
      });
      this.addFixedWidthColumn("Options", 56, new Comparator() {
         public int compare(PlayerState var1, PlayerState var2) {
            return 0;
         }
      });
   }

   protected Collection getElementList() {
      return this.getState().getOnlinePlayersLowerCaseMap().values();
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      this.getState().getGameState().getFactionManager();
      this.getState().getGameState().getCatalogManager();
      PlayerState var3 = this.getState().getPlayer();
      Iterator var11 = var2.iterator();

      while(var11.hasNext()) {
         final PlayerState var4 = (PlayerState)var11.next();
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState());
         ScrollableTableList.GUIClippedRow var8;
         (var8 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         ScrollableTableList.GUIClippedRow var9;
         (var9 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var6);
         ScrollableTableList.GUIClippedRow var10;
         (var10 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var7);
         var5.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;
         var7.getPos().y = 5.0F;
         var5.setTextSimple(var4.getName());
         var6.setTextSimple(new Object() {
            public String toString() {
               return var4.getFactionName();
            }
         });
         var7.setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var4.getPing());
            }
         });

         assert !var5.getText().isEmpty();

         assert !var6.getText().isEmpty();

         assert !var7.getText().isEmpty();

         GUIAncor var13 = new GUIAncor(this.getState(), 50.0F, (float)this.columnsHeight);
         GUITextButton var14 = new GUITextButton(this.getState(), 50, 20, GUITextButton.ColorPalette.CANCEL, new Object() {
            public String toString() {
               return PlayerStatisticsScrollableListNew.this.getState().getPlayer().getNetworkObject().isAdminClient.get() ? "options" : "-";
            }
         }, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse() && PlayerStatisticsScrollableListNew.this.getState().getPlayer().getNetworkObject().isAdminClient.get()) {
                  PlayerStatisticsScrollableListNew.this.pressedAdminOptions(var4);
               }

            }

            public boolean isOccluded() {
               return false;
            }
         });
         if (var3.getNetworkObject().isAdminClient.get()) {
            var14.setPos(0.0F, 2.0F, 0.0F);
            var13.attach(var14);
         }

         PlayerStatisticsScrollableListNew.WeaponRow var12;
         (var12 = new PlayerStatisticsScrollableListNew.WeaponRow(this.getState(), var4, new GUIElement[]{var8, var9, var10, var13})).onInit();
         var1.addWithoutUpdate(var12);
      }

      var1.updateDim();
   }

   public boolean isPlayerAdmin() {
      return this.getState().getPlayer().getNetworkObject().isAdminClient.get();
   }

   public boolean canEdit(CatalogPermission var1) {
      return var1.ownerUID.toLowerCase(Locale.ENGLISH).equals(this.getState().getPlayer().getName().toLowerCase(Locale.ENGLISH)) || this.isPlayerAdmin();
   }

   public WeaponAssignControllerManager getAssignWeaponControllerManager() {
      return this.getPlayerGameControlManager().getWeaponControlManager();
   }

   public InShipControlManager getInShipControlManager() {
      return this.getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager();
   }

   public PlayerGameControlManager getPlayerGameControlManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   protected void pressedAdminOptions(final PlayerState var1) {
      final GameClientState var2 = this.getState();
      final PlayerGameOkCancelInput var3;
      (var3 = new PlayerGameOkCancelInput("PlayerStatisticsPanel_PLAYER_ADMIN_OPTIONS", var2, "Player Admin Options: " + var1.getName(), "") {
         public boolean isOccluded() {
            return false;
         }

         public void onDeactivate() {
         }

         public void pressedOK() {
            this.deactivate();
         }
      }).getInputPanel().setOkButton(false);
      var3.getInputPanel().onInit();
      GUITextButton var4 = new GUITextButton(var2, 120, 20, GUITextButton.ColorPalette.CANCEL, new Object() {
         public String toString() {
            return var2.getPlayer().getNetworkObject().isAdminClient.get() ? "Kick" : "-";
         }
      }, new GUICallback() {
         public void callback(GUIElement var1x, MouseEvent var2x) {
            if (var2x.pressedLeftMouse() && var2.getPlayer().getNetworkObject().isAdminClient.get()) {
               var3.deactivate();
               (new PlayerGameTextInput("PlayerStatisticsPanel_KICK", var2, 100, "Kick", "Enter Reason") {
                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public boolean isOccluded() {
                     return false;
                  }

                  public String handleAutoComplete(String var1x, TextCallback var2x, String var3x) throws PrefixNotFoundException {
                     return null;
                  }

                  public void onFailedTextCheck(String var1x) {
                  }

                  public void onDeactivate() {
                     var3.activate();
                  }

                  public boolean onInput(String var1x) {
                     var2.getController().sendAdminCommand(AdminCommands.KICK_REASON, var1.getName(), var1x);
                     return true;
                  }
               }).activate();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      GUITextButton var5 = new GUITextButton(var2, 140, 20, FontLibrary.getBoldArial12White(), new Object() {
         public String toString() {
            return var2.getPlayer().getNetworkObject().isAdminClient.get() ? "Ban StarMade Account" : "-";
         }
      }, new GUICallback() {
         public void callback(GUIElement var1x, MouseEvent var2x) {
            if (var2x.pressedLeftMouse() && var2.getPlayer().getNetworkObject().isAdminClient.get()) {
               var2.getController().sendAdminCommand(AdminCommands.BAN_ACCOUNT_BY_PLAYERNAME, var1.getName());
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      GUITextButton var6 = new GUITextButton(var2, 140, 20, FontLibrary.getBoldArial12White(), new Object() {
         public String toString() {
            return var2.getPlayer().getNetworkObject().isAdminClient.get() ? "Ban Player Name" : "-";
         }
      }, new GUICallback() {
         public void callback(GUIElement var1x, MouseEvent var2x) {
            if (var2x.pressedLeftMouse() && var2.getPlayer().getNetworkObject().isAdminClient.get()) {
               var2.getController().sendAdminCommand(AdminCommands.BAN, var1.getName(), false);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      GUITextButton var7 = new GUITextButton(var2, 140, 20, FontLibrary.getBoldArial12White(), new Object() {
         public String toString() {
            return var2.getPlayer().getNetworkObject().isAdminClient.get() ? "Ban IP" : "-";
         }
      }, new GUICallback() {
         public void callback(GUIElement var1x, MouseEvent var2x) {
            if (var2x.pressedLeftMouse() && var2.getPlayer().getNetworkObject().isAdminClient.get()) {
               var2.getController().sendAdminCommand(AdminCommands.BAN_IP_BY_PLAYERNAME, var1.getName());
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      var3.getInputPanel().getContent().attach(var4);
      var6.getPos().y = 24.0F;
      var3.getInputPanel().getContent().attach(var6);
      var5.getPos().y = 48.0F;
      var3.getInputPanel().getContent().attach(var5);
      var7.getPos().y = 72.0F;
      var3.getInputPanel().getContent().attach(var7);
      var3.activate();
   }

   class WeaponRow extends ScrollableTableList.Row {
      public WeaponRow(InputState var2, PlayerState var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
