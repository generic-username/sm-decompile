package org.schema.game.client.view.gui.tutorial;

import java.util.ArrayList;
import javax.vecmath.Vector4f;
import org.newdawn.slick.UnicodeFont;
import org.schema.game.client.controller.tutorial.TutorialMode;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUITutorialControls extends GUIAncor implements GUICallback {
   private GUITextOverlay currentTutorialPart;
   private GUITextOverlay skipTutorialPart;
   private GUITextOverlay repeatTutorialPart;
   private GUITextOverlay skipTutorialStep;
   private GUITextOverlay endTutorial;
   private GUITextOverlay resetTutorial;
   private boolean init;

   public GUITutorialControls(InputState var1) {
      super(var1);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (this.getTutorial() != null && var2.getEventButtonState() && var2.getEventButton() == 0) {
         if (var1 == this.skipTutorialPart) {
            this.getTutorial().endStep();
            return;
         }

         if (var1 == this.repeatTutorialPart) {
            this.getTutorial().repeatStep();
            return;
         }

         if (var1 == this.skipTutorialStep) {
            this.getTutorial().skip();
            return;
         }

         if (var1 == this.endTutorial) {
            this.getTutorial().end();
            return;
         }

         if (var1 == this.resetTutorial) {
            this.getTutorial().repeat();
         }
      }

   }

   public boolean isOccluded() {
      return false;
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      super.draw();
   }

   public void onInit() {
      UnicodeFont var1 = FontLibrary.getRegularArial13White();
      this.currentTutorialPart = new GUITextOverlay(120, 30, FontLibrary.getRegularArial15White(), this.getState());
      this.skipTutorialPart = new GUITextOverlay(120, 30, var1, this.getState());
      this.repeatTutorialPart = new GUITextOverlay(120, 30, var1, this.getState());
      this.skipTutorialStep = new GUITextOverlay(120, 30, var1, this.getState());
      this.endTutorial = new GUITextOverlay(120, 30, var1, this.getState());
      this.resetTutorial = new GUITextOverlay(120, 30, var1, this.getState());
      this.skipTutorialPart.setCallback(this);
      this.repeatTutorialPart.setCallback(this);
      this.skipTutorialStep.setCallback(this);
      this.endTutorial.setCallback(this);
      this.resetTutorial.setCallback(this);
      this.skipTutorialPart.setMouseUpdateEnabled(true);
      this.repeatTutorialPart.setMouseUpdateEnabled(true);
      this.skipTutorialStep.setMouseUpdateEnabled(true);
      this.endTutorial.setMouseUpdateEnabled(true);
      this.resetTutorial.setMouseUpdateEnabled(true);
      this.currentTutorialPart.setText(new ArrayList(1));
      this.skipTutorialPart.setText(new ArrayList(1));
      this.repeatTutorialPart.setText(new ArrayList(1));
      this.skipTutorialStep.setText(new ArrayList(1));
      this.endTutorial.setText(new ArrayList(1));
      this.resetTutorial.setText(new ArrayList(1));
      this.currentTutorialPart.getText().add("Tutorial Controls");
      this.skipTutorialPart.getText().add("Skip Part");
      this.repeatTutorialPart.getText().add("Repeat Part");
      this.skipTutorialStep.getText().add("skip current step");
      this.endTutorial.getText().add("end tutorial");
      this.resetTutorial.getText().add("reset tutorial");
      Vector4f var7 = new Vector4f(0.0F, 0.0F, 0.0F, 0.7F);
      GUIColoredRectangle var2 = new GUIColoredRectangle(this.getState(), 120.0F, 20.0F, var7);
      GUIColoredRectangle var3 = new GUIColoredRectangle(this.getState(), 120.0F, 20.0F, var7);
      GUIColoredRectangle var4 = new GUIColoredRectangle(this.getState(), 120.0F, 20.0F, var7);
      GUIColoredRectangle var5 = new GUIColoredRectangle(this.getState(), 120.0F, 20.0F, var7);
      GUIColoredRectangle var6 = new GUIColoredRectangle(this.getState(), 120.0F, 20.0F, var7);
      GUIColoredRectangle var8 = new GUIColoredRectangle(this.getState(), 120.0F, 20.0F, var7);
      var5.getPos().set(0.0F, 30.0F, 0.0F);
      var4.getPos().set(0.0F, 60.0F, 0.0F);
      var3.getPos().set(120.0F, 60.0F, 0.0F);
      var8.getPos().set(0.0F, 90.0F, 0.0F);
      var6.getPos().set(120.0F, 90.0F, 0.0F);
      var2.attach(this.currentTutorialPart);
      var3.attach(this.skipTutorialPart);
      var4.attach(this.repeatTutorialPart);
      var5.attach(this.skipTutorialStep);
      var6.attach(this.endTutorial);
      var8.attach(this.resetTutorial);
      this.attach(var2);
      this.attach(var3);
      this.attach(var4);
      this.attach(var5);
      this.attach(var6);
      this.attach(var8);
      this.init = true;
   }

   public float getHeight() {
      return 180.0F;
   }

   public float getWidth() {
      return 260.0F;
   }

   public boolean isPositionCenter() {
      return false;
   }

   public TutorialMode getTutorial() {
      return ((GameClientState)this.getState()).getController().getTutorialMode();
   }
}
