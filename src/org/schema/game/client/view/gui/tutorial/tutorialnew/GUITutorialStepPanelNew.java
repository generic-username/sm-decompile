package org.schema.game.client.view.gui.tutorial.tutorialnew;

import org.schema.game.client.controller.tutorial.TutorialDialog;
import org.schema.game.client.controller.tutorial.TutorialMode;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIInnerTextbox;
import org.schema.schine.input.InputState;
import org.schema.schine.network.client.ClientState;

public class GUITutorialStepPanelNew extends GUIDialogWindow {
   private TutorialDialog dialog;
   private String message;
   private GUIAncor textAnc;
   private GUITextOverlay msgOverlay;

   public GUITutorialStepPanelNew(String var1, ClientState var2, String var3, TutorialDialog var4, Sprite var5) {
      super(var2, 390, 300, var1);
      this.dialog = var4;
      this.activeInterface = var4;
      this.message = var3;
   }

   public void onInit() {
      super.onInit();
      this.getMainContentPane().setTextBoxHeightLast(25);
      this.getMainContentPane().setListDetailMode((GUIInnerTextbox)this.getMainContentPane().getTextboxes().get(0));
      this.textAnc = new GUIAncor(this.getState(), 10.0F, 10.0F);
      GUIScrollablePanel var1;
      (var1 = new GUIScrollablePanel(10.0F, 10.0F, (GUIElement)this.getMainContentPane().getTextboxes().get(0), this.getState())).setScrollable(GUIScrollablePanel.SCROLLABLE_HORIZONTAL | GUIScrollablePanel.SCROLLABLE_VERTICAL);
      this.msgOverlay = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium14(), this.getState()) {
         public void onDirty() {
            GUITutorialStepPanelNew.this.textAnc.setWidth(GUITutorialStepPanelNew.this.msgOverlay.getMaxLineWidth() + 10);
            GUITutorialStepPanelNew.this.textAnc.setHeight(GUITutorialStepPanelNew.this.msgOverlay.getTextHeight() + 10);
         }
      };
      this.msgOverlay.setTextSimple(this.message);
      this.msgOverlay.onInit();
      this.msgOverlay.updateTextSize();
      this.textAnc.setPos(5.0F, 5.0F, 0.0F);
      this.textAnc.attach(this.msgOverlay);
      this.textAnc.setWidth(this.msgOverlay.getMaxLineWidth());
      this.textAnc.setHeight(this.msgOverlay.getTextHeight());
      var1.setContent(this.textAnc);
      ((GUIInnerTextbox)this.getMainContentPane().getTextboxes().get(0)).attach(var1);
      this.getMainContentPane().addNewTextBox(25);
      GUIHorizontalButtonTablePane var2;
      (var2 = new GUIHorizontalButtonTablePane(this.getState(), 3, 1, (GUIElement)this.getMainContentPane().getTextboxes().get(1))).onInit();
      ((GUIInnerTextbox)this.getMainContentPane().getTextboxes().get(1)).attach(var2);
      var2.addButton(0, 0, "BACK", (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUITutorialStepPanelNew.this.dialog.pressedBack();
            }

         }

         public boolean isOccluded() {
            return !GUITutorialStepPanelNew.this.isActive();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            ((GameClientState)var1).getController().getTutorialMode();
            return true;
         }

         public boolean isActive(InputState var1) {
            TutorialMode var2 = ((GameClientState)var1).getController().getTutorialMode();
            return GUITutorialStepPanelNew.this.isActive() && var2.hasBack();
         }
      });
      var2.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TUTORIAL_TUTORIALNEW_GUITUTORIALSTEPPANELNEW_0, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !GUITutorialStepPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUITutorialStepPanelNew.this.dialog.pressedSkip();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUITutorialStepPanelNew.this.isActive();
         }
      });
      var2.addButton(2, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TUTORIAL_TUTORIALNEW_GUITUTORIALSTEPPANELNEW_1, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !GUITutorialStepPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUITutorialStepPanelNew.this.dialog.pressedNext();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUITutorialStepPanelNew.this.isActive();
         }
      });
      this.getMainContentPane().addNewTextBox(25);
      (var2 = new GUIHorizontalButtonTablePane(this.getState(), 1, 1, (GUIElement)this.getMainContentPane().getTextboxes().get(2))).onInit();
      ((GUIInnerTextbox)this.getMainContentPane().getTextboxes().get(2)).attach(var2);
      var2.addButton(0, 0, new Object() {
         public String toString() {
            return ((GameClientState)GUITutorialStepPanelNew.this.getState()).getController().getTutorialMode() != null && ((GameClientState)GUITutorialStepPanelNew.this.getState()).getController().getTutorialMode().isEndState() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TUTORIAL_TUTORIALNEW_GUITUTORIALSTEPPANELNEW_2 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TUTORIAL_TUTORIALNEW_GUITUTORIALSTEPPANELNEW_3;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !GUITutorialStepPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUITutorialStepPanelNew.this.dialog.pressedEnd();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUITutorialStepPanelNew.this.isActive();
         }
      });
      this.orientate(48);
   }

   public void updateMessage(String var1, Sprite var2, TutorialDialog var3) {
      this.dialog = var3;
      this.activeInterface = var3;
      this.msgOverlay.setTextSimple(var1);
      this.msgOverlay.updateTextSize();
      this.textAnc.setWidth(this.msgOverlay.getMaxLineWidth() + 10);
      this.textAnc.setHeight(this.msgOverlay.getTextHeight() + 10);
   }
}
