package org.schema.game.client.view.gui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ConcurrentHashMap;
import javax.vecmath.Vector3f;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.manager.ingame.ship.InShipControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.admin.AdminCommands;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;

public class PlayerStatisticsPanel extends GUIElement implements Observer {
   private int nameSpace = 170;
   private int optionExtraSpace = 90;
   private GUIOverlay background;
   private GUIScrollablePanel scrollPanel;
   private GUIElementList panelList;
   private boolean firstDraw = true;
   private boolean reconstructionRequested;
   private ConcurrentHashMap playerMap = new ConcurrentHashMap();

   public PlayerStatisticsPanel(InputState var1) {
      super(var1);
      this.initialize();
      ((GameClientState)var1).addObserver(this);
   }

   public InShipControlManager getInShipControlManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager();
   }

   private void initialize() {
      this.background = new GUIOverlay(Controller.getResLoader().getSprite("panel-std-gui-"), this.getState());
      this.scrollPanel = new GUIScrollablePanel(512.0F, 366.0F, this.getState());
      this.panelList = new GUIElementList(this.getState());
      this.scrollPanel.setContent(this.panelList);
      this.attach(this.background);
      this.background.attach(this.scrollPanel);
      this.scrollPanel.setPos(260.0F, 64.0F, 0.0F);
   }

   private void reconstructList() {
      StateInterface var1;
      synchronized((var1 = (StateInterface)this.getState()).getLocalAndRemoteObjectContainer().getLocalObjects()) {
         for(int var3 = 0; var3 < this.panelList.size(); ++var3) {
            PlayerState var4;
            if ((var4 = (PlayerState)this.panelList.get(var3).getUserPointer()) != null && !var1.getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(var4.getId())) {
               this.panelList.remove(var3);
               this.playerMap.remove(var4);
               --var3;
            }
         }

         Iterator var6 = var1.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

         while(true) {
            if (!var6.hasNext()) {
               break;
            }

            Sendable var7;
            if ((var7 = (Sendable)var6.next()) instanceof PlayerState && !this.playerMap.containsKey(var7)) {
               this.panelList.add((GUIListElement)(new PlayerStatisticsPanel.PlayerListElement((PlayerState)var7, this.getState())));
            }
         }
      }

      this.panelList.updateDim();
   }

   public void update(Observable var1, Object var2) {
      this.reconstructionRequested = true;
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().hinderInteraction(30);
      GlUtil.glPushMatrix();
      this.transform();
      this.background.draw();
      GlUtil.glPopMatrix();
   }

   public float getHeight() {
      return this.background.getHeight();
   }

   public float getWidth() {
      return this.background.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void onInit() {
      this.background.onInit();
      this.scrollPanel.onInit();
      this.firstDraw = false;
      PlayerStatisticsPanel.TopTableElement var1;
      (var1 = new PlayerStatisticsPanel.TopTableElement(this.getState())).update("NAME (*upgraded)", "PING", "FACTION", "OPTION");
      GUIListElement var2 = new GUIListElement(var1, var1, this.getState());
      this.panelList.add(var2);
   }

   public void update(Timer var1) {
      super.update(var1);
      if (this.reconstructionRequested) {
         this.reconstructList();
         this.reconstructionRequested = false;
      }

      for(int var2 = 0; var2 < this.panelList.size(); ++var2) {
         this.panelList.get(var2).update(var1);
      }

   }

   class PlayerTableElement extends GUIElement {
      GUITextOverlay aText;
      GUITextOverlay bText;
      GUITextOverlay dText;
      GUITextButton eText;
      int space = 60;

      public PlayerTableElement(final InputState var2, final PlayerState var3) {
         super(var2);
         this.aText = new GUITextOverlay(300, 30, FontLibrary.getBoldArial12White(), var2);
         this.aText.setText(new ArrayList());
         this.aText.getText().add(new Object() {
            public String toString() {
               return var3.getName() + (var3.isUpgradedAccount() ? "*" : "");
            }
         });
         this.bText = new GUITextOverlay(300, 30, FontLibrary.getBoldArial12White(), var2);
         this.bText.setText(new ArrayList());
         this.bText.getText().add(new Object() {
            public String toString() {
               return String.valueOf(var3.getPing());
            }
         });
         Vector3f var10000 = this.bText.getPos();
         var10000.x += (float)(this.space + PlayerStatisticsPanel.this.nameSpace);
         this.dText = new GUITextOverlay(300, 30, FontLibrary.getBoldArial12White(), var2);
         this.dText.setText(new ArrayList());
         this.dText.getText().add(new Object() {
            public String toString() {
               return var3.getFactionController().getFactionName();
            }
         });
         var10000 = this.dText.getPos();
         var10000.x += (float)(2 * this.space + PlayerStatisticsPanel.this.nameSpace);
         this.eText = new GUITextButton(var2, 50, 20, FontLibrary.getBoldArial12White(), new Object() {
            public String toString() {
               return ((GameClientState)var2).getPlayer().getNetworkObject().isAdminClient.get() ? "options" : "-";
            }
         }, new GUICallback() {
            public boolean isOccluded() {
               return false;
            }

            public void callback(GUIElement var1, MouseEvent var2x) {
               if (var2x.pressedLeftMouse() && ((GameClientState)var2).getPlayer().getNetworkObject().isAdminClient.get()) {
                  final PlayerGameOkCancelInput var6;
                  (var6 = new PlayerGameOkCancelInput("PlayerStatisticsPanel_PLAYER_ADMIN_OPTIONS", (GameClientState)var2, "Player Admin Options: " + var3.getName(), "") {
                     public boolean isOccluded() {
                        return false;
                     }

                     public void onDeactivate() {
                     }

                     public void pressedOK() {
                        this.deactivate();
                     }
                  }).getInputPanel().setOkButton(false);
                  var6.getInputPanel().onInit();
                  GUITextButton var7 = new GUITextButton(var2, 140, 20, FontLibrary.getBoldArial12White(), new Object() {
                     public String toString() {
                        return ((GameClientState)var2).getPlayer().getNetworkObject().isAdminClient.get() ? "Kick" : "-";
                     }
                  }, new GUICallback() {
                     public void callback(GUIElement var1, MouseEvent var2x) {
                        if (var2x.pressedLeftMouse() && ((GameClientState)var2).getPlayer().getNetworkObject().isAdminClient.get()) {
                           var6.deactivate();
                           (new PlayerGameTextInput("PlayerStatisticsPanel_KICK", (GameClientState)var2, 100, "Kick", "Enter Reason") {
                              public String[] getCommandPrefixes() {
                                 return null;
                              }

                              public boolean isOccluded() {
                                 return false;
                              }

                              public String handleAutoComplete(String var1, TextCallback var2x, String var3x) throws PrefixNotFoundException {
                                 return null;
                              }

                              public void onFailedTextCheck(String var1) {
                              }

                              public void onDeactivate() {
                                 var6.activate();
                              }

                              public boolean onInput(String var1) {
                                 ((GameClientState)var2).getController().sendAdminCommand(AdminCommands.KICK_REASON, var3.getName(), var1);
                                 return true;
                              }
                           }).activate();
                        }

                     }

                     public boolean isOccluded() {
                        return false;
                     }
                  });
                  GUITextButton var3x = new GUITextButton(var2, 140, 20, FontLibrary.getBoldArial12White(), new Object() {
                     public String toString() {
                        return ((GameClientState)var2).getPlayer().getNetworkObject().isAdminClient.get() ? "Ban StarMade Account" : "-";
                     }
                  }, new GUICallback() {
                     public void callback(GUIElement var1, MouseEvent var2x) {
                        if (var2x.pressedLeftMouse() && ((GameClientState)var2).getPlayer().getNetworkObject().isAdminClient.get()) {
                           if (var3.getStarmadeName() != null) {
                              ((GameClientState)var2).getController().sendAdminCommand(AdminCommands.BAN_ACCOUNT_BY_PLAYERNAME, var3.getName());
                              return;
                           }

                           ((GameClientState)var2).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_PLAYERSTATISTICSPANEL_0, 0.0F);
                        }

                     }

                     public boolean isOccluded() {
                        return false;
                     }
                  });
                  GUITextButton var4 = new GUITextButton(var2, 140, 20, FontLibrary.getBoldArial12White(), new Object() {
                     public String toString() {
                        return ((GameClientState)var2).getPlayer().getNetworkObject().isAdminClient.get() ? "Ban Player Name" : "-";
                     }
                  }, new GUICallback() {
                     public void callback(GUIElement var1, MouseEvent var2x) {
                        if (var2x.pressedLeftMouse() && ((GameClientState)var2).getPlayer().getNetworkObject().isAdminClient.get()) {
                           ((GameClientState)var2).getController().sendAdminCommand(AdminCommands.BAN, var3.getName(), false);
                        }

                     }

                     public boolean isOccluded() {
                        return false;
                     }
                  });
                  GUITextButton var5 = new GUITextButton(var2, 140, 20, FontLibrary.getBoldArial12White(), new Object() {
                     public String toString() {
                        return ((GameClientState)var2).getPlayer().getNetworkObject().isAdminClient.get() ? "Ban IP" : "-";
                     }
                  }, new GUICallback() {
                     public void callback(GUIElement var1, MouseEvent var2x) {
                        if (var2x.pressedLeftMouse() && ((GameClientState)var2).getPlayer().getNetworkObject().isAdminClient.get()) {
                           ((GameClientState)var2).getController().sendAdminCommand(AdminCommands.BAN_IP_BY_PLAYERNAME, var3.getName());
                        }

                     }

                     public boolean isOccluded() {
                        return false;
                     }
                  });
                  var6.getInputPanel().getContent().attach(var7);
                  var4.getPos().y = 24.0F;
                  var6.getInputPanel().getContent().attach(var4);
                  var3x.getPos().y = 48.0F;
                  var6.getInputPanel().getContent().attach(var3x);
                  var5.getPos().y = 72.0F;
                  var6.getInputPanel().getContent().attach(var5);
                  var6.activate();
               }

            }
         });
         var10000 = this.eText.getPos();
         var10000.x += (float)(3 * this.space + PlayerStatisticsPanel.this.nameSpace + PlayerStatisticsPanel.this.optionExtraSpace);
      }

      public void cleanUp() {
      }

      public void draw() {
         this.transform();
         this.aText.draw();
         this.bText.draw();
         this.dText.draw();
         this.eText.draw();
      }

      public float getHeight() {
         return this.aText.getHeight();
      }

      public float getWidth() {
         return this.aText.getWidth() * 4.0F + (float)(4 * this.space);
      }

      public boolean isPositionCenter() {
         return false;
      }

      public void onInit() {
         this.aText.onInit();
         this.bText.onInit();
         this.dText.onInit();
         this.eText.onInit();
      }
   }

   class TopTableElement extends GUIElement {
      GUITextOverlay aText;
      GUITextOverlay bText;
      GUITextOverlay dText;
      GUITextOverlay eText;
      int space = 60;

      public TopTableElement(InputState var2) {
         super(var2);
         this.aText = new GUITextOverlay(300, 30, FontLibrary.getBoldArial12White(), var2);
         this.aText.setText(new ArrayList());
         this.aText.getText().add("");
         this.bText = new GUITextOverlay(300, 30, FontLibrary.getBoldArial12White(), var2);
         this.bText.setText(new ArrayList());
         this.bText.getText().add("");
         Vector3f var10000 = this.bText.getPos();
         var10000.x += (float)(this.space + PlayerStatisticsPanel.this.nameSpace);
         this.dText = new GUITextOverlay(300, 30, FontLibrary.getBoldArial12White(), var2);
         this.dText.setText(new ArrayList());
         this.dText.getText().add("");
         var10000 = this.dText.getPos();
         var10000.x += (float)(2 * this.space + PlayerStatisticsPanel.this.nameSpace);
         this.eText = new GUITextOverlay(300, 30, FontLibrary.getBoldArial12White(), var2);
         this.eText.setText(new ArrayList());
         this.eText.getText().add("");
         var10000 = this.eText.getPos();
         var10000.x += (float)(3 * this.space + PlayerStatisticsPanel.this.nameSpace + PlayerStatisticsPanel.this.optionExtraSpace);
      }

      public void cleanUp() {
      }

      public float getHeight() {
         return this.aText.getHeight();
      }

      public void draw() {
         this.transform();
         this.aText.draw();
         this.bText.draw();
         this.dText.draw();
         this.eText.draw();
      }

      public void update(String var1, String var2, String var3, String var4) {
         this.aText.getText().set(0, var1);
         this.bText.getText().set(0, var2);
         this.dText.getText().set(0, var3);
         this.eText.getText().set(0, var4);
      }

      public float getWidth() {
         return this.aText.getWidth() * 4.0F + (float)(4 * this.space);
      }

      public boolean isPositionCenter() {
         return false;
      }

      public void onInit() {
         this.aText.onInit();
         this.bText.onInit();
         this.dText.onInit();
         this.eText.onInit();
      }
   }

   class PlayerListElement extends GUIListElement {
      private PlayerState player;

      public PlayerListElement(PlayerState var2, InputState var3) {
         super(var3);
         PlayerStatisticsPanel.PlayerTableElement var4 = PlayerStatisticsPanel.this.new PlayerTableElement(var3, var2);
         this.setContent(var4);
         this.setSelectContent(var4);
         this.player = var2;
         this.update((Timer)null);
         this.setUserPointer(var2);
         PlayerStatisticsPanel.this.playerMap.put(var2, this);
      }

      public int hashCode() {
         return this.player.getId();
      }

      public boolean equals(Object var1) {
         return this.player.equals(((PlayerStatisticsPanel.PlayerListElement)var1).player);
      }

      public void update(Timer var1) {
      }
   }
}
