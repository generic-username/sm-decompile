package org.schema.game.client.view.gui.options;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.Map.Entry;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.schine.common.JoystickAxisMapping;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.EngineSettingsChangeListener;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICheckBox;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIEnterableList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUISettingSelector;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.SettingsInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.BasicInputController;
import org.schema.schine.input.InputState;
import org.schema.schine.input.JoystickAxisSingleMap;
import org.schema.schine.input.JoystickButtonMapping;
import org.schema.schine.input.JoystickEvent;
import org.schema.schine.input.JoystickMappingFile;
import org.schema.schine.input.KeyboardContext;
import org.schema.schine.input.KeyboardMappings;

public class GUIJoystickPanel extends GUIAncor implements Observer {
   private static final float TOP = 40.0F;
   private GUIElementList list;
   private boolean reconstructNeeded;
   private boolean init;
   private GUITextOverlay overlayNoJoy;

   public GUIJoystickPanel(GUIScrollablePanel var1, InputState var2) {
      super(var2);
      this.list = new GUIElementList(var2);
      this.list.setScrollPane(var1);
      this.reconstructNeeded = true;
      this.list.setPos(0.0F, 40.0F, 0.0F);
      this.attach(this.list);
      this.overlayNoJoy = new GUITextOverlay(40, 30, this.getState());
      this.overlayNoJoy.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_GUIJOYSTICKPANEL_0);
   }

   public void draw() {
      if (Controller.getControllerInput().getSize() == 0) {
         GlUtil.glPushMatrix();
         this.transform();
         this.overlayNoJoy.draw();
         GlUtil.glPopMatrix();
      } else {
         if (!this.init) {
            this.onInit();
            this.init = true;
         }

         if (this.reconstructNeeded) {
            this.reconstructList();
            this.reconstructNeeded = false;
         }

         super.draw();
      }
   }

   public void onInit() {
      GUITextOverlay[] var1 = new GUITextOverlay[Controller.getControllerInput().getSize()];

      for(int var2 = 0; var2 < Controller.getControllerInput().getSize(); ++var2) {
         var1[var2] = new GUITextOverlay(180, 15, this.getState());
         String var3 = Controller.getControllerInput().getName(var2);
         var1[var2].setTextSimple(var3.length() != 0 && !var3.equals("?") ? var3 : "unknown device");
         var1[var2].setUserPointer(var2);
      }

      Integer var4 = (Integer)EngineSettings.C_SELECTED_JOYSTICK.getCurrentState();
      Controller.getControllerInput().select(var4);
      GUIDropDownList var5;
      (var5 = new GUIDropDownList(this.getState(), 300, 32, 300, new DropDownCallback() {
         public void onSelectionChanged(GUIListElement var1) {
            if ((Integer)EngineSettings.C_SELECTED_JOYSTICK.getCurrentState() != (Integer)var1.getContent().getUserPointer()) {
               EngineSettings.C_SELECTED_JOYSTICK.setCurrentState(var1.getContent().getUserPointer());
               Controller.getControllerInput().select((Integer)EngineSettings.C_SELECTED_JOYSTICK.getCurrentState());
               System.err.println("JOYSTICK SET TO " + var1.getContent().getUserPointer());
               GUIJoystickPanel.this.reconstructNeeded = true;
            }

         }
      }, var1)).setSelectedIndex(var4);
      var5.onInit();
      this.attach(var5);
   }

   public float getHeight() {
      return this.list.getHeight() + 40.0F;
   }

   public float getWidth() {
      return this.list.getWidth();
   }

   public void reconstructList() {
      this.list.clear();
      GUIJoystickVirt var1;
      (var1 = new GUIJoystickVirt(this.getState())).onInit();
      this.list.add(new GUIListElement(var1, var1, this.getState()));
      final BasicInputController var8 = this.getState().getController().getInputController();
      GUIAncor var2 = new GUIAncor(this.getState(), 500.0F, 20.0F);
      GUITextOverlay var3;
      (var3 = new GUITextOverlay(30, 20, this.getState())).setTextSimple("in-game function");
      var3.getPos().x = 4.0F;
      GUITextOverlay var4;
      (var4 = new GUITextOverlay(30, 20, this.getState())).setTextSimple("assigned joystick axis");
      var4.getPos().x = 200.0F;
      GUITextOverlay var5;
      (var5 = new GUITextOverlay(30, 20, this.getState())).setTextSimple("invert");
      var5.getPos().x = 357.0F;
      GUITextOverlay var6;
      (var6 = new GUITextOverlay(30, 20, this.getState())).setTextSimple("sensivity");
      var6.getPos().x = 412.0F;
      var2.attach(var3);
      var2.attach(var4);
      var2.attach(var5);
      var2.attach(var6);
      this.list.add(new GUIListElement(var2, var2, this.getState()));
      Iterator var9 = var8.getJoystick().getAxis().entrySet().iterator();

      while(var9.hasNext()) {
         final Entry var10 = (Entry)var9.next();
         GUISettingSelector var12 = new GUISettingSelector(this.getState(), 100, 30, (GUIActiveInterface)null, new JoystickSettingInterface(var8.getJoystick(), (JoystickAxisMapping)var10.getKey(), ((JoystickAxisSingleMap)var10.getValue()).mapping, this.getState()));
         var5 = new GUITextOverlay(200, 20, this.getState());
         GUICheckBox var13 = new GUICheckBox(this.getState()) {
            protected void activate() throws StateParameterNotFoundException {
               var8.getJoystick().invertedAxis((JoystickAxisMapping)var10.getKey(), true);
            }

            protected void deactivate() throws StateParameterNotFoundException {
               var8.getJoystick().invertedAxis((JoystickAxisMapping)var10.getKey(), false);
            }

            protected boolean isActivated() {
               return var8.getJoystick().isAxisInverted((JoystickAxisMapping)var10.getKey());
            }
         };
         GUISettingSelector var7;
         (var7 = new GUISettingSelector(this.getState(), 30, 30, (GUIActiveInterface)null, new SettingsInterface() {
            public void switchSetting() throws StateParameterNotFoundException {
               if (var8.getJoystick().getSensivity((JoystickAxisMapping)var10.getKey()) < 1000.0F) {
                  var8.getJoystick().modSensivity((JoystickAxisMapping)var10.getKey(), 0.1F);
               }

            }

            public Object getCurrentState() {
               return var8.getJoystick().getSensivity((JoystickAxisMapping)var10.getKey());
            }

            public void switchSettingBack() throws StateParameterNotFoundException {
               if (var8.getJoystick().getSensivity((JoystickAxisMapping)var10.getKey()) > 0.1F) {
                  var8.getJoystick().modSensivity((JoystickAxisMapping)var10.getKey(), -0.1F);
               }

            }

            public void onSwitchedSetting(InputState var1) {
            }

            public void setCurrentState(Object var1) {
            }

            public void addChangeListener(EngineSettingsChangeListener var1) {
            }

            public void removeChangeListener(EngineSettingsChangeListener var1) {
            }
         })).onInit();
         var5.setTextSimple(((JoystickAxisMapping)var10.getKey()).desc);
         GUIAncor var11 = new GUIAncor(this.getState(), 500.0F, 32.0F);
         var12.getPos().x = 180.0F;
         var13.getPos().x = var12.getPos().x + var12.getWidth() + 10.0F;
         var7.getPos().x = var13.getPos().x + 35.0F;
         var11.attach(var5);
         var11.attach(var12);
         var11.attach(var13);
         var11.attach(var7);
         this.list.add(new GUIListElement(var11, var11, this.getState()));
      }

      this.list.add((GUIListElement)(new GUIMappingInputPanel(this.getState(), "Primary Action", new GUIAbstractJoystickElement(this.getState()) {
         public boolean hasDuplicate() {
            if (!var8.getJoystick().getLeftMouse().isSet()) {
               return false;
            } else if (var8.getJoystick().getLeftMouse().equals(var8.getJoystick().getRightMouse())) {
               return true;
            } else {
               KeyboardMappings[] var1;
               int var2 = (var1 = KeyboardMappings.values()).length;

               for(int var3 = 0; var3 < var2; ++var3) {
                  KeyboardMappings var4 = var1[var3];
                  if (var8.getJoystick().getLeftMouse().equals(var8.getJoystick().getButtonFor(var4))) {
                     return true;
                  }
               }

               return false;
            }
         }

         public void mapJoystickPressedNothing() {
            var8.getJoystick().setLeftMouse(new JoystickButtonMapping());
         }

         public boolean isHighlighted() {
            return false;
         }

         public void mapJoystickPressed(JoystickEvent var1) {
            var8.getJoystick().setLeftMouse(JoystickMappingFile.getPressedButton());
         }

         public String getDesc() {
            return "Primary Action";
         }

         public String getCurrentSettingString() {
            return var8.getJoystick().getLeftMouse().toString();
         }
      }, this.list.size() % 2 == 0)));
      this.list.add((GUIListElement)(new GUIMappingInputPanel(this.getState(), "Secondary Action", new GUIAbstractJoystickElement(this.getState()) {
         public boolean isHighlighted() {
            return false;
         }

         public void mapJoystickPressedNothing() {
            var8.getJoystick().setRightMouse(new JoystickButtonMapping());
         }

         public void mapJoystickPressed(JoystickEvent var1) {
            var8.getJoystick().setRightMouse(JoystickMappingFile.getPressedButton());
         }

         public String getDesc() {
            return "Secondary Action";
         }

         public String getCurrentSettingString() {
            return var8.getJoystick().getRightMouse().toString();
         }

         public boolean hasDuplicate() {
            if (!var8.getJoystick().getRightMouse().isSet()) {
               return false;
            } else if (var8.getJoystick().getLeftMouse().equals(var8.getJoystick().getRightMouse())) {
               return true;
            } else {
               KeyboardMappings[] var1;
               int var2 = (var1 = KeyboardMappings.values()).length;

               for(int var3 = 0; var3 < var2; ++var3) {
                  KeyboardMappings var4 = var1[var3];
                  if (var8.getJoystick().getRightMouse().equals(var8.getJoystick().getButtonFor(var4))) {
                     return true;
                  }
               }

               return false;
            }
         }
      }, this.list.size() % 2 == 0)));
      this.createButtons();
      this.list.onInit();
   }

   private void createButtons() {
      BasicInputController var1 = this.getState().getController().getInputController();
      ArrayList var2 = new ArrayList();
      KeyboardContext[] var3;
      int var4 = (var3 = KeyboardContext.values()).length;

      int var5;
      GUIEnterableList var13;
      for(var5 = 0; var5 < var4; ++var5) {
         KeyboardContext var6 = var3[var5];
         GUITextOverlay var7;
         (var7 = new GUITextOverlay(176, 30, FontLibrary.getBoldArialGreen15(), this.getState())).setText(new ArrayList());
         var7.getText().add("+ " + var6.getDesc());
         Vector3f var10000 = var7.getPos();
         var10000.y += 8.0F;
         GUIColoredRectangle var8 = new GUIColoredRectangle(this.getState(), 468.0F, 30.0F, new Vector4f(0.0F, 0.0F, 0.0F, 0.8F));
         GUITextOverlay var9;
         (var9 = new GUITextOverlay(176, 30, FontLibrary.getBoldArialGreen15(), this.getState())).setText(new ArrayList());
         var9.getText().add(var6.getDesc());
         var9.setMouseUpdateEnabled(true);
         var10000 = var9.getPos();
         var10000.y += 8.0F;
         var8.attach(var9);
         (var13 = new GUIEnterableList(this.getState(), var7, var8)).getPos().x = (float)(var6.getLvl() * 5);
         var13.addObserver(this);
         var13.setUserPointer("CATEGORY");
         var13.onInit();
         var13.setMouseUpdateEnabled(true);
         var13.setExpanded(true);
         GUIListElement var11 = new GUIListElement(var13, var13, this.getState());
         var13.setParent(this);
         this.list.add(var11);
         var2.add(var11);
      }

      KeyboardMappings[] var10;
      var4 = (var10 = KeyboardMappings.values()).length;

      for(var5 = 0; var5 < var4; ++var5) {
         KeyboardMappings var12 = var10[var5];
         (var13 = (GUIEnterableList)((GUIListElement)var2.get(var12.getContext().ordinal())).getContent()).getList().add((GUIListElement)(new GUIMappingInputPanel(this.getState(), var12.getDescription(), new GUIJoystickElement(this.getState(), var12, var1.getJoystick()), var13.getList().size() % 2 == 0)));
      }

      this.list.updateDim();
   }

   public void update(Observable var1, Object var2) {
      this.list.updateDim();
   }
}
