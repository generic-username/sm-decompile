package org.schema.game.client.view.gui.options;

import org.schema.common.util.StringTools;
import org.schema.schine.common.JoystickAxisMapping;
import org.schema.schine.graphicsengine.core.settings.EngineSettingsChangeListener;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.SettingsInterface;
import org.schema.schine.input.InputState;
import org.schema.schine.input.JoystickMappingFile;

public class JoystickSettingInterface implements SettingsInterface {
   public int assignedTo = -1;
   JoystickAxisMapping map;
   private JoystickMappingFile file;
   private InputState clientState;

   public JoystickSettingInterface(JoystickMappingFile var1, JoystickAxisMapping var2, int var3, InputState var4) {
      this.map = var2;
      this.assignedTo = var3;
      this.file = var1;
      this.clientState = var4;
   }

   public void switchSetting() throws StateParameterNotFoundException {
      if (JoystickMappingFile.ok()) {
         if (this.assignedTo >= JoystickMappingFile.getAxesCount() - 1) {
            this.assignedTo = -1;
         } else {
            ++this.assignedTo;
         }

         this.onSwitchedSetting(this.clientState);
      }
   }

   public Object getCurrentState() {
      if (!JoystickMappingFile.ok()) {
         return "invalid joysick";
      } else {
         if (this.assignedTo >= JoystickMappingFile.getAxesCount()) {
            this.assignedTo = JoystickMappingFile.getAxesCount() - 1;
         }

         return new Object() {
            public String toString() {
               if (JoystickSettingInterface.this.assignedTo >= JoystickMappingFile.getAxesCount()) {
                  JoystickSettingInterface.this.assignedTo = -1;
               }

               return JoystickSettingInterface.this.assignedTo == -1 ? "NONE" : JoystickMappingFile.getAxisName(JoystickSettingInterface.this.assignedTo) + " (" + StringTools.formatPointZero(JoystickSettingInterface.this.file.getAxis(JoystickSettingInterface.this.map)) + ")";
            }
         };
      }
   }

   public void switchSettingBack() throws StateParameterNotFoundException {
      if (JoystickMappingFile.ok()) {
         if (this.assignedTo < 0) {
            this.assignedTo = JoystickMappingFile.getAxesCount() - 1;
         } else {
            --this.assignedTo;
         }

         this.onSwitchedSetting(this.clientState);
      }
   }

   public void onSwitchedSetting(InputState var1) {
      if (JoystickMappingFile.ok()) {
         this.file.setAxis(this.map, this.assignedTo);
      }
   }

   public void setCurrentState(Object var1) {
      assert false;

   }

   public void addChangeListener(EngineSettingsChangeListener var1) {
   }

   public void removeChangeListener(EngineSettingsChangeListener var1) {
   }
}
