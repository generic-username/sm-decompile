package org.schema.game.client.view.gui.options.newoptions;

import java.awt.Component;
import java.awt.HeadlessException;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.mainmenu.MainMenuGUI;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.game.common.gui.CustomSkinCreateDialog;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.EngineSettingsType;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIEngineSettingsCheckBox;
import org.schema.schine.graphicsengine.forms.gui.GUISettingSelector;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.graphicsengine.forms.gui.newgui.settingsnew.GUISettingsElementPanelNew;
import org.schema.schine.input.InputState;
import org.schema.schine.resource.FileExt;

public class OptionsScrollableListNew extends ScrollableTableList implements Observer {
   private EngineSettingsType settingsType;
   private GUIActiveInterface a;

   public OptionsScrollableListNew(InputState var1, GUIActiveInterface var2, GUIElement var3, EngineSettingsType var4) {
      super(var1, 100.0F, 100.0F, var3);
      this.a = var2;
      this.settingsType = var4;
      this.columnsHeight = 32;
   }

   public void cleanUp() {
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_OPTIONSSCROLLABLELISTNEW_3, 0, new Comparator() {
         public int compare(EngineSettings var1, EngineSettings var2) {
            return var1.ordinal() - var2.ordinal();
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_OPTIONSSCROLLABLELISTNEW_0, 7.0F, new Comparator() {
         public int compare(EngineSettings var1, EngineSettings var2) {
            return var1.getDescription().compareToIgnoreCase(var2.getDescription());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_OPTIONSSCROLLABLELISTNEW_1, 3.0F, new Comparator() {
         public int compare(EngineSettings var1, EngineSettings var2) {
            return 0;
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, EngineSettings var2) {
            return var2.getDescription().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_OPTIONSSCROLLABLELISTNEW_2, ControllerElement.FilterRowStyle.FULL);
   }

   protected Collection getElementList() {
      return Arrays.asList(EngineSettings.values());
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      Iterator var9 = var2.iterator();

      while(var9.hasNext()) {
         EngineSettings var3 = (EngineSettings)var9.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         final GUIAncor var5 = new GUIAncor(this.getState());
         Object var6;
         if (var3 == EngineSettings.PLAYER_SKIN_CREATE) {
            ((GUIElement)(var6 = new GUIHorizontalButton(this.getState(), GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new Object() {
               public String toString() {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_OPTIONSSCROLLABLELISTNEW_4;
               }
            }, new GUICallback() {
               public boolean isOccluded() {
                  return !OptionsScrollableListNew.this.a.isActive();
               }

               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse()) {
                     MainMenuGUI.runningSwingDialog = true;
                     SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                           CustomSkinCreateDialog var1;
                           (var1 = new CustomSkinCreateDialog((JFrame)null)).setDefaultCloseOperation(2);
                           var1.setVisible(true);
                           var1.addWindowListener(new WindowListener() {
                              public void windowOpened(WindowEvent var1) {
                              }

                              public void windowIconified(WindowEvent var1) {
                              }

                              public void windowDeiconified(WindowEvent var1) {
                              }

                              public void windowDeactivated(WindowEvent var1) {
                              }

                              public void windowClosing(WindowEvent var1) {
                                 MainMenuGUI.runningSwingDialog = false;
                              }

                              public void windowClosed(WindowEvent var1) {
                                 MainMenuGUI.runningSwingDialog = false;
                              }

                              public void windowActivated(WindowEvent var1) {
                              }
                           });
                        }
                     });
                  }

               }
            }, this.a, new GUIActivationCallback() {
               public boolean isVisible(InputState var1) {
                  return true;
               }

               public boolean isActive(InputState var1) {
                  return OptionsScrollableListNew.this.a.isActive();
               }
            }) {
               public void draw() {
                  this.setWidth(var5.getWidth());
                  super.draw();
               }
            })).onInit();
         } else if (var3 == EngineSettings.PLAYER_SKIN) {
            var6 = new GUIAncor(this.getState()) {
               public void draw() {
                  this.setWidth(var5.getWidth());
                  this.setHeight(OptionsScrollableListNew.this.columnsHeight);
                  super.draw();
               }
            };
            GUIHorizontalButton var7;
            (var7 = new GUIHorizontalButton(this.getState(), GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new Object() {
               public String toString() {
                  String var1;
                  if ((var1 = EngineSettings.PLAYER_SKIN.getCurrentState().toString().trim()).length() > 0) {
                     FileExt var2 = new FileExt(var1);
                     return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_OPTIONSSCROLLABLELISTNEW_5, var2.getName());
                  } else {
                     return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_OPTIONSSCROLLABLELISTNEW_6;
                  }
               }
            }, new GUICallback() {
               public boolean isOccluded() {
                  return !OptionsScrollableListNew.this.a.isActive();
               }

               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse()) {
                     MainMenuGUI.runningSwingDialog = true;
                     SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                           JDialog var1;
                           (var1 = new JDialog()).setAlwaysOnTop(true);
                           var1.setVisible(true);
                           JFileChooser var2 = new JFileChooser(new FileExt("./")) {
                              private static final long serialVersionUID = 1L;

                              protected JDialog createDialog(Component var1) throws HeadlessException {
                                 JDialog var2;
                                 (var2 = super.createDialog(var1)).setLocationByPlatform(true);
                                 var2.setModal(true);
                                 var2.setAlwaysOnTop(true);
                                 return var2;
                              }
                           };
                           FileFilter var3 = new FileFilter() {
                              public boolean accept(File var1) {
                                 if (var1.isDirectory()) {
                                    return true;
                                 } else {
                                    return var1.getName().endsWith(".smskin");
                                 }
                              }

                              public String getDescription() {
                                 return ".smskin (StarMade Skin)";
                              }
                           };
                           var2.addChoosableFileFilter(var3);
                           var2.setFileFilter(var3);
                           var2.setAcceptAllFileFilterUsed(false);
                           if (var2.showDialog((Component)null, "Select Skin") == 0) {
                              File var5 = var2.getSelectedFile();
                              EngineSettings.PLAYER_SKIN.setCurrentState(var5.getAbsolutePath());

                              try {
                                 EngineSettings.write();
                              } catch (IOException var4) {
                                 var4.printStackTrace();
                              }
                           }

                           var1.dispose();
                           MainMenuGUI.runningSwingDialog = false;
                        }
                     });
                  }

               }
            }, this.a, new GUIActivationCallback() {
               public boolean isVisible(InputState var1) {
                  return true;
               }

               public boolean isActive(InputState var1) {
                  return OptionsScrollableListNew.this.a.isActive();
               }
            }) {
               public void draw() {
                  this.setWidth(var5.getWidth() - 20.0F);
                  super.draw();
               }
            }).onInit();
            GUIHorizontalButton var8;
            (var8 = new GUIHorizontalButton(this.getState(), GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new Object() {
               public String toString() {
                  return "X";
               }
            }, new GUICallback() {
               public boolean isOccluded() {
                  return !OptionsScrollableListNew.this.a.isActive();
               }

               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse()) {
                     EngineSettings.PLAYER_SKIN.setCurrentState("");

                     try {
                        EngineSettings.write();
                        return;
                     } catch (IOException var3) {
                        var3.printStackTrace();
                     }
                  }

               }
            }, this.a, new GUIActivationCallback() {
               public boolean isVisible(InputState var1) {
                  return true;
               }

               public boolean isActive(InputState var1) {
                  String var2 = EngineSettings.PLAYER_SKIN.getCurrentState().toString().trim();
                  return OptionsScrollableListNew.this.a.isActive() && var2.length() > 0;
               }
            }) {
               public void draw() {
                  this.setWidth(20);
                  this.setPos(var5.getWidth() - 20.0F, 0.0F, 0.0F);
                  super.draw();
               }
            }).onInit();
            ((GUIElement)var6).attach(var7);
            ((GUIElement)var6).attach(var8);
         } else if (var3.getCurrentState() instanceof Boolean) {
            var6 = new GUIEngineSettingsCheckBox(this.getState(), this.a, var3);
         } else {
            GUISettingSelector var11;
            (var11 = new GUISettingSelector(this.getState(), this.a, var3)).dependent = var5;
            var6 = var11;
         }

         GUISettingsElementPanelNew var12 = new GUISettingsElementPanelNew(this.getState(), (GUIElement)var6, false, false);
         var4.setTextSimple(var3.getDescription());
         var4.getPos().y = 5.0F;
         GUIAncor var13 = new GUIAncor(this.getState());
         OptionsScrollableListNew.SettingRow var10;
         (var10 = new OptionsScrollableListNew.SettingRow(this.getState(), var3, new GUIElement[]{var13, var4, var12})).onInit();
         var10.useColumnWidthElements[2] = var5;
         var1.addWithoutUpdate(var10);
      }

      var1.updateDim();
   }

   protected boolean isFiltered(EngineSettings var1) {
      return super.isFiltered(var1) || var1.getType() != this.settingsType || var1.isDebug();
   }

   public boolean isPlayerAdmin() {
      return ((GameClientState)this.getState()).getPlayer().getNetworkObject().isAdminClient.get();
   }

   public boolean canEdit(CatalogPermission var1) {
      return var1.ownerUID.toLowerCase(Locale.ENGLISH).equals(((GameClientState)this.getState()).getPlayer().getName().toLowerCase(Locale.ENGLISH)) || this.isPlayerAdmin();
   }

   class SettingRow extends ScrollableTableList.Row {
      public SettingRow(InputState var2, EngineSettings var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
