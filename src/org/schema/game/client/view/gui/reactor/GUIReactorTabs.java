package org.schema.game.client.view.gui.reactor;

import java.util.Iterator;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorElement;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorTree;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.font.FontStyle;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangleOutline;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUIToolTip;
import org.schema.schine.graphicsengine.forms.gui.TooltipProviderCallback;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraph;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContextPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIProgressBarDynamic;
import org.schema.schine.graphicsengine.util.timer.LinearTimerUtil;
import org.schema.schine.input.InputState;

public class GUIReactorTabs extends GUIElement {
   private final GUIElement dependent;
   private GUIScrollablePanel scrollPane;
   private GUIAncor ancor;
   private boolean init;
   private GUIReactorTree tree;
   private LinearTimerUtil timeTool = new LinearTimerUtil(1.9F);
   private LinearTimerUtil timeToolTest = new LinearTimerUtil(0.2F);
   private GUIReactorManagerInterface reactorPanel;
   private final GUIColoredRectangleOutline rectangle;
   private GUIProgressBarDynamic progressBar;

   public GUIReactorTabs(InputState var1, GUIElement var2, GUIReactorManagerInterface var3) {
      super(var1);
      this.dependent = var2;
      this.reactorPanel = var3;
      this.ancor = new GUIAncor(var1) {
         public float getHeight() {
            return GUIReactorTabs.this.dependent.getHeight();
         }

         public float getWidth() {
            return GUIReactorTabs.this.dependent.getWidth();
         }
      };
      this.scrollPane = new GUIScrollablePanel(10.0F, 10.0F, this.dependent, var1);
      this.scrollPane.setContent(this.ancor);
      this.progressBar = new GUIProgressBarDynamic(this.getState(), 10, 10) {
         public float getValue() {
            return GUIReactorTabs.this.tree != null && GUIReactorTabs.this.tree.getTree() != null ? GUIReactorTabs.this.tree.getTree().getChamberCapacity() : 0.0F;
         }

         public String getLabelText() {
            if (GUIReactorTabs.this.tree != null && GUIReactorTabs.this.tree.getTree() != null) {
               GUIReactorTabs.this.tree.getTree();
               return this.isAnimated() && Math.abs(this.getValue() - this.getAnimValue()) > 0.01F ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORTABS_1, StringTools.formatPointZero(this.getAnimValue() * 100.0F)) : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORTABS_0, StringTools.formatPointZero(this.getValue() * 100.0F));
            } else {
               return "";
            }
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }

         public boolean isActive() {
            return GUIReactorTabs.this.isActive();
         }

         public boolean isAnimated() {
            return true;
         }
      };
      this.rectangle = new GUIColoredRectangleOutline(var1, 64.0F, 64.0F, 3, new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
      this.attach(this.scrollPane);
   }

   public void cleanUp() {
      this.scrollPane.cleanUp();
      this.ancor.cleanUp();
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      this.progressBar.setHeight((int)this.ancor.getHeight() - 8);
      this.progressBar.setWidth((int)(this.ancor.getWidth() - this.progressBar.getPos().x) - 4);
      this.drawAttached();
   }

   public void onInit() {
      if (!this.init) {
         this.progressBar.onInit();
         this.progressBar.setBackgroundColor(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
         this.progressBar.setEmptyColor(new Vector4f(0.5F, 0.5F, 1.0F, 1.0F));
         this.progressBar.setFilledColor(new Vector4f(0.0F, 1.0F, 0.0F, 1.0F), new Vector4f(1.0F, 0.0F, 0.0F, 1.0F));
         int var1 = 0;
         short[] var2;
         int var3 = (var2 = ElementKeyMap.typeList()).length;

         for(int var4 = 0; var4 < var3; ++var4) {
            ElementInformation var5;
            if ((var5 = ElementKeyMap.getInfoFast(var2[var4])).isReactorChamberGeneral()) {
               GUIReactorTabs.Icon var6;
               (var6 = new GUIReactorTabs.Icon(this.getState(), var5, var1)).onInit();
               var6.setPos((float)(2 + var1 * 66), 2.0F, 0.0F);
               this.ancor.attach(var6);
               ++var1;
            }
         }

         this.progressBar.setPos((float)(2 + var1 * 66 + 4), 4.0F, 0.0F);
         if (EngineSettings.DRAW_TOOL_TIPS.isOn()) {
            this.progressBar.setToolTip(new GUIToolTip(this.getState(), new Object() {
               public String toString() {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORTABS_5;
               }
            }, this.progressBar) {
               public void draw() {
                  if (GUIReactorTabs.this.progressBar.isActive()) {
                     super.draw();
                  }

               }
            });
         }

         this.ancor.attach(this.progressBar);
         this.init = true;
      }
   }

   public void update(Timer var1) {
      this.timeTool.update(var1);
      this.timeToolTest.update(var1);
   }

   public boolean isActive() {
      return super.isActive() && this.reactorPanel.isActive();
   }

   public float getWidth() {
      return this.dependent.getWidth();
   }

   public float getHeight() {
      return this.dependent.getHeight();
   }

   public void setTree(GUIReactorTree var1) {
      this.tree = var1;
   }

   class Icon extends GUIElement implements GUICallback, TooltipProviderCallback {
      GUIOverlay backl = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 64px ChamberTabs-4x4-gui-"), this.getState());
      GUIOverlay l = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 64px ChamberTabs-4x4-gui-"), this.getState());
      private ElementInformation info;
      private final Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
      private GUIToolTip toolTip;
      private long lastLeftClick;

      public Icon(InputState var2, final ElementInformation var3, int var4) {
         super(var2);
         this.info = var3;
         this.l.onInit();
         this.backl.onInit();
         this.backl.setSpriteSubIndex(8);
         this.l.setSpriteSubIndex(var3.reactorGeneralIconIndex);
         this.setMouseUpdateEnabled(true);
         this.setCallback(this);
         if (EngineSettings.DRAW_TOOL_TIPS.isOn()) {
            this.toolTip = new GUIToolTip(var2, new Object() {
               public String toString() {
                  return Icon.this.info.getName() + "\n" + StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORTABS_6, StringTools.formatPointZero(GUIReactorTabs.this.tree.getReactorCapacityOf(var3) * 100.0F)) + "\n" + Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORTABS_2;
               }
            }, this);
         }

      }

      public GUIContextPane createContextPane() {
         GUIContextPane var1;
         (var1 = new GUIContextPane(this.getState(), 108.0F, 25.0F)).onInit();
         GUIHorizontalButtonTablePane var2;
         (var2 = new GUIHorizontalButtonTablePane(this.getState(), 1, 1, var1)).onInit();
         var2.activeInterface = new GUIActiveInterface() {
            public boolean isActive() {
               return GUIReactorTabs.this.isActive();
            }
         };
         var2.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORTABS_3, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
            public boolean isOccluded() {
               return !GUIReactorTabs.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  Icon.this.openFullTreeOfCurrentlySelectedChamber();
               }

            }
         }, new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return true;
            }
         }).activeInterface = var2.activeInterface;
         var1.attach(var2);
         System.err.println("[CLIENT][GUI] contect pane for reactor tab " + this.info.getName());
         return var1;
      }

      public void cleanUp() {
      }

      public void draw() {
         this.updateColor();
         if (this.isInside()) {
            this.color.x = Math.min(this.color.x + 0.2F, 1.0F);
            this.color.y = Math.min(this.color.y + 0.2F, 1.0F);
            this.color.z = Math.min(this.color.z + 0.2F, 1.0F);
         }

         this.l.getSprite().setTint(this.color);
         this.backl.getSprite().setTint(this.color);
         GlUtil.glPushMatrix();
         this.transform();
         this.checkMouseInside();
         this.backl.draw();
         this.l.draw();
         if (this.isSelected()) {
            GUIReactorTabs.this.rectangle.draw();
         }

         GlUtil.glPopMatrix();
         this.l.getSprite().setTint((Vector4f)null);
         this.backl.getSprite().setTint((Vector4f)null);
      }

      public boolean isSelected() {
         return this.info == GUIReactorTabs.this.reactorPanel.getSelectedTab();
      }

      private void updateColor() {
         ReactorTree var1;
         if ((var1 = GUIReactorTabs.this.tree.getTree()) != null) {
            Iterator var3 = var1.children.iterator();

            while(var3.hasNext()) {
               ReactorElement var2;
               if ((var2 = (ReactorElement)var3.next()).getTypeGeneral() == this.info) {
                  if (var2.isAllValid()) {
                     this.color.set(0.34F, 0.85F, 0.34F, 0.85F);
                     return;
                  }

                  if (var2.isAllValidOrUnspecified()) {
                     this.color.set(0.2F + (0.3F - GUIReactorTabs.this.timeTool.getTime() * 0.3F), 0.2F + (0.3F - GUIReactorTabs.this.timeTool.getTime() * 0.3F), Math.min(1.0F, 0.5F + (0.5F - GUIReactorTabs.this.timeTool.getTime() * 0.5F)), 0.85F);
                     return;
                  }

                  this.color.set(Math.min(1.0F, 0.5F + GUIReactorTabs.this.timeTool.getTime() * 0.5F), 0.2F + GUIReactorTabs.this.timeTool.getTime() * 0.3F, 0.2F + GUIReactorTabs.this.timeTool.getTime() * 0.3F, 0.85F);
                  return;
               }
            }
         }

         this.color.set(0.5F, 0.5F, 0.5F, 0.7F);
      }

      public void onInit() {
      }

      public float getWidth() {
         return this.backl.getWidth();
      }

      public float getHeight() {
         return this.backl.getHeight();
      }

      private void openFullTreeOfCurrentlySelectedChamber() {
         PlayerOkCancelInput var1;
         (var1 = new PlayerOkCancelInput("REACTOR_OVERVIEW_TREE", this.getState(), 900, 600, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORTABS_4, this.info.getName()), "", FontStyle.big) {
            public void pressedOK() {
               this.deactivate();
            }

            public void onDeactivate() {
            }
         }).getInputPanel().setCancelButton(false);
         var1.getInputPanel().onInit();
         GUIDialogWindow var2;
         (var2 = (GUIDialogWindow)var1.getInputPanel().getBackground()).getMainContentPane().setTextBoxHeightLast(24);
         GUIGraph var3;
         (var3 = GUIReactorTabs.this.tree.getTree().getTreeGraph(this.getState(), this.info, GUIReactorTabs.this.tree.getTree(), (ReactorElement)null, (DialogInput)null, var2.getMainContentPane().getContent(0))).onInit();
         GUIScrollablePanel var4;
         (var4 = new GUIScrollablePanel(10.0F, 10.0F, var2.getMainContentPane().getContent(0), this.getState())).setScrollable(GUIScrollablePanel.SCROLLABLE_HORIZONTAL | GUIScrollablePanel.SCROLLABLE_VERTICAL);
         var4.setContent(var3);
         var2.getMainContentPane().getContent(0).attach(var4);
         var1.activate();
      }

      public void callback(GUIElement var1, MouseEvent var2) {
         if (var2.pressedLeftMouse()) {
            if (GUIReactorTabs.this.reactorPanel.getSelectedTab() == this.info) {
               if (System.currentTimeMillis() - this.lastLeftClick < 200L) {
                  this.openFullTreeOfCurrentlySelectedChamber();
               }
            } else {
               GUIReactorTabs.this.reactorPanel.setSelectedTab(this.info);
            }

            this.lastLeftClick = System.currentTimeMillis();
         } else {
            if (var2.pressedRightMouse()) {
               this.getState().getController().getInputController().setCurrentContextPane(this.createContextPane());
            }

         }
      }

      public boolean isOccluded() {
         return !GUIReactorTabs.this.isActive();
      }

      public GUIToolTip getToolTip() {
         return this.toolTip;
      }

      public void setToolTip(GUIToolTip var1) {
         this.toolTip = var1;
      }
   }
}
