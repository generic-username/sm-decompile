package org.schema.game.client.view.gui.reactor;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.server.data.FactionState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIReactorFunctionalityList extends ScrollableTableList implements Observer {
   private ManagerContainer c;
   // $FF: synthetic field
   static final boolean $assertionsDisabled = !GUIReactorFunctionalityList.class.desiredAssertionStatus();

   public GUIReactorFunctionalityList(InputState var1, ManagerContainer var2, GUIElement var3) {
      super(var1, 100.0F, 100.0F, var3);
      this.c = var2;
      var2.getPowerInterface().addObserver(this);
   }

   public void cleanUp() {
      this.c.getPowerInterface().deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORFUNCTIONALITYLIST_0, 3.0F, new Comparator() {
         public int compare(PlayerUsableInterface var1, PlayerUsableInterface var2) {
            return var1.getName().compareTo(var2.getName());
         }
      }, true);
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORFUNCTIONALITYLIST_1, 2.0F, new Comparator() {
         public int compare(PlayerUsableInterface var1, PlayerUsableInterface var2) {
            String var10000 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORFUNCTIONALITYLIST_5;
            var10000 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORFUNCTIONALITYLIST_2;
            if (var1.getReloadInterface() != null) {
               var1.getReloadInterface().getReloadStatus(var1.getUsableId());
            }

            if (var2.getReloadInterface() != null) {
               var2.getReloadInterface().getReloadStatus(var2.getUsableId());
            }

            return var1.getName().toLowerCase(Locale.ENGLISH).compareTo(var2.getName().toLowerCase(Locale.ENGLISH));
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORFUNCTIONALITYLIST_4, 128, new Comparator() {
         public int compare(PlayerUsableInterface var1, PlayerUsableInterface var2) {
            return 0;
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, PlayerUsableInterface var2) {
            return var2.getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, ControllerElement.FilterRowStyle.FULL);
      this.activeSortColumnIndex = 0;
      this.continousSortColumn = 0;
   }

   protected Collection getElementList() {
      ObjectArrayList var1 = new ObjectArrayList();
      Iterator var2 = this.c.getPlayerUsable().iterator();

      while(var2.hasNext()) {
         PlayerUsableInterface var3;
         if ((var3 = (PlayerUsableInterface)var2.next()).isPlayerUsable() && var3.getUsableId() < -9223372036854774808L && var3.getUsableId() != -9223372036854775803L) {
            var1.add(var3);
         }
      }

      boolean var10000 = $assertionsDisabled;
      return var1;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((FactionState)this.getState()).getFactionManager();
      ((GameClientState)this.getState()).getPlayer();
      Iterator var10 = var2.iterator();

      while(var10.hasNext()) {
         final PlayerUsableInterface var3 = (PlayerUsableInterface)var10.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         ScrollableTableList.GUIClippedRow var6;
         (var6 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         var4.setTextSimple(var3.getName());
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         var5.setTextSimple(new Object() {
            public String toString() {
               return var3.getReloadInterface() != null ? var3.getReloadInterface().getReloadStatus(var3.getUsableId()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORFUNCTIONALITYLIST_3;
            }
         });
         GUITextButton var8 = new GUITextButton(this.getState(), 120, this.columnsHeight, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORFUNCTIONALITYLIST_6, new GUICallback() {
            public boolean isOccluded() {
               return !GUIReactorFunctionalityList.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse() || var2.pressedRightMouse() || var2.pressedMiddleMouse()) {
                  GUIReactorFunctionalityList.this.c.triggeredManualMouseEvent(((GameClientState)GUIReactorFunctionalityList.this.getState()).getPlayer().getId(), var3, var2);
               }

            }
         });
         GUIAncor var9;
         (var9 = new GUIAncor(this.getState(), 50.0F, (float)this.columnsHeight)).attach(var8);
         var4.getPos().y = 4.0F;
         var5.getPos().y = 4.0F;
         GUIReactorFunctionalityList.PlayerUsableInterfaceRow var11;
         (var11 = new GUIReactorFunctionalityList.PlayerUsableInterfaceRow(this.getState(), var3, new GUIElement[]{var6, var7, var9})).expanded = null;
         var11.onInit();
         var1.addWithoutUpdate(var11);
      }

      var1.updateDim();
   }

   class PlayerUsableInterfaceRow extends ScrollableTableList.Row {
      public PlayerUsableInterfaceRow(InputState var2, PlayerUsableInterface var3, GUIElement... var4) {
         super(var2, var3, var4);
      }
   }
}
