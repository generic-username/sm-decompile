package org.schema.game.client.view.gui.reactor;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import org.lwjgl.input.Keyboard;
import org.schema.common.util.CompareTools;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.server.data.FactionState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIReactorPriorityList extends ScrollableTableList implements Observer {
   private ManagerContainer c;
   private boolean blockDrag;
   // $FF: synthetic field
   static final boolean $assertionsDisabled = !GUIReactorPriorityList.class.desiredAssertionStatus();

   public GUIReactorPriorityList(InputState var1, ManagerContainer var2, GUIElement var3) {
      super(var1, 100.0F, 100.0F, var3);
      this.c = var2;
      var2.getPowerInterface().addObserver(this);
   }

   public void cleanUp() {
      this.c.getPowerInterface().deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPRIORITYLIST_0, 45, new Comparator() {
         public int compare(PowerConsumer.PowerConsumerCategory var1, PowerConsumer.PowerConsumerCategory var2) {
            int var3 = GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().getPriority(var1);
            int var4 = GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().getPriority(var2);
            return var3 - var4;
         }
      }, true);
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPRIORITYLIST_1, 180, new Comparator() {
         public int compare(PowerConsumer.PowerConsumerCategory var1, PowerConsumer.PowerConsumerCategory var2) {
            return var1.getName().toLowerCase(Locale.ENGLISH).compareTo(var2.getName().toLowerCase(Locale.ENGLISH));
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPRIORITYLIST_2, 68, new Comparator() {
         public int compare(PowerConsumer.PowerConsumerCategory var1, PowerConsumer.PowerConsumerCategory var2) {
            return CompareTools.compare(GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().getPercent(var1), GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().getPercent(var2));
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPRIORITYLIST_3, 60, new Comparator() {
         public int compare(PowerConsumer.PowerConsumerCategory var1, PowerConsumer.PowerConsumerCategory var2) {
            return CompareTools.compare(GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().getAmount(var1), GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().getAmount(var2));
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPRIORITYLIST_4, 1.0F, new Comparator() {
         public int compare(PowerConsumer.PowerConsumerCategory var1, PowerConsumer.PowerConsumerCategory var2) {
            return CompareTools.compare(GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().getConsumption(var1), GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().getConsumption(var2));
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPRIORITYLIST_8, 1.0F, new Comparator() {
         public int compare(PowerConsumer.PowerConsumerCategory var1, PowerConsumer.PowerConsumerCategory var2) {
            return CompareTools.compare(GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().getConsumptionPercent(var1), GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().getConsumptionPercent(var2));
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPRIORITYLIST_5, 60, new Comparator() {
         public int compare(PowerConsumer.PowerConsumerCategory var1, PowerConsumer.PowerConsumerCategory var2) {
            return 0;
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, PowerConsumer.PowerConsumerCategory var2) {
            return var2.getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, ControllerElement.FilterRowStyle.FULL);
      this.activeSortColumnIndex = 0;
      this.continousSortColumn = 0;
   }

   protected Collection getElementList() {
      ObjectArrayList var1;
      (var1 = new ObjectArrayList()).addAll(this.c.getPowerInterface().getPowerConsumerPriorityQueue().getQueue());
      boolean var10000 = $assertionsDisabled;
      return var1;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((FactionState)this.getState()).getFactionManager();
      ((GameClientState)this.getState()).getPlayer();
      Iterator var14 = var2.iterator();

      while(var14.hasNext()) {
         final PowerConsumer.PowerConsumerCategory var3 = (PowerConsumer.PowerConsumerCategory)var14.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var8 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var9 = new GUITextOverlayTable(10, 10, this.getState());
         ScrollableTableList.GUIClippedRow var10;
         (var10 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         var4.setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().getPriority(var3));
            }
         });
         ScrollableTableList.GUIClippedRow var16;
         (var16 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         var5.setTextSimple(var3.getName());
         var6.setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().getAmount(var3));
            }
         });
         ScrollableTableList.GUIClippedRow var11;
         (var11 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var7);
         var7.setTextSimple(new Object() {
            public String toString() {
               return GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().getConsumption(var3) <= 0.0D ? "-" : StringTools.formatPointZero(GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().getPercent(var3) * 100.0D) + "%";
            }
         });
         var8.setTextSimple(new Object() {
            public String toString() {
               return StringTools.formatSmallAndBig(GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().getConsumption(var3));
            }
         });
         var9.setTextSimple(new Object() {
            public String toString() {
               return StringTools.formatPointZero(GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().getConsumptionPercent(var3) * 100.0D) + "%";
            }
         });
         GUITextButton var17 = new GUITextButton(this.getState(), 25, this.columnsHeight, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPRIORITYLIST_6, new GUICallback() {
            public boolean isOccluded() {
               return !GUIReactorPriorityList.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  int var3x = -1;
                  if (Keyboard.isKeyDown(42) || Keyboard.isKeyDown(54)) {
                     var3x = -100000;
                  }

                  GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().move(GUIReactorPriorityList.this.c.getPowerInterface(), var3, var3x);
               }

            }
         }) {
            public void draw() {
               super.draw();
               if (this.isInside()) {
                  GUIReactorPriorityList.this.blockDrag = true;
               }

            }
         };
         GUITextButton var12 = new GUITextButton(this.getState(), 25, this.columnsHeight, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPRIORITYLIST_7, new GUICallback() {
            public boolean isOccluded() {
               return !GUIReactorPriorityList.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  int var3x = 1;
                  if (Keyboard.isKeyDown(42) || Keyboard.isKeyDown(54)) {
                     var3x = Integer.MAX_VALUE;
                  }

                  GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().move(GUIReactorPriorityList.this.c.getPowerInterface(), var3, var3x);
               }

            }
         }) {
            public void draw() {
               super.draw();
               if (this.isInside()) {
                  GUIReactorPriorityList.this.blockDrag = true;
               }

            }
         };
         GUIAncor var13;
         (var13 = new GUIAncor(this.getState(), 50.0F, (float)this.columnsHeight)).attach(var17);
         var12.getPos().x = var17.getWidth();
         var13.attach(var12);
         var5.getPos().y = 4.0F;
         var6.getPos().y = 4.0F;
         var8.getPos().y = 4.0F;
         var9.getPos().y = 4.0F;
         GUIReactorPriorityList.PowerConsumerCategoryRow var15;
         (var15 = new GUIReactorPriorityList.PowerConsumerCategoryRow(this.getState(), var3, new GUIElement[]{var10, var16, var11, var6, var8, var9, var13})).expanded = null;
         var15.onInit();
         var1.addWithoutUpdate(var15);
      }

      var1.updateDim();
   }

   public void draw() {
      this.blockDrag = false;
      super.draw();
   }

   class PowerConsumerCategoryRow extends ScrollableTableList.Row {
      public PowerConsumerCategoryRow(InputState var2, PowerConsumer.PowerConsumerCategory var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.draggable = true;
      }

      public void onDrop(ScrollableTableList.Row var1) {
         GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().moveAbs(GUIReactorPriorityList.this.c.getPowerInterface(), (PowerConsumer.PowerConsumerCategory)var1.f, GUIReactorPriorityList.this.c.getPowerInterface().getPowerConsumerPriorityQueue().getPriority((PowerConsumer.PowerConsumerCategory)this.f));
      }

      public boolean isDragStartOk() {
         return !GUIReactorPriorityList.this.blockDrag;
      }
   }
}
