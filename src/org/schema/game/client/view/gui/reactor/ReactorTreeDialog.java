package org.schema.game.client.view.gui.reactor;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public class ReactorTreeDialog extends DialogInput {
   private final GUIReactorPanel p;

   public ReactorTreeDialog(GameClientState var1, ManagedSegmentController var2) {
      super(var1);
      this.p = new GUIReactorPanel(var1, var2, this);
      this.p.onInit();

      assert var2.getManagerContainer().getPowerInterface().getReactorSet().getTrees().size() > 0 : "This Dialog should not be accessible";

   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public GUIElement getInputPanel() {
      return this.p;
   }

   public void onDeactivate() {
      this.p.cleanUp();
   }

   public void update(Timer var1) {
      super.update(var1);
      this.p.update(var1);
   }
}
