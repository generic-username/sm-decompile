package org.schema.game.client.view.gui.reactor;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import org.schema.common.util.CompareTools;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.server.data.FactionState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIReactorPowerConsumerList extends ScrollableTableList implements Observer {
   private ManagerContainer c;
   // $FF: synthetic field
   static final boolean $assertionsDisabled = !GUIReactorPowerConsumerList.class.desiredAssertionStatus();

   public GUIReactorPowerConsumerList(InputState var1, ManagerContainer var2, GUIElement var3) {
      super(var1, 100.0F, 100.0F, var3);
      this.c = var2;
      var2.getPowerInterface().addObserver(this);
   }

   public void cleanUp() {
      this.c.getPowerInterface().deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPOWERCONSUMERLIST_0, 45, new Comparator() {
         public int compare(PowerConsumer var1, PowerConsumer var2) {
            int var3 = GUIReactorPowerConsumerList.this.c.getPowerInterface().getPowerConsumerList().indexOf(var1);
            int var4 = GUIReactorPowerConsumerList.this.c.getPowerInterface().getPowerConsumerList().indexOf(var2);
            return var3 - var4;
         }
      }, true);
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPOWERCONSUMERLIST_1, 350, new Comparator() {
         public int compare(PowerConsumer var1, PowerConsumer var2) {
            return var1.getName().toLowerCase(Locale.ENGLISH).compareTo(var2.getName().toLowerCase(Locale.ENGLISH));
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPOWERCONSUMERLIST_2, 68, new Comparator() {
         public int compare(PowerConsumer var1, PowerConsumer var2) {
            return CompareTools.compare(var1.isPowerConsumerActive(), var2.isPowerConsumerActive());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPOWERCONSUMERLIST_3, 66, new Comparator() {
         public int compare(PowerConsumer var1, PowerConsumer var2) {
            return CompareTools.compare(var1.getPowered(), var2.getPowered());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPOWERCONSUMERLIST_4, 1.0F, new Comparator() {
         public int compare(PowerConsumer var1, PowerConsumer var2) {
            return CompareTools.compare(var1.getPowerConsumedPerSecondCharging(), var2.getPowerConsumedPerSecondCharging());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPOWERCONSUMERLIST_5, 1.0F, new Comparator() {
         public int compare(PowerConsumer var1, PowerConsumer var2) {
            return CompareTools.compare(var1.getPowerConsumedPerSecondResting(), var2.getPowerConsumedPerSecondResting());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPOWERCONSUMERLIST_6, 80, new Comparator() {
         public int compare(PowerConsumer var1, PowerConsumer var2) {
            long var3 = System.currentTimeMillis();
            return CompareTools.compare(var1.isPowerCharging(var3), var2.isPowerCharging(var3));
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPOWERCONSUMERLIST_7, 1.0F, new Comparator() {
         public int compare(PowerConsumer var1, PowerConsumer var2) {
            long var3 = System.currentTimeMillis();
            return CompareTools.compare(!var1.isPowerConsumerActive() ? 0.0D : (var1.isPowerCharging(var3) ? var1.getPowerConsumedPerSecondCharging() : var1.getPowerConsumedPerSecondResting()), !var2.isPowerConsumerActive() ? 0.0D : (var2.isPowerCharging(var3) ? var2.getPowerConsumedPerSecondCharging() : var2.getPowerConsumedPerSecondResting()));
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, PowerConsumer var2) {
            return var2.getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, ControllerElement.FilterRowStyle.FULL);
      this.activeSortColumnIndex = 0;
      this.continousSortColumn = 0;
   }

   protected Collection getElementList() {
      ObjectArrayList var1;
      (var1 = new ObjectArrayList()).addAll(this.c.getPowerInterface().getPowerConsumerList());
      boolean var10000 = $assertionsDisabled;
      return var1;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((FactionState)this.getState()).getFactionManager();
      ((GameClientState)this.getState()).getPlayer();
      final int var3 = 0;

      for(Iterator var15 = var2.iterator(); var15.hasNext(); ++var3) {
         final PowerConsumer var4 = (PowerConsumer)var15.next();
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var8 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var9 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var10 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var11 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var12 = new GUITextOverlayTable(10, 10, this.getState());
         ScrollableTableList.GUIClippedRow var14;
         (var14 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         var5.setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var3);
            }
         });
         ScrollableTableList.GUIClippedRow var17;
         (var17 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var6);
         var6.setTextSimple(var4.getName());
         var7.setTextSimple(new Object() {
            public String toString() {
               return var4.isPowerConsumerActive() ? "ON" : "OFF";
            }
         });
         ScrollableTableList.GUIClippedRow var13;
         (var13 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var8);
         var8.setTextSimple(new Object() {
            public String toString() {
               return StringTools.formatPointZero((double)var4.getPowered() * 100.0D) + "%";
            }
         });
         var9.setTextSimple(new Object() {
            public String toString() {
               return StringTools.formatPointZeroZero(var4.getPowerConsumedPerSecondCharging());
            }
         });
         var10.setTextSimple(new Object() {
            public String toString() {
               return StringTools.formatPointZeroZero(var4.getPowerConsumedPerSecondResting());
            }
         });
         var11.setTextSimple(new Object() {
            public String toString() {
               return var4.isPowerCharging(System.currentTimeMillis()) ? "CHARGE" : "REST";
            }
         });
         var12.setTextSimple(new Object() {
            public String toString() {
               return !var4.isPowerConsumerActive() ? "-" : StringTools.formatPointZeroZero(var4.isPowerCharging(System.currentTimeMillis()) ? var4.getPowerConsumedPerSecondCharging() : var4.getPowerConsumedPerSecondResting());
            }
         });
         var6.getPos().y = 4.0F;
         var7.getPos().y = 4.0F;
         var9.getPos().y = 4.0F;
         var13.getPos().y = 4.0F;
         var10.getPos().y = 4.0F;
         var11.getPos().y = 4.0F;
         var12.getPos().y = 4.0F;
         GUIReactorPowerConsumerList.PowerConsumerRow var16;
         (var16 = new GUIReactorPowerConsumerList.PowerConsumerRow(this.getState(), var4, new GUIElement[]{var14, var17, var7, var13, var9, var10, var11, var12})).expanded = null;
         var16.onInit();
         var1.addWithoutUpdate(var16);
      }

      var1.updateDim();
   }

   public void draw() {
      super.draw();
   }

   class PowerConsumerRow extends ScrollableTableList.Row {
      public PowerConsumerRow(InputState var2, PowerConsumer var3, GUIElement... var4) {
         super(var2, var3, var4);
      }
   }
}
