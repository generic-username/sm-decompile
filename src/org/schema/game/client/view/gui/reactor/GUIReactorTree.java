package org.schema.game.client.view.gui.reactor;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.Short2FloatOpenHashMap;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorSet;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorTree;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraph;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;

public class GUIReactorTree extends GUIElement implements Observer {
   private final ManagedSegmentController m;
   private ReactorTree tree;
   private int maxX;
   private int maxY;
   private GUIReactorManagerInterface manI;
   private boolean builtOnce;
   private GUIActiveInterface actIface;
   public static long selected = Long.MIN_VALUE;
   private static int lastSegCon;
   private static Long2ObjectOpenHashMap lastScrollX = new Long2ObjectOpenHashMap();
   private static Long2ObjectOpenHashMap lastScrollY = new Long2ObjectOpenHashMap();
   private GUIGraph reactorGraph;
   private GUIScrollablePanel p;
   private GUIElement dependent;

   public GUIReactorTree(GameClientState var1, GUIActiveInterface var2, ManagedSegmentController var3, GUIReactorManagerInterface var4, GUIElement var5, ReactorTree var6) {
      super(var1);
      this.actIface = var2;
      this.manI = var4;
      this.m = var3;
      this.tree = var6;
      this.dependent = var5;
      var3.getManagerContainer().getPowerInterface().addObserver(this);
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public void cleanUp() {
      Iterator var1 = this.getChilds().iterator();

      while(var1.hasNext()) {
         ((AbstractSceneNode)var1.next()).cleanUp();
      }

      this.m.getManagerContainer().getPowerInterface().deleteObserver(this);
   }

   public boolean isActive() {
      return super.isActive() && (this.actIface == null || this.actIface.isActive());
   }

   public void draw() {
      super.drawAttached();
      ((Short2FloatOpenHashMap)lastScrollX.get(this.tree.getId())).put(this.manI.getSelectedTab().id, this.p.getScrollX());
      ((Short2FloatOpenHashMap)lastScrollY.get(this.tree.getId())).put(this.manI.getSelectedTab().id, this.p.getScrollY());
   }

   public PowerInterface getPowerInterface() {
      return this.m.getManagerContainer().getPowerInterface();
   }

   public void onInit() {
      if (!this.builtOnce) {
         this.build();
      }

   }

   private void build() {
      if (this.reactorGraph != null) {
         this.p.cleanUp();
         this.reactorGraph.cleanUp();
      }

      this.reactorGraph = this.tree.getTreeGraphCurrent(this.manI.getSelectedTab(), this);
      if (lastSegCon != this.tree.pw.getSegmentController().getId()) {
         lastScrollX.clear();
         lastScrollY.clear();
         lastSegCon = this.tree.pw.getSegmentController().getId();
      }

      if (!lastScrollX.containsKey(this.tree.getId())) {
         lastScrollX.put(this.tree.getId(), new Short2FloatOpenHashMap());
         lastScrollY.put(this.tree.getId(), new Short2FloatOpenHashMap());
      }

      this.reactorGraph.onInit();
      this.p = new GUIScrollablePanel(10.0F, 10.0F, this.dependent, this.getState());
      this.p.setContent(this.reactorGraph);
      this.p.setScrollable(GUIScrollablePanel.SCROLLABLE_HORIZONTAL | GUIScrollablePanel.SCROLLABLE_VERTICAL);
      this.p.onInit();
      this.detachAll();
      if (this.reactorGraph.isEmptyGraph()) {
         GUITextOverlay var1;
         (var1 = new GUITextOverlay(10, 10, FontLibrary.FontSize.BIG.getFont(), this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORTREE_0);
         var1.onInit();
         GUIAncor var2 = new GUIAncor(this.getState()) {
            public void draw() {
               this.setWidth(GUIReactorTree.this.dependent.getWidth());
               this.setHeight(GUIReactorTree.this.dependent.getHeight());
               super.draw();
            }
         };
         var1.setPos(4.0F, 4.0F, 0.0F);
         var2.attach(var1);
         var1.autoWrapOn = var2;
         this.p.setContent(var2);
      }

      this.p.scrollHorizontal(((Short2FloatOpenHashMap)lastScrollX.get(this.tree.getId())).get(this.manI.getSelectedTab().id));
      this.p.scrollVertical(((Short2FloatOpenHashMap)lastScrollY.get(this.tree.getId())).get(this.manI.getSelectedTab().id));
      this.attach(this.p);
      this.builtOnce = true;
   }

   public float getWidth() {
      return (float)((this.maxX + 1) * GUIReactorTreeNode.NODE_WIDTH);
   }

   public float getHeight() {
      return (float)((this.maxY + 1) * GUIReactorTreeNode.NODE_HEIGHT);
   }

   public void update(Observable var1, Object var2) {
      if (var2 != null) {
         boolean var4 = false;
         Iterator var6 = ((ReactorSet)var2).getTrees().iterator();

         while(true) {
            ReactorTree var3;
            do {
               if (!var6.hasNext()) {
                  if (!var4) {
                     this.manI.onTreeNotFound(this);
                  }

                  return;
               }
            } while((var3 = (ReactorTree)var6.next()).getId() != this.tree.getId());

            Iterator var5 = this.getChilds().iterator();

            while(var5.hasNext()) {
               ((AbstractSceneNode)var5.next()).cleanUp();
            }

            this.detachAll();
            this.tree = var3;
            this.build();
            var4 = true;
         }
      }
   }

   public ReactorTree getTree() {
      return this.tree;
   }

   public void setSelected(GUIReactorTreeNode var1) {
      if (var1 != null && var1.element != null) {
         selected = var1.element.getId();
      } else {
         selected = Long.MIN_VALUE;
      }
   }

   public float getReactorCapacityOf(ElementInformation var1) {
      return this.tree.getReactorCapacityOf(var1);
   }
}
