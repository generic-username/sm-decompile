package org.schema.game.client.view.gui;

import org.schema.game.client.view.gui.weapon.WeaponSlotOverlayElement;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.TooltipProvider;

public interface HotbarInterface extends TooltipProvider {
   void orientate(int var1);

   float getHeight();

   void activateDragging(boolean var1);

   void draw();

   void drawDragging(WeaponSlotOverlayElement var1);

   void update(Timer var1);

   void onInit();

   boolean isInside();
}
