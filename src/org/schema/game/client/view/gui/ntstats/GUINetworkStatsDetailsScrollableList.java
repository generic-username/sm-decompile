package org.schema.game.client.view.gui.ntstats;

import it.unimi.dsi.fastutil.ints.Int2LongOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;
import org.schema.schine.network.DataStatsEntry;
import org.schema.schine.network.objects.NetworkObject;

public class GUINetworkStatsDetailsScrollableList extends ScrollableTableList implements DrawerObserver {
   private List list = new ObjectArrayList();

   public GUINetworkStatsDetailsScrollableList(InputState var1, GUIElement var2, DataStatsEntry var3) {
      super(var1, 100.0F, 100.0F, var2);
      Iterator var4 = var3.entry.entrySet().iterator();

      while(var4.hasNext()) {
         Entry var6 = (Entry)var4.next();
         this.list.add(new StatsDetailData((Class)var6.getKey(), (Int2LongOpenHashMap)var6.getValue()));
      }

      Comparator var5 = new Comparator() {
         public int compare(StatsDetailData var1, StatsDetailData var2) {
            if (var1.volume > var2.volume) {
               return -1;
            } else {
               return var1.volume < var2.volume ? 1 : 0;
            }
         }
      };
      Collections.sort(this.list, var5);
   }

   public void cleanUp() {
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn("Class", 4.0F, new Comparator() {
         public int compare(StatsDetailData var1, StatsDetailData var2) {
            return var1.ntObjClass.getSimpleName().compareTo(var2.ntObjClass.getSimpleName());
         }
      });
      this.addColumn("Volume", 1.0F, new Comparator() {
         public int compare(StatsDetailData var1, StatsDetailData var2) {
            if (var1.volume > var2.volume) {
               return 1;
            } else {
               return var1.volume < var2.volume ? -1 : 0;
            }
         }
      });
      this.addFixedWidthColumn("Fields", 80, new Comparator() {
         public int compare(StatsDetailData var1, StatsDetailData var2) {
            return var1.data.size() - var2.data.size();
         }
      });
   }

   protected Collection getElementList() {
      return this.list;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getPlayer();
      Iterator var10 = var2.iterator();

      while(var10.hasNext()) {
         StatsDetailData var3 = (StatsDetailData)var10.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         var4.setTextSimple(var3.ntObjClass.getSimpleName());
         var6.setTextSimple(StringTools.readableFileSize(var3.volume));
         var5.setTextSimple(var3.data.size());
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         ScrollableTableList.GUIClippedRow var8;
         (var8 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         ScrollableTableList.GUIClippedRow var9;
         (var9 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var6);
         var4.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         GUINetworkStatsDetailsScrollableList.StatRow var12;
         (var12 = new GUINetworkStatsDetailsScrollableList.StatRow(this.getState(), var3, new GUIElement[]{var7, var9, var8})).expanded = new GUIElementList(this.getState());
         ObjectArrayList var13 = new ObjectArrayList();
         String[] var14 = NetworkObject.getFieldNames(var3.ntObjClass);
         Iterator var11 = var3.data.int2LongEntrySet().iterator();

         GUINetworkStatsDetailsScrollableList.FieldEntry var16;
         while(var11.hasNext()) {
            it.unimi.dsi.fastutil.ints.Int2LongMap.Entry var15 = (it.unimi.dsi.fastutil.ints.Int2LongMap.Entry)var11.next();
            var16 = new GUINetworkStatsDetailsScrollableList.FieldEntry(var14[var15.getIntKey()], var15.getLongValue());
            var13.add(var16);
         }

         Collections.sort(var13);
         var11 = var13.iterator();

         while(var11.hasNext()) {
            if ((var16 = (GUINetworkStatsDetailsScrollableList.FieldEntry)var11.next()).volume > 0L) {
               var12.expanded.addWithoutUpdate(var16.toGUIListElement());
            }
         }

         var12.expanded.updateDim();
         var12.onInit();
         var1.addWithoutUpdate(var12);
      }

      var1.updateDim();
   }

   protected boolean isFiltered(StatsDetailData var1) {
      return var1.volume <= 0L || super.isFiltered(var1);
   }

   public void update(DrawerObservable var1, Object var2, Object var3) {
      this.flagDirty();
   }

   class FieldEntry implements Comparable, Comparator {
      public String name;
      public long volume;

      public FieldEntry(String var2, long var3) {
         this.name = var2;
         this.volume = var3;
      }

      public GUIListElement toGUIListElement() {
         GUIAncor var1 = new GUIAncor(GUINetworkStatsDetailsScrollableList.this.getState(), 200.0F, 25.0F);
         GUITextOverlayTable var2;
         (var2 = new GUITextOverlayTable(10, 10, GUINetworkStatsDetailsScrollableList.this.getState())).setTextSimple(this.name);
         GUITextOverlayTable var3;
         (var3 = new GUITextOverlayTable(10, 10, GUINetworkStatsDetailsScrollableList.this.getState())).setTextSimple(StringTools.readableFileSize(this.volume));
         var2.setPos(4.0F, 5.0F, 0.0F);
         var3.setPos(330.0F, 5.0F, 0.0F);
         var1.attach(var2);
         var1.attach(var3);
         return new GUIListElement(var1, var1, GUINetworkStatsDetailsScrollableList.this.getState());
      }

      public int compareTo(GUINetworkStatsDetailsScrollableList.FieldEntry var1) {
         return this.compare(this, var1);
      }

      public int compare(GUINetworkStatsDetailsScrollableList.FieldEntry var1, GUINetworkStatsDetailsScrollableList.FieldEntry var2) {
         if (var1.volume > var2.volume) {
            return -1;
         } else {
            return var1.volume < var2.volume ? 1 : 0;
         }
      }
   }

   class StatRow extends ScrollableTableList.Row {
      public StatRow(InputState var2, StatsDetailData var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.f = var3;
         this.highlightSelect = true;
      }
   }
}
