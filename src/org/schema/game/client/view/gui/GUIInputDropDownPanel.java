package org.schema.game.client.view.gui;

import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.input.InputState;

public class GUIInputDropDownPanel extends GUIInputPanel {
   private GUIDropDownList dropDown;
   public int yPos = 54;

   public GUIInputDropDownPanel(String var1, InputState var2, int var3, int var4, GUICallback var5, Object var6, Object var7, GUIDropDownList var8) {
      super(var1, var2, var3, var4, var5, var6, var7);

      assert var8 != null;

      this.dropDown = var8;
   }

   public void updateList(GUIDropDownList var1) {
      this.dropDown = var1;
      this.dropDown.setPos(3.0F, (float)this.yPos, 0.0F);
      this.dropDown.dependentWidthOffset = -5;
      this.dropDown.getList().updateDim();
   }

   public void onInit() {
      super.onInit();
      this.dropDown.setPos(3.0F, (float)this.yPos, 0.0F);
      this.dropDown.dependentWidthOffset = -5;
      if (isNewHud()) {
         if (!((GUIDialogWindow)this.getBackground()).getMainContentPane().getContent(0).getChilds().contains(this.dropDown)) {
            ((GUIDialogWindow)this.getBackground()).getMainContentPane().getContent(0).attach(this.dropDown);
            return;
         }
      } else {
         this.getBackground().attach(this.dropDown);
      }

   }

   public GUIDropDownList getDropDown() {
      return this.dropDown;
   }

   public void setDropDown(GUIDropDownList var1) {
      this.dropDown = var1;
   }
}
