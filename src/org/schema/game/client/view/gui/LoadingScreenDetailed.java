package org.schema.game.client.view.gui;

import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.glu.GLU;
import org.schema.common.util.StringTools;
import org.schema.common.util.data.DataUtil;
import org.schema.game.client.controller.GameMainMenuController;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.LoadingScreen;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Light;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.StateInterface;

public class LoadingScreenDetailed extends LoadingScreen {
   private final GUITextOverlay loadInfo;
   private final GUITextOverlay loadMessage;
   private boolean init;
   private Light light;
   private float adviceStartTime;
   private float time;
   private GameMainMenuController mainMenu;
   private List tips;

   private void initLoadingTips() {
      this.tips = new ArrayList(Arrays.asList(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_4, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_5, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_7, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_8, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_9, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_22, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_11, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_12, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_53, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_15, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_3, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_17, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_18, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_19, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_21, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_13, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_16, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_25, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_23, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_27, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_24, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_28, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_30, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_31, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_32, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_33, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_34, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_35, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_36, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_37, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_38, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_39, Keyboard.getKeyName(KeyboardMappings.RADIAL_MENU.getMapping())), StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_40, Keyboard.getKeyName(KeyboardMappings.BUILD_MODE_FIX_CAM.getMapping())), StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_41, Keyboard.getKeyName(KeyboardMappings.BUILD_MODE_FIX_CAM.getMapping())), StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_42, Keyboard.getKeyName(KeyboardMappings.BUILD_MODE_FIX_CAM.getMapping())), StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_43, Keyboard.getKeyName(KeyboardMappings.BUILD_MODE_FIX_CAM.getMapping())), StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_44, Keyboard.getKeyName(KeyboardMappings.BUILD_MODE_FIX_CAM.getMapping())), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_29, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_46, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_6, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_47, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_45, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_51, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_52, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_48, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_10, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_49, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_14, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_57, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_58, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_59, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_60, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_50, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_62, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_63, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_26, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_65, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_66, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_67, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_68, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_69, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_70, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_71));
   }

   public String getRandomTip() {
      int var1 = (new Random()).nextInt(this.tips.size());
      return (String)this.tips.get(var1);
   }

   public LoadingScreenDetailed() {
      this.initLoadingTips();
      System.err.println("[INIT] Creating Loading Screen");
      this.loadInfo = new GUITextOverlay(300, 300, FontLibrary.getBlenderProHeavy30(), (InputState)null);
      System.err.println("[INIT] Creating Loading Message");
      this.loadMessage = new GUITextOverlay(300, 300, FontLibrary.getBlenderProHeavy20(), (InputState)null);
      System.err.println("[INIT] Creating Light");
      this.light = new Light(0);
   }

   public void drawLoadingScreen() {
      if (!this.init) {
         this.onInit();
      }

      this.draw2d();
      this.draw3d();
   }

   private void draw3d() {
      Mesh var1 = Controller.getResLoader().getMesh("3dLogo");
      GUIElement.enableOrthogonal3d();
      GlUtil.glPushMatrix();
      GlUtil.translateModelview((float)(GLFrame.getWidth() - 170), (float)(GLFrame.getHeight() - 140), 0.0F);
      GlUtil.scaleModelview(40.0F, -40.0F, 40.0F);
      GlUtil.glColor4f(0.3F, 0.3F, 0.3F, 1.0F);
      GL11.glColorMask(true, true, true, true);
      GlUtil.glDepthMask(true);
      GL11.glShadeModel(7425);
      GlUtil.glEnable(2929);
      GlUtil.glEnable(2977);
      GlUtil.glDisable(2896);
      GlUtil.glDisable(3553);
      GlUtil.glDisable(32879);
      GlUtil.glDisable(2884);
      GlUtil.glDisable(2903);
      GL20.glUseProgram(0);
      FloatBuffer var2;
      (var2 = GlUtil.getDynamicByteBuffer(16, 0).asFloatBuffer()).put(0.6F);
      var2.put(0.6F);
      var2.put(0.6F);
      var2.put(0.9F);
      var2.rewind();
      FloatBuffer var3;
      (var3 = GlUtil.getDynamicByteBuffer(16, 1).asFloatBuffer()).put(0.9F);
      var3.put(0.9F);
      var3.put(0.9F);
      var3.put(0.9F);
      var3.rewind();
      IntBuffer var4;
      (var4 = GlUtil.getDynamicByteBuffer(16, 2).asIntBuffer()).put(66);
      var4.put(66);
      var4.put(66);
      var4.put(66);
      var4.rewind();
      GL11.glMaterial(1032, 4609, var2);
      GL11.glMaterial(1032, 4610, var3);
      GL11.glMaterial(1032, 5633, var4);
      this.light.setAmbience(new Vector4f(0.38F, 0.38F, 0.38F, 1.0F));
      this.light.setDiffuse(new Vector4f(0.68F, 0.68F, 0.68F, 1.0F));
      this.light.setSpecular(new Vector4f(0.99F, 0.99F, 0.99F, 1.0F));
      this.light.setPos(0.0F, 0.0F, 100.0F);
      this.light.draw();
      ((Mesh)var1.getChilds().get(1)).setRot(0.0F, this.time, 0.0F);
      GlUtil.glPushMatrix();
      ((Mesh)var1.getChilds().get(1)).transform();
      ((Mesh)var1.getChilds().get(1)).drawVBO();
      GlUtil.glPopMatrix();
      GlUtil.glPopMatrix();
      GUIElement.disableOrthogonal();
      GlUtil.glEnable(2896);
      GlUtil.glDisable(2977);
      GlUtil.glEnable(2929);
      GlUtil.glBindBuffer(34962, 0);
   }

   private void draw2d() {
      GlUtil.glMatrixMode(5889);
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      GLU.gluOrtho2D(0.0F, (float)GLFrame.getWidth(), (float)GLFrame.getHeight(), 0.0F);
      GlUtil.glMatrixMode(5888);
      GlUtil.glLoadIdentity();
      GlUtil.glPushMatrix();
      Sprite var1;
      (var1 = Controller.getResLoader().getSprite("schine")).setPos(30.0F, (float)(GLFrame.getHeight() - var1.getHeight()), 0.0F);
      var1.draw();
      (var1 = Controller.getResLoader().getSprite("SM_logo_white_nostar")).setPos((float)(GLFrame.getWidth() - var1.getWidth() + 106), (float)(GLFrame.getHeight() - var1.getHeight() + 68), 0.0F);
      var1.draw();
      GlUtil.glPopMatrix();
      GlUtil.glDisable(2896);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glDisable(3042);
      GlUtil.glDisable(2929);
      GlUtil.glDisable(3553);
      GlUtil.glEnable(2903);
      GlUtil.glColor4f(0.9F, 0.9F, 0.9F, 0.8F);
      GlUtil.glColor4f(0.2F, 0.9F, 0.2F, 0.9F);
      GUIElement.enableOrthogonal();
      this.loadInfo.setPos(10.0F, 10.0F, 0.0F);
      this.loadMessage.setPos(100.0F, 200.0F, 0.0F);
      if (this.loadMessage.getText().isEmpty() || this.time - this.adviceStartTime > 5000.0F) {
         this.adviceStartTime = this.time;
         this.loadMessage.setText(new ArrayList());
         this.loadMessage.getText().add(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_72);
         String[] var3 = this.getRandomTip().split("\n");

         for(int var2 = 0; var2 < var3.length; ++var2) {
            this.loadMessage.getText().add(var3[var2]);
         }
      }

      this.loadInfo.getText().clear();
      this.loadInfo.getText().add(Controller.getResLoader().getLoadString());
      String var4;
      if ((var4 = LoadingScreen.serverMessage) != null) {
         this.loadInfo.getText().add(var4);
      }

      this.loadInfo.draw();
      this.loadMessage.draw();
      GUIElement.disableOrthogonal();
      GlUtil.glDisable(2903);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glEnable(2929);
      GlUtil.glMatrixMode(5889);
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5888);
   }

   private void onInit() {
      this.loadInfo.onInit();
      this.loadMessage.onInit();
      this.init = true;
   }

   public void loadInitialResources() throws IOException, ResourceException {
      Controller.getResLoader().getImageLoader().loadImage(DataUtil.dataPath + "./image-resource/schine.png", "schine");
      Controller.getResLoader().getImageLoader().loadImage(DataUtil.dataPath + "./image-resource/loadingscreen-background.png", "loadingscreen-background");
      Controller.getResLoader().getImageLoader().loadImage(DataUtil.dataPath + "./image-resource/SM_logo_white_nostar.png", "SM_logo_white_nostar");
      Controller.getResLoader().loadModelDirectly("3dLogo", "./models/3Dlogo/", "3Dlogo");
      GlUtil.printGlError();
      System.out.println("[GLFrame] loading content data");
      Controller.getResLoader().setLoadString("preparing data");
   }

   public void update(Timer var1) {
      float var2 = Math.min(3.0F, var1.getDelta()) * 500.0F;
      this.time += var2;
   }

   public void handleException(Exception var1) {
      if (this.mainMenu == null) {
         GLFrame.processErrorDialogException((Exception)var1, (StateInterface)null);
      } else {
         this.mainMenu.errorDialog(var1);
      }
   }

   public GameMainMenuController getMainMenu() {
      return this.mainMenu;
   }

   public void setMainMenu(GameMainMenuController var1) {
      this.mainMenu = var1;
   }
}
