package org.schema.game.client.view.gui;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.io.IOException;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;
import org.schema.schine.resource.FileExt;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class GUIHelpPanelManager extends GUIElement {
   private Object2ObjectOpenHashMap entries = new Object2ObjectOpenHashMap();

   public GUIHelpPanelManager(InputState var1, String var2) throws ParserConfigurationException, SAXException, IOException {
      super(var1);
      Document var5 = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new FileExt(var2));
      Transition var10000 = Transition.BACK;
      var10000 = Transition.TUTORIAL_RESTART;
      var10000 = Transition.CONDITION_SATISFIED;
      var10000 = Transition.TUTORIAL_END;
      NodeList var6 = var5.getDocumentElement().getChildNodes();

      for(int var3 = 0; var3 < var6.getLength(); ++var3) {
         if (var6.item(var3).getNodeType() == 1) {
            GUIHelpPanel var4 = new GUIHelpPanel(var1, var6.item(var3));
            this.entries.put(var4.getOriginalTitle(), var4);
         }
      }

   }

   public void updateAll(InputState var1) {
      Iterator var2 = this.entries.values().iterator();

      while(var2.hasNext()) {
         ((GUIHelpPanel)var2.next()).update(var1);
      }

   }

   public GUIHelpPanel getGeneral() {
      return (GUIHelpPanel)this.entries.get("General");
   }

   public GUIHelpPanel getSelected() {
      GameClientState var1;
      PlayerGameControlManager var2;
      if ((var2 = (var1 = (GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager()).getPlayerIntercationManager().getPlayerCharacterManager().isTreeActive() && !var2.getPlayerIntercationManager().getPlayerCharacterManager().isSuspended()) {
         return var1.getCharacter().getGravity().isGravityOn() ? (GUIHelpPanel)this.entries.get("AstronautModeGravity") : (GUIHelpPanel)this.entries.get("AstronautModeZeroG");
      } else if (var2.getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController().isTreeActive() && !var2.getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController().isSuspended()) {
         return (GUIHelpPanel)this.entries.get("ShipFlightMode");
      } else if (var2.getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().isTreeActive() && !var2.getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().isSuspended() || var2.getPlayerIntercationManager().getSegmentControlManager().getSegmentBuildController().isTreeActive() && !var2.getPlayerIntercationManager().getSegmentControlManager().getSegmentBuildController().isSuspended()) {
         return (GUIHelpPanel)this.entries.get("BuildMode");
      } else if (var2.getShopControlManager().isTreeActive()) {
         return (GUIHelpPanel)this.entries.get("Shop");
      } else {
         return var2.getInventoryControlManager().isTreeActive() ? (GUIHelpPanel)this.entries.get("Inventory") : null;
      }
   }

   public void cleanUp() {
   }

   public void draw() {
      Vector3f var1 = new Vector3f(this.getPos());
      Vector3f var10000 = this.getPos();
      var10000.y -= this.getHeight();
      this.transform();
      this.getGeneral().draw();
      GUIHelpPanel var2;
      if ((var2 = this.getSelected()) != null) {
         var2.setPos(this.getGeneral().getWidth(), 0.0F, 0.0F);
         var2.draw();
      }

      this.setPos(var1);
   }

   public void onInit() {
   }

   public float getHeight() {
      GUIHelpPanel var1;
      return (var1 = this.getSelected()) != null ? Math.max(var1.getHeight(), this.getGeneral().getHeight()) : this.getGeneral().getHeight();
   }

   public float getWidth() {
      GUIHelpPanel var1;
      return (var1 = this.getSelected()) != null ? var1.getWidth() + this.getGeneral().getWidth() : this.getGeneral().getWidth();
   }
}
