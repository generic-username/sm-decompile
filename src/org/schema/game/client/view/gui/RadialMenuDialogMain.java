package org.schema.game.client.view.gui;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerRaceInput;
import org.schema.game.client.controller.PlayerTextInput;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.faction.FactionBlockDialog;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.reactor.ReactorTreeDialog;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.input.InputState;

public class RadialMenuDialogMain extends RadialMenuDialog implements GUIActivationCallback {
   public RadialMenuDialogMain(GameClientState var1) {
      super(var1);
   }

   public RadialMenu createMenu(RadialMenuDialog var1) {
      final GameClientState var2 = this.getState();
      RadialMenu var7;
      (var7 = new RadialMenu(var2, "MainRadial", var1, 800, 600, 50, FontLibrary.getBOLDBlender20())).addItem(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_0, new GUICallback() {
         public boolean isOccluded() {
            return !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               RadialMenuDialogMain.this.getPGCM().inventoryAction((Inventory)null);
               RadialMenuDialogMain.this.deactivate();
            }

         }
      }, this);
      var7.addItem(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_15, new GUICallback() {
         public boolean isOccluded() {
            return !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               RadialMenuDialogMain.this.getPGCM().factionAction();
               RadialMenuDialogMain.this.deactivate();
            }

         }
      }, this);
      var7.addItem(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_2, new GUICallback() {
         public boolean isOccluded() {
            return !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               RadialMenuDialogMain.this.getPGCM().navigationAction();
               RadialMenuDialogMain.this.deactivate();
            }

         }
      }, this);
      var7.addItem(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_17, new GUICallback() {
         public boolean isOccluded() {
            return !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               RadialMenuDialogMain.this.getPGCM().galaxyMapAction();
               RadialMenuDialogMain.this.deactivate();
            }

         }
      }, this);
      var7.addItem(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_3, new GUICallback() {
         public boolean isOccluded() {
            return !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               RadialMenuDialogMain.this.getPGCM().catalogAction();
               RadialMenuDialogMain.this.deactivate();
            }

         }
      }, this);
      var7.addItem(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_4, new GUICallback() {
         public boolean isOccluded() {
            return !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               RadialMenuDialogMain.this.getPGCM().shopAction();
               RadialMenuDialogMain.this.deactivate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return RadialMenuDialogMain.this.isActive(var2) && var2.isInShopDistance();
         }
      });
      var7.addItem(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_5, new GUICallback() {
         public boolean isOccluded() {
            return !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               RadialMenuDialogMain.this.getPGCM().fleetAction();
               RadialMenuDialogMain.this.deactivate();
            }

         }
      }, this);
      final GUIActivationCallback var3 = new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return RadialMenuDialogMain.this.isActive(var2) && RadialMenuDialogMain.this.getState().getCurrentPlayerObject() != null && RadialMenuDialogMain.this.getState().getCurrentPlayerObject() instanceof ManagedSegmentController;
         }
      };
      RadialMenu var4;
      RadialMenu var5;
      (var5 = (var4 = var7.addItemAsSubMenu(this.getOwnEntityName(), var3)).addItemAsSubMenu(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_31, var3)).addItem(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_32, new GUICallback() {
         public boolean isOccluded() {
            return !var3.isActive(var2) || !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               RadialMenuDialogMain.this.deactivate();
               RadialMenuDialogMain.this.getState().getController().getMineController().requestArmedClient((long)RadialMenuDialogMain.this.getState().getPlayer().getId(), RadialMenuDialogMain.this.getState().getPlayer().getCurrentSector());
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return var3.isVisible(var1);
         }

         public boolean isActive(InputState var1) {
            return var3.isActive(var1);
         }
      });
      var5.addItem(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_33, new GUICallback() {
         public boolean isOccluded() {
            return !var3.isActive(var2) || !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               RadialMenuDialogMain.this.deactivate();
               RadialMenuDialogMain.this.getState().getController().getMineController().requestArmedAllClient((long)RadialMenuDialogMain.this.getState().getPlayer().getId());
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return var3.isVisible(var1);
         }

         public boolean isActive(InputState var1) {
            return var3.isActive(var1);
         }
      });
      String var6 = this.getState().getPlayer().getMineAutoArmSeconds() >= 0 ? StringTools.formatCountdown(this.getState().getPlayer().getMineAutoArmSeconds()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_26;
      var5.addItem(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_27, var6), new GUICallback() {
         public boolean isOccluded() {
            return !var3.isActive(var2) || !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               RadialMenuDialogMain.this.deactivate();
               PlayerTextInput var10000 = new PlayerTextInput("INPOUTMINESEC", RadialMenuDialogMain.this.getState(), 8, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_28, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_29) {
                  public void onFailedTextCheck(String var1) {
                  }

                  public String handleAutoComplete(String var1, TextCallback var2x, String var3x) throws PrefixNotFoundException {
                     return null;
                  }

                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public boolean onInput(String var1) {
                     try {
                        int var3x = Math.max(-1, Integer.parseInt(var1));
                        ((GameClientState)this.getState()).getPlayer().requestMineArmTimerChange(var3x);
                        return true;
                     } catch (NumberFormatException var2x) {
                        this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_30);
                        return false;
                     }
                  }

                  public void onDeactivate() {
                  }
               };
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return var3.isVisible(var1);
         }

         public boolean isActive(InputState var1) {
            return var3.isActive(var1);
         }
      });
      var4.addItem(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_25, new GUICallback() {
         public boolean isOccluded() {
            return !var3.isActive(var2) || !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               RadialMenuDialogMain.this.deactivate();
               if (RadialMenuDialogMain.this.getState().getCurrentPlayerObject() != null && RadialMenuDialogMain.this.getState().getCurrentPlayerObject() instanceof ManagedSegmentController) {
                  (new ReactorTreeDialog(RadialMenuDialogMain.this.getState(), (ManagedSegmentController)RadialMenuDialogMain.this.getState().getCurrentPlayerObject())).activate();
               }
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return var3.isVisible(var1);
         }

         public boolean isActive(InputState var1) {
            return var3.isActive(var1) && ((SegmentController)RadialMenuDialogMain.this.getState().getCurrentPlayerObject()).hasAnyReactors();
         }
      });
      var4.addItem(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_6, new GUICallback() {
         public boolean isOccluded() {
            return !var3.isActive(var2) || !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               RadialMenuDialogMain.this.getPGCM().structureAction();
               RadialMenuDialogMain.this.deactivate();
            }

         }
      }, var3);
      var4.addItem(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_7, new GUICallback() {
         public boolean isOccluded() {
            return !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               RadialMenuDialogMain.this.getPGCM().weaponAction();
               RadialMenuDialogMain.this.deactivate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return var3.isActive(var1) && var2.getCurrentPlayerObject() instanceof Ship;
         }
      });
      var4.addItem(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_20, new GUICallback() {
         public boolean isOccluded() {
            return !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               RadialMenuDialogMain.this.getPGCM().aiConfigurationAction((SegmentPiece)null);
               RadialMenuDialogMain.this.deactivate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return var3.isActive(var1) && var2.getCurrentPlayerObject() instanceof Ship;
         }
      });
      var4.addItem(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_9, new GUICallback() {
         public boolean isOccluded() {
            return !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               if (RadialMenuDialogMain.this.getState().getCurrentPlayerObject() != null && RadialMenuDialogMain.this.getState().getCurrentPlayerObject() instanceof SegmentController && RadialMenuDialogMain.this.getState().getCurrentPlayerObject() instanceof ManagedSegmentController) {
                  Long2ObjectOpenHashMap var3;
                  if ((var3 = ((ManagedSegmentController)RadialMenuDialogMain.this.getState().getCurrentPlayerObject()).getManagerContainer().getNamedInventoriesClient()).size() > 0) {
                     RadialMenuDialogMain.this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().getInventoryPanel().deactivateAllOther();
                     RadialMenuDialogMain.this.getPGCM().inventoryAction((Inventory)var3.values().iterator().next(), true, true);
                  } else {
                     RadialMenuDialogMain.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_12, 0.0F);
                  }
               }

               RadialMenuDialogMain.this.deactivate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return var3.isActive(var1) && var2.getCurrentPlayerObject() instanceof ManagedSegmentController;
         }
      });
      var4.addItem(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_18, new GUICallback() {
         public boolean isOccluded() {
            return !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               if (RadialMenuDialogMain.this.getState().getCurrentPlayerObject() != null && RadialMenuDialogMain.this.getState().getCurrentPlayerObject() instanceof Ship) {
                  RadialMenuDialogMain.this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().activateThrustManager(RadialMenuDialogMain.this.getState().getShip());
               }

               RadialMenuDialogMain.this.deactivate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return var3.isActive(var1) && var2.getCurrentPlayerObject() instanceof Ship;
         }
      });
      var4.addItem(new Object() {
         public String toString() {
            if (RadialMenuDialogMain.this.getState().getCurrentPlayerObject() != null && RadialMenuDialogMain.this.getState().getCurrentPlayerObject() instanceof SegmentController) {
               return ((SegmentController)RadialMenuDialogMain.this.getState().getCurrentPlayerObject()).getElementClassCountMap().get((short)291) > 0 ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_1 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_16;
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_8;
            }
         }
      }, new GUICallback() {
         public boolean isOccluded() {
            return !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               if (!RadialMenuDialogMain.this.getState().getController().getPlayerInputs().isEmpty() && RadialMenuDialogMain.this.getState().getController().getPlayerInputs().get(0) instanceof FactionBlockDialog) {
                  ((DialogInterface)RadialMenuDialogMain.this.getState().getController().getPlayerInputs().get(0)).deactivate();
                  return;
               }

               SegmentController var3;
               if (RadialMenuDialogMain.this.getState().getCurrentPlayerObject() != null && RadialMenuDialogMain.this.getState().getCurrentPlayerObject() instanceof SegmentController && (var3 = (SegmentController)RadialMenuDialogMain.this.getState().getCurrentPlayerObject()).getElementClassCountMap().get((short)291) > 0) {
                  if (RadialMenuDialogMain.this.getState().getPlayer().getFactionId() != 0) {
                     RadialMenuDialogMain.this.getPGCM().getPlayerIntercationManager().activateFactionDiag(var3);
                     return;
                  }

                  if (var3.getFactionId() != 0) {
                     RadialMenuDialogMain.this.getPGCM().getPlayerIntercationManager().activateResetFactionIfOwner(var3);
                  }
               }
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return RadialMenuDialogMain.this.getState().getCurrentPlayerObject() != null && RadialMenuDialogMain.this.getState().getCurrentPlayerObject() instanceof SegmentController && ((SegmentController)RadialMenuDialogMain.this.getState().getCurrentPlayerObject()).getElementClassCountMap().get((short)291) > 0;
         }

         public boolean isHighlighted(InputState var1) {
            return !RadialMenuDialogMain.this.getState().getController().getPlayerInputs().isEmpty() && RadialMenuDialogMain.this.getState().getController().getPlayerInputs().get(0) instanceof FactionBlockDialog;
         }
      });
      RadialMenu var8 = var4.addItemAsSubMenu(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_21, this);
      this.addLoadUnloadSettings(var8, var2);
      (var8 = var7.addItemAsSubMenu(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_19, this)).addItem(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_10, new GUICallback() {
         public boolean isOccluded() {
            return !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               RadialMenuDialogMain.this.getState().getGlobalGameControlManager().getIngameControlManager().activatePlayerMesssages();
               RadialMenuDialogMain.this.deactivate();
            }

         }
      }, this);
      var8.addItem(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_11, new GUICallback() {
         public boolean isOccluded() {
            return !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               RadialMenuDialogMain.this.getState().getGlobalGameControlManager().getIngameControlManager().activateMesssageLog();
               RadialMenuDialogMain.this.deactivate();
            }

         }
      }, this);
      var8.addItem(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_13, new GUICallback() {
         public boolean isOccluded() {
            return !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               boolean var3 = true;
               Iterator var4 = RadialMenuDialogMain.this.getState().getController().getPlayerInputs().iterator();

               while(var4.hasNext()) {
                  if ((DialogInterface)var4.next() instanceof PlayerRaceInput) {
                     var3 = false;
                  }
               }

               if (var3) {
                  (new PlayerRaceInput(RadialMenuDialogMain.this.getState(), (SegmentPiece)null)).activate();
               }

               RadialMenuDialogMain.this.deactivate();
            }

         }
      }, this);
      return var7;
   }

   private void addLoadUnloadSettings(RadialMenu var1, final GameClientState var2) {
      SegmentController.PullPermission[] var3;
      int var4 = (var3 = SegmentController.PullPermission.values()).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         final SegmentController.PullPermission var6 = var3[var5];
         var1.addItem(new Object() {
            public String toString() {
               return var6.desc;
            }
         }, new GUICallback() {
            public boolean isOccluded() {
               return !RadialMenuDialogMain.this.isActive(var2);
            }

            public void callback(GUIElement var1, MouseEvent var2x) {
               if (var2x.pressedLeftMouse() && RadialMenuDialogMain.this.getState().getCurrentPlayerObject() != null && RadialMenuDialogMain.this.getState().getCurrentPlayerObject() instanceof SegmentController) {
                  ((SegmentController)RadialMenuDialogMain.this.getState().getCurrentPlayerObject()).getNetworkObject().pullPermissionChangeBuffer.add((byte)var6.ordinal());
               }

            }
         }, new GUIActivationHighlightCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return RadialMenuDialogMain.this.getState().getCurrentPlayerObject() != null && RadialMenuDialogMain.this.getState().getCurrentPlayerObject() instanceof SegmentController;
            }

            public boolean isHighlighted(InputState var1) {
               if (RadialMenuDialogMain.this.getState().getCurrentPlayerObject() != null && RadialMenuDialogMain.this.getState().getCurrentPlayerObject() instanceof SegmentController) {
                  return ((SegmentController)RadialMenuDialogMain.this.getState().getCurrentPlayerObject()).pullPermission == var6;
               } else {
                  return false;
               }
            }
         }, false);
      }

      var1.addItem(new Object() {
         public String toString() {
            if (RadialMenuDialogMain.this.getState().getCurrentPlayerObject() != null && RadialMenuDialogMain.this.getState().getCurrentPlayerObject() instanceof SegmentController) {
               SegmentController var1;
               if ((var1 = (SegmentController)RadialMenuDialogMain.this.getState().getCurrentPlayerObject()).railController.isDocked()) {
                  return var1.lastAllowed != var1.railController.previous.rail.getSegmentController().dbId ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_22 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_23;
               } else {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_24;
               }
            } else {
               return "-";
            }
         }
      }, new GUICallback() {
         public boolean isOccluded() {
            return !RadialMenuDialogMain.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            SegmentController var3;
            if (var2x.pressedLeftMouse() && RadialMenuDialogMain.this.getState().getCurrentPlayerObject() != null && RadialMenuDialogMain.this.getState().getCurrentPlayerObject() instanceof SegmentController && (var3 = (SegmentController)RadialMenuDialogMain.this.getState().getCurrentPlayerObject()).railController.isDocked()) {
               if (var3.lastAllowed != var3.railController.previous.rail.getSegmentController().dbId) {
                  var3.getNetworkObject().pullPermissionAskAnswerBuffer.add(var3.railController.previous.rail.getSegmentController().dbId);
                  return;
               }

               var3.getNetworkObject().pullPermissionAskAnswerBuffer.add(0L);
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return RadialMenuDialogMain.this.getState().getCurrentPlayerObject() != null && RadialMenuDialogMain.this.getState().getCurrentPlayerObject() instanceof SegmentController ? ((SegmentController)RadialMenuDialogMain.this.getState().getCurrentPlayerObject()).railController.isDocked() : false;
         }

         public boolean isHighlighted(InputState var1) {
            if (RadialMenuDialogMain.this.getState().getCurrentPlayerObject() != null && RadialMenuDialogMain.this.getState().getCurrentPlayerObject() instanceof SegmentController) {
               SegmentController var2;
               return (var2 = (SegmentController)RadialMenuDialogMain.this.getState().getCurrentPlayerObject()).railController.isDocked() && var2.lastAllowed == var2.railController.previous.rail.getSegmentController().dbId;
            } else {
               return false;
            }
         }
      }, false);
   }

   private Object getOwnEntityName() {
      return this.getState().getCurrentPlayerObject() != null ? this.getState().getCurrentPlayerObject().getType().getName() : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGMAIN_14;
   }

   public PlayerGameControlManager getPGCM() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   public boolean isVisible(InputState var1) {
      return true;
   }

   public boolean isActive(InputState var1) {
      return super.isActive();
   }
}
