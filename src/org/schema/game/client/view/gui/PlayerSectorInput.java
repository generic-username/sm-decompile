package org.schema.game.client.view.gui;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.navigation.navigationnew.SavedCoordinatesScrollableListNew;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;

public abstract class PlayerSectorInput extends PlayerGameTextInput {
   public PlayerSectorInput(GameClientState var1, Object var2, Object var3, String var4) {
      super("SEC_INPUT", var1, 630, 400, 100, var2, var3, var4);
      this.getInputPanel().onInit();
      ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().setTextBoxHeightLast(80);
      ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().addNewTextBox(1);
      SavedCoordinatesScrollableListNew var5;
      (var5 = new SavedCoordinatesScrollableListNew(this.getState(), ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().getContent(1), this)).onInit();
      ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().getContent(1).attach(var5);
      this.setInputChecker(new InputChecker() {
         public boolean check(String var1, TextCallback var2) {
            try {
               if (var1.length() == 0) {
                  return true;
               } else {
                  Vector3i.parseVector3iFree(var1);
                  return true;
               }
            } catch (NumberFormatException var3) {
               var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_PLAYERSECTORINPUT_0);
               return false;
            }
         }
      });
   }

   public String[] getCommandPrefixes() {
      return null;
   }

   public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
      return null;
   }

   public void onDeactivate() {
   }

   public void onFailedTextCheck(String var1) {
      this.setErrorMessage(var1);
   }

   public boolean onInput(String var1) {
      try {
         if (var1.trim().length() == 0) {
            this.handleEnteredEmpty();
         } else {
            this.handleEntered(Vector3i.parseVector3iFree(var1));
         }

         return true;
      } catch (NumberFormatException var2) {
         var2.printStackTrace();
         return false;
      }
   }

   public abstract void handleEnteredEmpty();

   public abstract void handleEntered(Vector3i var1);

   public abstract Object getSelectCoordinateButtonText();
}
