package org.schema.game.client.view.gui.advancedEntity;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.CollectionManagerChangeListener;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.ButtonCallback;
import org.schema.game.client.view.gui.advanced.tools.ButtonResult;
import org.schema.game.client.view.gui.advanced.tools.DropdownCallback;
import org.schema.game.client.view.gui.advanced.tools.DropdownResult;
import org.schema.game.client.view.gui.advanced.tools.StatLabelResult;
import org.schema.game.common.controller.SegNotifyType;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.FireingUnit;
import org.schema.game.common.controller.elements.UsableControllableFireingElementManager;
import org.schema.game.common.controller.elements.combination.Combinable;
import org.schema.game.common.controller.elements.combination.CombinationSettings;
import org.schema.game.common.controller.elements.combination.modifier.Modifier;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;

public abstract class AdvancedEntityWeaponGUIGroup extends AdvancedEntityGUIGroup implements CollectionManagerChangeListener {
   public FireingUnit unit;
   private long lastCheckCS;
   private CombinationSettings cachedCC;
   public ControlBlockElementCollectionManager selectedCollectionManager;
   public FireingUnit selectedElement;
   private boolean dirtyModuleDropdownDirty;
   private Modifier cachedModifier;
   private long lastCheck;

   public List getCollectionManagers() {
      return this.getEm().getCollectionManagers();
   }

   protected abstract CombinationSettings getWeaponCombiSettingsRaw();

   public CombinationSettings getWeaponCombiSettings() {
      if (this.getState().updateTime - this.lastCheckCS > 500L) {
         this.cachedCC = this.getWeaponCombiSettingsRaw();
         this.lastCheckCS = this.getState().getUpdateTime();
      }

      return this.cachedCC;
   }

   public AdvancedEntityWeaponGUIGroup(AdvancedGUIElement var1) {
      super(var1);
      this.getState().getController().addCollectionManagerChangeListener(this);
   }

   public abstract UsableControllableFireingElementManager getEm();

   public short getModuleType() {
      return this.getEm().controllingId;
   }

   public short getComputerType() {
      return this.getEm().controllerId;
   }

   public void selectSelectedWeapon() {
      if (this.selectedCollectionManager != null) {
         this.getPlayerInteractionControlManager().setSelectedBlockByActiveController(this.selectedCollectionManager.getControllerElement());
      }

   }

   public Modifier getCombiValue() {
      if (this.getState().updateTime - this.lastCheck > 500L) {
         ControlBlockElementCollectionManager var1;
         if ((var1 = this.selectedCollectionManager.getSupportCollectionManager()) != null) {
            this.cachedModifier = ((Combinable)this.getEm()).getAddOn().getGUI(this.selectedCollectionManager, this.selectedElement, (ControlBlockElementCollectionManager)var1, this.selectedCollectionManager.getEffectCollectionManager());
         } else {
            this.cachedModifier = null;
         }

         this.lastCheck = this.getState().getUpdateTime();
      }

      return this.cachedModifier;
   }

   public int getComputerCount() {
      return this.getTypeCount(this.getComputerType());
   }

   public int getModuleCount() {
      return this.getTypeCount(this.getModuleType());
   }

   public void combineWith(ControlBlockElementCollectionManager var1) {
      if (this.selectedCollectionManager != null) {
         this.getSegCon().getControlElementMap().switchControllerForElement(this.selectedCollectionManager.getControllerIndex(), var1.getControllerIndex(), var1.getControllerElement().getType());
      }

   }

   public void addWeaponPanel(GUIContentPane var1, int var2, int var3) {
      this.addButton(var1.getContent(0), var2, var3, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  if (AdvancedEntityWeaponGUIGroup.this.getState().getPlayerInputs().isEmpty()) {
                     AdvancedEntityWeaponGUIGroup.this.getPlayerGameControlManager().weaponAction();
                  }

               }
            };
         }

         public boolean isHighlighted() {
            return AdvancedEntityWeaponGUIGroup.this.getPlayerGameControlManager().getWeaponControlManager().isTreeActive();
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYWEAPONGUIGROUP_0;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
   }

   public int addWeaponBlockIcons(GUIContentPane var1, int var2, int var3) {
      this.addStatLabel(var1.getContent(0), var2, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYWEAPONGUIGROUP_1;
         }

         public String getValue() {
            return ElementKeyMap.getInfo(AdvancedEntityWeaponGUIGroup.this.getComputerType()).getName();
         }

         public int getStatDistance() {
            return 100;
         }
      });
      this.addStatLabel(var1.getContent(0), var2, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYWEAPONGUIGROUP_2;
         }

         public String getValue() {
            return ElementKeyMap.getInfo(AdvancedEntityWeaponGUIGroup.this.getModuleType()).getName();
         }

         public int getStatDistance() {
            return 100;
         }
      });
      this.addWeaponBlockIcon(var1, var2, var3, new Object() {
         public String toString() {
            return AdvancedEntityWeaponGUIGroup.this.getMan() != null && AdvancedEntityWeaponGUIGroup.this.getEm() != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYWEAPONGUIGROUP_3, ElementKeyMap.getInfo(AdvancedEntityWeaponGUIGroup.this.getComputerType()).getName()) : "";
         }
      }, new InitInterface() {
         public short getType() {
            return this.isInit() ? AdvancedEntityWeaponGUIGroup.this.getComputerType() : 1;
         }

         public boolean isInit() {
            return AdvancedEntityWeaponGUIGroup.this.getMan() != null && AdvancedEntityWeaponGUIGroup.this.getEm() != null;
         }
      });
      this.addWeaponBlockIcon(var1, var2 + 1, var3++, new Object() {
         public String toString() {
            return AdvancedEntityWeaponGUIGroup.this.getMan() != null && AdvancedEntityWeaponGUIGroup.this.getEm() != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYWEAPONGUIGROUP_4, ElementKeyMap.getInfo(AdvancedEntityWeaponGUIGroup.this.getModuleType()).getName()) : "";
         }
      }, new InitInterface() {
         public short getType() {
            return this.isInit() ? AdvancedEntityWeaponGUIGroup.this.getModuleType() : 1;
         }

         public boolean isInit() {
            return AdvancedEntityWeaponGUIGroup.this.getMan() != null && AdvancedEntityWeaponGUIGroup.this.getEm() != null;
         }
      });
      return var3;
   }

   public void addAddButton(GUIContentPane var1, int var2, int var3) {
      this.addButton(var1.getContent(0), var2, var3, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  AdvancedEntityWeaponGUIGroup.this.resetQueue();
                  AdvancedEntityWeaponGUIGroup.this.selectSelectedWeapon();
                  AdvancedEntityWeaponGUIGroup.this.promptBuild(AdvancedEntityWeaponGUIGroup.this.getModuleType(), 1, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYWEAPONGUIGROUP_5, ElementKeyMap.getInfo(AdvancedEntityWeaponGUIGroup.this.getModuleType()).getName()));
               }
            };
         }

         public boolean isActive() {
            return super.isActive() && AdvancedEntityWeaponGUIGroup.this.getState().getPlayer() != null && AdvancedEntityWeaponGUIGroup.this.getState().getPlayer().getInventory().existsInInventory(AdvancedEntityWeaponGUIGroup.this.getModuleType()) && AdvancedEntityWeaponGUIGroup.this.canQueue(AdvancedEntityWeaponGUIGroup.this.getModuleType(), 1);
         }

         public String getToolTipText() {
            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYWEAPONGUIGROUP_6, ElementKeyMap.getInfo(AdvancedEntityWeaponGUIGroup.this.getComputerType()).getName());
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYWEAPONGUIGROUP_7;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
   }

   public void addSelectButton(GUIContentPane var1, int var2, int var3) {
      this.addButton(var1.getContent(0), var2, var3, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  AdvancedEntityWeaponGUIGroup.this.selectSelectedWeapon();
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYWEAPONGUIGROUP_8;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
   }

   public void addBuildButton(GUIContentPane var1, GUIDockableDirtyInterface var2, int var3, int var4) {
      this.addButton(var1.getContent(0), var3, var4, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  AdvancedEntityWeaponGUIGroup.this.resetQueue();
                  AdvancedEntityWeaponGUIGroup.this.promptBuild(AdvancedEntityWeaponGUIGroup.this.getComputerType(), 1, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYWEAPONGUIGROUP_9, ElementKeyMap.getInfo(AdvancedEntityWeaponGUIGroup.this.getComputerType()).getName()));
                  AdvancedEntityWeaponGUIGroup.this.promptBuild(AdvancedEntityWeaponGUIGroup.this.getModuleType(), 1, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYWEAPONGUIGROUP_10, ElementKeyMap.getInfo(AdvancedEntityWeaponGUIGroup.this.getModuleType()).getName()));
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYWEAPONGUIGROUP_11;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }

         public boolean isActive() {
            return super.isActive() && AdvancedEntityWeaponGUIGroup.this.canQueue(AdvancedEntityWeaponGUIGroup.this.getComputerType(), 1) && AdvancedEntityWeaponGUIGroup.this.canQueue(AdvancedEntityWeaponGUIGroup.this.getModuleType(), 1);
         }
      });
   }

   public boolean isSlave(ControlBlockElementCollectionManager var1) {
      return this.getMan() != null && this.getMan().getSlavesAndEffects().contains(var1.getControllerIndex4());
   }

   public void onChange(ElementCollectionManager var1) {
      if (var1 == this.selectedCollectionManager) {
         this.dirtyModuleDropdownDirty = true;
      }

   }

   public abstract String getSystemNameShort();

   public abstract String getOutputNameShort();

   public class ModuleSelectDropdown extends DropdownResult {
      private ObjectArrayList list;
      private UsableControllableFireingElementManager lastMan;
      private List elements;

      public DropdownCallback initCallback() {
         return new DropdownCallback() {
            public void onChanged(Object var1) {
               if (var1 instanceof FireingUnit) {
                  AdvancedEntityWeaponGUIGroup.this.selectedElement = (FireingUnit)var1;
               }

            }
         };
      }

      public String getToolTipText() {
         return "Available Computers";
      }

      public String getName() {
         return "Select";
      }

      public Collection getDropdownElements(final GUIElement var1) {
         if (AdvancedEntityWeaponGUIGroup.this.getMan() != null && AdvancedEntityWeaponGUIGroup.this.selectedCollectionManager != null) {
            this.list = new ObjectArrayList();
            this.elements = AdvancedEntityWeaponGUIGroup.this.selectedCollectionManager.getElementCollections();
            AdvancedEntityWeaponGUIGroup.this.getMan().getPowerInterface().getReactorSet();
            Iterator var2 = this.elements.iterator();

            while(var2.hasNext()) {
               FireingUnit var3 = (FireingUnit)var2.next();
               String var4 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYWEAPONGUIGROUP_13, AdvancedEntityWeaponGUIGroup.this.getOutputNameShort(), var3.size());
               GUIAncor var5;
               (var5 = new GUIAncor(AdvancedEntityWeaponGUIGroup.this.getState(), 200.0F, 20.0F)).setUserPointer(var3);
               this.list.add(var5);
               GUITextOverlay var7;
               (var7 = new GUITextOverlay(100, 20, FontLibrary.FontSize.MEDIUM, AdvancedEntityWeaponGUIGroup.this.getState()) {
                  public void draw() {
                     if (var1 != null) {
                        this.limitTextWidth = (int)var1.getWidth();
                     }

                     super.draw();
                  }
               }).setTextSimple(var4);
               var7.getPos().x = 3.0F;
               var7.getPos().y = 2.0F;
               var5.attach(var7);
            }

            boolean var6 = false;
            if (AdvancedEntityWeaponGUIGroup.this.selectedElement != null && AdvancedEntityWeaponGUIGroup.this.selectedElement.elementCollectionManager == AdvancedEntityWeaponGUIGroup.this.selectedCollectionManager) {
               Iterator var8 = this.elements.iterator();

               while(var8.hasNext()) {
                  FireingUnit var9;
                  if ((var9 = (FireingUnit)var8.next()).idPos == AdvancedEntityWeaponGUIGroup.this.selectedElement.idPos) {
                     AdvancedEntityWeaponGUIGroup.this.selectedElement = var9;
                     var6 = true;
                     break;
                  }
               }
            }

            if (!var6) {
               AdvancedEntityWeaponGUIGroup.this.selectedElement = null;
            }

            if (AdvancedEntityWeaponGUIGroup.this.selectedElement == null) {
               if (this.getDefault() != null) {
                  AdvancedEntityWeaponGUIGroup.this.selectedElement = (FireingUnit)((GUIAncor)this.getDefault()).getUserPointer();
               } else {
                  AdvancedEntityWeaponGUIGroup.this.selectedElement = null;
               }
            }

            AdvancedEntityWeaponGUIGroup.this.dirtyModuleDropdownDirty = false;
            return this.list;
         } else {
            return new ObjectArrayList();
         }
      }

      public Object getDefault() {
         if (this.list != null && AdvancedEntityWeaponGUIGroup.this.selectedCollectionManager != null && !this.list.isEmpty()) {
            return this.list.size() > 0 ? (GUIElement)this.list.get(0) : null;
         } else {
            return null;
         }
      }

      public boolean needsListUpdate() {
         return AdvancedEntityWeaponGUIGroup.this.dirtyModuleDropdownDirty;
      }

      public void flagListNeedsUpdate(boolean var1) {
         AdvancedEntityWeaponGUIGroup.this.dirtyModuleDropdownDirty = var1;
      }
   }

   public class WeaponSelectDropdownResult extends DropdownResult implements DrawerObserver {
      private ObjectArrayList list;
      private boolean dirty;
      private UsableControllableFireingElementManager lastMan;
      private List collectionManagers;

      public DropdownCallback initCallback() {
         return new DropdownCallback() {
            public void onChanged(Object var1) {
               if (var1 instanceof ControlBlockElementCollectionManager) {
                  AdvancedEntityWeaponGUIGroup.this.selectedCollectionManager = (ControlBlockElementCollectionManager)var1;
                  AdvancedEntityWeaponGUIGroup.this.dirtyModuleDropdownDirty = true;
               }

            }
         };
      }

      public void update(Timer var1) {
         if (AdvancedEntityWeaponGUIGroup.this.getMan() != null && AdvancedEntityWeaponGUIGroup.this.getEm() != this.lastMan) {
            this.dirty = true;
            if (this.lastMan != null) {
               this.lastMan.deleteObserver(this);
            }

            this.lastMan = AdvancedEntityWeaponGUIGroup.this.getEm();
            this.lastMan.addObserver(this);
         }

         super.update(var1);
      }

      public String getToolTipText() {
         return "Available Computers";
      }

      public String getName() {
         return "Select";
      }

      public Collection getDropdownElements(final GUIElement var1) {
         if (AdvancedEntityWeaponGUIGroup.this.getMan() == null) {
            return new ObjectArrayList();
         } else {
            this.list = new ObjectArrayList();
            this.collectionManagers = AdvancedEntityWeaponGUIGroup.this.getCollectionManagers();
            AdvancedEntityWeaponGUIGroup.this.getMan().getPowerInterface().getReactorSet();
            boolean var2 = true;
            Iterator var3 = this.collectionManagers.iterator();

            while(var3.hasNext()) {
               ControlBlockElementCollectionManager var4 = (ControlBlockElementCollectionManager)var3.next();
               String var5 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYWEAPONGUIGROUP_14, AdvancedEntityWeaponGUIGroup.this.getSystemNameShort(), var4.getTotalSize());
               if (AdvancedEntityWeaponGUIGroup.this.isSlave(var4)) {
                  var5 = var5 + " " + Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYWEAPONGUIGROUP_12;
               }

               GUIAncor var6;
               (var6 = new GUIAncor(AdvancedEntityWeaponGUIGroup.this.getState(), 200.0F, 20.0F)).setUserPointer(var4);
               this.list.add(var6);
               GUITextOverlay var7;
               (var7 = new GUITextOverlay(100, 20, FontLibrary.FontSize.MEDIUM, AdvancedEntityWeaponGUIGroup.this.getState()) {
                  public void draw() {
                     if (var1 != null) {
                        this.limitTextWidth = (int)var1.getWidth();
                     }

                     super.draw();
                  }
               }).setTextSimple(var5);
               var7.getPos().x = 3.0F;
               var7.getPos().y = 2.0F;
               var6.attach(var7);
               if (var4 == AdvancedEntityWeaponGUIGroup.this.selectedCollectionManager) {
                  var2 = false;
               }
            }

            if (var2) {
               if (this.getDefault() != null) {
                  AdvancedEntityWeaponGUIGroup.this.selectedCollectionManager = (ControlBlockElementCollectionManager)((GUIAncor)this.getDefault()).getUserPointer();
               } else {
                  AdvancedEntityWeaponGUIGroup.this.selectedCollectionManager = null;
               }
            }

            if (AdvancedEntityWeaponGUIGroup.this.selectedCollectionManager != null) {
               this.change(AdvancedEntityWeaponGUIGroup.this.selectedCollectionManager);
            }

            AdvancedEntityWeaponGUIGroup.this.dirtyModuleDropdownDirty = true;
            this.dirty = false;
            return this.list;
         }
      }

      public Object getDefault() {
         if (AdvancedEntityWeaponGUIGroup.this.selectedCollectionManager != null && this.collectionManagers != null && this.collectionManagers.contains(AdvancedEntityWeaponGUIGroup.this.selectedCollectionManager)) {
            for(int var1 = 0; var1 < this.list.size(); ++var1) {
               if (((GUIElement)this.list.get(var1)).getUserPointer() == AdvancedEntityWeaponGUIGroup.this.selectedCollectionManager) {
                  return this.list.get(var1);
               }
            }
         }

         if (this.list != null && this.collectionManagers != null && !this.collectionManagers.isEmpty()) {
            return this.list.size() > 0 ? (GUIElement)this.list.get(0) : null;
         } else {
            return null;
         }
      }

      public boolean needsListUpdate() {
         return this.dirty;
      }

      public void flagListNeedsUpdate(boolean var1) {
         this.dirty = var1;
      }

      public void update(DrawerObservable var1, Object var2, Object var3) {
         if (var2 == SegNotifyType.SHIP_ELEMENT_CHANGED) {
            this.dirty = true;
         }

      }
   }
}
