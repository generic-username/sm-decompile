package org.schema.game.client.view.gui.advancedEntity;

import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.game.client.view.gui.advanced.tools.StatLabelResult;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.FireingUnit;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.controller.elements.StationaryManagerContainer;
import org.schema.game.common.controller.elements.combination.WeaponCombiSettings;
import org.schema.game.common.controller.elements.combination.modifier.WeaponUnitModifier;
import org.schema.game.common.controller.elements.weapon.WeaponCollectionManager;
import org.schema.game.common.controller.elements.weapon.WeaponElementManager;
import org.schema.game.common.controller.elements.weapon.WeaponUnit;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;

public class AdvancedEntityCannon extends AdvancedEntityWeaponGUIGroup {
   public AdvancedEntityCannon(AdvancedGUIElement var1) {
      super(var1);
   }

   public String getId() {
      return "AECANNON";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_0;
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      int var3 = this.addWeaponBlockIcons(var1, 0, 0);
      this.addBuildButton(var1, var2, 0, var3);
      this.addWeaponPanel(var1, 1, var3++);
      this.addSelectButton(var1, 0, var3);
      this.addAddButton(var1, 1, var3++);
      this.addLabel(var1.getContent(0), 0, var3++, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_1;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }
      });
      this.addDropdown(var1.getContent(0), 0, var3++, new AdvancedEntityWeaponGUIGroup.WeaponSelectDropdownResult());
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_36;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityCannon.this.selectedElement != null && AdvancedEntityCannon.this.selectedCollectionManager != null ? "[" + ElementCollection.getPosString(((WeaponCollectionManager)AdvancedEntityCannon.this.selectedCollectionManager).getControllerIndex()) + "]" : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_26;
         }

         public int getStatDistance() {
            return AdvancedEntityCannon.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_21;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityCannon.this.selectedCollectionManager != null ? StringTools.formatSeperated(((WeaponCollectionManager)AdvancedEntityCannon.this.selectedCollectionManager).getTotalSize()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_12;
         }

         public int getStatDistance() {
            return AdvancedEntityCannon.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_4;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityCannon.this.selectedCollectionManager != null ? String.valueOf(((WeaponCollectionManager)AdvancedEntityCannon.this.selectedCollectionManager).getElementCollections().size()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_10;
         }

         public int getStatDistance() {
            return AdvancedEntityCannon.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_6;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            ControlBlockElementCollectionManager var1;
            return AdvancedEntityCannon.this.selectedCollectionManager != null && (var1 = ((WeaponCollectionManager)AdvancedEntityCannon.this.selectedCollectionManager).getSupportCollectionManager()) != null ? String.valueOf(var1.getModuleName()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_40;
         }

         public int getStatDistance() {
            return AdvancedEntityCannon.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_8;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityCannon.this.selectedCollectionManager != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_9, StringTools.formatPointZero(((WeaponCollectionManager)AdvancedEntityCannon.this.selectedCollectionManager).getWeaponSpeed())) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_7;
         }

         public int getStatDistance() {
            return AdvancedEntityCannon.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_11;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityCannon.this.selectedCollectionManager != null ? StringTools.formatDistance(((WeaponCollectionManager)AdvancedEntityCannon.this.selectedCollectionManager).getWeaponDistance()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_5;
         }

         public int getStatDistance() {
            return AdvancedEntityCannon.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_13;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityCannon.this.selectedCollectionManager != null && ((WeaponCombiSettings)AdvancedEntityCannon.this.getWeaponCombiSettings()).possibleZoom > 0.0F ? StringTools.formatPointZero(1.0F / ((WeaponCombiSettings)AdvancedEntityCannon.this.getWeaponCombiSettings()).possibleZoom * 100.0F) + "%" : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_3;
         }

         public int getStatDistance() {
            return AdvancedEntityCannon.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_15;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityCannon.this.selectedCollectionManager != null && ((WeaponCollectionManager)AdvancedEntityCannon.this.selectedCollectionManager).getElementCollections().size() > 0 ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_16, StringTools.formatPointZeroZero(AdvancedEntityCannon.this.getEm().calculateReload((FireingUnit)((WeaponCollectionManager)AdvancedEntityCannon.this.selectedCollectionManager).getElementCollections().get(0)) / 1000.0D)) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_39;
         }

         public int getStatDistance() {
            return AdvancedEntityCannon.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_18;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityCannon.this.selectedCollectionManager != null && ((WeaponCollectionManager)AdvancedEntityCannon.this.selectedCollectionManager).getElementCollections().size() > 0 ? ((WeaponCombiSettings)AdvancedEntityCannon.this.getWeaponCombiSettings()).acidType.toString() : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_34;
         }

         public int getStatDistance() {
            return AdvancedEntityCannon.this.getTextDist();
         }
      });
      this.addLabel(var1.getContent(0), 0, var3++, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_20;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }
      });
      this.addDropdown(var1.getContent(0), 0, var3++, new AdvancedEntityWeaponGUIGroup.ModuleSelectDropdown());
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_35;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityCannon.this.selectedElement != null && AdvancedEntityCannon.this.selectedCollectionManager != null ? "[" + ElementCollection.getPosString(((WeaponUnit)AdvancedEntityCannon.this.selectedElement).idPos) + "]" : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_32;
         }

         public int getStatDistance() {
            return AdvancedEntityCannon.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_2;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityCannon.this.selectedElement != null ? StringTools.formatSeperated(((WeaponUnit)AdvancedEntityCannon.this.selectedElement).size()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_30;
         }

         public int getStatDistance() {
            return AdvancedEntityCannon.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_23;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedEntityCannon.this.selectedElement != null && AdvancedEntityCannon.this.selectedCollectionManager != null) {
               WeaponUnitModifier var1;
               return (var1 = (WeaponUnitModifier)AdvancedEntityCannon.this.getCombiValue()) != null ? StringTools.formatPointZero(var1.outputDamage) : StringTools.formatPointZero(((WeaponUnit)AdvancedEntityCannon.this.selectedElement).getDamage());
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_28;
            }
         }

         public int getStatDistance() {
            return AdvancedEntityCannon.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_25;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedEntityCannon.this.selectedElement != null && AdvancedEntityCannon.this.selectedCollectionManager != null) {
               WeaponUnitModifier var1;
               return (var1 = (WeaponUnitModifier)AdvancedEntityCannon.this.getCombiValue()) != null ? StringTools.formatPointZero(var1.outputRecoil) : StringTools.formatPointZero(((WeaponUnit)AdvancedEntityCannon.this.selectedElement).getRecoil());
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_24;
            }
         }

         public int getStatDistance() {
            return AdvancedEntityCannon.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_27;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedEntityCannon.this.selectedElement != null && AdvancedEntityCannon.this.selectedCollectionManager != null) {
               WeaponUnitModifier var1;
               return (var1 = (WeaponUnitModifier)AdvancedEntityCannon.this.getCombiValue()) != null ? StringTools.formatPointZero(var1.outputImpactForce) : StringTools.formatPointZero(((WeaponUnit)AdvancedEntityCannon.this.selectedElement).getImpactForce());
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_22;
            }
         }

         public int getStatDistance() {
            return AdvancedEntityCannon.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_29;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityCannon.this.selectedElement != null ? StringTools.formatPointZero(((WeaponUnit)AdvancedEntityCannon.this.selectedElement).getPowerConsumedPerSecondResting()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_19;
         }

         public int getStatDistance() {
            return AdvancedEntityCannon.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_31;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityCannon.this.selectedElement != null ? StringTools.formatPointZero(((WeaponUnit)AdvancedEntityCannon.this.selectedElement).getPowerConsumedPerSecondCharging()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_17;
         }

         public int getStatDistance() {
            return AdvancedEntityCannon.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_33;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityCannon.this.selectedElement != null ? StringTools.formatPointZero(((WeaponUnit)AdvancedEntityCannon.this.selectedElement).getProjectileWidth()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_14;
         }

         public int getStatDistance() {
            return AdvancedEntityCannon.this.getTextDist();
         }
      });
   }

   public WeaponElementManager getEm() {
      return this.getMan() instanceof StationaryManagerContainer ? (WeaponElementManager)((StationaryManagerContainer)this.getMan()).getWeapon().getElementManager() : (WeaponElementManager)((ShipManagerContainer)this.getMan()).getWeapon().getElementManager();
   }

   protected WeaponCombiSettings getWeaponCombiSettingsRaw() {
      return ((WeaponCollectionManager)this.selectedCollectionManager).getWeaponChargeParams();
   }

   public String getSystemNameShort() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_37;
   }

   public String getOutputNameShort() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYCANNON_38;
   }
}
