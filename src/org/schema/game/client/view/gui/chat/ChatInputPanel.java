package org.schema.game.client.view.gui.chat;

import org.schema.game.client.controller.PlayerChatInput;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.data.chat.ChannelRouter;
import org.schema.game.common.data.chat.ChatCallback;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContextPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIInnerTextbox;
import org.schema.schine.input.InputState;
import org.schema.schine.network.client.ClientState;

public class ChatInputPanel extends GUIInputPanel implements GUIActiveInterface {
   public PlayerChatInput playerInput;
   private GUIActiveInterface actInterface;
   private ChatPanel mainPanel;
   private boolean init;
   private ChatCallback chatCallback;

   public ChatInputPanel(String var1, ClientState var2, Object var3, int var4, int var5, ChatCallback var6, PlayerChatInput var7, GUIActiveInterface var8, ChatPanel var9) {
      super("ChatPanel" + var1, var2, var4, var5, var7, var3, "");
      this.mainPanel = var9;
      this.chatCallback = var6;
      this.actInterface = var8;
      this.autoOrientate = false;
      this.playerInput = var7;
      this.setCancelButton(false);
      this.setOkButton(false);
      this.setTitleOnTop(true);
      ((GUIDialogWindow)this.getBackground()).innerHeightSubstraction = 0.0F;
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public boolean isActive() {
      return this.actInterface.isActive();
   }

   public void update(Timer var1) {
   }

   private void createPanel() {
      ChatPanel.createChatPane(((GUIDialogWindow)this.background).getMainContentPane(), this.getState(), this.chatCallback, false, true, this.mainPanel);
      GUIHorizontalButtonTablePane var1;
      (var1 = new GUIHorizontalButtonTablePane(this.getState(), 1, 1, ((GUIInnerTextbox)((GUIDialogWindow)this.background).getMainContentPane().getTextboxes(1).get(1)).getContent())).onInit();
      var1.addButton(0, 0, "MANAGE", (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ChatInputPanel.this.actInterface.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               boolean var5 = ChannelRouter.allowAdminClient(ChatInputPanel.this.getState().getPlayer()) || ChatInputPanel.this.chatCallback.isModerator(ChatInputPanel.this.getState().getPlayer());
               int var6 = 1;
               if (var5 && (ChatInputPanel.this.chatCallback.hasChannelBanList() || ChatInputPanel.this.chatCallback.hasChannelModList())) {
                  ++var6;
               }

               if (var5 && ChatInputPanel.this.chatCallback.hasChannelMuteList()) {
                  ++var6;
               }

               if (var5 && ChatInputPanel.this.chatCallback.hasPossiblePassword()) {
                  ++var6;
               }

               GUIContextPane var3 = new GUIContextPane(ChatInputPanel.this.getState(), 140.0F, (float)(var6 * 25));
               GUIHorizontalButtonTablePane var7;
               (var7 = new GUIHorizontalButtonTablePane(ChatInputPanel.this.getState(), 1, var6, var3)).onInit();
               int var4 = 0;
               if (var5 && ChatInputPanel.this.chatCallback.hasChannelBanList()) {
                  ++var4;
                  var7.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATINPUTPANEL_0, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
                     public void callback(GUIElement var1, MouseEvent var2) {
                        if (var2.pressedLeftMouse()) {
                           PlayerGameOkCancelInput var4;
                           (var4 = new PlayerGameOkCancelInput("CHATMANAGEBANS", ChatInputPanel.this.getState(), 380, 400, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATINPUTPANEL_1, "") {
                              public void onDeactivate() {
                              }

                              public void pressedOK() {
                                 this.deactivate();
                              }
                           }).getInputPanel().onInit();
                           var4.getInputPanel().setCancelButton(false);
                           var4.getInputPanel().setOkButtonText(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATINPUTPANEL_4);
                           GUIAncor var5 = ((GUIDialogWindow)var4.getInputPanel().getBackground()).getMainContentPane().getContent(0);
                           BannedFromChannelScrollableListNew var3;
                           (var3 = new BannedFromChannelScrollableListNew(ChatInputPanel.this.getState(), var5, ChatInputPanel.this.chatCallback, ChatInputPanel.this.mainPanel)).onInit();
                           var5.attach(var3);
                           var4.activate();
                        }

                     }

                     public boolean isOccluded() {
                        return !ChatInputPanel.this.actInterface.isActive();
                     }
                  }, new GUIActivationCallback() {
                     public boolean isVisible(InputState var1) {
                        return true;
                     }

                     public boolean isActive(InputState var1) {
                        return ChatInputPanel.this.chatCallback.hasChannelBanList() && ChatInputPanel.this.chatCallback.getBanned().length > 0 && (ChannelRouter.allowAdminClient(ChatInputPanel.this.getState().getPlayer()) || ChatInputPanel.this.chatCallback.isModerator(ChatInputPanel.this.getState().getPlayer()));
                     }
                  });
               }

               if (var5 && ChatInputPanel.this.chatCallback.hasChannelMuteList()) {
                  var7.addButton(0, var4++, "MUTES", (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
                     public boolean isOccluded() {
                        return !ChatInputPanel.this.actInterface.isActive();
                     }

                     public void callback(GUIElement var1, MouseEvent var2) {
                        if (var2.pressedLeftMouse()) {
                           PlayerGameOkCancelInput var4;
                           (var4 = new PlayerGameOkCancelInput("CHATMANAGEMUTES", ChatInputPanel.this.getState(), 380, 400, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATINPUTPANEL_3, "") {
                              public void pressedOK() {
                                 this.deactivate();
                              }

                              public void onDeactivate() {
                              }
                           }).getInputPanel().onInit();
                           var4.getInputPanel().setCancelButton(false);
                           var4.getInputPanel().setOkButtonText(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATINPUTPANEL_2);
                           GUIAncor var5 = ((GUIDialogWindow)var4.getInputPanel().getBackground()).getMainContentPane().getContent(0);
                           MutedFromChannelScrollableListNew var3;
                           (var3 = new MutedFromChannelScrollableListNew(ChatInputPanel.this.getState(), var5, ChatInputPanel.this.chatCallback, ChatInputPanel.this.mainPanel)).onInit();
                           var5.attach(var3);
                           var4.activate();
                        }

                     }
                  }, new GUIActivationCallback() {
                     public boolean isVisible(InputState var1) {
                        return true;
                     }

                     public boolean isActive(InputState var1) {
                        return ChatInputPanel.this.chatCallback.hasChannelMuteList() && ChatInputPanel.this.chatCallback.getMuted().length > 0 && (ChannelRouter.allowAdminClient(ChatInputPanel.this.getState().getPlayer()) || ChatInputPanel.this.chatCallback.isModerator(ChatInputPanel.this.getState().getPlayer()));
                     }
                  });
               }

               if (var5 && ChatInputPanel.this.chatCallback.hasPossiblePassword()) {
                  var7.addButton(0, var4++, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATINPUTPANEL_5, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
                     public boolean isOccluded() {
                        return !ChatInputPanel.this.actInterface.isActive();
                     }

                     public void callback(GUIElement var1, MouseEvent var2) {
                        if (var2.pressedLeftMouse()) {
                           PlayerGameTextInput var3;
                           (var3 = new PlayerGameTextInput("ccChatPanelPW", ChatInputPanel.this.getState(), 16, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATINPUTPANEL_6, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATINPUTPANEL_7) {
                              public String[] getCommandPrefixes() {
                                 return null;
                              }

                              public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                                 return null;
                              }

                              public void onFailedTextCheck(String var1) {
                              }

                              public void onDeactivate() {
                              }

                              public boolean onInput(String var1) {
                                 ChatInputPanel.this.chatCallback.requestPasswordChangeOnClient(var1);
                                 return true;
                              }
                           }).setMinimumLength(0);
                           var3.activate();
                        }

                     }
                  }, new GUIActivationCallback() {
                     public boolean isVisible(InputState var1) {
                        return true;
                     }

                     public boolean isActive(InputState var1) {
                        return ChatInputPanel.this.chatCallback.hasPossiblePassword() && (ChannelRouter.allowAdminClient(ChatInputPanel.this.getState().getPlayer()) || ChatInputPanel.this.chatCallback.isModerator(ChatInputPanel.this.getState().getPlayer()));
                     }
                  });
               }

               var7.addButton(0, var4, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATINPUTPANEL_8, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
                  public boolean isOccluded() {
                     return !ChatInputPanel.this.actInterface.isActive();
                  }

                  public void callback(GUIElement var1, MouseEvent var2) {
                     if (var2.pressedLeftMouse()) {
                        PlayerGameOkCancelInput var4;
                        (var4 = new PlayerGameOkCancelInput("CHATMANAGEIGNORED", ChatInputPanel.this.getState(), 380, 400, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATINPUTPANEL_9, "") {
                           public void pressedOK() {
                              this.deactivate();
                           }

                           public void onDeactivate() {
                           }
                        }).getInputPanel().onInit();
                        var4.getInputPanel().setCancelButton(false);
                        var4.getInputPanel().setOkButtonText(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATINPUTPANEL_10);
                        GUIAncor var5 = ((GUIDialogWindow)var4.getInputPanel().getBackground()).getMainContentPane().getContent(0);
                        IgnoredByPlayerScrollableListNew var3;
                        (var3 = new IgnoredByPlayerScrollableListNew(ChatInputPanel.this.getState(), var5, ChatInputPanel.this.chatCallback, ChatInputPanel.this.mainPanel)).onInit();
                        var5.attach(var3);
                        var4.activate();
                     }

                  }
               }, new GUIActivationCallback() {
                  public boolean isVisible(InputState var1) {
                     return true;
                  }

                  public boolean isActive(InputState var1) {
                     return ChatInputPanel.this.getState().getPlayer().hasIgnored();
                  }
               });
               var3.attach(var7);
               ChatInputPanel.this.getState().getController().getInputController().setCurrentContextPane(var3);
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      ((GUIInnerTextbox)((GUIDialogWindow)this.background).getMainContentPane().getTextboxes(1).get(1)).getContent().attach(var1);
      GUIInnerTextbox var3 = ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(26);
      GUIHorizontalButtonTablePane var2;
      (var2 = new GUIHorizontalButtonTablePane(this.getState(), 3, 1, var3)).onInit();
      var2.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATINPUTPANEL_11, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ChatInputPanel.this.actInterface.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ChatInputPanel.this.chatCallback.setSticky(!ChatInputPanel.this.chatCallback.isSticky());
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isHighlighted(InputState var1) {
            return ChatInputPanel.this.chatCallback.isSticky();
         }

         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      var2.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATINPUTPANEL_12, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ChatInputPanel.this.actInterface.isActive() || !ChatInputPanel.this.chatCallback.isSticky();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ChatInputPanel.this.chatCallback.setFullSticky(!ChatInputPanel.this.chatCallback.isFullSticky());
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return ChatInputPanel.this.chatCallback.isSticky();
         }

         public boolean isHighlighted(InputState var1) {
            return ChatInputPanel.this.chatCallback.isFullSticky();
         }
      });
      var2.addButton(2, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATINPUTPANEL_13, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ChatInputPanel.this.actInterface.isActive() || !ChatInputPanel.this.chatCallback.canLeave();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && ChatInputPanel.this.chatCallback.canLeave()) {
               ChatInputPanel.this.chatCallback.leave(ChatInputPanel.this.getOwnPlayer());
               ChatInputPanel.this.mainPanel.closeChat(ChatInputPanel.this.chatCallback);
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return ChatInputPanel.this.chatCallback.canLeave();
         }
      });
      var3.attach(var2);
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFactionManager().getFaction(this.getOwnPlayer().getFactionId());
   }

   public float getHeight() {
      return ((GUIDialogWindow)this.background).getHeight();
   }

   public float getWidth() {
      return ((GUIDialogWindow)this.background).getWidth();
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.isTitleOnTop()) {
         this.infoText.setPos((float)((int)(this.background.getWidth() / 2.0F - (float)(this.infoText.getMaxLineWidth() / 2))), -16.0F, 0.0F);
      } else {
         this.infoText.setPos((float)((int)(this.background.getWidth() / 2.0F - (float)(this.infoText.getMaxLineWidth() / 2))), 8.0F, 0.0F);
      }

      ((GUIDialogWindow)this.background).draw();
   }

   public void onInit() {
      super.onInit();
      this.createPanel();
      ((GUIDialogWindow)this.background).activeInterface = this.actInterface;
      this.init = true;
   }
}
