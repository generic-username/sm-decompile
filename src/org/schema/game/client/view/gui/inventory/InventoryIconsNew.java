package org.schema.game.client.view.gui.inventory;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.newdawn.slick.Color;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.manager.ingame.InventorySlotSplitDialog;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventorySlot;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.TooltipProvider;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITexDrawableAreaCustomWrappable;
import org.schema.schine.input.Mouse;

public class InventoryIconsNew extends GUIElement implements TooltipProvider {
   public static final int ICON_SIZE = 64;
   public static final int ICON_SPACING = 4;
   public static final int ICON_TOTAL = 72;
   private final GUIScrollablePanel scrolPane;
   private final InventoryProvider inventoryProvider;
   private GUITexDrawableAreaCustomWrappable slotBg;
   private int height;
   private int width;
   private boolean init;
   private GameClientState state;
   private int maxRows;
   private int iconsPerRow;
   private final Vector4f iconTint = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   private GUITextOverlay[] iconsTexts;
   private GUIOverlay reload;
   private InventorySlotOverlayElement[] icons;
   private InventorySlotOverlayElement draggingIcon;
   private GUITextOverlay draggingIconText;
   private int startRow;
   private int firstIcon;
   private int lastRow;
   private int lastIcon;
   private int lastIconCount;
   private InventorySlot lastSelected;
   public InventoryCallBack invCallback;
   private InventoryToolInterface tools;
   private boolean filtering;
   private static final Vector4f cFiltered = new Vector4f(0.6F, 0.6F, 1.0F, 1.0F);
   private static final Vector4f cUnFiltered = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);

   public InventoryIconsNew(GameClientState var1, GUIScrollablePanel var2, InventoryProvider var3, InventoryCallBack var4, InventoryToolInterface var5) {
      super(var1);
      this.scrolPane = var2;
      this.inventoryProvider = var3;
      this.invCallback = var4;
      this.state = var1;
      this.tools = var5;
      this.setMouseUpdateEnabled(true);
   }

   public static void displaySplitDialog(InventorySlotOverlayElement var0, GameClientState var1, Inventory var2) {
      int var3 = var0.getSlot();
      short var4 = var0.getType();
      int var5 = var0.getMetaId();
      if (var4 == -32768) {
         List var8 = var2.getSubSlots(var3);
         (new InventorySlotSplitDialog(var1, var4, var3, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYICONSNEW_2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYICONSNEW_3, 1, var0, var8, var2)).activate();
      } else if (var4 > 0) {
         (new InventorySlotSplitDialog(var1, var4, var3, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYICONSNEW_4, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYICONSNEW_5, ElementKeyMap.getInfo(var4).getName()), 1, var0, (List)null, var2)).activate();
      } else {
         System.err.println("Cannot split meta items (shouldnt stack yet)");
         MetaObject var6;
         if ((var6 = var1.getMetaObjectManager().getObject(var5)) != null) {
            var1.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().suspend(true);
            DialogInput var7;
            if ((var7 = var6.getEditDialog(var1, var1.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager(), var2)) == null) {
               var1.getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYICONSNEW_0, 0.0F);
            } else {
               var7.activate();
            }
         } else {
            var1.getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYICONSNEW_1, 0.0F);
            var1.getMetaObjectManager().checkAvailable(var5, var1);
         }
      }
   }

   public void cleanUp() {
   }

   public boolean isInside() {
      return super.isInside() && this.scrolPane.isInside();
   }

   public void setInside(boolean var1) {
      super.setInside(var1);
   }

   private boolean isValid(InventorySlot var1) {
      if (ElementKeyMap.isValidType(var1.getType()) && ElementKeyMap.getInfoFast(var1.getType()).getName().toLowerCase(Locale.ENGLISH).contains(this.tools.getText().toLowerCase(Locale.ENGLISH))) {
         return true;
      } else if (var1.metaId != -1 && this.getState().getMetaObjectManager().getObject(var1.metaId) != null && this.getState().getMetaObjectManager().getObject(var1.metaId).getName().toLowerCase(Locale.ENGLISH).contains(this.tools.getText().toLowerCase(Locale.ENGLISH))) {
         return true;
      } else {
         if (var1.isMultiSlot()) {
            for(int var2 = 0; var2 < var1.getSubSlots().size(); ++var2) {
               if (this.isValid((InventorySlot)var1.getSubSlots().get(var2))) {
                  return true;
               }
            }
         }

         return false;
      }
   }

   public void draw() {
      if (this.inventoryProvider.getInventory() != null && !this.inventoryProvider.getInventory().clientRequestedDetails) {
         this.inventoryProvider.getInventory().requestClient(this.state);
      }

      this.calculateDim();
      GlUtil.glPushMatrix();
      this.transform();
      this.checkMouseInside();
      if (this.tools.getText() != null && this.tools.getText().trim().length() > 0) {
         this.filtering = true;
         this.slotBg.setColor(cFiltered);
      } else {
         this.filtering = false;
         this.slotBg.setColor(cUnFiltered);
      }

      this.slotBg.draw();

      assert this.init;

      int var1 = this.getInventory().getActiveSlotsMax() + 1;
      synchronized(this.getState()) {
         this.getState().setSynched();
         if (this.tools.getText() != null && this.tools.getText().trim().length() > 0) {
            Iterator var6 = this.getInventory().getMap().values().iterator();

            while(var6.hasNext()) {
               InventorySlot var4;
               if ((var4 = (InventorySlot)var6.next()).slot >= this.getInventory().getActiveSlotsMax() && this.isValid(var4)) {
                  if (var1 >= this.firstIcon && var1 < this.lastIcon - 1 && var1 - 1 - this.firstIcon >= 0 && var1 - 1 - this.firstIcon < this.icons.length) {
                     this.drawSlot(var4.slot, var1 - 1, var4, this.iconsPerRow);
                  }

                  ++var1;
               }
            }
         } else {
            for(int var3 = this.firstIcon; var3 < this.lastIcon; ++var3) {
               this.drawSlot(var3, var3, (InventorySlot)this.getInventory().getMap().get(var3), this.iconsPerRow);
            }
         }

         this.getState().setUnsynched();
      }

      Iterator var2 = this.getChilds().iterator();

      while(var2.hasNext()) {
         ((AbstractSceneNode)var2.next()).draw();
      }

      GlUtil.glPopMatrix();
   }

   private void drawSlot(int var1, int var2, InventorySlot var3, int var4) {
      InventoryIconsNew.IconSlot var12;
      (var12 = new InventoryIconsNew.IconSlot()).startPointY = 4;
      var12.startPointX = 4;
      var12.slot = var1;
      var12.slotPosToDrawM = var2;
      var12.countPosX = 0.0F;
      var12.countPosY = 0.0F;
      var12.second = false;

      assert var1 >= this.getInventory().getActiveSlotsMax();

      var12.setType((short)0);
      var12.setBuildIconNum(511);
      var12.elementsInInv = 0;
      var12.secondBuildIcon = -1;
      var12.secondType = -1;
      MetaObject var9 = null;
      if (!this.getInventory().isSlotEmpty(var1)) {
         var12.setType(var3.getType());
         var12.elementsInInv = var3.count();
         if (var12.getType() > 0) {
            var12.setBuildIconNum(ElementKeyMap.getInfo(var12.getType()).getBuildIconNum());
         } else if (var12.getType() != -32768) {
            var12.setBuildIconNum(Math.abs(var12.getType()));
            if (var12.getType() < 0) {
               var9 = this.getState().getMetaObjectManager().getObject(var3.metaId);
            }

            if (var12.getType() == MetaObjectManager.MetaObjectType.WEAPON.type && (var9 = this.getState().getMetaObjectManager().getObject(var3.metaId)) != null) {
               var12.setBuildIconNum(var12.getBuildIconNum() + var9.getExtraBuildIconIndex());
            }
         }
      }

      var12.scale = 1.0F;
      var12.object = var9;
      if (var12.getType() != -32768) {
         if (var12.secondBuildIcon >= 0) {
            var12.drawCount = true;
            var12.draw = true;
            var12.checkMouse = true;
            this.doDraw(var12);
            InventoryIconsNew.IconSlot var11;
            (var11 = new InventoryIconsNew.IconSlot(var12)).second = true;
            this.doDraw(var11);
            var12.drawCount = true;
            var12.draw = false;
            var12.checkMouse = true;
            this.doDraw(var12);
         } else {
            var12.drawCount = true;
            var12.draw = true;
            var12.checkMouse = true;
            this.doDraw(var12);
         }
      } else {
         ObjectArrayList var10 = new ObjectArrayList(var3.getSubSlots());

         for(int var5 = 1; var5 < var10.size() && var5 < 5; ++var5) {
            float var6 = (float)((var5 - 1) % 2);
            float var7 = (float)((var5 - 1) / 2);
            InventoryIconsNew.IconSlot var8;
            (var8 = new InventoryIconsNew.IconSlot()).object = var9;
            var8.setBuildIconNum(ElementKeyMap.getInfo(((InventorySlot)var10.get(var5)).getType()).getBuildIconNum());
            var8.slot = var1;
            var8.slotPosToDrawM = var12.slotPosToDrawM;
            var8.setType(((InventorySlot)var10.get(var5)).getType());
            var8.elementsInInv = ((InventorySlot)var10.get(var5)).count();
            var8.subCount = ((InventorySlot)var10.get(var5)).count();
            var8.startPointX = (int)((float)var12.startPointX + var6 * 32.0F);
            var8.startPointY = (int)((float)var12.startPointY + var7 * 32.0F);
            var8.countPosX = var6 * 44.0F;
            var8.countPosY = var7 * 42.0F;
            var8.scale = 0.5F;
            var8.drawCount = true;
            var8.draw = true;
            var8.checkMouse = false;
            this.doDraw(var8);
         }

         InventoryIconsNew.IconSlot var13;
         (var13 = new InventoryIconsNew.IconSlot(var12)).setBuildIconNum(ElementKeyMap.getInfo(((InventorySlot)var10.get(0)).getType()).getBuildIconNum());
         var13.elementsInInv = ((InventorySlot)var10.get(0)).count();
         var13.subCount = ((InventorySlot)var10.get(0)).count();
         var13.setType(((InventorySlot)var10.get(0)).getType());
         var13.startPointX = var12.startPointX + 8;
         var13.startPointY = var12.startPointY + 8;
         var13.drawCount = true;
         var13.draw = true;
         var13.checkMouse = false;
         var13.scale = 0.75F;
         this.doDraw(var13);
         var12.buildIconNum = ElementKeyMap.getInfo(((InventorySlot)var10.get(0)).getType()).getBuildIconNum();
         var12.elementsInInv = ((InventorySlot)var10.get(0)).count();
         var12.subCount = ((InventorySlot)var10.get(0)).count();
         var12.drawCount = false;
         var12.draw = false;
         var12.checkMouse = true;
         this.doDraw(var12);
      }
   }

   private void doDraw(InventoryIconsNew.IconSlot var1) {
      InventorySlotOverlayElement var2 = var1.getIcon();
      GUITextOverlay var3 = var1.getTextIcon();
      this.iconTint.set(1.0F, 1.0F, 1.0F, 1.0F);
      if (var2.getSprite().getTint() == null) {
         var2.getSprite().setTint(new Vector4f());
      }

      if (this.getState().getController().getTutorialMode() != null && this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager().isTreeActive() && this.inventoryProvider == this.getState().getPlayer().getInventory() && this.getState().getController().getTutorialMode().highlightType == var1.getType()) {
         var2.getSprite().getTint().set(0.4F + HudIndicatorOverlay.selectColorValue, 0.9F + HudIndicatorOverlay.selectColorValue, 0.1F + HudIndicatorOverlay.selectColorValue, 0.1F + HudIndicatorOverlay.selectColorValue);
         var3.setColor(new Vector4f(var2.getSprite().getTint()));
      } else {
         var2.getSprite().getTint().set(this.iconTint);
         var3.setColor(this.iconTint);
      }

      var3.setPos(var1.countPosX, var1.countPosY, 0.0F);
      var2.setMouseUpdateEnabled(var1.checkMouse);
      GlUtil.glPushMatrix();
      var2.setSlot(var1.slot);
      var2.setType(var1.getType());
      var2.setMeta(this.getInventory().getMeta(var1.slot));
      var2.setCount(var1.elementsInInv);
      var2.setScale(var1.scale, var1.scale, var1.scale);
      int var4;
      if ((var4 = var2.getCount(false)) <= 0 && var1.subCount <= 0) {
         var1.setBuildIconNum(511);
         var3.getText().set(0, "");
      } else {
         if (var1.subCount > 0) {
            if (var2.getSubSlotType() == var1.getType()) {
               var1.subCount -= var2.getDraggingCount();
            }

            var3.getText().set(0, String.valueOf(var1.subCount));
         } else {
            var3.getText().set(0, String.valueOf(var4));
         }

         if (this.getInventory().isInfinite()) {
            var3.getText().set(0, "");
         }
      }

      if (!var1.drawCount) {
         var3.getText().set(0, "");
      }

      var2.setInvisible(false);
      var2.getPos().y = (float)var1.getRowPos();
      var2.getPos().x = (float)var1.getColumnPos();
      var4 = var1.getBuildIconNum() / 256;
      if (var1.getType() > 0) {
         var2.setLayer(var4);
      } else {
         var2.setLayer(-1);
      }

      var1.setBuildIconNum((short)(var1.getBuildIconNum() % 256));
      var2.getSprite().setSelectedMultiSprite(var1.getBuildIconNum());
      if (var1.draw) {
         var2.draw();
         if (var1.object != null && var1.object.isDrawnOverlayInInventory()) {
            this.reload.setPos(var2.getPos());
            var1.object.drawPossibleOverlay(this.reload, this.getInventory());
         }
      } else if (var1.checkMouse) {
         var2.checkMouseInsideWithTransform();
      }

      GlUtil.glPopMatrix();
      var2.setScale(1.0F, 1.0F, 1.0F);
      if (var2.getSprite().getTint() != null) {
         var2.getSprite().getTint().set(1.0F, 1.0F, 1.0F, 1.0F);
      }

      var3.setColor(1.0F, 1.0F, 1.0F, 1.0F);
      var3.setPos(0.0F, 0.0F, 0.0F);
   }

   public void onInit() {
      this.slotBg = new GUITexDrawableAreaCustomWrappable(this.getState(), Controller.getResLoader().getSprite("inventory-slot-gui-").getMaterial().getTexture(), 0.0F, 0.0F);
      this.slotBg.wrapX = 0.5625F;
      this.slotBg.wrapY = 0.5625F;
      this.draggingIconText = this.getNewTextIcon();
      this.draggingIcon = new InventorySlotOverlayElement(false, this.state, false, this);
      this.draggingIcon.attach(this.draggingIconText);
      this.reload = new GUIOverlay(Controller.getResLoader().getSprite("HUD_Hotbar-8x2-gui-"), this.state);
      this.calculateDim();
      this.init = true;
   }

   private GUITextOverlay getNewTextIcon() {
      GUITextOverlay var1;
      (var1 = new GUITextOverlay(32, 32, FontLibrary.getBoldArial16White(), this.state)).setColor(Color.white);
      var1.setText(new ObjectArrayList(1));
      var1.getText().add("undefined");
      var1.setPos(2.0F, 2.0F, 0.0F);
      return var1;
   }

   private void reinitIcon(int var1) {
      this.icons[var1] = new InventorySlotOverlayElement(false, this.state, true, this);
      this.iconsTexts[var1] = this.getNewTextIcon();
      this.icons[var1].attach(this.iconsTexts[var1]);
   }

   private void reinitIcons(int var1) {
      this.icons = new InventorySlotOverlayElement[var1];
      this.iconsTexts = new GUITextOverlay[var1];

      for(int var2 = 0; var2 < var1; ++var2) {
         this.reinitIcon(var2);
      }

   }

   public void calculateDim() {
      this.scrolPane.getScrollX();
      int var1 = (int)this.scrolPane.getWidth();
      int var2 = (int)this.scrolPane.getScrollY();
      int var3 = (int)this.scrolPane.getHeight();
      this.iconsPerRow = var1 / 72;
      if (!this.getInventory().isEmpty()) {
         assert this.inventoryProvider != null;

         assert this.getInventory().getMap() != null;

         this.maxRows = (int)Math.ceil((double)Math.max(0, this.getInventory().getMap().lastIntKey() - this.getInventory().getActiveSlotsMax()) / (double)this.iconsPerRow) + 1;
         this.width = this.iconsPerRow * 72;
         this.height = this.maxRows * 72;
         this.slotBg.setWidth(this.width);
         this.slotBg.setHeight(this.height);
      } else {
         this.maxRows = 1;
         this.width = this.iconsPerRow * 72;
         this.height = this.maxRows * 72;
         this.slotBg.setWidth(this.width);
         this.slotBg.setHeight(this.height);
      }

      this.startRow = (int)Math.floor((double)var2 / 72.0D);
      this.firstIcon = this.startRow * this.iconsPerRow + this.getInventory().getActiveSlotsMax();
      this.lastRow = (int)Math.ceil(((double)var2 + (double)var3) / 72.0D);
      this.lastIcon = (this.lastRow + 1) * this.iconsPerRow + this.getInventory().getActiveSlotsMax();
      if ((var1 = this.lastIcon - this.firstIcon) > this.lastIconCount) {
         this.reinitIcons(var1);
         this.lastIconCount = var1;
      }

   }

   public void drawToolTip() {
      GlUtil.glPushMatrix();
      if (this.icons != null) {
         for(int var1 = 0; var1 < this.icons.length; ++var1) {
            this.icons[var1].drawToolTip();
         }
      }

      GlUtil.glPopMatrix();
   }

   public float getHeight() {
      return (float)this.height;
   }

   public float getWidth() {
      return (float)this.width;
   }

   public GameClientState getState() {
      return this.state;
   }

   public Inventory getInventory() {
      return this.inventoryProvider.getInventory();
   }

   public void drawDragging(InventorySlotOverlayElement var1) {
      GUIElement.enableOrthogonal();
      this.draggingIcon.setPos((float)(Mouse.getX() - var1.getDragPosX()), (float)(GLFrame.getHeight() - Mouse.getY() - var1.getDragPosY()), 0.0F);
      this.draggingIcon.setScale(1.0F, 1.0F, 1.0F);
      this.draggingIconText.setScale(1.0F, 1.0F, 1.0F);
      short var2;
      int var4;
      if ((var2 = var1.getType()) == -32768 && var1.getSubSlotType() == 0) {
         assert var1.getDraggingInventory() != null;

         ObjectArrayList var12 = new ObjectArrayList(var1.getDraggingInventory().getSubSlots(var1.getSlot()));
         boolean var11 = false;

         int var10;
         Vector3f var10000;
         for(int var5 = 1; var5 < var12.size() && var5 < 5; ++var5) {
            this.draggingIcon.setPos((float)(Mouse.getX() - var1.getDragPosX()), (float)(GLFrame.getHeight() - Mouse.getY() - var1.getDragPosY()), 0.0F);
            float var6 = (float)((var5 - 1) % 2);
            float var7 = (float)((var5 - 1) / 2);
            InventorySlot var8;
            var10 = (var4 = ElementKeyMap.getInfo((var8 = (InventorySlot)var12.get(var5)).getType()).getBuildIconNum()) / 256;
            var4 %= 256;
            this.draggingIcon.setLayer(var10);
            this.draggingIcon.setSpriteSubIndex(var4);
            this.draggingIconText.getText().set(0, String.valueOf(var8.count()));
            if (var8.isInfinite()) {
               this.draggingIconText.getText().set(0, "");
            }

            this.draggingIconText.setPos(var6 * 44.0F, var7 * 42.0F, 0.0F);
            this.draggingIcon.setScale(0.5F, 0.5F, 0.5F);
            var10000 = this.draggingIcon.getPos();
            var10000.x += var6 * 32.0F;
            var10000 = this.draggingIcon.getPos();
            var10000.y += var7 * 32.0F;
            this.draggingIcon.draw();
            this.draggingIconText.setPos(0.0F, 0.0F, 0.0F);
         }

         this.draggingIcon.setPos((float)(Mouse.getX() - var1.getDragPosX()), (float)(GLFrame.getHeight() - Mouse.getY() - var1.getDragPosY()), 0.0F);
         InventorySlot var13;
         var10 = (var4 = ElementKeyMap.getInfo((var13 = (InventorySlot)var12.get(0)).getType()).getBuildIconNum()) / 256;
         var4 %= 256;
         this.draggingIcon.setLayer(var10);
         this.draggingIcon.setSpriteSubIndex(var4);
         this.draggingIconText.getText().set(0, String.valueOf(var13.count()));
         if (var13.isInfinite()) {
            this.draggingIconText.getText().set(0, "");
         }

         this.draggingIcon.setScale(0.75F, 0.75F, 0.75F);
         var10000 = this.draggingIcon.getPos();
         var10000.x += 8.0F;
         var10000 = this.draggingIcon.getPos();
         var10000.y += 8.0F;
         this.draggingIcon.draw();
         GUIElement.disableOrthogonal();
      } else {
         if (var2 == -32768 && var1.getSubSlotType() != 0) {
            var2 = var1.getSubSlotType();
         }

         int var3 = 0;
         if (var2 > 0) {
            var4 = (var3 = ElementKeyMap.getInfo(var2).getBuildIconNum()) / 256;
            this.draggingIcon.setLayer(var4);
         } else if (var2 < 0) {
            var3 = Math.abs(var2);
            this.draggingIcon.setLayer(-1);
         }

         if (var2 == MetaObjectManager.MetaObjectType.WEAPON.type) {
            var4 = this.getInventory().getMeta(var1.getSlot());
            MetaObject var9;
            if ((var9 = this.getState().getMetaObjectManager().getObject(var4)) == null) {
               GUIElement.disableOrthogonal();
               return;
            }

            var3 += var9.getExtraBuildIconIndex();
         }

         var3 %= 256;
         this.draggingIcon.setSpriteSubIndex(var3);
         this.draggingIconText.getText().set(0, String.valueOf(var1.getCount(true)));
         if (var1.getDraggingInventory().isInfinite()) {
            this.draggingIconText.getText().set(0, "");
         }

         this.draggingIcon.draw();
         GUIElement.disableOrthogonal();
      }
   }

   public InventorySlot getLastSelected() {
      return this.lastSelected;
   }

   public void setLastSelected(InventorySlot var1) {
      if (var1 != null) {
         this.lastSelected = new InventorySlot(var1, 0);
      } else {
         this.lastSelected = null;
      }
   }

   public boolean isCurrentlyFilteringInventory() {
      return this.filtering;
   }

   public void clearFilter() {
      this.tools.clearFilter();
   }

   class IconSlot {
      InventorySlotOverlayElement elem;
      int slotPosToDrawM;
      public int slot;
      public int startPointY;
      public int elementsInInv;
      private short type;
      public int secondBuildIcon;
      private int buildIconNum;
      public short secondType;
      public int startPointX;
      public float scale;
      public boolean drawCount;
      public float countPosX;
      public float countPosY;
      public int subCount;
      public boolean checkMouse;
      public boolean draw;
      public MetaObject object;
      public boolean second;

      public IconSlot() {
      }

      public IconSlot(InventoryIconsNew.IconSlot var2) {
         this.elem = var2.elem;
         this.slotPosToDrawM = var2.slotPosToDrawM;
         this.slot = var2.slot;
         this.startPointY = var2.startPointY;
         this.elementsInInv = var2.elementsInInv;
         this.setType(var2.getType());
         this.secondBuildIcon = var2.secondBuildIcon;
         this.setBuildIconNum(var2.getBuildIconNum());
         this.secondType = var2.secondType;
         this.startPointX = var2.startPointX;
         this.scale = var2.scale;
         this.drawCount = var2.drawCount;
         this.countPosX = var2.countPosX;
         this.countPosY = var2.countPosY;
         this.subCount = var2.subCount;
         this.checkMouse = var2.checkMouse;
         this.draw = var2.draw;
         this.object = var2.object;
         this.second = var2.second;
      }

      public int getRow() {
         return (this.slotPosToDrawM - InventoryIconsNew.this.getInventory().getActiveSlotsMax()) / InventoryIconsNew.this.iconsPerRow;
      }

      public int getColumn() {
         return (this.slotPosToDrawM - InventoryIconsNew.this.getInventory().getActiveSlotsMax()) % InventoryIconsNew.this.iconsPerRow;
      }

      public int getColumnPos() {
         return this.startPointX + this.getColumn() * 72;
      }

      public int getRowPos() {
         return this.startPointY + this.getRow() * 72;
      }

      public short getType() {
         return this.second ? this.secondType : this.type;
      }

      public void setType(short var1) {
         this.type = var1;
      }

      public int getBuildIconNum() {
         return this.second ? this.secondBuildIcon : this.buildIconNum;
      }

      public void setBuildIconNum(int var1) {
         if (this.second) {
            this.secondBuildIcon = var1;
         } else {
            this.buildIconNum = var1;
         }
      }

      public InventorySlotOverlayElement getIcon() {
         return InventoryIconsNew.this.icons[this.slotPosToDrawM - InventoryIconsNew.this.firstIcon];
      }

      public GUITextOverlay getTextIcon() {
         return InventoryIconsNew.this.iconsTexts[this.slotPosToDrawM - InventoryIconsNew.this.firstIcon];
      }
   }
}
