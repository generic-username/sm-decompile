package org.schema.game.client.view.gui.inventory;

import org.schema.game.client.controller.manager.ingame.InventoryControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUITextInputBar;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.input.InputState;

public class InventoryIconTools extends GUIAncor implements InventoryToolInterface, GUICallback {
   private GUITextOverlayTable info;
   private GUITextInputBar searchBar;
   private GUITextButton reset;

   public InventoryIconTools(InputState var1) {
      super(var1);
      this.searchBar = new GUITextInputBar(var1, this, 30);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      InventoryControllerManager var3 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager();
      if (var2.pressedLeftMouse()) {
         var3.setSearchActive(true);
      }

      var3.setInSearchbar(true);
   }

   public void draw() {
      this.searchBar.setInGUIDraw(true);
      super.draw();
      this.searchBar.setInGUIDraw(false);
   }

   public void onInit() {
      super.onInit();
      this.searchBar.onInit();
      this.reset = new GUITextButton(this.getState(), 15, 15, "x", new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               InventoryIconTools.this.clearFilter();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }) {
         public void draw() {
            if (InventoryIconTools.this.getText().length() > 0) {
               super.draw();
            }

         }
      };
      this.reset.setTextPos(3, -3);
      this.info = new GUITextOverlayTable(45, 15, this.getState());
      this.info.setTextSimple("Search: ");
      this.searchBar.getPos().x = this.info.getWidth();
      this.reset.getPos().x = this.info.getWidth() + this.searchBar.getWidth() + 2.0F;
      this.attach(this.info);
      this.attach(this.searchBar);
      this.attach(this.reset);
   }

   public String getText() {
      return this.searchBar.getText();
   }

   public boolean isOccluded() {
      return false;
   }

   public boolean isActiveInventory(InventoryIconsNew var1) {
      return true;
   }

   public void clearFilter() {
      this.searchBar.reset();
   }
}
