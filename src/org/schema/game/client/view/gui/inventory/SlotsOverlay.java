package org.schema.game.client.view.gui.inventory;

import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.input.InputState;

public class SlotsOverlay extends GUIOverlay {
   private GUIScrollablePanel scroller;

   public SlotsOverlay(InputState var1, GUIScrollablePanel var2) {
      super(Controller.getResLoader().getSprite("inventory-slots-gui-"), var1);
      this.scroller = var2;
      this.setMouseUpdateEnabled(true);
   }

   public void draw() {
      this.setSprite(Controller.getResLoader().getSprite("inventory-slots-gui-"));
      super.draw();
   }

   public float getHeight() {
      return 360.0F;
   }

   public float getWidth() {
      return 503.0F;
   }

   public boolean isInside() {
      return super.isInside() && this.scroller.isInside();
   }

   public void setInventory(Inventory var1) {
   }
}
