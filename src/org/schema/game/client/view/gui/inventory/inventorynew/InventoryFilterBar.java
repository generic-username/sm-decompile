package org.schema.game.client.view.gui.inventory.inventorynew;

import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.input.InputState;

public class InventoryFilterBar extends GUIActivatableTextBar {
   public InventoryFilterBar(InputState var1, GUIElement var2, TextCallback var3, OnInputChangedCallback var4) {
      super(var1, FontLibrary.FontSize.SMALL, var2, var3, var4);
      this.icon = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 16px-8x8-gui-"), var1);
      this.icon.setSpriteSubIndex(1);
      this.icon.onInit();
      this.setClearButtonEnabled(true);
   }

   public InventoryFilterBar(InputState var1, String var2, GUIElement var3, TextCallback var4, OnInputChangedCallback var5) {
      super(var1, FontLibrary.FontSize.SMALL, var2, var3, var4, var5);
      this.icon = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 16px-8x8-gui-"), var1);
      this.icon.setSpriteSubIndex(1);
      this.icon.onInit();
      this.setClearButtonEnabled(true);
   }
}
