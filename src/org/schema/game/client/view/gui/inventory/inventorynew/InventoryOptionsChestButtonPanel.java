package org.schema.game.client.view.gui.inventory.inventorynew;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.Observable;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerBlockCategoryDropdownInputNew;
import org.schema.game.client.controller.PlayerBlockTypeDropdownInputNew;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.inventory.InventoryIconsNew;
import org.schema.game.client.view.gui.inventory.InventoryToolInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCategory;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.inventory.NetworkInventoryInterface;
import org.schema.game.common.data.player.inventory.StashInventory;
import org.schema.game.network.objects.LongStringPair;
import org.schema.game.network.objects.ShortIntPair;
import org.schema.game.network.objects.remote.RemoteLongString;
import org.schema.game.network.objects.remote.RemoteShortIntPair;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalProgressBar;
import org.schema.schine.input.InputState;

public class InventoryOptionsChestButtonPanel extends GUIAncor implements InventoryToolInterface {
   private String inventoryFilterText = "";
   private SecondaryInventoryPanelNew panel;
   private InventoryPanelNew mainPanel;
   private StashInventory inventory;
   private boolean inventoryActive;
   private SegmentController segmentController;
   private SegmentPiece pointUnsave;
   private InventoryFilterBar searchBar;

   public InventoryOptionsChestButtonPanel(InputState var1, SecondaryInventoryPanelNew var2, InventoryPanelNew var3, StashInventory var4) {
      super(var1);
      this.panel = var2;
      this.mainPanel = var3;
      this.inventory = var4;
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFaction();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public void onInit() {
      this.searchBar = new InventoryFilterBar(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_0, this, new TextCallback() {
         public String[] getCommandPrefixes() {
            return null;
         }

         public void onTextEnter(String var1, boolean var2, boolean var3) {
         }

         public void onFailedTextCheck(String var1) {
         }

         public void newLine() {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            InventoryOptionsChestButtonPanel.this.inventoryFilterText = var1;
            return var1;
         }
      });
      this.searchBar.getPos().x = 1.0F;
      this.searchBar.getPos().y = 2.0F;
      this.searchBar.leftDependentHalf = true;
      this.searchBar.onInit();
      this.attach(this.searchBar);
      this.segmentController = ((ManagerContainer)this.inventory.getInventoryHolder()).getSegmentController();
      GUIInventoryOtherDropDown var1;
      (var1 = new GUIInventoryOtherDropDown(this.getState(), this, this.segmentController, new DropDownCallback() {
         public void onSelectionChanged(GUIListElement var1) {
            if (var1.getContent().getUserPointer() != null && var1.getContent().getUserPointer() instanceof StashInventory) {
               StashInventory var2 = (StashInventory)var1.getContent().getUserPointer();
               InventoryOptionsChestButtonPanel.this.panel.getPlayerInput().deactivate();
               InventoryOptionsChestButtonPanel.this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager().setSecondInventory(var2);
               InventoryOptionsChestButtonPanel.this.mainPanel.update((Observable)null, (Object)null);
            }

         }
      })).onInit();
      var1.rightDependentHalf = true;
      var1.getPos().y = 2.0F;
      GUIHorizontalButtonTablePane var2;
      (var2 = new GUIHorizontalButtonTablePane(this.getState(), 2, 1, this)).onInit();
      this.pointUnsave = this.segmentController.getSegmentBuffer().getPointUnsave(this.inventory.getParameter());
      var2.addButton(0, 0, new Object() {
         public String toString() {
            InventoryOptionsChestButtonPanel.this.pointUnsave.refresh();
            InventoryOptionsChestButtonPanel.this.inventoryActive = InventoryOptionsChestButtonPanel.this.pointUnsave.isActive();
            return InventoryOptionsChestButtonPanel.this.inventoryActive ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_1 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_2;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               InventoryOptionsChestButtonPanel.this.pointUnsave.refresh();
               long var3 = ElementCollection.getEncodeActivation(InventoryOptionsChestButtonPanel.this.pointUnsave, true, !InventoryOptionsChestButtonPanel.this.pointUnsave.isActive(), false);
               InventoryOptionsChestButtonPanel.this.pointUnsave.getSegment().getSegmentController().sendBlockActivation(var3);
            }

         }

         public boolean isOccluded() {
            return !InventoryOptionsChestButtonPanel.this.panel.isActive();
         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }

         public boolean isHighlighted(InputState var1) {
            return InventoryOptionsChestButtonPanel.this.inventoryActive;
         }
      });
      var2.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_3, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !InventoryOptionsChestButtonPanel.this.panel.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               InventoryOptionsChestButtonPanel.popupProductionDialog(InventoryOptionsChestButtonPanel.this.getState(), InventoryOptionsChestButtonPanel.this.segmentController, InventoryOptionsChestButtonPanel.this.inventory, InventoryOptionsChestButtonPanel.this.pointUnsave);
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      var2.getPos().x = 1.0F;
      var2.getPos().y = 26.0F;
      this.attach(var2);
      GUIHorizontalProgressBar var4;
      (var4 = new GUIHorizontalProgressBar(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_4, this) {
         public float getValue() {
            if (InventoryOptionsChestButtonPanel.this.inventoryActive) {
               this.text = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_5;
               return (float)((double)(InventoryOptionsChestButtonPanel.this.getState().getController().getServerRunningTime() % 10000L) / 10000.0D);
            } else {
               this.text = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_6;
               return 0.0F;
            }
         }
      }).getColor().set(InventoryPanelNew.PROGRESS_COLOR);
      var4.onInit();
      var4.getPos().x = 1.0F;
      var4.getPos().y = 50.0F;
      this.attach(var4);
      (var2 = new GUIHorizontalButtonTablePane(this.getState(), 2, 1, this)).onInit();
      var2.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_7, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !InventoryOptionsChestButtonPanel.this.panel.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               final PlayerGameTextInput var3;
               (var3 = new PlayerGameTextInput("RENAME_INVENTORY", InventoryOptionsChestButtonPanel.this.getState(), 10, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_23, InventoryOptionsChestButtonPanel.this.inventory.getCustomName()), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_24) {
                  public void onDeactivate() {
                  }

                  public void onFailedTextCheck(String var1) {
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public boolean onInput(String var1) {
                     if (((ManagedSegmentController)InventoryOptionsChestButtonPanel.this.segmentController).getManagerContainer().getNamedInventoriesClient().size() < 16) {
                        ((ManagedSegmentController)InventoryOptionsChestButtonPanel.this.segmentController).getManagerContainer().getInventoryNetworkObject().getInventoryCustomNameModBuffer().add(new RemoteLongString(new LongStringPair(InventoryOptionsChestButtonPanel.this.inventory.getParameter(), var1.trim()), false));
                     } else {
                        this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_8, 0.0F);
                     }

                     return true;
                  }
               }).getInputPanel().onInit();
               GUITextButton var4 = new GUITextButton(InventoryOptionsChestButtonPanel.this.getState(), 100, 20, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_9, new GUICallback() {
                  public boolean isOccluded() {
                     return !var3.isActive();
                  }

                  public void callback(GUIElement var1, MouseEvent var2) {
                     if (var2.pressedLeftMouse()) {
                        ((ManagedSegmentController)InventoryOptionsChestButtonPanel.this.segmentController).getManagerContainer().getInventoryNetworkObject().getInventoryCustomNameModBuffer().add(new RemoteLongString(new LongStringPair(InventoryOptionsChestButtonPanel.this.inventory.getParameter(), ""), false));
                        var3.deactivate();
                     }

                  }
               }) {
                  public void draw() {
                     this.setPos(var3.getInputPanel().getButtonCancel().getPos().x + var3.getInputPanel().getButtonCancel().getWidth() + 15.0F, var3.getInputPanel().getButtonCancel().getPos().y, 0.0F);
                     super.draw();
                  }
               };
               var3.getInputPanel().getBackground().attach(var4);
               var3.activate();
            }

         }
      }, (GUIActivationCallback)null);
      var2.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_22, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !InventoryOptionsChestButtonPanel.this.panel.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (!InventoryOptionsChestButtonPanel.this.getState().getPlayer().isInventoryPersonalCargo(InventoryOptionsChestButtonPanel.this.inventory)) {
                  if (InventoryOptionsChestButtonPanel.this.inventory.getInventoryHolder() instanceof ManagerContainer) {
                     SegmentPiece var3 = ((ManagerContainer)InventoryOptionsChestButtonPanel.this.inventory.getInventoryHolder()).getSegmentController().getSegmentBuffer().getPointUnsave(InventoryOptionsChestButtonPanel.this.inventory.getParameter());
                     InventoryOptionsChestButtonPanel.this.getState().getPlayer().requestCargoInventoryChange(var3);
                     return;
                  }

                  assert false;
               } else {
                  InventoryOptionsChestButtonPanel.this.getState().getPlayer().requestCargoInventoryChange((SegmentPiece)null);
               }
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return InventoryOptionsChestButtonPanel.this.panel.isActive();
         }

         public boolean isHighlighted(InputState var1) {
            synchronized(InventoryOptionsChestButtonPanel.this.getState()) {
               InventoryOptionsChestButtonPanel.this.getState().setSynched();

               boolean var2;
               try {
                  var2 = InventoryOptionsChestButtonPanel.this.getState().getPlayer().isInventoryPersonalCargo(InventoryOptionsChestButtonPanel.this.inventory);
               } finally {
                  InventoryOptionsChestButtonPanel.this.getState().setUnsynched();
               }

               return var2;
            }
         }
      });
      var2.getPos().x = 1.0F;
      var2.getPos().y = 77.0F;
      this.attach(var2);
      this.attach(var1);
      GUIHorizontalArea var3;
      (var3 = new GUIHorizontalArea(this.getState(), GUIHorizontalArea.HButtonType.TEXT_FIELD, 10) {
         public void draw() {
            this.setWidth(InventoryOptionsChestButtonPanel.this.getWidth());
            super.draw();
         }
      }).getPos().x = 1.0F;
      var3.getPos().y = 102.0F;
      this.attach(var3);
      GUIScrollablePanel var5;
      (var5 = new GUIScrollablePanel(24.0F, 24.0F, this, this.getState())).setScrollable(GUIScrollablePanel.SCROLLABLE_NONE);
      var5.setLeftRightClipOnly = true;
      var5.dependent = var3;
      GUITextOverlay var6;
      (var6 = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM, this.getState()) {
         public void draw() {
            if (InventoryOptionsChestButtonPanel.this.inventory.isOverCapacity()) {
               this.setColor(1.0F, 0.3F, 0.3F, 1.0F);
            } else {
               this.setColor(1.0F, 1.0F, 1.0F, 1.0F);
            }

            super.draw();
         }
      }).setTextSimple(new Object() {
         public String toString() {
            return InventoryOptionsChestButtonPanel.this.inventory.getVolumeString();
         }
      });
      var6.setPos(4.0F, 4.0F, 0.0F);
      var3.attach(var6);
   }

   public boolean isActive() {
      return this.panel.isActive();
   }

   public String getText() {
      return this.inventoryFilterText;
   }

   public boolean isActiveInventory(InventoryIconsNew var1) {
      return this.mainPanel.isInventoryActive(var1);
   }

   public static void popupProductionDialog(final GameClientState var0, final SegmentController var1, final StashInventory var2, final SegmentPiece var3) {
      PlayerGameOkCancelInput var4;
      (var4 = new PlayerGameOkCancelInput("PRODUCTION_POPUP_MAIN", var0, 500, 400, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_10, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_11) {
         public final boolean isOccluded() {
            return false;
         }

         public final void onDeactivate() {
         }

         public final void pressedOK() {
            this.deactivate();
         }
      }).getInputPanel().setCancelButton(false);
      var4.getInputPanel().onInit();
      var4.getInputPanel().getContent().setHeight(80);
      ((GUIDialogWindow)var4.getInputPanel().getBackground()).getMainContentPane().setTextBoxHeightLast(80);
      ((GUIDialogWindow)var4.getInputPanel().getBackground()).getMainContentPane().addNewTextBox(100);
      final InventoryStashProductionScrollableListNew var5;
      (var5 = new InventoryStashProductionScrollableListNew(var0, ((GUIDialogWindow)var4.getInputPanel().getBackground()).getMainContentPane().getContent(1), var2, var3)).onInit();
      ((GUIDialogWindow)var4.getInputPanel().getBackground()).getMainContentPane().getContent(1).attach(var5);
      GUITextButton var6 = new GUITextButton(var0, 80, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_12, new GUICallback() {
         public final boolean isOccluded() {
            return false;
         }

         public final void callback(GUIElement var1x, MouseEvent var2x) {
            if (var2x.pressedLeftMouse() && var0.getPlayerInputs().size() == 1) {
               (new PlayerBlockTypeDropdownInputNew("PRODUCTION_POPUP_PICK", var0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_13, 2, 0, true) {
                  public void onAdditionalElementOk(Object var1x) {
                  }

                  public void onOk(ElementInformation var1x) {
                     int var2x = Math.max(1, this.getNumberValue(0));
                     int var3x = Math.max(0, this.getNumberValue(1));
                     var2.getFilter().filter.put(var1x.getId(), var2x);
                     var2.getFilter().fillUpTo.put(var1x.getId(), var3x);
                     ((NetworkInventoryInterface)var1.getNetworkObject()).getInventoryFilterBuffer().add(new RemoteShortIntPair(new ShortIntPair(var3.getAbsoluteIndex(), var1x.getId(), var2x), var1.isOnServer()));
                     ((NetworkInventoryInterface)var1.getNetworkObject()).getInventoryFillBuffer().add(new RemoteShortIntPair(new ShortIntPair(var3.getAbsoluteIndex(), var1x.getId(), var3x), var1.isOnServer()));
                     var5.flagDirty();
                  }

                  public void onOkMeta(MetaObject var1x) {
                     short var2x = var1x.getObjectBlockID();
                     if (MetaObjectManager.subIdTypes.contains(var2x)) {
                        var2x = (short)((short)(var2x - Math.abs(var2x << 8)) - var1x.getSubObjectId());
                     }

                     int var4 = Math.max(1, this.getNumberValue(0));
                     int var3x = Math.max(0, this.getNumberValue(1));
                     var2.getFilter().filter.put(var2x, var4);
                     var2.getFilter().fillUpTo.put(var2x, var3x);
                     ((NetworkInventoryInterface)var1.getNetworkObject()).getInventoryFilterBuffer().add(new RemoteShortIntPair(new ShortIntPair(var3.getAbsoluteIndex(), var2x, var4), var1.isOnServer()));
                     ((NetworkInventoryInterface)var1.getNetworkObject()).getInventoryFillBuffer().add(new RemoteShortIntPair(new ShortIntPair(var3.getAbsoluteIndex(), var2x, var3x), var1.isOnServer()));
                     var5.flagDirty();
                  }
               }).activate();
            }

         }
      });
      GUITextButton var8 = new GUITextButton(var0, 100, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_14, new GUICallback() {
         public final boolean isOccluded() {
            return false;
         }

         public final void callback(GUIElement var1x, MouseEvent var2x) {
            if (var2x.pressedLeftMouse() && var0.getPlayerInputs().size() == 1) {
               (new PlayerBlockCategoryDropdownInputNew("PRODUCTION_POPUP_PICK_CAT", var0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_15, 2, 0, true) {
                  public void onAdditionalElementOk(Object var1x) {
                  }

                  public void onOk(ElementCategory var1x) {
                     Iterator var5x = var1x.getInfoElementsRecursive(new ObjectArrayList()).iterator();

                     while(var5x.hasNext()) {
                        ElementInformation var2x = (ElementInformation)var5x.next();
                        int var3x = Math.max(1, this.getNumberValue(0));
                        int var4 = Math.max(0, this.getNumberValue(1));
                        var2.getFilter().filter.put(var2x.getId(), var3x);
                        var2.getFilter().fillUpTo.put(var2x.getId(), var4);
                        ((NetworkInventoryInterface)var1.getNetworkObject()).getInventoryFilterBuffer().add(new RemoteShortIntPair(new ShortIntPair(var3.getAbsoluteIndex(), var2x.getId(), var3x), var1.isOnServer()));
                        ((NetworkInventoryInterface)var1.getNetworkObject()).getInventoryFillBuffer().add(new RemoteShortIntPair(new ShortIntPair(var3.getAbsoluteIndex(), var2x.getId(), var4), var1.isOnServer()));
                     }

                     var5.flagDirty();
                  }

                  public void onOkMeta() {
                     MetaObjectManager.MetaObjectType[] var1x;
                     int var2x = (var1x = MetaObjectManager.MetaObjectType.values()).length;

                     for(int var3x = 0; var3x < var2x; ++var3x) {
                        MetaObjectManager.MetaObjectType var4;
                        short var5x = (var4 = var1x[var3x]).type;
                        int var6;
                        if (MetaObjectManager.subIdTypes.contains(var5x)) {
                           short[] var11;
                           var6 = (var11 = MetaObjectManager.getSubTypes(var4)).length;

                           for(int var8 = 0; var8 < var6; ++var8) {
                              short var9 = var11[var8];
                              short var10000 = var4.type;
                              var5x = (short)((short)(var10000 - Math.abs(var10000 << 8)) - var9);
                              int var12 = Math.max(1, this.getNumberValue(0));
                              int var10 = Math.max(0, this.getNumberValue(1));
                              var2.getFilter().filter.put(var5x, var12);
                              var2.getFilter().fillUpTo.put(var5x, var10);
                              ((NetworkInventoryInterface)var1.getNetworkObject()).getInventoryFilterBuffer().add(new RemoteShortIntPair(new ShortIntPair(var3.getAbsoluteIndex(), var5x, var12), var1.isOnServer()));
                              ((NetworkInventoryInterface)var1.getNetworkObject()).getInventoryFillBuffer().add(new RemoteShortIntPair(new ShortIntPair(var3.getAbsoluteIndex(), var5x, var10), var1.isOnServer()));
                           }
                        } else {
                           var6 = Math.max(1, this.getNumberValue(0));
                           int var7 = Math.max(0, this.getNumberValue(1));
                           var2.getFilter().filter.put(var5x, var6);
                           var2.getFilter().fillUpTo.put(var5x, var7);
                           ((NetworkInventoryInterface)var1.getNetworkObject()).getInventoryFilterBuffer().add(new RemoteShortIntPair(new ShortIntPair(var3.getAbsoluteIndex(), var5x, var6), var1.isOnServer()));
                           ((NetworkInventoryInterface)var1.getNetworkObject()).getInventoryFillBuffer().add(new RemoteShortIntPair(new ShortIntPair(var3.getAbsoluteIndex(), var5x, var7), var1.isOnServer()));
                        }
                     }

                     var5.flagDirty();
                  }
               }).activate();
            }

         }
      });
      GUITextButton var9 = new GUITextButton(var0, 120, 24, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_16, new GUICallback() {
         public final boolean isOccluded() {
            return false;
         }

         public final void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && var0.getPlayerInputs().size() == 1) {
               var5.apply((short)0, 0, 0, true);
               var5.flagDirty();
            }

         }
      });
      GUITextButton var7 = new GUITextButton(var0, 60, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_17, new GUICallback() {
         public final boolean isOccluded() {
            return false;
         }

         public final void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && var0.getPlayerInputs().size() == 1) {
               (new PlayerGameTextInput("PRODUCTION_POPUP_COUNT_CHA", var0, 8, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_18, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_19, "1") {
                  public boolean isOccluded() {
                     return false;
                  }

                  public void onFailedTextCheck(String var1) {
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public boolean onInput(String var1) {
                     try {
                        final int var3;
                        if ((var3 = Integer.parseInt(var1)) >= 0) {
                           (new PlayerGameTextInput("PRODUCTION_POPUP_COUNT_CHA", var0, 8, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_25, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_26, "1") {
                              public boolean isOccluded() {
                                 return false;
                              }

                              public void onFailedTextCheck(String var1) {
                              }

                              public String handleAutoComplete(String var1, TextCallback var2, String var3x) throws PrefixNotFoundException {
                                 return null;
                              }

                              public String[] getCommandPrefixes() {
                                 return null;
                              }

                              public boolean onInput(String var1) {
                                 try {
                                    int var3x = Integer.parseInt(var1);
                                    if (var3 >= 0) {
                                       var5.apply((short)0, var3, Math.max(0, var3x), true);
                                       var5.flagDirty();
                                       return true;
                                    }

                                    this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_20);
                                 } catch (NumberFormatException var2) {
                                    this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_21);
                                 }

                                 return false;
                              }

                              public void onDeactivate() {
                              }
                           }).activate();
                           return true;
                        }

                        this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_27);
                     } catch (NumberFormatException var2) {
                        this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSCHESTBUTTONPANEL_28);
                     }

                     return false;
                  }

                  public void onDeactivate() {
                  }
               }).activate();
            }

         }
      });
      var6.setPos(2.0F, 40.0F, 0.0F);
      var8.setPos(90.0F, 40.0F, 0.0F);
      var7.setPos(220.0F, 40.0F, 0.0F);
      var9.setPos(320.0F, 40.0F, 0.0F);
      var4.getInputPanel().getContent().attach(var6);
      var4.getInputPanel().getContent().attach(var8);
      var4.getInputPanel().getContent().attach(var9);
      var4.getInputPanel().getContent().attach(var7);
      var4.activate();
   }

   public void clearFilter() {
      this.searchBar.reset();
   }
}
