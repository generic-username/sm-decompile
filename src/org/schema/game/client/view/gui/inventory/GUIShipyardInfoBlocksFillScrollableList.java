package org.schema.game.client.view.gui.inventory;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.controller.PlayerShipyardInfoDialog;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIBlockSprite;
import org.schema.game.common.controller.elements.shipyard.ShipyardCollectionManager;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalProgressBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIShipyardInfoBlocksFillScrollableList extends ScrollableTableList implements DrawerObserver {
   ObjectArrayList items = new ObjectArrayList();
   private ShipyardCollectionManager item;

   public GUIShipyardInfoBlocksFillScrollableList(InputState var1, GUIElement var2, PlayerShipyardInfoDialog var3, ShipyardCollectionManager var4) {
      super(var1, 100.0F, 100.0F, var2);
      this.item = var4;
      var4.drawObserver = this;
   }

   public void updateTypes() {
      this.items.clear();

      for(int var1 = 0; var1 < ElementKeyMap.highestType + 1; ++var1) {
         short var2 = (short)var1;
         if (this.item.clientGoalFrom.get(var2) > 0 && ElementKeyMap.isValidType(var2)) {
            ElementKeyMap.getInfo(var2);
            ElementInformation var3 = ElementKeyMap.getInfo(var2);
            this.items.add(new ShipyardTypeRowItem(var3, this.item, (GameClientState)this.getState()));
         }
      }

   }

   public void cleanUp() {
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_GUISHIPYARDINFOBLOCKSFILLSCROLLABLELIST_0, 3.0F, new Comparator() {
         public int compare(ShipyardTypeRowItem var1, ShipyardTypeRowItem var2) {
            return var1.info.getName().compareToIgnoreCase(var2.info.getName());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_GUISHIPYARDINFOBLOCKSFILLSCROLLABLELIST_1, 64, new Comparator() {
         public int compare(ShipyardTypeRowItem var1, ShipyardTypeRowItem var2) {
            return var1.getProgress() - var2.getProgress();
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_GUISHIPYARDINFOBLOCKSFILLSCROLLABLELIST_2, 64, new Comparator() {
         public int compare(ShipyardTypeRowItem var1, ShipyardTypeRowItem var2) {
            return var1.getGoal() - var2.getGoal();
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_GUISHIPYARDINFOBLOCKSFILLSCROLLABLELIST_3, 100, new Comparator() {
         public int compare(ShipyardTypeRowItem var1, ShipyardTypeRowItem var2) {
            if (var1.getPercent() > var2.getPercent()) {
               return 1;
            } else {
               return var1.getPercent() < var2.getPercent() ? -1 : 0;
            }
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, ShipyardTypeRowItem var2) {
            return var2.info.getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_GUISHIPYARDINFOBLOCKSFILLSCROLLABLELIST_4, ControllerElement.FilterRowStyle.FULL);
   }

   protected Collection getElementList() {
      this.updateTypes();
      return this.items;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getPlayer();
      Iterator var9 = var2.iterator();

      while(var9.hasNext()) {
         final ShipyardTypeRowItem var3 = (ShipyardTypeRowItem)var9.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUIBlockSprite var5;
         (var5 = new GUIBlockSprite(this.getState(), var3.info.getId())).setScale(0.35F, 0.35F, 0.0F);
         var4.setTextSimple(var3.info.getName());
         ScrollableTableList.GUIClippedRow var6;
         (var6 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         var4.setPos(24.0F, 5.0F, 0.0F);
         var6.attach(var4);
         var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var11 = new GUITextOverlayTable(10, 10, this.getState());
         var4.setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var3.getProgress());
            }
         });
         var11.setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var3.getGoal());
            }
         });
         var4.getPos().y = 5.0F;
         var11.getPos().y = 5.0F;
         ScrollableTableList.GUIClippedRow var7 = new ScrollableTableList.GUIClippedRow(this.getState());
         GUIHorizontalProgressBar var8;
         (var8 = new GUIHorizontalProgressBar(this.getState(), var7) {
            public float getValue() {
               return var3.getPercent();
            }
         }).getColor().set(0.3F, 1.0F, 0.0F, 1.0F);
         var8.setDisplayPercent(true);
         var7.attach(var8);
         GUIShipyardInfoBlocksFillScrollableList.PlayerMessageRow var10;
         (var10 = new GUIShipyardInfoBlocksFillScrollableList.PlayerMessageRow(this.getState(), var3, new GUIElement[]{var6, var4, var11, var7})).onInit();
         var1.addWithoutUpdate(var10);
      }

      var1.updateDim();
   }

   public void update(DrawerObservable var1, Object var2, Object var3) {
      this.flagDirty();
   }

   class PlayerMessageRow extends ScrollableTableList.Row {
      public PlayerMessageRow(InputState var2, ShipyardTypeRowItem var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
