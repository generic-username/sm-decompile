package org.schema.game.client.view.gui.inventory.inventorynew;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observable;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerBigOkCancelInput;
import org.schema.game.client.controller.PlayerBlockTypeDropdownInputNew;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIBlockConsistenceGraph;
import org.schema.game.client.view.gui.GUIBlockSprite;
import org.schema.game.client.view.gui.inventory.InventoryIconsNew;
import org.schema.game.client.view.gui.inventory.InventoryToolInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.controller.elements.factory.FactoryCollectionManager;
import org.schema.game.common.controller.elements.factory.FactoryElementManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCategory;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FixedRecipe;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.inventory.NetworkInventoryInterface;
import org.schema.game.common.data.player.inventory.StashInventory;
import org.schema.game.network.objects.LongStringPair;
import org.schema.game.network.objects.remote.RemoteLongString;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalProgressBar;
import org.schema.schine.input.InputState;
import org.schema.schine.network.objects.remote.LongIntPair;
import org.schema.schine.network.objects.remote.RemoteLongIntPair;
import org.schema.schine.network.objects.remote.Streamable;

public class InventoryOptionsFactoryButtonPanel extends GUIAncor implements InventoryToolInterface {
   private String inventoryFilterText = "";
   private SecondaryInventoryPanelNew panel;
   private InventoryPanelNew mainPanel;
   private StashInventory inventory;
   private boolean inventoryActive;
   private SegmentController segmentController;
   private SegmentPiece pointUnsave;
   private InventoryFilterBar searchBar;

   public InventoryOptionsFactoryButtonPanel(InputState var1, SecondaryInventoryPanelNew var2, InventoryPanelNew var3, StashInventory var4) {
      super(var1);
      this.panel = var2;
      this.mainPanel = var3;
      this.inventory = var4;
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFaction();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public void onInit() {
      this.searchBar = new InventoryFilterBar(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_0, this, new TextCallback() {
         public String[] getCommandPrefixes() {
            return null;
         }

         public void onTextEnter(String var1, boolean var2, boolean var3) {
         }

         public void onFailedTextCheck(String var1) {
         }

         public void newLine() {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            InventoryOptionsFactoryButtonPanel.this.inventoryFilterText = var1;
            return var1;
         }
      });
      this.searchBar.getPos().x = 1.0F;
      this.searchBar.getPos().y = 2.0F;
      this.searchBar.leftDependentHalf = true;
      this.searchBar.onInit();
      this.attach(this.searchBar);
      this.segmentController = ((ManagerContainer)this.inventory.getInventoryHolder()).getSegmentController();
      GUIInventoryOtherDropDown var1;
      (var1 = new GUIInventoryOtherDropDown(this.getState(), this, this.segmentController, new DropDownCallback() {
         public void onSelectionChanged(GUIListElement var1) {
            if (var1.getContent().getUserPointer() != null && var1.getContent().getUserPointer() instanceof StashInventory) {
               StashInventory var2 = (StashInventory)var1.getContent().getUserPointer();
               InventoryOptionsFactoryButtonPanel.this.panel.getPlayerInput().deactivate();
               InventoryOptionsFactoryButtonPanel.this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager().setSecondInventory(var2);
               InventoryOptionsFactoryButtonPanel.this.mainPanel.update((Observable)null, (Object)null);
            }

         }
      })).onInit();
      var1.rightDependentHalf = true;
      var1.getPos().y = 2.0F;
      GUIHorizontalButtonTablePane var2;
      (var2 = new GUIHorizontalButtonTablePane(this.getState(), 2, 1, this)).onInit();
      this.pointUnsave = this.segmentController.getSegmentBuffer().getPointUnsave(this.inventory.getParameter());
      final boolean var3 = this.pointUnsave.getType() != 215 && this.pointUnsave.getType() != 213;
      var2.addButton(0, 0, new Object() {
         public String toString() {
            InventoryOptionsFactoryButtonPanel.this.pointUnsave.refresh();
            InventoryOptionsFactoryButtonPanel.this.inventoryActive = InventoryOptionsFactoryButtonPanel.this.pointUnsave.isActive();
            return InventoryOptionsFactoryButtonPanel.this.inventoryActive ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_1 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_2;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               InventoryOptionsFactoryButtonPanel.this.pointUnsave.refresh();
               System.err.println("FACTORY SETTING ACTIVE: " + InventoryOptionsFactoryButtonPanel.this.pointUnsave.isActive() + " -> " + !InventoryOptionsFactoryButtonPanel.this.pointUnsave.isActive());
               long var3 = ElementCollection.getEncodeActivation(InventoryOptionsFactoryButtonPanel.this.pointUnsave, true, !InventoryOptionsFactoryButtonPanel.this.pointUnsave.isActive(), false);
               InventoryOptionsFactoryButtonPanel.this.pointUnsave.getSegment().getSegmentController().sendBlockActivation(var3);
            }

         }

         public boolean isOccluded() {
            return !InventoryOptionsFactoryButtonPanel.this.panel.isActive();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      var2.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_3, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !InventoryOptionsFactoryButtonPanel.this.panel.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               InventoryOptionsFactoryButtonPanel.this.popupMacroProductionDialog();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return var3;
         }
      });
      var2.getPos().x = 1.0F;
      var2.getPos().y = 26.0F;
      this.attach(var2);
      (var2 = new GUIHorizontalButtonTablePane(this.getState(), 3, 1, this)).onInit();
      var2.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_25, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !InventoryOptionsFactoryButtonPanel.this.panel.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               short var4 = InventoryOptionsFactoryButtonPanel.this.inventory.getProduction();
               FixedRecipe var6;
               if (InventoryOptionsFactoryButtonPanel.this.pointUnsave.getType() == 213) {
                  var6 = ElementKeyMap.capsuleRecipe;
               } else if (InventoryOptionsFactoryButtonPanel.this.pointUnsave.getType() == 215) {
                  var6 = ElementKeyMap.microAssemblerRecipe;
               } else {
                  var6 = null;
               }

               if (InventoryOptionsFactoryButtonPanel.this.getState().getPlayerInputs().isEmpty()) {
                  String var3;
                  if (InventoryOptionsFactoryButtonPanel.this.pointUnsave.getType() == 213) {
                     var3 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_5;
                  } else if (InventoryOptionsFactoryButtonPanel.this.pointUnsave.getType() == 215) {
                     var3 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_6;
                  } else {
                     var3 = ElementKeyMap.getInfo(InventoryOptionsFactoryButtonPanel.this.inventory.getProduction()).getName();
                  }

                  PlayerGameOkCancelInput var7;
                  (var7 = new PlayerGameOkCancelInput("PRODUCTION_POPUP_GRAPHM", InventoryOptionsFactoryButtonPanel.this.getState(), 824, 400, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_7, var3), "") {
                     public boolean isOccluded() {
                        return false;
                     }

                     public void onDeactivate() {
                     }

                     public void pressedOK() {
                        this.deactivate();
                     }
                  }).getInputPanel().setCancelButton(false);
                  var7.getInputPanel().onInit();
                  if (var6 != null) {
                     GUIScrollablePanel var5;
                     (var5 = new GUIScrollablePanel(820.0F, 420.0F, ((GUIDialogWindow)var7.getInputPanel().getBackground()).getMainContentPane().getContent(0), InventoryOptionsFactoryButtonPanel.this.getState())).setContent(var6.getGUI(InventoryOptionsFactoryButtonPanel.this.getState()));
                     var7.getInputPanel().getContent().attach(var5);
                  } else {
                     assert var4 != 0;

                     var7.getInputPanel().getContent().attach(new GUIBlockConsistenceGraph(InventoryOptionsFactoryButtonPanel.this.getState(), ElementKeyMap.getInfo(var4), ((GUIDialogWindow)var7.getInputPanel().getBackground()).getMainContentPane().getContent(0)));
                  }

                  var7.activate();
               }
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return !var3 || ElementKeyMap.isValidType(InventoryOptionsFactoryButtonPanel.this.inventory.getProduction());
         }
      });
      var2.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_26, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !InventoryOptionsFactoryButtonPanel.this.panel.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               final PlayerGameTextInput var3;
               (var3 = new PlayerGameTextInput("RENAME_INVENTORY", InventoryOptionsFactoryButtonPanel.this.getState(), 10, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_9, InventoryOptionsFactoryButtonPanel.this.inventory.getCustomName()), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_10) {
                  public void onDeactivate() {
                  }

                  public void onFailedTextCheck(String var1) {
                  }

                  public boolean onInput(String var1) {
                     if (((ManagedSegmentController)InventoryOptionsFactoryButtonPanel.this.segmentController).getManagerContainer().getNamedInventoriesClient().size() < 16) {
                        ((ManagedSegmentController)InventoryOptionsFactoryButtonPanel.this.segmentController).getManagerContainer().getInventoryNetworkObject().getInventoryCustomNameModBuffer().add(new RemoteLongString(new LongStringPair(InventoryOptionsFactoryButtonPanel.this.inventory.getParameter(), var1.trim()), false));
                     } else {
                        this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_11, 0.0F);
                     }

                     return true;
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public String[] getCommandPrefixes() {
                     return null;
                  }
               }).getInputPanel().onInit();
               GUITextButton var4 = new GUITextButton(InventoryOptionsFactoryButtonPanel.this.getState(), 100, 20, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_12, new GUICallback() {
                  public boolean isOccluded() {
                     return !var3.isActive();
                  }

                  public void callback(GUIElement var1, MouseEvent var2) {
                     if (var2.pressedLeftMouse()) {
                        ((ManagedSegmentController)InventoryOptionsFactoryButtonPanel.this.segmentController).getManagerContainer().getInventoryNetworkObject().getInventoryCustomNameModBuffer().add(new RemoteLongString(new LongStringPair(InventoryOptionsFactoryButtonPanel.this.inventory.getParameter(), ""), false));
                        var3.deactivate();
                     }

                  }
               }) {
                  public void draw() {
                     this.setPos(var3.getInputPanel().getButtonCancel().getPos().x + var3.getInputPanel().getButtonCancel().getWidth() + 15.0F, var3.getInputPanel().getButtonCancel().getPos().y, 0.0F);
                     super.draw();
                  }
               };
               var3.getInputPanel().getBackground().attach(var4);
               var3.activate();
            }

         }
      }, (GUIActivationCallback)null);
      var2.addButton(2, 0, new Object() {
         public String toString() {
            String var1 = InventoryOptionsFactoryButtonPanel.this.inventory.getProductionLimit() > 0 ? String.valueOf(InventoryOptionsFactoryButtonPanel.this.inventory.getProductionLimit()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_4;
            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_8, var1);
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !InventoryOptionsFactoryButtonPanel.this.panel.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerGameTextInput("PRODUCTION_POPUP_COUNT_CHA", InventoryOptionsFactoryButtonPanel.this.getState(), 8, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_22, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_23, "1") {
                  public boolean isOccluded() {
                     return false;
                  }

                  public void onFailedTextCheck(String var1) {
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public boolean onInput(String var1) {
                     try {
                        int var3 = Math.max(0, Integer.parseInt(var1));
                        InventoryOptionsFactoryButtonPanel.this.inventory.setProductionLimit(var3);
                        ((ManagedSegmentController)InventoryOptionsFactoryButtonPanel.this.segmentController).getManagerContainer().getInventoryNetworkObject().getInventoryProductionLimitBuffer().add((Streamable)(new RemoteLongIntPair(new LongIntPair(InventoryOptionsFactoryButtonPanel.this.inventory.getParameter(), var3), false)));
                        return true;
                     } catch (NumberFormatException var2) {
                        this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_24);
                        return false;
                     }
                  }

                  public void onDeactivate() {
                  }
               }).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return !var3 || ElementKeyMap.isValidType(InventoryOptionsFactoryButtonPanel.this.inventory.getProduction());
         }
      });
      var2.getPos().x = 1.0F;
      var2.getPos().y = 50.0F;
      this.attach(var2);
      GUIHorizontalProgressBar var5;
      (var5 = new GUIHorizontalProgressBar(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_13, this) {
         public float getValue() {
            if (InventoryOptionsFactoryButtonPanel.this.inventoryActive) {
               FactoryCollectionManager var1;
               ManagerModuleCollection var2;
               if (InventoryOptionsFactoryButtonPanel.this.pointUnsave.getSegmentController() instanceof ManagedSegmentController && (var2 = ((ManagedSegmentController)InventoryOptionsFactoryButtonPanel.this.pointUnsave.getSegmentController()).getManagerContainer().getModulesControllerMap().get(InventoryOptionsFactoryButtonPanel.this.pointUnsave.getType())) != null && var2.getElementManager() instanceof FactoryElementManager && (var1 = (FactoryCollectionManager)((FactoryElementManager)var2.getElementManager()).getCollectionManagersMap().get(InventoryOptionsFactoryButtonPanel.this.pointUnsave.getAbsoluteIndex())) != null && var1.getPowered() < 1.0F) {
                  this.getColor().set(1.0F, 0.0F, 0.0F, 1.0F);
                  this.text = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_27, StringTools.formatPointZero(var1.getPowered() * 100.0F), StringTools.formatPointZero(var1.getPowerConsumedPerSecondCharging()));
                  return 1.0F;
               } else {
                  this.getColor().set(0.0F, 1.0F, 0.0F, 1.0F);
                  float var3;
                  if (InventoryOptionsFactoryButtonPanel.this.pointUnsave.getType() == 215) {
                     var3 = (float)((double)(InventoryOptionsFactoryButtonPanel.this.getState().getController().getServerRunningTime() % 2500L) / 2500.0D);
                     this.text = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_14;
                     return var3;
                  } else if (InventoryOptionsFactoryButtonPanel.this.pointUnsave.getType() == 213) {
                     var3 = (float)((double)(InventoryOptionsFactoryButtonPanel.this.getState().getController().getServerRunningTime() % 2500L) / 2500.0D);
                     this.text = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_15;
                     return var3;
                  } else if (ElementKeyMap.isValidType(InventoryOptionsFactoryButtonPanel.this.inventory.getProduction()) && ElementKeyMap.getFactorykeyset().contains(InventoryOptionsFactoryButtonPanel.this.pointUnsave.getType())) {
                     long var4 = (long)(ElementKeyMap.getInfo(InventoryOptionsFactoryButtonPanel.this.inventory.getProduction()).getFactoryBakeTime() * 1000.0F);
                     var3 = (float)((double)(InventoryOptionsFactoryButtonPanel.this.getState().getController().getServerRunningTime() % var4) / (double)var4);
                     this.text = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_16, ElementKeyMap.getInfo(InventoryOptionsFactoryButtonPanel.this.inventory.getProduction()).getName());
                     return var3;
                  } else {
                     var3 = (float)((double)(InventoryOptionsFactoryButtonPanel.this.getState().getController().getServerRunningTime() % 10000L) / 10000.0D);
                     this.text = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_17;
                     return var3;
                  }
               }
            } else {
               if (ElementKeyMap.isValidType(InventoryOptionsFactoryButtonPanel.this.inventory.getProduction()) && ElementKeyMap.getFactorykeyset().contains(InventoryOptionsFactoryButtonPanel.this.pointUnsave.getType())) {
                  this.text = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_18, ElementKeyMap.getInfo(InventoryOptionsFactoryButtonPanel.this.inventory.getProduction()).getName());
               } else {
                  this.text = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_19;
               }

               return 0.0F;
            }
         }
      }).getColor().set(InventoryPanelNew.PROGRESS_COLOR);
      var5.onInit();
      var5.getPos().x = 1.0F;
      var5.getPos().y = 74.0F;
      this.attach(var5);
      this.attach(var1);
      GUIHorizontalArea var4;
      (var4 = new GUIHorizontalArea(this.getState(), GUIHorizontalArea.HButtonType.TEXT_FIELD, 10) {
         public void draw() {
            this.setWidth(InventoryOptionsFactoryButtonPanel.this.getWidth());
            super.draw();
         }
      }).getPos().x = 1.0F;
      var4.getPos().y = 99.0F;
      this.attach(var4);
      GUIScrollablePanel var6;
      (var6 = new GUIScrollablePanel(24.0F, 24.0F, this, this.getState())).setScrollable(GUIScrollablePanel.SCROLLABLE_NONE);
      var6.setLeftRightClipOnly = true;
      var6.dependent = var4;
      GUITextOverlay var7;
      (var7 = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM, this.getState()) {
         public void draw() {
            if (InventoryOptionsFactoryButtonPanel.this.inventory.isOverCapacity()) {
               this.setColor(1.0F, 0.3F, 0.3F, 1.0F);
            } else {
               this.setColor(1.0F, 1.0F, 1.0F, 1.0F);
            }

            super.draw();
         }
      }).setTextSimple(new Object() {
         public String toString() {
            return InventoryOptionsFactoryButtonPanel.this.inventory.getVolumeString();
         }
      });
      var7.setPos(4.0F, 4.0F, 0.0F);
      var4.attach(var7);
   }

   public String getText() {
      return this.inventoryFilterText;
   }

   public boolean isActiveInventory(InventoryIconsNew var1) {
      return this.mainPanel.isInventoryActive(var1);
   }

   public void popupMicroOrMacroProductionDialog() {
      FixedRecipe var1;
      if (this.pointUnsave.getType() == 213) {
         var1 = ElementKeyMap.capsuleRecipe;
      } else {
         if (this.pointUnsave.getType() != 215) {
            throw new NullPointerException("Must be assigned");
         }

         var1 = ElementKeyMap.microAssemblerRecipe;
      }

      if (this.getState().getPlayerInputs().isEmpty()) {
         PlayerBigOkCancelInput var2 = new PlayerBigOkCancelInput(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_20, "") {
            public void onDeactivate() {
            }

            public boolean isOccluded() {
               return false;
            }

            public void pressedOK() {
               this.deactivate();
            }
         };
         GUIScrollablePanel var3;
         (var3 = new GUIScrollablePanel(820.0F, 420.0F, this.getState())).setContent(var1.getGUI(this.getState()));
         var2.getInputPanel().setCancelButton(false);
         var2.getInputPanel().onInit();
         var2.getInputPanel().getContent().attach(var3);
         var2.activate();
      }

   }

   public void popupMacroProductionDialog() {
      ObjectArrayList var1 = new ObjectArrayList();
      GUIAncor var2 = new GUIAncor(this.getState(), 600.0F, 32.0F);
      GUITextOverlay var3;
      (var3 = new GUITextOverlay(300, 32, FontLibrary.getBoldArial14White(), this.getState())).getPos().y = 7.0F;
      var2.attach(var3);
      var3.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSFACTORYBUTTONPANEL_21);
      var2.setUserPointer(new ElementInformation((short)0, "Use Slot Recipe", (ElementCategory)null, new short[6]));
      var1.add(var2);
      (new PlayerBlockTypeDropdownInputNew("PRODUCTION_POPUP_MACRO", this.getState(), "Pick", var1, 0, 0, false) {
         public ObjectArrayList getElements(GameClientState var1, String var2, ObjectArrayList var3) {
            ObjectArrayList var4 = new ObjectArrayList();
            if (var3 != null) {
               var4.addAll(var3);
            }

            Iterator var8 = ElementKeyMap.sortedByName.iterator();

            while(true) {
               ElementInformation var5;
               do {
                  do {
                     if (!var8.hasNext()) {
                        return var4;
                     }
                  } while(!(var5 = (ElementInformation)var8.next()).isProducedIn(InventoryOptionsFactoryButtonPanel.this.pointUnsave.getType()));
               } while(var2.trim().length() != 0 && !var5.getName().toLowerCase(Locale.ENGLISH).contains(var2.trim().toLowerCase(Locale.ENGLISH)));

               GUIAncor var6 = new GUIAncor(var1, 300.0F, 32.0F);
               var4.add(var6);
               GUITextOverlay var7;
               (var7 = new GUITextOverlay(100, 32, FontLibrary.getBoldArial12White(), var1)).setTextSimple(var5.getName());
               var6.setUserPointer(var5);
               GUIBlockSprite var9;
               (var9 = new GUIBlockSprite(var1, var5.getId())).getScale().set(0.5F, 0.5F, 0.5F);
               var6.attach(var9);
               var7.getPos().x = 50.0F;
               var7.getPos().y = 7.0F;
               var6.attach(var7);
            }
         }

         public void onAdditionalElementOk(Object var1) {
         }

         public void onOk(ElementInformation var1) {
            ((NetworkInventoryInterface)InventoryOptionsFactoryButtonPanel.this.segmentController.getNetworkObject()).getInventoryProductionBuffer().add(ElementCollection.getIndex4((short)InventoryOptionsFactoryButtonPanel.this.inventory.getParameterX(), (short)InventoryOptionsFactoryButtonPanel.this.inventory.getParameterY(), (short)InventoryOptionsFactoryButtonPanel.this.inventory.getParameterZ(), var1.getId()));
         }

         public void onOkMeta(MetaObject var1) {
         }
      }).activate();
   }

   public void clearFilter() {
      this.searchBar.reset();
   }
}
