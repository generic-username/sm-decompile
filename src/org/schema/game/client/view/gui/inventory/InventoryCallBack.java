package org.schema.game.client.view.gui.inventory;

public interface InventoryCallBack {
   void onFastSwitch(InventorySlotOverlayElement var1, InventoryIconsNew var2);
}
