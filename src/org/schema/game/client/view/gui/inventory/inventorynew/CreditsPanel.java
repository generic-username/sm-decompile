package org.schema.game.client.view.gui.inventory.inventorynew;

import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.manager.ingame.InventoryControllerManager;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.input.InputState;

public class CreditsPanel extends GUIElement implements GUICallback {
   private static Vector4f cWhite = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   private static Vector4f cGrey = new Vector4f(0.38F, 0.38F, 0.38F, 1.0F);
   boolean rightDependentHalf = false;
   boolean leftDependentHalf = false;
   GUIHorizontalArea creditsBg;
   GUITextOverlayTable credits;
   GUIOverlay creditsTakeButton;
   private GUIElement dependend;

   public CreditsPanel(InputState var1, GUIElement var2) {
      super(var1);
      this.dependend = var2;
   }

   public void cleanUp() {
      if (this.creditsBg != null) {
         this.creditsBg.cleanUp();
      }

      if (this.credits != null) {
         this.credits.cleanUp();
      }

      if (this.creditsTakeButton != null) {
         this.creditsTakeButton.cleanUp();
      }

   }

   public void draw() {
      int var1 = (int)this.dependend.getWidth();
      int var2 = 0;
      if (this.leftDependentHalf) {
         var1 = (int)this.dependend.getWidth() / 2;
      } else if (this.rightDependentHalf) {
         var1 = (int)this.dependend.getWidth() / 2;
         var2 = (int)this.dependend.getWidth() - var1;
      }

      GlUtil.glPushMatrix();
      this.transform();
      GlUtil.translateModelview((float)var2, 0.0F, 0.0F);
      this.creditsBg.setWidth(var1);
      this.creditsTakeButton.setPos((float)var1 - (this.creditsTakeButton.getWidth() + 4.0F), (float)((int)(this.creditsBg.getHeight() / 2.0F - this.creditsTakeButton.getHeight() / 2.0F)), 0.0F);
      if (this.creditsTakeButton.isInside()) {
         this.creditsTakeButton.setSpriteSubIndex(24);
      } else {
         this.creditsTakeButton.setSpriteSubIndex(23);
      }

      this.creditsBg.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.creditsBg = new GUIHorizontalArea(this.getState(), GUIHorizontalArea.HButtonType.TEXT_FILED_LIGHT, 10);
      this.credits = new GUITextOverlayTable(10, 10, this.getState());
      this.credits.setPos(6.0F, 5.0F, 0.0F);
      this.credits.setTextSimple(new Object() {
         public String toString() {
            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_CREDITSPANEL_0, ((GameClientState)CreditsPanel.this.getState()).getPlayer().getCredits());
         }
      });
      this.creditsBg.attach(this.credits);
      this.creditsTakeButton = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 16px-8x8-gui-"), this.getState()) {
         public void draw() {
            super.draw();
            this.getSprite().getTint().set(CreditsPanel.cWhite);
         }
      };
      this.creditsTakeButton.setSpriteSubIndex(23);
      if (this.creditsTakeButton.getSprite().getTint() == null) {
         this.creditsTakeButton.getSprite().setTint(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
      }

      this.creditsTakeButton.setMouseUpdateEnabled(true);
      this.creditsTakeButton.setCallback(this);
      this.creditsBg.attach(this.creditsTakeButton);
   }

   public float getHeight() {
      return this.creditsBg.getHeight();
   }

   public float getWidth() {
      return this.creditsBg.getWidth();
   }

   public PlayerGameControlManager getPlayerGameControlManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   public InventoryControllerManager getInventoryControlManager() {
      return this.getPlayerGameControlManager().getInventoryControlManager();
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         this.getInventoryControlManager().openDropCreditsDialog(0);
      }

   }

   public boolean isOccluded() {
      return !this.dependend.isActive();
   }
}
