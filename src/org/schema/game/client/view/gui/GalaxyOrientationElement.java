package org.schema.game.client.view.gui;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.data.GameClientState;

public class GalaxyOrientationElement extends OrientationElement {
   private final Transform transform = new Transform();

   public GalaxyOrientationElement(GameClientState var1) {
      super(var1);
      this.transform.setIdentity();
   }

   public Transform getWorldTransform() {
      return this.transform;
   }

   public boolean canDraw() {
      return true;
   }
}
