package org.schema.game.client.view.gui;

import java.util.ArrayList;
import java.util.Iterator;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.font.FontStyle;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIResizableGrabbableWindow;
import org.schema.schine.input.InputState;

public class GUIInputPanel extends GUIElement implements GUIInputInterface {
   public static final int TEXT_PANEL = 0;
   public static final int SMALL_PANEL = 1;
   public static final int BIG_PANEL = 2;
   public GUIResizableGrabbableWindow background;
   protected GUITextOverlay infoText;
   protected GUITextOverlay errorText;
   protected boolean autoOrientate;
   private GUITextButton buttonOK;
   private GUIElement buttonCancel;
   private final GUITextOverlay descriptionText;
   private boolean okButton;
   private boolean cancelButton;
   private String okButtonText;
   private String secondOptionButtonText;
   private String cancelButtonText;
   private long timeError;
   private long timeErrorShowed;
   private boolean firstDraw;
   private int infoTextSize;
   private boolean titleOnTop;
   protected GUIScrollablePanel scrollableContent;
   private GUITextButton buttonSecondOption;
   private int secondOptionWidth;
   private final GUIInputContentSizeInterface defaultContentInterface;
   public GUIInputContentSizeInterface contentInterface;
   private boolean secondOptionButton;

   public GUIInputPanel(String var1, InputState var2, int var3, int var4, GUICallback var5, GUIResizableGrabbableWindow var6) {
      super(var2);
      this.autoOrientate = true;
      this.okButton = true;
      this.cancelButton = true;
      this.okButtonText = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIINPUTPANEL_0;
      this.secondOptionButtonText = "";
      this.cancelButtonText = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIINPUTPANEL_1;
      this.firstDraw = true;
      this.secondOptionWidth = 85;
      this.defaultContentInterface = new GUIInputContentSizeInterface() {
         public int getWidth() {
            return GUIInputPanel.this.descriptionText.getMaxLineWidth() - 12;
         }

         public int getHeight() {
            return GUIInputPanel.this.descriptionText.getTextHeight() + 12;
         }
      };
      this.contentInterface = this.defaultContentInterface;
      this.setCallback(var5);
      this.infoText = new GUITextOverlay(256, 64, FontLibrary.FontSize.BIG.getFont(), var2);
      this.descriptionText = new GUITextOverlay(256, 64, FontStyle.def.getFont(), var2) {
         public void draw() {
            super.draw();
            this.setWidth(this.getMaxLineWidth());
            this.setHeight(this.getTextHeight());
         }
      };
      this.errorText = new GUITextOverlay(256, 64, var2);

      assert GuiDrawer.isNewHud();

      this.initNewGUI(var5, "", "", var6);
   }

   public GUIInputPanel(String var1, InputState var2, int var3, int var4, GUICallback var5, Object var6, Object var7, GUIResizableGrabbableWindow var8, FontStyle var9) {
      super(var2);
      this.autoOrientate = true;
      this.okButton = true;
      this.cancelButton = true;
      this.okButtonText = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIINPUTPANEL_0;
      this.secondOptionButtonText = "";
      this.cancelButtonText = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIINPUTPANEL_1;
      this.firstDraw = true;
      this.secondOptionWidth = 85;
      this.defaultContentInterface = new GUIInputContentSizeInterface() {
         public int getWidth() {
            return GUIInputPanel.this.descriptionText.getMaxLineWidth() - 12;
         }

         public int getHeight() {
            return GUIInputPanel.this.descriptionText.getTextHeight() + 12;
         }
      };
      this.contentInterface = this.defaultContentInterface;
      this.setCallback(var5);
      this.infoText = new GUITextOverlay(256, 64, FontLibrary.FontSize.BIG.getFont(), var2);
      this.descriptionText = new GUITextOverlay(256, 64, var9.getFont(), var2) {
         public void draw() {
            super.draw();
            this.setWidth(this.getMaxLineWidth());
            this.setHeight(this.getTextHeight());
         }
      };
      this.errorText = new GUITextOverlay(256, 64, var2);

      assert GuiDrawer.isNewHud();

      this.initNewGUI(var5, var6, var7, var8);
   }

   public GUIInputPanel(String var1, InputState var2, GUICallback var3, Object var4, Object var5, FontStyle var6) {
      this(var1, var2, 420, 180, var3, var4, var5, var6);
   }

   public GUIInputPanel(String var1, InputState var2, int var3, int var4, GUICallback var5, Object var6, Object var7, FontStyle var8) {
      this(var1, var2, var3, var4, var5, var6, var7, new GUIDialogWindow(var2, var3, var4, var1), var8);
   }

   public GUIInputPanel(String var1, InputState var2, GUICallback var3, Object var4, Object var5) {
      this(var1, var2, 420, 180, var3, var4, var5);
   }

   public GUIInputPanel(String var1, InputState var2, int var3, int var4, GUICallback var5, Object var6, Object var7) {
      this(var1, var2, var3, var4, var5, var6, var7, new GUIDialogWindow(var2, var3, var4, var1), FontStyle.def);
   }

   public void setOkButtonText(String var1) {
      this.okButtonText = var1;
   }

   public void setSecondOptionButtonWidth(int var1) {
      this.secondOptionWidth = var1;
      this.buttonSecondOption.setWidth(var1);
   }

   public void setSecondOptionButtonText(String var1) {
      this.secondOptionButtonText = var1;
   }

   public void setCancelButtonText(String var1) {
      this.cancelButtonText = var1;
   }

   protected void doOrientation() {
      if (isNewHud()) {
         this.background.doOrientation();
      } else {
         super.doOrientation();
      }
   }

   public float getHeight() {
      return 256.0F;
   }

   public float getWidth() {
      return 256.0F;
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void orientate(int var1) {
      this.background.orientate(var1);
   }

   private void initNewGUI(GUICallback var1, Object var2, Object var3, GUIResizableGrabbableWindow var4) {
      this.buttonOK = new GUITextButton(this.getState(), 85, 20, GUITextButton.ColorPalette.OK, new Object() {
         public String toString() {
            return GUIInputPanel.this.okButtonText;
         }
      }, var1);
      this.buttonSecondOption = new GUITextButton(this.getState(), this.secondOptionWidth, 20, GUITextButton.ColorPalette.OK, new Object() {
         public String toString() {
            return GUIInputPanel.this.secondOptionButtonText;
         }
      }, var1);
      this.buttonCancel = new GUITextButton(this.getState(), 85, 20, GUITextButton.ColorPalette.CANCEL, new Object() {
         public String toString() {
            return GUIInputPanel.this.cancelButtonText;
         }
      }, var1);
      this.background = var4;
      this.background.activeInterface = new GUIActiveInterface() {
         public boolean isActive() {
            return GUIInputPanel.this.getState().getController().getPlayerInputs().isEmpty() || ((DialogInterface)GUIInputPanel.this.getState().getController().getPlayerInputs().get(GUIInputPanel.this.getState().getController().getPlayerInputs().size() - 1)).getInputPanel() == GUIInputPanel.this;
         }
      };
      this.buttonOK.setCallback(var1);
      this.buttonOK.setUserPointer("OK");
      this.buttonOK.setMouseUpdateEnabled(true);
      this.buttonSecondOption.setCallback(var1);
      this.buttonSecondOption.setUserPointer("SECOND_OPTION");
      this.buttonSecondOption.setMouseUpdateEnabled(true);
      this.buttonCancel.setCallback(var1);
      this.buttonCancel.setUserPointer("CANCEL");
      this.buttonCancel.setMouseUpdateEnabled(true);
      this.background.setCloseCallback(var1);
      this.infoText.setTextSimple(var2);
      this.descriptionText.setTextSimple(var3);
      ArrayList var5 = new ArrayList();
      this.errorText.setText(var5);
   }

   public void setCallback(GUICallback var1) {
      if (this.buttonCancel != null) {
         this.buttonCancel.setCallback(var1);
      }

      if (this.buttonOK != null) {
         this.buttonOK.setCallback(var1);
      }

      if (this.buttonSecondOption != null) {
         this.buttonSecondOption.setCallback(var1);
      }

      if (this.background != null) {
         this.background.setCloseCallback(var1);
      }

      super.setCallback(var1);
   }

   public void cleanUp() {
      this.background.cleanUp();
      this.infoText.cleanUp();
      this.buttonOK.cleanUp();
      this.buttonSecondOption.cleanUp();
      this.buttonCancel.cleanUp();
   }

   public void setContentInScrollable(GUIElement var1) {
      this.scrollableContent.setContent(var1);
   }

   public GUIAncor getContent() {
      return ((GUIDialogWindow)this.background).getMainContentPane().getContent(0);
   }

   protected void adaptSizes() {
      this.scrollableContent.setWidth(((GUIDialogWindow)this.background).getInnerWidth() + 8);
      this.scrollableContent.setHeight(((GUIDialogWindow)this.background).getMainContentPane().getContent(0).getHeight());
      this.getContent().setWidth(Math.max(((GUIDialogWindow)this.background).getInnerWidth(), this.contentInterface.getWidth()));
      this.getContent().setHeight(Math.max(((GUIDialogWindow)this.background).getInnerHeigth() - ((GUIDialogWindow)this.background).getInset(), this.contentInterface.getHeight()));
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      if (this.needsReOrientation()) {
         this.background.orientate(48);
      }

      this.buttonOK.setPos(8.0F, (float)((int)(this.background.getHeight() - (42.0F + this.buttonOK.getHeight()))), 0.0F);
      if (this.isSecondOptionButton()) {
         this.buttonSecondOption.setPos((float)((int)(this.buttonOK.getPos().x + this.buttonOK.getWidth() + 5.0F)), (float)((int)this.buttonOK.getPos().y), 0.0F);
         this.buttonCancel.setPos((float)((int)(this.buttonSecondOption.getPos().x + this.buttonSecondOption.getWidth() + 5.0F)), (float)((int)this.buttonSecondOption.getPos().y), 0.0F);
      } else {
         this.buttonCancel.setPos((float)((int)(this.buttonOK.getPos().x + this.buttonOK.getWidth() + 5.0F)), (float)((int)this.buttonOK.getPos().y), 0.0F);
      }

      if (this.isTitleOnTop()) {
         System.err.println("INFO::: " + this.infoText.getText());
         this.infoText.setPos((float)((int)(this.background.getWidth() / 2.0F - (float)(this.infoText.getMaxLineWidth() / 2))), -124.0F, 0.0F);
      } else {
         this.infoText.setPos((float)((int)(this.background.getWidth() / 2.0F - (float)(this.infoText.getMaxLineWidth() / 2))), 8.0F, 0.0F);
      }

      this.adaptSizes();
      GlUtil.glPushMatrix();
      this.transform();
      if (this.timeError < System.currentTimeMillis() - this.timeErrorShowed) {
         this.errorText.getText().clear();
      }

      Iterator var1 = this.getChilds().iterator();

      while(var1.hasNext()) {
         ((AbstractSceneNode)var1.next()).draw();
      }

      GlUtil.glPopMatrix();
   }

   public void onInit() {
      if (this.firstDraw) {
         this.background.onInit();
         this.infoText.onInit();
         this.buttonOK.onInit();
         this.buttonCancel.onInit();
         this.buttonSecondOption.onInit();
         this.descriptionText.onInit();
         if (this.autoOrientate) {
            this.background.orientate(48);
         }

         this.attach(this.background);
         this.infoTextSize = this.infoText.getFont().getWidth(this.infoText.getText().get(0).toString());
         if (this.background instanceof GUIDialogWindow) {
            ((GUIDialogWindow)this.background).attachSuper(this.infoText);
         }

         this.background.attach(this.errorText);
         if (this.isOkButton()) {
            this.background.attach(this.buttonOK);
         }

         if (this.isSecondOptionButton()) {
            this.background.attach(this.buttonSecondOption);
         }

         if (this.isCancelButton()) {
            this.background.attach(this.buttonCancel);
         }

         this.infoText.setPos(100.0F, 8.0F, 0.0F);
         this.scrollableContent = new GUIScrollablePanel(100.0F, 100.0F, ((GUIDialogWindow)this.background).getMainContentPane().getContent(0), this.getState());
         this.scrollableContent.setContent(this.descriptionText);
         this.descriptionText.wrapSimple = false;
         this.descriptionText.autoWrapOn = this.scrollableContent;
         this.getContent().attach(this.scrollableContent);
         this.errorText.setPos(16.0F, this.background.getHeight() - 32.0F, 0.0F);
         this.descriptionText.setPos(2.0F, 2.0F, 0.0F);
         this.firstDraw = false;
      }
   }

   public GUIResizableGrabbableWindow getBackground() {
      return this.background;
   }

   public GUIElement getButtonCancel() {
      return this.buttonCancel;
   }

   public GUITextButton getButtonOK() {
      return this.buttonOK;
   }

   public GUITextOverlay getDescriptionText() {
      return this.descriptionText;
   }

   public boolean isCancelButton() {
      return this.cancelButton;
   }

   public void setCancelButton(boolean var1) {
      this.cancelButton = var1;
   }

   public boolean isOkButton() {
      return this.okButton;
   }

   public void setOkButton(boolean var1) {
      this.okButton = var1;
   }

   public void setErrorMessage(String var1, long var2) {
      if (this.errorText.getText().isEmpty()) {
         this.errorText.getText().add(var1);
      } else {
         this.errorText.getText().set(0, var1);
      }

      this.timeError = System.currentTimeMillis();
      this.timeErrorShowed = var2;
   }

   public void newLine() {
   }

   public boolean isFirstDraw() {
      return this.firstDraw;
   }

   public void setFirstDraw(boolean var1) {
      this.firstDraw = var1;
   }

   public boolean isTitleOnTop() {
      return this.titleOnTop;
   }

   public void setTitleOnTop(boolean var1) {
      this.titleOnTop = var1;
      if (var1) {
         ((GUIDialogWindow)this.background).setTopDist(0);
      } else {
         ((GUIDialogWindow)this.background).setTopDist(20);
      }
   }

   public boolean isSecondOptionButton() {
      return this.secondOptionButton;
   }

   public void setSecondOptionButton(boolean var1) {
      this.secondOptionButton = var1;
   }
}
