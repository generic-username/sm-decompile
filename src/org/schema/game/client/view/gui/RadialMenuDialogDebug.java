package org.schema.game.client.view.gui;

import java.util.Iterator;
import java.util.Map.Entry;
import org.schema.game.client.controller.PlayerTradeDialogIndependentInput;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.game.server.data.simulation.npc.diplomacy.DiplomacyAction;
import org.schema.game.server.data.simulation.npc.diplomacy.NPCDiplomacy;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class RadialMenuDialogDebug extends RadialMenuDialog implements GUIActivationCallback {
   public RadialMenuDialogDebug(GameClientState var1) {
      super(var1);
   }

   public RadialMenu createMenu(RadialMenuDialog var1) {
      final GameClientState var2 = this.getState();
      RadialMenu var13;
      (var13 = new RadialMenu(var2, "DebugRadial", var1, 800, 600, 50, FontLibrary.getBOLDBlender20())).addItem("Trade Menu", new GUICallback() {
         public boolean isOccluded() {
            return !RadialMenuDialogDebug.this.isActive(var2);
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               (new PlayerTradeDialogIndependentInput(RadialMenuDialogDebug.this.getState())).activate();
               RadialMenuDialogDebug.this.deactivate();
            }

         }
      }, (GUIActivationCallback)null);
      RadialMenu var3 = var13.addItemAsSubMenu("NPC CONTROL ", new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      Iterator var4 = var2.getFactionManager().getFactionCollection().iterator();

      while(true) {
         Faction var5;
         do {
            if (!var4.hasNext()) {
               return var13;
            }
         } while(!(var5 = (Faction)var4.next()).isNPC());

         final NPCFaction var14 = (NPCFaction)var5;
         RadialMenu var6;
         (var6 = var3.addItemAsSubMenu(var14.getName() + "(" + var14.getIdFaction() + ")", new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return true;
            }
         })).addItem("Inventory", new GUICallback() {
            public boolean isOccluded() {
               return !RadialMenuDialogDebug.this.isActive(var2);
            }

            public void callback(GUIElement var1, MouseEvent var2x) {
               if (var2x.pressedLeftMouse()) {
                  RadialMenuDialogDebug.this.getPGCM().inventoryAction(var2.getGameState().getInventory((long)var14.getIdFaction()));
                  RadialMenuDialogDebug.this.deactivate();
               }

            }
         }, (GUIActivationCallback)null);
         RadialMenu var7 = var6.addItemAsSubMenu("Turn Options", new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return true;
            }
         });
         NPCFaction.TurnType[] var8;
         int var9 = (var8 = NPCFaction.TurnType.values()).length;

         int var10;
         for(var10 = 0; var10 < var9; ++var10) {
            final NPCFaction.TurnType var11 = var8[var10];
            var7.addItem(new Object() {
               public String toString() {
                  return var11.name() + " " + (var14.getTurn(var11).active ? "ON" : "OFF");
               }
            }, new GUICallback() {
               public boolean isOccluded() {
                  return !RadialMenuDialogDebug.this.isActive(var2);
               }

               public void callback(GUIElement var1, MouseEvent var2x) {
                  if (var2x.pressedLeftMouse()) {
                     var14.sendCommand(NPCFaction.NPCFactionControlCommandType.TURN_MOD, var11.ordinal(), !var14.getTurn(var11).active);
                  }

               }
            }, (GUIActivationCallback)null);
         }

         var6.addItem(new Object() {
            public String toString() {
               return "Trigger Turn";
            }
         }, new GUICallback() {
            public boolean isOccluded() {
               return !RadialMenuDialogDebug.this.isActive(var2);
            }

            public void callback(GUIElement var1, MouseEvent var2x) {
               if (var2x.pressedLeftMouse()) {
                  var14.sendCommand(NPCFaction.NPCFactionControlCommandType.TURN_TRIGGER);
               }

            }
         }, (GUIActivationCallback)null);
         RadialMenu var17;
         RadialMenu var20 = (var17 = var6.addItemAsSubMenu("Diplomacy", new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return true;
            }
         })).addItemAsSubMenu("Trigger Turn", new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return true;
            }
         });
         NPCDiplomacy.NPCDipleExecType[] var23;
         int var25 = (var23 = NPCDiplomacy.NPCDipleExecType.values()).length;

         int var15;
         for(var15 = 0; var15 < var25; ++var15) {
            final NPCDiplomacy.NPCDipleExecType var12 = var23[var15];
            var20.addItem(new Object() {
               public String toString() {
                  return "Trigger " + var12.name();
               }
            }, new GUICallback() {
               public boolean isOccluded() {
                  return !RadialMenuDialogDebug.this.isActive(var2);
               }

               public void callback(GUIElement var1, MouseEvent var2x) {
                  if (var2x.pressedLeftMouse()) {
                     var14.sendCommand(NPCFaction.NPCFactionControlCommandType.DIPLOMACY_TRIGGER, var12.ordinal());
                  }

               }
            }, (GUIActivationCallback)null);
         }

         RadialMenu var24 = var17.addItemAsSubMenu("Trigger Reaction (Player)", new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return true;
            }
         });
         Iterator var26 = var14.getClientReactions().entrySet().iterator();

         while(var26.hasNext()) {
            final Entry var16 = (Entry)var26.next();
            var24.addItem(new Object() {
               public String toString() {
                  return (String)var16.getValue();
               }
            }, new GUICallback() {
               public boolean isOccluded() {
                  return !RadialMenuDialogDebug.this.isActive(var2);
               }

               public void callback(GUIElement var1, MouseEvent var2x) {
                  if (var2x.pressedLeftMouse()) {
                     var14.sendCommand(NPCFaction.NPCFactionControlCommandType.DIPLOMACY_REACTION, (Integer)var16.getKey(), RadialMenuDialogDebug.this.getState().getPlayer().getDbId());
                  }

               }
            }, (GUIActivationCallback)null);
         }

         RadialMenu var27 = var17.addItemAsSubMenu("Trigger Reaction (Faction)", new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return true;
            }
         });
         Iterator var18 = var14.getClientReactions().entrySet().iterator();

         while(var18.hasNext()) {
            final Entry var28 = (Entry)var18.next();
            var27.addItem(new Object() {
               public String toString() {
                  return (String)var28.getValue();
               }
            }, new GUICallback() {
               public boolean isOccluded() {
                  return !RadialMenuDialogDebug.this.isActive(var2);
               }

               public void callback(GUIElement var1, MouseEvent var2x) {
                  if (var2x.pressedLeftMouse()) {
                     var14.sendCommand(NPCFaction.NPCFactionControlCommandType.DIPLOMACY_REACTION, (long)(Integer)var28.getKey(), RadialMenuDialogDebug.this.getState().getPlayer().getFactionId());
                  }

               }
            }, (GUIActivationCallback)null);
         }

         var7 = var17.addItemAsSubMenu("Trigger Player Action", new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return true;
            }
         });
         DiplomacyAction.DiplActionType[] var30;
         var9 = (var30 = DiplomacyAction.DiplActionType.values()).length;

         for(var10 = 0; var10 < var9; ++var10) {
            final DiplomacyAction.DiplActionType var29 = var30[var10];
            var7.addItem(new Object() {
               public String toString() {
                  return var29.name();
               }
            }, new GUICallback() {
               public boolean isOccluded() {
                  return !RadialMenuDialogDebug.this.isActive(var2);
               }

               public void callback(GUIElement var1, MouseEvent var2x) {
                  if (var2x.pressedLeftMouse()) {
                     var14.sendCommand(NPCFaction.NPCFactionControlCommandType.DIPLOMACY_ACTION, var29.ordinal(), RadialMenuDialogDebug.this.getState().getPlayer().getDbId());
                  }

               }
            }, (GUIActivationCallback)null);
         }

         RadialMenu var32 = var17.addItemAsSubMenu("Modify Points", new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return true;
            }
         });
         int[] var21 = new int[]{1, 5, 25, 100, 500};
         this.modPoints(var14, var2, var32, var21, 1, "Player", this.getState().getPlayer().getDbId());
         this.modPoints(var14, var2, var32, var21, -1, "Player", this.getState().getPlayer().getDbId());
         this.modPoints(var14, var2, var32, var21, 1, "Faction", (long)this.getState().getPlayer().getFactionId());
         this.modPoints(var14, var2, var32, var21, -1, "Faction", (long)this.getState().getPlayer().getFactionId());
         var24 = var17.addItemAsSubMenu("Trigger Faction Action", new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return true;
            }
         });
         DiplomacyAction.DiplActionType[] var31;
         var15 = (var31 = DiplomacyAction.DiplActionType.values()).length;

         for(int var19 = 0; var19 < var15; ++var19) {
            final DiplomacyAction.DiplActionType var22 = var31[var19];
            var24.addItem(new Object() {
               public String toString() {
                  return var22.name();
               }
            }, new GUICallback() {
               public boolean isOccluded() {
                  return !RadialMenuDialogDebug.this.isActive(var2);
               }

               public void callback(GUIElement var1, MouseEvent var2x) {
                  if (var2x.pressedLeftMouse()) {
                     if (RadialMenuDialogDebug.this.getState().getFaction() != null) {
                        var14.sendCommand(NPCFaction.NPCFactionControlCommandType.DIPLOMACY_ACTION, var22.ordinal(), (long)RadialMenuDialogDebug.this.getState().getFaction().getIdFaction());
                        return;
                     }

                     RadialMenuDialogDebug.this.getState().getController().popupAlertTextMessage("Not in a faction", 0.0F);
                  }

               }
            }, (GUIActivationCallback)null);
         }

         var6.addItem(new Object() {
            public String toString() {
               return "Log Credits";
            }
         }, new GUICallback() {
            public boolean isOccluded() {
               return !RadialMenuDialogDebug.this.isActive(var2);
            }

            public void callback(GUIElement var1, MouseEvent var2x) {
               if (var2x.pressedLeftMouse()) {
                  var14.sendCommand(NPCFaction.NPCFactionControlCommandType.LOG_CREDIT_STATUS);
               }

            }
         }, (GUIActivationCallback)null);
      }
   }

   private void modPoints(final NPCFaction var1, final GameClientState var2, RadialMenu var3, int[] var4, int var5, final String var6, final long var7) {
      int var9 = (var4 = var4).length;

      for(int var10 = 0; var10 < var9; ++var10) {
         final int var11 = var4[var10] * var5;
         var3.addItem(new Object() {
            public String toString() {
               return var6 + " " + var11;
            }
         }, new GUICallback() {
            public boolean isOccluded() {
               return !RadialMenuDialogDebug.this.isActive(var2);
            }

            public void callback(GUIElement var1x, MouseEvent var2x) {
               if (var2x.pressedLeftMouse()) {
                  if (var7 >= 2147483647L || RadialMenuDialogDebug.this.getState().getFaction() != null) {
                     var1.sendCommand(NPCFaction.NPCFactionControlCommandType.MOD_POINTS, var11, var7);
                     return;
                  }

                  RadialMenuDialogDebug.this.getState().getController().popupAlertTextMessage("Not in a faction", 0.0F);
               }

            }
         }, (GUIActivationCallback)null);
      }

   }

   public PlayerGameControlManager getPGCM() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   public boolean isVisible(InputState var1) {
      return true;
   }

   public boolean isActive(InputState var1) {
      return super.isActive();
   }
}
