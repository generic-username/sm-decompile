package org.schema.game.client.view.gui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.awt.Color;
import java.awt.Font;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.vecmath.Vector3f;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.font.effects.OutlineEffect;
import org.schema.common.TimeStatistics;
import org.schema.game.client.controller.GUIController;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.controller.manager.HelpManager;
import org.schema.game.client.controller.manager.ingame.ChatControlManager;
import org.schema.game.client.controller.manager.ingame.InventoryControllerManager;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.controller.manager.ingame.ship.ShipExternalFlightController;
import org.schema.game.client.controller.manager.ingame.shop.ShopControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.chat.ChatPanel;
import org.schema.game.client.view.gui.newgui.GUIBlockNamePanel;
import org.schema.game.client.view.gui.playerstats.PlayerStatisticsPanelNew;
import org.schema.game.client.view.gui.shiphud.HudBasic;
import org.schema.game.client.view.gui.shiphud.newhud.Hud;
import org.schema.game.client.view.gui.shiphud.newhud.PopupMessageNew;
import org.schema.game.client.view.gui.tutorial.GUITutorialControls;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.controller.elements.effectblock.EffectElementManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.ZSortedDrawable;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITintScreenElement;
import org.schema.schine.graphicsengine.forms.gui.GUIToolTip;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.movie.subtitles.Subtitle;
import org.schema.schine.input.BasicInputController;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.objects.Sendable;

public class GuiDrawer implements Drawable, ZSortedDrawable {
   public static final int GUI_MODE_NONE = 0;
   public static final int GUI_MODE_INGAME = 1;
   public static final int GUI_MODE_INVENTORY = 2;
   public static final int GUI_MODE_SHOP = 4;
   public static final int GUI_MODE_CHAT = 8;
   public static final int GUI_MODE_CONTROLLER_MANAGER = 16;
   public static final int GUI_HELP_MANAGER = 32;
   public static final int GUI_MODE_SHIP_FLIGHT = 64;
   public final LinkedList popupMessages = new LinkedList();
   public final LinkedList titleMessages = new LinkedList();
   public final LinkedList bigMessages = new LinkedList();
   public GUIBlockNamePanel inViewPopup;
   private PlayerPanel playerPanel;
   private GameClientState state;
   private GUIController guiController;
   private PlayerStatisticsPanelNew playerStatisticsPanel;
   private GUITextOverlay detachedInfo;
   private GUITintScreenElement screenTint;
   private float hurtDir = 1.0F;
   private float hurtDuration = 0.2F;
   private float hurtIntensity = 0.3F;
   private float hurtTime = -1.0F;
   private ShipOrientationElement shipOrientationElement;
   private int guiMode = 1;
   private HudBasic hudBasic;
   private Hud hud;
   private final List subtitleOverlays = new ObjectArrayList();
   private GUITutorialControls tutorialControls;
   private boolean drawHud;
   private RadialMenuDialogMain currentRadialMenu;
   private int beforeRadialMouseX;
   private int beforeRadialMouseY;
   private boolean wasDownRadialButton;

   public GuiDrawer(GUIController var1) {
      this.state = var1.getState();
      this.guiController = var1;
      this.guiController.deleteObservers();
      EngineSettings.G_DRAW_GUI_ACTIVE.setCurrentState(true);
   }

   public static boolean isNewHud() {
      return GUIElement.isNewHud();
   }

   private boolean aiActive() {
      return this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getAiConfigurationManager().isTreeActive();
   }

   private boolean catalogActive() {
      return this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getCatalogControlManager().isTreeActive();
   }

   public void cleanUp() {
   }

   public void draw() {
      GlUtil.glDisable(2929);
      GL11.glClear(256);
      if (EngineSettings.G_DRAW_NO_OVERLAYS.isOn()) {
         this.hud.resetDrawnHUDAtAll();
      } else {
         if (EngineSettings.G_DRAW_GUI_ACTIVE.isOn()) {
            this.drawScreenTint();
            if (this.shipBuildActive()) {
               this.shipOrientationElement.draw();
            }

            this.drawHud = !this.shopActive() && !this.inventoryActive() && !this.chatManagerActive() && !this.aiActive() && !this.shipControllerManagerActive() && !this.navigationActive() && !this.factionActive() && !this.catalogActive() && !this.mapActive() && !this.cubatomActive() && !this.structureControllerActive();
            GUIElement.enableOrthogonal();
            if (this.drawHud) {
               TimeStatistics.reset("HUD");
               if (isNewHud()) {
                  this.hud.draw();
               } else {
                  this.hudBasic.draw();
               }

               TimeStatistics.set("HUD");
            } else {
               this.hud.resetDrawnHUDAtAll();
               if (this.mapActive()) {
                  if (isNewHud()) {
                     this.hud.getIndicator().drawMapIndications();
                  } else {
                     this.hudBasic.getIndicator().drawMapIndications();
                  }
               }
            }

            this.state.getController().getTutorialController().drawBackgroundPlayer();
            if (!this.state.getGlobalGameControlManager().getIngameControlManager().getAutoRoamController().isActive()) {
               this.playerPanel.draw();
            }

            GUIElement.disableOrthogonal();
         } else {
            this.hud.resetDrawnHUDAtAll();
         }

         GUIElement.enableOrthogonal();
         BasicInputController var1 = this.state.getController().getInputController();
         int var3;
         synchronized(this.state.getPlayerInputs()) {
            for(var3 = 0; var3 < this.state.getPlayerInputs().size(); ++var3) {
               ((DialogInterface)this.state.getPlayerInputs().get(var3)).getInputPanel().draw();
               if (var1.getCurrentActiveDropdown() != null) {
                  var1.getCurrentActiveDropdown().drawExpanded();
               }
            }
         }

         GUIElement.deactivateCallbacks = true;

         for(int var2 = 0; var2 < var1.getDeactivatedPlayerInputs().size(); ++var2) {
            DialogInterface var8;
            (var8 = (DialogInterface)var1.getDeactivatedPlayerInputs().get(var2)).updateDeacivated();
            var8.getInputPanel().draw();
            if (System.currentTimeMillis() - var8.getDeactivationTime() > 200L) {
               ((DialogInterface)var1.getDeactivatedPlayerInputs().get(var2)).getInputPanel().cleanUp();
               var1.getDeactivatedPlayerInputs().remove(var2);
               --var2;
            }
         }

         GUIElement.deactivateCallbacks = false;
         if (!this.state.getGlobalGameControlManager().getIngameControlManager().getChatControlManager().isActive() && Keyboard.isKeyDown(KeyboardMappings.PLAYER_LIST.getMapping()) && this.state.getPlayerInputs().isEmpty()) {
            this.playerStatisticsPanel.draw();
         }

         if (!this.wasDownRadialButton) {
            if (Keyboard.isKeyDown(KeyboardMappings.RADIAL_MENU.getMapping()) && !this.state.isInTextBox()) {
               if (this.currentRadialMenu == null) {
                  this.beforeRadialMouseX = Mouse.getX();
                  this.beforeRadialMouseY = Mouse.getY();
                  Mouse.setCursorPosition(GLFrame.getWidth() / 2, GLFrame.getHeight() / 2);
                  this.currentRadialMenu = new RadialMenuDialogMain(this.state);
                  this.currentRadialMenu.activate();
               }
            } else {
               this.state.setInTextBox(false);
               if (this.currentRadialMenu != null) {
                  if (this.state.getPlayerInputs().contains(this.currentRadialMenu)) {
                     this.currentRadialMenu.activateSelected();
                     this.currentRadialMenu.deactivate();
                  }

                  this.currentRadialMenu = null;
                  if (this.isInAdvBuildMode()) {
                     Mouse.setCursorPosition(this.beforeRadialMouseX, this.beforeRadialMouseY);
                  }
               }
            }
         }

         this.wasDownRadialButton = Keyboard.isKeyDown(KeyboardMappings.RADIAL_MENU.getMapping());
         GUIElement.disableOrthogonal();
         if (EngineSettings.G_DRAW_POPUPS.isOn()) {
            if (this.shipBuildActive()) {
               GUIElement.enableOrthogonal();
               this.inViewPopup.draw();
               GUIElement.disableOrthogonal();
            }

            synchronized(this.popupMessages) {
               var3 = 0;

               while(true) {
                  if (var3 >= this.popupMessages.size()) {
                     PopupMessageNew.targetPanel.set(0.0F, 0.0F);
                     break;
                  }

                  GUIElement.enableOrthogonal();
                  ((GUIPopupInterface)this.popupMessages.get(var3)).draw();
                  GUIElement.disableOrthogonal();
                  ++var3;
               }
            }

            synchronized(this.titleMessages) {
               var3 = 0;

               while(true) {
                  if (var3 >= this.titleMessages.size()) {
                     break;
                  }

                  GUIElement.enableOrthogonal();
                  ((BigTitleMessage)this.titleMessages.get(var3)).draw();
                  GUIElement.disableOrthogonal();
                  ++var3;
               }
            }

            synchronized(this.bigMessages) {
               for(var3 = 0; var3 < this.bigMessages.size(); ++var3) {
                  GUIElement.enableOrthogonal();
                  ((BigMessage)this.bigMessages.get(var3)).draw();
                  GUIElement.disableOrthogonal();
               }
            }
         }

         if (CameraMouseState.ungrabForced) {
            GUIElement.enableOrthogonal();
            this.detachedInfo.draw();
            GUIElement.disableOrthogonal();
         }

         GUIElement.enableOrthogonal();
         this.state.getController().getInputController().drawDropdownAndContext();
         GUIElement.disableOrthogonal();
         this.drawSubtitles();
         this.playerPanel.afterGUIDraw();
      }
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.screenTint = new GUITintScreenElement(this.state);
      this.screenTint.getColor().set(1.0F, 0.0F, 0.0F, 0.0F);
      this.screenTint.setBorder(35);
      this.screenTint.onInit();
      this.inViewPopup = new GUIBlockNamePanel(this.state);
      this.inViewPopup.onInit();
      this.playerStatisticsPanel = new PlayerStatisticsPanelNew(this.state);
      this.playerStatisticsPanel.onInit();
      this.hudBasic = new HudBasic(this.state);
      this.hud = new Hud(this.state);
      this.hudBasic.onInit();
      this.hud.onInit();
      this.playerPanel = new PlayerPanel(this.state);
      this.playerPanel.onInit();
      this.tutorialControls = new GUITutorialControls(this.state);
      this.tutorialControls.orientate(10);
      this.shipOrientationElement = new ShipOrientationElement(this.state);
      this.shipOrientationElement.onInit();
      Font var1 = new Font("Arial", 1, 26);
      UnicodeFont var3;
      (var3 = new UnicodeFont(var1)).getEffects().add(new OutlineEffect(4, Color.black));
      var3.getEffects().add(new ColorEffect(Color.white));
      var3.addAsciiGlyphs();

      try {
         var3.loadGlyphs();
      } catch (SlickException var2) {
         var2.printStackTrace();
      }

      this.detachedInfo = new GUITextOverlay(500, 50, var3, this.state);
      this.detachedInfo.setText(new ArrayList());
      this.detachedInfo.getText().add(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIDRAWER_0);
      this.doOrientation();
   }

   public int compareTo(ZSortedDrawable var1) {
      return Integer.MAX_VALUE;
   }

   private boolean cubatomActive() {
      return this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getCubatomControlManager().isTreeActive();
   }

   public void doOrientation() {
      this.playerStatisticsPanel.orientate(48);
      this.detachedInfo.orientate(48);
      this.hudBasic.orientate(48);
      this.hud.orientate(48);
   }

   private void drawScreenTint() {
      if (this.hurtTime < this.hurtDuration) {
         this.screenTint.getColor().w = this.hurtTime / this.hurtDuration * this.hurtIntensity;
         GUIElement.enableOrthogonal();
         this.screenTint.draw();
         GUIElement.disableOrthogonal();
      }

   }

   public void drawZSorted() {
      AbstractScene.zSortedMap.add((Comparable)this);
   }

   public FloatBuffer getBufferedTransformation() {
      return null;
   }

   public Vector3f getBufferedTransformationPosition() {
      return null;
   }

   private boolean factionActive() {
      return this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager().isTreeActive();
   }

   public PlayerGameControlManager getPlayerInteractionManager() {
      return this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   private boolean structureControllerActive() {
      return this.getPlayerInteractionManager().getStructureControlManager().isTreeActive();
   }

   public PlayerPanel getPlayerPanel() {
      return this.playerPanel;
   }

   public void handleMode(int var1, AbstractControlManager var2) {
      int var3 = var1;
      if ((this.guiMode & var1) == var1) {
         var3 = 0;
      }

      this.guiMode += var2.isTreeActive() ? var3 : -var1;
   }

   private boolean inventoryActive() {
      return this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager().isTreeActive();
   }

   public boolean isMouseOnPanel() {
      return this.playerPanel.isMouseOnPanel();
   }

   public void managerChanged(ElementCollectionManager var1) {
      this.playerPanel.managerChanged(var1);
   }

   private boolean mapActive() {
      return this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getMapControlManager().isTreeActive();
   }

   private boolean navigationActive() {
      return this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getNavigationControlManager().isTreeActive();
   }

   public void onSectorChange() {
      this.hudBasic.onSectorChange();
      this.hud.onSectorChange();
   }

   private boolean shipBuildActive() {
      return this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().isTreeActive();
   }

   private boolean isInAdvBuildMode() {
      return PlayerInteractionControlManager.isAdvancedBuildMode(this.state);
   }

   private boolean shipControllerManagerActive() {
      return this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getWeaponControlManager().isActive();
   }

   private boolean chatManagerActive() {
      return this.state.getGlobalGameControlManager().getIngameControlManager().getChatControlManager().isActive();
   }

   private boolean shopActive() {
      return this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getShopControlManager().isTreeActive();
   }

   public void drawSubtitles() {
      if (this.state.getActiveSubtitles() != null && EngineSettings.SUBTITLES.isOn()) {
         GUIElement.enableOrthogonal();
         GlUtil.glPushMatrix();
         List var1 = this.state.getActiveSubtitles();
         int var2 = 0;
         int var3 = 120;

         for(int var4 = var1.size() - 1; var4 >= 0; --var4) {
            String[] var5;
            for(int var6 = (var5 = ((Subtitle)var1.get(var4)).text.split("\n")).length - 1; var6 >= 0; --var6) {
               String var7 = var5[var6];
               if (this.subtitleOverlays.size() == var2) {
                  this.subtitleOverlays.add(new GUITextOverlay(10, 10, FontLibrary.getBlenderProHeavy31(), this.state));
               }

               GUITextOverlay var8;
               (var8 = (GUITextOverlay)this.subtitleOverlays.get(var2)).setTextSimple(var7);
               var8.updateTextSize();
               var8.getPos().x = (float)(GLFrame.getWidth() / 2 - var8.getMaxLineWidth() / 2);
               var8.getPos().y = (float)(GLFrame.getHeight() - (var3 + var8.getTextHeight()));
               var8.draw();
               var3 += var8.getTextHeight() + 6;
               ++var2;
            }
         }

         GlUtil.glPopMatrix();
         GUIElement.disableOrthogonal();
      }

   }

   public void startHurtAnimation(Sendable var1) {
      if (this.hurtTime < 0.0F) {
         this.hurtTime = 0.0F;
         this.hurtDir = 1.0F;
         if (var1 != null && var1 instanceof ManagedSegmentController && (ManagedSegmentController)var1 instanceof ShieldContainerInterface && ((ShieldContainerInterface)((ManagedSegmentController)var1)).getShieldAddOn().getShields() > 0.0D) {
            this.screenTint.getColor().x = 0.0F;
            this.screenTint.getColor().y = 0.0F;
            this.screenTint.getColor().z = 1.0F;
            return;
         }

         this.screenTint.getColor().x = 1.0F;
         this.screenTint.getColor().y = 0.0F;
         this.screenTint.getColor().z = 0.0F;
      }

   }

   public void notifySwitch(AbstractControlManager var1) {
      if (this.playerPanel != null) {
         this.playerPanel.notifySwitch(var1);
      }

      if (var1 instanceof ChatControlManager) {
         this.handleMode(8, var1);
      }

      if (var1 instanceof InventoryControllerManager) {
         this.handleMode(2, var1);
      }

      if (var1 instanceof ShipExternalFlightController) {
         this.handleMode(64, var1);
      }

      if (var1 instanceof ShopControllerManager) {
         this.handleMode(4, var1);
      }

      if (var1 instanceof HelpManager) {
         this.handleMode(32, var1);
      }

   }

   public void update(Timer var1) {
      if (this.hurtTime >= 0.0F) {
         this.hurtTime += this.hurtDir * var1.getDelta() * 2.0F;
         if (this.hurtTime > this.hurtDuration) {
            this.hurtDir = -1.0F;
         }
      } else {
         this.hurtDir = 1.0F;
      }

      if (GUIToolTip.tooltipGraphics != null) {
         GUIToolTip.tooltipGraphics.update(var1);
      }

      if (this.inViewPopup != null) {
         this.inViewPopup.update(var1);
      }

      if (!this.state.getGlobalGameControlManager().getIngameControlManager().getChatControlManager().isActive() && Keyboard.isKeyDown(KeyboardMappings.PLAYER_LIST.getMapping())) {
         this.playerStatisticsPanel.update(var1);
      }

      this.playerPanel.update(var1);
      this.hudBasic.update(var1);
      this.hud.update(var1);
      GUI3DBlockElement.linearTimer.update(var1);
      int var3;
      synchronized(this.popupMessages) {
         var3 = 0;

         for(int var4 = 0; var4 < this.popupMessages.size(); ++var4) {
            ((GUIPopupInterface)this.popupMessages.get(var4)).update(var1);
            if (!((GUIPopupInterface)this.popupMessages.get(var4)).isAlive()) {
               this.popupMessages.remove(var4);
               --var4;
            } else {
               ((GUIPopupInterface)this.popupMessages.get(var4)).setIndex((float)var4);
               ((GUIPopupInterface)this.popupMessages.get(var4)).setCurrentHeight(var3);
               var3 = (int)((float)var3 + ((GUIPopupInterface)this.popupMessages.get(var4)).getHeight());
            }
         }
      }

      synchronized(this.titleMessages) {
         var3 = 0;

         while(true) {
            if (var3 >= this.titleMessages.size()) {
               break;
            }

            ((BigTitleMessage)this.titleMessages.get(var3)).update(var1);
            if (!((BigTitleMessage)this.titleMessages.get(var3)).isAlive()) {
               this.titleMessages.remove(var3);
               --var3;
            } else {
               ((BigTitleMessage)this.titleMessages.get(var3)).index = (float)var3;
            }

            ++var3;
         }
      }

      synchronized(this.bigMessages) {
         for(var3 = 0; var3 < this.bigMessages.size(); ++var3) {
            ((BigMessage)this.bigMessages.get(var3)).update(var1);
            if (!((BigMessage)this.bigMessages.get(var3)).isAlive()) {
               this.bigMessages.remove(var3);
               --var3;
            } else {
               ((BigMessage)this.bigMessages.get(var3)).index = (float)var3;
            }
         }

      }
   }

   public void notifyEffectHit(SimpleTransformableSendableObject var1, EffectElementManager.OffensiveEffects var2) {
      if (isNewHud()) {
         this.playerPanel.getTopBar().notifyEffectHit(var1, var2);
         this.hud.notifyEffectHit(var1, var2);
      }

   }

   public PlayerStatisticsPanelNew getPlayerStatisticsPanel() {
      return this.playerStatisticsPanel;
   }

   public void setPlayerStatisticsPanel(PlayerStatisticsPanelNew var1) {
      this.playerStatisticsPanel = var1;
   }

   public ChatPanel getChatNew() {
      return this.playerPanel.getChat();
   }

   public Hud getHud() {
      return this.hud;
   }

   public void setHud(Hud var1) {
      this.hud = var1;
   }
}
