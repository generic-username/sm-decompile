package org.schema.game.client.view.gui.catalog.newcatalog;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.meta.BlockStorageMetaItem;

public class TypeRowStorageItem {
   public final ElementInformation info;
   public final int itemId;
   private GameClientState state;

   public TypeRowStorageItem(ElementInformation var1, int var2, GameClientState var3) {
      this.info = var1;
      this.itemId = var2;
      this.state = var3;
   }

   public BlockStorageMetaItem getItem() {
      return (BlockStorageMetaItem)this.state.getMetaObjectManager().getObject(this.itemId);
   }

   public int getCount() {
      return this.getItem().storage.get(this.info.getId());
   }

   public int hashCode() {
      return this.info.hashCode();
   }

   public boolean equals(Object var1) {
      return var1 instanceof TypeRowStorageItem && this.info.equals(((TypeRowStorageItem)var1).info);
   }
}
