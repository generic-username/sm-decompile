package org.schema.game.client.view.gui.catalog.newcatalog;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;

public class CatalogPanelNew extends GUIElement implements GUIActiveInterface {
   public GUIMainWindow catalogPanel;
   private GUIContentPane adminTab;
   private boolean init;
   private GUIContentPane availableTab;
   private CatalogScrollableListNew availList;
   private CatalogScrollableListNew adminList;

   public CatalogPanelNew(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      this.catalogPanel.draw();
   }

   public void onInit() {
      if (this.catalogPanel != null) {
         this.catalogPanel.cleanUp();
      }

      this.catalogPanel = new GUIMainWindow(this.getState(), 750, 550, "CatalogPanelNew");
      this.catalogPanel.onInit();
      this.catalogPanel.setCloseCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               CatalogPanelNew.this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().deactivateAll();
            }

         }

         public boolean isOccluded() {
            return !CatalogPanelNew.this.getState().getController().getPlayerInputs().isEmpty();
         }
      });
      this.catalogPanel.orientate(48);
      this.recreateTabs();
      this.init = true;
   }

   public void recreateTabs() {
      Object var1 = null;
      if (this.catalogPanel.getSelectedTab() < this.catalogPanel.getTabs().size()) {
         var1 = ((GUIContentPane)this.catalogPanel.getTabs().get(this.catalogPanel.getSelectedTab())).getTabName();
      }

      this.catalogPanel.clearTabs();
      this.availableTab = this.catalogPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGPANELNEW_1);
      if (this.getOwnPlayer().getNetworkObject().isAdminClient.get()) {
         this.adminTab = this.catalogPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGPANELNEW_2);
         this.createAdminCatalogPane();
      }

      this.createAvailableCatalogPane();
      this.catalogPanel.activeInterface = this;
      if (var1 != null) {
         for(int var2 = 0; var2 < this.catalogPanel.getTabs().size(); ++var2) {
            if (((GUIContentPane)this.catalogPanel.getTabs().get(var2)).getTabName().equals(var1)) {
               this.catalogPanel.setSelectedTab(var2);
               return;
            }
         }
      }

   }

   public void update(Timer var1) {
   }

   public void createAvailableCatalogPane() {
      if (this.availList != null) {
         this.availList.cleanUp();
      }

      CatalogOptionsButtonPanel var1;
      (var1 = new CatalogOptionsButtonPanel(this.getState(), this)).onInit();
      this.availableTab.setContent(0, var1);
      if (!CatalogOptionsButtonPanel.areMultiplayerButtonVisible()) {
         this.availableTab.setTextBoxHeightLast(58);
         this.availableTab.addNewTextBox(10);
      } else {
         this.availableTab.setTextBoxHeightLast(82);
         this.availableTab.addNewTextBox(10);
      }

      this.availList = new CatalogScrollableListNew(this.getState(), this.availableTab.getContent(1), 0, this.getState().getGameState().isBuyBBWithCredits(), false);
      this.availList.onInit();
      this.availableTab.getContent(1).attach(this.availList);
   }

   public void createAdminCatalogPane() {
      if (this.adminList != null) {
         this.adminList.cleanUp();
      }

      CatalogAdminOptionsButtonPanel var1;
      (var1 = new CatalogAdminOptionsButtonPanel(this.getState(), this)).onInit();
      this.adminTab.setContent(0, var1);
      this.adminTab.setTextBoxHeightLast(132);
      this.adminTab.addNewTextBox(10);
      this.adminList = new CatalogScrollableListNew(this.getState(), this.adminTab.getContent(1), 2, this.getState().getGameState().isBuyBBWithCredits(), false);
      this.adminList.onInit();
      this.adminTab.getContent(1).attach(this.adminList);
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFactionManager().getFaction(this.getOwnPlayer().getFactionId());
   }

   public float getHeight() {
      return this.catalogPanel.getHeight();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public float getWidth() {
      return this.catalogPanel.getWidth();
   }

   public boolean isActive() {
      return this.getState().getController().getPlayerInputs().isEmpty();
   }

   public void reset() {
      if (this.catalogPanel != null) {
         this.catalogPanel.reset();
      }

   }
}
