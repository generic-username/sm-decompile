package org.schema.game.client.view.gui.catalog;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerBigOkCancelInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.SimplePlayerCommands;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class CatalogEntryPanel extends GUIColoredRectangle {
   public static ElementCountMap currentRequestedBlockMap;
   private CatalogPermission permission;
   private String prefix;
   private boolean init;

   public CatalogEntryPanel(InputState var1, CatalogPermission var2, String var3, int var4) {
      super(var1, 510.0F, 30.0F, new Vector4f());
      this.permission = var2;
      this.prefix = var3;
      this.setIndex(var4);
   }

   public void draw() {
      super.draw();
   }

   public void onInit() {
      if (!this.init) {
         super.onInit();
         GUITextOverlay var1;
         (var1 = new GUITextOverlay(10, 10, this.getState())).setTextSimple(this.prefix);
         GUITextOverlay var2;
         (var2 = new GUITextOverlay(10, 10, this.getState())).setTextSimple(this.permission.getUid());
         GUITextOverlay var3;
         (var3 = new GUITextOverlay(10, 10, this.getState())).setTextSimple(this.permission.ownerUID);
         Object var4;
         if (((GameClientState)this.getState()).getGameState().isBuyBBWithCredits()) {
            ((GUITextOverlay)(var4 = new GUITextOverlay(10, 10, this.getState()))).setTextSimple(String.valueOf(this.permission.price));
         } else {
            var4 = new GUITextButton(this.getState(), 90, 20, StringTools.formatPointZero(this.permission.mass), new GUICallback() {
               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse() && ((GameClientState)CatalogEntryPanel.this.getState()).getPlayerInputs().isEmpty()) {
                     PlayerBigOkCancelInput var4;
                     (var4 = new PlayerBigOkCancelInput((GameClientState)CatalogEntryPanel.this.getState(), CatalogEntryPanel.this.permission.getUid(), "") {
                        public boolean isOccluded() {
                           return false;
                        }

                        public void onDeactivate() {
                        }

                        public void pressedOK() {
                           this.deactivate();
                        }
                     }).getInputPanel().setCancelButton(false);
                     var4.getInputPanel().onInit();
                     GUIElementList var5 = new GUIElementList(CatalogEntryPanel.this.getState()) {
                        public void draw() {
                           if (CatalogEntryPanel.currentRequestedBlockMap != null) {
                              int var1 = 0;

                              for(int var2 = 0; var2 < ElementKeyMap.highestType + 1; ++var2) {
                                 short var3 = (short)var2;
                                 int var4;
                                 if ((var4 = CatalogEntryPanel.currentRequestedBlockMap.get(var3)) > 0) {
                                    GUIColoredRectangle var5 = new GUIColoredRectangle(this.getState(), 800.0F, 20.0F, GUIElementList.getRowColor(var1));
                                    GUITextOverlay var6;
                                    (var6 = new GUITextOverlay(400, 20, this.getState())).setTextSimple(ElementKeyMap.getInfo(var3).getName());
                                    GUITextOverlay var7;
                                    (var7 = new GUITextOverlay(400, 20, this.getState())).setTextSimple(String.valueOf(var4));
                                    var7.getPos().y = 2.0F;
                                    var6.getPos().y = 2.0F;
                                    var7.getPos().x = 420.0F;
                                    var5.attach(var6);
                                    var5.attach(var7);
                                    this.add(new GUIListElement(var5, var5, this.getState()));
                                    ++var1;
                                 }
                              }

                              CatalogEntryPanel.currentRequestedBlockMap = null;
                           }

                           super.draw();
                        }
                     };
                     GUIScrollablePanel var3 = new GUIScrollablePanel(824.0F, 424.0F, CatalogEntryPanel.this.getState());
                     var5.setScrollPane(var3);
                     var3.setContent(var5);
                     var4.getInputPanel().getContent().attach(var3);
                     CatalogEntryPanel.currentRequestedBlockMap = null;
                     ((GameClientState)CatalogEntryPanel.this.getState()).getPlayer().sendSimpleCommand(SimplePlayerCommands.REQUEST_BLUEPRINT_ITEM_LIST, CatalogEntryPanel.this.permission.getUid());
                     var4.activate();
                  }

               }

               public boolean isOccluded() {
                  return false;
               }
            });
         }

         GUITextOverlay var5 = new GUITextOverlay(10, 10, this.getState());
         if (this.permission.rating > 0.0F) {
            var5.setTextSimple(StringTools.formatPointZero(this.permission.rating) + "/" + CatalogPermission.MAX_RATING);
         } else {
            var5.setTextSimple("n/a");
         }

         this.getState();
         Vector3f var10000 = var2.getPos();
         var10000.x += 7.0F;
         var3.getPos().x = var2.getPos().x + 210.0F;
         ((GUIElement)var4).getPos().x = var3.getPos().x + 140.0F;
         var5.getPos().x = ((GUIElement)var4).getPos().x + 100.0F;
         var1.getPos().y = 5.0F;
         var2.getPos().y = 5.0F;
         var3.getPos().y = 5.0F;
         ((GUIElement)var4).getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         this.attach(var1);
         this.attach(var2);
         this.attach(var3);
         this.attach((GUIElement)var4);
         this.attach(var5);
         this.init = true;
      }

   }

   public void setIndex(int var1) {
      this.setColor(var1 % 2 == 0 ? new Vector4f(0.0F, 0.0F, 0.0F, 0.0F) : new Vector4f(0.1F, 0.1F, 0.1F, 0.5F));
   }
}
