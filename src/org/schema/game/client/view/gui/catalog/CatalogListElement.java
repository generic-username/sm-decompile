package org.schema.game.client.view.gui.catalog;

import java.util.Comparator;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.input.InputState;

public class CatalogListElement extends GUIListElement implements Comparable {
   private final CatalogPermission permission;
   Comparator comperator;

   public CatalogListElement(GUIElement var1, GUIElement var2, CatalogPermission var3, InputState var4) {
      super(var1, var2, var4);
      this.permission = var3;
   }

   public int compareTo(CatalogListElement var1) {
      return this.comperator.compare(this, var1);
   }

   public CatalogPermission getPermission() {
      return this.permission;
   }
}
