package org.schema.game.client.view.gui.catalog;

import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.PlayerTextAreaInput;
import org.schema.game.client.controller.manager.ingame.catalog.CatalogControlManager;
import org.schema.game.client.controller.manager.ingame.catalog.CatalogPermissionEditDialog;
import org.schema.game.client.controller.manager.ingame.catalog.CatalogRateDialog;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.BlueprintPlayerHandleRequest;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.game.network.objects.remote.RemoteBlueprintPlayerRequest;
import org.schema.game.server.data.EntityRequest;
import org.schema.game.server.data.admin.AdminCommands;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class CatalogExtendedPanel extends GUIElement implements GUICallback {
   private GameClientState state;
   private CatalogPermission permission;
   private boolean showAdminSettings;

   public CatalogExtendedPanel(InputState var1, CatalogPermission var2, boolean var3) {
      super(var1);
      this.showAdminSettings = var3;
      this.state = (GameClientState)this.getState();
      this.permission = var2;
   }

   private void load() {
      this.getPlayerCatalogControlManager().suspend(true);
      String var1 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGEXTENDEDPANEL_0;
      PlayerGameTextInput var2;
      (var2 = new PlayerGameTextInput("CatalogExtended_load", (GameClientState)this.getState(), 50, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGEXTENDEDPANEL_1, var1, this.permission.getUid() + "_" + System.currentTimeMillis()) {
         public String[] getCommandPrefixes() {
            return null;
         }

         public boolean isOccluded() {
            return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) {
            return var1;
         }

         public void onDeactivate() {
            CatalogExtendedPanel.this.getPlayerCatalogControlManager().suspend(false);
         }

         public void onFailedTextCheck(String var1) {
            this.setErrorMessage("SHIPNAME INVALID: " + var1);
         }

         public boolean onInput(String var1) {
            if (this.getState().getCharacter() != null && this.getState().getCharacter().getPhysicsDataContainer() != null && this.getState().getCharacter().getPhysicsDataContainer().isInitialized()) {
               System.err.println("[CLIENT] BUYING CATALOG ENTRY: " + CatalogExtendedPanel.this.permission.getUid() + " FOR " + this.getState().getPlayer().getNetworkObject());
               BlueprintPlayerHandleRequest var2;
               (var2 = new BlueprintPlayerHandleRequest()).catalogName = CatalogExtendedPanel.this.permission.getUid();
               var2.entitySpawnName = var1;
               var2.save = false;
               var2.toSaveShip = -1;
               var2.directBuy = true;
               this.getState().getController().sendAdminCommand(AdminCommands.LOAD, var2.catalogName, var2.entitySpawnName);
               return true;
            } else {
               System.err.println("[ERROR] Character might not have been initialized");
               return false;
            }
         }
      }).setInputChecker(new InputChecker() {
         public boolean check(String var1, TextCallback var2) {
            if (EntityRequest.isShipNameValid(var1)) {
               return true;
            } else {
               var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGEXTENDEDPANEL_2);
               return false;
            }
         }
      });
      var2.activate();
   }

   private void buyEntry() {
      if (!((GameClientState)this.getState()).isInShopDistance()) {
         ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGEXTENDEDPANEL_3, 0.0F);
      }

      this.getPlayerCatalogControlManager().suspend(true);
      String var1 = "Please type in a name for your new Ship!";
      PlayerGameTextInput var2;
      (var2 = new PlayerGameTextInput("CatalogExtendedPanel_buyEntry", (GameClientState)this.getState(), 50, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGEXTENDEDPANEL_4, var1, this.permission.getUid() + "_" + System.currentTimeMillis()) {
         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) {
            return var1;
         }

         public boolean isOccluded() {
            return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
         }

         public void onDeactivate() {
            CatalogExtendedPanel.this.getPlayerCatalogControlManager().suspend(false);
         }

         public void onFailedTextCheck(String var1) {
            this.setErrorMessage("SHIPNAME INVALID: " + var1);
         }

         public boolean onInput(String var1) {
            if (this.getState().getCharacter() != null && this.getState().getCharacter().getPhysicsDataContainer() != null && this.getState().getCharacter().getPhysicsDataContainer().isInitialized()) {
               System.err.println("[CLIENT] BUYING CATALOG ENTRY: " + CatalogExtendedPanel.this.permission.getUid() + " FOR " + this.getState().getPlayer().getNetworkObject());
               BlueprintPlayerHandleRequest var2;
               (var2 = new BlueprintPlayerHandleRequest()).catalogName = CatalogExtendedPanel.this.permission.getUid();
               var2.entitySpawnName = var1;
               var2.save = false;
               var2.toSaveShip = -1;
               var2.directBuy = true;
               this.getState().getPlayer().getNetworkObject().catalogPlayerHandleBuffer.add(new RemoteBlueprintPlayerRequest(var2, false));
               return true;
            } else {
               System.err.println("[ERROR] Character might not have been initialized");
               return false;
            }
         }
      }).setInputChecker(new InputChecker() {
         public boolean check(String var1, TextCallback var2) {
            if (EntityRequest.isShipNameValid(var1)) {
               return true;
            } else {
               var2.onFailedTextCheck("Must only contain Letters or numbers or (_-)!");
               return false;
            }
         }
      });
      var2.activate();
   }

   private void buyEntryAsMeta() {
      if (!((GameClientState)this.getState()).isInShopDistance()) {
         ((GameClientState)this.getState()).getController().popupAlertTextMessage("ERROR:\nCannot buy!\nYou are not near a shop!", 0.0F);
      }

      (new PlayerGameOkCancelInput("CatalogExtendedPanel_buyEntryAsMeta", (GameClientState)this.getState(), StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGEXTENDEDPANEL_5, this.permission.getUid()), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGEXTENDEDPANEL_6) {
         public void onDeactivate() {
         }

         public boolean isOccluded() {
            return false;
         }

         public void pressedOK() {
            BlueprintPlayerHandleRequest var1;
            (var1 = new BlueprintPlayerHandleRequest()).catalogName = CatalogExtendedPanel.this.permission.getUid();
            var1.entitySpawnName = "";
            var1.save = false;
            var1.toSaveShip = -1;
            var1.directBuy = false;
            this.getState().getPlayer().getNetworkObject().catalogPlayerHandleBuffer.add(new RemoteBlueprintPlayerRequest(var1, false));
            this.deactivate();
         }
      }).activate();
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         if ("buy".equals(var1.getUserPointer())) {
            if (this.state.getGameState().isBuyBBWithCredits()) {
               this.buyEntry();
               return;
            }

            this.buyEntryAsMeta();
            return;
         }

         if ("description".equals(var1.getUserPointer())) {
            this.editDescription();
            return;
         }

         if ("permission".equals(var1.getUserPointer())) {
            this.editPermission();
            return;
         }

         if ("delete".equals(var1.getUserPointer())) {
            this.deleteEntry();
            return;
         }

         if ("owner".equals(var1.getUserPointer())) {
            this.changeOwner();
            return;
         }

         if ("rate".equals(var1.getUserPointer())) {
            this.rate();
            return;
         }

         if ("adminload".equals(var1.getUserPointer())) {
            this.load();
         }
      }

   }

   public boolean isOccluded() {
      return false;
   }

   private void changeOwner() {
      this.getPlayerCatalogControlManager().suspend(true);
      String var1 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGEXTENDEDPANEL_7, this.permission.getUid());
      PlayerGameTextInput var2;
      (var2 = new PlayerGameTextInput("CatalogExtendedPanel_changeOwner", (GameClientState)this.getState(), 50, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGEXTENDEDPANEL_8, var1, this.permission.ownerUID) {
         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) {
            return var1;
         }

         public boolean isOccluded() {
            return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
         }

         public void onDeactivate() {
            CatalogExtendedPanel.this.getPlayerCatalogControlManager().suspend(false);
         }

         public void onFailedTextCheck(String var1) {
            this.setErrorMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGEXTENDEDPANEL_9, var1));
         }

         public boolean onInput(String var1) {
            if (CatalogExtendedPanel.this.showAdminSettings && this.getState().getPlayer().getNetworkObject().isAdminClient.get()) {
               CatalogPermission var2;
               (var2 = new CatalogPermission(CatalogExtendedPanel.this.permission)).ownerUID = var1;
               var2.changeFlagForced = true;
               this.getState().getCatalogManager().clientRequestCatalogEdit(var2);
            } else {
               System.err.println("ERROR: CANNOT CHANGE OWNER (PERMISSION DENIED)");
            }

            return true;
         }
      }).setInputChecker(new InputChecker() {
         public boolean check(String var1, TextCallback var2) {
            if (EntityRequest.isShipNameValid(var1)) {
               return true;
            } else {
               var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGEXTENDEDPANEL_10);
               return false;
            }
         }
      });
      var2.activate();
   }

   public void cleanUp() {
   }

   public void draw() {
      this.drawAttached();
   }

   public void onInit() {
      GUITextButton var1;
      (var1 = new GUITextButton(this.state, 70, 20, new Vector4f(0.3F, 0.6F, 0.3F, 0.9F), new Vector4f(0.99F, 0.99F, 0.99F, 1.0F), FontLibrary.getBoldArial16White(), "BUY", this, this.getPlayerCatalogControlManager())).setUserPointer("buy");
      var1.setTextPos(14, 1);
      var1.getPos().y = -7.0F;
      GUITextButton var2;
      (var2 = new GUITextButton(this.state, 120, 20, new Vector4f(0.7F, 0.2F, 0.2F, 0.9F), new Vector4f(0.99F, 0.99F, 0.99F, 1.0F), FontLibrary.getBoldArial16White(), "Admin-Load", this, this.getPlayerCatalogControlManager()) {
         public void draw() {
            if (((GameClientState)this.getState()).getPlayer().getNetworkObject().isAdminClient.get()) {
               super.draw();
            }

         }
      }).setUserPointer("adminload");
      GUITextButton var3;
      (var3 = new GUITextButton(this.state, 80, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGEXTENDEDPANEL_11, this, this.getPlayerCatalogControlManager())).setUserPointer("owner");
      var3.setTextPos(5, 1);
      GUITextButton var4;
      (var4 = new GUITextButton(this.state, 90, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGEXTENDEDPANEL_12, this, this.getPlayerCatalogControlManager())).setUserPointer("description");
      var4.setTextPos(5, 1);
      GUITextButton var5;
      (var5 = new GUITextButton(this.state, 90, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGEXTENDEDPANEL_13, this, this.getPlayerCatalogControlManager())).setUserPointer("permission");
      var5.setTextPos(5, 1);
      GUITextButton var6;
      (var6 = new GUITextButton(this.state, 45, 20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGEXTENDEDPANEL_14, this, this.getPlayerCatalogControlManager())).setUserPointer("rate");
      var6.setTextPos(5, 1);
      GUITextButton var7;
      (var7 = new GUITextButton(this.state, 65, 20, new Vector4f(0.7F, 0.2F, 0.2F, 0.9F), new Vector4f(0.99F, 0.99F, 0.99F, 1.0F), FontLibrary.getRegularArial15White(), "delete", this, this.getPlayerCatalogControlManager())).setUserPointer("delete");
      var7.setTextPos(6, 1);
      var2.getPos().x = var1.getPos().x;
      var2.getPos().y = var1.getPos().y + var1.getHeight() + 3.0F;
      var3.getPos().x = var1.getPos().x + 80.0F;
      var4.getPos().x = var3.getPos().x + 90.0F;
      var5.getPos().x = var4.getPos().x + 100.0F;
      var6.getPos().x = var5.getPos().x + 100.0F;
      var7.getPos().x = var6.getPos().x + 70.0F;
      GUITextOverlay var8;
      (var8 = new GUITextOverlay(10, 10, this.state)).getPos().y = 35.0F;
      var8.setTextSimple(this.permission.description);
      this.attach(var1);
      this.attach(var5);
      this.attach(var7);
      this.attach(var8);
      this.attach(var4);
      this.attach(var2);
      this.attach(var6);
      if (this.showAdminSettings) {
         this.attach(var3);
      }

   }

   private void deleteEntry() {
      if ((!this.showAdminSettings || !((GameClientState)this.getState()).getPlayer().getNetworkObject().isAdminClient.get()) && !((GameClientState)this.getState()).getPlayer().getName().equals(this.permission.ownerUID)) {
         ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGEXTENDEDPANEL_15, 0.0F);
      } else {
         this.getPlayerCatalogControlManager().suspend(true);
         (new PlayerGameOkCancelInput("CatalogExtendedPanel_deleteEntry", (GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGEXTENDEDPANEL_16, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGEXTENDEDPANEL_17) {
            public boolean isOccluded() {
               return false;
            }

            public void onDeactivate() {
               CatalogExtendedPanel.this.getPlayerCatalogControlManager().suspend(false);
            }

            public void pressedOK() {
               CatalogPermission var1;
               if (CatalogExtendedPanel.this.showAdminSettings && this.getState().getPlayer().getNetworkObject().isAdminClient.get()) {
                  (var1 = new CatalogPermission(CatalogExtendedPanel.this.permission)).changeFlagForced = true;
                  this.getState().getCatalogManager().clientRequestCatalogRemove(var1);
               } else {
                  (var1 = new CatalogPermission(CatalogExtendedPanel.this.permission)).ownerUID = this.getState().getPlayer().getName();
                  this.getState().getCatalogManager().clientRequestCatalogRemove(var1);
               }

               this.deactivate();
            }
         }).activate();
      }
   }

   private void editDescription() {
      this.getPlayerCatalogControlManager().suspend(true);
      (new PlayerTextAreaInput("CatalogExtendedPanel_editDescription", (GameClientState)this.getState(), 140, 3, "Edit entry Description", Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGEXTENDEDPANEL_18, new String(this.permission.description)) {
         public void onDeactivate() {
            CatalogExtendedPanel.this.getPlayerCatalogControlManager().suspend(false);
         }

         public String[] getCommandPrefixes() {
            return null;
         }

         public boolean onInput(String var1) {
            CatalogPermission var2;
            (var2 = new CatalogPermission(CatalogExtendedPanel.this.permission)).description = var1;
            if (CatalogExtendedPanel.this.showAdminSettings && this.getState().getPlayer().getNetworkObject().isAdminClient.get()) {
               var2.changeFlagForced = true;
               this.getState().getCatalogManager().clientRequestCatalogEdit(var2);
            } else {
               var2.ownerUID = this.getState().getPlayer().getName();
               this.getState().getCatalogManager().clientRequestCatalogEdit(var2);
            }

            return true;
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public boolean isOccluded() {
            return false;
         }

         public void onFailedTextCheck(String var1) {
         }
      }).activate();
   }

   private void editPermission() {
      System.err.println("EDIT PERMISSION");
      this.getPlayerCatalogControlManager().suspend(true);
      (new CatalogPermissionEditDialog(this.state, this.permission)).activate();
   }

   public float getHeight() {
      return 80.0F;
   }

   public float getWidth() {
      return 510.0F;
   }

   public CatalogControlManager getPlayerCatalogControlManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getCatalogControlManager();
   }

   private void rate() {
      this.getPlayerCatalogControlManager().suspend(true);
      (new CatalogRateDialog(this.state, this.permission, this.showAdminSettings)).activate();
   }
}
