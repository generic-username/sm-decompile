package org.schema.game.client.view.gui.catalog.newcatalog;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.PlayerTextAreaInput;
import org.schema.game.client.controller.manager.ingame.catalog.CatalogPermissionEditDialog;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.view.BuildModeDrawer;
import org.schema.game.common.data.player.BlueprintPlayerHandleRequest;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.SimplePlayerCommands;
import org.schema.game.common.data.player.catalog.CatalogManager;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.game.network.objects.remote.RemoteBlueprintPlayerRequest;
import org.schema.game.server.data.EntityRequest;
import org.schema.game.server.data.admin.AdminCommands;
import org.schema.game.server.data.blueprintnw.BlueprintType;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.CreateGUIElementInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUICheckBoxTextPair;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterDropdown;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIPolygonStats;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableDropDown;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableInnerDescription;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.GuiDateFormats;
import org.schema.schine.input.InputState;

public class CatalogScrollableListNew extends ScrollableTableList implements Observer {
   public static final int AVAILABLE = 0;
   public static final int PERSONAL = 1;
   public static final int ADMIN = 2;
   private final boolean showPrice;
   public CatalogPermission selectedSingle;
   private int mode;
   private boolean selectSingle;
   private boolean spawnDocked;
   private boolean useOwnFaction;

   public CatalogScrollableListNew(InputState var1, GUIElement var2, int var3, boolean var4, boolean var5) {
      super(var1, 100.0F, 100.0F, var2);
      this.mode = var3;
      ((GameClientState)this.getState()).getCatalogManager().addObserver(this);
      ((GameClientState)this.getState()).getPlayer().getCatalog().addObserver(this);
      this.selectSingle = var5;
      this.showPrice = var4;
      this.useOwnFaction = ((GameClientState)this.getState()).getPlayer().getFactionId() > 0;
   }

   public void cleanUp() {
      ((GameClientState)this.getState()).getCatalogManager().deleteObserver(this);
      ((GameClientState)this.getState()).getPlayer().getCatalog().deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_0, 7.0F, new Comparator() {
         public int compare(CatalogPermission var1, CatalogPermission var2) {
            assert var1 != null;

            assert var2 != null;

            assert var1.getUid() != null;

            assert var2.getUid() != null;

            return var1.getUid().compareToIgnoreCase(var2.getUid());
         }
      }, true);
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_1, 65, new Comparator() {
         public int compare(CatalogPermission var1, CatalogPermission var2) {
            return var1.type.name().compareTo(var2.type.name());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_42, 101, new Comparator() {
         public int compare(CatalogPermission var1, CatalogPermission var2) {
            return var1.getClassification().ordinal() - var2.getClassification().ordinal();
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_51, 120, new Comparator() {
         public int compare(CatalogPermission var1, CatalogPermission var2) {
            if (var1.date > var2.date) {
               return 1;
            } else {
               return var1.date < var2.date ? -1 : 0;
            }
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_2, 60, new Comparator() {
         public int compare(CatalogPermission var1, CatalogPermission var2) {
            if (var1.mass > var2.mass) {
               return 1;
            } else {
               return var1.mass < var2.mass ? -1 : 0;
            }
         }
      });
      if (this.showPrice) {
         this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_3, 120, new Comparator() {
            public int compare(CatalogPermission var1, CatalogPermission var2) {
               if (var1.price > var2.price) {
                  return 1;
               } else {
                  return var1.price < var2.price ? -1 : 0;
               }
            }
         });
      }

      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_4, 60, new Comparator() {
         public int compare(CatalogPermission var1, CatalogPermission var2) {
            if (var1.rating > var2.rating) {
               return 1;
            } else {
               return var1.rating < var2.rating ? -1 : 0;
            }
         }
      });
      this.addDropdownFilter(new GUIListFilterDropdown(new Integer[]{0, 1}) {
         public boolean isOk(Integer var1, CatalogPermission var2) {
            if (var1 == 0) {
               return var2.ownerUID.toLowerCase(Locale.ENGLISH).equals(((GameClientState)CatalogScrollableListNew.this.getState()).getPlayer().getName().toLowerCase(Locale.ENGLISH));
            } else {
               return var1 == 1 ? var2.faction() : true;
            }
         }
      }, new CreateGUIElementInterface() {
         public GUIElement create(Integer var1) {
            GUIAncor var2 = new GUIAncor(CatalogScrollableListNew.this.getState(), 10.0F, 24.0F);
            GUITextOverlayTableDropDown var3;
            (var3 = new GUITextOverlayTableDropDown(10, 10, CatalogScrollableListNew.this.getState())).setTextSimple(var1 == 0 ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_5 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_6);
            var3.setPos(4.0F, 4.0F, 0.0F);
            var2.setUserPointer(var1);
            var2.attach(var3);
            return var2;
         }

         public GUIElement createNeutral() {
            GUIAncor var1 = new GUIAncor(CatalogScrollableListNew.this.getState(), 10.0F, 24.0F);
            GUITextOverlayTableDropDown var2;
            (var2 = new GUITextOverlayTableDropDown(10, 10, CatalogScrollableListNew.this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_7);
            var2.setPos(4.0F, 4.0F, 0.0F);
            var1.attach(var2);
            return var1;
         }
      }, ControllerElement.FilterRowStyle.LEFT);
      this.addDropdownFilter(new GUIListFilterDropdown(new BlueprintType[]{BlueprintType.SHIP, BlueprintType.SPACE_STATION}) {
         public boolean isOk(BlueprintType var1, CatalogPermission var2) {
            return var2.type == var1;
         }
      }, new CreateGUIElementInterface() {
         public GUIElement create(BlueprintType var1) {
            GUIAncor var2 = new GUIAncor(CatalogScrollableListNew.this.getState(), 10.0F, 20.0F);
            GUITextOverlayTableDropDown var3;
            (var3 = new GUITextOverlayTableDropDown(10, 10, CatalogScrollableListNew.this.getState())).setTextSimple(var1.name());
            var3.setPos(4.0F, 4.0F, 0.0F);
            var2.setUserPointer(var1);
            var2.attach(var3);
            return var2;
         }

         public GUIElement createNeutral() {
            GUIAncor var1 = new GUIAncor(CatalogScrollableListNew.this.getState(), 10.0F, 20.0F);
            GUITextOverlayTableDropDown var2;
            (var2 = new GUITextOverlayTableDropDown(10, 10, CatalogScrollableListNew.this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_49);
            var2.setPos(4.0F, 4.0F, 0.0F);
            var1.attach(var2);
            return var1;
         }
      }, ControllerElement.FilterRowStyle.RIGHT);
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, CatalogPermission var2) {
            return var2.getUid().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_8, ControllerElement.FilterRowStyle.LEFT);
      if (this.mode != 1) {
         this.addTextFilter(new GUIListFilterText() {
            public boolean isOk(String var1, CatalogPermission var2) {
               return var2.ownerUID.toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
            }
         }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_9, ControllerElement.FilterRowStyle.RIGHT);
      }

   }

   protected Collection getElementList() {
      PlayerState var1 = ((GameClientState)this.getState()).getPlayer();
      List var2;
      if (this.mode == 1) {
         var2 = var1.getCatalog().getPersonalCatalog();
      } else if (this.mode == 0) {
         var2 = var1.getCatalog().getAvailableCatalog();
      } else {
         if (this.mode != 2) {
            throw new IllegalArgumentException("[GUI] UNKNOWN CAT MODE: " + this.mode);
         }

         var2 = var1.getCatalog().getAllCatalog();
      }

      return var2;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getGameState().getFactionManager();
      final CatalogManager var3 = ((GameClientState)this.getState()).getGameState().getCatalogManager();
      ((GameClientState)this.getState()).getPlayer();
      Iterator var16 = var2.iterator();

      while(var16.hasNext()) {
         final CatalogPermission var4 = (CatalogPermission)var16.next();

         assert var4.getUid() != null;

         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var8 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var9 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var10 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var11 = new GUITextOverlayTable(10, 10, this.getState());
         ScrollableTableList.GUIClippedRow var12 = new ScrollableTableList.GUIClippedRow(this.getState());
         ScrollableTableList.GUIClippedRow var13 = new ScrollableTableList.GUIClippedRow(this.getState());
         var12.attach(var5);
         var13.attach(var10);
         var5.setTextSimple(var4.getUid());
         var6.setTextSimple(var4.type.type.getName());
         var9.setTextSimple(var4.price);
         var10.setTextSimple(var4.getClassification().getName());
         var7.setTextSimple(StringTools.massFormat(var4.mass));
         var8.setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var4.rating);
            }
         });
         var11.setTextSimple(GuiDateFormats.mediumFormat.format(var4.date));
         var5.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;
         var7.getPos().y = 5.0F;
         var8.getPos().y = 5.0F;
         var9.getPos().y = 5.0F;
         var10.getPos().y = 5.0F;
         var11.getPos().y = 5.0F;
         final CatalogScrollableListNew.CatalogRow var18;
         if (this.showPrice) {
            var18 = new CatalogScrollableListNew.CatalogRow(this.getState(), var4, new GUIElement[]{var12, var6, var13, var11, var7, var9, var8});
         } else {
            var18 = new CatalogScrollableListNew.CatalogRow(this.getState(), var4, new GUIElement[]{var12, var6, var13, var11, var7, var8});
         }

         if (!this.selectSingle) {
            var18.expanded = new GUIElementList(this.getState());
            final String var19 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_10, var4.ownerUID);
            final String var21 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_11, GuiDateFormats.catalogEntryCreated.format(var4.date));
            final GUITextOverlayTableInnerDescription var23;
            (var23 = new GUITextOverlayTableInnerDescription(10, 10, this.getState())).setTextSimple(new Object() {
               public String toString() {
                  return var19 + var21 + Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_12 + var4.description;
               }
            });
            var23.setPos(4.0F, 2.0F, 0.0F);
            final GUIAncor var20 = new GUIAncor(this.getState(), 100.0F, (float)Math.max(112, var23.getTextHeight() + 12)) {
               public void draw() {
                  this.setWidth(var18.l.getInnerTextbox().getWidth());
                  if ((float)var23.getTextHeight() != this.getHeight()) {
                     this.setChanged();
                     this.notifyObservers();
                     this.setHeight(var23.getTextHeight());
                     System.err.println("TEXT HEIGHT UPDATED");
                     var18.expanded.updateDim();
                  }

                  super.draw();
               }
            };
            GUITextButton var22 = new GUITextButton(this.getState(), 56, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_13, new GUICallback() {
               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse()) {
                     if (((GameClientState)CatalogScrollableListNew.this.getState()).getGameState().isBuyBBWithCredits()) {
                        CatalogScrollableListNew.this.buyEntry(var4);
                        return;
                     }

                     CatalogScrollableListNew.this.buyEntryAsMeta(var4);
                  }

               }

               public boolean isOccluded() {
                  return !CatalogScrollableListNew.this.isActive();
               }
            });
            GUITextButton var25 = new GUITextButton(this.getState(), 102, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_14, new GUICallback() {
               public boolean isOccluded() {
                  return !CatalogScrollableListNew.this.isActive();
               }

               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse()) {
                     (new PlayerTextAreaInput("CatalogScrollableListNew_EDIT_DESC", (GameClientState)CatalogScrollableListNew.this.getState(), 140, 3, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_15, "", var4.description) {
                        public String[] getCommandPrefixes() {
                           return null;
                        }

                        public boolean isOccluded() {
                           return false;
                        }

                        public String handleAutoComplete(String var1, TextCallback var2, String var3x) throws PrefixNotFoundException {
                           return null;
                        }

                        public void onDeactivate() {
                        }

                        public boolean onInput(String var1) {
                           if (!CatalogScrollableListNew.this.canEdit(var4)) {
                              return false;
                           } else {
                              var4.description = var1;
                              if (CatalogScrollableListNew.this.mode == 2 && this.getState().getPlayer().getNetworkObject().isAdminClient.get()) {
                                 var4.changeFlagForced = true;
                                 var3.clientRequestCatalogEdit(var4);
                              } else {
                                 var4.ownerUID = this.getState().getPlayer().getName();
                                 var3.clientRequestCatalogEdit(var4);
                              }

                              return true;
                           }
                        }

                        public void onFailedTextCheck(String var1) {
                        }
                     }).activate();
                  }

               }
            });
            GUITextButton var26 = new GUITextButton(this.getState(), 95, 24, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_16, new GUICallback() {
               public boolean isOccluded() {
                  return !CatalogScrollableListNew.this.isActive();
               }

               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse() && CatalogScrollableListNew.this.canEdit(var4)) {
                     (new CatalogPermissionEditDialog((GameClientState)CatalogScrollableListNew.this.getState(), var4)).activate();
                  }

               }
            });
            GUITextButton var27 = new GUITextButton(this.getState(), 70, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_17, new GUICallback() {
               public boolean isOccluded() {
                  return !CatalogScrollableListNew.this.isActive();
               }

               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse()) {
                     PlayerGameOkCancelInput var4x = new PlayerGameOkCancelInput("blueprintConsistence", (GameClientState)CatalogScrollableListNew.this.getState(), 400, 400, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_18, "") {
                        public void onDeactivate() {
                        }

                        public void pressedOK() {
                           this.deactivate();
                        }
                     };
                     ((GameClientState)CatalogScrollableListNew.this.getState()).getPlayer().sendSimpleCommand(SimplePlayerCommands.REQUEST_BLUEPRINT_ITEM_LIST, var4.getUid());
                     var4x.getInputPanel().onInit();
                     ((GUIDialogWindow)var4x.getInputPanel().background).getMainContentPane().setTextBoxHeightLast(25);
                     ((GUIDialogWindow)var4x.getInputPanel().background).getMainContentPane().addNewTextBox(40);
                     GUIHorizontalButtonTablePane var5;
                     (var5 = new GUIHorizontalButtonTablePane(CatalogScrollableListNew.this.getState(), 1, 1, ((GUIDialogWindow)var4x.getInputPanel().background).getMainContentPane().getContent(0))).onInit();
                     ((GUIDialogWindow)var4x.getInputPanel().background).getMainContentPane().getContent(0).attach(var5);
                     final GUIBlueprintConsistenceScrollableList var3;
                     (var3 = new GUIBlueprintConsistenceScrollableList(CatalogScrollableListNew.this.getState(), ((GUIDialogWindow)var4x.getInputPanel().background).getMainContentPane().getContent(1))).onInit();
                     ((GUIDialogWindow)var4x.getInputPanel().background).getMainContentPane().getContent(1).attach(var3);
                     var4x.getInputPanel().setOkButton(false);
                     var4x.getInputPanel().setOkButtonText(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_43);
                     var5.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_50, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
                        public void callback(GUIElement var1, MouseEvent var2) {
                           if (var2.pressedLeftMouse()) {
                              var3.setResources(!var3.isResources());
                              ((GameClientState)CatalogScrollableListNew.this.getState()).getPlayer().sendSimpleCommand(SimplePlayerCommands.REQUEST_BLUEPRINT_ITEM_LIST, var4.getUid());
                           }

                        }

                        public boolean isOccluded() {
                           return false;
                        }
                     }, new GUIActivationCallback() {
                        public boolean isVisible(InputState var1) {
                           return true;
                        }

                        public boolean isActive(InputState var1) {
                           return true;
                        }
                     });
                     var4x.activate();
                  }

               }
            });
            GUITextButton var28 = new GUITextButton(this.getState(), 94, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_19, new GUICallback() {
               public boolean isOccluded() {
                  return !CatalogScrollableListNew.this.isActive();
               }

               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse() && CatalogScrollableListNew.this.isPlayerAdmin()) {
                     CatalogScrollableListNew.this.load(var4);
                  }

               }
            });
            GUITextButton var29 = new GUITextButton(this.getState(), 64, 24, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_20, new GUICallback() {
               public boolean isOccluded() {
                  return !CatalogScrollableListNew.this.isActive();
               }

               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse() && CatalogScrollableListNew.this.canEdit(var4)) {
                     CatalogScrollableListNew.this.deleteEntry(var4);
                  }

               }
            });
            GUITextButton var14 = new GUITextButton(this.getState(), 120, 24, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_21, new GUICallback() {
               public boolean isOccluded() {
                  return !CatalogScrollableListNew.this.isActive();
               }

               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse() && CatalogScrollableListNew.this.isPlayerAdmin()) {
                     CatalogScrollableListNew.this.changeOwner(var4);
                  }

               }
            });
            GUIAncor var15;
            (var15 = new GUIAncor(this.getState(), 100.0F, 32.0F) {
               public void draw() {
                  this.getPos().y = var18.l.getList().getHeight();
                  super.draw();
               }
            }).attach(var22);
            var15.attach(var27);
            if (this.canEdit(var4)) {
               var15.attach(var25);
               var15.attach(var26);
               var15.attach(var29);
            }

            if (this.isPlayerAdmin()) {
               var15.attach(var28);
               var15.attach(var14);
            }

            var22.setPos(0.0F, 0.0F, 0.0F);
            var25.setPos(var22.getWidth() + 8.0F, 0.0F, 0.0F);
            var27.setPos(var22.getWidth() + 8.0F + var25.getWidth() + 8.0F, 0.0F, 0.0F);
            var26.setPos(var27.getWidth() + 8.0F + var22.getWidth() + 8.0F + var25.getWidth() + 8.0F, 0.0F, 0.0F);
            var29.setPos(var27.getWidth() + 8.0F + var22.getWidth() + 8.0F + var25.getWidth() + 8.0F + var26.getWidth() + 8.0F, 0.0F, 0.0F);
            var28.setPos(var27.getWidth() + 8.0F + var22.getWidth() + 8.0F + var25.getWidth() + 8.0F + var26.getWidth() + 8.0F + var29.getWidth() + 8.0F, 0.0F, 0.0F);
            var14.setPos(var27.getWidth() + 8.0F + var22.getWidth() + 8.0F + var25.getWidth() + 8.0F + var26.getWidth() + 8.0F + var29.getWidth() + 8.0F + var28.getWidth() + 8.0F, 0.0F, 0.0F);
            var20.attach(var23);
            var15.getPos().y = var20.getHeight();
            var18.expanded.add(new GUIListElement(var20, var20, this.getState()));
            if (var4.score != null) {
               var20 = new GUIAncor(this.getState(), 100.0F, 128.0F) {
                  public void draw() {
                     this.setWidth(var18.l.getInnerTextbox().getWidth());
                     super.draw();
                  }
               };
               var7 = new GUITextOverlayTable(10, 10, this.getState());
               ObjectArrayList var24 = new ObjectArrayList();
               var4.score.addStrings(var24);
               var7.setText(var24);
               var7.setPos(4.0F, 0.0F, 0.0F);
               var20.attach(var7);
               GUIPolygonStats var17 = new GUIPolygonStats(this.getState(), var4.score) {
                  public void draw() {
                     this.setPos(var20.getWidth() - this.getWidth() * 2.0F, -40.0F, 0.0F);
                     super.draw();
                  }
               };
               var20.attach(var17);
               var18.expanded.add(new GUIListElement(var20, var20, this.getState()));
            }

            var18.expanded.attach(var15);
         }

         var18.onInit();
         var1.addWithoutUpdate(var18);
      }

      var1.updateDim();
   }

   public boolean isPlayerAdmin() {
      return ((GameClientState)this.getState()).getPlayer().getNetworkObject().isAdminClient.get();
   }

   public boolean canEdit(CatalogPermission var1) {
      return var1.ownerUID.toLowerCase(Locale.ENGLISH).equals(((GameClientState)this.getState()).getPlayer().getName().toLowerCase(Locale.ENGLISH)) || this.isPlayerAdmin();
   }

   private void changeOwner(final CatalogPermission var1) {
      String var2 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_22, var1.getUid());
      PlayerGameTextInput var3;
      (var3 = new PlayerGameTextInput("CatalogExtendedPanel_changeOwner", (GameClientState)this.getState(), 50, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_23, var2, var1.ownerUID) {
         public void onDeactivate() {
         }

         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1x, TextCallback var2, String var3) {
            return var1x;
         }

         public boolean isOccluded() {
            return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
         }

         public void onFailedTextCheck(String var1x) {
            this.setErrorMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_24, var1x));
         }

         public boolean onInput(String var1x) {
            if (this.getState().getPlayer().getNetworkObject().isAdminClient.get()) {
               CatalogPermission var2;
               (var2 = new CatalogPermission(var1)).ownerUID = var1x;
               var2.changeFlagForced = true;
               this.getState().getCatalogManager().clientRequestCatalogEdit(var2);
            } else {
               System.err.println("ERROR: CANNOT CHANGE OWNER (PERMISSION DENIED)");
            }

            return true;
         }
      }).setInputChecker(new InputChecker() {
         public boolean check(String var1, TextCallback var2) {
            if (EntityRequest.isShipNameValid(var1)) {
               return true;
            } else {
               var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_44);
               return false;
            }
         }
      });
      var3.activate();
   }

   private void deleteEntry(final CatalogPermission var1) {
      if (!((GameClientState)this.getState()).getPlayer().getNetworkObject().isAdminClient.get() && !((GameClientState)this.getState()).getPlayer().getName().toLowerCase(Locale.ENGLISH).equals(var1.ownerUID.toLowerCase(Locale.ENGLISH))) {
         ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_45, 0.0F);
      } else {
         (new PlayerGameOkCancelInput("CatalogScrollableListNew_deleteEntry", (GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_46, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_47) {
            public boolean isOccluded() {
               return false;
            }

            public void onDeactivate() {
            }

            public void pressedOK() {
               CatalogPermission var1x;
               if (this.getState().getPlayer().getNetworkObject().isAdminClient.get()) {
                  (var1x = new CatalogPermission(var1)).changeFlagForced = true;
                  this.getState().getCatalogManager().clientRequestCatalogRemove(var1x);
               } else {
                  (var1x = new CatalogPermission(var1)).ownerUID = this.getState().getPlayer().getName();
                  this.getState().getCatalogManager().clientRequestCatalogRemove(var1x);
               }

               this.deactivate();
            }
         }).activate();
      }
   }

   private void load(final CatalogPermission var1) {
      String var2 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_31;
      PlayerGameTextInput var3;
      (var3 = new PlayerGameTextInput("CatalogScrollableListNew_f_load", (GameClientState)this.getState(), 400, 240, 50, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_32, var2, var1.getUid() + "_" + System.currentTimeMillis()) {
         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1x, TextCallback var2, String var3) {
            return var1x;
         }

         public boolean isOccluded() {
            return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
         }

         public void onDeactivate() {
         }

         public void onFailedTextCheck(String var1x) {
            this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_48 + " " + var1x);
         }

         public boolean onInput(String var1x) {
            if (this.getState().getCharacter() != null && this.getState().getCharacter().getPhysicsDataContainer() != null && this.getState().getCharacter().getPhysicsDataContainer().isInitialized()) {
               System.err.println("[CLIENT] BUYING CATALOG ENTRY: " + var1.getUid() + " FOR " + this.getState().getPlayer().getNetworkObject());
               BlueprintPlayerHandleRequest var2;
               (var2 = new BlueprintPlayerHandleRequest()).catalogName = var1.getUid();
               var2.entitySpawnName = var1x;
               var2.save = false;
               var2.toSaveShip = -1;
               var2.directBuy = true;
               var2.setOwnFaction = CatalogScrollableListNew.this.useOwnFaction;
               if (CatalogScrollableListNew.this.spawnDocked && BuildModeDrawer.currentPiece != null && BuildModeDrawer.currentPiece.isValid() && BuildModeDrawer.currentPiece.getInfo().isRailDockable()) {
                  var2.spawnOnId = BuildModeDrawer.currentPiece.getSegmentController().getId();
                  var2.spawnOnBlock = BuildModeDrawer.currentPiece.getAbsoluteIndex();
               }

               this.getState().getController().sendAdminCommand(CatalogScrollableListNew.this.spawnDocked ? AdminCommands.LOAD_AS_FACTION_DOCKED : AdminCommands.LOAD_AS_FACTION, var2.catalogName, var2.entitySpawnName, var2.setOwnFaction ? this.getState().getPlayer().getFactionId() : 0);
               return true;
            } else {
               System.err.println("[ERROR] Character might not have been initialized");
               return false;
            }
         }
      }).setInputChecker(new InputChecker() {
         public boolean check(String var1, TextCallback var2) {
            if (EntityRequest.isShipNameValid(var1)) {
               return true;
            } else {
               var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_34);
               return false;
            }
         }
      });
      var3.getInputPanel().onInit();
      GUICheckBoxTextPair var4;
      (var4 = new GUICheckBoxTextPair(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_35, 280, FontLibrary.getBlenderProMedium14(), 24) {
         public boolean isActivated() {
            return CatalogScrollableListNew.this.useOwnFaction;
         }

         public void deactivate() {
            CatalogScrollableListNew.this.useOwnFaction = false;
         }

         public void activate() {
            if (((GameClientState)this.getState()).getPlayer().getFactionId() > 0) {
               CatalogScrollableListNew.this.useOwnFaction = true;
            } else {
               ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_36, 0.0F);
               CatalogScrollableListNew.this.useOwnFaction = false;
            }
         }
      }).setPos(3.0F, 35.0F, 0.0F);
      ((GUIDialogWindow)var3.getInputPanel().background).getMainContentPane().getContent(0).attach(var4);
      (var4 = new GUICheckBoxTextPair(this.getState(), new Object() {
         public String toString() {
            return BuildModeDrawer.currentPiece != null && BuildModeDrawer.currentPiece.isValid() && BuildModeDrawer.currentPiece.getInfo().isRailDockable() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_52 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_53;
         }
      }, 280, FontLibrary.getBlenderProMedium14(), 24) {
         public boolean isActivated() {
            return CatalogScrollableListNew.this.spawnDocked;
         }

         public void deactivate() {
            CatalogScrollableListNew.this.spawnDocked = false;
         }

         public void activate() {
            System.err.println("LOAD DOCKED: " + BuildModeDrawer.currentPiece);
            if (BuildModeDrawer.currentPiece != null && BuildModeDrawer.currentPiece.isValid() && BuildModeDrawer.currentPiece.getInfo().isRailDockable()) {
               CatalogScrollableListNew.this.spawnDocked = true;
            } else {
               ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_57, 0.0F);
               CatalogScrollableListNew.this.spawnDocked = false;
            }
         }
      }).setPos(3.0F, 65.0F, 0.0F);
      ((GUIDialogWindow)var3.getInputPanel().background).getMainContentPane().getContent(0).attach(var4);
      var3.activate();
   }

   private void buyEntry(final CatalogPermission var1) {
      if (!((GameClientState)this.getState()).isInShopDistance()) {
         ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_30, 0.0F);
      }

      String var2 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_25;
      PlayerGameTextInput var3;
      (var3 = new PlayerGameTextInput("CatalogScrollableListNew_f_NewShip", (GameClientState)this.getState(), 400, 240, 50, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_26, var2, var1.getUid() + "_" + System.currentTimeMillis()) {
         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1x, TextCallback var2, String var3) {
            return var1x;
         }

         public boolean isOccluded() {
            return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
         }

         public void onDeactivate() {
         }

         public void onFailedTextCheck(String var1x) {
            this.setErrorMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_33, var1x));
         }

         public boolean onInput(String var1x) {
            if (this.getState().getCharacter() != null && this.getState().getCharacter().getPhysicsDataContainer() != null && this.getState().getCharacter().getPhysicsDataContainer().isInitialized()) {
               System.err.println("[CLIENT] BUYING CATALOG ENTRY: " + var1.getUid() + " FOR " + this.getState().getPlayer().getNetworkObject());
               BlueprintPlayerHandleRequest var2;
               (var2 = new BlueprintPlayerHandleRequest()).catalogName = var1.getUid();
               var2.entitySpawnName = var1x;
               var2.save = false;
               var2.toSaveShip = -1;
               var2.directBuy = true;
               var2.setOwnFaction = CatalogScrollableListNew.this.useOwnFaction;
               this.getState().getPlayer().getNetworkObject().catalogPlayerHandleBuffer.add(new RemoteBlueprintPlayerRequest(var2, false));
               return true;
            } else {
               System.err.println("[ERROR] Character might not have been initialized");
               return false;
            }
         }
      }).setInputChecker(new InputChecker() {
         public boolean check(String var1, TextCallback var2) {
            if (EntityRequest.isShipNameValid(var1)) {
               return true;
            } else {
               var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_27);
               return false;
            }
         }
      });
      var3.getInputPanel().onInit();
      GUICheckBoxTextPair var4;
      (var4 = new GUICheckBoxTextPair(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_28, 280, FontLibrary.getBlenderProMedium14(), 24) {
         public boolean isActivated() {
            return CatalogScrollableListNew.this.useOwnFaction;
         }

         public void deactivate() {
            CatalogScrollableListNew.this.useOwnFaction = false;
         }

         public void activate() {
            if (((GameClientState)this.getState()).getPlayer().getFactionId() > 0) {
               CatalogScrollableListNew.this.useOwnFaction = true;
            } else {
               ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_29, 0.0F);
               CatalogScrollableListNew.this.useOwnFaction = false;
            }
         }
      }).setPos(3.0F, 35.0F, 0.0F);
      ((GUIDialogWindow)var3.getInputPanel().background).getMainContentPane().getContent(0).attach(var4);
      (var4 = new GUICheckBoxTextPair(this.getState(), new Object() {
         public String toString() {
            return BuildModeDrawer.currentPiece != null && BuildModeDrawer.currentPiece.isValid() && BuildModeDrawer.currentPiece.getInfo().isRailDockable() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_55 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_56;
         }
      }, 280, FontLibrary.getBlenderProMedium14(), 24) {
         public boolean isActivated() {
            return CatalogScrollableListNew.this.spawnDocked;
         }

         public void deactivate() {
            CatalogScrollableListNew.this.spawnDocked = false;
         }

         public void activate() {
            if (BuildModeDrawer.currentPiece != null && BuildModeDrawer.currentPiece.isValid() && BuildModeDrawer.currentPiece.getInfo().isRailDockable()) {
               CatalogScrollableListNew.this.spawnDocked = true;
            } else {
               ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_54, 0.0F);
               CatalogScrollableListNew.this.spawnDocked = false;
            }
         }
      }).setPos(3.0F, 65.0F, 0.0F);
      ((GUIDialogWindow)var3.getInputPanel().background).getMainContentPane().getContent(0).attach(var4);
      var3.activate();
   }

   private void buyEntryAsMeta(final CatalogPermission var1) {
      if (!((GameClientState)this.getState()).isInShopDistance()) {
         ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_37, 0.0F);
      }

      String var2 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_38, var1.getUid());
      String var3 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_39;
      if (var1.getEntry() == BlueprintType.SPACE_STATION) {
         var3 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_40, StringTools.formatSeperated(((GameStateInterface)this.getState()).getGameState().getStationCost()), StringTools.formatSeperated(((GameClientState)this.getState()).getPlayer().getCredits()));
      }

      var3 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGSCROLLABLELISTNEW_41, var3);
      (new PlayerGameOkCancelInput("CatalogScrollableListNew_buyEntryAsMeta", (GameClientState)this.getState(), var2, var3) {
         public boolean isOccluded() {
            return false;
         }

         public void pressedOK() {
            BlueprintPlayerHandleRequest var1x;
            (var1x = new BlueprintPlayerHandleRequest()).catalogName = var1.getUid();
            var1x.entitySpawnName = "";
            var1x.save = false;
            var1x.toSaveShip = -1;
            var1x.directBuy = false;
            if (CatalogScrollableListNew.this.spawnDocked && BuildModeDrawer.currentPiece != null && BuildModeDrawer.currentPiece.isValid() && BuildModeDrawer.currentPiece.getInfo().isRailDockable()) {
               var1x.spawnOnId = BuildModeDrawer.currentPiece.getSegmentController().getId();
               var1x.spawnOnBlock = BuildModeDrawer.currentPiece.getAbsoluteIndex();
            }

            this.getState().getPlayer().getNetworkObject().catalogPlayerHandleBuffer.add(new RemoteBlueprintPlayerRequest(var1x, false));
            this.deactivate();
         }

         public void onDeactivate() {
         }
      }).activate();
   }

   class CatalogRow extends ScrollableTableList.Row {
      public CatalogRow(InputState var2, CatalogPermission var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }

      public float getExtendedHighlightBottomDist() {
         CatalogScrollableListNew.this.isPlayerAdmin();
         return 32.0F;
      }

      protected boolean isSimpleSelected() {
         return CatalogScrollableListNew.this.selectSingle && CatalogScrollableListNew.this.selectedSingle == this.f;
      }

      protected void clickedOnRow() {
         CatalogScrollableListNew.this.selectedSingle = (CatalogPermission)this.f;
         super.clickedOnRow();
      }
   }
}
