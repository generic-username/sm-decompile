package org.schema.game.client.view.gui.catalog.newcatalog;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import javax.vecmath.Vector3f;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.ship.InShipControlManager;
import org.schema.game.client.controller.manager.ingame.ship.WeaponAssignControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;
import org.schema.schine.resource.FileExt;

public class CatalogBattleScrollableListNew extends ScrollableTableList implements Observer {
   public final ObjectOpenHashSet currentList;
   private static final Vector3f dir = new Vector3f();
   private static final Vector3f dir1 = new Vector3f();
   private static final Vector3f dir2 = new Vector3f();
   public final List list;
   private boolean factionOption;

   public CatalogBattleScrollableListNew(InputState var1, GUIElement var2, boolean var3) {
      super(var1, 100.0F, 100.0F, var2);
      this.currentList = new ObjectOpenHashSet();
      this.list = new ObjectArrayList();
      this.columnsHeight = 32;
      this.factionOption = var3;
   }

   public CatalogBattleScrollableListNew(GameClientState var1, GUIAncor var2) {
      this(var1, var2, true);
   }

   public static void load(String var0, CatalogBattleScrollableListNew var1) {
      FileExt var16 = new FileExt(var0);
      DataInputStream var2 = null;
      boolean var10 = false;

      label88: {
         try {
            var10 = true;
            (var2 = new DataInputStream(new BufferedInputStream(new FileInputStream(var16)))).readInt();
            int var17 = var2.readInt();
            var1.currentList.clear();

            for(int var3 = 0; var3 < var17; ++var3) {
               CatalogBattleRowObject var4 = new CatalogBattleRowObject(var2.readUTF(), var2.readInt(), var2.readInt());
               var1.currentList.add(var4);
            }

            var10 = false;
            break label88;
         } catch (IOException var14) {
            var14.printStackTrace();
            var10 = false;
         } finally {
            if (var10) {
               try {
                  if (var2 != null) {
                     var2.close();
                  }
               } catch (IOException var11) {
                  var11.printStackTrace();
               }

            }
         }

         try {
            if (var2 != null) {
               var2.close();
            }

            return;
         } catch (IOException var12) {
            var12.printStackTrace();
            return;
         }
      }

      try {
         var2.close();
      } catch (IOException var13) {
         var13.printStackTrace();
      }
   }

   public static boolean write(String var0, CatalogBattleScrollableListNew var1) {
      FileExt var14;
      if ((var14 = new FileExt(var0)).isDirectory()) {
         var14.delete();
      }

      if (var14.getParentFile() != null) {
         var14.getParentFile().mkdirs();
      }

      DataOutputStream var2 = null;
      boolean var8 = false;

      label115: {
         try {
            var8 = true;
            (var2 = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(var14)))).writeInt(0);
            var2.writeInt(var1.currentList.size());
            Iterator var15 = var1.currentList.iterator();

            while(var15.hasNext()) {
               CatalogBattleRowObject var16 = (CatalogBattleRowObject)var15.next();
               var2.writeUTF(var16.catId);
               var2.writeInt(var16.faction);
               var2.writeInt(var16.amount);
            }

            var8 = false;
            break label115;
         } catch (IOException var12) {
            var12.printStackTrace();
            var8 = false;
         } finally {
            if (var8) {
               try {
                  if (var2 != null) {
                     var2.close();
                  }
               } catch (IOException var9) {
                  var9.printStackTrace();
               }

            }
         }

         try {
            if (var2 != null) {
               var2.close();
            }
         } catch (IOException var10) {
            var10.printStackTrace();
         }

         return false;
      }

      try {
         var2.close();
      } catch (IOException var11) {
         var11.printStackTrace();
      }

      return true;
   }

   public void cleanUp() {
      super.cleanUp();
   }

   public void onInit() {
      super.onInit();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGBATTLESCROLLABLELISTNEW_0, 6.0F, new Comparator() {
         public int compare(CatalogBattleRowObject var1, CatalogBattleRowObject var2) {
            return var1.catId.compareToIgnoreCase(var2.catId);
         }
      }, true);
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGBATTLESCROLLABLELISTNEW_12, 1.0F, new Comparator() {
         public int compare(CatalogBattleRowObject var1, CatalogBattleRowObject var2) {
            return var1.faction - var2.faction;
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGBATTLESCROLLABLELISTNEW_2, 1.0F, new Comparator() {
         public int compare(CatalogBattleRowObject var1, CatalogBattleRowObject var2) {
            if (var1.amount > var2.amount) {
               return 1;
            } else {
               return var1.amount < var2.amount ? -1 : 0;
            }
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGBATTLESCROLLABLELISTNEW_3, 240, new Comparator() {
         public int compare(CatalogBattleRowObject var1, CatalogBattleRowObject var2) {
            return 0;
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, CatalogBattleRowObject var2) {
            return var2.catId.toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGBATTLESCROLLABLELISTNEW_4, ControllerElement.FilterRowStyle.FULL);
   }

   protected Collection getElementList() {
      this.updateList();
      return this.list;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      this.getState().getGameState().getFactionManager();
      this.getState().getGameState().getCatalogManager();
      this.getState().getPlayer();
      Iterator var11 = var2.iterator();

      while(var11.hasNext()) {
         final CatalogBattleRowObject var3 = (CatalogBattleRowObject)var11.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6;
         (var6 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var3.amount);
            }
         });
         var5.setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var3.faction);
            }
         });
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         var4.setTextSimple(var3.catId);
         var4.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;

         assert !var4.getText().isEmpty();

         assert !var6.getText().isEmpty();

         GUIAncor var13 = new GUIAncor(this.getState(), 120.0F, (float)this.columnsHeight);
         GUITextButton var8 = new GUITextButton(this.getState(), 80, 22, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGBATTLESCROLLABLELISTNEW_5, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse() && var2.pressedLeftMouse()) {
                  CatalogBattleScrollableListNew.this.openAmountDialog(var3);
               }

            }

            public boolean isOccluded() {
               return !CatalogBattleScrollableListNew.this.isActive();
            }
         });
         GUITextButton var9 = new GUITextButton(this.getState(), 80, 22, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGBATTLESCROLLABLELISTNEW_6, new GUICallback() {
            public boolean isOccluded() {
               return !CatalogBattleScrollableListNew.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  CatalogBattleScrollableListNew.this.openFactionDialog(var3);
               }

            }
         });
         GUITextButton var10 = new GUITextButton(this.getState(), 30, 22, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGBATTLESCROLLABLELISTNEW_7, new GUICallback() {
            public boolean isOccluded() {
               return false;
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  CatalogBattleScrollableListNew.this.apply(var3, 0, var3.faction);
               }

            }
         });
         var8.setPos(2.0F, 2.0F, 0.0F);
         var9.setPos(2.0F + var8.getWidth() + 6.0F, 2.0F, 0.0F);
         var10.setPos(2.0F + var9.getWidth() + 6.0F + var8.getWidth() + 6.0F, 2.0F, 0.0F);
         var13.attach(var8);
         if (this.factionOption) {
            var13.attach(var9);
         }

         var13.attach(var10);
         CatalogBattleScrollableListNew.InventoryStashRow var12;
         (var12 = new CatalogBattleScrollableListNew.InventoryStashRow(this.getState(), var3, new GUIElement[]{var7, var5, var6, var13})).onInit();
         var1.addWithoutUpdate(var12);
      }

      var1.updateDim();
   }

   public boolean isPlayerAdmin() {
      return this.getState().getPlayer().getNetworkObject().isAdminClient.get();
   }

   public boolean canEdit(CatalogPermission var1) {
      return var1.ownerUID.toLowerCase(Locale.ENGLISH).equals(this.getState().getPlayer().getName().toLowerCase(Locale.ENGLISH)) || this.isPlayerAdmin();
   }

   public WeaponAssignControllerManager getAssignWeaponControllerManager() {
      return this.getPlayerGameControlManager().getWeaponControlManager();
   }

   public InShipControlManager getInShipControlManager() {
      return this.getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   private PlayerGameControlManager getPlayerGameControlManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   public void updateList() {
      this.list.clear();
      this.getState();
      Iterator var1 = this.currentList.iterator();

      while(var1.hasNext()) {
         CatalogBattleRowObject var2 = (CatalogBattleRowObject)var1.next();
         this.list.add(var2);
      }

   }

   public void apply(CatalogBattleRowObject var1, int var2, int var3) {
      if (var1 != null) {
         if (var2 == 0) {
            this.currentList.remove(var1);
         } else {
            this.currentList.remove(var1);
            var1.amount = var2;
            var1.faction = var3;
            this.currentList.add(var1);
         }

         this.flagDirty();
      }

   }

   private void openAmountDialog(final CatalogBattleRowObject var1) {
      (new PlayerGameTextInput("sstionScrollableListNew_AMOUNT", this.getState(), 8, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGBATTLESCROLLABLELISTNEW_8, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGBATTLESCROLLABLELISTNEW_9, String.valueOf(var1.amount)) {
         public boolean isOccluded() {
            return false;
         }

         public String[] getCommandPrefixes() {
            return null;
         }

         public void onFailedTextCheck(String var1x) {
         }

         public void onDeactivate() {
         }

         public String handleAutoComplete(String var1x, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public boolean onInput(String var1x) {
            try {
               int var3;
               if ((var3 = Integer.parseInt(var1x)) >= 0) {
                  CatalogBattleScrollableListNew.this.apply(var1, var3, var1.faction);
                  return true;
               }

               this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGBATTLESCROLLABLELISTNEW_10);
            } catch (NumberFormatException var2) {
               this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGBATTLESCROLLABLELISTNEW_11);
            }

            return false;
         }
      }).activate();
   }

   private void openFactionDialog(final CatalogBattleRowObject var1) {
      (new PlayerGameTextInput("sstionScrollableListNew_AMOUNT", this.getState(), 16, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGBATTLESCROLLABLELISTNEW_13, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGBATTLESCROLLABLELISTNEW_1, String.valueOf(var1.faction)) {
         public boolean isOccluded() {
            return false;
         }

         public void onFailedTextCheck(String var1x) {
         }

         public String handleAutoComplete(String var1x, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public String[] getCommandPrefixes() {
            return null;
         }

         public boolean onInput(String var1x) {
            try {
               int var3 = Integer.parseInt(var1x);
               CatalogBattleScrollableListNew.this.apply(var1, var1.amount, var3);
               return true;
            } catch (NumberFormatException var2) {
               this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGBATTLESCROLLABLELISTNEW_14);
               return false;
            }
         }

         public void onDeactivate() {
         }
      }).activate();
   }

   class InventoryStashRow extends ScrollableTableList.Row {
      public InventoryStashRow(InputState var2, CatalogBattleRowObject var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
