package org.schema.game.client.view.gui.catalog.newcatalog;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.meta.BlueprintMetaItem;

public class TypeRowItem {
   public final ElementInformation info;
   public final int itemId;
   private GameClientState state;

   public TypeRowItem(ElementInformation var1, int var2, GameClientState var3) {
      this.info = var1;
      this.itemId = var2;
      this.state = var3;
   }

   public BlueprintMetaItem getItem() {
      return (BlueprintMetaItem)this.state.getMetaObjectManager().getObject(this.itemId);
   }

   public int getProgress() {
      return this.getItem().progress.get(this.info.getId());
   }

   public int getGoal() {
      return this.getItem().goal.get(this.info.getId());
   }

   public float getPercent() {
      return (float)this.getProgress() / (float)this.getGoal();
   }

   public int hashCode() {
      return this.info.hashCode();
   }

   public boolean equals(Object var1) {
      return var1 instanceof TypeRowItem && this.info.equals(((TypeRowItem)var1).info);
   }
}
