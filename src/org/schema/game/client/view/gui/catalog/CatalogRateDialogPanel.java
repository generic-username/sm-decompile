package org.schema.game.client.view.gui.catalog;

import javax.vecmath.Vector4f;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.input.InputState;

public class CatalogRateDialogPanel extends GUIInputPanel {
   private GUICallback selectedCallback;

   public CatalogRateDialogPanel(InputState var1, GUICallback var2, CatalogPermission var3) {
      super("CatalogRateDialogPanel", var1, var2, "Rate", "Rate " + var3.getUid());
      this.setOkButton(false);
      this.selectedCallback = var2;
   }

   public void onInit() {
      super.onInit();

      for(int var1 = 0; var1 < 10; ++var1) {
         float var2 = (float)var1 / 10.0F;
         GUITextButton var4;
         (var4 = new GUITextButton(this.getState(), 30, 30, new Vector4f(1.0F - var2, var2, 0.2F, 1.0F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getBoldArial24(), String.valueOf(var1 + 1), this.selectedCallback)).setUserPointer(new Integer(var1));
         if (var1 < 9) {
            var4.setTextPos(6, 0);
         }

         int var3 = var1 * 40;
         var4.setPos((float)var3, 35.0F, 0.0F);
         this.getContent().attach(var4);
      }

   }
}
