package org.schema.game.client.view.gui.catalog.newcatalog;

import java.util.Iterator;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.game.common.data.player.catalog.CatalogWavePermission;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.font.FontStyle;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;

public class PlayerWaveManagerInput extends PlayerGameOkCancelInput {
   private int factionValue;
   private int diff;
   private CatalogBattleScrollableListNew scrl;
   private WaveScrollableListNew wfl;

   public PlayerWaveManagerInput(String var1, GameClientState var2, GUIElement var3) {
      super(var1, var2, 500, 500, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_PLAYERWAVEMANAGERINPUT_0, "");
      this.getInputPanel().setCancelButton(false);
      this.getInputPanel().onInit();
      ((GUIDialogWindow)this.getInputPanel().getBackground()).getMainContentPane().setTextBoxHeightLast(100);
      ((GUIDialogWindow)this.getInputPanel().getBackground()).getMainContentPane().addNewTextBox(100);
      this.wfl = new WaveScrollableListNew(this.getState(), ((GUIDialogWindow)this.getInputPanel().getBackground()).getMainContentPane().getContent(1));
      this.wfl.onInit();
      ((GUIDialogWindow)this.getInputPanel().getBackground()).getMainContentPane().getContent(1).attach(this.wfl);
      GUITextButton var4;
      (var4 = new GUITextButton(this.getState(), 60, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_PLAYERWAVEMANAGERINPUT_1, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && PlayerWaveManagerInput.this.getState().getPlayerInputs().get(PlayerWaveManagerInput.this.getState().getPlayerInputs().size() - 1) == PlayerWaveManagerInput.this) {
               PlayerWaveManagerInput.this.openBattleDialog();
            }

         }
      })).setPos(5.0F, 5.0F, 0.0F);
      ((GUIDialogWindow)this.getInputPanel().getBackground()).getMainContentPane().getContent(0).attach(var4);
   }

   public void onDeactivate() {
   }

   private void openBattleDialog() {
      final PlayerGameOkCancelInput var1;
      (var1 = new PlayerGameOkCancelInput("ADMIN_BATTLE_POPUP", this.getState(), 640, 400, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_PLAYERWAVEMANAGERINPUT_2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_PLAYERWAVEMANAGERINPUT_3, FontStyle.small) {
         public boolean isOccluded() {
            return this.getState().getPlayerInputs().get(this.getState().getPlayerInputs().size() - 1) != this;
         }

         public void pressedOK() {
            Iterator var1 = PlayerWaveManagerInput.this.scrl.list.iterator();

            while(var1.hasNext()) {
               CatalogBattleRowObject var2 = (CatalogBattleRowObject)var1.next();
               Iterator var3 = this.getState().getCatalogManager().getCatalog().iterator();

               while(var3.hasNext()) {
                  CatalogPermission var4;
                  if ((var4 = (CatalogPermission)var3.next()).getUid().equals(var2.catId)) {
                     CatalogWavePermission var5;
                     (var5 = new CatalogWavePermission()).amount = var2.amount;
                     var5.factionId = PlayerWaveManagerInput.this.factionValue;
                     var5.difficulty = (short)PlayerWaveManagerInput.this.diff;
                     var4.wavePermissions.remove(var5);
                     var4.wavePermissions.add(var5);
                     var4.changeFlagForced = true;
                     this.getState().getCatalogManager().clientRequestCatalogEdit(var4);
                     System.err.println("ADDED WAVE PERMISSION: " + var5 + " TO " + var4);
                  }
               }
            }

            PlayerWaveManagerInput.this.wfl.flagDirty();
            this.deactivate();
         }

         public void onDeactivate() {
         }
      }).getInputPanel().setCancelButton(false);
      var1.getInputPanel().onInit();
      ((GUIDialogWindow)var1.getInputPanel().getBackground()).getMainContentPane().setTextBoxHeightLast(120);
      ((GUIDialogWindow)var1.getInputPanel().getBackground()).getMainContentPane().addNewTextBox(100);
      this.scrl = new CatalogBattleScrollableListNew(this.getState(), ((GUIDialogWindow)var1.getInputPanel().getBackground()).getMainContentPane().getContent(1), false);
      this.scrl.onInit();
      ((GUIDialogWindow)var1.getInputPanel().getBackground()).getMainContentPane().getContent(1).attach(this.scrl);
      GUITextButton var2 = new GUITextButton(this.getState(), 60, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_PLAYERWAVEMANAGERINPUT_4, new GUICallback() {
         private int numberValue = 1;
         private CatalogScrollableListNew select;

         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1x, MouseEvent var2) {
            if (var2.pressedLeftMouse() && PlayerWaveManagerInput.this.getState().getPlayerInputs().get(PlayerWaveManagerInput.this.getState().getPlayerInputs().size() - 1) == var1) {
               PlayerGameOkCancelInput var3;
               (var3 = new PlayerGameOkCancelInput("CHSOE_CAT", PlayerWaveManagerInput.this.getState(), 400, 300, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_PLAYERWAVEMANAGERINPUT_5, "") {
                  public void onDeactivate() {
                  }

                  public void pressedOK() {
                     if (select.selectedSingle != null) {
                        CatalogPermission var1x = select.selectedSingle;
                        PlayerWaveManagerInput.this.scrl.currentList.add(new CatalogBattleRowObject(var1x.getUid(), 0, numberValue));
                        PlayerWaveManagerInput.this.scrl.flagDirty();
                        this.deactivate();
                     }

                  }
               }).getInputPanel().onInit();
               ((GUIDialogWindow)var3.getInputPanel().getBackground()).getMainContentPane().setTextBoxHeightLast(30);
               ((GUIDialogWindow)var3.getInputPanel().getBackground()).getMainContentPane().addNewTextBox(30);
               GUIActivatableTextBar var4;
               (var4 = new GUIActivatableTextBar(PlayerWaveManagerInput.this.getState(), FontLibrary.FontSize.MEDIUM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_PLAYERWAVEMANAGERINPUT_6, ((GUIDialogWindow)var3.getInputPanel().getBackground()).getMainContentPane().getContent(0), new TextCallback() {
                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public String handleAutoComplete(String var1x, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public void onFailedTextCheck(String var1x) {
                  }

                  public void onTextEnter(String var1x, boolean var2, boolean var3) {
                  }

                  public void newLine() {
                  }
               }, new OnInputChangedCallback() {
                  public String onInputChanged(String var1x) {
                     try {
                        numberValue = Integer.parseInt(var1x.trim());
                     } catch (NumberFormatException var2) {
                     }

                     return var1x;
                  }
               })).setPos(0.0F, 0.0F, 0.0F);
               ((GUIDialogWindow)var3.getInputPanel().getBackground()).getMainContentPane().getContent(0).attach(var4);
               this.select = new CatalogScrollableListNew(PlayerWaveManagerInput.this.getState(), ((GUIDialogWindow)var3.getInputPanel().background).getMainContentPane().getContent(1), 2, false, true);
               this.select.onInit();
               ((GUIDialogWindow)var3.getInputPanel().background).getMainContentPane().getContent(1).attach(this.select);
               var3.activate();
            }

         }
      });
      GUITextButton var3 = new GUITextButton(this.getState(), 120, 24, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_PLAYERWAVEMANAGERINPUT_7, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && PlayerWaveManagerInput.this.getState().getPlayerInputs().size() == 1) {
               PlayerWaveManagerInput.this.scrl.currentList.clear();
               PlayerWaveManagerInput.this.scrl.flagDirty();
            }

         }
      });
      GUIActivatableTextBar var4 = new GUIActivatableTextBar(this.getState(), FontLibrary.FontSize.MEDIUM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_PLAYERWAVEMANAGERINPUT_8, var1.getInputPanel().getContent(), new TextCallback() {
         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public void onFailedTextCheck(String var1) {
         }

         public void onTextEnter(String var1, boolean var2, boolean var3) {
         }

         public void newLine() {
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            try {
               PlayerWaveManagerInput.this.factionValue = Integer.parseInt(var1.trim());
            } catch (NumberFormatException var2) {
            }

            return var1;
         }
      });
      GUIActivatableTextBar var5 = new GUIActivatableTextBar(this.getState(), FontLibrary.FontSize.MEDIUM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_PLAYERWAVEMANAGERINPUT_9, var1.getInputPanel().getContent(), new TextCallback() {
         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public void onFailedTextCheck(String var1) {
         }

         public void onTextEnter(String var1, boolean var2, boolean var3) {
         }

         public void newLine() {
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            try {
               PlayerWaveManagerInput.this.diff = Integer.parseInt(var1.trim());
            } catch (NumberFormatException var2) {
            }

            return var1;
         }
      });
      var4.setPos(0.0F, 60.0F, 0.0F);
      var5.setPos(0.0F, 86.0F, 0.0F);
      var1.getInputPanel().getContent().attach(var4);
      var1.getInputPanel().getContent().attach(var5);
      var2.setPos(2.0F, 30.0F, 0.0F);
      var3.setPos(240.0F, 30.0F, 0.0F);
      var1.getInputPanel().getContent().attach(var2);
      var1.getInputPanel().getContent().attach(var3);
      var1.activate();
   }

   public void pressedOK() {
      this.deactivate();
   }
}
