package org.schema.game.client.view.gui.catalog;

import java.util.Collection;
import java.util.Observable;
import java.util.Observer;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class AccessibleCatalogPanel extends GUIElement implements Observer {
   ScrollableCatalogList list;
   private boolean needsUpdate;

   public AccessibleCatalogPanel(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.needsUpdate) {
         this.list.flagUpdate();
         this.needsUpdate = false;
      }

      this.drawAttached();
   }

   public void onInit() {
      this.list = new ScrollableCatalogList(this.getState(), false) {
         public Collection getEntries() {
            return ((GameClientState)this.getState()).getPlayer().getCatalog().getAvailableCatalog();
         }
      };
      this.list.onInit();
      this.attach(this.list);
   }

   public float getHeight() {
      return this.list.getHeight();
   }

   public float getWidth() {
      return this.list.getWidth();
   }

   public void update(Observable var1, Object var2) {
      this.needsUpdate = true;
   }
}
