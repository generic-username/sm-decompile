package org.schema.game.client.view.gui.catalog.newcatalog;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.controller.PlayerBlockStorageMetaDialog;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIBlockSprite;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.BlockStorageMetaItem;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;
import org.schema.schine.network.client.ClientState;

public class GUIBlockStorageMetaItemFillScrollableList extends ScrollableTableList implements DrawerObserver {
   ObjectArrayList items = new ObjectArrayList();
   private BlockStorageMetaItem item;
   private PlayerBlockStorageMetaDialog dialog;

   public GUIBlockStorageMetaItemFillScrollableList(InputState var1, GUIElement var2, PlayerBlockStorageMetaDialog var3, BlockStorageMetaItem var4) {
      super(var1, 100.0F, 100.0F, var2);
      this.item = var4;
      this.dialog = var3;
   }

   public void updateTypes() {
      this.items.clear();

      for(int var1 = 0; var1 < ElementKeyMap.highestType + 1; ++var1) {
         short var2;
         if (ElementKeyMap.isValidType(var2 = (short)var1)) {
            ElementKeyMap.getInfo(var2);
            ElementInformation var3 = ElementKeyMap.getInfo(var2);
            this.items.add(new TypeRowStorageItem(var3, this.item.getId(), (GameClientState)this.getState()));
         }
      }

   }

   public void cleanUp() {
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_GUIBLOCKSTORAGEMETAITEMFILLSCROLLABLELIST_0, 3.0F, new Comparator() {
         public int compare(TypeRowStorageItem var1, TypeRowStorageItem var2) {
            return var1.info.getName().compareToIgnoreCase(var2.info.getName());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_GUIBLOCKSTORAGEMETAITEMFILLSCROLLABLELIST_1, 64, new Comparator() {
         public int compare(TypeRowStorageItem var1, TypeRowStorageItem var2) {
            return var1.getCount() - var2.getCount();
         }
      }, true).reverseOrders();
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_GUIBLOCKSTORAGEMETAITEMFILLSCROLLABLELIST_2, 150, new Comparator() {
         public int compare(TypeRowStorageItem var1, TypeRowStorageItem var2) {
            return 0;
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, TypeRowStorageItem var2) {
            return var2.info.getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_GUIBLOCKSTORAGEMETAITEMFILLSCROLLABLELIST_3, ControllerElement.FilterRowStyle.FULL);
   }

   protected Collection getElementList() {
      this.updateTypes();
      return this.items;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getPlayer();
      Iterator var9 = var2.iterator();

      while(var9.hasNext()) {
         final TypeRowStorageItem var3 = (TypeRowStorageItem)var9.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUIBlockSprite var5;
         (var5 = new GUIBlockSprite(this.getState(), var3.info.getId())).setScale(0.35F, 0.35F, 0.0F);
         var4.setTextSimple(var3.info.getName());
         ScrollableTableList.GUIClippedRow var6;
         (var6 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         var4.setPos(24.0F, 5.0F, 0.0F);
         var6.attach(var4);
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var3.getCount());
            }
         });
         var4.getPos().y = 5.0F;
         GUIAncor var11 = new GUIAncor(this.getState(), 10.0F, 10.0F);
         GUITextButton var7 = new GUITextButton(this.getState(), 60, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_GUIBLOCKSTORAGEMETAITEMFILLSCROLLABLELIST_4, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  GUIBlockStorageMetaItemFillScrollableList.this.dialog.pressedGet(var3.info.getId());
               }

            }

            public boolean isOccluded() {
               return !GUIBlockStorageMetaItemFillScrollableList.this.isActive();
            }
         });
         GUITextButton var8 = new GUITextButton(this.getState(), 60, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_GUIBLOCKSTORAGEMETAITEMFILLSCROLLABLELIST_5, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  GUIBlockStorageMetaItemFillScrollableList.this.dialog.pressedAdd(var3.info.getId());
               }

            }

            public boolean isOccluded() {
               return !GUIBlockStorageMetaItemFillScrollableList.this.isActive();
            }
         }) {
            public void draw() {
               if (((ClientState)this.getState()).isAdmin()) {
                  super.draw();
               }

            }
         };
         var11.attach(var7);
         var8.getPos().set(var7.getWidth(), 0.0F, 0.0F);
         var11.attach(var8);
         GUIBlockStorageMetaItemFillScrollableList.PlayerMessageRow var10;
         (var10 = new GUIBlockStorageMetaItemFillScrollableList.PlayerMessageRow(this.getState(), var3, new GUIElement[]{var6, var4, var11})).onInit();
         var1.addWithoutUpdate(var10);
      }

      var1.updateDim();
   }

   public void update(DrawerObservable var1, Object var2, Object var3) {
      this.flagDirty();
   }

   class PlayerMessageRow extends ScrollableTableList.Row {
      public PlayerMessageRow(InputState var2, TypeRowStorageItem var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
