package org.schema.game.client.view.gui.catalog;

import java.util.Collection;
import java.util.Observable;
import java.util.Observer;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class PersonalCatalogPanel extends GUIElement implements Observer {
   private ScrollableCatalogList list;
   private CatalogToolsPanel tools;
   private boolean needsUpdate;

   public PersonalCatalogPanel(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.needsUpdate) {
         this.list.flagUpdate();
         this.needsUpdate = false;
      }

      this.drawAttached();
   }

   public void onInit() {
      this.tools = new CatalogToolsPanel(this.getState());
      this.tools.onInit();
      this.list = new ScrollableCatalogList(this.getState(), 542, (int)(340.0F - this.tools.getHeight()), false) {
         public Collection getEntries() {
            return ((GameClientState)this.getState()).getPlayer().getCatalog().getPersonalCatalog();
         }
      };
      this.list.onInit();
      this.list.getPos().y = this.tools.getHeight();
      this.attach(this.list);
      this.attach(this.tools);
   }

   public float getHeight() {
      return this.list.getHeight();
   }

   public float getWidth() {
      return this.list.getWidth();
   }

   public void update(Observable var1, Object var2) {
      this.needsUpdate = true;
   }
}
