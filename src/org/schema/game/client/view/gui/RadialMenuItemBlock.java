package org.schema.game.client.view.gui;

import javax.vecmath.Vector4f;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class RadialMenuItemBlock extends RadialMenuItem {
   private short type;

   public RadialMenuItemBlock(InputState var1, RadialMenu var2, int var3, short var4, GUIActivationCallback var5, GUICallback var6) {
      super(var1, var2, var3, var5, var6);
      this.type = var4;
   }

   protected void setColorAndPos(GUIBlockSprite var1, float var2, float var3, Vector4f var4) {
      assert var4 != null;

      assert var1 != null;

      assert var1.getColor() != null;

      var1.getColor().w = var4.w;
      var1.setPos((float)((int)((float)this.getCenterX() + var2 - var1.getWidth() / 2.0F)), (float)((int)((float)this.getCenterY() + var3 - var1.getHeight() / 2.0F)), 0.0F);
   }

   public GUIBlockSprite getLabel() {
      GUIBlockSprite var1 = new GUIBlockSprite(this.getState(), this.type);
      GUITextOverlay var2;
      (var2 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium16(), this.getState()) {
         public void draw() {
            if (RadialMenuItemBlock.this.isInside()) {
               super.draw();
            }

         }
      }).setTextSimple(ElementKeyMap.getInfo(this.type).getName());
      var2.setPos(-30.0F, (float)((int)var1.getHeight() + 3), 0.0F);
      var1.attach(var2);
      return var1;
   }
}
