package org.schema.game.client.view.gui.fleet;

import org.schema.game.common.data.fleet.Fleet;

public interface FleetSelectionInterface {
   Fleet getSelected();
}
