package org.schema.game.client.view.gui.fleet;

import org.schema.game.client.controller.PlayerButtonTilesInput;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.FleetCommandTypes;
import org.schema.game.common.data.fleet.FleetManager;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.server.data.EntityRequest;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.input.InputState;

public class FleetOptionButtons extends GUIAncor {
   private FleetPanel panel;

   public FleetOptionButtons(InputState var1, FleetPanel var2) {
      super(var1);
      this.panel = var2;
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFaction();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public void openCreateFleetDialog() {
      PlayerGameTextInput var1;
      (var1 = new PlayerGameTextInput("INFLEET", this.getState(), 64, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETOPTIONBUTTONS_0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETOPTIONBUTTONS_1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETOPTIONBUTTONS_13) {
         public void onFailedTextCheck(String var1) {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public String[] getCommandPrefixes() {
            return null;
         }

         public boolean onInput(String var1) {
            if (var1.length() <= 0) {
               this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETOPTIONBUTTONS_2, 0.0F);
               return false;
            } else {
               this.getState().getFleetManager().requestCreateFleet(var1, this.getState().getPlayer().getName());
               return true;
            }
         }

         public void onDeactivate() {
         }
      }).setInputChecker(new InputChecker() {
         public boolean check(String var1, TextCallback var2) {
            if (EntityRequest.isShipNameValid(var1)) {
               return true;
            } else {
               var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETOPTIONBUTTONS_3);
               return false;
            }
         }
      });
      var1.activate();
   }

   public FleetManager getFleetManager() {
      return this.getState().getFleetManager();
   }

   public void onInit() {
      GUIHorizontalButtonTablePane var1;
      (var1 = new GUIHorizontalButtonTablePane(this.getState(), 2, 2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETOPTIONBUTTONS_4, this)).onInit();
      var1.activeInterface = this.panel;
      var1.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETOPTIONBUTTONS_5, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && FleetOptionButtons.this.getState().getController().getPlayerInputs().isEmpty()) {
               FleetOptionButtons.this.openCreateFleetDialog();
            }

         }

         public boolean isOccluded() {
            return !FleetOptionButtons.this.isActive() || !FleetOptionButtons.this.getState().getController().getPlayerInputs().isEmpty();
         }
      }, (GUIActivationCallback)null);
      var1.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETOPTIONBUTTONS_6, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && FleetOptionButtons.this.getState().getController().getPlayerInputs().isEmpty() && FleetOptionButtons.this.getFleetManager().getSelected() != null) {
               (new PlayerGameOkCancelInput("CONFIRM", FleetOptionButtons.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETOPTIONBUTTONS_7, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETOPTIONBUTTONS_8) {
                  public void pressedOK() {
                     this.getState().getFleetManager().requestFleetRemove(FleetOptionButtons.this.getFleetManager().getSelected());
                     this.deactivate();
                  }

                  public void onDeactivate() {
                  }
               }).activate();
            }

         }

         public boolean isOccluded() {
            return !FleetOptionButtons.this.isActive() || !FleetOptionButtons.this.getState().getController().getPlayerInputs().isEmpty();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return FleetOptionButtons.this.getFleetManager().getSelected() != null;
         }
      });
      var1.addButton(0, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETOPTIONBUTTONS_9 + (this.getState().getGameState().isOnlyAddFactionToFleet() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETOPTIONBUTTONS_10 : ""), (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && FleetOptionButtons.this.getState().getController().getPlayerInputs().isEmpty() && FleetOptionButtons.this.getFleetManager().getSelected() != null) {
               (new AddShipToFleetPlayerInput(FleetOptionButtons.this.getState(), FleetOptionButtons.this.getFleetManager().getSelected())).activate();
            }

         }

         public boolean isOccluded() {
            return !FleetOptionButtons.this.isActive() || !FleetOptionButtons.this.getState().getController().getPlayerInputs().isEmpty();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return FleetOptionButtons.this.getFleetManager().getSelected() != null;
         }
      });
      var1.addButton(1, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETOPTIONBUTTONS_12, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && FleetOptionButtons.this.getState().getController().getPlayerInputs().isEmpty() && FleetOptionButtons.this.getFleetManager().getSelected() != null) {
               FleetOptionButtons.this.popupNewOrderDialog(FleetOptionButtons.this.getFleetManager().getSelected());
            }

         }

         public boolean isOccluded() {
            return !FleetOptionButtons.this.isActive() || !FleetOptionButtons.this.getState().getController().getPlayerInputs().isEmpty();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return FleetOptionButtons.this.getFleetManager().getSelected() != null;
         }
      });
      this.setPos(1.0F, 0.0F, 0.0F);
      this.attach(var1);
   }

   private void popupNewOrderDialog(Fleet var1) {
      PlayerButtonTilesInput var2 = new PlayerButtonTilesInput("FLEETORDER", this.getState(), 800, 640, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETOPTIONBUTTONS_11, 260, 165) {
         public void onDeactivate() {
         }
      };
      FleetCommandTypes[] var3;
      int var4 = (var3 = FleetCommandTypes.values()).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         var3[var5].addTile(var2, var1);
      }

      var2.activate();
   }
}
