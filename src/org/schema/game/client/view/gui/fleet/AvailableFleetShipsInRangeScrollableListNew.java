package org.schema.game.client.view.gui.fleet;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.server.data.FactionState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.CreateGUIElementInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterDropdown;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableDropDown;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class AvailableFleetShipsInRangeScrollableListNew extends ScrollableTableList implements Observer {
   private final Set selectedSegmentController = new ObjectOpenHashSet();

   public AvailableFleetShipsInRangeScrollableListNew(InputState var1, GUIElement var2) {
      super(var1, 100.0F, 100.0F, var2);
      ((GameClientController)this.getState().getController()).sectorChangeObservable.addObserver(this);
   }

   public void cleanUp() {
      ((GameClientController)this.getState().getController()).sectorChangeObservable.deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_AVAILABLEFLEETSHIPSINRANGESCROLLABLELISTNEW_3, 5.0F, new Comparator() {
         public int compare(SegmentController var1, SegmentController var2) {
            return var1.getRealName().compareToIgnoreCase(var2.getRealName());
         }
      }, true);
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_AVAILABLEFLEETSHIPSINRANGESCROLLABLELISTNEW_4, 3.0F, new Comparator() {
         public int compare(SegmentController var1, SegmentController var2) {
            FactionManager var3;
            return (var3 = ((FactionState)var1.getState()).getFactionManager()).getFactionName(var1.getFactionId()).compareTo(var3.getFactionName(var2.getFactionId()));
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_AVAILABLEFLEETSHIPSINRANGESCROLLABLELISTNEW_5, 1.0F, new Comparator() {
         public int compare(SegmentController var1, SegmentController var2) {
            Vector3i var3 = var1.getClientSector();
            Vector3i var4 = var2.getClientSector();
            if (var3 == null && var4 == null) {
               return 0;
            } else if (var3 == null) {
               return 1;
            } else {
               return var4 == null ? -1 : var3.compareTo(var4);
            }
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_AVAILABLEFLEETSHIPSINRANGESCROLLABLELISTNEW_6, 80, new Comparator() {
         public int compare(SegmentController var1, SegmentController var2) {
            return Boolean.compare(var1.railController.isDockedAndExecuted(), var2.railController.isDockedAndExecuted());
         }
      });
      this.addDropdownFilter(new GUIListFilterDropdown(new Integer[]{0, 1, 3}) {
         public boolean isOk(Integer var1, SegmentController var2) {
            switch(var1) {
            case 0:
               if (!var2.railController.isDockedAndExecuted()) {
                  return true;
               }

               return false;
            case 1:
               return var2.railController.isDockedAndExecuted();
            case 2:
               return true;
            default:
               return true;
            }
         }
      }, new CreateGUIElementInterface() {
         public GUIElement create(Integer var1) {
            GUIAncor var2 = new GUIAncor(AvailableFleetShipsInRangeScrollableListNew.this.getState(), 10.0F, 24.0F);
            GUITextOverlayTableDropDown var3 = new GUITextOverlayTableDropDown(10, 10, AvailableFleetShipsInRangeScrollableListNew.this.getState());
            switch(var1) {
            case 0:
               var3.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_AVAILABLEFLEETSHIPSINRANGESCROLLABLELISTNEW_0);
               break;
            case 1:
               var3.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_AVAILABLEFLEETSHIPSINRANGESCROLLABLELISTNEW_1);
               break;
            case 2:
               var3.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_AVAILABLEFLEETSHIPSINRANGESCROLLABLELISTNEW_2);
            }

            var3.setPos(4.0F, 4.0F, 0.0F);
            var2.setUserPointer(var1);
            var2.attach(var3);
            return var2;
         }

         public GUIElement createNeutral() {
            return null;
         }
      }, ControllerElement.FilterRowStyle.LEFT);
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, SegmentController var2) {
            return var2.getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, ControllerElement.FilterRowStyle.RIGHT);
   }

   protected Collection getElementList() {
      List var1 = ((GameClientState)this.getState()).getController().getPossibleFleetAdd();

      assert var1 != null;

      return var1;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      final FactionManager var3 = ((FactionState)this.getState()).getFactionManager();
      ((GameClientState)this.getState()).getPlayer();
      Iterator var10 = var2.iterator();

      while(var10.hasNext()) {
         final SegmentController var4 = (SegmentController)var10.next();
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var8 = new GUITextOverlayTable(10, 10, this.getState());
         ScrollableTableList.GUIClippedRow var9;
         (var9 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         var5.setTextSimple(new Object() {
            public String toString() {
               return var4.getRealName();
            }
         });
         var6.setTextSimple(new Object() {
            public String toString() {
               return var3.getFactionName(var4.getFactionId());
            }
         });
         var7.setTextSimple(new Object() {
            public String toString() {
               Vector3i var1;
               return (var1 = var4.getClientSector()) == null ? "-" : var1.toStringPure();
            }
         });
         var8.setTextSimple(new Object() {
            public String toString() {
               return var4.railController.isDockedAndExecuted() ? "X" : "";
            }
         });
         var5.getPos().y = 4.0F;
         var6.getPos().y = 4.0F;
         var7.getPos().y = 4.0F;
         var8.getPos().y = 4.0F;
         AvailableFleetShipsInRangeScrollableListNew.SegmentControllerRow var11;
         (var11 = new AvailableFleetShipsInRangeScrollableListNew.SegmentControllerRow(this.getState(), var4, new GUIElement[]{var9, var6, var7, var8})).expanded = null;
         var11.onInit();
         var1.addWithoutUpdate(var11);
      }

      var1.updateDim();
   }

   public Set getSelectedSegmentController() {
      return this.selectedSegmentController;
   }

   class SegmentControllerRow extends ScrollableTableList.Row {
      public SegmentControllerRow(InputState var2, SegmentController var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }

      protected boolean isSimpleSelected() {
         return AvailableFleetShipsInRangeScrollableListNew.this.getSelectedSegmentController().contains(this.f);
      }

      protected void clickedOnRow() {
         if (this.isSimpleSelected()) {
            AvailableFleetShipsInRangeScrollableListNew.this.getSelectedSegmentController().remove(this.f);
         } else {
            AvailableFleetShipsInRangeScrollableListNew.this.getSelectedSegmentController().add(this.f);
         }
      }
   }
}
