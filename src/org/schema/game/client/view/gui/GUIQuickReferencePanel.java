package org.schema.game.client.view.gui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITexDrawableArea;
import org.schema.schine.input.InputState;

public class GUIQuickReferencePanel extends GUIElement implements GUIActiveInterface {
   public GUIMainWindow mainPanel;
   private DialogInput diag;
   private List toCleanUp = new ObjectArrayList();
   private boolean init;

   public GUIQuickReferencePanel(InputState var1, DialogInput var2) {
      super(var1);
      this.diag = var2;
   }

   public void cleanUp() {
      Iterator var1 = this.toCleanUp.iterator();

      while(var1.hasNext()) {
         ((GUIElement)var1.next()).cleanUp();
      }

      this.toCleanUp.clear();
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.mainPanel.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      if (!this.init) {
         this.mainPanel = new GUIMainWindow(this.getState(), 880, GLFrame.getHeight() - 200, 300, 24, "QUICK_REF_DGL");
         this.mainPanel.onInit();
         this.mainPanel.clearTabs();
         String var1 = EngineSettings.LANGUAGE_PACK.getCurrentState().toString().toLowerCase(Locale.ENGLISH).startsWith("chinese") ? "infographics/power2.0/chinese/" : "infographics/power2.0/english/";
         this.createHelpTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIQUICKREFERENCEPANEL_0, var1 + "1_Reactors");
         this.createHelpTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIQUICKREFERENCEPANEL_1, var1 + "2_Chambers");
         this.createHelpTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIQUICKREFERENCEPANEL_2, var1 + "3_Shields");
         this.createHelpTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIQUICKREFERENCEPANEL_3, var1 + "4_SteathRecon");
         this.createHelpTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUIQUICKREFERENCEPANEL_4, var1 + "5_Integrity");
         this.mainPanel.activeInterface = this;
         this.mainPanel.setCloseCallback(new GUICallback() {
            public boolean isOccluded() {
               return !GUIQuickReferencePanel.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  GUIQuickReferencePanel.this.diag.deactivate();
               }

            }
         });
         this.init = true;
      }
   }

   public boolean isInside() {
      return this.mainPanel.isInside();
   }

   private GUIContentPane createHelpTab(String var1, String var2) {
      GUIContentPane var4;
      (var4 = this.mainPanel.addTab(var1)).setTextBoxHeightLast(48);
      Sprite var5 = Controller.getResLoader().getSprite(var2);
      GUITexDrawableArea var6;
      (var6 = new GUITexDrawableArea(this.getState(), var5.getMaterial().getTexture(), 0.109375F, 0.0F)).setWidth(800);
      GUIScrollablePanel var3;
      (var3 = new GUIScrollablePanel(10.0F, 10.0F, var4.getContent(0), this.getState())).setContent(var6);
      var3.onInit();
      var4.getContent(0).attach(var3);
      return var4;
   }

   public float getHeight() {
      return 0.0F;
   }

   public float getWidth() {
      return 0.0F;
   }

   public boolean isActive() {
      return this.diag.isActive();
   }
}
