package org.schema.game.client.view.gui;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyboardMappings;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class GUIHelpPanel extends GUIAncor {
   private final String title;
   private ObjectArrayList keysS = new ObjectArrayList();
   private ObjectArrayList valuesS = new ObjectArrayList();
   private ObjectArrayList keys = new ObjectArrayList();
   private ObjectArrayList values = new ObjectArrayList();
   private int valueSpacing;
   private int rows;
   private int columnDist;
   private String originalTitle;
   public static final Object2ObjectOpenHashMap translations = new Object2ObjectOpenHashMap();

   private static String getTranslation(String var0) {
      String var1;
      return (var1 = (String)translations.get(var0)) != null ? var1 : var0;
   }

   public GUIHelpPanel(InputState var1, Node var2) {
      super(var1);
      this.originalTitle = var2.getNodeName();
      this.title = getTranslation(this.originalTitle);

      assert var2.getAttributes().getNamedItem("width") != null : var2.getNodeName() + ": " + var2.getAttributes();

      this.width = (float)Integer.parseInt(var2.getAttributes().getNamedItem("width").getNodeValue());
      this.height = (float)Integer.parseInt(var2.getAttributes().getNamedItem("height").getNodeValue());
      this.valueSpacing = Integer.parseInt(var2.getAttributes().getNamedItem("valueSpacing").getNodeValue());
      this.rows = Integer.parseInt(var2.getAttributes().getNamedItem("rows").getNodeValue());
      this.columnDist = Integer.parseInt(var2.getAttributes().getNamedItem("columnDist").getNodeValue());
      NodeList var6 = var2.getChildNodes();

      for(int var3 = 0; var3 < var6.getLength(); ++var3) {
         Node var4;
         if ((var4 = var6.item(var3)).getNodeType() == 1) {
            String var5 = StringTools.cpt(getTranslation(var4.getNodeName()));
            this.keysS.add(var5);
            this.valuesS.add(var4.getTextContent());
         }
      }

      this.update(var1);
   }

   public void update(InputState var1) {
      this.detachAll();
      this.keys.clear();
      this.values.clear();

      assert this.keysS.size() == this.valuesS.size();

      GUITextOverlay var4;
      GUITextOverlay var5;
      for(int var2 = 0; var2 < this.keysS.size(); ++var2) {
         String var3 = KeyboardMappings.formatText((String)this.valuesS.get(var2));
         var4 = new GUITextOverlay(100, 15, FontLibrary.getBlenderProMedium14(), var1);
         var5 = new GUITextOverlay(100, 15, FontLibrary.getBlenderProMedium14(), var1);
         var4.setTextSimple((String)this.keysS.get(var2) + ":");
         var5.setTextSimple(var3);
         this.keys.add(var4);
         this.values.add(var5);
      }

      GUIColoredRectangle var8;
      (var8 = new GUIColoredRectangle(var1, this.width, this.height, new Vector4f(0.25F, 0.16F, 0.8F, 0.75F))).rounded = 4.0F;
      this.attach(var8);

      for(int var10 = 0; var10 < this.keys.size(); ++var10) {
         var5 = (GUITextOverlay)this.keys.get(var10);
         GUITextOverlay var9 = (GUITextOverlay)this.values.get(var10);
         int var6 = var10 / this.rows;
         int var7 = var10 % this.rows;
         var6 *= this.columnDist;
         var7 *= 15;
         var5.setPos((float)var6, (float)var7, 0.0F);
         var9.setPos((float)(var6 + this.valueSpacing), (float)var7, 0.0F);
         var8.attach(var5);
         var8.attach(var9);
      }

      (var4 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium14(), var1)).setTextSimple(this.getTitle().equals("General") ? this.getTitle() + " (hide by pressing '" + KeyboardMappings.HELP_SCREEN.getKeyChar() + "' or in game settings)" : this.getTitle());
      var4.getPos().y = -15.0F;
      var8.attach(var4);
   }

   public String getTitle() {
      return this.title;
   }

   public String getOriginalTitle() {
      return this.originalTitle;
   }
}
