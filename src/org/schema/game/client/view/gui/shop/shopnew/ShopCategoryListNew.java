package org.schema.game.client.view.gui.shop.shopnew;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import org.schema.game.client.controller.manager.ingame.shop.ShopControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.ElementCategory;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIEnterableList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class ShopCategoryListNew extends GUIAncor implements Observer, OnInputChangedCallback {
   private final GUIElementList categoryList;
   private final GUIElementList filteredFlatList;
   private boolean init;
   private GUIScrollablePanel motherPanel;
   private String filterText = "";
   private ShopCategoryListNew.ShopListElementCallback shopElementCallback;

   public ShopCategoryListNew(InputState var1, GUIScrollablePanel var2) {
      super(var1);
      this.categoryList = new GUIElementList(var1);
      this.categoryList.setScrollPane(var2);
      this.shopElementCallback = new ShopCategoryListNew.ShopListElementCallback();
      this.filteredFlatList = new GUIElementList(var1);
      this.filteredFlatList.setScrollPane(var2);
      this.filteredFlatList.setCallback(this.shopElementCallback);
      this.motherPanel = var2;
   }

   private void addRecursively(ElementCategory var1, GUIElementList var2, int var3) {
      var2.setCallback(this.shopElementCallback);
      GUITextOverlay var4;
      (var4 = new GUITextOverlay(500, 24, FontLibrary.getBlenderProMedium16(), this.getState())).setText(new ArrayList());
      var4.getText().add("+ " + var1.getCategory());
      GUITextOverlay var5;
      (var5 = new GUITextOverlay(500, 24, FontLibrary.getBlenderProMedium16(), this.getState())).setText(new ArrayList());
      var5.getText().add("- " + var1.getCategory());
      GUIAncor var6 = new GUIAncor(this.getState(), 500.0F, 24.0F);
      var5.setPos(4.0F, 4.0F, 0.0F);
      var6.attach(var5);
      GUIAncor var10 = new GUIAncor(this.getState(), 500.0F, 24.0F);
      var4.setPos(4.0F, 4.0F, 0.0F);
      var10.attach(var4);
      GUIEnterableList var9;
      (var9 = new GUIEnterableList(this.getState(), var10, var6)).scrollPanel = this.motherPanel;
      var9.getPos().x = (float)(var3 * 5);
      boolean var11 = true;
      int var12;
      if (var1.hasChildren()) {
         for(var12 = 0; var12 < var1.getChildren().size(); ++var12) {
            this.addRecursively((ElementCategory)var1.getChildren().get(var12), var9.getList(), var3 + 1);
            var11 = false;
         }
      }

      var9.getList().setCallback(this.shopElementCallback);
      var12 = 0;

      for(var3 = 0; var3 < var1.getInfoElements().size(); ++var3) {
         ElementInformation var7;
         if ((var7 = (ElementInformation)var1.getInfoElements().get(var3)).isShoppable()) {
            this.addToList(var9.getList(), var7, var12);
            ++var12;
            var11 = false;
         }
      }

      if (!var11) {
         var9.addObserver(this);
         var9.setUserPointer("CATEGORY");
         var9.onInit();
         var9.setMouseUpdateEnabled(true);
         GUIListElement var8 = new GUIListElement(var9, var9, this.getState());
         var9.setParent(this);
         var2.add(var8);
      }

   }

   private void addToList(GUIElementList var1, ElementInformation var2, int var3) {
      var1.add(var2.getShopItemElement(this.getState()).getListElement());
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      super.draw();
   }

   public void onInit() {
      super.onInit();
      ElementCategory var1;
      int var2;
      if ((var1 = ElementKeyMap.getCategoryHirarchy()).hasChildren()) {
         for(var2 = 0; var2 < var1.getChildren().size(); ++var2) {
            this.addRecursively((ElementCategory)var1.getChildren().get(var2), this.categoryList, 0);
         }
      }

      var2 = 0;

      for(int var3 = 0; var3 < var1.getInfoElements().size(); ++var3) {
         ElementInformation var4;
         if ((var4 = (ElementInformation)var1.getInfoElements().get(var3)).isShoppable()) {
            this.addToList(this.categoryList, var4, var2);
            ++var2;
         }
      }

      this.attach(this.categoryList);
      this.categoryList.onInit();
      this.categoryList.setMouseUpdateEnabled(true);
      this.setMouseUpdateEnabled(true);
      this.init = true;
   }

   public float getHeight() {
      return this.filterText.length() > 0 ? this.filteredFlatList.getHeight() : this.categoryList.getHeight();
   }

   public float getWidth() {
      return this.filterText.length() > 0 ? this.filteredFlatList.getWidth() : this.categoryList.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public ShopControllerManager getShopControlManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getShopControlManager();
   }

   public boolean isInside() {
      if (this.motherPanel != null) {
         return this.motherPanel.isInside() && super.isInside();
      } else {
         return super.isInside();
      }
   }

   private void updateSeatchList(String var1) {
      this.filteredFlatList.clear();
      if (var1.length() > 0) {
         Iterator var2 = ElementKeyMap.keySet.iterator();

         while(var2.hasNext()) {
            ElementInformation var3;
            if ((var3 = ElementKeyMap.getInfo((Short)var2.next())).isShoppable() && var3.getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH))) {
               this.filteredFlatList.addWithoutUpdate(var3.getShopItemElement(this.getState()).getListElement());
            }
         }

         this.filteredFlatList.updateDim();
         this.detachAll();
         this.attach(this.filteredFlatList);
      } else {
         this.filteredFlatList.updateDim();
         this.detachAll();
         this.attach(this.categoryList);
      }
   }

   public void update(Observable var1, Object var2) {
      this.categoryList.updateDim();
   }

   public String onInputChanged(String var1) {
      this.filterText = var1;
      this.updateSeatchList(var1);
      return var1;
   }

   class ShopListElementCallback implements GUICallback {
      private ShopListElementCallback() {
      }

      public void callback(GUIElement var1, MouseEvent var2) {
         if (var2.pressedLeftMouse() && var1 instanceof GUIListElement) {
            GUIListElement var3 = (GUIListElement)var1;
            if (!"CATEGORY".equals(var3.getContent().getUserPointer())) {
               if (ShopCategoryListNew.this.getShopControlManager().getCurrentlySelectedListElement() != null) {
                  ShopCategoryListNew.this.getShopControlManager().getCurrentlySelectedListElement().getList().deselectAll();
                  ShopCategoryListNew.this.getShopControlManager().getCurrentlySelectedListElement().setSelected(false);
               }

               var3.setSelected(true);
               ShopCategoryListNew.this.getShopControlManager().setSelectedElementClass((Short)var3.getContent().getUserPointer());
               ShopCategoryListNew.this.getShopControlManager().setCurrentlySelectedListElement(var3);
            }
         }

      }

      public boolean isOccluded() {
         return ShopCategoryListNew.this.motherPanel != null && (!ShopCategoryListNew.this.motherPanel.isActive() || ShopCategoryListNew.this.motherPanel.isInsideScrollBar());
      }

      // $FF: synthetic method
      ShopListElementCallback(Object var2) {
         this();
      }
   }
}
