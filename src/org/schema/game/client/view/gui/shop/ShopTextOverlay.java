package org.schema.game.client.view.gui.shop;

import org.newdawn.slick.Color;
import org.newdawn.slick.UnicodeFont;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.network.client.ClientState;

public class ShopTextOverlay extends GUITextOverlay {
   public ShopTextOverlay(int var1, int var2, ClientState var3) {
      super(var1, var2, var3);
   }

   public ShopTextOverlay(int var1, int var2, UnicodeFont var3, ClientState var4) {
      super(var1, var2, (UnicodeFont)var3, var4);
   }

   public ShopTextOverlay(int var1, int var2, UnicodeFont var3, Color var4, ClientState var5) {
      super(var1, var2, (UnicodeFont)var3, var4, var5);
   }
}
