package org.schema.game.client.view.gui.shop.shopnew;

import java.util.ArrayList;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.network.objects.TradePriceInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredUnderlayRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class ShopItemElement extends GUIAncor {
   private final short buildIcon;
   private final ElementInformation info;
   private GUIListElement guiListElement;

   public ShopItemElement(InputState var1, ElementInformation var2) {
      super(var1, 500.0F, 44.0F);
      this.buildIcon = (short)var2.getBuildIconNum();
      this.setUserPointer(var2.getId());
      this.info = var2;
   }

   public GUIListElement getListElement() {
      if (this.guiListElement == null) {
         GUIColoredUnderlayRectangle var1 = new GUIColoredUnderlayRectangle(this.getState(), this.getWidth(), this.getHeight(), new Vector4f(1.0F, 1.0F, 1.0F, 0.2F), this);
         this.guiListElement = new GUIListElement(this, var1, this.getState());
      }

      return this.guiListElement;
   }

   public void onInit() {
      super.onInit();
      int var1 = this.buildIcon / 256;
      ShopItemElement.InventoryStatePriceCallback var2 = new ShopItemElement.InventoryStatePriceCallback(this.info);
      Sprite var6 = Controller.getResLoader().getSprite("build-icons-" + StringTools.formatTwoZero(var1) + "-16x16-gui-");
      GUIOverlay var7 = new GUIOverlay(var6, this.getState());
      GUITextOverlay var3;
      (var3 = new GUITextOverlay(500, 20, FontLibrary.getBlenderProMedium15(), this.getState()) {
         public void draw() {
            this.setColor(0.5F, 0.8F, 1.0F, 1.0F);
            super.draw();
         }
      }).setText(new ArrayList());
      var3.getText().add(this.info.getName());
      var3.getPos().x = 32.0F;
      var3.getPos().y = 3.0F;
      GUITextOverlay var4;
      (var4 = new GUITextOverlay(224, 64, FontLibrary.getBlenderProMedium13(), this.getState())).getPos().x = 4.0F;
      var4.getPos().y = 28.0F;
      int var5 = this.buildIcon % 256;
      var7.setSpriteSubIndex(var5);
      var7.setScale(0.5F, 0.5F, 0.5F);
      var4.setTextSimple(var2);
      this.attach(var3);
      this.attach(var7);
      this.attach(var4);
      this.setMouseUpdateEnabled(true);
   }

   class InventoryStatePriceCallback {
      private ElementInformation info;
      private ShopItemElement.InventoryStateStockCallback inventoryStateStockCallbackWePurchase;
      private ShopItemElement.InventoryStateStockCallback inventoryStateStockCallbackWeSell;

      private String getPriceString(ShopInterface var1, boolean var2) {
         int var4;
         if ((var4 = var1.getPriceString(this.info, var2)) < 0) {
            return var2 ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPITEMELEMENT_4 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPITEMELEMENT_6;
         } else {
            String var3;
            int var5;
            if (var2) {
               if ((var5 = this.inventoryStateStockCallbackWePurchase.amount()) <= 0) {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPITEMELEMENT_0;
               } else {
                  var3 = "(x" + StringTools.massFormat(var5) + ")";
                  return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPITEMELEMENT_5, var4, var3);
               }
            } else if ((var5 = this.inventoryStateStockCallbackWeSell.amount()) <= 0) {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPITEMELEMENT_3;
            } else {
               var3 = "(x" + StringTools.massFormat(var5) + ")";
               if (var5 == Integer.MAX_VALUE) {
                  var3 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPITEMELEMENT_7;
               }

               return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPITEMELEMENT_8, var4, var3);
            }
         }
      }

      public InventoryStatePriceCallback(ElementInformation var2) {
         this.info = var2;
         this.inventoryStateStockCallbackWePurchase = ShopItemElement.this.new InventoryStateStockCallback(var2, true);
         this.inventoryStateStockCallbackWeSell = ShopItemElement.this.new InventoryStateStockCallback(var2, false);
      }

      public String toString() {
         ShopInterface var1;
         return (var1 = ((GameClientState)ShopItemElement.this.getState()).getCurrentClosestShop()) == null ? "err" : String.format("%-20s %s", this.getPriceString(var1, true), this.getPriceString(var1, false));
      }
   }

   class InventoryStateStockCallback {
      private ElementInformation info;
      private final boolean weWantToPurchase;

      public InventoryStateStockCallback(ElementInformation var2, boolean var3) {
         this.info = var2;
         this.weWantToPurchase = var3;
      }

      public int amount() {
         ShopInterface var1;
         if ((var1 = ((GameClientState)ShopItemElement.this.getState()).getCurrentClosestShop()) == null) {
            return -1;
         } else {
            var1.getShopInventory().getOverallQuantity(this.info.getId());
            int var2 = -1;
            TradePriceInterface var3;
            if ((var3 = var1.getPrice(this.info.getId(), !this.weWantToPurchase)) != null) {
               if (this.weWantToPurchase) {
                  var2 = var3.getLimit() < 0 ? var3.getAmount() : var3.getAmount() - var3.getLimit();
               } else {
                  var2 = var3.getLimit() < 0 ? Integer.MAX_VALUE : var3.getLimit() - var3.getAmount();
               }
            }

            return var2;
         }
      }

      public String toString() {
         int var1;
         return (var1 = this.amount()) <= 0 ? "err" : this.getBStr(var1);
      }

      private String getBStr(int var1) {
         if (var1 == Integer.MIN_VALUE) {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPITEMELEMENT_1;
         } else {
            return var1 <= 0 ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPITEMELEMENT_2 : StringTools.massFormat(var1);
         }
      }
   }
}
