package org.schema.game.client.view.gui.shop.shopnew;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.List;
import java.util.Locale;
import javax.vecmath.Vector3f;
import org.newdawn.slick.Color;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerBlockTypeDropdownInputNew;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.manager.ingame.shop.BuyQuantityDialog;
import org.schema.game.client.controller.manager.ingame.shop.ShopControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIBlockSprite;
import org.schema.game.client.view.gui.inventory.InventorySlotOverlayElement;
import org.schema.game.client.view.gui.inventory.inventorynew.InventoryOptionsChestButtonPanel;
import org.schema.game.client.view.gui.inventory.inventorynew.InventoryPanelNew;
import org.schema.game.client.view.gui.trade.GUIPricesScrollableList;
import org.schema.game.client.view.gui.trade.GUITradeActiveScrollableList;
import org.schema.game.client.view.gui.trade.GUITradeNodeScrollableList;
import org.schema.game.client.view.gui.trade.OrderDialog;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.controller.ShoppingAddOn;
import org.schema.game.common.controller.trade.TradeManager;
import org.schema.game.common.controller.trade.TradeNodeClient;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.inventory.ShopInventory;
import org.schema.game.network.objects.TradePrice;
import org.schema.game.network.objects.TradePriceInterface;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.Draggable;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.DropTarget;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalProgressBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIInnerTextbox;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUISearchBar;
import org.schema.schine.input.InputState;

public class ShopPanelNew extends GUIElement implements DropTarget, GUIActiveInterface {
   public GUIMainWindow shopPanel;
   private boolean isOwnShopBefore;
   private boolean isTradeShopBefore;
   private boolean init;
   private boolean flagShopTabRecreate;
   private GUIContentPane shopBuySellTab;
   private GUIContentPane shopOptionsTab;
   private ShopScrollableCategoryList shopList;
   private GUIContentPane shopRepairTab;
   private GUIContentPane tradeTab;
   private GUIContentPane pricesTab;
   private ShopInterface currentShop = this.getState().getCurrentClosestShop();
   private boolean lastWasTrade;
   private boolean inventoryActive;

   public ShopPanelNew(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
   }

   public boolean isTradeNode() {
      return this.currentShop != null && this.currentShop.isTradeNode() && (TradeNodeClient)this.getState().getController().getClientChannel().getGalaxyManagerClient().getTradeNodeDataById().get(this.currentShop.getSegmentController().dbId) != null;
   }

   public void draw() {
      if (!this.init) {
         this.currentShop = this.getState().getCurrentClosestShop();
         this.lastWasTrade = this.isTradeNode();
         this.isOwnShopBefore = this.isOwnShop();
         this.isTradeShopBefore = this.isShopTrader();
         if (this.currentShop == null) {
            return;
         }

         this.onInit();
      }

      if (this.currentShop != this.getState().getCurrentClosestShop() || this.isTradeNode() != this.lastWasTrade || this.isOwnShopBefore != this.isOwnShop() || this.isTradeShopBefore != this.isShopTrader()) {
         this.flagShopTabRecreate = true;
         this.currentShop = this.getState().getCurrentClosestShop();
         this.lastWasTrade = this.isTradeNode();
         this.isOwnShopBefore = this.isOwnShop();
         this.isTradeShopBefore = this.isShopTrader();
         List var1;
         if ((!this.lastWasTrade || !this.isTradeShopBefore) && !(var1 = this.getState().getPlayerInputs()).isEmpty() && var1.get(var1.size() - 1) instanceof OrderDialog) {
            this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_99, 0.0F);
            ((DialogInterface)var1.get(var1.size() - 1)).deactivate();
         }
      }

      if (this.currentShop != null) {
         if (this.flagShopTabRecreate) {
            this.recreateTabs();
            this.flagShopTabRecreate = false;
         }

         if (this.isOwnShop()) {
            this.shopBuySellTab.setTextBoxHeight(1, 1, 77);
         } else {
            this.shopBuySellTab.setTextBoxHeight(1, 1, 52);
         }

         this.shopPanel.draw();
      }
   }

   public void onInit() {
      if (this.shopPanel != null) {
         this.shopPanel.cleanUp();
      }

      this.shopPanel = new GUIMainWindow(this.getState(), 750, 550, "ShopPanelNew");
      this.shopPanel.onInit();
      this.shopPanel.setMouseUpdateEnabled(true);
      this.shopPanel.setCallback(new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (ShopPanelNew.this.shopPanel.isInside()) {
               ShopPanelNew.this.checkTarget(var2);
            }

         }
      });
      this.shopPanel.setCloseCallback(new GUICallback() {
         public boolean isOccluded() {
            return !ShopPanelNew.this.getState().getController().getPlayerInputs().isEmpty();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ShopPanelNew.this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().deactivateAll();
            }

         }
      });
      this.shopPanel.orientate(48);
      this.recreateTabs();
      this.init = true;
   }

   public void recreateTabs() {
      System.err.println("[CLIENT] Shop panel recreate for shop " + this.currentShop);
      Object var1 = null;
      if (this.shopPanel.getSelectedTab() < this.shopPanel.getTabs().size()) {
         var1 = ((GUIContentPane)this.shopPanel.getTabs().get(this.shopPanel.getSelectedTab())).getTabName();
      }

      this.shopPanel.clearTabs();
      this.shopBuySellTab = this.shopPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_2);
      this.shopRepairTab = this.shopPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_3);
      this.createShopPane();
      if (this.isShopTrader()) {
         this.shopOptionsTab = this.shopPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_4);
         this.createOptionPane();
         this.tradeTab = this.shopPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_72);
         this.createTradePane();
         this.pricesTab = this.shopPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_75);
         this.createPricesPane();
      }

      this.createRepairPane();
      this.shopPanel.activeInterface = this;
      if (var1 != null) {
         for(int var2 = 0; var2 < this.shopPanel.getTabs().size(); ++var2) {
            if (((GUIContentPane)this.shopPanel.getTabs().get(var2)).getTabName().equals(var1)) {
               this.shopPanel.setSelectedTab(var2);
               return;
            }
         }
      }

   }

   private void createTradeButtons(GUIAncor var1) {
      GUIHorizontalButtonTablePane var2;
      (var2 = new GUIHorizontalButtonTablePane(this.getState(), 1, 1, var1)).onInit();
      var2.addButton(0, 0, new Object() {
         public String toString() {
            return !ShopPanelNew.this.getState().getCurrentClosestShop().getShoppingAddOn().isTradeNode() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_77 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_100;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ShopInterface var3;
               (var3 = ShopPanelNew.this.getState().getCurrentClosestShop()).getShoppingAddOn().requestTradeNodeStateOnClient(!var3.getShoppingAddOn().isTradeNode());
            }

         }

         public boolean isOccluded() {
            return !ShopPanelNew.this.isActive();
         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return ShopPanelNew.this.isShopTrader();
         }

         public boolean isHighlighted(InputState var1) {
            return ShopPanelNew.this.getState().getCurrentClosestShop().getShoppingAddOn().isTradeNode();
         }
      });
      var1.attach(var2);
   }

   private void createTradePane() {
      this.tradeTab.setTextBoxHeightLast(26);
      this.createTradeButtons(this.tradeTab.getContent(0));
      this.tradeTab.addNewTextBox(150);
      if (this.isTradeNode()) {
         GUITradeActiveScrollableList var2;
         (var2 = new GUITradeActiveScrollableList(this.getState(), this.currentShop, this.tradeTab.getContent(1))).onInit();
         this.tradeTab.getContent(1).attach(var2);
         this.tradeTab.addNewTextBox(97);
         GUITradeNodeScrollableList var3;
         (var3 = new GUITradeNodeScrollableList(this.getState(), this.currentShop, this.tradeTab.getContent(2))).onInit();
         this.tradeTab.getContent(2).attach(var3);
         this.tradeTab.setEqualazingHorizontalDividers(1);
      } else {
         GUITextOverlay var1;
         (var1 = new GUITextOverlay(0, 0, this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_79);
         var1.setPos(6.0F, 10.0F, 0.0F);
         this.tradeTab.getContent(1).attach(var1);
      }
   }

   public void switchAll() {
      TradeNodeClient var1 = (TradeNodeClient)this.getState().getController().getClientChannel().getGalaxyManagerClient().getTradeNodeDataById().get(this.currentShop.getSegmentController().getDbId());
      if (this.isTradeNode() && this.isShopTrader()) {
         ShopInterface var2 = this.getState().getCurrentClosestShop();

         assert var2 != null && var2.getSegmentController().dbId == var1.getEntityDBId();

         if (var2 != null && var2.getSegmentController().dbId == var1.getEntityDBId()) {
            short[] var9;
            int var3 = (var9 = ElementKeyMap.typeList()).length;

            for(int var4 = 0; var4 < var3; ++var4) {
               short var5;
               ElementInformation var6 = ElementKeyMap.getInfo(var5 = var9[var4]);
               TradePriceInterface var7 = var2.getPrice(var6.getId(), true);
               TradePriceInterface var10 = var2.getPrice(var6.getId(), false);
               TradePrice var10000;
               TradePrice var8;
               if (var7 != null) {
                  var10000 = var8 = ShoppingAddOn.getPriceInstanceIfExisting(var2, var5, true);
                  var10000.setSell(!var10000.isSell());
                  var2.getShoppingAddOn().clientRequestSetPrice(this.getState().getPlayer(), var8);
                  if (var10 == null) {
                     (var8 = ShoppingAddOn.getPriceInstanceIfExisting(var2, var5, true)).setPrice(-1);
                     var2.getShoppingAddOn().clientRequestSetPrice(this.getState().getPlayer(), var8);
                  }
               }

               if (var10 != null) {
                  var10000 = var8 = ShoppingAddOn.getPriceInstanceIfExisting(var2, var5, false);
                  var10000.setSell(!var10000.isSell());
                  var2.getShoppingAddOn().clientRequestSetPrice(this.getState().getPlayer(), var8);
                  if (var7 == null) {
                     (var8 = ShoppingAddOn.getPriceInstanceIfExisting(var2, var5, false)).setPrice(-1);
                     var2.getShoppingAddOn().clientRequestSetPrice(this.getState().getPlayer(), var8);
                  }
               }
            }
         }
      }

   }

   private void createPricesPane() {
      TradeNodeClient var1 = (TradeNodeClient)this.getState().getController().getClientChannel().getGalaxyManagerClient().getTradeNodeDataById().get(this.currentShop.getSegmentController().getDbId());
      if (this.isTradeNode() && this.isShopTrader()) {
         int var5 = 0;
         if (this.getState().isAdmin()) {
            this.pricesTab.setTextBoxHeightLast(24);
            GUIHorizontalButtonTablePane var3;
            (var3 = new GUIHorizontalButtonTablePane(this.getState(), 1, 1, this.pricesTab.getContent(0))).onInit();
            var3.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_102, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse()) {
                     ShopPanelNew.this.switchAll();
                  }

               }

               public boolean isOccluded() {
                  return !ShopPanelNew.this.isActive();
               }
            }, new GUIActivationCallback() {
               public boolean isVisible(InputState var1) {
                  return true;
               }

               public boolean isActive(InputState var1) {
                  return true;
               }
            });
            this.pricesTab.getContent(0).attach(var3);
            ++var5;
            this.pricesTab.addNewTextBox(97);
         } else {
            this.pricesTab.setTextBoxHeightLast(97);
         }

         GUIPricesScrollableList var6;
         (var6 = new GUIPricesScrollableList(this.getState(), this.pricesTab.getContent(var5), this.currentShop, var1, true)).onInit();
         this.pricesTab.getContent(var5).attach(var6);
         this.pricesTab.addNewTextBox(10);
         ++var5;
         GUIPricesScrollableList var4;
         (var4 = new GUIPricesScrollableList(this.getState(), this.pricesTab.getContent(var5), this.currentShop, var1, false)).onInit();
         this.pricesTab.getContent(var5).attach(var4);
         this.pricesTab.setEqualazingHorizontalDividers(this.getState().isAdmin() ? 1 : 0);
      } else {
         this.pricesTab.setTextBoxHeightLast(97);
         GUITextOverlay var2;
         (var2 = new GUITextOverlay(0, 0, this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_78);
         var2.setPos(6.0F, 10.0F, 0.0F);
         this.pricesTab.getContent(0).attach(var2);
      }
   }

   private void createShopPane() {
      this.shopBuySellTab.setTextBoxHeightLast(28);
      this.shopBuySellTab.addNewTextBox(28);
      this.shopBuySellTab.addDivider(280);
      this.shopList = new ShopScrollableCategoryList(this.getState(), this.shopBuySellTab.getContent(0, 0), this);
      this.shopList.onInit();
      this.shopBuySellTab.getContent(0, 0).attach(this.shopList);
      this.shopBuySellTab.setListDetailMode(0, (GUIInnerTextbox)this.shopBuySellTab.getTextboxes(0).get(0));
      GUISearchBar var1 = new GUISearchBar(this.getState(), this.shopBuySellTab.getContent(0, 1), new TextCallback() {
         public String[] getCommandPrefixes() {
            return null;
         }

         public void onTextEnter(String var1, boolean var2, boolean var3) {
         }

         public void onFailedTextCheck(String var1) {
         }

         public void newLine() {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }
      }, this.shopList);
      this.shopBuySellTab.getContent(0, 1).attach(var1);
      this.shopBuySellTab.setTextBoxHeightLast(1, 97);
      GUIAncor var6 = new GUIAncor(this.getState(), 200.0F, 72.0F);
      GUITextOverlay var2 = new GUITextOverlay(1, 1, FontLibrary.getBlenderProMedium18(), this.getState());
      GUITextOverlay var3 = new GUITextOverlay(1, 1, FontLibrary.getBlenderProMedium15(), this.getState());
      GUITextOverlay var4 = new GUITextOverlay(1, 1, FontLibrary.getBlenderProMedium15(), this.getState()) {
         public void draw() {
            ShopInterface var1 = ((GameClientState)this.getState()).getCurrentClosestShop();

            assert var1 == null || var1.getShopInventory() != null : var1;

            if (var1 != null && var1.getShopInventory().isOverCapacity()) {
               this.setColor(Color.red);
            } else {
               this.setColor(Color.white);
            }

            super.draw();
         }
      };
      GUITextOverlay var5 = new GUITextOverlay(1, 1, FontLibrary.getBlenderProMedium15(), this.getState());
      var2.setTextSimple(new Object() {
         public String toString() {
            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_12, StringTools.formatSeperated(ShopPanelNew.this.getState().getPlayer().getCredits()));
         }
      });
      var3.setTextSimple(new Object() {
         public String toString() {
            ShopInterface var1;
            return (var1 = ShopPanelNew.this.getState().getCurrentClosestShop()) != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_13, StringTools.formatSeperated(var1.getCredits())) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_14;
         }
      });
      var4.setTextSimple(new Object() {
         public String toString() {
            ShopInterface var1;
            return (var1 = ShopPanelNew.this.getState().getCurrentClosestShop()) != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_73, StringTools.formatPointZero(var1.getShopInventory().getVolume()), StringTools.formatPointZero(var1.getShopInventory().getCapacity())) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_71;
         }
      });
      var5.setTextSimple(new Object() {
         public String toString() {
            ShopInterface var1;
            if ((var1 = ShopPanelNew.this.getState().getCurrentClosestShop()) != null) {
               if (var1.isAiShop()) {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_8;
               } else {
                  return var1.getShopOwners().isEmpty() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_9 : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_17, var1.getShopOwners().size() > 1 ? "s" : "", var1.getShopOwners().toString());
               }
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_18;
            }
         }
      });
      var2.setColor(0.9F, 0.9F, 0.7F, 1.0F);
      var2.getPos().x = 4.0F;
      var2.getPos().y = 4.0F;
      var6.attach(var2);
      var3.getPos().x = 4.0F;
      var3.getPos().y = 30.0F;
      var6.attach(var3);
      var5.getPos().x = 4.0F;
      var5.getPos().y = 51.0F;
      var6.attach(var5);
      var4.getPos().x = 4.0F;
      var4.getPos().y = 72.0F;
      var6.attach(var4);
      GUIScrollablePanel var7;
      (var7 = new GUIScrollablePanel(10.0F, 10.0F, this.shopBuySellTab.getContent(1, 0), this.getState())).setContent(var6);
      this.shopBuySellTab.getContent(1, 0).attach(var7);
      this.shopBuySellTab.addNewTextBox(1, 52);
      this.createBuySellButtonPanel(this.shopBuySellTab.getContent(1, 1));
      this.shopBuySellTab.addNewTextBox(1, 100);
      this.createBlockDescriptionPanel(this.shopBuySellTab.getContent(1, 2));
   }

   public void createRepairPane() {
      this.shopRepairTab.setTextBoxHeightLast(76);
      this.createShopRepairsButtonPanel(this.shopRepairTab.getContent(0));
   }

   public void createOptionPane() {
      this.shopOptionsTab.setTextBoxHeightLast(97);
      GUIAncor var1 = new GUIAncor(this.getState(), 200.0F, 72.0F);
      GUITextOverlay var2 = new GUITextOverlay(1, 1, FontLibrary.getBlenderProMedium18(), this.getState());
      GUITextOverlay var3 = new GUITextOverlay(1, 1, FontLibrary.getBlenderProMedium15(), this.getState());
      GUITextOverlay var4 = new GUITextOverlay(1, 1, FontLibrary.getBlenderProMedium15(), this.getState());
      GUITextOverlay var5 = new GUITextOverlay(1, 1, FontLibrary.getBlenderProMedium15(), this.getState()) {
         public void draw() {
            ShopInterface var1;
            if ((var1 = ((GameClientState)this.getState()).getCurrentClosestShop()) != null && var1.getShopInventory().isOverCapacity()) {
               this.setColor(Color.red);
            } else {
               this.setColor(Color.white);
            }

            super.draw();
         }
      };
      var2.setTextSimple(new Object() {
         public String toString() {
            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_5, StringTools.formatSeperated(ShopPanelNew.this.getState().getPlayer().getCredits()));
         }
      });
      var3.setTextSimple(new Object() {
         public String toString() {
            ShopInterface var1;
            return (var1 = ShopPanelNew.this.getState().getCurrentClosestShop()) != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_6, StringTools.formatSeperated(var1.getCredits())) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_7;
         }
      });
      var5.setTextSimple(new Object() {
         public String toString() {
            ShopInterface var1;
            return (var1 = ShopPanelNew.this.getState().getCurrentClosestShop()) != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_0, StringTools.formatPointZero(var1.getShopInventory().getVolume()), StringTools.formatPointZero(var1.getShopInventory().getCapacity())) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_74;
         }
      });
      var4.setTextSimple(new Object() {
         public String toString() {
            ShopInterface var1;
            if ((var1 = ShopPanelNew.this.getState().getCurrentClosestShop()) != null) {
               if (var1.isAiShop()) {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_15;
               } else {
                  return var1.getShopOwners().isEmpty() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_16 : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_10, var1.getShopOwners().size() > 1 ? "s" : "", var1.getShopOwners().toString());
               }
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_11;
            }
         }
      });
      var2.setColor(0.9F, 0.9F, 0.7F, 1.0F);
      var2.getPos().x = 4.0F;
      var2.getPos().y = 4.0F;
      var1.attach(var2);
      var3.getPos().x = 4.0F;
      var3.getPos().y = 30.0F;
      var1.attach(var3);
      var4.getPos().x = 4.0F;
      var4.getPos().y = 51.0F;
      var1.attach(var4);
      var5.getPos().x = 4.0F;
      var5.getPos().y = 72.0F;
      var1.attach(var5);
      GUIScrollablePanel var6;
      (var6 = new GUIScrollablePanel(10.0F, 10.0F, this.shopOptionsTab.getContent(0), this.getState())).setContent(var1);
      this.shopOptionsTab.getContent(0).attach(var6);
      this.shopOptionsTab.addNewTextBox(99);
      this.createShopOptionsButtonPanel(this.shopOptionsTab.getContent(1));
      this.shopOptionsTab.addNewTextBox(100);
      var1 = new GUIAncor(this.getState());
      GUIDropDownList var7;
      (var7 = this.getShopPermissionDropDown()).setPos(2.0F, 5.0F, 0.0F);
      GUIDropDownList var8;
      (var8 = this.getShopPermissionTradeDropDown()).setPos(2.0F, 33.0F, 0.0F);
      (var4 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium15(), this.getState())).setTextSimple(new Object() {
         public String toString() {
            ShopInterface var1;
            if ((var1 = ShopPanelNew.this.getState().getCurrentClosestShop()).getPermissionToPurchase() == TradeManager.PERM_ALL) {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_19;
            } else if (var1.getPermissionToPurchase() == TradeManager.PERM_ALL_BUT_ENEMY) {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_20;
            } else if (var1.getPermissionToPurchase() == TradeManager.PERM_ALLIES_AND_FACTION) {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_21;
            } else {
               return var1.getPermissionToPurchase() == TradeManager.PERM_FACTION ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_22 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_76;
            }
         }
      });
      var4.setPos(2.0F, 61.0F, 0.0F);
      (var5 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium15(), this.getState())).setTextSimple(new Object() {
         public String toString() {
            long var1;
            if ((var1 = ShopPanelNew.this.getState().getCurrentClosestShop().getPermissionToTrade()) == TradeManager.NONE) {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_23;
            } else if (var1 == TradeManager.PERM_FACTION) {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_24;
            } else if (var1 == TradeManager.PERM_ALLIES_AND_FACTION) {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_25;
            } else {
               return var1 == TradeManager.PERM_ALL ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_26 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_80;
            }
         }
      });
      var5.setPos(2.0F, 86.0F, 0.0F);
      var1.attach(var4);
      var1.attach(var5);
      var1.attach(var7);
      var1.attach(var8);
      (var6 = new GUIScrollablePanel(10.0F, 10.0F, this.shopOptionsTab.getContent(2), this.getState())).setContent(var1);
      this.shopOptionsTab.getContent(2).attach(var6);
   }

   private GUIDropDownList getShopPermissionTradeDropDown() {
      GUITextOverlay[] var1;
      (var1 = new GUITextOverlay[4])[0] = new GUITextOverlay(400, 20, this.getState());
      var1[0].setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_81);
      var1[0].setUserPointer(TradeManager.NONE);
      var1[1] = new GUITextOverlay(400, 20, this.getState());
      var1[1].setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_82);
      var1[1].setUserPointer(TradeManager.PERM_FACTION);
      var1[2] = new GUITextOverlay(400, 20, this.getState());
      var1[2].setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_83);
      var1[2].setUserPointer(TradeManager.PERM_ALLIES_AND_FACTION);
      var1[3] = new GUITextOverlay(400, 20, this.getState());
      var1[3].setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_84);
      var1[3].setUserPointer(TradeManager.PERM_ALL);
      GUIAncor[] var2 = new GUIAncor[var1.length];

      for(int var3 = 0; var3 < var2.length; ++var3) {
         var2[var3] = new GUIAncor(this.getState(), 100.0F, 20.0F);
         var2[var3].setUserPointer(var1[var3].getUserPointer());
         var2[var3].attach(var1[var3]);
         var1[var3].setPos(3.0F, 3.0F, 0.0F);
      }

      GUIDropDownList var6 = new GUIDropDownList(this.getState(), 400, 20, var1.length * 20 + 5, new DropDownCallback() {
         private boolean first = true;

         public void onSelectionChanged(GUIListElement var1) {
            long var2 = (Long)var1.getContent().getUserPointer();
            ShopInterface var4 = ShopPanelNew.this.getState().getCurrentClosestShop();
            if (ShopPanelNew.this.isOwnShop()) {
               if (this.first) {
                  this.first = false;
                  return;
               }

               var4.getShoppingAddOn().clientRequestTradePermission(ShopPanelNew.this.getState().getPlayer(), var2);
            }

         }
      }, var2) {
         public void draw() {
            if (ShopPanelNew.this.isOwnShop()) {
               super.draw();
            }

         }
      };
      ShopInterface var4 = this.getState().getCurrentClosestShop();
      if (this.isOwnShop()) {
         for(int var5 = 0; var5 < var6.getList().size(); ++var5) {
            if ((Long)var6.getList().get(var5).getContent().getUserPointer() == var4.getPermissionToTrade()) {
               var6.setSelectedElement(var6.getList().get(var5));
            }
         }
      }

      return var6;
   }

   private GUIDropDownList getShopPermissionDropDown() {
      GUITextOverlay[] var1;
      (var1 = new GUITextOverlay[4])[0] = new GUITextOverlay(400, 20, this.getState());
      var1[0].setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_85);
      var1[0].setUserPointer(TradeManager.PERM_ALL);
      var1[1] = new GUITextOverlay(400, 20, this.getState());
      var1[1].setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_86);
      var1[1].setUserPointer(TradeManager.PERM_ALL_BUT_ENEMY);
      var1[2] = new GUITextOverlay(400, 20, this.getState());
      var1[2].setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_87);
      var1[2].setUserPointer(TradeManager.PERM_ALLIES_AND_FACTION);
      var1[3] = new GUITextOverlay(400, 20, this.getState());
      var1[3].setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_88);
      var1[3].setUserPointer(TradeManager.PERM_FACTION);
      GUIAncor[] var2 = new GUIAncor[var1.length];

      for(int var3 = 0; var3 < var2.length; ++var3) {
         var2[var3] = new GUIAncor(this.getState(), 100.0F, 20.0F);
         var2[var3].setUserPointer(var1[var3].getUserPointer());
         var2[var3].attach(var1[var3]);
         var1[var3].setPos(3.0F, 3.0F, 0.0F);
      }

      GUIDropDownList var6 = new GUIDropDownList(this.getState(), 400, 20, var1.length * 20 + 5, new DropDownCallback() {
         boolean first = true;

         public void onSelectionChanged(GUIListElement var1) {
            long var2 = (Long)var1.getContent().getUserPointer();
            ShopInterface var4 = ShopPanelNew.this.getState().getCurrentClosestShop();
            if (ShopPanelNew.this.isOwnShop()) {
               if (this.first) {
                  this.first = false;
                  return;
               }

               var4.getShoppingAddOn().clientRequestLocalPermission(ShopPanelNew.this.getState().getPlayer(), var2);
            }

         }
      }, var2) {
         public void draw() {
            if (ShopPanelNew.this.isOwnShop()) {
               super.draw();
            }

         }
      };
      ShopInterface var4 = this.getState().getCurrentClosestShop();
      if (this.isOwnShop()) {
         for(int var5 = 0; var5 < var6.getList().size(); ++var5) {
            if ((Long)var6.getList().get(var5).getContent().getUserPointer() == var4.getPermissionToPurchase()) {
               var6.setSelectedElement(var6.getList().get(var5));
            }
         }
      }

      return var6;
   }

   private void createBlockDescriptionPanel(GUIAncor var1) {
      GUIScrollablePanel var2 = new GUIScrollablePanel(10.0F, 10.0F, var1, this.getState());
      GUIAncor var3 = new GUIAncor(this.getState(), 450.0F, 200.0F);
      var2.setContent(var3);
      GUITextOverlay var4;
      (var4 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium20(), this.getState()) {
         public void draw() {
            super.draw();
            if (ShopPanelNew.this.isOwnShop()) {
               this.setPos(36.0F, 40.0F, 0.0F);
            } else {
               this.setPos(36.0F, 8.0F, 0.0F);
            }
         }
      }).setTextSimple(new Object() {
         String lastCache;
         short lastSel;

         public String toString() {
            if (this.lastSel != ShopPanelNew.this.getShopControlManager().getSelectedElementClass() || this.lastCache == null) {
               if (ElementKeyMap.isValidType(ShopPanelNew.this.getShopControlManager().getSelectedElementClass())) {
                  this.lastCache = ElementKeyMap.getInfo(ShopPanelNew.this.getShopControlManager().getSelectedElementClass()).getName();
               } else if (ShopPanelNew.this.getShopControlManager().getSelectedElementClass() == -32768) {
                  this.lastCache = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_27;
               } else {
                  this.lastCache = "";
               }

               this.lastSel = ShopPanelNew.this.getShopControlManager().getSelectedElementClass();
            }

            return this.lastCache;
         }
      });
      GUITextOverlay var5;
      (var5 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium16(), this.getState())).setTextSimple(new Object() {
         public String toString() {
            if (ShopPanelNew.this.isOwnShop()) {
               ShopInterface var1;
               if ((var1 = ShopPanelNew.this.getState().getCurrentClosestShop()) != null && ElementKeyMap.isValidType(ShopPanelNew.this.getShopControlManager().getSelectedElementClass())) {
                  int var2;
                  return (var2 = var1.getPriceString(ElementKeyMap.getInfo(ShopPanelNew.this.getShopControlManager().getSelectedElementClass()), false)) > 0 ? "Shop owner buying this block for " + StringTools.formatSmallAndBig(var2) : "Can't sell";
               } else {
                  return "";
               }
            } else {
               return "";
            }
         }
      });
      GUITextOverlay var6;
      (var6 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium16(), this.getState())).setTextSimple(new Object() {
         public String toString() {
            if (ShopPanelNew.this.isOwnShop()) {
               ShopInterface var1;
               if ((var1 = ShopPanelNew.this.getState().getCurrentClosestShop()) != null && ElementKeyMap.isValidType(ShopPanelNew.this.getShopControlManager().getSelectedElementClass())) {
                  int var2;
                  return (var2 = var1.getPriceString(ElementKeyMap.getInfo(ShopPanelNew.this.getShopControlManager().getSelectedElementClass()), true)) > 0 ? "Shop owner selling this block for " + StringTools.formatSmallAndBig(var2) : "Can't buy";
               } else {
                  return "";
               }
            } else {
               return "";
            }
         }
      });
      GUITextOverlay var7;
      (var7 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium16(), this.getState())).setTextSimple(new Object() {
         public String toString() {
            ShopInterface var1;
            if ((var1 = ShopPanelNew.this.getState().getCurrentClosestShop()) != null && ElementKeyMap.isValidType(ShopPanelNew.this.getShopControlManager().getSelectedElementClass())) {
               int var2;
               return (var2 = var1.getShopInventory().getOverallQuantity(ShopPanelNew.this.getShopControlManager().getSelectedElementClass())) > 0 ? "STOCK: " + StringTools.formatSeperated(var2) : "Empty";
            } else {
               return "";
            }
         }
      });
      var5.setPos(1.0F, 1.0F, 0.0F);
      var6.setPos(1.0F, 18.0F, 0.0F);
      var7.setPos(300.0F, 9.0F, 0.0F);
      GUIBlockSprite var8 = new GUIBlockSprite(this.getState(), 0) {
         public void draw() {
            if (ElementKeyMap.isValidType(ShopPanelNew.this.getShopControlManager().getSelectedElementClass())) {
               this.type = ShopPanelNew.this.getShopControlManager().getSelectedElementClass();
               super.draw();
               this.setScale(0.5F, 0.5F, 0.0F);
               if (ShopPanelNew.this.isOwnShop()) {
                  this.setPos(1.0F, 33.0F, 0.0F);
                  return;
               }

               this.setPos(1.0F, 1.0F, 0.0F);
            }

         }
      };
      final GUITextOverlay var9;
      (var9 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium14(), this.getState()) {
         public void draw() {
            if (!ElementKeyMap.isValidType(ShopPanelNew.this.getShopControlManager().getSelectedElementClass()) && ShopPanelNew.this.getShopControlManager().getSelectedElementClass() != -32768) {
               this.setPos(4.0F, 4.0F, 0.0F);
            } else {
               this.setPos(4.0F, 36.0F, 0.0F);
            }

            if (ShopPanelNew.this.isOwnShop()) {
               Vector3f var10000 = this.getPos();
               var10000.y += 32.0F;
            }

            super.draw();
         }
      }).setTextSimple(new Object() {
         String lastCache;
         short lastSel;
         float lastParentSize = 0.0F;

         public String toString() {
            try {
               if (this.lastSel != ShopPanelNew.this.getShopControlManager().getSelectedElementClass() || this.lastCache == null || this.lastParentSize != ((GUIScrollablePanel)var9.getParent().getParent()).getWidth()) {
                  this.lastParentSize = ((GUIScrollablePanel)var9.getParent().getParent()).getWidth();
                  if (!ElementKeyMap.isValidType(ShopPanelNew.this.getShopControlManager().getSelectedElementClass())) {
                     if (ShopPanelNew.this.getShopControlManager().getSelectedElementClass() == -32768) {
                        this.lastCache = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_29;
                     } else {
                        this.lastCache = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_30;
                     }
                  } else {
                     String[] var1 = ElementKeyMap.getInfo(ShopPanelNew.this.getShopControlManager().getSelectedElementClass()).parseDescription(ShopPanelNew.this.getState());
                     StringBuilder var2 = new StringBuilder();
                     int var3 = 0;

                     while(true) {
                        if (var3 >= var1.length) {
                           this.lastCache = var2.toString();
                           break;
                        }

                        String[] var4 = var1[var3].split(" ");
                        String var5 = "";
                        int var6 = (var4 = var4).length;

                        for(int var7 = 0; var7 < var6; ++var7) {
                           String var8 = var4[var7];
                           var8 = var8 + " ";
                           if ((float)var9.getFont().getWidth(var5 + var8) >= this.lastParentSize) {
                              var2.append(var5);
                              var2.append("\n");
                              var5 = var8;
                           } else {
                              var5 = var5 + var8;
                           }
                        }

                        var2.append(var5);
                        var2.append("\n");
                        ++var3;
                     }
                  }

                  this.lastSel = ShopPanelNew.this.getShopControlManager().getSelectedElementClass();
               }

               return this.lastCache;
            } catch (Exception var9x) {
               return "error";
            }
         }
      });
      var3.attach(var8);
      var3.attach(var4);
      var3.attach(var9);
      var3.attach(var5);
      var3.attach(var6);
      var3.attach(var7);
      var1.attach(var2);
   }

   private void rebootShip() {
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().popupShipRebootBuyDialog();
   }

   private void createShopRepairsButtonPanel(GUIAncor var1) {
      GUIHorizontalButtonTablePane var2;
      (var2 = new GUIHorizontalButtonTablePane(this.getState(), 1, 2, var1)).onInit();
      var2.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_31, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ShopPanelNew.this.rebootShip();
            }

         }

         public boolean isOccluded() {
            return !ShopPanelNew.this.isActive();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return ShopPanelNew.this.getState().getShip() != null && !ShopPanelNew.this.getState().getShip().getHpController().isRebooting() && ShopPanelNew.this.getState().getShip().getHpController().getHpPercent() < 1.0D;
         }
      });
      var1.attach(var2);
   }

   private void createShopOptionsButtonPanel(GUIAncor var1) {
      GUIHorizontalButtonTablePane var2;
      (var2 = new GUIHorizontalButtonTablePane(this.getState(), 2, 3, var1)).onInit();
      var2.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_33, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ShopPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ShopPanelNew.popupDeposit(ShopPanelNew.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_57);
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return ShopPanelNew.this.isShopTrader();
         }
      });
      var2.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_34, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ShopPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ShopPanelNew.this.popupWithdrawal();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return ShopPanelNew.this.isShopTrader();
         }
      });
      var2.addButton(0, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_35, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ShopPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ShopPanelNew.this.popupAddShopOwner();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return ShopPanelNew.this.isOwnShop();
         }
      });
      var2.addButton(1, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_36, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ShopPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ShopPanelNew.this.popupRemoveShopOwner();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return ShopPanelNew.this.isOwnShop();
         }
      });
      final SegmentController var3 = this.currentShop.getSegmentController();
      final ShopInventory var4 = this.currentShop.getShopInventory();
      if (var3 instanceof ManagedSegmentController) {
         long var5;
         if ((var5 = ((ManagedSegmentController)var3).getManagerContainer().shopBlockIndex) != Long.MIN_VALUE) {
            final SegmentPiece var8;
            if ((var8 = var3.getSegmentBuffer().getPointUnsave(var5)) != null) {
               var2.addButton(0, 2, new Object() {
                  public String toString() {
                     var8.refresh();
                     ShopPanelNew.this.inventoryActive = var8.isActive();
                     return ShopPanelNew.this.inventoryActive ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_89 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_90;
                  }
               }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
                  public void callback(GUIElement var1, MouseEvent var2) {
                     if (var2.pressedLeftMouse()) {
                        var8.refresh();
                        long var3 = ElementCollection.getEncodeActivation(var8, true, !var8.isActive(), false);
                        var8.getSegment().getSegmentController().sendBlockActivation(var3);
                     }

                  }

                  public boolean isOccluded() {
                     return !ShopPanelNew.this.isActive();
                  }
               }, new GUIActivationHighlightCallback() {
                  public boolean isVisible(InputState var1) {
                     return true;
                  }

                  public boolean isActive(InputState var1) {
                     return ShopPanelNew.this.isShopTrader();
                  }

                  public boolean isHighlighted(InputState var1) {
                     return ShopPanelNew.this.inventoryActive;
                  }
               });
               var2.addButton(1, 2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_91, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
                  public boolean isOccluded() {
                     return !ShopPanelNew.this.isActive();
                  }

                  public void callback(GUIElement var1, MouseEvent var2) {
                     if (var2.pressedLeftMouse()) {
                        InventoryOptionsChestButtonPanel.popupProductionDialog(ShopPanelNew.this.getState(), var3, var4, var8);
                     }

                  }
               }, new GUIActivationCallback() {
                  public boolean isVisible(InputState var1) {
                     return true;
                  }

                  public boolean isActive(InputState var1) {
                     return ShopPanelNew.this.isShopTrader();
                  }
               });
               var1.attach(var2);
               GUIHorizontalProgressBar var7;
               (var7 = new GUIHorizontalProgressBar(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_92, var1) {
                  public float getValue() {
                     if (ShopPanelNew.this.inventoryActive) {
                        this.text = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_93;
                        return (float)((double)(ShopPanelNew.this.getState().getController().getServerRunningTime() % 10000L) / 10000.0D);
                     } else {
                        this.text = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_94;
                        return 0.0F;
                     }
                  }
               }).getColor().set(InventoryPanelNew.PROGRESS_COLOR);
               var7.onInit();
               var7.getPos().x = 1.0F;
               var7.getPos().y = 74.0F;
               var1.attach(var7);
            }
         }
      }
   }

   private boolean canBuyOne() {
      ShopInterface var1 = this.getState().getCurrentClosestShop();
      return isPurchasePermission(this.getState()) && var1 != null && ElementKeyMap.isValidType(this.getShopControlManager().getSelectedElementClass()) && (var1.getShopInventory().getOverallQuantity(this.getShopControlManager().getSelectedElementClass()) > 0 || var1.isInfiniteSupply()) && (this.isOwnPlayerOwner(var1) || var1.getShoppingAddOn().canAfford(this.getState().getPlayer(), this.getShopControlManager().getSelectedElementClass(), 1) > 0);
   }

   private boolean canSellOne() {
      ShopInterface var1 = this.getState().getCurrentClosestShop();
      return isPurchasePermission(this.getState()) && var1 != null && ElementKeyMap.isValidType(this.getShopControlManager().getSelectedElementClass()) && this.getState().getPlayer().getInventory().getOverallQuantity(this.getShopControlManager().getSelectedElementClass()) > 0 && (this.isOwnPlayerOwner(var1) || var1.getShoppingAddOn().canShopAfford(this.getShopControlManager().getSelectedElementClass(), 1) > 0);
   }

   private boolean isOwnPlayerOwner(ShopInterface var1) {
      return var1.getShopOwners().isEmpty() || var1.getShopOwners().contains(this.getOwnPlayer().getName().toLowerCase(Locale.ENGLISH));
   }

   private void createBuySellButtonPanel(GUIAncor var1) {
      GUIHorizontalButtonTablePane var2;
      (var2 = new GUIHorizontalButtonTablePane(this.getState(), 2, 3, var1)).onInit();
      var2.addButton(0, 0, new Object() {
         public String toString() {
            return ShopPanelNew.this.isShopTrader() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_37 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_38;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ShopPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ShopPanelNew.this.buyOne(ShopPanelNew.this.getShopControlManager().getSelectedElementClass());
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return ShopPanelNew.this.canBuyOne();
         }
      });
      var2.addButton(1, 0, new Object() {
         public String toString() {
            return ShopPanelNew.this.isShopTrader() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_39 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_40;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ShopPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (!ShopPanelNew.this.getState().getPlayer().getInventory().isLockedInventory()) {
                  ShopPanelNew.this.sellOne(ShopPanelNew.this.getShopControlManager().getSelectedElementClass());
                  return;
               }

               ShopPanelNew.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_46, 0.0F);
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return ShopPanelNew.this.canSellOne();
         }
      });
      var2.addButton(0, 1, new Object() {
         public String toString() {
            return ShopPanelNew.this.isShopTrader() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_42 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_43;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ShopPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ShopPanelNew.this.buyMore(ShopPanelNew.this.getShopControlManager().getSelectedElementClass());
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return ShopPanelNew.this.canBuyOne();
         }
      });
      var2.addButton(1, 1, new Object() {
         public String toString() {
            return ShopPanelNew.this.isShopTrader() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_44 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_45;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ShopPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (!ShopPanelNew.this.getState().getPlayer().getInventory().isLockedInventory()) {
                  ShopPanelNew.this.sellMore(ShopPanelNew.this.getShopControlManager().getSelectedElementClass());
                  return;
               }

               ShopPanelNew.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_41, 0.0F);
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return ShopPanelNew.this.canSellOne();
         }
      });
      var2.addButton(1, 2, new Object() {
         public String toString() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_47;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ShopPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ShopInterface var3 = ShopPanelNew.this.getState().getCurrentClosestShop();
               short var5 = ShopPanelNew.this.getShopControlManager().getSelectedElementClass();
               if (var3 != null && ElementKeyMap.isValidType(var5)) {
                  ElementInformation var6 = ElementKeyMap.getInfo(var5);
                  TradePriceInterface var4 = var3.getPrice(var6.getId(), false);
                  ShopPanelNew.this.editBuyPrice((int)(var4 != null && var4.getPrice() > 0 ? (long)var4.getPrice() : var6.getPrice(true)));
               }
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return ShopPanelNew.this.isShopTrader();
         }

         public boolean isActive(InputState var1) {
            return ShopPanelNew.this.isShopTrader() && ElementKeyMap.isValidType(ShopPanelNew.this.getShopControlManager().getSelectedElementClass());
         }
      });
      var2.addButton(0, 2, new Object() {
         public String toString() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_48;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ShopPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ShopInterface var3 = ShopPanelNew.this.getState().getCurrentClosestShop();
               short var5 = ShopPanelNew.this.getShopControlManager().getSelectedElementClass();
               if (var3 != null && ElementKeyMap.isValidType(var5)) {
                  ElementInformation var6 = ElementKeyMap.getInfo(var5);
                  TradePriceInterface var4 = var3.getPrice(var6.getId(), true);
                  ShopPanelNew.this.editSellPrice((int)(var4 != null && var4.getPrice() > 0 ? (long)var4.getPrice() : var6.getPrice(true)));
               }
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return ShopPanelNew.this.isShopTrader();
         }

         public boolean isActive(InputState var1) {
            return ShopPanelNew.this.isShopTrader() && ElementKeyMap.isValidType(ShopPanelNew.this.getShopControlManager().getSelectedElementClass());
         }
      });
      var1.attach(var2);
   }

   protected void editBuyPrice(int var1) {
      ObjectArrayList var2 = new ObjectArrayList();
      GUIAncor var3 = new GUIAncor(this.getState(), 100.0F, 32.0F);
      GUITextOverlay var4;
      (var4 = new GUITextOverlay(1, 1, FontLibrary.getBlenderProMedium18(), this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_49);
      var4.setPos(8.0F, 8.0F, 0.0F);
      var3.attach(var4);
      var3.setUserPointer("credits");
      var2.add(var3);
      (new PlayerBlockTypeDropdownInputNew("EditShopPriceDialog_AMOUNT", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_50, var2, 1, var1, false) {
         public void onOk(ElementInformation var1) {
            this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_28, 0.0F);
         }

         public void onAdditionalElementOk(Object var1) {
            ShopInterface var3;
            if ((var3 = this.getState().getCurrentClosestShop()) != null && ElementKeyMap.isValidType(ShopPanelNew.this.getShopControlManager().getSelectedElementClass())) {
               TradePrice var2;
               (var2 = ShoppingAddOn.getPriceInstance(var3, ShopPanelNew.this.getShopControlManager().getSelectedElementClass(), false)).setPrice(this.getNumberValue());
               var3.getShoppingAddOn().clientRequestSetPrice(this.getState().getPlayer(), var2);
            }

         }

         public void onOkMeta(MetaObject var1) {
         }
      }).activate();
   }

   protected void editSellPrice(int var1) {
      ObjectArrayList var2 = new ObjectArrayList();
      GUIAncor var3 = new GUIAncor(this.getState(), 100.0F, 32.0F);
      GUITextOverlay var4;
      (var4 = new GUITextOverlay(1, 1, FontLibrary.getBlenderProMedium18(), this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_51);
      var4.setPos(8.0F, 8.0F, 0.0F);
      var3.attach(var4);
      var3.setUserPointer("credits");
      var2.add(var3);
      (new PlayerBlockTypeDropdownInputNew("EditShopPriceDialog_AMOUNT", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_52, var2, 1, var1, false) {
         public void onOk(ElementInformation var1) {
            this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_101, 0.0F);
         }

         public void onAdditionalElementOk(Object var1) {
            ShopInterface var3;
            if ((var3 = this.getState().getCurrentClosestShop()) != null && ElementKeyMap.isValidType(ShopPanelNew.this.getShopControlManager().getSelectedElementClass())) {
               TradePrice var2;
               (var2 = ShoppingAddOn.getPriceInstance(var3, ShopPanelNew.this.getShopControlManager().getSelectedElementClass(), true)).setPrice(this.getNumberValue());
               var3.getShoppingAddOn().clientRequestSetPrice(this.getState().getPlayer(), var2);
            }

         }

         public void onOkMeta(MetaObject var1) {
         }
      }).activate();
   }

   public boolean isOwnShop() {
      return isOwnShop(this.getState());
   }

   private boolean isShopTrader() {
      return isShopTrader(this.getState());
   }

   public static boolean isShopTrader(GameClientState var0) {
      return ShoppingAddOn.isTradePermShop(var0, var0.getCurrentClosestShop());
   }

   public static boolean isOwnShop(GameClientState var0) {
      return ShoppingAddOn.isSelfOwnedShop(var0, var0.getCurrentClosestShop());
   }

   private static boolean isPurchasePermission(GameClientState var0) {
      return ShoppingAddOn.isPurchasePermission(var0.getPlayer(), var0.getCurrentClosestShop());
   }

   public static void popupDeposit(final GameClientState var0, String var1) {
      (new PlayerGameTextInput("ENTER_NAME", var0, 10, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_53, var1) {
         public final boolean isOccluded() {
            return false;
         }

         public final String[] getCommandPrefixes() {
            return null;
         }

         public final void onDeactivate() {
         }

         public final String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public final void onFailedTextCheck(String var1) {
         }

         public final boolean onInput(String var1) {
            if (var1.length() <= 0) {
               return false;
            } else {
               ShopInterface var2 = this.getState().getCurrentClosestShop();
               if (!ShopPanelNew.isShopTrader(var0)) {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_58, 0.0F);
               } else {
                  try {
                     long var3;
                     int var6 = (int)Math.min(var3 = Long.parseLong(var1.trim()), 2147483647L);
                     if ((long)this.getState().getPlayer().getCredits() < var3) {
                        var0.getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_95, 0.0F);
                        this.setText(String.valueOf(this.getState().getPlayer().getCredits()));
                        return false;
                     }

                     var2.getShoppingAddOn().clientRequestDeposit(this.getState().getPlayer(), var6);
                  } catch (NumberFormatException var5) {
                     var5.printStackTrace();
                     var0.getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_98, 0.0F);
                     return false;
                  }
               }

               return true;
            }
         }
      }).activate();
   }

   public void popupWithdrawal() {
      (new PlayerGameTextInput("ENTER_NAME", this.getState(), 10, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_56, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_54) {
         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public boolean isOccluded() {
            return false;
         }

         public void onDeactivate() {
         }

         public void onFailedTextCheck(String var1) {
         }

         public boolean onInput(String var1) {
            if (var1.length() <= 0) {
               return false;
            } else {
               ShopInterface var2 = this.getState().getCurrentClosestShop();
               if (!ShopPanelNew.this.isShopTrader()) {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_55, 0.0F);
               } else {
                  try {
                     long var3;
                     int var6 = (int)Math.min(var3 = Long.parseLong(var1.trim()), 2147483647L);
                     if (var2.getShoppingAddOn().getCredits() < var3) {
                        this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_97, 0.0F);
                        this.setText(String.valueOf(var2.getShoppingAddOn().getCredits()));
                        return false;
                     }

                     var2.getShoppingAddOn().clientRequestWithdrawal(this.getState().getPlayer(), var6);
                  } catch (NumberFormatException var5) {
                     var5.printStackTrace();
                     this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_96, 0.0F);
                     return false;
                  }
               }

               return true;
            }
         }
      }).activate();
   }

   public void popupAddShopOwner() {
      (new PlayerGameTextInput("ENTER_NAME", this.getState(), 80, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_59, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_60) {
         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public boolean isOccluded() {
            return false;
         }

         public void onDeactivate() {
         }

         public void onFailedTextCheck(String var1) {
         }

         public boolean onInput(String var1) {
            if (var1.length() < 3) {
               return false;
            } else {
               ShopInterface var2 = this.getState().getCurrentClosestShop();
               if (!ShopPanelNew.this.isOwnShop()) {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_61, 0.0F);
               } else {
                  var2.getShoppingAddOn().clientRequestPlayerAdd(this.getState().getPlayer(), var1.trim());
               }

               return true;
            }
         }
      }).activate();
   }

   public void popupRemoveShopOwner() {
      (new PlayerGameTextInput("ENTER_NAME", this.getState(), 80, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_62, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_63) {
         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public boolean isOccluded() {
            return false;
         }

         public void onDeactivate() {
         }

         public void onFailedTextCheck(String var1) {
         }

         public boolean onInput(String var1) {
            if (var1.length() < 3) {
               return false;
            } else {
               ShopInterface var2 = this.getState().getCurrentClosestShop();
               if (!ShopPanelNew.this.isOwnShop()) {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_64, 0.0F);
               } else {
                  var2.getShoppingAddOn().clientRequestPlayerRemove(this.getState().getPlayer(), var1.trim());
               }

               return true;
            }
         }
      }).activate();
   }

   public ShopControllerManager getShopControlManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getShopControlManager();
   }

   private void buyOne(short var1) {
      if (this.getShopControlManager().canBuy(var1)) {
         this.getState().getController().queueUIAudio("0022_action - purchase with credits");
         this.getState().getPlayer().getInventoryController().buy(var1, 1);
      }

   }

   private void sellOne(short var1) {
      if (this.getShopControlManager().canSell(var1)) {
         this.getState().getController().queueUIAudio("0022_action - receive credits");
         this.getState().getPlayer().getInventoryController().sell(var1, 1);
      }

   }

   private void buyMore(short var1) {
      ShopInterface var2;
      if ((var2 = this.getState().getCurrentClosestShop()) != null) {
         String var3 = ShoppingAddOn.isSelfOwnedShop(this.getState(), var2) ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_65 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_66;
         synchronized(this.getState().getController().getPlayerInputs()) {
            this.getState().getController().getPlayerInputs().add(new BuyQuantityDialog(this.getState(), var3, var1, 1, var2));
         }
      }
   }

   private void sellMore(short var1) {
      ShopInterface var2;
      if ((var2 = this.getState().getCurrentClosestShop()) != null) {
         this.getShopControlManager().openSellDialog(var1, 1, var2);
      } else {
         this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_67, 0.0F);
      }
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFactionManager().getFaction(this.getOwnPlayer().getFactionId());
   }

   public float getHeight() {
      return this.shopPanel.getHeight();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public float getWidth() {
      return this.shopPanel.getWidth();
   }

   public boolean isActive() {
      return this.getState().getController().getPlayerInputs().isEmpty();
   }

   public void reset() {
      if (this.shopPanel != null) {
         this.shopPanel.reset();
      }

   }

   public void checkTarget(MouseEvent var1) {
      if (var1.releasedLeftMouse()) {
         if (this.getState().getController().getInputController().getDragging() != null && this.isTarget(this.getState().getController().getInputController().getDragging()) && this.getState().getController().getInputController().getDragging() != this) {
            if (System.currentTimeMillis() - this.getState().getController().getInputController().getDragging().getTimeDragStarted() > 200L) {
               System.err.println("NOW DROPPING " + this.getState().getController().getInputController().getDragging());
               this.onDrop((InventorySlotOverlayElement)this.getState().getController().getInputController().getDragging());
            } else {
               System.err.println("NO DROP: time dragged to short");
            }

            this.getState().getController().getInputController().setDragging((Draggable)null);
         }

         if (this.getState().getController().getInputController().getDragging() != null && this.getState().getController().getInputController().getDragging() == this) {
            System.err.println("NO DROP: dragging and target are the same");
         }

         if (this.getState().getController().getInputController().getDragging() != null && !this.isTarget(this.getState().getController().getInputController().getDragging())) {
            System.err.println("NO DROP: not a target: " + this);
         }
      }

   }

   public boolean isTarget(Draggable var1) {
      return true;
   }

   public void onDrop(InventorySlotOverlayElement var1) {
      var1.setStickyDrag(false);
      ShopInterface var2;
      if ((var2 = this.getState().getCurrentClosestShop()) != null) {
         if (var1.getType() > 0) {
            if (!this.getState().getPlayer().getInventory().isLockedInventory()) {
               this.getShopControlManager().openSellDialog(var1.getType(), var1.getCount(true), var2);
            }
         } else if (var1.getType() < 0) {
            this.getState().getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_69, 0.0F);
         }
      } else {
         this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHOP_SHOPNEW_SHOPPANELNEW_70, 0.0F);
      }

      var1.reset();
   }
}
