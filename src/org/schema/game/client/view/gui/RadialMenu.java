package org.schema.game.client.view.gui;

import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector4f;
import org.newdawn.slick.UnicodeFont;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class RadialMenu extends GUIElement {
   public static final long MAX_FADE = 160L;
   private int height;
   private int width;
   private float radius;
   private float centerRadius;
   private UnicodeFont font;
   private final RadialMenuCallback radialMenuCallback;
   Vector4f deactivated;
   Vector4f color;
   Vector4f colorSelected;
   Vector4f highlight;
   Vector4f highlightSelected;
   Vector4f textColor;
   private RadialMenu parentMenu;
   private final List items;
   private RadialMenuCenter center;
   private RadialMenu fadingOutMenu;
   private long fadingOutTime;
   private long fadeInTime;
   private boolean forceBackButton;
   public final String menuId;
   public GUIElement posElem;
   private static final long TOOLTIP_TIME_AFTER_MS = 1200L;
   private static final long TOOLTIP_TIME_FIRST_MS = 150L;
   public static final Object2IntOpenHashMap tooltipMenuCount = new Object2IntOpenHashMap();

   public RadialMenu(InputState var1, String var2, RadialMenuCallback var3, int var4, int var5, int var6, UnicodeFont var7) {
      this(var1, var2, var3, (RadialMenu)null);
      this.height = var5;
      this.width = var4;
      this.radius = (float)(Math.min(var4, var5) / 2);
      this.centerRadius = (float)var6;
      this.font = var7;
   }

   public RadialMenu(InputState var1, String var2, RadialMenuCallback var3, RadialMenu var4) {
      super(var1);
      this.deactivated = new Vector4f(0.1334F, 0.1569F, 0.18F, 0.8F);
      this.color = new Vector4f(0.2941F, 0.345F, 0.4039F, 0.8F);
      this.colorSelected = new Vector4f(0.1372F, 0.549F, 0.67F, 0.8F);
      this.highlight = new Vector4f(0.2941F, 0.485F, 0.5439F, 0.92F);
      this.highlightSelected = new Vector4f(0.1372F, 0.549F, 0.67F, 0.92F);
      this.textColor = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
      this.items = new ObjectArrayList();
      this.menuId = var2;
      tooltipMenuCount.add(var2, 1);
      this.radialMenuCallback = var3;
      this.parentMenu = var4;
      this.center = new RadialMenuCenter(var1, this, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENU_0, (GUIActivationCallback)null);
      this.fadeIn();
   }

   public long getToolTipTime() {
      return tooltipMenuCount.get(this.menuId) < 2 ? 150L : 1200L;
   }

   public void addItem(Object var1, GUICallback var2, GUIActivationCallback var3) {
      this.addItem(var1, var2, var3, true);
   }

   public void addItem(Object var1, final GUICallback var2, final GUIActivationCallback var3, boolean var4) {
      GUICallback var5 = var2;
      if (var2 != null && var3 != null) {
         var5 = new GUICallback() {
            public boolean isOccluded() {
               return !var3.isActive(RadialMenu.this.getState()) || var2.isOccluded();
            }

            public void callback(GUIElement var1, MouseEvent var2x) {
               var2.callback(var1, var2x);
            }
         };
      }

      RadialMenuItemText var6;
      (var6 = new RadialMenuItemText(this.getState(), this, this.getTotalSlices(), var1, var3, var5)).setActivateOnDeactiveRadial(var4);
      this.items.add(var6);
   }

   public void addItemBlock(short var1, final GUICallback var2, Object var3, final GUIActivationCallback var4) {
      GUICallback var5 = var2;
      if (var2 != null && var4 != null) {
         var5 = new GUICallback() {
            public boolean isOccluded() {
               return !var4.isActive(RadialMenu.this.getState()) || var2.isOccluded();
            }

            public void callback(GUIElement var1, MouseEvent var2x) {
               var2.callback(var1, var2x);
            }
         };
      }

      RadialMenuItemBlock var6;
      (var6 = new RadialMenuItemBlock(this.getState(), this, this.getTotalSlices(), var1, var4, var5)).setToolTip(var3);
      this.items.add(var6);
   }

   public RadialMenu addItemAsSubMenu(Object var1, GUIActivationCallback var2) {
      RadialMenu var3 = new RadialMenu(this.getState(), this.menuId + "sub-" + this.items.size(), this.getRadialMenuCallback(), this);
      RadialMenuItemText var4;
      (var4 = new RadialMenuItemText(this.getState(), this, this.getTotalSlices(), var1, var2, (GUICallback)null)).setChildMenu(var3);
      this.items.add(var4);
      return var3;
   }

   public void setFadingOut(RadialMenu var1) {
      this.fadingOutTime = System.currentTimeMillis();
      this.fadingOutMenu = var1;
   }

   public int getCenterX() {
      assert this.getWidth() > 0.0F;

      return (int)(this.getWidth() / 2.0F);
   }

   public int getCenterY() {
      return (int)(this.getHeight() / 2.0F);
   }

   public float getHeight() {
      return this.parentMenu != null ? this.parentMenu.getHeight() : (float)this.height;
   }

   public float getWidth() {
      if (this.parentMenu != null) {
         return this.parentMenu.getWidth();
      } else {
         assert this.width > 0;

         return (float)this.width;
      }
   }

   public void cleanUp() {
   }

   public void fadeIn() {
      this.fadeInTime = System.currentTimeMillis();
   }

   public void draw() {
      if (this.posElem != null) {
         this.setPos((float)((int)(this.posElem.getPos().x - this.getWidth() / 2.0F)), (float)((int)(this.posElem.getPos().y - this.getHeight() / 2.0F)), 0.0F);
      } else {
         this.setPos((float)((int)((float)(GLFrame.getWidth() / 2) - this.getWidth() / 2.0F)), (float)((int)((float)(GLFrame.getHeight() / 2) - this.getHeight() / 2.0F)), 0.0F);
      }

      GlUtil.glPushMatrix();
      this.transform();
      long var1;
      if ((var1 = System.currentTimeMillis() - this.fadeInTime) < 160L) {
         float var3 = (float)var1 / 160.0F;
         this.drawFading(var3);
      } else {
         if (this.parentMenu != null || this.isForceBackButton()) {
            this.center.draw();
         }

         int var6;
         for(var6 = 0; var6 < this.items.size(); ++var6) {
            ((RadialMenuItem)this.items.get(var6)).draw();
         }

         for(var6 = 0; var6 < this.items.size(); ++var6) {
            ((RadialMenuItem)this.items.get(var6)).drawLabel();
         }

         if (this.fadingOutMenu != null) {
            long var7;
            if ((var7 = System.currentTimeMillis() - this.fadingOutTime) < 160L) {
               float var5 = 1.0F - (float)var7 / 160.0F;
               this.fadingOutMenu.drawFading(var5);
            } else {
               this.fadingOutMenu = null;
            }
         }
      }

      GlUtil.glPopMatrix();
      if (this.parentMenu == null) {
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      }

   }

   private void drawFading(float var1) {
      int var2;
      for(var2 = 0; var2 < this.items.size(); ++var2) {
         ((RadialMenuItem)this.items.get(var2)).drawFading(var1, var1, false, false);
      }

      for(var2 = 0; var2 < this.items.size(); ++var2) {
         ((RadialMenuItem)this.items.get(var2)).drawFadingText(var1, var1, false, false);
      }

   }

   public void onInit() {
   }

   public int getTotalSlices() {
      return this.items.size();
   }

   public RadialMenuCallback getRadialMenuCallback() {
      return this.radialMenuCallback;
   }

   public RadialMenu getParentMenu() {
      return this.parentMenu;
   }

   public void setParentMenu(RadialMenu var1) {
      this.parentMenu = var1;
   }

   public float getRadius() {
      return this.parentMenu != null ? this.parentMenu.getRadius() : this.radius;
   }

   public float getCenterRadius() {
      return this.parentMenu != null ? this.parentMenu.getCenterRadius() : this.centerRadius;
   }

   public UnicodeFont getFont() {
      return this.parentMenu != null ? this.parentMenu.getFont() : this.font;
   }

   public void activateSelected() {
      Iterator var1 = this.items.iterator();

      while(var1.hasNext()) {
         ((RadialMenuItem)var1.next()).activateSelected();
      }

   }

   public boolean isForceBackButton() {
      return this.forceBackButton;
   }

   public void setForceBackButton(boolean var1) {
      this.forceBackButton = var1;
   }
}
