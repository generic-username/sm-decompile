package org.schema.game.client.view.gui.structurecontrol;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;

public abstract class ActivateValueEntry implements GUIKeyValueEntry, GUICallback {
   private final Object name;

   public ActivateValueEntry(Object var1) {
      this.name = var1;
   }

   public GUIAncor get(GameClientState var1) {
      GUITextButton var2;
      (var2 = new GUITextButton(var1, 150, 20, this.name, this)).setTextPos(2, 0);
      return var2;
   }
}
