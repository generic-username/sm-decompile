package org.schema.game.client.view.gui.structurecontrol.structurenew;

import java.util.Collection;
import java.util.Set;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class InnerStructureScrollableListNew extends ScrollableTableList {
   public InnerStructureScrollableListNew(InputState var1, int var2, int var3, GUIElement var4) {
      super(var1, (float)var2, (float)var3, var4);
   }

   public void initColumns() {
   }

   protected Collection getElementList() {
      return null;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
   }
}
