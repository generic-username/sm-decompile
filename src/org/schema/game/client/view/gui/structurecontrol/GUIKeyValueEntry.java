package org.schema.game.client.view.gui.structurecontrol;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;

public interface GUIKeyValueEntry {
   GUIAncor get(GameClientState var1);
}
