package org.schema.game.client.view.gui.structurecontrol;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;

public class ModuleValue2Entry implements GUIKeyValueEntry {
   public final String name;
   public final Object value;
   private final Object value2;

   public ModuleValue2Entry(String var1, Object var2, Object var3) {
      this.name = var1;
      this.value = var2;
      this.value2 = var3;
   }

   public GUIAncor get(GameClientState var1) {
      GUIAncor var2 = new GUIAncor(var1, 300.0F, 18.0F);
      GUITextOverlay var3 = new GUITextOverlay(30, 18, FontLibrary.getRegularArial12WhiteWithoutOutline(), var1);
      GUITextOverlay var4 = new GUITextOverlay(30, 18, FontLibrary.getRegularArial12WhiteWithoutOutline(), var1);
      GUITextOverlay var5 = new GUITextOverlay(30, 18, FontLibrary.getRegularArial12WhiteWithoutOutline(), var1);
      var3.setTextSimple(this.name);
      var4.setTextSimple(this.value);
      var5.setTextSimple(this.value2);
      var4.setPos(160.0F, 0.0F, 0.0F);
      var5.setPos(200.0F, 0.0F, 0.0F);
      var2.attach(var3);
      var2.attach(var4);
      var2.attach(var5);
      return var2;
   }
}
