package org.schema.game.client.view.gui.shiphud;

import java.util.ArrayList;
import java.util.List;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ai.AiInterfaceContainer;
import org.schema.game.common.controller.ai.UnloadedAiEntityException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyboardMappings;

public class AiCrewAndFleetInformationPanel extends GUIAncor {
   Vector3i posFromSector = new Vector3i();
   private GUITextOverlay playerShipInfo;
   private long lastUpdate;

   public AiCrewAndFleetInformationPanel(InputState var1) {
      super(var1, 500.0F, 150.0F);
   }

   public void draw() {
      if (System.currentTimeMillis() - this.lastUpdate > 50L) {
         this.update();
         this.lastUpdate = System.currentTimeMillis();
      }

      super.draw();
   }

   public void onInit() {
      this.playerShipInfo = new GUITextOverlay(300, 40, FontLibrary.getRegularArial11White(), this.getState());
      this.playerShipInfo.setText(new ArrayList(10));
      this.playerShipInfo.getPos().x = 0.0F;
      this.playerShipInfo.getPos().y = 0.0F;
      this.attach(this.playerShipInfo);
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void update() {
      this.playerShipInfo.getText().clear();
      GameClientState var1;
      (var1 = (GameClientState)this.getState()).getPlayer().getCredits();
      PlayerInteractionControlManager var2 = var1.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager();
      var1.getCurrentPlayerObject();
      if (var1.getPlayer().getPlayerAiManager().hasCrew() && KeyboardMappings.CREW_CONTROL.isDown(var1)) {
         if (var2.getSelectedCrew() != null) {
            try {
               this.playerShipInfo.getText().add("selected: " + var2.getSelectedCrew().getRealName());
               this.playerShipInfo.getText().add("1: idle");
               this.playerShipInfo.getText().add("2: attack");
               this.playerShipInfo.getText().add("3: roam");
               this.playerShipInfo.getText().add("4: follow");
               this.playerShipInfo.getText().add("5: go to");
               this.playerShipInfo.getText().add("6: deselect");
               return;
            } catch (UnloadedAiEntityException var3) {
               this.playerShipInfo.getText().add("ERROR: entity not loaded");
               return;
            }
         }

         List var5 = var1.getPlayer().getPlayerAiManager().getCrew();

         for(int var6 = 0; var6 < var5.size(); ++var6) {
            try {
               this.playerShipInfo.getText().add("select " + (var6 + 1) + " " + ((AiInterfaceContainer)var5.get(var6)).getRealName());
            } catch (UnloadedAiEntityException var4) {
               this.playerShipInfo.getText().add("select " + (var6 + 1) + " " + ((AiInterfaceContainer)var5.get(var6)).getUID() + "(UNLOADED)");
            }
         }
      }

   }
}
