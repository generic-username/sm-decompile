package org.schema.game.client.view.gui.shiphud.newhud;

import javax.vecmath.Vector2f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.input.InputState;

public class TargetPlayerHealthBar extends FillableHorizontalBar {
   @ConfigurationElement(
      name = "Color"
   )
   public static Vector4i COLOR;
   @ConfigurationElement(
      name = "Offset"
   )
   public static Vector2f OFFSET;
   @ConfigurationElement(
      name = "FlipX"
   )
   public static boolean FLIPX;
   @ConfigurationElement(
      name = "FlipY"
   )
   public static boolean FLIPY;
   @ConfigurationElement(
      name = "FillStatusTextOnTop"
   )
   public static boolean FILL_ON_TOP;
   @ConfigurationElement(
      name = "TextPos"
   )
   public static Vector2f TEXT_POS;
   @ConfigurationElement(
      name = "TextDescPos"
   )
   public static Vector2f TEXT_DESC_POS;

   public Vector2f getTextPos() {
      return TEXT_POS;
   }

   public Vector2f getTextDescPos() {
      return TEXT_DESC_POS;
   }

   public TargetPlayerHealthBar(InputState var1) {
      super(var1);
   }

   public boolean isBarFlippedX() {
      return FLIPX;
   }

   public boolean isBarFlippedY() {
      return FLIPY;
   }

   public boolean isFillStatusTextOnTop() {
      return FILL_ON_TOP;
   }

   public float getFilled() {
      SimpleTransformableSendableObject var1;
      AbstractOwnerState var2;
      return (var1 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity()) != null && var1.getOwnerState() != null ? (var2 = var1.getOwnerState()).getHealth() / var2.getMaxHealth() : 0.0F;
   }

   public String getText() {
      SimpleTransformableSendableObject var1;
      if ((var1 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity()) != null && var1.getOwnerState() != null) {
         AbstractOwnerState var2 = var1.getOwnerState();
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_TARGETPLAYERHEALTHBAR_0 + var2.getHealth();
      } else {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_TARGETPLAYERHEALTHBAR_1;
      }
   }

   public Vector4i getConfigColor() {
      return COLOR;
   }

   public GUIPosition getConfigPosition() {
      return null;
   }

   public Vector2f getConfigOffset() {
      return OFFSET;
   }

   protected String getTag() {
      return "TargetPlayerHealthBar";
   }
}
