package org.schema.game.client.view.gui.shiphud.newhud;

import java.util.List;
import javax.vecmath.Vector2f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.controller.elements.ShieldLocal;
import org.schema.game.common.controller.elements.ShieldLocalAddOn;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.util.timer.LinearTimerUtil;
import org.schema.schine.graphicsengine.util.timer.TimerUtil;
import org.schema.schine.input.InputState;

public class ShieldBarRightLocal extends FillableBar {
   @ConfigurationElement(
      name = "Color"
   )
   public static Vector4i COLOR;
   @ConfigurationElement(
      name = "Position"
   )
   public static GUIPosition POSITION;
   @ConfigurationElement(
      name = "Offset"
   )
   public static Vector2f OFFSET;
   @ConfigurationElement(
      name = "FlipX"
   )
   public static boolean FLIPX;
   @ConfigurationElement(
      name = "FlipY"
   )
   public static boolean FLIPY;
   @ConfigurationElement(
      name = "FillStatusTextOnTop"
   )
   public static boolean FILL_ON_TOP;
   @ConfigurationElement(
      name = "OffsetText"
   )
   public static Vector2f OFFSET_TEXT;
   @ConfigurationElement(
      name = "SpriteSpan"
   )
   public static float SPRITE_SPAN;
   @ConfigurationElement(
      name = "ExtraTextYOffset"
   )
   public static float EXTRA_TEXT_Y_OFFSET;
   @ConfigurationElement(
      name = "TitleTextYOffset"
   )
   public static float TITLE_TEXT_Y_OFFSET;
   @ConfigurationElement(
      name = "TextXIndent"
   )
   public static float TEXT_X_INDENT;
   @ConfigurationElement(
      name = "XOffset"
   )
   public static float X_OFFSET;
   @ConfigurationElement(
      name = "StaticTextPos"
   )
   public static Vector2f STATIC_TEXT_POS;
   @ConfigurationElement(
      name = "StaticExtraTextPos"
   )
   public static Vector2f STATIC_EXTRA_TEXT_POS;
   @ConfigurationElement(
      name = "StaticTitleTextPos"
   )
   public static Vector2f STATIC_TITLE_TEXT_POS;
   public TimerUtil l = new LinearTimerUtil(0.5F);

   public ShieldBarRightLocal(InputState var1) {
      super(var1);
      this.drawExtraText = true;
   }

   public float getSpriteSpan() {
      return SPRITE_SPAN;
   }

   public float getTextIndent() {
      return TEXT_X_INDENT;
   }

   public float getExtraTextYOffset() {
      return EXTRA_TEXT_Y_OFFSET;
   }

   public float getTitleTextYOffset() {
      return TITLE_TEXT_Y_OFFSET;
   }

   public float getXOffset() {
      return X_OFFSET;
   }

   public Vector2f getStaticTextPos() {
      return STATIC_TEXT_POS;
   }

   public Vector2f getStaticExtraTextPos() {
      return STATIC_EXTRA_TEXT_POS;
   }

   public Vector2f getStaticTitleTextPos() {
      return STATIC_TITLE_TEXT_POS;
   }

   protected String getDisplayTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_SHIELDBARRIGHTLOCAL_0;
   }

   public boolean isBarFlippedX() {
      return FLIPX;
   }

   public boolean isBarFlippedY() {
      return FLIPY;
   }

   public boolean isFillStatusTextOnTop() {
      return FILL_ON_TOP;
   }

   public Vector2f getOffsetText() {
      return OFFSET_TEXT;
   }

   public void update(Timer var1) {
      super.update(var1);
      this.l.update(var1);
   }

   public float[] getFilled() {
      SimpleTransformableSendableObject var1;
      if ((var1 = ((GameClientState)this.getState()).getCurrentPlayerObject()) != null && var1 instanceof SegmentController && var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof ShieldContainerInterface && ((ManagedSegmentController)var1).getManagerContainer().isUsingPowerReactors()) {
         ShieldLocalAddOn var4;
         if (!(var4 = ((ShieldContainerInterface)((ManagedSegmentController)var1).getManagerContainer()).getShieldAddOn().getShieldLocalAddOn()).isAtLeastOneActive()) {
            return new float[]{0.0F};
         } else {
            List var5;
            float[] var2 = new float[(var5 = var4.getActiveShields()).size()];

            for(int var3 = 0; var3 < var5.size(); ++var3) {
               var2[var3] = ((ShieldLocal)var5.get(var3)).getPercentOne();
            }

            return var2;
         }
      } else {
         return new float[]{0.0F};
      }
   }

   public String getText(int var1) {
      SimpleTransformableSendableObject var2;
      if ((var2 = ((GameClientState)this.getState()).getCurrentPlayerObject()) != null && var2 instanceof SegmentController && var2 instanceof ManagedSegmentController && ((ManagedSegmentController)var2).getManagerContainer() instanceof ShieldContainerInterface && ((ManagedSegmentController)var2).getManagerContainer().isUsingPowerReactors()) {
         List var4 = ((ShieldContainerInterface)((ManagedSegmentController)var2).getManagerContainer()).getShieldAddOn().getShieldLocalAddOn().getActiveShields();
         if (var1 < var4.size()) {
            ShieldLocal var3 = (ShieldLocal)var4.get(var1);
            String var5 = "[" + var3.getPosString() + "]";
            if ((double)var3.getPercentOne() < 0.99D) {
               var5 = var5 + StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_SHIELDBARRIGHTLOCAL_1, StringTools.formatPointZero(var3.getRechargeRateIncludingPrevent()));
            }

            if (var3.getRechargePrevented() < 0.98D) {
               var5 = var5 + StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_SHIELDBARRIGHTLOCAL_4, StringTools.formatPointZero((1.0D - var3.getRechargePrevented()) * 100.0D));
            }

            return var5;
         } else {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_SHIELDBARRIGHTLOCAL_2;
         }
      } else {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_SHIELDBARRIGHTLOCAL_3;
      }
   }

   public Vector4i getConfigColor() {
      return COLOR;
   }

   public GUIPosition getConfigPosition() {
      return POSITION;
   }

   public Vector2f getConfigOffset() {
      return OFFSET;
   }

   protected String getTag() {
      return "ShieldBarLeft";
   }
}
