package org.schema.game.client.view.gui.shiphud.newhud;

import javax.vecmath.Vector2f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.input.InputState;

public class ReactorPowerBar extends FillableBarOne {
   @ConfigurationElement(
      name = "Color"
   )
   public static Vector4i COLOR;
   @ConfigurationElement(
      name = "Position"
   )
   public static GUIPosition POSITION;
   @ConfigurationElement(
      name = "Offset"
   )
   public static Vector2f OFFSET;
   @ConfigurationElement(
      name = "FlipX"
   )
   public static boolean FLIPX;
   @ConfigurationElement(
      name = "FlipY"
   )
   public static boolean FLIPY;
   @ConfigurationElement(
      name = "FillStatusTextOnTop"
   )
   public static boolean FILL_ON_TOP;
   @ConfigurationElement(
      name = "OffsetText"
   )
   public static Vector2f OFFSET_TEXT;

   public ReactorPowerBar(InputState var1) {
      super(var1);
      this.drawExtraText = true;
   }

   protected String getDisplayTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_REACTORPOWERBAR_0;
   }

   public boolean isBarFlippedX() {
      return FLIPX;
   }

   public boolean isBarFlippedY() {
      return FLIPY;
   }

   public boolean isFillStatusTextOnTop() {
      return FILL_ON_TOP;
   }

   public Vector2f getOffsetText() {
      return OFFSET_TEXT;
   }

   public float getFilledOne() {
      SimpleTransformableSendableObject var1;
      if ((var1 = ((GameClientState)this.getState()).getCurrentPlayerObject()) != null && var1 instanceof SegmentController && var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof PowerManagerInterface) {
         ((ManagedSegmentController)var1).getManagerContainer();
         return (float)this.getPI().getPowerAsPercent();
      } else {
         return 0.0F;
      }
   }

   public String getText(int var1) {
      SimpleTransformableSendableObject var2;
      if ((var2 = ((GameClientState)this.getState()).getCurrentPlayerObject()) != null && var2 instanceof SegmentController && var2 instanceof ManagedSegmentController && ((ManagedSegmentController)var2).getManagerContainer() instanceof PowerManagerInterface) {
         ((ManagedSegmentController)var2).getManagerContainer();
         return this.getPI().getCurrentConsumption() > 0.0D ? "[" + StringTools.formatPointZero(this.getPI().getPower()) + " / " + StringTools.formatPointZero(this.getPI().getMaxPower()) + "]; - " + StringTools.formatPointZero(this.getPI().getCurrentConsumptionPerSec()) + "/sec; + " + StringTools.formatPointZero(this.getPI().getRechargeRatePowerPerSec()) + "/sec]" : "[" + StringTools.formatPointZero(this.getPI().getPower()) + " / " + StringTools.formatPointZero(this.getPI().getMaxPower()) + "]; + " + StringTools.formatPointZero(this.getPI().getRechargeRatePowerPerSec()) + "/sec]";
      } else {
         return "";
      }
   }

   public PowerInterface getPI() {
      ManagerContainer var1;
      return (var1 = ((ManagedSegmentController)((GameClientState)this.getState()).getCurrentPlayerObject()).getManagerContainer()).getSegmentController().railController.isDockedAndExecuted() && var1.getSegmentController().railController.getRoot() instanceof ManagedSegmentController ? ((ManagedSegmentController)var1.getSegmentController().railController.getRoot()).getManagerContainer().getPowerInterface() : var1.getPowerInterface();
   }

   public Vector4i getConfigColor() {
      return COLOR;
   }

   public GUIPosition getConfigPosition() {
      return POSITION;
   }

   public Vector2f getConfigOffset() {
      return OFFSET;
   }

   protected String getTag() {
      return "PowerBar";
   }
}
