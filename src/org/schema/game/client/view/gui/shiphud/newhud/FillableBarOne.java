package org.schema.game.client.view.gui.shiphud.newhud;

import org.schema.schine.input.InputState;

public abstract class FillableBarOne extends FillableBar {
   public FillableBarOne(InputState var1) {
      super(var1);
   }

   public float[] getFilled() {
      return new float[]{this.getFilledOne()};
   }

   public abstract float getFilledOne();
}
