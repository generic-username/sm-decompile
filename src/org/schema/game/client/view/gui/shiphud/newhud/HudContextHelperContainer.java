package org.schema.game.client.view.gui.shiphud.newhud;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.game.client.view.effects.TimedIndication;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHelperIcon;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHelperIconManager;
import org.schema.schine.input.InputState;
import org.schema.schine.input.InputType;

public class HudContextHelperContainer {
   InputType type;
   int key;
   Object text;
   TimedIndication indication;
   HudContextHelperContainer.Hos position;
   ContextFilter filter;
   GUIHelperIcon icon;
   SegmentPiece p;

   public HudContextHelperContainer(InputType var1, int var2, Object var3, HudContextHelperContainer.Hos var4, ContextFilter var5) {
      this.set(var1, var2, var3, var4, var5);
   }

   public HudContextHelperContainer() {
   }

   public void set(InputType var1, int var2, Object var3, HudContextHelperContainer.Hos var4, ContextFilter var5) {
      this.type = var1;
      this.key = var2;
      this.text = var3;
      this.position = var4;
      this.filter = var5;
   }

   public void create(InputState var1) {
      this.icon = GUIHelperIconManager.get(var1, this.type, this.key);
      this.icon.setTextAfter(this.text);
   }

   public int hashCode() {
      return (((31 + this.key) * 31 + (this.position == null ? 0 : this.position.hashCode())) * 31 + (this.text == null ? 0 : this.text.hashCode())) * 31 + (this.type == null ? 0 : this.type.hashCode());
   }

   public boolean equals(Object var1) {
      if (this == var1) {
         return true;
      } else if (var1 == null) {
         return false;
      } else if (!(var1 instanceof HudContextHelperContainer)) {
         return false;
      } else {
         HudContextHelperContainer var2 = (HudContextHelperContainer)var1;
         if (this.key != var2.key) {
            return false;
         } else if (this.position != var2.position) {
            return false;
         } else {
            if (this.text == null) {
               if (var2.text != null) {
                  return false;
               }
            } else if (!this.text.equals(var2.text)) {
               return false;
            }

            return this.type == var2.type;
         }
      }
   }

   public void drawOnBlock() {
      assert this.p != null;

      this.indication = new TimedIndication(new Transform(), "", 0.1F, 100.0F);
      this.indication.setText(this.text);
      this.p.getTransform(this.indication.getCurrentTransform());
      Vector3f var10000 = this.indication.getCurrentTransform().origin;
      var10000.y += (float)(this.key << 4);
      HudIndicatorOverlay.toDrawTexts.add(this.indication);
   }

   public static enum Hos {
      MOUSE,
      LEFT,
      BLOCK;
   }
}
