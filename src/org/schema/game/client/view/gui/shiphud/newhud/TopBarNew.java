package org.schema.game.client.view.gui.shiphud.newhud;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.PlayerPanel;
import org.schema.game.client.view.gui.TopBarInterface;
import org.schema.game.common.controller.elements.effectblock.EffectElementManager;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyboardMappings;

public class TopBarNew extends HudConfig implements TopBarInterface {
   @ConfigurationElement(
      name = "Color"
   )
   public static Vector4i COLOR;
   @ConfigurationElement(
      name = "Position"
   )
   public static GUIPosition POSITION;
   @ConfigurationElement(
      name = "Offset"
   )
   public static Vector2f OFFSET;
   @ConfigurationElement(
      name = "ShopX"
   )
   public static float SHOP_X;
   @ConfigurationElement(
      name = "MailX"
   )
   public static float MAIL_X;
   @ConfigurationElement(
      name = "TradeX"
   )
   public static float TRADE_X;
   private GUIOverlay iconShop;
   private GUIOverlay iconMail;
   private GUIOverlay iconTrading;
   private BuffDebuff buffDebuff;
   private PlayerPanel panel;

   public TopBarNew(InputState var1, PlayerPanel var2) {
      super(var1);
      this.width = 500.0F;
      this.height = 20.0F;
      this.panel = var2;
   }

   public Vector4i getConfigColor() {
      return COLOR;
   }

   public GUIPosition getConfigPosition() {
      return POSITION;
   }

   public Vector2f getConfigOffset() {
      return OFFSET;
   }

   public void updateOrientation() {
      super.updateOrientation();
      if (this.panel.isPanelActive()) {
         Vector3f var10000 = this.getPos();
         var10000.y += 24.0F;
      }

   }

   protected String getTag() {
      return "TopBar";
   }

   public void update(Timer var1) {
      this.updateOrientation();
      this.buffDebuff.update(var1);
   }

   public void onInit() {
      super.onInit();
      this.buffDebuff = new BuffDebuff(this.getState()) {
         public void draw() {
            if (((GameClientState)this.getState()).getShip() != null) {
               super.draw();
            }

         }
      };
      this.iconShop = new GUIOverlay(Controller.getResLoader().getSprite("HUD_Sprites-8x8-gui-"), this.getState()) {
         public void draw() {
            if (!((GameClientState)this.getState()).isInShopDistance()) {
               this.getSprite().getTint().set(0.5F, 0.5F, 0.5F, 1.0F);
            } else {
               this.getSprite().getTint().set(1.0F, 1.0F, 1.0F, 1.0F);
            }

            super.draw();
         }

         public void onInit() {
            super.onInit();
            this.getSprite().setTint(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
         }
      };
      this.iconMail = new GUIOverlay(Controller.getResLoader().getSprite("HUD_Sprites-8x8-gui-"), this.getState()) {
         public void onInit() {
            super.onInit();
            this.getSprite().setTint(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
         }

         public void draw() {
            if (!((GameClientState)this.getState()).getController().getClientChannel().getPlayerMessageController().hasUnreadMessages()) {
               this.getSprite().getTint().set(0.5F, 0.5F, 0.5F, 1.0F);
            } else {
               this.getSprite().getTint().set(1.0F, 1.0F, 1.0F, 1.0F);
            }

            super.draw();
         }
      };
      this.iconTrading = new GUIOverlay(Controller.getResLoader().getSprite("HUD_Sprites-8x8-gui-"), this.getState()) {
         public void onInit() {
            super.onInit();
            this.getSprite().setTint(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
         }

         public void draw() {
            this.getSprite().getTint().set(0.5F, 0.5F, 0.5F, 1.0F);
            super.draw();
         }
      };
      GUITextOverlay var1 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium15(), this.getState()) {
         public void draw() {
            if (((GameClientState)this.getState()).isInShopDistance()) {
               super.draw();
            }

         }
      };
      GUITextOverlay var2 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium15(), this.getState()) {
         public void draw() {
            if (EngineSettings.TUTORIAL_NEW.isOn()) {
               super.draw();
            }

         }
      };
      GUITextOverlay var3 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium15(), this.getState()) {
         public void draw() {
            if (((GameClientState)this.getState()).getController().getClientChannel().getPlayerMessageController().hasUnreadMessages()) {
               super.draw();
            }

         }
      };
      GUITextOverlay var4 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium15(), this.getState()) {
         public void draw() {
         }
      };
      var1.setTextSimple(new Object() {
         public String toString() {
            return "'" + KeyboardMappings.SHOP_PANEL.getKeyChar() + "'";
         }
      });
      var2.setTextSimple(new Object() {
         public String toString() {
            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_TOPBARNEW_0, KeyboardMappings.TUTORIAL.getKeyChar());
         }
      });
      var3.setTextSimple(new Object() {
         public String toString() {
            return "'F4'";
         }
      });
      var4.setTextSimple(new Object() {
         public String toString() {
            return "";
         }
      });
      var1.getPos().x = 9.0F;
      var1.getPos().y = 32.0F;
      var3.getPos().x = 3.0F;
      var3.getPos().y = 32.0F;
      var4.getPos().x = 3.0F;
      var4.getPos().y = 32.0F;
      var2.setPos(-94.0F, 4.0F, 0.0F);
      this.iconShop.attach(var1);
      this.iconMail.attach(var3);
      this.iconTrading.attach(var4);
      this.iconShop.attach(var2);
      this.iconShop.onInit();
      this.iconMail.onInit();
      this.buffDebuff.onInit();
      this.iconTrading.onInit();
      this.iconShop.setSpriteSubIndex(40);
      this.iconMail.setSpriteSubIndex(41);
      this.iconTrading.setSpriteSubIndex(42);
      this.iconShop.getPos().x = SHOP_X;
      this.iconMail.getPos().x = MAIL_X;
      this.iconTrading.getPos().x = TRADE_X;
      this.attach(this.iconShop);
      this.attach(this.iconMail);
      this.attach(this.iconTrading);
      this.attach(this.buffDebuff);
      this.orientate(this.getConfigPosition().value);
      Vector3f var10000 = this.getPos();
      var10000.x += this.getConfigOffset().x;
      var10000 = this.getPos();
      var10000.y += this.getConfigOffset().y;
   }

   public void updateCreditsAndSpeed() {
      this.orientate(this.getConfigPosition().value);
      Vector3f var10000 = this.getPos();
      var10000.x += this.getConfigOffset().x;
      var10000 = this.getPos();
      var10000.y += this.getConfigOffset().y;
   }

   public void notifyEffectHit(SimpleTransformableSendableObject var1, EffectElementManager.OffensiveEffects var2) {
      this.buffDebuff.notifyEffectHit(var1, var2);
   }
}
