package org.schema.game.client.view.gui.shiphud;

import com.bulletphysics.dynamics.RigidBody;
import java.util.ArrayList;
import javax.vecmath.Vector3f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.TopBarInterface;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.effectblock.EffectElementManager;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class TopBar extends GUIElement implements TopBarInterface {
   Vector3i posFromSector = new Vector3i();
   private GUITextOverlay playerCredits;
   private GUITextOverlay playerShipInfo;
   private GUITextOverlay playerShipSpeed;
   private GUIOverlay topBar;
   private GUITextOverlay mail;

   public TopBar(InputState var1) {
      super(var1);
      this.topBar = new GUIOverlay(Controller.getResLoader().getSprite("top-bar-gui-"), var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.needsReOrientation()) {
         this.doOrientation();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.topBar.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.mail = new GUITextOverlay(300, 40, FontLibrary.getBoldArial12Yellow(), this.getState()) {
         public void draw() {
            if (((GameClientState)this.getState()).getController().getClientChannel().getPlayerMessageController().hasUnreadMessages()) {
               super.draw();
            }

         }
      };
      this.mail.setText(new ArrayList(1));
      this.mail.getText().add("New Mail (F4)");
      this.mail.getPos().x = 240.0F;
      this.mail.getPos().y = 70.0F;
      this.playerShipInfo = new GUITextOverlay(300, 40, FontLibrary.getRegularArial11White(), this.getState());
      this.playerShipInfo.setText(new ArrayList(1));
      this.playerShipInfo.getText().add("");
      this.playerShipInfo.getText().add("");
      this.playerShipInfo.getPos().x = 398.0F;
      this.playerShipInfo.getPos().y = 30.0F;
      this.topBar.attach(this.playerCredits);
      this.topBar.attach(this.playerShipSpeed);
      this.topBar.attach(this.playerShipInfo);
      this.topBar.attach(this.mail);
   }

   protected void doOrientation() {
      this.topBar.orientate(36);
   }

   public float getHeight() {
      return 64.0F;
   }

   public float getWidth() {
      return 768.0F;
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void updateCreditsAndSpeed() {
      GameClientState var1;
      long var2 = (long)(var1 = (GameClientState)this.getState()).getPlayer().getCredits();
      this.playerCredits.getText().set(0, String.valueOf(var2));
      SimpleTransformableSendableObject var8 = var1.getCurrentPlayerObject();
      boolean var3 = false;

      try {
         if (var8 != null) {
            if (var8.getPhysicsDataContainer().isInitialized() && var8.getPhysicsDataContainer().getObject() instanceof RigidBody) {
               RigidBody var4 = (RigidBody)var8.getPhysicsDataContainer().getObject();
               Vector3f var5 = new Vector3f();
               var4.getLinearVelocity(var5);
               var3 = true;
               this.playerShipSpeed.getText().set(0, StringTools.formatPointZero(var5.length()));
            }

            Vector3i var9;
            String var11 = Sector.isTutorialSector(var9 = var1.getCurrentRemoteSector().getNetworkObject().pos.getVector()) ? "Tutorial" : var9.x + ", " + var9.y + ", " + var9.z;
            if (var1.getCurrentClientSystem() == null) {
               this.playerShipInfo.getText().set(1, "Sector: " + var11 + " [Sys: scanning...]");
            } else {
               VoidSystem var6 = var1.getCurrentClientSystem();
               String var10 = Sector.isTutorialSector(var9) ? "Tutorial" : var6.getPos().x + ", " + var6.getPos().y + ", " + var6.getPos().z;
               if (var6.getOwnerFaction() != 0) {
                  if (var1.getFactionManager().existsFaction(var6.getOwnerFaction())) {
                     (new StringBuilder(" <")).append(var1.getFactionManager().getFaction(var6.getOwnerFaction()).getName()).append(">").toString();
                  } else {
                     (new StringBuilder("<unknown(")).append(var6.getOwnerFaction()).append(")>").toString();
                  }
               }

               this.playerShipInfo.getText().set(1, "Sec: " + var11 + "; Sys: " + var10);
            }

            if (var8 instanceof Ship) {
               Ship var12 = (Ship)var8;
               if (((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().isActive()) {
                  this.playerShipInfo.getText().set(0, var1.getPlayerName() + " in " + var12.getRealName());
               } else {
                  this.playerShipInfo.getText().set(0, var12.getRealName());
               }
            } else {
               this.playerShipInfo.getText().set(0, var1.getPlayerName());
            }
         }
      } catch (Exception var7) {
      }

      if (!var3) {
         this.playerShipSpeed.getText().set(0, "0");
      }

   }

   public void notifyEffectHit(SimpleTransformableSendableObject var1, EffectElementManager.OffensiveEffects var2) {
   }
}
