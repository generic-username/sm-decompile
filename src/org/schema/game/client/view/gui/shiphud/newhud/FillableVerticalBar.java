package org.schema.game.client.view.gui.shiphud.newhud;

import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.graphicsengine.util.timer.LinearTimerUtil;
import org.schema.schine.input.InputState;

public abstract class FillableVerticalBar extends HudConfig implements Shaderable {
   protected GUITextOverlay textDesc;
   private float glowIntensity = 0.0F;
   protected Sprite barSprite;
   protected GUITextOverlay text;
   protected LinearTimerUtil u = new LinearTimerUtil();
   protected Vector4f color;
   protected Vector4f colorWarn;

   public FillableVerticalBar(InputState var1) {
      super(var1);
   }

   public abstract double getFilledMargin();

   public abstract boolean isBarFlippedX();

   public abstract boolean isBarFlippedY();

   public abstract boolean isFillStatusTextOnTop();

   public void cleanUp() {
   }

   public void update(Timer var1) {
      if (this.getConfigPosition() != null) {
         this.updateOrientation();
      }

      this.u.update(var1);
   }

   public void draw() {
      GlUtil.glPushMatrix();
      this.transform();
      this.text.getText().set(0, this.getBarName() + " " + StringTools.formatPointZero(this.getFilled() * 100.0F) + "%");
      this.textDesc.getText().set(0, this.getText());
      this.textDesc.setColor(1.0F, 1.0F, 1.0F, 1.0F);
      this.text.setColor(1.0F, 1.0F, 1.0F, 1.0F);
      this.text.setPos((float)(this.getTextOffsetX() - this.text.getMaxLineWidth()), (float)this.getTextOffsetY(), 0.0F);
      this.textDesc.setPos((float)(this.getTextOffsetX() - this.textDesc.getMaxLineWidth()), (float)(this.getTextOffsetY() + 40), 0.0F);
      ShaderLibrary.powerBarShader.setShaderInterface(this);
      ShaderLibrary.powerBarShader.load();
      this.barSprite.draw();
      ShaderLibrary.powerBarShader.unload();
      this.text.draw();
      this.textDesc.draw();
      GlUtil.glPopMatrix();
   }

   protected abstract int getTextOffsetX();

   protected abstract int getTextOffsetY();

   protected abstract String getBarName();

   public void onInit() {
      this.barSprite = Controller.getResLoader().getSprite("HUD_VerticalBar_Long-4x1-gui-");
      this.barSprite.setSelectedMultiSprite(0);
      this.width = (float)this.barSprite.getWidth();
      this.height = (float)this.barSprite.getHeight();
      this.text = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium14(), this.getState());
      this.textDesc = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium14(), this.getState());
      this.text.setTextSimple("n/a");
      this.textDesc.setTextSimple("n/a");
      this.color = new Vector4f((float)this.getConfigColor().x / 255.0F, (float)this.getConfigColor().y / 255.0F, (float)this.getConfigColor().z / 255.0F, (float)this.getConfigColor().w / 255.0F);
      this.colorWarn = new Vector4f((float)this.getConfigColorWarn().x / 255.0F, (float)this.getConfigColorWarn().y / 255.0F, (float)this.getConfigColorWarn().z / 255.0F, (float)this.getConfigColorWarn().w / 255.0F);
   }

   public abstract Vector4i getConfigColorWarn();

   protected boolean isLongerBar() {
      return false;
   }

   public void onExit() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.glActiveTexture(33984);
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.glBindTexture(3553, this.barSprite.getMaterial().getTexture().getTextureId());
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderInt(var1, "barTex", 0);
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      float var2 = this.getFilled();
      GlUtil.updateShaderFloat(var1, "filled", var2);
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderFloat(var1, "glowIntensity", this.glowIntensity);
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderBoolean(var1, "flippedX", this.isBarFlippedX());
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderBoolean(var1, "flippedY", this.isBarFlippedY());
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderFloat(var1, "minTexCoord", 0.005F);
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderFloat(var1, "maxTexCoord", 0.995F);
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderVector4f(var1, "barColor", this.getColor(var2));
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      var1.recompiled = false;
   }

   protected abstract Vector4f getColor(float var1);

   public abstract float getFilled();

   public abstract String getText();

   public float getGlowIntensity() {
      return this.glowIntensity;
   }

   public void setGlowIntensity(float var1) {
      this.glowIntensity = var1;
   }
}
