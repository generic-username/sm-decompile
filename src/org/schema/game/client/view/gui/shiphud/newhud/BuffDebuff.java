package org.schema.game.client.view.gui.shiphud.newhud;

import it.unimi.dsi.fastutil.ints.Int2FloatOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Map.Entry;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.effectblock.EffectElementManager;
import org.schema.game.common.data.blockeffects.BlockEffectTypes;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.input.InputState;

public class BuffDebuff extends HudConfig {
   @ConfigurationElement(
      name = "BlendOutInSec"
   )
   public static float blendOutTime;
   @ConfigurationElement(
      name = "StayTime"
   )
   public static float stayTime;
   @ConfigurationElement(
      name = "EMPIndex"
   )
   public static HitIconIndex EMP_INDEX;
   @ConfigurationElement(
      name = "ExplosiveIndex"
   )
   public static HitIconIndex EXPLOSIVE_INDEX;
   @ConfigurationElement(
      name = "IonIndex"
   )
   public static HitIconIndex ION_INDEX;
   @ConfigurationElement(
      name = "OverdriveIndex"
   )
   public static HitIconIndex OVERDRIVE_INDEX;
   @ConfigurationElement(
      name = "PiercingIndex"
   )
   public static HitIconIndex PIERCING_INDEX;
   @ConfigurationElement(
      name = "PunchthroughIndex"
   )
   public static HitIconIndex PUNCHTHROUGH_INDEX;
   @ConfigurationElement(
      name = "RepairIndex"
   )
   public static HitIconIndex REPAIR_INDEX;
   @ConfigurationElement(
      name = "ShieldSupplyIndex"
   )
   public static HitIconIndex SHIELD_SUPPLY_INDEX;
   @ConfigurationElement(
      name = "ShieldDrainIndex"
   )
   public static HitIconIndex SHIELD_DRAIN_INDEX;
   @ConfigurationElement(
      name = "PowerSupplyIndex"
   )
   public static HitIconIndex POWER_SUPPLY_INDEX;
   @ConfigurationElement(
      name = "PowerDrainIndex"
   )
   public static HitIconIndex POWER_DRAIN_INDEX;
   @ConfigurationElement(
      name = "NoThrust"
   )
   public static HitIconIndex NO_THRUST;
   @ConfigurationElement(
      name = "NoPower"
   )
   public static HitIconIndex NO_POWER;
   @ConfigurationElement(
      name = "ShieldsDown"
   )
   public static HitIconIndex SHIELD_DOWN;
   @ConfigurationElement(
      name = "ThrusterOutage"
   )
   public static HitIconIndex THRUSTER_OUTAGE;
   @ConfigurationElement(
      name = "NoPowerRecharge"
   )
   public static HitIconIndex NO_POWER_RECHARGE;
   @ConfigurationElement(
      name = "NoShieldRecharge"
   )
   public static HitIconIndex NO_SHIELD_RECHARGE;
   @ConfigurationElement(
      name = "ThrusterOutageEff"
   )
   public static EffectIconIndex THRUSTER_OUTAGE_EFF;
   @ConfigurationElement(
      name = "NoPowerRechargeEff"
   )
   public static EffectIconIndex NO_POWER_RECHARGE_EFF;
   @ConfigurationElement(
      name = "NoShieldRechargeEff"
   )
   public static EffectIconIndex NO_SHIELD_RECHARGE_EFF;
   @ConfigurationElement(
      name = "Controlless"
   )
   public static EffectIconIndex CONTROLLESS;
   @ConfigurationElement(
      name = "Push"
   )
   public static EffectIconIndex PUSH;
   @ConfigurationElement(
      name = "Pull"
   )
   public static EffectIconIndex PULL;
   @ConfigurationElement(
      name = "Stop"
   )
   public static EffectIconIndex STOP;
   @ConfigurationElement(
      name = "StatusArmorHarden"
   )
   public static EffectIconIndex STATUS_ARMOR_HARDEN;
   @ConfigurationElement(
      name = "StatusPiercingProtection"
   )
   public static EffectIconIndex STATUS_PIERCING_PROTECTION;
   @ConfigurationElement(
      name = "StatusArmorHpAbsorptionBonus"
   )
   public static EffectIconIndex STATUS_ARMOR_HP_ABSORPTION_BONUS;
   @ConfigurationElement(
      name = "StatusArmorHpDeductionBonus"
   )
   public static EffectIconIndex STATUS_ARMOR_HP_DEDUCTION_BONUS;
   @ConfigurationElement(
      name = "StatusPowerShield"
   )
   public static EffectIconIndex STATUS_POWER_SHIELD;
   @ConfigurationElement(
      name = "StatusShieldHarden"
   )
   public static EffectIconIndex STATUS_SHIELD_HARDEN;
   @ConfigurationElement(
      name = "StatusTopSpeed"
   )
   public static EffectIconIndex STATUS_TOP_SPEED;
   @ConfigurationElement(
      name = "StatusAntiGravity"
   )
   public static EffectIconIndex STATUS_ANTI_GRAVITY;
   @ConfigurationElement(
      name = "StatusGravityEffectIgnorance"
   )
   public static EffectIconIndex STATUS_GRAVITY_EFFECT_IGNORANCE;
   @ConfigurationElement(
      name = "TakeOff"
   )
   public static EffectIconIndex TAKE_OFF;
   @ConfigurationElement(
      name = "Evade"
   )
   public static EffectIconIndex EVADE;
   @ConfigurationElement(
      name = "Position"
   )
   public static GUIPosition POSITION;
   @ConfigurationElement(
      name = "Offset"
   )
   public static Vector2f OFFSET;
   public static Object2ObjectOpenHashMap map = new Object2ObjectOpenHashMap();
   public static Object2ObjectOpenHashMap mapOffensive = new Object2ObjectOpenHashMap();
   public Int2FloatOpenHashMap activeIcons = new Int2FloatOpenHashMap();
   private GUIOverlay icons;

   public BuffDebuff(InputState var1) {
      super(var1);
   }

   public Vector4i getConfigColor() {
      return null;
   }

   public GUIPosition getConfigPosition() {
      return POSITION;
   }

   public Vector2f getConfigOffset() {
      return OFFSET;
   }

   protected String getTag() {
      return "BuffDebuff";
   }

   public void draw() {
      GlUtil.glPushMatrix();
      this.transform();
      int var1 = 0;
      int var2 = -1;
      Ship var10000 = ((GameClientState)this.getState()).getShip();
      ObjectIterator var3 = null;
      if (var10000 != null) {
         for(var3 = this.activeIcons.entrySet().iterator(); var3.hasNext(); this.icons.draw()) {
            Entry var4 = (Entry)var3.next();
            this.icons.setSpriteSubIndex(Math.abs((Integer)var4.getKey()));
            if ((Integer)var4.getKey() >= 0) {
               this.icons.getSprite().getTint().set(ColorPalette.buff);
               this.icons.getPos().x = (float)(var1 << 5);
               ++var1;
            } else {
               this.icons.getSprite().getTint().set(ColorPalette.debuff);
               this.icons.getPos().x = (float)((var2 << 5) - 10);
               --var2;
            }

            if ((Float)var4.getValue() > stayTime) {
               this.icons.getSprite().getTint().w = 1.0F - ((Float)var4.getValue() - stayTime) / blendOutTime;
            }
         }
      }

      Iterator var5 = this.getChilds().iterator();

      while(var5.hasNext()) {
         ((AbstractSceneNode)var5.next()).draw();
      }

      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.icons = new GUIOverlay(Controller.getResLoader().getSprite("HUD_Sprites-8x8-gui-"), this.getState());
      map.clear();
      mapOffensive.clear();
      mapOffensive.put(EffectElementManager.OffensiveEffects.EMP, EMP_INDEX);
      mapOffensive.put(EffectElementManager.OffensiveEffects.EXPLOSIVE, EXPLOSIVE_INDEX);
      mapOffensive.put(EffectElementManager.OffensiveEffects.ION, ION_INDEX);
      mapOffensive.put(EffectElementManager.OffensiveEffects.OVERDRIVE, OVERDRIVE_INDEX);
      mapOffensive.put(EffectElementManager.OffensiveEffects.PIERCING, PIERCING_INDEX);
      mapOffensive.put(EffectElementManager.OffensiveEffects.PUNCHTHROUGH, PUNCHTHROUGH_INDEX);
      mapOffensive.put(EffectElementManager.OffensiveEffects.THRUSTER_OUTAGE, THRUSTER_OUTAGE);
      mapOffensive.put(EffectElementManager.OffensiveEffects.NO_POWER_RECHARGE, NO_POWER_RECHARGE);
      mapOffensive.put(EffectElementManager.OffensiveEffects.NO_SHIELD_RECHARGE, NO_SHIELD_RECHARGE);
      Field[] var1;
      int var2 = (var1 = this.getClass().getFields()).length;

      int var3;
      for(var3 = 0; var3 < var2; ++var3) {
         Field var4;
         if ((var4 = var1[var3]).getType().equals(HitIconIndex.class)) {
            try {
               HitIconIndex var5 = (HitIconIndex)var4.get(this);

               assert var5.type != null;

               mapOffensive.put(var5.type, var5);
            } catch (Exception var6) {
               var6.printStackTrace();
               throw new RuntimeException(var6);
            }
         }

         if (var4.getType().equals(EffectIconIndex.class)) {
            try {
               EffectIconIndex var10 = (EffectIconIndex)var4.get(this);

               assert var10.type != null;

               map.put(var10.type, var10);
            } catch (Exception var7) {
               var7.printStackTrace();
               throw new RuntimeException(var7);
            }
         }
      }

      BlockEffectTypes[] var8;
      var2 = (var8 = BlockEffectTypes.values()).length;

      for(var3 = 0; var3 < var2; ++var3) {
         BlockEffectTypes var9 = var8[var3];

         assert var9 == BlockEffectTypes.NULL_EFFECT || map.containsKey(var9) : var9.name() + "; " + map;
      }

      this.icons.getSprite().setTint(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
      super.onInit();
   }

   public void update(Timer var1) {
      Ship var2;
      if ((var2 = ((GameClientState)this.getState()).getShip()) != null) {
         Iterator var5 = var2.getBlockEffectManager().getActiveEffectTypes().iterator();

         while(var5.hasNext()) {
            BlockEffectTypes var3 = (BlockEffectTypes)var5.next();
            EffectIconIndex var4 = (EffectIconIndex)map.get(var3);

            assert var4 != null : var3.name();

            this.activeIcons.put(var4.getIndex() * (var4.isBuff() ? 1 : -1), 0.0F);
         }

         var5 = this.activeIcons.keySet().iterator();

         while(var5.hasNext()) {
            int var7 = (Integer)var5.next();
            this.activeIcons.put(var7, this.activeIcons.get(var7) + var1.getDelta());
         }

         ObjectIterator var6 = this.activeIcons.int2FloatEntrySet().fastIterator();

         while(var6.hasNext()) {
            if (((it.unimi.dsi.fastutil.ints.Int2FloatMap.Entry)var6.next()).getFloatValue() >= blendOutTime + stayTime) {
               var6.remove();
            }
         }
      }

   }

   public void notifyEffectHit(SimpleTransformableSendableObject var1, EffectElementManager.OffensiveEffects var2) {
      HitIconIndex var3;
      if ((var3 = (HitIconIndex)mapOffensive.get(var2)) != null) {
         assert var3 != null : var2.name() + "::: " + mapOffensive;

         this.activeIcons.put(var3.getIndex() * (var3.isBuff() ? 1 : -1), 0.0F);
      } else {
         EffectIconIndex var4 = (EffectIconIndex)map.get(var2.getEffect());
         this.activeIcons.put(var4.getIndex() * (var4.isBuff() ? 1 : -1), 0.0F);
      }
   }
}
