package org.schema.game.client.view.gui.shiphud.newhud;

import javax.vecmath.Vector2f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.common.controller.FloatingRock;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.input.InputState;

public class IndicatorIndices extends HudConfig {
   @ConfigurationElement(
      name = "Lifeform"
   )
   public static int lifeform;
   @ConfigurationElement(
      name = "Asteroid"
   )
   public static int asteroid;
   @ConfigurationElement(
      name = "Other"
   )
   public static int other;
   @ConfigurationElement(
      name = "Shop"
   )
   public static int shop;
   @ConfigurationElement(
      name = "Planet"
   )
   public static int planet;
   @ConfigurationElement(
      name = "Ship"
   )
   public static int ship;
   @ConfigurationElement(
      name = "Station"
   )
   public static int station;

   public IndicatorIndices(InputState var1) {
      super(var1);
   }

   public static int getCIndex(SimpleTransformableSendableObject var0) {
      if (var0 instanceof AbstractCharacter) {
         return lifeform;
      } else if (var0 instanceof Ship) {
         return ship;
      } else if (var0 instanceof SpaceStation) {
         return station;
      } else if (var0 instanceof FloatingRock) {
         return asteroid;
      } else if (var0 instanceof Planet) {
         return planet;
      } else {
         return var0 instanceof ShopSpaceStation ? shop : other;
      }
   }

   public Vector4i getConfigColor() {
      return null;
   }

   public GUIPosition getConfigPosition() {
      return null;
   }

   public Vector2f getConfigOffset() {
      return null;
   }

   protected String getTag() {
      return "IndicatorIndices";
   }
}
