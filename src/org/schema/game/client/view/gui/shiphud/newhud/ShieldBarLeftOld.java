package org.schema.game.client.view.gui.shiphud.newhud;

import javax.vecmath.Vector2f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.input.InputState;

public class ShieldBarLeftOld extends FillableBarOne {
   @ConfigurationElement(
      name = "Color"
   )
   public static Vector4i COLOR;
   @ConfigurationElement(
      name = "Position"
   )
   public static GUIPosition POSITION;
   @ConfigurationElement(
      name = "Offset"
   )
   public static Vector2f OFFSET;
   @ConfigurationElement(
      name = "FlipX"
   )
   public static boolean FLIPX;
   @ConfigurationElement(
      name = "FlipY"
   )
   public static boolean FLIPY;
   @ConfigurationElement(
      name = "FillStatusTextOnTop"
   )
   public static boolean FILL_ON_TOP;
   @ConfigurationElement(
      name = "OffsetText"
   )
   public static Vector2f OFFSET_TEXT;

   public ShieldBarLeftOld(InputState var1) {
      super(var1);
      this.drawExtraText = true;
   }

   protected String getDisplayTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_SHIELDBARLEFTOLD_0;
   }

   public boolean isBarFlippedX() {
      return FLIPX;
   }

   public boolean isBarFlippedY() {
      return FLIPY;
   }

   public boolean isFillStatusTextOnTop() {
      return FILL_ON_TOP;
   }

   public Vector2f getOffsetText() {
      return OFFSET_TEXT;
   }

   public float getFilledOne() {
      SimpleTransformableSendableObject var1;
      return (var1 = ((GameClientState)this.getState()).getCurrentPlayerObject()) != null && var1 instanceof SegmentController && var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof ShieldContainerInterface ? ((ShieldContainerInterface)((ManagedSegmentController)var1).getManagerContainer()).getShieldAddOn().getPercentOne() : 0.0F;
   }

   public String getText(int var1) {
      SimpleTransformableSendableObject var3;
      if ((var3 = ((GameClientState)this.getState()).getCurrentPlayerObject()) != null && var3 instanceof SegmentController && var3 instanceof ManagedSegmentController && ((ManagedSegmentController)var3).getManagerContainer() instanceof ShieldContainerInterface) {
         ShieldContainerInterface var4;
         if ((var4 = (ShieldContainerInterface)((ManagedSegmentController)var3).getManagerContainer()).getShieldAddOn().getShields() < var4.getShieldAddOn().getShieldCapacity() && var4.getShieldAddOn().getShieldRechargeRate() > 0.0D) {
            String var2 = "+ " + StringTools.formatPointZero(var4.getShieldAddOn().getShieldRechargeRate());
            if (var4.getShieldAddOn().getRecovery() > 0.0D) {
               var2 = var2 + " ((!) Under Fire Rate: " + Math.ceil(var4.getShieldAddOn().getNerf() * 100.0D) + "%)";
            }

            return var2;
         } else {
            return "";
         }
      } else {
         return "";
      }
   }

   public Vector4i getConfigColor() {
      return COLOR;
   }

   public GUIPosition getConfigPosition() {
      return POSITION;
   }

   public Vector2f getConfigOffset() {
      return OFFSET;
   }

   protected String getTag() {
      return "ShieldBarRight";
   }
}
