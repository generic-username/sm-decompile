package org.schema.game.client.view.gui.shiphud.newhud;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.input.InputState;

public class SpeedBarFarRight extends FillableBarOne {
   @ConfigurationElement(
      name = "Color"
   )
   public static Vector4i COLOR;
   @ConfigurationElement(
      name = "Position"
   )
   public static GUIPosition POSITION;
   @ConfigurationElement(
      name = "Offset"
   )
   public static Vector2f OFFSET;
   @ConfigurationElement(
      name = "FlipX"
   )
   public static boolean FLIPX;
   @ConfigurationElement(
      name = "FlipY"
   )
   public static boolean FLIPY;
   @ConfigurationElement(
      name = "FillStatusTextOnTop"
   )
   public static boolean FILL_ON_TOP;
   @ConfigurationElement(
      name = "OffsetText"
   )
   public static Vector2f OFFSET_TEXT;
   private Vector3f tmp = new Vector3f();

   public SpeedBarFarRight(InputState var1) {
      super(var1);
      this.drawPercent = false;
   }

   protected String getDisplayTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_SPEEDBARFARRIGHT_0;
   }

   public boolean isBarFlippedX() {
      return FLIPX;
   }

   public boolean isBarFlippedY() {
      return FLIPY;
   }

   public boolean isFillStatusTextOnTop() {
      return FILL_ON_TOP;
   }

   public Vector2f getOffsetText() {
      return OFFSET_TEXT;
   }

   public float getFilledOne() {
      if (((GameClientState)this.getState()).getShip() != null) {
         float var1 = ((GameClientState)this.getState()).getShip().getLinearVelocity(this.tmp).length();
         float var2 = ((GameClientState)this.getState()).getShip().getCurrentMaxVelocity();
         return var1 / var2;
      } else {
         return 0.0F;
      }
   }

   public String getText(int var1) {
      return ((GameClientState)this.getState()).getShip() != null ? StringTools.formatPointZero(((GameClientState)this.getState()).getShip().getLinearVelocity(this.tmp).length()) + " m/s" : "0 m/s";
   }

   public Vector4i getConfigColor() {
      return COLOR;
   }

   public GUIPosition getConfigPosition() {
      return POSITION;
   }

   public Vector2f getConfigOffset() {
      return OFFSET;
   }

   protected String getTag() {
      return "SpeedBarFarRight";
   }
}
