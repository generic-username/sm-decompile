package org.schema.game.client.view.gui.shiphud;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.input.InputState;

public class GUIPowerBar extends GUIElement implements Shaderable {
   private final Vector4f color = new Vector4f(0.4F, 0.4F, 1.0F, 1.0F);
   private float glowIntensity = 0.0F;
   private Sprite barSprite;
   private float filled;

   public GUIPowerBar(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      GlUtil.glPushMatrix();
      this.transform();
      ShaderLibrary.powerBarShader.setShaderInterface(this);
      ShaderLibrary.powerBarShader.load();
      this.barSprite.draw();
      ShaderLibrary.powerBarShader.unload();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.barSprite = Controller.getResLoader().getSprite("bar-4x1-gui-");
   }

   public float getHeight() {
      return 512.0F;
   }

   public float getWidth() {
      return 128.0F;
   }

   public void onExit() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, this.barSprite.getMaterial().getTexture().getTextureId());
      GlUtil.updateShaderInt(var1, "barTex", 0);
      GlUtil.updateShaderFloat(var1, "filled", this.filled);
      GlUtil.updateShaderFloat(var1, "glowIntensity", this.glowIntensity);
      GlUtil.updateShaderFloat(var1, "minTexCoord", 0.01F);
      GlUtil.updateShaderFloat(var1, "maxTexCoord", 0.99F);
      GlUtil.updateShaderVector4f(var1, "barColor", this.color);
   }

   public Vector4f getColor() {
      return this.color;
   }

   public float getFilled() {
      return this.filled;
   }

   public void setFilled(float var1) {
      this.filled = var1;
   }

   public float getGlowIntensity() {
      return this.glowIntensity;
   }

   public void setGlowIntensity(float var1) {
      this.glowIntensity = var1;
   }
}
