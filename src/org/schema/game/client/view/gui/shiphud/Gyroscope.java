package org.schema.game.client.view.gui.shiphud;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.lwjgl.opengl.GL11;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.Ship;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class Gyroscope extends GUIElement {
   Transform t = new Transform();
   private GameClientState state;
   private Transform inv = new Transform();

   public Gyroscope(InputState var1) {
      super(var1);
      this.state = (GameClientState)var1;
   }

   public void cleanUp() {
   }

   public void draw() {
      Ship var1;
      if ((var1 = this.state.getShip()) != null) {
         Vector3f var2 = new Vector3f(1.0F, 0.0F, 0.0F);
         Vector3f var3 = new Vector3f(0.0F, 1.0F, 0.0F);
         Vector3f var4 = new Vector3f(0.0F, -1.0F, 0.0F);
         Vector3f var5 = GlUtil.getRightVector(new Vector3f(), (Transform)var1.getWorldTransform());
         Vector3f var6 = GlUtil.getUpVector(new Vector3f(), (Transform)var1.getWorldTransform());
         GlUtil.getBottomVector(new Vector3f(), var1.getWorldTransform());
         float var7 = var3.dot(var6);
         var4.dot(var6);
         var7 = (var7 + 1.0F) / 2.0F * this.getHeight();
         var2.dot(var5);
         GL11.glBegin(1);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 0.4F);
         GL11.glVertex2f(0.0F, var7);
         GL11.glVertex2f(this.getWidth(), var7);
         GL11.glEnd();
      }

   }

   public void onInit() {
   }

   public float getHeight() {
      return 128.0F;
   }

   public float getWidth() {
      return 128.0F;
   }

   public boolean isPositionCenter() {
      return false;
   }
}
