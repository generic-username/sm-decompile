package org.schema.game.client.view.gui.shiphud;

import com.bulletphysics.linearmath.Transform;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyboardMappings;

public class RadarOverlay extends GUIElement {
   Vector4f tint = new Vector4f();
   private GameClientState state;
   private float size;
   private GUITextOverlay mapText;
   private Transform t = new Transform();
   private Vector3f c = new Vector3f();

   public RadarOverlay(InputState var1, float var2) {
      super(var1);
      this.state = (GameClientState)var1;
      this.size = var2;
      this.mapText = new GUITextOverlay(100, 20, var1);
      this.mapText.setTextSimple("Open Galaxy Map (" + KeyboardMappings.MAP_PANEL.getKeyChar() + ")");
      this.mapText.setPos(0.0F, 124.0F, 0.0F);
   }

   public void cleanUp() {
   }

   public void draw() {
      SimpleTransformableSendableObject var1;
      if ((var1 = this.state.getCurrentPlayerObject()) != null) {
         if (var1 instanceof SegmentController) {
            this.t.set(((SegmentController)var1).getWorldTransformInverse());
         } else if (var1 instanceof AbstractCharacter) {
            this.t.set(Controller.getCamera().getWorldTransform());
            this.t.inverse();
         } else {
            this.t.set(var1.getWorldTransform());
            this.t.inverse();
         }

         var1 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity();
         GL11.glPointSize(2.0F);
         GL11.glBegin(0);
         Iterator var2 = this.state.getCurrentSectorEntities().values().iterator();

         while(var2.hasNext()) {
            SimpleTransformableSendableObject var3 = (SimpleTransformableSendableObject)var2.next();
            this.c.set(var3.getWorldTransformOnClient().origin);
            this.t.transform(this.c);
            this.c.scale(0.07F);
            if (this.c.length() < this.size / 2.0F) {
               HudIndicatorOverlay.getColor(var3, this.tint, var1 == var3, this.state);
               this.c.x = this.size / 2.0F - this.c.x;
               this.c.z = this.size / 2.0F - this.c.z;
               GlUtil.glColor4f(this.tint.x, this.tint.y, this.tint.z, this.tint.w);
               GL11.glVertex2f(this.c.x, this.c.z);
            }
         }

         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GL11.glEnd();
      }

      this.mapText.draw();
   }

   public void onInit() {
   }

   public float getHeight() {
      return 128.0F;
   }

   public float getWidth() {
      return 128.0F;
   }

   public boolean isPositionCenter() {
      return false;
   }
}
