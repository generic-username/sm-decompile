package org.schema.game.client.view.gui;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.font.effects.OutlineEffect;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class RoundEndPanel extends GUIElement {
   private static UnicodeFont font;
   private GUITextOverlay infoText;
   private GUITextOverlay errorText;
   private GUIOverlay background;
   private long timeError;
   private long timeErrorShowed;
   private String info;
   private boolean firstDraw = true;

   public RoundEndPanel(InputState var1, GUICallback var2, String var3) {
      super(var1);
      this.info = var3;
   }

   public void cleanUp() {
      this.background.cleanUp();
      this.infoText.cleanUp();
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      this.infoText.getText().set(0, this.info);
      GlUtil.glPushMatrix();
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      this.transform();
      if (this.timeError < System.currentTimeMillis() - this.timeErrorShowed) {
         this.errorText.getText().clear();
      }

      this.background.draw();
      GlUtil.glDisable(3042);
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      if (font == null) {
         Font var1 = new Font("Arial", 1, 28);
         (font = new UnicodeFont(var1)).getEffects().add(new OutlineEffect(4, Color.black));
         font.getEffects().add(new ColorEffect(Color.white));
         font.addAsciiGlyphs();

         try {
            font.loadGlyphs();
         } catch (SlickException var2) {
            var2.printStackTrace();
         }
      }

      this.infoText = new GUITextOverlay(256, 64, font, this.getState());
      this.errorText = new GUITextOverlay(256, 64, this.getState());
      this.background = new GUIOverlay(Controller.getResLoader().getSprite("panel-std-gui-"), this.getState());
      ArrayList var3;
      (var3 = new ArrayList()).add(this.info);
      this.infoText.setText(var3);
      var3 = new ArrayList();
      this.errorText.setText(var3);
      this.background.orientate(48);
      this.background.onInit();
      this.infoText.onInit();
      this.attach(this.background);
      this.background.attach(this.infoText);
      this.background.attach(this.errorText);
      this.infoText.setPos(280.0F, 80.0F, 0.0F);
      this.errorText.setPos(300.0F, 30.0F, 0.0F);
      this.firstDraw = false;
   }

   public float getHeight() {
      return 256.0F;
   }

   public float getWidth() {
      return 256.0F;
   }

   public boolean isPositionCenter() {
      return false;
   }

   public String getInfo() {
      return this.info;
   }

   public void setInfo(String var1) {
      this.info = var1;
   }

   public void setErrorMessage(String var1, long var2) {
      this.errorText.getText().add(var1);
      this.timeError = System.currentTimeMillis();
      this.timeErrorShowed = var2;
   }
}
