package org.schema.game.client.view.beam;

public enum BeamColors {
   BLUE,
   GREEN,
   RED,
   WHITE,
   PURPLE,
   YELLOW;
}
