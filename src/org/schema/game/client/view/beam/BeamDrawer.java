package org.schema.game.client.view.beam;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectHeapPriorityQueue;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.beam.AbstractBeamHandler;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.util.timer.LinearTimerUtil;

public class BeamDrawer implements Drawable {
   static final Vector3f tmpPosC = new Vector3f();
   static final Vector3f tmpPosCReg = new Vector3f();
   static final Vector3f tmpPosD = new Vector3f();
   static final Vector3f tmpPosF = new Vector3f();
   static final Vector3f dA = new Vector3f();
   static final Vector3f dB = new Vector3f();
   private static final Vector4f color0 = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   private static final Vector4f color0t = new Vector4f();
   private static final Vector4f color1t = new Vector4f();
   private static final Vector3f right = new Vector3f(0.0F, 0.0F, 1.0F);
   private static final Vector3f beamOrigUp = new Vector3f(0.0F, 0.0F, 1.0F);
   private static final int centerShift = 16;
   public static final Vector3f c = new Vector3f(-16.0F, -16.0F, -16.0F);
   private static final Vector3f a = new Vector3f();
   private static final Vector3f b = new Vector3f();
   private static final Vector3f crossAB = new Vector3f();
   private static final float TEX_COORD_MULT = 17.0F;
   private static Vector3f start = new Vector3f();
   private static Vector3f end = new Vector3f();
   private static Vector3f endReg = new Vector3f();
   private static Vector3f dir = new Vector3f();
   private static Vector3f up = new Vector3f();
   private static float lastDist;
   private static float size0t;
   private static float lastLenDiff;
   private static long timert;
   private static boolean lastHandheld;
   private final LinearTimerUtil linTimer = new LinearTimerUtil();
   private final Transform tmp = new Transform();
   private final Vector3i tmpPos = new Vector3i();
   private BeamDrawerManager manager;
   private List beamHandlers;
   private Vector4f color1;
   private ObjectOpenHashSet toDraw = new ObjectOpenHashSet();
   private boolean drawNeeded;
   private Vector3f p = new Vector3f();
   private Transform tmpTrans = new Transform();
   private int exceptionCount;
   private static Matrix3f rotTmp = new Matrix3f();
   public static boolean resetShader;

   public BeamDrawer(BeamDrawerManager var1, List var2) {
      this.set(var2, var1);
   }

   public static void prepareDraw() {
   }

   public static void drawConnection(BeamState var0, Transform var1, long var2, Vector3f var4) {
      if (var0.hitPoint != null) {
         end.set(var0.hitPoint);
         endReg.set(var0.to);
      } else {
         end.set(var0.to);
         endReg.set(var0.to);
      }

      tmpPosC.sub(end, var0.from);
      tmpPosCReg.sub(endReg, var0.from);
      var0.from.set(var0.relativePos);
      var0.getHandler().transform(var0);
      start.set(var0.from);
      end.add(start, tmpPosC);
      endReg.add(start, tmpPosCReg);
      if (!var0.drawVarsCamPos.epsilonEquals(var4, 0.01F) || !var0.drawVarsStart.epsilonEquals(start, 0.01F) || !var0.drawVarsEnd.epsilonEquals(end, 0.01F)) {
         var0.drawVarsCamPos.set(var4);
         var0.drawVarsStart.set(start);
         var0.drawVarsEnd.set(end);
         dA.sub(end, start);
         dB.sub(endReg, start);
         float var5 = FastMath.carmackLength(dA) / FastMath.carmackLength(dB);
         if (!GlUtil.isLineInView(start, end, Controller.vis.getVisLen())) {
            return;
         }

         dir.sub(start, end);
         float var6 = FastMath.carmackLength(dir);
         right.set(0.0F, 0.0F, 1.0F);
         var1.origin.set(start);
         up.cross(dir, right);
         if (up.lengthSquared() == 0.0F) {
            up.set(0.0F, 1.0F, 0.0F);
         }

         up.normalize();
         GlUtil.setForwardVector(dir, var1);
         GlUtil.setUpVector(up, var1);
         right.cross(dir, up);
         if (right.lengthSquared() == 0.0F) {
            right.set(0.0F, 0.0F, 1.0F);
         }

         FastMath.normalizeCarmack(right);
         GlUtil.setRightVector(right, var1);
         var0.drawVarsAxisAngle = getAngleAroundZAxis(start, end, var4, var1.basis);
         var0.drawVarsDist = var6;
         var0.drawVarsLenDiff = var5;
         var0.drawVarsDrawTransform.set(var1);
         rotTmp.setIdentity();
         rotTmp.rotZ(var0.drawVarsAxisAngle + 1.5707964F);
         var0.drawVarsDrawTransform.basis.mul(rotTmp);
      }

      updateShader(var0.drawVarsDist, var0.drawVarsLenDiff, var0.color, var0.size, var2, var0.handheld);
      GlUtil.glPushMatrix();
      GlUtil.glMultMatrix(var0.drawVarsDrawTransform);
      BeamDrawerManager.mesh.renderVBO();
      GlUtil.glPopMatrix();
   }

   private static float getAngleAroundZAxis(Vector3f var0, Vector3f var1, Vector3f var2, Matrix3f var3) {
      a.sub(var2, var0);
      b.sub(var1, var0);
      crossAB.cross(a, b);
      crossAB.normalize();
      beamOrigUp.set(0.0F, 1.0F, 0.0F);
      var3.transform(beamOrigUp);
      float var4 = (float)FastMath.acosFast((double)beamOrigUp.dot(crossAB));
      beamOrigUp.set(1.0F, 0.0F, 0.0F);
      var3.transform(beamOrigUp);
      float var5 = (float)FastMath.acosFast((double)beamOrigUp.dot(crossAB));
      if (1.5707964F > var5) {
         var4 = 6.2831855F - var4;
      }

      return var4;
   }

   public static void updateShader(float var0, float var1, Vector4f var2, float var3, long var4, boolean var6) {
      if (ShaderLibrary.simpleBeamShader.recompiled || resetShader) {
         color0t.set(0.0F, 0.0F, 0.0F, 0.0F);
         color1t.set(0.0F, 0.0F, 0.0F, 0.0F);
         timert = System.currentTimeMillis();
         size0t = -1.0F;
         lastDist = -1.0F;
         lastHandheld = !var6;
         resetShader = false;
      }

      if (!color0t.equals(color0)) {
         color0t.set(color0);
         GlUtil.updateShaderVector4f(ShaderLibrary.simpleBeamShader, "thrustColor0", color0t);
      }

      if (!color1t.equals(var2)) {
         color1t.set(var2);
         GlUtil.updateShaderVector4f(ShaderLibrary.simpleBeamShader, "thrustColor1", color1t);
      }

      if (Math.abs((var0 /= 17.0F) - lastDist) > 0.03F) {
         GlUtil.updateShaderFloat(ShaderLibrary.simpleBeamShader, "texCoordMult", var0);
         lastDist = var0;
      }

      if (Math.abs(var1 - lastLenDiff) > 0.03F) {
         GlUtil.updateShaderFloat(ShaderLibrary.simpleBeamShader, "lenDiff", var1);
         lastLenDiff = var1;
      }

      if (timert != var4) {
         timert = var4;
         var0 = (float)(var4 - timert) / 1000.0F;
         GlUtil.updateShaderFloat(ShaderLibrary.simpleBeamShader, "time", var0);
      }

      if (Math.abs(var3 - size0t) > 0.03F) {
         size0t = var3;
         GlUtil.updateShaderFloat(ShaderLibrary.simpleBeamShader, "size", size0t);
         GlUtil.updateShaderFloat(ShaderLibrary.simpleBeamShader, "sizeInv", Math.max(1.0E-7F, 1.0F / size0t));
      }

      if (lastHandheld != var6) {
         lastHandheld = var6;
         GlUtil.updateShaderBoolean(ShaderLibrary.simpleBeamShader, "handheld", var6);
      }

      ShaderLibrary.simpleBeamShader.recompiled = false;
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.beamHandlers == null) {
         if (this.exceptionCount % 100 == 0) {
            try {
               throw new Exception("[CLIENT][ERROR] ########## beam handlers null of beam drawer " + this);
            } catch (Exception var6) {
               var6.printStackTrace();
            }
         }

         ++this.exceptionCount;
      } else {
         if (this.drawNeeded && EngineSettings.G_DRAW_BEAMS.isOn()) {
            Iterator var1 = this.toDraw.iterator();

            while(var1.hasNext()) {
               Iterator var2 = ((AbstractBeamHandler)var1.next()).getBeamStates().values().iterator();

               while(var2.hasNext()) {
                  BeamState var3;
                  drawConnection(var3 = (BeamState)var2.next(), this.tmpTrans, this.manager.getTime(), Controller.getCamera().getPos());
                  if (this.manager.getZoomFac() != 0.0F) {
                     float var4 = var3.handheld ? 1.0F : 5.0F;
                     float var5 = var3.handheld ? 1.0F : Math.min(5.0F, Math.max(100.0F, var3.getPower() / 100.0F));
                     var3.fromInset.set(var3.from);
                     var3.dirTmp.sub(var3.to, var3.from);
                     FastMath.normalizeCarmack(var3.dirTmp);
                     if (!var3.handheld) {
                        var3.dirTmp.scale(0.5F);
                        var3.fromInset.add(var3.dirTmp);
                     }

                     this.manager.addHitpoint(var3.fromInset, var4 * 0.5F, var3.color);
                     if (var3.hitPoint != null) {
                        var3.toInset.set(var3.hitPoint);
                        if (!var3.handheld) {
                           var3.toInset.sub(var3.dirTmp);
                        }

                        this.manager.addHitpoint(var3.toInset, var5, var3.color);
                     }
                  }
               }
            }
         }

      }
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
   }

   public void clearObservers() {
      Iterator var1 = this.beamHandlers.iterator();

      while(var1.hasNext()) {
         ((BeamHandlerContainer)var1.next()).getHandler().setDrawer((BeamDrawer)null);
      }

      this.toDraw.clear();
   }

   private void draw(AbstractBeamHandler var1, boolean var2, long var3, Vector3f var5) {
      if (EngineSettings.G_DRAW_BEAMS.isOn()) {
         for(Iterator var6 = var1.getBeamStates().values().iterator(); var6.hasNext(); ++BeamDrawerManager.drawCalls) {
            BeamState var7 = (BeamState)var6.next();

            assert var7 != null;

            var7.color.set(this.color1);
            drawConnection(var7, this.tmp, var3, var5);
            if (var2 && var1.drawBlockSalvage()) {
               this.drawBlockSalvage(var7, this.tmp);
            }
         }

      }
   }

   private void drawBlockSalvage(BeamState var1, Transform var2) {
   }

   public void drawBlock(Transform var1) {
      GlUtil.glPushMatrix();
      GlUtil.glMultMatrix(var1);
      GlUtil.scaleModelview(1.01F, 1.01F, 1.01F);
      BeamDrawerManager.singlecubemesh.renderVBO();
      GlUtil.glPopMatrix();
   }

   private boolean isValid(SegmentPiece var1) {
      var1.refresh();
      return var1.getType() != 0;
   }

   private void drawBoxes(AbstractBeamHandler var1) {
      Vector3f var2 = Controller.getCamera().getPos();
      Iterator var5 = var1.getBeamStates().values().iterator();

      while(true) {
         BeamState var3;
         SegmentPiece var4;
         do {
            do {
               if (!var5.hasNext()) {
                  return;
               }
            } while((var4 = (var3 = (BeamState)var5.next()).currentHit) == null);
         } while(!this.isValid(var4));

         var4.getAbsolutePos(this.tmpPos);
         SegmentController var6 = var4.getSegmentController();
         if (!var3.lastHitPos.equals(this.tmpPos) || !var6.getWorldTransformOnClient().equals(var3.lastSegConTrans)) {
            var3.lastSegConTrans.set(var6.getWorldTransformOnClient());
            var3.lastHitTrans.set(var6.getWorldTransformOnClient());
            var3.lastHitPos.set(this.tmpPos);
            this.p.set((float)(var3.lastHitPos.x - 16), (float)(var3.lastHitPos.y - 16), (float)(var3.lastHitPos.z - 16));
            var3.lastHitTrans.basis.transform(this.p);
            var3.lastHitTrans.origin.add(this.p);
         }

         float var7;
         if ((var7 = Vector3fTools.distance(var2.x, var2.y, var2.z, var3.lastHitTrans.origin.x, var3.lastHitTrans.origin.y, var3.lastHitTrans.origin.z)) < 15.0F) {
            GlUtil.updateShaderFloat(ShaderLibrary.beamBoxShader, "texMult", 1.0F + FastMath.pow(var3.hitBlockTime * 2.5F, 0.8F) * 5.0F);
         }

         if (var7 < 80.0F) {
            this.drawBlock(var3.lastHitTrans);
         }
      }
   }

   void drawSelectionBoxes() {
      GlUtil.updateShaderVector4f(ShaderLibrary.beamBoxShader, "selectionColor", 0.2F, 0.4F, 0.9F, 0.7F);
      Iterator var1 = this.toDraw.iterator();

      while(var1.hasNext()) {
         AbstractBeamHandler var2 = (AbstractBeamHandler)var1.next();
         this.drawBoxes(var2);
      }

   }

   public List getBeamHandlers() {
      return this.beamHandlers;
   }

   public void setBeamHandlers(ArrayList var1) {
      this.beamHandlers = var1;
   }

   public void insertEnd(ObjectHeapPriorityQueue var1) {
      if (this.drawNeeded) {
         Iterator var2 = this.toDraw.iterator();

         while(var2.hasNext()) {
            Iterator var3 = ((AbstractBeamHandler)var2.next()).getBeamStates().values().iterator();

            while(var3.hasNext()) {
               BeamState var4;
               if ((var4 = (BeamState)var3.next()).hitPoint != null) {
                  tmpPosD.sub(Controller.getCamera().getPos(), var4.hitPoint);
               } else {
                  tmpPosD.sub(Controller.getCamera().getPos(), var4.to);
               }

               var4.camDistEnd = tmpPosD.lengthSquared();
               var1.enqueue(var4);
            }
         }
      }

   }

   public void insertStart(ObjectHeapPriorityQueue var1) {
      if (this.drawNeeded) {
         Iterator var2 = this.toDraw.iterator();

         while(var2.hasNext()) {
            Iterator var3 = ((AbstractBeamHandler)var2.next()).getBeamStates().values().iterator();

            while(var3.hasNext()) {
               BeamState var4 = (BeamState)var3.next();
               tmpPosD.sub(Controller.getCamera().getPos(), var4.from);
               var4.camDistStart = tmpPosD.lengthSquared();
               int var5 = var1.size();
               var1.enqueue(var4);

               assert var1.size() == var5 + 1 : var1.size();
            }
         }
      }

   }

   public void notifyDraw(AbstractBeamHandler var1, boolean var2) {
      if (var2) {
         this.toDraw.add(var1);
      } else {
         this.toDraw.remove(var1);
      }

      if (this.drawNeeded != !this.toDraw.isEmpty()) {
         this.manager.notifyOfBeam(this, !this.toDraw.isEmpty());
      }

      this.drawNeeded = !this.toDraw.isEmpty();
   }

   public void reset() {
      if (this.beamHandlers != null) {
         this.clearObservers();
      }

      this.beamHandlers = null;
      this.exceptionCount = 0;
   }

   public void set(List var1, BeamDrawerManager var2) {
      this.beamHandlers = var1;
      this.manager = var2;
      if (this.beamHandlers != null) {
         this.updateObservers();
      } else {
         this.clearObservers();
      }
   }

   public void update(Timer var1) {
      this.linTimer.update(var1);
   }

   public void updateObservers() {
      this.clearObservers();
      Iterator var1 = this.beamHandlers.iterator();

      while(var1.hasNext()) {
         ((BeamHandlerContainer)var1.next()).getHandler().setDrawer(this);
      }

   }

   public boolean isValid() {
      return this.beamHandlers != null;
   }
}
