package org.schema.game.client.view.camera;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.EditSegmentInterface;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.common.InputHandler;
import org.schema.schine.graphicsengine.camera.viewer.FixedViewer;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public class ShipMovableCameraViewable extends FixedViewer implements InputHandler {
   protected SegmentController controller;
   protected EditSegmentInterface edit;
   Transform tinv = new Transform();
   private Vector3f posCur = new Vector3f();
   private float minSpeed = 5.0F;
   public final Vector3f initialDist;
   public Vector3f blockPos = new Vector3f();

   public ShipMovableCameraViewable(EditSegmentInterface var1, Vector3f var2) {
      super(var1.getSegmentController());
      this.controller = var1.getSegmentController();
      this.edit = var1;
      this.initialDist = var2;
   }

   public synchronized Vector3f getPos() {
      Vector3f var1 = super.getPos();
      Vector3f var2 = this.getRelativeCubePos();
      this.tinv.set(this.getEntity().getWorldTransform());
      this.tinv.basis.transform(var2);
      var1.add(var2);
      return var1;
   }

   public boolean canMove(PlayerInteractionControlManager var1) {
      if (!var1.getInShipControlManager().getShipControlManager().getSegmentBuildController().isTreeActive() && !var1.getSegmentControlManager().isTreeActive()) {
         return false;
      } else if (var1.isTreeActive() && !var1.isSuspended()) {
         if (((GameClientState)this.controller.getState()).getGlobalGameControlManager().getIngameControlManager().getChatControlManager().isActive()) {
            return false;
         } else {
            return ((GameClientState)this.controller.getState()).getPlayer() == null || ((GameClientState)this.controller.getState()).getPlayer().canClientPressKey();
         }
      } else {
         return false;
      }
   }

   public void update(Timer var1) {
      if (this.canMove(((GameClientState)this.controller.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager())) {
         Vector3f var2 = new Vector3f(this.controller.getCamForwLocal());
         Vector3f var3 = new Vector3f(this.controller.getCamUpLocal());
         Vector3f var4 = new Vector3f(this.controller.getCamLeftLocal());
         float var5 = KeyboardMappings.BUILD_MODE_FAST_MOVEMENT.isDown(this.controller.getState()) ? (Float)EngineSettings.BUILD_MODE_SHIFT_SPEED.getCurrentState() : this.minSpeed;
         var2.scale(var5 * var1.getDelta());
         var3.scale(var5 * var1.getDelta());
         var4.scale(var5 * var1.getDelta());
         if (!KeyboardMappings.FORWARD.isDown(this.controller.getState()) || !KeyboardMappings.BACKWARDS.isDown(this.controller.getState())) {
            if (KeyboardMappings.FORWARD.isDown(this.controller.getState())) {
               this.posCur.add(var2);
            }

            if (KeyboardMappings.BACKWARDS.isDown(this.controller.getState())) {
               var2.scale(-1.0F);
               this.posCur.add(var2);
            }
         }

         if (!KeyboardMappings.STRAFE_LEFT.isDown(this.controller.getState()) || !KeyboardMappings.STRAFE_RIGHT.isDown(this.controller.getState())) {
            if (KeyboardMappings.STRAFE_LEFT.isDown(this.controller.getState())) {
               var4.scale(-1.0F);
               this.posCur.add(var4);
            }

            if (KeyboardMappings.STRAFE_RIGHT.isDown(this.controller.getState())) {
               this.posCur.add(var4);
            }
         }

         if (!KeyboardMappings.DOWN.isDown(this.controller.getState()) || !KeyboardMappings.UP.isDown(this.controller.getState())) {
            if (KeyboardMappings.DOWN.isDown(this.controller.getState())) {
               var3.scale(-1.0F);
               this.posCur.add(var3);
            }

            if (KeyboardMappings.UP.isDown(this.controller.getState())) {
               this.posCur.add(var3);
            }
         }

      }
   }

   public Vector3f getRelativeCubePos() {
      return new Vector3f(this.posCur.x + (float)this.edit.getCore().x - 16.0F + this.initialDist.x, this.posCur.y + (float)this.edit.getCore().y - 16.0F + this.initialDist.y, this.posCur.z + (float)this.edit.getCore().z - 16.0F + this.initialDist.z);
   }

   public void handleKeyEvent(KeyEventInterface var1) {
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public void jumpTo(Vector3i var1) {
      var1.sub(this.edit.getCore());
   }

   public void jumpToInstantly(Vector3i var1) {
      var1.sub(this.edit.getCore());
      this.blockPos.set((float)var1.x, (float)var1.y, (float)var1.z);
      this.posCur.set((float)var1.x, (float)var1.y, (float)var1.z);
   }

   public Vector3f getPosRaw() {
      return this.posCur;
   }

   public void jumpToInstantlyWithoutOffset(Vector3i var1) {
      this.blockPos.set((float)var1.x, (float)var1.y, (float)var1.z);
      this.posCur.set((float)var1.x, (float)var1.y, (float)var1.z);
   }
}
