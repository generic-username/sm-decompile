package org.schema.game.client.view.camera;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.game.client.controller.manager.ingame.EditSegmentInterface;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.JoystickAxisMapping;
import org.schema.schine.graphicsengine.camera.Camera;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.camera.TransitionCamera;
import org.schema.schine.graphicsengine.camera.viewer.AbstractViewer;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.input.JoystickMappingFile;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.objects.container.TransformTimed;

public class InShipCamera extends Camera implements SegmentControllerCamera {
   private final SegmentController controller;
   public boolean docked;
   private boolean align;
   private Matrix3f mRot;
   private InShipCamera.HelperCamera helperCamera;
   private Camera freeViewLockedMoveCamera;
   private BuildShipCamera freeCamera;
   private Vector3f shipForward = new Vector3f(0.0F, 0.0F, 1.0F);
   private Vector3f shipRight = new Vector3f(1.0F, 0.0F, 0.0F);
   private Vector3f shipUp = new Vector3f(0.0F, 1.0F, 0.0F);
   private boolean firstInit = false;
   private Matrix3f mRot2;
   private float[] rotMap;
   private int orientation;
   private Camera toReset;
   private TransitionCamera transitionCamera;
   private boolean cancel;
   private boolean adjustMode;
   private EditSegmentInterface c;
   private Transform adjustedTransformRelativeToBlock;

   public InShipCamera(EditSegmentInterface var1, Camera var2, SegmentPiece var3) {
      super(var1.getSegmentController().getState(), new InShipCamera.ShipViewable(var1));
      ((InShipCamera.ShipViewable)this.getViewable()).fm = this;
      this.c = var1;
      this.controller = var1.getSegmentController();
      this.rotMap = new float[6];
      this.rotMap[0] = 0.0F;
      this.rotMap[1] = 3.1415927F;
      this.rotMap[2] = -1.5707964F;
      this.rotMap[3] = 1.5707964F;
      this.rotMap[4] = -1.5707964F;
      this.rotMap[5] = 1.5707964F;
      this.toReset = var2;
      this.firstInit = true;
      byte var4 = var3.getOrientation();
      var4 = (byte)Math.max(0, Math.min(5, var4));
      this.setOrientation(var4);
      if (this.getOrientation() != 2 && this.getOrientation() != 3) {
         this.mRot = new Matrix3f();
         this.mRot.setIdentity();
         this.mRot.rotY(this.rotMap[this.getOrientation()]);
         this.mRot2 = new Matrix3f();
         this.mRot2.setIdentity();
         this.mRot2.rotY(-this.rotMap[this.getOrientation()]);
      } else {
         Matrix3f var5;
         (var5 = new Matrix3f()).setIdentity();
         var5.rotY(1.5707964F);
         this.mRot = new Matrix3f();
         this.mRot.setIdentity();
         this.mRot.rotX(this.rotMap[this.getOrientation()]);
         this.mRot.mul(var5);
         this.mRot2 = new Matrix3f();
         this.mRot2.setIdentity();
         this.mRot2.rotX(-this.rotMap[this.getOrientation()]);
         this.mRot2.mul(var5);
      }

      this.helperCamera = new InShipCamera.HelperCamera(var1.getSegmentController().getState(), new ShipOffsetCameraViewable(var1));
      this.freeViewLockedMoveCamera = new Camera(var1.getSegmentController().getState(), new ShipOffsetCameraViewable(var1));
      this.freeCamera = new BuildShipCamera((GameClientState)this.state, (Camera)null, var1, new InShipCamera.ShipAdjustViewable(var1), (Transform)null);
      this.reset();
      this.helperCamera.reset();
      this.freeViewLockedMoveCamera.reset();
      this.freeCamera.reset();
   }

   public void align() {
      this.align = true;
   }

   public void cancel() {
      this.cancel = true;
   }

   public void forceOrientation(Transform var1, Timer var2) {
      this.getWorldTransform().basis.set(var1.basis);
      this.setForward(GlUtil.getForwardVector(new Vector3f(), var1));
      this.setUp(GlUtil.getUpVector(new Vector3f(), var1));
      this.setRight(GlUtil.getRightVector(new Vector3f(), var1));
      this.getViewable().setLeft(GlUtil.getLeftVector(new Vector3f(), var1));
      this.getViewable().setForward(GlUtil.getForwardVector(new Vector3f(), var1));
      this.getViewable().setUp(GlUtil.getUpVector(new Vector3f(), var1));
      this.getLookAlgorithm().force(var1);
      this.helperCamera.getWorldTransform().basis.set(var1.basis);
      this.helperCamera.setForward(GlUtil.getForwardVector(new Vector3f(), var1));
      this.helperCamera.setUp(GlUtil.getUpVector(new Vector3f(), var1));
      this.helperCamera.setRight(GlUtil.getRightVector(new Vector3f(), var1));
      this.helperCamera.getViewable().setLeft(GlUtil.getLeftVector(new Vector3f(), var1));
      this.helperCamera.getViewable().setForward(GlUtil.getForwardVector(new Vector3f(), var1));
      this.helperCamera.getViewable().setUp(GlUtil.getUpVector(new Vector3f(), var1));
      this.helperCamera.getLookAlgorithm().force(var1);
      this.shipRight.set(GlUtil.getRightVector(new Vector3f(), var1));
      this.shipUp.set(GlUtil.getUpVector(new Vector3f(), var1));
      this.shipForward.set(GlUtil.getForwardVector(new Vector3f(), var1));
      this.update(var2, this.controller.isOnServer());
   }

   public final SegmentController getSegmentController() {
      return this.controller;
   }

   public InShipCamera.HelperCamera getHelperCamera() {
      return this.helperCamera;
   }

   public void setHelperCamera(InShipCamera.HelperCamera var1) {
      this.helperCamera = var1;
   }

   public Vector3f getHelperForward() {
      Transform var1 = new Transform(this.helperCamera.getWorldTransform());
      return GlUtil.getForwardVector(new Vector3f(), var1);
   }

   public int getOrientation() {
      return this.orientation;
   }

   public void setOrientation(int var1) {
      this.orientation = var1;
   }

   public void resetTransition(Camera var1) {
      this.setStable(false);
      this.transitionCamera = new TransitionCamera(var1, this, 0.2F);
   }

   public TransformTimed getWorldTransform() {
      return super.getWorldTransform();
   }

   public void reset() {
      super.reset();
      if (this.firstInit) {
         this.getWorldTransform().basis.set(this.getSegmentController().getWorldTransform().basis);
      }

   }

   private boolean addShipRotationFromEnteredNonCore() {
      SegmentPiece var1;
      return (var1 = ((GameClientState)this.state).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getEntered()) != null && !var1.equalsPos(Ship.core);
   }

   public void resetAdjustedMatrix() {
      ((InShipCamera.ShipViewable)this.getViewable()).offset.set(0.0F, 0.0F, 0.0F);
      this.adjustedTransformRelativeToBlock = null;
   }

   public void resetAdjustMode() {
      if (this.adjustMode) {
         this.freeCamera = new BuildShipCamera((GameClientState)this.state, (Camera)null, this.c, new InShipCamera.ShipAdjustViewable(this.c), (Transform)null);
         this.freeCamera.getWorldTransform().set(this.helperCamera.getWorldTransform());
         ((ShipMovableCameraViewable)this.freeCamera.getViewable()).jumpToInstantlyWithoutOffset(((ShipOffsetCameraViewable)this.getViewable()).getPosMod());
         this.resetAdjustedMatrix();
      } else {
         this.adjustedTransformRelativeToBlock = new Transform();
         this.adjustedTransformRelativeToBlock.setIdentity();
         ((Ship)this.getSegmentController()).getManagerContainer().getCockpitManager().setTransformFor(((GameClientState)this.state).getPlayer().getCockpit(), this.adjustedTransformRelativeToBlock);
         ((InShipCamera.ShipViewable)this.getViewable()).offset.set(this.adjustedTransformRelativeToBlock.origin);
         ((Ship)this.getSegmentController()).getManagerContainer().getCockpitManager().send(((GameClientState)this.state).getPlayer().getCockpit().block, this.adjustedTransformRelativeToBlock);
         this.resetAdjustedMatrix();
      }
   }

   public void setAdjustMode(boolean var1) {
      if (((GameClientState)this.controller.getState()).getPlayer() != null && !((GameClientState)this.controller.getState()).getPlayer().getCockpit().isCore()) {
         if (!this.adjustMode && var1) {
            Transform var2 = ((Ship)this.c.getSegmentController()).getManagerContainer().getCockpitManager().getTransform(((GameClientState)this.state).getPlayer().getCockpit().block);
            Transform var3;
            (var3 = new Transform()).setIdentity();
            var3.basis.set(var2.basis);
            this.freeCamera = new BuildShipCamera((GameClientState)this.state, (Camera)null, this.c, new InShipCamera.ShipAdjustViewable(this.c), var3);
            this.freeCamera.getWorldTransform().set(this.helperCamera.getWorldTransform());
            this.freeCamera.getLookAlgorithm().force(var3);
            ((ShipMovableCameraViewable)this.freeCamera.getViewable()).jumpToInstantlyWithoutOffset(((ShipOffsetCameraViewable)this.getViewable()).getPosMod());
            ((ShipMovableCameraViewable)this.freeCamera.getViewable()).getPosRaw().add(var2.origin);
            this.resetAdjustedMatrix();
         }

         if (this.adjustMode && !var1) {
            this.adjustedTransformRelativeToBlock = new Transform();
            this.adjustedTransformRelativeToBlock.set(this.getSegmentController().getWorldTransformInverse());
            this.adjustedTransformRelativeToBlock.mul(this.freeCamera.getWorldTransform());
            this.adjustedTransformRelativeToBlock.origin.sub(((ShipMovableCameraViewable)this.freeCamera.getViewable()).blockPos);
            ((Ship)this.getSegmentController()).getManagerContainer().getCockpitManager().setTransformFor(((GameClientState)this.state).getPlayer().getCockpit(), this.adjustedTransformRelativeToBlock);
            ((InShipCamera.ShipViewable)this.getViewable()).offset.set(this.adjustedTransformRelativeToBlock.origin);
            ((Ship)this.getSegmentController()).getManagerContainer().getCockpitManager().send(((GameClientState)this.state).getPlayer().getCockpit().block, this.adjustedTransformRelativeToBlock);
            System.err.println("ADJUSTED MAT: \n" + this.adjustedTransformRelativeToBlock.getMatrix(new Matrix4f()));
         }

         this.adjustMode = var1;
         System.err.println("[CLIENT][INSHIPCAMERA] ADJUST MODE " + this.adjustMode);
      } else {
         this.resetAdjustedMatrix();
      }
   }

   public void switchAdjustMode() {
      this.setAdjustMode(!this.adjustMode);
   }

   public synchronized void update(Timer var1, boolean var2) {
      if (this.firstInit) {
         GlUtil.getRightVector(this.shipRight, (Transform)this.getSegmentController().getWorldTransform());
         GlUtil.getUpVector(this.shipUp, (Transform)this.getSegmentController().getWorldTransform());
         GlUtil.getForwardVector(this.shipForward, (Transform)this.getSegmentController().getWorldTransform());
         this.setRight(this.shipRight);
         this.setUp(this.shipUp);
         this.setForward(this.shipForward);
         this.firstInit = false;
      }

      super.mouseRotationActive = false;
      super.update(var1, var2);
      GlUtil.getRightVector(this.shipRight, (Transform)this.getSegmentController().getWorldTransform());
      GlUtil.getUpVector(this.shipUp, (Transform)this.getSegmentController().getWorldTransform());
      GlUtil.getForwardVector(this.shipForward, (Transform)this.getSegmentController().getWorldTransform());
      this.setRight(this.shipRight);
      this.setUp(this.shipUp);
      this.setForward(this.shipForward);
      if (CameraMouseState.isInMouseControl((ClientStateInterface)this.state)) {
         ((ShipOffsetCameraViewable)this.helperCamera.getViewable()).getPosMod().set(((ShipOffsetCameraViewable)this.getViewable()).getPosMod());
         ((ShipOffsetCameraViewable)this.freeViewLockedMoveCamera.getViewable()).getPosMod().set(((ShipOffsetCameraViewable)this.getViewable()).getPosMod());
         this.helperCamera.update(var1, var2);
         this.freeViewLockedMoveCamera.update(var1, var2);
         this.freeCamera.update(var1, var2);
         if (this.getSegmentController() != null) {
            if (this.adjustMode) {
               this.setRight(this.freeCamera.getRight());
               this.setUp(this.freeCamera.getUp());
               this.setForward(this.freeCamera.getForward());
            } else if (KeyboardMappings.FREE_CAM.isDownOrSticky(this.state)) {
               this.setRight(this.freeViewLockedMoveCamera.getRight());
               this.setUp(this.freeViewLockedMoveCamera.getUp());
               this.setForward(this.freeViewLockedMoveCamera.getForward());
               PlayerState var4 = ((GameClientState)this.state).getPlayer();
               if (this.controller.getSlotAssignment().getAsIndex(var4.getCurrentShipControllerSlot()) == -9223372036854775779L) {
                  this.helperCamera.getWorldTransform().set(this.freeViewLockedMoveCamera.getWorldTransform());
               }
            } else if (EngineSettings.DEBUG_SHIP_CAM_ON_RCONTROL.isOn() && Keyboard.isKeyDown(157)) {
               this.setRight(this.helperCamera.getRight());
               this.setUp(this.helperCamera.getUp());
               this.setForward(this.helperCamera.getForward());
            } else if (this.addShipRotationFromEnteredNonCore()) {
               Vector3f var3 = new Vector3f();
               Vector3f var10 = new Vector3f();
               Vector3f var5 = new Vector3f();
               GlUtil.getRightVector(var3, (Transform)this.getSegmentController().getWorldTransform());
               GlUtil.getUpVector(var10, (Transform)this.getSegmentController().getWorldTransform());
               GlUtil.getForwardVector(var5, (Transform)this.getSegmentController().getWorldTransform());
               Transform var6 = new Transform();
               GlUtil.setForwardVector(this.shipForward, var6);
               GlUtil.setUpVector(this.shipUp, var6);
               GlUtil.setRightVector(this.shipRight, var6);
               Transform var7 = new Transform();
               GlUtil.setForwardVector(var5, var7);
               GlUtil.setUpVector(var10, var7);
               GlUtil.setRightVector(var3, var7);
               Matrix3f var8;
               (var8 = new Matrix3f()).sub(var7.basis, var6.basis);
               this.helperCamera.getWorldTransform().basis.add(var8);
               this.shipRight.set(var3);
               this.shipUp.set(var10);
               this.shipForward.set(var5);
            }
         }
      }

      if (this.getSegmentController() != null && !this.getSegmentController().railController.isDockedOrDirty() && !Keyboard.isKeyDown(157) && !KeyboardMappings.FREE_CAM.isDownOrSticky(this.state) && !this.adjustMode) {
         this.getWorldTransform().basis.set(this.getSegmentController().getWorldTransform().basis);
         Matrix3f var9;
         (var9 = new Matrix3f(this.mRot2)).invert();
         this.getWorldTransform().basis.mul(var9);
         if (this.adjustedTransformRelativeToBlock != null) {
            this.getWorldTransform().basis.mul(this.adjustedTransformRelativeToBlock.basis);
         } else {
            ((InShipCamera.ShipViewable)this.getViewable()).offset.set(0.0F, 0.0F, 0.0F);
         }
      }

      if (this.transitionCamera != null && this.transitionCamera.isActive()) {
         this.setStable(false);
         this.transitionCamera.update(var1, var2);
         this.getWorldTransform().set(this.transitionCamera.getWorldTransform());
      } else if (!this.isStable()) {
         this.setStable(true);
         this.helperCamera.reset();
      }

      if (this.toReset != null) {
         this.resetTransition(this.toReset);
         this.toReset = null;
      }

   }

   public void addRecoil(float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, float var9, float var10) {
      InShipCamera.Recoil var11 = new InShipCamera.Recoil();
      if (var3 != 0.0F) {
         var3 = (float)(Math.random() * (double)FastMath.sign(var3));
      } else {
         var3 = (float)((Math.random() - 0.5D) * 2.0D);
      }

      if (var4 != 0.0F) {
         var4 = (float)(Math.random() * (double)FastMath.sign(var4));
      } else {
         var4 = (float)((Math.random() - 0.5D) * 2.0D);
      }

      var11.force.set(var3, var4);
      if (var3 == 0.0F && var4 == 0.0F) {
         var11.force.set(1.0F, 0.0F);
      }

      var11.force.normalize();
      Vector2f var10000 = var11.force;
      var10000.x *= var1;
      var10000 = var11.force;
      var10000.y *= var2;
      if (var11.force.lengthSquared() > 0.0F) {
         var11.speedOut = var8 * var11.force.length();
         var11.speedOutAdd = var9;
         var11.speedOutPowMult = var10;
         var11.speedBack = var5 * var11.force.length();
         var11.speedBackAdd = var6;
         var11.speedBackPowMult = var7;

         InShipCamera.Recoil var13;
         for(Iterator var12 = this.helperCamera.recoils.iterator(); var12.hasNext(); var13.speedBack *= 2.0F) {
            InShipCamera.Recoil var14 = var13 = (InShipCamera.Recoil)var12.next();
            var14.speedOut *= 2.0F;
         }

         if (this.helperCamera.recoils.isEmpty()) {
            this.helperCamera.lastInitialRecoil = this.controller.getUpdateTime();
            this.helperCamera.lastRecoilState.set(this.helperCamera.getWorldTransform().basis);
         }

         this.helperCamera.recoils.add(var11);
      }

   }

   public void updateMouseState() {
      if (this.adjustMode) {
         this.freeCamera.updateMouseState();
      } else if (KeyboardMappings.FREE_CAM.isDownOrSticky(this.state)) {
         this.freeViewLockedMoveCamera.updateMouseState();
      } else {
         this.helperCamera.updateMouseState();
      }

      super.updateMouseState();
   }

   public boolean isInAdjustMode() {
      return this.adjustMode;
   }

   public void setAdjustMatrix(Transform var1) {
      this.adjustedTransformRelativeToBlock = new Transform(var1);
      ((InShipCamera.ShipViewable)this.getViewable()).offset.set(var1.origin);
   }

   public class HelperCamera extends Camera {
      public long lastInitialRecoil;
      public Matrix3f lastRecoilState = new Matrix3f();
      private TransformableOldRestrictedAxisCameraLook dockingOldAlgo;
      private TransformableRestrictedAxisCameraLook dockingAlgo;
      private TransformableRestrictedCameraLook defaultAlgo = new TransformableRestrictedCameraLook(this, InShipCamera.this.getSegmentController());
      private boolean wasDocked;
      public final ObjectArrayList recoils = new ObjectArrayList();

      public HelperCamera(StateInterface var2, AbstractViewer var3) {
         super(var2, var3);
         this.setLookAlgorithm(this.defaultAlgo);
         this.setMouseSensibilityX(0.2F);
         this.setMouseSensibilityY(0.2F);
         this.dockingOldAlgo = new TransformableOldRestrictedAxisCameraLook(this, InShipCamera.this.getSegmentController());
         this.dockingAlgo = new TransformableRestrictedAxisCameraLook(this, InShipCamera.this.getSegmentController());
      }

      private void setDockingAlgo(boolean var1) {
         if (InShipCamera.this.getSegmentController().getDockingController().isDocked()) {
            if (var1 || this.dockingOldAlgo.getOrientation() != InShipCamera.this.getSegmentController().getDockingController().getDockedOn().to.getOrientation() || this.dockingOldAlgo.correcting2Transformable != InShipCamera.this.getSegmentController().getDockingController().getDockedOn().to.getSegment().getSegmentController()) {
               this.dockingOldAlgo = new TransformableOldRestrictedAxisCameraLook(this, InShipCamera.this.getSegmentController());
            }

            this.dockingOldAlgo.setCorrectingForNonCoreEntry(InShipCamera.this.addShipRotationFromEnteredNonCore());
            this.dockingOldAlgo.getFollowing().set(InShipCamera.this.getSegmentController().getWorldTransform());
            this.dockingOldAlgo.setCorrecting2Transformable(InShipCamera.this.getSegmentController().getDockingController().getDockedOn().to.getSegment().getSegmentController());
            byte var3 = InShipCamera.this.getSegmentController().getDockingController().getDockedOn().to.getOrientation();
            this.dockingOldAlgo.setOrientation(var3);
            this.setLookAlgorithm(this.dockingOldAlgo);
         } else if (InShipCamera.this.getSegmentController().railController.isDockedAndExecuted()) {
            if (var1 = !this.wasDocked || var1 || this.dockingAlgo.getOrientation() != InShipCamera.this.getSegmentController().railController.previous.rail.getOrientation() || this.dockingAlgo.correcting2Transformable != InShipCamera.this.getSegmentController().railController.previous.rail.getSegmentController()) {
               assert this.dockingAlgo != null;

               assert InShipCamera.this.getSegmentController().railController.previous != null;

               System.err.println("NEW LOOKING ALGO " + this.dockingAlgo.getOrientation() + " / " + InShipCamera.this.getSegmentController().railController.previous.rail.getOrientation() + "; " + this.dockingAlgo.correcting2Transformable + " / " + InShipCamera.this.getSegmentController().railController.previous.rail.getSegmentController());
               this.dockingAlgo = new TransformableRestrictedAxisCameraLook(this, InShipCamera.this.getSegmentController());
            }

            this.dockingAlgo.setCorrecting2Transformable(InShipCamera.this.getSegmentController().railController.previous.rail.getSegmentController());
            byte var2 = InShipCamera.this.getSegmentController().railController.previous.rail.getOrientation();
            this.dockingAlgo.setOrientation(var2);
            this.setLookAlgorithm(this.dockingAlgo);
            if (var1) {
               this.dockingAlgo.mouseRotate(InShipCamera.this.getSegmentController().isOnServer(), 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F);
            }
         }

         this.wasDocked = InShipCamera.this.getSegmentController().railController.isDockedAndExecuted();
      }

      public void reset() {
         super.reset();
         this.getWorldTransform().basis.set(InShipCamera.this.getSegmentController().getWorldTransform().basis);
      }

      public boolean wasDockedOnUpdate() {
         return this.wasDocked;
      }

      public void update(Timer var1, boolean var2) {
         if (!InShipCamera.this.getSegmentController().getDockingController().isDocked() && !InShipCamera.this.getSegmentController().railController.isDockedOrDirty()) {
            this.setLookAlgorithm(this.defaultAlgo);
            ((TransformableRestrictedCameraLook)this.getLookAlgorithm()).setCorrecting(InShipCamera.this.addShipRotationFromEnteredNonCore());
            ((TransformableRestrictedCameraLook)this.getLookAlgorithm()).getFollowing().set(InShipCamera.this.getSegmentController().getWorldTransform());
            ((TransformableRestrictedCameraLook)this.getLookAlgorithm()).setOrientation(InShipCamera.this.getOrientation());
         } else {
            this.setDockingAlgo(false);
         }

         if (CameraMouseState.isInMouseControl((ClientStateInterface)this.state)) {
            this.updateMouseWheel();

            for(int var3 = 0; var3 < this.recoils.size(); ++var3) {
               if (!((InShipCamera.Recoil)this.recoils.get(var3)).apply(var2, var1, this)) {
                  this.recoils.remove(var3--);
               }
            }

            if (JoystickMappingFile.ok()) {
               GameClientState var10;
               double var4 = (var10 = (GameClientState)InShipCamera.this.getSegmentController().getState()).getController().getJoystickAxis(JoystickAxisMapping.PITCH);
               double var6 = var10.getController().getJoystickAxis(JoystickAxisMapping.YAW);
               double var8 = var10.getController().getJoystickAxis(JoystickAxisMapping.ROLL);
               this.getLookAlgorithm().mouseRotate(InShipCamera.this.getSegmentController().isOnServer(), (float)var6 * var1.getDelta() * 10.0F, (float)var4 * var1.getDelta() * 10.0F, (float)var8 * var1.getDelta() * 10.0F, this.getMouseSensibilityX(), this.getMouseSensibilityY(), this.getMouseSensibilityX());
            }

            if (KeyboardMappings.ROTATE_LEFT_SHIP.isDown(this.state)) {
               this.getLookAlgorithm().mouseRotate(var2, 0.0F, 0.0F, -30.0F * var1.getDelta(), 0.0F, this.getMouseSensibilityY(), this.getMouseSensibilityX());
            }

            if (KeyboardMappings.ROTATE_RIGHT_SHIP.isDown(this.state)) {
               this.getLookAlgorithm().mouseRotate(var2, 0.0F, 0.0F, 30.0F * var1.getDelta(), 0.0F, this.getMouseSensibilityY(), this.getMouseSensibilityX());
            }

            if (KeyboardMappings.ROLL.isDown(this.state)) {
               this.getLookAlgorithm().mouseRotate(var2, 0.0F, (!EngineSettings.S_MOUSE_SHIP_INVERT.isOn() && !EngineSettings.S_MOUSE_ALL_INVERT.isOn() ? (float)this.mouseState.dy : (float)(-this.mouseState.dy)) / (float)(GLFrame.getHeight() / 2), (float)this.mouseState.dx / (float)(GLFrame.getWidth() / 2), 0.0F, this.getMouseSensibilityY(), this.getMouseSensibilityX());
            } else {
               this.getLookAlgorithm().mouseRotate(var2, (float)this.mouseState.dx / (float)(GLFrame.getWidth() / 2), (!EngineSettings.S_MOUSE_SHIP_INVERT.isOn() && !EngineSettings.S_MOUSE_ALL_INVERT.isOn() ? (float)this.mouseState.dy : (float)(-this.mouseState.dy)) / (float)(GLFrame.getHeight() / 2), 0.0F, this.getMouseSensibilityX(), this.getMouseSensibilityY(), 0.0F);
            }

            if (InShipCamera.this.align) {
               System.err.println("[SHIP-CAMERA] ALIGN VIEW");
               if (InShipCamera.this.getSegmentController().railController.isDockedOrDirty()) {
                  System.err.println("[SHIP-CAMERA] TODO: ALIGN VIEW FOR RAIL");
               } else if (InShipCamera.this.getSegmentController().getDockingController().isDocked()) {
                  Matrix3f var11 = new Matrix3f(InShipCamera.this.getSegmentController().getDockingController().getDockedOn().to.getSegment().getSegmentController().getWorldTransformOnClient().basis);
                  InShipCamera.this.helperCamera.getWorldTransform().basis.set(var11);
                  var11.set(InShipCamera.this.getSegmentController().getDockingController().targetStartQuaternion);
                  InShipCamera.this.helperCamera.getWorldTransform().basis.mul(var11);
                  this.setDockingAlgo(true);
                  this.getLookAlgorithm().fix();
               } else {
                  InShipCamera.this.helperCamera.getWorldTransform().basis.setIdentity();
                  this.getLookAlgorithm().force(InShipCamera.this.helperCamera.getWorldTransform());
                  InShipCamera.this.getSegmentController().getPhysicsDataContainer().getObject().activate(true);
               }

               InShipCamera.this.align = false;
            }

            if (InShipCamera.this.cancel) {
               if (!InShipCamera.this.getSegmentController().getDockingController().isDocked() && !InShipCamera.this.getSegmentController().railController.isDockedOrDirty()) {
                  InShipCamera.this.helperCamera.getWorldTransform().basis.set(InShipCamera.this.getSegmentController().getWorldTransform().basis);
                  this.getLookAlgorithm().force(InShipCamera.this.helperCamera.getWorldTransform());
                  InShipCamera.this.getSegmentController().getPhysicsDataContainer().getObject().activate(true);
               }

               InShipCamera.this.cancel = false;
            }

            this.updateViewer(var1);
         }

      }
   }

   static class Recoil {
      public float speedOut;
      public float speedOutAdd;
      public float speedOutPowMult;
      public float speedBack;
      public float speedBackAdd;
      public float speedBackPowMult;
      public Vector2f force;
      public Vector2f forceNorm;
      public Vector2f totalCurrent;
      public boolean goingBack;
      public float timeAcc;
      public static final float T_STEP = 0.016666668F;

      private Recoil() {
         this.speedOut = 5.0F;
         this.speedBack = 1.0F;
         this.force = new Vector2f();
         this.forceNorm = new Vector2f();
         this.totalCurrent = new Vector2f();
         this.goingBack = false;
      }

      public boolean apply(boolean var1, Timer var2, InShipCamera.HelperCamera var3) {
         for(this.timeAcc += var2.getDelta(); this.timeAcc > 0.016666668F; var3.getLookAlgorithm().mouseRotate(var1, this.forceNorm.x, this.forceNorm.y, 0.0F, 1.0F, 1.0F, 0.0F)) {
            this.timeAcc -= 0.016666668F;
            float var7 = this.force.length();
            float var4 = this.totalCurrent.lengthSquared() > 0.0F ? this.totalCurrent.length() : 0.0F;
            float var5;
            float var6;
            if (!this.goingBack && this.totalCurrent.length() < this.force.length()) {
               var7 -= var4;
               this.forceNorm.set(this.force);
               this.forceNorm.normalize();
               var5 = this.speedOut;
               var6 = this.totalCurrent.length() / this.force.length();
               if (this.speedOutAdd > 0.0F) {
                  var5 *= FastMath.pow((1.0F - var6) * this.speedOutPowMult + this.speedOutAdd, 2.0F);
               }

               if ((var4 = var5 * 0.016666668F) > var7) {
                  this.forceNorm.scale(var7);
                  this.totalCurrent.set(this.force);
                  this.goingBack = true;
               } else {
                  this.forceNorm.scale(var4);
                  this.totalCurrent.add(this.forceNorm);
               }

               if (this.forceNorm.length() < 0.002F && var7 < 0.1F) {
                  this.totalCurrent.set(this.force);
                  this.goingBack = true;
               }
            } else {
               if (this.totalCurrent.lengthSquared() <= 0.0F) {
                  return false;
               }

               this.forceNorm.set(this.force);
               this.forceNorm.normalize();
               this.forceNorm.negate();
               float var10000 = this.speedBack;
               var5 = this.totalCurrent.length() / this.force.length();
               if (this.speedBackAdd > 0.0F) {
                  FastMath.pow((1.0F - var5) * this.speedBackPowMult + this.speedBackAdd, 2.0F);
               }

               var6 = this.speedBack * 0.016666668F;
               if (var4 - var6 < 0.0F) {
                  this.forceNorm.scale(var4);
                  this.totalCurrent.set(0.0F, 0.0F);
               } else {
                  this.forceNorm.scale(var6);
                  this.totalCurrent.add(this.forceNorm);
               }

               if (this.forceNorm.length() < 0.002F && var4 < 0.1F) {
                  this.totalCurrent.set(0.0F, 0.0F);
               }
            }
         }

         return true;
      }

      // $FF: synthetic method
      Recoil(Object var1) {
         this();
      }
   }

   static class ShipAdjustViewable extends ShipMovableCameraViewable {
      public ShipAdjustViewable(EditSegmentInterface var1) {
         super(var1, new Vector3f());
      }

      public boolean canMove(PlayerInteractionControlManager var1) {
         if (var1.isTreeActive() && !var1.isSuspended()) {
            if (((GameClientState)this.controller.getState()).getGlobalGameControlManager().getIngameControlManager().getChatControlManager().isActive()) {
               return false;
            } else {
               return ((GameClientState)this.controller.getState()).getPlayer() == null || ((GameClientState)this.controller.getState()).getPlayer().canClientPressKey();
            }
         } else {
            return false;
         }
      }
   }

   static class ShipViewable extends ShipOffsetCameraViewable {
      public InShipCamera fm;

      public ShipViewable(EditSegmentInterface var1) {
         super(var1);
      }

      public Vector3f getPos() {
         return this.fm.adjustMode ? this.fm.freeCamera.getViewable().getPos() : super.getPos();
      }
   }
}
