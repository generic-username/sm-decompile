package org.schema.game.client.view.camera;

import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.camera.viewer.FixedViewer;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Transformable;

public class UpperFixedViewer extends FixedViewer {
   public UpperFixedViewer(Transformable var1) {
      super(var1);
   }

   public Vector3f getPos() {
      Vector3f var1 = super.getPos();
      Vector3f var2;
      (var2 = GlUtil.getUpVector(new Vector3f(), this.transform)).scale(0.685F);
      var1.add(var2);
      return var1;
   }
}
