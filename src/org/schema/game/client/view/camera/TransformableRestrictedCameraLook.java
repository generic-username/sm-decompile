package org.schema.game.client.view.camera;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Matrix3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.schine.graphicsengine.camera.Camera;
import org.schema.schine.graphicsengine.camera.look.MouseLookAlgorithm;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Transformable;

public class TransformableRestrictedCameraLook implements MouseLookAlgorithm {
   private final Transform start = new Transform();
   private final Transform following = new Transform();
   private final Transform followingOld = new Transform();
   private final Transform before = new Transform();
   float limit = 0.7853982F;
   private Camera camera;
   private Vector3f mouse = new Vector3f();
   private Quat4f rotation = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
   private boolean correcting;
   private int orientation = 0;
   private Transformable transformable;
   private int iterations = 0;
   private boolean init = true;

   public TransformableRestrictedCameraLook(Camera var1, Transformable var2) {
      this.camera = var1;
      this.transformable = var2;
      this.getFollowing().setIdentity();
      this.followingOld.setIdentity();
      this.start.set(var2.getWorldTransform());
   }

   public void fix() {
      this.getFollowing().setIdentity();
      this.followingOld.setIdentity();
      this.start.set(this.transformable.getWorldTransform());
   }

   public void force(Transform var1) {
      this.getFollowing().set(var1);
      this.followingOld.set(var1);
      this.start.set(this.transformable.getWorldTransform());
   }

   public void mouseRotate(boolean var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      this.before.set(this.camera.getWorldTransform());
      this.iterations = 0;
      this.mouse.x = -var2 * var5;
      this.mouse.y = var3 * var6;
      this.mouse.z = var4 * var7;
      this.mouseRotate();
   }

   public void lookTo(Transform var1) {
   }

   private void freeRot() {
      Vector3f var1 = new Vector3f();
      Vector3f var2 = new Vector3f();
      Vector3f var3 = new Vector3f();
      this.getVectors(var1, var2, var3, this.transformable.getWorldTransform());
      if (this.init) {
         this.camera.setForward(var1);
         this.camera.setUp(var2);
         this.camera.setRight(var3);
         this.init = false;
         this.followingOld.set(this.camera.getWorldTransform());
      }

      this.camera.getWorldTransform().set(this.followingOld);
      Vector3f var4;
      (var4 = new Vector3f()).cross(var1, var2);
      (var4 = new Vector3f(var4)).normalize();
      float var5;
      if ((var5 = this.mouse.y) > 1.0F) {
         var5 = 1.0F;
      }

      if (var5 < -1.0F) {
         var5 = -1.0F;
      }

      Quat4f var12;
      if (this.mouse.y == 0.0F) {
         var12 = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
      } else {
         var12 = new Quat4f(var4.x * FastMath.sin(var5), var4.y * FastMath.sin(var5), var4.z * FastMath.sin(var5), FastMath.cos(var5));
      }

      (var4 = new Vector3f(var2)).normalize();
      float var6;
      if ((var6 = this.mouse.x) > 1.0F) {
         var6 = 1.0F;
      }

      if (var6 < -1.0F) {
         var6 = -1.0F;
      }

      Quat4f var13;
      if (this.mouse.x == 0.0F) {
         var13 = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
      } else {
         var13 = new Quat4f(var4.x * FastMath.sin(var6), var4.y * FastMath.sin(var6), var4.z * FastMath.sin(var6), FastMath.cos(var6));
      }

      (var4 = new Vector3f(var1)).normalize();
      float var7;
      if ((var7 = this.mouse.z) > 1.0F) {
         var7 = 1.0F;
      }

      if (var7 < -1.0F) {
         var7 = -1.0F;
      }

      Quat4f var9;
      Quat4f var11;
      if (this.mouse.z == 0.0F) {
         var11 = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
      } else {
         GlUtil.getForwardVector(var4, (Transform)this.camera.getWorldTransform());
         var4.normalize();
         Matrix3f var8 = new Matrix3f(this.transformable.getWorldTransform().basis);
         var9 = new Quat4f();
         Quat4fTools.set(var8, var9);
         Matrix3f var10 = new Matrix3f(this.camera.getWorldTransform().basis);
         var9 = new Quat4f();
         Quat4fTools.set(var10, var9);
         float var16 = FastMath.atan2Fast(GlUtil.getUpVector(new Vector3f(), (Transform)this.transformable.getWorldTransform()).y, GlUtil.getRightVector(new Vector3f(), (Transform)this.transformable.getWorldTransform()).y) - 1.5707964F;
         float var14 = FastMath.atan2Fast(this.camera.getUp().y, this.camera.getRight().y) - 1.5707964F;
         if (Math.abs(FastMath.atan2(FastMath.sin(var16 - var14), FastMath.cos(var16 - var14))) < 1.5707663F) {
            var11 = new Quat4f(var4.x * FastMath.sin(var7), var4.y * FastMath.sin(var7), var4.z * FastMath.sin(var7), FastMath.cos(var7));
         } else {
            var11 = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
         }
      }

      this.rotation.set(var12);
      this.rotation.mul(var13);
      this.rotation.mul(var11);
      this.rotation.normalize();
      Quat4f var15;
      (var15 = new Quat4f()).conjugate(this.rotation);
      var9 = new Quat4f(this.camera.getForward().x, this.camera.getForward().y, this.camera.getForward().z, 0.0F);
      Quat4f var17;
      (var17 = new Quat4f()).mul(this.rotation, var9);
      var17.mul(var15);
      var1.x = var17.x;
      var1.y = var17.y;
      var1.z = var17.z;
      var1.normalize();
      var9 = new Quat4f(this.camera.getUp().x, this.camera.getUp().y, this.camera.getUp().z, 0.0F);
      var17.mul(this.rotation, var9);
      var17.mul(var15);
      var2.x = var17.x;
      var2.y = var17.y;
      var2.z = var17.z;
      var2.normalize();
      var9 = new Quat4f(this.camera.getRight().x, this.camera.getRight().y, this.camera.getRight().z, 0.0F);
      var17.mul(this.rotation, var9);
      var17.mul(var15);
      var3.x = var17.x;
      var3.y = var17.y;
      var3.z = var17.z;
      var3.normalize();
      if (this.limitS(var1, var2, var3, this.transformable.getWorldTransform())) {
         this.camera.setForward(var1);
         this.camera.setUp(var2);
         this.camera.setRight(var3);
      }

   }

   public Transform getFollowing() {
      return this.following;
   }

   public int getOrientation() {
      return this.orientation;
   }

   public void setOrientation(int var1) {
      this.orientation = var1;
   }

   private void getVectors(Vector3f var1, Vector3f var2, Vector3f var3, Transform var4) {
      switch(this.orientation) {
      case 0:
         GlUtil.getForwardVector(var1, var4);
         GlUtil.getUpVector(var2, var4);
         GlUtil.getRightVector(var3, var4);
         return;
      case 1:
         GlUtil.getForwardVector(var1, var4);
         GlUtil.getUpVector(var2, var4);
         GlUtil.getRightVector(var3, var4);
         var1.negate();
         var3.negate();
         return;
      case 2:
         GlUtil.getUpVector(var1, var4);
         GlUtil.getRightVector(var2, var4);
         GlUtil.getForwardVector(var3, var4);
         return;
      case 3:
         GlUtil.getBottomVector(var1, var4);
         GlUtil.getLeftVector(var2, var4);
         GlUtil.getForwardVector(var3, var4);
      default:
         return;
      case 4:
         GlUtil.getLeftVector(var1, var4);
         GlUtil.getUpVector(var2, var4);
         GlUtil.getForwardVector(var3, var4);
         return;
      case 5:
         GlUtil.getRightVector(var1, var4);
         GlUtil.getUpVector(var2, var4);
         GlUtil.getForwardVector(var3, var4);
      }
   }

   public boolean isCorrecting() {
      return this.correcting;
   }

   public void setCorrecting(boolean var1) {
      this.correcting = var1;
   }

   private boolean limitS(Vector3f var1, Vector3f var2, Vector3f var3, Transform var4) {
      var2 = new Vector3f();
      Vector3f var5 = new Vector3f();
      Vector3f var6 = new Vector3f();
      this.getVectors(var2, var5, var6, var4);
      var2 = new Vector3f(var2);
      var1.normalize();
      var3.normalize();
      if (var2.angle(var1) < this.limit) {
         return true;
      } else {
         this.mouse.scale(0.5F);
         if (this.iterations < 10) {
            ++this.iterations;
            this.freeRot();
         }

         return false;
      }
   }

   public void mouseRotate() {
      this.freeRot();
      if (this.isCorrecting()) {
         Transform var1;
         (var1 = new Transform(this.transformable.getWorldTransform())).basis.sub(this.start.basis);
         Vector3f var2 = new Vector3f();
         Vector3f var3 = new Vector3f();
         Vector3f var4 = new Vector3f();
         this.getVectors(var2, var3, var4, var1);
         GlUtil.setForwardVector(var2, var1);
         GlUtil.setUpVector(var3, var1);
         GlUtil.setRightVector(var4, var1);
         this.camera.getWorldTransform().basis.add(var1.basis);
         this.start.set(this.transformable.getWorldTransform());
      }

      this.followingOld.set(this.camera.getWorldTransform());
   }

   public void reset() {
      this.getFollowing().setIdentity();
      this.followingOld.setIdentity();
      this.start.set(this.transformable.getWorldTransform());
   }
}
