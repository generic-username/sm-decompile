package org.schema.game.client.view.camera;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.EditSegmentInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.graphicsengine.camera.viewer.FixedViewer;
import org.schema.schine.graphicsengine.core.Timer;

public class ShipOffsetCameraViewable extends FixedViewer {
   protected Vector3i posMod = new Vector3i();
   protected SegmentController controller;
   protected EditSegmentInterface edit;
   Transform tinv = new Transform();
   Vector3f dist = new Vector3f();
   private Vector3f posCur = new Vector3f();
   private float jumpToBlockSpeed = 13.0F;
   Vector3f offset = new Vector3f();

   public ShipOffsetCameraViewable(EditSegmentInterface var1) {
      super(var1.getSegmentController());
      this.controller = var1.getSegmentController();
      this.edit = var1;
   }

   public synchronized Vector3i getCurrentBlock() {
      Vector3i var1;
      (var1 = new Vector3i(this.posMod)).add(this.edit.getCore());
      return var1;
   }

   public float getJumpToBlockSpeed() {
      return this.jumpToBlockSpeed;
   }

   public void setJumpToBlockSpeed(float var1) {
      this.jumpToBlockSpeed = var1;
   }

   public Vector3f getPos() {
      Vector3f var1 = super.getPos();
      Vector3f var2 = this.getRelativeCubePos();
      this.tinv.set(this.getEntity().getWorldTransform());
      this.tinv.basis.transform(var2);
      var1.add(var2);
      return var1;
   }

   public void update(Timer var1) {
      this.dist.set((float)this.posMod.x, (float)this.posMod.y, (float)this.posMod.z);
      this.dist.sub(this.posCur);
      float var2;
      if ((var2 = this.dist.length()) > 0.0F) {
         float var3 = this.dist.length();
         this.dist.normalize();
         this.dist.scale(var1.getDelta() * Math.max(var3 * 3.0F, this.getJumpToBlockSpeed()));
         if (this.dist.length() < var2) {
            this.posCur.add(this.dist);
         } else {
            this.posCur.set((float)this.posMod.x, (float)this.posMod.y, (float)this.posMod.z);
         }
      }
   }

   public Vector3i getPosMod() {
      return this.posMod;
   }

   public Vector3f getRelativeCubePos() {
      return new Vector3f(this.offset.x + this.posCur.x + (float)this.edit.getCore().x - 16.0F, this.offset.y + this.posCur.y + (float)this.edit.getCore().y - 16.0F, this.offset.z + this.posCur.z + (float)this.edit.getCore().z - 16.0F);
   }

   public void jumpTo(Vector3i var1) {
      var1.sub(this.edit.getCore());
      this.posMod.set(var1);
   }

   public void jumpToInstantly(Vector3i var1) {
      var1.sub(this.edit.getCore());
      this.posMod.set(var1);
      this.posCur.set((float)this.posMod.x, (float)this.posMod.y, (float)this.posMod.z);
   }
}
