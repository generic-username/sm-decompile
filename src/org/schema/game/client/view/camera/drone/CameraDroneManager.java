package org.schema.game.client.view.camera.drone;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.util.Iterator;
import java.util.Map;
import org.schema.game.client.controller.SendableAddedRemovedListener;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.texture.Texture;
import org.schema.schine.network.objects.Sendable;

public class CameraDroneManager implements SendableAddedRemovedListener, Drawable {
   private Mesh droneMesh;
   private boolean init;
   private final Map drones = new Object2ObjectOpenHashMap();
   private final GameClientState state;

   public CameraDroneManager(GameClientState var1) {
      this.state = var1;
   }

   public void cleanUp() {
      this.state.getController().removeSendableAddedRemovedListener(this);
      this.init = false;
      this.droneMesh = null;
      this.drones.clear();
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      this.loadShader(this.droneMesh.getMaterial().getTexture(), ShaderLibrary.mineShader);
      this.droneMesh.loadVBO(true);
      Iterator var1 = this.drones.values().iterator();

      while(var1.hasNext()) {
         CameraDrone var2 = (CameraDrone)var1.next();
         GlUtil.glPushMatrix();
         var2.draw(this.droneMesh);
         GlUtil.glPopMatrix();
      }

      this.droneMesh.unloadVBO(true);
      this.unloadShader(this.droneMesh.getMaterial().getTexture(), ShaderLibrary.mineShader);
   }

   protected void loadShader(Texture var1, Shader var2) {
      var2.loadWithoutUpdate();
      GlUtil.glActiveTexture(33984);
      GlUtil.glEnable(3553);
      GlUtil.glBindTexture(3553, var1.getTextureId());
      GlUtil.updateShaderInt(var2, "diffuseMap", 0);
      GlUtil.updateShaderVector4f(var2, "tint", 1.0F, 1.0F, 1.0F, 1.0F);
   }

   protected void unloadShader(Texture var1, Shader var2) {
      var2.unloadWithoutExit();
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.droneMesh = (Mesh)Controller.getResLoader().getMesh("CameraDrone").getChilds().get(0);
      this.state.getController().addSendableAddedRemovedListener(this);
      Iterator var1 = this.state.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

      while(var1.hasNext()) {
         Sendable var2 = (Sendable)var1.next();
         this.onAddedSendable(var2);
      }

      this.init = true;
   }

   public void onAddedSendable(Sendable var1) {
      if (var1 instanceof PlayerState) {
         PlayerState var2 = (PlayerState)var1;
         this.drones.put(var2, new CameraDrone(var2));
      }

   }

   public void onRemovedSendable(Sendable var1) {
      if (var1 instanceof PlayerState) {
         PlayerState var2 = (PlayerState)var1;
         this.drones.remove(var2);
      }

   }
}
