package org.schema.game.client.view.camera;

import com.bulletphysics.collision.dispatch.CollisionWorld.ClosestRayResultCallback;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.game.common.data.physics.PhysicsExt;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.schine.common.InputHandler;
import org.schema.schine.graphicsengine.camera.Camera;
import org.schema.schine.graphicsengine.camera.look.AxialCameraLook;
import org.schema.schine.graphicsengine.camera.viewer.FixedViewer;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.KeyEventInterface;

public class PlayerCamera extends Camera implements InputHandler {
   public static boolean alignedContinously = true;
   boolean first = true;
   private GameClientState state;
   private PlayerCharacter character;
   private Transform stdTransform;
   private ClosestRayResultCallback callBack;
   private Transform align;

   public PlayerCamera(GameClientState var1, PlayerCharacter var2) {
      super(var1, new UpperFixedViewer(var2));
      this.setCharacter(var2);
      this.state = var1;
      this.stdTransform = new Transform();
      this.stdTransform.setIdentity();
      this.setLookAlgorithm(new AxialCameraLook(this));
   }

   public Vector3i getCameraCubeOffset() {
      return ((ShipOffsetCameraViewable)this.getViewable()).getPosMod();
   }

   public PlayerCharacter getCharacter() {
      return this.character;
   }

   public void setCharacter(PlayerCharacter var1) {
      this.character = var1;
      ((FixedViewer)this.getViewable()).setEntity(var1);
   }

   public Vector3i getCurrentBlock() {
      return ((ShipOffsetCameraViewable)this.getViewable()).getCurrentBlock();
   }

   public ClosestRayResultCallback getNearestIntersection(PlayerCharacter var1) {
      if (this.getCameraOffset() > 0.0F) {
         Vector3f var4 = new Vector3f(var1.getHeadWorldTransform().origin);
         Vector3f var2 = new Vector3f(this.getOffsetPos(new Vector3f()));
         CubeRayCastResult var3;
         (var3 = new CubeRayCastResult(var4, var2, false, new SegmentController[0])).setOnlyCubeMeshes(true);
         return ((PhysicsExt)this.state.getPhysics()).testRayCollisionPoint(var4, var2, var3, false);
      } else {
         return null;
      }
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      ((ShipOffsetScrollableCameraViewable)this.getViewable()).handleKeyEvent(var1);
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public void jumpTo(Vector3i var1) {
      ((ShipOffsetCameraViewable)this.getViewable()).jumpTo(var1);
   }

   public Matrix3f getExtraOrientationRotation() {
      return this.getCharacter().getCharacterController().getAddRoation();
   }

   protected int limitWheel(int var1) {
      return Math.max(0, Math.min(var1, 2500));
   }

   public void update(Timer var1, boolean var2) {
      if (!this.getCharacter().getGravity().isAligedOnly() && !this.getCharacter().getGravity().isGravityOn()) {
         this.align = null;
         ((AxialCameraLook)this.getLookAlgorithm()).getFollowing().set(this.stdTransform);
      } else if (this.getCharacter().getGravity().isGravityOn() || this.getCharacter().getGravity().isAligedOnly() && alignedContinously) {
         ((AxialCameraLook)this.getLookAlgorithm()).getFollowing().set(this.getCharacter().getGravity().source.getWorldTransform());
         this.align = null;
      } else {
         if (this.align == null) {
            this.align = new Transform(this.getCharacter().getGravity().source.getWorldTransform());
         }

         ((AxialCameraLook)this.getLookAlgorithm()).getFollowing().set(this.align);
      }

      super.update(var1, var2);
      this.callBack = this.getNearestIntersection(this.getCharacter());
      if (this.callBack != null && this.callBack.hasHit()) {
         Vector3f var3;
         (var3 = new Vector3f()).sub(this.getOffsetPos(new Vector3f()), this.callBack.hitPointWorld);
         var3.scale(1.01F);
         this.getWorldTransform().origin.sub(var3);
      }

   }
}
