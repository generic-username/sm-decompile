package org.schema.game.client.view.camera;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.EditSegmentInterface;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.controller.manager.ingame.SegmentBuildController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.common.InputHandler;
import org.schema.schine.graphicsengine.camera.Camera;
import org.schema.schine.graphicsengine.camera.TransitionCamera;
import org.schema.schine.graphicsengine.camera.look.AxialCameraLook;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.KeyEventInterface;

public class BuildShipCamera extends Camera implements SegmentControllerCamera, InputHandler {
   private GameClientState state;
   private SegmentController ship;
   private TransitionCamera transitionCamera;
   private Camera toReset;

   public BuildShipCamera(GameClientState var1, Camera var2, EditSegmentInterface var3, ShipMovableCameraViewable var4, Transform var5) {
      super(var3.getSegmentController().getState(), var4, var5);
      this.state = var1;
      this.ship = var3.getSegmentController();
      this.setLookAlgorithm(new AxialCameraLook(this));
      this.toReset = var2;
   }

   public BuildShipCamera(GameClientState var1, Camera var2, EditSegmentInterface var3, Vector3f var4, Transform var5) {
      this(var1, var2, var3, new ShipMovableCameraViewable(var3, var4), var5);
   }

   public BuildShipCamera(GameClientState var1, EditSegmentInterface var2) {
      this(var1, (Camera)null, var2, (Vector3f)SegmentBuildController.INITAL_BUILD_CAM_DIST, (Transform)null);
   }

   public SegmentController getSegmentController() {
      return this.ship;
   }

   public Vector3f getRelativeCubePos() {
      return ((ShipMovableCameraViewable)this.getViewable()).getRelativeCubePos();
   }

   public GameClientState getState() {
      return this.state;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      ((ShipMovableCameraViewable)this.getViewable()).handleKeyEvent(var1);
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public void jumpTo(Vector3i var1) {
      ((ShipMovableCameraViewable)this.getViewable()).jumpTo(var1);
   }

   public void jumpToInstantly(Vector3i var1) {
      ((ShipMovableCameraViewable)this.getViewable()).jumpToInstantly(new Vector3i(var1));
   }

   public void resetTransition(Camera var1) {
      this.setStable(false);
      this.transitionCamera = new TransitionCamera(var1, this, 0.2F);
   }

   public void update(Timer var1, boolean var2) {
      if (!PlayerInteractionControlManager.isAdvancedBuildMode(this.getState())) {
         ((AxialCameraLook)this.getLookAlgorithm()).getFollowing().set(this.ship.getWorldTransform());
         super.update(var1, var2);
      }

      if (this.transitionCamera != null && this.transitionCamera.isActive()) {
         this.setStable(false);
         this.transitionCamera.update(var1, var2);
         this.getWorldTransform().set(this.transitionCamera.getWorldTransform());
      } else {
         this.setStable(true);
      }

      if (this.toReset != null) {
         this.resetTransition(this.toReset);
         this.toReset = null;
      }

   }
}
