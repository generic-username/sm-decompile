package org.schema.game.client.view.character;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.PlayerSkin;
import org.schema.game.common.data.player.simplified.SimplifiedCharacter;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.animation.LoopMode;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndex;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndexElement;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.network.StateInterface;

public class DrawableSimplifiedHumanCharacter extends AbstractDrawableSimplifiedHumanCharacter {
   private Vector3f lastPos;
   private float lastMovingDist;

   public DrawableSimplifiedHumanCharacter(SimplifiedCharacter var1, Timer var2, GameClientState var3) {
      super(var1, var2, var3);
   }

   public void loadClientBones(StateInterface var1) {
   }

   protected void handleTextureUpdate(Mesh var1, SimplifiedCharacter var2) {
      var1.getSkin().setDiffuseTexId(Controller.getResLoader().getSprite("playertex").getMaterial().getTexture().getTextureId());
   }

   protected void handleState(Timer var1, AbstractOwnerState var2) {
      this.timer = var1;
      if (!this.handleSitting()) {
         boolean var5 = ((SimplifiedCharacter)this.getEntity()).getGravity().isGravityOn();
         boolean var6 = !((SimplifiedCharacter)this.getEntity()).onGround();
         Vector3f var3 = new Vector3f(((SimplifiedCharacter)this.getEntity()).getMovingDir());
         Vector3f var4;
         (var4 = new Vector3f()).sub(this.getWorldTransform().origin, this.lastPos);
         if (var4.length() > 0.0F) {
            this.lastMovingDist = var4.length();
         }

         if (var3.lengthSquared() > 0.0F) {
            var3.normalize();
            this.moveDirectionAnimation(var3, var5, var6, this.lastMovingDist);
         } else if (((SimplifiedCharacter)this.getEntity()).forcedAnimation != null) {
            ((SimplifiedCharacter)this.getEntity()).forcedAnimation.apply(this.getEntity());
         } else if (((SimplifiedCharacter)this.getEntity()).getConversationPartner() != null) {
            if (!this.isAnim(AnimationIndex.TALK_SALUTE)) {
               this.setAnim(AnimationIndex.TALK_SALUTE, 0.2F, LoopMode.LOOP);
            }
         } else {
            this.standDirectionAnimation(var5, var6);
         }

         this.lastPos = new Vector3f(this.getWorldTransform().origin);
      }
   }

   public Vector3f getForward() {
      return ((SimplifiedCharacter)this.getEntity()).getForward();
   }

   public Vector3f getUp() {
      return ((SimplifiedCharacter)this.getEntity()).getUp();
   }

   public Vector3f getRight() {
      return ((SimplifiedCharacter)this.getEntity()).getRight();
   }

   public Transform getWorldTransform() {
      return ((SimplifiedCharacter)this.getEntity()).getWorldTransform();
   }

   protected float getScale() {
      return 1.0F;
   }

   protected AnimationIndexElement getAnimationState() {
      return ((SimplifiedCharacter)this.getEntity()).getAnimationState();
   }

   public SimpleTransformableSendableObject getGravitySource() {
      return ((SimplifiedCharacter)this.getEntity()).getGravity() != null ? ((SimplifiedCharacter)this.getEntity()).getGravity().source : null;
   }

   protected PlayerSkin getPlayerSkin() {
      return ((SimplifiedCharacter)this.getEntity()).getSkin();
   }
}
