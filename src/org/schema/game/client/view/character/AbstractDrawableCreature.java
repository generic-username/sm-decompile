package org.schema.game.client.view.character;

import com.bulletphysics.linearmath.Transform;
import java.util.Collection;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.common.data.creature.CreaturePartNode;
import org.schema.game.common.data.player.PlayerSkin;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.animation.LoopMode;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndex;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndexElement;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationStructEndPoint;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.Skin;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.CreatureStructure;

public class AbstractDrawableCreature extends AbstractAnimatedObject implements BoneLocationInterface, DrawableCharacterInterface {
   private final Vector3f minAABB = new Vector3f();
   private final Vector3f maxAABB = new Vector3f();
   private CreaturePart creaturePart;
   private Vector3f localTranslation = new Vector3f();

   public AbstractDrawableCreature(AICreature var1, Timer var2, GameClientState var3) {
      super(var2, var1, var3);

      assert var1 != null;

   }

   public void cleanUp() {
   }

   public boolean isInvisible() {
      return false;
   }

   public void onRemove() {
   }

   public boolean isInClientRange() {
      return ((AICreature)this.getEntity()).isInClientRange();
   }

   public void update(Timer var1) {
      if (this.getBoneAttachable() != null) {
         this.handleState();
         Iterator var2 = this.holders.iterator();

         while(var2.hasNext()) {
            ((AnimatedObjectHoldAnimation)var2.next()).apply();
         }
      }

   }

   public int getId() {
      return 0;
   }

   public boolean isInFrustum() {
      if (this.timer == null) {
         return false;
      } else if (!this.isShadowMode()) {
         ((AICreature)this.creature).getPhysicsDataContainer().getShape().getAabb(((AICreature)this.creature).getWorldTransformOnClient(), this.minAABB, this.maxAABB);
         return Controller.getCamera().isAABBInFrustum(this.minAABB, this.maxAABB);
      } else {
         return true;
      }
   }

   public Vector3f getForward() {
      return ((AICreature)this.creature).getOwnerState().getForward(new Vector3f());
   }

   public Vector3f getUp() {
      return ((AICreature)this.creature).getOwnerState().getUp(new Vector3f());
   }

   public Vector3f getRight() {
      return ((AICreature)this.creature).getOwnerState().getRight(new Vector3f());
   }

   public void setForcedAnimation(CreatureStructure.PartType var1, AnimationIndexElement var2, boolean var3) throws AnimationNotSetException {
      CreaturePartNode var4 = ((AICreature)this.creature).getCreatureNode();
      this.creaturePart.setPartAnimForced(var1, var4, var2, var3);
   }

   public void setAnimSpeedForced(CreatureStructure.PartType var1, float var2, boolean var3) {
      CreaturePartNode var4 = ((AICreature)this.creature).getCreatureNode();
      this.creaturePart.setAnimSpeedForced(var1, var4, var2, var3);
   }

   public void setForcedLoopMode(CreatureStructure.PartType var1, LoopMode var2, boolean var3) {
      CreaturePartNode var4 = ((AICreature)this.creature).getCreatureNode();
      this.creaturePart.setForcedLoopMode(var1, var4, var2, var3);
   }

   public Vector3f getOrientationUp() {
      return ((AICreature)this.creature).getCharacterController().upAxisDirection[1];
   }

   public Vector3f getOrientationRight() {
      return ((AICreature)this.creature).getCharacterController().upAxisDirection[0];
   }

   public Vector3f getOrientationForward() {
      return ((AICreature)this.creature).getCharacterController().upAxisDirection[2];
   }

   public Transform getWorldTransform() {
      return ((AICreature)this.creature).getWorldTransformOnClient();
   }

   protected float getScale() {
      return ((AICreature)this.creature).getScale();
   }

   protected AnimationIndexElement getAnimationState() {
      return ((AICreature)this.creature).getAnimationState();
   }

   public SimpleTransformableSendableObject getGravitySource() {
      return ((AICreature)this.getEntity()).getGravity().source;
   }

   protected PlayerSkin getPlayerSkin() {
      return null;
   }

   protected Vector3f getLocalTranslation() {
      return this.localTranslation;
   }

   public Skin getSkin() {
      assert ((AICreature)this.creature).getCreatureNode() != null : (AICreature)this.creature;

      assert Controller.getResLoader().getMesh(((AICreature)this.creature).getCreatureNode().meshName) != null : "not found " + ((AICreature)this.creature).getCreatureNode().meshName;

      Mesh var1 = (Mesh)Controller.getResLoader().getMesh(((AICreature)this.creature).getCreatureNode().meshName).getChilds().get(0);

      assert var1.getSkin() != null : "skin not found " + ((AICreature)this.creature).getCreatureNode().meshName;

      return var1.getSkin();
   }

   public BoneAttachable initBoneAttachable(AICreature var1) {
      assert var1 != null;

      CreaturePartNode var2 = var1.getCreatureNode();

      assert var2 != null;

      Mesh var3 = Controller.getResLoader().getMesh(var2.meshName);

      assert var3 != null : var2.meshName;

      this.creaturePart = new CreaturePart((GameClientState)var1.getState(), (Mesh)var3.getChilds().get(0), this);
      this.creaturePart.createHirachy(var1);
      return this.creaturePart;
   }

   public AnimationStructEndPoint getAnimation(AnimationIndexElement var1) {
      return var1.get(this.creaturePart.getAnimations());
   }

   public void inititalizeHolders(Collection var1) {
   }

   protected void handleState() {
      if (((AICreature)this.getEntity()).isAttacking()) {
         if (!this.isAnim(AnimationIndex.UPPERBODY_BAREHAND_MEELEE)) {
            this.setAnim(AnimationIndex.UPPERBODY_BAREHAND_MEELEE, LoopMode.CYCLE);
            return;
         }
      } else if (this.isAnim(AnimationIndex.UPPERBODY_BAREHAND_MEELEE)) {
         this.setAnim(AnimationIndex.IDLING_FLOATING);
      }

   }

   public CreatureStructure getCreatureStructure() {
      return this.state.getResourceMap().get(((AICreature)this.creature).getCreatureNode().meshName).creature;
   }

   public String getRootBoneName() {
      assert this.getSkin() != null : (AICreature)this.creature;

      assert this.getSkin().getSkeleton() != null : (AICreature)this.creature;

      assert this.getSkin().getSkeleton().getRootBone() != null : (AICreature)this.creature;

      return this.getSkin().getSkeleton().getRootBone().name;
   }

   public String getRootTorsoBoneName() {
      return this.getSkin().getSkeleton().getRootBone().name;
   }

   public void setForcedAnimation(CreatureStructure.PartType var1, AnimationIndexElement var2) throws AnimationNotSetException {
      this.setForcedAnimation(var1, var2, true);
   }

   public void setAnimSpeedForced(CreatureStructure.PartType var1, float var2) {
      this.setAnimSpeedForced(var1, var2, true);
   }

   public void setForcedLoopMode(CreatureStructure.PartType var1, LoopMode var2) {
      this.setForcedLoopMode(var1, var2, true);
   }

   public void loadClientBones(StateInterface var1) {
   }
}
