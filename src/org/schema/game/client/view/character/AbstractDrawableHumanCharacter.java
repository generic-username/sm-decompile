package org.schema.game.client.view.character;

import java.util.Collection;
import java.util.Iterator;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.client.view.effects.ConstantIndication;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.MetaObjectState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.element.meta.weapon.Weapon;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.graphicsengine.animation.LoopMode;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndex;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndexElement;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationStructEndPoint;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationStructure;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Bone;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.Skin;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.Mouse;
import org.schema.schine.resource.CreatureStructure;

public abstract class AbstractDrawableHumanCharacter extends AbstractAnimatedObject implements DrawableCharacterInterface {
   private final Vector3f minAABB = new Vector3f();
   private final Vector3f maxAABB = new Vector3f();
   protected boolean lastState;
   protected boolean lastBack;
   private Vector3f cPos = new Vector3f();
   private Vector3f localTranslation = new Vector3f();

   public AbstractDrawableHumanCharacter(AbstractCharacter var1, Timer var2, GameClientState var3) {
      super(var2, var1, var3);
      this.setPlayerCharacter(var1);
      this.indication = new ConstantIndication(var1.getWorldTransform(), this.getPlayerName() + " (" + var3.getFactionString() + ")");
      this.getBoneAttachable().setSpeed(2.0F);
   }

   public void setForcedAnimation(CreatureStructure.PartType var1, AnimationIndexElement var2, boolean var3) throws AnimationNotSetException {
      if (var3) {
         this.setAnim(var2);
      } else {
         this.setAnimTorso(var2);
      }
   }

   public void setAnimSpeedForced(CreatureStructure.PartType var1, float var2, boolean var3) {
      if (var3) {
         this.setAnimSpeed(var2);
      } else {
         this.setAnimTorsoSpeed(var2);
      }
   }

   public boolean isInClientRange() {
      return ((AbstractCharacter)this.getEntity()).isInClientRange();
   }

   public void setForcedLoopMode(CreatureStructure.PartType var1, LoopMode var2, boolean var3) {
      if (var3) {
         this.setLoopMode(var2);
      } else {
         this.setLoopModeTorso(var2);
      }
   }

   public void initManualAttachments() {
      this.attach(this.getRightHolster(), "Pistol");
   }

   protected Matrix3f getYawRotation() {
      if (this.getOwnerState().isSitting()) {
         Matrix3f var1;
         (var1 = new Matrix3f()).setIdentity();
         Vector3i var2;
         (var2 = new Vector3i()).sub(this.getOwnerState().sittingPosTo, this.getOwnerState().sittingPos);
         Vector3i var3;
         (var3 = new Vector3i()).sub(this.getOwnerState().sittingPosTo, this.getOwnerState().sittingPosLegs);
         Vector3f var4;
         GlUtil.setUpVector(var4 = new Vector3f(0.0F, 1.0F, 0.0F), var1);
         Vector3f var5 = new Vector3f();
         Element.getRelativeForward(Element.getSide(var3), Element.getSide(var2), var5);
         GlUtil.setForwardVector(var5, var1);
         Vector3f var6;
         (var6 = new Vector3f()).cross(var4, var5);
         GlUtil.setRightVector(var6, var1);
         return var1;
      } else {
         return super.getYawRotation();
      }
   }

   protected boolean skipYawRotation() {
      return this.getOwnerState().isSitting() || this.getOwnerState() == this.state.getPlayer() && this.getOwnerState() instanceof PlayerState && (Mouse.isButtonDown(2) || Keyboard.isKeyDown(54));
   }

   public Vector3f getOrientationUp() {
      return this.getPlayerCharacter().getCharacterController().upAxisDirection[1];
   }

   public Vector3f getOrientationRight() {
      return this.getPlayerCharacter().getCharacterController().upAxisDirection[0];
   }

   public Vector3f getOrientationForward() {
      return this.getPlayerCharacter().getCharacterController().upAxisDirection[2];
   }

   public void draw() {
      if (this.getOwnerState() != null && this.timer != null) {
         super.draw();
      }
   }

   protected Vector3f getLocalTranslation() {
      this.localTranslation.y = this.getPlayerCharacter().getCharacterHeightOffset();
      return this.localTranslation;
   }

   public Skin getSkin() {
      return ((Mesh)Controller.getResLoader().getMesh("PlayerMdl").getChilds().get(0)).getSkin();
   }

   public BoneAttachable initBoneAttachable(AbstractCharacter var1) {
      return new BoneAttachable((Mesh)Controller.getResLoader().getMesh("PlayerMdl").getChilds().get(0), this.state, this);
   }

   public AnimationStructEndPoint getAnimation(AnimationIndexElement var1) {
      Mesh var2 = (Mesh)Controller.getResLoader().getMesh("PlayerMdl").getChilds().get(0);
      AnimationStructure var3 = this.state.getResourceMap().get(var2.getParent().getName()).animation;

      assert !var1.get(var3).animations[0].equals("default") : var1.get(var3).getClass().getSimpleName();

      return var1.get(var3);
   }

   public void inititalizeHolders(Collection var1) {
      var1.add(new AnimatedObjectHoldAnimation(this, AnimationIndex.UPPERBODY_FABRICATOR_DRAW, AnimationIndex.UPPERBODY_FABRICATOR_IDLE, AnimationIndex.UPPERBODY_FABRICATOR_AWAY, "Fabricator", this.getRightForeArm(), (Bone)null) {
         public boolean checkCondition() {
            return AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getInventory().getType(AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getSelectedBuildSlot()) > 0 || AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().isHarvestingButton();
         }
      });
      var1.add(new AnimatedObjectHoldAnimation(this, AnimationIndex.UPPERBODY_GUN_DRAW, AnimationIndex.UPPERBODY_GUN_IDLE, AnimationIndex.UPPERBODY_GUN_AWAY, "Pistol", this.getRightHand(), this.getRightHolster()) {
         public boolean checkCondition() {
            if (AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getInventory().getType(AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getSelectedBuildSlot()) == MetaObjectManager.MetaObjectType.WEAPON.type) {
               int var1 = AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getInventory().getMeta(AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getSelectedBuildSlot());
               MetaObject var2;
               Weapon var3;
               if ((var2 = ((MetaObjectState)AbstractDrawableHumanCharacter.this.getPlayerCharacter().getState()).getMetaObjectManager().getObject(var1)) != null && var2 instanceof Weapon && (var3 = (Weapon)var2).getSubObjectType() != Weapon.WeaponSubType.MARKER && var3.getSubObjectType() != Weapon.WeaponSubType.HEAL && var3.getSubObjectType() != Weapon.WeaponSubType.TORCH && var3.getSubObjectType() != Weapon.WeaponSubType.POWER_SUPPLY && var3.getSubObjectType() != Weapon.WeaponSubType.GRAPPLE) {
                  return true;
               }
            }

            return false;
         }
      });
      var1.add(new AnimatedObjectHoldAnimation(this, AnimationIndex.UPPERBODY_GUN_DRAW, AnimationIndex.UPPERBODY_GUN_IDLE, AnimationIndex.UPPERBODY_GUN_AWAY, "Torch", this.getRightHand(), this.getRightHolster()) {
         public boolean checkCondition() {
            if (AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getInventory().getType(AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getSelectedBuildSlot()) == MetaObjectManager.MetaObjectType.WEAPON.type) {
               int var1 = AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getInventory().getMeta(AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getSelectedBuildSlot());
               MetaObject var2;
               if ((var2 = ((MetaObjectState)AbstractDrawableHumanCharacter.this.getPlayerCharacter().getState()).getMetaObjectManager().getObject(var1)) != null && var2 instanceof Weapon && ((Weapon)var2).getSubObjectType() == Weapon.WeaponSubType.TORCH) {
                  return true;
               }
            }

            return false;
         }
      });
      var1.add(new AnimatedObjectHoldAnimation(this, AnimationIndex.UPPERBODY_GUN_DRAW, AnimationIndex.UPPERBODY_GUN_IDLE, AnimationIndex.UPPERBODY_GUN_AWAY, "PowerSupplyBeam", this.getRightHand(), this.getRightHolster()) {
         public boolean checkCondition() {
            if (AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getInventory().getType(AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getSelectedBuildSlot()) == MetaObjectManager.MetaObjectType.WEAPON.type) {
               int var1 = AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getInventory().getMeta(AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getSelectedBuildSlot());
               MetaObject var2;
               if ((var2 = ((MetaObjectState)AbstractDrawableHumanCharacter.this.getPlayerCharacter().getState()).getMetaObjectManager().getObject(var1)) != null && var2 instanceof Weapon && ((Weapon)var2).getSubObjectType() == Weapon.WeaponSubType.POWER_SUPPLY) {
                  return true;
               }
            }

            return false;
         }
      });
      var1.add(new AnimatedObjectHoldAnimation(this, AnimationIndex.UPPERBODY_GUN_DRAW, AnimationIndex.UPPERBODY_GUN_IDLE, AnimationIndex.UPPERBODY_GUN_AWAY, "HealingBeam", this.getRightHand(), this.getRightHolster()) {
         public boolean checkCondition() {
            if (AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getInventory().getType(AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getSelectedBuildSlot()) == MetaObjectManager.MetaObjectType.WEAPON.type) {
               int var1 = AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getInventory().getMeta(AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getSelectedBuildSlot());
               MetaObject var2;
               if ((var2 = ((MetaObjectState)AbstractDrawableHumanCharacter.this.getPlayerCharacter().getState()).getMetaObjectManager().getObject(var1)) != null && var2 instanceof Weapon && ((Weapon)var2).getSubObjectType() == Weapon.WeaponSubType.HEAL) {
                  return true;
               }
            }

            return false;
         }
      });
      var1.add(new AnimatedObjectHoldAnimation(this, AnimationIndex.UPPERBODY_GUN_DRAW, AnimationIndex.UPPERBODY_GUN_IDLE, AnimationIndex.UPPERBODY_GUN_AWAY, "MarkerBeam", this.getRightHand(), this.getRightHolster()) {
         public boolean checkCondition() {
            if (AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getInventory().getType(AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getSelectedBuildSlot()) == MetaObjectManager.MetaObjectType.WEAPON.type) {
               int var1 = AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getInventory().getMeta(AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getSelectedBuildSlot());
               MetaObject var2;
               if ((var2 = ((MetaObjectState)AbstractDrawableHumanCharacter.this.getPlayerCharacter().getState()).getMetaObjectManager().getObject(var1)) != null && var2 instanceof Weapon && ((Weapon)var2).getSubObjectType() == Weapon.WeaponSubType.MARKER) {
                  return true;
               }
            }

            return false;
         }
      });
      var1.add(new AnimatedObjectHoldAnimation(this, AnimationIndex.UPPERBODY_GUN_DRAW, AnimationIndex.UPPERBODY_GUN_IDLE, AnimationIndex.UPPERBODY_GUN_AWAY, "GrappleBeam", this.getRightHand(), this.getRightHolster()) {
         public boolean checkCondition() {
            if (AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getInventory().getType(AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getSelectedBuildSlot()) == MetaObjectManager.MetaObjectType.WEAPON.type) {
               int var1 = AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getInventory().getMeta(AbstractDrawableHumanCharacter.this.getPlayerCharacter().getOwnerState().getSelectedBuildSlot());
               MetaObject var2;
               if ((var2 = ((MetaObjectState)AbstractDrawableHumanCharacter.this.getPlayerCharacter().getState()).getMetaObjectManager().getObject(var1)) != null && var2 instanceof Weapon && ((Weapon)var2).getSubObjectType() == Weapon.WeaponSubType.GRAPPLE) {
                  return true;
               }
            }

            return false;
         }
      });
   }

   protected void handleState() {
      this.handleState(this.timer, (AbstractCharacter)this.getEntity());
   }

   public CreatureStructure getCreatureStructure() {
      return this.state.getResourceMap().get("PlayerMdl").creature;
   }

   public void cleanUp() {
   }

   public boolean isInvisible() {
      if (!this.isShadowMode() && this.getPlayerCharacter() == this.state.getCharacter()) {
         this.cPos.set(Controller.getCamera().getPos());
         this.cPos.sub(this.getPlayerCharacter().getHeadWorldTransform().origin);
         if (this.cPos.length() < 0.2F) {
            return true;
         }
      }

      return this.getPlayerCharacter().isHidden() || this.getOwnerState().isInvisibilityMode();
   }

   protected abstract void handleTextureUpdate(Mesh var1, AbstractCharacter var2);

   protected String getPlayerName() {
      return this.getOwnerState() == null ? "<nobody>" : this.getOwnerState().getName();
   }

   public void onRemove() {
      HudIndicatorOverlay.toDrawTexts.remove(this.indication);
   }

   public void update(Timer var1) {
      try {
         if (this.getBoneAttachable() != null) {
            this.handleState(var1, (AbstractCharacter)this.getEntity());
            Iterator var3 = this.holders.iterator();

            while(var3.hasNext()) {
               ((AnimatedObjectHoldAnimation)var3.next()).apply();
            }
         }

      } catch (IllegalArgumentException var2) {
         System.err.println(var2.getMessage());
      }
   }

   public AbstractOwnerState getOwnerState() {
      return ((AbstractCharacter)this.getEntity()).getOwnerState();
   }

   public int getId() {
      return this.getPlayerCharacter().getId();
   }

   public boolean isInFrustum() {
      if (this.timer != null && this.getOwnerState() != null) {
         if (!this.isShadowMode()) {
            this.getPlayerCharacter().getPhysicsDataContainer().getShape().getAabb(this.getPlayerCharacter().getWorldTransformOnClient(), this.minAABB, this.maxAABB);
            return Controller.getCamera().isAABBInFrustum(this.minAABB, this.maxAABB);
         } else {
            return true;
         }
      } else {
         return false;
      }
   }

   protected abstract void handleState(Timer var1, AbstractCharacter var2);

   public abstract AbstractCharacter getPlayerCharacter();

   public abstract void setPlayerCharacter(AbstractCharacter var1);

   public boolean handleSitting() {
      if (!this.getOwnerState().isSitting()) {
         return false;
      } else {
         boolean var1 = false;
         boolean var2 = false;
         if (((AbstractCharacter)this.getEntity()).getGravity().source != null && ((AbstractCharacter)this.getEntity()).getGravity().source instanceof SegmentController) {
            SegmentPiece var3;
            var1 = (var3 = ((SegmentController)((AbstractCharacter)this.getEntity()).getGravity().source).getSegmentBuffer().getPointUnsave(this.getOwnerState().sittingPosLegs)) != null && var3.getType() != 0;
            SegmentPiece var4;
            var2 = (var4 = ((SegmentController)((AbstractCharacter)this.getEntity()).getGravity().source).getSegmentBuffer().getPointUnsave(this.getOwnerState().sittingPos)) != null && var4.getType() != 0 && ElementKeyMap.getInfo(var4.getType()).getBlockStyle() == BlockStyle.WEDGE;
         }

         if (var2) {
            if (var1) {
               if (!this.isAnim(AnimationIndex.SITTING_WEDGE_FLOOR_IDLE)) {
                  this.setAnim(AnimationIndex.SITTING_WEDGE_FLOOR_IDLE, 0.5F);
               }
            } else if (!this.isAnim(AnimationIndex.SITTING_WEDGE_NOFLOOR_IDLE)) {
               this.setAnim(AnimationIndex.SITTING_WEDGE_NOFLOOR_IDLE, 0.5F);
            }
         } else if (var1) {
            if (!this.isAnim(AnimationIndex.SITTING_BLOCK_FLOOR_IDLE)) {
               this.setAnim(AnimationIndex.SITTING_BLOCK_FLOOR_IDLE, 0.5F);
            }
         } else if (!this.isAnim(AnimationIndex.SITTING_BLOCK_NOFLOOR_IDLE)) {
            this.setAnim(AnimationIndex.SITTING_BLOCK_NOFLOOR_IDLE, 0.5F);
         }

         return true;
      }
   }
}
