package org.schema.game.client.view.character;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.player.PlayerSkin;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.JoystickAxisMapping;
import org.schema.schine.graphicsengine.animation.LoopMode;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndex;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndexElement;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.input.JoystickMappingFile;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.StateInterface;

public class DrawableHumanCharacterNew extends AbstractDrawableHumanCharacter {
   float lastMovingDist;
   private Vector3f lastPos = new Vector3f();

   public DrawableHumanCharacterNew(PlayerCharacter var1, Timer var2, GameClientState var3) {
      super(var1, var2, var3);
   }

   private void checkHelmet(PlayerState var1) {
      boolean var2 = this.isAttached(this.getHeadBone(), "Helmet");
      boolean var3;
      float var4;
      if ((var3 = var1.getHelmetSlot() >= 0 && var1.getInventory((Vector3i)null).getType(var1.getHelmetSlot()) == MetaObjectManager.MetaObjectType.HELMET.type) && !this.isAnimTorso(AnimationIndex.HELMET_ON) && !var2) {
         if (!this.isAnimTorsoActive()) {
            this.setAnimTorso(AnimationIndex.HELMET_ON, LoopMode.DONT_LOOP_DEACTIVATE);
            this.attach(this.getHeldBone(), "Helmet");
         }

         if (this.isAnimTorsoActive() && this.isAnimTorso(AnimationIndex.HELMET_OFF)) {
            var4 = this.getAnimTorsoTime();
            this.setAnimTorso(AnimationIndex.HELMET_ON, LoopMode.DONT_LOOP_DEACTIVATE);
            this.setAnimTorsoTime(this.getAnimTorsoMaxTime() - var4);
            this.attach(this.getHeldBone(), "Helmet");
            return;
         }
      } else if (!var3 && !this.isAnimTorso(AnimationIndex.HELMET_OFF) && (this.isAnimTorso(AnimationIndex.HELMET_ON) || var2)) {
         if (this.isAnimTorso(AnimationIndex.HELMET_ON)) {
            var4 = this.getAnimTorsoTime();
            this.setAnimTorso(AnimationIndex.HELMET_OFF, LoopMode.DONT_LOOP_DEACTIVATE);
            this.setAnimTorsoTime(this.getAnimTorsoMaxTime() - var4);
            this.detach(this.getHeadBone(), "Helmet");
            this.attach(this.getHeldBone(), "Helmet");
            return;
         }

         this.setAnimTorso(AnimationIndex.HELMET_OFF, LoopMode.DONT_LOOP_DEACTIVATE);
         this.detach(this.getHeadBone(), "Helmet");
         this.attach(this.getHeldBone(), "Helmet");
      }

   }

   protected void handleTextureUpdate(Mesh var1, PlayerCharacter var2) {
      PlayerState var3 = var2.getOwnerState();
      var1.currentTexCoordSet = var3.getNetworkObject().playerFaceId.get();
      var1.getSkin().setDiffuseTexId(Controller.getResLoader().getSprite("playertex").getMaterial().getTexture().getTextureId());
   }

   protected void handleState(Timer var1, PlayerCharacter var2) {
      PlayerState var11 = var2.getOwnerState();
      if (!this.handleSitting()) {
         try {
            var11.isKeyDownOrSticky(KeyboardMappings.FORWARD);
            var11.isKeyDownOrSticky(KeyboardMappings.BACKWARDS);
            var11.isKeyDownOrSticky(KeyboardMappings.STRAFE_LEFT);
            var11.isKeyDownOrSticky(KeyboardMappings.STRAFE_RIGHT);
            var11.isKeyDownOrSticky(KeyboardMappings.UP);
            var11.isKeyDownOrSticky(KeyboardMappings.DOWN);
            this.state.getController().getJoystick();
            if (JoystickMappingFile.ok()) {
               this.state.getController().getJoystickAxis(JoystickAxisMapping.FORWARD_BACK);
               this.state.getController().getJoystickAxis(JoystickAxisMapping.RIGHT_LEFT);
            }

            this.checkHelmet(var11);
            if (((PlayerCharacter)this.getEntity()).getProhibitingBuildingAroundOrigin() > 0.0F) {
               if (!this.isAttachedAnything(this.getLeftHolster())) {
                  this.attach(this.getLeftHolster(), "BuildInhibitor");
               }
            } else if (this.isAttached(this.getLeftHolster(), "BuildInhibitor")) {
               this.detach(this.getLeftHolster(), "BuildInhibitor");
            }

            var11.isKeyDownOrSticky(KeyboardMappings.BACKWARDS);
            Vector3f var3 = var11.getUp(new Vector3f());
            Vector3f var4;
            (var4 = new Vector3f(var3)).scale(-1.0F);
            Vector3f var5 = var11.getRight(new Vector3f());
            Vector3f var6 = new Vector3f(var5);
            var5.scale(-1.0F);
            Vector3f var7 = var11.getForward(new Vector3f());
            Vector3f var8;
            (var8 = new Vector3f(var7)).scale(-1.0F);
            Vector3f var9 = new Vector3f();
            if (var11.isKeyDownOrSticky(KeyboardMappings.FORWARD)) {
               var9.add(var7);
            }

            if (var11.isKeyDownOrSticky(KeyboardMappings.BACKWARDS)) {
               var9.add(var8);
            }

            if (var11.isKeyDownOrSticky(KeyboardMappings.STRAFE_LEFT)) {
               var9.add(var6);
            }

            if (var11.isKeyDownOrSticky(KeyboardMappings.STRAFE_RIGHT)) {
               var9.add(var5);
            }

            if (var11.isKeyDownOrSticky(KeyboardMappings.UP)) {
               var9.add(var3);
            }

            if (var11.isKeyDownOrSticky(KeyboardMappings.DOWN)) {
               var9.add(var4);
            }

            var11.handleJoystickDir(var9, var7, var5, var3);
            boolean var12 = ((PlayerCharacter)this.creature).getGravity().isGravityOn();
            boolean var13 = !((PlayerCharacter)this.creature).getCharacterController().onGround();
            (var5 = new Vector3f()).sub(this.getWorldTransform().origin, this.lastPos);
            if (var5.length() > 0.0F) {
               this.lastMovingDist = var5.length();
            }

            if (var9.lengthSquared() > 0.0F) {
               this.moveDirectionAnimation(var9, var12, var13, this.lastMovingDist);
            } else {
               this.standDirectionAnimation(var12, var13);
            }

            this.lastPos = new Vector3f(this.getWorldTransform().origin);
            this.timer = var1;
            this.indication.setText(this.getPlayerName() + var11.getFactionController().getFactionString());
         } catch (Exception var10) {
            System.err.println("DRAWABLE CHARACTER UPDATE FAILED: " + var10.getClass().getSimpleName() + ": " + var10.getMessage() + "; PlayerState: " + var11);
            var10.printStackTrace();
         }
      }
   }

   public PlayerCharacter getPlayerCharacter() {
      return (PlayerCharacter)this.creature;
   }

   public void setPlayerCharacter(PlayerCharacter var1) {
      this.creature = var1;
   }

   public Vector3f getForward() {
      return this.getPlayerCharacter().getOwnerState().getForward(new Vector3f());
   }

   public Vector3f getUp() {
      return this.getPlayerCharacter().getOwnerState().getUp(new Vector3f());
   }

   public Vector3f getRight() {
      return this.getPlayerCharacter().getOwnerState().getRight(new Vector3f());
   }

   public Transform getWorldTransform() {
      return this.getPlayerCharacter().getWorldTransformOnClient();
   }

   protected float getScale() {
      return 1.0F;
   }

   protected AnimationIndexElement getAnimationState() {
      return this.getPlayerCharacter().getAnimationState();
   }

   public SimpleTransformableSendableObject getGravitySource() {
      return this.getPlayerCharacter().getGravity().source;
   }

   public boolean isOwnPlayerCharacter() {
      return this.getPlayerCharacter().isClientOwnObject();
   }

   protected boolean tiltHead() {
      return !this.isAnimTorso(AnimationIndex.HELMET_ON) && !this.isAnimTorso(AnimationIndex.HELMET_OFF);
   }

   protected PlayerSkin getPlayerSkin() {
      return ((PlayerCharacter)this.creature).getOwnerState().getSkinManager().getTextureId();
   }

   public void loadClientBones(StateInterface var1) {
   }
}
