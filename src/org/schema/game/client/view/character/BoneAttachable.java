package org.schema.game.client.view.character;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;
import java.util.Iterator;
import java.util.Map.Entry;
import javax.vecmath.Vector4f;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerSkin;
import org.schema.schine.graphicsengine.animation.AnimationChannel;
import org.schema.schine.graphicsengine.animation.AnimationController;
import org.schema.schine.graphicsengine.animation.LoopMode;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndexElement;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationStructure;
import org.schema.schine.graphicsengine.animation.structure.classes.Moving;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Bone;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.texture.Texture;
import org.schema.schine.input.Keyboard;

public class BoneAttachable {
   protected final GameClientState state;
   final Mesh mesh;
   private final Object2ObjectArrayMap attachments = new Object2ObjectArrayMap();
   protected AnimationIndexElement animationState;
   protected AnimationIndexElement animationTorsoState;
   protected AnimationController controller;
   protected AnimationChannel channel;
   protected AnimationChannel channelTorso;
   protected long animationStarted;
   private Texture texture;
   private float speed = 4.0F;
   private Texture emissiveTexture;

   public BoneAttachable(Mesh var1, GameClientState var2, BoneLocationInterface var3) {
      this.state = var2;

      assert var2 != null;

      this.mesh = var1;
      this.setTexture(var1.getMaterial().getTexture());
      this.setEmissiveTexture(var1.getMaterial().getEmissiveTexture());
      if (var1.getSkin() != null) {
         this.controller = new AnimationController(var1.getSkin());
         this.channel = this.controller.createChannel();
         this.channelTorso = this.controller.createChannel();
         if (var3.getRootBoneName() == null) {
            var3.loadClientBones(var2);
         }

         assert var3.getRootBoneName() != null : var3 + "; " + var1;

         this.channel.addFromRootBone(var3.getRootBoneName());
         this.channelTorso.addFromRootBone(var3.getRootTorsoBoneName());
         if (var3.getHeldBoneName() != null && !var3.getHeldBoneName().equals("none")) {
            this.channelTorso.addBone(var3.getHeldBoneName());
         }

         this.channelTorso.setOverwritePreviousAnimation(true);
         var3.initializeListeners(this.controller, this.channel, this.channelTorso);
      }

   }

   public void setAnim(AnimationIndexElement var1, int var2, float var3) {
      assert var1.get(this.getAnimations()) != null : var1 + "; " + this.getAnimations();

      AnimationState var5 = new AnimationState(var1.get(this.getAnimations()), var2);

      try {
         this.channel.setAnim(var5.getAnimation(), var3);
         this.animationStarted = System.currentTimeMillis();
         this.animationState = var1;
         if (this.animationState.isType(Moving.class)) {
            this.channel.setSpeed(this.getSpeed());
            this.channelTorso.setSpeed(this.getSpeed());
         } else {
            this.channel.setSpeed(1.0F);
            this.channelTorso.setSpeed(1.0F);
         }
      } catch (RuntimeException var4) {
         var4.printStackTrace();
         System.err.println("EXCEPTION ON ANIM");

         assert this.mesh != null;

         assert var5.state != null;

         System.err.println("EXCEPTION WHEN SETTING: " + var5.state.getClass().getSimpleName() + ": " + var5.state.animations[var5.anim] + " in " + this.mesh.getName());
         throw var4;
      }
   }

   public void setAnim(AnimationIndexElement var1, int var2, float var3, LoopMode var4) {
      AnimationState var5 = new AnimationState(var1.get(this.getAnimations()), var2);
      this.channel.setAnim(var5.getAnimation(), var3);
      this.animationStarted = System.currentTimeMillis();
      this.channel.setLoopMode(var4);
      this.animationState = var1;
   }

   public void setAnimTorso(AnimationIndexElement var1, int var2, float var3) {
      AnimationState var4 = new AnimationState(var1.get(this.getAnimations()), var2);
      this.channelTorso.setAnim(var4.getAnimation(), var3);
      this.animationTorsoState = var1;
   }

   public void setAnimTorso(AnimationIndexElement var1, int var2, float var3, LoopMode var4) {
      AnimationState var5 = new AnimationState(var1.get(this.getAnimations()), var2);
      this.channelTorso.setAnim(var5.getAnimation(), var3);
      this.channelTorso.setLoopMode(var4);
      this.animationTorsoState = var1;
   }

   public boolean isAnimTorso(AnimationIndexElement var1) {
      return this.channelTorso.isActive() && this.animationTorsoState == var1;
   }

   public boolean isAnim(AnimationIndexElement var1) {
      return this.channel.isActive() && this.animationState == var1;
   }

   public void draw(Timer var1, PlayerSkin var2, Vector4f var3) {
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      this.applyTexture(this.mesh, var2);
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      this.updateController(var1);
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      this.mesh.getSkin().spotActivated = this.state.getWorldDrawer().isSpotLightSupport() == 0;
      this.mesh.getSkin().color.set(var3);
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      this.mesh.drawVBO(false);
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      this.drawAttached(var1, var2, var3);
   }

   public void drawAttached(Timer var1, PlayerSkin var2, Vector4f var3) {
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      Iterator var4 = this.attachments.entrySet().iterator();

      while(var4.hasNext()) {
         Entry var5;
         BoneAttachable var6;
         Mesh var7 = (var6 = (BoneAttachable)(var5 = (Entry)var4.next()).getValue()).mesh;
         Bone var10 = (Bone)var5.getKey();
         GlUtil.glPushMatrix();
         Transform var8 = new Transform();
         Matrix4f var9;
         if (var7.getSkin() != null) {
            var7.getSkin().getSkeleton().updateAttachment(var1, var10);
            var8.origin.set(var7.getSkin().getSkeleton().getRootBone().worldPos);
            var8.basis.set(var7.getSkin().getSkeleton().getRootBone().worldRot);
            var9 = new Matrix4f();
            Controller.getMat(var8, var9);
            var9.scale(new Vector3f(var7.getSkin().getSkeleton().getRootBone().worldScale.x, var7.getSkin().getSkeleton().getRootBone().worldScale.y, var7.getSkin().getSkeleton().getRootBone().worldScale.z));
            Controller.getMat(var9, var8);
         } else {
            var8.origin.set(var10.worldPos);
            var8.basis.set(var10.worldRot);
            var9 = new Matrix4f();
            Controller.getMat(var8, var9);
            var9.scale(new Vector3f(var10.worldScale.x, var10.worldScale.y, var10.worldScale.z));
            Controller.getMat(var9, var8);
         }

         GlUtil.glMultMatrix(var8);
         var6.applyTexture(var7, var2);
         var6.updateController(var1);
         if (var7.getSkin() != null) {
            var7.getSkin().color.set(var3);
         }

         var7.drawVBO(false);
         var6.drawAttached(var1, var2, var3);
         GlUtil.glPopMatrix();
      }

   }

   protected void updateController(Timer var1) {
      if (this.controller != null) {
         this.controller.update(var1);
      }

   }

   protected void applyTexture(Mesh var1, PlayerSkin var2) {
      if (var2 != null && var2.containsMesh(var1)) {
         if (var1.getSkin() != null) {
            var1.getSkin().setDiffuseTexId(var2.getDiffuseIdFor(var1));
            var1.getSkin().setEmissiveTexId(var2.getEmissiveIdFor(var1));
         } else {
            var1.getMaterial().setTexture(this.getTexture());
         }
      } else {
         if (this.getTexture() != null) {
            if (var1.getSkin() == null) {
               var1.getMaterial().setTexture(this.getTexture());
               return;
            }

            var1.getSkin().setDiffuseTexId(this.getTexture().getTextureId());
            if (this.getEmissiveTexture() != null) {
               var1.getSkin().setEmissiveTexId(this.getEmissiveTexture().getTextureId());
               return;
            }
         } else if (var1.getSkin() == null) {
            var1.getMaterial().attach(0);
         }

      }
   }

   public void updateState(AnimationIndexElement var1) {
   }

   public void updateAnimation(Timer var1) {
   }

   public AnimationStructure getAnimations() {
      assert this.mesh != null;

      assert this.mesh.getParent() != null : this.mesh.getName();

      assert this.mesh.getParent().getName() != null : this.mesh.getName();

      assert this.mesh.getParent().getName() != null : this.mesh.getName();

      assert this.state != null : this.mesh.getName();

      assert this.state.getResourceMap() != null : this.mesh.getName();

      assert this.state.getResourceMap().get(this.mesh.getParent().getName()) != null : this.mesh.getParent().getName();

      return this.state.getResourceMap().get(this.mesh.getParent().getName()).animation;
   }

   public String getAnimation(AnimationIndexElement var1) {
      return var1.get(this.getAnimations()).animations[0];
   }

   public float getSpeed() {
      return this.speed;
   }

   public void setSpeed(float var1) {
      this.speed = var1;
   }

   public boolean isAnimActive() {
      return this.channel.isActive();
   }

   public float getAnimTime() {
      return this.channel.getTime();
   }

   public void setAnimTime(float var1) {
      this.channel.setTime(var1);
   }

   public boolean isAnimTorsoActive() {
      return this.channelTorso.isActive();
   }

   public float getAnimTorsoTime() {
      return this.channelTorso.getTime();
   }

   public void setAnimTorsoTime(float var1) {
      this.channelTorso.setTime(var1);
   }

   public float getAnimTorsoMaxTime() {
      return this.channelTorso.getAnimMaxTime();
   }

   public float getAnimMaxTime() {
      return this.channel.getAnimMaxTime();
   }

   public Texture getTexture() {
      return this.texture;
   }

   public void setTexture(Texture var1) {
      this.texture = var1;
   }

   public boolean isEqualMeshFromResourceName(String var1) {
      return this.mesh.getParent().getName().equals(var1);
   }

   public Texture getEmissiveTexture() {
      return this.emissiveTexture;
   }

   public void setEmissiveTexture(Texture var1) {
      this.emissiveTexture = var1;
   }

   public void setAnimSpeed(float var1) {
      this.channel.setSpeed(var1);
   }

   public void setLoopModeTorso(LoopMode var1) {
      this.channelTorso.setLoopMode(var1);
   }

   public void setLoopMode(LoopMode var1) {
      this.channel.setLoopMode(var1);
   }

   public void setAnimTorsoSpeed(float var1) {
      this.channelTorso.setSpeed(var1);
   }

   public String getAnimTorsoName() {
      return this.channelTorso.getAnimationName();
   }

   public String getAnimName() {
      return this.channel.getAnimationName();
   }

   public Object2ObjectArrayMap getAttachments() {
      return this.attachments;
   }
}
