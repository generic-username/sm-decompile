package org.schema.game.client.view.character;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.effects.ConstantIndication;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.animation.AnimationChannel;
import org.schema.schine.graphicsengine.animation.AnimationController;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Bone;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.Skin;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.input.Mouse;

public class DrawableHumanCharacter implements DrawableCharacterInterface {
   private final PlayerCharacter playerCharacter;
   private final Vector3f minAABB = new Vector3f();
   private final Vector3f maxAABB = new Vector3f();
   private AnimationController controller;
   private AnimationChannel channel;
   private ConstantIndication indication;
   private boolean firstDraw = true;
   private Vector3f cPos = new Vector3f();
   private float yaw;
   private float pitch;
   private GameClientState state;
   private boolean lastState;
   private PlayerState playerState;
   private Timer timer;
   private Transform gravityOrientation = new Transform();
   private Transform t = new Transform();
   private Matrix3f mRot = new Matrix3f();
   private Matrix3f mRotPitch = new Matrix3f();
   private Vector3f forward = new Vector3f(0.0F, 0.0F, 1.0F);
   private Vector3f up = new Vector3f(0.0F, 1.0F, 0.0F);
   private Vector3f down = new Vector3f(0.0F, -1.0F, 0.0F);

   public DrawableHumanCharacter(PlayerCharacter var1, Timer var2, GameClientState var3) {
      this.timer = var2;
      this.playerCharacter = var1;
      this.state = var3;
      this.indication = new ConstantIndication(var1.getWorldTransform(), this.getPlayerName() + " (" + var3.getFactionString() + ")");
      this.gravityOrientation.setIdentity();
   }

   public void cleanUp() {
   }

   public boolean isInClientRange() {
      return this.playerCharacter.isInClientRange();
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      if (this.playerState != null && this.timer != null) {
         if (this.isInvisible()) {
            HudIndicatorOverlay.toDrawTexts.remove(this.indication);
         } else {
            GlUtil.setRightVector(this.playerCharacter.getCharacterController().upAxisDirection[0], this.gravityOrientation);
            GlUtil.setUpVector(this.playerCharacter.getCharacterController().upAxisDirection[1], this.gravityOrientation);
            GlUtil.setForwardVector(this.playerCharacter.getCharacterController().upAxisDirection[2], this.gravityOrientation);
            this.gravityOrientation.inverse();
            this.mRot.setIdentity();
            this.mRotPitch.setIdentity();
            this.mRot.set(this.getYawRotation());
            this.mRotPitch.set(this.getPitchRotation());
            Mesh var1;
            Bone var2 = (var1 = (Mesh)Controller.getResLoader().getMesh("Konata").getChilds().get(0)).getSkin().getSkeleton().getBone("sceleton Head");
            var1.getSkin().getSkeleton().getBone("sceleton L Hand");
            Quat4fTools.set(this.mRotPitch, var2.getAddLocalRot());
            this.controller.update(this.timer);
            if (this.playerCharacter == this.state.getCharacter()) {
               this.cPos.set(Controller.getCamera().getPos());
               this.cPos.sub(this.playerCharacter.getHeadWorldTransform().origin);
               if (this.cPos.length() < 0.2F) {
                  HudIndicatorOverlay.toDrawTexts.remove(this.indication);
                  return;
               }
            }

            if (!HudIndicatorOverlay.toDrawTexts.contains(this.indication)) {
               HudIndicatorOverlay.toDrawTexts.add(this.indication);
            }

            GlUtil.glPushMatrix();
            this.t.set(this.playerCharacter.getWorldTransformOnClient());
            this.t.basis.mul(this.mRot);
            GlUtil.glMultMatrix(this.t);
            GlUtil.translateModelview(var1.getInitionPos().x, var1.getInitionPos().y - 0.1F, var1.getInitionPos().z);
            GlUtil.scaleModelview(1.0F, 1.0F, 1.0F);
            if ((Boolean)this.playerCharacter.getNetworkObject().hit.get()) {
               var1.getSkin().color.set(1.0F, 0.3F, 0.3F, 1.0F);
            } else {
               var1.getSkin().color.set(1.0F, 1.0F, 1.0F, 1.0F);
            }

            var1.currentTexCoordSet = this.playerState.getNetworkObject().playerFaceId.get();
            var1.drawVBO(false);
            GlUtil.glPopMatrix();
            this.mRotPitch.setIdentity();
            Quat4fTools.set(this.mRotPitch, var2.getAddLocalRot());
         }
      }
   }

   public boolean isInvisible() {
      return this.playerCharacter.isHidden() || this.playerState.isInvisibilityMode();
   }

   public void onInit() {
      Skin var1 = ((Mesh)Controller.getResLoader().getMesh("Konata").getChilds().get(0)).getSkin();
      this.controller = new AnimationController(var1);
      this.channel = this.controller.createChannel();
      this.channel.addFromRootBone(var1.getSkeleton().getRootBone().name);

      try {
         this.channel.setAnim("default");
      } catch (Exception var2) {
      }

      this.firstDraw = false;
   }

   public boolean isOwnPlayerCharacter() {
      return this.playerCharacter.isClientOwnObject();
   }

   private Matrix3f getPitchRotation() {
      Vector3f var1 = this.playerState.getForward(new Vector3f());
      this.gravityOrientation.transform(var1);
      this.pitch = (float)FastMath.acosFast((double)var1.y);
      this.pitch = Math.max(0.85F, Math.min(2.9415927F, this.pitch));
      --this.pitch;
      Matrix3f var2;
      (var2 = new Matrix3f()).setIdentity();
      var2.rotY(this.pitch);
      return var2;
   }

   private String getPlayerName() {
      return this.playerState == null ? "<nobody>" : this.playerState.getName();
   }

   private Matrix3f getYawRotation() {
      Vector3f var1 = this.playerState.getForward(new Vector3f());
      Vector3f var2 = this.playerState.getUp(new Vector3f());
      this.gravityOrientation.transform(var1);
      this.gravityOrientation.transform(var2);
      if (this.playerState != this.state.getPlayer() || !Mouse.isButtonDown(2) && !Keyboard.isKeyDown(54)) {
         if (var1.epsilonEquals(this.up, 0.01F)) {
            this.yaw = FastMath.atan2Fast(-var2.x, -var2.z);
         } else if (var1.epsilonEquals(this.down, 0.01F)) {
            this.yaw = FastMath.atan2Fast(var2.x, var2.z);
         } else {
            this.yaw = FastMath.atan2Fast(var1.x, var1.z);
         }
      }

      Matrix3f var3;
      (var3 = new Matrix3f()).setIdentity();
      var3.rotY(this.yaw);
      return var3;
   }

   public void onRemove() {
      HudIndicatorOverlay.toDrawTexts.remove(this.indication);
   }

   public void update(Timer var1) {
      try {
         if (this.playerCharacter.getAttachedPlayers().size() > 0) {
            if (this.playerCharacter.getAttachedPlayers().size() > 1) {
               this.state.getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_CHARACTER_DRAWABLEHUMANCHARACTER_0, this.playerCharacter.getAttachedPlayers()), 0.0F);
            }

            this.playerState = (PlayerState)this.playerCharacter.getAttachedPlayers().get(0);
         }

         if (this.playerState != null) {
            try {
               boolean var2;
               if ((var2 = this.playerState.isKeyDownOrSticky(KeyboardMappings.FORWARD) | this.playerState.isKeyDownOrSticky(KeyboardMappings.BACKWARDS) | this.playerState.isKeyDownOrSticky(KeyboardMappings.STRAFE_LEFT) | this.playerState.isKeyDownOrSticky(KeyboardMappings.STRAFE_RIGHT) | this.playerState.isKeyDownOrSticky(KeyboardMappings.UP) | this.playerState.isKeyDownOrSticky(KeyboardMappings.DOWN)) != this.lastState && this.channel != null) {
                  if (var2) {
                     this.channel.setAnim("flying", 0.5F);
                  } else {
                     this.channel.setAnim("default", 0.5F);
                  }

                  this.lastState = var2;
               }

               this.timer = var1;
               this.indication.setText(this.getPlayerName() + this.playerState.getFactionController().getFactionString());
               return;
            } catch (Exception var3) {
               System.err.println("DRAWABLE CHARACTER UPDATE FAILED: " + var3.getClass().getSimpleName() + ": " + var3.getMessage() + "; channel " + this.channel + ", PlayerState: " + this.playerState);
               var3.printStackTrace();
            }
         }

      } catch (IllegalArgumentException var4) {
         System.err.println(var4.getMessage());
      }
   }

   public int getId() {
      return this.playerCharacter.getId();
   }

   public boolean isInFrustum() {
      if (this.timer != null && this.playerState != null) {
         this.playerCharacter.getPhysicsDataContainer().getShape().getAabb(this.playerCharacter.getWorldTransformOnClient(), this.minAABB, this.maxAABB);
         return Controller.getCamera().isAABBInFrustum(this.minAABB, this.maxAABB);
      } else {
         return false;
      }
   }

   public void setShadowMode(boolean var1) {
   }

   public AbstractCharacter getEntity() {
      return this.playerCharacter;
   }
}
