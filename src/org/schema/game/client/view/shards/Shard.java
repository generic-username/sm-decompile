package org.schema.game.client.view.shards;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.RigidDebrisBody;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.TransformaleObjectTmpVars;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.Mesh;

public class Shard {
   private static final float MAX_LIFE_TIME_SECS_SLOW = 3.0F;
   private static TransformaleObjectTmpVars v = new TransformaleObjectTmpVars();
   public final RigidDebrisBody body;
   public final Mesh hull;
   private float lifeTimeSecs;
   private short type = 1;
   private int sectorId;
   private Transform tmpLocal = new Transform();
   private Transform t = new Transform();
   private boolean killFlag;
   private float tl;
   private float slowLifeTimeSecs;
   private Vector3f gravity;

   public Shard(RigidDebrisBody var1, Mesh var2, int var3, Vector3f var4) {
      this.body = var1;
      this.hull = var2;
      this.sectorId = var3;
      this.gravity = var4;
      var1.shard = this;
      int var5 = (Integer)EngineSettings.D_LIFETIME_NORM.getCurrentState();
      this.lifeTimeSecs = (float)((double)var5 + Math.random() * (double)((float)var5 * 0.5F));
      this.slowLifeTimeSecs = (float)(3.0D + Math.random() * 3.0D);
   }

   public void draw() {
      this.body.getMotionState().getWorldTransform(this.t);
      GlUtil.glPushMatrix();
      GlUtil.glMultMatrix(this.t);
      this.hull.draw();
      GlUtil.glPopMatrix();
   }

   public void drawBulk() {
      if (Controller.getCamera() == null || Controller.getCamera().isBoundingSphereInFrustrum(this.t.origin, 1.42F)) {
         GlUtil.glPushMatrix();
         GlUtil.glMultMatrix(this.t);
         float var10000 = 1.0F - 0.25F * Math.min(1.0F, this.tl * 3.0F);
         GlUtil.scaleModelview(var10000, var10000, var10000);
         GlUtil.glColor4f((float)ElementKeyMap.getInfo(this.getType()).getTextureId(0), Math.min(1.0F, Math.min(this.lifeTimeSecs, this.slowLifeTimeSecs)), 0.0F, 1.0F);
         this.hull.drawVBO();
         GlUtil.glPopMatrix();
      }
   }

   public short getType() {
      return this.type;
   }

   public void setType(short var1) {
      this.type = var1;
   }

   public boolean update(Timer var1, GameClientState var2, int var3) {
      this.body.setGravity(this.gravity);
      this.tl += var1.getDelta();
      this.body.getMotionState().getWorldTransform(this.tmpLocal);
      if ((RemoteSector)var2.getLocalAndRemoteObjectContainer().getLocalObjects().get(this.sectorId) == null) {
         this.slowLifeTimeSecs -= var1.getDelta() * 5.0F;
      } else {
         SimpleTransformableSendableObject.calcWorldTransformRelative(var2.getCurrentSectorId(), var2.getPlayer().getCurrentSector(), this.sectorId, this.tmpLocal, var2, false, this.t, v);
         this.lifeTimeSecs -= var1.getDelta();
         if (this.killFlag) {
            this.slowLifeTimeSecs -= var1.getDelta() * 2.0F;
         }

         if (var3 > 0 || this.slowLifeTimeSecs < 1.0F) {
            this.slowLifeTimeSecs -= (float)var3 * var1.getDelta();
         }
      }

      return this.slowLifeTimeSecs > 0.0F && this.lifeTimeSecs > 0.0F;
   }

   public void kill() {
      if (!this.killFlag) {
         if (this.slowLifeTimeSecs > 1.0F) {
            this.slowLifeTimeSecs = 1.0F;
         }

         this.killFlag = true;
      }

   }

   public boolean isKilled() {
      return this.killFlag;
   }
}
