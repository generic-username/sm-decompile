package org.schema.game.client.view;

import java.util.Observable;

public class DrawerUpdate {
   public Object arg1;
   Observable arg0;

   public boolean equals(Object var1) {
      return this.arg0 == ((DrawerUpdate)var1).arg0 && this.arg1 == ((DrawerUpdate)var1).arg1;
   }

   public void set(Observable var1, Object var2) {
      this.arg0 = var1;
      this.arg1 = var2;
   }
}
