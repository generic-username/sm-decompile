package org.schema.game.client.view.buildhelper;

import com.bulletphysics.linearmath.MatrixUtil;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import java.nio.FloatBuffer;
import java.util.Iterator;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.schema.common.FastMath;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.graphicsengine.forms.simple.Box;

@BuildHelperClass(
   name = "Circle"
)
public class CircleBuildHelper extends BuildHelper {
   public static LongOpenHashSet poses = new LongOpenHashSet();
   private static Vector3f[][] verts = Box.init();
   @BuildHelperVar(
      type = "float",
      name = BuildHelperVarName.CIRCLE_RADIUS,
      min = 0,
      max = 5000
   )
   public float R;
   @BuildHelperVar(
      type = "float",
      name = BuildHelperVarName.CIRCLE_X_ROT,
      min = 0,
      max = 360
   )
   public float xRot;
   @BuildHelperVar(
      type = "float",
      name = BuildHelperVarName.CIRCLE_Y_ROT,
      min = 0,
      max = 360
   )
   public float yRot;
   @BuildHelperVar(
      type = "float",
      name = BuildHelperVarName.CIRCLE_Z_ROT,
      min = 0,
      max = 360
   )
   public float zRot;
   private int vertCount;
   private FloatBuffer fBuffer;

   public CircleBuildHelper(Transformable var1) {
      super(var1);
   }

   void createCircle(float var1, int var2, Matrix3f var3) {
      poses.clear();
      var2 = (5 - (FastMath.round(var1) << 2)) / 4;
      int var4 = 0;
      int var14 = FastMath.round(var1);
      Vector3f var5 = new Vector3f(0.0F, 0.0F, 0.0F);

      Vector3f var7;
      do {
         Vector3f var6 = new Vector3f(var5.x + (float)var4, var5.y + (float)var14, var5.z);
         var7 = new Vector3f(var5.x + (float)var4, var5.y - (float)var14, var5.z);
         Vector3f var8 = new Vector3f(var5.x - (float)var4, var5.y + (float)var14, var5.z);
         Vector3f var11 = new Vector3f(var5.x - (float)var4, var5.y - (float)var14, var5.z);
         Vector3f var12 = new Vector3f(var5.x + (float)var14, var5.y + (float)var4, var5.z);
         Vector3f var9 = new Vector3f(var5.x + (float)var14, var5.y - (float)var4, var5.z);
         Vector3f var10 = new Vector3f(var5.x - (float)var14, var5.y + (float)var4, var5.z);
         Vector3f var13 = new Vector3f(var5.x - (float)var14, var5.y - (float)var4, var5.z);
         var3.transform(var6);
         var3.transform(var7);
         var3.transform(var8);
         var3.transform(var11);
         var3.transform(var12);
         var3.transform(var9);
         var3.transform(var10);
         var3.transform(var13);
         poses.add(ElementCollection.getIndex(FastMath.round(var6.x), FastMath.round(var6.y), FastMath.round(var6.z)));
         poses.add(ElementCollection.getIndex(FastMath.round(var7.x), FastMath.round(var7.y), FastMath.round(var7.z)));
         poses.add(ElementCollection.getIndex(FastMath.round(var8.x), FastMath.round(var8.y), FastMath.round(var8.z)));
         poses.add(ElementCollection.getIndex(FastMath.round(var11.x), FastMath.round(var11.y), FastMath.round(var11.z)));
         poses.add(ElementCollection.getIndex(FastMath.round(var12.x), FastMath.round(var12.y), FastMath.round(var12.z)));
         poses.add(ElementCollection.getIndex(FastMath.round(var9.x), FastMath.round(var9.y), FastMath.round(var9.z)));
         poses.add(ElementCollection.getIndex(FastMath.round(var10.x), FastMath.round(var10.y), FastMath.round(var10.z)));
         poses.add(ElementCollection.getIndex(FastMath.round(var13.x), FastMath.round(var13.y), FastMath.round(var13.z)));
         if (var2 < 0) {
            var2 += 2 * var4 + 1;
         } else {
            var2 += 2 * (var4 - var14) + 1;
            --var14;
         }

         ++var4;
      } while(var4 <= var14);

      this.vertCount = poses.size() * 24;
      this.fBuffer = GlUtil.getDynamicByteBuffer(this.vertCount * 3 << 2, 8).asFloatBuffer();
      Vector3f[][] var15 = Box.getVertices(new Vector3f(-0.5F, -0.5F, -0.5F), new Vector3f(0.5F, 0.5F, 0.5F), verts);
      var7 = new Vector3f();
      Iterator var16 = poses.iterator();

      while(var16.hasNext()) {
         ElementCollection.getPosFromIndex((Long)var16.next(), var7);

         for(int var17 = 0; var17 < var15.length; ++var17) {
            for(int var18 = 0; var18 < var15[var17].length; ++var18) {
               this.fBuffer.put(var7.x + var15[var17][var18].x);
               this.fBuffer.put(var7.y + var15[var17][var18].y);
               this.fBuffer.put(var7.z + var15[var17][var18].z);
            }
         }
      }

      this.fBuffer.flip();
      this.percent = 1.0F;
      this.setFinished(true);
   }

   public void create() {
      Matrix3f var1;
      MatrixUtil.setEulerZYX(var1 = new Matrix3f(), 0.017453292F * this.xRot, 0.017453292F * this.yRot, 0.017453292F * this.zRot);
      this.createCircle(this.R, (int)(180.0F + this.R * 300.0F), var1);
      this.setInitialized(true);
   }

   public void drawLocal() {
      GlUtil.glEnableClientState(32884);
      GlUtil.glEnable(2884);
      GL11.glPolygonMode(1032, 6913);
      GlUtil.glDisable(2896);
      GlUtil.glDisable(2884);
      GlUtil.glEnable(2903);
      GlUtil.glDisable(32879);
      GlUtil.glDisable(3553);
      GlUtil.glDisable(3552);
      GlUtil.glEnable(3042);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glBindBuffer(34962, this.buffer);

      assert this.buffer != 0;

      GL11.glVertexPointer(3, 5126, 0, 0L);
      GL11.glDrawArrays(7, 0, this.vertCount);
      GlUtil.glEnable(2929);
      GlUtil.glEnable(2896);
      GlUtil.glDisable(2903);
      GlUtil.glEnable(2884);
      GlUtil.glDisable(3042);
      GL11.glPolygonMode(1032, 6914);
      GlUtil.glDisableClientState(32884);
   }

   public void clean() {
      if (this.buffer != 0) {
         GL15.glDeleteBuffers(this.buffer);
         this.buffer = 0;
      }

      this.setFinished(false);
      System.err.println("[CLIENT] Circle clean called");
   }

   public LongOpenHashSet getPoses() {
      return poses;
   }

   public void onFinished() {
      if (this.buffer != 0) {
         GL15.glDeleteBuffers(this.buffer);
      }

      this.buffer = GL15.glGenBuffers();
      GL15.glBindBuffer(34962, this.buffer);
      System.err.println("[CLIENT] Circle: Blocks: " + poses.size() + "; ByteBufferNeeded: " + (float)(this.vertCount * 3 << 2) / 1024.0F / 1024.0F + "MB");
      GL15.glBufferData(34962, this.fBuffer, 35044);
      GL15.glBindBuffer(34962, 0);
   }
}
