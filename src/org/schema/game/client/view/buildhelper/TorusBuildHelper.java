package org.schema.game.client.view.buildhelper;

import com.bulletphysics.linearmath.MatrixUtil;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import java.nio.FloatBuffer;
import java.util.Iterator;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.schema.common.FastMath;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.graphicsengine.forms.simple.Box;

@BuildHelperClass(
   name = "Torus"
)
public class TorusBuildHelper extends BuildHelper {
   public static LongOpenHashSet poses = new LongOpenHashSet();
   private static Vector3f[][] verts = Box.init();
   @BuildHelperVar(
      type = "float",
      name = BuildHelperVarName.TORUS_RADIUS,
      min = 0,
      max = 256
   )
   public float R;
   @BuildHelperVar(
      type = "float",
      name = BuildHelperVarName.TORUS_TUBE_RADIUS,
      min = 0,
      max = 256
   )
   public float r;
   @BuildHelperVar(
      type = "float",
      name = BuildHelperVarName.TORUS_X_ROT,
      min = 0,
      max = 360
   )
   public float xRot;
   @BuildHelperVar(
      type = "float",
      name = BuildHelperVarName.TORUS_Y_ROT,
      min = 0,
      max = 360
   )
   public float yRot;
   @BuildHelperVar(
      type = "float",
      name = BuildHelperVarName.TORUS_Z_ROT,
      min = 0,
      max = 360
   )
   public float zRot;
   private int vertCount;
   private FloatBuffer fBuffer;

   public TorusBuildHelper(Transformable var1) {
      super(var1);
   }

   public static void drawTorus(int var0, int var1, float var2, float var3, float var4) {
      poses.clear();
      float var14 = 3.1415927F / (float)var1;
      float var13 = 3.1415927F / (float)var0;
      GL11.glPolygonMode(1032, 6913);
      GlUtil.glDisable(2884);

      for(float var5 = -1.5707964F; var5 <= 1.5708964F; var5 += var14) {
         GL11.glBegin(5);

         for(float var6 = -3.1415927F; var6 <= 3.1416926F; var6 += var13) {
            float var7 = var2 * FastMath.cos(var5) * FastMath.cos(var6);
            float var10 = var3 * FastMath.cos(var5) * FastMath.sin(var6);
            float var11 = var4 * FastMath.sin(var5);
            GL11.glVertex3f(var7, var10, var11);
            float var8 = var2 * FastMath.cos(var5 + var14) * FastMath.cos(var6);
            float var9 = var3 * FastMath.cos(var5 + var14) * FastMath.sin(var6);
            float var12 = var4 * FastMath.sin(var5 + var14);
            GL11.glVertex3f(var8, var9, var12);
            poses.add(ElementCollection.getIndex(FastMath.round(var7), FastMath.round(var10), FastMath.round(var11)));
            poses.add(ElementCollection.getIndex(FastMath.round(var8), FastMath.round(var9), FastMath.round(var12)));
         }

         GL11.glEnd();
      }

      GlUtil.glEnable(2884);
      GL11.glPolygonMode(1032, 6914);
      Vector3f[][] var15 = Box.getVertices(new Vector3f(-0.5F, -0.5F, -0.5F), new Vector3f(0.5F, 0.5F, 0.5F), verts);
      GL11.glPolygonMode(1032, 6913);
      GlUtil.glDisable(2896);
      GlUtil.glDisable(2884);
      GlUtil.glEnable(2903);
      GlUtil.glDisable(32879);
      GlUtil.glDisable(3553);
      GlUtil.glDisable(3552);
      GlUtil.glEnable(3042);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GL11.glBegin(7);
      Vector3f var16 = new Vector3f();
      Iterator var17 = poses.iterator();

      while(var17.hasNext()) {
         ElementCollection.getPosFromIndex((Long)var17.next(), var16);

         for(int var18 = 0; var18 < var15.length; ++var18) {
            for(int var19 = 0; var19 < var15[var18].length; ++var19) {
               GL11.glVertex3f(var16.x + var15[var18][var19].x, var16.y + var15[var18][var19].y, var16.z + var15[var18][var19].z);
            }
         }
      }

      GL11.glEnd();
      GlUtil.glEnable(2929);
      GlUtil.glEnable(2896);
      GlUtil.glDisable(2903);
      GlUtil.glEnable(2884);
      GlUtil.glDisable(3042);
      GL11.glPolygonMode(1032, 6914);
   }

   public void createTorus(float var1, float var2, int var3, int var4, Matrix3f var5) {
      poses.clear();
      var4 = Math.min(var4, 999);
      var3 = Math.min(var3, 999);
      float var20 = 6.2831855F / (float)var4;
      float var19 = 6.2831855F / (float)var3;
      float var6 = 0.0F;
      float var7 = 0.0F;
      float var8 = (float)((6.283185307179586D + (double)var19) / (double)var19 * (double)((var20 + 6.2831855F) / var20));
      float var9 = 0.0F;
      Vector3f var10 = new Vector3f();

      for(Vector3f var11 = new Vector3f(); (double)var7 < 6.283185307179586D + (double)var19; var7 += var19) {
         for(var6 = 0.0F; var6 < var20 + 6.2831855F; this.percent = var9 / var8) {
            float var12 = (var1 + var2 * FastMath.cos(var6)) * FastMath.cos(var7);
            float var13 = (var1 + var2 * FastMath.cos(var6)) * FastMath.sin(var7);
            float var14 = var2 * FastMath.sin(var6);
            float var16 = (var1 + var2 * FastMath.cos(var6 + var20)) * FastMath.cos(var7 + var19);
            float var17 = (var1 + var2 * FastMath.cos(var6 + var20)) * FastMath.sin(var7 + var19);
            float var15 = var2 * FastMath.sin(var6 + var20);
            var10.set(var12, var13, var14);
            var11.set(var16, var17, var15);
            var5.transform(var10);
            var5.transform(var11);
            poses.add(ElementCollection.getIndex(FastMath.round(var10.x), FastMath.round(var10.y), FastMath.round(var10.z)));
            poses.add(ElementCollection.getIndex(FastMath.round(var11.x), FastMath.round(var11.y), FastMath.round(var11.z)));
            var6 += var20;
            ++var9;
         }
      }

      this.vertCount = poses.size() * 24;
      this.fBuffer = GlUtil.getDynamicByteBuffer(this.vertCount * 3 << 2, 8).asFloatBuffer();
      Vector3f[][] var21 = Box.getVertices(new Vector3f(-0.5F, -0.5F, -0.5F), new Vector3f(0.5F, 0.5F, 0.5F), verts);
      Vector3f var22 = new Vector3f();
      Iterator var23 = poses.iterator();

      while(var23.hasNext()) {
         ElementCollection.getPosFromIndex((Long)var23.next(), var22);

         for(int var24 = 0; var24 < var21.length; ++var24) {
            for(int var18 = 0; var18 < var21[var24].length; ++var18) {
               this.fBuffer.put(var22.x + var21[var24][var18].x);
               this.fBuffer.put(var22.y + var21[var24][var18].y);
               this.fBuffer.put(var22.z + var21[var24][var18].z);
            }
         }
      }

      this.fBuffer.flip();
      this.percent = 1.0F;
      this.setFinished(true);
   }

   public void create() {
      Matrix3f var1;
      MatrixUtil.setEulerZYX(var1 = new Matrix3f(), 0.017453292F * this.xRot, 0.017453292F * this.yRot, 0.017453292F * this.zRot);
      this.createTorus(this.R, this.r, (int)this.R << 3, (int)this.R << 3, var1);
      this.setInitialized(true);
   }

   public void drawLocal() {
      GlUtil.glEnableClientState(32884);
      GlUtil.glEnable(2884);
      GL11.glPolygonMode(1032, 6913);
      GlUtil.glDisable(2896);
      GlUtil.glDisable(2884);
      GlUtil.glEnable(2903);
      GlUtil.glDisable(32879);
      GlUtil.glDisable(3553);
      GlUtil.glDisable(3552);
      GlUtil.glEnable(3042);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glBindBuffer(34962, this.buffer);
      GL11.glVertexPointer(3, 5126, 0, 0L);
      GL11.glDrawArrays(7, 0, this.vertCount);
      GlUtil.glEnable(2929);
      GlUtil.glEnable(2896);
      GlUtil.glDisable(2903);
      GlUtil.glEnable(2884);
      GlUtil.glDisable(3042);
      GL11.glPolygonMode(1032, 6914);
      GlUtil.glDisableClientState(32884);
   }

   public void clean() {
      if (this.buffer != 0) {
         GL15.glDeleteBuffers(this.buffer);
         this.buffer = 0;
      }

      this.setFinished(false);
   }

   public LongOpenHashSet getPoses() {
      return poses;
   }

   public void onFinished() {
      if (this.buffer != 0) {
         GL15.glDeleteBuffers(this.buffer);
      }

      this.buffer = GL15.glGenBuffers();
      GL15.glBindBuffer(34962, this.buffer);
      System.err.println("[CLIENT] elipsoid: Blocks: " + poses.size() + "; ByteBufferNeeded: " + (float)(this.vertCount * 3 << 2) / 1024.0F / 1024.0F + "MB");
      GL15.glBufferData(34962, this.fBuffer, 35044);
      GL15.glBindBuffer(34962, 0);
   }
}
