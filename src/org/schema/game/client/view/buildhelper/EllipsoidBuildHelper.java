package org.schema.game.client.view.buildhelper;

import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import java.nio.FloatBuffer;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.schema.common.FastMath;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.graphicsengine.forms.simple.Box;

@BuildHelperClass(
   name = "Ellipsoid"
)
public class EllipsoidBuildHelper extends BuildHelper {
   public static LongOpenHashSet poses = new LongOpenHashSet();
   private static Vector3f[][] verts = Box.init();
   @BuildHelperVar(
      type = "float",
      name = BuildHelperVarName.ELIPSOID_RADIUS_X,
      min = 0,
      max = 256
   )
   public float xRadius;
   @BuildHelperVar(
      type = "float",
      name = BuildHelperVarName.ELIPSOID_RADIUS_Y,
      min = 0,
      max = 256
   )
   public float yRadius;
   @BuildHelperVar(
      type = "float",
      name = BuildHelperVarName.ELIPSOID_RADIUS_Z,
      min = 0,
      max = 256
   )
   public float zRadius;
   private int vertCount;
   private FloatBuffer fBuffer;

   public EllipsoidBuildHelper(Transformable var1) {
      super(var1);
   }

   public static void drawEllipsoid(int var0, int var1, float var2, float var3, float var4) {
      poses.clear();
      float var14 = 3.1415927F / (float)var1;
      float var13 = 3.1415927F / (float)var0;
      GL11.glPolygonMode(1032, 6913);
      GlUtil.glDisable(2884);

      for(float var5 = -1.5707964F; var5 <= 1.5708964F; var5 += var14) {
         GL11.glBegin(5);

         for(float var6 = -3.1415927F; var6 <= 3.1416926F; var6 += var13) {
            float var7 = var2 * FastMath.cos(var5) * FastMath.cos(var6);
            float var10 = var3 * FastMath.cos(var5) * FastMath.sin(var6);
            float var11 = var4 * FastMath.sin(var5);
            GL11.glVertex3f(var7, var10, var11);
            float var8 = var2 * FastMath.cos(var5 + var14) * FastMath.cos(var6);
            float var9 = var3 * FastMath.cos(var5 + var14) * FastMath.sin(var6);
            float var12 = var4 * FastMath.sin(var5 + var14);
            GL11.glVertex3f(var8, var9, var12);
            poses.add(ElementCollection.getIndex(FastMath.round(var7), FastMath.round(var10), FastMath.round(var11)));
            poses.add(ElementCollection.getIndex(FastMath.round(var8), FastMath.round(var9), FastMath.round(var12)));
         }

         GL11.glEnd();
      }

      GlUtil.glEnable(2884);
      GL11.glPolygonMode(1032, 6914);
      Vector3f[][] var15 = Box.getVertices(new Vector3f(-0.5F, -0.5F, -0.5F), new Vector3f(0.5F, 0.5F, 0.5F), verts);
      GL11.glPolygonMode(1032, 6913);
      GlUtil.glDisable(2896);
      GlUtil.glDisable(2884);
      GlUtil.glEnable(2903);
      GlUtil.glDisable(32879);
      GlUtil.glDisable(3553);
      GlUtil.glDisable(3552);
      GlUtil.glEnable(3042);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GL11.glBegin(7);
      Vector3f var16 = new Vector3f();
      Iterator var17 = poses.iterator();

      while(var17.hasNext()) {
         ElementCollection.getPosFromIndex((Long)var17.next(), var16);

         for(int var18 = 0; var18 < var15.length; ++var18) {
            for(int var19 = 0; var19 < var15[var18].length; ++var19) {
               GL11.glVertex3f(var16.x + var15[var18][var19].x, var16.y + var15[var18][var19].y, var16.z + var15[var18][var19].z);
            }
         }
      }

      GL11.glEnd();
      GlUtil.glEnable(2929);
      GlUtil.glEnable(2896);
      GlUtil.glDisable(2903);
      GlUtil.glEnable(2884);
      GlUtil.glDisable(3042);
      GL11.glPolygonMode(1032, 6914);
   }

   public void createEllipsoid(int var1, int var2, float var3, float var4, float var5) {
      poses.clear();
      float var17 = 3.1415927F / (float)var2;
      float var16 = 3.1415927F / (float)var1;
      float var6 = (4.712489F / var17 + 1.0F) * (4.712489F / var16 + 1.0F);
      float var7 = 0.0F;

      for(float var8 = -1.5707964F; var8 <= 1.5708964F; var8 += var17) {
         for(float var9 = -3.1415927F; var9 <= 3.1416926F; var9 += var16) {
            float var10 = var3 * FastMath.cos(var8) * FastMath.cos(var9);
            float var13 = var4 * FastMath.cos(var8) * FastMath.sin(var9);
            float var14 = var5 * FastMath.sin(var8);
            float var11 = var3 * FastMath.cos(var8 + var17) * FastMath.cos(var9);
            float var12 = var4 * FastMath.cos(var8 + var17) * FastMath.sin(var9);
            float var15 = var5 * FastMath.sin(var8 + var17);
            poses.add(ElementCollection.getIndex(FastMath.round(var10), FastMath.round(var13), FastMath.round(var14)));
            poses.add(ElementCollection.getIndex(FastMath.round(var11), FastMath.round(var12), FastMath.round(var15)));
            ++var7;
            this.percent = var7 / var6;
         }
      }

      this.vertCount = poses.size() * 24;
      this.fBuffer = GlUtil.getDynamicByteBuffer(this.vertCount * 3 << 2, 8).asFloatBuffer();
      Vector3f[][] var18 = Box.getVertices(new Vector3f(-0.5F, -0.5F, -0.5F), new Vector3f(0.5F, 0.5F, 0.5F), verts);
      Vector3f var19 = new Vector3f();
      Iterator var20 = poses.iterator();

      while(var20.hasNext()) {
         ElementCollection.getPosFromIndex((Long)var20.next(), var19);

         for(int var21 = 0; var21 < var18.length; ++var21) {
            for(int var22 = 0; var22 < var18[var21].length; ++var22) {
               this.fBuffer.put(var19.x + var18[var21][var22].x);
               this.fBuffer.put(var19.y + var18[var21][var22].y);
               this.fBuffer.put(var19.z + var18[var21][var22].z);
            }
         }
      }

      this.fBuffer.flip();
      this.percent = 1.0F;
      this.setFinished(true);
   }

   public void create() {
      int var1 = (int)Math.max(this.xRadius, Math.max(this.yRadius, Math.max(1.0F, this.zRadius)));
      this.createEllipsoid(var1 << 2, var1 << 2, this.xRadius, this.yRadius, this.zRadius);
      this.setInitialized(true);
   }

   public void drawLocal() {
      if (this.isFinished()) {
         GlUtil.glEnableClientState(32884);
         GlUtil.glEnable(2884);
         GL11.glPolygonMode(1032, 6913);
         GlUtil.glDisable(2896);
         GlUtil.glDisable(2884);
         GlUtil.glEnable(2903);
         GlUtil.glDisable(32879);
         GlUtil.glDisable(3553);
         GlUtil.glDisable(3552);
         GlUtil.glEnable(3042);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GlUtil.glBindBuffer(34962, this.buffer);
         GL11.glVertexPointer(3, 5126, 0, 0L);
         GL11.glDrawArrays(7, 0, this.vertCount);
         GlUtil.glEnable(2929);
         GlUtil.glEnable(2896);
         GlUtil.glDisable(2903);
         GlUtil.glEnable(2884);
         GlUtil.glDisable(3042);
         GL11.glPolygonMode(1032, 6914);
         GlUtil.glDisableClientState(32884);
      }
   }

   public void clean() {
      if (this.buffer != 0) {
         GL15.glDeleteBuffers(this.buffer);
         this.buffer = 0;
      }

      this.setFinished(false);
   }

   public LongOpenHashSet getPoses() {
      return poses;
   }

   public void onFinished() {
      if (this.buffer != 0) {
         GL15.glDeleteBuffers(this.buffer);
      }

      this.buffer = GL15.glGenBuffers();
      GL15.glBindBuffer(34962, this.buffer);
      System.err.println("[CLIENT] elipsoid: Blocks: " + poses.size() + "; ByteBufferNeeded: " + (float)(this.vertCount * 3 << 2) / 1024.0F / 1024.0F + "MB");
      GL15.glBufferData(34962, this.fBuffer, 35044);
      GL15.glBindBuffer(34962, 0);
   }
}
