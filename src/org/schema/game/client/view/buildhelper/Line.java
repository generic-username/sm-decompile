package org.schema.game.client.view.buildhelper;

import org.schema.common.util.linAlg.Vector3i;

public class Line {
   public Vector3i A;
   public Vector3i B;
}
