package org.schema.game.client.view.creaturetool.swing;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.character.AnimationNotSetException;
import org.schema.game.client.view.creaturetool.CreatureTool;
import org.schema.game.common.data.creature.AIRandomCompositeCreature;
import org.schema.game.common.data.creature.CreaturePartNode;
import org.schema.schine.resource.CreatureStructure;

public class CreatureToolMainPanel extends JPanel {
   private static final long serialVersionUID = 1L;
   private CreatureTool creatureTool;
   private GameClientState state;
   private CreatureToolEditPartPanel creatureToolEditPartPanel2;
   private CreatureToolEditPartPanel creatureToolEditPartPanel1;
   private CreatureToolEditPartPanel creatureToolEditPartPanel0;
   private JSpinner spinner;
   private JComboBox comboBox;

   public CreatureToolMainPanel(GameClientState var1, final CreatureTool var2) {
      this.state = var1;
      this.creatureTool = var2;
      GridBagLayout var3;
      (var3 = new GridBagLayout()).rowWeights = new double[]{1.0D, 1.0D};
      var3.columnWeights = new double[]{1.0D};
      this.setLayout(var3);
      JPanel var8 = new JPanel();
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var4.fill = 1;
      var4.gridx = 0;
      var4.gridy = 0;
      this.add(var8, var4);
      GridBagLayout var9;
      (var9 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var9.rowHeights = new int[]{0, 0};
      var9.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var9.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var8.setLayout(var9);
      JScrollPane var11 = new JScrollPane();
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).fill = 1;
      var5.gridx = 0;
      var5.gridy = 0;
      var8.add(var11, var5);
      var8 = new JPanel();
      var11.setViewportView(var8);
      (var9 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var9.rowHeights = new int[]{0, 0, 0};
      var9.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var9.rowWeights = new double[]{0.0D, 1.0D, Double.MIN_VALUE};
      var8.setLayout(var9);
      JPanel var14;
      (var14 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Parent Animation", 4, 2, (Font)null, (Color)null));
      (var5 = new GridBagConstraints()).fill = 1;
      var5.insets = new Insets(0, 0, 5, 0);
      var5.gridx = 0;
      var5.gridy = 0;
      var8.add(var14, var5);
      GridBagLayout var15;
      (var15 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var15.rowHeights = new int[]{0, 0};
      var15.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var15.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var14.setLayout(var15);
      this.comboBox = new JComboBox(new CreaturePartAnimationComboboxModel());
      this.comboBox.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            CreatureToolMainPanel.this.updateGUI();
         }
      });
      (var5 = new GridBagConstraints()).fill = 2;
      var5.gridx = 0;
      var5.gridy = 0;
      var14.add(this.comboBox, var5);
      var14 = new JPanel();
      (var5 = new GridBagConstraints()).fill = 2;
      var5.anchor = 18;
      var5.gridx = 0;
      var5.gridy = 1;
      var8.add(var14, var5);
      (var3 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0};
      var3.rowHeights = new int[]{0, 0, 0};
      var3.columnWeights = new double[]{0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var3.rowWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var14.setLayout(var3);
      JLabel var10 = new JLabel("Speed");
      (var5 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var5.gridx = 0;
      var5.gridy = 0;
      var14.add(var10, var5);
      this.spinner = new JSpinner(new CreatureToolSpeedSpinnerModel(var2, new CreatureStructure.PartType[]{CreatureStructure.PartType.BOTTOM, CreatureStructure.PartType.MIDDLE, CreatureStructure.PartType.TOP}));
      GridBagConstraints var12;
      (var12 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var12.gridx = 1;
      var12.gridy = 0;
      var14.add(this.spinner, var12);
      JButton var13;
      (var13 = new JButton("Spawn this abomination!")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            var2.spawn();
         }
      });
      (var5 = new GridBagConstraints()).weightx = 1.0D;
      var5.anchor = 13;
      var5.insets = new Insets(0, 0, 0, 5);
      var5.gridx = 2;
      var5.gridy = 0;
      var14.add(var13, var5);
      var8 = new JPanel();
      (var4 = new GridBagConstraints()).weighty = 3.0D;
      var4.fill = 1;
      var4.gridx = 0;
      var4.gridy = 1;
      this.add(var8, var4);
      (var9 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var9.rowHeights = new int[]{0, 0, 0, 0};
      var9.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var9.rowWeights = new double[]{1.0D, 1.0D, 1.0D, Double.MIN_VALUE};
      var8.setLayout(var9);
      var14 = new JPanel();
      (var5 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var5.fill = 1;
      var5.gridx = 0;
      var5.gridy = 0;
      var8.add(var14, var5);
      (var15 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var15.rowHeights = new int[]{0, 0};
      var15.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var15.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var14.setLayout(var15);
      JScrollPane var17 = new JScrollPane();
      GridBagConstraints var6;
      (var6 = new GridBagConstraints()).weighty = 1.0D;
      var6.weightx = 1.0D;
      var6.anchor = 18;
      var6.fill = 1;
      var6.gridx = 0;
      var6.gridy = 0;
      var14.add(var17, var6);
      this.creatureToolEditPartPanel0 = new CreatureToolEditPartPanel(CreatureStructure.PartType.BOTTOM, var1, var2, this);
      this.creatureToolEditPartPanel0.setBorder(new TitledBorder((Border)null, "Part-Basis", 4, 2, (Font)null, (Color)null));
      var17.setViewportView(this.creatureToolEditPartPanel0);
      (var9 = new GridBagLayout()).columnWidths = new int[]{0};
      var9.rowHeights = new int[]{0};
      var9.columnWeights = new double[]{Double.MIN_VALUE};
      var9.rowWeights = new double[]{Double.MIN_VALUE};
      var14 = new JPanel();
      (var5 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var5.fill = 1;
      var5.gridx = 0;
      var5.gridy = 1;
      var8.add(var14, var5);
      (var15 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var15.rowHeights = new int[]{0, 0};
      var15.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var15.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var14.setLayout(var15);
      var17 = new JScrollPane();
      (var6 = new GridBagConstraints()).anchor = 18;
      var6.weightx = 1.0D;
      var6.weighty = 1.0D;
      var6.fill = 1;
      var6.gridx = 0;
      var6.gridy = 0;
      var14.add(var17, var6);
      this.creatureToolEditPartPanel1 = new CreatureToolEditPartPanel(CreatureStructure.PartType.MIDDLE, var1, var2, this);
      this.creatureToolEditPartPanel1.setBorder(new TitledBorder((Border)null, "Second Part", 4, 2, (Font)null, (Color)null));
      var17.setViewportView(this.creatureToolEditPartPanel1);
      (var9 = new GridBagLayout()).columnWidths = new int[]{0};
      var9.rowHeights = new int[]{0};
      var9.columnWeights = new double[]{Double.MIN_VALUE};
      var9.rowWeights = new double[]{Double.MIN_VALUE};
      var14 = new JPanel();
      (var5 = new GridBagConstraints()).fill = 1;
      var5.gridx = 0;
      var5.gridy = 2;
      var8.add(var14, var5);
      (var3 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var3.rowHeights = new int[]{0, 0};
      var3.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var3.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var14.setLayout(var3);
      JScrollPane var16 = new JScrollPane();
      (var5 = new GridBagConstraints()).weighty = 1.0D;
      var5.weightx = 1.0D;
      var5.fill = 1;
      var5.gridx = 0;
      var5.gridy = 0;
      var14.add(var16, var5);
      this.creatureToolEditPartPanel2 = new CreatureToolEditPartPanel(CreatureStructure.PartType.TOP, var1, var2, this);
      this.creatureToolEditPartPanel2.setBorder(new TitledBorder((Border)null, "Third Part", 4, 2, (Font)null, (Color)null));
      var16.setViewportView(this.creatureToolEditPartPanel2);
      GridBagLayout var7;
      (var7 = new GridBagLayout()).columnWidths = new int[]{0};
      var7.rowHeights = new int[]{0};
      var7.columnWeights = new double[]{Double.MIN_VALUE};
      var7.rowWeights = new double[]{Double.MIN_VALUE};
   }

   public void updateGUI() {
      ((CreatureToolSpeedSpinnerModel)this.spinner.getModel()).fireChangeEvent();

      try {
         this.creatureTool.updateForcedAnimation(CreatureStructure.PartType.BOTTOM, this.comboBox.getSelectedItem());
      } catch (AnimationNotSetException var3) {
         System.err.println("[0] BOTTOM COULD NOT SET ANIMATION! " + this.comboBox.getSelectedItem());
      }

      try {
         this.creatureTool.updateForcedAnimation(CreatureStructure.PartType.MIDDLE, this.comboBox.getSelectedItem());
      } catch (AnimationNotSetException var2) {
         System.err.println("[1] MIDDLE COULD NOT SET ANIMATION! " + this.comboBox.getSelectedItem());
      }

      try {
         this.creatureTool.updateForcedAnimation(CreatureStructure.PartType.TOP, this.comboBox.getSelectedItem());
      } catch (AnimationNotSetException var1) {
         System.err.println("[2] TOP COULD NOT SET ANIMATION! " + this.comboBox.getSelectedItem());
      }

      this.creatureToolEditPartPanel0.updateGUI();
      this.creatureToolEditPartPanel1.updateGUI();
      this.creatureToolEditPartPanel2.updateGUI();
   }

   public void update() {
      Float var1 = null;
      if (this.creatureTool.getCreature() != null) {
         var1 = this.creatureTool.getCreature().getSpeed();
      }

      AIRandomCompositeCreature var5;
      if (this.creatureToolEditPartPanel0.getData() != null) {
         CreaturePartNode var2 = new CreaturePartNode(CreatureStructure.PartType.BOTTOM, this.state, this.creatureToolEditPartPanel0.getData().mesh, (String)null);
         if (this.creatureToolEditPartPanel1.getData() != null) {
            CreaturePartNode var3 = new CreaturePartNode(CreatureStructure.PartType.MIDDLE, this.state, this.creatureToolEditPartPanel1.getData().mesh, (String)null);
            var2.attach(this.state, var3, CreaturePartNode.AttachmentType.MAIN);
            if (this.creatureToolEditPartPanel2.getData() != null) {
               CreaturePartNode var4 = new CreaturePartNode(CreatureStructure.PartType.TOP, this.state, this.creatureToolEditPartPanel2.getData().mesh, (String)null);
               var3.attach(this.state, var4, CreaturePartNode.AttachmentType.MAIN);
            }
         }

         var5 = AIRandomCompositeCreature.instantiate(this.state, 4.0F, 1.0F, 0.5F, 0.25F, new Vector3i(1, 1, 1), var2);
      } else {
         var5 = null;
      }

      this.creatureTool.updateFromGUI(var5);
      if (var1 != null) {
         if (this.creatureTool.getCreature() != null) {
            this.creatureTool.getCreature().setSpeed(var1);
         }

         System.err.println("SET LAST SPEED: " + var1);
         ((CreatureToolSpeedSpinnerModel)this.spinner.getModel()).setValue(var1);
      }

      this.updateGUI();
   }

   public void updateParenAnimation(CreatureStructure.PartType var1) {
   }
}
