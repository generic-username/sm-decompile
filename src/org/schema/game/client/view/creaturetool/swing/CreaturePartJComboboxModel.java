package org.schema.game.client.view.creaturetool.swing;

import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.resource.CreatureStructure;

public class CreaturePartJComboboxModel implements ComboBoxModel {
   private final CreatureStructure.PartType type;
   private final List list;
   private String selected;

   public CreaturePartJComboboxModel(CreatureStructure.PartType var1) {
      this.type = var1;
      this.list = Controller.getResLoader().getResourceMap().getType(var1);
   }

   public int getSize() {
      return this.list.size() + 1;
   }

   public String getElementAt(int var1) {
      if (var1 >= 0) {
         return var1 == 0 ? "NONE" : (String)this.list.get(var1 - 1);
      } else {
         return "-1";
      }
   }

   public void addListDataListener(ListDataListener var1) {
   }

   public void removeListDataListener(ListDataListener var1) {
   }

   public CreatureStructure.PartType getType() {
      return this.type;
   }

   public void setSelectedItem(Object var1) {
      this.selected = var1.toString();
   }

   public Object getSelectedItem() {
      return this.selected;
   }
}
