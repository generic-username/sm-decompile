package org.schema.game.client.view.creaturetool.swing;

import java.util.Iterator;
import java.util.Vector;
import javax.swing.SpinnerModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.schema.game.client.view.creaturetool.CreatureTool;
import org.schema.schine.resource.CreatureStructure;

public class CreatureToolSpeedSpinnerModel implements SpinnerModel {
   private final CreatureTool tool;
   private Vector listeners = new Vector();
   private CreatureStructure.PartType[] types;

   public CreatureToolSpeedSpinnerModel(CreatureTool var1, CreatureStructure.PartType... var2) {
      this.tool = var1;
      this.types = var2;
   }

   public Object getValue() {
      if (this.tool.getCreature() != null) {
         System.err.println("GET VALUE: " + this.tool.getCreature() + " value " + this.tool.getCreature().getSpeed());
         return this.tool.getCreature().getSpeed();
      } else {
         System.err.println("VAL 0");
         return 0.0F;
      }
   }

   public void setValue(Object var1) {
      System.err.println("VALUE: " + this.tool.getCreature() + " value " + var1);
      if (this.tool.getCreature() != null && var1 != null) {
         try {
            this.tool.getCreature().setSpeed(Float.parseFloat(var1.toString()));
            CreatureStructure.PartType[] var6;
            int var2 = (var6 = this.types).length;

            for(int var3 = 0; var3 < var2; ++var3) {
               CreatureStructure.PartType var4 = var6[var3];
               this.tool.updateAnimSpeed(var4, this.tool.getCreature().getSpeed());
               this.tool.updateAnimSpeed(var4, this.tool.getCreature().getSpeed());
               this.tool.updateAnimSpeed(var4, this.tool.getCreature().getSpeed());
            }
         } catch (NumberFormatException var5) {
         }
      }

      this.fireChangeEvent();
   }

   public Object getNextValue() {
      return this.tool.getCreature() != null ? this.tool.getCreature().getSpeed() + 1.0F : 0.0F;
   }

   public Object getPreviousValue() {
      return this.tool.getCreature() != null ? this.tool.getCreature().getSpeed() - 1.0F : 0.0F;
   }

   public void addChangeListener(ChangeListener var1) {
      this.listeners.add(var1);
   }

   public void removeChangeListener(ChangeListener var1) {
      this.listeners.remove(var1);
   }

   public void fireChangeEvent() {
      Iterator var1 = this.listeners.iterator();

      while(var1.hasNext()) {
         ((ChangeListener)var1.next()).stateChanged(new ChangeEvent(this));
      }

   }
}
