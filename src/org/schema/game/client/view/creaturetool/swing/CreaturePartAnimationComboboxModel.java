package org.schema.game.client.view.creaturetool.swing;

import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndex;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndexElement;

public class CreaturePartAnimationComboboxModel implements ComboBoxModel {
   private AnimationIndexElement selected;

   public int getSize() {
      return AnimationIndex.animations.length;
   }

   public AnimationIndexElement getElementAt(int var1) {
      return AnimationIndex.animations[var1];
   }

   public void addListDataListener(ListDataListener var1) {
   }

   public void removeListDataListener(ListDataListener var1) {
   }

   public void setSelectedItem(Object var1) {
      this.selected = (AnimationIndexElement)var1;
   }

   public Object getSelectedItem() {
      return this.selected;
   }
}
