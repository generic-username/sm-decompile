package org.schema.game.client.view.creaturetool.swing;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.creaturetool.CreatureTool;
import org.schema.schine.resource.CreatureStructure;

public class CreatureToolEditPartPanel extends JPanel {
   private static final long serialVersionUID = 1L;
   private CreatureToolMainPanel creatureToolMainPanel;
   private CreatureToolPartDataset data;
   private JComboBox comboBox;
   private CreatureToolEditPartSettingsPanel creatureToolEditPartSettingsPanel;

   public CreatureToolEditPartPanel(CreatureStructure.PartType var1, GameClientState var2, CreatureTool var3, CreatureToolMainPanel var4) {
      this.creatureToolMainPanel = var4;
      GridBagLayout var5;
      (var5 = new GridBagLayout()).rowWeights = new double[]{0.0D, 1.0D};
      var5.columnWeights = new double[]{1.0D};
      this.setLayout(var5);
      JPanel var9;
      (var9 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Select", 4, 2, (Font)null, (Color)null));
      GridBagConstraints var6;
      (var6 = new GridBagConstraints()).weightx = 1.0D;
      var6.anchor = 18;
      var6.fill = 1;
      var6.insets = new Insets(0, 0, 5, 0);
      var6.gridx = 0;
      var6.gridy = 0;
      this.add(var9, var6);
      GridBagLayout var10;
      (var10 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var10.rowHeights = new int[]{0, 0};
      var10.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var10.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var9.setLayout(var10);
      this.comboBox = new JComboBox(new CreaturePartJComboboxModel(var1));
      this.comboBox.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            CreatureToolEditPartPanel.this.updateDataSet();
         }
      });
      (var6 = new GridBagConstraints()).weightx = 1.0D;
      var6.anchor = 15;
      var6.fill = 2;
      var6.gridx = 0;
      var6.gridy = 0;
      var9.add(this.comboBox, var6);
      this.creatureToolEditPartSettingsPanel = new CreatureToolEditPartSettingsPanel(var1, var2, var3, var4);
      this.creatureToolEditPartSettingsPanel.setBorder(new TitledBorder((Border)null, "Edit", 4, 2, (Font)null, (Color)null));
      GridBagConstraints var7;
      (var7 = new GridBagConstraints()).weightx = 1.0D;
      var7.fill = 1;
      var7.gridx = 0;
      var7.gridy = 1;
      this.add(this.creatureToolEditPartSettingsPanel, var7);
      GridBagLayout var8;
      (var8 = new GridBagLayout()).columnWidths = new int[]{0};
      var8.rowHeights = new int[]{0};
      var8.columnWeights = new double[]{Double.MIN_VALUE};
      var8.rowWeights = new double[]{Double.MIN_VALUE};
      this.creatureToolEditPartSettingsPanel.setLayout(var8);
   }

   private void updateDataSet() {
      if (!this.comboBox.getSelectedItem().toString().toLowerCase(Locale.ENGLISH).equals("none") && !this.comboBox.getSelectedItem().toString().toLowerCase(Locale.ENGLISH).equals("-1")) {
         this.data = new CreatureToolPartDataset();
         this.data.mesh = this.comboBox.getSelectedItem().toString();
      } else {
         this.data = null;
      }

      this.creatureToolMainPanel.update();
   }

   public CreatureToolPartDataset getData() {
      return this.data;
   }

   public void updateGUI() {
      this.creatureToolEditPartSettingsPanel.updateGUI();
   }
}
