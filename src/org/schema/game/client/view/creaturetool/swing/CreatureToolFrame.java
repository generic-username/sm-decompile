package org.schema.game.client.view.creaturetool.swing;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.creaturetool.CreatureTool;
import org.schema.schine.graphicsengine.core.GLFrame;

public class CreatureToolFrame extends JFrame {
   private static final long serialVersionUID = 1L;
   private JPanel contentPane;
   private CreatureTool creatureTool;

   public CreatureToolFrame(GameClientState var1, CreatureTool var2) {
      this.setBounds(GLFrame.getWidth() + 100, 50, 450, 700);
      this.contentPane = new JPanel();
      this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.contentPane.setLayout(new BorderLayout(0, 0));
      this.setContentPane(this.contentPane);
      CreatureToolMainPanel var3 = new CreatureToolMainPanel(var1, var2);
      this.contentPane.add(var3, "Center");
      this.creatureTool = var2;
   }

   public void dispose() {
      super.dispose();
      this.creatureTool.onGUIDispose();
   }
}
