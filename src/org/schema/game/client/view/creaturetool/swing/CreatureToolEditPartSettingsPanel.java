package org.schema.game.client.view.creaturetool.swing;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.character.AnimationNotSetException;
import org.schema.game.client.view.creaturetool.CreatureTool;
import org.schema.schine.resource.CreatureStructure;

public class CreatureToolEditPartSettingsPanel extends JPanel {
   private static final long serialVersionUID = 1L;
   private CreatureStructure.PartType type;
   private CreatureTool creatureTool;
   private CreatureToolMainPanel creatureToolMainPanel;
   private JLabel lblLoaded;
   private JComboBox comboBox;
   private JCheckBox chckbxOverrideParent;
   private JPanel panel_1;

   public CreatureToolEditPartSettingsPanel(CreatureStructure.PartType var1, GameClientState var2, CreatureTool var3, CreatureToolMainPanel var4) {
      this.type = var1;
      this.creatureTool = var3;
      this.creatureToolMainPanel = var4;
      GridBagLayout var5;
      (var5 = new GridBagLayout()).columnWeights = new double[]{1.0D};
      this.setLayout(var5);
      JPanel var6;
      (var6 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Animation", 4, 2, (Font)null, (Color)null));
      GridBagConstraints var7;
      (var7 = new GridBagConstraints()).weighty = 1.0D;
      var7.weightx = 1.0D;
      var7.anchor = 18;
      var7.fill = 1;
      var7.gridx = 0;
      var7.gridy = 0;
      this.add(var6, var7);
      GridBagLayout var8;
      (var8 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var8.rowHeights = new int[]{0, 0, 0, 0, 0};
      var8.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var8.rowWeights = new double[]{0.0D, 0.0D, 1.0D, 0.0D, Double.MIN_VALUE};
      var6.setLayout(var8);
      this.chckbxOverrideParent = new JCheckBox("Override Parent");
      this.chckbxOverrideParent.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            CreatureToolEditPartSettingsPanel.this.update();
         }
      });
      (var7 = new GridBagConstraints()).anchor = 17;
      var7.insets = new Insets(0, 0, 5, 0);
      var7.gridx = 0;
      var7.gridy = 0;
      var6.add(this.chckbxOverrideParent, var7);
      this.panel_1 = new JPanel();
      (var7 = new GridBagConstraints()).anchor = 18;
      var7.insets = new Insets(0, 0, 5, 0);
      var7.gridx = 0;
      var7.gridy = 2;
      var6.add(this.panel_1, var7);
      (var8 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var8.rowHeights = new int[]{0, 0};
      var8.columnWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var8.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      this.panel_1.setLayout(var8);
      this.lblLoaded = new JLabel("");
      (var7 = new GridBagConstraints()).gridx = 0;
      var7.gridy = 3;
      var6.add(this.lblLoaded, var7);
      this.comboBox = new JComboBox(new CreaturePartAnimationComboboxModel());
      this.comboBox.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            CreatureToolEditPartSettingsPanel.this.update();
         }
      });
      (var7 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var7.fill = 2;
      var7.gridx = 0;
      var7.gridy = 1;
      var6.add(this.comboBox, var7);
   }

   public void update() {
      if (this.chckbxOverrideParent.isSelected()) {
         try {
            this.creatureTool.updateForcedAnimation(this.type, this.comboBox.getSelectedItem());
            this.lblLoaded.setText("Loaded: " + this.comboBox.getSelectedItem());
         } catch (AnimationNotSetException var1) {
            System.err.println("COULD NOT SET ANIMATION! " + this.comboBox.getSelectedItem());
            this.lblLoaded.setText("NO ANIMATION SET FOR: " + this.comboBox.getSelectedItem());
         }
      } else {
         this.lblLoaded.setText("");
         this.creatureToolMainPanel.updateParenAnimation(this.type);
      }
   }

   public void updateGUI() {
      this.update();
   }
}
