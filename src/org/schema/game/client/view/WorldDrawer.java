package org.schema.game.client.view;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectArrayList;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.common.TimeStatistics;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.ClientSectorChangeListener;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.beam.BeamDrawerManager;
import org.schema.game.client.view.blackholedrawer.BlackHoleDrawer;
import org.schema.game.client.view.camera.drone.CameraDroneManager;
import org.schema.game.client.view.character.CharactersDrawer;
import org.schema.game.client.view.creaturetool.CreatureTool;
import org.schema.game.client.view.cubes.CubeBruteCollectionDrawer;
import org.schema.game.client.view.effects.Atmosphere;
import org.schema.game.client.view.effects.ConnectionDrawerManager;
import org.schema.game.client.view.effects.DepthBufferScene;
import org.schema.game.client.view.effects.EnergyStreamDrawerManager;
import org.schema.game.client.view.effects.ExhaustPlumes;
import org.schema.game.client.view.effects.ExplosionDrawer;
import org.schema.game.client.view.effects.FlareDrawerManager;
import org.schema.game.client.view.effects.ItemDrawer;
import org.schema.game.client.view.effects.LiftDrawer;
import org.schema.game.client.view.effects.MissileTrailDrawer;
import org.schema.game.client.view.effects.MuzzleFlash;
import org.schema.game.client.view.effects.PlanetCoreDrawer;
import org.schema.game.client.view.effects.PlumeAndMuzzleDrawer;
import org.schema.game.client.view.effects.Shadow;
import org.schema.game.client.view.effects.ShieldDrawerManager;
import org.schema.game.client.view.effects.SpaceParticleDrawer;
import org.schema.game.client.view.effects.TransporterEffectManager;
import org.schema.game.client.view.effects.segmentcontrollereffects.SegmentControllerEffectDrawer;
import org.schema.game.client.view.gamemap.GameMapDrawer;
import org.schema.game.client.view.gui.GUI3DBlockElement;
import org.schema.game.client.view.gui.GUIBlockConsistenceGraph;
import org.schema.game.client.view.gui.GuiDrawer;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.client.view.mines.MineDrawer;
import org.schema.game.client.view.occulus.OculusUtil;
import org.schema.game.client.view.planetdrawer.PlanetDrawer;
import org.schema.game.client.view.shards.Shard;
import org.schema.game.client.view.shards.ShardDrawer;
import org.schema.game.client.view.space.StarSkyNew;
import org.schema.game.client.view.sundrawer.SunDrawer;
import org.schema.game.client.view.textbox.AbstractTextBox;
import org.schema.game.client.view.tools.IconTextureBakery;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.physics.RigidDebrisBody;
import org.schema.game.common.data.world.ClientProximitySystem;
import org.schema.game.common.data.world.SectorInformation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.common.data.world.planet.Planet;
import org.schema.game.common.data.world.space.PlanetCore;
import org.schema.game.server.data.Galaxy;
import org.schema.schine.graphicsengine.OculusVrHelper;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GLException;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.util.GifEncoder;
import org.schema.schine.graphicsengine.util.timer.SinusTimerUtil;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.physics.Physical;
import org.schema.schine.resource.FileExt;

public class WorldDrawer extends AbstractSceneNode implements ClientSectorChangeListener {
   static final long STRUCTURE_BLINKING_MS = 1200L;
   public static boolean insideBuildMode;
   public static boolean flagTextureBake = false;
   public static boolean flagScreenShotWithGUI = false;
   public static boolean flagScreenShotWithoutGUI = false;
   public static boolean flagCreatureTool;
   public static boolean flagRecipeTrees;
   static SegmentController justEntered;
   static long justEnteredStart;
   private final LiftDrawer liftDrawer;
   private final PlumeAndMuzzleDrawer plumAndMuzzleDrawer;
   private final BuildModeDrawer buildModeDrawer;
   private final GameMapDrawer gameMapDrawer;
   private final CharactersDrawer characterDrawer;
   private final TransporterEffectManager transporterEffectManager;
   private final BeamDrawerManager beamDrawerManager;
   private final ShieldDrawerManager shieldDrawerManager;
   private final FlareDrawerManager flareDrawerManager;
   private final CubeBruteCollectionDrawer cubeBruteCollectionDrawer = new CubeBruteCollectionDrawer();
   private final ShardDrawer shards = new ShardDrawer();
   public Shadow shadow;
   public GifEncoder gifEncoder;
   public Vector3f secondSunLightPos;
   public Vector4f sunColor = new Vector4f();
   public Vector4f secondSunColor = new Vector4f();
   long diffAcc;
   Transform tmp = new Transform();
   FrameBufferObjects currentFBO;
   private final GameClientState state;
   private OculusUtil oculusUtil = new OculusUtil();
   private final Atmosphere atmosphere;
   private final StarSkyNew starSky;
   private final PlanetCoreDrawer planetCoreDrawer;
   private final ExplosionDrawer explosionDrawer;
   private final SpaceParticleDrawer spaceParticleDrawer;
   private final MissileTrailDrawer trailDrawer;
   private SegmentControllerEffectDrawer segmentControllerEffectDrawer;
   private final List projectileDrawers;
   private SinusTimerUtil arrowSineUtil = new SinusTimerUtil(9.0F);
   private PlanetDrawer planetDrawer;
   private final BlackHoleDrawer blackHoleDrawer;
   private final SunDrawer sunDrawer;
   private final SunDrawer sunDistDrawer;
   private final SunDrawer sunDrawerSec;
   private final ConnectionDrawerManager connectionDrawerManager;
   private GuiDrawer guiDrawer;
   private final EnergyStreamDrawerManager energyStreamDrawerManager;
   private final SegmentDrawer segmentDrawer;
   private CreatureTool creatureTool;
   private IconTextureBakery bakery;
   private final ItemDrawer itemDrawer;
   private Integer[] specialSectorsBuffer;
   private Integer[] specialSectors = new Integer[0];
   private int drawingSpecialSectorsId = -2;
   private final AbstractTextBox testBox;
   private Vector3f absoluteMousePosition = new Vector3f();
   private boolean flagManagedSegmentControllerUpdate;
   private boolean flagSegmentControllerUpdate;
   private boolean flagCharacterUpdate;
   private boolean wasOnPlanet;
   private boolean flagPlanetCoreUpdate;
   private Vector3i tmpSysPos = new Vector3i();
   private Vector3i tmpSecPos = new Vector3i();
   private Vector3i tmpSecPosCurrent = new Vector3i();
   private Vector3i extPos = new Vector3i();
   private Vector3i extOffset = new Vector3i();
   private Vector3i extPosMod = new Vector3i();
   public boolean initialized;
   boolean first = true;
   private List drawables = new ObjectArrayList();
   private boolean planetTest;
   private int i;
   private MineDrawer mineDrawer;
   private final CameraDroneManager cameraDroneManager;
   public static boolean drawError;
   Transform tmpTrans = new Transform();
   Vector3f absSectorPos = new Vector3f();
   Matrix3f rot = new Matrix3f();

   public WorldDrawer(GameClientState var1) {
      this.state = var1;
      this.projectileDrawers = new ObjectArrayList();
      this.drawables.add(this.beamDrawerManager = new BeamDrawerManager(var1));
      this.drawables.add(this.segmentDrawer = new SegmentDrawer(var1));
      this.drawables.add(this.shieldDrawerManager = new ShieldDrawerManager(var1));
      this.drawables.add(this.flareDrawerManager = new FlareDrawerManager());
      this.drawables.add(this.buildModeDrawer = new BuildModeDrawer(var1));
      this.drawables.add(this.gameMapDrawer = new GameMapDrawer(var1));
      this.drawables.add(this.liftDrawer = new LiftDrawer());
      this.drawables.add(this.guiDrawer = new GuiDrawer(var1.getGUIController()));
      this.drawables.add(this.planetDrawer = new PlanetDrawer(var1));
      this.drawables.add(this.blackHoleDrawer = new BlackHoleDrawer());
      this.drawables.add(this.sunDrawer = new SunDrawer(var1));
      this.drawables.add(this.sunDistDrawer = new SunDrawer(var1));
      this.drawables.add(this.sunDrawerSec = new SunDrawer(var1));
      this.drawables.add(this.starSky = new StarSkyNew(var1));
      this.drawables.add(this.transporterEffectManager = new TransporterEffectManager(var1));
      this.drawables.add(this.characterDrawer = new CharactersDrawer(var1));
      this.drawables.add(this.plumAndMuzzleDrawer = new PlumeAndMuzzleDrawer());
      this.drawables.add(this.explosionDrawer = new ExplosionDrawer());
      this.drawables.add(this.planetCoreDrawer = new PlanetCoreDrawer());
      this.drawables.add(this.spaceParticleDrawer = new SpaceParticleDrawer());
      this.drawables.add(this.segmentControllerEffectDrawer = new SegmentControllerEffectDrawer(var1));
      this.drawables.add(this.testBox = new AbstractTextBox(var1));
      this.drawables.add(this.trailDrawer = new MissileTrailDrawer(var1));
      this.drawables.add(this.connectionDrawerManager = new ConnectionDrawerManager(var1));
      this.drawables.add(this.energyStreamDrawerManager = new EnergyStreamDrawerManager(var1));
      this.drawables.add(this.itemDrawer = new ItemDrawer(var1));
      this.drawables.add(this.atmosphere = new Atmosphere(var1));
      this.drawables.add(this.cameraDroneManager = new CameraDroneManager(var1));
      this.drawables.add(this.mineDrawer = new MineDrawer(var1));
      ProjectileCannonDrawerVBO var2 = new ProjectileCannonDrawerVBO(var1.getParticleController());
      this.projectileDrawers.add(var2);
      this.drawables.add(var2);
      var1.getController().addSectorChangeListener(this);
   }

   public void cleanUp() {
      Iterator var1 = this.drawables.iterator();

      while(var1.hasNext()) {
         ((Drawable)var1.next()).cleanUp();
      }

   }

   public void prepareCamera() {
      GlUtil.glMatrixMode(5889);
      GlUtil.glPushMatrix();
      float var1 = (float)GLFrame.getWidth() / (float)GLFrame.getHeight();
      if (!EngineSettings.O_OCULUS_RENDERING.isOn()) {
         GlUtil.gluPerspective(Controller.projectionMatrix, (Float)EngineSettings.G_FOV.getCurrentState() * AbstractScene.getZoomFactorForRender(!this.getGameMapDrawer().isMapActive()), var1, 0.05F, this.state.getSectorSize() * 2.0F, true);
      } else {
         Matrix4f var2 = GlUtil.gluPerspective(Controller.projectionMatrix, (Float)EngineSettings.G_FOV.getCurrentState() * AbstractScene.getZoomFactorForRender(!this.getGameMapDrawer().isMapActive()), OculusVrHelper.getAspectRatio(), 0.05F, this.state.getSectorSize() * 2.0F, true);
         Matrix4f.mul(Controller.occulusProjMatrix, var2, var2);
         GlUtil.glLoadMatrix(var2);
      }

      GlUtil.glMatrixMode(5888);
      Controller.getCamera().updateFrustum();
   }

   public void retrieveMousePosition() {
      if (PlayerInteractionControlManager.isAdvancedBuildMode(this.state) && this.isInStructureBuildMode()) {
         this.absoluteMousePosition = AbstractScene.getAbsoluteMousePosition(this.absoluteMousePosition, this.currentFBO);
      }

   }

   public void draw() {
      PlanetDrawer.culled = 0;
      drawError = false;
      if (this.first || Keyboard.isKeyDown(60) || this.i % 10000 == 0) {
         GlUtil.printGlError();
         drawError = true;
         this.first = false;
      }

      ++this.i;
      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      SectorInformation.PlanetType var1 = null;
      boolean var2 = false;
      Vector3i var3 = Galaxy.getRelPosInGalaxyFromAbsSystem(this.tmpSysPos, new Vector3i());
      if (!this.state.getCurrentGalaxy().isVoid(var3.x, var3.y, var3.z)) {
         this.sunDrawer.drawPlasma();
      }

      this.segmentControllerEffectDrawer.draw();
      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      if (this.state.getPlayer().getProximitySector().getSectorId() != this.drawingSpecialSectorsId) {
         synchronized(this.state.getPlayer().getProximitySector()) {
            this.drawingSpecialSectorsId = this.state.getPlayer().getProximitySector().getSectorId();
            this.updateSpecialSectors();
         }
      }

      if (this.specialSectorsBuffer != null) {
         this.specialSectors = this.specialSectorsBuffer;
         this.specialSectorsBuffer = null;
      }

      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      this.tmp.setIdentity();
      var3 = new Vector3i();
      boolean var4 = false;
      long var6 = this.state.getController().calculateStartTime();
      float var5 = this.state.getGameState().getRotationProgession();

      for(int var9 = 0; var9 < this.specialSectors.length; ++var9) {
         int var10 = this.specialSectors[var9];
         this.state.getPlayer().getProximitySector().getPosFromIndex(var10, var3);
         SectorInformation.SectorType var11 = SectorInformation.SectorType.values()[this.state.getPlayer().getProximitySector().getSectorType(var10)];
         SectorInformation.PlanetType.values();
         this.state.getPlayer().getProximitySector().getPlanetType(var10);
         if (var11 == SectorInformation.SectorType.PLANET) {
            if (this.state.getPlayer().getCurrentSector().equals(var3)) {
               var1 = SectorInformation.PlanetType.values()[this.state.getPlayer().getProximitySector().getPlanetType(var10)];
               this.atmosphere.setPlanetType(var1);
               var2 = this.atmosphere.isInside();
               var4 = true;
            } else {
               Vector3i var8;
               (var8 = new Vector3i(var3)).sub(this.state.getPlayer().getCurrentSector());
               this.tmp.basis.setIdentity();
               StellarSystem.getPosFromSector(var3, new Vector3i());
               this.planetDrawer.drawFromPlanet = var4 || this.wasOnPlanet;
               this.planetDrawer.year = var5;
               this.planetDrawer.setPlanetSectorPos(var8);
               this.planetDrawer.setPlanetType(SectorInformation.PlanetType.values()[this.state.getPlayer().getProximitySector().getPlanetType(var10)]);
               this.planetDrawer.absSecPos = var3;
               this.planetDrawer.draw();
               GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            }
         }
      }

      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      this.wasOnPlanet = var4;
      this.getStarSky().setDrawFromPlanet(var4);
      this.getStarSky().setYear(var5);
      GL11.glDepthRange(0.0D, 1.0D);
      this.prepareCamera();
      GL11.glClear(256);
      GL11.glDepthRange(0.0D, 1.0D);
      if (var4 && !var2) {
         this.atmosphere.draw();
      }

      if (this.shadow == null) {
         this.getCharacterDrawer().draw();
         if (this.creatureTool != null) {
            this.creatureTool.draw();
         }
      }

      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      long var19 = System.currentTimeMillis();
      this.connectionDrawerManager.draw();
      this.energyStreamDrawerManager.draw();
      long var20;
      if ((var20 = System.currentTimeMillis() - var19) > 30L) {
         System.err.println("[CLIENT] CONNECTION DRAWING TOOK " + var20);
      }

      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      this.liftDrawer.draw();
      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      this.drawProjectiles(0.0F);
      this.mineDrawer.draw();
      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      this.getBeamDrawerManager().prepareSorted();
      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      this.getBeamDrawerManager().drawSalvageBoxes();
      TimeStatistics.reset("SEGMENTS");
      if (this.shadow != null) {
         this.shadow.renderScene();
      } else {
         this.segmentDrawer.setSegmentRenderPass(SegmentDrawer.SegmentRenderPass.OPAQUE);
         this.segmentDrawer.draw();
         this.getShards().draw();
      }

      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      this.segmentDrawer.drawTextBoxes();
      this.state.getParticleSystemManager().draw();
      TimeStatistics.set("SEGMENTS");
      this.cubeBruteCollectionDrawer.draw();
      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      this.itemDrawer.draw();
      this.cameraDroneManager.draw();
      this.explosionDrawer.drawShieldBubbled();
      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      try {
         if (justEntered != null && System.currentTimeMillis() - justEnteredStart < 1200L && this.state.getGlobalGameControlManager() != null && this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().isTreeActive()) {
            if (justEntered.blinkTime == 0L) {
               justEntered.blinkTime = System.currentTimeMillis();
            }

            System.currentTimeMillis();
            long var10000 = justEnteredStart;
            GlUtil.glPushMatrix();
            Mesh var15 = (Mesh)Controller.getResLoader().getMesh("Arrow").getChilds().get(0);
            Transform var16 = new Transform(justEntered.getWorldTransform());
            Vector3f var17;
            (var17 = new Vector3f(0.0F, 0.0F, 1.0F)).scale(this.arrowSineUtil.getTime());
            var16.basis.transform(var17);
            var16.origin.add(var17);
            GlUtil.glMultMatrix(var16);
            GlUtil.scaleModelview(0.3F, 0.3F, 0.3F);
            GlUtil.glEnable(3042);
            GlUtil.glBlendFunc(770, 771);
            GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
            GlUtil.glEnable(2903);
            GlUtil.glColor4f(1.0F, 1.0F, 1.0F, this.arrowSineUtil.getTime());
            var15.draw();
            GlUtil.glPopMatrix();
            GlUtil.glDisable(2903);
            GlUtil.glDisable(3042);
            GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         } else {
            justEntered = null;
            justEnteredStart = 0L;
         }
      } catch (NullPointerException var14) {
         var14.printStackTrace();
      }

      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      if (var2) {
         this.atmosphere.draw();
      }

      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      this.getPlumAndMuzzleDrawer().draw();
      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      this.spaceParticleDrawer.onPlanet(this.atmosphere.isInside());
      this.spaceParticleDrawer.draw();
      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      this.state.getPulseController().draw();
      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      this.flareDrawerManager.draw();
      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      this.shieldDrawerManager.draw();
      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      this.planetCoreDrawer.draw();
      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      this.transporterEffectManager.draw();
      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      this.trailDrawer.draw();
      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      if (var1 != null && !var2) {
         this.planetDrawer.drawFromPlanet = false;
         this.planetDrawer.setPlanetSectorPos(new Vector3i(0, 0, 0));
         this.planetDrawer.setPlanetType(var1);
         this.planetDrawer.draw();
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      }

      boolean var18;
      if (var18 = this.getGameMapDrawer().doDraw()) {
         this.getGameMapDrawer().draw();
      }

      GlUtil.glMatrixMode(5889);
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5888);
      Controller.getCamera().updateFrustum();
      if (!var18) {
         if (drawError) {
            GlUtil.printGlErrorCritical();
         }

         this.drawSystems(var6, var4);
      }

      this.prepareCamera();
      if (!var18) {
         this.segmentDrawer.setSegmentRenderPass(SegmentDrawer.SegmentRenderPass.TRANSPARENT);
         this.segmentDrawer.draw();
         this.buildModeDrawer.draw();
      }

      boolean var21 = this.planetTest;
      GlUtil.glMatrixMode(5889);
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5888);
      Controller.getCamera().updateFrustum();
      if (EngineSettings.P_PHYSICS_DEBUG_MODE.isOn()) {
         this.displayPhysicsInfo();
      }

      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      SegmentDrawer.forceFullLightingUpdate = false;
   }

   public void drawPointExplosions() {
      this.explosionDrawer.drawPoints();
   }

   public void drawProjectiles(float var1) {
      for(int var2 = 0; var2 < this.projectileDrawers.size(); ++var2) {
         ((ProjectileCannonDrawerVBO)this.projectileDrawers.get(var2)).setZoomFac(var1);
         ((ProjectileCannonDrawerVBO)this.projectileDrawers.get(var2)).draw();
      }

   }

   public void onInit() {
      Iterator var1 = this.drawables.iterator();

      while(var1.hasNext()) {
         ((Drawable)var1.next()).onInit();
      }

      this.initialized = true;
   }

   public void clearAll() {
      this.getPlumAndMuzzleDrawer().clear();
      this.shieldDrawerManager.clear();
      this.getBeamDrawerManager().clear();
      this.segmentDrawer.clearSegmentControllers();
      this.getCharacterDrawer().clear();
      this.connectionDrawerManager.clear();
      this.energyStreamDrawerManager.clear();
   }

   public AbstractSceneNode clone() {
      return null;
   }

   public void update(Timer var1) {
      if (this.initialized) {
         TimeStatistics.reset("update");
         this.oculusUtil.update(var1);
         long var2 = System.currentTimeMillis();
         long var4;
         if ((var4 = System.currentTimeMillis() - var2) > 10L) {
            System.err.println("[DRAWER][WARNING] synUPDATE took " + var4 + " ms");
         }

         var2 = System.currentTimeMillis();
         if (this.flagSegmentControllerUpdate) {
            this.segmentDrawer.updateSegmentControllerSet();
            this.flagSegmentControllerUpdate = false;
         }

         this.shards.update(var1, this.state);
         this.connectionDrawerManager.update(var1);
         this.energyStreamDrawerManager.update(var1);
         if (flagCreatureTool) {
            if (this.creatureTool == null) {
               this.creatureTool = new CreatureTool(this.state, var1);
            } else {
               this.creatureTool.onDiasble();
               this.creatureTool = null;
            }

            flagCreatureTool = false;
         }

         this.mineDrawer.update(var1);
         this.segmentDrawer.textBox.update(var1);
         if (this.flagPlanetCoreUpdate) {
            this.planetCoreDrawer.setCore((PlanetCore)null);
            Iterator var6 = this.state.getCurrentSectorEntities().values().iterator();

            while(var6.hasNext()) {
               Sendable var7;
               if ((var7 = (Sendable)var6.next()) instanceof PlanetCore && ((PlanetCore)var7).getSectorId() == this.state.getCurrentSectorId()) {
                  this.planetCoreDrawer.setCore((PlanetCore)var7);
               }
            }

            this.flagPlanetCoreUpdate = false;
         }

         long var10;
         if ((var10 = System.currentTimeMillis() - var2) > 10L) {
            System.err.println("[DRAWER][WARNING] seg controller set update took " + var10 + " ms");
         }

         var2 = System.currentTimeMillis();
         if (this.flagManagedSegmentControllerUpdate) {
            this.segManControllerUpdate();
            this.flagManagedSegmentControllerUpdate = false;
         }

         long var8;
         if ((var8 = System.currentTimeMillis() - var2) > 10L) {
            System.err.println("[DRAWER][WARNING] segManControllerUpdate took " + var8 + " ms");
         }

         if (this.flagCharacterUpdate) {
            this.getCharacterDrawer().updateCharacterSet(var1);
            this.flagCharacterUpdate = false;
         }

         this.arrowSineUtil.update(var1);
         this.getStarSky().update(var1);
         this.getCharacterDrawer().update(var1);
         this.planetDrawer.update(var1);
         this.blackHoleDrawer.update(var1);
         this.sunDrawer.update(var1);
         this.sunDrawerSec.update(var1);
         this.transporterEffectManager.updateLocal(var1);
         this.getGameMapDrawer().update(var1);
         TimeStatistics.reset("update sDrawer");
         this.segmentDrawer.update(var1);
         TimeStatistics.set("update sDrawer");
         if (TimeStatistics.get("update sDrawer") > 15L) {
            System.err.println("[DRAWER][WARNING] SegDrawer update took " + TimeStatistics.get("update sDrawer") + " ms");
         }

         this.explosionDrawer.update(var1);
         this.planetCoreDrawer.update(var1);
         if (EngineSettings.G_SPACE_PARTICLE.isOn()) {
            this.spaceParticleDrawer.update(var1);
         }

         this.shieldDrawerManager.update(var1);
         this.flareDrawerManager.update(var1);
         this.getPlumAndMuzzleDrawer().update(var1);
         this.trailDrawer.update(var1);
         this.segmentControllerEffectDrawer.update(var1);
         this.guiDrawer.update(var1);
         this.getBeamDrawerManager().update(var1);
         TimeStatistics.set("update");
         if (TimeStatistics.get("update") > 15L) {
            System.err.println("[DRAWER][WARNING] update took " + TimeStatistics.get("update") + " ms");
         }

      }
   }

   public void displayPhysicsInfo() {
      StringBuffer var1;
      (var1 = new StringBuffer()).append("Physics Data (!!warning slow): ");
      Iterator var2 = this.state.getPhysics().getDynamicsWorld().getCollisionObjectArray().iterator();

      while(var2.hasNext()) {
         CollisionObject var3 = (CollisionObject)var2.next();
         var1.append(var3.getCollisionShape().getClass().getSimpleName() + ": ");
         Iterator var4 = this.state.getCurrentSectorEntities().values().iterator();

         while(var4.hasNext()) {
            Sendable var5;
            if ((var5 = (Sendable)var4.next()) instanceof Physical && ((Physical)var5).getPhysicsDataContainer().getObject() == var3) {
               var1.append(var5);
            }
         }

         var1.append("; \n");
      }

      AbstractScene.infoList.add(var1.toString());
   }

   private void doScreenShot() {
      File[] var1 = (new FileExt("./")).listFiles();
      int var2 = 0;
      boolean var3 = true;

      while(true) {
         while(var3) {
            var3 = false;
            File[] var4 = var1;
            int var5 = var1.length;

            for(int var6 = 0; var6 < var5; ++var6) {
               if (var4[var6].getName().startsWith("starmade-screenshot-" + StringTools.formatFourZero(var2) + ".png")) {
                  System.err.println("Screen Already Exists: ./starmade-screenshot-" + StringTools.formatFourZero(var2) + ".png");
                  ++var2;
                  var3 = true;
                  break;
               }
            }
         }

         GlUtil.writeScreenToDisk("./starmade-screenshot-" + StringTools.formatFourZero(var2), "png", GLFrame.getWidth(), GLFrame.getHeight(), 4, this.currentFBO);
         return;
      }
   }

   public void drawGUI() {
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      if (flagScreenShotWithoutGUI) {
         this.doScreenShot();
         flagScreenShotWithoutGUI = false;
      }

      if (this.gifEncoder != null && !EngineSettings.GIF_GUI.isOn()) {
         GlUtil.writeScreenToGif(this.gifEncoder, GLFrame.getWidth(), GLFrame.getHeight(), 4, this.currentFBO);
      }

      if (GraphicsContext.isScreenSettingChanging()) {
         if (this.guiDrawer != null) {
            this.guiDrawer.cleanUp();
            this.drawables.remove(this.guiDrawer);
         }

         this.guiDrawer = new GuiDrawer(this.state.getGUIController());
         this.guiDrawer.onInit();
         this.drawables.add(this.guiDrawer);
      }

      TimeStatistics.reset("GUI");
      this.guiDrawer.draw();
      TimeStatistics.set("GUI");
      if (this.gifEncoder != null && EngineSettings.GIF_GUI.isOn()) {
         GlUtil.writeScreenToGif(this.gifEncoder, GLFrame.getWidth(), GLFrame.getHeight(), 4, this.currentFBO);
      }

      if (flagScreenShotWithGUI) {
         this.doScreenShot();
         flagScreenShotWithGUI = false;
      }

      GUI3DBlockElement.setMatrix();
      if (EngineSettings.T_ENABLE_TEXTURE_BAKER.isOn()) {
         if (this.bakery == null) {
            this.bakery = new IconTextureBakery();
         }

         this.bakery.drawTest();
      }

      if (flagRecipeTrees) {
         try {
            GUIBlockConsistenceGraph.bake(this.currentFBO, this.state);
         } catch (GLException var3) {
            var3.printStackTrace();
         }

         flagRecipeTrees = false;
      }

      if (flagTextureBake) {
         IconTextureBakery var1 = new IconTextureBakery();
         if (this.bakery == null) {
            this.bakery = new IconTextureBakery();
         }

         var1.sheetNumber = this.bakery.sheetNumber;

         try {
            var1.bake(this.currentFBO);
         } catch (GLException var2) {
            var2.printStackTrace();
         }

         flagTextureBake = false;
      }

   }

   public void drawSunBallForGodRays() {
      this.state.getScene().getMainLight().draw();
      Vector3f var1 = AbstractScene.mainLight.getPos();
      Vector3i var2 = Galaxy.getLocalCoordinatesFromSystem(this.state.getPlayer().getCurrentSystem(), new Vector3i());
      int var3 = this.state.getCurrentGalaxy().getSystemType(var2);
      if (!Galaxy.USE_GALAXY || var3 != 2 && !this.state.getCurrentGalaxy().isVoid(var2)) {
         if (!this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getMapControlManager().isTreeActive()) {
            GlUtil.glPushMatrix();
            GlUtil.translateModelview(var1.x, var1.y, var1.z);
            float var5 = var3 == 1 ? 9.0F : 4.0F;
            float var4;
            if ((var4 = (float)Math.pow((double)var1.length(), 1.2999999523162842D)) < 30000.0F) {
               var4 /= 30000.0F;
               var5 *= var4;
            }

            Controller.getResLoader().getMesh("Sphere").setScale(var5, var5, var5);
            Controller.getResLoader().getMesh("Sphere").draw();
            GlUtil.glPopMatrix();
            if (this.secondSunLightPos != null) {
               GlUtil.glPushMatrix();
               GlUtil.translateModelview(this.secondSunLightPos.x, this.secondSunLightPos.y, this.secondSunLightPos.z);
               var5 = 4.0F;
               if ((var4 = (float)Math.pow((double)var1.length(), 1.2999999523162842D)) < 30000.0F) {
                  var4 /= 30000.0F;
                  var5 = 4.0F * var4;
               }

               Controller.getResLoader().getMesh("Sphere").setScale(var5, var5, var5);
               Controller.getResLoader().getMesh("Sphere").draw();
               GlUtil.glPopMatrix();
            }

         }
      }
   }

   public void addShard(RigidDebrisBody var1, Mesh var2, short var3, int var4, Vector3f var5) {
      Shard var6;
      (var6 = new Shard(var1, var2, var4, var5)).setType(var3);
      this.getShards().add(var6);
   }

   private void drawSystems(long var1, boolean var3) {
      ClientProximitySystem var4 = this.state.getPlayer().getProximitySystem();
      new Vector3i();
      Vector3i var5 = this.state.getPlayer().getCurrentSystem();
      Vector3i var9;
      Vector3i var11;
      if (Galaxy.USE_GALAXY && !this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getMapControlManager().isTreeActive()) {
         this.sunDistDrawer.setUseQueries(false);
         this.sunDistDrawer.setUseLensFlare(false);
         this.sunDistDrawer.setDepthTest(true);

         for(int var6 = -1; var6 < 2; ++var6) {
            for(int var7 = -1; var7 < 2; ++var7) {
               for(int var8 = -1; var8 < 2; ++var8) {
                  this.extPosMod.set(var5);
                  this.extPosMod.add(var8, var7, var6);
                  var9 = Galaxy.getLocalCoordinatesFromSystem(this.extPosMod, this.extPos);
                  if (!this.state.getCurrentGalaxy().isVoid(var9)) {
                     int var10 = this.state.getCurrentGalaxy().getSystemType(var9);
                     var11 = this.state.getCurrentGalaxy().getSunPositionOffset(var9, this.extOffset);
                     Vector4f var16 = this.state.getCurrentGalaxy().getSunColor(var9);
                     this.tmpSecPos.set((var5.x + var8 << 4) + 8 + var11.x, (var5.y + var7 << 4) + 8 + var11.y, (var5.z + var6 << 4) + 8 + var11.z);
                     Vector3i var12;
                     (var12 = new Vector3i(this.tmpSecPos)).sub(this.state.getPlayer().getCurrentSector());
                     this.setMainLight(this.tmpSecPos, var1, this.tmpSysPos, var12, var3, this.tmp);
                     if (var10 == 2) {
                        this.blackHoleDrawer.draw();
                     } else if (var8 != 0 || var7 != 0 || var6 != 0) {
                        if (var10 == 1) {
                           this.sunDistDrawer.setColor(var16);
                           this.sunDistDrawer.sizeMult = 0.43F;
                           this.sunDistDrawer.draw();
                        } else if (var10 == 3) {
                           this.sunDistDrawer.setColor(var16);
                           this.sunDistDrawer.sizeMult = 0.15F;
                           this.sunDistDrawer.draw();
                           var9 = VoidSystem.getSecond(this.tmpSecPos, var11, new Vector3i());
                           (var12 = new Vector3i(var9)).sub(this.state.getPlayer().getCurrentSector());
                           this.setMainLight(var9, var1, this.tmpSysPos, var12, var3, this.tmp);
                           this.sunDistDrawer.draw();
                        } else if (var10 == 0) {
                           this.sunDistDrawer.setColor(var16);
                           this.sunDistDrawer.sizeMult = 0.15F;
                           this.sunDistDrawer.draw();
                        }
                     }
                  }
               }
            }
         }

         this.sunDistDrawer.setColor(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
         this.sunDistDrawer.sizeMult = 1.0F;
         this.sunDistDrawer.setDepthTest(false);
         this.sunDistDrawer.setUseLensFlare(true);
         this.sunDistDrawer.setUseQueries(true);
      }

      StellarSystem.getPosFromSector(this.state.getPlayer().getCurrentSector(), this.tmpSysPos);
      if (!var4.getBasePosition().equals(this.tmpSysPos)) {
         System.err.println("[CLIENT] WARNING: System not yet right: " + this.tmpSysPos + " / " + var4.getBasePosition());
      } else {
         this.tmpSecPos.set((this.tmpSysPos.x << 4) + 8, (this.tmpSysPos.y << 4) + 8, (this.tmpSysPos.z << 4) + 8);
         Vector3i var13 = Galaxy.getRelPosInGalaxyFromAbsSystem(this.tmpSysPos, new Vector3i());
         Vector3i var14 = this.state.getCurrentGalaxy().getSunPositionOffset(var13, new Vector3i());
         this.tmpSecPos.add(var14);
         Vector3i var15;
         (var15 = new Vector3i(this.tmpSecPos)).sub(this.state.getPlayer().getCurrentSector());
         this.setMainLight(this.tmpSecPos, var1, this.tmpSysPos, var15, var3, this.tmp);
         GL11.glDepthRange(0.0D, 1.0D);
         int var17 = this.state.getCurrentGalaxy().getSystemType(var13);
         this.sunDrawer.setRelativeSectorPos(var15);
         this.sunColor.set(this.state.getCurrentGalaxy().getSunColor(var13));
         this.sunDrawer.sizeMult = var17 == 1 ? 3.0F : 1.0F;
         boolean var18 = var17 == 3 || var17 == 1 || var17 == 0;
         if (!this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getMapControlManager().isTreeActive() && var18) {
            if (!EngineSettings.F_BLOOM.isOn() && this.state.getCurrentGalaxy().isStellarSystem(var13)) {
               if (Galaxy.USE_GALAXY) {
                  this.sunDrawer.setColor(this.sunColor);
               }

               this.sunDrawer.draw();
            }

            if (Galaxy.USE_GALAXY && var17 == 3) {
               var11 = VoidSystem.getSecond(this.tmpSecPos, var14, new Vector3i());
               (var9 = new Vector3i(var11)).sub(this.state.getPlayer().getCurrentSector());
               this.sunDrawerSec.setRelativeSectorPos(var9);
               this.secondSunColor.set(this.state.getCurrentGalaxy().getSunColor(var13.x + 30, var13.y + 30, var13.z + 30));
               this.setMainLight(var11, var1, this.tmpSysPos, var9, var3, this.tmp);
               this.secondSunLightPos = new Vector3f(AbstractScene.mainLight.getPos());
               if (!EngineSettings.F_BLOOM.isOn()) {
                  this.sunDrawerSec.setColor(this.secondSunColor);
                  this.sunDrawerSec.draw();
               }

               this.setMainLight(this.tmpSecPos, var1, this.tmpSysPos, var15, var3, this.tmp);
            } else {
               this.secondSunLightPos = null;
            }
         }

         AbstractScene.infoList.add("##### SEC/SYS " + this.tmpSysPos + " ; Abs: " + AbstractScene.mainLight.getPos());
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      }
   }

   public void flagJustEntered(SegmentController var1) {
      justEntered = var1;
      justEnteredStart = System.currentTimeMillis();
   }

   public Vector3f getAbsoluteMousePosition() {
      return this.absoluteMousePosition;
   }

   public BeamDrawerManager getBeamDrawerManager() {
      return this.beamDrawerManager;
   }

   public BuildModeDrawer getBuildModeDrawer() {
      return this.buildModeDrawer;
   }

   public ConnectionDrawerManager getConnectionDrawerManager() {
      return this.connectionDrawerManager;
   }

   public CubeBruteCollectionDrawer getCubeBruteCollectionDrawer() {
      return this.cubeBruteCollectionDrawer;
   }

   public ExplosionDrawer getExplosionDrawer() {
      return this.explosionDrawer;
   }

   public FlareDrawerManager getFlareDrawerManager() {
      return this.flareDrawerManager;
   }

   public GameMapDrawer getGameMapDrawer() {
      return this.gameMapDrawer;
   }

   public GuiDrawer getGuiDrawer() {
      return this.guiDrawer;
   }

   public LiftDrawer getLiftDrawer() {
      return this.liftDrawer;
   }

   public PlumeAndMuzzleDrawer getPlumAndMuzzleDrawer() {
      return this.plumAndMuzzleDrawer;
   }

   public List getProjectileDrawers() {
      return this.projectileDrawers;
   }

   public SegmentDrawer getSegmentDrawer() {
      return this.segmentDrawer;
   }

   public ShieldDrawerManager getShieldDrawerManager() {
      return this.shieldDrawerManager;
   }

   public StarSkyNew getStarSky() {
      return this.starSky;
   }

   public MissileTrailDrawer getTrailDrawer() {
      return this.trailDrawer;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      Keyboard.enableRepeatEvents(true);
      if (EngineSettings.T_ENABLE_TEXTURE_BAKER.isOn() && this.bakery != null) {
         this.bakery.handleKeyEvent(var1);
      }

      Keyboard.enableRepeatEvents(false);
   }

   public boolean isFlagCharacterUpdate() {
      return this.flagCharacterUpdate;
   }

   public void setFlagCharacterUpdate(boolean var1) {
      this.flagCharacterUpdate = var1;
   }

   public boolean isFlagManagedSegmentControllerUpdate() {
      return this.flagManagedSegmentControllerUpdate;
   }

   public void setFlagManagedSegmentControllerUpdate(boolean var1) {
      this.flagManagedSegmentControllerUpdate = var1;
   }

   public boolean isInStructureBuildMode() {
      return this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().isInAnyStructureBuildMode();
   }

   public void onEndFrame() {
      TimeStatistics.reset("CONTEXT");
      this.segmentDrawer.handleContextSwitches();
      SegmentControllerEffectDrawer.unaffectedTranslation = null;
      TimeStatistics.set("CONTEXT");
      this.state.getController().onEndFrame();
   }

   public void onStartFrame() {
      if (this.segmentDrawer != null) {
         this.segmentDrawer.checkSamples();
      }

      if (this.sunDrawer != null) {
         this.sunDrawer.checkSamples();
      }

      if (this.sunDrawerSec != null) {
         this.sunDrawerSec.checkSamples();
      }

      this.state.getController().onStartFrame();
   }

   public void resetCamera() {
      Controller.getCamera().reset();
   }

   private void segManControllerUpdate() {
      long var1 = System.currentTimeMillis();
      this.getPlumAndMuzzleDrawer().clear();
      this.shieldDrawerManager.clear();
      long var3;
      if ((var3 = System.currentTimeMillis() - var1) > 5L) {
         System.err.println("[WORLDDRAWER] WARNING: CLEAR TOOK " + var3);
      }

      this.getBeamDrawerManager().refresh(this.state.getCurrentSectorEntities());
      var1 = System.currentTimeMillis();
      this.flareDrawerManager.refresh(this.state.getCurrentSectorEntities());
      this.transporterEffectManager.sectorEntitiesChanged(this.state.getCurrentSectorEntities().values());
      Iterator var5 = this.state.getCurrentSectorEntities().values().iterator();

      long var7;
      while(var5.hasNext()) {
         Sendable var6 = (Sendable)var5.next();
         var7 = System.currentTimeMillis();
         if (var6 instanceof Ship) {
            this.getPlumAndMuzzleDrawer().addPlume(new ExhaustPlumes((Ship)var6));
            this.getPlumAndMuzzleDrawer().addMuzzle(new MuzzleFlash((Ship)var6));
            this.shieldDrawerManager.add((Ship)var6);
         }

         if (var6 instanceof SpaceStation || var6 instanceof Planet) {
            this.shieldDrawerManager.add((ManagedSegmentController)var6);
            this.flareDrawerManager.addController((ManagedSegmentController)var6);
         }

         long var9;
         if ((var9 = System.currentTimeMillis() - var7) > 5L) {
            System.err.println("[WORLDDRAWER] WARNING: DRAWER UPDATE OF " + var6 + " took " + var9);
         }
      }

      long var11;
      if ((var11 = System.currentTimeMillis() - var1) > 5L) {
         System.err.println("[WORLDDRAWER] WARNING: ADD TOOK " + var11);
      }

      var1 = System.currentTimeMillis();
      this.connectionDrawerManager.updateEntities();
      this.energyStreamDrawerManager.updateEntities();
      if ((var7 = System.currentTimeMillis() - var1) > 5L) {
         System.err.println("[WORLDDRAWER] WARNING: CONNECTION UPDATE TOOK " + var7);
      }

   }

   public void setFlagSegmentControllerUpdate(boolean var1) {
      this.flagSegmentControllerUpdate = var1;
   }

   private void setMainLight(Vector3i var1, long var2, Vector3i var4, Vector3i var5, boolean var6, Transform var7) {
      AbstractScene.farPlane = (this.state.getSectorSize() + 10.0F) * 32.0F;
      this.absSectorPos.set((float)var5.x * this.state.getSectorSize(), (float)var5.y * this.state.getSectorSize(), (float)var5.z * this.state.getSectorSize());
      if (var6) {
         var7.setIdentity();
         var7.origin.set(this.absSectorPos);
         float var8 = this.state.getGameState().getRotationProgession();
         this.rot.setIdentity();
         this.rot.rotX(6.2831855F * var8);
         this.rot.invert();
         TransformTools.rotateAroundPoint(this.absSectorPos, this.rot, var7, this.tmpTrans);
         AbstractScene.mainLight.setPos(var7.origin);
      } else {
         AbstractScene.mainLight.setPos(this.absSectorPos);
      }
   }

   public void updateSpecialSectors() {
      Runnable var1 = new Runnable() {
         public void run() {
            it.unimi.dsi.fastutil.objects.ObjectArrayList var1 = new it.unimi.dsi.fastutil.objects.ObjectArrayList();

            for(int var2 = 0; var2 < 32768; ++var2) {
               if (SectorInformation.SectorType.values()[WorldDrawer.this.state.getPlayer().getProximitySector().getSectorType(var2)] == SectorInformation.SectorType.PLANET) {
                  var1.add(var2);
               }
            }

            WorldDrawer.this.specialSectorsBuffer = (Integer[])var1.toArray(new Integer[var1.size()]);
         }
      };
      this.state.getThreadPoolLogins().execute(var1);
   }

   public CharactersDrawer getCharacterDrawer() {
      return this.characterDrawer;
   }

   public CreatureTool getCreatureTool() {
      return this.creatureTool;
   }

   public void setCreatureTool(CreatureTool var1) {
      this.creatureTool = var1;
   }

   public boolean isFlagPlanetCoreUpdate() {
      return this.flagPlanetCoreUpdate;
   }

   public void setFlagPlanetCoreUpdate(boolean var1) {
      this.flagPlanetCoreUpdate = var1;
   }

   public ShardDrawer getShards() {
      return this.shards;
   }

   public SegmentControllerEffectDrawer getSegmentControllerEffectDrawer() {
      return this.segmentControllerEffectDrawer;
   }

   public void setSegmentControllerEffectDrawer(SegmentControllerEffectDrawer var1) {
      this.segmentControllerEffectDrawer = var1;
   }

   public int isSpotLightSupport() {
      return this.state.spotlights.size() > 0 ? 0 : ShaderLibrary.CubeShaderType.NO_SPOT_LIGHTS.bit;
   }

   public TransporterEffectManager getTransporterEffectManager() {
      return this.transporterEffectManager;
   }

   public void onStopClient() {
      if (this.segmentDrawer != null) {
         this.segmentDrawer.onStopClient();
      }

   }

   public void drawAdditional(FrameBufferObjects var1, FrameBufferObjects var2, DepthBufferScene var3) {
      this.prepareCamera();
      GL11.glDepthRange(0.0D, 1.0D);
      this.explosionDrawer.draw(var1, var2, var3, DepthBufferScene.getNearPlane(), DepthBufferScene.getFarPlane());
      GlUtil.glMatrixMode(5889);
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5888);
      if (drawError) {
         GlUtil.printGlErrorCritical();
      }

      boolean var7 = false;
      boolean var4;
      if ((var4 = this.getSegmentDrawer().drawCheckElementCollections()) && (this.getGameMapDrawer() == null || !this.getGameMapDrawer().doDraw())) {
         var1.enable();
         GL11.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
         GL11.glClear(16640);
         var1.disable();
         if (this.getGameMapDrawer() == null || !this.getGameMapDrawer().doDraw()) {
            this.getSegmentDrawer().drawElementCollectionsToFrameBuffer(var1);
         }

         var7 = true;
      }

      SimpleTransformableSendableObject var5;
      SegmentController var8;
      if (!var4 && (this.getGameMapDrawer() == null || !this.getGameMapDrawer().doDraw()) && (var5 = this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity()) != null && var5 instanceof SegmentController && this.state.getShip() != var5 && (var8 = (SegmentController)var5).isInClientRange() && (EngineSettings.PERMA_OUTLINE.isOn() || Keyboard.isKeyDown(KeyboardMappings.SELECT_OUTLINE.getMapping()))) {
         var1.enable();
         if (!var7) {
            GL11.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
            GL11.glClear(16640);
         }

         Vector4f var6 = new Vector4f();
         HudIndicatorOverlay.getColor(var8, var6, false, this.state);
         var6.w = 1.0F;
         ShaderLibrary.cubeShader13SimpleWhite.loadWithoutUpdate();
         GlUtil.updateShaderVector4f(ShaderLibrary.cubeShader13SimpleWhite, "col", var6);
         ShaderLibrary.cubeShader13SimpleWhite.unloadWithoutExit();
         int var9 = this.getSegmentDrawer().drawSegmentController(var8, ShaderLibrary.cubeShader13SimpleWhite);
         var1.disable();
         if (var9 > 0) {
            var7 = true;
         }
      }

      if (var7) {
         var2.enable();
         GlUtil.glDisable(3042);
         GlUtil.glDisable(2929);
         GlUtil.glEnable(3042);
         GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
         this.getSegmentDrawer().drawElementCollectionsFromFrameBuffer(var1, var4 ? 0.5F : 0.1F);
         GlUtil.glDisable(3042);
         var2.disable();
      }

   }

   public void onSectorChangeSelf(int var1, int var2) {
      long var3 = System.currentTimeMillis();
      this.getGuiDrawer().onSectorChange();
      long var5;
      if ((var5 = System.currentTimeMillis() - var3) > 10L) {
         System.err.println("[WORLDDRAWER] WARNING: SECTOR CHANGE UPDATE FOR DRAWER TOOK " + var5);
      }

   }
}
