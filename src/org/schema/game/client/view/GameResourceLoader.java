package org.schema.game.client.view;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Locale;
import java.util.zip.ZipInputStream;
import javax.xml.parsers.ParserConfigurationException;
import org.lwjgl.opengl.GL11;
import org.schema.common.ParseException;
import org.schema.common.config.ConfigParserException;
import org.schema.common.util.data.DataUtil;
import org.schema.game.client.view.gui.shiphud.newhud.HudConfig;
import org.schema.game.client.view.shader.CubeMeshQuadsShader13;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.explosion.ExplosionRunnable;
import org.schema.game.common.data.player.PlayerSkin;
import org.schema.game.common.updater.FileUtil;
import org.schema.game.common.util.FolderZipper;
import org.schema.game.common.version.Version;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.NVXGPUMemoryInfo;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.GuiConfig;
import org.schema.schine.graphicsengine.shader.ErrorDialogException;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.texture.DDSLoader;
import org.schema.schine.graphicsengine.texture.Texture;
import org.schema.schine.graphicsengine.texture.TextureLoader;
import org.schema.schine.graphicsengine.texture.textureImp.Texture3D;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.ResourceLoadEntry;
import org.schema.schine.resource.ResourceLoader;
import org.xml.sax.SAXException;

public class GameResourceLoader extends ResourceLoader {
   public static final String CUSTOM_EFFECT_CONFIG_PATH;
   public static final String CUSTOM_BLOCK_BEHAVIOR_CONFIG_PATH;
   public static final String CUSTOM_FACTION_CONFIG_PATH;
   public static final String CUSTOM_TEXTURE_PATH;
   public static final String CUSTOM_TEXTURE_TEMPLATE_PATH;
   public static final String CUSTOM_CONFIG_IMPORT_TEMPLATE_PATH;
   public static final String buttonSpriteName = "buttons-8x8";
   public static final String CUSTOM_CONFIG_IMPORT_PATH;
   public static int cubeTexture3d;
   public static int cubeTexture3dNormal;
   public static Texture lavaTexture;
   public static Texture overlayTextures;
   public static Texture overlayTexturesLow;
   public static Texture[] cubeTextures;
   public static Texture[] cubeNormalTextures;
   public static Texture[] cubeTexturesLow;
   public static Texture[] cubeNormalTexturesLow;
   public static Texture[] effectTextures;
   public static Texture skyTexture;
   public static Texture simpleStarFieldTexture;
   public static Texture marpleTexture;
   public static Texture3D noiseVolume;
   public static PlayerSkin[] traidingSkin;
   private static byte[] buffer;
   // $FF: synthetic field
   static final boolean $assertionsDisabled = !GameResourceLoader.class.desiredAssertionStatus();

   public GameResourceLoader() {
      super(0);
   }

   public static File getConfigInputFile() {
      return new FileExt(CUSTOM_CONFIG_IMPORT_PATH + "BlockConfigImport.xml");
   }

   public void loadServer() throws FileNotFoundException, ResourceException, ParseException, SAXException, IOException, ParserConfigurationException {
      super.loadServer();
      this.enqueueModels();
      Controller.getResLoader().loadQueue.add(new ResourceLoadEntry("ElementDataInitialization", 4) {
         public void load(ResourceLoader var1) throws ResourceException, IOException {
            ElementKeyMap.initDataForGame();
         }
      });
      Controller.getResLoader().forceLoadAll();
   }

   public void loadClient() {
      Controller.getResLoader().loadQueue.add(new ResourceLoadEntry("ElementDataInitialization", 4) {
         public void load(ResourceLoader var1) throws ResourceException, IOException {
            ElementKeyMap.initDataForGame();
         }
      });
   }

   public static void createCustomTextureZip() throws IOException {
      String var0 = "";

      try {
         var0 = FileUtil.createFilesHashRecursively(CUSTOM_TEXTURE_PATH, new FileFilter() {
            public final boolean accept(File var1) {
               return var1.isDirectory() || var1.getName().toLowerCase(Locale.ENGLISH).endsWith(".png");
            }
         });
      } catch (NoSuchAlgorithmException var5) {
         var5.printStackTrace();
      }

      FileExt var1 = new FileExt(CUSTOM_TEXTURE_PATH + "pack.zip");
      FileExt var2;
      if ((var2 = new FileExt(CUSTOM_TEXTURE_PATH + "hash.txt")).exists() && var1.exists()) {
         BufferedReader var3;
         String var4 = (var3 = new BufferedReader(new FileReader(var2))).readLine();
         var3.close();
         if (var0.equals(var4)) {
            System.out.println("[RESOURCES][CustomTextures] No need to create pack.zip. Hash matches (as rewriting a zip changes the hash on it)");
            return;
         }
      }

      var2.delete();
      System.err.println("[CustomTextures] Writing hash: " + var0);
      BufferedWriter var6;
      (var6 = new BufferedWriter(new FileWriter(var2))).append(var0);
      var6.flush();
      var6.close();
      if (var1.exists()) {
         var1.delete();
      }

      FolderZipper.zipFolder(CUSTOM_TEXTURE_PATH, CUSTOM_TEXTURE_PATH + "pack.zip", "pack.zip", (FileFilter)null);
   }

   public static void copyDefaultCustomTexturesTo(String var0) throws IOException {
      FileExt var1 = new FileExt(var0);
      FileUtil.copyDirectory(new FileExt(CUSTOM_TEXTURE_TEMPLATE_PATH), var1);
   }

   public static void copyCustomConfig(String var0) throws IOException {
      FileExt var1 = new FileExt(var0);
      FileUtil.copyDirectory(new FileExt(CUSTOM_CONFIG_IMPORT_TEMPLATE_PATH), var1);
   }

   private static Texture getBlockOverlayTexture(String var0, int var1) throws IOException {
      return Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "./textures/block/" + var0 + "/" + var1 + "/overlays.png", 9729, true, true);
   }

   private static void loadCubeTextures(Texture[] var0, String var1, int var2, String var3) throws IOException {
      var0[0] = Controller.getTexLoader().getTexture2DAnyFormat(DataUtil.dataPath + "./textures/block/" + var1 + "/" + var2 + "/t000", 9729, true, true);
      var0[1] = Controller.getTexLoader().getTexture2DAnyFormat(DataUtil.dataPath + "./textures/block/" + var1 + "/" + var2 + "/t001", 9729, true, true);
      var0[2] = Controller.getTexLoader().getTexture2DAnyFormat(DataUtil.dataPath + "./textures/block/" + var1 + "/" + var2 + "/t002", 9729, true, true);
      var0[7] = Controller.getTexLoader().getTexture2DAnyFormat(var3 + "/" + var2 + "/custom", 9729, true, true);
   }

   private static void loadCubeNormalTextures(Texture[] var0, String var1, int var2, String var3, boolean var4) throws IOException {
      var0[0] = Controller.getTexLoader().getTexture2DAnyFormat(DataUtil.dataPath + "./textures/block/" + var1 + "/" + var2 + "/t000_NRM", 9729, true, true);
      var0[1] = Controller.getTexLoader().getTexture2DAnyFormat(DataUtil.dataPath + "./textures/block/" + var1 + "/" + var2 + "/t001_NRM", 9729, true, true);
      var0[2] = Controller.getTexLoader().getTexture2DAnyFormat(DataUtil.dataPath + "./textures/block/" + var1 + "/" + var2 + "/t002_NRM", 9729, true, true);
      var0[7] = Controller.getTexLoader().getTexture2DAnyFormat(var3 + "/" + var2 + "/custom_NRM", 9729, false, true);
      if (var4) {
         ShaderLibrary.USE_CUBE_TEXTURE_EMISSION = (new FileExt(DataUtil.dataPath + "./textures/block/" + var1 + "/" + var2 + "/t000_NRM.tga")).exists();
         System.err.println("[RESOURCE] emission texture present: " + ShaderLibrary.USE_CUBE_TEXTURE_EMISSION);
      }

   }

   public static File[] copyTextures(String var0) throws IOException {
      String var1 = EngineSettings.G_TEXTURE_PACK.getCurrentState().toString();
      if (!(new FileExt(DataUtil.dataPath + "./textures/block/" + var1 + "/")).exists()) {
         System.err.println("WARNING: texture pack: " + var1 + " does not exist. Reverting to default");
         EngineSettings.G_TEXTURE_PACK.setCurrentState("Default");
         var1 = EngineSettings.G_TEXTURE_PACK.getCurrentState().toString();
      }

      int var2 = 512;

      for(FileExt var3 = new FileExt(DataUtil.dataPath + "./textures/block/" + var1 + "/512/"); var2 > 64 && !var3.exists(); var3 = new FileExt(DataUtil.dataPath + "./textures/block/" + var1 + "/" + var2 + "/")) {
         var2 /= 2;
      }

      String var7 = EngineSettings.CLIENT_CUSTOM_TEXTURE_PATH.getCurrentState().toString();
      File[] var4;
      (var4 = new File[8])[0] = getTexture2DFileAnyFormat(DataUtil.dataPath + "./textures/block/" + var1 + "/" + var2 + "/t000");
      var4[1] = getTexture2DFileAnyFormat(DataUtil.dataPath + "./textures/block/" + var1 + "/" + var2 + "/t001");
      var4[2] = getTexture2DFileAnyFormat(DataUtil.dataPath + "./textures/block/" + var1 + "/" + var2 + "/t002");
      var4[7] = getTexture2DFileAnyFormat(var7 + "/" + var2 + "/custom");

      for(int var5 = 0; var5 < var4.length; ++var5) {
         if (var4[var5] != null) {
            FileExt var6;
            (var6 = new FileExt(var0 + "/" + var4[var5].getName())).createNewFile();
            FileUtil.copyFile(var4[var5], var6);
            var4[var5] = var6;
         }
      }

      return var4;
   }

   public static File getTexture2DFileAnyFormat(String var0) throws IOException {
      FileExt var1 = new FileExt(var0 + ".png");
      FileExt var2;
      if ((var2 = new FileExt(var0 + ".tga")).exists()) {
         return var2;
      } else if (var1.exists()) {
         return var1;
      } else {
         throw new FileNotFoundException("Neither .png or .tga found for resource " + var0);
      }
   }

   public static List getBlockTextureResourceLoadEntry() {
      ObjectArrayList var0 = new ObjectArrayList();
      final int var1 = (Integer)EngineSettings.G_TEXTURE_PACK_RESOLUTION.getCurrentState();
      final String var2 = EngineSettings.G_TEXTURE_PACK.getCurrentState().toString();
      final FileExt var3 = new FileExt(DataUtil.dataPath + "./textures/block/" + var2 + "/");
      final String var4 = EngineSettings.CLIENT_CUSTOM_TEXTURE_PATH.getCurrentState().toString();
      final FileExt var5 = new FileExt(var4);
      var0.add(new ResourceLoadEntry("Extract_Textures", 4) {
         public final void load(ResourceLoader var1) throws ResourceException {
            GameResourceLoader.extractIfBlockTexturesIfNecessary();
         }
      });
      var0.add(new ResourceLoadEntry("Copy_Custom_Textures", 4) {
         public final boolean canLoad() {
            assert var5 != null;

            return var5 != null && !var5.exists();
         }

         public final void load(ResourceLoader var1) throws ResourceException {
            assert false;

            try {
               throw new Exception("Custom textures in this resolution do not exist for this server. reverting to default");
            } catch (Exception var3) {
               var3.printStackTrace();

               try {
                  EngineSettings.CLIENT_CUSTOM_TEXTURE_PATH.setCurrentState("./customBlockTextures");
                  EngineSettings.write();
                  GameResourceLoader.copyDefaultCustomTexturesTo(EngineSettings.CLIENT_CUSTOM_TEXTURE_PATH.getCurrentState().toString());
               } catch (IOException var2) {
                  var2.printStackTrace();
                  throw new RuntimeException(var2);
               }

               EngineSettings.CLIENT_CUSTOM_TEXTURE_PATH.setCurrentState(var4);
            }
         }
      });
      var0.add(new ResourceLoadEntry(var2 + var3 + var1 + "TexArray", 5) {
         public final boolean canLoad() {
            return GraphicsContext.isTextureArrayEnabled();
         }

         public final void load(ResourceLoader var1x) throws ResourceException {
            try {
               String var6 = var2;
               if (!var3.exists()) {
                  System.err.println("WARNING: texture pack: " + var6 + " does not exist. Reverting to default");
                  EngineSettings.G_TEXTURE_PACK.setCurrentState("Default");
                  var6 = (String)EngineSettings.G_TEXTURE_PACK.getCurrentState();
               }

               System.err.println("[RESOURCE] Cube Texture Array Available");
               GameResourceLoader.printMemory("GL_MEMORY BEFORE NORMAL MAPS");
               if (EngineSettings.G_NORMAL_MAPPING.isOn()) {
                  try {
                     for(int var2x = 0; var2x < 3; ++var2x) {
                        FileExt var3x;
                        if (!(var3x = new FileExt(DataUtil.dataPath + "./textures/block/" + var6 + "/" + var1 + "/t00" + var2x + "_NRM.png")).exists()) {
                           System.err.println("WARNING: NOT USING NORAML MAPS BECAUSE NORMAL MAPS DONT EXIST " + var3x.getAbsolutePath());
                           throw new ErrorDialogException("\n\nNormal map for this texture pack not found: \n" + var3x.getAbsolutePath() + "\n\npress 'retry' to play without bump mapping");
                        }
                     }

                     FileExt var7;
                     if (!(var7 = new FileExt(var4 + "/" + var1 + "/custom_NRM.png")).exists()) {
                        System.err.println("WARNING: NOT USING NORAML MAPS BECAUSE NORMAL MAPS DONT EXIST " + var7.getAbsolutePath());
                        throw new ErrorDialogException("\n\nNormal map for this texture pack not found: \n" + var7.getAbsolutePath() + "\n\npress 'retry' to play without bump mapping");
                     }
                  } catch (ErrorDialogException var4x) {
                     GLFrame.processErrorDialogExceptionWithoutReportWithContinue(var4x, (StateInterface)null);
                     EngineSettings.G_NORMAL_MAPPING.setCurrentState(false);
                  }
               }

               if (GameResourceLoader.cubeTexture3d != 0) {
                  GL11.glDeleteTextures(GameResourceLoader.cubeTexture3d);
               }

               GameResourceLoader.printMemory("GL_MEMORY AFTER NORMAL MAPS BEFORE DEFFUSE");
               Controller.getTexLoader();
               GameResourceLoader.cubeTexture3d = TextureLoader.getTextureArray(new String[]{DataUtil.dataPath + "./textures/block/" + var6 + "/" + var1 + "/t000.png", DataUtil.dataPath + "./textures/block/" + var6 + "/" + var1 + "/t001.png", DataUtil.dataPath + "./textures/block/" + var6 + "/" + var1 + "/t002.png", DataUtil.dataPath + "./textures/block/" + var6 + "/" + var1 + "/overlays.png"}, 6408, 9729, 9729, true);
               GameResourceLoader.printMemory("GL_MEMORY AFTER DIFFUSE");
            } catch (Exception var5) {
               if (var5 instanceof ResourceException) {
                  throw (ResourceException)var5;
               } else {
                  var5.printStackTrace();
                  EngineSettings.G_NORMAL_MAPPING.setCurrentState(false);
                  GraphicsContext.setTextureArrayEnabled(false);
               }
            }
         }
      });
      var0.add(new ResourceLoadEntry(var2 + var3 + var1 + "texArrayNormal", 5) {
         public final boolean canLoad() {
            return GraphicsContext.isTextureArrayEnabled();
         }

         public final void load(ResourceLoader var1x) throws ResourceException {
            String var3x = var2;
            if (!var3.exists()) {
               System.err.println("WARNING: texture pack: " + var3x + " does not exist. Reverting to default");
               EngineSettings.G_TEXTURE_PACK.setCurrentState("Default");
               var3x = (String)EngineSettings.G_TEXTURE_PACK.getCurrentState();
            }

            try {
               if (EngineSettings.G_NORMAL_MAPPING.isOn()) {
                  if (GameResourceLoader.cubeTexture3dNormal != 0) {
                     GL11.glDeleteTextures(GameResourceLoader.cubeTexture3dNormal);
                  }

                  Controller.getTexLoader();
                  GameResourceLoader.cubeTexture3dNormal = TextureLoader.getTextureArray(new String[]{DataUtil.dataPath + "./textures/block/" + var3x + "/" + var1 + "/t000_NRM.png", DataUtil.dataPath + "./textures/block/" + var3x + "/" + var1 + "/t001_NRM.png", DataUtil.dataPath + "./textures/block/" + var3x + "/" + var1 + "/t002_NRM.png"}, 6408, 9729, 9729, true);
               }

            } catch (Exception var2x) {
               if (var2x instanceof ResourceException) {
                  throw (ResourceException)var2x;
               } else {
                  var2x.printStackTrace();
                  EngineSettings.G_NORMAL_MAPPING.setCurrentState(false);
                  GraphicsContext.setTextureArrayEnabled(false);
               }
            }
         }
      });
      var0.add(new ResourceLoadEntry(var2 + var3 + var1, 5) {
         public final boolean canLoad() {
            return !GraphicsContext.isTextureArrayEnabled();
         }

         public final void load(ResourceLoader var1x) throws ResourceException {
            String var9 = var2;
            if (!var3.exists()) {
               System.err.println("WARNING: texture pack: " + var9 + " does not exist. Reverting to default");
               EngineSettings.G_TEXTURE_PACK.setCurrentState("Default");
               var9 = (String)EngineSettings.G_TEXTURE_PACK.getCurrentState();
            }

            try {
               System.err.println("[RESOURCE] Loading cube textures");
               GameResourceLoader.printMemory("GL_MEMORY Before DEFFUSE");
               Texture[] var2x;
               int var3x;
               int var4x;
               Texture var5;
               if (GameResourceLoader.cubeTextures != null) {
                  var3x = (var2x = GameResourceLoader.cubeTextures).length;

                  for(var4x = 0; var4x < var3x; ++var4x) {
                     if ((var5 = var2x[var4x]) != null) {
                        var5.cleanUp();
                     }
                  }
               }

               if (GameResourceLoader.cubeTexturesLow != null) {
                  var3x = (var2x = GameResourceLoader.cubeTexturesLow).length;

                  for(var4x = 0; var4x < var3x; ++var4x) {
                     if ((var5 = var2x[var4x]) != null) {
                        var5.cleanUp();
                     }
                  }
               }

               if (GameResourceLoader.overlayTexturesLow != null) {
                  GameResourceLoader.overlayTexturesLow.cleanUp();
               }

               if (GameResourceLoader.overlayTextures != null) {
                  GameResourceLoader.overlayTextures.cleanUp();
               }

               GameResourceLoader.loadCubeTextures(GameResourceLoader.cubeTextures = new Texture[8], var9, var1, var4);
               GameResourceLoader.overlayTextures = GameResourceLoader.getBlockOverlayTexture(var9, var1);
               if (!"64".equals(var1) && (new FileExt(DataUtil.dataPath + "./textures/block/" + var9 + "/64/")).exists()) {
                  GameResourceLoader.cubeTexturesLow = new Texture[8];

                  try {
                     GameResourceLoader.loadCubeTextures(GameResourceLoader.cubeTexturesLow, var9, 64, var4);
                  } catch (Exception var7) {
                     System.err.println("Exception: ERROR LOADING LOW QUALITY TEXTURES!");
                     var7.printStackTrace();
                     System.err.println("Catched Exception: No low quality available. Program may continue but performance on big objects might suffer");
                     GameResourceLoader.cubeTexturesLow = GameResourceLoader.cubeTextures;
                  }

                  try {
                     GameResourceLoader.overlayTexturesLow = GameResourceLoader.getBlockOverlayTexture(var9, 64);
                  } catch (Exception var6) {
                     System.err.println("Exception: ERROR LOADING LOW QUALITY OVERLAYS!");
                     var6.printStackTrace();
                     System.err.println("Catched Exception: No low quality overlay available. Program may continue but performance on big objects might suffer");
                     GameResourceLoader.overlayTexturesLow = GameResourceLoader.overlayTextures;
                  }
               } else {
                  GameResourceLoader.cubeTexturesLow = GameResourceLoader.cubeTextures;
                  GameResourceLoader.overlayTexturesLow = GameResourceLoader.overlayTextures;
               }

               GameResourceLoader.printMemory("GL_MEMORY AFTER DIFFUSE");
               System.err.println("[RESOURCE] Loading cube textures DONE");
            } catch (IOException var8) {
               var8.printStackTrace();
               throw new ResourceException(DataUtil.dataPath + "./textures/block/");
            }
         }
      });
      var0.add(new ResourceLoadEntry(var2 + var3 + var1 + "normal", 5) {
         public final boolean canLoad() {
            return !GraphicsContext.isTextureArrayEnabled();
         }

         public final void load(ResourceLoader var1x) throws ResourceException {
            String var9 = var2;
            if (!var3.exists()) {
               System.err.println("WARNING: texture pack: " + var9 + " does not exist. Reverting to default");
               EngineSettings.G_TEXTURE_PACK.setCurrentState("Default");
               var9 = (String)EngineSettings.G_TEXTURE_PACK.getCurrentState();
            }

            Texture[] var2x;
            try {
               if (EngineSettings.G_NORMAL_MAPPING.isOn()) {
                  int var3x;
                  int var4x;
                  Texture var5;
                  if (GameResourceLoader.cubeNormalTextures != null) {
                     var3x = (var2x = GameResourceLoader.cubeNormalTextures).length;

                     for(var4x = 0; var4x < var3x; ++var4x) {
                        if ((var5 = var2x[var4x]) != null) {
                           var5.cleanUp();
                        }
                     }
                  }

                  if (GameResourceLoader.cubeNormalTexturesLow != null) {
                     var3x = (var2x = GameResourceLoader.cubeNormalTexturesLow).length;

                     for(var4x = 0; var4x < var3x; ++var4x) {
                        if ((var5 = var2x[var4x]) != null) {
                           var5.cleanUp();
                        }
                     }
                  }

                  GameResourceLoader.cubeNormalTextures = new Texture[8];

                  try {
                     for(int var10 = 0; var10 < 3; ++var10) {
                        FileExt var11;
                        if (!(var11 = new FileExt(DataUtil.dataPath + "./textures/block/" + var9 + "/" + var1 + "/t00" + var10 + "_NRM.tga")).exists()) {
                           var11 = new FileExt(DataUtil.dataPath + "./textures/block/" + var9 + "/" + var1 + "/t00" + var10 + "_NRM.png");
                        }

                        if (!var11.exists() && GameResourceLoader.cubeTextures[var10] != null) {
                           System.err.println("WARNING: NOT USING NORAML MAPS BECAUSE NORMAL MAPS DONT EXIST " + var11.getAbsolutePath());
                           throw new ErrorDialogException("\n\nNormal map (neither png or tga) for this texture pack not found: \n" + var11.getAbsolutePath() + "\n" + (new FileExt(DataUtil.dataPath + "./textures/block/" + var9 + "/" + var1 + "/t00" + var10 + "_NRM.tga")).getAbsolutePath() + " \n\npress 'retry' to play without bump mapping");
                        }
                     }

                     FileExt var12;
                     if (!(var12 = new FileExt(var4 + "/" + var1 + "/custom_NRM.png")).exists() && GameResourceLoader.cubeTextures[7] != null) {
                        System.err.println("WARNING: NOT USING NORAML MAPS BECAUSE NORMAL MAPS DONT EXIST " + var12.getAbsolutePath());
                        throw new ErrorDialogException("\n\nNormal map for this texture pack not found: \n" + var12.getAbsolutePath() + "\n\npress 'retry' to play without bump mapping");
                     }
                  } catch (ErrorDialogException var7) {
                     var2x = null;
                     GLFrame.processErrorDialogExceptionWithoutReportWithContinue(var7, (StateInterface)null);
                     EngineSettings.G_NORMAL_MAPPING.setCurrentState(false);
                  }

                  GameResourceLoader.loadCubeNormalTextures(GameResourceLoader.cubeNormalTextures, var9, var1, var4, true);
                  if (!"64".equals(var1) && (new FileExt(DataUtil.dataPath + "./textures/block/" + var9 + "/64/")).exists()) {
                     GameResourceLoader.cubeNormalTexturesLow = new Texture[8];

                     try {
                        GameResourceLoader.loadCubeNormalTextures(GameResourceLoader.cubeNormalTexturesLow, var9, 64, var4, false);
                     } catch (Exception var6) {
                        System.err.println("Exception: ERROR LOADING LOW QUALITY NORMAL MAPS!");
                        var6.printStackTrace();
                        System.err.println("Catched Exception: No low quality normal maps available. Program may continue but performance on big objects might suffer");
                        GameResourceLoader.cubeNormalTexturesLow = GameResourceLoader.cubeNormalTextures;
                     }
                  } else {
                     GameResourceLoader.cubeNormalTexturesLow = GameResourceLoader.cubeNormalTextures;
                  }

                  GameResourceLoader.printMemory("GL_MEMORY AFTER NORMAL");
               }

            } catch (IOException var8) {
               var2x = null;
               var8.printStackTrace();
               throw new ResourceException(DataUtil.dataPath + "./textures/block/");
            }
         }
      });
      return var0;
   }

   private static void extractIfBlockTexturesIfNecessary() {
      FileExt var0 = new FileExt("lastExtr");
      boolean var1 = true;
      DataInputStream var2 = null;

      try {
         if (var0.exists()) {
            var2 = new DataInputStream(new FileInputStream(var0));
            if (Version.build.equals(var2.readUTF())) {
               var1 = false;
            }
         }
      } catch (IOException var28) {
         var28.printStackTrace();
      } finally {
         if (var2 != null) {
            try {
               var2.close();
            } catch (IOException var25) {
               var25.printStackTrace();
            }
         }

      }

      FileExt var33 = new FileExt("./data/textures/block/Default/64/t000.png");
      System.err.println("[TEXTURELOADER] Checking if textures exist: " + var33.exists());
      if (!var1 && !var33.exists()) {
         System.err.println("[TEXTURELOADER] Textures do not exist! Forcing Re-Extraction of Texture Archives!");
         var1 = true;
      }

      if (var1) {
         buffer = new byte[10240];
         extractDirRec(new FileExt("./data/textures/block/"));
         buffer = null;
         DataOutputStream var32 = null;
         boolean var13 = false;

         label251: {
            try {
               var13 = true;
               var0.delete();
               (var32 = new DataOutputStream(new FileOutputStream(var0))).writeUTF(Version.build);
               var13 = false;
               break label251;
            } catch (IOException var30) {
               var30.printStackTrace();
               var13 = false;
            } finally {
               if (var13) {
                  if (var32 != null) {
                     try {
                        var32.close();
                     } catch (IOException var26) {
                        var26.printStackTrace();
                     }
                  }

               }
            }

            if (var32 != null) {
               try {
                  var32.close();
                  return;
               } catch (IOException var24) {
                  var24.printStackTrace();
                  return;
               }
            }

            return;
         }

         try {
            var32.close();
         } catch (IOException var27) {
            var27.printStackTrace();
         }
      }
   }

   private static void extractDirRec(File var0) {
      if (var0.isDirectory()) {
         File[] var6;
         int var7 = (var6 = var0.listFiles()).length;

         for(int var8 = 0; var8 < var7; ++var8) {
            extractDirRec(var6[var8]);
         }

      } else {
         if (var0.getName().toLowerCase(Locale.ENGLISH).endsWith(".zip")) {
            try {
               ZipInputStream var1;
               (var1 = new ZipInputStream(new BufferedInputStream(new FileInputStream(var0)))).getNextEntry();
               FileExt var2 = new FileExt(var0.getAbsolutePath().substring(0, var0.getAbsolutePath().length() - 4));
               BufferedOutputStream var3 = new BufferedOutputStream(new FileOutputStream(var2));
               System.out.println("[RESOURCE] Extracting " + var2.getAbsolutePath() + " ...");

               int var5;
               while((var5 = var1.read(buffer)) > 0) {
                  var3.write(buffer, 0, var5);
               }

               var1.close();
               var3.close();
               return;
            } catch (IOException var4) {
               var4.printStackTrace();
            }
         }

      }
   }

   public static void printMemory(String var0) {
      if (GraphicsContext.getCapabilities().GL_NVX_gpu_memory_info) {
         System.err.println(var0);
         int var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX());
         System.err.println("CURRENT_AVAILABLE: " + var1 / 1024 + "MB");
         var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX());
         System.err.println("TOTAL_AVAILABLE: " + var1 / 1024 + "MB");
         var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX());
         System.err.println("INFO_DEDICATED: " + var1 / 1024 + "MB");
         var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_EVICTED_MEMORY_NVX());
         System.err.println("INFO_EVICTED: " + var1 / 1024 + "MB");
      } else if (GraphicsContext.getCapabilities().GL_ATI_meminfo) {
         System.out.println("ATI VBO_FREE: " + GL11.glGetInteger(34811));
         System.out.println("ATI RENDERBUFFER_FREE: " + GL11.glGetInteger(34813));
         System.out.println("ATI TEXTURE_FREE: " + GL11.glGetInteger(34812));
      }

      GlUtil.printGlErrorCritical("Memory size access failed");
   }

   public static int getOverlayTextures(CubeMeshQuadsShader13.CubeTexQuality var0) {
      return var0 == CubeMeshQuadsShader13.CubeTexQuality.SELECTED ? overlayTextures.getTextureId() : overlayTexturesLow.getTextureId();
   }

   public static int getCubeTexture(int var0, CubeMeshQuadsShader13.CubeTexQuality var1) {
      return var1 == CubeMeshQuadsShader13.CubeTexQuality.SELECTED ? cubeTextures[var0].getTextureId() : cubeTexturesLow[var0].getTextureId();
   }

   public static int getCubeNormalTexture(int var0, CubeMeshQuadsShader13.CubeTexQuality var1) {
      return var1 == CubeMeshQuadsShader13.CubeTexQuality.SELECTED ? cubeNormalTextures[var0].getTextureId() : cubeNormalTexturesLow[var0].getTextureId();
   }

   public List loadCustom() throws ResourceException {
      List var1 = super.loadCustom();
      Texture3D var10000 = noiseVolume;
      var1.add(new ResourceLoadEntry("NoiseVolume", 5) {
         public void load(ResourceLoader var1) throws ResourceException {
            GlUtil.printGlErrorCritical();
            GameResourceLoader.noiseVolume = new Texture3D();

            try {
               DDSLoader.load(new FileExt(DataUtil.dataPath + "effects/thruster/NoiseVolume.dds"), GameResourceLoader.noiseVolume, false);
            } catch (IOException var2) {
               var2.printStackTrace();
               throw new ResourceException("noise volume");
            }

            GlUtil.printGlErrorCritical();
         }
      });
      var1.add(new ResourceLoadEntry("marple", 5) {
         public void load(ResourceLoader var1) throws ResourceException {
            try {
               GameResourceLoader.marpleTexture = Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "textures/marble-seamless-texture.png", true);
            } catch (IOException var2) {
               var2.printStackTrace();
               throw new ResourceException(DataUtil.dataPath + "textures/marble-seamless-texture.png");
            }
         }
      });
      var1.add(new ResourceLoadEntry("LavaTex", 5) {
         public void load(ResourceLoader var1) throws ResourceException {
            try {
               GameResourceLoader.lavaTexture = Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "textures/lava.png", true);
            } catch (IOException var2) {
               var2.printStackTrace();
               throw new ResourceException(DataUtil.dataPath + "textures/lava.png");
            }
         }
      });
      var1.add(new ResourceLoadEntry("ExplosionData", 4) {
         public void load(ResourceLoader var1) throws ResourceException {
            ExplosionRunnable.initialize();
         }
      });
      var1.add(new ResourceLoadEntry("SimpeStarField", 5) {
         public void load(ResourceLoader var1) throws ResourceException {
            try {
               GameResourceLoader.simpleStarFieldTexture = Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "textures/simplestarfield.png", true);
            } catch (IOException var2) {
               var2.printStackTrace();
               throw new ResourceException(DataUtil.dataPath + "textures/simplestarfield.png");
            }
         }
      });
      var1.add(new ResourceLoadEntry("Effects", 5) {
         public void load(ResourceLoader var1) throws ResourceException {
            try {
               System.err.println("[RESOURCE] Loading effects");
               (GameResourceLoader.effectTextures = new Texture[2])[0] = Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "effects/dudvmap.jpg", true);
               GameResourceLoader.effectTextures[1] = Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "effects/noise.png", true);
               System.err.println("[RESOURCE] Loading effects done");
            } catch (IOException var2) {
               var2.printStackTrace();
               throw new ResourceException(DataUtil.dataPath + "effects/");
            }
         }
      });
      var1.add(new ResourceLoadEntry("TradingSkins", 5) {
         public void load(ResourceLoader var1) throws ResourceException {
            GameResourceLoader.traidingSkin = new PlayerSkin[1];

            try {
               GameResourceLoader.traidingSkin[0] = PlayerSkin.create(new FileExt(DataUtil.dataPath + "textures/skins/"), "traid00");
            } catch (IOException var2) {
               var2.printStackTrace();
               throw new ResourceException(DataUtil.dataPath + "effects/");
            }
         }
      });
      var1.add(new ResourceLoadEntry("SkyTexture" + EngineSettings.G_USE_HIGH_QUALITY_BACKGROUND.isOn(), 5) {
         public void load(ResourceLoader var1) throws ResourceException {
            System.err.println("[RESOURCE] Loading background");
            String var3;
            if (!EngineSettings.G_USE_HIGH_QUALITY_BACKGROUND.isOn()) {
               var3 = DataUtil.dataPath + "sky/milkyway/Milky-Way-texture-cube";
            } else {
               var3 = DataUtil.dataPath + "sky/generic/generic";
            }

            try {
               GameResourceLoader.skyTexture = Controller.getTexLoader().getCubeMap(var3, "png");
               System.err.println("[RESOURCE] Loading background DONE");
            } catch (IOException var2) {
               var2.printStackTrace();
               throw new ResourceException(DataUtil.dataPath + var3);
            }
         }
      });
      var1.add(new ResourceLoadEntry("Occlusion" + EngineSettings.G_USE_OCCLUSION_CULLING.isOn(), 4) {
         public void load(ResourceLoader var1) throws ResourceException {
            if (EngineSettings.G_USE_OCCLUSION_CULLING.isOn()) {
               System.err.println("[OCLUSION] INITIALIZING OCCLUSION QUERIES: " + ((Integer)EngineSettings.G_MAX_SEGMENTSDRAWN.getCurrentState() << 1));
               GameResourceLoader.printMemory("Before Oclusion Queries Initialization");
               SegmentDrawer.segmentOcclusion.reinitialize((Integer)EngineSettings.G_MAX_SEGMENTSDRAWN.getCurrentState() << 1);
               GameResourceLoader.printMemory("After Oclusion Queries Initialization");
            }

         }
      });
      List var2 = getBlockTextureResourceLoadEntry();
      var1.addAll(var2);
      File[] var12;
      int var3 = (var12 = (new FileExt("./data/tutorial/")).listFiles()).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         File var5;
         if ((var5 = var12[var4]).isDirectory()) {
            int var6 = var5.listFiles().length;

            for(int var7 = 0; var7 < var6; ++var7) {
               if (var5.exists()) {
                  String[] var8 = var5.list();

                  for(int var9 = 0; var9 < var8.length; ++var9) {
                     if (var8[var9].endsWith(".png")) {
                        String var10;
                        int var11 = (var11 = (var10 = var8[var9]).lastIndexOf("/")) < 0 ? var10.lastIndexOf("\\") : var11;
                        String var13 = var10.substring(var11 + 1, var10.lastIndexOf(".png"));
                        ResourceLoadEntry var14;
                        (var14 = new ResourceLoadEntry(var5.getName() + "_" + var13, 0)).path = var5.getAbsolutePath() + File.separator;
                        var14.fileName = var10;
                        var1.add(var14);
                     }
                  }
               }
            }
         }
      }

      return var1;
   }

   public void onStopClient() {
   }

   public void enqueueConfigResources(final String var1, boolean var2) {
      ResourceLoadEntry var3 = new ResourceLoadEntry("CONFIG_HUD", 4) {
         public void load(ResourceLoader var1) throws ResourceException, IOException {
            try {
               HudConfig.load();
            } catch (IllegalArgumentException var2) {
               var2.printStackTrace();
               throw new ResourceException("HUD CONFIG FAILED TO PARSE");
            } catch (IllegalAccessException var3) {
               var3.printStackTrace();
               throw new ResourceException("HUD CONFIG FAILED TO PARSE");
            } catch (SAXException var4) {
               var4.printStackTrace();
               throw new ResourceException("HUD CONFIG FAILED TO PARSE");
            } catch (ParserConfigurationException var5) {
               var5.printStackTrace();
               throw new ResourceException("HUD CONFIG FAILED TO PARSE");
            } catch (ConfigParserException var6) {
               var6.printStackTrace();
               throw new ResourceException("HUD CONFIG FAILED TO PARSE");
            }
         }
      };
      boolean var10000 = $assertionsDisabled;
      if (var2) {
         this.loadedDataEntries.remove(var3);
      }

      this.loadQueue.add(var3);
      var3 = new ResourceLoadEntry("CONFIG_GUI", 4) {
         public void load(ResourceLoader var1x) throws ResourceException, IOException {
            try {
               System.err.println("[CLIENT] Loading GUI CONFIG: " + var1);
               GuiConfig.load(var1);
            } catch (IllegalArgumentException var2) {
               var2.printStackTrace();
               throw new ResourceException("GUI CONFIG FAILED TO PARSE");
            } catch (IllegalAccessException var3) {
               var3.printStackTrace();
               throw new ResourceException("GUI CONFIG FAILED TO PARSE");
            } catch (SAXException var4) {
               var4.printStackTrace();
               throw new ResourceException("GUI CONFIG FAILED TO PARSE");
            } catch (ParserConfigurationException var5) {
               var5.printStackTrace();
               throw new ResourceException("GUI CONFIG FAILED TO PARSE");
            } catch (ConfigParserException var6) {
               var6.printStackTrace();
               throw new ResourceException("GUI CONFIG FAILED TO PARSE");
            }
         }
      };
      this.loadedDataEntries.remove(var3);
      var10000 = $assertionsDisabled;
      this.loadQueue.add(var3);
      this.resetLoadCounts();
   }

   public static void enqueueGuiConfig() {
   }

   static {
      CUSTOM_EFFECT_CONFIG_PATH = "." + File.separator + "customEffectConfig" + File.separator;
      CUSTOM_BLOCK_BEHAVIOR_CONFIG_PATH = "." + File.separator + "customBlockBehaviorConfig" + File.separator;
      CUSTOM_FACTION_CONFIG_PATH = "." + File.separator + "customFactionConfig" + File.separator;
      CUSTOM_TEXTURE_PATH = "." + File.separator + "customBlockTextures" + File.separator;
      CUSTOM_TEXTURE_TEMPLATE_PATH = "." + File.separator + "data" + File.separator + "textures" + File.separator + "customTemplates" + File.separator;
      CUSTOM_CONFIG_IMPORT_TEMPLATE_PATH = "." + File.separator + "data" + File.separator + "config" + File.separator + "customConfigTemplate" + File.separator;
      CUSTOM_CONFIG_IMPORT_PATH = "." + File.separator + "customBlockConfig" + File.separator;
   }

   public static enum StandardButtons {
      BUY_BUTTON,
      SELL_BUTTON,
      OK_BUTTON,
      CANCEL_BUTTON,
      SPAWN_BUTTON,
      EXIT_BUTTON,
      RESUME_BUTTON,
      OPTIONS_BUTTON,
      EXIT_TO_WINDOWS_BUTTON,
      SUICIDE_BUTTON,
      BUY_MORE_BUTTON,
      SELL_MORE_BUTTON,
      GREEN_TEAM_BUTTON,
      BLUE_TEAM_BUTTON,
      SAVE_SHIP_BUTTON,
      SAVE_SHIP_LOCAL_BUTTON,
      UPLOAD_LOCAL_SHIP_BUTTON,
      NEXT_BUTTON,
      BACK_BUTTON,
      SKIP_BUTTON,
      END_TUTORIAL_BUTTON,
      TAKE_BUTTOM,
      PUT_BUTTOM,
      TAKE_QUANTITY_BUTTOM,
      PUT_QUANTITY_BUTTOM,
      MESSAGE_LOG_BUTTOM;

      public final int getSpriteNum(boolean var1) {
         return this.ordinal() + (var1 ? 32 : 0);
      }
   }
}
