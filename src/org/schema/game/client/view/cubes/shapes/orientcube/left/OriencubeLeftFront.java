package org.schema.game.client.view.cubes.shapes.orientcube.left;

import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.client.view.cubes.shapes.IconInterface;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.cubes.shapes.orientcube.right.OriencubeRightFront;

@BlockShape(
   name = "OriencubeLeftFront"
)
public class OriencubeLeftFront extends OrientCubeLeft implements IconInterface {
   private static final Oriencube mirror = new OriencubeRightFront();

   public byte getOrientCubePrimaryOrientation() {
      return 5;
   }

   public byte getOrientCubeSecondaryOrientation() {
      return 0;
   }

   public Oriencube getMirrorAlgo() {
      return mirror;
   }
}
