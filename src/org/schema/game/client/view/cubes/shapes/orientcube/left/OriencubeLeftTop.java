package org.schema.game.client.view.cubes.shapes.orientcube.left;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.client.view.cubes.shapes.IconInterface;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.cubes.shapes.orientcube.right.OriencubeRightTop;

@BlockShape(
   name = "OriencubeLeftTop"
)
public class OriencubeLeftTop extends OrientCubeLeft implements IconInterface {
   private static final Oriencube mirror = new OriencubeRightTop();

   public byte getOrientCubePrimaryOrientation() {
      return 5;
   }

   public byte getOrientCubeSecondaryOrientation() {
      return 2;
   }

   public Oriencube getMirrorAlgo() {
      return mirror;
   }

   public Transform getSecondaryTransform(Transform var1) {
      var1.setIdentity();
      var1.basis.rotY(4.712389F);
      return var1;
   }
}
