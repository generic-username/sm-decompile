package org.schema.game.client.view.cubes.shapes;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public interface IconInterface {
   void single(BlockRenderInfo var1, byte var2, byte var3, byte var4, byte var5, FloatBuffer var6, AlgorithmParameters var7);

   void single(BlockRenderInfo var1, byte var2, byte var3, byte var4, byte var5, IntBuffer var6, AlgorithmParameters var7);
}
