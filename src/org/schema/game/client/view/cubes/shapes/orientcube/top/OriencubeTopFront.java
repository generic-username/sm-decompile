package org.schema.game.client.view.cubes.shapes.orientcube.top;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.client.view.cubes.shapes.IconInterface;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.cubes.shapes.orientcube.bottom.OriencubeBottomFront;

@BlockShape(
   name = "OriencubeTopFront"
)
public class OriencubeTopFront extends OrientCubeTop implements IconInterface {
   private static final Oriencube mirror = new OriencubeBottomFront();

   public byte getOrientCubePrimaryOrientation() {
      return 2;
   }

   public byte getOrientCubeSecondaryOrientation() {
      return 0;
   }

   public Oriencube getMirrorAlgo() {
      return mirror;
   }

   public Transform getSecondaryTransform(Transform var1) {
      var1.setIdentity();
      return var1;
   }
}
