package org.schema.game.client.view.cubes.shapes;

import java.util.Locale;
import org.schema.game.common.data.element.ElementParserException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public enum BlockStyle {
   NORMAL(0, true, false, false, 6, "normal", "A normal 6 sided block", new String[]{"block"}),
   WEDGE(1, false, true, false, 12, "wedge", "A wedged block", new String[]{"wedge"}),
   CORNER(2, false, true, false, 24, "corner", "A corner blcok with a square base", new String[]{"corner"}),
   SPRITE(3, false, false, true, 6, "sprite", "An X-shaped spritelike block", new String[]{"sprite"}),
   TETRA(4, false, true, false, 8, "tetra", "A corner angeled block with a triangle base", new String[]{"tetra"}),
   HEPTA(5, false, true, false, 8, "penta", "A block with a tetra cut off", new String[]{"hepta"}),
   NORMAL24(6, true, false, false, 24, "normal24", "A block with 24 orientation like rails", new String[]{"24normal"});

   public final int id;
   public final String[] oldNames;
   public final String realName;
   public String desc;
   public int slab;
   public final boolean solidBlockStyle;
   public final boolean blendedBlockStyle;
   public final boolean cube;
   public final int orientations;
   public static final String[] ids = getAsStringId();
   public static final String[] names = getAsStringName();

   public static final String[] getAsStringId() {
      String[] var0 = new String[values().length];

      for(int var1 = 0; var1 < values().length; ++var1) {
         var0[var1] = String.valueOf(values()[var1].id);
      }

      return var0;
   }

   public static final String[] getAsStringName() {
      String[] var0 = new String[values().length];

      for(int var1 = 0; var1 < values().length; ++var1) {
         var0[var1] = String.valueOf(values()[var1].realName);
      }

      return var0;
   }

   private BlockStyle(int var3, boolean var4, boolean var5, boolean var6, int var7, String var8, String var9, String... var10) {
      this.id = var3;
      this.oldNames = var10;
      this.realName = var8;
      this.desc = var9;
      this.cube = var4;
      this.solidBlockStyle = var5;
      this.blendedBlockStyle = var6;
      this.orientations = var7;
   }

   public static String getDescs() {
      StringBuffer var0 = new StringBuffer();

      for(int var1 = 0; var1 < values().length; ++var1) {
         var0.append(values()[var1].realName + ": " + values()[var1].desc + ";\n");
      }

      return var0.toString();
   }

   public static BlockStyle getById(int var0) throws ElementParserException {
      for(int var1 = 0; var1 < values().length; ++var1) {
         if (values()[var1].id == var0) {
            return values()[var1];
         }
      }

      throw new ElementParserException("Block Style not found by id: " + var0);
   }

   public static BlockStyle parse(Element var0) {
      NodeList var4 = var0.getChildNodes();

      for(int var1 = 0; var1 < var4.getLength(); ++var1) {
         Node var2;
         if ((var2 = var4.item(var1)).getNodeType() == 1 && var2.getNodeName().toLowerCase(Locale.ENGLISH).equals("styleid")) {
            int var5 = Integer.parseInt(var2.getTextContent());

            for(int var3 = 0; var3 < values().length; ++var3) {
               if (values()[var3].id == var5) {
                  return values()[var3];
               }
            }
         }
      }

      throw new RuntimeException("NO STYLE");
   }

   public final void write(Element var1, Document var2) {
      Element var3;
      (var3 = var2.createElement("StyleId")).setTextContent(String.valueOf(this.id));
      var1.appendChild(var3);
   }
}
