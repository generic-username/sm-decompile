package org.schema.game.client.view.cubes.shapes.orientcube.bottom;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;

public abstract class OrientCubeBottom extends Oriencube {
   public Transform getPrimaryTransform(Vector3f var1, int var2, Transform var3) {
      var3.basis.rotZ(-3.1415927F);
      var3.origin.set(var1);
      Vector3f var10000 = var3.origin;
      var10000.y -= (float)var2;
      return var3;
   }
}
