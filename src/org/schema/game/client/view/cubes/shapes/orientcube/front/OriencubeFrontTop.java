package org.schema.game.client.view.cubes.shapes.orientcube.front;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.client.view.cubes.shapes.IconInterface;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.cubes.shapes.orientcube.back.OriencubeBackTop;

@BlockShape(
   name = "OriencubeFrontTop"
)
public class OriencubeFrontTop extends OrientCubeFront implements IconInterface {
   private static final Oriencube mirror = new OriencubeBackTop();

   public byte getOrientCubePrimaryOrientation() {
      return 0;
   }

   public byte getOrientCubeSecondaryOrientation() {
      return 2;
   }

   public Oriencube getMirrorAlgo() {
      return mirror;
   }

   public Transform getSecondaryTransform(Transform var1) {
      var1.setIdentity();
      var1.basis.rotY(3.1415927F);
      return var1;
   }
}
