package org.schema.game.client.view.cubes.shapes.tetrahedron;

import java.util.Arrays;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.IconInterface;

public abstract class TetrahedronShapeAlgorithm extends BlockShapeAlgorithm implements IconInterface {
   private int normal;
   private final byte[] wedgeOrientationCache = this.getWedgeOrientation();
   private Vector3i[] angledSideVerts;
   int currentPerm;
   protected int doubleAdd = 0;

   public TetrahedronShapeAlgorithm() {
      assert this.wedgeOrientationCache != null;

      if (this.wedgeOrientationCache != null) {
         byte[] var1;
         Arrays.sort(var1 = Arrays.copyOf(this.wedgeOrientationCache, this.wedgeOrientationCache.length));
         int var2 = 0;
         int var3 = 1;

         for(int var4 = 0; var4 < var1.length; ++var4) {
            var2 += var1[var4] * var3;
            var3 *= 10;
         }

         this.normal = var2;
         this.angledSideVerts = this.getAngledSideVerts();
      }

      assert this.normal > 5;

   }

   public boolean isAngled(int var1) {
      return var1 == this.getSidesAngled()[0];
   }

   public abstract Vector3i[] getAngledSideVerts();

   public int getWedgeNormal() {
      return this.normal;
   }

   public void modAngledVertex(int var1) {
      if (Math.abs(var1) >= 100) {
         if (var1 > 0) {
            var1 -= 100;
            this.doubleAdd = FastMath.cyclicModulo((int)(this.doubleAdd + var1), (int)3);
         } else {
            var1 += 100;
            this.doubleAdd = FastMath.cyclicModulo((int)(this.doubleAdd - var1), (int)3);
         }

         System.err.println(this + " DOUBLE VERTEX " + this.getDoubleVertex());
      } else {
         this.currentPerm = FastMath.cyclicModulo(this.currentPerm + var1, this.vertexPermutations3.length);
         var1 = this.vertexPermutations3[this.currentPerm][0] - 1;
         int var2 = this.vertexPermutations3[this.currentPerm][1] - 1;
         int var3 = this.vertexPermutations3[this.currentPerm][2] - 1;
         Vector3i[] var4 = new Vector3i[]{this.angledSideVerts[var1], this.angledSideVerts[var2], this.angledSideVerts[var3]};
         this.angledSideVerts = var4;

         for(var1 = 0; var1 < this.angledSideVerts.length; ++var1) {
            System.err.println("new Vector3i" + this.angledSideVerts[var1] + ",");
         }

      }
   }

   protected int getAngledSideLightRepresentitive(int var1, int var2) {
      if (var2 == this.getWedgeNormal()) {
         return this.getSidesAngled()[0];
      } else {
         return var1 != this.wedgeOrientationCache[0] && var1 != this.wedgeOrientationCache[1] && var1 != this.wedgeOrientationCache[2] ? super.getAngledSideLightRepresentitive(var1, var2) : -1;
      }
   }

   protected int getRepresentitiveNormal(int var1, int var2) {
      if (var2 == this.getWedgeNormal()) {
         return var2;
      } else {
         return var1 != this.wedgeOrientationCache[0] && var1 != this.wedgeOrientationCache[1] && var1 != this.wedgeOrientationCache[2] ? super.getRepresentitiveNormal(var1, var2) : -1;
      }
   }

   protected Vector3i[] getSideByNormal(int var1, int var2) {
      if (var2 == this.getWedgeNormal()) {
         return this.angledSideVerts;
      } else if (var1 != this.wedgeOrientationCache[0] && var1 != this.wedgeOrientationCache[1] && var1 != this.wedgeOrientationCache[2]) {
         return var2 < 6 ? super.getSideByNormal(var1, var2) : null;
      } else {
         return none;
      }
   }

   protected int getNormalBySide(int var1) {
      if (var1 == this.getSidesAngled()[0]) {
         return this.normal;
      } else {
         return var1 != this.wedgeOrientationCache[0] && var1 != this.wedgeOrientationCache[1] && var1 != this.wedgeOrientationCache[2] ? super.getNormalBySide(var1) : -1;
      }
   }
}
