package org.schema.game.client.view.cubes.shapes.sprite;

import com.bulletphysics.collision.shapes.ConvexShape;
import org.schema.game.client.view.cubes.shapes.AlgorithmParameters;
import org.schema.game.client.view.cubes.shapes.BlockShape;

@BlockShape(
   name = "SpriteBack"
)
public class SpriteBack extends SpriteShapeAlgorythm {
   private final int[] sidesAngled = new int[]{2};
   private final int[] sidesToTest = new int[0];

   protected int findYZ(int var1) {
      return this.findIndex(this.getClass(), var1);
   }

   protected int findXZ(int var1) {
      return this.findIndex(this.getClass(), var1);
   }

   protected int findXY(int var1) {
      return this.findIndex(SpriteFront.class, var1);
   }

   public boolean isPhysical() {
      return false;
   }

   public void createSide(int var1, short var2, AlgorithmParameters var3) {
      var3.vID = var2;
      var3.sid = var1;
      var3.normalMode = 0;
      var3.ext = this.extOrderMap[var1][var2];
      switch(var1) {
      case 0:
         var3.vID = 0;
         return;
      case 1:
         break;
      case 2:
         if (var3.vID != 0) {
            if (var3.vID == 1) {
               var3.sid = 3;
               var3.vID = 2;
               return;
            }

            if (var3.vID == 2) {
               var3.sid = 3;
               var3.vID = 1;
               return;
            }

            if (var3.vID == 3) {
               return;
            }
         }

         return;
      case 3:
         if (var3.vID == 0) {
            var3.sid = 2;
            var3.vID = 3;
            return;
         }

         if (var3.vID == 1 || var3.vID == 2 || var3.vID != 3) {
            return;
         }

         var3.sid = 2;
         break;
      case 4:
         if (var3.vID == 0 || var3.vID == 1) {
            return;
         }

         if (var3.vID == 2) {
            var3.sid = 5;
            var3.vID = 1;
            return;
         }

         if (var3.vID != 3) {
            return;
         }

         var3.sid = 5;
         break;
      case 5:
         if (var3.vID != 0 && var3.vID != 1) {
            if (var3.vID == 2) {
               var3.sid = 4;
               var3.vID = 1;
               return;
            }

            if (var3.vID == 3) {
               var3.sid = 4;
               break;
            }
         }

         return;
      default:
         return;
      }

      var3.vID = 0;
   }

   protected ConvexShape getShape() {
      assert false;

      throw new IllegalArgumentException();
   }

   public int[] getSidesToCheckForVis() {
      return this.sidesToTest;
   }

   public int[] getSidesAngled() {
      return this.sidesAngled;
   }

   public byte getPrimaryOrientation() {
      return 1;
   }
}
