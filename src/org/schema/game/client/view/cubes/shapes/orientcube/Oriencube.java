package org.schema.game.client.view.cubes.shapes.orientcube;

import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.shapes.AlgorithmParameters;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.IconInterface;
import org.schema.game.client.view.cubes.shapes.sprite.SpriteShapeAlgorythm;
import org.schema.game.common.data.element.Element;
import org.schema.schine.graphicsengine.core.GlUtil;

public abstract class Oriencube extends BlockShapeAlgorithm implements IconInterface {
   public int orientationArrayIndex;
   Vector3f finderVertexA = new Vector3f();
   Vector3f finderVertexB = new Vector3f();
   Vector3f finderVertex = new Vector3f();
   private final Transform basicTransform;
   private SpriteShapeAlgorythm repSprite;

   public Oriencube() {
      this.finderVertexA.set(Element.DIRECTIONSf[Element.switchLeftRight(this.getOrientCubePrimaryOrientation())]);
      this.finderVertexB.set(Element.DIRECTIONSf[Element.switchLeftRight(this.getOrientCubeSecondaryOrientation())]);
      this.finderVertexA.scale(1.5F);
      this.finderVertexB.scale(0.5F);
      this.finderVertex.add(this.finderVertexA, this.finderVertexB);
      this.basicTransform = new Transform();
      this.getPrimaryTransform(new Vector3f(), 0, this.basicTransform);
      Transform var1 = this.getSecondaryTransform(new Transform());
      this.basicTransform.mul(var1);
   }

   protected Vector3i[] getSideByNormal(int var1, int var2) {
      return super.getSideByNormal(var1 % 6, var1 % 6);
   }

   public boolean isAngled(int var1) {
      return false;
   }

   public abstract byte getOrientCubePrimaryOrientation();

   public abstract byte getOrientCubeSecondaryOrientation();

   protected int getAngledSideLightRepresentitive(int var1, int var2) {
      return var1 % 6;
   }

   private Oriencube getMirror(int var1, int var2) {
      int var3 = this.getOrientCubePrimaryOrientation();
      int var4 = this.getOrientCubeSecondaryOrientation();
      if (var3 != var1 && var3 != var2) {
         if (var4 == var1 || var4 == var2) {
            var4 = Element.getOpposite(var4);
         }
      } else {
         var3 = Element.getOpposite(var3);
      }

      return getOrientcube(var3, var4);
   }

   protected int findYZ(int var1) {
      return this.getMirror(4, 5).orientationArrayIndex;
   }

   protected int findXZ(int var1) {
      return this.getMirror(2, 3).orientationArrayIndex;
   }

   protected int findXY(int var1) {
      return this.getMirror(0, 1).orientationArrayIndex;
   }

   public int findRot(BlockShapeAlgorithm[] var1, Matrix3f var2) {
      if (this.rotBuffer.containsKey(var2)) {
         return this.rotBuffer.get(var2);
      } else {
         for(int var3 = 0; var3 < var1.length; ++var3) {
            BlockShapeAlgorithm var4;
            if ((var4 = var1[var3]) instanceof Oriencube && this != var4) {
               Vector3f var5 = new Vector3f(this.finderVertex);
               var2.transform(var5);
               if (var5.x > 1.4F) {
                  var5.x = 1.5F;
               } else if (var5.x > 0.4F) {
                  var5.x = 0.5F;
               } else if (var5.x < -1.4F) {
                  var5.x = -1.5F;
               } else if (var5.x < -0.4F) {
                  var5.x = -0.5F;
               } else {
                  var5.x = 0.0F;
               }

               if (var5.y > 1.4F) {
                  var5.y = 1.5F;
               } else if (var5.y > 0.4F) {
                  var5.y = 0.5F;
               } else if (var5.y < -1.4F) {
                  var5.y = -1.5F;
               } else if (var5.y < -0.4F) {
                  var5.y = -0.5F;
               } else {
                  var5.y = 0.0F;
               }

               if (var5.z > 1.4F) {
                  var5.z = 1.5F;
               } else if (var5.z > 0.4F) {
                  var5.z = 0.5F;
               } else if (var5.z < -1.4F) {
                  var5.z = -1.5F;
               } else if (var5.z < -0.4F) {
                  var5.z = -0.5F;
               } else {
                  var5.z = 0.0F;
               }

               if ((new Vector3f(((Oriencube)var4).finderVertex)).equals(var5)) {
                  this.rotBuffer.put(new Matrix3f(var2), var3);
                  return var3;
               }
            }
         }

         throw new IllegalArgumentException(this.toString());
      }
   }

   public void createSide(int var1, short var2, AlgorithmParameters var3) {
      var3.vID = var2;
      var3.sid = var1;
      var3.normalMode = 0;
      var3.ext = this.extOrderMap[var1][var2];
   }

   protected ConvexShape getShape() {
      throw new NullPointerException();
   }

   public boolean hasValidShape() {
      return false;
   }

   public int[] getSidesToCheckForVis() {
      throw new NullPointerException();
   }

   public int[] getSidesAngled() {
      throw new NullPointerException();
   }

   public abstract Oriencube getMirrorAlgo();

   public abstract Transform getPrimaryTransform(Vector3f var1, int var2, Transform var3);

   public Transform getSecondaryTransform(Transform var1) {
      var1.setIdentity();
      return var1;
   }

   public byte getOrientCubePrimaryOrientationSwitchedLeftRight() {
      byte var1;
      if ((var1 = this.getOrientCubePrimaryOrientation()) == 5 || var1 == 4) {
         var1 = (byte)Element.getOpposite(var1);
      }

      return var1;
   }

   public Matrix3f getOrientationMatrix(Matrix3f var1) {
      Vector3f var2 = new Vector3f(Element.DIRECTIONSf[this.getOrientCubePrimaryOrientation()]);
      Vector3f var3 = new Vector3f(Element.DIRECTIONSf[this.getOrientCubeSecondaryOrientation()]);
      Vector3f var4;
      (var4 = new Vector3f()).cross(var2, var3);
      var4.normalize();
      var1.setIdentity();
      GlUtil.setRightVector(var4, var1);
      GlUtil.setUpVector(var2, var1);
      GlUtil.setForwardVector(var3, var1);
      return var1;
   }

   public Matrix3f getOrientationMatrixSwitched(Matrix3f var1) {
      Vector3f var2 = new Vector3f(Element.DIRECTIONSf[Element.switchLeftRight(this.getOrientCubePrimaryOrientation())]);
      Vector3f var3 = new Vector3f(Element.DIRECTIONSf[Element.switchLeftRight(this.getOrientCubeSecondaryOrientation())]);
      Vector3f var4;
      (var4 = new Vector3f()).cross(var2, var3);
      var4.normalize();
      var1.setIdentity();
      GlUtil.setRightVector(var4, var1);
      GlUtil.setUpVector(var2, var1);
      GlUtil.setForwardVector(var3, var1);
      return var1;
   }

   public Transform getBasicTransform() {
      return this.basicTransform;
   }

   public SpriteShapeAlgorythm getSpriteAlgoRepresentitive() {
      return this.repSprite;
   }

   protected void onInit() {
      super.onInit();
      BlockShapeAlgorithm[] var1;
      int var2 = (var1 = BlockShapeAlgorithm.algorithms[2]).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         SpriteShapeAlgorythm var4;
         if ((var4 = (SpriteShapeAlgorythm)var1[var3]).getPrimaryOrientation() == this.getOrientCubePrimaryOrientation()) {
            this.repSprite = var4;
         }
      }

   }
}
