package org.schema.game.client.view.cubes.shapes.orientcube.right;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.client.view.cubes.shapes.IconInterface;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.cubes.shapes.orientcube.left.OriencubeLeftTop;

@BlockShape(
   name = "OriencubeRightTop"
)
public class OriencubeRightTop extends OrientCubeRight implements IconInterface {
   private static final Oriencube mirror = new OriencubeLeftTop();

   public byte getOrientCubePrimaryOrientation() {
      return 4;
   }

   public byte getOrientCubeSecondaryOrientation() {
      return 2;
   }

   public Oriencube getMirrorAlgo() {
      return mirror;
   }

   public Transform getSecondaryTransform(Transform var1) {
      var1.setIdentity();
      var1.basis.rotY(1.5707964F);
      return var1;
   }
}
