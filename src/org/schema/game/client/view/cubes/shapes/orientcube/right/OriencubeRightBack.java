package org.schema.game.client.view.cubes.shapes.orientcube.right;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.client.view.cubes.shapes.IconInterface;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.cubes.shapes.orientcube.left.OriencubeLeftBack;

@BlockShape(
   name = "OriencubeRightBack"
)
public class OriencubeRightBack extends OrientCubeRight implements IconInterface {
   private static final Oriencube mirror = new OriencubeLeftBack();

   public byte getOrientCubePrimaryOrientation() {
      return 4;
   }

   public byte getOrientCubeSecondaryOrientation() {
      return 1;
   }

   public Oriencube getMirrorAlgo() {
      return mirror;
   }

   public Transform getSecondaryTransform(Transform var1) {
      var1.setIdentity();
      var1.basis.rotY(3.1415927F);
      return var1;
   }
}
