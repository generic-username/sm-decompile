package org.schema.game.client.view.cubes.shapes;

public class AlgorithmParameters {
   public short vID;
   public int sid;
   public byte ext = 0;
   public int normalMode = 0;
   public byte insideMode = 0;
   public boolean fresh = true;
}
