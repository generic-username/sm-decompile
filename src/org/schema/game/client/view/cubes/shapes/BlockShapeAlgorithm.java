package org.schema.game.client.view.cubes.shapes;

import com.bulletphysics.collision.shapes.ConvexHullShape;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.util.ObjectArrayList;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import javax.vecmath.Matrix3f;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.StringTools;
import org.schema.common.util.data.DataUtil;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.CubeBufferFloat;
import org.schema.game.client.view.cubes.CubeBufferInt;
import org.schema.game.client.view.cubes.CubeMeshBufferContainer;
import org.schema.game.client.view.cubes.occlusion.Occlusion;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.cubes.shapes.orientcube.back.OriencubeBackBottom;
import org.schema.game.client.view.cubes.shapes.orientcube.back.OriencubeBackLeft;
import org.schema.game.client.view.cubes.shapes.orientcube.back.OriencubeBackRight;
import org.schema.game.client.view.cubes.shapes.orientcube.back.OriencubeBackTop;
import org.schema.game.client.view.cubes.shapes.orientcube.bottom.OriencubeBottomBack;
import org.schema.game.client.view.cubes.shapes.orientcube.bottom.OriencubeBottomFront;
import org.schema.game.client.view.cubes.shapes.orientcube.bottom.OriencubeBottomLeft;
import org.schema.game.client.view.cubes.shapes.orientcube.bottom.OriencubeBottomRight;
import org.schema.game.client.view.cubes.shapes.orientcube.front.OriencubeFrontBottom;
import org.schema.game.client.view.cubes.shapes.orientcube.front.OriencubeFrontLeft;
import org.schema.game.client.view.cubes.shapes.orientcube.front.OriencubeFrontRight;
import org.schema.game.client.view.cubes.shapes.orientcube.front.OriencubeFrontTop;
import org.schema.game.client.view.cubes.shapes.orientcube.left.OriencubeLeftBack;
import org.schema.game.client.view.cubes.shapes.orientcube.left.OriencubeLeftBottom;
import org.schema.game.client.view.cubes.shapes.orientcube.left.OriencubeLeftFront;
import org.schema.game.client.view.cubes.shapes.orientcube.left.OriencubeLeftTop;
import org.schema.game.client.view.cubes.shapes.orientcube.right.OriencubeRightBack;
import org.schema.game.client.view.cubes.shapes.orientcube.right.OriencubeRightBottom;
import org.schema.game.client.view.cubes.shapes.orientcube.right.OriencubeRightFront;
import org.schema.game.client.view.cubes.shapes.orientcube.right.OriencubeRightTop;
import org.schema.game.client.view.cubes.shapes.orientcube.top.OriencubeTopBack;
import org.schema.game.client.view.cubes.shapes.orientcube.top.OriencubeTopFront;
import org.schema.game.client.view.cubes.shapes.orientcube.top.OriencubeTopLeft;
import org.schema.game.client.view.cubes.shapes.orientcube.top.OriencubeTopRight;
import org.schema.game.client.view.cubes.shapes.pentahedron.topbottom.PentaBottomBackLeft;
import org.schema.game.client.view.cubes.shapes.pentahedron.topbottom.PentaBottomBackRight;
import org.schema.game.client.view.cubes.shapes.pentahedron.topbottom.PentaBottomFrontLeft;
import org.schema.game.client.view.cubes.shapes.pentahedron.topbottom.PentaBottomFrontRight;
import org.schema.game.client.view.cubes.shapes.pentahedron.topbottom.PentaTopBackLeft;
import org.schema.game.client.view.cubes.shapes.pentahedron.topbottom.PentaTopBackRight;
import org.schema.game.client.view.cubes.shapes.pentahedron.topbottom.PentaTopFrontLeft;
import org.schema.game.client.view.cubes.shapes.pentahedron.topbottom.PentaTopFrontRight;
import org.schema.game.client.view.cubes.shapes.spike.SpikeIcon;
import org.schema.game.client.view.cubes.shapes.spike.frontback.SpikeBackBackLeft;
import org.schema.game.client.view.cubes.shapes.spike.frontback.SpikeBackBackRight;
import org.schema.game.client.view.cubes.shapes.spike.frontback.SpikeBackFrontLeft;
import org.schema.game.client.view.cubes.shapes.spike.frontback.SpikeBackFrontRight;
import org.schema.game.client.view.cubes.shapes.spike.frontback.SpikeFrontBackLeft;
import org.schema.game.client.view.cubes.shapes.spike.frontback.SpikeFrontBackRight;
import org.schema.game.client.view.cubes.shapes.spike.frontback.SpikeFrontFrontLeft;
import org.schema.game.client.view.cubes.shapes.spike.frontback.SpikeFrontFrontRight;
import org.schema.game.client.view.cubes.shapes.spike.sideways.SpikeLeftBackLeft;
import org.schema.game.client.view.cubes.shapes.spike.sideways.SpikeLeftBackRight;
import org.schema.game.client.view.cubes.shapes.spike.sideways.SpikeLeftFrontLeft;
import org.schema.game.client.view.cubes.shapes.spike.sideways.SpikeLeftFrontRight;
import org.schema.game.client.view.cubes.shapes.spike.sideways.SpikeRightBackLeft;
import org.schema.game.client.view.cubes.shapes.spike.sideways.SpikeRightBackRight;
import org.schema.game.client.view.cubes.shapes.spike.sideways.SpikeRightFrontLeft;
import org.schema.game.client.view.cubes.shapes.spike.sideways.SpikeRightFrontRight;
import org.schema.game.client.view.cubes.shapes.spike.topbottom.SpikeBottomBackLeft;
import org.schema.game.client.view.cubes.shapes.spike.topbottom.SpikeBottomBackRight;
import org.schema.game.client.view.cubes.shapes.spike.topbottom.SpikeBottomFrontLeft;
import org.schema.game.client.view.cubes.shapes.spike.topbottom.SpikeBottomFrontRight;
import org.schema.game.client.view.cubes.shapes.spike.topbottom.SpikeTopBackLeft;
import org.schema.game.client.view.cubes.shapes.spike.topbottom.SpikeTopBackRight;
import org.schema.game.client.view.cubes.shapes.spike.topbottom.SpikeTopFrontLeft;
import org.schema.game.client.view.cubes.shapes.spike.topbottom.SpikeTopFrontRight;
import org.schema.game.client.view.cubes.shapes.sprite.SpriteBack;
import org.schema.game.client.view.cubes.shapes.sprite.SpriteBottom;
import org.schema.game.client.view.cubes.shapes.sprite.SpriteFront;
import org.schema.game.client.view.cubes.shapes.sprite.SpriteLeft;
import org.schema.game.client.view.cubes.shapes.sprite.SpriteRight;
import org.schema.game.client.view.cubes.shapes.sprite.SpriteTop;
import org.schema.game.client.view.cubes.shapes.tetrahedron.TetrahedronBottomBackLeft;
import org.schema.game.client.view.cubes.shapes.tetrahedron.TetrahedronBottomBackRight;
import org.schema.game.client.view.cubes.shapes.tetrahedron.TetrahedronBottomFrontLeft;
import org.schema.game.client.view.cubes.shapes.tetrahedron.TetrahedronBottomFrontRight;
import org.schema.game.client.view.cubes.shapes.tetrahedron.TetrahedronTopBackLeft;
import org.schema.game.client.view.cubes.shapes.tetrahedron.TetrahedronTopBackRight;
import org.schema.game.client.view.cubes.shapes.tetrahedron.TetrahedronTopFrontLeft;
import org.schema.game.client.view.cubes.shapes.tetrahedron.TetrahedronTopFrontRight;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeBottomBack;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeBottomFront;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeBottomLeft;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeBottomRight;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeIcon;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeLeftBack;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeLeftFront;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeLeftLeft;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeLeftRight;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeTopBack;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeTopFront;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeTopLeft;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeTopRight;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.ConvexHullShapeExt;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.resource.FileExt;

public abstract class BlockShapeAlgorithm implements IconInterface {
   public static final int[][] vertexOrderMap = new int[][]{{3, 1, 0, 2}, {2, 0, 1, 3}, {1, 0, 2, 3}, {3, 2, 0, 1}, {2, 0, 1, 3}, {3, 1, 0, 2}};
   public final int[][] vertexPermutations3 = new int[][]{{1, 2, 3}, {1, 3, 2}, {2, 1, 3}, {2, 3, 1}, {3, 2, 1}, {3, 1, 2}};
   public final int[][] vertexPermutations4 = new int[][]{{1, 2, 3, 4}, {2, 1, 3, 4}, {3, 2, 1, 4}, {4, 2, 3, 1}, {1, 2, 4, 3}, {2, 1, 4, 3}, {3, 2, 4, 1}, {4, 2, 1, 3}, {1, 3, 2, 4}, {2, 3, 1, 4}, {3, 1, 2, 4}, {4, 3, 2, 1}, {1, 3, 4, 2}, {2, 3, 4, 1}, {3, 1, 4, 2}, {4, 3, 1, 2}, {1, 4, 2, 3}, {2, 4, 1, 3}, {3, 4, 2, 1}, {4, 1, 2, 3}, {1, 4, 3, 2}, {2, 4, 3, 1}, {3, 4, 1, 2}, {4, 1, 3, 2}};
   public static final byte[][] vertexPermutationsIndex3 = new byte[][]{{0, 1, 2, 3}, {1, 0, 2, 3}, {2, 1, 0, 3}, {3, 1, 2, 0}, {0, 1, 3, 2}, {1, 0, 3, 2}, {2, 1, 3, 0}, {3, 1, 0, 2}, {0, 2, 1, 3}, {1, 2, 0, 3}, {2, 0, 1, 3}, {3, 2, 1, 0}, {0, 2, 3, 1}, {1, 2, 3, 0}, {2, 0, 3, 1}, {3, 2, 0, 1}, {0, 3, 1, 2}, {1, 3, 0, 2}, {2, 3, 1, 0}, {3, 0, 1, 2}, {0, 3, 2, 1}, {1, 3, 2, 0}, {2, 3, 0, 1}, {3, 0, 2, 1}};
   public static final byte[][] extOrderPerm = new byte[24][4];
   public static final byte[][][] texOrderMapNormal = new byte[][][]{{{2, 0, 1, 3}, {3, 1, 0, 2}, {3, 1, 0, 2}, {2, 0, 1, 3}, {3, 1, 0, 2}, {2, 0, 1, 3}}, {{2, 0, 1, 3}, {3, 1, 0, 2}, {3, 1, 0, 2}, {2, 0, 1, 3}, {3, 1, 0, 2}, {2, 0, 1, 3}}, {{2, 0, 1, 3}, {3, 1, 0, 2}, {3, 1, 0, 2}, {2, 0, 1, 3}, {3, 1, 0, 2}, {2, 0, 1, 3}}, {{2, 0, 1, 3}, {3, 1, 0, 2}, {3, 1, 0, 2}, {2, 0, 1, 3}, {3, 1, 0, 2}, {2, 0, 1, 3}}, {{2, 0, 1, 3}, {3, 1, 0, 2}, {3, 1, 0, 2}, {2, 0, 1, 3}, {3, 1, 0, 2}, {2, 0, 1, 3}}, {{2, 0, 1, 3}, {3, 1, 0, 2}, {3, 1, 0, 2}, {2, 0, 1, 3}, {3, 1, 0, 2}, {2, 0, 1, 3}}};
   public static final byte[][][] texOrderMap4x4 = new byte[][][]{{{2, 0, 1, 3}, {3, 1, 0, 2}, {3, 1, 0, 2}, {2, 0, 1, 3}, {3, 1, 0, 2}, {2, 0, 1, 3}}, {{2, 0, 1, 3}, {3, 1, 0, 2}, {3, 1, 0, 2}, {2, 0, 1, 3}, {3, 1, 0, 2}, {2, 0, 1, 3}}, {{2, 0, 1, 3}, {3, 1, 0, 2}, {3, 1, 0, 2}, {2, 0, 1, 3}, {3, 1, 0, 2}, {2, 0, 1, 3}}, {{2, 0, 1, 3}, {3, 1, 0, 2}, {3, 1, 0, 2}, {2, 0, 1, 3}, {3, 1, 0, 2}, {2, 0, 1, 3}}, {{2, 0, 1, 3}, {3, 1, 0, 2}, {3, 1, 0, 2}, {2, 0, 1, 3}, {3, 1, 0, 2}, {2, 0, 1, 3}}, {{2, 0, 1, 3}, {3, 1, 0, 2}, {3, 1, 0, 2}, {2, 0, 1, 3}, {3, 1, 0, 2}, {2, 0, 1, 3}}};
   public static final byte[][][] texOrderMapPointToOrient = new byte[][][]{{{2, 0, 1, 3}, {3, 1, 0, 2}, {3, 1, 0, 2}, {2, 0, 1, 3}, {3, 1, 0, 2}, {2, 0, 1, 3}}, {{2, 0, 1, 3}, {3, 1, 0, 2}, {3, 1, 0, 2}, {2, 0, 1, 3}, {3, 1, 0, 2}, {2, 0, 1, 3}}, {{2, 0, 1, 3}, {3, 1, 0, 2}, {3, 1, 0, 2}, {2, 0, 1, 3}, {3, 1, 0, 2}, {2, 0, 1, 3}}, {{2, 0, 1, 3}, {3, 1, 0, 2}, {3, 1, 0, 2}, {2, 0, 1, 3}, {3, 1, 0, 2}, {2, 0, 1, 3}}, {{2, 0, 1, 3}, {3, 1, 0, 2}, {3, 1, 0, 2}, {2, 0, 1, 3}, {3, 1, 0, 2}, {2, 0, 1, 3}}, {{2, 0, 1, 3}, {3, 1, 0, 2}, {3, 1, 0, 2}, {2, 0, 1, 3}, {3, 1, 0, 2}, {2, 0, 1, 3}}};
   public static final short[] vertexTriangleOrder = new short[]{0, 1, 2, 2, 3, 0};
   protected static final float SMALL_SCALE = 0.98F;
   protected static final Vector3f originalScaling = new Vector3f(1.0F, 1.0F, 1.0F);
   protected static final Vector3f smallerScaling = new Vector3f(0.48F, 0.48F, 0.48F);
   private static final float SMALL_SCALE_CALC = 0.99F;
   public static Properties prop;
   public static String configPath;
   public static BlockShapeAlgorithm[][] algorithms;
   public static int[][] xyMappings;
   public static int[][] xzMappings;
   public static int[][] yzMappings;
   private static IntOpenHashSet normalBlockAlgorithmIndices;
   private static Oriencube[][] orientcubes;
   private static boolean initialized;
   public final byte[][] extOrderMap;
   public final int[] extOrderMapPointer;
   public ConvexHullShapeExt smallerShape;
   public Object2IntOpenHashMap rotBuffer;
   public static int[][][][][] halfBlockConfig;
   private static int[] noSides;
   private static int[] oneSide;
   private static Vector3i[][] relativeBySide;
   public static Vector3i[] none;
   private static int[] openToAirNone;

   public BlockShapeAlgorithm() {
      this.extOrderMap = new byte[][]{extOrderPerm[0], extOrderPerm[0], extOrderPerm[0], extOrderPerm[0], extOrderPerm[0], extOrderPerm[0], extOrderPerm[0]};
      this.extOrderMapPointer = new int[]{0, 0, 0, 0, 0, 0, 0};
      this.rotBuffer = new Object2IntOpenHashMap();
      this.initTexOrder();
      this.rememberTexOrder();
   }

   public static void initialize() throws IOException {
      if (!initialized) {
         prop = new Properties();
         read();
         ArrayList var0 = new ArrayList();
         permute(new byte[]{0, 1, 2, 3}, 0, var0);

         int var1;
         for(var1 = 0; var1 < var0.size(); ++var1) {
            extOrderPerm[var1] = (byte[])var0.get(var1);
         }

         (normalBlockAlgorithmIndices = new IntOpenHashSet()).add(5);
         algorithms = new BlockShapeAlgorithm[][]{{new WedgeTopFront(), new WedgeTopRight(), new WedgeTopBack(), new WedgeTopLeft(), new WedgeBottomFront(), new WedgeBottomRight(), new WedgeBottomBack(), new WedgeBottomLeft(), new WedgeLeftFront(), new WedgeLeftRight(), new WedgeLeftBack(), new WedgeLeftLeft(), new WedgeTopFront(), new WedgeTopRight(), new WedgeTopBack(), new WedgeTopLeft(), new WedgeBottomFront(), new WedgeBottomRight(), new WedgeBottomBack(), new WedgeBottomLeft(), new WedgeLeftFront(), new WedgeLeftRight(), new WedgeLeftBack(), new WedgeLeftLeft(), new WedgeIcon()}, {new SpikeTopFrontRight(), new SpikeTopBackRight(), new SpikeTopBackLeft(), new SpikeTopFrontLeft(), new SpikeBottomFrontRight(), new SpikeBottomBackRight(), new SpikeBottomBackLeft(), new SpikeBottomFrontLeft(), new SpikeFrontFrontRight(), new SpikeFrontBackRight(), new SpikeFrontBackLeft(), new SpikeFrontFrontLeft(), new SpikeBackFrontRight(), new SpikeBackBackRight(), new SpikeBackBackLeft(), new SpikeBackFrontLeft(), new SpikeRightFrontRight(), new SpikeRightBackRight(), new SpikeRightBackLeft(), new SpikeRightFrontLeft(), new SpikeLeftFrontRight(), new SpikeLeftBackRight(), new SpikeLeftBackLeft(), new SpikeLeftFrontLeft(), new SpikeIcon()}, {new SpriteFront(), new SpriteBack(), new SpriteTop(), new SpriteBottom(), new SpriteRight(), new SpriteLeft(), new SpriteFront(), new SpriteBack(), new SpriteTop(), new SpriteBottom(), new SpriteRight(), new SpriteLeft(), new SpriteFront(), new SpriteBack(), new SpriteTop(), new SpriteBottom(), new SpriteRight(), new SpriteLeft(), new SpriteFront(), new SpriteBack(), new SpriteTop(), new SpriteBottom(), new SpriteRight(), new SpriteLeft(), new SpriteFront(), new SpriteBack(), new SpriteTop(), new SpriteBottom(), new SpriteRight(), new SpriteLeft(), new SpriteFront(), new SpriteBack(), new SpriteTop(), new SpriteBottom(), new SpriteRight(), new SpriteLeft(), new SpriteBottom()}, {new TetrahedronTopFrontRight(), new TetrahedronTopBackRight(), new TetrahedronTopBackLeft(), new TetrahedronTopFrontLeft(), new TetrahedronBottomFrontRight(), new TetrahedronBottomBackRight(), new TetrahedronBottomBackLeft(), new TetrahedronBottomFrontLeft(), new TetrahedronTopFrontRight(), new TetrahedronTopBackRight(), new TetrahedronTopBackLeft(), new TetrahedronTopFrontLeft(), new TetrahedronBottomFrontRight(), new TetrahedronBottomBackRight(), new TetrahedronBottomBackLeft(), new TetrahedronBottomFrontLeft(), new TetrahedronTopFrontRight(), new TetrahedronTopBackRight(), new TetrahedronTopBackLeft(), new TetrahedronTopFrontLeft(), new TetrahedronBottomFrontRight(), new TetrahedronBottomBackRight(), new TetrahedronBottomBackLeft(), new TetrahedronBottomFrontLeft(), new SpikeIcon()}, {new PentaTopFrontRight(), new PentaTopBackRight(), new PentaTopBackLeft(), new PentaTopFrontLeft(), new PentaBottomFrontRight(), new PentaBottomBackRight(), new PentaBottomBackLeft(), new PentaBottomFrontLeft(), new PentaTopFrontRight(), new PentaTopBackRight(), new PentaTopBackLeft(), new PentaTopFrontLeft(), new PentaBottomFrontRight(), new PentaBottomBackRight(), new PentaBottomBackLeft(), new PentaBottomFrontLeft(), new PentaTopFrontRight(), new PentaTopBackRight(), new PentaTopBackLeft(), new PentaTopFrontLeft(), new PentaBottomFrontRight(), new PentaBottomBackRight(), new PentaBottomBackLeft(), new PentaBottomFrontLeft(), new SpikeIcon()}, {new OriencubeFrontBottom(), new OriencubeFrontLeft(), new OriencubeFrontTop(), new OriencubeFrontRight(), new OriencubeBackBottom(), new OriencubeBackLeft(), new OriencubeBackTop(), new OriencubeBackRight(), new OriencubeBottomBack(), new OriencubeBottomLeft(), new OriencubeBottomFront(), new OriencubeBottomRight(), new OriencubeTopBack(), new OriencubeTopLeft(), new OriencubeTopFront(), new OriencubeTopRight(), new OriencubeRightFront(), new OriencubeRightTop(), new OriencubeRightBack(), new OriencubeRightBottom(), new OriencubeLeftFront(), new OriencubeLeftTop(), new OriencubeLeftBack(), new OriencubeLeftBottom(), new SpriteBottom()}};

         int var4;
         for(var1 = 0; var1 < algorithms.length; ++var1) {
            for(var4 = 0; var4 < algorithms[var1].length - 1; ++var4) {
               assert normalBlockAlgorithmIndices.contains(var1) || algorithms[var1][var4].getSidesToCheckForVis() != null : algorithms[var1][var4].getClass().getSimpleName();

               algorithms[var1][var4].createSmallShape();
               algorithms[var1][var4].onInit();
            }
         }

         int var2;
         for(var1 = 0; var1 < 6; ++var1) {
            for(var4 = 0; var4 < 6; ++var4) {
               for(var2 = 0; var2 < algorithms[5].length - 1; ++var2) {
                  if (((Oriencube)algorithms[5][var2]).getOrientCubePrimaryOrientation() == var1 && ((Oriencube)algorithms[5][var2]).getOrientCubeSecondaryOrientation() == var4) {
                     orientcubes[var1][var4] = (Oriencube)algorithms[5][var2];
                     orientcubes[var1][var4].orientationArrayIndex = var2;
                     break;
                  }
               }
            }
         }

         xyMappings = new int[algorithms.length][algorithms[0].length];
         xzMappings = new int[algorithms.length][algorithms[0].length];
         yzMappings = new int[algorithms.length][algorithms[0].length];

         for(var1 = 0; var1 < xyMappings.length; ++var1) {
            for(var4 = 0; var4 < xyMappings[var1].length - 1; ++var4) {
               xyMappings[var1][var4] = algorithms[var1][var4].findXY(var1);
               xzMappings[var1][var4] = algorithms[var1][var4].findXZ(var1);
               yzMappings[var1][var4] = algorithms[var1][var4].findYZ(var1);
            }
         }

         BlockShapeAlgorithm.TexOrderStyle[] var5;
         try {
            var4 = (var5 = BlockShapeAlgorithm.TexOrderStyle.values()).length;

            for(var2 = 0; var2 < var4; ++var2) {
               readTexOrder(var5[var2]);
            }
         } catch (Exception var3) {
            var5 = null;
            var3.printStackTrace();
         }

         initialized = true;
      }
   }

   public void onInit() {
   }

   public static Oriencube getOrientcube(int var0, int var1) {
      assert var0 != var1 : "can't request that algorithm. illogical: " + Element.getSideString(var0) + "; " + Element.getSideString(var1);

      return orientcubes[var0][var1];
   }

   public int getDoubleVertex() {
      return 0;
   }

   public static void read() {
      FileInputStream var0 = null;
      boolean var7 = false;

      label78: {
         try {
            var7 = true;
            var0 = new FileInputStream(configPath);
            prop.load(var0);
            var7 = false;
            break label78;
         } catch (IOException var11) {
            var11.printStackTrace();
            var7 = false;
         } finally {
            if (var7) {
               if (var0 != null) {
                  try {
                     var0.close();
                  } catch (IOException var9) {
                     var9.printStackTrace();
                  }
               }

            }
         }

         if (var0 != null) {
            try {
               var0.close();
               return;
            } catch (IOException var8) {
               var8.printStackTrace();
               return;
            }
         }

         return;
      }

      try {
         var0.close();
      } catch (IOException var10) {
         var10.printStackTrace();
      }
   }

   public static void permute(byte[] var0, int var1, ArrayList var2) {
      boolean var3 = false;

      int var4;
      for(var4 = 0; var4 < var2.size() && !var3; ++var4) {
         var3 = var3 || Arrays.equals((byte[])var2.get(var4), var0);
      }

      if (!var3) {
         var2.add(var0);
      }

      if (var0.length != var1) {
         for(var4 = var1; var4 < var0.length; ++var4) {
            byte[] var6;
            byte var5 = (var6 = Arrays.copyOf(var0, var0.length))[var4];
            var6[var4] = var6[var1];
            var6[var1] = var5;
            permute(var6, var1 + 1, var2);
         }

      }
   }

   public static void rearrageStat(int var0, boolean var1) {
   }

   public static void main(String[] var0) {
      HalfBlockArray.printHalfBlockArray();
   }

   public static boolean isValid(int var0, byte var1) {
      return var0 > 0 && getLocalAlgoIndex(var0, var1) < algorithms[var0 - 1].length;
   }

   public static boolean isValid(BlockStyle var0, byte var1) {
      return isValid(var0.id, var1);
   }

   public static byte getLocalAlgoIndex(BlockStyle var0, byte var1) {
      return getLocalAlgoIndex(var0.id, var1);
   }

   public static byte getLocalAlgoIndex(int var0, byte var1) {
      return (byte)(var1 % (algorithms[var0 - 1].length - 1));
   }

   public static BlockShapeAlgorithm getAlgo(BlockStyle var0, byte var1) {
      return getAlgo(var0.id, var1);
   }

   public static BlockShapeAlgorithm getAlgo(int var0, byte var1) {
      BlockShapeAlgorithm var2;
      if ((var2 = algorithms[var0 - 1][getLocalAlgoIndex(var0, var1)]) == null) {
         throw new NullPointerException("ERROR: algorithm does not exitst " + var0 + "; " + var1);
      } else {
         return var2;
      }
   }

   public static int[] getSidesToCheckForVis(ElementInformation var0, byte var1) {
      assert var0.getBlockStyle().solidBlockStyle == (!var0.isNormalBlockStyle() && var0.getBlockStyle() != BlockStyle.SPRITE);

      if (var0.blockStyle == BlockStyle.NORMAL24) {
         var1 = (byte)(var1 / 4);
      }

      if (var0.slab > 0) {
         oneSide[0] = Element.switchLeftRight(Element.getOpposite(var1));
         return oneSide;
      } else {
         return var0.getBlockStyle().solidBlockStyle && (!var0.hasLod() || var0.lodShapeStyle != 1) ? getAlgo(var0.blockStyle.id, var1).getSidesToCheckForVis() : noSides;
      }
   }

   public static ConvexShape getShape(int var0, byte var1) {
      return getAlgo(var0, var1).getShape();
   }

   public static ConvexShape getShape(BlockStyle var0, byte var1) {
      return getAlgo(var0.id, var1).getShape();
   }

   public static ConvexShape getSmallShape(BlockStyle var0, byte var1) {
      assert getAlgo(var0, var1).smallerShape != null : getAlgo(var0, var1);

      return getAlgo(var0, var1).smallerShape;
   }

   public static void readTexOrder() throws IOException {
      BlockShapeAlgorithm.TexOrderStyle[] var0;
      int var1 = (var0 = BlockShapeAlgorithm.TexOrderStyle.values()).length;

      for(int var2 = 0; var2 < var1; ++var2) {
         BlockShapeAlgorithm.TexOrderStyle var3;
         readTexOrder(var3 = var0[var2]);
         readTexOrder(var3);
         readTexOrder(var3);
      }

   }

   public static void readTexOrder(BlockShapeAlgorithm.TexOrderStyle var0) throws IOException {
      BufferedReader var1 = new BufferedReader(new FileReader(new FileExt(var0.path)));
      byte[][][] var6 = var0.map;
      String var2 = null;
      int var3 = 0;
      int var4 = 0;

      while(true) {
         do {
            do {
               if ((var2 = var1.readLine()) == null) {
                  var1.close();
                  return;
               }
            } while(var2.startsWith("//"));
         } while(!var2.contains(","));

         int var5 = var2.indexOf("//");
         String[] var7 = var2.substring(0, var5 < 0 ? var2.length() : var5).split(",");

         for(var5 = 0; var5 < var7.length; ++var5) {
            var6[var3][var4][var5] = (byte)Integer.parseInt(var7[var5].trim());
         }

         ++var4;
         if (var4 == 6) {
            var4 = 0;
            ++var3;
         }
      }
   }

   public static void writeTexOrder(BlockShapeAlgorithm.TexOrderStyle var0) throws IOException {
      BufferedWriter var1 = new BufferedWriter(new FileWriter(new FileExt(var0.path)));
      byte[][][] var5 = var0.map;

      for(int var2 = 0; var2 < var5.length; ++var2) {
         var1.write("//" + Element.getSideString(var2).toUpperCase(Locale.ENGLISH) + "\n");

         for(int var3 = 0; var3 < var5[var2].length; ++var3) {
            for(int var4 = 0; var4 < var5[var2][var3].length; ++var4) {
               var1.write(String.valueOf(var5[var2][var3][var4]));
               if (var4 < var5[var2][var3].length - 1) {
                  var1.write(",");
               }
            }

            var1.write(" //" + Element.getSideString(var3).toUpperCase(Locale.ENGLISH) + "\n");
         }
      }

      var1.close();
   }

   public static void normalBlock(BlockRenderInfo var0) {
      short var1;
      if (CubeMeshBufferContainer.isTriangle()) {
         for(var1 = 0; var1 < CubeMeshBufferContainer.VERTS_PER_FACE; ++var1) {
            short var2;
            short var6 = var2 = vertexTriangleOrder[var1];
            int var7 = var0.sideId;
            byte var5;
            if (var0.threeSided) {
               var5 = BlockShapeAlgorithm.TexOrderStyle.NORMAL.map[0][var0.sideId][var2];
            } else {
               var5 = var0.pointToOrientation.map[var0.orientation % 6][var0.sideId][var2];
            }

            put(var0, var7, var6, var5, var2, 0, (byte)0, false);
         }

      } else {
         for(var1 = 0; var1 < CubeMeshBufferContainer.VERTS_PER_FACE; ++var1) {
            int var3 = var0.sideId;
            byte var4;
            if (var0.threeSided) {
               var4 = BlockShapeAlgorithm.TexOrderStyle.NORMAL.map[0][var0.sideId][var1];
            } else {
               var4 = var0.pointToOrientation.map[var0.orientation % 6][var0.sideId][var1];
            }

            put(var0, var3, var1, var4, var1, 0, (byte)0, false);
         }

      }
   }

   protected static void put(BlockRenderInfo var0, int var1, short var2, byte var3, short var4, int var5, byte var6, boolean var7) {
      byte var8 = var0.container.getFinalLight(var0.lightIndex, var0.sideOccId + var4, 0);
      byte var9 = var0.container.getFinalLight(var0.lightIndex, var0.sideOccId + var4, 1);
      byte var10 = var0.container.getFinalLight(var0.lightIndex, var0.sideOccId + var4, 2);
      byte var11 = var0.container.getFinalLight(var0.lightIndex, var0.sideOccId + var4, 3);
      byte var12 = var0.container.getFinalLight(var0.lightIndex, var0.sideOccId + var4, 4);
      byte var13 = var0.container.getFinalLight(var0.lightIndex, var0.sideOccId + var4, 5);
      byte var14 = var0.container.getFinalLight(var0.lightIndex, var0.sideOccId + var4, 6);
      if (GraphicsContext.INTEGER_VERTICES) {
         IntBuffer var16 = var7 ? ((CubeBufferInt)var0.container.dataBuffer).buffers[6] : ((CubeBufferInt)var0.container.dataBuffer).buffers[var1];
         put(var0, var1, var2, var3, var4, var8, var9, var10, var11, var12, var13, var14, var5, var6, var16);
      } else {
         FloatBuffer var15 = var7 ? ((CubeBufferFloat)var0.container.dataBuffer).buffers[6] : ((CubeBufferFloat)var0.container.dataBuffer).buffers[var1];
         put(var0, var1, var2, var3, var4, var8, var9, var10, var11, var5, var6, var15);
      }
   }

   protected static void put(BlockRenderInfo var0, int var1, short var2, byte var3, short var4, byte var5, byte var6, byte var7, byte var8, byte var9, byte var10, byte var11, int var12, byte var13, IntBuffer var14) {
      int[] var18 = HalfBlockArray.getHalfBlockConfig(var0.blockStyle, var4, var1, var0.orientation, var0.halvedFactor);
      int var15 = ByteUtil.getCodeI((byte)var1, var0.layer, var0.typeCode, var0.hitPointsCode, var0.animatedCode, var3, (byte)var18[2], var0.onlyInBuildMode, var0.extendedBlockTexture);
      int var16 = ByteUtil.getCodeIndexI(var0.index, var5, var6, var7);
      int var17 = ByteUtil.getCodeSI(var12, var0.resOverlay, var8, var9, var10, var11, var18[0], var18[1]);
      var1 = (byte)vertexOrderMap[var1][var2] + var15;
      var14.put(var16);
      var14.put(var1);
      var14.put((int)var0.segIndex);
      var14.put(var17);

      assert var0.index + var4 < 393216 : "vert index is bigger: " + (var0.index + var4) + "/393216";

   }

   protected static void put(BlockRenderInfo var0, int var1, short var2, byte var3, short var4, byte var5, byte var6, byte var7, byte var8, int var9, byte var10, FloatBuffer var11) {
      int[] var12 = HalfBlockArray.getHalfBlockConfig(var0.blockStyle, var4, var1, var0.orientation, var0.halvedFactor);
      float var14 = ByteUtil.getCodeF((byte)var1, var0.layer, var0.typeCode, var0.hitPointsCode, var0.animatedCode, var3, (byte)var12[2], var0.onlyInBuildMode);
      float var15 = ByteUtil.getCodeIndexF((float)var0.index, var5, var6, var7);
      float var16 = ByteUtil.getCodeS(var9, var0.resOverlay, var8, var12[0], var12[1]);
      float var13 = (float)((byte)vertexOrderMap[var1][var2]) + var14;
      var11.put(var15);
      var11.put(var13);
      if (CubeMeshBufferContainer.vertexComponents > 2) {
         var11.put(var0.segIndex);
         var11.put(var16);
      }

      assert var0.index + var4 < 393216 : "vert index is bigger: " + (var0.index + var4) + "/393216";

   }

   public byte[] getWedgeOrientation() {
      return null;
   }

   public byte getWedgeGravityValidDir(byte var1) {
      return -1;
   }

   public int[] getProperty() {
      BlockShape var1 = (BlockShape)this.getClass().getAnnotation(BlockShape.class);

      assert prop != null;

      String var2 = prop.getProperty(var1.name());

      assert var2 != null : "not found in file: " + var1.name();

      int[] var4 = new int[7];
      String[] var5 = var2.split(",");

      for(int var3 = 0; var3 < 7; ++var3) {
         var4[var3] = Integer.parseInt(var5[var3]);
      }

      return var4;
   }

   public String propertyToString(int[] var1) {
      StringBuffer var2 = new StringBuffer();

      for(int var3 = 0; var3 < 7; ++var3) {
         var2.append(var1[var3]);
         if (var3 < 6) {
            var2.append(",");
         }
      }

      return var2.toString();
   }

   public static void changeTexCoordOrder(SegmentPiece var0, int var1, int var2) throws IOException {
      ElementInformation var3;
      if ((var3 = ElementKeyMap.getInfo(var0.getType())).individualSides == 3) {
         BlockShapeAlgorithm.TexOrderStyle var5;
         permute((var5 = BlockShapeAlgorithm.TexOrderStyle.NORMAL).map[0][var1], var2);
         writeTexOrder(var5);
      } else {
         byte var4 = var0.getOrientation();
         BlockShapeAlgorithm.TexOrderStyle var6;
         if (var3.extendedTexture) {
            var6 = BlockShapeAlgorithm.TexOrderStyle.AREA4x4;
         } else if (var3.sideTexturesPointToOrientation) {
            var6 = BlockShapeAlgorithm.TexOrderStyle.ORIENT;
         } else {
            var6 = BlockShapeAlgorithm.TexOrderStyle.NORMAL;
         }

         permute(var6.map[var4 % 6][var1], var2);
         writeTexOrder(var6);
      }
   }

   public static void permute(byte[] var0, int var1) {
      int var2 = -1;

      int var3;
      for(var3 = 0; var3 < vertexPermutationsIndex3.length; ++var3) {
         if (Arrays.equals(vertexPermutationsIndex3[var3], var0)) {
            var2 = var3;
            break;
         }
      }

      if (var2 == -1) {
         throw new NullPointerException("No permutation found");
      } else {
         if ((var2 += var1) < 0) {
            var2 = vertexPermutationsIndex3.length - 1;
         } else if (var2 >= vertexPermutationsIndex3.length) {
            var2 = 0;
         }

         for(var3 = 0; var3 < vertexPermutationsIndex3[var2].length; ++var3) {
            var0[var3] = vertexPermutationsIndex3[var2][var3];
         }

      }
   }

   public void modProperty(int var1, int var2) {
      System.err.println("MOD PROPERTY: SIDE: " + var1 + "; dir " + var2);
      int[] var3 = this.getProperty();

      assert var1 >= 0 : var1 + "; " + var2;

      var3[var1] = FastMath.cyclicModulo((int)(var3[var1] + var2), (int)24);
      BlockShape var15 = (BlockShape)this.getClass().getAnnotation(BlockShape.class);
      prop.setProperty(var15.name(), this.propertyToString(var3));
      FileOutputStream var16 = null;
      boolean var9 = false;

      label85: {
         label84: {
            try {
               SimpleDateFormat var17;
               try {
                  var9 = true;
                  var16 = new FileOutputStream(configPath);
                  var17 = StringTools.getSimpleDateFormat(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_CUBES_SHAPES_BLOCKSHAPEALGORITHM_0, "yyyy/MM/dd HH:mm:ss");
                  Date var18 = new Date();
                  prop.store(var16, "Generated " + var17.format(var18));
                  var9 = false;
                  break label84;
               } catch (IOException var13) {
                  var17 = null;
                  var13.printStackTrace();
                  var9 = false;
               }
            } finally {
               if (var9) {
                  if (var16 != null) {
                     try {
                        var16.close();
                     } catch (IOException var10) {
                        var10.printStackTrace();
                     }
                  }

               }
            }

            if (var16 != null) {
               try {
                  var16.close();
               } catch (IOException var11) {
                  var11.printStackTrace();
               }
            }
            break label85;
         }

         try {
            var16.close();
         } catch (IOException var12) {
            var12.printStackTrace();
         }
      }

      this.initTexOrder();
   }

   private void initTexOrder() {
      int[] var1 = this.getProperty();

      for(int var2 = 0; var2 < 7; ++var2) {
         this.extOrderMap[var2] = extOrderPerm[var1[var2]];
      }

   }

   protected int findIndex(Class var1, int var2) {
      for(int var3 = 0; var3 < algorithms[var2].length; ++var3) {
         if (var1.equals(algorithms[var2][var3].getClass())) {
            return var3;
         }
      }

      throw new IllegalArgumentException("NOT FOUND " + var1 + "; in type " + var2 + ": " + Arrays.toString(algorithms[var2]));
   }

   public void rearrage(int var1, int var2) {
      this.extOrderMapPointer[var1] = FastMath.cyclicModulo(this.extOrderMapPointer[var1] + var2, extOrderPerm.length);
      this.extOrderMap[var1] = extOrderPerm[this.extOrderMapPointer[var1]];
      System.err.println("REARRAGED: " + this + ": " + Element.getSideString(var1) + "(" + var1 + "): " + Arrays.toString(this.extOrderMap[var1]) + "-->" + this.extOrderMapPointer[var1]);
   }

   private void rememberTexOrder() {
      for(int var1 = 0; var1 < 7; ++var1) {
         byte[] var2 = this.extOrderMap[var1];
         boolean var3 = false;

         for(int var4 = 0; var4 < extOrderPerm.length; ++var4) {
            if (Arrays.equals(extOrderPerm[var4], var2)) {
               var3 = true;
               this.extOrderMapPointer[var1] = var4;
               break;
            }
         }

         assert var3;
      }

   }

   public void output() {
      System.err.println(this);
      System.err.println("\t");
      System.err.println("\t\tassert(this instanceof " + this.getClass().getSimpleName() + "):this+\" but " + this.getClass().getSimpleName() + "\";");

      for(int var1 = 0; var1 < 6; ++var1) {
         System.err.println("\t\textOrderMap[Element." + Element.getSideString(var1) + "] = extOrderPerm[" + this.extOrderMapPointer[var1] + "];");
      }

      System.err.println("\t}");
   }

   protected int findYZ(int var1) {
      for(int var2 = 0; var2 < algorithms[var1].length; ++var2) {
         BlockShapeAlgorithm var3;
         if ((var3 = algorithms[var1][var2]).isPhysical()) {
            ConvexHullShapeExt var11 = (ConvexHullShapeExt)var3.getShape();
            ConvexHullShapeExt var4 = (ConvexHullShapeExt)this.getShape();
            boolean var5 = true;

            int var6;
            Vector3f var7;
            boolean var8;
            int var9;
            Vector3f var10;
            for(var6 = 0; var6 < var4.getNumPoints(); ++var6) {
               var7 = new Vector3f();
               var4.getVertex(var6, var7);
               var7.x = -var7.x;
               var8 = false;

               for(var9 = 0; var9 < var11.getNumPoints(); ++var9) {
                  var10 = new Vector3f();
                  var11.getVertex(var9, var10);
                  if (var10.equals(var7)) {
                     var8 = true;
                     break;
                  }
               }

               if (!var8) {
                  var5 = false;
                  break;
               }
            }

            if (var5) {
               if (var11.getNumPoints() < 6 && algorithms[var1][var2] == this) {
                  for(var6 = 0; var6 < var4.getNumPoints(); ++var6) {
                     var7 = new Vector3f();
                     var4.getVertex(var6, var7);
                     var7.x = -var7.x;
                     var8 = false;

                     for(var9 = 0; var9 < var11.getNumPoints(); ++var9) {
                        var10 = new Vector3f();
                        var11.getVertex(var9, var10);
                        if (var10.equals(var7)) {
                           var8 = true;
                           break;
                        }
                     }

                     if (!var8) {
                        break;
                     }
                  }
               }

               assert var11.getNumPoints() == 6 || algorithms[var1][var2] != this : algorithms[var1][var2] + "; " + this;

               return var2;
            }
         }
      }

      throw new IllegalArgumentException(this.toString());
   }

   protected int findXZ(int var1) {
      for(int var2 = 0; var2 < algorithms[var1].length - 1; ++var2) {
         BlockShapeAlgorithm var3;
         if ((var3 = algorithms[var1][var2]).isPhysical()) {
            ConvexHullShapeExt var11 = (ConvexHullShapeExt)var3.getShape();
            ConvexHullShapeExt var4 = (ConvexHullShapeExt)this.getShape();
            boolean var5 = true;

            for(int var6 = 0; var6 < var4.getNumPoints(); ++var6) {
               Vector3f var7 = new Vector3f();
               var4.getVertex(var6, var7);
               var7.y = -var7.y;
               boolean var8 = false;

               for(int var9 = 0; var9 < var11.getNumPoints(); ++var9) {
                  Vector3f var10 = new Vector3f();
                  var11.getVertex(var9, var10);
                  if (var10.equals(var7)) {
                     var8 = true;
                     break;
                  }
               }

               if (!var8) {
                  var5 = false;
                  break;
               }
            }

            if (var5) {
               return var2;
            }
         }
      }

      throw new IllegalArgumentException(this.toString());
   }

   protected int findXY(int var1) {
      for(int var2 = 0; var2 < algorithms[var1].length; ++var2) {
         BlockShapeAlgorithm var3;
         if ((var3 = algorithms[var1][var2]).isPhysical()) {
            ConvexHullShapeExt var11 = (ConvexHullShapeExt)var3.getShape();
            ConvexHullShapeExt var4 = (ConvexHullShapeExt)this.getShape();
            boolean var5 = true;

            for(int var6 = 0; var6 < var4.getNumPoints(); ++var6) {
               Vector3f var7 = new Vector3f();
               var4.getVertex(var6, var7);
               var7.z = -var7.z;
               boolean var8 = false;

               for(int var9 = 0; var9 < var11.getNumPoints(); ++var9) {
                  Vector3f var10 = new Vector3f();
                  var11.getVertex(var9, var10);
                  if (var10.equals(var7)) {
                     var8 = true;
                     break;
                  }
               }

               if (!var8) {
                  var5 = false;
                  break;
               }
            }

            if (var5) {
               return var2;
            }
         }
      }

      throw new IllegalArgumentException(this.toString());
   }

   public void createSmallShape() {
      if (this.isPhysical() && !(this instanceof Oriencube) && this.getShape() != null) {
         List var1 = this.getSigPoints();
         ObjectArrayList var2 = ((ConvexHullShapeExt)this.getShape()).getPoints();
         ObjectArrayList var3 = new ObjectArrayList();
         Iterator var11 = var2.iterator();

         while(true) {
            label70:
            while(var11.hasNext()) {
               Vector3f var4 = (Vector3f)var11.next();
               if (var1.contains(var4)) {
                  Vector3f var12;
                  (var12 = new Vector3f(var4)).scale(0.98F);
                  var3.add(var12);
               } else {
                  float var5 = 100000.0F;
                  it.unimi.dsi.fastutil.objects.ObjectArrayList var6 = new it.unimi.dsi.fastutil.objects.ObjectArrayList();
                  Iterator var7 = var1.iterator();

                  while(true) {
                     Vector3f var8;
                     Vector3f var9;
                     int var14;
                     float var15;
                     do {
                        if (!var7.hasNext()) {
                           assert var6.size() > 0 : var6.size() + " :::: " + var1.size();

                           Vector3f var13 = new Vector3f(var4);

                           for(var14 = 0; var14 < var6.size(); ++var14) {
                              (var9 = new Vector3f()).sub((Tuple3f)var6.get(var14), var4);
                              var9.normalize();
                              var9.scale(0.00999999F);
                              var13.add(var9);
                           }

                           var13.scale(0.98F);
                           var3.add(var13);
                           continue label70;
                        }

                        var8 = (Vector3f)var7.next();
                        (var9 = new Vector3f()).sub(var8, var4);
                     } while((var15 = var9.length()) > var5);

                     var6.add(var8);

                     for(var14 = 0; var14 < var6.size(); ++var14) {
                        Vector3f var10;
                        (var10 = new Vector3f()).sub((Tuple3f)var6.get(var14), var4);
                        if (var10.length() - 0.001F > var5) {
                           var6.remove(var14);
                           --var14;
                        }
                     }

                     var5 = var15;
                  }
               }
            }

            this.smallerShape = new ConvexHullShapeExt(var3);
            this.smallerShape.setMargin(-0.03F);
            return;
         }
      } else {
         assert !this.isPhysical() || this instanceof Oriencube || this.getShape() != null : this;

      }
   }

   private List getSigPoints() {
      it.unimi.dsi.fastutil.objects.ObjectArrayList var1 = new it.unimi.dsi.fastutil.objects.ObjectArrayList();
      ObjectArrayList var2;
      Iterator var3 = (var2 = ((ConvexHullShapeExt)this.getShape()).getPoints()).iterator();

      label59:
      while(var3.hasNext()) {
         Vector3f var4 = (Vector3f)var3.next();
         it.unimi.dsi.fastutil.objects.ObjectArrayList var5 = new it.unimi.dsi.fastutil.objects.ObjectArrayList();
         Iterator var6 = var2.iterator();

         while(true) {
            Vector3f var7;
            do {
               do {
                  if (!var6.hasNext()) {
                     if (var5.size() == 3) {
                        var1.add(var4);
                     }
                     continue label59;
                  }

                  var7 = (Vector3f)var6.next();
               } while(var4 == var7);
            } while((var4.x != var7.x || var4.y != var7.y || var4.z == var7.z) && (var4.x != var7.x || var4.y == var7.y || var4.z != var7.z) && (var4.x == var7.x || var4.y != var7.y || var4.z != var7.z));

            var5.add(var7);
         }
      }

      assert var1.size() > 0 : this;

      return var1;
   }

   public int findRot(BlockShapeAlgorithm[] var1, Matrix3f var2) {
      if (this.rotBuffer.containsKey(var2)) {
         return this.rotBuffer.get(var2);
      } else {
         for(int var3 = 0; var3 < var1.length; ++var3) {
            BlockShapeAlgorithm var4;
            if ((var4 = var1[var3]).isPhysical() && this != var4) {
               ConvexHullShapeExt var12 = (ConvexHullShapeExt)var4.getShape();
               ConvexHullShapeExt var5 = (ConvexHullShapeExt)this.getShape();
               boolean var6 = true;

               for(int var7 = 0; var7 < var5.getNumPoints(); ++var7) {
                  Vector3f var8 = new Vector3f();
                  var5.getVertex(var7, var8);
                  var2.transform(var8);
                  if (var8.x > 0.4F) {
                     var8.x = 0.5F;
                  } else if (var8.x < -0.4F) {
                     var8.x = -0.5F;
                  }

                  if (var8.y > 0.4F) {
                     var8.y = 0.5F;
                  } else if (var8.y < -0.4F) {
                     var8.y = -0.5F;
                  }

                  if (var8.z > 0.4F) {
                     var8.z = 0.5F;
                  } else if (var8.z < -0.4F) {
                     var8.z = -0.5F;
                  }

                  boolean var9 = false;

                  for(int var10 = 0; var10 < var12.getNumPoints(); ++var10) {
                     Vector3f var11 = new Vector3f();
                     var12.getVertex(var10, var11);
                     if (var11.equals(var8)) {
                        var9 = true;
                        break;
                     }
                  }

                  if (!var9) {
                     var6 = false;
                     break;
                  }
               }

               if (var6) {
                  this.rotBuffer.put(new Matrix3f(var2), var3);
                  return var3;
               }
            }
         }

         throw new IllegalArgumentException(this.toString());
      }
   }

   public boolean isPhysical() {
      return true;
   }

   public abstract void createSide(int var1, short var2, AlgorithmParameters var3);

   public int getSixthSideOrientation() {
      return -1;
   }

   public abstract boolean isAngled(int var1);

   public void create(BlockRenderInfo var1) {
      AlgorithmParameters var2 = var1.container.p;
      short var3;
      if (CubeMeshBufferContainer.isTriangle()) {
         short var4;
         for(var3 = 0; var3 < CubeMeshBufferContainer.VERTS_PER_FACE; ++var3) {
            var4 = vertexTriangleOrder[var3];
            this.createSide(var1.sideId, var4, var2);
            put(var1, var2.sid, var2.vID, var2.ext, var4, var2.normalMode, var2.insideMode, this.isAngled(var1.sideId));
         }

         if (var1.sideId == this.getSixthSideOrientation() && this.hasExtraSide() && var2.fresh) {
            for(var3 = 0; var3 < CubeMeshBufferContainer.VERTS_PER_FACE; ++var3) {
               var4 = vertexTriangleOrder[var3];
               this.createSide(6, var4, var2);

               assert var2.sid < 6;

               if (Occlusion.isNormnew()) {
                  var1.sideOccId = 24;
               }

               put(var1, var2.sid, var2.vID, var2.ext, var4, var2.normalMode, var2.insideMode, true);
            }

            var2.fresh = false;
            return;
         }
      } else {
         for(var3 = 0; var3 < CubeMeshBufferContainer.VERTS_PER_FACE; ++var3) {
            this.createSide(var1.sideId, var3, var2);
            put(var1, var2.sid, var2.vID, var2.ext, var3, var2.normalMode, var2.insideMode, this.isAngled(var1.sideId));
         }

         if (var1.sideId == this.getSixthSideOrientation() && this.hasExtraSide() && var2.fresh) {
            for(var3 = 0; var3 < CubeMeshBufferContainer.VERTS_PER_FACE; ++var3) {
               this.createSide(6, var3, var2);

               assert var2.sid < 6;

               if (Occlusion.isNormnew()) {
                  var1.sideOccId = 24;
               }

               put(var1, var2.sid, var2.vID, var2.ext, var3, var2.normalMode, var2.insideMode, true);
            }

            var2.fresh = false;
         }
      }

   }

   public boolean hasExtraSide() {
      return false;
   }

   public void single(BlockRenderInfo var1, byte var2, byte var3, byte var4, byte var5, FloatBuffer var6, AlgorithmParameters var7) {
      short var8;
      if (CubeMeshBufferContainer.isTriangle()) {
         short var9;
         for(var8 = 0; var8 < CubeMeshBufferContainer.VERTS_PER_FACE; ++var8) {
            var9 = vertexTriangleOrder[var8];
            this.createSide(var1.sideId, var9, var7);
            put(var1, var7.sid, var7.vID, var7.ext, var9, var2, var3, var4, var5, var7.normalMode, var7.insideMode, var6);
         }

         if (this.hasExtraSide() && var7.fresh) {
            for(var8 = 0; var8 < CubeMeshBufferContainer.VERTS_PER_FACE; ++var8) {
               var9 = vertexTriangleOrder[var8];
               this.createSide(6, var9, var7);

               assert var7.sid < 6;

               put(var1, var7.sid, var7.vID, var7.ext, var9, var2, var3, var4, var5, var7.normalMode, var7.insideMode, var6);
            }

            var7.fresh = false;
            return;
         }
      } else {
         for(var8 = 0; var8 < CubeMeshBufferContainer.VERTS_PER_FACE; ++var8) {
            this.createSide(var1.sideId, var8, var7);
            put(var1, var7.sid, var7.vID, var7.ext, var8, var2, var3, var4, var5, var7.normalMode, var7.insideMode, var6);
         }

         if (this.hasExtraSide() && var7.fresh) {
            for(var8 = 0; var8 < CubeMeshBufferContainer.VERTS_PER_FACE; ++var8) {
               this.createSide(6, var8, var7);

               assert var7.sid < 6;

               put(var1, var7.sid, var7.vID, var7.ext, var8, var2, var3, var4, var5, var7.normalMode, var7.insideMode, var6);
            }

            var7.fresh = false;
         }
      }

   }

   public void single(BlockRenderInfo var1, byte var2, byte var3, byte var4, byte var5, IntBuffer var6, AlgorithmParameters var7) {
      short var8;
      if (CubeMeshBufferContainer.isTriangle()) {
         short var9;
         for(var8 = 0; var8 < CubeMeshBufferContainer.VERTS_PER_FACE; ++var8) {
            var9 = vertexTriangleOrder[var8];
            this.createSide(var1.sideId, var9, var7);
            put(var1, var7.sid, var7.vID, var7.ext, var9, var2, var3, var4, var5, (byte)0, (byte)0, (byte)0, var7.normalMode, var7.insideMode, var6);
         }

         if (this.hasExtraSide() && var7.fresh) {
            for(var8 = 0; var8 < CubeMeshBufferContainer.VERTS_PER_FACE; ++var8) {
               var9 = vertexTriangleOrder[var8];
               this.createSide(6, var9, var7);

               assert var7.sid < 6;

               put(var1, var7.sid, var7.vID, var7.ext, var9, var2, var3, var4, var5, (byte)0, (byte)0, (byte)0, var7.normalMode, var7.insideMode, var6);
            }

            var7.fresh = false;
            return;
         }
      } else {
         for(var8 = 0; var8 < CubeMeshBufferContainer.VERTS_PER_FACE; ++var8) {
            this.createSide(var1.sideId, var8, var7);
            put(var1, var7.sid, var7.vID, var7.ext, var8, var2, var3, var4, var5, (byte)0, (byte)0, (byte)0, var7.normalMode, var7.insideMode, var6);
         }

         if (this.hasExtraSide() && var7.fresh) {
            for(var8 = 0; var8 < CubeMeshBufferContainer.VERTS_PER_FACE; ++var8) {
               this.createSide(6, var8, var7);

               assert var7.sid < 6;

               put(var1, var7.sid, var7.vID, var7.ext, var8, var2, var3, var4, var5, (byte)0, (byte)0, (byte)0, var7.normalMode, var7.insideMode, var6);
            }

            var7.fresh = false;
         }
      }

   }

   protected abstract ConvexShape getShape();

   public abstract int[] getSidesToCheckForVis();

   public abstract int[] getSidesAngled();

   public String toString() {
      return this.getClass().getSimpleName();
   }

   public Vector3f getShapeCenter(Vector3f var1) {
      var1.set(0.0F, 0.0F, 0.0F);
      if (this.getShape() instanceof ConvexHullShape) {
         Iterator var2 = ((ConvexHullShape)this.getShape()).getPoints().iterator();

         while(var2.hasNext()) {
            Vector3f var3 = (Vector3f)var2.next();
            var1.add(var3);
         }

         var1.scale(1.0F / (float)((ConvexHullShape)this.getShape()).getNumPoints());
      }

      return var1;
   }

   public boolean hasValidShape() {
      return true;
   }

   public static Vector3i[] getSideVerticesByNormal(int var0, int var1, BlockShapeAlgorithm var2) {
      if (var2 == null) {
         return var1 < 6 ? relativeBySide[var1] : null;
      } else {
         return var2.getSideByNormal(var0, var1);
      }
   }

   public static int getRepresentitiveNormal(int var0, int var1, BlockShapeAlgorithm var2) {
      if (var2 == null) {
         return var1 < 6 ? var1 : -1;
      } else {
         return var2.getRepresentitiveNormal(var0, var1);
      }
   }

   public static int getAngledSideLightRepresentitive(int var0, int var1, BlockShapeAlgorithm var2) {
      if (var2 == null) {
         return var1 < 6 ? var0 : -1;
      } else {
         return var2.getAngledSideLightRepresentitive(var0, var1);
      }
   }

   public int getAngledSideLightRepresentitive(int var1, int var2) {
      return var1;
   }

   public Vector3i[] getSideByNormal(int var1, int var2) {
      return relativeBySide[var2];
   }

   public int getRepresentitiveNormal(int var1, int var2) {
      return var2;
   }

   public static int getNormalBySide(int var0, BlockShapeAlgorithm var1) {
      return var1 == null ? var0 : var1.getNormalBySide(var0);
   }

   public int getNormalBySide(int var1) {
      return var1;
   }

   public void modAngledVertex(int var1) {
   }

   public int[] getSidesOpenToAir() {
      return openToAirNone;
   }

   public int[] getSidesToTestSpecial() {
      return openToAirNone;
   }

   public BlockShapeAlgorithm getSpriteAlgoRepresentitive() {
      return this;
   }

   static {
      configPath = DataUtil.dataPath + "config/vertexInfo.properties";
      orientcubes = new Oriencube[6][6];
      noSides = new int[0];
      oneSide = new int[1];
      relativeBySide = new Vector3i[][]{{new Vector3i(1, 1, 1), new Vector3i(-1, 1, 1), new Vector3i(-1, -1, 1), new Vector3i(1, -1, 1)}, {new Vector3i(1, -1, -1), new Vector3i(-1, -1, -1), new Vector3i(-1, 1, -1), new Vector3i(1, 1, -1)}, {new Vector3i(1, 1, -1), new Vector3i(-1, 1, -1), new Vector3i(-1, 1, 1), new Vector3i(1, 1, 1)}, {new Vector3i(1, -1, 1), new Vector3i(-1, -1, 1), new Vector3i(-1, -1, -1), new Vector3i(1, -1, -1)}, {new Vector3i(-1, -1, 1), new Vector3i(-1, -1, -1), new Vector3i(-1, 1, -1), new Vector3i(-1, 1, 1)}, {new Vector3i(1, 1, 1), new Vector3i(1, 1, -1), new Vector3i(1, -1, -1), new Vector3i(1, -1, 1)}};
      none = new Vector3i[0];
      openToAirNone = new int[0];
   }

   public static enum TexOrderStyle {
      NORMAL("./data/textures/texOrderNormal.config", BlockShapeAlgorithm.texOrderMapNormal),
      ORIENT("./data/textures/texOrderPointToOrientation.config", BlockShapeAlgorithm.texOrderMapPointToOrient),
      AREA4x4("./data/textures/texOrder4x4.config", BlockShapeAlgorithm.texOrderMap4x4);

      public final String path;
      public final byte[][][] map;

      private TexOrderStyle(String var3, byte[][][] var4) {
         this.path = var3;
         this.map = var4;
      }
   }
}
