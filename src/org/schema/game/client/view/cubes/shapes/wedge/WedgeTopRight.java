package org.schema.game.client.view.cubes.shapes.wedge;

import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.util.ObjectArrayList;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.shapes.AlgorithmParameters;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.common.data.physics.ConvexHullShapeExt;

@BlockShape(
   name = "WedgeTopRight"
)
public class WedgeTopRight extends WedgeShapeAlgorithm {
   private final int[] sidesAngled = new int[]{2};
   private final int[] sidesToTest = new int[]{3, 4};
   private ConvexHullShapeExt shape;
   private int[] openToAirNone = new int[]{1, 0};

   public WedgeTopRight() {
      ObjectArrayList var1;
      (var1 = new ObjectArrayList()).add(new Vector3f(-0.5F, -0.5F, -0.5F));
      var1.add(new Vector3f(-0.5F, -0.5F, 0.5F));
      var1.add(new Vector3f(0.5F, -0.5F, 0.5F));
      var1.add(new Vector3f(0.5F, -0.5F, -0.5F));
      var1.add(new Vector3f(0.5F, 0.5F, -0.5F));
      var1.add(new Vector3f(0.5F, 0.5F, 0.5F));
      this.shape = new ConvexHullShapeExt(var1);
   }

   public byte[] getWedgeOrientation() {
      return new byte[]{5, 2};
   }

   public Vector3i[] getAngledSideVerts() {
      return new Vector3i[]{new Vector3i(1, 1, -1), new Vector3i(-1, -1, -1), new Vector3i(-1, -1, 1), new Vector3i(1, 1, 1)};
   }

   public byte getWedgeGravityValidDir(byte var1) {
      if (var1 == 3) {
         return 5;
      } else {
         return (byte)(var1 == 5 ? 2 : -1);
      }
   }

   public void createSide(int var1, short var2, AlgorithmParameters var3) {
      var3.vID = var2;
      var3.sid = var1;
      var3.normalMode = 0;
      var3.ext = this.extOrderMap[var1][var2];
      switch(var1) {
      case 0:
         if (var3.vID == 1) {
            var3.vID = 2;
         }
         break;
      case 1:
         if (var3.vID == 2) {
            var3.vID = 1;
            return;
         }
         break;
      case 2:
         var3.normalMode = 408;
         if (var3.vID == 2) {
            var3.sid = 3;
            var3.vID = 1;
            return;
         }

         if (var3.vID == 1) {
            var3.sid = 3;
            var3.vID = 2;
            return;
         }
      case 3:
      default:
         break;
      case 4:
         return;
      case 5:
         var3.vID = 0;
         return;
      }

   }

   public int[] getSidesOpenToAir() {
      return this.openToAirNone;
   }

   protected ConvexShape getShape() {
      return this.shape;
   }

   public int[] getSidesToCheckForVis() {
      return this.sidesToTest;
   }

   public int[] getSidesAngled() {
      return this.sidesAngled;
   }
}
