package org.schema.game.client.view.cubes.shapes.orientcube.top;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.client.view.cubes.shapes.IconInterface;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.cubes.shapes.orientcube.bottom.OriencubeBottomLeft;

@BlockShape(
   name = "OriencubeTopLeft"
)
public class OriencubeTopLeft extends OrientCubeTop implements IconInterface {
   private static final Oriencube mirror = new OriencubeBottomLeft();

   public byte getOrientCubePrimaryOrientation() {
      return 2;
   }

   public byte getOrientCubeSecondaryOrientation() {
      return 5;
   }

   public Oriencube getMirrorAlgo() {
      return mirror;
   }

   public Transform getSecondaryTransform(Transform var1) {
      var1.setIdentity();
      var1.basis.rotY(1.5707964F);
      return var1;
   }
}
