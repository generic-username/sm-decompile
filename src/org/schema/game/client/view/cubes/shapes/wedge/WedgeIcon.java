package org.schema.game.client.view.cubes.shapes.wedge;

import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.util.ObjectArrayList;
import java.nio.FloatBuffer;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.shapes.AlgorithmParameters;
import org.schema.game.client.view.cubes.shapes.BlockRenderInfo;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.common.data.physics.ConvexHullShapeExt;

@BlockShape(
   name = "WedgeIcon"
)
public class WedgeIcon extends WedgeShapeAlgorithm {
   private ConvexHullShapeExt shape;

   public WedgeIcon() {
      ObjectArrayList var1;
      (var1 = new ObjectArrayList()).add(new Vector3f(-0.5F, -0.5F, -0.5F));
      var1.add(new Vector3f(-0.5F, -0.5F, 0.5F));
      var1.add(new Vector3f(0.5F, -0.5F, 0.5F));
      var1.add(new Vector3f(0.5F, -0.5F, -0.5F));
      var1.add(new Vector3f(-0.5F, 0.5F, 0.5F));
      var1.add(new Vector3f(0.5F, 0.5F, 0.5F));
      this.shape = new ConvexHullShapeExt(var1);
   }

   public void createSide(int var1, short var2, AlgorithmParameters var3) {
      var3.vID = var2;
      var3.sid = var1;
      var3.normalMode = 0;
      var3.ext = this.extOrderMap[var1][var2];
   }

   public void single(BlockRenderInfo var1, byte var2, byte var3, byte var4, byte var5, FloatBuffer var6, AlgorithmParameters var7) {
      for(short var8 = 0; var8 < 4; ++var8) {
         short var9;
         int var10;
         byte var11;
         label35: {
            var9 = var8;
            var10 = var1.sideId;
            var11 = (byte)var8;
            switch(var1.sideId) {
            case 1:
               break;
            case 2:
               if (var8 == 0) {
                  var10 = 3;
                  break;
               } else if (var8 == 1) {
                  var10 = 3;
                  var9 = 3;
               } else if (var8 != 2 && var8 == 3) {
               }
            case 3:
            default:
               break label35;
            case 4:
               if (var8 == 2) {
                  var9 = 1;
               }
               break label35;
            case 5:
               if (var8 != 1) {
                  break label35;
               }
            }

            var9 = 0;
         }

         put(var1, var10, var9, var11, var8, var2, var3, var4, var5, var7.normalMode, var7.insideMode, var6);
      }

   }

   protected ConvexShape getShape() {
      return this.shape;
   }

   public int[] getSidesToCheckForVis() {
      return null;
   }

   public int[] getSidesAngled() {
      return null;
   }

   public Vector3i[] getAngledSideVerts() {
      return null;
   }
}
