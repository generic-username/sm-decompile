package org.schema.game.client.view.cubes.shapes.orientcube.top;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.client.view.cubes.shapes.IconInterface;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.cubes.shapes.orientcube.bottom.OriencubeBottomRight;

@BlockShape(
   name = "OriencubeTopRight"
)
public class OriencubeTopRight extends OrientCubeTop implements IconInterface {
   private static final Oriencube mirror = new OriencubeBottomRight();

   public byte getOrientCubePrimaryOrientation() {
      return 2;
   }

   public byte getOrientCubeSecondaryOrientation() {
      return 4;
   }

   public Oriencube getMirrorAlgo() {
      return mirror;
   }

   public Transform getSecondaryTransform(Transform var1) {
      var1.setIdentity();
      var1.basis.rotY(4.712389F);
      return var1;
   }
}
