package org.schema.game.client.view.cubes.shapes.spike;

import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.util.ObjectArrayList;
import javax.vecmath.Vector3f;
import org.schema.game.client.view.cubes.shapes.AlgorithmParameters;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.common.data.physics.ConvexHullShapeExt;

@BlockShape(
   name = "SpikeIcon"
)
public class SpikeIcon extends SpikeShapeAlgorithm {
   private ConvexHullShapeExt shape;

   public SpikeIcon() {
      ObjectArrayList var1;
      (var1 = new ObjectArrayList()).add(new Vector3f(-0.5F, -0.5F, -0.5F));
      var1.add(new Vector3f(-0.5F, -0.5F, 0.5F));
      var1.add(new Vector3f(0.5F, -0.5F, 0.5F));
      var1.add(new Vector3f(0.5F, -0.5F, -0.5F));
      var1.add(new Vector3f(-0.5F, 0.5F, 0.5F));
      this.shape = new ConvexHullShapeExt(var1);
   }

   public void createSide(int var1, short var2, AlgorithmParameters var3) {
      var3.vID = var2;
      var3.sid = var1;
      var3.normalMode = 0;
      var3.ext = this.extOrderMap[var1][var2];
      switch(var1) {
      case 0:
         if (var3.vID == 0) {
            var3.vID = 1;
            return;
         }

         return;
      case 1:
         break;
      case 2:
         if (var3.vID == 0) {
            var3.sid = 3;
            break;
         } else {
            if (var3.vID == 1) {
               var3.sid = 3;
               var3.vID = 3;
               return;
            }

            if (var3.vID != 2 && var3.vID == 3) {
               var3.sid = 3;
               var3.vID = 1;
               return;
            }

            return;
         }
      case 3:
      default:
         return;
      case 4:
         var3.vID = 0;
         return;
      case 5:
         if (var3.vID != 1) {
            return;
         }
      }

      var3.vID = 0;
   }

   protected ConvexShape getShape() {
      return this.shape;
   }

   public int[] getSidesToCheckForVis() {
      return null;
   }

   public int[] getSidesAngled() {
      return null;
   }

   protected int[][] getNormals() {
      return null;
   }
}
