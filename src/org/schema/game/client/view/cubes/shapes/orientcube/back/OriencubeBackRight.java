package org.schema.game.client.view.cubes.shapes.orientcube.back;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.client.view.cubes.shapes.IconInterface;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.cubes.shapes.orientcube.front.OriencubeFrontRight;

@BlockShape(
   name = "OriencubeBackRight"
)
public class OriencubeBackRight extends OrientCubeBack implements IconInterface {
   private static final Oriencube mirror = new OriencubeFrontRight();

   public byte getOrientCubePrimaryOrientation() {
      return 1;
   }

   public byte getOrientCubeSecondaryOrientation() {
      return 4;
   }

   public Oriencube getMirrorAlgo() {
      return mirror;
   }

   public Transform getSecondaryTransform(Transform var1) {
      var1.setIdentity();
      var1.basis.rotY(4.712389F);
      return var1;
   }
}
