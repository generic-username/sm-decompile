package org.schema.game.client.view.cubes.shapes.orientcube.right;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.client.view.cubes.shapes.IconInterface;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.cubes.shapes.orientcube.left.OriencubeLeftFront;

@BlockShape(
   name = "OriencubeRightFront"
)
public class OriencubeRightFront extends OrientCubeRight implements IconInterface {
   private static final Oriencube mirror = new OriencubeLeftFront();

   public byte getOrientCubePrimaryOrientation() {
      return 4;
   }

   public byte getOrientCubeSecondaryOrientation() {
      return 0;
   }

   public Oriencube getMirrorAlgo() {
      return mirror;
   }

   public Transform getSecondaryTransform(Transform var1) {
      var1.setIdentity();
      return var1;
   }
}
