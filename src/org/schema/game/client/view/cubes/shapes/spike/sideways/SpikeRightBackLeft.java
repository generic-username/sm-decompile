package org.schema.game.client.view.cubes.shapes.spike.sideways;

import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.util.ObjectArrayList;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.shapes.AlgorithmParameters;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.client.view.cubes.shapes.spike.SpikeShapeAlgorithm;
import org.schema.game.common.data.physics.ConvexHullShapeExt;

@BlockShape(
   name = "SpikeRightBackLeft"
)
public class SpikeRightBackLeft extends SpikeShapeAlgorithm {
   private final int[] sidesToTest = new int[]{5};
   private ConvexHullShapeExt shape;
   private final int[] sidesAngled = new int[]{3, 1};

   public SpikeRightBackLeft() {
      ObjectArrayList var1;
      (var1 = new ObjectArrayList()).add(new Vector3f(-0.5F, -0.5F, 0.5F));
      var1.add(new Vector3f(-0.5F, -0.5F, -0.5F));
      var1.add(new Vector3f(-0.5F, 0.5F, -0.5F));
      var1.add(new Vector3f(-0.5F, 0.5F, 0.5F));
      var1.add(new Vector3f(0.5F, 0.5F, 0.5F));
      this.shape = new ConvexHullShapeExt(var1);
   }

   protected int[][] getNormals() {
      return new int[][]{{4, 3}, {4, 1}};
   }

   public Vector3i[][] getAngledSideVerts() {
      return new Vector3i[][]{{new Vector3i(1, 1, 1), new Vector3i(-1, -1, 1), new Vector3i(-1, -1, -1), new Vector3i(1, 1, -1)}, {new Vector3i(-1, 1, -1), new Vector3i(1, -1, 1), new Vector3i(1, 1, 1), new Vector3i(-1, -1, -1)}};
   }

   public void createSide(int var1, short var2, AlgorithmParameters var3) {
      var3.vID = var2;
      var3.sid = var1;
      var3.normalMode = 0;
      var3.ext = this.extOrderMap[var1][var2];
      switch(var1) {
      case 0:
         if (var3.vID == 3) {
            var3.vID = 1;
            return;
         }
         break;
      case 1:
         var3.normalMode = 336;
         if (var3.vID == 3) {
            var3.sid = 0;
            var3.vID = 0;
            return;
         }

         if (var3.vID == 0) {
            var3.sid = 0;
            var3.vID = 0;
         }
         break;
      case 2:
         if (var3.vID == 0) {
            var3.vID = 1;
            return;
         }
         break;
      case 3:
         var3.normalMode = 352;
         if (var3.vID == 0) {
            var3.sid = 2;
            var3.vID = 3;
            return;
         }

         if (var3.vID == 3) {
            var3.sid = 2;
            var3.vID = 3;
            return;
         }
         break;
      case 4:
         var3.vID = 0;
         return;
      case 5:
         return;
      }

   }

   protected ConvexShape getShape() {
      return this.shape;
   }

   public int[] getSidesToCheckForVis() {
      return this.sidesToTest;
   }

   public int[] getSidesAngled() {
      return this.sidesAngled;
   }
}
