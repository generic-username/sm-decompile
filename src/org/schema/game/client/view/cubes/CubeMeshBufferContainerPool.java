package org.schema.game.client.view.cubes;

import java.util.ArrayList;

public class CubeMeshBufferContainerPool {
   private static final int POOL_SIZE = 4;
   private static final ArrayList pool = new ArrayList(4);
   private static boolean initialized;

   public static CubeMeshBufferContainer get() {
      synchronized(pool) {
         while(pool.isEmpty()) {
            try {
               pool.wait();
            } catch (InterruptedException var2) {
               var2.printStackTrace();
            }
         }

         return (CubeMeshBufferContainer)pool.remove(0);
      }
   }

   private static void initialize() {
      if (!initialized) {
         for(int var0 = 0; var0 < 4; ++var0) {
            pool.add(CubeMeshBufferContainer.getInstance());
         }

         initialized = true;
      }

   }

   public static void release(CubeMeshBufferContainer var0) {
      assert !pool.contains(var0);

      synchronized(pool) {
         pool.add(var0);
         pool.notify();
      }

      assert pool.size() <= 4;

   }

   public static int size() {
      return pool.size();
   }

   static {
      initialize();
   }
}
