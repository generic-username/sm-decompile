package org.schema.game.client.view.cubes;

import java.nio.FloatBuffer;
import org.schema.game.common.data.world.DrawableRemoteSegment;

public interface CubeMeshInterface {
   int OPAQUE = 0;
   int BLENDED = 1;
   int BOTH = 2;

   void cleanUp();

   void contextSwitch(CubeMeshBufferContainer var1, int[][] var2, int[][] var3, int var4, int var5, DrawableRemoteSegment var6);

   void draw(int var1, int var2);

   void drawMesh(int var1, int var2);

   boolean isInitialized();

   void setInitialized(boolean var1);

   void prepare(FloatBuffer var1);

   void released();
}
