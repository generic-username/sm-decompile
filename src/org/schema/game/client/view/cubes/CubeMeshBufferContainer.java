package org.schema.game.client.view.cubes;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Random;
import org.schema.common.FastMath;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.game.client.view.cubes.shapes.AlgorithmParameters;
import org.schema.game.client.view.cubes.shapes.BlockRenderInfo;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.SegmentData;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;

public class CubeMeshBufferContainer {
   public BlockRenderInfo r = new BlockRenderInfo();
   public static final int vertexComponents;
   public static final int SQUARE_CORNERS = 4;
   private static final int[][] orientationMapping;
   private static final int[][] orientationMapping24;
   private static final int[] tDim;
   private static final int[][] orientationMappingThree;
   public static int DRAW_STYLE;
   private static ElementInformation cargoTextureBlockInBuildMode;
   public static final int VERTS_PER_FACE;
   public IntArrayList beacons;
   public CubeBuffer dataBuffer;
   public AlgorithmParameters p = new AlgorithmParameters();
   ByteBuffer b = null;
   int[] blendedElementBuffer;
   short[] blendedElementTypeBuffer;
   byte[] lightData;
   byte[] visData;
   public Random random = new Random();
   public long seed = 0L;
   public float[] lodData;
   public short[] lodTypeAndOrientcubeIndex;
   public IntArrayList lodShapes;
   public static final int SIDES = 7;
   public static final int VERTICES_PER_SIDE = 4;
   public static final int COLOR_COORDS_PER_VERT = 7;

   public CubeMeshBufferContainer() {
      this.lodData = new float['耀' * SegmentData.lodDataSize];
      this.lodTypeAndOrientcubeIndex = new short[65536];
      this.lodShapes = new IntArrayList(32768);
   }

   public static int getLightInfoIndexFromIndex(int var0) {
      return var0 * 7 * 28;
   }

   public static int getLightInfoIndex(int var0, int var1, int var2) {
      return ((var2 << 10) + (var1 << 5) + var0) * 7 * 28;
   }

   public static int getLightInfoIndex(Vector3b var0) {
      return getLightInfoIndex(var0.x, var0.y, var0.z);
   }

   public static byte getOrientationCode3(int var0, int var1) {
      return (byte)(5 - orientationMappingThree[var1][var0]);
   }

   public static byte getOrientationCode6(int var0, int var1) {
      return (byte)(5 - orientationMapping[var1][var0]);
   }

   public static byte getOrientationCode24(int var0, int var1) {
      return (byte)(5 - orientationMapping24[var1][var0]);
   }

   public static final int putIndex(CubeMeshBufferContainer var0, int var1, int var2, SegmentData var3, int var4, int var5, short var6) {
      int var7 = getLightInfoIndexFromIndex(var2);
      ElementInformation var25;
      int var8 = (var25 = ElementKeyMap.getInfo(var6)).getIndividualSides();
      boolean var9 = var25.isAnimated();
      BlockStyle var10 = var25.getBlockStyle();
      byte var11 = var3.getOrientation(var2);
      boolean var12 = var3.isActive(var2);
      int var13 = 0;
      int var14 = 0;
      if (var25.resourceInjection != ElementInformation.ResourceInjectionType.OFF) {
         if (ElementKeyMap.orientationToResOverlayMapping[var11] > 0) {
            var13 = var25.resourceInjection.index + ElementKeyMap.orientationToResOverlayMapping[var11] - 1;
         }

         var11 = 0;
      }

      int var15 = var25.getSlab();
      boolean var16 = var25.isDrawnOnlyInBuildMode();
      boolean var17 = var25.isExtendedTexture();
      if (var25.getId() == 689) {
         if (var11 == 4) {
            var11 = 2;
            var16 = true;
            if (cargoTextureBlockInBuildMode == null && (cargoTextureBlockInBuildMode = ElementKeyMap.getInfoFast((Integer)EngineSettings.BLOCK_ID_OF_CARGO_SPACE_BUILD_MODE.getCurrentState())) == null) {
               try {
                  throw new Exception("Texture for cargo area could not be set. Id " + (Integer)EngineSettings.BLOCK_ID_OF_CARGO_SPACE_BUILD_MODE.getCurrentState() + " invalid. check BLOCK_ID_OF_CARGO_SPACE_IN_BUILD_MODE in settings.cfg");
               } catch (Exception var21) {
                  var21.printStackTrace();
                  cargoTextureBlockInBuildMode = ElementKeyMap.getInfo((short)411);
               }
            }

            var25 = cargoTextureBlockInBuildMode;
         } else {
            var15 = var11;
            if (var5 != 2 && var5 != 3) {
               var14 = var11;
            }

            var0.random.setSeed(var0.seed * (long)var2);
            var14 += var0.random.nextInt(5) << 2;
            var11 = 2;
         }
      }

      byte var18 = 0;
      if (var10 == BlockStyle.NORMAL24) {
         var18 = getOrientationCode24(var5, var11 / 4);
      } else if (var8 == 6) {
         var11 = (byte)Math.max(0, Math.min(5, var11));

         assert var11 < 6 : "Orientation wrong: " + var11;

         var18 = getOrientationCode6(var5, var11);
      } else if (var8 == 3) {
         var11 = (byte)Math.max(0, Math.min(5, var11));

         assert var11 < 6 : "Orientation wrong: " + var11;

         var18 = getOrientationCode3(var5, var11);
      }

      float var19 = (float)var3.getHitpointsByte(var2) * var25.getMaxHitPointsOneDivByByte();
      byte var20 = 0;
      if (var19 < 1.0F) {
         var20 = FastMath.clamp((byte)((int)((1.0F - var19) * 7.0F)), (byte)0, (byte)7);
      }

      byte var22 = var0.getVisFromDataIndex(var2);
      BlockShapeAlgorithm var29 = null;
      if (var10 != BlockStyle.NORMAL) {
         var29 = BlockShapeAlgorithm.algorithms[var10.id - 1][var11];
         if (var25.hasLod() && var25.lodShapeStyle == 1) {
            var29 = var29.getSpriteAlgoRepresentitive();
            var22 = Element.FULLVIS;
         }
      }

      if ((var22 & var4) != var4) {
         return 0;
      } else {
         var2 = var5 << 2;
         byte var24 = 0;
         if (var25.hasLod() || var9 && (var8 != 3 || var5 != 2 && var5 != 3)) {
            var24 = 1;
         }

         byte var26 = var25.getTextureLayer(var12, var18);
         short var27 = (short)(var25.getTextureIndexLocal(var12, var18) + var14);
         if (var25.isReactorChamberSpecific()) {
            ++var27;
         }

         var14 = ByteUtil.modU256(ByteUtil.divUSeg(var3.getSegment().pos.x) + 128);
         int var30 = ByteUtil.modU256(ByteUtil.divUSeg(var3.getSegment().pos.y) + 128);
         float var23 = (float)((ByteUtil.modU256(ByteUtil.divUSeg(var3.getSegment().pos.z) + 128) << 16) + (var30 << 8) + var14);
         BlockRenderInfo var28;
         (var28 = var0.r).sideId = var5;
         var28.layer = var26;
         var28.typeCode = var27;
         var28.hitPointsCode = var20;
         var28.animatedCode = var24;
         var28.lightIndex = var7;
         var28.sideOccId = var2;
         var28.index = var1;
         var28.segIndex = var23;
         var28.orientation = var11;
         var28.halvedFactor = var15;
         var28.blockStyle = var10;
         var28.container = var0;
         var28.resOverlay = var13;
         var28.onlyInBuildMode = var16;
         var28.extendedBlockTexture = var17;
         var28.threeSided = var8 == 3;
         var28.pointToOrientation = var25.sideTexturesPointToOrientation ? BlockShapeAlgorithm.TexOrderStyle.ORIENT : (var25.extendedTexture ? BlockShapeAlgorithm.TexOrderStyle.AREA4x4 : BlockShapeAlgorithm.TexOrderStyle.NORMAL);
         if (var10 != BlockStyle.NORMAL) {
            var29.create(var28);
         } else {
            BlockShapeAlgorithm.normalBlock(var28);
         }

         return VERTS_PER_FACE;
      }
   }

   public static boolean isTriangle() {
      return DRAW_STYLE == 4;
   }

   public static CubeMeshBufferContainer getInstance() {
      CubeMeshBufferContainer var0;
      (var0 = new CubeMeshBufferContainer()).generate();
      return var0;
   }

   public static void main(String[] var0) {
   }

   public void generate() {
      this.blendedElementBuffer = new int['耀'];
      this.blendedElementTypeBuffer = new short['耀'];
      this.lightData = new byte[6422528];
      this.visData = new byte['耀'];
      this.beacons = new IntArrayList(32768);
      if (GraphicsContext.INTEGER_VERTICES) {
         this.dataBuffer = new CubeBufferInt();
      } else {
         this.dataBuffer = new CubeBufferFloat();
      }

      this.dataBuffer.clearBuffers();
   }

   public byte getFinalLight(int var1, int var2, int var3) {
      return this.lightData[var1 + var2 * 7 + var3];
   }

   public byte getVis(int var1) {
      return this.visData[var1];
   }

   public byte getVis(int var1, int var2, int var3) {
      var1 = var1 + (var2 << 5) + (var3 << 10);
      return this.getVis(var1);
   }

   public byte getVis(Vector3b var1) {
      return this.getVis(var1.x, var1.y, var1.z);
   }

   public byte getVisFromDataIndex(int var1) {
      return this.visData[var1];
   }

   public void reset() {
      Arrays.fill(this.visData, (byte)0);
      Arrays.fill(this.lightData, (byte)0);
   }

   public void setFinalLight(int var1, byte var2, int var3, int var4) {
      this.lightData[var1 + var3 * 7 + var4] = var2;
   }

   public void setVis(byte var1, byte var2, byte var3, byte var4) {
      int var5 = var1 + (var2 << 5) + (var3 << 10);
      this.setVis(var5, var4);

      assert this.getVis(var1, var2, var3) == var4;

   }

   public void setVis(int var1, byte var2) {
      this.visData[var1] = var2;
   }

   public void setVis(Vector3b var1, byte var2) {
      this.setVis(var1.x, var1.y, var1.z, var2);
   }

   static {
      vertexComponents = ShaderLibrary.CUBE_VERTEX_COMPONENTS;
      orientationMapping = new int[][]{{5, 4, 3, 2, 0, 1}, {4, 5, 3, 2, 1, 0}, {1, 0, 5, 4, 2, 3}, {0, 1, 4, 5, 3, 2}, {1, 0, 3, 2, 4, 5}, {0, 1, 3, 2, 5, 4}};
      orientationMapping24 = new int[][]{{5, 4, 2, 3, 0, 1}, {4, 5, 2, 3, 1, 0}, {0, 1, 4, 5, 3, 2}, {1, 0, 5, 4, 2, 3}, {1, 0, 3, 2, 4, 5}, {0, 1, 2, 3, 5, 4}};
      tDim = new int[]{0, 0, 3, 2, 0, 0};
      orientationMappingThree = new int[][]{tDim, tDim, tDim, tDim, tDim, tDim};
      DRAW_STYLE = 4;
      VERTS_PER_FACE = isTriangle() ? 6 : 4;
   }
}
