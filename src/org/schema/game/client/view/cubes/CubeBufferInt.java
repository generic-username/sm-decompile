package org.schema.game.client.view.cubes;

import java.nio.Buffer;
import java.nio.IntBuffer;
import org.lwjgl.BufferUtils;

public class CubeBufferInt implements CubeBuffer {
   public final IntBuffer frontBuffer;
   public final IntBuffer backBuffer;
   public final IntBuffer bottomBuffer;
   public final IntBuffer topBuffer;
   public final IntBuffer rightBuffer;
   public final IntBuffer leftBuffer;
   public final IntBuffer angledBuffer;
   public final IntBuffer[] buffers;
   public final IntBuffer totalBuffer;

   public CubeBufferInt() {
      int var1 = 1572864 * CubeMeshBufferContainer.vertexComponents / 6;
      this.frontBuffer = BufferUtils.createIntBuffer(var1);
      this.backBuffer = BufferUtils.createIntBuffer(var1);
      this.bottomBuffer = BufferUtils.createIntBuffer(var1);
      this.topBuffer = BufferUtils.createIntBuffer(var1);
      this.rightBuffer = BufferUtils.createIntBuffer(var1);
      this.leftBuffer = BufferUtils.createIntBuffer(var1);
      this.angledBuffer = BufferUtils.createIntBuffer(var1 << 2);
      this.buffers = new IntBuffer[]{this.frontBuffer, this.backBuffer, this.bottomBuffer, this.topBuffer, this.rightBuffer, this.leftBuffer, this.angledBuffer};
      this.totalBuffer = BufferUtils.createIntBuffer(1572864 * CubeMeshBufferContainer.vertexComponents);
   }

   public void makeStructured(int[][] var1, int[][] var2) {
      this.totalBuffer.clear();
      this.flipBuffers();

      int var3;
      IntBuffer var4;
      int var5;
      for(var3 = 0; var3 < this.buffers.length; ++var3) {
         var4 = this.buffers[var3];
         var5 = var1[var3][1];
         var4.limit(var5);
         var4.position(0);
         this.totalBuffer.put(var4);
      }

      for(var3 = 0; var3 < this.buffers.length; ++var3) {
         var4 = this.buffers[var3];
         int var6 = (var5 = var1[var3][1]) + var2[var3][1];
         var4.limit(var6);
         var4.position(var5);
         this.totalBuffer.put(var4);
      }

   }

   public void make() {
      this.totalBuffer.clear();
      this.flipBuffers();

      for(int var1 = 0; var1 < this.buffers.length; ++var1) {
         IntBuffer var2 = this.buffers[var1];
         this.totalBuffer.put(var2);
      }

   }

   public void rewindBuffers() {
      IntBuffer[] var1;
      int var2 = (var1 = this.buffers).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         var1[var3].rewind();
      }

   }

   public void flipBuffers() {
      IntBuffer[] var1;
      int var2 = (var1 = this.buffers).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         var1[var3].flip();
      }

   }

   public void resetBuffers() {
      IntBuffer[] var1;
      int var2 = (var1 = this.buffers).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         var1[var3].reset();
      }

   }

   public void clearBuffers() {
      for(int var1 = 0; var1 < this.buffers.length; ++var1) {
         this.buffers[var1].clear();
      }

   }

   public int limitBuffers() {
      int var1 = 0;
      IntBuffer[] var2;
      int var3 = (var2 = this.buffers).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         IntBuffer var5 = var2[var4];
         var1 += var5.limit();
      }

      return var1;
   }

   public int totalPosition() {
      int var1 = 0;
      IntBuffer[] var2;
      int var3 = (var2 = this.buffers).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         IntBuffer var5 = var2[var4];
         var1 += var5.position();
      }

      return var1;
   }

   public void createOpaqueSizes(int[][] var1) {
      int var2 = 0;

      for(int var3 = 0; var3 < this.buffers.length; ++var3) {
         int var4 = this.buffers[var3].position();
         var1[var3][0] = var2;
         var1[var3][1] = var4;
         var2 += var4;
      }

   }

   public void createBlendedRanges(int[][] var1, int[][] var2) {
      int var3 = 0;

      for(int var4 = 0; var4 < this.buffers.length; ++var4) {
         int var5 = this.buffers[var4].position();
         int var6 = var1[var4][1];
         var5 -= var6;
         var2[var4][0] = var3;
         var2[var4][1] = var5;
         var3 += var5;
      }

   }

   public Buffer getTotalBuffer() {
      return this.totalBuffer;
   }
}
