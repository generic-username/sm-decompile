package org.schema.game.client.view.cubes.noise;

import org.schema.common.FastMath;

public class Perlin {
   static final int[] p = new int[512];
   static final int[] permutation = new int[]{151, 160, 137, 91, 90, 15, 131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23, 190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33, 88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166, 77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244, 102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196, 135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123, 5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42, 223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9, 129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228, 251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107, 49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254, 138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180};

   static float fade(float var0) {
      return var0 * var0 * var0 * (var0 * (var0 * 6.0F - 15.0F) + 10.0F);
   }

   static float grad(int var0, float var1, float var2, float var3) {
      float var4 = (var0 &= 15) < 8 ? var1 : var2;
      var1 = var0 < 4 ? var2 : (var0 != 12 && var0 != 14 ? var3 : var1);
      return ((var0 & 1) == 0 ? var4 : -var4) + ((var0 & 2) == 0 ? var1 : -var1);
   }

   static float lerp(float var0, float var1, float var2) {
      return var1 + var0 * (var2 - var1);
   }

   public static float noise(float var0, float var1, float var2) {
      int var3 = FastMath.fastFloor(var0);
      int var4 = FastMath.fastFloor(var1);
      int var5 = FastMath.fastFloor(var2);
      int var6 = var3 & 255;
      int var7 = var4 & 255;
      int var8 = var5 & 255;
      var0 -= (float)var3;
      var1 -= (float)var4;
      var2 -= (float)var5;
      float var11 = fade(var0);
      float var12 = fade(var1);
      float var13 = fade(var2);
      int var9 = p[var6] + var7;
      int var10 = p[var9] + var8;
      var9 = p[var9 + 1] + var8;
      var6 = p[var6 + 1] + var7;
      var7 = p[var6] + var8;
      var6 = p[var6 + 1] + var8;
      return lerp(var13, lerp(var12, lerp(var11, grad(p[var10], var0, var1, var2), grad(p[var7], var0 - 1.0F, var1, var2)), lerp(var11, grad(p[var9], var0, var1 - 1.0F, var2), grad(p[var6], var0 - 1.0F, var1 - 1.0F, var2))), lerp(var12, lerp(var11, grad(p[var10 + 1], var0, var1, var2 - 1.0F), grad(p[var7 + 1], var0 - 1.0F, var1, var2 - 1.0F)), lerp(var11, grad(p[var9 + 1], var0, var1 - 1.0F, var2 - 1.0F), grad(p[var6 + 1], var0 - 1.0F, var1 - 1.0F, var2 - 1.0F))));
   }

   static {
      for(int var0 = 0; var0 < 256; ++var0) {
         p[var0 + 256] = p[var0] = permutation[var0];
      }

   }
}
