package org.schema.game.client.view.cubes.cubedyn;

import it.unimi.dsi.fastutil.objects.ObjectAVLTreeSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectBidirectionalIterator;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import org.lwjgl.opengl.ARBMapBufferRange;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.cubes.CubeBufferFloat;
import org.schema.game.client.view.cubes.CubeMeshBufferContainer;
import org.schema.game.client.view.cubes.CubeMeshInterface;
import org.schema.game.client.view.cubes.CubeMeshNormal;
import org.schema.game.common.data.world.DrawableRemoteSegment;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

@Deprecated
public class CubeMeshManager {
   private static final boolean DEBUG = false;
   public static int MAX_BYTES;
   public final ObjectArrayList vboSegs = new ObjectArrayList();

   public CubeMeshManager() {
      System.err.println("[GRAPHICS] TRY USING VBO MAPPED BUFFER: " + EngineSettings.G_USE_VBO_MAP.isOn());
   }

   public CubeMeshManager.VBOCell getFreeSegment(int var1, CubeMeshManager.CubeMeshDyn var2) {
      CubeMeshManager.VBOCell var3 = null;

      for(int var4 = 0; var4 < this.vboSegs.size(); ++var4) {
         if ((var3 = ((CubeMeshManager.VBOSeg)this.vboSegs.get(var4)).getFree(var1, var2)) != null) {
            return var3;
         }
      }

      if (var3 == null) {
         CubeMeshManager.VBOSeg var5;
         (var5 = new CubeMeshManager.VBOSeg()).init();
         this.vboSegs.add(var5);
         var3 = var5.getFree(var1, var2);
      }

      assert var3 != null;

      return var3;
   }

   public CubeMeshInterface getInstance() {
      return new CubeMeshManager.CubeMeshDyn();
   }

   static {
      MAX_BYTES = (Integer)EngineSettings.G_VBO_BULKMODE_SIZE.getCurrentState() << 10 << 10;
   }

   public class CubeMeshDyn implements CubeMeshInterface {
      private boolean initialized;
      private CubeMeshManager.VBOCell currentVBOSeg;

      public void cleanUp() {
      }

      public void contextSwitch(CubeMeshBufferContainer var1, int[][] var2, int[][] var3, int var4, int var5, DrawableRemoteSegment var6) {
         if (!CubeMeshNormal.checkedForRangeMap) {
            CubeMeshNormal.setGl_ARB_map_buffer_range(GraphicsContext.getCapabilities().GL_ARB_map_buffer_range);
            System.err.println("USE BUFFER RANGE: " + CubeMeshNormal.isGl_ARB_map_buffer_range());
            CubeMeshNormal.checkedForRangeMap = true;
         }

         boolean var19 = false;
         var1.dataBuffer.make();
         var1.dataBuffer.getTotalBuffer().flip();
         if (!this.initialized) {
            this.prepare((FloatBuffer)null);
            var19 = true;
         }

         if (var1.dataBuffer.getTotalBuffer().limit() != 0) {
            long var7 = 0L;
            long var9 = 0L;
            long var11 = 0L;
            int var20 = var1.dataBuffer.getTotalBuffer().limit() << 2;
            if (this.currentVBOSeg != null) {
               this.currentVBOSeg.free = true;
            }

            if (this.currentVBOSeg == null || !this.currentVBOSeg.fits(var20, this)) {
               this.currentVBOSeg = CubeMeshManager.this.getFreeSegment(var20, this);
            }

            assert this.currentVBOSeg.bufferId != 0;

            GlUtil.glBindBuffer(34962, this.currentVBOSeg.bufferId);
            long var13 = System.nanoTime();
            if (CubeMeshNormal.USE_MAP_BUFFER) {
               if (CubeMeshNormal.isGl_ARB_map_buffer_range()) {
                  CubeMeshNormal.oldHelpBuffer = ARBMapBufferRange.glMapBufferRange(34962, (long)this.currentVBOSeg.startPositionByte, (long)this.currentVBOSeg.endPositionByte, CubeMeshNormal.mappingByte, CubeMeshNormal.oldHelpBuffer == null ? null : CubeMeshNormal.oldHelpBuffer);

                  assert this.currentVBOSeg.startPositionByte + (var1.dataBuffer.getTotalBuffer().limit() << 2) < CubeMeshManager.MAX_BYTES;
               } else {
                  CubeMeshNormal.oldHelpBuffer = GL15.glMapBuffer(34962, 35001, CubeMeshNormal.oldHelpBuffer == null ? null : CubeMeshNormal.oldHelpBuffer);
               }

               if (CubeMeshNormal.oldHelpBuffer == null && CubeMeshNormal.USE_MAP_BUFFER) {
                  EngineSettings.G_USE_VBO_MAP.setCurrentState(false);
                  CubeMeshNormal.USE_MAP_BUFFER = false;
                  System.err.println("[Exception]WARNING: MAPPED BUFFER HAS BEEN TURNED OFF " + GlUtil.getGlError());
               }
            }

            long var15 = System.nanoTime() - var13;
            boolean var21 = false;
            if (CubeMeshNormal.USE_MAP_BUFFER) {
               long var17 = System.nanoTime();
               FloatBuffer var22 = CubeMeshNormal.oldHelpBuffer.order(ByteOrder.nativeOrder()).asFloatBuffer();
               var9 = (System.nanoTime() - var17) / 1000000L;
               var17 = System.nanoTime();
               if (CubeMeshNormal.isGl_ARB_map_buffer_range()) {
                  var22.put(((CubeBufferFloat)var1.dataBuffer).totalBuffer);
               } else {
                  var22.position(this.currentVBOSeg.startPositionByte / 4);
                  var22.put(((CubeBufferFloat)var1.dataBuffer).totalBuffer);
               }

               var7 = (System.nanoTime() - var17) / 1000000L;
               var17 = System.nanoTime();
               var21 = GL15.glUnmapBuffer(34962);
               var11 = (System.nanoTime() - var17) / 1000000L;
            } else {
               GL15.glBufferSubData(34962, (long)this.currentVBOSeg.startPositionByte, ((CubeBufferFloat)var1.dataBuffer).totalBuffer);
            }

            if ((CubeMeshNormal.bufferContextSwitchTime = (CubeMeshNormal.bufferContextSwitchTime = System.nanoTime() - var13) / 1000000L) > 10L) {
               System.err.println("[CUBE] WARNING: context switch time: " + CubeMeshNormal.bufferContextSwitchTime + " ms : " + var15 / 1000000L + "ms: O " + var9 + "; P " + var7 + "; U " + var11 + "::; map " + CubeMeshNormal.USE_MAP_BUFFER + "; range " + CubeMeshNormal.isGl_ARB_map_buffer_range() + "; init " + var19 + "  unmap " + var21);
            }

            assert var1.dataBuffer.getTotalBuffer().limit() / CubeMeshBufferContainer.vertexComponents == this.currentVBOSeg.size() / 4 / CubeMeshBufferContainer.vertexComponents : var1.dataBuffer.getTotalBuffer().limit() / CubeMeshBufferContainer.vertexComponents + "; " + this.currentVBOSeg.size() / 4 / CubeMeshBufferContainer.vertexComponents;

            this.currentVBOSeg.blendBufferPos = var4;
         } else {
            if (this.currentVBOSeg != null) {
               this.currentVBOSeg.free = true;
               this.currentVBOSeg = null;
            }

         }
      }

      public void draw(int var1, int var2) {
         this.drawMesh(var1, var2);
      }

      public void drawMesh(int var1, int var2) {
         if (this.currentVBOSeg != null) {
            assert this.currentVBOSeg.bufferId != 0;

            if (GlUtil.glBindBuffer(34962, this.currentVBOSeg.bufferId)) {
               GL11.glVertexPointer(CubeMeshBufferContainer.vertexComponents, 5126, 0, 0L);
            }

            if (CubeMeshBufferContainer.isTriangle()) {
               if (var1 == 2) {
                  GL11.glDrawArrays(4, this.currentVBOSeg.startPositionByte / 4 / CubeMeshBufferContainer.vertexComponents, this.currentVBOSeg.size() / 4 / CubeMeshBufferContainer.vertexComponents);
                  return;
               }

               if (var1 == 0) {
                  GL11.glDrawArrays(4, this.currentVBOSeg.startPositionByte / 4 / CubeMeshBufferContainer.vertexComponents, this.currentVBOSeg.blendBufferPos / CubeMeshBufferContainer.vertexComponents);
                  return;
               }

               if (var1 == 1) {
                  GL11.glDrawArrays(4, this.currentVBOSeg.startPositionByte / 4 / CubeMeshBufferContainer.vertexComponents + this.currentVBOSeg.blendBufferPos / CubeMeshBufferContainer.vertexComponents, (this.currentVBOSeg.size() / 4 - this.currentVBOSeg.blendBufferPos) / CubeMeshBufferContainer.vertexComponents);
                  return;
               }
            } else {
               if (var1 == 2) {
                  GL11.glDrawArrays(7, this.currentVBOSeg.startPositionByte / 4 / CubeMeshBufferContainer.vertexComponents, this.currentVBOSeg.size() / 4 / CubeMeshBufferContainer.vertexComponents);
                  return;
               }

               if (var1 == 0) {
                  GL11.glDrawArrays(7, this.currentVBOSeg.startPositionByte / 4 / CubeMeshBufferContainer.vertexComponents, this.currentVBOSeg.blendBufferPos / CubeMeshBufferContainer.vertexComponents);
                  return;
               }

               if (var1 == 1) {
                  GL11.glDrawArrays(7, this.currentVBOSeg.startPositionByte / 4 / CubeMeshBufferContainer.vertexComponents + this.currentVBOSeg.blendBufferPos / CubeMeshBufferContainer.vertexComponents, (this.currentVBOSeg.size() / 4 - this.currentVBOSeg.blendBufferPos) / CubeMeshBufferContainer.vertexComponents);
               }
            }

         }
      }

      public boolean isInitialized() {
         return this.initialized;
      }

      public void setInitialized(boolean var1) {
         this.initialized = var1;
      }

      public void prepare(FloatBuffer var1) {
         this.initialized = true;
      }

      public void released() {
         if (this.currentVBOSeg != null) {
            this.currentVBOSeg.free = true;
            this.currentVBOSeg = null;
         }

      }
   }

   class VBOSeg {
      private final ObjectAVLTreeSet cells;
      int bufferId;
      int currentSize;

      private VBOSeg() {
         this.cells = new ObjectAVLTreeSet();
      }

      public void init() {
         if (this.bufferId == 0) {
            this.createBuffer();
         }

      }

      public void createBuffer() {
         this.bufferId = GL15.glGenBuffers();
         ++CubeMeshNormal.initializedBuffers;
         Controller.loadedVBOBuffers.add(this.bufferId);
         GlUtil.glBindBuffer(34962, this.bufferId);
         GL15.glBufferData(34962, (long)CubeMeshManager.MAX_BYTES, CubeMeshNormal.BUFFER_FLAG);
         GameClientState.realVBOSize += CubeMeshManager.MAX_BYTES;
         GameClientState.prospectedVBOSize += 1572864 * CubeMeshBufferContainer.vertexComponents << 2;
         GlUtil.glBindBuffer(34962, 0);
      }

      public CubeMeshManager.VBOCell getFree(int var1, CubeMeshManager.CubeMeshDyn var2) {
         ObjectBidirectionalIterator var3 = this.cells.iterator();
         int var4 = 0;

         CubeMeshManager.VBOCell var5;
         do {
            if (!var3.hasNext()) {
               if (this.currentSize + var1 < CubeMeshManager.MAX_BYTES) {
                  var5 = CubeMeshManager.this.new VBOCell(this.currentSize, this.currentSize + var1, this.bufferId);
                  this.currentSize += var1;
                  boolean var6 = var5.fits(var1, var2);
                  this.cells.add(var5);

                  assert var6;

                  assert !var5.free;

                  return var5;
               }

               return null;
            }

            var5 = (CubeMeshManager.VBOCell)var3.next();

            assert var5.initialStart == var4;

            var4 = var5.initialEnd;
         } while(!var5.fits(var1, var2));

         return var5;
      }

      // $FF: synthetic method
      VBOSeg(Object var2) {
         this();
      }
   }

   class VBOCell implements Comparable {
      final int initialStart;
      final int initialEnd;
      final int bufferId;
      public int blendBufferPos;
      boolean free = true;
      int startPositionByte;
      int endPositionByte;

      public VBOCell(int var2, int var3, int var4) {
         this.initialStart = var2;
         this.initialEnd = var3;
         this.bufferId = var4;
      }

      public boolean fits(int var1, CubeMeshManager.CubeMeshDyn var2) {
         if (this.free && var1 <= this.sizeInitial()) {
            this.startPositionByte = this.initialStart;
            this.endPositionByte = this.initialStart + var1;

            assert this.endPositionByte <= this.initialEnd;

            this.free = false;
            return true;
         } else {
            return false;
         }
      }

      private int sizeInitial() {
         return this.initialEnd - this.initialStart;
      }

      public int compareTo(CubeMeshManager.VBOCell var1) {
         return this.initialStart - var1.initialStart;
      }

      public int size() {
         return this.endPositionByte - this.startPositionByte;
      }

      public String toString() {
         return "VBOCell [initialStart=" + this.initialStart + ", initialEnd=" + this.initialEnd + ", bufferId=" + this.bufferId + ", free=" + this.free + ", startPositionByte=" + this.startPositionByte + ", endPositionByte=" + this.endPositionByte + "]";
      }
   }
}
