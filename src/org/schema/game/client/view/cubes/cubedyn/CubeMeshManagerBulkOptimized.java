package org.schema.game.client.view.cubes.cubedyn;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectAVLTreeSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectBidirectionalIterator;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL15;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.cubes.CubeBufferFloat;
import org.schema.game.client.view.cubes.CubeBufferInt;
import org.schema.game.client.view.cubes.CubeMeshBufferContainer;
import org.schema.game.client.view.cubes.CubeMeshInterface;
import org.schema.game.client.view.cubes.CubeMeshNormal;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.world.DrawableRemoteSegment;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;

public class CubeMeshManagerBulkOptimized {
   private static final boolean DEBUG = false;
   public static int MAX_BYTES;
   private static ObjectArrayList markerPool;
   public final ObjectArrayList vboSegs = new ObjectArrayList();
   public final Int2ObjectOpenHashMap reservedVBOSegs = new Int2ObjectOpenHashMap();

   public CubeMeshManagerBulkOptimized() {
      System.err.println("[GRAPHICS] TRY USING VBO MAPPED BUFFER: " + EngineSettings.G_USE_VBO_MAP.isOn());
   }

   private static DrawMarker getMarker() {
      return !markerPool.isEmpty() ? (DrawMarker)markerPool.remove(markerPool.size() - 1) : new DrawMarker();
   }

   private static void releaseMarker(DrawMarker var0) {
      var0.reset();
      markerPool.add(var0);
   }

   private CubeMeshManagerBulkOptimized.VBOCell getCell(int var1, CubeMeshManagerBulkOptimized.CubeMeshDynOpt var2, int var3) {
      CubeMeshManagerBulkOptimized.VBOCell var4 = null;

      for(int var5 = 0; var5 < this.vboSegs.size(); ++var5) {
         if ((var4 = ((CubeMeshManagerBulkOptimized.VBOSeg)this.vboSegs.get(var5)).getFree(var1, var2)) != null) {
            return var4;
         }
      }

      if (var4 == null) {
         CubeMeshManagerBulkOptimized.VBOSeg var6;
         (var6 = new CubeMeshManagerBulkOptimized.VBOSeg()).init(var1);
         this.vboSegs.add(var6);
         var6.reserved = var3;
         if (var3 > 0 && (ObjectArrayList)this.reservedVBOSegs.get(var3) == null) {
            ObjectArrayList var7 = new ObjectArrayList();
            this.reservedVBOSegs.put(var3, var7);
         }

         var4 = var6.getFree(var1, var2);
      }

      return var4;
   }

   public CubeMeshManagerBulkOptimized.VBOCell getFreeSegment(int var1, CubeMeshManagerBulkOptimized.CubeMeshDynOpt var2, DrawableRemoteSegment var3) {
      CubeMeshManagerBulkOptimized.VBOCell var4 = this.getCell(var1, var2, 0);

      assert var4 != null : var3 + "; " + var3.getSegmentController();

      assert var4 != null;

      return var4;
   }

   public CubeMeshInterface getInstance() {
      return new CubeMeshManagerBulkOptimized.CubeMeshDynOpt();
   }

   public void drawMulti(boolean var1, Shader var2) {
      int var3 = this.vboSegs.size();
      boolean var4 = EngineSettings.USE_GL_MULTI_DRAWARRAYS.isOn();
      Shader var5 = var2;
      Shader var6 = var2;
      Shader var7 = null;
      if (var2 != null && var2.optionBits >= 0) {
         (var7 = ShaderLibrary.getCubeShader(var2.optionBits | ShaderLibrary.CubeShaderType.VIRTUAL.bit)).setShaderInterface(var2.getShaderInterface());
      }

      for(int var18 = 0; var18 < var3; ++var18) {
         CubeMeshManagerBulkOptimized.VBOSeg var8 = (CubeMeshManagerBulkOptimized.VBOSeg)this.vboSegs.get(var18);
         if (GlUtil.glBindBuffer(34962, var8.bufferId)) {
            if (GraphicsContext.INTEGER_VERTICES) {
               GlUtil.glVertexAttribIPointer(0, CubeMeshBufferContainer.vertexComponents, 5124, 0, 0L);
            } else {
               GL11.glVertexPointer(CubeMeshBufferContainer.vertexComponents, 5126, 0, 0L);
            }
         }

         int var9 = var8.markers.size();

         for(int var10 = 0; var10 < var9; ++var10) {
            DrawMarker var11;
            if ((var11 = (DrawMarker)var8.markers.get(var10)).start.size() > 0) {
               GlUtil.glPushMatrix();
               GlUtil.glMultMatrix(var11.t);
               if (var7 != null) {
                  if ((var11.optionBits & 1) == 1) {
                     if (var5 != var7) {
                        var6.unload();
                        var7.load();
                        var5 = var7;
                     }
                  } else if (var5 != var6) {
                     var7.unload();
                     var6.load();
                     var5 = var6;
                  }
               }

               if (var4) {
                  long var14 = System.currentTimeMillis();
                  IntBuffer var12;
                  (var12 = GlUtil.getDynamicByteBuffer(var11.start.size() << 2, 0).order(ByteOrder.nativeOrder()).asIntBuffer()).put(var11.start.elements(), 0, var11.start.size());
                  var12.flip();
                  IntBuffer var13;
                  (var13 = GlUtil.getDynamicByteBuffer(var11.count.size() << 2, 1).order(ByteOrder.nativeOrder()).asIntBuffer()).put(var11.count.elements(), 0, var11.count.size());
                  var13.flip();
                  long var16;
                  if ((var16 = System.currentTimeMillis() - var14) > 50L) {
                     System.err.println("WARNING: MultiDraw Buffer Time: " + var16 + "ms");
                  }

                  var14 = System.currentTimeMillis();
                  GL14.glMultiDrawArrays(4, var12, var13);
                  if ((var16 = System.currentTimeMillis() - var14) > 50L) {
                     System.err.println("WARNING: MultiDraw Draw Time: " + var12 + "; " + var13 + " " + var16 + "ms");
                  }
               } else {
                  int var21 = var11.start.size();

                  for(int var15 = 0; var15 < var21; ++var15) {
                     int var19 = var11.start.getInt(var15);
                     int var20 = var11.count.getInt(var15);
                     GL11.glDrawArrays(4, var19, var20);
                  }
               }

               GlUtil.glPopMatrix();
            }

            if (var1) {
               releaseMarker(var11);
            }
         }

         if (var1) {
            var8.markers.clear();
         }
      }

   }

   static {
      MAX_BYTES = (Integer)EngineSettings.G_VBO_BULKMODE_SIZE.getCurrentState() << 10 << 10;
      markerPool = new ObjectArrayList();
   }

   public class CubeMeshDynOpt implements CubeMeshInterface {
      private boolean initialized;
      private CubeMeshManagerBulkOptimized.VBOCell currentVBOCell;

      public void mark(Transform var1, int var2, int var3, boolean var4, int var5) {
         if (this.currentVBOCell != null) {
            int var6;
            int var7;
            if (var5 == 63) {
               if (!var4) {
                  var6 = this.currentVBOCell.startPositionByte / 4 / CubeMeshBufferContainer.vertexComponents;
                  var7 = this.currentVBOCell.blendedFloatStartPos / CubeMeshBufferContainer.vertexComponents;
               } else {
                  var6 = this.currentVBOCell.startPositionByte / 4 / CubeMeshBufferContainer.vertexComponents + this.currentVBOCell.blendedFloatStartPos / CubeMeshBufferContainer.vertexComponents;
                  var7 = (this.currentVBOCell.lengthInBytes / 4 - this.currentVBOCell.blendedFloatStartPos) / CubeMeshBufferContainer.vertexComponents;
               }

               if (var7 > 0) {
                  DrawMarker var11;
                  if (!this.currentVBOCell.vboSeg.markers.isEmpty() && ((DrawMarker)this.currentVBOCell.vboSeg.markers.get(this.currentVBOCell.vboSeg.markers.size() - 1)).id == var2) {
                     var11 = (DrawMarker)this.currentVBOCell.vboSeg.markers.get(this.currentVBOCell.vboSeg.markers.size() - 1);
                  } else {
                     (var11 = CubeMeshManagerBulkOptimized.getMarker()).t.set(var1);
                     var11.id = var2;
                     var11.optionBits = var3;
                     this.currentVBOCell.vboSeg.markers.add(var11);
                  }

                  var11.start.add(var6);
                  var11.count.add(var7);
               }

               return;
            }

            for(var6 = 0; var6 <= 0; ++var6) {
               var7 = Element.SIDE_FLAG[0];
               if ((var5 & var7) == var7) {
                  var7 = this.currentVBOCell.startPositionByte / 4 / CubeMeshBufferContainer.vertexComponents;
                  int var8;
                  if (!var4) {
                     boolean var9 = false;
                     var8 = this.currentVBOCell.opaqueRanges[0][0] / CubeMeshBufferContainer.vertexComponents;
                     var8 += var7;
                     var7 = this.currentVBOCell.opaqueRanges[0][1] / CubeMeshBufferContainer.vertexComponents;
                  } else {
                     int var10 = var7 + this.currentVBOCell.blendedFloatStartPos / CubeMeshBufferContainer.vertexComponents;
                     var8 = this.currentVBOCell.blendedRanges[0][0] / CubeMeshBufferContainer.vertexComponents;
                     var8 += var10;
                     var7 = this.currentVBOCell.blendedRanges[0][1] / CubeMeshBufferContainer.vertexComponents;
                  }

                  if (var7 > 0) {
                     DrawMarker var12;
                     if (!this.currentVBOCell.vboSeg.markers.isEmpty() && ((DrawMarker)this.currentVBOCell.vboSeg.markers.get(this.currentVBOCell.vboSeg.markers.size() - 1)).id == var2) {
                        var12 = (DrawMarker)this.currentVBOCell.vboSeg.markers.get(this.currentVBOCell.vboSeg.markers.size() - 1);
                     } else {
                        (var12 = CubeMeshManagerBulkOptimized.getMarker()).t.set(var1);
                        var12.id = var2;
                        var12.optionBits = var3;
                        this.currentVBOCell.vboSeg.markers.add(var12);
                     }

                     var12.start.add(var8);
                     var12.count.add(var7);
                  }
               }
            }
         }

      }

      public void cleanUp() {
      }

      public void contextSwitch(CubeMeshBufferContainer var1, int[][] var2, int[][] var3, int var4, int var5, DrawableRemoteSegment var6) {
         if (!CubeMeshNormal.checkedForRangeMap) {
            CubeMeshNormal.setGl_ARB_map_buffer_range(GraphicsContext.getCapabilities().GL_ARB_map_buffer_range);
            System.err.println("USE BUFFER RANGE: " + CubeMeshNormal.isGl_ARB_map_buffer_range());
            CubeMeshNormal.checkedForRangeMap = true;
         }

         boolean var13 = false;
         var1.dataBuffer.makeStructured(var2, var3);
         var1.dataBuffer.getTotalBuffer().flip();
         if (!this.initialized) {
            this.prepare((FloatBuffer)null);
            var13 = true;
         }

         if (var1.dataBuffer.getTotalBuffer().limit() != 0) {
            int var7 = var1.dataBuffer.getTotalBuffer().limit() << 2;
            if (this.currentVBOCell != null) {
               this.currentVBOCell.free = true;
            }

            if (this.currentVBOCell == null || !this.currentVBOCell.stillFitsInTaken(var7, this)) {
               this.currentVBOCell = CubeMeshManagerBulkOptimized.this.getFreeSegment(var7, this, var6);
            }

            assert this.currentVBOCell != null : var6 + "; " + var6.getSegmentController();

            assert this.currentVBOCell.getBufferId() != 0;

            GlUtil.glBindBuffer(34962, this.currentVBOCell.getBufferId());
            long var8 = System.nanoTime();
            long var10 = System.nanoTime() - var8;
            if (GraphicsContext.INTEGER_VERTICES) {
               GL15.glBufferSubData(34962, (long)this.currentVBOCell.startPositionByte, ((CubeBufferInt)var1.dataBuffer).totalBuffer);
            } else {
               GL15.glBufferSubData(34962, (long)this.currentVBOCell.startPositionByte, ((CubeBufferFloat)var1.dataBuffer).totalBuffer);
            }

            if ((CubeMeshNormal.bufferContextSwitchTime = (CubeMeshNormal.bufferContextSwitchTime = System.nanoTime() - var8) / 1000000L) > 10L) {
               System.err.println("[CUBE] WARNING: context switch time: " + CubeMeshNormal.bufferContextSwitchTime + " ms : " + var10 / 1000000L + "ms: O 0; P 0; U 0::; map " + CubeMeshNormal.USE_MAP_BUFFER + "; range " + CubeMeshNormal.isGl_ARB_map_buffer_range() + "; init " + var13 + "  unmap true");
            }

            assert var1.dataBuffer.getTotalBuffer().limit() / CubeMeshBufferContainer.vertexComponents == this.currentVBOCell.lengthInBytes / 4 / CubeMeshBufferContainer.vertexComponents : var1.dataBuffer.getTotalBuffer().limit() / CubeMeshBufferContainer.vertexComponents + "; " + this.currentVBOCell.lengthInBytes / 4 / CubeMeshBufferContainer.vertexComponents;

            this.currentVBOCell.blendedFloatStartPos = var4;

            for(int var12 = 0; var12 < 7; ++var12) {
               this.currentVBOCell.blendedRanges[var12][0] = var3[var12][0];
               this.currentVBOCell.blendedRanges[var12][1] = var3[var12][1];
               this.currentVBOCell.opaqueRanges[var12][0] = var2[var12][0];
               this.currentVBOCell.opaqueRanges[var12][1] = var2[var12][1];
            }

         } else {
            if (this.currentVBOCell != null) {
               this.currentVBOCell.free = true;
               this.currentVBOCell = null;
            }

         }
      }

      public void draw(int var1, int var2) {
         this.drawMesh(var1, var2);
      }

      public void drawMesh(int var1, int var2) {
         if (this.currentVBOCell != null) {
            assert this.currentVBOCell.getBufferId() != 0;

            boolean var10000 = GlUtil.glBindBuffer(34962, this.currentVBOCell.getBufferId());
            boolean var3 = false;
            if (var10000) {
               if (GraphicsContext.INTEGER_VERTICES) {
                  GlUtil.glVertexAttribIPointer(0, CubeMeshBufferContainer.vertexComponents, 5124, 0, 0L);
               } else {
                  GL11.glVertexPointer(CubeMeshBufferContainer.vertexComponents, 5126, 0, 0L);
               }
            }

            if (CubeMeshBufferContainer.isTriangle()) {
               if (var2 == 63) {
                  if (var1 == 2) {
                     GL11.glDrawArrays(4, this.currentVBOCell.startPositionByte / 4 / CubeMeshBufferContainer.vertexComponents, this.currentVBOCell.lengthInBytes / 4 / CubeMeshBufferContainer.vertexComponents);
                     return;
                  }

                  if (var1 == 0) {
                     GL11.glDrawArrays(4, this.currentVBOCell.startPositionByte / 4 / CubeMeshBufferContainer.vertexComponents, this.currentVBOCell.blendedFloatStartPos / CubeMeshBufferContainer.vertexComponents);
                     return;
                  }

                  if (var1 == 1) {
                     GL11.glDrawArrays(4, this.currentVBOCell.startPositionByte / 4 / CubeMeshBufferContainer.vertexComponents + this.currentVBOCell.blendedFloatStartPos / CubeMeshBufferContainer.vertexComponents, (this.currentVBOCell.lengthInBytes / 4 - this.currentVBOCell.blendedFloatStartPos) / CubeMeshBufferContainer.vertexComponents);
                     return;
                  }
               } else {
                  if (var1 == 2) {
                     GL11.glDrawArrays(4, this.currentVBOCell.startPositionByte / 4 / CubeMeshBufferContainer.vertexComponents, this.currentVBOCell.lengthInBytes / 4 / CubeMeshBufferContainer.vertexComponents);
                     return;
                  }

                  int var4;
                  int var5;
                  if (var1 == 0) {
                     for(var1 = 0; var1 < 7; ++var1) {
                        var5 = var1 == 6 ? 0 : Element.SIDE_FLAG[var1];
                        if ((var2 & var5) == var5) {
                           var5 = this.currentVBOCell.startPositionByte / 4 / CubeMeshBufferContainer.vertexComponents;
                           var4 = this.currentVBOCell.opaqueRanges[var1][0] / CubeMeshBufferContainer.vertexComponents;
                           var5 += var4;
                           var4 = this.currentVBOCell.opaqueRanges[var1][1] / CubeMeshBufferContainer.vertexComponents;
                           GL11.glDrawArrays(4, var5, var4);
                        }
                     }

                     return;
                  }

                  if (var1 == 1) {
                     for(var1 = 0; var1 < 7; ++var1) {
                        var5 = var1 == 6 ? 0 : Element.SIDE_FLAG[var1];
                        if ((var2 & var5) == var5) {
                           var5 = this.currentVBOCell.startPositionByte / 4 / CubeMeshBufferContainer.vertexComponents + this.currentVBOCell.blendedFloatStartPos / CubeMeshBufferContainer.vertexComponents;
                           var4 = this.currentVBOCell.blendedRanges[var1][0] / CubeMeshBufferContainer.vertexComponents;
                           var5 += var4;
                           var4 = this.currentVBOCell.blendedRanges[var1][1] / CubeMeshBufferContainer.vertexComponents;
                           GL11.glDrawArrays(4, var5, var4);
                        }
                     }

                     return;
                  }
               }
            } else {
               if (var1 == 2) {
                  GL11.glDrawArrays(7, this.currentVBOCell.startPositionByte / 4 / CubeMeshBufferContainer.vertexComponents, this.currentVBOCell.lengthInBytes / 4 / CubeMeshBufferContainer.vertexComponents);
                  return;
               }

               if (var1 == 0) {
                  GL11.glDrawArrays(7, this.currentVBOCell.startPositionByte / 4 / CubeMeshBufferContainer.vertexComponents, this.currentVBOCell.blendedFloatStartPos / CubeMeshBufferContainer.vertexComponents);
                  return;
               }

               if (var1 == 1) {
                  GL11.glDrawArrays(7, this.currentVBOCell.startPositionByte / 4 / CubeMeshBufferContainer.vertexComponents + this.currentVBOCell.blendedFloatStartPos / CubeMeshBufferContainer.vertexComponents, (this.currentVBOCell.lengthInBytes / 4 - this.currentVBOCell.blendedFloatStartPos) / CubeMeshBufferContainer.vertexComponents);
               }
            }

         }
      }

      public boolean isInitialized() {
         return this.initialized;
      }

      public void setInitialized(boolean var1) {
         this.initialized = var1;
      }

      public void prepare(FloatBuffer var1) {
         this.initialized = true;
      }

      public void released() {
         if (this.currentVBOCell != null) {
            this.currentVBOCell.released();
            this.currentVBOCell = null;
         }

      }
   }

   class VBOSeg {
      final ObjectArrayList markers;
      private final ObjectAVLTreeSet cells;
      public int takenCount;
      public int reserved;
      int bufferId;
      int currentSize;
      private int maxBytes;

      private VBOSeg() {
         this.markers = new ObjectArrayList();
         this.cells = new ObjectAVLTreeSet();
      }

      public void init(int var1) {
         if (this.bufferId == 0) {
            this.createBuffer(var1);
         }

      }

      public void checkAllFree() {
         if (this.takenCount == 0) {
            this.cells.clear();
            this.currentSize = 0;
            if (this.reserved > 0) {
               ObjectArrayList var1;
               (var1 = (ObjectArrayList)CubeMeshManagerBulkOptimized.this.reservedVBOSegs.get(this.reserved)).remove(this);
               if (var1.isEmpty()) {
                  CubeMeshManagerBulkOptimized.this.reservedVBOSegs.remove(this.reserved);
               }

               this.reserved = 0;
            }
         }

      }

      public void createBuffer(int var1) {
         var1 = Math.max(var1 + 1, CubeMeshManagerBulkOptimized.MAX_BYTES);
         this.maxBytes = var1;
         this.bufferId = GL15.glGenBuffers();
         ++CubeMeshNormal.initializedBuffers;
         Controller.loadedVBOBuffers.add(this.bufferId);
         GlUtil.glBindBuffer(34962, this.bufferId);
         GL15.glBufferData(34962, (long)var1, CubeMeshNormal.BUFFER_FLAG);
         GameClientState.realVBOSize += var1;
         GameClientState.prospectedVBOSize += 1572864 * CubeMeshBufferContainer.vertexComponents << 2;
         GlUtil.glBindBuffer(34962, 0);
      }

      public CubeMeshManagerBulkOptimized.VBOCell getFree(int var1, CubeMeshManagerBulkOptimized.CubeMeshDynOpt var2) {
         ObjectBidirectionalIterator var3 = this.cells.iterator();
         int var4 = 0;

         CubeMeshManagerBulkOptimized.VBOCell var5;
         do {
            if (!var3.hasNext()) {
               if (this.currentSize + var1 < this.maxBytes) {
                  var5 = CubeMeshManagerBulkOptimized.this.new VBOCell(this.currentSize, this.currentSize + var1, this);
                  this.currentSize += var1;
                  boolean var6 = var5.fitIn(var1, var2);
                  this.cells.add(var5);

                  assert var6;

                  assert !var5.free;

                  return var5;
               }

               return null;
            }

            var5 = (CubeMeshManagerBulkOptimized.VBOCell)var3.next();

            assert var5.initialStart == var4 : var5.initialStart + " : " + var4;

            var4 = var5.initialEnd;
         } while(!var5.fitIn(var1, var2));

         return var5;
      }

      public int hashCode() {
         return this.bufferId;
      }

      public boolean equals(Object var1) {
         return ((CubeMeshManagerBulkOptimized.VBOSeg)var1).bufferId == this.bufferId;
      }

      // $FF: synthetic method
      VBOSeg(Object var2) {
         this();
      }

      // $FF: synthetic method
      static int access$200(CubeMeshManagerBulkOptimized.VBOSeg var0) {
         return var0.maxBytes;
      }
   }

   class VBOCell implements Comparable {
      final int initialStart;
      final int initialEnd;
      final CubeMeshManagerBulkOptimized.VBOSeg vboSeg;
      public int blendedFloatStartPos;
      boolean free = true;
      int startPositionByte;
      int endPositionByte;
      int lengthInBytes;
      public int[][] opaqueRanges = new int[7][2];
      public int[][] blendedRanges = new int[7][2];

      public VBOCell(int var2, int var3, CubeMeshManagerBulkOptimized.VBOSeg var4) {
         this.initialStart = var2;
         this.initialEnd = var3;
         this.vboSeg = var4;
      }

      public boolean stillFitsInTaken(int var1, CubeMeshManagerBulkOptimized.CubeMeshDynOpt var2) {
         if (var1 <= this.sizeInitial()) {
            this.startPositionByte = this.initialStart;
            this.endPositionByte = this.initialStart + var1;
            this.lengthInBytes = var1;
            return true;
         } else {
            return false;
         }
      }

      public boolean fitIn(int var1, CubeMeshManagerBulkOptimized.CubeMeshDynOpt var2) {
         if (this.free && var1 <= this.sizeInitial()) {
            this.startPositionByte = this.initialStart;
            this.endPositionByte = this.initialStart + var1;
            this.lengthInBytes = var1;

            assert this.endPositionByte <= this.initialEnd;

            this.free = false;
            ++this.vboSeg.takenCount;
            return true;
         } else {
            return false;
         }
      }

      private int sizeInitial() {
         return this.initialEnd - this.initialStart;
      }

      public int compareTo(CubeMeshManagerBulkOptimized.VBOCell var1) {
         return this.initialStart - var1.initialStart;
      }

      public int getBufferId() {
         return this.vboSeg.bufferId;
      }

      public String toString() {
         return "VBOCell [initialStart=" + this.initialStart + ", initialEnd=" + this.initialEnd + ", bufferId=" + this.vboSeg.bufferId + ", free=" + this.free + ", startPositionByte=" + this.startPositionByte + ", endPositionByte=" + this.endPositionByte + "]";
      }

      public void released() {
         this.free = true;
         --this.vboSeg.takenCount;
         this.vboSeg.checkAllFree();
      }
   }
}
