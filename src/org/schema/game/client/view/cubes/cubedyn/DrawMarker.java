package org.schema.game.client.view.cubes.cubedyn;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.IntArrayList;

public class DrawMarker {
   public static final int VIRTUAL = 1;
   int id;
   Transform t = new Transform();
   IntArrayList start = new IntArrayList();
   IntArrayList count = new IntArrayList();
   public int optionBits;

   public void reset() {
      this.start.clear();
      this.count.clear();
      this.id = -1;
      this.optionBits = 0;
   }
}
