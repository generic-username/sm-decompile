package org.schema.game.client.view.cubes;

import com.bulletphysics.linearmath.Transform;
import java.util.Collection;
import java.util.Iterator;
import org.lwjgl.opengl.GL11;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;

public class CubeBruteCollectionDrawer implements Drawable {
   public Collection drawCollection;
   public SimpleTransformableSendableObject obj;
   private int diaplayListIndex;
   private boolean generated;

   public void cleanUp() {
   }

   public void draw() {
      if (this.drawCollection != null) {
         if (!this.generated) {
            this.generateDisplayList();
         }

         GlUtil.glPushMatrix();
         GlUtil.glMultMatrix((Transform)this.obj.getWorldTransform());
         GlUtil.glBlendFunc(770, 771);
         GlUtil.glEnable(3042);
         GlUtil.glDisable(2929);
         GlUtil.glDisable(2896);
         GlUtil.glEnable(2903);
         GlUtil.glDisable(3553);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

         assert this.generated;

         GL11.glCallList(this.diaplayListIndex);
         GlUtil.glDisable(2903);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GlUtil.glDisable(3042);
         GlUtil.glEnable(2929);
         GlUtil.glPopMatrix();
      }
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
   }

   protected void generateDisplayList() {
      if (this.diaplayListIndex != 0) {
         GL11.glDeleteLists(this.diaplayListIndex, 1);
      }

      this.diaplayListIndex = GL11.glGenLists(1);
      GL11.glNewList(this.diaplayListIndex, 4864);
      GL11.glBegin(0);
      Vector3i var1 = new Vector3i();
      Iterator var2 = this.drawCollection.iterator();

      while(var2.hasNext()) {
         ElementCollection.getPosFromIndex((Long)var2.next(), var1);
         var1.sub(16, 16, 16);
         GL11.glVertex3f((float)var1.x, (float)var1.y, (float)var1.z);
      }

      GL11.glEnd();
      GL11.glEndList();
      this.generated = true;
   }

   public void updateCollection(Collection var1, SimpleTransformableSendableObject var2) {
      this.drawCollection = var1;
      this.obj = var2;
      this.generated = false;
   }
}
