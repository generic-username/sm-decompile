package org.schema.game.client.view.cubes;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.world.DrawableRemoteSegment;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class CubeDataPool {
   public final int POOL_SIZE;
   private final ObjectArrayList pool;
   private final ObjectArrayList taken;
   private int quota;
   private int overQuota;

   public CubeDataPool() {
      this.POOL_SIZE = (Integer)EngineSettings.G_MAX_SEGMENTSDRAWN.getCurrentState() + 10;
      this.pool = new ObjectArrayList((Integer)EngineSettings.G_MAX_SEGMENTSDRAWN.getCurrentState());
      this.taken = new ObjectArrayList((Integer)EngineSettings.G_MAX_SEGMENTSDRAWN.getCurrentState());
      this.quota = this.POOL_SIZE;
   }

   public void checkPoolSize() {
   }

   public void cleanUp(int var1) {
      synchronized(this.pool) {
         for(int var3 = 0; var3 < this.taken.size(); ++var3) {
            DrawableRemoteSegment var4;
            if ((var4 = ((CubeData)this.taken.get(var3)).lastTouched) != null && var4.getSortingSerial() < var1 - 5000 && !var4.isInUpdate()) {
               if (!this.pool.contains(this.taken.get(var3))) {
                  var4.releaseContainerFromPool();
                  var4.disposeAll();
                  var4.setActive(false);
                  System.err.println("WARNING: Exception: cleaned up stuck mesh! " + var4 + "; " + var4.getSegmentController() + "; SegSector: " + var4.getSegmentController().getSectorId() + "; CurrentSector " + ((GameClientState)var4.getSegmentController().getState()).getCurrentSectorId());
                  --var3;
               }
            } else if (var4 == null) {
               System.err.println("WARNING: Exception: null mesh! ");
            }
         }

      }
   }

   public void cleanUpGL() {
      synchronized(this.pool) {
         CubeData var3;
         for(Iterator var2 = this.pool.iterator(); var2.hasNext(); var3.lastTouched = null) {
            if ((var3 = (CubeData)var2.next()).cubeMesh != null) {
               var3.cubeMesh.cleanUp();
            }
         }

         this.pool.clear();
      }
   }

   private CubeData getDataInstance() {
      synchronized(this.pool) {
         CubeData var2;
         if (this.pool.isEmpty() && this.quota > 0) {
            var2 = new CubeData();
            --this.quota;
            this.taken.add(var2);
            return var2;
         } else {
            while(this.pool.isEmpty()) {
               try {
                  this.pool.wait(5000L);
               } catch (InterruptedException var3) {
                  var3.printStackTrace();
               }

               if (this.pool.isEmpty()) {
                  ++this.overQuota;
                  var2 = new CubeData();
                  this.taken.add(var2);
                  System.err.println("[CUBEMESH] WARNING: overquota " + (this.POOL_SIZE + this.overQuota));
                  return var2;
               }
            }

            var2 = (CubeData)this.pool.remove(0);
            this.taken.add(var2);
            var2.generated = false;

            assert this.pool.size() <= this.POOL_SIZE;

            return var2;
         }
      }
   }

   public CubeData getMesh(DrawableRemoteSegment var1) {
      synchronized(this.pool) {
         CubeData var3;
         (var3 = this.getDataInstance()).lastTouched = var1;

         assert var3.lastTouched != null;

         return var3;
      }
   }

   public boolean isFree(CubeData var1) {
      return this.pool.contains(var1);
   }

   public void release(CubeData var1) {
      synchronized(this.pool) {
         if (var1 != null) {
            var1.released();
            var1.lastTouched = null;
            if (!this.pool.contains(var1)) {
               this.pool.add(var1);
               this.taken.remove(var1);
               this.pool.notify();
            }
         }

      }
   }

   public int size() {
      return this.pool.size();
   }

   public String stats() {
      return this.pool.size() + " / " + this.taken.size() + " / " + (this.POOL_SIZE + this.overQuota);
   }
}
