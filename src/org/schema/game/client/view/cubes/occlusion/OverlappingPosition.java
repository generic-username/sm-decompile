package org.schema.game.client.view.cubes.occlusion;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;

public class OverlappingPosition extends Vector3i {
   OverlappingPositionVertex[] verticesOverlapping = new OverlappingPositionVertex[3];
   public BlockShapeAlgorithm blockShape;
   private byte orientation;
   boolean touchingNeighbor;

   public OverlappingPosition() {
      for(int var1 = 0; var1 < this.verticesOverlapping.length; ++var1) {
         this.verticesOverlapping[var1] = new OverlappingPositionVertex();
      }

   }

   public Vector3i getAbsPosRelativeFrom(Vector3i var1, Vector3i var2) {
      var2.set(var1.x + this.x, var1.y + this.y, var1.z + this.z);
      return var2;
   }

   public boolean exists(Occlusion var1, Vector3i var2) {
      int var5 = Occlusion.getContainIndex(var2.x + this.x, var2.y + this.y, var2.z + this.z);
      int var3;
      if ((var3 = Math.abs(var1.contain[var5])) != 0) {
         this.orientation = var1.orientation[var5];
         this.blockShape = null;
         ElementInformation var4;
         if ((var4 = ElementKeyMap.getInfoFast(var3)).getBlockStyle() != BlockStyle.NORMAL) {
            this.blockShape = BlockShapeAlgorithm.getAlgo(var4.getBlockStyle(), this.orientation);
            if (var4.hasLod() && var4.lodShapeStyle == 1) {
               this.blockShape = this.blockShape.getSpriteAlgoRepresentitive();
            }
         }

         return true;
      } else {
         return false;
      }
   }

   public void set(int var1, int var2, int var3, Occlusion var4) {
      super.set(var1, var2, var3);
      this.touchingNeighbor = Math.abs(var1) + Math.abs(var2) + Math.abs(var3) == 1;
   }
}
