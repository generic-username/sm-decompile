package org.schema.game.client.view.cubes.occlusion;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.CubeMeshBufferContainer;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.pentahedron.topbottom.PentaShapeAlgorithm;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;

abstract class SideProcessor {
   protected final BlockSide blockSide;
   private final Vector4f tmpColor = new Vector4f();
   private final Vector3f tmpLightDir = new Vector3f();
   public int[] blockedSides = new int[2];
   int blockP = 0;
   public static final int lDirBits = 8;
   public static final int lDirBitLow = 3;

   public SideProcessor(int var1) {
      this.blockSide = new BlockSide(var1);
   }

   public void calculateActualOverlap(CenterVertex var1, Vector3i var2, OverlappingPosition var3, Occlusion var4) {
      int var10000 = this.getRelevantCoord(var3);
      boolean var5 = false;
      if (var10000 == 0 || this.blockSide.normal >= 6) {
         int var14 = var3.x << 1;
         int var6 = var3.y << 1;
         int var7 = var3.z << 1;
         int var8 = BlockShapeAlgorithm.getRepresentitiveNormal(this.blockSide.sideId, this.blockSide.normal, var3.blockShape);
         Vector3i[] var9;
         if ((var9 = BlockShapeAlgorithm.getSideVerticesByNormal(this.blockSide.sideId, this.blockSide.normal, var3.blockShape)) != null) {
            for(int var10 = 0; var10 < var9.length; ++var10) {
               int var11 = var14 + var9[var10].x;
               int var12 = var6 + var9[var10].y;
               int var13 = var7 + var9[var10].z;
               if (this.blockSide.normal >= 6 && this.blockSide.normal != var8) {
                  break;
               }

               if (this.checkOverlapShare(var1, var3, var11, var12, var13)) {
                  var11 = this.blockSide.sideId;
                  if (this.blockSide.normal >= 6 && this.blockSide.normal == var8) {
                     var11 = BlockShapeAlgorithm.getAngledSideLightRepresentitive(this.blockSide.sideId, this.blockSide.normal, var3.blockShape);

                     assert var11 < 6 : this.blockSide.sideId + "; " + this.blockSide.normal + "; " + var3.blockShape;
                  }

                  var12 = this.blockSide.normal;
                  boolean var15 = true;
                  if (this.blockSide.normal < 6) {
                     Vector3i var16 = Element.DIRECTIONSi[this.blockSide.normal];
                     var15 = this.checkCloseBlock(var2, var3, var16, var4) && this.checkRemoteBlock(var2, var3, var16, var4);
                  }

                  if (var15) {
                     var1.addShareWith(var2, var3, var11, var12, var4);
                  } else {
                     var1.addBlackShare();
                  }
               }
            }
         }
      }

   }

   private boolean checkCloseBlock(Vector3i var1, OverlappingPosition var2, Vector3i var3, Occlusion var4) {
      int var5 = Occlusion.getContainIndex(var1.x + var3.x, var1.y + var3.y, var1.z + var3.z);
      int var8;
      if ((var8 = Math.abs(var4.contain[var5])) != 0) {
         byte var6 = var4.orientation[var5];
         ElementInformation var9;
         int[] var7 = BlockShapeAlgorithm.getSidesToCheckForVis(var9 = ElementKeyMap.getInfoFast(var8), var6);
         if (var9.slab > 0 && var7.length > 0) {
            for(var8 = 0; var8 < var7.length; ++var8) {
               int var10 = var7[var8];
               Vector3i var11;
               if ((var11 = Element.DIRECTIONSi[var10]).x != 0 && var11.x == var2.x || var11.y != 0 && var11.y == var2.y || var11.z != 0 && var11.z == var2.z) {
                  return false;
               }
            }
         }
      }

      return true;
   }

   private boolean checkRemoteBlock(Vector3i var1, OverlappingPosition var2, Vector3i var3, Occlusion var4) {
      int var6 = Occlusion.getContainIndex(var1.x + var2.x + var3.x, var1.y + var2.y + var3.y, var1.z + var2.z + var3.z);
      int var10;
      if ((var10 = Math.abs(var4.contain[var6])) != 0) {
         byte var7 = var4.orientation[var6];
         int[] var8;
         if ((var8 = BlockShapeAlgorithm.getSidesToCheckForVis(ElementKeyMap.getInfoFast(var10), var7)).length > 0) {
            int var9;
            int var11;
            if (var2.touchingNeighbor) {
               var9 = Element.getDirectionFromCoords(var2.x, var2.y, var2.z);

               for(var10 = 0; var10 < var8.length; ++var10) {
                  var11 = var8[var10];
                  if (var9 == Element.getOpposite(var11)) {
                     this.blockedSides[this.blockP] = var9;
                     ++this.blockP;
                     return false;
                  }
               }
            } else if (this.blockP > 0) {
               for(var9 = 0; var9 < this.blockP; ++var9) {
                  var10 = this.blockedSides[var9];

                  for(var11 = 0; var11 < var8.length; ++var11) {
                     int var5 = var8[var11];
                     if (var10 == Element.getOpposite(var5)) {
                        return false;
                     }
                  }
               }
            }
         }
      }

      return true;
   }

   public boolean checkOverlapShare(CenterVertex var1, OverlappingPosition var2, int var3, int var4, int var5) {
      return false;
   }

   public int getRelevantCoord(OverlappingPosition var1) {
      return 0;
   }

   public void process(int var1, Vector3i var2, BlockShapeAlgorithm var3, Occlusion var4) {
      this.blockSide.normal = BlockShapeAlgorithm.getNormalBySide(var1, var3);
      if (this.blockSide.normal >= 0) {
         Vector3i[] var5;
         int var6;
         if ((var5 = BlockShapeAlgorithm.getSideVerticesByNormal(this.blockSide.sideId, this.blockSide.normal, var3)) != null) {
            if (var5.length == 0) {
               return;
            }

            for(var6 = 0; var6 < this.blockSide.p.length; ++var6) {
               if (var6 < var5.length) {
                  this.blockSide.p[var6].set(var5[var6]);
               }

               this.blockSide.p[var6].reset();
            }

            if (var5.length == 3) {
               this.blockSide.p[3].set(var5[var3.getDoubleVertex()]);
               this.blockSide.p[3].reset();
            }

            this.blockSide.processOverlapping(this, var4, var2);
         } else {
            for(var6 = 0; var6 < this.blockSide.p.length; ++var6) {
               this.blockSide.p[var6].setError();
            }
         }

         var6 = var1 << 2;

         for(int var11 = 0; var11 < this.blockSide.p.length; ++var11) {
            CenterVertex var7 = this.blockSide.p[var11];
            if (var1 < 6) {
               var7.addSelfColor(var2, var1, var4);
            } else {
               var7.addSelfColor(var2, ((PentaShapeAlgorithm)var3).getSidesAngled()[0], var4);
            }

            Vector4f var8 = var7.getAverage(this.tmpColor, this.tmpLightDir);
            int var9 = CubeMeshBufferContainer.getLightInfoIndex(var2.x, var2.y, var2.z);
            CubeMeshBufferContainer var10;
            (var10 = var4.getContainer()).setFinalLight(var9, (byte)FastMath.round(FastMath.clamp(var8.x, 0.0F, 1.0F) * 31.0F), var6 + var11, 0);
            var10.setFinalLight(var9, (byte)FastMath.round(FastMath.clamp(var8.y, 0.0F, 1.0F) * 31.0F), var6 + var11, 1);
            var10.setFinalLight(var9, (byte)FastMath.round(FastMath.clamp(var8.z, 0.0F, 1.0F) * 31.0F), var6 + var11, 2);
            var10.setFinalLight(var9, (byte)FastMath.round(FastMath.clamp(var8.w, 0.0F, 1.0F) * 31.0F), var6 + var11, 3);
            var7.reset();
         }

      }
   }

   public byte encode(float var1) {
      return (byte)((byte)FastMath.fastCeil(Math.abs(var1 * 3.0F)) + (var1 < 0.0F ? 8 : 0));
   }
}
