package org.schema.game.client.view.cubes.occlusion;

public class Offset {
   public float depth;
   public int x;
   public int y;
   public int z;

   public Offset(float var1, int var2, int var3, int var4) {
      this.x = var2;
      this.y = var3;
      this.z = var4;
      this.depth = var1;
   }

   public String toString() {
      return "(" + this.x + ", " + this.y + ", " + this.z + "; " + this.depth + ")";
   }
}
