package org.schema.game.client.view.cubes.occlusion;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;

public class CenterVertex extends Vector3i {
   int overlap = 0;
   private OverlappingPosition[] overlappingPositions = new OverlappingPosition[7];
   private OverlappingPosition[] toTest = new OverlappingPosition[7];
   Vector4f color = new Vector4f();
   private Vector4f colorTmp = new Vector4f();
   private Vector3f lightDir = new Vector3f();
   private Vector3f lightDirTmp = new Vector3f();
   float sharedNum;
   private Vector3i absPosTmp = new Vector3i();

   public CenterVertex() {
      for(int var1 = 0; var1 < this.overlappingPositions.length; ++var1) {
         this.overlappingPositions[var1] = new OverlappingPosition();
      }

   }

   public void reset() {
      this.color.set(0.0F, 0.0F, 0.0F, 0.0F);
      this.lightDir.set(0.0F, 0.0F, 0.0F);
      this.sharedNum = 0.0F;
   }

   public void setError() {
      this.sharedNum = 0.0F;
      this.color.set(1.0F, 0.0F, 0.0F, 0.0F);
      this.lightDir.set(0.0F, 0.0F, 0.0F);
   }

   public void calculatePossibleOverlapping(SideProcessor var1, Vector3i var2, Occlusion var3) {
      this.overlap = 0;
      this.overlappingPositions[0].set(this.x, 0, 0, var3);
      this.overlappingPositions[1].set(0, 0, this.z, var3);
      this.overlappingPositions[2].set(0, this.y, 0, var3);
      this.overlappingPositions[3].set(0, this.y, this.z, var3);
      this.overlappingPositions[4].set(this.x, this.y, 0, var3);
      this.overlappingPositions[5].set(this.x, 0, this.z, var3);
      this.overlappingPositions[6].set(this.x, this.y, this.z, var3);
      var1.blockedSides[0] = -1;
      var1.blockedSides[1] = -1;
      var1.blockP = 0;

      for(int var4 = 0; var4 < this.overlappingPositions.length; ++var4) {
         if (this.overlappingPositions[var4].exists(var3, var2)) {
            this.toTest[this.overlap] = this.overlappingPositions[var4];
            var1.calculateActualOverlap(this, var2, this.toTest[this.overlap], var3);
            ++this.overlap;
         }
      }

   }

   public void addShareWith(Vector3i var1, OverlappingPosition var2, int var3, int var4, Occlusion var5) {
      var1 = var2.getAbsPosRelativeFrom(var1, this.absPosTmp);

      assert var3 < 6;

      Vector4f var6 = var5.getLight(var1, var3, this.colorTmp, this.lightDirTmp);
      this.lightDir.add(this.lightDirTmp);
      this.color.add(var6);
      ++this.sharedNum;
   }

   public void addBlackShare() {
      ++this.sharedNum;
   }

   public Vector4f getAverage(Vector4f var1, Vector3f var2) {
      float var3 = 1.0F / this.sharedNum;
      var1.set(this.color.x * var3, this.color.y * var3, this.color.z * var3, this.color.w * var3);
      var1.scale(1.28F);
      var2.set(this.lightDir.x * var3, this.lightDir.y * var3, this.lightDir.z * var3);
      return var1;
   }

   public void addSelfColor(Vector3i var1, int var2, Occlusion var3) {
      Vector4f var4 = var3.getLight(var1, var2, this.colorTmp, this.lightDirTmp);
      this.lightDir.add(this.lightDirTmp);
      this.color.add(var4);
      ++this.sharedNum;
   }
}
