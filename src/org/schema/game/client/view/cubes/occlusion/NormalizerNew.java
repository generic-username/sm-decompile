package org.schema.game.client.view.cubes.occlusion;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementInformation;

public class NormalizerNew {
   private final Vector3i centerPos = new Vector3i();
   private final SideProcessor[] normalBlockProcessors = new SideProcessor[]{new NormalizerNew.FrontProcessor(0), new NormalizerNew.BackProcessor(1), new NormalizerNew.TopProcessor(2), new NormalizerNew.BottomProcessor(3), new NormalizerNew.RightProcessor(4), new NormalizerNew.LeftProcessor(5)};
   private final NormalizerNew.ExtraSideProcessor extraSide = new NormalizerNew.ExtraSideProcessor(6);
   private byte orientation;
   private BlockShapeAlgorithm algo;

   public void normalize(Occlusion var1, int var2, int var3, int var4, byte var5, ElementInformation var6) {
      this.centerPos.set(var2, var3, var4);
      var2 = Occlusion.getContainIndex(var2, var3, var4);
      this.orientation = var1.orientation[var2];
      this.algo = null;
      if (var6.getBlockStyle() != BlockStyle.NORMAL) {
         this.algo = BlockShapeAlgorithm.getAlgo(var6.getBlockStyle(), this.orientation);
         if (var6.hasLod() && var6.lodShapeStyle == 1) {
            this.algo = ((Oriencube)this.algo).getSpriteAlgoRepresentitive();
         }
      }

      for(var2 = Element.SIDE_FLAG.length - 1; var2 >= 0; --var2) {
         var3 = Element.SIDE_FLAG[var2];
         if (var5 >= var3) {
            this.normalBlockProcessors[var2].process(var2, this.centerPos, this.algo, var1);
            var5 = (byte)(var5 - var3);
         }
      }

      if (var6.getBlockStyle() == BlockStyle.HEPTA) {
         this.extraSide.process(6, this.centerPos, this.algo, var1);
      }

   }

   class LeftProcessor extends SideProcessor {
      public LeftProcessor(int var2) {
         super(var2);
      }

      public boolean checkOverlapShare(CenterVertex var1, OverlappingPosition var2, int var3, int var4, int var5) {
         return var4 == var1.y && var5 == var1.z;
      }

      public int getRelevantCoord(OverlappingPosition var1) {
         return var1.x;
      }
   }

   class RightProcessor extends SideProcessor {
      public RightProcessor(int var2) {
         super(var2);
      }

      public boolean checkOverlapShare(CenterVertex var1, OverlappingPosition var2, int var3, int var4, int var5) {
         return var4 == var1.y && var5 == var1.z;
      }

      public int getRelevantCoord(OverlappingPosition var1) {
         return var1.x;
      }
   }

   class BottomProcessor extends SideProcessor {
      public BottomProcessor(int var2) {
         super(var2);
      }

      public boolean checkOverlapShare(CenterVertex var1, OverlappingPosition var2, int var3, int var4, int var5) {
         return var3 == var1.x && var5 == var1.z;
      }

      public int getRelevantCoord(OverlappingPosition var1) {
         return var1.y;
      }
   }

   class TopProcessor extends SideProcessor {
      public TopProcessor(int var2) {
         super(var2);
      }

      public boolean checkOverlapShare(CenterVertex var1, OverlappingPosition var2, int var3, int var4, int var5) {
         return var3 == var1.x && var5 == var1.z;
      }

      public int getRelevantCoord(OverlappingPosition var1) {
         return var1.y;
      }
   }

   class BackProcessor extends SideProcessor {
      public BackProcessor(int var2) {
         super(var2);
      }

      public boolean checkOverlapShare(CenterVertex var1, OverlappingPosition var2, int var3, int var4, int var5) {
         return var3 == var1.x && var4 == var1.y;
      }

      public int getRelevantCoord(OverlappingPosition var1) {
         return var1.z;
      }
   }

   class FrontProcessor extends SideProcessor {
      public FrontProcessor(int var2) {
         super(var2);
      }

      public boolean checkOverlapShare(CenterVertex var1, OverlappingPosition var2, int var3, int var4, int var5) {
         return var3 == var1.x && var4 == var1.y;
      }

      public int getRelevantCoord(OverlappingPosition var1) {
         return var1.z;
      }
   }

   class ExtraSideProcessor extends SideProcessor {
      public ExtraSideProcessor(int var2) {
         super(var2);
      }

      public boolean checkOverlapShare(CenterVertex var1, OverlappingPosition var2, int var3, int var4, int var5) {
         return var3 == var1.x && var4 == var1.y && var5 == var1.z;
      }

      public int getRelevantCoord(OverlappingPosition var1) {
         return var1.z;
      }
   }
}
