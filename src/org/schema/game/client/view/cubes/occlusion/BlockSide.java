package org.schema.game.client.view.cubes.occlusion;

import org.schema.common.util.linAlg.Vector3i;

public class BlockSide extends Vector3i {
   int sideId;
   protected final CenterVertex[] p = new CenterVertex[4];
   public int normal;

   public BlockSide(int var1) {
      this.sideId = var1;
      this.normal = -1;

      for(var1 = 0; var1 < this.p.length; ++var1) {
         this.p[var1] = new CenterVertex();
      }

   }

   public void reset() {
      this.normal = -1;

      for(int var1 = 0; var1 < this.p.length; ++var1) {
         this.p[var1].reset();
      }

   }

   public void processOverlapping(SideProcessor var1, Occlusion var2, Vector3i var3) {
      for(int var4 = 0; var4 < this.p.length; ++var4) {
         this.p[var4].calculatePossibleOverlapping(var1, var3, var2);
      }

   }
}
