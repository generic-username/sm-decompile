package org.schema.game.client.view.cubes.occlusion;

import javax.vecmath.Vector4f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.view.cubes.CubeMeshBufferContainer;
import org.schema.game.client.view.cubes.shapes.AlgorithmParameters;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.world.SegmentData;

@Deprecated
public class NormalizerOld {
   private static final int[] mappings = new int[]{4, 5, 6, 6, 7, 0, 0, 1, 2, 2, 3, 4};
   private static float[] multTable = new float[]{1.0F, 0.5F, 0.33333334F, 0.25F};
   final AlgorithmParameters po = new AlgorithmParameters();
   private final Vector4i[] p = new Vector4i[8];
   private final Vector4f[] avgLight = new Vector4f[4];
   private final Vector4f[] s = new Vector4f[4];
   private final Vector4i[] sPos = new Vector4i[4];
   private final Vector4f lightSum = new Vector4f();
   public short type;
   public boolean active;
   public byte orientation;
   public BlockStyle blockStyle;
   private Vector4i posAndSide = new Vector4i();

   public NormalizerOld() {
      int var1;
      for(var1 = 0; var1 < 8; ++var1) {
         this.p[var1] = new Vector4i();
      }

      for(var1 = 0; var1 < 4; ++var1) {
         this.s[var1] = new Vector4f();
         this.avgLight[var1] = new Vector4f();
         this.sPos[var1] = new Vector4i();
      }

   }

   private final void calcAverage(short var1, Vector4i var2, Vector4i[] var3, int var4, Occlusion var5) {
      BlockShapeAlgorithm var6 = null;
      short var7 = (short)(var1 * 3);
      if (this.blockStyle != BlockStyle.NORMAL && this.blockStyle != BlockStyle.WEDGE) {
         if ((var6 = BlockShapeAlgorithm.getAlgo(this.blockStyle, this.orientation)).hasExtraSide() && var6.getSidesAngled().length > 0 && var6.getSidesAngled()[0] == var4) {
            var6.createSide(6, var1, this.po);
         } else {
            var6.createSide(var4, var1, this.po);
         }

         if (var4 == this.po.sid) {
            var7 = (short)(this.po.vID * 3);
         }
      }

      this.avgLight[var1].set(this.getAverageLight(var1, var2, var3[mappings[var7]], var3[mappings[var7 + 1]], var3[mappings[var7 + 2]], var4, var5, var6));
   }

   private final void applyNormalization(Vector4i var1, Vector4i[] var2, int var3, SegmentData var4, boolean var5, Occlusion var6) {
      for(short var9 = 0; var9 < 4; ++var9) {
         this.calcAverage(var9, var1, var2, var3, var6);
      }

      int var10 = var3 << 2;
      int var7 = CubeMeshBufferContainer.getLightInfoIndex(var1.x, var1.y, var1.z);
      int var8;
      if (var5) {
         for(var8 = 3; var8 >= 0; --var8) {
            var6.getContainer().setFinalLight(var7, (byte)((int)(FastMath.clamp(this.avgLight[var8].x, 0.0F, 1.0F) * 31.0F)), var10 + (3 - var8), 0);
            var6.getContainer().setFinalLight(var7, (byte)((int)(FastMath.clamp(this.avgLight[var8].y, 0.0F, 1.0F) * 31.0F)), var10 + (3 - var8), 1);
            var6.getContainer().setFinalLight(var7, (byte)((int)(FastMath.clamp(this.avgLight[var8].z, 0.0F, 1.0F) * 31.0F)), var10 + (3 - var8), 2);
            var6.getContainer().setFinalLight(var7, (byte)((int)(FastMath.clamp(this.avgLight[var8].w, 0.0F, 1.0F) * 31.0F)), var10 + (3 - var8), 3);
         }

      } else {
         for(var8 = 0; var8 < 4; ++var8) {
            var6.getContainer().setFinalLight(var7, (byte)((int)(FastMath.clamp(this.avgLight[var8].x, 0.0F, 1.0F) * 31.0F)), var10 + var8, 0);
            var6.getContainer().setFinalLight(var7, (byte)((int)(FastMath.clamp(this.avgLight[var8].y, 0.0F, 1.0F) * 31.0F)), var10 + var8, 1);
            var6.getContainer().setFinalLight(var7, (byte)((int)(FastMath.clamp(this.avgLight[var8].z, 0.0F, 1.0F) * 31.0F)), var10 + var8, 2);
            var6.getContainer().setFinalLight(var7, (byte)((int)(FastMath.clamp(this.avgLight[var8].w, 0.0F, 1.0F) * 31.0F)), var10 + var8, 3);
         }

      }
   }

   public boolean oneDiff(Vector4i var1, Vector4i var2) {
      if (var1.x == var2.x && var1.y == var2.y) {
         return true;
      } else if (var1.x == var2.x && var1.z == var2.z) {
         return true;
      } else {
         return var1.y == var2.y && var1.z == var2.z ? true : true;
      }
   }

   private Vector4f getAverageLight(short var1, Vector4i var2, Vector4i var3, Vector4i var4, Vector4i var5, int var6, Occlusion var7, BlockShapeAlgorithm var8) {
      this.lightSum.set(0.0F, 0.0F, 0.0F, 0.0F);
      this.sPos[0].set(var2);
      this.sPos[1].set(var3);
      this.sPos[2].set(var4);
      this.sPos[3].set(var5);
      Vector4i var10000;
      Vector3i var9;
      short var10;
      if (var8 != null) {
         if (this.po.sid != var6) {
            var9 = Element.DIRECTIONSi[Element.OPPOSITE_SIDE[var6]];

            for(var10 = 1; var10 < 4; ++var10) {
               assert Occlusion.biggerSegmentValid(this.sPos[var10].x + var9.x, this.sPos[var10].y + var9.y, this.sPos[var10].z + var9.z) : this.sPos[var10] + " -> " + Element.getSideString(var6) + " | " + Element.getSideString(this.po.sid) + ": " + (this.sPos[var10].x + var9.x) + ", " + (this.sPos[var10].y + var9.y) + ", " + (this.sPos[var10].z + var9.z) + "; algo: " + var8.getClass().getSimpleName();

               var10000 = this.sPos[var10];
               var10000.x += var9.x;
               var10000 = this.sPos[var10];
               var10000.y += var9.y;
               var10000 = this.sPos[var10];
               var10000.z += var9.z;
            }
         } else if (var8.hasExtraSide() && var8.getSixthSideOrientation() == var6) {
            var9 = Element.DIRECTIONSi[var6];

            for(byte var11 = 0; var11 <= 0; var11 = 1) {
               assert Occlusion.biggerSegmentValid(this.sPos[0].x + var9.x, this.sPos[0].y + var9.y, this.sPos[0].z + var9.z) : this.sPos[0] + " -> " + Element.getSideString(var6) + " | " + Element.getSideString(this.po.sid) + ": " + (this.sPos[0].x + var9.x) + ", " + (this.sPos[0].y + var9.y) + ", " + (this.sPos[0].z + var9.z) + "; algo: " + var8.getClass().getSimpleName();

               var10000 = this.sPos[0];
               var10000.x += var9.x;
               var10000 = this.sPos[0];
               var10000.y += var9.y;
               var10000 = this.sPos[0];
               var10000.z += var9.z;
            }
         }
      }

      var9 = Element.DIRECTIONSi[var6];

      for(var10 = 0; var10 < 4; ++var10) {
         if (var10 > 0 && var7.contain[Occlusion.getContainIndex(this.sPos[var10].x + var9.x, this.sPos[var10].y + var9.y, this.sPos[var10].z + var9.z)] > 0) {
            if (!Occlusion.biggerSegmentValid(this.sPos[var10].x + var9.x, this.sPos[var10].y + var9.y, this.sPos[var10].z + var9.z)) {
               throw new IllegalArgumentException("occlusion invalid" + Element.getSideString(var6) + " | " + Element.getSideString(this.po.sid) + ": " + (this.sPos[var10].x + var9.x) + ", " + (this.sPos[var10].y + var9.y) + ", " + (this.sPos[var10].z + var9.z) + "; algo: " + var8.getClass().getSimpleName());
            }

            var10000 = this.sPos[var10];
            var10000.x += var9.x;
            var10000 = this.sPos[var10];
            var10000.y += var9.y;
            var10000 = this.sPos[var10];
            var10000.z += var9.z;
         }
      }

      return this.lightSum;
   }

   public void normalize(Vector3i var1, SegmentData var2, int var3, Occlusion var4) {
      boolean var5 = var3 == 1 || var3 == 3 || var3 == 5;
      if (var3 >= 2) {
         if (var3 < 4) {
            if (!var5) {
               this.normalizeTop(var1, var2, var3, var4);
               return;
            }

            this.normalizeBottom(var1, var2, var3, var4);
            return;
         }

         if (!var5) {
            this.normalizeRight(var1, var2, var3, var4);
            return;
         }

         this.normalizeLeft(var1, var2, var3, var4);
      }

   }

   final void normalizeFront(Vector3i var1, SegmentData var2, int var3, Occlusion var4) {
      this.posAndSide.set(var1.x, var1.y, var1.z, var3);
      this.p[0].set(var1.x - 1, var1.y, var1.z, var3);
      this.p[1].set(var1.x - 1, var1.y + 1, var1.z, var3);
      this.p[2].set(var1.x, var1.y + 1, var1.z, var3);
      this.p[3].set(var1.x + 1, var1.y + 1, var1.z, var3);
      this.p[4].set(var1.x + 1, var1.y, var1.z, var3);
      this.p[5].set(var1.x + 1, var1.y - 1, var1.z, var3);
      this.p[6].set(var1.x, var1.y - 1, var1.z, var3);
      this.p[7].set(var1.x - 1, var1.y - 1, var1.z, var3);
      this.applyNormalization(this.posAndSide, this.p, var3, var2, true, var4);
   }

   final void normalizeBack(Vector3i var1, SegmentData var2, int var3, Occlusion var4) {
      this.posAndSide.set(var1.x, var1.y, var1.z, var3);
      this.p[0].set(var1.x - 1, var1.y, var1.z, var3);
      this.p[1].set(var1.x - 1, var1.y + 1, var1.z, var3);
      this.p[2].set(var1.x, var1.y + 1, var1.z, var3);
      this.p[3].set(var1.x + 1, var1.y + 1, var1.z, var3);
      this.p[4].set(var1.x + 1, var1.y, var1.z, var3);
      this.p[5].set(var1.x + 1, var1.y - 1, var1.z, var3);
      this.p[6].set(var1.x, var1.y - 1, var1.z, var3);
      this.p[7].set(var1.x - 1, var1.y - 1, var1.z, var3);
      this.applyNormalization(this.posAndSide, this.p, var3, var2, false, var4);
   }

   final void normalizeLeft(Vector3i var1, SegmentData var2, int var3, Occlusion var4) {
      this.posAndSide.set(var1.x, var1.y, var1.z, var3);
      this.p[0].set(var1.x, var1.y, var1.z - 1, var3);
      this.p[1].set(var1.x, var1.y - 1, var1.z - 1, var3);
      this.p[2].set(var1.x, var1.y - 1, var1.z, var3);
      this.p[3].set(var1.x, var1.y - 1, var1.z + 1, var3);
      this.p[4].set(var1.x, var1.y, var1.z + 1, var3);
      this.p[5].set(var1.x, var1.y + 1, var1.z + 1, var3);
      this.p[6].set(var1.x, var1.y + 1, var1.z, var3);
      this.p[7].set(var1.x, var1.y + 1, var1.z - 1, var3);
      this.applyNormalization(this.posAndSide, this.p, var3, var2, false, var4);
   }

   final void normalizeRight(Vector3i var1, SegmentData var2, int var3, Occlusion var4) {
      this.posAndSide.set(var1.x, var1.y, var1.z, var3);
      this.p[0].set(var1.x, var1.y, var1.z - 1, var3);
      this.p[1].set(var1.x, var1.y + 1, var1.z - 1, var3);
      this.p[2].set(var1.x, var1.y + 1, var1.z, var3);
      this.p[3].set(var1.x, var1.y + 1, var1.z + 1, var3);
      this.p[4].set(var1.x, var1.y, var1.z + 1, var3);
      this.p[5].set(var1.x, var1.y - 1, var1.z + 1, var3);
      this.p[6].set(var1.x, var1.y - 1, var1.z, var3);
      this.p[7].set(var1.x, var1.y - 1, var1.z - 1, var3);
      this.applyNormalization(this.posAndSide, this.p, var3, var2, false, var4);
   }

   final void normalizeBottom(Vector3i var1, SegmentData var2, int var3, Occlusion var4) {
      this.posAndSide.set(var1.x, var1.y, var1.z, var3);
      this.p[0].set(var1.x - 1, var1.y, var1.z, var3);
      this.p[1].set(var1.x - 1, var1.y, var1.z - 1, var3);
      this.p[2].set(var1.x, var1.y, var1.z - 1, var3);
      this.p[3].set(var1.x + 1, var1.y, var1.z - 1, var3);
      this.p[4].set(var1.x + 1, var1.y, var1.z, var3);
      this.p[5].set(var1.x + 1, var1.y, var1.z + 1, var3);
      this.p[6].set(var1.x, var1.y, var1.z + 1, var3);
      this.p[7].set(var1.x - 1, var1.y, var1.z + 1, var3);
      this.applyNormalization(this.posAndSide, this.p, var3, var2, false, var4);
   }

   final void normalizeTop(Vector3i var1, SegmentData var2, int var3, Occlusion var4) {
      this.posAndSide.set(var1.x, var1.y, var1.z, var3);
      this.p[0].set(var1.x - 1, var1.y, var1.z, var3);
      this.p[1].set(var1.x - 1, var1.y, var1.z + 1, var3);
      this.p[2].set(var1.x, var1.y, var1.z + 1, var3);
      this.p[3].set(var1.x + 1, var1.y, var1.z + 1, var3);
      this.p[4].set(var1.x + 1, var1.y, var1.z, var3);
      this.p[5].set(var1.x + 1, var1.y, var1.z - 1, var3);
      this.p[6].set(var1.x, var1.y, var1.z - 1, var3);
      this.p[7].set(var1.x - 1, var1.y, var1.z - 1, var3);
      this.applyNormalization(this.posAndSide, this.p, var3, var2, false, var4);
   }
}
