package org.schema.game.client.view.cubes.occlusion;

import org.schema.common.FastMath;

public class Ray {
   public byte[] points;
   public float[] depths;
   float[] data = new float[6];
   int point_count;

   public Ray(int var1) {
      this.point_count = var1;
   }

   public void compute(float var1, float var2, float var3) {
      this.points = new byte[this.point_count * 3];
      this.depths = new float[this.point_count];
      this.togrid(var1, var2, var3, this.points, this.depths);
      this.data[4] = var1 < 0.0F ? -var1 : 0.0F;
      this.data[5] = var1 > 0.0F ? var1 : 0.0F;
      this.data[2] = var2 < 0.0F ? -var2 : 0.0F;
      this.data[3] = var2 > 0.0F ? var2 : 0.0F;
      this.data[0] = var3 < 0.0F ? -var3 : 0.0F;
      this.data[1] = var3 > 0.0F ? var3 : 0.0F;
   }

   private void togrid(float var1, float var2, float var3, byte[] var4, float[] var5) {
      float var6 = var1 * 0.3F;
      float var7 = var2 * 0.3F;
      float var8 = var3 * 0.3F;
      var1 = 0.0F;
      var2 = 0.0F;
      var3 = 0.0F;
      int var9 = 0;
      int var10 = 0;
      int var11 = 0;
      int var12 = 0;

      for(int var13 = 0; var12 < this.point_count * 3; var3 += var8) {
         int var14 = Math.round(var1);
         int var15 = Math.round(var2);
         int var16 = Math.round(var3);
         if (var14 != var9 || var15 != var10 || var16 != var11) {
            while(var12 < this.point_count * 3 && (var14 != var9 || var15 != var10 || var16 != var11)) {
               if (var14 != var9) {
                  if (var9 < var14) {
                     ++var9;
                  } else {
                     --var9;
                  }
               } else if (var15 != var10) {
                  if (var10 < var15) {
                     ++var10;
                  } else {
                     --var10;
                  }
               } else if (var16 != var11) {
                  if (var11 < var16) {
                     ++var11;
                  } else {
                     --var11;
                  }
               }

               float var17 = FastMath.sqrt(var1 * var1 + var2 * var2 + var3 * var3);
               var4[var12] = (byte)var9;
               var4[var12 + 1] = (byte)var10;
               var4[var12 + 2] = (byte)var11;
               var5[var13] = 1.0F / var17;
               var12 += 3;
               ++var13;
            }
         }

         var1 += var6;
         var2 += var7;
      }

   }
}
