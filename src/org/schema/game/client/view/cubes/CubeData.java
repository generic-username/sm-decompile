package org.schema.game.client.view.cubes;

import org.schema.game.client.view.cubes.cubedyn.CubeMeshManagerBulkOptimized;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.DrawableRemoteSegment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.schine.graphicsengine.forms.SimplePosElement;

public class CubeData {
   public static final CubeMeshManagerBulkOptimized manager = new CubeMeshManagerBulkOptimized();
   public static int drawnMeshes;
   public static int cubeMeshes;
   public static boolean useBulkMode = true;
   public int totalDrawnBlockCount;
   public DrawableRemoteSegment lastTouched;
   public CubeMeshInterface cubeMesh;
   public boolean generated;
   private int blendedElementsCount;
   private int endBufferPos;
   private int blendBufferPos;
   public int[] opaqueSizes = new int[7];
   public int[][] opaqueRanges = new int[7][2];
   public int[][] blendedRanges = new int[7][2];

   public static void resetDrawn() {
   }

   public void contextSwitch(CubeMeshBufferContainer var1, DrawableRemoteSegment var2, int var3) {
      if (this.cubeMesh == null) {
         if (useBulkMode) {
            this.cubeMesh = manager.getInstance();
         } else {
            this.cubeMesh = new CubeMeshNormal();
         }
      }

      this.cubeMesh.contextSwitch(var1, this.opaqueRanges, this.blendedRanges, this.blendBufferPos, this.totalDrawnBlockCount, var2);
   }

   public void released() {
      if (this.cubeMesh != null) {
         this.cubeMesh.released();
      }

   }

   public void createIndex(SegmentData var1, CubeMeshBufferContainer var2) {
      var2.dataBuffer.clearBuffers();
      this.totalDrawnBlockCount = 0;
      this.blendedElementsCount = 0;
      int var3 = 0;

      int var4;
      int var6;
      for(var4 = 0; var4 < 32768; ++var3) {
         short var5;
         if ((var5 = var1.getType(var3)) != 0 && ElementInformation.isPhysical(var5, var1, var3)) {
            ++this.totalDrawnBlockCount;
            if (ElementKeyMap.getInfo(var5).isBlended()) {
               var2.blendedElementBuffer[this.blendedElementsCount] = var4;
               var2.blendedElementTypeBuffer[this.blendedElementsCount] = var5;
               ++this.blendedElementsCount;
            } else {
               var2.p.fresh = true;

               for(var6 = 0; var6 < 6; ++var6) {
                  CubeMeshBufferContainer.putIndex(var2, var4, var3, var1, SimplePosElement.SIDE_FLAG[var6], var6, var5);
               }
            }
         }

         ++var4;
      }

      var2.dataBuffer.createOpaqueSizes(this.opaqueRanges);
      this.blendBufferPos = var2.dataBuffer.totalPosition();

      for(var3 = 0; var3 < this.blendedElementsCount; ++var3) {
         int var7 = var4 = var2.blendedElementBuffer[var3];
         var2.p.fresh = true;

         for(var6 = 0; var6 < 6; ++var6) {
            CubeMeshBufferContainer.putIndex(var2, var4, var7, var1, SimplePosElement.SIDE_FLAG[var6], var6, var2.blendedElementTypeBuffer[var3]);
         }
      }

      var2.dataBuffer.createBlendedRanges(this.opaqueRanges, this.blendedRanges);
      this.endBufferPos = var2.dataBuffer.totalPosition();
   }

   public void draw(int var1, int var2) {
      this.cubeMesh.draw(var1, var2);
      ++drawnMeshes;
   }

   public int getBlendBufferPos() {
      return this.blendBufferPos;
   }

   public void setBlendBufferPos(int var1) {
      this.blendBufferPos = var1;
   }

   public int getBlendedElementsCount() {
      return this.blendedElementsCount;
   }
}
