package org.schema.game.client.view.cubes.lodshapes;

import com.bulletphysics.linearmath.Transform;
import java.nio.FloatBuffer;
import java.util.List;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.forms.Mesh;

public interface CubeLodShapeInterface extends Comparable {
   float getLodDistance();

   boolean isBlockAtDistance();

   short getBlockTypeAtDistance();

   byte getOrientation();

   boolean isPhysical();

   boolean isPhysicalMesh();

   List getPhysicalMesh();

   Mesh getModel(int var1, boolean var2);

   byte[] getBlockRepresentation();

   Vector4f[] getLighting();

   short getType();

   Transform getClientTransform();

   void fillLightBuffers(Transform var1, FloatBuffer var2, FloatBuffer var3);

   void setLight(int var1, float var2, float var3, float var4, float var5);

   void resetLight();

   void calcLight();
}
