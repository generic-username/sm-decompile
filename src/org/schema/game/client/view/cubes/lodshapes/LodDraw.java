package org.schema.game.client.view.cubes.lodshapes;

import com.bulletphysics.linearmath.Transform;
import java.nio.FloatBuffer;
import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.forms.Mesh;

public class LodDraw implements Comparable {
   private static Vector3f posTmp = new Vector3f();
   public Transform transform = new Transform();
   public short type;
   public Mesh mesh;
   public Mesh meshDetail;
   public float[] lightingAndPos;
   public int pointer;
   public boolean faulty;

   public int compareTo(LodDraw var1) {
      return this.type - var1.type;
   }

   public void fillLightBuffers(FloatBuffer var1, FloatBuffer var2) {
      var1.rewind();
      var2.rewind();

      for(int var3 = 0; var3 < 4; ++var3) {
         var2.put(this.lightingAndPos, this.pointer, 4);
         int var4 = this.pointer + 4;
         posTmp.x = this.lightingAndPos[var4];
         posTmp.y = this.lightingAndPos[var4 + 1];
         posTmp.z = this.lightingAndPos[var4 + 2];
         this.transform.transform(posTmp);
         var1.put(posTmp.x);
         var1.put(posTmp.y);
         var1.put(posTmp.z);
         this.pointer += 7;
      }

      var1.rewind();
      var2.rewind();
   }
}
