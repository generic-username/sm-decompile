package org.schema.game.client.view;

import org.schema.common.util.linAlg.Vector4D;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.damage.projectile.ProjectileController;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class ProjectileDrawer implements Drawable, Shaderable {
   Vector4D camPos;
   Vector4D lightPos;
   private ProjectileController projectileController;
   private GameClientState state;
   private float cover;
   private Sprite sprite;
   private Shader shader;

   public ProjectileDrawer(ProjectileController var1) {
      this.projectileController = var1;
      this.state = (GameClientState)var1.getState();
   }

   public void cleanUp() {
   }

   public void draw() {
      this.sprite.setBillboard(true);
      this.sprite.getScale().set(0.3F, 0.3F, 0.3F);
      if (this.projectileController.getParticleCount() > 0) {
         this.shader = ShaderLibrary.projectileShader;
         GlUtil.glPushMatrix();
         this.shader.setShaderInterface(this);
         this.shader.load();
         GlUtil.glEnable(2929);
         GlUtil.glDepthMask(false);
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         this.sprite.setFlip(true);
         this.shader.unload();
         GlUtil.glDisable(3042);
         GlUtil.glDepthMask(true);
         GlUtil.glPopMatrix();
         GlUtil.glDisable(2903);
         GlUtil.glEnable(2896);
      }

   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.sprite = Controller.getResLoader().getSprite("smoke4");
      this.lightPos = new Vector4D();
      this.camPos = new Vector4D();
   }

   public void onExit() {
   }

   public void updateShader(DrawableScene var1) {
      this.lightPos.set(var1.getLight().getPos().x, var1.getLight().getPos().y, var1.getLight().getPos().z, 1.0F);
      this.camPos.set(Controller.getCamera().getPos().x, Controller.getCamera().getPos().y, Controller.getCamera().getPos().z, 0.0F);
   }

   public void updateShaderParameters(Shader var1) {
      this.cover = 5.0F;
      GlUtil.updateShaderVector4D(var1, "source", this.camPos);
      GlUtil.updateShaderFloat(var1, "cCover", this.cover * 2.0F);
      GlUtil.updateShaderVector4D(var1, "lightPos", this.lightPos);
      GlUtil.updateShaderTexture2D(var1, "noiseVolume", this.sprite.getMaterial().getTexture().getTextureId(), 0);
   }
}
