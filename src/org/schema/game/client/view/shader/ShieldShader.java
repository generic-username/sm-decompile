package org.schema.game.client.view.shader;

import java.nio.FloatBuffer;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.BufferUtils;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.client.view.effects.ShieldDrawerManager;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class ShieldShader implements Shaderable {
   public static final int MAX_NUM = 8;
   private static final FloatBuffer fbAlphas = BufferUtils.createFloatBuffer(8);
   private static final FloatBuffer fbDmg = BufferUtils.createFloatBuffer(8);
   private static final FloatBuffer fbPerc = BufferUtils.createFloatBuffer(8);
   private static final FloatBuffer fb = BufferUtils.createFloatBuffer(24);
   private static final Vector3f[] tmpP = new Vector3f[16];
   public Shader s = null;
   private float[] alphas = new float[8];
   private Vector4f[] points = new Vector4f[8];
   private int collisionNum;
   private float minAlpha = 0.0F;
   private float maxDistance = 4.0F;
   private boolean pointsChanged = false;
   private float[] percent = new float[8];

   public ShieldShader() {
      for(int var1 = 0; var1 < 8; ++var1) {
         this.points[var1] = new Vector4f();
      }

   }

   public void addCollision(Vector3f var1, float var2, float var3) {
      if (this.collisionNum < 8) {
         this.alphas[this.collisionNum] = 1.0F;
         this.points[this.collisionNum].set(var1.x, var1.y, var1.z, Math.min(1.0F, Math.max(0.01F, var2 / 1000.0F)));
         this.percent[this.collisionNum] = var3;
         ++this.collisionNum;
         this.pointsChanged = true;
      }

   }

   public void drawPoints() {
   }

   public int getCollisionNum() {
      return this.collisionNum;
   }

   public void setCollisionNum(int var1) {
      this.collisionNum = var1;
   }

   public boolean hasCollisionInRange(float var1, float var2, float var3) {
      for(int var4 = 0; var4 < this.getCollisionNum(); ++var4) {
         float var5 = var1 - this.points[var4].x;
         float var6 = var2 - this.points[var4].y;
         float var7 = var3 - this.points[var4].z;
         if (var5 * var5 + var6 * var6 + var7 * var7 < 576.0F) {
            return true;
         }
      }

      return false;
   }

   public void onExit() {
      GlUtil.glActiveTexture(33986);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      if (var1 == null) {
         throw new NullPointerException("Shield Shader NULL; Shield Drawing enabled: " + EngineSettings.G_DRAW_SHIELDS.isOn());
      } else {
         GlUtil.glActiveTexture(33984);
         GlUtil.glBindTexture(3553, Controller.getResLoader().getSprite("shield_tex").getMaterial().getTexture().getTextureId());
         GlUtil.glActiveTexture(33985);
         GlUtil.glBindTexture(3553, GameResourceLoader.effectTextures[0].getTextureId());
         GlUtil.glActiveTexture(33986);
         GlUtil.glBindTexture(3553, GameResourceLoader.effectTextures[1].getTextureId());
         if (var1.recompiled) {
            GlUtil.updateShaderInt(var1, "m_ShieldTex", 0);
            GlUtil.updateShaderInt(var1, "m_Distortion", 1);
            GlUtil.updateShaderInt(var1, "m_Noise", 2);
            GlUtil.updateShaderCubeNormalsBiNormalsAndTangentsBoolean(var1);
            FloatBuffer var2;
            (var2 = GlUtil.getDynamicByteBuffer(72, 0).asFloatBuffer()).rewind();

            for(int var3 = 0; var3 < CubeMeshQuadsShader13.quadPosMark.length; ++var3) {
               var2.put(CubeMeshQuadsShader13.quadPosMark[var3].x);
               var2.put(CubeMeshQuadsShader13.quadPosMark[var3].y);
               var2.put(CubeMeshQuadsShader13.quadPosMark[var3].z);
            }

            var2.rewind();
            GlUtil.updateShaderFloats3(var1, "quadPosMark", var2);
            var1.recompiled = false;
         }

         for(int var4 = 0; var4 < this.getCollisionNum(); ++var4) {
            fbAlphas.put(var4, this.alphas[var4]);
            if (this.pointsChanged) {
               fb.put(var4 * 3, this.points[var4].x);
               fb.put(var4 * 3 + 1, this.points[var4].y);
               fb.put(var4 * 3 + 2, this.points[var4].z);
               fbDmg.put(var4, this.points[var4].w);
               fbPerc.put(var4, this.percent[var4]);
            }
         }

         fbAlphas.rewind();
         GlUtil.updateShaderFloats1(var1, "m_CollisionAlphas", fbAlphas);
         if (this.pointsChanged) {
            fb.rewind();
            GlUtil.updateShaderFloats3(var1, "m_Collisions", fb);
            this.pointsChanged = false;
            GlUtil.updateShaderInt(var1, "m_CollisionNum", this.getCollisionNum());
            fbDmg.rewind();
            GlUtil.updateShaderFloats1(var1, "m_Damages", fbDmg);
            fbPerc.rewind();
            GlUtil.updateShaderFloats1(var1, "m_Percent", fbPerc);
         }

         GlUtil.updateShaderFloat(var1, "m_MinAlpha", this.minAlpha);
         GlUtil.updateShaderFloat(var1, "m_MaxDistance", this.maxDistance);
         GlUtil.updateShaderFloat(var1, "m_Time", ShieldDrawerManager.time);
      }
   }

   public void reset() {
      this.collisionNum = 0;
   }

   public void update(Timer var1) {
      int var2;
      for(var2 = 0; var2 < this.getCollisionNum(); ++var2) {
         float[] var10000 = this.alphas;
         var10000[var2] -= var1.getDelta() * 0.7F;
      }

      for(var2 = 0; var2 < this.getCollisionNum(); ++var2) {
         if (this.alphas[var2] <= 0.0F) {
            this.alphas[var2] = 0.0F;
            this.alphas[var2] = this.alphas[this.getCollisionNum() - 1];
            this.points[var2].set(this.points[this.getCollisionNum() - 1]);
            this.setCollisionNum(this.getCollisionNum() - 1);
            this.pointsChanged = true;
         }
      }

   }

   static {
      for(int var0 = 0; var0 < tmpP.length; ++var0) {
         tmpP[var0] = new Vector3f();
      }

   }
}
