package org.schema.game.client.view.shader;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class CubeMeshPointShader implements Shaderable {
   Matrix4f mvp = new Matrix4f();
   Matrix4f modelView = new Matrix4f();
   Matrix4f projection = new Matrix4f();
   private int textureAId = 0;

   public int getTextureAId() {
      return this.textureAId;
   }

   public void setTextureAId(int var1) {
      this.textureAId = var1;
   }

   public void onExit() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      this.modelView = GlUtil.retrieveModelviewMatrix(this.modelView);
      this.projection = GlUtil.retrieveProjectionMatrix(this.projection);
      this.mvp = GlUtil.retrieveModelviewProjectionMatrix();
      GlUtil.modelBuffer.rewind();
      this.modelView.load(GlUtil.modelBuffer);
      GlUtil.retrieveNormalMatrix(this.modelView);
      GlUtil.printGlErrorCritical();
      GlUtil.mvpBuffer.rewind();
      GlUtil.updateShaderMat4(var1, "mvpMatrix", GlUtil.mvpBuffer, false);
      GlUtil.normalBuffer.rewind();
      GlUtil.updateShaderMat3(var1, "normalMatrix", GlUtil.normalBuffer, false);
      GlUtil.updateShaderVector3f(var1, "lightDir", new Vector3f(0.0F, 1.0F, 0.0F));
      GlUtil.printGlErrorCritical();
      GlUtil.updateShaderVector3f(var1, "lightColor", new Vector3f(1.0F, 1.0F, 1.0F));
      GlUtil.printGlErrorCritical();
      GlUtil.updateShaderVector3f(var1, "lightAmbient", new Vector3f(0.3F, 0.3F, 0.3F));
      GlUtil.printGlErrorCritical();
      GlUtil.updateShaderVector4f(var1, "matColor", new Vector4f(0.3F, 0.3F, 0.3F, 1.0F));
      GlUtil.printGlErrorCritical();
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, this.getTextureAId());
      GlUtil.updateShaderFloat(var1, "Size2", 0.5F);
      GlUtil.updateShaderInt(var1, "mainTexA", 0);
      GlUtil.printGlErrorCritical();
   }
}
