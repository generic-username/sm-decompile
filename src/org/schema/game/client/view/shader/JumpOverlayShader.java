package org.schema.game.client.view.shader;

import java.nio.FloatBuffer;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class JumpOverlayShader implements Shaderable {
   public Shader s = null;
   public float minAlpha = 0.0F;
   public float m_time = 0.0F;

   public void onExit() {
      GlUtil.glActiveTexture(33986);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      if (var1 == null) {
         throw new NullPointerException("Jump Shader NULL; Jump Drawing enabled: " + EngineSettings.G_DRAW_SHIELDS.isOn());
      } else {
         GlUtil.glActiveTexture(33984);
         GlUtil.glBindTexture(3553, Controller.getResLoader().getSprite("shield_tex").getMaterial().getTexture().getTextureId());
         GlUtil.glActiveTexture(33985);
         GlUtil.glBindTexture(3553, GameResourceLoader.effectTextures[0].getTextureId());
         GlUtil.glActiveTexture(33986);
         GlUtil.glBindTexture(3553, GameResourceLoader.effectTextures[1].getTextureId());
         if (var1.recompiled) {
            GlUtil.updateShaderInt(var1, "m_ShieldTex", 0);
            GlUtil.updateShaderInt(var1, "m_Distortion", 1);
            GlUtil.updateShaderInt(var1, "m_Noise", 2);
            GlUtil.updateShaderCubeNormalsBiNormalsAndTangentsBoolean(var1);
            FloatBuffer var2;
            (var2 = GlUtil.getDynamicByteBuffer(72, 0).asFloatBuffer()).rewind();

            for(int var3 = 0; var3 < CubeMeshQuadsShader13.quadPosMark.length; ++var3) {
               var2.put(CubeMeshQuadsShader13.quadPosMark[var3].x);
               var2.put(CubeMeshQuadsShader13.quadPosMark[var3].y);
               var2.put(CubeMeshQuadsShader13.quadPosMark[var3].z);
            }

            var2.rewind();
            GlUtil.updateShaderFloats3(var1, "quadPosMark", var2);
         }

         GlUtil.updateShaderFloat(var1, "m_MinAlpha", this.minAlpha);
         GlUtil.updateShaderFloat(var1, "m_Time", this.m_time);
         var1.recompiled = false;
      }
   }
}
