package org.schema.game.client.view.meshlod;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.schine.graphicsengine.forms.Sprite;

public class LODDeferredSpriteCollection extends ObjectArrayList {
   private static final long serialVersionUID = -8875572023215012643L;
   public Sprite deferredSprite;
}
