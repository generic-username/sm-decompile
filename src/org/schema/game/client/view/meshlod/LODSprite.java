package org.schema.game.client.view.meshlod;

import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.Sprite;

public class LODSprite extends LODMesh {
   public final String spriteName;
   private final int subSpriteIndex;
   private Sprite sprite;

   public LODSprite(int var1, float var2, String var3, int var4) {
      super(var1, var2);
      this.spriteName = var3;
      this.subSpriteIndex = var4;
   }

   public void loadResourcesA() {
      this.sprite = Controller.getResLoader().getSprite(this.spriteName);

      assert this.sprite != null : this.spriteName;

   }

   public void loadResourcesB() {
   }

   public void unloadResourcesA() {
   }

   public void unloadResourcesB() {
   }

   public void drawA(LODDrawerInterface var1) {
      var1.drawSprites(this.sprite, this.subSpriteIndex);
   }

   public void drawB(LODDrawerInterface var1) {
   }

   public boolean isDeferred() {
      return true;
   }

   public boolean isDrawA() {
      return false;
   }

   public boolean isDrawB() {
      return false;
   }

   public Sprite getDefferredSprite() {
      this.sprite = Controller.getResLoader().getSprite(this.spriteName);

      assert this.sprite != null : this.spriteName;

      return this.sprite;
   }

   public boolean isBlending() {
      return false;
   }

   public boolean isDeferredA() {
      return true;
   }

   public boolean isDeferredB() {
      return false;
   }
}
