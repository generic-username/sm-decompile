package org.schema.game.client.view.meshlod;

import org.schema.schine.graphicsengine.core.Timer;

public interface LODDrawerSystemInterface {
   void drawLevel(int var1, LODMesh var2, LODDeferredSpriteCollection var3);

   void update(int var1, Timer var2, LODMesh var3);
}
