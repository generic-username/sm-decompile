package org.schema.game.client.view.meshlod;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.List;
import org.schema.schine.graphicsengine.core.Timer;

public class LODMeshSystem {
   private List levels = new ObjectArrayList();

   public void addLevelSingleMesh(float var1, String var2) {
      LODSingleMesh var3 = new LODSingleMesh(this.levels.size(), var1, var2);

      assert var3.meshA != null;

      this.levels.add(var3);
   }

   public void addLevelDoubleMesh(float var1, String var2, String var3) {
      LODDoubleMesh var4 = new LODDoubleMesh(this.levels.size(), var1, var2, var3);

      assert var4.meshA != null;

      assert var4.meshB != null;

      this.levels.add(var4);
   }

   public void addLevelSpriteMesh(float var1, String var2, String var3, int var4) {
      LODMeshSprite var5 = new LODMeshSprite(this.levels.size(), var1, var2, var3, var4);

      assert var5.meshA != null;

      assert var5.spriteName != null;

      this.levels.add(var5);
   }

   public void addLevelSprite(float var1, String var2, int var3) {
      LODSprite var4 = new LODSprite(this.levels.size(), var1, var2, var3);

      assert var4.spriteName != null;

      this.levels.add(var4);
   }

   public void update(Timer var1, LODDrawerSystemInterface var2) {
      for(int var3 = 0; var3 < this.levels.size(); ++var3) {
         var2.update(var3, var1, (LODMesh)this.levels.get(var3));
      }

   }

   public void draw(LODDrawerSystemInterface var1, LODDeferredSpriteCollection var2) {
      for(int var3 = 0; var3 < this.levels.size(); ++var3) {
         var1.drawLevel(var3, (LODMesh)this.levels.get(var3), var2);
      }

   }

   public int getLevelCount() {
      return this.levels.size();
   }

   public void init(LODMeshSystem.LODStage... var1) {
      float var2 = 0.0F;

      for(int var3 = 0; var3 < var1.length; ++var3) {
         LODMeshSystem.LODStage var4;
         if (var3 > 0) {
            var4 = var1[var3 - 1];
            LODMeshSystem.LODStage var5 = var1[var3];
            var2 += var5.marginIn;

            assert var4.name != null : var4 + "; " + var3;

            if (var5.spriteMode) {
               this.addLevelSpriteMesh(var2, var4.name, var5.name, var5.subSpriteIndex);
            } else {
               this.addLevelDoubleMesh(var2, var4.name, var5.name);
            }
         }

         var4 = var1[var3];
         var2 += var3 == var1.length - 1 ? Float.POSITIVE_INFINITY : var4.distance;
         if (var4.spriteMode) {
            this.addLevelSprite(var2, var4.name, var4.subSpriteIndex);
         } else {
            this.addLevelSingleMesh(var2, var4.name);
         }
      }

      ((ObjectArrayList)this.levels).trim();
   }

   public int getLevelFromDistance(float var1) {
      for(int var2 = 0; var2 < this.levels.size(); ++var2) {
         if (var1 <= ((LODMesh)this.levels.get(var2)).maxDistance) {
            return var2;
         }
      }

      return this.levels.size() - 1;
   }

   public void cleanUp() {
   }

   public float getBlendingValue(int var1, float var2) {
      if (!((LODMesh)this.levels.get(var1)).isBlending()) {
         return 0.0F;
      } else {
         float var3 = 0.0F;
         if (var1 > 0) {
            var3 = ((LODMesh)this.levels.get(var1 - 1)).maxDistance;
         }

         float var4;
         return (var4 = ((LODMesh)this.levels.get(var1)).maxDistance) == Float.POSITIVE_INFINITY ? 0.0F : (var2 - var3) / (var4 - var3);
      }
   }

   public static class LODStage {
      public final float distance;
      public final float marginIn;
      public final boolean spriteMode;
      public final String name;
      public final int subSpriteIndex;

      public LODStage(float var1, float var2, boolean var3, String var4, int var5) {
         this.distance = var1;
         this.marginIn = var2;
         this.spriteMode = var3;
         this.name = var4;
         this.subSpriteIndex = var5;
      }
   }
}
