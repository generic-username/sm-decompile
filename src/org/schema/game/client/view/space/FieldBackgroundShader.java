package org.schema.game.client.view.space;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class FieldBackgroundShader implements Shaderable {
   public Vector4f color = new Vector4f(0.5F, 0.3F, 0.3F, 1.0F);
   public Vector4f color2 = new Vector4f(1.3F, 1.8F, 1.0F, 1.0F);
   public float time;
   public float seed;
   public int resolution;
   public boolean needsFieldUpdate = true;
   public float rotSecA;
   public float rotSecB;
   public float rotSecC;

   public void onExit() {
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderVector4f(var1, "viewport", (float)Controller.viewport.get(0), (float)Controller.viewport.get(1), (float)Controller.viewport.get(2), (float)Controller.viewport.get(3));
      GlUtil.updateShaderVector2f(var1, "resolution", (float)this.resolution, (float)this.resolution);
      GlUtil.updateShaderVector2f(var1, "invResolution", 1.0F / (float)this.resolution, 1.0F / (float)this.resolution);
      GlUtil.updateShaderFloat(var1, "time", this.seed);
      GlUtil.updateShaderInt(var1, "fieldIteration", 30);
      GlUtil.updateShaderInt(var1, "field2Iteration", 24);
      GlUtil.updateShaderVector4f(var1, "color", this.color);
      GlUtil.updateShaderVector4f(var1, "color2", this.color2);
      if (var1.recompiled) {
         var1.recompiled = false;
      }

   }

   public boolean isRecompiled() {
      return ShaderLibrary.fieldShader.recompiled;
   }
}
