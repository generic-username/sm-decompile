package org.schema.game.client.view.space;

import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.graphicsengine.texture.Texture;

public class StarSkyBackgroundShader implements Shaderable {
   private static FloatBuffer projBuffer = BufferUtils.createFloatBuffer(16);
   private static FloatBuffer modelBuffer = BufferUtils.createFloatBuffer(16);
   private static FloatBuffer mvpBuffer = BufferUtils.createFloatBuffer(16);
   private Texture cubeMapTexture;

   public StarSkyBackgroundShader() {
      this.loadBackgroundMap();
   }

   public Texture getCubeMapTexture() {
      return this.cubeMapTexture;
   }

   public void setCubeMapTexture(Texture var1) {
      this.cubeMapTexture = var1;
   }

   private void loadBackgroundMap() {
      assert GameResourceLoader.skyTexture != null;

      this.setCubeMapTexture(GameResourceLoader.skyTexture);
   }

   public void onExit() {
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderInt(var1, "CubeMapTex", 0);
      GlUtil.updateShaderVector4f(var1, "MaterialColor", 1.0F, 1.0F, 1.0F, 1.0F);
   }
}
