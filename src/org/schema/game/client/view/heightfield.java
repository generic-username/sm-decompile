package org.schema.game.client.view;

public class heightfield {
   heightfield.Stats stats = new heightfield.Stats();
   int m_size = 0;
   int m_log_size = 0;
   int root_level;
   float sample_spacing = 1.0F;
   float vertical_scale;
   float input_vertical_scale;
   heightfield.hf m_bt;
   heightfield.hl m_level;

   heightfield(float var1, float var2) {
      this.vertical_scale = var1;
      this.input_vertical_scale = var2;
      this.m_bt = null;
      this.m_level = null;
   }

   static int lowest_one(int var0) {
      int var1;
      for(var1 = 0; var1 < 32 && (var0 & 1) == 0; var0 >>= 1) {
         ++var1;
      }

      return var1;
   }

   void activate(int var1, int var2, int var3) {
      assert var3 < 15;

      int var4 = this.get_level(var1, var2);
      if (var3 > var4) {
         if (var4 == -1) {
            ++this.stats.output_vertices;
         }

         this.set_level(var1, var2, var3);
      }

   }

   void clear() {
      if (this.m_level != null) {
         this.m_level = null;
      }

      this.m_size = 0;
      this.m_log_size = 0;
   }

   int get_level(int var1, int var2) {
      var2 = this.m_level.get(var2, var1 >> 1);
      if ((var1 & 1) != 0) {
         var2 >>= 4;
      }

      return (var2 &= 15) == 15 ? -1 : var2;
   }

   int height(int var1, int var2) {
      return (int)(this.m_bt.get_sample(var1, var2) * this.input_vertical_scale / this.vertical_scale);
   }

   int iclamp(int var1, int var2, int var3) {
      return Math.max(var2, Math.min(var1, var3));
   }

   int minimum_edge_lod(int var1) {
      var1 = lowest_one(var1);
      var1 = this.m_log_size - var1 - 1;
      return this.iclamp(this.root_level - var1, 0, this.root_level);
   }

   int node_index(int var1, int var2) {
      if (var1 >= 0 && var1 < this.m_size && var2 >= 0 && var2 < this.m_size) {
         int var3 = lowest_one(var1 | var2);
         int var4 = this.m_log_size - var3 - 1;
         int var5 = 1431655765 & (1 << (var4 << 1)) - 1;
         ++var3;
         var1 >>= var3;
         var2 >>= var3;
         return var5 + (var2 << var4) + var1;
      } else {
         return -1;
      }
   }

   void set_level(int var1, int var2, int var3) {
      assert var3 >= -1 && var3 < 15;

      var3 &= 15;
      int var4 = this.m_level.get(var2, var1 >> 1);
      if ((var1 & 1) != 0) {
         var4 = var4 & 15 | var3 << 4;
      } else {
         var4 = var4 & 240 | var3;
      }

      this.m_level.set(var2, var1 >> 1, var4);
   }

   public class Stats {
      int input_vertices;
      int output_vertices;
      int output_real_triangles;
      int output_degenerate_triangles;
      int output_chunks;
      int output_size;
   }

   public class hl {
      public int get(int var1, int var2) {
         return 0;
      }

      public void set(int var1, int var2, int var3) {
      }
   }

   public class hf {
      public float get_sample(int var1, int var2) {
         return 0.0F;
      }
   }
}
