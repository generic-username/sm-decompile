package org.schema.game.client.view.effects;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.game.client.view.tools.ColorTools;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.missile.ClientMissileManager;
import org.schema.game.common.data.missile.Missile;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.particle.ParticleController;

public class MissileHeadEffectController extends ParticleController {
   private Vector3f p = new Vector3f();
   private Vector3f tmp = new Vector3f();
   private ClientMissileManager missileController;

   public MissileHeadEffectController(boolean var1, ClientMissileManager var2) {
      super(var1, MissileTrailDrawer.MAX_TRAILS << 1);
      this.missileController = var2;
   }

   public void add(Missile var1) {
      int var2 = this.addParticle(var1.getWorldTransformOnClient().origin, this.tmp);
      ((MissileHeadParticleContainer)this.getParticles()).setId(var2, var1.getId());
      ElementInformation var4;
      if (ElementKeyMap.isValidType(var1.getColorType()) && (var4 = ElementKeyMap.getInfo(var1.getColorType())).isLightSource()) {
         Vector4f var3;
         (var3 = new Vector4f()).set(var4.getLightSourceColor());
         ColorTools.brighten(var3);
         ((MissileHeadParticleContainer)this.getParticles()).setColor(var2, var3);
      } else {
         ((MissileHeadParticleContainer)this.getParticles()).setColor(var2, 1.0F, 1.0F, 1.0F, 1.0F);
      }
   }

   public boolean updateParticle(int var1, Timer var2) {
      Missile var3;
      if ((var3 = this.missileController.getMissile((short)((MissileHeadParticleContainer)this.getParticles()).getId(var1))) != null && var3.isAlive()) {
         this.p.set(var3.getWorldTransformOnClient().origin);
         ((MissileHeadParticleContainer)this.getParticles()).setPos(var1, this.p.x, this.p.y, this.p.z);
         return true;
      } else {
         return false;
      }
   }

   protected MissileHeadParticleContainer getParticleInstance(int var1) {
      return new MissileHeadParticleContainer(var1);
   }

   public int addParticle(Vector3f var1, Vector3f var2) {
      if (this.particlePointer >= ((MissileHeadParticleContainer)this.getParticles()).getCapacity() - 1) {
         ((MissileHeadParticleContainer)this.getParticles()).growCapacity();
      }

      int var3 = this.idGen++;
      int var4;
      if (this.isOrderedDelete()) {
         var4 = this.particlePointer % ((MissileHeadParticleContainer)this.getParticles()).getCapacity();
         ((MissileHeadParticleContainer)this.getParticles()).setPos(var4, var1.x, var1.y, var1.z);
         ((MissileHeadParticleContainer)this.getParticles()).setStart(var4, var1.x, var1.y, var1.z);
         ((MissileHeadParticleContainer)this.getParticles()).setVelocity(var4, var2.x, var2.y, var2.z);
         ((MissileHeadParticleContainer)this.getParticles()).setLifetime(var4, 0.0F);
         ((MissileHeadParticleContainer)this.getParticles()).setId(var4, var3);
         ++this.particlePointer;
         return var4;
      } else {
         var4 = this.particlePointer % ((MissileHeadParticleContainer)this.getParticles()).getCapacity();
         ((MissileHeadParticleContainer)this.getParticles()).setPos(var4, var1.x, var1.y, var1.z);
         ((MissileHeadParticleContainer)this.getParticles()).setStart(var4, var1.x, var1.y, var1.z);
         ((MissileHeadParticleContainer)this.getParticles()).setVelocity(var4, var2.x, var2.y, var2.z);
         ((MissileHeadParticleContainer)this.getParticles()).setLifetime(var4, 0.0F);
         ((MissileHeadParticleContainer)this.getParticles()).setId(var4, var3);
         ++this.particlePointer;
         return var4;
      }
   }
}
