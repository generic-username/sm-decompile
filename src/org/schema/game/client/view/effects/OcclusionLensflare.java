package org.schema.game.client.view.effects;

import java.util.ArrayList;
import java.util.Collection;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.forms.PositionableSubColorSprite;
import org.schema.schine.graphicsengine.forms.Sprite;

public class OcclusionLensflare implements Drawable {
   public float sizeMult = 1.0F;
   public float extraScale = 6.0F;
   public boolean drawLensFlareEffects = true;
   public boolean depthTest;
   Vector3f camPos = new Vector3f();
   Vector3f m_LightSourcePos = new Vector3f();
   Vector3f vLightSourceToCamera = new Vector3f();
   Vector3f ptIntersect = new Vector3f();
   Vector3f vLightSourceToIntersect = new Vector3f();
   Vector3f pt = new Vector3f();
   ArrayList elements = new ArrayList(16);
   float length;
   private Sprite sprite;
   private int pointer;
   private Vector4f mainColor = new Vector4f();

   public OcclusionLensflare() {
      for(int var1 = 0; var1 < 16; ++var1) {
         this.elements.add(new OcclusionLensflare.FlareElement());
      }

   }

   public void cleanUp() {
   }

   public void draw() {
      assert this.sprite != null;

      this.pointer = 0;
      this.m_LightSourcePos.set(this.getLightPos());
      this.camPos.set(Controller.getCamera().getPos());
      this.vLightSourceToCamera.sub(this.camPos, this.m_LightSourcePos);
      this.length = this.vLightSourceToCamera.length();
      this.ptIntersect.scale(this.length, Controller.getCamera().getForward());
      this.ptIntersect.add(this.camPos);
      this.vLightSourceToIntersect.sub(this.ptIntersect, this.m_LightSourcePos);
      this.length = this.vLightSourceToIntersect.length();
      this.vLightSourceToIntersect.normalize();
      if (this.mainColor.x == 1.0F && this.mainColor.y == 1.0F && this.mainColor.z == 1.0F) {
         this.RenderBigGlow(0.6F, 0.6F, 0.7F, this.mainColor.w, this.m_LightSourcePos, 16.0F * this.sizeMult);
         this.RenderStreaks(0.6F, 0.6F, 0.8F, 1.0F, this.m_LightSourcePos, 16.0F * this.sizeMult);
      } else {
         this.RenderBigGlow(this.mainColor.x * 0.8F, this.mainColor.y * 0.8F, this.mainColor.z * 0.8F, this.mainColor.w, this.m_LightSourcePos, 16.0F * this.sizeMult);
         this.RenderStreaks(this.mainColor.x * 0.8F, this.mainColor.y * 0.8F, this.mainColor.z * 0.8F, 1.0F, this.m_LightSourcePos, 16.0F * this.sizeMult);
      }

      this.RenderGlow(0.8F, 0.8F, 1.0F, 0.9F, this.m_LightSourcePos, 3.5F * this.sizeMult);
      if (this.drawLensFlareEffects) {
         this.length *= 2.0F;
         this.pt.scale(this.length * 0.1F, this.vLightSourceToIntersect);
         this.pt.add(this.m_LightSourcePos);
         this.RenderGlow(0.9F, 0.6F, 0.4F, 0.5F, this.pt, 0.6F);
         this.pt.scale(this.length * 0.15F, this.vLightSourceToIntersect);
         this.pt.add(this.m_LightSourcePos);
         this.RenderHalo(0.8F, 0.5F, 0.6F, 0.5F, this.pt, 1.7F);
         this.pt.scale(this.length * 0.175F, this.vLightSourceToIntersect);
         this.pt.add(this.m_LightSourcePos);
         this.RenderHalo(0.9F, 0.2F, 0.1F, 0.5F, this.pt, 0.83F);
         this.pt.scale(this.length * 0.285F, this.vLightSourceToIntersect);
         this.pt.add(this.m_LightSourcePos);
         this.RenderHalo(0.7F, 0.7F, 0.4F, 0.5F, this.pt, 1.6F);
         this.pt.scale(this.length * 0.2755F, this.vLightSourceToIntersect);
         this.pt.add(this.m_LightSourcePos);
         this.RenderGlow(0.9F, 0.9F, 0.2F, 0.5F, this.pt, 0.8F);
         this.pt.scale(this.length * 0.4775F, this.vLightSourceToIntersect);
         this.pt.add(this.m_LightSourcePos);
         this.RenderGlow(0.93F, 0.82F, 0.73F, 0.5F, this.pt, 1.0F);
         this.pt.scale(this.length * 0.49F, this.vLightSourceToIntersect);
         this.pt.add(this.m_LightSourcePos);
         this.RenderHalo(0.7F, 0.6F, 0.5F, 0.5F, this.pt, 1.4F);
         this.pt.scale(this.length * 0.65F, this.vLightSourceToIntersect);
         this.pt.add(this.m_LightSourcePos);
         this.RenderGlow(0.7F, 0.8F, 0.3F, 0.5F, this.pt, 1.8F);
         this.pt.scale(this.length * 0.63F, this.vLightSourceToIntersect);
         this.pt.add(this.m_LightSourcePos);
         this.RenderGlow(0.4F, 0.3F, 0.2F, 0.5F, this.pt, 1.4F);
         this.pt.scale(this.length * 0.8F, this.vLightSourceToIntersect);
         this.pt.add(this.m_LightSourcePos);
         this.RenderHalo(0.7F, 0.5F, 0.5F, 0.5F, this.pt, 1.4F);
         this.pt.scale(this.length * 0.7825F, this.vLightSourceToIntersect);
         this.pt.add(this.m_LightSourcePos);
         this.RenderGlow(0.8F, 0.5F, 0.1F, 0.5F, this.pt, 0.6F);
         this.pt.scale(this.length, this.vLightSourceToIntersect);
         this.pt.add(this.m_LightSourcePos);
         this.RenderHalo(0.5F, 0.5F, 0.7F, 0.5F, this.pt, 1.7F);
         this.pt.scale(this.length * 0.975F, this.vLightSourceToIntersect);
         this.pt.add(this.m_LightSourcePos);
         this.RenderGlow(0.4F, 0.1F, 0.9F, 0.5F, this.pt, 2.0F);
      } else {
         for(int var1 = this.pointer; var1 < this.elements.size(); ++var1) {
            ((OcclusionLensflare.FlareElement)this.elements.get(var1)).canDraw = false;
         }
      }

      this.sprite.blendFunc = 1;
      this.sprite.setDepthTest(this.depthTest);
      Sprite.draw3D(this.sprite, (Collection)this.elements, Controller.getCamera());
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.sprite = Controller.getResLoader().getSprite("lens_flare-4x1-c-");

      assert this.sprite != null;

   }

   public Vector3f getLightPos() {
      return AbstractScene.mainLight.getPos();
   }

   private OcclusionLensflare.FlareElement flare(float var1, float var2, float var3, float var4, Vector3f var5, float var6) {
      OcclusionLensflare.FlareElement var7;
      (var7 = (OcclusionLensflare.FlareElement)this.elements.get(this.pointer++)).canDraw = true;
      var7.pos.set(var5);
      var7.scale = var6 * this.extraScale;
      var7.color.set(var1, var2, var3, var4);
      return var7;
   }

   private void RenderBigGlow(float var1, float var2, float var3, float var4, Vector3f var5, float var6) {
      this.flare(var1, var2, var3, var4, var5, var6).subSprite = 3;
   }

   private void RenderGlow(float var1, float var2, float var3, float var4, Vector3f var5, float var6) {
      this.flare(var1, var2, var3, var4, var5, var6).subSprite = 1;
   }

   private void RenderHalo(float var1, float var2, float var3, float var4, Vector3f var5, float var6) {
      this.flare(var1, var2, var3, var4, var5, var6).subSprite = 0;
   }

   private void RenderStreaks(float var1, float var2, float var3, float var4, Vector3f var5, float var6) {
      this.flare(var1, var2, var3, var4, var5, var6).subSprite = 2;
   }

   public void setFillRate(float var1) {
   }

   public void setMainColor(Vector4f var1) {
      this.mainColor.set(var1);
   }

   class FlareElement implements PositionableSubColorSprite {
      public Vector4f color;
      Vector3f pos;
      float scale;
      int subSprite;
      private boolean canDraw;

      private FlareElement() {
         this.color = new Vector4f();
         this.pos = new Vector3f();
         this.subSprite = 0;
         this.canDraw = true;
      }

      public Vector4f getColor() {
         return this.color;
      }

      public Vector3f getPos() {
         return this.pos;
      }

      public float getScale(long var1) {
         return this.scale;
      }

      public int getSubSprite(Sprite var1) {
         return this.subSprite;
      }

      public boolean canDraw() {
         return this.canDraw;
      }

      // $FF: synthetic method
      FlareElement(Object var2) {
         this();
      }
   }
}
