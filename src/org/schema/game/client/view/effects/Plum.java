package org.schema.game.client.view.effects;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.graphicsengine.core.Controller;

public class Plum implements Comparable {
   private static final Vector3f tV = new Vector3f();
   private static final Vector3f tO = new Vector3f();
   private static Vector3f helper = new Vector3f();
   private static Transform t = new Transform();
   private static Vector3f localTranslation = new Vector3f(-50.0F, 0.0F, 0.0F);
   private SegmentController controller;
   private Vector3i position;

   public int compareTo(Plum var1) {
      if (var1 == null) {
         return 1;
      } else if (var1.hashCode() == this.hashCode()) {
         return 0;
      } else {
         this.getWorldTransform(t, localTranslation);
         tV.set(t.origin);
         var1.getWorldTransform(t, localTranslation);
         tO.set(t.origin);
         tV.sub(Controller.getCamera().getPos());
         tO.sub(Controller.getCamera().getPos());
         int var2;
         return (var2 = (int)(tV.length() * 10000.0F - tO.length() * 10000.0F)) == 0 ? 1 : var2;
      }
   }

   public void getWorldTransform(Transform var1, Vector3f var2) {
      t.set(this.controller.getWorldTransformOnClient());
      helper.set((float)(this.position.x - 16) + var2.x, (float)(this.position.y - 16) + var2.y, (float)(this.position.z - 16) + var2.z);
      t.basis.transform(helper);
      t.origin.add(helper);
      var1.set(t);
   }

   public int hashCode() {
      return this.position.hashCode() + this.controller.hashCode();
   }

   public boolean equals(Object var1) {
      return this.position.equals(((Plum)var1).position) && this.controller == ((Plum)var1).controller;
   }

   public void reset() {
      this.controller = null;
      this.position = null;
   }

   public void set(SegmentController var1, Vector3i var2) {
      this.controller = var1;
      this.position = var2;
   }
}
