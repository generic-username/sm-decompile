package org.schema.game.client.view.effects;

import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.linearmath.Transform;
import java.util.ArrayList;
import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.lift.LiftUnit;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.texture.Material;
import org.schema.schine.network.objects.Sendable;

public class LiftDrawer implements Drawable {
   private final ArrayList activeUnits = new ArrayList();
   private boolean init;
   private Mesh box;
   private Transform wt = new Transform();
   private Vector3f half = new Vector3f();
   private Material material;

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      for(int var1 = 0; var1 < this.activeUnits.size(); ++var1) {
         SegmentController var2 = ((LiftUnit)this.activeUnits.get(var1)).getSegmentController();
         if (((LiftUnit)this.activeUnits.get(var1)).isActive() && var2.getSectorId() == ((GameClientState)var2.getState()).getCurrentSectorId() && ((GameClientState)var2.getState()).getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().containsKey(var2.getId())) {
            LiftUnit var3 = (LiftUnit)this.activeUnits.get(var1);
            ShaderLibrary.perpixelShader.loadWithoutUpdate();
            this.material.attach(0);
            if (var3.getBody() != null) {
               Integer var4 = (Integer)var3.getBody().getUserPointer();
               Sendable var5;
               if ((var5 = (Sendable)var2.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var4)) == null || !(var5 instanceof SegmentController)) {
                  System.err.println("[CLIENT][ERROR] lift cannot be drawn: objectID " + var4 + " -> " + var5);
                  ((LiftUnit)this.activeUnits.get(var1)).deactivate();
                  this.activeUnits.remove(var1);
                  continue;
               }

               if (((SegmentController)var5).getSectorId() != ((GameClientState)var2.getState()).getCurrentSectorId()) {
                  ((LiftUnit)this.activeUnits.get(var1)).deactivate();
                  this.activeUnits.remove(var1);
                  --var1;
                  continue;
               }

               var3.getBody().getWorldTransform(this.wt);
               GlUtil.glPushMatrix();
               GlUtil.glMultMatrix(this.wt);
               ((BoxShape)var3.getBody().getCollisionShape()).getHalfExtentsWithoutMargin(this.half);
               GlUtil.scaleModelview(this.half.x * 2.0F, this.half.y * 2.0F, this.half.z * 2.0F);
               this.box.draw();
               GlUtil.glPopMatrix();
            }

            this.material.detach();
            ShaderLibrary.perpixelShader.unloadWithoutExit();
         } else {
            ((LiftUnit)this.activeUnits.get(var1)).deactivate();
            this.activeUnits.remove(var1);
            --var1;
         }
      }

   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.box = (Mesh)Controller.getResLoader().getMesh("Box").getChilds().get(0);
      this.material = new Material();
      this.material.setShininess(64.0F);
      this.material.setSpecular(new float[]{1.3F, 1.3F, 1.3F, 1.0F});
      this.init = true;
   }

   public void updateActivate(LiftUnit var1, boolean var2) {
      if (var2) {
         this.activeUnits.add(var1);
      }

   }
}
