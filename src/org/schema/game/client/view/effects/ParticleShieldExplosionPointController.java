package org.schema.game.client.view.effects;

import java.util.Random;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.particle.ParticleController;

public class ParticleShieldExplosionPointController extends ParticleController {
   private static final float MAX_LIFETIME = 3.0F;
   Vector3f dir = new Vector3f();
   private Vector3f velocityHelper = new Vector3f();
   private Vector4f colorHelper = new Vector4f();
   private Vector3f posHelper = new Vector3f();
   private Vector3f startHelper = new Vector3f();
   private Vector3f posBeforeUpdate = new Vector3f();
   private Vector3f velocityBefore = new Vector3f();
   private float explosionSpeed = 0.003F;
   float time;
   private static Random r = new Random();

   public ParticleShieldExplosionPointController(boolean var1) {
      super(var1);
   }

   public void addExplosion(Vector3f var1, Vector3f var2, float var3) {
      this.dir.set(0.0F, 0.0F, 0.0F);
      int var4 = this.addParticle(var1, this.dir);
      ((ShieldHitParticleContainer)this.getParticles()).setDamage(var4, var3);
      ((ShieldHitParticleContainer)this.getParticles()).setImpactForce(var4, 6.51F);
      ((ShieldHitParticleContainer)this.getParticles()).setColor(var4, 1.0F, 1.0F, 1.0F, 1.0F);
   }

   public void update(Timer var1) {
      super.update(var1);
      this.time += var1.getDelta();
      if (this.time > 1.0F) {
         this.time -= (float)((int)this.time);
      }

   }

   public boolean updateParticle(int var1, Timer var2) {
      float var3 = ((ShieldHitParticleContainer)this.getParticles()).getLifetime(var1);
      float var4 = ((ShieldHitParticleContainer)this.getParticles()).getImpactForce(var1);
      ((ShieldHitParticleContainer)this.getParticles()).setLifetime(var1, var3 + var2.getDelta());
      return var3 < var4;
   }

   protected ShieldHitParticleContainer getParticleInstance(int var1) {
      return new ShieldHitParticleContainer(var1);
   }

   public int addParticle(Vector3f var1, Vector3f var2) {
      if (this.particlePointer >= ((ShieldHitParticleContainer)this.getParticles()).getCapacity() - 1) {
         ((ShieldHitParticleContainer)this.getParticles()).growCapacity();
      }

      int var3 = this.idGen++;
      int var4;
      if (this.isOrderedDelete()) {
         var4 = this.particlePointer % ((ShieldHitParticleContainer)this.getParticles()).getCapacity();
         ((ShieldHitParticleContainer)this.getParticles()).setPos(var4, var1.x, var1.y, var1.z);
         ((ShieldHitParticleContainer)this.getParticles()).setStart(var4, var1.x, var1.y, var1.z);
         ((ShieldHitParticleContainer)this.getParticles()).setVelocity(var4, var2.x, var2.y, var2.z);
         ((ShieldHitParticleContainer)this.getParticles()).setLifetime(var4, 0.0F);
         ((ShieldHitParticleContainer)this.getParticles()).setId(var4, var3);
         ((ShieldHitParticleContainer)this.getParticles()).setBlockHitIndex(var4, 0);
         ((ShieldHitParticleContainer)this.getParticles()).setShotStatus(var4, 0);
         ++this.particlePointer;
         return var4;
      } else {
         var4 = this.particlePointer % ((ShieldHitParticleContainer)this.getParticles()).getCapacity();
         ((ShieldHitParticleContainer)this.getParticles()).setPos(var4, var1.x, var1.y, var1.z);
         ((ShieldHitParticleContainer)this.getParticles()).setStart(var4, var1.x, var1.y, var1.z);
         ((ShieldHitParticleContainer)this.getParticles()).setVelocity(var4, var2.x, var2.y, var2.z);
         ((ShieldHitParticleContainer)this.getParticles()).setLifetime(var4, 0.0F);
         ((ShieldHitParticleContainer)this.getParticles()).setId(var4, var3);
         ((ShieldHitParticleContainer)this.getParticles()).setBlockHitIndex(var4, 0);
         ((ShieldHitParticleContainer)this.getParticles()).setShotStatus(var4, 0);
         ++this.particlePointer;
         return var4;
      }
   }
}
