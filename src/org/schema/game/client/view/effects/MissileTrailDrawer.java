package org.schema.game.client.view.effects;

import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.missile.Missile;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.particle.ParticleController;
import org.schema.schine.graphicsengine.forms.particle.simple.ParticleSimpleDrawer;
import org.schema.schine.graphicsengine.forms.particle.simple.ParticleSimpleDrawerMissile;
import org.schema.schine.graphicsengine.forms.particle.trail.ParticleTrailDrawer;

public class MissileTrailDrawer implements Drawable {
   public static final int MAX_TRAILS;
   private final ObjectArrayFIFOQueue trailsToAddOrRemove = new ObjectArrayFIFOQueue();
   float maxSpeed = 0.2F;
   Vector3f lasPos = new Vector3f();
   private MissileTrailController[] particleSystem;
   private ParticleTrailDrawer particleSystemDrawer;
   private MissileHeadEffectController particleMissileHeadController;
   private ParticleSimpleDrawer particleMissileDrawer;
   private boolean firstDraw = true;
   private GameClientState state;

   public MissileTrailDrawer(GameClientState var1) {
      this.state = var1;
      this.particleSystem = new MissileTrailController[MAX_TRAILS];

      for(int var2 = 0; var2 < MAX_TRAILS; ++var2) {
         this.particleSystem[var2] = new MissileTrailController((Missile)null, this);
      }

      this.particleSystemDrawer = new ParticleTrailDrawer((ParticleController)null);
      this.particleMissileHeadController = new MissileHeadEffectController(false, var1.getController().getClientMissileManager());
      this.particleMissileDrawer = new ParticleSimpleDrawerMissile(this.particleMissileHeadController, 16.0F);
   }

   private void addTrail(Missile var1) {
      for(int var2 = 0; var2 < MAX_TRAILS; ++var2) {
         if (!this.particleSystem[var2].isAlive()) {
            this.particleSystem[var2].startTrail(var1);
            this.particleMissileHeadController.add(var1);
            return;
         }
      }

   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      this.particleMissileDrawer.draw();

      for(int var1 = 0; var1 < MAX_TRAILS; ++var1) {
         if (this.particleSystem[var1].isAlive()) {
            this.particleSystemDrawer.setParticleController(this.particleSystem[var1]);
            this.particleSystemDrawer.draw();
         }
      }

   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.particleSystemDrawer.onInit();
      this.firstDraw = false;
   }

   public void endTrail(Missile var1) {
      synchronized(this.trailsToAddOrRemove) {
         this.trailsToAddOrRemove.enqueue(new MissileTrailDrawer.AddRemoveTrail(var1, 1));
      }
   }

   private void removeTrail(Missile var1) {
      for(int var2 = 0; var2 < MAX_TRAILS; ++var2) {
         if (this.particleSystem[var2].isAlive() && !this.particleSystem[var2].letItEnd && this.particleSystem[var2].getMissile() == var1) {
            this.particleSystem[var2].endTrail();
            this.state.getWorldDrawer().getExplosionDrawer().addExplosion(this.particleSystem[var2].getMissile().getWorldTransformOnClient().origin, 50.0F, this.particleSystem[var2].getMissile().getWeaponId());
            return;
         }
      }

   }

   public void startTrail(Missile var1) {
      synchronized(this.trailsToAddOrRemove) {
         this.trailsToAddOrRemove.enqueue(new MissileTrailDrawer.AddRemoveTrail(var1, 0));
      }
   }

   public void translateTrail(Missile var1) {
      synchronized(this.trailsToAddOrRemove) {
         this.trailsToAddOrRemove.enqueue(new MissileTrailDrawer.AddRemoveTrail(var1, 2));
      }
   }

   public void update(Timer var1) {
      synchronized(this.trailsToAddOrRemove) {
         while(!this.trailsToAddOrRemove.isEmpty()) {
            MissileTrailDrawer.AddRemoveTrail var3;
            if ((var3 = (MissileTrailDrawer.AddRemoveTrail)this.trailsToAddOrRemove.dequeue()).mode == 0) {
               this.addTrail(var3.t);
            } else if (var3.mode == 1) {
               this.removeTrail(var3.t);
            } else if (var3.mode != 2) {
               throw new IllegalArgumentException("Unknown mode: " + var3.mode);
            }
         }
      }

      for(int var5 = 0; var5 < MAX_TRAILS; ++var5) {
         if (this.particleSystem[var5].isAlive()) {
            this.particleSystem[var5].update(var1);
         }
      }

      this.lasPos.set(Controller.getCamera().getPos());
      if (this.particleSystemDrawer != null) {
         this.particleSystemDrawer.update(var1);
      }

      this.particleMissileHeadController.update(var1);
   }

   static {
      MAX_TRAILS = (Integer)EngineSettings.G_MAX_MISSILE_TRAILS.getCurrentState();
   }

   class AddRemoveTrail {
      public static final int ADD = 0;
      public static final int REMOVE = 1;
      public static final int TRANSLATE = 2;
      Missile t;
      int mode;

      public AddRemoveTrail(Missile var2, int var3) {
         this.t = var2;
         this.mode = var3;
      }
   }
}
