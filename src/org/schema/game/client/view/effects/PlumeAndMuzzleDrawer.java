package org.schema.game.client.view.effects;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import java.util.Iterator;
import java.util.Map;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.client.view.MainGameGraphics;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.SilhouetteShaderAlpha;
import org.schema.schine.graphicsengine.texture.textureImp.Texture3D;

public class PlumeAndMuzzleDrawer implements BloomEffectInterface, Drawable {
   static Mesh plumeMesh;
   static Texture3D tex;
   private final ObjectArrayFIFOQueue toAddPlumes = new ObjectArrayFIFOQueue();
   private final ObjectArrayFIFOQueue toAddMuzzle = new ObjectArrayFIFOQueue();
   private final Map plumeDrawers = new Object2ObjectOpenHashMap();
   private final Map muzzleDrawers = new Object2ObjectOpenHashMap();
   private SilhouetteShaderAlpha silhouetteShaderAlpha;
   private boolean firstDraw = true;

   public void addMuzzle(MuzzleFlash var1) {
      this.toAddMuzzle.enqueue(var1);
   }

   public void addPlume(ExhaustPlumes var1) {
      this.toAddPlumes.enqueue(var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      if (!MainGameGraphics.drawBloomedEffects()) {
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         GlUtil.glEnable(32879);
         Shader var1;
         (var1 = ShaderLibrary.exaustShader).loadWithoutUpdate();
         GlUtil.glBindTexture(32879, tex.getId());
         GlUtil.updateShaderInt(var1, "noiseTex", 0);
         ((Mesh)plumeMesh.getChilds().get(0)).loadMeshPointers();
         Iterator var2 = this.plumeDrawers.values().iterator();

         while(var2.hasNext()) {
            ExhaustPlumes var3;
            (var3 = (ExhaustPlumes)var2.next()).raw = false;
            var3.draw();
         }

         GlUtil.updateShaderInt(var1, "noiseTex", 0);
         var2 = this.muzzleDrawers.values().iterator();

         while(var2.hasNext()) {
            MuzzleFlash var4;
            (var4 = (MuzzleFlash)var2.next()).raw = false;
            var4.draw();
         }

         ((Mesh)plumeMesh.getChilds().get(0)).unloadMeshPointers();
         GlUtil.glDisable(32879);
         GlUtil.glDisable(3042);
         var1.unloadWithoutExit();
      }
   }

   public void drawRaw() {
      if (this.firstDraw) {
         this.onInit();
      }

      this.silhouetteShaderAlpha.color.set(1.0F, 1.0F, 1.0F, 1.0F);
      ShaderLibrary.silhouetteAlpha.setShaderInterface(this.silhouetteShaderAlpha);
      ShaderLibrary.silhouetteAlpha.load();
      ((Mesh)plumeMesh.getChilds().get(0)).loadMeshPointers();

      Iterator var1;
      ExhaustPlumes var2;
      for(var1 = this.plumeDrawers.values().iterator(); var1.hasNext(); var2.raw = false) {
         (var2 = (ExhaustPlumes)var1.next()).raw = true;
         var2.draw();
      }

      MuzzleFlash var3;
      for(var1 = this.muzzleDrawers.values().iterator(); var1.hasNext(); var3.raw = false) {
         (var3 = (MuzzleFlash)var1.next()).raw = true;
         var3.draw();
      }

      ((Mesh)plumeMesh.getChilds().get(0)).unloadMeshPointers();
      ShaderLibrary.silhouetteAlpha.unload();
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      if (plumeMesh == null) {
         plumeMesh = Controller.getResLoader().getMesh("ExhaustPlum");
      }

      if (this.silhouetteShaderAlpha == null) {
         this.silhouetteShaderAlpha = new SilhouetteShaderAlpha();
      }

      if (tex == null) {
         tex = GameResourceLoader.noiseVolume;
      }

      this.firstDraw = false;
   }

   public void clear() {
      Iterator var1 = this.muzzleDrawers.values().iterator();

      while(var1.hasNext()) {
         ((MuzzleFlash)var1.next()).cleanUp();
      }

      this.muzzleDrawers.clear();
      var1 = this.plumeDrawers.values().iterator();

      while(var1.hasNext()) {
         ((ExhaustPlumes)var1.next()).cleanUp();
      }

      this.plumeDrawers.clear();
   }

   public void scheduleUpdatePlums() {
      Iterator var1 = this.plumeDrawers.values().iterator();

      while(var1.hasNext()) {
         ((ExhaustPlumes)var1.next()).scheduleUpdate();
      }

   }

   public void update(Timer var1) {
      while(!this.toAddPlumes.isEmpty()) {
         ExhaustPlumes var2 = (ExhaustPlumes)this.toAddPlumes.dequeue();
         this.plumeDrawers.put(var2.getShip(), var2);
      }

      while(!this.toAddMuzzle.isEmpty()) {
         MuzzleFlash var3 = (MuzzleFlash)this.toAddMuzzle.dequeue();
         this.muzzleDrawers.put(var3.getShip(), var3);
      }

      Iterator var4 = this.plumeDrawers.values().iterator();

      while(var4.hasNext()) {
         ((ExhaustPlumes)var4.next()).update(var1);
      }

      var4 = this.muzzleDrawers.values().iterator();

      while(var4.hasNext()) {
         ((MuzzleFlash)var4.next()).update(var1);
      }

   }
}
