package org.schema.game.client.view.effects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.Mesh;

public class ShieldDrawerManager implements Observer, Drawable {
   public static float time;
   static Mesh mesh;
   private static ArrayList shieldDrawerPool = new ArrayList();
   private final Map shieldDrawers = new HashMap();
   private final ArrayList toAdd = new ArrayList();
   private final GameClientState state;
   private int shieldPointer;
   private ShieldDrawer[] toDraw = new ShieldDrawer[128];

   public ShieldDrawerManager(GameClientState var1) {
      this.state = var1;
   }

   private static ShieldDrawer get(ManagedSegmentController var0) {
      if (shieldDrawerPool.isEmpty()) {
         return new ShieldDrawer(var0);
      } else {
         ShieldDrawer var1;
         (var1 = (ShieldDrawer)shieldDrawerPool.remove(0)).set(var0);
         return var1;
      }
   }

   private static void releaseDrawer(ShieldDrawer var0) {
      var0.reset();
      shieldDrawerPool.add(var0);
   }

   public void add(ManagedSegmentController var1) {
      this.toAdd.add(get(var1));
   }

   public void cleanUp() {
   }

   public void draw() {
      if (EngineSettings.G_DRAW_SHIELDS.isOn()) {
         ;
      }
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      mesh = (Mesh)Controller.getResLoader().getMesh("Sphere").getChilds().get(0);
   }

   public void clear() {
      Iterator var1 = this.shieldDrawers.values().iterator();

      while(var1.hasNext()) {
         ShieldDrawer var2;
         (var2 = (ShieldDrawer)var1.next()).deleteObserver(this);
         releaseDrawer(var2);
      }

      this.shieldPointer = 0;
      this.shieldDrawers.clear();
   }

   public ShieldDrawer get(SegmentController var1) {
      return (ShieldDrawer)this.shieldDrawers.get(var1);
   }

   public GameClientState getState() {
      return this.state;
   }

   public void update(Observable var1, Object var2) {
      if ((Boolean)var2) {
         boolean var5 = true;

         for(int var3 = 0; var3 < this.toDraw.length && var3 < this.shieldPointer; ++var3) {
            if (this.toDraw[var3] == var1) {
               var5 = false;
               break;
            }
         }

         if (var5 && this.shieldPointer < this.toDraw.length) {
            this.toDraw[this.shieldPointer] = (ShieldDrawer)var1;
            ++this.shieldPointer;
         }

      } else {
         if (this.shieldPointer < this.toDraw.length) {
            for(int var4 = 0; var4 < this.toDraw.length; ++var4) {
               if (this.toDraw[var4] == (ShieldDrawer)var1) {
                  this.toDraw[var4] = this.toDraw[this.shieldPointer - 1];
                  --this.shieldPointer;
                  return;
               }
            }
         }

      }
   }

   public void update(Timer var1) {
      if ((time += var1.getDelta()) > 1.0F) {
         time -= (float)((int)time);
      }

      while(!this.toAdd.isEmpty()) {
         ShieldDrawer var2;
         (var2 = (ShieldDrawer)this.toAdd.remove(0)).addObserver(this);
         this.shieldDrawers.put(var2.controller, var2);
      }

      for(int var3 = 0; var3 < this.shieldPointer; ++var3) {
         this.toDraw[var3].update(var1);
      }

   }
}
