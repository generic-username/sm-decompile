package org.schema.game.client.view.effects;

import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.particle.explosion.particle.ParticleExplosionController;
import org.schema.schine.graphicsengine.forms.particle.explosion.particle.ParticleExplosionDrawer;
import org.schema.schine.graphicsengine.forms.particle.explosion.point.ParticleExplosionPointController;
import org.schema.schine.graphicsengine.forms.particle.explosion.point.ParticleExplosionPointDrawer;

public class ExplosionDrawer implements Drawable {
   private ParticleExplosionController particleSystemSprite = new ParticleExplosionController(false);
   private ParticleExplosionDrawer particleSystemDrawerSprite;
   private ParticleExplosionPointController particleSystemBall;
   private ParticleExplosionPointDrawer particleSystemDrawerBall;
   private ParticleShieldExplosionPointController particleSystemShieldBall;
   private ParticleShieldExplosionPointDrawer particleSystemDrawerShieldBall;
   private boolean firstDraw = true;

   public ExplosionDrawer() {
      this.particleSystemDrawerSprite = new ParticleExplosionDrawer(this.particleSystemSprite);
      this.particleSystemBall = new ParticleExplosionPointController(false);
      this.particleSystemDrawerBall = new ParticleExplosionPointDrawer(this.particleSystemBall);
      this.particleSystemShieldBall = new ParticleShieldExplosionPointController(false);
      this.particleSystemDrawerShieldBall = new ParticleShieldExplosionPointDrawer(this.particleSystemShieldBall);
   }

   public void addExplosion(Vector3f var1) {
      this.particleSystemSprite.addExplosion(new Vector3f(var1), Controller.getCamera().getPos(), 4.0F, 3, Long.MIN_VALUE);
      this.particleSystemBall.addExplosion(new Vector3f(var1), Controller.getCamera().getPos(), 4.0F, 3, 4.0F, Long.MIN_VALUE);
   }

   public void addExplosion(Vector3f var1, float var2) {
      this.particleSystemSprite.addExplosion(new Vector3f(var1), Controller.getCamera().getPos(), var2, 3, Long.MIN_VALUE);
      this.particleSystemBall.addExplosion(new Vector3f(var1), Controller.getCamera().getPos(), var2, 3, 4.0F, Long.MIN_VALUE);
   }

   public void addExplosion(Vector3f var1, float var2, int var3, float var4) {
      this.particleSystemSprite.addExplosion(new Vector3f(var1), Controller.getCamera().getPos(), var2, var3, Long.MIN_VALUE);
      this.particleSystemBall.addExplosion(new Vector3f(var1), Controller.getCamera().getPos(), var2, var3, var4, Long.MIN_VALUE);
   }

   public void addShieldBubbleHit(Vector3f var1, float var2) {
      this.particleSystemShieldBall.addExplosion(var1, Controller.getCamera().getPos(), var2);
   }

   public void addExplosion(Vector3f var1, float var2, long var3) {
      this.particleSystemSprite.addExplosion(new Vector3f(var1), Controller.getCamera().getPos(), var2, 3, var3);
      this.particleSystemBall.addExplosion(new Vector3f(var1), Controller.getCamera().getPos(), var2, 3, 5.0F, var3);
   }

   public void cleanUp() {
   }

   public void draw(FrameBufferObjects var1, FrameBufferObjects var2, DepthBufferScene var3, float var4, float var5) {
      if (this.firstDraw) {
         this.onInit();
      }

      this.particleSystemDrawerSprite.draw(var1, var2, var3.getDepthTextureId(), var4, var5, 3.0F);
   }

   public void drawShieldBubbled() {
      if (this.firstDraw) {
         this.onInit();
      }

      this.particleSystemDrawerShieldBall.draw();
   }

   public void drawPoints() {
      if (this.firstDraw) {
         this.onInit();
      }

      this.particleSystemDrawerBall.draw();
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      this.particleSystemDrawerSprite.draw();
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.particleSystemDrawerSprite.onInit();
      this.firstDraw = false;
   }

   public void update(Timer var1) {
      this.particleSystemBall.update(var1);
      this.particleSystemSprite.update(var1);
      this.particleSystemShieldBall.update(var1);
   }
}
