package org.schema.game.client.view.effects;

import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.Ship;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.particle.simple.ParticleSimpleController;
import org.schema.schine.graphicsengine.forms.particle.simple.SimpleParticleContainer;

public class ShipFlareParticleController extends ParticleSimpleController {
   Vector3f tmp = new Vector3f();

   public ShipFlareParticleController(boolean var1) {
      super(var1, 16);
   }

   public void addFlare(Vector3i var1) {
      this.tmp.set(0.0F, 0.0F, 0.0F);
      int var2 = this.addParticle(this.tmp, this.tmp);
      ((SimpleParticleContainer)this.getParticles()).setPos(var2, (float)(var1.x - Ship.core.x), (float)(var1.y - Ship.core.y), (float)(var1.z - Ship.core.z));
      ((SimpleParticleContainer)this.getParticles()).setStart(var2, (float)var1.x, (float)var1.y, (float)var1.z);
      ((SimpleParticleContainer)this.getParticles()).setColor(var2, 1.0F, 1.0F, 1.0F, 1.0F);
   }

   public void removeFlare(Vector3i var1) {
      for(int var2 = 0; var2 < this.getParticleCount(); ++var2) {
         ((SimpleParticleContainer)this.getParticles()).getStart(var2, this.tmp);
         if (this.tmp.x == (float)var1.x && this.tmp.y == (float)var1.y && this.tmp.z == (float)var1.z) {
            this.deleteParticle(var2);
            System.err.println("FOUND PARTICLE TO DELETE: " + var2);
            return;
         }
      }

   }

   public boolean updateParticle(int var1, Timer var2) {
      return true;
   }
}
