package org.schema.game.client.view.effects;

import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.particle.movingeffect.ParticleMovingEffectController;
import org.schema.schine.graphicsengine.forms.particle.movingeffect.ParticleMovingEffectDrawer;

public class SpaceParticleDrawer implements Drawable {
   protected ParticleMovingEffectController particleSystem = new ParticleMovingEffectController(false);
   protected ParticleMovingEffectDrawer particleSystemDrawer;
   float maxSpeed = 0.2F;
   Vector3f dist = new Vector3f();
   Vector3f lasPos = new Vector3f();
   float distance = 0.0F;
   private boolean firstDraw = true;
   private boolean onPlanet;

   public SpaceParticleDrawer() {
      this.particleSystemDrawer = new ParticleMovingEffectDrawer(this.particleSystem);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (EngineSettings.G_SPACE_PARTICLE.isOn()) {
         if (this.firstDraw) {
            this.onInit();
         }

         this.particleSystemDrawer.draw();
      }

   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.particleSystemDrawer.onInit();
      this.firstDraw = false;
   }

   public void onPlanet(boolean var1) {
      this.onPlanet = var1;
   }

   public void update(Timer var1) {
      if (!this.onPlanet) {
         this.dist.set(Controller.getCamera().getPos());
         this.dist.sub(this.lasPos);
         float var2 = this.dist.length();
         this.distance += var2;
         if (this.distance > 0.05F) {
            this.particleSystem.updateEffectFromCam(Math.min(this.distance, this.maxSpeed), this.maxSpeed);
            this.distance = 0.0F;
         }
      }

      this.particleSystem.update(var1);
      this.lasPos.set(Controller.getCamera().getPos());
   }
}
