package org.schema.game.client.view.effects;

import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectPool;
import it.unimi.dsi.fastutil.objects.ObjectAVLTreeSet;
import it.unimi.dsi.fastutil.objects.ObjectBidirectionalIterator;
import java.util.Iterator;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.MainGameGraphics;
import org.schema.game.client.view.effects.segmentcontrollereffects.JumpStart;
import org.schema.game.client.view.effects.segmentcontrollereffects.RunningEffect;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.thrust.ThrusterCollectionManager;
import org.schema.game.common.controller.elements.thrust.ThrusterUnit;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class ExhaustPlumes implements Drawable, Shaderable {
   static Shader shader;
   static Vector4f color0tStat = new Vector4f();
   static Vector4f color1tStat = new Vector4f();
   private static Matrix3f rot = new Matrix3f();
   Vector4f color0 = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   Vector4f color1 = new Vector4f(0.0F, 0.0F, 1.0F, 1.0F);
   Vector4f color0t = new Vector4f();
   Vector4f color1t = new Vector4f();
   Vector3f helper = new Vector3f();
   Transform t = new Transform();
   Vector3f localTranslation = new Vector3f(0.0F, 0.0F, -0.5F);
   Vector3i test = new Vector3i();
   private Ship ship;
   private ObjectAVLTreeSet unsortedplumes = new ObjectAVLTreeSet();
   private float ticks = 0.0F;
   private boolean initialized;
   private boolean firstDraw = true;
   private long scheduleUpdate = -1L;
   private ObjectPool plumPool = ObjectPool.get(Plum.class);
   private SegmentPiece pointUnsaveTmp = new SegmentPiece();
   public boolean raw = true;

   public ExhaustPlumes(Ship var1) {
      this.setShip(var1);
      rot.rotY(3.1415927F);
   }

   public void cleanUp() {
      this.getShip();
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      if (!this.ship.isCloakedFor(((GameClientState)this.ship.getState()).getCurrentPlayerObject()) && this.ship.percentageDrawn >= 1.0F && !this.ship.isInAdminInvisibility()) {
         if (MainGameGraphics.drawBloomedEffects()) {
            if (!this.initialized) {
               if (this.ship.getWorldTransform() != null) {
                  this.scheduleUpdate();
                  this.initialized = true;
               }

            } else {
               boolean var1 = true;
               ObjectBidirectionalIterator var2 = this.unsortedplumes.iterator();

               while(var2.hasNext()) {
                  Plum var3;
                  (var3 = (Plum)var2.next()).getWorldTransform(this.t, this.localTranslation);
                  this.t.basis.mul(rot);
                  if (GlUtil.isPointInCamRange(this.t.origin, Controller.vis.getVisLen()) && GlUtil.isInViewFrustum(this.t, PlumeAndMuzzleDrawer.plumeMesh, 0.0F)) {
                     if (var1) {
                        if (!this.raw) {
                           this.updateShader();
                        } else {
                           this.updateBloomShader(var3);
                        }

                        var1 = false;
                     }

                     GlUtil.glPushMatrix();
                     GlUtil.glMultMatrix(this.t);
                     ((Mesh)PlumeAndMuzzleDrawer.plumeMesh.getChilds().get(0)).drawVBOAttributed();
                     GlUtil.glPopMatrix();
                  }
               }

            }
         }
      }
   }

   private void updateBloomShader(Plum var1) {
      float var3 = Math.min(0.99F, this.ship.lastSpeed / this.ship.getMaxServerSpeed());
      Vector4f var2 = this.ship.getManagerContainer().getColorCore();
      GlUtil.updateShaderVector4f(ShaderLibrary.silhouetteAlpha, "silhouetteColor", var3 * var2.x, var3 * var2.y, var3 * var2.z, 1.0F);
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      shader = ShaderLibrary.exaustShader;
   }

   public Ship getShip() {
      return this.ship;
   }

   public void setShip(Ship var1) {
      this.ship = var1;
   }

   public void onExit() {
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
   }

   public void onPlumesChanged() {
      synchronized(this.unsortedplumes) {
         Iterator var2 = this.unsortedplumes.iterator();

         while(var2.hasNext()) {
            Plum var3;
            (var3 = (Plum)var2.next()).reset();
            this.plumPool.release(var3);
         }

         this.unsortedplumes.clear();
         var2 = ((ThrusterCollectionManager)this.ship.getManagerContainer().getThrusterElementManager().getCollection()).getElementCollections().iterator();

         label35:
         while(var2.hasNext()) {
            Iterator var8 = ((ThrusterUnit)var2.next()).getLastElements().values().iterator();

            while(true) {
               SegmentPiece var4;
               long var5;
               do {
                  if (!var8.hasNext()) {
                     continue label35;
                  }

                  ElementCollection.getPosFromIndex(var5 = (Long)var8.next(), this.test);
                  --this.test.z;
               } while((var4 = this.ship.getSegmentBuffer().getPointUnsave(this.test, this.pointUnsaveTmp)) != null && var4.getType() != 0);

               Plum var9;
               (var9 = (Plum)this.plumPool.get()).set(this.ship, ElementCollection.getPosFromIndex(var5, new Vector3i()));
               this.unsortedplumes.add(var9);
            }
         }

      }
   }

   public void scheduleUpdate() {
      this.scheduleUpdate = System.currentTimeMillis();
   }

   public void update(Timer var1) {
      float var2 = this.ship.getVelocity().length();
      float var3 = 0.3F;
      RunningEffect var4;
      if ((var4 = ((GameClientState)this.ship.getState()).getWorldDrawer().getSegmentControllerEffectDrawer().getEffect(this.ship)) != null && var4 instanceof JumpStart) {
         var2 = this.ship.getCurrentMaxVelocity();
         var3 = 0.1F;
      }

      if (this.ship.getSegmentController().isDocked()) {
         if (this.ship.getRootShip().getManagerContainer().thrustConfiguration.thrustSharing && this.ship.getRootShip() != this.ship) {
            this.ship.lastSpeed = this.ship.getRootShip().lastSpeed;
         } else {
            this.ship.lastSpeed = Math.max(var2, this.ship.lastSpeed - var1.getDelta() * var3 * this.ship.getMaxServerSpeed());
         }
      } else if (this.ship.lastSpeed < var2) {
         this.ship.lastSpeed = Math.min(var2, this.ship.lastSpeed + var1.getDelta() * var3 * this.ship.getMaxServerSpeed());
      } else {
         this.ship.lastSpeed = Math.max(var2, this.ship.lastSpeed - var1.getDelta() * var3 * this.ship.getMaxServerSpeed());
      }

      if (this.scheduleUpdate > 0L && System.currentTimeMillis() - this.scheduleUpdate > 100L) {
         this.onPlumesChanged();
         this.scheduleUpdate = -1L;
      }

      this.ticks = (float)((double)this.ticks + (double)(var1.getDelta() / 100.0F) * ((Math.random() + 9.999999747378752E-5D) / 0.10000000149011612D));
      if (this.ticks > 1.0F) {
         this.ticks = 0.0F;
      }

   }

   public void updateShader() {
      float var1 = Math.min(0.99F, this.ship.lastSpeed / this.ship.getMaxServerSpeed());
      this.color0t.set(this.color0);
      this.color1t.set(this.color1);
      this.color0t.scale(var1);
      this.color1t.scale(var1);
      this.color1t.x = 0.5F - var1 / 2.0F;
      this.color1t.z = var1;
      if (!color0tStat.equals(this.color0t)) {
         GlUtil.updateShaderVector4f(shader, "thrustColor0", this.color0t);
         color0tStat.set(this.color0t);
      }

      if (!color1tStat.equals(this.color1t)) {
         GlUtil.updateShaderVector4f(shader, "thrustColor1", this.color1t);
         color1tStat.set(this.color1t);
      }

      GlUtil.updateShaderFloat(shader, "ticks", this.ticks);
      if (this.ship.isDocked()) {
         System.err.println("[PLUME] update shader");
      }

   }
}
