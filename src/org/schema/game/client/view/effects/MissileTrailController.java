package org.schema.game.client.view.effects;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.game.client.view.tools.ColorTools;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.missile.Missile;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.particle.ParticleController;
import org.schema.schine.graphicsengine.forms.particle.trail.TrailControllerInterface;

public class MissileTrailController extends ParticleController implements TrailControllerInterface {
   private static final float MAX_LIFETIME = 15.0F;
   private final Vector3f pos = new Vector3f();
   private final Vector3f vel = new Vector3f();
   public int particleIndex = -1;
   boolean letItEnd;
   int sectorBefore = -1;
   private Missile missile;
   private boolean alive = false;
   private Vector3f posTmp = new Vector3f();
   private int MAX_ADDITIONAL_SECTIONS = 10;
   private Transform lastTarget;
   private Vector4f color;
   private Vector3f dirDummy = new Vector3f();

   public MissileTrailController(Missile var1, MissileTrailDrawer var2) {
      super(false, 1024);
      this.setOrderedDelete(true);
      this.missile = var1;
      this.sectorBefore = -1;
      this.color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   }

   private void addOne(Vector3f var1) {
      if (!this.letItEnd) {
         this.addParticle(var1, this.dirDummy);
      }

   }

   public void endTrail() {
      this.letItEnd = true;
   }

   public boolean isAlive() {
      return this.alive;
   }

   public void setAlive(boolean var1) {
      this.alive = var1;
   }

   public Missile getMissile() {
      return this.missile;
   }

   public void setMissile(Missile var1) {
      this.missile = var1;
      this.sectorBefore = -1;
   }

   public void startTrail(Missile var1) {
      this.reset();
      this.missile = var1;
      this.color.set(1.0F, 1.0F, 1.0F, 1.0F);
      ElementInformation var2;
      if (var1 != null && ElementKeyMap.isValidType(var1.getColorType()) && (var2 = ElementKeyMap.getInfoFast(var1.getColorType())).isLightSource()) {
         this.color.set(var2.getLightSourceColor());
         ColorTools.brighten(this.color);
      }

      this.lastTarget = null;
      this.letItEnd = false;
      this.setAlive(true);
   }

   public boolean updateParticle(int var1, Timer var2) {
      float var3 = ((MissileTrailParticleContainer)this.getParticles()).getLifetime(var1);
      ((MissileTrailParticleContainer)this.getParticles()).getPos(var1, this.pos);
      ((MissileTrailParticleContainer)this.getParticles()).getVelocity(var1, this.vel);
      ((MissileTrailParticleContainer)this.getParticles()).setLifetime(var1, var3 + var2.getDelta());
      ((MissileTrailParticleContainer)this.getParticles()).setPos(var1, this.pos.x + this.vel.x, this.pos.y + this.vel.y, this.pos.z + this.vel.z);
      return var3 < 15.0F;
   }

   public void update(Timer var1) {
      float var2 = Math.max(0.5F, this.missile.getSpeed() * 0.01F);
      if (this.lastTarget == null) {
         this.addOne(this.missile.getWorldTransformOnClient().origin);
         this.lastTarget = new Transform(this.missile.getWorldTransformOnClient());
      } else {
         this.posTmp.sub(this.lastTarget.origin, this.missile.getWorldTransformOnClient().origin);
         if (this.posTmp.length() > 300.0F) {
            System.err.println("MISSILE SRCTOR CHANGE FOR DRAWER " + this.posTmp);
            this.reset();
            this.lastTarget.set(this.missile.getWorldTransformOnClient());
         }

         this.posTmp.set(this.missile.getWorldTransformOnClient().origin);
         this.posTmp.sub(this.lastTarget.origin);
         if (this.posTmp.length() > var2) {
            this.addOne(this.missile.getWorldTransformOnClient().origin);
            this.lastTarget.set(this.missile.getWorldTransformOnClient());
         }
      }

      this.sectorBefore = this.missile.getSectorId();
      super.update(var1);
      if (this.letItEnd && this.getParticleCount() <= 0) {
         this.setAlive(false);
      }

   }

   public Vector4f getColor() {
      return this.color;
   }

   protected MissileTrailParticleContainer getParticleInstance(int var1) {
      return new MissileTrailParticleContainer(var1);
   }

   public int addParticle(Vector3f var1, Vector3f var2) {
      if (this.particlePointer >= ((MissileTrailParticleContainer)this.getParticles()).getCapacity() - 1) {
         ((MissileTrailParticleContainer)this.getParticles()).growCapacity();
      }

      ++this.idGen;
      int var3;
      if (this.isOrderedDelete()) {
         var3 = this.particlePointer % ((MissileTrailParticleContainer)this.getParticles()).getCapacity();
         ((MissileTrailParticleContainer)this.getParticles()).setPos(var3, var1.x, var1.y, var1.z);
         ((MissileTrailParticleContainer)this.getParticles()).setStart(var3, var1.x, var1.y, var1.z);
         ((MissileTrailParticleContainer)this.getParticles()).setVelocity(var3, var2.x, var2.y, var2.z);
         ((MissileTrailParticleContainer)this.getParticles()).setLifetime(var3, 0.0F);
         ++this.particlePointer;
         return var3;
      } else {
         var3 = this.particlePointer % ((MissileTrailParticleContainer)this.getParticles()).getCapacity();
         ((MissileTrailParticleContainer)this.getParticles()).setPos(var3, var1.x, var1.y, var1.z);
         ((MissileTrailParticleContainer)this.getParticles()).setStart(var3, var1.x, var1.y, var1.z);
         ((MissileTrailParticleContainer)this.getParticles()).setVelocity(var3, var2.x, var2.y, var2.z);
         ((MissileTrailParticleContainer)this.getParticles()).setLifetime(var3, 0.0F);
         ++this.particlePointer;
         return var3;
      }
   }
}
