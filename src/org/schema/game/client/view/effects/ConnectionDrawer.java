package org.schema.game.client.view.effects;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap.Entry;
import java.nio.FloatBuffer;
import java.util.Iterator;
import javax.vecmath.AxisAngle4f;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.texture.Material;

public class ConnectionDrawer implements Drawable {
   static int maxVerts = 1024;
   private static Vector3f[] tubeVerticesA = new Vector3f[8];
   private static Vector3f[] tubeVerticesNormal = new Vector3f[8];
   private static Vector3f[] tubeVerticesB = new Vector3f[8];
   private static Vector3f[] tubeTexCoord = new Vector3f[8];
   private static Vector3f[] tubeVerticesAPre = new Vector3f[8];
   private static Vector3f[] tubeVerticesNormalPre = new Vector3f[8];
   private static Vector3f[] tubeVerticesBPre = new Vector3f[8];
   private static Vector3f[] tubeTexCoordPre = new Vector3f[8];
   private final SegmentController c;
   private final Long2ObjectOpenHashMap connections = new Long2ObjectOpenHashMap();
   private int VBOIndex;
   private Vector3f start = new Vector3f();
   private Vector3f end = new Vector3f();
   private Vector3f dist = new Vector3f();
   private Vector3f forward = new Vector3f();
   private Vector3f up = new Vector3f(0.0F, 1.0F, 0.0F);
   private Vector3f right = new Vector3f();
   private Matrix3f m = new Matrix3f();
   private AxisAngle4f aa = new AxisAngle4f();
   private int vertices;
   private boolean flagUpdate = true;
   private boolean noDraw;
   private int VBONormalIndex;
   private int VBOTexCoordsIndex;
   private Material material;
   private long failed = -1L;
   private boolean conOn;
   private boolean conLohOn;
   private final Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);

   public ConnectionDrawer(SegmentController var1) {
      this.c = var1;
      this.conOn = EngineSettings.G_DRAW_ANY_CONNECTIONS.isOn();
   }

   public void buildMesh() {
      long var1 = System.currentTimeMillis();
      long var3 = System.currentTimeMillis();
      int var5 = 0;
      Iterator var6 = this.connections.long2ObjectEntrySet().iterator();

      while(var6.hasNext()) {
         Entry var7;
         Iterator var8 = ((LongOpenHashSet)(var7 = (Entry)var6.next()).getValue()).iterator();

         while(var8.hasNext()) {
            long var9 = (Long)var8.next();
            int var11 = Math.abs(ElementCollection.getPosX(var7.getLongKey()) - ElementCollection.getPosX(var9));
            int var12 = Math.abs(ElementCollection.getPosY(var7.getLongKey()) - ElementCollection.getPosY(var9));
            int var13 = Math.abs(ElementCollection.getPosZ(var7.getLongKey()) - ElementCollection.getPosZ(var9));
            short var24 = (short)ElementCollection.getType(var9);
            if (var11 + var12 + var13 > 1 && ElementKeyMap.isValidType(var24) && ElementKeyMap.getInfo(var24).drawConnection()) {
               ++var5;
            }
         }
      }

      long var22 = System.currentTimeMillis() - var3;
      this.noDraw = var5 == 0;
      if (!this.noDraw) {
         this.vertices = var5 << 1 << 1 << 3;
         var3 = System.currentTimeMillis();

         int var23;
         for(var23 = this.vertices * 3 << 2; maxVerts < var23; maxVerts <<= 1) {
         }

         FloatBuffer var25 = GlUtil.getDynamicByteBuffer(maxVerts, 0).asFloatBuffer();
         FloatBuffer var10 = GlUtil.getDynamicByteBuffer(maxVerts, 1).asFloatBuffer();
         FloatBuffer var26 = GlUtil.getDynamicByteBuffer(maxVerts, 2).asFloatBuffer();
         long var27 = System.currentTimeMillis() - var3;
         var3 = System.currentTimeMillis();
         var5 = 0;
         Iterator var16 = this.connections.long2ObjectEntrySet().iterator();

         while(var16.hasNext()) {
            Entry var17 = (Entry)var16.next();
            this.start.set(-16.0F + (float)ElementCollection.getPosX((Long)var17.getKey()), -16.0F + (float)ElementCollection.getPosY((Long)var17.getKey()), -16.0F + (float)ElementCollection.getPosZ((Long)var17.getKey()));
            Iterator var18 = ((LongOpenHashSet)var17.getValue()).iterator();

            while(var18.hasNext()) {
               long var19 = (Long)var18.next();
               int var21 = Math.abs(ElementCollection.getPosX(var17.getLongKey()) - ElementCollection.getPosX(var19));
               var23 = Math.abs(ElementCollection.getPosY(var17.getLongKey()) - ElementCollection.getPosY(var19));
               int var14 = Math.abs(ElementCollection.getPosZ(var17.getLongKey()) - ElementCollection.getPosZ(var19));
               short var15 = (short)ElementCollection.getType(var19);
               if (var21 + var23 + var14 > 1 && ElementKeyMap.isValidType(var15) && ElementKeyMap.getInfo(var15).drawConnection()) {
                  this.end.set(-16.0F + (float)ElementCollection.getPosX(var19), -16.0F + (float)ElementCollection.getPosY(var19), -16.0F + (float)ElementCollection.getPosZ(var19));
                  var5 += this.putStraightSection(this.start, this.end, var25, var10, var26, 1, 1);
               }
            }
         }

         long var28 = System.currentTimeMillis() - var3;
         var3 = System.currentTimeMillis();

         assert this.vertices == var5 : this.vertices + "/" + var5;

         if (this.VBOIndex == 0) {
            this.VBOIndex = GL15.glGenBuffers();
            Controller.loadedVBOBuffers.add(this.VBOIndex);
            this.VBONormalIndex = GL15.glGenBuffers();
            Controller.loadedVBOBuffers.add(this.VBONormalIndex);
            this.VBOTexCoordsIndex = GL15.glGenBuffers();
            Controller.loadedVBOBuffers.add(this.VBOTexCoordsIndex);
         }

         GlUtil.glBindBuffer(34962, this.VBOIndex);
         var25.flip();
         GL15.glBufferData(34962, var25, 35044);
         GlUtil.glBindBuffer(34962, this.VBONormalIndex);
         var10.flip();
         GL15.glBufferData(34962, var10, 35044);
         GlUtil.glBindBuffer(34962, this.VBOTexCoordsIndex);
         var26.flip();
         GL15.glBufferData(34962, var26, 35044);
         GlUtil.glBindBuffer(34962, 0);
         this.material = new Material();
         this.material.setShininess(64.0F);
         this.material.setSpecular(new float[]{0.9F, 0.9F, 0.9F, 1.0F});
         long var29 = System.currentTimeMillis() - var3;
         long var20;
         if ((var20 = System.currentTimeMillis() - var1) > 30L) {
            System.err.println("CONNECTION BUILDING TOOK " + var20 + "ms : tookInitial " + var22 + "; tookFetchBuffers " + var27 + "; tookVerInser " + var28 + "; tookVBO " + var29);
         }

      }
   }

   public void cleanUp() {
      if (this.VBOIndex != 0) {
         GL15.glDeleteBuffers(this.VBOIndex);
         GL15.glDeleteBuffers(this.VBONormalIndex);
         GL15.glDeleteBuffers(this.VBOTexCoordsIndex);
      }

   }

   public void draw() {
      if (this.failed > 0L && System.currentTimeMillis() - this.failed > 2500L) {
         this.flagUpdate = true;
         this.failed = -1L;
      }

      if (this.conOn != EngineSettings.G_DRAW_ANY_CONNECTIONS.isOn()) {
         this.flagUpdate = true;
         this.conOn = EngineSettings.G_DRAW_ANY_CONNECTIONS.isOn();
      }

      if (this.conLohOn != EngineSettings.G_DRAW_ALL_CONNECTIONS.isOn()) {
         this.flagUpdate = true;
         this.conLohOn = EngineSettings.G_DRAW_ALL_CONNECTIONS.isOn();
      }

      if (this.flagUpdate) {
         this.updateConnections();
         this.buildMesh();
         this.flagUpdate = false;
      }

      if (!this.noDraw && this.connections.size() > 0) {
         GlUtil.glPushMatrix();
         GlUtil.glMultMatrix((Transform)this.c.getWorldTransformOnClient());
         GlUtil.glEnable(2903);
         GlUtil.glEnable(2896);
         GlUtil.glEnable(2929);
         this.material.attach(0);
         GlUtil.glColor4f(this.getColor());
         GlUtil.glEnableClientState(32884);
         GlUtil.glEnableClientState(32885);
         GlUtil.glEnableClientState(32888);
         GlUtil.glBindBuffer(34962, this.VBOTexCoordsIndex);
         GL11.glTexCoordPointer(3, 5126, 0, 0L);
         GlUtil.glBindBuffer(34962, this.VBONormalIndex);
         GL11.glNormalPointer(5126, 0, 0L);
         GlUtil.glBindBuffer(34962, this.VBOIndex);
         GL11.glVertexPointer(3, 5126, 0, 0L);
         GL11.glDrawArrays(7, 0, this.vertices);
         GlUtil.glBindBuffer(34962, 0);
         GlUtil.glDisableClientState(32884);
         GlUtil.glDisableClientState(32885);
         GlUtil.glDisableClientState(32888);
         GlUtil.glDisable(2903);
         GlUtil.glPopMatrix();
      }

   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
   }

   public void flagUpdate() {
      this.flagUpdate = true;
   }

   public SegmentController getSegmentController() {
      return this.c;
   }

   private int putStraightSection(Vector3f var1, Vector3f var2, FloatBuffer var3, FloatBuffer var4, FloatBuffer var5, int var6, int var7) {
      this.right.set(1.0F, 0.0F, 0.0F);
      this.up.set(0.0F, 1.0F, 0.0F);
      this.forward.set(0.0F, 0.0F, 1.0F);
      this.right.scale(0.1F);
      this.forward.sub(var2, var1);
      this.up.cross(this.right, this.forward);
      if (this.up.lengthSquared() == 0.0F) {
         this.up.set(0.0F, 1.0F, 0.0F);
      }

      this.up.normalize();
      this.right.cross(this.up, this.forward);
      if (this.right.lengthSquared() == 0.0F) {
         this.right.set(1.0F, 0.0F, 0.0F);
      }

      this.right.normalize();
      GlUtil.setRightVector(this.right, this.m);
      GlUtil.setUpVector(this.up, this.m);
      GlUtil.setForwardVector(this.forward, this.m);

      for(var6 = 0; var6 < 8; ++var6) {
         tubeVerticesA[var6].set(tubeVerticesAPre[var6]);
         tubeVerticesB[var6].set(tubeVerticesBPre[var6]);
         this.m.transform(tubeVerticesA[var6]);
         this.m.transform(tubeVerticesB[var6]);
         tubeVerticesNormal[var6].set(tubeVerticesA[var6]);
         tubeTexCoord[var6].set(tubeTexCoordPre[var6]);
         tubeVerticesA[var6].add(var1);
         tubeVerticesB[var6].add(var2);
      }

      assert var7 != 0;

      for(int var8 = 0; var8 < 8; ++var8) {
         GlUtil.putPoint3(var3, tubeVerticesA[var8]);
         GlUtil.putPoint3(var4, tubeVerticesNormal[var8]);
         tubeTexCoord[var8].x = 0.0F;
         GlUtil.putPoint3(var5, tubeTexCoord[var8]);
         GlUtil.putPoint3(var3, tubeVerticesA[(var8 + 1) % 8]);
         GlUtil.putPoint3(var4, tubeVerticesNormal[(var8 + 1) % 8]);
         tubeTexCoord[(var8 + 1) % 8].x = 0.0F;
         GlUtil.putPoint3(var5, tubeTexCoord[(var8 + 1) % 8]);
         GlUtil.putPoint3(var3, tubeVerticesB[(var8 + 1) % 8]);
         GlUtil.putPoint3(var4, tubeVerticesNormal[(var8 + 1) % 8]);
         tubeTexCoord[(var8 + 1) % 8].x = 1.0F;
         GlUtil.putPoint3(var5, tubeTexCoord[(var8 + 1) % 8]);
         GlUtil.putPoint3(var3, tubeVerticesB[var8]);
         GlUtil.putPoint3(var4, tubeVerticesNormal[var8]);
         tubeTexCoord[var8].x = 1.0F;
         GlUtil.putPoint3(var5, tubeTexCoord[var8]);
      }

      return 32;
   }

   public void updateConnections() {
      long var1 = System.currentTimeMillis();
      this.connections.clear();
      this.failed = -1L;
      Long2ObjectOpenHashMap var3;
      Iterator var4 = (var3 = this.c.getControlElementMap().getControllingMap().getAll()).entrySet().iterator();

      while(var4.hasNext()) {
         java.util.Map.Entry var5 = (java.util.Map.Entry)var4.next();
         SegmentPiece var6 = new SegmentPiece();
         if (((FastCopyLongOpenHashSet)var5.getValue()).size() > 0) {
            if ((var6 = this.getSegmentController().getSegmentBuffer().getPointUnsave((Long)var5.getKey(), var6)) != null) {
               if (ElementKeyMap.isValidType(var6.getType()) && ElementKeyMap.getInfo(var6.getType()).drawConnection()) {
                  this.connections.put((Long)var5.getKey(), var5.getValue());
               }
            } else {
               this.failed = System.currentTimeMillis();
            }
         }
      }

      long var7;
      if ((var7 = System.currentTimeMillis() - var1) > 40L) {
         System.err.println("[CLIENT] update connections took " + var7 + "ms; controllers: " + var3.size() + "; checks: 0");
      }

   }

   public Vector4f getColor() {
      return this.color;
   }

   static {
      for(int var0 = 0; var0 < 8; ++var0) {
         tubeVerticesA[var0] = new Vector3f();
         tubeVerticesB[var0] = new Vector3f();
         tubeVerticesNormal[var0] = new Vector3f();
         tubeTexCoord[var0] = new Vector3f();
         tubeVerticesAPre[var0] = new Vector3f();
         tubeVerticesBPre[var0] = new Vector3f();
         tubeVerticesNormalPre[var0] = new Vector3f();
         tubeTexCoordPre[var0] = new Vector3f();
      }

      Vector3f var6 = new Vector3f();
      Vector3f var1 = new Vector3f();
      Vector3f var2 = new Vector3f();
      AxisAngle4f var3 = new AxisAngle4f();
      Matrix3f var4 = new Matrix3f();
      var6.set(0.0F, 0.0F, 1.0F);
      var1.set(1.0F, 0.0F, 0.0F);
      var2.set(0.0F, 1.0F, 0.0F);
      var1.scale(0.1F);
      float var7 = 0.0F;
      float var8 = 0.0F;

      for(int var5 = 0; var5 < 8; ++var5) {
         var3.set(var6, var7);
         var4.set(var3);
         tubeVerticesAPre[var5].set(0.0F, 0.1F, 0.0F);
         var4.transform(tubeVerticesAPre[var5]);
         tubeVerticesNormalPre[var5].set(tubeVerticesAPre[var5]);
         tubeVerticesBPre[var5].set(tubeVerticesAPre[var5]);
         tubeTexCoordPre[var5].set(0.0F, var8, 0.0F);
         var8 += 0.125F;
         var7 += 0.7853982F;
      }

   }
}
