package org.schema.game.client.view.effects;

import java.util.Collection;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.forms.Sprite;

public class ItemDrawer implements Drawable {
   private GameClientState state;
   private boolean init;
   private Sprite[] sprite;

   public ItemDrawer(GameClientState var1) {
      this.state = var1;
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      RemoteSector var1;
      if ((var1 = this.state.getCurrentRemoteSector()) != null && !var1.getItems().isEmpty()) {
         for(int var2 = 0; var2 < this.sprite.length; ++var2) {
            Sprite var3;
            (var3 = this.sprite[var2]).setScale(0.01F, 0.01F, 0.01F);
            var3.setFlip(true);
            var3.setBillboard(true);
            Sprite.draw3D(var3, (Collection)var1.getItems().values(), Controller.getCamera());
            var3.setBillboard(false);
            var3.setFlip(false);
            var3.setScale(1.0F, 1.0F, 1.0F);
         }

      }
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.sprite = new Sprite[5];
      this.sprite[0] = Controller.getResLoader().getSprite("build-icons-00-16x16-gui-");
      this.sprite[1] = Controller.getResLoader().getSprite("build-icons-01-16x16-gui-");
      this.sprite[2] = Controller.getResLoader().getSprite("build-icons-02-16x16-gui-");
      this.sprite[3] = Controller.getResLoader().getSprite("build-icons-extra-gui-");
      this.sprite[4] = Controller.getResLoader().getSprite("meta-icons-00-16x16-gui-");
      this.init = true;
   }
}
