package org.schema.game.client.view.effects;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import java.util.Iterator;
import java.util.Map.Entry;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;

public class ConnectionDrawerManager implements Drawable {
   private final Object2ObjectOpenHashMap map = new Object2ObjectOpenHashMap();
   private final GameClientState state;
   private float conTime;

   public ConnectionDrawerManager(GameClientState var1) {
      this.state = var1;
   }

   public void cleanUp() {
      Iterator var1 = this.map.values().iterator();

      while(var1.hasNext()) {
         ((ConnectionDrawer)var1.next()).cleanUp();
      }

   }

   public void draw() {
      ShaderLibrary.tubesShader.loadWithoutUpdate();
      float var1 = (float)((double)(this.state.getController().getServerRunningTime() % 5000L) / 5000.0D);
      GlUtil.updateShaderFloat(ShaderLibrary.tubesShader, "time", var1);
      Iterator var3 = this.map.values().iterator();

      while(var3.hasNext()) {
         ConnectionDrawer var2;
         if (!(var2 = (ConnectionDrawer)var3.next()).getSegmentController().isCloakedFor(this.state.getCurrentPlayerObject())) {
            var2.draw();
         }
      }

      ShaderLibrary.tubesShader.unloadWithoutExit();
      GlUtil.glColor4fForced(1.0F, 1.0F, 1.0F, 1.0F);
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
   }

   public void update(Timer var1) {
      this.conTime += var1.getDelta() * 2.0F;
      this.conTime -= (float)((int)this.conTime);
   }

   public void onConnectionChanged(SendableSegmentController var1) {
      ConnectionDrawer var2;
      if ((var2 = (ConnectionDrawer)this.map.get(var1)) != null) {
         var2.flagUpdate();
      } else {
         System.err.println("[CLIENT][ConnectionDrawer] WARNING segController to update not found!!!!!!!!!!! searching " + var1);
      }
   }

   public void updateEntities() {
      Iterator var1 = this.state.getCurrentSectorEntities().values().iterator();

      while(true) {
         SimpleTransformableSendableObject var2;
         ConnectionDrawer var3;
         do {
            if (!var1.hasNext()) {
               ObjectIterator var4 = this.map.entrySet().iterator();

               while(true) {
                  Entry var5;
                  do {
                     if (!var4.hasNext()) {
                        return;
                     }
                  } while(((SegmentController)(var5 = (Entry)var4.next()).getKey()).getSectorId() == this.state.getCurrentSectorId() && ((SegmentController)var5.getKey()).getState().getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(((SegmentController)var5.getKey()).getId()));

                  if ((var3 = (ConnectionDrawer)this.map.get(var5)) != null) {
                     var3.cleanUp();
                  }

                  var4.remove();
               }
            }
         } while(!((var2 = (SimpleTransformableSendableObject)var1.next()) instanceof Planet) && !(var2 instanceof SpaceStation) && !(var2 instanceof Ship));

         if (!this.map.containsKey(var2)) {
            var3 = new ConnectionDrawer((SegmentController)var2);
            this.map.put((SegmentController)var2, var3);
         }
      }
   }

   public void clear() {
      this.cleanUp();
      this.map.clear();
   }
}
