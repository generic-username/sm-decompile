package org.schema.game.client.view.effects.segmentcontrollereffects;

import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.graphicsengine.core.Timer;

public class TestEffect extends RunningEffect {
   Vector3f tmp = new Vector3f();
   private float timeLived;
   private TunnelEffect tunnel;

   public TestEffect(SegmentController var1, long var2) {
      super(var1, SegConEffects.TEST, var2);
      this.tunnel = new TunnelEffect(var1);
      this.tunnel.onInit();
      SegmentControllerEffectDrawer.spaceParticleDrawer.reset();
      this.tunnel.setTransform(var1.getWorldTransform());
   }

   public boolean isDrawOriginal() {
      return true;
   }

   public void update(Timer var1) {
      this.timeLived += var1.getDelta();
      this.tunnel.update(var1);
   }

   public boolean isAlive() {
      return true;
   }

   public void loadShader() {
   }

   public void unloadShader() {
   }

   public void drawInsideEffect() {
      SegmentControllerEffectDrawer.spaceParticleDrawer.updateCam();
      this.tunnel.draw();
   }

   public void drawOutsideEffect() {
   }

   public void modifyModelview(GameClientState var1) {
   }

   public int overlayBlendMode() {
      return 0;
   }
}
