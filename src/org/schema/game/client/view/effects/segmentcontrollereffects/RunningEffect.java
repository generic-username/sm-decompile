package org.schema.game.client.view.effects.segmentcontrollereffects;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.graphicsengine.core.Timer;

public abstract class RunningEffect {
   public final SegmentController segmentController;
   public final SegConEffects type;
   public final long timeStarted;

   public RunningEffect(SegmentController var1, SegConEffects var2, long var3) {
      this.segmentController = var1;
      this.type = var2;
      this.timeStarted = var3;
   }

   public static RunningEffect getInstance(SegmentController var0, SegConEffects var1, long var2) {
      Object var4;
      switch(var1) {
      case JUMP_END:
         var4 = new JumpEnd(var0, var2);
         break;
      case JUMP_START:
         var4 = new JumpStart(var0, var2);
         break;
      case TEST:
         var4 = new TestEffect(var0, var2);
         break;
      default:
         throw new IllegalArgumentException(var1.toString());
      }

      assert ((RunningEffect)var4).type == var1;

      return (RunningEffect)var4;
   }

   public abstract boolean isDrawOriginal();

   public int hashCode() {
      return this.segmentController.hashCode();
   }

   public boolean equals(Object var1) {
      return ((RunningEffect)var1).segmentController == this.segmentController;
   }

   public abstract void update(Timer var1);

   public abstract boolean isAlive();

   public abstract void loadShader();

   public abstract void unloadShader();

   public abstract void drawInsideEffect();

   public abstract void drawOutsideEffect();

   public abstract void modifyModelview(GameClientState var1);

   public abstract int overlayBlendMode();
}
