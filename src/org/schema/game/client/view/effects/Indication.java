package org.schema.game.client.view.effects;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.ColoredInterface;

public abstract class Indication {
   private static final float TEXT_MAX_DISTANCE = 100.0F;
   public float lifetime = 0.3F;
   public Transform start;
   private float timeLived;
   private Object text;
   private Vector4f color = null;
   private float dist;

   public Indication(Transform var1, Object var2) {
      this.start = var1;
      this.text = var2;
      this.dist = 100.0F;
   }

   public Vector4f getColor() {
      return this.color;
   }

   public void setColor(final Vector4f var1) {
      final String var2 = this.text.toString();
      this.color = var1;
      this.text = new ColoredInterface() {
         public Vector4f getColor() {
            return var1;
         }

         public String toString() {
            return var2;
         }
      };
   }

   public abstract Transform getCurrentTransform();

   public Object getText() {
      return this.text;
   }

   public void setText(Object var1) {
      this.text = var1;
   }

   public int hashCode() {
      return this.text.hashCode();
   }

   public boolean equals(Object var1) {
      return ((Indication)var1).text.equals(this.text);
   }

   public boolean isAlive() {
      return this.timeLived < this.lifetime;
   }

   public float scaleIndication() {
      return 1.0F;
   }

   public void update(Timer var1) {
      this.timeLived += var1.getDelta();
   }

   public float getDist() {
      return this.dist;
   }

   public void setDist(float var1) {
      this.dist = var1;
   }
}
