package org.schema.game.client.view.effects;

import com.bulletphysics.linearmath.Transform;

public class ConstantIndication extends Indication {
   public ConstantIndication(Transform var1, Object var2) {
      super(var1, var2);
   }

   public Transform getCurrentTransform() {
      return this.start;
   }

   public boolean isAlive() {
      return true;
   }
}
