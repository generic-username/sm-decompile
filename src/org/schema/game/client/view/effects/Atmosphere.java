package org.schema.game.client.view.effects;

import javax.vecmath.Vector3f;
import org.lwjgl.opengl.GL11;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.world.SectorInformation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.space.PlanetCore;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.input.Keyboard;
import org.schema.schine.network.objects.Sendable;

public class Atmosphere implements Drawable, Shaderable {
   private final GameClientState state;
   private float m_Kr;
   private float m_Kr4PI;
   private float m_Km;
   private float m_Km4PI;
   private float m_ESun;
   private float m_fInnerRadius;
   private float m_fOuterRadius;
   private float m_fScale;
   private float[] m_fWavelength = new float[3];
   private float[] m_fWavelength4 = new float[3];
   private float m_fRayleighScaleDepth;
   private Mesh pSphere;
   private boolean init;
   private SectorInformation.PlanetType type;
   private float currentRadius = 0.0F;
   private boolean drawError;
   private Vector3i currentSec = new Vector3i();

   public Atmosphere(GameClientState var1) {
      this.state = var1;
   }

   public void cleanUp() {
   }

   public void draw() {
      this.drawError = Keyboard.isKeyDown(60);
      if (!EngineSettings.G_ATMOSPHERE_SHADER.getCurrentState().equals("none")) {
         if (this.state.getPlayer() != null) {
            ShaderLibrary.skyFromAtmo.setShaderInterface(this);
            ShaderLibrary.skyFromSpace.setShaderInterface(this);
            Vector3i var1 = this.state.getPlayer().getCurrentSector();
            String var2 = SimpleTransformableSendableObject.EntityType.PLANET_CORE.dbPrefix + var1.x + "_" + var1.y + "_" + var1.z;
            Sendable var5;
            if (!((var5 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var2)) instanceof PlanetCore)) {
               if (!this.currentSec.equals(var1)) {
                  System.err.println("[CLIENT][ATHMOSPHERE] cannot draw. no core: " + var5);
               }

               this.currentSec.set(var1);
            } else {
               this.currentSec.set(var1);
               PlanetCore var3;
               float var6 = (var3 = (PlanetCore)var5).getRadius() + 150.0F;
               if (!this.init || this.currentRadius != var6) {
                  System.err.println("[CLIENT][ATHMOSPHERE] initializing athomsphere: " + var3.toString() + "; RADIUS: " + var3.getRadius());
                  this.currentRadius = var6;
                  this.onInit();
               }

               if (this.drawError) {
                  GlUtil.printGlErrorCritical();
               }

               GlUtil.glEnable(3042);
               GlUtil.glBlendFunc(770, 771);
               this.pSphere.loadVBO(true);
               if (this.drawError) {
                  GlUtil.printGlErrorCritical();
               }

               boolean var4;
               if (var4 = this.isInside()) {
                  if (this.drawError) {
                     GlUtil.printGlErrorCritical();
                  }

                  ShaderLibrary.skyFromAtmo.load();
                  if (this.drawError) {
                     GlUtil.printGlErrorCritical();
                  }

                  GL11.glCullFace(1028);
                  this.drawPass();
                  if (this.drawError) {
                     GlUtil.printGlErrorCritical();
                  }
               } else {
                  GL11.glFrontFace(2304);
                  if (this.drawError) {
                     GlUtil.printGlErrorCritical();
                  }

                  ShaderLibrary.skyFromSpace.load();
                  this.drawPass();
                  if (this.drawError) {
                     GlUtil.printGlErrorCritical();
                  }
               }

               if (this.drawError) {
                  GlUtil.printGlErrorCritical();
               }

               GL11.glCullFace(1029);
               GL11.glFrontFace(2305);
               GlUtil.glDisable(3042);
               if (var4) {
                  if (this.drawError) {
                     GlUtil.printGlErrorCritical();
                  }

                  ShaderLibrary.skyFromAtmo.unload();
                  if (this.drawError) {
                     GlUtil.printGlErrorCritical();
                  }
               } else {
                  if (this.drawError) {
                     GlUtil.printGlErrorCritical();
                  }

                  ShaderLibrary.skyFromSpace.unload();
                  if (this.drawError) {
                     GlUtil.printGlErrorCritical();
                  }
               }

               this.pSphere.unloadVBO(true);
               if (this.drawError) {
                  GlUtil.printGlErrorCritical();
               }

            }
         }
      }
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      if (!EngineSettings.G_ATMOSPHERE_SHADER.getCurrentState().equals("none")) {
         this.m_Kr = 0.0025F;
         this.m_Km = 0.001F;
         this.m_Kr4PI = this.m_Kr * 4.0F * 3.1415927F;
         this.m_Km4PI = this.m_Km * 4.0F * 3.1415927F;
         this.m_ESun = 40.0F;
         this.m_fInnerRadius = this.currentRadius - 60.0F;
         this.m_fOuterRadius = this.currentRadius;
         this.m_fScale = 1.0F / (this.m_fOuterRadius - this.m_fInnerRadius);
         this.m_fWavelength[0] = 0.65F;
         this.m_fWavelength[1] = 0.57F;
         this.m_fWavelength[2] = 0.475F;
         this.m_fWavelength4[0] = FastMath.pow(this.m_fWavelength[0], 4.0F);
         this.m_fWavelength4[1] = FastMath.pow(this.m_fWavelength[1], 4.0F);
         this.m_fWavelength4[2] = FastMath.pow(this.m_fWavelength[2], 4.0F);
         this.m_fRayleighScaleDepth = 0.25F;
         this.pSphere = (Mesh)Controller.getResLoader().getMesh("GeoSphere").getChilds().get(0);
         this.init = true;
      }
   }

   private void drawPass() {
      GlUtil.glPushMatrix();
      this.pSphere.renderVBO();
      GlUtil.glPopMatrix();
   }

   public boolean isInside() {
      return (new Vector3f(Controller.getCamera().getPos())).length() < this.m_fOuterRadius;
   }

   public void onExit() {
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      if (this.drawError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderFloat(var1, "meshScale", 0.0033333334F * this.m_fOuterRadius);
      if (this.drawError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderVector4f(var1, "tint", this.type.atmosphereInner);
      if (this.drawError) {
         GlUtil.printGlErrorCritical();
      }

      Vector3f var2 = new Vector3f(Controller.getCamera().getPos());
      GlUtil.updateShaderVector3f(var1, "v3CameraPos", var2);
      if (this.drawError) {
         GlUtil.printGlErrorCritical();
      }

      Vector3f var3;
      (var3 = new Vector3f(AbstractScene.mainLight.getPos())).sub(var2);
      var3.normalize();
      GlUtil.updateShaderVector3f(var1, "v3LightPos", var3);
      if (this.drawError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderVector3f(var1, "v3InvWavelength", 1.0F / this.m_fWavelength4[0], 1.0F / this.m_fWavelength4[1], 1.0F / this.m_fWavelength4[2]);
      if (this.drawError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderFloat(var1, "fCameraHeight", var2.length());
      if (this.drawError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderFloat(var1, "fCameraHeight2", var2.lengthSquared());
      if (this.drawError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderFloat(var1, "fInnerRadius", this.m_fInnerRadius);
      if (this.drawError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderFloat(var1, "fInnerRadius2", this.m_fInnerRadius * this.m_fInnerRadius);
      if (this.drawError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderFloat(var1, "fOuterRadius", this.m_fOuterRadius);
      GlUtil.updateShaderFloat(var1, "fOuterRadius2", this.m_fOuterRadius * this.m_fOuterRadius);
      GlUtil.updateShaderFloat(var1, "fKrESun", this.m_Kr * this.m_ESun);
      GlUtil.updateShaderFloat(var1, "fKmESun", this.m_Km * this.m_ESun);
      GlUtil.updateShaderFloat(var1, "fKr4PI", this.m_Kr4PI);
      GlUtil.updateShaderFloat(var1, "fKm4PI", this.m_Km4PI);
      GlUtil.updateShaderFloat(var1, "fScale", this.m_fScale);
      GlUtil.updateShaderFloat(var1, "fScaleDepth", this.m_fRayleighScaleDepth);
      GlUtil.updateShaderFloat(var1, "fScaleOverScaleDepth", this.m_fScale / this.m_fRayleighScaleDepth);
      if (this.drawError) {
         GlUtil.printGlErrorCritical();
      }

   }

   public void setPlanetType(SectorInformation.PlanetType var1) {
      this.type = var1;
   }
}
