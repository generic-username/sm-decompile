package org.schema.game.client.view.effects;

import com.bulletphysics.linearmath.Transform;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.transporter.TransporterCollectionManager;
import org.schema.game.common.controller.elements.transporter.TransporterUnit;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;

public class TransporterEffectGroup {
   public SegmentController segmentController;
   public List transporters;
   Vector3i posiTmp = new Vector3i();
   Vector3f posfTmp = new Vector3f();
   SegmentPiece p = new SegmentPiece();
   Transform tmpTrans = new Transform();
   private Vector3f colorFrom = new Vector3f(0.2F, 0.3F, 0.4F);
   private Vector3f colorTo = new Vector3f(0.2F, 0.4F, 0.3F);

   public TransporterEffectGroup(SegmentController var1, List var2) {
      this.segmentController = var1;
      this.transporters = var2;
      this.tmpTrans.setIdentity();
   }

   public void updateLocal(Timer var1) {
   }

   public void draw(Mesh var1) {
      GlUtil.glPushMatrix();
      GlUtil.glMultMatrix((Transform)this.segmentController.getWorldTransformOnClient());
      Iterator var2 = this.transporters.iterator();

      while(true) {
         TransporterCollectionManager var3;
         do {
            if (!var2.hasNext()) {
               GlUtil.glPopMatrix();
               return;
            }
         } while(!(var3 = (TransporterCollectionManager)var2.next()).isTransporterActive() && !var3.isTransporterReceivingActive());

         GlUtil.updateShaderVector3f(ShaderLibrary.transporterShader, "colorMain", var3.isTransporterActive() ? this.colorFrom : this.colorTo);
         GlUtil.updateShaderFloat(ShaderLibrary.transporterShader, "intensity", var3.getEffectIntesity());
         Iterator var8 = var3.getElementCollections().iterator();

         while(var8.hasNext()) {
            Iterator var4 = ((TransporterUnit)var8.next()).getNeighboringCollection().iterator();

            while(var4.hasNext()) {
               long var6;
               ElementCollection.getPosFromIndex(var6 = (Long)var4.next(), this.posiTmp);
               SegmentPiece var5;
               if ((var5 = this.segmentController.getSegmentBuffer().getPointUnsave(var6, this.p)) != null) {
                  GlUtil.glPushMatrix();
                  this.posiTmp.add(Element.DIRECTIONSi[Element.switchLeftRight(var5.getOrientation())]);
                  this.posiTmp.sub(16, 16, 16);
                  this.posfTmp.set((float)this.posiTmp.x, (float)this.posiTmp.y, (float)this.posiTmp.z);
                  GlUtil.translateModelview(this.posfTmp);
                  this.tmpTrans.basis.set(Element.getRotationPerSideTopBase(var5.getOrientation()));
                  GlUtil.glMultMatrix(this.tmpTrans);
                  GlUtil.translateModelview(0.0F, -0.5F, 0.0F);
                  var1.drawVBO();
                  GlUtil.glPopMatrix();
               }
            }
         }
      }
   }
}
