package org.schema.game.client.view.effects;

import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.common.data.Dodecahedron;
import org.schema.game.common.data.world.space.PlanetCore;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class PlanetCoreDrawer implements Drawable, Shaderable {
   float time = 0.0F;
   PlanetCore core;
   Dodecahedron h;

   public void onExit() {
   }

   public void updateShader(DrawableScene var1) {
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderFloat(var1, "time", this.time);
      GlUtil.glBindTexture(3553, GameResourceLoader.lavaTexture.getTextureId());
      GlUtil.updateShaderInt(var1, "lavaTex", 0);
   }

   public void setCore(PlanetCore var1) {
      if (this.core != var1) {
         this.core = var1;
         if (this.core != null) {
            this.h = new Dodecahedron(var1.getRadius());
            this.h.create();
            return;
         }

         this.h = null;
      }

   }

   public void cleanUp() {
      if (this.h != null) {
         this.h.cleanUp();
      }

   }

   public void draw() {
      if (this.h != null) {
         ShaderLibrary.lavaShader.setShaderInterface(this);
         ShaderLibrary.lavaShader.load();
         this.h.draw();
         ShaderLibrary.lavaShader.unload();
      }

   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
   }

   public void update(Timer var1) {
      this.time += var1.getDelta();
   }
}
