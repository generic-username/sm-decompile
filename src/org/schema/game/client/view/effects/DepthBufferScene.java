package org.schema.game.client.view.effects;

import java.nio.ByteBuffer;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.SegmentDrawer;
import org.schema.game.client.view.SegmentOcclusion;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.input.Keyboard;

public class DepthBufferScene {
   private GameClientState state;
   private static int depthFBO;
   private static int depthTexture;
   private int width = GLFrame.getWidth();
   private int height = GLFrame.getHeight();
   private static boolean init;

   public DepthBufferScene(GameClientState var1) {
      this.state = var1;
   }

   public void onInit() throws GraphicsException {
      depthFBO = GL30.glGenFramebuffers();
      depthTexture = GL11.glGenTextures();
      GL11.glBindTexture(3553, depthTexture);
      GL11.glTexParameteri(3553, 10242, 33071);
      GL11.glTexParameteri(3553, 10243, 33071);
      GL11.glTexParameteri(3553, 10241, 9728);
      GlUtil.printGlErrorCritical();
      GL11.glTexParameteri(3553, 10240, 9728);
      GlUtil.printGlErrorCritical();
      GL11.glTexImage2D(3553, 0, 33191, this.width, this.height, 0, 6402, 5126, (ByteBuffer)null);
      GL30.glBindFramebuffer(36160, depthFBO);
      GL30.glFramebufferTexture2D(36160, 36096, 3553, depthTexture, 0);
      GL11.glDrawBuffer(0);
      if (GL30.glCheckFramebufferStatus(36160) != 36053) {
         throw new GraphicsException("FBO FAILED TO INIT");
      } else {
         GL11.glBindTexture(3553, 0);
         GL30.glBindFramebuffer(36160, 0);
         init = true;
      }
   }

   public void createDepthTexture() throws GraphicsException {
      if (!init) {
         this.onInit();
      }

      GL30.glBindFramebuffer(36160, depthFBO);
      GL11.glDepthMask(true);
      GL11.glClearColor(0.0F, 0.0F, 1.0F, 1.0F);
      GL11.glClear(16640);
      GlUtil.glEnable(2929);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
      this.state.getWorldDrawer().getSegmentDrawer().setSegmentRenderPass(SegmentDrawer.SegmentRenderPass.ALL);
      this.state.getWorldDrawer().prepareCamera();
      GL11.glDepthRange(0.0D, 1.0D);
      this.state.getWorldDrawer().getSegmentDrawer().draw(SegmentDrawer.shader, ShaderLibrary.depthCubeShader, false, false, (SegmentOcclusion)null, this.state.getNumberOfUpdate());
      this.state.getWorldDrawer().getSegmentDrawer().drawCubeLod(true);
      this.state.getWorldDrawer().getSegmentDrawer().enableCulling(true);
      this.state.getWorldDrawer().getCharacterDrawer().shadow = true;
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      GL20.glUseProgram(0);
      GlUtil.glMatrixMode(5889);
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5888);
      this.state.getWorldDrawer().getCharacterDrawer().draw();
      this.state.getWorldDrawer().getShards().draw();
      if (this.state.getWorldDrawer().getCreatureTool() != null) {
         this.state.getWorldDrawer().getCreatureTool().draw();
      }

      this.state.getWorldDrawer().getCharacterDrawer().shadow = false;
      GlUtil.glEnable(2884);
      GL30.glBindFramebuffer(36160, 0);
      GL11.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
      GL11.glDepthMask(true);
      GlUtil.glEnable(2929);
   }

   public void drawDepthTextureOnScreen(FrameBufferObjects var1) {
      if (var1 != null) {
         var1.enable();
      }

      this.prepareDraw(depthTexture);
      GL11.glBegin(7);
      GL11.glTexCoord2f(0.0F, 0.0F);
      GL11.glVertex2f(0.0F, 0.0F);
      GL11.glTexCoord2f(0.0F, 1.0F);
      GL11.glVertex2f(0.0F, (float)GLFrame.getHeight());
      GL11.glTexCoord2f(1.0F, 1.0F);
      GL11.glVertex2f((float)GLFrame.getWidth(), (float)GLFrame.getHeight());
      GL11.glTexCoord2f(1.0F, 0.0F);
      GL11.glVertex2f((float)GLFrame.getWidth(), 0.0F);
      GL11.glEnd();
      this.finishUpDraw();
      if (var1 != null) {
         var1.disable();
      }

   }

   private void prepareDraw(int var1) {
      GlUtil.glMatrixMode(5889);
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      GlUtil.gluOrtho2D(0.0F, (float)GLFrame.getWidth(), 0.0F, (float)GLFrame.getHeight());
      GlUtil.glMatrixMode(5888);
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      GlUtil.glDisable(2896);
      GlUtil.glDisable(2929);
      GlUtil.glEnable(3553);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, var1);
      GlUtil.glDisable(2884);
   }

   private void finishUpDraw() {
      GlUtil.glEnable(2884);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glDisable(3553);
      GlUtil.glEnable(2896);
      GlUtil.glDisable(3042);
      GlUtil.glEnable(2929);
      GlUtil.glMatrixMode(5889);
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5888);
      GlUtil.glPopMatrix();
   }

   public int getDepthTextureId() {
      return depthTexture;
   }

   public static float getNearPlane() {
      return AbstractScene.getNearPlane();
   }

   public static float getFarPlane() {
      return AbstractScene.getFarPlane();
   }
}
