package org.schema.game.mod.listeners;

public interface BlockInitializationListener extends ModListener {
   void onInitializeBlockData();
}
