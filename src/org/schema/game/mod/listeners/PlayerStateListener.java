package org.schema.game.mod.listeners;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.Sendable;

public interface PlayerStateListener extends ModListener {
   void onPlayerKilled(PlayerState var1, Damager var2);

   void onPlayerCreated(PlayerState var1);

   void onPlayerSpawned(PlayerState var1, PlayerCharacter var2);

   void onPlayerCreditsChanged(PlayerState var1);

   void onPlayerUpdate(PlayerState var1, Timer var2);

   void onPlayerSectorChanged(PlayerState var1);

   void onPlayerRemoved(PlayerState var1);

   void onPlayerChangedContol(PlayerState var1, PlayerControllable var2, Vector3i var3, Sendable var4, Vector3i var5);
}
