package org.schema.game.mod.listeners;

public interface NetworkInitializationListener extends ModListener {
   void registerNetworkClasses();

   void registerRemoteClasses();
}
