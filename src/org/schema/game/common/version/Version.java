package org.schema.game.common.version;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.schine.resource.FileExt;

public class Version {
   private static final String versionFile = "version.txt";
   private static final String devVersionFile = "version_dev";
   public static String VERSION = "0.0.0";
   public static String build = "undefined";
   public static String OS;

   public static void loadVersion() {
      loadVersion("");
   }

   public static boolean is64Bit() {
      return System.getProperty("os.arch").contains("64");
   }

   public static void loadVersion(String var0) {
      if (!var0.isEmpty() && !var0.endsWith(File.separator)) {
         var0 = var0 + File.separator;
      } else if (var0.isEmpty()) {
         var0 = "." + File.separator;
      }

      System.err.println("[VERSION] loading version from install dir: " + var0);

      try {
         FileExt var1 = new FileExt(var0 + "version_dev");
         VERSION = "0.0.0";
         build = "undefined";
         BufferedReader var3;
         if (var1.exists()) {
            VERSION = (var3 = new BufferedReader(new FileReader(var1))).readLine();
            build = "latest";
            var3.close();
         } else if ((var1 = new FileExt(var0 + "version.txt")).exists()) {
            String[] var4;
            VERSION = (var4 = (var3 = new BufferedReader(new FileReader(var1))).readLine().split("#"))[0].trim();
            build = var4[1].trim();
            var3.close();
         }

         if (build == null) {
            throw new VersionNotFoundException("no version file found, or coudn't parse");
         }
      } catch (Exception var2) {
         var2.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var2);
      }

      System.out.println("[VERSION] VERSION: " + VERSION);
      System.out.println("[VERSION] BUILD: " + build);
   }

   public static boolean equalVersion(String var0) {
      return VERSION.compareTo(var0) == 0;
   }

   public static int compareVersion(String var0) {
      return var0.contains("n/a") ? -1 : compareVersion(VERSION, var0);
   }

   public static int compareVersion(String var0, String var1) {
      if (var0.split("\\.").length <= 2) {
         var0 = "0." + Float.parseFloat(var0);
      }

      if (var1.split("\\.").length <= 2) {
         var1 = "0." + Float.parseFloat(var1);
      }

      return compareSemVersion(var1, var0);
   }

   private static int compareSemVersion(String var0, String var1) {
      if (var0.trim().equals(var1.trim())) {
         return 0;
      } else {
         String[] var6 = var0.split("\\.");
         String[] var8 = var1.split("\\.");

         assert var6.length == 3;

         assert var8.length == 3;

         int var2 = Integer.parseInt(var6[0]);
         int var3 = Integer.parseInt(var6[1]);
         int var7 = Integer.parseInt(var6[2]);
         int var4 = Integer.parseInt(var8[0]);
         int var5 = Integer.parseInt(var8[1]);
         int var9 = Integer.parseInt(var8[2]);
         if (var2 > var4) {
            return 1;
         } else if (var2 < var4) {
            return -1;
         } else if (var3 > var5) {
            return 1;
         } else if (var3 < var5) {
            return -1;
         } else if (var7 > var9) {
            return 1;
         } else {
            return var7 < var9 ? -1 : 0;
         }
      }
   }

   public static void main(String[] var0) {
      VERSION = "0.10";

      assert compareVersion("1.00") > 0;

      assert compareVersion("0.11") > 0;

      assert compareVersion("0.10") == 0;

      assert compareVersion("0.09") < 0;

      assert compareVersion("0.001") < 0;

      VERSION = "0.10.0";

      assert compareVersion("1.00.1") > 0;

      assert compareVersion("0.11.0") > 0;

      assert compareVersion("0.10.0") == 0;

      assert compareVersion("0.10.1") > 0;

      assert compareVersion("0.09.9") < 0;

      assert compareVersion("0.66") < 0;

      assert compareVersion("0.001.0") < 0;

      VERSION = "0.10.55";

      assert compareVersion("1.00.55") > 0;

      assert compareVersion("0.10.54") < 0;

      assert compareVersion("123.123") > 0;

   }

   public static boolean isDev() {
      return "latest".equals(build);
   }
}
