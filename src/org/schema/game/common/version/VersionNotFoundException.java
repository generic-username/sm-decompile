package org.schema.game.common.version;

public class VersionNotFoundException extends Exception {
   private static final long serialVersionUID = 1L;

   public VersionNotFoundException(String var1) {
      super(var1);
   }
}
