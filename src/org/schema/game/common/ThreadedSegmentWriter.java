package org.schema.game.common;

import java.io.IOException;
import org.schema.game.common.data.world.RemoteSegment;

public class ThreadedSegmentWriter {
   private boolean shutdown;
   private Queue requests = new Queue();

   public void shutdown() {
      this.shutdown = true;
      this.requests.shutdown();
   }

   public ThreadedSegmentWriter(String var1) {
      Runnable var2 = new Runnable() {
         public void run() {
            while(!ThreadedSegmentWriter.this.shutdown) {
               try {
                  RemoteSegment var1 = (RemoteSegment)ThreadedSegmentWriter.this.requests.take();
                  if (ThreadedSegmentWriter.this.shutdown) {
                     return;
                  }

                  ThreadedSegmentWriter.this.realWrite(var1);
               } catch (InterruptedException var2) {
               } catch (IOException var3) {
                  var3.printStackTrace();
               }
            }

         }
      };
      Thread var3;
      (var3 = new Thread(var2)).setDaemon(true);
      var3.setName(var1 + "_SEGMENT_WRITER_THREAD");
      var3.start();
   }

   public void finish(RemoteSegment var1) {
      while(var1 != null && this.requests.contains(var1)) {
         try {
            System.err.println("WAITING TO FINISH WRITING " + var1);
            Thread.sleep(100L);
         } catch (InterruptedException var2) {
            var2.printStackTrace();
         }
      }

   }

   private void realWrite(RemoteSegment var1) throws IOException {
      var1.getSegmentController().getSegmentProvider().writeSegment(var1, var1.getLastChanged());
   }

   public void queueWrite(RemoteSegment var1) {
      this.requests.add(var1);
   }
}
