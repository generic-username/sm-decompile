package org.schema.game.common;

import java.io.BufferedInputStream;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Scanner;
import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL10;
import org.lwjgl.util.WaveData;
import org.schema.common.util.data.ResourceUtil;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.resource.FileExt;

public class OpenALTest {
   IntBuffer buffer = BufferUtils.createIntBuffer(1);
   IntBuffer source = BufferUtils.createIntBuffer(1);
   FloatBuffer sourcePos = (FloatBuffer)BufferUtils.createFloatBuffer(3).put(new float[]{0.0F, 0.0F, 0.0F}).rewind();
   FloatBuffer sourceVel = (FloatBuffer)BufferUtils.createFloatBuffer(3).put(new float[]{0.0F, 0.0F, 0.0F}).rewind();
   FloatBuffer listenerPos = (FloatBuffer)BufferUtils.createFloatBuffer(3).put(new float[]{0.0F, 0.0F, 0.0F}).rewind();
   FloatBuffer listenerVel = (FloatBuffer)BufferUtils.createFloatBuffer(3).put(new float[]{0.0F, 0.0F, 0.0F}).rewind();
   FloatBuffer listenerOri = (FloatBuffer)BufferUtils.createFloatBuffer(6).put(new float[]{0.0F, 0.0F, -1.0F, 0.0F, 1.0F, 0.0F}).rewind();

   public static void main(String[] var0) {
      try {
         LibLoader.loadNativeLibs(false, -1, true);
         (new OpenALTest()).execute();
      } catch (Exception var1) {
         var1.printStackTrace();
      }
   }

   public void execute() {
      System.err.println("ERROR? " + AL10.alGetError());
      if (this.loadALData() == 0) {
         System.out.println("Error loading data.");
      } else {
         this.setListenerValues();
         System.out.println("OpenAL Tutorial 1 - Single Static Source");
         System.out.println("[Menu]");
         System.out.println("p - Play the sample.");
         System.out.println("s - Stop the sample.");
         System.out.println("h - Pause the sample.");
         System.out.println("q - Quit the program.");
         char var1 = ' ';
         AL10.alSourcePlay(this.source.get(0));
         Scanner var2 = new Scanner(System.in);

         while(var1 != 'q') {
            try {
               System.out.print("Input: ");
               var1 = var2.nextLine().charAt(0);
            } catch (Exception var3) {
               var1 = 'q';
            }

            switch(var1) {
            case 'h':
               AL10.alSourcePause(this.source.get(0));
               break;
            case 'p':
               System.err.println("PLAY");
               AL10.alSourcePlay(this.source.get(0));
               break;
            case 's':
               AL10.alSourceStop(this.source.get(0));
            }
         }

         this.killALData();
      }
   }

   void killALData() {
      AL10.alDeleteSources(this.source);
      AL10.alDeleteBuffers(this.buffer);
   }

   int loadALData() {
      AL10.alGenBuffers(this.buffer);
      if (AL10.alGetError() != 0) {
         return 0;
      } else {
         FileExt var1 = new FileExt("./data/audio-resource/Explosions/FancyPants.wav");
         ResourceUtil var2 = new ResourceUtil();
         String var3 = var1.getAbsolutePath();
         System.err.println("EXISTS: " + var1.exists() + " -> " + var3);
         WaveData var5 = null;

         try {
            var5 = WaveData.create(new BufferedInputStream(var2.getResourceAsInputStream(var3)));
         } catch (ResourceException var4) {
            var4.printStackTrace();
         }

         System.err.println(var5 + " " + var5.format + ", " + var5.data + ", " + var5.samplerate);
         AL10.alBufferData(this.buffer.get(0), var5.format, var5.data, var5.samplerate);
         var5.dispose();
         AL10.alGenSources(this.source);
         if (AL10.alGetError() != 0) {
            return 0;
         } else {
            AL10.alSourcei(this.source.get(0), 4105, this.buffer.get(0));
            AL10.alSourcef(this.source.get(0), 4099, 1.0F);
            AL10.alSourcef(this.source.get(0), 4106, 1.0F);
            AL10.alSource(this.source.get(0), 4100, this.sourcePos);
            AL10.alSource(this.source.get(0), 4102, this.sourceVel);
            return AL10.alGetError() == 0 ? 1 : 0;
         }
      }
   }

   void setListenerValues() {
      AL10.alListener(4100, this.listenerPos);
      AL10.alListener(4102, this.listenerVel);
      AL10.alListener(4111, this.listenerOri);
   }
}
