package org.schema.game.common.updater;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import org.schema.game.common.version.Version;

public class InstallOptions extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private JComboBox comboBox;

   public InstallOptions(final UpdatePanel var1, JFrame var2) {
      super(var2);
      this.setBounds(100, 100, 887, 354);
      this.setTitle("Install Options");
      this.getContentPane().setLayout(new BorderLayout());
      JPanel var6;
      (var6 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var6, "South");
      JButton var3;
      (var3 = new JButton("OK")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            try {
               InstallOptions.this.dispose();
            } catch (Exception var2) {
               var2.printStackTrace();
               JOptionPane.showOptionDialog(new JPanel(), "Exception: " + var2.getClass().getSimpleName() + ": " + var2.getMessage(), "ERROR", 0, 0, (Icon)null, (Object[])null, (Object)null);
            }
         }
      });
      var3.setActionCommand("OK");
      var6.add(var3);
      this.getRootPane().setDefaultButton(var3);
      (var3 = new JButton("Cancel")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            InstallOptions.this.dispose();
         }
      });
      var3.setActionCommand("Cancel");
      var6.add(var3);
      JTabbedPane var7 = new JTabbedPane(1);
      this.getContentPane().add(var7, "North");
      var7.addTab("Download Options", (Icon)null, this.contentPanel, (String)null);
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      GridBagLayout var10;
      (var10 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var10.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
      var10.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var10.rowWeights = new double[]{0.0D, 1.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 1.0D, Double.MIN_VALUE};
      this.contentPanel.setLayout(var10);
      JPanel var11;
      (var11 = new JPanel()).setBorder(new TitledBorder(new EtchedBorder(1, (Color)null, (Color)null), "Build Switcher", 4, 2, (Font)null, (Color)null));
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var4.fill = 1;
      var4.gridx = 0;
      var4.gridy = 2;
      this.contentPanel.add(var11, var4);
      GridBagLayout var5;
      (var5 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var5.rowHeights = new int[]{0, 0, 0};
      var5.columnWeights = new double[]{0.0D, 1.0D, Double.MIN_VALUE};
      var5.rowWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var11.setLayout(var5);
      IndexFileEntry[] var8 = new IndexFileEntry[var1.updater.versions.size()];

      for(int var13 = 0; var13 < var8.length; ++var13) {
         var8[var13] = (IndexFileEntry)var1.updater.versions.get(var13);
      }

      this.comboBox = new JComboBox(var8);
      (var4 = new GridBagConstraints()).gridwidth = 2;
      var4.insets = new Insets(0, 0, 5, 0);
      var4.fill = 2;
      var4.gridx = 0;
      var4.gridy = 0;
      var11.add(this.comboBox, var4);
      if (!Version.build.equals("undefined")) {
         for(int var9 = 0; var9 < var1.updater.versions.size(); ++var9) {
            IndexFileEntry var17;
            if ((var17 = (IndexFileEntry)var1.updater.versions.get(var9)).equals(Version.build)) {
               this.comboBox.setSelectedItem(var17);
            }
         }
      } else if (var1.updater.versions.size() > 0) {
         IndexFileEntry var12 = (IndexFileEntry)var1.updater.versions.get(var1.updater.versions.size() - 1);
         this.comboBox.setSelectedItem(var12);
      }

      JButton var14;
      (var14 = new JButton("Search newest")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            if (var1.updater.versions.size() > 0) {
               IndexFileEntry var2 = (IndexFileEntry)var1.updater.versions.get(var1.updater.versions.size() - 1);
               InstallOptions.this.comboBox.setSelectedItem(var2);
            }

         }
      });
      (var4 = new GridBagConstraints()).anchor = 17;
      var4.insets = new Insets(0, 0, 0, 5);
      var4.gridx = 0;
      var4.gridy = 1;
      var11.add(var14, var4);
      (var14 = new JButton("Search currently installed")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            if (!Version.build.equals("undefined")) {
               for(int var3 = 0; var3 < var1.updater.versions.size(); ++var3) {
                  IndexFileEntry var2;
                  if ((var2 = (IndexFileEntry)var1.updater.versions.get(var3)).build.equals(Version.build)) {
                     InstallOptions.this.comboBox.setSelectedItem(var2);
                  }
               }
            }

         }
      });
      (var4 = new GridBagConstraints()).anchor = 13;
      var4.gridx = 1;
      var4.gridy = 1;
      var11.add(var14, var4);
      (var6 = new JPanel()).setBorder(new TitledBorder((Border)null, "Other Options", 4, 2, (Font)null, (Color)null));
      GridBagConstraints var15;
      (var15 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var15.fill = 1;
      var15.gridx = 0;
      var15.gridy = 3;
      this.contentPanel.add(var6, var15);
      (var10 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var10.rowHeights = new int[]{0, 0, 0, 0};
      var10.columnWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var10.rowWeights = new double[]{0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var6.setLayout(var10);
      JButton var18;
      (var18 = new JButton("Download Normal")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            var1.updater.startUpdateNew(UpdatePanel.installDir, (IndexFileEntry)InstallOptions.this.comboBox.getSelectedItem(), false, 0);
            InstallOptions.this.dispose();
         }
      });
      GridBagConstraints var16;
      (var16 = new GridBagConstraints()).anchor = 17;
      var16.insets = new Insets(0, 0, 5, 5);
      var16.gridx = 0;
      var16.gridy = 0;
      var6.add(var18, var16);
      (var18 = new JButton("Force Download All & Overwrite")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            var1.updater.startUpdateNew(UpdatePanel.installDir, (IndexFileEntry)InstallOptions.this.comboBox.getSelectedItem(), true, 0);
            InstallOptions.this.dispose();
         }
      });
      (var16 = new GridBagConstraints()).weightx = 1.0D;
      var16.anchor = 13;
      var16.insets = new Insets(0, 0, 5, 0);
      var16.gridx = 1;
      var16.gridy = 0;
      var6.add(var18, var16);
      JLabel var19 = new JLabel("Use normal download to repair your current installation or quickly switch to another version");
      (var16 = new GridBagConstraints()).anchor = 17;
      var16.insets = new Insets(0, 5, 5, 0);
      var16.gridwidth = 2;
      var16.gridx = 0;
      var16.gridy = 1;
      var6.add(var19, var16);
      var19 = new JLabel("Use \"Force Download All & Override\" to re-download and replace every file (takes longer)");
      (var16 = new GridBagConstraints()).anchor = 17;
      var16.gridwidth = 2;
      var16.insets = new Insets(0, 5, 5, 0);
      var16.gridx = 0;
      var16.gridy = 2;
      var6.add(var19, var16);
   }
}
