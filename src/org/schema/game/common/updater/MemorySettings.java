package org.schema.game.common.updater;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import org.schema.common.util.security.OperatingSystem;
import org.schema.schine.resource.FileExt;

public class MemorySettings extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private JTextField textField;
   private JTextField textField_1;
   private JTextField textField_2;
   private JTextField textField_3;
   private JTextField textField_4;
   private JTextField textField_5;

   public MemorySettings(JFrame var1) {
      super(var1);
      this.setTitle("Memory Settings");
      this.setBounds(100, 100, 387, 354);
      this.getContentPane().setLayout(new BorderLayout());
      JPanel var5;
      (var5 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var5, "South");
      JButton var2;
      (var2 = new JButton("OK")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            try {
               if (MemorySettings.is64Bit()) {
                  UpdatePanel.maxMemory = Integer.parseInt(MemorySettings.this.textField.getText());
                  UpdatePanel.minMemory = Integer.parseInt(MemorySettings.this.textField_1.getText());
                  UpdatePanel.earlyGenMemory = Integer.parseInt(MemorySettings.this.textField_2.getText());
               } else {
                  UpdatePanel.maxMemory32 = Integer.parseInt(MemorySettings.this.textField.getText());
                  UpdatePanel.minMemory32 = Integer.parseInt(MemorySettings.this.textField_1.getText());
                  UpdatePanel.earlyGenMemory32 = Integer.parseInt(MemorySettings.this.textField_2.getText());
               }

               UpdatePanel.serverMaxMemory = Integer.parseInt(MemorySettings.this.textField_3.getText());
               UpdatePanel.serverMinMemory = Integer.parseInt(MemorySettings.this.textField_4.getText());
               UpdatePanel.serverEarlyGenMemory = Integer.parseInt(MemorySettings.this.textField_5.getText());

               try {
                  MemorySettings.saveSettings();
               } catch (Exception var2) {
                  var2.printStackTrace();
                  JOptionPane.showOptionDialog(new JPanel(), "Settings applied but failed to save for next session", "ERROR", 0, 0, (Icon)null, (Object[])null, (Object)null);
               }

               MemorySettings.this.dispose();
            } catch (Exception var3) {
               var3.printStackTrace();
               JOptionPane.showOptionDialog(new JPanel(), "Please only use numbers", "ERROR", 0, 0, (Icon)null, (Object[])null, (Object)null);
            }
         }
      });
      var2.setActionCommand("OK");
      var5.add(var2);
      this.getRootPane().setDefaultButton(var2);
      (var2 = new JButton("Cancel")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            MemorySettings.this.dispose();
         }
      });
      var2.setActionCommand("Cancel");
      var5.add(var2);
      JTabbedPane var6 = new JTabbedPane(1);
      this.getContentPane().add(var6, "North");
      var6.addTab("Client & SinglePlayer", (Icon)null, this.contentPanel, (String)null);
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      GridBagLayout var9;
      (var9 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var9.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
      var9.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var9.rowWeights = new double[]{0.0D, 1.0D, 0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      this.contentPanel.setLayout(var9);
      JLabel var10 = new JLabel("Client & Single Player Memory Settings");
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.gridx = 0;
      var3.gridy = 0;
      this.contentPanel.add(var10, var3);
      JTextPane var11;
      (var11 = new JTextPane()).setText("Please keep in mind that 32 bit OS have a limit of allocating memory. Should 1024 throw out of memory exceptions, please try less then 1024");
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.fill = 1;
      var3.gridx = 0;
      var3.gridy = 1;
      this.contentPanel.add(var11, var3);
      JPanel var12;
      (var12 = new JPanel()).setBorder(new TitledBorder(new EtchedBorder(1, (Color)null, (Color)null), "Maximal Memory (MB)", 4, 2, (Font)null, (Color)null));
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.fill = 1;
      var3.gridx = 0;
      var3.gridy = 2;
      this.contentPanel.add(var12, var3);
      GridBagLayout var13;
      (var13 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var13.rowHeights = new int[]{0, 0};
      var13.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var13.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var12.setLayout(var13);
      this.textField = new JTextField();
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).fill = 2;
      var4.gridx = 0;
      var4.gridy = 0;
      var12.add(this.textField, var4);
      if (is64Bit()) {
         this.textField.setText(String.valueOf(UpdatePanel.maxMemory));
      } else {
         this.textField.setText(String.valueOf(UpdatePanel.maxMemory32));
      }

      this.textField.setColumns(10);
      (var12 = new JPanel()).setBorder(new TitledBorder(new EtchedBorder(1, (Color)null, (Color)null), "Initial Memory (MB)", 4, 2, (Font)null, (Color)null));
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.fill = 1;
      var3.gridx = 0;
      var3.gridy = 3;
      this.contentPanel.add(var12, var3);
      (var13 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var13.rowHeights = new int[]{0, 0};
      var13.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var13.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var12.setLayout(var13);
      this.textField_1 = new JTextField();
      (var4 = new GridBagConstraints()).fill = 2;
      var4.gridx = 0;
      var4.gridy = 0;
      var12.add(this.textField_1, var4);
      if (is64Bit()) {
         this.textField_1.setText(String.valueOf(UpdatePanel.minMemory));
      } else {
         this.textField_1.setText(String.valueOf(UpdatePanel.minMemory32));
      }

      this.textField_1.setColumns(10);
      (var12 = new JPanel()).setBorder(new TitledBorder(new EtchedBorder(1, (Color)null, (Color)null), "Early Generation Memory", 4, 2, (Font)null, (Color)null));
      (var3 = new GridBagConstraints()).fill = 1;
      var3.gridx = 0;
      var3.gridy = 4;
      this.contentPanel.add(var12, var3);
      (var13 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var13.rowHeights = new int[]{0, 0};
      var13.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var13.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var12.setLayout(var13);
      this.textField_2 = new JTextField();
      (var4 = new GridBagConstraints()).fill = 2;
      var4.gridx = 0;
      var4.gridy = 0;
      var12.add(this.textField_2, var4);
      if (is64Bit()) {
         this.textField_2.setText(String.valueOf(UpdatePanel.earlyGenMemory));
      } else {
         this.textField_2.setText(String.valueOf(UpdatePanel.earlyGenMemory32));
      }

      this.textField_2.setColumns(10);
      (var12 = new JPanel()).setBorder(new EmptyBorder(5, 5, 5, 5));
      var6.addTab("Dedicated Server", (Icon)null, var12, (String)null);
      (var13 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var13.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
      var13.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var13.rowWeights = new double[]{0.0D, 1.0D, 0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var12.setLayout(var13);
      JLabel var14 = new JLabel("Dedicated Server Memory Settings");
      (var4 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var4.gridx = 0;
      var4.gridy = 0;
      var12.add(var14, var4);
      JTextPane var15;
      (var15 = new JTextPane()).setText("Please keep in mind that 32 bit OS have a limit of allocating memory. Should 1024 throw out of memory exceptions, please try less then 1024");
      (var4 = new GridBagConstraints()).fill = 1;
      var4.insets = new Insets(0, 0, 5, 0);
      var4.gridx = 0;
      var4.gridy = 1;
      var12.add(var15, var4);
      JPanel var16;
      (var16 = new JPanel()).setBorder(new TitledBorder(new EtchedBorder(1, (Color)null, (Color)null), "Maximal Memory (MB)", 4, 2, (Font)null, (Color)null));
      (var4 = new GridBagConstraints()).fill = 1;
      var4.insets = new Insets(0, 0, 5, 0);
      var4.gridx = 0;
      var4.gridy = 2;
      var12.add(var16, var4);
      GridBagLayout var7;
      (var7 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var7.rowHeights = new int[]{0, 0};
      var7.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var7.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var16.setLayout(var7);
      this.textField_3 = new JTextField();
      this.textField_3.setText(String.valueOf(UpdatePanel.serverMaxMemory));
      this.textField_3.setColumns(10);
      GridBagConstraints var8;
      (var8 = new GridBagConstraints()).fill = 2;
      var8.gridx = 0;
      var8.gridy = 0;
      var16.add(this.textField_3, var8);
      (var16 = new JPanel()).setBorder(new TitledBorder(new EtchedBorder(1, (Color)null, (Color)null), "Initial Memory (MB)", 4, 2, (Font)null, (Color)null));
      (var4 = new GridBagConstraints()).fill = 1;
      var4.insets = new Insets(0, 0, 5, 0);
      var4.gridx = 0;
      var4.gridy = 3;
      var12.add(var16, var4);
      (var7 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var7.rowHeights = new int[]{0, 0};
      var7.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var7.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var16.setLayout(var7);
      this.textField_4 = new JTextField();
      this.textField_4.setText(String.valueOf(UpdatePanel.serverMinMemory));
      this.textField_4.setColumns(10);
      (var8 = new GridBagConstraints()).fill = 2;
      var8.gridx = 0;
      var8.gridy = 0;
      var16.add(this.textField_4, var8);
      (var16 = new JPanel()).setBorder(new TitledBorder(new EtchedBorder(1, (Color)null, (Color)null), "Early Generation Memory", 4, 2, (Font)null, (Color)null));
      (var4 = new GridBagConstraints()).fill = 1;
      var4.gridx = 0;
      var4.gridy = 4;
      var12.add(var16, var4);
      (var7 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var7.rowHeights = new int[]{0, 0};
      var7.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var7.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var16.setLayout(var7);
      this.textField_5 = new JTextField();
      this.textField_5.setText(String.valueOf(UpdatePanel.serverEarlyGenMemory));
      this.textField_5.setColumns(10);
      (var8 = new GridBagConstraints()).fill = 2;
      var8.gridx = 0;
      var8.gridy = 0;
      var16.add(this.textField_5, var8);
   }

   public static boolean is64Bit() {
      return System.getProperty("os.arch").contains("64");
   }

   public static void loadSettings() throws IOException {
      FileExt var0 = new FileExt(OperatingSystem.getAppDir(), "settings.properties");
      Properties var1 = new Properties();
      if (var0.exists()) {
         FileInputStream var2 = new FileInputStream(var0);
         var1.load(var2);
         var2.close();
         if (var1.get("maxMemory") != null) {
            UpdatePanel.maxMemory = Integer.parseInt(var1.get("maxMemory").toString());
         }

         if (var1.get("minMemory") != null) {
            UpdatePanel.minMemory = Integer.parseInt(var1.get("minMemory").toString());
         }

         if (var1.get("earlyGenMemory") != null) {
            UpdatePanel.earlyGenMemory = Integer.parseInt(var1.get("earlyGenMemory").toString());
         }

         if (var1.get("maxMemory32") != null) {
            UpdatePanel.maxMemory32 = Integer.parseInt(var1.get("maxMemory32").toString());
         }

         if (var1.get("minMemory32") != null) {
            UpdatePanel.minMemory32 = Integer.parseInt(var1.get("minMemory32").toString());
         }

         if (var1.get("earlyGenMemory32") != null) {
            UpdatePanel.earlyGenMemory32 = Integer.parseInt(var1.get("earlyGenMemory32").toString());
         }

         if (var1.get("serverMaxMemory") != null) {
            UpdatePanel.serverMaxMemory = Integer.parseInt(var1.get("serverMaxMemory").toString());
         }

         if (var1.get("serverMinMemory") != null) {
            UpdatePanel.serverMinMemory = Integer.parseInt(var1.get("serverMinMemory").toString());
         }

         if (var1.get("serverEarlyGenMemory") != null) {
            UpdatePanel.serverEarlyGenMemory = Integer.parseInt(var1.get("serverEarlyGenMemory").toString());
         }

         if (var1.get("port") != null) {
            UpdatePanel.port = Integer.parseInt(var1.get("port").toString());
         }

         if (var1.get("installDir") != null) {
            UpdatePanel.installDir = var1.get("installDir").toString();
         }

         if (var1.get("buildBranch") != null) {
            UpdatePanel.buildBranch = Updater.VersionFile.valueOf(var1.get("buildBranch").toString());
         }

         var0.createNewFile();
         FileOutputStream var3 = new FileOutputStream(var0);
         var1.store(var3, "Properties for the StarMade Starter");
         var3.flush();
         var3.close();
      } else {
         System.err.println("ERROR, FILE DOES NOT EXIST: " + var0.getAbsolutePath());
      }
   }

   public static void main(String[] var0) {
      try {
         MemorySettings var2;
         (var2 = new MemorySettings(new JFrame())).setDefaultCloseOperation(2);
         var2.setVisible(true);
      } catch (Exception var1) {
         var1.printStackTrace();
      }
   }

   public static void saveSettings() throws IOException {
      FileExt var0 = new FileExt(OperatingSystem.getAppDir(), "settings.properties");
      Properties var1;
      (var1 = new Properties()).put("maxMemory", String.valueOf(UpdatePanel.maxMemory));
      var1.put("minMemory", String.valueOf(UpdatePanel.minMemory));
      var1.put("earlyGenMemory", String.valueOf(UpdatePanel.earlyGenMemory));
      var1.put("maxMemory32", String.valueOf(UpdatePanel.maxMemory32));
      var1.put("minMemory32", String.valueOf(UpdatePanel.minMemory32));
      var1.put("earlyGenMemory32", String.valueOf(UpdatePanel.earlyGenMemory32));
      var1.put("serverMaxMemory", String.valueOf(UpdatePanel.serverMaxMemory));
      var1.put("serverMinMemory", String.valueOf(UpdatePanel.serverMinMemory));
      var1.put("serverEarlyGenMemory", String.valueOf(UpdatePanel.serverEarlyGenMemory));
      var1.put("port", String.valueOf(UpdatePanel.port));
      var1.put("installDir", UpdatePanel.installDir);
      var1.put("buildBranch", UpdatePanel.buildBranch.name());
      var0.createNewFile();
      FileOutputStream var2 = new FileOutputStream(var0);
      var1.store(var2, "Properties for the StarMade Starter");
      var2.flush();
      var2.close();
      System.out.println("Memory Settings saved to: " + var0.getAbsolutePath());
   }
}
