package org.schema.game.common.updater;

public class IndexFileEntry implements Comparable {
   final String path;
   final String version;
   final String build;
   final Updater.VersionFile v;

   public IndexFileEntry(String var1, String var2, String var3, Updater.VersionFile var4) {
      this.path = var1;
      this.version = var2;
      this.build = var3;
      this.v = var4;
   }

   public int compareTo(IndexFileEntry var1) {
      return this.build.compareToIgnoreCase(var1.build);
   }

   public String toString() {
      return this.build + " v" + this.version + " (" + this.v.name() + ")";
   }
}
