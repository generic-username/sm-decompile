package org.schema.game.common.updater;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import org.schema.common.util.StringTools;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.game.common.util.ZipGUICallback;
import org.schema.game.common.version.Version;
import org.schema.schine.resource.FileExt;

public class UpdatePanel extends JPanel implements Observer {
   private static final long serialVersionUID = 1L;
   public static int maxMemory = 1024;
   public static int minMemory = 512;
   public static int earlyGenMemory = 128;
   public static int maxMemory32 = 512;
   public static int minMemory32 = 256;
   public static int earlyGenMemory32 = 64;
   public static int serverMaxMemory = 1024;
   public static int serverMinMemory = 1024;
   public static int serverEarlyGenMemory = 256;
   public static int port = 4242;
   public static String installDir = "./StarMade/";
   public static Updater.VersionFile buildBranch;
   public static JFrame frame;
   final Updater updater;
   private JButton btnUpdateToNewest;
   private JProgressBar progressBar;
   private JButton btnRefresh;
   private JButton btnStartGame;
   private JLabel label;
   private JLabel lblNewLabel;
   private JButton btnStartDedicatedServer;
   private JLabel label_1;
   private JProgressBar progressBar_File;
   private JButton btnOptions;
   private JButton btnRegisterGameWith;

   public UpdatePanel(final JFrame var1) {
      frame = var1;

      try {
         MemorySettings.loadSettings();
      } catch (Exception var3) {
         var3.printStackTrace();
      }

      this.updater = new Updater(installDir);
      this.updater.addObserver(this);
      GridBagLayout var2;
      (var2 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0, 0, 0};
      var2.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
      var2.columnWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var2.rowWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      this.setLayout(var2);
      this.progressBar = new JProgressBar();
      this.progressBar.setPreferredSize(new Dimension(250, 30));
      this.progressBar.setMinimumSize(new Dimension(200, 34));
      this.progressBar.setStringPainted(true);
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).weightx = 1.0D;
      var5.gridwidth = 5;
      var5.fill = 1;
      var5.insets = new Insets(5, 0, 5, 0);
      var5.gridx = 0;
      var5.gridy = 0;
      this.add(this.progressBar, var5);
      this.btnStartGame = new JButton("Start Game");
      this.btnStartGame.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            UpdatePanel.this.startInstance(new String[0]);
         }
      });
      this.progressBar_File = new JProgressBar();
      this.progressBar_File.setStringPainted(true);
      this.progressBar_File.setPreferredSize(new Dimension(250, 20));
      this.progressBar_File.setMinimumSize(new Dimension(200, 14));
      (var5 = new GridBagConstraints()).weightx = 1.0D;
      var5.fill = 1;
      var5.gridwidth = 4;
      var5.insets = new Insets(0, 0, 5, 5);
      var5.gridx = 0;
      var5.gridy = 1;
      this.add(this.progressBar_File, var5);
      this.btnStartGame.setAlignmentX(1.0F);
      (var5 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var5.anchor = 13;
      var5.gridx = 4;
      var5.gridy = 1;
      this.btnStartGame.setEnabled(this.updater.lookForGame(installDir));
      this.add(this.btnStartGame, var5);
      this.label_1 = new JLabel("");
      (var5 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var5.gridx = 0;
      var5.gridy = 2;
      this.add(this.label_1, var5);
      this.btnUpdateToNewest = new JButton("Update and install latest version");
      this.btnUpdateToNewest.setFont(new Font("Tahoma", 1, 11));
      this.btnUpdateToNewest.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            UpdatePanel.this.updater.startUpdateNew(UpdatePanel.installDir, (IndexFileEntry)UpdatePanel.this.updater.versions.get(UpdatePanel.this.updater.versions.size() - 1), false, 0);
         }
      });
      this.btnRefresh = new JButton("refresh");
      this.btnRefresh.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            UpdatePanel.this.updater.reloadVersion(UpdatePanel.installDir);
            UpdatePanel.this.updater.startLoadVersionList(UpdatePanel.buildBranch);
            UpdatePanel.this.btnStartGame.setEnabled(UpdatePanel.this.updater.lookForGame(UpdatePanel.installDir));
            UpdatePanel.this.btnStartDedicatedServer.setEnabled(UpdatePanel.this.updater.lookForGame(UpdatePanel.installDir));
         }
      });
      (var5 = new GridBagConstraints()).anchor = 18;
      var5.insets = new Insets(0, 0, 5, 5);
      var5.gridx = 0;
      var5.gridy = 3;
      this.add(this.btnRefresh, var5);
      this.btnOptions = new JButton("Options & Repair");
      this.btnOptions.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            (new InstallOptions(UpdatePanel.this, var1)).setVisible(true);
         }
      });
      this.btnRegisterGameWith = new JButton("Register With Steam");
      this.btnRegisterGameWith.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            String var3 = "To keep StarMade as open as possible, the game can be played and bought outside of steam.\nThis means however, that all purchases must be registered in our central database.\n\nIf you bought the game in steam, please create an account on https://registry.star-made.org/,\nand you can upgrade your account with steam.\n\nWe're sorry for the inconvenience. We are working on an automated system.\n";
            JOptionPane.showMessageDialog(var1, var3, "Steam", 1);

            try {
               Launcher.openWebpage(new URL("https://registry.star-made.org/steam"));
            } catch (MalformedURLException var2) {
               var2.printStackTrace();
            }
         }
      });
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var4.gridx = 1;
      var4.gridy = 3;
      if (Launcher.steam) {
         this.add(this.btnRegisterGameWith, var4);
      }

      (var4 = new GridBagConstraints()).weightx = 1.0D;
      var4.anchor = 13;
      var4.insets = new Insets(0, 0, 5, 5);
      var4.gridx = 2;
      var4.gridy = 3;
      this.add(this.btnOptions, var4);
      this.btnUpdateToNewest.setAlignmentX(1.0F);
      this.btnUpdateToNewest.setEnabled(this.updater.isNewerVersionAvailable());
      (var4 = new GridBagConstraints()).anchor = 13;
      var4.insets = new Insets(0, 0, 5, 5);
      var4.gridx = 3;
      var4.gridy = 3;
      this.add(this.btnUpdateToNewest, var4);
      this.btnStartDedicatedServer = new JButton("Start Dedicated Server");
      this.btnStartDedicatedServer.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            UpdatePanel.this.startServerInstance(new String[0]);
         }
      });
      this.btnStartDedicatedServer.setEnabled(this.updater.lookForGame(installDir));
      (var4 = new GridBagConstraints()).anchor = 12;
      var4.insets = new Insets(0, 0, 5, 0);
      var4.gridx = 4;
      var4.gridy = 3;
      this.add(this.btnStartDedicatedServer, var4);
      this.label = new JLabel("");
      (var4 = new GridBagConstraints()).gridwidth = 4;
      var4.insets = new Insets(0, 0, 5, 5);
      var4.gridx = 0;
      var4.gridy = 4;
      var4.anchor = 17;
      this.add(this.label, var4);
      this.lblNewLabel = new JLabel("");
      (var4 = new GridBagConstraints()).gridwidth = 4;
      var4.insets = new Insets(0, 0, 0, 5);
      var4.gridx = 0;
      var4.gridy = 5;
      var4.anchor = 17;
      this.add(this.lblNewLabel, var4);
      this.updater.startLoadVersionList(buildBranch);
      (new Thread(new Runnable() {
         public void run() {
            try {
               Thread.sleep(1200L);
            } catch (InterruptedException var2) {
               var2.printStackTrace();
            }

            for(; UpdatePanel.this.isVisible(); EventQueue.invokeLater(new Runnable() {
               public void run() {
                  UpdatePanel.this.repaint();
               }
            })) {
               try {
                  Thread.sleep(500L);
               } catch (InterruptedException var1) {
                  var1.printStackTrace();
               }
            }

         }
      })).start();
   }

   private static String getJavaExec() {
      return !System.getProperty("os.name").equals("Mac OS X") && !System.getProperty("os.name").contains("Linux") ? "javaw" : "java";
   }

   public static boolean is64Bit() {
      return System.getProperty("os.arch").contains("64");
   }

   private void startInstance(final String[] var1) {
      SwingUtilities.invokeLater(new Runnable() {
         public void run() {
            try {
               String var1x = "";

               for(int var2 = 0; var2 < var1.length; ++var2) {
                  var1x = var1x + " " + var1[var2];
               }

               String var6 = UpdatePanel.this.updater.getStarMadeStartPath(UpdatePanel.installDir);
               FileExt var8 = new FileExt(var6);
               String[] var5;
               if (UpdatePanel.is64Bit()) {
                  var5 = new String[]{UpdatePanel.getJavaExec(), "-Djava.net.preferIPv4Stack=true", "-Xmn" + UpdatePanel.earlyGenMemory + "M", "-Xms" + UpdatePanel.minMemory + "M", "-Xmx" + UpdatePanel.maxMemory + "M", "-Xincgc", "-jar", var8.getAbsolutePath(), "-force", "-port:" + UpdatePanel.port, var1x};
               } else {
                  var5 = new String[]{UpdatePanel.getJavaExec(), "-Djava.net.preferIPv4Stack=true", "-Xmn" + UpdatePanel.earlyGenMemory32 + "M", "-Xms" + UpdatePanel.minMemory32 + "M", "-Xmx" + UpdatePanel.maxMemory32 + "M", "-Xincgc", "-jar", var8.getAbsolutePath(), "-force", "-port:" + UpdatePanel.port, var1x};
               }

               System.err.println("RUNNING COMMAND: " + var5);
               ProcessBuilder var7;
               (var7 = new ProcessBuilder(var5)).environment();
               if ((var8 = new FileExt(UpdatePanel.installDir)).exists()) {
                  var7.directory(var8.getAbsoluteFile());
                  var7.start();
                  System.err.println("Exiting because updater starting game");

                  try {
                     throw new Exception("System.exit() called");
                  } catch (Exception var3) {
                     var3.printStackTrace();
                     System.exit(0);
                  }
               } else {
                  throw new FileNotFoundException("Cannot find the Install Directory: " + UpdatePanel.installDir);
               }
            } catch (IOException var4) {
               var4.printStackTrace();
               GuiErrorHandler.processErrorDialogException(var4);
            }
         }
      });
   }

   private void startServerInstance(final String[] var1) {
      SwingUtilities.invokeLater(new Runnable() {
         public void run() {
            try {
               String var1x = "";

               for(int var2 = 0; var2 < var1.length; ++var2) {
                  var1x = var1x + " " + var1[var2];
               }

               String var7 = UpdatePanel.this.updater.getStarMadeStartPath(UpdatePanel.installDir);
               FileExt var8 = new FileExt(var7);
               String[] var5 = new String[]{UpdatePanel.getJavaExec(), "-Djava.net.preferIPv4Stack=true", "-Xmn" + UpdatePanel.serverEarlyGenMemory + "M", "-Xms" + UpdatePanel.serverMinMemory + "M", "-Xmx" + UpdatePanel.serverMaxMemory + "M", "-Xincgc", "-jar", var8.getAbsolutePath(), "-server", "-gui", "-port:" + UpdatePanel.port, var1x};
               System.err.println("RUNNING COMMAND: " + var5);
               ProcessBuilder var6;
               (var6 = new ProcessBuilder(var5)).environment();
               if ((var8 = new FileExt(UpdatePanel.installDir)).exists()) {
                  var6.directory(var8.getAbsoluteFile());
                  var6.start();

                  try {
                     throw new Exception("System.exit() called");
                  } catch (Exception var3) {
                     var3.printStackTrace();
                     System.exit(0);
                  }
               } else {
                  throw new FileNotFoundException("Cannot find the Install Directory: " + UpdatePanel.installDir);
               }
            } catch (IOException var4) {
               var4.printStackTrace();
               GuiErrorHandler.processErrorDialogException(var4);
            }
         }
      });
   }

   public void update(Observable var1, Object var2) {
      String var4;
      if (var2 != null) {
         if (var2.equals("resetbars")) {
            this.progressBar.setString("");
            this.progressBar.setValue(0);
            this.progressBar_File.setValue(0);
            this.progressBar_File.setString("");
         } else if (var2.equals("reload Versions")) {
            System.err.println("[UPDATER] trigger versions reloading");
            this.updater.startLoadVersionList(buildBranch);
            this.btnStartGame.setEnabled(this.updater.lookForGame(installDir));
            this.btnStartDedicatedServer.setEnabled(this.updater.lookForGame(installDir));
         } else if (var2.equals("versions loaded")) {
            System.err.println("[UPDATER] trigger versions loaded " + installDir);
            this.updater.reloadVersion(installDir);
            this.btnUpdateToNewest.setEnabled(this.updater.isNewerVersionAvailable());
            this.progressBar.setString("");
         } else if (var2.equals("updating")) {
            this.btnUpdateToNewest.setEnabled(false);
            this.btnStartGame.setEnabled(false);
            this.btnStartDedicatedServer.setEnabled(false);
            this.btnOptions.setEnabled(false);
            this.btnRefresh.setEnabled(false);
         } else if (var2.equals("finished")) {
            System.err.println("FINISHED Update");
            this.btnUpdateToNewest.setEnabled(false);
            this.btnRefresh.setEnabled(true);
            this.btnOptions.setEnabled(true);
            this.updater.reloadVersion(installDir);
            this.updater.startLoadVersionList(buildBranch);
            this.btnStartGame.setEnabled(this.updater.lookForGame(installDir));
            this.btnStartDedicatedServer.setEnabled(this.updater.lookForGame(installDir));
         } else if (var2.equals("reset")) {
            this.progressBar.setString("");
            this.progressBar.setValue(0);
            this.progressBar_File.setValue(0);
            this.progressBar_File.setString("");
         } else {
            int var10;
            if (var2 instanceof ZipGUICallback) {
               ZipGUICallback var7;
               var10 = (int)Math.ceil((double)((float)(var7 = (ZipGUICallback)var2).fileIndex / (float)var7.fileMax * 100.0F));
               this.progressBar.setString("Backing files up: " + var10 + "  %");
               this.progressBar.setValue(var10);
            } else {
               if (var2 instanceof FileDownloadUpdate) {
                  FileDownloadUpdate var9;
                  var10 = (int)((float)(var9 = (FileDownloadUpdate)var2).downloaded / (float)var9.size * 100.0F);
                  int var12 = (int)((float)var9.currentSize / (float)var9.totalSize * 100.0F);
                  var4 = StringTools.formatPointZero((double)var9.totalSize / 1024.0D / 1024.0D) + "MB";
                  String var5 = StringTools.formatPointZero((double)var9.currentSize / 1024.0D / 1024.0D) + "MB";
                  String var6;
                  if (var9.downloadSpeed / 1000000.0D > 0.5D) {
                     var6 = StringTools.formatPointZeroZero(var9.downloadSpeed / 1000000.0D) + " MB/sec";
                  } else {
                     var6 = StringTools.formatPointZeroZero(var9.downloadSpeed / 1000.0D) + " kB/sec";
                  }

                  this.progressBar.setString("Downloading files " + var9.index + "/" + var9.total + " (" + var5 + "/" + var4 + ") [" + var12 + "%]");
                  var4 = "(" + StringTools.formatPointZero((double)var9.size / 1024.0D / 1024.0D) + "MB) ";
                  this.progressBar_File.setString("(Downloading 5 files) largest currently: " + var9.fileName + " " + var4 + " [" + var10 + "%] " + var6);
                  this.progressBar_File.setValue(var10);
                  this.progressBar.setValue(var12);
                  return;
               }

               if (var2 instanceof String) {
                  this.progressBar.setString(var2.toString());
               }
            }
         }
      }

      boolean var8 = this.updater.lookForGame(installDir);
      FileExt var11 = new FileExt(installDir);
      String var3;
      if (Version.build != null && !Version.build.equals("undefined")) {
         var3 = "Currently installed StarMade Version: v" + Version.VERSION + "; build " + Version.build;
      } else {
         var3 = "Currently no valid StarMade version installed";
      }

      if (this.updater.isNewerVersionAvailable()) {
         if (this.updater.versions != null && !this.updater.versions.isEmpty()) {
            var3 = var3 + ". A new Version is Available! (v" + ((IndexFileEntry)this.updater.versions.get(this.updater.versions.size() - 1)).version + "; build " + ((IndexFileEntry)this.updater.versions.get(this.updater.versions.size() - 1)).build + ")";
         } else {
            var3 = var3 + ". StarMade is Available to Install!";
         }

         this.label.setForeground(Color.GREEN.darker());
      } else {
         var3 = var3 + ". You already have the latest version";
         this.label.setForeground(Color.BLACK);
      }

      this.label.setText(var3);
      var4 = (var8 ? "Installation found in " + var11.getAbsolutePath() : "No installation found in " + var11.getAbsolutePath()).replace("/./", "/").replace("\\.\\", "\\");
      this.lblNewLabel.setText(var4);
      if (var8) {
         this.lblNewLabel.setForeground(Color.GREEN.darker());
      } else {
         this.lblNewLabel.setForeground(Color.RED.darker());
      }
   }

   static {
      buildBranch = Updater.VersionFile.RELEASE;
   }
}
