package org.schema.game.common.updater;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.nio.channels.Selector;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import org.apache.commons.io.output.StringBuilderWriter;
import org.schema.game.common.util.FolderZipper;
import org.schema.schine.resource.FileExt;

public class FileUtil {
   public static final char DIR_SEPARATOR_UNIX = '/';
   public static final char DIR_SEPARATOR_WINDOWS = '\\';
   public static final char DIR_SEPARATOR;
   public static final String LINE_SEPARATOR_UNIX = "\n";
   public static final String LINE_SEPARATOR_WINDOWS = "\r\n";
   public static final String LINE_SEPARATOR;
   private static final int EOF = -1;
   private static final int DEFAULT_BUFFER_SIZE = 4096;

   public static void copyDirectory(File var0, File var1) throws IOException {
      if (!var0.isDirectory()) {
         copyFile(var0, var1);
      } else {
         if (!var1.exists()) {
            var1.mkdir();
         }

         String[] var2 = var0.list();

         for(int var3 = 0; var3 < var2.length; ++var3) {
            copyDirectory(new FileExt(var0, var2[var3]), new FileExt(var1, var2[var3]));
         }

      }
   }

   public static URL convertToURLEscapingIllegalCharacters(String var0) throws UnsupportedEncodingException, URISyntaxException, MalformedURLException {
      var0 = URLDecoder.decode(var0, "UTF-8");
      URL var1 = new URL(var0);
      return (new URI(var1.getProtocol(), var1.getUserInfo(), var1.getHost(), var1.getPort(), var1.getPath(), var1.getQuery(), var1.getRef())).toURL();
   }

   public static void copyFileIfDifferentPath(File var0, File var1) throws IOException {
      if (!var0.getCanonicalPath().equals(var1.getCanonicalPath())) {
         copyFile(var0, var1);
      } else {
         System.err.println("[IO][COPY] (NO COPY SINCE SAME PATH) FILE FROM " + var0.getAbsolutePath() + " to " + var1.getAbsolutePath());
      }
   }

   public static void copyFile(File var0, File var1) throws IOException {
      System.err.println("[IO][COPY] FILE FROM " + var0.getAbsolutePath() + " to " + var1.getAbsolutePath());
      BufferedInputStream var4 = new BufferedInputStream(new FileInputStream(var0));
      BufferedOutputStream var5 = new BufferedOutputStream(new FileOutputStream(var1));
      byte[] var2 = new byte[1024];

      int var3;
      while((var3 = var4.read(var2)) > 0) {
         var5.write(var2, 0, var3);
      }

      var5.flush();
      var4.close();
      var5.close();
   }

   public static final void copyInputStream(InputStream var0, OutputStream var1) throws IOException {
      byte[] var2 = new byte[1024];

      int var3;
      while((var3 = var0.read(var2)) >= 0) {
         var1.write(var2, 0, var3);
      }

      var0.close();
      var1.close();
   }

   public static File createTempDirectory(String var0) throws IOException {
      File var1;
      if (!(var1 = File.createTempFile(var0, Long.toString(System.nanoTime()))).delete()) {
         throw new IOException("Could not delete temp file: " + var1.getAbsolutePath());
      } else if (!var1.mkdir()) {
         throw new IOException("Could not create temp directory: " + var1.getAbsolutePath());
      } else {
         return var1;
      }
   }

   public static void deleteRecursive(File var0) throws IOException {
      if (var0.isDirectory()) {
         File[] var1;
         int var2 = (var1 = var0.listFiles()).length;

         for(int var3 = 0; var3 < var2; ++var3) {
            deleteRecursive(var1[var3]);
         }
      }

      if (!var0.delete()) {
         System.err.println("Failed to del: " + var0.getAbsolutePath());
      }

   }

   public static boolean deleteDir(File var0) {
      if (var0.isDirectory()) {
         String[] var1 = var0.list();

         for(int var2 = 0; var2 < var1.length; ++var2) {
            if (!deleteDir(new FileExt(var0, var1[var2]))) {
               return false;
            }
         }
      }

      return var0.delete();
   }

   public static void extract(File var0, String var1) throws IOException {
      extract(var0, var1, (String)null, (FolderZipper.ZipCallback)null);
   }

   public static long getExtractedFilesSize(File var0) throws ZipException, IOException {
      long var1 = 0L;
      ZipFile var8 = new ZipFile(var0);

      long var4;
      try {
         for(Enumeration var3 = var8.entries(); var3.hasMoreElements(); var1 += var4) {
            var4 = ((ZipEntry)var3.nextElement()).getSize();
         }
      } finally {
         var8.close();
      }

      return var1;
   }

   public static void extract(File var0, String var1, String var2, FolderZipper.ZipCallback var3) throws IOException {
      if (!var1.endsWith("/")) {
         var1 = var1 + "/";
      }

      ZipFile var9;
      (var9 = new ZipFile(var0)).entries();
      (new FileExt(var1)).mkdirs();
      Enumeration var4 = var9.entries();
      FileExt var5;
      (var5 = new FileExt(var1)).mkdirs();

      while(var4.hasMoreElements()) {
         ZipEntry var6;
         if ((var6 = (ZipEntry)var4.nextElement()).isDirectory()) {
            if (var2 != null) {
               (new FileExt(var1 + var6.getName().replaceFirst(var2, ""))).mkdir();
            } else {
               (new FileExt(var1 + var6.getName())).mkdir();
            }
         } else {
            new String(var6.getName());
            String var7;
            if (var2 != null) {
               var7 = new String(var6.getName().replaceFirst(var2, ""));
            } else {
               var7 = new String(var6.getName());
            }

            int var8;
            if ((var8 = var7.lastIndexOf("/")) >= 0) {
               var7 = var7.substring(0, var8);
               FileExt var11;
               if (!(var11 = new FileExt(var1 + var7)).exists()) {
                  var11.mkdirs();
               }
            }

            FileExt var12;
            if (var2 != null) {
               var12 = new FileExt(var1 + var6.getName().replaceFirst(var2, ""));
            } else {
               var12 = new FileExt(var1 + var6.getName());
            }

            if (var3 != null) {
               var3.update(var12);
            }

            System.err.println("Extracting file: " + var6.getName() + " exists: " + var5.exists() + ", is Dir: " + var5.isDirectory() + ". " + var5.getAbsolutePath());
            BufferedInputStream var13 = new BufferedInputStream(var9.getInputStream(var6));
            BufferedOutputStream var10 = new BufferedOutputStream(new FileOutputStream(var12));
            copyInputStream(var13, var10);
         }
      }

      var9.close();
   }

   public static void main(String[] var0) {
      try {
         extract(new FileExt("./sector-export/planet.smsec"), "./sector-export/mm/", (String)null, (FolderZipper.ZipCallback)null);
         deleteRecursive(new FileExt("./sector-export/mm/"));
         System.err.println((new FileExt("./sector-export/mm/")).delete());
      } catch (IOException var1) {
         var1.printStackTrace();
      }
   }

   public static void backupFile(File var0) throws IOException {
      System.err.println("BACKING UP FILE: " + var0.getAbsolutePath());
      String var1 = var0.getName();

      for(int var2 = 0; (new FileExt(var1)).exists(); ++var2) {
         var1 = var0.getName() + "." + var2;
      }

      System.err.println("BACKING UP FILE TO: " + var0.getAbsolutePath() + " -> " + var1);
      copyFile(var0, new FileExt(var1));
   }

   public static void copyURLToFile(URL var0, File var1, int var2, int var3, DownloadCallback var4) throws IOException {
      URLConnection var5;
      (var5 = var0.openConnection()).setConnectTimeout(var2);
      var5.setReadTimeout(var3);
      copyInputStreamToFile(var5.getInputStream(), var1, var4);
   }

   public static void copyURLToFile(URL var0, File var1, int var2, int var3, DownloadCallback var4, String var5, String var6, boolean var7) throws IOException {
      while(true) {
         try {
            URLConnection var13;
            (var13 = var0.openConnection()).setConnectTimeout(var2);
            var13.setReadTimeout(var3);
            BufferedInputStream var14;
            if (var7) {
               if (var1.exists()) {
                  long var8 = var1.length();
                  var13.setAllowUserInteraction(true);
                  var13.setRequestProperty("Range", "bytes=" + var8 + "-");
                  var13.setConnectTimeout(14000);
                  (var14 = new BufferedInputStream(var13.getInputStream())).skip(var8);
               } else {
                  var14 = new BufferedInputStream(var13.getInputStream());
               }
            } else {
               var14 = new BufferedInputStream(var13.getInputStream());
            }

            copyInputStreamToFile(var14, var1, var4);
            return;
         } catch (IOException var11) {
            IOException var12 = var11;
            if (!var7) {
               throw var11;
            }

            try {
               System.err.println("Disconnected: " + var12.getClass() + ": " + var12.getMessage() + "! Trying to resume download!");
               Thread.sleep(1000L);
            } catch (InterruptedException var10) {
               var10.printStackTrace();
            }

            var1 = var1;
            var0 = var0;
         }
      }
   }

   public static void copyInputStreamToFile(InputStream var0, File var1, DownloadCallback var2) throws IOException {
      try {
         FileOutputStream var9 = openOutputStream(var1);

         try {
            copy(var0, var9, var2);
            var9.close();
         } finally {
            closeQuietly((Closeable)var9);
         }
      } finally {
         closeQuietly((Closeable)var0);
      }

   }

   public static int copy(InputStream var0, OutputStream var1, DownloadCallback var2) throws IOException {
      long var3;
      return (var3 = copyLarge(var0, var1, var2)) > 2147483647L ? -1 : (int)var3;
   }

   public static Iterable getClasses(String var0) throws ClassNotFoundException, IOException {
      ClassLoader var1 = Thread.currentThread().getContextClassLoader();
      String var2 = var0.replace('.', '/');
      Enumeration var4 = var1.getResources(var2);
      ArrayList var6 = new ArrayList();

      while(var4.hasMoreElements()) {
         URL var3 = (URL)var4.nextElement();
         var6.add(new FileExt(var3.getFile()));
      }

      ArrayList var8 = new ArrayList();
      Iterator var5 = var6.iterator();

      while(var5.hasNext()) {
         File var7 = (File)var5.next();
         var8.addAll(findClasses(var7, var0));
      }

      return var8;
   }

   private static List findClasses(File var0, String var1) throws ClassNotFoundException {
      ArrayList var2 = new ArrayList();
      if (!var0.exists()) {
         return var2;
      } else {
         File[] var6;
         int var3 = (var6 = var0.listFiles()).length;

         for(int var4 = 0; var4 < var3; ++var4) {
            File var5;
            if ((var5 = var6[var4]).isDirectory()) {
               var2.addAll(findClasses(var5, var1 + "." + var5.getName()));
            } else if (var5.getName().endsWith(".class")) {
               var2.add(Class.forName(var1 + '.' + var5.getName().substring(0, var5.getName().length() - 6)));
            }
         }

         return var2;
      }
   }

   public static FileOutputStream openOutputStream(File var0) throws IOException {
      return openOutputStream(var0, false);
   }

   public static FileOutputStream openOutputStream(File var0, boolean var1) throws IOException {
      if (var0.exists()) {
         if (var0.isDirectory()) {
            throw new IOException("File '" + var0 + "' exists but is a directory");
         }

         if (!var0.canWrite()) {
            throw new IOException("File '" + var0 + "' cannot be written to");
         }
      } else {
         File var2;
         if ((var2 = var0.getParentFile()) != null && !var2.mkdirs() && !var2.isDirectory()) {
            throw new IOException("Directory '" + var2 + "' could not be created");
         }
      }

      return new FileOutputStream(var0, var1);
   }

   public static void close(URLConnection var0) {
      if (var0 instanceof HttpURLConnection) {
         ((HttpURLConnection)var0).disconnect();
      }

   }

   public static void closeQuietly(Reader var0) {
      closeQuietly((Closeable)var0);
   }

   public static void closeQuietly(Writer var0) {
      closeQuietly((Closeable)var0);
   }

   public static void closeQuietly(InputStream var0) {
      closeQuietly((Closeable)var0);
   }

   public static void closeQuietly(OutputStream var0) {
      closeQuietly((Closeable)var0);
   }

   public static void closeQuietly(Closeable var0) {
      try {
         if (var0 != null) {
            var0.close();
         }

      } catch (IOException var1) {
      }
   }

   public static void closeQuietly(Socket var0) {
      if (var0 != null) {
         try {
            var0.close();
            return;
         } catch (IOException var1) {
         }
      }

   }

   public static void closeQuietly(Selector var0) {
      if (var0 != null) {
         try {
            var0.close();
            return;
         } catch (IOException var1) {
         }
      }

   }

   public static void closeQuietly(ServerSocket var0) {
      if (var0 != null) {
         try {
            var0.close();
            return;
         } catch (IOException var1) {
         }
      }

   }

   public static long copyLarge(InputStream var0, OutputStream var1, DownloadCallback var2) throws IOException {
      return copyLarge(var0, var1, new byte[4096], var2);
   }

   public static long copyLarge(InputStream var0, OutputStream var1, byte[] var2, DownloadCallback var3) throws IOException {
      long var4 = 0L;
      boolean var6 = false;

      int var7;
      while(-1 != (var7 = var0.read(var2))) {
         var1.write(var2, 0, var7);
         var4 += (long)var7;
         if (var3 != null) {
            var3.downloaded(var4, (long)var7);
         }
      }

      return var4;
   }

   public static byte[] createChecksum(File var0) throws NoSuchAlgorithmException, IOException {
      BufferedInputStream var4 = new BufferedInputStream(new FileInputStream(var0));
      byte[] var1 = new byte[1024];
      MessageDigest var2 = MessageDigest.getInstance("SHA1");

      int var3;
      do {
         if ((var3 = var4.read(var1)) > 0) {
            var2.update(var1, 0, var3);
         }
      } while(var3 != -1);

      var4.close();
      return var2.digest();
   }

   public static byte[] createChecksum(String var0) throws NoSuchAlgorithmException, IOException {
      return createChecksum(new File(var0));
   }

   public static byte[] createChecksumZipped(String var0) throws NoSuchAlgorithmException, IOException {
      GZIPInputStream var4 = new GZIPInputStream(new BufferedInputStream(new FileInputStream(var0)));
      byte[] var1 = new byte[1024];
      MessageDigest var2 = MessageDigest.getInstance("SHA1");

      int var3;
      do {
         if ((var3 = var4.read(var1)) > 0) {
            var2.update(var1, 0, var3);
         }
      } while(var3 != -1);

      var4.close();
      return var2.digest();
   }

   public static String getSha1Checksum(String var0) throws NoSuchAlgorithmException, IOException {
      byte[] var3 = createChecksum(var0);
      String var1 = "";

      for(int var2 = 0; var2 < var3.length; ++var2) {
         var1 = var1 + Integer.toString((var3[var2] & 255) + 256, 16).substring(1);
      }

      return var1;
   }

   public static String getSha1ChecksumZipped(String var0) throws NoSuchAlgorithmException, IOException {
      byte[] var3 = createChecksumZipped(var0);
      String var1 = "";

      for(int var2 = 0; var2 < var3.length; ++var2) {
         var1 = var1 + Integer.toString((var3[var2] & 255) + 256, 16).substring(1);
      }

      return var1;
   }

   public static String getSha1Checksum(File var0) throws NoSuchAlgorithmException, IOException {
      byte[] var3 = createChecksum(var0);
      String var1 = "";

      for(int var2 = 0; var2 < var3.length; ++var2) {
         var1 = var1 + Integer.toString((var3[var2] & 255) + 256, 16).substring(1);
      }

      return var1;
   }

   public static String fileToString(File var0) throws IOException {
      FileReader var1 = null;
      boolean var5 = false;

      String var8;
      try {
         var5 = true;
         var1 = new FileReader(var0);
         BufferedReader var7 = new BufferedReader(var1);
         StringBuffer var2 = new StringBuffer();

         while(true) {
            String var3;
            if ((var3 = var7.readLine()) == null) {
               var8 = var2.toString();
               var5 = false;
               break;
            }

            var2.append(var3);
            var2.append("\n");
         }
      } finally {
         if (var5) {
            if (var1 != null) {
               var1.close();
            }

         }
      }

      var1.close();
      return var8;
   }

   public static int countFilesRecusrively(String var0) {
      FileExt var3 = new FileExt(var0);
      int var1 = 0;
      File[] var4;
      if ((var4 = var3.listFiles()) != null) {
         for(int var2 = 0; var2 < var4.length; ++var2) {
            if (var4[var2].isDirectory()) {
               var1 += countFilesRecusrively(var4[var2].getAbsolutePath());
            }

            ++var1;
         }
      }

      return var1;
   }

   private static void createFilesHashRecursively(String var0, FileFilter var1, ObjectArrayList var2) throws NoSuchAlgorithmException, IOException {
      FileExt var4;
      File[] var5;
      if ((var4 = new FileExt(var0)).exists() && var4.isDirectory() && (var5 = var4.listFiles()) != null) {
         for(int var3 = 0; var3 < var5.length; ++var3) {
            if (var5[var3].isDirectory()) {
               if (var1.accept(var5[var3])) {
                  createFilesHashRecursively(var5[var3].getAbsolutePath(), var1, var2);
               }
            } else if (var1.accept(var5[var3])) {
               var2.add(var5[var3]);
            }
         }
      }

   }

   public static String createFilesHashRecursively(String var0, FileFilter var1) throws NoSuchAlgorithmException, IOException {
      ObjectArrayList var2 = new ObjectArrayList();
      StringBuffer var3 = new StringBuffer();
      createFilesHashRecursively(var0, var1, var2);
      Collections.sort(var2, new Comparator() {
         public final int compare(File var1, File var2) {
            return var1.getAbsolutePath().compareTo(var2.getAbsolutePath());
         }
      });
      Iterator var4 = var2.iterator();

      while(var4.hasNext()) {
         File var5 = (File)var4.next();
         var3.append(getSha1Checksum(var5));
      }

      return var3.toString();
   }

   static {
      DIR_SEPARATOR = File.separatorChar;
      StringBuilderWriter var0 = new StringBuilderWriter(4);
      PrintWriter var1;
      (var1 = new PrintWriter(var0)).println();
      LINE_SEPARATOR = var0.toString();
      var1.close();
   }
}
