package org.schema.game.common.updater;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import org.dom4j.DocumentException;
import org.schema.game.common.api.exceptions.NotLoggedInException;

public class NewsPanel extends JPanel {
   private static final long serialVersionUID = 1L;
   private JScrollPane currentScrollPane;
   private GridBagConstraints gbc_scrollPane;
   private HtmlDisplayer htmlDisplay;

   public NewsPanel() {
      GridBagLayout var1;
      (var1 = new GridBagLayout()).columnWidths = new int[]{450, 0};
      var1.rowHeights = new int[]{10, 290, 0, 0};
      var1.columnWeights = new double[]{0.0D, Double.MIN_VALUE};
      var1.rowWeights = new double[]{0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      this.setLayout(var1);
      JPanel var3 = new JPanel();
      GridBagConstraints var2;
      (var2 = new GridBagConstraints()).anchor = 14;
      var2.insets = new Insets(0, 0, 5, 0);
      var2.gridx = 0;
      var2.gridy = 2;
      this.add(var3, var2);
      JButton var4;
      (var4 = new JButton("Refresh News")).setHorizontalAlignment(4);
      var3.add(var4);
      this.gbc_scrollPane = new GridBagConstraints();
      this.gbc_scrollPane.gridheight = 2;
      this.gbc_scrollPane.weighty = 1.0D;
      this.gbc_scrollPane.weightx = 1.0D;
      this.gbc_scrollPane.fill = 1;
      this.gbc_scrollPane.gridx = 0;
      this.gbc_scrollPane.gridy = 0;
      this.currentScrollPane = new JScrollPane(this.htmlDisplay = new HtmlDisplayer());
      this.add(this.currentScrollPane, this.gbc_scrollPane);
      this.refreshNews();
      this.add(this.currentScrollPane, this.gbc_scrollPane);
      var4.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            NewsPanel.this.refreshNews();
         }
      });
   }

   public void doUpdate() {
      try {
         ArrayList var1 = NewsRetriever.getNews(Launcher.newsFetchStyle, 1);

         for(int var2 = 0; var2 < var1.size(); ++var2) {
            try {
               this.htmlDisplay.setText((String)var1.get(var2));
            } catch (Exception var4) {
               var4.printStackTrace();
               this.htmlDisplay.setText("Error: " + var4.getClass() + ": " + var4.getMessage());
            }

            this.invalidate();
            this.validate();
            this.repaint();
         }

      } catch (IOException var5) {
         var5.printStackTrace();
      } catch (NotLoggedInException var6) {
         var6.printStackTrace();
      } catch (DocumentException var7) {
         var7.printStackTrace();
      }
   }

   private void refreshNews() {
      (new Thread(new Runnable() {
         public void run() {
            SwingUtilities.invokeLater(new Runnable() {
               public void run() {
                  NewsPanel.this.doUpdate();
               }
            });
         }
      })).start();
   }
}
