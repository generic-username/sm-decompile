package org.schema.game.common.updater;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class FileEntry implements Comparable {
   String name;
   long size;
   private String version;
   private String checksum;

   public FileEntry(String var1, long var2, String var4) {
      this.name = var1;
      this.size = var2;
      this.version = new String(var1);
      this.version = this.version.replace("starmade-build_", "");
      this.version = this.version.replace(".zip", "");
      this.checksum = var4;
   }

   public int compareTo(FileEntry var1) {
      return this.name.compareToIgnoreCase(var1.name);
   }

   public String getVersion() {
      return this.version;
   }

   public String toString() {
      return "FileEntry [name=" + this.name + ", size=" + this.size + "]";
   }

   public void validateCheckSum() throws CheckSumFailedException, IOException {
      String var1 = null;

      try {
         var1 = FileUtil.getSha1Checksum(this.name);
      } catch (NoSuchAlgorithmException var2) {
         var2.printStackTrace();
      }

      System.err.println("Checking checksum for " + this.name);
      System.err.println("star-made.org: " + this.checksum);
      System.err.println("downloaded file: " + var1);
      if (!this.checksum.equals(var1)) {
         throw new CheckSumFailedException("The downloaded mirror file didn't match the checksum give from star-made.org. The file might be modified!");
      }
   }
}
