package org.schema.game.common.updater;

import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import org.schema.common.util.security.OperatingSystem;
import org.schema.game.common.updater.backup.StarMadeBackupTool;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.game.common.version.OldVersionException;
import org.schema.game.common.version.Version;
import org.schema.schine.resource.FileExt;

public class Updater extends Observable {
   public static final int BACK_NONE = 0;
   public static final int BACK_DB = 1;
   public static final int BACK_ALL = 2;
   public static String FILES_URL = "http://files.star-made.org/";
   public static String UPDATE_SITE_OLD = "http://files.star-made.org/build/";
   public static String LAUNCHER_VERSION_SITE = "http://files.star-made.org/version";
   public static String MIRROR_SITE = "http://files.star-made.org/mirrors";
   public static String selectedMirror;
   public final ArrayList versions = new ArrayList();
   private final ArrayList mirrorURLs = new ArrayList();
   boolean loading = false;
   boolean versionsLoaded = false;
   private StarMadeBackupTool backup = new StarMadeBackupTool();
   private boolean updating;

   public Updater(String var1) {
      this.reloadVersion(var1);
   }

   public static void withoutGUI(boolean var0, String var1, Updater.VersionFile var2, int var3, boolean var4) {
      Updater var5 = new Updater(var1);

      try {
         var5.startLoadVersionList(var2);

         while(var5.loading) {
            Thread.sleep(100L);
         }

         if (var4) {
            selectVersion(true, var5, var0, var1, var2, var3, var4);
         } else if (var5.isNewerVersionAvailable()) {
            System.err.println("A New Version Is Available!");
            var5.startUpdateNew(var1, (IndexFileEntry)var5.versions.get(var5.versions.size() - 1), false, var3);
         } else {
            System.err.println("You Are Already on the Newest Version: use -force to force an update");
         }
      } catch (InterruptedException var6) {
         var6.printStackTrace();
      }
   }

   public static void selectVersion(boolean var0, Updater var1, boolean var2, String var3, Updater.VersionFile var4, int var5, boolean var6) {
      while(true) {
         int var9;
         if (var0) {
            for(var9 = 0; var9 < var1.versions.size(); ++var9) {
               System.out.println("[" + var9 + "] v" + ((IndexFileEntry)var1.versions.get(var9)).version + "; " + ((IndexFileEntry)var1.versions.get(var9)).build);
            }
         }

         try {
            System.out.println("Select the build you want to install (type in number in brackets and press Enter)");
            var9 = Integer.parseInt((new BufferedReader(new InputStreamReader(System.in))).readLine());
         } catch (NumberFormatException var7) {
            System.out.println("Error: Input must be number");
            var0 = false;
            continue;
         } catch (IOException var8) {
            var8.printStackTrace();
            var0 = false;
            continue;
         }

         if (var9 >= 0 && var9 < var1.versions.size() - 1) {
            var1.startUpdateNew(var3, (IndexFileEntry)var1.versions.get(var9), false, var5);
            return;
         }

         System.out.println("Error: Version does not exist");
         var0 = false;
      }
   }

   public static String en(String var0) throws UnsupportedEncodingException {
      return URLEncoder.encode(var0, "ISO-8859-1");
   }

   public static int getRemoteLauncherVersion() throws IOException {
      URLConnection var0;
      (var0 = (new URL(LAUNCHER_VERSION_SITE)).openConnection()).setRequestProperty("User-Agent", "StarMade-Updater_" + Launcher.version);
      var0.setConnectTimeout(10000);
      var0.setReadTimeout(10000);
      BufferedReader var1;
      int var2 = Integer.parseInt((var1 = new BufferedReader(new InputStreamReader(new BufferedInputStream(var0.getInputStream())))).readLine());
      var1.close();
      return var2;
   }

   public static int askBackup(JFrame var0) {
      String[] var1 = new String[]{"Yes (Only Database)", "Yes (Everything)", "No"};
      switch(JOptionPane.showOptionDialog(var0, "Create Backup of current game data? (recommended)", "Backup?", 1, 3, (Icon)null, var1, var1[0])) {
      case 0:
         return 1;
      case 1:
         return 2;
      case 2:
         return 0;
      default:
         return 0;
      }
   }

   private static String getJavaExec() {
      return !System.getProperty("os.name").equals("Mac OS X") && !System.getProperty("os.name").contains("Linux") ? "javaw" : "java";
   }

   public static void selfUpdate(final String[] var0) throws IOException {
      if (Launcher.class.getResource("/smselfupdate.jar") != null) {
         System.err.println("Extracting self updating jar");
         InputStream var1 = Updater.class.getResourceAsStream("/smselfupdate.jar");
         final FileExt var2 = new FileExt("smselfupdate.jar");
         FileUtil.copyInputStreamToFile(var1, var2, new DownloadCallback() {
            public final void downloaded(long var1, long var3) {
            }
         });
         var1.close();
         System.err.println("Extracting self updating jar DONE: " + var2.getAbsolutePath());
         SwingUtilities.invokeLater(new Runnable() {
            public final void run() {
               try {
                  String var1 = "";

                  for(int var2x = 0; var2x < var0.length; ++var2x) {
                     var1 = var1 + " " + var0[var2x];
                  }

                  String[] var6 = new String[]{Updater.getJavaExec(), "-Djava.net.preferIPv4Stack=true", "-jar", var2.getAbsolutePath(), var1};
                  System.err.println("RUNNING COMMAND: " + var6);
                  ProcessBuilder var5;
                  (var5 = new ProcessBuilder(var6)).environment();
                  FileExt var7 = new FileExt("./");
                  var5.directory(var7.getAbsoluteFile());
                  var5.start();
                  System.err.println("Exiting because launcher starting selfupdater");

                  try {
                     throw new Exception("System.exit() called");
                  } catch (Exception var3) {
                     var3.printStackTrace();
                     System.exit(0);
                  }
               } catch (IOException var4) {
                  var4.printStackTrace();
                  GuiErrorHandler.processErrorDialogException(var4);
               }
            }
         });
      } else {
         throw new FileNotFoundException("Couldnt find selfupdate in jar");
      }
   }

   public synchronized void addObserver(Observer var1) {
      super.addObserver(var1);
      this.backup.addObserver(var1);
   }

   public boolean checkMirrow(FileEntry var1) {
      String var3 = selectedMirror + var1.name;

      try {
         URLConnection var4;
         (var4 = (new URL(var3)).openConnection()).setReadTimeout(15000);
         var4.setRequestProperty("User-Agent", "StarMade-Updater_" + Launcher.version);
         var4.connect();
         var4.getInputStream();
         return true;
      } catch (IOException var2) {
         var2.printStackTrace();
         System.err.println("Mirror not available " + selectedMirror);
         return false;
      }
   }

   public void extract(FileEntry var1, String var2) throws IOException {
      if (!var2.endsWith("/")) {
         var2 = var2 + "/";
      }

      System.err.println("Extracting " + var1);
      ZipFile var3;
      Enumeration var4 = (var3 = new ZipFile(var1.name)).entries();
      FileExt var10000 = new FileExt(var2);
      ZipEntry var5 = null;
      var10000.mkdirs();

      while(var4.hasMoreElements()) {
         if ((var5 = (ZipEntry)var4.nextElement()).isDirectory()) {
            System.err.println("Extracting directory: " + var5.getName());
            (new FileExt(var2 + var5.getName())).mkdir();
         } else {
            this.setChanged();
            this.notifyObservers("Extracting " + var5.getName());
            System.err.println("Extracting file: " + var5.getName());
            FileUtil.copyInputStream(var3.getInputStream(var5), new BufferedOutputStream(new FileOutputStream(var2 + var5.getName())));
         }
      }

      var3.close();
      System.err.println("Deleting archive " + var1);
      (new FileExt(var1.name)).delete();
   }

   public String getStarMadeStartPath(String var1) {
      return var1 + File.separator + "StarMade.jar";
   }

   public boolean isNewerVersionAvailable() {
      if (!this.versionsLoaded) {
         return false;
      } else if (this.versions.isEmpty()) {
         System.err.println("versions empty");
         return false;
      } else if (Version.build != null && !Version.build.equals("undefined")) {
         if (Version.build.equals("latest")) {
            System.err.println("newer version always available for develop version!");
            return true;
         } else {
            System.out.println("checking your version " + Version.build + " against latest " + ((IndexFileEntry)this.versions.get(this.versions.size() - 1)).build + " = " + Version.build.compareTo(((IndexFileEntry)this.versions.get(this.versions.size() - 1)).build));
            return this.versions.size() > 0 && Version.build.compareTo(((IndexFileEntry)this.versions.get(this.versions.size() - 1)).build) < 0;
         }
      } else {
         System.err.println("Version build null or undefined");
         return true;
      }
   }

   private void loadVersionList(Updater.VersionFile var1) throws IOException {
      this.setChanged();
      this.notifyObservers("Retrieving Launcher Version");
      this.loading = true;

      try {
         this.versions.clear();
         if (getRemoteLauncherVersion() > Launcher.version) {
            throw new OldVersionException("You have an old Launcher Version.\nPlease download the latest Launcher Version at http://www.star-made.org/\n('retry' will let you ignore this message [not recommended!])");
         }
      } catch (MalformedURLException var43) {
         var43.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var43);
      } catch (IOException var44) {
         var44.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var44);
      } catch (OldVersionException var45) {
         var45.printStackTrace();
         if (!GraphicsEnvironment.isHeadless()) {
            Launcher.askForLauncherUpdate();
         }
      } finally {
         this.loading = false;
      }

      this.setChanged();
      this.notifyObservers("Retrieving Mirrors");
      this.loading = true;

      URLConnection var2;
      BufferedReader var3;
      String var4;
      try {
         this.versions.clear();
         (var2 = (new URL(MIRROR_SITE)).openConnection()).setConnectTimeout(10000);
         var2.setRequestProperty("User-Agent", "StarMade-Updater_" + Launcher.version);
         var3 = new BufferedReader(new InputStreamReader(new BufferedInputStream(var2.getInputStream())));

         while((var4 = var3.readLine()) != null) {
            this.mirrorURLs.add(var4);
         }

         var3.close();
      } catch (MalformedURLException var40) {
         var40.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var40);
      } catch (IOException var41) {
         var41.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var41);
      } finally {
         this.loading = false;
      }

      this.setChanged();
      this.notifyObservers("Retrieving Versions");
      this.loading = true;

      try {
         this.versions.clear();
         (var2 = (new URL(var1.location)).openConnection()).setConnectTimeout(10000);
         var2.setReadTimeout(10000);
         var2.setRequestProperty("User-Agent", "StarMade-Updater_" + Launcher.version);
         var3 = new BufferedReader(new InputStreamReader(new BufferedInputStream(var2.getInputStream())));

         while((var4 = var3.readLine()) != null) {
            String[] var5;
            String[] var47;
            String var6 = (var5 = (var47 = var4.split(" ", 2))[0].split("#", 2))[0];
            String var48 = var5[1];
            var4 = var47[1];
            this.versions.add(new IndexFileEntry(var4, var6, var48, var1));
         }

         Collections.sort(this.versions);
         System.err.println("loaded files (sorted) " + this.versions);
         var3.close();
         this.versionsLoaded = true;
         this.setChanged();
         this.notifyObservers("versions loaded");
         var2.getInputStream().close();
         return;
      } catch (MalformedURLException var37) {
         var37.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var37);
         return;
      } catch (IOException var38) {
         var38.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var38);
      } finally {
         this.loading = false;
      }

   }

   public boolean lookForGame(String var1) {
      return (new FileExt(this.getStarMadeStartPath(var1))).exists();
   }

   public void reloadVersion(String var1) {
      try {
         Version.loadVersion(var1);
      } catch (Exception var2) {
         var2.printStackTrace();
      }
   }

   public void saveUrl(final FileEntry var1) throws MalformedURLException, IOException {
      if (selectedMirror == null) {
         Random var2 = new Random();
         selectedMirror = (String)this.mirrorURLs.get(var2.nextInt(this.mirrorURLs.size()));
      }

      if (!selectedMirror.endsWith("/")) {
         selectedMirror = selectedMirror + "/";
      }

      this.setChanged();
      this.notifyObservers("connecting...");
      String var5 = selectedMirror + var1.name;
      FileExt var3;
      (var3 = new FileExt(var1.name + ".filepart")).delete();
      final FileDownloadUpdate var4 = new FileDownloadUpdate();
      FileUtil.copyURLToFile(new URL(var5), var3, 50000, 50000, new DownloadCallback() {
         public void downloaded(long var1x, long var3) {
            var4.downloaded = var1x;
            var4.size = var1.size;
            var4.fileName = var4.fileName;
            Updater.this.setChanged();
            Updater.this.notifyObservers(var4);
         }
      });
      var3.renameTo(new FileExt(var1.name));
   }

   public void startLoadVersionList(final Updater.VersionFile var1) {
      this.loading = true;
      (new Thread(new Runnable() {
         public void run() {
            try {
               Updater.this.loadVersionList(var1);
            } catch (IOException var1x) {
               var1x.printStackTrace();
            }
         }
      })).start();
   }

   public void startUpdateNew(String var1, IndexFileEntry var2, boolean var3, int var4) {
      if (!this.updating) {
         FileExt var14;
         try {
            Eula var5;
            if ((var5 = this.getEula()) != null && UpdatePanel.frame != null) {
               JTextArea var6 = new JTextArea(var5.text);
               JScrollPane var7 = new JScrollPane(var6);
               var6.setLineWrap(true);
               var6.setWrapStyleWord(true);
               var7.setPreferredSize(new Dimension(500, 500));
               Object[] var13 = new Object[]{"I have read the EULA and accept", "I don't accept"};
               if (JOptionPane.showOptionDialog(UpdatePanel.frame, var7, "StarMade EULA", 0, 3, (Icon)null, var13, var13[0]) != 0) {
                  return;
               }

               var14 = new FileExt(OperatingSystem.getAppDir(), "eula.properties");
               Properties var15;
               (var15 = new Properties()).put("EULA", var5.title);
               var14.createNewFile();
               FileOutputStream var11 = new FileOutputStream(var14);
               var15.store(var11, "StarMade Eula Acceptance");
               var11.flush();
               var11.close();
            }
         } catch (IOException var8) {
            var8.printStackTrace();
            GuiErrorHandler.processErrorDialogException(var8);
         }

         FileExt var12;
         if (!(var12 = new FileExt(var1)).exists()) {
            var12.mkdirs();
         }

         if (!var12.isDirectory()) {
            try {
               throw new IOException("Installation dir is not a directory");
            } catch (IOException var10) {
               GuiErrorHandler.processErrorDialogException(var10);
            }
         }

         if (!var12.canWrite()) {
            try {
               throw new IOException("Your operating System denies access \nto where you are trying to install StarMade (for good reasons)\n" + (new FileExt(var1)).getAbsolutePath() + "\n\nTo solve this Problem,\nPlease change the install destination to another directory,\nOr Force the install by executing this file as administrator");
            } catch (IOException var9) {
               GuiErrorHandler.processErrorDialogException(var9);
            }
         }

         this.setChanged();
         this.notifyObservers("updating");
         var14 = new FileExt(var1);
         int var16 = var4;
         if (UpdatePanel.frame != null && var14.exists() && var14.isDirectory() && var14.list().length > 0) {
            var16 = askBackup(UpdatePanel.frame);
         }

         this.downloadDiff(var14, var1, var2, var16, var3);
      }
   }

   private void downloadDiff(final File var1, final String var2, final IndexFileEntry var3, final int var4, final boolean var5) {
      this.updating = true;
      (new Thread(new Runnable() {
         public void run() {
            try {
               if (var4 != 0) {
                  Updater.this.setChanged();
                  Updater.this.notifyObservers("Creating backup!");
                  boolean var2x = (var4 & 1) == 1;
                  System.err.println("BACKING UP: " + var2);
                  Updater.this.backup.backUp(var2, "server-database", String.valueOf(System.currentTimeMillis()), ".zip", false, var2x, (FileFilter)null);
               }

               Updater.this.setChanged();
               Updater.this.notifyObservers("Retrieving checksums for v" + var3.version + "(build " + var3.build + ")");
               ChecksumFile var1x = Updater.this.getChecksums(var3.path);
               System.err.println("Downloaded checksums: \n" + var1x.toString());
               String var11 = Updater.FILES_URL + var3.path + "/";
               var1x.download(var5, var11, var1, var2, new FileDowloadCallback() {
                  public void update(FileDownloadUpdate var1x) {
                     Updater.this.setChanged();
                     Updater.this.notifyObservers(var1x);
                  }

                  public void update(String var1x) {
                     Updater.this.setChanged();
                     Updater.this.notifyObservers(var1x);
                  }
               });
               Updater.this.setChanged();
               Updater.this.notifyObservers("Update Successfull!");

               try {
                  Thread.sleep(500L);
               } catch (InterruptedException var7) {
                  var7.printStackTrace();
               }

               Updater.this.setChanged();
               Updater.this.notifyObservers("reset");
               return;
            } catch (IOException var8) {
               var8.printStackTrace();
               Updater.this.setChanged();
               Updater.this.notifyObservers("failed IO");
               GuiErrorHandler.processErrorDialogException(var8);
            } catch (NoSuchAlgorithmException var9) {
               var9.printStackTrace();
               Updater.this.setChanged();
               Updater.this.notifyObservers("failed Sha");
               GuiErrorHandler.processErrorDialogException(var9);
               return;
            } finally {
               Updater.this.updating = false;
               Updater.this.setChanged();
               Updater.this.notifyObservers("finished");
            }

         }
      })).start();
   }

   public Eula getEula() throws IOException {
      System.err.println("URL::: " + FILES_URL + "/smeula.txt");
      URLConnection var1;
      (var1 = (new URL(FILES_URL + "/smeula.txt")).openConnection()).setRequestProperty("User-Agent", "StarMade-Updater_" + Launcher.version);
      var1.setConnectTimeout(10000);
      var1.setReadTimeout(10000);
      Eula var2 = new Eula();
      BufferedReader var7 = new BufferedReader(new InputStreamReader(new BufferedInputStream(var1.getInputStream())));

      StringBuffer var3;
      String var4;
      for(var3 = new StringBuffer(); (var4 = var7.readLine()) != null; var3.append(var4 + "\n")) {
         if (var2.title == null) {
            var2.title = var4;
            FileExt var5 = new FileExt(OperatingSystem.getAppDir(), "eula.properties");
            Properties var6 = new Properties();
            if (var5.exists()) {
               FileInputStream var8 = new FileInputStream(var5);
               var6.load(var8);
               var8.close();
               if (var6.getProperty("EULA") != null && var6.getProperty("EULA").equals(var2.title)) {
                  return null;
               }
            }
         }
      }

      var7.close();
      var2.text = var3.toString();
      return var2;
   }

   public ChecksumFile getChecksums(String var1) throws IOException {
      URLConnection var3;
      (var3 = (new URL(FILES_URL + var1 + "/checksums")).openConnection()).setRequestProperty("User-Agent", "StarMade-Updater_" + Launcher.version);
      var3.setConnectTimeout(10000);
      var3.setReadTimeout(10000);
      BufferedReader var4 = new BufferedReader(new InputStreamReader(new BufferedInputStream(var3.getInputStream())));
      ChecksumFile var2;
      (var2 = new ChecksumFile()).parse(var4);
      var4.close();
      return var2;
   }

   public static enum VersionFile {
      PRE("http://files.star-made.org/prebuildindex"),
      DEV("http://files.star-made.org/devbuildindex"),
      RELEASE("http://files.star-made.org/releasebuildindex"),
      ARCHIVE("http://files.star-made.org/archivebuildindex");

      public final String location;

      private VersionFile(String var3) {
         this.location = var3;
      }
   }
}
