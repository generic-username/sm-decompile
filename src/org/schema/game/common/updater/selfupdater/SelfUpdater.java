package org.schema.game.common.updater.selfupdater;

import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Observable;
import javax.swing.SwingUtilities;
import org.schema.common.util.StringTools;
import org.schema.game.common.updater.DownloadCallback;
import org.schema.game.common.updater.FileUtil;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.schine.resource.FileExt;

public class SelfUpdater extends Observable {
   public String LAUNCHER_PATH = "http://files.star-made.org/StarMade-Starter";
   boolean running;
   private SelfUpdaterFrame updaterFrame;

   public static void main(String[] var0) {
      (new SelfUpdater()).runT();
   }

   private static String getJavaExec() {
      return !System.getProperty("os.name").equals("Mac OS X") && !System.getProperty("os.name").contains("Linux") ? "javaw" : "java";
   }

   public void runT() {
      this.running = true;
      SwingUtilities.invokeLater(new Runnable() {
         public void run() {
            if (!GraphicsEnvironment.isHeadless()) {
               SelfUpdater.this.updaterFrame = new SelfUpdaterFrame();
               SelfUpdater.this.updaterFrame.setAlwaysOnTop(true);
               SelfUpdater.this.updaterFrame.setVisible(true);
            }

            SelfUpdater.this.running = false;
         }
      });

      while(this.running) {
         try {
            Thread.sleep(100L);
         } catch (InterruptedException var2) {
            var2.printStackTrace();
         }
      }

      try {
         this.downloadNewVersion();
      } catch (Exception var3) {
         var3.printStackTrace();
         if (!GraphicsEnvironment.isHeadless()) {
            GuiErrorHandler.processErrorDialogException(var3);
         }
      }

      this.restartLauncher(new String[0]);
   }

   public void restartLauncher(String[] var1) {
      try {
         String var2 = "";

         for(int var3 = 0; var3 < var1.length; ++var3) {
            var2 = var2 + " " + var1[var3];
         }

         FileExt var6;
         String[] var9;
         if (!System.getProperty("os.name").equals("Mac OS X") && !System.getProperty("os.name").contains("Linux")) {
            var6 = new FileExt("StarMade-Starter.exe");
            var9 = new String[]{var6.getAbsolutePath(), var2};
         } else {
            var6 = new FileExt("StarMade-Starter.jar");
            var9 = new String[]{getJavaExec(), "-Djava.net.preferIPv4Stack=true", "-jar", var6.getAbsolutePath(), var2};
         }

         System.err.println("RUNNING COMMAND: " + var9);
         ProcessBuilder var7;
         (var7 = new ProcessBuilder(var9)).environment();
         FileExt var8 = new FileExt("./");
         var7.directory(var8.getAbsoluteFile());
         var7.start();
         System.err.println("Exiting because selfupdater starting launcher");

         try {
            throw new Exception("System.exit() called");
         } catch (Exception var4) {
            var4.printStackTrace();
            System.exit(0);
         }
      } catch (IOException var5) {
         var5.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var5);
      }
   }

   public void downloadNewVersion() throws UnsupportedEncodingException, MalformedURLException, IOException, URISyntaxException {
      String var1 = this.LAUNCHER_PATH;
      String var2 = "StarMade-Starter";
      if (!System.getProperty("os.name").equals("Mac OS X") && !System.getProperty("os.name").contains("Linux")) {
         var1 = var1 + ".exe";
         var2 = var2 + ".exe";
      } else {
         var1 = var1 + ".jar";
         var2 = var2 + ".jar";
      }

      final long var4 = (long)FileUtil.convertToURLEscapingIllegalCharacters(var1).openConnection().getContentLength();
      System.err.println("File size to download: " + var4);
      FileExt var3 = new FileExt(var2 + ".tmp");
      FileUtil.copyURLToFile(FileUtil.convertToURLEscapingIllegalCharacters(var1), var3, 50000, 50000, new DownloadCallback() {
         public void downloaded(long var1, long var3) {
            if (SelfUpdater.this.updaterFrame != null) {
               SelfUpdater.this.updaterFrame.getProgressBar().setValue((int)((float)var1 / (float)var4 * 100.0F));
               SelfUpdater.this.updaterFrame.getProgressBar().setString("Downloading... " + StringTools.formatPointZero((double)var1 / 1000000.0D) + "MB/" + StringTools.formatPointZero((double)var4 / 1000000.0D) + "MB    " + SelfUpdater.this.updaterFrame.getProgressBar().getValue() + " %");
            }

         }
      }, "dev", "dev", true);
      FileExt var8;
      if ((var8 = new FileExt(var2)).exists()) {
         var8.delete();
      }

      var3.renameTo(new FileExt(var2));
   }
}
