package org.schema.game.common.updater.selfupdater;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

public class SelfUpdaterFrame extends JFrame {
   private static final long serialVersionUID = 1L;
   private JPanel contentPane;
   private JProgressBar progressBar;

   public SelfUpdaterFrame() {
      this.setTitle("StarMade Launcher Self Updater");
      this.setDefaultCloseOperation(3);
      this.setBounds(100, 100, 450, 138);
      this.contentPane = new JPanel();
      this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.contentPane.setLayout(new BorderLayout(0, 0));
      this.setContentPane(this.contentPane);
      JPanel var1 = new JPanel();
      this.contentPane.add(var1, "Center");
      GridBagLayout var2;
      (var2 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var2.rowHeights = new int[]{0, 0, 0, 0};
      var2.columnWeights = new double[]{0.0D, Double.MIN_VALUE};
      var2.rowWeights = new double[]{0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var1.setLayout(var2);
      JLabel var4 = new JLabel("This tool downloads the latest Launcher Version. ");
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.gridx = 0;
      var3.gridy = 0;
      var1.add(var4, var3);
      var4 = new JLabel("The new launcher version will start when update finishes.");
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.gridx = 0;
      var3.gridy = 1;
      var1.add(var4, var3);
      this.progressBar = new JProgressBar();
      this.progressBar.setStringPainted(true);
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).weighty = 1.0D;
      var5.weightx = 1.0D;
      var5.fill = 1;
      var5.gridx = 0;
      var5.gridy = 2;
      var1.add(this.progressBar, var5);
   }

   public static void main(String[] var0) {
      EventQueue.invokeLater(new Runnable() {
         public final void run() {
            try {
               (new SelfUpdaterFrame()).setVisible(true);
            } catch (Exception var1) {
               var1.printStackTrace();
            }
         }
      });
   }

   public JProgressBar getProgressBar() {
      return this.progressBar;
   }

   public void setProgressBar(JProgressBar var1) {
      this.progressBar = var1;
   }
}
