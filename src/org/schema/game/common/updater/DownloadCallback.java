package org.schema.game.common.updater;

public interface DownloadCallback {
   void downloaded(long var1, long var3);
}
