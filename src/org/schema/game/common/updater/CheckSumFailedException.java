package org.schema.game.common.updater;

public class CheckSumFailedException extends Exception {
   private static final long serialVersionUID = 1L;

   public CheckSumFailedException(String var1) {
      super(var1);
   }
}
