package org.schema.game.common.updater;

import javax.swing.JEditorPane;
import javax.swing.text.DefaultCaret;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

public class HtmlDisplayer extends JEditorPane {
   private static final long serialVersionUID = 1L;
   private HTMLEditorKit kit;

   public HtmlDisplayer() {
      this.setEditable(false);
      this.kit = new HTMLEditorKit();
      this.setEditorKit(this.kit);
      StyleSheet var1;
      (var1 = this.kit.getStyleSheet()).addRule("body {color:#bfbfbf; font-family:Verdana,Arial,sans-serif; margin: 4px; background-color : #292929; }");
      var1.addRule("h1 {color: #fffeff;}");
      var1.addRule("h2 {color: #fffeff;}");
      var1.addRule("pre {font : 10px monaco; color : #3b3b3b; background-color : black; }");
      Document var2 = this.kit.createDefaultDocument();
      this.setDocument(var2);
      ((DefaultCaret)this.getCaret()).setUpdatePolicy(1);
   }
}
