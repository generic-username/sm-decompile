package org.schema.game.common.gui;

import it.unimi.dsi.fastutil.objects.ObjectCollection;
import java.util.Iterator;
import javax.swing.table.AbstractTableModel;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.server.ServerController;
import org.schema.schine.network.server.ServerProcessor;

public class ClientTableModel extends AbstractTableModel {
   private static final long serialVersionUID = 1L;
   private ServerController serverController;

   public String getColumnName(int var1) {
      if (var1 == 0) {
         return "#";
      } else if (var1 == 1) {
         return "ID";
      } else if (var1 == 2) {
         return "Name";
      } else if (var1 == 3) {
         return "NetStatus";
      } else {
         return var1 == 4 ? "Ping" : "unknown";
      }
   }

   public Class getColumnClass(int var1) {
      return this.getValueAt(0, var1).getClass();
   }

   public int getRowCount() {
      try {
         return this.serverController.getServerState().getClients().size();
      } catch (Exception var1) {
         return 0;
      }
   }

   public int getColumnCount() {
      return 5;
   }

   public Object getValueAt(int var1, int var2) {
      try {
         ObjectCollection var3 = this.serverController.getServerState().getClients().values();
         RegisteredClientOnServer var4 = null;
         int var5 = 0;

         for(Iterator var8 = var3.iterator(); var8.hasNext(); ++var5) {
            RegisteredClientOnServer var6 = (RegisteredClientOnServer)var8.next();
            if (var5 >= var1) {
               var4 = var6;
               break;
            }
         }

         ServerProcessor var9 = var4.getProcessor();
         if (var2 == 0) {
            return String.valueOf(var1);
         }

         if (var9.getClient() == null) {
            return "UNKNOWN -4242";
         }

         if (var2 == 1) {
            return var4.getId();
         }

         if (var2 == 2) {
            return var4.getPlayerName();
         }

         if (var2 == 3) {
            if (var9.isAlive()) {
               return "alive";
            }

            return "dead";
         }

         if (var2 == 4) {
            return var9.getPingTime();
         }
      } catch (Exception var7) {
      }

      return "-";
   }

   public void initData(ServerController var1) {
      this.serverController = var1;
   }

   public void update(ServerController var1) {
      this.initData(var1);
      this.fireTableDataChanged();
   }
}
