package org.schema.game.common.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ButtonGroup;
import javax.swing.ComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataListener;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.ClientStatics;
import org.schema.game.common.Starter;
import org.schema.game.common.controller.database.DatabaseIndex;
import org.schema.game.common.crashreporter.CrashReportGUI;
import org.schema.game.common.facedit.ElementEditorFrame;
import org.schema.game.common.gui.serverlist.ServerListPanel;
import org.schema.game.common.gui.worldmanager.WorldManager;
import org.schema.game.common.starcalc.StarCalc;
import org.schema.game.common.staremote.Staremote;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.game.common.version.Version;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.editor.LanguageEditor;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.network.client.HostPortLoginName;
import org.schema.schine.resource.FileExt;

public class ClientDialog extends JFrame {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private final ButtonGroup buttonGroup = new ButtonGroup();
   private JTextField loginNameTextField;
   private JComboBox hostPortTextField;
   private boolean startLocalServer;
   private ClientDialog.CallBack callBack;
   private ArrayList history;
   private JRadioButton rdbtnSinglePlayer;
   private JRadioButton rdbtnMultiPlayer;
   private JComboBox gameModeCombobox;
   private JScrollPane scrollPane;
   private JButton okButton;
   private JPanel mainPanel;

   public ClientDialog(Observer var1, ArrayList var2) {
      super("StarMade alpha " + Version.VERSION + " (" + Version.build + ")");
      if (var2.isEmpty()) {
         var2.add(new HostPortLoginName("localhost", 4242, (byte)0, ""));
      }

      this.addWindowListener(new WindowAdapter() {
         public void windowClosing(WindowEvent var1) {
            System.err.println("Exiting because of window closed");
            ClientDialog.this.dispose();

            try {
               throw new Exception("System.exit() called");
            } catch (Exception var2) {
               var2.printStackTrace();
               System.exit(0);
            }
         }

         public void windowClosed(WindowEvent var1) {
            System.err.println("Exiting because of window closed");
            ClientDialog.this.dispose();

            try {
               throw new Exception("System.exit() called");
            } catch (Exception var2) {
               var2.printStackTrace();
               System.exit(0);
            }
         }
      });
      this.history = var2;
      this.setDefaultCloseOperation(3);
      ImageIcon var4 = new ImageIcon("./data/image-resource/icon32.png");
      this.setIconImage(var4.getImage());
      this.setAlwaysOnTop(false);
      this.setUndecorated(false);
      this.callBack = new ClientDialog.CallBack();
      this.callBack.addObserver(var1);
      this.setTitle("StarMade Connection Setup");
      GraphicsDevice var8;
      (var8 = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice()).getDisplayMode().getWidth();
      var8.getDisplayMode().getHeight();
      this.setBounds(50, 50, 500, 815);
      this.loginNameTextField = new JTextField();
      this.loginNameTextField.setPreferredSize(new Dimension(80, 20));
      this.loginNameTextField.setHorizontalAlignment(0);
      this.loginNameTextField.setToolTipText(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_0);
      this.getContentPane().setLayout(new BorderLayout());
      if (var2 != null && !var2.isEmpty()) {
         this.loginNameTextField.setText(((HostPortLoginName)var2.get(var2.size() - 1)).loginName);
      }

      this.mainPanel = new JPanel();
      this.getContentPane().add(this.mainPanel, "Center");
      this.mainPanel.setLayout(new BorderLayout(0, 0));
      this.mainPanel.add(this.contentPanel, "Center");
      GridBagLayout var9;
      (var9 = new GridBagLayout()).columnWidths = new int[]{268, 0};
      var9.rowHeights = new int[]{0, 0};
      var9.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var9.rowWeights = new double[]{0.0D, 0.0D};
      this.contentPanel.setLayout(var9);
      JPanel var10 = new JPanel();
      GridBagConstraints var14;
      (var14 = new GridBagConstraints()).insets = new Insets(5, 0, 5, 0);
      var14.anchor = 17;
      var14.gridx = 0;
      var14.gridy = 0;
      this.contentPanel.add(var10, var14);
      GridBagLayout var15;
      (var15 = new GridBagLayout()).columnWidths = new int[]{280, 46, 0};
      var15.rowHeights = new int[]{40, 0};
      var15.columnWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var15.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var10.setLayout(var15);
      JLabel var17;
      (var17 = new JLabel("StarMade alpha V" + Version.VERSION)).setFont(new Font("Ubuntu-Title", 0, 30));
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).anchor = 18;
      var5.insets = new Insets(0, 0, 0, 5);
      var5.gridx = 0;
      var5.gridy = 0;
      var10.add(var17, var5);
      (var10 = new JPanel()).setBorder(new TitledBorder((Border)null, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_1, 4, 2, (Font)null, (Color)null));
      (var14 = new GridBagConstraints()).anchor = 18;
      var14.insets = new Insets(5, 0, 5, 0);
      var14.gridx = 0;
      var14.gridy = 1;
      this.contentPanel.add(var10, var14);
      (var15 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var15.rowHeights = new int[]{20, 0, 0};
      var15.columnWeights = new double[]{0.0D, 0.0D};
      var15.rowWeights = new double[]{0.0D, 0.0D, 0.0D};
      var10.setLayout(var15);
      (var14 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var14.fill = 2;
      var14.gridx = 0;
      var14.gridy = 0;
      var10.add(this.loginNameTextField, var14);
      this.loginNameTextField.setColumns(30);
      (var17 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_2)).setFont(new Font("Tahoma", 0, 10));
      (var5 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var5.anchor = 17;
      var5.gridx = 0;
      var5.gridy = 1;
      var10.add(var17, var5);
      JButton var19;
      (var19 = new JButton("Uplink To Star-Made.org")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            LoginDialog var2;
            (var2 = new LoginDialog(ClientDialog.this)).setDefaultCloseOperation(2);
            var2.setVisible(true);
         }
      });
      (var5 = new GridBagConstraints()).anchor = 17;
      var5.gridx = 0;
      var5.gridy = 2;
      var10.add(var19, var5);
      (var10 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_3, 4, 2, (Font)null, new Color(0, 0, 0)));
      (var14 = new GridBagConstraints()).fill = 1;
      var14.gridx = 0;
      var14.gridy = 2;
      this.contentPanel.add(var10, var14);
      (var15 = new GridBagLayout()).columnWidths = new int[]{0};
      var15.rowHeights = new int[]{0, 0};
      var15.columnWeights = new double[]{0.0D};
      var15.rowWeights = new double[]{0.0D, 0.0D};
      var10.setLayout(var15);
      JPanel var24;
      (var24 = new JPanel()).setBorder(new TitledBorder((Border)null, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_4, 4, 2, (Font)null, (Color)null));
      (var5 = new GridBagConstraints()).weightx = 1.0D;
      var5.anchor = 18;
      var5.insets = new Insets(0, 0, 5, 5);
      var5.fill = 2;
      var5.gridx = 0;
      var5.gridy = 0;
      var10.add(var24, var5);
      GridBagLayout var18;
      (var18 = new GridBagLayout()).columnWidths = new int[]{21, 0, 80, 0, 0};
      var18.rowHeights = new int[]{21, 0};
      var18.columnWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var18.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var24.setLayout(var18);
      this.rdbtnSinglePlayer = new JRadioButton("");
      (var5 = new GridBagConstraints()).anchor = 17;
      var5.insets = new Insets(0, 0, 0, 5);
      var5.gridx = 0;
      var5.gridy = 0;
      var24.add(this.rdbtnSinglePlayer, var5);
      this.buttonGroup.add(this.rdbtnSinglePlayer);
      this.rdbtnSinglePlayer.setSelected(EngineSettings.S_INITIAL_SETTING.getCurrentState().equals("Single Player"));
      this.rdbtnSinglePlayer.addChangeListener(new ChangeListener() {
         public void stateChanged(ChangeEvent var1) {
            ClientDialog.this.hostPortTextField.setEnabled(!ClientDialog.this.rdbtnSinglePlayer.isSelected());
            ClientDialog.this.gameModeCombobox.setEnabled(ClientDialog.this.rdbtnSinglePlayer.isSelected());
            if (ClientDialog.this.rdbtnSinglePlayer.isSelected()) {
               EngineSettings.S_INITIAL_SETTING.setCurrentState("Single Player");
            } else {
               EngineSettings.S_INITIAL_SETTING.setCurrentState("Multi Player");
            }
         }
      });
      JLabel var20 = new JLabel("Game Mode");
      GridBagConstraints var6;
      (var6 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var6.anchor = 13;
      var6.gridx = 1;
      var6.gridy = 0;
      var24.add(var20, var6);
      this.gameModeCombobox = new JComboBox(new EngineSettingComboBoxModel(EngineSettings.S_GAME_MODE));
      (var5 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var5.gridx = 2;
      var5.gridy = 0;
      var24.add(this.gameModeCombobox, var5);
      JButton var25;
      (var25 = new JButton(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_5)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            (new WorldManager((JDialog)null)).setVisible(true);
         }
      });
      (var6 = new GridBagConstraints()).weightx = 1.0D;
      var6.anchor = 13;
      var6.gridx = 3;
      var6.gridy = 0;
      var24.add(var25, var6);
      this.hostPortTextField = new JComboBox(new ClientDialog.ComboBoxListModel());
      JPanel var28;
      (var28 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_6, 4, 2, (Font)null, new Color(0, 0, 0)));
      (var6 = new GridBagConstraints()).weightx = 1.0D;
      var6.anchor = 18;
      var6.insets = new Insets(0, 0, 5, 5);
      var6.gridx = 0;
      var6.gridy = 1;
      var10.add(var28, var6);
      (var9 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0};
      var9.rowHeights = new int[]{0, 0, 0};
      var9.columnWeights = new double[]{1.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var9.rowWeights = new double[]{0.0D, 1.0D, Double.MIN_VALUE};
      var28.setLayout(var9);
      this.rdbtnMultiPlayer = new JRadioButton("");
      (var14 = new GridBagConstraints()).anchor = 17;
      var14.insets = new Insets(0, 0, 5, 5);
      var14.gridx = 0;
      var14.gridy = 0;
      var28.add(this.rdbtnMultiPlayer, var14);
      this.rdbtnMultiPlayer.setSelected(EngineSettings.S_INITIAL_SETTING.getCurrentState().equals("Multi Player"));
      this.buttonGroup.add(this.rdbtnMultiPlayer);
      var24 = new JPanel();
      GridBagConstraints var12;
      (var12 = new GridBagConstraints()).insets = new Insets(0, 10, 5, 5);
      var12.weightx = 100.0D;
      var12.anchor = 18;
      var12.gridx = 1;
      var12.gridy = 0;
      var28.add(var24, var12);
      GridBagLayout var21;
      (var21 = new GridBagLayout()).columnWidths = new int[]{52, 0, 80, 0};
      var21.rowHeights = new int[]{20, 0};
      var21.columnWeights = new double[]{0.0D, 1.0D, 0.0D, Double.MIN_VALUE};
      var21.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var24.setLayout(var21);
      JLabel var7 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_7);
      (var12 = new GridBagConstraints()).ipady = 1;
      var12.ipadx = 1;
      var12.fill = 2;
      var12.insets = new Insets(0, 0, 0, 5);
      var12.gridx = 0;
      var12.gridy = 0;
      var24.add(var7, var12);
      this.hostPortTextField.setEditable(true);
      this.hostPortTextField.setSelectedIndex(var2.size() - 1);
      this.hostPortTextField.getEditor().setItem(((HostPortLoginName)var2.get(var2.size() - 1)).toString());
      GridBagConstraints var22;
      (var22 = new GridBagConstraints()).fill = 2;
      var22.anchor = 11;
      var22.gridx = 2;
      var22.gridy = 0;
      var24.add(this.hostPortTextField, var22);
      JButton var13;
      (var13 = new JButton(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_8)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            JDialog var2;
            (var2 = new JDialog(ClientDialog.this, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_9)).setAlwaysOnTop(true);
            var2.setContentPane(new ServerListPanel(ClientDialog.this, var2));
            var2.setSize(900, 600);
            var2.setDefaultCloseOperation(2);
            var2.setVisible(true);
         }
      });
      GridBagConstraints var11;
      (var11 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var11.gridx = 2;
      var11.gridy = 0;
      var28.add(var13, var11);
      this.scrollPane = new JScrollPane();
      (var5 = new GridBagConstraints()).weighty = 1.0D;
      var5.weightx = 1.0D;
      var5.fill = 1;
      var5.gridx = 0;
      var5.gridy = 3;
      this.contentPanel.add(this.scrollPane, var5);
      ClientSchineSettingsPanel var23 = new ClientSchineSettingsPanel(this);
      this.scrollPane.setViewportView(var23);
      var23.setBorder(new TitledBorder((Border)null, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_18, 4, 2, (Font)null, (Color)null));
      (var10 = new JPanel()).setLayout(new FlowLayout(2));
      this.mainPanel.add(var10, "South");
      this.okButton = new JButton(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_12);
      this.okButton.setMultiClickThreshhold(1000L);
      this.okButton.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ClientDialog.this.okPressed(ClientDialog.this);
         }
      });
      this.okButton.setActionCommand(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_11);
      var10.add(this.okButton);
      this.getRootPane().setDefaultButton(this.okButton);
      (var19 = new JButton(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_14)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            System.err.println("Exiting because of pressing 'cancel' client dialog");

            try {
               throw new Exception("System.exit() called");
            } catch (Exception var2) {
               var2.printStackTrace();
               System.exit(0);
            }
         }
      });
      var19.setActionCommand(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_13);
      var10.add(var19);
      JMenuBar var30 = new JMenuBar();
      this.setJMenuBar(var30);
      JMenu var16 = new JMenu(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_15);
      var30.add(var16);
      JMenuItem var26;
      (var26 = new JMenuItem(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_16)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            LoginDialog var2;
            (var2 = new LoginDialog(ClientDialog.this)).setDefaultCloseOperation(2);
            var2.setVisible(true);
         }
      });
      var16.add(var26);
      var16 = new JMenu("Settings");
      var30.add(var16);
      final JCheckBoxMenuItem var29;
      (var29 = new JCheckBoxMenuItem(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_17)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            if (var29.isSelected()) {
               ClientSchineAdvancedSettingsPanel var3 = new ClientSchineAdvancedSettingsPanel();
               ClientDialog.this.scrollPane.setViewportView(var3);
               var3.setBorder(new TitledBorder((Border)null, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_19, 4, 2, (Font)null, (Color)null));
            } else {
               ClientSchineSettingsPanel var2 = new ClientSchineSettingsPanel(ClientDialog.this);
               ClientDialog.this.scrollPane.setViewportView(var2);
               var2.setBorder(new TitledBorder((Border)null, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_10, 4, 2, (Font)null, (Color)null));
            }
         }
      });
      var16.add(var29);
      var16 = new JMenu("Tools");
      var30.add(var16);
      (var26 = new JMenuItem(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_20)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            CatalogManagerEditorController var2;
            (var2 = new CatalogManagerEditorController(ClientDialog.this)).setDefaultCloseOperation(2);
            var2.setVisible(true);
         }
      });
      JMenuItem var27;
      (var27 = new JMenuItem(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_21)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            LanguageEditor.main(new String[]{"disposeonexit"});
         }
      });
      var16.add(var27);
      var16.add(var26);
      (var26 = new JMenuItem(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_22)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            try {
               if (JOptionPane.showOptionDialog(ClientDialog.this, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_23, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_24, 0, 1, (Icon)null, new String[]{"Ok", "Cancel"}, "Ok") != 1) {
                  JOptionPane.showMessageDialog(ClientDialog.this, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_25);
                  BluePrintController.active.migrateCatalogV0078toV0079(1);
                  JOptionPane.showOptionDialog(ClientDialog.this, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_26, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_27, 0, 1, (Icon)null, new String[]{"Ok"}, "Ok");
               }
            } catch (IOException var2) {
               var2.printStackTrace();
               GuiErrorHandler.processErrorDialogException(var2);
            }
         }
      });
      (var27 = new JMenuItem(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_28)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            StarCalc.main((String[])null);
         }
      });
      var16.add(var27);
      var16.add(var26);
      (var26 = new JMenuItem(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_29)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            Object[] var3 = new Object[]{Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_30, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_31, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_32};
            switch(JOptionPane.showOptionDialog(ClientDialog.this, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_33, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_34, 1, 3, (Icon)null, var3, var3[2])) {
            case 0:
               try {
                  Starter.cleanUniverseWithBackup();
               } catch (IOException var2) {
                  var2.printStackTrace();
                  GuiErrorHandler.processErrorDialogException(var2);
               }

               System.out.println("RESETTING DB");
               break;
            case 1:
               Starter.cleanUniverseWithoutBackup();
            case 2:
            }

            System.out.println("Creating empty database");
            (new FileExt(GameServerState.ENTITY_DATABASE_PATH)).mkdirs();
            (new FileExt(GameServerState.SEGMENT_DATA_DATABASE_PATH)).mkdirs();
         }
      });
      var16.add(var26);
      (var26 = new JMenuItem(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_35)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            try {
               DatabaseIndex var3;
               (var3 = new DatabaseIndex()).display("SELECT * FROM FTL");
               var3.display("SELECT * FROM ENTITIES");
               var3.display("SELECT * FROM SECTORS");
               var3.destroy();
            } catch (SQLException var2) {
               var2.printStackTrace();
            }
         }
      });
      var16.add(var26);
      (var26 = new JMenuItem(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_36)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            (new Staremote()).startConnectionGUI();
         }
      });
      var16.add(var26);
      (var26 = new JMenuItem(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_37)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            Object[] var2 = new Object[]{"Reset", "Cancel"};
            switch(JOptionPane.showOptionDialog(ClientDialog.this, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_38, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_39, 1, 3, (Icon)null, var2, var2[1])) {
            case 0:
               Starter.cleanClientCacheWithoutBackup();
            default:
               (new FileExt(ClientStatics.ENTITY_DATABASE_PATH)).mkdirs();
               (new FileExt(ClientStatics.SEGMENT_DATA_DATABASE_PATH)).mkdirs();
            }
         }
      });
      var16.add(var26);
      var16 = new JMenu(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_40);
      var30.add(var16);
      (var26 = new JMenuItem(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_41)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            CrashReportGUI.main(new String[0]);
         }
      });
      var16.add(var26);
      var16 = new JMenu(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_42);
      var30.add(var16);
      (var26 = new JMenuItem(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_43)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            EventQueue.invokeLater(new Runnable() {
               public void run() {
                  try {
                     final ElementEditorFrame var1 = new ElementEditorFrame();
                     SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                           var1.setVisible(true);
                        }
                     });
                  } catch (Exception var2) {
                     var2.printStackTrace();
                     GuiErrorHandler.processErrorDialogException(var2);
                  }
               }
            });
         }
      });
      var16.add(var26);
      (var26 = new JMenuItem(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_44)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            (new CustomSkinCreateDialog(ClientDialog.this)).setVisible(true);
         }
      });
      var16.add(var26);
      (var26 = new JMenuItem(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_45)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            (new CustomSkinLoadDialog(ClientDialog.this)).setVisible(true);
         }
      });
      var16.add(var26);
      (new Thread(new Runnable() {
         public void run() {
            try {
               Thread.sleep(1200L);
            } catch (InterruptedException var2) {
               var2.printStackTrace();
            }

            for(; ClientDialog.this.isVisible(); EventQueue.invokeLater(new Runnable() {
               public void run() {
                  ClientDialog.this.repaint();
               }
            })) {
               try {
                  Thread.sleep(500L);
               } catch (InterruptedException var1) {
                  var1.printStackTrace();
               }
            }

         }
      })).start();
   }

   public void dispose() {
      try {
         EngineSettings.write();
      } catch (IOException var1) {
         var1.printStackTrace();
      }

      super.dispose();
   }

   private void okPressed(JFrame var1) {
      try {
         System.out.println("[CLIENT][TEXTURE] CURRENT TEX PACK: " + EngineSettings.G_TEXTURE_PACK.getCurrentState());
         int var2 = 0;
         String var3;
         if (!(var3 = EngineSettings.G_TEXTURE_PACK.getCurrentState().toString()).equals("Default") && !var3.equals("Pixel")) {
            var2 = JOptionPane.showOptionDialog(var1, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_46, "TexturePack Warning", 2, 0, (Icon)null, new String[]{"Ok", "Cancel"}, "Ok");
         }

         if (var2 == 0) {
            this.onDialogOK();
         }

      } catch (Exception var4) {
         JOptionPane.showOptionDialog(var1, var4.getMessage(), Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_47, 0, 0, (Icon)null, new String[]{"Ok"}, "Ok");
      }
   }

   private void onDialogOK() {
      String var1;
      if ((var1 = this.loginNameTextField.getText().trim()).length() <= 0) {
         throw new IllegalArgumentException(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_53);
      } else if (var1.length() > 32) {
         throw new IllegalArgumentException(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_54);
      } else if (var1.length() <= 2) {
         throw new IllegalArgumentException(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_55);
      } else if (var1.matches("[_-]+")) {
         throw new IllegalArgumentException(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_56);
      } else if (!var1.matches("[a-zA-Z0-9_-]+")) {
         throw new IllegalArgumentException(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_57);
      } else if (this.rdbtnSinglePlayer.isSelected()) {
         this.startLocalServer = true;
         HostPortLoginName var2 = new HostPortLoginName("localhost", 4242, (byte)0, var1);
         this.callBack.callBack(var2);
      } else {
         this.connectMultiplayer(this.hostPortTextField.getSelectedItem().toString());
      }
   }

   public void connectMultiplayer(String var1) {
      String var2;
      if ((var2 = this.loginNameTextField.getText().trim()).length() <= 0) {
         throw new IllegalArgumentException(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_48);
      } else if (var2.length() > 32) {
         throw new IllegalArgumentException(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_49);
      } else if (var2.length() <= 2) {
         throw new IllegalArgumentException(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_50);
      } else if (var2.matches("[_-]+")) {
         throw new IllegalArgumentException(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_51);
      } else if (!var2.matches("[a-zA-Z0-9_-]+")) {
         throw new IllegalArgumentException(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_52);
      } else {
         String[] var5;
         if ((var5 = var1.split("\\s*:\\s*")).length != 2) {
            throw new IllegalArgumentException(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_60);
         } else {
            int var3;
            try {
               var3 = Integer.parseInt(var5[1].trim());
            } catch (NumberFormatException var4) {
               throw new IllegalArgumentException(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_59, var5[1].trim()));
            }

            if ((var1 = var5[0].trim()).length() == 0) {
               throw new IllegalArgumentException(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTDIALOG_58);
            } else {
               HostPortLoginName var6 = new HostPortLoginName(var1, var3, (byte)0, var2);
               this.callBack.callBack(var6);
            }
         }
      }
   }

   class ComboBoxListModel implements ComboBoxModel {
      String selected;

      private ComboBoxListModel() {
      }

      public int getSize() {
         return ClientDialog.this.history.size();
      }

      public Object getElementAt(int var1) {
         return ((HostPortLoginName)ClientDialog.this.history.get(var1)).host + ":" + ((HostPortLoginName)ClientDialog.this.history.get(var1)).port;
      }

      public void addListDataListener(ListDataListener var1) {
      }

      public Object getSelectedItem() {
         return this.selected;
      }

      public void removeListDataListener(ListDataListener var1) {
      }

      public void setSelectedItem(Object var1) {
         this.selected = (String)var1;
      }

      // $FF: synthetic method
      ComboBoxListModel(Object var2) {
         this();
      }
   }

   class CallBack extends Observable {
      private CallBack() {
      }

      public void callBack(HostPortLoginName var1) {
         try {
            EngineSettings.write();
         } catch (IOException var4) {
            var4.printStackTrace();
         }

         if (ClientDialog.this.startLocalServer) {
            this.setChanged();
            this.notifyObservers();
         } else {
            Starter.serverInitFinished = true;
            synchronized(Starter.serverLock) {
               Starter.serverLock.notify();
            }
         }

         this.setChanged();
         this.notifyObservers(var1);
         ClientDialog.this.setVisible(false);
         ClientDialog.this.setVisible(false);
      }

      // $FF: synthetic method
      CallBack(Object var2) {
         this();
      }
   }
}
