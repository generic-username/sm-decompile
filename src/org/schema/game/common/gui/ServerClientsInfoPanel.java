package org.schema.game.common.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.server.ServerController;

public class ServerClientsInfoPanel extends JPanel {
   private static final long serialVersionUID = 1L;
   private JTable clientTable;
   private ClientTableModel clientTableModel;
   private ServerController serverController;

   public ServerClientsInfoPanel(ServerController var1) {
      this.serverController = var1;
      this.clientTableModel = new ClientTableModel();
      this.setLayout(new BorderLayout(0, 0));
      this.clientTable = new JTable(this.clientTableModel);
      this.clientTable.setFillsViewportHeight(true);
      this.clientTable.setAutoCreateRowSorter(true);
      this.clientTable.addMouseListener(new MouseAdapter() {
         public void mouseReleased(MouseEvent var1) {
            int var2;
            if ((var2 = ServerClientsInfoPanel.this.clientTable.rowAtPoint(var1.getPoint())) >= 0 && var2 < ServerClientsInfoPanel.this.clientTable.getRowCount()) {
               ServerClientsInfoPanel.this.clientTable.setRowSelectionInterval(var2, var2);
            } else {
               ServerClientsInfoPanel.this.clientTable.clearSelection();
            }

            if ((var2 = ServerClientsInfoPanel.this.clientTable.getSelectedRow()) >= 0) {
               if (var1.isPopupTrigger() && var1.getComponent() instanceof JTable) {
                  ServerClientsInfoPanel.this.popUp(var2).show(var1.getComponent(), var1.getX(), var1.getY());
               }

            }
         }
      });
      this.add(this.clientTable);
      this.add(this.clientTable, "Center");
      this.add(this.clientTable.getTableHeader(), "North");
   }

   private JPopupMenu popUp(final int var1) {
      JPopupMenu var2 = new JPopupMenu();
      JMenuItem var3;
      (var3 = new JMenuItem("KICK")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            try {
               Object var4 = ServerClientsInfoPanel.this.clientTableModel.getValueAt(var1, 1);
               String var2 = ServerClientsInfoPanel.this.clientTableModel.getValueAt(var1, 2).toString();
               System.err.println("[SERVER] GUI KICK PLAYER: " + var4);
               ServerClientsInfoPanel.this.serverController.sendLogout(Integer.parseInt(var4.toString()), Lng.ORG_SCHEMA_GAME_COMMON_GUI_SERVERCLIENTSINFOPANEL_0);
               ServerClientsInfoPanel.this.serverController.unregister(Integer.parseInt(var4.toString()));
               ServerClientsInfoPanel.this.serverController.broadcastMessage(new Object[]{451, var2}, 0);
            } catch (Exception var3) {
               var3.printStackTrace();
            }
         }
      });
      var2.add(var3);
      return var2;
   }

   public void update(ServerController var1) {
      this.clientTableModel.update(var1);
   }
}
