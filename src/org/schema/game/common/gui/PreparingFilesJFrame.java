package org.schema.game.common.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class PreparingFilesJFrame extends JFrame {
   private static final long serialVersionUID = 1L;
   private JPanel contentPane;

   public PreparingFilesJFrame() {
      this.setResizable(false);
      this.setAlwaysOnTop(true);
      this.setTitle("Preparing Files");
      this.setDefaultCloseOperation(3);
      this.setBounds(400, 400, 447, 91);
      this.contentPane = new JPanel();
      this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.contentPane.setLayout(new BorderLayout(0, 0));
      this.setContentPane(this.contentPane);
      JLabel var1 = new JLabel("Preparing Files (one time operation)...");
      this.contentPane.add(var1, "Center");
   }

   public static void main(String[] var0) {
      EventQueue.invokeLater(new Runnable() {
         public final void run() {
            try {
               (new PreparingFilesJFrame()).setVisible(true);
            } catch (Exception var1) {
               var1.printStackTrace();
            }
         }
      });
   }
}
