package org.schema.game.common.gui;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.states.StaticStates;

public class EngineSettingCustomComboBoxModel extends AbstractListModel implements ComboBoxModel {
   private static final long serialVersionUID = 1L;
   private EngineSettings setting;
   private String[] possibilities;

   public EngineSettingCustomComboBoxModel(EngineSettings var1, String[] var2) {
      assert var1.getPossibleStates() instanceof StaticStates;

      this.setting = var1;
      this.possibilities = var2;
   }

   public int getSize() {
      return this.possibilities.length;
   }

   public Object getElementAt(int var1) {
      return var1 >= 0 ? this.possibilities[var1] : this.setting.getCurrentState();
   }

   public Object getSelectedItem() {
      return this.setting.getCurrentState();
   }

   public void setSelectedItem(Object var1) {
      this.setting.setCurrentState(var1);
   }
}
