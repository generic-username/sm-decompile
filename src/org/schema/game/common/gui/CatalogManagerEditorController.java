package org.schema.game.common.gui;

import java.awt.BorderLayout;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import org.schema.game.server.data.GameServerState;

public class CatalogManagerEditorController extends JDialog {
   private static final long serialVersionUID = 1L;
   private JPanel contentPane;

   public CatalogManagerEditorController(JFrame var1) {
      this.setDefaultCloseOperation(2);
      this.setBounds(100, 100, 688, 351);
      this.contentPane = new JPanel();
      this.setTitle("StarMade Ship Catalog Manager");
      this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.contentPane.setLayout(new BorderLayout(0, 0));
      this.setContentPane(this.contentPane);
      this.setAlwaysOnTop(true);
      CatalogPanel var2 = new CatalogPanel(this);
      this.contentPane.add(var2, "Center");
   }

   public static void main(String[] var0) {
      JFrame var2;
      (var2 = new JFrame("TT")).setDefaultCloseOperation(3);
      GameServerState.initPaths(false, 0);
      CatalogManagerEditorController var1 = new CatalogManagerEditorController(var2);
      var2.setVisible(true);
      var1.setVisible(true);
   }
}
