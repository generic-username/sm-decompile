package org.schema.game.common.gui.worldmanager;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;

public class WorldManagerTableModel extends AbstractTableModel {
   private static final long serialVersionUID = 1L;
   private Vector infos = new Vector();

   public String getColumnName(int var1) {
      if (var1 == 0) {
         return "Name";
      } else if (var1 == 1) {
         return "Default";
      } else {
         return var1 == 2 ? "Location" : "unknown";
      }
   }

   public Class getColumnClass(int var1) {
      if (var1 == 0) {
         return String.class;
      } else if (var1 == 1) {
         return String.class;
      } else {
         return var1 == 2 ? String.class : super.getColumnClass(var1);
      }
   }

   public int getRowCount() {
      return this.infos.size();
   }

   public int getColumnCount() {
      return 3;
   }

   public Object getValueAt(int var1, int var2) {
      WorldInfo var3 = (WorldInfo)this.infos.get(var1);
      if (var2 == 0) {
         return var3.name;
      } else if (var2 == 1) {
         return var3.isDefault();
      } else {
         return var2 == 2 ? var3.path : "-";
      }
   }

   public int getVersionColumn() {
      return 3;
   }

   public void clear() {
      this.infos.clear();
      this.fireTableDataChanged();
   }

   public void update(WorldInfo var1) {
      this.infos.add(var1);
      this.fireTableDataChanged();
   }

   public TableRowSorter getSorter() {
      TableRowSorter var1;
      (var1 = new TableRowSorter(this) {
      }).setComparator(0, new Comparator() {
         public int compare(String var1, String var2) {
            return var1.compareTo(var2);
         }
      });
      var1.setComparator(1, new Comparator() {
         public int compare(String var1, String var2) {
            return var1.compareTo(var2);
         }
      });
      var1.setComparator(2, new Comparator() {
         public int compare(String var1, String var2) {
            return var1.compareTo(var2);
         }
      });
      return var1;
   }

   public void addAll(Collection var1) {
      this.infos.addAll(var1);
      this.fireTableDataChanged();
   }

   public void replaceAll(List var1) {
      this.infos.clear();
      this.addAll(var1);
   }
}
