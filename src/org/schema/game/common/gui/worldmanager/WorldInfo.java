package org.schema.game.common.gui.worldmanager;

public class WorldInfo {
   public final String name;
   public final String path;
   private final boolean defaultWorld;

   public WorldInfo(String var1, String var2, boolean var3) {
      this.name = var1;
      this.path = var2;
      this.defaultWorld = var3;
   }

   public boolean isDefault() {
      return this.defaultWorld;
   }
}
