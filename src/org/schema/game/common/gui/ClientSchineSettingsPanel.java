package org.schema.game.common.gui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.ComboBoxModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.ListDataListener;
import org.schema.common.util.StringTools;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.TexturePack;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class ClientSchineSettingsPanel extends JPanel {
   private static final long serialVersionUID = 1L;
   private final TexturePack[] texturePacks = TexturePack.createTexturePacks();
   protected TexturePack current;

   public ClientSchineSettingsPanel(final JFrame var1) {
      this.current = this.texturePacks[0];

      int var2;
      for(var2 = 0; var2 < this.texturePacks.length; ++var2) {
         if (this.texturePacks[var2].name.equals("Cartoon")) {
            this.current = this.texturePacks[var2];
         }
      }

      for(var2 = 0; var2 < this.texturePacks.length; ++var2) {
         if (this.texturePacks[var2].name.equals(EngineSettings.G_TEXTURE_PACK.getCurrentState())) {
            this.current = this.texturePacks[var2];
         }
      }

      GridBagLayout var13;
      (var13 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var13.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
      var13.columnWeights = new double[]{0.0D, 1.0D, Double.MIN_VALUE};
      var13.rowWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      this.setLayout(var13);
      JLabel var14 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_0);
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).anchor = 17;
      var3.insets = new Insets(0, 5, 5, 5);
      var3.gridx = 0;
      var3.gridy = 0;
      this.add(var14, var3);
      SettingComboBox var15 = new SettingComboBox(EngineSettings.TUTORIAL_NEW);
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.fill = 2;
      var3.gridx = 1;
      var3.gridy = 0;
      this.add(var15, var3);
      var14 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_1);
      (var3 = new GridBagConstraints()).anchor = 17;
      var3.insets = new Insets(0, 5, 5, 5);
      var3.gridx = 0;
      var3.gridy = 1;
      this.add(var14, var3);
      final String var18 = Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_2;
      final String var16 = Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_3;
      final String var4 = Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_4;
      final String var5 = Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_5;
      String[] var6 = new String[]{var18, var16, var4, var5};
      final JComboBox var24;
      (var24 = new JComboBox(var6)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            if (var24.getSelectedItem().equals(var18)) {
               ServerConfig.AI_WEAPON_AIMING_ACCURACY.setCurrentState(10);
            } else if (var24.getSelectedItem().equals(var16)) {
               ServerConfig.AI_WEAPON_AIMING_ACCURACY.setCurrentState(30);
            } else if (var24.getSelectedItem().equals(var4)) {
               ServerConfig.AI_WEAPON_AIMING_ACCURACY.setCurrentState(100);
            } else if (var24.getSelectedItem().equals(var5)) {
               ServerConfig.AI_WEAPON_AIMING_ACCURACY.setCurrentState(1000);
            }

            try {
               System.err.println("WRITTEN SERVER DIFFICULTY LEVEL " + ServerConfig.AI_WEAPON_AIMING_ACCURACY.getCurrentState());
               ServerConfig.write();
            } catch (IOException var2) {
               var2.printStackTrace();
            }
         }
      });
      ServerConfig.read();
      switch((Integer)ServerConfig.AI_WEAPON_AIMING_ACCURACY.getCurrentState()) {
      case 10:
         System.err.println("Difficulty read: easy");
         var24.setSelectedIndex(0);
         break;
      case 30:
         System.err.println("Difficulty read: medium");
         var24.setSelectedIndex(1);
         break;
      case 100:
         System.err.println("Difficulty read: hard");
         var24.setSelectedIndex(2);
         break;
      case 1000:
         System.err.println("Difficulty read: mean");
         var24.setSelectedIndex(3);
      }

      GridBagConstraints var20;
      (var20 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var20.fill = 2;
      var20.gridx = 1;
      var20.gridy = 1;
      this.add(var24, var20);
      var14 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_6);
      (var3 = new GridBagConstraints()).anchor = 17;
      var3.insets = new Insets(0, 5, 5, 5);
      var3.gridx = 0;
      var3.gridy = 2;
      this.add(var14, var3);
      var15 = new SettingComboBox(EngineSettings.G_SINGLEPLAYER_CREATIVE_MODE);
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.fill = 2;
      var3.gridx = 1;
      var3.gridy = 2;
      this.add(var15, var3);
      (var14 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_7)).setFont(new Font("Arial", 0, 13));
      (var3 = new GridBagConstraints()).anchor = 17;
      var3.gridwidth = 2;
      var3.insets = new Insets(5, 0, 5, 0);
      var3.gridx = 0;
      var3.gridy = 3;
      this.add(var14, var3);
      var14 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_8);
      (var3 = new GridBagConstraints()).insets = new Insets(0, 5, 5, 5);
      var3.anchor = 17;
      var3.gridx = 0;
      var3.gridy = 4;
      this.add(var14, var3);
      SettingComboBox var22 = new SettingComboBox(EngineSettings.G_RESOLUTION);
      GridBagConstraints var17;
      (var17 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var17.fill = 2;
      var17.gridx = 1;
      var17.gridy = 4;
      this.add(var22, var17);
      JLabel var19 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_9);
      GridBagConstraints var21;
      (var21 = new GridBagConstraints()).anchor = 17;
      var21.insets = new Insets(0, 5, 5, 5);
      var21.gridx = 0;
      var21.gridy = 5;
      this.add(var19, var21);
      SettingComboBox var23 = new SettingComboBox(EngineSettings.G_FULLSCREEN);
      GridBagConstraints var25;
      (var25 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var25.fill = 2;
      var25.gridx = 1;
      var25.gridy = 5;
      this.add(var23, var25);
      JLabel var26 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_10);
      GridBagConstraints var7;
      (var7 = new GridBagConstraints()).anchor = 17;
      var7.insets = new Insets(0, 5, 5, 5);
      var7.gridx = 0;
      var7.gridy = 6;
      this.add(var26, var7);
      SettingComboBox var27 = new SettingComboBox(EngineSettings.G_FOV);
      (var7 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var7.fill = 2;
      var7.gridx = 1;
      var7.gridy = 6;
      this.add(var27, var7);
      var26 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_11);
      (var7 = new GridBagConstraints()).anchor = 17;
      var7.insets = new Insets(0, 5, 5, 5);
      var7.gridx = 0;
      var7.gridy = 7;
      this.add(var26, var7);
      var27 = new SettingComboBox(EngineSettings.G_MAX_SEGMENTSDRAWN);
      (var7 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var7.fill = 2;
      var7.gridx = 1;
      var7.gridy = 7;
      this.add(var27, var7);
      var26 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_12);
      (var7 = new GridBagConstraints()).anchor = 17;
      var7.insets = new Insets(0, 5, 5, 5);
      var7.gridx = 0;
      var7.gridy = 8;
      this.add(var26, var7);
      JPanel var28 = new JPanel();
      (var7 = new GridBagConstraints()).fill = 1;
      var7.insets = new Insets(0, 0, 5, 0);
      var7.gridx = 1;
      var7.gridy = 8;
      this.add(var28, var7);
      GridBagLayout var29;
      (var29 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var29.rowHeights = new int[]{0, 0};
      var29.columnWeights = new double[]{1.0D, 1.0D};
      var29.rowWeights = new double[]{1.0D, 1.0D};
      var28.setLayout(var29);
      SettingComboBox var30 = new SettingComboBox(EngineSettings.G_SHADOWS);
      GridBagConstraints var8;
      (var8 = new GridBagConstraints()).fill = 2;
      var8.gridx = 0;
      var8.gridy = 0;
      var28.add(var30, var8);
      var30 = new SettingComboBox(EngineSettings.G_SHADOW_QUALITY);
      (var8 = new GridBagConstraints()).fill = 2;
      var8.gridx = 1;
      var8.gridy = 0;
      var28.add(var30, var8);
      var26 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_13);
      (var7 = new GridBagConstraints()).anchor = 17;
      var7.insets = new Insets(0, 5, 5, 5);
      var7.gridx = 0;
      var7.gridy = 9;
      this.add(var26, var7);
      var27 = new SettingComboBox(EngineSettings.G_NORMAL_MAPPING);
      (var7 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var7.fill = 2;
      var7.gridx = 1;
      var7.gridy = 9;
      this.add(var27, var7);
      var26 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_14);
      (var7 = new GridBagConstraints()).anchor = 17;
      var7.insets = new Insets(0, 5, 5, 5);
      var7.gridx = 0;
      var7.gridy = 10;
      this.add(var26, var7);
      var27 = new SettingComboBox(EngineSettings.LIGHT_RAY_COUNT);
      (var7 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var7.fill = 2;
      var7.gridx = 1;
      var7.gridy = 10;
      this.add(var27, var7);
      var26 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_15);
      (var7 = new GridBagConstraints()).anchor = 17;
      var7.insets = new Insets(0, 5, 5, 5);
      var7.gridx = 0;
      var7.gridy = 11;
      this.add(var26, var7);
      String var32 = EngineSettings.O_OCULUS_RENDERING.isOn() ? Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_16 : Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_17;
      final JButton var34;
      (var34 = new JButton(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_18, var32))).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            if (EngineSettings.O_OCULUS_RENDERING.isOn()) {
               EngineSettings.O_OCULUS_RENDERING.changeBooleanSetting(false);
               EngineSettings.F_FRAME_BUFFER.changeBooleanSetting(false);

               try {
                  EngineSettings.write();
               } catch (IOException var4) {
                  var1x = null;
                  var4.printStackTrace();
               }

               JOptionPane.showMessageDialog(var1, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_23, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_22, 1);
               var34.setText(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_25);
            } else {
               String var6 = Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_19;
               String[] var2 = new String[]{Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_20, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_21};
               int var10000 = JOptionPane.showOptionDialog(var1, var6, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_24, 0, 1, (Icon)null, var2, var2[1]);
               boolean var7 = false;
               if (var10000 == 0) {
                  EngineSettings.O_OCULUS_RENDERING.changeBooleanSetting(true);
                  EngineSettings.F_FRAME_BUFFER.changeBooleanSetting(true);

                  try {
                     EngineSettings.write();
                  } catch (IOException var3) {
                     var1x = null;
                     var3.printStackTrace();
                  }

                  try {
                     throw new Exception("System.exit() called");
                  } catch (Exception var5) {
                     var1x = null;
                     var5.printStackTrace();
                     System.exit(0);
                  }
               }

            }
         }
      });
      var34.setMinimumSize(new Dimension(145, 21));
      var34.setPreferredSize(new Dimension(145, 21));
      (var7 = new GridBagConstraints()).fill = 2;
      var7.anchor = 17;
      var7.insets = new Insets(0, 0, 5, 0);
      var7.gridx = 1;
      var7.gridy = 11;
      this.add(var34, var7);
      var26 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_26);
      (var7 = new GridBagConstraints()).anchor = 17;
      var7.insets = new Insets(0, 5, 5, 5);
      var7.gridx = 0;
      var7.gridy = 12;
      this.add(var26, var7);
      var28 = new JPanel();
      (var7 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var7.fill = 1;
      var7.gridx = 1;
      var7.gridy = 12;
      this.add(var28, var7);
      (var29 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0};
      var29.rowHeights = new int[]{0, 0, 0};
      var29.columnWeights = new double[]{1.0D, 0.0D, 1.0D, Double.MIN_VALUE};
      var29.rowWeights = new double[]{1.0D, 1.0D, Double.MIN_VALUE};
      var28.setLayout(var29);
      var30 = new SettingComboBox(EngineSettings.G_PROD_BG);
      (var8 = new GridBagConstraints()).fill = 2;
      var8.insets = new Insets(0, 0, 5, 5);
      var8.gridx = 0;
      var8.gridy = 0;
      var28.add(var30, var8);
      JLabel var35 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_27);
      (var8 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var8.anchor = 13;
      var8.gridx = 1;
      var8.gridy = 0;
      var28.add(var35, var8);
      var30 = new SettingComboBox(EngineSettings.G_PROD_BG_QUALITY);
      (var8 = new GridBagConstraints()).fill = 2;
      var8.insets = new Insets(0, 0, 5, 0);
      var8.gridx = 2;
      var8.gridy = 0;
      var28.add(var30, var8);
      var26 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_28);
      (var7 = new GridBagConstraints()).anchor = 17;
      var7.insets = new Insets(0, 5, 5, 5);
      var7.gridx = 0;
      var7.gridy = 13;
      this.add(var26, var7);
      var28 = new JPanel();
      (var7 = new GridBagConstraints()).fill = 2;
      var7.anchor = 17;
      var7.insets = new Insets(0, 0, 5, 0);
      var7.gridx = 1;
      var7.gridy = 13;
      this.add(var28, var7);
      (var29 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0, 0};
      var29.rowHeights = new int[]{0, 0};
      var29.columnWeights = new double[]{0.0D, 1.0D, 0.0D, 1.0D, Double.MIN_VALUE};
      var29.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var28.setLayout(var29);
      var35 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_29);
      (var8 = new GridBagConstraints()).anchor = 13;
      var8.insets = new Insets(0, 5, 0, 5);
      var8.gridx = 0;
      var8.gridy = 0;
      var28.add(var35, var8);
      JComboBox var36 = new JComboBox(new ComboBoxModel() {
         public int getSize() {
            return ClientSchineSettingsPanel.this.texturePacks.length;
         }

         public void addListDataListener(ListDataListener var1) {
         }

         public void setSelectedItem(Object var1) {
            ClientSchineSettingsPanel.this.current = (TexturePack)var1;
            EngineSettings.G_TEXTURE_PACK.setCurrentState(ClientSchineSettingsPanel.this.current.name);
            boolean var3 = false;

            for(int var2 = 0; var2 < ClientSchineSettingsPanel.this.current.resolutions.length; ++var2) {
               if (EngineSettings.G_TEXTURE_PACK_RESOLUTION.getCurrentState().equals(new Integer(ClientSchineSettingsPanel.this.current.resolutions[var2]))) {
                  var3 = true;
                  break;
               }
            }

            if (!var3) {
               EngineSettings.G_TEXTURE_PACK_RESOLUTION.setCurrentState(ClientSchineSettingsPanel.this.current.resolutions[0]);
            }

         }

         public Object getElementAt(int var1) {
            return ClientSchineSettingsPanel.this.texturePacks[var1];
         }

         public void removeListDataListener(ListDataListener var1) {
         }

         public Object getSelectedItem() {
            return EngineSettings.G_TEXTURE_PACK.getCurrentState();
         }
      });
      (var8 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var8.fill = 1;
      var8.gridx = 1;
      var8.gridy = 0;
      var28.add(var36, var8);
      JLabel var31 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_30);
      GridBagConstraints var9;
      (var9 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var9.anchor = 13;
      var9.gridx = 2;
      var9.gridy = 0;
      var28.add(var31, var9);
      final JComboBox var33 = new JComboBox(new ComboBoxModel() {
         public void removeListDataListener(ListDataListener var1) {
         }

         public int getSize() {
            if (ClientSchineSettingsPanel.this.current != null) {
               return ClientSchineSettingsPanel.this.current.resolutions.length;
            } else {
               assert false;

               return 0;
            }
         }

         public Object getElementAt(int var1) {
            if (ClientSchineSettingsPanel.this.current != null) {
               return ClientSchineSettingsPanel.this.current.resolutions[var1];
            } else {
               assert false;

               return 128;
            }
         }

         public void addListDataListener(ListDataListener var1) {
         }

         public void setSelectedItem(Object var1) {
            EngineSettings.G_TEXTURE_PACK_RESOLUTION.setCurrentState(var1);
            EngineSettings.G_TEXTURE_PACK.setCurrentState(ClientSchineSettingsPanel.this.current.name);
         }

         public Object getSelectedItem() {
            return EngineSettings.G_TEXTURE_PACK_RESOLUTION.getCurrentState();
         }
      });
      (var9 = new GridBagConstraints()).weightx = 1.0D;
      var9.anchor = 17;
      var9.fill = 2;
      var9.gridx = 3;
      var9.gridy = 0;
      var28.add(var33, var9);
      var36.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            var33.revalidate();
            var33.repaint();
         }
      });
      var33.revalidate();
      var33.repaint();
      (var26 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_31)).setFont(new Font("Arial", 0, 13));
      (var7 = new GridBagConstraints()).anchor = 17;
      var7.insets = new Insets(10, 0, 5, 5);
      var7.gridx = 0;
      var7.gridy = 14;
      this.add(var26, var7);
      (var34 = new JButton(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_32)).setMinimumSize(new Dimension(113, 21));
      var34.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            String var6 = Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_33;
            String var2 = Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_34;
            String var3 = Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_35;
            String var4 = Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_36;
            String var5 = Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_37;
            JOptionPane.showMessageDialog(var1, var6 + "\n\n" + var2 + "\n\n" + var3 + "\n\n" + var4 + "\n\n" + var5, Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_38, 1);
         }
      });
      GridBagConstraints var10;
      (var10 = new GridBagConstraints()).anchor = 17;
      var10.insets = new Insets(0, 0, 5, 0);
      var10.gridx = 1;
      var10.gridy = 15;
      this.add(var34, var10);
      JLabel var11 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_39);
      (var25 = new GridBagConstraints()).anchor = 17;
      var25.insets = new Insets(0, 5, 5, 5);
      var25.gridx = 0;
      var25.gridy = 16;
      this.add(var11, var25);
      SettingComboBox var12 = new SettingComboBox(EngineSettings.S_SOUND_SYS_ENABLED);
      (var25 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var25.fill = 2;
      var25.gridx = 1;
      var25.gridy = 16;
      this.add(var12, var25);
      var11 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_40);
      (var25 = new GridBagConstraints()).anchor = 17;
      var25.insets = new Insets(0, 5, 5, 5);
      var25.gridx = 0;
      var25.gridy = 17;
      this.add(var11, var25);
      var12 = new SettingComboBox(EngineSettings.S_SOUND_VOLUME_GLOBAL);
      (var25 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var25.fill = 2;
      var25.gridx = 1;
      var25.gridy = 17;
      this.add(var12, var25);
      var11 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_41);
      (var25 = new GridBagConstraints()).anchor = 13;
      var25.insets = new Insets(0, 5, 5, 5);
      var25.gridx = 0;
      var25.gridy = 18;
      this.add(var11, var25);
      var12 = new SettingComboBox(EngineSettings.USE_OPEN_AL_SOUND);
      (var25 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var25.fill = 2;
      var25.gridx = 1;
      var25.gridy = 18;
      this.add(var12, var25);
      (var11 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_42)).setFont(new Font("Arial", 0, 13));
      (var25 = new GridBagConstraints()).anchor = 16;
      var25.gridwidth = 2;
      var25.insets = new Insets(10, 0, 5, 0);
      var25.gridx = 0;
      var25.gridy = 19;
      this.add(var11, var25);
      var11 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_43);
      (var25 = new GridBagConstraints()).anchor = 17;
      var25.insets = new Insets(0, 5, 5, 5);
      var25.gridx = 0;
      var25.gridy = 20;
      this.add(var11, var25);
      var12 = new SettingComboBox(EngineSettings.S_MOUSE_ALL_INVERT);
      (var25 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var25.fill = 2;
      var25.gridx = 1;
      var25.gridy = 20;
      this.add(var12, var25);
      var11 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_CLIENTSCHINESETTINGSPANEL_44);
      (var25 = new GridBagConstraints()).anchor = 17;
      var25.insets = new Insets(0, 5, 0, 5);
      var25.gridx = 0;
      var25.gridy = 21;
      this.add(var11, var25);
      var12 = new SettingComboBox(EngineSettings.S_MOUSE_SHIP_INVERT);
      (var25 = new GridBagConstraints()).fill = 2;
      var25.gridx = 1;
      var25.gridy = 21;
      this.add(var12, var25);
      var19.setLabelFor(var23);
      var14.setLabelFor(var22);
   }
}
