package org.schema.game.common.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import org.schema.game.common.ClientRunnable;
import org.schema.game.common.Starter;
import org.schema.game.common.api.SessionNewStyle;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.game.common.util.StarMadeCredentials;
import org.schema.schine.auth.exceptions.WrongUserNameOrPasswordException;
import org.schema.schine.common.language.Lng;

public class LoginDialog extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel;
   private JTextField textField;
   private JPasswordField passwordField;
   private JCheckBox chckbxSaveCredentialsencrypted;
   private ClientRunnable callback;

   public LoginDialog(JFrame var1) {
      this(var1, (ClientRunnable)null);
   }

   public LoginDialog(JFrame var1, ClientRunnable var2) {
      super(var1);
      this.contentPanel = new JPanel();
      this.callback = var2;
      this.setTitle(Lng.ORG_SCHEMA_GAME_COMMON_GUI_LOGINDIALOG_0);
      this.setBounds(100, 100, 470, 275);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var4;
      (var4 = new GridBagLayout()).columnWidths = new int[]{0};
      var4.rowHeights = new int[]{0, 0, 20, 0, 0, 0, 0};
      var4.columnWeights = new double[]{0.0D};
      var4.rowWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D};
      this.contentPanel.setLayout(var4);
      JLabel var5 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_LOGINDIALOG_1);
      GridBagConstraints var6;
      (var6 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var6.gridx = 0;
      var6.gridy = 0;
      this.contentPanel.add(var5, var6);
      var5 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_LOGINDIALOG_2);
      (var6 = new GridBagConstraints()).insets = new Insets(0, 0, 20, 0);
      var6.gridx = 0;
      var6.gridy = 1;
      this.contentPanel.add(var5, var6);
      JPanel var7;
      (var7 = new JPanel()).setBorder(new EtchedBorder(1, (Color)null, (Color)null));
      (var6 = new GridBagConstraints()).weightx = 1.0D;
      var6.fill = 1;
      var6.insets = new Insets(0, 0, 5, 0);
      var6.gridx = 0;
      var6.gridy = 2;
      this.contentPanel.add(var7, var6);
      GridBagLayout var10;
      (var10 = new GridBagLayout()).columnWidths = new int[]{0, 80, 0, 50, 0};
      var10.rowHeights = new int[]{20, 0};
      var10.columnWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var10.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var7.setLayout(var10);
      JLabel var11 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_LOGINDIALOG_3);
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).anchor = 13;
      var3.insets = new Insets(0, 0, 0, 5);
      var3.gridx = 0;
      var3.gridy = 0;
      var7.add(var11, var3);
      this.textField = new JTextField();
      (var6 = new GridBagConstraints()).weightx = 0.5D;
      var6.anchor = 11;
      var6.fill = 2;
      var6.insets = new Insets(0, 0, 0, 5);
      var6.gridx = 1;
      var6.gridy = 0;
      var7.add(this.textField, var6);
      this.textField.setColumns(13);
      var11 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_LOGINDIALOG_4);
      (var3 = new GridBagConstraints()).anchor = 13;
      var3.insets = new Insets(0, 0, 0, 5);
      var3.gridx = 2;
      var3.gridy = 0;
      var7.add(var11, var3);
      this.passwordField = new JPasswordField();
      (var6 = new GridBagConstraints()).weightx = 1.0D;
      var6.anchor = 11;
      var6.fill = 2;
      var6.gridx = 3;
      var6.gridy = 0;
      var7.add(this.passwordField, var6);
      this.chckbxSaveCredentialsencrypted = new JCheckBox("Save Login (encrypted)");
      GridBagConstraints var8;
      (var8 = new GridBagConstraints()).anchor = 13;
      var8.insets = new Insets(0, 0, 5, 0);
      var8.gridx = 0;
      var8.gridy = 3;
      this.contentPanel.add(this.chckbxSaveCredentialsencrypted, var8);
      JButton var9;
      (var9 = new JButton(Lng.ORG_SCHEMA_GAME_COMMON_GUI_LOGINDIALOG_5)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            try {
               StarMadeCredentials.removeFile();
            } catch (Exception var2) {
               var2.printStackTrace();
               GuiErrorHandler.processErrorDialogException(var2);
            }
         }
      });
      var9.setEnabled(StarMadeCredentials.exists());
      var9.setHorizontalAlignment(4);
      (var6 = new GridBagConstraints()).anchor = 13;
      var6.insets = new Insets(0, 0, 5, 0);
      var6.gridx = 0;
      var6.gridy = 4;
      this.contentPanel.add(var9, var6);
      (var5 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_LOGINDIALOG_6)).setForeground(new Color(139, 0, 0));
      var5.setFont(new Font("Tahoma", 1, 12));
      (var6 = new GridBagConstraints()).insets = new Insets(15, 0, 5, 0);
      var6.anchor = 17;
      var6.gridx = 0;
      var6.gridy = 5;
      this.contentPanel.add(var5, var6);
      var5 = new JLabel(Lng.ORG_SCHEMA_GAME_COMMON_GUI_LOGINDIALOG_7);
      (var6 = new GridBagConstraints()).anchor = 17;
      var6.gridx = 0;
      var6.gridy = 6;
      this.contentPanel.add(var5, var6);
      (var7 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var7, "South");
      JButton var12;
      (var12 = new JButton("OK")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            String var4 = LoginDialog.this.textField.getText();
            String var2 = new String(LoginDialog.this.passwordField.getPassword());
            switch(Starter.getAuthStyle()) {
            case 0:
               throw new IllegalArgumentException("AuthStyle " + Starter.getAuthStyle() + " is no longer supported.");
            case 1:
               try {
                  LoginDialog.this.loginNewStyle(var4, var2);
                  return;
               } catch (Exception var3) {
                  var3.printStackTrace();
                  GuiErrorHandler.processErrorDialogException(var3);
                  return;
               }
            default:
               throw new IllegalArgumentException("Authstyle Unknown: " + Starter.getAuthStyle());
            }
         }
      });
      var12.setActionCommand("OK");
      var7.add(var12);
      this.getRootPane().setDefaultButton(var12);
      (var12 = new JButton(this.callback != null ? Lng.ORG_SCHEMA_GAME_COMMON_GUI_LOGINDIALOG_10 : Lng.ORG_SCHEMA_GAME_COMMON_GUI_LOGINDIALOG_11)).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            LoginDialog.this.setVisible(false);
            LoginDialog.this.dispose();
            if (LoginDialog.this.callback != null) {
               try {
                  throw new Exception("System.exit() called");
               } catch (Exception var2) {
                  var2.printStackTrace();
                  System.exit(0);
               }
            }

         }
      });
      var12.setActionCommand(this.callback != null ? Lng.ORG_SCHEMA_GAME_COMMON_GUI_LOGINDIALOG_8 : Lng.ORG_SCHEMA_GAME_COMMON_GUI_LOGINDIALOG_9);
      var7.add(var12);
   }

   public void loginNewStyle(String var1, String var2) throws IOException, WrongUserNameOrPasswordException {
      (new SessionNewStyle("starMadeOrg")).login(var1, var2);
      if (this.chckbxSaveCredentialsencrypted.isSelected()) {
         (new StarMadeCredentials(var1, var2)).write();
      }

      this.dispose();
      if (this.callback != null) {
         this.callback.callback();
      }

   }
}
