package org.schema.game.common.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class ConnectionDialog extends JDialog implements Observer {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   public boolean exit = true;
   private JLabel lblConnecting;
   private boolean success;

   public ConnectionDialog() {
      this.setAlwaysOnTop(true);
      this.setDefaultCloseOperation(2);
      this.setTitle("StarMade Connector");
      this.setBounds(100, 100, 411, 105);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setLayout(new FlowLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      this.lblConnecting = new JLabel("Connecting");
      this.contentPanel.add(this.lblConnecting);
      JPanel var1;
      (var1 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var1, "South");
      JButton var2;
      (var2 = new JButton("Cancel")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            System.out.println("Connection Dialog Terminated (cancel) -> EXIT 0");

            try {
               Thread.sleep(500L);
            } catch (InterruptedException var3) {
               var3.printStackTrace();
            }

            try {
               throw new Exception("System.exit() called");
            } catch (Exception var2) {
               var2.printStackTrace();
               System.exit(0);
            }
         }
      });
      var2.setActionCommand("Cancel");
      var1.add(var2);
   }

   public void dispose() {
      super.dispose();
      if (!this.success && this.exit) {
         try {
            throw new RuntimeException("Disposing from unatural cause! (" + this.success + "; " + this.exit + ")");
         } catch (Exception var1) {
            var1.printStackTrace();
            System.err.println("Connection Dialog Terminated (dispose) -> EXIT 0");
            (new Thread() {
               public void run() {
                  try {
                     Thread.sleep(5000L);
                  } catch (InterruptedException var2) {
                     var2.printStackTrace();
                  }

                  try {
                     throw new Exception("System.exit() called");
                  } catch (Exception var1) {
                     var1.printStackTrace();
                     System.exit(0);
                  }
               }
            }).start();
         }
      }

   }

   public void update(Observable var1, Object var2) {
      if (var2 != null && !"STATE_DISPOSE".equals(var2)) {
         this.lblConnecting.setText(var2.toString());
      } else {
         System.err.println("[CLIENT] disposing connection dialog: " + var2);
         this.success = true;
         this.dispose();
      }
   }
}
