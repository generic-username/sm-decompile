package org.schema.game.common.gui.serverlist;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.RowFilter.Entry;
import javax.swing.table.TableRowSorter;
import org.schema.game.common.gui.ClientDialog;
import org.schema.game.common.version.Version;
import org.schema.schine.network.ServerInfo;
import org.schema.schine.network.ServerListRetriever;

public class ServerListPanel extends JPanel implements Observer {
   private static final long serialVersionUID = 1L;
   private ServerListTableModel model = new ServerListTableModel();
   private ServerListRetriever r;
   private JTable table;
   private JCheckBox chckbxShowOnlyCompatible;
   private JLabel lblIdle;
   private int notCompatible;

   public ServerListPanel(final ClientDialog var1, final JDialog var2) {
      GridBagLayout var3;
      (var3 = new GridBagLayout()).rowWeights = new double[]{0.0D, 1.0D, 0.0D};
      var3.columnWeights = new double[]{0.0D};
      this.setLayout(var3);
      JScrollPane var10 = new JScrollPane();
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).weightx = 1.0D;
      var4.weighty = 1.0D;
      var4.insets = new Insets(0, 0, 5, 0);
      var4.fill = 1;
      var4.gridx = 0;
      var4.gridy = 1;
      this.add(var10, var4);
      this.table = new ServerListTable(this.model);
      this.table.setShowVerticalLines(false);
      this.table.setSelectionMode(0);
      this.table.setFillsViewportHeight(true);
      this.table.setColumnSelectionAllowed(false);
      this.table.setCellSelectionEnabled(false);
      this.table.getColumnModel().getColumn(0).setWidth(128);
      this.table.getColumnModel().getColumn(0).setPreferredWidth(128);
      this.table.getColumnModel().getColumn(1).setWidth(128);
      this.table.getColumnModel().getColumn(1).setPreferredWidth(128);
      this.table.getColumnModel().getColumn(2).setWidth(300);
      this.table.getColumnModel().getColumn(2).setPreferredWidth(300);
      this.table.getColumnModel().getColumn(3).setWidth(50);
      this.table.getColumnModel().getColumn(3).setPreferredWidth(50);
      this.table.getColumnModel().getColumn(3).setMaxWidth(50);
      this.table.getColumnModel().getColumn(4).setWidth(50);
      this.table.getColumnModel().getColumn(4).setPreferredWidth(50);
      this.table.getColumnModel().getColumn(4).setMaxWidth(50);
      this.table.getColumnModel().getColumn(5).setWidth(40);
      this.table.getColumnModel().getColumn(5).setPreferredWidth(40);
      this.table.getColumnModel().getColumn(5).setMaxWidth(40);
      this.table.getColumnModel().getColumn(6).setWidth(60);
      this.table.getColumnModel().getColumn(6).setPreferredWidth(60);
      this.table.getColumnModel().getColumn(6).setMaxWidth(60);
      this.table.setRowSelectionAllowed(true);
      this.table.getTableHeader().setToolTipText("Click to specify sorting; Control-Click to specify secondary sorting");
      this.table.addMouseListener(new MouseAdapter() {
         public void mousePressed(MouseEvent var1x) {
            JTable var2 = (JTable)var1x.getSource();
            Point var3 = var1x.getPoint();
            int var4;
            if ((var4 = var2.rowAtPoint(var3)) >= 0 && var1x.getClickCount() == 2) {
               var1.connectMultiplayer((String)var2.getValueAt(var4, 0));
            }

         }
      });
      RowFilter var13 = new RowFilter() {
         public boolean include(Entry var1) {
            return ServerListPanel.this.chckbxShowOnlyCompatible.isSelected() ? Version.equalVersion(var1.getValue(ServerListPanel.this.model.getVersionColumn()).toString()) : true;
         }
      };
      TableRowSorter var5;
      (var5 = this.model.getSorter()).setRowFilter(var13);
      this.table.setRowSorter(var5);
      var10.setViewportView(this.table);
      JPanel var12;
      (var12 = new JPanel()).setPreferredSize(new Dimension(300, 20));
      (var4 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var4.fill = 1;
      var4.gridx = 0;
      var4.gridy = 2;
      this.add(var12, var4);
      GridBagLayout var14;
      (var14 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0};
      var14.rowHeights = new int[]{0};
      var14.columnWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D};
      var14.rowWeights = new double[]{0.0D};
      var12.setLayout(var14);
      JButton var15;
      (var15 = new JButton("Connect")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            if (ServerListPanel.this.table.getSelectedRow() >= 0) {
               var1.connectMultiplayer((String)ServerListPanel.this.table.getValueAt(ServerListPanel.this.table.getSelectedRow(), 0));
            }

         }
      });
      GridBagConstraints var6;
      (var6 = new GridBagConstraints()).weighty = 1.0D;
      var6.weightx = 1.0D;
      var6.anchor = 16;
      var6.insets = new Insets(0, 0, 0, 5);
      var6.gridx = 0;
      var6.gridy = 0;
      var12.add(var15, var6);
      JButton var7;
      (var7 = new JButton("Refresh")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ServerListPanel.this.refresh();
         }
      });
      (var4 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var4.gridx = 1;
      var4.gridy = 0;
      var12.add(var7, var4);
      (var7 = new JButton("Cancel")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            var2.dispose();
         }
      });
      this.lblIdle = new JLabel("idle");
      GridBagConstraints var8;
      (var8 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var8.gridx = 2;
      var8.gridy = 0;
      var12.add(this.lblIdle, var8);
      (var8 = new GridBagConstraints()).weighty = 1.0D;
      var8.weightx = 1.0D;
      var8.anchor = 14;
      var8.gridx = 3;
      var8.gridy = 0;
      var12.add(var7, var8);
      JPanel var9 = new JPanel();
      (var8 = new GridBagConstraints()).fill = 1;
      var8.gridx = 0;
      var8.gridy = 0;
      this.add(var9, var8);
      GridBagLayout var11;
      (var11 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var11.rowHeights = new int[]{0, 0};
      var11.columnWeights = new double[]{0.0D, Double.MIN_VALUE};
      var11.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var9.setLayout(var11);
      this.chckbxShowOnlyCompatible = new JCheckBox("Show only Compatible");
      this.chckbxShowOnlyCompatible.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ServerListPanel.this.model.fireTableDataChanged();
         }
      });
      this.chckbxShowOnlyCompatible.setSelected(true);
      (var8 = new GridBagConstraints()).gridx = 0;
      var8.gridy = 0;
      var9.add(this.chckbxShowOnlyCompatible, var8);
      this.refresh();
   }

   private void refresh() {
      this.notCompatible = 0;
      this.model.clear();
      if (this.r != null) {
         this.r.active = false;
         this.r.deleteObserver(this);
      }

      try {
         Thread.sleep(10L);
      } catch (InterruptedException var1) {
         var1.printStackTrace();
      }

      if (this.r == null) {
         this.r = new ServerListRetriever();
      }

      this.r.addObserver(this);
      this.r.startRetrieving();
   }

   public void update(Observable var1, Object var2) {
      if (var2 instanceof ServerInfo) {
         ServerInfo var4 = (ServerInfo)var2;
         this.model.update(var4);
         if (!Version.equalVersion(var4.getVersion())) {
            ++this.notCompatible;
         }

      } else if (var2 instanceof ServerListRetriever) {
         ServerListRetriever var3;
         if ((var3 = (ServerListRetriever)var2).loaded + var3.timeouts + var3.failed < var3.toLoad) {
            this.lblIdle.setText("retrieving... " + (var3.loaded + var3.timeouts + var3.failed) + " / " + var3.toLoad + " (timeouts: " + var3.timeouts + ", failed: " + var3.failed + ", incompatible: " + this.notCompatible + ")");
         } else {
            this.lblIdle.setText("done retrieving " + var3.toLoad + " servers! loaded: " + var3.loaded + " (incompatible: " + this.notCompatible + "); timeouts: " + var3.timeouts + "; failed: " + var3.failed);
         }
      } else {
         if (var2 instanceof String) {
            this.lblIdle.setText(var2.toString());
         }

      }
   }
}
