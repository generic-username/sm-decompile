package org.schema.game.common.controller.io;

import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.Channels;

public class SegmentOutputStream extends ByteArrayOutputStream {
   public SegmentOutputStream(int var1) {
      super(var1);
   }

   public int count() {
      return this.count;
   }

   public void writeToFile(RandomAccessFile var1, long var2, String var4, File var5) throws IOException {
      var1.getChannel().transferFrom(Channels.newChannel(new FastByteArrayInputStream(this.buf, 0, this.count)), var2, (long)this.count);
      this.reset();
   }

   public void writeToFileWithoutMap(RandomAccessFile var1, long var2, String var4, File var5) throws IOException {
      var1.seek(var2);
      var1.write(this.buf, 0, this.count);
      this.reset();
   }
}
