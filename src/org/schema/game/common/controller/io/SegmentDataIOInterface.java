package org.schema.game.common.controller.io;

import com.googlecode.javaewah.EWAHCompressedBitmap;
import java.io.IOException;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.DeserializationException;
import org.schema.game.common.data.world.RemoteSegment;

public interface SegmentDataIOInterface {
   void releaseFileHandles() throws IOException;

   boolean write(RemoteSegment var1, long var2, boolean var4, boolean var5) throws IOException;

   IOFileManager getManager();

   int getSize(int var1, int var2, int var3) throws IOException;

   long getTimeStamp(int var1, int var2, int var3) throws IOException;

   int request(int var1, int var2, int var3, RemoteSegment var4) throws IOException, DeserializationException;

   EWAHCompressedBitmap requestSignature(int var1, int var2, int var3) throws IOException, DeserializationException;

   void writeEmpty(int var1, int var2, int var3, SegmentController var4, long var5, boolean var7) throws IOException;
}
