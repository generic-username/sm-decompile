package org.schema.game.common.controller.io;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import org.schema.schine.resource.FileExt;

public class RandomFileOutputStream extends OutputStream {
   protected RandomAccessFile randomFile;
   protected boolean sync;

   public RandomFileOutputStream(File var1) throws IOException {
      this(var1, false);
   }

   public RandomFileOutputStream(RandomAccessFile var1, boolean var2) throws IOException {
      this.randomFile = var1;
      this.sync = var2;
   }

   public RandomFileOutputStream(File var1, boolean var2) throws IOException {
      if ((var1 = var1.getAbsoluteFile()).getParentFile() != null) {
         var1.mkdirs();
      }

      this.randomFile = new RandomAccessFile(var1, "rw");
      this.sync = var2;
   }

   public RandomFileOutputStream(IOFileManager var1, String var2, boolean var3) throws IOException {
      this.sync = var3;
   }

   public RandomFileOutputStream(String var1) throws IOException {
      this(var1, false);
   }

   public RandomFileOutputStream(String var1, boolean var2) throws IOException {
      this((File)(new FileExt(var1)), var2);
   }

   public FileDescriptor getFD() throws IOException {
      return this.randomFile.getFD();
   }

   public long getFilePointer() throws IOException {
      return this.randomFile.getFilePointer();
   }

   public void setFilePointer(long var1) throws IOException {
      this.randomFile.seek(var1);
   }

   public long getFileSize() throws IOException {
      return this.randomFile.length();
   }

   public void setFileSize(long var1) throws IOException {
      this.randomFile.setLength(var1);
   }

   public void write(int var1) throws IOException {
      this.randomFile.write(var1);
      if (this.sync) {
         this.randomFile.getFD().sync();
      }

   }

   public void write(byte[] var1) throws IOException {
      this.randomFile.write(var1);
      if (this.sync) {
         this.randomFile.getFD().sync();
      }

   }

   public void write(byte[] var1, int var2, int var3) throws IOException {
      this.randomFile.write(var1, var2, var3);
      if (this.sync) {
         this.randomFile.getFD().sync();
      }

   }

   public void flush() throws IOException {
      if (this.sync) {
         this.randomFile.getFD().sync();
      }

   }

   public void close() throws IOException {
      this.randomFile.close();
   }
}
