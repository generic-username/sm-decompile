package org.schema.game.common.controller.io;

import com.googlecode.javaewah.EWAHCompressedBitmap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.apache.commons.lang3.SerializationException;
import org.schema.common.ByteBufferInputStream;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.ClientStatics;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.TransientSegmentController;
import org.schema.game.common.data.world.DeserializationException;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.resource.FileExt;

public class SegmentDataIONew implements SegmentDataIOInterface {
   public static final int DIM = 16;
   public static final int DIMxDIM = 256;
   public static final int DIMxDIMxDIM = 4096;
   public static final byte VERSION = 6;
   private final Long2ObjectOpenHashMap fileNameCacheRead = new Long2ObjectOpenHashMap();
   private final Long2ObjectOpenHashMap fileNameCacheWrite = new Long2ObjectOpenHashMap();
   private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
   private final IOFileManager manager;
   private final UniqueIdentifierInterface segmentController;
   private final boolean onServer;
   private final String segmentDataPath;

   public SegmentDataIONew(UniqueIdentifierInterface var1, boolean var2) {
      this.segmentController = var1;
      this.onServer = var2;
      this.manager = new IOFileManager(var1.getUniqueIdentifier(), var1.isOnServer());
      if (var2) {
         this.segmentDataPath = GameServerState.SEGMENT_DATA_DATABASE_PATH;
      } else {
         this.segmentDataPath = ClientStatics.SEGMENT_DATA_DATABASE_PATH;
      }
   }

   public void releaseFileHandles() throws IOException {
      long var1 = System.currentTimeMillis();
      this.manager.closeAll();
      long var3;
      if ((var3 = System.currentTimeMillis() - var1) > 10L) {
         System.err.println("[SEGMENT-IO] WARNING: File Handle Release for " + this.segmentController + " on server(" + this.segmentController.isOnServer() + ") took " + var3 + " ms");
      }

   }

   public static int requestStatic(int var0, int var1, int var2, String var3, ByteBuffer var4, String var5, RemoteSegment var6) throws IOException, DeserializationException {
      var3 = SegmentDataFileUtils.getSegFile(var0, var1, var2, var3, (String)null, (Long2ObjectOpenHashMap)null, var5);
      FileExt var15;
      if (!(var15 = new FileExt(var3)).exists()) {
         System.err.println("NO FILE: " + var15.getAbsolutePath());
         return 2;
      } else {
         SegmentRegionFileNew var7 = null;

         try {
            var7 = (SegmentRegionFileNew)IOFileManager.getFromFile(var15);

            assert var7 != null;

            if (!var7.getHeader().isEmpty(var0, var1, var2)) {
               if (var7.getHeader().isNoData(var0, var1, var2)) {
                  return 2;
               }

               int var16 = var7.getHeader().getSize(var0, var1, var2);
               short var8;
               long var9 = SegmentHeader.getAbsoluteFilePos(var8 = SegmentHeader.convertToDataOffset(var7.getHeader().getOffset(var0, var1, var2)));
               var4.rewind();
               var4.limit(var16);
               var7.getFile().getChannel().read(var4, var9);
               var4.rewind();
               BufferedInputStream var13 = new BufferedInputStream(new ByteBufferInputStream(var4));
               DataInputStream var14 = new DataInputStream(var13);
               System.err.println("::: READING: " + var0 + ", " + var1 + ", " + var2 + "; off: " + var8 + " -> " + var9 + "; size " + var16 + "; " + var3);
               var6.pos.set(var0, var1, var2);
               var6.deserialize(var14, var16, false, false, 0L);
               var6.timestampTmp = var6.lastChangedDeserialized;
               return 0;
            }

            var6.timestampTmp = -1L;
         } finally {
            var7.close();
         }

         return 1;
      }
   }

   public int request(int var1, int var2, int var3, RemoteSegment var4) throws IOException, DeserializationException {
      this.rwl.readLock().lock();

      try {
         SegmentRegionFileNew var24;
         label505: {
            label506: {
               String var5 = SegmentDataFileUtils.getSegFile(var1, var2, var3, this.segmentController.getReadUniqueIdentifier(), this.segmentController.getObfuscationString(), this.fileNameCacheRead, this.getSegmentDataPath(true));
               FileExt var6 = new FileExt(var5);
               if (!this.manager.existsFile(var5) && !var6.exists()) {
                  return 2;
               }

               var24 = (SegmentRegionFileNew)this.manager.getFileAndLockIt(var5, true);
               ByteBuffer var7 = null;
               ByteBufferInputStream var8 = null;
               DataInputStream var9 = null;
               boolean var19 = false;

               try {
                  var19 = true;

                  assert var24 != null;

                  if (var24.getHeader().isEmpty(var1, var2, var3)) {
                     var4.timestampTmp = -1L;
                     var19 = false;
                     break label505;
                  }

                  if (var24.getHeader().isNoData(var1, var2, var3)) {
                     var19 = false;
                     break label506;
                  }

                  int var10 = var24.getHeader().getSize(var1, var2, var3);
                  long var11 = SegmentHeader.getAbsoluteFilePos(SegmentHeader.convertToDataOffset(var24.getHeader().getOffset(var1, var2, var3)));
                  (var7 = this.segmentController.getDataByteBuffer()).rewind();
                  var7.limit(var10);
                  var24.getFile().getChannel().read(var7, var11);
                  var7.rewind();
                  var8 = new ByteBufferInputStream(var7);
                  var9 = new DataInputStream(var8);

                  boolean var23;
                  try {
                     var23 = var4.deserialize(var9, var10, false, true, this.segmentController.getUpdateTime());
                  } catch (IOException var20) {
                     System.err.println("Exception: IOException " + var20 + " happened on " + this.segmentController + " -> file: " + var5);
                     throw var20;
                  }

                  if (var23) {
                     var4.timestampTmp = System.currentTimeMillis();
                     var19 = false;
                  } else {
                     var4.timestampTmp = var4.lastChangedDeserialized;
                     var19 = false;
                  }
               } finally {
                  if (var19) {
                     if (var7 != null) {
                        this.segmentController.releaseDataByteBuffer(var7);
                     }

                     if (var8 != null) {
                        var8.close();
                     }

                     if (var9 != null) {
                        var9.close();
                     }

                     assert var24 != null;

                     assert var24.rwl != null;

                     assert var24.rwl.readLock() != null;

                     var24.rwl.readLock().unlock();
                  }
               }

               if (var7 != null) {
                  this.segmentController.releaseDataByteBuffer(var7);
               }

               var8.close();
               var9.close();

               assert var24 != null;

               assert var24.rwl != null;

               assert var24.rwl.readLock() != null;

               var24.rwl.readLock().unlock();
               return 0;
            }

            assert var24 != null;

            assert var24.rwl != null;

            assert var24.rwl.readLock() != null;

            var24.rwl.readLock().unlock();
            return 2;
         }

         assert var24 != null;

         assert var24.rwl != null;

         assert var24.rwl.readLock() != null;

         var24.rwl.readLock().unlock();
      } finally {
         this.rwl.readLock().unlock();
      }

      return 1;
   }

   public IOFileManager getManager() {
      return this.manager;
   }

   public static SegmentRegionFileNew write(RemoteSegment var0, long var1, File var3, SegmentRegionFileNew var4) throws IOException {
      SegmentRegionFileNew var9;
      if (var4 != null) {
         var9 = var4;
      } else {
         var9 = (SegmentRegionFileNew)IOFileManager.getFromFile(var3);
      }

      if (var0.isEmpty()) {
         onEmptySegment(var0.pos.x, var0.pos.y, var0.pos.z, var1, var9);
         return null;
      } else {
         SegmentOutputStream var7 = var9.segmentOutputStream;
         DataOutputStream var2 = var9.dro;
         short var10;
         if ((var10 = var9.getHeader().getOffset(var0.pos.x, var0.pos.y, var0.pos.z)) == 0) {
            var10 = var9.getHeader().createNewDataOffsetOnFileEnd(var9);
         } else {
            var10 = SegmentHeader.convertToDataOffset(var10);
         }

         long var5 = SegmentHeader.getAbsoluteFilePos(var10);
         int var8 = var0.serialize(var2, true, System.currentTimeMillis(), (byte)6);
         var9.writeSegment(var5, var7);

         assert var8 < 49152 : var8 + "/49152";

         if (var8 >= 49152) {
            throw new IOException("Critical error. segment size exceeded file-sector-size: " + var8 + "; 16388");
         } else {
            var9.getHeader().updateAndWrite(var0.pos.x, var0.pos.y, var0.pos.z, var10, var8, var9);
            return var9;
         }
      }
   }

   public boolean write(RemoteSegment var1, long var2, boolean var4, boolean var5) throws IOException {
      if (!this.canWrite(var1, var5)) {
         return false;
      } else {
         try {
            this.rwl.writeLock().lock();
            String var35 = SegmentDataFileUtils.getSegFile(var1.pos.x, var1.pos.y, var1.pos.z, this.segmentController.getWriteUniqueIdentifier(), this.segmentController.getObfuscationString(), this.fileNameCacheWrite, this.getSegmentDataPath(false));
            new FileExt(var35);
            System.err.println("[SEGMENTIO] " + this.segmentController + " WRITING SEGMENT " + var1.pos + " TO " + var35 + "; UID: " + this.segmentController.getWriteUniqueIdentifier());
            SegmentRegionFileNew var37 = null;
            SegmentData var6 = var1.getSegmentData();

            try {
               var37 = (SegmentRegionFileNew)this.manager.getFileAndLockIt(var35, false);
               if (var6 != null) {
                  var6.rwl.readLock().lock();
               }

               long var9 = System.currentTimeMillis();
               if (var2 <= 0L) {
                  var1.setLastChanged(var9);
               }

               long var11;
               if ((var11 = var37.getHeader().getTimeStamp(var1.pos.x, var1.pos.y, var1.pos.z, var37)) <= var9 && var11 >= var2) {
                  System.err.println("[IO] OLD: TS: " + var11 + "; " + var2 + "; " + System.currentTimeMillis());
                  return false;
               }

               if (var11 > var9) {
                  System.err.println("[" + (this.segmentController.isOnServer() ? "[SERVER]" : "[CLIENT]") + "[IO] WARNING: segment last changed in the future: " + this.segmentController + ": " + var1.pos + "; DB TS: " + new Date(var11) + "; LAST CHANGED: " + new Date(var2));
               }

               if (!var1.isEmpty()) {
                  SegmentOutputStream var36 = var37.segmentOutputStream;
                  DataOutputStream var7 = var37.dro;
                  short var8;
                  if ((var8 = var37.getHeader().getOffset(var1.pos.x, var1.pos.y, var1.pos.z)) == 0) {
                     var8 = var37.getHeader().createNewDataOffsetOnFileEnd(var37);
                  } else {
                     var8 = SegmentHeader.convertToDataOffset(var8);
                  }

                  int var33 = var1.serialize(var7, true, var2, (byte)6);
                  long var13 = SegmentHeader.getAbsoluteFilePos(var8);
                  var37.writeSegment(var13, var36);

                  assert var33 < 49152 : var33 + "/49152";

                  if (var33 >= 49152) {
                     throw new IOException("Critical error. segment size exceeded file-sector-size: " + var33 + "; 16388");
                  }

                  System.currentTimeMillis();
                  System.currentTimeMillis();
                  var37.getHeader().updateAndWrite(var1.pos.x, var1.pos.y, var1.pos.z, var8, var33, var37);
                  if (this.segmentController.isOnServer() && ServerConfig.FORCE_DISK_WRITE_COMPLETION.isOn()) {
                     var37.dro.flush();
                  }

                  if (this.segmentController.isOnServer() && ServerConfig.DEBUG_SEGMENT_WRITING.isOn()) {
                     var37.dro.flush();
                     RemoteSegment var34;
                     (var34 = new RemoteSegment((SegmentController)this.segmentController)).pos.set(var1.pos);

                     try {
                        this.request(var34.pos.x, var34.pos.y, var34.pos.z, var34);
                        if (!Arrays.equals(var1.getSegmentData().getAsIntBuffer(), var34.getSegmentData().getAsIntBuffer())) {
                           try {
                              throw new IOException("Exception !!!!!!!!!! ERROR in segment data writing " + var34.pos + " on " + this.segmentController);
                           } catch (Exception var27) {
                              var27.printStackTrace();
                           }
                        } else {
                           System.err.println("SEGMENT READ IS OK!");
                        }
                     } catch (DeserializationException var28) {
                        var28.printStackTrace();
                     } catch (SegmentDataWriteException var29) {
                        var29.printStackTrace();
                     }
                  }

                  return true;
               }

               if (ServerConfig.DEBUG_EMPTY_CHUNK_WRITES.isOn()) {
                  System.err.println("[SERVER][DEBUG] WRITE EMPTY: " + var1.getSegmentController() + ": " + var1.pos + "; " + var2);
               }

               System.err.println("[IO] EMPTY");
               onEmptySegment(var1.pos.x, var1.pos.y, var1.pos.z, var2, var37);
            } finally {
               if (var6 != null) {
                  var6.rwl.readLock().unlock();
               }

               var37.rwl.writeLock().unlock();
            }
         } catch (IOException var31) {
            System.err.println("Exception HAPPENED ON SEGMENT " + var1 + "; controller: " + var1.getSegmentController() + "; size: " + var1.getSize() + "; data " + var1.getSegmentData());
            var31.printStackTrace();
            if (this.segmentController.isOnServer()) {
               ((GameServerState)((SegmentController)this.segmentController).getState()).getController().broadcastMessage(new Object[]{78}, 3);
            }

            throw var31;
         } finally {
            this.rwl.writeLock().unlock();
         }

         return false;
      }
   }

   public final String getSegmentDataPath(boolean var1) {
      return var1 && this.isOnServer() && this.segmentController.isLoadByBlueprint() ? this.segmentController.getBlueprintSegmentDataPath() : this.segmentDataPath;
   }

   private static void onEmptySegment(int var0, int var1, int var2, long var3, SegmentRegionFileNew var5) throws IOException {
      if (!var5.getHeader().isEmpty(var0, var1, var2)) {
         RandomFileOutputStream var6 = new RandomFileOutputStream(var5.getFile(), false);
         var5.getHeader().writeEmptyDirectly(var0, var1, var2, var6);
      }

   }

   public int getSize(int var1, int var2, int var3) throws IOException {
      String var4;
      if (!GameClientController.exists(var4 = SegmentDataFileUtils.getSegFile(var1, var2, var3, this.segmentController.getReadUniqueIdentifier(), this.segmentController.getObfuscationString(), this.fileNameCacheRead, this.getSegmentDataPath(true)))) {
         return -1;
      } else {
         SegmentRegionFileNew var5 = null;

         try {
            var1 = (var5 = (SegmentRegionFileNew)this.manager.getFileAndLockIt(var4, true)).getHeader().getSize(var1, var2, var3);
            return var1;
         } catch (Exception var8) {
            var8.printStackTrace();
            this.manager.removeFile(var4);
         } finally {
            if (var5 != null) {
               var5.rwl.readLock().unlock();
            }

         }

         return -1;
      }
   }

   public long getTimeStamp(int var1, int var2, int var3) throws IOException {
      String var4;
      if (!GameClientController.exists(var4 = SegmentDataFileUtils.getSegFile(var1, var2, var3, this.segmentController.getReadUniqueIdentifier(), this.segmentController.getObfuscationString(), this.fileNameCacheRead, this.getSegmentDataPath(true)))) {
         return -1L;
      } else {
         SegmentRegionFileNew var5 = null;

         try {
            long var6 = (var5 = (SegmentRegionFileNew)this.manager.getFileAndLockIt(var4, true)).getHeader().getTimeStamp(var1, var2, var3, var5);
            return var6;
         } catch (Exception var10) {
            var10.printStackTrace();
            this.manager.removeFile(var4);
         } finally {
            if (var5 != null) {
               var5.rwl.readLock().unlock();
            }

         }

         return -1L;
      }
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public boolean canWrite(RemoteSegment var1, boolean var2) {
      if (var1.getSegmentController().isOnServer() && var1.getSegmentController() != null && !(var1.getSegmentController() instanceof TransientSegmentController)) {
         Universe var3;
         synchronized((var3 = ((GameServerState)var1.getSegmentController().getState()).getUniverse()).writeMap) {
            try {
               long var5 = System.currentTimeMillis();
               long var7 = var1.getSegmentController().getLastWrite();

               assert var1.getSegmentController().getWriteUniqueIdentifier() != null;

               long var9;
               if ((var9 = var3.writeMap.getLong(var1.getSegmentController().getWriteUniqueIdentifier())) > 0L && var9 != var7) {
                  throw new SerializationException(var1.getSegmentController() + "sec[" + var1.getSegmentController().getSectorId() + "]; Tried to write old version over new (rollbackbug) entity-UID LAST WRITTEN " + var9 + "; entity-instance LAST WRITTEN " + var7 + ";  UID: " + var1.getSegmentController().getUniqueIdentifier());
               }

               var3.writeMap.put(var1.getSegmentController().getWriteUniqueIdentifier(), var5);
               var1.getSegmentController().setLastWrite(var5);

               assert var1.getSegmentController().getLastWrite() == var5;
            } catch (SerializationException var11) {
               var11.printStackTrace();
               ((GameServerState)var1.getSegmentController().getState()).getController().broadcastMessageAdmin(new Object[]{79, var1.getSegmentController(), ((GameServerState)var1.getSegmentController().getState()).getUniverse().getSector(var1.getSegmentController().getSectorId())}, 3);
               return false;
            }
         }
      }

      if (var1.getSegmentController() instanceof TransientSegmentController && !((TransientSegmentController)var1.getSegmentController()).isTouched()) {
         return false;
      } else if (var1.getSegmentController().isOnServer() && (var1.isDeserializing() || var1.isRevalidating())) {
         return false;
      } else if (this.segmentController.isLoadByBlueprint()) {
         if (var2) {
            System.err.println("[SEGMENTIO] " + this.segmentController + " NOT WRITING AS IT'S LOADED BY BLUEPRINT UID: " + this.segmentController.getWriteUniqueIdentifier());
         }

         return false;
      } else {
         return true;
      }
   }

   public EWAHCompressedBitmap requestSignature(int var1, int var2, int var3) throws IOException, DeserializationException {
      this.rwl.readLock().lock();

      EWAHCompressedBitmap var12;
      try {
         String var10 = SegmentDataFileUtils.getSegFile(var1, var2, var3, this.segmentController.getReadUniqueIdentifier(), this.segmentController.getObfuscationString(), this.fileNameCacheRead, this.getSegmentDataPath(true));
         FileExt var11 = new FileExt(var10);
         if (!this.manager.existsFile(var10) && !var11.exists()) {
            return null;
         }

         EWAHCompressedBitmap var13 = new EWAHCompressedBitmap(64);
         SegmentRegionFileNew var14 = null;

         try {
            var14 = (SegmentRegionFileNew)this.manager.getFileAndLockIt(var10, true);

            for(var1 = 0; var1 < 4096; ++var1) {
               if (var14.getHeader().isEmptyOrNoData(var1)) {
                  var13.set(var1);
               }

               ++var1;
            }
         } finally {
            var14.rwl.readLock().unlock();
         }

         var12 = var13;
      } finally {
         this.rwl.readLock().unlock();
      }

      return var12;
   }

   public void writeEmpty(int var1, int var2, int var3, SegmentController var4, long var5, boolean var7) throws IOException {
      if (!(var4 instanceof TransientSegmentController) || ((TransientSegmentController)var4).isTouched()) {
         if (!var4.isLoadByBlueprint()) {
            try {
               this.rwl.writeLock().lock();
               String var23 = SegmentDataFileUtils.getSegFile(var1, var2, var3, this.segmentController.getWriteUniqueIdentifier(), this.segmentController.getObfuscationString(), this.fileNameCacheWrite, this.getSegmentDataPath(false));
               SegmentRegionFileNew var8 = null;
               if (this.manager.existsFile(var23) || (new FileExt(var23)).exists()) {
                  try {
                     var8 = (SegmentRegionFileNew)this.manager.getFileAndLockIt(var23, false);
                     onEmptySegment(var1, var2, var3, var5, var8);
                     return;
                  } finally {
                     try {
                        if (var8 != null && var8.rwl != null && var8.rwl.writeLock() != null) {
                           var8.rwl.writeLock().unlock();
                        } else {
                           System.err.println("Exception LOCK NULL: " + var8);
                           if (var8 != null) {
                              System.err.println("Exception LOCK RWL NULL: " + var8.rwl);
                              if (var8.rwl != null) {
                                 System.err.println("Exception LOCK RWL NULL: " + var8.rwl.writeLock());
                              }
                           }
                        }
                     } catch (Exception var19) {
                        var19.printStackTrace();
                     }

                  }
               }
            } catch (IOException var21) {
               System.err.println("Exception HAPPENED ON EMPTY SEGMENT " + var1 + ", " + var2 + ", " + var3 + "; controller: " + var4 + ";");
               var21.printStackTrace();
               if (this.segmentController.isOnServer()) {
                  ((GameServerState)((SegmentController)this.segmentController).getState()).getController().broadcastMessage(new Object[]{80}, 3);
               }

               throw var21;
            } finally {
               this.rwl.writeLock().unlock();
            }

         }
      }
   }

   public static int checkVersionSkip(int var0, SegmentData.SegmentDataType var1) {
      return var0 == 5 && var1 == SegmentData.SegmentDataType.INT_ARRAY ? 6 : var0;
   }
}
