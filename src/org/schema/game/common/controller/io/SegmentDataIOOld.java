package org.schema.game.common.controller.io;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.ClientStatics;
import org.schema.game.common.controller.SegmentBufferIteratorEmptyInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.DeserializationException;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.Segment;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.resource.FileExt;

public class SegmentDataIOOld {
   public static final int READ_DATA = 0;
   public static final int READ_EMPTY = 1;
   public static final int READ_NO_DATA = 2;
   private static final Vector3i maxSegementsPerFile;
   static final int size;
   static final int headerTotalSize;
   static final int timestampTotalSize;
   private static final int sectorInKb = 5120;
   private static byte[] emptyHeader;
   private static byte[] minusOne;
   private final String segmentDataPath;
   int[] headerData = new int[2];
   private SegmentController segmentController;
   private byte[] headerArray = new byte[8];
   private byte[] timestampArray = new byte[8];
   private boolean onServer;
   private long timeStampSeekDebug;

   public SegmentDataIOOld(SegmentController var1, boolean var2) {
      this.segmentController = var1;
      this.onServer = var2;
      if (var2) {
         this.segmentDataPath = GameServerState.SEGMENT_DATA_DATABASE_PATH;
      } else {
         this.segmentDataPath = ClientStatics.SEGMENT_DATA_DATABASE_PATH;
      }
   }

   public static int getOffsetSeekIndex(int var0, int var1, int var2) {
      int var3 = Math.abs(var0) >> 4 & 15;
      int var4 = Math.abs(var1) >> 4 & 15;
      int var5;
      int var6 = (var5 = Math.abs(var2) >> 4 & 15) * maxSegementsPerFile.y * maxSegementsPerFile.x + var4 * maxSegementsPerFile.x + var3;

      assert var6 < size : var6 + "/" + size + ": " + var0 + ", " + var1 + ", " + var2 + " ---> " + var3 + ", " + var4 + ", " + var5 + " ";

      return var6;
   }

   public static void main(String[] var0) {
   }

   private void createSegmentControllerData(int var1, int var2, int var3, File var4) throws IOException {
      long var5 = System.currentTimeMillis();
      FileOutputStream var7 = new FileOutputStream(var4);
      BufferedOutputStream var8 = new BufferedOutputStream(var7);
      DataOutputStream var9 = new DataOutputStream(var8);
      this.writeEmptyHeader(var9);
      var9.close();
      var7.close();
      var8.close();
      var7.close();
      System.err.println("Wrote Empty Header " + emptyHeader.length + ": " + var4.getName() + " - " + this.segmentController.getState() + "; " + (System.currentTimeMillis() - var5) + "ms");
   }

   void getHeader(int var1, int var2, int var3, int[] var4) throws IOException {
      String var5 = this.getSegFile(var1, var2, var3, this.segmentController);
      RandomAccessFile var8;
      synchronized(var8 = new RandomAccessFile(var5, "r")) {
         this.getHeader(var1, var2, var3, var4, var8);
      }
   }

   private void getHeader(int var1, int var2, int var3, int[] var4, RandomAccessFile var5) throws IOException {
      var1 = getOffsetSeekIndex(var1, var2, var3);
      this.getHeader(var1, var4, var5);
   }

   private void getHeader(int var1, int[] var2, RandomAccessFile var3) throws IOException {
      var3.seek((long)(var1 * this.headerArray.length));
      var2[0] = var3.readInt();
      var2[1] = var3.readInt();
   }

   String getSegFile(int var1, int var2, int var3, SegmentController var4) {
      var1 = ByteUtil.divSeg(var1) / maxSegementsPerFile.x - (var1 < 0 ? 1 : 0);
      var2 = ByteUtil.divSeg(var2) / maxSegementsPerFile.y - (var2 < 0 ? 1 : 0);
      var3 = ByteUtil.divSeg(var3) / maxSegementsPerFile.z - (var3 < 0 ? 1 : 0);
      return this.getSegmentDataPath() + var4.getUniqueIdentifier() + "." + var1 + "." + var2 + "." + var3 + ".smd";
   }

   public final String getSegmentDataPath() {
      return this.segmentDataPath;
   }

   public long getTimeStamp(int var1, int var2, int var3) throws IOException {
      synchronized(this) {
         String var5 = this.getSegFile(var1, var2, var3, this.segmentController);
         if (!(new FileExt(var5)).exists()) {
            return -1L;
         } else {
            RandomAccessFile var17;
            synchronized(var17 = new RandomAccessFile(var5, "r")) {
               var1 = getOffsetSeekIndex(var1, var2, var3);
               var17.seek((long)(headerTotalSize + var1 * this.timestampArray.length));

               try {
                  long var7 = var17.readLong();
                  return var7;
               } catch (Exception var13) {
                  var13.printStackTrace();
               } finally {
                  var17.close();
               }
            }

            return -1L;
         }
      }
   }

   public long getTimeStampSeekDebug() {
      return this.timeStampSeekDebug;
   }

   public void setTimeStampSeekDebug(long var1) {
      this.timeStampSeekDebug = var1;
   }

   private void onEmptySegment(RemoteSegment var1) throws IOException {
      FileExt var2 = new FileExt(this.getSegFile(var1.pos.x, var1.pos.y, var1.pos.z, this.segmentController));
      RandomFileOutputStream var5 = new RandomFileOutputStream(var2, false);
      DataOutputStream var3 = new DataOutputStream(var5);
      int var4 = getOffsetSeekIndex(var1.pos.x, var1.pos.y, var1.pos.z);
      var5.setFilePointer((long)(var4 * this.headerArray.length));
      var3.writeInt(-1);
      var3.writeInt(-1);
      var5.setFilePointer((long)(headerTotalSize + var4 * this.timestampArray.length));
      var3.writeLong(var1.getLastChanged());
      var3.flush();
      var5.flush();
      var5.close();
      var3.close();
   }

   public void purge() {
      synchronized(this) {
         this.segmentController.getSegmentBuffer().iterateOverEveryElement(new SegmentBufferIteratorEmptyInterface() {
            public boolean handle(Segment var1, long var2) {
               FileExt var4;
               if ((var4 = new FileExt(SegmentDataIOOld.this.getSegFile(var1.pos.x, var1.pos.y, var1.pos.z, SegmentDataIOOld.this.segmentController))).exists()) {
                  var4.delete();
               }

               return true;
            }

            public boolean handleEmpty(int var1, int var2, int var3, long var4) {
               FileExt var6;
               if ((var6 = new FileExt(SegmentDataIOOld.this.getSegFile(var1, var2, var3, SegmentDataIOOld.this.segmentController))).exists()) {
                  var6.delete();
               }

               return true;
            }
         }, true);
      }
   }

   public void purge(RemoteSegment var1) throws IOException {
      FileExt var2 = new FileExt(this.getSegFile(var1.pos.x, var1.pos.y, var1.pos.z, this.segmentController));
      RandomFileOutputStream var5 = new RandomFileOutputStream(var2, false);
      DataOutputStream var3 = new DataOutputStream(var5);
      int var4 = getOffsetSeekIndex(var1.pos.x, var1.pos.y, var1.pos.z);
      var5.setFilePointer((long)(headerTotalSize + var4 * this.timestampArray.length));
      var3.writeLong(0L);
      var3.flush();
      var5.flush();
      var5.close();
      var3.close();
   }

   public int request(int var1, int var2, int var3, RemoteSegment var4) throws IOException, DeserializationException {
      synchronized(this) {
         String var6 = this.getSegFile(var1, var2, var3, this.segmentController);
         FileExt var7;
         if (!(var7 = new FileExt(var6)).exists()) {
            this.createSegmentControllerData(var1, var2, var3, var7);
            return 2;
         } else {
            byte var10000;
            RandomAccessFile var22;
            synchronized(var22 = new RandomAccessFile(var6, "r")) {
               int var9 = getOffsetSeekIndex(var1, var2, var3);
               this.getHeader(var9, this.headerData, var22);
               int var10 = this.headerData[0];
               int var11 = this.headerData[1];
               if (var10 >= 0) {
                  long var12 = (long)(headerTotalSize + var9 * this.timestampArray.length);

                  assert var12 < var22.length() : " " + var12 + " / " + var22.length() + " on  (" + var1 + ", " + var2 + ", " + var3 + ") on " + var7.getName() + " " + var4.getSegmentController() + " offest(" + var10 + "); offsetIndex(" + var9 + ")";

                  var22.seek(var12);
                  this.setTimeStampSeekDebug(var12);
                  var22.readLong();

                  assert var11 > 0 && var11 < 5120 : " len: " + var11 + " / 5120 ON " + var7.getName() + " (" + var1 + ", " + var2 + ", " + var3 + ")";

                  long var14 = (long)(headerTotalSize + timestampTotalSize + var10 * 5120);
                  var22.seek(var14);
                  synchronized(this.segmentController.getState().getDataBuffer()) {
                     var22.readFully(this.segmentController.getState().getDataBuffer(), 0, var11);
                     ByteArrayInputStream var20 = new ByteArrayInputStream(this.segmentController.getState().getDataBuffer(), 0, var11);
                     DataInputStream var21 = new DataInputStream(var20);
                     var4.deserialize(var21, var11, false, true, this.segmentController.getState().getUpdateTime());
                     var4.setLastChanged(System.currentTimeMillis());
                     var20.close();
                     var21.close();
                  }

                  var10000 = 0;
                  return var10000;
               }

               if (var11 >= 0) {
                  return 2;
               }

               try {
                  var22.seek((long)(headerTotalSize + var9 * this.timestampArray.length));
                  var22.readLong();
                  var4.setLastChanged(System.currentTimeMillis());
               } catch (IOException var16) {
                  System.err.println("COULD NOT READ TIMESTAMP FOR " + var4 + " ... " + var16.getMessage());
                  var4.setLastChanged(System.currentTimeMillis());
               }

               var10000 = 1;
            }

            return var10000;
         }
      }
   }

   public void write(RemoteSegment var1) throws IOException {
      synchronized(this) {
         FileExt var3;
         if (!(var3 = new FileExt(this.getSegFile(var1.pos.x, var1.pos.y, var1.pos.z, this.segmentController))).exists()) {
            this.createSegmentControllerData(var1.pos.x, var1.pos.y, var1.pos.z, var3);
         }

         if (var1.isEmpty()) {
            this.onEmptySegment(var1);
         } else {
            System.currentTimeMillis();
            long var4 = this.getTimeStamp(var1.pos.x, var1.pos.y, var1.pos.z);
            if (var1.getLastChanged() <= 0L) {
               var1.setLastChanged(System.currentTimeMillis());
            }

            if (var4 >= var1.getLastChanged()) {
               boolean var10000 = this.onServer;
            } else {
               System.currentTimeMillis();
               System.currentTimeMillis();
               this.getHeader(var1.pos.x, var1.pos.y, var1.pos.z, this.headerData);
               System.currentTimeMillis();
               System.currentTimeMillis();
               RandomFileOutputStream var11 = new RandomFileOutputStream(var3, false);
               DataOutputStream var12 = new DataOutputStream(var11);
               int var5;
               if ((var5 = this.headerData[0]) < 0) {
                  long var8 = var11.getFileSize();
                  var5 = Math.max(0, (int)((var8 - (long)headerTotalSize - (long)timestampTotalSize) / 5120L));
                  var11.setFileSize(var11.getFileSize() + 5120L);
                  var11.setFilePointer(var8);
               } else {
                  var11.setFilePointer((long)(headerTotalSize + timestampTotalSize + var5 * 5120));
               }

               var11.getFilePointer();
               int var6 = var1.serialize(var12, true, System.currentTimeMillis(), (byte)2);

               assert var6 < 5120 : var6 + "/5120";

               System.currentTimeMillis();
               var11.getFileSize();
               System.currentTimeMillis();
               int var7 = getOffsetSeekIndex(var1.pos.x, var1.pos.y, var1.pos.z);
               System.currentTimeMillis();
               System.currentTimeMillis();
               var11.setFilePointer((long)(var7 * this.headerArray.length));

               assert var6 < 5120;

               var12.writeInt(var5);
               var12.writeInt(var6);
               var11.setFilePointer((long)(headerTotalSize + var7 * this.timestampArray.length));
               var12.writeLong(var1.getLastChanged());
               System.currentTimeMillis();
               System.currentTimeMillis();
               System.currentTimeMillis();
               System.currentTimeMillis();
               var11.flush();
               var12.flush();
               var12.close();
               var11.close();
               System.currentTimeMillis();
            }
         }
      }
   }

   private void writeEmptyHeader(DataOutputStream var1) throws IOException {
      var1.write(emptyHeader);
      var1.flush();
   }

   static {
      headerTotalSize = (size = (maxSegementsPerFile = new Vector3i(16, 16, 16)).x * maxSegementsPerFile.y * maxSegementsPerFile.z) << 3;
      timestampTotalSize = size << 3;
      emptyHeader = new byte[headerTotalSize + timestampTotalSize];
      minusOne = ByteUtil.intToByteArray(-1);
      int var0 = 0;

      for(int var1 = 0; var1 < maxSegementsPerFile.z; ++var1) {
         for(int var2 = 0; var2 < maxSegementsPerFile.y; ++var2) {
            for(int var3 = 0; var3 < maxSegementsPerFile.x; ++var3) {
               for(int var4 = 0; var4 < minusOne.length; ++var4) {
                  emptyHeader[var0++] = minusOne[var4];
               }

               var0 += 4;
            }
         }
      }

      assert var0 == emptyHeader.length / 2 : var0 + "/" + emptyHeader.length / 2;

   }
}
