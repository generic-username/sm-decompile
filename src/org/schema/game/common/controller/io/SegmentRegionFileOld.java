package org.schema.game.common.controller.io;

import com.googlecode.javaewah.EWAHCompressedBitmap;
import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class SegmentRegionFileOld extends SegmentRegionFileHandle {
   private static final int DATA_ROW = 16;
   private final int[] offsets;
   private final int[] sizes;
   private final long[] timestamps;
   private int version;

   public SegmentRegionFileOld(String var1, boolean var2, File var3, String var4, IOFileManager var5) throws IOException {
      super(12288, var1, var2, var3, var4, var5);
      this.offsets = new int[SegmentDataIO16.size];
      this.sizes = new int[SegmentDataIO16.size];
      this.timestamps = new long[SegmentDataIO16.size];
      this.createHeader();
   }

   public static byte[] createTimestampHeader(long var0) throws IOException {
      byte[] var2 = new byte[SegmentDataIO16.size << 3];
      DataOutputStream var3 = new DataOutputStream(new FastByteArrayOutputStream(var2));

      for(int var4 = 0; var4 < SegmentDataIO16.size; ++var4) {
         var3.writeLong(var0);
      }

      var3.close();
      return var2;
   }

   public static void writeLastChanged(File var0, byte[] var1) throws IOException {
      RandomAccessFile var2 = new RandomAccessFile(var0, "rw");

      assert var1.length == SegmentDataIO16.size << 3;

      var2.seek((long)(4 + (SegmentDataIO16.size << 2) + (SegmentDataIO16.size << 2)));
      var2.write(var1);
      var2.close();
   }

   public int getVersion() {
      return this.version;
   }

   public void setVersion(int var1) {
      this.version = var1;
   }

   protected void createHeader() throws IOException {
      long var1 = System.currentTimeMillis();
      boolean var3 = this.f.exists();
      long var4 = 0L;
      if (var3) {
         this.file = new RandomAccessFile(this.f, "rw");
         var4 = this.f.length();
      }

      long var6 = System.currentTimeMillis() - var1;
      if (var4 >= (long)SegmentDataIO16.headerTotalSize) {
         byte[] var8 = new byte[(SegmentDataIO16.size << 4) + 4];
         this.file.read(var8, 0, var8.length);
         DataInputStream var9 = new DataInputStream(new FastByteArrayInputStream(var8));
         this.version = var9.readInt();

         int var11;
         for(var11 = 0; var11 < SegmentDataIO16.size; ++var11) {
            this.offsets[var11] = var9.readInt();
            this.sizes[var11] = var9.readInt();
         }

         for(var11 = 0; var11 < SegmentDataIO16.size; ++var11) {
            this.timestamps[var11] = var9.readLong();
         }

         var9.close();
      } else {
         if (var3) {
            try {
               throw new NoHeaderException("No Header in file " + this.getDesc() + " -> " + this.f.getName() + "; read: " + var4 + "; is now: " + this.file.length() + " / " + this.f.length() + " (must be min: " + SegmentDataIO16.headerTotalSize + "); REWRITING HEADER; fExisted: " + this.f.exists());
            } catch (Exception var10) {
               var10.printStackTrace();
            }
         }

         SegmentDataIO16.writeEmptyHeader(this.f);
         if (!var3) {
            this.file = new RandomAccessFile(this.f, "rw");
         }

         DataInputStream var12 = new DataInputStream(new FastByteArrayInputStream(SegmentDataIO16.emptyHeader));
         this.version = var12.readInt();

         int var14;
         for(var14 = 0; var14 < SegmentDataIO16.size; ++var14) {
            this.offsets[var14] = var12.readInt();
            this.sizes[var14] = var12.readInt();
         }

         for(var14 = 0; var14 < SegmentDataIO16.size; ++var14) {
            this.timestamps[var14] = var12.readLong();
         }

         var12.close();
      }

      long var13;
      if ((var13 = System.currentTimeMillis() - var1) > 60L) {
         System.err.println("[IO] WARNING: " + this.getDesc() + " Header creation for " + this.getName() + " took " + var13 + "ms; [synch: " + var6 + "ms] existed: " + var3);
      }

   }

   public int getBlockDataOffset(int var1) {
      return this.offsets[var1];
   }

   public int getSize(int var1) {
      return this.sizes[var1];
   }

   public long getTimestamp(int var1) {
      return this.timestamps[var1];
   }

   public void setHeader(int var1, int var2, int var3, long var4) {
      this.offsets[var1] = var2;
      this.sizes[var1] = var3;
      this.timestamps[var1] = var4;
   }

   public void writeHeader() throws IOException {
      System.err.println("[IO] Writing Header of " + this.getName());
      this.file.writeInt(this.version);

      int var1;
      for(var1 = 0; var1 < this.offsets.length; ++var1) {
         this.file.writeInt(this.offsets[var1]);
         this.file.writeInt(this.sizes[var1]);
      }

      for(var1 = 0; var1 < this.timestamps.length; ++var1) {
         this.file.writeLong(this.timestamps[var1]);
      }

   }

   public void writeSegmentWithoutMap(long var1, SegmentOutputStream var3) throws IOException {
      var3.writeToFileWithoutMap(this.file, var1, this.getName(), this.f);
   }

   private EWAHCompressedBitmap calculateSignature(int var1, int var2, int var3) {
      var1 = 0;
      EWAHCompressedBitmap var9 = new EWAHCompressedBitmap(512);

      for(var3 = 0; var3 < 0 + SegmentDataIO16.maxSegementsPerFile.z; ++var3) {
         for(int var4 = 0; var4 < 0 + SegmentDataIO16.maxSegementsPerFile.y; ++var4) {
            for(int var5 = 0; var5 < 0 + SegmentDataIO16.maxSegementsPerFile.x; ++var5) {
               int var6 = var5 << 5;
               int var7 = var4 << 5;
               int var8 = var3 << 5;
               var6 = SegmentDataIO16.getHeaderOffset(var6 - 256, var7 - 256, var8 - 256);
               var7 = this.getBlockDataOffset(var6);
               var6 = this.getSize(var6);
               if (var7 < 0) {
                  if (var6 < 0) {
                     var9.set(var1);
                  }
               } else {
                  assert var6 > 0;
               }

               ++var1;
            }
         }
      }

      return var9;
   }

   public EWAHCompressedBitmap getSignature(int var1, int var2, int var3) {
      return this.calculateSignature(var1, var2, var3);
   }
}
