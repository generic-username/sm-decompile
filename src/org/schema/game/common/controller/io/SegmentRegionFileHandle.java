package org.schema.game.common.controller.io;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.schema.schine.resource.FileExt;

public abstract class SegmentRegionFileHandle {
   public final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
   protected File f;
   protected final String name;
   protected final String UID;
   protected final boolean onServer;
   public final DataOutputStream dro;
   public final SegmentOutputStream segmentOutputStream;
   public boolean needsHeaderWriting;
   private long lastAccess;
   private final IOFileManager ioFileManager;
   private boolean dirty;
   protected RandomAccessFile file;

   public int hashCode() {
      return this.name.hashCode();
   }

   public boolean equals(Object var1) {
      return ((SegmentRegionFileHandle)var1).name.equals(this.name);
   }

   public SegmentRegionFileHandle(int var1, String var2, boolean var3, File var4, String var5, IOFileManager var6) throws IOException {
      this.UID = var2;
      this.onServer = var3;
      this.name = var5;
      this.f = var4;
      this.lastAccess = System.currentTimeMillis();
      this.ioFileManager = var6;
      this.segmentOutputStream = new SegmentOutputStream(var1);
      this.dro = new DataOutputStream(this.segmentOutputStream);
      if (var6 != null) {
         var6.registerExists(var5);
      }

   }

   public RandomAccessFile getFile() {
      return this.file;
   }

   public String getDesc() {
      return this.onServer ? "SERVER[" : "CLIENT[" + this.UID + "]";
   }

   public void close() throws IOException {
      this.file.getChannel().close();
      this.file.close();
   }

   public void reopen() throws IOException {
      if (this.file != null) {
         try {
            this.file.getChannel().close();
            this.file.close();
         } catch (IOException var1) {
            var1.printStackTrace();
         }
      }

      this.file = new RandomAccessFile(new FileExt(this.name), "rw");
   }

   public void updateLastAccess() {
      this.lastAccess = System.currentTimeMillis();
   }

   public IOFileManager getIoFileManager() {
      return this.ioFileManager;
   }

   public long getLastAccessTime() {
      return this.lastAccess;
   }

   public String getName() {
      return this.name;
   }

   public File getNormalFile() {
      return this.f;
   }

   protected abstract void createHeader() throws IOException;

   public void delete() throws IOException {
      this.close();
      this.f.delete();
   }

   public abstract void writeHeader() throws IOException;

   public void writeSegment(long var1, SegmentOutputStream var3) throws IOException {
      var3.writeToFile(this.file, var1, this.getName(), this.f);
      this.dirty = true;
   }

   public void flush(boolean var1) throws IOException {
      if (this.dirty || var1) {
         this.file.getChannel().force(false);
         this.dirty = false;
      }

   }
}
