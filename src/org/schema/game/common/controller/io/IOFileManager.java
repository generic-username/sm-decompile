package org.schema.game.common.controller.io;

import it.unimi.dsi.fastutil.objects.Object2BooleanOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.schema.schine.resource.FileExt;

public class IOFileManager {
   public static final ReentrantReadWriteLock rwlServer = new ReentrantReadWriteLock();
   public static final ReentrantReadWriteLock rwlClient = new ReentrantReadWriteLock();
   private static final int MAX_OPEN_FILES = 500;
   private static final Object2ObjectOpenHashMap staticFileMapServer = new Object2ObjectOpenHashMap();
   private static final Object2ObjectOpenHashMap staticFileMapClient = new Object2ObjectOpenHashMap();
   private static final Object removeLock = new Object();
   private final Object2ObjectOpenHashMap fileMap = new Object2ObjectOpenHashMap();
   private final boolean onServer;
   private final String UID;
   private Object2BooleanOpenHashMap existMap = new Object2BooleanOpenHashMap();

   public IOFileManager(String var1, boolean var2) {
      this.UID = var1;
      this.onServer = var2;
   }

   private static void removeOldestHandle(IOFileManager var0, String var1) throws IOException {
      System.currentTimeMillis();
      SegmentRegionFileHandle var6 = null;
      long var3 = -1L;
      Iterator var2 = var0.getStaticFileMap().values().iterator();

      while(true) {
         SegmentRegionFileHandle var5;
         do {
            if (!var2.hasNext()) {
               var6.rwl.writeLock().lock();
               var6.getName();

               assert var6 != null;

               if (var6.needsHeaderWriting) {
                  var6.writeHeader();
                  var6.needsHeaderWriting = false;
               }

               var6.flush(false);
               var6.close();
               var0.getStaticFileMap().remove(var6.getName());
               var6.getIoFileManager().fileMap.remove(var6.getName());
               var6.rwl.writeLock().unlock();
               return;
            }

            var5 = (SegmentRegionFileHandle)var2.next();
         } while(var3 >= 0L && var5.getLastAccessTime() >= var3);

         var6 = var5;
         var3 = var5.getLastAccessTime();
      }
   }

   public static void writeAllHeaders(IOFileManager var0) throws IOException {
      try {
         var0.getStaticFileLock().readLock().lock();
         System.currentTimeMillis();
         Iterator var1 = var0.getStaticFileMap().values().iterator();

         while(var1.hasNext()) {
            SegmentRegionFileHandle var2;
            if ((var2 = (SegmentRegionFileHandle)var1.next()).needsHeaderWriting) {
               var2.writeHeader();
               var2.needsHeaderWriting = false;
            }
         }
      } finally {
         var0.getStaticFileLock().readLock().unlock();
      }

   }

   public static void writeAllOpenFiles(IOFileManager var0) {
      try {
         var0.getStaticFileLock().readLock().lock();

         SegmentRegionFileHandle var2;
         for(Iterator var1 = var0.getStaticFileMap().values().iterator(); var1.hasNext(); var2.rwl.writeLock().unlock()) {
            (var2 = (SegmentRegionFileHandle)var1.next()).rwl.writeLock().lock();

            try {
               var2.flush(false);
            } catch (IOException var5) {
               var5.printStackTrace();
            }
         }
      } finally {
         var0.getStaticFileLock().readLock().unlock();
      }

   }

   public void closeAll() throws IOException {
      try {
         this.getStaticFileLock().writeLock().lock();
         Iterator var1 = this.fileMap.entrySet().iterator();

         while(var1.hasNext()) {
            Entry var2;
            ((SegmentRegionFileHandle)(var2 = (Entry)var1.next()).getValue()).rwl.writeLock().lock();
            this.getStaticFileMap().remove(var2.getKey());
            ((SegmentRegionFileHandle)var2.getValue()).close();
            ((SegmentRegionFileHandle)var2.getValue()).rwl.writeLock().unlock();
         }

         this.fileMap.clear();
      } finally {
         this.getStaticFileLock().writeLock().unlock();
      }
   }

   public boolean checkEmptyExists(String var1) {
      boolean var4;
      try {
         this.getStaticFileLock().readLock().lock();
         if (!this.existMap.containsKey(var1)) {
            this.existMap.put(var1, (new FileExt(var1)).exists());
         }

         var4 = this.existMap.getBoolean(var1);
      } finally {
         this.getStaticFileLock().readLock().unlock();
      }

      return var4;
   }

   public void registerExists(String var1) {
      try {
         this.getStaticFileLock().readLock().lock();
         this.existMap.put(var1, true);
      } finally {
         this.getStaticFileLock().readLock().unlock();
      }

   }

   public boolean existsFile(String var1) {
      boolean var4;
      try {
         this.getStaticFileLock().readLock().lock();
         var4 = this.fileMap.containsKey(var1);
      } finally {
         this.getStaticFileLock().readLock().unlock();
      }

      return var4;
   }

   public SegmentRegionFileHandle getFileNormal(String var1) throws IOException {
      Object var2;
      try {
         this.getStaticFileLock().readLock().lock();
         var2 = (SegmentRegionFileHandle)this.fileMap.get(var1);
      } finally {
         this.getStaticFileLock().readLock().unlock();
      }

      if (var2 == null) {
         try {
            this.getStaticFileLock().writeLock().lock();
            if (this.getStaticFileMap().size() >= 500) {
               removeOldestHandle(this, var1);
            }

            FileExt var12 = new FileExt(var1);
            Object var10000 = var1.endsWith(".smd2") ? new SegmentRegionFileOld(this.UID, this.onServer, var12, var1, this) : new SegmentRegionFileNew(this.UID, this.onServer, var12, var1, this);
            var2 = var10000;
            if (!((SegmentRegionFileHandle)var10000).getFile().getChannel().isOpen()) {
               System.err.println("[IO] Exception: initial opening of " + var1 + " failed!");
            }

            this.fileMap.put(((SegmentRegionFileHandle)var2).getName(), var2);
            this.getStaticFileMap().put(((SegmentRegionFileHandle)var2).getName(), var2);
         } finally {
            this.getStaticFileLock().writeLock().unlock();
         }
      }

      if (!((SegmentRegionFileHandle)var2).getFile().getChannel().isOpen()) {
         try {
            throw new FileNotFoundException("Having to reopen closed file " + var1);
         } catch (Exception var11) {
            var11.printStackTrace();
            System.err.println("[IFFileManager] WARNING: the file " + ((SegmentRegionFileHandle)var2).getName() + " was closed for some reason. Reopening!");
            ((SegmentRegionFileHandle)var2).reopen();
         }
      }

      ((SegmentRegionFileHandle)var2).updateLastAccess();
      return (SegmentRegionFileHandle)var2;
   }

   public static SegmentRegionFileHandle getFromFile(File var0) throws IOException {
      String var1;
      Object var10000 = (var1 = var0.getName()).endsWith(".smd2") ? new SegmentRegionFileOld(var1, true, var0, var1, (IOFileManager)null) : new SegmentRegionFileNew(var1, true, var0, var1, (IOFileManager)null);
      Object var2 = var10000;
      if (!((SegmentRegionFileHandle)var10000).getFile().getChannel().isOpen()) {
         throw new IOException("[IO] Exception: initial opening of " + var1 + " failed!");
      } else {
         ((SegmentRegionFileHandle)var2).updateLastAccess();
         return (SegmentRegionFileHandle)var2;
      }
   }

   public SegmentRegionFileHandle getFileAndLockIt(String var1, boolean var2) throws IOException {
      Object var3;
      try {
         this.getStaticFileLock().readLock().lock();
         if ((var3 = (SegmentRegionFileHandle)this.fileMap.get(var1)) != null) {
            if (!((SegmentRegionFileHandle)var3).getFile().getChannel().isOpen()) {
               try {
                  throw new FileNotFoundException("Having to reopen closed file " + var1);
               } catch (Exception var14) {
                  var14.printStackTrace();
                  System.err.println("[IFFileManager] WARNING: the file " + ((SegmentRegionFileHandle)var3).getName() + " was closed for some reason. Reopening!");
                  ((SegmentRegionFileHandle)var3).reopen();
               }
            }

            ((SegmentRegionFileHandle)var3).updateLastAccess();
            if (var2) {
               ((SegmentRegionFileHandle)var3).rwl.readLock().lock();
            } else {
               ((SegmentRegionFileHandle)var3).rwl.writeLock().lock();
            }

            return (SegmentRegionFileHandle)var3;
         }
      } finally {
         this.getStaticFileLock().readLock().unlock();
      }

      if (var3 == null) {
         try {
            this.getStaticFileLock().writeLock().lock();
            if (this.getStaticFileMap().size() >= 500) {
               removeOldestHandle(this, var1);
            }

            FileExt var16 = new FileExt(var1);
            Object var10000 = var1.endsWith(".smd2") ? new SegmentRegionFileOld(this.UID, this.onServer, var16, var1, this) : new SegmentRegionFileNew(this.UID, this.onServer, var16, var1, this);
            var3 = var10000;
            if (!((SegmentRegionFileHandle)var10000).getFile().getChannel().isOpen()) {
               System.err.println("[IO] Exception: initial opening of " + var1 + " failed!");
            }

            this.fileMap.put(((SegmentRegionFileHandle)var3).getName(), var3);
            this.getStaticFileMap().put(((SegmentRegionFileHandle)var3).getName(), var3);
            if (!((SegmentRegionFileHandle)var3).getFile().getChannel().isOpen()) {
               try {
                  throw new FileNotFoundException("Having to reopen closed file " + var1);
               } catch (Exception var12) {
                  var1 = null;
                  var12.printStackTrace();
                  System.err.println("[IFFileManager] WARNING: the file " + ((SegmentRegionFileHandle)var3).getName() + " was closed for some reason. Reopening!");
                  ((SegmentRegionFileHandle)var3).reopen();
               }
            }

            ((SegmentRegionFileHandle)var3).updateLastAccess();
            if (var2) {
               ((SegmentRegionFileHandle)var3).rwl.readLock().lock();
            } else {
               ((SegmentRegionFileHandle)var3).rwl.writeLock().lock();
            }
         } finally {
            this.getStaticFileLock().writeLock().unlock();
         }
      }

      return (SegmentRegionFileHandle)var3;
   }

   public final Object2ObjectOpenHashMap getStaticFileMap() {
      return this.onServer ? staticFileMapServer : staticFileMapClient;
   }

   public final ReentrantReadWriteLock getStaticFileLock() {
      return this.onServer ? rwlServer : rwlClient;
   }

   public void removeFileFromMap(String var1) throws IOException {
      this.getStaticFileLock().writeLock().lock();

      try {
         SegmentRegionFileHandle var4;
         if ((var4 = (SegmentRegionFileHandle)this.fileMap.get(var1)) != null) {
            this.fileMap.remove(var4.getName());
            this.getStaticFileMap().remove(var4.getName());
         }
      } finally {
         this.getStaticFileLock().writeLock().unlock();
      }

   }

   public void removeFile(String var1) throws IOException {
      this.getStaticFileLock().writeLock().lock();

      try {
         SegmentRegionFileHandle var4;
         if ((var4 = (SegmentRegionFileHandle)this.fileMap.get(var1)) != null) {
            var4.delete();
            this.fileMap.remove(var4.getName());
            this.getStaticFileMap().remove(var4.getName());
         }
      } finally {
         this.getStaticFileLock().writeLock().unlock();
      }

   }

   public static void cleanUp(boolean var0) {
      SegmentRegionFileHandle var1;
      Iterator var4;
      if (var0) {
         rwlServer.writeLock().lock();
         var4 = staticFileMapServer.values().iterator();

         while(var4.hasNext()) {
            var1 = (SegmentRegionFileHandle)var4.next();

            try {
               var1.close();
            } catch (IOException var2) {
               var2.printStackTrace();
            }
         }

         staticFileMapServer.clear();
         rwlServer.writeLock().unlock();
      } else {
         rwlClient.writeLock().lock();
         var4 = staticFileMapClient.values().iterator();

         while(var4.hasNext()) {
            var1 = (SegmentRegionFileHandle)var4.next();

            try {
               var1.close();
            } catch (IOException var3) {
               var3.printStackTrace();
            }
         }

         staticFileMapClient.clear();
         rwlClient.writeLock().unlock();
      }
   }
}
