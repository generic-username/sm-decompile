package org.schema.game.common.controller.io;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import org.schema.game.common.data.world.SegmentData3Byte;
import org.schema.game.common.data.world.SegmentDataIntArray;
import org.schema.game.common.data.world.SegmentDataOld;
import org.schema.game.common.data.world.SegmentDataRepairAll;

public class SegmentSerializationBuffers {
   private static boolean created;
   private byte[] byteArray = new byte[4096];
   public final Int2ObjectOpenHashMap dummies = new Int2ObjectOpenHashMap();
   public final Deflater deflater = new Deflater();
   public final Inflater inflater = new Inflater();
   public byte[] SEGMENT_BYTE_FORMAT_BUFFER = new byte[98304];
   public byte[] SEGMENT_BUFFER = new byte[1048576];
   private FastByteArrayOutputStream byteArrayOutput = new FastByteArrayOutputStream(4096);
   private static final ObjectArrayList pool = new ObjectArrayList();

   public void ensureSegmentBufferSize(int var1) {
      int var2;
      for(var2 = this.SEGMENT_BUFFER.length; var2 < var1; var2 <<= 1) {
      }

      this.SEGMENT_BUFFER = new byte[var2];
   }

   public FastByteArrayOutputStream getStaticArrayOutput() {
      this.byteArrayOutput.reset();
      return this.byteArrayOutput;
   }

   public byte[] getStaticArray(int var1) {
      if (this.byteArray.length < var1) {
         int var2;
         for(var2 = this.byteArray.length; var2 < var1; var2 <<= 1) {
         }

         this.byteArray = new byte[var2];
      }

      return this.byteArray;
   }

   public SegmentSerializationBuffers() {
      createDummies(this.dummies);
   }

   public static void create() {
      synchronized(pool) {
         if (!created) {
            for(int var1 = 0; var1 < 15; ++var1) {
               pool.add(new SegmentSerializationBuffers());
            }

            created = true;
         }

      }
   }

   public static SegmentSerializationBuffers get() {
      synchronized(pool) {
         return pool.isEmpty() ? new SegmentSerializationBuffers() : (SegmentSerializationBuffers)pool.remove(pool.size() - 1);
      }
   }

   public static void free(SegmentSerializationBuffers var0) {
      synchronized(pool) {
         pool.add(var0);
      }
   }

   public static void createDummies(Int2ObjectOpenHashMap var0) {
      if (var0.isEmpty()) {
         var0.put(0, new SegmentDataOld(false));
         var0.put(1, new SegmentDataRepairAll(false));
         var0.put(2, new SegmentData3Byte(true));
         var0.put(3, new SegmentDataIntArray(true));
         var0.put(4, new SegmentDataIntArray(true));
         var0.put(5, new SegmentDataIntArray(true));
         var0.put(6, new SegmentDataIntArray(true));
      }

   }

   static {
      create();
   }
}
