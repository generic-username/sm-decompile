package org.schema.game.common.controller.io;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class SegmentRegionFileNew extends SegmentRegionFileHandle {
   private final SegmentHeader header = new SegmentHeader();

   public SegmentRegionFileNew(String var1, boolean var2, File var3, String var4, IOFileManager var5) throws IOException {
      super(32768, var1, var2, var3, var4, var5);
      this.createHeader();
   }

   public int getVersion() {
      return this.getHeader().version;
   }

   protected void createHeader() throws IOException {
      assert this.header != null;

      System.currentTimeMillis();
      boolean var1 = this.f.exists();
      long var2 = 0L;
      if (var1) {
         this.file = new RandomAccessFile(this.f, "rw");
         var2 = this.f.length();
      }

      if (var2 >= 16388L) {
         this.header.read(this.file);
      } else {
         if (var1) {
            try {
               throw new NoHeaderException("No Header in file " + this.getDesc() + " -> " + this.f.getName() + "; read: " + var2 + "; is now: " + this.file.length() + " / " + this.f.length() + " (must be min: 16388); REWRITING HEADER; fExisted: " + this.f.exists());
            } catch (Exception var4) {
               var4.printStackTrace();
            }
         }

         SegmentHeader.writeEmptyHeader(this.f);
         if (!var1) {
            this.file = new RandomAccessFile(this.f, "rw");
         }

         this.getHeader().resetToEmpty();
      }
   }

   public void writeHeader() throws IOException {
      System.err.println("[IO] Writing Header of " + this.getName());
      this.getHeader().writeTo(this.file);
   }

   public void writeSegmentWithoutMap(long var1, SegmentOutputStream var3) throws IOException {
      var3.writeToFileWithoutMap(this.file, var1, this.getName(), this.f);
   }

   public SegmentHeader getHeader() {
      return this.header;
   }
}
