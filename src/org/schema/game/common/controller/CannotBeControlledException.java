package org.schema.game.common.controller;

import org.schema.game.common.data.element.ElementInformation;

public class CannotBeControlledException extends Exception {
   private static final long serialVersionUID = 1L;
   public final ElementInformation info;
   public final ElementInformation fromInfo;

   public CannotBeControlledException(ElementInformation var1, ElementInformation var2) {
      this.fromInfo = var1;
      this.info = var2;
   }
}
