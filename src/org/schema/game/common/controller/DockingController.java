package org.schema.game.common.controller;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.CompoundShape;
import com.bulletphysics.collision.shapes.CompoundShapeChild;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Matrix3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Quat4Util;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.client.view.camera.InShipCamera;
import org.schema.game.common.Starter;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.controller.elements.dockingBlock.DockingBlockCollectionManager;
import org.schema.game.common.controller.elements.dockingBlock.DockingBlockManagerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementDocking;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.CubeShape;
import org.schema.game.common.data.physics.CubesCompoundShape;
import org.schema.game.common.data.physics.RigidBodySegmentController;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Chunk16SegmentData;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.network.objects.NetworkSegmentController;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.blueprint.DockingTagOverwrite;
import org.schema.schine.ai.stateMachines.AiInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.objects.container.UpdateWithoutPhysicsObjectInterface;
import org.schema.schine.network.objects.remote.UnsaveNetworkOperationException;
import org.schema.schine.network.server.ServerMessage;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class DockingController implements UpdateWithoutPhysicsObjectInterface {
   private final List dockedOnThis = new ObjectArrayList();
   private final SegmentController segmentController;
   private final Transform dockingPos = new Transform();
   private final Transform dockingPosInverse = new Transform();
   private final Vector3f size = new Vector3f();
   private final Quat4f fromTmp = new Quat4f();
   private final Quat4f res = new Quat4f();
   public Quat4f targetQuaternion = new Quat4f();
   public DockingTagOverwrite tagOverwrite;
   public Quat4f targetStartQuaternion = new Quat4f();
   Vector3f mod = new Vector3f();
   private ElementDocking dockedOn = null;
   private String delayedDockUID = null;
   private Vector3i delayedDockPos;
   private boolean delayedUndock;
   private long lastDockingOrUndocking;
   private long lastUnDocking;
   private long lastSentMessage;
   private long delayCheck;
   private boolean docked;
   private boolean sizeSetFromTag;
   private boolean loadedFromTag;
   private boolean triggerMotherShipRemoved;
   private long dockingUIDnotFound;
   private boolean updateOnce;
   private Quat4f delayedDockLocalRot;
   private int dockingCode;
   private Quat4f localDockingOrientation = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);

   public DockingController(SegmentController var1) {
      this.segmentController = var1;
      this.dockingPos.setIdentity();
      this.dockingPosInverse.setIdentity();
      this.targetStartQuaternion.set(0.0F, 0.0F, 0.0F, 1.0F);
   }

   public static void getDockingTransformation(byte var0, Transform var1) {
      switch(var0) {
      case 0:
         var1.basis.rotX(1.5707964F);
         return;
      case 1:
         Matrix3f var2;
         (var2 = new Matrix3f()).rotY(-1.5707964F);
         var1.basis.rotZ(1.5707964F);
         var1.basis.mul(var2);
         var2.rotZ(1.5707964F);
         var1.basis.mul(var2);
         return;
      case 2:
         return;
      case 3:
         var1.basis.rotX(3.1415927F);
         return;
      case 4:
         var1.basis.rotZ(1.5707964F);
         return;
      case 5:
         var1.basis.rotZ(-1.5707964F);
      default:
      }
   }

   public static boolean isTurretDocking(SegmentPiece var0) {
      return var0.getType() == 7;
   }

   public static void removeChieldsRecusrive(SegmentController var0, CubesCompoundShape var1) {
      var1.getNumChildShapes();
      var1.removeChildShape(var0.getPhysicsDataContainer().getShapeChild().childShape);
      if (var0.getDockingController().getDockedOnThis().size() > 0) {
         Iterator var2 = var0.getDockingController().getDockedOnThis().iterator();

         while(var2.hasNext()) {
            removeChieldsRecusrive(((ElementDocking)var2.next()).from.getSegment().getSegmentController(), var1);
         }
      }

   }

   public static void updateTargetWithRemovedController(SegmentController var0, SegmentController var1) {
      if (!var0.isMarkedForDeleteVolatile() && var0.getState().getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(var0.getId())) {
         Vector3f var2 = new Vector3f();
         Vector3f var3 = new Vector3f();
         RigidBody var4;
         if (var0.getPhysicsDataContainer().getObject() != null && var0.getPhysicsDataContainer().getObject() instanceof RigidBody) {
            (var4 = (RigidBody)var0.getPhysicsDataContainer().getObject()).getLinearVelocity(var2);
            var4.getAngularVelocity(var3);
         }

         var0.onPhysicsRemove();
         if (var0.getPhysicsDataContainer().lastCenter.length() > 0.0F) {
            Vector3f var5 = ((CubesCompoundShape)var0.getPhysicsDataContainer().getShape()).getCenterOfMass();
            var0.getWorldTransform().basis.transform(var5);
            var0.getWorldTransform().origin.add(var5);
         }

         if (var0.getDockingController().getDockedOnThis().size() > 0) {
            CubesCompoundShape var7 = (CubesCompoundShape)var0.getPhysicsDataContainer().getShape();
            removeChieldsRecusrive(var1, var7);
            if (var0.getMass() > 0.0F) {
               var0.getPhysicsDataContainer().updateMass(var0.getMass(), true);
            }

            RigidBody var8;
            (var8 = var0.getPhysics().getBodyFromShape(var7, var0.getMass(), var0.getWorldTransform())).setUserPointer(var0.getId());

            assert var8.getCollisionShape() == var7;

            var0.getPhysicsDataContainer().setObject(var8);
            Iterator var6 = var0.getDockingController().getDockedOnThis().iterator();

            while(var6.hasNext()) {
               ((ElementDocking)var6.next()).from.getSegment().getSegmentController().getPhysicsDataContainer().setObject((CollisionObject)null);
            }

            assert var0.getPhysicsDataContainer().getShape() == var7;

            assert var0.getPhysicsDataContainer().getShape() == var8.getCollisionShape();

            var0.onPhysicsAdd();
            if (var0.getMass() > 0.0F) {
               var0.getPhysicsDataContainer().updateMass(var0.getMass(), true);
            }
         } else {
            System.err.println("[DOCKING] doing complete physics reset for " + var0);
            var0.getPhysicsDataContainer().setObject((CollisionObject)null);
            var0.getPhysicsDataContainer().setShape((CollisionShape)null);
            var0.getPhysicsDataContainer().setShapeChield((CompoundShapeChild)null, -1);
            var0.getPhysicsDataContainer().initialTransform.set(var0.getWorldTransform());
            var0.getRemoteTransformable().getInitialTransform().set(var0.getWorldTransform());
            var0.initPhysics();
            var0.onPhysicsAdd();
         }

         if (var0.getPhysicsDataContainer().getObject() != null && var0.getPhysicsDataContainer().getObject() instanceof RigidBody) {
            (var4 = (RigidBody)var0.getPhysicsDataContainer().getObject()).setLinearVelocity(var2);
            var4.setAngularVelocity(var3);
         }

      } else {
         System.err.println("[DOCKING] no update needed from undocking for mothership " + var0 + " -> segmentController has been deleted");
      }
   }

   public static void getDockingTransformationRecursive(SegmentController var0, byte var1, Transform var2) {
      Transform var3;
      (var3 = new Transform()).setIdentity();
      getDockingTransformation(var1, var3);
      if (var0.getDockingController().isDocked()) {
         getDockingTransformation(var0.getDockingController().getDockedOn().to.getOrientation(), var2);
      }

      var2.mul(var3);
   }

   public void checkDockingValid() throws CollectionNotLoadedException {
      if (this.segmentController.isOnServer() || this.segmentController.isClientOwnObject()) {
         if (this.segmentController.getDockingController().isDocked()) {
            boolean var1 = false;
            boolean var2 = false;
            SegmentController var3;
            if ((var3 = this.segmentController.getDockingController().getDockedOn().to.getSegment().getSegmentController()) instanceof ManagedSegmentController && ((ManagedSegmentController)var3).getManagerContainer() instanceof DockingBlockManagerInterface) {
               DockingBlockManagerInterface var4 = (DockingBlockManagerInterface)((ManagedSegmentController)var3).getManagerContainer();
               Vector3i var5 = this.segmentController.getDockingController().getDockedOn().to.getAbsolutePos(new Vector3i());
               if (this.isOnServer()) {
                  SegmentPiece var6 = this.getDockedOn().from;
                  SegmentPiece var7 = this.getDockedOn().to;
                  SegmentController var8 = var6.getSegment().getSegmentController();
                  SegmentController var9 = var7.getSegment().getSegmentController();
                  var8.getSegmentBuffer().getPointUnsave(var6.getAbsolutePos(new Vector3i()), var6);
                  var9.getSegmentBuffer().getPointUnsave(var7.getAbsolutePos(new Vector3i()), var7);
                  if (var8 == null || var9 == null) {
                     return;
                  }

                  if (!(var1 = var7 != null && var7.getType() != 0 && ElementKeyMap.getInfo(var7.getType()).isOldDockable())) {
                     System.err.println("[DOCKING] " + this.segmentController + " -> target is not a valid dockable block: " + (var7 != null) + ", " + var7 != null ? var7.getType() != 0 : "null, " + (var7 != null && var7.getType() != 0 ? ElementKeyMap.getInfo(var7.getType()).isOldDockable() : "null"));
                  }
               } else {
                  var1 = true;
               }

               Iterator var13 = var4.getDockingBlock().iterator();

               while(var13.hasNext()) {
                  ManagerModuleCollection var14 = (ManagerModuleCollection)var13.next();
                  boolean var15 = false;

                  try {
                     Iterator var16 = var14.getCollectionManagers().iterator();

                     while(var16.hasNext()) {
                        DockingBlockCollectionManager var12;
                        if ((var12 = (DockingBlockCollectionManager)var16.next()).getControllerPos().equals(var5)) {
                           var2 = true;
                           if (!var12.isObjectDockable(this.segmentController, this.segmentController.getDockingController().getDockedOn().to.getOrientation(), false) & !((GameStateInterface)this.getState()).getGameState().isIgnoreDockingArea()) {
                              if (this.isOnServer()) {
                                 System.err.println("[SERVER] docking invalid: " + this.segmentController + ": obj not dockable");
                                 ((GameServerState)this.getState()).getController().broadcastMessageAdmin(new Object[]{17, this.segmentController}, 3);
                                 this.requestDelayedUndock(true);
                              } else if (this.segmentController.isClientOwnObject()) {
                                 ((GameClientState)this.segmentController.getState()).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_DOCKINGCONTROLLER_1, this.segmentController.getRealName()), 0.0F);
                              }

                              var15 = true;
                              break;
                           }
                        }
                     }
                  } catch (ConcurrentModificationException var10) {
                     var10.printStackTrace();
                     System.err.println("Exception could be catched and handeled by deferring docking valid request");
                     throw new CollectionNotLoadedException();
                  }

                  if (var15) {
                     break;
                  }
               }
            } else {
               assert false;
            }

            if (!var2) {
               if (this.isOnServer()) {
                  if (!var1) {
                     System.err.println("[SERVER] docking invalid: " + this.segmentController + ": docking module removed");
                     this.requestDelayedUndock(true);
                  } else {
                     System.err.println("[SERVER] docking valid can not yet be checked. Block is dockable but not in docking manager yet: " + this.segmentController);
                  }
               } else if (this.segmentController.isClientOwnObject() || var3.isClientOwnObject()) {
                  ((GameClientState)this.segmentController.getState()).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_DOCKINGCONTROLLER_2, this.segmentController.getRealName()), 0.0F);
               }
            }
         }

         Iterator var11 = this.getDockedOnThis().iterator();

         while(var11.hasNext()) {
            ((ElementDocking)var11.next()).from.getSegment().getSegmentController().flagUpdateDocking();
         }
      }

   }

   private boolean checkFactionAllowed(SegmentPiece var1, SegmentPiece var2) {
      if (var2.getSegment().getSegmentController().getFactionId() == 0) {
         return true;
      } else {
         boolean var3 = var1.getSegment().getSegmentController().getFactionId() == var2.getSegment().getSegmentController().getFactionId();
         boolean var4 = false;
         if (var1.getSegment().getSegmentController() instanceof PlayerControllable) {
            Iterator var5 = ((PlayerControllable)var1.getSegment().getSegmentController()).getAttachedPlayers().iterator();

            while(var5.hasNext()) {
               if (((PlayerState)var5.next()).getFactionId() == var2.getSegment().getSegmentController().getFactionId()) {
                  var4 = true;
                  break;
               }
            }
         }

         boolean var6 = this.loadedFromTag;
         this.loadedFromTag = false;
         return !this.isOnServer() || var3 || var4 || var6;
      }
   }

   public boolean checkLastDockDelay() {
      return System.currentTimeMillis() - this.lastDockingOrUndocking > 1000L;
   }

   public SegmentController getAbsoluteMother() {
      return this.isDocked() ? this.getDockedOn().to.getSegment().getSegmentController().getDockingController().getAbsoluteMother() : this.segmentController;
   }

   private boolean dock(SegmentPiece var1, SegmentPiece var2, boolean var3, boolean var4) throws DockingNotYetUpdatedException {
      if (!this.segmentController.railController.isInAnyRailRelation() && !var2.getSegmentController().railController.isInAnyRailRelation()) {
         assert this.segmentController != var2.getSegment().getSegmentController();

         SegmentController var5 = var2.getSegment().getSegmentController();
         SegmentController var6 = var2.getSegment().getSegmentController();
         this.updateOnce = false;
         ElementDocking var8;
         if (!var2.getSegment().getSegmentController().getDockingController().getDockedOnThis().isEmpty()) {
            Iterator var7 = var2.getSegment().getSegmentController().getDockingController().getDockedOnThis().iterator();

            while(var7.hasNext()) {
               if ((var8 = (ElementDocking)var7.next()).to.equals(var2)) {
                  this.segmentController.sendControllingPlayersServerMessage(new Object[]{19, var8.from.getSegment().getSegmentController().toNiceString()}, 3);
                  this.lastDockingOrUndocking = System.currentTimeMillis();
                  return false;
               }
            }
         }

         Transform var20;
         (var20 = new Transform()).setIdentity();
         if (var2.getSegment().getSegmentController().getDockingController().isDocked()) {
            var20.set(var5.getPhysicsDataContainer().getShapeChild().transform);
            var6 = var2.getSegment().getSegmentController().getDockingController().getDockedOn().to.getSegment().getSegmentController().getDockingController().getAbsoluteMother();
            if (isTurretDocking(var2.getSegment().getSegmentController().getDockingController().getDockedOn().to)) {
               System.err.println("[DOCKING] ERROR: cannot dock onto docked object (turret chain): " + var1.getSegment().getSegmentController() + " -> " + var2.getSegment().getSegmentController());
               this.segmentController.sendControllingPlayersServerMessage(new Object[]{20}, 3);
               this.lastDockingOrUndocking = System.currentTimeMillis();
               return false;
            }
         }

         if (!this.getDockedOnThis().isEmpty()) {
            for(int var21 = 0; var21 < this.getDockedOnThis().size(); ++var21) {
               SegmentController var9 = ((ElementDocking)this.getDockedOnThis().get(var21)).from.getSegment().getSegmentController();
               if (!var4 && !var9.getDockingController().updateOnce) {
                  throw new DockingNotYetUpdatedException();
               }
            }

            if (isTurretDocking(var2)) {
               System.err.println("[DOCKING] ERROR: cannot dock onto docked object (turret chain): " + var1.getSegment().getSegmentController() + " -> " + var2.getSegment().getSegmentController());
               this.segmentController.sendControllingPlayersServerMessage(new Object[]{21}, 3);
               this.lastDockingOrUndocking = System.currentTimeMillis();
               return false;
            }
         }

         if (this.isDocked() && this.getDockedOn().to.getSegment().getSegmentController() == var5) {
            return false;
         } else {
            this.setDockedOn(new ElementDocking(var1, var2, var3));
            this.lastDockingOrUndocking = System.currentTimeMillis();
            var8 = new ElementDocking(var1, var2, var3);
            if (!var5.getDockingController().getDockedOnThis().contains(var8)) {
               var5.getDockingController().getDockedOnThis().add(var8);
            }

            this.updateDockingPosition();
            Transform var22;
            (var22 = new Transform()).setIdentity();
            Vector3i var14 = this.getDockedOn().to.getAbsolutePos(new Vector3i());
            var22.origin.set((float)(var14.x - 16), (float)(var14.y - 16), (float)(var14.z - 16));
            switch(this.getDockedOn().to.getOrientation()) {
            case 0:
               GlUtil.getForwardVector(this.mod, var22);
               break;
            case 1:
               GlUtil.getBackVector(this.mod, var22);
               break;
            case 2:
               GlUtil.getUpVector(this.mod, var22);
               break;
            case 3:
               GlUtil.getBottomVector(this.mod, var22);
               break;
            case 4:
               GlUtil.getLeftVector(this.mod, var22);
               break;
            case 5:
               GlUtil.getRightVector(this.mod, var22);
            }

            BoundingBox var15 = this.getDockedOn().from.getSegment().getSegmentController().getSegmentBuffer().getBoundingBox();
            if ((this.sizeSetFromTag || !this.isOnServer()) && this.size.length() != 0.0F) {
               this.sizeSetFromTag = false;
            } else {
               this.size.set(var15.min);
            }

            if (isTurretDocking(this.getDockedOn().to)) {
               this.mod.scale(1.5F);
            } else {
               float var16 = 0.0F;
               switch(this.getDockedOn().to.getOrientation()) {
               case 0:
                  var16 = this.size.y;
                  break;
               case 1:
                  var16 = this.size.y;
                  break;
               case 2:
                  var16 = this.size.y;
                  break;
               case 3:
                  var16 = this.size.y;
                  break;
               case 4:
                  var16 = this.size.y;
                  break;
               case 5:
                  var16 = this.size.y;
               }

               var16 = Math.abs(var16);
               this.mod.scale(var16 + 0.5F);
            }

            var22.origin.add(this.mod);
            this.segmentController.onPhysicsRemove();
            var6.onPhysicsRemove();
            CompoundShape var19 = (CompoundShape)this.segmentController.getPhysicsDataContainer().getShape();
            CompoundShape var17 = (CompoundShape)var6.getPhysicsDataContainer().getShape();

            assert var17 != var19 : this.segmentController + "; " + var6;

            for(int var18 = 0; var18 < var19.getNumChildShapes(); ++var18) {
               this.getDockedOn().to.refresh();
               byte var23 = this.getDockedOn().to.getOrientation();
               Transform var10 = ((CompoundShapeChild)var19.getChildList().get(var18)).transform;
               Transform var11;
               (var11 = new Transform()).setIdentity();
               var11.mul(var20);
               getDockingTransformation(var23, var22);
               var11.mul(var22);
               var11.mul(var10);
               var17.addChildShape(var11, var19.getChildShape(var18));
               System.err.println("[DOCKING] " + this.getState() + " ADDING: " + var18 + ": " + var19 + " -> " + var17);

               assert var17 != var19;

               if (var19.getChildShape(var18) instanceof CubeShape) {
                  CubeShape var24 = (CubeShape)var19.getChildShape(var18);
                  int var12 = var17.getChildList().size() - 1;
                  var24.getSegmentController().getPhysicsDataContainer().setShapeChield((CompoundShapeChild)var17.getChildList().get(var12), var12);

                  assert var17.getChildList().contains(var24.getSegmentController().getPhysicsDataContainer().getShapeChild());

                  Quat4fTools.set(var11.basis, var24.getSegmentBuffer().getSegmentController().getDockingController().targetStartQuaternion);
                  Transform var13;
                  (var13 = new Transform()).setIdentity();
                  if (var10.equals(var13) && var20.equals(var13) && isTurretDocking(var2)) {
                     Matrix3f var26;
                     (var26 = new Matrix3f()).set(new Quat4f(this.localDockingOrientation));
                     var26.mul(var11.basis);
                     var11.basis.set(var26);
                  } else {
                     this.localDockingOrientation.set(0.0F, 0.0F, 0.0F, 1.0F);
                  }

                  Quat4fTools.set(var11.basis, var24.getSegmentBuffer().getSegmentController().getDockingController().targetQuaternion);
               }
            }

            var17.recalculateLocalAabb();
            System.err.println("[DOCKING] " + this.segmentController.getState() + " DOCKED ON TARGET SHAPE: " + var17);
            ((RigidBodySegmentController)this.segmentController.getPhysicsDataContainer().getObject()).getInvMass();
            this.segmentController.getPhysicsDataContainer().updateMass(this.segmentController.getMass(), true);
            ((RigidBodySegmentController)this.segmentController.getPhysicsDataContainer().getObject()).getInvMass();
            Iterator var29;
            if (this.segmentController.getSectorId() != var5.getSectorId()) {
               this.segmentController.setSectorId(var5.getSectorId());
               if (this.segmentController instanceof PlayerControllable) {
                  var29 = ((PlayerControllable)this.segmentController).getAttachedPlayers().iterator();

                  while(var29.hasNext()) {
                     PlayerState var25 = (PlayerState)var29.next();
                     System.err.println("[SERVER][DOCKING] sector docking on border! " + this.segmentController + " has players attached. Doing Sector Change for " + var25);
                     Vector3i var31;
                     if (this.isOnServer()) {
                        var31 = ((GameServerState)this.getState()).getUniverse().getSector(var5.getSectorId()).pos;
                     } else {
                        var31 = ((RemoteSector)this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var5.getSectorId())).clientPos();
                     }

                     var25.setCurrentSector(new Vector3i(var31));
                     var25.setCurrentSectorId(var5.getSectorId());
                     PlayerCharacter var33;
                     if ((var33 = var25.getAssingedPlayerCharacter()) != null) {
                        System.err.println("[SERVER][DOCKING] sector docking on border! " + var31 + " has CHARACTER. Doing Sector Change for " + var33 + ": ");
                        var33.setSectorId(var5.getSectorId());
                     } else {
                        System.err.println("[SERVER] WARNING NO PLAYER CHARACTER ATTACHED TO " + var25);
                     }
                  }
               }
            }

            if (var6.getPhysicsDataContainer().lastCenter.length() > 0.0F) {
               Vector3f var30 = ((CubesCompoundShape)var6.getPhysicsDataContainer().getShape()).getCenterOfMass();
               var6.getWorldTransform().basis.transform(var30);
               var6.getWorldTransform().origin.add(var30);
            }

            RigidBody var28;
            (var28 = var6.getPhysics().getBodyFromShape(var17, var6.getMass() > 0.0F ? var6.getMass() + this.segmentController.getMass() : 0.0F, var6.getWorldTransform())).setUserPointer(var6.getId());

            assert var28.getCollisionShape() == var17;

            var6.getPhysicsDataContainer().setObject(var28);
            var29 = var5.getDockingController().getDockedOnThis().iterator();

            while(var29.hasNext()) {
               ((ElementDocking)var29.next()).from.getSegment().getSegmentController().getPhysicsDataContainer().setObject((CollisionObject)null);
            }

            var6.getPhysicsDataContainer().updatePhysical(this.getState().getUpdateTime());
            var6.onPhysicsAdd();
            ((RigidBodySegmentController)var6.getPhysicsDataContainer().getObject()).activate(true);
            if (this.segmentController instanceof SendableSegmentController) {
               ((SendableSegmentController)this.segmentController).handleNTDockChanged();
            }

            assert var6.getPhysicsDataContainer().getShape() == var17;

            assert var6.getPhysicsDataContainer().getShape() == var28.getCollisionShape();

            if (!this.isOnServer() && ((GameClientState)this.getState()).getCurrentSectorId() == this.segmentController.getSectorId()) {
               ((GameClientController)this.getState().getController()).queueTransformableAudio("0022_ambience sfx - ambient hangar sounds hydraulics", this.segmentController.getWorldTransform(), 0.99F);
            }

            if (this.isOnServer()) {
               SendableSegmentController var32 = (SendableSegmentController)var2.getSegment().getSegmentController();
               Vector3i var27 = var2.getAbsolutePos(new Vector3i());
               System.err.println("[DOCK] ACTIVATE SURROUND SERVER WITH DOCK " + var27);
               var32.activateSurroundServer(true, var27, 405);
            }

            this.dockingCode = var6.getId();
            Starter.modManager.onSegmentControllerDocking(this.segmentController);
            this.segmentController.flagupdateMass();
            System.err.println("[DOCK] Speed after docking " + this.segmentController.getSpeedCurrent());
            if (this.segmentController.isClientOwnObject() && Controller.getCamera() != null && Controller.getCamera() instanceof InShipCamera) {
               ((InShipCamera)Controller.getCamera()).docked = true;
            }

            return true;
         }
      } else {
         this.segmentController.sendControllingPlayersServerMessage(new Object[]{18}, 3);
         return false;
      }
   }

   public boolean isPublicException(SegmentPiece var1) throws DockingNotYetUpdatedException {
      Vector3i var2 = new Vector3i();
      Vector3i var3 = new Vector3i();
      var1.getAbsolutePos(var2);

      for(int var4 = 0; var4 < 6; ++var4) {
         var3.add(var2, Element.DIRECTIONSi[var4]);
         SegmentPiece var5;
         if ((var5 = var1.getSegment().getSegmentController().getSegmentBuffer().getPointUnsave(var3)) == null) {
            throw new DockingNotYetUpdatedException();
         }

         if (var5.getType() == 346) {
            return true;
         }
      }

      return false;
   }

   private boolean dockOrLand(SegmentPiece var1, SegmentPiece var2) throws DockingNotYetUpdatedException {
      var1.refresh();
      if (var1.getType() == 0) {
         System.err.println("[DOCKING] NOT DOCKING " + var2.getSegment().getSegmentController() + " ON NOTHING: " + var1 + " ON " + var1.getSegment().getSegmentController());
         return false;
      } else {
         boolean var3 = this.isPublicException(var1);
         if (!this.checkFactionAllowed(var2, var1)) {
            var1.getSegment().getSegmentController();
            if (!var3) {
               if (!this.isOnServer() && ((GameClientState)this.getState()).getShip() == this.segmentController) {
                  ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_DOCKINGCONTROLLER_9, 0.0F);
               }

               System.err.println("[DOCKING] NOT DOCKING: faction does not equal " + this.segmentController.getState());
               if (this.isOnServer() && System.currentTimeMillis() - this.lastSentMessage > 4000L) {
                  if (this.segmentController instanceof PlayerControllable) {
                     Iterator var4 = ((PlayerControllable)this.segmentController).getAttachedPlayers().iterator();

                     while(var4.hasNext()) {
                        PlayerState var5;
                        (var5 = (PlayerState)var4.next()).sendServerMessage(new ServerMessage(new Object[]{22}, 3, var5.getId()));
                     }
                  }

                  this.lastSentMessage = System.currentTimeMillis();
               }

               return false;
            }
         }

         if (ElementKeyMap.getInfo(var1.getType()).isOldDockable()) {
            return this.dock(var2, var1, var3, false);
         } else {
            assert false : var1.getType();

            return false;
         }
      }
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var5;
      String var2 = (String)(var5 = (Tag[])var1.getValue())[0].getValue();
      byte var3 = 0;
      if (var5.length > 5 && var5[5].getType() == Tag.Type.BYTE) {
         var3 = (Byte)var5[5].getValue();
      }

      if (var5.length > 6 && var5[6].getType() == Tag.Type.VECTOR4f) {
         this.localDockingOrientation = new Quat4f((Vector4f)var5[6].getValue());
      }

      if (!var2.equals("NONE")) {
         this.segmentController.setHidden(true);
         this.loadedFromTag = true;
         Vector3i var4 = (Vector3i)var5[1].getValue();
         if (this.segmentController.isLoadedFromChunk16()) {
            var4.add(Chunk16SegmentData.SHIFT);
         }

         this.requestDelayedDock(var2, var4, new Quat4f(this.localDockingOrientation), var3);
      }

      if (var5.length > 4 && var5[4].getType() == Tag.Type.VECTOR3f && "s".equals(var5[4].getName())) {
         this.size.set((Vector3f)var5[4].getValue());
         this.sizeSetFromTag = true;
      }

   }

   public String getDelayedDock() {
      return this.delayedDockUID;
   }

   public ElementDocking getDockedOn() {
      return this.dockedOn;
   }

   public void setDockedOn(ElementDocking var1) {
      this.dockedOn = var1;
      this.setDocked(var1 != null);
   }

   public List getDockedOnThis() {
      return this.dockedOnThis;
   }

   public Transform getDockingPos() {
      return this.dockingPos;
   }

   public Transform getDockingPosInverse() {
      return this.dockingPosInverse;
   }

   public Vector3f getSize() {
      return this.size;
   }

   private StateInterface getState() {
      return this.segmentController.getState();
   }

   public void getWorldTransform() {
   }

   public boolean isDocked() {
      return this.docked;
   }

   public void setDocked(boolean var1) {
      this.docked = var1;
   }

   public SegmentController getLocalMother() {
      return this.dockedOn.to.getSegment().getSegmentController();
   }

   public boolean isOnServer() {
      return this.segmentController.isOnServer();
   }

   public boolean isTriggerMotherShipRemoved() {
      return this.triggerMotherShipRemoved;
   }

   public void setTriggerMotherShipRemoved(boolean var1) {
      this.triggerMotherShipRemoved = var1;
   }

   public void onDockChanged(NetworkSegmentController var1) {
      if (this.isOnServer()) {
         if (this.isDocked()) {
            assert this.getState().isSynched();

            if (NetworkObject.CHECKUNSAVE && !this.getState().isSynched()) {
               throw new UnsaveNetworkOperationException();
            }

            var1.dockingSize.set(this.size);
            var1.dockingOrientation.set(this.localDockingOrientation.x, this.localDockingOrientation.y, this.localDockingOrientation.z, this.localDockingOrientation.w);
            var1.dockedTo.set(this.getDockedOn().to.getSegment().getSegmentController().getUniqueIdentifier());
            var1.dockedElement.set(new Vector4i(this.getDockedOn().to.getAbsolutePos(new Vector3i()), 0));
            return;
         }

         var1.dockingSize.set(new Vector3f(0.0F, 0.0F, 0.0F));
         var1.dockedTo.set("NONE");
      }

   }

   private void onExistsDelayedDock() throws DockingNotYetUpdatedException {
      assert this.delayedDockUID != null;

      Sendable var2;
      boolean var1 = (var2 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getUidObjectMap().get(this.delayedDockUID)) != null && var2 instanceof SegmentController;
      if (var2 == this.segmentController) {
         try {
            throw new DockingException(this.segmentController + " attempted to dock to itself");
         } catch (DockingException var3) {
            var3.printStackTrace();
            ((GameServerState)this.getState()).getController().broadcastMessageAdmin(new Object[]{23, this.segmentController}, 3);
            System.err.println("[DOCKING] ERROR: docking target for: " + this + " is invalid: " + this.delayedDockUID + "; unhiding...");
            this.delayedDockUID = null;
            this.segmentController.setHidden(false);
            this.setTriggerMotherShipRemoved(false);
         }
      } else if (var1) {
         this.dockingUIDnotFound = -1L;
         SegmentController var5 = (SegmentController)var2;

         assert var5.getUniqueIdentifier().equals(this.delayedDockUID);

         if (var5.getPhysicsDataContainer().isInitialized() && var5.getSegmentBuffer().getBoundingBox().isInitialized() && var5.getSegmentBuffer().getBoundingBox().atLeastOne()) {
            if (this.segmentController.getPhysicsDataContainer().isInitialized() && this.segmentController.getSegmentBuffer().getBoundingBox().isInitialized() && this.segmentController.getSegmentBuffer().getBoundingBox().atLeastOne()) {
               if (var5.getTotalElements() <= 0 || this.segmentController.getTotalElements() <= 0) {
                  System.err.println("[DOCKING] Object has zero elements: " + this.getState() + " with " + var2 + " ON " + (this.isOnServer() ? "SERVER" : "CLIENT"));
               }

               SegmentPiece var6 = var5.getSegmentBuffer().getPointUnsave(this.delayedDockPos);
               SegmentPiece var7 = this.segmentController.getSegmentBuffer().getPointUnsave(Ship.core);
               if (var6 != null && var7 != null) {
                  if (var6.getSegment().getSegmentController().getPhysicsDataContainer().isInitialized() && var7.getSegment().getSegmentController().getPhysicsDataContainer().isInitialized()) {
                     if (this.dockOrLand(var6, var7)) {
                        if (this.isOnServer()) {
                           if ((this.segmentController.getFactionId() == 0 || this.segmentController.getElementClassCountMap().get((short)291) == 0) && !this.isPublicException(var6)) {
                              this.segmentController.setFactionId(var6.getSegment().getSegmentController().getFactionId());
                           }

                           this.segmentController.setHidden(false);
                        }
                     } else if (this.isOnServer()) {
                        if (this.segmentController.getElementClassCountMap().get((short)291) == 0) {
                           this.segmentController.setFactionId(0);
                        }

                        this.segmentController.setHidden(false);
                        System.err.println("[DOCKING] docking failed (docking not executed): UNHIDE SEGMENTCONTROLLER: " + this.segmentController);
                     }

                     this.delayedDockUID = null;
                  } else {
                     System.err.println("[DOCKING] Deffered delayed dock ");
                  }
               } else {
                  this.delayCheck = System.currentTimeMillis();
               }
            } else {
               System.err.println("[DOCKING] SELF PHYSICS NOT YET INITIALIZED: " + this.getState() + " with " + var2 + " ON " + (this.isOnServer() ? "SERVER" : "CLIENT"));
               this.delayCheck = System.currentTimeMillis();
            }
         } else {
            this.delayCheck = System.currentTimeMillis();
         }
      } else if (this.dockingUIDnotFound <= 0L) {
         this.dockingUIDnotFound = System.currentTimeMillis();
      } else {
         if (this.isOnServer() && System.currentTimeMillis() - this.dockingUIDnotFound > 100000L) {
            try {
               throw new DockingException("Undocking of " + this + " because of docking timout (target " + this.delayedDockUID + " did not appear for 100 seconds)");
            } catch (DockingException var4) {
               var4.printStackTrace();
               ((GameServerState)this.getState()).getController().broadcastMessageAdmin(new Object[]{24, this.segmentController.getName(), this.delayedDockUID}, 3);
               System.err.println("[DOCKING] ERROR: docking target for: " + this + " does not exist: " + this.delayedDockUID + "; unhiding...");
               this.delayedDockUID = null;
               this.segmentController.setHidden(false);
               this.setTriggerMotherShipRemoved(false);
            }
         }

      }
   }

   public void requestDelayedDock(String var1, Vector3i var2, Quat4f var3, int var4) {
      if (this.checkLastDockDelay()) {
         this.delayedDockUID = var1;
         this.delayedDockPos = var2;
         this.delayedDockLocalRot = new Quat4f(var3);
      }

   }

   public void requestDelayedUndock(boolean var1) {
      if (var1 || this.checkLastDockDelay()) {
         System.err.println("[DOCKING]" + this.segmentController.getState() + " REQUEST UNDOCK " + this.segmentController + "; ");
         this.delayedUndock = true;
      }

   }

   public Tag toTagStructure() {
      if (this.delayedDockUID != null) {
         this.tagOverwrite = new DockingTagOverwrite(this.delayedDockUID, this.delayedDockPos, this.delayedDockLocalRot, (byte)0);
      } else {
         Quat4f var1 = new Quat4f(this.targetQuaternion);
         Quat4f var2;
         (var2 = new Quat4f(this.targetStartQuaternion)).inverse();
         var1.mul(var2);
         this.localDockingOrientation.x = var1.x;
         this.localDockingOrientation.y = var1.y;
         this.localDockingOrientation.z = var1.z;
         this.localDockingOrientation.w = var1.w;
      }

      Tag var8;
      if (this.tagOverwrite == null) {
         var8 = new Tag(Tag.Type.STRING, (String)null, this.isDocked() ? this.getDockedOn().to.getSegment().getSegmentController().getUniqueIdentifier() : "NONE");
      } else {
         var8 = new Tag(Tag.Type.STRING, (String)null, this.tagOverwrite.dockTo);
      }

      ((String)var8.getValue()).equals("NONE");
      Tag var9;
      if (this.tagOverwrite == null) {
         var9 = new Tag(Tag.Type.VECTOR3i, (String)null, this.isDocked() ? this.getDockedOn().to.getAbsolutePos(new Vector3i()) : new Vector3i());
      } else {
         var9 = new Tag(Tag.Type.VECTOR3i, (String)null, this.tagOverwrite.pos);
      }

      Tag var3 = new Tag(Tag.Type.BYTE, (String)null, (byte)0);
      Tag var4 = new Tag(Tag.Type.BYTE, (String)null, (byte)0);
      Tag var5 = new Tag(Tag.Type.VECTOR3f, "s", this.size);
      Tag var6;
      if (this.tagOverwrite == null) {
         var6 = new Tag(Tag.Type.BYTE, (String)null, (byte)0);
      } else {
         var6 = new Tag(Tag.Type.BYTE, (String)null, (byte)0);
      }

      Tag var7;
      if (this.tagOverwrite == null) {
         var7 = new Tag(Tag.Type.VECTOR4f, (String)null, new Vector4f(this.localDockingOrientation));
      } else {
         var7 = new Tag(Tag.Type.VECTOR4f, (String)null, new Vector4f(this.tagOverwrite.rot));
      }

      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var8, var9, var3, var4, var5, var6, var7, FinishTag.INST});
   }

   private void undock() {
      if (this.isDocked()) {
         this.updateOnce = false;
         ElementDocking var1;
         SegmentController var2 = (var1 = this.getDockedOn()).to.getSegment().getSegmentController();
         if (var1.to.getType() != 0 && !ElementKeyMap.getInfo(var1.to.getType()).isOldDockable()) {
            assert false : var1.to.getType();
         } else {
            System.err.println("[DOCKING] NOW UNDOCKING: " + this.segmentController + "; " + this.segmentController.getState() + "; DOCKED TO TYPE: " + var1.to.getType() + "; curpos: " + this.segmentController.getWorldTransform().origin);
            SegmentController var3 = var1.to.getSegment().getSegmentController();
            Vector3f var4 = new Vector3f();
            if (var1.to.getSegment().getSegmentController().getPhysicsDataContainer().getObject() != null) {
               ((RigidBody)var3.getPhysicsDataContainer().getObject()).getLinearVelocity(var4);
            } else {
               SegmentController var5 = var1.to.getSegment().getSegmentController().getDockingController().getAbsoluteMother();

               assert var5.getPhysicsDataContainer().getObject() != null : var5;

               ((RigidBody)var5.getPhysicsDataContainer().getObject()).getLinearVelocity(var4);
            }

            if (!var3.getDockingController().getDockedOnThis().remove(var1)) {
               System.err.println("Exception: WARNING! UNDOCK UNSUCCESSFULL " + var1 + ": " + var3.getDockingController().getDockedOnThis());
            }

            this.setDockedOn((ElementDocking)null);
            if (var1.to.getSegment().getSegmentController().getPhysicsDataContainer().getObject() != null) {
               updateTargetWithRemovedController(var3, this.segmentController);
            } else {
               updateTargetWithRemovedController(var1.to.getSegment().getSegmentController().getDockingController().getAbsoluteMother(), this.segmentController);
            }

            if (this.isOnServer()) {
               this.segmentController.vServerAttachment.clear();
            }

            CubesCompoundShape var8;
            if (this.segmentController.getPhysicsDataContainer().lastCenter.length() > 0.0F && (var8 = (CubesCompoundShape)this.segmentController.getPhysicsDataContainer().getShape()) != null) {
               Vector3f var11 = var8.getCenterOfMass();
               this.segmentController.getWorldTransform().basis.transform(var11);
               this.segmentController.getWorldTransform().origin.add(var11);
            }

            this.segmentController.getPhysicsDataContainer().setObject((CollisionObject)null);
            this.segmentController.getPhysicsDataContainer().setShape((CollisionShape)null);
            this.segmentController.getPhysicsDataContainer().setShapeChield((CompoundShapeChild)null, -1);
            this.segmentController.getPhysicsDataContainer().initialTransform.set(this.segmentController.getWorldTransform());
            this.segmentController.getRemoteTransformable().getInitialTransform().set(this.segmentController.getWorldTransform());
            this.segmentController.initPhysics();
            this.segmentController.onPhysicsAdd();
            if (this.segmentController.getDockingController().getDockedOnThis().size() > 0) {
               ArrayList var9 = new ArrayList();
               Iterator var12 = this.segmentController.getDockingController().getDockedOnThis().iterator();

               ElementDocking var6;
               while(var12.hasNext()) {
                  var6 = (ElementDocking)var12.next();
                  var9.add(var6);
               }

               var12 = var9.iterator();

               while(var12.hasNext()) {
                  ((ElementDocking)var12.next()).from.getSegment().getSegmentController().getDockingController().undock();
               }

               var12 = var9.iterator();

               while(var12.hasNext()) {
                  var6 = (ElementDocking)var12.next();

                  try {
                     var6.from.getSegment().getSegmentController().getDockingController().dock(var6.from, var6.to, this.isPublicException(var6.to), true);
                  } catch (DockingNotYetUpdatedException var7) {
                     var7.printStackTrace();
                  }
               }
            }

            ((RigidBody)this.segmentController.getPhysicsDataContainer().getObject()).setLinearVelocity(var4);
            this.segmentController.getRemoteTransformable().receivedTransformation.linearVelocity.set(var4);
            this.segmentController.getPhysicsDataContainer().updatePhysical(this.getState().getUpdateTime());
            this.segmentController.setFlagPhysicsInit(false);
            this.size.set(0.0F, 0.0F, 0.0F);
            if (this.segmentController instanceof SendableSegmentController) {
               ((SendableSegmentController)this.segmentController).handleNTDockChanged();
            }

            if (this.segmentController.getElementClassCountMap().get((short)291) == 0 && this.segmentController.getFactionId() > 0) {
               System.err.println("[DOCKING] resetting faction ID from " + this.segmentController.getFactionId());
               this.segmentController.setFactionId(0);
            }

            if (this.isOnServer()) {
               SendableSegmentController var10 = (SendableSegmentController)var1.to.getSegment().getSegmentController();
               Vector3i var13 = var1.to.getAbsolutePos(new Vector3i());
               System.err.println("[DOCK] ACTIVATE SURROUND SERVER WITH UNDOCK " + var13);
               var10.activateSurroundServer(false, var13, 405);
            }
         }

         this.lastDockingOrUndocking = System.currentTimeMillis();
         this.lastUnDocking = System.currentTimeMillis();
         this.localDockingOrientation.set(0.0F, 0.0F, 0.0F, 1.0F);
         this.dockingCode = -1;
         Starter.modManager.onSegmentControllerUndocking(this.segmentController);
         this.segmentController.flagupdateMass();
         var2.flagupdateMass();
         if (!this.isDocked()) {
            ((RigidBodySegmentController)this.segmentController.getPhysicsDataContainer().getObject()).undockingProtection = var2;
         }
      }

   }

   public void onSmootherSet(Transform var1) {
      if (!this.getDockedOnThis().isEmpty()) {
         Iterator var2 = this.getDockedOnThis().iterator();

         while(var2.hasNext()) {
            ElementDocking var3;
            if ((var3 = (ElementDocking)var2.next()) == null || var3.from == null) {
               assert false;

               throw new NullPointerException("Invalid docking: " + var3);
            }

            SegmentController var4 = var3.from.getSegment().getSegmentController();
            if (!this.isOnServer() && this.segmentController.clientVirtualObject != null) {
               assert this.segmentController.getWorldTransform().equals(var1) : "Should be equal since we just set it";

               var4.getPhysicsDataContainer().updateManuallyWithChildTrans(this.segmentController.getWorldTransform(), (CompoundShape)this.segmentController.getDockingController().getRoot().getPhysicsDataContainer().getShape());
               var4.getDockingController().updateOnce = true;
            }
         }
      }

   }

   public SegmentController getRoot() {
      return this.isDocked() ? this.dockedOn.to.getSegmentController().getDockingController().getRoot() : this.segmentController;
   }

   private void updateDockedOnThis(Timer var1, SegmentController var2) {
      if (var2.getPhysicsDataContainer().isInitialized()) {
         CollisionObject var3;
         if (var2.getPhysicsDataContainer().getObject() == null) {
            var2 = this.getDockedOn().to.getSegment().getSegmentController();
            if ((var3 = this.getDockedOn().to.getSegment().getSegmentController().getDockingController().getAbsoluteMother().getPhysicsDataContainer().getObject()) == null) {
               System.err.println("[DOCKING]" + var2.getState() + " Exception: cannot update docking position from chain. no mother for " + this.segmentController + " -> " + var2 + "; mother has no physics object " + this.getDockedOn().to.getSegment().getSegmentController());
               return;
            }
         } else if ((var3 = var2.getPhysicsDataContainer().getObject()) == null) {
            System.err.println("[DOCKING]" + var2.getState() + " Exception: cannot update docking position. no mother for " + this.segmentController + " -> " + var2 + "; mother has no physics object " + var2);
            return;
         }

         assert var3 != null : var2;

         ElementDocking var4 = null;
         Iterator var5 = this.getDockedOnThis().iterator();

         while(true) {
            if (!var5.hasNext()) {
               if (var4 != null) {
                  this.getDockedOnThis().remove(var4);
               }

               ((CompoundShape)var3.getCollisionShape()).recalculateLocalAabb();
               break;
            }

            ElementDocking var6;
            if ((var6 = (ElementDocking)var5.next()) == null || var6.from == null) {
               assert false;

               throw new RuntimeException("Invalid docking: " + var6);
            }

            SegmentController var7;
            if ((var7 = var6.from.getSegment().getSegmentController()).getPhysicsDataContainer().isInitialized()) {
               if (!this.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().containsKey(var7.getId())) {
                  System.err.println("[DOCKING] UPDATING " + var2 + " MASS BECAUSE DOCKED SHIP DOESNT EXIST ANYMORE: " + var7);
                  updateTargetWithRemovedController(var2, var7);
                  var4 = var6;
                  if (this.isOnServer()) {
                     ((SendableSegmentController)var2).activateSurroundServer(false, var6.to.getAbsolutePos(new Vector3i()), 405);
                  }
               } else {
                  var7.getDockingController().updateTransform(var1);
               }
            }

            if (!this.isOnServer()) {
               CollisionObject var10000 = var2.clientVirtualObject;
            }

            var7.getDockingController().updateOnce = true;

            assert var3 instanceof RigidBody;
         }
      }

   }

   private void updateDocking(Timer var1) {
      if (this.segmentController.getPhysicsDataContainer().isInitialized() && this.segmentController.getSegmentBuffer().getBoundingBox().isInitialized() && var1.currentTime - this.delayCheck >= 1000L) {
         if (this.isOnServer() && this.delayedDockUID != null && this.isTriggerMotherShipRemoved()) {
            System.err.println("[DOCKING] docking failed (mothership removed): UNHIDE SEGMENTCONTROLLER: " + this.segmentController);
            this.segmentController.setHidden(false);
            this.setTriggerMotherShipRemoved(false);
         }

         if (this.delayedDockUID != null) {
            try {
               this.onExistsDelayedDock();
            } catch (DockingNotYetUpdatedException var2) {
               System.err.println("[DOCK] " + this.segmentController + " Waiting to dock for chain to be initialized");
            }
         }

         if (this.delayedUndock) {
            if (this.isOnServer()) {
               System.err.println("[DOCKING] UNDOCKING: SET HIDDEN FALSE ON SERVER FOR " + this);
               this.segmentController.setHidden(false);
               this.segmentController.getNetworkObject().hidden.setChanged(true);
            }

            this.undock();
            this.delayedUndock = false;
         }

         if (!this.getDockedOnThis().isEmpty()) {
            this.updateDockedOnThis(var1, this.segmentController);
         }

         if (this.isDocked() && !this.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().containsKey(this.getDockedOn().to.getSegment().getSegmentController().getId())) {
            System.err.println("[DOCKING] undocking this " + this.segmentController + " because mothership is deleted: " + this.getDockedOn().to.getSegment().getSegmentController());
            if (this.isOnServer()) {
               System.err.println("[DOCKING] UNDOCKING: SET HIDDEN FALSE ON SERVER FOR " + this);
               this.segmentController.setHidden(false);
               this.segmentController.getNetworkObject().hidden.setChanged(true);
            }

            this.undock();
         }

      }
   }

   public void updateDockingPosition() {
      this.getDockedOn().to.getTransform(this.getDockingPos());
      switch(this.getDockedOn().to.getOrientation()) {
      case 0:
         GlUtil.getForwardVector(this.mod, this.getDockingPos());
         break;
      case 1:
         GlUtil.getBackVector(this.mod, this.getDockingPos());
         break;
      case 2:
         GlUtil.getUpVector(this.mod, this.getDockingPos());
         break;
      case 3:
         GlUtil.getBottomVector(this.mod, this.getDockingPos());
         break;
      case 4:
         GlUtil.getLeftVector(this.mod, this.getDockingPos());
         break;
      case 5:
         GlUtil.getRightVector(this.mod, this.getDockingPos());
      }

      BoundingBox var1 = this.getDockedOn().from.getSegment().getSegmentController().getSegmentBuffer().getBoundingBox();
      Vector3f var2;
      (var2 = new Vector3f()).sub(var1.max, var1.min);
      if (isTurretDocking(this.getDockedOn().to)) {
         this.mod.scale(4.5F);
      } else {
         this.mod.scale(var2.y / 2.0F);
      }

      this.getDockingPos().origin.add(this.mod);
      this.getDockingPosInverse().set(this.getDockingPos());
      this.getDockingPosInverse().inverse();
   }

   public void updateFromNetworkObject(NetworkSegmentController var1) {
      if (this.isOnServer()) {
         if (var1.dockClientUndockRequests.getReceiveBuffer().size() > 0) {
            this.requestDelayedUndock(true);
         }

      } else {
         var1.dockingSize.getVector(this.size);
         var1.dockingOrientation.getVector(this.localDockingOrientation);
         if (!this.segmentController.isOnServer() && (!(this.segmentController instanceof PlayerControllable) || ((PlayerControllable)this.segmentController).getAttachedPlayers().isEmpty()) && (!(this.segmentController instanceof AiInterface) || !((AiInterface)this.segmentController).getAiConfiguration().isActiveAI())) {
            this.segmentController.getNetworkObject().dockingTrans.getVector(this.targetQuaternion);
         }

         String var2 = (String)var1.dockedTo.get();
         boolean var3 = !this.isDocked() && !var2.equals("NONE");
         boolean var4 = this.isDocked() && !var2.equals("NONE") && !var2.equals(this.getDockedOn().to.getSegment().getSegmentController().getUniqueIdentifier());
         boolean var5 = this.isDocked() && var2.equals("NONE");
         boolean var6 = this.delayedDockUID != null && !this.isDocked() && var2.equals("NONE");
         if (var3 || var4) {
            Vector4i var7 = var1.dockedElement.getVector();
            if (this.isOnServer()) {
               this.segmentController.setHidden(true);
            }

            this.requestDelayedDock(var2, new Vector3i(var7.x, var7.y, var7.z), new Quat4f(this.localDockingOrientation.x, this.localDockingOrientation.y, this.localDockingOrientation.z, this.localDockingOrientation.w), var7.w);
         }

         if (var5 && !this.delayedUndock) {
            System.err.println(this.getState() + " [DOCKING] UNDOCK REQUEST FROM SERVER (delayedRequest) " + this.segmentController);
            this.requestDelayedUndock(true);
         }

         if (var6) {
            this.delayedDockUID = null;
         }

      }
   }

   public void updateLocal(Timer var1) {
      if (this.isDocked() || this.getDockedOnThis().size() > 0) {
         this.segmentController.getPhysicsDataContainer().oldDockingUpdateWithoutPhysicsObjectInterface = this;
      }

      this.updateDocking(var1);
   }

   private void updateTransform(Timer var1) {
      if (this.isDocked()) {
         this.updateDockingPosition();
         if (isTurretDocking(this.getDockedOn().to)) {
            this.udpateTurretTransform(var1);
         }
      }

   }

   private void udpateTurretTransform(Timer var1) {
      Transform var2;
      Quat4fTools.set((var2 = this.segmentController.getPhysicsDataContainer().getShapeChild().transform).basis, this.fromTmp);
      this.fromTmp.normalize();
      this.targetQuaternion.normalize();
      if (this.targetQuaternion.w != 0.0F) {
         if (this.fromTmp.w == 0.0F) {
            this.res.set(this.targetQuaternion);
         } else {
            Quat4Util.slerp(this.fromTmp, this.targetQuaternion, Math.max(0.001F, Math.min(1.0F, var1.getDelta() * 50.0F)), this.res);
            this.res.normalize();
         }

         var2.basis.set(this.res);
      }

      if (this.segmentController.isOnServer()) {
         Vector4f var3 = new Vector4f(this.targetQuaternion.x, this.targetQuaternion.y, this.targetQuaternion.z, this.targetQuaternion.w);
         if (!this.segmentController.getNetworkObject().dockingTrans.getVector().epsilonEquals(var3, 0.01F)) {
            this.segmentController.getNetworkObject().dockingTrans.set(var3);
         }
      }

   }

   public void updateWithoutPhysicsObject() {
      if (this.isDocked()) {
         SegmentController var1;
         for(var1 = this.getDockedOn().to.getSegment().getSegmentController(); var1.getDockingController().isDocked(); var1 = var1.getDockingController().getDockedOn().to.getSegment().getSegmentController()) {
         }

         this.segmentController.getPhysicsDataContainer().updateManuallyWithChildTransOld(var1.getWorldTransform());
      }

   }

   public void checkRootIntegrity() {
   }

   public boolean isInAnyDockingRelation(SegmentController var1) {
      boolean var2 = false;
      if (this.isDocked() && var1.getDockingController().isDocked()) {
         return this.getLocalMother() == var1.getDockingController().getLocalMother();
      } else {
         if (this.isDocked()) {
            var2 = this.getDockedOn().to.getSegment().getSegmentController().getDockingController().isInAnyDockingRelation(var1);
         }

         return var2 || this.isDockedTo(var1) || var1.getDockingController().isDockedTo(this.segmentController);
      }
   }

   public boolean isDockedTo(SegmentController var1) {
      if (this.isDocked()) {
         if (this.getDockedOn().to.getSegment().getSegmentController() == var1) {
            return true;
         }

         if (this.getDockedOn().to.getSegment().getSegmentController().getDockingController().isDocked() && this.getDockedOn().to.getSegment().getSegmentController().getDockingController().getDockedOn().to.getSegment().getSegmentController() == var1) {
            return true;
         }
      }

      return false;
   }

   public void setFactionAll(int var1) {
      this.segmentController.setFactionId(var1);
      Iterator var2 = this.getDockedOnThis().iterator();

      while(var2.hasNext()) {
         ((ElementDocking)var2.next()).from.getSegment().getSegmentController().getDockingController().setFactionAll(var1);
      }

   }

   public long getLastUnDocking() {
      return this.lastUnDocking;
   }

   public boolean isTurretDocking() {
      return this.isDocked() && isTurretDocking(this.getDockedOn().to);
   }

   public boolean isInAnyDockingRelation() {
      return this.isDocked() || !this.dockedOnThis.isEmpty();
   }

   public int getDockingCode() {
      return this.dockingCode;
   }

   public void setDockingCode(int var1) {
      this.dockingCode = var1;
   }
}
