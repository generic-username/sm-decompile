package org.schema.game.common.controller;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.IntCollection;
import java.util.Set;
import org.schema.game.common.controller.trade.TradeNode;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.player.inventory.NoSlotFreeException;
import org.schema.game.common.data.player.inventory.ShopInventory;
import org.schema.game.network.objects.TradePriceInterface;
import org.schema.schine.network.StateInterface;

public interface ShopInterface {
   void fillInventory(boolean var1, boolean var2) throws NoSlotFreeException;

   long getCredits();

   ShopNetworkInterface getNetworkObject();

   int getPriceString(ElementInformation var1, boolean var2);

   long getPermissionToPurchase();

   long getPermissionToTrade();

   TradePriceInterface getPrice(short var1, boolean var2);

   int getSectorId();

   SegmentBufferInterface getSegmentBuffer();

   ShopInventory getShopInventory();

   Set getShopOwners();

   ShoppingAddOn getShoppingAddOn();

   StateInterface getState();

   Transform getWorldTransform();

   boolean isOnServer();

   void modCredits(long var1);

   void sendInventoryModification(IntCollection var1, long var2);

   void sendInventoryModification(int var1, long var2);

   int getFactionId();

   boolean isInfiniteSupply();

   boolean isAiShop();

   boolean isTradeNode();

   TradeNode getTradeNode();

   SegmentController getSegmentController();

   boolean isValidShop();

   boolean isNPCHomeBase();

   boolean wasValidTradeNode();
}
