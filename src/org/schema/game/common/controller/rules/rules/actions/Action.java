package org.schema.game.common.controller.rules.rules.actions;

import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.RuleFieldValue;
import org.schema.game.common.controller.rules.rules.conditions.RuleFieldValueInterface;
import org.schema.game.common.data.world.RuleEntityContainer;
import org.schema.game.common.util.FieldUtils;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.TopLevelType;
import org.schema.schine.network.XMLSerializationInterface;
import org.w3c.dom.Attr;
import org.w3c.dom.Comment;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class Action implements RuleFieldValueInterface, SerialializationInterface, XMLSerializationInterface {
   private static byte VERSION = 0;

   public TopLevelType getEntityType() {
      return this.getType().getType();
   }

   public abstract ActionTypes getType();

   private Node createConditionElement(Document var1, Node var2) {
      Element var4 = var1.createElement("Action");
      Attr var3;
      (var3 = var1.createAttribute("type")).setValue(String.valueOf(this.getType().UID));
      var4.getAttributes().setNamedItem(var3);
      (var3 = var1.createAttribute("version")).setValue(String.valueOf(VERSION));
      var4.getAttributes().setNamedItem(var3);
      Comment var5 = var1.createComment(this.getType().name());
      var4.appendChild(var5);
      return var4;
   }

   public abstract void onTrigger(Object var1);

   public abstract void onUntrigger(Object var1);

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      List var4;
      Collections.sort(var4 = FieldUtils.getAllFields(new ObjectArrayList(), this.getClass()), new Comparator() {
         public int compare(Field var1, Field var2) {
            RuleValue var3 = (RuleValue)var1.getAnnotation(RuleValue.class);
            RuleValue var4 = (RuleValue)var2.getAnnotation(RuleValue.class);
            if (var3 == null && var4 == null) {
               return 0;
            } else if (var3 == null && var4 != null) {
               return -1;
            } else {
               return var3 != null && var4 == null ? 1 : var3.tag().compareTo(var3.tag());
            }
         }
      });
      Iterator var9 = var4.iterator();

      while(var9.hasNext()) {
         Field var5;
         (var5 = (Field)var9.next()).setAccessible(true);
         if ((RuleValue)var5.getAnnotation(RuleValue.class) != null) {
            try {
               if (var5.getType() == Boolean.TYPE) {
                  var5.setBoolean(this, var1.readBoolean());
               } else if (var5.getType() == Float.TYPE) {
                  var5.setFloat(this, var1.readFloat());
               } else if (var5.getType() == Long.TYPE) {
                  var5.setLong(this, var1.readLong());
               } else if (var5.getType() == Short.TYPE) {
                  var5.setShort(this, var1.readShort());
               } else if (var5.getType() == Integer.TYPE) {
                  var5.setInt(this, var1.readInt());
               } else if (var5.getType() == Byte.TYPE) {
                  var5.setByte(this, var1.readByte());
               } else if (var5.getType() == Double.TYPE) {
                  var5.setDouble(this, var1.readDouble());
               } else if (SerialializationInterface.class.isAssignableFrom(var5.getType())) {
                  ((SerialializationInterface)var5.get(this)).deserialize(var1, var2, var3);
               } else {
                  var5.set(this, var1.readUTF());
               }
            } catch (DOMException var6) {
               var6.printStackTrace();
            } catch (IllegalArgumentException var7) {
               var7.printStackTrace();
            } catch (IllegalAccessException var8) {
               var8.printStackTrace();
            }
         }
      }

   }

   public Action duplicate() throws IOException {
      Action var1 = this.getType().fac.instantiateAction();
      FastByteArrayOutputStream var2 = new FastByteArrayOutputStream(10240);
      DataOutputStream var3 = new DataOutputStream(var2);
      this.serialize(var3, true);
      DataInputStream var4 = new DataInputStream(new FastByteArrayInputStream(var2.array, 0, (int)var2.position()));
      var1.deserialize(var4, 0, true);
      return var1;
   }

   public List createFieldValues() {
      return RuleFieldValue.create(this);
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeByte((byte)this.getType().UID);
      List var3;
      Collections.sort(var3 = FieldUtils.getAllFields(new ObjectArrayList(), this.getClass()), new Comparator() {
         public int compare(Field var1, Field var2) {
            RuleValue var3 = (RuleValue)var1.getAnnotation(RuleValue.class);
            RuleValue var4 = (RuleValue)var2.getAnnotation(RuleValue.class);
            if (var3 == null && var4 == null) {
               return 0;
            } else if (var3 == null && var4 != null) {
               return -1;
            } else {
               return var3 != null && var4 == null ? 1 : var3.tag().compareTo(var3.tag());
            }
         }
      });
      Iterator var8 = var3.iterator();

      while(var8.hasNext()) {
         Field var4;
         (var4 = (Field)var8.next()).setAccessible(true);
         if ((RuleValue)var4.getAnnotation(RuleValue.class) != null) {
            try {
               if (var4.getType() == Boolean.TYPE) {
                  var1.writeBoolean(var4.getBoolean(this));
               } else if (var4.getType() == Float.TYPE) {
                  var1.writeFloat(var4.getFloat(this));
               } else if (var4.getType() == Long.TYPE) {
                  var1.writeLong(var4.getLong(this));
               } else if (var4.getType() == Short.TYPE) {
                  var1.writeShort(var4.getShort(this));
               } else if (var4.getType() == Integer.TYPE) {
                  var1.writeInt(var4.getInt(this));
               } else if (var4.getType() == Byte.TYPE) {
                  var1.writeByte(var4.getByte(this));
               } else if (var4.getType() == Double.TYPE) {
                  var1.writeDouble(var4.getDouble(this));
               } else if (SerialializationInterface.class.isAssignableFrom(var4.getType())) {
                  ((SerialializationInterface)var4.get(this)).serialize(var1, var2);
               } else {
                  var1.writeUTF(var4.get(this).toString());
               }
            } catch (DOMException var5) {
               var5.printStackTrace();
            } catch (IllegalArgumentException var6) {
               var6.printStackTrace();
            } catch (IllegalAccessException var7) {
               var7.printStackTrace();
            }
         }
      }

   }

   public void parseXML(Node var1) {
      assert var1.getAttributes().getNamedItem("type") != null;

      NodeList var11 = var1.getChildNodes();
      List var2 = FieldUtils.getAllFields(new ObjectArrayList(), this.getClass());

      label82:
      for(int var3 = 0; var3 < var11.getLength(); ++var3) {
         Node var4 = var11.item(var3);
         Iterator var5 = var2.iterator();

         while(var5.hasNext()) {
            Field var6;
            (var6 = (Field)var5.next()).setAccessible(true);
            RuleValue var7;
            if ((var7 = (RuleValue)var6.getAnnotation(RuleValue.class)) != null && var7.tag().toLowerCase(Locale.ENGLISH).equals(var4.getNodeName().toLowerCase(Locale.ENGLISH))) {
               try {
                  if (var6.getType() == Boolean.TYPE) {
                     var6.setBoolean(this, Boolean.parseBoolean(var4.getTextContent()));
                     break;
                  }

                  if (var6.getType() == Float.TYPE) {
                     var6.setFloat(this, Float.parseFloat(var4.getTextContent()));
                     break;
                  }

                  if (var6.getType() == Long.TYPE) {
                     var6.setLong(this, Long.parseLong(var4.getTextContent()));
                     break;
                  }

                  if (var6.getType() == Short.TYPE) {
                     var6.setShort(this, Short.parseShort(var4.getTextContent()));
                     break;
                  }

                  if (var6.getType() == Integer.TYPE) {
                     var6.setInt(this, Integer.parseInt(var4.getTextContent()));
                     break;
                  }

                  if (var6.getType() == Byte.TYPE) {
                     var6.setByte(this, Byte.parseByte(var4.getTextContent()));
                     break;
                  }

                  if (var6.getType() == Double.TYPE) {
                     var6.setDouble(this, Double.parseDouble(var4.getTextContent()));
                     break;
                  }

                  if (ActionList.class.isAssignableFrom(var6.getType())) {
                     NodeList var12 = var4.getChildNodes();
                     int var13 = 0;

                     while(true) {
                        if (var13 >= var12.getLength()) {
                           continue label82;
                        }

                        Node var14;
                        if (ActionList.isActionListNode(var14 = var12.item(var13))) {
                           ((ActionList)var6.get(this)).parseXML(var14);
                           continue label82;
                        }

                        ++var13;
                     }
                  }

                  var6.set(this, var4.getTextContent());
               } catch (DOMException var8) {
                  var8.printStackTrace();
               } catch (IllegalArgumentException var9) {
                  var9.printStackTrace();
               } catch (IllegalAccessException var10) {
                  var10.printStackTrace();
               }
               break;
            }
         }
      }

   }

   public Node writeXML(Document var1, Node var2) {
      var2 = this.createConditionElement(var1, var2);
      Iterator var3 = FieldUtils.getAllFields(new ObjectArrayList(), this.getClass()).iterator();

      while(var3.hasNext()) {
         Field var4;
         (var4 = (Field)var3.next()).setAccessible(true);
         RuleValue var5;
         if ((var5 = (RuleValue)var4.getAnnotation(RuleValue.class)) != null) {
            Element var9 = var1.createElement(var5.tag());

            try {
               if (var4.getType() == Boolean.TYPE) {
                  var9.setTextContent(String.valueOf(var4.getBoolean(this)));
               } else if (var4.getType() == Float.TYPE) {
                  var9.setTextContent(String.valueOf(var4.getFloat(this)));
               } else if (var4.getType() == Long.TYPE) {
                  var9.setTextContent(String.valueOf(var4.getLong(this)));
               } else if (var4.getType() == Short.TYPE) {
                  var9.setTextContent(String.valueOf(var4.getShort(this)));
               } else if (var4.getType() == Integer.TYPE) {
                  var9.setTextContent(String.valueOf(var4.getInt(this)));
               } else if (var4.getType() == Byte.TYPE) {
                  var9.setTextContent(String.valueOf(var4.getByte(this)));
               } else if (var4.getType() == Double.TYPE) {
                  var9.setTextContent(String.valueOf(var4.getDouble(this)));
               } else if (ActionList.class.isAssignableFrom(var4.getType())) {
                  var9.appendChild(((ActionList)var4.get(this)).writeXML(var1, var9));
               } else {
                  var9.setTextContent(String.valueOf(var4.get(this).toString()));
               }
            } catch (DOMException var6) {
               var6.printStackTrace();
            } catch (IllegalArgumentException var7) {
               var7.printStackTrace();
            } catch (IllegalAccessException var8) {
               var8.printStackTrace();
            }

            var2.appendChild(var9);
         }
      }

      return var2;
   }

   public abstract String getDescriptionShort();

   public abstract void onTrigger(RuleEntityContainer var1, TopLevelType var2);

   public abstract void onUntrigger(RuleEntityContainer var1, TopLevelType var2);
}
