package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerInFleetCondition extends SegmentControllerCondition {
   public long getTrigger() {
      return 2097152L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_IS_IN_FLEET;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      return var6 ? true : var3.isInFleet();
   }

   public String getDescriptionShort() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERINFLEETCONDITION_0;
   }
}
