package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerAlwaysTrueCondition extends SegmentControllerCondition {
   @RuleValue(
      tag = "AlwaysTrue"
   )
   public boolean alwaysTrue = true;

   public long getTrigger() {
      return 1L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_ALWAYS_TRUE_CONDITION;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      return var6 ? true : this.alwaysTrue;
   }

   public String getDescriptionShort() {
      return this.alwaysTrue ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERALWAYSTRUECONDITION_0 : Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERALWAYSTRUECONDITION_1;
   }
}
