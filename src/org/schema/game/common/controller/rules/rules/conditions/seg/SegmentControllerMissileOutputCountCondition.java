package org.schema.game.common.controller.rules.rules.conditions.seg;

import java.util.Iterator;
import org.schema.game.common.controller.elements.WeaponManagerInterface;
import org.schema.game.common.controller.elements.missile.dumb.DumbMissileCollectionManager;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.ManagedSegmentController;

public class SegmentControllerMissileOutputCountCondition extends SegmentControllerAbstractOutputCountCondition {
   public ConditionTypes getType() {
      return ConditionTypes.SEG_OUTPUTS_PER_MISSILE;
   }

   public double getOutputCount(ManagedSegmentController var1) {
      int var2 = 0;
      if (var1.getManagerContainer() instanceof WeaponManagerInterface) {
         for(Iterator var3 = ((WeaponManagerInterface)var1.getManagerContainer()).getMissile().getCollectionManagers().iterator(); var3.hasNext(); var2 = Math.max(((DumbMissileCollectionManager)var3.next()).getElementCollections().size(), var2)) {
         }
      }

      return (double)var2;
   }

   public String getQuantifierString() {
      return "Missile Output(s)";
   }
}
