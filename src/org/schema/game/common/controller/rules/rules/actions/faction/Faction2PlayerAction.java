package org.schema.game.common.controller.rules.rules.actions.faction;

import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.controller.rules.rules.actions.player.PlayerActionList;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

public class Faction2PlayerAction extends FactionAction {
   @RuleValue(
      tag = "Actions"
   )
   public PlayerActionList actions = new PlayerActionList();

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_FACTION_FACTION2PLAYERACTION_0, this.actions.size());
   }

   public void onTrigger(Faction var1) {
      if (var1.isOnServer()) {
         GameServerState var2 = (GameServerState)var1.getState();
         Iterator var4 = var1.getMembersUID().keySet().iterator();

         while(var4.hasNext()) {
            String var3 = (String)var4.next();
            PlayerState var5;
            if ((var5 = var2.getPlayerFromNameIgnoreCaseWOException(var3)) != null) {
               this.actions.onTrigger(var5);
            }
         }
      }

   }

   public void onUntrigger(Faction var1) {
      if (var1.isOnServer()) {
         GameServerState var2 = (GameServerState)var1.getState();
         Iterator var4 = var1.getMembersUID().keySet().iterator();

         while(var4.hasNext()) {
            String var3 = (String)var4.next();
            PlayerState var5;
            if ((var5 = var2.getPlayerFromNameIgnoreCaseWOException(var3)) != null) {
               this.actions.onUntrigger(var5);
            }
         }
      }

   }

   public ActionTypes getType() {
      return ActionTypes.FACTION_2_PLAYERS;
   }
}
