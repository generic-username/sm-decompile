package org.schema.game.common.controller.rules.rules.actions.player;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.controller.rules.rules.actions.sector.SectorActionList;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Sector;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

public class Player2SectorAction extends PlayerAction {
   @RuleValue(
      tag = "Actions"
   )
   public SectorActionList actions = new SectorActionList();

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_PLAYER_PLAYER2SECTORACTION_0, this.actions.size());
   }

   public void onTrigger(PlayerState var1) {
      Sector var2;
      if (var1.isOnServer() && (var2 = ((GameServerState)var1.getState()).getUniverse().getSector(var1.getCurrentSectorId())) != null) {
         this.actions.onTrigger(var2.getRemoteSector());
      }

   }

   public void onUntrigger(PlayerState var1) {
      Sector var2;
      if (var1.isOnServer() && (var2 = ((GameServerState)var1.getState()).getUniverse().getSector(var1.getCurrentSectorId())) != null) {
         this.actions.onUntrigger(var2.getRemoteSector());
      }

   }

   public ActionTypes getType() {
      return ActionTypes.PLAYER_2_SECTOR;
   }
}
