package org.schema.game.common.controller.rules.rules.actions.seg;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerPopupMessageAction extends SegmentControllerAction {
   @RuleValue(
      tag = "TriggerMessage"
   )
   public String triggerMessage = "";
   @RuleValue(
      tag = "TriggerMsgType",
      intMap = {0, 1, 2, 3, 4},
      int2StringMap = {"Simple", "Info", "Warning", "Error", "Dialog"}
   )
   public int triggerMsgType;
   @RuleValue(
      tag = "UntriggerMessage"
   )
   public String untriggerMessage = "";
   @RuleValue(
      tag = "UntriggerMsgType",
      intMap = {0, 1, 2, 3, 4},
      int2StringMap = {"Simple", "Info", "Warning", "Error", "Dialog"}
   )
   public int untriggerMsgType;

   public ActionTypes getType() {
      return ActionTypes.SEG_POPUP_MESSAGE;
   }

   public String getDescriptionShort() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLERPOPUPMESSAGEACTION_0;
   }

   public void onTrigger(SegmentController var1) {
      if (var1.isOnServer() && this.triggerMessage.trim().length() > 0) {
         var1.sendServerMessage(this.triggerMessage, this.triggerMsgType);
      }

   }

   public void onUntrigger(SegmentController var1) {
      if (var1.isOnServer() && this.untriggerMessage.trim().length() > 0) {
         var1.sendServerMessage(this.untriggerMessage, this.untriggerMsgType);
      }

   }
}
