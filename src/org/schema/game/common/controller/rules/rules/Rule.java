package org.schema.game.common.controller.rules.rules;

import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleSet;
import org.schema.game.common.controller.rules.RuleSetManager;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.actions.Action;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.controller.rules.rules.conditions.Condition;
import org.schema.game.common.controller.rules.rules.conditions.ConditionList;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.controller.rules.rules.conditions.faction.FactionCondition;
import org.schema.game.common.controller.rules.rules.conditions.player.PlayerCondition;
import org.schema.game.common.controller.rules.rules.conditions.sector.SectorCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerCondition;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.RuleEntityContainer;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.TopLevelType;
import org.schema.schine.network.XMLSerializationInterface;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Rule implements SerialializationInterface, XMLSerializationInterface {
   private int ruleId;
   private static byte VERSION = 0;
   private String uniqueIdentifier;
   protected final ConditionList conditions = new ConditionList();
   protected final List actions = new ObjectArrayList();
   public boolean allTrue;
   private boolean triggered;
   private static int ruleIdGen;
   public TopLevelType ruleType;
   private final RuleStateChange stateChange;
   public boolean ignoreRule;
   public boolean checkIgnored;
   public String ruleSetUID;

   public Rule(boolean var1) {
      this.ruleType = TopLevelType.SEGMENT_CONTROLLER;
      this.stateChange = new RuleStateChange();
      this.ruleSetUID = "undefined";
      if (var1) {
         this.ruleId = ++ruleIdGen;
      }

   }

   public Rule duplicate(String var1, boolean var2) throws IOException {
      Rule var6;
      int var3 = (var6 = new Rule(var2)).ruleId;
      FastByteArrayOutputStream var4 = new FastByteArrayOutputStream(10240);
      DataOutputStream var5 = new DataOutputStream(var4);
      this.serialize(var5, true);
      DataInputStream var7 = new DataInputStream(new FastByteArrayInputStream(var4.array, 0, (int)var4.position()));
      var6.deserialize(var7, 0, true);
      var6.uniqueIdentifier = var1;
      var6.ruleId = var3;
      return var6;
   }

   public void parseXML(Node var1) {
      if (var1.getAttributes().getNamedItem("version") == null) {
         throw new RuleParserException("missing version attribute on rule");
      } else if (var1.getAttributes().getNamedItem("id") == null) {
         throw new RuleParserException("missing id attribute on rule");
      } else if (var1.getAttributes().getNamedItem("alltrue") == null) {
         throw new RuleParserException("missing alltrue attribute on rule");
      } else {
         if (var1.getAttributes().getNamedItem("type") != null) {
            this.ruleType = TopLevelType.values()[Integer.parseInt(var1.getAttributes().getNamedItem("type").getNodeValue())];
         }

         Byte.parseByte(var1.getAttributes().getNamedItem("version").getNodeValue());
         this.setUniqueIdentifier(var1.getAttributes().getNamedItem("id").getNodeValue());
         this.allTrue = Boolean.parseBoolean(var1.getAttributes().getNamedItem("alltrue").getNodeValue());
         this.conditions.clear();
         this.actions.clear();
         NodeList var8 = var1.getChildNodes();

         for(int var2 = 0; var2 < var8.getLength(); ++var2) {
            Node var3;
            if ((var3 = var8.item(var2)).getNodeType() == 1) {
               NodeList var4;
               int var5;
               Node var6;
               Node var7;
               if (var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("conditions")) {
                  var4 = var3.getChildNodes();

                  for(var5 = 0; var5 < var4.getLength(); ++var5) {
                     if ((var6 = var4.item(var5)).getNodeType() == 1) {
                        if ((var7 = var6.getAttributes().getNamedItem("type")) == null) {
                           throw new RuleParserException("No type on condition node ");
                        }

                        Condition var9;
                        (var9 = ConditionTypes.getByUID(Integer.parseInt(var7.getNodeValue())).fac.instantiateCondition()).parseXML(var6);
                        this.conditions.add(var9);
                        if (this.ruleType == null) {
                           this.ruleType = var9.getType().getType();
                        }
                     }
                  }
               }

               if (var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("actions")) {
                  var4 = var3.getChildNodes();

                  for(var5 = 0; var5 < var4.getLength(); ++var5) {
                     if ((var6 = var4.item(var5)).getNodeType() == 1) {
                        if ((var7 = var6.getAttributes().getNamedItem("type")) == null) {
                           throw new RuleParserException("No type on action node ");
                        }

                        Action var10;
                        (var10 = ActionTypes.getByUID(Integer.parseInt(var7.getNodeValue())).fac.instantiateAction()).parseXML(var6);
                        this.actions.add(var10);
                        if (this.ruleType == null) {
                           this.ruleType = var10.getType().getType();
                        }
                     }
                  }
               }
            }
         }

      }
   }

   public int hashCode() {
      return 31 + (this.uniqueIdentifier == null ? 0 : this.uniqueIdentifier.hashCode());
   }

   public boolean equals(Object var1) {
      if (this == var1) {
         return true;
      } else if (var1 == null) {
         return false;
      } else if (!(var1 instanceof Rule)) {
         return false;
      } else {
         Rule var2 = (Rule)var1;
         if (this.uniqueIdentifier == null) {
            if (var2.uniqueIdentifier != null) {
               return false;
            }
         } else if (!this.uniqueIdentifier.equals(var2.uniqueIdentifier)) {
            return false;
         }

         return true;
      }
   }

   public Node writeXML(Document var1, Node var2) {
      Element var6 = var1.createElement("Rule");
      Attr var3;
      (var3 = var1.createAttribute("version")).setValue(String.valueOf(VERSION));
      var6.getAttributes().setNamedItem(var3);
      (var3 = var1.createAttribute("id")).setValue(this.getUniqueIdentifier());
      var6.getAttributes().setNamedItem(var3);
      (var3 = var1.createAttribute("alltrue")).setValue(String.valueOf(this.allTrue));
      var6.getAttributes().setNamedItem(var3);
      (var3 = var1.createAttribute("type")).setValue(String.valueOf(this.ruleType.ordinal()));
      var6.getAttributes().setNamedItem(var3);
      Element var7 = var1.createElement("Conditions");
      var6.appendChild(var7);
      Iterator var4 = this.conditions.iterator();

      while(var4.hasNext()) {
         Node var5 = ((Condition)var4.next()).writeXML(var1, var6);
         var7.appendChild(var5);
      }

      Element var8 = var1.createElement("Actions");
      var6.appendChild(var8);
      Iterator var10 = this.actions.iterator();

      while(var10.hasNext()) {
         Node var9 = ((Action)var10.next()).writeXML(var1, var6);
         var8.appendChild(var9);
      }

      return var6;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeByte(VERSION);
      var1.writeInt(this.ruleId);
      var1.writeUTF(this.getUniqueIdentifier());
      var1.writeByte((byte)this.ruleType.ordinal());
      var1.writeBoolean(this.allTrue);
      var1.writeInt(this.conditions.size());
      Iterator var3 = this.conditions.iterator();

      while(var3.hasNext()) {
         ((Condition)var3.next()).serialize(var1, var2);
      }

      var1.writeInt(this.actions.size());
      var3 = this.actions.iterator();

      while(var3.hasNext()) {
         ((Action)var3.next()).serialize(var1, var2);
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      var1.readByte();
      this.ruleId = var1.readInt();
      this.setUniqueIdentifier(var1.readUTF());
      this.ruleType = TopLevelType.values()[var1.readByte()];
      this.allTrue = var1.readBoolean();
      int var4 = var1.readInt();
      this.conditions.clear();

      int var5;
      for(var5 = 0; var5 < var4; ++var5) {
         Condition var6;
         (var6 = ConditionTypes.getByUID(var1.readByte()).fac.instantiateCondition()).deserialize(var1, var2, var3);
         this.conditions.add(var6);
      }

      var5 = var1.readInt();
      this.actions.clear();

      for(int var8 = 0; var8 < var5; ++var8) {
         Action var7;
         (var7 = ActionTypes.getByUID(var1.readByte()).fac.instantiateAction()).deserialize(var1, var2, var3);
         this.actions.add(var7);
      }

   }

   public String getUniqueIdentifier() {
      return this.uniqueIdentifier;
   }

   public int getRuleId() {
      return this.ruleId;
   }

   public void checkUID(RuleSetManager var1) {
      String var2 = this.getUniqueIdentifier();

      for(int var3 = 0; var1.ruleUIDlkMap.containsKey(var2.toLowerCase(Locale.ENGLISH)); ++var3) {
         var2 = this.getUniqueIdentifier() + "_" + var3;
      }

      this.setUniqueIdentifier(var2);
   }

   public void assignNewId() {
      this.ruleId = ruleIdGen++;
   }

   public void removeCondition(Condition var1) {
      this.conditions.remove(var1);
   }

   public void addCondition(Condition var1) {
      if ((!this.actions.isEmpty() || !this.conditions.isEmpty()) && this.getEntityType() != var1.getEntityType()) {
         throw new RuntimeException("Incompatible type " + var1.getEntityType().name() + "; Rule: " + this.getEntityType().name());
      } else {
         this.conditions.add(var1);
      }
   }

   public void removeAction(Action var1) {
      this.actions.remove(var1);
   }

   public void addAction(Action var1) {
      if ((!this.actions.isEmpty() || !this.conditions.isEmpty()) && this.getEntityType() != var1.getEntityType()) {
         throw new RuntimeException("Incompatible type " + var1.getEntityType().name() + "; Rule: " + this.getEntityType().name());
      } else {
         this.actions.add(var1);
      }
   }

   public int getConditionCount() {
      return this.conditions.size();
   }

   public int getActionCount() {
      return this.actions.size();
   }

   public List getActions() {
      return this.actions;
   }

   public ConditionList getConditions() {
      return this.conditions;
   }

   public void setUniqueIdentifier(String var1) {
      this.uniqueIdentifier = var1;
   }

   public TopLevelType getEntityType() {
      Iterator var1;
      if ((var1 = this.getConditions().iterator()).hasNext()) {
         return ((Condition)var1.next()).getType().getType();
      } else {
         return (var1 = this.getActions().iterator()).hasNext() ? ((Action)var1.next()).getType().getType() : TopLevelType.GENERAL;
      }
   }

   private void sendReset(RuleEntityContainer var1, TopLevelType var2) {
      this.stateChange.clear();
      this.resetConditions(false);
      if (this.triggered) {
         this.onTrigger(var1, var2, false);
         this.triggered = false;
         this.stateChange.triggerState = RuleStateChange.RuleTriggerState.UNTRIGGERED;
      }

      this.createCompleteConditionState(this.stateChange);
      var1.getRuleEntityManager().sendRuleStateChange(this, this.stateChange);
      this.stateChange.clear();
   }

   public void process(RuleEntityContainer var1, TopLevelType var2, long var3) {
      if (var2 == this.ruleType) {
         if (this.ignoreRule) {
            System.err.println("NOT CHECKING RULE " + this.uniqueIdentifier + "; Ignored!");
            if (!this.checkIgnored) {
               this.sendReset(var1, var2);
               this.checkIgnored = true;
            }

         } else {
            this.checkIgnored = false;
            boolean var5 = false;

            for(short var6 = 0; var6 < this.conditions.size(); ++var6) {
               Condition var7 = (Condition)this.conditions.get(var6);
               boolean var12;
               boolean var13;
               switch(var2) {
               case FACTION:
                  Faction var18 = (Faction)var1;
                  FactionCondition var19;
                  if ((var19 = (FactionCondition)var7).isTriggeredOn(var3)) {
                     var12 = var7.isSatisfied();
                     this.stateChange.lastSatisfied = var12;
                     var13 = var19.checkSatisfied(var6, this.stateChange, var18, var3, false);
                     if (var12 != var13) {
                        var5 = true;
                     }
                  }
                  break;
               case SECTOR:
                  RemoteSector var15 = (RemoteSector)var1;
                  SectorCondition var17;
                  if ((var17 = (SectorCondition)var7).isTriggeredOn(var3)) {
                     var12 = var7.isSatisfied();
                     this.stateChange.lastSatisfied = var12;
                     var13 = var17.checkSatisfied(var6, this.stateChange, var15, var3, false);
                     if (var12 != var13) {
                        var5 = true;
                     }
                  }
                  break;
               case PLAYER:
                  PlayerState var14 = (PlayerState)var1;
                  PlayerCondition var16;
                  if ((var16 = (PlayerCondition)var7).isTriggeredOn(var3)) {
                     var12 = var7.isSatisfied();
                     this.stateChange.lastSatisfied = var12;
                     var13 = var16.checkSatisfied(var6, this.stateChange, var14, var3, false);
                     if (var12 != var13) {
                        var5 = true;
                     }
                  }
                  break;
               case SEGMENT_CONTROLLER:
                  SegmentController var8 = (SegmentController)var1;
                  SegmentControllerCondition var9;
                  if ((var9 = (SegmentControllerCondition)var7).isTriggeredOn(var3)) {
                     var12 = var7.isSatisfied();
                     this.stateChange.lastSatisfied = var12;
                     var13 = var9.checkSatisfied(var6, this.stateChange, var8, var3, false);
                     if (var12 != var13) {
                        var5 = true;
                     }
                  }
                  break;
               default:
                  assert false;
               }
            }

            if (var5) {
               try {
                  boolean var11 = this.triggered;
                  this.triggered = this.checkRuleTriggered();
                  this.stateChange.triggerState = this.triggered ? RuleStateChange.RuleTriggerState.TRIGGERED : RuleStateChange.RuleTriggerState.UNTRIGGERED;
                  if (var11 != this.triggered) {
                     this.onTrigger(var1, var2, this.triggered);
                  }

                  var1.getRuleEntityManager().triggerRuleStateChanged();
               } catch (RuntimeException var10) {
                  var10.printStackTrace();
                  ((GameServerState)var1.getRuleEntityManager().entity.getState()).getController().broadcastMessageAdmin(new Object[]{121, this.uniqueIdentifier}, 3);
               }
            }

            if (this.stateChange.changed()) {
               var1.getRuleEntityManager().sendRuleStateChange(this, this.stateChange);
            }

            this.stateChange.clear();
         }
      }
   }

   public void createCompleteConditionState(RuleStateChange var1) {
      for(short var2 = 0; var2 < this.conditions.size(); ++var2) {
         ((Condition)this.conditions.get(var2)).createStateChange(var2, var1);
      }

   }

   private void resetConditions(boolean var1) {
      Iterator var2 = this.conditions.iterator();

      while(var2.hasNext()) {
         ((Condition)var2.next()).resetCondition(var1);
      }

   }

   private void onTrigger(RuleEntityContainer var1, TopLevelType var2, boolean var3) {
      Iterator var4;
      if (var3) {
         var4 = this.actions.iterator();

         while(var4.hasNext()) {
            ((Action)var4.next()).onTrigger(var1, var2);
         }

      } else {
         var4 = this.actions.iterator();

         while(var4.hasNext()) {
            ((Action)var4.next()).onUntrigger(var1, var2);
         }

      }
   }

   private boolean checkRuleTriggered() {
      Iterator var1;
      if (this.allTrue) {
         var1 = this.conditions.iterator();

         do {
            if (!var1.hasNext()) {
               return true;
            }
         } while(((Condition)var1.next()).isSatisfied());

         return false;
      } else {
         var1 = this.conditions.iterator();

         do {
            if (!var1.hasNext()) {
               return false;
            }
         } while(!((Condition)var1.next()).isSatisfied());

         return true;
      }
   }

   public boolean receiveState(RuleEntityContainer var1, TopLevelType var2, RuleStateChange var3) {
      boolean var4 = false;

      try {
         for(int var5 = 0; var5 < var3.changeLogCond.size(); ++var5) {
            short var6 = var3.changeLogCond.get(var5);
            Condition var7;
            boolean var8 = (var7 = (Condition)this.conditions.get(Math.abs(var6) - 1)).isSatisfied();
            var5 = var7.processReceivedState(var5, var3.changeLogCond, var6);
            if (var7.isSatisfied() != var8) {
               var4 = true;
            }
         }

         if (var3.triggerState != RuleStateChange.RuleTriggerState.UNCHANGED) {
            this.triggered = var3.triggerState == RuleStateChange.RuleTriggerState.TRIGGERED;
            this.onTrigger(var1, var2, this.triggered);
         }
      } catch (Exception var9) {
         var9.printStackTrace();
         System.err.println("ERROR WHEN RECEIVING INCREMENTAL RULE STATE: Requesting full update for this rule");
         this.requestRuleState(var1);
      }

      return var4;
   }

   public void sendCompleteRuleState(RuleEntityContainer var1) {
      this.stateChange.clear();
      this.createCompleteConditionState(this.stateChange);
      if (this.triggered) {
         this.stateChange.triggerState = RuleStateChange.RuleTriggerState.TRIGGERED;
      }

      var1.getRuleEntityManager().sendRuleStateChange(this, this.stateChange);
      this.stateChange.clear();
   }

   public void requestRuleState(RuleEntityContainer var1) {
      assert !var1.isOnServer() : "Can't request on server (We are Authority)";

      var1.getNetworkObject().getRuleStateRequestBuffer().add(this.ruleId);
   }

   public void getAllConditions(ConditionList var1) {
      Iterator var2 = this.conditions.iterator();

      while(var2.hasNext()) {
         ((Condition)var2.next()).addToList(var1);
      }

   }

   public String getRuleSetUID() {
      return this.ruleSetUID;
   }

   public void setRuleSet(RuleSet var1) {
      this.ruleSetUID = var1.uniqueIdentifier;
   }
}
