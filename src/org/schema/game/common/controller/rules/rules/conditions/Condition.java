package org.schema.game.common.controller.rules.rules.conditions;

import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleParserException;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.util.FieldUtils;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.TopLevelType;
import org.schema.schine.network.XMLSerializationInterface;
import org.w3c.dom.Attr;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class Condition implements RuleFieldValueInterface, SerialializationInterface, XMLSerializationInterface {
   public static final long TRIGGER_ON_RULE_CHANGE = 1L;
   public static final long TRIGGER_ON_AI_ACTIVE_CHANGE = 2L;
   public static final long TRIGGER_ON_TIMED_CONDITION = 4L;
   public static final long TRIGGER_ON_ATTACK = 8L;
   public static final long TRIGGER_ON_FACTION_CHANGE = 16L;
   public static final long TRIGGER_ON_SECTOR_ENTITIES_CHANGED = 32L;
   public static final long TRIGGER_ON_RULE_STATE_CHANGE = 64L;
   public static final long TRIGGER_ON_ALL = -1L;
   public boolean forceTrue;
   private boolean satisfied;
   private static byte VERSION = 0;
   public Condition parent;

   public final boolean isSatisfied() {
      return this.satisfied;
   }

   public void createStateChange(short var1, RuleStateChange var2) {
      var2.changeLogCond.add(this.isSatisfied() ? (short)(var1 + 1) : (short)(-(var1 + 1)));
   }

   public final boolean checkSatisfied(short var1, RuleStateChange var2, Object var3, long var4, boolean var6) {
      if (this.isTriggeredOn(var4)) {
         int var7 = var2.changeLogCond.size();
         boolean var8 = this.processCondition(var1, var2, var3, var4, var6);
         boolean var9 = var2.changeLogCond.size() > var7;
         if (var8 != this.satisfied || var9) {
            this.satisfied = var8;
            var2.changeLogCond.add(var7, this.satisfied ? (short)(var1 + 1) : (short)(-(var1 + 1)));
         }
      }

      return this.satisfied;
   }

   public boolean isTriggeredOn(long var1) {
      return (this.getTrigger() & var1) != 0L;
   }

   public abstract long getTrigger();

   protected abstract boolean processCondition(short var1, RuleStateChange var2, Object var3, long var4, boolean var6);

   public abstract ConditionTypes getType();

   private Node createConditionElement(Document var1, Node var2) {
      Element var4 = var1.createElement("Condition");
      Attr var3;
      (var3 = var1.createAttribute("type")).setValue(String.valueOf(this.getType().UID));
      var4.getAttributes().setNamedItem(var3);
      (var3 = var1.createAttribute("version")).setValue(String.valueOf(VERSION));
      var4.getAttributes().setNamedItem(var3);
      Comment var5 = var1.createComment(this.getType().name());
      var4.appendChild(var5);
      return var4;
   }

   public List createFieldValues() {
      return RuleFieldValue.create(this);
   }

   public Node writeXML(Document var1, Node var2) {
      var2 = this.createConditionElement(var1, var2);
      Iterator var3 = FieldUtils.getAllFields(new ObjectArrayList(), this.getClass()).iterator();

      while(true) {
         Field var4;
         RuleValue var5;
         do {
            if (!var3.hasNext()) {
               return var2;
            }
         } while((var5 = (RuleValue)(var4 = (Field)var3.next()).getAnnotation(RuleValue.class)) == null);

         Element var11 = var1.createElement(var5.tag());

         try {
            if (var4.getType() == Boolean.TYPE) {
               var11.setTextContent(String.valueOf(var4.getBoolean(this)));
            } else if (var4.getType() == Float.TYPE) {
               var11.setTextContent(String.valueOf(var4.getFloat(this)));
            } else if (var4.getType() == Long.TYPE) {
               var11.setTextContent(String.valueOf(var4.getLong(this)));
            } else if (var4.getType() == Short.TYPE) {
               var11.setTextContent(String.valueOf(var4.getShort(this)));
            } else if (var4.getType() == Integer.TYPE) {
               var11.setTextContent(String.valueOf(var4.getInt(this)));
            } else if (var4.getType() == Byte.TYPE) {
               var11.setTextContent(String.valueOf(var4.getByte(this)));
            } else if (var4.getType() == Double.TYPE) {
               var11.setTextContent(String.valueOf(var4.getDouble(this)));
            } else if (isEnum(var4)) {
               Enum var10 = (Enum)var4.get(this);
               var11.setTextContent(String.valueOf(var10.ordinal()));
            } else if (var4.getType() == FactionRange.class) {
               ((FactionRange)var4.get(this)).writeXML(var1, var11);
            } else if (var4.getType() == ConditionList.class) {
               ConditionList var9 = (ConditionList)var4.get(this);

               for(int var6 = 0; var6 < var9.size(); ++var6) {
                  Node var7 = ((Condition)var9.get(var6)).writeXML(var1, var11);
                  var11.appendChild(var7);
               }
            } else {
               var11.setTextContent(String.valueOf(var4.get(this).toString()));
            }
         } catch (Exception var8) {
            throw new RuleParserException(var8);
         }

         var2.appendChild(var11);
      }
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeByte((byte)this.getType().UID);
      List var3;
      Collections.sort(var3 = FieldUtils.getAllFields(new ObjectArrayList(), this.getClass()), new Comparator() {
         public int compare(Field var1, Field var2) {
            RuleValue var3 = (RuleValue)var1.getAnnotation(RuleValue.class);
            RuleValue var4 = (RuleValue)var2.getAnnotation(RuleValue.class);
            if (var3 == null && var4 == null) {
               return 0;
            } else if (var3 == null && var4 != null) {
               return -1;
            } else {
               return var3 != null && var4 == null ? 1 : var3.tag().compareTo(var3.tag());
            }
         }
      });
      Iterator var7 = var3.iterator();

      while(true) {
         Field var4;
         do {
            if (!var7.hasNext()) {
               return;
            }
         } while((RuleValue)(var4 = (Field)var7.next()).getAnnotation(RuleValue.class) == null);

         try {
            if (var4.getType() == Boolean.TYPE) {
               var1.writeBoolean(var4.getBoolean(this));
            } else if (var4.getType() == Float.TYPE) {
               var1.writeFloat(var4.getFloat(this));
            } else if (var4.getType() == Long.TYPE) {
               var1.writeLong(var4.getLong(this));
            } else if (var4.getType() == Short.TYPE) {
               var1.writeShort(var4.getShort(this));
            } else if (var4.getType() == Integer.TYPE) {
               var1.writeInt(var4.getInt(this));
            } else if (var4.getType() == Byte.TYPE) {
               var1.writeByte(var4.getByte(this));
            } else if (var4.getType() == Double.TYPE) {
               var1.writeDouble(var4.getDouble(this));
            } else if (isEnum(var4)) {
               Enum var10 = (Enum)var4.get(this);
               var1.writeShort(var10.ordinal());
            } else if (var4.getType() == FactionRange.class) {
               FactionRange var9 = (FactionRange)var4.get(this);
               var1.writeInt(var9.from);
               var1.writeInt(var9.to);
            } else if (var4.getType() == ConditionList.class) {
               ConditionList var8 = (ConditionList)var4.get(this);
               var1.writeInt(var8.size());

               for(int var5 = 0; var5 < var8.size(); ++var5) {
                  ((Condition)var8.get(var5)).serialize(var1, var2);
               }
            } else {
               var1.writeUTF(var4.get(this).toString());
            }
         } catch (Exception var6) {
            throw new RuleParserException(var6);
         }
      }
   }

   public static boolean isEnum(Field var0) {
      Class var1;
      return (var1 = var0.getType()) instanceof Class && var1.isEnum();
   }

   public void parseXML(Node var1) {
      assert var1.getAttributes().getNamedItem("type") != null;

      NodeList var11 = var1.getChildNodes();
      List var2 = FieldUtils.getAllFields(new ObjectArrayList(), this.getClass());

      for(int var3 = 0; var3 < var11.getLength(); ++var3) {
         Node var4 = var11.item(var3);
         Iterator var5 = var2.iterator();

         while(var5.hasNext()) {
            Field var6;
            RuleValue var7;
            if ((var7 = (RuleValue)(var6 = (Field)var5.next()).getAnnotation(RuleValue.class)) != null && var7.tag().toLowerCase(Locale.ENGLISH).equals(var4.getNodeName().toLowerCase(Locale.ENGLISH))) {
               try {
                  if (var6.getType() == Boolean.TYPE) {
                     var6.setBoolean(this, Boolean.parseBoolean(var4.getTextContent()));
                     break;
                  }

                  if (var6.getType() == Float.TYPE) {
                     var6.setFloat(this, Float.parseFloat(var4.getTextContent()));
                     break;
                  }

                  if (var6.getType() == Long.TYPE) {
                     var6.setLong(this, Long.parseLong(var4.getTextContent()));
                     break;
                  }

                  if (var6.getType() == Short.TYPE) {
                     var6.setShort(this, Short.parseShort(var4.getTextContent()));
                     break;
                  }

                  if (var6.getType() == Integer.TYPE) {
                     var6.setInt(this, Integer.parseInt(var4.getTextContent()));
                     break;
                  }

                  if (var6.getType() == Byte.TYPE) {
                     var6.setByte(this, Byte.parseByte(var4.getTextContent()));
                     break;
                  }

                  if (var6.getType() == Double.TYPE) {
                     var6.setDouble(this, Double.parseDouble(var4.getTextContent()));
                     break;
                  }

                  if (isEnum(var6)) {
                     Enum[] var14 = (Enum[])var6.getType().getMethod("values").invoke((Object)null);
                     var6.set(this, var14[Integer.parseInt(var4.getTextContent())]);
                     break;
                  }

                  if (var6.getType() == FactionRange.class) {
                     ((FactionRange)var6.get(this)).parseXML(var4);
                     break;
                  }

                  if (var6.getType() != ConditionList.class) {
                     var6.set(this, var4.getTextContent());
                     break;
                  }

                  NodeList var13 = var4.getChildNodes();
                  ConditionList var12;
                  (var12 = (ConditionList)var6.get(this)).clear();

                  for(int var15 = 0; var15 < var13.getLength(); ++var15) {
                     Node var8;
                     if ((var8 = var13.item(var15)).getNodeType() == 1) {
                        Node var9;
                        if ((var9 = var8.getAttributes().getNamedItem("type")) == null) {
                           throw new RuleParserException("No type attribute on Condition Node (ConditionList). Node Name: " + var8.getNodeName());
                        }

                        Condition var16;
                        (var16 = ConditionTypes.getByUID(Byte.parseByte(var9.getNodeValue())).fac.instantiateCondition()).parseXML(var8);
                        var12.add(var16);
                     }
                  }

                  var6.set(this, var12);
                  break;
               } catch (Exception var10) {
                  throw new RuleParserException(var10);
               }
            }
         }
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      List var4;
      Collections.sort(var4 = FieldUtils.getAllFields(new ObjectArrayList(), this.getClass()), new Comparator() {
         public int compare(Field var1, Field var2) {
            RuleValue var3 = (RuleValue)var1.getAnnotation(RuleValue.class);
            RuleValue var4 = (RuleValue)var2.getAnnotation(RuleValue.class);
            if (var3 == null && var4 == null) {
               return 0;
            } else if (var3 == null && var4 != null) {
               return -1;
            } else {
               return var3 != null && var4 == null ? 1 : var3.tag().compareTo(var3.tag());
            }
         }
      });
      Iterator var11 = var4.iterator();

      while(true) {
         Field var5;
         ConditionList var6;
         RuleValue var10000;
         do {
            if (!var11.hasNext()) {
               return;
            }

            var10000 = (RuleValue)(var5 = (Field)var11.next()).getAnnotation(RuleValue.class);
            var6 = null;
         } while(var10000 == null);

         try {
            if (var5.getType() == Boolean.TYPE) {
               var5.setBoolean(this, var1.readBoolean());
            } else if (var5.getType() == Float.TYPE) {
               var5.setFloat(this, var1.readFloat());
            } else if (var5.getType() == Long.TYPE) {
               var5.setLong(this, var1.readLong());
            } else if (var5.getType() == Short.TYPE) {
               var5.setShort(this, var1.readShort());
            } else if (var5.getType() == Integer.TYPE) {
               var5.setInt(this, var1.readInt());
            } else if (var5.getType() == Byte.TYPE) {
               var5.setByte(this, var1.readByte());
            } else if (var5.getType() == Double.TYPE) {
               var5.setDouble(this, var1.readDouble());
            } else if (isEnum(var5)) {
               Enum[] var13 = (Enum[])var5.getType().getMethod("values").invoke((Object)null);
               var5.set(this, var13[var1.readShort()]);
            } else if (var5.getType() == FactionRange.class) {
               FactionRange var12;
               (var12 = (FactionRange)var5.get(this)).from = var1.readInt();
               var12.to = var1.readInt();
            } else if (var5.getType() != ConditionList.class) {
               var5.set(this, var1.readUTF());
            } else {
               (var6 = (ConditionList)var5.get(this)).clear();
               int var7 = var1.readInt();

               for(int var8 = 0; var8 < var7; ++var8) {
                  Condition var9;
                  (var9 = ConditionTypes.getByUID(var1.readByte()).fac.instantiateCondition()).deserialize(var1, var2, var3);
                  var6.add(var9);
               }

               var5.set(this, var6);
            }
         } catch (Exception var10) {
            throw new RuleParserException(var10);
         }
      }
   }

   public abstract String getDescriptionShort();

   public Condition duplicate() throws IOException {
      Condition var1 = this.getType().fac.instantiateCondition();
      FastByteArrayOutputStream var2 = new FastByteArrayOutputStream(10240);
      DataOutputStream var3 = new DataOutputStream(var2);
      this.serialize(var3, true);
      DataInputStream var4 = new DataInputStream(new FastByteArrayInputStream(var2.array, 0, (int)var2.position()));
      var1.deserialize(var4, 0, true);
      return var1;
   }

   public long calculateGroupTriggerRec(long var1) {
      return this.getTrigger();
   }

   public TopLevelType getEntityType() {
      return this.getType().getType();
   }

   public int processReceivedState(int var1, ShortArrayList var2, short var3) {
      boolean var4;
      if ((var4 = var3 > 0) != this.satisfied) {
         this.satisfied = var4;
      }

      return var1;
   }

   public void resetCondition(boolean var1) {
      this.satisfied = var1;
   }

   public void addToList(ConditionList var1) {
      var1.add(this);
   }
}
