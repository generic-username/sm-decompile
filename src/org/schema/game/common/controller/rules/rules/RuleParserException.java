package org.schema.game.common.controller.rules.rules;

public class RuleParserException extends RuntimeException {
   private static final long serialVersionUID = -3462162891269365149L;

   public RuleParserException() {
   }

   public RuleParserException(String var1, Throwable var2, boolean var3, boolean var4) {
      super(var1, var2, var3, var4);
   }

   public RuleParserException(String var1, Throwable var2) {
      super(var1, var2);
   }

   public RuleParserException(String var1) {
      super(var1);
   }

   public RuleParserException(Throwable var1) {
      super(var1);
   }
}
