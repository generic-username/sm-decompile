package org.schema.game.common.controller.rules.rules.conditions.faction;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.language.Lng;

public class FactionFPCountCondition extends FactionMoreLessCondition {
   @RuleValue(
      tag = "FactionPoints"
   )
   public float points;

   public long getTrigger() {
      return 8192L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.FACTION_FP_COUNT;
   }

   public String getCountString() {
      return String.valueOf(this.points);
   }

   public String getQuantifierString() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_FACTION_FACTIONFPCOUNTCONDITION_0, this.points);
   }

   protected boolean processCondition(short var1, RuleStateChange var2, Faction var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         float var7 = var3.factionPoints;
         if (this.moreThan) {
            return var7 > this.points;
         } else {
            return var7 <= this.points;
         }
      }
   }
}
