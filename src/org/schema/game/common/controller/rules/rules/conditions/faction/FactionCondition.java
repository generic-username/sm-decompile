package org.schema.game.common.controller.rules.rules.conditions.faction;

import org.schema.game.common.controller.rules.rules.conditions.Condition;

public abstract class FactionCondition extends Condition {
   public static final long TRIGGER_ON_FACTION_MEMBER_MOD = 2048L;
   public static final long TRIGGER_ON_FACTION_RELATIONSHIP_MOD = 4096L;
   public static final long TRIGGER_ON_FACTION_POINTS_CHANGE = 8192L;
}
