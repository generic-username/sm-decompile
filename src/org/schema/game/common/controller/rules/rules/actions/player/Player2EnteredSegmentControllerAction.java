package org.schema.game.common.controller.rules.rules.actions.player;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Iterator;
import java.util.Set;
import org.schema.common.util.StringTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rails.RailController;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerActionList;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;

public class Player2EnteredSegmentControllerAction extends PlayerAction {
   @RuleValue(
      tag = "Actions"
   )
   public SegmentControllerActionList actions = new SegmentControllerActionList();
   @RuleValue(
      tag = "Target"
   )
   public RailController.RailTarget target;
   @RuleValue(
      tag = "PilotOnly"
   )
   public boolean pilot;
   private Set targets;

   public Player2EnteredSegmentControllerAction() {
      this.target = RailController.RailTarget.EVERYTHING;
      this.pilot = false;
      this.targets = new ObjectOpenHashSet();
   }

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_PLAYER_PLAYER2ENTEREDSEGMENTCONTROLLERACTION_0, this.actions.size());
   }

   public void onTrigger(PlayerState var1) {
      SimpleTransformableSendableObject var2;
      if (var1.isOnServer() && (var2 = var1.getFirstControlledTransformableWOExc()) instanceof SegmentController && (!this.pilot || var2.getOwnerState() == var1)) {
         this.targets.clear();
         this.target.getTargets((SegmentController)var2, this.targets);
         Iterator var3 = this.targets.iterator();

         while(var3.hasNext()) {
            SegmentController var4 = (SegmentController)var3.next();
            this.actions.onTrigger(var4);
         }
      }

   }

   public void onUntrigger(PlayerState var1) {
      SimpleTransformableSendableObject var2;
      if (var1.isOnServer() && (var2 = var1.getFirstControlledTransformableWOExc()) instanceof SegmentController && (!this.pilot || var2.getOwnerState() == var1)) {
         this.targets.clear();
         this.target.getTargets((SegmentController)var2, this.targets);
         Iterator var3 = this.targets.iterator();

         while(var3.hasNext()) {
            SegmentController var4 = (SegmentController)var3.next();
            this.actions.onUntrigger(var4);
         }
      }

   }

   public ActionTypes getType() {
      return ActionTypes.PLAYER_2_SEG;
   }
}
