package org.schema.game.common.controller.rules.rules.conditions.player;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;

public class PlayerHasCreditsCondition extends PlayerMoreLessCondition {
   @RuleValue(
      tag = "Credits"
   )
   public long credits;

   public long getTrigger() {
      return 4096L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.PLAYER_HAS_CREDITS;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, PlayerState var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         long var7 = (long)var3.getCredits();
         if (this.moreThan) {
            return var7 > this.credits;
         } else {
            return var7 <= this.credits;
         }
      }
   }

   public String getCountString() {
      return String.valueOf(this.credits);
   }

   public String getQuantifierString() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_PLAYER_PLAYERHASCREDITSCONDITION_0, this.credits);
   }
}
