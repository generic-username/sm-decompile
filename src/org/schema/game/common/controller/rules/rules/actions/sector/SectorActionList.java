package org.schema.game.common.controller.rules.rules.actions.sector;

import org.schema.game.common.controller.rules.rules.actions.Action;
import org.schema.game.common.controller.rules.rules.actions.ActionList;
import org.schema.schine.network.TopLevelType;

public class SectorActionList extends ActionList {
   private static final long serialVersionUID = 1L;

   public TopLevelType getEntityType() {
      return TopLevelType.SECTOR;
   }

   public void add(Action var1) {
      this.add((SectorAction)var1);
   }
}
