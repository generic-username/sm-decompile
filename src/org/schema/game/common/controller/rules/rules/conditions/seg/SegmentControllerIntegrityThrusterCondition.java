package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.ManagedSegmentController;

public class SegmentControllerIntegrityThrusterCondition extends SegmentControllerAbstractIntegrityCondition {
   public double getSmallestIntegrity(ManagedSegmentController var1) {
      return var1.getManagerContainer() instanceof ShipManagerContainer ? ((ShipManagerContainer)var1.getManagerContainer()).getThrusterElementManager().lowestIntegrity : Double.POSITIVE_INFINITY;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_INTEGRITY_THRUSTER_CONDITION;
   }

   public String getQuantifierString() {
      return "Thruster Integrity";
   }
}
