package org.schema.game.common.controller.rules.rules.actions.sector;

import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerActionList;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;

public class Sector2EntityAction extends SectorAction {
   @RuleValue(
      tag = "Actions"
   )
   public SegmentControllerActionList actions = new SegmentControllerActionList();

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SECTOR_SECTOR2ENTITYACTION_0, this.actions.size());
   }

   public void onTrigger(RemoteSector var1) {
      if (var1.isOnServer()) {
         var1.getState();
         Iterator var2 = var1.getServerSector().getEntities().iterator();

         while(var2.hasNext()) {
            SimpleTransformableSendableObject var3;
            if ((var3 = (SimpleTransformableSendableObject)var2.next()).getSectorId() == var1.getId() && var3 instanceof SegmentController) {
               this.actions.onTrigger((SegmentController)var3);
            }
         }
      }

   }

   public void onUntrigger(RemoteSector var1) {
      if (var1.isOnServer()) {
         var1.getState();
         Iterator var2 = var1.getServerSector().getEntities().iterator();

         while(var2.hasNext()) {
            SimpleTransformableSendableObject var3;
            if ((var3 = (SimpleTransformableSendableObject)var2.next()).getSectorId() == var1.getId() && var3 instanceof SegmentController) {
               this.actions.onUntrigger((SegmentController)var3);
            }
         }
      }

   }

   public ActionTypes getType() {
      return ActionTypes.SECTOR_2_SEG;
   }
}
