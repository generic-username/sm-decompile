package org.schema.game.common.controller.rules.rules.actions.player;

import org.schema.game.common.controller.rules.rules.actions.Action;
import org.schema.game.common.controller.rules.rules.actions.ActionList;
import org.schema.schine.network.TopLevelType;

public class PlayerActionList extends ActionList {
   private static final long serialVersionUID = 1L;

   public TopLevelType getEntityType() {
      return TopLevelType.PLAYER;
   }

   public void add(Action var1) {
      this.add((PlayerAction)var1);
   }
}
