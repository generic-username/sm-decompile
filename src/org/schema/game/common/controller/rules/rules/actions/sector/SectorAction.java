package org.schema.game.common.controller.rules.rules.actions.sector;

import org.schema.game.common.controller.rules.rules.actions.Action;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.RuleEntityContainer;
import org.schema.schine.network.TopLevelType;

public abstract class SectorAction extends Action {
   public void onTrigger(RuleEntityContainer var1, TopLevelType var2) {
      assert this.getEntityType() == var2;

      RemoteSector var3 = (RemoteSector)var1;
      this.onTrigger(var3);
   }

   public void onUntrigger(RuleEntityContainer var1, TopLevelType var2) {
      assert this.getEntityType() == var2;

      RemoteSector var3 = (RemoteSector)var1;
      this.onUntrigger(var3);
   }
}
