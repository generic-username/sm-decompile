package org.schema.game.common.controller.rules.rules.actions.seg;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.controller.rules.rules.actions.faction.FactionActionList;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

public class SegmentController2FactionAction extends SegmentControllerAction {
   @RuleValue(
      tag = "Actions"
   )
   public FactionActionList actions = new FactionActionList();

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLER2FACTIONACTION_0, this.actions.size());
   }

   public void onTrigger(SegmentController var1) {
      if (var1.isOnServer()) {
         GameServerState var2 = (GameServerState)var1.getState();
         ObjectOpenHashSet var3 = new ObjectOpenHashSet();
         if (var2.getFactionManager().existsFaction(var1.getFactionId())) {
            var3.add(var2.getFactionManager().getFaction(var1.getFactionId()));
         }

         Iterator var4 = var3.iterator();

         while(var4.hasNext()) {
            Faction var5 = (Faction)var4.next();
            this.actions.onTrigger(var5);
         }
      }

   }

   public void onUntrigger(SegmentController var1) {
      if (var1.isOnServer()) {
         GameServerState var2 = (GameServerState)var1.getState();
         ObjectOpenHashSet var3 = new ObjectOpenHashSet();
         if (var2.getFactionManager().existsFaction(var1.getFactionId())) {
            var3.add(var2.getFactionManager().getFaction(var1.getFactionId()));
         }

         Iterator var4 = var3.iterator();

         while(var4.hasNext()) {
            Faction var5 = (Faction)var4.next();
            this.actions.onUntrigger(var5);
         }
      }

   }

   public ActionTypes getType() {
      return ActionTypes.SEG_2_FACTION;
   }
}
