package org.schema.game.common.controller.rules.rules.actions.seg;

import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerEnableEnergyStreamAction extends SegmentControllerAction {
   public ActionTypes getType() {
      return ActionTypes.SEG_ENABLE_ENERGY_STREAM;
   }

   public String getDescriptionShort() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLERENABLEENERGYSTREAMACTION_0;
   }

   public void onTrigger(SegmentController var1) {
      if (var1 instanceof ManagedUsableSegmentController) {
         ((ManagedUsableSegmentController)var1).getManagerContainer().getPowerInterface().setEnergyStreamEnabled(true);
      }

   }

   public void onUntrigger(SegmentController var1) {
      if (var1 instanceof ManagedUsableSegmentController) {
         ((ManagedUsableSegmentController)var1).getManagerContainer().getPowerInterface().setEnergyStreamEnabled(false);
      }

   }
}
