package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.ManagedSegmentController;

public class SegmentControllerReactorLevelCondition extends SegmentControllerMoreLessCondition {
   @RuleValue(
      tag = "Capacity"
   )
   public int level;

   public long getTrigger() {
      return 8192L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_REACTOR_LEVEL_CONDITION;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else if (var3 instanceof ManagedSegmentController) {
         PowerInterface var7;
         if ((var7 = ((ManagedSegmentController)var3).getManagerContainer().getPowerInterface()).getActiveReactor() != null) {
            if (this.moreThan) {
               return var7.getActiveReactor().getLevel() > this.level;
            } else {
               return var7.getActiveReactor().getLevel() <= this.level;
            }
         } else {
            return false;
         }
      } else {
         return false;
      }
   }

   public String getCountString() {
      return String.valueOf(this.level);
   }

   public String getQuantifierString() {
      return "Reactor Level";
   }
}
