package org.schema.game.common.controller.rules.rules.actions.player;

import java.io.IOException;
import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.admin.AdminCommandQueueElement;
import org.schema.schine.common.language.Lng;

public class PlayerWarpAction extends PlayerAction {
   @RuleValue(
      tag = "SectorX"
   )
   public int sectorX;
   @RuleValue(
      tag = "SectorY"
   )
   public int sectorY;
   @RuleValue(
      tag = "SectorZ"
   )
   public int sectorZ;
   @RuleValue(
      tag = "WarpAsCopy"
   )
   public boolean copy;

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_PLAYER_PLAYERWARPACTION_0, this.sectorX, this.sectorY, this.sectorZ);
   }

   public void onTrigger(PlayerState var1) {
      if (var1.isOnServer()) {
         Vector3i var2 = new Vector3i(this.sectorX, this.sectorY, this.sectorZ);

         try {
            Sector var6;
            if ((var6 = ((GameServerState)var1.getState()).getUniverse().getSector(var2)) != null) {
               Iterator var3 = var1.getControllerState().getUnits().iterator();

               while(var3.hasNext()) {
                  ControllerStateUnit var4;
                  if ((var4 = (ControllerStateUnit)var3.next()).playerControllable instanceof SimpleTransformableSendableObject) {
                     ((GameServerState)var1.getState()).getController().queueSectorSwitch(AdminCommandQueueElement.getControllerRoot((SimpleTransformableSendableObject)var4.playerControllable), var6.pos, 1, this.copy, true, true);
                  }
               }
            }

            return;
         } catch (IOException var5) {
            var5.printStackTrace();
         }
      }

   }

   public void onUntrigger(PlayerState var1) {
   }

   public ActionTypes getType() {
      return ActionTypes.PLAYER_WARP;
   }
}
