package org.schema.game.common.controller.rules.rules.conditions.seg;

import java.util.Iterator;
import org.schema.game.common.controller.elements.WeaponManagerInterface;
import org.schema.game.common.controller.elements.beam.damageBeam.DamageBeamCollectionManager;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.schine.common.language.Lng;

public class SegmentControllerBeamOutputCountCondition extends SegmentControllerAbstractOutputCountCondition {
   public ConditionTypes getType() {
      return ConditionTypes.SEG_OUTPUTS_PER_BEAM;
   }

   public double getOutputCount(ManagedSegmentController var1) {
      int var2 = 0;
      if (var1.getManagerContainer() instanceof WeaponManagerInterface) {
         for(Iterator var3 = ((WeaponManagerInterface)var1.getManagerContainer()).getBeam().getCollectionManagers().iterator(); var3.hasNext(); var2 = Math.max(((DamageBeamCollectionManager)var3.next()).getElementCollections().size(), var2)) {
         }
      }

      return (double)var2;
   }

   public String getQuantifierString() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERBEAMOUTPUTCOUNTCONDITION_0;
   }
}
