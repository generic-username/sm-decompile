package org.schema.game.common.controller.rules.rules.actions;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.schema.game.common.controller.rules.rules.actions.faction.Faction2PlayerAction;
import org.schema.game.common.controller.rules.rules.actions.faction.FactionAction;
import org.schema.game.common.controller.rules.rules.actions.faction.FactionActionFactory;
import org.schema.game.common.controller.rules.rules.actions.faction.FactionModFactionPointsAction;
import org.schema.game.common.controller.rules.rules.actions.faction.FactionSetFactionPointsAction;
import org.schema.game.common.controller.rules.rules.actions.player.Player2EnteredSegmentControllerAction;
import org.schema.game.common.controller.rules.rules.actions.player.Player2FactionAction;
import org.schema.game.common.controller.rules.rules.actions.player.Player2SectorAction;
import org.schema.game.common.controller.rules.rules.actions.player.PlayerAction;
import org.schema.game.common.controller.rules.rules.actions.player.PlayerActionFactory;
import org.schema.game.common.controller.rules.rules.actions.player.PlayerBanAction;
import org.schema.game.common.controller.rules.rules.actions.player.PlayerKickAction;
import org.schema.game.common.controller.rules.rules.actions.player.PlayerKickOutOfEntityAction;
import org.schema.game.common.controller.rules.rules.actions.player.PlayerKillAction;
import org.schema.game.common.controller.rules.rules.actions.player.PlayerModBlockTypeAction;
import org.schema.game.common.controller.rules.rules.actions.player.PlayerModBlockTypeRecurringAction;
import org.schema.game.common.controller.rules.rules.actions.player.PlayerModCreditsAction;
import org.schema.game.common.controller.rules.rules.actions.player.PlayerModCreditsRecurringAction;
import org.schema.game.common.controller.rules.rules.actions.player.PlayerModFactionPointsAction;
import org.schema.game.common.controller.rules.rules.actions.player.PlayerRunAdminCommandAction;
import org.schema.game.common.controller.rules.rules.actions.player.PlayerSendMessageAction;
import org.schema.game.common.controller.rules.rules.actions.player.PlayerSetCreditsAction;
import org.schema.game.common.controller.rules.rules.actions.player.PlayerSetFactionPointsAction;
import org.schema.game.common.controller.rules.rules.actions.player.PlayerWarpAction;
import org.schema.game.common.controller.rules.rules.actions.sector.Sector2EntityAction;
import org.schema.game.common.controller.rules.rules.actions.sector.Sector2FactionAction;
import org.schema.game.common.controller.rules.rules.actions.sector.Sector2PlayerAction;
import org.schema.game.common.controller.rules.rules.actions.sector.SectorAction;
import org.schema.game.common.controller.rules.rules.actions.sector.SectorActionFactory;
import org.schema.game.common.controller.rules.rules.actions.sector.SectorChmodAction;
import org.schema.game.common.controller.rules.rules.actions.sector.SectorRunAdminCommandAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentController2FactionAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentController2PlayerAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentController2SectorAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerActionFactory;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerApplyEffectAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerBeamExplosiveAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerCannonExplosiveAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerChamberExplosiveAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerConsoleMessageAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerEnableEnergyStreamAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerMissileExplosiveAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerModFactionPointsAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerModifyThrusterAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerPopupMessageAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerPopupMessageInBuildModeAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerReactorExplosiveAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerRunAdminCommandAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerSetAIAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerSetFactionAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerSetFactionPointsAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerShieldCapacityExplosiveAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerShieldRegenExplosiveAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerStabilizerExplosiveAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerThrusterExplosiveAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerTrackAction;
import org.schema.game.common.controller.rules.rules.actions.seg.SegmentControllerVoidAction;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;
import org.schema.schine.network.TopLevelType;

public enum ActionTypes {
   DO_NOTHING(1, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerVoidAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_0;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_1;
      }
   }),
   SEG_APPLY_EFFECT(2, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerApplyEffectAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_2;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_3;
      }
   }),
   SEG_CONSOLE_MESSAGE(3, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerConsoleMessageAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_4;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_5;
      }
   }),
   SEG_POPUP_MESSAGE(4, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerPopupMessageAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_6;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_7;
      }
   }),
   SEG_MODIFY_THRUSTER_ACTION(5, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerModifyThrusterAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_8;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_9;
      }
   }),
   SEG_POPUP_MESSAGE_IN_BUILD_MODE(6, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerPopupMessageInBuildModeAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_10;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_11;
      }
   }),
   SEG_TRACK(7, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerTrackAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_12;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_13;
      }
   }),
   SEG_ENABLE_ENERGY_STREAM(8, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerEnableEnergyStreamAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_14;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_15;
      }
   }),
   SEG_BEAM_EXPLOSIVE(9, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerBeamExplosiveAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_16;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_17;
      }
   }),
   SEG_MISSILE_EXPLOSIVE(10, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerMissileExplosiveAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_18;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_19;
      }
   }),
   SEG_CANNON_EXPLOSIVE(11, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerCannonExplosiveAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_20;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_21;
      }
   }),
   SEG_REACTOR_EXPLOSIVE(12, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerReactorExplosiveAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_22;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_23;
      }
   }),
   SEG_REACTOR_CHAMBER_EXPLOSIVE(13, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerChamberExplosiveAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_24;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_25;
      }
   }),
   SEG_SHIELD_CAPACITY_EXPLOSIVE(14, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerShieldCapacityExplosiveAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_26;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_27;
      }
   }),
   SEG_SHIELD_REGEN_EXPLOSIVE(15, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerShieldRegenExplosiveAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_28;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_29;
      }
   }),
   SEG_STABILIZER_EXPLOSIVE(16, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerStabilizerExplosiveAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_30;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_31;
      }
   }),
   SEG_THRUSTER_EXPLOSIVE(17, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerThrusterExplosiveAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_32;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_33;
      }
   }),
   SEG_SET_FACTION(18, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerSetFactionAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_34;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_35;
      }
   }),
   SEG_SET_AI(19, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerSetAIAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_36;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_37;
      }
   }),
   SEG_RUN_ADMIN_COMMAND(20, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerRunAdminCommandAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_42;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_39;
      }
   }),
   SECTOR_CHMOD(21, new SectorActionFactory() {
      public final SectorAction instantiateAction() {
         return new SectorChmodAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_40;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_41;
      }
   }),
   SECTOR_RUN_ADMIN_COMMAND(22, new SectorActionFactory() {
      public final SectorAction instantiateAction() {
         return new SectorRunAdminCommandAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_38;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_43;
      }
   }),
   PLAYER_KILL(23, new PlayerActionFactory() {
      public final PlayerAction instantiateAction() {
         return new PlayerKillAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_45;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_44;
      }
   }),
   PLAYER_KICK(24, new PlayerActionFactory() {
      public final PlayerAction instantiateAction() {
         return new PlayerKickAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_46;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_47;
      }
   }),
   PLAYER_BAN(25, new PlayerActionFactory() {
      public final PlayerAction instantiateAction() {
         return new PlayerBanAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_48;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_49;
      }
   }),
   PLAYER_WARP(26, new PlayerActionFactory() {
      public final PlayerAction instantiateAction() {
         return new PlayerWarpAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_50;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_51;
      }
   }),
   PLAYER_SET_CREDITS(27, new PlayerActionFactory() {
      public final PlayerAction instantiateAction() {
         return new PlayerSetCreditsAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_52;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_53;
      }
   }),
   PLAYER_KICK_OUT_OF_ENTITY(28, new PlayerActionFactory() {
      public final PlayerAction instantiateAction() {
         return new PlayerKickOutOfEntityAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_55;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_54;
      }
   }),
   PLAYER_SEND_MESSAGE(29, new PlayerActionFactory() {
      public final PlayerAction instantiateAction() {
         return new PlayerSendMessageAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_56;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_57;
      }
   }),
   PLAYER_MOD_CREDITS(30, new PlayerActionFactory() {
      public final PlayerAction instantiateAction() {
         return new PlayerModCreditsAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_58;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_59;
      }
   }),
   PLAYER_RUN_ADMIN_COMMAND(31, new PlayerActionFactory() {
      public final PlayerAction instantiateAction() {
         return new PlayerRunAdminCommandAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_60;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_61;
      }
   }),
   PLAYER_SET_FACTION_POINTS(32, new PlayerActionFactory() {
      public final PlayerAction instantiateAction() {
         return new PlayerSetFactionPointsAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_62;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_63;
      }
   }),
   PLAYER_MOD_FACTION_POINTS(33, new PlayerActionFactory() {
      public final PlayerAction instantiateAction() {
         return new PlayerModFactionPointsAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_64;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_65;
      }
   }),
   SEG_SET_FACTION_POINTS(34, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerSetFactionPointsAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_66;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_67;
      }
   }),
   SEG_MOD_FACTION_POINTS(35, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentControllerModFactionPointsAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_68;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_69;
      }
   }),
   FACTION_SET_FACTION_POINTS(36, new FactionActionFactory() {
      public final FactionAction instantiateAction() {
         return new FactionSetFactionPointsAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_70;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_71;
      }
   }),
   FACTION_MOD_FACTION_POINTS(37, new FactionActionFactory() {
      public final FactionAction instantiateAction() {
         return new FactionModFactionPointsAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_72;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_73;
      }
   }),
   FACTION_2_PLAYERS(38, new FactionActionFactory() {
      public final FactionAction instantiateAction() {
         return new Faction2PlayerAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_75;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_74;
      }
   }),
   PLAYER_2_SEG(39, new PlayerActionFactory() {
      public final PlayerAction instantiateAction() {
         return new Player2EnteredSegmentControllerAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_77;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_76;
      }
   }),
   PLAYER_2_SECTOR(40, new PlayerActionFactory() {
      public final PlayerAction instantiateAction() {
         return new Player2SectorAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_79;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_78;
      }
   }),
   PLAYER_2_FACTION(41, new PlayerActionFactory() {
      public final PlayerAction instantiateAction() {
         return new Player2FactionAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_81;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_80;
      }
   }),
   SECTOR_2_PLAYER(42, new SectorActionFactory() {
      public final SectorAction instantiateAction() {
         return new Sector2PlayerAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_82;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_84;
      }
   }),
   SECTOR_2_SEG(43, new SectorActionFactory() {
      public final SectorAction instantiateAction() {
         return new Sector2EntityAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_83;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_85;
      }
   }),
   SECTOR_2_FACTION(44, new SectorActionFactory() {
      public final SectorAction instantiateAction() {
         return new Sector2FactionAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_86;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_87;
      }
   }),
   SEG_2_FACTION(45, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentController2FactionAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_89;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_88;
      }
   }),
   SEG_2_PLAYER(46, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentController2PlayerAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_91;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_90;
      }
   }),
   SEG_2_SECTOR(47, new SegmentControllerActionFactory() {
      public final SegmentControllerAction instantiateAction() {
         return new SegmentController2SectorAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_93;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_92;
      }
   }),
   PLAYER_MOD_CREDITS_RECURRING(48, new PlayerActionFactory() {
      public final PlayerAction instantiateAction() {
         return new PlayerModCreditsRecurringAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_94;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_95;
      }
   }),
   PLAYER_MOD_BLOCK_TYPE(49, new PlayerActionFactory() {
      public final PlayerAction instantiateAction() {
         return new PlayerModBlockTypeAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_96;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_97;
      }
   }),
   PLAYER_MOD_BLOCK_TYPE_RECURRING(50, new PlayerActionFactory() {
      public final PlayerAction instantiateAction() {
         return new PlayerModBlockTypeRecurringAction();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_98;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_ACTIONTYPES_99;
      }
   });

   public final int UID;
   private final Translatable name;
   private final Translatable desc;
   public final ActionFactory fac;
   private static final Int2ObjectOpenHashMap t = new Int2ObjectOpenHashMap();

   public static ActionTypes getByUID(int var0) {
      return (ActionTypes)t.get(var0);
   }

   public final String getName() {
      return this.name.getName(this);
   }

   public final String getDesc() {
      return this.desc.getName(this);
   }

   private ActionTypes(int var3, ActionFactory var4, Translatable var5, Translatable var6) {
      this.UID = var3;
      this.fac = var4;
      this.name = var5;
      this.desc = var6;
   }

   public final TopLevelType getType() {
      return this.fac.getType();
   }

   public static List getSortedByName(TopLevelType var0) {
      ObjectArrayList var1 = new ObjectArrayList();
      ActionTypes[] var2;
      int var3 = (var2 = values()).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         ActionTypes var5;
         if ((var5 = var2[var4]).getType() == var0) {
            var1.add(var5);
         }
      }

      Collections.sort(var1, new Comparator() {
         public final int compare(ActionTypes var1, ActionTypes var2) {
            return var1.getName().compareTo(var2.getName());
         }
      });
      return var1;
   }

   static {
      ActionTypes[] var0;
      int var1 = (var0 = values()).length;

      for(int var2 = 0; var2 < var1; ++var2) {
         ActionTypes var3 = var0[var2];
         if (t.containsKey(var3.UID)) {
            throw new RuntimeException("ERROR INITIALIZING org.schema.game.common.controller.rules.rules.actions: duplicate UID");
         }

         if (var3.fac.instantiateAction() == null) {
            throw new RuntimeException("ERROR INITIALIZING org.schema.game.common.controller.rules.rules.actions: Factory returned null " + var3.name());
         }

         if (var3.fac.instantiateAction().getType() != var3) {
            throw new RuntimeException("ERROR INITIALIZING org.schema.game.common.controller.rules.rules.actions: Factory type mismatch: " + var3.name() + "; " + var3.fac.instantiateAction().getType().name());
         }

         t.put(var3.UID, var3);
      }

   }
}
