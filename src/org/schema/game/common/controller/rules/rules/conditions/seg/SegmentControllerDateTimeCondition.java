package org.schema.game.common.controller.rules.rules.conditions.seg;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerDateTimeCondition extends SegmentControllerAbstractDateTimeCondition {
   @RuleValue(
      tag = "Day"
   )
   public int day;
   @RuleValue(
      tag = "Month",
      intMap = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11},
      int2StringMap = {"Jan", "Feb", "Mar", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"}
   )
   public int month;
   @RuleValue(
      tag = "Year"
   )
   public int year;
   @RuleValue(
      tag = "Hour"
   )
   public int hour;
   @RuleValue(
      tag = "Minute"
   )
   public int minute;
   @RuleValue(
      tag = "Second"
   )
   public int second;

   public Date getDate() {
      return (new GregorianCalendar(this.year, this.month, Math.min(31, Math.max(1, this.day)), Math.abs(this.hour % 24), Math.abs(this.minute % 60), Math.abs(this.second % 60))).getTime();
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_DATE_TIME;
   }

   public String getDescriptionShort() {
      SimpleDateFormat var1 = StringTools.getSimpleDateFormat(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERDATETIMECONDITION_0, "yyyy/MM/dd HH:mm:ss");
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERDATETIMECONDITION_1, String.valueOf(!this.after), var1.format(this.getDate()), String.valueOf(this.after));
   }
}
