package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.controller.rules.rules.conditions.FactionRange;
import org.schema.schine.common.language.Lng;

public class SegmentControllerFactionCondition extends SegmentControllerCondition {
   @RuleValue(
      tag = "FactionId"
   )
   public FactionRange factionRange = new FactionRange();

   public long getTrigger() {
      return 16L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_IS_FACTION;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      return var6 ? true : this.factionRange.isInRange(var3.getFactionId());
   }

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERFACTIONCONDITION_1, String.valueOf(this.factionRange));
   }
}
