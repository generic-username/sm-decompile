package org.schema.game.common.controller.rules.rules.conditions.seg;

import java.io.IOException;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

public class SegmentControllerSystemOwnedByCondition extends SegmentControllerCondition {
   @RuleValue(
      tag = "Own"
   )
   public boolean own = false;
   @RuleValue(
      tag = "Relationship"
   )
   public FactionRelation.RType relation;

   public SegmentControllerSystemOwnedByCondition() {
      this.relation = FactionRelation.RType.NEUTRAL;
   }

   public long getTrigger() {
      return 1048576L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_SYSTEM_RELATIONSHIP;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         GameServerState var8 = (GameServerState)var3.getState();
         Vector3i var9 = var3.getSystem(new Vector3i());
         new Vector3i();

         try {
            StellarSystem var10 = var8.getUniverse().getStellarSystemFromStellarPos(var9);
            int var11 = var3.getFactionId();
            if (var3.getOwnerState() != null) {
               var11 = var3.getOwnerState().getFactionId();
            }

            if (this.own) {
               return var10.getOwnerFaction() != 0 && var10.getOwnerFaction() == var11;
            } else {
               return this.relation == var8.getFactionManager().getRelation(var11, var10.getOwnerFaction());
            }
         } catch (IOException var7) {
            var7.printStackTrace();
            return false;
         }
      }
   }

   public String getDescriptionShort() {
      return this.own ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERSYSTEMOWNEDBYCONDITION_0 : StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERSYSTEMOWNEDBYCONDITION_1, this.relation.getName());
   }
}
