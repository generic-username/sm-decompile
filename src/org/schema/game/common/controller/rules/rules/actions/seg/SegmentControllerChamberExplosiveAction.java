package org.schema.game.common.controller.rules.rules.actions.seg;

import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerModuleSingle;
import org.schema.game.common.controller.elements.power.reactor.chamber.ReactorChamberElementManager;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerChamberExplosiveAction extends SegmentControllerAction {
   public String getExplName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLERCHAMBEREXPLOSIVEACTION_0;
   }

   public ActionTypes getType() {
      return ActionTypes.SEG_REACTOR_CHAMBER_EXPLOSIVE;
   }

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLERCHAMBEREXPLOSIVEACTION_1, this.getExplName());
   }

   public void onTrigger(SegmentController var1) {
      if (var1 instanceof ManagedUsableSegmentController) {
         Iterator var2 = ((ManagedUsableSegmentController)var1).getManagerContainer().getChambers().iterator();

         while(var2.hasNext()) {
            ((ReactorChamberElementManager)((ManagerModuleSingle)var2.next()).getElementManager()).setExplosiveStructure(true);
         }
      }

   }

   public void onUntrigger(SegmentController var1) {
      if (var1 instanceof ManagedUsableSegmentController) {
         Iterator var2 = ((ManagedUsableSegmentController)var1).getManagerContainer().getChambers().iterator();

         while(var2.hasNext()) {
            ((ReactorChamberElementManager)((ManagerModuleSingle)var2.next()).getElementManager()).setExplosiveStructure(false);
         }
      }

   }
}
