package org.schema.game.common.controller.rules.rules.actions.seg;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerModifyThrusterAction extends SegmentControllerAction {
   @RuleValue(
      tag = "ThrusterPercent"
   )
   public float thrusterPercent = 1.0F;

   public ActionTypes getType() {
      return ActionTypes.SEG_MODIFY_THRUSTER_ACTION;
   }

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLERMODIFYTHRUSTERACTION_1, this.thrusterPercent * 100.0F);
   }

   public void onTrigger(SegmentController var1) {
      if (var1 instanceof Ship) {
         System.err.println(var1.getState() + "[ACTION] Trigger Thruster Modify action for " + var1 + ": " + this.thrusterPercent);
         ((Ship)var1).getManagerContainer().getThrusterElementManager().ruleModifierOnThrust = this.thrusterPercent;
      }

   }

   public void onUntrigger(SegmentController var1) {
      if (var1 instanceof Ship) {
         ((Ship)var1).getManagerContainer().getThrusterElementManager().ruleModifierOnThrust = 1.0F;
      }

   }
}
