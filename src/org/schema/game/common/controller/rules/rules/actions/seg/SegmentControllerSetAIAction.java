package org.schema.game.common.controller.rules.rules.actions.seg;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerSetAIAction extends SegmentControllerAction {
   @RuleValue(
      tag = "StateOnTrigger"
   )
   public boolean onTrigger;
   @RuleValue(
      tag = "StateOnUntrigger"
   )
   public boolean onUntrigger;

   public ActionTypes getType() {
      return ActionTypes.SEG_SET_AI;
   }

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLERSETAIACTION_0, this.onTrigger ? "on" : "off", this.onUntrigger ? "on" : "off");
   }

   public void onTrigger(SegmentController var1) {
      var1.railController.activateAllAIServer(this.onTrigger, true, true, true);
   }

   public void onUntrigger(SegmentController var1) {
      var1.railController.activateAllAIServer(this.onUntrigger, true, true, true);
   }
}
