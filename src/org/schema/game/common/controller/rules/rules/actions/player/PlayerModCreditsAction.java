package org.schema.game.common.controller.rules.rules.actions.player;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;

public class PlayerModCreditsAction extends PlayerAction {
   @RuleValue(
      tag = "Credits"
   )
   public int credits = 0;

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_PLAYER_PLAYERMODCREDITSACTION_0, this.credits);
   }

   public void onTrigger(PlayerState var1) {
      if (var1.isOnServer()) {
         var1.modCreditsServer((long)this.credits);
      }

   }

   public void onUntrigger(PlayerState var1) {
   }

   public ActionTypes getType() {
      return ActionTypes.PLAYER_MOD_CREDITS;
   }
}
