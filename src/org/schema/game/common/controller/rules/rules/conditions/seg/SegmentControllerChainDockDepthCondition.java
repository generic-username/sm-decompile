package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerChainDockDepthCondition extends SegmentControllerMoreLessCondition {
   @RuleValue(
      tag = "Entities"
   )
   public int entities;

   public long getTrigger() {
      return 131072L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_CHAIN_DOCK_DEPTH;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else if (this.moreThan) {
         return var3.railController.getDockingDepthFromHere() > this.entities;
      } else {
         return var3.railController.getDockingDepthFromHere() <= this.entities;
      }
   }

   public String getQuantifierString() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERCHAINDOCKDEPTHCONDITION_0;
   }

   public String getCountString() {
      return String.valueOf(this.entities);
   }
}
