package org.schema.game.common.controller.rules.rules.conditions.player;

import org.schema.game.common.controller.rules.rules.conditions.ConditionFactory;
import org.schema.schine.network.TopLevelType;

public abstract class PlayerConditionFactory implements ConditionFactory {
   public TopLevelType getType() {
      return TopLevelType.PLAYER;
   }
}
