package org.schema.game.common.controller.rules.rules;

import org.schema.game.common.data.world.RemoteSector;

public class SectorRuleEntityManager extends RuleEntityManager {
   public SectorRuleEntityManager(RemoteSector var1) {
      super(var1);
   }

   public byte getEntitySubType() {
      return 100;
   }

   public void triggerSectorChmod() {
      this.trigger(2048L);
   }
}
