package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.game.client.view.mainmenu.gui.ruleconfig.ConditionProvider;
import org.schema.game.common.controller.rules.rules.conditions.Condition;
import org.schema.game.common.controller.rules.rules.conditions.ConditionList;

public interface ConditionGroup extends ConditionProvider {
   ConditionList getConditions();

   boolean isAllTrue();

   void setAllTrue(boolean var1);

   void addCondition(Condition var1);

   void removeCondition(Condition var1);
}
