package org.schema.game.common.controller.rules.rules.conditions.seg;

import java.util.Date;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.TimedCondition;

public abstract class SegmentControllerAbstractDateTimeCondition extends SegmentControllerCondition implements TimedCondition {
   public long firstFired = -1L;
   public boolean flagTriggered;
   @RuleValue(
      tag = "FalseBeforeTrueAfter"
   )
   public boolean after = true;
   private boolean flagEndTriggered;

   public abstract Date getDate();

   public long getTrigger() {
      return 4L;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         long var7 = var3.getUpdateTime();
         boolean var9;
         if (!(var9 = this.isTimeToFire(var7)) && this.firstFired == -1L) {
            this.firstFired = var3.getState().getUpdateTime();
            var3.getRuleEntityManager().addDurationCheck(this);
            this.onFire();
         }

         if (var9) {
            return this.triggededAfterPassed();
         } else {
            return !this.triggededAfterPassed();
         }
      }
   }

   protected boolean triggededAfterPassed() {
      return this.after;
   }

   protected void onFire() {
   }

   public boolean isTimeToFire(long var1) {
      return var1 >= this.getDate().getTime();
   }

   public void flagTriggeredTimedCondition() {
      this.flagTriggered = true;
   }

   public boolean isTriggeredTimedCondition() {
      return this.flagTriggered;
   }

   public boolean isRemoveOnTriggered() {
      return false;
   }

   public boolean isTriggeredTimedEndCondition() {
      return this.flagEndTriggered;
   }

   public void flagTriggeredTimedEndCondition() {
      this.flagEndTriggered = true;
   }
}
