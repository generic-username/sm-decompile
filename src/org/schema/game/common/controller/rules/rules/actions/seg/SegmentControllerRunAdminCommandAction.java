package org.schema.game.common.controller.rules.rules.actions.seg;

import java.util.Locale;
import org.schema.common.util.StringTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.admin.AdminCommandIllegalArgument;
import org.schema.game.server.data.admin.AdminCommands;
import org.schema.schine.common.language.Lng;

public class SegmentControllerRunAdminCommandAction extends SegmentControllerAction {
   @RuleValue(
      tag = "AdminCommand"
   )
   public String cmd = "";

   public ActionTypes getType() {
      return ActionTypes.SEG_RUN_ADMIN_COMMAND;
   }

   public String getDescriptionShort() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLERRUNADMINCOMMANDACTION_0;
   }

   public void onTrigger(SegmentController var1) {
      if (var1.isOnServer()) {
         try {
            if (this.cmd.trim().length() > 0) {
               String var2;
               String[] var3 = (var2 = this.cmd.replaceAll("<uid>", var1.getUniqueIdentifier())).split("\\s+");
               AdminCommands var4 = (AdminCommands)Enum.valueOf(AdminCommands.class, var3[0].toUpperCase(Locale.ENGLISH));
               if ((var2 = var2.substring(var2.indexOf(var3[0]) + var3[0].length()).trim()).length() > 0) {
                  String[] var6 = StringTools.splitParameters(var2);
                  Object[] var7 = AdminCommands.packParameters(var4, var6);
                  ((GameServerState)var1.getState()).getController().enqueueAdminCommand(((GameServerState)var1.getState()).getAdminLocalClient(), var4, var7);
                  return;
               }

               if (var4.getTotalParameterCount() > 0) {
                  var2 = "need ";
                  if (var4.getRequiredParameterCount() != var4.getTotalParameterCount()) {
                     var2 = var2 + "minimum of " + var4.getRequiredParameterCount();
                  } else {
                     var2 = var2 + var4.getTotalParameterCount();
                  }

                  throw new AdminCommandIllegalArgument(var4, (String[])null, "No parameters provided: " + var2);
               }

               ((GameServerState)var1.getState()).getController().enqueueAdminCommand(((GameServerState)var1.getState()).getAdminLocalClient(), var4, new Object[0]);
            }

         } catch (Exception var5) {
            var5.printStackTrace();
            throw new RuntimeException(var5);
         }
      }
   }

   public void onUntrigger(SegmentController var1) {
   }
}
