package org.schema.game.common.controller.rules.rules.actions.player;

import java.io.IOException;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

public class PlayerKickAction extends PlayerAction {
   @RuleValue(
      tag = "Reason"
   )
   public String reason = "";

   public String getDescriptionShort() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_PLAYER_PLAYERKICKACTION_0;
   }

   public void onTrigger(PlayerState var1) {
      if (var1.isOnServer()) {
         try {
            ((GameServerState)var1.getState()).getController().sendLogout(var1.getClientId(), this.reason);
            return;
         } catch (IOException var2) {
            var2.printStackTrace();
         }
      }

   }

   public void onUntrigger(PlayerState var1) {
   }

   public ActionTypes getType() {
      return ActionTypes.PLAYER_KICK;
   }
}
