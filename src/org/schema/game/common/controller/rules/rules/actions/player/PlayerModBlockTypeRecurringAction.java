package org.schema.game.common.controller.rules.rules.actions.player;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;

public class PlayerModBlockTypeRecurringAction extends PlayerRecurringAction {
   @RuleValue(
      tag = "BlockType"
   )
   public int blockType = 0;
   @RuleValue(
      tag = "Amount"
   )
   public int amount = 0;
   @RuleValue(
      tag = "Seconds"
   )
   public int seconds;

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_PLAYER_PLAYERMODBLOCKTYPERECURRINGACTION_0, this.amount, ElementKeyMap.toString(this.blockType), this.seconds);
   }

   public ActionTypes getType() {
      return ActionTypes.PLAYER_MOD_BLOCK_TYPE_RECURRING;
   }

   public long getCheckInterval() {
      return (long)this.seconds * 1000L;
   }

   public void onActive(PlayerState var1) {
      if (var1.isOnServer() && ElementKeyMap.isValidType(this.blockType)) {
         int var2 = var1.getInventory().incExistingOrNextFreeSlot((short)this.blockType, this.amount);
         var1.sendInventoryModification(var2, Long.MIN_VALUE);
      }

   }
}
