package org.schema.game.common.controller.rules.rules.conditions.sector;

import java.util.Iterator;
import java.util.Set;
import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.controller.rules.rules.conditions.FactionRange;
import org.schema.game.common.controller.rules.rules.conditions.seg.EnumConditionEntityTypes;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.objects.Sendable;

public class SectorContainsCondition extends SectorMoreLessCondition {
   @RuleValue(
      tag = "Type"
   )
   public EnumConditionEntityTypes type;
   @RuleValue(
      tag = "Amount"
   )
   public int count;
   @RuleValue(
      tag = "FactionRange"
   )
   public FactionRange factionRange;

   public SectorContainsCondition() {
      this.type = EnumConditionEntityTypes.SHIP;
      this.factionRange = new FactionRange();
   }

   public long getTrigger() {
      return 2048L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SECTOR_CHMOD;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, RemoteSector var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         int var7 = 0;
         SimpleTransformableSendableObject var5;
         if (var3.isOnServer()) {
            Set var10000 = var3.getServerSector().getEntities();
            var2 = null;
            Iterator var9 = var10000.iterator();

            while(var9.hasNext()) {
               var5 = (SimpleTransformableSendableObject)var9.next();
               if (this.factionRange.isInRange(var5.getFactionId()) && var5.getSectorId() == var3.getId()) {
                  ++var7;
               }
            }
         } else {
            Iterator var8 = var3.getState().getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

            while(var8.hasNext()) {
               Sendable var10;
               if ((var10 = (Sendable)var8.next()) instanceof SimpleTransformableSendableObject) {
                  var5 = (SimpleTransformableSendableObject)var10;
                  if (this.factionRange.isInRange(var5.getFactionId()) && var5.getSectorId() == var3.getId()) {
                     ++var7;
                  }
               }
            }
         }

         if (this.moreThan) {
            return var7 > this.count;
         } else {
            return var7 <= this.count;
         }
      }
   }

   public String getCountString() {
      return String.valueOf(this.count);
   }

   public String getQuantifierString() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SECTOR_SECTORCONTAINSCONDITION_0, this.type.name(), this.factionRange);
   }
}
