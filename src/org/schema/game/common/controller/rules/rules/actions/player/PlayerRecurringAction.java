package org.schema.game.common.controller.rules.rules.actions.player;

import org.schema.game.common.controller.rules.rules.actions.RecurringAction;
import org.schema.game.common.data.player.PlayerState;

public abstract class PlayerRecurringAction extends PlayerAction implements RecurringAction {
   private long time;

   public void onTrigger(PlayerState var1) {
      if (var1.isOnServer()) {
         var1.getRuleEntityManager().addRecurringAction(this);
      }

   }

   public void onUntrigger(PlayerState var1) {
      if (var1.isOnServer()) {
         var1.getRuleEntityManager().removeRecurringAction(this);
      }

   }

   public long getLastActivate() {
      return this.time;
   }

   public void setLastActivate(long var1) {
      this.time = var1;
   }
}
