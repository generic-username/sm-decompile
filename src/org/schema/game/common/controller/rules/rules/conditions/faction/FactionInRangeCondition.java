package org.schema.game.common.controller.rules.rules.conditions.faction;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.controller.rules.rules.conditions.FactionRange;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.language.Lng;

public class FactionInRangeCondition extends FactionCondition {
   @RuleValue(
      tag = "Range"
   )
   public FactionRange range = new FactionRange();

   public long getTrigger() {
      return 1L;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, Faction var3, long var4, boolean var6) {
      return this.range.isInRange(var3.getIdFaction());
   }

   public ConditionTypes getType() {
      return ConditionTypes.FACTION_IS_RANGE;
   }

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_FACTION_FACTIONINRANGECONDITION_0, this.range);
   }
}
