package org.schema.game.common.controller.rules.rules.conditions.sector;

import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.Sector;

public class SectorChmodCondition extends SectorCondition {
   @RuleValue(
      tag = "NoAttack"
   )
   public boolean noAttack;
   @RuleValue(
      tag = "NoSpawn"
   )
   public boolean noSpawn;
   @RuleValue(
      tag = "NoIndicators"
   )
   public boolean noIndicators;
   @RuleValue(
      tag = "NoEntry"
   )
   public boolean noEntry;
   @RuleValue(
      tag = "NoExit"
   )
   public boolean noExit;
   @RuleValue(
      tag = "NoFPLoss"
   )
   public boolean noFPLoss;

   public long getTrigger() {
      return 2048L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SECTOR_CHMOD;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, RemoteSector var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         int var7;
         if (var3.isOnServer()) {
            var7 = var3.getServerSector().getProtectionMode();
         } else {
            var7 = var3.getSectorModeClient();
         }

         int var8 = this.getMask();
         return (var7 & var8) == var8;
      }
   }

   public int getMask() {
      return 0 | (this.noAttack ? Sector.SectorMode.PROT_NO_ATTACK.code : 0) | (this.noSpawn ? Sector.SectorMode.PROT_NO_SPAWN.code : 0) | (this.noIndicators ? Sector.SectorMode.NO_INDICATIONS.code : 0) | (this.noEntry ? Sector.SectorMode.LOCK_NO_ENTER.code : 0) | (this.noExit ? Sector.SectorMode.LOCK_NO_EXIT.code : 0) | (this.noFPLoss ? Sector.SectorMode.NO_FP_LOSS.code : 0);
   }

   public String getDescriptionShort() {
      return Sector.getPermissionString(this.getMask());
   }
}
