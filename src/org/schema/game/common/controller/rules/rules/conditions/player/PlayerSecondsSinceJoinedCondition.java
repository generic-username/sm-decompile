package org.schema.game.common.controller.rules.rules.conditions.player;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.controller.rules.rules.conditions.TimedCondition;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;

public class PlayerSecondsSinceJoinedCondition extends PlayerCondition implements TimedCondition {
   @RuleValue(
      tag = "Seconds"
   )
   public int durationInSecs;
   public long firstFired = -1L;
   public boolean flagTriggered;
   private boolean flagEndTriggered;

   public long getTrigger() {
      return 4L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.PLAYER_JOINED_AFTER_SECS;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, PlayerState var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         if (this.firstFired == -1L) {
            this.firstFired = var3.getState().getUpdateTime();
            var3.getRuleEntityManager().addDurationCheck(this);
         }

         long var7 = var3.getState().getUpdateTime();
         return this.isTimeToFire(var7);
      }
   }

   public boolean isTimeToFire(long var1) {
      return var1 >= this.firstFired + (long)(this.durationInSecs * 1000);
   }

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_PLAYER_PLAYERSECONDSSINCEJOINEDCONDITION_0, this.durationInSecs);
   }

   public void flagTriggeredTimedCondition() {
      this.flagTriggered = true;
   }

   public boolean isTriggeredTimedCondition() {
      return this.flagTriggered;
   }

   public boolean isRemoveOnTriggered() {
      return true;
   }

   public boolean isTriggeredTimedEndCondition() {
      return this.flagEndTriggered;
   }

   public void flagTriggeredTimedEndCondition() {
      this.flagEndTriggered = true;
   }
}
