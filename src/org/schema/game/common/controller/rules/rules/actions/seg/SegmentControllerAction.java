package org.schema.game.common.controller.rules.rules.actions.seg;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.rules.actions.Action;
import org.schema.game.common.data.world.RuleEntityContainer;
import org.schema.schine.network.TopLevelType;

public abstract class SegmentControllerAction extends Action {
   public void onTrigger(RuleEntityContainer var1, TopLevelType var2) {
      assert this.getEntityType() == var2;

      SegmentController var3 = (SegmentController)var1;
      this.onTrigger(var3);
   }

   public void onUntrigger(RuleEntityContainer var1, TopLevelType var2) {
      assert this.getEntityType() == var2;

      SegmentController var3 = (SegmentController)var1;
      this.onUntrigger(var3);
   }
}
