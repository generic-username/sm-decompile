package org.schema.game.common.controller.rules.rules.actions.seg;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerVoidAction extends SegmentControllerAction {
   public ActionTypes getType() {
      return ActionTypes.DO_NOTHING;
   }

   public String getDescriptionShort() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLERVOIDACTION_0;
   }

   public void onTrigger(SegmentController var1) {
   }

   public void onUntrigger(SegmentController var1) {
   }
}
