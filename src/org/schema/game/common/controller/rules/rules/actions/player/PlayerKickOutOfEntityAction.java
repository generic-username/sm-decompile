package org.schema.game.common.controller.rules.rules.actions.player;

import java.util.Iterator;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rails.RailRelation;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

public class PlayerKickOutOfEntityAction extends PlayerAction {
   public String getDescriptionShort() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_PLAYER_PLAYERKICKOUTOFENTITYACTION_0;
   }

   public void onTrigger(PlayerState var1) {
      if (var1.isOnServer() && var1.getFirstControlledTransformableWOExc() instanceof SegmentController) {
         this.kickPlayersOut((GameServerState)var1.getState(), (SegmentController)var1.getFirstControlledTransformableWOExc(), true, var1);
      }

   }

   public void onUntrigger(PlayerState var1) {
   }

   public ActionTypes getType() {
      return ActionTypes.PLAYER_KICK_OUT_OF_ENTITY;
   }

   public void kickPlayersOut(GameServerState var1, SegmentController var2, boolean var3, PlayerState var4) {
      if (var4 == null) {
         var2.kickAllPlayersOutServer();
      } else {
         var2.kickPlayerOutServer(var4);
      }

      if (var3) {
         Iterator var6 = var2.railController.next.iterator();

         while(var6.hasNext()) {
            RailRelation var5 = (RailRelation)var6.next();
            this.kickPlayersOut(var1, var5.docked.getSegmentController(), var3, var4);
         }
      }

   }
}
