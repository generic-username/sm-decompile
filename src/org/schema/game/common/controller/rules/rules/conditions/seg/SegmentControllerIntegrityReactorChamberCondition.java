package org.schema.game.common.controller.rules.rules.conditions.seg;

import java.util.Iterator;
import java.util.List;
import org.schema.game.common.controller.elements.ManagerModuleSingle;
import org.schema.game.common.controller.elements.power.reactor.chamber.ReactorChamberCollectionManager;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.ManagedSegmentController;

public class SegmentControllerIntegrityReactorChamberCondition extends SegmentControllerAbstractIntegrityCondition {
   public double getSmallestIntegrity(ManagedSegmentController var1) {
      List var5 = var1.getManagerContainer().getPowerInterface().getChambers();
      double var3 = Double.POSITIVE_INFINITY;

      ManagerModuleSingle var2;
      for(Iterator var6 = var5.iterator(); var6.hasNext(); var3 = Math.min(var3, ((ReactorChamberCollectionManager)var2.getCollectionManager()).getLowestIntegrity())) {
         var2 = (ManagerModuleSingle)var6.next();
      }

      return var3;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_INTEGRITY_CHAMBER_CONDITION;
   }

   public long getTrigger() {
      return 73728L;
   }

   public String getQuantifierString() {
      return "Reactor Chamber Integrity";
   }
}
