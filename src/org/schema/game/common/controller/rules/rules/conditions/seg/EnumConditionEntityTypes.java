package org.schema.game.common.controller.rules.rules.conditions.seg;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Set;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;

public enum EnumConditionEntityTypes {
   SHIP(new SimpleTransformableSendableObject.EntityType[]{SimpleTransformableSendableObject.EntityType.SHIP}),
   TURRET(new SimpleTransformableSendableObject.EntityType[]{SimpleTransformableSendableObject.EntityType.SHIP}),
   STATION(new SimpleTransformableSendableObject.EntityType[]{SimpleTransformableSendableObject.EntityType.SPACE_STATION}),
   PLANET(new SimpleTransformableSendableObject.EntityType[]{SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT, SimpleTransformableSendableObject.EntityType.PLANET_CORE, SimpleTransformableSendableObject.EntityType.PLANET_ICO}),
   SHOP(new SimpleTransformableSendableObject.EntityType[]{SimpleTransformableSendableObject.EntityType.SHOP}),
   ASTEROID(new SimpleTransformableSendableObject.EntityType[]{SimpleTransformableSendableObject.EntityType.ASTEROID});

   private final Set e = new ObjectOpenHashSet();

   private EnumConditionEntityTypes(SimpleTransformableSendableObject.EntityType... var3) {
      SimpleTransformableSendableObject.EntityType[] var5 = var3;
      var2 = var3.length;

      for(int var6 = 0; var6 < var2; ++var6) {
         SimpleTransformableSendableObject.EntityType var4 = var5[var6];
         this.e.add(var4);
      }

   }

   public final boolean isType(SimpleTransformableSendableObject var1) {
      if (!this.e.contains(var1.getType())) {
         return false;
      } else {
         if (var1 instanceof SegmentController) {
            SegmentController var2 = (SegmentController)var1;
            if (this == TURRET && var2.railController.isRoot()) {
               return false;
            }

            if (!var2.railController.isRoot()) {
               return false;
            }
         }

         return true;
      }
   }
}
