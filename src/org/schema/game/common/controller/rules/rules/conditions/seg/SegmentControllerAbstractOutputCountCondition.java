package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.data.ManagedSegmentController;

public abstract class SegmentControllerAbstractOutputCountCondition extends SegmentControllerMoreLessCondition {
   @RuleValue(
      tag = "Count"
   )
   public int count;

   public long getTrigger() {
      return 8192L;
   }

   public abstract double getOutputCount(ManagedSegmentController var1);

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else if (var3 instanceof ManagedSegmentController) {
         if (this.moreThan) {
            return this.getOutputCount((ManagedSegmentController)var3) > (double)this.count;
         } else {
            return this.getOutputCount((ManagedSegmentController)var3) <= (double)this.count;
         }
      } else {
         return false;
      }
   }

   public String getCountString() {
      return String.valueOf(this.count);
   }
}
