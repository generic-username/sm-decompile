package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerAIActiveCondition extends SegmentControllerCondition {
   @RuleValue(
      tag = "IsActive"
   )
   public boolean active;

   public long getTrigger() {
      return 2L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_IS_AI_ACTIVE;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         return this.active == var3.isAIControlled();
      }
   }

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERAIACTIVECONDITION_0, StringTools.getOnOff(this.active));
   }
}
