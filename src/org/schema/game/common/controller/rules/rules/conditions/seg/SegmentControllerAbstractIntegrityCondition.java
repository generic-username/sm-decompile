package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.data.ManagedSegmentController;

public abstract class SegmentControllerAbstractIntegrityCondition extends SegmentControllerMoreLessCondition {
   @RuleValue(
      tag = "Integrity"
   )
   public double integrity;

   public long getTrigger() {
      return 8192L;
   }

   public abstract double getSmallestIntegrity(ManagedSegmentController var1);

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else if (var3 instanceof ManagedSegmentController) {
         double var7 = this.getSmallestIntegrity((ManagedSegmentController)var3);
         if (this.moreThan) {
            return var7 > this.integrity;
         } else {
            return var7 <= this.integrity;
         }
      } else {
         return false;
      }
   }

   public String getCountString() {
      return String.valueOf(this.integrity);
   }
}
