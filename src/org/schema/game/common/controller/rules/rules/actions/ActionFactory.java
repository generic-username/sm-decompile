package org.schema.game.common.controller.rules.rules.actions;

import org.schema.schine.network.TopLevelType;

public interface ActionFactory {
   Action instantiateAction();

   TopLevelType getType();
}
