package org.schema.game.common.controller.rules.rules.conditions.player;

import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;

public class PlayerInSectorCondition extends PlayerCondition {
   @RuleValue(
      tag = "FromX"
   )
   public int xFrom;
   @RuleValue(
      tag = "ToX"
   )
   public int xTo;
   @RuleValue(
      tag = "FromY"
   )
   public int yFrom;
   @RuleValue(
      tag = "ToY"
   )
   public int yTo;
   @RuleValue(
      tag = "FromZ"
   )
   public int zFrom;
   @RuleValue(
      tag = "ToZ"
   )
   public int zTo;

   public long getTrigger() {
      return 32L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.PLAYER_IN_SECTOR;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, PlayerState var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         Vector3i var7;
         return (var7 = var3.getCurrentSector()).x >= this.xFrom && var7.x <= this.xTo && var7.y >= this.yFrom && var7.y <= this.yTo && var7.z >= this.zFrom && var7.z <= this.zTo;
      }
   }

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_PLAYER_PLAYERINSECTORCONDITION_0, this.xFrom, this.xTo, this.yFrom, this.yTo, this.zFrom, this.zTo);
   }
}
