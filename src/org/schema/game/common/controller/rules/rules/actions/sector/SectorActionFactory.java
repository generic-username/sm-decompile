package org.schema.game.common.controller.rules.rules.actions.sector;

import org.schema.game.common.controller.rules.rules.actions.ActionFactory;
import org.schema.schine.network.TopLevelType;

public abstract class SectorActionFactory implements ActionFactory {
   public TopLevelType getType() {
      return TopLevelType.SECTOR;
   }
}
