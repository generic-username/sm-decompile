package org.schema.game.common.controller.rules.rules.actions.player;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;

public class PlayerModBlockTypeAction extends PlayerAction {
   @RuleValue(
      tag = "BlockType"
   )
   public int blockType = 0;
   @RuleValue(
      tag = "Amount"
   )
   public int amount = 0;

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_PLAYER_PLAYERMODBLOCKTYPEACTION_0, this.amount, ElementKeyMap.toString(this.blockType));
   }

   public void onTrigger(PlayerState var1) {
      if (var1.isOnServer() && ElementKeyMap.isValidType(this.blockType)) {
         int var2 = var1.getInventory().incExistingOrNextFreeSlot((short)this.blockType, this.amount);
         var1.sendInventoryModification(var2, Long.MIN_VALUE);
      }

   }

   public void onUntrigger(PlayerState var1) {
   }

   public ActionTypes getType() {
      return ActionTypes.PLAYER_MOD_BLOCK_TYPE;
   }
}
