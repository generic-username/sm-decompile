package org.schema.game.common.controller.rules.rules;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.rules.RuleSet;
import org.schema.game.common.controller.rules.RuleSetManager;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.actions.ActionUpdate;
import org.schema.game.common.controller.rules.rules.actions.RecurringAction;
import org.schema.game.common.controller.rules.rules.conditions.TimedCondition;
import org.schema.game.common.data.world.RuleEntityContainer;
import org.schema.game.network.objects.NTRuleInterface;
import org.schema.game.network.objects.remote.RemoteRuleStateChange;
import org.schema.game.network.objects.remote.RemoteRuleStateChangeBuffer;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.RemoteIntBuffer;
import org.schema.schine.network.objects.remote.RemoteString;
import org.schema.schine.network.objects.remote.Streamable;

public abstract class RuleEntityManager extends Observable {
   private static final String ADD_INDIVIDUAL = "ADDIN_";
   private static final String REMOVE_INDIVIDUAL = "DELIN_";
   private static final String ADD_IGNORE = "ADDIG_";
   private static final String REMOVE_IGNORE = "DELIG_";
   public final RuleEntityContainer entity;
   private final List recurringActions = new ObjectArrayList();
   private final List updates = new ObjectArrayList();
   public final Int2ObjectOpenHashMap ruleIdMap = new Int2ObjectOpenHashMap();
   public final Int2ObjectOpenHashMap ruleIdDifferentTypeDebugMap = new Int2ObjectOpenHashMap();
   public final ObjectArrayList rulesActiveTotal = new ObjectArrayList();
   public final ObjectArrayList rulesTotal = new ObjectArrayList();
   public final ObjectArrayList globalRules = new ObjectArrayList();
   public final ObjectArrayList individualRules = new ObjectArrayList();
   private final ObjectArrayFIFOQueue receivedRuleStates = new ObjectArrayFIFOQueue();
   private final ObjectArrayFIFOQueue receivedRuleIndividualRequests = new ObjectArrayFIFOQueue();
   private final IntArrayFIFOQueue receivedRuleStateRequests = new IntArrayFIFOQueue();
   private final Object2ObjectOpenHashMap individualRulesUIDMap = new Object2ObjectOpenHashMap();
   private final Object2ObjectOpenHashMap individualGlobalRuleExceptionUIDMap = new Object2ObjectOpenHashMap();
   private final ObjectArrayList timedConditions = new ObjectArrayList();
   private long trigger = 0L;
   private boolean flagRuleChanged = true;
   private boolean ruleChangePrepare;

   public List getActiveRules() {
      return this.rulesActiveTotal;
   }

   public void triggerRuleStateChanged() {
      this.trigger(64L);
   }

   public void triggerTimedCondition() {
      this.trigger(4L);
   }

   public void triggerOnAIActivityChange() {
      this.trigger(2L);
   }

   public void triggerOnFactionChange() {
      this.trigger(16L);
   }

   public void triggerOnAttack() {
      this.trigger(8L);
   }

   public void trigger(long var1) {
      assert this.isOnServer();

      this.trigger |= var1;
   }

   public void addDurationCheck(TimedCondition var1) {
      this.timedConditions.add(var1);
   }

   public void removeDurationCheck(TimedCondition var1) {
      this.timedConditions.remove(var1);
   }

   public void onRulesChanged() throws IOException {
      this.updates.clear();
      this.rulesActiveTotal.clear();
      this.rulesTotal.clear();
      this.globalRules.clear();
      this.individualRules.clear();
      this.receivedRuleStates.clear();
      this.ruleIdMap.clear();
      this.ruleIdDifferentTypeDebugMap.clear();
      List var1 = ((GameStateInterface)this.entity.getState()).getGameState().getRuleManager().getGlobalRules(this.getEntitySubType(), new ObjectArrayList());
      System.err.println(this.getState() + "; " + this.entity + " ON RULE CHANGED: " + var1);
      Iterator var5 = var1.iterator();

      while(true) {
         RuleSet var3;
         Iterator var10;
         while(var5.hasNext()) {
            RuleSet var2 = (RuleSet)var5.next();
            var3 = new RuleSet(var2, var2.getUniqueIdentifier());
            this.globalRules.add(var3);
            if (this.individualGlobalRuleExceptionUIDMap.containsKey(var2.getUniqueIdentifier().toLowerCase(Locale.ENGLISH))) {
               for(var10 = var3.iterator(); var10.hasNext(); ((Rule)var10.next()).ignoreRule = true) {
               }
            } else {
               for(var10 = var3.iterator(); var10.hasNext(); ((Rule)var10.next()).ignoreRule = false) {
               }
            }
         }

         ObjectIterator var6 = this.individualRulesUIDMap.values().iterator();
         System.err.println(this.getState() + " " + this.entity + " [ONRULECHANGE] INDIVIDUAL RULES " + this.individualRulesUIDMap.size() + "; MAP: " + this.individualRulesUIDMap);

         while(true) {
            while(var6.hasNext()) {
               String var8 = (String)var6.next();
               if ((var3 = ((GameStateInterface)this.entity.getState()).getGameState().getRuleManager().getRuleSetByUID(var8)) == null) {
                  try {
                     throw new Exception("WARNING. no ruleset found for " + var8);
                  } catch (Exception var4) {
                     var4.printStackTrace();
                     var6.remove();
                  }
               } else {
                  var3 = new RuleSet(var3, var3.getUniqueIdentifier());
                  this.individualRules.add(var3);
               }
            }

            Iterator var9 = this.globalRules.iterator();

            Rule var7;
            while(var9.hasNext()) {
               var10 = ((RuleSet)var9.next()).iterator();

               while(var10.hasNext()) {
                  var7 = (Rule)var10.next();
                  if (this.entity.getTopLevelType() == var7.ruleType) {
                     if (!var7.ignoreRule) {
                        this.rulesActiveTotal.add(var7);
                     }

                     this.rulesTotal.add(var7);
                  } else {
                     this.ruleIdDifferentTypeDebugMap.put(var7.getRuleId(), var7);
                  }
               }
            }

            var9 = this.individualRules.iterator();

            while(var9.hasNext()) {
               var10 = ((RuleSet)var9.next()).iterator();

               while(var10.hasNext()) {
                  var7 = (Rule)var10.next();
                  if (this.entity.getTopLevelType() == var7.ruleType) {
                     if (!var7.ignoreRule) {
                        this.rulesActiveTotal.add(var7);
                     }

                     this.rulesTotal.add(var7);
                  } else {
                     this.ruleIdDifferentTypeDebugMap.put(var7.getRuleId(), var7);
                  }
               }
            }

            System.err.println(this.getState() + " " + this.entity + " [ONRULECHANGE] RESULT: TOTAL: " + this.rulesTotal.size() + "; globalSets: " + this.globalRules.size() + "; individualSets: " + this.individualRules.size() + ";");
            var9 = this.rulesTotal.iterator();

            while(var9.hasNext()) {
               Rule var11 = (Rule)var9.next();
               this.ruleIdMap.put(var11.getRuleId(), var11);
            }

            if (!this.entity.isOnServer()) {
               this.requestRuleStatesFromServer();
            } else {
               System.err.println("CHEKCING ALL RULES FOR SERVER");
               this.checkAllRules();
            }

            this.setChanged();
            this.notifyObservers();
            this.flagRuleChanged = false;
            return;
         }
      }
   }

   public StateInterface getState() {
      return this.entity.getState();
   }

   public abstract byte getEntitySubType();

   private void checkRules(long var1) {
      Iterator var3 = this.getActiveRules().iterator();

      while(var3.hasNext()) {
         ((Rule)var3.next()).process(this.entity, this.entity.getTopLevelType(), var1);
      }

   }

   private void checkAllRules() {
      this.checkRules(-1L);
   }

   private void requestRuleStatesFromServer() {
      Iterator var1 = this.getActiveRules().iterator();

      while(var1.hasNext()) {
         ((Rule)var1.next()).requestRuleState(this.entity);
      }

   }

   public RuleEntityManager(RuleEntityContainer var1) {
      this.entity = var1;
   }

   public void update(Timer var1) {
      if (!this.isOnServer() && this.ruleChangePrepare) {
         System.err.println("[CLIENT] RuleChange Received but not applied in game state yet. Not executing rule updates");
      } else if (!this.isOnServer() && !this.getRuleSetManager().receivedInitialOnClient) {
         System.err.println("Not Updating " + this.isOnServer());
      } else {
         if (this.flagRuleChanged) {
            try {
               this.onRulesChanged();
            } catch (IOException var6) {
               var6.printStackTrace();
               this.flagRuleChanged = false;
            }
         }

         while(!this.receivedRuleStates.isEmpty()) {
            this.processReceivedState((RuleStateChange)this.receivedRuleStates.dequeue());
         }

         Iterator var2 = this.updates.iterator();

         while(var2.hasNext()) {
            ((ActionUpdate)var2.next()).update(var1);
         }

         if (this.trigger != 0L) {
            assert this.isOnServer();

            this.checkRules(this.trigger);
            this.trigger = 0L;
         }

         int var9;
         for(var9 = 0; var9 < this.recurringActions.size(); ++var9) {
            RecurringAction var3 = (RecurringAction)this.recurringActions.get(var9);
            if (var1.currentTime - var3.getLastActivate() > var3.getCheckInterval()) {
               var3.onActive(this.entity);
               var3.setLastActivate(var1.currentTime);
            }
         }

         while(!this.receivedRuleIndividualRequests.isEmpty()) {
            String var10 = (String)this.receivedRuleIndividualRequests.dequeue();
            this.processIndividualProcess(var10, this.isOnServer());
         }

         while(!this.receivedRuleStateRequests.isEmpty()) {
            assert this.isOnServer() : "Rule State Request must have been on client and may only be sent from server " + this.entity;

            var9 = this.receivedRuleStateRequests.dequeueInt();
            Rule var11;
            if ((var11 = (Rule)this.ruleIdMap.get(var9)) != null) {
               var11.sendCompleteRuleState(this.entity);
            } else if ((Rule)this.ruleIdDifferentTypeDebugMap.get(var9) == null) {
               try {
                  throw new Exception("Rule state request for nonexiting rule: " + this.entity + "; ruleId: " + var9 + "; " + this.ruleIdMap);
               } catch (Exception var7) {
                  var7.printStackTrace();
               }
            }
         }

         if (this.isOnServer()) {
            for(var9 = 0; var9 < this.timedConditions.size(); ++var9) {
               TimedCondition var12 = (TimedCondition)this.timedConditions.get(var9);
               long var4 = this.entity.getState().getUpdateTime();
               boolean var8;
               if ((var8 = var12.isTimeToFire(var4)) && !var12.isTriggeredTimedCondition()) {
                  var12.flagTriggeredTimedCondition();
                  this.triggerTimedCondition();
               } else if (!var8 && var12.isTriggeredTimedCondition() && !var12.isTriggeredTimedEndCondition()) {
                  var12.flagTriggeredTimedEndCondition();
                  this.triggerTimedCondition();
               }

               if (var12.isRemoveOnTriggered() && var12.isTriggeredTimedCondition()) {
                  this.timedConditions.remove(var9);
                  --var9;
               }
            }
         }

      }
   }

   private void processIndividualProcess(String var1, boolean var2) {
      String var3;
      if (var1.startsWith("ADDIN_")) {
         var3 = var1.substring(6);

         assert this.getRuleSetManager().getRuleSetByUID(var3) != null : var3;

         this.individualRulesUIDMap.put(var3.toLowerCase(Locale.ENGLISH), var3);
         System.err.println(this.getState() + " " + this.entity + " [RULEENTITYMANAGER] RECEIVED ADD INDIVIDUAL ADD " + var3 + " deligate: " + var2);
      } else {
         String var4;
         if (var1.startsWith("DELIN_")) {
            var3 = var1.substring(6);
            var4 = (String)this.individualRulesUIDMap.remove(var3.toLowerCase(Locale.ENGLISH));

            assert var4 != null : var3;

            System.err.println(this.getState() + " " + this.entity + " [RULEENTITYMANAGER] RECEIVED REMOVE INDIVIDUAL ADD " + var3 + " deligate: " + var2);
         } else if (var1.startsWith("ADDIG_")) {
            var3 = var1.substring(6);

            assert this.getRuleSetManager().getRuleSetByUID(var3) != null : var3;

            this.individualGlobalRuleExceptionUIDMap.put(var3.toLowerCase(Locale.ENGLISH), var3);
            System.err.println(this.getState() + " " + this.entity + " [RULEENTITYMANAGER] RECEIVED IGNORE ADD " + var3 + " deligate: " + var2);
         } else {
            if (!var1.startsWith("DELIG_")) {
               throw new RuntimeException("Unknown individual Rule command: " + var1);
            }

            var3 = var1.substring(6);
            var4 = (String)this.individualGlobalRuleExceptionUIDMap.remove(var3.toLowerCase(Locale.ENGLISH));

            assert var4 != null : var3;

            System.err.println(this.getState() + " " + this.entity + " [RULEENTITYMANAGER] RECEIVED IGNORE REMOVE " + var3 + " deligate: " + var2);
         }
      }

      if (var2) {
         this.entity.getNetworkObject().getRuleIndividualAddRemoveBuffer().add((Streamable)(new RemoteString(var1, this.isOnServer())));
      }

      this.flagRuleChanged();
   }

   public void flagRuleChanged() {
      this.ruleChangePrepare = false;
      this.flagRuleChanged = true;
   }

   private boolean isOnServer() {
      return this.entity.isOnServer();
   }

   public boolean existsUpdatableAction(ActionUpdate var1) {
      for(int var2 = 0; var2 < this.updates.size(); ++var2) {
         if (((ActionUpdate)this.updates.get(var2)).getAction() == var1.getAction()) {
            return true;
         }
      }

      return false;
   }

   public void addUpdatableAction(ActionUpdate var1) {
      if (!this.existsUpdatableAction(var1)) {
         var1.onAdd();
         this.updates.add(var1);
      }

   }

   public void removeUpdatableAction(ActionUpdate var1) {
      for(int var2 = 0; var2 < this.updates.size(); ++var2) {
         if (((ActionUpdate)this.updates.get(var2)).getAction() == var1.getAction()) {
            ((ActionUpdate)this.updates.get(var2)).onRemove();
            this.updates.remove(var2);
            return;
         }
      }

   }

   public void sendRuleStateChange(Rule var1, RuleStateChange var2) {
      assert this.isOnServer();

      RuleStateChange var3 = new RuleStateChange(var1, var2);

      assert this.entity.getNetworkObject() instanceof NTRuleInterface;

      this.entity.getNetworkObject().getRuleStateChangeBuffer().add(new RemoteRuleStateChange(var3, this.entity.isOnServer()));
   }

   public void receive(NTRuleInterface var1) {
      RemoteRuleStateChangeBuffer var2 = var1.getRuleStateChangeBuffer();

      int var3;
      for(var3 = 0; var3 < var2.getReceiveBuffer().size(); ++var3) {
         RuleStateChange var4 = (RuleStateChange)((RemoteRuleStateChange)var2.getReceiveBuffer().get(var3)).get();
         this.receivedRuleStates.enqueue(var4);
      }

      RemoteIntBuffer var5 = var1.getRuleStateRequestBuffer();

      for(var3 = 0; var3 < var5.getReceiveBuffer().size(); ++var3) {
         int var7 = var5.getReceiveBuffer().get(var3);
         this.receivedRuleStateRequests.enqueue(var7);
      }

      RemoteBuffer var6 = var1.getRuleIndividualAddRemoveBuffer();

      for(var3 = 0; var3 < var6.getReceiveBuffer().size(); ++var3) {
         String var8 = (String)((RemoteString)var6.getReceiveBuffer().get(var3)).get();
         this.receivedRuleIndividualRequests.enqueue(var8);
      }

   }

   public RuleSetManager getRuleSetManager() {
      return ((GameStateInterface)this.entity.getState()).getGameState().getRuleManager();
   }

   private void processReceivedState(RuleStateChange var1) {
      Rule var2;
      if ((var2 = (Rule)this.ruleIdMap.get(var1.ruleIdNT)) != null) {
         var2.receiveState(this.entity, this.entity.getTopLevelType(), var1);
      } else {
         if ((Rule)this.ruleIdDifferentTypeDebugMap.get(var1.ruleIdNT) == null) {
            try {
               throw new Exception("Rule ID not found for " + this.entity + "; ID: " + var1.ruleIdNT + "; ID MAP: " + this.ruleIdMap);
            } catch (Exception var3) {
               var3.printStackTrace();
            }
         }

      }
   }

   public void addIndividualRuleSet(RuleSet var1) {
      this.entity.getNetworkObject().getRuleIndividualAddRemoveBuffer().add((Streamable)(new RemoteString("ADDIN_" + var1.uniqueIdentifier, this.isOnServer())));
      System.err.println("SENDING ADD INDIVIDUAL RULE " + var1.uniqueIdentifier);
   }

   public void removeIndividualRuleSet(RuleSet var1) {
      this.entity.getNetworkObject().getRuleIndividualAddRemoveBuffer().add((Streamable)(new RemoteString("DELIN_" + var1.uniqueIdentifier, this.isOnServer())));
      System.err.println("SENDING REMOVE INDIVIDUAL RULE " + var1.uniqueIdentifier);
   }

   public void addIgnoreRuleSet(RuleSet var1) {
      this.entity.getNetworkObject().getRuleIndividualAddRemoveBuffer().add((Streamable)(new RemoteString("ADDIG_" + var1.uniqueIdentifier, this.isOnServer())));
      System.err.println("SENDING ADD IGNORE RULE " + var1.uniqueIdentifier);
   }

   public void removeIgnoreRuleSet(RuleSet var1) {
      this.entity.getNetworkObject().getRuleIndividualAddRemoveBuffer().add((Streamable)(new RemoteString("DELIG_" + var1.uniqueIdentifier, this.isOnServer())));
      System.err.println("SENDING REMOVE IGNORE RULE " + var1.uniqueIdentifier);
   }

   public void removeIndividualRuleSetByRule(Rule var1) {
      Iterator var2 = this.individualRules.iterator();

      while(var2.hasNext()) {
         RuleSet var3;
         if ((var3 = (RuleSet)var2.next()).contains(var1)) {
            this.removeIndividualRuleSet(var3);
         }
      }

   }

   public void addIgnorelRuleSetByRule(Rule var1) {
      Iterator var2 = this.globalRules.iterator();

      while(var2.hasNext()) {
         RuleSet var3;
         if ((var3 = (RuleSet)var2.next()).contains(var1)) {
            this.addIgnoreRuleSet(var3);
         }
      }

   }

   public void removeIgnorelRuleSetByRule(Rule var1) {
      Iterator var2 = this.globalRules.iterator();

      while(var2.hasNext()) {
         RuleSet var3;
         if ((var3 = (RuleSet)var2.next()).contains(var1) && var1.ruleType == this.entity.getTopLevelType()) {
            this.removeIgnoreRuleSet(var3);
         }
      }

   }

   public void flagRuleChangePrepare() {
      this.ruleChangePrepare = true;
   }

   public void addRecurringAction(RecurringAction var1) {
      this.recurringActions.add(var1);
   }

   public void removeRecurringAction(RecurringAction var1) {
      if (!this.recurringActions.remove(var1)) {
         System.err.println(this.getState() + " [WARNING] RECURRING ACTION NOT REMOVED: " + this.recurringActions + "; " + var1);
      }

   }
}
