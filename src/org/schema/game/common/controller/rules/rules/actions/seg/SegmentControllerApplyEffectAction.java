package org.schema.game.common.controller.rules.rules.actions.seg;

import java.util.Locale;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.data.blockeffects.config.ConfigGroup;
import org.schema.schine.common.language.Lng;

public class SegmentControllerApplyEffectAction extends SegmentControllerAction {
   @RuleValue(
      tag = "EffectUID"
   )
   public String effectUID = "";

   public ActionTypes getType() {
      return ActionTypes.SEG_APPLY_EFFECT;
   }

   public String getDescriptionShort() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLERAPPLYEFFECTACTION_0;
   }

   public void onTrigger(SegmentController var1) {
      if (this.effectUID.trim().length() > 0 && var1.isOnServer()) {
         ConfigGroup var2;
         if ((var2 = (ConfigGroup)var1.getConfigManager().getConfigPool().poolMapLowerCase.get(this.effectUID.toLowerCase(Locale.ENGLISH))) != null) {
            var1.getConfigManager().addEffectAndSend(var2, true, var1.getNetworkObject());
            return;
         }

         System.err.println("[SERVER][RULE][ACTION][APPLYEFFECT][ERROR] Can't add effect to " + var1 + ". Effect UID not found: " + this.effectUID);
      }

   }

   public void onUntrigger(SegmentController var1) {
      if (this.effectUID.trim().length() > 0 && var1.isOnServer()) {
         ConfigGroup var2;
         if ((var2 = (ConfigGroup)var1.getConfigManager().getConfigPool().poolMapLowerCase.get(this.effectUID.toLowerCase(Locale.ENGLISH))) != null) {
            var1.getConfigManager().removeEffectAndSend(var2, true, var1.getNetworkObject());
            return;
         }

         System.err.println("[SERVER][RULE][ACTION][APPLYEFFECT][ERROR] Can't remove effect from " + var1 + ". UID not found: " + this.effectUID);
      }

   }
}
