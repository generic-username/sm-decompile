package org.schema.game.common.controller.rules.rules.actions.seg;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.controller.rules.rules.actions.sector.SectorActionList;
import org.schema.game.common.data.world.Sector;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

public class SegmentController2SectorAction extends SegmentControllerAction {
   @RuleValue(
      tag = "Actions"
   )
   public SectorActionList actions = new SectorActionList();

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLER2SECTORACTION_0, this.actions.size());
   }

   public void onTrigger(SegmentController var1) {
      Sector var2;
      if (var1.isOnServer() && (var2 = ((GameServerState)var1.getState()).getUniverse().getSector(var1.getSectorId())) != null) {
         this.actions.onTrigger(var2.getRemoteSector());
      }

   }

   public void onUntrigger(SegmentController var1) {
      Sector var2;
      if (var1.isOnServer() && (var2 = ((GameServerState)var1.getState()).getUniverse().getSector(var1.getSectorId())) != null) {
         this.actions.onUntrigger(var2.getRemoteSector());
      }

   }

   public ActionTypes getType() {
      return ActionTypes.SEG_2_SECTOR;
   }
}
