package org.schema.game.common.controller.rules.rules.conditions.seg;

import java.util.Iterator;
import org.schema.game.common.controller.elements.WeaponManagerInterface;
import org.schema.game.common.controller.elements.missile.dumb.DumbMissileCollectionManager;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.ManagedSegmentController;

public class SegmentControllerIntegrityMissileCondition extends SegmentControllerAbstractIntegrityCondition {
   public double getSmallestIntegrity(ManagedSegmentController var1) {
      double var2 = Double.POSITIVE_INFINITY;
      if (var1.getManagerContainer() instanceof WeaponManagerInterface) {
         Iterator var5 = ((WeaponManagerInterface)var1.getManagerContainer()).getMissile().getCollectionManagers().iterator();

         while(var5.hasNext()) {
            DumbMissileCollectionManager var4;
            if ((var4 = (DumbMissileCollectionManager)var5.next()).getLowestIntegrity() < var2) {
               var2 = var4.getLowestIntegrity();
            }
         }
      }

      return var2;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_INTEGRITY_MISSILE_CONDITION;
   }

   public String getQuantifierString() {
      return "Missile Integrity";
   }
}
