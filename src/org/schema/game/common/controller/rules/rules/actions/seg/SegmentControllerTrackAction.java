package org.schema.game.common.controller.rules.rules.actions.seg;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerTrackAction extends SegmentControllerAction {
   public ActionTypes getType() {
      return ActionTypes.SEG_TRACK;
   }

   public String getDescriptionShort() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLERTRACKACTION_0;
   }

   public void onTrigger(SegmentController var1) {
      if (var1.isOnServer()) {
         var1.setTracked(true);
      }

   }

   public void onUntrigger(SegmentController var1) {
      if (var1.isOnServer()) {
         var1.setTracked(false);
      }

   }
}
