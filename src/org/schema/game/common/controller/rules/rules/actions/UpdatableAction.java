package org.schema.game.common.controller.rules.rules.actions;

public interface UpdatableAction {
   ActionUpdate getActionUpdate();
}
