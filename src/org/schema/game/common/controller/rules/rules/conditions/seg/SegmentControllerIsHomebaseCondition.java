package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerIsHomebaseCondition extends SegmentControllerCondition {
   @RuleValue(
      tag = "IsHomebase"
   )
   public boolean isHomebase;

   public long getTrigger() {
      return 4194320L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_IS_HOMEBASE;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         return var3.isHomeBase() == this.isHomebase;
      }
   }

   public String getDescriptionShort() {
      return this.isHomebase ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERISHOMEBASECONDITION_0 : Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERISHOMEBASECONDITION_1;
   }
}
