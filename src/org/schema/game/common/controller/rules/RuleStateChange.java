package org.schema.game.common.controller.rules;

import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import org.schema.game.common.controller.rules.rules.Rule;
import org.schema.schine.network.SerialializationInterface;

public class RuleStateChange implements SerialializationInterface {
   public int ruleIdNT = Integer.MIN_VALUE;
   public final ShortArrayList changeLogCond = new ShortArrayList();
   public RuleStateChange.RuleTriggerState triggerState;
   public boolean lastSatisfied;

   public RuleStateChange() {
      this.triggerState = RuleStateChange.RuleTriggerState.UNCHANGED;
   }

   public RuleStateChange(Rule var1, RuleStateChange var2) {
      this.triggerState = RuleStateChange.RuleTriggerState.UNCHANGED;
      this.ruleIdNT = var1.getRuleId();
      this.changeLogCond.addAll(var2.changeLogCond);
      this.triggerState = var2.triggerState;
   }

   public void clear() {
      this.changeLogCond.clear();
      this.triggerState = RuleStateChange.RuleTriggerState.UNCHANGED;
   }

   public boolean changed() {
      return !this.changeLogCond.isEmpty();
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      assert this.ruleIdNT != Integer.MIN_VALUE;

      var1.writeInt(this.ruleIdNT);
      var1.writeShort(this.changeLogCond.size());
      Iterator var4 = this.changeLogCond.iterator();

      while(var4.hasNext()) {
         short var3 = (Short)var4.next();
         var1.writeShort(var3);
      }

      var1.writeByte((byte)this.triggerState.ordinal());
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.ruleIdNT = var1.readInt();
      short var4 = var1.readShort();
      this.changeLogCond.ensureCapacity(var4);

      for(int var5 = 0; var5 < var4; ++var5) {
         this.changeLogCond.add(var1.readShort());
      }

      this.triggerState = RuleStateChange.RuleTriggerState.values()[var1.readByte()];
   }

   public static enum RuleTriggerState {
      UNCHANGED,
      TRIGGERED,
      UNTRIGGERED;
   }
}
