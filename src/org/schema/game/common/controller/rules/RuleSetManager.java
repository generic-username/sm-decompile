package org.schema.game.common.controller.rules;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedInputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.schema.common.XMLTools;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.rules.Rule;
import org.schema.game.common.controller.rules.rules.RuleParserException;
import org.schema.game.common.data.world.RuleEntityContainer;
import org.schema.game.common.updater.FileUtil;
import org.schema.game.common.util.DataUtil;
import org.schema.game.network.objects.NetworkGameState;
import org.schema.game.network.objects.remote.RemoteRuleSetManager;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.TopLevelType;
import org.schema.schine.network.XMLSerializationInterface;
import org.schema.schine.network.objects.Sendable;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class RuleSetManager implements SerialializationInterface, XMLSerializationInterface {
   public static final String rulesPath;
   public static final String defaultRulesPath;
   private static final byte VERSION = 0;
   private Object2ObjectOpenHashMap ruleSets;
   public final Object2ObjectOpenHashMap ruleUIDlkMap;
   public final Int2ObjectOpenHashMap ruleIdMap;
   public GameStateInterface state;
   private final RuleSetManager.RuleCol col;
   public boolean includePropertiesInSendAndSaveOnServer;
   public boolean receivedInitialOnClient;
   public RulePropertyContainer receivedFullRuleChange;

   public RuleSetManager() {
      this.ruleSets = new Object2ObjectOpenHashMap();
      this.ruleUIDlkMap = new Object2ObjectOpenHashMap();
      this.ruleIdMap = new Int2ObjectOpenHashMap();
      this.col = new RuleSetManager.RuleCol();
      this.state = null;
   }

   public RuleSetManager(GameStateInterface var1) {
      this.ruleSets = new Object2ObjectOpenHashMap();
      this.ruleUIDlkMap = new Object2ObjectOpenHashMap();
      this.ruleIdMap = new Int2ObjectOpenHashMap();
      this.col = new RuleSetManager.RuleCol();
      this.state = var1;
   }

   public RuleSetManager(RuleSetManager var1) throws IOException {
      this();
      FastByteArrayOutputStream var2 = new FastByteArrayOutputStream(1048576);
      DataOutputStream var3 = new DataOutputStream(var2);
      var1.serialize(var3, true);
      DataInputStream var4 = new DataInputStream(new FastByteArrayInputStream(var2.array, 0, (int)var2.position()));
      this.deserialize(var4, 0, true);
   }

   public void loadFromDisk() throws ParserConfigurationException, SAXException, IOException {
      this.loadFromDisk(rulesPath);
   }

   public void loadFromDisk(String var1) throws ParserConfigurationException, SAXException, IOException {
      this.loadRulesFromDisk(var1);
   }

   public void writeToDisk() throws ParserConfigurationException, TransformerException, IOException {
      this.writeToDisk(rulesPath);
   }

   public void writeToDisk(File var1) throws ParserConfigurationException, TransformerException, IOException {
      writeRules((File)var1, this.ruleSets.values());
   }

   public void writeToDisk(String var1) throws ParserConfigurationException, TransformerException, IOException {
      this.writeRules(var1);
   }

   public static void writeRules(String var0, Collection var1) throws ParserConfigurationException, TransformerException, FileNotFoundException {
      writeRules(new File(var0), var1);
   }

   public static void writeRules(File var0, Collection var1) throws ParserConfigurationException, TransformerException, FileNotFoundException {
      Document var2 = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

      assert var1 != null;

      assert var2 != null;

      var2.appendChild(writeXML(var2, var1));
      XMLTools.writeDocument(var0, var2);
   }

   private void writeRules(String var1) throws ParserConfigurationException, TransformerException, FileNotFoundException {
      writeRules((String)var1, this.ruleSets.values());
   }

   public void loadRulesFromDisk(String var1) throws ParserConfigurationException, SAXException, IOException {
      File var2;
      File var3;
      if (!(var3 = new File(var1)).exists() && (var2 = new File(defaultRulesPath)).exists()) {
         FileUtil.copyFile(var2, var3);
      }

      this.loadFromDisk(var3);
   }

   public void loadFromDisk(File var1) throws ParserConfigurationException, SAXException, IOException {
      if (var1.exists()) {
         this.loadRulesFromDisk(var1);
      } else {
         System.err.println("[RULES] no rules read from disk: " + var1.getCanonicalPath());
      }

      this.createCollections();
   }

   private void loadRulesFromDisk(File var1) throws ParserConfigurationException, SAXException, IOException {
      DocumentBuilder var2 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      BufferedInputStream var3 = new BufferedInputStream(new FileInputStream(var1), 4096);
      Document var4 = var2.parse(var3);
      var3.close();
      this.parseXML(var4.getDocumentElement());
   }

   public void createCollections() {
      this.ruleUIDlkMap.clear();
      this.ruleIdMap.clear();
      Iterator var1 = this.ruleSets.values().iterator();

      while(var1.hasNext()) {
         RuleSet var2 = (RuleSet)var1.next();
         this.addRules(var2);
      }

      this.col.generate();
   }

   public RuleSet removeRuleSet(String var1) {
      RuleSet var2;
      if ((var2 = (RuleSet)this.ruleSets.remove(var1)) != null) {
         this.removeRules(var2);
      }

      this.createCollections();
      return var2;
   }

   private void removeRules(RuleSet var1) {
      Rule var2;
      for(Iterator var3 = var1.iterator(); var3.hasNext(); var2.ruleSetUID = "undefined") {
         var2 = (Rule)var3.next();
         this.ruleUIDlkMap.remove(var2.getUniqueIdentifier().toLowerCase(Locale.ENGLISH));
         this.ruleIdMap.remove(var2.getRuleId());
      }

   }

   public void addRuleSet(RuleSet var1) {
      Iterator var2 = var1.iterator();

      while(var2.hasNext()) {
         Rule var3;
         String var4 = (var3 = (Rule)var2.next()).getUniqueIdentifier();
         int var5 = 0;

         String var6;
         for(var6 = var4; this.ruleUIDlkMap.containsKey(var6.toLowerCase(Locale.ENGLISH)); ++var5) {
            var6 = var4 + var5;
         }

         var3.setUniqueIdentifier(var6);
      }

      this.ruleSets.put(var1.uniqueIdentifier, var1);
      this.addRules(var1);
      this.createCollections();
   }

   private void addRules(RuleSet var1) {
      Iterator var2 = var1.iterator();

      while(var2.hasNext()) {
         Rule var3 = (Rule)var2.next();
         this.ruleUIDlkMap.put(var3.getUniqueIdentifier().toLowerCase(Locale.ENGLISH), var3);
         this.ruleIdMap.put(var3.getRuleId(), var3);
         var3.setRuleSet(var1);
      }

   }

   public void parseXML(Node var1) {
      if (var1.getAttributes().getNamedItem("version") == null) {
         throw new RuleParserException("missing version attribute on ruleset");
      } else {
         Byte.parseByte(var1.getAttributes().getNamedItem("version").getNodeValue());
         this.ruleSets.clear();
         NodeList var5 = var1.getChildNodes();

         for(int var2 = 0; var2 < var5.getLength(); ++var2) {
            Node var3;
            if ((var3 = var5.item(var2)).getNodeType() == 1) {
               if (!var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("ruleset")) {
                  throw new RuleParserException("Node name must be 'Rule'");
               }

               RuleSet var4;
               (var4 = new RuleSet()).parseXML(var3);
               this.ruleSets.put(var4.getUniqueIdentifier(), var4);
            }
         }

      }
   }

   public static Node writeXML(Document var0, Collection var1) {
      Element var2 = var0.createElement("RuleSets");
      Attr var3;
      (var3 = var0.createAttribute("version")).setValue("0");
      var2.getAttributes().setNamedItem(var3);

      assert var1.size() > 0;

      Iterator var4 = var1.iterator();

      while(var4.hasNext()) {
         Node var5 = ((RuleSet)var4.next()).writeXML(var0, var2);
         var2.appendChild(var5);
      }

      return var2;
   }

   public Node writeXML(Document var1, Node var2) {
      return writeXML(var1, (Collection)this.ruleSets.values());
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      if (this.getProperties() == null) {
         System.err.println("[RULES][WARNING] tried to submit properties but properties were null");
         this.includePropertiesInSendAndSaveOnServer = false;
      }

      var1.writeBoolean(this.includePropertiesInSendAndSaveOnServer);
      var1.writeInt(this.ruleSets.size());
      Iterator var3 = this.ruleSets.values().iterator();

      while(var3.hasNext()) {
         ((RuleSet)var3.next()).serialize(var1, var2);
      }

      if (this.includePropertiesInSendAndSaveOnServer) {
         System.err.println("[RULESET] serializing all Rules and properties.");
         this.getProperties().serialize(var1, var2);
         this.includePropertiesInSendAndSaveOnServer = false;
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.includePropertiesInSendAndSaveOnServer = var1.readBoolean();
      int var4 = var1.readInt();

      for(int var5 = 0; var5 < var4; ++var5) {
         RuleSet var6;
         (var6 = new RuleSet()).deserialize(var1, var2, var3);
         this.ruleSets.put(var6.getUniqueIdentifier(), var6);
      }

      this.createCollections();
      if (this.includePropertiesInSendAndSaveOnServer) {
         RulePropertyContainer var7;
         (var7 = new RulePropertyContainer(this)).deserialize(var1, var2, var3);
         this.receivedFullRuleChange = var7;
         this.includePropertiesInSendAndSaveOnServer = false;
      }

   }

   public void initializeOnServer() {
      try {
         this.loadFromDisk();
      } catch (ParserConfigurationException var1) {
         var1.printStackTrace();
      } catch (SAXException var2) {
         var2.printStackTrace();
      } catch (IOException var3) {
         var3.printStackTrace();
      }
   }

   public Collection getRuleSets() {
      return this.ruleSets.values();
   }

   public RuleSet getRuleSetByUID(String var1) {
      RuleSet var2;
      if ((var2 = (RuleSet)this.ruleSets.get(var1)) == null) {
         System.err.println("RuleSet by UID not found: '" + var1 + "'; available: " + this.ruleSets);
      }

      return var2;
   }

   public void trigger(SegmentController var1, long var2) {
      var1.getRuleEntityManager().trigger(var2);
   }

   public List getGlobalRules(byte var1, List var2) {
      return this.getProperties().getGlobalRules(var1, var2);
   }

   public RulePropertyContainer getProperties() {
      if (this.state == null) {
         assert this.state != null : "No Properties";

         return null;
      } else {
         assert this.state.getGameState() != null;

         return this.state.getGameState().getRuleProperties();
      }
   }

   public void updateToNetworkObject(NetworkGameState var1) {
   }

   public void updateToFullNetworkObject(NetworkGameState var1) {
      this.includePropertiesInSendAndSaveOnServer = true;
      var1.ruleSetManagerBuffer.add(new RemoteRuleSetManager(this, var1));
   }

   public void initFromNetworkObject(NetworkGameState var1) {
   }

   public void flagChanged(StateInterface var1) {
      synchronized(var1) {
         Iterator var5 = var1.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

         while(var5.hasNext()) {
            Sendable var3;
            if ((var3 = (Sendable)var5.next()) instanceof RuleEntityContainer) {
               ((RuleEntityContainer)var3).getRuleEntityManager().flagRuleChanged();
            }
         }

      }
   }

   public void setState(GameStateInterface var1) {
      this.state = var1;
   }

   public boolean containtsName(RuleSet var1) {
      return this.ruleSets.containsKey(var1.uniqueIdentifier);
   }

   static {
      rulesPath = "." + File.separator + "rules.xml";
      defaultRulesPath = DataUtil.dataPath + "config" + File.separator + "defaultRules.xml";
   }

   public class RuleCol {
      private Object2ObjectOpenHashMap ruleByType = new Object2ObjectOpenHashMap();

      public void generate() {
         this.ruleByType.clear();

         Rule var2;
         Object var4;
         for(Iterator var1 = RuleSetManager.this.ruleIdMap.values().iterator(); var1.hasNext(); ((List)var4).add(var2)) {
            TopLevelType var3 = (var2 = (Rule)var1.next()).getEntityType();
            if ((var4 = (List)this.ruleByType.get(var3)) == null) {
               var4 = new ObjectArrayList();
               this.ruleByType.put(var3, var4);
            }
         }

      }
   }
}
