package org.schema.game.common.controller.rules;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.List;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class RuleContainer implements TagSerializable {
   private final SimpleTransformableSendableObject obj;
   private static byte VERSION = 0;
   private final List globalRules = new ObjectArrayList();
   private final List individualRules = new ObjectArrayList();
   private final RuleSetManager ruleManager;

   public RuleContainer(SimpleTransformableSendableObject var1) {
      this.obj = var1;
      this.ruleManager = ((GameStateInterface)var1.getState()).getGameState().getRuleManager();
   }

   public SimpleTransformableSendableObject getObj() {
      return this.obj;
   }

   public void onRuleChange() {
   }

   public RuleSetManager getRuleManager() {
      return this.ruleManager;
   }

   public void fromTagStructure(Tag var1) {
   }

   public Tag toTagStructure() {
      return null;
   }
}
