package org.schema.game.common.controller.rules;

import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.schema.common.XMLTools;
import org.schema.game.common.controller.rules.rules.Rule;
import org.schema.game.common.controller.rules.rules.RuleParserException;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.TopLevelType;
import org.schema.schine.network.XMLSerializationInterface;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class RuleSet extends ObjectArrayList implements SerialializationInterface, XMLSerializationInterface {
   private static final long serialVersionUID = 1L;
   private static final byte VERSION = 0;
   public String uniqueIdentifier;

   public RuleSet() {
   }

   public RuleSet(RuleSet var1, String var2) throws IOException {
      FastByteArrayOutputStream var3 = new FastByteArrayOutputStream(1048576);
      DataOutputStream var4 = new DataOutputStream(var3);
      var1.serialize(var4, true);
      DataInputStream var5 = new DataInputStream(new FastByteArrayInputStream(var3.array, 0, (int)var3.position()));
      this.deserialize(var5, 0, true);
      this.uniqueIdentifier = var2;

      assert var2 != null;

   }

   public void assignNewIds() {
      Iterator var1 = this.iterator();

      while(var1.hasNext()) {
         ((Rule)var1.next()).assignNewId();
      }

   }

   public void checkUIDRules(RuleSetManager var1) {
      Iterator var2 = this.iterator();

      while(var2.hasNext()) {
         ((Rule)var2.next()).checkUID(var1);
      }

   }

   public int hashCode() {
      return super.hashCode() * 31 + (this.uniqueIdentifier == null ? 0 : this.uniqueIdentifier.hashCode());
   }

   public boolean equals(Object var1) {
      if (this == var1) {
         return true;
      } else if (!super.equals(var1)) {
         return false;
      } else if (!(var1 instanceof RuleSet)) {
         return false;
      } else {
         RuleSet var2 = (RuleSet)var1;
         if (this.uniqueIdentifier == null) {
            if (var2.uniqueIdentifier != null) {
               return false;
            }
         } else if (!this.uniqueIdentifier.equals(var2.uniqueIdentifier)) {
            return false;
         }

         return true;
      }
   }

   public void parseXML(Node var1) {
      if (var1.getAttributes().getNamedItem("version") == null) {
         throw new RuleParserException("missing version attribute on ruleset");
      } else if (var1.getAttributes().getNamedItem("id") == null) {
         throw new RuleParserException("missing id attribute on ruleset");
      } else {
         Byte.parseByte(var1.getAttributes().getNamedItem("version").getNodeValue());
         this.uniqueIdentifier = var1.getAttributes().getNamedItem("id").getNodeValue();

         assert this.uniqueIdentifier != null;

         this.clear();
         NodeList var5 = var1.getChildNodes();

         for(int var2 = 0; var2 < var5.getLength(); ++var2) {
            Node var3;
            if ((var3 = var5.item(var2)).getNodeType() == 1) {
               if (!var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("rule")) {
                  throw new RuleParserException("Node name must be 'Rule'");
               }

               Rule var4;
               (var4 = new Rule(true)).parseXML(var3);
               this.add(var4);
            }
         }

      }
   }

   public Node writeXML(Document var1, Node var2) {
      Element var5 = var1.createElement("RuleSet");
      Attr var3;
      (var3 = var1.createAttribute("version")).setValue("0");
      var5.getAttributes().setNamedItem(var3);
      (var3 = var1.createAttribute("id")).setValue(this.uniqueIdentifier);
      var5.getAttributes().setNamedItem(var3);
      Iterator var6 = this.iterator();

      while(var6.hasNext()) {
         Node var4 = ((Rule)var6.next()).writeXML(var1, var5);
         var5.appendChild(var4);
      }

      return var5;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeUTF(this.uniqueIdentifier);
      var1.writeInt(this.size());

      for(int var3 = 0; var3 < this.size(); ++var3) {
         ((Rule)this.get(var3)).serialize(var1, var2);
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.uniqueIdentifier = var1.readUTF();
      int var4 = var1.readInt();

      for(int var5 = 0; var5 < var4; ++var5) {
         Rule var6;
         (var6 = new Rule(false)).deserialize(var1, var2, var3);
         var6.setRuleSet(this);
         this.add(var6);
      }

   }

   public String getUniqueIdentifier() {
      return this.uniqueIdentifier;
   }

   public TopLevelType getEntityType() {
      Iterator var1;
      return (var1 = this.iterator()).hasNext() ? ((Rule)var1.next()).getEntityType() : TopLevelType.GENERAL;
   }

   public void export(File var1) throws IOException, ParserConfigurationException, TransformerException {
      Document var2 = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      ObjectArrayList var3;
      (var3 = new ObjectArrayList(1)).add(this);
      var2.appendChild(RuleSetManager.writeXML(var2, (Collection)var3));
      XMLTools.writeDocument(var1, var2);
   }
}
