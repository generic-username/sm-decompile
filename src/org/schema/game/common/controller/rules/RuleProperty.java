package org.schema.game.common.controller.rules;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.List;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.TopLevelType;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class RuleProperty implements SerialializationInterface, TagSerializable {
   public static final byte ALL_SUBTYPES = -1;
   public byte VERSION = 0;
   public boolean global;
   public byte subType = -1;
   public RuleSet ruleSet;
   public String receivedRuleSetUID;

   public RuleProperty() {
   }

   public RuleProperty(RuleSet var1) {
      this.ruleSet = var1;
   }

   public List getAvailableSubtypes(RuleSet var1, List var2) {
      TopLevelType var7 = var1.getEntityType();
      var2.clear();
      SimpleTransformableSendableObject.EntityType[] var3;
      int var4 = (var3 = SimpleTransformableSendableObject.EntityType.values()).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         SimpleTransformableSendableObject.EntityType var6;
         if ((var6 = var3[var5]).used && var6.topLevelType == var7) {
            var2.add(var6);
         }
      }

      return var2;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeUTF(this.ruleSet.getUniqueIdentifier());
      var1.writeBoolean(this.global);
      var1.writeByte(this.subType);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.receivedRuleSetUID = var1.readUTF();
      this.global = var1.readBoolean();
      this.subType = var1.readByte();
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2;
      (var2 = var1.getStruct())[0].getByte();
      this.receivedRuleSetUID = var2[1].getString();
      this.global = var2[2].getByte() == 1;
      this.subType = var2[3].getByte();
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, this.VERSION), new Tag(Tag.Type.STRING, (String)null, this.ruleSet.uniqueIdentifier), new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.global ? 1 : 0))), new Tag(Tag.Type.BYTE, (String)null, this.subType), FinishTag.INST});
   }
}
