package org.schema.game.common.controller.rules;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.network.objects.NetworkGameState;
import org.schema.game.network.objects.remote.RemoteRuleRuleProperty;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class RulePropertyContainer implements SerialializationInterface, TagSerializable {
   public byte VERSION = 0;
   private final Object2ObjectOpenHashMap ruleUIDlkMap = new Object2ObjectOpenHashMap();
   public final RuleSetManager manager;
   public GameStateInterface state;
   private boolean changed;
   private final ObjectArrayFIFOQueue receivedRuleProperties = new ObjectArrayFIFOQueue();

   public static String getPropertiesPath() {
      return GameServerState.ENTITY_DATABASE_PATH + File.separator + "ruleproperties.tag";
   }

   public RulePropertyContainer(RuleSetManager var1) {
      this.manager = var1;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeInt(this.ruleUIDlkMap.size());
      Iterator var3 = this.ruleUIDlkMap.values().iterator();

      while(var3.hasNext()) {
         ((RuleProperty)var3.next()).serialize(var1, var2);
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      int var4 = var1.readInt();

      for(int var5 = 0; var5 < var4; ++var5) {
         RuleProperty var6;
         (var6 = new RuleProperty()).deserialize(var1, var2, var3);
         RuleSet var7;
         if ((var7 = this.manager.getRuleSetByUID(var6.receivedRuleSetUID)) != null) {
            var6.ruleSet = var7;
            this.ruleUIDlkMap.put(var6.ruleSet.uniqueIdentifier.toLowerCase(Locale.ENGLISH), var6);
         } else {
            try {
               throw new Exception("Received property RuleSet not found: " + var6.receivedRuleSetUID);
            } catch (Exception var8) {
               var8.printStackTrace();
            }
         }
      }

   }

   public void fromTagStructure(Tag var1) {
      Tag[] var6;
      (var6 = var1.getStruct())[0].getByte();
      var6 = var6[1].getStruct();

      for(int var2 = 0; var2 < var6.length - 1; ++var2) {
         RuleProperty var3;
         (var3 = new RuleProperty()).fromTagStructure(var6[var2]);
         RuleSet var4;
         if ((var4 = this.manager.getRuleSetByUID(var3.receivedRuleSetUID)) != null) {
            var3.ruleSet = var4;
            this.ruleUIDlkMap.put(var3.ruleSet.uniqueIdentifier.toLowerCase(Locale.ENGLISH), var3);
         } else {
            try {
               throw new Exception("Received property RuleSet not found: " + var3.receivedRuleSetUID);
            } catch (Exception var5) {
               var5.printStackTrace();
            }
         }
      }

   }

   public Tag toTagStructure() {
      Tag[] var1 = new Tag[this.ruleUIDlkMap.size() + 1];
      int var2 = 0;

      for(Iterator var3 = this.ruleUIDlkMap.values().iterator(); var3.hasNext(); ++var2) {
         RuleProperty var4 = (RuleProperty)var3.next();
         var1[var2] = var4.toTagStructure();
      }

      var1[var2] = FinishTag.INST;
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, this.VERSION), new Tag(Tag.Type.STRUCT, (String)null, var1), FinishTag.INST});
   }

   public void loadFromDisk(String var1) throws IOException {
      File var2;
      if ((var2 = new File(var1)).exists()) {
         Tag var3 = Tag.readFrom(new BufferedInputStream(new FileInputStream(var2)), true, false);
         this.fromTagStructure(var3);
      } else {
         System.err.println("[RULES][PROPERTIES] no properties read. File doesn't exist: " + var2.getCanonicalPath() + "; Will be created on save");
      }
   }

   public void saveToDisk(String var1) throws IOException {
      File var2;
      if ((var2 = new File(var1)).exists()) {
         var2.delete();
      }

      BufferedOutputStream var3 = new BufferedOutputStream(new FileOutputStream(var2));
      this.toTagStructure().writeTo(var3, true);
   }

   public RuleProperty getProperty(RuleSet var1) {
      assert var1.uniqueIdentifier != null;

      assert this.ruleUIDlkMap != null;

      RuleProperty var2;
      if ((var2 = (RuleProperty)this.ruleUIDlkMap.get(var1.uniqueIdentifier.toLowerCase(Locale.ENGLISH))) == null) {
         var2 = new RuleProperty(var1);
         this.ruleUIDlkMap.put(var1.uniqueIdentifier.toLowerCase(Locale.ENGLISH), var2);
      }

      return var2;
   }

   public byte getSubType(RuleSet var1) {
      return this.getProperty(var1).subType;
   }

   public boolean isGlobal(RuleSet var1) {
      return this.getProperty(var1).global;
   }

   public void setGlobal(RuleSet var1, boolean var2, boolean var3) {
      RuleProperty var4;
      (var4 = this.getProperty(var1)).global = var2;
      System.err.println("SET GLOBAL FOR " + var1.uniqueIdentifier + " -> " + var2);
      if (var3) {
         assert this.state != null;

         assert this.state.getGameState().getNetworkObject().rulePropertyBuffer != null;

         this.state.getGameState().getNetworkObject().rulePropertyBuffer.add(new RemoteRuleRuleProperty(var4, this.state.getGameState().isOnServer()));
      }

   }

   public List getGlobalRules(byte var1, List var2) {
      System.err.println("GET GLOBAL RULES " + this.ruleUIDlkMap + "; " + var1);
      Iterator var3 = this.ruleUIDlkMap.values().iterator();

      while(true) {
         RuleProperty var4;
         do {
            do {
               if (!var3.hasNext()) {
                  return var2;
               }
            } while(!(var4 = (RuleProperty)var3.next()).global);
         } while(var4.subType != -1 && var4.subType != var1);

         assert var4.ruleSet != null : var4.receivedRuleSetUID;

         var2.add(var4.ruleSet);
      }
   }

   public void setSubAllTypes(RuleSet var1, boolean var2) {
      this.setSubTypes(var1, (byte)-1, var2);
   }

   public void setSubTypes(RuleSet var1, byte var2, boolean var3) {
      RuleProperty var4;
      (var4 = this.getProperty(var1)).subType = var2;
      if (var3) {
         this.state.getGameState().getNetworkObject().rulePropertyBuffer.add(new RemoteRuleRuleProperty(var4, this.state.getGameState().isOnServer()));
      }

   }

   public void initializeOnServer() {
      try {
         this.loadFromDisk(getPropertiesPath());
      } catch (IOException var1) {
         var1.printStackTrace();
      }
   }

   public void update() {
      while(!this.receivedRuleProperties.isEmpty()) {
         RuleProperty var1;
         (var1 = (RuleProperty)this.receivedRuleProperties.dequeue()).ruleSet = this.manager.getRuleSetByUID(var1.receivedRuleSetUID);
         if (var1.ruleSet != null) {
            this.ruleUIDlkMap.put(var1.ruleSet.uniqueIdentifier.toLowerCase(Locale.ENGLISH), var1);
            if (this.state.getGameState().isOnServer()) {
               this.state.getGameState().getNetworkObject().rulePropertyBuffer.add(new RemoteRuleRuleProperty(var1, this.state.getGameState().isOnServer()));
            }

            this.changed = true;
         }
      }

      if (this.changed) {
         this.manager.flagChanged((StateInterface)this.state);
         if (this.state.getGameState().isOnServer()) {
            try {
               this.saveToDisk(getPropertiesPath());
            } catch (IOException var2) {
               var2.printStackTrace();
            }
         }

         this.changed = false;
      }

   }

   public void updateFromNetworkObject(NetworkGameState var1) {
      ObjectArrayList var3 = var1.rulePropertyBuffer.getReceiveBuffer();

      for(int var2 = 0; var2 < var3.size(); ++var2) {
         this.receivedRuleProperties.enqueue(((RemoteRuleRuleProperty)var3.get(var2)).get());
      }

   }

   public void initFromNetworkObject(NetworkGameState var1) {
      this.updateFromNetworkObject(var1);
   }

   public void sendAll(NetworkGameState var1) {
      Iterator var2 = this.ruleUIDlkMap.values().iterator();

      while(var2.hasNext()) {
         RuleProperty var3 = (RuleProperty)var2.next();
         var1.rulePropertyBuffer.add(new RemoteRuleRuleProperty(var3, this.state.getGameState().isOnServer()));
      }

   }
}
