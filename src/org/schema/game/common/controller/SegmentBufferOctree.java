package org.schema.game.common.controller;

import com.googlecode.javaewah.EWAHCompressedBitmap;
import java.util.Arrays;
import javax.vecmath.Vector3f;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.SegmentRetrieveCallback;
import org.schema.game.common.data.world.DrawableRemoteSegment;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.Segment;
import org.schema.schine.graphicsengine.forms.DebugBox;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.network.ServerInfo;

public class SegmentBufferOctree {
   public static final short NOTHING = -2;
   public static final short EMPTY = -1;
   private static final int MAX_FILL = 4096;
   private static final int[] LEVELS;
   private static final short[][][][] iteration;
   private static final int[] LEVELS_SUB;
   final SegmentBuffer buffer;
   private byte[][] tree;
   private short[] indices;
   private long[] lastChanged;
   private Segment[] backing;
   private short pointer;
   private long lastDraw;

   public SegmentBufferOctree(SegmentBuffer var1) {
      this.tree = new byte[LEVELS_SUB.length][];
      this.indices = new short[4096];
      this.lastChanged = new long[4096];
      this.backing = new Segment[32];
      this.pointer = 0;
      Arrays.fill(this.indices, (short)-2);
      this.buffer = var1;

      for(int var2 = 0; var2 < LEVELS_SUB.length; ++var2) {
         this.tree[var2] = new byte[LEVELS_SUB[var2]];
      }

   }

   public static boolean testAabbAgainstAabb2(Vector3i var0, Vector3i var1, Vector3i var2, Vector3i var3) {
      boolean var4 = var0.x <= var3.x && var1.x >= var2.x;
      var4 = var0.z <= var3.z && var1.z >= var2.z ? var4 : false;
      return var0.y <= var3.y && var1.y >= var2.y ? var4 : false;
   }

   public static boolean testAabbAgainstAabb2(int var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11) {
      boolean var12 = var0 <= var9 && var3 >= var6;
      var12 = var2 <= var11 && var5 >= var8 ? var12 : false;
      return var1 <= var10 && var4 >= var7 ? var12 : false;
   }

   private static boolean intersects(int var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, SegmentBuffer var10) {
      int var12 = var1 + (LEVELS[var0] / 2 << 5);
      int var11 = var2 + (LEVELS[var0] / 2 << 5);
      var0 = var3 + (LEVELS[var0] / 2 << 5);
      return testAabbAgainstAabb2(var1, var2, var3, var12, var11, var0, var4, var5, var6, var7, var8, var9);
   }

   private static int getOctreeIndex(int var0, int var1, int var2, int var3, Vector3i var4) {
      int var5 = (ByteUtil.divUSeg(var0) - var4.x) / LEVELS[var3];
      int var6 = (ByteUtil.divUSeg(var1) - var4.y) / LEVELS[var3];
      int var7 = (ByteUtil.divUSeg(var2) - var4.z) / LEVELS[var3];
      int var8 = LEVELS[LEVELS.length - 1 - var3];

      assert var5 >= 0 && var6 >= 0 && var7 >= 0 : var5 + ", " + var6 + ", " + var7 + "; " + var8 + "; " + var0 + ". " + var1 + ", " + var2 + " - " + var4;

      assert var5 < var8 && var6 < var8 && var7 < var8 : var5 + ", " + var6 + ", " + var7 + "; lvl: " + var3 + "; aLvl " + var8 + "; " + var0 + ". " + var1 + ", " + var2 + " - " + var4;

      return var7 * var8 * var8 + var6 * var8 + var5;
   }

   private static int getOctreeIndex(Vector3i var0, int var1, Vector3i var2) {
      return getOctreeIndex(var0.x, var0.y, var0.z, var1, var2);
   }

   private static final int getLocalIndex(int var0, int var1, int var2, int var3, Vector3i var4) {
      var0 = (ByteUtil.divUSeg(var0) - var4.x) / (LEVELS[var3] / 2);
      var1 = (ByteUtil.divUSeg(var1) - var4.y) / (LEVELS[var3] / 2);
      return ((ByteUtil.divUSeg(var2) - var4.z) / (LEVELS[var3] / 2) % 2 << 2) + (var1 % 2 << 1) + var0 % 2;
   }

   public void clear() {
      this.clearArrays();
   }

   private void placeInArray(Segment var1) {
      short var2 = (short)SegmentBuffer.getIndex(var1.pos.x, var1.pos.y, var1.pos.z, this.buffer.getRegionStart());
      if (!var1.isEmpty()) {
         assert this.indices[var2] < 0;

         if (this.backing.length == this.pointer) {
            int var3 = this.backing.length << 1;
            this.backing = (Segment[])Arrays.copyOf(this.backing, var3);
         }

         this.backing[this.pointer] = var1;
         this.indices[var2] = this.pointer++;
      } else {
         this.indices[var2] = -1;
      }
   }

   private void replaceInArray(Segment var1) {
      short var2 = (short)SegmentBuffer.getIndex(var1.pos.x, var1.pos.y, var1.pos.z, this.buffer.getRegionStart());
      this.backing[this.indices[var2]] = var1;
   }

   private boolean existsinArray(Segment var1) {
      short var2 = (short)SegmentBuffer.getIndex(var1.pos.x, var1.pos.y, var1.pos.z, this.buffer.getRegionStart());
      return this.indices[var2] != -2;
   }

   private boolean existsinArray(int var1, int var2, int var3) {
      short var4 = (short)SegmentBuffer.getIndex(var1, var2, var3, this.buffer.getRegionStart());
      return this.indices[var4] != -2;
   }

   private boolean isEmptyinArray(Segment var1) {
      short var2 = (short)SegmentBuffer.getIndex(var1.pos.x, var1.pos.y, var1.pos.z, this.buffer.getRegionStart());
      return this.indices[var2] == -1;
   }

   private void clearArrays() {
      Arrays.fill(this.indices, (short)-2);
      Arrays.fill(this.backing, (Object)null);

      for(int var1 = 0; var1 < LEVELS_SUB.length; ++var1) {
         Arrays.fill(this.tree[var1], (byte)0);
      }

      Arrays.fill(this.lastChanged, 0L);
      this.pointer = 0;
   }

   private void removeFromArray(Segment var1, short var2) {
      this.removeFromArray(var1.pos.x, var1.pos.y, var1.pos.z, var2);
   }

   private void removeFromArray(int var1, int var2, int var3, short var4) {
      short var5 = (short)SegmentBuffer.getIndex(var1, var2, var3, this.buffer.getRegionStart());
      this.removeFromArray(var5, var4);
   }

   private void removeFromArray(short var1, short var2) {
      short var3 = (short)(this.pointer - 1);
      short var4 = this.indices[var1];
      this.indices[var1] = var2;
      this.lastChanged[var1] = 0L;
      if (var4 >= 0) {
         this.backing[var4] = null;
         if (var3 > 0 && var4 != var3) {
            var1 = (short)SegmentBuffer.getIndex(this.backing[var3].pos.x, this.backing[var3].pos.y, this.backing[var3].pos.z, this.buffer.getRegionStart());
            this.backing[var4] = this.backing[var3];
            this.backing[var3] = null;
            this.indices[var1] = var4;
         }

         --this.pointer;
         if (this.backing.length > 32 && this.backing.length / 2 == this.pointer) {
            int var5 = this.backing.length / 2;
            this.backing = (Segment[])Arrays.copyOf(this.backing, var5);
         }
      }

   }

   public void setEmpty(int var1, int var2, int var3) {
      this.removeFromArray(var1, var2, var3, (short)-1);
      this.updateTree(LEVELS.length - 1, var1, var2, var3, true);
   }

   public int getSegmentState(int var1, int var2, int var3) {
      short var4 = (short)SegmentBuffer.getIndex(var1, var2, var3, this.buffer.getRegionStart());
      return this.indices[var4];
   }

   public boolean isEmptyTree(Vector3i var1) {
      return this.isEmptyTree(0, var1);
   }

   public boolean iterateOverNonEmptyElement(SegmentBufferIteratorInterface var1) {
      for(int var2 = 0; var2 < this.pointer; ++var2) {
         boolean var3;
         if (!(var3 = var1.handle(this.backing[var2], this.lastChanged[(short)SegmentBuffer.getIndex(this.backing[var2].pos.x, this.backing[var2].pos.y, this.backing[var2].pos.z, this.buffer.getRegionStart())]))) {
            return var3;
         }
      }

      return true;
   }

   public boolean iterateOverEveryElement(SegmentBufferIteratorEmptyInterface var1) {
      int var2 = 0;

      for(int var3 = 0; var3 < 16; ++var3) {
         for(int var4 = 0; var4 < 16; ++var4) {
            for(int var5 = 0; var5 < 16; ++var5) {
               short var6 = this.indices[var2];
               boolean var7 = true;
               if (var6 >= 0) {
                  var7 = var1.handle(this.backing[var6], this.lastChanged[var2]);
               } else if (var6 == -1) {
                  var7 = var1.handleEmpty((var5 << 5) + this.buffer.getRegionStartBlock().x, (var4 << 5) + this.buffer.getRegionStartBlock().y, (var3 << 5) + this.buffer.getRegionStartBlock().z, this.lastChanged[var2]);
               }

               ++var2;
               if (!var7) {
                  assert false;

                  return var7;
               }
            }
         }
      }

      return true;
   }

   public void draw() {
      this.draw(0);
   }

   public boolean iterateOverNonEmptyElementRange(SegmentBufferIteratorInterface var1, Vector3i var2, Vector3i var3) {
      return this.iterateOverNonEmptyElementRange(var1, var2.x, var2.y, var2.z, var3.x, var3.y, var3.z);
   }

   public boolean iterateOverNonEmptyElementRange(SegmentBufferIteratorInterface var1, int var2, int var3, int var4, int var5, int var6, int var7) {
      var2 -= this.buffer.getRegionStartBlock().x;
      var3 -= this.buffer.getRegionStartBlock().y;
      var4 -= this.buffer.getRegionStartBlock().z;
      var5 -= this.buffer.getRegionStartBlock().x;
      var6 -= this.buffer.getRegionStartBlock().y;
      var7 -= this.buffer.getRegionStartBlock().z;

      for(int var8 = 0; var8 < LEVELS_SUB[0]; ++var8) {
         int var9 = this.tree[0][var8] & 255;

         assert 0 < iteration.length : "0 -> " + iteration.length;

         assert var8 < iteration[0].length : var8 + " -> " + iteration[0].length;

         assert var9 < iteration[0][var8].length : var9 + " -> " + iteration[0][var8].length;

         short[] var10 = iteration[0][var8][var9];

         assert var9 != 255 || var10.length == 8 : var10.length;

         if (!this.isEmptyTree(0, var8)) {
            for(var9 = 0; var9 < var10.length; ++var9) {
               short var11 = var10[var9];
               int var12 = LEVELS[LEVELS.length - 1] << 1;
               int var13 = var11 / (var12 * var12);
               int var15;
               int var14 = (var15 = var11 - var13 * var12 * var12) / var12;
               var15 = (var15 - var14 * var12) * 32 * (LEVELS[0] / 2);
               var14 *= 32 * (LEVELS[0] / 2);
               var13 *= 32 * (LEVELS[0] / 2);
               boolean var16;
               if (intersects(0, var15, var14, var13, var2, var3, var4, var5, var6, var7, this.buffer) && !(var16 = this.iterateOverNonEmptyElementRangeRecursive(1, var2, var3, var4, var5, var6, var7, var10[var9], var1))) {
                  return var16;
               }
            }
         }
      }

      return true;
   }

   private boolean iterateOverNonEmptyElementRangeRecursive(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, SegmentBufferIteratorInterface var9) {
      short[] var15 = iteration[var1][var8][this.tree[var1][var8] & 255];

      for(int var10 = 0; var10 < var15.length; ++var10) {
         short var11 = var15[var10];
         int var12 = LEVELS[LEVELS.length - 1 - var1] << 1;
         int var13 = var11 / (var12 * var12);
         int var16;
         int var14 = (var16 = var11 - var13 * var12 * var12) / var12;
         var16 = (var16 - var14 * var12) * 32 * (LEVELS[var1] / 2);
         var14 *= 32 * (LEVELS[var1] / 2);
         var13 *= 32 * (LEVELS[var1] / 2);
         if (intersects(var1, var16, var14, var13, var2, var3, var4, var5, var6, var7, this.buffer)) {
            boolean var17;
            if (var1 == LEVELS.length - 1) {
               Segment var18 = this.getFromRelativeCords(var16, var14, var13);

               assert var18 != null : var16 + ", " + var14 + ", " + var13;

               var17 = var9.handle(var18, this.lastChanged[(short)SegmentBuffer.getIndexAbsolute(var16, var14, var13)]);
            } else {
               var17 = this.iterateOverNonEmptyElementRangeRecursive(var1 + 1, var2, var3, var4, var5, var6, var7, var15[var10], var9);
            }

            if (!var17) {
               return var17;
            }
         }
      }

      return true;
   }

   private boolean isEmptyTree(int var1, Vector3i var2) {
      while(true) {
         int var3 = getOctreeIndex(var2, var1, this.buffer.getRegionStart());
         boolean var4;
         if (var4 = this.isEmptyTree(var1, var3)) {
            return true;
         }

         if (var1 == LEVELS.length - 1) {
            return var4;
         }

         ++var1;
         this = this;
      }
   }

   public void remove(int var1, int var2, int var3) {
      this.removeFromArray(var1, var2, var3, (short)-2);
      this.updateTree(LEVELS.length - 1, var1, var2, var3, true);
   }

   public void remove(Segment var1) {
      this.removeFromArray(var1, (short)-2);
      this.updateTree(LEVELS.length - 1, var1.pos.x, var1.pos.y, var1.pos.z, true);
   }

   public void getSegment(int var1, int var2, int var3, SegmentRetrieveCallback var4) {
      this.getSegment(var1, var2, var3, var4, this.buffer.getRegionStart());
   }

   private void getSegment(int var1, int var2, int var3, SegmentRetrieveCallback var4, Vector3i var5) {
      short var6 = (short)SegmentBuffer.getIndex(var1, var2, var3, var5);
      var6 = this.indices[var6];
      var4.pos.set(var1, var2, var3);
      var4.abspos.set(var1 >> 4, var2 >> 4, var3 >> 4);
      if (var6 < 0) {
         var4.state = var6;
         var4.segment = null;
      } else {
         var4.state = 1;
         var4.segment = this.backing[var6];

         assert this.backing[var6].pos.equals(var1, var2, var3) : this.backing[var6].pos + "; " + var1 + ", " + var2 + ", " + var3;
      }

      assert var4.segment == null || var4.pos.equals(var4.segment.pos) : var4.pos + "; " + var4.segment.pos;

   }

   public Segment getSegment(int var1, int var2, int var3, Vector3i var4) {
      short var6 = (short)SegmentBuffer.getIndex(var1, var2, var3, var4);
      if ((var6 = this.indices[var6]) < 0) {
         if (var6 == -1) {
            Object var5;
            if (this.buffer.getSegmentController().isOnServer()) {
               var5 = new RemoteSegment(this.buffer.getSegmentController());
            } else {
               var5 = new DrawableRemoteSegment(this.buffer.getSegmentController());
            }

            ((Segment)var5).setPos(var1, var2, var3);
            return (Segment)var5;
         } else {
            return null;
         }
      } else {
         return this.backing[var6];
      }
   }

   private Segment getFromRelativeCords(int var1, int var2, int var3) {
      short var4 = (short)SegmentBuffer.getIndexAbsolute(var1, var2, var3);
      return this.indices[var4] < 0 ? null : this.backing[this.indices[var4]];
   }

   public void insert(Segment var1) {
      label33: {
         if (this.existsinArray(var1)) {
            if (this.isEmptyinArray(var1) == var1.isEmpty()) {
               if (!var1.isEmpty()) {
                  this.replaceInArray(var1);
               }
               break label33;
            }

            if (var1.isEmpty()) {
               this.removeFromArray(var1, (short)-2);
            }
         }

         this.placeInArray(var1);
      }

      boolean var2 = this.isEmptyTree(var1.pos);
      this.updateTree(LEVELS.length - 1, var1.pos.x, var1.pos.y, var1.pos.z, var1.isEmpty());

      assert !var2 || var1.isEmpty() || !this.isEmptyTree(var1.pos) : var1.pos + "; " + (var1.isEmpty() ? "EMPTY" : "NON-EMPTY") + "; " + var2 + "; " + var1.isEmpty() + " -> " + this.isEmptyTree(var1.pos);

   }

   private void draw(int var1) {
      assert false;

      if (System.currentTimeMillis() - this.lastDraw > 700L) {
         for(int var2 = 0; var2 < LEVELS_SUB[var1]; ++var2) {
            int var3 = this.tree[var1][var2] & 255;

            assert var1 < iteration.length : var1 + " -> " + iteration.length;

            assert var2 < iteration[var1].length : var2 + " -> " + iteration[var1].length;

            assert var3 < iteration[var1][var2].length : var3 + " -> " + iteration[var1][var2].length;

            short[] var4 = iteration[var1][var2][var3];

            assert var3 != 255 || var4.length == 8 : var4.length;

            if (!this.isEmptyTree(var1, var2)) {
               for(var3 = 0; var3 < var4.length; ++var3) {
                  short var5 = var4[var3];
                  int var6 = LEVELS[LEVELS.length - 1 - var1] << 1;
                  int var7 = var5 / (var6 * var6);
                  int var10;
                  int var8 = (var10 = var5 - var7 * var6 * var6) / var6;
                  var10 = (var10 - var8 * var6) * 32 * (LEVELS[var1] / 2);
                  var8 *= 32 * (LEVELS[var1] / 2);
                  var7 *= 32 * (LEVELS[var1] / 2);
                  Vector3f var11 = new Vector3f((float)(this.buffer.getRegionStartBlock().x - 16), (float)(this.buffer.getRegionStartBlock().y - 16), (float)(this.buffer.getRegionStartBlock().z - 16));
                  Vector3f var9 = new Vector3f(var11);
                  var11.x += (float)var10;
                  var11.y += (float)var8;
                  var11.z += (float)var7;
                  var10 += LEVELS[var1] / 2 << 5;
                  var8 += LEVELS[var1] / 2 << 5;
                  var7 += LEVELS[var1] / 2 << 5;
                  var9.x += (float)var10;
                  var9.y += (float)var8;
                  var9.z += (float)var7;
                  DebugBox var12 = new DebugBox(var11, var9, this.buffer.getSegmentController().getWorldTransform(), var1 == 0 ? 1.0F : 0.0F, var1 == 1 ? 1.0F : 0.0F, var1 == 2 ? 1.0F : 0.0F, 1.0F);
                  DebugDrawer.boxes.add(var12);
               }
            }
         }

         if (var1 < LEVELS.length - 1) {
            this.draw(var1 + 1);
         }

         this.lastDraw = System.currentTimeMillis();
      }

   }

   private void updateTree(int var1, int var2, int var3, int var4, boolean var5) {
      while(true) {
         int var6 = getOctreeIndex(var2, var3, var4, var1, this.buffer.getRegionStart());
         this.isEmptyTree(var1, var6);
         this.updateOcree(var1, var2, var3, var4, var6, var5);
         boolean var7 = this.isEmptyTree(var1, var6);

         assert var5 || !var7;

         if (var1 - 1 < 0) {
            return;
         }

         int var10001 = var1 - 1;
         var5 = var7;
         var4 = var4;
         var3 = var3;
         var2 = var2;
         var1 = var10001;
         this = this;
      }
   }

   private void updateOcree(int var1, int var2, int var3, int var4, int var5, boolean var6) {
      var2 = getLocalIndex(var2, var3, var4, var1, this.buffer.getRegionStart());
      byte var7 = (byte)(1 << var2);
      byte[] var10000;
      if (var6) {
         var10000 = this.tree[var1];
         var10000[var5] = (byte)(var10000[var5] & ~var7);
      } else {
         var10000 = this.tree[var1];
         var10000[var5] |= var7;
      }
   }

   private boolean isEmptyTree(int var1, int var2) {
      return this.tree[var1][var2] == 0;
   }

   public boolean contains(Vector3i var1) {
      return this.existsinArray(var1.x, var1.y, var1.z);
   }

   public boolean contains(int var1, int var2, int var3) {
      return this.existsinArray(var1, var2, var3);
   }

   public long getLastChanged(Vector3i var1) {
      return this.lastChanged[(short)SegmentBuffer.getIndex(var1.x, var1.y, var1.z, this.buffer.getRegionStart())];
   }

   public void setLastChanged(Vector3i var1, long var2) {
      if (var2 > ServerInfo.curtime + 100000L) {
         try {
            throw new Exception("Tried to set last Changed to future " + ServerInfo.curtime + "; " + this.lastChanged);
         } catch (Exception var4) {
            var4.printStackTrace();

            assert false;
         }
      }

      this.lastChanged[(short)SegmentBuffer.getIndex(var1.x, var1.y, var1.z, this.buffer.getRegionStart())] = var2;
   }

   public EWAHCompressedBitmap applyBitMap(EWAHCompressedBitmap var1) {
      int var2 = 0;
      EWAHCompressedBitmap var3 = new EWAHCompressedBitmap(64);

      for(int var4 = 0; var4 < 16; ++var4) {
         for(int var5 = 0; var5 < 16; ++var5) {
            for(int var6 = 0; var6 < 16; ++var6) {
               if (this.indices[var2] < 0) {
                  var3.set(var2);
               }

               ++var2;
            }
         }
      }

      assert var1 != null;

      return var1.and(var3);
   }

   public boolean iterateOverUnloaded(SegmentBufferIteratorEmptyInterface var1) {
      int var2 = 0;

      for(int var3 = 0; var3 < 16; ++var3) {
         for(int var4 = 0; var4 < 16; ++var4) {
            for(int var5 = 0; var5 < 16; ++var5) {
               short var6 = this.indices[var2];
               boolean var7 = true;
               if (var6 != -1) {
                  var7 = var1.handleEmpty((var5 << 5) + this.buffer.getRegionStartBlock().x, (var4 << 5) + this.buffer.getRegionStartBlock().y, (var3 << 5) + this.buffer.getRegionStartBlock().z, this.lastChanged[var2]);
               }

               if (!var7) {
                  return var7;
               }

               ++var2;
            }
         }
      }

      return true;
   }

   static {
      iteration = new short[(LEVELS = new int[]{8, 4, 2}).length][][][];
      LEVELS_SUB = new int[]{8, 64, 512};

      int var0;
      for(var0 = 0; var0 < LEVELS.length; ++var0) {
         iteration[var0] = new short[LEVELS_SUB[var0]][][];
      }

      var0 = 2;
      Vector3i var1 = new Vector3i();
      new Vector3i();

      for(int var2 = 0; var2 < LEVELS.length; var0 <<= 1) {
         int var3 = var0;

         for(int var4 = 0; var4 < var3; ++var4) {
            for(int var5 = 0; var5 < var3; ++var5) {
               for(int var6 = 0; var6 < var3; ++var6) {
                  var1.set(var6, var5, var4);
                  int var7 = var4 * var3 * var3 + var5 * var3 + var6;
                  iteration[var2][var7] = new short[256][];

                  for(int var8 = 0; var8 < 256; ++var8) {
                     int var9 = 0;

                     byte var11;
                     byte var12;
                     int var14;
                     for(byte var10 = 0; var10 < 2; ++var10) {
                        for(var11 = 0; var11 < 2; ++var11) {
                           for(var12 = 0; var12 < 2; ++var12) {
                              int var13 = (var10 << 2) + (var11 << 1) + var12;
                              var14 = 1 << var13;
                              if ((var8 & var14) == var14) {
                                 ++var9;
                              }
                           }
                        }
                     }

                     iteration[var2][var7][var8] = new short[var9];
                     int var17 = 0;

                     for(var11 = 0; var11 < 2; ++var11) {
                        for(var12 = 0; var12 < 2; ++var12) {
                           for(byte var19 = 0; var19 < 2; ++var19) {
                              var14 = (var11 << 2) + (var12 << 1) + var19;
                              var9 = 1 << var14;
                              if ((var8 & var9) == var9) {
                                 var9 = var6 * LEVELS[var2] + var19 * (LEVELS[var2] / 2) << 5;
                                 var14 = var5 * LEVELS[var2] + var12 * (LEVELS[var2] / 2) << 5;
                                 int var15 = var4 * LEVELS[var2] + var11 * (LEVELS[var2] / 2) << 5;
                                 var9 = ByteUtil.divUSeg(var9) / (LEVELS[var2] / 2);
                                 var14 = ByteUtil.divUSeg(var14) / (LEVELS[var2] / 2);
                                 var15 = ByteUtil.divUSeg(var15) / (LEVELS[var2] / 2);
                                 int var16 = LEVELS[LEVELS.length - 1 - var2] << 1;
                                 short var18 = (short)(var15 * var16 * var16 + var14 * var16 + var9);
                                 iteration[var2][var7][var8][var17] = var18;
                                 ++var17;
                              }
                           }
                        }
                     }
                  }
               }
            }
         }

         ++var2;
      }

   }
}
