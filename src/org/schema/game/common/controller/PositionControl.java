package org.schema.game.common.controller;

import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import java.util.Iterator;
import org.schema.game.common.data.element.ElementCollection;

public class PositionControl {
   private final LongOpenHashSet controlMap = new LongOpenHashSet();
   private final LongOpenHashSet controlPosMap = new LongOpenHashSet();
   private short type;

   public void clear() {
      this.getControlMap().clear();
   }

   public boolean equals(Object var1) {
      if (var1 != null && var1 instanceof PositionControl) {
         return this.getType() == ((PositionControl)var1).getType();
      } else {
         return false;
      }
   }

   public LongOpenHashSet getControlMap() {
      return this.controlMap;
   }

   public short getType() {
      return this.type;
   }

   public void setType(short var1) {
      this.type = var1;
   }

   public void addControlled(LongOpenHashSet var1) {
      this.controlMap.addAll(var1);
      Iterator var4 = var1.iterator();

      while(var4.hasNext()) {
         long var2 = (Long)var4.next();
         this.getControlPosMap().add(ElementCollection.getPosIndexFrom4(var2));
      }

   }

   public LongOpenHashSet getControlPosMap() {
      return this.controlPosMap;
   }
}
