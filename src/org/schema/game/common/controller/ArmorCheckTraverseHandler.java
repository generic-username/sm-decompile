package org.schema.game.common.controller;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.InnerSegmentIterator;
import org.schema.game.common.data.physics.RayTraceGridTraverser;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;

public class ArmorCheckTraverseHandler extends InnerSegmentIterator {
   public ArmorValue armorValue;

   public boolean handle(int var1, int var2, int var3, RayTraceGridTraverser var4) {
      assert this.armorValue != null;

      SegmentController var5 = this.getContextObj();
      int var6 = var1 - this.currentSeg.pos.x + 16;
      int var7 = var2 - this.currentSeg.pos.y + 16;
      int var8 = var3 - this.currentSeg.pos.z + 16;
      if (this.debug) {
         var4.drawDebug(var1 + 16, var2 + 16, var3 + 16, this.tests, var5.getWorldTransform());
      }

      ++this.tests;
      SegmentData var9 = this.currentSeg.getSegmentData();
      if (var6 >= 0 && var6 < 32 && var7 >= 0 && var7 < 32 && var8 >= 0 && var8 < 32) {
         short var11;
         if ((var11 = var9.getType(var3 = SegmentData.getInfoIndex((byte)var6, (byte)var7, (byte)var8))) > 0 && ElementInformation.isPhysicalRayTests(var11, var9, var3) && this.isZeroHpPhysical(var9, var3)) {
            this.v.elemA.set((byte)var6, (byte)var7, (byte)var8);
            this.v.elemPosA.set((float)(this.v.elemA.x - 16), (float)(this.v.elemA.y - 16), (float)(this.v.elemA.z - 16));
            Vector3f var10000 = this.v.elemPosA;
            var10000.x += (float)this.currentSeg.pos.x;
            var10000 = this.v.elemPosA;
            var10000.y += (float)this.currentSeg.pos.y;
            var10000 = this.v.elemPosA;
            var10000.z += (float)this.currentSeg.pos.z;
            this.rayResult.collisionObject = this.collisionObject;
            boolean var10;
            if (!(var10 = this.processRawHitUnshielded(this.currentSeg, var3, var11, this.v.elemA, this.v.elemPosA, this.testCubes))) {
               this.hitSignal = true;
            }

            return var10;
         }

         if (this.armorValue.typesHit.size() > 0) {
            this.hitSignal = true;
            return false;
         }
      }

      return true;
   }

   private boolean processRawHitUnshielded(Segment var1, int var2, short var3, Vector3b var4, Vector3f var5, Transform var6) {
      ElementInformation var7 = ElementKeyMap.getInfoFast(var3);
      this.armorValue.typesHit.add(var7);
      ArmorValue var10000 = this.armorValue;
      var10000.armorValueAccumulatedRaw += var7.getArmorValue() + var7.getArmorValue() * (float)this.armorValue.typesHit.size() * VoidElementManager.ARMOR_THICKNESS_BONUS;
      var10000 = this.armorValue;
      var10000.armorIntegrity += (float)var1.getSegmentData().getHitpointsByte(var2) * 0.007874016F;
      return var7.isArmor();
   }
}
