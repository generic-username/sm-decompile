package org.schema.game.common.controller;

import org.schema.game.network.objects.NetworkEntityProvider;
import org.schema.schine.network.objects.Sendable;

public interface NetworkListenerEntity extends Sendable {
   void setClientId(int var1);

   int getClientId();

   void setId(int var1);

   int getId();

   NetworkEntityProvider getNetworkObject();

   boolean isSendTo();
}
