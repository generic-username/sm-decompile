package org.schema.game.common.controller;

import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import java.util.Stack;
import org.schema.common.FastMath;
import org.schema.game.server.controller.world.factory.planet.FastNoiseSIMD;

public class SegmentBufferDensityMap {
   private static final int VOXEL_STEP_X = 16641;
   private static final int VOXEL_STEP_Y = 129;
   private static final int VOXEL_STEP_Z = 1;
   private static Stack objectPool = new Stack();
   public float[] densityMap = FastNoiseSIMD.a(129, 129, 129);
   public float[] densityMapTemp = FastNoiseSIMD.a(129, 129, 129);
   private Int2IntOpenHashMap voxelIndexVertMap = new Int2IntOpenHashMap();
   private IntArrayList tris = new IntArrayList();
   private FloatArrayList verts = new FloatArrayList();
   private FloatArrayList normals = new FloatArrayList();

   public static SegmentBufferDensityMap getSegmentBufferDensityMap() {
      synchronized(objectPool) {
         return objectPool.empty() ? new SegmentBufferDensityMap() : (SegmentBufferDensityMap)objectPool.pop();
      }
   }

   public static void returnSegmentBufferDensityMap(SegmentBufferDensityMap var0) {
      var0.clean();
      synchronized(objectPool) {
         objectPool.push(var0);
      }
   }

   public void clean() {
      this.voxelIndexVertMap.clear();
      this.tris.clear();
      this.verts.clear();
      this.normals.clear();
   }

   public void generateMesh() {
      for(int var1 = 0; var1 < 128; ++var1) {
         for(int var2 = 0; var2 < 128; ++var2) {
            for(int var3 = 0; var3 < 128; ++var3) {
               int var4 = var1 * 16641 + var2 * 129 + var3;
               float var5 = this.densityMap[var4];
               float var6 = this.densityMap[var4 + 16641];
               float var7 = this.densityMap[var4 + 129];
               float var8 = this.densityMap[var4 + 1];
               int var9;
               int var10;
               int var11;
               int var12;
               if (var5 >= 0.0F) {
                  if (var6 < 0.0F) {
                     var11 = this.voxelVertIndex(var4, (float)var1, (float)(var2 + 1), (float)var3);
                     var12 = this.voxelVertIndex(var4, (float)var1, (float)(var2 + 1), (float)(var3 + 1));
                     var9 = this.voxelVertIndex(var4, (float)var1, (float)var2, (float)(var3 + 1));
                     var10 = this.voxelVertIndex(var4, (float)var1, (float)var2, (float)var3);
                     this.addQuadTris(var11, var12, var9, var10);
                  }

                  if (var7 < 0.0F) {
                     var11 = this.voxelVertIndex(var4, (float)var1, (float)var2, (float)(var3 + 1));
                     var12 = this.voxelVertIndex(var4, (float)(var1 + 1), (float)var2, (float)(var3 + 1));
                     var9 = this.voxelVertIndex(var4, (float)(var1 + 1), (float)var2, (float)var3);
                     var10 = this.voxelVertIndex(var4, (float)var1, (float)var2, (float)var3);
                     this.addQuadTris(var11, var12, var9, var10);
                  }

                  if (var8 < 0.0F) {
                     var11 = this.voxelVertIndex(var4, (float)(var1 + 1), (float)var2, (float)var3);
                     var12 = this.voxelVertIndex(var4, (float)(var1 + 1), (float)(var2 + 1), (float)var3);
                     var9 = this.voxelVertIndex(var4, (float)var1, (float)(var2 + 1), (float)var3);
                     var10 = this.voxelVertIndex(var4, (float)var1, (float)var2, (float)var3);
                     this.addQuadTris(var11, var12, var9, var10);
                  }
               } else {
                  if (var6 >= 0.0F) {
                     var11 = this.voxelVertIndex(var4, (float)var1, (float)var2, (float)var3);
                     var12 = this.voxelVertIndex(var4, (float)var1, (float)var2, (float)(var3 + 1));
                     var9 = this.voxelVertIndex(var4, (float)var1, (float)(var2 + 1), (float)(var3 + 1));
                     var10 = this.voxelVertIndex(var4, (float)var1, (float)(var2 + 1), (float)var3);
                     this.addQuadTris(var11, var12, var9, var10);
                  }

                  if (var7 >= 0.0F) {
                     var11 = this.voxelVertIndex(var4, (float)var1, (float)var2, (float)var3);
                     var12 = this.voxelVertIndex(var4, (float)(var1 + 1), (float)var2, (float)var3);
                     var9 = this.voxelVertIndex(var4, (float)(var1 + 1), (float)var2, (float)(var3 + 1));
                     var10 = this.voxelVertIndex(var4, (float)var1, (float)var2, (float)(var3 + 1));
                     this.addQuadTris(var11, var12, var9, var10);
                  }

                  if (var8 >= 0.0F) {
                     var11 = this.voxelVertIndex(var4, (float)var1, (float)var2, (float)var3);
                     var12 = this.voxelVertIndex(var4, (float)var1, (float)(var2 + 1), (float)var3);
                     var9 = this.voxelVertIndex(var4, (float)(var1 + 1), (float)(var2 + 1), (float)var3);
                     var10 = this.voxelVertIndex(var4, (float)(var1 + 1), (float)var2, (float)var3);
                     this.addQuadTris(var11, var12, var9, var10);
                  }
               }
            }
         }
      }

      this.verts.size();
   }

   private int voxelVertIndex(int var1, float var2, float var3, float var4) {
      if (this.voxelIndexVertMap.containsKey(var1)) {
         return this.voxelIndexVertMap.get(var1);
      } else {
         float var5 = this.densityMap[var1];
         float var6 = this.densityMap[var1 + 16641];
         float var7 = this.densityMap[var1 + 129];
         float var8 = this.densityMap[var1 + 16641 + 129];
         float var9 = this.densityMap[var1 + 1];
         float var10 = this.densityMap[var1 + 16641 + 1];
         float var11 = this.densityMap[var1 + 129 + 1];
         float var12 = this.densityMap[var1 + 16641 + 129 + 1];
         float var13 = (var5 + var6 + var7 + var8 + var9 + var10 + var11 + var12) * -0.125F;
         float var14 = (-var5 + var6 - var7 + var8 - var9 + var10 - var11 + var12) * 0.25F;
         float var15 = (-var5 - var6 + var7 + var8 - var9 - var10 + var11 + var12) * 0.25F;
         var5 = (-var5 - var6 - var7 - var8 + var9 + var10 + var11 + var12) * 0.25F;
         var6 = var14 * var14 + var15 * var15 + var5 * var5;
         var13 /= var6;
         var14 *= var13;
         var15 *= var13;
         var5 *= var13;
         int var17 = this.verts.size();
         this.voxelIndexVertMap.add(var1, var17);
         this.verts.add(var2 + var14);
         this.verts.add(var3 + var15);
         this.verts.add(var4 + var5);
         float var16 = FastMath.carmackInvSqrt(var6);
         var14 *= var16;
         var15 *= var16;
         var5 *= var16;
         this.normals.add(var14);
         this.normals.add(var15);
         this.normals.add(var5);
         return var17;
      }
   }

   private void addQuadTris(int var1, int var2, int var3, int var4) {
      float var5 = this.verts.get(var1) - this.verts.get(var3);
      float var6 = this.verts.get(var1 + 1) - this.verts.get(var3 + 1);
      float var7 = this.verts.get(var1 + 2) - this.verts.get(var3 + 2);
      float var8 = this.verts.get(var2) - this.verts.get(var4);
      float var9 = this.verts.get(var2 + 1) - this.verts.get(var4 + 1);
      float var10 = this.verts.get(var2 + 2) - this.verts.get(var4 + 2);
      if (var5 * var5 + var6 * var6 + var7 * var7 < var8 * var8 + var9 * var9 + var10 * var10) {
         this.tris.add(var1);
         this.tris.add(var2);
         this.tris.add(var3);
         this.tris.add(var1);
         this.tris.add(var3);
         this.tris.add(var4);
      } else {
         this.tris.add(var2);
         this.tris.add(var3);
         this.tris.add(var4);
         this.tris.add(var2);
         this.tris.add(var4);
         this.tris.add(var1);
      }
   }
}
