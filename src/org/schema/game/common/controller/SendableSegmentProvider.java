package org.schema.game.common.controller;

import com.googlecode.javaewah.EWAHCompressedBitmap;
import it.unimi.dsi.fastutil.longs.LongArrayFIFOQueue;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.util.Iterator;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.controller.element.world.ClientSegmentProvider;
import org.schema.game.common.controller.elements.InventoryMap;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.player.GenericProvider;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventoryHolder;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.Segment;
import org.schema.game.network.objects.BitsetResponse;
import org.schema.game.network.objects.NetworkSegmentProvider;
import org.schema.game.network.objects.remote.RemoteBitset;
import org.schema.game.network.objects.remote.RemoteControlStructure;
import org.schema.game.network.objects.remote.RemoteInventory;
import org.schema.game.network.objects.remote.RemoteInventoryBuffer;
import org.schema.game.network.objects.remote.RemoteLongStringBuffer;
import org.schema.game.network.objects.remote.RemoteManualMouseEvent;
import org.schema.game.network.objects.remote.RemoteServerMessage;
import org.schema.game.network.objects.remote.RemoteShortIntPairBuffer;
import org.schema.game.network.objects.remote.RemoteTextBlockPair;
import org.schema.game.network.objects.remote.TextBlockPair;
import org.schema.game.server.controller.GameServerController;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.RemoteLongBuffer;
import org.schema.schine.network.server.ServerMessage;

public class SendableSegmentProvider extends GenericProvider {
   private final Vector3i posTmp = new Vector3i();
   private NetworkSegmentProvider networkSegmentProvider;
   private boolean controlElementMapRequestReceived = false;
   private boolean flagServerSendInventories = true;
   private LongOpenHashSet requestedTextBlocks = new LongOpenHashSet();
   private LongOpenHashSet changedTextBoxLongRangeIndices = new LongOpenHashSet();

   public SendableSegmentProvider(StateInterface var1) {
      super(var1);
   }

   private void answerSegmentSignatureRequest(long var1, long var3) {
      StateInterface var10000 = this.state;

      try {
         long var5 = ElementCollection.getPosIndexFrom4(var1);
         short var12 = (short)ElementCollection.getType(var1);
         NetworkSegmentProvider var2 = this.getNetworkObject();
         ElementCollection.getPosFromIndex(var5, this.posTmp);
         int var7;
         if ((var7 = ((SendableSegmentController)this.getSegmentController()).getSegmentBuffer().getSegmentState(this.posTmp.x, this.posTmp.y, this.posTmp.z)) >= 0) {
            RemoteSegment var13 = (RemoteSegment)((SendableSegmentController)this.getSegmentController()).getSegmentFromCache(this.posTmp.x, this.posTmp.y, this.posTmp.z);
            synchronized(this.getNetworkObject()) {
               ((GameServerState)this.state).getController();
               GameServerController.handleSegmentRequest(var2, var13, var5, var3, var12);
            }
         } else if (var7 == -1) {
            synchronized(this.getNetworkObject()) {
               ((GameServerState)this.state).getController().handleEmpty(var2, (SegmentController)this.getSegmentController(), this.posTmp, var5, var3);
            }
         } else {
            ((GameServerState)this.state).getController().scheduleSegmentRequest((SegmentController)this.getSegmentController(), new Vector3i(this.posTmp), var2, var3, var12, false, false);
         }
      } catch (Exception var11) {
         var11.printStackTrace();
         System.err.println("[SendableSegmentProvider] Exception catched for ID: " + this.getSegmentController() + "; if null, the segmentcontroller has probably been removed (id for both: " + this.getId() + ")");
      }
   }

   public NetworkSegmentProvider getNetworkObject() {
      return this.networkSegmentProvider;
   }

   public void newNetworkObject() {
      this.networkSegmentProvider = new NetworkSegmentProvider(this.state, this);
      if (this.getSegmentController() instanceof ManagedSegmentController && ((ManagedSegmentController)this.getSegmentController()).getManagerContainer() instanceof InventoryHolder) {
         this.networkSegmentProvider.invetoryBuffer = new RemoteInventoryBuffer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), this.networkSegmentProvider);
      } else {
         this.networkSegmentProvider.invetoryBuffer = new RemoteInventoryBuffer((InventoryHolder)null, this.networkSegmentProvider);
      }
   }

   public void updateFromNetworkObject(NetworkObject var1, int var2) {
      super.updateFromNetworkObject(var1, var2);
      if ((SendableSegmentController)this.getSegmentController() == null) {
         System.err.println("[SendableSegmentProvider] no longer updating: missing segment controller: " + this.getId() + ": " + this.getState());
      } else {
         this.setId(this.networkSegmentProvider.id.get());
         if (!this.isOnServer()) {
            ((ClientSegmentProvider)((SendableSegmentController)this.getSegmentController()).getSegmentProvider()).receivedFromNT(this, (NetworkSegmentProvider)var1);
         }

         for(var2 = 0; var2 < this.getNetworkObject().messagesToBlocks.getReceiveBuffer().size(); ++var2) {
            ServerMessage var3 = (ServerMessage)((RemoteServerMessage)this.getNetworkObject().messagesToBlocks.getReceiveBuffer().get(var2)).get();
            ((SendableSegmentController)this.getSegmentController()).receivedBlockMessages.enqueue(var3);
         }

         ((SendableSegmentController)this.getSegmentController()).handleReceivedModifications((NetworkSegmentProvider)var1);
         ((SendableSegmentController)this.getSegmentController()).handleReceivedBlockActivations((NetworkSegmentProvider)var1);
         if (this.isOnServer()) {
            int var13;
            if (((NetworkSegmentProvider)var1).segmentBufferRequestBuffer.getReceiveBuffer().size() > 0) {
               synchronized(((GameServerState)this.state).getSegmentRequests()) {
                  for(var13 = 0; var13 < ((NetworkSegmentProvider)var1).segmentBufferRequestBuffer.getReceiveBuffer().size(); ++var13) {
                     this.answerSegmentBufferRequest(((NetworkSegmentProvider)var1).segmentBufferRequestBuffer.getReceiveBuffer().get(var13));
                  }
               }
            }

            if (((NetworkSegmentProvider)var1).segmentClientToServerCombinedRequestBuffer.getReceiveBuffer().size() > 0) {
               synchronized(((GameServerState)this.state).getSegmentRequests()) {
                  for(var13 = 0; var13 < ((NetworkSegmentProvider)var1).segmentClientToServerCombinedRequestBuffer.getReceiveBuffer().size(); var13 += 2) {
                     long var4 = ((NetworkSegmentProvider)var1).segmentClientToServerCombinedRequestBuffer.getReceiveBuffer().get(var13);
                     long var6 = ((NetworkSegmentProvider)var1).segmentClientToServerCombinedRequestBuffer.getReceiveBuffer().get(var13 + 1);
                     this.answerSegmentSignatureRequest(var4, var6);
                  }
               }
            }
         }

         if (this.isOnServer() && this.controlElementMapRequestReceived && !(Boolean)this.getNetworkObject().requestedInitialControlMap.get()) {
            this.controlElementMapRequestReceived = false;
         }

         if (this.isOnServer() && !this.controlElementMapRequestReceived && (Boolean)this.getNetworkObject().requestedInitialControlMap.get()) {
            this.getNetworkObject().initialControlMap.add(new RemoteControlStructure(this, this.isOnServer()));
            this.controlElementMapRequestReceived = true;
         }

         ManagerContainer var12;
         if (this.isOnServer() && this.getSegmentController() instanceof ManagedSegmentController && ((ManagedSegmentController)this.getSegmentController()).getManagerContainer() instanceof InventoryHolder) {
            var12 = ((ManagedSegmentController)this.getSegmentController()).getManagerContainer();
            if (this.getNetworkObject().inventoryDetailRequests.getReceiveBuffer().size() > 0) {
               var12.handleInventoryDetailRequests(this.getNetworkObject(), this.getNetworkObject().inventoryDetailRequests);
            }
         }

         if (!this.isOnServer() && this.getSegmentController() instanceof ManagedSegmentController && ((ManagedSegmentController)this.getSegmentController()).getManagerContainer() instanceof InventoryHolder) {
            var12 = ((ManagedSegmentController)this.getSegmentController()).getManagerContainer();
            if (this.getNetworkObject().inventoryDetailAnswers.getReceiveBuffer().size() > 0) {
               var12.handleInventoryDetailAnswer(this.getNetworkObject(), this.getNetworkObject().inventoryDetailAnswers);
            }
         }

         if (this.getSegmentController() instanceof ManagedSegmentController) {
            var12 = ((ManagedSegmentController)this.getSegmentController()).getManagerContainer();
            ObjectArrayList var15 = this.getNetworkObject().manualMouseEventBuffer.getReceiveBuffer();

            for(int var14 = 0; var14 < var15.size(); ++var14) {
               ManagerContainer.ManualMouseEvent var5 = (ManagerContainer.ManualMouseEvent)((RemoteManualMouseEvent)var15.get(var14)).get();
               var12.triggeredManualMouseEvent(var5);
            }
         }

         if (!this.isOnServer() && this.getSegmentController() instanceof ManagedSegmentController && ((ManagedSegmentController)this.getSegmentController()).getManagerContainer() instanceof InventoryHolder) {
            var12 = ((ManagedSegmentController)this.getSegmentController()).getManagerContainer();
            if (this.getNetworkObject().invetoryBuffer.getReceiveBuffer().size() > 0) {
               System.err.println("[CLIENT] RECEIVED INITIAL INVETORY LIST FOR " + this.getSegmentController());
               var12.handleInventoryFromNT(this.getNetworkObject().invetoryBuffer, (RemoteLongBuffer)null, (RemoteBuffer)null, (RemoteShortIntPairBuffer)null, (RemoteShortIntPairBuffer)null, (RemoteLongStringBuffer)null);
               var12.handleInventoryReceivedNT();
            }
         }

         for(var2 = 0; var2 < ((NetworkSegmentProvider)var1).textBlockResponsesAndChangeRequests.getReceiveBuffer().size(); ++var2) {
            synchronized(((SendableSegmentController)this.getSegmentController()).receivedTextBlocks) {
               TextBlockPair var16 = (TextBlockPair)((RemoteTextBlockPair)((NetworkSegmentProvider)var1).textBlockResponsesAndChangeRequests.getReceiveBuffer().get(var2)).get();
               ((SendableSegmentController)this.getSegmentController()).receivedTextBlocks.enqueue(var16);
            }
         }

         for(var2 = 0; var2 < ((NetworkSegmentProvider)var1).textBlockChangeInLongRange.getReceiveBuffer().size(); ++var2) {
            ((NetworkSegmentProvider)var1).textBlockChangeInLongRange.getReceiveBuffer().getLong(var2);
            LongArrayFIFOQueue var18;
            synchronized(var18 = ((SendableSegmentController)this.getSegmentController()).textBlockChangeInLongRange){}
         }

         for(var2 = 0; var2 < ((NetworkSegmentProvider)var1).textBlockRequests.getReceiveBuffer().size(); ++var2) {
            long var17 = ((NetworkSegmentProvider)var1).textBlockRequests.getReceiveBuffer().getLong(var2);
            synchronized(((SendableSegmentController)this.getSegmentController()).receivedTextBlockRequests) {
               TextBlockPair var19;
               (var19 = new TextBlockPair()).provider = this;
               var19.block = var17;
               ((SendableSegmentController)this.getSegmentController()).receivedTextBlockRequests.enqueue(var19);
               ((SendableSegmentController)this.getSegmentController()).addFlagRemoveCachedTextBoxes(this);
            }
         }

         if (this.isOnServer() && (Boolean)((NetworkSegmentProvider)var1).signalDelete.get()) {
            this.setMarkedForDeleteVolatile(true);
         }

      }
   }

   public void updateLocal(Timer var1) {
      super.updateLocal(var1);
      if (this.isConnectionReady() && this.flagServerSendInventories) {
         InventoryMap var2;
         ManagerContainer var6;
         synchronized(var2 = (var6 = ((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getInventories()) {
            Iterator var7 = var2.values().iterator();

            while(true) {
               if (!var7.hasNext()) {
                  break;
               }

               Inventory var4 = (Inventory)var7.next();
               this.getNetworkObject().invetoryBuffer.add(new RemoteInventory(var4, var6, true, this.onServer));
            }
         }

         this.flagServerSendInventories = false;
      }

   }

   public void requestCurrentControlMap() {
      ((SendableSegmentController)this.getSegmentController()).getControlElementMap().setFlagRequested(true);
      this.getNetworkObject().requestedInitialControlMap.forceClientUpdates();
      this.getNetworkObject().requestedInitialControlMap.set(true, true);
   }

   public void resetControlMapRequest() {
      ((SendableSegmentController)this.getSegmentController()).getControlElementMap().setFlagRequested(false);
      this.getNetworkObject().requestedInitialControlMap.forceClientUpdates();
      this.getNetworkObject().requestedInitialControlMap.set(false, true);
   }

   public void requestCurrentInventories() {
      if (this.getSegmentController() instanceof ManagedSegmentController && ((ManagedSegmentController)this.getSegmentController()).getManagerContainer() instanceof InventoryHolder) {
         try {
            ((GameClientController)this.getState().getController()).requestInvetoriesUnblocked(((SendableSegmentController)this.getSegmentController()).getId());
            return;
         } catch (IOException var1) {
            var1.printStackTrace();
         }
      }

   }

   public void sendServerInventories() {
      if (this.isConnectionReady()) {
         this.flagServerSendInventories = true;
         if (this.isOnServer()) {
            ((GameServerState)this.getState()).scheduleUpdate(this);
         }
      }

   }

   public void setConnectionReady() {
      this.getNetworkObject().connectionReady.set(true);
   }

   public void answerSegmentBufferRequest(long var1) {
      StateInterface var10000 = this.state;

      try {
         NetworkSegmentProvider var3 = this.getNetworkObject();
         ElementCollection.getPosFromIndex(var1, this.posTmp);
         int var9 = ((SendableSegmentController)this.getSegmentController()).getSegmentBuffer().getSegmentState(this.posTmp.x, this.posTmp.y, this.posTmp.z);
         long var4 = SegmentBufferManager.getBufferIndexFromAbsolute(this.posTmp);
         EWAHCompressedBitmap var10;
         BitsetResponse var11;
         if (var9 >= 0) {
            if ((var10 = ((SendableSegmentController)this.getSegmentController()).getSegmentProvider().getSegmentDataIO().requestSignature(this.posTmp.x, this.posTmp.y, this.posTmp.z)) != null) {
               var10 = ((SendableSegmentController)this.getSegmentController()).getSegmentBuffer().applyBitMap(var4, var10);
            }

            if (var10 != null) {
               ((SendableSegmentController)this.getSegmentController()).getSegmentBuffer().insertFromBitset(this.posTmp, var4, var10, new SegmentBufferIteratorEmptyInterface() {
                  public boolean handle(Segment var1, long var2) {
                     return false;
                  }

                  public boolean handleEmpty(int var1, int var2, int var3, long var4) {
                     return false;
                  }
               });
            }

            var11 = new BitsetResponse(var4, var10, new Vector3i(this.posTmp));
            synchronized(this.getNetworkObject()) {
               this.getNetworkObject().segmentBufferAwnserBuffer.add(new RemoteBitset(var11, this.getNetworkObject()));
            }
         } else if (var9 == -1) {
            if ((var10 = ((SendableSegmentController)this.getSegmentController()).getSegmentProvider().getSegmentDataIO().requestSignature(this.posTmp.x, this.posTmp.y, this.posTmp.z)) != null) {
               var10 = ((SendableSegmentController)this.getSegmentController()).getSegmentBuffer().applyBitMap(var4, var10);
            }

            if (var10 != null) {
               ((SendableSegmentController)this.getSegmentController()).getSegmentBuffer().insertFromBitset(this.posTmp, var4, var10, new SegmentBufferIteratorEmptyInterface() {
                  public boolean handle(Segment var1, long var2) {
                     return false;
                  }

                  public boolean handleEmpty(int var1, int var2, int var3, long var4) {
                     return false;
                  }
               });
            }

            var11 = new BitsetResponse(var4, var10, new Vector3i(this.posTmp));
            synchronized(this.getNetworkObject()) {
               this.getNetworkObject().segmentBufferAwnserBuffer.add(new RemoteBitset(var11, this.getNetworkObject()));
            }
         } else {
            ((GameServerState)this.state).getController().scheduleSegmentRequest((SegmentController)this.getSegmentController(), new Vector3i(this.posTmp), var3, -1L, (short)-2, true, false);
         }
      } catch (Exception var8) {
         var8.printStackTrace();
         System.err.println("[SendableSegmentProvider] Exception catched for ID: " + this.getSegmentController() + "; if null, the segmentcontroller has probably been removed (id for both: " + this.getId() + ")");
      }
   }

   public void clientTextBlockRequest(long var1) {
      assert !this.isOnServer();

      synchronized(this.requestedTextBlocks) {
         if (!this.requestedTextBlocks.contains(var1)) {
            this.requestedTextBlocks.add(var1);
            synchronized(this.getNetworkObject().getState()) {
               boolean var5;
               if (var5 = !this.getNetworkObject().getState().isSynched()) {
                  this.getNetworkObject().getState().setSynched();
               }

               this.getNetworkObject().textBlockRequests.add(var1);
               if (var5) {
                  this.getNetworkObject().getState().setUnsynched();
               }
            }
         }

      }
   }

   public LongOpenHashSet getRequestedTextBlocks() {
      return this.requestedTextBlocks;
   }

   public void setRequestedTextBlocks(LongOpenHashSet var1) {
      this.requestedTextBlocks = var1;
   }

   public void sendTextBoxCacheClearIfNotSentYet(long var1) {
      assert this.isOnServer();

      if (!this.changedTextBoxLongRangeIndices.contains(var1)) {
         this.getNetworkObject().textBlockChangeInLongRange.add(var1);
         this.changedTextBoxLongRangeIndices.add(var1);
      }

   }

   public void clearChangedTextBoxLongRangeIndices() {
      this.changedTextBoxLongRangeIndices.clear();
   }

   public boolean isPrivateNetworkObject() {
      return true;
   }
}
