package org.schema.game.common.controller;

import org.schema.game.common.data.element.ElementInformation;

public interface ElementCheckCallback {
   boolean isCheckCriteriaSatisfied(ElementInformation var1);

   boolean isCriteriaToCheckSatisfied(ElementInformation var1);
}
