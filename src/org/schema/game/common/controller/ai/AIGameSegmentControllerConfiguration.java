package org.schema.game.common.controller.ai;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.graphicsengine.core.settings.states.StaticStates;
import org.schema.schine.network.StateInterface;

public abstract class AIGameSegmentControllerConfiguration extends AIGameConfiguration {
   public AIGameSegmentControllerConfiguration(StateInterface var1, SegmentController var2) {
      super(var1, var2);
   }

   public void initialize(Int2ObjectOpenHashMap var1) {
      var1.put(Types.AIM_AT.ordinal(), new AIConfiguationElements(Types.AIM_AT, "Any", new StaticStates(new Object[]{"Any", "Selected Target", "Ships", "Stations", "Missiles", "Astronauts"}), this));
      var1.put(Types.TYPE.ordinal(), new AIConfiguationElements(Types.TYPE, "Ship", new StaticStates(new Object[]{"Turret", "Ship", "Fleet"}), this));
      var1.put(Types.ACTIVE.ordinal(), new AIConfiguationElements(Types.ACTIVE, false, new StaticStates(new Object[]{false, true}), this));
      var1.put(Types.MANUAL.ordinal(), new AIConfiguationElements(Types.MANUAL, false, new StaticStates(new Object[]{false, true}), this));
      var1.put(Types.PRIORIZATION.ordinal(), new AIConfiguationElements(Types.PRIORIZATION, "Highest", new StaticStates(new Object[]{"Highest", "Lowest", "Random"}), this));
      var1.put(Types.FIRE_MODE.ordinal(), new AIConfiguationElements(Types.FIRE_MODE, "Simultaneous", new StaticStates(new Object[]{"Simultaneous", "Volley"}), this));
   }
}
