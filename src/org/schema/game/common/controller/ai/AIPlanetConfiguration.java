package org.schema.game.common.controller.ai;

import org.schema.game.common.controller.Planet;
import org.schema.game.server.ai.PlanetAIEntity;
import org.schema.schine.network.StateInterface;

public class AIPlanetConfiguration extends AIGameSegmentControllerConfiguration {
   public AIPlanetConfiguration(StateInterface var1, Planet var2) {
      super(var1, var2);
   }

   protected PlanetAIEntity getIdleEntityState() {
      return new PlanetAIEntity("PAI", (Planet)this.getOwner());
   }

   protected boolean isForcedHitReaction() {
      return ((Planet)this.getOwner()).getFactionId() < 0;
   }

   protected void prepareActivation() {
   }
}
