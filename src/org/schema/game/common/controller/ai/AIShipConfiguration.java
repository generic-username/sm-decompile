package org.schema.game.common.controller.ai;

import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.fleetcontrollable.FleetControllableShipAIEntity;
import org.schema.game.server.ai.program.searchanddestroy.SimpleSearchAndDestroyShipAIEntity;
import org.schema.game.server.ai.program.turret.TurretShipAIEntity;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.StateInterface;

public class AIShipConfiguration extends AIGameSegmentControllerConfiguration {
   public AIShipConfiguration(StateInterface var1, Ship var2) {
      super(var1, var2);
   }

   protected ShipAIEntity getIdleEntityState() {
      return new ShipAIEntity("shipAiEntity", (Ship)this.getOwner());
   }

   protected void onClientSettingChanged(AIConfiguationElements var1) {
      if (var1.getType() == Types.ACTIVE) {
         var1.isOn();
         this.setAIEntity(new ShipAIEntity("shipAiEntity", (Ship)this.getOwner()));
      }

   }

   public void onCoreDestroyed(Damager var1) {
      if (this.isActiveAI()) {
         if (var1 != null && ((Ship)this.getOwner()).getFactionId() == -1) {
            ((GameServerState)var1.getState()).getController().spawnKillBonus(this.getOwner());
         }

         if (this.getOwner() != null && ((Ship)this.getOwner()).getFactionId() != 0) {
            ((Ship)this.getOwner()).railController.resetFactionForEntitiesWithoutFactionBlock(((Ship)this.getOwner()).getFactionId());
         }
      }

      super.onCoreDestroyed(var1);
   }

   public void onStartOverheating(Damager var1) {
      if (this.isActiveAI()) {
         if (var1 != null && ((Ship)this.getOwner()).getFactionId() == -1) {
            ((GameServerState)var1.getState()).getController().spawnKillBonus(this.getOwner());
         }

         if (this.getOwner() != null && ((Ship)this.getOwner()).getFactionId() != 0) {
            ((Ship)this.getOwner()).railController.resetFactionForEntitiesWithoutFactionBlock(((Ship)this.getOwner()).getFactionId());
         }
      }

      super.onStartOverheating(var1);
   }

   protected boolean isForcedHitReaction() {
      return false;
   }

   protected void onServerSettingChanged(AIConfiguationElements var1) {
      if (var1.getType() == Types.TYPE) {
         if (var1.getCurrentState().equals("Turret")) {
            this.setAIEntity(new TurretShipAIEntity((Ship)this.getOwner(), !this.get(Types.ACTIVE).isOn()));
         } else if (var1.getCurrentState().equals("Ship")) {
            this.setAIEntity(new SimpleSearchAndDestroyShipAIEntity((Ship)this.getOwner(), !this.get(Types.ACTIVE).isOn()));
         } else if (var1.getCurrentState().equals("Fleet")) {
            this.setAIEntity(new FleetControllableShipAIEntity((Ship)this.getOwner(), false));
         }
      }

      if (((String)this.get(Types.TYPE).getCurrentState()).equals("Fleet") && !this.get(Types.ACTIVE).isOn()) {
         this.get(Types.ACTIVE).setCurrentState(true, false);
      }

      super.onServerSettingChanged(var1);
   }

   protected void prepareActivation() {
      AIConfiguationElements var1;
      if ((var1 = this.get(Types.TYPE)).getCurrentState().equals("Turret")) {
         this.setAIEntity(new TurretShipAIEntity((Ship)this.getOwner(), !this.get(Types.ACTIVE).isOn()));
      } else if (var1.getCurrentState().equals("Ship")) {
         this.setAIEntity(new SimpleSearchAndDestroyShipAIEntity((Ship)this.getOwner(), !this.get(Types.ACTIVE).isOn()));
      } else {
         if (var1.getCurrentState().equals("Fleet")) {
            this.setAIEntity(new FleetControllableShipAIEntity((Ship)this.getOwner(), false));
            if (!this.get(Types.ACTIVE).isOn()) {
               this.get(Types.ACTIVE).setCurrentState(true, true);
            }
         }

      }
   }
}
