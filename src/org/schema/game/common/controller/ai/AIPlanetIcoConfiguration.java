package org.schema.game.common.controller.ai;

import org.schema.game.common.controller.PlanetIco;
import org.schema.game.server.ai.PlanetIcoAIEntity;
import org.schema.schine.network.StateInterface;

public class AIPlanetIcoConfiguration extends AIGameSegmentControllerConfiguration {
   public AIPlanetIcoConfiguration(StateInterface var1, PlanetIco var2) {
      super(var1, var2);
   }

   protected PlanetIcoAIEntity getIdleEntityState() {
      return new PlanetIcoAIEntity("PAICO", (PlanetIco)this.getOwner());
   }

   protected boolean isForcedHitReaction() {
      return ((PlanetIco)this.getOwner()).getFactionId() < 0;
   }

   protected void prepareActivation() {
   }
}
