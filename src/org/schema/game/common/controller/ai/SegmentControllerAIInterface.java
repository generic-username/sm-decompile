package org.schema.game.common.controller.ai;

import org.schema.schine.ai.stateMachines.AiInterface;

public interface SegmentControllerAIInterface extends AiInterface {
   void activateAI(boolean var1, boolean var2);
}
