package org.schema.game.common.controller;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.schine.network.SerialializationInterface;

public class ShopOption implements SerialializationInterface {
   public String playerName;
   public int senderId;
   public long permission;
   public long credits;
   public ShopOption.ShopOptionType type;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeByte(this.type.ordinal());
      switch(this.type) {
      case LOCAL_PERMISSION:
      case TRADE_PERMISSION:
         var1.writeLong(this.permission);
         return;
      case CREDIT_WITHDRAWAL:
      case CREDIT_DEPOSIT:
         var1.writeLong(this.credits);
         return;
      case USER_ADD:
      case USER_REMOVE:
         var1.writeUTF(this.playerName);
      default:
      }
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.type = ShopOption.ShopOptionType.values()[var1.readByte()];
      switch(this.type) {
      case LOCAL_PERMISSION:
      case TRADE_PERMISSION:
         this.permission = var1.readLong();
         break;
      case CREDIT_WITHDRAWAL:
      case CREDIT_DEPOSIT:
         this.credits = var1.readLong();
         break;
      case USER_ADD:
      case USER_REMOVE:
         this.playerName = var1.readUTF();
      }

      this.senderId = var2;
   }

   public String toString() {
      return "ShopOption [senderId=" + this.senderId + ", type=" + this.type + ", playerName=" + this.playerName + ", permission=" + this.permission + ", credits=" + this.credits + "]";
   }

   public static enum ShopOptionType {
      USER_ADD,
      USER_REMOVE,
      LOCAL_PERMISSION,
      TRADE_PERMISSION,
      CREDIT_WITHDRAWAL,
      CREDIT_DEPOSIT;
   }
}
