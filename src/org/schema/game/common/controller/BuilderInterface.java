package org.schema.game.common.controller;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.BuildInstruction;
import org.schema.game.client.controller.manager.ingame.BuildSelectionCallback;
import org.schema.game.client.view.buildhelper.BuildHelper;

public interface BuilderInterface {
   void build(int var1, int var2, int var3, short var4, int var5, boolean var6, BuildSelectionCallback var7, Vector3i var8, int[] var9, BuildHelper var10, BuildInstruction var11) throws PositionBlockedException;
}
