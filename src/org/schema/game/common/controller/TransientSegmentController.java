package org.schema.game.common.controller;

public interface TransientSegmentController {
   boolean isTouched();

   void setTouched(boolean var1, boolean var2);

   boolean isMoved();

   void setMoved(boolean var1);

   boolean needsTagSave();
}
