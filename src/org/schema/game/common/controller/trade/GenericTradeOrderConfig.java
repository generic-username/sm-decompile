package org.schema.game.common.controller.trade;

import java.io.DataInput;
import java.io.IOException;

public class GenericTradeOrderConfig extends TradeOrderConfig {
   private double cargoPerShip;
   private long costPerSystem;
   private long costPerCargoShip;
   private double timePerSectorInSecs;
   private double profitOfValue;
   private double profitOfValuePerSystem;

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.cargoPerShip = var1.readDouble();
      this.costPerSystem = var1.readLong();
      this.costPerCargoShip = var1.readLong();
      this.timePerSectorInSecs = var1.readDouble();
      this.profitOfValue = var1.readDouble();
      this.profitOfValuePerSystem = var1.readDouble();
   }

   public double getCargoPerShip() {
      return this.cargoPerShip;
   }

   public long getCostPerSystem() {
      return this.costPerSystem;
   }

   public long getCostPerCargoShip() {
      return this.costPerCargoShip;
   }

   public double getTimePerSectorInSecs() {
      return this.timePerSectorInSecs;
   }

   public double getProfitOfValue() {
      return this.profitOfValue;
   }

   public double getProfitOfValuePerSystem() {
      return this.profitOfValuePerSystem;
   }
}
