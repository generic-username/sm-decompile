package org.schema.game.common.controller.trade;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import org.schema.common.util.LogInterface;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.controller.ShoppingAddOn;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.world.GalaxyManager;
import org.schema.game.network.objects.TradePriceInterface;
import org.schema.game.network.objects.TradePrices;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.StateInterface;

public class TradeOrder extends Observable implements LogInterface, SerialializationInterface {
   private TradeOrderConfig config;
   public LogInterface logger;
   private StateInterface state;
   public long toDbId;
   public long fromDbId;
   private Vector3i fromSystem;
   private Vector3i toSystem;
   private List orders;
   private double buyVolume;
   public ClientChannel clientChannelReceivedOn;
   private double sellVolume;
   private long usedPriceBuy;
   private long usedPriceSell;
   public boolean dirty;
   public int cacheId;

   public void log(String var1, LogInterface.LogLevel var2) {
      if (this.logger != null) {
         this.logger.log(var1, var2);
      } else {
         System.err.println("[TRADEORDER] " + var1);
      }
   }

   public TradeOrder(StateInterface var1, TradeOrderConfig var2, long var3, Vector3i var5, Vector3i var6, TradeNodeStub var7) {
      this(var1);
      this.fromSystem = new Vector3i(var5);
      this.toSystem = new Vector3i(var6);
      this.fromDbId = var3;
      this.toDbId = var7.getEntityDBId();
      this.config = var2;

      assert var3 > 0L;

      assert this.toDbId > 0L;

   }

   public TradeOrder(StateInterface var1) {
      this.orders = new ObjectArrayList();
      this.cacheId = Integer.MIN_VALUE;
      this.state = var1;
   }

   public int getTradeDistance() {
      return (int)Math.max(1.0D, Math.ceil((double)Vector3i.getDisatance(this.fromSystem, this.toSystem)));
   }

   public long getTotalPrice() {
      return this.getPriceDifference() + this.getTradingGuildPrice();
   }

   public long getPricePerDistance() {
      return (long)this.getTradeDistance() * this.config.getCostPerSystem();
   }

   public long getPriceDifference() {
      return this.getBuyPrice() - this.getSellPrice();
   }

   public long getTradeingGuildProfitTotal() {
      return this.getTradeingGuildProfit() + this.getTradeingGuildProfitPerSystem();
   }

   public long getTradeingGuildProfit() {
      return (long)((double)(this.getBuyPrice() + this.getSellPrice()) * this.config.getProfitOfValue());
   }

   public long getTradeingGuildProfitPerSystem() {
      return (long)((double)(this.getBuyPrice() + this.getSellPrice()) * this.config.getProfitOfValuePerSystem() * (double)this.getTradeDistance());
   }

   public long getTradeShipCost() {
      return (long)this.getUsedTradeShips() * this.config.getCostPerCargoShip();
   }

   public int getUsedTradeShips() {
      return this.getUsedTradeShipsBuy() + this.getUsedTradeShipsSell();
   }

   public int getUsedTradeShipsBuy() {
      return (int)Math.max(0.0D, Math.ceil(this.buyVolume / this.config.getCargoPerShip()));
   }

   public int getUsedTradeShipsSell() {
      return (int)Math.max(0.0D, Math.ceil(this.sellVolume / this.config.getCargoPerShip()));
   }

   public long getDeliveryCost() {
      return this.getPricePerDistance() + this.getTradeShipCost();
   }

   public long getTradingGuildPrice() {
      return Math.max(this.getTradeingGuildProfitTotal(), this.getDeliveryCost());
   }

   private void calcVolume(Short2ObjectOpenHashMap var1, Short2ObjectOpenHashMap var2) {
      this.buyVolume = 0.0D;
      this.sellVolume = 0.0D;

      for(int var3 = 0; var3 < this.orders.size(); ++var3) {
         TradeOrder.TradeOrderElement var4 = (TradeOrder.TradeOrderElement)this.orders.get(var3);

         assert var4.getInfo() != null;

         if (var4.getInfo() != null) {
            if (var4.isBuyOrder()) {
               this.buyVolume += (double)(var4.getInfo().getVolume() * (float)var4.amount);
            } else {
               this.sellVolume += (double)(var4.getInfo().getVolume() * (float)var4.amount);
            }
         } else {
            System.err.println("UNKNOWN BLOCK USED IN TRADE ORDER:::: " + var4.type);
         }
      }

   }

   public void normalize(GameServerState var1, float var2, float var3) {
      TradeNodeStub var4 = (TradeNodeStub)var1.getUniverse().getGalaxyManager().getTradeNodeDataById().get(this.fromDbId);
      TradeNodeStub var5 = (TradeNodeStub)var1.getUniverse().getGalaxyManager().getTradeNodeDataById().get(this.toDbId);
      long var6 = (long)((double)var4.getCredits() * (double)var2);
      long var8 = (long)((double)var5.getCredits() * (double)var3);
      double var10;
      double var12;
      Iterator var14;
      TradeOrder.TradeOrderElement var15;
      int var16;
      if (this.getSellPrice() > var8) {
         var10 = (double)var8 / (double)this.getSellPrice();
         var14 = this.orders.iterator();

         while(var14.hasNext()) {
            if (!(var15 = (TradeOrder.TradeOrderElement)var14.next()).isBuyOrder()) {
               var12 = (double)((var16 = var5.getTradePricesCached(var1, this.cacheId).getCachedBuyPrice(this.cacheId, var15.type).getPrice()) * var15.amount) * var10;
               var15.amount = (int)var12 / var16;
            }
         }
      }

      this.recalc();
      if (this.getTotalPrice() > var6) {
         var10 = (double)var6 / (double)this.getTotalPrice();
         var14 = this.orders.iterator();

         while(var14.hasNext()) {
            if ((var15 = (TradeOrder.TradeOrderElement)var14.next()).isBuyOrder()) {
               var12 = (double)((var16 = var5.getTradePricesCached(var1, this.cacheId).getCachedSellPrice(this.cacheId, var15.type).getPrice()) * var15.amount) * var10;
               var15.amount = (int)var12 / var16;
            }
         }
      }

      this.recalc();
   }

   public void recalc() {
      boolean var1 = this.isEmpty();
      TradeNodeStub var4;
      TradeNodeStub var5;
      if (this.state instanceof GameClientState) {
         var4 = (TradeNodeStub)((GameClientState)this.state).getController().getClientChannel().getGalaxyManagerClient().getTradeNodeDataById().get(this.fromDbId);
         var5 = (TradeNodeStub)((GameClientState)this.state).getController().getClientChannel().getGalaxyManagerClient().getTradeNodeDataById().get(this.toDbId);
      } else {
         var4 = (TradeNodeStub)((GameServerState)this.state).getUniverse().getGalaxyManager().getTradeNodeDataById().get(this.fromDbId);
         var5 = (TradeNodeStub)((GameServerState)this.state).getUniverse().getGalaxyManager().getTradeNodeDataById().get(this.toDbId);
      }

      Short2ObjectOpenHashMap var2;
      Short2ObjectOpenHashMap var3;
      if (var5 != null) {
         if (var5 instanceof TradeNodeClient) {
            List var6;
            var2 = TradeNodeStub.toMap(var6 = ((TradeNodeClient)var5).getTradePricesClient(), true);
            var3 = TradeNodeStub.toMap(var6, false);
            if (var6.isEmpty()) {
               this.dirty = true;
            }
         } else {
            TradePrices var10;
            (var10 = var5.getTradePricesCached((GameServerState)this.state, this.cacheId)).getCachedPrices(this.cacheId);
            var2 = var10.cachedBuyPrices;
            var3 = var10.cachedSellPrices;
         }
      } else {
         assert this.state != null;

         DataInputStream var11;
         if ((var11 = ((GameServerState)this.state).getDatabaseIndex().getTableManager().getTradeNodeTable().getTradePricesAsStream(this.toDbId)) == null) {
            this.log(" Exception. Trade Prices of DB ID OF TRADE NOT FOUND: " + this.toDbId, LogInterface.LogLevel.ERROR);
            return;
         }

         try {
            TradePrices var7 = ShoppingAddOn.deserializeTradePrices(var11, true);
            var11.close();
            List var9;

            assert !(var9 = var7.getPrices()).isEmpty() : this.toDbId;

            var2 = TradeNodeStub.toMap(var9, true);
            var3 = TradeNodeStub.toMap(var9, false);
         } catch (IOException var8) {
            var8.printStackTrace();
            return;
         }
      }

      this.calcVolume(var2, var3);
      this.calcPrices(var2, var3);
      if (var4 != null) {
         this.calcStock(var2, var3, var4, var5);
      } else {
         TradeNodeStub var12 = (TradeNodeStub)((GameServerState)this.state).getUniverse().getGalaxyManager().getTradeNodeDataById().get(this.fromDbId);
         TradeNodeStub var13 = (TradeNodeStub)((GameServerState)this.state).getUniverse().getGalaxyManager().getTradeNodeDataById().get(this.toDbId);
         if (var12 != null && var13 != null) {
            this.calcStock(var2, var3, var12, var13);
         }
      }

      assert var1 == this.isEmpty() : var1 + " -> " + this.isEmpty();

   }

   private void calcStock(Short2ObjectOpenHashMap var1, Short2ObjectOpenHashMap var2, TradeNodeStub var3, TradeNodeStub var4) {
      for(int var5 = 0; var5 < this.orders.size(); ++var5) {
         TradeOrder.TradeOrderElement var6;
         TradePriceInterface var7;
         if ((var6 = (TradeOrder.TradeOrderElement)this.orders.get(var5)).isBuyOrder()) {
            var7 = (TradePriceInterface)var2.get(var6.type);
            var6.over = var4.getMax(var6.buyOrder, var7) < var6.amount;
         } else {
            var7 = (TradePriceInterface)var1.get(var6.type);
            var6.over = var3.getMax(var6.buyOrder, var7) < var6.amount;
         }
      }

   }

   private void calcPrices(Short2ObjectOpenHashMap var1, Short2ObjectOpenHashMap var2) {
      this.usedPriceBuy = 0L;
      this.usedPriceSell = 0L;

      for(int var3 = 0; var3 < this.orders.size(); ++var3) {
         TradeOrder.TradeOrderElement var4;
         TradePriceInterface var5;
         if ((var4 = (TradeOrder.TradeOrderElement)this.orders.get(var3)).isBuyOrder()) {
            if ((var5 = (TradePriceInterface)var2.get(var4.type)) != null) {
               assert var5.getPrice() >= 0;

               this.usedPriceBuy += (long)var5.getPrice() * (long)var4.amount;

               assert this.usedPriceBuy >= 0L;
            } else {
               this.log("REMOVE NONEXISTENT BUY ORDER " + this.orders.get(var3) + "; \nBUYMAP: " + var1 + "\nSELLMAP: " + var2, LogInterface.LogLevel.DEBUG);
               this.orders.remove(var3);
               --var3;
               this.setChanged();
               this.notifyObservers();
            }
         } else if ((var5 = (TradePriceInterface)var1.get(var4.type)) != null) {
            assert var5.getPrice() >= 0;

            this.usedPriceSell += (long)var5.getPrice() * (long)var4.amount;

            assert this.usedPriceSell >= 0L;
         } else {
            this.log("REMOVE NONEXISTENT SELL ORDER " + this.orders.get(var3) + "; \nBUYMAP: " + var1 + "\nSELLMAP: " + var2, LogInterface.LogLevel.DEBUG);
            this.orders.remove(var3);
            --var3;
            this.setChanged();
            this.notifyObservers();
         }
      }

   }

   public void addOrChangeBuy(short var1, int var2, boolean var3) {
      TradeOrder.TradeOrderElement var4 = new TradeOrder.TradeOrderElement(var1, var2, true);
      this.addOrChangeOrder(var4, var3);
   }

   public void addOrChangeSell(short var1, int var2, boolean var3) {
      TradeOrder.TradeOrderElement var4 = new TradeOrder.TradeOrderElement(var1, var2, false);
      this.addOrChangeOrder(var4, var3);
   }

   public void addOrChangeOrder(TradeOrder.TradeOrderElement var1, boolean var2) {
      int var3;
      if ((var3 = this.orders.indexOf(var1)) >= 0) {
         if (var1.amount == 0) {
            this.orders.remove(var3);
         } else {
            ((TradeOrder.TradeOrderElement)this.orders.get(var3)).amount = var1.amount;
         }
      } else {
         this.orders.add(var1);

         assert !this.isEmpty() : this.orders;
      }

      this.setChanged();
      this.notifyObservers();
      if (var2) {
         this.recalc();
      }

   }

   public boolean isEmpty() {
      return this.orders.isEmpty();
   }

   public List getElements() {
      return this.orders;
   }

   public double getBuyVolume() {
      return this.buyVolume;
   }

   public double getSellVolume() {
      return this.sellVolume;
   }

   public long getBuyPrice() {
      return this.usedPriceBuy;
   }

   public long getSellPrice() {
      return this.usedPriceSell;
   }

   public Vector3i getFromSystem() {
      return this.fromSystem;
   }

   public void setFromSystem(Vector3i var1) {
      this.fromSystem = var1;
   }

   public Vector3i getToSystem() {
      return this.toSystem;
   }

   public void setToSystem(Vector3i var1) {
      this.toSystem = var1;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeLong(this.fromDbId);
      var1.writeLong(this.toDbId);
      var1.writeDouble(this.buyVolume);
      var1.writeDouble(this.sellVolume);
      var1.writeLong(this.usedPriceBuy);
      var1.writeLong(this.usedPriceSell);
      this.fromSystem.serialize(var1);
      this.toSystem.serialize(var1);
      var1.writeShort(this.orders.size());
      Iterator var3 = this.orders.iterator();

      while(var3.hasNext()) {
         ((TradeOrder.TradeOrderElement)var3.next()).serialize(var1, var2);
      }

      this.config.serialize(var1, var2);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.fromDbId = var1.readLong();
      this.toDbId = var1.readLong();
      this.buyVolume = var1.readDouble();
      this.sellVolume = var1.readDouble();
      this.usedPriceBuy = var1.readLong();
      this.usedPriceSell = var1.readLong();
      this.fromSystem = Vector3i.deserializeStatic(var1);
      this.toSystem = Vector3i.deserializeStatic(var1);
      short var4 = var1.readShort();

      for(int var5 = 0; var5 < var4; ++var5) {
         TradeOrder.TradeOrderElement var6;
         (var6 = new TradeOrder.TradeOrderElement()).deserialize(var1, var2, var3);
         this.orders.add(var6);
      }

      assert this.fromDbId > 0L;

      assert this.toDbId > 0L;

      this.config = new GenericTradeOrderConfig();
      this.config.deserialize(var1, var2, var3);
   }

   public boolean checkCanSellWithInventory(Short2ObjectOpenHashMap var1, TradeNodeStub var2) {
      Iterator var3 = this.orders.iterator();

      while(var3.hasNext()) {
         TradeOrder.TradeOrderElement var4;
         if (!(var4 = (TradeOrder.TradeOrderElement)var3.next()).buyOrder) {
            TradePriceInterface var5 = (TradePriceInterface)var1.get(var4.type);
            int var6 = 0;
            if (var5 != null) {
               var6 = var5.getAmount();
            }

            if (var2 instanceof TradeNode) {
               var6 = ((TradeNode)var2).shop.getShopInventory().getOverallQuantity(var4.type);
            } else {
               ShopInterface var7;
               if (var2 instanceof TradeNodeClient && (var7 = ((TradeNodeClient)var2).getLoadedShop()) != null) {
                  var6 = var7.getShopInventory().getOverallQuantity(var4.type);
               }
            }

            if (var6 <= 0) {
               this.log("ERROR: check HAVE TO SELL block (DENIED AMOUNT == 0): " + ElementKeyMap.toString(var4.type) + "; Stock: " + var6 + "; Wanted: " + var4.amount, LogInterface.LogLevel.ERROR);
               return false;
            }

            if (var6 < var4.amount) {
               this.log("ERROR: check HAVE TO SELL block (DENIED AMOUNT < WANTED): " + ElementKeyMap.toString(var4.type) + "; Stock: " + var6 + "; Wanted: " + var4.amount, LogInterface.LogLevel.ERROR);
               return false;
            }
         }
      }

      return true;
   }

   public boolean checkCanSellToOtherWithInventory(Short2ObjectOpenHashMap var1) {
      Iterator var2 = this.orders.iterator();

      while(var2.hasNext()) {
         TradeOrder.TradeOrderElement var3;
         if (!(var3 = (TradeOrder.TradeOrderElement)var2.next()).buyOrder) {
            TradePriceInterface var4;
            if ((var4 = (TradePriceInterface)var1.get(var3.type)) == null) {
               this.log("ERROR: check SELL block (NO PRICE SET): " + ElementKeyMap.toString(var3.type) + "\n:" + var1, LogInterface.LogLevel.ERROR);
               return false;
            }

            if (var4.getLimit() < 0) {
               return true;
            }

            if (var4.getAmount() + var3.amount > var4.getLimit()) {
               this.log("ERROR: check SELL block (OVER LIMIT): " + ElementKeyMap.toString(var3.type) + "; Limit: " + var4.getLimit() + "; Stock: " + var4.getAmount() + "; Wanted: " + var3.amount, LogInterface.LogLevel.ERROR);
               return false;
            }
         }
      }

      return true;
   }

   public boolean checkCanFromOtherBuyWithInventory(Short2ObjectOpenHashMap var1) {
      Iterator var2 = this.orders.iterator();

      while(var2.hasNext()) {
         TradeOrder.TradeOrderElement var3;
         if ((var3 = (TradeOrder.TradeOrderElement)var2.next()).buyOrder) {
            TradePriceInterface var4;
            if ((var4 = (TradePriceInterface)var1.get(var3.type)) == null) {
               this.log("ERROR: check block: " + ElementKeyMap.toString(var3.type) + "; No Price found for wanted block", LogInterface.LogLevel.ERROR);
               return false;
            }

            if (var4.getLimit() >= 0 && var4.getAmount() <= var4.getLimit()) {
               this.log("ERROR: check block: " + ElementKeyMap.toString(var3.type) + "; Amount over limit; Limit: " + var4.getLimit() + "; Stock: " + var4.getAmount() + "; Wanted: " + var3.amount, LogInterface.LogLevel.ERROR);
               return false;
            }

            int var5;
            if ((var5 = var4.getAmount() - Math.max(0, var4.getLimit())) < var3.amount) {
               this.log("ERROR: check block: " + ElementKeyMap.toString(var3.type) + "; Available: " + var5 + "; Wanted: " + var3.amount, LogInterface.LogLevel.ERROR);
               return false;
            }
         }
      }

      return true;
   }

   public List getOrders() {
      return this.orders;
   }

   public void sendMsgError(Object[] var1) {
      if (this.clientChannelReceivedOn != null) {
         this.clientChannelReceivedOn.getPlayer().sendServerMessagePlayerError(var1);
      } else {
         if (this.state != null && this.state instanceof GameClientState) {
            ((GameClientState)this.state).getController().popupAlertTextMessage(StringTools.getFormatedMessage(var1), 0.0F);
         }

      }
   }

   public void sendMsgInfo(Object[] var1) {
      if (this.clientChannelReceivedOn != null) {
         this.clientChannelReceivedOn.getPlayer().sendServerMessagePlayerInfo(var1);
      } else {
         if (this.state != null && this.state instanceof GameClientState) {
            ((GameClientState)this.state).getController().popupInfoTextMessage(StringTools.getFormatedMessage(var1), 0.0F);
         }

      }
   }

   public String print(GameServerState var1) {
      GalaxyManager var4;
      TradeNodeStub var2 = (TradeNodeStub)(var4 = var1.getUniverse().getGalaxyManager()).getTradeNodeDataById().get(this.fromDbId);
      TradeNodeStub var5 = (TradeNodeStub)var4.getTradeNodeDataById().get(this.toDbId);
      StringBuffer var3;
      (var3 = new StringBuffer()).append("---- PRINTING TRADE ORDER START ----\n");
      var3.append("FROM: " + var2.getStationName() + " sys " + var2.getSystem() + " sec " + var2.getSector() + "; Capacity: " + StringTools.formatPointZero(var2.getVolume()) + " / " + StringTools.formatPointZero(var2.getCapacity()) + "; Credits: " + StringTools.formatSmallAndBig(var2.getCredits()) + "; \n");
      var3.append("TO  : " + var5.getStationName() + " sys " + var5.getSystem() + " sec " + var5.getSector() + "; Capacity: " + StringTools.formatPointZero(var5.getVolume()) + " / " + StringTools.formatPointZero(var5.getCapacity()) + "; Credits: " + StringTools.formatSmallAndBig(var5.getCredits()) + "; \n");
      var3.append("-----\n");
      var3.append("BUY   VOLUME : " + StringTools.formatPointZero(this.getBuyVolume()) + "\n");
      var3.append("SELL  VOLUME : " + StringTools.formatPointZero(this.getSellVolume()) + "\n");
      var3.append("-----\n");
      var3.append("BUY PRICE    : " + StringTools.formatSmallAndBig(this.getBuyPrice()) + "\n");
      var3.append("SELL PRICE   : " + StringTools.formatSmallAndBig(this.getSellPrice()) + "\n");
      var3.append("---------------------------------\n");
      var3.append("TOTAL PRICE  : " + StringTools.formatSmallAndBig(this.getTotalPrice()) + "\n");
      var3.append("DELIVERY COST: " + StringTools.formatSmallAndBig(this.getDeliveryCost()) + "\n");
      var3.append("USED SHIPS   : " + StringTools.formatSmallAndBig(this.getUsedTradeShips()) + "\n");
      var3.append("---------------------------------\n");
      Iterator var6 = this.orders.iterator();

      TradeOrder.TradeOrderElement var7;
      while(var6.hasNext()) {
         if ((var7 = (TradeOrder.TradeOrderElement)var6.next()).isBuyOrder()) {
            var3.append("  ORDER BUY  " + var7.amount + " x " + ElementKeyMap.toString(var7.getType()) + "\n");
         }
      }

      var6 = this.orders.iterator();

      while(var6.hasNext()) {
         if (!(var7 = (TradeOrder.TradeOrderElement)var6.next()).isBuyOrder()) {
            var3.append("  ORDER SELL " + var7.amount + " x " + ElementKeyMap.toString(var7.getType()) + "\n");
         }
      }

      var3.append("'''' PRINTING TRADE ORDER END ''''\n");
      return var3.toString();
   }

   public boolean hasCache() {
      return this.cacheId != Integer.MIN_VALUE;
   }

   public long assignFleetBuy(TradeNodeStub var1, TradeNodeStub var2, Vector3i var3, Vector3i var4) {
      Faction var5;
      if ((var5 = ((GameServerState)this.state).getFactionManager().getFaction(var1.getFactionId())) != null && var5.isNPC()) {
         NPCFaction var6 = (NPCFaction)var5;
         ElementCountMap var7 = this.getItems(true);
         return var6.getFleetManager().spawnTradingFleet(var7, var3, var4).dbid;
      } else {
         return -1L;
      }
   }

   private ElementCountMap getItems(boolean var1) {
      ElementCountMap var4 = new ElementCountMap();
      Iterator var2 = this.orders.iterator();

      while(var2.hasNext()) {
         TradeOrder.TradeOrderElement var3;
         (var3 = (TradeOrder.TradeOrderElement)var2.next()).isBuyOrder();
         var4.inc(var3.type, var3.amount);
      }

      return var4;
   }

   public long assignFleetSell(TradeNodeStub var1, TradeNodeStub var2, Vector3i var3, Vector3i var4) {
      Faction var5;
      if ((var5 = ((GameServerState)this.state).getFactionManager().getFaction(var2.getFactionId())) != null && var5.isNPC()) {
         NPCFaction var6 = (NPCFaction)var5;
         ElementCountMap var7 = this.getItems(true);
         return var6.getFleetManager().spawnTradingFleet(var7, var3, var4).dbid;
      } else {
         return -1L;
      }
   }

   public short checkActiveRoutesCanSellAllTypesTo(long var1, TradeActiveMap var3) {
      Iterator var7 = var3.getTradeList().iterator();

      while(true) {
         TradeActive var4;
         do {
            if (!var7.hasNext()) {
               return 0;
            }
         } while((var4 = (TradeActive)var7.next()).getToId() != var1);

         Iterator var5 = this.orders.iterator();

         while(var5.hasNext()) {
            TradeOrder.TradeOrderElement var6;
            if (!(var6 = (TradeOrder.TradeOrderElement)var5.next()).isBuyOrder() && var4.getBlocks().get(var6.getType()) > 0) {
               return var6.getType();
            }
         }
      }
   }

   public static TradeActive checkActiveRoutesCanSellTypeTo(long var0, short var2, TradeActiveMap var3) {
      Iterator var5 = var3.getTradeList().iterator();

      TradeActive var4;
      do {
         if (!var5.hasNext()) {
            return null;
         }
      } while((var4 = (TradeActive)var5.next()).getToId() != var0 || var4.getBlocks().get(var2) <= 0);

      return var4;
   }

   public class TradeOrderElement implements SerialializationInterface {
      public short type;
      public int amount;
      private boolean buyOrder;
      public boolean over;

      public boolean isBuyOrder() {
         return this.buyOrder;
      }

      public int hashCode() {
         return 31 + this.type * 123 * (this.isBuyOrder() ? -234123 : 0);
      }

      public boolean equals(Object var1) {
         TradeOrder.TradeOrderElement var2 = (TradeOrder.TradeOrderElement)var1;
         return this.type == var2.type && this.isBuyOrder() == var2.isBuyOrder();
      }

      public TradeOrderElement(short var2, int var3, boolean var4) {
         this.type = var2;
         this.amount = var3;
         this.buyOrder = var4;
      }

      public TradeOrderElement() {
      }

      public ElementInformation getInfo() {
         return ElementKeyMap.getInfoFast(this.type);
      }

      public short getType() {
         return this.type;
      }

      public void serialize(DataOutput var1, boolean var2) throws IOException {
         var1.writeBoolean(this.buyOrder);
         var1.writeShort(this.type);
         var1.writeInt(this.amount);
      }

      public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
         this.buyOrder = var1.readBoolean();
         this.type = var1.readShort();
         this.amount = var1.readInt();
      }

      public boolean isOverAmount(TradeNodeClient var1) {
         return this.over;
      }

      public String toString() {
         return "TradeOrderElement [type=" + this.type + ", amount=" + this.amount + ", buy=" + this.buyOrder + ", over=" + this.over + "]";
      }
   }
}
