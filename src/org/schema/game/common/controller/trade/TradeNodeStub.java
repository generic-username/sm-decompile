package org.schema.game.common.controller.trade;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.network.objects.TradePrice;
import org.schema.game.network.objects.TradePriceInterface;
import org.schema.game.network.objects.TradePrices;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.objects.Sendable;

public class TradeNodeStub implements SerialializationInterface {
   private long tradePermission;
   private long entityDBId;
   protected Vector3i system;
   private Set owners;
   private String stationName;
   private int factionId;
   private double volume;
   private double capacity;
   private long credits;
   public boolean remove;
   protected Vector3i sector;
   private TradePrices tradePricesCached;
   private int cacheDate;

   public TradeNodeStub() {
      this.tradePermission = TradeManager.PERM_ALL_BUT_ENEMY;
      this.entityDBId = -1L;
      this.system = new Vector3i();
   }

   public double getVolume() {
      return this.volume;
   }

   public void setVolume(double var1) {
      this.volume = var1;
   }

   public double getCapacity() {
      return this.capacity;
   }

   public void setCapacity(double var1) {
      this.capacity = var1;
   }

   public boolean isPermission(TradeManager.TradePerm var1) {
      return (this.tradePermission & var1.val) == var1.val;
   }

   public TradePrices getTradePricesInstance(GameServerState var1) {
      TradePrices var2 = var1.getDatabaseIndex().getTableManager().getTradeNodeTable().getTradePrices(this.getEntityDBId());

      assert var2.entDbId == this.getEntityDBId();

      return var2;
   }

   public void cacheTradePrices(GameServerState var1) {
      this.tradePricesCached = this.getTradePricesInstance(var1);
   }

   public TradePrices getTradePricesCached(GameServerState var1, int var2) {
      if (this.tradePricesCached == null || var2 != this.cacheDate) {
         this.cacheTradePrices(var1);
         this.tradePricesCached.getCachedPrices(var2);
      }

      return this.tradePricesCached;
   }

   public String getOwnerString() {
      this.owners = this.getOwners();
      if (this.owners.isEmpty()) {
         return "";
      } else {
         StringBuffer var1 = new StringBuffer();
         int var2 = 0;
         int var3 = 0;

         for(Iterator var4 = this.owners.iterator(); var4.hasNext(); ++var2) {
            String var5 = (String)var4.next();
            if (var3 + var5.length() + 1 >= 128) {
               break;
            }

            var1.append(var5);
            if (var2 < this.owners.size() - 1) {
               var1.append(";");
            }

            var3 += var5.length() + 1;
         }

         return var1.toString();
      }
   }

   public void parseOwnerString(String var1) {
      this.owners = new ObjectOpenHashSet();
      String[] var5;
      int var2 = (var5 = var1.split(";")).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         String var4 = var5[var3];
         this.owners.add(var4);
      }

   }

   public long getTradePermission() {
      return this.tradePermission;
   }

   public void setTradePermission(long var1) {
      this.tradePermission = var1;
   }

   public InputStream getPricesInputStream() throws IOException {
      throw new IllegalArgumentException();
   }

   public long getEntityDBId() {
      return this.entityDBId;
   }

   public void setEntityDBId(long var1) {
      this.entityDBId = var1;
   }

   public Vector3i getSystem() {
      return this.system;
   }

   public void setSystem(Vector3i var1) {
      this.system = var1;
   }

   public Set getOwners() {
      return this.owners;
   }

   public void setOwners(Set var1) {
      this.owners = var1;
   }

   public String getStationName() {
      return this.stationName;
   }

   public void setStationName(String var1) {
      this.stationName = var1;
   }

   public int getFactionId() {
      return this.factionId;
   }

   public void setFactionId(int var1) {
      this.factionId = var1;
   }

   public void updateWith(TradeNodeStub var1) {
      this.sector = new Vector3i(var1.getSector());
      this.system = new Vector3i(var1.getSystem());
      this.owners = var1.getOwners();
      this.stationName = var1.stationName;
      this.factionId = var1.factionId;
      this.volume = var1.volume;
      this.capacity = var1.capacity;
      this.credits = var1.credits;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeBoolean(this.remove);
      var1.writeLong(this.entityDBId);

      assert this.entityDBId >= 0L;

      if (!this.remove) {
         this.getSystem().serialize(var1);
         this.sector.serialize(var1);
         var1.writeUTF(this.getOwnerString());
         var1.writeUTF(this.getStationName());
         var1.writeInt(this.getFactionId());
         var1.writeDouble(this.getVolume());
         var1.writeDouble(this.getCapacity());
         var1.writeLong(this.getCredits());
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.remove = var1.readBoolean();
      this.entityDBId = var1.readLong();
      if (!this.remove) {
         this.system = Vector3i.deserializeStatic(var1);
         this.sector = Vector3i.deserializeStatic(var1);
         this.parseOwnerString(var1.readUTF());
         this.stationName = var1.readUTF();
         this.factionId = var1.readInt();
         this.volume = var1.readDouble();
         this.capacity = var1.readDouble();
         this.credits = var1.readLong();
      }

   }

   public void setCredits(long var1) {
      this.credits = var1;
   }

   public long getCredits() {
      return this.credits;
   }

   public int hashCode() {
      return 31 + (int)(this.entityDBId ^ this.entityDBId >>> 32);
   }

   public boolean equals(Object var1) {
      TradeNodeStub var2 = (TradeNodeStub)var1;
      return this.getEntityDBId() == var2.getEntityDBId();
   }

   public int getMaxOwn(boolean var1, TradePriceInterface var2) {
      return this.getMax(!var1, var2);
   }

   public int getMax(boolean var1, TradePriceInterface var2) {
      assert var2 != null;

      assert var2.getInfo() != null;

      if (var1) {
         return var2.getLimit() < 0 ? var2.getAmount() : Math.max(0, var2.getAmount() - var2.getLimit());
      } else {
         double var3 = this.getCapacity() - this.getVolume();
         int var5 = (int)Math.floor(Math.max(0.0D, var3) / (double)var2.getInfo().getVolume());
         return var2.getLimit() < 0 ? Math.max(0, var5) : Math.max(0, Math.min(var5, var2.getLimit() - var2.getAmount()));
      }
   }

   public static Short2ObjectOpenHashMap toMap(List var0, boolean var1) {
      Short2ObjectOpenHashMap var2 = new Short2ObjectOpenHashMap(var0.size());
      Iterator var4 = var0.iterator();

      while(var4.hasNext()) {
         TradePriceInterface var3 = (TradePriceInterface)var4.next();
         if (var1 != var3.isSell()) {
            var2.put(var3.getType(), var3);
         }
      }

      return var2;
   }

   public String toString() {
      return this.getEntityDBId() + " : OWN " + this.getOwnerString() + " : " + this.getStationName() + " : " + this.getSector() + "; " + this.getClass().getSimpleName();
   }

   public void removeBoughtBlocks(TradeOrder var1, List var2, GameServerState var3) {
      Iterator var6 = var1.getOrders().iterator();

      while(var6.hasNext()) {
         TradeOrder.TradeOrderElement var4;
         if ((var4 = (TradeOrder.TradeOrderElement)var6.next()).isBuyOrder()) {
            this.removeBlocks(var4, var2);
         }
      }

      try {
         var3.getDatabaseIndex().getTableManager().getTradeNodeTable().setTradePrices(this.getEntityDBId(), this.getVolume(), this.getCredits(), var2);
      } catch (SQLException var5) {
         var5.printStackTrace();
      }
   }

   public void addBlocks(TradeActive var1, List var2, GameServerState var3) {
      Short2ObjectOpenHashMap var4 = new Short2ObjectOpenHashMap();
      Short2ObjectOpenHashMap var5 = new Short2ObjectOpenHashMap();
      Iterator var6 = var2.iterator();

      while(var6.hasNext()) {
         TradePriceInterface var7;
         if ((var7 = (TradePriceInterface)var6.next()).isBuy()) {
            var4.put(var7.getType(), var7);
         } else {
            var5.put(var7.getType(), var7);
         }
      }

      var6 = ElementKeyMap.keySet.iterator();

      while(var6.hasNext()) {
         short var13 = (Short)var6.next();
         int var8;
         if ((var8 = var1.getBlocks().get(var13)) > 0) {
            TradePriceInterface var9;
            if ((var9 = (TradePriceInterface)var4.get(var13)) != null) {
               var9.setAmount(var9.getAmount() + var8);
            } else {
               var2.add(new TradePrice(var13, var8, -1, -1, false));
            }

            if ((var9 = (TradePriceInterface)var5.get(var13)) != null) {
               var9.setAmount(var9.getAmount() + var8);
            } else {
               var2.add(new TradePrice(var13, var8, -1, -1, true));
            }

            double var10 = (double)(ElementKeyMap.getInfo(var13).getVolume() * (float)var8);
            this.setVolume(this.getVolume() + var10);
         }
      }

      assert this.getEntityDBId() > 0L;

      try {
         var3.getDatabaseIndex().getTableManager().getTradeNodeTable().setTradePrices(this.getEntityDBId(), this.getVolume(), this.getCredits(), var2);
      } catch (SQLException var12) {
         var12.printStackTrace();
      }
   }

   public void removeBlocks(TradeOrder.TradeOrderElement var1, List var2) {
      boolean var3 = false;
      Iterator var7 = var2.iterator();

      while(var7.hasNext()) {
         TradePriceInterface var4;
         if ((var4 = (TradePriceInterface)var7.next()).getType() == var1.type) {
            var3 = true;
            double var5 = (double)(ElementKeyMap.getInfo(var1.type).getVolume() * (float)var1.amount);
            var4.setAmount(var4.getAmount() - var1.amount);
            this.setVolume(this.getVolume() - var5);
         }
      }

      assert var3;

   }

   public void removeSoldBlocks(TradeOrder var1, List var2, GameServerState var3) {
      Iterator var6 = var1.getOrders().iterator();

      while(var6.hasNext()) {
         TradeOrder.TradeOrderElement var4;
         if (!(var4 = (TradeOrder.TradeOrderElement)var6.next()).isBuyOrder()) {
            this.removeBlocks(var4, var2);
         }
      }

      try {
         var3.getDatabaseIndex().getTableManager().getTradeNodeTable().setTradePrices(this.getEntityDBId(), this.getVolume(), this.getCredits(), var2);
      } catch (SQLException var5) {
         var5.printStackTrace();
      }
   }

   public void modCredits(long var1, GameServerState var3) {
      this.credits += var1;

      try {
         var3.getDatabaseIndex().getTableManager().getTradeNodeTable().setTradeCredits(this.getEntityDBId(), this.getCredits());
      } catch (SQLException var4) {
         var4.printStackTrace();
      }
   }

   public void setSector(Vector3i var1) {
      this.sector = var1;
      VoidSystem.getContainingSystem(this.sector, this.system);
   }

   public Vector3i getSector() {
      return this.sector;
   }

   public static void modifyCreditsServer(GameServerState var0, long var1, long var3) throws SQLException {
      synchronized(var0) {
         boolean var6;
         if (!(var6 = var0.isSynched())) {
            var0.setSynched();
         }

         Sendable var7 = (Sendable)var0.getLocalAndRemoteObjectContainer().getDbObjects().get(var1);

         try {
            if (var7 != null && var7 instanceof ShopInterface) {
               long var8 = var3 - ((ShopInterface)var7).getShoppingAddOn().getCredits();
               ((ShopInterface)var7).getShoppingAddOn().modCredits(var8);
               return;
            }
         } finally {
            if (!var6) {
               var0.setUnsynched();
            }

         }
      }

      var0.getDatabaseIndex().getTableManager().getTradeNodeTable().setTradeCredits(var1, var3);
   }

   public static void setPrices(GameServerState var0, TradeNodeStub var1, TradePrices var2) throws SQLException, IOException {
      long var3 = var1.getEntityDBId();
      synchronized(var0) {
         boolean var6;
         if (!(var6 = var0.isSynched())) {
            var0.setSynched();
         }

         Sendable var13 = (Sendable)var0.getLocalAndRemoteObjectContainer().getDbObjects().get(var3);

         try {
            if (var13 != null && var13 instanceof ShopInterface) {
               ShopInterface var11 = (ShopInterface)var13;
               List var12 = var2.getPrices();
               var11.getShoppingAddOn().putAllPrices(var12);
               var11.getShoppingAddOn().sendPrices();
               return;
            }
         } finally {
            if (!var6) {
               var0.setUnsynched();
            }

         }
      }

      var0.getDatabaseIndex().getTableManager().getTradeNodeTable().setTradePrices(var1.getEntityDBId(), var1.getCredits(), var1.getVolume(), var1.getCapacity(), var2);
   }
}
