package org.schema.game.common.controller.trade.manualtrade;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.data.MetaObjectState;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.server.ServerStateInterface;

public class ManualTradeItem implements SerialializationInterface {
   public byte side;
   public short type;
   public int amount;
   public int metaId;
   public short itemId;
   public int tradeId;
   public boolean acc;
   public boolean join;
   public ManualTrade.Mode mode;
   StateInterface state;
   private boolean requested;

   public ManualTradeItem() {
      this.mode = ManualTrade.Mode.ITEM;
   }

   public ManualTradeItem(StateInterface var1, int var2, byte var3) {
      this.mode = ManualTrade.Mode.ITEM;
      this.state = var1;
      this.tradeId = var2;
      this.side = var3;
   }

   public ManualTradeItem(StateInterface var1, int var2, byte var3, short var4, int var5, int var6, short var7) {
      this.mode = ManualTrade.Mode.ITEM;
      this.state = var1;
      this.tradeId = var2;
      this.side = var3;
      this.type = var4;
      this.amount = var5;
      this.metaId = var6;
      this.itemId = var7;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeInt(this.tradeId);
      var1.writeByte(this.side);
      var1.writeByte(this.mode.ordinal());
      if (this.mode == ManualTrade.Mode.ITEM) {
         var1.writeShort(this.itemId);
         var1.writeShort(this.type);
         var1.writeInt(this.amount);
         var1.writeInt(this.metaId);
      } else if (this.mode == ManualTrade.Mode.ACCEPT) {
         var1.writeBoolean(this.acc);
      } else if (this.mode == ManualTrade.Mode.JOIN) {
         var1.writeBoolean(this.join);
      }

      if (this.mode != ManualTrade.Mode.CANCEL) {
         throw new IllegalArgumentException(this.mode.toString());
      }
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.tradeId = var1.readInt();
      this.side = var1.readByte();
      this.mode = ManualTrade.Mode.values()[var1.readByte()];
      if (this.mode == ManualTrade.Mode.ITEM) {
         this.itemId = var1.readShort();
         this.type = var1.readShort();
         this.amount = var1.readInt();
         this.metaId = var1.readInt();
      } else if (this.mode == ManualTrade.Mode.ACCEPT) {
         this.acc = var1.readBoolean();
      } else if (this.mode == ManualTrade.Mode.JOIN) {
         this.join = var1.readBoolean();
      } else if (this.mode != ManualTrade.Mode.CANCEL) {
         throw new IllegalArgumentException(this.mode.toString());
      }
   }

   public String toString() {
      if (this.type > 0) {
         return ElementKeyMap.getInfo(this.type).getName();
      } else if (this.type < 0) {
         MetaObjectManager var1 = ((MetaObjectState)this.state).getMetaObjectManager();
         if (!this.isOnServer() && !this.requested) {
            var1.checkAvailable(this.metaId, (MetaObjectState)this.state);
            this.requested = true;
         }

         MetaObject var2;
         return (var2 = var1.getObject(this.metaId)) == null ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_MANUALTRADE_MANUALTRADEITEM_0 : var2.getName();
      } else {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_MANUALTRADE_MANUALTRADEITEM_1;
      }
   }

   private boolean isOnServer() {
      return this.state instanceof ServerStateInterface;
   }

   public boolean equals(Object var1) {
      ManualTradeItem var2 = (ManualTradeItem)var1;
      return this.itemId == var2.itemId;
   }

   public int hashCode() {
      return this.itemId;
   }
}
