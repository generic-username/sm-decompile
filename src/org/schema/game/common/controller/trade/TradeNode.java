package org.schema.game.common.controller.trade;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.network.objects.TradePrices;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.objects.Sendable;

public class TradeNode extends TradeNodeStub {
   public ShopInterface shop;

   public InputStream getPricesInputStream() throws IOException {
      FastByteArrayOutputStream var1 = new FastByteArrayOutputStream(10240);
      this.shop.getShoppingAddOn().serializeTradePrices(new DataOutputStream(var1));
      return new FastByteArrayInputStream(var1.array, 0, (int)var1.length());
   }

   public TradePrices getTradePricesInstance(GameServerState var1) {
      return this.shop.getShoppingAddOn().getTradePrices();
   }

   public void setFromShop(ShopInterface var1) {
      this.shop = var1;
   }

   public void modCredits(long var1, GameServerState var3) {
      super.modCredits(var1, var3);
      this.shop.modCredits(var1);
   }

   public long getEntityDBId() {
      return this.shop.getSegmentController().dbId;
   }

   public Vector3i getSystem() {
      Sendable var1;
      if ((var1 = (Sendable)this.shop.getSegmentController().getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(this.shop.getSegmentController().getSectorId())) != null && var1 instanceof RemoteSector) {
         RemoteSector var2 = (RemoteSector)var1;
         if (this.shop.getSegmentController().isOnServer()) {
            this.system = VoidSystem.getContainingSystem(var2.getServerSector().pos, new Vector3i());
            return this.system;
         } else {
            this.system = VoidSystem.getContainingSystem(var2.clientPos(), new Vector3i());
            return this.system;
         }
      } else {
         return null;
      }
   }

   public Vector3i getSector() {
      Sendable var1;
      if ((var1 = (Sendable)this.shop.getSegmentController().getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(this.shop.getSegmentController().getSectorId())) != null && var1 instanceof RemoteSector) {
         RemoteSector var2 = (RemoteSector)var1;
         if (this.shop.getSegmentController().isOnServer()) {
            this.sector = new Vector3i(var2.getServerSector().pos);
            return this.sector;
         } else {
            this.sector = new Vector3i(var2.clientPos());
            return this.sector;
         }
      } else {
         return null;
      }
   }

   public Set getOwners() {
      this.setOwners(this.shop.getShoppingAddOn().getOwnerPlayers());
      return super.getOwners();
   }

   public String getStationName() {
      this.setStationName(this.shop.getSegmentController().getRealName());
      return super.getStationName();
   }

   public int getFactionId() {
      this.setFactionId(this.shop.getSegmentController().getFactionId());
      return super.getFactionId();
   }

   public long getTradePermission() {
      this.setTradePermission(this.shop.getPermissionToTrade());
      return super.getTradePermission();
   }

   public double getVolume() {
      this.setVolume(this.shop.getShopInventory().getVolume());
      return super.getVolume();
   }

   public double getCapacity() {
      this.setCapacity(this.shop.getShopInventory().getCapacity());
      return super.getCapacity();
   }

   public long getCredits() {
      super.setCredits(this.shop.getCredits());
      return super.getCredits();
   }

   public void addBlocks(TradeActive var1, List var2, GameServerState var3) {
      IntOpenHashSet var6 = new IntOpenHashSet();
      Iterator var7 = ElementKeyMap.keySet.iterator();

      while(var7.hasNext()) {
         short var4 = (Short)var7.next();
         int var5;
         if ((var5 = var1.getBlocks().get(var4)) > 0) {
            var6.add(this.shop.getShopInventory().incExistingOrNextFreeSlotWithoutException(var4, var5));
         }
      }

      this.shop.getShopInventory().sendInventoryModification(var6);
   }

   public void removeBoughtBlocks(TradeOrder var1, List var2, GameServerState var3) {
      Iterator var4 = var1.getOrders().iterator();

      while(var4.hasNext()) {
         TradeOrder.TradeOrderElement var5;
         if ((var5 = (TradeOrder.TradeOrderElement)var4.next()).isBuyOrder()) {
            this.removeBlocks(var5, var2);
         }
      }

   }

   public void removeBlocksDirect(TradeOrder.TradeOrderElement var1) {
      IntOpenHashSet var2 = new IntOpenHashSet();
      this.shop.getShopInventory().decreaseBatch(var1.type, var1.amount, var2);
      this.shop.getShopInventory().sendInventoryModification(var2);
   }

   public void removeSoldBlocks(TradeOrder var1, List var2, GameServerState var3) {
      Iterator var4 = var1.getOrders().iterator();

      while(var4.hasNext()) {
         TradeOrder.TradeOrderElement var5;
         if (!(var5 = (TradeOrder.TradeOrderElement)var4.next()).isBuyOrder()) {
            this.removeBlocksDirect(var5);
         }
      }

   }
}
