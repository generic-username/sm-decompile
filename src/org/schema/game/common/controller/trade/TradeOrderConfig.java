package org.schema.game.common.controller.trade;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.schine.network.SerialializationInterface;

public abstract class TradeOrderConfig implements SerialializationInterface {
   public abstract double getCargoPerShip();

   public abstract long getCostPerSystem();

   public abstract long getCostPerCargoShip();

   public abstract double getTimePerSectorInSecs();

   public abstract double getProfitOfValue();

   public abstract double getProfitOfValuePerSystem();

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeDouble(this.getCargoPerShip());
      var1.writeLong(this.getCostPerSystem());
      var1.writeLong(this.getCostPerCargoShip());
      var1.writeDouble(this.getTimePerSectorInSecs());
      var1.writeDouble(this.getProfitOfValue());
      var1.writeDouble(this.getProfitOfValuePerSystem());
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      throw new IllegalArgumentException("Receiving part is only implemented by generic Config class");
   }
}
