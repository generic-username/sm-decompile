package org.schema.game.common.controller.trade;

import org.schema.game.common.controller.elements.shop.ShopElementManager;

public class TradingGuildTradeOrderConfig extends TradeOrderConfig {
   public double getCargoPerShip() {
      return ShopElementManager.TRADING_GUILD_CARGO_PER_SHIP;
   }

   public long getCostPerSystem() {
      return ShopElementManager.TRADING_GUILD_COST_PER_SYSTEM;
   }

   public long getCostPerCargoShip() {
      return ShopElementManager.TRADING_GUILD_COST_PER_CARGO_SHIP;
   }

   public double getTimePerSectorInSecs() {
      return ShopElementManager.TRADING_GUILD_TIME_PER_SECTOR_SEC;
   }

   public double getProfitOfValue() {
      return ShopElementManager.TRADING_GUILD_PROFIT_OF_VALUE;
   }

   public double getProfitOfValuePerSystem() {
      return ShopElementManager.TRADING_GUILD_PROFIT_OF_VALUE_PER_SYSTEM;
   }
}
