package org.schema.game.common.controller.generator;

import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import obfuscated.A;
import obfuscated.I;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.PlanetIco;
import org.schema.game.common.data.Icosahedron;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentDataBitMap;
import org.schema.game.common.data.world.SegmentDataIntArray;
import org.schema.game.common.data.world.SegmentDataSingle;
import org.schema.game.common.data.world.SegmentDataSingleSideEdge;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.controller.GenerationElementMap;
import org.schema.game.server.controller.GeneratorGridLock;
import org.schema.game.server.controller.RequestData;
import org.schema.game.server.controller.RequestDataIcoPlanet;
import org.schema.game.server.controller.RequestDataStructureGen;
import org.schema.game.server.controller.TerrainChunkCacheElement;
import org.schema.game.server.data.ServerConfig;

public class PlanetIcoCreatorThread extends CreatorThread {
   public static final ObjectArrayFIFOQueue dataPool;
   private I terrainGenerator;
   private static final int CENTER_INDEX;

   public RequestData allocateRequestData(int var1, int var2, int var3) {
      synchronized(dataPool) {
         while(dataPool.isEmpty()) {
            try {
               dataPool.wait();
            } catch (InterruptedException var4) {
               var4.printStackTrace();
            }
         }

         return (RequestData)dataPool.dequeue();
      }
   }

   public void freeRequestData(RequestData var1, int var2, int var3, int var4) {
      assert var1 != null;

      synchronized(dataPool) {
         dataPool.enqueue(var1);
         dataPool.notify();
      }
   }

   public PlanetIcoCreatorThread(PlanetIco var1, I var2) {
      super(var1);
      this.terrainGenerator = var2;
   }

   public int isConcurrent() {
      return 2;
   }

   public int loadFromDatabase(Segment var1) {
      return 0;
   }

   public void onNoExistingSegmentFound(Segment var1, RequestData var2) {
      TerrainChunkCacheElement var3;
      Object var4 = (var3 = ((RequestDataIcoPlanet)var2).rawChunks[CENTER_INDEX]).preData;
      boolean var5 = true;
      if (var3.preData == null || var3.preData.getSegmentData() == null) {
         assert var1.getSegmentData() instanceof SegmentDataIntArray : var1.getSegmentData().getClass().getSimpleName();

         var4 = var1;
         var5 = false;
      }

      assert ((Segment)var4).getSegmentData() instanceof SegmentDataIntArray : ((Segment)var4).getSegmentData() + "; ACT_PRE " + var5;

      ((RequestDataIcoPlanet)var2).currentChunkCache = var3;
      I var10000 = this.terrainGenerator;
      I.a((Segment)var4, (RequestDataStructureGen)((RequestDataIcoPlanet)var2));
      ((RemoteSegment)var1).setLastChanged(System.currentTimeMillis());
      if (!var3.isEmpty()) {
         GenerationElementMap var9 = ((RequestDataIcoPlanet)var2).currentChunkCache.generationElementMap;
         Object var6 = null;
         if (((Segment)var4).getSegmentData().isBlockAddedForced()) {
            if (var3.isFullyFilledWithOneType()) {
               int var10 = var9.getBlockDataFromList(0);
               if (!var3.allInSide) {
                  var6 = new SegmentDataSingleSideEdge(false, var10);
               } else {
                  var6 = new SegmentDataSingle(false, var10);
               }
            } else {
               var3.generationElementMap.addBlock(0);
               if (var9.containsBlockIndexList.size() <= 16) {
                  int[] var11 = new int[var9.containsBlockIndexList.size()];

                  for(int var7 = 0; var7 < var11.length; ++var7) {
                     var11[var7] = var9.getBlockDataFromList(var7);
                  }

                  var6 = new SegmentDataBitMap(false, var11, ((Segment)var4).getSegmentData());
               }
            }
         }

         if (var6 != null) {
            ((SegmentData)var6).setSize(var1.getSize());
            var1.getSegmentData().setBlockAddedForced(false);
            var1.getSegmentController().getSegmentProvider().getSegmentDataManager().addToFreeSegmentData(var1.getSegmentData(), true, true);
            ((SegmentData)var6).setBlockAddedForced(true);
            ((SegmentData)var6).assignData(var1);
         } else if (var5) {
            try {
               System.arraycopy(var3.preData.getSegmentData().getAsIntBuffer(), 0, var1.getSegmentData().getAsIntBuffer(), 0, var1.getSegmentData().getAsIntBuffer().length);
            } catch (SegmentDataWriteException var8) {
               var8.printStackTrace();
            }

            var1.getSegmentData().setBlockAddedForced(var3.preData.getSegmentData().isBlockAddedForced());
            var1.setSize(var3.preData.getSize());
            var1.getSegmentData().setSize(var3.preData.getSegmentData().getSize());
            var1.getSegmentData().setNeedsRevalidate(true);
            var3.preData.getSegmentData().setBlockAddedForced(false);
            var1.getSegmentController().getSegmentProvider().getSegmentDataManager().addToFreeSegmentData(var3.preData.getSegmentData(), true, true);
         }

         if (var5) {
            var3.preData.setSegmentData((SegmentData)null);
            var3.preData = null;
         }

      }
   }

   public void doFirstStage(RemoteSegment var1, TerrainChunkCacheElement var2, RequestDataIcoPlanet var3) {
      Vector3i var4 = new Vector3i();
      Icosahedron.segmentLowPoint(var1.pos, var4);
      if (this.terrainGenerator.a(var4.x, var4.y, var4.z)) {
         assert var2.isEmpty();

         var3.currentChunkCache.setStructureList((A)null);
      } else {
         Icosahedron.segmentHighPoint(var1.pos, var4);
         var3.currentChunkCache = var2;

         try {
            int var6;
            A var7;
            if ((var6 = this.terrainGenerator.a(var4.x, var4.y, var4.z)) != -1) {
               var7 = this.terrainGenerator.a(var1, var3, var6);
            } else {
               var7 = this.terrainGenerator.a((Segment)var1, (RequestData)var3);
            }

            var3.currentChunkCache.setStructureList(var7);
         } catch (SegmentDataWriteException var5) {
            throw new RuntimeException("Cannot write to this chunk type " + var5.data.getClass().toString(), var5);
         }
      }
   }

   public boolean predictEmpty(Vector3i var1) {
      return false;
   }

   public boolean predictFirstStageEmpty(Vector3i var1) {
      Vector3i var2 = new Vector3i();
      Icosahedron.segmentLowPoint(var1, var2);
      return this.terrainGenerator.a(var2.x, var2.y, var2.z) || !Icosahedron.isSegmentInSide(var1);
   }

   static {
      dataPool = new ObjectArrayFIFOQueue((Integer)ServerConfig.CHUNK_REQUEST_THREAD_POOL_SIZE_CPU.getCurrentState());

      for(int var0 = 0; var0 < (Integer)ServerConfig.CHUNK_REQUEST_THREAD_POOL_SIZE_CPU.getCurrentState(); ++var0) {
         dataPool.enqueue(new RequestDataIcoPlanet());
      }

      CENTER_INDEX = GeneratorGridLock.getGridCoordinateLocal(1, 1, 1);
   }
}
