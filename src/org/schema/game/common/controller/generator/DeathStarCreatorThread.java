package org.schema.game.common.controller.generator;

import obfuscated.a;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.TeamDeathStar;
import org.schema.game.common.data.world.Segment;
import org.schema.game.server.controller.RequestData;

public class DeathStarCreatorThread extends CreatorThread {
   private a creator;

   public DeathStarCreatorThread(TeamDeathStar var1) {
      super(var1);
      var1.getSeed();
      this.creator = new a(new Vector3i(Ship.core));
   }

   public int isConcurrent() {
      return 1;
   }

   public int loadFromDatabase(Segment var1) {
      return -1;
   }

   public void onNoExistingSegmentFound(Segment var1, RequestData var2) {
      this.creator.a(this.getSegmentController(), var1, var2);
   }

   public boolean predictEmpty(Vector3i var1) {
      return false;
   }
}
