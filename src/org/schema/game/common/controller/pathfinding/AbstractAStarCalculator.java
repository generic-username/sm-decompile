package org.schema.game.common.controller.pathfinding;

import it.unimi.dsi.fastutil.floats.Float2LongRBTreeMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.FloatingRock;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.controller.pathfinding.AbstractPathFindingHandler;
import org.schema.game.server.controller.pathfinding.AbstractPathRequest;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.graphicsengine.forms.debug.DebugPoint;

public abstract class AbstractAStarCalculator {
   public static final long MAX_CALCULATION_TIME = 5000L;
   protected final boolean recordPath;
   protected final LongArrayList path = new LongArrayList();
   protected final Long2ObjectOpenHashMap pathMap = new Long2ObjectOpenHashMap();
   private final Vector3i dirA = new Vector3i();
   private final Vector3i dirB = new Vector3i();
   private final Vector3f bbTest = new Vector3f();
   protected SegmentController controller;
   protected Vector3i search;
   protected LongOpenHashSet currentPart;
   protected FloatingRock to;
   protected Vector3i min = new Vector3i(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);
   protected Vector3i max = new Vector3i(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
   protected Vector3i center = new Vector3i();
   protected long currentStart;
   protected Vector3i origin;
   protected BoundingBox roam;
   private Float2LongRBTreeMap openCollection = new Float2LongRBTreeMap();
   private LongOpenHashSet closedCollection = new LongOpenHashSet();
   private ObjectArrayList partCollection = new ObjectArrayList();
   private Vector3i absPosDist = new Vector3i();
   private Vector3i nextPosTmp = new Vector3i();
   private Vector3i absPosBase = new Vector3i();
   private Vector3i absPos = new Vector3i();
   private Vector3i absPosBefore = new Vector3i();

   public AbstractAStarCalculator(boolean var1) {
      this.recordPath = var1;
   }

   public void optimizePath(LongArrayList var1) {
   }

   public void init(AbstractPathRequest var1) {
      this.controller = var1.getSegmentController();
      this.min.set(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);
      this.max.set(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
      this.openCollection.clear();
      this.closedCollection.clear();
      this.partCollection.clear();
      this.pathMap.clear();
      this.path.clear();
      this.currentPart = null;
   }

   public boolean calculateDir(int var1, int var2, int var3, Vector3i var4, BoundingBox var5, Vector3i var6) {
      this.origin = var4;
      this.roam = new BoundingBox(var5);

      assert var4.x > Integer.MIN_VALUE && var4.y > Integer.MIN_VALUE && var4.z > Integer.MIN_VALUE;

      Vector3f var10000 = this.roam.min;
      var10000.x += (float)var4.x;
      var10000 = this.roam.min;
      var10000.y += (float)var4.y;
      var10000 = this.roam.min;
      var10000.z += (float)var4.z;
      var10000 = this.roam.max;
      var10000.x += (float)var4.x;
      var10000 = this.roam.max;
      var10000.y += (float)var4.y;
      var10000 = this.roam.max;
      var10000.z += (float)var4.z;
      this.closedCollection.add(ElementCollection.getIndex(var1, var2, var3));
      float var23 = var5.max.x - var5.min.x;
      float var7 = var5.max.y - var5.min.y;
      float var25 = var5.max.z - var5.min.z;
      var23 = Math.max(Math.max(var23, var7), var25);
      int var24 = Universe.getRandom().nextInt((int)Math.ceil((double)var23)) + 1;

      while(!this.closedCollection.isEmpty()) {
         long var8;
         if (this.openCollection.isEmpty()) {
            assert !this.closedCollection.isEmpty();

            LongIterator var26;
            var8 = (var26 = this.closedCollection.iterator()).nextLong();
            var26.remove();
            this.openCollection.put(1.0F, var8);
            this.currentPart = new LongOpenHashSet();
            this.partCollection.add(this.currentPart);
            this.currentStart = var8;
            this.currentPart.add(var8);
            ElementCollection.getPosFromIndex(var8, this.absPos);
            this.min.x = Math.min(this.absPos.x, this.min.x);
            this.min.y = Math.min(this.absPos.y, this.min.y);
            this.min.z = Math.min(this.absPos.z, this.min.z);
            this.max.x = Math.max(this.absPos.x + 1, this.max.x);
            this.max.y = Math.max(this.absPos.y + 1, this.max.y);
            this.max.z = Math.max(this.absPos.z + 1, this.max.z);
            if (this.recordPath) {
               this.pathMap.put(var8, new AbstractAStarCalculator.Node(0L, 1.0F));
            }
         }

         while(!this.openCollection.isEmpty()) {
            var25 = this.openCollection.firstFloatKey();
            var8 = this.openCollection.remove(var25);
            this.absPosBefore.set(this.absPos);
            ElementCollection.getPosFromIndex(var8, this.absPos);

            for(int var28 = 0; var28 < 6; ++var28) {
               long var10 = ElementCollection.getSide(var8, var28);
               if (!this.currentPart.contains(var10)) {
                  this.nextPosTmp.set(this.absPos);
                  this.nextPosTmp.add(Element.DIRECTIONSi[var28]);
                  this.absPosDist.set(var1, var2, var3);
                  this.absPosDist.sub(this.nextPosTmp);
                  if (this.absPosDist.length() >= (float)var24 && this.canTravelPoint(this.nextPosTmp, this.absPos, this.controller)) {
                     long var30 = ElementCollection.getIndex(this.nextPosTmp);
                     if (this.recordPath) {
                        this.pathMap.put(var10, new AbstractAStarCalculator.Node(var8, 0.0F));
                        long var32 = var30;

                        AbstractAStarCalculator.Node var18;
                        for(long var16 = -1L; var32 != this.currentStart; var32 = var18.parent) {
                           var18 = (AbstractAStarCalculator.Node)this.pathMap.get(var32);

                           assert var18 != null : this.currentStart + "::: " + var16 + " -> " + var32 + " :::" + var30 + "\n" + this.pathMap;

                           this.path.add(var32);
                           var16 = var32;
                        }

                        this.path.add(this.currentStart);
                        if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
                           Iterator var19 = this.path.iterator();

                           while(var19.hasNext()) {
                              ElementCollection.getPosFromIndex((Long)var19.next(), this.absPosBase);
                              Vector3f var21 = new Vector3f((float)(this.absPosBase.x - 16), (float)(this.absPosBase.y - 16), (float)(this.absPosBase.z - 16));
                              if (this.controller != null) {
                                 this.controller.getWorldTransform().transform(var21);
                              }

                              DebugPoint var22;
                              (var22 = new DebugPoint(var21, new Vector4f(0.0F, 0.0F, 1.0F, 1.0F))).size = 0.6F;
                              var22.LIFETIME = 10000L;
                              DebugDrawer.points.add(var22);
                           }
                        }
                     }

                     return true;
                  }

                  if (this.canTravelPoint(this.nextPosTmp, this.absPos, this.controller)) {
                     this.min.x = Math.min(this.nextPosTmp.x, this.min.x);
                     this.min.y = Math.min(this.nextPosTmp.y, this.min.y);
                     this.min.z = Math.min(this.nextPosTmp.z, this.min.z);
                     this.max.x = Math.max(this.nextPosTmp.x, this.max.x);
                     this.max.y = Math.max(this.nextPosTmp.y, this.max.y);
                     this.max.z = Math.max(this.nextPosTmp.z, this.max.z);
                     this.currentPart.add(var10);
                     float var12 = this.getWeightByBestDir(this.absPosBefore, this.absPos, this.nextPosTmp, var6);

                     assert var12 < Float.MAX_VALUE;

                     if (this.recordPath) {
                        AbstractAStarCalculator.Node var14 = (AbstractAStarCalculator.Node)this.pathMap.get(var8);
                        AbstractAStarCalculator.Node var15;
                        if ((var15 = (AbstractAStarCalculator.Node)this.pathMap.get(var10)) == null) {
                           var15 = new AbstractAStarCalculator.Node();
                           this.pathMap.put(var10, var15);
                           var15.parent = var8;
                           var15.costTo = var14.costTo + 0.5F;
                        } else if (var14.costTo + 0.5F < var15.costTo) {
                           var15.parent = var8;
                           var15.costTo = var14.costTo + 0.5F;
                        }

                        var12 += var15.costTo;
                        this.openCollection.values().remove(var10);
                     }

                     while(this.openCollection.containsKey(var12)) {
                        float var31 = var12;
                        var12 += 0.001F;
                        if (var31 == var12) {
                           System.err.println("ROUNDING ERROR: " + var12);

                           assert false;

                           var12 = 0.0F;
                        }
                     }

                     this.openCollection.put(var12, var10);
                  }
               }
            }
         }
      }

      System.err.println("[PATH] CALC MINMAX " + this.min + ", " + this.max);
      if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
         Iterator var27 = this.currentPart.iterator();

         while(var27.hasNext()) {
            ElementCollection.getPosFromIndex((Long)var27.next(), this.absPosBase);
            Vector3f var20 = new Vector3f((float)(this.absPosBase.x - 16), (float)(this.absPosBase.y - 16), (float)(this.absPosBase.z - 16));
            this.controller.getWorldTransform().transform(var20);
            DebugPoint var29;
            (var29 = new DebugPoint(var20, new Vector4f(1.0F, 0.0F, 0.0F, 1.0F))).LIFETIME = 8000L;
            var29.size = 0.6F;
            DebugDrawer.points.add(var29);
         }
      }

      return false;
   }

   protected boolean isTurn(Vector3i var1, Vector3i var2, Vector3i var3) {
      if (var1.equals(var2)) {
         return false;
      } else {
         this.dirA.sub(var2, var1);
         this.dirB.sub(var3, var2);
         return this.dirA.x != 0 && this.dirB.x == 0 || this.dirB.x != 0 && this.dirA.x == 0 || this.dirA.z != 0 && this.dirB.z == 0 || this.dirB.z != 0 && this.dirA.z == 0;
      }
   }

   protected boolean isInbound(Vector3i var1) {
      this.bbTest.set((float)(var1.x - 16), (float)(var1.y - 16), (float)(var1.z - 16));
      return this.roam.isInside(this.bbTest);
   }

   public boolean calculate(int var1, int var2, int var3, int var4, int var5, int var6) throws CalculationTookTooLongException {
      long var7 = System.currentTimeMillis();
      this.search = new Vector3i(var4, var5, var6);
      if (var1 == var4 && var2 == var5 && var3 == var6) {
         return false;
      } else {
         this.closedCollection.add(ElementCollection.getIndex(var1, var2, var3));
         var1 = 0;

         while(!this.closedCollection.isEmpty()) {
            long var9;
            if (this.openCollection.isEmpty()) {
               assert !this.closedCollection.isEmpty();

               LongIterator var20;
               var9 = (var20 = this.closedCollection.iterator()).nextLong();
               var20.remove();
               float var23 = this.getWeight(this.search, this.search, ElementCollection.getPosFromIndex(var9, this.absPosBase));
               this.openCollection.put(var23, var9);
               this.currentPart = new LongOpenHashSet();
               this.partCollection.add(this.currentPart);
               this.currentStart = var9;
               this.currentPart.add(var9);
               ElementCollection.getPosFromIndex(var9, this.absPos);
               this.min.x = Math.min(this.absPos.x, this.min.x);
               this.min.y = Math.min(this.absPos.y, this.min.y);
               this.min.z = Math.min(this.absPos.z, this.min.z);
               this.max.x = Math.max(this.absPos.x + 1, this.max.x);
               this.max.y = Math.max(this.absPos.y + 1, this.max.y);
               this.max.z = Math.max(this.absPos.z + 1, this.max.z);
               if (this.recordPath) {
                  this.pathMap.put(var9, new AbstractAStarCalculator.Node(0L, var23));
               }
            }

            while(!this.openCollection.isEmpty()) {
               ++AbstractPathFindingHandler.currentIt;
               ++var1;
               if (var1 % 1000 == 0 && System.currentTimeMillis() - var7 > 5000L) {
                  throw new CalculationTookTooLongException("Calculated Steps: " + var1);
               }

               float var22 = this.openCollection.firstFloatKey();
               ElementCollection.getPosFromIndex(var9 = this.openCollection.remove(var22), this.absPos);
               AbstractAStarCalculator.Node var25;
               if ((var25 = (AbstractAStarCalculator.Node)this.pathMap.get(var9)) != null) {
                  ElementCollection.getPosFromIndex(var25.parent, this.absPosBefore);
               } else {
                  this.absPosBefore.set(this.absPos);
               }

               for(var2 = 0; var2 < 6; ++var2) {
                  long var11 = ElementCollection.getSide(var9, var2);
                  if (!this.currentPart.contains(var11)) {
                     this.nextPosTmp.set(this.absPos);
                     this.nextPosTmp.add(Element.DIRECTIONSi[var2]);
                     if (this.nextPosTmp.equals(this.search) && this.canTravelPoint(this.nextPosTmp, this.absPos, this.controller)) {
                        long var27 = ElementCollection.getIndex(this.search);
                        if (this.recordPath) {
                           this.pathMap.put(var11, new AbstractAStarCalculator.Node(var9, 0.0F));
                           long var28 = var27;

                           AbstractAStarCalculator.Node var19;
                           for(long var17 = -1L; var28 != this.currentStart; var28 = var19.parent) {
                              var19 = (AbstractAStarCalculator.Node)this.pathMap.get(var28);

                              assert var19 != null : this.currentStart + "::: " + var17 + " -> " + var28 + " :::" + var27 + "\n" + this.pathMap;

                              this.path.add(var28);
                              var17 = var28;
                           }

                           this.path.add(this.currentStart);
                        }

                        return true;
                     }

                     if (this.canTravelPoint(this.nextPosTmp, this.absPos, this.controller)) {
                        this.min.x = Math.min(this.nextPosTmp.x, this.min.x);
                        this.min.y = Math.min(this.nextPosTmp.y, this.min.y);
                        this.min.z = Math.min(this.nextPosTmp.z, this.min.z);
                        this.max.x = Math.max(this.nextPosTmp.x, this.max.x);
                        this.max.y = Math.max(this.nextPosTmp.y, this.max.y);
                        this.max.z = Math.max(this.nextPosTmp.z, this.max.z);
                        this.currentPart.add(var11);
                        float var13 = this.getWeight(this.absPosBefore, this.absPos, this.nextPosTmp);
                        float var14 = this.getMoveWeight(this.absPosBefore, this.absPos, this.nextPosTmp);
                        if (this.recordPath) {
                           AbstractAStarCalculator.Node var15 = (AbstractAStarCalculator.Node)this.pathMap.get(var9);
                           AbstractAStarCalculator.Node var16;
                           if ((var16 = (AbstractAStarCalculator.Node)this.pathMap.get(var11)) == null) {
                              var16 = new AbstractAStarCalculator.Node();
                              this.pathMap.put(var11, var16);
                              var16.parent = var9;
                              var16.costTo = var15.costTo + var14;
                           } else if (var15.costTo + var14 < var16.costTo) {
                              var16.parent = var9;
                              var16.costTo = var15.costTo + var14;
                           }

                           var13 += var16.costTo;
                           this.openCollection.values().remove(var11);
                        }

                        while(this.openCollection.containsKey(var13)) {
                           var13 += 0.001F;
                        }

                        this.openCollection.put(var13, var11);
                     }
                  }
               }
            }
         }

         System.err.println("CALC MINMAX " + this.min + ", " + this.max);
         if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
            Iterator var24 = this.currentPart.iterator();

            while(var24.hasNext()) {
               ElementCollection.getPosFromIndex((Long)var24.next(), this.absPosBase);
               Vector3f var26 = new Vector3f((float)(this.absPosBase.x - 16), (float)(this.absPosBase.y - 16), (float)(this.absPosBase.z - 16));
               if (this.controller != null) {
                  this.controller.getWorldTransform().transform(var26);
               }

               DebugPoint var21;
               (var21 = new DebugPoint(var26, new Vector4f(1.0F, 0.0F, 0.0F, 1.0F))).LIFETIME = 8000L;
               var21.size = 0.6F;
               DebugDrawer.points.add(var21);
            }
         }

         return false;
      }
   }

   protected float getMoveWeight(Vector3i var1, Vector3i var2, Vector3i var3) {
      return 0.5F;
   }

   public abstract boolean canTravelPoint(Vector3i var1, Vector3i var2, SegmentController var3);

   protected abstract float getWeight(Vector3i var1, Vector3i var2, Vector3i var3);

   protected abstract float getWeightByBestDir(Vector3i var1, Vector3i var2, Vector3i var3, Vector3i var4);

   protected float getDistToSearchPos(Vector3i var1) {
      this.absPosBase.set(var1);
      Vector3i var10000 = this.absPosBase;
      var10000.x -= this.search.x;
      var10000 = this.absPosBase;
      var10000.y -= this.search.y;
      var10000 = this.absPosBase;
      var10000.z -= this.search.z;
      return this.absPosBase.length();
   }

   protected float getDistToSearchPos(int var1) {
      ElementCollection.getPosFromIndex((long)var1, this.absPosBase);
      Vector3i var10000 = this.absPosBase;
      var10000.x -= this.search.x;
      var10000 = this.absPosBase;
      var10000.y -= this.search.y;
      var10000 = this.absPosBase;
      var10000.z -= this.search.z;
      return this.absPosBase.length();
   }

   public LongArrayList getPath() {
      return this.path;
   }

   public int getCurrentPartSize() {
      return this.currentPart.size();
   }

   class Node {
      long parent;
      float costTo;

      public Node(long var2, float var4) {
         this.parent = var2;
         this.costTo = var4;
      }

      public Node() {
      }

      public String toString() {
         return "Node [parent=" + this.parent + ", costTo=" + this.costTo + "]";
      }
   }
}
