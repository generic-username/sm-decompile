package org.schema.game.common.controller.pathfinding;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.FloatingRock;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.generator.EmptyCreatorThread;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.controller.pathfinding.BreakTestRequest;

public class IslandCalculator extends AbstractAStarCalculator {
   public static byte[] buffer = new byte[4096];
   public static FastByteArrayOutputStream sb;
   private ObjectOpenHashSet reRequest = new ObjectOpenHashSet();

   public IslandCalculator() {
      super(false);
   }

   public void init(BreakTestRequest var1) {
      super.init(var1);
      this.reRequest.clear();
   }

   public boolean canTravelPoint(Vector3i var1, Vector3i var2, SegmentController var3) {
      return var3 == null || var3.getSegmentBuffer().existsPointUnsave(var1);
   }

   protected float getWeight(Vector3i var1, Vector3i var2, Vector3i var3) {
      return this.getDistToSearchPos(var2);
   }

   protected float getWeightByBestDir(Vector3i var1, Vector3i var2, Vector3i var3, Vector3i var4) {
      return 0.0F;
   }

   public void send(SendableSegmentController var1) {
   }

   public boolean breakUp(SegmentController var1) {
      if (this.currentPart.isEmpty()) {
         System.err.println("CURRENT PART IS EMPTY");
         return false;
      } else {
         LongIterator var2 = this.currentPart.iterator();
         Vector3i var3 = new Vector3i();
         Vector3i var4 = new Vector3i();
         Vector3i var5 = new Vector3i();
         this.to = new FloatingRock(var1.getState());
         SegmentPiece var6 = new SegmentPiece();
         Object2ObjectOpenHashMap var7 = new Object2ObjectOpenHashMap();
         System.err.println("LOCAL MINMAX: " + this.min + "; " + this.max);
         this.center.set(this.min);
         this.center.add((this.max.x - this.min.x) / 2 - 16, (this.max.y - this.min.y) / 2 - 16, (this.max.z - this.min.z) / 2 - 16);
         this.max.sub(this.center);
         this.min.sub(this.center);
         System.err.println("CENTERED MINMAX: " + this.min + "; " + this.max + " ;;; " + this.center);

         SegmentPiece var10;
         for(long var8 = System.currentTimeMillis(); var2.hasNext(); this.reRequest.add(var10.getSegment())) {
            ElementCollection.getPosFromIndex(var2.nextLong(), var3);
            var10 = var1.getSegmentBuffer().getPointUnsave(var3, var6);
            var4.sub(var3, this.center);
            ByteUtil.modUSeg(var4.x);
            ByteUtil.modUSeg(var4.y);
            ByteUtil.modUSeg(var4.z);
            int var11 = ByteUtil.divUSeg(var4.x);
            int var12 = ByteUtil.divUSeg(var4.y);
            int var13 = ByteUtil.divUSeg(var4.z);
            var11 <<= 5;
            var12 <<= 5;
            var13 <<= 5;
            var5.set(var11, var12, var13);
            RemoteSegment var20;
            if ((var20 = (RemoteSegment)var7.get(var5)) == null) {
               (var20 = new RemoteSegment(this.to)).pos.set(var5);
               this.to.getSegmentProvider().getFreeSegmentData().assignData(var20);
               var7.put(new Vector3i(var5), var20);
            }

            byte var22 = (byte)ByteUtil.modUSeg(var4.x);
            byte var23 = (byte)ByteUtil.modUSeg(var4.y);
            byte var14 = (byte)ByteUtil.modUSeg(var4.z);

            try {
               var20.getSegmentData().applySegmentData(var22, var23, var14, var10.getData(), 0, true, var20.getAbsoluteIndex(var22, var23, var14), true, true, var8);
            } catch (SegmentDataWriteException var16) {
               var16.printStackTrace();
            }

            try {
               var10.getSegment().getSegmentData().setType(SegmentData.getInfoIndex(var10.x, var10.y, var10.z), (short)0);
            } catch (SegmentDataWriteException var15) {
               var15.printStackTrace();
            }
         }

         this.to.setUniqueIdentifier("ENTITY_FLOATINGROCK_DEBRIS_" + System.currentTimeMillis());
         this.min.x = ByteUtil.divSeg(this.min.x);
         this.min.y = ByteUtil.divSeg(this.min.y);
         this.min.z = ByteUtil.divSeg(this.min.z);
         this.max.x = ByteUtil.divSeg(this.max.x) + 1;
         this.max.y = ByteUtil.divSeg(this.max.y) + 1;
         this.max.z = ByteUtil.divSeg(this.max.z) + 1;
         this.to.initialize();
         this.to.getMinPos().set(this.min);
         this.to.getMaxPos().set(this.max);
         this.to.setId(var1.getState().getNextFreeObjectId());
         this.to.setSectorId(var1.getSectorId());
         Transform var18 = new Transform(var1.getWorldTransform());
         Vector3f var21 = new Vector3f((float)this.center.x, (float)this.center.y, (float)this.center.z);
         var18.basis.transform(var21);
         var18.origin.add(var21);
         this.to.getInitialTransform().set(var18);
         this.to.setCreatorThread(new EmptyCreatorThread(this.to));
         this.to.setTouched(true, true);
         Iterator var19 = var7.values().iterator();

         while(var19.hasNext()) {
            RemoteSegment var17 = (RemoteSegment)var19.next();
            System.err.println("ACT ADDING SEGMENT: " + var17 + ": " + var17.getSize());
            this.to.getSegmentProvider().addSegmentToBuffer(var17);
         }

         return true;
      }
   }

   static {
      sb = new FastByteArrayOutputStream(buffer);
   }
}
