package org.schema.game.common.controller;

public interface SegmentBufferUnloadedIteratorInterface {
   boolean handle(int var1, int var2, int var3);
}
