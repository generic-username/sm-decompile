package org.schema.game.common.controller;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.client.view.camera.InShipCamera;
import org.schema.game.common.controller.ntsmothers.TransformationSmoother;
import org.schema.game.common.data.physics.PairCachingGhostObjectAlignable;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.NetworkEntityProvider;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.network.Identifiable;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.client.ClientProcessor;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.objects.LocalSectorTransition;
import org.schema.schine.network.objects.NetworkEntity;
import org.schema.schine.network.objects.NetworkTransformation;
import org.schema.schine.network.objects.WarpTransformation;
import org.schema.schine.network.objects.container.PhysicsDataContainer;
import org.schema.schine.network.objects.container.TransformTimed;
import org.schema.schine.network.objects.remote.RemoteFloatPrimitiveArray;
import org.schema.schine.network.objects.remote.RemoteWarpTransformation;
import org.schema.schine.network.server.ServerStateInterface;
import org.schema.schine.physics.Physical;

public abstract class RemoteTransformable implements Transformable, Physical {
   private static final int EQUAL_TIMES = 32;
   private static final int LIN_ROT_UPDATES = 8;
   public final List warpedTransformations = new ObjectArrayList();
   final RemoteTransformable.ReceivedTransformation receivedTransformation = new RemoteTransformable.ReceivedTransformation();
   private final Transform initialTransform;
   private final boolean onServer;
   private final RemoteTransformable.ReceivedTransformation receivedTransformationLocal = new RemoteTransformable.ReceivedTransformation();
   private final Transform nextTransform = new Transform();
   private final Vector3f tmpDist = new Vector3f();
   private final Transform tmpT = new Transform();
   public boolean useSmoother = true;
   public int equalCounter;
   int uCounter = 0;
   private TransformationSmoother transformationSmoother;
   private PhysicsDataContainer physicsDataContainer;
   private float mass = 0.1F;
   private Identifiable obj;
   private StateInterface state;
   private Transform lastTransform = new Transform();
   private boolean sendFromClient;
   private boolean snapped;
   private Vector3f tmp = new Vector3f();
   private long lastSent;
   private boolean sendLinearAndAngularFromServer;
   private long lastMassUpdate;
   private boolean waitingForApprovalWarp;
   private long lastAll;
   private long timeOfPhysicsUpdate;

   public RemoteTransformable(Identifiable var1, StateInterface var2) {
      this.obj = var1;
      this.state = var2;
      this.onServer = var2 instanceof ServerStateInterface;
      this.transformationSmoother = new TransformationSmoother(this);
      this.initialTransform = new Transform();
      this.initialTransform.setIdentity();
      this.setPhysicsDataContainer(new PhysicsDataContainer());
   }

   public void broadcastTransform(Transform var1, Vector3f var2, Vector3f var3, LocalSectorTransition var4, NetworkEntity var5) {
      assert this.getState() instanceof ServerStateInterface;

      RemoteFloatPrimitiveArray var6 = new RemoteFloatPrimitiveArray(22, var5);
      var1.getOpenGLMatrix(var6.getFloatArray());
      var6.getFloatArray()[16] = var2.x;
      var6.getFloatArray()[17] = var2.y;
      var6.getFloatArray()[18] = var2.z;
      var6.getFloatArray()[19] = var3.x;
      var6.getFloatArray()[20] = var3.y;
      var6.getFloatArray()[21] = var3.z;
      WarpTransformation var7;
      (var7 = new WarpTransformation()).t = new Transform(var1);
      var7.lin = new Vector3f(var2);
      var7.ang = new Vector3f(var3);
      var7.local = var4;
      var5.warpingTransformation.add(new RemoteWarpTransformation(var7, var5));
      if (this.getIdentifiable() instanceof PlayerControllable && !((PlayerControllable)this.getIdentifiable()).getAttachedPlayers().isEmpty()) {
         System.err.println("################# SERVER WAITING FOR PLAYER WARP");
         this.waitingForApprovalWarp = true;
      }

   }

   private void checkReceivedTransformation() {
      if (this.receivedTransformation.changed) {
         synchronized(this.receivedTransformation) {
            this.receivedTransformationLocal.timestamp = this.receivedTransformation.timestamp;
            this.receivedTransformationLocal.transform.set(this.receivedTransformation.transform);
            this.receivedTransformationLocal.linearVelocity.set(this.receivedTransformation.linearVelocity);
            this.receivedTransformationLocal.angularVelocity.set(this.receivedTransformation.angularVelocity);
            this.receivedTransformationLocal.receivedVelocities = this.receivedTransformation.receivedVelocities;
            this.receivedTransformationLocal.playerAttached = this.receivedTransformation.playerAttached;
            this.receivedTransformationLocal.changed = true;
            this.receivedTransformationLocal.changedOnce = true;
            this.receivedTransformation.changed = false;
         }
      }

      if (this.receivedTransformationLocal.changed) {
         this.receivedTransformationLocal.changed = false;
         if (this.getPhysicsDataContainer().getObject() instanceof PairCachingGhostObjectAlignable) {
            PairCachingGhostObjectAlignable var1 = (PairCachingGhostObjectAlignable)this.getPhysicsDataContainer().getObject();
            if (this.receivedTransformationLocal.playerAttached != (var1.getAttached() != null)) {
               this.receivedTransformationLocal.changedOnce = false;
               return;
            }
         }

         CollisionObject var4;
         if ((var4 = this.getPhysicsDataContainer().getObject()) instanceof RigidBody) {
            RigidBody var2 = (RigidBody)var4;
            if (!this.isOnServer() && ((GameClientState)this.getState()).currentEnterTry == this.obj && System.currentTimeMillis() - ((GameClientState)this.getState()).currentEnterTryTime < 1000L) {
               System.err.println("NOT SMOOTHING VELO SHIP");
            } else if (this.receivedTransformationLocal.receivedVelocities && !var2.isStaticObject()) {
               var2.setLinearVelocity(this.receivedTransformationLocal.linearVelocity);
               var2.setAngularVelocity(this.receivedTransformationLocal.angularVelocity);
               if (this.isOnServer()) {
                  this.sendLinearAndAngularFromServer = true;
               }
            }
         }

         if (this.isSmoothingGeneral()) {
            if (!this.isOnServer() && ((GameClientState)this.getState()).currentEnterTry == this.obj && this.getState().getUpdateTime() - ((GameClientState)this.getState()).currentEnterTryTime < 1000L) {
               System.err.println("[CLIENT] NOT SMOOTHING SHIP");
               return;
            }

            if (!this.isOnServer()) {
               if (this.isSmoothingEntity()) {
                  this.transformationSmoother.recordStep(new Transform(this.receivedTransformationLocal.transform), this.receivedTransformationLocal.timestamp);
                  return;
               }

               this.transformationSmoother.onNoSmooth();
               return;
            }
         } else {
            this.transformationSmoother.onNoSmooth();
            if (!this.isOnServer() || !this.waitingForApprovalWarp) {
               this.transformationSmoother.doInstantStep(this.receivedTransformationLocal.transform);
            }
         }
      }

   }

   public boolean isSmoothingEntity() {
      return (!(this.getIdentifiable() instanceof SegmentController) || !((SegmentController)this.getIdentifiable()).getDockingController().isDocked()) && this.useSmoother && !((SimpleTransformableSendableObject)this.getIdentifiable()).isHidden();
   }

   public boolean isSmoothingGeneral() {
      if (this.isOnServer()) {
         return false;
      } else {
         return this.getIdentifiable() instanceof SimpleTransformableSendableObject && ((SimpleTransformableSendableObject)this.getIdentifiable()).getSectorId() == ((GameClientState)this.getState()).getCurrentSectorId() && !GameClientState.smoothDisableDebug;
      }
   }

   public long getCurrentLatency() {
      return this.state instanceof ServerStateInterface ? 0L : ((ClientState)this.state).getPing();
   }

   public int getEqualCounter() {
      return this.equalCounter;
   }

   public void setEqualCounter(int var1) {
      this.equalCounter = var1;
   }

   public Identifiable getIdentifiable() {
      return this.obj;
   }

   public Transform getInitialTransform() {
      return this.initialTransform;
   }

   public float getMass() {
      return this.mass;
   }

   public PhysicsDataContainer getPhysicsDataContainer() {
      return this.physicsDataContainer;
   }

   public void setPhysicsDataContainer(PhysicsDataContainer var1) {
      this.physicsDataContainer = var1;
   }

   public void setMass(float var1) {
      this.mass = var1;
   }

   public TransformTimed getWorldTransform() {
      return this.physicsDataContainer.getCurrentPhysicsTransform();
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public boolean isSendFromClient() {
      return this.sendFromClient || !this.isOnServer() && this.obj instanceof SimpleTransformableSendableObject && ((SimpleTransformableSendableObject)this.obj).isChainedToSendFromClient();
   }

   public void setSendFromClient(boolean var1) {
      this.sendFromClient = var1;
   }

   public boolean isSnapped() {
      return this.snapped;
   }

   public void setSnapped(boolean var1) {
      this.snapped = var1;
   }

   public void receiveTransformation(NetworkEntityProvider var1) {
      if (!(this.getState() instanceof ClientState) || !this.isSendFromClient()) {
         boolean var2 = false;
         NetworkTransformation var3 = (NetworkTransformation)var1.transformationBuffer.get();
         Transform var4 = ((NetworkTransformation)var1.transformationBuffer.get()).getTransformReceive();
         Vector3f var5 = new Vector3f(((NetworkTransformation)var1.transformationBuffer.get()).getLinReceive());
         Vector3f var10 = new Vector3f(((NetworkTransformation)var1.transformationBuffer.get()).getAngReceive());
         long var6;
         if (this.isOnServer()) {
            var6 = var3.getTimeStampReceive();
         } else {
            var6 = ((ClientProcessor)((GameClientState)this.getState()).getProcessor()).getServerPacketSentTimestamp();
         }

         if (var3.received) {
            var3.received = false;
            var2 = true;
         }

         if (var2) {
            synchronized(this.receivedTransformation) {
               this.receivedTransformation.timestamp = var6;
               this.receivedTransformation.transform.set(var4);
               this.receivedTransformation.linearVelocity.set(var5);
               this.receivedTransformation.angularVelocity.set(var10);
               this.receivedTransformation.receivedVelocities = var3.receivedVil;
               this.receivedTransformation.playerAttached = var3.isPlayerAttachedReceive();

               assert this.receivedTransformation.transform.getMatrix(new Matrix4f()).determinant() != 0.0F : this.getIdentifiable() + "\n" + this.receivedTransformation.transform.getMatrix(new Matrix4f());

               var3.receivedVil = false;
               this.receivedTransformation.changed = true;
               if (this.getIdentifiable() instanceof FloatingRock && this.receivedTransformation.transform.origin.lengthSquared() == 0.0F) {
                  try {
                     throw new Exception("WARNING: received strange transformation: " + this.getIdentifiable() + "; " + this.getState() + "; " + this.receivedTransformation.transform.origin);
                  } catch (Exception var8) {
                     var8.printStackTrace();
                  }
               }

            }
         }
      }
   }

   private void receiveWarpTransformation(NetworkEntity var1) {
      if (this.getState() instanceof ClientState && this.getPhysicsDataContainer().isInitialized()) {
         for(int var2 = 0; var2 < var1.warpingTransformation.getReceiveBuffer().size(); ++var2) {
            RemoteWarpTransformation var3 = (RemoteWarpTransformation)var1.warpingTransformation.getReceiveBuffer().get(var2);
            this.warpedTransformations.add(var3.get());
         }
      }

   }

   private void sendTransform() {
      if (!(this.getIdentifiable() instanceof SegmentController) || !((SegmentController)this.getIdentifiable()).getDockingController().isDocked() && !((SegmentController)this.getIdentifiable()).railController.isDockedOrDirty()) {
         if (this.getPhysicsDataContainer().isInitialized()) {
            if (!(this.getIdentifiable() instanceof SimpleTransformableSendableObject) || !((SimpleTransformableSendableObject)this.getIdentifiable()).isHidden()) {
               boolean var1 = false;
               this.nextTransform.set(this.getPhysicsDataContainer().getOriginalTransform());
               PairCachingGhostObjectAlignable var2;
               if (this.getPhysicsDataContainer().getObject() instanceof PairCachingGhostObjectAlignable && (var2 = (PairCachingGhostObjectAlignable)this.getPhysicsDataContainer().getObject()).getAttached() != null) {
                  var1 = true;
                  this.nextTransform.set(var2.localWorldTransform);
               }

               SimpleTransformableSendableObject var9 = (SimpleTransformableSendableObject)this.getIdentifiable();
               boolean var3 = false;
               boolean var4 = false;
               if (!this.getPhysicsDataContainer().getObject().isStaticOrKinematicObject() && this.timeOfPhysicsUpdate - this.lastSent > 20000L || this.sendLinearAndAngularFromServer) {
                  this.sendLinearAndAngularFromServer = false;
                  this.equalCounter = 32;
                  this.uCounter = 9;
               }

               boolean var5 = this.lastTransform.equals(this.nextTransform);
               boolean var6 = !this.getPhysicsDataContainer().getObject().isStaticOrKinematicObject() || this.getPhysicsDataContainer().getObject() instanceof PairCachingGhostObjectAlignable;
               if (this.obj instanceof SegmentController && ((SegmentController)this.obj).getMass() <= 0.0F) {
                  var6 = false;
               }

               if (!var5 || var6 && this.equalCounter >= 32) {
                  this.equalCounter = 0;
                  var3 = true;
                  if (this.getPhysicsDataContainer().getObject() instanceof RigidBody && this.uCounter > 8) {
                     var4 = true;
                     this.uCounter = 0;
                  }

                  this.lastTransform.set(this.nextTransform);
                  ++this.uCounter;
                  this.lastSent = System.currentTimeMillis();
               } else if (var5) {
                  ++this.equalCounter;
               }

               var5 = false;
               if (var3 && this.timeOfPhysicsUpdate - this.lastAll > 3000L) {
                  this.lastAll = this.timeOfPhysicsUpdate;
                  var5 = true;
               }

               Iterator var10 = var9.getListeners().iterator();

               while(true) {
                  do {
                     NetworkListenerEntity var11;
                     do {
                        do {
                           if (!var10.hasNext()) {
                              return;
                           }
                        } while(!(var11 = (NetworkListenerEntity)var10.next()).isSendTo() && !var5);
                     } while(!var3);

                     NetworkEntityProvider var12;
                     ((NetworkTransformation)(var12 = var11.getNetworkObject()).transformationBuffer.get()).setTimeStamp(this.timeOfPhysicsUpdate);
                     ((NetworkTransformation)var12.transformationBuffer.get()).getTransform().set(this.nextTransform);
                     ((NetworkTransformation)var12.transformationBuffer.get()).setPlayerAttached(var1);
                     CollisionObject var7;
                     if ((var7 = this.getPhysicsDataContainer().getObject()) instanceof RigidBody) {
                        RigidBody var13 = (RigidBody)var7;
                        ((NetworkTransformation)var12.transformationBuffer.get()).setLin(var13.getLinearVelocity(this.tmp));
                        ((NetworkTransformation)var12.transformationBuffer.get()).setAng(var13.getAngularVelocity(this.tmp));
                        var13.getLinearVelocity(this.tmp);
                        if (var4) {
                           ((NetworkTransformation)var12.transformationBuffer.get()).sendVil = true;
                        } else {
                           ((NetworkTransformation)var12.transformationBuffer.get()).sendVil = false;
                        }
                     }

                     assert ((NetworkTransformation)var12.transformationBuffer.get()).getTransform().getMatrix(new Matrix4f()).determinant() != 0.0F : "\n" + this.receivedTransformation.transform.getMatrix(new Matrix4f());

                     var12.transformationBuffer.setChanged(true);
                     var12.setChanged(true);
                     ((NetworkTransformation)var12.transformationBuffer.get()).prime = true;
                  } while(!(this.getIdentifiable() instanceof FloatingRock) || this.nextTransform.origin.lengthSquared() != 0.0F);

                  try {
                     throw new Exception("WARNING: SENDING strange transformation: " + this.getIdentifiable() + "; " + this.getState() + "; " + this.nextTransform.origin);
                  } catch (Exception var8) {
                     var8.printStackTrace();
                  }
               }
            }
         }
      }
   }

   public void setLagCompensation(boolean var1) {
   }

   public void update(Timer var1) {
      if (this.getPhysicsDataContainer().isInitialized() && this.getPhysicsDataContainer().getObject() == null) {
         this.receivedTransformation.changed = false;
         this.receivedTransformation.transform.set(this.getWorldTransform());
      }

      if (this.getPhysicsDataContainer().isInitialized() && this.getPhysicsDataContainer().getObject() != null) {
         this.checkReceivedTransformation();
         if (this.getPhysicsDataContainer().getObject() instanceof RigidBody) {
            ((RigidBody)this.getPhysicsDataContainer().getObject()).getLinearVelocity(new Vector3f());
         }

         boolean var2 = false;

         Transform var4;
         while(!this.warpedTransformations.isEmpty()) {
            var2 = true;
            if (this.obj instanceof SimpleTransformableSendableObject && !((SimpleTransformableSendableObject)this.obj).isHidden()) {
               ((SimpleTransformableSendableObject)this.obj).setWarpToken(true);
            }

            WarpTransformation var3 = (WarpTransformation)this.warpedTransformations.remove(0);
            var4 = new Transform(var3.t);
            if (var3.local != null) {
               assert !this.isOnServer();

               if (this.getPhysicsDataContainer().getObject() instanceof RigidBody && !var3.local.oldPosPlanet && !var3.local.newPosPlanet) {
                  var4 = var3.local.getTransitionTransform(this.getWorldTransform());
                  ((RigidBody)this.getPhysicsDataContainer().getObject()).getLinearVelocity(var3.lin);
                  ((RigidBody)this.getPhysicsDataContainer().getObject()).getAngularVelocity(var3.ang);
               }
            }

            CollisionObject var5;
            (var5 = this.getPhysicsDataContainer().getObject()).setWorldTransform(var4);
            var5.setInterpolationWorldTransform(var4);
            PairCachingGhostObjectAlignable var9;
            if (this.getPhysicsDataContainer().getObject() instanceof PairCachingGhostObjectAlignable && (var9 = (PairCachingGhostObjectAlignable)this.getPhysicsDataContainer().getObject()).localWorldTransform != null) {
               Transform var6;
               (var6 = new Transform(var9.getAttached().getWorldTransform())).inverse();
               Transform var7 = new Transform(var4);
               var6.mul(var7);
               var9.localWorldTransform.set(var6);
            }

            if (this.getPhysicsDataContainer().getObject() instanceof RigidBody) {
               ((RigidBody)this.getPhysicsDataContainer().getObject()).setCenterOfMassTransform(var4);
               ((RigidBody)this.getPhysicsDataContainer().getObject()).getMotionState().setWorldTransform(var4);
               ((RigidBody)this.getPhysicsDataContainer().getObject()).setLinearVelocity(var3.lin);
               ((RigidBody)this.getPhysicsDataContainer().getObject()).setAngularVelocity(var3.ang);
            }

            this.getWorldTransform().set(var4);
            this.getPhysicsDataContainer().updatePhysical(this.getState().getUpdateTime());
            this.transformationSmoother.reset();
            if (!this.isOnServer()) {
               if (this.obj == ((GameClientState)this.state).getShip() && ((GameClientState)this.state).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController().isActive() && Controller.getCamera() instanceof InShipCamera) {
                  ((InShipCamera)Controller.getCamera()).forceOrientation(var4, var1);
               }

               if (this.getIdentifiable() == ((GameClientState)this.getState()).getCurrentPlayerObject()) {
                  System.err.println("[REMOTE] FLAGGIN OWN CLIENT warp: " + this.getIdentifiable() + ": " + var4.origin);
                  ((GameClientState)this.getState()).flagWarped();
               }
            }
         }

         if (var2 && this.getIdentifiable() instanceof SimpleTransformableSendableObject && ((SimpleTransformableSendableObject)this.getIdentifiable()).isClientOwnObject()) {
            System.err.println("[CLIENT] WARP RECEIVED " + this.getWorldTransform().origin + " ACC SENT");
            ((SimpleTransformableSendableObject)this.getIdentifiable()).getNetworkObject().receivedWarpACC.add((byte)1);
         }

         if (this.isOnServer() && this.getIdentifiable() instanceof SimpleTransformableSendableObject && ((SimpleTransformableSendableObject)this.getIdentifiable()).isConrolledByActivePlayer() && !this.waitingForApprovalWarp && (!(this.getIdentifiable() instanceof SegmentController) || !((SegmentController)this.getIdentifiable()).getDockingController().isDocked() && !((SegmentController)this.getIdentifiable()).railController.isDockedOrDirty())) {
            boolean var8;
            Vector3f var10000;
            Vector3f var10001;
            Vector3f var10002;
            if (this.getPhysicsDataContainer().getObject() != null && this.getPhysicsDataContainer().getObject() instanceof PairCachingGhostObjectAlignable && ((PairCachingGhostObjectAlignable)this.getPhysicsDataContainer().getObject()).getAttached() != null) {
               var8 = true;
               var4 = ((PairCachingGhostObjectAlignable)this.getPhysicsDataContainer().getObject()).localWorldTransform;
               var10000 = this.tmpDist;
               var10001 = this.receivedTransformationLocal.transform.origin;
               var10002 = var4.origin;
            } else {
               var8 = false;
               var10000 = this.tmpDist;
               var10001 = this.receivedTransformationLocal.transform.origin;
               var10002 = this.getPhysicsDataContainer().getObject() != null ? this.getPhysicsDataContainer().getObject().getWorldTransform(this.tmpT).origin : this.getPhysicsDataContainer().getOriginalTransform().origin;
            }

            var10000.sub(var10001, var10002);
            if (this.tmpDist.length() > 15.0F && this.receivedTransformationLocal.changedOnce) {
               System.err.println("[SERVER] snap plCntrldObj " + this.getIdentifiable() + " from " + this.getWorldTransform().origin + " (original " + this.getPhysicsDataContainer().getOriginalTransform().origin + " [" + this.getPhysicsDataContainer().lastCenter + "] PhysObjNotNull? " + (this.getPhysicsDataContainer().getObject() != null) + ") to " + this.receivedTransformationLocal.transform.origin + "; ATTACHED: " + var8);
               this.transformationSmoother.doInstantStep(new Transform(this.receivedTransformationLocal.transform));
            }
         }
      }

      this.transformationSmoother.update(var1);
      this.timeOfPhysicsUpdate = this.state.getUpdateTime();
   }

   public void updateFromRemoteInitialTransform(NetworkEntity var1) {
      if (this.state instanceof ClientStateInterface && this.getPhysicsDataContainer().isInitialized()) {
         this.setMass(var1.mass.get());
         this.getPhysicsDataContainer().updateMass(this.getMass(), true);
      }

      this.getInitialTransform().setFromOpenGLMatrix(var1.initialTransform.getFloatArray());
   }

   public void updateFromRemoteTransform(NetworkEntity var1) {
      if (this.state instanceof ClientStateInterface && this.getMass() != var1.mass.get() && this.getPhysicsDataContainer().isInitialized()) {
         this.setMass(var1.mass.get());
         this.getPhysicsDataContainer().updateMass(this.getMass(), true);
      }

      if (this.getIdentifiable() instanceof PlayerControllable && ((PlayerControllable)this.getIdentifiable()).getAttachedPlayers().isEmpty()) {
         if (this.waitingForApprovalWarp) {
            System.err.println("################# SERVER NO MORE WAITING FOR PLAYER WARP (NO PLAYERS ATTACHED)");
         }

         this.waitingForApprovalWarp = false;
      }

      if (this.isOnServer() && this.getIdentifiable() instanceof SimpleTransformableSendableObject && !((SimpleTransformableSendableObject)this.getIdentifiable()).getNetworkObject().receivedWarpACC.getReceiveBuffer().isEmpty()) {
         if (this.waitingForApprovalWarp) {
            System.err.println("################# SERVER NO MORE WAITING FOR PLAYER WARP (ACC RECEIVED)");
         }

         this.waitingForApprovalWarp = false;
      }

      this.receiveWarpTransformation(var1);
   }

   public void updateToRemoteInitialTransform(NetworkEntity var1) {
      if (this.state instanceof ServerStateInterface) {
         var1.mass.set(this.getMass());
         if (this.getPhysicsDataContainer().isInitialized()) {
            this.getInitialTransform().set(this.getWorldTransform());
         }

         this.getInitialTransform().getOpenGLMatrix(var1.initialTransform.getFloatArray());
         var1.initialTransform.setChanged(true);
      } else {
         var1.initialTransform.forceClientUpdates();
         this.getInitialTransform().getOpenGLMatrix(var1.initialTransform.getFloatArray());
         var1.initialTransform.setChanged(true);
      }
   }

   public void updateToRemoteTransform(NetworkEntity var1, StateInterface var2) {
      if (this.isOnServer() && var1.mass.getFloat() != this.getMass() && System.currentTimeMillis() - this.lastMassUpdate > 300L) {
         var1.mass.set(this.getMass());
         this.lastMassUpdate = System.currentTimeMillis();
      }

      if (this.getPhysicsDataContainer() != null && this.getPhysicsDataContainer().isInitialized()) {
         if (this.isOnServer() || this.isSendFromClient()) {
            this.sendTransform();
         }

      }
   }

   public void warp(Transform var1, boolean var2) {
      if (this.physicsDataContainer.getObject() != null) {
         Transform var3 = this.physicsDataContainer.getObject().getWorldTransform(new Transform());
         if (!var2) {
            var3.origin.set(var1.origin);
         } else {
            var3.set(var1);
         }

         CollisionObject var4;
         (var4 = this.getPhysicsDataContainer().getObject()).setWorldTransform(var3);
         var4.setInterpolationWorldTransform(var3);
         if (this.getPhysicsDataContainer().getObject() instanceof RigidBody) {
            ((RigidBody)this.getPhysicsDataContainer().getObject()).setCenterOfMassTransform(var3);
            ((RigidBody)this.getPhysicsDataContainer().getObject()).getMotionState().setWorldTransform(var3);
         }

         this.getWorldTransform().set(var3);
         this.getPhysicsDataContainer().updatePhysical(this.getState().getUpdateTime());
         this.transformationSmoother.reset();
      }

   }

   public void onSmootherSet(Transform var1) {
      if (this.obj instanceof SimpleTransformableSendableObject) {
         ((SimpleTransformableSendableObject)this.obj).onSmootherSet(var1);
      }

   }

   class ReceivedTransformation {
      public final Transform transform = new Transform();
      public final Vector3f linearVelocity = new Vector3f();
      public final Vector3f angularVelocity = new Vector3f();
      public long timestamp;
      public boolean receivedVelocities;
      public boolean playerAttached;
      public boolean changedOnce;
      boolean changed;
   }
}
