package org.schema.game.common.controller;

import org.schema.game.common.data.world.Segment;

public interface SegmentBufferIntersectionInterface {
   boolean handle(Segment var1, Segment var2);
}
