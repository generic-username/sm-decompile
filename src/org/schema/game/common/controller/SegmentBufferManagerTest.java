package org.schema.game.common.controller;

import junit.framework.TestCase;
import org.schema.common.util.linAlg.Vector3i;

public class SegmentBufferManagerTest extends TestCase {
   final int segMax = 512;
   Vector3i exp = new Vector3i();
   Vector3i test = new Vector3i();

   private void equ(Vector3i var1, Vector3i var2) {
   }

   protected void setUp() throws Exception {
      super.setUp();
   }

   protected void tearDown() throws Exception {
      super.tearDown();
   }

   public void testNegSegmentCalculations() {
      Vector3i var10000;
      int var1;
      int var2;
      int var3;
      for(var1 = -512; var1 < 0; ++var1) {
         for(var2 = 0; var2 < 512; ++var2) {
            for(var3 = 0; var3 < 512; ++var3) {
               this.exp.set(-8, 0, 0);
               this.test.set(var1, var2, var3);
               var10000 = this.exp;
               var10000 = this.test;
            }
         }
      }

      for(var1 = -512; var1 < 0; ++var1) {
         for(var2 = -512; var2 < 0; ++var2) {
            for(var3 = -512; var3 < 0; ++var3) {
               this.exp.set(-16, -16, -16);
               this.test.set(var1, var2, var3);
               var10000 = this.exp;
               var10000 = this.test;
            }
         }
      }

      this.tNeg(-33, -10, -10);
      this.tNeg(-10, 2, -10);
      this.tNeg(-10, 12, 10);
      this.tNeg(10, -89, 23);
      this.tNeg(10, 10, -12);
   }

   public void testPosSegmentCalculations() {
      Vector3i var10000;
      int var1;
      int var2;
      int var3;
      for(var1 = 0; var1 < 512; ++var1) {
         for(var2 = 0; var2 < 512; ++var2) {
            for(var3 = 0; var3 < 512; ++var3) {
               this.exp.set(0, 0, 0);
               this.test.set(var1, var2, var3);
               var10000 = this.exp;
               var10000 = this.test;
            }
         }
      }

      for(var1 = 512; var1 < 1024; ++var1) {
         for(var2 = 0; var2 < 512; ++var2) {
            for(var3 = 0; var3 < 512; ++var3) {
               this.exp.set(8, 0, 0);
               this.test.set(var1, var2, var3);
               var10000 = this.exp;
               var10000 = this.test;
            }
         }
      }

      this.tNeg(0, 0, 0);
      this.tNeg(1, 0, 0);
      this.tNeg(0, 1, 1);
   }

   private void tNeg(int var1, int var2, int var3) {
      for(int var4 = var1 << 9; var4 < var1 + 1 << 9; ++var4) {
         for(int var5 = var2 << 9; var5 < var2 + 1 << 9; ++var5) {
            for(int var6 = var3 << 9; var6 < var3 + 1 << 9; ++var6) {
               this.exp.set(var1 << 4, var2 << 4, var3 << 4);
               this.test.set(var4, var5, var6);
               Vector3i var10000 = this.exp;
               var10000 = this.test;
            }
         }
      }

   }
}
