package org.schema.game.common.controller.database.tables;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.schema.game.common.data.player.inventory.FreeItem;
import org.schema.game.common.data.world.Sector;

public class SectorItemTable extends Table {
   public static final int itemArraySize = 22528;

   public SectorItemTable(TableManager var1, Connection var2) {
      super("SECTORS_ITEMS", var1, var2);
   }

   public void define() {
      this.addColumn("ID", "BIGINT", true);
      this.addColumn("ITEMS", "VARBINARY(22528) not null");
   }

   @Deprecated
   public void createSectorItemTable() throws SQLException {
      Statement var1;
      (var1 = this.c.createStatement()).execute("DROP TABLE SECTORS_ITEMS if exists;");
      var1.execute("CREATE CACHED TABLE SECTORS_ITEMS(BIGINT, ITEMS VARBINARY(22528) not null, primary key (ID));");
      var1.close();
   }

   public static byte[] getItemBinaryString(Map var0) throws IOException {
      ByteArrayOutputStream var1 = new ByteArrayOutputStream(Math.min(22528, var0.size() * 22));
      DataOutputStream var2 = new DataOutputStream(var1);
      int var3 = 0;

      for(Iterator var5 = var0.entrySet().iterator(); var5.hasNext(); var3 += 22) {
         Entry var4 = (Entry)var5.next();
         if (var3 >= 22528) {
            break;
         }

         var2.writeShort(((FreeItem)var4.getValue()).getType());
         var2.writeInt(((FreeItem)var4.getValue()).getCount());
         var2.writeFloat(((FreeItem)var4.getValue()).getPos().x);
         var2.writeFloat(((FreeItem)var4.getValue()).getPos().y);
         var2.writeFloat(((FreeItem)var4.getValue()).getPos().z);
         var2.writeInt(((FreeItem)var4.getValue()).getMetaId());
      }

      var1.flush();
      return var1.toByteArray();
   }

   public void afterCreation(Statement var1) {
   }

   public void loadItems(Statement var1, Sector var2) throws SQLException, IOException {
      ResultSet var3;
      if ((var3 = var1.executeQuery("SELECT ITEMS FROM SECTORS_ITEMS WHERE ID = " + var2.getDBId() + ";")).next()) {
         var2.loadItems(var3.getBytes(1));
      }

   }

   public static void updateOrInsertItems(Statement var0, long var1, Map var3) throws SQLException, IOException {
      ResultSet var10000 = var0.executeQuery("SELECT ID FROM SECTORS_ITEMS WHERE ID = " + var1 + ";");
      PreparedStatement var4 = null;
      if (var10000.next()) {
         if (var3.isEmpty()) {
            var0.execute("DELETE FROM SECTORS_ITEMS WHERE ID = " + var1 + ";");
         } else {
            byte[] var6 = getItemBinaryString(var3);
            PreparedStatement var5;
            (var5 = var0.getConnection().prepareStatement("UPDATE SECTORS_ITEMS SET (ITEMS) = (CAST(? AS VARBINARY(22528))) WHERE ID = CAST(? AS BIGINT);")).setBytes(1, var6);
            var5.setLong(2, var1);
            var5.execute();
            var5.close();
         }
      } else {
         if (!var3.isEmpty()) {
            (var4 = var0.getConnection().prepareStatement("INSERT INTO SECTORS_ITEMS(ID, ITEMS) VALUES(CAST(? AS BIGINT),CAST(? AS VARBINARY(22528)));")).setLong(1, var1);
            var4.setBytes(2, getItemBinaryString(var3));
            var4.execute();
            var4.close();
         }

      }
   }
}
