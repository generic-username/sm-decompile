package org.schema.game.common.controller.database.tables;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class IdGenTable extends Table {
   public IdGenTable(TableManager var1, Connection var2) {
      super("ID_GEN_TABLE", var1, var2);
   }

   public void define() {
      this.addColumn("ID", "VARCHAR(128) not null", true);
      this.addColumn("ID_GEN", "BIGINT not null");
   }

   public void updateOrInsert(String var1, long var2) throws SQLException {
      Statement var4 = null;

      try {
         PreparedStatement var5;
         if (!(var4 = this.c.createStatement()).executeQuery("SELECT ID_GEN FROM " + this.table + " WHERE ID ='" + var1 + "'").next()) {
            (var5 = var4.getConnection().prepareStatement("INSERT INTO " + this.table + "(ID, ID_GEN) VALUES(CAST(? AS VARCHAR(128)),CAST(? AS BIGINT));")).setString(1, var1);
            var5.setLong(2, var2);
            var5.executeUpdate();
            var5.close();
         } else {
            (var5 = var4.getConnection().prepareStatement("UPDATE " + this.table + " SET(ID_GEN) = (CAST(? AS BIGINT)) WHERE ID = CAST(? AS VARCHAR(128));")).setLong(1, var2);
            var5.setString(2, var1);
            int var8 = var5.executeUpdate();

            assert var8 > 0;

            var5.close();
         }
      } finally {
         if (var4 != null) {
            var4.close();
         }

      }

   }

   public long getId(String var1) throws SQLException {
      Statement var2 = null;

      long var3;
      try {
         ResultSet var7;
         if (!(var7 = (var2 = this.c.createStatement()).executeQuery("SELECT ID_GEN FROM " + this.table + " WHERE ID ='" + var1 + "'")).next()) {
            return 0L;
         }

         var3 = var7.getLong(1);
      } finally {
         if (var2 != null) {
            var2.close();
         }

      }

      return var3;
   }

   public void afterCreation(Statement var1) {
   }
}
