package org.schema.game.common.controller.database.tables;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class TableManager {
   private final Connection c;
   private final SystemTable systemTable;
   private final EntityTable entityTable;
   private final FTLTable FTLTable;
   private final Map tables = new Object2ObjectOpenHashMap();
   private final List tableList = new ObjectArrayList();
   private final SectorItemTable sectorItemTable;
   private final SectorTable sectorTable;
   private final TradeHistoryTable tradeHistoryTable;
   private final TradeNodeTable tradeNodeTable;
   private final VisibilityTable visibilityTable;
   private final PlayerTable playerTable;
   private final FleetTable fleetTable;
   private final FleetMemberTable fleetMemberTable;
   private final EntityEffectTable entityEffectTable;
   private final NPCStatTable npcStatTable;
   private final PlayerMessagesTable playerMessagesTable;
   private final MinesTable minesTable;
   private final IdGenTable idGenTable;
   // $FF: synthetic field
   static final boolean $assertionsDisabled = !TableManager.class.desiredAssertionStatus();

   public TableManager(Connection var1) {
      this.c = var1;
      this.addTable(this.idGenTable = new IdGenTable(this, var1));
      this.addTable(this.playerTable = new PlayerTable(this, var1));
      this.addTable(this.systemTable = new SystemTable(this, var1));
      this.addTable(this.sectorTable = new SectorTable(this, var1));
      this.addTable(this.sectorItemTable = new SectorItemTable(this, var1));
      this.addTable(this.entityTable = new EntityTable(this, var1));
      this.addTable(this.entityEffectTable = new EntityEffectTable(this, var1));
      this.addTable(this.FTLTable = new FTLTable(this, var1));
      this.addTable(this.visibilityTable = new VisibilityTable(this, var1));
      this.addTable(this.fleetTable = new FleetTable(this, var1));
      this.addTable(this.fleetMemberTable = new FleetMemberTable(this, var1));
      this.addTable(this.tradeNodeTable = new TradeNodeTable(this, var1));
      this.addTable(this.tradeHistoryTable = new TradeHistoryTable(this, var1));
      this.addTable(this.npcStatTable = new NPCStatTable(this, var1));
      this.addTable(this.playerMessagesTable = new PlayerMessagesTable(this, var1));
      this.addTable(this.minesTable = new MinesTable(this, var1));
      ((ObjectArrayList)this.tableList).trim();
   }

   private void addTable(Table var1) {
      this.tables.put(var1.table, var1);
      this.tableList.add(var1);
   }

   public void create() throws SQLException {
      Iterator var1 = this.tableList.iterator();

      Table var2;
      do {
         if (!var1.hasNext()) {
            return;
         }

         var2 = (Table)var1.next();
         System.err.println("[DATABASE] Creating table: " + var2.table);
         var2.createTable();
      } while($assertionsDisabled || var2.existsTable());

      throw new AssertionError(var2.table);
   }

   public SystemTable getSystemTable() {
      return this.systemTable;
   }

   public EntityTable getEntityTable() {
      return this.entityTable;
   }

   public FTLTable getFTLTable() {
      return this.FTLTable;
   }

   public SectorItemTable getSectorItemTable() {
      return this.sectorItemTable;
   }

   public SectorTable getSectorTable() {
      return this.sectorTable;
   }

   public TradeHistoryTable getTradeHistoryTable() {
      return this.tradeHistoryTable;
   }

   public TradeNodeTable getTradeNodeTable() {
      return this.tradeNodeTable;
   }

   public VisibilityTable getVisibilityTable() {
      return this.visibilityTable;
   }

   public PlayerTable getPlayerTable() {
      return this.playerTable;
   }

   public FleetTable getFleetTable() {
      return this.fleetTable;
   }

   public FleetMemberTable getFleetMemberTable() {
      return this.fleetMemberTable;
   }

   public EntityEffectTable getEntityEffectTable() {
      return this.entityEffectTable;
   }

   public NPCStatTable getNpcStatTable() {
      return this.npcStatTable;
   }

   public PlayerMessagesTable getPlayerMessagesTable() {
      return this.playerMessagesTable;
   }

   public void migrate() throws SQLException {
      Iterator var1 = this.tableList.iterator();

      while(var1.hasNext()) {
         Table var2 = (Table)var1.next();
         Statement var3 = this.c.createStatement();
         if (!var2.existsTable()) {
            var2.createTable();
         }

         var2.migrate(var3);
         var3.close();
      }

   }

   public MinesTable getMinesTable() {
      return this.minesTable;
   }

   public IdGenTable getIdGenTable() {
      return this.idGenTable;
   }
}
