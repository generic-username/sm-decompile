package org.schema.game.common.controller.database.tables;

import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.controller.ShoppingAddOn;
import org.schema.game.common.controller.database.FogOfWarReceiver;
import org.schema.game.common.controller.trade.TradeManager;
import org.schema.game.common.controller.trade.TradeNode;
import org.schema.game.common.controller.trade.TradeNodeStub;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.network.objects.TradePrices;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.simulation.npc.NPCFaction;

public class TradeNodeTable extends Table {
   public static final int itemSize = 73732;

   public TradeNodeTable(TableManager var1, Connection var2) {
      super("TRADE_NODES", var1, var2);
   }

   public void define() {
      Statement var1;
      try {
         if ((var1 = this.c.createStatement()).executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'TRADES';").next()) {
            var1.execute("ALTER TABLE TRADES DROP CONSTRAINT ffATN;");
            var1.execute("ALTER TABLE TRADES DROP CONSTRAINT ffBTN;");
         }

         var1.close();
      } catch (SQLException var2) {
         var1 = null;
         var2.printStackTrace();
      }

      this.addColumn("ID", "BIGINT", true);
      this.addColumn("SEC_X", "INT NOT NULL");
      this.addColumn("SEC_Y", "INT NOT NULL");
      this.addColumn("SEC_Z", "INT NOT NULL");
      this.addColumn("PLAYER", "VARCHAR(128) not null");
      this.addColumn("STATION_NAME", "VARCHAR(128) not null");
      this.addColumn("FACTION", "INT NOT NULL");
      this.addColumn("PERMISSION", "BIGINT DEFAULT " + TradeManager.PERM_ALL_BUT_ENEMY + " not null");
      this.addColumn("ITEMS", "VARBINARY(73732) not null");
      this.addColumn("VOLUME", "DOUBLE NOT NULL");
      this.addColumn("CAPACITY", "DOUBLE NOT NULL");
      this.addColumn("CREDITS", "BIGINT NOT NULL");
      this.addIndex("facIndex", new String[]{"FACTION"});
      this.addIndex("playerIndex", new String[]{"PLAYER"});
      this.addIndex("trSysCoordIndex", new String[]{"SEC_X", "SEC_Y", "SEC_Z"});
   }

   public void setTradeCredits(long var1, long var3) throws SQLException {
      Statement var5;
      PreparedStatement var6;
      (var6 = (var5 = this.c.createStatement()).getConnection().prepareStatement("UPDATE TRADE_NODES SET (CREDITS) = (CAST(? AS BIGINT)) WHERE ID = CAST(? AS BIGINT);")).setDouble(1, (double)var3);
      var6.setLong(2, var1);
      var6.execute();
      var5.close();
   }

   public void setTradePrices(long var1, double var3, long var5, List var7) throws SQLException {
      Statement var8;
      PreparedStatement var9;
      (var9 = (var8 = this.c.createStatement()).getConnection().prepareStatement("UPDATE TRADE_NODES SET (VOLUME, CREDITS, ITEMS) = (CAST(? AS DOUBLE),CAST(? AS BIGINT),CAST(? AS VARBINARY(73732))) WHERE ID = CAST(? AS BIGINT);")).setDouble(1, var3);
      var9.setDouble(2, (double)var5);
      FastByteArrayOutputStream var11 = new FastByteArrayOutputStream(10240);
      DataOutputStream var4 = new DataOutputStream(var11);

      try {
         ShoppingAddOn.serializeTradePrices(var4, true, TradePrices.getFromPrices(var7, var1), var1);
      } catch (IOException var10) {
         throw new SQLException(var10);
      }

      byte[] var12 = Arrays.copyOf(var11.array, (int)var11.position());
      var9.setBytes(3, var12);
      var9.setLong(4, var1);
      var9.execute();
      var8.close();
   }

   public void setTradePrices(long var1, long var3, double var5, double var7, TradePrices var9) throws SQLException, IOException {
      PreparedStatement var10;
      (var10 = this.c.createStatement().getConnection().prepareStatement("UPDATE TRADE_NODES SET (ITEMS,VOLUME, CAPACITY, CREDITS ) = (CAST(? AS VARBINARY(73732)),CAST(? AS DOUBLE),CAST(? AS DOUBLE),CAST(? AS BIGINT)) WHERE ID = CAST(? AS BIGINT);")).setBytes(1, var9.getPricesBytesCompressed(true));
      var10.setDouble(2, var5);
      var10.setDouble(3, var7);
      var10.setLong(4, var3);
      var10.setLong(5, var1);
      var10.executeUpdate();
      var10.close();
   }

   public static void insertOrUpdateTradeNode(Statement var0, TradeNodeStub var1) throws SQLException, IOException {
      ResultSet var2;
      PreparedStatement var3;
      byte[] var4;
      if ((var2 = var0.executeQuery("SELECT ID FROM TRADE_NODES WHERE ID = " + var1.getEntityDBId() + ";")).next()) {
         var2.getLong(1);
         (var3 = var0.getConnection().prepareStatement("UPDATE TRADE_NODES SET (SEC_X, SEC_Y, SEC_Z, PLAYER, STATION_NAME, FACTION, PERMISSION, ITEMS,VOLUME, CAPACITY, CREDITS ) = (CAST(? AS INT),CAST(? AS INT),CAST(? AS INT),CAST(? AS VARCHAR(128)),CAST(? AS VARCHAR(128)),CAST(? AS INT),CAST(? AS BIGINT),CAST(? AS VARBINARY(73732)),CAST(? AS DOUBLE),CAST(? AS DOUBLE),CAST(? AS BIGINT)) WHERE ID = CAST(? AS BIGINT);")).setInt(1, var1.getSector().x);
         var3.setInt(2, var1.getSector().y);
         var3.setInt(3, var1.getSector().z);
         var3.setString(4, var1.getOwnerString());
         var3.setString(5, var1.getStationName());
         var3.setInt(6, var1.getFactionId());
         var3.setLong(7, var1.getTradePermission());
         var4 = new byte[var1.getPricesInputStream().available()];
         var1.getPricesInputStream().read(var4);
         var3.setBytes(8, var4);
         var3.setDouble(9, var1.getVolume());
         var3.setDouble(10, var1.getCapacity());
         var3.setLong(11, var1.getCredits());
         var3.setLong(12, var1.getEntityDBId());
         var3.executeUpdate();
         var3.close();
      } else {
         (var3 = var0.getConnection().prepareStatement("INSERT INTO TRADE_NODES(SEC_X, SEC_Y, SEC_Z, PLAYER, STATION_NAME, FACTION, PERMISSION, ITEMS, VOLUME, CAPACITY, CREDITS, ID) VALUES(CAST(? AS INT),CAST(? AS INT),CAST(? AS INT),CAST(? AS VARCHAR(128)),CAST(? AS VARCHAR(128)),CAST(? AS INT),CAST(? AS BIGINT),CAST(? AS VARBINARY(73732)), CAST(? AS DOUBLE),CAST(? AS DOUBLE),CAST(? AS BIGINT),CAST(? AS BIGINT));")).setInt(1, var1.getSector().x);
         var3.setInt(2, var1.getSector().y);
         var3.setInt(3, var1.getSector().z);
         var3.setString(4, var1.getOwnerString());
         var3.setString(5, var1.getStationName());
         var3.setInt(6, var1.getFactionId());
         var3.setLong(7, var1.getTradePermission());
         var4 = new byte[var1.getPricesInputStream().available()];
         var1.getPricesInputStream().read(var4);
         var3.setBytes(8, var4);
         var3.setDouble(9, var1.getVolume());
         var3.setDouble(10, var1.getCapacity());
         var3.setLong(11, var1.getCredits());
         var3.setLong(12, var1.getEntityDBId());
         var3.executeUpdate();
         var3.close();
      }
   }

   public void insertOrUpdateTradeNode(TradeNodeStub var1) throws SQLException, IOException {
      Statement var2;
      insertOrUpdateTradeNode(var2 = this.c.createStatement(), var1);
      var2.close();
   }

   public static void removeTradeNode(Statement var0, TradeNodeStub var1) throws SQLException {
      removeTradeNode(var0, var1.getEntityDBId());
   }

   public void removeTradeNode(TradeNodeStub var1) throws SQLException {
      this.removeTradeNode(var1.getEntityDBId());
   }

   public static void removeTradeNode(Statement var0, long var1) throws SQLException {
      if (var0.executeQuery("SELECT ID FROM TRADE_NODES WHERE ID = " + var1 + ";").next()) {
         try {
            throw new Exception("[SERVER][DATABASE] REMOVE TRADENODE: " + var1 + " (Not an error just a trace just in case this shouldn't have happened)");
         } catch (Exception var3) {
            var3.printStackTrace();
            var0.executeUpdate("DELETE FROM TRADE_NODES WHERE ID = " + var1 + ";");
         }
      }

   }

   public List getTradeNodes(int var1, int var2, int var3, int var4, int var5, int var6) throws SQLException {
      ObjectArrayList var7 = new ObjectArrayList();

      ResultSet var10;
      try {
         Statement var8;
         var10 = (var8 = this.c.createStatement()).executeQuery("SELECT SEC_X, SEC_Y, SEC_Z, PLAYER, STATION_NAME, FACTION, PERMISSION, VOLUME, CAPACITY, CREDITS, ID FROM TRADE_NODES WHERE SEC_X >= " + var1 + " AND SEC_X <= " + var4 + " AND SEC_Y >= " + var2 + " AND SEC_Y <= " + var5 + " AND SEC_Z >= " + var3 + " AND SEC_Z <= " + var6 + ";");

         while(var10.next()) {
            var7.add(getTradeNodeStubFromResult(var10.getLong(11), var10, (TradeNodeStub)null));
         }

         var8.close();
      } catch (SQLException var9) {
         var10 = null;
         var9.printStackTrace();
      }

      return var7;
   }

   public TradeNodeStub getTradeNode(long var1) throws SQLException {
      Statement var3;
      TradeNodeStub var4 = getTradeNode(var3 = this.c.createStatement(), var1, (TradeNodeStub)null);
      var3.close();
      return var4;
   }

   public void getTradeNode(long var1, TradeNodeStub var3) throws SQLException {
      Statement var4;
      getTradeNode(var4 = this.c.createStatement(), var1, var3);
      var4.close();
   }

   public static TradeNodeStub getTradeNode(Statement var0, long var1) throws SQLException {
      return getTradeNode(var0, var1, (TradeNodeStub)null);
   }

   public static TradeNodeStub getTradeNode(Statement var0, long var1, TradeNodeStub var3) throws SQLException {
      ResultSet var4;
      return (var4 = var0.executeQuery("SELECT SEC_X, SEC_Y, SEC_Z, PLAYER, STATION_NAME, FACTION, PERMISSION, VOLUME, CAPACITY, CREDITS FROM TRADE_NODES WHERE ID = " + var1 + ";")).next() ? getTradeNodeStubFromResult(var1, var4, var3) : null;
   }

   static void writeTradeNode(Statement var0, GameServerState var1, Vector3i var2, ShopInterface var3) throws SQLException {
      long var4;
      if ((var4 = var3.getSegmentBuffer().getSegmentController().dbId) > 0L) {
         TradeNodeStub var6 = getTradeNode(var0, var4);
         boolean var7 = false;
         if (var6 != null && !var3.isTradeNode()) {
            System.err.println("[SERVER][DATABASE] REMOVING TRADENODE. ShopInterface " + var3 + " is not a tradenode: removing: " + var4);
            removeTradeNode(var0, var4);
            var7 = true;
         } else if (var3 != null && var3.isTradeNode()) {
            if (!var3.isValidShop()) {
               if (FactionManager.isNPCFaction(var3.getFactionId())) {
                  Faction var11 = var1.getFactionManager().getFaction(var3.getFactionId());
                  System.err.println("FACT " + var11);
                  if (var11 != null) {
                     String var12 = var3.getSegmentController().getType().dbPrefix + ((NPCFaction)var11).getHomebaseUID();
                     System.err.println("FACT " + var11 + ": " + var3.getSegmentController().getUniqueIdentifier() + " ---> " + var12);
                  }

                  try {
                     throw new Exception("TRIED TO REMOVE NPC SHOP: " + var3.getSegmentController() + "; ");
                  } catch (Exception var10) {
                     var10.printStackTrace();

                     assert false;
                  }
               }

               if (var3.wasValidTradeNode()) {
                  removeTradeNode(var0, var3.getSegmentController().getDbId());
               }
            } else {
               try {
                  insertOrUpdateTradeNode(var0, var3.getTradeNode());
               } catch (IOException var9) {
                  var9.printStackTrace();
               }
            }

            var7 = true;
         }

         if (var7) {
            VoidSystem.getContainingSystem(var2, new Vector3i());
            synchronized(var1.getUniverse().tradeNodesDirty) {
               var1.getUniverse().tradeNodesDirty.enqueue(var4);
               return;
            }
         }
      }

   }

   private static TradeNodeStub getTradeNodeStubFromResult(long var0, ResultSet var2, TradeNodeStub var3) throws SQLException {
      if (var3 == null) {
         var3 = new TradeNodeStub();
      }

      var3.setEntityDBId(var0);
      var3.setSector(new Vector3i(var2.getInt(1), var2.getInt(2), var2.getInt(3)));
      var3.parseOwnerString(var2.getString(4));
      var3.setStationName(var2.getString(5));
      var3.setFactionId(var2.getInt(6));
      var3.setTradePermission(var2.getLong(7));
      var3.setVolume(var2.getDouble(8));
      var3.setCapacity(var2.getDouble(9));
      var3.setCredits(var2.getLong(10));
      return var3;
   }

   public TradeNodeStub updateTradeNode(long var1, TradeNode var3) throws SQLException {
      Statement var4 = null;

      try {
         TradeNodeStub var9 = getTradeNode(var4 = this.c.createStatement(), var1, var3);
         return var9;
      } catch (SQLException var7) {
         var7.printStackTrace();
      } finally {
         if (var4 != null) {
            var4.close();
         }

      }

      return null;
   }

   public void removeTradeNode(long var1) {
      try {
         Statement var3;
         removeTradeNode(var3 = this.c.createStatement(), var1);
         var3.close();
      } catch (SQLException var4) {
         var4.printStackTrace();
      }
   }

   public void fillTradeNodeStubData(Object2ObjectOpenHashMap var1, Long2ObjectOpenHashMap var2) {
      new Vector3i();

      try {
         Statement var3 = this.c.createStatement();
         Statement var4 = this.c.createStatement();
         ResultSet var5 = var3.executeQuery("SELECT SEC_X, SEC_Y, SEC_Z, PLAYER, STATION_NAME, FACTION, PERMISSION, VOLUME, CAPACITY, CREDITS, ID FROM TRADE_NODES;");

         while(var5.next()) {
            TradeNodeStub var6 = getTradeNodeStubFromResult(var5.getLong(11), var5, (TradeNodeStub)null);
            if (var4.executeQuery("SELECT ID FROM ENTITIES WHERE ID = " + var6.getEntityDBId() + ";").next()) {
               var2.put(var6.getEntityDBId(), var6);
               Object var7;
               if ((var7 = (List)var1.get(var6.getSystem())) == null) {
                  var7 = new ObjectArrayList();
                  var1.put(new Vector3i(var6.getSystem()), var7);
               }

               ((List)var7).add(var6);
            } else {
               System.err.println("[DATABADE] Exception; Trade Node had no attached entity: removing");
               this.removeTradeNode(var6);
            }
         }

         var3.close();
         var4.close();
      } catch (SQLException var8) {
         var8.printStackTrace();
      }
   }

   public List getTradeNodesBySystem(Vector3i var1) throws SQLException {
      return this.getTradeNodes(var1.x << 4, var1.y << 4, var1.z << 4, (var1.x << 4) + 16 - 1, (var1.y << 4) + 16 - 1, (var1.z << 4) + 16 - 1);
   }

   public List getTradePricesAsList(long var1) {
      TradePrices var3;
      return (var3 = this.getTradePrices(var1)) == null ? null : var3.getPrices();
   }

   public TradePrices getTradePrices(long var1) {
      DataInputStream var4;
      try {
         if ((var4 = this.getTradePricesAsStream(var1)) != null) {
            return ShoppingAddOn.deserializeTradePrices(var4, true);
         }
      } catch (IOException var3) {
         var4 = null;
         var3.printStackTrace();
      }

      return null;
   }

   public DataInputStream getTradePricesAsStream(long var1) {
      DataInputStream var3 = null;
      Statement var4 = null;

      try {
         ResultSet var13;
         try {
            if ((var13 = (var4 = this.c.createStatement()).executeQuery("SELECT ITEMS FROM TRADE_NODES WHERE ID = " + var1 + ";")).next()) {
               var3 = new DataInputStream(new FastByteArrayInputStream(var13.getBytes(1)));
            }
         } catch (SQLException var11) {
            var13 = null;
            var11.printStackTrace();
         }
      } finally {
         try {
            var4.close();
         } catch (Exception var10) {
            var10.printStackTrace();
         }

      }

      return var3;
   }

   public DataInputStream switchTradePrices() {
      DataInputStream var1 = null;

      ResultSet var3;
      try {
         Statement var2;
         var3 = (var2 = this.c.createStatement()).executeQuery("SELECT ID, ITEMS FROM TRADE_NODES;");

         while(var3.next()) {
            long var4 = var3.getLong(1);
            System.err.println("[MIGRATION] SWITCHING TRADE PRICES OF TRADE ID: " + var4);
            TradePrices var6 = ShoppingAddOn.deserializeTradePrices(var1 = new DataInputStream(new FastByteArrayInputStream(var3.getBytes(2))), true).switchBuyAndSell();
            PreparedStatement var7 = var2.getConnection().prepareStatement("UPDATE TRADE_NODES SET(ITEMS) = (CAST(? AS VARBINARY(73732))) WHERE ID = CAST(? AS BIGINT);");
            FastByteArrayOutputStream var8 = new FastByteArrayOutputStream(10240);
            DataOutputStream var9 = new DataOutputStream(var8);

            try {
               ShoppingAddOn.serializeTradePrices(var9, true, var6, var4);
            } catch (IOException var10) {
               throw new SQLException(var10);
            }

            byte[] var13 = Arrays.copyOf(var8.array, (int)var8.position());
            var7.setBytes(1, var13);
            var7.setLong(2, var4);
            var7.executeUpdate();
         }

         var2.close();
      } catch (SQLException var11) {
         var3 = null;
         var11.printStackTrace();
      } catch (IOException var12) {
         var3 = null;
         var12.printStackTrace();
      }

      return var1;
   }

   public long getTradeNodeCredits(long var1) {
      long var3 = -1L;

      ResultSet var7;
      try {
         Statement var5;
         if ((var7 = (var5 = this.c.createStatement()).executeQuery("SELECT CREDITS FROM TRADE_NODES WHERE ID = " + var1 + ";")).next()) {
            var3 = var7.getLong(1);
         }

         var5.close();
      } catch (SQLException var6) {
         var7 = null;
         var6.printStackTrace();
      }

      return var3;
   }

   public boolean isVisibileSystem(FogOfWarReceiver var1, int var2, int var3, int var4) throws SQLException {
      Statement var5 = null;

      try {
         boolean var10 = (var5 = this.c.createStatement()).executeQuery("SELECT ID FROM VISIBILITY WHERE ID = " + var1.getFogOfWarId() + " AND X = " + var2 + " AND Y = " + var3 + " AND Z = " + var4 + ";").next();
         return var10;
      } catch (SQLException var8) {
         var8.printStackTrace();
      } finally {
         if (var5 != null) {
            var5.close();
         }

      }

      return false;
   }

   @Deprecated
   public void createTradeNodeTable() throws SQLException {
      Statement var1;
      if ((var1 = this.c.createStatement()).executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'TRADES';").next()) {
         var1.execute("ALTER TABLE TRADES DROP CONSTRAINT ffATN;");
         var1.execute("ALTER TABLE TRADES DROP CONSTRAINT ffBTN;");
      }

      var1.execute("DROP TABLE TRADE_NODES if exists;");
      var1.execute("CREATE CACHED TABLE TRADE_NODES(BIGINT, SEC_X INT not null, SEC_Y INT not null, SEC_Z INT not null, PLAYER VARCHAR(128) not null, STATION_NAME VARCHAR(128) not null, FACTION INT not null, PERMISSION BIGINT DEFAULT " + TradeManager.PERM_ALL_BUT_ENEMY + " not null, ITEMS VARBINARY(73732) not null, VOLUME DOUBLE not null, CAPACITY DOUBLE not null, CREDITS BIGINT not null, primary key (ID));");
      var1.execute("create index facIndex on TRADE_NODES(FACTION);");
      var1.execute("create index playerIndex on TRADE_NODES(PLAYER);");
      var1.execute("create index trSysCoordIndex on TRADE_NODES(SEC_X,SEC_Y,SEC_Z);");
      var1.close();
   }

   public void afterCreation(Statement var1) {
   }
}
