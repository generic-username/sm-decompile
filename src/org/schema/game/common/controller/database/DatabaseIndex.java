package org.schema.game.common.controller.database;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.PercentCallbackInterface;
import org.schema.game.common.controller.database.tables.EntityTable;
import org.schema.game.common.controller.database.tables.SectorTable;
import org.schema.game.common.controller.database.tables.SystemTable;
import org.schema.game.common.controller.database.tables.TableManager;
import org.schema.game.common.controller.generator.AsteroidCreatorThread;
import org.schema.game.common.data.player.inventory.FreeItem;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.network.RegisteredClientInterface;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.tag.Tag;

public class DatabaseIndex {
   private static final Object dbPath = new Object() {
      public final String toString() {
         return GameServerState.ENTITY_DATABASE_PATH + "index" + File.separator;
      }
   };
   private static final HashMap sqlTokens;
   public static final int UID_LENGTH = 128;
   private static Pattern sqlTokenPattern;
   final Connection c;
   private HashMap sqlTokensBack;
   private Pattern sqlTokenPatternBack;
   private final TableManager tableManager;

   public DatabaseIndex() throws SQLException {
      String[][] var1 = new String[][]{{"\u0000", "\\x00", "\\\\0"}, {"'", "'", "\\\\'"}, {"\"", "\"", "\\\\\""}, {"\b", "\\x08", "\\\\b"}, {"\n", "\\n", "\\\\n"}, {"\r", "\\r", "\\\\r"}, {"\t", "\\t", "\\\\t"}, {"\u001a", "\\x1A", "\\\\Z"}, {"\\", "\\\\", "\\\\\\\\"}};
      this.sqlTokensBack = new HashMap();
      String var2 = "";
      int var3 = (var1 = var1).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         String[] var5 = var1[var4];
         sqlTokens.put(var5[0], var5[2]);
         var2 = var2 + (var2.isEmpty() ? "" : "|") + var5[1];
      }

      this.sqlTokenPatternBack = Pattern.compile("(" + var2 + ')');
      System.err.println("[SQL] Fetching connection (may take some if game crashed on the run before)");
      if (FastMath.isPowerOfTwo((Integer)ServerConfig.SQL_NIO_FILE_SIZE.getCurrentState())) {
         String var6 = ";hsqldb.nio_max_size=" + ServerConfig.SQL_NIO_FILE_SIZE.getCurrentState() + ";";
         this.c = DriverManager.getConnection("jdbc:hsqldb:file:" + getDbPath() + ";shutdown=true" + var6, "SA", "");
         System.err.println("[SQL] connection successfull");

         assert !this.c.isClosed();

         assert this.c.isValid(30);

         this.setMVCCMode();
         this.setDbNIOFileSizeControl((Integer)ServerConfig.SQL_NIO_FILE_SIZE.getCurrentState());
         this.tableManager = new TableManager(this.c);
      } else {
         throw new SQLException("server.cfg: SQL_NIO_FILE_SIZE must be power of two (256, 512, 1024,...), but is: " + ServerConfig.SQL_NIO_FILE_SIZE.getCurrentState());
      }
   }

   public static String getDbPath() {
      return dbPath.toString();
   }

   public static boolean existsDB() {
      return (new FileExt(getDbPath())).exists();
   }

   public static void main(String[] var0) throws SQLException, IOException {
      GameServerState.readDatabasePosition(false);
      (new DatabaseIndex()).display("SELECT * FROM EFFECTS");
   }

   public static void registerDriver() {
      try {
         Class.forName("org.hsqldb.jdbc.JDBCDriver");
      } catch (Exception var1) {
         System.err.println("ERROR: failed to load HSQLDB JDBC driver.");
         var1.printStackTrace();
      }
   }

   public static DefaultTableModel resultSetToTableModel(DefaultTableModel var0, ResultSet var1) throws SQLException {
      ResultSetMetaData var2 = var1.getMetaData();
      if (var0 == null) {
         var0 = new DefaultTableModel();
      }

      String[] var3 = new String[var2.getColumnCount()];

      for(int var4 = 0; var4 < var3.length; ++var4) {
         var3[var4] = var2.getColumnLabel(var4 + 1);
      }

      var0.setColumnIdentifiers(var3);

      while(var1.next()) {
         Object[] var6 = new Object[var3.length];

         for(int var5 = 0; var5 < var6.length; ++var5) {
            var6[var5] = var1.getObject(var5 + 1);
         }

         var0.addRow(var6);
      }

      return var0;
   }

   public static void updateFromFile(File var0, Statement var1) throws IOException, SQLException {
      if (var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.SHOP.dbPrefix) || var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.SPACE_STATION.dbPrefix) || var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.ASTEROID_MANAGED.dbPrefix) || var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.ASTEROID.dbPrefix) || var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT.dbPrefix) || var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.SHIP.dbPrefix)) {
         DataInputStream var2;
         Tag var3 = Tag.readFrom(var2 = new DataInputStream(new FileInputStream(var0)), true, false);
         var2.close();
         Tag var16;
         if (var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.SHOP.dbPrefix)) {
            var16 = ((Tag[])var3.getValue())[0];
         } else if (!var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.SPACE_STATION.dbPrefix) && !var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.ASTEROID_MANAGED.dbPrefix)) {
            if (var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT.dbPrefix)) {
               System.err.println("PLANET TAGS: " + ((Tag[])var3.getValue())[0].getValue() + "; " + ((Tag[])var3.getValue())[1].getValue() + "; " + ((Tag[])var3.getValue())[2].getValue());
               var16 = ((Tag[])var3.getValue())[2];
            } else {
               var16 = var3;
            }
         } else {
            var16 = ((Tag[])var3.getValue())[1];
         }

         int var17 = -1;
         SimpleTransformableSendableObject.EntityType[] var4;
         int var5 = (var4 = SimpleTransformableSendableObject.EntityType.values()).length;

         for(int var6 = 0; var6 < var5; ++var6) {
            SimpleTransformableSendableObject.EntityType var7 = var4[var6];
            if (var0.getName().startsWith(var7.dbPrefix)) {
               var17 = var7.dbTypeId;
               break;
            }
         }

         assert var17 >= 0;

         System.err.println("PARSING: " + var0.getName() + " -> " + var17 + "; tagVAL: " + var16.getValue());
         Tag[] var19;
         Tag[] var21 = (Tag[])(var19 = (Tag[])var16.getValue())[6].getValue();
         String var23 = (String)var19[0].getValue();
         Vector3i var24 = (Vector3i)var21[3].getValue();
         long var10;
         if (var19.length > 11 && var19[11].getType() == Tag.Type.LONG) {
            var10 = (Long)var19[11].getValue();
         } else {
            var10 = 0L;
         }

         String var15;
         if (var19.length > 10 && var19[10].getType() == Tag.Type.STRING) {
            var15 = (String)var19[10].getValue();
         } else {
            var15 = "";
         }

         if (var15 == null) {
            var15 = "";
         }

         if (var15.startsWith("ENTITY_PLAYERSTATE_")) {
            var15 = var15.substring(19);
         }

         String var18;
         if (var19.length > 9 && var19[9].getType() == Tag.Type.STRING) {
            var18 = (String)var19[9].getValue();
         } else {
            var18 = "";
         }

         if (var18 == null) {
            var18 = "";
         }

         if (var18.startsWith("ENTITY_PLAYERSTATE_")) {
            var18 = var18.substring(19);
         }

         boolean var8 = true;
         if (var19.length > 12 && var19[12].getValue() != null && var19[12].getType() == Tag.Type.BYTE) {
            var8 = (Byte)var19[12].getValue() == 1;
         }

         String var9;
         if (var19[5].getValue() != null) {
            var9 = (String)var19[5].getValue();
         } else {
            var9 = "undef";
         }

         int var12 = 0;
         if (var21[4].getValue() != null && var21[4].getType() == Tag.Type.INT) {
            var12 = (Integer)var21[4].getValue();
            System.err.println("PARSED FACTION " + var12);
         } else {
            System.err.println("COULD NOT PARSE FACTION " + var21[4].getType().name());
         }

         float[] var13 = new float[(var21 = (Tag[])var21[1].getValue()).length];

         for(int var14 = 0; var14 < var21.length; ++var14) {
            var13[var14] = (Float)var21[var14].getValue();
         }

         Transform var26;
         (var26 = new Transform()).setFromOpenGLMatrix(var13);
         Vector3i var22 = (Vector3i)var19[1].getValue();
         Vector3i var25 = (Vector3i)var19[2].getValue();
         int var20 = (Integer)var19[8].getValue();
         EntityTable.updateOrInsertSegmentController(var1, var23.split("_", 3)[2], var24, var17, var10, var15, var18, var9, var8, var12, var26.origin, var22, var25, var20, -1L, -1L, false, false);
      }

   }

   public static void updateFromFileSector(File var0, Statement var1) throws IOException, SQLException {
      if (var0.getName().startsWith("SECTOR")) {
         DataInputStream var7;
         Tag var2 = Tag.readFrom(var7 = new DataInputStream(new FileInputStream(var0)), true, false);
         var7.close();
         HashMap var3 = new HashMap();
         Tag[] var4;
         Vector3i var8 = (Vector3i)(var4 = (Tag[])var2.getValue())[0].getValue();

         assert var8 != null;

         int var9 = (Integer)var4[2].getValue();
         var4 = (Tag[])var4[3].getValue();

         for(int var5 = 0; var5 < var4.length - 1; ++var5) {
            FreeItem var6;
            (var6 = new FreeItem()).fromTagStructure(var4[var5], (GameServerState)null);
            var6.setId(GameServerState.itemIds++);
            if (var6.getType() != 0) {
               var3.put(var6.getId(), var6);
            }
         }

         SectorTable.updateOrInsertSector(var1.getConnection(), -1L, var8, var9, var3, 0, -1L, false, 0L);
         System.err.println("[SQL] INSERTED SECTOR " + var8);
      }

   }

   public static String unscape(String var0) {
      Matcher var2 = sqlTokenPattern.matcher(var0);
      StringBuffer var1 = new StringBuffer();

      while(var2.find()) {
         var2.appendReplacement(var1, (String)sqlTokens.get(var2.group(1)));
      }

      var2.appendTail(var1);
      return var1.toString();
   }

   public static String escape(String var0) {
      Matcher var2 = sqlTokenPattern.matcher(var0);
      StringBuffer var1 = new StringBuffer();

      while(var2.find()) {
         var2.appendReplacement(var1, (String)sqlTokens.get(var2.group(1)));
      }

      var2.appendTail(var1);
      return var1.toString();
   }

   public static boolean isUsernameOk(String var0) {
      if (var0.length() <= 0) {
         return false;
      } else if (var0.length() > 32) {
         return false;
      } else if (var0.length() <= 2) {
         return false;
      } else if (var0.matches("[_-]+")) {
         return false;
      } else {
         return var0.matches("[a-zA-Z0-9_-]+");
      }
   }

   public void test(String var1) throws SQLException {
      System.err.println("START Query: " + var1);
      long var2 = System.currentTimeMillis();
      Statement var4;
      ResultSet var5 = (var4 = this.c.createStatement()).executeQuery(var1);
      var4.close();
      long var6 = System.currentTimeMillis() - var2;
      System.err.println("Query took " + var6 + ": " + var1 + "; ");
      int var8 = 0;

      while(var5.next()) {
         ++var8;
         var5.getLong(1);
      }

      System.err.println("DONE Query took " + var6 + ": " + var1 + "; count#" + var8);
   }

   public void checkDatabase() throws SQLException, IOException {
      if (!existsDB()) {
         this.createDatabase();
         this.fillCompleteDatabase();
      }

   }

   public void commit() throws SQLException {
      this.c.commit();
   }

   public void createDatabase() throws SQLException {
      this.getTableManager().create();
      FileExt var1 = new FileExt(GameServerState.DATABASE_PATH + "version");

      try {
         BufferedWriter var3 = new BufferedWriter(new FileWriter(var1));
         Galaxy.USE_GALAXY = true;
         var3.append("2");
         var3.close();
      } catch (IOException var2) {
         var2.printStackTrace();
      }
   }

   public void destroy() throws SQLException {
      Statement var1;
      (var1 = this.c.createStatement()).execute("SHUTDOWN COMPACT;");
      var1.close();
      this.c.close();
   }

   public void display(String var1) throws SQLException {
      Statement var2;
      ResultSet var3 = (var2 = this.c.createStatement()).executeQuery(var1);
      var2.close();
      final JFrame var4;
      (var4 = new JFrame()).setDefaultCloseOperation(3);
      var4.setSize(1200, 700);
      var4.setContentPane(new JScrollPane(new JTable(resultSetToTableModel(new DefaultTableModel(), var3))));
      SwingUtilities.invokeLater(new Runnable() {
         public void run() {
            var4.setVisible(true);
         }
      });
      var3.close();
   }

   public void fillCompleteDatabase() throws IOException, SQLException {
      this.fillCompleteDatabase((PercentCallbackInterface)null);
   }

   public void fillCompleteDatabase(PercentCallbackInterface var1) throws IOException, SQLException {
      File[] var2 = (new FileExt(GameServerState.DATABASE_PATH)).listFiles();
      Statement var3 = this.c.createStatement();
      int var4 = var2.length;
      this.fillSystemsTable(var2, var3, var1, var4);
      this.fillSectorsTable(var2, var3, var1, var4);
      this.fillEntitiesTable(var2, var3, var1, var4);
      File[] var5 = (new FileExt(GameServerState.DATABASE_PATH)).listFiles(new FilenameFilter() {
         public boolean accept(File var1, String var2) {
            return var2.startsWith("SECTOR") || var2.startsWith("VOIDSYSTEM");
         }
      });

      for(int var6 = 0; var6 < var5.length; ++var6) {
         System.err.println("[MIGRATION] CLEANING UP DEPRECATED FILE: " + var5[var6].getName());
         var5[var6].delete();
      }

      var3.close();
   }

   public void fillEntitiesTable(File[] var1, Statement var2, PercentCallbackInterface var3, int var4) throws IOException, SQLException {
      int var5 = 0;
      int var6 = (var1 = var1).length;

      for(int var7 = 0; var7 < var6; ++var7) {
         File var8;
         updateFromFile(var8 = var1[var7], var2);
         if (var3 != null) {
            var3.update((int)((float)var5 / (float)var4 * 100.0F) + "%  (" + var8.getName() + ")");
         }

         ++var5;
      }

   }

   public void fillSectorsTable(File[] var1, Statement var2, PercentCallbackInterface var3, int var4) throws IOException, SQLException {
      int var5 = 0;
      int var6 = (var1 = var1).length;

      for(int var7 = 0; var7 < var6; ++var7) {
         File var8;
         updateFromFileSector(var8 = var1[var7], var2);
         if (var3 != null) {
            var3.update((int)((float)var5 / (float)var4 * 100.0F) + "%  (" + var8.getName() + ")");
         }

         ++var5;
      }

   }

   public void fillSystemsTable(File[] var1, Statement var2, PercentCallbackInterface var3, int var4) throws IOException, SQLException {
      int var5 = 0;
      int var6 = (var1 = var1).length;

      for(int var7 = 0; var7 < var6; ++var7) {
         File var8;
         SystemTable.updateFromFileStarSystem(var8 = var1[var7], var2);
         if (var3 != null) {
            var3.update((int)((float)var5 / (float)var4 * 100.0F) + "%  (" + var8.getName() + ")");
         }

         ++var5;
      }

   }

   public void migrateUIDFieldSize() throws SQLException {
      Statement var1;
      ResultSet var2;
      int var3;
      if ((var2 = (var1 = this.c.createStatement()).executeQuery("SELECT CHARACTER_MAXIMUM_LENGTH FROM information_schema.COLUMNS WHERE TABLE_NAME = 'ENTITIES' AND COLUMN_NAME = 'UID'")).next()) {
         var3 = var2.getInt(1);
         System.err.println("[DATABASE] ENTITIES UID SIZE: " + var3);
         if (var3 == 64) {
            var1.executeUpdate("ALTER TABLE 'ENTITIES' ALTER COLUMN 'UID' SET DATA TYPE VARCHAR(128)");
         }
      } else {
         assert false;
      }

      var1.close();
      if ((var2 = (var1 = this.c.createStatement()).executeQuery("SELECT CHARACTER_MAXIMUM_LENGTH FROM information_schema.COLUMNS WHERE TABLE_NAME = 'FTL' AND COLUMN_NAME = 'FROM_UID'")).next()) {
         var3 = var2.getInt(1);
         System.err.println("[DATABASE] FTL UID SIZE: " + var3);
         if (var3 == 64) {
            var1.executeUpdate("ALTER TABLE 'FTL' ALTER COLUMN 'FROM_UID' SET DATA TYPE VARCHAR(128)");
            var1.executeUpdate("ALTER TABLE 'FTL' ALTER COLUMN 'TO_UID' SET DATA TYPE VARCHAR(128)");
         }
      } else {
         assert false;
      }

      var1.close();
      if ((var2 = (var1 = this.c.createStatement()).executeQuery("SELECT CHARACTER_MAXIMUM_LENGTH FROM information_schema.COLUMNS WHERE TABLE_NAME = 'SYSTEMS' AND COLUMN_NAME = 'OWNER_UID'")).next()) {
         var3 = var2.getInt(1);
         System.err.println("[DATABASE] SYSTEMS UID SIZE: " + var3);
         if (var3 == 64) {
            var1.executeUpdate("ALTER TABLE 'SYSTEMS' ALTER COLUMN 'OWNER_UID' SET DATA TYPE VARCHAR(128)");
         }
      } else {
         assert false;
      }

      var1.close();
   }

   public void migrateAddSectorAsteroidsTouched() throws SQLException {
      Statement var1;
      if (!(var1 = this.c.createStatement()).executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'SECTORS' AND COLUMN_NAME = 'TRANSIENT'").next()) {
         System.err.println("[SQL] Database migration needed: TRANSIENT (this can take a little) [DO NOT KILL THE PROCESS]");
         var1.executeUpdate("ALTER TABLE SECTORS ADD COLUMN TRANSIENT BOOLEAN DEFAULT true not null");
         var1.executeUpdate("DELETE FROM ENTITIES WHERE TYPE = 3 AND TOUCHED = FALSE;");
      }

      var1.close();
      if (!(var1 = this.c.createStatement()).executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'SECTORS' AND COLUMN_NAME = 'LAST_REPLENISHED'").next()) {
         System.err.println("[SQL] Database migration needed: LAST_REPLENISHED (this can take a little) [DO NOT KILL THE PROCESS]");
         var1.executeUpdate("ALTER TABLE SECTORS ADD COLUMN LAST_REPLENISHED BIGINT DEFAULT 0 not null");
      }

      var1.close();
   }

   public void migrateAddFields() throws SQLException {
      Statement var1 = this.c.createStatement();
      this.c.getMetaData();
      ResultSet var10000 = var1.executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'ENTITIES' AND COLUMN_NAME = 'GEN_ID'");
      ResultSet var2 = null;
      if (!var10000.next()) {
         System.err.println("[SQL] Database migration needed: GEN_ID");
         var1.executeUpdate("ALTER TABLE ENTITIES ADD COLUMN GEN_ID INT");
         var2 = var1.executeQuery("SELECT UID FROM ENTITIES WHERE TYPE = 3 AND TOUCHED = FALSE;");
         Random var3 = new Random();

         while(var2.next()) {
            var1.executeUpdate("UPDATE ENTITIES SET GEN_ID = " + var3.nextInt(AsteroidCreatorThread.AsteroidType.values().length) + " WHERE TYPE = 3 AND UID = '" + var2.getString(1) + "';");
         }
      }

      if (!var1.executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'SYSTEMS' AND COLUMN_NAME = 'OWNER_UID'").next()) {
         System.err.println("[SQL] Database migration needed: SYSTEMS Owner colums");
         var1.executeUpdate("ALTER TABLE SYSTEMS ADD COLUMN OWNER_UID VARCHAR(128)");
         var1.executeUpdate("ALTER TABLE SYSTEMS ADD COLUMN OWNER_FACTION INT DEFAULT 0 not null;");
         var1.executeUpdate("ALTER TABLE SYSTEMS ADD COLUMN OWNER_X INT DEFAULT 0 not null;");
         var1.executeUpdate("ALTER TABLE SYSTEMS ADD COLUMN OWNER_Y INT DEFAULT 0 not null;");
         var1.executeUpdate("ALTER TABLE SYSTEMS ADD COLUMN OWNER_Z INT DEFAULT 0 not null;");
         var1.execute("create index sysOwnFacIndex on SYSTEMS(OWNER_FACTION);");
         var1.execute("create index sysOwnUIDIndex on SYSTEMS(OWNER_UID);");
      }

      if (!var1.executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'ENTITIES' AND COLUMN_NAME = 'DOCKED_ROOT'").next()) {
         System.err.println("[SQL] Database migration needed: ENTITIES :: DOCKED_ROOT");
         var1.executeUpdate("ALTER TABLE ENTITIES ADD COLUMN DOCKED_ROOT BIGINT DEFAULT -1;");
         var1.execute("create index dockedRootIndex on ENTITIES(DOCKED_ROOT);");
      }

      if (!var1.executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'ENTITIES' AND COLUMN_NAME = 'DOCKED_TO'").next()) {
         System.err.println("[SQL] Database migration needed: ENTITIES :: DOCKED_TO");
         var1.executeUpdate("ALTER TABLE ENTITIES ADD COLUMN DOCKED_TO BIGINT DEFAULT -1;");
         var1.execute("create index dockedToIndex on ENTITIES(DOCKED_TO);");
      }

      if (!var1.executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'ENTITIES' AND COLUMN_NAME = 'SPAWNED_ONLY_IN_DB'").next()) {
         System.err.println("[SQL] Database migration needed: ENTITIES :: SPAWNED_ONLY_IN_DB");
         var1.executeUpdate("ALTER TABLE ENTITIES ADD COLUMN SPAWNED_ONLY_IN_DB BOOLEAN DEFAULT FALSE;");
      }

      if (!var1.executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'FLEET_MEMBERS' AND COLUMN_NAME = 'FACTION'").next()) {
         System.err.println("[SQL] Database migration needed: FLEET_MEMBERS :: FACTION");
         var1.executeUpdate("ALTER TABLE FLEET_MEMBERS ADD COLUMN FACTION INT DEFAULT 0;");
         var1.execute("create index ddEA on FLEET_MEMBERS(FACTION);");
      }

      if (!var1.executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'ENTITIES' AND COLUMN_NAME = 'ID'").next()) {
         System.err.println("[SQL] Database migration needed: ID");
         var1.executeUpdate("ALTER TABLE ENTITIES DROP PRIMARY KEY");
         var1.executeUpdate("ALTER TABLE ENTITIES ADD COLUMN ID BIGINT GENERATED BY DEFAULT AS IDENTITY (START WITH 1 INCREMENT BY 1)");
         var2 = var1.executeQuery("SELECT UID,TYPE FROM ENTITIES;");

         long var5;
         for(var5 = 0L; var2.next(); ++var5) {
            var1.executeUpdate("UPDATE ENTITIES SET ID = " + var5 + " WHERE TYPE = " + var2.getByte(2) + " AND UID = '" + var2.getString(1) + "';");
         }

         var1.executeUpdate("ALTER TABLE ENTITIES ALTER COLUMN ID RESTART WITH " + var5 + ";");
         var1.executeUpdate("ALTER TABLE ENTITIES ADD PRIMARY KEY(ID);");
         var1.execute("create unique index uidType on ENTITIES(UID,TYPE);");
      }

      var1.close();
   }

   public void migrateSectorsAndSystems(PercentCallbackInterface var1) throws SQLException, IOException {
      Statement var2;
      ResultSet var10000 = (var2 = this.c.createStatement()).executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'SYSTEMS';");
      File[] var3 = null;
      if (!var10000.next()) {
         this.tableManager.getSystemTable().createTable();
         this.tableManager.getSectorTable().createTable();
         this.tableManager.getSectorItemTable().createTable();
         int var4 = (var3 = (new FileExt(GameServerState.DATABASE_PATH)).listFiles()).length;
         this.fillSystemsTable(var3, var2, var1, var4);
         this.fillSectorsTable(var3, var2, var1, var4);
         File[] var5 = (new FileExt(GameServerState.DATABASE_PATH)).listFiles(new FilenameFilter() {
            public boolean accept(File var1, String var2) {
               return var2.startsWith("SECTOR") || var2.startsWith("VOIDSYSTEM");
            }
         });

         for(int var6 = 0; var6 < var5.length; ++var6) {
            System.err.println("[MIGRATION] CLEANING UP DEPRECATED FILE: " + var5[var6].getName());
            var5[var6].delete();
         }
      }

   }

   public void migrateFleets(PercentCallbackInterface var1) throws SQLException, IOException {
      Statement var2;
      if (!(var2 = this.c.createStatement()).executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'FLEETS';").next()) {
         this.tableManager.getFleetTable().createTable();
         this.tableManager.getFleetMemberTable().createTable();
      } else {
         if (!var2.executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'FLEET_MEMBERS' AND COLUMN_NAME = 'DOCKED_TO'").next()) {
            var2.executeUpdate("ALTER TABLE FLEET_MEMBERS ADD COLUMN DOCKED_TO BIGINT DEFAULT -1 not null");
         }

         if (!var2.executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'FLEETS' AND COLUMN_NAME = 'COMMAND'").next()) {
            var2.executeUpdate("ALTER TABLE FLEETS ADD COLUMN COMMAND VARBINARY(1024)");
         }
      }

      var2.close();
   }

   public void migrateTrade(PercentCallbackInterface var1) throws SQLException, IOException {
      Statement var3;
      if ((var3 = this.c.createStatement()).executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'TRADE_NODES' AND COLUMN_NAME = 'SYS_X';").next()) {
         var3.execute("DROP TABLE TRADE_NODES if exists;");
         var3.execute("DROP TABLE TRADE_HISTORY if exists");
      }

      ResultSet var2 = var3.executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'TRADE_HISTORY';");
      var3.execute("DROP TABLE TRADES if exists;");
      if (!var2.next()) {
         this.tableManager.getTradeNodeTable();
         this.tableManager.getTradeHistoryTable().createTable();
      }

      var3.close();
   }

   public void migrateEffects(PercentCallbackInterface var1) throws SQLException, IOException {
      Statement var2;
      if (!(var2 = this.c.createStatement()).executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'EFFECTS';").next()) {
         this.tableManager.getEntityEffectTable().createTable();
      }

      var2.close();
   }

   public void migrateVisibilityAndPlayers(PercentCallbackInterface var1) throws SQLException, IOException {
      Statement var2;
      if (!(var2 = this.c.createStatement()).executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'VISIBILITY';").next()) {
         var2.execute("DROP TABLE VISIBILITY if exists;");
         var2.execute("DROP TABLE PLAYERS if exists");
         this.tableManager.getVisibilityTable().createTable();
         this.tableManager.getVisibilityTable().createTable();
      }

      var2.close();
   }

   public void migrateNPCFactionStats() throws SQLException, IOException {
      Statement var1;
      if (!(var1 = this.c.createStatement()).executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'NPC_STATS';").next()) {
         System.err.println("[DATABASE] switch trade prices");
         this.tableManager.getTradeNodeTable().switchTradePrices();
         this.tableManager.getNpcStatTable().createTable();
      }

      var1.close();
   }

   public void migrateMessageSystem(PercentCallbackInterface var1) throws SQLException, IOException {
      Statement var2;
      if (!(var2 = this.c.createStatement()).executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'PLAYER_MESSAGES';").next()) {
         this.tableManager.getPlayerMessagesTable().createTable();
      }

      var2.close();
   }

   public List resultToList(ResultSet var1) throws SQLException {
      long var2 = System.currentTimeMillis();
      ObjectArrayList var4 = new ObjectArrayList();
      if (var1.next()) {
         do {
            var4.add(new DatabaseEntry(var1));
         } while(var1.next());
      }

      if (!var1.isClosed()) {
         var1.close();
      }

      if (System.currentTimeMillis() - var2 > 10L) {
         System.err.println("[SQL] SECTOR QUERY LIST CONVERSION TOOK " + (System.currentTimeMillis() - var2) + "ms; list size: " + var4.size() + ";");
      }

      return var4;
   }

   public void setAutoCommit(boolean var1) throws SQLException {
      this.c.setAutoCommit(var1);
   }

   private void setDbTransactionControl(String var1) {
      Statement var2 = null;
      boolean var8 = false;

      label80: {
         try {
            var8 = true;
            (var2 = this.c.createStatement()).execute("SET DATABASE TRANSACTION CONTROL " + var1);
            var8 = false;
            break label80;
         } catch (SQLException var12) {
            var1 = null;
            var12.printStackTrace();
            var8 = false;
         } finally {
            if (var8) {
               try {
                  if (var2 != null) {
                     var2.close();
                  }
               } catch (SQLException var10) {
                  var10.printStackTrace();
               }

            }
         }

         try {
            if (var2 != null) {
               var2.close();
            }

            return;
         } catch (SQLException var9) {
            var9.printStackTrace();
            return;
         }
      }

      try {
         if (var2 != null) {
            var2.close();
         }

      } catch (SQLException var11) {
         var1 = null;
         var11.printStackTrace();
      }
   }

   private void setDbNIOFileSizeControl(int var1) {
      if (!FastMath.isPowerOfTwo(var1)) {
         throw new IllegalArgumentException("DATABASE NIO FILE SIZE MUST BE POWER OF TWO, BUT WAS " + var1);
      } else {
         Statement var2 = null;

         try {
            (var2 = this.c.createStatement()).execute("SET FILES NIO SIZE " + var1);
            return;
         } catch (SQLException var9) {
            var9.printStackTrace();
         } finally {
            try {
               if (var2 != null) {
                  var2.close();
               }
            } catch (SQLException var8) {
               var8.printStackTrace();
            }

         }

      }
   }

   public void setLockingMode() {
      this.setDbTransactionControl("LOCKS");
   }

   public void setMVCCMode() {
      this.setDbTransactionControl("MVCC");
   }

   public void updateFromFile(File var1) throws IOException, SQLException {
      Statement var2 = this.c.createStatement();
      updateFromFile(var1, var2);
      var2.close();
   }

   public static void resultSetToStringBuffer(StringBuffer var0, ResultSet var1) throws SQLException {
      ResultSetMetaData var2;
      String[] var3 = new String[(var2 = var1.getMetaData()).getColumnCount()];

      int var4;
      for(var4 = 0; var4 < var3.length; ++var4) {
         var3[var4] = var2.getColumnLabel(var4 + 1);
      }

      for(var4 = 0; var4 < var3.length; ++var4) {
         var0.append("\"" + var3[var4] + "\"");
         if (var4 < var3.length - 1) {
            var0.append(";");
         }
      }

      var0.append("\n");

      while(var1.next()) {
         Object[] var6 = new Object[var3.length];

         int var5;
         for(var5 = 0; var5 < var6.length; ++var5) {
            var6[var5] = var1.getObject(var5 + 1);
         }

         for(var5 = 0; var5 < var6.length; ++var5) {
            var0.append("\"" + var6[var5] + "\"");
            if (var5 < var6.length - 1) {
               var0.append(";");
            }
         }

         var0.append("\n");
      }

   }

   public void adminSql(String var1, boolean var2, boolean var3, GameServerState var4, RegisteredClientInterface var5, StringBuffer var6) {
      Statement var19 = null;
      boolean var12 = false;

      label74: {
         try {
            var12 = true;
            var19 = this.c.createStatement();
            ResultSet var18;
            if (!var2) {
               var18 = var19.executeQuery(var1);
               resultSetToStringBuffer(var6, var18);
               var12 = false;
            } else if (!var3) {
               var19.executeUpdate(var1);
               var12 = false;
            } else {
               var19.executeUpdate(var1, 1);
               var18 = var19.getGeneratedKeys();
               resultSetToStringBuffer(var6, var18);
               var12 = false;
            }
            break label74;
         } catch (Exception var16) {
            var16.printStackTrace();
            var12 = false;
         } finally {
            if (var12) {
               try {
                  var19.close();
               } catch (Exception var13) {
                  var13.printStackTrace();
                  var6.append(var13.getClass().getName() + ": " + var13.getMessage());
               }

            }
         }

         try {
            var19.close();
            return;
         } catch (Exception var14) {
            var14.printStackTrace();
            var6.append(var14.getClass().getName() + ": " + var14.getMessage());
            return;
         }
      }

      try {
         var19.close();
      } catch (Exception var15) {
         var15.printStackTrace();
         var6.append(var15.getClass().getName() + ": " + var15.getMessage());
      }
   }

   public TableManager getTableManager() {
      return this.tableManager;
   }

   static {
      String[][] var0 = new String[][]{{"\u0000", "\\x00", "\\\\0"}, {"'", "'", "\\\\'"}, {"\"", "\"", "\\\\\""}, {"\b", "\\x08", "\\\\b"}, {"\n", "\\n", "\\\\n"}, {"\r", "\\r", "\\\\r"}, {"\t", "\\t", "\\\\t"}, {"\u001a", "\\x1A", "\\\\Z"}, {"\\", "\\\\", "\\\\\\\\"}};
      sqlTokens = new HashMap();
      String var1 = "";
      int var2 = (var0 = var0).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         String[] var4 = var0[var3];
         sqlTokens.put(var4[0], var4[2]);
         var1 = var1 + (var1.isEmpty() ? "" : "|") + var4[1];
      }

      sqlTokenPattern = Pattern.compile("(" + var1 + ')');
   }
}
