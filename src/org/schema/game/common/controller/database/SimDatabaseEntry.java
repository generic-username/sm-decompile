package org.schema.game.common.controller.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.schema.common.util.linAlg.Vector3i;

public class SimDatabaseEntry {
   public Vector3i sectorPos;
   public int type;
   public int faction;
   public int creatorID;

   public SimDatabaseEntry(ResultSet var1) throws SQLException {
      this.sectorPos = new Vector3i(var1.getInt(1), var1.getInt(2), var1.getInt(3));
      this.type = var1.getByte(4);
      this.faction = var1.getInt(5);
      this.creatorID = var1.getInt(6);
   }
}
