package org.schema.game.common.controller.rails;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.common.language.Lng;

public class DockingFailReason {
   public String reason;

   public void popupClient(SegmentController var1) {
      if (!var1.isOnServer() && this.reason != null) {
         var1.popupOwnClientMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RAILS_DOCKINGFAILREASON_0, this.reason), 3);
      }

   }
}
