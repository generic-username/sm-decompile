package org.schema.game.common.controller.rails;

import com.bulletphysics.collision.dispatch.ManifoldResult;
import com.bulletphysics.collision.narrowphase.DiscreteCollisionDetectorInterface.ClosestPointInput;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CompoundShape;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.IDebugDraw;
import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.common.controller.SegmentBufferIteratorInterface;
import org.schema.game.common.controller.SegmentBufferManager;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.BoxBoxDetector;
import org.schema.game.common.data.physics.CubeShape;
import org.schema.game.common.data.physics.GjkPairDetectorExt;
import org.schema.game.common.data.physics.ObjectPool;
import org.schema.game.common.data.physics.octree.ArrayOctree;
import org.schema.game.common.data.physics.octree.ArrayOctreeTraverse;
import org.schema.game.common.data.physics.sweepandpruneaabb.OverlappingSweepPair;
import org.schema.game.common.data.physics.sweepandpruneaabb.SegmentAabbInterface;
import org.schema.game.common.data.physics.sweepandpruneaabb.SweepPoint;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.BoundingBox;

public class RailCollisionChecker {
   private static ThreadLocal threadLocal = new ThreadLocal() {
      protected final RailCollisionVars initialValue() {
         return new RailCollisionVars();
      }
   };
   protected final ObjectPool pointInputsPool = ObjectPool.get(ClosestPointInput.class);
   private final List testFrom = new ObjectArrayList();
   private final List testTo = new ObjectArrayList();
   private final RailCollisionVars v;
   private final RailCollisionChecker.InnerSegmentAddHandler innerHandler = new RailCollisionChecker.InnerSegmentAddHandler();
   private final RailCollisionChecker.OuterSegmentAddHandler outerHandler = new RailCollisionChecker.OuterSegmentAddHandler();
   private final GjkPairDetectorExt gjkPairDetector;
   ManifoldResult resultOut = new ManifoldResult();
   private SegmentController mainTestDocked;
   private boolean hasCollision;
   private boolean detailedTest;
   private ObjectArrayList log = new ObjectArrayList();
   private BoxBoxDetector boxboxPairDetector = new BoxBoxDetector();
   private long lastTimeDebug;
   private boolean rTest;
   private boolean uTest;
   private boolean fTest;
   private static final int margin = 1;
   private static final boolean old = false;

   public RailCollisionChecker() {
      this.v = (RailCollisionVars)threadLocal.get();
      this.boxboxPairDetector.fastContactOnly = true;
      this.gjkPairDetector = new GjkPairDetectorExt(this.v.gjkVariables);
      this.gjkPairDetector.useExtraPenetrationCheck = true;
      this.gjkPairDetector.fastContactOnly = true;
      this.gjkPairDetector.init((ConvexShape)null, (ConvexShape)null, this.v.simplexSolver, this.v.pdSolver);
   }

   public boolean checkPotentialCollisionWithRail(SegmentController var1, SegmentController[] var2, boolean var3) {
      long var4;
      if ((var4 = System.currentTimeMillis()) - this.lastTimeDebug > 3000L) {
         this.lastTimeDebug = var4;
      }

      this.detailedTest = !var3;
      this.mainTestDocked = var1;
      var1 = var1.railController.getRoot();
      this.addToTestsRecusively(var1, this.testTo, var2);

      for(int var8 = 0; var8 < this.testFrom.size(); ++var8) {
         for(int var9 = 0; var9 < this.testTo.size(); ++var9) {
            this.log.clear();
            SegmentController var10 = (SegmentController)this.testFrom.get(var8);
            SegmentController var5 = (SegmentController)this.testTo.get(var9);
            Transform var6 = var10.railController.getPotentialRelativeToRootLocalTransform();
            Transform var7 = var5.railController.getPotentialRelativeToRootLocalTransform();
            this.detailedTest = !var3 || var10.railController.isTurretDocked() || var5.railController.isTurretDocked();
            if (this.processCollision(var10, var5, var6, var7)) {
               this.testFrom.clear();
               this.testTo.clear();

               for(var8 = 0; var8 < this.log.size(); ++var8) {
                  System.err.println((String)this.log.get(var8));
               }

               return true;
            }
         }
      }

      this.testFrom.clear();
      this.testTo.clear();
      return false;
   }

   private void addToTestsRecusively(SegmentController var1, List var2, SegmentController[] var3) {
      boolean var4 = false;
      if (var3 != null) {
         SegmentController[] var5 = var3;
         int var6 = var3.length;

         for(int var7 = 0; var7 < var6; ++var7) {
            if (var5[var7] == var1) {
               var4 = true;
            }
         }
      }

      if (!var4) {
         var2.add(var1);
      }

      for(int var8 = 0; var8 < var1.railController.next.size(); ++var8) {
         RailRelation var9;
         if ((var9 = (RailRelation)var1.railController.next.get(var8)).docked.getSegmentController() == this.mainTestDocked) {
            this.addToTestsRecusively(var9.docked.getSegmentController(), this.testFrom, var3);
         } else {
            this.addToTestsRecusively(var9.docked.getSegmentController(), var2, var3);
         }
      }

   }

   private boolean processCollision(SegmentController var1, SegmentController var2, Transform var3, Transform var4) {
      this.hasCollision = false;
      this.v.oSet = ArrayOctree.getSet(var1.isOnServer());
      CubeShape var5 = (CubeShape)((CompoundShape)var1.getPhysicsDataContainer().getShape()).getChildShape(0);
      CubeShape var6 = (CubeShape)((CompoundShape)var2.getPhysicsDataContainer().getShape()).getChildShape(0);
      var5.getAabb(var3, this.v.outerWorld.min, this.v.outerWorld.max);
      var6.getAabb(var4, this.v.innerWorld.min, this.v.innerWorld.max);
      if (!AabbUtil2.testAabbAgainstAabb2(this.v.outerWorld.min, this.v.outerWorld.max, this.v.innerWorld.min, this.v.innerWorld.max)) {
         return false;
      } else {
         assert var1 != var2;

         this.v.tmpTrans0.set(var3);
         this.v.tmpTrans1.set(var4);
         this.v.tmpTrans0Actual.set(var3);
         this.v.tmpTrans1Actual.set(var4);
         this.v.tmpTrans1Rel.set(var3);
         this.v.tmpTrans1Rel.inverse();
         this.v.tmpTrans1Rel.mul(var4);
         this.v.tmpTrans0Rel.setIdentity();
         this.v.tmpTrans0.set(this.v.tmpTrans0Rel);
         this.v.tmpTrans1.set(this.v.tmpTrans1Rel);
         this.v.absolute1.set(this.v.tmpTrans0.basis);
         MatrixUtil.absolute(this.v.absolute1);
         this.v.absolute2.set(this.v.tmpTrans1.basis);
         MatrixUtil.absolute(this.v.absolute2);
         this.v.innerMinBlock.set(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);
         this.v.innerMaxBlock.set(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
         this.v.outerMinBlock.set(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);
         this.v.outerMaxBlock.set(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
         Vector3f var14 = GlUtil.getRightVector(this.v.right0, this.v.tmpTrans0);
         Vector3f var16 = GlUtil.getRightVector(this.v.right1, this.v.tmpTrans1);
         Vector3f var18 = GlUtil.getUpVector(this.v.up0, this.v.tmpTrans0);
         Vector3f var20 = GlUtil.getUpVector(this.v.up1, this.v.tmpTrans1);
         Vector3f var7 = GlUtil.getForwardVector(this.v.forw0, this.v.tmpTrans0);
         Vector3f var8 = GlUtil.getForwardVector(this.v.forw1, this.v.tmpTrans1);
         this.rTest = var14.epsilonEquals(var16, 0.005F);
         this.uTest = var18.epsilonEquals(var20, 0.005F);
         this.fTest = var7.epsilonEquals(var8, 0.005F);
         this.v.box0.setMargin(0.0F);
         this.v.box1.setMargin(0.0F);
         var5.setMargin(0.0F);
         var6.setMargin(0.0F);
         this.v.wtInv0.set(this.v.tmpTrans0);
         this.v.wtInv0.inverse();
         this.v.wtInv1.set(this.v.tmpTrans1);
         this.v.wtInv1.inverse();
         if (!this.doAABBIntersection(var5, var6)) {
            return false;
         } else {
            Math.abs(ByteUtil.divSeg(this.v.maxIntA.x) - ByteUtil.divSeg(this.v.minIntA.x));
            Math.abs(ByteUtil.divSeg(this.v.maxIntA.y) - ByteUtil.divSeg(this.v.minIntA.y));
            Math.abs(ByteUtil.divSeg(this.v.maxIntA.z) - ByteUtil.divSeg(this.v.minIntA.z));
            Math.abs(ByteUtil.divSeg(this.v.maxIntB.x) - ByteUtil.divSeg(this.v.minIntB.x));
            Math.abs(ByteUtil.divSeg(this.v.maxIntB.y) - ByteUtil.divSeg(this.v.minIntB.y));
            Math.abs(ByteUtil.divSeg(this.v.maxIntB.z) - ByteUtil.divSeg(this.v.minIntB.z));

            assert this.v.minIntA.x <= this.v.maxIntA.x;

            assert this.v.minIntA.y <= this.v.maxIntA.y;

            assert this.v.minIntA.z <= this.v.maxIntA.z;

            assert this.v.minIntB.x <= this.v.maxIntB.x;

            assert this.v.minIntB.y <= this.v.maxIntB.y;

            assert this.v.minIntB.z <= this.v.maxIntB.z;

            int var15 = this.v.maxIntA.x - this.v.minIntA.x;
            int var17 = this.v.maxIntA.y - this.v.minIntA.y;
            int var19 = this.v.maxIntA.z - this.v.minIntA.z;
            int var21 = this.v.maxIntB.x - this.v.minIntB.x;
            int var22 = this.v.maxIntB.y - this.v.minIntB.y;
            int var23 = this.v.maxIntB.z - this.v.minIntB.z;

            assert this.v.innerNonEmptySegments.isEmpty();

            assert this.v.outerNonEmptySegments.isEmpty();

            assert this.v.bbCacheInner.isEmpty();

            boolean var9 = false;

            try {
               ((SegmentBufferManager)var5.getSegmentBuffer()).lastIteration = 0;
               ((SegmentBufferManager)var6.getSegmentBuffer()).lastIteration = 0;
               ((SegmentBufferManager)var5.getSegmentBuffer()).lastDoneIteration = 0;
               ((SegmentBufferManager)var6.getSegmentBuffer()).lastDoneIteration = 0;
               if (var15 > 5000 || var17 > 5000 || var17 > 5000) {
                  System.err.println("Exception collision overlap to big " + var15 + "; " + var17 + "; " + var19 + "; for: " + var5.getSegmentBuffer().getSegmentController() + "; boundingBox: " + var5.getSegmentBuffer().getBoundingBox());
                  return false;
               }

               if (var21 <= 5000 && var22 <= 5000 && var22 <= 5000) {
                  var5.getSegmentBuffer().iterateOverNonEmptyElementRange(this.outerHandler, this.v.minIntA, this.v.maxIntA, false);
                  var6.getSegmentBuffer().iterateOverNonEmptyElementRange(this.innerHandler, this.v.minIntB, this.v.maxIntB, false);
                  var9 = this.handleSweepAndPrune(var5, var6);
                  return var9;
               }

               System.err.println("Exception collision overlap to big " + var21 + "; " + var22 + "; " + var23 + "; for: " + var5.getSegmentBuffer().getSegmentController() + "; boundingBox: " + var5.getSegmentBuffer().getBoundingBox());
            } catch (Exception var12) {
               var12.printStackTrace();
               return var9;
            } finally {
               this.v.innerNonEmptySegments.clear();
               this.v.outerNonEmptySegments.clear();
               this.v.bbCacheInner.clear();
            }

            return false;
         }
      }
   }

   private boolean doAABBIntersection(SegmentAabbInterface var1, SegmentAabbInterface var2) {
      this.v.tmpAABBTrans0.set(this.v.wtInv0);
      this.v.tmpAABBTrans0.mul(this.v.tmpTrans1);
      var2.getAabbUncached(this.v.tmpAABBTrans0, this.v.innerAABBforA.min, this.v.innerAABBforA.max, false);
      var1.getAabbIdent(this.v.intersection.min, this.v.intersection.max);
      BoundingBox var3;
      if ((var3 = this.v.intersection.getIntersection(this.v.innerAABBforA, this.v.intersectionInASpaceWithB)) == null) {
         return false;
      } else {
         this.v.minIntA.x = ByteUtil.divSeg((int)(var3.min.x - 1.0F)) << 5;
         this.v.minIntA.y = ByteUtil.divSeg((int)(var3.min.y - 1.0F)) << 5;
         this.v.minIntA.z = ByteUtil.divSeg((int)(var3.min.z - 1.0F)) << 5;
         this.v.maxIntA.x = FastMath.fastCeil((var3.max.x + 1.0F) / 32.0F) << 5;
         this.v.maxIntA.y = FastMath.fastCeil((var3.max.y + 1.0F) / 32.0F) << 5;
         this.v.maxIntA.z = FastMath.fastCeil((var3.max.z + 1.0F) / 32.0F) << 5;
         this.v.tmpAABBTrans1.set(this.v.wtInv1);
         this.v.tmpAABBTrans1.mul(this.v.tmpTrans0);
         var1.getAabb(this.v.tmpAABBTrans1, this.v.outerAABBforB.min, this.v.outerAABBforB.max);
         var2.getAabb(TransformTools.ident, this.v.intersection.min, this.v.intersection.max);
         if ((var3 = this.v.intersection.getIntersection(this.v.outerAABBforB, this.v.intersectionInBSpaceWithA)) == null) {
            return false;
         } else {
            this.v.minIntB.x = ByteUtil.divSeg((int)(var3.min.x - 1.0F)) << 5;
            this.v.minIntB.y = ByteUtil.divSeg((int)(var3.min.y - 1.0F)) << 5;
            this.v.minIntB.z = ByteUtil.divSeg((int)(var3.min.z - 1.0F)) << 5;
            this.v.maxIntB.x = FastMath.fastCeil((var3.max.x + 1.0F) / 32.0F) << 5;
            this.v.maxIntB.y = FastMath.fastCeil((var3.max.y + 1.0F) / 32.0F) << 5;
            this.v.maxIntB.z = FastMath.fastCeil((var3.max.z + 1.0F) / 32.0F) << 5;
            return true;
         }
      }
   }

   private boolean handleSweepAndPrune(SegmentAabbInterface var1, SegmentAabbInterface var2) {
      this.v.sweeper.fill(var1, this.v.tmpTrans0, var2, this.v.tmpTrans1, this.v.outerNonEmptySegments, this.v.innerNonEmptySegments);
      this.v.sweeper.getOverlapping();
      Iterator var3 = this.v.sweeper.pairs.iterator();

      do {
         if (!var3.hasNext()) {
            this.v.sweeper.pairs.clear();
            this.v.innerNonEmptySegments.clear();
            this.v.outerNonEmptySegments.clear();
            this.v.bbCacheInner.clear();
            return false;
         }

         OverlappingSweepPair var4;
         SweepPoint var5 = (var4 = (OverlappingSweepPair)var3.next()).a;
         SweepPoint var6 = var4.b;
         this.processDistinctCollision(var1, var2, ((Segment)var5.seg).getSegmentData(), ((Segment)var6.seg).getSegmentData(), this.v.tmpTrans0, this.v.tmpTrans1);
      } while(!this.hasCollision);

      return true;
   }

   private void processDistinctCollision(SegmentAabbInterface var1, SegmentAabbInterface var2, SegmentData var3, SegmentData var4, Transform var5, Transform var6) {
      this.v.octreeTopLevelSweeper.abs0 = this.v.absolute1;
      this.v.octreeTopLevelSweeper.abs1 = this.v.absolute2;
      this.v.octreeTopLevelSweeper.seg0 = var3.getSegment();
      this.v.octreeTopLevelSweeper.set = this.v.oSet;
      this.v.simpleListA.max = 0;
      this.v.simpleListB.max = 0;
      this.v.octreeTopLevelSweeper.debug = false;
      this.v.octreeTopLevelSweeper.fill(var3.getSegment(), var5, var4.getSegment(), var6, this.v.simpleListA, this.v.simpleListB);
      this.v.octreeTopLevelSweeper.getOverlapping();
      int var7 = this.v.octreeTopLevelSweeper.pairs.size();

      for(int var8 = 0; var8 < var7; ++var8) {
         OverlappingSweepPair var9;
         SweepPoint var10 = (var9 = (OverlappingSweepPair)this.v.octreeTopLevelSweeper.pairs.get(var8)).a;
         SweepPoint var11 = var9.b;
         this.handle16(var1, var2, var3, var4, var5, var6, var10, var11);
      }

   }

   private void handle16(SegmentAabbInterface var1, SegmentAabbInterface var2, SegmentData var3, SegmentData var4, Transform var5, Transform var6, SweepPoint var7, SweepPoint var8) {
      int var9 = (Integer)var7.seg;
      int var10 = (Integer)var8.seg;
      if (!this.v.intersectionCallBackAwithB.initialized) {
         this.v.intersectionCallBackAwithB.createHitCache(4096);
         this.v.intersectionCallBackBwithA.createHitCache(4096);
      }

      this.v.intersectionCallBackAwithB.reset();
      this.v.intersectionCallBackBwithA.reset();
      this.v.bbSeg16a.min.set(var7.minX, var7.minY, var7.minZ);
      this.v.bbSeg16a.max.set(var7.maxX, var7.maxY, var7.maxZ);
      this.v.bbSeg16b.min.set(var8.minX, var8.minY, var8.minZ);
      this.v.bbSeg16b.max.set(var8.maxX, var8.maxY, var8.maxZ);
      this.v.intersectionCallBackAwithB = var3.getOctree().findIntersectingAABBFromFirstLvl(var9, this.v.oSet, this.v.intersectionCallBackAwithB, var3.getSegment(), var5, this.v.absolute1, 0.0F, this.v.bbSeg16b.min, this.v.bbSeg16b.max, 1.0F);
      if (this.v.intersectionCallBackAwithB.hitCount != 0) {
         this.v.intersectionCallBackBwithA.reset();
         this.v.oSet.debug = var4.getSegmentController().isOnServer();
         this.v.intersectionCallBackBwithA = var4.getOctree().findIntersectingAABBFromFirstLvl(var10, this.v.oSet, this.v.intersectionCallBackBwithA, var4.getSegment(), var6, this.v.absolute2, -0.001F, this.v.bbSeg16a.min, this.v.bbSeg16a.max, 1.0F);
         this.v.oSet.debug = false;
         if (this.v.intersectionCallBackBwithA.hitCount != 0) {
            this.v.simpleListA.max = this.v.intersectionCallBackAwithB.hitCount;
            this.v.simpleListB.max = this.v.intersectionCallBackBwithA.hitCount;
            this.v.octreeSweeper.fill(this.v.intersectionCallBackAwithB, (Transform)null, this.v.intersectionCallBackBwithA, (Transform)null, this.v.simpleListA, this.v.simpleListB);
            this.v.octreeSweeper.getOverlapping();
            this.v.tmpTrans3.set(this.v.tmpTrans0);
            this.v.tmpTrans4.set(this.v.tmpTrans1);
            var9 = this.v.octreeSweeper.pairs.size();

            for(var10 = 0; var10 < var9; ++var10) {
               OverlappingSweepPair var11;
               SweepPoint var10000 = (var11 = (OverlappingSweepPair)this.v.octreeSweeper.pairs.get(var10)).a;
               var10000 = var11.b;
               int var13 = this.v.intersectionCallBackAwithB.getHit((Integer)var11.a.seg, this.v.bbOuterOct.min, this.v.bbOuterOct.max, this.v.startA, this.v.endA);
               int var14 = this.v.intersectionCallBackAwithB.getNodeIndexHit((Integer)var11.a.seg);
               int var15 = this.v.intersectionCallBackBwithA.getHit((Integer)var11.b.seg, this.v.bbInnerOct.min, this.v.bbInnerOct.max, this.v.startB, this.v.endB);
               int var12 = this.v.intersectionCallBackBwithA.getNodeIndexHit((Integer)var11.b.seg);
               this.doNarrowTest(var3, var4, this.v.startA, this.v.endA, this.v.startB, this.v.endB, var13, var15, var14, var12, this.v.bbOctIntersection);
               if (this.hasCollision) {
                  break;
               }
            }

         }
      }
   }

   private void processDistinctCollisionOld(SegmentAabbInterface var1, SegmentAabbInterface var2, SegmentData var3, SegmentData var4, Transform var5, Transform var6) {
      if (!this.v.intersectionCallBackAwithB.initialized) {
         this.v.intersectionCallBackAwithB.createHitCache(4096);
         this.v.intersectionCallBackBwithA.createHitCache(4096);
      }

      this.v.intersectionCallBackAwithB.reset();
      this.v.intersectionCallBackBwithA.reset();
      this.v.intersectionCallBackAwithB = var3.getOctree().findIntersectingAABB(this.v.oSet, this.v.intersectionCallBackAwithB, var3.getSegment(), var5, this.v.absolute1, 0.0F, this.v.bbSectorIntersection.min, this.v.bbSectorIntersection.max, 1.0F);
      if (this.v.intersectionCallBackAwithB.hitCount != 0) {
         this.v.intersectionCallBackBwithA.reset();
         this.v.oSet.debug = var4.getSegmentController().isOnServer();
         this.v.intersectionCallBackBwithA = var4.getOctree().findIntersectingAABB(this.v.oSet, this.v.intersectionCallBackBwithA, var4.getSegment(), var6, this.v.absolute2, -0.001F, this.v.bbSectorIntersection.min, this.v.bbSectorIntersection.max, 1.0F);
         this.v.oSet.debug = false;
         if (this.v.intersectionCallBackBwithA.hitCount != 0) {
            this.v.simpleListA.max = this.v.intersectionCallBackAwithB.hitCount;
            this.v.simpleListB.max = this.v.intersectionCallBackBwithA.hitCount;
            this.v.octreeSweeper.fill(this.v.intersectionCallBackAwithB, (Transform)null, this.v.intersectionCallBackBwithA, (Transform)null, this.v.simpleListA, this.v.simpleListB);
            this.v.octreeSweeper.getOverlapping();
            this.v.tmpTrans3.set(this.v.tmpTrans0);
            this.v.tmpTrans4.set(this.v.tmpTrans1);
            int var9 = this.v.octreeSweeper.pairs.size();

            for(int var10 = 0; var10 < var9; ++var10) {
               OverlappingSweepPair var11;
               SweepPoint var10000 = (var11 = (OverlappingSweepPair)this.v.octreeSweeper.pairs.get(var10)).a;
               var10000 = var11.b;
               int var13 = this.v.intersectionCallBackAwithB.getHit((Integer)var11.a.seg, this.v.bbOuterOct.min, this.v.bbOuterOct.max, this.v.startA, this.v.endA);
               int var7 = this.v.intersectionCallBackAwithB.getNodeIndexHit((Integer)var11.a.seg);
               int var8 = this.v.intersectionCallBackBwithA.getHit((Integer)var11.b.seg, this.v.bbInnerOct.min, this.v.bbInnerOct.max, this.v.startB, this.v.endB);
               int var12 = this.v.intersectionCallBackBwithA.getNodeIndexHit((Integer)var11.b.seg);
               this.doNarrowTest(var3, var4, this.v.startA, this.v.endA, this.v.startB, this.v.endB, var13, var8, var7, var12, this.v.bbOctIntersection);
               if (this.hasCollision) {
                  break;
               }
            }

         }
      }
   }

   private void doNarrowTest(SegmentData var1, SegmentData var2, Vector3b var3, Vector3b var4, Vector3b var5, Vector3b var6, int var7, int var8, int var9, int var10, BoundingBox var11) {
      byte[][] var24 = ArrayOctreeTraverse.tMap[var7];
      byte[][] var25 = ArrayOctreeTraverse.tMap[var8];
      int[] var26 = ArrayOctreeTraverse.tIndexMap[var9][var7];
      int[] var27 = ArrayOctreeTraverse.tIndexMap[var10][var8];

      assert var24.length <= 8 : var24.length;

      assert var25.length <= 8 : var25.length;

      for(var9 = 0; var9 < var24.length; ++var9) {
         var10 = var3.x + var24[var9][0];
         int var28 = var3.y + var24[var9][1];
         int var12 = var3.z + var24[var9][2];
         int var13 = var26[var9];
         ElementInformation var14;
         short var15;
         if ((var15 = var1.getType(var13)) != 0 && var15 == 411 | (var14 = ElementKeyMap.getInfo(var15)).isPhysical(var1.isActive(var13))) {
            byte var31;
            byte var16 = var31 = var1.getOrientation(var13);
            if (var15 == 689) {
               if (var31 == 4) {
                  continue;
               }

               var16 = 2;
            }

            this.v.elemPosA.set((float)(var10 + var1.getSegment().pos.x), (float)(var28 + var1.getSegment().pos.y), (float)(var12 + var1.getSegment().pos.z));
            this.v.elemPosAAbs.set(this.v.elemPosA);
            this.v.tmpTrans0.transform(this.v.elemPosAAbs);

            for(var10 = 0; var10 < var25.length; ++var10) {
               var28 = var5.x + var25[var10][0];
               var12 = var5.y + var25[var10][1];
               int var17 = var5.z + var25[var10][2];
               int var18 = var27[var10];
               ElementInformation var19;
               short var20;
               if ((var20 = var2.getType(var18)) != 0 && var20 == 411 | (var19 = ElementKeyMap.getInfo(var20)).isPhysical(var2.isActive(var18)) && var15 != 411 && var20 != 411) {
                  byte var33;
                  byte var21 = var33 = var2.getOrientation(var18);
                  if (var20 == 689) {
                     if (var33 == 4) {
                        continue;
                     }

                     var21 = 2;
                  }

                  this.v.elemPosB.set((float)(var28 + var2.getSegment().pos.x), (float)(var12 + var2.getSegment().pos.y), (float)(var17 + var2.getSegment().pos.z));
                  this.v.elemPosBAbs.set(this.v.elemPosB);
                  this.v.tmpTrans1.transform(this.v.elemPosBAbs);
                  this.v.elemPosDist.sub(this.v.elemPosBAbs, this.v.elemPosAAbs);
                  if ((!var14.blockStyle.solidBlockStyle || !var19.blockStyle.solidBlockStyle) && !this.detailedTest) {
                     if (FastMath.carmackLength(this.v.elemPosDist) < 0.8F) {
                        this.hasCollision = true;
                        return;
                     }
                  } else if (FastMath.carmackLength(this.v.elemPosDist) < 1.733F && (!this.rTest || Math.abs(Vector3fTools.projectScalar(this.v.elemPosDist, this.v.right0)) < 0.999F) && (!this.uTest || Math.abs(Vector3fTools.projectScalar(this.v.elemPosDist, this.v.up0)) < 0.999F) && (!this.fTest || Math.abs(Vector3fTools.projectScalar(this.v.elemPosDist, this.v.forw0)) < 0.999F)) {
                     this.v.tmpTrans3.origin.set(this.v.elemPosAAbs);
                     this.v.tmpTrans4.origin.set(this.v.elemPosBAbs);
                     Object var29 = this.v.box0;
                     Object var30 = this.v.box1;
                     boolean var32 = false;
                     if (var14.blockStyle.solidBlockStyle) {
                        var29 = BlockShapeAlgorithm.getSmallShape(var14.getBlockStyle(), var16);
                        var32 = true;

                        assert var29 != null : BlockShapeAlgorithm.getAlgo(var14.getBlockStyle(), var16);

                        var30 = this.v.box1M;
                     }

                     if (var19.blockStyle.solidBlockStyle) {
                        var30 = BlockShapeAlgorithm.getSmallShape(var19.getBlockStyle(), var21);

                        assert var30 != null : BlockShapeAlgorithm.getAlgo(var19.getBlockStyle(), var21);

                        var32 = true;
                        if (var29 == this.v.box0) {
                           var29 = this.v.box0M;
                        }
                     }

                     Transform var22 = this.v.tmpTrans3;
                     if (var14.getSlab(var31) > 0) {
                        (var22 = this.v.BT_A).set(this.v.tmpTrans3);
                        this.v.orientTT.set(Element.DIRECTIONSf[Element.switchLeftRight(var16 % 6)]);
                        var22.basis.transform(this.v.orientTT);
                        if (var32) {
                           switch(var14.getSlab(var31)) {
                           case 1:
                              this.v.orientTT.scale(0.125F);
                              var29 = this.v.box34M[var16 % 6];
                              break;
                           case 2:
                              this.v.orientTT.scale(0.25F);
                              var29 = this.v.box12M[var16 % 6];
                              break;
                           case 3:
                              this.v.orientTT.scale(0.375F);
                              var29 = this.v.box14M[var16 % 6];
                           }
                        } else {
                           switch(var14.getSlab(var31)) {
                           case 1:
                              this.v.orientTT.scale(0.125F);
                              var29 = this.v.box34[var16 % 6];
                              break;
                           case 2:
                              this.v.orientTT.scale(0.25F);
                              var29 = this.v.box12[var16 % 6];
                              break;
                           case 3:
                              this.v.orientTT.scale(0.375F);
                              var29 = this.v.box14[var16 % 6];
                           }
                        }

                        var22.origin.sub(this.v.orientTT);
                        this.boxboxPairDetector.cachedBoxSize = false;
                     }

                     Transform var23 = this.v.tmpTrans4;
                     if (var19.getSlab(var33) > 0) {
                        (var23 = this.v.BT_B).set(this.v.tmpTrans4);
                        this.v.orientTT.set(Element.DIRECTIONSf[Element.switchLeftRight(var21 % 6)]);
                        var23.basis.transform(this.v.orientTT);
                        if (var32) {
                           switch(var19.getSlab(var33)) {
                           case 1:
                              this.v.orientTT.scale(0.125F);
                              var30 = this.v.box34M[var21 % 6];
                              break;
                           case 2:
                              this.v.orientTT.scale(0.25F);
                              var30 = this.v.box12M[var21 % 6];
                              break;
                           case 3:
                              this.v.orientTT.scale(0.375F);
                              var30 = this.v.box14M[var21 % 6];
                           }
                        } else {
                           switch(var19.getSlab(var33)) {
                           case 1:
                              this.v.orientTT.scale(0.125F);
                              var30 = this.v.box34[var21 % 6];
                              break;
                           case 2:
                              this.v.orientTT.scale(0.25F);
                              var30 = this.v.box12[var21 % 6];
                              break;
                           case 3:
                              this.v.orientTT.scale(0.375F);
                              var30 = this.v.box14[var21 % 6];
                           }
                        }

                        var23.origin.sub(this.v.orientTT);
                        this.boxboxPairDetector.cachedBoxSize = false;
                     }

                     if (var32) {
                        var28 = this.doNonBlockCollision((ConvexShape)var29, (ConvexShape)var30, var22, var23, this.v.elemPosA, this.v.elemPosB, var15, var20, var1.getSegmentController().getId(), var2.getSegmentController().getId());
                     } else {
                        var28 = this.doExtCubeCubeCollision((BoxShape)var29, (BoxShape)var30, var22, var23, this.v.elemPosA, this.v.elemPosB, var15, var20, var1.getSegmentController().getId(), var2.getSegmentController().getId());
                     }

                     if (var28 > 0) {
                        this.hasCollision = true;
                        return;
                     }
                  }
               }
            }
         }
      }

   }

   private int doNonBlockCollision(ConvexShape var1, ConvexShape var2, Transform var3, Transform var4, Vector3f var5, Vector3f var6, short var7, short var8, int var9, int var10) {
      ClosestPointInput var11;
      (var11 = (ClosestPointInput)this.pointInputsPool.get()).init();
      this.gjkPairDetector.setMinkowskiA(var1);
      this.gjkPairDetector.setMinkowskiB(var2);
      this.gjkPairDetector.log = this.log;
      var11.maximumDistanceSquared = var1.getMargin() + var2.getMargin() + 0.02F;
      var11.maximumDistanceSquared *= var11.maximumDistanceSquared;
      var11.transformA.set(var3);
      var11.transformB.set(var4);
      this.gjkPairDetector.getClosestPoints(var11, this.resultOut, (IDebugDraw)null, false, var5, var6, var7, var8, var9, var10);
      this.pointInputsPool.release(var11);
      return this.gjkPairDetector.contacts;
   }

   private int doExtCubeCubeCollision(BoxShape var1, BoxShape var2, Transform var3, Transform var4, Vector3f var5, Vector3f var6, short var7, short var8, int var9, int var10) {
      ClosestPointInput var11;
      (var11 = (ClosestPointInput)this.pointInputsPool.get()).init();
      var11.maximumDistanceSquared = var1.getMargin() + var2.getMargin() + 0.02F;
      var11.maximumDistanceSquared *= var11.maximumDistanceSquared;
      var11.transformA.set(var3);
      var11.transformB.set(var4);
      this.boxboxPairDetector.GetClosestPoints(var1, var2, var11, this.resultOut, (IDebugDraw)null, false, var5, var6, var7, var8, var9, var10);
      this.pointInputsPool.release(var11);
      return this.boxboxPairDetector.contacts;
   }

   class OuterSegmentAddHandler implements SegmentBufferIteratorInterface {
      private OuterSegmentAddHandler() {
      }

      public boolean handle(Segment var1, long var2) {
         if (var1.getSegmentData() != null && !var1.isEmpty()) {
            RailCollisionChecker.this.v.outerNonEmptySegments.add(var1);
            RailCollisionChecker.this.v.outerMaxBlock.x = Math.max(RailCollisionChecker.this.v.outerMaxBlock.x, var1.getSegmentData().getMax().x + var1.pos.x);
            RailCollisionChecker.this.v.outerMaxBlock.y = Math.max(RailCollisionChecker.this.v.outerMaxBlock.y, var1.getSegmentData().getMax().y + var1.pos.y);
            RailCollisionChecker.this.v.outerMaxBlock.z = Math.max(RailCollisionChecker.this.v.outerMaxBlock.z, var1.getSegmentData().getMax().z + var1.pos.z);
            RailCollisionChecker.this.v.outerMinBlock.x = Math.min(RailCollisionChecker.this.v.outerMinBlock.x, var1.getSegmentData().getMin().x + var1.pos.x);
            RailCollisionChecker.this.v.outerMinBlock.y = Math.min(RailCollisionChecker.this.v.outerMinBlock.y, var1.getSegmentData().getMin().y + var1.pos.y);
            RailCollisionChecker.this.v.outerMinBlock.z = Math.min(RailCollisionChecker.this.v.outerMinBlock.z, var1.getSegmentData().getMin().z + var1.pos.z);
            return true;
         } else {
            return true;
         }
      }

      // $FF: synthetic method
      OuterSegmentAddHandler(Object var2) {
         this();
      }
   }

   class InnerSegmentAddHandler implements SegmentBufferIteratorInterface {
      private InnerSegmentAddHandler() {
      }

      public boolean handle(Segment var1, long var2) {
         if (var1.getSegmentData() != null && !var1.isEmpty()) {
            RailCollisionChecker.this.v.innerNonEmptySegments.add(var1);
            RailCollisionChecker.this.v.innerMaxBlock.x = Math.max(RailCollisionChecker.this.v.innerMaxBlock.x, var1.getSegmentData().getMax().x + var1.pos.x);
            RailCollisionChecker.this.v.innerMaxBlock.y = Math.max(RailCollisionChecker.this.v.innerMaxBlock.y, var1.getSegmentData().getMax().y + var1.pos.y);
            RailCollisionChecker.this.v.innerMaxBlock.z = Math.max(RailCollisionChecker.this.v.innerMaxBlock.z, var1.getSegmentData().getMax().z + var1.pos.z);
            RailCollisionChecker.this.v.innerMinBlock.x = Math.min(RailCollisionChecker.this.v.innerMinBlock.x, var1.getSegmentData().getMin().x + var1.pos.x);
            RailCollisionChecker.this.v.innerMinBlock.y = Math.min(RailCollisionChecker.this.v.innerMinBlock.y, var1.getSegmentData().getMin().y + var1.pos.y);
            RailCollisionChecker.this.v.innerMinBlock.z = Math.min(RailCollisionChecker.this.v.innerMinBlock.z, var1.getSegmentData().getMin().z + var1.pos.z);
            return true;
         } else {
            return true;
         }
      }

      // $FF: synthetic method
      InnerSegmentAddHandler(Object var2) {
         this();
      }
   }
}
