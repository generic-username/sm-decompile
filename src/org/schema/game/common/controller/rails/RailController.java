package org.schema.game.common.controller.rails;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.shapes.CompoundShape;
import com.bulletphysics.collision.shapes.CompoundShapeChild;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2LongOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Tuple4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.common.util.TranslatableEnum;
import org.schema.common.util.linAlg.Quat4Util;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.client.view.camera.InShipCamera;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.client.view.gui.weapon.WeaponRowElementInterface;
import org.schema.game.client.view.gui.weapon.WeaponSegmentControllerUsableElement;
import org.schema.game.common.controller.BlockLogicReplaceInterface;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.game.common.controller.ai.AIGameConfiguration;
import org.schema.game.common.controller.ai.SegmentControllerAIInterface;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.controller.elements.ActivationManagerInterface;
import org.schema.game.common.controller.elements.ManagerActivityInterface;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ManagerReloadInterface;
import org.schema.game.common.controller.elements.RailManagerInterface;
import org.schema.game.common.controller.elements.SegmentControllerUsable;
import org.schema.game.common.controller.elements.ShieldAddOn;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.controller.elements.rail.massenhancer.RailMassEnhancerCollectionManager;
import org.schema.game.common.controller.elements.rail.speed.RailSpeedElementManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.VoidUniqueSegmentPiece;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.CubesCompoundShape;
import org.schema.game.common.data.physics.RigidBodySegmentController;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SectorNotFoundException;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.game.network.objects.RailMove;
import org.schema.game.network.objects.remote.RemoteRailMoveRequest;
import org.schema.game.network.objects.remote.RemoteRailRequest;
import org.schema.game.server.controller.SectorSwitch;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.stateMachines.AIConfigurationInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.BoundingSphere;
import org.schema.schine.graphicsengine.util.timer.LinearTimerUtil;
import org.schema.schine.input.InputType;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.objects.container.UpdateWithoutPhysicsObjectInterface;
import org.schema.schine.network.objects.remote.RemoteVector4f;
import org.schema.schine.network.objects.remote.Streamable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class RailController implements BlockLogicReplaceInterface, UpdateWithoutPhysicsObjectInterface {
   public static final byte TYPE_TAG_NOTHING = 0;
   public static final byte TYPE_TAG_DOCKED = 1;
   public static final byte TYPE_TAG_RAIL_ROOT = 2;
   public static final byte TYPE_TAG_ACTIVE_REQUEST = 3;
   public final List next = new ObjectArrayList();
   private final SegmentController self;
   private final Transform relativeToRootLocalTransform;
   private final Transform railTurretMovingLocalTransform;
   private final Transform railTurretMovingLocalTransformTarget;
   private final Transform railMovingLocalAtDockTransform;
   private final Transform railMovingLocalTransform;
   private final Transform railOriginalLocalTransform;
   public final Transform potentialBasicRelativeTransform;
   private final Transform railUpToThisOriginalLocalTransform;
   private final Transform basicRelativeTransform;
   private final Transform potentialRelativeToRootLocalTransform;
   private final Transform potentialRailTurretMovingLocalTransform;
   private final Transform potentialRailMovingLocalTransform;
   private final Transform potentialRailOriginalLocalTransform;
   private final Transform potentialRailUpToThisOriginalLocalTransform;
   private final Transform prefRot = new Transform();
   private Transform railTurretMovingLocalTransformLastUpdate = new Transform();
   private Transform railMovingLocalTransformLastUpdate = new Transform();
   private Transform railTurretMovingLocalTransformLastUpdateR;
   private Transform railMovingLocalTransformLastUpdateR;
   private final List railMovingProspectedRots = new ObjectArrayList(8);
   private final RailCollisionChecker collisionChecker = new RailCollisionChecker();
   private final ObjectArrayFIFOQueue railRequests = new ObjectArrayFIFOQueue();
   private final Vector3f lastLinearVelocityFromRoot = new Vector3f();
   private final SegmentPiece[] tmpPieces = new SegmentPiece[6];
   private final SegmentPiece tmpPiece = new SegmentPiece();
   private final SegmentPiece tmpPieceR = new SegmentPiece();
   private final Vector3i tmpPos = new Vector3i();
   public Matrix3f turretRotYHelp;
   public RailRelation previous;
   public RailRequest railRequestCurrent;
   public Transform turretRotX;
   private Vector3f railMovingProspectedPos;
   private LinearTimerUtil linearMoveTest = new LinearTimerUtil(0.5F);
   private float dockingMass;
   private boolean dirty;
   private long dockedSince;
   private long lastDisconnect;
   private float railSpeed = 3.0F;
   private float railSpeedRotBasis = 5.0F;
   private float railSpeedRot = 1.5F;
   private float railSpeedPercent;
   private boolean requestedToDefaultFlag;
   private long lastValidityCheck;
   private long lastTurretColCheck;
   private long lastCurretCollisionHappened;
   private float railMovingProspectedRotTime;
   private float colLinTimer;
   private float colRotTimer;
   private float railMovingProspectedRotSegmentTime;
   private boolean loadedFromTag;
   private boolean colByLinearMovement;
   public String[] dockedUIDSFromTag;
   private boolean recY;
   private long lastDockRequest;
   private final Vector3f lastMovementDirRelativeToRoot = new Vector3f();
   private boolean shootOut;
   private boolean markedTransformation;
   private boolean didFirstTransform;
   public boolean shootOutFlag;
   public boolean shootOutExecute;
   public int shootOutCount;
   private int level;
   private long rotDelay;
   private long rotDelayAcc;
   private long movDelay;
   private long movDelayAcc;
   private long DELAY_ROT = 1500L;
   private long DELAY_MOV = 1500L;
   private boolean waitingShootout;
   private long checkedExpected;
   private final ObjectArrayFIFOQueue onDock = new ObjectArrayFIFOQueue();
   private final ObjectArrayFIFOQueue onUndock = new ObjectArrayFIFOQueue();
   private boolean allConnectionsLoaded;
   public RailController.RailReset activeRailReset;
   public RailRelation.DockValidity currentDockingValidity;
   private RailController.RailDockerUsableDocked dockedUsable;
   private boolean initDockUsable;
   private int dockCheckCount;
   private SegmentPiece tmpPc;
   public Vector3f railedInertia;
   private boolean debugFlag;
   private final ObjectOpenHashSet expectedToDock;
   public Transform railMovingLocalTransformTargetBef;
   private static boolean verboseTag = false;

   public RailController(SegmentController var1) {
      this.currentDockingValidity = RailRelation.DockValidity.UNKNOWN;
      this.tmpPc = new SegmentPiece();
      this.railedInertia = new Vector3f();
      this.expectedToDock = new ObjectOpenHashSet();
      this.railMovingLocalTransformTargetBef = new Transform();
      this.self = var1;
      this.relativeToRootLocalTransform = new Transform();
      this.railMovingLocalTransform = new Transform();
      this.railOriginalLocalTransform = new Transform();
      this.railMovingLocalAtDockTransform = new Transform();
      this.railTurretMovingLocalTransform = new Transform();
      this.basicRelativeTransform = new Transform();
      this.railUpToThisOriginalLocalTransform = new Transform();
      this.railTurretMovingLocalTransformTarget = new Transform();
      this.potentialRelativeToRootLocalTransform = new Transform();
      this.potentialRailOriginalLocalTransform = new Transform();
      this.potentialRailTurretMovingLocalTransform = new Transform();
      this.potentialRailMovingLocalTransform = new Transform();
      this.potentialBasicRelativeTransform = new Transform();
      this.potentialRailUpToThisOriginalLocalTransform = new Transform();
      this.resetTransformations();
   }

   public void onUndock(RailController.RailTrigger var1) {
      assert this.isOnServer();

      this.onUndock.enqueue(var1);
   }

   public void onDock(RailController.RailTrigger var1) {
      assert this.isOnServer();

      this.onDock.enqueue(var1);
   }

   public static RailRequest fromTagR(Tag var0, int var1, Set var2) {
      RailRequest var3 = null;
      if (var0.getType() == Tag.Type.STRUCT) {
         byte var4;
         Tag[] var5;
         switch(var4 = (var5 = var0.getStruct())[0].getByte()) {
         case 1:
            var3 = fromTagDocked(var5[1], var1);
            break;
         case 2:
            fromTagRailRoot(var5[1]);
            break;
         case 3:
            var3 = fromTagActiveRequest(var5[1], var1);
            break;
         default:
            assert false : var4;
         }

         if (var5.length > 2 && var5[2].getType() == Tag.Type.STRUCT) {
            fromUIDTag(var5[2], var1, var2);
         }
      } else {
         assert var0.getType() == Tag.Type.BYTE;
      }

      return var3;
   }

   private static void fromUIDTag(Tag var0, int var1, Set var2) {
      Tag[] var5 = var0.getStruct();

      for(int var3 = 0; var3 < var5.length - 1; ++var3) {
         if (var5[var3].getType() == Tag.Type.STRUCT) {
            try {
               var2.add(RailRequest.readFromTag(var5[var3], var1));
            } catch (Exception var4) {
               var4.printStackTrace();
            }
         }
      }

   }

   private static RailRequest fromTagRailRoot(Tag var0) {
      return null;
   }

   private static RailRequest fromTagActiveRequest(Tag var0, int var1) {
      RailRequest var2;
      if ((var2 = RailRequest.readFromTag(var0.getStruct()[0], var1)).disconnect) {
         var2 = null;
      } else {
         System.err.println("[SERVER][RAIL] loaded rail relation from tag: " + var2);
      }

      return var2;
   }

   private static RailRequest fromTagDocked(Tag var0, int var1) {
      return fromTagActiveRequest(var0, var1);
   }

   public static Tag addPostfixToTagUIDS(Tag var0, String var1, int var2) {
      ObjectOpenHashSet var3 = new ObjectOpenHashSet();
      RailRequest var4;
      if ((var4 = fromTagR(var0, var2, var3)) != null) {
         StringBuilder var10000 = new StringBuilder();
         VoidUniqueSegmentPiece var10002 = var4.rail;
         var10002.uniqueIdentifierSegmentController = var10000.append(var10002.uniqueIdentifierSegmentController).append(var1).toString();
         var10000 = new StringBuilder();
         var10002 = var4.docked;
         var10002.uniqueIdentifierSegmentController = var10000.append(var10002.uniqueIdentifierSegmentController).append(var1).toString();
         System.err.println("[RAIL] LOADED TAG AND APPENDED: " + var4.rail.uniqueIdentifierSegmentController + " -> " + var4.docked.uniqueIdentifierSegmentController);
         System.err.println("[RAIL] LOADED TAG EXPECTED: " + var3);
         return getRequestTag(var4, postfixExpected(var3, var1));
      } else {
         return getRootRailTag(postfixExpected(var3, var1));
      }
   }

   private static Tag postfixExpected(Collection var0, String var1) {
      Tag[] var2 = new Tag[var0.size() + 1];
      int var3 = 0;

      for(Iterator var5 = var0.iterator(); var5.hasNext(); ++var3) {
         RailRequest var4;
         (var4 = (RailRequest)var5.next()).docked.uniqueIdentifierSegmentController = var4.docked.uniqueIdentifierSegmentController + var1;
         var2[var3] = var4.getTag();
         System.err.println("[RAIL] ADDED POSTFIX TO EXPECTED: " + var4.docked.uniqueIdentifierSegmentController);
      }

      var2[var2.length - 1] = FinishTag.INST;
      return new Tag(Tag.Type.STRUCT, (String)null, var2);
   }

   private static Tag getRequestTag(RailRequest var0, Tag var1) {
      Tag var2 = new Tag(Tag.Type.BYTE, (String)null, (byte)3);
      Tag var3 = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var0.getTag(), FinishTag.INST});
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var2, var3, var1 != null ? var1 : new Tag(Tag.Type.BYTE, (String)null, (byte)0), FinishTag.INST});
   }

   public static boolean checkTurretBaseModifiable(SegmentController var0, SegmentController var1) {
      if (var1 instanceof PlayerControllable && !((PlayerControllable)var1).getAttachedPlayers().isEmpty()) {
         Iterator var2 = var1.railController.next.iterator();

         while(var2.hasNext()) {
            RailRelation var3;
            if ((var3 = (RailRelation)var2.next()).docked.getSegmentController() != var0 && var3.isTurretDock() && var1 instanceof PlayerControllable && !((PlayerControllable)var1).getAttachedPlayers().isEmpty()) {
               if (var0.isOnServer()) {
                  return false;
               }

               if (((GameClientState)var0.getState()).getPlayer().getName().compareToIgnoreCase(((PlayerState)((PlayerControllable)var1).getAttachedPlayers().get(0)).getName()) < 0) {
                  return false;
               }
            }
         }

         return true;
      } else {
         return true;
      }
   }

   public Tag getDockedUIDsTag(List var1, ObjectOpenHashSet var2) {
      Tag[] var3 = new Tag[var1.size() + var2.size() + 1];
      int var4 = 0;

      for(int var5 = 0; var5 < var1.size(); ++var5) {
         RailRequest var6 = ((RailRelation)var1.get(var5)).getRailRequest(this);
         var3[var4] = var6.getTag();
         ++var4;
      }

      for(Iterator var8 = var2.iterator(); var8.hasNext(); ++var4) {
         RailRequest var7 = (RailRequest)var8.next();
         var3[var4] = var7.getTag();
      }

      var3[var3.length - 1] = FinishTag.INST;
      return new Tag(Tag.Type.STRUCT, (String)null, var3);
   }

   public void disconnectClient() {
      this.requestDisconnect();
      this.lastDisconnect = System.currentTimeMillis();
   }

   private void requestDisconnect() {
      RailRequest var1;
      (var1 = new RailRequest()).disconnect = true;
      var1.sentFromServer = this.isOnServer();
      this.self.getNetworkObject().railRequestBuffer.add(new RemoteRailRequest(var1, this.self.isOnServer()));
      this.lastDisconnect = System.currentTimeMillis();
   }

   public void connectServer(SegmentPiece var1, SegmentPiece var2) {
      RailRequest var3;
      (var3 = this.getRailRequest(var1, var2, (Vector3i)null, (Vector3i)null, RailRelation.DockingPermission.NORMAL)).sentFromServer = false;
      this.self.onPhysicsRemove();
      this.self.setSectorId(var2.getSegmentController().getSectorId());
      if (this.railRequestCurrent == null && !this.isDockedOrDirty()) {
         this.railRequestCurrent = var3;
      }

   }

   public RailRequest getRailRequest(SegmentPiece var1, SegmentPiece var2, Vector3i var3, Vector3i var4, RailRelation.DockingPermission var5) {
      return getRailRequest(var1, var2, var3, var4, var5, this.railTurretMovingLocalTransformTarget, this.railMovingLocalTransform, this.railMovingLocalAtDockTransform, this.self, this.isOnServer());
   }

   public static RailRequest getRailRequest(SegmentPiece var0, SegmentPiece var1, Vector3i var2, Vector3i var3, RailRelation.DockingPermission var4, Transform var5, Transform var6, Transform var7, SegmentController var8, boolean var9) {
      RailRequest var10;
      (var10 = new RailRequest()).turretTransform.set(var5);
      var10.movedTransform.set(var6);
      var10.railMovingLocalAtDockTransform = new Transform(var7);
      var10.rail = new VoidUniqueSegmentPiece(var1);
      var10.docked = new VoidUniqueSegmentPiece(var0);
      var10.railMovingToDockerPosOnRail = var3;
      var10.dockingPermission = var4;
      if (var2 == null) {
         var1.getAbsolutePos(var10.railDockerPosOnRail);
         byte var11;
         if ((var11 = RailRelation.getOrientcubeAlgo(var1).getOrientCubePrimaryOrientation()) == 4 || var11 == 5) {
            var11 = (byte)Element.getOpposite(var11);
         }

         Vector3i var12 = Element.DIRECTIONSi[var11];
         var10.railDockerPosOnRail.add(var12);
         System.err.println("[RAIL] " + var8.getState() + " " + var8 + " DOCKED AT " + var10.railDockerPosOnRail + "; (Primary was: " + Element.getSideString(var11) + ")");
      } else {
         var10.railDockerPosOnRail.set(var2);
      }

      var10.sentFromServer = var9;
      return var10;
   }

   private void requestConnect(SegmentPiece var1, SegmentPiece var2, Vector3i var3, Vector3i var4, RailRelation.DockingPermission var5) {
      this.self.getNetworkObject().railRequestBuffer.add(new RemoteRailRequest(this.getRailRequest(var1, var2, var3, var4, var5), this.self.isOnServer()));
   }

   private void handleRailRequestConnect(RemoteRailRequest var1) {
      this.railRequests.enqueue(var1.get());
   }

   public boolean hasInitialRailRequest() {
      return !this.railRequests.isEmpty() || this.railRequestCurrent != null;
   }

   private void handleRailRequestConnectServer(RailRequest var1) {
      if (!var1.disconnect && var1.rail.getSegmentController() == null) {
         var1.rail.setSegmentControllerFromUID(this.getState());
      }

      this.self.onPhysicsRemove();
      this.railRequestCurrent = var1;
   }

   private void handleRailRequestConnectClient(RailRequest var1) {
      this.self.onPhysicsRemove();
      this.railRequestCurrent = var1;
   }

   public void updateToFullNetworkObject() {
      if (this.railRequestCurrent != null) {
         this.self.getNetworkObject().railRequestBuffer.add(new RemoteRailRequest(this.railRequestCurrent, this.self.isOnServer()));
         this.railRequestCurrent.sentFromServer = true;
      } else {
         if (this.previous != null) {
            this.requestConnect(this.previous.docked, this.previous.rail, this.previous.currentRailContact, this.previous.railContactToGo, this.previous.dockingPermission);
         }

      }
   }

   public void updateToNetworkObject() {
      if (this.railRequestCurrent != null && !this.railRequestCurrent.sentFromServer) {
         this.self.getNetworkObject().railRequestBuffer.add(new RemoteRailRequest(this.railRequestCurrent, this.self.isOnServer()));
         this.railRequestCurrent.sentFromServer = true;
      }

   }

   public void sendClientTurretResetRequest() {
      assert !this.isOnServer();

      System.err.println("[CLIENT] sending turret reset request");
      this.self.getNetworkObject().railMoveToPos.add(new RemoteRailMoveRequest(new RailMove(true), this.isOnServer()));
   }

   public void flagResetTurretServer() {
      this.requestedToDefaultFlag = true;
   }

   private void resetTurretServer() {
      assert this.isOnServer();

      if (this.isDockedAndExecuted() && this.previous.isTurretDockBasic() && !this.isChainSendFromClient()) {
         this.railTurretMovingLocalTransformTarget.setIdentity();
         this.railTurretMovingLocalTransform.setIdentity();
         this.self.getNetworkObject().railMoveToPos.add(new RemoteRailMoveRequest(new RailMove(true), this.isOnServer()));
      }

      Iterator var1 = this.next.iterator();

      while(var1.hasNext()) {
         ((RailRelation)var1.next()).docked.getSegmentController().railController.resetTurretServer();
      }

   }

   public void updateFromNetworkObject() {
      int var1;
      for(var1 = 0; var1 < this.self.getNetworkObject().railRequestBuffer.getReceiveBuffer().size(); ++var1) {
         RemoteRailRequest var2 = (RemoteRailRequest)this.self.getNetworkObject().railRequestBuffer.getReceiveBuffer().get(var1);
         this.handleRailRequestConnect(var2);
      }

      for(var1 = 0; var1 < this.self.getNetworkObject().railMoveToPos.getReceiveBuffer().size(); ++var1) {
         RailMove var6 = (RailMove)((RemoteRailMoveRequest)this.self.getNetworkObject().railMoveToPos.getReceiveBuffer().get(var1)).get();
         if (this.isOnServer()) {
            if (var6.isTurretBackToDefault) {
               this.requestedToDefaultFlag = true;
            }
         } else if (this.previous != null) {
            if (var6.shootOut) {
               this.shootOutFlag = true;
            }

            if (var6.isTurretBackToDefault) {
               assert !this.isOnServer();

               System.err.println("[CLIENT] received turret reset request for " + this.self);
               this.railTurretMovingLocalTransformTarget.setIdentity();
               this.railTurretMovingLocalTransform.setIdentity();
            } else {
               byte var3 = var6.rotationSide;
               byte var4;
               if ((var4 = var6.rotationCode) != RailRelation.RotationType.values().length) {
                  if (var6.hasTranslation) {
                     if (this.railMovingProspectedPos != null && this.previous.railContactToGo != null && this.railMovingProspectedRots.isEmpty()) {
                        this.prefRot.origin.set(this.railMovingLocalTransform.origin);
                        Vector3f var11;
                        (var11 = new Vector3f()).sub(this.railMovingProspectedPos, this.railMovingLocalTransform.origin);
                        if (var11.length() > 1.9F) {
                           this.railMovingLocalTransform.origin.set(this.railMovingProspectedPos);
                           this.previous.currentRailContact.set(this.previous.railContactToGo);
                           this.railMovingLocalAtDockTransform.origin.set(this.railMovingLocalTransform.origin);
                        }
                     }

                     this.previous.railContactToGo = new Vector3i(var6.toGoX, var6.toGoY, var6.toGoZ);
                  }

                  if (var6.hasLastRailRotation) {
                     this.railMovingLocalTransform.basis.set(new Quat4f(var6.lastRailTargetX, var6.lastRailTargetY, var6.lastRailTargetZ, var6.lastRailTargetW));
                     this.railMovingProspectedRots.clear();
                     this.railMovingLocalAtDockTransform.basis.set(this.railMovingLocalTransform.basis);
                  }

                  this.previous.rotationCode = RailRelation.RotationType.values()[var4];
                  this.previous.rotationSide = (byte)var3;
                  this.railSpeedPercent = var6.speedPercent;
                  this.applyRailGoTo();
               } else if (!this.railMovingProspectedRots.isEmpty()) {
                  int var5 = (int)this.railMovingProspectedRotTime;
                  ObjectArrayList var7 = new ObjectArrayList();
                  Quat4f var9 = Quat4fTools.set(this.railMovingLocalAtDockTransform.basis, new Quat4f());

                  for(int var10 = var5 - 1; var10 >= 0; --var10) {
                     var7.add(this.railMovingProspectedRots.get(var10));
                  }

                  var7.add(var9);
                  this.railMovingProspectedRots.clear();
                  this.railMovingProspectedRots.addAll(var7);
                  this.railMovingProspectedRotTime = 0.0F;
                  this.railMovingProspectedRotSegmentTime = 0.0F;
                  this.colRotTimer = 0.0F;
               }
            }
         }
      }

      if (!this.self.getRemoteTransformable().isSendFromClient()) {
         RemoteVector4f var8;
         for(var1 = 0; var1 < this.self.getNetworkObject().railTurretTransSecondary.getReceiveBuffer().size(); ++var1) {
            var8 = (RemoteVector4f)this.self.getNetworkObject().railTurretTransSecondary.getReceiveBuffer().get(var1);
            this.turretRotX = new Transform();
            this.turretRotX.setIdentity();
            this.turretRotX.basis.set(var8.getVector(new Quat4f()));
         }

         for(var1 = 0; var1 < this.self.getNetworkObject().railTurretTransPrimary.getReceiveBuffer().size(); ++var1) {
            var8 = (RemoteVector4f)this.self.getNetworkObject().railTurretTransPrimary.getReceiveBuffer().get(var1);
            this.turretRotYHelp = new Matrix3f();
            this.turretRotYHelp.set(var8.getVector(new Quat4f()));
            this.recY = true;
         }
      }

   }

   public void connect(SegmentPiece var1, SegmentPiece var2, Vector3i var3, Vector3i var4, boolean var5, boolean var6, RailRelation.DockingPermission var7, boolean var8, boolean var9) {
      if (!this.self.getDockingController().isInAnyDockingRelation() && !var2.getSegmentController().getDockingController().isInAnyDockingRelation()) {
         if (!this.isDocked()) {
            if (var2.getSegmentController().railController.getRoot() == ((SegmentPiece)var1).getSegmentController()) {
               System.err.println("[RAIL][CONNECT] " + this.self.getState() + " " + this.self + " CANNOT DOCK: LOOP DETECTED");
               ((SegmentPiece)var1).getSegmentController().sendControllingPlayersServerMessage(new Object[]{112}, 3);
               return;
            }

            if (var2.getType() == 679) {
               VoidUniqueSegmentPiece var10;
               (var10 = new VoidUniqueSegmentPiece()).setSegmentController(((SegmentPiece)var1).getSegmentController());
               var10.setType((short)1);
               var10.voidPos.set(16, 16, 16);
               var1 = var10;
            }

            this.lastLinearVelocityFromRoot.set(0.0F, 0.0F, 0.0F);
            SegmentController var13 = var2.getSegmentController();
            RailRelation var11;
            (var11 = new RailRelation((SegmentPiece)var1, var2)).currentRailContact.set(var3);
            this.previous = var11;
            this.dockedSince = System.currentTimeMillis();
            if (var5) {
               var11.setInRotationServer();
            }

            if (!var13.railController.next.contains(var11)) {
               var13.railController.next.add(var11);
            }

            assert this.self == var11.docked.getSegmentController() : this.self + " " + var11.docked.getSegmentController();

            assert this.previous == var11.docked.getSegmentController().railController.previous : this.self + " " + var11.docked.getSegmentController();

            assert this.previous != null : this.self + " -> " + var13;

            assert this.getRoot().railController.treeValid() : this.self;

            if (this.self != var11.docked.getSegmentController()) {
               throw new IllegalArgumentException("Ship to dock isnt the same as self: " + this.self + " " + var11.docked.getSegmentController() + "; This could mean that there are 2 ships with the same UID");
            }

            this.getRoot().railController.calculatePotentialChainTranformsRecursive(true);
            if (((SegmentPiece)var1).getSegmentController().isVirtualBlueprint() && var2.getSegmentController().isVirtualBlueprint()) {
               var9 = true;
            }

            var5 = false;
            if (!var9) {
               var5 = this.collisionChecker.checkPotentialCollisionWithRail(this.previous.docked.getSegmentController(), (SegmentController[])null, true);
            }

            if (!this.isOnServer()) {
               this.previous.dockingPermission = var7;
            }

            boolean var14 = true;
            if (this.isOnServer()) {
               var14 = this.checkFactionAllowed(this.previous, var7);
            }

            if (this.getDeepness() > ((GameStateInterface)this.getState()).getGameState().getMaxChainDocking()) {
               System.err.println("[RAIL][CONNECT] " + this.self.getState() + " " + this.self + " CANNOT DOCK: MAX CHAINS " + this.getDeepness() + " / " + ((GameStateInterface)this.getState()).getGameState().getMaxChainDocking());
               this.disconnectFromChain();
               if (this.isOnServer()) {
                  ((SegmentPiece)var1).getSegmentController().sendControllingPlayersServerMessage(new Object[]{113, ((GameStateInterface)this.getState()).getGameState().getMaxChainDocking()}, 3);
               }

               this.markTreeDirty();
               return;
            }

            if (this.isOnServer() && !var14 && !((SegmentPiece)var1).getSegmentController().isVirtualBlueprint()) {
               System.err.println("[RAIL][CONNECT] " + this.self.getState() + " " + this.self + " CANNOT DOCK: FACTION NOT ALLOWED");
               this.disconnectFromChain();
               ((SegmentPiece)var1).getSegmentController().sendControllingPlayersServerMessage(new Object[]{114}, 3);
               this.markTreeDirty();
               return;
            }

            if (var5) {
               System.err.println("[RAIL][CONNECT] " + this.self.getState() + " " + this.self + " CANNOT DOCK: COLLIDES");
               this.disconnectFromChain();
               if (this.isOnServer()) {
                  ((SegmentPiece)var1).getSegmentController().sendControllingPlayersServerMessage(new Object[]{115}, 3);
               }

               if (this.isOnServer() && ((SegmentPiece)var1).getSegmentController().isVirtualBlueprint()) {
                  try {
                     throw new Exception("[SERVER] Exception: undocked virtual blueprint. Removing " + ((SegmentPiece)var1).getSegmentController() + ". was igonored collision: " + var9);
                  } catch (Exception var12) {
                     var12.printStackTrace();
                     ((SegmentPiece)var1).getSegmentController().markForPermanentDelete(true);
                  }
               }

               this.markTreeDirty();
               return;
            }

            if (!var8 && this.isOnServer()) {
               this.onServerDocking((SegmentPiece)var1, var2);
            } else if (!this.isOnServer()) {
               this.onDockClient();
            }

            if (this.isOnServer() && !var6) {
               if (((SegmentPiece)var1).getSegmentController() instanceof PlayerControllable && !((PlayerControllable)((SegmentPiece)var1).getSegmentController()).getAttachedPlayers().isEmpty()) {
                  ((SegmentPiece)var1).getSegmentController().lastDockerPlayerServerLowerCase = ((PlayerState)((PlayerControllable)((SegmentPiece)var1).getSegmentController()).getAttachedPlayers().get(0)).getName().toLowerCase(Locale.ENGLISH);
               }

               this.requestConnect((SegmentPiece)var1, var2, var3, var4, this.previous.dockingPermission);
            }

            this.markTreeDirty();
         }
      } else if (this.isOnServer()) {
         ((SegmentPiece)var1).getSegmentController().sendControllingPlayersServerMessage(new Object[]{111}, 3);
         return;
      }

   }

   public int getDeepness() {
      return this.getDeepnessRec(0);
   }

   public int getDockingDepthFromHere() {
      return this.getDockingDepthFromHere(0);
   }

   private int getDockingDepthFromHere(int var1) {
      if (this.next.size() > 0) {
         ++var1;
      }

      RailRelation var3;
      for(Iterator var2 = this.next.iterator(); var2.hasNext(); var1 = Math.max(var1, var3.getDockedRController().getDockingDepthFromHere(var1))) {
         var3 = (RailRelation)var2.next();
      }

      return var1;
   }

   private int getDeepnessRec(int var1) {
      while(this.isDocked()) {
         RailController var10000 = this.previous.getRailRController();
         ++var1;
         this = var10000;
      }

      return var1;
   }

   public void disconnect() {
      if (this.previous != null) {
         if (this.isOnServer() && this.self.isVirtualBlueprint()) {
            this.self.setVirtualBlueprintRecursive(true);

            try {
               ((GameServerState)this.getState()).getController().writeSingleEntityWithDock(this.self);
            } catch (IOException var3) {
               var3.printStackTrace();
            } catch (SQLException var4) {
               var4.printStackTrace();
            }

            this.self.setMarkedForDeleteVolatileIncludingDocks(true);
         }

         if (this.isOnServer()) {
            this.onUndockedServer(this.previous.currentRailContact);
         } else {
            this.onUndockClient();
         }

         this.previous.getCurrentRailContactPiece(this.tmpPieces);
         if (!this.shootOutExecute && (this.isOnServer() || !this.shootOutFlag)) {
            for(int var1 = 0; var1 < this.tmpPieces.length; ++var1) {
               SegmentPiece var2;
               if ((var2 = this.tmpPieces[var1]) != null && var2.getType() == 939) {
                  System.err.println("[RAIL][" + this.getState() + "] " + this + " disconnecting from shootout rail. PREPARING SHOOTOUT!");
                  if (this.lastMovementDirRelativeToRoot.lengthSquared() == 0.0F) {
                     this.lastMovementDirRelativeToRoot.set(1.0E-4F, 0.0F, 0.0F);
                  }

                  this.shootOut = true;
                  break;
               }
            }
         } else if (!this.isOnServer() && this.shootOutFlag && this.lastMovementDirRelativeToRoot.lengthSquared() == 0.0F) {
            System.err.println("[CLIENT][RAIL][SHOOTOUT] Special case (not moved yet) " + this.self);
            this.waitingShootout = true;
         } else {
            System.err.println("[RAIL][DISCONNECT] SHOOTOUT FOR " + this.getState() + ": " + this.self + "; DIR: " + this.lastMovementDirRelativeToRoot);
            this.shootOut = true;
            this.shootOutExecute = false;
            this.shootOutFlag = false;
         }

         this.getRoot().railController.removeObjectPhysicsRecusively();
         System.err.println("[RAIL] " + this.self.getState() + " DISCONNECTING FROM RAIL: " + this.self + " (was connected to rail: " + this.previous.rail.getSegmentController() + ")");
         RailRelation var5;
         (var5 = this.disconnectFromChain()).getRailRController().markTreeDirty();
         this.markTreeDirty();

         assert this.dirty;

         if (this.isOnServer()) {
            this.requestDisconnect();
         }

         System.err.println("[RAIL] " + this.self.getState() + " DISCONNECTED FROM RAIL: " + this.self + " LEFT IN RAIL " + var5.rail.getSegmentController() + ": " + var5.getRailRController().next);
         this.lastDisconnect = System.currentTimeMillis();
         this.resetTransformations();
      }

   }

   private void onUndockClient() {
      ((GameClientState)this.getState()).onDockChanged(this.self, false);
      this.self.onDockingChanged(false);
      if (this.dockedUsable != null) {
         this.dockedUsable.man.removePlayerUsable(this.dockedUsable);
      }

   }

   private void onDockClient() {
      ((GameClientState)this.getState()).onDockChanged(this.self, true);
      this.self.onDockingChanged(true);
      if (this.dockedUsable != null) {
         this.dockedUsable.man.addPlayerUsable(this.dockedUsable);
      }

   }

   public boolean isOnServer() {
      return this.self.isOnServer();
   }

   private RailRelation disconnectFromChain() {
      RailRelation var1 = this.previous;
      ((RigidBody)this.getRoot().getPhysicsDataContainer().getObject()).getLinearVelocity(this.lastLinearVelocityFromRoot);
      boolean var2 = var1.getRailRController().next.remove(var1);

      assert var2 : "not removed: " + var1 + ": in col: " + var1.getRailRController().next;

      assert !var1.getRailRController().next.contains(var1) : "still contains: " + var1 + ": in col: " + var1.getRailRController().next;

      Iterator var4 = var1.getRailRController().next.iterator();

      RailRelation var3;
      do {
         if (!var4.hasNext()) {
            this.railMovingProspectedRots.clear();
            this.railMovingProspectedPos = null;
            this.railMovingProspectedRotSegmentTime = 0.0F;
            this.railMovingProspectedRotTime = 0.0F;
            this.previous = null;
            return var1;
         }

         var3 = (RailRelation)var4.next();

         assert var3.docked.getSegmentController() != this.self : var3;
      } while($assertionsDisabled || var3.rail.getSegmentController() != this.self);

      throw new AssertionError(var3);
   }

   private void resetTransformations() {
      this.relativeToRootLocalTransform.setIdentity();
      this.railOriginalLocalTransform.setIdentity();
      this.railMovingLocalTransform.setIdentity();
      this.railMovingLocalAtDockTransform.setIdentity();
      this.railTurretMovingLocalTransform.setIdentity();
      this.basicRelativeTransform.setIdentity();
      this.railUpToThisOriginalLocalTransform.setIdentity();
      this.railTurretMovingLocalTransformTarget.setIdentity();
      this.potentialRelativeToRootLocalTransform.setIdentity();
      this.potentialRailOriginalLocalTransform.setIdentity();
      this.potentialRailTurretMovingLocalTransform.setIdentity();
      this.potentialBasicRelativeTransform.setIdentity();
      this.potentialRailUpToThisOriginalLocalTransform.setIdentity();
      this.potentialRailMovingLocalTransform.setIdentity();
      this.railTurretMovingLocalTransformLastUpdate = new Transform();
      this.railMovingLocalTransformLastUpdate = new Transform();
      this.railTurretMovingLocalTransformLastUpdateR = new Transform();
      this.railMovingLocalTransformLastUpdateR = new Transform();
      this.railMovingLocalTransformTargetBef.setIdentity();
      this.prefRot.setIdentity();
      this.railMovingProspectedRots.clear();
      this.turretRotX = null;
      this.turretRotYHelp = null;
      this.didFirstTransform = false;
   }

   public void update(Timer var1) {
      if (!this.initDockUsable) {
         if (this.self instanceof ManagedSegmentController) {
            this.dockedUsable = new RailController.RailDockerUsableDocked(((ManagedSegmentController)this.self).getManagerContainer(), this.self);
         } else {
            this.dockedUsable = null;
         }

         this.initDockUsable = true;
      }

      if (this.dockedUsable != null && !this.isDockedAndExecuted()) {
         this.dockedUsable.disReq = false;
      }

      if (this.activeRailReset != null) {
         assert this.isOnServer();

         if (this.activeRailReset.waitingForUndock) {
            System.err.println("[RAILRESET] disconnect from rail by reset: " + this.self);
            this.disconnect();
            this.activeRailReset.waitingForUndock = false;
         } else {
            System.err.println("[RAILRESET] redock after reset: " + this.self);
            this.connectServer(this.activeRailReset.docker, this.activeRailReset.rail);
            this.activeRailReset = null;
         }
      }

      if (this.isOnServer() && this.requestedToDefaultFlag) {
         System.err.println("[SERVER] resetting turret for: " + this.self);
         this.resetTurretServer();
         this.requestedToDefaultFlag = false;
      }

      if (this.expectedToDock.size() > 0 && var1.currentTime - this.self.getTimeCreated() > 30000L && var1.currentTime - this.checkedExpected > 15000L) {
         ++this.dockCheckCount;
         if (this.isOnServer()) {
            GameServerState var2 = (GameServerState)this.getState();
            ObjectIterator var3 = this.expectedToDock.iterator();

            label238:
            while(true) {
               while(true) {
                  while(true) {
                     if (!var3.hasNext()) {
                        break label238;
                     }

                     RailRequest var4 = (RailRequest)var3.next();
                     Sendable var5;
                     if ((var5 = (Sendable)var2.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var4)) == null) {
                        DatabaseEntry var21;
                        if ((var21 = var2.getDatabaseIndex().getTableManager().getEntityTable().getEntryForFullUID(var4.docked.uniqueIdentifierSegmentController)) == null) {
                           try {
                              throw new Exception("[SERVER][RAIL] TODOCKCHECK " + this.self + " in " + this.self.getSector(new Vector3i()) + " tried to find toDock " + var4 + "; and found no trace of the entity loaded and in the database");
                           } catch (Exception var11) {
                              var11.printStackTrace();
                              System.err.println("[SERVER][RAIL] " + this.self + " REMOVING DOCKING CHECK FOR NON-EXISTENT OBJECT: " + var4);
                              var3.remove();
                           }
                        } else {
                           System.err.println("[SERVER][RAIL] TODOCKCHECK " + this.self + " in " + this.self.getSector(new Vector3i()) + " tried to find toDock " + var4 + "; and found unloaded database entity: " + var21);
                        }
                     } else {
                        SegmentController var6 = (SegmentController)var5;
                        System.err.println("[SERVER][RAIL] TODOCKCHECK " + this.self + " in " + this.self.getSector(new Vector3i()) + " tried to find toDock " + var4 + "; and found existing loaded entity: " + var6 + " in " + var6.getSector(new Vector3i()));
                        var6.railController.railRequestCurrent = var4;
                     }
                  }
               }
            }
         }

         if (this.expectedToDock.size() > 0) {
            Iterator var12;
            try {
               throw new Exception("[RAIL] Object " + this.self + " still needs entities to dock on it: " + this.expectedToDock + "; currently docked: " + this.next);
            } catch (Exception var7) {
               var7.printStackTrace();
               var12 = this.next.iterator();
            }

            label217:
            while(var12.hasNext()) {
               RailRelation var15 = (RailRelation)var12.next();
               ObjectIterator var19 = this.expectedToDock.iterator();

               while(true) {
                  if (var19.hasNext()) {
                     RailRequest var22 = (RailRequest)var19.next();
                     if (!var15.docked.equalsUniqueIdentifier(var22.docked.uniqueIdentifierSegmentController)) {
                        continue;
                     }

                     try {
                        throw new Exception("[RAIL][WARNING] (non serious) FOUND DOCKED OBJECT FOR Object " + this.self + " still needs entities to dock on it: " + this.expectedToDock + "; FOUND: " + var15.docked.getSegmentController().getUniqueIdentifierFull());
                     } catch (Exception var10) {
                        var10.printStackTrace();
                        var19.remove();
                     }
                  }

                  try {
                     Iterator var23 = this.expectedToDock.iterator();

                     String[] var18;
                     RailRequest var20;
                     do {
                        if (!var23.hasNext()) {
                           continue label217;
                        }

                        var20 = (RailRequest)var23.next();
                     } while(!var15.docked.getSegmentController().getUniqueIdentifier().startsWith(var20.docked.uniqueIdentifierSegmentController) || (var18 = var15.docked.getSegmentController().getUniqueIdentifier().substring(var20.docked.uniqueIdentifierSegmentController.length()).split("_")) == null || var18.length != 3);

                     this.expectedToDock.remove(var20);

                     try {
                        throw new Exception("[RAIL][WARNING] (non serious) FOUND DOCKED OBJECT FOR Object " + this.self + " SIMILAR: " + var20 + " WITH " + var15.docked.getSegmentController().getUniqueIdentifier());
                     } catch (Exception var8) {
                        var8.printStackTrace();
                     }
                  } catch (Exception var9) {
                     var9.printStackTrace();
                  }
                  break;
               }
            }
         }

         if (this.isOnServer() && this.expectedToDock.size() > 0 && this.dockCheckCount > 5 && this.dockCheckCount % 5 == 0) {
            ((GameServerState)this.getState()).getController().broadcastMessageAdmin(new Object[]{116, this.self.toNiceString(), this.expectedToDock.size()}, 3);
         }

         this.checkedExpected = var1.currentTime;
      }

      this.handleRailAutoMovementDecision(var1);
      if (this.isDockedAndExecuted() && var1.currentTime - this.lastValidityCheck > 800L) {
         RailRelation.DockValidity var13 = this.previous.getDockingValidity();
         this.previous.docked.getSegmentController().railController.currentDockingValidity = var13;
         switch(var13) {
         case SHIPYARD_FAILED:
            if (this.isOnServer()) {
               if (!this.self.isMarkedForDeleteVolatile()) {
                  System.err.println("[RAIL][SERVER] DISCONNECT FROM RAIL " + this.self + "! track became invalid");
                  this.disconnect();
               } else {
                  System.err.println("[RAIL][SERVER] ***NOT*** DISCONNECTING FROM RAIL " + this.self + " when track became invalid. object was already marked for volatile removal");
               }
            }
         case RAIL_DOCK_MISSING:
         case TRACK_MISSING:
         case OK:
         case UNKNOWN:
         default:
            this.lastValidityCheck = var1.currentTime;
         }
      }

      if (!this.railRequests.isEmpty()) {
         while(!this.railRequests.isEmpty()) {
            RailRequest var14 = (RailRequest)this.railRequests.dequeue();
            if (this.debugFlag) {
               System.err.println("[RAILDEBUG] " + this.self + ": ACTIVE RAIL QUEUE: " + var14);
            }

            if (this.self.isOnServer()) {
               this.handleRailRequestConnectServer(var14);
            } else {
               this.handleRailRequestConnectClient(var14);
            }
         }
      }

      if (!this.isOnServer() && this.shootOutFlag && this.lastMovementDirRelativeToRoot.lengthSquared() == 0.0F) {
         System.err.println("[CLIENT][RAIL] CLIENT IS WAITING FOR SHOOTOUT DIRECTION");
         this.waitingShootout = true;
      } else if (this.railRequestCurrent != null) {
         this.handleCurrentRailRequest();
      }

      if (this.self.getPhysicsDataContainer() != null) {
         this.self.getPhysicsDataContainer().updateWithoutPhysicsObjectInterfaceRail = this;
      }

      if (this.dirty && this.isRoot()) {
         this.recreateRootObjectPhysics();
         this.dirty = false;
      }

      for(int var16 = 0; var16 < this.next.size(); ++var16) {
         boolean var17 = false;
         if (!this.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().containsKey(((RailRelation)this.next.get(var16)).docked.getSegmentController().getId())) {
            System.err.println("[RAIL] " + this.getState() + " Object " + ((RailRelation)this.next.get(var16)).docked.getSegmentController() + " docked to " + this.self + " has been removed from game state");
            this.next.remove(var16);
            --var16;
            var17 = true;
         }

         if (var17) {
            this.markTreeDirty();
         }
      }

      if (this.isOnServer() && this.isDockedAndExecuted() && this.isTurretDocked() && this.markedTransformation && var1.currentTime - this.lastTurretColCheck > 500L) {
         label154: {
            this.getRoot().railController.calculatePotentialChainTranformsRecursive(true);
            if (this.collisionChecker.checkPotentialCollisionWithRail(this.previous.docked.getSegmentController(), (SegmentController[])null, false)) {
               if (this.lastCurretCollisionHappened == 0L) {
                  this.lastCurretCollisionHappened = var1.currentTime;
                  break label154;
               }

               if (this.getState().getUpdateTime() - this.lastTurretColCheck <= 5000L) {
                  break label154;
               }

               this.requestedToDefaultFlag = true;
            }

            this.lastCurretCollisionHappened = 0L;
         }

         this.lastTurretColCheck = this.getState().getUpdateTime();
         this.markedTransformation = false;
      }

      this.debugFlag = false;
   }

   private void handleCurrentRailRequest() {
      if (this.debugFlag) {
         System.err.println("[RAILDEBUG] " + this.self + ": ACTIVE RAIL REQUEST: " + this.railRequestCurrent);
      }

      if (this.railRequestCurrent.executionTime <= 0L) {
         this.railRequestCurrent.executionTime = System.currentTimeMillis();
      }

      if (this.isOnServer() && this.self.getSectorId() != this.getRoot().getSectorId()) {
         Sector var1 = ((GameServerState)this.getState()).getUniverse().getSector(this.getRoot().getSectorId());
         SectorSwitch var7;
         (var7 = new SectorSwitch(this.self, var1.pos, 1)).forceEvenOnDocking = true;

         try {
            System.err.println("[RAIL] Switching sector due to rail request: " + this.self + " " + this.self.getSectorId() + " -> (RootSector) " + this.getRoot().getSectorId());
            var7.execute((GameServerState)this.getState());
         } catch (IOException var5) {
            var5.printStackTrace();
         }
      }

      if (this.executeRailRequest(this.railRequestCurrent)) {
         if (!this.railRequestCurrent.disconnect) {
            this.railRequestCurrent.rail.getSegmentController().railController.removeExpectedByUid(this.self.getUniqueIdentifier());
         }

         this.railRequestCurrent = null;
      } else {
         boolean var8 = true;
         if (!this.isOnServer() && System.currentTimeMillis() < this.railRequestCurrent.executionTime + 10000L) {
            var8 = false;
         }

         System.err.println("[RAIL] Rail Request Error EXECUTION TIME: " + (System.currentTimeMillis() - this.railRequestCurrent.executionTime));
         if (var8) {
            Sendable var9 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getUidObjectMap().get(this.railRequestCurrent.rail.uniqueIdentifierSegmentController);
            Sendable var2 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getUidObjectMap().get(this.railRequestCurrent.docked.uniqueIdentifierSegmentController);

            try {
               throw new Exception("Dock Failed: Object to dock was not available " + this.self + ": " + this.railRequestCurrent + " :: " + var9 + " (" + this.railRequestCurrent.rail.uniqueIdentifierSegmentController + "); " + var2 + "(" + this.railRequestCurrent.docked.uniqueIdentifierSegmentController + ")");
            } catch (Exception var6) {
               var6.printStackTrace();
               if (this.isOnServer()) {
                  List var10;
                  DatabaseEntry var11;
                  try {
                     if ((var10 = ((GameServerState)this.getState()).getDatabaseIndex().getTableManager().getEntityTable().getByUIDExact(DatabaseEntry.removePrefixWOException(this.railRequestCurrent.docked.uniqueIdentifierSegmentController), 1)).size() > 0) {
                        var11 = (DatabaseEntry)var10.get(0);
                        System.err.println("[RAIL][ERROR] DOCKED FOUND " + var11.uid + " IN DATABASE IN SECTOR: " + var11.sectorPos);
                     } else {
                        System.err.println("[RAIL][ERROR] DOCKED NOT FOUND IN DATABASE FOR:  " + this.railRequestCurrent.docked.uniqueIdentifierSegmentController);
                     }
                  } catch (SQLException var4) {
                     var4.printStackTrace();
                  }

                  try {
                     if ((var10 = ((GameServerState)this.getState()).getDatabaseIndex().getTableManager().getEntityTable().getByUIDExact(DatabaseEntry.removePrefixWOException(this.railRequestCurrent.rail.uniqueIdentifierSegmentController), 1)).size() > 0) {
                        var11 = (DatabaseEntry)var10.get(0);
                        System.err.println("[RAIL][ERROR] RAIL FOUND " + var11.uid + " IN DATABASE IN SECTOR: " + var11.sectorPos);
                     } else {
                        System.err.println("[RAIL][ERROR] RAIL NOT FOUND IN DATABASE FOR:  " + this.railRequestCurrent.rail.uniqueIdentifierSegmentController);
                     }
                  } catch (SQLException var3) {
                     var3.printStackTrace();
                  }
               }

               this.railRequestCurrent = null;
               if (FactionManager.isNPCFaction(this.self.getFactionId()) && this.isOnServer()) {
                  this.destroyDockedRecursive();
               }

            }
         } else {
            System.err.println("[DOCK][" + this.getState() + "] Failed dock of " + this.railRequestCurrent);
         }
      }
   }

   private boolean removeExpectedByUid(String var1) {
      ObjectIterator var2 = this.expectedToDock.iterator();

      RailRequest var3;
      do {
         if (!var2.hasNext()) {
            return false;
         }

         var3 = (RailRequest)var2.next();
      } while(!var1.equals(var3.docked.uniqueIdentifierSegmentController));

      var2.remove();
      return true;
   }

   private RailManagerInterface getRailManagerInterfaceOfRail() {
      return this.isDockedAndExecuted() && this.previous.rail.getSegmentController() instanceof ManagedSegmentController && ((ManagedSegmentController)this.previous.rail.getSegmentController()).getManagerContainer() instanceof RailManagerInterface ? (RailManagerInterface)((ManagedSegmentController)this.previous.rail.getSegmentController()).getManagerContainer() : null;
   }

   public float getRailMassPercent() {
      RailManagerInterface var1;
      return (var1 = this.getRailManagerInterfaceOfRail()) != null ? ((RailMassEnhancerCollectionManager)var1.getRailMassEnhancer().getCollectionManager()).getRailPercent(this.calculateRailMassIncludingSelf()) : 1.0F;
   }

   private void handleRailAutoMovementDecision(Timer var1) {
      if (this.isOnServer() && this.isDockedAndExecuted() && this.shootOutExecute) {
         this.disconnect();
      } else {
         if (this.isOnServer() && this.isDockedAndExecuted() && this.isDidFirstTransformation() && !this.isTurretDocked() && !this.isShipyardDocked() && this.getRoot().isSegmentBufferFullyLoadedServerRailRec() && this.previous.railContactToGo == null && this.previous.rotationCode == RailRelation.RotationType.NONE && this.allConnectionsLoaded()) {
            if (this.previous.delayNextMoveSec > 0.0D) {
               RailRelation var10000 = this.previous;
               var10000.delayNextMoveSec -= (double)var1.getDelta();
               return;
            }

            this.previous.delayNextMoveSec = 0.0D;
            SegmentPiece[] var9 = this.previous.getCurrentRailContactPiece(this.tmpPieces);

            for(int var2 = 0; var2 < var9.length; ++var2) {
               SegmentPiece var3;
               if ((var3 = var9[var2]) != null) {
                  Oriencube var4;
                  byte var5;
                  if ((var5 = (var4 = RailRelation.getOrientcubeAlgo(var3)).getOrientCubeSecondaryOrientation()) == 5) {
                     var5 = 4;
                  } else if (var5 == 4) {
                     var5 = 5;
                  }

                  Vector3i var10 = new Vector3i(Element.DIRECTIONSi[var5]);
                  Vector3i var6 = new Vector3i();
                  var3.getAbsolutePos(var6);
                  var6.add(var10);
                  SegmentPiece var12 = this.previous.rail.getSegmentController().getSegmentBuffer().getPointUnsave(var6, this.tmpPiece);
                  if (var3.getType() == 939 && (var12 == null || !ElementKeyMap.isValidType(var12.getType()) || var12.getType() != 939 || var12.getFullOrientation() != var3.getFullOrientation())) {
                     System.err.println("[RAIL] Shootout. Reached end of shootout rail. Disconnecting " + this.self);
                     this.previous.railContactToGo = var12.getAbsolutePos(new Vector3i());
                     byte var14 = var4.getOrientCubePrimaryOrientationSwitchedLeftRight();
                     Vector3i var15 = new Vector3i(Element.DIRECTIONSi[var14]);
                     this.previous.railContactToGo.add(var15);
                     this.applyRailGoTo();
                     this.shootOutFlag = true;
                     RailMove var16;
                     (var16 = new RailMove(this.previous.railContactToGo, (byte)this.previous.rotationCode.ordinal(), this.previous.rotationSide, this.getLastProspectedRot(), this.railSpeedPercent)).shootOut = true;
                     this.self.getNetworkObject().railMoveToPos.add(new RemoteRailMoveRequest(var16, this.isOnServer()));
                     return;
                  }

                  if (var12 != null && ElementKeyMap.isValidType(var12.getType()) && ElementKeyMap.getInfo(var12.getType()).isRailTrack()) {
                     if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
                        var12.debugDrawPoint(this.previous.rail.getSegmentController().getWorldTransform(), 0.1F, 0.1F, 1.0F, 0.3F, 1.0F, 2000L);
                     }

                     Oriencube var11 = RailRelation.getOrientcubeAlgo(var12);
                     byte var7 = var4.getOrientCubePrimaryOrientationSwitchedLeftRight();
                     Vector3i var8 = new Vector3i(Element.DIRECTIONSi[var7]);
                     if (var7 == var11.getOrientCubePrimaryOrientationSwitchedLeftRight()) {
                        if (var12.getType() == 939) {
                           this.railSpeedPercent = 5.0F;
                        } else {
                           if (this.previous.rail.getSegmentController() instanceof ManagedSegmentController && ((ManagedSegmentController)this.previous.rail.getSegmentController()).getManagerContainer() instanceof RailManagerInterface) {
                              RailManagerInterface var13 = (RailManagerInterface)((ManagedSegmentController)this.previous.rail.getSegmentController()).getManagerContainer();
                              this.railSpeedPercent = ((RailSpeedElementManager)var13.getRailSpeed().getElementManager()).getRailSpeedForTrack(var3.getAbsoluteIndexWithType4());
                           } else {
                              this.railSpeedPercent = 0.5F;
                           }

                           if (this.railSpeedPercent == 0.0F) {
                              return;
                           }
                        }

                        if (!this.previous.doneInRotationServer() && (var3.getType() == 664 || var3.getType() == 669)) {
                           this.calcRotationDecision(var3, var4);
                        }

                        this.previous.railContactToGo = var12.getAbsolutePos(new Vector3i());
                        this.previous.railContactToGo.add(var8);
                        this.applyRailGoTo();
                        this.self.getNetworkObject().railMoveToPos.add(new RemoteRailMoveRequest(new RailMove(this.previous.railContactToGo, (byte)this.previous.rotationCode.ordinal(), this.previous.rotationSide, this.getLastProspectedRot(), this.railSpeedPercent), this.isOnServer()));
                     } else {
                        this.railRotDecision(var3, var4);
                     }
                  } else {
                     this.railRotDecision(var3, var4);
                  }
               }
            }
         }

      }
   }

   private boolean railRotDecision(SegmentPiece var1, Oriencube var2) {
      if (this.previous.rotationCode != RailRelation.RotationType.NONE || this.previous.doneInRotationServer() || var1.getType() != 664 && var1.getType() != 669) {
         return false;
      } else {
         if (this.previous.rail.getSegmentController() instanceof ManagedSegmentController && ((ManagedSegmentController)this.previous.rail.getSegmentController()).getManagerContainer() instanceof RailManagerInterface) {
            RailManagerInterface var3 = (RailManagerInterface)((ManagedSegmentController)this.previous.rail.getSegmentController()).getManagerContainer();
            this.railSpeedPercent = ((RailSpeedElementManager)var3.getRailSpeed().getElementManager()).getRailSpeedForTrack(var1.getAbsoluteIndexWithType4());
         } else {
            this.railSpeedPercent = 0.5F;
         }

         if (this.railSpeedPercent == 0.0F) {
            return false;
         } else {
            this.calcRotationDecision(var1, var2);
            this.applyRailGoTo();
            this.self.getNetworkObject().railMoveToPos.add(new RemoteRailMoveRequest(new RailMove((byte)this.previous.rotationCode.ordinal(), this.previous.rotationSide, this.getLastProspectedRot(), this.railSpeedPercent), this.isOnServer()));
            return true;
         }
      }
   }

   private boolean allConnectionsLoaded() {
      if (!this.allConnectionsLoaded) {
         SegmentPiece[] var1 = this.previous.getCurrentRailContactPiece(this.tmpPieces);

         for(int var2 = 0; var2 < var1.length; ++var2) {
            SegmentPiece var3;
            Short2ObjectOpenHashMap var4;
            FastCopyLongOpenHashSet var7;
            if ((var3 = var1[var2]) != null && (var4 = var3.getSegmentController().getControlElementMap().getControllingMap().get(var3.getAbsoluteIndex())) != null && (var7 = (FastCopyLongOpenHashSet)var4.get((short)405)) != null && var7.size() > 0) {
               Iterator var8 = var7.iterator();

               while(var8.hasNext()) {
                  long var5 = (Long)var8.next();
                  if (var3.getSegmentController().getSegmentBuffer().getPointUnsave(var5, this.tmpPieceR) == null) {
                     return false;
                  }
               }
            }
         }

         this.allConnectionsLoaded = true;
      }

      return true;
   }

   private boolean isDidFirstTransformation() {
      return this.didFirstTransform;
   }

   private Quat4f getLastProspectedRot() {
      Quat4f var1 = new Quat4f();
      Quat4fTools.set(this.railMovingLocalAtDockTransform.basis, var1);
      return var1;
   }

   private void calcRotationDecision(SegmentPiece var1, Oriencube var2) {
      this.previous.continueRotation = false;
      Short2ObjectOpenHashMap var3;
      FastCopyLongOpenHashSet var8;
      if ((var3 = var1.getSegmentController().getControlElementMap().getControllingMap().get(var1.getAbsoluteIndex())) != null && (var8 = (FastCopyLongOpenHashSet)var3.get((short)405)) != null && var8.size() > 0) {
         int var4 = 0;
         Iterator var9 = var8.iterator();

         while(var9.hasNext()) {
            long var6 = (Long)var9.next();
            SegmentPiece var5;
            if ((var5 = var1.getSegmentController().getSegmentBuffer().getPointUnsave(var6, this.tmpPieceR)) != null && var5.getType() == 405 && var5.isActive()) {
               ++var4;
            }
         }

         if (var4 > 0) {
            this.previous.rotationSide = var2.getOrientCubePrimaryOrientation();
            this.previous.rotationCode = RailRelation.RotationType.values()[Math.min(8, var4) + (var1.getType() == 669 ? 8 : 0)];
            if (var4 > 8) {
               this.previous.continueRotation = true;
               return;
            }
         } else {
            this.previous.setInRotationServer();
         }

      } else {
         this.previous.rotationSide = var2.getOrientCubePrimaryOrientation();
         this.previous.rotationCode = var1.getType() == 669 ? RailRelation.RotationType.CCW_90 : RailRelation.RotationType.CW_90;
      }
   }

   private void applyRailGoTo() {
      if (this.previous.railContactToGo != null) {
         Vector3f var1 = new Vector3f(this.railMovingLocalAtDockTransform.origin);
         Vector3f var2;
         (var2 = new Vector3f()).x = (float)(this.previous.railContactToGo.x - this.previous.currentRailContact.x);
         var2.y = (float)(this.previous.railContactToGo.y - this.previous.currentRailContact.y);
         var2.z = (float)(this.previous.railContactToGo.z - this.previous.currentRailContact.z);
         var1.add(var2);
         this.railMovingProspectedPos = var1;
         this.colLinTimer = 0.0F;
      }

      if (this.previous.rotationCode != RailRelation.RotationType.NONE) {
         Matrix3f var7 = new Matrix3f(this.railMovingLocalAtDockTransform.basis);
         Matrix3f var8;
         (var8 = new Matrix3f(this.railMovingLocalAtDockTransform.basis)).invert();
         Matrix3f[] var3 = this.previous.rotationCode.getRotation(this.previous.rotationSide);

         for(int var4 = 0; (float)var4 < this.previous.rotationCode.rad; ++var4) {
            Matrix3f var5 = new Matrix3f(this.railMovingLocalAtDockTransform.basis);
            Matrix3f var6;
            (var6 = new Matrix3f(var3[var4])).invert();
            var5.mul(var8);
            var5.mul(var6);
            var5.mul(var7);
            Quat4f var9 = new Quat4f();
            Quat4fTools.set(var5, var9);
            this.railMovingProspectedRots.add(var9);
         }

         assert (float)this.railMovingProspectedRots.size() == this.previous.rotationCode.rad : this.previous.rotationCode + ": " + this.previous.rotationCode.rad + " / " + this.railMovingProspectedRots.size();

         this.railMovingProspectedRotTime = 0.0F;
         this.railMovingProspectedRotSegmentTime = 0.0F;
      }

   }

   public StateInterface getState() {
      return this.self.getState();
   }

   private boolean executeRailRequest(RailRequest var1) {
      if (var1.disconnect) {
         this.disconnect();
         return true;
      } else {
         Sendable var2 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getUidObjectMap().get(var1.rail.uniqueIdentifierSegmentController);
         Sendable var3 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getUidObjectMap().get(var1.docked.uniqueIdentifierSegmentController);
         if (var2 != null && var3 != null) {
            SegmentController var4 = (SegmentController)var2;
            SegmentController var5 = (SegmentController)var3;
            var1.rail.setSegmentController(var4);
            var1.docked.setSegmentController(var5);
            var5.railController.resetTransformations();
            this.railMovingLocalAtDockTransform.set(var1.railMovingLocalAtDockTransform);
            this.railMovingLocalTransform.set(var1.movedTransform);
            this.railTurretMovingLocalTransform.set(var1.turretTransform);
            this.railTurretMovingLocalTransformTarget.set(this.railTurretMovingLocalTransform);
            this.connect(var1.docked, var1.rail, var1.railDockerPosOnRail, var1.railMovingToDockerPosOnRail, var1.didRotationInPlace, var1.sentFromServer, var1.dockingPermission, var1.fromtag, var1.ignoreCollision);
            return true;
         } else {
            System.err.println("[RAIL] Rail Request Failed: Not loaded: " + var1.rail.uniqueIdentifierSegmentController + "( " + (var2 == null ? "UNLOADED" : "LOADED AS " + var2) + ") or " + var1.docked.uniqueIdentifierSegmentController + "(" + (var3 == null ? "UNLOADED" : "LOADED AS " + var3) + ")");
            return false;
         }
      }
   }

   public void disconnectFromRailRailIfContact(SegmentPiece var1, long var2) {
      FastCopyLongOpenHashSet var8;
      if (this.isRail() && (var8 = (FastCopyLongOpenHashSet)this.self.getControlElementMap().getControllingMap().getAll().get(var1.getAbsoluteIndex())) != null) {
         Iterator var9 = var8.iterator();

         boolean var3;
         do {
            long var6;
            SegmentPiece var10;
            do {
               do {
                  if (!var9.hasNext()) {
                     return;
                  }

                  var6 = (Long)var9.next();
               } while((var10 = this.self.getSegmentBuffer().getPointUnsave(var6, this.tmpPc)) == null);
            } while(!ElementKeyMap.isValidType(var10.getType()));

            var3 = false;

            for(int var4 = 0; var4 < this.next.size(); ++var4) {
               RailRelation var5;
               if ((var5 = (RailRelation)this.next.get(var4)).isCurrentRailContactPiece(var10)) {
                  var5.docked.getSegmentController().railController.disconnect();
                  var3 = true;
                  break;
               }
            }
         } while(!var3);
      }

   }

   private void updateAutoRailMovementPos(Timer var1) {
      this.prefRot.origin.set(this.railMovingLocalTransform.origin);
      Vector3f var2;
      (var2 = new Vector3f()).sub(this.railMovingProspectedPos, this.railMovingLocalTransform.origin);
      Vector3f var3;
      (var3 = new Vector3f(var2)).normalize();
      float var4 = var1.getDelta() * this.getRailSpeed() * this.railSpeedPercent * this.getRailMassPercent();
      if (this.waitingShootout) {
         var4 = var1.getDelta() * 50.0F;
         this.waitingShootout = false;
      }

      var3.scale(var4);
      if (var2.lengthSquared() != 0.0F && var3.length() <= var2.length()) {
         this.railMovingLocalTransform.origin.add(var3);
      } else {
         this.railMovingLocalTransform.origin.set(this.railMovingProspectedPos);
      }

      this.getRoot().railController.calculatePotentialChainTranformsRecursive(true);
      this.colByLinearMovement = this.collisionChecker.checkPotentialCollisionWithRail(this.previous.docked.getSegmentController(), (SegmentController[])null, false);
      if (this.colByLinearMovement) {
         this.movDelay = this.DELAY_MOV;
         this.railMovingLocalTransform.origin.set(this.prefRot.origin);
         this.colLinTimer += var1.getDelta();
         this.colLinTimer += (float)((double)this.movDelayAcc / 1000.0D);
         this.movDelayAcc = 0L;
         if (this.colLinTimer >= 1.0F && this.isOnServer()) {
            this.previous.railContactToGo = new Vector3i(this.previous.currentRailContact);
            this.applyRailGoTo();
            this.previous.delayNextMoveSec = Math.random() * 5.0D;
            if (this.railSpeedPercent == 0.0F) {
               this.railSpeedPercent = 0.5F;
            }

            this.self.getNetworkObject().railMoveToPos.add(new RemoteRailMoveRequest(new RailMove(this.previous.railContactToGo, (byte)this.previous.rotationCode.ordinal(), this.previous.rotationSide, this.getLastProspectedRot(), this.railSpeedPercent), this.isOnServer()));
            return;
         }
      } else {
         if (this.railMovingProspectedPos.equals(this.railMovingLocalTransform.origin)) {
            if (this.isOnServer()) {
               this.onLeftBlockServer(this.previous.currentRailContact);
            }

            this.previous.currentRailContact.set(this.previous.railContactToGo);
            this.railMovingLocalAtDockTransform.origin.set(this.railMovingLocalTransform.origin);
            if (this.isOnServer()) {
               this.onReachedNextBlockServer(this.previous.currentRailContact);
            }

            this.railMovingProspectedPos = null;
            this.previous.railContactToGo = null;
         }

         this.colLinTimer = 0.0F;
      }

   }

   private void updateAutoRailMovementRot(Timer var1) {
      this.railSpeedRot = this.railSpeedRotBasis * this.railSpeedPercent * this.getRailMassPercent();
      this.prefRot.set(this.railMovingLocalTransform);
      boolean var2 = false;
      int var3 = (int)this.railMovingProspectedRotTime;
      if (this.railMovingProspectedRotTime < this.previous.rotationCode.rad && var3 < this.railMovingProspectedRots.size()) {
         Quat4f var4 = new Quat4f();
         if (var3 == 0) {
            Quat4fTools.set(this.railMovingLocalAtDockTransform.basis, var4);
         } else {
            var4.set((Tuple4f)this.railMovingProspectedRots.get(var3 - 1));
         }

         Quat4f var5 = new Quat4f();
         Quat4Util.slerp(var4, (Quat4f)this.railMovingProspectedRots.get(var3), this.railMovingProspectedRotSegmentTime, true, var5);
         MatrixUtil.setRotation(this.railMovingLocalTransform.basis, var5);
      } else {
         this.railMovingLocalTransform.basis.set((Quat4f)this.railMovingProspectedRots.get(this.railMovingProspectedRots.size() - 1));
         var2 = true;
      }

      if (var2) {
         this.railMovingProspectedRots.clear();
         this.previous.rotationCode = RailRelation.RotationType.NONE;
         this.railMovingLocalAtDockTransform.basis.set(this.railMovingLocalTransform.basis);
         if (!this.previous.continueRotation) {
            this.previous.setInRotationServer();
         }

         if (this.isOnServer() && this.railMovingProspectedPos == null) {
            this.onRotatedOnlyServer();
            return;
         }
      } else {
         this.getRoot().railController.calculatePotentialChainTranformsRecursive(true);
         if (this.collisionChecker.checkPotentialCollisionWithRail(this.previous.docked.getSegmentController(), (SegmentController[])null, false)) {
            this.rotDelay = this.DELAY_ROT;
            this.colRotTimer += var1.getDelta();
            this.colRotTimer += (float)((double)this.rotDelayAcc / 1000.0D);
            this.rotDelayAcc = 0L;
            if (this.colRotTimer > 1.0F && this.isOnServer()) {
               ObjectArrayList var8 = new ObjectArrayList();
               Quat4f var6 = Quat4fTools.set(this.railMovingLocalAtDockTransform.basis, new Quat4f());

               for(int var7 = var3 - 1; var7 >= 0; --var7) {
                  var8.add(this.railMovingProspectedRots.get(var7));
               }

               var8.add(var6);
               this.railMovingProspectedRots.clear();
               this.railMovingProspectedRots.addAll(var8);
               this.colRotTimer = 0.0F;
               this.railMovingProspectedRotTime = 0.0F;
               this.railMovingProspectedRotSegmentTime = 0.0F;
               this.self.getNetworkObject().railMoveToPos.add(new RemoteRailMoveRequest(new RailMove((byte)RailRelation.RotationType.values().length, (byte)0, this.getLastProspectedRot(), this.railSpeedPercent), this.isOnServer()));
            }

            this.railMovingLocalTransform.basis.set(this.prefRot.basis);
            return;
         }

         this.railMovingProspectedRotTime += var1.getDelta() * this.railSpeedRot;
         this.railMovingProspectedRotSegmentTime += var1.getDelta() * this.railSpeedRot;
         if (this.railMovingProspectedRotSegmentTime >= 1.0F) {
            --this.railMovingProspectedRotSegmentTime;
         }
      }

   }

   private void onRotatedOnlyServer() {
      assert this.isOnServer();

      SegmentPiece[] var1 = this.previous.getCurrentRailContactPiece(this.tmpPieces);

      for(int var2 = 0; var2 < var1.length; ++var2) {
         SegmentPiece var3;
         if ((var3 = var1[var2]) != null) {
            ((SendableSegmentController)this.previous.rail.getSegmentController()).activateSurroundServer(true, var3.getAbsolutePos(this.tmpPos), ElementKeyMap.getSignalTypesActivatedOnSurround());
         }
      }

   }

   private void onServerDocking(SegmentPiece var1, SegmentPiece var2) {
      ((SendableSegmentController)var1.getSegmentController()).activateSurroundServer(true, var1.getAbsolutePos(this.tmpPos), ElementKeyMap.getSignalTypesActivatedOnSurround());
      ((SendableSegmentController)var2.getSegmentController()).activateSurroundServer(true, var2.getAbsolutePos(this.tmpPos), ElementKeyMap.getSignalTypesActivatedOnSurround());
      System.err.println("[RAIL] On Docking, Speed: " + var1.getSegmentController().getSpeedCurrent());
      if (var2.getType() == 937) {
         ((SendableSegmentController)var2.getSegmentController()).acivateConnectedSignalsServer(true, var2.getAbsoluteIndex());
      }

      while(!this.onDock.isEmpty()) {
         ((RailController.RailTrigger)this.onDock.dequeue()).handle(this.previous);
      }

      this.self.onDockingChanged(true);
      if (this.dockedUsable != null) {
         this.dockedUsable.man.addPlayerUsable(this.dockedUsable);
      }

   }

   private void onUndockedServer(Vector3i var1) {
      this.onLeftBlockServer(var1);
      ((SendableSegmentController)this.self).activateSurroundServer(false, this.previous.docked.getAbsolutePos(this.tmpPos), ElementKeyMap.getSignalTypesActivatedOnSurround());

      while(!this.onUndock.isEmpty()) {
         ((RailController.RailTrigger)this.onUndock.dequeue()).handle(this.previous);
      }

      this.self.onDockingChanged(false);
      if (this.dockedUsable != null) {
         this.dockedUsable.man.removePlayerUsable(this.dockedUsable);
      }

   }

   private void onReachedNextBlockServer(Vector3i var1) {
      assert this.isOnServer();

      SegmentPiece[] var4 = this.previous.getCurrentRailContactPiece(this.tmpPieces);

      for(int var2 = 0; var2 < var4.length; ++var2) {
         SegmentPiece var3;
         if ((var3 = var4[var2]) != null && ElementKeyMap.isValidType(var3.getType()) && !ElementKeyMap.getInfoFast(var3.getType()).isRailRotator()) {
            this.previous.resetInRotationServer();
            if (!((SendableSegmentController)this.previous.rail.getSegmentController()).acivateConnectedSignalsServer(true, var3.getAbsoluteIndex())) {
               ((SendableSegmentController)this.previous.rail.getSegmentController()).activateSurroundServer(true, var3.getAbsolutePos(this.tmpPos), ElementKeyMap.getSignalTypesActivatedOnSurround());
            }
         }
      }

   }

   private void onLeftBlockServer(Vector3i var1) {
      assert this.isOnServer();

      SegmentPiece[] var4 = this.previous.getCurrentRailContactPiece(this.tmpPieces);

      for(int var2 = 0; var2 < var4.length; ++var2) {
         SegmentPiece var3;
         if ((var3 = var4[var2]) != null && ElementKeyMap.isValidType(var3.getType()) && !ElementKeyMap.getInfoFast(var3.getType()).isRailRotator() && !((SendableSegmentController)this.previous.rail.getSegmentController()).acivateConnectedSignalsServer(false, var3.getAbsoluteIndex())) {
            ((SendableSegmentController)this.previous.rail.getSegmentController()).activateSurroundServer(false, var3.getAbsolutePos(this.tmpPos), ElementKeyMap.getSignalTypesActivatedOnSurround());
         }
      }

   }

   private void updateAutoRailMovement(Timer var1) {
      long var2 = System.currentTimeMillis();
      long var4;
      if (this.isDockedAndExecuted()) {
         if (this.railMovingProspectedPos != null && this.previous.railContactToGo != null && this.railMovingProspectedRots.isEmpty()) {
            if (this.movDelay > 0L) {
               var4 = (long)(var1.getDelta() * 1000.0F);
               this.movDelay -= var4;
               this.movDelayAcc += var4;
            } else {
               this.updateAutoRailMovementPos(var1);
            }
         } else {
            label56: {
               if (!this.railMovingProspectedRots.isEmpty()) {
                  if (this.rotDelay > 0L) {
                     var4 = (long)(var1.getDelta() * 1000.0F);
                     this.rotDelay -= var4;
                     this.rotDelayAcc += var4;
                     break label56;
                  }

                  this.updateAutoRailMovementRot(var1);
               }

               this.colByLinearMovement = false;
            }
         }

         if (this.isOnServer() && this.shootOutFlag && !this.shootOutExecute) {
            if (this.shootOutCount > 5) {
               System.err.println("[RAIL][SHOOTOUT] preparing shootout " + this.getState() + "; " + this.self);
               if (this.lastMovementDirRelativeToRoot.lengthSquared() == 0.0F) {
                  System.err.println("[RAIL][SHOOTOUT] preparing shootout NOT MOVED YET: " + this.getState() + "; " + this.self);
                  this.waitingShootout = true;
                  if (this.previous.railContactToGo != null) {
                     System.err.println("[RAIL][SHOOTOUT] TO GO: " + this.previous.railContactToGo + "; " + this.railMovingProspectedPos.equals(this.railMovingLocalTransform.origin));
                     this.updateAutoRailMovementPos(var1);
                  }
               } else {
                  this.shootOutExecute = true;
                  this.shootOutFlag = false;
                  this.shootOutCount = 0;
               }
            } else {
               ++this.shootOutCount;
            }
         }
      }

      if ((var4 = System.currentTimeMillis() - var2) > 30L) {
         System.err.println("[RAIL] physics of " + this.self + " took " + var4 + "; Mov: ProspectMove " + this.railMovingProspectedPos + "; GoTo " + (this.previous != null ? this.previous.railContactToGo : "not Docked") + "; RotSize: " + this.railMovingProspectedRots.size());
      }

   }

   public void onOrientatePhysics(float var1) {
      if (this.isDockedAndExecuted() && this.isTurretDocked()) {
         this.updateTurretInterpolationF(var1);
      }

      if (this.isDockedAndExecuted() && this.previous.getRailRController().isDockedAndExecuted() && this.previous.getRailRController().doTurretOrientate(var1)) {
         this.previous.getRailRController().calculateChainTranformsRecursive();
      }

   }

   private boolean checkFactionAllowed(RailRelation var1, RailRelation.DockingPermission var2) {
      SegmentPiece var3 = var1.docked;
      SegmentPiece var4 = var1.rail;
      if (this.loadedFromTag) {
         var1.dockingPermission = var2;
         this.loadedFromTag = false;
         return true;
      } else if (SegmentController.isPublicException(var4, var3.getSegmentController().getFactionId())) {
         var1.dockingPermission = RailRelation.DockingPermission.PUBLIC;
         return true;
      } else if (var4.getSegmentController().getFactionId() == 0) {
         return true;
      } else if (var3.getSegmentController() instanceof Ship && var4.getSegmentController() instanceof Ship && ((Ship)var3.getSegmentController()).isInFleet() && ((Ship)var4.getSegmentController()).isInFleet() && ((Ship)var3.getSegmentController()).getFleet() == ((Ship)var4.getSegmentController()).getFleet()) {
         return true;
      } else {
         boolean var5 = var3.getSegmentController().getFactionId() == var4.getSegmentController().getFactionId();
         boolean var6 = false;
         if (var3.getSegmentController() instanceof PlayerControllable) {
            Iterator var7 = ((PlayerControllable)var3.getSegmentController()).getAttachedPlayers().iterator();

            while(var7.hasNext()) {
               if (((PlayerState)var7.next()).getFactionId() == var4.getSegmentController().getFactionId()) {
                  var6 = true;
                  break;
               }
            }
         }

         boolean var8 = this.loadedFromTag;
         this.loadedFromTag = false;
         return !this.isOnServer() || var5 || var6 || var8;
      }
   }

   private boolean doTurretOrientate(float var1) {
      if (this.turretRotX == null) {
         if (this.turretRotYHelp != null) {
            this.handleTurretPrimary(var1);
            this.turretRotYHelp = null;
            return true;
         } else {
            return false;
         }
      } else {
         Vector3f var2 = GlUtil.getRightVector(new Vector3f(), this.turretRotX);
         Vector3f var3 = GlUtil.getUpVector(new Vector3f(), this.turretRotX);
         Vector3f var4 = GlUtil.getForwardVector(new Vector3f(), this.turretRotX);
         this.self.getPhysics().orientateRailTurret(this.self, var4, var3, var2, 0.0F, 0.0F, 0.0F, var1, this.getTurretRailSpeed());
         Quat4f var5 = new Quat4f();
         Quat4fTools.set(this.turretRotX.basis, var5);
         if (this.isOnServer() || this.self.getRemoteTransformable().isSendFromClient()) {
            this.self.getNetworkObject().railTurretTransSecondary.add((Streamable)(new RemoteVector4f(new Vector4f(var5), this.isOnServer())));
         }

         this.turretRotX = null;
         this.updateTurretInterpolationF(var1);
         if (this.turretRotYHelp != null) {
            this.handleTurretPrimary(var1);
            this.turretRotYHelp = null;
         }

         return true;
      }
   }

   private void handleTurretPrimary(float var1) {
      if (this.recY) {
         Vector3f var2 = GlUtil.getRightVector(new Vector3f(), this.turretRotYHelp);
         Vector3f var3 = GlUtil.getUpVector(new Vector3f(), this.turretRotYHelp);
         Vector3f var4 = GlUtil.getForwardVector(new Vector3f(), this.turretRotYHelp);
         this.self.getPhysics().orientateRailTurret(this.self, var4, var3, var2, 0.0F, 0.0F, 0.0F, var1, this.getTurretRailSpeed());
         this.recY = false;
         this.updateTurretInterpolationF(var1);
      }

      if (this.isOnServer() || this.self.getRemoteTransformable().isSendFromClient()) {
         Quat4f var5 = new Quat4f();
         Quat4fTools.set(this.turretRotYHelp, var5);
         this.self.getNetworkObject().railTurretTransPrimary.add((Streamable)(new RemoteVector4f(new Vector4f(var5), this.isOnServer())));
      }

   }

   public void updateChildPhysics(Timer var1) {
      this.updateAutoRailMovement(var1);
      if (this.isRoot() && this.isRail()) {
         this.calculateChainTranformsRecursive();
      }

   }

   private void updateTurretInterpolationF(float var1) {
      if (!this.railTurretMovingLocalTransform.basis.equals(this.railTurretMovingLocalTransformTarget.basis)) {
         Quat4f var2 = new Quat4f();
         Quat4f var3 = new Quat4f();
         Quat4fTools.set(this.railTurretMovingLocalTransform.basis, var2);
         Quat4fTools.set(this.railTurretMovingLocalTransformTarget.basis, var3);
         if (var2.epsilonEquals(var3, 0.001F)) {
            this.railTurretMovingLocalTransform.basis.set(var3);
         } else {
            Quat4f var4 = new Quat4f();
            Quat4Util.slerp(var2, var3, Math.max(0.0F, Math.min(1.0F, var1 * this.getTurretRailSpeed())), var4);
            var4.normalize();
            this.railTurretMovingLocalTransform.basis.set(var4);
         }

         this.markTransformationChanged();
      }

      this.calculateChainTranformsRecursive();
   }

   private void markTransformationChanged() {
      this.markedTransformation = true;
      Iterator var1 = this.next.iterator();

      while(var1.hasNext()) {
         ((RailRelation)var1.next()).getDockedRController().markTransformationChanged();
      }

   }

   public boolean checkChildCollision(Transform var1, SegmentController... var2) {
      Vector3f var3 = GlUtil.getRightVector(new Vector3f(), var1);
      Vector3f var4 = GlUtil.getUpVector(new Vector3f(), var1);
      Vector3f var5 = GlUtil.getForwardVector(new Vector3f(), var1);
      this.self.getPhysics().orientateRailTurret(this.self, var5, var4, var3, 0.0F, 0.0F, 0.0F, -1.0F, -1.0F, this.railUpToThisOriginalLocalTransform, this.potentialRailTurretMovingLocalTransform);
      var1 = new Transform(this.railTurretMovingLocalTransform);
      this.railTurretMovingLocalTransform.set(this.potentialRailTurretMovingLocalTransform);
      this.getRoot().railController.calculatePotentialChainTranformsRecursive(true);
      this.railTurretMovingLocalTransform.set(var1);
      return this.collisionChecker.checkPotentialCollisionWithRail(this.previous.docked.getSegmentController(), var2, false);
   }

   public void updateDockedFromPhysicsWorld() {
      if (this.isRoot()) {
         this.calculateChainTranformsRecursive();
      }

      this.self.getPhysicsDataContainer().updatePhysical(this.getState().getUpdateTime());

      for(int var1 = 0; var1 < this.next.size(); ++var1) {
         ((RailRelation)this.next.get(var1)).getDockedRController().updateDockedFromPhysicsWorld();
      }

   }

   public void updateWithoutPhysicsObject() {
      if (this.isDockedAndExecuted()) {
         if (Vector3fTools.diffLengthSquared(this.relativeToRootLocalTransform.origin, this.self.getPhysicsDataContainer().getShapeChild().transform.origin) > 0.001F) {
            this.lastMovementDirRelativeToRoot.sub(this.relativeToRootLocalTransform.origin, this.self.getPhysicsDataContainer().getShapeChild().transform.origin);
            this.getRoot().getWorldTransform().basis.transform(this.lastMovementDirRelativeToRoot);
         }

         assert !TransformTools.isNan(this.relativeToRootLocalTransform);

         this.self.getPhysicsDataContainer().getShapeChild().transform.set(this.relativeToRootLocalTransform);
         SegmentController var1 = this.getRoot();
         this.checkRootIntegrity();

         assert !TransformTools.isNan(var1.getWorldTransform());

         this.self.getPhysicsDataContainer().updateManuallyWithChildTrans(var1.getWorldTransform(), (CompoundShape)var1.getPhysicsDataContainer().getShape());
      }

   }

   public void checkRootIntegrity() {
   }

   private void removeObjectPhysicsRecusively() {
      this.self.onPhysicsRemove();
      if (!this.isRoot()) {
         this.self.getPhysicsDataContainer().setObject((CollisionObject)null);
      }

      for(int var1 = 0; var1 < this.next.size(); ++var1) {
         ((RailRelation)this.next.get(var1)).docked.getSegmentController().railController.removeObjectPhysicsRecusively();
      }

   }

   private void recreateRootObjectPhysics() {
      assert this.isRoot();

      Vector3f var1 = new Vector3f();
      Vector3f var2 = new Vector3f();
      if (this.self.getPhysicsDataContainer().getObject() != null && this.self.getPhysicsDataContainer().getObject() instanceof RigidBody) {
         ((RigidBody)this.self.getPhysicsDataContainer().getObject()).getLinearVelocity(var1);
         ((RigidBody)this.self.getPhysicsDataContainer().getObject()).getAngularVelocity(var2);
      }

      this.removeObjectPhysicsRecusively();
      CompoundShape var3;
      CompoundShapeChild var4 = (CompoundShapeChild)(var3 = (CompoundShape)this.self.getPhysicsDataContainer().getShape()).getChildList().get(0);
      var3.getChildList().clear();
      var4.transform.setIdentity();
      var3.getChildList().add(var4);
      this.calculateChainTranformsRecursive();
      this.self.flagupdateMass();
      this.level = 0;

      for(int var5 = 0; var5 < this.next.size(); ++var5) {
         ((RailRelation)this.next.get(var5)).docked.getSegmentController().railController.recreateSubObjectPhysics(var3, this.self, this.level + 1);
      }

      this.checkRootIntegrity();
      this.dockingMass = this.calculateRailMassIncludingSelf();
      this.calculateInertia();

      assert var3.getChildList().size() > 0;

      if (TransformTools.isNan(this.self.getWorldTransform())) {
         System.err.println("Exception: (Before CoM) Error adding ship \n" + this.self + "\nPosition invalid\nPlease send in logs");
         if (this.isOnServer()) {
            System.err.println("Exception: Error adding ship \n" + this.self + "\nPosition invalid\nPlease send in logs");
            ((GameServerState)this.getState()).getController().broadcastMessageAdmin(new Object[]{117, this.self}, 3);
         }

      } else {
         if (this.self.getPhysicsDataContainer().lastCenter.length() > 0.0F) {
            Vector3f var6;
            if (Vector3fTools.isNan(var6 = ((CubesCompoundShape)this.self.getPhysicsDataContainer().getShape()).getCenterOfMass())) {
               System.err.println("Exception: (CoM) Error adding ship \n" + this.self + "\nPosition invalid\nPlease send in logs");
               if (this.isOnServer()) {
                  System.err.println("Exception: Error adding ship \n" + this.self + "\nPosition invalid\nPlease send in logs");
                  ((GameServerState)this.getState()).getController().broadcastMessageAdmin(new Object[]{118, this.self}, 3);
               }

               return;
            }

            this.self.getWorldTransform().basis.transform(var6);
            this.self.getWorldTransform().origin.add(var6);
         }

         if (TransformTools.isNan(this.self.getWorldTransform())) {
            System.err.println("Exception: ");
            if (this.isOnServer()) {
               System.err.println("Exception: (After CoM)Error adding ship \n" + this.self + "\nPosition invalid\nPlease send in logs");
               ((GameServerState)this.getState()).getController().broadcastMessageAdmin(new Object[]{119, this.self}, 3);
            }

         } else {
            RigidBody var11;
            (var11 = this.self.getPhysics().getBodyFromShape(var3, this.self.getMass() > 0.0F ? this.dockingMass : 0.0F, this.self.getWorldTransform())).setUserPointer(this.self.getId());

            assert var11.getCollisionShape() == var3;

            this.self.getPhysicsDataContainer().setShapeChield((CompoundShapeChild)null, -1);
            this.self.getPhysicsDataContainer().setObject(var11);

            assert !this.self.getPhysics().containsObject(var11);

            for(int var12 = 0; var12 < this.self.getPhysics().getDynamicsWorld().getCollisionObjectArray().size(); ++var12) {
               CollisionObject var7;
               if ((var7 = (CollisionObject)this.self.getPhysics().getDynamicsWorld().getCollisionObjectArray().get(var12)) instanceof RigidBodySegmentController) {
                  RigidBodySegmentController var8 = (RigidBodySegmentController)var7;

                  assert var8.getSegmentController() != this.self;
               }
            }

            if (TransformTools.isNan(var4.transform)) {
               System.err.println("Exception: (chield) Error adding ship \n" + this.self + "\nPosition invalid\nPlease send in logs");
               if (this.isOnServer()) {
                  System.err.println("Exception: Error adding ship \n" + this.self + "\nPosition invalid\nPlease send in logs");
                  ((GameServerState)this.getState()).getController().broadcastMessageAdmin(new Object[]{120, this.self}, 3);
               }

            } else {
               this.self.onPhysicsAdd();
               this.self.getPhysicsDataContainer().setShapeChield(var4, 0);
               com.bulletphysics.util.ObjectArrayList var13 = ((CubesCompoundShape)this.self.getPhysicsDataContainer().getShape()).getChildList();

               for(int var9 = 0; var9 < var13.size(); ++var9) {
                  var13.get(var9);
               }

               boolean var10 = this.lastLinearVelocityFromRoot.lengthSquared() > 0.0F;
               if (this.shootOut) {
                  this.lastMovementDirRelativeToRoot.normalize();
                  this.lastMovementDirRelativeToRoot.scale(25.0F);
                  System.err.println("[RAIL] SHOOTOUT REGISTERED " + this.getRoot().getState() + " " + this.lastMovementDirRelativeToRoot);
                  this.lastLinearVelocityFromRoot.add(this.lastMovementDirRelativeToRoot);
               }

               var11.setLinearVelocity(this.lastLinearVelocityFromRoot);
               this.lastLinearVelocityFromRoot.set(0.0F, 0.0F, 0.0F);
               this.checkRootIntegrity();
               this.self.getPhysicsDataContainer().updatePhysical(this.getState().getUpdateTime());
               if (this.self.getPhysicsDataContainer().getObject() != null && this.self.getPhysicsDataContainer().getObject() instanceof RigidBody) {
                  if (!var10) {
                     if (this.shootOut) {
                        var1.add(this.lastMovementDirRelativeToRoot);
                     }

                     ((RigidBody)this.self.getPhysicsDataContainer().getObject()).setLinearVelocity(var1);
                  }

                  ((RigidBody)this.self.getPhysicsDataContainer().getObject()).setAngularVelocity(var2);
               }

               this.checkRootIntegrity();

               assert !this.isOnServer() || this.self.getPhysics().containsObject(var11);

               if (this.shootOut) {
                  System.err.println("[RAIL] shootout " + this.self + " shooting out from rail!");
                  this.shootOut = false;
               }

            }
         }
      }
   }

   public boolean isParentDock(SegmentController var1) {
      return this.isInAnyRailRelationWith(var1) && var1.railController.level <= this.level;
   }

   public boolean isChildDock(SegmentController var1) {
      return this.isInAnyRailRelationWith(var1) && var1.railController.level > this.level;
   }

   private void recreateSubObjectPhysics(CompoundShape var1, SegmentController var2, int var3) {
      try {
         CompoundShape var4;
         CompoundShapeChild var5 = (CompoundShapeChild)(var4 = (CompoundShape)this.self.getPhysicsDataContainer().getShape()).getChildList().get(0);
         var4.getChildList().clear();
         var4.getChildList().add(var5);
         var1.addChildShape(this.relativeToRootLocalTransform, var4.getChildShape(0));
         int var8 = var1.getChildList().size() - 1;
         this.self.getPhysicsDataContainer().setShapeChield((CompoundShapeChild)var1.getChildList().get(var8), var8);
         if (this.previous == null) {
            throw new NullPointerException("Incosistent dock! " + this.self + " is not docked but called in chain from parent: " + var2);
         } else {
            this.previous.executed = true;
            this.self.flagupdateMass();
            this.level = var3;

            for(int var7 = 0; var7 < this.next.size(); ++var7) {
               assert ((RailRelation)this.next.get(var7)).docked.getSegmentController().railController.previous != null : ((RailRelation)this.next.get(var7)).docked.getSegmentController();

               ((RailRelation)this.next.get(var7)).docked.getSegmentController().railController.recreateSubObjectPhysics(var1, this.self, var3 + 1);
            }

         }
      } catch (RuntimeException var6) {
         System.err.println("EXCEPTION FOR " + this.self);
         var6.printStackTrace();
      }
   }

   private void markTreeDirty() {
      if (this.previous != null) {
         this.previous.rail.getSegmentController().railController.markTreeDirty();
      } else {
         this.dirty = true;
         this.markRelationsNotExecuted();
      }

      assert this.getRoot().railController.treeValid();

   }

   private boolean treeValid() {
      Iterator var1 = this.next.iterator();

      RailRelation var2;
      do {
         if (!var1.hasNext()) {
            return true;
         }

         if ((var2 = (RailRelation)var1.next()).docked.getSegmentController().railController.previous != var2) {
            return false;
         }
      } while(var2.docked.getSegmentController().railController.treeValid());

      return false;
   }

   private void markRelationsNotExecuted() {
      if (this.previous != null) {
         this.previous.executed = false;
      }

      for(int var1 = 0; var1 < this.next.size(); ++var1) {
         ((RailRelation)this.next.get(var1)).docked.getSegmentController().railController.markRelationsNotExecuted();
      }

   }

   public boolean isRoot() {
      return this.previous == null;
   }

   public SegmentController getRoot() {
      return this.previous != null ? this.previous.rail.getSegmentController().railController.getRoot() : this.self;
   }

   public float calculateRailMassIncludingSelf() {
      float var1 = Math.max(0.01F, this.self.getMassWithoutDockIncludingStation());

      for(int var2 = 0; var2 < this.next.size(); ++var2) {
         var1 += ((RailRelation)this.next.get(var2)).getDockedRController().calculateRailMassIncludingSelf();
      }

      return var1;
   }

   public Vector3f calculateInertia() {
      this.railedInertia.set(this.self.getPhysicsDataContainer().inertia);

      for(int var1 = 0; var1 < this.next.size(); ++var1) {
         this.railedInertia.add(((RailRelation)this.next.get(var1)).getDockedRController().calculateInertia());
      }

      return this.railedInertia;
   }

   public void calculatePotentialChainTranformsRecursive(boolean var1) {
      if (this.isRoot()) {
         this.potentialRelativeToRootLocalTransform.setIdentity();
         this.potentialRailMovingLocalTransform.setIdentity();
         this.potentialRailTurretMovingLocalTransform.setIdentity();
         this.potentialRailOriginalLocalTransform.setIdentity();
         this.potentialBasicRelativeTransform.setIdentity();
      } else {
         assert this.previous != null;

         this.potentialRelativeToRootLocalTransform.set(this.previous.rail.getSegmentController().railController.potentialRelativeToRootLocalTransform);
         this.potentialRailUpToThisOriginalLocalTransform.set(this.previous.rail.getSegmentController().railController.potentialRelativeToRootLocalTransform);
         if (var1) {
            if (this.railTurretMovingLocalTransform.equals(this.railTurretMovingLocalTransformLastUpdateR) && this.railMovingLocalTransform.equals(this.railMovingLocalTransformLastUpdateR)) {
               this.potentialBasicRelativeTransform.set(this.basicRelativeTransform);
               this.potentialRailOriginalLocalTransform.set(this.railOriginalLocalTransform);
            } else {
               this.previous.getBlockTransform(this.potentialBasicRelativeTransform, this.potentialRailOriginalLocalTransform, this.railTurretMovingLocalTransform, this.railMovingLocalTransform);
               this.railTurretMovingLocalTransformLastUpdateR.set(this.railTurretMovingLocalTransform);
               this.railMovingLocalTransformLastUpdateR.set(this.railMovingLocalTransform);
            }
         } else {
            this.previous.getBlockTransform(this.potentialBasicRelativeTransform, this.potentialRailOriginalLocalTransform, this.potentialRailTurretMovingLocalTransform, this.potentialRailMovingLocalTransform);
         }

         this.potentialRailUpToThisOriginalLocalTransform.mul(this.potentialRailOriginalLocalTransform);
         this.potentialRelativeToRootLocalTransform.mul(this.potentialBasicRelativeTransform);
      }

      for(int var2 = 0; var2 < this.next.size(); ++var2) {
         ((RailRelation)this.next.get(var2)).getDockedRController().calculatePotentialChainTranformsRecursive(var1);
      }

   }

   public String verbose() {
      return this.getState() + " ------- " + this.self.getUniqueIdentifier() + " " + this.railMovingLocalTransform.origin;
   }

   public void calculateChainTranformsRecursive() {
      if (this.isRoot()) {
         this.relativeToRootLocalTransform.setIdentity();
         this.railMovingLocalTransform.setIdentity();
         this.railTurretMovingLocalTransform.setIdentity();
         this.railOriginalLocalTransform.setIdentity();
         this.basicRelativeTransform.setIdentity();
      } else {
         if (this.isOnServer() && this.previous.rail.getSegmentController().isVirtualBlueprint()) {
            this.self.setVirtualBlueprint(true);
         }

         assert this.previous != null;

         this.relativeToRootLocalTransform.set(this.previous.rail.getSegmentController().railController.relativeToRootLocalTransform);
         this.railUpToThisOriginalLocalTransform.set(this.previous.rail.getSegmentController().railController.relativeToRootLocalTransform);
         if (!this.railTurretMovingLocalTransform.equals(this.railTurretMovingLocalTransformLastUpdate) || !this.railMovingLocalTransform.equals(this.railMovingLocalTransformLastUpdate)) {
            this.previous.getBlockTransform(this.basicRelativeTransform, this.railOriginalLocalTransform, this.railTurretMovingLocalTransform, this.railMovingLocalTransform);
            this.railTurretMovingLocalTransformLastUpdate.set(this.railTurretMovingLocalTransform);
            this.railMovingLocalTransformLastUpdate.set(this.railMovingLocalTransform);
         }

         assert !TransformTools.isNan(this.railOriginalLocalTransform);

         assert !TransformTools.isNan(this.basicRelativeTransform);

         this.railUpToThisOriginalLocalTransform.mul(this.railOriginalLocalTransform);
         this.relativeToRootLocalTransform.mul(this.basicRelativeTransform);

         assert !TransformTools.isNan(this.railUpToThisOriginalLocalTransform);

         assert !TransformTools.isNan(this.relativeToRootLocalTransform);
      }

      this.didFirstTransform = true;

      for(int var1 = 0; var1 < this.next.size(); ++var1) {
         ((RailRelation)this.next.get(var1)).getDockedRController().calculateChainTranformsRecursive();
      }

   }

   public Transform getRailMovingLocalTransform() {
      return this.railTurretMovingLocalTransform;
   }

   public Transform getRailMovingLocalTransformTarget() {
      return this.railTurretMovingLocalTransformTarget;
   }

   public boolean isRail() {
      return !this.next.isEmpty();
   }

   public boolean isDockedOrDirty() {
      return this.isDocked() || this.dirty;
   }

   public boolean isDocked() {
      return this.previous != null;
   }

   public boolean isDockedAndExecuted() {
      return this.previous != null && this.previous.executed;
   }

   public void fromTag(Tag var1, int var2, boolean var3) {
      ObjectOpenHashSet var4 = new ObjectOpenHashSet();
      this.railRequestCurrent = fromTagR(var1, var2, var4);
      if (var3) {
         Iterator var5 = var4.iterator();

         while(var5.hasNext()) {
            RailRequest var6 = (RailRequest)var5.next();
            this.addExpectedToDock(var6);
         }

         System.err.println("[SERVER][RAIL] " + this.self + " Expected to dock " + this.expectedToDock);
      }

      if (verboseTag && this.railRequestCurrent != null) {
         System.err.println("[SERVER][RAIL] LOADING: " + this.self);
         System.err.println("railMovingLocalAtDockTransform:");
         System.err.println(this.railRequestCurrent.railMovingLocalAtDockTransform.getMatrix(new Matrix4f()));
         System.err.println("movedTransform:");
         System.err.println(this.railRequestCurrent.movedTransform.getMatrix(new Matrix4f()));
         System.err.println("turretTransform:");
         System.err.println(this.railRequestCurrent.turretTransform.getMatrix(new Matrix4f()));
         System.err.println("-----------------------------------------------");
      }

      if (this.railRequestCurrent != null) {
         this.loadedFromTag = true;
      }

   }

   public Tag getTag() {
      Tag var1;
      if (this.railRequestCurrent != null) {
         var1 = getRequestTag(this.railRequestCurrent, this.getDockedUIDsTag(this.next, this.expectedToDock));

         assert var1.getType() == Tag.Type.STRUCT;

         assert (Byte)((Tag[])var1.getValue())[0].getValue() == 3;

         return var1;
      } else if (this.previous != null) {
         var1 = this.getDockedTag();

         assert var1.getType() == Tag.Type.STRUCT;

         assert (Byte)((Tag[])var1.getValue())[0].getValue() == 1;

         return var1;
      } else if (this.isRoot() && this.isRail()) {
         var1 = getRootRailTag(this.getDockedUIDsTag(this.next, this.expectedToDock));

         assert var1.getType() == Tag.Type.STRUCT;

         assert (Byte)((Tag[])var1.getValue())[0].getValue() == 2;

         return var1;
      } else {
         return new Tag(Tag.Type.BYTE, (String)null, (byte)0);
      }
   }

   private static Tag getRootRailTag(Tag var0) {
      Tag var1 = new Tag(Tag.Type.BYTE, (String)null, (byte)2);
      Tag var2 = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{FinishTag.INST});
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, var2, var0, FinishTag.INST});
   }

   private Tag getDockedTag() {
      Tag var1 = new Tag(Tag.Type.BYTE, (String)null, (byte)1);
      RailRequest var2;
      (var2 = new RailRequest()).turretTransform.set(this.railTurretMovingLocalTransform);
      var2.movedTransform.set(this.railMovingLocalTransform);
      var2.rail = new VoidUniqueSegmentPiece(this.previous.rail);
      var2.docked = new VoidUniqueSegmentPiece(this.previous.docked);
      var2.railDockerPosOnRail.set(this.previous.currentRailContact);
      var2.railMovingLocalAtDockTransform.set(this.railMovingLocalAtDockTransform);
      var2.didRotationInPlace = this.previous.doneInRotationServer();
      var2.dockingPermission = this.previous.dockingPermission;
      Tag var3 = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var2.getTag(), FinishTag.INST});
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, var3, this.getDockedUIDsTag(this.next, this.expectedToDock), FinishTag.INST});
   }

   public long isDockedSince() {
      return this.dockedSince;
   }

   public long getLastDisconnect() {
      return this.lastDisconnect;
   }

   public Transform getRelativeToRootLocalTransform() {
      return this.relativeToRootLocalTransform;
   }

   public Transform getPotentialRelativeToRootLocalTransform() {
      return this.potentialRelativeToRootLocalTransform;
   }

   public Transform getRailUpToThisOriginalLocalTransform() {
      return this.railUpToThisOriginalLocalTransform;
   }

   public boolean isTurretDocked() {
      return this.previous != null && this.previous.isTurretDock();
   }

   public boolean isShipyardDocked() {
      return this.previous != null && this.previous.isShipyardDock();
   }

   public boolean isShipyardDockedRecursive() {
      return this.previous != null && (this.previous.isShipyardDock() || this.previous.getRailRController().isShipyardDockedRecursive());
   }

   public boolean isClientCameraSet() {
      return !this.self.isClientOwnObject() || Controller.getCamera() == null || Controller.getCamera() instanceof InShipCamera && ((InShipCamera)Controller.getCamera()).getHelperCamera().wasDockedOnUpdate();
   }

   public boolean isChainSendFromClient() {
      if (this.isDockedAndExecuted() && this.isTurretDocked()) {
         Iterator var1 = this.next.iterator();

         while(var1.hasNext()) {
            RailRelation var2;
            if ((var2 = (RailRelation)var1.next()).isTurretDock() && var2.docked.getSegmentController().getRemoteTransformable().isSendFromClient()) {
               return checkTurretBaseModifiable(var2.docked.getSegmentController(), this.self);
            }
         }
      }

      return false;
   }

   public void modifyDockingRequestNames(String var1, String var2) {
      assert this.railRequestCurrent != null;

      this.railRequestCurrent.docked.uniqueIdentifierSegmentController = var1;
      this.railRequestCurrent.rail.uniqueIdentifierSegmentController = var2;
   }

   public boolean hasActiveDockingRequest() {
      return this.railRequestCurrent != null || this.isDocked();
   }

   public float getTurretRailSpeed() {
      return 50.0F;
   }

   public boolean isInAnyRailRelation() {
      return this.isRail() || this.isDockedOrDirty();
   }

   public void destroyDockedRecursive() {
      Iterator var1 = this.next.iterator();

      while(var1.hasNext()) {
         RailRelation var2;
         (var2 = (RailRelation)var1.next()).docked.getSegmentController().railController.destroyDockedRecursive();
         var2.docked.getSegmentController().markForPermanentDelete(true);
         var2.docked.getSegmentController().setMarkedForDeleteVolatile(true);
      }

   }

   public boolean isPublicPermissionAlongChain() {
      boolean var1;
      if (var1 = this.isDockedAndExecuted()) {
         return var1 && this.previous.dockingPermission == RailRelation.DockingPermission.PUBLIC ? true : this.previous.rail.getSegmentController().railController.isPublicPermissionAlongChain();
      } else {
         return false;
      }
   }

   public int getDockedFactionId(int var1) {
      return this.isDockedAndExecuted() && this.previous.dockingPermission != RailRelation.DockingPermission.PUBLIC && var1 == 0 ? this.getNextMotherFaction() : var1;
   }

   private int getNextMotherFaction() {
      while(this.self.getOriginalFactionId() == 0) {
         if (!this.isDockedAndExecuted()) {
            return 0;
         }

         this = this.previous.getRailRController();
      }

      return this.self.getOriginalFactionId();
   }

   public SegmentController getChainElementByUID(String var1) {
      if (this.self.getUniqueIdentifier().equals(var1)) {
         return this.self;
      } else {
         Iterator var2 = this.next.iterator();

         SegmentController var3;
         do {
            if (!var2.hasNext()) {
               return null;
            }
         } while((var3 = ((RailRelation)var2.next()).docked.getSegmentController().railController.getChainElementByUID(var1)) == null);

         return var3;
      }
   }

   public String getRailUID() {
      if (!this.isRoot() && this.isDockedAndExecuted()) {
         StringBuffer var1 = new StringBuffer();
         this.previous.getRailRController().getRailUIDRec(var1, this.previous);
         var1.insert(0, "rl");
         return var1.toString();
      } else {
         return "";
      }
   }

   private void getRailUIDRec(StringBuffer var1, RailRelation var2) {
      while(true) {
         int var3 = this.next.indexOf(var2);
         var1.insert(0, var3);
         if (!this.isDockedAndExecuted()) {
            return;
         }

         RailController var10000 = this.previous.getRailRController();
         var2 = this.previous;
         var1 = var1;
         this = var10000;
      }
   }

   public boolean isInAnyRailRelationWith(SegmentController var1) {
      return this.getRoot() == var1.railController.getRoot();
   }

   public void getAll(List var1) {
      if (!var1.contains(this.self)) {
         var1.add(this.self);
      }

      Iterator var2 = this.next.iterator();

      while(var2.hasNext()) {
         ((RailRelation)var2.next()).docked.getSegmentController().railController.getAll(var1);
      }

   }

   public void activateAllAIServer(boolean var1, boolean var2, boolean var3, boolean var4) {
      SegmentController var7;
      for(Iterator var5 = this.next.iterator(); var5.hasNext(); var7.railController.activateAllAIServer(var1, var2, var3, var4)) {
         RailRelation var6;
         if ((var7 = (var6 = (RailRelation)var5.next()).docked.getSegmentController()).getElementClassCountMap().get((short)121) > 0 || var4) {
            AIConfigurationInterface var8;
            AIGameConfiguration var9;
            if (var2 && var6.isTurretDock() && var6.isTurretDockLastAxis() && var7 instanceof SegmentControllerAIInterface && (var8 = ((SegmentControllerAIInterface)var7).getAiConfiguration()) instanceof AIGameConfiguration) {
               (var9 = (AIGameConfiguration)var8).get(Types.TYPE).setCurrentState("Turret", true);
               var9.get(Types.ACTIVE).setCurrentState(var1, true);
               ((Ship)var7).getAiConfiguration().applyServerSettings();
               System.err.println("[SERVER] ACTIVAING TURRET AI " + var7);
            }

            if (var3 && !var6.isTurretDock() && var7 instanceof SegmentControllerAIInterface && (var8 = ((SegmentControllerAIInterface)var7).getAiConfiguration()) instanceof AIGameConfiguration) {
               (var9 = (AIGameConfiguration)var8).get(Types.TYPE).setCurrentState("Ship", true);
               var9.get(Types.ACTIVE).setCurrentState(var1, true);
               ((Ship)var7).getAiConfiguration().applyServerSettings();
               System.err.println("[SERVER] ACTIVAING NORMAL AI " + var7);
            }
         }
      }

   }

   public void activateAllAIClient(boolean var1, boolean var2, boolean var3) {
      assert !this.isOnServer();

      SegmentController var6;
      for(Iterator var4 = this.next.iterator(); var4.hasNext(); var6.railController.activateAllAIClient(var1, var2, var3)) {
         RailRelation var5;
         if ((var6 = (var5 = (RailRelation)var4.next()).docked.getSegmentController()).getElementClassCountMap().get((short)121) > 0) {
            AIConfigurationInterface var7;
            AIGameConfiguration var8;
            if (var2 && var5.isTurretDock() && var5.isTurretDockLastAxis() && var6 instanceof SegmentControllerAIInterface && (var7 = ((SegmentControllerAIInterface)var6).getAiConfiguration()) instanceof AIGameConfiguration) {
               (var8 = (AIGameConfiguration)var7).get(Types.TYPE).setCurrentState("Turret", true);
               var8.get(Types.ACTIVE).setCurrentState(var1, true);
            }

            if (var3 && !var5.isTurretDock() && var6 instanceof SegmentControllerAIInterface && (var7 = ((SegmentControllerAIInterface)var6).getAiConfiguration()) instanceof AIGameConfiguration) {
               (var8 = (AIGameConfiguration)var7).get(Types.TYPE).setCurrentState("Ship", true);
               var8.get(Types.ACTIVE).setCurrentState(var1, true);
            }
         }
      }

   }

   public void undockAllClient() {
      assert !this.isOnServer();

      Iterator var1 = this.next.iterator();

      while(var1.hasNext()) {
         ((RailRelation)var1.next()).docked.getSegmentController().railController.requestDisconnect();
      }

   }

   public void undockAllClientTurret() {
      assert !this.isOnServer();

      Iterator var1 = this.next.iterator();

      while(var1.hasNext()) {
         RailRelation var2;
         if ((var2 = (RailRelation)var1.next()).isTurretDock()) {
            var2.docked.getSegmentController().railController.requestDisconnect();
         }
      }

   }

   public void undockAllClientNormal() {
      assert !this.isOnServer();

      Iterator var1 = this.next.iterator();

      while(var1.hasNext()) {
         RailRelation var2;
         if (!(var2 = (RailRelation)var1.next()).isTurretDock()) {
            var2.docked.getSegmentController().railController.requestDisconnect();
         }
      }

   }

   public boolean isInAnyRailRelationWith(SimpleTransformableSendableObject var1) {
      return var1 instanceof SegmentController ? this.isInAnyRailRelationWith((SegmentController)var1) : false;
   }

   public boolean isAllAdditionalBlueprintInfoReceived() {
      if (!((SendableSegmentController)this.self).getNetworkObject().additionalBlueprintData.getBoolean()) {
         return false;
      } else {
         Iterator var1 = this.next.iterator();

         do {
            if (!var1.hasNext()) {
               return true;
            }
         } while(((RailRelation)var1.next()).docked.getSegmentController().railController.isAllAdditionalBlueprintInfoReceived());

         return false;
      }
   }

   public void sendAdditionalBlueprintInfoToClient() {
      assert this.isOnServer();

      if (this.self instanceof ManagedSegmentController && ((ManagedSegmentController)this.self).getManagerContainer() instanceof ActivationManagerInterface) {
         ((ManagedSegmentController)this.self).getManagerContainer().sendFullDestinationUpdate();
      }

      Iterator var1 = this.next.iterator();

      while(var1.hasNext()) {
         ((RailRelation)var1.next()).docked.getSegmentController().railController.sendAdditionalBlueprintInfoToClient();
      }

   }

   public boolean isTurretDockLastAxis() {
      return this.isDockedAndExecuted() && this.isTurretDocked() && this.previous.isTurretDockLastAxis();
   }

   public void setAllScrap(boolean var1) {
      this.self.setScrap(var1);
      Iterator var2 = this.next.iterator();

      while(var2.hasNext()) {
         ((RailRelation)var2.next()).getDockedRController().setAllScrap(true);
      }

   }

   public double getShieldsRecursive() {
      double var1 = 0.0D;
      if (this.self instanceof ManagedSegmentController && ((ManagedSegmentController)this.self).getManagerContainer() instanceof ShieldContainerInterface) {
         var1 = ((ShieldContainerInterface)((ManagedSegmentController)this.self).getManagerContainer()).getShieldAddOn().getShields();
      }

      if (this.isDockedAndExecuted()) {
         var1 += this.previous.getRailRController().getShieldsRecursiveP();
      }

      return var1;
   }

   private double getShieldsRecursiveP() {
      double var1 = 0.0D;
      ShieldAddOn var3;
      if (this.self instanceof ManagedSegmentController && ((ManagedSegmentController)this.self).getManagerContainer() instanceof ShieldContainerInterface && (double)(var3 = ((ShieldContainerInterface)((ManagedSegmentController)this.self).getManagerContainer()).getShieldAddOn()).getPercentOne() > 0.5D) {
         var1 = 0.0D + var3.getShields() * 0.5D;
      }

      if (this.isDockedAndExecuted()) {
         var1 += this.previous.getRailRController().getShieldsRecursiveP();
      }

      return var1;
   }

   public void fillShieldsMapRecursive(Vector3f var1, int var2, Int2DoubleOpenHashMap var3, Int2DoubleOpenHashMap var4, Int2DoubleOpenHashMap var5, Int2IntOpenHashMap var6, Int2IntOpenHashMap var7, Int2LongOpenHashMap var8) throws SectorNotFoundException {
      if (this.getRoot().isUsingLocalShields()) {
         if (this.isDockedAndExecuted()) {
            var6.put(this.self.getId(), this.previous.rail.getSegmentController().getId());
            var7.put(this.self.getId(), this.getRoot().getId());
            this.getRoot().railController.fillShieldsMapRecursive(var1, var2, var3, var4, var5, var6, var7, var8);
            return;
         }

         if (this.self instanceof ManagedSegmentController && ((ManagedSegmentController)this.self).getManagerContainer() instanceof ShieldContainerInterface) {
            ((ShieldContainerInterface)((ManagedSegmentController)this.self).getManagerContainer()).getShieldAddOn().getShieldLocalAddOn().fillForExplosion(var1, var2, var3, var4, var5, var8);
            return;
         }
      } else {
         if (this.self instanceof ManagedSegmentController && ((ManagedSegmentController)this.self).getManagerContainer() instanceof ShieldContainerInterface) {
            ShieldAddOn var9;
            if ((var9 = ((ShieldContainerInterface)((ManagedSegmentController)this.self).getManagerContainer()).getShieldAddOn()).isUsingLocalShields()) {
               var9.getShieldLocalAddOn().fillForExplosion(var1, var2, var3, var4, var5, var8);
            } else {
               var3.put(this.self.getId(), var9.getShields());
               var4.put(this.self.getId(), var9.getShields());
               var5.put(this.self.getId(), var9.getShields() > 0.0D && var9.getShieldCapacity() > 0.0D ? var9.getShields() / var9.getShieldCapacity() : 0.0D);
            }
         }

         if (this.isDockedAndExecuted()) {
            var6.put(this.self.getId(), this.previous.rail.getSegmentController().getId());
            var7.put(this.self.getId(), this.getRoot().getId());
            this.previous.getRailRController().fillShieldsMapRecursive(var1, var2, var3, var4, var5, var6, var7, var8);
         }
      }

   }

   public RailCollisionChecker getCollisionChecker() {
      return this.collisionChecker;
   }

   public boolean isFullyLoadedRecursive() {
      if (this.self.getSegmentBuffer().isFullyLoaded()) {
         Iterator var1 = this.next.iterator();

         do {
            if (!var1.hasNext()) {
               if (this.expectedToDock.size() == 0) {
                  return true;
               }

               return false;
            }
         } while(((RailRelation)var1.next()).getDockedRController().isFullyLoadedRecursive());

         return false;
      } else {
         return false;
      }
   }

   public void fillElementCountMapRecursive(ElementCountMap var1) {
      var1.add(this.self.getElementClassCountMap());
      Iterator var2 = this.next.iterator();

      while(var2.hasNext()) {
         ((RailRelation)var2.next()).getDockedRController().fillElementCountMapRecursive(var1);
      }

   }

   public boolean isAnyChildOf(SegmentController var1) {
      if (var1 == this.self) {
         return true;
      } else {
         return this.previous != null ? this.previous.getRailRController().isAnyChildOf(var1) : false;
      }
   }

   public boolean isOkToDockClientCheck(SegmentPiece var1, SegmentPiece var2, DockingFailReason var3) {
      if (!(var1.getSegmentController() instanceof Ship)) {
         var3.reason = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RAILS_RAILCONTROLLER_12;
         return false;
      } else if (var1.getSegmentController().isVirtualBlueprint()) {
         var3.reason = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RAILS_RAILCONTROLLER_13;
         return false;
      } else if (var2.getSegmentController().isVirtualBlueprint()) {
         var3.reason = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RAILS_RAILCONTROLLER_15;
         return false;
      } else if (var1.getSegmentController().railController.isDockedAndExecuted()) {
         var3.reason = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RAILS_RAILCONTROLLER_14;
         return false;
      } else if (SegmentController.isPublicException(var2, var1.getSegmentController().getFactionId())) {
         return true;
      } else if (var2.getSegmentController().getFactionId() == 0) {
         return true;
      } else if (var1.getSegmentController().getFactionId() == var2.getSegmentController().getFactionId()) {
         return true;
      } else {
         if (var1.getSegmentController() instanceof PlayerControllable) {
            Iterator var4 = ((PlayerControllable)var1.getSegmentController()).getAttachedPlayers().iterator();

            while(var4.hasNext()) {
               if (((PlayerState)var4.next()).getFactionId() == var2.getSegmentController().getFactionId()) {
                  return true;
               }
            }
         }

         var3.reason = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RAILS_RAILCONTROLLER_11;
         return false;
      }
   }

   public void connectClient(SegmentPiece var1, SegmentPiece var2) {
      if (System.currentTimeMillis() - this.lastDockRequest > 5000L && System.currentTimeMillis() - this.lastDisconnect > 5000L) {
         this.resetTransformations();
         this.railMovingLocalAtDockTransform.setIdentity();
         System.err.println("[CLIENT] FIRST DOCKING REQUEST MADE: " + var1 + " -> " + var2);
         this.requestConnect(var1, var2, (Vector3i)null, (Vector3i)null, (RailRelation.DockingPermission)null);
         this.lastDockRequest = System.currentTimeMillis();
      } else {
         GameClientState var3 = (GameClientState)var1.getSegmentController().getState();
         if (System.currentTimeMillis() - this.lastDockRequest < 5000L) {
            var3.getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RAILS_RAILCONTROLLER_8, (int)(5000L - (System.currentTimeMillis() - this.lastDockRequest)) / 1000), "dockDelay", 0.0F);
         } else {
            if (System.currentTimeMillis() - this.lastDisconnect < 5000L) {
               var3.getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RAILS_RAILCONTROLLER_10, (int)(5000L - (System.currentTimeMillis() - this.lastDisconnect)) / 1000), "dockDelay", 0.0F);
            }

         }
      }
   }

   public SegmentController getOneBeforeRoot() {
      return this.previous != null && this.previous.rail.getSegmentController().railController.previous != null ? this.previous.rail.getSegmentController().railController.getRoot() : this.self;
   }

   public boolean isInAnyRailRelationWith(SimpleGameObject var1) {
      return var1 != null && var1 instanceof SimpleTransformableSendableObject && this.isInAnyRailRelationWith((SimpleTransformableSendableObject)var1);
   }

   public void undockAllServer() {
      assert this.isOnServer();

      ObjectArrayList var1;
      (var1 = new ObjectArrayList()).addAll(this.next);
      Iterator var3 = var1.iterator();

      while(var3.hasNext()) {
         RailRelation var2 = (RailRelation)var3.next();
         System.err.println("[RAIL][UNDOCKALL] UNDOCKING " + var2.docked.getSegmentController() + " from " + this.self);
         var2.docked.getSegmentController().railController.disconnect();
      }

   }

   public boolean isBlockNextToLogicOkTuUse(SegmentPiece var1, SegmentPiece var2) {
      if (var2 != null && var1 != null && ElementKeyMap.isValidType(var2.getType()) && ElementKeyMap.isValidType(var1.getType())) {
         ElementInformation var4 = ElementKeyMap.getInfoFast(var2.getType());
         ElementInformation var3 = ElementKeyMap.getInfoFast(var1.getType());
         return var4.isRailTrack() && var3.isRailTrack() || var4.isRailRotator() && var3.isRailRotator();
      } else {
         return false;
      }
   }

   public void afterReplaceBlock(SegmentPiece var1, SegmentPiece var2) {
      Iterator var6 = this.next.iterator();

      while(var6.hasNext()) {
         RailRelation var3;
         SegmentPiece[] var4 = (var3 = (RailRelation)var6.next()).getCurrentRailContactPiece(new SegmentPiece[6]);

         for(int var5 = 0; var5 < var4.length; ++var5) {
            if (var4[var5] != null && var2.equalsPos(var4[var5].getAbsolutePos(new Vector3i()))) {
               var3.resetInRotationServer();
            }
         }
      }

   }

   public boolean fromBlockOk(SegmentPiece var1) {
      return ElementKeyMap.isValidType(var1.getType()) && (ElementKeyMap.getInfo(var1.getType()).isRailTrack() || ElementKeyMap.getInfo(var1.getType()).isRailRotator());
   }

   public boolean equalsBlockData(SegmentPiece var1, SegmentPiece var2) {
      return var1.getType() == var2.getType() && var1.getOrientation() == var2.getOrientation();
   }

   public void modifyReplacement(SegmentPiece var1, SegmentPiece var2) {
   }

   public void markForPermanentDelete(boolean var1) {
      this.self.markForPermanentDelete(var1);
      Iterator var2 = this.next.iterator();

      while(var2.hasNext()) {
         ((RailRelation)var2.next()).docked.getSegmentController().railController.markForPermanentDelete(var1);
      }

   }

   public void calcBoundingSphereTotal(BoundingSphere var1) {
      var1.radius = Math.max(var1.radius, this.relativeToRootLocalTransform.origin.length() + this.self.getBoundingSphere().radius);
      Iterator var2 = this.next.iterator();

      while(var2.hasNext()) {
         ((RailRelation)var2.next()).docked.getSegmentController().railController.calcBoundingSphereTotal(var1);
      }

   }

   public void setFactionIdForEntitiesWithoutFactionBlock(int var1) {
      if (this.self instanceof ManagedSegmentController && ((ManagedSegmentController)this.self).getManagerContainer().getFactionBlockPos() == Long.MIN_VALUE) {
         this.self.setFactionId(var1);
         Iterator var2 = this.next.iterator();

         while(var2.hasNext()) {
            ((RailRelation)var2.next()).docked.getSegmentController().railController.setFactionIdForEntitiesWithoutFactionBlock(var1);
         }
      }

   }

   public void resetFactionForEntitiesWithoutFactionBlock(int var1) {
      if (var1 == this.self.getFactionId()) {
         this.self.setFactionId(0);
      }

      Iterator var2 = this.next.iterator();

      while(var2.hasNext()) {
         RailRelation var3;
         if ((var3 = (RailRelation)var2.next()).docked.getSegmentController().getElementClassCountMap().get((short)291) == 0) {
            var3.docked.getSegmentController().railController.resetFactionForEntitiesWithoutFactionBlock(var1);
         }
      }

   }

   public boolean isDebugFlag() {
      return this.debugFlag;
   }

   public void setDebugFlag(boolean var1) {
      this.debugFlag = var1;
      Iterator var2 = this.next.iterator();

      while(var2.hasNext()) {
         ((RailRelation)var2.next()).docked.getSegmentController().railController.setDebugFlag(true);
      }

   }

   public void addExpectedToDock(RailRequest var1) {
      this.expectedToDock.add(var1);
   }

   public ObjectOpenHashSet getExpectedToDock() {
      return this.expectedToDock;
   }

   public float getRailSpeed() {
      return this.self.getConfigManager().apply(StatusEffectType.RAIL_SPEED, this.railSpeed);
   }

   public int getTotalDockedFromHere() {
      int var1 = this.next.size();

      RailRelation var3;
      for(Iterator var2 = this.next.iterator(); var2.hasNext(); var1 += var3.docked.getSegmentController().railController.getTotalDockedFromHere()) {
         var3 = (RailRelation)var2.next();
      }

      return var1;
   }

   public int getDockedCount() {
      return this.next.size();
   }

   public int getTurretCount() {
      int var1 = 0;
      Iterator var2 = this.next.iterator();

      while(var2.hasNext()) {
         if (((RailRelation)var2.next()).isTurretDock()) {
            ++var1;
         }
      }

      return var1;
   }

   public int getNormalDockCount() {
      int var1 = 0;
      Iterator var2 = this.next.iterator();

      while(var2.hasNext()) {
         if (!((RailRelation)var2.next()).isTurretDock()) {
            ++var1;
         }
      }

      return var1;
   }

   public boolean hasTurret() {
      Iterator var1 = this.next.iterator();

      do {
         if (!var1.hasNext()) {
            return false;
         }
      } while(!((RailRelation)var1.next()).isTurretDock());

      return true;
   }

   public RailRequest getCurrentRailRequest() {
      return this.railRequestCurrent;
   }

   public void onClear() {
      this.allConnectionsLoaded = false;
      this.didFirstTransform = false;
   }

   public void resetRailAll() {
      this.getRoot().railController.resetRailRec();
   }

   private void resetRailRec() {
      if (!this.isRoot()) {
         this.resetRail();
      }

      Iterator var1 = this.next.iterator();

      while(var1.hasNext()) {
         ((RailRelation)var1.next()).docked.getSegmentController().railController.resetRailRec();
      }

   }

   public void resetRail() {
      if (this.isDockedAndExecuted()) {
         RailController.RailReset var1;
         (var1 = new RailController.RailReset()).docker = this.previous.docked.getSegmentController().getSegmentBuffer().getPointUnsave(this.previous.docked.getAbsoluteIndex());
         var1.rail = this.previous.rail.getSegmentController().getSegmentBuffer().getPointUnsave(this.previous.rail.getAbsoluteIndex());
         if (var1.rail.getType() != 0 && var1.rail.getInfo().isRailDockable() && var1.docker.getType() != 0 && var1.docker.getInfo().isRailDocker()) {
            this.activeRailReset = var1;
         } else {
            System.err.println("[RAIL] ERROR: INCOMPATIBLE TYPES: " + this.self);
         }
      } else {
         System.err.println("[RAIL] ELEMENT IS NOT DOCKED: " + this.self);
      }
   }

   public boolean isPowered() {
      return this.currentDockingValidity == RailRelation.DockValidity.OK;
   }

   public void getDockedRecusive(Collection var1) {
      var1.add(this.self);
      Iterator var2 = this.next.iterator();

      while(var2.hasNext()) {
         ((RailRelation)var2.next()).getDockedRController().getDockedRecusive(var1);
      }

   }

   public static enum RailTarget implements TranslatableEnum {
      ROOT_ONLY,
      EVERYTHING,
      DOCKED_ONLY,
      DOCKED_AND_UPWARDS;

      public final String getName() {
         switch(this) {
         case DOCKED_AND_UPWARDS:
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RAILS_RAILCONTROLLER_22;
         case DOCKED_ONLY:
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RAILS_RAILCONTROLLER_23;
         case EVERYTHING:
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RAILS_RAILCONTROLLER_24;
         case ROOT_ONLY:
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RAILS_RAILCONTROLLER_25;
         default:
            throw new RuntimeException("Unknown value: " + this.name());
         }
      }

      public final void getTargets(SegmentController var1, Collection var2) {
         switch(this) {
         case DOCKED_AND_UPWARDS:
            var1.railController.getDockedRecusive(var2);
            return;
         case DOCKED_ONLY:
            var2.add(var1);
            return;
         case EVERYTHING:
            var2.clear();
            var1.railController.getRoot().railController.getDockedRecusive(var2);
            return;
         case ROOT_ONLY:
            var2.add(var1.railController.getRoot());
            return;
         default:
            throw new RuntimeException("Unknown value: " + this.name());
         }
      }
   }

   public class RailDockerUsableDocked extends SegmentControllerUsable {
      private boolean disReq;

      public RailDockerUsableDocked(ManagerContainer var2, SegmentController var3) {
         super(var2, var3);
      }

      public void handleMouseEvent(ControllerStateUnit var1, MouseEvent var2) {
      }

      public void onPlayerDetachedFromThisOrADock(ManagedUsableSegmentController var1, PlayerState var2, PlayerControllable var3) {
      }

      public void onSwitched(boolean var1) {
      }

      public void onLogicActivate(SegmentPiece var1, boolean var2, Timer var3) {
      }

      public boolean isAddToPlayerUsable() {
         return true;
      }

      public WeaponRowElementInterface getWeaponRow() {
         return new WeaponSegmentControllerUsableElement(this);
      }

      public boolean isControllerConnectedTo(long var1, short var3) {
         return true;
      }

      public float getWeaponSpeed() {
         return 0.0F;
      }

      public float getWeaponDistance() {
         return 0.0F;
      }

      public boolean isPlayerUsable() {
         return this.getSegmentController().railController.isDockedAndExecuted();
      }

      public long getUsableId() {
         return -9223372036854775775L;
      }

      public void handleControl(ControllerStateInterface var1, Timer var2) {
         if (var1.isPrimaryShootButtonDown() && RailController.this.isDockedAndExecuted() && !RailController.this.isOnServer() && !this.disReq) {
            if (!this.getSegmentController().isVirtualBlueprint()) {
               String var3 = this.getSegmentController().railController.getOneBeforeRoot().lastDockerPlayerServerLowerCase;
               if (this.getSegmentController().railController.getRoot() instanceof ShopSpaceStation && (!(RailController.this.self instanceof PlayerControllable) || !((PlayerControllable)RailController.this.self).getAttachedPlayers().isEmpty() && var3.length() != 0 && !((AbstractOwnerState)((PlayerControllable)RailController.this.self).getAttachedPlayers().get(0)).getName().toLowerCase(Locale.ENGLISH).equals(var3))) {
                  ((GameClientController)this.getState().getController()).popupAlertTextMessage("ALDC", StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RAILS_RAILCONTROLLER_17, this.getSegmentController().lastDockerPlayerServerLowerCase), 0.0F);
                  return;
               }

               System.err.println("[CLIENT][RAILBEAM] Disconnecting from tail");
               this.getSegmentController().railController.disconnectClient();
               this.disReq = true;
               return;
            }

            ((GameClientController)this.getState().getController()).popupAlertTextMessage("ALDC", Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RAILS_RAILCONTROLLER_18, 0.0F);
         }

      }

      public ManagerReloadInterface getReloadInterface() {
         return null;
      }

      public ManagerActivityInterface getActivityInterface() {
         return null;
      }

      public String getName() {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RAILS_RAILCONTROLLER_19;
      }

      public void handleKeyEvent(ControllerStateUnit var1, KeyboardMappings var2) {
      }

      public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
         var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RAILS_RAILCONTROLLER_20, var3, ContextFilter.IMPORTANT);
      }

      public String getWeaponRowName() {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RAILS_RAILCONTROLLER_21;
      }

      public short getWeaponRowIcon() {
         return 663;
      }
   }

   class RailReset {
      public SegmentPiece rail;
      public SegmentPiece docker;
      public boolean waitingForUndock;

      private RailReset() {
         this.waitingForUndock = true;
      }

      // $FF: synthetic method
      RailReset(Object var2) {
         this();
      }
   }

   public interface RailTrigger {
      void handle(RailRelation var1);
   }
}
