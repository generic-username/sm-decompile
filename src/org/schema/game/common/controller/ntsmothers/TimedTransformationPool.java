package org.schema.game.common.controller.ntsmothers;

import java.util.ArrayList;

public class TimedTransformationPool {
   private static final int MAX_POOL_SIZE = 13000;
   private ArrayList pool = new ArrayList();

   public TimedTransformation get() {
      synchronized(this.pool) {
         return this.pool.isEmpty() ? new TimedTransformation() : (TimedTransformation)this.pool.remove(this.pool.size() - 1);
      }
   }

   public void release(TimedTransformation var1) {
      synchronized(this.pool) {
         if (this.pool.size() + 1 > 13000) {
            var1.transform = null;
            var1.timestamp = -1L;
         } else {
            this.pool.add(var1);
         }

      }
   }
}
