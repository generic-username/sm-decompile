package org.schema.game.common.controller;

public interface SegmentBufferIteratorEmptyInterface extends SegmentBufferIteratorInterface {
   boolean handleEmpty(int var1, int var2, int var3, long var4);
}
