package org.schema.game.common.controller;

import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongList;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import java.util.Iterator;
import java.util.List;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;

public class BlockTypeSearchRunnable implements Runnable, SegmentBufferIteratorInterface {
   public final ShortOpenHashSet searchFor = new ShortOpenHashSet();
   public final BlockTypeSearchRunnable.BlockTypeSearchCallback callback;
   public final SegmentController c;
   public List datas;
   private boolean done;
   private boolean useIndex4;
   private BlockTypeSearchRunnableManager.BlockTypeSearchProgressCallback p;

   public BlockTypeSearchRunnable(SegmentController var1, BlockTypeSearchRunnableManager.BlockTypeSearchProgressCallback var2, BlockTypeSearchRunnable.BlockTypeSearchCallback var3, boolean var4, short... var5) {
      this.c = var1;
      this.callback = var3;
      this.useIndex4 = var4;
      this.p = var2;
      short[] var6 = var5;
      int var7 = var5.length;

      for(int var8 = 0; var8 < var7; ++var8) {
         short var9 = var6[var8];
         this.searchFor.add(var9);
      }

   }

   public void run() {
      this.datas = new ObjectArrayList(this.c.getSegmentBuffer().getTotalNonEmptySize());
      this.c.getSegmentBuffer().iterateOverNonEmptyElement(this, true);
      LongArrayList var1 = new LongArrayList(1024);
      Iterator var2 = this.datas.iterator();

      while(true) {
         SegmentData var3;
         Segment var4;
         do {
            if (!var2.hasNext()) {
               System.err.println("[CLIENT] Block Type Search. Found Blocks: " + var1.size());
               this.callback.handleThreaded(var1);
               this.done = true;
               return;
            }
         } while((var4 = (var3 = (SegmentData)var2.next()).getSegment()) == null);

         Vector3i var10 = var4.pos;
         int var5 = 0;

         for(short var6 = 0; var6 < 32; ++var6) {
            for(short var7 = 0; var7 < 32; ++var7) {
               for(short var8 = 0; var8 < 32; ++var8) {
                  short var9 = var3.getType(var5);
                  if (this.searchFor.contains(var9)) {
                     if (this.useIndex4) {
                        var1.add(ElementCollection.getIndex4((short)(var10.x + var8), (short)(var10.y + var7), (short)(var10.z + var6), var9));
                     } else {
                        var1.add(ElementCollection.getIndex(var10.x + var8, var10.y + var7, var10.z + var6));
                     }
                  }

                  ++var5;
               }
            }
         }
      }
   }

   public boolean handle(Segment var1, long var2) {
      SegmentData var4;
      if ((var4 = var1.getSegmentData()) != null) {
         this.datas.add(var4);
      }

      return true;
   }

   public boolean isDone() {
      return this.done;
   }

   public void executeSynchAfterDone() {
      this.callback.executeAfterDone();
   }

   public void cleanUpAfterDone() {
      this.searchFor.clear();
      this.datas.clear();
      this.p.onDone();
   }

   public interface BlockTypeSearchCallback {
      void handleThreaded(LongList var1);

      void executeAfterDone();
   }
}
