package org.schema.game.common.controller;

import com.bulletphysics.linearmath.Transform;
import com.googlecode.javaewah.EWAHCompressedBitmap;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.SegmentRetrieveCallback;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.schine.graphicsengine.forms.BoundingBox;

public interface SegmentBufferInterface {
   void addImmediate(Segment var1);

   int clear(boolean var1);

   boolean containsIndex(long var1);

   boolean containsKey(int var1, int var2, int var3);

   boolean containsKey(Vector3i var1);

   boolean containsValue(Segment var1);

   boolean existsPointUnsave(Vector3i var1);

   boolean existsPointUnsave(long var1);

   boolean existsPointUnsave(int var1, int var2, int var3);

   Segment get(int var1, int var2, int var3);

   Segment get(long var1);

   Segment get(Vector3i var1);

   BoundingBox getBoundingBox();

   SegmentBuffer getBuffer(Vector3i var1);

   long getLastInteraction();

   long getLastSegmentLoadChanged();

   SegmentPiece getPointUnsave(int var1, int var2, int var3);

   SegmentPiece getPointUnsave(int var1, int var2, int var3, SegmentPiece var4);

   SegmentPiece getPointUnsave(long var1);

   SegmentPiece getPointUnsave(long var1, SegmentPiece var3);

   SegmentPiece getPointUnsave(Vector3i var1);

   SegmentPiece getPointUnsave(Vector3i var1, SegmentPiece var2);

   SegmentController getSegmentController();

   void setSegmentController(SegmentController var1);

   int getTotalNonEmptySize();

   int getTotalSize();

   boolean handle(Vector3i var1, SegmentBufferIteratorInterface var2, long var3);

   boolean handleNonEmpty(int var1, int var2, int var3, SegmentBufferIteratorInterface var4, long var5);

   boolean handleNonEmpty(Vector3i var1, SegmentBufferIteratorInterface var2, long var3);

   void incActive(int var1, Segment var2);

   boolean isEmpty();

   void setEmpty(Vector3i var1);

   boolean isUntouched();

   void iterateIntersecting(SegmentBufferInterface var1, Transform var2, SegmentBufferIntersectionInterface var3, SegmentBufferIntersectionVariables var4, Vector3i var5, Vector3i var6, Vector3i var7, Vector3i var8);

   boolean iterateOverEveryElement(SegmentBufferIteratorEmptyInterface var1, boolean var2);

   boolean iterateOverNonEmptyElement(SegmentBufferIteratorInterface var1, boolean var2);

   boolean iterateOverEveryChangedElement(SegmentBufferIteratorEmptyInterface var1, boolean var2);

   boolean iterateOverNonEmptyElementRange(SegmentBufferIteratorInterface var1, int var2, int var3, int var4, int var5, int var6, int var7, boolean var8);

   void onAddedElement(short var1, int var2, byte var3, byte var4, byte var5, Segment var6, long var7, byte var9);

   void onRemovedElementClient(short var1, int var2, byte var3, byte var4, byte var5, Segment var6, long var7);

   Segment removeImmediate(Vector3i var1, boolean var2);

   void restructBB();

   void restructBBFast(SegmentData var1);

   void setLastInteraction(long var1, Vector3i var3);

   int size();

   void update();

   void updateBB(Segment var1);

   void updateLastSegmentLoadChanged();

   void updateNumber();

   boolean iterateOverNonEmptyElementRange(SegmentBufferIteratorInterface var1, Vector3i var2, Vector3i var3, boolean var4);

   long getLastChanged(Vector3i var1);

   void setLastChanged(Vector3i var1, long var2);

   void get(Vector3i var1, SegmentRetrieveCallback var2);

   void get(int var1, int var2, int var3, SegmentRetrieveCallback var4);

   int getSegmentState(Vector3i var1);

   int getSegmentState(int var1, int var2, int var3);

   int getSegmentState(long var1);

   int setEmpty(int var1, int var2, int var3);

   EWAHCompressedBitmap applyBitMap(long var1, EWAHCompressedBitmap var3);

   void insertFromBitset(Vector3i var1, long var2, EWAHCompressedBitmap var4, SegmentBufferIteratorEmptyInterface var5);

   long getLastBufferSaved();

   void setLastBufferSaved(long var1);

   long getLastBufferChanged();

   boolean isFullyLoaded();

   void restructBBFastOnRemove(Vector3i var1);

   void onSegmentBecameEmpty(Segment var1);
}
