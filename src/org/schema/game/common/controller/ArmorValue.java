package org.schema.game.common.controller;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;

public class ArmorValue {
   public ObjectArrayList typesHit = new ObjectArrayList();
   public float armorValueAccumulatedRaw;
   public float armorIntegrity;
   public float totalArmorValue;

   public void reset() {
      this.typesHit.clear();
      this.armorValueAccumulatedRaw = 0.0F;
      this.armorIntegrity = 0.0F;
      this.totalArmorValue = 0.0F;
   }

   public void calculate() {
      assert !this.typesHit.isEmpty();

      this.armorIntegrity /= (float)this.typesHit.size();
      this.totalArmorValue = this.armorValueAccumulatedRaw * this.armorIntegrity;
   }

   public void set(ArmorValue var1) {
      this.reset();
      this.typesHit.addAll(var1.typesHit);
      this.armorValueAccumulatedRaw = var1.armorValueAccumulatedRaw;
      this.armorIntegrity = var1.armorIntegrity;
      this.totalArmorValue = var1.totalArmorValue;
   }
}
