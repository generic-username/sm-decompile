package org.schema.game.common.controller;

import java.util.Comparator;
import org.schema.common.config.ConfigParserException;
import org.w3c.dom.Node;

public class HpCondition implements Comparable, Comparator {
   public float hpPercent;
   public HpTrigger trigger;

   public static HpCondition parse(Node var0) throws ConfigParserException {
      HpCondition var1 = new HpCondition();

      try {
         var1.hpPercent = Float.parseFloat(var0.getAttributes().getNamedItem("conditionhp").getNodeValue());
      } catch (Exception var2) {
         var2.printStackTrace();
         throw new ConfigParserException(var0.getParentNode().getNodeName() + " -> " + var0.getNodeName() + "; must have valid attribute 'conditionhp'");
      }

      var1.trigger = HpTrigger.parse(var0);
      return var1;
   }

   public int compare(HpCondition var1, HpCondition var2) {
      return var1.compareTo(var2);
   }

   public int compareTo(HpCondition var1) {
      return Float.compare(this.hpPercent, var1.hpPercent);
   }
}
