package org.schema.game.common.controller.elements.shop;

import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;

public class ShopCollectionManager extends ControlBlockElementCollectionManager {
   public ShopCollectionManager(SegmentPiece var1, SegmentController var2, ShopElementManager var3) {
      super(var1, (short)347, var2, var3);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return ShopUnit.class;
   }

   public boolean needsUpdate() {
      return false;
   }

   public ShopUnit getInstance() {
      return new ShopUnit();
   }

   public boolean isUsingIntegrity() {
      return false;
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[0];
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHOP_SHOPCOLLECTIONMANAGER_0;
   }
}
