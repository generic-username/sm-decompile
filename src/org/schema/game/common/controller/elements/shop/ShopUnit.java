package org.schema.game.common.controller.elements.shop;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.element.ElementCollection;

public class ShopUnit extends ElementCollection {
   public String toString() {
      return "ShopUnit " + super.toString();
   }

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((ShopElementManager)((ShopCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (ShopCollectionManager)this.elementCollectionManager, var2, var3);
   }
}
