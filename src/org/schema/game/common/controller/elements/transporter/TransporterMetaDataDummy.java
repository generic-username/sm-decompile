package org.schema.game.common.controller.elements.transporter;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class TransporterMetaDataDummy extends BlockMetaDataDummy {
   public String destinationUID;
   public Vector3i destinationBlock;
   public String name;
   public byte publicAccess;

   protected void fromTagStructrePriv(Tag var1, int var2) {
      Tag[] var3 = (Tag[])var1.getValue();
      this.name = (String)var3[0].getValue();
      this.destinationUID = (String)var3[1].getValue();
      this.destinationBlock = (Vector3i)var3[2].getValue();
      this.destinationBlock.add(var2, var2, var2);
      this.publicAccess = (Byte)var3[3].getValue();
   }

   public String getTagName() {
      return "TR";
   }

   protected Tag toTagStructurePriv() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.STRING, (String)null, this.name), new Tag(Tag.Type.STRING, (String)null, this.destinationUID), new Tag(Tag.Type.VECTOR3i, (String)null, this.destinationBlock), new Tag(Tag.Type.BYTE, (String)null, this.publicAccess), FinishTag.INST});
   }
}
