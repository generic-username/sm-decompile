package org.schema.game.common.controller.elements.jumpdrive;

import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import org.schema.common.FastMath;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.effectblock.EffectElementManager;
import org.schema.game.common.controller.elements.power.PowerAddOn;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.network.objects.remote.RemoteValueUpdate;
import org.schema.game.network.objects.valueUpdate.JumpChargeValueUpdate;
import org.schema.game.network.objects.valueUpdate.NTValueUpdateInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.input.InputType;
import org.schema.schine.resource.tag.Tag;

public class JumpDriveCollectionManager extends ControlBlockElementCollectionManager implements PlayerUsableInterface {
   private float initialCharge;
   private long lastSent;
   private long lastSentZero;
   private short lastCharge;

   public JumpDriveCollectionManager(SegmentPiece var1, SegmentController var2, JumpDriveElementManager var3) {
      super(var1, (short)545, var2, var3);
   }

   protected void applyMetaData(BlockMetaDataDummy var1) {
      assert this.initialCharge == 0.0F;

      this.initialCharge = ((JumpDriveMetaDataDummy)var1).charge;
   }

   public void updateStructure(long var1) {
      LongOpenHashSet var3;
      if (this.getSegmentController().isOnServer() && this.initialCharge > 0.0F && (var3 = (LongOpenHashSet)this.getSegmentController().getControlElementMap().getControllingMap().getAll().get(ElementCollection.getIndex(this.getControllerPos()))) != null && var3.size() <= this.getTotalSize()) {
         this.setCharge(this.initialCharge);
         this.initialCharge = 0.0F;
         this.sendChargeUpdate();
      }

      super.updateStructure(var1);
   }

   protected Tag toTagStructurePriv() {
      return new Tag(Tag.Type.FLOAT, (String)null, this.getCharge());
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return JumpDriveUnit.class;
   }

   public void handleControl(ControllerStateInterface var1, Timer var2) {
      ((JumpDriveElementManager)this.getElementManager()).handle(var1, var2);
   }

   public boolean needsUpdate() {
      return false;
   }

   public JumpDriveUnit getInstance() {
      return new JumpDriveUnit();
   }

   protected void onChangedCollection() {
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
      }

      if (this.getSegmentController().isOnServer()) {
         this.setCharge(0.0F);
         this.sendChargeUpdate();
      }

   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      int var1;
      for(var1 = 0; var1 < this.getElementCollections().size(); ++var1) {
         ((JumpDriveUnit)this.getElementCollections().get(var1)).getPowerConsumption();
      }

      var1 = (int)FastMath.ceil((float)((double)this.getSegmentController().getMass() * 10.0D * (double)JumpDriveElementManager.RATIO_NEEDED_TO_TOTAL));
      int var2 = Math.max(0, this.getTotalSize() - var1);
      float var3 = (float)(this.getTotalSize() - var2) * JumpDriveElementManager.CHARGE_NEEDED_FOR_JUMP_PER_BLOCK;
      float var4 = (float)var2 * JumpDriveElementManager.CHARGE_NEEDED_FOR_JUMP_PER_BLOCK_AFTER_RATIO;
      return new GUIKeyValueEntry[]{new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVECOLLECTIONMANAGER_1, this.getChargeNeededForJump()), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVECOLLECTIONMANAGER_2, JumpDriveElementManager.CHARGE_NEEDED_FOR_JUMP_PER_BLOCK + " x " + (this.getTotalSize() - var2) + " = " + var3), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVECOLLECTIONMANAGER_3, JumpDriveElementManager.CHARGE_NEEDED_FOR_JUMP_PER_BLOCK_AFTER_RATIO + " x " + var2 + " = " + var4), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVECOLLECTIONMANAGER_4, this.getChargeAddedPerSec()), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVECOLLECTIONMANAGER_5, JumpDriveElementManager.RATIO_NEEDED_TO_TOTAL), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVECOLLECTIONMANAGER_6, StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVECOLLECTIONMANAGER_7, this.getTotalSize(), var1))};
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVECOLLECTIONMANAGER_8;
   }

   public float getChargeAddedPerSec() {
      return JumpDriveElementManager.CHARGE_ADDED_PER_SECOND + JumpDriveElementManager.CHARGE_ADDED_PER_SECOND_PER_BLOCK * (float)this.getTotalSize();
   }

   public void charge(Timer var1) {
      if (this.isCharged()) {
         this.getSegmentController().popupOwnClientMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVECOLLECTIONMANAGER_10, 1);
      } else {
         if (this.getTotalSize() > 0) {
            if (this.lastCharge != this.getState().getNumberOfUpdate()) {
               PowerAddOn var2 = ((PowerManagerInterface)this.getContainer()).getPowerAddOn();
               float var3 = var1.getDelta() * this.getChargeAddedPerSec();
               if (var2.consumePowerInstantly((double)var3)) {
                  float var4 = this.getCharge();
                  this.setCharge(Math.min(this.getChargeNeededForJump(), this.getCharge() + var3));
                  if (this.isCharged()) {
                     this.getSegmentController().popupOwnClientMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVECOLLECTIONMANAGER_9, 1);
                     if (var4 < this.getCharge()) {
                        this.sendChargeUpdate();
                     }
                  }
               } else if (this.getSegmentController().isClientOwnObject() && ((GameClientState)this.getState()).getWorldDrawer() != null) {
                  ((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().notifyEffectHit(this.getSegmentController(), EffectElementManager.OffensiveEffects.NO_POWER);
               }

               this.lastCharge = this.getState().getNumberOfUpdate();
               return;
            }
         } else if (this.getSegmentController().isClientOwnObject() && ((GameClientState)this.getState()).getWorldDrawer() != null) {
            ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVECOLLECTIONMANAGER_11, "JDMCNO", 0.0F);
         }

      }
   }

   public void jump() {
      if (this.getSegmentController().isOnServer()) {
         if (this.isCharged()) {
            if (this.getSegmentController().engageJump(JumpDriveElementManager.BASE_DISTANCE_SECTORS)) {
               this.setCharge(0.0F);
               this.sendChargeUpdate();
               return;
            }
         } else if (System.currentTimeMillis() - this.lastSent > 3000L) {
            this.getSegmentController().sendControllingPlayersServerMessage(new Object[]{45, StringTools.formatPointZero(this.getCharge()), StringTools.formatPointZero(this.getChargeNeededForJump())}, 3);
            this.lastSent = System.currentTimeMillis();
            return;
         }
      } else if (this.isCharged()) {
         this.setCharge(0.0F);
      }

   }

   public boolean isCharged() {
      return this.getCharge() >= this.getChargeNeededForJump();
   }

   public float getChargeNeededForJump() {
      int var1 = (int)FastMath.ceil((float)((double)this.getSegmentController().getMass() * 10.0D * (double)JumpDriveElementManager.RATIO_NEEDED_TO_TOTAL));
      var1 = Math.max(0, this.getTotalSize() - var1);
      float var2 = (float)(this.getTotalSize() - var1) * JumpDriveElementManager.CHARGE_NEEDED_FOR_JUMP_PER_BLOCK;
      float var5 = (float)var1 * JumpDriveElementManager.CHARGE_NEEDED_FOR_JUMP_PER_BLOCK_AFTER_RATIO;
      double var3 = 0.0D;
      if ((double)this.getTotalSize() / ((double)this.getSegmentController().getMass() * 10.0D) < (double)JumpDriveElementManager.RATIO_NEEDED_TO_TOTAL) {
         var3 = 1.0D - (double)this.getTotalSize() / ((double)this.getSegmentController().getMass() * 10.0D) / (double)JumpDriveElementManager.RATIO_NEEDED_TO_TOTAL;
      }

      return (float)((double)(var5 += JumpDriveElementManager.CHARGE_NEEDED_FOR_JUMP_FIX + var2) + var3 * (double)var5);
   }

   public float getCharge() {
      return this.getContainer().floatValueMap.get(ElementCollection.getIndex(this.getControllerPos()));
   }

   public void setCharge(float var1) {
      this.getContainer().floatValueMap.put(ElementCollection.getIndex(this.getControllerPos()), var1);
   }

   public void sendChargeUpdate() {
      JumpChargeValueUpdate var1;
      (var1 = new JumpChargeValueUpdate()).setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), this.getControllerElement().getAbsoluteIndex());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   public void onHit(double var1, DamageDealerType var3) {
      if (this.getSegmentController().isOnServer() && this.getCharge() > 0.0F && System.currentTimeMillis() - this.lastSentZero > 5000L) {
         this.setCharge(0.0F);
         this.sendChargeUpdate();
         this.lastSentZero = System.currentTimeMillis();
      }

   }

   public void discharge(double var1) {
      this.setCharge((float)Math.max(0.0D, (double)this.getCharge() - var1));
   }

   public float getSensorValue(SegmentPiece var1) {
      return Math.min(1.0F, this.getCharge() / Math.max(1.0E-4F, this.getChargeNeededForJump()));
   }

   public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
      var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVECOLLECTIONMANAGER_12, var3, ContextFilter.IMPORTANT);
      var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.SECONDARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVECOLLECTIONMANAGER_13, var3, ContextFilter.IMPORTANT);
   }
}
