package org.schema.game.common.controller.elements.jumpdrive;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.FireingUnit;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;

public class JumpDriveUnit extends FireingUnit {
   private float getBaseConsume() {
      return JumpDriveElementManager.CHARGE_ADDED_PER_SECOND + (float)this.size() * JumpDriveElementManager.CHARGE_ADDED_PER_SECOND_PER_BLOCK;
   }

   public String toString() {
      return "JumpDriveUnit " + super.toString();
   }

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((JumpDriveElementManager)((JumpDriveCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (JumpDriveCollectionManager)this.elementCollectionManager, var2, var3);
   }

   public float getBasePowerConsumption() {
      assert false;

      return 0.0F;
   }

   public float getPowerConsumption() {
      return this.getBaseConsume();
   }

   public float getPowerConsumptionWithoutEffect() {
      return this.getBaseConsume();
   }

   public float getReloadTimeMs() {
      return (float)JumpDriveElementManager.RELOAD_AFTER_USE_MS;
   }

   public float getInitializationTime() {
      return (float)JumpDriveElementManager.RELOAD_AFTER_USE_MS;
   }

   public float getDistanceRaw() {
      return (float)JumpDriveElementManager.BASE_DISTANCE_SECTORS;
   }

   public float getFiringPower() {
      return 0.0F;
   }

   public double getPowerConsumedPerSecondResting() {
      return (double)this.size();
   }

   public double getPowerConsumedPerSecondCharging() {
      return (double)this.size();
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.JUMP_DRIVE;
   }

   public float getDamage() {
      return 0.0F;
   }
}
