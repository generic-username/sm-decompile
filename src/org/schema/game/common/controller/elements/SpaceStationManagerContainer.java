package org.schema.game.common.controller.elements;

import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.data.player.inventory.InventoryHolder;
import org.schema.schine.network.StateInterface;

public class SpaceStationManagerContainer extends StationaryManagerContainer implements DoorContainerInterface, LiftContainerInterface, ShieldContainerInterface, InventoryHolder {
   public SpaceStationManagerContainer(StateInterface var1, SpaceStation var2) {
      super(var1, var2);
   }
}
