package org.schema.game.common.controller.elements;

import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.controller.elements.shipyard.ShipyardUnit;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class ElementCollectionCalculationThreadExecution {
   private final ElementCollectionManager man;
   private final ObjectArrayList collections = new ObjectArrayList();
   private ElementCollection lastElementCollection;
   private LongOpenHashSet closedCollection;
   private LongArrayList openCollection;
   private CollectionCalculationCallback callback;
   private int pointer;
   private LongArrayList rawCollection;
   private Random random = new Random();
   private final long updateInProgressSignature;
   private LongOpenHashSet totalSet;
   private final List collectionSetList = new ObjectArrayList();
   private static List setPool = new ObjectArrayList();
   private int pidGen;

   public ElementCollectionCalculationThreadExecution(ElementCollectionManager var1) {
      this.man = var1;
      this.updateInProgressSignature = var1.updateInProgress;
   }

   public int getRawSize() {
      FastCopyLongOpenHashSet var1;
      return (var1 = this.man.rawCollection) != null ? var1.size() : 0;
   }

   public String toString() {
      LongArrayList var1 = this.rawCollection;
      return "EXEC(" + this.man + "; raw: " + (var1 != null ? var1.size() : -1) + ")";
   }

   public static void main(String[] var0) {
      for(int var6 = -100; var6 < 100; ++var6) {
         for(int var1 = -100; var1 < 100; ++var1) {
            for(int var2 = -100; var2 < 100; ++var2) {
               Vector3i var3;
               ElementCollection.getIndex(var3 = new Vector3i(var2, var1, var6));

               for(int var4 = 0; var4 < 6; ++var4) {
                  Vector3i var5;
                  (var5 = new Vector3i(var3)).add(Element.DIRECTIONSi[var4]);
                  ElementCollection.getIndex(var5);
               }
            }
         }
      }

   }

   public void apply() {
      this.man.clearCollectionForApply();
      this.man.getElementCollections().addAll(this.getCollections());
      Iterator var1;
      ElementCollection var2;
      if (!this.getCollections().isEmpty() && this.getCollections().get(0) instanceof PowerConsumer) {
         var1 = this.getCollections().iterator();

         while(var1.hasNext()) {
            var2 = (ElementCollection)var1.next();
            this.man.getElementManager().getManagerContainer().addConsumer((PowerConsumer)var2);
         }
      }

      if (!this.getCollections().isEmpty() && this.getCollections().get(0) instanceof PlayerUsableInterface) {
         var1 = this.getCollections().iterator();

         while(var1.hasNext()) {
            var2 = (ElementCollection)var1.next();
            this.man.getElementManager().getManagerContainer().addPlayerUsable((PlayerUsableInterface)var2);
         }
      }

      if (!this.man.getSegmentController().isOnServer()) {
         var1 = this.getCollections().iterator();

         while(var1.hasNext()) {
            if ((var2 = (ElementCollection)var1.next()).getMesh() != null && var2.getMesh().isDraw()) {
               ((GameClientState)this.man.getSegmentController().getState()).getWorldDrawer().getSegmentDrawer().getElementCollectionDrawer().flagUpdate();
               break;
            }
         }
      }

      this.getCollections().clear();
   }

   public void flagUpdateFinished() {
      this.getMan().flagUpdateFinished(this);
   }

   public ElementCollection getCollectionInstance() {
      return this.getMan().newElementCollection(this.getMan().getEnhancerClazz(), this.getMan(), this.getMan().getSegmentController());
   }

   public ObjectArrayList getCollections() {
      return this.collections;
   }

   public ElementCollectionManager getMan() {
      return this.man;
   }

   public int hashCode() {
      return this.man.hashCode();
   }

   public boolean equals(Object var1) {
      return this.man == ((ElementCollectionCalculationThreadExecution)var1).man;
   }

   public void initialize(LongOpenHashSet var1, LongOpenHashSet var2, LongArrayList var3, LongArrayList var4, CollectionCalculationCallback var5) {
      this.totalSet = var2;
      this.closedCollection = var1;
      this.openCollection = var4;
      this.rawCollection = var3;
      this.callback = var5;
      this.pointer = 0;
   }

   private void onCalculationFinished() {
      this.onFinish();
      this.callback.callback(this);
   }

   public void onFinish() {
      this.lastElementCollection = null;
      this.closedCollection = null;
      this.openCollection = null;
   }

   public static LongOpenHashSet allocateLongSet() {
      synchronized(setPool) {
         return setPool.isEmpty() ? new LongOpenHashSet(256) : (LongOpenHashSet)setPool.remove(setPool.size() - 1);
      }
   }

   public static void freeLongSet(LongOpenHashSet var0) {
      synchronized(setPool) {
         setPool.add(var0);
      }
   }

   public void process() {
      assert this.rawCollection.size() == this.closedCollection.size() : this.rawCollection.size() + "; " + this.closedCollection.size();

      while(true) {
         int var4;
         int var20;
         while(!this.closedCollection.isEmpty()) {
            int var3;
            ElementCollection var5;
            int var6;
            int var7;
            int var8;
            int var10;
            int var11;
            int var24;
            long var28;
            int var29;
            if (this.man.requiredNeigborsPerBlock() == ElementCollectionManager.CollectionShape.PROXIMITY) {
               float var17 = ((ProximityCollectionInterface)this.man).getGroupProximity();

               for(ObjectArrayList var21 = new ObjectArrayList(); !this.closedCollection.isEmpty(); var21.clear()) {
                  long var19;
                  do {
                     assert !this.closedCollection.isEmpty();

                     assert this.pointer < this.rawCollection.size() : "\n" + this.closedCollection + "; \n" + this.rawCollection;

                     var19 = this.rawCollection.getLong(this.pointer);
                     ++this.pointer;

                     assert this.pointer < this.rawCollection.size() || this.closedCollection.contains(var19) : var19 + " " + this.pointer + "/" + this.rawCollection.size() + ";\n" + this.closedCollection + "; \n" + this.rawCollection;
                  } while(!this.closedCollection.remove(var19));

                  var24 = ElementCollection.getPosX(var19);
                  var6 = ElementCollection.getPosY(var19);
                  var7 = ElementCollection.getPosZ(var19);
                  var8 = 0;

                  for(var29 = 0; var29 < 6; ++var29) {
                     long var30 = ElementCollection.getSide(var24, var6, var7, var29);
                     if (this.totalSet.contains(var30)) {
                        ++var8;
                     }
                  }

                  for(var29 = 0; var29 < this.collectionSetList.size(); ++var29) {
                     ElementCollectionCalculationThreadExecution.CollectionSetContainer var31;
                     if ((var31 = (ElementCollectionCalculationThreadExecution.CollectionSetContainer)this.collectionSetList.get(var29)).contains(var24, var6, var7)) {
                        var31.add(var19, var24, var6, var7, var17);
                        var31.addTouching(var8);
                        var21.add(var31);
                     }
                  }

                  ElementCollectionCalculationThreadExecution.CollectionSetContainer var32;
                  if (var21.size() > 1) {
                     var32 = (ElementCollectionCalculationThreadExecution.CollectionSetContainer)var21.remove(var21.size() - 1);

                     for(var10 = 0; var10 < var21.size(); ++var10) {
                        ElementCollectionCalculationThreadExecution.CollectionSetContainer var33 = (ElementCollectionCalculationThreadExecution.CollectionSetContainer)var21.get(var10);
                        var32.combine(var33);
                        var33.free();
                        this.collectionSetList.remove(var33);
                     }
                  } else if (var21.isEmpty()) {
                     (var32 = new ElementCollectionCalculationThreadExecution.CollectionSetContainer(this.pidGen++)).add(var19, var24, var6, var7, var17);
                     var32.addTouching(var8);
                     this.collectionSetList.add(var32);
                  }
               }

               for(var3 = 0; var3 < this.collectionSetList.size(); ++var3) {
                  ElementCollectionCalculationThreadExecution.CollectionSetContainer var25 = (ElementCollectionCalculationThreadExecution.CollectionSetContainer)this.collectionSetList.get(var3);
                  if (!(var5 = this.getCollectionInstance()).isDetailedNeighboringCollection()) {
                     var5.takeVolatileCollection();
                  }

                  var5.prevalid = true;
                  Iterator var27 = var25.s.iterator();

                  while(var27.hasNext()) {
                     var29 = ElementCollection.getPosX(var28 = (Long)var27.next());
                     var10 = ElementCollection.getPosY(var28);
                     var11 = ElementCollection.getPosZ(var28);
                     var5.addElement(var28, var29, var10, var11);
                  }

                  for(var6 = 0; var6 < var25.touching.length; ++var6) {
                     var5.touching[var6] = var25.touching[var6];
                  }

                  this.getCollections().add(var5);
                  this.lastElementCollection = var5;
                  var25.free();
               }

               this.collectionSetList.clear();
            } else {
               long var1;
               if (this.openCollection.isEmpty()) {
                  assert !this.closedCollection.isEmpty();

                  do {
                     assert !this.closedCollection.isEmpty();

                     assert this.pointer < this.rawCollection.size() : "\n" + this.closedCollection + "; \n" + this.rawCollection;

                     var1 = this.rawCollection.getLong(this.pointer);
                     ++this.pointer;

                     assert this.pointer < this.rawCollection.size() || this.closedCollection.contains(var1) : var1 + " " + this.pointer + "/" + this.rawCollection.size() + ";\n" + this.closedCollection + "; \n" + this.rawCollection;
                  } while(!this.closedCollection.remove(var1));

                  this.openCollection.add(var1);
                  if (!(var5 = this.getCollectionInstance()).isDetailedNeighboringCollection()) {
                     var5.takeVolatileCollection();
                  }

                  var5.prevalid = true;
                  var6 = ElementCollection.getPosX(var1);
                  var7 = ElementCollection.getPosY(var1);
                  var8 = ElementCollection.getPosZ(var1);
                  var5.addElement(var1, var6, var7, var8);
                  this.getCollections().add(var5);
                  this.lastElementCollection = var5;
               }

               if (!this.openCollection.isEmpty()) {
                  int var10002;
                  if (this.man.requiredNeigborsPerBlock() == ElementCollectionManager.CollectionShape.ALL_IN_ONE) {
                     this.openCollection.clear();
                     this.openCollection.addAll(this.closedCollection);

                     while(!this.openCollection.isEmpty()) {
                        var3 = ElementCollection.getPosX(var1 = this.openCollection.removeLong(this.openCollection.size() - 1));
                        var4 = ElementCollection.getPosY(var1);
                        var24 = ElementCollection.getPosZ(var1);
                        this.lastElementCollection.addElement(var1, var3, var4, var24);
                        var6 = 0;

                        for(var7 = 0; var7 < 6; ++var7) {
                           long var26 = ElementCollection.getSide(var3, var4, var24, var7);
                           if (this.closedCollection.contains(var26)) {
                              ++var6;
                           }
                        }

                        var10002 = this.lastElementCollection.touching[var6]++;
                     }

                     assert this.openCollection.isEmpty();

                     this.closedCollection.clear();
                  } else if (this.man.requiredNeigborsPerBlock() == ElementCollectionManager.CollectionShape.SEPERATED) {
                     this.openCollection.clear();
                  } else {
                     while(!this.openCollection.isEmpty()) {
                        var4 = ElementCollection.getPosX(var1 = this.openCollection.removeLong(this.openCollection.size() - 1));
                        var24 = ElementCollection.getPosY(var1);
                        var6 = ElementCollection.getPosZ(var1);
                        var7 = 0;

                        for(var8 = 0; var8 < 6; ++var8) {
                           long var9 = ElementCollection.getSide(var4, var24, var6, var8);
                           if (this.closedCollection.remove(var9)) {
                              this.lastElementCollection.addElement(var9, var4 + Element.DIRECTIONSi[var8].x, var24 + Element.DIRECTIONSi[var8].y, var6 + Element.DIRECTIONSi[var8].z);
                              this.openCollection.add(var9);
                              ++var7;
                           } else if (this.totalSet.contains(var9)) {
                              ++var7;
                           }
                        }

                        var10002 = this.lastElementCollection.touching[var7]++;
                     }
                  }

                  this.lastElementCollection.setValid(true);
                  if (this.man.requiredNeigborsPerBlock() != ElementCollectionManager.CollectionShape.ANY_NEIGHBOR) {
                     if (this.man.requiredNeigborsPerBlock() == ElementCollectionManager.CollectionShape.LOOP) {
                        for(var20 = 0; var20 < 7; ++var20) {
                           if (var20 != 2 && this.lastElementCollection.touching[var20] > 0) {
                              this.lastElementCollection.setValid(false);
                              this.lastElementCollection.prevalid = false;
                              break;
                           }
                        }
                     } else if (this.man.requiredNeigborsPerBlock() == ElementCollectionManager.CollectionShape.RIP) {
                        int var16 = 0;
                        long var2 = Long.MIN_VALUE;
                        long var23 = Long.MIN_VALUE;

                        for(var6 = 0; var6 < this.lastElementCollection.getNeighboringCollectionUnsave().size(); ++var6) {
                           var29 = ElementCollection.getPosX(var28 = this.lastElementCollection.getNeighboringCollectionUnsave().getLong(var6));
                           var10 = ElementCollection.getPosY(var28);
                           var11 = ElementCollection.getPosZ(var28);
                           int var12 = 0;

                           for(int var13 = 0; var13 < 6; ++var13) {
                              long var14 = ElementCollection.getSide(var29, var10, var11, var13);
                              if (this.lastElementCollection.getNeighboringCollectionUnsave().contains(var14)) {
                                 ++var12;
                              }
                           }

                           if (var12 != 2) {
                              if (var12 != 1 || var16 >= 2) {
                                 var2 = var2 == Long.MIN_VALUE ? var28 : var2;
                                 var23 = var23 == Long.MIN_VALUE ? var28 : var23;
                                 this.lastElementCollection.setValid(false);
                                 this.lastElementCollection.setInvalidReason("Unit Group Invalid! Must be in a C shape with exactly two ends");
                                 this.lastElementCollection.prevalid = false;
                                 break;
                              }

                              if (var16 == 0) {
                                 var2 = var28;
                              } else if (var28 > var2) {
                                 var23 = var2;
                                 var2 = var28;
                              } else {
                                 var23 = var28;
                              }

                              ++var16;
                           }
                        }

                        ((ShipyardUnit)this.lastElementCollection).endA = var2;
                        ((ShipyardUnit)this.lastElementCollection).endB = var23;
                        boolean var10000 = this.lastElementCollection.prevalid;
                        if (this.lastElementCollection.prevalid) {
                           assert var16 == 2;

                           assert var2 != Long.MIN_VALUE;

                           assert var23 != Long.MIN_VALUE;

                           var6 = ElementCollection.getPosX(var2);
                           var7 = ElementCollection.getPosY(var2);
                           var8 = ElementCollection.getPosZ(var2);
                           var29 = ElementCollection.getPosX(var23);
                           var10 = ElementCollection.getPosY(var23);
                           var11 = ElementCollection.getPosZ(var23);
                           if ((var6 != var29 || var7 != var10) && (var6 != var29 || var8 != var11) && (var7 != var10 || var8 != var11)) {
                              this.lastElementCollection.setInvalidReason("Unit Group Invalid! The end blocks of the C must be in a straight line");
                              this.lastElementCollection.setValid(false);
                              this.lastElementCollection.prevalid = false;
                           }
                        }
                     }
                  }
               }
            }
         }

         boolean var18 = false;
         if (!this.man.getSegmentController().isOnServer() && Math.abs(this.man.drawnUpdateNumber - this.man.getSegmentController().getState().getNumberOfUpdate()) < 10) {
            var18 = true;
         }

         for(var20 = 0; var20 < this.getCollections().size(); ++var20) {
            ElementCollection var22;
            ElementCollection var34 = var22 = (ElementCollection)this.getCollections().get(var20);
            var34.setSize(var34.getNeighboringCollectionUnsave().size());
            this.random.setSeed((long)this.man.getSegmentController().getId() + var22.getSignificator() + (long)var22.size());
            var4 = this.random.nextInt(var22.size());
            var22.setRandomlySelectedFromLastThreadUpdate(var22.getNeighboringCollectionUnsave().getLong(var4));
            if (var22.prevalid) {
               var22.calculateExtraDataAfterCreationThreaded(this.updateInProgressSignature, this.totalSet);
            }

            if (!this.man.getSegmentController().isOnServer() && EngineSettings.CREATE_MANAGER_MESHES.isOn() && var22.hasMesh()) {
               var22.calculateMesh(this.updateInProgressSignature, var18);
            }

            if (!this.man.isDetailedElementCollections()) {
               var22.storeRandomBlocksForNonDetailed();
               var22.freeVolatileCollection();
            }
         }

         this.onCalculationFinished();
         return;
      }
   }

   static class CollectionSetContainer {
      private float minX = Float.POSITIVE_INFINITY;
      private float minY = Float.POSITIVE_INFINITY;
      private float minZ = Float.POSITIVE_INFINITY;
      private float maxX = Float.NEGATIVE_INFINITY;
      private float maxY = Float.NEGATIVE_INFINITY;
      private float maxZ = Float.NEGATIVE_INFINITY;
      private int[] touching = new int[7];
      LongOpenHashSet s;
      private int id;

      public CollectionSetContainer(int var1) {
         this.id = var1;
         this.allocate();
      }

      public void allocate() {
         this.s = ElementCollectionCalculationThreadExecution.allocateLongSet();
      }

      public void free() {
         Arrays.fill(this.touching, 0);
         this.s.clear();
         ElementCollectionCalculationThreadExecution.freeLongSet(this.s);
      }

      public boolean contains(int var1, int var2, int var3) {
         return (float)var1 >= this.minX && (float)var1 <= this.maxX && (float)var2 >= this.minY && (float)var2 <= this.maxY && (float)var3 >= this.minZ && (float)var3 <= this.maxZ;
      }

      public void add(long var1, int var3, int var4, int var5, float var6) {
         this.s.add(var1);
         this.minX = Math.min(this.minX, (float)var3 - var6);
         this.minY = Math.min(this.minY, (float)var4 - var6);
         this.minZ = Math.min(this.minZ, (float)var5 - var6);
         this.maxX = Math.max(this.maxX, (float)var3 + var6);
         this.maxY = Math.max(this.maxY, (float)var4 + var6);
         this.maxZ = Math.max(this.maxZ, (float)var5 + var6);
      }

      public int hashCode() {
         return this.id;
      }

      public boolean equals(Object var1) {
         return this.id == ((ElementCollectionCalculationThreadExecution.CollectionSetContainer)var1).id;
      }

      public void combine(ElementCollectionCalculationThreadExecution.CollectionSetContainer var1) {
         this.s.addAll(var1.s);
         this.minX = Math.min(this.minX, var1.minX);
         this.minY = Math.min(this.minY, var1.minY);
         this.minZ = Math.min(this.minZ, var1.minZ);
         this.maxX = Math.max(this.maxX, var1.maxX);
         this.maxY = Math.max(this.maxY, var1.maxY);
         this.maxZ = Math.max(this.maxZ, var1.maxZ);

         for(int var2 = 0; var2 < this.touching.length; ++var2) {
            int[] var10000 = this.touching;
            var10000[var2] += var1.touching[var2];
         }

      }

      public void addTouching(int var1) {
         int var10002 = this.touching[var1]++;
      }
   }
}
