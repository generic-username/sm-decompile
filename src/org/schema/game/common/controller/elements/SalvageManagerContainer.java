package org.schema.game.common.controller.elements;

public interface SalvageManagerContainer {
   ManagerModuleCollection getSalvage();
}
