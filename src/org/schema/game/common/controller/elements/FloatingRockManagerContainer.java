package org.schema.game.common.controller.elements;

import org.schema.game.common.controller.FloatingRockManaged;
import org.schema.game.common.data.player.inventory.InventoryHolder;
import org.schema.schine.network.StateInterface;

public class FloatingRockManagerContainer extends StationaryManagerContainer implements DoorContainerInterface, LiftContainerInterface, ShieldContainerInterface, InventoryHolder {
   public FloatingRockManagerContainer(StateInterface var1, FloatingRockManaged var2) {
      super(var1, var2);
   }
}
