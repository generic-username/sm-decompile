package org.schema.game.common.controller.elements;

import org.schema.game.common.controller.Planet;
import org.schema.game.common.data.player.inventory.InventoryHolder;
import org.schema.schine.network.StateInterface;

public class PlanetManagerContainer extends StationaryManagerContainer implements DoorContainerInterface, LiftContainerInterface, ShieldContainerInterface, InventoryHolder {
   public PlanetManagerContainer(StateInterface var1, Planet var2) {
      super(var1, var2);
   }
}
