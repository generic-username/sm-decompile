package org.schema.game.common.controller.elements;

import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.schema.common.util.StringTools;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.newgui.PolygonStatsInterface;
import org.schema.schine.network.SerialializationInterface;

public class EntityIndexScore implements PolygonStatsInterface, SerialializationInterface {
   public static final Short2ObjectOpenHashMap serializationMap;
   public final List children = new ArrayList();
   public static final short version = 1;
   public double weaponDamageIndex;
   public double weaponRangeIndex;
   public double weaponhitPropabilityIndex;
   public double weaponSpecialIndex;
   public double weaponPowerConsumptionPerSecondIndex;
   public double hitpoints;
   public double armor;
   public double thrust;
   public double shields;
   public double jumpDriveIndex;
   public double mass;
   public double powerRecharge;
   public double maxPower;
   public double support;
   public double mining;
   public boolean docked;
   public boolean turret;
   public double offensiveIndex;
   public double defensiveIndex;
   public double powerIndex;
   public double mobilityIndex;
   public double dangerIndex;
   public double survivabilityIndex;
   public double supportIndex;
   public double miningIndex;

   public String toString() {
      return "EntityIndexScore [version=1, weaponDamageIndex=" + this.weaponDamageIndex + ", weaponRangeIndex=" + this.weaponRangeIndex + ", weaponhitPropabilityIndex=" + this.weaponhitPropabilityIndex + ", weaponSpecialIndex=" + this.weaponSpecialIndex + ", weaponPowerConsumptionPerSecondIndex=" + this.weaponPowerConsumptionPerSecondIndex + ", hitpoints=" + this.hitpoints + ", armor=" + this.armor + ", thrust=" + this.thrust + ", shields=" + this.shields + ", jumpDriveIndex=" + this.jumpDriveIndex + ", mass=" + this.mass + ", powerRecharge=" + this.powerRecharge + ", maxPower=" + this.maxPower + ", support=" + this.support + ", docked=" + this.docked + ", turret=" + this.turret + ", children=" + this.children + ", offensiveIndex=" + this.offensiveIndex + ", defensiveIndex=" + this.defensiveIndex + ", powerIndex=" + this.powerIndex + ", mobilityIndex=" + this.mobilityIndex + ", dangerIndex=" + this.dangerIndex + ", survivabilityIndex=" + this.survivabilityIndex + ", supportIndex=" + this.supportIndex + "]";
   }

   public String toShortString() {
      return "EntityIndexScore [version=1,\n offensiveIndex=" + (long)this.offensiveIndex + ",\n defensiveIndex=" + (long)this.defensiveIndex + ",\n powerIndex=" + (long)this.powerIndex + ",\n mobilityIndex=" + (long)this.mobilityIndex + ",\n dangerIndex=" + (long)this.dangerIndex + ",\n survivabilityIndex=" + (long)this.survivabilityIndex + ",\n miningIndex=" + (long)this.miningIndex + ",\n supportIndex=" + (long)this.supportIndex + "]";
   }

   public void addScores(EntityIndexScore var1) {
      Field[] var2;
      int var3 = (var2 = this.getClass().getFields()).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         Field var5;
         (var5 = var2[var4]).setAccessible(true);
         if (var5.getType() == Double.TYPE) {
            try {
               var5.setDouble(this, var5.getDouble(this) + var5.getDouble(var1));
            } catch (IllegalArgumentException var6) {
               var6.printStackTrace();
            } catch (IllegalAccessException var7) {
               var7.printStackTrace();
            }
         }
      }

   }

   public int getDataPointsNum() {
      return 5;
   }

   public double getPercent(int var1) {
      double var2 = 0.0D;

      for(int var4 = 0; var4 < this.getDataPointsNum(); ++var4) {
         var2 = Math.max(this.getValue(var4), var2);
      }

      return var2 > 0.0D ? this.getValue(var1) / var2 : 0.0D;
   }

   public double getValue(int var1) {
      switch(var1) {
      case 0:
         return this.offensiveIndex;
      case 1:
         return this.defensiveIndex;
      case 2:
         return this.mobilityIndex;
      case 3:
         return this.supportIndex;
      case 4:
         return this.miningIndex;
      default:
         return 0.0D;
      }
   }

   public String getValueName(int var1) {
      switch(var1) {
      case 0:
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_ENTITYINDEXSCORE_0;
      case 1:
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_ENTITYINDEXSCORE_1;
      case 2:
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_ENTITYINDEXSCORE_2;
      case 3:
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_ENTITYINDEXSCORE_3;
      case 4:
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_ENTITYINDEXSCORE_4;
      default:
         return "unknown";
      }
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeShort(1);
      ((EntityIndexScoreSerializationInterface)serializationMap.get((short)1)).serialize(this, var1, var2);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      short var4 = var1.readShort();
      ((EntityIndexScoreSerializationInterface)serializationMap.get(var4)).deserialize(this, var1, var2, var3);
   }

   public void addStrings(Collection var1) {
      for(int var2 = 0; var2 < this.getDataPointsNum(); ++var2) {
         var1.add(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_ENTITYINDEXSCORE_5, this.getValueName(var2), StringTools.formatSeperated((long)this.getValue(var2))));
      }

   }

   static {
      (serializationMap = new Short2ObjectOpenHashMap()).put((short)0, new EntityIndexScoreSerializationInterface() {
         public final void serialize(EntityIndexScore var1, DataOutput var2, boolean var3) throws IOException {
            var2.writeDouble(var1.offensiveIndex);
            var2.writeDouble(var1.defensiveIndex);
            var2.writeDouble(var1.powerIndex);
            var2.writeDouble(var1.mobilityIndex);
            var2.writeDouble(var1.dangerIndex);
            var2.writeDouble(var1.survivabilityIndex);
            var2.writeDouble(var1.offensiveIndex);
            var2.writeDouble(var1.supportIndex);
         }

         public final void deserialize(EntityIndexScore var1, DataInput var2, int var3, boolean var4) throws IOException {
            var1.offensiveIndex = var2.readDouble();
            var1.defensiveIndex = var2.readDouble();
            var1.powerIndex = var2.readDouble();
            var1.mobilityIndex = var2.readDouble();
            var1.dangerIndex = var2.readDouble();
            var1.survivabilityIndex = var2.readDouble();
            var1.offensiveIndex = var2.readDouble();
            var1.supportIndex = var2.readDouble();
         }
      });
      serializationMap.put((short)1, new EntityIndexScoreSerializationInterface() {
         public final void serialize(EntityIndexScore var1, DataOutput var2, boolean var3) throws IOException {
            var2.writeDouble(var1.offensiveIndex);
            var2.writeDouble(var1.defensiveIndex);
            var2.writeDouble(var1.powerIndex);
            var2.writeDouble(var1.mobilityIndex);
            var2.writeDouble(var1.dangerIndex);
            var2.writeDouble(var1.survivabilityIndex);
            var2.writeDouble(var1.offensiveIndex);
            var2.writeDouble(var1.supportIndex);
            var2.writeDouble(var1.miningIndex);
         }

         public final void deserialize(EntityIndexScore var1, DataInput var2, int var3, boolean var4) throws IOException {
            var1.offensiveIndex = var2.readDouble();
            var1.defensiveIndex = var2.readDouble();
            var1.powerIndex = var2.readDouble();
            var1.mobilityIndex = var2.readDouble();
            var1.dangerIndex = var2.readDouble();
            var1.survivabilityIndex = var2.readDouble();
            var1.offensiveIndex = var2.readDouble();
            var1.supportIndex = var2.readDouble();
            var1.miningIndex = var2.readDouble();
         }
      });
   }
}
