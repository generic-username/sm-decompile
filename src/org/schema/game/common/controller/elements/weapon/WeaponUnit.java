package org.schema.game.common.controller.elements.weapon;

import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.combination.modifier.tagMod.DamageUnitInterface;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.CustomOutputUnit;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ShootContainer;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.graphicsengine.core.Timer;

public class WeaponUnit extends CustomOutputUnit implements DamageUnitInterface {
   private float projectileWidth = 1.0F;

   private float getBaseConsume() {
      return (float)(this.size() + this.getEffectBonus()) * WeaponElementManager.BASE_POWER_CONSUMPTION;
   }

   public int getEffectBonus() {
      return Math.min(this.size(), (int)((double)this.size() / (double)((WeaponCollectionManager)this.elementCollectionManager).getTotalSize() * (double)((WeaponCollectionManager)this.elementCollectionManager).getEffectTotal()));
   }

   public float getDamageWithoutEffect() {
      return (float)this.size() * this.getBaseDamage();
   }

   protected DamageDealerType getDamageType() {
      return DamageDealerType.PROJECTILE;
   }

   public float getDamage() {
      float var1 = (float)(this.size() + this.getEffectBonus()) * this.getBaseDamage();
      return this.getConfigManager().apply(StatusEffectType.WEAPON_DAMAGE, this.getDamageType(), var1);
   }

   public float getBaseDamage() {
      return WeaponElementManager.BASE_DAMAGE.get(this.getSegmentController().isUsingPowerReactors()) * ((WeaponCollectionManager)this.elementCollectionManager).currentDamageMult;
   }

   public float getExtraConsume() {
      return 1.0F + (float)Math.max(0, ((WeaponCollectionManager)this.elementCollectionManager).getElementCollections().size() - 1) * WeaponElementManager.ADDITIONAL_POWER_CONSUMPTION_PER_UNIT_MULT;
   }

   public float getSpeed() {
      return ((GameStateInterface)this.getSegmentController().getState()).getGameState().getMaxGalaxySpeed() * WeaponElementManager.BASE_SPEED;
   }

   public String toString() {
      return "WeaponUnit " + super.toString();
   }

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((WeaponElementManager)((WeaponCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (WeaponCollectionManager)this.elementCollectionManager, var2, var3);
   }

   public float getBasePowerConsumption() {
      return WeaponElementManager.BASE_POWER_CONSUMPTION;
   }

   public float getPowerConsumption() {
      return this.getExtraConsume() * this.getBaseConsume();
   }

   public float getPowerConsumptionWithoutEffect() {
      return (float)this.size() * WeaponElementManager.BASE_POWER_CONSUMPTION;
   }

   public float getImpactForce() {
      return WeaponElementManager.IMPACT_FORCE;
   }

   public float getRecoil() {
      return WeaponElementManager.RECOIL;
   }

   public int getPenetrationDepth(float var1) {
      return Math.max(1, WeaponElementManager.PROJECTILE_PENETRATION_DEPTH_BASIC + Math.round((float)Math.pow((double)var1, (double)WeaponElementManager.PROJECTILE_PENETRATION_DEPTH_EXP) * WeaponElementManager.PROJECTILE_PENETRATION_DEPTH_EXP_MULT));
   }

   public float getReloadTimeMs() {
      return WeaponElementManager.BASE_RELOAD;
   }

   public float getInitializationTime() {
      return WeaponElementManager.BASE_RELOAD;
   }

   public float getDistanceRaw() {
      return WeaponElementManager.BASE_DISTANCE * ((GameStateInterface)this.getSegmentController().getState()).getGameState().getWeaponRangeReference();
   }

   public float getFiringPower() {
      return this.getDamage();
   }

   public double getPowerConsumedPerSecondResting() {
      return ((WeaponElementManager)((WeaponCollectionManager)this.elementCollectionManager).getElementManager()).calculatePowerConsumptionCombi(this.getPowerConsumedPerSecondRestingPerBlock(), false, this);
   }

   public double getPowerConsumedPerSecondCharging() {
      return ((WeaponElementManager)((WeaponCollectionManager)this.elementCollectionManager).getElementManager()).calculatePowerConsumptionCombi(this.getPowerConsumedPerSecondChargingPerBlock(), true, this);
   }

   public double getPowerConsumedPerSecondRestingPerBlock() {
      double var1 = (double)WeaponElementManager.REACTOR_POWER_CONSUMPTION_RESTING;
      return this.getConfigManager().apply(StatusEffectType.WEAPON_TOP_OFF_RATE, this.getDamageType(), var1);
   }

   public double getPowerConsumedPerSecondChargingPerBlock() {
      double var1 = (double)WeaponElementManager.REACTOR_POWER_CONSUMPTION_CHARGING;
      return this.getConfigManager().apply(StatusEffectType.WEAPON_CHARGE_RATE, this.getDamageType(), var1);
   }

   public void doShot(ControllerStateInterface var1, Timer var2, ShootContainer var3) {
      boolean var4 = var1.canFocusWeapon() && ((WeaponCollectionManager)this.elementCollectionManager).isInFocusMode();
      var1.getShootingDir(this.getSegmentController(), var3, this.getDistanceFull(), this.getSpeed(), ((WeaponCollectionManager)this.elementCollectionManager).getControllerPos(), var4, true);

      assert var3.shootingDirTemp.lengthSquared() > 0.0F : var3.shootingDirTemp;

      if (!this.isAimable()) {
         var3.shootingDirTemp.set(var3.shootingDirStraightTemp);
      }

      var3.shootingDirTemp.normalize();
      float var5 = this.getSpeed() * ((GameStateInterface)this.getSegmentController().getState()).getGameState().isRelativeProjectiles();
      var3.shootingDirTemp.scale(var5);
      ((WeaponElementManager)((WeaponCollectionManager)this.elementCollectionManager).getElementManager()).doShot(this, (WeaponCollectionManager)this.elementCollectionManager, var3, var1.getPlayerState(), var2);
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.CANNONS;
   }

   public void calculateExtraDataAfterCreationThreaded(long var1, LongOpenHashSet var3) {
      super.calculateExtraDataAfterCreationThreaded(var1, var3);
      int[] var12 = new int[4];
      byte[] var2 = new byte[4];
      byte var4 = ((WeaponCollectionManager)this.elementCollectionManager).getControllerElement().getOrientation();
      byte var5 = 0;

      byte var6;
      for(var6 = 0; var5 < 6; ++var5) {
         if (var5 != var4 && var5 != Element.getOpposite(var4)) {
            var2[var6] = var5;
            ++var6;
         }
      }

      int var15;
      int var16;
      for(var15 = 0; var15 < var12.length; ++var15) {
         var6 = var2[var15];
         Vector3i var13 = Element.DIRECTIONSi[var6];
         var16 = 0;

         boolean var17;
         do {
            int var7 = this.getOutput().x + (var16 + 1) * var13.x;
            int var8 = this.getOutput().y + (var16 + 1) * var13.y;
            int var9 = this.getOutput().z + (var16 + 1) * var13.z;
            long var10 = ElementCollection.getIndex(var7, var8, var9);
            if (var17 = var3.contains(var10)) {
               ++var16;
            }
         } while(var17);

         var12[var15] = var16;
      }

      var15 = var12[0] + 1 + var12[1];
      var16 = var12[2] + 1 + var12[3];
      int var14 = Math.max(var15, var16);
      this.projectileWidth = (float)var14 * WeaponElementManager.PROJECTILE_WIDTH_MULT;
   }

   public float getProjectileWidth() {
      return this.projectileWidth;
   }

   public boolean isAimable() {
      return WeaponElementManager.AIMABLE != 0;
   }
}
