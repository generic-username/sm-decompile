package org.schema.game.common.controller.elements.door;

import java.util.Iterator;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ActivateValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public class DoorUnit extends ElementCollection {
   public void activate(boolean var1) {
      long var5;
      for(Iterator var2 = this.getNeighboringCollection().iterator(); var2.hasNext(); ((SendableSegmentController)this.getSegmentController()).getBlockActivationBuffer().enqueue(var5)) {
         long var3 = (Long)var2.next();
         if (var1) {
            var5 = ElementCollection.getActivation(var3, false, false);

            assert ElementCollection.getType(var5) < 10;
         } else {
            var5 = ElementCollection.getDeactivation(var3, false, false);

            assert ElementCollection.getType(var5) < 10;
         }
      }

   }

   public void cleanUp() {
      super.cleanUp();
   }

   public boolean hasMesh() {
      return false;
   }

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ControllerManagerGUI.create(var1, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOOR_DOORUNIT_0, this, new ActivateValueEntry(new Object() {
         public String toString() {
            return DoorUnit.this.isActive() ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOOR_DOORUNIT_1 : Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOOR_DOORUNIT_2;
         }
      }) {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               DoorUnit.this.activateClient(DoorUnit.this.isActive());
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
   }

   public boolean isActive() {
      if (this.getElementCollectionId() != null) {
         this.getElementCollectionId().refresh();
         return this.getElementCollectionId().isActive();
      } else {
         return false;
      }
   }

   public void refreshDoorCapabilities() {
   }

   public void activateClient(boolean var1) {
      this.getSegmentController().sendBlockActivation(ElementCollection.getEncodeActivation(this.getElementCollectionId(), true, !var1, false));
   }
}
