package org.schema.game.common.controller.elements.door;

import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import java.util.Iterator;
import org.schema.game.client.view.gui.structurecontrol.ActivateValueEntry;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.BlockActivationListenerInterface;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public class DoorCollectionManager extends ElementCollectionManager implements BlockActivationListenerInterface {
   public DoorCollectionManager(SegmentController var1, VoidElementManager var2) {
      super((short)122, var1, var2);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return DoorUnit.class;
   }

   public boolean needsUpdate() {
      return false;
   }

   public DoorUnit getInstance() {
      return new DoorUnit();
   }

   public boolean isUsingIntegrity() {
      return false;
   }

   protected void onChangedCollection() {
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[]{new ActivateValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOOR_DOORCOLLECTIONMANAGER_0) {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               Iterator var3 = DoorCollectionManager.this.getElementCollections().iterator();

               while(var3.hasNext()) {
                  ((DoorUnit)var3.next()).activateClient(false);
               }
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }, new ActivateValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOOR_DOORCOLLECTIONMANAGER_1) {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               Iterator var3 = DoorCollectionManager.this.getElementCollections().iterator();

               while(var3.hasNext()) {
                  ((DoorUnit)var3.next()).activateClient(true);
               }
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }};
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOOR_DOORCOLLECTIONMANAGER_2;
   }

   public int onActivate(SegmentPiece var1, boolean var2, boolean var3) {
      if (this.getSegmentController().isOnServer()) {
         long var4 = var1.getAbsoluteIndex();
         Iterator var6 = this.getElementCollections().iterator();

         while(var6.hasNext()) {
            DoorUnit var7;
            if ((var7 = (DoorUnit)var6.next()).contains(var4)) {
               var7.activate(var3);
               if (var3) {
                  return 1;
               }

               return 0;
            }
         }
      }

      return var3 ? 1 : 0;
   }

   public void updateActivationTypes(ShortOpenHashSet var1) {
      if (this.getSegmentController().isOnServer()) {
         var1.addAll(ElementKeyMap.doorTypes);
      }

   }

   public float getSensorValue(SegmentPiece var1) {
      return var1.isActive() ? 1.0F : 0.0F;
   }

   public boolean isHandlingActivationForType(short var1) {
      return ElementKeyMap.isDoor(var1);
   }
}
