package org.schema.game.common.controller.elements.warpgate;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.effects.RaisingIndication;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.Destination;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.InterControllerCollectionManager;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class WarpgateCollectionManager extends ControlBlockElementCollectionManager implements InterControllerCollectionManager, PowerConsumer {
   private long lastPopup;
   private Destination warpDestination = new Destination();
   private float powered;

   public WarpgateCollectionManager(SegmentPiece var1, SegmentController var2, WarpgateElementManager var3, Long2ObjectOpenHashMap var4, Long2ObjectOpenHashMap var5) {
      super(var1, (short)543, var2, var3);

      assert var1 != null;

      assert var4 != null;

      String var6;
      if ((var6 = (String)var4.remove(var1.getAbsoluteIndex())) != null) {
         this.warpDestination.uid = var6;
         this.warpDestination.local.set((Vector3i)var5.get(var1.getAbsoluteIndex()));
      }

   }

   protected Tag toTagStructurePriv() {
      Tag var1 = new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.isValid() ? 1 : 0)));
      Tag var2 = new Tag(Tag.Type.STRING, (String)null, this.warpDestination.uid);
      Tag var3 = new Tag(Tag.Type.VECTOR3i, (String)null, this.warpDestination.local);
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, var2, var3, FinishTag.INST});
   }

   public boolean isUsingIntegrity() {
      return false;
   }

   public ElementCollectionManager.CollectionShape requiredNeigborsPerBlock() {
      return ElementCollectionManager.CollectionShape.LOOP;
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return WarpgateUnit.class;
   }

   public boolean needsUpdate() {
      return true;
   }

   public WarpgateUnit getInstance() {
      return new WarpgateUnit();
   }

   protected void onChangedCollection() {
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
      }

   }

   public void update(Timer var1) {
      if (this.isValid()) {
         ((WarpgateUnit)this.getElementCollections().get(0)).update(var1);
      } else {
         if (!this.getSegmentController().isOnServer() && ((GameClientState)this.getSegmentController().getState()).getCurrentSectorId() == this.getSegmentController().getSectorId() && System.currentTimeMillis() - this.lastPopup > 5000L) {
            Transform var3;
            (var3 = new Transform()).setIdentity();
            Vector3i var2 = this.getControllerPos();
            var3.origin.set((float)(var2.x - 16), (float)(var2.y - 16), (float)(var2.z - 16));
            this.getSegmentController().getWorldTransform().transform(var3.origin);
            RaisingIndication var4;
            (var4 = new RaisingIndication(var3, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_WARPGATE_WARPGATECOLLECTIONMANAGER_0, 1.0F, 0.1F, 0.1F, 1.0F)).speed = 0.1F;
            var4.lifetime = 3.0F;
            HudIndicatorOverlay.toDrawTexts.add(var4);
            this.lastPopup = System.currentTimeMillis();
         }

      }
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[0];
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_WARPGATE_WARPGATECOLLECTIONMANAGER_1;
   }

   public boolean isValid() {
      return this.getElementCollections().size() == 1 && ((WarpgateUnit)this.getElementCollections().get(0)).isValid();
   }

   public String getWarpDestinationUID() {
      return this.warpDestination.uid;
   }

   public void setDestination(String var1, Vector3i var2) {
      this.warpDestination.uid = var1;
      this.warpDestination.local.set(var2);
   }

   public int getWarpType() {
      return 0;
   }

   public int getWarpPermission() {
      return 0;
   }

   public Vector3i getLocalDestination() {
      return this.warpDestination.local;
   }

   private double getReactorPowerUsage() {
      double var1 = Math.pow((double)WarpgateElementManager.REACTOR_POWER_CONST_NEEDED_PER_BLOCK * (double)this.getTotalSize(), (double)WarpgateElementManager.REACTOR_POWER_POW);
      return this.getConfigManager().apply(StatusEffectType.WARP_POWER_EFFICIENCY, var1);
   }

   public double getPowerConsumedPerSecondResting() {
      return this.getReactorPowerUsage();
   }

   public double getPowerConsumedPerSecondCharging() {
      return this.getReactorPowerUsage();
   }

   public boolean isPowerCharging(long var1) {
      return false;
   }

   public void setPowered(float var1) {
      this.powered = var1;
   }

   public float getPowered() {
      return this.powered;
   }

   public void reloadFromReactor(double var1, Timer var3, float var4, boolean var5, float var6) {
   }

   public int getMaxDistance() {
      return (int)this.getConfigManager().apply(StatusEffectType.WARP_DISTANCE, WarpgateElementManager.DISTANCE_IN_SECTORS);
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.WARP_GATE;
   }

   public boolean isPowerConsumerActive() {
      return true;
   }

   public void dischargeFully() {
   }
}
