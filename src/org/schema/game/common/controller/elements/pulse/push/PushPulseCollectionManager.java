package org.schema.game.common.controller.elements.pulse.push;

import javax.vecmath.Vector4f;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.pulse.PulseCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.input.InputType;

public class PushPulseCollectionManager extends PulseCollectionManager {
   private static Vector4f defaultColor = new Vector4f(0.8F, 0.8F, 1.0F, 1.0F);

   public PushPulseCollectionManager(SegmentPiece var1, SegmentController var2, PushPulseElementManager var3) {
      super(var1, (short)345, var2, var3);
   }

   protected Class getType() {
      return PushPulseUnit.class;
   }

   public PushPulseUnit getInstance() {
      return new PushPulseUnit();
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_PULSE_PUSH_PUSHPULSECOLLECTIONMANAGER_0;
   }

   public Vector4f getDefaultColor() {
      return defaultColor;
   }

   public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
      var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_PULSE_PUSH_PUSHPULSECOLLECTIONMANAGER_1, var3, ContextFilter.IMPORTANT);
   }
}
