package org.schema.game.common.controller.elements.pulse.push;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.pulse.PulseElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;

public class PushPulseElementManager extends PulseElementManager {
   public static boolean debug = false;
   @ConfigurationElement(
      name = "Force"
   )
   public static float BASE_FORCE = 100.0F;
   @ConfigurationElement(
      name = "ReloadMs"
   )
   public static float BASE_RELOAD = 10000.0F;
   @ConfigurationElement(
      name = "Radius",
      description = "blast radius"
   )
   public static float BASE_RADIUS = 16.0F;
   @ConfigurationElement(
      name = "PowerConsumption"
   )
   public static float BASE_POWER_CONSUMPTION = 300.0F;
   @ConfigurationElement(
      name = "ReactorPowerConsumptionResting"
   )
   public static float REACTOR_POWER_CONSUMPTION_RESTING = 10.0F;
   @ConfigurationElement(
      name = "ReactorPowerConsumptionCharging"
   )
   public static float REACTOR_POWER_CONSUMPTION_CHARGING = 10.0F;

   public PushPulseElementManager(SegmentController var1) {
      super((short)344, (short)345, var1);
   }

   public void updateActivationTypes(ShortOpenHashSet var1) {
      var1.add((short)345);
   }

   public void addSinglePulse(Transform var1, Vector3f var2, float var3, float var4, long var5, Vector4f var7) {
      this.getPulseController().addPushPulse(var1, var2, this.getSegmentController(), var3, var4, var5, var7);
   }

   public String getManagerName() {
      return "Push Pulse System Collective";
   }

   public ControllerManagerGUI getGUIUnitValues(PushPulseUnit var1, PushPulseCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      if (var4 != null) {
         var2.setEffectTotal(var4.getTotalSize());
      } else {
         var2.setEffectTotal(0);
      }

      float var6 = var1.getPulsePower();
      float var7 = var1.getRadius();
      float var8 = var1.getReloadTimeMs();
      float var5 = var1.getPowerConsumption();
      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_PULSE_PUSH_PUSHPULSEELEMENTMANAGER_0, var1, new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_PULSE_PUSH_PUSHPULSEELEMENTMANAGER_1, StringTools.formatPointZero(var6)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_PULSE_PUSH_PUSHPULSEELEMENTMANAGER_2, StringTools.formatPointZero(var7)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_PULSE_PUSH_PUSHPULSEELEMENTMANAGER_15, StringTools.formatPointZero(var8)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_PULSE_PUSH_PUSHPULSEELEMENTMANAGER_4, StringTools.formatPointZero(var5)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_PULSE_PUSH_PUSHPULSEELEMENTMANAGER_5, StringTools.formatPointZero(0.0F)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_PULSE_PUSH_PUSHPULSEELEMENTMANAGER_6, StringTools.formatPointZero(0.0F)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_PULSE_PUSH_PUSHPULSEELEMENTMANAGER_7, StringTools.formatPointZero(0.0F)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_PULSE_PUSH_PUSHPULSEELEMENTMANAGER_8, StringTools.formatPointZero(0.0F)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_PULSE_PUSH_PUSHPULSEELEMENTMANAGER_9, StringTools.formatPointZero(0.0F)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_PULSE_PUSH_PUSHPULSEELEMENTMANAGER_10, StringTools.formatPointZero(0.0F)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_PULSE_PUSH_PUSHPULSEELEMENTMANAGER_11, StringTools.formatPointZero(0.0F)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_PULSE_PUSH_PUSHPULSEELEMENTMANAGER_12, StringTools.formatPointZero(0.0F)));
   }

   protected String getTag() {
      return "pushpulse";
   }

   public PushPulseCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new PushPulseCollectionManager(var1, this.getSegmentController(), this);
   }

   protected void playSound(PushPulseUnit var1, Transform var2) {
      ((GameClientController)this.getState().getController()).queueTransformableAudio("0022_spaceship user - laser gun single fire small", var2, 0.99F);
   }
}
