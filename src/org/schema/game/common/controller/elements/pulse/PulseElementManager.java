package org.schema.game.common.controller.elements.pulse;

import com.bulletphysics.linearmath.Transform;
import java.io.IOException;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.BlockActivationListenerInterface;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.IntegrityBasedInterface;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.controller.elements.ShootingRespose;
import org.schema.game.common.controller.elements.UsableControllableFireingElementManager;
import org.schema.game.common.controller.elements.combination.CombinationAddOn;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ShootContainer;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public abstract class PulseElementManager extends UsableControllableFireingElementManager implements BlockActivationListenerInterface, IntegrityBasedInterface {
   public static boolean debug = false;
   private final ShootContainer shootContainer = new ShootContainer();

   public PulseElementManager(short var1, short var2, SegmentController var3) {
      super(var1, var2, var3);
   }

   public void doShot(PulseUnit var1, PulseCollectionManager var2, ShootContainer var3, PlayerState var4, Timer var5) {
      var4 = null;
      var2.setEffectTotal(0);
      if (var2.getEffectConnectedElement() != Long.MIN_VALUE) {
         short var8 = (short)ElementCollection.getType(var2.getEffectConnectedElement());
         ManagerModuleCollection var9 = this.getManagerContainer().getModulesControllerMap().get(var8);
         ControlBlockElementCollectionManager var6;
         if ((var6 = CombinationAddOn.getEffect(var2.getEffectConnectedElement(), var9, this.getSegmentController())) != null) {
            var2.setEffectTotal(var6.getTotalSize());
         }
      }

      if (var1.canUse(System.currentTimeMillis(), false)) {
         if (!this.isUsingPowerReactors() && !this.consumePower(var1.getPowerConsumption())) {
            this.handleResponse(ShootingRespose.NO_POWER, var1, var3.weapontOutputWorldPos);
         } else {
            Transform var10;
            (var10 = new Transform()).setIdentity();
            var10.origin.set(var3.weapontOutputWorldPos);
            var1.setStandardShotReloading();
            Vector3f var11;
            (var11 = new Vector3f(var3.shootingDirTemp)).normalize();
            long var12 = var2.getUsableId();
            this.addSinglePulse(var1, var11, var10, var12, var2.getColor());
            this.handleResponse(ShootingRespose.FIRED, var1, var3.weapontOutputWorldPos);
         }
      } else {
         this.handleResponse(ShootingRespose.RELOADING, var1, var3.weapontOutputWorldPos);
      }
   }

   public void addSinglePulse(PulseUnit var1, Vector3f var2, Transform var3, long var4, Vector4f var6) {
      this.addSinglePulse(var3, var2, var1.getPulsePower(), var1.getRadius(), var4, var6);
   }

   public abstract void addSinglePulse(Transform var1, Vector3f var2, float var3, float var4, long var5, Vector4f var7);

   public int onActivate(SegmentPiece var1, boolean var2, boolean var3) {
      long var4 = var1.getAbsoluteIndex();

      for(int var8 = 0; var8 < this.getCollectionManagers().size(); ++var8) {
         Iterator var6 = ((PulseCollectionManager)this.getCollectionManagers().get(var8)).getElementCollections().iterator();

         while(var6.hasNext()) {
            PulseUnit var7;
            if ((var7 = (PulseUnit)var6.next()).contains(var4)) {
               var7.setMainPiece(var1, var3);
               if (var3) {
                  return 1;
               }

               return 0;
            }
         }
      }

      if (var3) {
         return 1;
      } else {
         return 0;
      }
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_PULSE_PULSEELEMENTMANAGER_3;
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
      if (!var1.isFlightControllerActive()) {
         if (debug) {
            System.err.println("NOT ACTIVE");
         }

      } else if (this.getCollectionManagers().isEmpty()) {
         if (debug) {
            System.err.println("NO WEAPONS");
         }

      } else {
         try {
            if (!this.convertDeligateControls(var1, this.shootContainer.controlledFromOrig, this.shootContainer.controlledFrom)) {
               if (debug) {
                  System.err.println("NO SLOT");
               }

               return;
            }
         } catch (IOException var8) {
            var8.printStackTrace();
            return;
         }

         System.currentTimeMillis();
         this.getPowerManager().sendNoPowerHitEffectIfNeeded();
         if (debug) {
            System.err.println("FIREING CONTROLLERS: " + this.getState() + ", " + this.getCollectionManagers().size() + " FROM: " + this.shootContainer.controlledFrom);
         }

         for(int var3 = 0; var3 < this.getCollectionManagers().size(); ++var3) {
            PulseCollectionManager var4 = (PulseCollectionManager)this.getCollectionManagers().get(var3);
            if (var1.isSelected(var4.getControllerElement(), this.shootContainer.controlledFrom)) {
               boolean var5 = this.shootContainer.controlledFromOrig.equals(this.shootContainer.controlledFrom) | this.getControlElementMap().isControlling(this.shootContainer.controlledFromOrig, var4.getControllerPos(), this.controllerId);
               if (debug) {
                  System.err.println("Controlling " + var5 + " " + this.getState());
               }

               if (var5 && var4.allowedOnServerLimit()) {
                  if (this.shootContainer.controlledFromOrig.equals(Ship.core)) {
                     var1.getControlledFrom(this.shootContainer.controlledFromOrig);
                  }

                  if (debug) {
                     System.err.println("Controlling " + var5 + " " + this.getState() + ": " + var4.getElementCollections().size());
                  }

                  for(int var9 = 0; var9 < var4.getElementCollections().size(); ++var9) {
                     PulseUnit var6;
                     Vector3i var7 = (var6 = (PulseUnit)var4.getElementCollections().get(var9)).getOutput();
                     this.shootContainer.weapontOutputWorldPos.set((float)(var7.x - 16), (float)(var7.y - 16), (float)(var7.z - 16));
                     if (debug) {
                        System.err.println("WEAPON OUTPUT ON " + this.getState() + " -> " + var7);
                     }

                     if (this.getSegmentController().isOnServer()) {
                        this.getSegmentController().getWorldTransform().transform(this.shootContainer.weapontOutputWorldPos);
                     } else {
                        this.getSegmentController().getWorldTransformOnClient().transform(this.shootContainer.weapontOutputWorldPos);
                     }

                     this.shootContainer.centeralizedControlledFromPos.set(this.shootContainer.controlledFromOrig);
                     this.shootContainer.centeralizedControlledFromPos.sub(Ship.core);
                     this.shootContainer.camPos.set(this.getSegmentController().getAbsoluteElementWorldPosition(this.shootContainer.centeralizedControlledFromPos, this.shootContainer.tmpCampPos));
                     var1.getShootingDir(this.getSegmentController(), this.shootContainer, var6.getDistanceFull(), 1.0F, var4.getControllerPos(), false, false);
                     this.shootContainer.shootingDirTemp.normalize();
                     this.doShot(var6, var4, this.shootContainer, var1.getPlayerState(), var2);
                  }

                  if (var4.getElementCollections().isEmpty() && this.clientIsOwnShip()) {
                     ((GameClientState)this.getState()).getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_PULSE_PULSEELEMENTMANAGER_0, 0.0F);
                  }
               }
            }
         }

         if (this.getCollectionManagers().isEmpty() && this.clientIsOwnShip()) {
            ((GameClientState)this.getState()).getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_PULSE_PULSEELEMENTMANAGER_2, 0.0F);
         }

      }
   }
}
