package org.schema.game.common.controller.elements.pulse.push;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.controller.elements.pulse.PulseUnit;

public class PushPulseUnit extends PulseUnit {
   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((PushPulseElementManager)((PushPulseCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (PushPulseCollectionManager)this.elementCollectionManager, var2, var3);
   }

   public float getRadius() {
      return PushPulseElementManager.BASE_RADIUS;
   }

   public String toString() {
      return "PushPulseUnit [significator=" + this.significator + "]";
   }

   public float getPulsePower() {
      return PushPulseElementManager.BASE_FORCE;
   }

   public float getDamageWithoutEffect() {
      return this.getPulsePower();
   }

   public float getBasePulsePower() {
      return PushPulseElementManager.BASE_FORCE;
   }

   public float getBasePowerConsumption() {
      return PushPulseElementManager.BASE_POWER_CONSUMPTION;
   }

   public float getPowerConsumption() {
      return (float)this.size() * PushPulseElementManager.BASE_POWER_CONSUMPTION;
   }

   public float getPowerConsumptionWithoutEffect() {
      return (float)this.size() * PushPulseElementManager.BASE_POWER_CONSUMPTION;
   }

   public float getReloadTimeMs() {
      return PushPulseElementManager.BASE_RELOAD;
   }

   public float getInitializationTime() {
      return PushPulseElementManager.BASE_RELOAD;
   }

   public float getFiringPower() {
      return 0.0F;
   }

   public double getPowerConsumedPerSecondResting() {
      return (double)this.size() * (double)PushPulseElementManager.REACTOR_POWER_CONSUMPTION_RESTING;
   }

   public double getPowerConsumedPerSecondCharging() {
      return (double)this.size() * (double)PushPulseElementManager.REACTOR_POWER_CONSUMPTION_CHARGING;
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.PULSE;
   }

   public float getDamage() {
      return 0.0F;
   }
}
