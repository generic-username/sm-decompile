package org.schema.game.common.controller.elements;

public interface ElementChangeListenerInterface {
   void onAddedAnyElement();

   void onRemovedAnyElement();
}
