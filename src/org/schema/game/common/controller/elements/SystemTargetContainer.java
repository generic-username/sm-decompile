package org.schema.game.common.controller.elements;

import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.floats.FloatList;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.List;
import java.util.Random;
import org.schema.game.common.data.element.ElementCollection;

public class SystemTargetContainer {
   private final List fullList = new ObjectArrayList();
   private final FloatList fullPrioList = new FloatArrayList();
   private float fullPrio;

   public void initialize(List var1) {
      this.fullPrio = 0.0F;
      this.fullList.clear();
      this.fullPrioList.clear();

      int var2;
      for(var2 = 0; var2 < var1.size(); ++var2) {
         TargetableSystemInterface var3;
         if ((var3 = (TargetableSystemInterface)var1.get(var2)).hasAnyBlock()) {
            this.fullList.add(var3);
            this.fullPrioList.add((float)var3.getPriority());
            this.fullPrio += (float)var3.getPriority();
         }
      }

      for(var2 = 0; var2 < this.fullPrioList.size(); ++var2) {
         this.fullPrioList.set(var2, (Float)this.fullPrioList.get(var2) / this.fullPrio);
      }

   }

   public TargetableSystemInterface getRandom(Random var1) {
      if (this.fullList.isEmpty()) {
         return null;
      } else {
         float var5 = var1.nextFloat();
         TargetableSystemInterface var2 = (TargetableSystemInterface)this.fullList.get(0);
         float var3 = this.fullPrioList.getFloat(0);

         for(int var4 = 1; var4 < this.fullList.size(); ++var4) {
            var3 += this.fullPrioList.getFloat(var4);
            if (var5 > var3 - this.fullPrioList.getFloat(var4 - 1) && var5 <= var3) {
               var2 = (TargetableSystemInterface)this.fullList.get(var4);
               break;
            }
         }

         return var2;
      }
   }

   public ElementCollection getRandomCollection(Random var1) {
      TargetableSystemInterface var2;
      return (var2 = this.getRandom(var1)) != null ? var2.getRandomCollection(var1) : null;
   }

   public boolean isEmpty() {
      return this.fullList.isEmpty();
   }
}
