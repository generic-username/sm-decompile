package org.schema.game.common.controller.elements;

import org.schema.game.common.data.element.ElementKeyMap;

public class ModuleCollectionMap {
   private final ManagerModuleCollection[] values;
   ManagerModuleCollection all;
   ManagerModuleCollection signal;
   ManagerModuleCollection railTrack;
   ManagerModuleCollection railInv;

   public ModuleCollectionMap() {
      this.values = new ManagerModuleCollection[ElementKeyMap.highestType + 1];
   }

   public void put(short var1, ManagerModuleCollection var2) {
      if (var1 == 32767) {
         this.all = var2;
      } else if (var1 == 30000) {
         this.signal = var2;
      } else if (var1 == 29999) {
         this.railTrack = var2;
      } else if (var1 == 29998) {
         this.railInv = var2;
      } else {
         this.values[var1] = var2;
      }
   }

   public ManagerModuleCollection get(short var1) {
      if (var1 == 32767) {
         return this.all;
      } else if (var1 == 30000) {
         return this.signal;
      } else if (var1 == 29999) {
         return this.railTrack;
      } else {
         return var1 == 29998 ? this.railInv : this.values[var1];
      }
   }

   public boolean containsKey(short var1) {
      if (var1 == 32767) {
         return this.all != null;
      } else if (var1 == 30000) {
         return this.signal != null;
      } else if (var1 == 29999) {
         return this.railTrack != null;
      } else if (var1 == 29998) {
         return this.railInv != null;
      } else {
         return this.values[var1] != null;
      }
   }
}
