package org.schema.game.common.controller.elements.explosive;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.world.Universe;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class ExplosiveCollectionManager extends ElementCollectionManager {
   private long lastSpontaniousCheck;

   public ExplosiveCollectionManager(SegmentController var1, ExplosiveElementManager var2) {
      super((short)14, var1, var2);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return ExplosiveUnit.class;
   }

   public boolean needsUpdate() {
      return true;
   }

   public void update(Timer var1) {
      super.update(var1);
      if (this.getSegmentController().isOnServer() && this.getElementCollections().size() > 0 && this.getConfigManager().apply(StatusEffectType.WARHEAD_CHANCE_FOR_SPONTANIOUS_EXPLODE, 0.0F) > 0.0F) {
         long var2 = (long)(ExplosiveElementManager.SPONTANIOUS_EXPLODE_CHECK_FREQUENCY_SEC * 1000.0F);
         if (var1.currentTime - this.lastSpontaniousCheck > var2) {
            Universe.getRandom().nextFloat();
            long var4 = this.rawCollection.iterator().nextLong();
            this.getSegmentController().sendControllingPlayersServerMessage(new Object[]{39}, 3);
            ((ExplosiveElementManager)this.getElementManager()).addExplosion(ElementCollection.getPosFromIndex(var4, new Vector3i()));
            this.lastSpontaniousCheck = var1.currentTime;
         }
      }

   }

   public ExplosiveUnit getInstance() {
      return new ExplosiveUnit();
   }

   protected void onChangedCollection() {
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[]{new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_EXPLOSIVE_EXPLOSIVECOLLECTIONMANAGER_0, ((ExplosiveElementManager)this.getElementManager()).getDamage())};
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_EXPLOSIVE_EXPLOSIVECOLLECTIONMANAGER_1;
   }
}
