package org.schema.game.common.controller.elements.explosive;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.damage.HitType;
import org.schema.game.common.controller.elements.BlockKillInterface;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerUpdatableInterface;
import org.schema.game.common.controller.elements.UsableControllableSingleElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.explosion.AfterExplosionCallback;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;

public class ExplosiveElementManager extends UsableControllableSingleElementManager implements BlockKillInterface, ManagerUpdatableInterface {
   @ConfigurationElement(
      name = "SpontaniousExplodeCheckFrequency"
   )
   public static float SPONTANIOUS_EXPLODE_CHECK_FREQUENCY_SEC = 10.0F;
   @ConfigurationElement(
      name = "BlockDamage"
   )
   public static float INITIAL_DAMAGE = 10000.0F;
   @ConfigurationElement(
      name = "PlayerDamage"
   )
   public static float PLAYER_DAMAGE = 200.0F;
   @ConfigurationElement(
      name = "Radius"
   )
   public static float INITIAL_RADIUS = 8.0F;
   private final ObjectArrayFIFOQueue explosions = new ObjectArrayFIFOQueue();
   private LongOpenHashSet explodingBlocks = new LongOpenHashSet();
   private final Vector3i paramTmp = new Vector3i();

   public ExplosiveElementManager(SegmentController var1) {
      super(var1, ExplosiveCollectionManager.class);
   }

   public void addExplosion(Vector3i var1) {
      this.addExplosion(var1, (Vector3f)null);
   }

   public float getDamage() {
      return this.getSegmentController().getConfigManager().apply(StatusEffectType.WARHEAD_DAMAGE, INITIAL_DAMAGE);
   }

   public float getDamagePlayer() {
      return this.getSegmentController().getConfigManager().apply(StatusEffectType.WARHEAD_DAMAGE, PLAYER_DAMAGE);
   }

   public float getRadius() {
      return this.getSegmentController().getConfigManager().apply(StatusEffectType.WARHEAD_RADIUS, INITIAL_RADIUS);
   }

   public void addExplosion(Vector3i var1, Vector3f var2) {
      long var3 = ElementCollection.getIndex(var1);
      if (!this.explodingBlocks.contains(var3)) {
         this.explodingBlocks.add(var3);
         ExplosiveUnit var7 = null;
         Sector var5;
         if (this.getSegmentController().isOnServer() && (var5 = ((GameServerState)this.getSegmentController().getState()).getUniverse().getSector(this.getSegmentController().getSectorId())) != null && var5.isProtected()) {
            System.err.println("[EXPLOSION] not adding explosion because sector is protected!");
         } else {
            Iterator var4 = ((ExplosiveCollectionManager)this.getCollection()).getElementCollections().iterator();

            while(var4.hasNext()) {
               ExplosiveUnit var8;
               if ((var8 = (ExplosiveUnit)var4.next()).getNeighboringCollection().contains(ElementCollection.getIndex(var1))) {
                  var7 = var8;
                  break;
               }
            }

            if (var7 != null) {
               this.explodingBlocks.addAll(var7.getNeighboringCollection());
               synchronized(this.explosions) {
                  if (var2 == null) {
                     var2 = new Vector3f((float)(var1.x - 16), (float)(var1.y - 16), (float)(var1.z - 16));
                  }

                  ExplosiveElementManager.ExplosionType var9 = new ExplosiveElementManager.ExplosionType(new Vector3i(var1), new Vector3f(var2), (EditableSendableSegmentController)this.getSegmentController(), (byte)1);
                  this.explosions.enqueue(var9);
               }
            } else {
               System.err.println("EXPLOSION POINT NOT FOUND " + var1 + " on " + this.getSegmentController());
            }
         }
      }
   }

   public void onControllerChange() {
   }

   public void update(Timer var1) {
      if (!this.explosions.isEmpty()) {
         System.currentTimeMillis();
         synchronized(this.explosions) {
            int var2 = this.explosions.size();

            for(int var3 = 0; var3 < var2 && !this.explosions.isEmpty(); ++var3) {
               final ExplosiveElementManager.ExplosionType var4 = (ExplosiveElementManager.ExplosionType)this.explosions.first();
               this.getSegmentController().getWorldTransform().transform(var4.where);
               Transform var5;
               (var5 = new Transform()).setIdentity();
               var5.origin.set(var4.where);
               new Vector3f();
               new Vector3f();
               if (this.getSegmentController().isOnServer()) {
                  AfterExplosionCallback var6 = new AfterExplosionCallback() {
                     public void onExplosionDone() {
                        ExplosiveElementManager.this.explodingBlocks.remove(ElementCollection.getIndex(var4.id));
                     }
                  };
                  ((EditableSendableSegmentController)this.getSegmentController()).addExplosion(this.getSegmentController(), DamageDealerType.EXPLOSIVE, HitType.INTERNAL, ElementCollection.getIndex(var4.id), var5, this.getRadius(), this.getDamage(), true, var6, 3);
               }

               this.explosions.dequeue();
               --var2;
            }

         }
      }
   }

   public ControllerManagerGUI getGUIUnitValues(ExplosiveUnit var1, ExplosiveCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return null;
   }

   protected String getTag() {
      return "explosive";
   }

   public ExplosiveCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new ExplosiveCollectionManager(this.getSegmentController(), this);
   }

   protected void playSound(ExplosiveUnit var1, Transform var2) {
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
      if (var1.isFlightControllerActive()) {
         if (Ship.core.equals(var1.getParameter(this.paramTmp))) {
            ;
         }
      }
   }

   public void onKilledBlock(long var1, short var3, Damager var4) {
      float var5;
      if (this.getSegmentController().isOnServer() && var3 == 14 && ((var5 = this.getSegmentController().getConfigManager().apply(StatusEffectType.WARHEAD_CHANCE_FOR_EXPLOSION_ON_HIT, 1.0F)) >= 1.0F || Universe.getRandom().nextFloat() <= var5)) {
         this.addExplosion(ElementCollection.getPosFromIndex(var1, new Vector3i()));
      }

   }

   public boolean canUpdate() {
      return true;
   }

   public void onNoUpdate(Timer var1) {
   }

   public class ExplosionType {
      private final Vector3i id;
      private final Vector3f where;
      private long started = System.currentTimeMillis();

      public ExplosionType(Vector3i var2, Vector3f var3, EditableSendableSegmentController var4, byte var5) {
         this.id = var2;
         this.where = var3;
      }

      public boolean equals(Object var1) {
         return ((ExplosiveElementManager.ExplosionType)var1).id.equals(this.id);
      }
   }
}
