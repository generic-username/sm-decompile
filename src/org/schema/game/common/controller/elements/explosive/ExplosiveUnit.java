package org.schema.game.common.controller.elements.explosive;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.common.language.Lng;

public class ExplosiveUnit extends ElementCollection {
   protected void significatorUpdate(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, long var10) {
      super.significatorUpdateMin(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
   }

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ControllerManagerGUI.create(var1, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_EXPLOSIVE_EXPLOSIVEUNIT_0, this, new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_EXPLOSIVE_EXPLOSIVEUNIT_1, ((ExplosiveElementManager)((ExplosiveCollectionManager)this.elementCollectionManager).getElementManager()).getDamage()));
   }
}
