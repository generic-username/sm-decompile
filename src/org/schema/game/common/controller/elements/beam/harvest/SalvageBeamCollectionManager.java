package org.schema.game.common.controller.elements.beam.harvest;

import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.common.controller.Salvager;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.beam.BeamCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.input.InputType;
import org.schema.schine.input.KeyboardMappings;

public class SalvageBeamCollectionManager extends BeamCollectionManager {
   private final SalvageBeamHandler handler;

   public SalvageBeamCollectionManager(SegmentPiece var1, SegmentController var2, SalvageElementManager var3) {
      super(var1, (short)24, var2, var3);
      this.handler = new SalvageBeamHandler((Salvager)var2, this);
   }

   public SalvageBeamHandler getHandler() {
      return this.handler;
   }

   protected Class getType() {
      return SalvageUnit.class;
   }

   public SalvageUnit getInstance() {
      return new SalvageUnit();
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_HARVEST_SALVAGEBEAMCOLLECTIONMANAGER_2;
   }

   public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
      var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_HARVEST_SALVAGEBEAMCOLLECTIONMANAGER_0, var3, ContextFilter.IMPORTANT);
      var2.addHelper(InputType.KEYBOARD, KeyboardMappings.SWITCH_FIRE_MODE.getMapping(), StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_HARVEST_SALVAGEBEAMCOLLECTIONMANAGER_1, this.getFireMode().getName()), var3, ContextFilter.CRUCIAL);
   }
}
