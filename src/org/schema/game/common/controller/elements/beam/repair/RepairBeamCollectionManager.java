package org.schema.game.common.controller.elements.beam.repair;

import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.FocusableUsableModule;
import org.schema.game.common.controller.elements.beam.BeamCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.input.InputType;
import org.schema.schine.input.KeyboardMappings;

public class RepairBeamCollectionManager extends BeamCollectionManager {
   private final RepairBeamHandler handler;

   public RepairBeamCollectionManager(SegmentPiece var1, SegmentController var2, RepairElementManager var3) {
      super(var1, (short)30, var2, var3);
      this.handler = new RepairBeamHandler(var2, this);
   }

   public RepairBeamHandler getHandler() {
      return this.handler;
   }

   protected Class getType() {
      return RepairUnit.class;
   }

   public RepairUnit getInstance() {
      return new RepairUnit();
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_REPAIR_REPAIRBEAMCOLLECTIONMANAGER_0;
   }

   public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
      var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_REPAIR_REPAIRBEAMCOLLECTIONMANAGER_1, var3, ContextFilter.IMPORTANT);
      var2.addHelper(InputType.KEYBOARD, KeyboardMappings.SWITCH_FIRE_MODE.getMapping(), StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_REPAIR_REPAIRBEAMCOLLECTIONMANAGER_2, this.getFireMode().getName()), var3, ContextFilter.CRUCIAL);
   }

   public FocusableUsableModule.FireMode getFireMode() {
      return FocusableUsableModule.FireMode.UNFOCUSED;
   }
}
