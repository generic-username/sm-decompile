package org.schema.game.common.controller.elements.beam.repair;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.damage.HitType;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.beam.BeamUnit;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;

public class RepairUnit extends BeamUnit {
   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((RepairElementManager)((RepairBeamCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (RepairBeamCollectionManager)this.elementCollectionManager, var2, var3);
   }

   public float getBeamPowerWithoutEffect() {
      return this.getBeamPower();
   }

   public float getBeamPower() {
      return (float)this.size() * this.getBaseBeamPower();
   }

   public void flagBeamFiredWithoutTimeout() {
      ((RepairBeamCollectionManager)this.elementCollectionManager).flagBeamFiredWithoutTimeout(this);
   }

   public float getBaseBeamPower() {
      return RepairElementManager.REPAIR_PER_HIT.get(this.getSegmentController().isUsingPowerReactors());
   }

   public float getPowerConsumption() {
      return (float)this.size() * RepairElementManager.POWER_CONSUMPTION;
   }

   public float getDistanceRaw() {
      return RepairElementManager.DISTANCE * ((GameStateInterface)this.getSegmentController().getState()).getGameState().getWeaponRangeReference();
   }

   public float getBasePowerConsumption() {
      return RepairElementManager.POWER_CONSUMPTION;
   }

   public float getPowerConsumptionWithoutEffect() {
      return (float)this.size() * RepairElementManager.POWER_CONSUMPTION;
   }

   public double getPowerConsumedPerSecondResting() {
      return (double)this.size() * (double)RepairElementManager.REACTOR_POWER_CONSUMPTION_RESTING;
   }

   public double getPowerConsumedPerSecondCharging() {
      return (double)this.size() * (double)RepairElementManager.REACTOR_POWER_CONSUMPTION_CHARGING;
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.SUPPORT_BEAMS;
   }

   public HitType getHitType() {
      return HitType.SUPPORT;
   }

   public float getDamage() {
      return 0.0F;
   }
}
