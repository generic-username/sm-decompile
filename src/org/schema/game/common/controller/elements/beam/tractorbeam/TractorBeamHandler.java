package org.schema.game.common.controller.elements.beam.tractorbeam;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import java.util.Collection;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.view.beam.BeamColors;
import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.Salvager;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.controller.elements.power.reactor.StabilizerPath;
import org.schema.game.common.controller.elements.thrust.ThrusterElementManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.beam.BeamHandler;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.game.common.data.physics.RigidBodySegmentController;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;
import org.schema.schine.graphicsengine.core.Timer;
/**
 * Handles tractor beams. Computes the hit event of a tractorbeam pulling, pushing or holding an entity
 */
public class TractorBeamHandler extends BeamHandler {

   private static final float SPEED_MULT_PLAYER = 2.0F;
   /**
    * Vector3f that gets filled with the hit rigidbodies linear velocity
    */
   private Vector3f linearVelocityTmp = new Vector3f();
   /**
    * time tracker used to keep physx in synch (probably, similar to time.deltatime in unity)
    */
   private float timeTracker;
   private Transform tmp = new Transform();
   /**
    * dead field
    */
   private boolean wasRightDown;

   /**
    * constructor
    * only called from TractorBeamCollectionManager
    * @param var1 SegmentController, forcecasted into Salvager
    * @param var2 TractorBeamCollectionManager that calls the constructor
    */
   public TractorBeamHandler(Salvager var1, BeamHandlerContainer var2) {
      super((SegmentController)var1, var2);
   }

   /**
    * checks if tractor beam shooter is the same as the hit entity. compares segmentcontrollers
    * is an inherited method from AbstractBeamHandler, reason for unused 3 extra params
    * could maybe return false positive for docked entities?
    * @param var1 unused
    * @param var2 hit segmentcontroller
    * @param var3 unused
    * @param var4 unused
    * @return true if hit sc is not the shooter
    */
   //TODO check if a tractorbeam can hit its own turret/docked entity
   public boolean canhit(BeamState var1, SegmentController var2, String[] var3, Vector3i var4) {
      return !var2.equals(this.getBeamShooter());
   }

   /**
    * check if hit block is to be ignored
    * will check if the block is only drawn in buildmode and has "return this.lodShapeString.length() > 0;"
    * @param var1 block ID //TODO (needs check)
    * @return true for blocks to ignore
    */
   public boolean ignoreBlock(short var1) {
      ElementInformation var2;
      return (var2 = ElementKeyMap.getInfoFast(var1)).isDrawnOnlyInBuildMode() && !var2.hasLod();
   }

   /**
    * get beam timeout in seconds
    * hardcoded value
    * @return 0.05f hardcoded
    */
   public float getBeamTimeoutInSecs() {
      return 0.05F;
   }

   /**
    * tickrate is inherited through at least 3 classes. proabably 0.1f
    * @param var1
    * @return
    */
   public float getBeamToHitInSecs(BeamState var1) {
      return var1.getTickRate();
   }

   protected boolean isHitsCubesOnly() {
      return true;
   }

   protected boolean canHit(CollisionObject var1) {
      return !(var1.getUserPointer() instanceof StabilizerPath);
   }

   /**
    * Eventmethod called when a tractorbeam hits a collider.
    * Checks if the hit is valid and can move the target.
    * Checks which mode is active (pull, hold, push).
    * Calls moveTo on the target to move it.
    * @param var1 Tractorbeam (?)
    * @param var2 ?
    * @param var3 BeamHandlerContainer
    * @param var4 hit block (?)
    * @param var5 ?
    * @param var6 ?
    * @param var7 timer for physix synch?
    * @param var8
    * @return var2 input param unchanged?
    */
   public int onBeamHit(BeamState var1, int var2, BeamHandlerContainer var3, SegmentPiece var4, Vector3f var5, Vector3f var6, Timer var7, Collection var8) {
      if (((SegmentController)this.getBeamShooter()).isOnServer() && var1.currentHit != null) {
         SegmentController var9;
         if (!((var9 = var1.currentHit.getSegmentController()).railController.getRoot().getPhysicsDataContainer().getObject() instanceof RigidBodySegmentController)) {
            return 0;
         }

         var9.calcWorldTransformRelative(((SegmentController)this.getBeamShooter()).getSectorId(), ((SegmentController)this.getBeamShooter()).getSector(new Vector3i()));
         this.tmp.set(((SegmentController)this.getBeamShooter()).getWorldTransformInverse());
         this.tmp.mul(var9.getClientTransform());
         Vector3f var10 = new Vector3f();
         TractorBeamHandler.TractorMode var11 = this.getMode(var1);
         switch(var11) {
         case HOLD:
         default:
            break;
         case PUSH:
            if ((var5 = new Vector3f(var1.initalRelativeTranform.origin)).lengthSquared() > 0.0F) {
               var5.normalize();
               var1.initalRelativeTranform.origin.add(var5);
            }
            break;
         case PULL:
            if ((var5 = new Vector3f(var1.initalRelativeTranform.origin)).lengthSquared() > 6.0F) {
               var5.normalize();
               var1.initalRelativeTranform.origin.sub(var5);
            }
         }

         var10.sub(var1.initalRelativeTranform.origin, this.tmp.origin);
         ((SegmentController)this.getBeamShooter()).getWorldTransform().basis.transform(var10);
         this.moveTo(var7, var1, var10, var9);
      }

      return var2;
   }

   private TractorBeamHandler.TractorMode getMode(BeamState var1) {
      TractorBeamCollectionManager var2;
      if ((var2 = (TractorBeamCollectionManager)((TractorElementManager)((ShipManagerContainer)((ManagedSegmentController)this.getBeamShooter()).getManagerContainer()).getTractorBeam().getElementManager()).getCollectionManagersMap().get(var1.weaponId)) != null) {
         return var2.getTractorMode();
      } else {
         assert false : "tractor beam not found " + var1.weaponId;

         System.err.println(this.getState() + " ERROR: Tractor Mode not found for " + this.getBeamShooter() + "; " + var1.weaponId);
         return TractorBeamHandler.TractorMode.HOLD;
      }
   }

   /**
    * will move the target entity according to beam strength
    * gets linear velocity of rigidbody, modifies it, sets linear velocity
    * @param var1 Timer used for physx synchro (i think)
    * @param var2 beam
    * @param var3 pull direction
    * @param var4 target segmentcontroller
    */
   public void moveTo(Timer var1, BeamState var2, Vector3f var3, SegmentController var4) {
      if (!var4.getDockingController().isDocked() && !var4.railController.isDockedOrDirty()) {
         if (!(var4.getPhysicsDataContainer().getObject() instanceof RigidBody)) { //not a ridigbody
            System.err.println("[TRACTORBEAM] cant move: " + var4);
         } else {
            float var12 = Math.min(
                    Math.max(1.0F, var4.getMass() * TractorElementManager.FORCE_TO_MASS_MAX/* hardcoded 10f */ //mass * acceleration = force
                    ), var2.getPower()); //range from 10 m/s^2 to beam power
            float var5 = ThrusterElementManager.getUpdateFrequency();
            float var6 = var3.length(); //pull vector length
            (var3 = new Vector3f(var3)).normalize();
            RigidBody var7 = (RigidBody)var4.getPhysicsDataContainer().getObject();
            this.timeTracker += var1.getDelta();
            this.timeTracker = Math.min(var5 * 100.0F, this.timeTracker);

            while(this.timeTracker >= var5) {
               this.timeTracker -= var5;
               float var11 = var12;
               Vector3f var8;
               if ((double)var6 > 0.05D) {
                  var7.activate();
                  var7.getLinearVelocity(this.linearVelocityTmp);
                  var8 = var7.getLinearVelocity(new Vector3f());
                  Vector3f var9;
                  (var9 = new Vector3f(this.linearVelocityTmp)).normalize();
                  if (Vector3fTools.diffLength(var9, var3) > 1.5F) {
                     var8.scale(0.1F);
                     var7.setLinearVelocity(var8);
                  } else if (Vector3fTools.diffLength(var9, var3) > 1.0F) {
                     var8.scale(0.4F);
                     var7.setLinearVelocity(var8);
                  } else if (Vector3fTools.diffLength(var9, var3) > 0.3F) {
                     var8.scale(0.7F);
                     var7.setLinearVelocity(var8);
                  } else if (var6 < 2.0F) {
                     if (var8.length() > 3.0F) {
                        var8.scale(0.8F);
                        var7.setLinearVelocity(var8);
                     }

                     var11 = (float)((double)var12 * 0.5D);
                  } else if (var6 < 1.0F) {
                     if (var8.length() > 2.0F) {
                        var8.scale(0.6F);
                        var7.setLinearVelocity(var8);
                     }

                     var11 = (float)((double)var12 * 0.3D);
                  } else if (var6 < 0.5F) {
                     var8.scale(0.1F);
                     var7.setLinearVelocity(var8);
                     var11 = (float)((double)var12 * 0.1D);
                  } else {
                     var11 = var12 * 1.01F;
                  }

                  (var9 = new Vector3f(var3)).normalize();
                  float var10 = FactionManager.isNPCFaction(var4.getFactionId()) ? ((GameStateInterface)var4.getState()).getGameState().getNPCFleetSpeedLoaded() : 2.0F;
                  ThrusterElementManager.applyThrustForce(var9, var11, var7, var4, var10, var8, true);
               } else if ((double)var6 < 0.05D) {
                  (var8 = var7.getLinearVelocity(new Vector3f())).scale(0.3F);
                  if (var8.length() < 1.0F) {
                     var8.set(0.0F, 0.0F, 0.0F);
                  }

                  var7.setLinearVelocity(var8);
               }
            }

         }
      }
   }

   /**
    * always false, what is this used for? why 8 parameters?
    * @param var1
    * @param var2
    * @param var3
    * @param var4
    * @param var5
    * @param var6
    * @param var7
    * @param var8
    * @return always false, hardcoded value
    */
   protected boolean onBeamHitNonCube(BeamState var1, int var2, BeamHandlerContainer var3, Vector3f var4, Vector3f var5, CubeRayCastResult var6, Timer var7, Collection var8) {
      return false;
   }

   protected boolean ignoreNonPhysical(BeamState var1) {
      return false;
   }

   /**
    * return blue as color
    * @param var1 BeamState
    * @return blue
    */
   public Vector4f getDefaultColor(BeamState var1) {
      return getColorRange(BeamColors.BLUE);
   }

   /**
    * tractor beam mode
    * //TODO fix change method. holding key will cycle mode. change to onbuttondown/up so its only called once per press.
    */
   public static enum TractorMode {
      HOLD(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_TRACTORBEAM_TRACTORBEAMHANDLER_0;
         }
      }),
      PUSH(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_TRACTORBEAM_TRACTORBEAMHANDLER_1;
         }
      }),
      PULL(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_TRACTORBEAM_TRACTORBEAMHANDLER_2;
         }
      });

      private final Translatable nm;

      private TractorMode(Translatable var3) {
         this.nm = var3;
      }

      public final String getName() {
         return this.nm.getName(this);
      }
   }
}
