package org.schema.game.common.controller.elements.beam;

public interface ShieldConditionInterface {
   boolean isActive();

   boolean isShieldUsable();
}
