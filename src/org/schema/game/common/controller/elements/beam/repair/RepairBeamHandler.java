package org.schema.game.common.controller.elements.beam.repair;

import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap.Entry;
import it.unimi.dsi.fastutil.shorts.Short2IntOpenHashMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.util.Collection;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.BlockBuffer;
import org.schema.game.client.controller.manager.ingame.BuildCallback;
import org.schema.game.client.controller.manager.ingame.BuildInstruction;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.beam.BeamColors;
import org.schema.game.client.view.buildhelper.BuildHelper;
import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.Salvage;
import org.schema.game.common.controller.SegmentCollisionCheckerCallback;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.controller.elements.effectblock.EffectElementManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.beam.BeamHandler;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.schine.graphicsengine.core.Timer;

public class RepairBeamHandler extends BeamHandler {
   private Object2ObjectOpenHashMap connectedInventories = new Object2ObjectOpenHashMap();
   private Short2IntOpenHashMap consTmp = new Short2IntOpenHashMap();
   private Short2IntOpenHashMap haveTmp = new Short2IntOpenHashMap();
   private long lastSentMsg;

   public RepairBeamHandler(SegmentController var1, BeamHandlerContainer var2) {
      super(var1, var2);
   }

   public boolean canhit(BeamState var1, SegmentController var2, String[] var3, Vector3i var4) {
      var3[0] = "";
      return !var2.equals(this.getBeamShooter()) && var2 instanceof Salvage && ((Salvage)var2).isRepariableFor(this, var3, var4);
   }

   public float getBeamTimeoutInSecs() {
      return 0.05F;
   }

   public boolean ignoreBlock(short var1) {
      ElementInformation var2;
      return (var2 = ElementKeyMap.getInfoFast(var1)).isDrawnOnlyInBuildMode() && !var2.hasLod();
   }

   public float getBeamToHitInSecs(BeamState var1) {
      return var1.getTickRate();
   }

   public int onBeamHit(BeamState var1, int var2, BeamHandlerContainer var3, SegmentPiece var4, Vector3f var5, Vector3f var6, Timer var7, Collection var8) {
      if (this.getBeamShooter() instanceof EditableSendableSegmentController) {
         if (var4.getSegmentController().isClientOwnObject() && ((GameClientState)var4.getSegmentController().getState()).getWorldDrawer() != null) {
            ((GameClientState)var4.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().notifyEffectHit(var4.getSegmentController(), EffectElementManager.OffensiveEffects.REPAIR);
         }

         if (((SegmentController)this.getBeamShooter()).isOnServer()) {
            byte var9 = var4.getInfo().getMaxHitPointsByte();
            if (var4.getHitpointsByte() < var9) {
               var4.setHitpointsByte(var9);
               var4.applyToSegment(((SegmentController)this.getBeamShooter()).isOnServer());
            } else {
               ManagedUsableSegmentController var10;
               if (var4.getSegmentController() instanceof ManagedUsableSegmentController && (var10 = (ManagedUsableSegmentController)var4.getSegmentController()).getBlockKillRecorder().size() > 0) {
                  this.undo(var1.controllerPos, var10, var10.getBlockKillRecorder(), (int)var1.getPower());
               }
            }
         }
      }

      return var2;
   }

   public void undo(Vector3i var1, ManagedUsableSegmentController var2, BlockBuffer var3, int var4) {
      boolean var14 = false;

      label190: {
         Iterator var21;
         Entry var24;
         label191: {
            try {
               var14 = true;
               this.connectInventories(ElementCollection.getIndex(var1));
               this.checkInventories(var3, var4);
               final BuildInstruction.Remove var16;
               (var16 = new BuildInstruction.Remove()).where = new SegmentPiece();
               var16.connectedFromThis = new LongOpenHashSet();
               SegmentCollisionCheckerCallback var5 = new SegmentCollisionCheckerCallback();
               int var6 = 0;

               while(true) {
                  if (var6 >= Math.min(var4, var3.size())) {
                     var14 = false;
                     break label190;
                  }

                  if (!this.consumeResource(var3.peakNextType())) {
                     if (((SegmentController)this.getBeamShooter()).getState().getUpdateTime() - this.lastSentMsg > 1000L) {
                        short var22;
                        short var23;
                        short var25 = (var23 = (short)ElementKeyMap.getInfo(var22 = var3.peakNextType()).getSourceReference()) != 0 ? var23 : var22;
                        ((SegmentController)this.getBeamShooter()).sendServerMessage((Object[])(new Object[]{34, ElementKeyMap.getInfo(var25).getName()}), 3);
                        this.lastSentMsg = ((SegmentController)this.getBeamShooter()).getState().getUpdateTime();
                        var14 = false;
                     } else {
                        var14 = false;
                     }
                     break label191;
                  }

                  var3.createInstruction(var2, var16);
                  if (var2.getCollisionChecker().checkPieceCollision(var16.where, var5, false)) {
                     var14 = false;
                     break;
                  }

                  Vector3i var7 = new Vector3i();
                  BuildCallback var8 = new BuildCallback() {
                     public void onBuild(Vector3i var1, Vector3i var2, short var3) {
                     }

                     public long getSelectedControllerPos() {
                        return var16.controller;
                     }
                  };
                  SegmentPiece var9;
                  if ((var9 = var16.where).getType() != 0) {
                     Vector3i var10 = var9.getAbsolutePos(new Vector3i());
                     var2.build(var10.x, var10.y, var10.z, var9.getType(), var9.getOrientation(), var9.isActive(), var8, var7, new int[]{0, 1}, (BuildHelper)null, (BuildInstruction)null);
                     if (var16.connectedFromThis != null) {
                        var21 = var16.connectedFromThis.iterator();

                        while(var21.hasNext()) {
                           long var11 = (Long)var21.next();
                           var2.getControlElementMap().removeControlledFromAll(ElementCollection.getPosIndexFrom4(var11), (short)ElementCollection.getType(var11), true);
                           var2.getControlElementMap().addControllerForElement(var9.getAbsoluteIndex(), ElementCollection.getPosIndexFrom4(var11), (short)ElementCollection.getType(var11));
                        }
                     }
                  }

                  ++var6;
               }
            } finally {
               if (var14) {
                  Iterator var18 = this.connectedInventories.object2ObjectEntrySet().iterator();

                  while(var18.hasNext()) {
                     Entry var19;
                     ((Inventory)(var19 = (Entry)var18.next()).getKey()).sendInventoryModification((IntCollection)var19.getValue());
                  }

                  this.connectedInventories.clear();
                  this.consTmp.clear();
                  this.haveTmp.clear();
               }
            }

            var21 = this.connectedInventories.object2ObjectEntrySet().iterator();

            while(var21.hasNext()) {
               ((Inventory)(var24 = (Entry)var21.next()).getKey()).sendInventoryModification((IntCollection)var24.getValue());
            }

            this.connectedInventories.clear();
            this.consTmp.clear();
            this.haveTmp.clear();
            return;
         }

         var21 = this.connectedInventories.object2ObjectEntrySet().iterator();

         while(var21.hasNext()) {
            ((Inventory)(var24 = (Entry)var21.next()).getKey()).sendInventoryModification((IntCollection)var24.getValue());
         }

         this.connectedInventories.clear();
         this.consTmp.clear();
         this.haveTmp.clear();
         return;
      }

      Iterator var17 = this.connectedInventories.object2ObjectEntrySet().iterator();

      while(var17.hasNext()) {
         Entry var20;
         ((Inventory)(var20 = (Entry)var17.next()).getKey()).sendInventoryModification((IntCollection)var20.getValue());
      }

      this.connectedInventories.clear();
      this.consTmp.clear();
      this.haveTmp.clear();
   }

   private void checkInventories(BlockBuffer var1, int var2) {
      this.consTmp.clear();
      this.haveTmp.clear();
      var2 = Math.min(var1.size(), var2);
      var1.peak(var2, this.consTmp);
      Iterator var6 = this.consTmp.keySet().iterator();

      while(var6.hasNext()) {
         short var7 = (Short)var6.next();
         Iterator var3 = this.connectedInventories.keySet().iterator();

         while(var3.hasNext()) {
            Inventory var4 = (Inventory)var3.next();
            short var5 = (short)ElementKeyMap.getInfo(var7).getSourceReference();
            this.haveTmp.add(var7, var4.getOverallQuantity(var5 != 0 ? var5 : var7));
         }
      }

   }

   private void connectInventories(long var1) {
      this.connectedInventories.clear();
      Short2ObjectOpenHashMap var7;
      if ((var7 = ((SegmentController)this.getBeamShooter()).getControlElementMap().getControllingMap().get(var1)) != null) {
         Iterator var2 = var7.keySet().iterator();

         while(true) {
            short var3;
            do {
               do {
                  if (!var2.hasNext()) {
                     if (this.connectedInventories.isEmpty() && ((SegmentController)this.getBeamShooter()).getOwnerState() != null) {
                        this.connectedInventories.put(((SegmentController)this.getBeamShooter()).getOwnerState().getInventory(), new IntOpenHashSet());
                     }

                     return;
                  }
               } while(!ElementKeyMap.isValidType(var3 = (Short)var2.next()));
            } while(!ElementKeyMap.getInfoFast(var3).isInventory());

            Iterator var8 = ((FastCopyLongOpenHashSet)var7.get(var3)).iterator();

            while(var8.hasNext()) {
               long var5 = (Long)var8.next();
               Inventory var4;
               if ((var4 = ((ManagedSegmentController)this.getBeamShooter()).getManagerContainer().getInventory(ElementCollection.getPosIndexFrom4(var5))) != null) {
                  this.connectedInventories.put(var4, new IntOpenHashSet());
               }
            }
         }
      }
   }

   private boolean consumeResource(short var1) {
      if (this.haveTmp.get(var1) <= 0) {
         return false;
      } else {
         this.haveTmp.add(var1, -1);
         Iterator var2 = this.connectedInventories.object2ObjectEntrySet().iterator();

         while(var2.hasNext()) {
            Entry var3 = (Entry)var2.next();
            short var4 = (var4 = (short)ElementKeyMap.getInfo(var1).getSourceReference()) != 0 ? var4 : var1;
            if (((Inventory)var3.getKey()).containsAny(var4)) {
               ((Inventory)var3.getKey()).decreaseBatch(var4, 1, (IntOpenHashSet)var3.getValue());
               break;
            }
         }

         return true;
      }
   }

   protected boolean onBeamHitNonCube(BeamState var1, int var2, BeamHandlerContainer var3, Vector3f var4, Vector3f var5, CubeRayCastResult var6, Timer var7, Collection var8) {
      return false;
   }

   public Vector4f getDefaultColor(BeamState var1) {
      return getColorRange(BeamColors.PURPLE);
   }
}
