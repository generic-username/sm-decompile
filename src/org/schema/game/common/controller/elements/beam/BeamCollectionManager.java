package org.schema.game.common.controller.elements.beam;

import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.FocusableUsableModule;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ShieldAddOn;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.controller.elements.combination.BeamCombiSettings;
import org.schema.game.common.controller.elements.weapon.ZoomableUsableModule;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public abstract class BeamCollectionManager extends ControlBlockElementCollectionManager implements BeamHandlerContainer, PlayerUsableInterface, FocusableUsableModule, ZoomableUsableModule {
   float beamCharge;
   private boolean wasShootButtonDown;
   private boolean charged;
   protected long lastBeamFired;
   private FocusableUsableModule.FireMode mode = FocusableUsableModule.FireMode.getDefault(this.getClass());

   public BeamCollectionManager(SegmentPiece var1, short var2, SegmentController var3, BeamElementManager var4) {
      super(var1, var2, var3, var4);
   }

   public boolean needsUpdate() {
      return true;
   }

   public void onChangedCollection() {
      super.onChangedCollection();
      this.getHandler().clearStates();
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
      }

   }

   public float getChargeTime() {
      return 0.0F;
   }

   public BeamCombiSettings getWeaponChargeParams() {
      ((BeamElementManager)this.getElementManager()).getCombiSettings().chargeTime = this.getChargeTime();
      ((BeamElementManager)this.getElementManager()).getCombiSettings().possibleZoom = this.getPossibleZoomRaw();
      ((BeamElementManager)this.getElementManager()).getCombiSettings().burstTime = ((BeamElementManager)this.getElementManager()).getBurstTime();
      ControlBlockElementCollectionManager var1;
      if (((BeamElementManager)this.getElementManager()).getAddOn() != null && (var1 = this.getSupportCollectionManager()) != null) {
         ((BeamElementManager)this.getElementManager()).getAddOn().calcCombiSettings(((BeamElementManager)this.getElementManager()).getCombiSettings(), this, var1, this.getEffectCollectionManager());
      }

      return ((BeamElementManager)this.getElementManager()).getCombiSettings();
   }

   public void flagBeamFiredWithoutTimeout(BeamUnit var1) {
      this.lastBeamFired = this.getState().getUpdateTime();
   }

   public void onSwitched(boolean var1) {
      super.onSwitched(var1);
      this.charged = false;
      this.wasShootButtonDown = false;
      this.beamCharge = 0.0F;
   }

   public float getPossibleZoomRaw() {
      return -1.0F;
   }

   public boolean isInFocusMode() {
      return this.mode == FocusableUsableModule.FireMode.FOCUSED;
   }

   public void setFireMode(FocusableUsableModule.FireMode var1) {
      this.mode = var1;
   }

   public FocusableUsableModule.FireMode getFireMode() {
      return this.mode;
   }

   public boolean canUseCollection(ControllerStateInterface var1, Timer var2) {
      if (!super.canUseCollection(var1, var2)) {
         return false;
      } else {
         try {
            BeamCombiSettings var3;
            if (var1.isPrimaryShootButtonDown() && (var3 = this.getWeaponChargeParams()).chargeTime > 0.0F) {
               if (this.wasShootButtonDown && this.charged) {
                  return true;
               }

               boolean var4 = false;
               Iterator var5 = this.getElementCollections().iterator();

               while(var5.hasNext()) {
                  if (!((BeamUnit)var5.next()).canUse(var2.currentTime, false)) {
                     var4 = true;
                     break;
                  }
               }

               if (!var4 && this.beamCharge < var3.chargeTime) {
                  if (this.beamCharge == 0.0F && this.dropShieldsOnCharge() && ((BeamElementManager)this.getElementManager()).getManagerContainer() instanceof ShieldContainerInterface) {
                     ShieldAddOn var8;
                     (var8 = ((ShieldContainerInterface)((BeamElementManager)this.getElementManager()).getManagerContainer()).getShieldAddOn()).onHit(0L, (short)0, (double)((long)Math.ceil(var8.getShields())), DamageDealerType.GENERAL);
                     var8.getShieldLocalAddOn().addShieldCondition(new ShieldConditionInterface() {
                        public boolean isActive() {
                           return BeamCollectionManager.this.beamCharge > 0.0F;
                        }

                        public boolean isShieldUsable() {
                           return false;
                        }
                     }, true);
                  }

                  this.beamCharge = Math.min(var3.chargeTime, this.beamCharge + var2.getDelta());
               }

               if (this.beamCharge < var3.chargeTime) {
                  return false;
               }

               this.charged = true;
               this.beamCharge = 0.0F;
               return true;
            }
         } finally {
            this.wasShootButtonDown = var1.isPrimaryShootButtonDown();
         }

         return true;
      }
   }

   protected boolean dropShieldsOnCharge() {
      return false;
   }

   protected void onNotShootingButtonDown(ControllerStateInterface var1, Timer var2) {
      super.onNotShootingButtonDown(var1, var2);
      BeamCombiSettings var3 = this.getWeaponChargeParams();
      this.wasShootButtonDown = false;
      if (var3.chargeTime > 0.0F) {
         if (this.beamCharge >= var3.chargeTime) {
            this.handleControlShot(var1, var2);
            this.beamCharge = 0.0F;
            return;
         }

         this.charged = false;
         if (this.beamCharge > 0.0F) {
            this.getSegmentController().popupOwnClientMessage("notFireBeamNotCharged", StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_BEAMCOLLECTIONMANAGER_0, StringTools.formatPointZero(var3.chargeTime)), 3);
            this.beamCharge = 0.0F;
         }
      }

   }

   protected void onRemovedCollection(long var1, BeamCollectionManager var3) {
      super.onRemovedCollection(var1, var3);
      ((BeamElementManager)this.getElementManager()).notifyBeamDrawer();
   }

   public void update(Timer var1) {
      super.update(var1);
      this.getHandler().update(var1);
   }

   public InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3) {
      return ((BeamElementManager)this.getElementManager()).getManagerContainer().getAttackEffectSet(var1, var3);
   }

   public MetaWeaponEffectInterface getMetaWeaponEffect(long var1, DamageDealerType var3) {
      return null;
   }

   public boolean handleBeamLatch(ManagerContainer.ReceivedBeamLatch var1) {
      return this.getHandler().handleBeamLatch(var1);
   }

   public float getPossibleZoom() {
      return this.getWeaponChargeParams().possibleZoom;
   }
}
