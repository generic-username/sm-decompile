package org.schema.game.common.controller.elements.beam.tractorbeam;

import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.network.objects.valueUpdate.ByteModuleValueUpdate;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;

public class TractorModeValueUpdate extends ByteModuleValueUpdate {
   public boolean applyClient(ManagerContainer var1) {
      assert var1 instanceof ShipManagerContainer;

      TractorBeamCollectionManager var2;
      if ((var2 = (TractorBeamCollectionManager)((TractorElementManager)((ShipManagerContainer)var1).getTractorBeam().getElementManager()).getCollectionManagersMap().get(this.parameter)) != null) {
         var2.setTractorModeFromReceived(TractorBeamHandler.TractorMode.values()[this.val]);
         return true;
      } else {
         return false;
      }
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.parameter = var2;
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.TRACTOR_MODE;
   }
}
