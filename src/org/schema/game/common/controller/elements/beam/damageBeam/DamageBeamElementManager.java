package org.schema.game.common.controller.elements.beam.damageBeam;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import java.util.Iterator;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.elements.BlockActivationListenerInterface;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.WeaponElementManagerInterface;
import org.schema.game.common.controller.elements.WeaponStatisticsData;
import org.schema.game.common.controller.elements.beam.BeamElementManager;
import org.schema.game.common.controller.elements.combination.Combinable;
import org.schema.game.common.controller.elements.combination.CombinationAddOn;
import org.schema.game.common.controller.elements.combination.DamageBeamCombinationAddOn;
import org.schema.game.common.controller.elements.combination.modifier.BeamUnitModifier;
import org.schema.game.common.controller.elements.config.FloatReactorDualConfigElement;
import org.schema.game.common.controller.elements.effectblock.EffectCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;

public class DamageBeamElementManager extends BeamElementManager implements BlockActivationListenerInterface, WeaponElementManagerInterface, Combinable {
   static final double[] weightedLookupTable = new double[300];
   @ConfigurationElement(
      name = "DamageReductionPerArmorValueMult"
   )
   public static float DAMAGE_REDUCTION_PER_ARMOR_VALUE_MULT = 1.0F;
   @ConfigurationElement(
      name = "DamagePerHit"
   )
   public static FloatReactorDualConfigElement DAMAGE_PER_HIT = new FloatReactorDualConfigElement();
   @ConfigurationElement(
      name = "PowerConsumptionPerTick"
   )
   public static float POWER_CONSUMPTION = 10.0F;
   @ConfigurationElement(
      name = "Distance"
   )
   public static float DISTANCE = 30.0F;
   @ConfigurationElement(
      name = "TickRate"
   )
   public static float TICK_RATE = 100.0F;
   @ConfigurationElement(
      name = "CoolDown"
   )
   public static float COOL_DOWN = 3.0F;
   @ConfigurationElement(
      name = "BurstTime"
   )
   public static float BURST_TIME = 3.0F;
   @ConfigurationElement(
      name = "InitialTicks"
   )
   public static float INITIAL_TICKS = 1.0F;
   @ConfigurationElement(
      name = "ReactorPowerConsumptionResting"
   )
   public static float REACTOR_POWER_CONSUMPTION_RESTING = 10.0F;
   @ConfigurationElement(
      name = "ReactorPowerConsumptionCharging"
   )
   public static float REACTOR_POWER_CONSUMPTION_CHARGING = 10.0F;
   @ConfigurationElement(
      name = "AdditionalPowerConsumptionPerUnitMult"
   )
   public static float ADDITIONAL_POWER_CONSUMPTION_PER_UNIT_MULT = 100.0F;
   @ConfigurationElement(
      name = "RailHitMultiplierParent"
   )
   public static double RAIL_HIT_MULTIPLIER_PARENT = 3.0D;
   @ConfigurationElement(
      name = "RailHitMultiplierChild"
   )
   public static double RAIL_HIT_MULTIPLIER_CHILD = 3.0D;
   @ConfigurationElement(
      name = "EffectConfiguration"
   )
   public static InterEffectSet basicEffectConfiguration = new InterEffectSet();
   @ConfigurationElement(
      name = "LatchOn"
   )
   public static int LATCH_ON = 1;
   @ConfigurationElement(
      name = "CheckLatchConnection"
   )
   public static int CHECK_LATCH_CONNECTION = 1;
   @ConfigurationElement(
      name = "Penetration"
   )
   public static int PENETRATION = 1;
   @ConfigurationElement(
      name = "AcidDamagePercentage"
   )
   public static float ACID_DAMAGE_PERCENTAGE = 1.0F;
   @ConfigurationElement(
      name = "FriendlyFire"
   )
   public static int FRIENDLY_FIRE = 1;
   @ConfigurationElement(
      name = "Aimable"
   )
   public static int AIMABLE = 1;
   @ConfigurationElement(
      name = "ChargeTime"
   )
   public static float CHARGE_TIME = 1.0F;
   @ConfigurationElement(
      name = "MinEffectiveValue"
   )
   public static float MIN_EFFECTIVE_VALUE = 1.0F;
   @ConfigurationElement(
      name = "MinEffectiveRange"
   )
   public static float MIN_EFFECTIVE_RANGE = 1.0F;
   @ConfigurationElement(
      name = "MaxEffectiveValue"
   )
   public static float MAX_EFFECTIVE_VALUE = 1.0F;
   @ConfigurationElement(
      name = "MaxEffectiveRange"
   )
   public static float MAX_EFFECTIVE_RANGE = 1.0F;
   @ConfigurationElement(
      name = "DropShieldsOnCharging"
   )
   public static boolean DROP_SHIELDS_ON_CHARGING = false;
   @ConfigurationElement(
      name = "PossibleZoom"
   )
   public static float POSSIBLE_ZOOM = 0.0F;
   private final WeaponStatisticsData tmpOutput = new WeaponStatisticsData();
   private final DamageBeamCombinationAddOn addOn;

   public double getRailHitMultiplierParent() {
      return RAIL_HIT_MULTIPLIER_PARENT;
   }

   public double getRailHitMultiplierChild() {
      return RAIL_HIT_MULTIPLIER_CHILD;
   }

   public DamageBeamElementManager(SegmentController var1) {
      super((short)414, (short)415, var1);
      this.addOn = new DamageBeamCombinationAddOn(this, (GameStateInterface)var1.getState());
   }

   public void updateActivationTypes(ShortOpenHashSet var1) {
      var1.add((short)415);
   }

   protected String getTag() {
      return "damagebeam";
   }

   public DamageBeamCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new DamageBeamCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_DAMAGEBEAM_DAMAGEBEAMELEMENTMANAGER_9;
   }

   protected void playSound(DamageBeamUnit var1, Transform var2) {
   }

   public CombinationAddOn getAddOn() {
      return this.addOn;
   }

   public ControllerManagerGUI getGUIUnitValues(DamageBeamUnit var1, DamageBeamCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      if (var4 != null) {
         var2.setEffectTotal(var4.getTotalSize());
      } else {
         var2.setEffectTotal(0);
      }

      float var5 = var1.getBeamPower();
      float var6 = var1.getDistance();
      var1.getPowerConsumption();
      float var7 = var1.getTickRate();
      float var8 = 0.0F;
      float var9 = var1.getCoolDownSec();
      float var10 = var1.getBurstTime();
      if (var3 != null) {
         BeamUnitModifier var11;
         var6 = (var11 = (BeamUnitModifier)this.getAddOn().getGUI(var2, var1, (ControlBlockElementCollectionManager)var3, var4)).outputDistance;
         var5 = var11.outputDamagePerHit;
         float var10000 = var11.outputPowerConsumption;
         var9 = var11.outputCoolDown;
         var10 = var11.outputBurstTime;
      }

      if (var4 != null) {
         var4.getElementManager();
         var8 = CombinationAddOn.getRatio(var2, var4);
      }

      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_DAMAGEBEAM_DAMAGEBEAMELEMENTMANAGER_0, var1, new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_DAMAGEBEAM_DAMAGEBEAMELEMENTMANAGER_1, StringTools.formatPointZero(var5)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_DAMAGEBEAM_DAMAGEBEAMELEMENTMANAGER_2, StringTools.formatPointZero(var7)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_DAMAGEBEAM_DAMAGEBEAMELEMENTMANAGER_3, StringTools.formatPointZero(var5 * (1.0F / Math.max(1.0E-4F, var7)))), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_DAMAGEBEAM_DAMAGEBEAMELEMENTMANAGER_6, StringTools.formatPointZero(var6)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_DAMAGEBEAM_DAMAGEBEAMELEMENTMANAGER_7, StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_DAMAGEBEAM_DAMAGEBEAMELEMENTMANAGER_10, StringTools.formatPointZero(var9))), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_DAMAGEBEAM_DAMAGEBEAMELEMENTMANAGER_11, StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_DAMAGEBEAM_DAMAGEBEAMELEMENTMANAGER_8, StringTools.formatPointZero(var10))), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_DAMAGEBEAM_DAMAGEBEAMELEMENTMANAGER_4, var1.getPowerConsumedPerSecondResting()), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_DAMAGEBEAM_DAMAGEBEAMELEMENTMANAGER_5, var1.getPowerConsumedPerSecondCharging()), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_DAMAGEBEAM_DAMAGEBEAMELEMENTMANAGER_20, StringTools.formatPointZero(var8)));
   }

   public float getTickRate() {
      return TICK_RATE;
   }

   public float getCoolDown() {
      return COOL_DOWN;
   }

   public float getBurstTime() {
      return BURST_TIME;
   }

   public float getInitialTicks() {
      return INITIAL_TICKS;
   }

   public WeaponStatisticsData getStatistics(DamageBeamUnit var1, DamageBeamCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4, WeaponStatisticsData var5) {
      if (var4 != null) {
         var2.setEffectTotal(var4.getTotalSize());
      } else {
         var2.setEffectTotal(0);
      }

      var5.damage = var1.getBaseBeamPower();
      var5.speed = 100.0F;
      var5.tickRate = var1.getTickRate();
      var5.burstTime = var1.getBurstTime();
      var5.distance = var1.getDistance();
      var5.reload = var1.getReloadTimeMs();
      var5.powerConsumption = var1.getPowerConsumption();
      var5.split = 1.0F;
      var5.mode = 1.0F;
      var5.effectRatio = 0.0F;
      if (var3 != null) {
         BeamUnitModifier var6 = (BeamUnitModifier)this.getAddOn().getGUI(var2, var1, (ControlBlockElementCollectionManager)var3, var4);
         var5.distance = var6.outputDistance;
         var5.damage = var6.outputDamagePerHit;
         var5.powerConsumption = var6.outputPowerConsumption;
         var5.burstTime = var6.outputBurstTime;
      }

      if (var4 != null) {
         var4.getElementManager();
         var5.effectRatio = CombinationAddOn.getRatio(var2, var4);
      }

      return var5;
   }

   public double calculateWeaponDamageIndex() {
      double var1 = 0.0D;
      Iterator var3 = this.getCollectionManagers().iterator();

      while(var3.hasNext()) {
         DamageBeamCollectionManager var4;
         ControlBlockElementCollectionManager var5 = (var4 = (DamageBeamCollectionManager)var3.next()).getSupportCollectionManager();
         EffectCollectionManager var6 = var4.getEffectCollectionManager();

         for(Iterator var7 = var4.getElementCollections().iterator(); var7.hasNext(); var1 += (double)(this.tmpOutput.damage / this.tmpOutput.split * (1.0F / Math.max(1.0E-4F, this.tmpOutput.tickRate))) / ((double)this.tmpOutput.reload / 1000.0D)) {
            DamageBeamUnit var8 = (DamageBeamUnit)var7.next();
            this.getStatistics(var8, var4, var5, var6, this.tmpOutput);
         }
      }

      return var1;
   }

   public double calculateWeaponRangeIndex() {
      double var1 = 0.0D;
      double var3 = 0.0D;
      Iterator var5 = this.getCollectionManagers().iterator();

      while(var5.hasNext()) {
         DamageBeamCollectionManager var6;
         ControlBlockElementCollectionManager var7 = (var6 = (DamageBeamCollectionManager)var5.next()).getSupportCollectionManager();
         EffectCollectionManager var8 = var6.getEffectCollectionManager();

         for(Iterator var9 = var6.getElementCollections().iterator(); var9.hasNext(); ++var3) {
            DamageBeamUnit var10 = (DamageBeamUnit)var9.next();
            this.getStatistics(var10, var6, var7, var8, this.tmpOutput);
            var1 += (double)this.tmpOutput.distance;
         }
      }

      if (var3 > 0.0D) {
         return var1 / var3;
      } else {
         return 0.0D;
      }
   }

   public double calculateWeaponHitPropabilityIndex() {
      double var1 = 0.0D;
      Iterator var3 = this.getCollectionManagers().iterator();

      while(var3.hasNext()) {
         DamageBeamCollectionManager var4;
         ControlBlockElementCollectionManager var5 = (var4 = (DamageBeamCollectionManager)var3.next()).getSupportCollectionManager();
         EffectCollectionManager var6 = var4.getEffectCollectionManager();

         for(Iterator var7 = var4.getElementCollections().iterator(); var7.hasNext(); var1 += (double)(100.0F * this.tmpOutput.split) / ((double)this.tmpOutput.reload / 1000.0D)) {
            DamageBeamUnit var8 = (DamageBeamUnit)var7.next();
            this.getStatistics(var8, var4, var5, var6, this.tmpOutput);
         }
      }

      return var1;
   }

   public double calculateWeaponSpecialIndex() {
      Iterator var1 = this.getCollectionManagers().iterator();

      while(var1.hasNext()) {
         DamageBeamCollectionManager var2;
         ControlBlockElementCollectionManager var3 = (var2 = (DamageBeamCollectionManager)var1.next()).getSupportCollectionManager();
         EffectCollectionManager var4 = var2.getEffectCollectionManager();
         Iterator var5 = var2.getElementCollections().iterator();

         while(var5.hasNext()) {
            DamageBeamUnit var6 = (DamageBeamUnit)var5.next();
            this.getStatistics(var6, var2, var3, var4, this.tmpOutput);
         }
      }

      return 0.0D;
   }

   public double calculateWeaponPowerConsumptionPerSecondIndex() {
      double var1 = 0.0D;
      Iterator var3 = this.getCollectionManagers().iterator();

      while(var3.hasNext()) {
         DamageBeamCollectionManager var4;
         ControlBlockElementCollectionManager var5 = (var4 = (DamageBeamCollectionManager)var3.next()).getSupportCollectionManager();
         EffectCollectionManager var6 = var4.getEffectCollectionManager();

         DamageBeamUnit var8;
         for(Iterator var7 = var4.getElementCollections().iterator(); var7.hasNext(); var1 += (double)var8.getPowerConsumption()) {
            var8 = (DamageBeamUnit)var7.next();
            this.getStatistics(var8, var4, var5, var6, this.tmpOutput);
         }
      }

      return var1;
   }
}
