package org.schema.game.common.controller.elements.beam.tractorbeam;

import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.schine.resource.tag.Tag;

public class TractorBeamMetaDataDummy extends BlockMetaDataDummy {
   public TractorBeamHandler.TractorMode mode;

   protected void fromTagStructrePriv(Tag var1, int var2) {
      this.mode = TractorBeamHandler.TractorMode.values()[var1.getByte()];
   }

   public String getTagName() {
      return "TRM";
   }

   protected Tag toTagStructurePriv() {
      return new Tag(Tag.Type.BYTE, (String)null, (byte)this.mode.ordinal());
   }
}
