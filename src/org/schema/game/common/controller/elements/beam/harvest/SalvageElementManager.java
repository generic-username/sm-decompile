package org.schema.game.common.controller.elements.beam.harvest;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.beam.BeamElementManager;
import org.schema.game.common.controller.elements.combination.CombinationAddOn;
import org.schema.game.common.controller.elements.config.FloatReactorDualConfigElement;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;

public class SalvageElementManager extends BeamElementManager {
   @ConfigurationElement(
      name = "SalvageDamageNeededPerBlock"
   )
   public static float SALVAGE_DAMAGE_NEEDED_PER_BLOCK = 3.0F;
   @ConfigurationElement(
      name = "SalvageDamagePerHit"
   )
   public static FloatReactorDualConfigElement SALVAGE_DAMAGE_PER_HIT = new FloatReactorDualConfigElement();
   @ConfigurationElement(
      name = "TickRate"
   )
   public static float TICK_RATE = 100.0F;
   @ConfigurationElement(
      name = "PowerConsumptionPerTick"
   )
   public static float POWER_CONSUMPTION = 10.0F;
   @ConfigurationElement(
      name = "Distance"
   )
   public static float DISTANCE = 30.0F;
   @ConfigurationElement(
      name = "CoolDown"
   )
   public static float COOL_DOWN = 3.0F;
   @ConfigurationElement(
      name = "ReactorPowerConsumptionResting"
   )
   public static float REACTOR_POWER_CONSUMPTION_RESTING = 10.0F;
   @ConfigurationElement(
      name = "ReactorPowerConsumptionCharging"
   )
   public static float REACTOR_POWER_CONSUMPTION_CHARGING = 10.0F;
   @ConfigurationElement(
      name = "BurstTime"
   )
   public static float BURST_TIME = 3.0F;
   @ConfigurationElement(
      name = "InitialTicks"
   )
   public static float INITIAL_TICKS = 1.0F;
   @ConfigurationElement(
      name = "RailHitMultiplierParent"
   )
   public static double RAIL_HIT_MULTIPLIER_PARENT = 3.0D;
   @ConfigurationElement(
      name = "RailHitMultiplierChild"
   )
   public static double RAIL_HIT_MULTIPLIER_CHILD = 3.0D;

   public double getRailHitMultiplierParent() {
      return RAIL_HIT_MULTIPLIER_PARENT;
   }

   public double getRailHitMultiplierChild() {
      return RAIL_HIT_MULTIPLIER_CHILD;
   }

   public SalvageElementManager(SegmentController var1) {
      super((short)4, (short)24, var1);
   }

   public void updateActivationTypes(ShortOpenHashSet var1) {
      var1.add((short)24);
   }

   protected String getTag() {
      return "salvagebeam";
   }

   public SalvageBeamCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new SalvageBeamCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_HARVEST_SALVAGEELEMENTMANAGER_4;
   }

   protected void playSound(SalvageUnit var1, Transform var2) {
   }

   public ControllerManagerGUI getGUIUnitValues(SalvageUnit var1, SalvageBeamCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      float var5 = var1.getBeamPower();
      float var6 = var1.getTickRate();
      float var7 = var1.getDistance();
      var1.getPowerConsumption();
      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_HARVEST_SALVAGEELEMENTMANAGER_0, var1, new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_HARVEST_SALVAGEELEMENTMANAGER_1, StringTools.formatPointZero(var5)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_HARVEST_SALVAGEELEMENTMANAGER_2, StringTools.formatPointZeroZero(var6)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_HARVEST_SALVAGEELEMENTMANAGER_3, StringTools.formatPointZero(var7)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_HARVEST_SALVAGEELEMENTMANAGER_5, var1.getPowerConsumedPerSecondResting()), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_HARVEST_SALVAGEELEMENTMANAGER_7, var1.getPowerConsumedPerSecondCharging()));
   }

   public float getTickRate() {
      return TICK_RATE;
   }

   public float getCoolDown() {
      return COOL_DOWN;
   }

   public float getBurstTime() {
      return BURST_TIME;
   }

   public float getInitialTicks() {
      return INITIAL_TICKS;
   }

   public double getMiningScore() {
      return (double)this.totalSize;
   }

   public CombinationAddOn getAddOn() {
      return null;
   }
}
