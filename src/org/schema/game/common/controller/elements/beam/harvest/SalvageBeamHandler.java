package org.schema.game.common.controller.elements.beam.harvest;

import com.bulletphysics.collision.dispatch.CollisionObject;
import java.util.Collection;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.beam.BeamColors;
import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.Salvage;
import org.schema.game.common.controller.Salvager;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.controller.elements.power.reactor.StabilizerPath;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.beam.BeamHandler;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.schine.graphicsengine.core.Timer;

public class SalvageBeamHandler extends BeamHandler {
   public SalvageBeamHandler(Salvager var1, BeamHandlerContainer var2) {
      super((SegmentController)var1, var2);
   }

   public boolean canhit(BeamState var1, SegmentController var2, String[] var3, Vector3i var4) {
      return !var2.equals(this.getBeamShooter()) && var2 instanceof Salvage && ((Salvage)var2).isSalvagableFor((Salvager)this.getBeamShooter(), var3, var4);
   }

   public boolean ignoreBlock(short var1) {
      ElementInformation var2;
      return (var2 = ElementKeyMap.getInfoFast(var1)).isDrawnOnlyInBuildMode() && !var2.hasLod();
   }

   public float getBeamTimeoutInSecs() {
      return 0.05F;
   }

   public float getBeamToHitInSecs(BeamState var1) {
      return var1.getTickRate();
   }

   protected boolean isHitsCubesOnly() {
      return true;
   }

   protected boolean canHit(CollisionObject var1) {
      return !(var1.getUserPointer() instanceof StabilizerPath);
   }

   public int onBeamHit(BeamState var1, int var2, BeamHandlerContainer var3, SegmentPiece var4, Vector3f var5, Vector3f var6, Timer var7, Collection var8) {
      ((Salvager)this.getBeamShooter()).handleSalvage(var1, var2, var3, var5, var4, var7, var8);
      ((Salvage)var4.getSegmentController()).handleBeingSalvaged(var1, var3, var6, var4, var2);
      return var2;
   }

   protected boolean onBeamHitNonCube(BeamState var1, int var2, BeamHandlerContainer var3, Vector3f var4, Vector3f var5, CubeRayCastResult var6, Timer var7, Collection var8) {
      return false;
   }

   protected boolean ignoreNonPhysical(BeamState var1) {
      return false;
   }

   public Vector4f getDefaultColor(BeamState var1) {
      return getColorRange(BeamColors.BLUE);
   }
}
