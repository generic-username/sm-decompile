package org.schema.game.common.controller.elements;

import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.StateInterface;

public abstract class SegmentControllerUsable implements PlayerUsableInterface {
   public final SegmentController segmentController;
   public final ManagerContainer man;

   public SegmentControllerUsable(ManagerContainer var1) {
      this(var1, var1.getSegmentController());
   }

   public SegmentControllerUsable(ManagerContainer var1, SegmentController var2) {
      this.segmentController = var2;
      this.man = var1;
   }

   public SegmentController getSegmentController() {
      return this.segmentController;
   }

   public ManagerContainer getContainer() {
      return this.man;
   }

   public StateInterface getState() {
      return this.getSegmentController().getState();
   }

   public abstract String getWeaponRowName();

   public abstract short getWeaponRowIcon();

   public boolean isAddToPlayerUsable() {
      return true;
   }

   public int getMaxCharges() {
      return 0;
   }

   public int getCharges() {
      return 0;
   }

   public void onPlayerDetachedFromThisOrADock(ManagedUsableSegmentController var1, PlayerState var2, PlayerControllable var3) {
   }

   public void handleKeyEvent(ControllerStateUnit var1, KeyboardMappings var2) {
   }

   public float getWeaponSpeed() {
      return 0.0F;
   }

   public float getWeaponDistance() {
      return 0.0F;
   }

   public void handleMouseEvent(ControllerStateUnit var1, MouseEvent var2) {
   }

   public void onSwitched(boolean var1) {
   }

   public void onLogicActivate(SegmentPiece var1, boolean var2, Timer var3) {
   }
}
