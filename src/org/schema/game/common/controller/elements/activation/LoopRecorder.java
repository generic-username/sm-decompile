package org.schema.game.common.controller.elements.activation;

import it.unimi.dsi.fastutil.longs.LongLinkedOpenHashSet;

public class LoopRecorder {
   private LongLinkedOpenHashSet trail = new LongLinkedOpenHashSet();

   public boolean hasLoop(long var1) {
      return this.trail.contains(var1);
   }

   public void add(long var1) {
      this.trail.add(var1);
   }
}
