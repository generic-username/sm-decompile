package org.schema.game.common.controller.elements.activation;

import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.game.common.data.element.meta.weapon.MarkerBeam;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class ActivationDestMetaDataDummy extends BlockMetaDataDummy {
   public MarkerBeam dest;

   protected void fromTagStructrePriv(Tag var1, int var2) {
      Tag[] var3;
      if ((var3 = (Tag[])var1.getValue())[0].getType() == Tag.Type.STRUCT) {
         this.dest = MarkerBeam.getMarkingFromTag(var3[0], var2);
      }

   }

   public String getTagName() {
      return "ACD";
   }

   protected Tag toTagStructurePriv() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{this.dest.toTag(), FinishTag.INST});
   }
}
