package org.schema.game.common.controller.elements.activation;

import java.util.Iterator;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.SignalTrace;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;

public class AbstractUnit extends ElementCollection {
   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((ActivationElementManager)((ActivationCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (ActivationCollectionManager)this.elementCollectionManager, var2, var3);
   }

   public void onActivate(ActivationCollectionManager var1, ActivationElementManager var2, SegmentPiece var3, boolean var4) {
      Iterator var10 = this.getNeighboringCollection().iterator();

      SendableSegmentController.SignalQueue var12;
      do {
         if (!var10.hasNext()) {
            return;
         }

         long var6 = (Long)var10.next();
         boolean var11 = false;
         if (ElementKeyMap.isValidType(var3.getType()) && ElementKeyMap.getInfo(var3.getType()).controlsAll()) {
            var11 = true;
         }

         long var8;
         if (var4) {
            var8 = ElementCollection.getActivation(var6, var11, true);

            assert ElementCollection.getType(var8) > 10;
         } else {
            var8 = ElementCollection.getDeactivation(var6, var11, true);

            assert ElementCollection.getType(var8) > 10;
         }

         var12 = ((SendableSegmentController)this.getSegmentController()).signalQueue;
         SignalTrace var5;
         (var5 = ((SendableSegmentController)this.getSegmentController()).getSignalPool().get()).set(ElementCollection.getPosIndexFrom4(var8), var8, ((SendableSegmentController)this.getSegmentController()).currentTrace);
         var12.enqueue(var5);
      } while(!this.getSegmentController().isOnServer() || var12.size() <= (Integer)ServerConfig.MAX_LOGIC_SIGNAL_QUEUE_PER_OBJECT.getCurrentState());

      ((GameServerState)this.getSegmentController().getState()).getController().broadcastMessage(new Object[]{31, this.getSegmentController().getRealName(), this.getSegmentController().getSector(new Vector3i())}, 3);
      var12.clear();
   }
}
