package org.schema.game.common.controller.elements.combination.modifier;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.lang.reflect.Field;
import java.util.Locale;
import org.schema.common.config.ConfigParserException;
import org.schema.common.config.ConfigurationElement;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.combination.CombinationSettings;
import org.schema.game.common.controller.elements.combination.modifier.tagMod.BasicModifier;
import org.schema.game.common.controller.elements.combination.modifier.tagMod.formula.FloatBuffFormula;
import org.schema.game.common.controller.elements.combination.modifier.tagMod.formula.FloatFormula;
import org.schema.game.common.controller.elements.combination.modifier.tagMod.formula.FloatNervFomula;
import org.schema.game.common.controller.elements.combination.modifier.tagMod.formula.SetFomula;
import org.schema.game.common.data.element.ElementCollection;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class Modifier {
   public static final int OLD_POWER_INDEX = 1;
   public static final int NEW_POWER_REACTOR_INDEX = 0;
   public static final String OLD_POWER_VERSION = "noreactor";
   public boolean initialized;
   public boolean loadedDual;

   public abstract void handle(ElementCollection var1, ControlBlockElementCollectionManager var2, float var3);

   public abstract void calcCombiSettings(CombinationSettings var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3, float var4);

   public void load(Node var1, int var2) throws ConfigParserException, IllegalArgumentException, IllegalAccessException {
      NodeList var3 = var1.getChildNodes();
      Field[] var4 = this.getClass().getDeclaredFields();

      int var8;
      Node var9;
      label175:
      for(int var5 = 0; var5 < var3.getLength(); ++var5) {
         boolean var6 = false;
         Node var7 = var3.item(var5);

         for(var8 = 0; var8 < var3.getLength(); ++var8) {
            var9 = var3.item(var8);
            if (var7.getNodeType() == 1 && var5 != var8 && var7.getNodeName().toLowerCase(Locale.ENGLISH).equals(var9.getNodeName().toLowerCase(Locale.ENGLISH))) {
               var6 = true;
               this.loadedDual = true;
               break;
            }
         }

         boolean var22 = false;
         boolean var23 = false;
         int var10;
         ConfigurationElement var11;
         if (var7.getNodeType() == 1) {
            var22 = true;

            for(var10 = 0; var10 < var4.length; ++var10) {
               var4[var10].setAccessible(true);
               var4[var10].getAnnotations();
               if ((var11 = (ConfigurationElement)var4[var10].getAnnotation(ConfigurationElement.class)) != null && var7.getNodeName().toLowerCase(Locale.ENGLISH).equals(var11.name().toLowerCase(Locale.ENGLISH))) {
                  assert var7.getAttributes().getNamedItem("style") != null : var1.getAttributes();

                  String var24 = var7.getAttributes().getNamedItem("style").getNodeValue().toLowerCase(Locale.ENGLISH);
                  float var12 = 0.0F;
                  var23 = false;
                  boolean var13 = false;
                  boolean var14 = false;
                  Node var15;
                  if (var6) {
                     if ((var15 = var7.getAttributes().getNamedItem("version")) != null) {
                        if (var15.getNodeValue().toLowerCase(Locale.ENGLISH).equals("noreactor".toLowerCase(Locale.ENGLISH)) && var2 != 1) {
                           continue label175;
                        }
                     } else if (var2 == 1) {
                        continue label175;
                     }
                  }

                  if ((var15 = var7.getAttributes().getNamedItem("value")) != null) {
                     try {
                        var12 = Float.parseFloat(var15.getNodeValue().toLowerCase(Locale.ENGLISH));
                        var23 = true;
                     } catch (NumberFormatException var18) {
                        throw new ConfigParserException(var1.getParentNode().getParentNode().getNodeName() + "->" + var1.getParentNode().getNodeName() + "->" + var1.getNodeName() + " value=... item has to be float " + var1.getNodeName() + " -> " + var7.getNodeName() + ", but was '" + var15.getNodeValue() + "'", var18);
                     }
                  }

                  if ((var15 = var7.getAttributes().getNamedItem("inverse")) != null) {
                     try {
                        var13 = Boolean.parseBoolean(var15.getNodeValue().toLowerCase(Locale.ENGLISH));
                     } catch (NumberFormatException var17) {
                        throw new ConfigParserException(var1.getParentNode().getParentNode().getNodeName() + "->" + var1.getParentNode().getNodeName() + "->" + var1.getNodeName() + " inverse=... item has to be boolean " + var1.getNodeName() + " -> " + var7.getNodeName() + ", but was '" + var15.getNodeValue() + "'", var17);
                     }
                  }

                  if ((var15 = var7.getAttributes().getNamedItem("linear")) != null) {
                     try {
                        var14 = Boolean.parseBoolean(var15.getNodeValue().toLowerCase(Locale.ENGLISH));
                     } catch (NumberFormatException var16) {
                        throw new ConfigParserException(var1.getParentNode().getParentNode().getNodeName() + "->" + var1.getParentNode().getNodeName() + "->" + var1.getNodeName() + " linear=... item has to be boolean " + var1.getNodeName() + " -> " + var7.getNodeName() + ", but was '" + var15.getNodeValue() + "'", var16);
                     }
                  }

                  if (var24.equals("buff")) {
                     if (!var23) {
                        throw new ConfigParserException("for style '" + var24 + "', a 'value' attribute has not been found (not existing or misspelled) " + var1.getParentNode().getParentNode().getNodeName() + " -> " + var1.getParentNode().getNodeName() + " -> " + var1.getNodeName() + " -> " + var7.getNodeName());
                     }

                     var4[var10].set(this, new BasicModifier(var13, var12, var14, new FloatBuffFormula()));
                     var23 = true;
                  } else if (var24.equals("nerf")) {
                     if (!var23) {
                        throw new ConfigParserException("for style '" + var24 + "', a 'value' attribute has not been found (not existing or misspelled) " + var1.getParentNode().getParentNode().getNodeName() + " -> " + var1.getParentNode().getNodeName() + " -> " + var1.getNodeName() + " -> " + var7.getNodeName());
                     }

                     var4[var10].set(this, new BasicModifier(var13, var12, var14, new FloatNervFomula()));
                     var23 = true;
                  } else if (var24.equals("set")) {
                     if (!var23) {
                        throw new ConfigParserException("for style '" + var24 + "', a 'value' attribute has not been found (not existing or misspelled) " + var1.getParentNode().getParentNode().getNodeName() + " -> " + var1.getParentNode().getNodeName() + " -> " + var1.getNodeName() + " -> " + var7.getNodeName());
                     }

                     var4[var10].set(this, new BasicModifier(var13, var12, var14, new SetFomula()));
                     var23 = true;
                  } else {
                     if (!var24.equals("skip")) {
                        throw new ConfigParserException("style=... style-value is unknown: '" + var24 + "' (has to be either buff/nerf/set/skip) in " + var1.getParentNode().getParentNode().getNodeName() + " -> " + var1.getParentNode().getNodeName() + " -> " + var1.getNodeName() + " -> " + var7.getNodeName());
                     }

                     var4[var10].set(this, new BasicModifier(false, 0.0F, var14, (FloatFormula)null));
                     var23 = true;
                  }

                  assert var4[var10].get(this) != null : var4[var10].getName() + "; " + var24 + "; " + var12;
               }
            }
         }

         if (var22 && !var23) {
            for(var10 = 0; var10 < var4.length; ++var10) {
               var4[var10].setAccessible(true);
               var4[var10].getAnnotations();
               if ((var11 = (ConfigurationElement)var4[var10].getAnnotation(ConfigurationElement.class)) != null) {
                  System.err.println("FIELD: " + this.getClass().getSimpleName() + ": " + var4[var10].getName() + ": annotation: " + var11.name());
               }
            }

            throw new ConfigParserException("no modifier found in game for config-tag: " + var1.getParentNode().getParentNode().getNodeName() + " -> " + var1.getParentNode().getNodeName() + " -> " + var1.getNodeName() + " -> " + var7.getNodeName() + " (multi index: " + (var5 == 1 ? "OLD" : "NEW") + ")");
         }
      }

      ObjectOpenHashSet var19 = new ObjectOpenHashSet(var3.getLength());

      for(int var20 = 0; var20 < var4.length; ++var20) {
         var4[var20].setAccessible(true);
         ConfigurationElement var21;
         if ((var21 = (ConfigurationElement)var4[var20].getAnnotation(ConfigurationElement.class)) != null && var4[var20].get(this) == null) {
            var19.clear();

            for(var8 = 0; var8 < var3.getLength(); ++var8) {
               if ((var9 = var3.item(var8)).getNodeType() == 1) {
                  if (!var19.add(var9.getNodeName().toLowerCase(Locale.ENGLISH)) && var9.getAttributes().getNamedItem("version") != null) {
                     throw new ConfigParserException("Duplicate XML node '" + var9.getNodeName() + "': " + var21.name() + " (" + var1.getParentNode().getParentNode().getNodeName() + " -> " + var1.getParentNode().getNodeName() + " -> " + var1.getNodeName() + ")");
                  }

                  if (var9.getAttributes().getNamedItem("version") != null) {
                     var19.remove(var9.getNodeName().toLowerCase(Locale.ENGLISH));
                  }

                  System.err.println("XML NODE READ: " + var9.getNodeName());
               }
            }

            throw new ConfigParserException("Modifier not loaded: " + var21.name() + " (not found in " + var1.getParentNode().getParentNode().getNodeName() + " -> " + var1.getParentNode().getNodeName() + " -> " + var1.getNodeName() + ")");
         }
      }

      this.initialized = true;
   }

   public String toString() {
      StringBuffer var1 = new StringBuffer();
      Field[] var2 = this.getClass().getDeclaredFields();

      for(int var3 = 0; var3 < var2.length; ++var3) {
         var2[var3].setAccessible(true);
         if ((ConfigurationElement)var2[var3].getAnnotation(ConfigurationElement.class) != null) {
            try {
               var1.append(var2[var3].getName() + ": " + var2[var3].get(this).toString() + "; \n");
            } catch (IllegalArgumentException var4) {
               var4.printStackTrace();
            } catch (IllegalAccessException var5) {
               var5.printStackTrace();
            }
         }
      }

      return var1.toString();
   }
}
