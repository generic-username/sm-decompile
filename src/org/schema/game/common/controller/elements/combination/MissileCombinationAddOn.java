package org.schema.game.common.controller.elements.combination;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.config.ConfigurationElement;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.ShootingRespose;
import org.schema.game.common.controller.elements.beam.damageBeam.DamageBeamCollectionManager;
import org.schema.game.common.controller.elements.combination.modifier.MissileUnitModifier;
import org.schema.game.common.controller.elements.combination.modifier.Modifier;
import org.schema.game.common.controller.elements.combination.modifier.MultiConfigModifier;
import org.schema.game.common.controller.elements.missile.MissileUnit;
import org.schema.game.common.controller.elements.missile.dumb.DumbMissileCollectionManager;
import org.schema.game.common.controller.elements.missile.dumb.DumbMissileElementManager;
import org.schema.game.common.controller.elements.missile.dumb.DumbMissileUnit;
import org.schema.game.common.controller.elements.weapon.WeaponCollectionManager;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ShootContainer;
import org.schema.game.common.data.missile.updates.MissileSpawnUpdate;
import org.schema.game.common.data.physics.RigidBodySegmentController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Timer;

public class MissileCombinationAddOn extends CombinationAddOn {
   @ConfigurationElement(
      name = "cannon"
   )
   public static final MultiConfigModifier missileCannonUnitModifier = new MultiConfigModifier() {
      public final MissileUnitModifier instance() {
         return new MissileUnitModifier();
      }
   };
   @ConfigurationElement(
      name = "beam"
   )
   public static final MultiConfigModifier missileBeamUnitModifier = new MultiConfigModifier() {
      public final MissileUnitModifier instance() {
         return new MissileUnitModifier();
      }
   };
   @ConfigurationElement(
      name = "missile"
   )
   public static final MultiConfigModifier missileMissileUnitModifier = new MultiConfigModifier() {
      public final MissileUnitModifier instance() {
         return new MissileUnitModifier();
      }
   };

   public MissileCombinationAddOn(DumbMissileElementManager var1, GameStateInterface var2) {
      super(var1, var2);
   }

   public ShootingRespose handle(MissileUnitModifier var1, DumbMissileCollectionManager var2, DumbMissileUnit var3, ControlBlockElementCollectionManager var4, ControlBlockElementCollectionManager var5, ShootContainer var6, SimpleTransformableSendableObject var7) {
      float var20 = getRatio(var2, var4);
      var1.handle((MissileUnit)var3, var4, var20);
      Transform var19;
      (var19 = new Transform()).setIdentity();
      var19.origin.set(var6.weapontOutputWorldPos);
      long var10 = var2.getUsableId();
      long var12 = var2.getLightConnectedElement();
      short var21 = 0;
      if (var12 != Long.MIN_VALUE) {
         var21 = (short)ElementCollection.getType(var12);
      }

      float var8 = var1.outputSpeed;
      float var9 = var1.outputDistance;
      long var16 = (long)var1.outputReload;
      float var22 = var1.outputDamage;
      var3.setShotReloading(var16);
      if (var2.getSegmentController().isOnServer()) {
         for(int var18 = 0; var18 < var1.outputSplit; ++var18) {
            Vector3f var13 = new Vector3f(var6.shootingDirTemp);
            if (var18 > 1) {
               var13.normalize();
               var6.shootingUpTemp.normalize();
               var6.shootingRightTemp.normalize();
               var6.shootingUpTemp.scale((this.r.nextFloat() - 0.5F) * 0.5F);
               var6.shootingRightTemp.scale((this.r.nextFloat() - 0.5F) * 0.5F);
               var13.add(var6.shootingUpTemp);
               var13.add(var6.shootingRightTemp);
            }

            var13.normalize();
            var13.scale(var8);
            if (var1.outputMode == (float)MissileSpawnUpdate.MissileType.DUMB.configIndex) {
               ((DumbMissileElementManager)this.elementManager).getMissileController().addDumbMissile(var2.getSegmentController(), var19, var13, var8, var22 / (float)var1.outputSplit, var9, var10, var21);
            } else if (var1.outputMode == (float)MissileSpawnUpdate.MissileType.HEAT.configIndex) {
               ((DumbMissileElementManager)this.elementManager).getMissileController().addHeatMissile(var2.getSegmentController(), var19, var13, var8, var22 / (float)var1.outputSplit, var9, var10, var21);
            } else if (var1.outputMode == (float)MissileSpawnUpdate.MissileType.FAFO.configIndex) {
               ((DumbMissileElementManager)this.elementManager).getMissileController().addFafoMissile(var2.getSegmentController(), var19, var13, var8, var22 / (float)var1.outputSplit, var9, var10, var7, var21);
            } else {
               if (var1.outputMode != (float)MissileSpawnUpdate.MissileType.BOMB.configIndex) {
                  throw new RuntimeException("Unknown missile type: " + var1.outputMode);
               }

               float var23 = DumbMissileElementManager.BOMB_ACTIVATION_TIME_SEC;
               Vector3f var14 = new Vector3f();
               if (var2.getSegmentController().railController.getRoot().getPhysicsDataContainer().getObject() instanceof RigidBodySegmentController) {
                  ((RigidBodySegmentController)var2.getSegmentController().railController.getRoot().getPhysicsDataContainer().getObject()).getLinearVelocity(var14);
                  if (var14.lengthSquared() > 0.0F) {
                     var8 = var14.length();
                     var14.normalize();
                  } else {
                     var14.set(0.0F, 0.0F, 1.0F);
                     var8 = 0.0F;
                  }
               } else {
                  var14.set(0.0F, 0.0F, 1.0F);
                  var8 = 0.0F;
               }

               int var15 = 1 + (int)(var1.outputAdditionalCapacityUsedPerDamage / var1.outputDamage) * (int)var1.outputAdditionalCapacityUsedPerDamageMult;
               ((DumbMissileElementManager)this.elementManager).getMissileController().addBombMissile(var2.getSegmentController(), var19, var23, var14, var8, var22 / (float)var1.outputSplit, var9, var10, var15, var21);
            }
         }
      }

      return ShootingRespose.FIRED;
   }

   protected String getTag() {
      return "missile";
   }

   public ShootingRespose handleCannonCombi(DumbMissileCollectionManager var1, DumbMissileUnit var2, WeaponCollectionManager var3, ControlBlockElementCollectionManager var4, ShootContainer var5, SimpleTransformableSendableObject var6, PlayerState var7, Timer var8, float var9) {
      return this.handle((MissileUnitModifier)missileCannonUnitModifier.get((ElementCollectionManager)var3), var1, var2, var3, var4, var5, var6);
   }

   public ShootingRespose handleBeamCombi(DumbMissileCollectionManager var1, DumbMissileUnit var2, DamageBeamCollectionManager var3, ControlBlockElementCollectionManager var4, ShootContainer var5, SimpleTransformableSendableObject var6, PlayerState var7, Timer var8, float var9) {
      return this.handle((MissileUnitModifier)missileBeamUnitModifier.get((ElementCollectionManager)var3), var1, var2, var3, var4, var5, var6);
   }

   public ShootingRespose handleMissileCombi(DumbMissileCollectionManager var1, DumbMissileUnit var2, DumbMissileCollectionManager var3, ControlBlockElementCollectionManager var4, ShootContainer var5, SimpleTransformableSendableObject var6, PlayerState var7, Timer var8, float var9) {
      return this.handle((MissileUnitModifier)missileMissileUnitModifier.get((ElementCollectionManager)var3), var1, var2, var3, var4, var5, var6);
   }

   public Modifier getGUI(DumbMissileCollectionManager var1, DumbMissileUnit var2, WeaponCollectionManager var3, ControlBlockElementCollectionManager var4) {
      MissileUnitModifier var5;
      (var5 = (MissileUnitModifier)missileCannonUnitModifier.get((ElementCollectionManager)var3)).handle((MissileUnit)var2, var3, getRatio(var1, var3));
      return var5;
   }

   public Modifier getGUI(DumbMissileCollectionManager var1, DumbMissileUnit var2, DamageBeamCollectionManager var3, ControlBlockElementCollectionManager var4) {
      MissileUnitModifier var5;
      (var5 = (MissileUnitModifier)missileBeamUnitModifier.get((ElementCollectionManager)var3)).handle((MissileUnit)var2, var3, getRatio(var1, var3));
      return var5;
   }

   public Modifier getGUI(DumbMissileCollectionManager var1, DumbMissileUnit var2, DumbMissileCollectionManager var3, ControlBlockElementCollectionManager var4) {
      MissileUnitModifier var5;
      (var5 = (MissileUnitModifier)missileMissileUnitModifier.get((ElementCollectionManager)var3)).handle((MissileUnit)var2, var3, getRatio(var1, var3));
      return var5;
   }

   public double calcCannonCombiPowerConsumption(double var1, DumbMissileCollectionManager var3, DumbMissileUnit var4, WeaponCollectionManager var5, ControlBlockElementCollectionManager var6) {
      return ((MissileUnitModifier)missileCannonUnitModifier.get((ElementCollectionManager)var5)).calculatePowerConsumption(var1, var4, var5, getRatio(var3, var5));
   }

   public double calcBeamCombiPowerConsumption(double var1, DumbMissileCollectionManager var3, DumbMissileUnit var4, DamageBeamCollectionManager var5, ControlBlockElementCollectionManager var6) {
      return ((MissileUnitModifier)missileBeamUnitModifier.get((ElementCollectionManager)var5)).calculatePowerConsumption(var1, var4, var5, getRatio(var3, var5));
   }

   public double calcMissileCombiPowerConsumption(double var1, DumbMissileCollectionManager var3, DumbMissileUnit var4, DumbMissileCollectionManager var5, ControlBlockElementCollectionManager var6) {
      return ((MissileUnitModifier)missileMissileUnitModifier.get((ElementCollectionManager)var5)).calculatePowerConsumption(var1, var4, var5, getRatio(var3, var5));
   }

   public double calcCannonCombiReload(DumbMissileCollectionManager var1, DumbMissileUnit var2, WeaponCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ((MissileUnitModifier)missileCannonUnitModifier.get((ElementCollectionManager)var3)).calculateReload(var2, var3, getRatio(var1, var3));
   }

   public double calcBeamCombiReload(DumbMissileCollectionManager var1, DumbMissileUnit var2, DamageBeamCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ((MissileUnitModifier)missileBeamUnitModifier.get((ElementCollectionManager)var3)).calculateReload(var2, var3, getRatio(var1, var3));
   }

   public double calcMissileCombiReload(DumbMissileCollectionManager var1, DumbMissileUnit var2, DumbMissileCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ((MissileUnitModifier)missileMissileUnitModifier.get((ElementCollectionManager)var3)).calculateReload(var2, var3, getRatio(var1, var3));
   }

   public void calcCannonCombiSettings(MissileCombiSettings var1, DumbMissileCollectionManager var2, WeaponCollectionManager var3, ControlBlockElementCollectionManager var4) {
      ((MissileUnitModifier)missileCannonUnitModifier.get((ElementCollectionManager)var3)).calcCombiSettings((MissileCombiSettings)var1, var2, var3, getRatio(var2, var3));
   }

   public void calcBeamCombiSettings(MissileCombiSettings var1, DumbMissileCollectionManager var2, DamageBeamCollectionManager var3, ControlBlockElementCollectionManager var4) {
      ((MissileUnitModifier)missileBeamUnitModifier.get((ElementCollectionManager)var3)).calcCombiSettings((MissileCombiSettings)var1, var2, var3, getRatio(var2, var3));
   }

   public void calcMissileCombiPowerSettings(MissileCombiSettings var1, DumbMissileCollectionManager var2, DumbMissileCollectionManager var3, ControlBlockElementCollectionManager var4) {
      ((MissileUnitModifier)missileMissileUnitModifier.get((ElementCollectionManager)var3)).calcCombiSettings((MissileCombiSettings)var1, var2, var3, getRatio(var2, var3));
   }
}
