package org.schema.game.common.controller.elements.combination.modifier.tagMod;

public interface IntValueModifier extends ValueModifier {
   int getOuput(int var1);
}
