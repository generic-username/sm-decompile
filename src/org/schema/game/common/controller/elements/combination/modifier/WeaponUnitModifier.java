package org.schema.game.common.controller.elements.combination.modifier;

import org.schema.common.config.ConfigurationElement;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.damage.acid.AcidDamageFormula;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.combination.WeaponCombiSettings;
import org.schema.game.common.controller.elements.combination.modifier.tagMod.BasicModifier;
import org.schema.game.common.controller.elements.combination.modifier.tagMod.formula.SetFomula;
import org.schema.game.common.controller.elements.weapon.WeaponCollectionManager;
import org.schema.game.common.controller.elements.weapon.WeaponUnit;

public class WeaponUnitModifier extends Modifier {
   @ConfigurationElement(
      name = "Damage"
   )
   public BasicModifier damageModifier;
   @ConfigurationElement(
      name = "Reload"
   )
   public BasicModifier reloadModifier;
   @ConfigurationElement(
      name = "PowerConsumption"
   )
   public BasicModifier powerModifier;
   @ConfigurationElement(
      name = "Distance"
   )
   public BasicModifier distanceModifier;
   @ConfigurationElement(
      name = "Speed"
   )
   public BasicModifier speedModifier;
   @ConfigurationElement(
      name = "Recoil"
   )
   public BasicModifier recoilModifier;
   @ConfigurationElement(
      name = "ImpactForce"
   )
   public BasicModifier impactForceModifier;
   @ConfigurationElement(
      name = "AcidFormula"
   )
   public BasicModifier acidType;
   @ConfigurationElement(
      name = "ProjectileWidth"
   )
   public BasicModifier projectileWidthModifier;
   @ConfigurationElement(
      name = "ChargeMax"
   )
   public BasicModifier chargeMaxModifier;
   @ConfigurationElement(
      name = "ChargeSpeed"
   )
   public BasicModifier chargeSpeedModifier;
   @ConfigurationElement(
      name = "CursorRecoilX"
   )
   public BasicModifier cursorRecoilXModifier;
   @ConfigurationElement(
      name = "CursorRecoilMinX"
   )
   public BasicModifier cursorRecoilMinXModifier;
   @ConfigurationElement(
      name = "CursorRecoilMaxX"
   )
   public BasicModifier cursorRecoilMaxXModifier;
   @ConfigurationElement(
      name = "CursorRecoilDirX"
   )
   public BasicModifier cursorRecoilDirXModifier;
   @ConfigurationElement(
      name = "CursorRecoilY"
   )
   public BasicModifier cursorRecoilYModifier;
   @ConfigurationElement(
      name = "CursorRecoilMinY"
   )
   public BasicModifier cursorRecoilMinYModifier;
   @ConfigurationElement(
      name = "CursorRecoilMaxY"
   )
   public BasicModifier cursorRecoilMaxYModifier;
   @ConfigurationElement(
      name = "CursorRecoilDirY"
   )
   public BasicModifier cursorRecoilDirYModifier;
   @ConfigurationElement(
      name = "PossibleZoom"
   )
   public BasicModifier possibleZoomMod;
   @ConfigurationElement(
      name = "Aimable"
   )
   public BasicModifier aimableModifier;
   public float outputProjectileWidth;
   public float outputDamage;
   public float outputDistance;
   public float outputSpeed;
   public float outputReload;
   public float outputPowerConsumption;
   public float outputImpactForce;
   public float outputRecoil;
   public int outputAcidType;
   public boolean outputAimable;

   public void handle(WeaponUnit var1, ControlBlockElementCollectionManager var2, float var3) {
      this.outputDamage = this.damageModifier.getOuput(var1.getBaseDamage(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
      this.outputReload = this.reloadModifier.getOuput(var1.getReloadTimeMs(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
      float var4 = this.distanceModifier.getOuput(var1.getDistance(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
      this.outputDistance = this.distanceModifier.formulas instanceof SetFomula ? var4 * ((GameStateInterface)var1.getSegmentController().getState()).getGameState().getWeaponRangeReference() : var4;
      this.outputAimable = (int)this.aimableModifier.getOuput(var1.isAimable() ? 1.0F : 0.0F, var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3) != 0;
      this.outputSpeed = this.speedModifier.getOuput(var1.getSpeed(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
      this.outputImpactForce = (float)((int)this.impactForceModifier.getOuput(var1.getImpactForce(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3));
      this.outputPowerConsumption = this.powerModifier.getOuput(var1.getBasePowerConsumption() * var1.getExtraConsume(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
      this.outputRecoil = this.recoilModifier.getOuput(var1.getRecoil(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
      this.outputProjectileWidth = this.projectileWidthModifier.getOuput(var1.getProjectileWidth(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
      this.outputAcidType = (int)this.acidType.getOuput(1.0F, var1.size(), var2.getTotalSize(), 0, var3);
   }

   public double calculateReload(WeaponUnit var1, ControlBlockElementCollectionManager var2, float var3) {
      return (double)this.reloadModifier.getOuput(var1.getReloadTimeMs(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
   }

   public double calculatePowerConsumption(double var1, WeaponUnit var3, ControlBlockElementCollectionManager var4, float var5) {
      return (double)this.powerModifier.getOuput((float)(var1 * (double)var3.getExtraConsume()), var3.size(), var3.getCombiBonus(var4.getTotalSize()), var3.getEffectBonus(), var5);
   }

   public void calcCombiSettings(WeaponCombiSettings var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3, float var4) {
      int var5 = (int)this.acidType.getOuput(1.0F, var2.getTotalSize(), var3.getTotalSize(), 0, var4);
      var1.acidType = AcidDamageFormula.AcidFormulaType.values()[var5];
      var1.damageChargeMax = this.chargeMaxModifier.getOuput(((WeaponCollectionManager)var2).getDamageChargeMax(), var2.getTotalSize(), var3.getTotalSize(), 0, var4);
      var1.damageChargeSpeed = this.chargeSpeedModifier.getOuput(((WeaponCollectionManager)var2).getDamageChargeSpeed(), var2.getTotalSize(), var3.getTotalSize(), 0, var4);
      var1.cursorRecoilX = this.cursorRecoilXModifier.getOuput(((WeaponCollectionManager)var2).getCursorRecoilX(), var2.getTotalSize(), var3.getTotalSize(), 0, var4);
      var1.cursorRecoilMinX = this.cursorRecoilMinXModifier.getOuput(((WeaponCollectionManager)var2).getCursorRecoilMinX(), var2.getTotalSize(), var3.getTotalSize(), 0, var4);
      var1.cursorRecoilMaxX = this.cursorRecoilMaxXModifier.getOuput(((WeaponCollectionManager)var2).getCursorRecoilMaxX(), var2.getTotalSize(), var3.getTotalSize(), 0, var4);
      var1.cursorRecoilDirX = this.cursorRecoilDirXModifier.getOuput(((WeaponCollectionManager)var2).getCursorRecoilDirX(), var2.getTotalSize(), var3.getTotalSize(), 0, var4);
      var1.cursorRecoilY = this.cursorRecoilYModifier.getOuput(((WeaponCollectionManager)var2).getCursorRecoilY(), var2.getTotalSize(), var3.getTotalSize(), 0, var4);
      var1.cursorRecoilMinY = this.cursorRecoilMinYModifier.getOuput(((WeaponCollectionManager)var2).getCursorRecoilMinY(), var2.getTotalSize(), var3.getTotalSize(), 0, var4);
      var1.cursorRecoilMaxY = this.cursorRecoilMaxYModifier.getOuput(((WeaponCollectionManager)var2).getCursorRecoilMaxY(), var2.getTotalSize(), var3.getTotalSize(), 0, var4);
      var1.cursorRecoilDirY = this.cursorRecoilDirYModifier.getOuput(((WeaponCollectionManager)var2).getCursorRecoilDirY(), var2.getTotalSize(), var3.getTotalSize(), 0, var4);
      var1.possibleZoom = this.possibleZoomMod.getOuput(((WeaponCollectionManager)var2).getPossibleZoomRaw(), var2.getTotalSize(), var3.getTotalSize(), 0, var4);
   }
}
