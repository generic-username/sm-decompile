package org.schema.game.common.controller.elements.combination.modifier.tagMod.formula;

public abstract class Formula {
   private float maxBonus;
   private float ratio;
   private boolean inverse;
   private boolean linear;
   private int combisize;
   private int effectsize;
   private int masterSize;

   public float getMaxBonus() {
      return this.maxBonus;
   }

   public void setMaxBonus(float var1) {
      this.maxBonus = var1;
   }

   public float getRatio() {
      return this.ratio;
   }

   public void setRatio(float var1) {
      this.ratio = var1;
   }

   public boolean isInverse() {
      return this.inverse;
   }

   public void setInverse(boolean var1) {
      this.inverse = var1;
   }

   public boolean isLinear() {
      return this.linear;
   }

   public void setLinear(boolean var1) {
      this.linear = var1;
   }

   public int getCombisize() {
      return this.combisize;
   }

   public void setCombisize(int var1) {
      this.combisize = var1;
   }

   public int getEffectsize() {
      return this.effectsize;
   }

   public void setEffectsize(int var1) {
      this.effectsize = var1;
   }

   public int getMasterSize() {
      return this.masterSize;
   }

   public void setMasterSize(int var1) {
      this.masterSize = var1;
   }

   public String toString() {
      return this.getClass().getSimpleName();
   }
}
