package org.schema.game.common.controller.elements.combination.modifier.tagMod.formula;

public class LongBuffFormula extends LongFormula {
   public long getOuput(long var1) {
      float var3 = this.getRatio();
      float var4 = this.getMaxBonus();
      if (this.isInverse()) {
         var3 = 1.0F / (var3 * var4);
      } else {
         var3 *= var4;
      }

      return (long)((float)var1 * var3);
   }
}
