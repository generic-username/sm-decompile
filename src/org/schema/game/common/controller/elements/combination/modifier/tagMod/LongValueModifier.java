package org.schema.game.common.controller.elements.combination.modifier.tagMod;

public interface LongValueModifier extends ValueModifier {
   long getOuput(long var1);
}
