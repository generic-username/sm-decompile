package org.schema.game.common.controller.elements.combination.modifier.tagMod.formula;

import org.schema.game.common.controller.elements.combination.modifier.tagMod.FloatValueModifier;

public abstract class FloatFormula extends Formula implements FloatValueModifier {
   public abstract boolean needsRatio();
}
