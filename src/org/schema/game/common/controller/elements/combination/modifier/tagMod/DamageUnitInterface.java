package org.schema.game.common.controller.elements.combination.modifier.tagMod;

public interface DamageUnitInterface {
   float getDamage();

   float getBaseDamage();
}
