package org.schema.game.common.controller.elements.combination;

import org.schema.common.config.ConfigurationElement;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.elements.UsableElementManager;
import org.schema.game.common.controller.elements.beam.damageBeam.DamageBeamElementManager;
import org.schema.game.common.controller.elements.combination.modifier.BeamUnitModifier;
import org.schema.game.common.controller.elements.combination.modifier.MultiConfigModifier;

public class DamageBeamCombinationAddOn extends BeamCombinationAddOn {
   @ConfigurationElement(
      name = "cannon"
   )
   private static final MultiConfigModifier beamCannonUnitModifier = new MultiConfigModifier() {
      public final BeamUnitModifier instance() {
         return new BeamUnitModifier();
      }
   };
   @ConfigurationElement(
      name = "beam"
   )
   private static final MultiConfigModifier beamBeamUnitModifier = new MultiConfigModifier() {
      public final BeamUnitModifier instance() {
         return new BeamUnitModifier();
      }
   };
   @ConfigurationElement(
      name = "missile"
   )
   private static final MultiConfigModifier beamMissileUnitModifier = new MultiConfigModifier() {
      public final BeamUnitModifier instance() {
         return new BeamUnitModifier();
      }
   };

   public DamageBeamCombinationAddOn(DamageBeamElementManager var1, GameStateInterface var2) {
      super(var1, var2);
   }

   protected String getTag() {
      return "damagebeam";
   }

   protected BeamUnitModifier getBeamCannonUnitModifier() {
      return (BeamUnitModifier)beamCannonUnitModifier.get((UsableElementManager)this.elementManager);
   }

   protected BeamUnitModifier getBeamMissileUnitModifier() {
      return (BeamUnitModifier)beamMissileUnitModifier.get((UsableElementManager)this.elementManager);
   }

   protected BeamUnitModifier getBeamBeamUnitModifier() {
      return (BeamUnitModifier)beamBeamUnitModifier.get((UsableElementManager)this.elementManager);
   }
}
