package org.schema.game.common.controller.elements.combination.modifier;

import org.schema.common.config.ConfigParserException;
import org.schema.common.config.MultiConfigField;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.UsableElementManager;
import org.schema.game.common.controller.elements.config.ReactorDualConfigElement;
import org.w3c.dom.Node;

public abstract class MultiConfigModifier extends ReactorDualConfigElement implements MultiConfigField {
   private Modifier[] field;
   public boolean initialized;

   public MultiConfigModifier() {
      this.set(this.instance(), this.instance());
   }

   public Modifier get(int var1) {
      return this.field[var1];
   }

   public void set(Modifier... var1) {
      this.field = var1;
   }

   public abstract Modifier instance();

   public Modifier get(boolean var1) {
      return this.field[getIndex(var1)];
   }

   public Modifier get(ElementCollectionManager var1) {
      return this.get(var1.getSegmentController().isUsingPowerReactors());
   }

   public Modifier get(UsableElementManager var1) {
      return this.get(var1.getSegmentController().isUsingPowerReactors());
   }

   public void load(Node var1) throws IllegalArgumentException, IllegalAccessException, ConfigParserException {
      for(int var2 = 0; var2 < this.field.length; ++var2) {
         this.field[var2].loadedDual = false;
         this.field[var2].load(var1, var2);
      }

   }

   public boolean checkInitialized() {
      if (!this.initialized) {
         return false;
      } else {
         for(int var1 = 0; var1 < this.field.length; ++var1) {
            if (!this.field[var1].initialized) {
               return false;
            }
         }

         return true;
      }
   }

   public void setInitialized(boolean var1, boolean var2) {
      this.initialized = var1;
      if (var2) {
         for(int var3 = 0; var3 < this.field.length; ++var3) {
            this.field[var3].initialized = var1;
         }
      }

   }
}
