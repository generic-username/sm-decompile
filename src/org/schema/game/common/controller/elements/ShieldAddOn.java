package org.schema.game.common.controller.elements;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2LongOpenHashMap;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.effects.RaisingIndication;
import org.schema.game.client.view.effects.ShieldDrawer;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.damage.HitReceiverType;
import org.schema.game.common.controller.damage.HitType;
import org.schema.game.common.controller.damage.effects.InterEffectHandler;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.elements.power.PowerAddOn;
import org.schema.game.common.controller.rails.RailRelation;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.blockeffects.FastSegmentControllerStatus;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.world.SectorNotFoundException;
import org.schema.game.network.objects.remote.RemoteValueUpdate;
import org.schema.game.network.objects.valueUpdate.NTValueUpdateInterface;
import org.schema.game.network.objects.valueUpdate.ShieldExpectedValueUpdate;
import org.schema.game.network.objects.valueUpdate.ShieldRechargeValueUpdate;
import org.schema.game.network.objects.valueUpdate.ShieldValueUpdate;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.network.objects.remote.RemoteVector4f;
import org.schema.schine.network.objects.remote.Streamable;

public class ShieldAddOn implements HittableInterface, ManagerUpdatableInterface {
   private final SegmentController segmentController;
   public ShieldContainerInterface sc;
   Vector3i posTmp = new Vector3i();
   private double shields;
   private double getShieldCapacityHP;
   private double shieldRechargeRate;
   private float accPower;
   private double recovery;
   private double initialShields;
   private double shieldsBefore;
   private double recoveryOnZeroShields;
   private double nerf;
   private double lastPowerConsumption;
   private boolean transferredDamage;
   private boolean regenEnabled = true;
   private int clientExpectedSize = -1;
   private final ShieldLocalAddOn shieldLocalAddOn;
   public static final Vector4f shieldHitColor = new Vector4f(0.3F, 0.3F, 1.0F, 1.0F);

   public ShieldAddOn(ShieldContainerInterface var1, SegmentController var2) {
      this.sc = var1;
      this.segmentController = var2;
      var1.addUpdatable(this);
      this.shieldLocalAddOn = new ShieldLocalAddOn(var1, var2);
   }

   public static void main(String[] var0) {
      for(int var2 = 1; var2 < 1000000; ++var2) {
         int var1 = (int)Math.ceil(Math.max(1.0D, Math.log10((double)var2)));
         if (var2 % (int)Math.pow(10.0D, (double)(var1 - 1)) == 0) {
            System.err.println(var2 + "C: " + var1 + "; " + Math.pow((double)(var2 << 1), 0.39D) * 50.0D);
            System.err.println(var2 + "E: " + var1 + "; " + Math.pow((double)(var2 << 1), 0.8D) * 400.0D);
         }
      }

   }

   public double handleShieldHit(Damager var1, InterEffectSet var2, Vector3f var3, int var4, DamageDealerType var5, HitType var6, double var7, long var9) throws SectorNotFoundException {
      if (var1 != null && var1 instanceof SegmentController && this.segmentController.railController.isInAnyRailRelationWith((SegmentController)var1)) {
         return 0.0D;
      } else {
         InterEffectSet var11 = null;
         if (var1 != null) {
            var11 = var1.getAttackEffectSet(var9, var5);
         }

         double var12 = var7;
         if (var11 != null && var2 != null) {
            var12 = (double)InterEffectHandler.handleEffects((float)var7, var11, var2, var6, var5, HitReceiverType.SHIELD, (short)0);
         } else {
            System.err.println("[WARNING] Shield attacked without attack effect set " + this.getSegmentController());
         }

         double var14;
         if (this.isUsingLocalShields()) {
            return (var14 = this.getShieldLocalAddOn().handleShieldHit(var1, var3, var4, var5, var12, var9)) == var12 ? var7 : var14;
         } else {
            this.transferredDamage = false;
            if (this.getShields() <= 0.0D) {
               return this.checkRailIntercept(var7, var5, true, false);
            } else {
               var14 = this.getShields();
               this.onHit(0L, (short)0, (double)((int)var7), var5);
               double var16;
               if (this.getShields() <= 0.0D) {
                  if (var7 < var14) {
                     var16 = var14;
                  } else {
                     var16 = var7;
                  }
               } else {
                  var16 = 0.0D;
               }

               if (!this.sc.getSegmentController().isOnServer() && !GLFrame.isFinished() && this.sc.getSegmentController().isInClientRange()) {
                  GameClientState var18 = (GameClientState)this.sc.getState();
                  Transform var20;
                  (var20 = new Transform()).setIdentity();
                  var20.origin.set(var3);
                  FastSegmentControllerStatus var21 = ((SendableSegmentController)this.sc.getSegmentController()).getBlockEffectManager().status;
                  var7 *= (double)(1.0F - var21.shieldHarden);
                  if (this.transferredDamage) {
                     HudIndicatorOverlay.toDrawTexts.add(new RaisingIndication(var20, (int)var7 + " transfer", shieldHitColor.x, shieldHitColor.y, shieldHitColor.z, shieldHitColor.w));
                  } else {
                     HudIndicatorOverlay.toDrawTexts.add(new RaisingIndication(var20, String.valueOf((int)var7), shieldHitColor.x, shieldHitColor.y, shieldHitColor.z, shieldHitColor.w));
                  }

                  var18.getWorldDrawer().getExplosionDrawer().addExplosion(var3, 4.0F, var9);
                  if (!this.transferredDamage) {
                     ShieldDrawer var19;
                     if ((var19 = var18.getWorldDrawer().getShieldDrawerManager().get(this.getSegmentController())) != null) {
                        var19.addHitOld(var3, (float)var7);
                     } else {
                        System.err.println("[CLIENT] ERROR: shield drawer for " + this.getSegmentController() + " not initialized");
                     }
                  }
               }

               return var16;
            }
         }
      }
   }

   private double checkRailIntercept(double var1, DamageDealerType var3, boolean var4, boolean var5) {
      if (this.getSegmentController().railController.isDockedAndExecuted()) {
         if (this.shields > 0.0D && var1 >= this.shields) {
            double var8 = this.shields - 1.0D;
            this.shields = 1.0D;
            var1 -= var8;
         }

         SegmentController var10;
         if ((var10 = this.getSegmentController().railController.previous.rail.getSegmentController()) instanceof ManagedSegmentController && ((ManagedSegmentController)var10).getManagerContainer() instanceof ShieldContainerInterface) {
            return ((ShieldContainerInterface)((ManagedSegmentController)var10).getManagerContainer()).getShieldAddOn().checkRailDockingDamageRecursive(var1, var3, this.getSegmentController().railController.previous, var4, var5);
         }
      }

      return var1;
   }

   public double getInitialShields() {
      return this.initialShields;
   }

   public void setInitialShields(double var1) {
      this.initialShields = var1;
   }

   public double getNerf() {
      return this.nerf;
   }

   public double getRecovery() {
      return this.recovery;
   }

   public void sendShieldUpdate() {
      if (this.isUsingLocalShields()) {
         this.getShieldLocalAddOn().sendShieldUpdate((ShieldLocal)null);
      } else {
         assert this.getSegmentController().isOnServer();

         ShieldValueUpdate var1 = new ShieldValueUpdate();

         assert var1.getType() == ValueUpdate.ValTypes.SHIELD;

         var1.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer());
         ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
      }
   }

   public void sendShieldExpectedUpdate() {
      assert this.getSegmentController().isOnServer();

      ShieldExpectedValueUpdate var1 = new ShieldExpectedValueUpdate();

      assert var1.getType() == ValueUpdate.ValTypes.SHIELD_EXPECTED;

      var1.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   public void sendRegenEnabledUpdate() {
      if (this.isUsingLocalShields()) {
         this.getShieldLocalAddOn().sendRegenEnabledUpdate();
      } else {
         assert this.getSegmentController().isOnServer();

         ShieldRechargeValueUpdate var1 = new ShieldRechargeValueUpdate();

         assert var1.getType() == ValueUpdate.ValTypes.SHIELD_REGEN_ENABLED;

         var1.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer());
         ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
      }
   }

   public double getShieldCapacityWithoutInitial() {
      return this.getShieldCapacityHP;
   }

   public double getShieldCapacity() {
      double var1 = this.getShieldCapacityHP + (!(this.segmentController instanceof Ship) && !(this.segmentController instanceof SpaceStation) ? 0.0D : VoidElementManager.SHIELD_CAPACITY_INITIAL);
      return this.getSegmentController().getConfigManager().apply(StatusEffectType.SHIELD_CAPACITY, var1);
   }

   public double getShieldRechargeRate() {
      if (!this.regenEnabled) {
         return 0.0D;
      } else {
         double var1 = (this.shieldRechargeRate + VoidElementManager.SHIELD_RECHARGE_INITIAL) * (double)((SendableSegmentController)this.getSegmentController()).getBlockEffectManager().status.shieldRegenPercent;
         return this.getSegmentController().getConfigManager().apply(StatusEffectType.SHIELD_RECHARGE_RATE, var1);
      }
   }

   public void setShieldRechargeRate(long var1) {
      this.shieldRechargeRate = (double)var1;
   }

   public double getShields() {
      return this.shields;
   }

   public void setShields(double var1) {
      this.shields = var1;
   }

   public void onHit(double var1, DamageDealerType var3, boolean var4, boolean var5) {
      this.getSegmentController().getConfigManager().apply(StatusEffectType.SHIELD_DAMAGE_RESISTANCE, var3, var1);
      if (var1 > this.shields && this.getSegmentController().railController.isDockedAndExecuted()) {
         var1 = this.checkRailIntercept(var1, var3, var4, var5);
      }

      double var6 = this.shields;
      FastSegmentControllerStatus var8 = ((SendableSegmentController)this.sc.getSegmentController()).getBlockEffectManager().status;
      this.shields = Math.max(0.0D, this.shields - var1 * (double)(var5 ? 1.0F : 1.0F - var8.shieldHarden));
      if (var4) {
         this.useNormalRecovery();
      }

      if (this.shields == 0.0D) {
         this.onShieldsZero(var6);
      }

   }

   public void onHit(long var1, short var3, double var4, DamageDealerType var6) {
      this.onHit(var4, var6, true, false);
   }

   public void useNormalRecovery() {
      float var1 = (float)VoidElementManager.SHIELD_DIRECT_RECOVERY_TIME_IN_SEC;
      var1 = this.getSegmentController().getConfigManager().apply(StatusEffectType.SHIELD_UNDER_FIRE_TIMEOUT, var1);
      this.recovery = (double)var1;
   }

   public void onShieldsZero(double var1) {
      if (this.shields == 0.0D && this.getShieldCapacity() > 0.0D) {
         float var3 = (float)VoidElementManager.SHIELD_RECOVERY_TIME_IN_SEC;
         var3 = this.getSegmentController().getConfigManager().apply(StatusEffectType.SHIELD_UNDER_FIRE_TIMEOUT, var3);
         this.recoveryOnZeroShields = (double)var3;
         if (this.getSegmentController().isClientOwnObject()) {
            ((GameClientState)this.getSegmentController().getState()).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIELDADDON_0, VoidElementManager.SHIELD_RECOVERY_TIME_IN_SEC), 0.0F);
            return;
         }

         if (this.getSegmentController().isOnServer() && var1 > 0.0D) {
            this.sendShieldUpdate();
         }
      }

   }

   private double checkRailDockingDamageRecursive(double var1, DamageDealerType var3, RailRelation var4, boolean var5, boolean var6) {
      while($assertionsDisabled || VoidElementManager.SHIELD_DOCK_TRANSFER_LIMIT > 0.0D) {
         if ((double)this.getPercentOne() > VoidElementManager.SHIELD_DOCK_TRANSFER_LIMIT && this.getShields() > var1) {
            this.onHit(var1, var3, var5, var6);
            this.transferredDamage = true;
            if (!this.sc.getSegmentController().isOnServer() && !GLFrame.isFinished() && this.sc.getSegmentController().isInClientRange()) {
               ShieldDrawer var10;
               if ((var10 = ((GameClientState)this.sc.getState()).getWorldDrawer().getShieldDrawerManager().get(this.getSegmentController())) != null) {
                  Vector3f var8;
                  Vector3f var9 = var8 = var4.rail.getAbsolutePos(new Vector3f());
                  var9.x -= 16.0F;
                  var8.y -= 16.0F;
                  var8.z -= 16.0F;
                  this.getSegmentController().getWorldTransformOnClient().transform(var8);
                  var10.addHitOld(var8, (float)var1);
               } else {
                  System.err.println("[CLIENT] ERROR: shield drawer for " + this.getSegmentController() + " not initialized");
               }
            }

            return 0.0D;
         }

         SegmentController var7;
         if (!this.getSegmentController().railController.isDockedAndExecuted() || !((var7 = this.getSegmentController().railController.previous.rail.getSegmentController()) instanceof ManagedSegmentController) || !(((ManagedSegmentController)var7).getManagerContainer() instanceof ShieldContainerInterface)) {
            return var1;
         }

         ShieldAddOn var10000 = ((ShieldContainerInterface)((ManagedSegmentController)var7).getManagerContainer()).getShieldAddOn();
         var4 = this.getSegmentController().railController.previous;
         var3 = var3;
         var1 = var1;
         this = var10000;
      }

      throw new AssertionError();
   }

   public int getExpectedShieldSize() {
      return this.sc.getShieldCapacityManager().expected;
   }

   public void update(Timer var1) {
      if (((ManagedSegmentController)this.getSegmentController()).getManagerContainer().isRequestedInitalValuesIfNeeded()) {
         if (this.isUsingLocalShields()) {
            this.getShieldLocalAddOn().update(var1);
         } else if (this.shields >= this.getShieldCapacity() && this.initialShields > 0.0D) {
            int var6;
            if (this.getSegmentController().isOnServer()) {
               if (this.getSegmentController() instanceof Ship && ((Ship)this.getSegmentController()).getSpawnedFrom() != null) {
                  var6 = ((Ship)this.getSegmentController()).getSpawnedFrom().getElementMap().get((short)3);
               } else {
                  var6 = this.sc.getShieldCapacityManager().expected;
               }
            } else {
               var6 = this.clientExpectedSize;
            }

            if (var6 >= 0 && this.sc.getShieldCapacityManager().getTotalSize() >= var6) {
               System.err.println("[SHIELDS] " + this.getSegmentController() + " CHECKING against bb: " + var6 + " / " + this.sc.getShieldCapacityManager().getTotalSize() + "; limit reached. setting cap to prevent overcharge");
               this.initialShields = 0.0D;
               this.shields = this.getShieldCapacity();
               if (this.getSegmentController().isOnServer()) {
                  this.sendShieldExpectedUpdate();
                  this.sendShieldUpdate();
               }
            }

         } else if (!this.sc.getShieldRegenManager().getElementCollections().isEmpty() || VoidElementManager.SHIELD_INITIAL_CORE) {
            double var2;
            double var4;
            if (this.initialShields > 0.0D && this.shields < this.getShieldCapacity()) {
               var2 = this.getShieldCapacity() - this.shields;
               var4 = Math.min(this.initialShields, var2);
               this.shields += var4;
               this.initialShields -= var4;
            }

            this.nerf = 1.0D;
            if (this.recovery > 0.0D) {
               this.recovery = Math.max(0.0D, this.recovery - (double)var1.getDelta());
               var2 = this.shields / this.getShieldCapacity();
               this.nerf = var2;
               this.nerf *= (double)VoidElementManager.SHIELD_RECOVERY_NERF_MULT_PER_PERCENT;
               this.nerf = this.getSegmentController().getConfigManager().apply(StatusEffectType.SHIELD_UNDER_FIRE_RECHARGE_NERF, this.nerf);
               this.nerf = 1.0D - this.nerf;
               this.nerf *= (double)VoidElementManager.SHIELD_RECOVERY_NERF_MULT;
            }

            if (this.segmentController.getHpController().isRebooting()) {
               this.shields = 0.0D;
            } else if (this.recoveryOnZeroShields > 0.0D) {
               this.recoveryOnZeroShields = Math.max(0.0D, this.recoveryOnZeroShields - (double)var1.getDelta());
            } else {
               if (this.accPower < 0.0F) {
                  this.accPower = 0.0F;
               }

               this.accPower += var1.getDelta();
               if (this.accPower > 10.0F) {
                  this.accPower = 10.0F;
               }

               var2 = VoidElementManager.SHIELD_RECHARGE_CYCLE_TIME;
               if ((double)this.accPower >= var2) {
                  this.rechargeCycle(var2);
                  this.accPower = (float)((double)this.accPower - var2);
               }

               if (this.getSegmentController().isOnServer() && this.shieldsBefore != this.shields) {
                  var4 = this.getShieldCapacity() / 100.0D;
                  if (Math.abs(this.shieldsBefore - this.shields) >= var4 * ((Integer)ServerConfig.BROADCAST_SHIELD_PERCENTAGE.getCurrentState()).doubleValue() || this.shields == 0.0D || this.shields == this.getShieldCapacity()) {
                     this.sendShieldUpdate();
                     this.shieldsBefore = this.shields;
                  }
               }

            }
         }
      }
   }

   public void incShields(double var1) {
      this.shields = Math.min(this.getShieldCapacity(), this.shields + var1);
   }

   private void rechargeCycle(double var1) {
      double var3;
      if (this.shields >= this.getShieldCapacity()) {
         var3 = (double)VoidElementManager.SHIELD_FULL_POWER_CONSUMPTION;
      } else {
         var3 = (double)VoidElementManager.SHIELD_RECHARGE_POWER_CONSUMPTION;
      }

      var3 *= this.getShieldRechargeRate() * this.nerf * var1;
      PowerAddOn var5;
      double var6;
      if ((var5 = ((ShieldContainerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getPowerAddOn()).getPower() < var3 && !this.getSegmentController().getDockingController().isDocked()) {
         if ((var6 = var5.getPower()) > 0.0D) {
            this.lastPowerConsumption = var6;
            var5.consumePowerInstantly(var6);
            double var8 = var6 / var3 * this.getShieldRechargeRate() * this.nerf * var1;
            this.shields = Math.min(this.getShieldCapacity(), this.shields + var8);
         }

      } else {
         this.lastPowerConsumption = var3;
         if (var5.consumePowerInstantly(var3)) {
            var6 = this.getShieldRechargeRate() * this.nerf * var1;
            this.shields = Math.min(this.getShieldCapacity(), this.shields + var6);
         }

      }
   }

   public boolean isUsingPowerReactors() {
      return this.sc.isUsingPowerReactors();
   }

   public SegmentController getSegmentController() {
      return this.sc.getSegmentController();
   }

   public void setShieldCapacityHP(double var1) {
      this.getShieldCapacityHP = var1;
   }

   public double getRecoveryOut() {
      return this.recoveryOnZeroShields;
   }

   public void setRecoveryOut(double var1) {
      this.recoveryOnZeroShields = var1;
   }

   public String getShieldString() {
      if (EngineSettings.G_SHOW_PURE_NUMBERS_FOR_SHIELD_AND_POWER.isOn()) {
         return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIELDADDON_1, StringTools.formatPointZero(this.getShields()), StringTools.formatPointZero(this.getShieldCapacity()), this.getInitialShields());
      } else {
         return this.getShieldCapacity() > 0.0D ? StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIELDADDON_2, StringTools.formatPointZero(this.getShields() / this.getShieldCapacity() * 100.0D), this.getInitialShields() > 0.0D ? "(+)" : "") : "";
      }
   }

   public void flagCompleteLoad() {
   }

   public double getLastPowerConsumption() {
      return this.lastPowerConsumption;
   }

   public float getPercentOne() {
      return (float)(this.getShields() / this.getShieldCapacity());
   }

   public boolean isRegenEnabled() {
      return this.regenEnabled;
   }

   public void setRegenEnabled(boolean var1) {
      this.regenEnabled = var1;
      if (this.getSegmentController().isOnServer()) {
         this.sendRegenEnabledUpdate();
      }

   }

   public boolean isUsingLocalShields() {
      return this.segmentController.isUsingPowerReactors();
   }

   public void sendShieldHit(Vector3f var1, float var2) {
      assert this.getSegmentController().isOnServer();

      this.getSegmentController().getNetworkObject().shieldHits.add((Streamable)(new RemoteVector4f(var1, var2, this.getSegmentController().isOnServer())));
   }

   public double getShieldsRail() {
      return this.segmentController.railController.getShieldsRecursive();
   }

   public void fillShieldsMapRecursive(Vector3f var1, int var2, Int2DoubleOpenHashMap var3, Int2DoubleOpenHashMap var4, Int2DoubleOpenHashMap var5, Int2IntOpenHashMap var6, Int2IntOpenHashMap var7, Int2LongOpenHashMap var8) throws SectorNotFoundException {
      this.segmentController.railController.fillShieldsMapRecursive(var1, var2, var3, var4, var5, var6, var7, var8);
   }

   public void setExpectedShieldClient(int var1) {
      this.clientExpectedSize = var1;
   }

   public int updatePrio() {
      return 1;
   }

   public ShieldLocalAddOn getShieldLocalAddOn() {
      return this.shieldLocalAddOn;
   }

   public boolean isUsingLocalShieldsAtLeastOneActive() {
      return this.isUsingLocalShields() && this.getShieldLocalAddOn().isAtLeastOneActive();
   }

   public boolean canUpdate() {
      return true;
   }

   public void onNoUpdate(Timer var1) {
   }
}
