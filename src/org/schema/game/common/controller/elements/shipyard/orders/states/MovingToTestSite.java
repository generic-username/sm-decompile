package org.schema.game.common.controller.elements.shipyard.orders.states;

import com.bulletphysics.linearmath.Transform;
import java.io.IOException;
import java.util.Iterator;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.shipyard.orders.ShipyardEntityState;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.objects.Sendable;

public class MovingToTestSite extends ShipyardState {
   private SegmentController currentDocked;
   private PlayerState playerState;
   private Sector sec;
   private long secLoaded;
   private boolean moved;

   public MovingToTestSite(ShipyardEntityState var1) {
      super(var1);
   }

   public boolean onEnterS() {
      this.currentDocked = this.getEntityState().getCurrentDocked();
      this.playerState = null;
      this.sec = null;
      this.moved = false;
      Sendable var1;
      if ((var1 = (Sendable)((GameServerState)this.getEntityState().getState()).getLocalAndRemoteObjectContainer().getLocalObjects().get(this.getEntityState().playerStateSent)) != null && var1 instanceof PlayerState) {
         this.playerState = (PlayerState)var1;
      }

      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      if (this.playerState == null) {
         this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_MOVINGTOTESTSITE_0);
         this.stateTransition(Transition.SY_ERROR);
         if (this.currentDocked != null && !this.currentDocked.isVirtualBlueprint()) {
            System.err.println("[SERVER][SHIPYARD] no player: Removing real spawn");
            this.currentDocked.setMarkedForDeletePermanentIncludingDocks(true);
         }
      } else if (this.currentDocked != null) {
         if (this.currentDocked.isVirtualBlueprint()) {
            this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_MOVINGTOTESTSITE_1);
            this.stateTransition(Transition.SY_ERROR);
         } else if (this.currentDocked.isFullyLoadedWithDock()) {
            if (this.currentDocked.railController.isDockedAndExecuted()) {
               this.currentDocked.railController.disconnect();
            } else if (this.currentDocked.getOwnerState() != null) {
               System.err.println("[SERVER][SHIPYARD] someone is in ship: Removing real spawn");
               this.currentDocked.setMarkedForDeletePermanentIncludingDocks(true);
               this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_MOVINGTOTESTSITE_2);
               this.stateTransition(Transition.SY_ERROR);
            } else if (this.playerState.getFirstControlledTransformableWOExc() != null && this.playerState.getFirstControlledTransformableWOExc() instanceof PlayerCharacter) {
               try {
                  if (this.sec == null) {
                     this.sec = ((GameServerState)this.getEntityState().getState()).getUniverse().getSector(this.playerState.testSector);
                     this.sec.noEnter(true);
                     this.sec.noExit(true);
                     this.secLoaded = System.currentTimeMillis();
                  } else if (System.currentTimeMillis() - this.secLoaded < 500L) {
                     this.sec.setActive(true);
                  } else {
                     this.sec.setActive(true);

                     assert !this.moved;

                     boolean var1 = false;
                     Iterator var2 = this.getEntityState().getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

                     while(var2.hasNext()) {
                        Sendable var3;
                        if ((var3 = (Sendable)var2.next()) instanceof SimpleTransformableSendableObject && ((SimpleTransformableSendableObject)var3).getSectorId() == this.sec.getId()) {
                           ((SimpleTransformableSendableObject)var3).markForPermanentDelete(true);
                           ((SimpleTransformableSendableObject)var3).setMarkedForDeleteVolatile(true);
                           System.err.println("[SERVER][MOVINGTOTESTSITE] cleaning up test sector " + var3);
                           var1 = true;
                           break;
                        }
                     }

                     if (!var1) {
                        this.moved = true;
                        this.playerState.instantiateInventoryServer(false);
                        this.playerState.spawnData.preSpecialSectorTransform = new Transform(this.playerState.getFirstControlledTransformableWOExc().getWorldTransform());
                        this.playerState.spawnData.preSpecialSector = new Vector3i(this.playerState.getCurrentSector());
                        ((GameServerState)this.getEntityState().getState()).getController().queueSectorSwitch(this.currentDocked, this.sec.pos, 1, false, true, true);
                        ((GameServerState)this.getEntityState().getState()).getController().queueSectorSwitch(this.playerState.getFirstControlledTransformableWOExc(), this.sec.pos, 1, false, true, true);
                        this.sec = null;
                        if (this.getEntityState().lastConstructedDesign != null) {
                           this.getEntityState().designToLoad = this.getEntityState().lastConstructedDesign.getId();
                           this.getEntityState().lastConstructedDesign = null;
                        }

                        this.stateTransition(Transition.SY_MOVING_TO_TEST_SITE_DONE);
                     }
                  }
               } catch (IOException var4) {
                  var4.printStackTrace();
                  this.currentDocked.setMarkedForDeletePermanentIncludingDocks(true);
                  this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_MOVINGTOTESTSITE_3);
                  System.err.println("[SERVER][SHIPYARD] sector load failed: Removing real spawn");
                  this.stateTransition(Transition.SY_ERROR);
               }
            } else {
               this.currentDocked.setMarkedForDeletePermanentIncludingDocks(true);
               this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_MOVINGTOTESTSITE_4);
               System.err.println("[SERVER][SHIPYARD] player is in any ship: Removing real spawn");
               this.stateTransition(Transition.SY_ERROR);
            }
         }
      } else {
         this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_MOVINGTOTESTSITE_5);
         this.stateTransition(Transition.SY_ERROR);
      }

      return false;
   }
}
