package org.schema.game.common.controller.elements.shipyard.orders.states;

import java.util.Iterator;
import org.schema.common.LogUtil;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.shipyard.orders.ShipyardEntityState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.objects.Sendable;

public class DesignLoaded extends ShipyardState {
   private long lastCheckForCollision;
   private boolean checkSectorOverlap;

   public DesignLoaded(ShipyardEntityState var1) {
      super(var1);
   }

   public boolean onEnterS() {
      this.getEntityState().getShipyardCollectionManager().setCompletionOrderPercentAndSendIfChanged(0.0D);
      this.checkSectorOverlap = true;
      this.lastCheckForCollision = System.currentTimeMillis();
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      if (!this.getEntityState().getShipyardCollectionManager().isLoadedDesignValid()) {
         System.err.println("[SERVER] Currently loaded design not valid. UNLOADING " + this.getEntityState().getCurrentDesign() + " REASON: " + this.getEntityState().getShipyardCollectionManager().isLoadedDesignValidToSring());
         LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": CURRENT DESIGN NO LONGER VALID: " + this.getEntityState().getCurrentDesign() + "; " + this.getEntityState().getShipyardCollectionManager().isLoadedDesignValidToSring());
         this.stateTransition(Transition.SY_UNLOAD_DESIGN);
         return false;
      } else {
         if (System.currentTimeMillis() - this.lastCheckForCollision < 600L) {
            SegmentController var1;
            boolean var2 = (var1 = this.getEntityState().getCurrentDocked()).railController.getCollisionChecker().checkPotentialCollisionWithRail(var1, (SegmentController[])null, true);
            LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": loaded design checking for collision: " + var2);
            if (!var2 && this.checkSectorOverlap) {
               var2 = var1.getCollisionChecker().checkSectorCollisionWithChildShapeExludingRails();
               if (var1.railController.isFullyLoadedRecursive()) {
                  this.checkSectorOverlap = false;
                  Iterator var3 = var1.getState().getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

                  while(var3.hasNext()) {
                     Sendable var4;
                     if ((var4 = (Sendable)var3.next()) instanceof SegmentController && !((SegmentController)var4).railController.isDockedAndExecuted() && ((SegmentController)var4).getSectorId() == var1.getSectorId() && !((SegmentController)var4).railController.isFullyLoadedRecursive()) {
                        this.checkSectorOverlap = true;
                        break;
                     }
                  }
               }
            }

            if (var2) {
               LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": removing design sicne it overlaps with structure");
               this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_DESIGNLOADED_0);
               this.stateTransition(Transition.SY_UNLOAD_DESIGN);
            }

            this.lastCheckForCollision = System.currentTimeMillis();
         }

         return false;
      }
   }

   public String getClientShortDescription() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_DESIGNLOADED_1;
   }

   public boolean canEdit() {
      return true;
   }
}
