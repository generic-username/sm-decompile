package org.schema.game.common.controller.elements.shipyard;

import com.bulletphysics.linearmath.Transform;
import java.util.Iterator;
import org.schema.common.config.ConfigurationElement;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.NTReceiveInterface;
import org.schema.game.common.controller.elements.NTSenderInterface;
import org.schema.game.common.controller.elements.TagModuleUsableInterface;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.controller.elements.factory.CargoCapacityElementManagerInterface;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.NetworkObject;

public class ShipyardElementManager extends UsableControllableElementManager implements NTReceiveInterface, NTSenderInterface, TagModuleUsableInterface, CargoCapacityElementManagerInterface {
   public static final String TAG_ID = "SYRD";
   @ConfigurationElement(
      name = "ConstructionTickInSeconds"
   )
   public static double CONSTRUCTION_TICK_IN_SECONDS = 1.0D;
   @ConfigurationElement(
      name = "ConstructionBlocksTakenPerTick"
   )
   public static int CONSTRUCTION_BLOCKS_TAKEN_PER_TICK = 1;
   @ConfigurationElement(
      name = "DeconstructionTimePerBlockInMilliseconds"
   )
   public static double DECONSTRUCTION_MS_PER_BLOCK = 0.01D;
   @ConfigurationElement(
      name = "DeconstructionConstantTimeInMilliseconds"
   )
   public static int DECONSTRUCTION_CONST_TIME_MS = 1;
   @ConfigurationElement(
      name = "PowerNeededPerShipyardBlock"
   )
   public static float POWER_COST_NEEDED_PER_BLOCK = 50.0F;
   @ConfigurationElement(
      name = "ShipyardArcMaxSpacing"
   )
   public static int ARC_MAX_SPACING = 16;
   @ConfigurationElement(
      name = "ReactorPowerConsumptionResting"
   )
   public static float REACTOR_POWER_CONSUMPTION_RESTING = 0.0F;
   @ConfigurationElement(
      name = "ReactorPowerConsumptionCharging"
   )
   public static float REACTOR_POWER_CONSUMPTION_CHARGING = 10.0F;

   public ShipyardElementManager(SegmentController var1) {
      super((short)677, (short)678, var1);
   }

   public void updateFromNT(NetworkObject var1) {
   }

   public void updateToFullNT(NetworkObject var1) {
      if (this.getSegmentController().isOnServer()) {
         this.sendAllShipyardStatesToClient();
      }

   }

   private void sendAllShipyardStatesToClient() {
      Iterator var1 = this.getCollectionManagers().iterator();

      while(var1.hasNext()) {
         ((ShipyardCollectionManager)var1.next()).sendShipyardStateToClient();
      }

   }

   public ControllerManagerGUI getGUIUnitValues(ShipyardUnit var1, ShipyardCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDELEMENTMANAGER_0, var1);
   }

   public boolean canHandle(ControllerStateInterface var1) {
      return false;
   }

   protected String getTag() {
      return "shipyard";
   }

   public ShipyardCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new ShipyardCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDELEMENTMANAGER_1;
   }

   protected void playSound(ShipyardUnit var1, Transform var2) {
      ((GameClientController)this.getState().getController()).queueTransformableAudio("0022_spaceship user - laser gun single fire small", var2, 0.99F);
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
   }

   public String getTagId() {
      return "SYRD";
   }

   public boolean isValidShipYard(SegmentPiece var1) {
      return true;
   }

   public BlockMetaDataDummy getDummyInstance() {
      return new ShipyardMetaDataDummy();
   }
}
