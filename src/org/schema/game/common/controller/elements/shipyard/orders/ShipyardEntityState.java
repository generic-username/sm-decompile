package org.schema.game.common.controller.elements.shipyard.orders;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.vecmath.Vector3f;
import org.schema.common.LogUtil;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.InventoryMap;
import org.schema.game.common.controller.elements.shipyard.ShipyardCollectionManager;
import org.schema.game.common.controller.rails.RailRelation;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.VirtualBlueprintMetaItem;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventoryChangeMap;
import org.schema.game.common.data.player.inventory.InventorySlot;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;
import org.schema.schine.ai.stateMachines.AiEntityState;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class ShipyardEntityState extends AiEntityState {
   private static final byte VERSION = 3;
   private final ShipyardCollectionManager shipyardCollectionManager;
   public ElementCountMap currentMapFrom = new ElementCountMap();
   public ElementCountMap currentMapTo = new ElementCountMap();
   public String currentName;
   public BlueprintClassification currentClassification;
   public int designToLoad = -1;
   public String currentBlueprintName;
   public boolean spawnDesignWithoutBlocks;
   public int playerStateSent;
   private Tag invs;
   public boolean isInRepair;
   public boolean wasUndockedManually;
   public int lastDeconstructedBlockCount;
   public int designToBlueprintOwner;
   public int lastOrderFactionId;
   public VirtualBlueprintMetaItem lastConstructedDesign;

   public ShipyardEntityState(String var1, ShipyardCollectionManager var2, StateInterface var3) {
      super(var1, var3);
      this.shipyardCollectionManager = var2;
   }

   public SegmentController getSegmentController() {
      return this.shipyardCollectionManager.getSegmentController();
   }

   public ShipyardCollectionManager getShipyardCollectionManager() {
      return this.shipyardCollectionManager;
   }

   public SegmentController getCurrentDocked() {
      return this.shipyardCollectionManager.getCurrentDocked();
   }

   public Inventory getInventory() {
      return this.shipyardCollectionManager.getInventory();
   }

   public VirtualBlueprintMetaItem getCurrentDesign() {
      return this.shipyardCollectionManager.getCurrentDesignObject();
   }

   public boolean isLoadedDesignValid() {
      return this.shipyardCollectionManager.isLoadedDesignValid();
   }

   public void sendShipyardStateToClient() {
      this.shipyardCollectionManager.sendShipyardStateToClient();
   }

   public void sendShipyardErrorToClient(String var1) {
      this.shipyardCollectionManager.sendShipyardErrorToClient(var1);
   }

   public void sendShipyardCommandToServer(int var1, ShipyardCollectionManager.ShipyardCommandType var2, Object... var3) {
      this.shipyardCollectionManager.sendShipyardCommandToServer(var1, var2, var3);
   }

   public void handle(ShipyardCollectionManager.ShipyardCommandType var1, Object[] var2) {
      this.currentName = null;
      this.currentBlueprintName = null;
      this.isInRepair = false;
      this.spawnDesignWithoutBlocks = false;
      this.playerStateSent = -1;
      LogUtil.sy().fine(this.getSegmentController() + ": Command on server " + var1.name() + "; args: " + Arrays.toString(var2));
      switch(var1) {
      case CREATE_NEW_DESIGN:
         this.currentName = (String)var2[0];
         return;
      case DECONSTRUCT:
         this.currentName = (String)var2[0];
         return;
      case SPAWN_DESIGN:
         this.currentName = (String)var2[0];
         return;
      case DESIGN_TO_BLUEPRINT:
         this.currentName = (String)var2[0];
         this.designToBlueprintOwner = (Integer)var2[1];
         this.currentClassification = BlueprintClassification.values()[(Integer)var2[2]];
         return;
      case BLUEPRINT_TO_DESIGN:
         this.currentName = (String)var2[0];
         this.currentBlueprintName = (String)var2[1];
         return;
      case CATALOG_TO_DESIGN:
         this.currentName = (String)var2[0];
         this.currentBlueprintName = (String)var2[1];
         return;
      case REPAIR_FROM_DESIGN:
         this.isInRepair = true;
         this.designToLoad = (Integer)var2[0];
         if (this.getCurrentDocked() != null) {
            this.currentName = this.getCurrentDocked().getRealName();
            return;
         }
         break;
      case TEST_DESIGN:
         this.playerStateSent = (Integer)var2[0];
         Sendable var3;
         if ((var3 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(this.playerStateSent)) != null && var3 instanceof PlayerState) {
            this.currentName = ((PlayerState)var3).getName() + "_" + System.currentTimeMillis();
         } else {
            this.currentName = "SERVER_ERROR_NO_PLAYER_" + System.currentTimeMillis();
         }

         this.spawnDesignWithoutBlocks = true;
         return;
      case LOAD_DESIGN:
         this.designToLoad = (Integer)var2[0];
      }

   }

   public boolean setCompletionOrderPercentAndSendIfChanged(double var1) {
      return this.shipyardCollectionManager.setCompletionOrderPercentAndSendIfChanged(var1);
   }

   public void unloadCurrentDockedVolatile() {
      LogUtil.sy().fine(this.getSegmentController() + " unload docked volatile");
      this.shipyardCollectionManager.unloadCurrentDockedVolatile();
   }

   public SegmentController loadDesign(VirtualBlueprintMetaItem var1) {
      LogUtil.sy().fine(this.getSegmentController() + " load design from meta item: " + var1);
      return this.shipyardCollectionManager.loadDesign(var1);
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var3;
      byte var2 = (Byte)(var3 = (Tag[])var1.getValue())[0].getValue();
      this.currentMapTo.readByteArray((byte[])var3[1].getValue());
      this.currentName = var3[2].getType() == Tag.Type.BYTE ? null : (String)var3[2].getValue();
      this.designToLoad = (Integer)var3[3].getValue();
      if (var2 > 0) {
         this.isInRepair = (Byte)var3[4].getValue() > 0;
         this.lastDeconstructedBlockCount = (Integer)var3[5].getValue();
      }

      if (var2 > 1) {
         this.lastOrderFactionId = (Integer)var3[6].getValue();
      }

      if (var2 > 2) {
         this.currentClassification = BlueprintClassification.values()[(Byte)var3[7].getValue()];
      }

      LogUtil.sy().fine(this.getSegmentController() + " from tag: current name: currentName: " + this.currentName + "; designToLoad: " + this.designToLoad);
   }

   public Tag toTagStructure() {
      Tag var1 = new Tag(Tag.Type.BYTE, (String)null, (byte)3);
      Tag var2 = new Tag(Tag.Type.BYTE_ARRAY, (String)null, this.currentMapTo.getByteArray());
      Tag var3 = this.currentName != null ? new Tag(Tag.Type.STRING, (String)null, this.currentName) : new Tag(Tag.Type.BYTE, (String)null, (byte)0);
      Tag var4 = new Tag(Tag.Type.INT, (String)null, this.designToLoad);
      Tag var5 = new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.isInRepair ? 1 : 0)));
      Tag var6 = new Tag(Tag.Type.INT, (String)null, this.lastDeconstructedBlockCount);
      Tag var7 = new Tag(Tag.Type.INT, (String)null, this.lastOrderFactionId);
      Tag var8 = new Tag(Tag.Type.BYTE, (String)null, this.currentClassification != null ? (byte)this.currentClassification.ordinal() : 0);
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, var2, var3, var4, var5, var6, var7, var8, FinishTag.INST});
   }

   private void putIn(short var1, int var2, int var3, Inventory var4, int var5, FastCopyLongOpenHashSet var6, InventoryChangeMap var7) {
      if (var1 == -32768 || var2 > 0) {
         if (var1 == -32768) {
            InventorySlot var13 = var4.getSlot(var5);
            System.err.println("[SHIPYARD] DECONSTRUCTING MULTISLOT: " + var13);
            Iterator var14 = var13.getSubSlots().iterator();

            while(var14.hasNext()) {
               InventorySlot var16 = (InventorySlot)var14.next();
               System.err.println("[SHIPYARD] DECONSTRUCTING MULTISLOT " + var16 + ";");
               this.putIn(var16.getType(), var16.count(), var16.metaId, var4, var5, var6, var7);
            }

         } else {
            if (ElementKeyMap.isValidType(var1) && ElementKeyMap.getInfoFast(var1).getSourceReference() != 0) {
               var1 = (short)ElementKeyMap.getInfoFast(var1).getSourceReference();
            }

            Inventory var8 = this.getInventory();
            boolean var9 = false;
            if (this.getInventory().canPutIn(var1, var2)) {
               var7.getInv(var8).add(var8.incExistingOrNextFreeSlotWithoutException(var1, var2, var3));
               var9 = true;
            } else if (var6 != null) {
               Iterator var10 = var6.iterator();

               while(var10.hasNext()) {
                  long var11 = (Long)var10.next();
                  if ((var8 = this.shipyardCollectionManager.getContainer().getInventory(ElementCollection.getPosIndexFrom4(var11))).canPutIn(var1, var2)) {
                     var7.getInv(var8).add(var8.incExistingOrNextFreeSlotWithoutException(var1, var2, var3));
                     var9 = true;
                     break;
                  }
               }
            }

            Sector var15;
            if (!var9 && (var15 = ((GameServerState)this.getState()).getUniverse().getSector(this.getSegmentController().getSectorId())) != null) {
               Vector3i var17 = Element.DIRECTIONSi[Math.min(Math.max(0, this.shipyardCollectionManager.getControllerElement().getOrientation()), 5)];
               Vector3f var12;
               Vector3f var10000 = var12 = this.shipyardCollectionManager.getControllerElement().getAbsolutePos(new Vector3f());
               var10000.x -= 16.0F;
               var12.y -= 16.0F;
               var12.z -= 16.0F;
               var12.x += (float)var17.x;
               var12.y += (float)var17.y;
               var12.z += (float)var17.z;
               var12.x = (float)((double)var12.x + (Math.random() - 0.5D));
               var12.y = (float)((double)var12.y + (Math.random() - 0.5D));
               var12.z = (float)((double)var12.z + (Math.random() - 0.5D));
               this.getSegmentController().getWorldTransform().transform(var12);
               System.err.println("[INVENTORY][SPAWNING] TO spawning inventory at " + ElementKeyMap.toString(var1) + " count: " + var2 + " -> " + var12);
               var15.getRemoteSector().addItem(var12, var1, -1, var2);
            }

         }
      }
   }

   private void putInInventoryAndConnected(Inventory var1) {
      Short2ObjectOpenHashMap var2 = this.getSegmentController().getControlElementMap().getControllingMap().get(this.shipyardCollectionManager.getControllerElement().getAbsoluteIndex());
      FastCopyLongOpenHashSet var3 = null;
      if (var2 != null) {
         var3 = (FastCopyLongOpenHashSet)var2.get((short)120);
      }

      InventoryChangeMap var9 = new InventoryChangeMap();
      Iterator var4 = var1.getSlots().iterator();

      while(var4.hasNext()) {
         int var5 = (Integer)var4.next();
         short var6 = var1.getType(var5);
         int var7 = var1.getCount(var5, var6);
         int var8 = var1.getMeta(var5);
         this.putIn(var6, var7, var8, var1, var5, var3, var9);
      }

      var9.sendAll();
   }

   public void putInInventoryAndConnected(ElementCountMap var1, boolean var2) {
      Short2ObjectOpenHashMap var3 = this.getSegmentController().getControlElementMap().getControllingMap().get(this.shipyardCollectionManager.getControllerElement().getAbsoluteIndex());
      FastCopyLongOpenHashSet var4 = null;
      if (var3 != null) {
         var4 = (FastCopyLongOpenHashSet)var3.get((short)120);
      }

      Random var14 = new Random((long)this.getSegmentController().getName().hashCode());
      InventoryChangeMap var5 = new InventoryChangeMap();
      Iterator var6 = ElementKeyMap.keySet.iterator();

      while(true) {
         int var7;
         int var8;
         do {
            if (!var6.hasNext()) {
               var5.sendAll();
               return;
            }

            var8 = var7 = (Short)var6.next();
         } while((var8 = var1.get((short)var8)) <= 0);

         if (ElementKeyMap.isValidType((short)var7) && ElementKeyMap.getInfoFast((short)var7).getSourceReference() != 0) {
            var7 = (short)ElementKeyMap.getInfoFast((short)var7).getSourceReference();
         }

         if (var2) {
            var7 = var14.nextBoolean() ? 546 : 547;
         }

         Inventory var9 = this.getInventory();
         boolean var10 = false;
         if (this.getInventory().canPutIn((short)var7, var8)) {
            var5.getInv(var9).add(var9.incExistingOrNextFreeSlotWithoutException((short)var7, var8));
            var10 = true;
         } else if (var4 != null) {
            Iterator var11 = var4.iterator();

            while(var11.hasNext()) {
               long var12 = (Long)var11.next();
               if ((var9 = this.shipyardCollectionManager.getContainer().getInventory(ElementCollection.getPosIndexFrom4(var12))).canPutIn((short)var7, var8)) {
                  var5.getInv(var9).add(var9.incExistingOrNextFreeSlotWithoutException((short)var7, var8));
                  var10 = true;
                  break;
               }
            }
         }

         Sector var15;
         if (!var10 && (var15 = ((GameServerState)this.getState()).getUniverse().getSector(this.getSegmentController().getSectorId())) != null) {
            Vector3i var16 = Element.DIRECTIONSi[Math.min(Math.max(0, this.shipyardCollectionManager.getControllerElement().getOrientation()), 5)];
            Vector3f var13;
            Vector3f var10000 = var13 = this.shipyardCollectionManager.getControllerElement().getAbsolutePos(new Vector3f());
            var10000.x -= 16.0F;
            var13.y -= 16.0F;
            var13.z -= 16.0F;
            var13.x += (float)var16.x;
            var13.y += (float)var16.y;
            var13.z += (float)var16.z;
            var13.x = (float)((double)var13.x + (Math.random() - 0.5D));
            var13.y = (float)((double)var13.y + (Math.random() - 0.5D));
            var13.z = (float)((double)var13.z + (Math.random() - 0.5D));
            this.getSegmentController().getWorldTransform().transform(var13);
            System.err.println("[INVENTORY][SPAWNING] TE spawning inventory at " + ElementKeyMap.toString((short)var7) + " count: " + var8 + " -> " + var13);
            var15.getRemoteSector().addItem(var13, (short)var7, -1, var8);
         }
      }
   }

   public void saveInventories(SegmentController var1) {
      LogUtil.sy().fine(this.getSegmentController() + " from tag: current name: Deconstructing: saving inventories of: " + var1);
      ObjectArrayList var2 = new ObjectArrayList();
      this.saveInventoriesRecursive(var1, var2);
      Tag[] var3 = new Tag[var2.size() + 1];

      for(int var4 = 0; var4 < var2.size(); ++var4) {
         var3[var4] = (Tag)var2.get(var4);
      }

      var3[var3.length - 1] = FinishTag.INST;
      this.invs = new Tag(Tag.Type.STRUCT, (String)null, var3);
      LogUtil.sy().fine(this.getSegmentController() + " from tag: current name: Deconstructing: DONE saving inventories of: " + var1 + ": " + var2.size());
   }

   private void saveInventoriesRecursive(SegmentController var1, List var2) {
      Iterator var3;
      if (var1 instanceof ManagedSegmentController) {
         InventoryMap var10000 = ((ManagedSegmentController)var1).getManagerContainer().getInventories();
         Inventory var4 = null;
         var3 = var10000.values().iterator();

         while(var3.hasNext()) {
            var4 = (Inventory)var3.next();
            Tag var5 = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.STRING, (String)null, var1.getUniqueIdentifier()), new Tag(Tag.Type.LONG, (String)null, var4.getParameter()), var4.toTagStructure(), FinishTag.INST});
            var2.add(var5);
            this.putInInventoryAndConnected(var4);
         }
      }

      var3 = var1.railController.next.iterator();

      while(var3.hasNext()) {
         RailRelation var6 = (RailRelation)var3.next();
         this.saveInventoriesRecursive(var6.docked.getSegmentController(), var2);
      }

   }
}
