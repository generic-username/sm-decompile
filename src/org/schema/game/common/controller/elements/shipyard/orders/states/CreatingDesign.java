package org.schema.game.common.controller.elements.shipyard.orders.states;

import com.bulletphysics.linearmath.Transform;
import java.sql.SQLException;
import org.schema.common.LogUtil;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.shipyard.orders.ShipyardEntityState;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.element.meta.VirtualBlueprintMetaItem;
import org.schema.game.common.data.player.inventory.NoSlotFreeException;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.SegmentDataIntArray;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.EntityRequest;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.objects.Sendable;

public class CreatingDesign extends ShipyardState {
   public CreatingDesign(ShipyardEntityState var1) {
      super(var1);
   }

   public boolean onEnterS() {
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      if (this.getEntityState().getCurrentDocked() != null) {
         LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Cannot create design: shipyard occupied");
         this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATINGDESIGN_0);
         this.stateTransition(Transition.SY_ERROR);
      } else if (!this.getEntityState().getInventory().hasFreeSlot()) {
         LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Cannot create design: no slot free in shipyard computer");
         this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATINGDESIGN_1);
         this.stateTransition(Transition.SY_ERROR);
      } else {
         boolean var1;
         if (!(var1 = (Sendable)this.getEntityState().getState().getLocalAndRemoteObjectContainer().getUidObjectMap().get(SimpleTransformableSendableObject.EntityType.SHIP.dbPrefix + this.getEntityState().currentName) != null)) {
            try {
               var1 = !((GameServerState)this.getEntityState().getState()).getDatabaseIndex().getTableManager().getEntityTable().getByUIDExact(this.getEntityState().currentName, 1).isEmpty();
            } catch (SQLException var8) {
               var8.printStackTrace();
            }
         }

         if (var1) {
            LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Cannot create: Design name already exists");
            this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATINGDESIGN_2);
            this.stateTransition(Transition.SY_ERROR);
            return false;
         }

         double var4;
         if ((var4 = this.getTickCount()) >= 1.0D) {
            this.getEntityState().setCompletionOrderPercentAndSendIfChanged(1.0D);
            Transform var9;
            (var9 = new Transform()).setIdentity();
            float[] var2 = new float[16];
            var9.getOpenGLMatrix(var2);
            Ship var10;
            (var10 = EntityRequest.getNewShip(this.getEntityState().getState(), SimpleTransformableSendableObject.EntityType.SHIP.dbPrefix + this.getEntityState().currentName, this.getEntityState().getSegmentController().getSectorId(), this.getEntityState().currentName, var2, -2, -2, -2, 2, 2, 2, "SHIPYARD_" + this.getEntityState().getSegmentController().getUniqueIdentifier(), false)).setTouched(true, false);
            RemoteSegment var11;
            (var11 = new RemoteSegment(var10)).setSegmentData(new SegmentDataIntArray(var10.getState() instanceof ClientStateInterface));
            var11.getSegmentData().setSegment(var11);

            try {
               var11.getSegmentData().setInfoElementUnsynched((byte)Ship.core.x, (byte)Ship.core.y, (byte)Ship.core.z, (short)1, true, var11.getAbsoluteIndex((byte)8, (byte)8, (byte)8), var10.getState().getUpdateTime());
            } catch (SegmentDataWriteException var7) {
               throw new RuntimeException("Should always be normal data", var7);
            }

            var11.setLastChanged(System.currentTimeMillis());
            var10.getSegmentBuffer().addImmediate(var11);
            var10.getSegmentBuffer().updateBB(var11);
            boolean var12 = this.getEntityState().getShipyardCollectionManager().createDockingRelation(var10, true);
            LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Creating design: ship spawned: docked: " + var10 + "; DOCKING: " + var12);
            if (var12) {
               ((GameServerState)this.getEntityState().getState()).getMetaObjectManager();
               VirtualBlueprintMetaItem var13 = (VirtualBlueprintMetaItem)MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.VIRTUAL_BLUEPRINT.type, (short)-1, true);
               boolean var3 = false;

               try {
                  int var14 = this.getEntityState().getInventory().getFreeSlot();
                  var13.UID = var10.getUniqueIdentifier();
                  var13.virtualName = new String(this.getEntityState().currentName);
                  this.getEntityState().getInventory().put(var14, var13);
                  this.getEntityState().getInventory().sendInventoryModification(var14);
                  this.getEntityState().getShipyardCollectionManager().setCurrentDesign(var13.getId());
                  var10.initialize();
                  this.getEntityState().getShipyardCollectionManager().sendShipyardStateToClient();
                  ((GameServerState)this.getEntityState().getState()).getController().getSynchController().addNewSynchronizedObjectQueued(var10);
                  LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Creating design: loaded and docked successfully " + var10);
                  this.stateTransition(Transition.SY_LOADING_DONE);
               } catch (NoSlotFreeException var6) {
                  LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Critical Server Error No slot while space reserved ");
                  var6.printStackTrace();
                  this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATINGDESIGN_3);
                  this.stateTransition(Transition.SY_ERROR);
               }
            } else {
               this.stateTransition(Transition.SY_ERROR);
            }
         } else {
            this.getEntityState().getShipyardCollectionManager().setCompletionOrderPercentAndSendIfChanged((double)((float)var4));
         }
      }

      return false;
   }

   public String getClientShortDescription() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATINGDESIGN_4;
   }
}
