package org.schema.game.common.controller.elements.shipyard.orders.states;

import java.util.Iterator;
import org.schema.common.LogUtil;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.shipyard.orders.ShipyardEntityState;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.VirtualBlueprintMetaItem;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;

public class WaitingForShipyardOrder extends ShipyardState {
   private long startedWaitingLocal;

   public WaitingForShipyardOrder(ShipyardEntityState var1) {
      super(var1);
   }

   public boolean onEnterS() {
      this.getEntityState().getShipyardCollectionManager().setCompletionOrderPercent(-1.0D);
      this.getEntityState().getShipyardCollectionManager().sendShipyardStateToClient();
      this.startedWaitingLocal = System.currentTimeMillis();
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      SegmentController var1;
      if ((var1 = this.getEntityState().getCurrentDocked()) != null) {
         LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Waiting for design but got an unexpected dock: " + var1);
         if (!var1.isMarkedForDeleteVolatile()) {
            if (!var1.isVirtualBlueprint()) {
               LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Waiting for design but got an unexpected dock: " + var1 + " -> normal dock");
               this.stateTransition(Transition.SY_LOAD_NORMAL);
            } else {
               LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Waiting for design but got an unexpected dock: " + var1 + " -> design");
               boolean var2 = false;
               Iterator var3 = this.getEntityState().getInventory().getSlots().iterator();

               while(var3.hasNext()) {
                  int var4 = (Integer)var3.next();
                  MetaObject var5;
                  if ((var4 = this.getEntityState().getInventory().getMeta(var4)) >= 0 && (var5 = ((GameServerState)this.getEntityState().getState()).getMetaObjectManager().getObject(var4)) != null && var5 instanceof VirtualBlueprintMetaItem && ((VirtualBlueprintMetaItem)var5).UID.equals(var1.getUniqueIdentifier())) {
                     this.getEntityState().designToLoad = var5.getId();

                     assert this.getMachine().getFsm().getCurrentState() == this;

                     LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Waiting for design but got an unexpected dock: " + var1 + " DESIGN DOCK AND DESIGN FOUND IN INVENTORY");
                     System.err.println("[SERVER][SHIPYARD] there is a design docked but no design loaded. Design found in inventory: " + var1);
                     this.stateTransition(Transition.SY_LOAD_DESIGN);
                     var2 = true;
                     break;
                  }
               }

               if (!var2) {
                  LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Waiting for design but got an unexpected dock: " + var1 + " -> DESIGN DOCKED BUT DESIGN NOT FOUND IN INV: unload design");
                  this.getEntityState().unloadCurrentDockedVolatile();
               }
            }
         }
      }

      return false;
   }

   public String getClientShortDescription() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_WAITINGFORSHIPYARDORDER_0;
   }
}
