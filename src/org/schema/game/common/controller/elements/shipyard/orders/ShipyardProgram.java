package org.schema.game.common.controller.elements.shipyard.orders;

import java.util.HashMap;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.schine.ai.stateMachines.AIConfiguationElementsInterface;

public class ShipyardProgram extends TargetProgram {
   public static final String STD = "STD";

   public ShipyardProgram(ShipyardEntityState var1, boolean var2) {
      super(var1, var2);
   }

   public void onAISettingChanged(AIConfiguationElementsInterface var1) {
   }

   protected String getStartMachine() {
      return "STD";
   }

   protected void initializeMachines(HashMap var1) {
      var1.put("STD", new ShipyardMachine((ShipyardEntityState)this.getEntityState(), this));
   }
}
