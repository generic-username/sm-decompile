package org.schema.game.common.controller.elements.shipyard.orders.states;

import org.schema.common.LogUtil;
import org.schema.game.common.controller.elements.shipyard.orders.ShipyardEntityState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class RevertPullingBlocksFromInventory extends ShipyardState {
   public RevertPullingBlocksFromInventory(ShipyardEntityState var1) {
      super(var1);
   }

   public boolean onEnterS() {
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      if (this.getEntityState().currentMapTo.getTotalAmount() > 0L) {
         LogUtil.sy().fine(this + " " + this.getEntityState() + "; " + this.getEntityState().getSegmentController() + " REVERT PULLING ");
         this.getEntityState().putInInventoryAndConnected(this.getEntityState().currentMapTo, false);
         this.getEntityState().currentMapTo.resetAll();
      }

      this.stateTransition(Transition.SY_BLOCK_TRANSACTION_FINISHED);
      return false;
   }
}
