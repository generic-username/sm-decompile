package org.schema.game.common.controller.elements.shipyard.orders.states;

import org.schema.common.LogUtil;
import org.schema.game.common.controller.elements.shipyard.orders.ShipyardEntityState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;

public class UnloadingDesign extends ShipyardState {
   public UnloadingDesign(ShipyardEntityState var1) {
      super(var1);
   }

   public boolean onEnterS() {
      this.getEntityState().getShipyardCollectionManager().setCompletionOrderPercentAndSendIfChanged(0.0D);
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      if (this.getEntityState().isLoadedDesignValid()) {
         double var1;
         if ((var1 = this.getTickCount()) < 1.0D) {
            LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Unloading design waiting " + var1);
            this.getEntityState().getShipyardCollectionManager().setCompletionOrderPercentAndSendIfChanged((double)((float)var1));
         } else {
            this.getEntityState().unloadCurrentDockedVolatile();
            LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Unloading design DONE " + var1);
            this.getEntityState().getShipyardCollectionManager().setCompletionOrderPercentAndSendIfChanged(1.0D);
            this.getEntityState().getShipyardCollectionManager().setCurrentDesign(-1);
            this.getEntityState().sendShipyardStateToClient();
            this.stateTransition(Transition.SY_UNLOADING_DONE);
         }
      } else {
         LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Design no longer loaded");
         this.getEntityState().getShipyardCollectionManager().setCurrentDesign(-1);
         this.getEntityState().sendShipyardStateToClient();
         this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_UNLOADINGDESIGN_0);
         this.stateTransition(Transition.SY_ERROR);
      }

      return false;
   }
}
