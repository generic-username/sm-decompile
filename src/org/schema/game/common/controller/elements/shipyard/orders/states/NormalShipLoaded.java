package org.schema.game.common.controller.elements.shipyard.orders.states;

import org.schema.common.LogUtil;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.shipyard.orders.ShipyardEntityState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;

public class NormalShipLoaded extends ShipyardState {
   private SegmentController docked = null;

   public NormalShipLoaded(ShipyardEntityState var1) {
      super(var1);
   }

   public boolean onEnterS() {
      this.docked = null;
      return false;
   }

   public boolean onExit() {
      this.docked = null;
      return false;
   }

   public boolean onUpdate() throws FSMException {
      SegmentController var1 = this.getEntityState().getCurrentDocked();
      if (this.docked != var1) {
         LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Normal Ship Docked: " + var1);
      }

      this.docked = var1;
      if (var1 == null || var1.isVirtualBlueprint()) {
         if (var1 != null && var1.isVirtualBlueprint()) {
            LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Blueprint undocked");
            this.getEntityState().unloadCurrentDockedVolatile();
         }

         if (this.getEntityState().wasUndockedManually) {
            LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Normal Ship Undocked manually");
            this.getEntityState().wasUndockedManually = false;
         } else {
            LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Normal Ship Undocked for another reason");
            this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_NORMALSHIPLOADED_0);
         }

         this.stateTransition(Transition.SY_ERROR);
      }

      return false;
   }

   public String getClientShortDescription() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_NORMALSHIPLOADED_1;
   }

   public boolean canEdit() {
      return true;
   }

   public boolean canUndock() {
      return true;
   }
}
