package org.schema.game.common.controller.elements.shipyard;

import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.schine.resource.tag.Tag;

public class ShipyardMetaDataDummy extends BlockMetaDataDummy {
   Tag tag;

   protected void fromTagStructrePriv(Tag var1, int var2) {
      this.tag = var1;
   }

   public String getTagName() {
      return "SYRD";
   }

   protected Tag toTagStructurePriv() {
      return this.tag;
   }
}
