package org.schema.game.common.controller.elements.shipyard;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.bytes.Byte2BooleanOpenHashMap;
import it.unimi.dsi.fastutil.bytes.Byte2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.Object2ByteOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import javax.vecmath.Vector3f;
import org.reflections.Reflections;
import org.reflections.scanners.Scanner;
import org.schema.common.LogUtil;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerButtonTilesInput;
import org.schema.game.client.controller.PlayerGameDropDownInput;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerTextInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.view.effects.RaisingIndication;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.power.PowerAddOn;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.controller.elements.shipyard.orders.ShipyardEntityState;
import org.schema.game.common.controller.elements.shipyard.orders.ShipyardMachine;
import org.schema.game.common.controller.elements.shipyard.orders.ShipyardProgram;
import org.schema.game.common.controller.elements.shipyard.orders.states.BlueprintSpawned;
import org.schema.game.common.controller.elements.shipyard.orders.states.CollectingBlocksFromInventory;
import org.schema.game.common.controller.elements.shipyard.orders.states.Constructing;
import org.schema.game.common.controller.elements.shipyard.orders.states.ConvertingRealToVirtual;
import org.schema.game.common.controller.elements.shipyard.orders.states.ConvertingVirtualToReal;
import org.schema.game.common.controller.elements.shipyard.orders.states.CreateBlueprintFromDesign;
import org.schema.game.common.controller.elements.shipyard.orders.states.CreateDesignFromBlueprint;
import org.schema.game.common.controller.elements.shipyard.orders.states.CreatingDesign;
import org.schema.game.common.controller.elements.shipyard.orders.states.Deconstructing;
import org.schema.game.common.controller.elements.shipyard.orders.states.DesignLoaded;
import org.schema.game.common.controller.elements.shipyard.orders.states.LoadingDesign;
import org.schema.game.common.controller.elements.shipyard.orders.states.MovingToTestSite;
import org.schema.game.common.controller.elements.shipyard.orders.states.NormalShipLoaded;
import org.schema.game.common.controller.elements.shipyard.orders.states.PuttingBlocksInInventory;
import org.schema.game.common.controller.elements.shipyard.orders.states.RemovingDesign;
import org.schema.game.common.controller.elements.shipyard.orders.states.RemovingPhysical;
import org.schema.game.common.controller.elements.shipyard.orders.states.RevertPullingBlocksFromInventory;
import org.schema.game.common.controller.elements.shipyard.orders.states.RevertPuttingBlocksInInventory;
import org.schema.game.common.controller.elements.shipyard.orders.states.ShipyardState;
import org.schema.game.common.controller.elements.shipyard.orders.states.SpawningBlueprint;
import org.schema.game.common.controller.elements.shipyard.orders.states.UnloadingDesign;
import org.schema.game.common.controller.elements.shipyard.orders.states.WaitingForShipyardOrder;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.game.common.controller.rails.RailRelation;
import org.schema.game.common.controller.rails.RailRequest;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.MetaObjectState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.VoidUniqueSegmentPiece;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.meta.BlueprintMetaItem;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.VirtualBlueprintMetaItem;
import org.schema.game.common.data.player.BlueprintPlayerHandleRequest;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.DrawableRemoteSegment;
import org.schema.game.common.data.world.EntityUID;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.remote.RemoteValueUpdate;
import org.schema.game.network.objects.remote.ShipyardCommand;
import org.schema.game.network.objects.valueUpdate.NTValueUpdateInterface;
import org.schema.game.network.objects.valueUpdate.ShipyardBlockGoalValueUpdate;
import org.schema.game.network.objects.valueUpdate.ShipyardClientCommandValueUpdate;
import org.schema.game.network.objects.valueUpdate.ShipyardClientStateRequestValueUpdate;
import org.schema.game.network.objects.valueUpdate.ShipyardCurrentStateValueUpdate;
import org.schema.game.network.objects.valueUpdate.ShipyardErrorValueUpdate;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;
import org.schema.game.server.data.EntityRequest;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;
import org.schema.game.server.data.blueprintnw.BlueprintType;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.input.InputState;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class ShipyardCollectionManager extends ControlBlockElementCollectionManager implements Comparator, PowerConsumer {
   private static final InputChecker shipInputChecker = new InputChecker() {
      public final boolean check(String var1, TextCallback var2) {
         if (EntityRequest.isShipNameValid(var1)) {
            return true;
         } else {
            var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_0);
            return false;
         }
      }
   };
   private static Object2ByteOpenHashMap stateClass2Ids = new Object2ByteOpenHashMap();
   private static Byte2ObjectOpenHashMap ids2StateClass = new Byte2ObjectOpenHashMap();
   private static Byte2ObjectOpenHashMap ids2StateDescription = new Byte2ObjectOpenHashMap();
   private static Byte2BooleanOpenHashMap idsCanCancel = new Byte2BooleanOpenHashMap();
   private static Byte2BooleanOpenHashMap idsHasBlockGoal = new Byte2BooleanOpenHashMap();
   private static Byte2BooleanOpenHashMap idsCanEdit = new Byte2BooleanOpenHashMap();
   private static Byte2BooleanOpenHashMap idsCanUndock = new Byte2BooleanOpenHashMap();
   private long lastPopup;
   private ShortArrayList distances = new ShortArrayList();
   public final byte VERSION = 1;
   public static boolean DEBUG_MODE;
   private boolean unitsValid;
   private int biggest = -1;
   private final ShipyardEntityState serverEntityState;
   private long lastFSMUpdate;
   public byte currentClientState = -1;
   private double completionOrderPercent;
   private byte lastState = -1;
   private int lastIntPercent;
   private int currentDesign = -1;
   private IntOpenHashSet requestedMeta = new IntOpenHashSet();
   private String waitForExistsUntilFSMUpdate;
   private long lastStateRequest;
   public ElementCountMap clientGoalFrom = new ElementCountMap();
   public ElementCountMap clientGoalTo = new ElementCountMap();
   private boolean wasValid;
   private int maxX;
   private int maxY;
   private int maxZ;
   private int minX;
   private int minY;
   private int minZ;
   private boolean dockingValid;
   private long lastDockValidTest;
   private boolean isPowered = true;
   private float powered;
   private ShipyardState lastServerState;
   private boolean wasPowered;
   private boolean wasValidLogCheck = true;
   private boolean wasDockingValid = true;
   Vector3f minbb = new Vector3f();
   Vector3f maxbb = new Vector3f();
   Vector3f minbbOut = new Vector3f();
   Vector3f maxbbOut = new Vector3f();
   public DrawerObserver drawObserver;
   private boolean instantBuild;

   private static void addClass(Class var0, byte var1) {
      assert !ids2StateClass.containsKey(var1);

      assert !stateClass2Ids.containsKey(var0);

      stateClass2Ids.put(var0, var1);
      ids2StateClass.put(var1, var0);
   }

   private static void readBuildHelperClasses() {
      try {
         addClass(BlueprintSpawned.class, (byte)0);
         addClass(CollectingBlocksFromInventory.class, (byte)1);
         addClass(ConvertingRealToVirtual.class, (byte)2);
         addClass(ConvertingVirtualToReal.class, (byte)3);
         addClass(CreatingDesign.class, (byte)4);
         addClass(Deconstructing.class, (byte)5);
         addClass(DesignLoaded.class, (byte)6);
         addClass(NormalShipLoaded.class, (byte)7);
         addClass(PuttingBlocksInInventory.class, (byte)8);
         addClass(RemovingDesign.class, (byte)9);
         addClass(RemovingPhysical.class, (byte)10);
         addClass(RevertPullingBlocksFromInventory.class, (byte)11);
         addClass(RevertPuttingBlocksInInventory.class, (byte)12);
         addClass(SpawningBlueprint.class, (byte)13);
         addClass(Constructing.class, (byte)14);
         addClass(UnloadingDesign.class, (byte)15);
         addClass(WaitingForShipyardOrder.class, (byte)16);
         addClass(LoadingDesign.class, (byte)17);
         addClass(CreateBlueprintFromDesign.class, (byte)18);
         addClass(CreateDesignFromBlueprint.class, (byte)19);
         addClass(MovingToTestSite.class, (byte)20);
         Iterator var0 = (new Reflections("org.schema.game.common.controller.elements.shipyard.orders.state", new Scanner[0])).getSubTypesOf(ShipyardState.class).iterator();

         while(var0.hasNext()) {
            Class var1 = (Class)var0.next();

            assert var1 != null : var1;

            assert stateClass2Ids != null;

            assert ids2StateClass != null;

            assert stateClass2Ids.getByte(var1) != -1 : "missing id for class: " + var1.getName();

            assert ids2StateClass.get(stateClass2Ids.getByte(var1)) == var1 : "invalid id for class: " + var1.getName();

            ShipyardState var2 = (ShipyardState)var1.getConstructor(ShipyardEntityState.class).newInstance(null);
            idsCanCancel.put(stateClass2Ids.getByte(var1), var2.canCancel());
            idsHasBlockGoal.put(stateClass2Ids.getByte(var1), var2.hasBlockGoal());
            idsCanUndock.put(stateClass2Ids.getByte(var1), var2.canUndock());
            idsCanEdit.put(stateClass2Ids.getByte(var1), var2.canEdit());
            ids2StateDescription.put(stateClass2Ids.getByte(var1), var2.getClientShortDescription());
         }

      } catch (Exception var3) {
         var3.printStackTrace();

         assert false;

      }
   }

   public ShipyardCollectionManager(SegmentPiece var1, SegmentController var2, ShipyardElementManager var3) {
      super(var1, (short)678, var2, var3);
      if (var2.isOnServer()) {
         this.serverEntityState = new ShipyardEntityState("SY", this, var2.getState());
         this.serverEntityState.setCurrentProgram(new ShipyardProgram(this.serverEntityState, false));
      } else {
         this.serverEntityState = null;
      }
   }

   public boolean isCurrentStateCancelClient() {
      return idsCanCancel.containsKey(this.currentClientState) && idsCanCancel.get(this.currentClientState);
   }

   public boolean isCurrentStateWithGoalListClient() {
      return idsHasBlockGoal.containsKey(this.currentClientState) && idsHasBlockGoal.get(this.currentClientState);
   }

   public boolean isCurrentStateUndockable() {
      return idsCanUndock.containsKey(this.currentClientState) && idsCanUndock.get(this.currentClientState);
   }

   public boolean isPublicException(int var1) {
      return SegmentController.isBlockPublicException(this.getSegmentController(), this.getControllerPos(), var1);
   }

   public List getCatalog() {
      ObjectArrayList var1 = new ObjectArrayList();
      Iterator var2 = ((GameClientState)this.getState()).getPlayer().getCatalog().getAvailableCatalog().iterator();

      while(var2.hasNext()) {
         CatalogPermission var3;
         if ((var3 = (CatalogPermission)var2.next()).type == BlueprintType.SHIP) {
            GUIAncor var4 = new GUIAncor((GameClientState)this.getState(), 300.0F, 24.0F);
            GUITextOverlay var5;
            (var5 = new GUITextOverlay(300, 24, (GameClientState)this.getState())).setTextSimple(var3.getUid());
            var5.setPos(5.0F, 4.0F, 0.0F);
            var4.attach(var5);
            var4.setUserPointer(var3.getUid());
            var1.add(var4);
         }
      }

      Collections.sort(var1, new Comparator() {
         public int compare(GUIElement var1, GUIElement var2) {
            GUITextOverlay var3 = (GUITextOverlay)var1.getChilds().get(0);
            GUITextOverlay var4 = (GUITextOverlay)var2.getChilds().get(0);
            return var3.getText().get(0).toString().toLowerCase(Locale.ENGLISH).compareTo(var4.getText().get(0).toString().toLowerCase(Locale.ENGLISH));
         }
      });
      return var1;
   }

   public VirtualBlueprintMetaItem getCurrentDesignObject() {
      if (this.getCurrentDesign() >= 0) {
         MetaObject var1;
         if (this.getSegmentController().isOnServer()) {
            if ((var1 = ((MetaObjectState)this.getSegmentController().getState()).getMetaObjectManager().getObject(this.getCurrentDesign())) != null && var1 instanceof VirtualBlueprintMetaItem) {
               return (VirtualBlueprintMetaItem)var1;
            }
         } else if ((var1 = ((MetaObjectState)this.getSegmentController().getState()).getMetaObjectManager().getObject(this.getCurrentDesign())) == null) {
            if (this.requestedMeta.contains(this.getCurrentDesign())) {
               ((MetaObjectState)this.getSegmentController().getState()).getMetaObjectManager().checkAvailable(this.getCurrentDesign(), (MetaObjectState)this.getSegmentController().getState());
               this.requestedMeta.add(this.getCurrentDesign());
            }
         } else if (var1 instanceof VirtualBlueprintMetaItem) {
            return (VirtualBlueprintMetaItem)var1;
         }
      }

      return null;
   }

   public boolean isCommandUsable(ShipyardCollectionManager.ShipyardCommandType var1) {
      boolean var2 = DesignLoaded.class.equals(ids2StateClass.get(this.currentClientState)) && var1 == ShipyardCollectionManager.ShipyardCommandType.UNLOAD_DESIGN;
      return (this.getCurrentDocked() == null || this.isDockingValid() || var2) && var1.requiredState.equals(ids2StateClass.get(this.currentClientState));
   }

   protected Tag toTagStructurePriv() {
      if (this.getSegmentController().isOnServer()) {
         Tag var1 = new Tag(Tag.Type.BYTE, (String)null, (byte)1);
         Tag var2 = new Tag(Tag.Type.BYTE, (String)null, this.getStateByteOnServer());
         System.err.println("[SERVER][SHIPYARD] " + this.getSegmentController() + "; writing current design: " + this.getCurrentDesign());
         Tag var3 = new Tag(Tag.Type.INT, (String)null, this.getCurrentDesign());
         Tag var4 = this.serverEntityState.toTagStructure();
         ShipyardState var5;
         if ((var5 = this.getCurrentStateServer()) != null) {
            System.err.println("[SERVER][SHIPYARD] saved ongoning state " + this.getSegmentController() + ": " + var5 + ": Start " + var5.getStartTime() + "; ticks done " + var5.getTicksDone());
         }

         Tag var6 = new Tag(Tag.Type.LONG, (String)null, var5 != null ? var5.getStartTime() : -1L);
         Tag var7 = new Tag(Tag.Type.INT, (String)null, var5 != null ? var5.getTicksDone() : -1);
         return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, var2, var3, var4, var6, var7, FinishTag.INST});
      } else {
         return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, -2), FinishTag.INST});
      }
   }

   private void fromTagStructure(Tag var1) {
      byte var2;
      Tag[] var6;
      if ((var2 = (Byte)(var6 = (Tag[])var1.getValue())[0].getValue()) >= 0) {
         byte var3 = (Byte)var6[1].getValue();
         this.serverEntityState.fromTagStructure(var6[3]);
         ShipyardState var8;
         if ((var8 = (ShipyardState)((ShipyardMachine)this.getServerEntityState().getCurrentProgram().getMachine()).getById((Class)ids2StateClass.get(var3), this.serverEntityState.isInRepair)) != null) {
            var8.setLoadedFromTag(true);
            this.getServerEntityState().getCurrentProgram().getMachine().setState(var8);
            if (var2 > 0) {
               long var4 = (Long)var6[4].getValue();
               int var7 = (Integer)var6[5].getValue();
               var8.loadedStartTime = var4;
               var8.loadedTicksDone = var7;
            }
         }

         this.setCurrentDesign((Integer)var6[2].getValue());
         MetaObject var9 = ((MetaObjectState)this.getSegmentController().getState()).getMetaObjectManager().getObject(this.getCurrentDesign());
         System.err.println("[SERVER][SHIPYARD] from tag loaded design: " + this.getCurrentDesign() + ": " + var9);
         LogUtil.sy().fine(this.getSegmentController() + "; " + this + " from tag loaded design: " + this.getCurrentDesign() + ": " + var9);
         if (var9 != null && var9 instanceof VirtualBlueprintMetaItem && this.loadDesign((VirtualBlueprintMetaItem)var9) != null) {
            this.waitForExistsUntilFSMUpdate = ((VirtualBlueprintMetaItem)var9).UID;
            LogUtil.sy().fine(this.getSegmentController() + "; " + this + "  wait for exists UID: " + ((VirtualBlueprintMetaItem)var9).UID);
         }
      }

   }

   protected void applyMetaData(BlockMetaDataDummy var1) {
      this.fromTagStructure(((ShipyardMetaDataDummy)var1).tag);
   }

   public ElementCollectionManager.CollectionShape requiredNeigborsPerBlock() {
      return ElementCollectionManager.CollectionShape.RIP;
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return ShipyardUnit.class;
   }

   public boolean needsUpdate() {
      return true;
   }

   public ShipyardUnit getInstance() {
      return new ShipyardUnit();
   }

   public boolean alwaysValid() {
      return DEBUG_MODE || ((GameStateInterface)this.getState()).getGameState().isShipyardIgnoreStructure();
   }

   protected void onChangedCollection() {
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
      }

      if (this.alwaysValid()) {
         this.unitsValid = true;
      } else {
         this.unitsValid = false;
         if (this.getElementCollections().size() > 0) {
            this.unitsValid = true;
            Iterator var1 = this.getElementCollections().iterator();

            while(var1.hasNext()) {
               ShipyardUnit var2;
               if (!(var2 = (ShipyardUnit)var1.next()).isValid()) {
                  if (var2.getInvalidReason() == null) {
                     var2.setInvalidReason(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_42);
                  }

                  LogUtil.sy().fine(this.getSegmentController() + "; " + this + " One or more shipyard unit invalid: " + var2);
                  this.unitsValid = false;
                  break;
               }
            }
         }

         if (this.unitsValid) {
            int var17 = 0;
            int var18 = 0;
            int var3 = 0;
            this.maxX = Integer.MIN_VALUE;
            this.maxY = Integer.MIN_VALUE;
            this.maxZ = Integer.MIN_VALUE;
            this.minX = Integer.MAX_VALUE;
            this.minY = Integer.MAX_VALUE;
            this.minZ = Integer.MAX_VALUE;
            Iterator var4 = this.getElementCollections().iterator();

            ShipyardUnit var5;
            while(var4.hasNext()) {
               ShipyardUnit var10000 = var5 = (ShipyardUnit)var4.next();
               var10000.getMax(var10000.max);
               var5.getMin(var5.min);
               this.maxX = Math.max(this.maxX, var5.max.x);
               this.maxY = Math.max(this.maxY, var5.max.y);
               this.maxZ = Math.max(this.maxZ, var5.max.z);
               this.minX = Math.min(this.minX, var5.min.x);
               this.minY = Math.min(this.minY, var5.min.y);
               this.minZ = Math.min(this.minZ, var5.min.z);
               if (var5.xDim) {
                  ++var17;
               } else if (var5.yDim) {
                  ++var18;
               } else {
                  ++var3;

                  assert var5.zDim;
               }
            }

            if (var17 > var18) {
               if (var17 > var3) {
                  this.biggest = 0;
               } else {
                  this.biggest = 2;
               }
            } else if (var18 > var3) {
               this.biggest = 1;
            } else {
               this.biggest = 2;
            }

            this.distances.clear();
            this.distances.ensureCapacity(this.getElementCollections().size());
            var4 = this.getElementCollections().iterator();

            while(true) {
               do {
                  label233:
                  do {
                     while(var4.hasNext()) {
                        var5 = (ShipyardUnit)var4.next();
                        if (this.biggest == 0) {
                           if (var5.xDim && !var5.yDim && !var5.zDim) {
                              var5.normalPos = var5.min.x;
                           } else {
                              var5.valid = false;
                              var5.setInvalidReason(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_45);
                              this.unitsValid = false;
                           }
                           continue label233;
                        }

                        if (this.biggest == 1) {
                           if (!var5.xDim && var5.yDim && !var5.zDim) {
                              var5.normalPos = var5.min.y;
                           } else {
                              var5.valid = false;
                              var5.setInvalidReason(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_47);
                              this.unitsValid = false;
                           }

                           if (var5.valid && (var5.max.x != this.maxX || var5.max.z != this.maxZ || var5.min.x != this.minX || var5.min.z != this.minZ)) {
                              var5.valid = false;
                              var5.setInvalidReason(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_44 + "[" + var5.min + ", " + var5.max + "] / [(" + this.minX + ", " + this.minY + ", " + this.minZ + "), (" + this.maxX + ", " + this.maxY + ", " + this.maxZ + ")]");
                              this.unitsValid = false;
                           }
                        } else {
                           assert this.biggest == 2;

                           if (!var5.xDim && !var5.yDim && var5.zDim) {
                              var5.normalPos = var5.min.z;
                           } else {
                              var5.valid = false;
                              var5.setInvalidReason(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_43);
                              this.unitsValid = false;
                           }

                           if (var5.valid && (var5.max.x != this.maxX || var5.max.y != this.maxY || var5.min.x != this.minX || var5.min.y != this.minY)) {
                              var5.valid = false;
                              var5.setInvalidReason(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_46 + "[" + var5.min + ", " + var5.max + "] / [(" + this.minX + ", " + this.minY + ", " + this.minZ + "), (" + this.maxX + ", " + this.maxY + ", " + this.maxZ + ")]");
                              this.unitsValid = false;
                           }
                        }
                     }

                     if (!this.unitsValid) {
                        LogUtil.sy().fine(this.getSegmentController() + "; " + this + " One or more shipyard unit invalid (PRECHECK)");
                     }

                     if (this.unitsValid) {
                        Collections.sort(this.getElementCollections(), this);
                        int var19 = this.getElementCollections().size();
                        int var20 = ElementCollection.getPosX(((ShipyardUnit)this.getElementCollections().get(0)).endA);
                        var17 = ElementCollection.getPosY(((ShipyardUnit)this.getElementCollections().get(0)).endA);
                        var18 = ElementCollection.getPosZ(((ShipyardUnit)this.getElementCollections().get(0)).endA);
                        var3 = ElementCollection.getPosX(((ShipyardUnit)this.getElementCollections().get(0)).endB);
                        int var6 = ElementCollection.getPosY(((ShipyardUnit)this.getElementCollections().get(0)).endB);
                        int var7 = ElementCollection.getPosZ(((ShipyardUnit)this.getElementCollections().get(0)).endB);
                        int var8 = this.getMaxArcDistance();

                        for(int var9 = 0; var9 < var19; ++var9) {
                           ShipyardUnit var10 = (ShipyardUnit)this.getElementCollections().get(var9);
                           if (var9 == 0 && var19 > 1) {
                              if (Math.abs(var10.normalPos - ((ShipyardUnit)this.getElementCollections().get(var9 + 1)).normalPos) > var8) {
                                 var10.setValid(false);
                                 var10.setInvalidReason(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_51, Math.abs(var10.normalPos - ((ShipyardUnit)this.getElementCollections().get(var9 + 1)).normalPos), var8));
                                 this.unitsValid = false;
                              }
                           } else if (var9 == var19 - 1 && var19 > 1) {
                              if (Math.abs(var10.normalPos - ((ShipyardUnit)this.getElementCollections().get(var9 - 1)).normalPos) > var8) {
                                 var10.setValid(false);
                                 var10.setInvalidReason(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_52, Math.abs(var10.normalPos - ((ShipyardUnit)this.getElementCollections().get(var9 - 1)).normalPos), var8));
                                 this.unitsValid = false;
                              }
                           } else if (var19 > 1 && var9 < var19 - 1 && var9 > 0) {
                              if (Math.abs(var10.normalPos - ((ShipyardUnit)this.getElementCollections().get(var9 + 1)).normalPos) > var8) {
                                 var10.setValid(false);
                                 var10.setInvalidReason(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_50, Math.abs(var10.normalPos - ((ShipyardUnit)this.getElementCollections().get(var9 + 1)).normalPos), var8));
                                 this.unitsValid = false;
                              }
                           } else if (var19 > 1 && var9 < var19 - 1 && var9 > 0 && Math.abs(var10.normalPos - ((ShipyardUnit)this.getElementCollections().get(var9 - 1)).normalPos) > var8) {
                              var10.setValid(false);
                              var10.setInvalidReason(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_49, Math.abs(var10.normalPos - ((ShipyardUnit)this.getElementCollections().get(var9 - 1)).normalPos), var8));
                              this.unitsValid = false;
                           }

                           int var11 = ElementCollection.getPosX(var10.endA);
                           int var12 = ElementCollection.getPosY(var10.endA);
                           int var13 = ElementCollection.getPosZ(var10.endA);
                           int var14 = ElementCollection.getPosX(var10.endB);
                           int var15 = ElementCollection.getPosY(var10.endB);
                           int var16 = ElementCollection.getPosZ(var10.endB);
                           if (this.biggest == 0) {
                              if (var12 != var17 || var15 != var6 || var13 != var18 || var16 != var7) {
                                 var10.setValid(false);
                                 var10.setInvalidReason(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_53);
                                 this.unitsValid = false;
                              }
                           } else if (this.biggest == 1) {
                              if (var11 != var20 || var14 != var3 || var13 != var18 || var16 != var7) {
                                 var10.setValid(false);
                                 var10.setInvalidReason(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_54);
                                 this.unitsValid = false;
                              }
                           } else {
                              assert this.biggest == 2;

                              if (var11 != var20 || var14 != var3 || var12 != var17 || var15 != var6) {
                                 var10.setValid(false);
                                 var10.setInvalidReason(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_55);
                                 this.unitsValid = false;
                              }
                           }
                        }

                        if (!this.unitsValid) {
                           LogUtil.sy().fine(this.getSegmentController() + "; " + this + " One or more shipyard unit invalid (MIDCHECK)");
                           return;
                        }
                     }

                     return;
                  } while(!var5.valid);
               } while(var5.max.y == this.maxY && var5.max.z == this.maxZ && var5.min.y == this.minY && var5.min.z == this.minZ);

               var5.valid = false;
               var5.setInvalidReason(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_48 + " [" + var5.min + ", " + var5.max + "] / [(" + this.minX + ", " + this.minY + ", " + this.minZ + "), (" + this.maxX + ", " + this.maxY + ", " + this.maxZ + ")]");
               this.unitsValid = false;
            }
         }
      }
   }

   private int getMaxArcDistance() {
      return ShipyardElementManager.ARC_MAX_SPACING;
   }

   public float getPowerConsumption() {
      return DEBUG_MODE ? 0.0F : (float)this.getTotalSize() * ShipyardElementManager.POWER_COST_NEEDED_PER_BLOCK;
   }

   private double getReactorPowerUsage() {
      double var1 = (double)ShipyardElementManager.REACTOR_POWER_CONSUMPTION_RESTING * (double)this.getTotalSize();
      return this.getConfigManager().apply(StatusEffectType.WARP_POWER_EFFICIENCY, var1);
   }

   public double getPowerConsumedPerSecondResting() {
      return this.getReactorPowerUsage();
   }

   public double getPowerConsumedPerSecondCharging() {
      return this.getReactorPowerUsage();
   }

   public boolean isPowerCharging(long var1) {
      return true;
   }

   public void setPowered(float var1) {
      this.powered = var1;
   }

   public float getPowered() {
      return this.powered;
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.SHIPYARD;
   }

   public void reloadFromReactor(double var1, Timer var3, float var4, boolean var5, float var6) {
   }

   public boolean isPowerConsumerActive() {
      return true;
   }

   public boolean isUsingIntegrity() {
      return false;
   }

   public String toString() {
      return "ShipColMan(" + this.getControllerPos() + ")";
   }

   public void update(Timer var1) {
      boolean var2 = var1.currentTime - this.lastFSMUpdate > 100L;
      boolean var3 = this.isValid();
      if (this.getSegmentController().isUsingPowerReactors()) {
         this.isPowered = this.getPowered() > 0.9999999F;
      } else {
         PowerAddOn var4 = ((PowerManagerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getPowerAddOn();
         float var5 = this.getPowerConsumption() * 0.1F;
         if (!var4.canConsumePowerInstantly(var5)) {
            this.isPowered = false;
         } else {
            this.isPowered = true;
            if (var2 && var3) {
               var4.consumePowerInstantly((double)var5);
            }
         }
      }

      if (this.getSegmentController().isOnServer()) {
         if (this.wasValidLogCheck && !var3) {
            LogUtil.sy().fine(this.getSegmentController() + "; " + this + " " + this.getCurrentStateServer() + " no longer valid");
         } else if (!this.wasValidLogCheck && var3) {
            LogUtil.sy().fine(this.getSegmentController() + "; " + this + " " + this.getCurrentStateServer() + " is now valid again");
         }
      }

      this.wasValidLogCheck = var3;
      if (var2 && var3) {
         this.lastFSMUpdate = var1.currentTime;
      }

      if (!this.getSegmentController().isOnServer()) {
         SegmentController var14;
         boolean var15;
         if (var15 = (var14 = this.getCurrentDocked()) != null) {
            float var6 = -1.0F;
            if (var14.getTotalElements() > 50000) {
               var6 = 100.0F;
            }

            if (var14.isVirtualBlueprint() && this.currentClientState == stateClass2Ids.getByte(Constructing.class)) {
               if (var6 > 0.0F) {
                  var14.percentageDrawn = Math.max(0.01F, (float)((int)((float)this.getCompletionOrderPercent() * var6)) / var6);
               } else {
                  var14.percentageDrawn = Math.max(0.01F, (float)this.getCompletionOrderPercent());
               }
            } else if (!var14.isVirtualBlueprint() && this.currentClientState == stateClass2Ids.getByte(Deconstructing.class)) {
               if (var6 > 0.0F) {
                  var14.percentageDrawn = 1.0F - Math.max(0.01F, (float)((int)((float)this.getCompletionOrderPercent() * var6)) / var6);
               } else {
                  var14.percentageDrawn = 1.0F - (float)this.getCompletionOrderPercent();
               }
            } else {
               var14.percentageDrawn = 1.0F;
            }
         }

         SegmentPiece var20;
         if (this.getConnectedCorePositionValid() && (var20 = this.getSegmentController().getSegmentBuffer().getPointUnsave(this.getConnectedCorePositionBlock4())) != null) {
            ((DrawableRemoteSegment)var20.getSegment()).exceptDockingBlock = var15;
            if (((DrawableRemoteSegment)var20.getSegment()).exceptDockingBlock != ((DrawableRemoteSegment)var20.getSegment()).exceptDockingBlockMarked) {
               ((DrawableRemoteSegment)var20.getSegment()).setNeedsMeshUpdate(true);
               ((DrawableRemoteSegment)var20.getSegment()).exceptDockingBlockMarked = ((DrawableRemoteSegment)var20.getSegment()).exceptDockingBlock;
            }
         }
      }

      if (var1.currentTime - this.lastDockValidTest > 500L) {
         this.setDockingValid(this.isCurrentDockedValid());
         this.lastDockValidTest = var1.currentTime;
         if (this.getSegmentController().isOnServer()) {
            if (this.wasDockingValid && !this.isDockingValid()) {
               LogUtil.sy().fine(this.getSegmentController() + "; " + this + " " + this.getCurrentStateServer() + ": docking no longer valid: " + this.getCurrentDocked());
            } else if (!this.wasDockingValid && this.isDockingValid()) {
               LogUtil.sy().fine(this.getSegmentController() + "; " + this + " " + this.getCurrentStateServer() + ": docking is now valid: " + this.getCurrentDocked());
            }

            this.wasDockingValid = this.isDockingValid();
         }
      }

      if (!this.getSegmentController().isOnServer() && (this.currentClientState < 0 && var1.currentTime - this.lastStateRequest > 500L || ((GameClientState)this.getState()).getCurrentSectorId() == this.getSegmentController().getSectorId() && var1.currentTime - this.lastStateRequest > 10000L)) {
         this.sendStateRequestToServer(ShipyardCollectionManager.ShipyardRequestType.STATE);
         this.lastStateRequest = var1.currentTime;
      }

      if (var3) {
         this.wasValid = true;
         if (this.getSegmentController().isOnServer()) {
            if (!this.isPowered()) {
               if (this.wasPowered) {
                  LogUtil.sy().fine(this.getSegmentController() + "; " + this + " " + this.getCurrentStateServer() + ": no longer powered");
               }
            } else if (!this.wasPowered) {
               LogUtil.sy().fine(this.getSegmentController() + "; " + this + " " + this.getCurrentStateServer() + ": is now powered");
            }
         }

         this.wasPowered = this.isPowered();
         if (this.getSegmentController().isOnServer() && this.isPowered() && var2) {
            if (this.lastServerState != this.getCurrentStateServer()) {
               LogUtil.sy().fine(this.getSegmentController() + "; " + this + " updating now in state: " + this.getCurrentStateServer());
            }

            this.lastServerState = this.getCurrentStateServer();
            if (this.getCurrentStateServer() != null && this.getCurrentStateServer().isPullingResources()) {
               LogUtil.sy().fine(this.getSegmentController() + "; " + this + " " + this.getCurrentStateServer() + " pulling resources");
               this.getCurrentStateServer().pullResources();
            }

            Object2ObjectOpenHashMap var16 = this.getState().getLocalAndRemoteObjectContainer().getUidObjectMap();
            boolean var21;
            if ((var21 = this.waitForExistsUntilFSMUpdate != null && !var16.containsKey(this.waitForExistsUntilFSMUpdate)) && this.lastServerState != this.getCurrentStateServer()) {
               LogUtil.sy().fine(this.getSegmentController() + "; " + this + " " + this.getCurrentStateServer() + " waiting for design UID: " + this.waitForExistsUntilFSMUpdate);
            }

            SegmentPiece var11;
            if (this.waitForExistsUntilFSMUpdate != null && var16.containsKey(this.waitForExistsUntilFSMUpdate) && ((var11 = ((SegmentController)var16.get(this.waitForExistsUntilFSMUpdate)).getSegmentBuffer().getPointUnsave(Ship.core)) == null || var11.getType() != 1)) {
               var21 = true;
               if (this.lastServerState != this.getCurrentStateServer()) {
                  LogUtil.sy().fine(this.getSegmentController() + "; " + this + " " + this.getCurrentStateServer() + " waiting for design UID core loaded (UID exists): " + this.waitForExistsUntilFSMUpdate);
               }
            }

            if (var21 || this.getCurrentDocked() != null && !this.getCurrentDocked().isFullyLoadedWithDock()) {
               if (this.lastState != -2) {
                  this.sendShipyardStateToClient();
                  this.lastState = -2;
               }

               if (this.lastServerState != this.getCurrentStateServer()) {
                  LogUtil.sy().fine(this.getSegmentController() + "; " + this + " " + this.getCurrentStateServer() + " waiting for design fully loaded with dock (UID exists): " + this.waitForExistsUntilFSMUpdate + "; DOCK: " + this.getCurrentDocked());
                  return;
               }
            } else {
               this.waitForExistsUntilFSMUpdate = null;

               try {
                  assert this.getServerEntityState().getCurrentProgram() != null;

                  assert !this.getServerEntityState().getCurrentProgram().isSuspended();

                  this.getServerEntityState().updateOnActive(var1);
               } catch (FSMException var7) {
                  var7.printStackTrace();
                  LogUtil.sy().log(Level.WARNING, this.getSegmentController() + "; " + this + " " + this.getCurrentStateServer() + " FSM Exception: " + var7.getClass() + "; " + var7.getMessage(), var7);
               }

               if (this.getCurrentDocked() != null && !this.isDockingValid()) {
                  LogUtil.sy().fine(this.getSegmentController() + "; " + this + " " + this.getCurrentStateServer() + " current dock invalid: " + this.getCurrentDocked());
                  if (this.lastState != -3) {
                     this.sendShipyardStateToClient();
                     this.lastState = -3;
                     return;
                  }
               } else {
                  if (this.getServerEntityState().getCurrentProgram().getMachine() != null && this.getServerEntityState().getCurrentProgram().getMachine().getFsm().getCurrentState() != null) {
                     State var8 = this.getServerEntityState().getCurrentProgram().getMachine().getFsm().getCurrentState();
                     if (stateClass2Ids.get(var8.getClass()) != this.lastState) {
                        LogUtil.sy().fine(this.getSegmentController() + "; " + this + " " + this.getCurrentStateServer() + " sending current state to client: " + var8);
                        this.sendShipyardStateToClient();
                        this.lastState = stateClass2Ids.get(var8.getClass());
                     }

                     return;
                  }

                  LogUtil.sy().severe(this.getSegmentController() + "; " + this + " " + this.getCurrentStateServer() + " SHIPYARD IN INVALID STATE! " + this.getServerEntityState().getCurrentProgram().getMachine() + "; " + (this.getServerEntityState().getCurrentProgram().getMachine() != null ? this.getServerEntityState().getCurrentProgram().getMachine().getFsm().getCurrentState() : ""));

                  assert false;

                  if (this.lastState != -1) {
                     this.sendShipyardStateToClient();
                     this.lastState = -1;
                  }
               }
            }

            return;
         }
      } else {
         if (this.wasValid && this.getSegmentController().isOnServer()) {
            State var17 = this.getServerEntityState().getCurrentProgram().getMachine().getStartState();
            this.getServerEntityState().getCurrentProgram().getMachine().setState(var17);
            var17.setNewState(true);
            this.wasValid = false;
            this.setCurrentDesign(-1);
            this.sendShipyardStateToClient();
            LogUtil.sy().fine(this.getSegmentController() + "; " + this + " " + this.getCurrentStateServer() + " shipyard no longer valid: " + this.getCurrentDocked() + ": start state: " + var17);
         }

         if (!this.getSegmentController().isOnServer() && ((GameClientState)this.getSegmentController().getState()).getCurrentSectorId() == this.getSegmentController().getSectorId() && this.getTotalSize() > 0 && System.currentTimeMillis() - this.lastPopup > 5000L) {
            Iterator var18 = this.getElementCollections().iterator();

            RaisingIndication var10;
            while(var18.hasNext()) {
               ShipyardUnit var22;
               if (!(var22 = (ShipyardUnit)var18.next()).isValid() && var22.getInvalidReason() != null) {
                  Transform var12;
                  (var12 = new Transform()).setIdentity();
                  Vector3i var9 = ElementCollection.getPosFromIndex(var22.endA, new Vector3i());
                  var12.origin.set((float)(var9.x - 16), (float)(var9.y - 16), (float)(var9.z - 16));
                  this.getSegmentController().getWorldTransform().transform(var12.origin);
                  (var10 = new RaisingIndication(var12, var22.getInvalidReason(), 1.0F, 0.3F, 0.3F, 1.0F)).speed = 0.1F;
                  var10.lifetime = 4.6F;
                  HudIndicatorOverlay.toDrawTexts.add(var10);
               }
            }

            Transform var19;
            (var19 = new Transform()).setIdentity();
            Vector3i var23 = this.getControllerPos();
            var19.origin.set((float)(var23.x - 16), (float)(var23.y - 16), (float)(var23.z - 16));
            this.getSegmentController().getWorldTransform().transform(var19.origin);
            String var13;
            if (!this.unitsValid) {
               var13 = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_59;
            } else if (!this.getConnectedCorePositionValid()) {
               var13 = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_57;
            } else if (!this.isPowered()) {
               var13 = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_58;
            } else {
               var13 = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_56;
            }

            (var10 = new RaisingIndication(var19, var13, 1.0F, 0.1F, 0.1F, 1.0F)).speed = 0.03F;
            var10.lifetime = 6.6F;
            HudIndicatorOverlay.toDrawTexts.add(var10);
            this.lastPopup = System.currentTimeMillis();
         }
      }

   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[]{new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_60, StringTools.formatPointZero(this.getPowerConsumption())), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_61, this.getMaxArcDistance())};
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_62;
   }

   public boolean isValid() {
      return this.alwaysValid() | (this.unitsValid && this.getConnectedCorePositionValid());
   }

   private boolean getConnectedCorePositionValid() {
      long var1;
      if ((var1 = this.getConnectedCorePositionBlock4()) == Long.MIN_VALUE) {
         return false;
      } else {
         int var3 = ElementCollection.getPosX(var1);
         int var4 = ElementCollection.getPosY(var1);
         int var5 = ElementCollection.getPosZ(var1);
         return var3 >= this.minX && var4 >= this.minY && var5 >= this.minZ && var3 < this.maxX && var4 < this.maxY && var5 < this.maxZ;
      }
   }

   public int compare(ShipyardUnit var1, ShipyardUnit var2) {
      return var2.normalPos - var1.normalPos;
   }

   public boolean isCurrentDockedValid() {
      SegmentController var1;
      if ((var1 = this.getCurrentDocked()) != null && var1.getSegmentBuffer().getBoundingBox().isValid()) {
         if (DEBUG_MODE) {
            return true;
         } else {
            Transform var2 = new Transform(var1.getPhysicsDataContainer().getShapeChild().transform);
            this.minbb.set(var1.getSegmentBuffer().getBoundingBox().min);
            this.maxbb.set(var1.getSegmentBuffer().getBoundingBox().max);
            ++this.minbb.x;
            ++this.minbb.y;
            ++this.minbb.z;
            --this.maxbb.x;
            --this.maxbb.y;
            --this.maxbb.z;
            var2.transform(this.minbb);
            var2.transform(this.maxbb);
            this.minbbOut.x = Math.min(this.minbb.x, this.maxbb.x);
            this.minbbOut.y = Math.min(this.minbb.y, this.maxbb.y);
            this.minbbOut.z = Math.min(this.minbb.z, this.maxbb.z);
            this.maxbbOut.x = Math.max(this.minbb.x, this.maxbb.x);
            this.maxbbOut.y = Math.max(this.minbb.y, this.maxbb.y);
            this.maxbbOut.z = Math.max(this.minbb.z, this.maxbb.z);
            Vector3f var10000 = this.minbbOut;
            var10000.x += 17.0F;
            var10000 = this.minbbOut;
            var10000.y += 17.0F;
            var10000 = this.minbbOut;
            var10000.z += 17.0F;
            var10000 = this.maxbbOut;
            var10000.x += 16.0F;
            var10000 = this.maxbbOut;
            var10000.y += 16.0F;
            var10000 = this.maxbbOut;
            var10000.z += 16.0F;
            this.minbbOut.x = (float)Math.round(this.minbbOut.x);
            this.minbbOut.y = (float)Math.round(this.minbbOut.y);
            this.minbbOut.z = (float)Math.round(this.minbbOut.z);
            this.maxbbOut.x = (float)Math.round(this.maxbbOut.x);
            this.maxbbOut.y = (float)Math.round(this.maxbbOut.y);
            this.maxbbOut.z = (float)Math.round(this.maxbbOut.z);
            if (this.biggest == 0) {
               return this.minbbOut.x >= (float)this.minX && this.maxbbOut.x <= (float)this.maxX;
            } else if (this.biggest == 1) {
               return this.minbbOut.y >= (float)this.minY && this.maxbbOut.y <= (float)this.maxY;
            } else if (this.biggest == 2) {
               return this.minbbOut.z >= (float)this.minZ && this.maxbbOut.z <= (float)this.maxZ;
            } else {
               return this.minbbOut.x >= (float)this.minX && this.minbbOut.y >= (float)this.minY && this.minbbOut.z >= (float)this.minZ && this.maxbbOut.x <= (float)this.maxX && this.maxbbOut.y <= (float)this.maxY && this.maxbbOut.z <= (float)this.maxZ;
            }
         }
      } else {
         return true;
      }
   }

   public SegmentController getCurrentDocked() {
      long var1;
      if ((var1 = this.getConnectedCorePositionBlock4()) != Long.MIN_VALUE) {
         Iterator var3 = this.getSegmentController().railController.next.iterator();

         while(var3.hasNext()) {
            RailRelation var4;
            if ((var4 = (RailRelation)var3.next()).rail.getAbsoluteIndexWithType4() == var1) {
               return var4.docked.getSegmentController();
            }
         }
      }

      return null;
   }

   public LongOpenHashSet getConnectedCorePositionBlockMap() {
      Short2ObjectOpenHashMap var1;
      return (var1 = this.getSegmentController().getControlElementMap().getControllingMap().get(this.getControllerElement().getAbsoluteIndex())) != null ? (LongOpenHashSet)var1.get((short)679) : null;
   }

   public long getConnectedCorePositionBlock4() {
      LongOpenHashSet var1;
      return (var1 = this.getConnectedCorePositionBlockMap()) != null && var1.size() == 1 ? var1.iterator().nextLong() : Long.MIN_VALUE;
   }

   public Inventory getInventory() {
      return this.getContainer().getInventory(this.getControllerElement().getAbsoluteIndex());
   }

   public ShipyardEntityState getServerEntityState() {
      return this.serverEntityState;
   }

   public ShipyardState getCurrentStateServer() {
      return this.getServerEntityState().getCurrentProgram() != null && this.getServerEntityState().getCurrentProgram().getMachine() != null && this.getServerEntityState().getCurrentProgram().getMachine().getFsm() != null && this.getServerEntityState().getCurrentProgram().getMachine().getFsm().getCurrentState() != null ? (ShipyardState)this.getServerEntityState().getCurrentProgram().getMachine().getFsm().getCurrentState() : null;
   }

   public void handleShipyardCommandOnServer(int var1, ShipyardCollectionManager.ShipyardCommandType var2, Object[] var3) {
      if (this.getServerEntityState().getCurrentProgram() != null && this.getServerEntityState().getCurrentProgram().getMachine() != null && this.getServerEntityState().getCurrentProgram().getMachine().getFsm() != null && this.getServerEntityState().getCurrentProgram().getMachine().getFsm().getCurrentState() != null) {
         this.getServerEntityState().lastOrderFactionId = var1;
         State var5 = this.getServerEntityState().getCurrentProgram().getMachine().getFsm().getCurrentState();
         if (var2.requiredState.equals(var5.getClass())) {
            try {
               if (this.canExecute(var2)) {
                  this.serverEntityState.handle(var2, var3);
                  this.getServerEntityState().getCurrentProgram().getMachine().getFsm().stateTransition(var2.transition);
               }

            } catch (FSMException var4) {
               var4.printStackTrace();
               this.sendShipyardErrorToClient(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_63, StringTools.wrap(var4.getMessage(), 100)));
            }
         } else {
            this.sendShipyardErrorToClient(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_64, var2.requiredState.getSimpleName(), var5.getClass().getSimpleName()));

            assert false : var2.name() + "; CURRENT: " + var5.getClass().getSimpleName() + "; REQUIRED: " + var2.requiredState.getSimpleName();

         }
      } else {
         this.sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_65);

         assert false;

      }
   }

   private boolean canExecute(ShipyardCollectionManager.ShipyardCommandType var1) {
      return true;
   }

   public void sendShipyardStateToClient() {
      assert this.getSegmentController().isOnServer();

      ShipyardCurrentStateValueUpdate var1 = new ShipyardCurrentStateValueUpdate();

      assert var1.getType() == ValueUpdate.ValTypes.SHIPYARD_STATE_UPDATE;

      var1.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), this.getControllerElement().getAbsoluteIndex());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   public void sendShipyardGoalToClient() {
      assert this.getSegmentController().isOnServer();

      ShipyardBlockGoalValueUpdate var1 = new ShipyardBlockGoalValueUpdate();

      assert var1.getType() == ValueUpdate.ValTypes.SHIPYARD_BLOCK_GOAL;

      var1.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), this.getControllerElement().getAbsoluteIndex());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   public void sendShipyardErrorToClient(String var1) {
      assert this.getSegmentController().isOnServer();

      ShipyardErrorValueUpdate var2 = new ShipyardErrorValueUpdate(var1);

      assert var2.getType() == ValueUpdate.ValTypes.SHIPYARD_ERROR_UPDATE;

      var2.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), this.getControllerElement().getAbsoluteIndex());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var2, this.getSegmentController().isOnServer()));
   }

   public void sendShipyardCommandToServer(int var1, ShipyardCollectionManager.ShipyardCommandType var2, Object... var3) {
      assert !this.getSegmentController().isOnServer();

      ShipyardClientCommandValueUpdate var4 = new ShipyardClientCommandValueUpdate(new ShipyardCommand(var1, var2, var3));

      assert var4.getType() == ValueUpdate.ValTypes.SHIPYARD_CLIENT_COMMAND;

      var4.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), this.getControllerElement().getAbsoluteIndex());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var4, this.getSegmentController().isOnServer()));
   }

   public void sendStateRequestToServer(ShipyardCollectionManager.ShipyardRequestType var1) {
      assert !this.getSegmentController().isOnServer();

      ShipyardClientStateRequestValueUpdate var2 = new ShipyardClientStateRequestValueUpdate(var1);

      assert var2.getType() == ValueUpdate.ValTypes.SHIPYARD_CLIENT_STATE_REQUEST;

      var2.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), this.getControllerElement().getAbsoluteIndex());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var2, this.getSegmentController().isOnServer()));
   }

   public boolean isWorkingOnOrder() {
      return this.completionOrderPercent >= 0.0D;
   }

   public double getCompletionOrderPercent() {
      return this.completionOrderPercent;
   }

   public List getGUIInventoryDesignList() {
      ObjectArrayList var1 = new ObjectArrayList();
      Iterator var2 = this.getInventory().getSlots().iterator();

      while(var2.hasNext()) {
         int var3 = (Integer)var2.next();
         if ((var3 = this.getInventory().getMeta(var3)) >= 0) {
            MetaObject var4;
            if ((var4 = ((MetaObjectState)this.getSegmentController().getState()).getMetaObjectManager().getObject(var3)) == null) {
               if (this.requestedMeta.contains(var3)) {
                  ((MetaObjectState)this.getSegmentController().getState()).getMetaObjectManager().checkAvailable(var3, (MetaObjectState)this.getSegmentController().getState());
                  this.requestedMeta.add(var3);
               }
            } else if (var4 instanceof VirtualBlueprintMetaItem) {
               VirtualBlueprintMetaItem var6 = (VirtualBlueprintMetaItem)var4;
               GUIAncor var7 = new GUIAncor((GameClientState)this.getState(), 300.0F, 24.0F);
               GUITextOverlay var5;
               (var5 = new GUITextOverlay(300, 24, (GameClientState)this.getState())).setTextSimple(var6.virtualName);
               var5.setPos(5.0F, 4.0F, 0.0F);
               var7.attach(var5);
               var7.setUserPointer(var6.getId());
               var1.add(var7);
            }
         }
      }

      Collections.sort(var1, new Comparator() {
         public int compare(GUIElement var1, GUIElement var2) {
            GUITextOverlay var3 = (GUITextOverlay)var1.getChilds().get(0);
            GUITextOverlay var4 = (GUITextOverlay)var2.getChilds().get(0);
            return var3.getText().get(0).toString().toLowerCase(Locale.ENGLISH).compareTo(var4.getText().get(0).toString().toLowerCase(Locale.ENGLISH));
         }
      });
      return var1;
   }

   public List getGUIInventoryBlueprintList() {
      ObjectArrayList var1 = new ObjectArrayList();
      Iterator var2 = this.getInventory().getSlots().iterator();

      while(var2.hasNext()) {
         int var3 = (Integer)var2.next();
         if ((var3 = this.getInventory().getMeta(var3)) >= 0) {
            MetaObject var4;
            if ((var4 = ((MetaObjectState)this.getSegmentController().getState()).getMetaObjectManager().getObject(var3)) == null) {
               if (this.requestedMeta.contains(var3)) {
                  ((MetaObjectState)this.getSegmentController().getState()).getMetaObjectManager().checkAvailable(var3, (MetaObjectState)this.getSegmentController().getState());
                  this.requestedMeta.add(var3);
               }
            } else if (var4 instanceof BlueprintMetaItem) {
               BlueprintMetaItem var6 = (BlueprintMetaItem)var4;
               GUIAncor var7 = new GUIAncor((GameClientState)this.getState(), 300.0F, 24.0F);
               GUITextOverlay var5;
               (var5 = new GUITextOverlay(300, 24, (GameClientState)this.getState())).setTextSimple(var6.blueprintName);
               var5.setPos(5.0F, 4.0F, 0.0F);
               var7.attach(var5);
               var7.setUserPointer(var6.blueprintName);
               var1.add(var7);
            }
         }
      }

      Collections.sort(var1, new Comparator() {
         public int compare(GUIElement var1, GUIElement var2) {
            GUITextOverlay var3 = (GUITextOverlay)var1.getChilds().get(0);
            GUITextOverlay var4 = (GUITextOverlay)var2.getChilds().get(0);
            return var3.getText().get(0).toString().toLowerCase(Locale.ENGLISH).compareTo(var4.getText().get(0).toString().toLowerCase(Locale.ENGLISH));
         }
      });
      return var1;
   }

   public boolean setCompletionOrderPercent(double var1) {
      this.completionOrderPercent = var1;
      if ((int)(var1 * 100.0D) != this.lastIntPercent) {
         this.lastIntPercent = (int)(var1 * 100.0D);
         return true;
      } else {
         return false;
      }
   }

   public boolean setCompletionOrderPercentAndSendIfChanged(double var1) {
      boolean var3;
      if (var3 = this.setCompletionOrderPercent(var1)) {
         this.sendShipyardStateToClient();
      }

      return var3;
   }

   public byte getStateByteOnServer() {
      return this.isValid() && this.getServerEntityState().getCurrentProgram() != null && this.getServerEntityState().getCurrentProgram().getMachine() != null && this.getServerEntityState().getCurrentProgram().getMachine().getFsm() != null && this.getServerEntityState().getCurrentProgram().getMachine().getFsm().getCurrentState() != null ? stateClass2Ids.getByte(this.getServerEntityState().getCurrentProgram().getMachine().getFsm().getCurrentState().getClass()) : -1;
   }

   public String getStateDescription() {
      if (this.currentClientState == -2) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_66;
      } else {
         return this.currentClientState < 0 ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_67 + (this.isValid() ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_68 : Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_69) : (String)ids2StateDescription.get(this.currentClientState);
      }
   }

   public boolean isLoadedDesignValid() {
      SegmentController var1;
      return this.getCurrentDesign() >= 0 && this.isValid() && this.getInventory().conatainsMetaItem(this.getCurrentDesign()) && (var1 = this.getCurrentDocked()) != null && this.getCurrentDesignObject() != null && this.getCurrentDesignObject().UID.equals(var1.getUniqueIdentifier()) && var1.isVirtualBlueprint();
   }

   public String isLoadedDesignValidToSring() {
      if (this.getCurrentDesign() >= 0 && !this.getInventory().conatainsMetaItem(this.getCurrentDesign())) {
         System.err.println("META ITEM MISSING: " + this.getCurrentDesign());
         System.err.println("INVETORY: " + this.getInventory().getParameter() + "; content: " + this.getInventory().toString());
      }

      SegmentController var1;
      return "CurrentDesign ID > 0? " + (this.getCurrentDesign() >= 0) + "; SY valid: " + this.isValid() + "; InventoryHadDesignMeta: " + this.getInventory().conatainsMetaItem(this.getCurrentDesign()) + "; currentDocked != null? " + ((var1 = this.getCurrentDocked()) != null) + "; UID equals: " + (var1 != null && this.getCurrentDesignObject() != null && this.getCurrentDesignObject().UID != null && this.getCurrentDesignObject().UID.equals(var1.getUniqueIdentifier())) + "; isVirtual: " + (var1 != null && var1.isVirtualBlueprint());
   }

   public void unloadCurrentDockedVolatile() {
      SegmentController var1;
      if ((var1 = this.getCurrentDocked()) != null) {
         LogUtil.sy().fine(this.getSegmentController() + "; " + this + " write&unload starting for " + var1);
         var1.setVirtualBlueprintRecursive(true);

         try {
            ((GameServerState)this.getState()).getController().writeSingleEntityWithDock(var1);
         } catch (IOException var2) {
            var2.printStackTrace();
         } catch (SQLException var3) {
            var3.printStackTrace();
         }

         var1.setMarkedForDeleteVolatileIncludingDocks(true);
         LogUtil.sy().fine(this.getSegmentController() + "; " + this + " write&unload successful: design marked for volatile remove: " + var1);
      } else {
         LogUtil.sy().fine(this.getSegmentController() + "; " + this + " cannot unload design since nothing is loaded");
      }
   }

   public SegmentController loadDesign(VirtualBlueprintMetaItem var1) {
      GameServerState var2;
      Sector var3;
      if ((var3 = (var2 = (GameServerState)this.getState()).getUniverse().getSector(this.getSegmentController().getSectorId())) != null) {
         try {
            SegmentController var5;
            if ((var5 = var3.loadSingleEntitiyWithDock(var2, new EntityUID(var1.UID, DatabaseEntry.getEntityType(var1.UID), -1L), true)) != null && var5 instanceof SegmentController) {
               LogUtil.sy().fine(this.getSegmentController() + "; " + this + " design successfully loaded " + var1.UID + "; " + var5 + " -> " + var5);
               return var5;
            }

            LogUtil.sy().fine(this.getSegmentController() + "; " + this + " load error for design " + var1.UID + "; " + var5);
            System.err.println("[SERVER][SHIPYARD] Cannot load design Entity for design not valid in database: " + var1.UID + "; " + var5);
            this.sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_86);
         } catch (SQLException var4) {
            var4.printStackTrace();
            this.sendShipyardErrorToClient(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_87, var4.getClass().getSimpleName(), var4.getMessage()));
         }
      } else {
         LogUtil.sy().fine(this.getSegmentController() + "; " + this + " sector to load design null " + var1.UID);
         System.err.println("[SERVER][SHIPYARD] Cannot load design Entity for design not found in database: " + var1.UID);
         this.sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_88);
      }

      return null;
   }

   public boolean createDockingRelation(SegmentController var1, boolean var2) {
      var1.setVirtualBlueprintRecursive(var2);
      VoidUniqueSegmentPiece var6;
      (var6 = new VoidUniqueSegmentPiece()).setSegmentController(var1);
      var6.setType((short)1);
      var6.voidPos.set(16, 16, 16);
      long var4;
      if ((var4 = this.getConnectedCorePositionBlock4()) != Long.MIN_VALUE && ElementCollection.getType(var4) == 679) {
         SegmentPiece var3;
         if ((var3 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var4)) != null && var3.getType() != 679) {
            System.err.println("[SERVER][SHIPYARD] Cannot create design! No core position block connected to this shipyard");
            this.sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_71);
            return false;
         } else {
            RailRequest var7;
            (var7 = var1.railController.getRailRequest(var6, var3, var3.getAbsolutePos(new Vector3i()), (Vector3i)null, RailRelation.DockingPermission.PUBLIC)).fromtag = true;
            var7.ignoreCollision = true;
            var7.sentFromServer = false;
            var1.railController.railRequestCurrent = var7;
            return true;
         }
      } else {
         System.err.println("[SERVER][SHIPYARD] Cannot create design! No core position block connected to this shipyard!");
         this.sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_70);
         return false;
      }
   }

   public void cancelOrderOnServer() {
      assert this.getSegmentController().isOnServer();

      if (this.getServerEntityState().getCurrentProgram() != null && this.getServerEntityState().getCurrentProgram().getMachine() != null && this.getServerEntityState().getCurrentProgram().getMachine().getFsm() != null && this.getServerEntityState().getCurrentProgram().getMachine().getFsm().getCurrentState() != null) {
         State var1;
         if ((var1 = this.getServerEntityState().getCurrentProgram().getMachine().getFsm().getCurrentState()) instanceof ShipyardState && ((ShipyardState)var1).canCancel()) {
            try {
               var1.stateTransition(Transition.SY_CANCEL);
            } catch (FSMException var2) {
               var2.printStackTrace();
               this.sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_73);
            }
         } else {
            this.sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_72);
         }
      } else {
         this.sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_74);
      }
   }

   public Class getCurrentClientStateClass() {
      assert !this.getSegmentController().isOnServer();

      return (Class)ids2StateClass.get(this.currentClientState);
   }

   public boolean isDockedInEditableState() {
      return idsCanEdit.get(this.currentClientState);
   }

   public boolean isPowered() {
      if (this.getSegmentController().isUsingPowerReactors()) {
         return this.getPowered() >= 1.0F || DEBUG_MODE;
      } else {
         return this.isPowered || DEBUG_MODE;
      }
   }

   public void undockRequestedFromShipyard() {
      SegmentController var1;
      if ((var1 = this.getCurrentDocked()) != null) {
         System.err.println("[SERVER] Manually undocking " + var1);
         this.getServerEntityState().wasUndockedManually = true;
         var1.railController.disconnect();
      }

   }

   public boolean isInstantBuild() {
      return this.instantBuild;
   }

   public boolean isDockingValid() {
      return this.dockingValid;
   }

   public void setDockingValid(boolean var1) {
      this.dockingValid = var1;
   }

   protected void onRemovedCollection(long var1, ShipyardCollectionManager var3) {
      super.onRemovedCollection(var1, var3);
      State var4;
      if (this.getSegmentController().isOnServer() && this.getServerEntityState().isStateSet() && (var4 = this.getServerEntityState().getCurrentProgram().getMachine().getFsm().getCurrentState()) instanceof ShipyardState) {
         ((ShipyardState)var4).onShipyardRemoved(this.getControllerPos());
      }

   }

   public int getCurrentDesign() {
      return this.currentDesign;
   }

   public void setCurrentDesign(int var1) {
      this.currentDesign = var1;
   }

   public void dischargeFully() {
   }

   static {
      stateClass2Ids.defaultReturnValue((byte)-1);
      readBuildHelperClasses();
      DEBUG_MODE = EngineSettings.SECRET.getCurrentState().toString().contains("shipyard");
   }

   public static enum ShipyardCommandType {
      CREATE_NEW_DESIGN(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_1;
         }
      }, new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_2;
         }
      }, new ShipyardCollectionManager.ShipyardCommandDialog() {
         public final void clientSend(final ShipyardCollectionManager var1) {
            PlayerTextInput var2;
            (var2 = new PlayerTextInput("ASK", (GameClientState)var1.getState(), 50, "Name", "Choose a name", "D_" + System.currentTimeMillis()) {
               public String[] getCommandPrefixes() {
                  return null;
               }

               public boolean isOccluded() {
                  return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
               }

               public String handleAutoComplete(String var1x, TextCallback var2, String var3) {
                  return var1x;
               }

               public void onDeactivate() {
               }

               public void onFailedTextCheck(String var1x) {
                  this.setErrorMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_82, var1x));
               }

               public boolean onInput(String var1x) {
                  var1.sendShipyardCommandToServer(((GameClientState)this.getState()).getPlayer().getFactionId(), ShipyardCollectionManager.ShipyardCommandType.CREATE_NEW_DESIGN, var1x);
                  return true;
               }
            }).setInputChecker(ShipyardCollectionManager.shipInputChecker);
            var2.activate();
         }
      }, WaitingForShipyardOrder.class, Transition.SY_CREATE_DESIGN, new Class[]{String.class}),
      UNLOAD_DESIGN(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_4;
         }
      }, new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_5;
         }
      }, new ShipyardCollectionManager.ShipyardCommandDialog() {
         public final void clientSend(ShipyardCollectionManager var1) {
            var1.sendShipyardCommandToServer(((GameClientState)var1.getState()).getPlayer().getFactionId(), ShipyardCollectionManager.ShipyardCommandType.UNLOAD_DESIGN);
         }
      }, DesignLoaded.class, Transition.SY_UNLOAD_DESIGN, new Class[0]),
      LOAD_DESIGN(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_6;
         }
      }, new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_7;
         }
      }, new ShipyardCollectionManager.ShipyardCommandDialog() {
         public final void clientSend(final ShipyardCollectionManager var1) {
            List var2;
            if ((var2 = var1.getGUIInventoryDesignList()).isEmpty()) {
               ((GameClientState)var1.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_39, 0.0F);
            } else {
               (new PlayerGameDropDownInput("SELECT_DESIGN", (GameClientState)var1.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_40, 24, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_10, var2) {
                  public void onDeactivate() {
                  }

                  public boolean isOccluded() {
                     return false;
                  }

                  public void pressedOK(GUIListElement var1x) {
                     var1.sendShipyardCommandToServer(((GameClientState)var1.getState()).getPlayer().getFactionId(), ShipyardCollectionManager.ShipyardCommandType.LOAD_DESIGN, (Integer)var1x.getContent().getUserPointer());
                     this.deactivate();
                  }
               }).activate();
            }
         }
      }, WaitingForShipyardOrder.class, Transition.SY_LOAD_DESIGN, new Class[]{Integer.class}),
      DECONSTRUCT(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_11;
         }
      }, new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_12;
         }
      }, new ShipyardCollectionManager.ShipyardCommandDialog() {
         public final void clientSend(final ShipyardCollectionManager var1) {
            PlayerTextInput var2;
            (var2 = new PlayerTextInput("ASK", (GameClientState)var1.getState(), 50, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_80, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_84, "D_" + System.currentTimeMillis()) {
               public String[] getCommandPrefixes() {
                  return null;
               }

               public boolean isOccluded() {
                  return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
               }

               public String handleAutoComplete(String var1x, TextCallback var2, String var3) {
                  return var1x;
               }

               public void onDeactivate() {
               }

               public void onFailedTextCheck(String var1x) {
                  this.setErrorMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_3, var1x));
               }

               public boolean onInput(String var1x) {
                  var1.sendShipyardCommandToServer(((GameClientState)var1.getState()).getPlayer().getFactionId(), ShipyardCollectionManager.ShipyardCommandType.DECONSTRUCT, var1x);
                  return true;
               }
            }).setInputChecker(ShipyardCollectionManager.shipInputChecker);
            var2.activate();
         }
      }, NormalShipLoaded.class, Transition.SY_DECONSTRUCT, new Class[]{String.class}),
      DECONSTRUCT_RECYCLE(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_14;
         }
      }, new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_15;
         }
      }, new ShipyardCollectionManager.ShipyardCommandDialog() {
         public final void clientSend(final ShipyardCollectionManager var1) {
            (new PlayerGameOkCancelInput("CONFIRM", (GameClientState)var1.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_16, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_17) {
               public boolean isOccluded() {
                  return false;
               }

               public void onDeactivate() {
               }

               public void pressedOK() {
                  var1.sendShipyardCommandToServer(((GameClientState)var1.getState()).getPlayer().getFactionId(), ShipyardCollectionManager.ShipyardCommandType.DECONSTRUCT_RECYCLE);
                  this.deactivate();
               }
            }).activate();
         }
      }, NormalShipLoaded.class, Transition.SY_DECONSTRUCT_RECYCLE, new Class[0]),
      SPAWN_DESIGN(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_18;
         }
      }, new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_19;
         }
      }, new ShipyardCollectionManager.ShipyardCommandDialog() {
         public final void clientSend(final ShipyardCollectionManager var1) {
            PlayerTextInput var2;
            (var2 = new PlayerTextInput("ASK", (GameClientState)var1.getState(), 50, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_83, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_81, "D_" + System.currentTimeMillis()) {
               public String[] getCommandPrefixes() {
                  return null;
               }

               public boolean isOccluded() {
                  return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
               }

               public String handleAutoComplete(String var1x, TextCallback var2, String var3) {
                  return var1x;
               }

               public void onDeactivate() {
               }

               public void onFailedTextCheck(String var1x) {
                  this.setErrorMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_85, var1x));
               }

               public boolean onInput(String var1x) {
                  var1.sendShipyardCommandToServer(((GameClientState)var1.getState()).getPlayer().getFactionId(), ShipyardCollectionManager.ShipyardCommandType.SPAWN_DESIGN, var1x);
                  return true;
               }
            }).setInputChecker(ShipyardCollectionManager.shipInputChecker);
            var2.activate();
         }
      }, DesignLoaded.class, Transition.SY_SPAWN_DESIGN, new Class[]{String.class}),
      CATALOG_TO_DESIGN(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_22;
         }
      }, new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_75;
         }
      }, new ShipyardCollectionManager.ShipyardCommandDialog() {
         public final void clientSend(final ShipyardCollectionManager var1) {
            List var2;
            if ((var2 = var1.getCatalog()).isEmpty()) {
               ((GameClientState)var1.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_76, 0.0F);
            } else {
               (new PlayerGameDropDownInput("SELECT_BB", (GameClientState)var1.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_79, 24, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_77, var2) {
                  public void onDeactivate() {
                  }

                  public boolean isOccluded() {
                     return false;
                  }

                  public void pressedOK(final GUIListElement var1x) {
                     PlayerTextInput var2;
                     (var2 = new PlayerTextInput("ASK", (GameClientState)var1.getState(), 50, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_20, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_21, "D_" + System.currentTimeMillis()) {
                        public String[] getCommandPrefixes() {
                           return null;
                        }

                        public boolean isOccluded() {
                           return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
                        }

                        public String handleAutoComplete(String var1xx, TextCallback var2, String var3) {
                           return var1xx;
                        }

                        public void onDeactivate() {
                        }

                        public void onFailedTextCheck(String var1xx) {
                           this.setErrorMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_13, var1xx));
                        }

                        public boolean onInput(String var1xx) {
                           var1.sendShipyardCommandToServer(((GameClientState)var1.getState()).getPlayer().getFactionId(), ShipyardCollectionManager.ShipyardCommandType.CATALOG_TO_DESIGN, var1xx, var1x.getContent().getUserPointer());
                           return true;
                        }
                     }).setInputChecker(ShipyardCollectionManager.shipInputChecker);
                     var2.activate();
                     this.deactivate();
                  }
               }).activate();
            }
         }
      }, WaitingForShipyardOrder.class, Transition.SY_CONVERT_BLUEPRINT_TO_DESIGN, new Class[]{String.class, String.class}),
      BLUEPRINT_TO_DESIGN(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_78;
         }
      }, new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_23;
         }
      }, new ShipyardCollectionManager.ShipyardCommandDialog() {
         public final void clientSend(final ShipyardCollectionManager var1) {
            List var2;
            if ((var2 = var1.getGUIInventoryBlueprintList()).isEmpty()) {
               ((GameClientState)var1.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_24, 0.0F);
            } else {
               (new PlayerGameDropDownInput("SELECT_BB", (GameClientState)var1.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_25, 24, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_26, var2) {
                  public void onDeactivate() {
                  }

                  public boolean isOccluded() {
                     return false;
                  }

                  public void pressedOK(final GUIListElement var1x) {
                     PlayerTextInput var2;
                     (var2 = new PlayerTextInput("ASK", (GameClientState)var1.getState(), 50, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_32, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_28, "D_" + System.currentTimeMillis()) {
                        public String[] getCommandPrefixes() {
                           return null;
                        }

                        public boolean isOccluded() {
                           return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
                        }

                        public String handleAutoComplete(String var1xx, TextCallback var2, String var3) {
                           return var1xx;
                        }

                        public void onDeactivate() {
                        }

                        public void onFailedTextCheck(String var1xx) {
                           this.setErrorMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_29, var1xx));
                        }

                        public boolean onInput(String var1xx) {
                           var1.sendShipyardCommandToServer(((GameClientState)var1.getState()).getPlayer().getFactionId(), ShipyardCollectionManager.ShipyardCommandType.BLUEPRINT_TO_DESIGN, var1xx, var1x.getContent().getUserPointer());
                           return true;
                        }
                     }).setInputChecker(ShipyardCollectionManager.shipInputChecker);
                     var2.activate();
                     this.deactivate();
                  }
               }).activate();
            }
         }
      }, WaitingForShipyardOrder.class, Transition.SY_CONVERT_BLUEPRINT_TO_DESIGN, new Class[]{String.class, String.class}),
      DESIGN_TO_BLUEPRINT(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_30;
         }
      }, new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_31;
         }
      }, new ShipyardCollectionManager.ShipyardCommandDialog() {
         public final void clientSend(final ShipyardCollectionManager var1) {
            final BlueprintPlayerHandleRequest var2 = new BlueprintPlayerHandleRequest();
            PlayerTextInput var3;
            (var3 = new PlayerTextInput("ASK", (GameClientState)var1.getState(), 50, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_27, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_33, "D_" + System.currentTimeMillis()) {
               public String[] getCommandPrefixes() {
                  return null;
               }

               public boolean isOccluded() {
                  return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
               }

               public String handleAutoComplete(String var1x, TextCallback var2x, String var3) {
                  return var1x;
               }

               public void onDeactivate() {
               }

               public void onFailedTextCheck(String var1x) {
                  this.setErrorMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_34, var1x));
               }

               public boolean onInput(String var1x) {
                  var1.sendShipyardCommandToServer(((GameClientState)var1.getState()).getPlayer().getFactionId(), ShipyardCollectionManager.ShipyardCommandType.DESIGN_TO_BLUEPRINT, var1x, ((GameClientState)this.getState()).getPlayer().getId(), var2.classification.ordinal());
                  return true;
               }
            }).getInputPanel().onInit();
            GUIElement[] var4 = BlueprintClassification.getGUIElements((GameClientState)var1.getState(), SimpleTransformableSendableObject.EntityType.SHIP);
            var2.classification = (BlueprintClassification)var4[0].getUserPointer();
            GUIDropDownList var5;
            (var5 = new GUIDropDownList((GameClientState)var1.getState(), 300, 24, 200, new DropDownCallback() {
               public void onSelectionChanged(GUIListElement var1) {
                  var2.classification = (BlueprintClassification)var1.getContent().getUserPointer();
               }
            }, var4)).setPos(4.0F, 30.0F, 0.0F);
            ((GUIDialogWindow)var3.getInputPanel().getBackground()).getMainContentPane().getContent(0).attach(var5);
            var3.setInputChecker(ShipyardCollectionManager.shipInputChecker);
            var3.activate();
         }
      }, DesignLoaded.class, Transition.SY_CONVERT_TO_BLUEPRINT, new Class[]{String.class, Integer.class, Integer.class}),
      TEST_DESIGN(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_35;
         }
      }, new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_36;
         }
      }, new ShipyardCollectionManager.ShipyardCommandDialog() {
         public final void clientSend(ShipyardCollectionManager var1) {
            var1.sendShipyardCommandToServer(((GameClientState)var1.getState()).getPlayer().getFactionId(), ShipyardCollectionManager.ShipyardCommandType.TEST_DESIGN, ((GameClientState)var1.getState()).getPlayer().getId());
         }
      }, DesignLoaded.class, Transition.SY_TEST_DESIGN, new Class[]{Integer.class}),
      REPAIR_FROM_DESIGN(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_37;
         }
      }, new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_38;
         }
      }, new ShipyardCollectionManager.ShipyardCommandDialog() {
         public final void clientSend(final ShipyardCollectionManager var1) {
            List var2;
            if ((var2 = var1.getGUIInventoryDesignList()).isEmpty()) {
               ((GameClientState)var1.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_8, 0.0F);
            } else {
               (new PlayerGameDropDownInput("SELECT_DESIGN", (GameClientState)var1.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_9, 24, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_SHIPYARDCOLLECTIONMANAGER_41, var2) {
                  public void onDeactivate() {
                  }

                  public boolean isOccluded() {
                     return false;
                  }

                  public void pressedOK(GUIListElement var1x) {
                     var1.sendShipyardCommandToServer(((GameClientState)var1.getState()).getPlayer().getFactionId(), ShipyardCollectionManager.ShipyardCommandType.REPAIR_FROM_DESIGN, (Integer)var1x.getContent().getUserPointer());
                     this.deactivate();
                  }
               }).activate();
            }
         }
      }, NormalShipLoaded.class, Transition.SY_REPAIR_TO_DESIGN, new Class[]{Integer.class});

      public final Class[] args;
      private final Translatable name;
      private final Translatable description;
      public final Transition transition;
      private final ShipyardCollectionManager.ShipyardCommandDialog c;
      private final Class requiredState;

      private ShipyardCommandType(Translatable var3, Translatable var4, ShipyardCollectionManager.ShipyardCommandDialog var5, Class var6, Transition var7, Class... var8) {
         this.name = var3;
         this.args = var8;
         this.description = var4;
         this.c = var5;
         this.requiredState = var6;
         this.transition = var7;
      }

      public final String getName() {
         return this.name.getName(this);
      }

      public final String getDescription() {
         return this.description.getName(this);
      }

      public final void checkMatches(Object[] var1) {
         if (this.args.length != var1.length) {
            throw new IllegalArgumentException("Invalid argument count: Provided: " + Arrays.toString(var1) + ", but needs: " + Arrays.toString(this.args));
         } else {
            for(int var2 = 0; var2 < this.args.length; ++var2) {
               if (!var1[var2].getClass().equals(this.args[var2])) {
                  System.err.println("Not Equal: " + var1[var2] + " and " + this.args[var2]);
                  throw new IllegalArgumentException("Invalid argument on index " + var2 + ": Provided: " + Arrays.toString(var1) + "; cannot take " + var1[var2] + ":" + var1[var2].getClass() + ", it has to be type: " + this.args[var2].getClass());
               }
            }

         }
      }

      public final void addTile(final PlayerButtonTilesInput var1, final ShipyardCollectionManager var2) {
         final GUIActivationCallback var3 = new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return var2.isCommandUsable(ShipyardCommandType.this);
            }
         };
         var1.addTile(this.getName(), this.getDescription(), GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
            public boolean isOccluded() {
               return !var1.isActive() || var3 != null && !var3.isActive((ClientState)var2.getState());
            }

            public void callback(GUIElement var1x, MouseEvent var2x) {
               if (var2x.pressedLeftMouse()) {
                  ShipyardCommandType.this.c.clientSend(var2);
                  var1.deactivate();
                  if (ShipyardCommandType.this == ShipyardCollectionManager.ShipyardCommandType.TEST_DESIGN) {
                     ((GameClientState)var1x.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().inventoryAction((Inventory)null);
                  }
               }

            }
         }, var3);
      }
   }

   public static enum ShipyardRequestType {
      STATE,
      INFO,
      CANCEL,
      UNDOCK;
   }

   public interface ShipyardCommandDialog {
      void clientSend(ShipyardCollectionManager var1);
   }
}
