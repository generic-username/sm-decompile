package org.schema.game.common.controller.elements.shipyard.orders;

import org.schema.game.common.controller.elements.shipyard.orders.states.BlueprintSpawned;
import org.schema.game.common.controller.elements.shipyard.orders.states.Constructing;
import org.schema.game.common.controller.elements.shipyard.orders.states.ConvertingRealToVirtual;
import org.schema.game.common.controller.elements.shipyard.orders.states.CreateBlueprintFromDesign;
import org.schema.game.common.controller.elements.shipyard.orders.states.CreateDesignFromBlueprint;
import org.schema.game.common.controller.elements.shipyard.orders.states.CreatingDesign;
import org.schema.game.common.controller.elements.shipyard.orders.states.Deconstructing;
import org.schema.game.common.controller.elements.shipyard.orders.states.DesignLoaded;
import org.schema.game.common.controller.elements.shipyard.orders.states.LoadingDesign;
import org.schema.game.common.controller.elements.shipyard.orders.states.MovingToTestSite;
import org.schema.game.common.controller.elements.shipyard.orders.states.NormalShipLoaded;
import org.schema.game.common.controller.elements.shipyard.orders.states.RemovingDesign;
import org.schema.game.common.controller.elements.shipyard.orders.states.RemovingPhysical;
import org.schema.game.common.controller.elements.shipyard.orders.states.RevertPullingBlocksFromInventory;
import org.schema.game.common.controller.elements.shipyard.orders.states.SpawningBlueprint;
import org.schema.game.common.controller.elements.shipyard.orders.states.UnloadingDesign;
import org.schema.game.common.controller.elements.shipyard.orders.states.WaitingForShipyardOrder;
import org.schema.schine.ai.MachineProgram;
import org.schema.schine.ai.stateMachines.FiniteStateMachine;
import org.schema.schine.ai.stateMachines.Message;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public class ShipyardMachine extends FiniteStateMachine {
   private WaitingForShipyardOrder waiting;
   private DesignLoaded designLoaded;
   private NormalShipLoaded normalLoaded;
   private LoadingDesign loadingDesign;
   private Constructing spawningDesign;
   private Constructing constructingRepair;
   private Deconstructing deconstructing;
   private Deconstructing deconstructingRepair;

   public ShipyardMachine(ShipyardEntityState var1, MachineProgram var2) {
      super(var1, var2, "");
   }

   public void addTransition(State var1, Transition var2, State var3) {
      var1.addTransition(var2, var3);
   }

   public ShipyardEntityState getObj() {
      return (ShipyardEntityState)super.getObj();
   }

   public void createFSM(String var1) {
      ShipyardEntityState var2 = this.getObj();
      this.waiting = new WaitingForShipyardOrder(var2);
      this.designLoaded = new DesignLoaded(var2);
      this.normalLoaded = new NormalShipLoaded(var2);
      this.normalLoaded.addTransition(Transition.SY_ERROR, this.waiting);
      this.designLoaded.addTransition(Transition.SY_ERROR, this.waiting);
      this.waiting.addTransition(Transition.SY_LOAD_NORMAL, this.normalLoaded);
      this.createDesign(this.waiting, this.designLoaded, var2);
      this.loadDesign(this.waiting, this.designLoaded, var2);
      this.loadNormal(this.waiting, this.normalLoaded, var2);
      this.unloadDesign(this.designLoaded, var2);
      this.deconstructionOrder(this.normalLoaded, this.designLoaded, var2);
      this.saveBlueprint(this.designLoaded, this.designLoaded, var2);
      this.createDesignFromBlueprint(this.waiting, this.designLoaded, var2);
      this.spawnDesign(this.designLoaded, this.normalLoaded, var2);
      this.repairDesign(this.normalLoaded, this.normalLoaded, var2);
      this.testDesign(this.designLoaded, this.waiting, var2);
      this.spawnBlueprint(this.waiting, this.designLoaded, this.normalLoaded, var2);
      this.setStartingState(this.waiting);
   }

   private DesignLoaded createDesign(WaitingForShipyardOrder var1, DesignLoaded var2, ShipyardEntityState var3) {
      CreatingDesign var4 = new CreatingDesign(var3);
      var1.addTransition(Transition.SY_CREATE_DESIGN, var4);
      var4.addTransition(Transition.SY_LOADING_DONE, var2);
      var4.addTransition(Transition.SY_ERROR, var1);
      return var2;
   }

   private DesignLoaded saveBlueprint(DesignLoaded var1, DesignLoaded var2, ShipyardEntityState var3) {
      CreateBlueprintFromDesign var4 = new CreateBlueprintFromDesign(var3);
      var1.addTransition(Transition.SY_CONVERT_TO_BLUEPRINT, var4);
      var4.addTransition(Transition.SY_CONVERSION_DONE, var1);
      var4.addTransition(Transition.SY_ERROR, var1);
      return var2;
   }

   private DesignLoaded createDesignFromBlueprint(WaitingForShipyardOrder var1, DesignLoaded var2, ShipyardEntityState var3) {
      CreateDesignFromBlueprint var4 = new CreateDesignFromBlueprint(var3);
      var1.addTransition(Transition.SY_CONVERT_BLUEPRINT_TO_DESIGN, var4);
      var4.addTransition(Transition.SY_CONVERSION_DONE, var2);
      var4.addTransition(Transition.SY_ERROR, var1);
      return var2;
   }

   private DesignLoaded loadDesign(WaitingForShipyardOrder var1, DesignLoaded var2, ShipyardEntityState var3) {
      this.loadingDesign = new LoadingDesign(var3);
      new RemovingDesign(var3);
      var1.addTransition(Transition.SY_LOAD_DESIGN, this.loadingDesign);
      this.loadingDesign.addTransition(Transition.SY_ERROR, var1);
      this.loadingDesign.addTransition(Transition.SY_LOADING_DONE, var2);
      return var2;
   }

   private NormalShipLoaded loadNormal(WaitingForShipyardOrder var1, NormalShipLoaded var2, ShipyardEntityState var3) {
      var1.addTransition(Transition.SY_LOAD_NORMAL, var2);
      var2.addTransition(Transition.SY_ERROR, var1);
      return var2;
   }

   private void unloadDesign(DesignLoaded var1, ShipyardEntityState var2) {
      UnloadingDesign var3 = new UnloadingDesign(var2);
      var1.addTransition(Transition.SY_UNLOAD_DESIGN, var3);
      var3.addTransition(Transition.SY_UNLOADING_DONE, this.waiting);
      var3.addTransition(Transition.SY_ERROR, this.waiting);
   }

   private DesignLoaded deconstructionOrder(NormalShipLoaded var1, DesignLoaded var2, ShipyardEntityState var3) {
      this.deconstructing = new Deconstructing(var3);
      var1.addTransition(Transition.SY_DECONSTRUCT, this.deconstructing);
      var1.addTransition(Transition.SY_DECONSTRUCT_RECYCLE, this.deconstructing);
      this.deconstructing.addTransition(Transition.SY_ERROR, var1);
      this.deconstructing.addTransition(Transition.SY_CANCEL, var1);
      this.deconstructing.addTransition(Transition.SY_DECONSTRUCTION_DONE, this.loadingDesign);
      this.deconstructing.addTransition(Transition.SY_DECONSTRUCTION_DONE_NO_DESIGN, this.waiting);
      return var2;
   }

   private NormalShipLoaded repairDesign(NormalShipLoaded var1, NormalShipLoaded var2, ShipyardEntityState var3) {
      this.deconstructingRepair = new Deconstructing(var3);
      var1.addTransition(Transition.SY_REPAIR_TO_DESIGN, this.deconstructingRepair);
      this.deconstructingRepair.addTransition(Transition.SY_ERROR, var1);
      this.deconstructingRepair.addTransition(Transition.SY_CANCEL, var1);
      LoadingDesign var4 = new LoadingDesign(var3);
      this.constructingRepair = new Constructing(var3);
      this.deconstructingRepair.addTransition(Transition.SY_DECONSTRUCTION_DONE_NO_DESIGN, var4);
      var4.addTransition(Transition.SY_ERROR, var1);
      var4.addTransition(Transition.SY_LOADING_DONE, this.constructingRepair);
      RevertPullingBlocksFromInventory var5 = new RevertPullingBlocksFromInventory(var3);
      this.constructingRepair.addTransition(Transition.SY_ERROR, var5);
      this.constructingRepair.addTransition(Transition.SY_CANCEL, var5);
      this.constructingRepair.addTransition(Transition.SY_SPAWN_DONE, var2);
      var5.addTransition(Transition.SY_BLOCK_TRANSACTION_FINISHED, var1);
      return var2;
   }

   private void testDesign(DesignLoaded var1, WaitingForShipyardOrder var2, ShipyardEntityState var3) {
      Constructing var4;
      (var4 = new Constructing(var3)).testDesign = true;
      MovingToTestSite var5 = new MovingToTestSite(var3);
      LoadingDesign var6 = new LoadingDesign(var3);
      var1.addTransition(Transition.SY_TEST_DESIGN, var4);
      var4.addTransition(Transition.SY_SPAWN_DONE, var5);
      var4.addTransition(Transition.SY_ERROR, var1);
      var5.addTransition(Transition.SY_MOVING_TO_TEST_SITE_DONE, var6);
      var6.addTransition(Transition.SY_ERROR, var1);
      var6.addTransition(Transition.SY_LOADING_DONE, var2);
      var5.addTransition(Transition.SY_ERROR, var2);
   }

   private NormalShipLoaded spawnDesign(DesignLoaded var1, NormalShipLoaded var2, ShipyardEntityState var3) {
      this.spawningDesign = new Constructing(var3);
      var1.addTransition(Transition.SY_SPAWN_DESIGN, this.spawningDesign);
      RevertPullingBlocksFromInventory var4 = new RevertPullingBlocksFromInventory(var3);
      this.spawningDesign.addTransition(Transition.SY_ERROR, var4);
      this.spawningDesign.addTransition(Transition.SY_CANCEL, var4);
      this.spawningDesign.addTransition(Transition.SY_SPAWN_DONE, var2);
      var4.addTransition(Transition.SY_BLOCK_TRANSACTION_FINISHED, var1);
      return var2;
   }

   private NormalShipLoaded spawnBlueprint(WaitingForShipyardOrder var1, DesignLoaded var2, NormalShipLoaded var3, ShipyardEntityState var4) {
      SpawningBlueprint var5 = new SpawningBlueprint(var4);
      var1.addTransition(Transition.SY_SPAWN_BLUEPRINT, var5);
      BlueprintSpawned var7 = new BlueprintSpawned(var4);
      var5.addTransition(Transition.SY_SPAWN_DONE, var7);
      RemovingPhysical var6 = new RemovingPhysical(var4);
      var7.addTransition(Transition.SY_ERROR, var6);
      ConvertingRealToVirtual var8 = new ConvertingRealToVirtual(var4);
      var7.addTransition(Transition.SY_CONVERT, var8);
      var8.addTransition(Transition.SY_CONVERSION_DONE, var2);
      return var3;
   }

   public void onMsg(Message var1) {
   }

   public State getById(Class var1, boolean var2) {
      if (var1 == this.designLoaded.getClass()) {
         return this.designLoaded;
      } else if (var1 == this.spawningDesign.getClass()) {
         return var2 ? this.constructingRepair : this.spawningDesign;
      } else if (var1 == this.deconstructing.getClass()) {
         return var2 ? this.deconstructingRepair : this.deconstructing;
      } else {
         return null;
      }
   }
}
