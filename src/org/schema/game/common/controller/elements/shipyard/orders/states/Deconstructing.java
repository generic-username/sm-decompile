package org.schema.game.common.controller.elements.shipyard.orders.states;

import org.schema.common.LogUtil;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.shipyard.ShipyardElementManager;
import org.schema.game.common.controller.elements.shipyard.orders.ShipyardEntityState;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.element.meta.VirtualBlueprintMetaItem;
import org.schema.game.common.data.player.inventory.NoSlotFreeException;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;

public class Deconstructing extends ShipyardState {
   private SegmentController currentDocked;
   private boolean waitForUnload;
   private int deconstructTimeMs;
   private int removing;

   public Deconstructing(ShipyardEntityState var1) {
      super(var1);
   }

   public boolean onEnterS() {
      this.currentDocked = this.getEntityState().getCurrentDocked();
      this.getEntityState().currentMapTo.resetAll();
      this.getEntityState().currentMapFrom.resetAll();
      this.waitForUnload = false;
      this.removing = 0;
      if (this.currentDocked != null) {
         this.currentDocked.railController.fillElementCountMapRecursive(this.getEntityState().currentMapFrom);
         this.deconstructTimeMs = ShipyardElementManager.DECONSTRUCTION_CONST_TIME_MS + (int)(ShipyardElementManager.DECONSTRUCTION_MS_PER_BLOCK * (double)this.currentDocked.getTotalElements());
      }

      return false;
   }

   public boolean canCancel() {
      return true;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      if (this.removing != 0) {
         LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Deconstruct: removing flag " + this.removing);
         if (!this.getEntityState().getState().getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(this.removing)) {
            LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Deconstruct: removing flag DONE NO DESIGN");
            this.stateTransition(Transition.SY_DECONSTRUCTION_DONE_NO_DESIGN);
         }
      } else if (this.waitForUnload) {
         LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Deconstruct: waiting for unload " + this.getEntityState().getCurrentDocked());
         if (this.getEntityState().getCurrentDocked() == null) {
            this.currentDocked = null;
            this.stateTransition(Transition.SY_DECONSTRUCTION_DONE);
         } else {
            System.err.println("[SHIPYARD][DECONSTRUCTING] Waiting for unload " + this.currentDocked);
         }
      } else if (this.currentDocked != null && this.currentDocked == this.getEntityState().getCurrentDocked()) {
         if (this.getTickCount() * 1000.0D > (double)this.deconstructTimeMs) {
            LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Deconstruct: TICK");
            if (this.getEntityState().currentMapTo.getExistingTypeCount() == 0) {
               this.getEntityState().currentMapTo.add(this.getEntityState().currentMapFrom);

               assert this.getEntityState().currentMapTo.getExistingTypeCount() > 0;

               LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Adding all block types needed " + this.getEntityState().currentMapTo.getExistingTypeCount());
            } else if (this.getEntityState().currentName != null && !this.getEntityState().isInRepair && !this.getEntityState().getInventory().hasFreeSlot()) {
               LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Failed: No slot fr");
               this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_DECONSTRUCTING_1);
               this.stateTransition(Transition.SY_ERROR);
            } else {
               this.getEntityState().setCompletionOrderPercentAndSendIfChanged(1.0D);
               if (this.getEntityState().currentName != null && !this.getEntityState().isInRepair) {
                  ((GameServerState)this.getEntityState().getState()).getMetaObjectManager();
                  VirtualBlueprintMetaItem var1 = (VirtualBlueprintMetaItem)MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.VIRTUAL_BLUEPRINT, (short)-1, true);
                  LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": deconstruct: created item " + var1);
                  boolean var2 = false;

                  try {
                     int var4 = this.getEntityState().getInventory().getFreeSlot();
                     var1.UID = new String(this.currentDocked.getUniqueIdentifier());
                     var1.virtualName = new String(this.getEntityState().currentName);
                     this.currentDocked.setRealName(this.getEntityState().currentName);
                     LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": deconstruct: forcing current dock load " + this.currentDocked);
                     this.currentDocked.getSegmentProvider().enqueueHightPrio(0, 0, 0, true);

                     assert this.currentDocked.getSegmentBuffer().getPointUnsave(Ship.core).getType() == 1;

                     this.getEntityState().getInventory().put(var4, var1);
                     this.getEntityState().getInventory().sendInventoryModification(var4);
                     this.getEntityState().getShipyardCollectionManager().sendShipyardStateToClient();
                     this.getEntityState().unloadCurrentDockedVolatile();
                     this.waitForUnload = true;
                     this.getEntityState().putInInventoryAndConnected(this.getEntityState().currentMapTo, this.currentDocked.isScrap());
                     this.getEntityState().saveInventories(this.currentDocked);
                     this.getEntityState().currentMapTo.resetAll();
                     this.getEntityState().currentMapFrom.resetAll();
                     System.err.println("[SERVER] CREATED DESIGN " + var1);
                     this.getEntityState().designToLoad = var1.getId();
                     this.getEntityState().currentName = null;
                     LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": deconstruct: created design successful " + var1);
                  } catch (NoSlotFreeException var3) {
                     LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": NO SLOT BUT RESERVED");
                     var3.printStackTrace();
                     this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_DECONSTRUCTING_2);
                     this.stateTransition(Transition.SY_ERROR);
                  }
               } else {
                  LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Save inventories and destroy; CURRENT: " + this.getEntityState().currentName + "; REPAIRING " + this.getEntityState().isInRepair);
                  this.getEntityState().lastDeconstructedBlockCount = (int)Math.min(2147483647L, this.getEntityState().currentMapTo.getTotalAmount());
                  this.getEntityState().putInInventoryAndConnected(this.getEntityState().currentMapTo, this.currentDocked.isScrap());
                  this.getEntityState().saveInventories(this.currentDocked);
                  this.currentDocked.railController.destroyDockedRecursive();
                  this.currentDocked.markForPermanentDelete(true);
                  this.currentDocked.setMarkedForDeleteVolatile(true);
                  this.removing = this.currentDocked.getId();
                  LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Destroying: " + this.currentDocked);
               }
            }
         } else {
            LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Deconstruct: in progress " + this.getTickCount() + "; " + this.deconstructTimeMs / 1000);
            this.getEntityState().setCompletionOrderPercentAndSendIfChanged(this.getTickCount() * 1000.0D / (double)this.deconstructTimeMs);
            if (this.getEntityState().currentName != null && !this.getEntityState().isInRepair && !this.getEntityState().getInventory().hasFreeSlot()) {
               LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Deconstruct: failed: no slot free");
               this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_DECONSTRUCTING_3);
               this.stateTransition(Transition.SY_ERROR);
            }
         }
      } else {
         LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Deconstruct: FAILED NO LONGER DOCKED " + this.currentDocked);
         this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_DECONSTRUCTING_0);
         System.err.println("[SHIPYARD][DECONSTRUCTION] FAILED: DOCKED CHANGED: " + this.currentDocked + "; " + this.getEntityState().getCurrentDocked());
         this.stateTransition(Transition.SY_ERROR);
      }

      return false;
   }
}
