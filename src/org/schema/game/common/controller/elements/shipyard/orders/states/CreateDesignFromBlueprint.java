package org.schema.game.common.controller.elements.shipyard.orders.states;

import com.bulletphysics.linearmath.Transform;
import java.io.IOException;
import org.schema.common.LogUtil;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.shipyard.orders.ShipyardEntityState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.element.meta.VirtualBlueprintMetaItem;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.inventory.NoSlotFreeException;
import org.schema.game.common.data.world.Sector;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.controller.EntityAlreadyExistsException;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.blueprint.ChildStats;
import org.schema.game.server.data.blueprint.SegmentControllerOutline;
import org.schema.game.server.data.blueprint.SegmentControllerSpawnCallbackDirect;
import org.schema.game.server.data.blueprintnw.BlueprintType;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;

public class CreateDesignFromBlueprint extends ShipyardState {
   private SegmentController newShip;

   public CreateDesignFromBlueprint(ShipyardEntityState var1) {
      super(var1);
   }

   public boolean onEnterS() {
      this.newShip = null;
      return false;
   }

   public boolean onExit() {
      this.newShip = null;
      return false;
   }

   public boolean onUpdate() throws FSMException {
      SegmentController var1 = this.getEntityState().getCurrentDocked();
      if (this.newShip != null) {
         if (this.newShip.getSegmentBuffer().getPointUnsave(Ship.core) != null) {
            if (this.getEntityState().getState().getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(this.newShip.getId()) && this.newShip.getSegmentBuffer().getPointUnsave(Ship.core).getType() == 1) {
               if (this.newShip.railController.isDockedAndExecuted()) {
                  LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Created design and docked from blueprint successfull " + this.newShip);
                  this.stateTransition(Transition.SY_CONVERSION_DONE);
               } else if (!this.newShip.railController.isDockedOrDirty()) {
                  LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Created design and docked from blueprint failed " + this.newShip + "; Cant dock");
                  this.newShip.railController.destroyDockedRecursive();
                  this.newShip.markForPermanentDelete(true);
                  this.newShip.setMarkedForDeleteVolatile(true);
                  System.err.println("[SERVER][SHIPYARD] Create Design From blueprint failed. Ship " + this.newShip + " couldnt dock!");
                  this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATEDESIGNFROMBLUEPRINT_2);
                  this.stateTransition(Transition.SY_ERROR);
               }
            } else {
               LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Created design and docked from blueprint WAITING " + this.newShip + "; No core or entity doesnt exist yet");
            }
         } else {
            LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Created design and docked from blueprint WAITING " + this.newShip + "; No core exists");
         }
      } else if (var1 != null) {
         LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Created design from bp failed: occupied");
         this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATEDESIGNFROMBLUEPRINT_3);
         this.stateTransition(Transition.SY_ERROR);
      } else if (!this.getEntityState().getInventory().hasFreeSlot()) {
         LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Created design from bp failed: no slot");
         this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATEDESIGNFROMBLUEPRINT_5);
         this.stateTransition(Transition.SY_ERROR);
      } else {
         double var2;
         if ((var2 = this.getTickCount()) >= 1.0D) {
            this.getEntityState().setCompletionOrderPercentAndSendIfChanged(1.0D);
            Transform var9;
            (var9 = new Transform()).setIdentity();

            Sector var12;
            try {
               if ((var12 = ((GameServerState)this.getEntityState().getState()).getUniverse().getSector(this.getEntityState().getSegmentController().getSectorId())) != null) {
                  assert this.getEntityState().currentBlueprintName != null;

                  assert this.getEntityState().currentName != null;

                  SegmentControllerOutline var10;
                  (var10 = BluePrintController.active.loadBluePrint((GameServerState)this.getEntityState().getState(), this.getEntityState().currentBlueprintName, this.getEntityState().currentName, var9, -1, 0, var12.pos, "SHIPYARD_" + this.getEntityState().getSegmentController().getUniqueIdentifier(), PlayerState.buffer, (SegmentPiece)null, false, new ChildStats(false))).checkOkName();
                  if (var10 != null && var10.en.getType() == BlueprintType.SPACE_STATION) {
                     LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Created design from bp failed: STTAION");
                     this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATEDESIGNFROMBLUEPRINT_0);
                     this.stateTransition(Transition.SY_ERROR);
                  }

                  if (var10 != null && var10.hasOldDocking()) {
                     LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Created design from bp failed: OLD DOCK");
                     this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATEDESIGNFROMBLUEPRINT_1);
                     this.stateTransition(Transition.SY_ERROR);
                  } else {
                     this.newShip = var10.spawn(var12.pos, true, new ChildStats(false), new SegmentControllerSpawnCallbackDirect((GameServerState)this.getEntityState().getState(), var12.pos) {
                        public void onNoDocker() {
                           CreateDesignFromBlueprint.this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATEDESIGNFROMBLUEPRINT_11);
                        }
                     });
                     LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": creating and loading design -> FORCE REQUEST! " + this.newShip);
                     this.newShip.getSegmentProvider().enqueueHightPrio(0, 0, 0, true);
                     if (this.getEntityState().getShipyardCollectionManager().createDockingRelation(this.newShip, true)) {
                        ((GameServerState)this.getEntityState().getState()).getMetaObjectManager();
                        VirtualBlueprintMetaItem var11 = (VirtualBlueprintMetaItem)MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.VIRTUAL_BLUEPRINT.type, (short)-1, true);
                        boolean var13 = false;

                        try {
                           int var14 = this.getEntityState().getInventory().getFreeSlot();
                           var11.UID = this.newShip.getUniqueIdentifier();
                           var11.virtualName = new String(this.getEntityState().currentName);
                           this.getEntityState().getInventory().put(var14, var11);
                           this.getEntityState().getInventory().sendInventoryModification(var14);
                           this.getEntityState().getShipyardCollectionManager().setCurrentDesign(var11.getId());
                           this.newShip.initialize();
                           this.getEntityState().getShipyardCollectionManager().sendShipyardStateToClient();
                           LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Created design from bp successfull: " + this.newShip);
                        } catch (NoSlotFreeException var4) {
                           var4.printStackTrace();
                           this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATEDESIGNFROMBLUEPRINT_6);
                           this.stateTransition(Transition.SY_ERROR);
                           LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Created design from bp failed: no slot but reserved");
                        }
                     } else {
                        LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Created design from bp failed (CANNOT DOCK) destroying: " + this.newShip);
                        this.newShip.railController.destroyDockedRecursive();
                        this.newShip.markForPermanentDelete(true);
                        this.newShip.setMarkedForDeleteVolatile(true);
                        this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATEDESIGNFROMBLUEPRINT_7);
                        this.stateTransition(Transition.SY_ERROR);
                     }
                  }
               } else {
                  LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Created design from bp failed: Sector null");
               }
            } catch (EntityNotFountException var5) {
               var12 = null;
               var5.printStackTrace();
               this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATEDESIGNFROMBLUEPRINT_8);
               LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Created design from bp failed: entity not found");
               this.stateTransition(Transition.SY_ERROR);
            } catch (IOException var6) {
               var12 = null;
               var6.printStackTrace();
               this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATEDESIGNFROMBLUEPRINT_9);
               LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Created design from bp failed: IO Exception");
               this.stateTransition(Transition.SY_ERROR);
            } catch (EntityAlreadyExistsException var7) {
               var12 = null;
               var7.printStackTrace();
               this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATEDESIGNFROMBLUEPRINT_10);
               LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Created design from bp failed: Entity already exists");
               this.stateTransition(Transition.SY_ERROR);
            } catch (StateParameterNotFoundException var8) {
               var12 = null;
               var8.printStackTrace();
               this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATEDESIGNFROMBLUEPRINT_4);
               LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Created design from bp failed: state parameter not found");
               this.stateTransition(Transition.SY_ERROR);
            }
         } else {
            LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Created design from bp in progress: " + var2);
            this.getEntityState().getShipyardCollectionManager().setCompletionOrderPercentAndSendIfChanged((double)((float)var2));
         }
      }

      return false;
   }
}
