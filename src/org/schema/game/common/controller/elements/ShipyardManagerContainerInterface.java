package org.schema.game.common.controller.elements;

public interface ShipyardManagerContainerInterface {
   ManagerModuleCollection getShipyard();
}
