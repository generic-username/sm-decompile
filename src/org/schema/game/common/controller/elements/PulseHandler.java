package org.schema.game.common.controller.elements;

public interface PulseHandler {
   PulseController getPulseController();
}
