package org.schema.game.common.controller.elements.dockingBlock;

import org.schema.game.common.data.element.ElementCollection;

public abstract class DockingBlockUnit extends ElementCollection {
   protected void significatorUpdate(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, long var10) {
      var1 = var8 - (var8 - var5) / 2;
      var2 = var9 - (var9 - var6) / 2;
      this.significator = ElementCollection.getIndex(var7, var1, var2);
   }
}
