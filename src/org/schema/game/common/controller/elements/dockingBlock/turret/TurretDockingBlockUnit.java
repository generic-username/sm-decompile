package org.schema.game.common.controller.elements.dockingBlock.turret;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.EmptyValueEntry;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.dockingBlock.DockingBlockUnit;

public class TurretDockingBlockUnit extends DockingBlockUnit {
   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ControllerManagerGUI.create(var1, "Turret Docking Module", this, new EmptyValueEntry());
   }
}
