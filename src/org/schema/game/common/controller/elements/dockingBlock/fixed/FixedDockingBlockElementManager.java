package org.schema.game.common.controller.elements.dockingBlock.fixed;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.dockingBlock.DockingBlockElementManager;
import org.schema.game.common.data.SegmentPiece;

public class FixedDockingBlockElementManager extends DockingBlockElementManager {
   public FixedDockingBlockElementManager(SegmentController var1) {
      super(var1, (short)289, (short)290);
   }

   protected String getTag() {
      return "fixeddockingblock";
   }

   public FixedDockingBlockCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new FixedDockingBlockCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return "Fixed Docking System Collective";
   }

   protected void playSound(FixedDockingBlockUnit var1, Transform var2) {
   }

   public void updateActivationTypes(ShortOpenHashSet var1) {
      var1.add((short)289);
   }

   public boolean isHandlingActivationForType(short var1) {
      return false;
   }
}
