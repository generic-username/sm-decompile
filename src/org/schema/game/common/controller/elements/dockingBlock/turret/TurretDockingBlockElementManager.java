package org.schema.game.common.controller.elements.dockingBlock.turret;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.dockingBlock.DockingBlockElementManager;
import org.schema.game.common.data.SegmentPiece;

public class TurretDockingBlockElementManager extends DockingBlockElementManager {
   public TurretDockingBlockElementManager(SegmentController var1) {
      super(var1, (short)7, (short)88);
   }

   protected String getTag() {
      return "turretdockingblock";
   }

   public TurretDockingBlockCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new TurretDockingBlockCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return "Turret Docking System Collective";
   }

   protected void playSound(TurretDockingBlockUnit var1, Transform var2) {
   }

   public void updateActivationTypes(ShortOpenHashSet var1) {
      var1.add((short)7);
   }

   public boolean isHandlingActivationForType(short var1) {
      return false;
   }
}
