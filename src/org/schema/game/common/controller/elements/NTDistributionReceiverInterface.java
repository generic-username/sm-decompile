package org.schema.game.common.controller.elements;

import org.schema.game.common.controller.ReceivedDistribution;
import org.schema.schine.network.objects.NetworkEntity;

public interface NTDistributionReceiverInterface {
   boolean receiveDistribution(ReceivedDistribution var1, NetworkEntity var2);
}
