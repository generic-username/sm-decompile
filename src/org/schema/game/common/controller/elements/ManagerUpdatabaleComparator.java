package org.schema.game.common.controller.elements;

import java.util.Comparator;

public class ManagerUpdatabaleComparator implements Comparator {
   public int compare(ManagerUpdatableInterface var1, ManagerUpdatableInterface var2) {
      return var2.updatePrio() - var1.updatePrio();
   }
}
