package org.schema.game.common.controller.elements;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import javax.vecmath.Vector3f;
import org.schema.common.CallInterace;
import org.schema.common.LogUtil;
import org.schema.common.config.ConfigParserException;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.config.DoubleMultiConfigField;
import org.schema.common.config.FloatMultiConfigField;
import org.schema.common.config.IntMultiConfigField;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.LibLoader;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.HpConditionList;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentBufferInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.SlotAssignment;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.projectile.ProjectileController;
import org.schema.game.common.controller.elements.combination.Combinable;
import org.schema.game.common.controller.elements.config.ReactorDualConfigElement;
import org.schema.game.common.controller.elements.effectblock.EffectElementManager;
import org.schema.game.common.controller.elements.power.PowerAddOn;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.BlockEffectTypes;
import org.schema.game.common.data.element.ControlElementMap;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.ShipConfigurationNotFoundException;
import org.schema.game.server.controller.GameServerController;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.container.PhysicsDataContainer;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class UsableElementManager extends DrawerObservable {
   public static ObjectOpenHashSet initializedServer = new ObjectOpenHashSet();
   public static ObjectOpenHashSet initializedClient = new ObjectOpenHashSet();
   private final SegmentController segmentController;
   public long nextShot;
   public int totalSize;
   long lastEnqueue;
   public double lowestIntegrity = Double.POSITIVE_INFINITY;
   private boolean updatable;
   private boolean explosiveStructure;

   public UsableElementManager(SegmentController var1) {
      this.segmentController = var1;
   }

   public boolean isOnServer() {
      return this.getSegmentController().isOnServer();
   }

   public abstract void onElementCollectionsChanged();

   public boolean isAddToPlayerUsable() {
      return true;
   }

   public boolean isCombinable() {
      return this instanceof Combinable && ((Combinable)this).getAddOn() != null;
   }

   public void init(ManagerContainer var1) {
   }

   public void onPlayerDetachedFromThisOrADock(ManagedUsableSegmentController var1, PlayerState var2, PlayerControllable var3) {
   }

   public double calculateReload(ElementCollection var1) {
      return 0.0D;
   }

   public abstract void flagCheckUpdatable();

   public boolean canConsumePower(float var1) {
      if (var1 <= 0.0F) {
         return true;
      } else if (this.getPowerManager().canConsumePowerInstantly(var1)) {
         return true;
      } else {
         SegmentController var2;
         return this.getSegmentController().getDockingController().getDockedOn() != null && (var2 = this.getSegmentController().getDockingController().getDockedOn().to.getSegment().getSegmentController()) instanceof ManagedSegmentController && ((ManagedSegmentController)var2).getManagerContainer() instanceof PowerManagerInterface ? ((PowerManagerInterface)((ManagedSegmentController)var2).getManagerContainer()).getPowerAddOn().canConsumePowerInstantly(var1) : false;
      }
   }

   public double getPower() {
      SegmentController var1;
      return this.getSegmentController().getDockingController().getDockedOn() != null && (var1 = this.getSegmentController().getDockingController().getDockedOn().to.getSegment().getSegmentController()) instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof PowerManagerInterface ? ((PowerManagerInterface)((ManagedSegmentController)var1).getManagerContainer()).getPowerAddOn().getPower() : 0.0D;
   }

   public boolean isUsingPowerReactors() {
      return this.getManagerContainer().getPowerInterface().isUsingPowerReactors();
   }

   public boolean consumePower(float var1) {
      if (var1 > 0.0F && !this.getPowerManager().consumePowerInstantly((double)var1)) {
         SegmentController var2;
         return this.getSegmentController().getDockingController().isDocked() && (var2 = this.getSegmentController().getDockingController().getDockedOn().to.getSegment().getSegmentController()) instanceof ManagedSegmentController && ((ManagedSegmentController)var2).getManagerContainer() instanceof PowerManagerInterface ? ((PowerManagerInterface)((ManagedSegmentController)var2).getManagerContainer()).getPowerAddOn().consumePowerInstantly((double)var1) : false;
      } else {
         return true;
      }
   }

   public abstract ControllerManagerGUI getGUIUnitValues(ElementCollection var1, ElementCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4);

   public void printTags() {
      Field[] var1;
      int var2 = (var1 = this.getClass().getDeclaredFields()).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         Field var4;
         ConfigurationElement var5;
         if ((var5 = (ConfigurationElement)(var4 = var1[var3]).getAnnotation(ConfigurationElement.class)) != null) {
            try {
               System.out.println("<" + var5.name() + ">" + var4.get(this).toString() + "</" + var5.name() + ">");
            } catch (IllegalArgumentException var6) {
               var6.printStackTrace();
            } catch (IllegalAccessException var7) {
               var7.printStackTrace();
            }
         }
      }

   }

   public EffectElementManager getEffect(short var1) {
      return ((EffectManagerContainer)this.getManagerContainer()).getEffect(var1);
   }

   public boolean canHandle(ControllerStateInterface var1) {
      return !this.getSegmentController().isVirtualBlueprint() && (var1.isPrimaryShootButtonDown() || var1.isSecondaryShootButtonDown());
   }

   public SlotAssignment checkShipConfig(ControllerStateInterface var1) throws ShipConfigurationNotFoundException {
      return var1.getPlayerState() == null ? null : this.getSegmentController().getSlotAssignment();
   }

   protected abstract String getTag();

   public void parse(Document var1) throws IllegalArgumentException, IllegalAccessException, ConfigParserException {
      parse(var1, this.getSegmentController().isOnServer(), this, this.getClass());
   }

   public static void parseTest() {
      try {
         LogUtil.setUp(20, new CallInterace() {
            public final void call() {
            }
         });
         ElementKeyMap.initializeData((File)null);
         LibLoader.loadNativeLibs(true, -1, false);
         BlockShapeAlgorithm.initialize();
         GameServerState var0;
         (var0 = new GameServerState()).setController(new GameServerController(var0));
         var0.udpateTime = System.currentTimeMillis();
         var0.getController().parseBlockBehavior("./data/config/blockBehaviorConfig.xml");
         (new Ship(var0)).getManagerContainer().reparseBlockBehavior(true);
      } catch (SQLException var1) {
         var1.printStackTrace();
      } catch (IOException var2) {
         var2.printStackTrace();
      } catch (Exception var3) {
         var3.printStackTrace();
      }
   }

   public static void parse(Document var0, boolean var1, UsableElementManager var2, Class var3) throws IllegalArgumentException, IllegalAccessException, ConfigParserException {
      assert var0 != null;

      synchronized(initializedServer) {
         if (var1 && !initializedServer.contains(var3) || !var1 && !initializedClient.contains(var3)) {
            Element var10000 = var0.getDocumentElement();
            var0 = null;
            NodeList var27 = var10000.getChildNodes();
            Field[] var5 = var3.getDeclaredFields();
            Object2ObjectOpenHashMap var6 = new Object2ObjectOpenHashMap();
            Field[] var7 = var5;
            int var8 = var5.length;

            int var9;
            for(var9 = 0; var9 < var8; ++var9) {
               Field var10;
               ConfigurationElement var11;
               if ((var11 = (ConfigurationElement)(var10 = var7[var9]).getAnnotation(ConfigurationElement.class)) != null) {
                  var6.put(var11.name().toLowerCase(Locale.ENGLISH), var10);
               }
            }

            boolean var28 = false;
            ObjectOpenHashSet var29 = new ObjectOpenHashSet();
            var9 = 0;

            while(true) {
               int var12;
               if (var9 >= var27.getLength()) {
                  if (!var28) {
                     throw new ConfigParserException("Block module Tag \"" + var2.getTag() + "\" not found in block behavior configuation. Please create it (case insensitive)");
                  }

                  var3.getAnnotations();
                  Field[] var31 = var5;
                  int var33 = var5.length;

                  for(var12 = 0; var12 < var33; ++var12) {
                     Field var34;
                     (var34 = var31[var12]).setAccessible(true);
                     ConfigurationElement var35;
                     if ((var35 = (ConfigurationElement)var34.getAnnotation(ConfigurationElement.class)) != null && !var29.contains(var34)) {
                        throw new ConfigParserException("virtual field " + var34.getName() + " (" + var35.name() + ") not found. Please define a tag \"" + var35.name() + "\" inside the <BasicValues> of \"" + var2.getTag() + "\"");
                     }
                  }

                  if (var1) {
                     initializedServer.add(var3);
                  } else {
                     initializedClient.add(var3);
                  }
                  break;
               }

               Node var30;
               if ((var30 = var27.item(var9)).getNodeType() == 1 && var30.getNodeName().toLowerCase(Locale.ENGLISH).equals(var2.getTag())) {
                  NodeList var32 = var30.getChildNodes();
                  var28 = true;

                  for(var12 = 0; var12 < var32.getLength(); ++var12) {
                     Node var13;
                     if ((var13 = var32.item(var12)).getNodeType() == 1) {
                        if (!var13.getNodeName().toLowerCase(Locale.ENGLISH).equals("basicvalues")) {
                           if (!var13.getNodeName().toLowerCase(Locale.ENGLISH).equals("combination")) {
                              throw new ConfigParserException("tag \"" + var30.getNodeName() + " -> " + var13.getNodeName() + "\" unknown in this context (has to be either \"BasicValues\" or \"Combinable\")");
                           }

                           if (!(var2 instanceof Combinable)) {
                              throw new ConfigParserException(var30.getNodeName() + " -> " + var13.getNodeName() + " class is not combinable " + var2 + ", but has combinable tag");
                           }

                           ((Combinable)var2).getAddOn().parse(var13);
                        } else {
                           NodeList var14 = var13.getChildNodes();

                           for(int var15 = 0; var15 < var14.getLength(); ++var15) {
                              Node var16;
                              if ((var16 = var14.item(var15)).getNodeType() == 1) {
                                 Node var17;
                                 boolean var18 = (var17 = var16.getAttributes().getNamedItem("version")) != null;
                                 Field var19;
                                 if ((var19 = (Field)var6.get(var16.getNodeName().toLowerCase(Locale.ENGLISH))) == null) {
                                    throw new ConfigParserException(var30.getNodeName() + "-> " + var13.getNodeName() + " -> " + var16.getNodeName() + ": No appropriate field found for tag: " + var16.getNodeName());
                                 }

                                 var19.setAccessible(true);
                                 UsableElementManager var20 = var2;

                                 try {
                                    if (var19.getType() == Boolean.TYPE) {
                                       assert !var18 : var19.getName() + " of " + var3 + " is versioned and can't be primitive (" + var3.getSimpleName() + ".java:0)";

                                       var19.setBoolean(var20, Boolean.parseBoolean(var16.getTextContent()));
                                    } else if (var19.getType() == Integer.TYPE) {
                                       assert !var18 : var19.getName() + " of " + var3 + " is versioned and can't be primitive (" + var3.getSimpleName() + ".java:0)";

                                       var19.setInt(var20, Integer.parseInt(var16.getTextContent()));
                                    } else if (var19.getType() == Short.TYPE) {
                                       assert !var18 : var19.getName() + " of " + var3 + " is versioned and can't be primitive (" + var3.getSimpleName() + ".java:0)";

                                       var19.setShort(var20, Short.parseShort(var16.getTextContent()));
                                    } else if (var19.getType() == Byte.TYPE) {
                                       assert !var18 : var19.getName() + " of " + var3 + " is versioned and can't be primitive (" + var3.getSimpleName() + ".java:0)";

                                       var19.setByte(var20, Byte.parseByte(var16.getTextContent()));
                                    } else if (var19.getType() == Float.TYPE) {
                                       assert !var18 : var19.getName() + " of " + var3 + " is versioned and can't be primitive (" + var3.getSimpleName() + ".java:0)";

                                       var19.setFloat(var20, Float.parseFloat(var16.getTextContent()));
                                    } else if (var19.getType() == Double.TYPE) {
                                       assert !var18 : var19.getName() + " of " + var3 + " is versioned and can't be primitive (" + var3.getSimpleName() + ".java:0)";

                                       var19.setDouble(var20, Double.parseDouble(var16.getTextContent()));
                                    } else if (var19.getType() == Long.TYPE) {
                                       assert !var18 : var19.getName() + " of " + var3 + " is versioned and can't be primitive (" + var3.getSimpleName() + ".java:0)";

                                       var19.setLong(var20, Long.parseLong(var16.getTextContent()));
                                    } else if (InterEffectSet.class.equals(var19.getType())) {
                                       ((InterEffectSet)var19.get(var20)).parseXML(var16);
                                    } else if (ReactorDualConfigElement.class.isAssignableFrom(var19.getType())) {
                                       int var36 = ReactorDualConfigElement.getIndex(!var18 || !var17.getNodeValue().toLowerCase(Locale.ENGLISH).equals("noreactor"));
                                       if (IntMultiConfigField.class.isAssignableFrom(var19.getType())) {
                                          ((IntMultiConfigField)var19.get(var20)).set(var36, Integer.parseInt(var16.getTextContent()));
                                       } else if (FloatMultiConfigField.class.isAssignableFrom(var19.getType())) {
                                          ((FloatMultiConfigField)var19.get(var20)).set(var36, Float.parseFloat(var16.getTextContent()));
                                       } else {
                                          if (!DoubleMultiConfigField.class.isAssignableFrom(var19.getType())) {
                                             throw new ConfigParserException("Unknown type: " + var19.getType() + " Cannot parse field: " + var19.getName() + "; " + var19.getType() + "; with " + var16.getTextContent());
                                          }

                                          ((DoubleMultiConfigField)var19.get(var20)).set(var36, Double.parseDouble(var16.getTextContent()));
                                       }
                                    } else if (var19.getType().equals(HpConditionList.class)) {
                                       ((HpConditionList)var19.get(var20)).parse(var16);
                                    } else if (var19.getType().equals(BlockEffectTypes.class)) {
                                       BlockEffectTypes var37;
                                       if ((var37 = BlockEffectTypes.valueOf(var16.getTextContent())) == null) {
                                          throw new ConfigParserException("Cannot parse enum field: " + var30.getNodeName() + "-> " + var13.getNodeName() + " -> " + var16.getNodeName() + ": " + var19.getName() + "; " + var19.getType() + "; enum unkown (possible: " + Arrays.toString(BlockEffectTypes.values()) + ")");
                                       }

                                       var19.set(var20, var37);
                                    } else if (var19.getType().isEnum()) {
                                       System.err.println("NM: " + var19.getType().getSimpleName());

                                       assert !var18;

                                       try {
                                          Enum var38;
                                          if ((var38 = (Enum)var19.getType().getMethod("valueOf", String.class).invoke((Object)null, var16.getTextContent())) == null) {
                                             Enum[] var39 = (Enum[])var19.getType().getMethod("values").invoke((Object)null);
                                             throw new ConfigParserException("Cannot parse enum field: " + var30.getNodeName() + "-> " + var13.getNodeName() + " -> " + var16.getNodeName() + ": " + var19.getName() + "; " + var19.getType() + "; enum unkown (possible: " + Arrays.toString(var39) + ")");
                                          }

                                          var19.set(var20, var38);
                                       } catch (NoSuchMethodException var21) {
                                          var21.printStackTrace();
                                       } catch (SecurityException var22) {
                                          var22.printStackTrace();
                                       } catch (InvocationTargetException var23) {
                                          var23.printStackTrace();
                                       } catch (DOMException var24) {
                                          var24.printStackTrace();
                                       }
                                    } else {
                                       if (!var19.getType().equals(String.class)) {
                                          throw new ConfigParserException("Cannot parse field: " + var19.getName() + "; " + var19.getType());
                                       }

                                       assert !var18 : var19.getName() + " of " + var3 + " is versioned and can't be primitive";

                                       String var40 = var16.getTextContent().replaceAll("\\r\\n|\\r|\\n", "").replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r").replaceAll("\\\\t", "\t");
                                       var19.set(var20, var40);
                                    }
                                 } catch (NumberFormatException var25) {
                                    throw new ConfigParserException("Cannot parse field: " + var19.getName() + "; " + var19.getType() + "; with '" + var16.getTextContent() + "'", var25);
                                 }

                                 var29.add(var19);
                              }
                           }
                        }
                     }
                  }
               }

               ++var9;
            }
         } else if (var1) {
            assert initializedServer.size() > 0;
         } else {
            assert initializedClient.size() > 0;
         }

      }
   }

   public boolean hasAnyBlock() {
      return this.totalSize > 0;
   }

   protected boolean clientIsOwnShip() {
      return ((PlayerControllable)this.segmentController).isClientOwnObject();
   }

   protected List getAttachedPlayers() {
      return ((ManagedUsableSegmentController)this.segmentController).getAttachedPlayers();
   }

   public ControlElementMap getControlElementMap() {
      return this.segmentController.getControlElementMap();
   }

   public ManagerContainer getManagerContainer() {
      return ((ManagedSegmentController)this.segmentController).getManagerContainer();
   }

   public abstract ElementCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2);

   public abstract String getManagerName();

   public GUIKeyValueEntry[] getGUIElementCollectionValues() {
      return new GUIKeyValueEntry[0];
   }

   protected PhysicsDataContainer getPhysicsDataContainer() {
      return this.segmentController.getPhysicsDataContainer();
   }

   protected PowerAddOn getPowerManager() {
      return ((PowerManagerInterface)this.getManagerContainer()).getPowerAddOn();
   }

   public ProjectileController getParticleController() {
      return ((EditableSendableSegmentController)this.segmentController).getParticleController();
   }

   public PulseController getPulseController() {
      return ((PulseHandler)this.segmentController).getPulseController();
   }

   protected SegmentBufferInterface getSegmentBuffer() {
      return this.segmentController.getSegmentBuffer();
   }

   public void notifyShooting(ElementCollection var1) {
      this.notifyObservers(var1, "s");
   }

   protected abstract void playSound(ElementCollection var1, Transform var2);

   public void handleResponse(ShootingRespose var1, ElementCollection var2, Vector3f var3) {
      switch(var1) {
      case FIRED:
         if (!this.getSegmentController().isOnServer()) {
            Transform var4;
            (var4 = new Transform()).setIdentity();
            var4.origin.set(var3);
            this.playSound(var2, var4);
         }

         this.notifyShooting(var2);
         this.getManagerContainer().onAction();
         return;
      case RELOADING:
         return;
      case CHARGING:
         return;
      case INITIALIZING:
         if (this.getSegmentController().isClientOwnObject()) {
            ((GameClientState)this.getSegmentController().getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_USABLEELEMENTMANAGER_0, 0.0F);
            return;
         }
         break;
      case INVALID_COMBI:
         if (this.getSegmentController().isClientOwnObject()) {
            ((GameClientState)this.getSegmentController().getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_USABLEELEMENTMANAGER_1, 0.0F);
            return;
         }
         break;
      case NO_COMBINATION:
         if (this.getSegmentController().isClientOwnObject()) {
            ((GameClientState)this.getSegmentController().getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_USABLEELEMENTMANAGER_2, 0.0F);
            return;
         }
         break;
      case NO_POWER:
         if (this.getSegmentController().isClientOwnObject() && ((GameClientState)this.getState()).getWorldDrawer() != null) {
            ((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().notifyEffectHit(this.getSegmentController(), EffectElementManager.OffensiveEffects.NO_POWER);
         }
      }

   }

   public SegmentController getSegmentController() {
      return this.segmentController;
   }

   public StateInterface getState() {
      return this.segmentController.getState();
   }

   protected Transform getWorldTransform() {
      return this.segmentController.getWorldTransform();
   }

   public abstract void handle(ControllerStateInterface var1, Timer var2);

   public int updatePrio() {
      return 0;
   }

   public void setUpdatable(boolean var1) {
      if (!this.updatable && var1) {
         this.getManagerContainer().flagUpdatableCheckFor(this);
      } else if (this.updatable && !var1) {
         this.getManagerContainer().flagUpdatableCheckFor(this);
      }

      this.updatable = var1;
   }

   public boolean isUpdatable() {
      return this.updatable;
   }

   public void setExplosiveStructure(boolean var1) {
      this.explosiveStructure = var1;
   }

   public boolean isExplosiveStructure() {
      return this.explosiveStructure;
   }
}
