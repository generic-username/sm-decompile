package org.schema.game.common.controller.elements.power.reactor.tree;

import it.unimi.dsi.fastutil.longs.Long2IntMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.ShortList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Matrix3f;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.elements.power.reactor.MainReactorUnit;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.controller.elements.power.reactor.chamber.ReactorChamberUnit;
import org.schema.game.common.data.blockeffects.config.ConfigPool;
import org.schema.game.common.data.blockeffects.config.ConfigProviderSource;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class ReactorSet implements SerialializationInterface {
   private static byte TAG_VERSION = 0;
   private final PowerInterface pw;
   private final List trees = new ObjectArrayList();
   private boolean damaged;
   private final ReactorSet.ReactorSectorProjection reactorSectorProjection = new ReactorSet.ReactorSectorProjection();
   private final Long2ObjectOpenHashMap treeMap = new Long2ObjectOpenHashMap();
   private final Long2ObjectOpenHashMap bonusCache = new Long2ObjectOpenHashMap();
   private boolean updateBooted = true;
   public boolean dischargeAll;
   private float accumulatedBootUp;
   private final List reactorListeners = new ObjectArrayList();
   private boolean receivedTree;

   public ReactorSet(PowerInterface var1) {
      this.pw = var1;
   }

   public void build() {
      ReactorTree var1;
      if ((var1 = this.getActiveReactor()) != null && var1.isAnyChamberBootingUp()) {
         this.accumulatedBootUp = var1.getAccumulatedBootUp();
      }

      int var5 = this.trees.size();
      Iterator var2 = this.trees.iterator();

      while(var2.hasNext()) {
         ReactorTree var3;
         if ((var3 = (ReactorTree)var2.next()).hasModifiedBonusMatrix()) {
            this.bonusCache.put(var3.getId(), var3.getBonusMatrix());
         }
      }

      this.trees.clear();
      this.treeMap.clear();
      var2 = this.pw.getMainReactors().iterator();

      while(var2.hasNext()) {
         MainReactorUnit var6 = (MainReactorUnit)var2.next();
         ReactorTree var4;
         (var4 = new ReactorTree(this.pw)).build(var6);
         float var7;
         if ((var7 = (float)var4.getSpecificCountRec()) > 0.0F && (double)this.accumulatedBootUp > 0.9D) {
            var7 = this.accumulatedBootUp / var7;
            var4.distributeBootUp(var7);
         }

         if (this.bonusCache.containsKey(var4.getId())) {
            var4.getBonusMatrix().set((Matrix3f)this.bonusCache.remove(var4.getId()));
         }

         this.trees.add(var4);
         this.treeMap.put(var4.getId(), var4);
      }

      if (var5 > 0 && this.trees.size() == 0) {
         this.pw.onLastReactorRemoved();
      }

   }

   public boolean onBlockKilledServer(Damager var1, short var2, long var3, Long2IntMap var5) {
      Iterator var6 = this.trees.iterator();

      ReactorTree var7;
      do {
         if (!var6.hasNext()) {
            return false;
         }
      } while(!(var7 = (ReactorTree)var6.next()).onBlockKilledServer(var1, var2, var3, var5));

      this.damaged = true;
      if (var7.isActiveTree() && this.pw.getSegmentController().getConfigManager().apply(StatusEffectType.REACTOR_FAILSAFE, false)) {
         float var8 = this.pw.getSegmentController().getConfigManager().apply(StatusEffectType.REACTOR_FAILSAFE_THRESHOLD, 1.0F);
         if (var7.getHpPercent() < (double)var8) {
            this.pw.switchActiveReacorToMostHp(var7);
         }
      }

      return true;
   }

   public void print() {
      System.err.println("Printing all Reactor Trees: " + this.trees.size());
      System.err.println("---------------------------------");
      Iterator var1 = this.trees.iterator();

      while(var1.hasNext()) {
         ((ReactorTree)var1.next()).print();
         System.err.println("---------------------------------");
      }

   }

   public List getTrees() {
      return this.trees;
   }

   public boolean isInAnyTree(ReactorChamberUnit var1) {
      Iterator var2 = this.trees.iterator();

      do {
         if (!var2.hasNext()) {
            return false;
         }
      } while(!((ReactorTree)var2.next()).isUnitPartOfTree(var1));

      return true;
   }

   public int size() {
      return this.trees.size();
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeBoolean(this.dischargeAll);
      var1.writeShort(this.trees.size());

      for(int var3 = 0; var3 < this.trees.size(); ++var3) {
         ((ReactorTree)this.trees.get(var3)).serialize(var1, var2);
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.trees.clear();
      this.treeMap.clear();
      this.dischargeAll = var1.readBoolean();
      short var4 = var1.readShort();

      for(int var5 = 0; var5 < var4; ++var5) {
         ReactorTree var6;
         (var6 = new ReactorTree(this.pw)).deserialize(var1, var2, var3);
         this.trees.add(var6);
         this.treeMap.put(var6.getId(), var6);
      }

      this.receivedTree = true;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var4;
      (var4 = var1.getStruct())[0].getByte();
      var4 = var4[1].getStruct();

      for(int var2 = 0; var2 < var4.length - 1; ++var2) {
         ReactorTree var3;
         (var3 = new ReactorTree(this.pw)).fromTagStructure(var4[var2]);
         this.trees.add(var3);
         this.treeMap.put(var3.getId(), var3);
      }

      this.damaged = this.calcAnyMainDamaged();
   }

   public Tag toTagStructure() {
      Tag var1 = new Tag(Tag.Type.BYTE, (String)null, TAG_VERSION);
      if (!this.pw.isOnServer()) {
         return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{FinishTag.INST}), FinishTag.INST});
      } else {
         Tag[] var2;
         Tag[] var10000 = var2 = new Tag[this.trees.size() + 1];
         var10000[var10000.length - 1] = FinishTag.INST;

         for(int var3 = 0; var3 < var2.length - 1; ++var3) {
            var2[var3] = ((ReactorTree)this.trees.get(var3)).toTagStructure();
         }

         return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, new Tag(Tag.Type.STRUCT, (String)null, var2), FinishTag.INST});
      }
   }

   public void apply(ReactorSet var1) {
      this.trees.clear();
      this.treeMap.clear();
      this.trees.addAll(var1.trees);
      Iterator var3 = this.trees.iterator();

      while(var3.hasNext()) {
         ReactorTree var2 = (ReactorTree)var3.next();
         this.treeMap.put(var2.getId(), var2);
      }

      this.damaged = this.calcAnyMainDamaged();
      this.receivedTree = true;
   }

   public boolean calcAnyMainDamaged() {
      Iterator var1 = this.trees.iterator();

      do {
         if (!var1.hasNext()) {
            return false;
         }
      } while(!((ReactorTree)var1.next()).isDamagedAny());

      return true;
   }

   public boolean isAnyMainDamaged() {
      Iterator var1 = this.trees.iterator();

      do {
         if (!var1.hasNext()) {
            return false;
         }
      } while(!((ReactorTree)var1.next()).isDamagedMain());

      return true;
   }

   public boolean isAnyDamaged() {
      return this.damaged;
   }

   public boolean applyReceivedSizeChange(long var1, int var3) {
      Iterator var4 = this.trees.iterator();

      ReactorTree var5;
      do {
         if (!var4.hasNext()) {
            return false;
         }
      } while(!(var5 = (ReactorTree)var4.next()).applyReceivedSizeChange(var1, var3));

      this.damaged = this.calcAnyMainDamaged();
      if (this.receivedTree) {
         Iterator var6 = this.reactorListeners.iterator();

         while(var6.hasNext()) {
            ((ReactorTreeChangeListener)var6.next()).onReactorSizeChanged(var5, this.damaged);
         }
      }

      return this.damaged;
   }

   public void getAppliedConfigGroups(ShortList var1) {
      Iterator var2 = this.trees.iterator();

      while(var2.hasNext()) {
         ReactorTree var3;
         if ((var3 = (ReactorTree)var2.next()).isActiveTree()) {
            var3.getAppliedConfigGroups(var1);
         }
      }

   }

   public void getAppliedConfigSectorGroups(ShortList var1) {
      Iterator var2 = this.trees.iterator();

      while(var2.hasNext()) {
         ReactorTree var3;
         if ((var3 = (ReactorTree)var2.next()).isActiveTree()) {
            var3.getAppliedConfigSectorGroups(var1);
         }
      }

   }

   public ConfigProviderSource getSectorConfigProjection() {
      return this.reactorSectorProjection;
   }

   public Long2ObjectOpenHashMap getTreeMap() {
      return this.treeMap;
   }

   public ReactorElement getChamber(long var1) {
      Iterator var3 = this.trees.iterator();

      ReactorElement var4;
      do {
         if (!var3.hasNext()) {
            return null;
         }
      } while((var4 = ((ReactorTree)var3.next()).getChamber(var1)) == null);

      return var4;
   }

   public void update(Timer var1, long var2) {
      Iterator var4;
      if (this.receivedTree) {
         var4 = this.reactorListeners.iterator();

         while(var4.hasNext()) {
            ((ReactorTreeChangeListener)var4.next()).onReceivedTree();
         }

         this.receivedTree = false;
      }

      var4 = this.trees.iterator();

      while(var4.hasNext()) {
         ReactorTree var5;
         if ((var5 = (ReactorTree)var4.next()).getId() == var2) {
            this.updateBooted = var5.updateBooted(var1);
         }
      }

   }

   public ReactorTree getActiveReactor() {
      return (ReactorTree)this.getTreeMap().get(this.pw.getActiveReactorId());
   }

   public void getAllReactorElementsWithConfig(ConfigPool var1, StatusEffectType var2, Collection var3) {
      ReactorTree var4;
      if ((var4 = this.getActiveReactor()) != null) {
         var4.getAllReactorElementsWithConfig(var1, var2, var3);
      }

   }

   public void removeReactorTreeListener(ReactorTreeChangeListener var1) {
      this.reactorListeners.remove(var1);
   }

   public void addReactorTreeListener(ReactorTreeChangeListener var1) {
      this.reactorListeners.add(var1);
   }

   class ReactorSectorProjection implements ConfigProviderSource {
      private ReactorSectorProjection() {
      }

      public ShortList getAppliedConfigGroups(ShortList var1) {
         ReactorSet.this.getAppliedConfigSectorGroups(var1);
         return var1;
      }

      public long getSourceId() {
         return -1L;
      }

      // $FF: synthetic method
      ReactorSectorProjection(Object var2) {
         this();
      }
   }
}
