package org.schema.game.common.controller.elements.power.reactor;

import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.PolygonTools;
import org.schema.common.util.linAlg.PolygonToolsVars;
import org.schema.common.util.linAlg.Triangle;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.common.util.linAlg.quickhull.Point3d;
import org.schema.common.util.linAlg.quickhull.QuickHull3D;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.power.reactor.chamber.ConduitUnit;
import org.schema.game.common.controller.elements.shield.CenterOfMassUnit;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.common.language.Lng;

public class MainReactorUnit extends CenterOfMassUnit {
   private boolean single;
   public Triangle[] tris;
   private Vector3f tmpPoint = new Vector3f();
   private long[] singles;

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      Vector3i var4;
      (var4 = new Vector3i()).sub(this.getMax(new Vector3i()), this.getMin(new Vector3i()));
      return ControllerManagerGUI.create(var1, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_MAINREACTORUNIT_0, this, new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_MAINREACTORUNIT_1, var4), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_MAINREACTORUNIT_2, "N/A"));
   }

   public void calculateExtraDataAfterCreationThreaded(long var1, LongOpenHashSet var3) {
      LongArrayList var4;
      if ((var4 = this.getNeighboringCollectionUnsave()).size() > 3) {
         Point3d[] var5 = new Point3d[var4.size() + 2];
         int var6 = 0;

         for(int var7 = 0; var7 < var4.size(); ++var7) {
            long var8 = var4.getLong(var7);
            var5[var6] = new Point3d((double)ElementCollection.getPosX(var8), (double)ElementCollection.getPosY(var8), (double)ElementCollection.getPosZ(var8));
            ++var6;
            if (var7 == 0) {
               var5[var6] = new Point3d((double)ElementCollection.getPosX(var8) + 0.5D, (double)ElementCollection.getPosY(var8) + 0.5D, (double)ElementCollection.getPosZ(var8) + 0.5D);
               ++var6;
               var5[var6] = new Point3d((double)ElementCollection.getPosX(var8) - 0.5D, (double)ElementCollection.getPosY(var8) - 0.5D, (double)ElementCollection.getPosZ(var8) - 0.5D);
               ++var6;
            }
         }

         int var16;
         try {
            QuickHull3D var14 = new QuickHull3D();
            if (var4.size() > 20000) {
               var14.setExplicitDistanceTolerance(0.34D);
            } else if (var4.size() > 10000) {
               var14.setExplicitDistanceTolerance(0.32D);
            } else if (var4.size() > 5000) {
               var14.setExplicitDistanceTolerance(0.3D);
            } else if (var4.size() > 3000) {
               var14.setExplicitDistanceTolerance(0.2D);
            } else if (var4.size() > 1000) {
               var14.setExplicitDistanceTolerance(0.1D);
            }

            var14.build(var5, var5.length);
            var14.triangulate();
            var16 = var14.getNumFaces();
            int[][] var9 = var14.getFaces();
            var5 = var14.getVertices();
            this.tris = new Triangle[var16];

            for(var6 = 0; var6 < var16; ++var6) {
               Point3d var15 = var5[var9[var6][0]];
               Point3d var10 = var5[var9[var6][1]];
               Point3d var11 = var5[var9[var6][2]];
               this.tris[var6] = new Triangle(new Vector3f((float)var15.x, (float)var15.y, (float)var15.z), new Vector3f((float)var10.x, (float)var10.y, (float)var10.z), new Vector3f((float)var11.x, (float)var11.y, (float)var11.z));
            }
         } catch (Exception var12) {
            System.err.println("[QUICKHULL]" + this.getSegmentController().getState() + " WARNING: failed calculation of quickhull for " + this.getSegmentController() + ". Using fallback. Used " + var4.size() + " blocks");
            this.single = true;
            this.singles = new long[var4.size()];

            for(var16 = 0; var16 < this.singles.length; ++var16) {
               this.singles[var16] = var4.getLong(var16);
            }
         }
      } else {
         this.single = true;
         this.singles = new long[var4.size()];

         for(int var13 = 0; var13 < this.singles.length; ++var13) {
            this.singles[var13] = var4.getLong(var13);
         }
      }

      super.calculateExtraDataAfterCreationThreaded(var1, var3);
   }

   public boolean onChangeFinished() {
      Iterator var1 = this.getPowerInterface().getConduits().getElementCollections().iterator();

      while(var1.hasNext()) {
         ConduitUnit var2;
         if (ElementCollection.overlaps(var2 = (ConduitUnit)var1.next(), this, 1, 0)) {
            var2.flagCalcConnections();
         }
      }

      return super.onChangeFinished();
   }

   public float distanceToThis(long var1, PolygonToolsVars var3) {
      double var4 = (double)ElementCollection.getPosX(var1);
      double var6 = (double)ElementCollection.getPosY(var1);
      double var8 = (double)ElementCollection.getPosZ(var1);
      if (this.single) {
         float var19 = Float.POSITIVE_INFINITY;

         for(int var2 = 0; var2 < this.singles.length; ++var2) {
            long var11;
            double var13 = (double)ElementCollection.getPosX(var11 = this.singles[var2]) - var4;
            double var15 = (double)ElementCollection.getPosY(var11) - var6;
            double var17 = (double)ElementCollection.getPosZ(var11) - var8;
            float var10;
            if ((var10 = FastMath.carmackSqrt((float)(var13 * var13 + var15 * var15 + var17 * var17))) < var19) {
               var19 = var10;
               var3.outFrom.set((float)ElementCollection.getPosX(var11), (float)ElementCollection.getPosY(var11), (float)ElementCollection.getPosZ(var11));
            }
         }

         return var19;
      } else if (this.tris == null) {
         return Float.POSITIVE_INFINITY;
      } else {
         this.tmpPoint.set((float)var4, (float)var6, (float)var8);
         return PolygonTools.distance(this.tris, this.tris.length, this.tmpPoint, var3);
      }
   }
}
