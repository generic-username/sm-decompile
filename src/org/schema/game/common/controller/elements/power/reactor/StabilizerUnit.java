package org.schema.game.common.controller.elements.power.reactor;

import it.unimi.dsi.fastutil.longs.Long2FloatOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.util.CompareTools;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.PolygonToolsVars;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.elements.BlockConnectionPath;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorTree;
import org.schema.game.common.controller.elements.shield.CenterOfMassUnit;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.common.language.Lng;

public class StabilizerUnit extends CenterOfMassUnit {
   public final PolygonToolsVars v = new PolygonToolsVars();
   public Long2FloatOpenHashMap distances;
   private double stabilization;
   public float distMargin = 5.0F;
   private int reactorSide = -1;
   private long reactorIdSide = Long.MIN_VALUE;
   private boolean bonusSlot;
   private String reactorSideString = "";
   private double bonusEfficiency;
   private Vector3f up = new Vector3f(0.0F, 1.0F, 0.0F);
   private Vector3f hlp = new Vector3f();
   private Vector3f dist = new Vector3f();
   private Vector3f distOther = new Vector3f();
   private Matrix3f m = new Matrix3f();
   public float smallestAngle;
   public StabilizerUnit smallestAngleTo;

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      Vector3i var4;
      (var4 = new Vector3i()).sub(this.getMax(new Vector3i()), this.getMin(new Vector3i()));
      return ControllerManagerGUI.create(var1, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_STABILIZERUNIT_0, this, new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_STABILIZERUNIT_1, var4));
   }

   public void onClear() {
      super.onClear();
      this.distances = null;
   }

   public void calculateExtraDataAfterCreationThreaded(long var1, LongOpenHashSet var3) {
      ObjectArrayList var4;
      double var5;
      synchronized(this.getSegmentController().getState()) {
         try {
            this.getSegmentController().getState().setSynched();
            var4 = new ObjectArrayList(this.getPowerInterface().getMainReactors());
            var5 = this.getPowerInterface().getReactorOptimalDistance();
         } finally {
            this.getSegmentController().getState().setUnsynched();
         }
      }

      this.distances = new Long2FloatOpenHashMap(this.getNeighboringCollection().size());
      this.distances.defaultReturnValue(-1.0F);
      this.stabilization = 0.0D;
      long var7 = 0L;
      long var9 = System.currentTimeMillis();

      for(int var11 = 0; var11 < this.getNeighboringCollection().size(); ++var11) {
         long var12 = this.getNeighboringCollection().getLong(var11);
         float var14 = Float.POSITIVE_INFINITY;

         MainReactorUnit var16;
         for(Iterator var15 = var4.iterator(); var15.hasNext(); var14 = Math.min(var14, var16.distanceToThis(var12, this.v))) {
            var16 = (MainReactorUnit)var15.next();
         }

         if (var14 != Float.POSITIVE_INFINITY) {
            this.distances.put(var12, var14);
            this.stabilization += this.getPowerInterface().calcStabilization(var5, var14);
         }

         if (var11 > 0 && var11 % 10000 == 0) {
            long var23 = System.currentTimeMillis() - var9;
            double var17 = (double)(var7 += var23) / (double)(var11 + 1);
            if (!this.getSegmentController().isOnServer()) {
               System.err.println(this + " DONE: " + var11 + " / " + this.getNeighboringCollection().size() + "; avg processing: " + StringTools.formatPointZeroZero(var17) + " ms");
            }

            var9 = System.currentTimeMillis();
         }
      }

      super.calculateExtraDataAfterCreationThreaded(var1, var3);
      if (this.getSegmentController().isOnServer()) {
         this.distances = null;
      }

   }

   public double getStabilization() {
      return this.stabilization;
   }

   public StabilizerPath calculatePath(double var1, ReactorTree var3, BlockConnectionPath var4) {
      ObjectArrayList var26 = new ObjectArrayList(var4.getPaths());
      Vector3i var5;
      final long var7 = ElementCollection.getIndex(var5 = this.getCoMOrigin());
      final long var9;
      int var24 = ElementCollection.getPosX(var9 = var3.getCenterOfMass());
      int var6 = ElementCollection.getPosY(var9);
      int var11 = ElementCollection.getPosZ(var9);
      final int var12 = ElementCollection.getPosX(var7);
      final int var13 = ElementCollection.getPosY(var7);
      final int var14 = ElementCollection.getPosZ(var7);
      float var15;
      float var16;
      float var17;
      float var18;
      if ((var15 = VoidElementManager.REACTOR_STABILIZATION_ENERGY_STREAM_DISTANCE) >= 0.0F) {
         var16 = (float)(var24 - var12);
         var17 = (float)(var6 - var13);
         var18 = (float)(var11 - var14);
         if (Vector3fTools.length(var16, var17, var18) <= var15) {
            return null;
         }
      }

      var16 = (float)(var24 - var5.x);
      var17 = (float)(var6 - var5.y);
      var18 = (float)(var11 - var5.z);
      var16 = var16 * var16 + var17 * var17 + var18 * var18;
      int var10000 = var5.x;
      var10000 = var5.y;
      var10000 = var5.z;
      float var25 = var16;
      LongArrayList var27 = null;
      Iterator var28 = var26.iterator();

      long var20;
      int var22;
      int var23;
      float var30;
      LongArrayList var31;
      while(var28.hasNext()) {
         LongArrayList var19;
         var31 = var19 = (LongArrayList)var28.next();
         var22 = ElementCollection.getPosX(var20 = var31.getLong(var31.size() - 1));
         var23 = ElementCollection.getPosY(var20);
         var11 = ElementCollection.getPosZ(var20);
         var15 = (float)(var22 - var12);
         var16 = (float)(var23 - var13);
         var30 = (float)(var11 - var14);

         for(var30 = var15 * var15 + var16 * var16 + var30 * var30; var30 < var25; var25 = var30) {
            var27 = var19;
         }
      }

      Collections.sort(var26, new Comparator() {
         public int compare(LongArrayList var1, LongArrayList var2) {
            long var5;
            int var3 = ElementCollection.getPosX(var5 = var1.getLong(var1.size() - 1));
            int var4 = ElementCollection.getPosY(var5);
            int var11 = ElementCollection.getPosZ(var5);
            float var9 = (float)(var3 - var12);
            float var10 = (float)(var4 - var13);
            float var12x = (float)(var11 - var14);
            float var7 = var9 * var9 + var10 * var10 + var12x * var12x;
            var3 = ElementCollection.getPosX(var5 = var2.getLong(var2.size() - 1));
            var4 = ElementCollection.getPosY(var5);
            var11 = ElementCollection.getPosZ(var5);
            var9 = (float)(var3 - var12);
            var10 = (float)(var4 - var13);
            var12x = (float)(var11 - var14);
            float var8 = var9 * var9 + var10 * var10 + var12x * var12x;
            return CompareTools.compare(var7, var8);
         }
      });
      if (var27 != null) {
         for(var6 = 0; var6 < var26.size(); ++var6) {
            var31 = (LongArrayList)var26.get(var6);
            var22 = ElementCollection.getPosX(var20 = var31.getLong(var31.size() - 1));
            var23 = ElementCollection.getPosY(var20);
            var11 = ElementCollection.getPosZ(var20);
            var15 = (float)(var22 - var12);
            var16 = (float)(var23 - var13);
            var30 = (float)(var11 - var14);
            if (var15 * var15 + var16 * var16 + var30 * var30 > var25 + this.distMargin * this.distMargin) {
               var26.remove(var6);
               --var6;
            }
         }

         Collections.sort(var26, new Comparator() {
            public int compare(LongArrayList var1, LongArrayList var2) {
               float var3 = StabilizerUnit.this.calcPathLength(var1, var9, var7);
               float var4 = StabilizerUnit.this.calcPathLength(var2, var9, var7);
               return CompareTools.compare(var3, var4);
            }
         });
         var27 = (LongArrayList)var26.get(0);
      }

      StabilizerPath var29;
      (var29 = new StabilizerPath(var1, this.getPowerInterface(), this)).start = var9;
      long var32 = var29.start;
      long var33;
      if (var27 != null) {
         for(Iterator var21 = var27.iterator(); var21.hasNext(); var32 = var33) {
            var33 = (Long)var21.next();
            var29.nodes.put(var32, var33);
         }
      }

      var29.nodes.put(var32, var7);
      return var29;
   }

   private float calcPathLength(LongArrayList var1, long var2, long var4) {
      float var6 = 0.0F;
      long var7 = var2;

      int var3;
      int var9;
      int var15;
      int var16;
      for(var15 = 0; var15 < var1.size(); ++var15) {
         long var10 = var1.getLong(var15);
         var3 = ElementCollection.getPosX(var7);
         var9 = ElementCollection.getPosY(var7);
         var16 = ElementCollection.getPosZ(var7);
         int var8 = ElementCollection.getPosX(var10);
         int var12 = ElementCollection.getPosY(var10);
         int var13 = ElementCollection.getPosZ(var10);
         var6 += Vector3fTools.length((float)(var8 - var3), (float)(var12 - var9), (float)(var13 - var16));
         var7 = var10;
      }

      var15 = ElementCollection.getPosX(var7);
      int var14 = ElementCollection.getPosY(var7);
      int var11 = ElementCollection.getPosZ(var7);
      var3 = ElementCollection.getPosX(var4);
      var9 = ElementCollection.getPosY(var4);
      var16 = ElementCollection.getPosZ(var4);
      return var6 + Vector3fTools.length((float)(var3 - var15), (float)(var9 - var14), (float)(var16 - var11));
   }

   public int determineSide(long var1, long var3) {
      this.dist.x = (float)(this.getCoMOrigin().x - ElementCollection.getPosX(var3));
      this.dist.y = (float)(this.getCoMOrigin().y - ElementCollection.getPosY(var3));
      this.dist.z = (float)(this.getCoMOrigin().z - ElementCollection.getPosZ(var3));
      ReactorTree var5 = (ReactorTree)this.getPowerInterface().getReactorSet().getTreeMap().get(var1);
      this.m.set(var5.getBonusMatrix());
      this.m.invert();
      this.m.transform(this.dist);
      if (Math.abs(this.dist.x) > Math.abs(this.dist.y) && Math.abs(this.dist.x) >= Math.abs(this.dist.z)) {
         this.reactorSide = this.dist.x >= 0.0F ? 4 : 5;
         this.reactorSideString = "*" + this.dist.x + "*, " + this.dist.y + ", " + this.dist.z;
      } else if (Math.abs(this.dist.y) >= Math.abs(this.dist.x) && Math.abs(this.dist.y) > Math.abs(this.dist.z)) {
         this.reactorSide = this.dist.y >= 0.0F ? 2 : 3;
         this.reactorSideString = this.dist.x + ", *" + this.dist.y + "*, " + this.dist.z;
      } else {
         this.reactorSide = this.dist.z >= 0.0F ? 0 : 1;
         this.reactorSideString = this.dist.x + ", " + this.dist.y + ", *" + this.dist.z + "*";
      }

      this.reactorIdSide = var1;
      return this.reactorSide;
   }

   public float calcAngle(long var1, StabilizerUnit var3, long var4) {
      this.dist.x = (float)(this.getCoMOrigin().x - ElementCollection.getPosX(var4));
      this.dist.y = (float)(this.getCoMOrigin().y - ElementCollection.getPosY(var4));
      this.dist.z = (float)(this.getCoMOrigin().z - ElementCollection.getPosZ(var4));
      this.distOther.x = (float)(var3.getCoMOrigin().x - ElementCollection.getPosX(var4));
      this.distOther.y = (float)(var3.getCoMOrigin().y - ElementCollection.getPosY(var4));
      this.distOther.z = (float)(var3.getCoMOrigin().z - ElementCollection.getPosZ(var4));
      return Vector3fTools.getAngleSigned(this.dist, this.distOther, this.up, this.hlp);
   }

   public long getReactorIdSide() {
      return this.reactorIdSide;
   }

   public int getReactorSide() {
      return this.reactorSide;
   }

   public void setBonusSlot(boolean var1) {
      this.bonusSlot = var1;
   }

   public boolean isBonusSlot() {
      return this.bonusSlot;
   }

   public String getReactorSideString() {
      return this.reactorSideString;
   }

   public double getBonusEfficiency() {
      return this.bonusEfficiency;
   }

   public void setBonusEfficiency(double var1) {
      this.bonusEfficiency = var1;
   }
}
