package org.schema.game.common.controller.elements.power.reactor;

import it.unimi.dsi.fastutil.objects.Object2DoubleOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Observable;
import org.schema.game.network.objects.PowerInterfaceNetworkObject;
import org.schema.game.network.objects.remote.RemoteReactorPriorityQueue;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class ReactorPriorityQueue extends Observable implements Comparator, SerialializationInterface {
   private final List queue;
   private final Object2IntOpenHashMap prioMap = new Object2IntOpenHashMap();
   private final Object2IntOpenHashMap amountMap = new Object2IntOpenHashMap();
   private final Object2DoubleOpenHashMap consumptionMap = new Object2DoubleOpenHashMap();
   private final Object2DoubleOpenHashMap percentMap = new Object2DoubleOpenHashMap();
   private boolean changed;
   private boolean received;
   private float totalPowered;
   private float totalAmount;
   private final PowerInterface pi;

   public ReactorPriorityQueue(PowerInterface var1) {
      this.pi = var1;
      ObjectArrayList var2;
      (var2 = new ObjectArrayList(PowerConsumer.PowerConsumerCategory.values())).trim();
      this.queue = var2;
   }

   public List getQueue() {
      return this.queue;
   }

   public int getAmount(PowerConsumer.PowerConsumerCategory var1) {
      return this.amountMap.getInt(var1);
   }

   public double getConsumption(PowerConsumer.PowerConsumerCategory var1) {
      return this.consumptionMap.getDouble(var1);
   }

   public double getConsumptionPercent(PowerConsumer.PowerConsumerCategory var1) {
      return this.consumptionMap.getDouble(var1) / this.pi.getRechargeRatePowerPerSec();
   }

   public double getPercent(PowerConsumer.PowerConsumerCategory var1, double var2) {
      if (!this.amountMap.containsKey(var1)) {
         return var2;
      } else {
         double var4;
         return (var4 = (double)this.getAmount(var1)) == 0.0D ? 0.0D : this.percentMap.getDouble(var1) / var4;
      }
   }

   public double getPercent(PowerConsumer.PowerConsumerCategory var1) {
      return this.getPercent(var1, 1.0D);
   }

   public void resetStats() {
      this.percentMap.clear();
      this.amountMap.clear();
      this.consumptionMap.clear();
   }

   public void addPercent(PowerConsumer.PowerConsumerCategory var1, double var2) {
      this.percentMap.add(var1, var2);
   }

   public void addConsumption(PowerConsumer.PowerConsumerCategory var1, double var2) {
      this.consumptionMap.add(var1, var2);
   }

   public void addAmount(PowerConsumer.PowerConsumerCategory var1) {
      this.amountMap.add(var1, 1);
   }

   public float getTotalPercent() {
      return this.totalAmount == 0.0F ? 0.0F : this.totalPowered / this.totalAmount;
   }

   public void addTotalPercent(float var1) {
      this.totalPowered += var1;
      ++this.totalAmount;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var2 = this.isDefaultOrder();
      var1.writeBoolean(var2);
      if (!var2) {
         for(int var4 = 0; var4 < this.queue.size(); ++var4) {
            PowerConsumer.PowerConsumerCategory var3 = (PowerConsumer.PowerConsumerCategory)this.queue.get(var4);
            var1.writeByte((byte)var3.ordinal());
         }
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      boolean var4 = var1.readBoolean();
      PowerConsumer.PowerConsumerCategory[] var5 = PowerConsumer.PowerConsumerCategory.values();
      if (var4) {
         this.queue.clear();

         for(var2 = 0; var2 < var5.length; ++var2) {
            this.queue.add(var5[var2]);
         }
      } else {
         this.queue.clear();

         for(var2 = 0; var2 < var5.length; ++var2) {
            this.queue.add(var5[var1.readByte()]);
         }
      }

      this.changed = true;
   }

   private boolean isDefaultOrder() {
      PowerConsumer.PowerConsumerCategory[] var1 = PowerConsumer.PowerConsumerCategory.values();

      for(int var2 = 0; var2 < this.queue.size(); ++var2) {
         if (this.queue.get(var2) != var1[var2]) {
            return false;
         }
      }

      return true;
   }

   public Tag toTagStructure() {
      if (this.isDefaultOrder()) {
         return new Tag(Tag.Type.BYTE, (String)null, (byte)1);
      } else {
         Tag[] var1;
         Tag[] var10000 = var1 = new Tag[this.queue.size() + 1];
         var10000[var10000.length - 1] = FinishTag.INST;

         for(int var2 = 0; var2 < this.queue.size(); ++var2) {
            var1[var2] = new Tag(Tag.Type.BYTE, (String)null, (byte)((PowerConsumer.PowerConsumerCategory)this.queue.get(var2)).ordinal());
         }

         return new Tag(Tag.Type.STRUCT, (String)null, var1);
      }
   }

   public void fromTagStructure(Tag var1) {
      if (var1.getType() == Tag.Type.STRUCT) {
         Tag[] var6 = var1.getStruct();
         PowerConsumer.PowerConsumerCategory[] var2 = PowerConsumer.PowerConsumerCategory.values();
         ObjectOpenHashSet var3 = new ObjectOpenHashSet(var2);
         this.queue.clear();

         for(int var4 = 0; var4 < var6.length - 1; ++var4) {
            byte var5;
            if ((var5 = var6[var4].getByte()) < var2.length) {
               this.queue.add(var2[var5]);
               var3.remove(var2[var5]);
            }
         }

         this.queue.addAll(var3);
         this.flagChanged();
      }

   }

   private void flagChanged() {
      this.changed = true;
   }

   public boolean hasChanged() {
      return this.changed;
   }

   public void updateLocal(Timer var1, PowerInterface var2) {
      if (this.received) {
         if (var2.isOnServer()) {
            this.send(var2.getNetworkObject());
         }

         this.received = false;
         this.setChanged();
         this.notifyObservers();
      }

      if (this.changed) {
         this.prioMap.clear();

         for(int var3 = 0; var3 < this.queue.size(); ++var3) {
            this.prioMap.put(this.queue.get(var3), var3);
         }

         var2.flagConsumersChanged();
         this.changed = false;
         this.setChanged();
         this.notifyObservers();
      }

   }

   public void receive(PowerInterfaceNetworkObject var1) {
      if (var1.getReactorPrioQueueBuffer().getReceiveBuffer().size() > 0) {
         this.received = true;
      }

   }

   public void send(PowerInterfaceNetworkObject var1) {
      var1.getReactorPrioQueueBuffer().add(new RemoteReactorPriorityQueue(this, var1.getReactorPrioQueueBuffer().onServer));
   }

   public void move(PowerInterface var1, PowerConsumer.PowerConsumerCategory var2, int var3) {
      int var4 = this.queue.indexOf(var2);
      int var5 = Math.abs(var3);

      for(var3 = var3 > 0 ? 1 : -1; var5 > 0 && (var3 > 0 || var4 > 0) && (var4 < this.queue.size() - 1 || var3 < 0); --var5) {
         var4 += var3;
      }

      this.moveAbs(var1, var2, var4);
   }

   public int getPriority(PowerConsumer.PowerConsumerCategory var1) {
      return this.prioMap.getInt(var1);
   }

   public void moveAbs(PowerInterface var1, PowerConsumer.PowerConsumerCategory var2, int var3) {
      this.queue.remove(var2);
      this.queue.add(var3, var2);
      this.changed = true;
      this.send(var1.getNetworkObject());
   }

   public int compare(PowerConsumer var1, PowerConsumer var2) {
      return this.getPriority(var1.getPowerConsumerCategory()) - this.getPriority(var2.getPowerConsumerCategory());
   }
}
