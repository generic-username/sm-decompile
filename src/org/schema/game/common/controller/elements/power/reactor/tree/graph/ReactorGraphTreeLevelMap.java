package org.schema.game.common.controller.elements.power.reactor.tree.graph;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

public class ReactorGraphTreeLevelMap {
   public final Int2ObjectOpenHashMap map = new Int2ObjectOpenHashMap();
}
