package org.schema.game.common.controller.elements.power.reactor.chamber;

import it.unimi.dsi.fastutil.longs.LongListIterator;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Set;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.power.reactor.MainReactorUnit;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;

public class ConduitUnit extends ElementCollection {
   private static final int BULK = 32;
   private LongListIterator currentIterator;
   private SegmentPiece tmp = new SegmentPiece();
   private final ObjectOpenHashSet connectedTmp = new ObjectOpenHashSet();
   private final ObjectOpenHashSet connected = new ObjectOpenHashSet();
   private final ObjectOpenHashSet connectedMainTmp = new ObjectOpenHashSet();
   private final ObjectOpenHashSet connectedMain = new ObjectOpenHashSet();
   private Vector3i tmpPos = new Vector3i();
   private boolean dirty;

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      Vector3i var4;
      (var4 = new Vector3i()).sub(this.getMax(new Vector3i()), this.getMin(new Vector3i()));
      return ControllerManagerGUI.create(var1, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_CHAMBER_CONDUITUNIT_0, this, new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_CHAMBER_CONDUITUNIT_1, var4), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_CHAMBER_CONDUITUNIT_2, "N/A"));
   }

   public boolean onChangeFinished() {
      this.flagCalcConnections();
      return super.onChangeFinished();
   }

   public void flagCalcConnections() {
      this.connectedTmp.clear();
      this.connectedMainTmp.clear();
      this.currentIterator = this.getNeighboringCollection().iterator();
      ((ConduitCollectionManager)this.elementCollectionManager).flagConduitsDirty();
   }

   public void updateCalcConnections() {
      for(int var1 = 0; var1 < 32 && !this.isFinishedCalcConnections(); ++var1) {
         long var2 = this.currentIterator.nextLong();
         this.dirty = true;

         for(int var4 = 0; var4 < 6; ++var4) {
            ElementCollection.getPosFromIndex(var2, this.tmpPos);
            this.tmpPos.add(Element.DIRECTIONSi[var4]);
            long var5 = ElementCollection.getIndex(this.tmpPos);
            SegmentPiece var9;
            if ((var9 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var5, this.tmp)) != null && ElementKeyMap.isValidType(var9.getType())) {
               long var7;
               if (ElementKeyMap.getInfoFast(var9.getType()).isReactorChamberAny()) {
                  var7 = var9.getAbsoluteIndex();
                  ReactorChamberUnit var10;
                  if ((var10 = this.getPowerInterface().getReactorChamber(var7)) != null) {
                     this.connectedTmp.add(var10);
                  }
               } else if (var9.getType() == 1008) {
                  var7 = var9.getAbsoluteIndex();
                  MainReactorUnit var11;
                  if ((var11 = this.getPowerInterface().getReactor(var7)) != null) {
                     this.connectedMainTmp.add(var11);
                  }
               }
            }
         }
      }

      if (this.isFinishedCalcConnections() && this.dirty) {
         this.connected.clear();
         this.connected.addAll(this.connectedTmp);
         this.connectedTmp.clear();
         this.connectedMain.clear();
         this.connectedMain.addAll(this.connectedMainTmp);
         this.connectedMainTmp.clear();
         this.dirty = false;
      }

   }

   public boolean isValidConduit() {
      return this.connectedMain.size() == 0 && this.connected.size() == 2 || this.connectedMain.size() == 1 && this.connected.size() > 0;
   }

   public String getInvalidConduit() {
      return !this.isValidConduit() ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_CHAMBER_CONDUITUNIT_3 : "";
   }

   public boolean isFinishedCalcConnections() {
      return !this.currentIterator.hasNext();
   }

   public Set getConnectedChambers() {
      return this.connected;
   }

   public Set getConnectedReactors() {
      return this.connectedMain;
   }
}
