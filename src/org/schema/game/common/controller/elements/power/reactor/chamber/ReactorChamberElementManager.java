package org.schema.game.common.controller.elements.power.reactor.chamber;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.IntegrityBasedInterface;
import org.schema.game.common.controller.elements.UsableControllableSingleElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.graphicsengine.core.Timer;

public class ReactorChamberElementManager extends UsableControllableSingleElementManager implements IntegrityBasedInterface {
   private short chamberType;

   public ReactorChamberElementManager(short var1, SegmentController var2, Class var3) {
      super(var2, var3);
      this.chamberType = var1;
   }

   public void onControllerChange() {
   }

   protected String getTag() {
      return "chamber";
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
   }

   public int getPriority() {
      return 15;
   }

   public ControllerManagerGUI getGUIUnitValues(ReactorChamberUnit var1, ReactorChamberCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return null;
   }

   public ReactorChamberCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new ReactorChamberCollectionManager(this.chamberType, this.getSegmentController(), this);
   }

   protected void playSound(ReactorChamberUnit var1, Transform var2) {
   }
}
