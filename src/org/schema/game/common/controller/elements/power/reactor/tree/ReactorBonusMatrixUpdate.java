package org.schema.game.common.controller.elements.power.reactor.tree;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import javax.vecmath.Matrix3f;
import org.schema.schine.network.SerialializationInterface;

public class ReactorBonusMatrixUpdate implements SerialializationInterface {
   public long id;
   public Matrix3f bonusMatrix;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeLong(this.id);
      var1.writeFloat(this.bonusMatrix.m00);
      var1.writeFloat(this.bonusMatrix.m01);
      var1.writeFloat(this.bonusMatrix.m02);
      var1.writeFloat(this.bonusMatrix.m10);
      var1.writeFloat(this.bonusMatrix.m11);
      var1.writeFloat(this.bonusMatrix.m12);
      var1.writeFloat(this.bonusMatrix.m20);
      var1.writeFloat(this.bonusMatrix.m21);
      var1.writeFloat(this.bonusMatrix.m22);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.id = var1.readLong();
      this.bonusMatrix = new Matrix3f();
      this.bonusMatrix.m00 = var1.readFloat();
      this.bonusMatrix.m01 = var1.readFloat();
      this.bonusMatrix.m02 = var1.readFloat();
      this.bonusMatrix.m10 = var1.readFloat();
      this.bonusMatrix.m11 = var1.readFloat();
      this.bonusMatrix.m12 = var1.readFloat();
      this.bonusMatrix.m20 = var1.readFloat();
      this.bonusMatrix.m21 = var1.readFloat();
      this.bonusMatrix.m22 = var1.readFloat();
   }
}
