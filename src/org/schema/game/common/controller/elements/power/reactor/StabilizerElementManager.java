package org.schema.game.common.controller.elements.power.reactor;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.IntegrityBasedInterface;
import org.schema.game.common.controller.elements.UsableControllableSingleElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.graphicsengine.core.Timer;

public class StabilizerElementManager extends UsableControllableSingleElementManager implements IntegrityBasedInterface {
   public StabilizerElementManager(SegmentController var1, Class var2) {
      super(var1, var2);
   }

   public void onControllerChange() {
   }

   protected String getTag() {
      return "stabilizer";
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
   }

   public ControllerManagerGUI getGUIUnitValues(StabilizerUnit var1, StabilizerCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return null;
   }

   public StabilizerCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new StabilizerCollectionManager(this.getSegmentController(), this);
   }

   protected void playSound(StabilizerUnit var1, Transform var2) {
   }

   public int getPriority() {
      return 15;
   }
}
