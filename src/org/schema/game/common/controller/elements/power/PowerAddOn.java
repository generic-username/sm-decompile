package org.schema.game.common.controller.elements.power;

import java.util.List;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.FloatingRock;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.ManagerUpdatableInterface;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.effectblock.EffectElementManager;
import org.schema.game.common.controller.rails.RailRelation;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.network.objects.remote.RemoteValueUpdate;
import org.schema.game.network.objects.valueUpdate.BatteryActiveValueClientToServerUpdate;
import org.schema.game.network.objects.valueUpdate.BatteryPowerExpectedValueUpdate;
import org.schema.game.network.objects.valueUpdate.BatteryPowerValueUpdate;
import org.schema.game.network.objects.valueUpdate.NTValueUpdateInterface;
import org.schema.game.network.objects.valueUpdate.PowerExpectedValueUpdate;
import org.schema.game.network.objects.valueUpdate.PowerRechargeValueUpdate;
import org.schema.game.network.objects.valueUpdate.PowerValueUpdate;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class PowerAddOn implements ManagerUpdatableInterface {
   private final PowerManagerInterface powerManager;
   private final SegmentController segmentController;
   private double power;
   private double batteryPower;
   private double maxPower;
   private double recharge;
   private double powerRailed;
   private double maxPowerRailed;
   private double rechargeRailed;
   private long recovery;
   private double initialPower;
   private long disabledTime = -1L;
   private long lastDiabledTime;
   private double powerBefore;
   private long lastSend;
   private double powerConsumedPerSecondTmp;
   private boolean rechargeEnabled = true;
   private float powerTimeAdd;
   private double powerConsumedPerSecond;
   private double over;
   private double batteryMaxPower;
   private double batteryRecharge;
   private double initialBatteryPower;
   private boolean batteryActive = true;
   private Vector3i selectedSlotBinding = new Vector3i();
   private int expectedPowerClient = -1;

   public PowerAddOn(PowerManagerInterface var1, SegmentController var2) {
      this.powerManager = var1;
      this.segmentController = var2;
      var1.addUpdatable(this);
   }

   public double consumePower(double var1, Timer var3) {
      return this.consumePowerInstantly(var1 * (double)var3.getDelta()) ? var1 * (double)var3.getDelta() : 0.0D;
   }

   public boolean canConsumePowerInstantly(float var1) {
      if (var1 == 0.0F) {
         return true;
      } else if (!this.segmentController.railController.isDockedAndExecuted() && !this.segmentController.getDockingController().isDocked() && !this.sufficientPower((double)var1)) {
         return false;
      } else {
         if (this.segmentController.getDockingController().isDocked()) {
            SegmentController var2;
            if ((var2 = this.segmentController.getDockingController().getDockedOn().to.getSegment().getSegmentController()) instanceof ManagedSegmentController && ((ManagedSegmentController)var2).getManagerContainer() instanceof PowerManagerInterface) {
               PowerManagerInterface var3 = (PowerManagerInterface)((ManagedSegmentController)var2).getManagerContainer();
               double var10000 = this.power;
               var10000 = this.over;
               if ((double)var1 > this.power + this.over) {
                  return var3.getPowerAddOn().canConsumePowerInstantly(var1);
               }
            }
         } else if (this.segmentController.railController.isDockedAndExecuted()) {
            return this.sufficientPowerRail((double)var1);
         }

         return true;
      }
   }

   public boolean consumePowerInstantly(double var1) {
      return this.consumePowerInstantly(var1, false);
   }

   public boolean consumePowerInstantly(double var1, boolean var3) {
      double var4 = var1;
      if (this.over > 0.0D) {
         double var6 = Math.min(var1, this.over);
         var1 -= var6;
         this.over -= var6;
      }

      if (!var3 && !this.segmentController.railController.isDockedAndExecuted() && !this.segmentController.getDockingController().isDocked() && !this.sufficientPowerRail(var1)) {
         if (this.recovery <= 0L) {
            this.recovery = System.currentTimeMillis();
         }

         return false;
      } else {
         boolean var11 = true;
         double var7 = this.power;
         SegmentController var9;
         if (this.segmentController.getDockingController().isDocked()) {
            if ((var9 = this.segmentController.getDockingController().getDockedOn().to.getSegment().getSegmentController()) instanceof ManagedSegmentController && ((ManagedSegmentController)var9).getManagerContainer() instanceof PowerManagerInterface) {
               PowerManagerInterface var10 = (PowerManagerInterface)((ManagedSegmentController)var9).getManagerContainer();
               double var10000 = this.power;
               if (var1 > this.power) {
                  return var10.getPowerAddOn().consumePowerInstantly(var1);
               }

               if (this.over > var1) {
                  this.over -= var1;
               } else {
                  this.power = Math.max(0.0D, this.power - var1);
                  this.powerRailed = Math.max(0.0D, this.powerRailed - var1);
               }
            } else {
               this.setPower(Math.max(0.0D, this.power - var1));
               this.powerRailed = Math.max(0.0D, this.powerRailed - var1);
            }
         } else if (this.segmentController.railController.isDockedAndExecuted()) {
            var11 = false;
            if (this.power >= var1) {
               this.power -= var1;
               this.powerRailed -= var1;
               var11 = true;
            } else {
               var1 -= this.power;
               this.powerRailed = Math.max(0.0D, this.powerRailed - this.power);
               this.power = 0.0D;
               if (this.recovery <= 0L) {
                  this.recovery = System.currentTimeMillis();
               }

               if ((var9 = this.segmentController.railController.previous.rail.getSegmentController()) instanceof ManagedSegmentController && ((ManagedSegmentController)var9).getManagerContainer() instanceof PowerManagerInterface) {
                  var11 = ((PowerManagerInterface)((ManagedSegmentController)var9).getManagerContainer()).getPowerAddOn().consumePowerInstantly(var1);
               }
            }
         } else {
            this.power = Math.max(0.0D, this.power - var1);
            this.powerRailed = Math.max(0.0D, this.powerRailed - var1);
         }

         if (this.power == 0.0D && var7 > 0.0D) {
            if (this.recovery <= 0L) {
               this.recovery = System.currentTimeMillis();
            }

            if (this.segmentController.isOnServer()) {
               this.sendPowerUpdate();
            }
         }

         this.powerConsumedPerSecondTmp += var4;
         return var11;
      }
   }

   public void sendPowerUpdate() {
      assert this.segmentController.isOnServer();

      PowerValueUpdate var1 = new PowerValueUpdate();

      assert var1.getType() == ValueUpdate.ValTypes.POWER;

      var1.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   public void sendPowerExpectedUpdate() {
      assert this.segmentController.isOnServer();

      PowerExpectedValueUpdate var1 = new PowerExpectedValueUpdate();

      assert var1.getType() == ValueUpdate.ValTypes.POWER_EXPECTED;

      var1.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   public void sendBatteryPowerUpdate() {
      assert this.segmentController.isOnServer();

      BatteryPowerValueUpdate var1 = new BatteryPowerValueUpdate();

      assert var1.getType() == ValueUpdate.ValTypes.POWER_BATTERY;

      var1.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   public void sendBatteryPowerExpectedUpdate() {
      assert this.segmentController.isOnServer();

      BatteryPowerExpectedValueUpdate var1 = new BatteryPowerExpectedValueUpdate();

      assert var1.getType() == ValueUpdate.ValTypes.POWER_BATTERY_EXPECTED;

      var1.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   public void sendBatteryActiveUpdateClient() {
      assert !this.segmentController.isOnServer();

      BatteryActiveValueClientToServerUpdate var1 = new BatteryActiveValueClientToServerUpdate();

      assert var1.getType() == ValueUpdate.ValTypes.POWER_BATTERY_ACTIVE;

      var1.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   public void sendRechargeEnabledUpdate() {
      assert this.getSegmentController().isOnServer();

      PowerRechargeValueUpdate var1 = new PowerRechargeValueUpdate();

      assert var1.getType() == ValueUpdate.ValTypes.POWER_REGEN_ENABLED;

      var1.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   public SegmentController getSegmentController() {
      return this.segmentController;
   }

   public void disableTemporary(int var1) {
      this.disabledTime = System.currentTimeMillis() + (long)var1;
      this.lastDiabledTime = System.currentTimeMillis() + (long)var1;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.setInitialPower((Double)var2[0].getValue());
      this.setInitialBatteryPower((Double)var2[1].getValue());
   }

   public void fromTagStructureOld(Tag var1) {
      this.setInitialPower((Double)var1.getValue());
   }

   public int getBaseCapacity() {
      if (this.segmentController instanceof Planet) {
         return VoidElementManager.POWER_FIXED_PLANET_BASE_CAPACITY;
      } else {
         return this.segmentController instanceof FloatingRock ? VoidElementManager.POWER_FIXED_ASTEROID_BASE_CAPACITY : VoidElementManager.POWER_FIXED_BASE_CAPACITY;
      }
   }

   public double getInitialPower() {
      return this.initialPower;
   }

   public void setInitialPower(double var1) {
      this.initialPower = var1;
   }

   public void setInitialBatteryPower(double var1) {
      this.initialBatteryPower = var1;
   }

   public long getLastDiabledTime() {
      return this.lastDiabledTime;
   }

   public double getMaxPowerWithoutDock() {
      return (double)this.getBaseCapacity() + this.maxPower;
   }

   public double getMaxPower() {
      if (this.segmentController.railController.isDockedAndExecuted()) {
         return this.maxPowerRailed;
      } else {
         double var3;
         if (this.segmentController.getDockingController().isDocked()) {
            SegmentController var1;
            if ((var1 = this.segmentController.getDockingController().getDockedOn().to.getSegment().getSegmentController()) instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof PowerManagerInterface) {
               PowerManagerInterface var4 = (PowerManagerInterface)((ManagedSegmentController)var1).getManagerContainer();
               var3 = this.maxPower + var4.getPowerCapacityManager().getMaxPower() + (double)this.getBaseCapacity();
            } else {
               var3 = this.maxPower;
            }
         } else {
            var3 = this.maxPower;
         }

         return (double)this.getBaseCapacity() + var3;
      }
   }

   public void setMaxPower(double var1) {
      this.maxPower = var1;
   }

   public double getPower() {
      return this.segmentController.railController.isDockedAndExecuted() ? this.powerRailed : this.power;
   }

   public void setPower(double var1) {
      this.power = var1;
   }

   public double getPowerSimple() {
      return this.power;
   }

   public double getPowerRailed() {
      return this.powerRailed;
   }

   public double getRechargeForced() {
      return this.recharge * (double)((SendableSegmentController)this.getSegmentController()).getBlockEffectManager().status.powerRegenPercent;
   }

   public double getRecharge() {
      return !this.rechargeEnabled ? 0.0D : this.recharge * (double)((SendableSegmentController)this.getSegmentController()).getBlockEffectManager().status.powerRegenPercent;
   }

   public void setRecharge(double var1) {
      this.recharge = var1;
   }

   public void incPower(double var1) {
      double var3 = (double)this.getBaseCapacity() + this.maxPower;
      this.setPower(Math.min(var3, this.power + var1));
   }

   private void recalcPowerRailedRoot() {
      this.recalcPowerRailedRoot(0.0D, 0.0D, 0.0D);
   }

   private void recalcPowerRailedRoot(double var1, double var3, double var5) {
      if (this.segmentController.isVirtualBlueprint() && this.segmentController.railController.isDockedAndExecuted() && !this.segmentController.railController.previous.rail.getSegmentController().isVirtualBlueprint()) {
         var5 = 0.0D;
         var3 = 0.0D;
         var1 = 0.0D;
      }

      this.powerRailed = var1 + this.power;
      this.rechargeRailed = var3 + this.getRecharge();
      this.maxPowerRailed = var5 + this.maxPower + (double)this.getBaseCapacity();
      List var7;
      int var2 = (var7 = this.segmentController.railController.next).size();

      for(int var8 = 0; var8 < var2; ++var8) {
         ((PowerManagerInterface)((ManagedSegmentController)((RailRelation)var7.get(var8)).docked.getSegmentController()).getManagerContainer()).getPowerAddOn().recalcPowerRailedRoot(this.powerRailed, this.rechargeRailed, this.maxPowerRailed);
      }

   }

   public boolean isInRecovery() {
      return this.recovery >= 0L;
   }

   private boolean sufficientPower(double var1) {
      return var1 <= this.power + this.over;
   }

   private boolean sufficientPowerRail(double var1) {
      return var1 <= this.powerRailed + this.over;
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.DOUBLE, (String)null, this.power), new Tag(Tag.Type.DOUBLE, (String)null, this.batteryPower), FinishTag.INST});
   }

   private void handleInitialPower(double var1) {
      if (this.power >= this.getMaxPower() && this.initialPower > 0.0D) {
         int var5;
         if (this.isOnServer()) {
            if (this.segmentController instanceof Ship && ((Ship)this.segmentController).getSpawnedFrom() != null) {
               var5 = ((Ship)this.segmentController).getSpawnedFrom().getElementMap().get((short)331);
            } else {
               var5 = this.powerManager.getPowerCapacityManager().expected;
            }
         } else {
            var5 = this.expectedPowerClient;
         }

         if (var5 >= 0 && this.powerManager.getPowerCapacityManager().getTotalSize() >= var5) {
            System.err.println("[POWER] " + this.segmentController + " CHECKING against bb: " + var5 + " / " + this.powerManager.getPowerCapacityManager().getTotalSize() + "; limit reached. setting cap to prevent overcharge");
            this.initialPower = 0.0D;
            this.setPower(this.getMaxPower());
            if (this.isOnServer()) {
               this.sendPowerUpdate();
               this.sendPowerExpectedUpdate();
            }
         }

      } else {
         double var3;
         if ((var3 = var1 - this.power) > 0.0D) {
            var3 = Math.min(var3, this.initialPower);
            this.initialPower -= var3;
            this.setPower(this.power + var3);
         }

         if (this.getSegmentController().getSegmentBuffer().isFullyLoaded() && this.getPower() >= this.getMaxPower()) {
            this.initialPower = 0.0D;
         }

      }
   }

   private void handleRecharge(Timer var1, double var2, double var4) {
      if (this.initialPower > 0.0D) {
         this.handleInitialPower(var4);
      } else {
         double var6;
         if ((var6 = this.power + (double)var1.getDelta() * var2 - var4) > 0.0D) {
            this.over = var6;
         }

         this.setPower(Math.min(var4, this.power + (double)var1.getDelta() * var2));
         if (this.isBatteryActive() && this.batteryPower > 0.0D) {
            double var8 = Math.min(this.batteryPower, this.getPowerBatteryConsumedPerSecond() * (double)var1.getDelta());
            if (VoidElementManager.POWER_BATTERY_TRANSFER_TOP_OFF_ONLY) {
               var8 = Math.min(Math.max(0.0D, var4 - this.power), var8);
            }

            this.setPower(Math.min(var4, this.power + var8));
            this.batteryPower -= var8;
         }

      }
   }

   public double getPowerBatteryConsumedPerSecond() {
      return (double)VoidElementManager.POWER_BATTERY_TRANSFER_RATE_PER_SEC * this.batteryMaxPower;
   }

   public int getExpectedBatterySize() {
      return this.powerManager.getPowerBatteryManager().expected;
   }

   public int getExpectedPowerSize() {
      return this.powerManager.getPowerManager().expected;
   }

   private void handleInitialBatteryPower(double var1) {
      if (this.batteryPower >= this.batteryMaxPower && this.initialBatteryPower > 0.0D) {
         int var5;
         if (this.isOnServer()) {
            if (this.segmentController instanceof Ship && ((Ship)this.segmentController).getSpawnedFrom() != null) {
               var5 = ((Ship)this.segmentController).getSpawnedFrom().getElementMap().get((short)978);
            } else {
               var5 = this.powerManager.getPowerBatteryManager().expected;
            }
         } else {
            var5 = this.expectedPowerClient;
         }

         if (var5 >= 0 && this.powerManager.getPowerBatteryManager().getTotalSize() >= var5) {
            System.err.println("[POWERBATTERY] " + this.segmentController + " CHECKING against bb: " + var5 + " / " + this.powerManager.getPowerCapacityManager().getTotalSize() + "; limit reached. setting cap to prevent overcharge");
            this.initialBatteryPower = 0.0D;
            this.batteryPower = this.batteryMaxPower;
            if (this.isOnServer()) {
               this.sendBatteryPowerUpdate();
               this.sendBatteryPowerExpectedUpdate();
            }
         }

      } else {
         double var3;
         if ((var3 = var1 - this.batteryPower) > 0.0D) {
            var3 = Math.min(var3, this.initialBatteryPower);
            this.initialBatteryPower -= var3;
            this.batteryPower += var3;
         }

      }
   }

   public boolean isOnServer() {
      return this.segmentController.isOnServer();
   }

   private void handleBatteryRecharge(Timer var1, double var2, double var4) {
      if (this.initialBatteryPower > 0.0D) {
         this.handleInitialBatteryPower(var4);
      } else {
         double var10000 = this.batteryPower;
         var1.getDelta();
         this.batteryPower = Math.min(var4, this.batteryPower + (double)var1.getDelta() * var2);
      }
   }

   public void update(Timer var1) {
      if (((ManagedSegmentController)this.getSegmentController()).getManagerContainer().isRequestedInitalValuesIfNeeded()) {
         this.over = 0.0D;
         this.powerTimeAdd += var1.getDelta();
         if (this.powerTimeAdd >= 1.0F) {
            this.powerConsumedPerSecond = this.powerConsumedPerSecondTmp;
            this.powerConsumedPerSecondTmp = 0.0D;
            this.powerTimeAdd = 0.0F;
         }

         if (System.currentTimeMillis() - this.recovery > VoidElementManager.POWER_RECOVERY_TIME) {
            this.recovery = -1L;
         }

         if (this.segmentController.getHpController().isRebooting()) {
            this.power = 0.0D;
            this.powerBefore = this.power;
         } else if (!this.isInRecovery()) {
            double var2 = (double)this.getBaseCapacity() + this.maxPower;
            this.handleBatteryRecharge(var1, this.getBatteryRecharge(), this.batteryMaxPower);
            if (this.disabledTime > 0L) {
               if (System.currentTimeMillis() >= this.disabledTime) {
                  this.disabledTime = -1L;
               }
            } else {
               this.handleRecharge(var1, this.getRecharge(), var2);
            }

            double var10000;
            if (this.segmentController.isOnServer()) {
               var10000 = this.initialPower;
            }

            if (this.segmentController.isOnServer()) {
               var10000 = this.initialBatteryPower;
            }

            long var4;
            boolean var8 = (var4 = System.currentTimeMillis()) - this.lastSend > 25000L;
            if (this.segmentController.isOnServer() && (this.powerBefore != this.power || var8)) {
               double var6 = var2 / 100.0D;
               if (var8 || Math.abs(this.powerBefore - this.power) >= var6 * ((Integer)ServerConfig.BROADCAST_SHIELD_PERCENTAGE.getCurrentState()).doubleValue() || this.power == 0.0D || this.power == var2) {
                  this.sendPowerUpdate();
                  this.powerBefore = this.power;
                  this.lastSend = var4;
               }
            }
         }

         if (!this.segmentController.railController.isDockedOrDirty()) {
            this.recalcPowerRailedRoot(0.0D, 0.0D, 0.0D);
         }

      }
   }

   public String getPowerString() {
      return EngineSettings.G_SHOW_PURE_NUMBERS_FOR_SHIELD_AND_POWER.isOn() ? "[" + StringTools.formatPointZero(this.getPower()) + " / " + StringTools.formatPointZero(this.getMaxPower()) + " # IN " + this.getInitialPower() + " Power]" : "[" + StringTools.formatPointZero(this.getPower() / this.getMaxPower() * 100.0D) + "%" + (this.getInitialPower() > 0.0D ? "(+)" : "") + " Power]";
   }

   public float getPercentOne() {
      return (float)(this.getPower() / this.getMaxPower());
   }

   public float getBatteryPercentOne() {
      return (float)(this.batteryPower / Math.max(1.0E-5D, this.batteryMaxPower));
   }

   public float getPercentOneOfLowestInChain() {
      if (this.segmentController.railController.isRoot()) {
         return this.getPercentOne();
      } else {
         SegmentController var1;
         if ((var1 = this.segmentController.railController.previous.rail.getSegmentController()) instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof PowerManagerInterface) {
            PowerManagerInterface var2 = (PowerManagerInterface)((ManagedSegmentController)var1).getManagerContainer();
            return Math.max(this.getPercentOne(), var2.getPowerAddOn().getPercentOneOfLowestInChain());
         } else {
            return this.getPercentOne();
         }
      }
   }

   public double getPowerConsumedPerSecond() {
      return this.powerConsumedPerSecond;
   }

   public void sendNoPowerHitEffectIfNeeded() {
      if (this.powerRailed <= 0.5D && this.segmentController.isClientOwnObject() && ((GameClientState)this.segmentController.getState()).getWorldDrawer() != null) {
         ((GameClientState)this.segmentController.getState()).getWorldDrawer().getGuiDrawer().notifyEffectHit(this.getSegmentController(), EffectElementManager.OffensiveEffects.NO_POWER);
      }

   }

   public boolean isRechargeEnabled() {
      return this.rechargeEnabled;
   }

   public void setRechargeEnabled(boolean var1) {
      this.rechargeEnabled = var1;
      if (this.getSegmentController().isOnServer()) {
         this.sendRechargeEnabledUpdate();
      }

   }

   public void setBatteryMaxPower(double var1) {
      this.batteryMaxPower = var1;
   }

   public void setBatteryRecharge(double var1) {
      this.batteryRecharge = var1;
   }

   public double getBatteryRechargeRaw() {
      return this.batteryRecharge;
   }

   public double getBatteryRecharge() {
      return this.isBatteryActive() ? this.getBatteryRechargeOn() : this.getBatteryRechargeOff();
   }

   public double getBatteryRechargeOn() {
      return this.batteryRecharge * VoidElementManager.POWER_BATTERY_TURNED_ON_MULT;
   }

   public double getBatteryRechargeOff() {
      return this.batteryRecharge * VoidElementManager.POWER_BATTERY_TURNED_OFF_MULT;
   }

   public double getBatteryMaxPower() {
      return this.batteryMaxPower;
   }

   public double getBatteryPower() {
      return this.batteryPower;
   }

   public void setBatteryPower(double var1) {
      this.batteryPower = var1;
   }

   public double getInitialBatteryPower() {
      return this.initialBatteryPower;
   }

   public boolean isBatteryActive() {
      return ((ManagedSegmentController)this.segmentController).getManagerContainer().isPowerBatteryAlwaysOn() ? true : this.batteryActive;
   }

   public void setBatteryActive(boolean var1) {
      System.err.println("[POWERADDON] " + this.getSegmentController().getState() + "; " + this.getSegmentController() + " BATTERY ACTIVE: " + var1);
      this.batteryActive = var1;
   }

   public void setExpectedBatteryClient(int var1) {
   }

   public void setExpectedPowerClient(int var1) {
      this.expectedPowerClient = var1;
   }

   public int updatePrio() {
      return 100;
   }

   public boolean canUpdate() {
      return true;
   }

   public void onNoUpdate(Timer var1) {
   }
}
