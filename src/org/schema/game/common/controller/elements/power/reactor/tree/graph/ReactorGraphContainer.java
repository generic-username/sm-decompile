package org.schema.game.common.controller.elements.power.reactor.tree.graph;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraphElementGraphicsContainer;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraphElementGraphicsGlobal;

public abstract class ReactorGraphContainer extends GUIGraphElementGraphicsContainer {
   public Vector4f customBackgroundColor;
   protected static Vector4f textColor0 = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   protected static Vector4f neutral = new Vector4f(1.0F, 1.0F, 1.0F, 0.9F);
   protected static Vector4f backgroundColor0 = new Vector4f(0.3F, 0.2F, 0.2F, 0.8F);
   protected static Vector4f backgroundColorGrey = new Vector4f(0.2F, 0.2F, 0.2F, 0.8F);
   protected static Vector4f backgroundColorRed = new Vector4f(0.6F, 0.2F, 0.2F, 0.8F);
   protected static Vector4f backgroundColorGreen = new Vector4f(0.2F, 0.6F, 0.2F, 0.8F);
   protected static Vector4f backgroundColorBlue = new Vector4f(0.2F, 0.2F, 0.6F, 0.8F);
   protected static Vector4f backgroundColorYellow = new Vector4f(0.2F, 0.6F, 0.6F, 0.8F);
   protected static Vector4f connectionColorGrey = new Vector4f(0.8F, 0.8F, 0.8F, 1.0F);
   protected static Vector4f connectionColorYellow = new Vector4f(0.5F, 0.8F, 0.8F, 1.0F);
   protected static Vector4f connectionColorBlue = new Vector4f(0.5F, 0.5F, 0.8F, 1.0F);
   protected static Vector4f connectionColorRed = new Vector4f(1.0F, 0.4F, 0.4F, 1.0F);
   protected static Vector4f connectionColorGreen = new Vector4f(0.4F, 1.0F, 0.4F, 1.0F);
   public static final int CONNECTION_BLUE = 0;
   public static final int CONNECTION_ORANGE = 1;
   public static final int CONNECTION_RED = 2;
   public static final int CONNECTION_GREEN = 3;

   public ReactorGraphContainer(GUIGraphElementGraphicsGlobal var1) {
      super(var1);
   }

   public Vector4f getBackgroundColor() {
      return this.customBackgroundColor == null ? backgroundColor0 : this.customBackgroundColor;
   }

   public Vector4f getTextColor() {
      return textColor0;
   }

   public FontLibrary.FontSize getFontSize() {
      return FontLibrary.FontSize.MEDIUM;
   }

   public int getTextOffsetX() {
      return 24;
   }

   public int getTextOffsetY() {
      return 24;
   }
}
