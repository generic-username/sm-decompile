package org.schema.game.common.controller.elements.power.reactor.chamber;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class ConduitCollectionManager extends ElementCollectionManager implements PowerConsumer {
   private boolean allConduitsReady;

   public ConduitCollectionManager(SegmentController var1, ConduitElementManager var2) {
      super((short)1010, var1, var2);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return ConduitUnit.class;
   }

   public boolean needsUpdate() {
      return true;
   }

   public void update(Timer var1) {
      boolean var4 = true;
      Iterator var2 = this.getElementCollections().iterator();

      while(var2.hasNext()) {
         ConduitUnit var3;
         (var3 = (ConduitUnit)var2.next()).updateCalcConnections();
         if (!var3.isFinishedCalcConnections()) {
            var4 = false;
         }
      }

      if (var4 && !this.allConduitsReady) {
         this.getPowerInterface().onAnyReactorModulesChanged();
      }

      this.allConduitsReady = var4;
   }

   public void checkRemovedChamber(List var1) {
      Iterator var2 = this.getElementCollections().iterator();

      while(var2.hasNext()) {
         ConduitUnit var3;
         Iterator var4 = (var3 = (ConduitUnit)var2.next()).getConnectedChambers().iterator();

         while(var4.hasNext()) {
            ReactorChamberUnit var5 = (ReactorChamberUnit)var4.next();
            if (!var1.contains(var5)) {
               var3.flagCalcConnections();
            }
         }
      }

   }

   public ConduitUnit getInstance() {
      return new ConduitUnit();
   }

   protected void onChangedCollection() {
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[]{new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_CHAMBER_CONDUITCOLLECTIONMANAGER_0, "")};
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_CHAMBER_CONDUITCOLLECTIONMANAGER_1;
   }

   public float getSensorValue(SegmentPiece var1) {
      return 0.0F;
   }

   public Set getConnected(long var1) {
      Iterator var3 = this.getElementCollections().iterator();

      ConduitUnit var4;
      do {
         if (!var3.hasNext()) {
            return null;
         }
      } while(!(var4 = (ConduitUnit)var3.next()).getNeighboringCollection().contains(var1));

      return var4.getConnectedChambers();
   }

   public boolean isAllConduitsReady() {
      return this.allConduitsReady;
   }

   public void flagConduitsDirty() {
      this.allConduitsReady = false;
   }

   public double getPowerConsumedPerSecondResting() {
      float var1 = VoidElementManager.POWER_REACTOR_CONDUIT_POWER_CONSUMPTION_PER_SEC;
      this.getSegmentController().getConfigManager().apply(StatusEffectType.POWER_CONDUIT_POWER_USAGE, var1);
      return (double)var1;
   }

   public double getPowerConsumedPerSecondCharging() {
      return this.getPowerConsumedPerSecondResting();
   }

   public boolean isPowerCharging(long var1) {
      return false;
   }

   public void setPowered(float var1) {
   }

   public float getPowered() {
      return 1.0F;
   }

   public boolean isPowerConsumerActive() {
      return false;
   }

   public void reloadFromReactor(double var1, Timer var3, float var4, boolean var5, float var6) {
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.OTHERS;
   }

   public void dischargeFully() {
   }
}
