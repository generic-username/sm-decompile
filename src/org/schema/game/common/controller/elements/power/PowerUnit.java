package org.schema.game.common.controller.elements.power;

import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.common.language.Lng;

public class PowerUnit extends ElementCollection {
   private double recharge;

   private static double f(double var0) {
      return Math.max(0.1D, 1.0D - 0.5D * Math.pow(2.0D * (1.0D - var0), 2.5D));
   }

   public static double integrate(double var0, double var2) {
      double var4 = (var2 - var0) / 9.0D;
      double var6 = 0.3333333333333333D * (f(var0) + f(var2));

      double var8;
      int var10;
      for(var10 = 1; var10 < 9; var10 += 2) {
         var8 = var0 + var4 * (double)var10;
         var6 += 1.3333333333333333D * f(var8);
      }

      for(var10 = 2; var10 < 9; var10 += 2) {
         var8 = var0 + var4 * (double)var10;
         var6 += 0.6666666666666666D * f(var8);
      }

      return var6 * var4;
   }

   public boolean hasMesh() {
      return false;
   }

   public double getRecharge() {
      return this.recharge;
   }

   public void refreshPowerCapabilities() {
      this.recharge = (double)this.getBBTotalSize();
      this.recharge = Math.max(0.33329999446868896D, Math.pow(this.recharge / 3.0D, 1.7D));
   }

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      Vector3i var4;
      (var4 = new Vector3i()).sub(this.getMax(new Vector3i()), this.getMin(new Vector3i()));
      return ControllerManagerGUI.create(var1, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_POWERUNIT_0, this, new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_POWERUNIT_1, var4), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_POWERUNIT_2, StringTools.formatPointZero(this.recharge)));
   }
}
