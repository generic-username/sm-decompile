package org.schema.game.common.controller.elements.power.reactor.tree.graph;

import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.reactor.GUIReactorTree;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorElement;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraphConnection;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraphElementBackground;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraphElementGraphicsGlobal;

public class ReactorGraphContainerReactorElement extends ReactorGraphContainer implements GUICallback {
   private ReactorElement element;

   public ReactorGraphContainerReactorElement(GUIGraphElementGraphicsGlobal var1, ReactorElement var2) {
      super(var1);
      this.element = var2;
   }

   public String getText() {
      return this.element.getInfo().getName();
   }

   public GUICallback getSelectionCallback() {
      return this;
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         GUIReactorTree.selected = this.element.getId();
      }

   }

   public boolean isOccluded() {
      return !this.getGlobal().isActive();
   }

   public boolean isSelected() {
      return GUIReactorTree.selected == this.element.getId();
   }

   public Vector4f getBackgroundColor() {
      return neutral;
   }

   public Vector4f getConnectionColorTo() {
      return neutral;
   }

   public void setConnectionColor(GUIGraphConnection var1) {
      Sprite var2 = Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 32px Conduit-4x1-gui-");
      byte var3 = 0;
      if (this.element.isGeneral()) {
         if (!this.element.isParentValidTreeNode()) {
            var3 = 2;
         } else {
            var3 = 1;
         }
      } else if (this.element.isValidTreeNode()) {
         var3 = 0;
      }

      var1.setTextured(var2.getMaterial().getTexture(), var3, 4);
      var1.getLineColor().set(this.getConnectionColorTo());
   }

   public void setBackgroundColor(GUIGraphElementBackground var1) {
      ReactorGraphBackground var2;
      (var2 = (ReactorGraphBackground)var1).setColorEnum(ReactorGraphBackground.ColorEnum.GREEN);
      if (this.element.isGeneral()) {
         if (!this.element.isParentValidTreeNode()) {
            if (this.element.isGeneralChain()) {
               var2.setColorEnum(ReactorGraphBackground.ColorEnum.YELLOW_OFF);
            } else {
               var2.setColorEnum(ReactorGraphBackground.ColorEnum.RED_OFF);
            }
         } else if (this.element.isGeneral() && this.element.parent != null && !this.element.parent.isGeneral() && this.element.parent.getInfo().chamberRoot != this.element.type) {
            var2.setColorEnum(ReactorGraphBackground.ColorEnum.RED_OFF);
         } else {
            var2.setColorEnum(ReactorGraphBackground.ColorEnum.BLUE_OFF);
         }
      } else if (!this.element.isValidTreeNode()) {
         var2.setColorEnum(ReactorGraphBackground.ColorEnum.RED_OFF);
      } else if (!this.element.isBooted()) {
         var2.setColorEnum(ReactorGraphBackground.ColorEnum.ORANGE_OFF);
      }

      var2.setColor(this.getBackgroundColor());
   }

   public String getToolTipText() {
      if (!this.element.root.isWithinCapacity()) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_GRAPH_REACTORGRAPHCONTAINERREACTORELEMENT_7;
      } else if (!this.element.validConduit) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_GRAPH_REACTORGRAPHCONTAINERREACTORELEMENT_0;
      } else if (this.element.isGeneral()) {
         if (!this.element.isParentValidTreeNode()) {
            return this.element.isGeneralChain() ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_GRAPH_REACTORGRAPHCONTAINERREACTORELEMENT_1 : Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_GRAPH_REACTORGRAPHCONTAINERREACTORELEMENT_2;
         } else {
            return this.element.isGeneral() && this.element.parent != null && !this.element.parent.isGeneral() && this.element.parent.getInfo().chamberRoot != this.element.type ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_GRAPH_REACTORGRAPHCONTAINERREACTORELEMENT_5 : this.element.getInfo().getName() + "\n" + Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_GRAPH_REACTORGRAPHCONTAINERREACTORELEMENT_6;
         }
      } else if (!this.element.isValidTreeNode()) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_GRAPH_REACTORGRAPHCONTAINERREACTORELEMENT_8;
      } else {
         String var1 = this.element.getInfo().getChamberEffectInfo(((GameClientState)this.getGlobal().getState()).getConfigPool());
         if (this.element.getInfo().isChamberUpgraded()) {
            var1 = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_GRAPH_REACTORGRAPHCONTAINERREACTORELEMENT_3 + var1;
         }

         String var2 = StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_GRAPH_REACTORGRAPHCONTAINERREACTORELEMENT_9, StringTools.formatPointZero(this.element.getChamberCapacity() * 100.0F), StringTools.formatPointZero(this.element.getCapacityRecursivelyUpwards() * 100.0F));
         return this.element.getInfo().getName() + "\n" + var1 + "\n" + var2 + "\n" + Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_GRAPH_REACTORGRAPHCONTAINERREACTORELEMENT_4;
      }
   }

   public GUIGraphElementBackground initiateBackground() {
      return new ReactorGraphBackgroundBig(this.getGlobal().getState());
   }

   public void drawExtra(int var1, int var2) {
      if (!this.element.isBooted()) {
         float var3;
         if ((double)(var3 = 1.0F - this.element.getBootStatusPercent()) < 0.25D) {
            GlUtil.glColor4f(1.0F, 0.0F, 0.0F, 0.5F);
         } else if ((double)var3 < 0.5D) {
            GlUtil.glColor4f(0.0F, 1.0F, 1.0F, 0.5F);
         } else if ((double)var3 < 0.75D) {
            GlUtil.glColor4f(1.0F, 1.0F, 0.0F, 0.5F);
         } else {
            GlUtil.glColor4f(0.0F, 1.0F, 0.0F, 0.5F);
         }

         float var4 = (float)(var1 - 48) * this.element.getBootStatusPercent();
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 1);
         GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
         GlUtil.glDisable(3553);
         GlUtil.glEnable(2903);
         GL11.glBegin(7);
         GL11.glVertex2f(24.0F, 24.0F);
         GL11.glVertex2f(24.0F, (float)(var2 - 24));
         GL11.glVertex2f(var4 + 24.0F, (float)(var2 - 24));
         GL11.glVertex2f(var4 + 24.0F, 24.0F);
         GL11.glEnd();
         GlUtil.glDisable(3042);
      }

   }
}
