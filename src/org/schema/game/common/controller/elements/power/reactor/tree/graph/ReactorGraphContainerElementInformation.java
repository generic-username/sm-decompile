package org.schema.game.common.controller.elements.power.reactor.tree.graph;

import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraphConnection;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraphElementBackground;

public class ReactorGraphContainerElementInformation extends ReactorGraphContainer implements GUICallback {
   public static short selected;
   private static long selectTime;
   private ElementInformation element;
   private String desc;
   private boolean availableToBeSelected;

   public ReactorGraphContainerElementInformation(ReactorGraphGlobal var1, ElementInformation var2) {
      super(var1);
      this.element = var2;
      this.desc = var2.getChamberEffectInfo(((GameClientState)this.getGlobal().getState()).getConfigPool());
      if (var2.isChamberUpgraded()) {
         this.desc = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_GRAPH_REACTORGRAPHCONTAINERELEMENTINFORMATION_0 + this.desc;
      }

      String var3 = StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_GRAPH_REACTORGRAPHCONTAINERELEMENTINFORMATION_4, StringTools.formatPointZero(var2.chamberCapacity * 100.0F), StringTools.formatPointZero(var2.getChamberCapacityBranch() * 100.0F));
      String var4;
      if ((var4 = var2.getDescriptionIncludingChamberUpgraded()).equals(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_GRAPH_REACTORGRAPHCONTAINERELEMENTINFORMATION_1)) {
         var4 = "";
      }

      this.desc = var2.getName() + "\n" + (var4.length() > 0 ? var4 + "\n" : "") + this.desc + "\n" + var3;
      this.availableToBeSelected = !var1.tree.containsElementAnyChild(var2.getId()) && var1.onNode != null && var1.onNode.getPossibleSpecifications().contains(var2.getId());
      this.availableToBeSelected &= var2.isChamberPermitted(var1.getTree().pw.getSegmentController().getType());
      if (this.availableToBeSelected && selected == 0) {
         selected = var2.getId();
      }

   }

   public String getText() {
      return this.element.getName();
   }

   public ReactorGraphGlobal getGlobal() {
      return (ReactorGraphGlobal)super.getGlobal();
   }

   public GUICallback getSelectionCallback() {
      return this;
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse() && this.availableToBeSelected) {
         if (selected == this.element.getId() && System.currentTimeMillis() - selectTime < 300L) {
            if (this.getGlobal().onNode != null && ElementKeyMap.isValidType(selected)) {
               this.getGlobal().tree.pw.convertRequest(this.getGlobal().onNode.getId(), selected);
               if (this.getGlobal().ip != null) {
                  this.getGlobal().ip.deactivate();
                  return;
               }
            }
         } else {
            selected = this.element.getId();
            selectTime = System.currentTimeMillis();
         }
      }

   }

   public boolean isOccluded() {
      return !this.getGlobal().isActive();
   }

   public boolean isSelected() {
      return selected == this.element.getId();
   }

   public Vector4f getBackgroundColor() {
      return neutral;
   }

   public Vector4f getConnectionColorTo() {
      return neutral;
   }

   public void setConnectionColor(GUIGraphConnection var1) {
      Sprite var2 = Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 32px Conduit-4x1-gui-");
      byte var3;
      if (this.element.isChamberUpgraded()) {
         var3 = 3;
      } else {
         var3 = 0;
      }

      var1.setTextured(var2.getMaterial().getTexture(), var3, 4);
      var1.getLineColor().set(this.getConnectionColorTo());
   }

   public String getToolTipText() {
      if (this.getGlobal().tree.existsMutuallyExclusiveFor(this.element.id)) {
         return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_GRAPH_REACTORGRAPHCONTAINERELEMENTINFORMATION_3, ElementKeyMap.toString(this.element.chamberMutuallyExclusive), this.desc);
      } else {
         return !this.element.isChamberPermitted(this.getGlobal().tree.pw.getSegmentController().getType()) ? StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_GRAPH_REACTORGRAPHCONTAINERELEMENTINFORMATION_2, this.desc) : this.desc;
      }
   }

   public GUIGraphElementBackground initiateBackground() {
      return (GUIGraphElementBackground)(this.element.isChamberUpgraded() ? new ReactorGraphBackgroundSmall(this.getGlobal().getState()) : new ReactorGraphBackgroundBig(this.getGlobal().getState()));
   }

   public void setBackgroundColor(GUIGraphElementBackground var1) {
      ReactorGraphBackground var2 = (ReactorGraphBackground)var1;
      if (this.getGlobal().tree.existsMutuallyExclusiveFor(this.element.id)) {
         var2.setColorEnum(ReactorGraphBackground.ColorEnum.RED);
      } else if (!this.element.isChamberPermitted(this.getGlobal().tree.pw.getSegmentController().getType())) {
         var2.setColorEnum(ReactorGraphBackground.ColorEnum.RED);
      } else if (this.getGlobal().tree.containsElementAnyChild(this.element.getId())) {
         var2.setColorEnum(ReactorGraphBackground.ColorEnum.GREEN);
         if (this.element.isChamberUpgraded()) {
            var2.setColorEnum(ReactorGraphBackground.ColorEnum.CYAN);
         }

      } else if (this.getGlobal().onNode != null && this.getGlobal().onNode.getPossibleSpecifications().contains(this.element.getId())) {
         var2.setColorEnum(ReactorGraphBackground.ColorEnum.BLUE);
         if (this.element.isChamberUpgraded()) {
            var2.setColorEnum(ReactorGraphBackground.ColorEnum.MAGENTA);
         }

      } else {
         var2.setColorEnum(ReactorGraphBackground.ColorEnum.GREY);
      }
   }
}
