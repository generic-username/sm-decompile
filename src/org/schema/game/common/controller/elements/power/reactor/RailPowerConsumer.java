package org.schema.game.common.controller.elements.power.reactor;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.schine.graphicsengine.core.Timer;

public class RailPowerConsumer implements PowerConsumer {
   private final ManagerContainer man;
   private final SegmentController segmentController;
   private float dockedPowered;

   public RailPowerConsumer(ManagerContainer var1) {
      this.man = var1;
      this.segmentController = var1.getSegmentController();
   }

   public boolean isDocked() {
      return this.segmentController.railController.isDocked();
   }

   public double getPowerConsumedPerSecondResting() {
      return 0.0D;
   }

   public double getPowerConsumedPerSecondCharging() {
      return 0.0D;
   }

   public boolean isPowerCharging(long var1) {
      return false;
   }

   public void setPowered(float var1) {
      this.dockedPowered = var1;
   }

   public float getPowered() {
      return this.dockedPowered;
   }

   public boolean isPowerConsumerActive() {
      return !this.man.getSegmentController().railController.next.isEmpty();
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.DOCKS;
   }

   public void reloadFromReactor(double var1, Timer var3, float var4, boolean var5, float var6) {
   }

   public String getName() {
      return "RailConsumer";
   }

   public void dischargeFully() {
   }
}
