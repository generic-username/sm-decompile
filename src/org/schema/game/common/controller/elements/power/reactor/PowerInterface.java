package org.schema.game.common.controller.elements.power.reactor;

import it.unimi.dsi.fastutil.longs.LongArrayFIFOQueue;
import java.util.Collection;
import java.util.List;
import java.util.Observer;
import java.util.Set;
import javax.vecmath.Matrix3f;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ManagerUpdatableInterface;
import org.schema.game.common.controller.elements.power.reactor.chamber.ConduitCollectionManager;
import org.schema.game.common.controller.elements.power.reactor.chamber.ReactorChamberUnit;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorElement;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorSet;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorTree;
import org.schema.game.common.data.blockeffects.config.ConfigPool;
import org.schema.game.common.data.blockeffects.config.ConfigProviderSource;
import org.schema.game.network.objects.PowerInterfaceNetworkObject;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.resource.tag.Tag;

public interface PowerInterface extends ManagerUpdatableInterface, ConfigProviderSource {
   double getPower();

   double getMaxPower();

   void flagStabilizersDirty();

   List getMainReactors();

   List getChambers();

   ReactorChamberUnit getReactorChamber(long var1);

   ReactorElement getChamber(long var1);

   MainReactorUnit getReactor(long var1);

   ConduitCollectionManager getConduits();

   Set getConnectedChambersToConduit(long var1);

   void createPowerTree();

   ReactorSet getReactorSet();

   void onFinishedStabilizerChange();

   void calcBiggestAndActiveReactor();

   double getReactorOptimalDistance();

   StabilizerCollectionManager getStabilizerCollectionManager();

   double getPowerAsPercent();

   double calcStabilization(double var1, float var3);

   void update(Timer var1);

   void sendBonusMatrixUpdate(ReactorTree var1, Matrix3f var2);

   boolean isUsingPowerReactors();

   void addConsumer(PowerConsumer var1);

   void removeConsumer(PowerConsumer var1);

   double getCurrentConsumption();

   double getCurrentLocalConsumption();

   double getCurrentLocalConsumptionPerSec();

   double getRechargeRatePercentPerSec();

   double getRechargeRatePowerPerSec();

   double getCurrentPowerGain();

   double getCurrentConsumptionPerSec();

   void convertRequest(long var1, short var3);

   LongArrayFIFOQueue getChamberConvertRequests();

   LongArrayFIFOQueue getTreeBootRequests();

   void deleteObserver(Observer var1);

   void addObserver(Observer var1);

   boolean isInAnyTree(ReactorChamberUnit var1);

   void boot(ReactorTree var1);

   boolean isActiveReactor(ReactorTree var1);

   float getReactorSwitchCooldown();

   double getActiveReactorIntegrity();

   float getReactorRebootCooldown();

   void fromTagStructure(Tag var1);

   Tag toTagStructure();

   void updateFromNetworkObject(NetworkObject var1);

   void updateToFullNetworkObject(NetworkObject var1);

   void updateToNetworkObject(NetworkObject var1);

   void initFromNetworkObject(NetworkObject var1);

   void onBlockKilledServer(Damager var1, short var2, long var3);

   float getRebootTimeSec();

   boolean isAnyDamaged();

   void requestRecalibrate();

   void onAnyReactorModulesChanged();

   List getPowerConsumerList();

   ReactorPriorityQueue getPowerConsumerPriorityQueue();

   boolean isOnServer();

   PowerInterfaceNetworkObject getNetworkObject();

   void flagConsumersChanged();

   void consumePower(Timer var1, PowerImplementation var2);

   float getReactorToChamberSizeRelation();

   boolean isChamberValid(int var1, int var2);

   int getNeededMinForReactorLevel(int var1);

   int getNeededMaxForReactorLevel(int var1);

   ConfigPool getConfigPool();

   void reactorTreeReceived(ReactorTree var1);

   void checkRemovedChamber(List var1);

   double getStabilizerEfficiencyTotal();

   long getCurrentHp();

   long getCurrentMaxHp();

   SegmentController getSegmentController();

   double getPowerConsumptionAsPercent();

   boolean hasActiveReactors();

   ReactorTree getActiveReactor();

   float getChamberCapacity();

   long getActiveReactorId();

   boolean hasAnyReactors();

   ManagerContainer getManagerContainer();

   void switchActiveReacorToMostHp(ReactorTree var1);

   void setReactorBoost(float var1);

   float getReactorBoost();

   boolean isInstable();

   void registerProjectionConfigurationSource(ConfigProviderSource var1);

   void unregisterProjectionConfigurationSource(ConfigProviderSource var1);

   void addSectorConfigProjection(Collection var1);

   boolean isActiveReactor(long var1);

   boolean isAnyRebooting();

   double getStabilizerIntegrity();

   List getStabilizerPaths();

   void onPhysicsAdd();

   void onPhysicsRemove();

   void drawDebugEnergyStream();

   float getStabilzerPathRadius();

   void flagStabilizerPathCalc();

   void onLastReactorRemoved();

   void onBlockDamageServer(Damager var1, int var2, short var3, long var4);

   void onShieldDamageServer(double var1);

   void consumePowerTick(float var1, Timer var2, PowerImplementation var3, float var4);

   void destroyStabilizersBasedOnReactorSize(Damager var1);

   double getStabilizerEfficiencyExtra();

   float getExtraDamageTakenFromStabilization();

   void doEnergyStreamCooldownOnHit(Damager var1, float var2, long var3);

   float getCurrentEnergyStreamDamageCooldown();

   void injectPower(Damager var1, double var2);

   void setEnergyStreamEnabled(boolean var1);
}
