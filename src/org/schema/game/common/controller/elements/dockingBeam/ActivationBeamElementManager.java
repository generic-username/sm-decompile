package org.schema.game.common.controller.elements.dockingBeam;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.weapon.WeaponRowElementInterface;
import org.schema.game.client.view.gui.weapon.WeaponSegmentControllerUsableElement;
import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerActivityInterface;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ManagerReloadInterface;
import org.schema.game.common.controller.elements.ManagerUpdatableInterface;
import org.schema.game.common.controller.elements.SegmentControllerUsable;
import org.schema.game.common.controller.elements.UsableElementManager;
import org.schema.game.common.controller.elements.beam.BeamCommand;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.beam.AbstractBeamHandler;
import org.schema.game.common.data.element.beam.BeamReloadCallback;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.input.InputType;

public class ActivationBeamElementManager extends UsableElementManager implements BeamHandlerContainer, ManagerUpdatableInterface, BeamReloadCallback {
   @ConfigurationElement(
      name = "Distance"
   )
   public static float DOCKING_BEAM_DISTANCE = 100.0F;
   private Vector3f shootingDirTemp = new Vector3f();
   private ActivationBeamHandler activationBeamManager;
   private boolean addedUsable;
   private final Vector3i controlledFromTmp = new Vector3i();

   public ActivationBeamElementManager(SegmentController var1) {
      super(var1);
      this.activationBeamManager = new ActivationBeamHandler(var1, this);
   }

   public ActivationBeamHandler getActivationBeamManager() {
      return this.activationBeamManager;
   }

   public void update(Timer var1) {
      if (!this.addedUsable) {
         this.getManagerContainer().addPlayerUsable(new ActivationBeamElementManager.ActivationBeamDockerUsable(this.getManagerContainer()));
         this.addedUsable = true;
      }

      this.activationBeamManager.update(var1);
   }

   public ControllerManagerGUI getGUIUnitValues(ElementCollection var1, ElementCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return null;
   }

   protected String getTag() {
      return "dockingbeam";
   }

   public ElementCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      throw new IllegalAccessError("This should not be called. ever");
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOCKINGBEAM_ACTIVATIONBEAMELEMENTMANAGER_0;
   }

   protected void playSound(ElementCollection var1, Transform var2) {
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
      if (var1.isFlightControllerActive() && var1.isUnitInPlayerSector()) {
         if (var1.isMouseButtonDown(0) || var1.isMouseButtonDown(1)) {
            Vector3i var3 = var1.getControlledFrom(this.controlledFromTmp);
            if (this.getSegmentBuffer().getPointUnsave(var3) != null) {
               if (this.getSegmentController().railController.isDocked()) {
                  if (this.getSegmentController().isClientOwnObject() && var2.currentTime - this.getSegmentController().railController.isDockedSince() > 1000L && var2.currentTime - this.getSegmentController().railController.getLastDisconnect() > 1000L && this.getSegmentController().railController.isShipyardDocked()) {
                     if (!this.getSegmentController().isVirtualBlueprint()) {
                        this.getSegmentController().railController.disconnectClient();
                        return;
                     }

                     ((GameClientController)this.getState().getController()).popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOCKINGBEAM_ACTIVATIONBEAMELEMENTMANAGER_1, 0.0F);
                  }

               } else {
                  var1.getForward(this.shootingDirTemp);
                  this.shootingDirTemp.scale(DOCKING_BEAM_DISTANCE);
                  Vector3f var4 = new Vector3f();
                  this.getSegmentController().getAbsoluteElementWorldPosition(new Vector3i(var3.x - 16, var3.y - 16, var3.z - 16), var4);
                  Vector3f var5;
                  (var5 = new Vector3f()).set(var4);
                  var5.add(this.shootingDirTemp);
                  Vector3f var6 = new Vector3f((float)(var3.x - 16), (float)(var3.y - 16), (float)(var3.z - 16));
                  BeamCommand var7;
                  (var7 = new BeamCommand()).currentTime = var2.currentTime;
                  var7.identifier = ElementCollection.getIndex(var3);
                  var7.relativePos.set(var6);
                  var7.reloadCallback = this;
                  var7.from.set(var4);
                  var7.to.set(var5);
                  var7.playerState = var1.getPlayerState();
                  var7.beamTimeout = 0.1F;
                  var7.tickRate = 0.1F;
                  var7.beamPower = 0.0F;
                  var7.controllerPos = var3;
                  var7.cooldownSec = 0.0F;
                  var7.bursttime = -1.0F;
                  var7.initialTicks = 1.0F;
                  var7.powerConsumedByTick = 0.0F;
                  var7.powerConsumedExtraByTick = 0.0F;
                  var7.weaponId = -9223372036854775804L;
                  var7.latchOn = false;
                  var7.checkLatchConnection = false;
                  var7.firendlyFire = true;
                  var7.penetrating = false;
                  var7.acidDamagePercent = 0.0F;
                  var7.minEffectiveRange = 1.0F;
                  var7.minEffectiveValue = 1.0F;
                  var7.maxEffectiveRange = 1.0F;
                  var7.maxEffectiveValue = 1.0F;
                  this.activationBeamManager.addBeam(var7);
                  this.getManagerContainer().onAction();
               }
            }
         }
      }
   }

   public void setShotReloading(long var1) {
   }

   public boolean canUse(long var1, boolean var3) {
      return true;
   }

   public boolean isInitializing(long var1) {
      return false;
   }

   public long getNextShoot() {
      return 0L;
   }

   public long getCurrentReloadTime() {
      return 0L;
   }

   public boolean canUpdate() {
      return true;
   }

   public void onNoUpdate(Timer var1) {
   }

   public void onElementCollectionsChanged() {
   }

   public void sendHitConfirm(byte var1) {
   }

   public boolean isSegmentController() {
      return true;
   }

   public SimpleTransformableSendableObject getShootingEntity() {
      return this.getSegmentController();
   }

   public int getFactionId() {
      return this.getSegmentController().getFactionId();
   }

   public String getName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOCKINGBEAM_ACTIVATIONBEAMELEMENTMANAGER_3;
   }

   public AbstractOwnerState getOwnerState() {
      return this.getSegmentController().getOwnerState();
   }

   public void sendClientMessage(String var1, int var2) {
      this.getSegmentController().sendClientMessage(var1, var2);
   }

   public void sendServerMessage(Object[] var1, int var2) {
      this.getSegmentController().sendServerMessage(var1, var2);
   }

   public float getDamageGivenMultiplier() {
      return 1.0F;
   }

   public InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3) {
      return null;
   }

   public MetaWeaponEffectInterface getMetaWeaponEffect(long var1, DamageDealerType var3) {
      return null;
   }

   public int getSectorId() {
      return this.getSegmentController().getSectorId();
   }

   public AbstractBeamHandler getHandler() {
      return this.activationBeamManager;
   }

   public void flagCheckUpdatable() {
      this.setUpdatable(false);
   }

   public void flagBeamFiredWithoutTimeout() {
   }

   public class ActivationBeamDockerUsable extends SegmentControllerUsable {
      public ActivationBeamDockerUsable(ManagerContainer var2) {
         super(var2);
      }

      public int hashCode() {
         return (31 + this.getOuterType().hashCode()) * 31 + -2147483644;
      }

      public boolean equals(Object var1) {
         return var1 instanceof ActivationBeamElementManager.ActivationBeamDockerUsable;
      }

      private ActivationBeamElementManager getOuterType() {
         return ActivationBeamElementManager.this;
      }

      public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
         var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOCKINGBEAM_ACTIVATIONBEAMELEMENTMANAGER_4, var3, ContextFilter.IMPORTANT);
      }

      public WeaponRowElementInterface getWeaponRow() {
         return new WeaponSegmentControllerUsableElement(this);
      }

      public boolean isAddToPlayerUsable() {
         return true;
      }

      public boolean isControllerConnectedTo(long var1, short var3) {
         return var3 == 1;
      }

      public void onPlayerDetachedFromThisOrADock(ManagedUsableSegmentController var1, PlayerState var2, PlayerControllable var3) {
      }

      public boolean isPlayerUsable() {
         return true;
      }

      public long getUsableId() {
         return -9223372036854775804L;
      }

      public void handleControl(ControllerStateInterface var1, Timer var2) {
         ActivationBeamElementManager.this.handle(var1, var2);
      }

      public ManagerReloadInterface getReloadInterface() {
         return null;
      }

      public ManagerActivityInterface getActivityInterface() {
         return null;
      }

      public String getWeaponRowName() {
         return this.getName();
      }

      public short getWeaponRowIcon() {
         return 405;
      }

      public String getName() {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOCKINGBEAM_ACTIVATIONBEAMELEMENTMANAGER_2;
      }
   }
}
