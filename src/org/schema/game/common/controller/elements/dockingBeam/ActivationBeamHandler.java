package org.schema.game.common.controller.elements.dockingBeam;

import java.util.Collection;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.view.beam.BeamColors;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.FloatingRockManaged;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.beam.BeamHandler;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseButton;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;

public class ActivationBeamHandler extends BeamHandler implements BeamHandlerContainer {
   private static final float TIME_BEAM_TO_HIT_ONE_PIECE_IN_SECS = 0.2F;
   private final ActivationBeamElementManager dockingBeamElementManager;
   private long lastActivate;

   public ActivationBeamHandler(SegmentController var1, ActivationBeamElementManager var2) {
      super(var1, (BeamHandlerContainer)null);
      this.dockingBeamElementManager = var2;
   }

   public boolean canhit(BeamState var1, SegmentController var2, String[] var3, Vector3i var4) {
      boolean var5;
      if (!(var5 = !var2.equals(this.getBeamShooter()) && (var2 instanceof Ship || var2 instanceof SpaceStation || var2 instanceof Planet || var2 instanceof FloatingRockManaged || var2 instanceof ShopSpaceStation))) {
         var3[0] = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOCKINGBEAM_ACTIVATIONBEAMHANDLER_0;
      }

      return var5;
   }

   public float getBeamTimeoutInSecs() {
      return 0.05F;
   }

   public boolean affectsTargetBlock() {
      return true;
   }

   public float getBeamToHitInSecs(BeamState var1) {
      return 0.2F;
   }

   public int onBeamHit(BeamState var1, int var2, BeamHandlerContainer var3, SegmentPiece var4, Vector3f var5, Vector3f var6, Timer var7, Collection var8) {
      var4.refresh();
      if (var4.getType() == 0) {
         return 0;
      } else {
         ElementInformation var9 = ElementKeyMap.getInfo(var4.getType());
         var4.getSegmentController();
         if (!((SegmentController)this.getBeamShooter()).isOnServer() && var4.getType() == 679) {
            SegmentPiece var11;
            if ((var11 = ((SegmentController)this.getBeamShooter()).getSegmentBuffer().getPointUnsave(Ship.core)) != null & var11.getType() == 1 && !var4.getSegmentController().isVirtualBlueprint()) {
               ((SegmentController)this.getBeamShooter()).railController.connectClient(var11, var4);
            }
         } else if (var9.getBlockStyle() != BlockStyle.NORMAL24) {
            boolean var10 = !var4.isActive();
            if (!((GameStateInterface)this.getState()).getGameState().isAllowOldDockingBeam()) {
               var10 = var1.mouseButton[MouseButton.LEFT.button];
            }

            if ((!((GameStateInterface)this.getState()).getGameState().isAllowOldDockingBeam() || !var1.mouseButton[MouseButton.RIGHT.button]) && var9.canActivate()) {
               if (!((SegmentController)this.getBeamShooter()).isOnServer() && ((GameClientState)((SegmentController)this.getBeamShooter()).getState()).getController().allowedToActivate(var4)) {
                  ((GameClientState)((SegmentController)this.getBeamShooter()).getState()).getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOCKINGBEAM_ACTIVATIONBEAMHANDLER_9, 0.0F);
                  if (var10 != var4.isActive() && System.currentTimeMillis() - this.lastActivate > (long)(((GameStateInterface)this.getState()).getGameState().isAllowOldDockingBeam() ? 1000 : 10) && (var4.getType() == 55 || var4.getType() == 283 || var4.getType() == 284 || var4.getType() == 282 || var4.getType() == 285 || ElementKeyMap.isBeacon(var4.getType()) || ElementKeyMap.isValidType(var4.getType()) && ElementKeyMap.getInfo(var4.getType()).isDoor() || ElementKeyMap.isValidType(var4.getType()) && ElementKeyMap.getInfo(var4.getType()).isSignal())) {
                     if (var4.getSegment().getSegmentController().mayActivateOnThis((SegmentController)this.getBeamShooter(), var4)) {
                        var4.getSegment().getSegmentController().sendBlockActivation(ElementCollection.getEncodeActivation(var4, true, var10, false));
                        this.lastActivate = System.currentTimeMillis();
                     } else {
                        ((GameClientState)((SegmentController)this.getBeamShooter()).getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOCKINGBEAM_ACTIVATIONBEAMHANDLER_10, 0.0F);
                     }
                  }
               }
            } else if (((SegmentController)this.getBeamShooter()).isClientOwnObject()) {
               ((GameClientState)((SegmentController)this.getBeamShooter()).getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOCKINGBEAM_ACTIVATIONBEAMHANDLER_1, 0.0F);
            }
         }

         return 1;
      }
   }

   protected boolean onBeamHitNonCube(BeamState var1, int var2, BeamHandlerContainer var3, Vector3f var4, Vector3f var5, CubeRayCastResult var6, Timer var7, Collection var8) {
      return false;
   }

   protected boolean ignoreNonPhysical(BeamState var1) {
      return var1.mouseButton[MouseButton.LEFT.button];
   }

   public Vector4f getDefaultColor(BeamState var1) {
      return getColorRange(BeamColors.WHITE);
   }

   public ActivationBeamElementManager getDockingBeamElementManager() {
      return this.dockingBeamElementManager;
   }

   public ActivationBeamHandler getHandler() {
      return this;
   }

   public StateInterface getState() {
      return this.dockingBeamElementManager.getSegmentController().getState();
   }

   public void sendHitConfirm(byte var1) {
   }

   public boolean isSegmentController() {
      return true;
   }

   public int getFactionId() {
      return this.dockingBeamElementManager.getSegmentController().getFactionId();
   }

   public String getName() {
      return "Docking Beam";
   }

   public boolean ignoreBlock(short var1) {
      ElementInformation var2;
      return (var2 = ElementKeyMap.getInfoFast(var1)).isDrawnOnlyInBuildMode() && !var2.hasLod();
   }

   public AbstractOwnerState getOwnerState() {
      return this.dockingBeamElementManager.getSegmentController().getOwnerState();
   }
}
