package org.schema.game.common.controller.elements;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import org.schema.game.common.data.element.ElementKeyMap;

public class ModuleMap {
   private final ManagerModule[] values;
   private ManagerModule all;
   private ManagerModule signal;
   private ManagerModule railTrack;
   private ManagerModule railInv;

   public ModuleMap() {
      this.values = new ManagerModule[ElementKeyMap.highestType + 1];
   }

   public void put(short var1, ManagerModule var2) {
      if (var1 == 32767) {
         this.all = var2;
      } else if (var1 == 30000) {
         this.signal = var2;
      } else if (var1 == 29999) {
         this.railTrack = var2;
      } else if (var1 == 29998) {
         this.railInv = var2;
      } else {
         this.values[var1] = var2;
      }
   }

   public ManagerModule get(short var1) {
      if (var1 == 32767) {
         return this.all;
      } else if (var1 == 30000) {
         return this.signal;
      } else if (var1 == 29999) {
         return this.railTrack;
      } else {
         return var1 == 29998 ? this.railInv : this.values[var1];
      }
   }

   public boolean containsKey(short var1) {
      if (var1 == 32767) {
         return this.all != null;
      } else if (var1 == 30000) {
         return this.signal != null;
      } else if (var1 == 29999) {
         return this.railTrack != null;
      } else if (var1 == 29998) {
         return this.railInv != null;
      } else {
         return this.values[var1] != null;
      }
   }

   public boolean checkIntegrity() {
      ManagerModule[] var1;
      int var2 = (var1 = this.values).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         ManagerModule var4;
         if ((var4 = var1[var3]) != null && !this.check(var4)) {
            return false;
         }
      }

      if (this.check(this.all) && this.check(this.signal) && this.check(this.railTrack)) {
         return true;
      } else {
         return false;
      }
   }

   private boolean check(ManagerModule var1) {
      ObjectOpenHashSet var2;
      (var2 = new ObjectOpenHashSet()).add(var1);

      while(var1.getNext() != null) {
         var1 = var1.getNext();
         if (var2.contains(var1)) {
            System.err.println("ERROR: Manager Module " + var1 + " has loop");

            try {
               throw new RuntimeException("Invalid Manager Module: " + var1 + "; " + ElementKeyMap.toString(var1.getElementID()) + "; " + var2);
            } catch (Exception var3) {
               var3.printStackTrace();
               return false;
            }
         }

         var2.add(var1);
      }

      return true;
   }

   public String toString() {
      return "ModuleMap [all=" + this.all + ", signal=" + this.signal + ", railTrack=" + this.railTrack + "]";
   }
}
