package org.schema.game.common.controller.elements;

import it.unimi.dsi.fastutil.longs.Long2LongOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;

public class BlockConnectionPath {
   private final Long2LongOpenHashMap backing = new Long2LongOpenHashMap();
   private boolean changed = true;
   private final LongArrayList startPoints = new LongArrayList();
   private final ObjectArrayList paths = new ObjectArrayList();
   private final ManagerContainer man;
   private final LongOpenHashSet blocks = new LongOpenHashSet();

   public BlockConnectionPath(ManagerContainer var1) {
      this.man = var1;
   }

   public void remove(long var1) {
      this.changed = true;
      this.backing.remove(var1);
      this.man.getPowerInterface().flagStabilizerPathCalc();
   }

   public void put(long var1, long var3) {
      this.changed = true;
      this.backing.put(var1, var3);
      this.man.getPowerInterface().flagStabilizerPathCalc();
   }

   public boolean containsKey(long var1) {
      return this.backing.containsKey(var1);
   }

   public long get(long var1) {
      return this.backing.get(var1);
   }

   public void recalc() {
      if (this.changed) {
         this.startPoints.clear();
         this.paths.clear();
         LongSet var1 = this.backing.keySet();
         LongOpenHashSet var2 = new LongOpenHashSet(this.blocks);
         LongOpenHashSet var3 = new LongOpenHashSet(this.backing.values());
         Iterator var9 = var1.iterator();

         while(var9.hasNext()) {
            long var4 = (Long)var9.next();
            var2.remove(var4);
            var2.remove(this.backing.get(var4));
            if (!var3.contains(var4)) {
               this.startPoints.add(var4);
            }
         }

         this.startPoints.addAll(var2);

         for(int var10 = 0; var10 < this.startPoints.size(); ++var10) {
            var3.clear();
            LongArrayList var11 = new LongArrayList();
            long var5 = this.startPoints.getLong(var10);
            var11.add(var5);

            while(this.backing.containsKey(var11.getLong(var11.size() - 1))) {
               long var7 = this.backing.get(var11.getLong(var11.size() - 1));
               if (var3.contains(var7)) {
                  break;
               }

               var3.add(var7);
               var11.add(var7);
            }

            this.paths.add(var11);
         }

         this.changed = false;
      }

   }

   public ObjectArrayList getPaths() {
      this.recalc();
      return this.paths;
   }

   public void removeBlock(long var1) {
      this.changed = true;
      this.blocks.remove(var1);
      this.man.getPowerInterface().flagStabilizerPathCalc();
   }

   public void addBlock(long var1) {
      this.changed = true;
      this.blocks.add(var1);
      this.man.getPowerInterface().flagStabilizerPathCalc();
   }
}
