package org.schema.game.common.controller.elements.railbeam;

import java.util.Collection;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.beam.BeamColors;
import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.controller.rails.DockingFailReason;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.beam.BeamHandler;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseButton;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;

public class RailBeamHandler extends BeamHandler implements BeamHandlerContainer {
   private static final float TIME_BEAM_TO_HIT_ONE_PIECE_IN_SECS = 0.2F;
   private final RailBeamElementManager railBeamElementManager;

   public RailBeamHandler(SegmentController var1, RailBeamElementManager var2) {
      super(var1, (BeamHandlerContainer)null);
      this.railBeamElementManager = var2;
   }

   public boolean canhit(BeamState var1, SegmentController var2, String[] var3, Vector3i var4) {
      boolean var5;
      if (!(var5 = !var2.equals(this.getBeamShooter()) && (var2 instanceof Ship || var2 instanceof SpaceStation || var2 instanceof Planet || var2 instanceof ShopSpaceStation))) {
         var3[0] = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAILBEAM_RAILBEAMHANDLER_0;
      }

      return var5;
   }

   public float getBeamTimeoutInSecs() {
      return 0.05F;
   }

   public float getBeamToHitInSecs(BeamState var1) {
      return 0.2F;
   }

   public int onBeamHit(BeamState var1, int var2, BeamHandlerContainer var3, SegmentPiece var4, Vector3f var5, Vector3f var6, Timer var7, Collection var8) {
      final SegmentPiece var10;
      (var10 = new SegmentPiece(var4)).refresh();
      if (var10.getType() == 0) {
         return 0;
      } else {
         ElementInformation var11 = ElementKeyMap.getInfo(var10.getType());
         var10.getSegmentController();
         final SegmentPiece var9;
         if (((SegmentController)this.getBeamShooter()).isClientOwnObject() && (var9 = ((SegmentController)this.getBeamShooter()).getSegmentBuffer().getPointUnsave(var1.identifyerSig)) != null && var9.getType() == 663 && (var11.isRailTrack() || var11.isRailTurret() || var11.isRailShipyardCore())) {
            System.err.println("[CLIENT][RAIL][BEAM] HITTING RAIL " + var9 + " -> " + var10);
            if (!var10.getSegmentController().isVirtualBlueprint()) {
               if (!var9.getSegmentController().railController.isInAnyRailRelationWith(var10.getSegmentController())) {
                  DockingFailReason var12 = new DockingFailReason();
                  if (((SegmentController)this.getBeamShooter()).railController.isOkToDockClientCheck(var9, var10, var12)) {
                     ((SegmentController)this.getBeamShooter()).railController.connectClient(var9, var10);
                  } else {
                     var12.popupClient((SegmentController)this.getBeamShooter());
                  }
               }
            } else {
               (new PlayerGameOkCancelInput("CONFIRM", (GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAILBEAM_RAILBEAMHANDLER_1, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAILBEAM_RAILBEAMHANDLER_2) {
                  public void onDeactivate() {
                  }

                  public boolean isOccluded() {
                     return false;
                  }

                  public void pressedOK() {
                     DockingFailReason var1 = new DockingFailReason();
                     if (((SegmentController)RailBeamHandler.this.getBeamShooter()).railController.isOkToDockClientCheck(var9, var10, var1)) {
                        ((SegmentController)RailBeamHandler.this.getBeamShooter()).railController.connectClient(var9, var10);
                     } else {
                        var1.popupClient((SegmentController)RailBeamHandler.this.getBeamShooter());
                     }

                     this.deactivate();
                  }
               }).activate();
            }
         }

         return 1;
      }
   }

   protected boolean onBeamHitNonCube(BeamState var1, int var2, BeamHandlerContainer var3, Vector3f var4, Vector3f var5, CubeRayCastResult var6, Timer var7, Collection var8) {
      return false;
   }

   protected boolean ignoreNonPhysical(BeamState var1) {
      return var1.mouseButton[MouseButton.LEFT.button];
   }

   public Vector4f getDefaultColor(BeamState var1) {
      return getColorRange(BeamColors.WHITE);
   }

   public RailBeamElementManager getDockingBeamElementManager() {
      return this.railBeamElementManager;
   }

   public RailBeamHandler getHandler() {
      return this;
   }

   public StateInterface getState() {
      return this.railBeamElementManager.getSegmentController().getState();
   }

   public void sendHitConfirm(byte var1) {
   }

   public boolean isSegmentController() {
      return true;
   }

   public int getFactionId() {
      return this.railBeamElementManager.getSegmentController().getFactionId();
   }

   public String getName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAILBEAM_RAILBEAMHANDLER_3;
   }

   public boolean ignoreBlock(short var1) {
      ElementInformation var2;
      return (var2 = ElementKeyMap.getInfoFast(var1)).isDrawnOnlyInBuildMode() && !var2.hasLod();
   }

   public AbstractOwnerState getOwnerState() {
      return this.railBeamElementManager.getSegmentController().getOwnerState();
   }
}
