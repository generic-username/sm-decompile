package org.schema.game.common.controller.elements.railbeam;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongSet;
import java.io.IOException;
import java.util.Locale;
import javax.vecmath.Vector3f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.weapon.WeaponRowElement;
import org.schema.game.client.view.gui.weapon.WeaponRowElementInterface;
import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.CannotImmediateRequestOnClientException;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.game.common.controller.SlotAssignment;
import org.schema.game.common.controller.elements.BeamHandlerDeligator;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerActivityInterface;
import org.schema.game.common.controller.elements.ManagerReloadInterface;
import org.schema.game.common.controller.elements.ManagerUpdatableInterface;
import org.schema.game.common.controller.elements.UsableElementManager;
import org.schema.game.common.controller.elements.beam.BeamCommand;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.beam.BeamReloadCallback;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.ShipConfigurationNotFoundException;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.input.InputType;
import org.schema.schine.input.KeyboardMappings;

public class RailBeamElementManager extends UsableElementManager implements BeamHandlerDeligator, ManagerUpdatableInterface, BeamReloadCallback {
   @ConfigurationElement(
      name = "Distance"
   )
   public static float DOCKING_BEAM_DISTANCE = 100.0F;
   private final Long2ObjectOpenHashMap railDockers = new Long2ObjectOpenHashMap();
   private final SegmentPiece tmpPiece = new SegmentPiece();
   private Vector3f shootingDirTemp = new Vector3f();
   private RailBeamHandler railBeamManager;
   private Vector3i controlledFromOrig = new Vector3i();
   private Vector3i controlledFrom = new Vector3i();

   public RailBeamElementManager(SegmentController var1) {
      super(var1);
      this.railBeamManager = new RailBeamHandler(var1, this);
   }

   public RailBeamHandler getRailBeamManager() {
      return this.railBeamManager;
   }

   protected boolean convertDeligateControls(ControllerStateInterface var1, Vector3i var2, Vector3i var3) throws IOException {
      var1.getParameter(var2);
      var1.getParameter(var3);
      if (var1.getPlayerState() == null) {
         return true;
      } else {
         SegmentPiece var6;
         if ((var6 = this.getSegmentBuffer().getPointUnsave(var3, new SegmentPiece())) == null) {
            return false;
         } else {
            if (var6.getType() == 1) {
               try {
                  SlotAssignment var7 = this.checkShipConfig(var1);
                  int var4 = var1.getPlayerState().getCurrentShipControllerSlot();
                  if (var1.getPlayerState() != null && !var7.hasConfigForSlot(var4)) {
                     return false;
                  }

                  var3.set(var7.get(var4));
               } catch (ShipConfigurationNotFoundException var5) {
                  return false;
               }
            }

            return true;
         }
      }
   }

   public void update(Timer var1) {
      this.railBeamManager.update(var1);
   }

   public ControllerManagerGUI getGUIUnitValues(ElementCollection var1, ElementCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return null;
   }

   protected String getTag() {
      return "railbeam";
   }

   public ElementCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      throw new IllegalAccessError("This should not be called. ever");
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAILBEAM_RAILBEAMELEMENTMANAGER_2;
   }

   protected void playSound(ElementCollection var1, Transform var2) {
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
      try {
         if (var1.isFlightControllerActive() && var1.isUnitInPlayerSector()) {
            try {
               if (!this.convertDeligateControls(var1, this.controlledFromOrig, this.controlledFrom)) {
                  return;
               }
            } catch (IOException var7) {
               var7.printStackTrace();
               return;
            }

            SegmentPiece var3;
            if ((var3 = this.getSegmentBuffer().getPointUnsave(this.controlledFrom, this.tmpPiece)) != null && var3.getType() == 663) {
               if (this.getSegmentController().railController.isDocked()) {
                  if (this.getSegmentController().isClientOwnObject() && var2.currentTime - this.getSegmentController().railController.isDockedSince() > 1000L && var2.currentTime - this.getSegmentController().railController.getLastDisconnect() > 1000L) {
                     if (!this.getSegmentController().isVirtualBlueprint()) {
                        String var10 = this.getSegmentController().railController.getOneBeforeRoot().lastDockerPlayerServerLowerCase;
                        if (this.getSegmentController().railController.getRoot() instanceof ShopSpaceStation && !this.getAttachedPlayers().isEmpty() && var10.length() != 0 && !((AbstractOwnerState)this.getAttachedPlayers().get(0)).getName().toLowerCase(Locale.ENGLISH).equals(var10)) {
                           ((GameClientController)this.getState().getController()).popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAILBEAM_RAILBEAMELEMENTMANAGER_1, this.getSegmentController().lastDockerPlayerServerLowerCase), 0.0F);
                           return;
                        }

                        System.err.println("[CLIENT][RAILBEAM] Disconnecting from tail");
                        this.getSegmentController().railController.disconnectClient();
                        return;
                     }

                     ((GameClientController)this.getState().getController()).popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAILBEAM_RAILBEAMELEMENTMANAGER_0, 0.0F);
                  }

               } else if (var2.currentTime - this.getSegmentController().railController.getLastDisconnect() >= 1000L) {
                  var1.getForward(this.shootingDirTemp);
                  this.shootingDirTemp.scale(DOCKING_BEAM_DISTANCE);
                  Vector3f var9 = new Vector3f();
                  this.getSegmentController().getAbsoluteElementWorldPosition(new Vector3i(this.controlledFrom.x - 16, this.controlledFrom.y - 16, this.controlledFrom.z - 16), var9);
                  Vector3f var4;
                  (var4 = new Vector3f()).set(var9);
                  var4.add(this.shootingDirTemp);
                  Vector3f var5 = new Vector3f((float)(this.controlledFrom.x - 16), (float)(this.controlledFrom.y - 16), (float)(this.controlledFrom.z - 16));
                  BeamCommand var6;
                  (var6 = new BeamCommand()).currentTime = var2.currentTime;
                  var6.identifier = ElementCollection.getIndex(this.controlledFrom);
                  var6.relativePos.set(var5);
                  var6.reloadCallback = this;
                  var6.from.set(var9);
                  var6.to.set(var4);
                  var6.playerState = var1.getPlayerState();
                  var6.beamTimeout = 0.1F;
                  var6.tickRate = 0.1F;
                  var6.beamPower = 0.0F;
                  var6.controllerPos = this.controlledFrom;
                  var6.cooldownSec = 0.0F;
                  var6.bursttime = -1.0F;
                  var6.initialTicks = 1.0F;
                  var6.powerConsumedByTick = 0.0F;
                  var6.powerConsumedExtraByTick = 0.0F;
                  var6.weaponId = var6.identifier;
                  var6.latchOn = false;
                  var6.checkLatchConnection = false;
                  var6.firendlyFire = true;
                  var6.penetrating = false;
                  var6.acidDamagePercent = 0.0F;
                  var6.minEffectiveRange = 1.0F;
                  var6.minEffectiveValue = 1.0F;
                  var6.maxEffectiveRange = 1.0F;
                  var6.maxEffectiveValue = 1.0F;
                  this.railBeamManager.addBeam(var6);
                  this.getManagerContainer().onAction();
               }
            }
         }
      } catch (CannotImmediateRequestOnClientException var8) {
         System.err.println("[CLIENT][WARNING] " + this.getSegmentController() + " Cannot DOCK! segment not yet in buffer " + var8.getSegIndex() + ". -> requested");
      }
   }

   public void setShotReloading(long var1) {
   }

   public boolean canUse(long var1, boolean var3) {
      return true;
   }

   public boolean isInitializing(long var1) {
      return false;
   }

   public long getNextShoot() {
      return 0L;
   }

   public long getCurrentReloadTime() {
      return 0L;
   }

   public LongSet getRailDockers() {
      return this.railDockers.keySet();
   }

   public void addRailDocker(long var1) {
      long var3 = ElementCollection.getPosIndexFrom4(var1);
      RailBeamElementManager.RailDockerUsableUndocked var5 = new RailBeamElementManager.RailDockerUsableUndocked(var3);
      this.railDockers.put(var3, var5);
      this.getManagerContainer().addPlayerUsable(var5);
   }

   public void removeRailDockers(long var1) {
      long var3 = ElementCollection.getPosIndexFrom4(var1);
      RailBeamElementManager.RailDockerUsable var5;
      if ((var5 = (RailBeamElementManager.RailDockerUsable)this.railDockers.remove(var3)) != null) {
         this.getManagerContainer().removePlayerUsable(var5);
      }

   }

   public boolean canUpdate() {
      return true;
   }

   public void onNoUpdate(Timer var1) {
   }

   public void onElementCollectionsChanged() {
   }

   public void flagCheckUpdatable() {
      this.setUpdatable(false);
   }

   public BeamHandlerContainer getBeamHandler() {
      return this.railBeamManager;
   }

   public void flagBeamFiredWithoutTimeout() {
   }

   public class RailDockerUsable implements PlayerUsableInterface {
      public final long index;

      public RailDockerUsable(long var2) {
         this.index = var2;
      }

      public int hashCode() {
         return (31 + this.getOuterType().hashCode()) * 31 + (int)(this.index ^ this.index >>> 32);
      }

      public boolean equals(Object var1) {
         return var1 instanceof RailBeamElementManager.RailDockerUsable && this.index == ((RailBeamElementManager.RailDockerUsable)var1).index;
      }

      public void handleMouseEvent(ControllerStateUnit var1, MouseEvent var2) {
      }

      private RailBeamElementManager getOuterType() {
         return RailBeamElementManager.this;
      }

      public void onPlayerDetachedFromThisOrADock(ManagedUsableSegmentController var1, PlayerState var2, PlayerControllable var3) {
      }

      public void onSwitched(boolean var1) {
      }

      public void onLogicActivate(SegmentPiece var1, boolean var2, Timer var3) {
      }

      public boolean isAddToPlayerUsable() {
         return true;
      }

      public WeaponRowElementInterface getWeaponRow() {
         SegmentPiece var1;
         return (var1 = RailBeamElementManager.this.getSegmentController().getSegmentBuffer().getPointUnsave(this.index)) != null ? new WeaponRowElement(var1) : null;
      }

      public boolean isControllerConnectedTo(long var1, short var3) {
         return var3 == 1;
      }

      public float getWeaponSpeed() {
         return 0.0F;
      }

      public float getWeaponDistance() {
         return 0.0F;
      }

      public boolean isPlayerUsable() {
         return !RailBeamElementManager.this.getSegmentController().railController.isDockedAndExecuted();
      }

      public long getUsableId() {
         return this.index;
      }

      public void handleControl(ControllerStateInterface var1, Timer var2) {
         if (var1.isPrimaryShootButtonDown() && RailBeamElementManager.this.canUse(var2.currentTime, true)) {
            RailBeamElementManager.this.handle(var1, var2);
         }

      }

      public ManagerReloadInterface getReloadInterface() {
         return null;
      }

      public ManagerActivityInterface getActivityInterface() {
         return null;
      }

      public String getName() {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAILBEAM_RAILBEAMELEMENTMANAGER_3;
      }

      public void handleKeyEvent(ControllerStateUnit var1, KeyboardMappings var2) {
      }

      public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
         var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAILBEAM_RAILBEAMELEMENTMANAGER_4, var3, ContextFilter.IMPORTANT);
      }
   }

   public class RailDockerUsableUndocked extends RailBeamElementManager.RailDockerUsable {
      public RailDockerUsableUndocked(long var2) {
         super(var2);
      }
   }
}
