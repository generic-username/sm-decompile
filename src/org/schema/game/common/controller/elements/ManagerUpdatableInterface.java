package org.schema.game.common.controller.elements;

import org.schema.schine.graphicsengine.core.Timer;

public interface ManagerUpdatableInterface {
   void update(Timer var1);

   int updatePrio();

   boolean canUpdate();

   void onNoUpdate(Timer var1);
}
