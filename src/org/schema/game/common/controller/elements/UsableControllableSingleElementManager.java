package org.schema.game.common.controller.elements;

import java.io.IOException;
import java.util.Random;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SlotAssignment;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ShipConfigurationNotFoundException;

public abstract class UsableControllableSingleElementManager extends UsableElementManager implements TargetableSystemInterface {
   private ElementCollectionManager collection;
   private final Class clazz;

   public UsableControllableSingleElementManager(SegmentController var1, Class var2) {
      super(var1);
      this.clazz = var2;
   }

   public void onElementCollectionsChanged() {
      this.lowestIntegrity = this.collection.getLowestIntegrity();
   }

   public void init(ManagerContainer var1) {
      this.collection = this.getNewCollectionManager((SegmentPiece)null, this.clazz);

      assert this.collection != null;

      if (this.collection instanceof PowerConsumer) {
         var1.addConsumer((PowerConsumer)this.collection);
      }

      if (this.collection instanceof PlayerUsableInterface) {
         var1.addPlayerUsable((PlayerUsableInterface)this.collection);
      }

   }

   public void onKilledBlock(long var1, short var3, Damager var4) {
      if (this.getSegmentController().isOnServer()) {
         assert this instanceof BlockKillInterface;

         if (this.lowestIntegrity < VoidElementManager.INTEGRITY_MARGIN) {
            this.collection.checkIntegrity(var1, var3, var4);
         }
      }

   }

   public int getPriority() {
      return 1;
   }

   public ElementCollection getRandomCollection(Random var1) {
      return this.getCollection().getElementCollections().size() > 0 ? (ElementCollection)this.getCollection().getElementCollections().get(var1.nextInt(this.getCollection().getElementCollections().size())) : null;
   }

   protected boolean convertDeligateControls(ControllerStateInterface var1, Vector3i var2, Vector3i var3) throws IOException {
      if (var1.getPlayerState() == null) {
         return true;
      } else {
         var1.getParameter(var2);
         var1.getParameter(var3);
         SegmentPiece var6;
         if ((var6 = this.getSegmentBuffer().getPointUnsave(var3, new SegmentPiece())) == null) {
            return false;
         } else {
            if (var6 != null && var6.getType() == 1) {
               try {
                  SlotAssignment var7 = this.checkShipConfig(var1);
                  int var5 = var1.getCurrentShipControllerSlot();
                  if (!var7.hasConfigForSlot(var5)) {
                     return false;
                  }

                  var3.set(var7.get(var5));
               } catch (ShipConfigurationNotFoundException var4) {
                  return false;
               }
            }

            return true;
         }
      }
   }

   public final ElementCollectionManager getCollection() {
      return this.collection;
   }

   public String getManagerName() {
      return this.getCollection().getModuleName();
   }

   public abstract void onControllerChange();

   public void flagCheckUpdatable() {
      this.setUpdatable(this.getCollection().isStructureUpdateNeeded());
   }
}
