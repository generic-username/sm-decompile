package org.schema.game.common.controller.elements;

public interface SupportElementManagerInterface {
   double calculateSupportIndex();

   double calculateSupportPowerConsumptionPerSecondIndex();
}
