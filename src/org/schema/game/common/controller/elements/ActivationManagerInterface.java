package org.schema.game.common.controller.elements;

public interface ActivationManagerInterface {
   ManagerModuleCollection getActivation();
}
