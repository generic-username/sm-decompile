package org.schema.game.common.controller.elements.activationgate;

import com.bulletphysics.collision.broadphase.BroadphasePair;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.effects.RaisingIndication;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.power.PowerAddOn;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.PairCachingGhostObjectAlignable;
import org.schema.game.common.data.physics.RigidBodySegmentController;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.DebugBox;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;

public class ActivationGateUnit extends ElementCollection {
   Vector3i min = new Vector3i();
   Vector3i max = new Vector3i();
   Vector3f minf = new Vector3f();
   Vector3f maxf = new Vector3f();
   Vector3f minOut = new Vector3f();
   Vector3f maxOut = new Vector3f();
   Vector3f minOtherOut = new Vector3f();
   Vector3f maxOtherOut = new Vector3f();
   Vector3f minBoxOther = new Vector3f(1.0F, 1.0F, 1.0F);
   Vector3f maxBoxOther = new Vector3f(1.0F, 1.0F, 1.0F);
   private boolean inGraphics;
   private long lastPopup;
   private Transform otherTrans = new Transform();
   private Transform otherTransBef = new Transform();
   private Transform invTrans = new Transform();
   private Transform invTransBef = new Transform();
   private int xDelta;
   private int yDelta;
   private int zDelta;
   private boolean xDim;
   private boolean yDim;
   private boolean zDim;
   private final Vector3f normal = new Vector3f();
   private final float[] param = new float[1];

   public float getPowerNeeded(SimpleTransformableSendableObject var1) {
      return var1.getMass() * ActivationGateElementManager.POWER_NEEDED_PER_MASS;
   }

   public float getPowerConsumption() {
      return (float)this.size() * ActivationGateElementManager.POWER_CONST_NEEDED_PER_BLOCK;
   }

   public boolean isValid() {
      return (this.xDim || this.yDim || this.zDim) && super.isValid();
   }

   public String toString() {
      return "ActivationGateUnit " + super.toString();
   }

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((ActivationGateElementManager)((ActivationGateCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (ActivationGateCollectionManager)this.elementCollectionManager, var2, var3);
   }

   public String getValidInfo() {
      boolean var1 = this.xDim || this.yDim || this.zDim;
      return "DimOK: " + var1 + " (" + this.xDelta + ", " + this.yDelta + ", " + this.zDelta + "); 2Neighbors: " + super.isValid();
   }

   public boolean isInGraphics() {
      return this.inGraphics;
   }

   public void setInGraphics(boolean var1) {
      this.inGraphics = var1;
   }

   public void update(Timer var1) {
      assert this.isValid();

      PowerAddOn var2 = ((PowerManagerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getPowerAddOn();
      float var7 = this.getPowerConsumption() * var1.getDelta();
      boolean var8;
      if (this.getSegmentController().isOnServer()) {
         if (var2.canConsumePowerInstantly(var7) && var2.consumePowerInstantly((double)var7)) {
            var8 = false;

            for(int var3 = 0; var3 < this.getSegmentController().getPhysics().getDynamicsWorld().getBroadphase().getOverlappingPairCache().getNumOverlappingPairs(); ++var3) {
               BroadphasePair var4 = (BroadphasePair)this.getSegmentController().getPhysics().getDynamicsWorld().getBroadphase().getOverlappingPairCache().getOverlappingPairArray().get(var3);
               RigidBodySegmentController var9 = null;
               PairCachingGhostObjectAlignable var5 = null;
               if (var4.pProxy0.clientObject == this.getSegmentController().railController.getRoot().getPhysicsDataContainer().getObject()) {
                  if (var4.pProxy1.clientObject instanceof RigidBodySegmentController) {
                     var9 = (RigidBodySegmentController)var4.pProxy1.clientObject;
                  } else if (var4.pProxy1.clientObject instanceof PairCachingGhostObjectAlignable) {
                     var5 = (PairCachingGhostObjectAlignable)var4.pProxy1.clientObject;
                  }
               } else if (var4.pProxy1.clientObject == this.getSegmentController().railController.getRoot().getPhysicsDataContainer().getObject()) {
                  if (var4.pProxy0.clientObject instanceof RigidBodySegmentController) {
                     var9 = (RigidBodySegmentController)var4.pProxy0.clientObject;
                  } else if (var4.pProxy0.clientObject instanceof PairCachingGhostObjectAlignable) {
                     var5 = (PairCachingGhostObjectAlignable)var4.pProxy0.clientObject;
                  }
               }

               boolean var6;
               boolean var12;
               if (var9 != null) {
                  if (!var8) {
                     this.getMin(this.min);
                     this.getMax(this.max);
                     this.minf.set((float)(this.min.x - 16), (float)(this.min.y - 16), (float)(this.min.z - 16));
                     this.maxf.set((float)(this.max.x - 16), (float)(this.max.y - 16), (float)(this.max.z - 16));
                     AabbUtil2.transformAabb(this.minf, this.maxf, 0.0F, this.getSegmentController().getWorldTransform(), this.minOut, this.maxOut);
                     var8 = true;
                  }

                  var9.getAabb(this.minOtherOut, this.maxOtherOut);
                  this.getBeforAndAfter(var9);
                  this.param[0] = 1.0F;
                  this.normal.set(0.0F, 0.0F, 0.0F);
                  var12 = AabbUtil2.testAabbAgainstAabb2(this.minOut, this.maxOut, this.minOtherOut, this.maxOtherOut);
                  var6 = AabbUtil2.rayAabb(this.otherTransBef.origin, this.otherTrans.origin, this.minOut, this.maxOut, this.param, this.normal);
                  if (var12 || var6) {
                     this.narrowTest(var9);
                  }
               } else if (var5 != null) {
                  if (!var8) {
                     this.getMin(this.min);
                     this.getMax(this.max);
                     this.minf.set((float)(this.min.x - 16), (float)(this.min.y - 16), (float)(this.min.z - 16));
                     this.maxf.set((float)(this.max.x - 16), (float)(this.max.y - 16), (float)(this.max.z - 16));
                     AabbUtil2.transformAabb(this.minf, this.maxf, 0.0F, this.getSegmentController().getWorldTransform(), this.minOut, this.maxOut);
                     var8 = true;
                  }

                  var5.getCollisionShape().getAabb(var5.getWorldTransform(new Transform()), this.minOtherOut, this.maxOtherOut);
                  this.getBeforAndAfter(var5);
                  this.param[0] = 1.0F;
                  this.normal.set(0.0F, 0.0F, 0.0F);
                  var12 = AabbUtil2.testAabbAgainstAabb2(this.minOut, this.maxOut, this.minOtherOut, this.maxOtherOut);
                  var6 = AabbUtil2.rayAabb(this.otherTransBef.origin, this.otherTrans.origin, this.minOut, this.maxOut, this.param, this.normal);
                  if (var12 || var6) {
                     this.narrowTest(var5);
                  }
               }
            }

            return;
         }
      } else {
         this.isInGraphics();
         var8 = var2.canConsumePowerInstantly(var7) && var2.consumePowerInstantly((double)var7);
         if (!this.getSegmentController().isOnServer() && ((GameClientState)this.getSegmentController().getState()).getCurrentSectorId() == this.getSegmentController().getSectorId() && !var8 && System.currentTimeMillis() - this.lastPopup > 5000L) {
            Transform var10;
            (var10 = new Transform()).setIdentity();
            Vector3i var13 = ((ActivationGateCollectionManager)this.elementCollectionManager).getControllerPos();
            var10.origin.set((float)(var13.x - 16), (float)(var13.y - 16), (float)(var13.z - 16));
            this.getSegmentController().getWorldTransform().transform(var10.origin);
            RaisingIndication var11;
            (var11 = new RaisingIndication(var10, "Insufficient Energy\nNeeded/Sec vs PowerRecharge/sec:\n" + StringTools.formatPointZero(this.getPowerConsumption()) + " / " + StringTools.formatPointZero(var2.getRecharge()), 1.0F, 0.1F, 0.1F, 1.0F)).speed = 0.2F;
            var11.lifetime = 1.0F;
            HudIndicatorOverlay.toDrawTexts.add(var11);
            this.lastPopup = System.currentTimeMillis();
         }
      }

   }

   public void getBeforAndAfter(CollisionObject var1) {
      if (var1 instanceof RigidBodySegmentController) {
         RigidBodySegmentController var2 = (RigidBodySegmentController)var1;
         this.otherTrans.set(var2.getSegmentController().getPhysicsDataContainer().thisTransform);
         this.otherTransBef.set(var2.getSegmentController().getPhysicsDataContainer().lastTransform);
      } else if (var1 instanceof PairCachingGhostObjectAlignable) {
         PairCachingGhostObjectAlignable var3 = (PairCachingGhostObjectAlignable)var1;
         this.otherTrans.set(var3.getObj().getPhysicsDataContainer().thisTransform);
         this.otherTransBef.set(var3.getObj().getPhysicsDataContainer().lastTransform);
      }

      this.invTrans.set(this.getSegmentController().getWorldTransformInverse());
      this.invTrans.mul(this.otherTrans);
      this.invTransBef.set(this.getSegmentController().getWorldTransformInverse());
      this.invTransBef.mul(this.otherTransBef);
   }

   public void debugDraw(Vector3i var1) {
      this.debugDraw(var1.x, var1.y, var1.z);
   }

   public void debugDraw(int var1, int var2, int var3) {
      if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
         Transform var4 = new Transform(this.getSegmentController().getWorldTransform());
         Vector3f var5;
         (var5 = new Vector3f()).set((float)var1, (float)var2, (float)var3);
         var5.x -= 16.0F;
         var5.y -= 16.0F;
         var5.z -= 16.0F;
         var4.basis.transform(var5);
         var4.origin.add(var5);
         DebugBox var6;
         (var6 = new DebugBox(new Vector3f(-0.51F, -0.51F, -0.51F), new Vector3f(0.51F, 0.51F, 0.51F), var4, 1.0F, 0.0F, 0.0F, 1.0F)).LIFETIME = 200L;
         DebugDrawer.boxes.add(var6);
      }

   }

   private void narrowTest(CollisionObject var1) {
      this.getBeforAndAfter(var1);
      Vector3f var2 = new Vector3f(this.invTransBef.origin);
      Vector3f var3;
      (var3 = new Vector3f(this.invTrans.origin)).sub(var2);
      if (var3.lengthSquared() > 0.0F) {
         int var4;
         int var5;
         long var6;
         long var7;
         Transform var8;
         Vector3i var9;
         DebugBox var10;
         Vector3f var11;
         int var12;
         if (this.max.x - this.min.x == 1) {
            if ((var2 = Vector3fTools.intersectLinePLane(this.invTransBef.origin, this.invTrans.origin, new Vector3f(this.minf.x, 0.0F, 0.0F), new Vector3f(1.0F, 0.0F, 0.0F))) != null && var2.y <= this.maxf.y && var2.y >= this.minf.y && var2.z <= this.maxf.z && var2.z >= this.minf.z) {
               var9 = new Vector3i(Math.round(var2.x) + 16, Math.round(var2.y) + 16, Math.round(var2.z) + 16);
               this.debugDraw(var9);
               var4 = 0;

               for(var5 = var9.y + 1; var5 <= this.max.y; ++var5) {
                  this.debugDraw(var9.x, var5, var9.z);
                  var6 = getIndex(var9.x, var5, var9.z);
                  if (this.getNeighboringCollection().contains(var6)) {
                     ++var4;
                  }
               }

               var12 = 0;
               if (var4 % 2 == 1) {
                  for(var5 = var9.y - 1; var5 >= this.min.y; --var5) {
                     this.debugDraw(var9.x, var5, var9.z);
                     var7 = getIndex(var9.x, var5, var9.z);
                     if (this.getNeighboringCollection().contains(var7)) {
                        ++var12;
                     }
                  }
               }

               if (var4 % 2 == 1 && var12 % 2 == 1) {
                  if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
                     var8 = new Transform(this.getSegmentController().getWorldTransform());
                     (var11 = new Vector3f()).set((float)var9.x, (float)var9.y, (float)var9.z);
                     var11.x -= 16.0F;
                     var11.y -= 16.0F;
                     var11.z -= 16.0F;
                     var8.basis.transform(var11);
                     var8.origin.add(var11);
                     (var10 = new DebugBox(new Vector3f(-0.51F, -0.51F, -0.51F), new Vector3f(0.51F, 0.51F, 0.51F), var8, 0.0F, 0.0F, 1.0F, 1.0F)).LIFETIME = 200L;
                     DebugDrawer.boxes.add(var10);
                  }

                  System.err.println("X#################### activating!!!!!!!!!!!!! " + var2);
                  this.tryActivate(var1);
                  return;
               }

               System.err.println("X-------------------- activating FAILED ON GATE: DirA intersection " + var4 + "; DirB intersection: " + var12);
               return;
            }
         } else if (this.max.y - this.min.y == 1) {
            if ((var2 = Vector3fTools.intersectLinePLane(this.invTransBef.origin, this.invTrans.origin, new Vector3f(0.0F, this.minf.y, 0.0F), new Vector3f(0.0F, 1.0F, 0.0F))) != null && var2.x <= this.maxf.x && var2.x >= this.minf.x && var2.z <= this.maxf.z && var2.z >= this.minf.z) {
               var9 = new Vector3i(Math.round(var2.x) + 16, Math.round(var2.y) + 16, Math.round(var2.z) + 16);
               this.debugDraw(var9);
               var4 = 0;

               for(var5 = var9.x + 1; var5 <= this.max.x; ++var5) {
                  this.debugDraw(var5, var9.y, var9.z);
                  var6 = getIndex(var5, var9.y, var9.z);
                  if (this.getNeighboringCollection().contains(var6)) {
                     ++var4;
                  }
               }

               var12 = 0;
               if (var4 % 2 == 1) {
                  for(var5 = var9.x - 1; var5 >= this.min.x; --var5) {
                     this.debugDraw(var5, var9.y, var9.z);
                     var7 = getIndex(var5, var9.y, var9.z);
                     if (this.getNeighboringCollection().contains(var7)) {
                        ++var12;
                     }
                  }
               }

               if (var4 % 2 == 1 && var12 % 2 == 1) {
                  if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
                     var8 = new Transform(this.getSegmentController().getWorldTransform());
                     (var11 = new Vector3f()).set((float)var9.x, (float)var9.y, (float)var9.z);
                     var11.x -= 16.0F;
                     var11.y -= 16.0F;
                     var11.z -= 16.0F;
                     var8.basis.transform(var11);
                     var8.origin.add(var11);
                     (var10 = new DebugBox(new Vector3f(-0.51F, -0.51F, -0.51F), new Vector3f(0.51F, 0.51F, 0.51F), var8, 0.0F, 0.0F, 1.0F, 1.0F)).LIFETIME = 200L;
                     DebugDrawer.boxes.add(var10);
                  }

                  System.err.println("Y#################### activating!!!!!!!!!!!!! " + var2);
                  this.tryActivate(var1);
                  return;
               }

               System.err.println("Y-------------------- activating FAILED ON GATE: DirA intersection " + var4 + "; DirB intersection: " + var12);
               return;
            }
         } else if ((var2 = Vector3fTools.intersectLinePLane(this.invTransBef.origin, this.invTrans.origin, new Vector3f(0.0F, 0.0F, this.minf.z), new Vector3f(0.0F, 0.0F, 1.0F))) != null && var2.x <= this.maxf.x && var2.x >= this.minf.x && var2.y <= this.maxf.y && var2.y >= this.minf.y) {
            var9 = new Vector3i(Math.round(var2.x) + 16, Math.round(var2.y) + 16, Math.round(var2.z) + 16);
            this.debugDraw(var9);
            var4 = 0;

            for(var5 = var9.y + 1; var5 <= this.max.y; ++var5) {
               this.debugDraw(var9.x, var5, var9.z);
               var6 = getIndex(var9.x, var5, var9.z);
               if (this.getNeighboringCollection().contains(var6)) {
                  ++var4;
               }
            }

            var12 = 0;
            if (var4 % 2 == 1) {
               for(var5 = var9.y - 1; var5 >= this.min.y; --var5) {
                  this.debugDraw(var9.x, var5, var9.z);
                  var7 = getIndex(var9.x, var5, var9.z);
                  if (this.getNeighboringCollection().contains(var7)) {
                     ++var12;
                  }
               }
            }

            if (var4 % 2 == 1 && var12 % 2 == 1) {
               if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
                  var8 = new Transform(this.getSegmentController().getWorldTransform());
                  (var11 = new Vector3f()).set((float)var9.x, (float)var9.y, (float)var9.z);
                  var11.x -= 16.0F;
                  var11.y -= 16.0F;
                  var11.z -= 16.0F;
                  var8.basis.transform(var11);
                  var8.origin.add(var11);
                  (var10 = new DebugBox(new Vector3f(-0.51F, -0.51F, -0.51F), new Vector3f(0.51F, 0.51F, 0.51F), var8, 0.0F, 0.0F, 1.0F, 1.0F)).LIFETIME = 200L;
                  DebugDrawer.boxes.add(var10);
               }

               System.err.println("Z#################### activating!!!!!!!!!!!!! " + var2);
               this.tryActivate(var1);
               return;
            }

            System.err.println("Z-------------------- activating FAILED ON GATE: DirA intersection " + var4 + "; DirB intersection: " + var12);
         }
      }

   }

   private void tryActivate(CollisionObject var1) {
      System.err.println("[ACTIVATIONGATE] ACTIVATING " + var1);
      PowerAddOn var2;
      if (var1 instanceof RigidBodySegmentController) {
         RigidBodySegmentController var4 = (RigidBodySegmentController)var1;
         var2 = ((PowerManagerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getPowerAddOn();
         if (this.getSegmentController().isUsingOldPower() && !var2.consumePowerInstantly((double)this.getPowerNeeded(var4.getSegmentController()))) {
            var4.getSegmentController().sendControllingPlayersServerMessage(new Object[]{32, this.getPowerNeeded(var4.getSegmentController())}, 3);
         } else {
            System.err.println("[ACTIVATIONGATE] " + this.getSegmentController().getState() + " activate from object: " + var4);
            this.activate(var4);
         }
      } else {
         if (var1 instanceof PairCachingGhostObjectAlignable) {
            PairCachingGhostObjectAlignable var3 = (PairCachingGhostObjectAlignable)var1;
            var2 = ((PowerManagerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getPowerAddOn();
            if (!this.getSegmentController().isUsingOldPower() || var2.consumePowerInstantly(1.0D)) {
               System.err.println("[ACTIVATIONGATE] " + this.getSegmentController().getState() + " activating object: " + var3);
               this.activate(var3);
               return;
            }

            var3.getObj().sendControllingPlayersServerMessage(new Object[]{33}, 3);
         }

      }
   }

   private void activate(CollisionObject var1) {
      Short2ObjectOpenHashMap var8;
      if ((var8 = this.getSegmentController().getControlElementMap().getControllingMap().get(((ActivationGateCollectionManager)this.elementCollectionManager).getControllerElement().getAbsoluteIndex())) != null) {
         short[] var2;
         int var3 = (var2 = ElementKeyMap.signalArray).length;

         for(int var4 = 0; var4 < var3; ++var4) {
            short var5 = var2[var4];
            FastCopyLongOpenHashSet var9;
            if ((var9 = (FastCopyLongOpenHashSet)var8.get(var5)) != null) {
               Iterator var10 = var9.iterator();

               while(var10.hasNext()) {
                  long var6 = (Long)var10.next();
                  ((SendableSegmentController)this.getSegmentController()).activateSwitchSingleServer(ElementCollection.getPosIndexFrom4(var6));
               }
            }
         }
      }

   }

   public void calculateExtraDataAfterCreationThreaded(long var1, LongOpenHashSet var3) {
      this.getMin(this.min);
      this.getMax(this.max);
      this.xDelta = this.max.x - this.min.x;
      this.yDelta = this.max.y - this.min.y;
      this.zDelta = this.max.z - this.min.z;
      this.xDim = this.xDelta == 1 && this.yDelta > 1 && this.zDelta > 1;
      this.yDim = this.xDelta > 1 && this.yDelta == 1 && this.zDelta > 1;
      this.zDim = this.xDelta > 1 && this.yDelta > 1 && this.zDelta == 1;
   }
}
