package org.schema.game.common.controller.elements;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.SegNotifyType;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.SlotAssignment;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.config.ConfigEntityManager;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ShipConfigurationNotFoundException;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public abstract class UsableControllableElementManager extends UsableElementManager implements TargetableSystemInterface {
   private static final boolean DEBUG = false;
   private final ObjectArrayList collectionManagers = new ObjectArrayList();
   private final Long2ObjectOpenHashMap collectionManagerMap = new Long2ObjectOpenHashMap();
   public final short controllerId;
   public final short controllingId;
   private long lastSendLimitWarning;
   private boolean integrityChecked;
   private long startCheck;
   private boolean needsDeepCheck;
   private int deepCheckPointer;
   private DeepStructureChecker deepChecker;
   public LongOpenHashSet uniqueConnections;

   public boolean isCheckForUniqueConnections() {
      return false;
   }

   public ConfigEntityManager getConfigManager() {
      return this.getSegmentController().getConfigManager();
   }

   public void onElementCollectionsChanged() {
      this.lowestIntegrity = Double.POSITIVE_INFINITY;
      int var1 = this.collectionManagers.size();

      for(int var2 = 0; var2 < var1; ++var2) {
         this.lowestIntegrity = Math.min(this.lowestIntegrity, ((ControlBlockElementCollectionManager)this.collectionManagers.get(var2)).getLowestIntegrity());
      }

   }

   public UsableControllableElementManager(short var1, short var2, SegmentController var3) {
      super(var3);
      this.controllerId = var1;
      this.controllingId = var2;
      if (var2 != 32767 && var2 != 29998 && var2 != 29999 && var2 != 30000) {
         ElementInformation var5 = ElementKeyMap.getInfo(var1);
         ElementInformation var4 = ElementKeyMap.getInfo(var2);

         assert var5.getControlling().contains(var2) : this.getClass() + "; " + ElementKeyMap.toString(var5.getControlling()) + " : " + ElementKeyMap.toString(var1) + " -> " + ElementKeyMap.toString(var2);

         assert var4.getControlledBy().contains(var1) : this.getClass() + "; " + var4.getName() + ": controlled by set " + var4.getControlledBy() + " does not contain " + ElementKeyMap.getInfo(var1) + "(" + var1 + ")";
      }

   }

   public boolean canHandle(ControllerStateInterface var1) {
      if (this.getSegmentController().isOnServer() && ((GameServerState)this.getState()).getGameConfig().hasControllerLimit(this.controllerId, this.getCollectionManagers().size())) {
         if (System.currentTimeMillis() - this.lastSendLimitWarning > 5000L) {
            this.getSegmentController().sendControllingPlayersServerMessage(new Object[]{71, ((GameServerState)this.getState()).getGameConfig().getControllerLimit(this.controllerId)}, 3);
            this.lastSendLimitWarning = System.currentTimeMillis();
         }

         return false;
      } else if (this.getSegmentController().checkBlockMassServerLimitOk()) {
         return super.canHandle(var1);
      } else {
         if (!this.getSegmentController().isOnServer() && System.currentTimeMillis() - this.lastSendLimitWarning > 5000L) {
            int var4 = ((GameStateInterface)this.getState()).getGameState().getBlockLimit(this.getSegmentController());
            double var2 = (double)Math.round(((GameStateInterface)this.getState()).getGameState().getMassLimit(this.getSegmentController()) * 100.0D) / 100.0D;
            String var5 = var4 > 0 ? var4 + " " + Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_USABLECONTROLLABLEELEMENTMANAGER_1 : "";
            var5 = var5 + (var5.length() > 0 && var2 > 0.0D ? " " + Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_USABLECONTROLLABLEELEMENTMANAGER_2 + " " : "");
            var5 = var5 + (var2 > 0.0D ? var2 + " " + Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_USABLECONTROLLABLEELEMENTMANAGER_3 : "");
            this.getSegmentController().popupOwnClientMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_USABLECONTROLLABLEELEMENTMANAGER_4, var5), 3);
            this.lastSendLimitWarning = System.currentTimeMillis();
         }

         return false;
      }
   }

   public static Vector3f getShootingDir(SegmentController var0, Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4, Vector3f var5, Vector3f var6, boolean var7, Vector3i var8, SegmentPiece var9) {
      var4.set(var1);
      var5.set(var2);
      var6.set(var3);
      SegmentPiece var10;
      if (var7 && (var10 = var0.getSegmentBuffer().getPointUnsave(var8, var9)) != null && var10.getOrientation() != 0) {
         switch(var10.getOrientation()) {
         case 1:
            GlUtil.getBackVector(var4, var0.getWorldTransform());
            GlUtil.getUpVector(var5, (Transform)var0.getWorldTransform());
            GlUtil.getRightVector(var6, (Transform)var0.getWorldTransform());
            break;
         case 2:
            GlUtil.getUpVector(var4, (Transform)var0.getWorldTransform());
            GlUtil.getForwardVector(var5, (Transform)var0.getWorldTransform());
            GlUtil.getRightVector(var6, (Transform)var0.getWorldTransform());
            break;
         case 3:
            GlUtil.getBottomVector(var4, var0.getWorldTransform());
            GlUtil.getBackVector(var5, var0.getWorldTransform());
            GlUtil.getRightVector(var6, (Transform)var0.getWorldTransform());
            break;
         case 4:
            GlUtil.getLeftVector(var4, var0.getWorldTransform());
            GlUtil.getUpVector(var5, (Transform)var0.getWorldTransform());
            GlUtil.getBackVector(var6, var0.getWorldTransform());
            break;
         case 5:
            GlUtil.getRightVector(var4, (Transform)var0.getWorldTransform());
            GlUtil.getUpVector(var5, (Transform)var0.getWorldTransform());
            GlUtil.getForwardVector(var6, (Transform)var0.getWorldTransform());
         }
      }

      return var4;
   }

   public static final void drawReload(InputState var0, Vector3i var1, Vector3i var2, Vector4f var3, boolean var4, float var5) {
      drawReload(var0, var1, var2, var3, var4, var5, false, 0.0F, 0, -1L, (GUITextOverlay)null);
   }

   public static final void drawReloadText(InputState var0, Vector3i var1, Vector3i var2, GUITextOverlay var3) {
      GlUtil.glPushMatrix();
      GlUtil.glTranslatef((float)var1.x + (float)var2.x / 2.0F, (float)var1.y + (float)var2.y / 2.0F, 0.0F);
      GlUtil.glDisable(2896);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      var3.draw();
      GlUtil.glPopMatrix();
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glEnable(2896);
      GlUtil.glDisable(3042);
   }

   public static final void drawReload(InputState var0, Vector3i var1, Vector3i var2, Vector4f var3, boolean var4, float var5, boolean var6, float var7, int var8, long var9, GUITextOverlay var11) {
      if (var5 > 10000.0F) {
         throw new RuntimeException("Too much charge? " + var5);
      } else {
         var5 = Math.min(1.0F, var5);
         GlUtil.glPushMatrix();
         GlUtil.glTranslatef((float)var1.x + (float)var2.x / 2.0F, (float)var1.y + (float)var2.y / 2.0F, 0.0F);
         GlUtil.glDisable(2896);
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         GlUtil.glColor4f(var3.x, var3.y, var3.z, var3.w);
         float var12 = var5 + 0.125F;
         if (!var6 || var5 < 1.0F) {
            GL11.glBegin(6);
            GL11.glVertex3f(0.0F, 0.0F, 0.0F);

            for(float var13 = (float)var2.x / 2.0F; var12 >= 0.12F; var12 -= 0.005F) {
               float var15 = var12;
               if (var12 < 0.125F) {
                  var15 = 0.125F;
               }

               if (var15 > 1.0F) {
                  --var15;
               }

               if (var15 < 0.25F) {
                  GL11.glVertex2f((float)var2.x * var15 * 4.0F - var13, -var13);
               } else if (var15 < 0.5F) {
                  var15 -= 0.25F;
                  GL11.glVertex2f(var13, (float)var2.y * var15 * 4.0F - var13);
               } else if (var15 < 0.75F) {
                  var15 -= 0.5F;
                  GL11.glVertex2f((float)(-var2.x) * var15 * 4.0F + var13, var13);
               } else {
                  var15 -= 0.75F;
                  GL11.glVertex2f(-var13, (float)(-var2.y) * var15 * 4.0F + var13);
               }
            }

            GL11.glEnd();
         }

         if (var9 > 0L) {
            var11.setTextSimple(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_USABLECONTROLLABLEELEMENTMANAGER_5, StringTools.formatPointZero((double)var9 / 1000.0D)));
            var11.setPos(-8.0F, 7.0F, 0.0F);
            if (var6) {
               var11.getPos().x = -20.0F;
            }

            var11.draw();
         } else if (var6 || var8 > 1) {
            String var14;
            if (!var6) {
               var14 = String.valueOf((int)var7);
            } else if (Math.abs((float)Math.round(var7) - var7) < 0.05F) {
               var14 = String.valueOf(Math.round(var7));
            } else {
               var14 = StringTools.formatPointZero(var7);
            }

            var11.setTextSimple(var14 + " / " + var8);
            var11.setPos(-8.0F, 7.0F, 0.0F);
            if (var6) {
               var11.getPos().x = -20.0F;
            }

            var11.draw();
         }

         GlUtil.glPopMatrix();
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GlUtil.glEnable(2896);
         GlUtil.glDisable(3042);
      }
   }

   public int getPriority() {
      return 1;
   }

   public ElementCollection getRandomCollection(Random var1) {
      if (this.hasAnyBlock()) {
         ControlBlockElementCollectionManager var2;
         if ((var2 = (ControlBlockElementCollectionManager)this.getCollectionManagers().get(var1.nextInt(this.getCollectionManagers().size()))).getElementCollections().size() > 0 && var2.getTotalSize() > 0) {
            return (ElementCollection)var2.getElementCollections().get(var1.nextInt(var2.getElementCollections().size()));
         }

         for(int var4 = 0; var4 < this.getCollectionManagers().size(); ++var4) {
            ControlBlockElementCollectionManager var3;
            if ((var3 = (ControlBlockElementCollectionManager)this.getCollectionManagers().get(var4)).getElementCollections().size() > 0 && var3.getTotalSize() > 0) {
               return (ElementCollection)var3.getElementCollections().get(var1.nextInt(var3.getElementCollections().size()));
            }
         }
      }

      return null;
   }

   public void beforeUpdate() {
   }

   public void checkIntegrityServer() {
      Iterator var2;
      if (!this.integrityChecked && ElementKeyMap.isValidType(this.controllerId) && ElementKeyMap.isValidType(this.controllingId) && this.getSegmentController().isFullyLoaded()) {
         if (this.startCheck == 0L) {
            this.startCheck = System.currentTimeMillis();
         } else if (System.currentTimeMillis() - this.startCheck > 30000L) {
            int var1 = 0;
            var2 = this.collectionManagers.iterator();

            while(var2.hasNext()) {
               ControlBlockElementCollectionManager var3 = (ControlBlockElementCollectionManager)var2.next();
               Short2ObjectOpenHashMap var5;
               FastCopyLongOpenHashSet var6;
               if ((var5 = this.getSegmentController().getControlElementMap().getControllingMap().get(ElementCollection.getIndex(var3.getControllerPos()))) != null && (var6 = (FastCopyLongOpenHashSet)var5.get(this.controllingId)) != null) {
                  var1 += var6.size();
               }
            }

            if (var1 > this.getSegmentController().getElementClassCountMap().get(this.controllingId)) {
               this.needsDeepCheck = true;
            }

            this.integrityChecked = true;
         }
      }

      if (this.needsDeepCheck) {
         if (this.deepCheckPointer >= this.collectionManagers.size() && (this.deepChecker == null || this.deepChecker.isDone())) {
            this.needsDeepCheck = false;
            this.deepChecker = null;
            System.err.println("[SERVER][CHECKER] " + this.getSegmentController() + " PERFORMING DEEP STRUCTURE CHECK DONE! Total Comps: " + this.deepCheckPointer);
            return;
         }

         if (this.deepChecker == null || this.deepChecker.isDone()) {
            ControlBlockElementCollectionManager var4 = (ControlBlockElementCollectionManager)this.collectionManagers.get(this.deepCheckPointer);
            if (this.deepChecker == null) {
               this.deepChecker = new DeepStructureChecker();
            }

            this.deepChecker.set(this.getSegmentController(), var4.getControllerIndex(), this.controllingId, var4.getTotalSize());
            var2 = var4.getElementCollections().iterator();

            while(var2.hasNext()) {
               ElementCollection var7 = (ElementCollection)var2.next();
               this.deepChecker.init(var7.getNeighboringCollection());
            }

            this.deepChecker.update();
            ++this.deepCheckPointer;
            return;
         }

         this.deepChecker.update();
      }

   }

   public void afterUpdate() {
   }

   public void addConnectionIfNecessary(Vector3i var1, short var2, Vector3i var3, short var4) {
      ControlBlockElementCollectionManager var10;
      if (var4 != this.controllingId && this.controllingId != 32767) {
         if (ElementKeyMap.isValidType(this.controllerId) && ElementKeyMap.getInfo(this.controllerId).isCombiConnectAny(var4) && (var10 = (ControlBlockElementCollectionManager)this.getCollectionManagersMap().get(ElementCollection.getIndex(var1))) != null) {
            short var5;
            long var6;
            long var8;
            ControlBlockElementCollectionManager var9;
            ManagerModuleCollection var11;
            if (ElementKeyMap.getInfo(this.controllerId).isCombiConnectSupport(var4)) {
               if ((var6 = ElementCollection.getIndex4((short)var3.x, (short)var3.y, (short)var3.z, var4)) != var10.getSlaveConnectedElement()) {
                  if (var10.getSlaveConnectedElement() != Long.MIN_VALUE) {
                     var8 = ElementCollection.getPosIndexFrom4(var10.getSlaveConnectedElement());
                     var5 = (short)ElementCollection.getType(var10.getSlaveConnectedElement());
                     this.getSegmentController().getControlElementMap().removeControllerForElement(ElementCollection.getIndex(var1), var8, var5);
                     if (this.getSegmentController() instanceof Ship) {
                        this.getSegmentController().getControlElementMap().addControllerForElement(ElementCollection.getIndex(16, 16, 16), var8, var5);
                        this.getSegmentController().getControlElementMap().removeControllerForElement(ElementCollection.getIndex(16, 16, 16), ElementCollection.getIndex(var3), var4);
                     }
                  }

                  var10.setSlaveConnectedElement(var6);
                  if ((var11 = this.getManagerContainer().getModulesControllerMap().get(var4)) != null && (var9 = (ControlBlockElementCollectionManager)var11.getCollectionManagersMap().get(ElementCollection.getIndex(var3))) != null) {
                     this.disconnectAllSupportAndEffectAndLight(var9, var3);
                  }

                  this.notifyObservers(SegNotifyType.SHIP_ELEMENT_CHANGED);
                  if (this.getState() instanceof GameClientState) {
                     ((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().getWeaponManagerPanel().setReconstructionRequested(true);
                  }
               }

               return;
            }

            if (ElementKeyMap.getInfo(this.controllerId).isCombiConnectEffect(var4)) {
               if ((var6 = ElementCollection.getIndex4((short)var3.x, (short)var3.y, (short)var3.z, var4)) != var10.getEffectConnectedElement()) {
                  if (var10.getEffectConnectedElement() != Long.MIN_VALUE) {
                     var8 = ElementCollection.getPosIndexFrom4(var10.getEffectConnectedElement());
                     var5 = (short)ElementCollection.getType(var10.getEffectConnectedElement());
                     this.getSegmentController().getControlElementMap().removeControllerForElement(ElementCollection.getIndex(var1), var8, var5);
                     if (this.getSegmentController() instanceof Ship) {
                        this.getSegmentController().getControlElementMap().addControllerForElement(ElementCollection.getIndex(16, 16, 16), var8, var5);
                        this.getSegmentController().getControlElementMap().removeControllerForElement(ElementCollection.getIndex(16, 16, 16), ElementCollection.getIndex(var3), var4);
                     }
                  }

                  var10.setEffectConnectedElement(var6);
                  this.notifyObservers(SegNotifyType.SHIP_ELEMENT_CHANGED);
                  if (this.getState() instanceof GameClientState) {
                     ((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().getWeaponManagerPanel().setReconstructionRequested(true);
                  }

                  if ((var11 = this.getManagerContainer().getModulesControllerMap().get(var4)) != null && (var9 = (ControlBlockElementCollectionManager)var11.getCollectionManagersMap().get(ElementCollection.getIndex(var3))) != null) {
                     this.disconnectAllSupportAndEffectAndLight(var9, var3);
                  }
               }

               return;
            }

            if (ElementKeyMap.getInfo(this.controllerId).isLightConnect(var4)) {
               if ((var6 = ElementCollection.getIndex4((short)var3.x, (short)var3.y, (short)var3.z, var4)) != var10.getLightConnectedElement()) {
                  if (var10.getLightConnectedElement() != Long.MIN_VALUE) {
                     var8 = ElementCollection.getPosIndexFrom4(var10.getLightConnectedElement());
                     var5 = (short)ElementCollection.getType(var10.getLightConnectedElement());
                     this.getSegmentController().getControlElementMap().removeControllerForElement(ElementCollection.getIndex(var1), var8, var5);
                     this.getSegmentController().getControlElementMap().removeControllerForElement(ElementCollection.getIndex(16, 16, 16), ElementCollection.getIndex(var3), var4);
                  }

                  var10.setLightConnectedElement(var6);
                  this.notifyObservers(SegNotifyType.SHIP_ELEMENT_CHANGED);
                  if (this.getState() instanceof GameClientState) {
                     ((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().getWeaponManagerPanel().setReconstructionRequested(true);
                  }

                  if ((var11 = this.getManagerContainer().getModulesControllerMap().get(var4)) != null && (var9 = (ControlBlockElementCollectionManager)var11.getCollectionManagersMap().get(ElementCollection.getIndex(var3))) != null) {
                     this.disconnectAllSupportAndEffectAndLight(var9, var3);
                  }
               }

               return;
            }
         }

      } else if ((var10 = (ControlBlockElementCollectionManager)this.getCollectionManagersMap().get(ElementCollection.getIndex(var1))) != null) {
         var10.addModded(var3, var4);
         this.notifyObservers(SegNotifyType.SHIP_ELEMENT_CHANGED);
      }
   }

   private void disconnectAllSupportAndEffectAndLight(ControlBlockElementCollectionManager var1, Vector3i var2) {
      long var3;
      short var5;
      if (var1.getSlaveConnectedElement() != Long.MIN_VALUE) {
         var3 = ElementCollection.getPosIndexFrom4(var1.getSlaveConnectedElement());
         var5 = (short)ElementCollection.getType(var1.getSlaveConnectedElement());
         this.getSegmentController().getControlElementMap().removeControllerForElement(ElementCollection.getIndex(var2), var3, var5);
         if (this.getSegmentController() instanceof Ship) {
            this.getSegmentController().getControlElementMap().addControllerForElement(ElementCollection.getIndex(16, 16, 16), var3, var5);
         }
      }

      if (var1.getEffectConnectedElement() != Long.MIN_VALUE) {
         var3 = ElementCollection.getPosIndexFrom4(var1.getEffectConnectedElement());
         var5 = (short)ElementCollection.getType(var1.getEffectConnectedElement());
         this.getSegmentController().getControlElementMap().removeControllerForElement(ElementCollection.getIndex(var2), var3, var5);
         if (this.getSegmentController() instanceof Ship) {
            this.getSegmentController().getControlElementMap().addControllerForElement(ElementCollection.getIndex(16, 16, 16), var3, var5);
         }
      }

      if (var1.getLightConnectedElement() != Long.MIN_VALUE) {
         var3 = ElementCollection.getPosIndexFrom4(var1.getLightConnectedElement());
         var5 = (short)ElementCollection.getType(var1.getLightConnectedElement());
         this.getSegmentController().getControlElementMap().removeControllerForElement(ElementCollection.getIndex(var2), var3, var5);
      }

      var1.setSlaveConnectedElement(Long.MIN_VALUE);
      var1.setEffectConnectedElement(Long.MIN_VALUE);
      var1.setLightConnectedElement(Long.MIN_VALUE);
   }

   public void removeConnectionIfNecessary(Vector3i var1, Vector3i var2, short var3) {
      ControlBlockElementCollectionManager var4;
      if (var3 != this.controllingId && this.controllingId != 32767) {
         if (ElementKeyMap.isValidType(this.controllerId) && ElementKeyMap.getInfo(this.controllerId).isCombiConnectAny(var3)) {
            if ((var4 = (ControlBlockElementCollectionManager)this.getCollectionManagersMap().get(ElementCollection.getIndex(var1))) != null) {
               FastCopyLongOpenHashSet var5;
               if (ElementKeyMap.getInfo(this.controllerId).isCombiConnectSupport(var3)) {
                  if (this.getSegmentController() instanceof Ship && var4.getSlaveConnectedElementRaw() != Long.MIN_VALUE && ((var5 = (FastCopyLongOpenHashSet)this.getSegmentController().getControlElementMap().getControllingMap().getAll().get(ElementCollection.getIndex(Ship.core))) == null || !var5.contains(var4.getSlaveConnectedElementRaw()))) {
                     this.getSegmentController().getControlElementMap().addControllerForElement(ElementCollection.getIndex(Ship.core), ElementCollection.getPosIndexFrom4(var4.getSlaveConnectedElementRaw()), (short)ElementCollection.getType(var4.getSlaveConnectedElementRaw()));
                  }

                  var4.setSlaveConnectedElement(Long.MIN_VALUE);
                  this.notifyObservers(SegNotifyType.SHIP_ELEMENT_CHANGED);
                  if (this.getState() instanceof GameClientState) {
                     ((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().getWeaponManagerPanel().setReconstructionRequested(true);
                  }

                  return;
               }

               if (ElementKeyMap.getInfo(this.controllerId).isCombiConnectEffect(var3)) {
                  if (this.getSegmentController() instanceof Ship && var4.getEffectConnectedElementRaw() != Long.MIN_VALUE && ((var5 = (FastCopyLongOpenHashSet)this.getSegmentController().getControlElementMap().getControllingMap().getAll().get(ElementCollection.getIndex(Ship.core))) == null || !var5.contains(var4.getEffectConnectedElementRaw()))) {
                     this.getSegmentController().getControlElementMap().addControllerForElement(ElementCollection.getIndex(16, 16, 16), ElementCollection.getPosIndexFrom4(var4.getEffectConnectedElementRaw()), (short)ElementCollection.getType(var4.getEffectConnectedElementRaw()));
                  }

                  var4.setEffectConnectedElement(Long.MIN_VALUE);
                  this.notifyObservers(SegNotifyType.SHIP_ELEMENT_CHANGED);
                  if (this.getState() instanceof GameClientState) {
                     ((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().getWeaponManagerPanel().setReconstructionRequested(true);
                  }

                  return;
               }

               if (ElementKeyMap.getInfo(this.controllerId).isLightConnect(var3)) {
                  var4.setLightConnectedElement(Long.MIN_VALUE);
                  this.notifyObservers(SegNotifyType.SHIP_ELEMENT_CHANGED);
                  if (this.getState() instanceof GameClientState) {
                     ((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().getWeaponManagerPanel().setReconstructionRequested(true);
                  }

                  return;
               }
            }

         }
      } else if ((var4 = (ControlBlockElementCollectionManager)this.getCollectionManagersMap().get(ElementCollection.getIndex(var1))) != null) {
         var4.remove(var2);
         this.notifyObservers(SegNotifyType.SHIP_ELEMENT_CHANGED);
      }
   }

   protected boolean convertDeligateControls(ControllerStateInterface var1, Vector3i var2, Vector3i var3) throws IOException {
      var1.getParameter(var2);
      var1.getParameter(var3);
      if (var1.getPlayerState() == null) {
         return true;
      } else {
         SegmentPiece var6;
         if ((var6 = this.getSegmentBuffer().getPointUnsave(var3)) == null) {
            return false;
         } else {
            if (var6.getType() == 1) {
               try {
                  SlotAssignment var7 = this.checkShipConfig(var1);
                  int var4 = var1.getPlayerState().getCurrentShipControllerSlot();
                  if (var1.getPlayerState() != null && !var7.hasConfigForSlot(var4)) {
                     return false;
                  }

                  var3.set(var7.get(var4));
               } catch (ShipConfigurationNotFoundException var5) {
                  return false;
               }
            }

            return true;
         }
      }
   }

   public List getCollectionManagers() {
      return this.collectionManagers;
   }

   public Long2ObjectOpenHashMap getCollectionManagersMap() {
      return this.collectionManagerMap;
   }

   public final boolean hasMetaData() {
      return this instanceof TagModuleUsableInterface;
   }

   public void onAddedCollection(long var1, ControlBlockElementCollectionManager var3) {
   }

   public void onConnectionRemoved(long var1, ControlBlockElementCollectionManager var3) {
   }

   public Tag toTagStructure() {
      ObjectArrayList var1 = new ObjectArrayList(this.getCollectionManagers().size());

      for(int var2 = 0; var2 < this.getCollectionManagers().size(); ++var2) {
         if (((ControlBlockElementCollectionManager)this.getCollectionManagers().get(var2)).hasTag()) {
            var1.add(((ControlBlockElementCollectionManager)this.getCollectionManagers().get(var2)).toTagStructure());
         }
      }

      Tag[] var4 = new Tag[var1.size() + 1];

      for(int var3 = 0; var3 < var1.size(); ++var3) {
         var4[var3] = (Tag)var1.get(var3);
      }

      assert ((TagModuleUsableInterface)this).getTagId() != null : this.getClass();

      var4[var1.size()] = FinishTag.INST;
      return new Tag(Tag.Type.STRUCT, ((TagModuleUsableInterface)this).getTagId(), var4);
   }

   public void flagCheckUpdatable() {
      int var1;
      if ((var1 = this.collectionManagers.size()) > 10000) {
         System.err.println("Exception: Entity " + this.getSegmentController() + " has abnormal amount of collectionManagers: " + var1);
      }

      if (var1 > 1000000) {
         System.err.println("Exception: FATAL! Entity " + this.getSegmentController() + " has abnormal amount of collectionManagers: " + var1 + " Entity is being removed now!");
         this.setUpdatable(false);
         this.getSegmentController().destroyPersistent();
      } else {
         for(int var2 = 0; var2 < var1; ++var2) {
            if (((ControlBlockElementCollectionManager)this.collectionManagers.get(var2)).isStructureUpdateNeeded()) {
               this.setUpdatable(true);
               return;
            }
         }

         this.setUpdatable(false);
      }
   }

   public boolean isUsingRegisteredActivation() {
      return false;
   }
}
