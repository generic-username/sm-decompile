package org.schema.game.common.controller.elements.scanner;

import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import java.util.Iterator;
import org.schema.common.FastMath;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.cloaking.CloakingElementManager;
import org.schema.game.common.controller.elements.effectblock.EffectElementManager;
import org.schema.game.common.controller.elements.jamming.JammingElementManager;
import org.schema.game.common.controller.elements.power.PowerAddOn;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.remote.RemoteValueUpdate;
import org.schema.game.network.objects.valueUpdate.NTValueUpdateInterface;
import org.schema.game.network.objects.valueUpdate.ScanChargeValueUpdate;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.tag.Tag;

public class ScannerCollectionManager extends ControlBlockElementCollectionManager {
   private float initialCharge;
   private long lastSent;
   private long lastSentZero;
   private boolean hideChargedMessage;

   public ScannerCollectionManager(SegmentPiece var1, SegmentController var2, ScannerElementManager var3) {
      super(var1, (short)655, var2, var3);
   }

   protected void applyMetaData(BlockMetaDataDummy var1) {
      assert this.initialCharge == 0.0F;

      this.initialCharge = ((ScannerMetaDataDummy)var1).charge;
   }

   public void updateStructure(long var1) {
      LongOpenHashSet var3;
      if (this.getSegmentController().isOnServer() && this.initialCharge > 0.0F && (var3 = (LongOpenHashSet)this.getSegmentController().getControlElementMap().getControllingMap().getAll().get(ElementCollection.getIndex(this.getControllerPos()))) != null && var3.size() <= this.getTotalSize()) {
         this.setCharge(this.initialCharge);
         this.initialCharge = 0.0F;
         this.sendChargeUpdate();
      }

      super.updateStructure(var1);
   }

   protected Tag toTagStructurePriv() {
      return new Tag(Tag.Type.FLOAT, (String)null, this.getCharge());
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return ScannerUnit.class;
   }

   public boolean needsUpdate() {
      return true;
   }

   public ScannerUnit getInstance() {
      return new ScannerUnit();
   }

   protected void onChangedCollection() {
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
      }

      if (this.getSegmentController().isOnServer()) {
         this.setCharge(0.0F);
         this.sendChargeUpdate();
      }

   }

   public void update(Timer var1) {
      super.update(var1);
      this.chargeUpAutomatically(var1);
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      int var1;
      for(var1 = 0; var1 < this.getElementCollections().size(); ++var1) {
         ((ScannerUnit)this.getElementCollections().get(var1)).getPowerConsumption();
      }

      var1 = (int)FastMath.ceil((float)((double)this.getSegmentController().getTotalElements() * (double)ScannerElementManager.RATIO_NEEDED_TO_TOTAL));
      int var2 = Math.max(0, this.getTotalSize() - var1);
      float var3 = (float)(this.getTotalSize() - var2) * ScannerElementManager.CHARGE_NEEDED_FOR_SCAN_PER_BLOCK;
      float var4 = (float)var2 * ScannerElementManager.CHARGE_NEEDED_FOR_SCAN_PER_BLOCK_AFTER_RATIO;
      return new GUIKeyValueEntry[]{new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SCANNER_SCANNERCOLLECTIONMANAGER_5, this.getChargeNeededForScan()), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SCANNER_SCANNERCOLLECTIONMANAGER_6, ScannerElementManager.CHARGE_NEEDED_FOR_SCAN_PER_BLOCK + " x " + (this.getTotalSize() - var2) + " = " + var3), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SCANNER_SCANNERCOLLECTIONMANAGER_7, ScannerElementManager.CHARGE_NEEDED_FOR_SCAN_PER_BLOCK_AFTER_RATIO + " x " + var2 + " = " + var4), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SCANNER_SCANNERCOLLECTIONMANAGER_8, this.getChargeAddedPerSec()), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SCANNER_SCANNERCOLLECTIONMANAGER_9, ScannerElementManager.RATIO_NEEDED_TO_TOTAL), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SCANNER_SCANNERCOLLECTIONMANAGER_10, this.getTotalSize() + " / " + var1)};
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SCANNER_SCANNERCOLLECTIONMANAGER_11;
   }

   public float getChargeAddedPerSec() {
      return ScannerElementManager.CHARGE_ADDED_PER_SECOND + ScannerElementManager.CHARGE_ADDED_PER_SECOND_PER_BLOCK * (float)this.getTotalSize();
   }

   public void charge(Timer var1) {
      PowerAddOn var2 = ((PowerManagerInterface)this.getContainer()).getPowerAddOn();
      if (this.getTotalSize() > 0) {
         float var3 = var1.getDelta() * this.getChargeAddedPerSec();
         if (var2.consumePowerInstantly((double)var3)) {
            float var4 = this.getCharge();
            this.setCharge(Math.min(this.getChargeNeededForScan(), this.getCharge() + var3));
            if (this.isCharged() && var4 < this.getCharge() && !this.hideChargedMessage && this.getState() instanceof GameClientState && !((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().isTreeActive()) {
               this.getSegmentController().popupOwnClientMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SCANNER_SCANNERCOLLECTIONMANAGER_12, 1);
               this.hideChargedMessage = true;
               this.sendChargeUpdate();
            }

            return;
         }

         if (this.getSegmentController().isClientOwnObject() && ((GameClientState)this.getState()).getWorldDrawer() != null) {
            ((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().notifyEffectHit(this.getSegmentController(), EffectElementManager.OffensiveEffects.NO_POWER);
         }
      }

   }

   public void scanOnServer(PlayerState var1) {
      assert this.getSegmentController().isOnServer();

      if (this.getSegmentController().isOnServer()) {
         if (this.isCharged()) {
            this.setCharge(0.0F);
            this.sendChargeUpdate();
            this.getSegmentController().sendControllingPlayersServerMessage(new Object[]{59, var1.getCurrentSystem()}, 1);
            System.err.println("[SERVER][SCAN] finding scannable...");
            Iterator var2 = this.getSegmentController().getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

            label55:
            while(true) {
               while(true) {
                  Sendable var3;
                  Ship var5;
                  do {
                     do {
                        if (!var2.hasNext()) {
                           System.err.println("[SERVER][SCAN] finding scannables done...");
                           ((GameServerState)this.getSegmentController().getState()).scanOnServer(this, var1);
                           this.hideChargedMessage = false;
                           break label55;
                        }
                     } while(!((var3 = (Sendable)var2.next()) instanceof Ship));
                  } while(!(var5 = (Ship)var3).isNeighbor(this.getSegmentController().getSectorId(), var5.getSectorId()));

                  boolean var4 = false;
                  if ((var5.isCloakedFor((SimpleTransformableSendableObject)null) || var5.isJammingFor((SimpleTransformableSendableObject)null)) && var5.getMass() <= (float)this.getTotalSize() * ScannerElementManager.ECM_STRENGTH_MOD) {
                     var5.getManagerContainer().getCloakElementManager().stopCloak(CloakingElementManager.REUSE_DELAY_ON_SCAN_MS);
                     var5.getManagerContainer().getJammingElementManager().stopJamming(JammingElementManager.REUSE_DELAY_ON_SCAN_MS);
                     var4 = true;
                  }

                  if (var4) {
                     var5.sendControllingPlayersServerMessage(new Object[]{60}, 3);
                     this.getSegmentController().sendControllingPlayersServerMessage(new Object[]{61, var5.toNiceString()}, 1);
                  } else if (var5.isCloakedFor((SimpleTransformableSendableObject)null) || var5.isJammingFor((SimpleTransformableSendableObject)null)) {
                     this.getSegmentController().sendControllingPlayersServerMessage(new Object[]{62}, 3);
                  }
               }
            }
         } else if (System.currentTimeMillis() - this.lastSent > 3000L) {
            this.getSegmentController().sendControllingPlayersServerMessage(new Object[]{63, StringTools.formatPointZero(this.getCharge()), StringTools.formatPointZero(this.getChargeNeededForScan())}, 3);
            this.lastSent = System.currentTimeMillis();
         }
      }

      ((ScannerElementManager)this.getElementManager()).setLastScan(System.currentTimeMillis());
   }

   private void chargeUpAutomatically(Timer var1) {
      if (System.currentTimeMillis() - ((ScannerElementManager)this.getElementManager()).getLastScan() > ScannerElementManager.RELOAD_AFTER_USE_MS && !this.isCharged()) {
         this.charge(var1);
      }

   }

   public boolean isCharged() {
      return this.getCharge() >= this.getChargeNeededForScan();
   }

   public float getChargeNeededForScan() {
      int var1 = (int)FastMath.ceil((float)((double)this.getSegmentController().getTotalElements() * (double)ScannerElementManager.RATIO_NEEDED_TO_TOTAL));
      var1 = Math.max(0, this.getTotalSize() - var1);
      float var2 = (float)(this.getTotalSize() - var1) * ScannerElementManager.CHARGE_NEEDED_FOR_SCAN_PER_BLOCK;
      float var5 = (float)var1 * ScannerElementManager.CHARGE_NEEDED_FOR_SCAN_PER_BLOCK_AFTER_RATIO;
      double var3 = 0.0D;
      if ((double)this.getTotalSize() / (double)this.getSegmentController().getTotalElements() < (double)ScannerElementManager.RATIO_NEEDED_TO_TOTAL) {
         var3 = 1.0D - (double)this.getTotalSize() / (double)this.getSegmentController().getTotalElements() / (double)ScannerElementManager.RATIO_NEEDED_TO_TOTAL;
      }

      return (float)((double)(var5 += ScannerElementManager.CHARGE_NEEDED_FOR_SCAN_FIX + var2) + var3 * (double)var5);
   }

   public float getCharge() {
      return this.getContainer().floatValueMap.get(ElementCollection.getIndex(this.getControllerPos()));
   }

   public void setCharge(float var1) {
      this.getContainer().floatValueMap.put(ElementCollection.getIndex(this.getControllerPos()), var1);
   }

   public void sendChargeUpdate() {
      ScanChargeValueUpdate var1;
      (var1 = new ScanChargeValueUpdate()).setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), this.getControllerElement().getAbsoluteIndex());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   public void hasHit(double var1, DamageDealerType var3) {
      if (this.getSegmentController().isOnServer() && this.getCharge() > 0.0F && System.currentTimeMillis() - this.lastSentZero > 5000L) {
         this.setCharge(0.0F);
         this.sendChargeUpdate();
         this.lastSentZero = System.currentTimeMillis();
      }

   }

   public float getSensorValue(SegmentPiece var1) {
      return Math.min(1.0F, this.getCharge() / Math.max(1.0E-4F, this.getChargeNeededForScan()));
   }
}
