package org.schema.game.common.controller.elements.scanner;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import java.io.IOException;
import javax.vecmath.Vector3f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.elements.BlockActivationListenerInterface;
import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.HittableInterface;
import org.schema.game.common.controller.elements.ManagerReloadInterface;
import org.schema.game.common.controller.elements.NTReceiveInterface;
import org.schema.game.common.controller.elements.NTSenderInterface;
import org.schema.game.common.controller.elements.SupportElementManagerInterface;
import org.schema.game.common.controller.elements.TagModuleUsableInterface;
import org.schema.game.common.controller.elements.UsableControllableFireingElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.SimplePlayerCommands;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.InputState;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.server.ServerMessage;

public class ScannerElementManager extends UsableControllableFireingElementManager implements BlockActivationListenerInterface, HittableInterface, ManagerReloadInterface, NTReceiveInterface, NTSenderInterface, SupportElementManagerInterface, TagModuleUsableInterface {
   public static final String TAG_ID = "SC";
   @ConfigurationElement(
      name = "ChargeNeededForScan"
   )
   public static float CHARGE_NEEDED_FOR_SCAN_FIX = 1000.0F;
   @ConfigurationElement(
      name = "ChargeNeededForScanPerBlock"
   )
   public static float CHARGE_NEEDED_FOR_SCAN_PER_BLOCK = 1000.0F;
   @ConfigurationElement(
      name = "ChargeNeededForScanPerBlockAfterRatioMet"
   )
   public static float CHARGE_NEEDED_FOR_SCAN_PER_BLOCK_AFTER_RATIO = 1000.0F;
   @ConfigurationElement(
      name = "DefaultScanDistance"
   )
   public static float DEFAULT_SCAN_DISTANCE = 4.0F;
   @ConfigurationElement(
      name = "EnemySystemDistanceMult"
   )
   public static float ENEMY_SYSTEM_DISTANCE_MULT = 4.0F;
   @ConfigurationElement(
      name = "AllySystemDistanceMult"
   )
   public static float SLLY_SYSTEM_DISTANCE_MULT = 4.0F;
   @ConfigurationElement(
      name = "ReloadMsAfterUseMs"
   )
   public static long RELOAD_AFTER_USE_MS = 1000L;
   @ConfigurationElement(
      name = "ChargeAddedPerSecond"
   )
   public static float CHARGE_ADDED_PER_SECOND = 10.0F;
   @ConfigurationElement(
      name = "ChargeAddedPerSecondPerBlock"
   )
   public static float CHARGE_ADDED_PER_SECOND_PER_BLOCK = 10.0F;
   @ConfigurationElement(
      name = "RatioNeededToTotalBlockCount"
   )
   public static float RATIO_NEEDED_TO_TOTAL = 0.1F;
   @ConfigurationElement(
      name = "ECMStrengthMod"
   )
   public static float ECM_STRENGTH_MOD = 10.0F;
   public static boolean debug = false;
   private Vector3f shootingDirTemp = new Vector3f();
   private Vector3f shootingUpTemp = new Vector3f();
   private Vector3f shootingRightTemp = new Vector3f();
   private Vector3f shootingForwardTemp = new Vector3f();
   private Vector3i controlledFromOrig = new Vector3i();
   private Vector3i controlledFrom = new Vector3i();
   private long lastScan;

   public ScannerElementManager(SegmentController var1) {
      super((short)654, (short)655, var1);
   }

   public String getTagId() {
      return "SC";
   }

   public ControllerManagerGUI getGUIUnitValues(ScannerUnit var1, ScannerCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SCANNER_SCANNERELEMENTMANAGER_12, var1);
   }

   protected String getTag() {
      return "scanner";
   }

   public ScannerCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new ScannerCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SCANNER_SCANNERELEMENTMANAGER_13;
   }

   protected void playSound(ScannerUnit var1, Transform var2) {
      ((GameClientController)this.getState().getController()).queueTransformableAudio("0022_spaceship user - laser gun single fire small", var2, 0.99F);
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
      System.currentTimeMillis();
      if (!var1.isFlightControllerActive()) {
         if (debug) {
            System.err.println("NOT ACTIVE");
         }

      } else if (this.getCollectionManagers().isEmpty()) {
         if (debug) {
            System.err.println("NO WEAPONS");
         }

      } else {
         try {
            if (!this.convertDeligateControls(var1, this.controlledFromOrig, this.controlledFrom)) {
               if (debug) {
                  System.err.println("NO SLOT");
               }

               return;
            }
         } catch (IOException var4) {
            var4.printStackTrace();
            return;
         }

         this.getPowerManager().sendNoPowerHitEffectIfNeeded();
         if (debug) {
            System.err.println("FIREING CONTROLLERS: " + this.getState() + ", " + this.getCollectionManagers().size() + " FROM: " + this.controlledFrom);
         }

         if (this.getCollectionManagers().size() > 0 && !this.getSegmentController().isOnServer()) {
            ScannerCollectionManager var5 = (ScannerCollectionManager)this.getCollectionManagers().get(0);
            if (var1.isSelected(var5.getControllerElement(), this.controlledFrom)) {
               boolean var3 = this.controlledFromOrig.equals(this.controlledFrom) | this.getControlElementMap().isControlling(this.controlledFromOrig, var5.getControllerPos(), this.controllerId);
               if (debug) {
                  System.err.println("Controlling " + var3 + " " + this.getState());
               }

               if (var3) {
                  if (this.controlledFromOrig.equals(Ship.core)) {
                     var1.getControlledFrom(this.controlledFromOrig);
                  }

                  if (var1.getPlayerState() != null && var1.getPlayerState() == ((GameClientState)this.getState()).getPlayer() && !var5.getElementCollections().isEmpty() && this.clientIsOwnShip() && (var1.isMouseButtonDown(0) || var1.isMouseButtonDown(1))) {
                     this.checkScan(var1.getPlayerState());
                  } else {
                     if (this.clientIsOwnShip() && var1.getPlayerState() == null) {
                        System.err.println("[CLIENT] Exception: player state null: " + var1.getPlayerState());
                     }

                     if (this.clientIsOwnShip() && var1.getPlayerState() != ((GameClientState)this.getState()).getPlayer()) {
                        System.err.println("[CLIENT] Exception: player state is different: " + var1.getPlayerState() + " vs: " + ((GameClientState)this.getState()).getPlayer());
                     }
                  }
               }
            }
         }

         if (this.getCollectionManagers().isEmpty() && this.clientIsOwnShip()) {
            ((GameClientState)this.getState()).getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SCANNER_SCANNERELEMENTMANAGER_1, 0.0F);
         }

      }
   }

   public void executeScanOnServer(PlayerState var1) {
      if (this.getCollectionManagers().size() > 0) {
         ScannerCollectionManager var2;
         if ((var2 = (ScannerCollectionManager)this.getCollectionManagers().get(0)).isCharged()) {
            var2.scanOnServer(var1);
         } else {
            var1.sendServerMessage(new ServerMessage(new Object[]{64, var2.getCharge(), var2.getChargeNeededForScan()}, 3, var1.getId()));
         }
      } else {
         var1.sendServerMessage(new ServerMessage(new Object[]{65}, 3, var1.getId()));
      }
   }

   public void checkScan(PlayerState var1) {
      if (System.currentTimeMillis() - this.lastScan > 500L) {
         if (this.getCollectionManagers().size() > 0) {
            ScannerCollectionManager var2;
            if ((var2 = (ScannerCollectionManager)this.getCollectionManagers().get(0)).isCharged()) {
               System.err.println("[CLIENT][SCAN] " + this.getSegmentController() + " SENDING SIMPLE COMMAND");
               var1.sendSimpleCommand(SimplePlayerCommands.SCAN, this.getSegmentController().getId());
               ((GameClientState)this.getState()).getController().getClientChannel().getGalaxyManagerClient().resetClientVisibilitySystem(var1.getCurrentSystem());
               this.lastScan = System.currentTimeMillis() + 2000L;
               return;
            }

            if (var1.isClientOwnPlayer()) {
               ((GameClientState)this.getState()).getController().popupInfoTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SCANNER_SCANNERELEMENTMANAGER_4, var2.getCharge(), var2.getChargeNeededForScan()), 0.0F);
            }

            this.lastScan = System.currentTimeMillis();
            return;
         }

         if (var1.isClientOwnPlayer()) {
            ((GameClientState)this.getState()).getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SCANNER_SCANNERELEMENTMANAGER_5, 0.0F);
         }

         this.lastScan = System.currentTimeMillis();
      }

   }

   public int onActivate(SegmentPiece var1, boolean var2, boolean var3) {
      return var3 ? 1 : 0;
   }

   public void updateActivationTypes(ShortOpenHashSet var1) {
   }

   public void updateFromNT(NetworkObject var1) {
   }

   public void updateToFullNT(NetworkObject var1) {
      this.getSegmentController().isOnServer();
   }

   public String getReloadStatus(long var1) {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SCANNER_SCANNERELEMENTMANAGER_14;
   }

   public void drawReloads(Vector3i var1, Vector3i var2, long var3) {
      ScannerCollectionManager var5;
      if ((var5 = (ScannerCollectionManager)this.getCollectionManagersMap().get(var3)) != null) {
         float var6 = var5.getCharge() / var5.getChargeNeededForScan();
         drawReload((InputState)this.getState(), var1, var2, reloadColor, false, var6);
      }

   }

   public void onHit(long var1, short var3, double var4, DamageDealerType var6) {
      if (this.getSegmentController().isOnServer()) {
         for(int var7 = 0; var7 < this.getCollectionManagers().size(); ++var7) {
            ((ScannerCollectionManager)this.getCollectionManagers().get(var7)).hasHit(var4, var6);
         }
      }

   }

   public long getLastScan() {
      return this.lastScan;
   }

   public void setLastScan(long var1) {
      this.lastScan = var1;
   }

   public double calculateSupportIndex() {
      double var1 = 0.0D;

      for(int var3 = 0; var3 < this.getCollectionManagers().size(); ++var3) {
         var1 += (double)((ScannerCollectionManager)this.getCollectionManagers().get(var3)).getTotalSize();
      }

      return var1;
   }

   public double calculateSupportPowerConsumptionPerSecondIndex() {
      double var1 = 0.0D;

      for(int var3 = 0; var3 < this.getCollectionManagers().size(); ++var3) {
         var1 += (double)((ScannerCollectionManager)this.getCollectionManagers().get(var3)).getChargeNeededForScan();
      }

      return var1;
   }

   public BlockMetaDataDummy getDummyInstance() {
      return new ScannerMetaDataDummy();
   }
}
