package org.schema.game.common.controller.elements.scanner;

import org.schema.game.common.controller.elements.ActiveChargeValueUpdate;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.RecharchableActivatableDurationSingleModule;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;

public class ScanAddOnChargeValueUpdate extends ActiveChargeValueUpdate {
   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.SCAN_CHARGE_REACTOR;
   }

   public RecharchableActivatableDurationSingleModule getModule(ManagerContainer var1) {
      return var1.getScanAddOn();
   }
}
