package org.schema.game.common.controller.elements;

import org.schema.common.util.linAlg.Vector3i;

public class Destination {
   public String uid = "none";
   public Vector3i local = new Vector3i();

   public int hashCode() {
      return this.uid.hashCode() + this.local.hashCode();
   }

   public boolean equals(Object var1) {
      Destination var2;
      return (var2 = (Destination)var1).uid.equals(this.uid) && var2.local.equals(this.local);
   }
}
