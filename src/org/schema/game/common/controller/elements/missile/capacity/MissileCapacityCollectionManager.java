package org.schema.game.common.controller.elements.missile.capacity;

import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.elements.BlockKillInterface;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;

public class MissileCapacityCollectionManager extends ElementCollectionManager implements BlockKillInterface {
   private float totalThrust;
   private float totalThrustRaw;

   public MissileCapacityCollectionManager(SegmentController var1, MissileCapacityElementManager var2) {
      super((short)362, var1, var2);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return MissileCapacityUnit.class;
   }

   public boolean needsUpdate() {
      return false;
   }

   public MissileCapacityUnit getInstance() {
      return new MissileCapacityUnit();
   }

   protected void onFinishedCollection() {
      super.onFinishedCollection();
      float var1 = ((MissileCapacityElementManager)this.getElementManager()).getMissileCapacityMax();
      float var2 = (float)this.getTotalSize();
      float var3;
      switch(MissileCapacityElementManager.MISSILE_CAPACITY_CALC_STYLE) {
      case LINEAR:
         var2 = MissileCapacityElementManager.MISSILE_CAPACITY_BASIC + var2 * MissileCapacityElementManager.MISSILE_CAPACITY_PER_BLOCK;
         break;
      case EXP:
         var2 = MissileCapacityElementManager.MISSILE_CAPACITY_BASIC + Math.max(0.0F, (float)Math.pow((double)var2, (double)MissileCapacityElementManager.MISSILE_CAPACITY_EXP) * MissileCapacityElementManager.MISSILE_CAPACITY_EXP_MULT);
         break;
      case DOUBLE_EXP:
         var3 = MissileCapacityElementManager.MISSILE_CAPACITY_EXP_THRESHOLD * (float)Math.pow((double)Math.min(var2 / MissileCapacityElementManager.MISSILE_CAPACITY_EXP_THRESHOLD, 1.0F), (double)MissileCapacityElementManager.MISSILE_CAPACITY_EXP_FIRST_HALF) * MissileCapacityElementManager.MISSILE_CAPACITY_EXP_MULT_FIRST_HALF;
         var2 = (float)Math.pow((double)Math.max(var2 - MissileCapacityElementManager.MISSILE_CAPACITY_EXP_THRESHOLD, 0.0F), (double)MissileCapacityElementManager.MISSILE_CAPACITY_EXP_SECOND_HALF) * MissileCapacityElementManager.MISSILE_CAPACITY_EXP_MULT_SECOND_HALF;
         var2 += MissileCapacityElementManager.MISSILE_CAPACITY_BASIC + var3;
         break;
      case LOG:
         var2 = MissileCapacityElementManager.MISSILE_CAPACITY_BASIC + Math.max(0.0F, ((float)Math.log10((double)var2) + MissileCapacityElementManager.MISSILE_CAPACITY_LOG_OFFSET) * MissileCapacityElementManager.MISSILE_CAPACITY_LOG_FACTOR);
         break;
      default:
         throw new RuntimeException("Illegal calc style " + VoidElementManager.SHIELD_LOCAL_RADIUS_CALC_STYLE);
      }

      ((MissileCapacityElementManager)this.getElementManager()).setMissileCapacityMax(var2);
      if (this.getSegmentController().isOnServer() && ((MissileCapacityElementManager)this.getElementManager()).getMissileCapacityMax() < var1) {
         var3 = ((MissileCapacityElementManager)this.getElementManager()).getMissileCapacity();
         ((MissileCapacityElementManager)this.getElementManager()).setMissileCapacity(Math.min(((MissileCapacityElementManager)this.getElementManager()).getMissileCapacity(), ((MissileCapacityElementManager)this.getElementManager()).getMissileCapacityMax()));
         if (var3 != ((MissileCapacityElementManager)this.getElementManager()).getMissileCapacity()) {
            ((MissileCapacityElementManager)this.getElementManager()).sendMissileCapacity();
         }
      }

   }

   public boolean isDetailedElementCollections() {
      return false;
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      this.getElementManager();
      return new GUIKeyValueEntry[0];
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_CAPACITY_MISSILECAPACITYCOLLECTIONMANAGER_0;
   }

   public float getSensorValue(SegmentPiece var1) {
      return Math.min(1.0F, this.getSegmentController().getMissileCapacity() / this.getSegmentController().getMissileCapacityMax());
   }

   public ElementCollectionManager.CollectionShape requiredNeigborsPerBlock() {
      return ElementCollectionManager.CollectionShape.ALL_IN_ONE;
   }

   public void onKilledBlock(long var1, short var3, Damager var4) {
      this.checkIntegrity(var1, var3, var4);
   }

   protected void onChangedCollection() {
   }
}
