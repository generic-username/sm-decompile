package org.schema.game.common.controller.elements.missile.capacity;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.element.ElementCollection;

public class MissileCapacityUnit extends ElementCollection {
   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((MissileCapacityElementManager)((MissileCapacityCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (MissileCapacityCollectionManager)this.elementCollectionManager, var2, var3);
   }
}
