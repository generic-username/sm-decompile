package org.schema.game.common.controller.elements.missile;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.ai.AIGameConfiguration;
import org.schema.game.common.controller.ai.SegmentControllerAIInterface;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.FocusableUsableModule;
import org.schema.game.common.data.SegmentPiece;

public abstract class MissileCollectionManager extends ControlBlockElementCollectionManager implements FocusableUsableModule {
   private FocusableUsableModule.FireMode mode = FocusableUsableModule.FireMode.getDefault(this.getClass());

   public MissileCollectionManager(SegmentPiece var1, short var2, SegmentController var3, MissileElementManager var4) {
      super(var1, var2, var3, var4);
   }

   public boolean isInFocusMode() {
      return this.mode == FocusableUsableModule.FireMode.FOCUSED;
   }

   public void setFireMode(FocusableUsableModule.FireMode var1) {
      this.mode = var1;
   }

   public FocusableUsableModule.FireMode getFireMode() {
      return this.mode;
   }

   public boolean isAllowedVolley() {
      return true;
   }

   public boolean isVolleyShot() {
      if (this.getSegmentController().isAIControlled() && this.getSegmentController() instanceof SegmentControllerAIInterface && ((SegmentControllerAIInterface)this.getSegmentController()).getAiConfiguration().isActiveAI() && ((AIGameConfiguration)((SegmentControllerAIInterface)this.getSegmentController()).getAiConfiguration()).get(Types.FIRE_MODE).getCurrentState().equals("Volley")) {
         return true;
      } else {
         return this.mode == FocusableUsableModule.FireMode.VOLLEY;
      }
   }
}
