package org.schema.game.common.controller.elements.missile;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import java.io.IOException;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.FastMath;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.BlockActivationListenerInterface;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.IntegrityBasedInterface;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.controller.elements.ShootingRespose;
import org.schema.game.common.controller.elements.UsableCombinableControllableElementManager;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.controller.elements.UsableControllableFireingElementManager;
import org.schema.game.common.controller.elements.UsableElementManager;
import org.schema.game.common.controller.elements.WeaponElementManagerInterface;
import org.schema.game.common.controller.elements.WeaponStatisticsData;
import org.schema.game.common.controller.elements.combination.CombinationAddOn;
import org.schema.game.common.controller.elements.combination.MissileCombiSettings;
import org.schema.game.common.controller.elements.combination.MissileCombinationAddOn;
import org.schema.game.common.controller.elements.combination.modifier.MissileUnitModifier;
import org.schema.game.common.controller.elements.effectblock.EffectCollectionManager;
import org.schema.game.common.controller.elements.missile.capacity.MissileCapacityElementManager;
import org.schema.game.common.controller.elements.missile.dumb.DumbMissileElementManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ShootContainer;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.controller.GameServerController;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public abstract class MissileElementManager extends UsableCombinableControllableElementManager implements BlockActivationListenerInterface, IntegrityBasedInterface, WeaponElementManagerInterface {
   private static boolean debug = false;
   private final short controlling;
   private final WeaponStatisticsData tmpOutput = new WeaponStatisticsData();
   private final ShootContainer shootContainer = new ShootContainer();
   private final MissileElementManager.MissileDrawReloadListener reloadListener = new MissileElementManager.MissileDrawReloadListener();
   private final MissileCombiSettings combiSettings = new MissileCombiSettings();

   public MissileElementManager(short var1, short var2, SegmentController var3) {
      super(var1, var2, var3);
      this.controlling = var2;
   }

   public MissileCombiSettings getCombiSettings() {
      return this.combiSettings;
   }

   protected int getMissileMode(MissileCollectionManager var1) {
      int var2 = 0;
      if (this.getAddOn() != null && var1.getSlaveConnectedElement() != Long.MIN_VALUE) {
         switch((short)ElementCollection.getType(var1.getSlaveConnectedElement())) {
         case 6:
            var2 = (int)((MissileUnitModifier)MissileCombinationAddOn.missileCannonUnitModifier.get((UsableElementManager)this)).modeModifier.getOuput(0.0F);
            break;
         case 38:
            var2 = (int)((MissileUnitModifier)MissileCombinationAddOn.missileMissileUnitModifier.get((UsableElementManager)this)).modeModifier.getOuput(0.0F);
            break;
         case 414:
            var2 = (int)((MissileUnitModifier)MissileCombinationAddOn.missileBeamUnitModifier.get((UsableElementManager)this)).modeModifier.getOuput(0.0F);
         }
      }

      return var2;
   }

   public void doShot(MissileUnit var1, MissileCollectionManager var2, ShootContainer var3, float var4, SimpleTransformableSendableObject var5, PlayerState var6, Timer var7) {
      ManagerModuleCollection var8 = null;
      short var9;
      if (var2.getEffectConnectedElement() != Long.MIN_VALUE) {
         var9 = (short)ElementCollection.getType(var2.getEffectConnectedElement());
         var8 = this.getManagerContainer().getModulesControllerMap().get(var9);
      }

      if (var2.getEffectConnectedElement() != Long.MIN_VALUE) {
         var9 = (short)ElementCollection.getType(var2.getEffectConnectedElement());
         var8 = this.getManagerContainer().getModulesControllerMap().get(var9);
         ControlBlockElementCollectionManager var10;
         if ((var10 = CombinationAddOn.getEffect(var2.getEffectConnectedElement(), var8, this.getSegmentController())) != null) {
            var2.setEffectTotal(var10.getTotalSize());
         }
      }

      if (this.getAddOn() != null && var2.getSlaveConnectedElement() != Long.MIN_VALUE) {
         var9 = (short)ElementCollection.getType(var2.getSlaveConnectedElement());
         ManagerModuleCollection var18 = this.getManagerContainer().getModulesControllerMap().get(var9);
         ShootingRespose var11 = this.handleAddOn(this, var2, var1, var18, var8, var3, var5, var6, var7, -1.0F);
         this.handleResponse(var11, var1, var3.weapontOutputWorldPos);
      } else if (var1.canUse(var7.currentTime, true)) {
         float var16 = var1.getPowerConsumption();
         if (!this.isUsingPowerReactors() && !this.consumePower(var16)) {
            this.handleResponse(ShootingRespose.NO_POWER, var1, var3.weapontOutputWorldPos);
         } else {
            Transform var14;
            (var14 = new Transform()).setIdentity();
            var14.origin.set(var3.weapontOutputWorldPos);
            var1.setStandardShotReloading();
            long var17 = var2.getLightConnectedElement();
            short var15 = 0;
            if (var17 != Long.MIN_VALUE) {
               var15 = (short)ElementCollection.getType(var17);
            }

            if (this.getSegmentController().isOnServer()) {
               assert var3.shootingDirTemp.lengthSquared() != 0.0F;

               long var12 = var2.getUsableId();
               this.addMissile(this.getSegmentController(), var14, new Vector3f(var3.shootingDirTemp), var4, var1.getDamage(), var1.getDistance(), var12, var5, var15);
            }

            this.handleResponse(ShootingRespose.FIRED, var1, var3.weapontOutputWorldPos);
         }
      } else {
         this.handleResponse(ShootingRespose.RELOADING, var1, var3.weapontOutputWorldPos);
      }
   }

   public boolean checkMissileCapacity() {
      SegmentController var1;
      if (this.getSegmentController() instanceof SegmentController && (var1 = this.getSegmentController().railController.getRoot()) instanceof ManagedSegmentController) {
         return ((ManagedSegmentController)var1).getManagerContainer().getMissileCapacity() >= 1.0F;
      } else {
         return false;
      }
   }

   protected abstract void addMissile(SegmentController var1, Transform var2, Vector3f var3, float var4, float var5, float var6, long var7, SimpleTransformableSendableObject var9, short var10);

   public int onActivate(SegmentPiece var1, boolean var2, boolean var3) {
      long var4 = var1.getAbsoluteIndex();

      for(int var8 = 0; var8 < this.getCollectionManagers().size(); ++var8) {
         Iterator var6 = ((MissileCollectionManager)this.getCollectionManagers().get(var8)).getElementCollections().iterator();

         while(var6.hasNext()) {
            MissileUnit var7;
            if ((var7 = (MissileUnit)var6.next()).contains(var4)) {
               var7.setMainPiece(var1, var3);
               if (var3) {
                  return 1;
               }

               return 0;
            }
         }
      }

      if (var3) {
         return 1;
      } else {
         return 0;
      }
   }

   public void updateActivationTypes(ShortOpenHashSet var1) {
      var1.add(this.controlling);
   }

   public MissileController getMissileController() {
      return ((GameServerController)this.getSegmentController().getState().getController()).getMissileController();
   }

   public abstract boolean isTargetLocking(MissileCollectionManager var1);

   public abstract boolean isHeatSeeking(MissileCollectionManager var1);

   public boolean isTargetLocking(SegmentPiece var1) {
      MissileCollectionManager var2;
      return var1 != null && (var2 = (MissileCollectionManager)this.getCollectionManagersMap().get(var1.getAbsoluteIndex())) != null ? this.isTargetLocking(var2) : false;
   }

   public boolean isHeatSeeking(SegmentPiece var1) {
      MissileCollectionManager var2;
      return var1 != null && (var2 = (MissileCollectionManager)this.getCollectionManagersMap().get(var1.getAbsoluteIndex())) != null ? this.isHeatSeeking(var2) : false;
   }

   public ControllerManagerGUI getGUIUnitValues(MissileUnit var1, MissileCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      this.getStatistics(var1, var2, var3, var4, this.tmpOutput);
      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_MISSILEELEMENTMANAGER_4, var1, new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_MISSILEELEMENTMANAGER_5, StringTools.formatPointZero(this.tmpOutput.damage)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_MISSILEELEMENTMANAGER_7, StringTools.formatPointZero(this.tmpOutput.speed)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_MISSILEELEMENTMANAGER_8, StringTools.formatPointZero(this.tmpOutput.distance)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_MISSILEELEMENTMANAGER_9, StringTools.formatPointZero(this.tmpOutput.reload)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_MISSILEELEMENTMANAGER_10, var1.getPowerConsumedPerSecondResting()), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_MISSILEELEMENTMANAGER_12, var1.getPowerConsumedPerSecondCharging()));
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
      if (!var1.isFlightControllerActive()) {
         if (debug) {
            System.err.println("NO 1");
         }

      } else if (this.getCollectionManagers().isEmpty()) {
         if (debug) {
            System.err.println("NO 2 " + this.getClass().getSimpleName() + "; " + var1.getClass().getSimpleName());
         }

      } else {
         try {
            if (!this.convertDeligateControls(var1, this.shootContainer.controlledFromOrig, this.shootContainer.controlledFrom)) {
               if (debug) {
                  System.err.println("NO 3");
               }

               return;
            }
         } catch (IOException var7) {
            var7.printStackTrace();
            return;
         }

         for(int var3 = 0; var3 < this.getCollectionManagers().size(); ++var3) {
            MissileCollectionManager var4 = (MissileCollectionManager)this.getCollectionManagers().get(var3);
            boolean var5 = var1.isSelected(var4.getControllerElement(), this.shootContainer.controlledFrom);
            boolean var6 = var1.isAISelected(var4.getControllerElement(), this.shootContainer.controlledFrom, var3, this.getCollectionManagers().size(), var4);
            if (!var5 && debug) {
               System.err.println("NO 4 " + this.getClass().getSimpleName() + "; " + var1.getClass().getSimpleName() + "; " + var4.getControllerPos() + "; " + this.shootContainer.controlledFrom);
            }

            if (var5 && var6) {
               if (!(var5 = this.shootContainer.controlledFromOrig.equals(this.shootContainer.controlledFrom) | this.getControlElementMap().isControlling(this.shootContainer.controlledFromOrig, var4.getControllerPos(), this.controllerId)) && debug) {
                  System.err.println("NO 4");
               }

               if (var5 && var4.allowedOnServerLimit()) {
                  if (this.shootContainer.controlledFromOrig.equals(Ship.core)) {
                     var1.getControlledFrom(this.shootContainer.controlledFromOrig);
                  }

                  if (debug) {
                     System.err.println("FIRE MISSILE " + var1.getClass());
                  }

                  var4.handleControlShot(var1, var2);
                  if (var4.getElementCollections().isEmpty() && this.clientIsOwnShip()) {
                     ((GameClientState)this.getState()).getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_MISSILEELEMENTMANAGER_1, 0.0F);
                  }
               }
            }
         }

         if (this.getCollectionManagers().isEmpty() && this.clientIsOwnShip()) {
            ((GameClientState)this.getState()).getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_MISSILEELEMENTMANAGER_3, 0.0F);
         }

      }
   }

   public void drawReloads(Vector3i var1, Vector3i var2, long var3) {
      this.handleReload(var1, var2, var3, this.reloadListener);
   }

   public WeaponStatisticsData getStatistics(MissileUnit var1, MissileCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4, WeaponStatisticsData var5) {
      if (var4 != null) {
         var2.setEffectTotal(var4.getTotalSize());
      } else {
         var2.setEffectTotal(0);
      }

      var5.damage = var1.getDamage();
      var5.speed = var1.getSpeed();
      var5.distance = var1.getDistance();
      var5.reload = var1.getReloadTimeMs();
      var5.powerConsumption = var1.getPowerConsumption();
      var5.split = 1.0F;
      var5.mode = 1.0F;
      var5.effectRatio = 0.0F;
      if (var3 != null) {
         MissileUnitModifier var6 = (MissileUnitModifier)this.getAddOn().getGUI(var2, var1, (ControlBlockElementCollectionManager)var3, var4);
         var5.mode = var6.outputMode;
         var5.damage = var6.outputDamage;
         var5.speed = var6.outputSpeed;
         var5.distance = var6.outputDistance;
         var5.reload = var6.outputReload;
         var5.powerConsumption = var6.outputPowerConsumption;
         var5.split = (float)var6.outputSplit;
      }

      if (var4 != null) {
         var4.getElementManager();
         var5.effectRatio = CombinationAddOn.getRatio(var2, var4);
      }

      return var5;
   }

   public double calculateWeaponDamageIndex() {
      double var1 = 0.0D;
      Iterator var3 = this.getCollectionManagers().iterator();

      while(var3.hasNext()) {
         MissileCollectionManager var4;
         ControlBlockElementCollectionManager var5 = (var4 = (MissileCollectionManager)var3.next()).getSupportCollectionManager();
         EffectCollectionManager var6 = var4.getEffectCollectionManager();

         for(Iterator var7 = var4.getElementCollections().iterator(); var7.hasNext(); var1 += (double)this.tmpOutput.damage / ((double)this.tmpOutput.reload / 1000.0D)) {
            MissileUnit var8 = (MissileUnit)var7.next();
            this.getStatistics(var8, var4, var5, var6, this.tmpOutput);
         }
      }

      return var1;
   }

   public double calculateWeaponRangeIndex() {
      double var1 = 0.0D;
      double var3 = 0.0D;
      Iterator var5 = this.getCollectionManagers().iterator();

      while(var5.hasNext()) {
         MissileCollectionManager var6;
         ControlBlockElementCollectionManager var7 = (var6 = (MissileCollectionManager)var5.next()).getSupportCollectionManager();
         EffectCollectionManager var8 = var6.getEffectCollectionManager();

         for(Iterator var9 = var6.getElementCollections().iterator(); var9.hasNext(); ++var3) {
            MissileUnit var10 = (MissileUnit)var9.next();
            this.getStatistics(var10, var6, var7, var8, this.tmpOutput);
            var1 += (double)this.tmpOutput.distance;
         }
      }

      if (var3 > 0.0D) {
         return var1 / var3;
      } else {
         return 0.0D;
      }
   }

   public double calculateWeaponHitPropabilityIndex() {
      double var1 = 0.0D;
      Iterator var3 = this.getCollectionManagers().iterator();

      while(var3.hasNext()) {
         MissileCollectionManager var4;
         ControlBlockElementCollectionManager var5 = (var4 = (MissileCollectionManager)var3.next()).getSupportCollectionManager();
         EffectCollectionManager var6 = var4.getEffectCollectionManager();

         for(Iterator var7 = var4.getElementCollections().iterator(); var7.hasNext(); var1 += (double)(DumbMissileElementManager.BASE_SPEED * this.tmpOutput.split) / ((double)this.tmpOutput.reload / 1000.0D)) {
            MissileUnit var8 = (MissileUnit)var7.next();
            this.getStatistics(var8, var4, var5, var6, this.tmpOutput);
         }
      }

      return var1;
   }

   public double calculateWeaponSpecialIndex() {
      Iterator var1 = this.getCollectionManagers().iterator();

      while(var1.hasNext()) {
         MissileCollectionManager var2;
         ControlBlockElementCollectionManager var3 = (var2 = (MissileCollectionManager)var1.next()).getSupportCollectionManager();
         EffectCollectionManager var4 = var2.getEffectCollectionManager();
         Iterator var5 = var2.getElementCollections().iterator();

         while(var5.hasNext()) {
            MissileUnit var6 = (MissileUnit)var5.next();
            this.getStatistics(var6, var2, var3, var4, this.tmpOutput);
         }
      }

      return 0.0D;
   }

   public double calculateWeaponPowerConsumptionPerSecondIndex() {
      double var1 = 0.0D;
      Iterator var3 = this.getCollectionManagers().iterator();

      while(var3.hasNext()) {
         MissileCollectionManager var4;
         ControlBlockElementCollectionManager var5 = (var4 = (MissileCollectionManager)var3.next()).getSupportCollectionManager();
         EffectCollectionManager var6 = var4.getEffectCollectionManager();

         MissileUnit var8;
         for(Iterator var7 = var4.getElementCollections().iterator(); var7.hasNext(); var1 += (double)var8.getPowerConsumption()) {
            var8 = (MissileUnit)var7.next();
            this.getStatistics(var8, var4, var5, var6, this.tmpOutput);
         }
      }

      return var1;
   }

   public class MissileDrawReloadListener implements UsableControllableFireingElementManager.ReloadListener {
      private GUITextOverlay chargesText;
      private short lastDraw;

      public String onDischarged(InputState var1, Vector3i var2, Vector3i var3, Vector4f var4, boolean var5, float var6) {
         if (this.chargesText == null) {
            this.chargesText = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM.getFont(), (InputState)MissileElementManager.this.getState());
            this.chargesText.onInit();
         }

         if (this.lastDraw != var1.getNumberOfUpdate()) {
            UsableControllableElementManager.drawReload(var1, var2, var3, var4, var5, var6, true, (float)((int)MissileElementManager.this.getSegmentController().getMissileCapacity()), (int)MissileElementManager.this.getSegmentController().getMissileCapacityMax(), -1L, this.chargesText);
            this.lastDraw = var1.getNumberOfUpdate();
         }

         return null;
      }

      public String onReload(InputState var1, Vector3i var2, Vector3i var3, Vector4f var4, boolean var5, float var6) {
         UsableControllableElementManager.drawReload(var1, var2, var3, var4, var5, var6);
         return null;
      }

      public String onFull(InputState var1, Vector3i var2, Vector3i var3, Vector4f var4, boolean var5, float var6, long var7) {
         return null;
      }

      public void drawForElementCollectionManager(InputState var1, Vector3i var2, Vector3i var3, Vector4f var4, long var5) {
         if (this.chargesText == null) {
            this.chargesText = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM.getFont(), (InputState)MissileElementManager.this.getState());
            this.chargesText.onInit();
         }

         boolean var7 = false;
         if (this.lastDraw != var1.getNumberOfUpdate()) {
            UsableControllableElementManager.drawReload(var1, var2, var3, UsableControllableFireingElementManager.reloadColor, false, 1.0F, true, (float)((int)MissileElementManager.this.getSegmentController().getMissileCapacity()), (int)MissileElementManager.this.getSegmentController().getMissileCapacityMax(), -1L, this.chargesText);
            this.lastDraw = var1.getNumberOfUpdate();
            var7 = true;
         }

         if (var7 && MissileElementManager.this.getSegmentController().getMissileCapacity() < (float)((int)MissileElementManager.this.getSegmentController().getMissileCapacityMax()) && MissileCapacityElementManager.MISSILE_CAPACITY_RELOAD_MODE == MissileCapacityElementManager.MissileCapacityReloadMode.ALL) {
            this.chargesText.setTextSimple(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_MISSILEELEMENTMANAGER_0, (int)FastMath.ceil(((ManagedSegmentController)MissileElementManager.this.getSegmentController()).getManagerContainer().getMissileCapacityTimer())));
            Vector3f var10000 = this.chargesText.getPos();
            var10000.x -= 10.0F;
            var10000 = this.chargesText.getPos();
            var10000.y -= 20.0F;
            UsableControllableElementManager.drawReloadText(var1, var2, var3, this.chargesText);
         }

      }
   }
}
