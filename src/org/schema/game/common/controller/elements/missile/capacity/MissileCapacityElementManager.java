package org.schema.game.common.controller.elements.missile.capacity;

import com.bulletphysics.linearmath.Transform;
import java.util.Iterator;
import org.schema.common.config.ConfigurationElement;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.RailDockingListener;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.controller.elements.UnitCalcStyle;
import org.schema.game.common.controller.elements.UsableControllableSingleElementManager;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.game.common.controller.rails.RailRelation;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.network.objects.remote.RemoteValueUpdate;
import org.schema.game.network.objects.valueUpdate.MissileCapacityValueUpdate;
import org.schema.game.network.objects.valueUpdate.NTValueUpdateInterface;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class MissileCapacityElementManager extends UsableControllableSingleElementManager implements RailDockingListener, PowerConsumer {
   private static final byte VERSION = 1;
   @ConfigurationElement(
      name = "BasicMissileCapacity"
   )
   public static float MISSILE_CAPACITY_BASIC = 0.0F;
   @ConfigurationElement(
      name = "MissileCapacityReloadMode"
   )
   public static MissileCapacityElementManager.MissileCapacityReloadMode MISSILE_CAPACITY_RELOAD_MODE;
   @ConfigurationElement(
      name = "MissileCapacityPerSec"
   )
   public static float MISSILE_CAPACITY_PER_SEC;
   @ConfigurationElement(
      name = "MissileCapacityReloadConstant"
   )
   public static float MISSILE_CAPACITY_RELOAD_CONSTANT;
   @ConfigurationElement(
      name = "MissileCapacityReloadResetOnFireManual"
   )
   public static boolean MISSILE_CAPACITY_RESET_ON_FIRE_MANUAL;
   @ConfigurationElement(
      name = "MissileCapacityReloadResetOnFireAI"
   )
   public static boolean MISSILE_CAPACITY_RESET_ON_FIRE_AI;
   @ConfigurationElement(
      name = "ReactorPowerConsumptionResting"
   )
   public static float POWER_CONSPUMTION_RESTING;
   @ConfigurationElement(
      name = "ReactorPowerConsumptionCharging"
   )
   public static float POWER_CONSPUMTION_CHARGING;
   @ConfigurationElement(
      name = "MissileCapacityCalcStyle",
      description = "LINEAR, EXP, LOG"
   )
   public static UnitCalcStyle MISSILE_CAPACITY_CALC_STYLE;
   @ConfigurationElement(
      name = "MissileCapacityPerBlock"
   )
   public static float MISSILE_CAPACITY_PER_BLOCK;
   @ConfigurationElement(
      name = "MissileCapacityExp"
   )
   public static float MISSILE_CAPACITY_EXP;
   @ConfigurationElement(
      name = "MissileCapacityExpMult"
   )
   public static float MISSILE_CAPACITY_EXP_MULT;
   @ConfigurationElement(
      name = "MissileCapacityExpFirstHalf"
   )
   public static float MISSILE_CAPACITY_EXP_FIRST_HALF;
   @ConfigurationElement(
      name = "MissileCapacityExpMultFirstHalf"
   )
   public static float MISSILE_CAPACITY_EXP_MULT_FIRST_HALF;
   @ConfigurationElement(
      name = "MissileCapacityExpThreshold"
   )
   public static float MISSILE_CAPACITY_EXP_THRESHOLD;
   @ConfigurationElement(
      name = "MissileCapacityExpSecondHalf"
   )
   public static float MISSILE_CAPACITY_EXP_SECOND_HALF;
   @ConfigurationElement(
      name = "MissileCapacityExpMultSecondHalf"
   )
   public static float MISSILE_CAPACITY_EXP_MULT_SECOND_HALF;
   @ConfigurationElement(
      name = "MissileCapacityLogFactor"
   )
   public static float MISSILE_CAPACITY_LOG_FACTOR;
   @ConfigurationElement(
      name = "MissileCapacityLogOffset"
   )
   public static float MISSILE_CAPACITY_LOG_OFFSET;
   private float powered;
   private float missileCapacity = 1.0F;
   private float missileCapacityMax = 1.0F;
   private boolean loadedFromTag;
   private float timer;

   public MissileCapacityElementManager(SegmentController var1) {
      super(var1, MissileCapacityCollectionManager.class);
      if (!var1.isOnServer()) {
         this.addObserver((DrawerObserver)var1.getState());
      }

      this.timer = MISSILE_CAPACITY_RELOAD_CONSTANT;
   }

   public void onControllerChange() {
   }

   public ControllerManagerGUI getGUIUnitValues(MissileCapacityUnit var1, MissileCapacityCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_CAPACITY_MISSILECAPACITYELEMENTMANAGER_0, var1);
   }

   public boolean canHandle(ControllerStateInterface var1) {
      return false;
   }

   protected String getTag() {
      return "missilecapacity";
   }

   public MissileCapacityCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new MissileCapacityCollectionManager(this.getSegmentController(), this);
   }

   protected void playSound(MissileCapacityUnit var1, Transform var2) {
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
   }

   public double getPowerConsumedPerSecondResting() {
      return (double)((float)this.totalSize * POWER_CONSPUMTION_RESTING);
   }

   public double getPowerConsumedPerSecondCharging() {
      return (double)((float)this.totalSize * POWER_CONSPUMTION_CHARGING);
   }

   public boolean isPowerCharging(long var1) {
      return this.getSegmentController().getMissileCapacity() < this.getSegmentController().getMissileCapacityMax();
   }

   public void setPowered(float var1) {
      this.powered = var1;
   }

   public float getPowered() {
      return this.powered;
   }

   public void reloadFromReactor(double var1, Timer var3, float var4, boolean var5, float var6) {
      float var7 = this.getMissileCapacity();
      if (MISSILE_CAPACITY_RELOAD_MODE == MissileCapacityElementManager.MissileCapacityReloadMode.SINGLE) {
         if (this.getMissileCapacity() < this.getMissileCapacityMax()) {
            this.setMissileCapacity(this.getMissileCapacity() + (float)var1 * MISSILE_CAPACITY_PER_SEC);
         }
      } else {
         assert MISSILE_CAPACITY_RELOAD_MODE == MissileCapacityElementManager.MissileCapacityReloadMode.ALL;

         if (this.getMissileCapacity() < this.getMissileCapacityMax()) {
            this.timer = (float)((double)this.timer - var1);
            if (this.timer <= 0.0F) {
               this.setMissileCapacity(this.getMissileCapacityMax());
               this.timer = this.getMissileCapacityReloadTime();
            }
         }
      }

      if (this.getSegmentController().isOnServer() && var7 != this.getMissileCapacity() && (this.getMissileCapacity() == 0.0F || this.getMissileCapacity() == this.getMissileCapacityMax())) {
         this.sendMissileCapacity();
      }

   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.MISSILES;
   }

   public boolean isPowerConsumerActive() {
      return true;
   }

   public String getName() {
      return "MissileCapacityElementManager";
   }

   public void dischargeFully() {
      this.setMissileCapacity(0.0F);
      if (this.getSegmentController().isOnServer()) {
         this.sendMissileCapacity();
      }

   }

   public void sendMissileCapacity() {
      assert this.getSegmentController().isOnServer();

      MissileCapacityValueUpdate var1 = new MissileCapacityValueUpdate();

      assert var1.getType() == ValueUpdate.ValTypes.MISSILE_CAPACITY_UPDATE;

      var1.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   public Tag toTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)1), new Tag(Tag.Type.FLOAT, (String)null, this.getMissileCapacity()), new Tag(Tag.Type.FLOAT, (String)null, this.getMissileCapacityMax()), new Tag(Tag.Type.FLOAT, (String)null, this.getMissileCapacityTimer()), FinishTag.INST});
   }

   public void readFromTag(Tag var1) {
      Tag[] var3;
      byte var2 = (var3 = var1.getStruct())[0].getByte();
      this.setMissileCapacity(var3[1].getFloat());
      this.setMissileCapacityMax(var3[2].getFloat());
      if (var2 > 0) {
         this.setMissileTimer(var3[3].getFloat());
      }

      this.loadedFromTag = true;
   }

   public float getMissileCapacityMax() {
      return this.missileCapacityMax;
   }

   public void setMissileCapacityMax(float var1) {
      this.missileCapacityMax = var1;
   }

   public float getMissileCapacity() {
      return this.missileCapacity;
   }

   public void setMissileCapacity(float var1) {
      this.missileCapacity = var1;
   }

   public float getMissileCapacityTimer() {
      return this.timer;
   }

   public void setMissileTimer(float var1) {
      this.timer = var1;
   }

   public float getMissileCapacityReloadTime() {
      return 0.0F + this.getMissileCapacityMax() * MISSILE_CAPACITY_PER_SEC + MISSILE_CAPACITY_RELOAD_CONSTANT;
   }

   public void dockingChanged(SegmentController var1, boolean var2) {
      if (this.isOnServer() && !var2) {
         label16: {
            if ((var1 = this.getSegmentController().railController.getRoot()) == this.getSegmentController()) {
               System.err.println("[SERVER][MISSILECAPACITY][WARNING] undocking reported docked to self");
            } else if (isThisOrAnyDockMissilesCapable(this.getSegmentController())) {
               float var3 = Math.min(this.getMissileCapacityMax(), var1.getMissileCapacity());
               this.setMissileCapacity(var3);
               break label16;
            }

            this.setMissileCapacity(0.0F);
         }

         this.sendMissileCapacity();
      }

   }

   private static boolean isThisOrAnyDockMissilesCapable(SegmentController var0) {
      if (((ManagedSegmentController)var0).getManagerContainer() instanceof ShipManagerContainer && ((ShipManagerContainer)((ManagedSegmentController)var0).getManagerContainer()).getMissile().getCollectionManagers().size() > 0) {
         return true;
      } else {
         if (!var0.railController.next.isEmpty()) {
            Iterator var1 = var0.railController.next.iterator();

            while(var1.hasNext()) {
               if (isThisOrAnyDockMissilesCapable(((RailRelation)var1.next()).docked.getSegmentController())) {
                  return true;
               }
            }
         }

         return false;
      }
   }

   static {
      MISSILE_CAPACITY_RELOAD_MODE = MissileCapacityElementManager.MissileCapacityReloadMode.ALL;
      MISSILE_CAPACITY_PER_SEC = 0.0F;
      MISSILE_CAPACITY_RELOAD_CONSTANT = 300.0F;
      MISSILE_CAPACITY_RESET_ON_FIRE_MANUAL = false;
      MISSILE_CAPACITY_RESET_ON_FIRE_AI = true;
      POWER_CONSPUMTION_RESTING = 0.0F;
      POWER_CONSPUMTION_CHARGING = 0.0F;
      MISSILE_CAPACITY_CALC_STYLE = UnitCalcStyle.LINEAR;
      MISSILE_CAPACITY_PER_BLOCK = 0.5F;
      MISSILE_CAPACITY_EXP = 0.5F;
      MISSILE_CAPACITY_EXP_MULT = 0.5F;
      MISSILE_CAPACITY_EXP_FIRST_HALF = 0.5F;
      MISSILE_CAPACITY_EXP_MULT_FIRST_HALF = 0.5F;
      MISSILE_CAPACITY_EXP_THRESHOLD = 500000.0F;
      MISSILE_CAPACITY_EXP_SECOND_HALF = 0.5F;
      MISSILE_CAPACITY_EXP_MULT_SECOND_HALF = 0.5F;
      MISSILE_CAPACITY_LOG_FACTOR = 0.5F;
      MISSILE_CAPACITY_LOG_OFFSET = 0.5F;
   }

   public static enum MissileCapacityReloadMode {
      ALL,
      SINGLE;
   }
}
