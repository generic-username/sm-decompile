package org.schema.game.common.controller.elements.missile.dumb;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.missile.MissileUnit;

public class DumbMissileUnit extends MissileUnit {
   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((DumbMissileElementManager)((DumbMissileCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (DumbMissileCollectionManager)this.elementCollectionManager, var2, var3);
   }
}
