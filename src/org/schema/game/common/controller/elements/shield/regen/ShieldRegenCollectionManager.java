package org.schema.game.common.controller.elements.shield.regen;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.ShieldAddOn;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.controller.elements.ShieldHitCallback;
import org.schema.game.common.controller.elements.ShieldLocal;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;

public class ShieldRegenCollectionManager extends ElementCollectionManager {
   private long lastLocalShieldGet;
   private final Long2ObjectOpenHashMap lastLocalShieldCache = new Long2ObjectOpenHashMap();

   public ShieldRegenCollectionManager(SegmentController var1, VoidElementManager var2) {
      super((short)478, var1, var2);
   }

   private void updateCapabilities() {
      long var1 = 0L;
      Iterator var3 = this.getElementCollections().iterator();

      while(var3.hasNext()) {
         ShieldRegenUnit var4 = (ShieldRegenUnit)var3.next();
         var1 = (long)((float)var1 + (float)var4.size() * VoidElementManager.SHIELD_EXTRA_RECHARGE_MULT_PER_UNIT);
         var4.size();
      }

      var1 = (long)(Math.pow((double)var1 * VoidElementManager.SHIELD_RECHARGE_PRE_POW_MUL, VoidElementManager.SHIELD_RECHARGE_POW) * VoidElementManager.SHIELD_RECHARGE_TOTAL_MUL);
      ShieldAddOn var5;
      (var5 = ((ShieldContainerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getShieldAddOn()).setShieldRechargeRate(var1);
      var5.setShields(Math.min(var5.getShields(), var5.getShieldCapacity()));
   }

   public int getMargin() {
      return 0;
   }

   public boolean isDetailedElementCollections() {
      return true;
   }

   protected Class getType() {
      return ShieldRegenUnit.class;
   }

   public boolean needsUpdate() {
      return false;
   }

   public ShieldRegenUnit getInstance() {
      return new ShieldRegenUnit();
   }

   protected void onChangedCollection() {
      this.updateCapabilities();
      if (this.getSegmentController().isUsingPowerReactors()) {
         ((ShieldContainerInterface)this.getContainer()).getShieldAddOn().getShieldLocalAddOn().flagCalcLocalShields();
      }

   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      ShieldAddOn var1 = ((ShieldContainerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getShieldAddOn();
      return new GUIKeyValueEntry[]{new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIELD_REGEN_SHIELDREGENCOLLECTIONMANAGER_0, StringTools.formatPointZero(var1.getShieldCapacity())), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIELD_REGEN_SHIELDREGENCOLLECTIONMANAGER_1, StringTools.formatPointZero(var1.getShieldRechargeRate()))};
   }

   public void clear() {
      super.clear();
      this.lastLocalShieldCache.clear();
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIELD_REGEN_SHIELDREGENCOLLECTIONMANAGER_2;
   }

   public float getSensorValue(SegmentPiece var1) {
      ShieldAddOn var2;
      if ((var2 = ((ShieldContainerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getShieldAddOn()).isUsingLocalShields()) {
         ShieldLocal var3 = (ShieldLocal)this.lastLocalShieldCache.get(var1.getAbsoluteIndex());
         if (this.lastLocalShieldGet + (long)(var3 == null ? 500 : 5000) < this.getSegmentController().getState().getUpdateTime()) {
            this.lastLocalShieldCache.put(var1.getAbsoluteIndex(), var2.getShieldLocalAddOn().getContainingShield((ShieldContainerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), var1.getAbsoluteIndex()));
            this.lastLocalShieldGet = this.getSegmentController().getState().getUpdateTime();
         }

         ShieldLocal var4;
         return (var4 = (ShieldLocal)this.lastLocalShieldCache.get(var1.getAbsoluteIndex())) != null ? (float)Math.min(1.0D, var4.getShields() / Math.max(9.999999747378752E-5D, var4.getShieldCapacity())) : 0.0F;
      } else {
         return (float)Math.min(1.0D, var2.getShields() / Math.max(9.999999747378752E-5D, var2.getShieldCapacity()));
      }
   }

   public ShieldAddOn getShieldAddOn() {
      return ((ShieldContainerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getShieldAddOn();
   }

   public void shieldHit(ShieldHitCallback var1) {
      if (this.getSegmentController().isOnServer()) {
         this.checkIntegrityForced(var1.damager);
      }

   }
}
