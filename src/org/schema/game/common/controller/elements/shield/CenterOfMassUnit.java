package org.schema.game.common.controller.elements.shield;

import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.element.ElementCollection;

public abstract class CenterOfMassUnit extends ElementCollection {
   private final Vector3f centerOfMassUnweighted = new Vector3f();
   private final Vector3f centerOfMass = new Vector3f();
   private final Vector3i comOrigin = new Vector3i();

   protected void updateBB(int var1, int var2, int var3, long var4) {
      super.updateBB(var1, var2, var3, var4);
      Vector3f var10000 = this.centerOfMassUnweighted;
      var10000.x += (float)var1;
      var10000 = this.centerOfMassUnweighted;
      var10000.y += (float)var2;
      var10000 = this.centerOfMassUnweighted;
      var10000.z += (float)var3;
   }

   public void calculateExtraDataAfterCreationThreaded(long var1, LongOpenHashSet var3) {
      super.calculateExtraDataAfterCreationThreaded(var1, var3);
      this.centerOfMass.set(this.centerOfMassUnweighted);
      this.centerOfMass.scale(1.0F / (float)this.size());
      this.comOrigin.set(Math.round(this.centerOfMass.x), Math.round(this.centerOfMass.y), Math.round(this.centerOfMass.z));
   }

   public void resetAABB() {
      super.resetAABB();
      this.centerOfMassUnweighted.set(0.0F, 0.0F, 0.0F);
      this.centerOfMass.set(0.0F, 0.0F, 0.0F);
   }

   public Vector3i getCoMOrigin() {
      return this.comOrigin;
   }
}
