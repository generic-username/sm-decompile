package org.schema.game.common.controller.elements;

import java.util.Iterator;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;

public abstract class ManagerModule {
   private final UsableElementManager elementManager;
   private final short elementID;
   private ManagerModule next;

   public ManagerModule(UsableElementManager var1, short var2) {
      this.elementManager = var1;
      this.elementID = var2;
   }

   public void addControlledBlock(Vector3i var1, short var2, Vector3i var3, short var4) {
   }

   public void clear() {
   }

   public short getElementID() {
      return this.elementID;
   }

   public UsableElementManager getElementManager() {
      return this.elementManager;
   }

   public ManagerModule getNext() {
      return this.next;
   }

   public void setNext(ManagerModule var1) {
      this.next = var1;
   }

   public void onConnectionRemoved(Vector3i var1, Vector3i var2, short var3) {
   }

   public String toString() {
      return "(" + this.getElementManager().getClass().getSimpleName() + ": " + (this.elementID == 29999 ? "TYPE_RAIL_TRACK" : (this.elementID == 29998 ? "TYPE_RAIL_INV" : (this.elementID == 32767 ? "TYPE_ALL" : ElementKeyMap.getInfo(this.elementID).getName()))) + ")";
   }

   public abstract void update(Timer var1, long var2);

   public ControllerManagerGUI createGUI(GameClientState var1) {
      if (!(this instanceof ManagerModuleCollection)) {
         if (this instanceof ManagerModuleSingle) {
            return ((ManagerModuleSingle)this).getCollectionManager().createGUI(var1);
         } else {
            throw new NullPointerException();
         }
      } else {
         ManagerModuleCollection var2 = (ManagerModuleCollection)this;
         GUIElementList var3 = new GUIElementList(var1);
         GUIKeyValueEntry[] var4 = var2.getGUIElementCollectionValues();

         for(int var5 = 0; var5 < var4.length; ++var5) {
            GUIAncor var6 = var4[var5].get(var1);
            var3.addWithoutUpdate(new GUIListElement(var6, var6, var1));
         }

         Iterator var9 = var2.getCollectionManagers().iterator();

         while(var9.hasNext()) {
            ControllerManagerGUI var7 = ((ElementCollectionManager)var9.next()).createGUI(var1);

            assert var7 != null;

            GUIListElement var8 = var7.getListEntry(var1, var3);

            assert var8 != null;

            var3.addWithoutUpdate(var8);
         }

         var3.updateDim();
         ControllerManagerGUI var10;
         (var10 = new ControllerManagerGUI()).createFrom(var1, var2, var3);

         assert var10.check() : var10;

         return var10;
      }
   }

   public double calculateWeaponDamageIndex() {
      return this.elementManager instanceof WeaponElementManagerInterface ? ((WeaponElementManagerInterface)this.elementManager).calculateWeaponDamageIndex() : 0.0D;
   }

   public double calculateWeaponHitPropabilityIndex() {
      return this.elementManager instanceof WeaponElementManagerInterface ? ((WeaponElementManagerInterface)this.elementManager).calculateWeaponHitPropabilityIndex() : 0.0D;
   }

   public double calculateWeaponSpecialIndex() {
      return this.elementManager instanceof WeaponElementManagerInterface ? ((WeaponElementManagerInterface)this.elementManager).calculateWeaponSpecialIndex() : 0.0D;
   }

   public double calculateWeaponRangeIndex() {
      return this.elementManager instanceof WeaponElementManagerInterface ? ((WeaponElementManagerInterface)this.elementManager).calculateWeaponRangeIndex() : 0.0D;
   }

   public double calculateWeaponPowerConsumptionPerSecondIndex() {
      return this.elementManager instanceof WeaponElementManagerInterface ? ((WeaponElementManagerInterface)this.elementManager).calculateWeaponPowerConsumptionPerSecondIndex() : 0.0D;
   }

   public double calculateSupportIndex() {
      return this.elementManager instanceof SupportElementManagerInterface ? ((SupportElementManagerInterface)this.elementManager).calculateSupportIndex() : 0.0D;
   }

   public double calculateSupportPowerConsumptionPerSecondIndex() {
      return this.elementManager instanceof SupportElementManagerInterface ? ((SupportElementManagerInterface)this.elementManager).calculateSupportPowerConsumptionPerSecondIndex() : 0.0D;
   }

   public double calculateStealthIndex(double var1) {
      return this.elementManager instanceof StealthElementManagerInterface ? ((StealthElementManagerInterface)this.elementManager).calculateStealthIndex(var1) : 0.0D;
   }

   public void init(ManagerContainer var1) {
      this.elementManager.init(var1);
   }

   public abstract void onFullyLoaded();

   public abstract boolean needsAnyUpdate();
}
