package org.schema.game.common.controller.elements;

public enum UnitCalcStyle {
   LINEAR,
   BOX_DIM_MULT,
   BOX_DIM_ADD,
   EXP,
   DOUBLE_EXP,
   LOG,
   LOG_LEVELED;
}
