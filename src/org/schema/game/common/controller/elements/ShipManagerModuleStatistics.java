package org.schema.game.common.controller.elements;

import java.io.File;
import javax.script.CompiledScript;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.beam.harvest.SalvageElementManager;

public class ShipManagerModuleStatistics extends ModuleStatistics {
   public static long lastModified;
   public static CompiledScript script;

   public ShipManagerModuleStatistics(Ship var1, ShipManagerContainer var2) {
      super(var1, var2);
   }

   public double calculateDangerIndex() {
      return 0.0D;
   }

   public double calculateMobilityIndex() {
      return 0.0D;
   }

   public double calculateSurvivabilityIndex() {
      return 0.0D;
   }

   public synchronized CompiledScript getCompiledScript() {
      return script;
   }

   public synchronized void setCompiledScript(CompiledScript var1) {
      script = var1;
   }

   public synchronized long getScriptChanged() {
      return lastModified;
   }

   public synchronized void setScriptChanged(long var1) {
      lastModified = var1;
   }

   public String getScript() {
      return File.separator + "statistics" + File.separator + "ship-index-calculation.lua";
   }

   public double getJumpDriveIndex() {
      return (double)((ShipManagerContainer)this.managerContainer).getJumpDrive().getCollectionManagers().size();
   }

   public double getThrust() {
      return (double)((ShipManagerContainer)this.managerContainer).getThrusterElementManager().getActualThrust();
   }

   public double getShields() {
      return ((ShipManagerContainer)this.managerContainer).getShieldAddOn().getShieldCapacity();
   }

   public double getMining() {
      return ((SalvageElementManager)((ShipManagerContainer)this.managerContainer).getSalvage().getElementManager()).getMiningScore();
   }
}
