package org.schema.game.common.controller.elements.mines;

import org.schema.game.common.data.mines.Mine;

public interface ClientMineListener {
   void onRemovedMine(Mine var1);

   void onAddMine(Mine var1);

   void onChangedMine(Mine var1);

   void onBecomingInactive(Mine var1);

   void onBecomingActive(Mine var1);
}
