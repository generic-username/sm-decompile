package org.schema.game.common.controller.elements.mines;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.CustomOutputUnit;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ShootContainer;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;

public class MineLayerUnit extends CustomOutputUnit {
   private final Vector3i pTmp = new Vector3i();
   private final SegmentPiece pieceTmp = new SegmentPiece();
   private final SegmentPiece pieceTmpD = new SegmentPiece();

   public String toString() {
      return "MineLayerUnit " + super.toString();
   }

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((MineLayerElementManager)((MineLayerCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (MineLayerCollectionManager)this.elementCollectionManager, var2, var3);
   }

   public double getPowerConsumedPerSecondResting() {
      return 0.0D;
   }

   public double getPowerConsumedPerSecondCharging() {
      return 0.0D;
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.MINES;
   }

   public short[] calcComposition() {
      short[] var1 = new short[6];
      long var2 = this.getNeighboringCollection().get(0);
      SegmentPiece var5;
      if ((var5 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var2, this.pieceTmp)) != null) {
         assert var5.getType() == 37 : var5;

         for(int var3 = 0; var3 < 6; ++var3) {
            var5.getAbsolutePos(this.pTmp);
            this.pTmp.add(Element.DIRECTIONSi[var3]);
            SegmentPiece var4;
            if ((var4 = this.getSegmentController().getSegmentBuffer().getPointUnsave(this.pTmp, this.pieceTmpD)) != null && var4.getType() != 0) {
               var1[var3] = var4.getType();
            }
         }
      }

      return var1;
   }

   public void doShot(ControllerStateInterface var1, Timer var2, ShootContainer var3) {
      MineLayerElementManager var7 = (MineLayerElementManager)((MineLayerCollectionManager)this.elementCollectionManager).getElementManager();
      int var4;
      if (var1.getPlayerState() != null) {
         var4 = var1.getPlayerState().getMineAutoArmSeconds();
      } else if (this.getSegmentController().isInFleet()) {
         Fleet var5 = this.getSegmentController().getFleet();
         PlayerState var6;
         if ((var6 = ((GameServerState)this.getSegmentController().getState()).getPlayerFromNameIgnoreCaseWOException(var5.getOwner())) != null) {
            var4 = var6.getMineAutoArmSeconds();
         } else {
            var4 = 60;
         }
      } else {
         var4 = 60;
      }

      var7.layMine(this, (MineLayerCollectionManager)this.elementCollectionManager, var4, var3);
   }

   public float getBasePowerConsumption() {
      return 0.0F;
   }

   public float getPowerConsumption() {
      return 0.0F;
   }

   public float getPowerConsumptionWithoutEffect() {
      return 0.0F;
   }

   public float getReloadTimeMs() {
      return 500.0F;
   }

   public float getInitializationTime() {
      return 1.0F;
   }

   public float getDistanceRaw() {
      return 0.0F;
   }

   public float getFiringPower() {
      return 0.0F;
   }

   public float getDamage() {
      return 0.0F;
   }
}
