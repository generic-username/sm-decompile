package org.schema.game.common.controller.elements.effectblock.kinetic;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.effectblock.EffectCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;

public class KineticEffectCollectionManager extends EffectCollectionManager {
   public KineticEffectCollectionManager(SegmentPiece var1, SegmentController var2, KineticEffectElementManager var3) {
      super(var1, (short)354, var2, var3);
   }

   protected Class getType() {
      return KineticEffectUnit.class;
   }

   public KineticEffectUnit getInstance() {
      return new KineticEffectUnit();
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_EFFECTBLOCK_KINETIC_KINETICEFFECTCOLLECTIONMANAGER_0;
   }
}
