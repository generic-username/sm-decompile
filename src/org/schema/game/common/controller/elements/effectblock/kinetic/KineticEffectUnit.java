package org.schema.game.common.controller.elements.effectblock.kinetic;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.effectblock.EffectCollectionManager;
import org.schema.game.common.controller.elements.effectblock.EffectUnit;

public class KineticEffectUnit extends EffectUnit {
   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((KineticEffectElementManager)((KineticEffectCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (EffectCollectionManager)this.elementCollectionManager, var2, var3);
   }
}
