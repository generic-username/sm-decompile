package org.schema.game.common.controller.elements.effectblock.heat;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.effectblock.EffectCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;

public class HeatEffectCollectionManager extends EffectCollectionManager {
   public HeatEffectCollectionManager(SegmentPiece var1, SegmentController var2, HeatEffectElementManager var3) {
      super(var1, (short)352, var2, var3);
   }

   protected Class getType() {
      return HeatEffectUnit.class;
   }

   public HeatEffectUnit getInstance() {
      return new HeatEffectUnit();
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_EFFECTBLOCK_HEAT_HEATEFFECTCOLLECTIONMANAGER_0;
   }
}
