package org.schema.game.common.controller.elements.effectblock;

import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.data.blockeffects.BlockEffect;
import org.schema.game.common.data.blockeffects.BlockEffectManager;
import org.schema.game.common.data.blockeffects.BlockEffectTypes;
import org.schema.schine.common.language.Lng;

public abstract class EffectElementManager extends UsableControllableElementManager {
   public EffectElementManager(short var1, short var2, SegmentController var3) {
      super(var1, var2, var3);
   }

   public BlockEffectManager getBlockEffectManager() {
      return ((SendableSegmentController)this.getSegmentController()).getBlockEffectManager();
   }

   protected boolean isActiveEffect(EffectCollectionManager var1) {
      BlockEffect var2;
      return (var2 = var1.getCurrentBlockEffect()) != null && var2.isAlive();
   }

   protected void deactivateEffect(EffectCollectionManager var1) {
      BlockEffect var2;
      if ((var2 = var1.getCurrentBlockEffect()) != null) {
         var2.end();
      }

   }

   public abstract InterEffectSet getInterEffect();

   public ControllerManagerGUI getGUIUnitValues(EffectUnit var1, EffectCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      float var5 = this.getSegmentController().getMassWithDocks() * VoidElementManager.DEVENSIVE_EFFECT_MAX_PERCENT_MASS_MULT;
      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_EFFECTBLOCK_EFFECTELEMENTMANAGER_1, var1, new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_EFFECTBLOCK_EFFECTELEMENTMANAGER_0, var2.getName()), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_EFFECTBLOCK_EFFECTELEMENTMANAGER_6, StringTools.formatPointZero((float)var2.getTotalSize() * 100.0F / var5)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_EFFECTBLOCK_EFFECTELEMENTMANAGER_2, StringTools.formatPointZero((float)var2.getTotalSize())), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_EFFECTBLOCK_EFFECTELEMENTMANAGER_3, StringTools.formatPointZeroZero((float)var2.getTotalSize() / var5)));
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_EFFECTBLOCK_EFFECTELEMENTMANAGER_7;
   }

   public static enum OffensiveEffects {
      EMP((BlockEffectTypes)null),
      EXPLOSIVE((BlockEffectTypes)null),
      ION((BlockEffectTypes)null),
      OVERDRIVE((BlockEffectTypes)null),
      PIERCING((BlockEffectTypes)null),
      PUNCHTHROUGH((BlockEffectTypes)null),
      PULL(BlockEffectTypes.PULL),
      PUSH(BlockEffectTypes.PUSH),
      STOP(BlockEffectTypes.STOP),
      REPAIR((BlockEffectTypes)null),
      SHIELD_SUPPLY((BlockEffectTypes)null),
      SHIELD_DRAIN((BlockEffectTypes)null),
      POWER_SUPPLY((BlockEffectTypes)null),
      POWER_DRAIN((BlockEffectTypes)null),
      NO_THRUST((BlockEffectTypes)null),
      NO_POWER((BlockEffectTypes)null),
      SHIELD_DOWN((BlockEffectTypes)null),
      THRUSTER_OUTAGE(BlockEffectTypes.THRUSTER_OUTAGE),
      NO_POWER_RECHARGE(BlockEffectTypes.NO_POWER_RECHARGE),
      NO_SHIELD_RECHARGE(BlockEffectTypes.NO_SHIELD_RECHARGE);

      private final BlockEffectTypes effectType;

      private OffensiveEffects(BlockEffectTypes var3) {
         this.effectType = var3;
      }

      public final BlockEffectTypes getEffect() {
         return this.effectType;
      }
   }
}
