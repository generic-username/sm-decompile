package org.schema.game.common.controller.elements.effectblock.heat;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.effectblock.EffectCollectionManager;
import org.schema.game.common.controller.elements.effectblock.EffectUnit;

public class HeatEffectUnit extends EffectUnit {
   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((HeatEffectElementManager)((HeatEffectCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (EffectCollectionManager)this.elementCollectionManager, var2, var3);
   }
}
