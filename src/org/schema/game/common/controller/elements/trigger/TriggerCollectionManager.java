package org.schema.game.common.controller.elements.trigger;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;

public class TriggerCollectionManager extends ControlBlockElementCollectionManager {
   public TriggerCollectionManager(SegmentPiece var1, SegmentController var2, TriggerElementManager var3) {
      super(var1, (short)411, var2, var3);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return TriggerUnit.class;
   }

   public boolean needsUpdate() {
      return false;
   }

   public TriggerUnit getInstance() {
      return new TriggerUnit();
   }

   public boolean isUsingIntegrity() {
      return false;
   }

   protected void onChangedCollection() {
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
      }

   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[0];
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_TRIGGER_TRIGGERCOLLECTIONMANAGER_0;
   }
}
