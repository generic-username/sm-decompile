package org.schema.game.common.controller.elements.trigger;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class TriggerElementManager extends UsableControllableElementManager {
   public static boolean debug = false;

   public TriggerElementManager(SegmentController var1) {
      super((short)413, (short)411, var1);
   }

   public ControllerManagerGUI getGUIUnitValues(TriggerUnit var1, TriggerCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return null;
   }

   protected String getTag() {
      return "trigger";
   }

   public TriggerCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new TriggerCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_TRIGGER_TRIGGERELEMENTMANAGER_0;
   }

   protected void playSound(TriggerUnit var1, Transform var2) {
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
   }
}
