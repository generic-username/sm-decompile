package org.schema.game.common.controller.elements.racegate;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.effects.RaisingIndication;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.Destination;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.InterControllerCollectionManager;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class RacegateCollectionManager extends ControlBlockElementCollectionManager implements InterControllerCollectionManager, PowerConsumer {
   private long lastPopup;
   private Destination raceDestination = new Destination();
   private float powered;

   public RacegateCollectionManager(SegmentPiece var1, SegmentController var2, RacegateElementManager var3, Long2ObjectOpenHashMap var4, Long2ObjectOpenHashMap var5) {
      super(var1, (short)684, var2, var3);

      assert var1 != null;

      assert var4 != null;

      String var7 = (String)var4.remove(var1.getAbsoluteIndex());
      Vector3i var6 = (Vector3i)var5.get(var1.getAbsoluteIndex());
      if (var7 != null) {
         this.raceDestination.uid = var7;
         this.raceDestination.local.set(var6);
      }

   }

   protected Tag toTagStructurePriv() {
      Tag var1 = new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.isValid() ? 1 : 0)));
      Tag var2 = new Tag(Tag.Type.STRING, (String)null, this.raceDestination.uid);
      Tag var3 = new Tag(Tag.Type.VECTOR3i, (String)null, this.raceDestination.local);
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, var2, var3, FinishTag.INST});
   }

   public boolean isUsingIntegrity() {
      return false;
   }

   public ElementCollectionManager.CollectionShape requiredNeigborsPerBlock() {
      return ElementCollectionManager.CollectionShape.LOOP;
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return RacegateUnit.class;
   }

   public boolean needsUpdate() {
      return true;
   }

   public RacegateUnit getInstance() {
      return new RacegateUnit();
   }

   protected void onChangedCollection() {
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
      }

   }

   public void update(Timer var1) {
      if (this.isValid()) {
         ((RacegateUnit)this.getElementCollections().get(0)).update(var1);
      } else {
         if (!this.getSegmentController().isOnServer() && ((GameClientState)this.getSegmentController().getState()).getCurrentSectorId() == this.getSegmentController().getSectorId() && System.currentTimeMillis() - this.lastPopup > 5000L) {
            Transform var3;
            (var3 = new Transform()).setIdentity();
            Vector3i var2 = this.getControllerPos();
            var3.origin.set((float)(var2.x - 16), (float)(var2.y - 16), (float)(var2.z - 16));
            this.getSegmentController().getWorldTransform().transform(var3.origin);
            RaisingIndication var4;
            (var4 = new RaisingIndication(var3, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RACEGATE_RACEGATECOLLECTIONMANAGER_0, 1.0F, 0.1F, 0.1F, 1.0F)).speed = 0.1F;
            var4.lifetime = 3.0F;
            HudIndicatorOverlay.toDrawTexts.add(var4);
            this.lastPopup = System.currentTimeMillis();
         }

      }
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[0];
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RACEGATE_RACEGATECOLLECTIONMANAGER_1;
   }

   public boolean isValid() {
      return this.getElementCollections().size() == 1 && ((RacegateUnit)this.getElementCollections().get(0)).isValid();
   }

   public String getWarpDestinationUID() {
      return this.raceDestination.uid;
   }

   public void setDestination(String var1, Vector3i var2) {
      this.raceDestination.uid = var1;
      this.raceDestination.local.set(var2);
   }

   public int getWarpType() {
      return 2;
   }

   public int getWarpPermission() {
      return 0;
   }

   public Vector3i getLocalDestination() {
      return this.raceDestination.local;
   }

   public Destination getNextGate() {
      return this.raceDestination;
   }

   public void setPowered(float var1) {
      this.powered = var1;
   }

   public float getPowered() {
      return this.powered;
   }

   private double getReactorPowerUsage() {
      return 0.1D;
   }

   public double getPowerConsumedPerSecondResting() {
      return this.getReactorPowerUsage();
   }

   public double getPowerConsumedPerSecondCharging() {
      return this.getReactorPowerUsage();
   }

   public boolean isPowerCharging(long var1) {
      return false;
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.OTHERS;
   }

   public void reloadFromReactor(double var1, Timer var3, float var4, boolean var5, float var6) {
   }

   public boolean isPowerConsumerActive() {
      return true;
   }

   public void dischargeFully() {
   }
}
