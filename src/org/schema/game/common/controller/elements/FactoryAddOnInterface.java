package org.schema.game.common.controller.elements;

public interface FactoryAddOnInterface {
   FactoryAddOn getFactory();
}
