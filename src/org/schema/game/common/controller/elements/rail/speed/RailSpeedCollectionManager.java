package org.schema.game.common.controller.elements.rail.speed;

import it.unimi.dsi.fastutil.longs.LongArrayList;
import java.util.Iterator;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class RailSpeedCollectionManager extends ControlBlockElementCollectionManager {
   private final SegmentPiece p = new SegmentPiece();
   private boolean checkComplete;

   public RailSpeedCollectionManager(SegmentPiece var1, SegmentController var2, RailSpeedElementManager var3) {
      super(var1, (short)32767, var2, var3);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return RailSpeedUnit.class;
   }

   public boolean needsUpdate() {
      return false;
   }

   public RailSpeedUnit getInstance() {
      return new RailSpeedUnit();
   }

   public boolean isUsingIntegrity() {
      return false;
   }

   protected void onChangedCollection() {
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
      }

   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[0];
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_SPEED_RAILSPEEDCOLLECTIONMANAGER_0;
   }

   public boolean checkAllConnections() {
      if (this.checkComplete) {
         return true;
      } else {
         long var1 = ElementCollection.getIndex(this.getControllerPos());
         FastCopyLongOpenHashSet var3 = (FastCopyLongOpenHashSet)this.getSegmentController().getControlElementMap().getControllingMap().getAll().get(var1);
         int var4 = 0;
         LongArrayList var5 = new LongArrayList();
         if (var3 != null) {
            Iterator var13 = var3.iterator();

            SegmentPiece var6;
            long var7;
            while(var13.hasNext()) {
               var7 = (Long)var13.next();
               if ((var6 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var7, this.p)) == null) {
                  return false;
               }

               if (ElementKeyMap.isValidType(var6.getType()) && ElementKeyMap.getInfoFast(var6.getType()).isRailTrack()) {
                  ++var4;
               } else if (!ElementKeyMap.isValidType(var6.getType()) || !ElementKeyMap.getInfoFast(var6.getType()).isSignal() && var6.getType() != 120) {
                  var5.add(var7);
               }
            }

            var13 = var5.iterator();

            while(var13.hasNext()) {
               var7 = (Long)var13.next();
               var6 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var7, this.p);

               try {
                  throw new Exception("WARNING: Irregular block connected to rail speed: " + var6 + "; Removing Connection");
               } catch (Exception var11) {
                  var11.printStackTrace();
                  this.getSegmentController().getControlElementMap().removeControllerForElement(var1, ElementCollection.getPosIndexFrom4(var7), (short)ElementCollection.getType(var7));
               }
            }
         }

         int var14 = 0;

         RailSpeedUnit var8;
         Iterator var16;
         for(var16 = this.getElementCollections().iterator(); var16.hasNext(); var14 += var8.size()) {
            var8 = (RailSpeedUnit)var16.next();
         }

         if (var14 == var4) {
            this.checkComplete = true;
            return true;
         } else {
            if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
               System.err.println("RAILSPEED: COMP: " + var14 + " / " + var4);
               var16 = this.getElementCollections().iterator();

               while(var16.hasNext()) {
                  Iterator var15 = ((RailSpeedUnit)var16.next()).getNeighboringCollection().iterator();

                  while(var15.hasNext()) {
                     long var9 = (Long)var15.next();
                     SegmentPiece var12;
                     if ((var12 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var9, this.p)) == null) {
                        System.err.println("RAILSPEED: POINT NOT LOADED: " + ElementCollection.getPosFromIndex(var9, new Vector3i()));
                     }

                     System.err.println("RAILSPEED: " + this.getControllerPos() + " COLLECTION POINT " + var12);
                     SegmentPiece.debugDrawPoint(this.getControllerPos(), this.getSegmentController().getWorldTransform(), 0.1F, 1.0F, 1.0F, 0.3F, 1.0F, 2000L);
                     var12.debugDrawPoint(this.getSegmentController().getWorldTransform(), 0.1F, 1.0F, 0.1F, 0.3F, 1.0F, 2000L);
                  }
               }
            }

            return false;
         }
      }
   }
}
