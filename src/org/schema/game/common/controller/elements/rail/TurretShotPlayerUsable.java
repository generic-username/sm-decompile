package org.schema.game.common.controller.elements.rail;

import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.client.view.gui.weapon.WeaponRowElementInterface;
import org.schema.game.client.view.gui.weapon.WeaponSegmentControllerUsableElement;
import org.schema.game.common.controller.elements.ManagerActivityInterface;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ManagerReloadInterface;
import org.schema.game.common.controller.elements.SegmentControllerUsable;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.input.InputType;

public class TurretShotPlayerUsable extends SegmentControllerUsable {
   public MouseEvent.ShootButton shootFlag = null;

   public TurretShotPlayerUsable(ManagerContainer var1) {
      super(var1);
   }

   public WeaponRowElementInterface getWeaponRow() {
      return new WeaponSegmentControllerUsableElement(this);
   }

   public boolean isControllerConnectedTo(long var1, short var3) {
      return true;
   }

   public boolean isPlayerUsable() {
      return this.segmentController.railController.hasTurret();
   }

   public long getUsableId() {
      return -9223372036854775779L;
   }

   public void handleControl(ControllerStateInterface var1, Timer var2) {
      this.shootFlag = null;
      if (var1.isPrimaryShootButtonDown() && var1.isFlightControllerActive() && this.segmentController.isOnServer()) {
         this.shootFlag = MouseEvent.ShootButton.PRIMARY_FIRE;
      }

   }

   public ManagerReloadInterface getReloadInterface() {
      return null;
   }

   public ManagerActivityInterface getActivityInterface() {
      return null;
   }

   public String getWeaponRowName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_TURRETSHOTPLAYERUSABLE_1;
   }

   public short getWeaponRowIcon() {
      return 665;
   }

   public String getName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_TURRETSHOTPLAYERUSABLE_0;
   }

   public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
      var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_TURRETSHOTPLAYERUSABLE_2, var3, ContextFilter.IMPORTANT);
   }
}
