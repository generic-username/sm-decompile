package org.schema.game.common.controller.elements.rail.speed;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.element.ElementCollection;

public class RailSpeedUnit extends ElementCollection {
   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((RailSpeedElementManager)((RailSpeedCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (RailSpeedCollectionManager)this.elementCollectionManager, var2, var3);
   }
}
