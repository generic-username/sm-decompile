package org.schema.game.common.controller.elements.rail.inv;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.element.ElementCollection;

public class RailConnectionUnit extends ElementCollection {
   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((RailConnectionElementManager)((RailConnectionCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (RailConnectionCollectionManager)this.elementCollectionManager, var2, var3);
   }
}
