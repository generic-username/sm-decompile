package org.schema.game.common.controller.elements.rail.massenhancer;

import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class RailMassEnhancerCollectionManager extends ElementCollectionManager implements PowerConsumer {
   private boolean on = true;
   private float powered;

   public RailMassEnhancerCollectionManager(SegmentController var1, VoidElementManager var2) {
      super((short)671, var1, var2);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return RailMassEnhancerUnit.class;
   }

   public boolean needsUpdate() {
      return true;
   }

   public RailMassEnhancerUnit getInstance() {
      return new RailMassEnhancerUnit();
   }

   protected void onChangedCollection() {
   }

   public boolean isUsingIntegrity() {
      return false;
   }

   public void update(Timer var1) {
      if (this.getSegmentController().isUsingPowerReactors()) {
         this.on = this.getPowered() >= 1.0F;
      } else if (this.getContainer() instanceof PowerManagerInterface) {
         this.on = ((PowerManagerInterface)this.getContainer()).getPowerAddOn().consumePower((double)this.getTotalSize() * VoidElementManager.RAIL_MASS_ENHANCER_POWER_CONSUMED_PER_ENHANCER, var1) > 0.0D;
      } else {
         this.on = false;
      }

      super.update(var1);
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[]{new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_MASSENHANCER_RAILMASSENHANCERCOLLECTIONMANAGER_0, String.valueOf(this.getTotalSize())), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_MASSENHANCER_RAILMASSENHANCERCOLLECTIONMANAGER_1, String.valueOf(VoidElementManager.RAIL_MASS_ENHANCER_FREE_MASS)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_MASSENHANCER_RAILMASSENHANCERCOLLECTIONMANAGER_2, String.valueOf(VoidElementManager.RAIL_MASS_ENHANCER_MASS_ADDED_PER_ENHANCER)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_MASSENHANCER_RAILMASSENHANCERCOLLECTIONMANAGER_3, this.isOn() ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_MASSENHANCER_RAILMASSENHANCERCOLLECTIONMANAGER_4 : Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_MASSENHANCER_RAILMASSENHANCERCOLLECTIONMANAGER_5), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_MASSENHANCER_RAILMASSENHANCERCOLLECTIONMANAGER_6, this.getTotalMassAllowed())};
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_MASSENHANCER_RAILMASSENHANCERCOLLECTIONMANAGER_7;
   }

   public float getTotalMassAllowed() {
      int var1 = this.isOn() ? this.getTotalSize() : 0;
      return VoidElementManager.RAIL_MASS_ENHANCER_FREE_MASS + (float)var1 * VoidElementManager.RAIL_MASS_ENHANCER_MASS_ADDED_PER_ENHANCER;
   }

   public float getRailPercent(float var1) {
      if ((var1 -= this.getTotalMassAllowed()) <= 0.0F) {
         return 1.0F;
      } else {
         var1 *= VoidElementManager.RAIL_MASS_ENHANCER_PERCENT_COST_PER_MASS_ABOVE_ENHANCER_PROVIDED;
         return Math.min(1.0F, Math.max(0.05F, 1.0F - var1));
      }
   }

   public boolean isOn() {
      return this.on;
   }

   private double getReactorPowerUsage() {
      double var1 = (double)VoidElementManager.RAIL_MASS_ENHANCER_REACTOR_POWER_CONSUMPTION_CHARGING * (double)this.getTotalSize();
      return this.getConfigManager().apply(StatusEffectType.RAIL_ENHANCER_POWER_EFFICIENCY, var1);
   }

   public double getPowerConsumedPerSecondResting() {
      return this.getReactorPowerUsage();
   }

   public double getPowerConsumedPerSecondCharging() {
      return this.getReactorPowerUsage();
   }

   public boolean isPowerCharging(long var1) {
      return true;
   }

   public void setPowered(float var1) {
      this.powered = var1;
   }

   public float getPowered() {
      return this.powered;
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.DOCKS;
   }

   public void reloadFromReactor(double var1, Timer var3, float var4, boolean var5, float var6) {
   }

   public boolean isPowerConsumerActive() {
      return true;
   }

   public void dischargeFully() {
   }
}
