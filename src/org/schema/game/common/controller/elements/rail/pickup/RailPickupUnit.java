package org.schema.game.common.controller.elements.rail.pickup;

import it.unimi.dsi.fastutil.longs.Long2LongOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongSet;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetFormationingAbstract;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class RailPickupUnit extends ElementCollection {
   private Long2LongOpenHashMap lastUsed = new Long2LongOpenHashMap();
   Vector3f dockerPos = new Vector3f();
   Vector3f railAreaPos = new Vector3f();
   SegmentPiece test = new SegmentPiece();

   public void activate(SegmentPiece var1, boolean var2) {
      long var3 = var1.getAbsoluteIndex();
      long var5;
      if (var2) {
         var5 = ElementCollection.getActivation(var3, false, false);

         assert ElementCollection.getType(var5) < 10;
      } else {
         var5 = ElementCollection.getDeactivation(var3, false, false);

         assert ElementCollection.getType(var5) < 10;
      }

      ((SendableSegmentController)this.getSegmentController()).getBlockActivationBuffer().enqueue(var5);
   }

   public void cleanUp() {
      super.cleanUp();
   }

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ControllerManagerGUI.create(var1, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_PICKUP_RAILPICKUPUNIT_0, this);
   }

   public boolean isActive() {
      if (this.getElementCollectionId() != null) {
         this.getElementCollectionId().refresh();
         return this.getElementCollectionId().isActive();
      } else {
         return false;
      }
   }

   public void update(Timer var1) {
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getCurrentSectorEntities().values();
         var1 = null;
         if (((GameClientState)this.getSegmentController().getState()).getShip() != null) {
            Ship var5 = ((GameClientState)this.getSegmentController().getState()).getShip();
            this.handle(var5);
         }

      } else {
         Sector var3;
         if ((var3 = ((GameServerState)this.getSegmentController().getState()).getUniverse().getSector(this.getSegmentController().getSectorId())) != null) {
            Iterator var4 = var3.getEntities().iterator();

            while(true) {
               SimpleTransformableSendableObject var2;
               do {
                  do {
                     do {
                        if (!var4.hasNext()) {
                           return;
                        }
                     } while(!((var2 = (SimpleTransformableSendableObject)var4.next()) instanceof SegmentController));
                  } while(!((SegmentController)var2).railController.isRoot());
               } while(var2 instanceof PlayerControllable && !((PlayerControllable)var2).getAttachedPlayers().isEmpty());

               this.handle((SegmentController)var2);
            }
         }
      }
   }

   private void handle(SegmentController var1) {
      if (this.getSegmentController().isVirtualBlueprint() || var1 == this.getSegmentController() || !(var1 instanceof Ship) || !((Ship)var1).railController.isRoot() || !this.checkRaildockingProximity((Ship)var1)) {
         ;
      }
   }

   private boolean checkRaildockingProximity(Ship var1) {
      LongSet var2 = var1.getManagerContainer().getRailBeam().getRailDockers();
      Iterator var3 = this.getNeighboringCollection().iterator();

      while(true) {
         long var4;
         do {
            if (!var3.hasNext()) {
               return false;
            }

            var4 = (Long)var3.next();
         } while(!this.isActive(var4));

         ElementCollection.getPosFromIndex(var4, this.railAreaPos);
         Vector3f var10000 = this.railAreaPos;
         var10000.x -= 16.0F;
         var10000 = this.railAreaPos;
         var10000.y -= 16.0F;
         var10000 = this.railAreaPos;
         var10000.z -= 16.0F;
         this.getSegmentController().getWorldTransform().transform(this.railAreaPos);
         Iterator var6 = var2.iterator();

         while(var6.hasNext()) {
            long var7;
            ElementCollection.getPosFromIndex(var7 = (Long)var6.next(), this.dockerPos);
            var10000 = this.dockerPos;
            var10000.x -= 16.0F;
            var10000 = this.dockerPos;
            var10000.y -= 16.0F;
            var10000 = this.dockerPos;
            var10000.z -= 16.0F;
            var1.getWorldTransform().transform(this.dockerPos);
            if (this.isInPickupProximity(this.dockerPos, this.railAreaPos)) {
               this.dock(var1, var7, var4);
               return true;
            }
         }
      }
   }

   private void dock(Ship var1, long var2, long var4) {
      SegmentPiece var9 = var1.getSegmentBuffer().getPointUnsave(var2, new SegmentPiece());
      SegmentPiece var3 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var4, new SegmentPiece());
      if (var9 != null && var3 != null && System.currentTimeMillis() - this.lastUsed.get(var4) > 3000L) {
         for(int var6 = 0; var6 < 6; ++var6) {
            Vector3i var7 = Element.DIRECTIONSi[var6];
            Vector3i var8 = new Vector3i();
            var3.getAbsolutePos(var8);
            var8.add(var7);
            SegmentPiece var10;
            if (ElementKeyMap.isValidType((var10 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var8, new SegmentPiece())).getType()) && ElementKeyMap.getInfoFast(var10.getType()).isRailTrack()) {
               if (this.getSegmentController().isOnServer()) {
                  if (var3.getAbsoluteIndex() == var1.lastPickupAreaUsed || !this.hasFleetFormatingState(var1)) {
                     var1.lastPickupAreaUsed = var3.getAbsoluteIndex();
                     var1.getNetworkObject().lastPickupAreaUsed.add(var1.lastPickupAreaUsed);
                     var1.railController.connectServer(var9, var10);
                     this.lastUsed.put(var4, System.currentTimeMillis());
                     return;
                  }
               } else {
                  var1.lastPickupAreaUsed = var3.getAbsoluteIndex();
                  var1.getNetworkObject().lastPickupAreaUsed.add(var1.lastPickupAreaUsed);
                  var1.railController.connectClient(var9, var10);
                  this.lastUsed.put(var4, System.currentTimeMillis());
               }

               return;
            }
         }
      }

   }

   private boolean hasFleetFormatingState(Ship var1) {
      return var1.getAiConfiguration() != null && var1.getAiConfiguration().getAiEntityState() != null && ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram() != null && ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine() != null && ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm() != null && ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().getCurrentState() != null && ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().getCurrentState() instanceof FleetFormationingAbstract;
   }

   private boolean isInPickupProximity(Vector3f var1, Vector3f var2) {
      return Vector3fTools.diffLength(var1, var2) < 3.0F;
   }

   public boolean isActive(long var1) {
      SegmentPiece var3;
      return (var3 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var1, this.test)) != null && var3.isActive();
   }
}
