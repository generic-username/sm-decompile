package org.schema.game.common.controller.elements.rail.inv;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;

public class RailConnectionCollectionManager extends ControlBlockElementCollectionManager {
   public RailConnectionCollectionManager(SegmentPiece var1, SegmentController var2, RailConnectionElementManager var3) {
      super(var1, (short)29998, var2, var3);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return RailConnectionUnit.class;
   }

   public boolean needsUpdate() {
      return false;
   }

   public RailConnectionUnit getInstance() {
      return new RailConnectionUnit();
   }

   public boolean isUsingIntegrity() {
      return false;
   }

   protected void onChangedCollection() {
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
      }

   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[0];
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_INV_RAILCONNECTIONCOLLECTIONMANAGER_0;
   }
}
