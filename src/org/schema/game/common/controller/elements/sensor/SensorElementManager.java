package org.schema.game.common.controller.elements.sensor;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class SensorElementManager extends UsableControllableElementManager {
   public static boolean debug = false;

   public SensorElementManager(SegmentController var1) {
      super((short)980, (short)405, var1);
   }

   public ControllerManagerGUI getGUIUnitValues(SensorUnit var1, SensorCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return null;
   }

   protected String getTag() {
      return "sensor";
   }

   public SensorCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new SensorCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SENSOR_SENSORELEMENTMANAGER_0;
   }

   protected void playSound(SensorUnit var1, Transform var2) {
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
   }
}
