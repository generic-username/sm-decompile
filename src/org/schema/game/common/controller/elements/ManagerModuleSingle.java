package org.schema.game.common.controller.elements;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.world.Segment;
import org.schema.schine.graphicsengine.core.Timer;

public class ManagerModuleSingle extends ManagerModule {
   private final short controllerID;
   private final UsableControllableSingleElementManager elementManager;

   public ManagerModuleSingle(UsableControllableSingleElementManager var1, short var2, short var3) {
      super(var1, var3);
      this.elementManager = var1;
      this.controllerID = var2;
   }

   public short getControllerID() {
      return this.controllerID;
   }

   public void addControlledBlock(Vector3i var1, short var2, Vector3i var3, short var4) {
      this.getCollectionManager().addModded(var3, var4);
      this.elementManager.onControllerChange();
   }

   public void onFullyLoaded() {
   }

   public void clear() {
      this.getCollectionManager().clear();
      this.elementManager.totalSize = 0;
   }

   public UsableControllableSingleElementManager getElementManager() {
      return this.elementManager;
   }

   public void onConnectionRemoved(Vector3i var1, Vector3i var2, short var3) {
      this.getCollectionManager().remove(var2);
      this.elementManager.onControllerChange();
   }

   public void addElement(long var1, short var3) {
      this.getCollectionManager().doAdd(var1, var3);
   }

   public ElementCollectionManager getCollectionManager() {
      assert this.getElementManager() != null;

      assert this.getElementManager().getCollection() != null;

      return this.getElementManager().getCollection();
   }

   public void removeElement(byte var1, byte var2, byte var3, Segment var4) {
      this.getCollectionManager().remove(ElementCollection.getIndex(var1, var2, var3, var4));
   }

   public void update(Timer var1, long var2) {
      ElementCollectionManager var4 = this.elementManager.getCollection();

      assert var4 != null;

      var4.updateStructure(var2);
      if (var4.needsUpdate()) {
         var4.update(var1);
      }

   }

   public boolean needsAnyUpdate() {
      ElementCollectionManager var1;
      return (var1 = this.elementManager.getCollection()).rawCollection != null && var1.rawCollection.size() > 0 && var1.needsUpdate() || this.getElementManager().isUpdatable();
   }
}
