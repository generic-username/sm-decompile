package org.schema.game.common.controller.elements;

import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.schema.game.common.util.FastCopyLongOpenHashSet;

public class ElementCollectionCalculationThreadManager extends Thread {
   private final ObjectArrayList queue = new ObjectArrayList();
   private boolean finished;
   private ElementCollectionCalculationThreadExecution lastQueueElement;
   private boolean onServer;
   private ElementCollectionCalculationThreadManager.SizedExecutor[] executors;

   public ElementCollectionCalculationThreadManager(boolean var1) {
      super(var1 ? "ElementCollectionCalculationThreadManagerServer" : "ElementCollectionCalculationThreadManagerClient");
      this.onServer = var1;
      this.setDaemon(true);
      this.setPriority(4);
      this.executors = new ElementCollectionCalculationThreadManager.SizedExecutor[]{new ElementCollectionCalculationThreadManager.SizedExecutor(25, 32, var1), new ElementCollectionCalculationThreadManager.SizedExecutor(15, 256, var1), new ElementCollectionCalculationThreadManager.SizedExecutor(7, 1024, var1), new ElementCollectionCalculationThreadManager.SizedExecutor(5, -1, var1)};
      ElementCollectionCalculationThreadManager.SizedExecutor[] var4;
      int var2 = (var4 = this.executors).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         var4[var3].start();
      }

   }

   public void enqueue(ElementCollectionCalculationThreadExecution var1) {
      synchronized(this.queue) {
         if (!this.queue.contains(var1)) {
            this.queue.add(var1);
            this.queue.notify();
         }

      }
   }

   public void run() {
      try {
         while(!this.finished) {
            ElementCollectionCalculationThreadExecution var1;
            synchronized(this.queue) {
               while(this.queue.isEmpty()) {
                  try {
                     this.queue.wait(5000L);
                  } catch (Exception var7) {
                     var7.printStackTrace();
                  }

                  if (this.finished) {
                     return;
                  }
               }

               var1 = (ElementCollectionCalculationThreadExecution)this.queue.remove(0);
            }

            this.lastQueueElement = var1;
            synchronized(var1.getMan().getSegmentController().getState()) {
               if (!var1.getMan().getSegmentController().getState().getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(var1.getMan().getSegmentController().getId())) {
                  System.err.println("[ElColManTh][" + var1.getMan().getSegmentController().getState() + "] " + var1.getMan() + " not executing Element Collection update because segmentController no longer exists");
                  continue;
               }
            }

            for(int var2 = 0; var2 < this.executors.length; ++var2) {
               if (var1.getRawSize() <= this.executors[var2].maxRaw || var2 == this.executors.length - 1) {
                  this.executors[var2].enqueuePriv(var1);
                  break;
               }
            }
         }

      } catch (Throwable var10) {
         throw var10;
      }
   }

   public int getSize() {
      return this.queue.size();
   }

   public ElementCollectionCalculationThreadExecution getLastQueueElement() {
      return this.lastQueueElement;
   }

   public void onStop() {
      this.finished = true;
      ElementCollectionCalculationThreadManager.SizedExecutor[] var1;
      int var2 = (var1 = this.executors).length;

      int var3;
      for(var3 = 0; var3 < var2; ++var3) {
         var1[var3].finished = true;
      }

      synchronized(this.queue) {
         this.queue.notify();
      }

      var2 = (var1 = this.executors).length;

      for(var3 = 0; var3 < var2; ++var3) {
         var1[var3].onStop();
      }

   }

   static class ElementCollectionCalculationThread implements Runnable, CollectionCalculationCallback {
      private final LongArrayList rawCollection;
      private final FastCopyLongOpenHashSet closedCollection;
      private final LongArrayList openCollection;
      ElementCollectionCalculationThreadExecution nextQueueElement;
      private ElementCollectionCalculationThreadManager.SizedExecutor sizedExecutor;
      private int expected;
      private final FastCopyLongOpenHashSet totalSet;
      private boolean onServer;

      public ElementCollectionCalculationThread(int var1, boolean var2) {
         if (var1 <= 0) {
            var1 = 1024;
         }

         this.onServer = var2;
         this.rawCollection = new LongArrayList(var1);
         this.closedCollection = new FastCopyLongOpenHashSet(var1);
         this.totalSet = new FastCopyLongOpenHashSet(var1);
         this.openCollection = new LongArrayList(var1);
         this.expected = var1;
      }

      public void init(ElementCollectionCalculationThreadExecution var1, ElementCollectionCalculationThreadManager.SizedExecutor var2) {
         this.nextQueueElement = var1;
         this.sizedExecutor = var2;
      }

      public void callback(ElementCollectionCalculationThreadExecution var1) {
      }

      public void run() {
         try {
            if (this.nextQueueElement.getMan().flagPrepareUpdate(this.closedCollection)) {
               this.totalSet.deepApplianceCopy(this.closedCollection);
               this.rawCollection.addAll(this.closedCollection);
               this.nextQueueElement.initialize(this.closedCollection, this.totalSet, this.rawCollection, this.openCollection, this);
               this.nextQueueElement.process();
               this.nextQueueElement.flagUpdateFinished();
            } else {
               this.nextQueueElement.getMan().updateInProgress = Long.MIN_VALUE;
            }
         } finally {
            this.sizedExecutor.addBackToPool(this);
            this.totalSet.clear();
            this.openCollection.clear();
            this.closedCollection.clear();
            this.rawCollection.clear();
            this.openCollection.trim(this.expected);
            this.closedCollection.trim(this.expected);
            this.rawCollection.trim(this.expected);
         }

      }
   }

   static class SizedExecutor extends Thread {
      private final ObjectArrayList privQueue = new ObjectArrayList();
      private final ExecutorService executor;
      private final int maxRaw;
      private final ObjectArrayFIFOQueue threadDataPool;
      public boolean finished;
      private boolean onServer;

      public SizedExecutor(int var1, int var2, boolean var3) {
         super(var3 ? "ServerECCalcExecutor" : "ClientECCalcExecutor_" + (var2 < 0 ? "UNLIMITED" : var2));
         this.setPriority(1);
         this.onServer = var3;
         this.threadDataPool = new ObjectArrayFIFOQueue(var1);
         this.executor = Executors.newFixedThreadPool(var1);

         for(int var4 = 0; var4 < var1; ++var4) {
            this.threadDataPool.enqueue(new ElementCollectionCalculationThreadManager.ElementCollectionCalculationThread(var2, var3));
            this.executor.execute(new Runnable() {
               public void run() {
                  try {
                     Thread.sleep(2000L);
                  } catch (InterruptedException var1) {
                     var1.printStackTrace();
                  }
               }
            });
         }

         this.maxRaw = var2;
      }

      public void run() {
         try {
            while(!this.finished) {
               ElementCollectionCalculationThreadExecution var1;
               synchronized(this.privQueue) {
                  while(this.privQueue.isEmpty()) {
                     try {
                        this.privQueue.wait(20000L);
                     } catch (Exception var10) {
                        var10.printStackTrace();
                     }

                     if (this.finished) {
                        return;
                     }
                  }

                  var1 = (ElementCollectionCalculationThreadExecution)this.privQueue.remove(0);
               }

               ElementCollectionCalculationThreadManager.ElementCollectionCalculationThread var2;
               synchronized(this.threadDataPool) {
                  while(this.threadDataPool.isEmpty()) {
                     try {
                        this.threadDataPool.wait(20000L);
                     } catch (InterruptedException var9) {
                        var9.printStackTrace();
                     }

                     if (this.finished) {
                        return;
                     }
                  }

                  var2 = (ElementCollectionCalculationThreadManager.ElementCollectionCalculationThread)this.threadDataPool.dequeue();
               }

               var2.init(var1, this);
               this.executor.execute(var2);
            }

         } finally {
            this.executor.shutdown();
         }
      }

      public void enqueuePriv(ElementCollectionCalculationThreadExecution var1) {
         synchronized(this.privQueue) {
            if (!this.privQueue.contains(var1)) {
               this.privQueue.add(var1);
               this.privQueue.notify();
            }

         }
      }

      public void addBackToPool(ElementCollectionCalculationThreadManager.ElementCollectionCalculationThread var1) {
         synchronized(this.threadDataPool) {
            this.threadDataPool.enqueue(var1);
            this.threadDataPool.notify();
         }
      }

      public void onStop() {
         synchronized(this.threadDataPool) {
            this.threadDataPool.notify();
         }

         try {
            this.executor.shutdownNow();
         } catch (Exception var3) {
            var3.printStackTrace();
         }
      }
   }
}
