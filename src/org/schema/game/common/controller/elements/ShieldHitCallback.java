package org.schema.game.common.controller.elements;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SectorNotFoundException;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;

public class ShieldHitCallback {
   public boolean hasHit;
   public float xWorld;
   public float yWorld;
   public float zWorld;
   private double damage;
   public float xLocalBlock;
   public float yLocalBlock;
   public float zLocalBlock;
   private Vector3f tmp = new Vector3f();
   public Damager damager;
   public DamageDealerType damageType;
   public long weaponId;
   public SegmentController originalHitEntity;
   public SegmentController nowHitEntity;
   public int projectileSectorId;
   private final Transform inputTransform;
   private final Transform outputTransform;

   public ShieldHitCallback() {
      this.damageType = DamageDealerType.GENERAL;
      this.inputTransform = new Transform();
      this.outputTransform = new Transform();
   }

   public void reset() {
      this.hasHit = false;
      this.damager = null;
      this.originalHitEntity = null;
      this.nowHitEntity = null;
      this.damageType = DamageDealerType.GENERAL;
      this.inputTransform.setIdentity();
      this.outputTransform.setIdentity();
      this.setDamage(0.0D);
   }

   public void onShieldOutage(ShieldLocal var1) {
      var1.onOutage(true);
   }

   public void onShieldDamage(ShieldLocal var1) {
      var1.onDamage(this.getDamage());
   }

   public void convertWorldHitPoint(SegmentController var1) throws SectorNotFoundException {
      this.tmp.set(this.xWorld, this.yWorld, this.zWorld);
      if (!var1.isOnServer()) {
         var1.getClientTransformInverse().transform(this.tmp);
      } else {
         if (this.projectileSectorId != var1.getSectorId()) {
            Sector var2 = ((GameServerState)var1.getState()).getUniverse().getSector(var1.getSectorId());
            Sector var3;
            if ((var3 = ((GameServerState)var1.getState()).getUniverse().getSector(this.projectileSectorId)) != null && var2 != null) {
               this.inputTransform.origin.set(this.tmp);
               SimpleTransformableSendableObject.calcWorldTransformRelative(var2.getId(), var2.pos, var3.getSectorId(), this.inputTransform, var2.getState(), true, this.outputTransform, var1.v);
               this.tmp.set(this.outputTransform.origin);
            } else {
               if (var3 == null) {
                  throw new SectorNotFoundException(this.projectileSectorId);
               }

               if (var2 == null) {
                  throw new SectorNotFoundException(var1.getSectorId());
               }
            }
         }

         var1.getWorldTransformInverse().transform(this.tmp);
      }

      this.xLocalBlock = this.tmp.x + 16.0F;
      this.yLocalBlock = this.tmp.y + 16.0F;
      this.zLocalBlock = this.tmp.z + 16.0F;
   }

   public String getLocalPosString() {
      return this.xLocalBlock + ", " + this.yLocalBlock + ", " + this.zLocalBlock;
   }

   public String getWorldPosString() {
      return this.xWorld + ", " + this.yWorld + ", " + this.zWorld;
   }

   public double getDamage() {
      return this.damage;
   }

   public void setDamage(double var1) {
      this.damage = var1;
   }
}
