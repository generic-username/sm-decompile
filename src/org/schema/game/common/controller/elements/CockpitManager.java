package org.schema.game.common.controller.elements;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.Long2ObjectLinkedOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongBidirectionalIterator;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.ship.InShipControlManager;
import org.schema.game.client.controller.manager.ingame.ship.ShipExternalFlightController;
import org.schema.game.client.view.camera.InShipCamera;
import org.schema.game.client.view.camera.ShipOffsetCameraViewable;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.network.objects.NetworkShip;
import org.schema.game.network.objects.remote.RemoteCockpitManager;
import org.schema.schine.graphicsengine.camera.Camera;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.remote.LongTransformPair;
import org.schema.schine.network.objects.remote.RemoteLongTransformationPair;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;
import org.schema.schine.resource.tag.TagSerializableLong2TransformMap;

public class CockpitManager implements SerialializationInterface, TagSerializable {
   private static final byte TAG_VERSION = 0;
   private final TagSerializableLong2TransformMap metaCockpitMap;
   private final TagSerializableLong2TransformMap cockpits;
   private final ShipManagerContainer c;
   private int cockpitIndex = -1;
   private Vector3i cockpitTmp = new Vector3i();

   public CockpitManager(ShipManagerContainer var1) {
      this.c = var1;
      this.metaCockpitMap = new TagSerializableLong2TransformMap();
      this.metaCockpitMap.ignoreIdentTransforms = true;
      this.cockpits = new TagSerializableLong2TransformMap();
      this.cockpits.ignoreIdentTransforms = true;
      Transform var2 = new Transform(TransformTools.ident);
      this.metaCockpitMap.defaultReturnValue(var2);
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      this.metaCockpitMap.serialize(var1, var2);
      this.cockpits.serialize(var1, var2);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.metaCockpitMap.deserialize(var1, var2, var3);
      TagSerializableLong2TransformMap var4;
      (var4 = new TagSerializableLong2TransformMap()).deserialize(var1, var2, var3);
      this.metaCockpitMap.putAll(var4);
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2;
      (var2 = var1.getStruct())[0].getByte();
      this.metaCockpitMap.putAll((TagSerializableLong2TransformMap)var2[1].getValue());
      this.cockpits.putAll((TagSerializableLong2TransformMap)var2[2].getValue());
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.SERIALIZABLE, (String)null, this.metaCockpitMap), new Tag(Tag.Type.SERIALIZABLE, (String)null, this.cockpits), FinishTag.INST});
   }

   public Long2ObjectLinkedOpenHashMap getCockpits() {
      return this.cockpits;
   }

   public void removeCockpit(long var1) {
      this.cockpits.remove(var1);
      this.metaCockpitMap.remove(var1);
   }

   public void addCockpit(long var1) {
      this.cockpits.put(var1, new Transform((Transform)this.metaCockpitMap.remove(var1)));
   }

   public void initFromNetworkObject(NetworkShip var1) {
      for(int var2 = 0; var2 < var1.cockpitManagerBuffer.getReceiveBuffer().size(); ++var2) {
         var1.cockpitManagerBuffer.getReceiveBuffer().get(var2);
      }

   }

   public void updateToFullNetworkObject(NetworkShip var1) {
      if (this.cockpits.size() > 0 || this.metaCockpitMap.size() > 0) {
         var1.cockpitManagerBuffer.add(new RemoteCockpitManager(this, var1));
      }

   }

   public void handleKeyEvent(KeyEventInterface var1, ShipExternalFlightController var2) {
      if (!this.cockpits.isEmpty() && var2.getEntered().getType() == 1) {
         if (KeyboardMappings.SWITCH_COCKPIT_PREVIOUS.isEventKey(var1, var2.getState())) {
            if (this.cockpitIndex == 0) {
               this.cockpitIndex = -1;
               this.resetShipViewPos(var2);
               return;
            }

            if (this.cockpitIndex < 0) {
               this.cockpitIndex = this.cockpits.size() - 1;
            } else {
               --this.cockpitIndex;
            }

            this.setShipViewPos(var2);
            return;
         }

         if (KeyboardMappings.SWITCH_COCKPIT_NEXT.isEventKey(var1, var2.getState())) {
            if (this.cockpitIndex + 1 >= this.cockpits.size()) {
               this.cockpitIndex = -1;
               this.resetShipViewPos(var2);
               return;
            }

            if (this.cockpitIndex < 0) {
               this.cockpitIndex = 0;
            } else {
               ++this.cockpitIndex;
            }

            this.setShipViewPos(var2);
         }
      }

   }

   public StateInterface getState() {
      return this.c.getState();
   }

   public void onSwitch(ShipExternalFlightController var1) {
      if (this.cockpitIndex >= 0 && this.cockpitIndex < this.cockpits.size()) {
         this.setShipViewPos(var1);
      } else {
         this.cockpitIndex = -1;
         this.resetShipViewPos(var1);
      }
   }

   private void resetShipViewPos(ShipExternalFlightController var1) {
      System.out.println("SHIPCAMERA: resetShipViewPos called");
      InShipControlManager var2 = var1.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager();
      var1.shipCamera = new InShipCamera(var2.getShipControlManager(), Controller.getCamera(), var1.getEntered());
      var1.shipCamera.setCameraStartOffset(0.0F);
      Controller.setCamera(var1.shipCamera);
      ((InShipCamera)var1.shipCamera).docked = var1.getEntered().getSegmentController().getDockingController().isDocked() || var1.getEntered().getSegmentController().railController.isDockedOrDirty();
   }

   private void setShipViewPos(ShipExternalFlightController var1) {
      Vector3i var2 = ElementCollection.getPosFromIndex(this.getCurrentCockpitBlock(), new Vector3i());
      SegmentPiece var3;
      if ((var3 = var1.getShip().getSegmentBuffer().getPointUnsave(var2)) != null) {
         InShipControlManager var4 = var1.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager();
         var1.shipCamera = new InShipCamera(var4.getShipControlManager(), Controller.getCamera(), var3);
         var1.shipCamera.setCameraStartOffset(0.0F);
         Controller.setCamera(var1.shipCamera);
         var2.x -= 16;
         var2.y -= 16;
         var2.z -= 16;
         ((ShipOffsetCameraViewable)var1.shipCamera.getViewable()).getPosMod().set(var2);
         Transform var5 = this.getTransform(var1.getState().getPlayer().getCockpit().block);
         if (!var1.getState().getPlayer().getCockpit().equalsBlockPos(var2)) {
            var1.getState().getPlayer().getCockpit().changeBlockClient(var2, var5, var1.shipCamera);
         }

      } else {
         this.resetCockpitIndex();
         this.resetShipViewPos(var1);
      }
   }

   public void updateLocal(ShipExternalFlightController var1, Camera var2, Timer var3) {
      Transform var5;
      if (this.cockpitIndex >= 0) {
         this.cockpitTmp.set(((ShipOffsetCameraViewable)var2.getViewable()).getPosMod());
         Vector3i var10000 = this.cockpitTmp;
         var10000.x += 16;
         var10000 = this.cockpitTmp;
         var10000.y += 16;
         var10000 = this.cockpitTmp;
         var10000.z += 16;
         if (!this.cockpits.containsKey(ElementCollection.getIndex(this.cockpitTmp))) {
            this.cockpitIndex = -1;
            Vector3i var4;
            var10000 = var4 = var1.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getEntered().getAbsolutePos(new Vector3i());
            var10000.x -= 16;
            var4.y -= 16;
            var4.z -= 16;
            ((ShipOffsetCameraViewable)var2.getViewable()).getPosMod().set(var4);
         }

         if (!var1.getState().getPlayer().getCockpit().equalsBlockPos(this.cockpitTmp)) {
            var5 = this.getTransform(ElementCollection.getIndex(this.cockpitTmp));
            var1.getState().getPlayer().getCockpit().changeBlockClient(this.cockpitTmp, var5, var1.shipCamera);
            return;
         }
      } else if (!var1.getState().getPlayer().getCockpit().equalsBlockPos(Ship.core)) {
         var5 = this.getTransform(ElementCollection.getIndex(Ship.core));
         var1.getState().getPlayer().getCockpit().changeBlockClient(Ship.core, var5, var1.shipCamera);
      }

   }

   public Transform getTransform(long var1) {
      Transform var3;
      if ((var3 = (Transform)this.cockpits.get(var1)) == null) {
         (var3 = new Transform()).setIdentity();
      } else {
         var3 = new Transform(var3);
      }

      return var3;
   }

   public long getCurrentCockpitBlock() {
      if (this.cockpitIndex >= 0 && !this.cockpits.isEmpty() && this.cockpitIndex < this.cockpits.size()) {
         LongBidirectionalIterator var1;
         long var2 = (var1 = this.cockpits.keySet().iterator()).nextLong();

         for(int var4 = 0; var4 < this.cockpitIndex; ++var4) {
            var2 = var1.nextLong();
         }

         return var2;
      } else {
         return ElementCollection.getIndex(Ship.core);
      }
   }

   public void resetCockpitIndex() {
      this.cockpitIndex = -1;
   }

   public void setTransformFor(Cockpit var1, Transform var2) {
      this.cockpits.put(var1.block, new Transform(var2));
      var1.changeBlockClient(ElementCollection.getPosFromIndex(var1.block, new Vector3i()), new Transform(var2), (Camera)null);
   }

   public void updateFromNetworkObject() {
      ObjectArrayList var1 = ((Ship)this.c.getSegmentController()).getNetworkObject().cockpitManagerUpdateBuffer.getReceiveBuffer();

      for(int var2 = 0; var2 < var1.size(); ++var2) {
         LongTransformPair var3 = (LongTransformPair)((RemoteLongTransformationPair)var1.get(var2)).get();
         this.cockpits.put(var3.l, new Transform(var3.t));
         if (this.c.isOnServer()) {
            this.send(var3.l, var3.t);
         }
      }

   }

   public void send(long var1, Transform var3) {
      ((Ship)this.c.getSegmentController()).getNetworkObject().cockpitManagerUpdateBuffer.add(new RemoteLongTransformationPair(new LongTransformPair(var1, new Transform(var3)), ((Ship)this.c.getSegmentController()).getNetworkObject()));
   }
}
