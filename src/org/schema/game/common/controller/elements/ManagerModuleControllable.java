package org.schema.game.common.controller.elements;

import org.schema.schine.graphicsengine.core.Timer;

public class ManagerModuleControllable extends ManagerModule {
   private final short controllerID;

   public ManagerModuleControllable(UsableControllableElementManager var1, short var2, short var3) {
      super(var1, var2);
      this.controllerID = var3;
   }

   public short getControllerID() {
      return this.controllerID;
   }

   public void update(Timer var1, long var2) {
   }

   public void onFullyLoaded() {
   }

   public boolean needsAnyUpdate() {
      return ((UsableControllableElementManager)this.getElementManager()).isUpdatable() || ((UsableControllableElementManager)this.getElementManager()).getCollectionManagers().size() > 0 && ((ControlBlockElementCollectionManager)((UsableControllableElementManager)this.getElementManager()).getCollectionManagers().get(0)).needsUpdate();
   }
}
