package org.schema.game.common.controller.elements.cloaking;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector4f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerActivityInterface;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ManagerReloadInterface;
import org.schema.game.common.controller.elements.ManagerUpdatableInterface;
import org.schema.game.common.controller.elements.StealthElementManagerInterface;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.controller.elements.UsableControllableSingleElementManager;
import org.schema.game.common.controller.elements.power.PowerAddOn;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.remote.RemoteValueUpdate;
import org.schema.game.network.objects.valueUpdate.CloakDelayValueUpdate;
import org.schema.game.network.objects.valueUpdate.NTValueUpdateInterface;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;
import org.schema.schine.ai.stateMachines.AiInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.InputState;

public class CloakingElementManager extends UsableControllableSingleElementManager implements ManagerActivityInterface, ManagerReloadInterface, ManagerUpdatableInterface, StealthElementManagerInterface {
   @ConfigurationElement(
      name = "PowerConsumedPerSecondPerBlock"
   )
   public static float CLOAKING_POWER_CONSUME_PER_TOTAL_BLOCK_AND_SECOND = 100.0F;
   @ConfigurationElement(
      name = "ReuseDelayOnActionMs"
   )
   public static int REUSE_DELAY_ON_ACTION_MS = 6000;
   @ConfigurationElement(
      name = "ReuseDelayOnModificationMs"
   )
   public static int REUSE_DELAY_ON_MODIFICATION_MS = 6000;
   @ConfigurationElement(
      name = "ReuseDelayOnSwitchedOffMs"
   )
   public static int REUSE_DELAY_ON_SWITCHED_OFF_MS = 1000;
   @ConfigurationElement(
      name = "ReuseDelayOnHitMs"
   )
   public static int REUSE_DELAY_ON_HIT_MS = 10000;
   @ConfigurationElement(
      name = "ReuseDelayOnScanMs"
   )
   public static int REUSE_DELAY_ON_SCAN_MS = 30000;
   @ConfigurationElement(
      name = "ReuseDelayOnNoPowerMs"
   )
   private static int REUSE_DELAY_ON_NO_POWER_MS = 6000;
   private final Vector4f reloadColor = new Vector4f(0.0F, 0.8F, 1.0F, 0.4F);
   private boolean cloaked = false;
   private long cloakStartTime;
   private Vector3i controlledFromOrig = new Vector3i();
   private Vector3i controlledFrom = new Vector3i();
   private int delay;
   private boolean wasCloaked;

   public CloakingElementManager(SegmentController var1) {
      super(var1, CloakingCollectionManager.class);
   }

   public void init(ManagerContainer var1) {
      super.init(var1);
   }

   public float getActualCloak() {
      return ((CloakingCollectionManager)this.getCollection()).getTotalCloak();
   }

   public long getCloakStartTime() {
      return this.cloakStartTime;
   }

   public void setCloakStartTime(long var1) {
      this.cloakStartTime = var1;
   }

   public boolean isActive() {
      return this.isCloaked();
   }

   public boolean isCloaked() {
      return this.getSegmentController().isOnServer() ? this.cloaked : ((Ship)this.getSegmentController()).isCloakedFor((SimpleTransformableSendableObject)null);
   }

   public void setCloaked(boolean var1) {
      this.cloaked = var1;
   }

   public void onControllerChange() {
   }

   public void onHit() {
      this.stopCloak(REUSE_DELAY_ON_HIT_MS);
   }

   public void stopCloak(int var1) {
      if (this.getSegmentController().isOnServer() && this.isCloaked()) {
         System.err.println("[CLOAK] " + this.getSegmentController().getState() + " " + this.getSegmentController() + " CLOAKING STOPPED");
         this.delay = var1;
         this.setCloaked(false);
         this.setCloakStartTime(System.currentTimeMillis());
         this.sendDelayUpdate();
      }

   }

   public int getDelay() {
      return this.delay;
   }

   public void setDelay(int var1) {
      this.delay = var1;
      this.setCloakStartTime(System.currentTimeMillis());
   }

   public void sendDelayUpdate() {
      assert this.getSegmentController().isOnServer();

      CloakDelayValueUpdate var1 = new CloakDelayValueUpdate();

      assert var1.getType() == ValueUpdate.ValTypes.CLOAK_DELAY;

      var1.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   public float getPowerConsumption(float var1) {
      return this.getSegmentController().getMass() * 10.0F * var1 * CLOAKING_POWER_CONSUME_PER_TOTAL_BLOCK_AND_SECOND;
   }

   public void update(Timer var1) {
      PowerAddOn var2 = ((PowerManagerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getPowerAddOn();
      float var3 = this.getPowerConsumption(var1.getDelta());
      if (!this.getSegmentController().isOnServer() && this.wasCloaked != this.isCloaked()) {
         this.setCloakStartTime(System.currentTimeMillis());
      }

      if (this.getSegmentController().isOnServer()) {
         if (this.isCloaked() && !var2.consumePowerInstantly((double)var3)) {
            this.stopCloak(REUSE_DELAY_ON_NO_POWER_MS);
         }
      } else if (this.getSegmentController() instanceof Ship && (Boolean)((Ship)this.getSegmentController()).getNetworkObject().cloaked.get()) {
         var2.consumePowerInstantly((double)var3);
      }

      if (this.isCloaked() && ((PlayerControllable)this.getSegmentController()).getAttachedPlayers().isEmpty() && !((AiInterface)this.getSegmentController()).getAiConfiguration().isActiveAI() && this.getSegmentController().isOnServer()) {
         this.stopCloak(0);
      }

      this.wasCloaked = this.isCloaked();
   }

   public ControllerManagerGUI getGUIUnitValues(CloakingUnit var1, CloakingCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ControllerManagerGUI.create((GameClientState)this.getState(), "Cloaking Unit", var1);
   }

   protected String getTag() {
      return "cloaking";
   }

   public CloakingCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new CloakingCollectionManager(this.getSegmentController(), this);
   }

   protected void playSound(CloakingUnit var1, Transform var2) {
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
      if (var1.isFlightControllerActive()) {
         long var3;
         if ((var3 = var2.currentTime - this.getCloakStartTime()) >= 600L) {
            if (var3 >= (long)this.delay) {
               if (this.getCollection() != null && !((CloakingCollectionManager)this.getCollection()).getElementCollections().isEmpty()) {
                  if (this.getSegmentController().isOnServer()) {
                     this.getActualCloak();
                     if (this.getSegmentController().isOnServer()) {
                        if (!this.isCloaked()) {
                           System.err.println("[CLOAK] " + this.getSegmentController().getState() + " " + this.getSegmentController() + " CLOAKING STARTED");
                           this.setCloakStartTime(System.currentTimeMillis());
                           this.setCloaked(true);
                           return;
                        }

                        this.stopCloak(REUSE_DELAY_ON_SWITCHED_OFF_MS);
                     }
                  }

               }
            }
         }
      }
   }

   public double calculateStealthIndex(double var1) {
      return ((CloakingCollectionManager)this.getCollection()).getTotalSize() > 1 ? Math.min(1.0D, this.getPowerManager().getRechargeForced() / (double)this.getPowerConsumption(1.0F)) * var1 : 0.0D;
   }

   public String getReloadStatus(long var1) {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_CLOAKING_CLOAKINGELEMENTMANAGER_0;
   }

   public void drawReloads(Vector3i var1, Vector3i var2, long var3) {
      long var5 = System.currentTimeMillis() - this.getCloakStartTime();
      if (!this.isCloaked() && var5 < (long)this.delay && this.delay > 0) {
         float var7 = 1.0F - (float)var5 / (float)this.delay;
         UsableControllableElementManager.drawReload((InputState)this.getState(), var1, var2, this.reloadColor, false, var7);
      }

   }

   public boolean canUpdate() {
      return true;
   }

   public void onNoUpdate(Timer var1) {
   }

   public int getCharges() {
      return 0;
   }

   public int getMaxCharges() {
      return 0;
   }
}
