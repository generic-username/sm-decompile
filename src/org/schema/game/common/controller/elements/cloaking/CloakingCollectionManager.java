package org.schema.game.common.controller.elements.cloaking;

import java.util.Iterator;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.weapon.WeaponRowElement;
import org.schema.game.client.view.gui.weapon.WeaponRowElementInterface;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.input.InputType;
import org.schema.schine.input.KeyboardMappings;

public class CloakingCollectionManager extends ElementCollectionManager implements PlayerUsableInterface {
   private float totalCloak;
   private long usableId = Long.MIN_VALUE;

   public CloakingCollectionManager(SegmentController var1, CloakingElementManager var2) {
      super((short)22, var1, var2);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return CloakingUnit.class;
   }

   public boolean needsUpdate() {
      return false;
   }

   public CloakingUnit getInstance() {
      return new CloakingUnit();
   }

   public void handleMouseEvent(ControllerStateUnit var1, MouseEvent var2) {
   }

   public void onLogicActivate(SegmentPiece var1, boolean var2, Timer var3) {
   }

   protected void onChangedCollection() {
      this.refreshMaxCloak();
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
      }

      CloakingElementManager var1;
      if (this.getElementCollections().isEmpty() && (var1 = ((ShipManagerContainer)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getCloakElementManager()).isCloaked()) {
         var1.stopCloak(CloakingElementManager.REUSE_DELAY_ON_MODIFICATION_MS);
      }

   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[0];
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_CLOAKING_CLOAKINGCOLLECTIONMANAGER_0;
   }

   public float getTotalCloak() {
      return this.totalCloak;
   }

   public void setTotalCloak(float var1) {
      this.totalCloak = var1;
   }

   private void refreshMaxCloak() {
      this.setTotalCloak(0.0F);
      Iterator var1 = this.getElementCollections().iterator();

      while(var1.hasNext()) {
         CloakingUnit var2;
         (var2 = (CloakingUnit)var1.next()).refreshCloakingCapabilities();
         this.setTotalCloak(this.getTotalCloak() + var2.getCloak());
      }

   }

   public float getSensorValue(SegmentPiece var1) {
      return this.getSegmentController().isCloakedFor((SimpleTransformableSendableObject)null) ? 1.0F : 0.0F;
   }

   public WeaponRowElementInterface getWeaponRow() {
      SegmentPiece var1;
      return !this.getElementCollections().isEmpty() && (var1 = ((CloakingUnit)this.getElementCollections().get(0)).getElementCollectionId()) != null ? new WeaponRowElement(var1) : null;
   }

   public boolean isControllerConnectedTo(long var1, short var3) {
      return true;
   }

   public void doAdd(long var1, short var3) {
      if (this.usableId == Long.MIN_VALUE) {
         this.usableId = var1;
      }

      if (this.rawCollection == null || this.rawCollection.isEmpty()) {
         ((CloakingElementManager)this.getElementManager()).getManagerContainer().addPlayerUsable(this);
      }

      super.doAdd(var1, var3);
   }

   public boolean doRemove(long var1) {
      boolean var3 = super.doRemove(var1);
      if (this.rawCollection.isEmpty()) {
         ((CloakingElementManager)this.getElementManager()).getManagerContainer().removePlayerUsable(this);
         this.usableId = Long.MIN_VALUE;
      }

      return var3;
   }

   public boolean isPlayerUsable() {
      return true;
   }

   public long getUsableId() {
      return this.usableId;
   }

   public void handleControl(ControllerStateInterface var1, Timer var2) {
      if (var1.clickedOnce(0) || var1.clickedOnce(1)) {
         ((CloakingElementManager)this.getElementManager()).handle(var1, var2);
      }

   }

   public void handleKeyEvent(ControllerStateUnit var1, KeyboardMappings var2) {
   }

   public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
      var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), "Cloak/Uncloak", var3, ContextFilter.IMPORTANT);
   }
}
