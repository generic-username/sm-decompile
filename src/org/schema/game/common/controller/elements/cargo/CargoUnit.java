package org.schema.game.common.controller.elements.cargo;

import it.unimi.dsi.fastutil.longs.LongComparator;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.world.DrawableRemoteSegment;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentDataWriteException;

public class CargoUnit extends ElementCollection {
   private final ObjectOpenHashSet affected = new ObjectOpenHashSet();
   private SegmentPiece tmp = new SegmentPiece();
   private final Random r = new Random();
   private final ShortArrayList countsPerY = new ShortArrayList();
   private final ShortArrayList cachedRow = new ShortArrayList();
   private static final LongComparator comp = new LongComparator() {
      public final int compare(Long var1, Long var2) {
         return this.compare(var1, var2);
      }

      public final int compare(long var1, long var3) {
         int var5 = ElementCollection.getPosY(var1);
         int var2 = ElementCollection.getPosY(var3);
         return var5 - var2;
      }
   };

   public String toString() {
      return "CargoUnit " + super.toString();
   }

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((CargoElementManager)((CargoCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (CargoCollectionManager)this.elementCollectionManager, var2, var3);
   }

   public double updateBlocks(double var1, double var3) {
      this.r.setSeed(ElementCollection.getIndex(((CargoCollectionManager)this.elementCollectionManager).getControllerPos()));
      this.size();
      boolean var5 = false;
      int var6 = 0;
      double var9 = Math.max(1.0E-7D, this.getCapacity() + var3) / (double)this.size();

      for(int var17 = 0; var17 < this.countsPerY.size(); ++var17) {
         short var4 = this.countsPerY.get(var17);
         byte var7 = -1;
         boolean var8 = false;

         for(int var11 = 0; var11 < var4; ++var11) {
            byte var19;
            if (var1 <= 0.0D) {
               if (var7 > -2) {
                  var7 = 4;
               }

               var19 = 4;
            } else if (!var8 && (double)var4 * var9 > var1) {
               var7 = -2;
               var19 = (byte)(this.r.nextInt(3) + 1);
            } else {
               var7 = 0;
               var8 = true;
               var19 = 0;
            }

            if (this.cachedRow.size() > var17 && this.cachedRow.getShort(var17) < 0 || this.countsPerY.size() == 1 && var4 == 1) {
               if (this.getNeighboringCollection() == null) {
                  System.err.println("Exception: [CARGO] NCOL NULL");
               }

               if (var6 >= this.getNeighboringCollection().size()) {
                  System.err.println("Exception: [CARGO] OUT OF BOUNDS");
                  return 0.0D;
               }

               long var13 = this.getNeighboringCollection().getLong(var6);
               if (this.getSegmentController() == null) {
                  System.err.println("Exception: [CARGO] SEG CON NULL");
               }

               if (this.getSegmentController().getSegmentBuffer() == null) {
                  System.err.println("Exception: [CARGO] SEG BUFF NULL");
               }

               SegmentPiece var12;
               if ((var12 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var13, this.tmp)) != null && var12.getOrientation() != var19 && var12.getSegment() != null && var12.getSegment().getSegmentData() != null) {
                  try {
                     var12.getSegment().getSegmentData().setOrientation(var12.getInfoIndex(), var19);
                  } catch (SegmentDataWriteException var16) {
                     SegmentDataWriteException.replaceData(var12.getSegment());

                     try {
                        var12.getSegment().getSegmentData().setOrientation(var12.getInfoIndex(), var19);
                     } catch (SegmentDataWriteException var15) {
                        var15.printStackTrace();
                        throw new RuntimeException(var15);
                     }
                  }

                  if (!this.getSegmentController().isOnServer()) {
                     this.affected.add(var12.getSegment());
                  }
               }
            }

            ++var6;
            var1 -= var9;
         }

         this.cachedRow.set(var17, var7);
      }

      if (!this.getSegmentController().isOnServer()) {
         Iterator var18 = this.affected.iterator();

         while(var18.hasNext()) {
            ((DrawableRemoteSegment)((Segment)var18.next())).dataChanged(true);
         }

         this.affected.clear();
      }

      return var1;
   }

   public void resetBlocks() {
      Iterator var1 = this.getNeighboringCollection().iterator();

      while(true) {
         ObjectOpenHashSet var3;
         do {
            if (!var1.hasNext()) {
               return;
            }

            long var2 = (Long)var1.next();
            SegmentPiece var6 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var2, this.tmp);
            var3 = new ObjectOpenHashSet();
            if (var6 != null && var6.getSegment() != null && var6.getSegment().getSegmentData() != null) {
               try {
                  var6.getSegment().getSegmentData().setOrientation(var6.getInfoIndex(), (byte)4);
               } catch (SegmentDataWriteException var5) {
                  SegmentDataWriteException.replaceDataOnClient(var6.getSegment().getSegmentData());

                  try {
                     var6.getSegment().getSegmentData().setOrientation(var6.getInfoIndex(), (byte)4);
                  } catch (SegmentDataWriteException var4) {
                     var4.printStackTrace();
                     throw new RuntimeException(var4);
                  }
               }

               var3.add(var6.getSegment());
            }
         } while(this.getSegmentController().isOnServer());

         Iterator var7 = var3.iterator();

         while(var7.hasNext()) {
            ((DrawableRemoteSegment)((Segment)var7.next())).dataChanged(true);
         }
      }
   }

   public void calculateExtraDataAfterCreationThreaded(long var1, LongOpenHashSet var3) {
      Collections.sort(this.getNeighboringCollection(), comp);
      this.countsPerY.clear();
      short var6 = 0;
      int var2 = Integer.MIN_VALUE;
      int var7 = this.getNeighboringCollection().size();

      int var4;
      for(var4 = 0; var4 < var7; ++var4) {
         int var5 = ElementCollection.getPosY(this.getNeighboringCollection().getLong(var4));
         if (var2 == Integer.MIN_VALUE) {
            var2 = var5;
         }

         if (var5 != var2) {
            this.countsPerY.add(var6);
            var2 = var5;
            var6 = 0;
         }

         ++var6;
      }

      this.countsPerY.add(var6);

      for(var4 = 0; var4 < this.countsPerY.size(); ++var4) {
         this.cachedRow.add((short)-1);
      }

      if (!this.countsPerY.isEmpty()) {
         this.cachedRow.trim();
         this.countsPerY.trim();
      }

   }

   public double getCapacity() {
      return Math.pow((double)this.size() * ((CargoElementManager)((CargoCollectionManager)this.elementCollectionManager).getElementManager()).getCapacityPerBlockMult(), ((CargoElementManager)((CargoCollectionManager)this.elementCollectionManager).getElementManager()).getCapacityPerGroupQuadratic());
   }
}
