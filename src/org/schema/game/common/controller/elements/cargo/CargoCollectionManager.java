package org.schema.game.common.controller.elements.cargo;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.DrawableRemoteSegment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class CargoCollectionManager extends ControlBlockElementCollectionManager {
   private double lastInventoryVolume;
   private long lastBlockUpdate;
   private long lastBleedUpdate;
   private boolean flagCollectionChanged;
   private final SegmentPiece tmpPce = new SegmentPiece();

   public CargoCollectionManager(SegmentPiece var1, SegmentController var2, CargoElementManager var3) {
      super(var1, (short)689, var2, var3);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return CargoUnit.class;
   }

   public boolean isUsingIntegrity() {
      return false;
   }

   public boolean needsUpdate() {
      return true;
   }

   public CargoUnit getInstance() {
      return new CargoUnit();
   }

   protected void onChangedCollection() {
      super.onChangedCollection();
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
      }

      this.flagCollectionChanged = true;
   }

   public void remove(long var1) {
      super.remove(var1);
      SegmentPiece var5;
      if ((var5 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var1, this.tmpPce)) != null) {
         SegmentData var2;
         if ((var2 = var5.getSegment().getSegmentData()) != null) {
            try {
               var2.setOrientation(var5.getInfoIndex(), (byte)4);
            } catch (SegmentDataWriteException var4) {
               var2 = SegmentDataWriteException.replaceDataOnClient(var2);

               try {
                  var2.setOrientation(var5.getInfoIndex(), (byte)4);
               } catch (SegmentDataWriteException var3) {
                  var3.printStackTrace();
                  throw new RuntimeException(var3);
               }
            }
         }

         if (!this.getSegmentController().isOnServer() && var5.getSegment() != null) {
            ((DrawableRemoteSegment)var5.getSegment()).dataChanged(true);
         }
      }

   }

   protected void onRemovedCollection(long var1, CargoCollectionManager var3) {
      super.onRemovedCollection(var1, var3);

      for(int var4 = 0; var4 < this.getElementCollections().size(); ++var4) {
         ((CargoUnit)this.getElementCollections().get(var4)).resetBlocks();
      }

   }

   public void update(Timer var1) {
      super.update(var1);
      long var2 = !this.getSegmentController().isOnServer() ? 4000L : 9000L + (long)(Math.random() * 2000.0D);
      if ((Math.abs(this.lastInventoryVolume - this.getInventoryVolume()) > 5.0D || this.flagCollectionChanged) && var1.currentTime - this.lastBlockUpdate > var2 && (this.getSegmentController().isOnServer() || this.getSegmentController().isInClientRange())) {
         double var4 = this.getInventoryVolume();

         for(int var6 = 0; var6 < this.getElementCollections().size(); ++var6) {
            var4 = ((CargoUnit)this.getElementCollections().get(var6)).updateBlocks(var4, var6 == 0 ? ((CargoElementManager)this.getElementManager()).getInventoryBaseCapacity() : 0.0D);
         }

         this.lastInventoryVolume = this.getInventoryVolume();
         this.lastBlockUpdate = var1.currentTime;
         this.flagCollectionChanged = false;
      }

      if (this.getSegmentController().isOnServer() && ServerConfig.CARGO_BLEED_AT_OVER_CAPACITY.isOn() && var1.currentTime - this.lastBleedUpdate > 60000L) {
         Inventory var7;
         if ((var7 = ((CargoElementManager)this.getElementManager()).getManagerContainer().getInventory(this.getControllerPos())) != null && var7.isOverCapacity() && this.getSegmentController().isFullyLoadedWithDock() && System.currentTimeMillis() - this.getSegmentController().getTimeCreated() > 60000L) {
            var7.spawnVolumeInSpace(this.getSegmentController(), CargoElementManager.PERCENTAGE_BLEED_PER_MINUTE);
         }

         this.lastBleedUpdate = var1.currentTime;
      }

   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[0];
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_CARGO_CARGOCOLLECTIONMANAGER_0;
   }

   public double getInventoryVolume() {
      Inventory var1;
      return (var1 = ((CargoElementManager)this.getElementManager()).getManagerContainer().getInventory(this.getControllerPos())) != null ? var1.getVolume() : 0.0D;
   }

   public double getCapacity() {
      double var1 = ((CargoElementManager)this.getElementManager()).getInventoryBaseCapacity();

      for(int var3 = 0; var3 < this.getElementCollections().size(); ++var3) {
         var1 += ((CargoUnit)this.getElementCollections().get(var3)).getCapacity();
      }

      return this.getSegmentController().getConfigManager().apply(StatusEffectType.CARGO_VOLUME, var1);
   }

   public float getSensorValue(SegmentPiece var1) {
      return (float)Math.min(1.0D, this.getInventoryVolume() / Math.max(9.999999747378752E-5D, this.getCapacity()));
   }
}
