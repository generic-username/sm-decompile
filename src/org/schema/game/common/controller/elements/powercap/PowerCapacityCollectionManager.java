package org.schema.game.common.controller.elements.powercap;

import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.power.PowerAddOn;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;

public class PowerCapacityCollectionManager extends ElementCollectionManager {
   private double maxPower;

   public PowerCapacityCollectionManager(SegmentController var1, VoidElementManager var2) {
      super((short)331, var1, var2);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return PowerCapacityUnit.class;
   }

   public boolean needsUpdate() {
      return false;
   }

   public boolean isDetailedElementCollections() {
      return false;
   }

   public PowerCapacityUnit getInstance() {
      return new PowerCapacityUnit();
   }

   protected void onChangedCollection() {
      this.refreshMaxPower();
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[]{new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWERCAP_POWERCAPACITYCOLLECTIONMANAGER_0, StringTools.formatPointZero(this.maxPower))};
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWERCAP_POWERCAPACITYCOLLECTIONMANAGER_1;
   }

   public double getMaxPower() {
      return this.maxPower;
   }

   public void setMaxPower(double var1) {
      this.maxPower = var1;
   }

   private void refreshMaxPower() {
      this.maxPower = 0.0D;

      double var2;
      for(Iterator var1 = this.getElementCollections().iterator(); var1.hasNext(); this.maxPower += var2) {
         var2 = Math.pow((double)((PowerCapacityUnit)var1.next()).size(), (double)VoidElementManager.POWER_TANK_CAPACITY_POW) * (double)VoidElementManager.POWER_TANK_CAPACITY_LINEAR;
      }

      ((PowerManagerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getPowerAddOn().setMaxPower(this.maxPower);
   }

   public float getSensorValue(SegmentPiece var1) {
      PowerAddOn var2 = ((PowerManagerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getPowerAddOn();
      return (float)Math.min(1.0D, var2.getPower() / Math.max(9.999999747378752E-5D, var2.getMaxPower()));
   }
}
