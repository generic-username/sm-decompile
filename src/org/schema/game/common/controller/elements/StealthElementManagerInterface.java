package org.schema.game.common.controller.elements;

public interface StealthElementManagerInterface {
   double calculateStealthIndex(double var1);
}
