package org.schema.game.common.controller.elements;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.longs.Long2FloatOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2LongOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongArrayFIFOQueue;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongSet;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectCollection;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.shorts.Short2IntOpenHashMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Map.Entry;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.CompareTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.controller.element.world.ClientSegmentProvider;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateControllerInterface;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.client.data.RailDockingListener;
import org.schema.game.client.view.ElementCollectionDrawer;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.SlotAssignment;
import org.schema.game.common.controller.damage.DamageDealer;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.damage.acid.AcidDamageFormula;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.controller.elements.activation.ActivationCollectionManager;
import org.schema.game.common.controller.elements.activation.ActivationDestMetaDataDummy;
import org.schema.game.common.controller.elements.beam.BeamElementManager;
import org.schema.game.common.controller.elements.beam.repair.RepairElementManager;
import org.schema.game.common.controller.elements.cargo.CargoCollectionManager;
import org.schema.game.common.controller.elements.cloaking.StealthAddOn;
import org.schema.game.common.controller.elements.dockingBlock.DockingElementManagerInterface;
import org.schema.game.common.controller.elements.effectblock.EffectAddOnManager;
import org.schema.game.common.controller.elements.effectblock.EffectElementManager;
import org.schema.game.common.controller.elements.factory.CargoCapacityElementManagerInterface;
import org.schema.game.common.controller.elements.jumpprohibiter.InterdictionAddOn;
import org.schema.game.common.controller.elements.power.PowerAddOn;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.controller.elements.power.reactor.MainReactorCollectionManager;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.controller.elements.power.reactor.PowerImplementation;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.controller.elements.power.reactor.RailPowerConsumer;
import org.schema.game.common.controller.elements.power.reactor.ReactorBoostAddOn;
import org.schema.game.common.controller.elements.power.reactor.StabilizerCollectionManager;
import org.schema.game.common.controller.elements.power.reactor.chamber.ConduitCollectionManager;
import org.schema.game.common.controller.elements.rail.TurretShotPlayerUsable;
import org.schema.game.common.controller.elements.scanner.ScanAddOn;
import org.schema.game.common.controller.elements.weapon.ZoomableUsableModule;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.MetaObjectState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.blockeffects.config.ConfigProviderSource;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.ShootContainer;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.inventory.ActiveInventory;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventoryClientAction;
import org.schema.game.common.data.player.inventory.InventoryExceededException;
import org.schema.game.common.data.player.inventory.InventoryFilter;
import org.schema.game.common.data.player.inventory.InventoryHolder;
import org.schema.game.common.data.player.inventory.InventoryMultMod;
import org.schema.game.common.data.player.inventory.InventorySlotRemoveMod;
import org.schema.game.common.data.player.inventory.ItemsToCreditsInventory;
import org.schema.game.common.data.player.inventory.NetworkInventoryInterface;
import org.schema.game.common.data.player.inventory.ShopInventory;
import org.schema.game.common.data.player.inventory.StashInventory;
import org.schema.game.common.data.player.inventory.TypeAmountFastMap;
import org.schema.game.common.data.player.inventory.TypeAmountLoopHandle;
import org.schema.game.common.data.world.Chunk16SegmentData;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.game.network.objects.LongStringPair;
import org.schema.game.network.objects.NetworkSegmentProvider;
import org.schema.game.network.objects.ShortIntPair;
import org.schema.game.network.objects.remote.RemoteInventory;
import org.schema.game.network.objects.remote.RemoteInventoryBuffer;
import org.schema.game.network.objects.remote.RemoteInventoryClientAction;
import org.schema.game.network.objects.remote.RemoteInventoryFilter;
import org.schema.game.network.objects.remote.RemoteInventoryFilterBuffer;
import org.schema.game.network.objects.remote.RemoteInventoryMultMod;
import org.schema.game.network.objects.remote.RemoteInventorySlotRemove;
import org.schema.game.network.objects.remote.RemoteLongString;
import org.schema.game.network.objects.remote.RemoteLongStringBuffer;
import org.schema.game.network.objects.remote.RemoteManualMouseEvent;
import org.schema.game.network.objects.remote.RemoteShortIntPair;
import org.schema.game.network.objects.remote.RemoteShortIntPairBuffer;
import org.schema.game.network.objects.remote.RemoteValueUpdate;
import org.schema.game.network.objects.valueUpdate.DestinationValueUpdate;
import org.schema.game.network.objects.valueUpdate.NTValueUpdateInterface;
import org.schema.game.network.objects.valueUpdate.ServerValueRequestUpdate;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.game.server.data.blueprintnw.BBWirelessLogicMarker;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseButton;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.objects.remote.LongIntPair;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.RemoteLongBuffer;
import org.schema.schine.network.objects.remote.RemoteLongIntPair;
import org.schema.schine.network.objects.remote.Streamable;
import org.schema.schine.network.server.ServerMessage;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.ListSpawnObjectCallback;
import org.schema.schine.resource.tag.Tag;

public abstract class ManagerContainer implements InventoryHolder {
   public static final long TIME_STEP_STASH_PULL = 10000L;
   private static final int BLOCK_ADD_REMOVE_LISTEN_DELAY = 500;
   private static boolean startedClientStatic;
   public final Long2FloatOpenHashMap floatValueMap = new Long2FloatOpenHashMap();
   protected final Object2ObjectOpenHashMap em2modulesMap = new Object2ObjectOpenHashMap();
   protected final ObjectArrayList modules = new ObjectArrayList();
   protected final ObjectArrayList updateModules = new ObjectArrayList();
   protected final ObjectArrayList handleModules = new ObjectArrayList();
   protected final ModuleMap modulesMap = new ModuleMap();
   protected final ModuleCollectionMap modulesControllerMap = new ModuleCollectionMap();
   protected final ModuleCollectionMap effectMap = new ModuleCollectionMap();
   protected final ObjectArrayList targetableSystems = new ObjectArrayList();
   private final ObjectArrayList receiverModules = new ObjectArrayList();
   private final ObjectArrayList updatable = new ObjectArrayList();
   private final ObjectArrayList distributionReceiverModules = new ObjectArrayList();
   private final ObjectArrayList hittableModules = new ObjectArrayList();
   private final ObjectArrayList killableBlockModules = new ObjectArrayList();
   private final ObjectArrayList senderModules = new ObjectArrayList();
   private final ObjectArrayList changeListenModules = new ObjectArrayList();
   private final ObjectArrayList blockActivateListenModules = new ObjectArrayList();
   private final ObjectArrayList beamModules = new ObjectArrayList();
   private final Set activeInventories = new ObjectOpenHashSet();
   private final ShortOpenHashSet typesThatNeedActivation = new ShortOpenHashSet();
   private final ObjectArrayList delayedInventoryAdd = new ObjectArrayList();
   private final ObjectArrayList timedUpdateInterfaces = new ObjectArrayList();
   private final ObjectArrayList delayedInventoryRemove = new ObjectArrayList();
   private final InventoryMap inventories = new InventoryMap();
   private final Long2ObjectOpenHashMap initialBlockMetaData = new Long2ObjectOpenHashMap();
   private final LongArrayFIFOQueue ntInventoryProdMods = new LongArrayFIFOQueue();
   private final ObjectArrayFIFOQueue ntInventoryProdLimitMods = new ObjectArrayFIFOQueue();
   private final ObjectArrayFIFOQueue ntInventoryCustomNameMods = new ObjectArrayFIFOQueue();
   private final ObjectArrayFIFOQueue ntInventoryFilterMods = new ObjectArrayFIFOQueue();
   private final ObjectArrayFIFOQueue ntInventoryFillMods = new ObjectArrayFIFOQueue();
   private final ObjectArrayFIFOQueue ntInventoryMods = new ObjectArrayFIFOQueue();
   private final ObjectArrayFIFOQueue ntInventoryRemoves = new ObjectArrayFIFOQueue();
   private final ObjectArrayFIFOQueue ntInventoryMultMods = new ObjectArrayFIFOQueue();
   private final ObjectArrayFIFOQueue ntInventorySlotRemoveMods = new ObjectArrayFIFOQueue();
   private final ObjectArrayList weapons = new ObjectArrayList();
   public final ManagerContainer.SingleControllerStateUnit unitSingle;
   private final SegmentController segmentController;
   private final boolean onServer;
   private final Long2ObjectOpenHashMap namedInventoriesClient = new Long2ObjectOpenHashMap();
   private final Object2ObjectOpenHashMap delayedCummulativeInventoryModsToSend = new Object2ObjectOpenHashMap();
   private final LongOpenHashSet filterInventories = new LongOpenHashSet();
   private final Transform tTmp = new Transform();
   private final Vector3f sPos = new Vector3f();
   private static final ManagerUpdatabaleComparator updatableComp = new ManagerUpdatabaleComparator();
   public boolean loadInventoriesFromTag = true;
   protected Collection dockingModules = new ArrayList();
   private ObjectArrayFIFOQueue clientInventoryActions = new ObjectArrayFIFOQueue();
   private Short2IntOpenHashMap relevantMap = new Short2IntOpenHashMap();
   private long flagAnyBlockAdded = -1L;
   private long flagAnyBlockRemoved = -1L;
   private ObjectArrayFIFOQueue valueUpdates = new ObjectArrayFIFOQueue();
   private boolean namedInventoriesClientChanged = true;
   private long lastInventoryFilterStep = 0L;
   public double volumeTotal;
   private final Long2ObjectOpenHashMap invProdMapDelayed = new Long2ObjectOpenHashMap();
   public long shopBlockIndex = Long.MIN_VALUE;
   private final Long2ObjectOpenHashMap blockActivationsForModules = new Long2ObjectOpenHashMap();
   private final List moduleExplosions = new ObjectArrayList();
   private final Vector4f colorCoreDefault = new Vector4f(0.1F, 0.5F, 1.0F, 1.0F);
   private final Vector4f colorCore;
   private long itemToSpawnCheckStart;
   private long factionBlockPos;
   private final Long2ObjectMap playerUsable;
   private final Long2ObjectMap revealingActionListeners;
   public final BlockConnectionPath stabilizerNodePath;
   protected boolean[] specialBlocks;
   private final List tagModules;
   private TurretShotPlayerUsable turretShotUsable;
   private final Long2LongOpenHashMap flaggedConnections;
   private List manualEvents;
   private final ManagerContainer.PullInvHanlder pHandler;
   private final SegmentPiece tmp;
   private final PowerInterface powerInterface;
   private final LongOpenHashSet buildBlocks;
   private final ScanAddOn scanAddOn;
   private final StealthAddOn stealthAddOn;
   private final Set effectConfigSources;
   private final EffectAddOnManager effectAddOnManager;
   private ObjectArrayFIFOQueue rechargableSinfleModulesToAdd;
   private boolean notLoadedOnClient;
   private ReactorBoostAddOn reactorBoostAddOn;
   private InterdictionAddOn interdictionBoostAddOn;
   private final LongSet slavesAndEffects;
   private ObjectArrayFIFOQueue inventoryDetailRequests;
   private ElementCollectionDrawer.MContainerDrawJob graphicsListener;
   public long lastChangedElement;
   private final ObjectArrayFIFOQueue receivedBeamLatches;
   public final List beamInteracesSingle;
   private final ObjectArrayFIFOQueue effectChangeQueue;
   private final ObjectArrayFIFOQueue checkUpdatableQueue;
   private final ObjectArrayList railDockingListeners;
   private PlayerUsableInterface lastUsableUsed;
   private final StateInterface state;
   private PlayerUsableInterface lastUsable;
   private float timeSpentHudLastUsable;
   private boolean flagModuleKilled;
   private float integrityUpdateDelay;
   private boolean flagBlockAdded;
   private boolean flagBlockDamaged;
   private boolean flagBlockKilled;
   private float repairDelay;
   private long lastThrownInvalidConnectionException;
   private final SystemTargetContainer targetContainer;
   private boolean flagTargetRecalc;
   private long lastTargetRecalc;
   private boolean flagElementCollectionChanged;

   public ManagerContainer(StateInterface var1, SegmentController var2) {
      this.colorCore = new Vector4f(this.colorCoreDefault);
      this.itemToSpawnCheckStart = -1L;
      this.factionBlockPos = Long.MIN_VALUE;
      this.playerUsable = new Long2ObjectOpenHashMap();
      this.revealingActionListeners = new Long2ObjectOpenHashMap();
      this.stabilizerNodePath = new BlockConnectionPath(this);
      this.tagModules = new ObjectArrayList();
      this.flaggedConnections = new Long2LongOpenHashMap();
      this.manualEvents = new ObjectArrayList();
      this.pHandler = new ManagerContainer.PullInvHanlder();
      this.tmp = new SegmentPiece();
      this.buildBlocks = new LongOpenHashSet();
      this.effectConfigSources = new ObjectOpenHashSet();
      this.notLoadedOnClient = true;
      this.slavesAndEffects = new LongOpenHashSet();
      this.inventoryDetailRequests = new ObjectArrayFIFOQueue();
      this.receivedBeamLatches = new ObjectArrayFIFOQueue();
      this.beamInteracesSingle = new ObjectArrayList();
      this.effectChangeQueue = new ObjectArrayFIFOQueue();
      this.checkUpdatableQueue = new ObjectArrayFIFOQueue();
      this.railDockingListeners = new ObjectArrayList();
      this.targetContainer = new SystemTargetContainer();
      this.flagTargetRecalc = true;
      this.state = var1;
      this.segmentController = var2;
      this.rechargableSinfleModulesToAdd = new ObjectArrayFIFOQueue();
      this.powerInterface = new PowerImplementation(this);
      this.onServer = var2.isOnServer();
      this.unitSingle = new ManagerContainer.SingleControllerStateUnit();
      this.effectAddOnManager = new EffectAddOnManager(this);
      this.initialize(var1);
      this.scanAddOn = new ScanAddOn(this);
      this.stealthAddOn = new StealthAddOn(this);
      this.reactorBoostAddOn = new ReactorBoostAddOn(this);
      this.interdictionBoostAddOn = new InterdictionAddOn(this);
      this.addRechModules();
      Iterator var4 = this.modules.iterator();

      ManagerModule var6;
      while(var4.hasNext()) {
         (var6 = (ManagerModule)var4.next()).init(this);
         if (var6.getElementManager() instanceof WeaponElementManagerInterface) {
            this.getWeapons().add((WeaponElementManagerInterface)var6.getElementManager());
         }

         if (var6.getElementManager() instanceof RailDockingListener) {
            this.railDockingListeners.add((RailDockingListener)var6.getElementManager());
         }

         if (var6.getElementManager() instanceof PowerConsumer) {
            this.addConsumer((PowerConsumer)var6.getElementManager());
         }

         if (var6.getElementManager() instanceof PlayerUsableInterface) {
            this.addPlayerUsable((PlayerUsableInterface)var6.getElementManager());
         }

         if (var6.getElementManager() instanceof TagModuleUsableInterface) {
            this.addTagUsable((TagModuleUsableInterface)var6.getElementManager());
         }

         this.em2modulesMap.put(var6.getElementManager(), var6);
         this.checkUpdatableQueue.enqueue(var6.getElementManager());
      }

      this.turretShotUsable = new TurretShotPlayerUsable(this);
      this.addPlayerUsable(this.turretShotUsable);
      this.reparseBlockBehavior(false);
      this.addConsumer(new RailPowerConsumer(this));

      for(int var5 = 0; var5 < this.modules.size(); ++var5) {
         if ((var6 = (ManagerModule)this.modules.get(var5)).getElementManager() instanceof BeamHandlerDeligator) {
            this.beamInteracesSingle.add(((BeamHandlerDeligator)var6.getElementManager()).getBeamHandler());
         }

         if (var6 instanceof ManagerModuleControllable) {
            ManagerModuleControllable var3 = (ManagerModuleControllable)var6;

            assert !this.getModulesMap().containsKey(var3.getControllerID()) : "already contains: " + ElementKeyMap.toString(var3.getControllerID()) + "; " + var3 + "; " + this.getModulesMap();

            this.getModulesMap().put(var3.getControllerID(), var3);
            if (ElementKeyMap.isValidType(var3.getControllerID()) && ElementKeyMap.getInfo(var3.getControllerID()).isEffectCombinationController()) {
               assert var3.getElementManager() instanceof EffectElementManager;

               this.effectMap.put(var3.getControllerID(), (ManagerModuleCollection)var3);
            } else {
               assert !(var3.getElementManager() instanceof EffectElementManager);
            }

            if (var3 instanceof ManagerModuleCollection) {
               assert !this.getModulesControllerMap().containsKey(var3.getControllerID()) : var3 + "; " + this.getModulesMap();

               this.getModulesControllerMap().put(var3.getControllerID(), (ManagerModuleCollection)var3);
            }

            if (var3.getElementManager() instanceof TimedUpdateInterface) {
               this.timedUpdateInterfaces.add((TimedUpdateInterface)var3.getElementManager());
            }
         }

         if (!this.getModulesMap().containsKey(var6.getElementID())) {
            this.getModulesMap().put(var6.getElementID(), var6);
         } else {
            ManagerModule var7;
            for(var7 = this.getModulesMap().get(var6.getElementID()); var7.getNext() != null; var7 = var7.getNext()) {
            }

            var7.setNext(var6);
         }

         if (var6 instanceof ManagerModuleSingle) {
            ElementCollectionManager var8 = ((ManagerModuleSingle)var6).getCollectionManager();
            this.addTypeModifiers(var8);
         }

         if (!(var6.getElementManager() instanceof VoidElementManager)) {
            this.handleModules.add(var6);
         }

         if (var6.getElementManager() instanceof DockingElementManagerInterface) {
            this.dockingModules.add((ManagerModuleCollection)var6);
         }

         if (var6.getElementManager() instanceof TargetableSystemInterface) {
            this.targetableSystems.add((TargetableSystemInterface)var6.getElementManager());
         }

         this.addTypeModifiers(var6.getElementManager());
      }

      this.modules.trim();
      this.handleModules.trim();
      this.timedUpdateInterfaces.trim();
      this.getWeapons().trim();
      this.railDockingListeners.trim();
      this.afterInitialize();

      assert this.getModulesMap().checkIntegrity();

   }

   protected abstract void afterInitialize();

   public void reparseBlockBehavior(boolean var1) {
      if (var1) {
         if (this.getSegmentController().isOnServer()) {
            UsableElementManager.initializedServer.clear();
         } else {
            UsableElementManager.initializedClient.clear();
         }
      }

      for(int var6 = 0; var6 < this.modules.size(); ++var6) {
         ManagerModule var2 = (ManagerModule)this.modules.get(var6);
         boolean var3 = false;

         while(!var3) {
            try {
               assert ((GameStateInterface)this.segmentController.getState()).getBlockBehaviorConfig() != null;

               ((GameStateInterface)this.segmentController.getState()).getBlockBehaviorConfig();
               var2.getElementManager().parse(((GameStateInterface)this.segmentController.getState()).getBlockBehaviorConfig());
               var3 = true;
            } catch (Exception var5) {
               if (GameClientController.availableGUI) {
                  GuiErrorHandler.processErrorDialogException(var5);
                  ((GameStateControllerInterface)this.segmentController.getState().getController()).parseBlockBehavior("./data/config/blockBehaviorConfig.xml");
               } else {
                  var5.printStackTrace();
                  ((GameServerState)this.getState()).getController().broadcastMessage(new Object[]{49}, 3);
                  ((GameStateControllerInterface)this.segmentController.getState().getController()).parseBlockBehavior("./data/config/revertBlockBehaviorConfig.xml");
               }
            }
         }
      }

   }

   private void addInventory(long var1, short var3) {
      assert this.getSegmentController().isOnServer();

      if (!this.getInventories().containsKey(var1) || this.delayedInventoryRemove.contains(new Vector4i(ElementCollection.getPosFromIndex(var1, new Vector3i()), 0))) {
         if (var3 == 120) {
            this.delayedInventoryAdd.add(new StashInventory(this, var1));
            return;
         }

         if (var3 == 114) {
            this.delayedInventoryAdd.add(new ItemsToCreditsInventory(this, var1));
            return;
         }

         if (ElementKeyMap.getFactorykeyset().contains(var3)) {
            this.delayedInventoryAdd.add(new StashInventory(this, var1));
            return;
         }

         if (var3 == 677) {
            this.delayedInventoryAdd.add(new StashInventory(this, var1));
         }
      }

   }

   private void addTypeModifiers(Object var1) {
      if (var1 instanceof ManagerUpdatableInterface) {
         this.updatable.add((ManagerUpdatableInterface)var1);
      }

      if (var1 instanceof NTReceiveInterface) {
         this.receiverModules.add((NTReceiveInterface)var1);
      }

      if (var1 instanceof NTDistributionReceiverInterface) {
         this.distributionReceiverModules.add((NTDistributionReceiverInterface)var1);
      }

      if (var1 instanceof BlockActivationListenerInterface) {
         this.blockActivateListenModules.add((BlockActivationListenerInterface)var1);
         ((BlockActivationListenerInterface)var1).updateActivationTypes(this.typesThatNeedActivation);
      }

      if (var1 instanceof HittableInterface) {
         this.hittableModules.add((HittableInterface)var1);
      }

      if (var1 instanceof BlockKillInterface) {
         this.killableBlockModules.add((BlockKillInterface)var1);
      }

      if (var1 instanceof NTSenderInterface) {
         this.senderModules.add((NTSenderInterface)var1);
      }

      if (var1 instanceof ElementChangeListenerInterface) {
         this.changeListenModules.add((ElementChangeListenerInterface)var1);
      }

      if (var1 instanceof BeamElementManager) {
         this.beamModules.add((BeamElementManager)var1);
      }

      if (var1 instanceof BeamHandlerContainer) {
         this.beamInteracesSingle.add((BeamHandlerContainer)var1);
      }

      if (var1 instanceof BeamHandlerDeligator) {
         this.beamInteracesSingle.add(((BeamHandlerDeligator)var1).getBeamHandler());
      }

   }

   private void announceInventory(long var1, boolean var3, Inventory var4, boolean var5) {
      synchronized(this.getInventories()) {
         RemoteInventory var8;
         if (var5) {
            assert this.getSegmentController().getSegmentBuffer().getPointUnsave(var1).getType() != 689;

            this.getInventories().put(var1, var4);
            var8 = new RemoteInventory(var4, this, var5, this.getSegmentController().isOnServer());
            this.getInventoryInterface().getInventoriesChangeBuffer().add(var8);
         } else {
            System.err.println("[MANAGERCONTAINER] ANNOUNCE INVENTORY REMOVE ON " + this.getSegmentController() + ": preserve " + var3);
            Inventory var9;
            if ((var9 = this.getInventories().remove(var1)) != null) {
               this.onInventoryRemove(var9, var3);
               var8 = new RemoteInventory(var9, this, var5, this.getSegmentController().isOnServer());
               this.getInventoryInterface().getInventoriesChangeBuffer().add(var8);
            }
         }

      }
   }

   public boolean canBeControlled(short var1, short var2) {
      return ElementKeyMap.isValidType(var2) && ElementKeyMap.isValidType(var1) && (this.getModulesMap().containsKey(var2) || ElementInformation.canBeControlled(var1, var2));
   }

   public void clear() {
      for(int var1 = 0; var1 < this.modules.size(); ++var1) {
         ((ManagerModule)this.modules.get(var1)).clear();
      }

      this.slavesAndEffects.clear();
      this.notLoadedOnClient = true;
   }

   protected void fromExtraTag(Tag var1) {
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var4;
      if (!var1.getName().equals("container")) {
         if (var1.getName().equals("controllerStructure")) {
            this.handleTag(var1);
         } else {
            var4 = (Tag[])var1.getValue();

            for(int var7 = 0; var7 < var4.length; ++var7) {
               if (var4[var7].getType() != Tag.Type.FINISH) {
                  this.handleTag(var4[var7]);
               }
            }

         }
      } else {
         var4 = (Tag[])var1.getValue();
         this.fromTagInventory(var4[0]);
         if (var4[1].getType() == Tag.Type.STRUCT) {
            Tag[] var2 = (Tag[])var4[1].getValue();

            for(int var3 = 0; var3 < var2.length; ++var3) {
               var2[var3].getType();
               Tag.Type var10000 = Tag.Type.FINISH;
            }
         }

         if (this instanceof PowerManagerInterface && var4.length > 2) {
            PowerAddOn var5 = ((PowerManagerInterface)this).getPowerAddOn();
            if ("pw".equals(var4[2].getName())) {
               var5.fromTagStructureOld(var4[2]);
            } else if (var4[2].getType() == Tag.Type.STRUCT) {
               var5.fromTagStructure(var4[2]);
            }
         }

         if (this instanceof ShieldContainerInterface && var4.length > 3) {
            if (var4[3].getType() == Tag.Type.DOUBLE) {
               assert ((ShieldContainerInterface)this).getShieldRegenManager() != null;

               assert var4[3].getValue() != null;

               ((ShieldContainerInterface)this).getShieldAddOn().setInitialShields((Double)var4[3].getValue());
            } else if (var4[3].getType() == Tag.Type.STRUCT) {
               ((ShieldContainerInterface)this).getShieldAddOn().getShieldLocalAddOn().fromTagStructure(var4[3]);
            }
         }

         if (var4.length > 4) {
            this.fromExtraTag(var4[4]);
         }

         if (var4.length > 5 && var4[5].getType() != Tag.Type.FINISH) {
            ((SendableSegmentController)this.segmentController).fromActivationStateTag(var4[5]);
         }

         if (var4.length > 6 && var4[6].getType() != Tag.Type.FINISH) {
            this.segmentController.readTextBlockData(var4[6]);
         }

         if (var4.length > 7 && var4[7].getType() != Tag.Type.FINISH) {
            this.readRelevantBlockCounts(var4[7]);
         }

         if (var4.length > 8 && var4[8].getType() != Tag.Type.FINISH) {
            this.fromWarpGateTag(var4[8]);
         }

         if (var4.length > 9 && var4[9].getType() != Tag.Type.FINISH) {
            byte var6 = 0;
            if (this.getSegmentController().isLoadedFromChunk16()) {
               var6 = 8;
            }

            this.fromTagModule(var4[9], var6);
         }

         if (var4.length > 10 && var4[10].getType() != Tag.Type.FINISH) {
            this.getSegmentController().aiFromTagStructure(var4[10]);
         }

         if (var4.length > 11 && var4[11].getType() != Tag.Type.FINISH) {
            this.getSegmentController().getSlotAssignment().fromTagStructure(var4[11]);
         }

         if (var4.length > 12 && var4[12].getType() != Tag.Type.FINISH) {
            this.fromRaceGateTag(var4[12]);
         }

         if (var4.length > 13 && var4[13].getType() != Tag.Type.FINISH) {
            this.fromAdditionalDummiesNotLoadedLastTime(var4[13]);
         }

         if (var4.length > 14 && var4[14].getType() != Tag.Type.FINISH) {
            Tag.listFromTagStructSP(this.moduleExplosions, (Tag)var4[14], new ListSpawnObjectCallback() {
               public ModuleExplosion get(Object var1) {
                  Tag var3 = (Tag)var1;
                  ModuleExplosion var2;
                  (var2 = new ModuleExplosion()).fromTagStructure(var3);
                  return var2;
               }
            });
         }

         if (var4.length > 15 && var4[15].getType() != Tag.Type.FINISH) {
            this.getPowerInterface().fromTagStructure(var4[15]);
         }

         if (var4.length > 16 && var4[16].getType() != Tag.Type.FINISH) {
            this.segmentController.pullPermission = SegmentController.PullPermission.values()[var4[16].getByte()];
         }

      }
   }

   private void fromAdditionalDummiesNotLoadedLastTime(Tag var1) {
      Tag[] var3 = (Tag[])var1.getValue();

      for(int var2 = 0; var2 < var3.length - 1; ++var2) {
         this.fromTagDummy(var3[var2], 0, var3[var2].getName());
      }

   }

   public void fromTagModule(Tag var1, int var2) {
      Tag[] var4 = (Tag[])var1.getValue();

      for(int var3 = 0; var3 < var4.length - 1; ++var3) {
         this.fromTagModuleSigle(var4[var3], var2);
      }

   }

   private void fromTagDistribution(Tag var1) {
   }

   private void fromTagModuleSigle(Tag var1, int var2) {
      Tag[] var3 = (Tag[])var1.getValue();

      for(int var4 = 0; var4 < var3.length && var3[var4].getType() != Tag.Type.FINISH; ++var4) {
         this.fromTagDummy(var3[var4], var2, var1.getName());
      }

   }

   private void fromTagDummy(Tag var1, int var2, String var3) {
      Iterator var4 = this.tagModules.iterator();

      TagModuleUsableInterface var5;
      do {
         if (!var4.hasNext()) {
            assert "EF-9223372036854775780".equals(var3) : "No handler found: " + var3;

            return;
         }
      } while(!(var5 = (TagModuleUsableInterface)var4.next()).getTagId().equals(var3));

      BlockMetaDataDummy var6 = var5.getDummyInstance();

      assert var6 != null : "missing implementation of function: Class: " + var5.getClass().getSimpleName();

      var6.fromTagStructure(var1, var2);
      this.getInitialBlockMetaData().put(var6.pos, var6);
   }

   public void fromTagInventory(Tag var1) {
      Tag[] var7 = (Tag[])var1.getValue();

      for(int var2 = 0; var2 < var7.length && var7[var2].getType() != Tag.Type.FINISH; ++var2) {
         Tag[] var3;
         int var4 = (Integer)(var3 = (Tag[])var7[var2].getValue())[0].getValue();
         Vector3i var5 = (Vector3i)var3[1].getValue();
         if (this.getSegmentController().isLoadedFromChunk16()) {
            var5.add(Chunk16SegmentData.SHIFT);
         }

         Object var6 = null;
         if (var4 == 3) {
            var6 = new StashInventory(this, ElementCollection.getIndex(var5));
         }

         if (var4 == 1) {
            var6 = new ItemsToCreditsInventory(this, ElementCollection.getIndex(var5));
         }

         assert var6 != null : "unknown type: " + var4;

         ((Inventory)var6).fromTagStructure(var3[2]);
         if (!this.loadInventoriesFromTag) {
            ((Inventory)var6).clear();
            System.err.println("[TAG] " + this.getSegmentController() + " clearing inventory (keeping filters)");
         }

         this.getInventories().put(((Inventory)var6).getParameterIndex(), (Inventory)var6);
      }

   }

   protected Tag getExtraTag() {
      return new Tag(Tag.Type.BYTE, "ex", (byte)0);
   }

   public Long2ObjectOpenHashMap getInitialBlockMetaData() {
      return this.initialBlockMetaData;
   }

   public InventoryMap getInventories() {
      return this.inventories;
   }

   public Inventory getInventory(Vector3i var1) {
      long var2 = ElementCollection.getIndex(var1);
      return (Inventory)this.getInventories().get(var2);
   }

   public double getCapacityFor(Inventory var1) {
      CargoCollectionManager var2;
      return (var2 = (CargoCollectionManager)this.getCargo().getCollectionManagersMap().get(var1.getParameterIndex())) != null ? var2.getCapacity() : 0.0D;
   }

   public void volumeChanged(double var1, double var3) {
      double var5 = var3 - var1;
      this.volumeTotal += var5;
      this.getSegmentController().flagupdateMass();
   }

   public void sendInventoryErrorMessage(Object[] var1, Inventory var2) {
      if (this.isOnServer()) {
         (new ServerMessage(var1, 4)).block = var2.getParameterIndex();
      }

   }

   public Inventory getInventory(long var1) {
      return (Inventory)this.getInventories().get(var1);
   }

   public String getName() {
      return this.segmentController.getRealName();
   }

   public StateInterface getState() {
      return this.state;
   }

   public String printInventories() {
      return this.getInventories().toString();
   }

   public void sendInventoryModification(IntCollection var1, long var2) {
      Inventory var4;
      if ((var4 = this.getInventory(var2)) != null) {
         InventoryMultMod var6 = new InventoryMultMod(var1, var4, var2);
         this.getInventoryInterface().getInventoryMultModBuffer().add(new RemoteInventoryMultMod(var6, this.getSegmentController().getNetworkObject()));
      } else {
         try {
            throw new IllegalArgumentException("[INVENTORY] Exception: tried to send inventory " + var2 + " (inventory was null at that position)");
         } catch (Exception var5) {
            var5.printStackTrace();
         }
      }
   }

   public void sendInventoryModification(int var1, long var2) {
      IntArrayList var4;
      (var4 = new IntArrayList(1)).add(var1);
      this.sendInventoryModification(var4, var2);
   }

   public void sendInventorySlotRemove(int var1, long var2) {
      if (this.getInventory(var2) != null) {
         this.getInventoryInterface().getInventorySlotRemoveRequestBuffer().add(new RemoteInventorySlotRemove(new InventorySlotRemoveMod(var1, var2), this.isOnServer()));
      } else {
         try {
            throw new IllegalArgumentException("[INVENTORY] Exception: tried to send inventory " + var2);
         } catch (Exception var4) {
            var4.printStackTrace();
         }
      }
   }

   protected NetworkInventoryInterface getInventoryInterface() {
      return (NetworkInventoryInterface)this.getSegmentController().getNetworkObject();
   }

   public ObjectArrayList getModules() {
      return this.modules;
   }

   public ModuleCollectionMap getModulesControllerMap() {
      return this.modulesControllerMap;
   }

   public ModuleMap getModulesMap() {
      return this.modulesMap;
   }

   public SegmentController getSegmentController() {
      return this.segmentController;
   }

   public void handleControl(ControllerStateInterface var1, Timer var2) {
      ControllerStateUnit var10;
      if ((var10 = (ControllerStateUnit)var1).parameter instanceof Vector3i) {
         assert var10.getPlayerState() != null;

         long var4 = ElementCollection.getIndex((Vector3i)var10.parameter);
         long var6 = this.getSelectedSlot(var10, var4);
         PlayerUsableInterface var3;
         if ((var3 = (PlayerUsableInterface)this.playerUsable.get(var6)) != null) {
            if (this.lastUsableUsed != var3) {
               if (this.lastUsableUsed != null) {
                  this.lastUsableUsed.onSwitched(false);
               }

               var3.onSwitched(true);
               this.lastUsableUsed = var3;
            }

            var3.handleControl(var10, var2);
         }

         long[] var11;
         int var12 = (var11 = PlayerUsableInterface.ALWAYS_SELECTED).length;

         for(int var5 = 0; var5 < var12; ++var5) {
            long var8 = var11[var5];
            PlayerUsableInterface var13;
            if ((var13 = (PlayerUsableInterface)this.playerUsable.get(var8)) != null) {
               var13.handleControl(var10, var2);
            }
         }
      }

   }

   public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, Timer var3) {
      if (var1.parameter instanceof Vector3i) {
         assert var1.getPlayerState() != null;

         long var4 = ElementCollection.getIndex((Vector3i)var1.parameter);
         long var6 = this.getSelectedSlot(var1, var4);
         PlayerUsableInterface var11;
         if ((var11 = (PlayerUsableInterface)this.playerUsable.get(var6)) != null) {
            if (this.lastUsable == var11) {
               this.timeSpentHudLastUsable += var3.getDelta();
            } else {
               this.timeSpentHudLastUsable = 0.0F;
            }

            HudContextHelperContainer.Hos var10;
            if (this.timeSpentHudLastUsable < (float)(Integer)EngineSettings.SHOW_MODULE_HELP_ON_CURSOR.getCurrentState()) {
               var10 = HudContextHelperContainer.Hos.MOUSE;
            } else {
               var10 = HudContextHelperContainer.Hos.LEFT;
            }

            var11.addHudConext(var1, var2, var10);
            this.lastUsable = var11;
         }

         this.lastUsable = var11;
         long[] var12;
         int var13 = (var12 = PlayerUsableInterface.ALWAYS_SELECTED).length;

         for(int var5 = 0; var5 < var13; ++var5) {
            long var8 = var12[var5];
            PlayerUsableInterface var14;
            if ((var14 = (PlayerUsableInterface)this.playerUsable.get(var8)) != null) {
               var14.addHudConext(var1, var2, HudContextHelperContainer.Hos.LEFT);
            }
         }

      } else {
         this.lastUsable = null;
      }
   }

   public float getSelectedWeaponRange(ControllerStateUnit var1) {
      if (var1.parameter instanceof Vector3i) {
         assert var1.getPlayerState() != null;

         long var2 = ElementCollection.getIndex((Vector3i)var1.parameter);
         long var4 = this.getSelectedSlot(var1, var2);
         PlayerUsableInterface var6;
         if ((var6 = (PlayerUsableInterface)this.playerUsable.get(var4)) != null) {
            return var6.getWeaponDistance();
         }
      }

      return 0.0F;
   }

   public float getSelectedWeaponSpeed(ControllerStateUnit var1) {
      if (var1.parameter instanceof Vector3i) {
         assert var1.getPlayerState() != null;

         long var2 = ElementCollection.getIndex((Vector3i)var1.parameter);
         long var4 = this.getSelectedSlot(var1, var2);
         PlayerUsableInterface var6;
         if ((var6 = (PlayerUsableInterface)this.playerUsable.get(var4)) != null) {
            return var6.getWeaponSpeed();
         }
      }

      return 0.0F;
   }

   public void handleKeyEvent(ControllerStateUnit var1, KeyboardMappings var2) {
      if (var1.parameter instanceof Vector3i) {
         long var3 = ElementCollection.getIndex((Vector3i)var1.parameter);
         long var5 = this.getSelectedSlot(var1, var3);
         PlayerUsableInterface var9;
         if ((var9 = (PlayerUsableInterface)this.playerUsable.get(var5)) != null) {
            var9.handleKeyEvent(var1, var2);
         }

         long[] var10;
         int var4 = (var10 = PlayerUsableInterface.ALWAYS_SELECTED).length;

         for(int var11 = 0; var11 < var4; ++var11) {
            long var7 = var10[var11];
            PlayerUsableInterface var6;
            if ((var6 = (PlayerUsableInterface)this.playerUsable.get(var7)) != null) {
               var6.handleKeyEvent(var1, var2);
            }
         }
      }

   }

   public boolean handleBlockActivate(SegmentPiece var1, boolean var2, boolean var3) {
      Iterator var4 = this.blockActivateListenModules.iterator();

      BlockActivationListenerInterface var5;
      do {
         if (!var4.hasNext()) {
            return true;
         }
      } while(!(var5 = (BlockActivationListenerInterface)var4.next()).isHandlingActivationForType(var1.getType()) || var5.onActivate(var1, var2, var3) >= 0);

      return false;
   }

   public void registerActivationForModule(UsableControllableElementManager var1, long var2, long var4, Timer var6) {
      ManagerContainer.BlockAct var7;
      if ((var7 = (ManagerContainer.BlockAct)this.blockActivationsForModules.get(var2)) == null) {
         var7 = new ManagerContainer.BlockAct();
         this.blockActivationsForModules.put(var2, var7);
      }

      var7.trigger = var4;
      var7.block = var2;
      var7.module = var1;
      var7.timeStarted = var6.currentTime;
   }

   public boolean handleActivateBlockActivate(SegmentPiece var1, long var2, boolean var4, Timer var5) {
      PlayerUsableInterface var6;
      if ((var6 = this.getPlayerUsable(var1.getAbsoluteIndex())) != null) {
         var6.onLogicActivate(var1, var4, var5);
      }

      ManagerModuleCollection var7;
      if ((var7 = this.getModulesControllerMap().get(var1.getType())) != null && var7.getElementManager().isUsingRegisteredActivation() && var1.isActive() && !var4 && !this.getSegmentController().isVirtualBlueprint()) {
         this.registerActivationForModule(var7.getElementManager(), var1.getAbsoluteIndex(), var2, var5);
      }

      if ((!var1.isActive() || var4 || var1.getType() != 7) && var1.getType() != 289) {
         return !this.getSegmentController().isOnServer() ? this.handleBlockActivate(var1, var4, var1.isActive()) : true;
      } else {
         return this.handleBlockActivate(var1, var4, var1.isActive());
      }
   }

   protected void handleGlobalInventorySlotRemove(InventorySlotRemoveMod var1) {
      assert false;

   }

   protected void handleGlobalInventory(InventoryMultMod var1) {
      assert false;

   }

   public void handleInventoryFromNT(RemoteInventoryBuffer var1, RemoteLongBuffer var2, RemoteBuffer var3, RemoteShortIntPairBuffer var4, RemoteShortIntPairBuffer var5, RemoteLongStringBuffer var6) {
      int var7;
      if (var2 != null) {
         for(var7 = 0; var7 < var2.getReceiveBuffer().size(); ++var7) {
            long var8 = var2.getReceiveBuffer().getLong(var7);
            synchronized(this.ntInventoryProdMods) {
               this.ntInventoryProdMods.enqueue(var8);
            }
         }
      }

      if (var3 != null) {
         for(var7 = 0; var7 < var3.getReceiveBuffer().size(); ++var7) {
            LongIntPair var21 = (LongIntPair)((RemoteLongIntPair)var3.getReceiveBuffer().get(var7)).get();
            synchronized(this.ntInventoryProdLimitMods) {
               this.ntInventoryProdLimitMods.enqueue(var21);
            }
         }
      }

      if (var6 != null) {
         for(var7 = 0; var7 < var6.getReceiveBuffer().size(); ++var7) {
            LongStringPair var22 = (LongStringPair)((RemoteLongString)var6.getReceiveBuffer().get(var7)).get();
            synchronized(this.ntInventoryCustomNameMods) {
               this.ntInventoryCustomNameMods.enqueue(var22);
            }
         }
      }

      ShortIntPair var23;
      if (var4 != null) {
         for(var7 = 0; var7 < var4.getReceiveBuffer().size(); ++var7) {
            var23 = (ShortIntPair)((RemoteShortIntPair)var4.getReceiveBuffer().get(var7)).get();
            synchronized(this.ntInventoryFilterMods) {
               this.ntInventoryFilterMods.enqueue(var23);
            }
         }
      }

      if (var5 != null) {
         for(var7 = 0; var7 < var5.getReceiveBuffer().size(); ++var7) {
            var23 = (ShortIntPair)((RemoteShortIntPair)var5.getReceiveBuffer().get(var7)).get();
            synchronized(this.ntInventoryFillMods) {
               this.ntInventoryFillMods.enqueue(var23);
            }
         }
      }

      ObjectArrayList var26 = var1.getReceiveBuffer();

      for(int var25 = 0; var25 < var26.size(); ++var25) {
         Inventory var9 = (Inventory)((RemoteInventory)var26.get(var25)).get();
         if (((RemoteInventory)var26.get(var25)).isAdd()) {
            synchronized(this.ntInventoryMods) {
               this.ntInventoryMods.enqueue(var9);
            }
         } else {
            synchronized(this.ntInventoryRemoves) {
               this.ntInventoryRemoves.enqueue(var9);
            }
         }
      }

      ObjectArrayList var27 = this.getInventoryInterface().getInventoryMultModBuffer().getReceiveBuffer();

      int var24;
      for(var24 = 0; var24 < var27.size(); ++var24) {
         RemoteInventoryMultMod var10 = (RemoteInventoryMultMod)var27.get(var24);
         synchronized(this.ntInventoryMultMods) {
            this.ntInventoryMultMods.enqueue(var10.get());
         }
      }

      var27 = this.getInventoryInterface().getInventorySlotRemoveRequestBuffer().getReceiveBuffer();

      for(var24 = 0; var24 < var27.size(); ++var24) {
         InventorySlotRemoveMod var28 = (InventorySlotRemoveMod)((RemoteInventorySlotRemove)var27.get(var24)).get();
         synchronized(this.ntInventorySlotRemoveMods) {
            this.ntInventorySlotRemoveMods.enqueue(var28);
         }
      }

      if (this.getInventoryNetworkObject().getInventoryClientActionBuffer().getReceiveBuffer().size() > 0) {
         Iterator var30 = this.getInventoryNetworkObject().getInventoryClientActionBuffer().getReceiveBuffer().iterator();

         while(var30.hasNext()) {
            InventoryClientAction var29 = (InventoryClientAction)((RemoteInventoryClientAction)var30.next()).get();
            synchronized(this.clientInventoryActions) {
               this.clientInventoryActions.enqueue(var29);
            }
         }
      }

   }

   private void handleTag(Tag var1) {
      if (!var1.getName().equals("wepContr")) {
         if (var1.getName().equals("controllerStructure")) {
            this.fromTagInventory(var1);
         } else {
            assert false : var1.getName();

         }
      }
   }

   public void initFromNetworkObject(NetworkObject var1) {
      this.getPowerInterface().initFromNetworkObject(var1);
      if (!this.isOnServer()) {
         this.relevantMap.putAll((Map)this.getSegmentController().getNetworkObject().relevantBlockCounts.get());
         Iterator var4 = this.relevantMap.entrySet().iterator();

         while(var4.hasNext()) {
            Entry var2;
            short var3 = (Short)(var2 = (Entry)var4.next()).getKey();
            int var5 = (Integer)var2.getValue();
            ManagerModule var6;
            if ((var6 = this.getModulesMap().get(var3)) != null && var6 instanceof ManagerModuleSingle) {
               ManagerModuleSingle var7;
               (var7 = (ManagerModuleSingle)var6).getCollectionManager().expected = var5;
               var7.getCollectionManager().rawCollection = new FastCopyLongOpenHashSet(var5);
            }
         }
      }

   }

   protected abstract void initialize(StateInterface var1);

   public void onAction() {
   }

   private void updateConnectionModified(long var1) {
      if (!this.flaggedConnections.isEmpty()) {
         ObjectIterator var3 = this.flaggedConnections.long2LongEntrySet().fastIterator();

         while(var3.hasNext()) {
            it.unimi.dsi.fastutil.longs.Long2LongMap.Entry var4 = (it.unimi.dsi.fastutil.longs.Long2LongMap.Entry)var3.next();
            if (var1 - var4.getLongValue() > 10000L) {
               var3.remove();
            }
         }
      }

   }

   private void flagConnectionModified(long var1, long var3) {
      this.flaggedConnections.put(var1, var3);
   }

   public boolean isConnectionFlagged(long var1) {
      return this.flaggedConnections.containsKey(var1);
   }

   private static void addToSpecialMap(short var0, boolean[] var1) {
      var1[var0] = true;
   }

   protected void getSpecialMap(boolean[] var1) {
      short[] var2;
      int var3 = (var2 = ElementKeyMap.typeList()).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         short var5;
         ElementInformation var6;
         if ((var6 = ElementKeyMap.getInfoFast(var5 = var2[var4])).isSignal() || var6.isRailTrack() || var6.isInventory() || this.getModulesControllerMap().get(var5) != null || this.getModulesMap().get(ElementKeyMap.getCollectionType(var5)) != null || var5 == 980 || var5 == 1 || var5 == 291 || var5 == 347 || var5 == 123 || var5 == 123 || var5 == 47 || var5 == 1126 || var5 == 663 || var5 == 679 || var5 == 121 || var5 == 66 || ElementKeyMap.factoryInfoArray[var5] || var6.isLightSource()) {
            var1[var5] = true;
         }
      }

   }

   public void addControllerBlockWithAddingBlock(short var1, long var2, Segment var4, boolean var5) {
      ElementInformation var6;
      ManagerModuleCollection var12;
      if ((var6 = ElementKeyMap.getInfoFast(var1)).isSignal()) {
         var12 = this.getModulesControllerMap().get((short)30000);
      } else if (var6.isRailTrack()) {
         var12 = this.getModulesControllerMap().get((short)29998);
      } else {
         var12 = this.getModulesControllerMap().get(var1);
      }

      if (var1 == 980) {
         var12 = this.getSensor();
      }

      Short2ObjectOpenHashMap var7;
      FastCopyLongOpenHashSet var16;
      if (var1 == 1 && (var7 = this.segmentController.getControlElementMap().getControllingMap().get(var2)) != null) {
         Iterator var8 = ElementKeyMap.lightTypes.iterator();

         while(var8.hasNext()) {
            short var9 = (Short)var8.next();
            if ((var16 = (FastCopyLongOpenHashSet)var7.get(var9)) != null && !var16.isEmpty()) {
               short var13;
               if (ElementKeyMap.isValidType(var13 = (short)ElementCollection.getType(var16.iterator().nextLong()))) {
                  this.colorCore.set(ElementKeyMap.getInfo(var13).getLightSourceColor());
               }
               break;
            }
         }
      }

      if (var12 != null) {
         if (var12.getElementManager() instanceof CargoCapacityElementManagerInterface) {
            this.getModulesControllerMap().get((short)120).addControllerBlockFromAddedBlock(var2, var4, var5);
         }

         var12.addControllerBlockFromAddedBlock(var2, var4, var5);
         if (this.isOnServer()) {
            this.onSpecialTypeAdd(var1, var2);
         }

         if (var1 == 677 || var1 == 120 || var1 == 347) {
            this.filterInventories.add(var2);
            return;
         }
      } else {
         ManagerModule var14;
         if ((var14 = this.getModulesMap().get(ElementKeyMap.getCollectionType(var1))) != null && var14 instanceof ManagerModuleSingle) {
            ((ManagerModuleSingle)var14).addElement(var2, var1);
         } else {
            assert var1 != 2;

            if (this.isOnServer()) {
               this.onSpecialTypeAdd(var1, var2);
            }
         }

         if (var1 == 120) {
            if (this.getSegmentController().getControlElementMap().getControllingMap().getAll().containsKey(var2) && !((FastCopyLongOpenHashSet)this.getSegmentController().getControlElementMap().getControllingMap().getAll().get(var2)).isEmpty()) {
               this.filterInventories.add(var2);
               return;
            }
         } else {
            Short2ObjectOpenHashMap var15;
            if (var1 == 66 && (var15 = this.getSegmentController().getControlElementMap().getControllingMap().get(var2)) != null && (var16 = (FastCopyLongOpenHashSet)var15.get((short)66)) != null) {
               Iterator var17 = var16.iterator();

               while(var17.hasNext()) {
                  long var10 = (Long)var17.next();
                  this.stabilizerNodePath.put(var2, ElementCollection.getPosIndexFrom4(var10));
               }
            }
         }
      }

   }

   public void onAddedElementSynched(short var1, Segment var2, long var3, long var5, boolean var7) {
      this.flagTargetRecalc = true;

      assert var1 < this.specialBlocks.length : ElementKeyMap.toString(var1) + "; " + this.specialBlocks.length;

      if (this.specialBlocks[var1]) {
         this.flagBlockAdded = true;
         if (var1 == 347) {
            this.shopBlockIndex = var3;
            if (this.segmentController.isOnServer()) {
               this.filterInventories.add(var3);
            }
         }

         if (var1 == 66) {
            this.stabilizerNodePath.addBlock(var3);
         }

         if (var1 == 291) {
            this.factionBlockPos = var3;
         }

         if (var1 == 123) {
            this.buildBlocks.add(var3);
         }

         if (this.segmentController.isOnServer() && (ElementKeyMap.factoryInfoArray[var1] || var1 == 677)) {
            LongOpenHashSet var8 = (LongOpenHashSet)this.getSegmentController().getControlElementMap().getControllingMap().getAll().get(var3);
            if (var1 == 677 || var8 != null && var8.size() > 0) {
               this.filterInventories.add(var3);
            }

            this.addInventory(var3, var1);
         }

         this.addControllerBlockWithAddingBlock(var1, var3, var2, var7);
         this.flagAnyBlockAdded = var5;
      }
   }

   public void onRemovedElementSynched(short var1, int var2, byte var3, byte var4, byte var5, Segment var6, boolean var7) {
      this.flagTargetRecalc = true;
      if (var1 == 66) {
         this.stabilizerNodePath.remove(var6.getAbsoluteIndex(var3, var4, var5));
         this.stabilizerNodePath.removeBlock(var6.getAbsoluteIndex(var3, var4, var5));
      }

      if (ElementKeyMap.getFactorykeyset().contains(var1)) {
         this.removeInventory(var3, var4, var5, var6, var7);
      }

      if (var1 == 347) {
         this.shopBlockIndex = Long.MIN_VALUE;
      }

      if (var1 == 291) {
         this.factionBlockPos = Long.MIN_VALUE;
      }

      if (var1 == 123) {
         this.buildBlocks.remove(var6.getAbsoluteIndex(var3, var4, var5));
      }

      ElementInformation var10;
      ManagerModuleCollection var11;
      if ((var10 = ElementKeyMap.getInfoFast(var1)).isSignal()) {
         var11 = this.getModulesControllerMap().get((short)30000);
      } else if (var10.isRailTrack()) {
         var11 = this.getModulesControllerMap().get((short)29998);
      } else {
         var11 = this.getModulesControllerMap().get(var1);
      }

      if (var1 == 980) {
         var11 = this.getSensor();
      }

      if (var11 != null) {
         if (var11.getElementManager() instanceof CargoCapacityElementManagerInterface) {
            this.getModulesControllerMap().get((short)120).removeControllerBlock(var3, var4, var5, var6);
         }

         var11.removeControllerBlock(var3, var4, var5, var6);
         if (var1 == 677 || var1 == 120 || var1 == 347) {
            this.filterInventories.remove(ElementCollection.getIndex(var3, var4, var5));
            this.removeInventory(var3, var4, var5, var6, var7);
         }

         this.onSpecialTypesRemove(var1, var3, var4, var5, var6, var7);
      } else {
         ManagerModule var12;
         if (ElementKeyMap.isDoor(var1)) {
            var12 = this.getModulesMap().get((short)122);
         } else {
            var12 = this.getModulesMap().get(var1);
         }

         if (var12 != null && var12 instanceof ManagerModuleSingle) {
            ((ManagerModuleSingle)var12).removeElement(var3, var4, var5, var6);
         } else {
            this.onSpecialTypesRemove(var1, var3, var4, var5, var6, var7);
         }

         if (var1 == 120 || var1 == 677) {
            long var8 = var6.getAbsoluteIndex(var3, var4, var5);
            this.filterInventories.remove(var8);
         }
      }

      this.flagAnyBlockRemoved = System.currentTimeMillis();
   }

   public void onConnectionAdded(Vector3i var1, short var2, Vector3i var3, short var4) {
      long var5 = ElementCollection.getIndex(var1);
      long var7 = ElementCollection.getIndex(var3);
      this.flagConnectionModified(var5, this.getState().getUpdateTime());
      this.flagTargetRecalc = true;
      if (var2 == 120 || ElementKeyMap.isToStashConnectable(var2) && var4 == 120) {
         this.filterInventories.add(var5);
         if (var4 != 689) {
            return;
         }
      }

      if (var2 == 66 && var4 == 66) {
         long var11;
         if (this.stabilizerNodePath.containsKey(var5) && (var11 = this.stabilizerNodePath.get(var5)) != var7) {
            this.segmentController.getControlElementMap().removeControllerForElement(var5, var11, (short)66);
         }

         this.stabilizerNodePath.put(var5, var7);
      } else if (var2 == 1 && ElementKeyMap.getInfo(var4).isLightSource()) {
         this.getSegmentController().getControlElementMap().disconnectAllLightBlocks(ElementCollection.getIndex(var1), ElementCollection.getIndex4(var3, var4));
         this.colorCore.set(ElementKeyMap.getInfo(var4).getLightSourceColor());
      } else if (!ElementKeyMap.getInfo(var2).isRailSpeedActivationConnect(var4)) {
         Object var9;
         if ((var9 = this.getModulesControllerMap().get(var2)) != null && ((ManagerModule)var9).getElementManager() instanceof CargoCapacityElementManagerInterface) {
            this.getModulesControllerMap().get((short)120).addControlledBlock(var1, var2, var3, var4);
         }

         if (var9 == null) {
            var9 = this.getModulesMap().get(var4);
         }

         if (ElementKeyMap.isValidType(var2) && ElementKeyMap.getInfo(var2).controlsAll()) {
            var9 = this.getModulesMap().get((short)32767);
         }

         if (var9 == null && ElementKeyMap.getInfo(var2).isRailSpeedTrackConnect(var4)) {
            var9 = this.getModulesMap().get((short)29999);
         }

         if (var2 == 980) {
            var9 = this.getSensor();
         }

         if (var9 == null) {
            if (this.getSegmentController().isClientOwnObject()) {
               ((GameClientState)this.getSegmentController().getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MANAGERCONTAINER_2, 0.0F);
            } else {
               System.err.println(this.getSegmentController().getState() + " WARNING: Could not find Manager Module for " + ElementKeyMap.toString(var2) + " -> " + ElementKeyMap.toString(var4) + " on " + this.getClass().getSimpleName() + ": " + this.getModulesMap());
            }
         } else {
            assert var9 != null : "critical: no module found for " + this.getSegmentController() + ": " + var4 + "; " + this.getModulesMap();

            if (((ManagerModule)var9).getNext() != null) {
               while(((ManagerModule)var9).getNext() != null) {
                  assert var9 instanceof ManagerModuleCollection;

                  ManagerModuleCollection var10;
                  if ((var10 = (ManagerModuleCollection)var9).getControllerID() == var2 || var10.getControllerID() == 30000 && ElementKeyMap.getInfo(var2).isSignal() || var10.getControllerID() == 29999 && ElementKeyMap.getInfo(var2).isRailTrack()) {
                     break;
                  }

                  var9 = ((ManagerModule)var9).getNext();
               }
            }

            assert var9 != null : var4 + " -> " + ElementKeyMap.getInfo(var4).getName() + ": " + this.getModulesMap();

            if (var9 == null) {
               throw new NullPointerException("Could not find Manager Module for " + var4 + ": " + this.getModulesMap());
            } else {
               if (var9 instanceof ManagerModuleCollection) {
                  ((ManagerModule)var9).addControlledBlock(var1, var2, var3, var4);
               }

            }
         }
      }
   }

   public void onConnectionRemoved(Vector3i var1, short var2, Vector3i var3, short var4) {
      this.flagTargetRecalc = true;
      long var5;
      if (var2 == 120 || ElementKeyMap.isToStashConnectable(var2) && var4 == 120) {
         var5 = ElementCollection.getIndex(var1);
         LongOpenHashSet var6;
         if ((var6 = (LongOpenHashSet)this.getSegmentController().getControlElementMap().getControllingMap().getAll().get(var5)) == null || var6.isEmpty()) {
            this.filterInventories.remove(ElementCollection.getIndex(var1));
         }

         if (var4 != 689) {
            return;
         }
      }

      if (var2 == 66 && var4 == 66) {
         var5 = ElementCollection.getIndex(var1);
         if (this.stabilizerNodePath.containsKey(var5) && this.stabilizerNodePath.get(var5) == ElementCollection.getIndex(var3)) {
            this.stabilizerNodePath.remove(var5);
         }

      } else if (var4 == 679) {
         System.err.println("[ManagerContainer] SHIPYARD CORE POSITION BLOCK CONNECTION REMOVED");
      } else if (var2 == 1 && ElementKeyMap.getInfo(var4).isLightSource()) {
         this.colorCore.set(this.colorCoreDefault);
      } else {
         assert this.getModulesMap() != null;

         assert var4 != 0;

         if (!ElementInformation.canBeControlledOld(var2, var4) && !ElementInformation.canBeControlled(var2, var4) && (!ElementKeyMap.isValidType(var4) || !this.getModulesMap().containsKey(var4)) && this.getState().getUpdateTime() - this.lastThrownInvalidConnectionException > 3000L) {
            try {
               throw new Exception("Object controllable status invalid: " + ElementKeyMap.toString(var2) + "; " + ElementKeyMap.toString(var4));
            } catch (Exception var8) {
               var8.printStackTrace();
               this.lastThrownInvalidConnectionException = this.getState().getUpdateTime();
            }
         }

         if (ElementKeyMap.isValidType(var2) && ElementKeyMap.isValidType(var4)) {
            if (!ElementKeyMap.getInfo(var2).isRailSpeedActivationConnect(var4)) {
               if (ElementKeyMap.isValidType(var4) && (this.getModulesMap().containsKey(var4) || ElementKeyMap.getInfo(var4).isSignal() || ElementKeyMap.getInfo(var4).isRailTrack() || ElementKeyMap.getInfo(var4).canActivate())) {
                  Object var9 = this.getModulesMap().get(var4);
                  ManagerModuleCollection var10;
                  if ((var10 = this.getModulesControllerMap().get(var2)) != null && var10.getElementManager() instanceof CargoCapacityElementManagerInterface) {
                     this.getModulesControllerMap().get((short)120).onConnectionRemoved(var1, var3, var4);
                  }

                  if (ElementKeyMap.isValidType(var2) && ElementKeyMap.getInfo(var2).controlsAll()) {
                     var9 = this.getModulesMap().get((short)32767);
                  }

                  if (var2 == 980) {
                     var9 = this.getSensor();
                  }

                  if (var9 != null || !ElementKeyMap.getInfo(var2).isLightConnect(var4)) {
                     if (ElementKeyMap.getInfo(var2).isCombiConnectAny(var4)) {
                        assert var10 != null;

                        var9 = var10;
                     }

                     if (var9 == null && ElementKeyMap.getInfo(var2).isRailSpeedTrackConnect(var4)) {
                        var9 = this.getModulesMap().get((short)29999);

                        assert var9 != null;
                     }

                     if (var9 != null || var2 != 4 || var4 != 120) {
                        if (var9 != null || !ElementKeyMap.getInfo(var4).isSignal()) {
                           if (var9 == null) {
                              if (this.getState().getUpdateTime() - this.lastThrownInvalidConnectionException > 5000L) {
                                 System.err.println("Exception: manager module null");

                                 try {
                                    throw new Exception("manager null (doesnt belong / maybe deprecated block module) " + ElementKeyMap.toString(var2) + " -> " + ElementKeyMap.toString(var4) + "; canCon: " + ElementInformation.canBeControlled(var2, var4) + "; ModuleMap.get(controlledType): " + this.getModulesMap().containsKey(var4));
                                 } catch (Exception var7) {
                                    var7.printStackTrace();
                                    this.lastThrownInvalidConnectionException = this.getState().getUpdateTime();
                                 }
                              }

                           } else {
                              if (((ManagerModule)var9).getNext() != null) {
                                 while(((ManagerModule)var9).getNext() != null) {
                                    assert var9 instanceof ManagerModuleCollection;

                                    if ((var10 = (ManagerModuleCollection)var9).getControllerID() == var2 || var10.getControllerID() == 30000 && ElementKeyMap.getInfo(var2).isSignal() || var10.getControllerID() == 29999 && ElementKeyMap.getInfo(var2).isRailTrack()) {
                                       break;
                                    }

                                    var9 = ((ManagerModule)var9).getNext();
                                 }
                              }

                              assert var9 != null : var4 + " -> " + ElementKeyMap.getInfo(var4).getName() + ": " + this.getModulesMap();

                              if (var9 instanceof ManagerModuleCollection) {
                                 ((ManagerModule)var9).onConnectionRemoved(var1, var3, var4);
                              }

                           }
                        }
                     }
                  }
               } else {
                  if ((long)this.getState().getNumberOfUpdate() - this.lastThrownInvalidConnectionException > 2000L) {
                     System.err.println("Exception: tried to remove controller: " + ElementKeyMap.toString(var4) + ": " + this.getModulesMap() + " for " + this.getSegmentController());
                     this.lastThrownInvalidConnectionException = (long)this.getState().getNumberOfUpdate();
                  }

               }
            }
         } else {
            System.err.println("Exception: tried to remove controller to invalid type: " + ElementKeyMap.toString(var2) + " -> " + ElementKeyMap.toString(var4) + ": " + var1 + " -> " + var3);
         }
      }
   }

   public void onBlockDamage(long var1, short var3, int var4, DamageDealerType var5, Damager var6) {
      for(int var7 = 0; var7 < this.hittableModules.size(); ++var7) {
         ((HittableInterface)this.hittableModules.get(var7)).onHit(var1, var3, (double)var4, var5);
      }

      if (this.isOnServer()) {
         this.getPowerInterface().onBlockDamageServer(var6, var4, var3, var1);
      }

      if (var4 > 0) {
         this.flagBlockDamaged = true;
      }

   }

   public void onBlockKill(long var1, short var3, Damager var4) {
      for(int var5 = 0; var5 < this.killableBlockModules.size(); ++var5) {
         ((BlockKillInterface)this.killableBlockModules.get(var5)).onKilledBlock(var1, var3, var4);
      }

      if (this.isOnServer() && ElementKeyMap.isValidType(var3) && (var3 == 1008 || var3 == 1009 || var3 == 1010 || ElementKeyMap.getInfo(var3).isReactorChamberAny())) {
         this.getPowerInterface().onBlockKilledServer(var4, var3, var1);
         this.flagModuleKilled = true;
      }

      if (this.modulesMap.containsKey(var3)) {
         this.flagModuleKilled = true;
      }

      this.flagBlockKilled = true;
   }

   public void onHitNotice() {
   }

   private void onInventoryRemove(Inventory var1, boolean var2) {
      assert this.isOnServer();

      if (!var2 && this.segmentController.isOnServer()) {
         System.err.println("[MANAGERCONTAINER] REMOVING INVENTORY! (now spawning in space)" + this.getSegmentController().getState() + " - " + this.getSegmentController());
         var1.spawnInSpace(this.getSegmentController());
      }

   }

   protected void onSpecialTypeAdd(short var1, long var2) {
      assert this.isOnServer();

      if (var1 == 120) {
         this.addInventory(var2, var1);
         LongOpenHashSet var4;
         if ((var4 = (LongOpenHashSet)this.getSegmentController().getControlElementMap().getControllingMap().getAll().get(var2)) != null && var4.size() > 0) {
            this.filterInventories.add(var2);
         }
      }

   }

   public boolean isOnServer() {
      return this.onServer;
   }

   protected void onSpecialTypesRemove(short var1, byte var2, byte var3, byte var4, Segment var5, boolean var6) {
      if (var1 == 120) {
         this.filterInventories.remove(ElementCollection.getIndex(var2, var3, var4));
         this.removeInventory(var2, var3, var4, var5, var6);
      }

   }

   private void removeInventory(byte var1, byte var2, byte var3, Segment var4, boolean var5) {
      if (this.getSegmentController().isOnServer()) {
         System.err.println("[MANAGERCONTAINER] onRemovedElement REMOVING INVENTORY!! " + this.getSegmentController() + "; " + this.getSegmentController().getState() + " preserveC: " + var5);
         if (!var5) {
            Vector3i var6 = var4.getAbsoluteElemPos(var1, var2, var3, new Vector3i());
            Vector4i var7 = new Vector4i(var6, var5 ? 1 : 0);
            if (this.getInventories().containsKey(ElementCollection.getIndex(var6)) && !this.delayedInventoryRemove.contains(var7)) {
               this.delayedInventoryRemove.add(var7);
            }

            return;
         }

         System.err.println("[SERVER] keeping inventory for now so it's not emptied by anything " + this.getSegmentController());
      }

   }

   public void sendInventoryDelayed(Inventory var1, int var2) {
      IntOpenHashSet var3;
      if ((var3 = (IntOpenHashSet)this.delayedCummulativeInventoryModsToSend.get(var1)) == null) {
         var3 = new IntOpenHashSet();
         this.delayedCummulativeInventoryModsToSend.put(var1, var3);
      }

      var3.add(var2);
   }

   public Tag getInventoryTag() {
      synchronized(this.getInventories()) {
         Tag[] var1 = new Tag[this.getInventories().size() + 1];
         int var3 = 0;

         for(Iterator var4 = this.getInventories().entrySet().iterator(); var4.hasNext(); ++var3) {
            Entry var5 = (Entry)var4.next();
            Tag[] var6;
            (var6 = new Tag[4])[0] = new Tag(Tag.Type.INT, (String)null, ((Inventory)var5.getValue()).getLocalInventoryType());
            var6[1] = new Tag(Tag.Type.VECTOR3i, (String)null, ElementCollection.getPosFromIndex((Long)var5.getKey(), new Vector3i()));
            var6[2] = ((Inventory)var5.getValue()).toTagStructure();
            var6[3] = FinishTag.INST;
            var1[var3] = new Tag(Tag.Type.STRUCT, (String)null, var6);
         }

         var1[this.getInventories().size()] = FinishTag.INST;
         return new Tag(Tag.Type.STRUCT, (String)null, var1);
      }
   }

   public Tag toTagStructure() {
      Tag var1 = this.getInventoryTag();
      Tag var2 = new Tag(Tag.Type.INT, "shipMan0", 0);
      Tag var3;
      if (this instanceof PowerManagerInterface) {
         var3 = ((PowerManagerInterface)this).getPowerAddOn().toTagStructure();
      } else {
         var3 = new Tag(Tag.Type.BYTE, (String)null, 0);
      }

      Tag var4;
      if (this instanceof ShieldContainerInterface) {
         if (this.isUsingPowerReactors()) {
            var4 = ((ShieldContainerInterface)this).getShieldAddOn().getShieldLocalAddOn().toTagStructure();
         } else {
            var4 = new Tag(Tag.Type.DOUBLE, "sh", ((ShieldContainerInterface)this).getShieldAddOn().getShields());
         }
      } else {
         var4 = new Tag(Tag.Type.BYTE, (String)null, 0);
      }

      Tag var5 = ((SendableSegmentController)this.segmentController).getActivationStateTag();
      Tag var6 = this.segmentController.getTextBlockTag();
      Tag var7 = this.getRelevantElementCountMap();
      Tag var8 = this.getWarpGateTag();
      Tag var9 = this.getRaceGateTag();
      Tag var10 = this.getSegmentController().aiToTagStructure();
      Tag var11 = this.getUnloadedDummiesTag();
      Tag var12 = Tag.listToTagStruct((List)this.moduleExplosions, (String)null);
      Tag var13 = this.getPowerInterface().toTagStructure();
      return new Tag(Tag.Type.STRUCT, "container", new Tag[]{var1, var2, var3, var4, this.getExtraTag(), var5, var6, var7, var8, this.getModuleTag(), var10, this.segmentController.getSlotAssignment().toTagStructure(), var9, var11, var12, var13, Tag.getByteTag((byte)this.segmentController.pullPermission.ordinal()), FinishTag.INST});
   }

   private Tag getUnloadedDummiesTag() {
      Tag[] var1;
      Tag[] var10000 = var1 = new Tag[this.initialBlockMetaData.values().size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;
      int var2 = 0;

      for(Iterator var3 = this.initialBlockMetaData.values().iterator(); var3.hasNext(); ++var2) {
         BlockMetaDataDummy var4 = (BlockMetaDataDummy)var3.next();
         var1[var2] = var4.toTagStructure();
      }

      return new Tag(Tag.Type.STRUCT, this.getSegmentController().isLoadedFromChunk16() ? "c16" : null, var1);
   }

   public Tag getModuleTag() {
      ObjectArrayList var1 = new ObjectArrayList();
      Iterator var2 = this.tagModules.iterator();

      while(var2.hasNext()) {
         TagModuleUsableInterface var3 = (TagModuleUsableInterface)var2.next();
         var1.add(var3.toTagStructure());
      }

      Tag[] var5 = new Tag[var1.size() + 1];

      for(int var4 = 0; var4 < var1.size(); ++var4) {
         var5[var4] = (Tag)var1.get(var4);
      }

      var5[var5.length - 1] = FinishTag.INST;
      return new Tag(Tag.Type.STRUCT, (String)null, var5);
   }

   protected void fromWarpGateTag(Tag var1) {
   }

   protected Tag getWarpGateTag() {
      return new Tag(Tag.Type.BYTE, (String)null, (byte)0);
   }

   protected void fromRaceGateTag(Tag var1) {
   }

   protected Tag getRaceGateTag() {
      return new Tag(Tag.Type.BYTE, (String)null, (byte)0);
   }

   private Tag getRelevantElementCountMap() {
      ShortArrayList var1 = new ShortArrayList();

      for(int var2 = 0; var2 < this.getModules().size(); ++var2) {
         if (this.getModules().get(var2) instanceof ManagerModuleSingle) {
            short var3 = ((ManagerModuleSingle)this.getModules().get(var2)).getElementID();
            var1.add(var3);
         }
      }

      Tag[] var6;
      Tag[] var10000 = var6 = new Tag[var1.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;

      for(int var7 = 0; var7 < var1.size(); ++var7) {
         Tag var4 = new Tag(Tag.Type.SHORT, (String)null, var1.get(var7));
         int var5 = this.segmentController.getElementClassCountMap().get(var1.get(var7));
         Tag var8 = new Tag(Tag.Type.INT, (String)null, var5);
         var6[var7] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var4, var8, FinishTag.INST});
      }

      return new Tag(Tag.Type.STRUCT, (String)null, var6);
   }

   private void readRelevantBlockCounts(Tag var1) {
      Tag[] var6 = (Tag[])var1.getValue();

      for(int var2 = 0; var2 < var6.length - 1; ++var2) {
         Tag[] var3;
         short var4 = (Short)(var3 = (Tag[])var6[var2].getValue())[0].getValue();
         int var7 = (Integer)var3[1].getValue();
         ManagerModule var5;
         if (ElementKeyMap.isValidType(var4) && (var5 = this.getModulesMap().get(var4)) != null && var5 instanceof ManagerModuleSingle) {
            ManagerModuleSingle var8 = (ManagerModuleSingle)var5;
            if (var7 < 0) {
               System.err.println("[SERVER][LAOD] Exception: " + this.getSegmentController() + " provided a negative value as expected block count for: " + ElementKeyMap.toString(var4) + "; This file might have been messed with!");
               var8.getCollectionManager().expected = 2;
            } else {
               var8.getCollectionManager().expected = Math.max(2, var7);
            }

            if (var7 > 128) {
               this.relevantMap.put(var4, var7);
            }
         }
      }

   }

   public void updateFromNetworkObject(NetworkObject var1, int var2) {
      for(var2 = 0; var2 < this.receiverModules.size(); ++var2) {
         ((NTReceiveInterface)this.receiverModules.get(var2)).updateFromNT(this.getSegmentController().getNetworkObject());
      }

      ObjectArrayList var7;
      if (this.getSegmentController().getNetworkObject() instanceof NTValueUpdateInterface && !(var7 = ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().getReceiveBuffer()).isEmpty()) {
         synchronized(this.valueUpdates) {
            for(int var4 = 0; var4 < var7.size(); ++var4) {
               RemoteValueUpdate var5 = (RemoteValueUpdate)var7.get(var4);
               this.valueUpdates.enqueue(var5.get());
            }
         }
      }

      this.getPowerInterface().updateFromNetworkObject(var1);
      this.handleInventoryFromNT(this.getInventoryInterface().getInventoriesChangeBuffer(), this.getInventoryInterface().getInventoryProductionBuffer(), this.getInventoryInterface().getInventoryProductionLimitBuffer(), this.getInventoryInterface().getInventoryFilterBuffer(), this.getInventoryInterface().getInventoryFillBuffer(), this.getInventoryInterface().getInventoryCustomNameModBuffer());
      if (!this.isOnServer() && !this.isInClientRange()) {
         ((GameClientState)this.getState()).unloadedInventoryUpdates.enqueue(this);
      }

      for(var2 = 0; var2 < this.segmentController.getNetworkObject().blockDelayTimers.getReceiveBuffer().size(); var2 += 2) {
         this.integrityUpdateDelay = this.segmentController.getNetworkObject().blockDelayTimers.getReceiveBuffer().get(var2);
         this.repairDelay = this.segmentController.getNetworkObject().blockDelayTimers.getReceiveBuffer().get(var2 + 1);
      }

   }

   public boolean isInClientRange() {
      return this.segmentController.isInClientRange();
   }

   public void handleInventoryReceivedNT() {
      Inventory var2;
      if (this.getInventoryInterface() != null && !this.ntInventoryRemoves.isEmpty()) {
         synchronized(this.ntInventoryRemoves) {
            while(!this.ntInventoryRemoves.isEmpty()) {
               var2 = (Inventory)this.ntInventoryRemoves.dequeue();
               var2 = this.getInventories().remove(var2.getParameterIndex());
               if (!this.isOnServer() && var2 != null) {
                  this.volumeChanged(var2.getVolume(), 0.0D);
                  if (var2 instanceof StashInventory && ((StashInventory)var2).getCustomName() != null && ((StashInventory)var2).getCustomName().length() > 0) {
                     this.getNamedInventoriesClient().remove(var2.getParameterIndex());
                     this.namedInventoriesClientChanged = true;
                  }
               }
            }
         }
      }

      if (!this.getSegmentController().isOnServer()) {
         for(int var1 = 0; var1 < this.inventories.inventoriesList.size(); ++var1) {
            ((Inventory)this.inventories.inventoriesList.get(var1)).clientUpdate();
         }
      }

      if (this.getInventoryInterface() != null && !this.ntInventoryMods.isEmpty()) {
         synchronized(this.ntInventoryMods) {
            while(true) {
               while(!this.ntInventoryMods.isEmpty()) {
                  if ((var2 = (Inventory)this.ntInventoryMods.dequeue()) instanceof ShopInventory) {
                     this.handleShopInventoryReceived((ShopInventory)var2);
                  } else {
                     if (!this.isOnServer() && var2 instanceof StashInventory && ((StashInventory)var2).getCustomName() != null && ((StashInventory)var2).getCustomName().length() > 0) {
                        this.getNamedInventoriesClient().put(var2.getParameterIndex(), (StashInventory)var2);
                        this.namedInventoriesClientChanged = true;
                     }

                     this.getInventories().put(var2.getParameterIndex(), var2);
                     if (!this.getSegmentController().isOnServer()) {
                        var2.requestMissingMetaObjects();
                        ManagerContainer.DelayedInvProdSet var3;
                        if ((var3 = (ManagerContainer.DelayedInvProdSet)this.invProdMapDelayed.remove(var2.getParameterIndex())) != null && var2 instanceof StashInventory) {
                           if (var3.inventoryProduction != 0) {
                              this.getInventoryInterface().getInventoryProductionBuffer().add(ElementCollection.getIndex4(var2.getParameterIndex(), var3.inventoryProduction));
                           }

                           Iterator var4;
                           Entry var5;
                           if (var3.inventoryFilter != null) {
                              var4 = var3.inventoryFilter.entrySet().iterator();

                              while(var4.hasNext()) {
                                 var5 = (Entry)var4.next();
                                 this.getInventoryInterface().getInventoryFilterBuffer().add(new RemoteShortIntPair(new ShortIntPair(var2.getParameterIndex(), (Short)var5.getKey(), (Integer)var5.getValue()), this.segmentController.isOnServer()));
                              }
                           }

                           if (var3.inventoryProductionLimit != 0) {
                              this.getInventoryInterface().getInventoryProductionLimitBuffer().add((Streamable)(new RemoteLongIntPair(new LongIntPair(var2.getParameterIndex(), var3.inventoryProductionLimit), this.segmentController.isOnServer())));
                           }

                           if (var3.inventoryFillUpFilters != null) {
                              var4 = var3.inventoryFillUpFilters.entrySet().iterator();

                              while(var4.hasNext()) {
                                 var5 = (Entry)var4.next();
                                 this.getInventoryInterface().getInventoryFillBuffer().add(new RemoteShortIntPair(new ShortIntPair(var2.getParameterIndex(), (Short)var5.getKey(), (Integer)var5.getValue()), this.segmentController.isOnServer()));
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      if (!this.ntInventorySlotRemoveMods.isEmpty()) {
         synchronized(this.ntInventorySlotRemoveMods) {
            while(!this.ntInventorySlotRemoveMods.isEmpty()) {
               InventorySlotRemoveMod var20;
               if ((var20 = (InventorySlotRemoveMod)this.ntInventorySlotRemoveMods.dequeue()).parameter == Long.MIN_VALUE) {
                  this.handleGlobalInventorySlotRemove(var20);
               } else {
                  Inventory var22;
                  if ((var22 = this.getInventory(var20.parameter)) != null) {
                     boolean var29 = this.isOnServer();
                     var22.removeSlot(var20.slot, var29);
                  }
               }
            }
         }
      }

      Inventory var31;
      if (!this.ntInventoryMultMods.isEmpty()) {
         synchronized(this.ntInventoryMultMods) {
            ObjectArrayList var21 = new ObjectArrayList();

            while(true) {
               while(!this.ntInventoryMultMods.isEmpty()) {
                  InventoryMultMod var23;
                  if ((var23 = (InventoryMultMod)this.ntInventoryMultMods.dequeue()).parameter == Long.MIN_VALUE) {
                     this.handleGlobalInventory(var23);
                  } else if ((var31 = this.getInventory(var23.parameter)) != null) {
                     if (this.isOnServer()) {
                        boolean var36 = var31.isEmpty();
                        boolean var6 = var31.isAlmostFull();
                        var31.handleReceived(var23, this.getInventoryInterface());
                        boolean var26 = var31.isEmpty();
                        boolean var7 = var31.isAlmostFull();
                        if (!var36 && var26) {
                           this.sendConnected(var31.getParameterIndex(), false, (short)409);
                        } else if (var36 && !var26) {
                           this.sendConnected(var31.getParameterIndex(), true, (short)409);
                        } else if (!var6 && var7) {
                           this.sendConnected(var31.getParameterIndex(), false, (short)405);
                        } else if (var6 && !var7) {
                           this.sendConnected(var31.getParameterIndex(), true, (short)405);
                        }
                     } else {
                        var31.handleReceived(var23, this.getInventoryInterface());
                     }
                  } else {
                     if (!this.segmentController.isOnServer() && this.isInClientRange()) {
                        SegmentPiece var34;
                        if ((var34 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var23.parameter)) == null) {
                           var21.add(var23);
                        }

                        System.err.println("[MANAGERCONTAINER] Exc: NOT FOUND MULT INVENTORY (received mod): " + this.getSegmentController() + " - " + var23.parameter + "; destination: " + var34 + "; " + this.getInventories().keySet());
                     }

                     assert !this.segmentController.isOnServer();
                  }
               }

               if (!var21.isEmpty()) {
                  while(!var21.isEmpty()) {
                     this.ntInventoryMultMods.enqueue(var21.remove(0));
                  }
               }
               break;
            }
         }
      }

      Inventory var39;
      if (!this.ntInventoryProdMods.isEmpty()) {
         synchronized(this.ntInventoryProdMods) {
            while(!this.ntInventoryProdMods.isEmpty()) {
               long var24;
               Vector3i var32 = ElementCollection.getPosFromIndex(var24 = this.ntInventoryProdMods.dequeueLong(), new Vector3i());
               short var38 = (short)ElementCollection.getType(var24);
               if ((var39 = this.getInventory(var32)) != null && var39 instanceof StashInventory) {
                  ((StashInventory)var39).setProduction(var38);
                  if (this.getSegmentController().isOnServer()) {
                     this.getInventoryInterface().getInventoryProductionBuffer().add(var24);
                  }
               }
            }
         }
      }

      Vector3i var27;
      if (!this.ntInventoryProdLimitMods.isEmpty()) {
         synchronized(this.ntInventoryProdLimitMods) {
            while(!this.ntInventoryProdLimitMods.isEmpty()) {
               LongIntPair var25;
               var27 = ElementCollection.getPosFromIndex((var25 = (LongIntPair)this.ntInventoryProdLimitMods.dequeue()).l, new Vector3i());
               if ((var31 = this.getInventory(var27)) != null && var31 instanceof StashInventory) {
                  ((StashInventory)var31).setProductionLimit(var25.i);
                  if (this.getSegmentController().isOnServer()) {
                     this.getInventoryInterface().getInventoryProductionLimitBuffer().add((Streamable)(new RemoteLongIntPair(var25, this.isOnServer())));
                  }
               }
            }
         }
      }

      if (!this.ntInventoryCustomNameMods.isEmpty()) {
         synchronized(this.ntInventoryCustomNameMods) {
            while(!this.ntInventoryCustomNameMods.isEmpty()) {
               LongStringPair var28;
               var27 = ElementCollection.getPosFromIndex((var28 = (LongStringPair)this.ntInventoryCustomNameMods.dequeue()).longVal, new Vector3i());
               if ((var31 = this.getInventory(var27)) != null && var31 instanceof StashInventory) {
                  ((StashInventory)var31).setCustomName(var28.stringVal);
                  if (this.getSegmentController().isOnServer()) {
                     this.getInventoryInterface().getInventoryCustomNameModBuffer().add(new RemoteLongString(var28, true));
                  } else {
                     if (var28.stringVal.length() > 0) {
                        this.getNamedInventoriesClient().put(var28.longVal, (StashInventory)var31);
                     } else {
                        this.getNamedInventoriesClient().remove(var28.longVal);
                     }

                     this.namedInventoriesClientChanged = true;
                  }
               }
            }
         }
      }

      ShortIntPair var30;
      short var40;
      int var41;
      if (!this.ntInventoryFilterMods.isEmpty()) {
         synchronized(this.ntInventoryFilterMods) {
            while(!this.ntInventoryFilterMods.isEmpty()) {
               var27 = ElementCollection.getPosFromIndex((var30 = (ShortIntPair)this.ntInventoryFilterMods.dequeue()).pos, new Vector3i());
               var40 = var30.type;
               var41 = var30.count;
               if ((var39 = this.getInventory(var27)) != null && var39 instanceof StashInventory) {
                  if (var41 == 0) {
                     ((StashInventory)var39).getFilter().filter.remove(var40);
                  } else {
                     assert var41 > 0;

                     ((StashInventory)var39).getFilter().filter.put(var40, var41);
                  }

                  if (this.getSegmentController().isOnServer()) {
                     this.getInventoryInterface().getInventoryFilterBuffer().add(new RemoteShortIntPair(var30, true));
                  }
               }
            }
         }
      }

      if (!this.ntInventoryFillMods.isEmpty()) {
         synchronized(this.ntInventoryFillMods) {
            while(!this.ntInventoryFillMods.isEmpty()) {
               var27 = ElementCollection.getPosFromIndex((var30 = (ShortIntPair)this.ntInventoryFillMods.dequeue()).pos, new Vector3i());
               var40 = var30.type;
               var41 = var30.count;
               if ((var39 = this.getInventory(var27)) != null && var39 instanceof StashInventory) {
                  if (var41 == 0) {
                     ((StashInventory)var39).getFilter().fillUpTo.remove(var40);
                  } else {
                     assert var41 > 0;

                     ((StashInventory)var39).getFilter().fillUpTo.put(var40, var41);
                  }

                  if (this.getSegmentController().isOnServer()) {
                     this.getInventoryInterface().getInventoryFillBuffer().add(new RemoteShortIntPair(var30, true));
                  }
               }
            }
         }
      }

      if (!this.clientInventoryActions.isEmpty()) {
         assert this.isOnServer();

         Object2ObjectOpenHashMap var19 = new Object2ObjectOpenHashMap();
         synchronized(this.clientInventoryActions) {
            while(!this.clientInventoryActions.isEmpty()) {
               try {
                  InventoryClientAction var35 = (InventoryClientAction)this.clientInventoryActions.dequeue();

                  assert var35.ownInventoryOwnerId == this.getId();

                  Sendable var42;
                  if ((var42 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var35.otherInventoryOwnerId)) != null) {
                     Object var43;
                     if (var42 instanceof ManagedSegmentController) {
                        var43 = ((ManagedSegmentController)var42).getManagerContainer();
                     } else {
                        var43 = (InventoryHolder)var42;
                     }

                     if ((var39 = this.getInventory(var35.ownInventoryPosId)) != null) {
                        var39.doSwitchSlotsOrCombine(var35.slot, var35.otherSlot, var35.subSlot, ((InventoryHolder)var43).getInventory(var35.otherInventoryPosId), var35.count, var19);
                     } else {
                        assert false;
                     }
                  } else {
                     assert false;
                  }
               } catch (InventoryExceededException var8) {
                  var8.printStackTrace();
               }
            }
         }

         if (!var19.isEmpty()) {
            Iterator var33 = var19.entrySet().iterator();

            while(var33.hasNext()) {
               Entry var37;
               ((Inventory)(var37 = (Entry)var33.next()).getKey()).sendInventoryModification((IntCollection)var37.getValue());
            }
         }
      }

   }

   private void addRechModules() {
      while(!this.rechargableSinfleModulesToAdd.isEmpty()) {
         RecharchableSingleModule var1 = (RecharchableSingleModule)this.rechargableSinfleModulesToAdd.dequeue();
         this.addPlayerUsable(var1);
         this.addConsumer(var1);
         this.addTagUsable(var1);
         this.addUpdatable(var1);
      }

   }

   public void updateLocal(Timer var1) {
      this.getState().getDebugTimer().start(this.getSegmentController(), "ManagerContainerUpdate");
      long var2 = System.currentTimeMillis();
      int var4 = this.updatable.size();

      int var5;
      for(var5 = 0; var5 < var4; ++var5) {
         ManagerUpdatableInterface var6;
         if ((var6 = (ManagerUpdatableInterface)this.updatable.get(var5)).canUpdate()) {
            var6.update(var1);
         } else {
            var6.onNoUpdate(var1);
         }
      }

      if (this.integrityUpdateDelay > 0.0F) {
         this.integrityUpdateDelay = Math.max(0.0F, this.integrityUpdateDelay - var1.getDelta());
      }

      if (this.repairDelay > 0.0F) {
         this.repairDelay = Math.max(0.0F, this.repairDelay - var1.getDelta());
      }

      if (this.flagBlockDamaged) {
         this.repairDelay = RepairElementManager.REPAIR_OUT_OF_COMBAT_DELAY_SEC;
         this.flagBlockDamaged = false;
      }

      if (this.flagBlockKilled) {
         this.repairDelay = RepairElementManager.REPAIR_OUT_OF_COMBAT_DELAY_SEC;
         this.flagBlockKilled = false;
      }

      if (this.flagModuleKilled) {
         this.integrityUpdateDelay = VoidElementManager.COLLECTION_INTEGRITY_UNDER_FIRE_UPDATE_DELAY_SEC;
         this.flagModuleKilled = false;
      }

      if (this.flagBlockAdded) {
         if (this.getSegmentController().isFullyLoadedWithDock()) {
            this.integrityUpdateDelay = 0.0F;
         }

         this.flagBlockAdded = false;
      }

      if (this.flagElementCollectionChanged) {
         if (this.isOnServer()) {
            this.getSegmentController().getRuleEntityManager().triggerOnCollectionUpdate();
         }

         this.flagElementCollectionChanged = false;
      }

      if (!this.manualEvents.isEmpty()) {
         for(var5 = 0; var5 < this.manualEvents.size(); ++var5) {
            ManagerContainer.ManualMouseEvent var14;
            if (!(var14 = (ManagerContainer.ManualMouseEvent)this.manualEvents.get(var5)).sent && !this.isOnServer()) {
               NetworkSegmentProvider var7 = ((ClientSegmentProvider)this.getSegmentController().getSegmentProvider()).getSendableSegmentProvider().getNetworkObject();
               var14.send(var7);
            }

            var14.sent = true;
            if (var14.execute(this, var1)) {
               this.manualEvents.remove(var5--);
            }
         }
      }

      Iterator var15;
      if (!this.receivedBeamLatches.isEmpty()) {
         while(!this.receivedBeamLatches.isEmpty()) {
            ManagerContainer.ReceivedBeamLatch var16 = (ManagerContainer.ReceivedBeamLatch)this.receivedBeamLatches.dequeue();
            var15 = this.beamModules.iterator();

            while(var15.hasNext() && !((BeamElementManager)var15.next()).handleBeamLatch(var16)) {
            }
         }
      }

      this.updateConnectionModified(var1.currentTime);

      while(!((SendableSegmentController)this.getSegmentController()).receivedBlockMessages.isEmpty()) {
         assert !this.isOnServer();

         ServerMessage var18 = (ServerMessage)((SendableSegmentController)this.getSegmentController()).receivedBlockMessages.dequeue();
         ((GameClientState)this.getState()).getPlayer().handleReceivedBlockMsg(this, var18);
      }

      if (!this.blockActivationsForModules.isEmpty()) {
         ObjectIterator var21 = this.blockActivationsForModules.values().iterator();

         label369:
         while(true) {
            ManagerContainer.BlockAct var17;
            SegmentPiece var20;
            do {
               label359:
               do {
                  while(true) {
                     while(true) {
                        if (!var21.hasNext()) {
                           break label369;
                        }

                        var17 = (ManagerContainer.BlockAct)var21.next();
                        this.unitSingle.block = var17.block;
                        var17.module.handle(this.unitSingle, var1);
                        if (this.isOnServer()) {
                           if (var17.trigger != Long.MIN_VALUE) {
                              continue label359;
                           }

                           if (var1.currentTime - var17.timeStarted > 500L) {
                              if ((var20 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var17.block, this.tmp)) != null) {
                                 var20.setActive(false);
                                 ((SendableSegmentController)this.getSegmentController()).sendBlockActiveChanged(ElementCollection.getPosX(var17.block), ElementCollection.getPosY(var17.block), ElementCollection.getPosZ(var17.block), var20.isActive());
                              }

                              var21.remove();
                           }
                        } else if ((var20 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var17.block, this.tmp)) != null && !var20.isActive()) {
                           var21.remove();
                        }
                     }
                  }
               } while((var20 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var17.trigger, this.tmp)) == null);
            } while(var20.isValid() && !var20.isDead() && var20.isActive());

            var21.remove();
         }
      }

      for(var5 = 0; var5 < this.moduleExplosions.size(); ++var5) {
         ModuleExplosion var19;
         if (!(var19 = (ModuleExplosion)this.moduleExplosions.get(var5)).isFinished()) {
            var19.update(var1, this.getSegmentController());
         }

         if (var19.isFinished()) {
            this.moduleExplosions.remove(var5);
            --var5;
         }
      }

      if (!this.inventoryDetailRequests.isEmpty()) {
         while(!this.inventoryDetailRequests.isEmpty()) {
            ManagerContainer.InventoryDetailRequest var30;
            var15 = (var30 = (ManagerContainer.InventoryDetailRequest)this.inventoryDetailRequests.dequeue()).requested.iterator();

            while(var15.hasNext()) {
               long var22 = (Long)var15.next();
               Inventory var9;
               if ((var9 = this.getInventory(var22)) != null) {
                  var9.receivedFilterRequest(var30.prov);
               } else {
                  System.err.println("[MANAGERCONTAINER] WARNING: " + this.getSegmentController() + " Inventory Detail request: ID not found " + var22);
               }
            }
         }
      }

      while(!this.effectChangeQueue.isEmpty()) {
         ((EffectChangeHanlder)this.effectChangeQueue.dequeue()).onEffectChanged();
      }

      Inventory var26;
      long var32;
      ObjectArrayList var33;
      if (this.getSegmentController().itemsToSpawnWith != null) {
         if (this.itemToSpawnCheckStart <= 0L) {
            this.itemToSpawnCheckStart = var1.currentTime;
         }

         if (var1.currentTime - this.itemToSpawnCheckStart > 8000L) {
            Collections.sort(var33 = new ObjectArrayList(this.getInventories().inventoriesList), new Comparator() {
               public int compare(Inventory var1, Inventory var2) {
                  double var3 = var1.getCapacity() - var1.getVolume();
                  return CompareTools.compare(var2.getCapacity() - var2.getVolume(), var3);
               }
            });
            var15 = var33.iterator();

            while(var15.hasNext()) {
               if ((var26 = (Inventory)var15.next()).getCapacity() - var26.getVolume() > 1.0D) {
                  IntOpenHashSet var8 = new IntOpenHashSet();
                  var32 = this.getSegmentController().itemsToSpawnWith.getTotalAmount();
                  var26.incAndConsume(this.getSegmentController().itemsToSpawnWith, var8);
                  long var11 = this.getSegmentController().itemsToSpawnWith.getTotalAmount();
                  System.err.println("[SERVER][SPAWNWITHCARGO] PUT IN " + (var32 - var11) + "; CHANGED SLOTS " + var8.size());
                  if (var8.size() > 0) {
                     var26.sendInventoryModification(var8);
                  }

                  if (this.getSegmentController().itemsToSpawnWith.getExistingTypeCount() == 0) {
                     break;
                  }
               }
            }

            System.err.println("[SERVER][SPAWNWITHCARGO] CARGO TYPES STILL TO SPAWN: " + this.getSegmentController().itemsToSpawnWith.getExistingTypeCount());
            if (this.getSegmentController().itemsToSpawnWith.getExistingTypeCount() == 0 || var1.currentTime - this.itemToSpawnCheckStart > 30000L) {
               this.getSegmentController().itemsToSpawnWith = null;
            }
         }
      }

      int var31;
      if (!this.valueUpdates.isEmpty()) {
         var33 = new ObjectArrayList();
         synchronized(this.valueUpdates) {
            label299:
            while(true) {
               while(!this.valueUpdates.isEmpty()) {
                  ValueUpdate var28;
                  if (!(var28 = (ValueUpdate)this.valueUpdates.dequeue()).applyClient(this) && var28.failedCount < 50) {
                     ++var28.failedCount;
                     var28.lastTry = var1.currentTime;
                     var33.add(var28);
                  } else if (this.isOnServer() && var28.deligateToClient) {
                     ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var28, this.getSegmentController().isOnServer()));
                  }
               }

               var31 = 0;

               while(true) {
                  if (var31 >= var33.size()) {
                     break label299;
                  }

                  this.valueUpdates.enqueue(var33.get(var31));
                  ++var31;
               }
            }
         }
      }

      if (!this.delayedCummulativeInventoryModsToSend.isEmpty()) {
         Iterator var36 = this.delayedCummulativeInventoryModsToSend.entrySet().iterator();

         while(var36.hasNext()) {
            Entry var23;
            ((Inventory)(var23 = (Entry)var36.next()).getKey()).sendInventoryModification((IntCollection)var23.getValue());
         }

         this.delayedCummulativeInventoryModsToSend.clear();
      }

      this.handleInventoryReceivedNT();
      this.handleFilterInventories(var1);
      this.modules.size();

      while(!this.checkUpdatableQueue.isEmpty()) {
         UsableElementManager var25 = (UsableElementManager)this.checkUpdatableQueue.dequeue();
         ManagerModule var35 = (ManagerModule)this.em2modulesMap.get(var25);

         assert var35 != null : var25;

         if (var35.needsAnyUpdate()) {
            if (!this.updateModules.contains(var35)) {
               this.updateModules.add(var35);
            }
         } else {
            this.updateModules.remove(var35);
         }
      }

      for(int var29 = 0; var29 < this.updateModules.size(); ++var29) {
         ((ManagerModule)this.updateModules.get(var29)).update(var1, var1.currentTime);
      }

      boolean var34;
      for(var34 = false; !this.delayedInventoryRemove.isEmpty(); var34 = true) {
         Vector4i var37 = (Vector4i)this.delayedInventoryRemove.remove(0);
         this.announceInventory(ElementCollection.getIndex(var37.x, var37.y, var37.z), var37.w == 1, (Inventory)null, false);
      }

      while(!this.delayedInventoryAdd.isEmpty()) {
         var26 = (Inventory)this.delayedInventoryAdd.remove(0);
         if (!this.getInventories().containsKey(var26.getParameterIndex())) {
            if (this.getSegmentController().isOnServer()) {
               System.err.println("[SERVER] " + this.getSegmentController() + " ADDING NEW INVENTORY " + var26.getParameter());
            } else {
               System.err.println("[CLIENT] " + this.getSegmentController() + " ADDING NEW INVENTORY " + var26.getParameter());
            }

            this.announceInventory(var26.getParameterIndex(), false, var26, true);
            var34 = true;
         }
      }

      Iterator var38;
      if (var34) {
         this.activeInventories.clear();
         var38 = this.getInventories().values().iterator();

         while(var38.hasNext()) {
            Inventory var24;
            if ((var24 = (Inventory)var38.next()) instanceof ActiveInventory) {
               this.activeInventories.add((ActiveInventory)var24);
            }
         }
      } else if (this.getSegmentController().isOnServer()) {
         var38 = this.activeInventories.iterator();

         while(var38.hasNext()) {
            ActiveInventory var27;
            if ((var27 = (ActiveInventory)var38.next()).isActivated()) {
               var27.updateLocal(var1);
            }
         }
      }

      if (this.flagAnyBlockAdded > 0L && this.getState().getUpdateTime() > this.flagAnyBlockAdded + 500L) {
         for(var31 = 0; var31 < this.changeListenModules.size(); ++var31) {
            ((ElementChangeListenerInterface)this.changeListenModules.get(var31)).onAddedAnyElement();
         }

         this.flagAnyBlockAdded = -1L;
      }

      if (this.flagAnyBlockRemoved > 0L && this.getState().getUpdateTime() > this.flagAnyBlockRemoved + 500L) {
         for(var31 = 0; var31 < this.changeListenModules.size(); ++var31) {
            ((ElementChangeListenerInterface)this.changeListenModules.get(var31)).onRemovedAnyElement();
         }

         this.flagAnyBlockRemoved = -1L;
      }

      if ((var32 = System.currentTimeMillis() - var2) > 10L) {
         System.err.println(this.getSegmentController().getState() + " MANAGER_CONTAINER TOOK TOO LONG: " + var32 + "ms; " + this.getName() + "; " + this);
      }

      this.getState().getDebugTimer().end(this.getSegmentController(), "ManagerContainerUpdate");
   }

   protected void handleShopInventoryReceived(ShopInventory var1) {
   }

   public void sendFullDestinationUpdate() {
      if (this instanceof ActivationManagerInterface) {
         ActivationManagerInterface var1 = (ActivationManagerInterface)this;

         for(int var2 = 0; var2 < var1.getActivation().getCollectionManagers().size(); ++var2) {
            ActivationCollectionManager var3;
            if ((var3 = (ActivationCollectionManager)var1.getActivation().getCollectionManagers().get(var2)).getDestination() != null) {
               DestinationValueUpdate var4;
               (var4 = new DestinationValueUpdate()).setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), var3.getControllerElement().getAbsoluteIndex());
               ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var4, this.getSegmentController().isOnServer()));
            }
         }
      }

      this.getSegmentController().getNetworkObject().additionalBlueprintData.set(true);
   }

   private void handleDockedInventories(Object2ObjectOpenHashMap var1) {
      SegmentPiece var2 = this.getSegmentController().railController.previous.docked;
      SegmentPiece[] var3 = this.getSegmentController().railController.previous.getCurrentRailContactPiece(new SegmentPiece[6]);
      Short2ObjectOpenHashMap var4;
      FastCopyLongOpenHashSet var15;
      if ((var4 = this.getSegmentController().getControlElementMap().getControllingMap().get(var2.getAbsoluteIndex())) != null && (var15 = (FastCopyLongOpenHashSet)var4.get((short)120)) != null && var15.size() > 0) {
         SegmentPiece var5 = new SegmentPiece();
         Iterator var16 = var15.iterator();

         while(true) {
            SegmentPiece var6;
            Inventory var7;
            long var8;
            do {
               do {
                  if (!var16.hasNext()) {
                     return;
                  }

                  var8 = (Long)var16.next();
               } while((var6 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var8, var5)) == null);
            } while((var7 = (Inventory)((InventoryHolder)var6.getSegmentController()).getInventories().get(var6.getAbsoluteIndex())) == null);

            for(int var17 = 0; var17 < var3.length; ++var17) {
               SegmentPiece var9;
               Short2ObjectOpenHashMap var10;
               FastCopyLongOpenHashSet var18;
               if ((var9 = var3[var17]) != null && ElementKeyMap.isRailLoadOrUnload(var9.getType()) && (var10 = var9.getSegmentController().getControlElementMap().getControllingMap().get(var9.getAbsoluteIndex())) != null && (var18 = (FastCopyLongOpenHashSet)var10.get((short)120)) != null) {
                  Iterator var19 = var18.iterator();

                  while(var19.hasNext()) {
                     long var13 = (Long)var19.next();
                     Inventory var11;
                     SegmentPiece var12;
                     if ((var11 = (Inventory)((InventoryHolder)var9.getSegmentController()).getInventories().get(ElementCollection.getPosIndexFrom4(var13))) != null && (var12 = var9.getSegmentController().getSegmentBuffer().getPointUnsave(var13)) != null && var6.isActive() && var12.isActive()) {
                        boolean var20;
                        if (var9.getType() == 1104) {
                           var20 = this.doInventoryTransferFrom((StashInventory)var7, var11, var1);
                           ((ManagerContainer)var7.getInventoryHolder()).sendConnected(var7.getParameterIndex(), var20, (short)405);
                        } else if (var9.getType() == 1105 && var2.getSegmentController().isAllowedToTakeItemsByRail(var9.getSegmentController())) {
                           var20 = this.doInventoryTransferFrom((StashInventory)var11, var7, var1);
                           ((ManagerContainer)var11.getInventoryHolder()).sendConnected(var11.getParameterIndex(), var20, (short)405);
                        }
                     }
                  }
               }
            }
         }
      }
   }

   private void handleFilterInventories(Timer var1) {
      if (this.isOnServer()) {
         long var2;
         if ((var2 = this.segmentController.getState().getController().getServerRunningTime() / 10000L) > this.lastInventoryFilterStep) {
            new SegmentPiece();
            SegmentPiece var22 = new SegmentPiece();
            new Vector3i();
            Object2ObjectOpenHashMap var4 = new Object2ObjectOpenHashMap();
            if (this.getSegmentController().railController.isDocked()) {
               this.handleDockedInventories(var4);
            }

            boolean var5 = false;
            Iterator var6 = this.filterInventories.iterator();

            while(true) {
               Inventory var7;
               long var8;
               SegmentPiece var10;
               do {
                  do {
                     do {
                        do {
                           if (!var6.hasNext()) {
                              var6 = var4.entrySet().iterator();

                              while(var6.hasNext()) {
                                 Entry var23;
                                 ((Inventory)(var23 = (Entry)var6.next()).getKey()).sendInventoryModification((IntCollection)var23.getValue());
                              }

                              this.lastInventoryFilterStep = var2;
                              return;
                           }

                           var8 = (Long)var6.next();
                        } while((var7 = this.getInventory(var8)) == null);
                     } while(var7.getFilter().filter.size() <= 0);
                  } while((var10 = var7.getBlockIfPossible()) == null);
               } while(!var10.isActive());

               assert var7 instanceof StashInventory;

               var7.isAlmostFull();
               boolean var24 = var7.isEmpty();
               StashInventory var11 = (StashInventory)var7;
               ShortArrayList var12 = ElementKeyMap.inventoryTypes;
               Short2ObjectOpenHashMap var13;
               if ((var13 = this.getSegmentController().getControlElementMap().getControllingMap().get(var8)) != null) {
                  Iterator var25 = var12.iterator();

                  label89:
                  while(true) {
                     short var14;
                     LongOpenHashSet var26;
                     do {
                        if (!var25.hasNext()) {
                           break label89;
                        }

                        var14 = (Short)var25.next();
                     } while((var26 = (LongOpenHashSet)var13.get(var14)) == null);

                     Iterator var27 = var26.iterator();

                     while(var27.hasNext()) {
                        long var16 = (Long)var27.next();
                        SegmentPiece var15;
                        Inventory var29;
                        if ((var15 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var16, var22)) != null && (var29 = ((InventoryHolder)var15.getSegmentController()).getInventory(var15.getAbsoluteIndex())) != null) {
                           long var18 = System.currentTimeMillis();
                           var5 = this.doInventoryTransferFrom(var11, var29, var4);
                           long var20;
                           if ((var20 = System.currentTimeMillis() - var18) > 50L) {
                              System.err.println("[SERVER] " + this.getSegmentController() + "; Inventory transfer pull took: " + var20 + "ms; ");
                           }
                        }
                     }
                  }
               }

               var7.isAlmostFull();
               boolean var28 = var7.isEmpty();
               this.sendConnected(var8, var5, (short)405);
               if (var24 && !var28) {
                  this.sendConnected(var8, true, (short)409);
               }
            }
         }
      }
   }

   private boolean doInventoryTransferFrom(StashInventory var1, Inventory var2, Object2ObjectOpenHashMap var3) {
      IntOpenHashSet var4 = (IntOpenHashSet)var3.get(var1);
      TypeAmountFastMap var5 = var1.getFilter().filter;
      TypeAmountFastMap var10000 = var1.getFilter().fillUpTo;
      if (var4 == null) {
         var4 = new IntOpenHashSet(var5.size());
         var3.put(var1, var4);
      }

      boolean var6 = var2.isEmpty();
      IntOpenHashSet var7;
      if ((var7 = (IntOpenHashSet)var3.get(var2)) == null) {
         var7 = new IntOpenHashSet(var5.size());
         var3.put(var2, var7);
      }

      var5.getTypes().size();
      this.pHandler.set(var1, var2, var3, var7, var4, 0L, 0L, true);
      var5.handleLoop(this.pHandler);
      boolean var8 = var2.isEmpty();
      if (!var6 && var8) {
         this.sendConnected(var2.getParameterIndex(), false, (short)409);
      } else if (var6 && !var8) {
         this.sendConnected(var2.getParameterIndex(), true, (short)409);
      }

      return this.pHandler.transferOk;
   }

   public void sendConnected(long var1, boolean var3, short var4) {
      long var7;
      Short2ObjectOpenHashMap var9;
      FastCopyLongOpenHashSet var10;
      if ((var9 = this.getSegmentController().getControlElementMap().getControllingMap().get(var1)) != null && (var10 = (FastCopyLongOpenHashSet)var9.get(var4)) != null) {
         for(Iterator var11 = var10.iterator(); var11.hasNext(); ((SendableSegmentController)this.getSegmentController()).getBlockActivationBuffer().enqueue(var7)) {
            long var5 = (Long)var11.next();
            if (var3) {
               var7 = ElementCollection.getActivation(var5, true, false);
            } else {
               var7 = ElementCollection.getDeactivation(var5, true, false);
            }
         }
      }

   }

   public void updateToFullNetworkObject(NetworkObject var1) {
      for(int var2 = 0; var2 < this.senderModules.size(); ++var2) {
         ((NTSenderInterface)this.senderModules.get(var2)).updateToFullNT(var1);
      }

      this.getPowerInterface().updateToFullNetworkObject(var1);
      if (this.isOnServer()) {
         ((Short2IntOpenHashMap)this.segmentController.getNetworkObject().relevantBlockCounts.get()).putAll(this.relevantMap);
         this.segmentController.getNetworkObject().relevantBlockCounts.setChanged(true);
      }

   }

   public void updateToNetworkObject(NetworkObject var1) {
      this.getPowerInterface().updateToNetworkObject(var1);
   }

   public ShortOpenHashSet getTypesThatNeedActivation() {
      return this.typesThatNeedActivation;
   }

   public abstract boolean isTargetLocking(SegmentPiece var1);

   public int hashCode() {
      return this.segmentController.getId();
   }

   public boolean equals(Object var1) {
      if (var1 != null && var1 instanceof ManagerContainer) {
         return this.segmentController.getId() == ((ManagerContainer)var1).segmentController.getId();
      } else {
         return false;
      }
   }

   public Long2ObjectOpenHashMap getNamedInventoriesClient() {
      return this.namedInventoriesClient;
   }

   public boolean isNamedInventoriesClientChanged() {
      return this.namedInventoriesClientChanged;
   }

   public void setNamedInventoriesClientChanged(boolean var1) {
      this.namedInventoriesClientChanged = var1;
   }

   public void handleBlueprintWireless(String var1, List var2) {
      for(int var3 = 0; var3 < var2.size(); ++var3) {
         BBWirelessLogicMarker var4 = (BBWirelessLogicMarker)var2.get(var3);
         BlockMetaDataDummy var5;
         if ((var5 = (BlockMetaDataDummy)this.getInitialBlockMetaData().get(var4.fromLocation)) != null && var5 instanceof ActivationDestMetaDataDummy && ((ActivationDestMetaDataDummy)var5).dest != null) {
            String var6 = (new String(var1)).replaceFirst("ENTITY_SPACESTATION", "ENTITY_SHIP").replaceFirst("ENTITY_PLANET", "ENTITY_SHIP").replaceFirst("ENTITY_FLOATINGROCKMANAGED", "ENTITY_SHIP").replaceFirst("ENTITY_FLOATINGROCK", "ENTITY_SHIP");
            ((ActivationDestMetaDataDummy)var5).dest.marking = var6 + var4.marking;
            System.err.println("[SERVER][BLUEPRINT][WIRELESS] set blueprint wireless to own: " + this.getSegmentController() + " -> " + ((ActivationDestMetaDataDummy)var5).dest.marking + "; " + var1 + "; " + var4.marking);
         }
      }

   }

   private SystemTargetContainer checkTargatableSystems() {
      if (this.flagTargetRecalc && this.getState().getUpdateTime() - this.lastTargetRecalc > 5000L) {
         this.targetContainer.initialize(this.targetableSystems);
         this.lastTargetRecalc = this.getState().getUpdateTime();
         this.flagTargetRecalc = false;
      }

      return this.targetContainer;
   }

   public void transformAimingAt(Vector3f var1, Damager var2, SimpleGameObject var3, Random var4, float var5) {
      var1.set((float)((Math.random() - 0.5D) * (double)var5), (float)((Math.random() - 0.5D) * (double)var5), (float)((Math.random() - 0.5D) * (double)var5));
      var1.add(var3.getClientTransform().origin);
      SystemTargetContainer var8;
      if ((var8 = this.checkTargatableSystems()).isEmpty()) {
         if (this.factionBlockPos != Long.MIN_VALUE) {
            this.sPos.set((float)(ElementCollection.getPosX(this.factionBlockPos) - 16), (float)(ElementCollection.getPosY(this.factionBlockPos) - 16), (float)(ElementCollection.getPosZ(this.factionBlockPos) - 16));
            var3.getClientTransform().basis.transform(this.sPos);
            var1.add(this.sPos);
            if (ServerConfig.DEBUG_FSM_STATE.isOn()) {
               System.err.println("[FSMDEBUG][TARGET] TARGETING FACTION BLOCK A of " + this.getSegmentController());
               return;
            }
         } else {
            this.sPos.set(var3.getCenterOfMass(this.tTmp.origin));
            var3.getClientTransform().basis.transform(this.sPos);
            var1.add(this.sPos);
            if (ServerConfig.DEBUG_FSM_STATE.isOn()) {
               System.err.println("[FSMDEBUG][TARGET] TARGETING COM A of " + this.getSegmentController());
               return;
            }
         }
      } else {
         ElementCollection var9;
         if ((var9 = var8.getRandomCollection(var4)) == null) {
            if (this.factionBlockPos != Long.MIN_VALUE) {
               this.sPos.set((float)(ElementCollection.getPosX(this.factionBlockPos) - 16), (float)(ElementCollection.getPosY(this.factionBlockPos) - 16), (float)(ElementCollection.getPosZ(this.factionBlockPos) - 16));
               var3.getClientTransform().basis.transform(this.sPos);
               var1.add(this.sPos);
               if (ServerConfig.DEBUG_FSM_STATE.isOn()) {
                  System.err.println("[FSMDEBUG][TARGET] TARGETING FACTION BLOCK B OF " + this.getSegmentController());
                  return;
               }
            } else {
               this.sPos.set(var3.getCenterOfMass(this.tTmp.origin));
               var3.getClientTransform().basis.transform(this.sPos);
               var1.add(this.sPos);
               if (ServerConfig.DEBUG_FSM_STATE.isOn()) {
                  System.err.println("[FSMDEBUG][TARGET] TARGETING COM B of " + this.getSegmentController());
                  return;
               }
            }
         } else {
            long var6 = var9.getRandomlySelectedFromLastThreadUpdate();
            this.sPos.set((float)(ElementCollection.getPosX(var6) - 16), (float)(ElementCollection.getPosY(var6) - 16), (float)(ElementCollection.getPosZ(var6) - 16));
            var3.getClientTransform().basis.transform(this.sPos);
            var1.add(this.sPos);
            if (ServerConfig.DEBUG_FSM_STATE.isOn()) {
               System.err.println("[FSMDEBUG][TARGET] TARGETING MODULE " + var9.getName() + " of " + this.getSegmentController());
            }
         }
      }

   }

   public void getAimingAtRelative(Vector3f var1, Damager var2, SimpleGameObject var3, Random var4, float var5) {
      var1.set((float)((Math.random() - 0.5D) * (double)var5), (float)((Math.random() - 0.5D) * (double)var5), (float)((Math.random() - 0.5D) * (double)var5));
      SystemTargetContainer var8 = this.checkTargatableSystems();
      if (var4.nextInt(10) != 0 && !var8.isEmpty()) {
         ElementCollection var9;
         if ((var9 = var8.getRandomCollection(var4)) == null) {
            var1.add(var3.getCenterOfMass(this.tTmp.origin));
         } else {
            long var6 = var9.getRandomlySelectedFromLastThreadUpdate();
            this.sPos.set((float)(ElementCollection.getPosX(var6) - 16), (float)(ElementCollection.getPosY(var6) - 16), (float)(ElementCollection.getPosZ(var6) - 16));
            var1.add(this.sPos);
         }
      } else if (this.factionBlockPos != Long.MIN_VALUE) {
         this.sPos.set((float)(ElementCollection.getPosX(this.factionBlockPos) - 16), (float)(ElementCollection.getPosY(this.factionBlockPos) - 16), (float)(ElementCollection.getPosZ(this.factionBlockPos) - 16));
         var1.add(this.sPos);
      } else {
         var1.add(var3.getCenterOfMass(this.tTmp.origin));
      }
   }

   public double getMassFromInventories() {
      return this.getSegmentController().getConfigManager().apply(StatusEffectType.CARGO_WEIGHT, this.volumeTotal * (double)VoidElementManager.VOLUME_MASS_MULT);
   }

   public abstract ModuleStatistics getStatisticsManager();

   public abstract ManagerModuleCollection getCargo();

   public void addDelayedProductionAndFilterClientSet(Vector3i var1, Short2IntOpenHashMap var2, Short2IntOpenHashMap var3, short var4, int var5) {
      ManagerContainer.DelayedInvProdSet var6 = new ManagerContainer.DelayedInvProdSet(var1, var2, var3, var4, var5);
      this.invProdMapDelayed.put(ElementCollection.getIndex(var1), var6);
   }

   public abstract ManagerModuleSingle getRailPickup();

   public abstract ManagerModuleCollection getRailSys();

   public String toString() {
      return "MANAGER[" + this.getSegmentController() + "]";
   }

   public void handleMouseEvent(ControllerStateUnit var1, MouseEvent var2) {
      if (var1.parameter instanceof Vector3i) {
         long var3 = ElementCollection.getIndex((Vector3i)var1.parameter);
         long var5 = this.getSelectedSlot(var1, var3);
         PlayerUsableInterface var9;
         if ((var9 = (PlayerUsableInterface)this.playerUsable.get(var5)) != null) {
            var9.handleMouseEvent(var1, var2);
         }

         long[] var10;
         int var4 = (var10 = PlayerUsableInterface.ALWAYS_SELECTED).length;

         for(int var11 = 0; var11 < var4; ++var11) {
            long var7 = var10[var11];
            PlayerUsableInterface var6;
            if ((var6 = (PlayerUsableInterface)this.playerUsable.get(var7)) != null) {
               var6.handleMouseEvent(var1, var2);
            }
         }
      }

   }

   public boolean getSelectedSlot(ControllerStateUnit var1, Vector3i var2) {
      var1.getParameter(var2);
      SegmentPiece var3;
      if ((var3 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var2, this.tmp)) != null && var3.getType() == 1) {
         SlotAssignment var5 = this.checkShipConfig(var1);
         int var4 = var1.getCurrentShipControllerSlot();
         if (!var5.hasConfigForSlot(var4)) {
            return false;
         } else {
            var2.set(var5.get(var4));
            return true;
         }
      } else {
         return false;
      }
   }

   public long getSelectedSlot(ControllerStateUnit var1, long var2) {
      SegmentPiece var5;
      if ((var5 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var2, this.tmp)) != null && var5.getType() == 1) {
         SlotAssignment var6 = this.checkShipConfig(var1);
         int var4 = var1.getCurrentShipControllerSlot();
         return var6 != null && var6.hasConfigForSlot(var4) ? var6.getAsIndex(var4) : Long.MIN_VALUE;
      } else {
         return Long.MIN_VALUE;
      }
   }

   public SlotAssignment checkShipConfig(ControllerStateInterface var1) {
      return var1.getPlayerState() == null ? null : this.getSegmentController().getSlotAssignment();
   }

   public void addModuleExplosions(ModuleExplosion var1) {
      assert this.isOnServer();

      if (this.isOnServer()) {
         this.moduleExplosions.add(var1);
      }
   }

   public boolean isPowerBatteryAlwaysOn() {
      return false;
   }

   public abstract ManagerModuleCollection getSensor();

   public Vector4f getColorCore() {
      return this.colorCore;
   }

   public long getFactionBlockPos() {
      return this.factionBlockPos;
   }

   public PowerInterface getPowerInterface() {
      return this.powerInterface;
   }

   public abstract MainReactorCollectionManager getMainReactor();

   public abstract StabilizerCollectionManager getStabilizer();

   public abstract List getChambers();

   public abstract ConduitCollectionManager getConduit();

   public LongOpenHashSet getBuildBlocks() {
      return this.buildBlocks;
   }

   public void flagAllCollectionsDirty() {
      Iterator var1 = this.getModules().iterator();

      while(true) {
         while(var1.hasNext()) {
            ManagerModule var2;
            if ((var2 = (ManagerModule)var1.next()).getElementManager() instanceof UsableControllableSingleElementManager) {
               ((UsableControllableSingleElementManager)var2.getElementManager()).getCollection().flagDirty();
            } else if (var2.getElementManager() instanceof UsableControllableElementManager) {
               Iterator var3 = ((UsableControllableElementManager)var2.getElementManager()).getCollectionManagers().iterator();

               while(var3.hasNext()) {
                  ((ControlBlockElementCollectionManager)var3.next()).flagDirty();
               }
            }
         }

         System.err.println("[MANAGER CONTAINER] " + this.getSegmentController() + " FLAGGED ALL MANAGERS DIRTY");
         return;
      }
   }

   public void onFullyLoaded() {
      Iterator var1 = this.modules.iterator();

      while(var1.hasNext()) {
         ((ManagerModule)var1.next()).onFullyLoaded();
      }

   }

   public boolean isUsingPowerReactors() {
      return this.getPowerInterface().isUsingPowerReactors();
   }

   public void removeConsumer(PowerConsumer var1) {
      this.getPowerInterface().removeConsumer(var1);
   }

   public void addConsumer(PowerConsumer var1) {
      this.getPowerInterface().addConsumer(var1);
   }

   public void addPlayerUsable(PlayerUsableInterface var1) {
      if (var1.isAddToPlayerUsable() && var1.getUsableId() > Long.MIN_VALUE) {
         this.playerUsable.put(var1.getUsableId(), var1);
         if (var1 instanceof RevealingActionListener) {
            this.revealingActionListeners.put(var1.getUsableId(), (RevealingActionListener)var1);
         }
      }

   }

   public void removePlayerUsable(PlayerUsableInterface var1) {
      this.playerUsable.remove(var1.getUsableId());
      this.revealingActionListeners.remove(var1.getUsableId());
      if (this.isOnServer()) {
         this.getSegmentController().getSlotAssignment().removeByPosAndSend(var1.getUsableId());
      }

   }

   public ObjectCollection getPlayerUsable() {
      return this.playerUsable.values();
   }

   public PlayerUsableInterface getPlayerUsable(long var1) {
      return (PlayerUsableInterface)this.playerUsable.get(var1);
   }

   public void addUpdatable(ManagerUpdatableInterface var1) {
      assert !this.updatable.contains(var1) : var1 + "; " + this.updatable;

      this.updatable.add(var1);
      Collections.sort(this.updatable, updatableComp);
   }

   public void onRevealingAction() {
      Iterator var1 = this.revealingActionListeners.values().iterator();

      while(var1.hasNext()) {
         ((RevealingActionListener)var1.next()).onRevealingAction();
      }

   }

   public void addTagUsable(TagModuleUsableInterface var1) {
      Iterator var2 = this.tagModules.iterator();

      TagModuleUsableInterface var3;
      do {
         if (!var2.hasNext()) {
            this.tagModules.add(var1);
            return;
         }

         var3 = (TagModuleUsableInterface)var2.next();
      } while(var1 != var3 && (!var3.getTagId().equals(var1.getTagId()) || var3.getDummyInstance().getClass() == var1.getDummyInstance().getClass()));

      throw new RuntimeException("Duplicate TAG ID: " + var3.getTagId() + ": " + var3.getClass().getSimpleName() + " and " + var1.getClass().getSimpleName());
   }

   public ScanAddOn getScanAddOn() {
      return this.scanAddOn;
   }

   public StealthAddOn getStealthAddOn() {
      return this.stealthAddOn;
   }

   public void addEffectSource(ConfigProviderSource var1) {
      this.effectConfigSources.add(var1);
   }

   public void removeEffectSource(ConfigProviderSource var1) {
      this.effectConfigSources.remove(var1);
   }

   public void registerTransientEffetcs(List var1) {
      var1.addAll(this.effectConfigSources);
   }

   public EffectAddOnManager getEffectAddOnManager() {
      return this.effectAddOnManager;
   }

   public void addRechargeSingleModule(RecharchableSingleModule var1) {
      this.rechargableSinfleModulesToAdd.enqueue(var1);
   }

   public static void onClientStartStatic() {
      if (!startedClientStatic) {
         StabilizerCollectionManager.startStaticThread();
         startedClientStatic = true;
      }

   }

   public boolean isRequestedInitalValuesIfNeeded() {
      if (this.isOnServer()) {
         return true;
      } else if (!this.getSegmentController().isInClientRange()) {
         this.notLoadedOnClient = true;
         return false;
      } else {
         if (this.notLoadedOnClient) {
            ServerValueRequestUpdate var1 = new ServerValueRequestUpdate(ServerValueRequestUpdate.Type.ALL);

            assert var1.getType() == ValueUpdate.ValTypes.SERVER_UPDATE_REQUEST;

            var1.setServer(this);
            ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));

            assert !this.getSegmentController().isOnServer();

            this.notLoadedOnClient = false;
         }

         return true;
      }
   }

   public boolean hasActiveReactors() {
      return this.powerInterface.hasActiveReactors();
   }

   public boolean hasAnyReactors() {
      return this.powerInterface.hasAnyReactors();
   }

   public ReactorBoostAddOn getReactorBoostAddOn() {
      return this.reactorBoostAddOn;
   }

   public InterdictionAddOn getInterdictionAddOn() {
      return this.interdictionBoostAddOn;
   }

   public void modifySlavesAndEffects(long var1, long var3) {
      this.getSlavesAndEffects().remove(var1);
      if (var3 != Long.MIN_VALUE) {
         this.getSlavesAndEffects().add(var3);
      }

   }

   public LongSet getSlavesAndEffects() {
      return this.slavesAndEffects;
   }

   public void handleInventoryDetailRequests(NetworkSegmentProvider var1, RemoteLongBuffer var2) {
      ManagerContainer.InventoryDetailRequest var3;
      (var3 = new ManagerContainer.InventoryDetailRequest()).prov = var1;

      for(int var4 = 0; var4 < var2.getReceiveBuffer().size(); ++var4) {
         var3.requested.add(var2.getReceiveBuffer().getLong(var4));
      }

      this.inventoryDetailRequests.enqueue(var3);
   }

   public void handleInventoryDetailAnswer(NetworkSegmentProvider var1, RemoteInventoryFilterBuffer var2) {
      for(int var5 = 0; var5 < var2.getReceiveBuffer().size(); ++var5) {
         InventoryFilter var3 = (InventoryFilter)((RemoteInventoryFilter)var2.getReceiveBuffer().get(var5)).get();
         Inventory var4 = this.getInventory(var3.inventoryId);

         assert var4 != null : var3.inventoryId;

         if (var4 != null) {
            var4.receivedFilterAnswer(var3);
         }
      }

   }

   public void unregisterGraphicsListener() {
      this.graphicsListener = null;
   }

   public void registerGraphicsListener(ElementCollectionDrawer.MContainerDrawJob var1) {
      this.graphicsListener = var1;
   }

   public void flagElementChanged() {
      if (this.graphicsListener != null) {
         this.graphicsListener.flagChanged();
      }

      this.lastChangedElement = this.getState().getUpdateTime();
      this.flagElementCollectionChanged = true;
   }

   public void onPlayerDetachedFromThisOrADock(ManagedUsableSegmentController var1, PlayerState var2, PlayerControllable var3) {
      Iterator var4 = this.playerUsable.values().iterator();

      while(var4.hasNext()) {
         ((PlayerUsableInterface)var4.next()).onPlayerDetachedFromThisOrADock(var1, var2, var3);
      }

   }

   public void triggeredManualMouseEvent(int var1, PlayerUsableInterface var2, MouseEvent var3) {
      ManagerContainer.ManualMouseEvent var4;
      (var4 = new ManagerContainer.ManualMouseEvent()).playerId = var1;
      var4.usableId = var2.getUsableId();
      var4.eventButton = var3.button;
      this.triggeredManualMouseEvent(var4);
   }

   public void triggeredManualMouseEvent(ManagerContainer.ManualMouseEvent var1) {
      this.manualEvents.add(var1);
   }

   public boolean existsExplosion(Damager var1, short var2, long var3) {
      List var6;
      int var7 = (var6 = this.moduleExplosions).size();

      for(int var5 = 0; var5 < var7; ++var5) {
         if (((ModuleExplosion)var6.get(var5)).getModuleBB().isInside((float)ElementCollection.getPosX(var3), (float)ElementCollection.getPosY(var3), (float)ElementCollection.getPosZ(var3))) {
            return true;
         }
      }

      return false;
   }

   public List getModuleExplosions() {
      return this.moduleExplosions;
   }

   public InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3) {
      PlayerUsableInterface var4;
      if ((var4 = this.getPlayerUsable(var1)) != null && var4 instanceof DamageDealer) {
         DamageDealer var5 = (DamageDealer)var4;

         assert var5.getDamageDealerType() == var3;

         return var5.getAttackEffectSet();
      } else {
         return null;
      }
   }

   public MetaWeaponEffectInterface getMetaWeaponEffect(long var1, DamageDealerType var3) {
      PlayerUsableInterface var4;
      if ((var4 = this.getPlayerUsable(var1)) != null && var4 instanceof DamageDealer) {
         DamageDealer var5 = (DamageDealer)var4;

         assert var5.getDamageDealerType() == var3;

         return var5.getMetaWeaponEffect();
      } else {
         return null;
      }
   }

   public void addReceivedBeamLatch(long var1, int var3, long var4) {
      ManagerContainer.ReceivedBeamLatch var6;
      (var6 = new ManagerContainer.ReceivedBeamLatch()).beamId = var1;
      var6.objId = var3;
      var6.blockPos = var4;
      this.receivedBeamLatches.enqueue(var6);
   }

   public List getBeamInterfacesSingle() {
      return this.beamInteracesSingle;
   }

   public List getBeamManagers() {
      return this.beamModules;
   }

   public abstract float getMissileCapacity();

   public abstract float getMissileCapacityMax();

   public abstract float getMissileCapacityTimer();

   public abstract float getMissileCapacityReloadTime();

   public abstract void setMissileCapacity(float var1, float var2, boolean var3);

   public abstract AcidDamageFormula.AcidFormulaType getAcidType(long var1);

   public void queueOnEffectChange(EffectChangeHanlder var1) {
      this.effectChangeQueue.enqueue(var1);
   }

   public void flagUpdatableCheckFor(UsableElementManager var1) {
      this.checkUpdatableQueue.enqueue(var1);
   }

   public float getIntegrityUpdateDelay() {
      return this.integrityUpdateDelay;
   }

   public void setIntegrityUpdateDelay(float var1) {
      this.integrityUpdateDelay = var1;
   }

   public float getRepairDelay() {
      return this.repairDelay;
   }

   public void onDockingChanged(boolean var1) {
      Iterator var2 = this.railDockingListeners.iterator();

      while(var2.hasNext()) {
         ((RailDockingListener)var2.next()).dockingChanged(this.segmentController, var1);
      }

   }

   public float getSelectedWeaponZoom(PlayerState var1) {
      Iterator var5 = var1.getControllerState().getUnits().iterator();

      while(var5.hasNext()) {
         ControllerStateUnit var2;
         if ((var2 = (ControllerStateUnit)var5.next()).playerControllable == this.getSegmentController() && var2.parameter instanceof Vector3i) {
            long var3 = this.getSelectedSlot(var2, ElementCollection.getIndex((Vector3i)var2.parameter));
            PlayerUsableInterface var6;
            if ((var6 = this.getPlayerUsable(var3)) instanceof ZoomableUsableModule) {
               return ((ZoomableUsableModule)var6).getPossibleZoom();
            }
         }
      }

      return -1.0F;
   }

   public void sendAllFireModes() {
      Iterator var1 = this.weapons.iterator();

      while(true) {
         WeaponElementManagerInterface var2;
         do {
            if (!var1.hasNext()) {
               return;
            }
         } while(!((var2 = (WeaponElementManagerInterface)var1.next()) instanceof UsableControllableFireingElementManager));

         Iterator var4 = ((UsableControllableFireingElementManager)var2).getCollectionManagers().iterator();

         while(var4.hasNext()) {
            ControlBlockElementCollectionManager var3;
            if ((var3 = (ControlBlockElementCollectionManager)var4.next()) instanceof FocusableUsableModule && ((FocusableUsableModule)var3).getFireMode() != FocusableUsableModule.FireMode.getDefault(var3.getClass())) {
               ((FocusableUsableModule)var3).sendFireMode();
            }
         }
      }
   }

   public ObjectArrayList getWeapons() {
      return this.weapons;
   }

   public void resetIntegrityDelay() {
      this.integrityUpdateDelay = 0.0F;
      this.sendDelayUpdate();
   }

   private void sendDelayUpdate() {
      this.segmentController.getNetworkObject().blockDelayTimers.add(this.integrityUpdateDelay);
      this.segmentController.getNetworkObject().blockDelayTimers.add(this.repairDelay);
   }

   public void resetRepairDelay() {
      this.repairDelay = 0.0F;
      this.sendDelayUpdate();
   }

   public static class ReceivedBeamLatch {
      public long beamId;
      public int objId;
      public long blockPos;
   }

   public static class ManualMouseEvent implements SerialializationInterface {
      public boolean sent;
      long usableId;
      int eventButton;
      int playerId;
      private long firstExecution;

      public void send(NetworkSegmentProvider var1) {
         var1.manualMouseEventBuffer.add(new RemoteManualMouseEvent(this, var1.isOnServer()));
      }

      public boolean execute(ManagerContainer var1, Timer var2) {
         Sendable var3;
         if (!((var3 = (Sendable)var1.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(this.playerId)) instanceof PlayerState)) {
            System.err.println("[MANUAL EVENT] " + this + " " + var1 + " PLAYER STATE NULL");
         } else {
            PlayerState var6 = (PlayerState)var3;
            ManagerContainer.ManualMouseEvent.ControllerStateManualUnit var4;
            (var4 = new ManagerContainer.ManualMouseEvent.ControllerStateManualUnit()).playerState = var6;
            var4.playerControllable = (PlayerControllable)var1.getSegmentController();
            var4.parameter = new Vector3i(Ship.core);
            PlayerUsableInterface var5;
            if ((var5 = (PlayerUsableInterface)var1.playerUsable.get(this.usableId)) != null) {
               if (var2.currentTime > this.firstExecution + 500L && var6.isMouseButtonDown(MouseButton.LEFT.button) || var6.isMouseButtonDown(MouseButton.RIGHT.button)) {
                  var5.handleControl(var4, var2);
               }

               if (this.firstExecution == 0L) {
                  this.firstExecution = var2.currentTime;
               }
            } else {
               System.err.println("[MANUAL EVENT] " + this + " " + var1 + " USABLE NULL: " + this.usableId);
            }

            if (var6.isMouseButtonDown(MouseButton.LEFT.button) || var6.isMouseButtonDown(MouseButton.RIGHT.button)) {
               return false;
            }
         }

         return true;
      }

      public void serialize(DataOutput var1, boolean var2) throws IOException {
         var1.writeLong(this.usableId);
         var1.writeByte(this.eventButton);
         var1.writeInt(this.playerId);
      }

      public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
         this.usableId = var1.readLong();
         this.eventButton = var1.readByte();
         this.playerId = var1.readInt();
      }

      public String toString() {
         return "ManualMouseEvent [usableId=" + this.usableId + ", eventButton=" + this.eventButton + ", playerId=" + this.playerId + "]";
      }

      public class ControllerStateManualUnit extends ControllerStateUnit {
         public boolean isMouseButtonDown(int var1) {
            return var1 == ManualMouseEvent.this.eventButton;
         }

         public boolean wasMouseButtonDownServer(int var1) {
            return false;
         }

         public boolean isFlightControllerActive() {
            return true;
         }
      }
   }

   class InventoryDetailRequest {
      NetworkSegmentProvider prov;
      private final LongArrayList requested;

      private InventoryDetailRequest() {
         this.requested = new LongArrayList();
      }

      // $FF: synthetic method
      InventoryDetailRequest(Object var2) {
         this();
      }
   }

   class DelayedInvProdSet {
      Short2IntOpenHashMap inventoryFilter;
      short inventoryProduction;
      int inventoryProductionLimit;
      Short2IntOpenHashMap inventoryFillUpFilters;

      public DelayedInvProdSet(Vector3i var2, Short2IntOpenHashMap var3, Short2IntOpenHashMap var4, short var5, int var6) {
         this.inventoryFilter = var3;
         this.inventoryProduction = var5;
         this.inventoryFillUpFilters = var4;
         this.inventoryProductionLimit = var6;
      }
   }

   public class SingleControllerStateUnit implements ControllerStateInterface {
      public long block;
      Vector3f tmpForw;
      Vector3f tmpUp;
      Vector3f tmpRight;
      private SegmentPiece tmp;

      private SingleControllerStateUnit() {
         this.tmpForw = new Vector3f();
         this.tmpUp = new Vector3f();
         this.tmpRight = new Vector3f();
         this.tmp = new SegmentPiece();
      }

      public Vector3i getParameter(Vector3i var1) {
         return ElementCollection.getPosFromIndex(this.block, var1);
      }

      public PlayerState getPlayerState() {
         return null;
      }

      public Vector3f getForward(Vector3f var1) {
         return GlUtil.getForwardVector(var1, (Transform)ManagerContainer.this.getSegmentController().getWorldTransform());
      }

      public Vector3f getUp(Vector3f var1) {
         return GlUtil.getUpVector(var1, (Transform)ManagerContainer.this.getSegmentController().getWorldTransform());
      }

      public Vector3f getRight(Vector3f var1) {
         return GlUtil.getRightVector(var1, (Transform)ManagerContainer.this.getSegmentController().getWorldTransform());
      }

      public boolean isMouseButtonDown(int var1) {
         return true;
      }

      public boolean wasMouseButtonDownServer(int var1) {
         return false;
      }

      public boolean isUnitInPlayerSector() {
         return true;
      }

      public Vector3i getControlledFrom(Vector3i var1) {
         this.getParameter(var1);
         return var1;
      }

      public boolean isFlightControllerActive() {
         return true;
      }

      public int getCurrentShipControllerSlot() {
         return 0;
      }

      public boolean isKeyDown(KeyboardMappings var1) {
         return true;
      }

      public void handleJoystickDir(Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4) {
      }

      public SimpleTransformableSendableObject getAquiredTarget() {
         return null;
      }

      public boolean getShootingDir(SegmentController var1, ShootContainer var2, float var3, float var4, Vector3i var5, boolean var6, boolean var7) {
         System.currentTimeMillis();
         var2.shootingDirTemp.set(UsableControllableElementManager.getShootingDir(var1, this.getForward(this.tmpForw), this.getUp(this.tmpUp), this.getRight(this.tmpRight), var2.shootingForwardTemp, var2.shootingUpTemp, var2.shootingRightTemp, true, var5, this.tmp));
         this.tmp.reset();
         var2.shootingDirStraightTemp.set(UsableControllableElementManager.getShootingDir(var1, GlUtil.getForwardVector(this.tmpForw, (Transform)var1.getWorldTransform()), GlUtil.getUpVector(this.tmpUp, (Transform)var1.getWorldTransform()), GlUtil.getRightVector(this.tmpRight, (Transform)var1.getWorldTransform()), var2.shootingStraightForwardTemp, var2.shootingStraightUpTemp, var2.shootingStraightRightTemp, true, var5, this.tmp));
         this.tmp.reset();
         return true;
      }

      public boolean isSelected(SegmentPiece var1, Vector3i var2) {
         return this.block == var1.getAbsoluteIndex();
      }

      public boolean isAISelected(SegmentPiece var1, Vector3i var2, int var3, int var4, ElementCollectionManager var5) {
         return true;
      }

      public boolean canFlyShip() {
         return true;
      }

      public boolean canRotateShip() {
         return true;
      }

      public float getBeamTimeout() {
         return 3.0F;
      }

      public boolean isSelected(PlayerUsableInterface var1, ManagerContainer var2) {
         return false;
      }

      public boolean isPrimaryShootButtonDown() {
         return true;
      }

      public boolean isSecondaryShootButtonDown() {
         return false;
      }

      public boolean canFocusWeapon() {
         return false;
      }

      public boolean clickedOnce(int var1) {
         return var1 == 0;
      }

      public void addCockpitOffset(Vector3f var1) {
      }

      // $FF: synthetic method
      SingleControllerStateUnit(Object var2) {
         this();
      }
   }

   class PullInvHanlder implements TypeAmountLoopHandle {
      private StashInventory pullTo;
      private Inventory pullFrom;
      private IntOpenHashSet changedSlotsOthers;
      private IntOpenHashSet ownChanged;
      private long nanoTotalBatch;
      private long nanoTotalInc;
      private boolean transferOk;

      private PullInvHanlder() {
         this.nanoTotalBatch = 0L;
         this.nanoTotalInc = 0L;
         this.transferOk = true;
      }

      public void set(StashInventory var1, Inventory var2, Object2ObjectOpenHashMap var3, IntOpenHashSet var4, IntOpenHashSet var5, long var6, long var8, boolean var10) {
         this.pullTo = var1;
         this.pullFrom = var2;
         this.changedSlotsOthers = var4;
         this.ownChanged = var5;
         this.nanoTotalBatch = var6;
         this.nanoTotalInc = var8;
         this.transferOk = var10;
      }

      public void handle(short var1, int var2) {
         int var5;
         int var9;
         if (var1 > 0) {
            int var8 = this.pullFrom.getOverallQuantity(var1);
            if (var2 > var8) {
               var2 = var8;
            }

            if ((var9 = this.pullTo.getFilter().fillUpTo.get(var1)) > 0) {
               if ((var5 = this.pullTo.getOverallQuantity(var1)) >= var9) {
                  return;
               }

               var2 = Math.min(var9 - var5, var2);
            }

            if (var2 > 0) {
               if (this.pullTo.canPutIn(var1, var2)) {
                  long var10 = System.nanoTime();
                  this.pullFrom.decreaseBatch(var1, var2, this.changedSlotsOthers);
                  this.nanoTotalBatch += System.nanoTime() - var10;
                  var10 = System.nanoTime();
                  int var7 = this.pullTo.incExistingOrNextFreeSlot(var1, var2);
                  this.nanoTotalInc += System.nanoTime() - var10;
                  this.ownChanged.add(var7);
                  return;
               }

               this.transferOk = false;
            }

         } else {
            if (var1 < 0) {
               short var4 = -1;
               short var3;
               if (var1 > -256) {
                  var3 = var1;
               } else {
                  var3 = (short)(-(Math.abs(var1) / 256));
                  var4 = (short)(Math.abs(var1) % 256 + var3);
               }

               if ((var9 = this.pullFrom.getFirstMetaItemByType(var3, var4)) >= 0 && !this.pullTo.isOverCapacity(((MetaObjectState)ManagerContainer.this.getState()).getMetaObjectManager().getVolume(this.pullFrom.getMeta(var9)))) {
                  var5 = this.pullFrom.getMeta(var9);
                  this.pullFrom.put(var9, (short)0, 0, -1);
                  this.changedSlotsOthers.add(var9);
                  int var6 = this.pullTo.putNextFreeSlot(var3, 1, var5);
                  this.ownChanged.add(var6);
               }
            }

         }
      }

      // $FF: synthetic method
      PullInvHanlder(Object var2) {
         this();
      }
   }

   class BlockAct {
      UsableControllableElementManager module;
      long block;
      long timeStarted;
      public long trigger;

      private BlockAct() {
         this.trigger = Long.MIN_VALUE;
      }

      // $FF: synthetic method
      BlockAct(Object var2) {
         this();
      }
   }
}
