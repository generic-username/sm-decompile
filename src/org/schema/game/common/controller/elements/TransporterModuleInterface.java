package org.schema.game.common.controller.elements;

public interface TransporterModuleInterface {
   ManagerModuleCollection getTransporter();
}
