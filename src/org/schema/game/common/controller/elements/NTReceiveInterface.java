package org.schema.game.common.controller.elements;

import org.schema.schine.network.objects.NetworkObject;

public interface NTReceiveInterface {
   void updateFromNT(NetworkObject var1);
}
