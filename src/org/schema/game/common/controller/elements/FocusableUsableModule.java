package org.schema.game.common.controller.elements;

import org.schema.game.common.controller.elements.beam.harvest.SalvageBeamCollectionManager;
import org.schema.game.common.controller.elements.beam.repair.RepairElementManager;
import org.schema.game.common.controller.elements.beam.tractorbeam.TractorBeamCollectionManager;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;

public interface FocusableUsableModule {
   FocusableUsableModule.FireMode getFireMode();

   boolean isInFocusMode();

   void setFireMode(FocusableUsableModule.FireMode var1);

   void sendFireMode();

   public static enum FireMode {
      UNFOCUSED(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_FOCUSABLEUSABLEMODULE_0;
         }
      }),
      FOCUSED(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_FOCUSABLEUSABLEMODULE_1;
         }
      }),
      VOLLEY(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_FOCUSABLEUSABLEMODULE_2;
         }
      });

      private final Translatable nm;

      private FireMode(Translatable var3) {
         this.nm = var3;
      }

      public static FocusableUsableModule.FireMode getDefault(Class var0) {
         return var0 != SalvageBeamCollectionManager.class && var0 != TractorBeamCollectionManager.class && var0 != RepairElementManager.class ? FOCUSED : UNFOCUSED;
      }

      public final String getName() {
         return this.nm.getName(this);
      }
   }
}
