package org.schema.game.common.controller.elements.config;

public class ReactorDualConfigElement {
   public static int getIndex(boolean var0) {
      return var0 ? 0 : 1;
   }
}
