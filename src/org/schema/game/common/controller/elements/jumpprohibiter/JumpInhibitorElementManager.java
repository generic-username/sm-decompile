package org.schema.game.common.controller.elements.jumpprohibiter;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import java.io.IOException;
import java.util.Iterator;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.BlockActivationListenerInterface;
import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerReloadInterface;
import org.schema.game.common.controller.elements.NTReceiveInterface;
import org.schema.game.common.controller.elements.NTSenderInterface;
import org.schema.game.common.controller.elements.TagModuleUsableInterface;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.controller.elements.UsableControllableFireingElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.InputState;
import org.schema.schine.network.objects.NetworkObject;

public class JumpInhibitorElementManager extends UsableControllableFireingElementManager implements BlockActivationListenerInterface, ManagerReloadInterface, NTReceiveInterface, NTSenderInterface, TagModuleUsableInterface {
   public static final String TAG_ID = "JP";
   @ConfigurationElement(
      name = "DischargePerSecond"
   )
   public static float DISCHARGE_PER_SECOND = 10.0F;
   @ConfigurationElement(
      name = "DischargePerSecondPerBlock"
   )
   public static float DISCHARGE_PER_SECOND_PER_BLOCK = 10.0F;
   @ConfigurationElement(
      name = "PowerConsumptionPerSecond"
   )
   public static float POWER_CONSUMPTION_PER_SECOND = 10.0F;
   @ConfigurationElement(
      name = "PowerConsumptionPerSecondPerBlock"
   )
   public static float POWER_CONSUMPTION_PER_SECOND_PER_BLOCK = 10.0F;
   @ConfigurationElement(
      name = "DischargeFriendlyShips"
   )
   public static boolean DISCHARGE_FRIENDLY_SHIPS = true;
   @ConfigurationElement(
      name = "DischargeSelf"
   )
   public static boolean DISCHARGE_SELF = true;
   @ConfigurationElement(
      name = "ReactorPowerConsumptionResting"
   )
   public static float REACTOR_POWER_CONSUMPTION_RESTING = 10.0F;
   @ConfigurationElement(
      name = "ReactorPowerConsumptionCharging"
   )
   public static float REACTOR_POWER_CONSUMPTION_CHARGING = 10.0F;
   public static boolean debug = false;
   private Vector3i controlledFromOrig = new Vector3i();
   private Vector3i controlledFrom = new Vector3i();
   private long lastAct;

   public JumpInhibitorElementManager(SegmentController var1) {
      super((short)681, (short)682, var1);
   }

   public String getTagId() {
      return "JP";
   }

   public ControllerManagerGUI getGUIUnitValues(JumpInhibitorUnit var1, JumpInhibitorCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPPROHIBITER_JUMPINHIBITORELEMENTMANAGER_2, var1);
   }

   protected String getTag() {
      return "jumpinhibitor";
   }

   public JumpInhibitorCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new JumpInhibitorCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPPROHIBITER_JUMPINHIBITORELEMENTMANAGER_3;
   }

   protected void playSound(JumpInhibitorUnit var1, Transform var2) {
      ((GameClientController)this.getState().getController()).queueTransformableAudio("0022_spaceship user - laser gun single fire small", var2, 0.99F);
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
      System.currentTimeMillis();
      if (!var1.isFlightControllerActive()) {
         if (debug) {
            System.err.println("NOT ACTIVE");
         }

      } else if (this.getCollectionManagers().isEmpty()) {
         if (debug) {
            System.err.println("NO WEAPONS");
         }

      } else {
         try {
            if (!this.convertDeligateControls(var1, this.controlledFromOrig, this.controlledFrom)) {
               if (debug) {
                  System.err.println("NO SLOT");
               }

               return;
            }
         } catch (IOException var5) {
            var5.printStackTrace();
            return;
         }

         this.getPowerManager().sendNoPowerHitEffectIfNeeded();
         if (debug) {
            System.err.println("FIREING CONTROLLERS: " + this.getState() + ", " + this.getCollectionManagers().size() + " FROM: " + this.controlledFrom);
         }

         for(int var6 = 0; var6 < this.getCollectionManagers().size(); ++var6) {
            JumpInhibitorCollectionManager var3 = (JumpInhibitorCollectionManager)this.getCollectionManagers().get(var6);
            if (var1.isSelected(var3.getControllerElement(), this.controlledFrom)) {
               boolean var4 = this.controlledFromOrig.equals(this.controlledFrom) | this.getControlElementMap().isControlling(this.controlledFromOrig, var3.getControllerPos(), this.controllerId);
               if (debug) {
                  System.err.println("Controlling " + var4 + " " + this.getState());
               }

               if (var4 && var3.allowedOnServerLimit() && this.getSegmentController().isOnServer() && System.currentTimeMillis() - this.lastAct > 300L) {
                  var3.activeInhibitor = !var3.activeInhibitor;
                  var3.sendActiveUpdate();
                  this.lastAct = System.currentTimeMillis();
               }
            }
         }

         if (this.getCollectionManagers().isEmpty() && this.clientIsOwnShip()) {
            ((GameClientState)this.getState()).getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPPROHIBITER_JUMPINHIBITORELEMENTMANAGER_1, 0.0F);
         }

      }
   }

   public int onActivate(SegmentPiece var1, boolean var2, boolean var3) {
      return var3 ? 1 : 0;
   }

   public void updateActivationTypes(ShortOpenHashSet var1) {
      var1.add((short)16);
   }

   public void updateFromNT(NetworkObject var1) {
   }

   public String getReloadStatus(long var1) {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPPROHIBITER_JUMPINHIBITORELEMENTMANAGER_4;
   }

   public void updateToFullNT(NetworkObject var1) {
      this.getSegmentController().isOnServer();
   }

   public void drawReloads(Vector3i var1, Vector3i var2, long var3) {
      Iterator var5 = this.getCollectionManagers().iterator();

      while(var5.hasNext()) {
         if (((JumpInhibitorCollectionManager)var5.next()).activeInhibitor) {
            UsableControllableElementManager.drawReload((InputState)this.getState(), var1, var2, reloadColor, false, 1.0F);
         }
      }

   }

   public BlockMetaDataDummy getDummyInstance() {
      return new JumpInhibitorMetaDataDummy();
   }
}
