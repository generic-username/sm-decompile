package org.schema.game.common.controller.elements.jumpprohibiter;

import java.util.Iterator;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.controller.elements.jumpdrive.JumpDriveCollectionManager;
import org.schema.game.common.controller.elements.power.PowerAddOn;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.network.objects.remote.RemoteValueUpdate;
import org.schema.game.network.objects.valueUpdate.JumpInhibitorValueUpdate;
import org.schema.game.network.objects.valueUpdate.NTValueUpdateInterface;
import org.schema.game.server.data.FactionState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.input.InputType;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.tag.Tag;

public class JumpInhibitorCollectionManager extends ControlBlockElementCollectionManager implements PlayerUsableInterface {
   public boolean activeInhibitor;
   private double accum = 0.0D;

   public JumpInhibitorCollectionManager(SegmentPiece var1, SegmentController var2, JumpInhibitorElementManager var3) {
      super(var1, (short)682, var2, var3);
   }

   protected void applyMetaData(BlockMetaDataDummy var1) {
      this.activeInhibitor = ((JumpInhibitorMetaDataDummy)var1).active;
   }

   public void update(Timer var1) {
      super.update(var1);
      if (this.activeInhibitor) {
         if (this.getTotalSize() == 0) {
            if (this.getSegmentController().isOnServer() && this.getSegmentController().isFullyLoadedWithDock()) {
               this.activeInhibitor = false;
               this.sendActiveUpdate();
               return;
            }
         } else {
            PowerAddOn var2 = ((PowerManagerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getPowerAddOn();
            float var3 = this.getPowerConsumptionPerSec() * var1.getDelta();
            if (var2.consumePowerInstantly((double)var3)) {
               if (this.getSegmentController().isOnServer()) {
                  this.dischargeOthers(var1);
                  return;
               }
            } else if (this.getSegmentController().isOnServer() && this.getSegmentController().isFullyLoadedWithDock()) {
               this.activeInhibitor = false;
               this.sendActiveUpdate();
            }
         }
      }

   }

   private void dischargeOthers(Timer var1) {
      assert this.getSegmentController().isOnServer();

      this.accum += (double)var1.getDelta();
      if (this.accum >= 1.0D) {
         long var2;
         double var4 = (double)((float)(var2 = (long)this.accum) * this.getChargeAddedPerSec());
         Iterator var10 = this.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

         while(true) {
            Ship var11;
            do {
               Sendable var6;
               do {
                  if (!var10.hasNext()) {
                     this.accum -= (double)var2;
                     return;
                  }
               } while(!((var6 = (Sendable)var10.next()) instanceof Ship));

               var11 = (Ship)var6;
            } while(!this.isDischarging(var11));

            ManagerModuleCollection var7 = var11.getManagerContainer().getJumpDrive();
            boolean var8 = false;
            Iterator var12 = var7.getCollectionManagers().iterator();

            while(var12.hasNext()) {
               JumpDriveCollectionManager var9;
               if ((var9 = (JumpDriveCollectionManager)var12.next()).getCharge() > 0.0F) {
                  var9.discharge(var4);
                  var9.sendChargeUpdate();
                  var8 = true;
               }
            }

            if (var8) {
               var11.sendControllingPlayersServerMessage(new Object[]{48, this.getSegmentController().toNiceString()}, 3);
            }
         }
      }
   }

   private boolean isDischarging(Ship var1) {
      if (!JumpInhibitorElementManager.DISCHARGE_SELF && this.getSegmentController().railController.isInAnyRailRelationWith((SegmentController)var1)) {
         return false;
      } else {
         if (!var1.railController.isDockedAndExecuted() && var1.isNeighbor(var1.getSectorId(), this.getSegmentController().getSectorId())) {
            if (JumpInhibitorElementManager.DISCHARGE_FRIENDLY_SHIPS) {
               return true;
            }

            if (((FactionState)this.getState()).getFactionManager().getRelation(this.getSegmentController(), var1) != FactionRelation.RType.FRIEND) {
               return true;
            }
         }

         return false;
      }
   }

   public void sendActiveUpdate() {
      JumpInhibitorValueUpdate var1;
      (var1 = new JumpInhibitorValueUpdate()).setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), this.getControllerElement().getAbsoluteIndex());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   protected Tag toTagStructurePriv() {
      return new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.activeInhibitor ? 1 : 0)));
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return JumpInhibitorUnit.class;
   }

   public boolean isPlayerUsable() {
      return !this.isUsingPowerReactors();
   }

   public boolean isControllerConnectedTo(long var1, short var3) {
      return true;
   }

   public boolean needsUpdate() {
      return true;
   }

   public JumpInhibitorUnit getInstance() {
      return new JumpInhibitorUnit();
   }

   protected void onChangedCollection() {
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
      }

      super.onChangedCollection();
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      float var1 = 0.0F;

      for(int var2 = 0; var2 < this.getElementCollections().size(); ++var2) {
         var1 += ((JumpInhibitorUnit)this.getElementCollections().get(var2)).getPowerConsumption();
      }

      return new GUIKeyValueEntry[]{new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPPROHIBITER_JUMPINHIBITORCOLLECTIONMANAGER_1, this.getChargeAddedPerSec()), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPPROHIBITER_JUMPINHIBITORCOLLECTIONMANAGER_2, var1)};
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPPROHIBITER_JUMPINHIBITORCOLLECTIONMANAGER_3;
   }

   public float getChargeAddedPerSec() {
      return JumpInhibitorElementManager.DISCHARGE_PER_SECOND + JumpInhibitorElementManager.DISCHARGE_PER_SECOND_PER_BLOCK * (float)this.getTotalSize();
   }

   public float getPowerConsumptionPerSec() {
      return JumpInhibitorElementManager.POWER_CONSUMPTION_PER_SECOND + JumpInhibitorElementManager.POWER_CONSUMPTION_PER_SECOND_PER_BLOCK * (float)this.getTotalSize();
   }

   public void handleControl(ControllerStateInterface var1, Timer var2) {
      if (var1.clickedOnce(0)) {
         ((JumpInhibitorElementManager)this.getElementManager()).handle(var1, var2);
      }

   }

   public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
      var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPPROHIBITER_JUMPINHIBITORCOLLECTIONMANAGER_4, var3, ContextFilter.IMPORTANT);
   }
}
