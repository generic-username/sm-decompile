package org.schema.game.common.controller.elements.factory;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.common.language.Lng;

public class FactoryUnit extends ElementCollection {
   private int capability;

   public void refreshFactoryCapabilities(FactoryCollectionManager var1) {
      this.capability = this.getNeighboringCollection().size();
      var1.addCapability(this.capability);
   }

   public String toString() {
      return "FactoryUnit";
   }

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ControllerManagerGUI.create(var1, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_FACTORY_FACTORYUNIT_0, this, new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_FACTORY_FACTORYUNIT_1, this.capability));
   }
}
