package org.schema.game.common.controller.elements.factory.factories;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.factory.FactoryCollectionManager;
import org.schema.game.common.controller.elements.factory.FactoryElementManager;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FixedRecipe;
import org.schema.game.common.data.player.inventory.Inventory;

public class FactoryRefineryElementManager extends FactoryElementManager {
   public FactoryRefineryElementManager(SegmentController var1) {
      super(var1, (short)213, (short)212);
   }

   public FixedRecipe getCurrentRecipe(Inventory var1, FactoryCollectionManager var2) {
      return ElementKeyMap.capsuleRecipe;
   }
}
