package org.schema.game.common.controller.elements.factory.factories;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.factory.FactoryElementManager;

public class FactoryPowerModuleElementManager extends FactoryElementManager {
   public FactoryPowerModuleElementManager(SegmentController var1) {
      super(var1, (short)211, (short)212);
   }
}
