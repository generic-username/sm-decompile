package org.schema.game.common.controller.elements;

import org.schema.game.common.controller.elements.explosive.ExplosiveCollectionManager;
import org.schema.game.common.controller.elements.explosive.ExplosiveElementManager;

public interface ExplosiveManagerContainerInterface {
   ExplosiveElementManager getExplosiveElementManager();

   ExplosiveCollectionManager getExplosiveCollectionManager();

   ManagerModuleSingle getExplosive();
}
