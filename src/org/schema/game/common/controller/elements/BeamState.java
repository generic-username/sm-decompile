package org.schema.game.common.controller.elements;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ArmorValue;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.HitType;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.beam.AbstractBeamHandler;
import org.schema.game.common.data.element.beam.BeamReloadCallback;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.graphicsengine.core.MouseButton;

public class BeamState {
   public final Vector3f hitPointCache = new Vector3f();
   public final SegmentPiece p1 = new SegmentPiece();
   public final SegmentPiece p2 = new SegmentPiece();
   public final Vector3f relativePos = new Vector3f();
   public final long identifyerSig;
   public final Vector3f from = new Vector3f();
   public final Vector3f to = new Vector3f();
   public final Vector4f color = new Vector4f();
   private final AbstractBeamHandler handler;
   public Vector3f hitPoint;
   public float timeRunningSinceLastUpdate = 0.0F;
   public long lastCheck = -1L;
   public float timeSpent;
   public SegmentPiece segmentHit;
   public SegmentPiece currentHit;
   public float hitOneSegment;
   public float hitBlockTime;
   public SegmentController cachedLastSegment;
   public Vector3b cachedLastPos = new Vector3b(-1.0F, -1.0F, -1.0F);
   public float camDistStart;
   public float camDistEnd;
   public float size = 1.0F;
   public boolean[] mouseButton = new boolean[MouseButton.values().length];
   public long weaponId;
   public float timeOutInSecs;
   public float burstTime;
   public float initialTicks;
   public float timeRunning;
   public BeamReloadCallback reloadCallback;
   public float powerConsumptionPerTick;
   public float powerConsumptionExtraPerTick;
   public float totalConsumedPower;
   public int beamType;
   public boolean markDeath;
   public MetaObject originMetaObject;
   public Vector3i controllerPos;
   public int ticksToDo;
   public long fireStart;
   public boolean dontFade;
   public int ticksDone;
   private float tickRate;
   private float power;
   public double railParent;
   public double railChild;
   public boolean handheld;
   public Vector3f drawVarsCamPos = new Vector3f();
   public Vector3f drawVarsStart = new Vector3f();
   public Vector3f drawVarsEnd = new Vector3f();
   public float drawVarsAxisAngle;
   public float drawVarsDist;
   public float drawVarsLenDiff;
   public Transform drawVarsDrawTransform = new Transform();
   public Transform lastHitTrans = new Transform();
   public Transform lastSegConTrans = new Transform();
   public Vector3i lastHitPos = new Vector3i();
   public boolean oldPower;
   public boolean latchOn;
   public long firstLatch;
   public HitType hitType;
   public Transform initalRelativeTranform = new Transform();
   public boolean friendlyFire;
   public boolean penetrating;
   public float acidDamagePercent;
   public Vector3f hitNormalWorld = new Vector3f();
   public Vector3f hitNormalRelative = new Vector3f();
   public boolean checkLatchConnection = true;
   public Vector3f dirTmp = new Vector3f();
   public Vector3f fromInset = new Vector3f();
   public Vector3f toInset = new Vector3f();
   public float beamLength;
   public float minEffectiveRange;
   public float minEffectiveValue;
   public float maxEffectiveRange;
   public float maxEffectiveValue;
   public int hitSectorId = -1;
   private final Vector3f dd = new Vector3f();
   public boolean ignoreShield;
   public ArmorValue armorValue = new ArmorValue();
   public boolean ignoreArmor;

   public BeamState(long var1, Vector3f var3, Vector3f var4, Vector3f var5, PlayerState var6, float var7, float var8, long var9, int var11, MetaObject var12, Vector3i var13, boolean var14, boolean var15, boolean var16, HitType var17, AbstractBeamHandler var18) {
      this.identifyerSig = var1;
      this.from.set(var4);
      this.to.set(var5);
      this.relativePos.set(var3);
      this.setTickRate(var7);
      this.setPower(var8);
      this.handler = var18;
      this.weaponId = var9;
      this.beamType = var11;
      this.originMetaObject = var12;
      this.color.set(var18.getColor(this));
      this.controllerPos = var13;
      this.handheld = var14;
      this.oldPower = var18.isUsingOldPower();
      this.latchOn = var15;
      this.checkLatchConnection = var16;
      this.firstLatch = Long.MIN_VALUE;
      this.hitType = var17;
   }

   public float getTickRate() {
      return this.tickRate;
   }

   public void setTickRate(float var1) {
      this.tickRate = var1;
   }

   public int hashCode() {
      return (int)this.identifyerSig * this.getHandler().getBeamShooter().hashCode();
   }

   public boolean equals(Object var1) {
      return ((BeamState)var1).getHandler().getBeamShooter() == this.getHandler().getBeamShooter() && ((BeamState)var1).identifyerSig == this.identifyerSig;
   }

   public float getPower() {
      return this.power;
   }

   public void setPower(float var1) {
      this.power = var1;
   }

   public AbstractBeamHandler getHandler() {
      return this.handler;
   }

   public boolean isAlive() {
      if (this.markDeath) {
         return false;
      } else if (!this.dontFade && this.timeRunningSinceLastUpdate > this.timeOutInSecs) {
         return false;
      } else if (this.burstTime <= 0.0F && this.timeRunningSinceLastUpdate < this.timeOutInSecs) {
         return true;
      } else {
         return this.initialTicks > 0.0F || this.ticksToDo > 0 || this.timeRunning < this.timeOutInSecs;
      }
   }

   public boolean isOnServer() {
      return this.handler.isOnServer();
   }

   public void reset() {
      this.ignoreArmor = false;
      this.ignoreShield = false;
      this.currentHit = null;
      this.hitPoint = null;
      this.segmentHit = null;
      this.hitSectorId = -1;
      this.hitOneSegment = 0.0F;
      this.hitBlockTime = 0.0F;
      this.timeSpent = 0.0F;
      this.firstLatch = Long.MIN_VALUE;
      this.initalRelativeTranform.setIdentity();
      this.armorValue.reset();
   }

   public float getPowerByBeamLength() {
      return this.power * this.getBeamLengthModifier();
   }

   public float getBeamLengthModifier() {
      if (this.beamType == 6) {
         return 1.0F;
      } else if (!this.penetrating && this.beamLength > 0.0F && this.maxEffectiveValue != this.minEffectiveValue) {
         float var1;
         if ((var1 = Vector3fTools.diffLength(this.to, this.from) / this.beamLength) >= this.maxEffectiveRange) {
            var1 = this.maxEffectiveValue;
         } else if (var1 <= this.minEffectiveRange) {
            var1 = this.minEffectiveValue;
         } else {
            var1 = (var1 - this.minEffectiveRange) / Math.abs(this.maxEffectiveRange - this.minEffectiveRange);
            float var2;
            if (this.maxEffectiveValue < this.minEffectiveValue) {
               var2 = this.minEffectiveValue - this.maxEffectiveValue;
               var1 = this.minEffectiveValue - var2 * var1;
            } else {
               var2 = this.maxEffectiveValue - this.minEffectiveValue;
               var1 = this.minEffectiveValue + var2 * var1;
            }
         }

         return var1;
      } else {
         return 1.0F;
      }
   }

   public int calcPreviousArmorDamageReduction(float var1) {
      if (VoidElementManager.ARMOR_CALC_STYLE == ArmorDamageCalcStyle.EXPONENTIAL) {
         var1 = Math.max(0.0F, FastMath.pow(var1, VoidElementManager.BEAM_ARMOR_EXPONENTIAL_INCOMING_EXPONENT) / (FastMath.pow(this.armorValue.totalArmorValue, VoidElementManager.BEAM_ARMOR_EXPONENTIAL_ARMOR_VALUE_TOTAL_EXPONENT) + FastMath.pow(var1, VoidElementManager.BEAM_ARMOR_EXPONENTIAL_INCOMING_DAMAGE_ADDED_EXPONENT)));
      } else {
         var1 = Math.max(0.0F, var1 - VoidElementManager.BEAM_ARMOR_FLAT_DAMAGE_REDUCTION * var1);
         var1 = Math.max(0.0F, var1 - Math.min(VoidElementManager.BEAM_ARMOR_THICKNESS_DAMAGE_REDUCTION_MAX, VoidElementManager.BEAM_ARMOR_THICKNESS_DAMAGE_REDUCTION * (float)this.armorValue.typesHit.size() * var1));
      }

      return (int)var1;
   }
}
