package org.schema.game.common.controller.elements;

import org.schema.game.common.controller.PlanetIco;
import org.schema.game.common.data.player.inventory.InventoryHolder;
import org.schema.schine.network.StateInterface;

public class PlanetIcoManagerContainer extends StationaryManagerContainer implements DoorContainerInterface, LiftContainerInterface, ShieldContainerInterface, InventoryHolder {
   public PlanetIcoManagerContainer(StateInterface var1, PlanetIco var2) {
      super(var1, var2);
   }
}
