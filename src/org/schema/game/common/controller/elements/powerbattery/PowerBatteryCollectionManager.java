package org.schema.game.common.controller.elements.powerbattery;

import java.util.Iterator;
import java.util.List;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.client.view.gui.weapon.WeaponPowerBatteryRowElement;
import org.schema.game.client.view.gui.weapon.WeaponRowElementInterface;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.elements.BlockKillInterface;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerActivityInterface;
import org.schema.game.common.controller.elements.ModuleExplosion;
import org.schema.game.common.controller.elements.StationaryManagerContainer;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.power.PowerAddOn;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.input.InputType;
import org.schema.schine.input.KeyboardMappings;

public class PowerBatteryCollectionManager extends ElementCollectionManager implements PlayerUsableInterface, BlockKillInterface, ManagerActivityInterface {
   private double maxPower;
   private double recharge;
   private long lastHit;

   public PowerBatteryCollectionManager(SegmentController var1, VoidElementManager var2) {
      super((short)978, var1, var2);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return PowerBatteryUnit.class;
   }

   public boolean needsUpdate() {
      return false;
   }

   public void handleMouseEvent(ControllerStateUnit var1, MouseEvent var2) {
      if (!this.isOnServer() && var2.pressedLeftMouse()) {
         PowerAddOn var3;
         PowerAddOn var10000 = var3 = ((PowerManagerInterface)((VoidElementManager)this.getElementManager()).getManagerContainer()).getPowerAddOn();
         var10000.setBatteryActive(!var10000.isBatteryActive());
         var3.sendBatteryActiveUpdateClient();
      }

   }

   public void onLogicActivate(SegmentPiece var1, boolean var2, Timer var3) {
   }

   public PowerBatteryUnit getInstance() {
      return new PowerBatteryUnit();
   }

   protected void onChangedCollection() {
      this.refreshMaxPower();
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return ((ManagedSegmentController)this.getSegmentController()).getManagerContainer() instanceof StationaryManagerContainer ? new GUIKeyValueEntry[]{new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWERBATTERY_POWERBATTERYCOLLECTIONMANAGER_2, StringTools.formatPointZero(this.recharge * VoidElementManager.POWER_BATTERY_TURNED_OFF_MULT)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWERBATTERY_POWERBATTERYCOLLECTIONMANAGER_1, StringTools.formatPointZero(this.recharge * VoidElementManager.POWER_BATTERY_TURNED_ON_MULT)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWERBATTERY_POWERBATTERYCOLLECTIONMANAGER_0, StringTools.formatPointZero(this.maxPower)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWERBATTERY_POWERBATTERYCOLLECTIONMANAGER_3, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWERBATTERY_POWERBATTERYCOLLECTIONMANAGER_10)} : new GUIKeyValueEntry[]{new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWERBATTERY_POWERBATTERYCOLLECTIONMANAGER_6, StringTools.formatPointZero(this.recharge * VoidElementManager.POWER_BATTERY_TURNED_OFF_MULT)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWERBATTERY_POWERBATTERYCOLLECTIONMANAGER_7, StringTools.formatPointZero(this.recharge * VoidElementManager.POWER_BATTERY_TURNED_ON_MULT)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWERBATTERY_POWERBATTERYCOLLECTIONMANAGER_8, StringTools.formatPointZero(this.maxPower)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWERBATTERY_POWERBATTERYCOLLECTIONMANAGER_9, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWERBATTERY_POWERBATTERYCOLLECTIONMANAGER_4)};
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWERBATTERY_POWERBATTERYCOLLECTIONMANAGER_5;
   }

   public double getMaxPower() {
      return this.maxPower;
   }

   public void setMaxPower(double var1) {
      this.maxPower = var1;
   }

   private void refreshMaxPower() {
      this.maxPower = 0.0D;
      this.recharge = 0.0D;
      int var1 = 0;

      PowerBatteryUnit var3;
      for(Iterator var2 = this.getElementCollections().iterator(); var2.hasNext(); var1 += var3.size()) {
         (var3 = (PowerBatteryUnit)var2.next()).refreshPowerCapabilities();
         this.maxPower += var3.getMaxPower();
         this.recharge += var3.getRecharge();
      }

      ((PowerManagerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getPowerAddOn().setBatteryMaxPower(this.maxPower);
      this.recharge += (double)var1 * VoidElementManager.POWER_BATTERY_LINEAR_GROWTH;
      ((PowerManagerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getPowerAddOn().setBatteryRecharge(this.recharge);
   }

   public boolean isActive() {
      return ((PowerManagerInterface)((VoidElementManager)this.getElementManager()).getManagerContainer()).getPowerAddOn().isBatteryActive();
   }

   public void onKilledBlock(long var1, short var3, Damager var4) {
      if (this.getSegmentController().isOnServer()) {
         if (var3 == 978 && System.currentTimeMillis() - this.lastHit > 1000L) {
            this.lastHit = System.currentTimeMillis();
            List var5;
            int var6 = (var5 = this.getContainer().getModuleExplosions()).size();

            int var7;
            for(var7 = 0; var7 < var6; ++var7) {
               if (((ModuleExplosion)var5.get(var7)).getModuleBB().isInside((float)ElementCollection.getPosX(var1), (float)ElementCollection.getPosY(var1), (float)ElementCollection.getPosZ(var1))) {
                  return;
               }
            }

            for(var7 = 0; var7 < this.getElementCollections().size(); ++var7) {
               if (((PowerBatteryUnit)this.getElementCollections().get(var7)).getNeighboringCollection().contains(var1)) {
                  PowerBatteryUnit var8 = (PowerBatteryUnit)this.getElementCollections().get(var7);
                  long var9 = (long)(1.0D / Math.max(1.0E-7D, VoidElementManager.POWER_BATTERY_EXPLOSION_RATE) * 1000.0D);
                  long var11 = (long)(VoidElementManager.POWER_BATTERY_EXPLOSION_RADIUS_PER_BLOCKS * (double)var8.size());
                  int var17 = (int)Math.min(VoidElementManager.POWER_BATTERY_EXPLOSION_RADIUS_MAX, (double)Math.min(64L, Math.max(3L, var11)));
                  long var13 = (long)(VoidElementManager.POWER_BATTERY_EXPLOSION_DAMAGE_PER_BLOCKS * (double)var8.size());
                  var6 = (int)Math.min(VoidElementManager.POWER_BATTERY_EXPLOSION_DAMAGE_MAX, (double)Math.min(2147483647L, Math.max(0L, var13)));
                  long var15 = (long)(VoidElementManager.POWER_BATTERY_EXPLOSION_COUNT_PER_BLOCKS * (double)var8.size());
                  var7 = (int)(Math.min(1.0D, VoidElementManager.POWER_BATTERY_EXPLOSION_COUNT_PERCENT) * (double)var8.size());
                  var7 = Math.max(1, (int)Math.min((long)var7, var15));
                  var8.explodeOnServer(var7, var1, var3, var9, var17, var6, false, ModuleExplosion.ExplosionCause.POWER_AUX, var4);
                  return;
               }
            }
         }

      }
   }

   public float getSensorValue(SegmentPiece var1) {
      PowerAddOn var2 = ((PowerManagerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getPowerAddOn();
      return (float)Math.min(1.0D, var2.getBatteryPower() / Math.max(9.999999747378752E-5D, var2.getBatteryMaxPower()));
   }

   public WeaponRowElementInterface getWeaponRow() {
      if (this.getTotalSize() > 0) {
         Vector3i var1 = ((PowerBatteryUnit)this.getElementCollections().get(0)).getIdPos(new Vector3i());
         SegmentPiece var2;
         if ((var2 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var1)) != null && var2.getType() == 978) {
            return new WeaponPowerBatteryRowElement(var2, this);
         }
      }

      return null;
   }

   public boolean isControllerConnectedTo(long var1, short var3) {
      return true;
   }

   public boolean isPlayerUsable() {
      return this.getSegmentController().isUsingOldPower();
   }

   public long getUsableId() {
      return -9223372036854775805L;
   }

   public void handleControl(ControllerStateInterface var1, Timer var2) {
      throw new RuntimeException("Implement");
   }

   public void handleKeyEvent(ControllerStateUnit var1, KeyboardMappings var2) {
   }

   public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
      var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWERBATTERY_POWERBATTERYCOLLECTIONMANAGER_11, var3, ContextFilter.IMPORTANT);
   }
}
