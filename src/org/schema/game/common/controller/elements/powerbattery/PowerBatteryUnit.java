package org.schema.game.common.controller.elements.powerbattery;

import org.schema.common.FastMath;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.common.language.Lng;

public class PowerBatteryUnit extends ElementCollection {
   private double recharge;
   private double maxPower;

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      Vector3i var4;
      (var4 = new Vector3i()).sub(this.getMax(new Vector3i()), this.getMin(new Vector3i()));
      return ControllerManagerGUI.create(var1, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWERBATTERY_POWERBATTERYUNIT_1, this, new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWERBATTERY_POWERBATTERYUNIT_0, var4), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWERBATTERY_POWERBATTERYUNIT_2, this.size()), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWERBATTERY_POWERBATTERYUNIT_3, StringTools.formatPointZero(this.recharge + (double)this.size() * VoidElementManager.POWER_BATTERY_LINEAR_GROWTH)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWERBATTERY_POWERBATTERYUNIT_4, StringTools.formatPointZero(this.maxPower)));
   }

   public boolean hasMesh() {
      return false;
   }

   public double getRecharge() {
      return this.recharge;
   }

   public void refreshPowerCapabilities() {
      this.recharge = (double)this.size();
      this.recharge = Math.pow(this.recharge * VoidElementManager.POWER_BATTERY_GROUP_MULTIPLIER, VoidElementManager.POWER_BATTERY_GROUP_POW);
      this.recharge = Math.max(1.0D, FastMath.logGrowth(this.recharge, VoidElementManager.POWER_BATTERY_GROUP_GROWTH, VoidElementManager.POWER_BATTERY_GROUP_CEILING));
      this.maxPower = Math.pow((double)this.size(), (double)VoidElementManager.POWER_BATTERY_CAPACITY_POW) * (double)VoidElementManager.POWER_BATTERY_CAPACITY_LINEAR;
   }

   public double getMaxPower() {
      return this.maxPower;
   }
}
