package org.schema.game.common.controller;

import org.schema.common.util.linAlg.Vector3i;

public class DimensionFilter {
   private int[] f = new int[3];
   private boolean[] b = new boolean[3];

   public int getXFilter() {
      return this.f[0];
   }

   public void setXFilter(int var1) {
      this.f[0] = var1;
      this.b[0] = true;
   }

   public int getYFilter() {
      return this.f[1];
   }

   public void setYFilter(int var1) {
      this.f[1] = var1;
      this.b[1] = true;
   }

   public int getZFilter() {
      return this.f[2];
   }

   public void setZFilter(int var1) {
      this.f[2] = var1;
      this.b[2] = true;
   }

   public boolean hasXFilter() {
      return this.b[0];
   }

   public boolean hasYFilter() {
      return this.b[1];
   }

   public boolean hasZFilter() {
      return this.b[2];
   }

   public boolean isValid(Vector3i var1) {
      if (this.b[0]) {
         if (var1.x != this.f[0]) {
            System.err.println("X valid " + var1.x + "/" + this.f[0] + " ");
         }

         return var1.x == this.f[0];
      } else if (this.b[1]) {
         if (var1.y != this.f[1]) {
            System.err.println("Y valid " + var1.y + "/" + this.f[1] + " ");
         }

         return var1.y == this.f[1];
      } else if (this.b[2]) {
         if (var1.z != this.f[2]) {
            System.err.println("Z valid " + var1.z + "/" + this.f[2] + " ");
         }

         return var1.z == this.f[2];
      } else {
         return true;
      }
   }
}
