package org.schema.game.common.controller;

import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.element.beam.AbstractBeamHandler;

public interface BeamHandlerContainer extends Damager {
   AbstractBeamHandler getHandler();
}
