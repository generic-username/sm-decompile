package org.schema.game.common.controller.activities;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.elements.Destination;

public class RaceDestination extends Destination {
   public Vector3i sector = new Vector3i();
   public String uid_full;

   public int hashCode() {
      return super.hashCode() + this.sector.hashCode();
   }

   public boolean equals(Object var1) {
      RaceDestination var2;
      return (var2 = (RaceDestination)var1).sector.equals(this.sector) && super.equals(var2);
   }

   public String toString() {
      return "RaceDestination [sector=" + this.sector + ", uid=" + this.uid + ", local=" + this.local + "]";
   }
}
