package org.schema.game.common.controller;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collections;
import java.util.Locale;
import org.schema.common.config.ConfigParserException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class HpConditionList {
   private ObjectArrayList list;
   private ObjectArrayList listOld;

   public void parse(Node var1) throws ConfigParserException {
      Node var2 = var1.getAttributes().getNamedItem("version");
      boolean var3 = false;
      if (var2 != null && var2.getNodeValue().toLowerCase(Locale.ENGLISH).equals("noreactor")) {
         var3 = true;
      }

      NodeList var6 = var1.getChildNodes();
      ObjectArrayList var7 = new ObjectArrayList();

      for(int var4 = 0; var4 < var6.getLength(); ++var4) {
         Node var5;
         if ((var5 = var6.item(var4)).getNodeType() == 1) {
            HpCondition var8 = HpCondition.parse(var5);
            var7.add(var8);
         }
      }

      if (var7.size() > 0) {
         var7.trim();
      }

      Collections.sort(var7);
      if (var3) {
         this.listOld = var7;
      } else {
         this.list = var7;
      }
   }

   public ObjectArrayList get(boolean var1) {
      if (!var1 && this.listOld != null) {
         if (this.listOld == null) {
            throw new RuntimeException("HP Triggers(old) missing");
         } else {
            return this.listOld;
         }
      } else {
         assert this.list != null;

         if (this.list == null) {
            throw new RuntimeException("HP Triggers(Reactor) missing");
         } else {
            return this.list;
         }
      }
   }

   public String checkString() {
      return "Reactor: " + this.list + ";    Old: " + this.listOld;
   }
}
