package org.schema.game.common.controller;

public class DockingException extends Exception {
   private static final long serialVersionUID = 1L;

   public DockingException(String var1) {
      super(var1);
   }
}
