package org.schema.game.common.controller.damage;

public enum HitReceiverType {
   BLOCK,
   SHIELD,
   MINE,
   CHARACTER,
   OTHER,
   ARMOR;
}
