package org.schema.game.common.controller.damage.effects;

public class EMEffectHandler extends InterEffectHandler {
   public InterEffectHandler.InterEffectType getType() {
      return InterEffectHandler.InterEffectType.EM;
   }
}
