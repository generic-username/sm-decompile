package org.schema.game.common.controller.damage.effects;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Arrays;
import java.util.Locale;
import org.schema.common.config.ConfigParserException;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.data.blockeffects.config.ConfigEntityManager;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.ElementInformation;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class InterEffectSet {
   public static final int length = InterEffectHandler.InterEffectType.values().length;
   private float[] strength;

   public InterEffectSet() {
      this.strength = new float[length];
   }

   public InterEffectSet(InterEffectSet var1) {
      this.strength = new float[length];
      this.setEffect(var1);
   }

   public float getStrength(InterEffectHandler.InterEffectType var1) {
      return this.strength[var1.ordinal()];
   }

   public void reset() {
      Arrays.fill(this.strength, 0.0F);
   }

   public void setEffect(InterEffectSet var1) {
      for(int var2 = 0; var2 < length; ++var2) {
         this.strength[var2] = var1.strength[var2];
      }

   }

   public void setStrength(InterEffectHandler.InterEffectType var1, float var2) {
      this.strength[var1.ordinal()] = var2;
   }

   public void scaleAdd(InterEffectSet var1, float var2) {
      for(int var3 = 0; var3 < length; ++var3) {
         float[] var10000 = this.strength;
         var10000[var3] += var1.strength[var3] * var2;
      }

   }

   public boolean hasEffect(InterEffectHandler.InterEffectType var1) {
      return this.getStrength(var1) > 0.0F;
   }

   public void parseXML(Node var1) throws ConfigParserException {
      ObjectOpenHashSet var2 = new ObjectOpenHashSet();
      InterEffectHandler.InterEffectType[] var3 = InterEffectHandler.InterEffectType.values();
      NodeList var4 = var1.getChildNodes();

      for(int var5 = 0; var5 < var4.getLength(); ++var5) {
         Node var6;
         if ((var6 = var4.item(var5)).getNodeType() == 1) {
            String var7 = var6.getNodeName().toLowerCase(Locale.ENGLISH);
            boolean var8 = false;
            InterEffectHandler.InterEffectType[] var9 = var3;
            int var10 = var3.length;

            for(int var11 = 0; var11 < var10; ++var11) {
               InterEffectHandler.InterEffectType var12;
               if ((var12 = var9[var11]).id.toLowerCase(Locale.ENGLISH).equals(var7)) {
                  try {
                     this.strength[var12.ordinal()] = Float.parseFloat(var6.getTextContent());
                  } catch (NumberFormatException var13) {
                     throw new ConfigParserException(var6.getParentNode().getParentNode().getNodeName() + "->" + var6.getParentNode().getNodeName() + "->" + var6.getNodeName() + " must be floating point value", var13);
                  }

                  var8 = true;
                  var2.add(var12);
                  break;
               }
            }

            if (!var8) {
               throw new ConfigParserException(var6.getParentNode().getParentNode().getNodeName() + "->" + var6.getParentNode().getNodeName() + "->" + var6.getNodeName() + " no effect found for '" + var7 + "'; must be one of: " + Arrays.toString(var3));
            }
         }
      }

      if (var2.size() < var3.length) {
         throw new ConfigParserException(var1.getParentNode().getNodeName() + "->" + var1.getNodeName() + " missing effects element! set: " + var2 + ";. Must contain all of " + Arrays.toString(var3));
      }
   }

   public boolean isZero() {
      float[] var1;
      int var2 = (var1 = this.strength).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         if (var1[var3] != 0.0F) {
            return false;
         }
      }

      return true;
   }

   public String toString() {
      StringBuffer var1 = new StringBuffer();
      InterEffectHandler.InterEffectType[] var2 = InterEffectHandler.InterEffectType.values();
      var1.append("EFFECT[");

      for(int var3 = 0; var3 < length; ++var3) {
         var1.append("(");
         var1.append(var2[var3].id);
         var1.append(" = ");
         var1.append(this.strength[var3]);
         var1.append(")");
         if (var3 < length - 1) {
            var1.append(", ");
         }
      }

      var1.append("]");
      return var1.toString();
   }

   public void applyAddEffectConfig(ConfigEntityManager var1, StatusEffectType var2, InterEffectHandler.InterEffectType var3) {
      int var4 = var3.ordinal();
      float[] var10000 = this.strength;
      var10000[var4] += var1.apply(var2, this.strength[var4]);
   }

   public void mul(InterEffectSet var1) {
      for(int var2 = 0; var2 < length; ++var2) {
         float[] var10000 = this.strength;
         var10000[var2] *= var1.strength[var2];
      }

   }

   public void add(InterEffectSet var1) {
      for(int var2 = 0; var2 < length; ++var2) {
         float[] var10000 = this.strength;
         var10000[var2] += var1.strength[var2];
      }

   }

   public void setDefenseFromInfo(ElementInformation var1) {
      this.setAdd(var1.effectArmor, var1.isArmor() ? VoidElementManager.armorEffectConfiguration : VoidElementManager.basicEffectConfiguration);
   }

   public void setAdd(InterEffectSet var1, InterEffectSet var2) {
      this.setEffect(var1);
      this.add(var2);
   }
}
