package org.schema.game.common.controller.damage;

import org.schema.schine.common.language.Lng;

public enum DamageDealerType {
   PROJECTILE,
   MISSILE,
   PULSE,
   BEAM,
   EXPLOSIVE,
   GENERAL;

   public final String getName() {
      switch(this) {
      case BEAM:
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_DAMAGE_DAMAGEDEALERTYPE_0;
      case EXPLOSIVE:
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_DAMAGE_DAMAGEDEALERTYPE_1;
      case GENERAL:
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_DAMAGE_DAMAGEDEALERTYPE_2;
      case MISSILE:
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_DAMAGE_DAMAGEDEALERTYPE_3;
      case PROJECTILE:
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_DAMAGE_DAMAGEDEALERTYPE_4;
      case PULSE:
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_DAMAGE_DAMAGEDEALERTYPE_5;
      default:
         return "DamageDealerType(N/A)";
      }
   }
}
