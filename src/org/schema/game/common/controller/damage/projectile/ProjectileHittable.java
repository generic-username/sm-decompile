package org.schema.game.common.controller.damage.projectile;

import org.schema.game.common.controller.damage.Hittable;

public interface ProjectileHittable extends Hittable {
}
