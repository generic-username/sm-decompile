package org.schema.game.common.controller.damage.projectile;

import javax.vecmath.Vector3f;

public class ParticleHitCallback {
   public boolean hit = false;
   public boolean killedBlock;
   private float damageDone;
   public Vector3f hitPointWorld;
   public Vector3f hitNormalWorld;
   public ProjectileBlockHit blockHit;
   public boolean abortWhole;
   public long beforeBlockIndex = Long.MIN_VALUE;
   public long afterBlockIndex = Long.MIN_VALUE;

   public boolean hasDoneDamage() {
      return this.hit && this.getDamageDone() > 0.0F;
   }

   public void reset() {
      this.beforeBlockIndex = Long.MIN_VALUE;
      this.afterBlockIndex = Long.MIN_VALUE;
      this.hit = false;
      this.abortWhole = false;
      this.killedBlock = false;
      this.damageDone = 0.0F;
   }

   public float getDamageDone() {
      return this.damageDone;
   }

   public void addDamageDone(float var1) {
      this.damageDone += var1;
   }
}
