package org.schema.game.common.controller.damage.projectile;

import java.util.Arrays;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.forms.particle.DamageContainerInterface;
import org.schema.schine.graphicsengine.forms.particle.ParticleContainer;
import org.schema.schine.graphicsengine.forms.particle.StartContainerInterface;

public class ProjectileParticleContainer extends ParticleContainer implements DamageContainerInterface, StartContainerInterface {
   public static final int blocksizeInt = 9;
   private static final int id = 0;
   private static final int blockHitIndex = 1;
   private static final int ownerId = 2;
   private static final int wepId0 = 3;
   private static final int wepId1 = 4;
   private static final int shotStatus = 5;
   private static final int acidFormulaIndex = 6;
   private static final int penetrationDepth = 7;
   private static final int spriteCode = 8;
   public static final int blocksizeFloat = 19;
   private static final int pos = 0;
   private static final int velocity = 3;
   private static final int start = 6;
   private static final int color = 9;
   private static final int lifetime = 13;
   private static final int damage = 14;
   private static final int maxDistance = 15;
   private static final int damageInitial = 16;
   private static final int width = 17;
   private static final int impactForce = 18;
   private float[] particleArrayFloat;
   private int[] particleArrayInt;

   public ProjectileParticleContainer(int var1) {
      super(var1);
      this.particleArrayFloat = new float[var1 * 19];
      this.particleArrayInt = new int[var1 * 9];
   }

   public void reset() {
      Arrays.fill(this.particleArrayFloat, 0.0F);
      Arrays.fill(this.particleArrayInt, 0);
   }

   public void growCapacity() {
      this.capacity <<= 1;

      assert this.capacity < 65536 : this.capacity;

      this.particleArrayFloat = Arrays.copyOf(this.particleArrayFloat, this.capacity * 19);
      this.particleArrayInt = Arrays.copyOf(this.particleArrayInt, this.capacity * 9);
   }

   public static final int getIndexFloat(int var0) {
      return var0 * 19;
   }

   public static final int getIndexInt(int var0) {
      return var0 * 9;
   }

   public Vector4f getColor(int var1, Vector4f var2) {
      var1 = getIndexFloat(var1);
      var2.x = this.particleArrayFloat[var1 + 9];
      var2.y = this.particleArrayFloat[var1 + 9 + 1];
      var2.z = this.particleArrayFloat[var1 + 9 + 2];
      var2.w = this.particleArrayFloat[var1 + 9 + 3];
      return var2;
   }

   public float getLifetime(int var1) {
      var1 = getIndexFloat(var1);
      return this.particleArrayFloat[var1 + 13];
   }

   public int getId(int var1) {
      var1 = getIndexInt(var1);
      return this.particleArrayInt[var1];
   }

   public Vector3f getPos(int var1, Vector3f var2) {
      var1 = getIndexFloat(var1);
      var2.x = this.particleArrayFloat[var1];
      var2.y = this.particleArrayFloat[var1 + 1];
      var2.z = this.particleArrayFloat[var1 + 2];
      return var2;
   }

   public Vector3f getStart(int var1, Vector3f var2) {
      var1 = getIndexFloat(var1);
      var2.x = this.particleArrayFloat[var1 + 6];
      var2.y = this.particleArrayFloat[var1 + 6 + 1];
      var2.z = this.particleArrayFloat[var1 + 6 + 2];
      return var2;
   }

   public void copy(int var1, int var2) {
      int var3 = getIndexFloat(var1);
      int var4 = getIndexFloat(var2);

      int var5;
      for(var5 = 0; var5 < 19; ++var5) {
         this.particleArrayFloat[var4 + var5] = this.particleArrayFloat[var3 + var5];
      }

      var5 = getIndexInt(var1);
      var1 = getIndexInt(var2);

      for(var2 = 0; var2 < 9; ++var2) {
         this.particleArrayInt[var1 + var2] = this.particleArrayInt[var5 + var2];
      }

   }

   public float[] getArrayFloat() {
      return this.particleArrayFloat;
   }

   public int[] getArrayInt() {
      return this.particleArrayInt;
   }

   public int getBlockHitIndex(int var1) {
      return this.particleArrayInt[getIndexInt(var1) + 1];
   }

   public void setBlockHitIndex(int var1, int var2) {
      this.particleArrayInt[getIndexInt(var1) + 1] = var2;
   }

   public int getAcidFormulaIndex(int var1) {
      return this.particleArrayInt[getIndexInt(var1) + 6];
   }

   public void setAcidFormulaIndex(int var1, int var2) {
      this.particleArrayInt[getIndexInt(var1) + 6] = var2;
   }

   public void incBlockHitIndex(int var1) {
      int var10002 = this.particleArrayInt[getIndexInt(var1) + 1]++;
   }

   protected void swapValuesInt(int var1, int var2) {
      var1 = getIndexInt(var1);
      var2 = getIndexInt(var2);

      for(int var3 = 0; var3 < 9; ++var3) {
         int var4 = this.particleArrayInt[var2 + var3];
         this.particleArrayInt[var2 + var3] = this.particleArrayInt[var1 + var3];
         this.particleArrayInt[var1 + var3] = var4;
      }

   }

   protected void swapValuesFloat(int var1, int var2) {
      var1 = getIndexFloat(var1);
      var2 = getIndexFloat(var2);

      for(int var3 = 0; var3 < 19; ++var3) {
         float var4 = this.particleArrayFloat[var2 + var3];
         this.particleArrayFloat[var2 + var3] = this.particleArrayFloat[var1 + var3];
         this.particleArrayFloat[var1 + var3] = var4;
      }

   }

   public Vector3f getVelocity(int var1, Vector3f var2) {
      var1 = getIndexFloat(var1);
      var2.x = this.particleArrayFloat[var1 + 3];
      var2.y = this.particleArrayFloat[var1 + 3 + 1];
      var2.z = this.particleArrayFloat[var1 + 3 + 2];
      return var2;
   }

   public void setColor(int var1, float var2, float var3, float var4, float var5) {
      var1 = getIndexFloat(var1);
      this.particleArrayFloat[var1 + 9] = var2;
      this.particleArrayFloat[var1 + 9 + 1] = var3;
      this.particleArrayFloat[var1 + 9 + 2] = var4;
      this.particleArrayFloat[var1 + 9 + 3] = var5;
   }

   public void setColor(int var1, Vector4f var2) {
      this.setColor(var1, var2.x, var2.y, var2.z, var2.w);
   }

   public void setLifetime(int var1, float var2) {
      this.particleArrayFloat[getIndexFloat(var1) + 13] = var2;
   }

   public void setId(int var1, int var2) {
      this.particleArrayInt[getIndexInt(var1)] = var2;
   }

   public int getOwnerId(int var1) {
      return this.particleArrayInt[getIndexInt(var1) + 2];
   }

   public void setOwnerId(int var1, int var2) {
      this.particleArrayInt[getIndexInt(var1) + 2] = var2;
   }

   public int getShotStatus(int var1) {
      return this.particleArrayInt[getIndexInt(var1) + 5];
   }

   public void setShotStatus(int var1, int var2) {
      this.particleArrayInt[getIndexInt(var1) + 5] = var2;
   }

   public long getWeaponId(int var1) {
      int var2 = this.particleArrayInt[getIndexInt(var1) + 3];
      var1 = this.particleArrayInt[getIndexInt(var1) + 4];
      return (long)var2 << 32 | (long)var1 & 4294967295L;
   }

   public void setWeaponId(int var1, long var2) {
      var1 = getIndexInt(var1);
      this.particleArrayInt[var1 + 3] = (int)(var2 >> 32);
      this.particleArrayInt[var1 + 4] = (int)var2;
   }

   public int getPenetrationDepth(int var1) {
      return this.particleArrayInt[getIndexInt(var1) + 7];
   }

   public void setPenetrationDepth(int var1, int var2) {
      this.particleArrayInt[getIndexInt(var1) + 7] = var2;
   }

   public float getDamage(int var1) {
      return this.particleArrayFloat[getIndexFloat(var1) + 14];
   }

   public void setDamage(int var1, float var2) {
      this.particleArrayFloat[getIndexFloat(var1) + 14] = var2;
   }

   public float getWidth(int var1) {
      return this.particleArrayFloat[getIndexFloat(var1) + 17];
   }

   public void setWidth(int var1, float var2) {
      this.particleArrayFloat[getIndexFloat(var1) + 17] = var2;
   }

   public float getDamageInitial(int var1) {
      return this.particleArrayFloat[getIndexFloat(var1) + 16];
   }

   public void setDamageInitial(int var1, float var2) {
      this.particleArrayFloat[getIndexFloat(var1) + 16] = var2;
   }

   public float getImpactForce(int var1) {
      return this.particleArrayFloat[getIndexFloat(var1) + 18];
   }

   public void setImpactForce(int var1, float var2) {
      this.particleArrayFloat[getIndexFloat(var1) + 18] = var2;
   }

   public void setMaxDistance(int var1, float var2) {
      this.particleArrayFloat[getIndexFloat(var1) + 15] = var2;
   }

   public float getMaxDistance(int var1) {
      return this.particleArrayFloat[getIndexFloat(var1) + 15];
   }

   public void setPos(int var1, float var2, float var3, float var4) {
      var1 = getIndexFloat(var1);
      this.particleArrayFloat[var1] = var2;
      this.particleArrayFloat[var1 + 1] = var3;
      this.particleArrayFloat[var1 + 2] = var4;
   }

   public void setStart(int var1, float var2, float var3, float var4) {
      var1 = getIndexFloat(var1);
      this.particleArrayFloat[var1 + 6] = var2;
      this.particleArrayFloat[var1 + 6 + 1] = var3;
      this.particleArrayFloat[var1 + 6 + 2] = var4;
   }

   public void setVelocity(int var1, float var2, float var3, float var4) {
      var1 = getIndexFloat(var1);
      this.particleArrayFloat[var1 + 3] = var2;
      this.particleArrayFloat[var1 + 3 + 1] = var3;
      this.particleArrayFloat[var1 + 3 + 2] = var4;
   }

   public int getSpriteCode(int var1) {
      return this.particleArrayInt[getIndexInt(var1) + 8];
   }

   public int getSpriteCodeSpriteMaxY(int var1, int var2, int var3, int var4) {
      return this.getSpriteCode(var1) / 10000;
   }

   public int getSpriteCodeSpriteMaxX(int var1, int var2, int var3, int var4) {
      return this.getSpriteCode(var1) % 10000 / 100;
   }

   public int getSpriteCodeSpriteIndex(int var1, int var2, int var3, int var4) {
      return this.getSpriteCode(var1) % 100;
   }

   public void setSpriteCode(int var1, int var2, int var3, int var4) {
      this.setSpriteCode(var1, var2 + var3 * 100 + var4 * 10000);
   }

   public void setSpriteCode(int var1, int var2) {
      this.particleArrayInt[getIndexInt(var1) + 8] = var2;
   }
}
