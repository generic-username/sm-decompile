package org.schema.game.common.controller.damage.projectile;

import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.game.common.data.physics.PairCachingGhostObjectAlignable;
import org.schema.game.common.data.physics.RigidBodySimple;
import org.schema.game.common.data.player.AbstractCharacter;

public class ProjectileHandlerCharacter extends ProjectileHandler {
   public ProjectileController.ProjectileHandleState handle(Damager var1, ProjectileController var2, Vector3f var3, Vector3f var4, ProjectileParticleContainer var5, int var6, CubeRayCastResult var7) {
      if (var7.hasHit()) {
         float var8 = var5.getDamage(var6);
         AbstractCharacter var9;
         if (var7.collisionObject instanceof PairCachingGhostObjectAlignable) {
            var9 = (AbstractCharacter)((PairCachingGhostObjectAlignable)var7.collisionObject).getObj();
         } else {
            var9 = (AbstractCharacter)((RigidBodySimple)var7.collisionObject).getSimpleTransformableSendableObject();
         }

         if (var1.getOwnerState() != var9.getOwnerState()) {
            return !var9.checkAttack(var1, true, true) ? ProjectileController.ProjectileHandleState.PROJECTILE_NO_HIT : this.damageCharacter(var9, var8, var1, var7.hitPointWorld);
         } else {
            return ProjectileController.ProjectileHandleState.PROJECTILE_NO_HIT;
         }
      } else {
         return ProjectileController.ProjectileHandleState.PROJECTILE_NO_HIT;
      }
   }

   public ProjectileController.ProjectileHandleState handleBefore(Damager var1, ProjectileController var2, Vector3f var3, Vector3f var4, ProjectileParticleContainer var5, int var6, CubeRayCastResult var7) {
      return ProjectileController.ProjectileHandleState.PROJECTILE_IGNORE;
   }

   public ProjectileController.ProjectileHandleState handleAfterIfNotStopped(Damager var1, ProjectileController var2, Vector3f var3, Vector3f var4, ProjectileParticleContainer var5, int var6, CubeRayCastResult var7) {
      return ProjectileController.ProjectileHandleState.PROJECTILE_IGNORE;
   }

   private ProjectileController.ProjectileHandleState damageCharacter(AbstractCharacter var1, float var2, Damager var3, Vector3f var4) {
      if (var1.isOnServer()) {
         if (!var1.canAttack(var3)) {
            return ProjectileController.ProjectileHandleState.PROJECTILE_NO_HIT_STOP;
         }

         var1.damage(var2, var3);
      } else {
         ((GameClientState)var1.getState()).getWorldDrawer().getExplosionDrawer().addExplosion(var4);
      }

      return ProjectileController.ProjectileHandleState.PROJECTILE_HIT_STOP;
   }
}
