package org.schema.game.common.controller.damage.projectile;

import javax.vecmath.Vector3f;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.physics.CubeRayCastResult;

public class ProjectileHandlerIgnore extends ProjectileHandler {
   public ProjectileController.ProjectileHandleState handleBefore(Damager var1, ProjectileController var2, Vector3f var3, Vector3f var4, ProjectileParticleContainer var5, int var6, CubeRayCastResult var7) {
      return ProjectileController.ProjectileHandleState.PROJECTILE_IGNORE;
   }

   public ProjectileController.ProjectileHandleState handleAfterIfNotStopped(Damager var1, ProjectileController var2, Vector3f var3, Vector3f var4, ProjectileParticleContainer var5, int var6, CubeRayCastResult var7) {
      return ProjectileController.ProjectileHandleState.PROJECTILE_IGNORE;
   }

   public ProjectileController.ProjectileHandleState handle(Damager var1, ProjectileController var2, Vector3f var3, Vector3f var4, ProjectileParticleContainer var5, int var6, CubeRayCastResult var7) {
      return ProjectileController.ProjectileHandleState.PROJECTILE_IGNORE;
   }
}
