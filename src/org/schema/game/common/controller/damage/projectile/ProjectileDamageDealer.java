package org.schema.game.common.controller.damage.projectile;

import org.schema.game.common.controller.damage.DamageDealer;
import org.schema.game.common.controller.damage.acid.AcidDamageFormula;

public interface ProjectileDamageDealer extends DamageDealer {
   AcidDamageFormula.AcidFormulaType getAcidFormula();
}
