package org.schema.game.common.controller.damage.projectile;

import javax.vecmath.Vector3f;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.elements.power.reactor.StabilizerPath;
import org.schema.game.common.data.physics.CubeRayCastResult;

public class ProjectileHandlerStabilizerPath extends ProjectileHandler {
   public ProjectileController.ProjectileHandleState handle(Damager var1, ProjectileController var2, Vector3f var3, Vector3f var4, ProjectileParticleContainer var5, int var6, CubeRayCastResult var7) {
      float var8 = var5.getDamage(var6);
      ((StabilizerPath)var7.collisionObject.getUserPointer()).onHit(var1, var8);
      return ProjectileController.ProjectileHandleState.PROJECTILE_HIT_CONTINUE;
   }

   public ProjectileController.ProjectileHandleState handleBefore(Damager var1, ProjectileController var2, Vector3f var3, Vector3f var4, ProjectileParticleContainer var5, int var6, CubeRayCastResult var7) {
      return ProjectileController.ProjectileHandleState.PROJECTILE_IGNORE;
   }

   public ProjectileController.ProjectileHandleState handleAfterIfNotStopped(Damager var1, ProjectileController var2, Vector3f var3, Vector3f var4, ProjectileParticleContainer var5, int var6, CubeRayCastResult var7) {
      return ProjectileController.ProjectileHandleState.PROJECTILE_IGNORE;
   }
}
