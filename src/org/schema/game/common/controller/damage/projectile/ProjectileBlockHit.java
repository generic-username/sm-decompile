package org.schema.game.common.controller.damage.projectile;

import javax.vecmath.Vector3f;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.SegmentData;

public class ProjectileBlockHit {
   SegmentController objectId;
   public long block;
   Vector3f absPosition = new Vector3f();
   public int localBlock;
   private SegmentData segmentData;

   public SegmentData getSegmentData() {
      if (this.segmentData != null && this.segmentData.getSegment().getSegmentData() != this.segmentData) {
         this.segmentData = this.segmentData.getSegment().getSegmentData();
      }

      return this.segmentData;
   }

   public void setSegmentData(SegmentData var1) {
      this.segmentData = var1;
   }
}
