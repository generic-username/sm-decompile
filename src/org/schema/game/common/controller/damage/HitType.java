package org.schema.game.common.controller.damage;

public enum HitType {
   WEAPON,
   SUPPORT,
   ENVIROMENTAL,
   INTERNAL,
   GENERAL;
}
