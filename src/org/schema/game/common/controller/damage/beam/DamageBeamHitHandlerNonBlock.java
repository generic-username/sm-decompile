package org.schema.game.common.controller.damage.beam;

import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.schine.graphicsengine.core.Timer;

public abstract class DamageBeamHitHandlerNonBlock implements DamageBeamHitHandler {
   public abstract int onBeamDamage(SimpleGameObject var1, BeamState var2, int var3, CubeRayCastResult var4, Timer var5);
}
