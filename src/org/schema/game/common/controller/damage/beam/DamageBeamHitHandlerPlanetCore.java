package org.schema.game.common.controller.damage.beam;

import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.game.common.data.world.space.PlanetCore;
import org.schema.schine.graphicsengine.core.Timer;

public class DamageBeamHitHandlerPlanetCore extends DamageBeamHitHandlerNonBlock {
   public void reset() {
   }

   public int onBeamDamage(PlanetCore var1, BeamState var2, int var3, CubeRayCastResult var4, Timer var5) {
      if (var1.checkAttack(var2.getHandler().getBeamShooter(), true, true)) {
         if (var1.isOnServer()) {
            var1.setHitPoints(Math.max(0.0F, var1.getHitPoints() - var2.getPowerByBeamLength() * (float)var3));
            if (var1.getHitPoints() <= 0.0F) {
               var1.setDestroyed(true);
            }
         }

         return var3;
      } else {
         return 0;
      }
   }
}
