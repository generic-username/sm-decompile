package org.schema.game.common.controller;

import java.util.ArrayList;
import java.util.Collection;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.world.Segment;

public class SegmentProviderQueue extends ArrayList {
   private static final long serialVersionUID = 1L;
   private SegmentController con;
   private Vector3i tmpPos = new Vector3i();

   public SegmentProviderQueue(Collection var1, SegmentController var2) {
      super(var1);
      this.con = var2;
   }

   public SegmentProviderQueue(int var1, SegmentController var2) {
      super(var1);
      this.con = var2;
   }

   public SegmentProviderQueue(SegmentController var1) {
      this.con = var1;
   }

   public boolean add(Vector3i var1) {
      assert this.isInBound(var1);

      return super.add(var1);
   }

   protected boolean isInBound(Vector3i var1) {
      return this.con.isInbound(Segment.getSegmentIndexFromSegmentElement(var1.x, var1.y, var1.z, this.tmpPos));
   }
}
