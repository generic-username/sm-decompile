package org.schema.game.common.controller;

import org.schema.game.network.objects.remote.RemoteCompressedShopPricesBuffer;
import org.schema.game.network.objects.remote.RemoteShopOptionBuffer;
import org.schema.game.network.objects.remote.RemoteTradePriceBuffer;
import org.schema.game.network.objects.remote.RemoteTradePriceSingleBuffer;
import org.schema.schine.network.objects.remote.RemoteBooleanPrimitive;
import org.schema.schine.network.objects.remote.RemoteByteBuffer;
import org.schema.schine.network.objects.remote.RemoteLongPrimitive;

public interface ShopNetworkInterface {
   RemoteTradePriceSingleBuffer getPriceModifyBuffer();

   RemoteCompressedShopPricesBuffer getCompressedPricesUpdateBuffer();

   RemoteLongPrimitive getShopCredits();

   RemoteBooleanPrimitive getInfiniteSupply();

   RemoteBooleanPrimitive getTradeNodeOn();

   RemoteByteBuffer getTradeNodeOnRequest();

   RemoteShopOptionBuffer getShopOptionBuffer();

   RemoteTradePriceBuffer getPricesUpdateBuffer();

   RemoteLongPrimitive getTradePermission();

   RemoteLongPrimitive getLocalPermission();
}
