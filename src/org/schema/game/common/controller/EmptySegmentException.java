package org.schema.game.common.controller;

public class EmptySegmentException extends Exception {
   private static final long serialVersionUID = 1L;
   public int x;
   public int y;
   public int z;

   public EmptySegmentException(int var1, int var2, int var3) {
      this.x = var1;
      this.y = var2;
      this.z = var3;
   }
}
