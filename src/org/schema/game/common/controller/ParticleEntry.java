package org.schema.game.common.controller;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.psys.ParticleSystemConfiguration;
import org.schema.schine.resource.FileExt;

public class ParticleEntry {
   private String particleName;
   private Vector3f origin;

   public ParticleEntry() {
   }

   public ParticleEntry(String var1, Vector3f var2) {
      this.particleName = var1;
      this.origin = var2;
   }

   public void handleClient(GameClientState var1) {
      try {
         Transform var2;
         (var2 = new Transform()).origin.set(this.origin);
         FileExt var3;
         if (!(var3 = new FileExt("./data/effects/particles/" + this.particleName + ".xml")).exists()) {
            var1.getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_PARTICLEENTRY_0, this.particleName), 3.0F);
         } else {
            var1.getParticleSystemManager().startParticleSystemWorld(ParticleSystemConfiguration.fromFile(var3, false), var2);
         }
      } catch (Exception var4) {
         var4.printStackTrace();
      }
   }

   public Vector3f getOrigin() {
      return this.origin;
   }

   public void setOrigin(Vector3f var1) {
      this.origin = var1;
   }

   public String getParticleName() {
      return this.particleName;
   }

   public void setParticleName(String var1) {
      this.particleName = var1;
   }
}
