package org.schema.game.common.controller;

import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.Map.Entry;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import javax.vecmath.Vector3f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.io.SegmentSerializationBuffers;
import org.schema.game.common.controller.trade.TradeManager;
import org.schema.game.common.controller.trade.TradeNodeClient;
import org.schema.game.common.controller.trade.TradeNodeStub;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.PlayerControlledTransformableNotFound;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.inventory.NoSlotFreeException;
import org.schema.game.common.data.player.inventory.TradePricePair;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.Universe;
import org.schema.game.network.objects.TradePrice;
import org.schema.game.network.objects.TradePriceInterface;
import org.schema.game.network.objects.TradePriceSingle;
import org.schema.game.network.objects.TradePrices;
import org.schema.game.network.objects.remote.RemoteShopOption;
import org.schema.game.network.objects.remote.RemoteTradePrice;
import org.schema.game.network.objects.remote.RemoteTradePriceSingle;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.input.InputState;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.server.ServerMessage;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class ShoppingAddOn implements SerialializationInterface {
   private static final float SHOP_DISTANCE = 64.0F;
   private static final int SHOP_CHECK_PERIOD = 500;
   private static long SHOP_PRICE_CHECK_PERIOD = 900000L;
   final ShopInterface shop;
   final Short2ObjectOpenHashMap priceList;
   private final Vector3f sMin;
   private final Vector3f sMax;
   private final Vector3f oMin;
   private final Vector3f oMax;
   private final Set ownerPlayers;
   private long lastShopCheck;
   private long lastPriceCheck;
   private double basePriceMult;
   private boolean active;
   private boolean flagForcedUpdate;
   private long permissionToPurchase;
   private long permissionToTrade;
   private long lastHit;
   private long lastFleetSent;
   private Int2IntOpenHashMap strikes;
   private long credits;
   private boolean infiniteSupply;
   private boolean tradeNodeOn;
   private boolean oldVersionClearFlag;
   private boolean checkedDataBaseNode;
   private ObjectArrayFIFOQueue priceModifyBuffer;
   private ObjectArrayFIFOQueue optionBuffer;
   private ObjectArrayFIFOQueue pricesBuffer;
   public static final byte VERSION = 0;

   public ShoppingAddOn(ShopInterface var1) {
      this.priceList = new Short2ObjectOpenHashMap(ElementKeyMap.highestType);
      this.sMin = new Vector3f();
      this.sMax = new Vector3f();
      this.oMin = new Vector3f();
      this.oMax = new Vector3f();
      this.ownerPlayers = new ObjectOpenHashSet();
      this.basePriceMult = 1.0D;
      this.active = true;
      this.permissionToPurchase = TradeManager.PERM_ALL_BUT_ENEMY;
      this.permissionToTrade = TradeManager.PERM_FACTION;
      this.strikes = new Int2IntOpenHashMap();
      this.credits = 0L;
      this.priceModifyBuffer = new ObjectArrayFIFOQueue();
      this.optionBuffer = new ObjectArrayFIFOQueue();
      this.pricesBuffer = new ObjectArrayFIFOQueue();
      this.shop = var1;
      if (this.isAIShop()) {
         this.credits = ((Integer)ServerConfig.SHOP_NPC_STARTING_CREDITS.getCurrentState()).longValue();
      }

   }

   public void onHit(Damager var1) {
      if (this.isOnServer()) {
         if (this.isAIShop() && this.isActive() && var1 != null && var1.getOwnerState() != null && var1.getOwnerState() instanceof PlayerState) {
            PlayerState var2 = (PlayerState)var1.getOwnerState();
            Sector var4;
            if (var1.getState() instanceof GameServerState && (var4 = ((GameServerState)var1.getState()).getUniverse().getSector(this.shop.getSectorId())) != null && var4.isProtected()) {
               var2.sendServerMessage(new ServerMessage(new Object[]{144}, 1, var2.getId()));
               return;
            }

            if (System.currentTimeMillis() - this.lastHit > 5000L) {
               int var5 = 0;
               if (this.strikes.containsKey(var2.getId())) {
                  var5 = this.strikes.get(var2.getId());
               }

               ++var5;
               if (var5 < 3) {
                  System.err.println("STRIKES: " + var5);
                  if (var5 <= 1) {
                     var2.sendServerMessage(new ServerMessage(new Object[]{145}, 2, var2.getId()));
                  } else {
                     var2.sendServerMessage(new ServerMessage(new Object[]{146}, 3, var2.getId()));
                  }

                  this.strikes.put(var2.getId(), var5);
               } else if (System.currentTimeMillis() - this.lastFleetSent > 40000L) {
                  var2.sendServerMessage(new ServerMessage(new Object[]{147}, 3, var2.getId()));
                  this.lastFleetSent = System.currentTimeMillis();

                  try {
                     ((GameServerState)this.shop.getState()).getSimulationManager().sendToAttackSpecific(var2.getFirstControlledTransformable(), -2, 6);
                  } catch (PlayerControlledTransformableNotFound var3) {
                     var3.printStackTrace();
                  }
               } else {
                  switch(Universe.getRandom().nextInt(7)) {
                  case 0:
                     var2.sendServerMessage(new ServerMessage(new Object[]{148}, 3, var2.getId()));
                     break;
                  case 1:
                     var2.sendServerMessage(new ServerMessage(new Object[]{149}, 3, var2.getId()));
                     break;
                  case 2:
                     var2.sendServerMessage(new ServerMessage(new Object[]{150}, 3, var2.getId()));
                     break;
                  case 3:
                     var2.sendServerMessage(new ServerMessage(new Object[]{151}, 3, var2.getId()));
                     break;
                  case 4:
                     var2.sendServerMessage(new ServerMessage(new Object[]{152}, 3, var2.getId()));
                     break;
                  case 5:
                     var2.sendServerMessage(new ServerMessage(new Object[]{153}, 3, var2.getId()));
                     break;
                  case 6:
                     var2.sendServerMessage(new ServerMessage(new Object[]{154}, 3, var2.getId()));
                  }
               }

               this.lastHit = System.currentTimeMillis();
            }
         }

      }
   }

   public boolean hasPermission(AbstractOwnerState var1) {
      if (!this.getOwnerPlayers().isEmpty() && !this.getOwnerPlayers().contains(var1.getName().toLowerCase(Locale.ENGLISH))) {
         if (TradeManager.isPermission(this.shop.getPermissionToPurchase(), TradeManager.TradePerm.ENEMY)) {
            return true;
         } else {
            FactionManager var2 = ((FactionState)this.shop.getState()).getFactionManager();
            if (TradeManager.isPermission(this.shop.getPermissionToPurchase(), TradeManager.TradePerm.FACTION) && var1.getFactionId() == this.shop.getFactionId()) {
               return true;
            } else if (TradeManager.isPermission(this.shop.getPermissionToPurchase(), TradeManager.TradePerm.ALLY) && (var1.getFactionId() == this.shop.getFactionId() || var2.isFriend(var1.getFactionId(), this.shop.getFactionId()))) {
               return true;
            } else {
               return TradeManager.isPermission(this.shop.getPermissionToPurchase(), TradeManager.TradePerm.NEUTRAL) && !var2.isEnemy(this.shop.getFactionId(), var1);
            }
         }
      } else {
         return true;
      }
   }

   public boolean isAIShop() {
      return this.shop instanceof ShopSpaceStation || this.shop.getFactionId() == -2;
   }

   private int calculatePrice(ElementInformation var1) {
      assert this.shop.isOnServer();

      double var2 = (double)(Float)ServerConfig.SHOP_SELL_BUY_PRICES_LOWER_LIMIT.getCurrentState();
      double var4 = (double)(Float)ServerConfig.SHOP_SELL_BUY_PRICES_UPPER_LIMIT.getCurrentState();
      int var6 = this.shop.getShopInventory().getOverallQuantity(var1.getId());
      long var7 = var1.getPrice(((GameStateInterface)this.shop.getState()).getMaterialPrice());
      double var9;
      double var11;
      if (!ServerConfig.SHOP_USE_STATIC_SELL_BUY_PRICES.isOn()) {
         var9 = Math.pow((double)Math.max(1, this.shop.getShopInventory().getMaxStock() - var6), 0.35D) * 0.10000000149011612D * (double)var7;
         var11 = Math.max((double)var7, var9 * this.basePriceMult);
         var11 = Math.min((double)var7 * var4, Math.max(var11, (double)var7 * var2));
      } else {
         double var10000 = (double)var7;
         double var10001 = (double)var7;
         var9 = 0.0D;
         var11 = Math.max(var10000, var10001 * this.basePriceMult);
      }

      return (int)Math.min(2147483647L, (long)var11);
   }

   private void checkShopDistance(long var1, boolean var3) {
      if (var3 || var1 > this.lastShopCheck + 500L) {
         synchronized(this.shop.getState().getLocalAndRemoteObjectContainer().getLocalObjects()) {
            Iterator var4 = this.shop.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

            while(true) {
               while(true) {
                  Sendable var5;
                  do {
                     if (!var4.hasNext()) {
                        this.lastShopCheck = var1;
                        return;
                     }
                  } while(!((var5 = (Sendable)var4.next()) instanceof ShopperInterface));

                  if (this.isActive() && this.isInShopDistance((ShopperInterface)var5) && ((ShopperInterface)var5).getSectorId() == this.shop.getSectorId()) {
                     ((ShopperInterface)var5).getShopsInDistance().add(this.shop);
                  } else {
                     ((ShopperInterface)var5).getShopsInDistance().remove(this.shop);
                  }
               }
            }
         }
      }
   }

   public static boolean isTradePermShop(PlayerState var0, ShopInterface var1) {
      if (var1 != null && !var1.isAiShop()) {
         if (var1.getShopOwners().isEmpty() || var1.getShopOwners().contains(var0.getName().toLowerCase(Locale.ENGLISH))) {
            return true;
         }

         long var2;
         if ((var2 = var1.getPermissionToTrade()) == TradeManager.PERM_ALL) {
            return true;
         }

         int var5 = var1.getFactionId();
         int var4 = var0.getFactionId();
         if (TradeManager.isPermission(var2, TradeManager.TradePerm.FACTION) && var5 != 0 && var4 == var5) {
            return true;
         }

         if (TradeManager.isPermission(var2, TradeManager.TradePerm.ALLY) && ((FactionState)var0.getState()).getFactionManager().isFriend(var4, var5)) {
            return true;
         }
      }

      return false;
   }

   public void clientRequestCredits(PlayerState var1, int var2, boolean var3) {
      assert var1.isClientOwnPlayer();

      if (this.isAIShop()) {
         ((GameClientState)var1.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHOPPINGADDON_16, 0.0F);
      } else if (isTradePermShop(var1, this.shop)) {
         ShopOption var4;
         (var4 = new ShopOption()).type = var3 ? ShopOption.ShopOptionType.CREDIT_WITHDRAWAL : ShopOption.ShopOptionType.CREDIT_DEPOSIT;
         var4.credits = var3 ? (long)(-Math.abs(var2)) : (long)Math.abs(var2);
         this.shop.getNetworkObject().getShopOptionBuffer().add(new RemoteShopOption(var4, this.isOnServer()));
      } else {
         ((GameClientState)var1.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHOPPINGADDON_17, 0.0F);
      }
   }

   public static String toStringPrice(TradePriceInterface var0, int var1) {
      return var0 == null ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHOPPINGADDON_19 : StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHOPPINGADDON_20, var1 * var0.getPrice());
   }

   public String toStringForPurchase(short var1, int var2) {
      return toStringPrice(this.getPrice(var1, false), var2);
   }

   public int canAfford(PlayerState var1, short var2, int var3) {
      if (this.shop.isAiShop() || !this.shop.getShopOwners().isEmpty() && !this.shop.getShopOwners().contains(var1.getName().toLowerCase(Locale.ENGLISH))) {
         if (this.shop.getShoppingAddOn().isInfiniteSupply()) {
            return var3;
         } else if (!this.shop.getShoppingAddOn().hasPermission(var1)) {
            var1.sendServerMessage(new ServerMessage(new Object[]{155}, 3, var1.getId()));
            return 0;
         } else {
            TradePriceInterface var6;
            if ((var6 = this.getPrice(var2, false)) == null) {
               return 0;
            } else if (var6.getPrice() == 0) {
               return 0;
            } else {
               int var4 = var6.getAmount();
               if (var6.getLimit() >= 0) {
                  var4 = var6.getAmount() - var6.getLimit();
               }

               int var5 = Math.min(var4, Math.min(var3, var1.getCredits() / var6.getPrice()));
               return Math.max(0, var5);
            }
         }
      } else {
         return var3;
      }
   }

   public int canShopAfford(short var1, int var2) {
      TradePriceInterface var4 = this.getPrice(var1, true);
      if (this.shop.getShoppingAddOn().isInfiniteSupply()) {
         return var2;
      } else if (var4 == null) {
         return 0;
      } else if (var4.getPrice() == 0) {
         return 0;
      } else {
         int var3 = var2;
         if (var4.getLimit() >= 0) {
            var3 = var4.getLimit() - var4.getAmount();
         }

         int var5 = (int)Math.min((long)var3, Math.min((long)var2, this.getCredits() / (long)var4.getPrice()));
         return Math.max(0, var5);
      }
   }

   public String toStringForSale(short var1, int var2) {
      return toStringPrice(this.getPrice(var1, true), var2);
   }

   public static boolean isSelfOwnedShop(InputState var0, ShopInterface var1) {
      return var1 != null && !var1.isAiShop() && (var1.getShopOwners().isEmpty() || var1.getShopOwners().contains(((GameClientState)var0).getPlayer().getName().toLowerCase(Locale.ENGLISH)));
   }

   public static boolean isTradePermShop(GameClientState var0, ShopInterface var1) {
      if (var1 != null && !var1.isAiShop()) {
         if (var1.getShopOwners().isEmpty() || var1.getShopOwners().contains(var0.getPlayer().getName().toLowerCase(Locale.ENGLISH))) {
            return true;
         }

         long var2;
         if ((var2 = var1.getPermissionToTrade()) == TradeManager.PERM_ALL) {
            return true;
         }

         int var5 = var1.getFactionId();
         int var4 = var0.getPlayer().getFactionId();
         if (TradeManager.isPermission(var2, TradeManager.TradePerm.FACTION) && var4 == var5) {
            return true;
         }

         if (TradeManager.isPermission(var2, TradeManager.TradePerm.ALLY) && var0.getFactionManager().isFriend(var4, var5)) {
            return true;
         }
      }

      return false;
   }

   public int getPriceString(ElementInformation var1, boolean var2) {
      TradePriceInterface var3;
      return (var3 = this.getPrice(var1.getId(), !var2)) != null ? var3.getPrice() : -1;
   }

   public static TradePrice getPriceInstanceIfExisting(ShopInterface var0, short var1, boolean var2) {
      TradePriceInterface var3;
      if ((var3 = var0.getPrice(var1, var2)) != null) {
         assert var3.getType() == var1;

         return new TradePrice(var3.getType(), var3.getAmount(), var3.getPrice(), var3.getLimit(), var3.isSell());
      } else {
         return null;
      }
   }

   public static TradePrice getPriceInstance(ShopInterface var0, short var1, boolean var2) {
      boolean var3 = !var2;
      TradePriceInterface var4;
      TradePrice var5;
      if ((var4 = var0.getPrice(var1, var2)) != null) {
         assert var4.getType() == var1;

         var5 = new TradePrice(var4.getType(), var4.getAmount(), var4.getPrice(), var4.getLimit(), var4.isSell());
      } else {
         var5 = new TradePrice(var1, 0, -1, -1, var3);
      }

      return var5;
   }

   public void clientRequestDeposit(PlayerState var1, int var2) {
      this.clientRequestCredits(var1, var2, false);
   }

   public void clientRequestLocalPermission(PlayerState var1, long var2) {
      if (this.isAIShop()) {
         ((GameClientState)var1.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHOPPINGADDON_13, 0.0F);
      } else if (!this.getOwnerPlayers().isEmpty() && !this.getOwnerPlayers().contains(var1.getName().toLowerCase(Locale.ENGLISH))) {
         ((GameClientState)var1.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHOPPINGADDON_15, 0.0F);
      } else {
         ShopOption var4;
         (var4 = new ShopOption()).permission = var2;
         var4.type = ShopOption.ShopOptionType.LOCAL_PERMISSION;
         this.shop.getNetworkObject().getShopOptionBuffer().add(new RemoteShopOption(var4, this.isOnServer()));
      }
   }

   public void clientRequestTradePermission(PlayerState var1, long var2) {
      if (this.isAIShop()) {
         ((GameClientState)var1.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHOPPINGADDON_11, 0.0F);
      } else if (!this.getOwnerPlayers().isEmpty() && !this.getOwnerPlayers().contains(var1.getName().toLowerCase(Locale.ENGLISH))) {
         ((GameClientState)var1.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHOPPINGADDON_12, 0.0F);
      } else {
         ShopOption var4;
         (var4 = new ShopOption()).permission = var2;
         var4.type = ShopOption.ShopOptionType.TRADE_PERMISSION;
         this.shop.getNetworkObject().getShopOptionBuffer().add(new RemoteShopOption(var4, this.isOnServer()));
      }
   }

   public void clientRequestPlayerRemove(PlayerState var1, String var2) {
      assert var1.isClientOwnPlayer();

      assert var2 != null;

      if (this.isAIShop()) {
         ((GameClientState)var1.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHOPPINGADDON_31, 0.0F);
      } else if (!this.getOwnerPlayers().isEmpty() && !this.getOwnerPlayers().contains(var1.getName().toLowerCase(Locale.ENGLISH))) {
         ((GameClientState)var1.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHOPPINGADDON_32, 0.0F);
      } else {
         ShopOption var3;
         (var3 = new ShopOption()).playerName = var2;
         var3.type = ShopOption.ShopOptionType.USER_REMOVE;
         this.shop.getNetworkObject().getShopOptionBuffer().add(new RemoteShopOption(var3, this.isOnServer()));
      }
   }

   public void clientRequestPlayerAdd(PlayerState var1, String var2) {
      assert var1.isClientOwnPlayer();

      assert var2 != null;

      if (this.isAIShop()) {
         ((GameClientState)var1.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHOPPINGADDON_29, 0.0F);
      } else if (var2.length() <= 0) {
         ((GameClientState)var1.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHOPPINGADDON_14, 0.0F);
      } else if (!this.getOwnerPlayers().isEmpty() && !this.getOwnerPlayers().contains(var1.getName().toLowerCase(Locale.ENGLISH))) {
         ((GameClientState)var1.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHOPPINGADDON_30, 0.0F);
      } else {
         ShopOption var3;
         (var3 = new ShopOption()).playerName = var2;
         var3.type = ShopOption.ShopOptionType.USER_ADD;
         this.shop.getNetworkObject().getShopOptionBuffer().add(new RemoteShopOption(var3, this.isOnServer()));
      }
   }

   public void clientRequestSetPrice(PlayerState var1, TradePriceInterface var2) {
      assert var1.isClientOwnPlayer();

      if (this.isAIShop()) {
         ((GameClientState)var1.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHOPPINGADDON_27, 0.0F);
      } else if (isTradePermShop(var1, this.shop)) {
         TradePriceSingle var3;
         (var3 = new TradePriceSingle()).tp = var2;
         this.shop.getNetworkObject().getPriceModifyBuffer().add(new RemoteTradePriceSingle(var3, false));
      } else {
         ((GameClientState)var1.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHOPPINGADDON_28, 0.0F);
      }
   }

   public void clientRequestWithdrawal(PlayerState var1, int var2) {
      this.clientRequestCredits(var1, var2, true);
   }

   public void clearInventory(boolean var1) throws NoSlotFreeException {
      IntOpenHashSet var2 = new IntOpenHashSet();
      this.shop.getShopInventory().clear(var2);
      if (var1) {
         this.shop.getShopInventory().sendInventoryModification(var2);
      }

   }

   public void fillInventory(boolean var1, boolean var2) throws NoSlotFreeException {
      IntOpenHashSet var3 = new IntOpenHashSet();
      boolean var4 = Universe.getRandom().nextInt(5) == 0;
      if (var2) {
         this.shop.getShopInventory().clear(var3);
      }

      short[] var5;
      int var6 = (var5 = ElementKeyMap.typeList()).length;

      for(int var7 = 0; var7 < var6; ++var7) {
         Short var8;
         ElementInformation var9 = ElementKeyMap.getInfo(var8 = var5[var7]);
         int var10 = this.shop.getShopInventory().getOverallQuantity(var8);
         int var11 = this.shop.getShopInventory().getMaxStock();
         int var12 = 0;
         if (var2) {
            var12 = this.shop.getShopInventory().getMaxStock();
         } else if (var9.getType().hasParent("ship")) {
            if (Universe.getRandom().nextInt(10) == 0) {
               var12 = Universe.getRandom().nextInt(100);
            } else {
               var12 = 100 + Universe.getRandom().nextInt(10000);
            }
         } else if (var9.getType().hasParent("terrain")) {
            if (var9.getType().hasParent("mineral")) {
               if (var4) {
                  var12 = 2000 + Universe.getRandom().nextInt(10000);
               } else if (Universe.getRandom().nextInt(3) == 0) {
                  if (Universe.getRandom().nextInt(10) == 0) {
                     var12 = 500 + Universe.getRandom().nextInt(5000);
                  } else {
                     var12 = Universe.getRandom().nextInt(100);
                  }
               }
            } else {
               var12 = Universe.getRandom().nextInt(10000);
            }
         } else if (var9.getType().hasParent("spacestation")) {
            var12 = 100 + Universe.getRandom().nextInt(5000);
         } else {
            var12 = Universe.getRandom().nextInt(1000);
         }

         if ((var12 = Math.min(var11 - var10, var12)) > 0) {
            var3.add(this.shop.getShopInventory().incExistingOrNextFreeSlot(var8, var12));
         }
      }

      if (var1) {
         this.shop.getShopInventory().sendInventoryModification(var3);
      }

   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2;
      if ((var2 = var1.getStruct())[0].getType() != Tag.Type.BYTE) {
         this.fromOldTag(var1);
      } else {
         var2[0].getByte();
         this.basePriceMult = var2[1].getDouble();
         this.lastPriceCheck = var2[2].getLong();
         byte[] var6 = var2[3].getByteArray();
         this.permissionToPurchase = var2[4].getLong();
         this.credits = var2[5].getLong();
         ObjectArrayList var3;
         Tag.listFromTagStruct(var3 = new ObjectArrayList(), (Tag[])((Tag[])var2[6].getValue()));

         for(int var4 = 0; var4 < var3.size(); ++var4) {
            this.getOwnerPlayers().add(((String)var3.get(var4)).toLowerCase(Locale.ENGLISH));
         }

         this.setInfiniteSupply((Byte)var2[7].getValue() == 1);
         this.setTradeNodeOn((Byte)var2[8].getValue() == 1);
         this.permissionToTrade = var2[9].getLong();

         try {
            TradePrices var7 = deserializeTradePrices(new DataInputStream(new FastByteArrayInputStream(var6)), true);
            this.putAllPrices(var7.getPrices());
         } catch (IOException var5) {
            var5.printStackTrace();
         }
      }
   }

   public Tag toTagStructure() {
      byte[] var1 = null;

      try {
         var1 = this.getTradePrices().getPricesBytesCompressed(true);
      } catch (IOException var10) {
         var10.printStackTrace();
      }

      Tag var2 = new Tag(Tag.Type.DOUBLE, (String)null, this.getBasePriceMult());
      Tag var3 = new Tag(Tag.Type.LONG, (String)null, this.lastPriceCheck);
      Tag var11 = new Tag(Tag.Type.BYTE_ARRAY, (String)null, var1);
      Tag var4 = new Tag(Tag.Type.LONG, (String)null, this.permissionToPurchase);
      Tag var5 = new Tag(Tag.Type.LONG, (String)null, this.permissionToTrade);
      Tag var6 = new Tag(Tag.Type.LONG, (String)null, this.credits);
      ObjectOpenHashSet var7 = new ObjectOpenHashSet(this.getOwnerPlayers());
      if (FactionManager.isNPCFaction(this.shop.getFactionId())) {
         var7.removeAll(((GameStateInterface)this.getState()).getGameState().getNPCShopOwnersDebugSet());
      }

      Tag var12 = Tag.listToTagStruct((Collection)var7, Tag.Type.STRING, (String)null);
      Tag var8 = new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.infiniteSupply ? 1 : 0)));

      assert !this.shop.isNPCHomeBase() || this.isTradeNode() : this.shop + ". NPC homebase should be active trade node";

      Tag var9 = new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.isTradeNode() ? 1 : 0)));
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), var2, var3, var11, var4, var6, var12, var8, var9, var5, FinishTag.INST});
   }

   private void fromOldTag(Tag var1) {
      Tag[] var4 = var1.getStruct();
      this.fromTagStructure(var4[0], var4[1], var4[2]);
      if (var4[3].getType() == Tag.Type.BYTE) {
         byte var2 = (Byte)var4[3].getValue();
         this.setPermissionToPurchase(TradeManager.PERM_ALL);
         switch(var2) {
         case 0:
            this.setPermissionToPurchase(TradeManager.PERM_ALL);
            break;
         case 1:
            this.setPermissionToPurchase(TradeManager.PERM_ALL_BUT_ENEMY);
            break;
         case 2:
            this.setPermissionToPurchase(TradeManager.PERM_ALLIES_AND_FACTION);
            break;
         case 3:
            this.setPermissionToPurchase(TradeManager.PERM_FACTION);
         }
      } else {
         this.setPermissionToPurchase((Long)var4[3].getValue());
      }

      this.credits = var4[4].getType() == Tag.Type.LONG ? (Long)var4[4].getValue() : ((Integer)var4[4].getValue()).longValue();
      ObjectArrayList var5;
      Tag.listFromTagStruct(var5 = new ObjectArrayList(), (Tag[])((Tag[])var4[5].getValue()));

      for(int var3 = 0; var3 < var5.size(); ++var3) {
         this.getOwnerPlayers().add(((String)var5.get(var3)).toLowerCase(Locale.ENGLISH));
      }

      if (var4[6].getType() != Tag.Type.FINISH) {
         this.setInfiniteSupply((Byte)var4[6].getValue() == 1);
         if (this.isInfiniteSupply()) {
            System.err.println("[SERVER][TAG] set infinite supply on shop " + this);
         }
      }

      if (var4.length > 7 && var4[7].getType() != Tag.Type.FINISH) {
         this.setTradeNodeOn((Byte)var4[7].getValue() == 1);
         if (this.isTradeNodeOn()) {
            System.err.println("[SERVER][TAG] set trade node on shop " + this);
         }
      }

      if (var4.length > 8 && var4[8].getType() != Tag.Type.FINISH) {
         this.setPermissionToTrade((Long)var4[8].getValue());
      }

   }

   public void fromTagStructure(Tag var1, Tag var2, Tag var3) {
      this.setBasePriceMult((Double)var1.getValue());
      this.lastPriceCheck = (Long)var2.getValue();
      Tag[] var6 = (Tag[])var3.getValue();

      for(int var7 = 0; var7 < var6.length - 1; ++var7) {
         short var4;
         Tag[] var8;
         if ((var8 = (Tag[])var6[var7].getValue())[1].getType() == Tag.Type.INT) {
            var4 = (Short)var8[0].getValue();
            new OldPrice(var4, (short)-1, (Integer)var8[1].getValue(), (short)-1, (Integer)var8[1].getValue());
            this.oldVersionClearFlag = true;
         } else {
            Tag[] var5;
            if ((var5 = (Tag[])var8[1].getValue()).length > 5) {
               var4 = (Short)var8[0].getValue();
               OldPrice var9;
               (var9 = new OldPrice(var4, (Short)var5[0].getValue(), (Integer)var5[1].getValue(), (Short)var5[2].getValue(), (Integer)var5[3].getValue())).buyDownTo = (Integer)var5[4].getValue();
               var9.sellUpTo = (Integer)var5[5].getValue();
               TradePrice var11 = new TradePrice(var4, 0, var9.amountBuy, var9.buyDownTo, true);
               TradePrice var10 = new TradePrice(var4, 0, var9.amountSell, var9.sellUpTo, true);
               if (var9.priceTypeBuy == -1) {
                  this.putPrice(var11);
               }

               if (var9.priceTypeSell == -1) {
                  this.putPrice(var10);
               }
            } else if (var5[2].getType() == Tag.Type.SHORT) {
               var4 = (Short)var8[0].getValue();
               new OldPrice(var4, (Short)var5[0].getValue(), (Integer)var5[1].getValue(), (Short)var5[2].getValue(), (Integer)var5[3].getValue());
               this.oldVersionClearFlag = true;
            } else {
               var4 = (Short)var8[0].getValue();
               new OldPrice(var4, (Short)var5[0].getValue(), (Integer)var5[1].getValue(), (Short)var5[0].getValue(), (Integer)var5[1].getValue());
               this.oldVersionClearFlag = true;
            }
         }
      }

   }

   public double getBasePriceMult() {
      return this.basePriceMult;
   }

   public void setBasePriceMult(double var1) {
      this.basePriceMult = var1;
   }

   public long getCredits() {
      return this.credits;
   }

   public void setCredits(long var1) {
      this.credits = var1;
   }

   public Set getOwnerPlayers() {
      if (this.ownerPlayers.isEmpty() && FactionManager.isNPCFaction(this.shop.getFactionId())) {
         this.ownerPlayers.add(("#NPC_" + this.shop.getFactionId()).toLowerCase(Locale.ENGLISH));
         this.ownerPlayers.addAll(((GameStateInterface)this.getState()).getGameState().getNPCShopOwnersDebugSet());
      }

      return this.ownerPlayers;
   }

   public long getPermissionToPurchase() {
      return this.permissionToPurchase;
   }

   public void setPermissionToPurchase(long var1) {
      this.permissionToPurchase = var1;
   }

   public boolean isActive() {
      return this.active;
   }

   public void setActive(boolean var1) {
      this.flagForcedUpdate = var1 != this.active;
      this.active = var1;
   }

   public boolean isInShopDistance(ShopperInterface var1) {
      if (var1.getSectorId() != this.shop.getSectorId()) {
         return false;
      } else {
         var1.getTransformedAABB(this.oMin, this.oMax, 0.0F, new Vector3f(), new Vector3f(), (Transform)null);
         if (this.shop.getSegmentBuffer().isEmpty()) {
            this.oMax.sub(this.oMin);
            this.oMax.scale(0.5F);
            this.oMin.add(this.oMax);
            this.oMin.sub(this.shop.getWorldTransform().origin);
            return this.oMin.length() < 64.0F;
         } else {
            Vector3f var3 = new Vector3f(this.shop.getSegmentBuffer().getBoundingBox().min);
            Vector3f var2 = new Vector3f(this.shop.getSegmentBuffer().getBoundingBox().max);
            if (!GlUtil.checkAABB(var3, var2)) {
               this.shop.getSegmentBuffer().restructBB();
               return false;
            } else {
               AabbUtil2.transformAabb(var3, var2, 64.0F, this.shop.getWorldTransform(), this.sMin, this.sMax);
               return AabbUtil2.testAabbAgainstAabb2(this.sMin, this.sMax, this.oMin, this.oMax);
            }
         }
      }
   }

   public void modCredits(long var1) {
      long var3;
      if ((var3 = this.credits + var1) < 0L) {
         var3 = 0L;
      } else if (var3 > 2147483647L) {
         var3 = 2147483647L;
      }

      this.credits = var3;
   }

   public void onSectorInactiveClient() {
      synchronized(this.shop.getState().getLocalAndRemoteObjectContainer().getLocalObjects()) {
         Iterator var2 = this.shop.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

         while(true) {
            Sendable var3;
            do {
               if (!var2.hasNext()) {
                  return;
               }
            } while(!((var3 = (Sendable)var2.next()) instanceof ShopperInterface));

            if (this.isInShopDistance((ShopperInterface)var3) && ((ShopperInterface)var3).getSectorId() == this.shop.getSectorId()) {
               ((ShopperInterface)var3).getShopsInDistance().add(this.shop);
            } else {
               ((ShopperInterface)var3).getShopsInDistance().remove(this.shop);
            }
         }
      }
   }

   private void handleOptions() {
      if (!this.optionBuffer.isEmpty()) {
         synchronized(this.optionBuffer) {
            while(!this.optionBuffer.isEmpty()) {
               ShopOption var2 = (ShopOption)this.optionBuffer.dequeue();
               this.executeShopOption(var2);
            }

         }
      }
   }

   private void handlePrices() {
      if (!this.pricesBuffer.isEmpty()) {
         synchronized(this.pricesBuffer) {
            while(!this.pricesBuffer.isEmpty()) {
               TradePrices var2 = (TradePrices)this.pricesBuffer.dequeue();
               this.executePricesUpdate(var2);
            }

         }
      }
   }

   private void executePricesUpdate(TradePrices var1) {
      this.putAllPrices(var1.getPrices());
      if (!this.isOnServer()) {
         this.tradeNodeSettingChangedClient();
      }

   }

   private void executeShopOption(ShopOption var1) {
      PlayerState var2 = null;
      if (this.isOnServer()) {
         try {
            var2 = ((GameServerState)this.shop.getState()).getPlayerFromStateId(var1.senderId);
         } catch (PlayerNotFountException var3) {
            var3.printStackTrace();
         }

         if (var2 == null || this.isAIShop() || this.getOwnerPlayers().size() > 0 && !this.getOwnerPlayers().contains(var2.getName().toLowerCase(Locale.ENGLISH))) {
            if (var2 != null) {
               var2.sendServerMessage(new ServerMessage(new Object[]{156}, 3, var2.getId()));
            }

            return;
         }
      }

      switch(var1.type) {
      case LOCAL_PERMISSION:
         System.err.println(this.getState() + " Executed local permission " + this.shop + ": " + var1.permission);
         this.setPermissionToPurchase(var1.permission);
         break;
      case TRADE_PERMISSION:
         System.err.println(this.getState() + " Executed trade permission " + this.shop + ": " + var1.permission);
         this.setPermissionToTrade(var1.permission);
         break;
      case USER_ADD:
         this.getOwnerPlayers().add(var1.playerName.toLowerCase(Locale.ENGLISH));
         break;
      case USER_REMOVE:
         this.getOwnerPlayers().remove(var1.playerName.toLowerCase(Locale.ENGLISH));
         break;
      case CREDIT_WITHDRAWAL:
         if (this.isOnServer()) {
            if (this.getCredits() >= Math.abs(var1.credits)) {
               var2.modCreditsServer(Math.abs(var1.credits));
               this.modCredits(-Math.abs(var1.credits));
            } else {
               var2.sendServerMessage(new ServerMessage(new Object[]{157}, 3, var2.getId()));
            }
         }
         break;
      case CREDIT_DEPOSIT:
         if (this.isOnServer()) {
            if ((long)var2.getCredits() >= Math.abs(var1.credits)) {
               var2.modCreditsServer(-Math.abs(var1.credits));
               this.modCredits(Math.abs(var1.credits));
            } else {
               var2.sendServerMessage(new ServerMessage(new Object[]{158}, 3, var2.getId()));
            }
         }
      }

      if (this.shop.isOnServer()) {
         this.shop.getNetworkObject().getShopOptionBuffer().add(new RemoteShopOption(var1, this.shop.isOnServer()));
      } else {
         this.notifyClientPriceChange();
         this.tradeNodeSettingChangedClient();
      }
   }

   private void executePriceMod(TradePriceSingle var1) {
      TradePricePair var2;
      if ((var2 = (TradePricePair)this.priceList.get(var1.getType())) == null) {
         var2 = new TradePricePair();
         this.priceList.put(var1.getType(), var2);
      }

      if (var1.isBuy()) {
         var2.buy = new ShoppingAddOn.PriceRep(var1);
      } else {
         var2.sell = new ShoppingAddOn.PriceRep(var1);
      }

      if (this.shop.isOnServer()) {
         this.shop.getNetworkObject().getPriceModifyBuffer().add(new RemoteTradePriceSingle(var1, this.shop.isOnServer()));
      } else {
         this.notifyClientPriceChange();
         this.tradeNodeSettingChangedClient();
      }
   }

   private void handlePriceMods() {
      if (!this.priceModifyBuffer.isEmpty()) {
         synchronized(this.priceModifyBuffer) {
            while(!this.priceModifyBuffer.isEmpty()) {
               TradePriceSingle var2 = (TradePriceSingle)this.priceModifyBuffer.dequeue();
               this.executePriceMod(var2);
            }

         }
      }
   }

   public void receivePrices(boolean var1) {
      this.setInfiniteSupply(this.shop.getNetworkObject().getInfiniteSupply().get());
      if (!this.shop.isOnServer()) {
         boolean var2 = this.tradeNodeOn;
         this.setTradeNodeOn(this.shop.getNetworkObject().getTradeNodeOn().getBoolean());
         if (!var1 && this.isTradeNodeOn() != var2) {
            this.tradeNodeSettingChangedClient();
         }

         long var3 = this.credits;
         this.credits = this.shop.getNetworkObject().getShopCredits().getLong();
         if (var3 != this.credits) {
            this.notifyClientPriceChange();
         }
      } else {
         Iterator var9 = this.shop.getNetworkObject().getTradeNodeOnRequest().getReceiveBuffer().iterator();

         while(var9.hasNext()) {
            if ((var1 = (Byte)var9.next() == 1) != this.isTradeNodeOn()) {
               this.setTradeNodeOn(var1);
               this.tradeNodeSettingChangedServer();
            }
         }
      }

      int var8;
      for(var8 = 0; var8 < this.shop.getNetworkObject().getPricesUpdateBuffer().getReceiveBuffer().size(); ++var8) {
         TradePrices var10 = (TradePrices)((RemoteTradePrice)this.shop.getNetworkObject().getPricesUpdateBuffer().getReceiveBuffer().get(var8)).get();
         synchronized(this.pricesBuffer) {
            this.pricesBuffer.enqueue(var10);
         }
      }

      for(var8 = 0; var8 < this.shop.getNetworkObject().getShopOptionBuffer().getReceiveBuffer().size(); ++var8) {
         ShopOption var11 = (ShopOption)((RemoteShopOption)this.shop.getNetworkObject().getShopOptionBuffer().getReceiveBuffer().get(var8)).get();
         synchronized(this.optionBuffer) {
            this.optionBuffer.enqueue(var11);
         }
      }

      for(var8 = 0; var8 < this.shop.getNetworkObject().getPriceModifyBuffer().getReceiveBuffer().size(); ++var8) {
         TradePriceSingle var12 = (TradePriceSingle)((RemoteTradePriceSingle)this.shop.getNetworkObject().getPriceModifyBuffer().getReceiveBuffer().get(var8)).get();
         System.err.println("[SHOP] " + this.shop.getSegmentController() + " " + this.shop.getState() + " received price mod: " + var12);
         synchronized(this.priceModifyBuffer) {
            this.priceModifyBuffer.enqueue(var12);
         }
      }

   }

   private void notifyClientPriceChange() {
      assert !this.isOnServer();

      ClientChannel var1;
      TradeNodeClient var2;
      if ((var1 = ((GameClientState)this.shop.getState()).getController().getClientChannel()) != null && (var2 = (TradeNodeClient)var1.getGalaxyManagerClient().getTradeNodeDataById().get(this.shop.getSegmentController().getDbId())) != null) {
         var2.priceChangeListener.onChanged();
      }

   }

   public void tradeNodeSettingChangedServer() {
      assert this.isOnServer();

      if (this.shop.getSegmentController().getSystem(new Vector3i()) != null) {
         if (!this.isTradeNodeOn()) {
            ((GameServerState)this.shop.getState()).getDatabaseIndex().getTableManager().getTradeNodeTable().removeTradeNode(this.shop.getSegmentController().dbId);
         } else {
            try {
               ((GameServerState)this.shop.getState()).getDatabaseIndex().getTableManager().getTradeNodeTable().insertOrUpdateTradeNode(this.shop.getTradeNode());
            } catch (SQLException var1) {
               var1.printStackTrace();
            } catch (IOException var2) {
               var2.printStackTrace();
            }
         }

         ((GameServerState)this.shop.getState()).getUniverse().tradeNodesDirty.enqueue(this.shop.getSegmentController().getDbId());
      } else {
         System.err.println("[SHOP] Error: System could not be retrieved");
      }
   }

   public void tradeNodeSettingChangedClient() {
      assert !this.isOnServer();

   }

   public void reset() {
      System.err.println("[SHOPADDON] resetting shop " + this.shop);
      this.getOwnerPlayers().clear();
      this.setPermissionToPurchase(TradeManager.PERM_ALL);
      this.setCredits(0L);
      if (this.shop.isOnServer()) {
         IntOpenHashSet var1 = new IntOpenHashSet();
         this.shop.getShopInventory().clear(var1);
         this.shop.sendInventoryModification(var1, Long.MIN_VALUE);
         this.priceList.clear();
         this.updateServerPrices(true, false);
      }

      this.flagForcedUpdate = true;
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      deserialize(this, this.isOnServer(), var1, var2);
   }

   public static void deserialize(ShoppingAddOn var0, boolean var1, DataInput var2, int var3) throws IOException {
      TradePrices var4;
      (var4 = new TradePrices(100)).deserialize(var2, var3, var1);
      List var5 = var4.getPrices();
      var0.putAllPrices(var5);
   }

   public void putAllPrices(List var1) {
      Iterator var3 = var1.iterator();

      while(var3.hasNext()) {
         TradePriceInterface var2 = (TradePriceInterface)var3.next();
         this.putPrice(var2);
      }

   }

   public void buy(PlayerState var1, short var2, int var3, ShopInterface var4, IntOpenHashSet var5, IntOpenHashSet var6) throws NoSlotFreeException {
      assert var4.getShopInventory().checkVolume();

      boolean var7 = true;
      int var8;
      if ((var8 = var4.getShopInventory().getOverallQuantity(var2)) < var3 && !var4.getShoppingAddOn().isInfiniteSupply()) {
         var3 = Math.min(var8, var3);
         var1.sendServerMessage(new ServerMessage(new Object[]{159, var8}, 3, var1.getId()));
      }

      label77: {
         var8 = var3;
         if (!var1.getInventory().canPutIn(var2, var3)) {
            var1.sendServerMessage(new ServerMessage(new Object[]{160}, 3, var1.getId()));
         } else {
            if (!var4.isAiShop() && (var4.getShopOwners().isEmpty() || var4.getShopOwners().contains(var1.getName().toLowerCase(Locale.ENGLISH)))) {
               var7 = true;
               break label77;
            }

            TradePriceInterface var9;
            if ((var9 = this.getPrice(var2, false)) == null) {
               var1.sendServerMessage(new ServerMessage(new Object[]{161}, 3, var1.getId()));
               return;
            }

            var8 = this.canAfford(var1, var2, var3);
            if (var1.getCredits() >= var8 * var9.getPrice()) {
               var1.modCreditsServer((long)(-var8 * var9.getPrice()));
               var4.modCredits((long)(var8 * var9.getPrice()));
               break label77;
            }
         }

         var7 = false;
      }

      if (var7) {
         var4.getShopInventory().decreaseBatch(var2, var8, var6);
         int var10 = var1.getInventory().incExistingOrNextFreeSlot(var2, var8);
         var5.add(var10);

         assert var4.getShopInventory().checkVolume();

         assert var1.getInventory().checkVolume();

         assert var4.getShopInventory().slotsContaining(var2) <= 1;
      }

      assert var4.getShopInventory().checkVolume();

   }

   public void sell(PlayerState var1, short var2, int var3, ShopInterface var4, IntOpenHashSet var5, IntOpenHashSet var6) throws NoSlotFreeException {
      assert var4.getShopInventory().checkVolume();

      boolean var7 = true;
      int var8 = var1.getInventory((Vector3i)null).getOverallQuantity(var2);
      int var9 = var3;
      if (var8 < var3) {
         var9 = var8;
         var1.sendServerMessage(new ServerMessage(new Object[]{162, var8}, 3, var1.getId()));
      }

      if (!var4.isAiShop() && (var4.getShopOwners().isEmpty() || var4.getShopOwners().contains(var1.getName().toLowerCase(Locale.ENGLISH)))) {
         var7 = true;
      } else {
         TradePriceInterface var10;
         if ((var10 = this.getPrice(var2, true)) == null) {
            var1.sendServerMessage(new ServerMessage(new Object[]{163}, 3, var1.getId()));
            return;
         }

         var8 = var9 * var10.getPrice();
         if (var4.getCredits() < (long)(var9 * var10.getPrice())) {
            var1.sendServerMessage(new ServerMessage(new Object[]{164, var4.getCredits()}, 3, var1.getId()));
            var7 = false;
         } else {
            var1.modCreditsServer((long)var8);
            var4.modCredits((long)(-var8));
         }
      }

      if (var7) {
         assert var4.getShopInventory().slotsContaining(var2) <= 1;

         var1.getInventory((Vector3i)null).decreaseBatch(var2, var9, var5);
         var6.add(var4.getShopInventory().incExistingOrNextFreeSlot(var2, var9));

         assert var4.getShopInventory().slotsContaining(var2) <= 1;

         assert var4.getShopInventory().checkVolume();
      }

   }

   private void putPrice(TradePriceInterface var1) {
      TradePricePair var2;
      if ((var2 = (TradePricePair)this.priceList.get(var1.getType())) == null) {
         var2 = new TradePricePair();
         this.priceList.put(var1.getType(), var2);
      }

      if (var1.isBuy()) {
         var2.buy = new ShoppingAddOn.PriceRep(var1);
      } else {
         var2.sell = new ShoppingAddOn.PriceRep(var1);
      }
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      this.getTradePrices().serialize(var1, var2);
   }

   public boolean isOnServer() {
      return this.shop.isOnServer();
   }

   public void requestTradeNodeStateOnClient(boolean var1) {
      this.shop.getNetworkObject().getTradeNodeOnRequest().add((byte)(var1 ? 1 : 0));
   }

   public void sendPrices() {
      TradePrices var1 = this.getTradePrices();
      RemoteTradePrice var2 = new RemoteTradePrice(var1, this.isOnServer());
      this.shop.getNetworkObject().getPricesUpdateBuffer().add(var2);
   }

   public void update(long var1) {
      if (this.oldVersionClearFlag) {
         assert this.isOnServer();

         this.priceList.clear();
         this.updateServerPrices(true, false);

         assert this.isAIShop() || this.priceList.isEmpty();

         this.oldVersionClearFlag = false;
      }

      long var11;
      if (!this.checkedDataBaseNode) {
         if (this.shop.isNPCHomeBase()) {
            this.setTradeNodeOn(true);
         }

         if (this.isOnServer() && this.isTradeNodeOn()) {
            if ((var11 = ((GameServerState)this.getState()).getDatabaseIndex().getTableManager().getTradeNodeTable().getTradeNodeCredits(this.shop.getSegmentController().getDbId())) >= 0L) {
               this.credits = var11;
            }

            TradeNodeStub var5;
            try {
               if ((var5 = ((GameServerState)this.getState()).getDatabaseIndex().getTableManager().getTradeNodeTable().getTradeNode(this.shop.getSegmentController().getDbId())) != null && this.permissionToTrade != var5.getTradePermission()) {
                  this.permissionToTrade = var5.getTradePermission();
                  ShopOption var6;
                  (var6 = new ShopOption()).permission = this.permissionToTrade;
                  var6.type = ShopOption.ShopOptionType.TRADE_PERMISSION;
                  var6.permission = this.permissionToTrade;
                  this.shop.getNetworkObject().getShopOptionBuffer().add(new RemoteShopOption(var6, this.shop.isOnServer()));
               }
            } catch (SQLException var9) {
               var5 = null;
               var9.printStackTrace();
            }

            DataInputStream var13;
            if ((var13 = ((GameServerState)this.getState()).getDatabaseIndex().getTableManager().getTradeNodeTable().getTradePricesAsStream(this.shop.getSegmentController().getDbId())) != null) {
               try {
                  TradePrices var16 = deserializeTradePrices(var13, true);
                  var13.close();
                  List var12 = var16.getPrices();
                  IntOpenHashSet var4 = new IntOpenHashSet();
                  Iterator var14 = var12.iterator();

                  while(var14.hasNext()) {
                     TradePriceInterface var17 = (TradePriceInterface)var14.next();
                     int var7 = this.shop.getShopInventory().getOverallQuantity(var17.getType());
                     int var8;
                     if ((var8 = var17.getAmount()) != var7) {
                        this.shop.getShopInventory().deleteAllSlotsWithType(var17.getType(), var4);
                        this.shop.getShopInventory().putNextFreeSlotWithoutException(var17.getType(), var8, -1);
                        System.err.println("[SERVER][SHOP] applied changed inventory from shopping: " + ElementKeyMap.toString(var17.getType()) + " " + var7 + " -> " + var8);
                     }
                  }

                  if (var4.size() > 0) {
                     this.shop.getShopInventory().sendInventoryModification(var4);
                  }

                  if (FactionManager.isNPCFaction(this.shop.getFactionId())) {
                     this.priceList.clear();
                  }

                  this.putAllPrices(var12);
                  this.sendAllPrices();
               } catch (IOException var10) {
                  var10.printStackTrace();
               }
            }
         } else {
            TradeNodeStub var3;
            if (!this.isOnServer() && this.isTradeNodeOn() && (var3 = (TradeNodeStub)((GameClientState)this.getState()).getController().getClientChannel().getGalaxyManagerClient().getTradeNodeDataById().get(this.shop.getSegmentController().getDbId())) != null) {
               ((TradeNodeClient)var3).priceChangeListener.onChanged();
            }
         }

         this.checkedDataBaseNode = true;
      }

      this.handlePriceMods();
      this.handleOptions();
      this.handlePrices();
      if (this.shop.isOnServer()) {
         this.shop.getNetworkObject().getShopCredits().set(this.credits);
         this.shop.getNetworkObject().getInfiniteSupply().set(this.isInfiniteSupply());
         this.shop.getNetworkObject().getTradeNodeOn().set(this.isTradeNodeOn());
      }

      if (this.isActive() || this.flagForcedUpdate) {
         this.checkShopDistance(var1, this.flagForcedUpdate);
         if (this.shop.isOnServer() && (GameServerState.updateAllShopPricesFlag || var1 > this.lastPriceCheck + SHOP_PRICE_CHECK_PERIOD)) {
            var11 = System.currentTimeMillis();
            if (this.isAIShop()) {
               this.updateServerPrices(true, false);
            }

            long var15;
            if ((var15 = System.currentTimeMillis() - var11) > 3L) {
               System.err.println("[SERVER] updating prices for: " + this + " took " + var15);
            }

            if (this.isAIShop() && this.credits < 2147473647L) {
               this.credits += (long)(Integer)ServerConfig.SHOP_NPC_RECHARGE_CREDITS.getCurrentState();
            }

            SHOP_PRICE_CHECK_PERIOD = (long)(300000.0D + Math.random() * 10.0D * 60000.0D);
            this.lastPriceCheck = var1;
         }

         this.flagForcedUpdate = false;
      }

   }

   public StateInterface getState() {
      return this.shop.getState();
   }

   public void updateToFullNT() {
      if (this.shop.isOnServer()) {
         this.shop.getNetworkObject().getInfiniteSupply().set(this.isInfiniteSupply());
         this.shop.getNetworkObject().getTradeNodeOn().set(this.isTradeNodeOn());
         this.shop.getNetworkObject().getTradePermission().set(this.permissionToTrade);
         this.shop.getNetworkObject().getLocalPermission().set(this.permissionToPurchase);
         Iterator var1 = this.getOwnerPlayers().iterator();

         while(var1.hasNext()) {
            String var2 = (String)var1.next();
            ShopOption var3;
            (var3 = new ShopOption()).type = ShopOption.ShopOptionType.USER_ADD;
            var3.playerName = var2;
            this.shop.getNetworkObject().getShopOptionBuffer().add(new RemoteShopOption(var3, this.shop.isOnServer()));
         }

         this.sendAllPrices();
      }

   }

   void updateServerPrices(boolean var1, boolean var2) {
      assert this.shop.isOnServer();

      short[] var3;
      int var4 = (var3 = ElementKeyMap.typeList()).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         short var6;
         ElementInformation var7 = ElementKeyMap.getInfo(var6 = var3[var5]);
         if (this.isAIShop()) {
            int var10 = this.calculatePrice(var7);
            int var8 = this.shop.getShopInventory().getOverallQuantity(var6);
            TradePrice var9 = new TradePrice(var6, var8, var10, -1, true);
            TradePrice var11 = new TradePrice(var6, var8, var10, -1, false);
            TradePricePair var12 = (TradePricePair)this.priceList.get(var6);
            if (var2) {
               if (var12 == null) {
                  var12 = new TradePricePair();
                  this.priceList.put(var6, var12);
               }

               if (!var2 || var12.buy == null) {
                  var12.buy = new ShoppingAddOn.PriceRep(var11);
               }

               if (!var2 || var12.sell == null) {
                  var12.sell = new ShoppingAddOn.PriceRep(var9);
               }
            }
         }
      }

      if (var1) {
         this.sendAllPrices();
      }

   }

   private void sendAllPrices() {
      assert this.isOnServer();

      this.sendPrices();
   }

   public boolean isInfiniteSupply() {
      return this.infiniteSupply;
   }

   public void setInfiniteSupply(boolean var1) {
      this.infiniteSupply = var1;
   }

   public void cleanUp() {
      synchronized(this.shop.getState().getLocalAndRemoteObjectContainer().getLocalObjects()) {
         Iterator var2 = this.shop.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

         while(var2.hasNext()) {
            Sendable var3;
            if ((var3 = (Sendable)var2.next()) instanceof ShopperInterface) {
               ((ShopperInterface)var3).getShopsInDistance().remove(this.shop);
            }
         }

      }
   }

   public boolean isTradeNode() {
      return this.isTradeNodeOn();
   }

   public TradePrices getTradePrices() {
      TradePrices var1;
      (var1 = new TradePrices(this.priceList.size() << 1)).entDbId = this.shop.getSegmentController().getDbId();
      Iterator var2 = this.priceList.entrySet().iterator();

      while(var2.hasNext()) {
         TradePricePair var3;
         if ((var3 = (TradePricePair)((Entry)var2.next()).getValue()).buy != null) {
            var1.addBuy(var3.buy.getType(), var3.buy.getAmount(), var3.buy.getPrice(), var3.buy.getLimit());
         }

         if (var3.sell != null) {
            var1.addSell(var3.sell.getType(), var3.sell.getAmount(), var3.sell.getPrice(), var3.sell.getLimit());
         }
      }

      return var1;
   }

   public List getPricesRep() {
      ObjectArrayList var1 = new ObjectArrayList();
      Iterator var2 = this.priceList.values().iterator();

      while(var2.hasNext()) {
         TradePricePair var3;
         if ((var3 = (TradePricePair)var2.next()).buy != null) {
            var1.add(var3.buy);
         }

         if (var3.sell != null) {
            var1.add(var3.sell);
         }
      }

      return var1;
   }

   public static TradePrices deserializeTradePrices(DataInputStream var0, boolean var1) throws IOException {
      SegmentSerializationBuffers var2 = SegmentSerializationBuffers.get();
      int var3 = var0.readInt();
      int var4 = var0.readInt();
      byte[] var5 = var2.SEGMENT_BUFFER;
      byte[] var6 = var2.getStaticArray(var3);
      Inflater var7 = var2.inflater;

      TradePrices var15;
      try {
         assert var4 <= var5.length : var4 + "/" + var5.length;

         int var14 = var0.read(var5, 0, var4);

         assert var14 == var4 : var14 + "/" + var4;

         var7.reset();
         var7.setInput(var5, 0, var4);
         int var17 = var7.inflate(var6, 0, var3);

         assert var17 == var3 : var17 + " / " + var3;

         DataInputStream var16 = new DataInputStream(new FastByteArrayInputStream(var6, 0, var3));
         (var15 = new TradePrices(var4 / 128)).deserialize(var16, 0, var1);
         var16.close();
         if (var17 == 0) {
            System.err.println("[PRICES] WARNING: INFLATED BYTES 0: " + var7.needsInput() + " " + var7.needsDictionary());
         }
      } catch (DataFormatException var11) {
         var11.printStackTrace();
         throw new IOException(var11);
      } catch (IOException var12) {
         throw var12;
      } finally {
         SegmentSerializationBuffers.free(var2);
      }

      return var15;
   }

   public void serializeTradePrices(DataOutputStream var1) throws IOException {
      serializeTradePrices(var1, this.isOnServer(), this.getTradePrices(), this.shop.getSegmentController().getDbId());
   }

   public static void serializeTradePrices(DataOutputStream var0, boolean var1, TradePrices var2, long var3) throws IOException {
      SegmentSerializationBuffers var6 = SegmentSerializationBuffers.get();

      try {
         byte[] var7 = var6.SEGMENT_BUFFER;
         FastByteArrayOutputStream var5 = var6.getStaticArrayOutput();
         DataOutputStream var8 = new DataOutputStream(var5);
         var2.entDbId = var3;
         var2.serialize(var8, var1);
         var6.deflater.reset();
         var6.deflater.setInput(var5.array, 0, (int)var5.position());
         var6.deflater.finish();
         int var11 = var6.deflater.deflate(var7);
         var0.writeInt(var2.byteCount);
         var0.writeInt(var11);
         var0.write(var7, 0, var11);
      } finally {
         SegmentSerializationBuffers.free(var6);
      }

   }

   public boolean isTradeNodeOn() {
      assert !this.isOnServer() || !this.shop.isNPCHomeBase() || this.tradeNodeOn : this.shop;

      return this.tradeNodeOn;
   }

   public void setTradeNodeOn(boolean var1) {
      assert !this.isOnServer() || !this.shop.isNPCHomeBase() || var1 : this.shop + " tried to deactive NPC trade node";

      this.tradeNodeOn = var1;
   }

   public long getPermissionToTrade() {
      return this.permissionToTrade;
   }

   public void setPermissionToTrade(long var1) {
      this.permissionToTrade = var1;
   }

   public static boolean isPurchasePermission(PlayerState var0, ShopInterface var1) {
      return var1.getShoppingAddOn().hasPermission(var0);
   }

   public TradePriceInterface getPrice(short var1, boolean var2) {
      TradePricePair var3;
      if ((var3 = (TradePricePair)this.priceList.get(var1)) != null) {
         return var2 ? var3.buy : var3.sell;
      } else {
         return null;
      }
   }

   public void initFromNetwokObject(NetworkObject var1) {
      this.receivePrices(true);
      this.permissionToTrade = this.shop.getNetworkObject().getTradePermission().getLong();
      this.permissionToPurchase = this.shop.getNetworkObject().getLocalPermission().getLong();
   }

   public class PriceRep implements TradePriceInterface {
      final TradePriceInterface p;

      public PriceRep(TradePriceInterface var2) {
         assert var2 != null;

         this.p = var2;
      }

      public short getType() {
         return this.p.getType();
      }

      public int getAmount() {
         return ShoppingAddOn.this.shop.getShopInventory().getOverallQuantity(this.getType());
      }

      public int getPrice() {
         return this.p.getPrice();
      }

      public boolean isSell() {
         return this.p.isSell();
      }

      public int hashCode() {
         return (this.isSell() ? 4000 : 1) * this.getType();
      }

      public boolean equals(Object var1) {
         return ((TradePriceInterface)var1).isSell() == this.isSell() && ((TradePriceInterface)var1).getType() == this.getType() && this.getPrice() == ((TradePriceInterface)var1).getPrice() && this.getLimit() == ((TradePriceInterface)var1).getLimit();
      }

      public int getLimit() {
         return this.p.getLimit();
      }

      public ElementInformation getInfo() {
         return ElementKeyMap.getInfoFast(this.getType());
      }

      public void setAmount(int var1) {
         IntOpenHashSet var2 = new IntOpenHashSet();
         ShoppingAddOn.this.shop.getShopInventory().deleteAllSlotsWithType(this.getType(), var2);
         ShoppingAddOn.this.shop.getShopInventory().incExistingOrNextFreeSlot(this.getType(), var1, -1);
         ShoppingAddOn.this.shop.getShopInventory().sendInventoryModification(var2);
         this.p.setAmount(var1);
      }

      public boolean isBuy() {
         return this.p.isBuy();
      }

      public String toString() {
         return "PriceRep " + (this.isBuy() ? "BUY" : "SELL") + "[type: " + this.getType() + "; Price: " + this.getPrice() + "; Stock: " + this.getAmount() + "]";
      }
   }
}
