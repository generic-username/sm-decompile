package org.schema.game.common.controller;

import it.unimi.dsi.fastutil.bytes.Byte2LongOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ByteOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import java.util.Iterator;
import java.util.Map.Entry;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.network.objects.NetworkSegmentController;
import org.schema.game.network.objects.ShipKeyConfig;
import org.schema.game.network.objects.remote.RemoteShipKeyConfig;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class SlotAssignment implements TagSerializable {
   private final Byte2LongOpenHashMap slot2Pos = new Byte2LongOpenHashMap();
   private final Long2ByteOpenHashMap pos2Slot = new Long2ByteOpenHashMap();
   private final SendableSegmentController segmentController;
   private final ObjectArrayFIFOQueue receivedChanged = new ObjectArrayFIFOQueue();
   private boolean reassigned;

   public SlotAssignment(SendableSegmentController var1) {
      this.segmentController = var1;
   }

   public void modAndSend(byte var1, Vector3i var2) {
      this.modAndSend(var1, var2.x, var2.y, var2.z);
   }

   public void modAndSend(byte var1, int var2, int var3, int var4) {
      this.modAndSend(var1, ElementCollection.getIndex(var2, var3, var4));
   }

   public String toString() {
      return "[SLOTS: " + this.segmentController + ": " + this.slot2Pos + "]";
   }

   public void modAndSend(byte var1, long var2) {
      byte var4 = this.mod(var1, var2);
      this.send(var4);
      this.send(var1);
   }

   private boolean checkConsistency() {
      if (this.slot2Pos.size() != this.pos2Slot.size()) {
         System.err.println("Exception: SIZES DIFFER: " + this.slot2Pos + " ;;;;; " + this.pos2Slot);
         return false;
      } else {
         Iterator var1 = this.slot2Pos.keySet().iterator();

         byte var2;
         do {
            if (!var1.hasNext()) {
               return true;
            }

            var2 = (Byte)var1.next();
         } while(this.pos2Slot.get(this.slot2Pos.get(var2)) == var2);

         return false;
      }
   }

   public void update() {
      if (!this.receivedChanged.isEmpty()) {
         synchronized(this.receivedChanged) {
            while(!this.receivedChanged.isEmpty()) {
               ShipKeyConfig var2 = (ShipKeyConfig)this.receivedChanged.dequeue();

               assert this.checkConsistency() : var2;

               if (var2.remove) {
                  this.removeBySlot(var2.slot);
               } else {
                  this.mod(var2.slot, var2.blockPos);
               }

               assert this.checkConsistency() : var2;

               if (this.segmentController.isOnServer()) {
                  this.segmentController.getNetworkObject().slotKeyBuffer.add(new RemoteShipKeyConfig(var2, this.segmentController.isOnServer()));
               }
            }
         }
      }

      if (!this.reassigned && this.segmentController.isOnServer()) {
         long var1 = ElementCollection.getIndex(Ship.core);
         if (((ManagedSegmentController)this.segmentController).getManagerContainer().getPlayerUsable(var1) == null) {
            this.removeByPosAndSend(var1);
         }

         this.reassigned = true;
      }

   }

   public void updateFromNetworkObject(NetworkSegmentController var1) {
      for(int var2 = 0; var2 < var1.slotKeyBuffer.getReceiveBuffer().size(); ++var2) {
         ShipKeyConfig var3 = (ShipKeyConfig)((RemoteShipKeyConfig)var1.slotKeyBuffer.getReceiveBuffer().get(var2)).get();
         synchronized(this.receivedChanged) {
            this.receivedChanged.enqueue(var3);
         }
      }

   }

   public void sendAll() {
      Iterator var1 = this.slot2Pos.keySet().iterator();

      while(var1.hasNext()) {
         byte var2 = (Byte)var1.next();
         this.send(var2);
      }

   }

   private void send(byte var1) {
      if (var1 >= 0) {
         ShipKeyConfig var2;
         (var2 = new ShipKeyConfig()).slot = var1;
         if (this.slot2Pos.containsKey(var1)) {
            var2.blockPos = this.slot2Pos.get(var1);
         } else {
            var2.remove = true;
         }

         this.segmentController.getNetworkObject().slotKeyBuffer.add(new RemoteShipKeyConfig(var2, this.segmentController.isOnServer()));

         assert this.checkConsistency();
      }

   }

   private byte mod(byte var1, long var2) {
      assert this.checkConsistency();

      byte var4 = -1;
      this.slot2Pos.remove(var1);
      if (this.pos2Slot.containsKey(var2)) {
         var4 = this.pos2Slot.remove(var2);
      }

      this.pos2Slot.values().remove(var1);
      this.slot2Pos.put(var1, var2);
      this.pos2Slot.put(var2, var1);

      assert this.checkConsistency();

      return var4;
   }

   private void removeBySlot(byte var1) {
      assert this.checkConsistency();

      long var2 = this.slot2Pos.remove(var1);
      this.pos2Slot.remove(var2);

      assert this.checkConsistency();

   }

   private byte removeByPos(Vector3i var1) {
      return this.removeByPos(var1.x, var1.y, var1.z);
   }

   private byte removeByPos(int var1, int var2, int var3) {
      return this.removeByPos(ElementCollection.getIndex(var1, var2, var3));
   }

   public byte removeByPos(long var1) {
      assert this.checkConsistency();

      if (this.pos2Slot.containsKey(var1)) {
         byte var3 = this.pos2Slot.remove(var1);
         this.slot2Pos.remove(var3);

         assert this.checkConsistency();

         return var3;
      } else {
         assert this.checkConsistency();

         return -1;
      }
   }

   public void removeBySlotAndSend(byte var1) {
      if (this.hasConfigForSlot(var1)) {
         this.removeBySlot(var1);
         this.send(var1);
      }

   }

   public byte removeByPosAndSend(Vector3i var1) {
      byte var2 = this.removeByPos(var1);
      this.send(var2);
      return var2;
   }

   public byte removeByPosAndSend(int var1, int var2, int var3) {
      byte var4 = this.removeByPos(ElementCollection.getIndex(var1, var2, var3));
      this.send(var4);
      return var4;
   }

   public byte removeByPosAndSend(long var1) {
      byte var3 = this.removeByPos(var1);
      this.send(var3);
      return var3;
   }

   public boolean hasConfigForPos(Vector3i var1) {
      return this.hasConfigForPos(var1.x, var1.y, var1.z);
   }

   public boolean hasConfigForPos(int var1, int var2, int var3) {
      return this.hasConfigForPos(ElementCollection.getIndex(var1, var2, var3));
   }

   public boolean hasConfigForPos(long var1) {
      return this.pos2Slot.containsKey(var1);
   }

   public boolean hasConfigForSlot(int var1) {
      return this.slot2Pos.containsKey((byte)var1);
   }

   public Vector3i get(int var1) {
      return this.hasConfigForSlot(var1) ? ElementCollection.getPosFromIndex(this.slot2Pos.get((byte)var1), new Vector3i()) : null;
   }

   public long getAsIndex(int var1) {
      return this.hasConfigForSlot(var1) ? this.slot2Pos.get((byte)var1) : Long.MIN_VALUE;
   }

   public int getByIndex(long var1) {
      return this.hasConfigForPos(var1) ? this.pos2Slot.get(var1) : -1;
   }

   public int getByPos(Vector3i var1) {
      return this.getByIndex(ElementCollection.getIndex(var1));
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var7 = (Tag[])((Tag[])var1.getValue())[1].getValue();

      for(int var2 = 0; var2 < var7.length - 1; ++var2) {
         Tag[] var3;
         byte var4 = (Byte)(var3 = (Tag[])var7[var2].getValue())[0].getValue();
         long var5 = (Long)var3[1].getValue();
         if (this.segmentController.isLoadedFromChunk16()) {
            var5 = ElementCollection.shiftIndex(var5, 8, 8, 8);
         }

         this.slot2Pos.put(var4, var5);
         this.pos2Slot.put(var5, var4);
      }

   }

   public Tag toTagStructure() {
      Tag[] var1;
      Tag[] var10000 = var1 = new Tag[this.slot2Pos.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;
      int var2 = 0;

      for(Iterator var3 = this.slot2Pos.entrySet().iterator(); var3.hasNext(); ++var2) {
         Entry var4 = (Entry)var3.next();
         var1[var2] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, var4.getKey()), new Tag(Tag.Type.LONG, (String)null, var4.getValue()), FinishTag.INST});
      }

      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.STRUCT, (String)null, var1), FinishTag.INST});
   }
}
