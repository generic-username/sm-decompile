package org.schema.game.common.controller;

import org.schema.common.util.linAlg.Vector3i;

public class ReceivedDistribution {
   public Vector3i controllerPos;
   public long idPos;
   public int id;
   public int dist;
   public int stateId;

   public ReceivedDistribution(Vector3i var1, long var2, int var4, int var5, int var6) {
      this.controllerPos = var1;
      this.idPos = var2;
      this.id = var4;
      this.dist = var5;
      this.stateId = var6;
   }
}
