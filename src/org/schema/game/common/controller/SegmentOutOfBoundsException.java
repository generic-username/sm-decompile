package org.schema.game.common.controller;

public class SegmentOutOfBoundsException extends Exception {
   private static final long serialVersionUID = 1L;

   public SegmentOutOfBoundsException(int var1, int var2, int var3, SegmentController var4) {
      super("segment out of bounds: " + var1 + ", " + var2 + ", " + var3 + "; [32x -> " + var4.getMinPos() + " - " + var4.getMaxPos() + "] on " + var4 + " (" + var4.getState() + ")");
   }
}
