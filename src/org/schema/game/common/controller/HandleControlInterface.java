package org.schema.game.common.controller;

import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.graphicsengine.core.Timer;

public interface HandleControlInterface {
   void handleControl(ControllerStateInterface var1, Timer var2);
}
