package org.schema.game.common.starcalc;

import java.awt.EventQueue;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;
import org.schema.schine.common.language.Lng;

public class StarCalc extends JFrame {
   private static final long serialVersionUID = 1L;
   private JPanel contentPane;

   public StarCalc() {
      this.setTitle("StarCalc");
      this.setDefaultCloseOperation(2);
      this.setBounds(100, 100, 1142, 610);
      this.contentPane = new JPanel();
      this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.setContentPane(this.contentPane);
      this.contentPane.setLayout(new BoxLayout(this.contentPane, 0));
      JTabbedPane var1 = new JTabbedPane(1);
      this.contentPane.add(var1, new BoxLayout(this.contentPane, 0));
      WeaponFormulaCalculator var2 = new WeaponFormulaCalculator();
      var1.addTab(Lng.ORG_SCHEMA_GAME_COMMON_STARCALC_STARCALC_0, (Icon)null, var2, (String)null);
      var2.setLayout(new BoxLayout(var2, 0));
   }

   public static void main(String[] var0) {
      EventQueue.invokeLater(new Runnable() {
         public final void run() {
            try {
               StarCalc var1;
               (var1 = new StarCalc()).setVisible(true);
               var1.requestFocus();
            } catch (Exception var2) {
               var2.printStackTrace();
            }
         }
      });
   }
}
