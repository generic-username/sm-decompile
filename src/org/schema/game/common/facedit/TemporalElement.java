package org.schema.game.common.facedit;

public class TemporalElement {
   private short id;
   private int iconId;
   private short[] textureId;
   private TemporalFactory factory;
   private String name;

   public TemporalFactory getFactory() {
      return this.factory;
   }

   public void setFactory(TemporalFactory var1) {
      this.factory = var1;
   }

   public int getIconId() {
      return this.iconId;
   }

   public void setIconId(int var1) {
      this.iconId = var1;
   }

   public short getId() {
      return this.id;
   }

   public void setId(short var1) {
      this.id = var1;
   }

   public String getName() {
      return this.name;
   }

   public void setName(String var1) {
      this.name = var1;
   }

   public short[] getTextureId() {
      return this.textureId;
   }

   public void setTextureId(short[] var1) {
      this.textureId = var1;
   }
}
