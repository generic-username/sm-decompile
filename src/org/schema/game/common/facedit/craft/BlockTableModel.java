package org.schema.game.common.facedit.craft;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.table.AbstractTableModel;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;

public class BlockTableModel extends AbstractTableModel {
   private static final long serialVersionUID = 1L;

   public BlockTableModel(JFrame var1) {
   }

   public int getRowCount() {
      return ElementKeyMap.typeList() == null ? 0 : ElementKeyMap.typeList().length;
   }

   public int getColumnCount() {
      return 5;
   }

   public Object getValueAt(int var1, int var2) {
      ElementInformation var3 = ElementKeyMap.getInfo(ElementKeyMap.typeList()[var1]);
      (new JButton("Graph")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
         }
      });
      switch(var2) {
      case 0:
         return var3.getId();
      case 1:
         return var3.getName();
      case 2:
         return ElementInformation.rDesc[var3.blockResourceType];
      case 3:
         return var3.getPrice(false);
      case 4:
         return var3.dynamicPrice;
      default:
         return "unknown cell";
      }
   }

   public String getColumnName(int var1) {
      switch(var1) {
      case 0:
         return "ID";
      case 1:
         return "NAME";
      case 2:
         return "RESOURCE TYPE";
      case 3:
         return "FIX PRICE";
      case 4:
         return "DYN PRICE";
      default:
         return super.getColumnName(var1);
      }
   }
}
