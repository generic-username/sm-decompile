package org.schema.game.common.facedit.craft;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableRowSorter;
import org.schema.game.common.data.element.ElementKeyMap;

public class BlockTable extends JPanel {
   private static final long serialVersionUID = 1L;
   private JTable table;

   public BlockTable(final JFrame var1) {
      GridBagLayout var2;
      (var2 = new GridBagLayout()).rowWeights = new double[]{1.0D, 0.0D};
      var2.columnWeights = new double[]{1.0D};
      this.setLayout(var2);
      JScrollPane var6 = new JScrollPane();
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.fill = 1;
      var3.gridx = 0;
      var3.gridy = 0;
      this.add(var6, var3);
      final BlockTableModel var8 = new BlockTableModel(var1);
      this.table = new JTable(var8);
      final TableRowSorter var4 = new TableRowSorter();
      this.table.setRowSorter(var4);
      var4.setModel(var8);
      var6.setViewportView(this.table);
      JButton var7;
      (var7 = new JButton("Graph")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            if (BlockTable.this.table.getSelectedRow() >= 0) {
               short var3 = (Short)var8.getValueAt(var4.convertRowIndexToModel(BlockTable.this.table.getSelectedRow()), 0);
               JDialog var2;
               (var2 = new JDialog(var1)).setSize(1024, 500);
               var2.setAlwaysOnTop(true);
               var2.setTitle("Block consistence Graph");
               var2.setLayout(new BorderLayout());
               var2.getContentPane().add(ElementKeyMap.getInfo(var3).getGraph(), "Center");
               var2.setVisible(true);
            }

         }
      });
      var7.setHorizontalAlignment(4);
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).anchor = 12;
      var5.gridx = 0;
      var5.gridy = 1;
      this.add(var7, var5);
   }
}
