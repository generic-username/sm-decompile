package org.schema.game.common.facedit;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import org.schema.game.common.data.element.FactoryResource;

public class FactoryResourceEditPanel extends JPanel {
   private static final long serialVersionUID = 1L;
   private JList list;
   private ArrayListModel arrayListModel;

   public FactoryResourceEditPanel(final JFrame var1, String var2, List var3) {
      this.arrayListModel = new ArrayListModel(var3);
      this.list = new JList();
      this.setBorder(new TitledBorder((Border)null, var2, 4, 2, (Font)null, (Color)null));
      GridBagLayout var7;
      (var7 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0};
      var7.rowHeights = new int[]{0, 0, 0};
      var7.columnWeights = new double[]{1.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var7.rowWeights = new double[]{0.0D, 1.0D, Double.MIN_VALUE};
      this.setLayout(var7);
      JButton var8 = new JButton("Add");
      GridBagConstraints var9;
      (var9 = new GridBagConstraints()).anchor = 17;
      var9.insets = new Insets(0, 0, 5, 5);
      var9.gridx = 0;
      var9.gridy = 0;
      this.add(var8, var9);
      var8.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            (new FactoryResourceSingleEditDialog(var1, (FactoryResource)null, FactoryResourceEditPanel.this.arrayListModel)).setVisible(true);
         }
      });
      (var8 = new JButton("Edit")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            Iterator var2 = Arrays.asList(FactoryResourceEditPanel.this.list.getSelectedValues()).iterator();

            Object var3;
            do {
               if (!var2.hasNext()) {
                  return;
               }

               var3 = var2.next();
            } while(var1x == null);

            (new FactoryResourceSingleEditDialog(var1, (FactoryResource)var3, FactoryResourceEditPanel.this.arrayListModel)).setVisible(true);
         }
      });
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var4.gridx = 1;
      var4.gridy = 0;
      this.add(var8, var4);
      JButton var5 = new JButton("Delete");
      GridBagConstraints var10;
      (var10 = new GridBagConstraints()).anchor = 13;
      var10.weightx = 1.0D;
      var10.insets = new Insets(0, 0, 5, 0);
      var10.gridx = 2;
      var10.gridy = 0;
      this.add(var5, var10);
      var5.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            Iterator var2 = Arrays.asList(FactoryResourceEditPanel.this.list.getSelectedValues()).iterator();

            while(var2.hasNext()) {
               Object var3 = var2.next();
               if (var1 != null) {
                  FactoryResourceEditPanel.this.arrayListModel.remove((FactoryResource)var3);
               }
            }

         }
      });
      JScrollPane var6 = new JScrollPane();
      (var10 = new GridBagConstraints()).gridwidth = 3;
      var10.fill = 1;
      var10.gridx = 0;
      var10.gridy = 1;
      this.add(var6, var10);
      var6.setViewportView(this.list);
      this.list.setModel(this.arrayListModel);
   }
}
