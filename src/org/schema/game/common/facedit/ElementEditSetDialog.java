package org.schema.game.common.facedit;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;

public class ElementEditSetDialog extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private TreeSetListModel model;
   private JButton btnAdd;
   private JButton btnDelete;
   private JList list;

   public ElementEditSetDialog(final JFrame var1, final Collection var2) {
      super(var1, true);
      final HashSet var3 = new HashSet();
      Iterator var4 = var2.iterator();

      while(var4.hasNext()) {
         Short var5 = (Short)var4.next();
         var3.add(ElementKeyMap.getInfo(var5));
      }

      this.setBounds(100, 100, 450, 300);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var6;
      (var6 = new GridBagLayout()).columnWidths = new int[]{224, 150};
      var6.rowHeights = new int[]{0, 0};
      var6.columnWeights = new double[]{0.0D, 0.0D};
      var6.rowWeights = new double[]{0.0D, 0.0D};
      this.contentPanel.setLayout(var6);
      this.btnAdd = new JButton("Add");
      GridBagConstraints var8;
      (var8 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var8.gridx = 0;
      var8.gridy = 0;
      this.contentPanel.add(this.btnAdd, var8);
      this.btnDelete = new JButton("Delete");
      (var8 = new GridBagConstraints()).weightx = 1.0D;
      var8.insets = new Insets(0, 0, 5, 5);
      var8.gridx = 1;
      var8.gridy = 0;
      this.contentPanel.add(this.btnDelete, var8);
      JScrollPane var10 = new JScrollPane();
      GridBagConstraints var7;
      (var7 = new GridBagConstraints()).weighty = 1.0D;
      var7.weightx = 1.0D;
      var7.gridwidth = 2;
      var7.insets = new Insets(0, 0, 0, 5);
      var7.fill = 1;
      var7.gridx = 0;
      var7.gridy = 1;
      this.contentPanel.add(var10, var7);
      this.list = new JList();
      this.model = new TreeSetListModel();
      this.list.setModel(this.model);
      var4 = var3.iterator();

      while(var4.hasNext()) {
         this.model.add((Comparable)var4.next());
      }

      var10.setViewportView(this.list);
      JPanel var11;
      (var11 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var11, "South");
      JButton var9;
      (var9 = new JButton("OK")).setActionCommand("OK");
      var11.add(var9);
      this.getRootPane().setDefaultButton(var9);
      var9.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            var3.clear();
            var3.addAll(ElementEditSetDialog.this.model.getCollection());
            var2.clear();
            Iterator var3x = var3.iterator();

            while(var3x.hasNext()) {
               ElementInformation var2x = (ElementInformation)var3x.next();
               var2.add(var2x.getId());
            }

            ElementEditSetDialog.this.dispose();
         }
      });
      (var9 = new JButton("Cancel")).setActionCommand("Cancel");
      var9.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ElementEditSetDialog.this.dispose();
         }
      });
      var11.add(var9);
      this.btnAdd.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            (new ElementChoserDialog(var1, new ElementChoseInterface() {
               public void onEnter(ElementInformation var1x) {
                  ElementEditSetDialog.this.model.add(var1x);
               }
            })).setVisible(true);
         }
      });
      this.btnDelete.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            Object[] var4;
            if ((var4 = ElementEditSetDialog.this.list.getSelectedValues()) != null) {
               for(int var2 = 0; var2 < var4.length; ++var2) {
                  Object var3;
                  if ((var3 = var4[var2]) != null) {
                     ElementEditSetDialog.this.model.remove((ElementInformation)var3);
                  }
               }
            }

         }
      });
   }
}
