package org.schema.game.common.facedit;

import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import javax.swing.AbstractListModel;

public class TreeSetListModel extends AbstractListModel {
   private static final long serialVersionUID = 1L;
   private TreeSet treeSet;

   public TreeSetListModel() {
      this.treeSet = new TreeSet();
   }

   public TreeSetListModel(Comparator var1) {
      this.treeSet = new TreeSet(var1);
   }

   public boolean add(Comparable var1) {
      boolean var2;
      if (var2 = this.treeSet.add(var1)) {
         int var3 = this.getIndexOf(var1);
         this.fireIntervalAdded(this, var3, var3 + 1);
      }

      return var2;
   }

   public TreeSet getCollection() {
      return this.treeSet;
   }

   public int getIndexOf(Comparable var1) {
      int var2 = 0;

      for(Iterator var3 = this.treeSet.iterator(); var3.hasNext(); ++var2) {
         if (((Comparable)var3.next()).equals(var1)) {
            return var2;
         }
      }

      return -1;
   }

   public int getSize() {
      return this.treeSet.size();
   }

   public Comparable getElementAt(int var1) {
      if (var1 >= 0 && var1 < this.getSize()) {
         Iterator var5 = this.treeSet.iterator();

         for(int var3 = 0; var5.hasNext(); ++var3) {
            Comparable var4 = (Comparable)var5.next();
            if (var1 == var3) {
               return var4;
            }
         }

         return null;
      } else {
         String var2 = "index, " + var1 + ", is out of bounds for getSize() = " + this.getSize();
         throw new IllegalArgumentException(var2);
      }
   }

   public boolean remove(Comparable var1) {
      int var2;
      if ((var2 = this.getIndexOf(var1)) < 0) {
         return false;
      } else {
         boolean var3 = this.treeSet.remove(var1);
         this.fireIntervalRemoved(this, var2, var2 + 1);
         return var3;
      }
   }
}
