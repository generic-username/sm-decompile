package org.schema.game.common.facedit.model;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.w3c.dom.Node;

public class ModelCategory {
   public String name;
   public String path;
   public List models = new ObjectArrayList();
   public Node node;
   public DefaultMutableTreeNode treeNode;
   // $FF: synthetic field
   static final boolean $assertionsDisabled = !ModelCategory.class.desiredAssertionStatus();

   public String toString() {
      return this.name + "(" + this.path + ")";
   }

   public void save() throws IOException, ResourceException, ParserConfigurationException, TransformerException {
      System.err.println("Saving " + this.name + "; " + this.path);
      Iterator var1 = this.models.iterator();

      while(var1.hasNext()) {
         ((Model)var1.next()).save();
      }

      ObjectArrayList var3 = new ObjectArrayList();

      for(int var2 = 0; var2 < this.node.getChildNodes().getLength(); ++var2) {
         var3.add(this.node.getChildNodes().item(var2));
      }

      Iterator var5 = var3.iterator();

      Node var4;
      while(var5.hasNext()) {
         var4 = (Node)var5.next();
         this.node.removeChild(var4);
      }

      var5 = this.models.iterator();

      do {
         if (!var5.hasNext()) {
            return;
         }

         var4 = ((Model)var5.next()).createNode(this.node.getOwnerDocument());
         this.node.appendChild(var4);
      } while($assertionsDisabled || var4.getParentNode() == this.node);

      throw new AssertionError();
   }

   public class ModelComp implements Comparator {
      DefaultMutableTreeNode parent;

      public ModelComp(DefaultMutableTreeNode var2) {
         this.parent = var2;
      }

      public int compare(Model var1, Model var2) {
         return var1.getIndexOf(this.parent) - var2.getIndexOf(this.parent);
      }
   }
}
