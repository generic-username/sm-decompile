package org.schema.game.common.facedit.model;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.schema.common.XMLTools;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.w3c.dom.Document;

public class ModelCategories {
   public File file;
   public Document doc;
   public Object2ObjectOpenHashMap models = new Object2ObjectOpenHashMap();

   public void clear() {
      this.models.clear();
   }

   public void save() throws IOException, ResourceException, ParserConfigurationException, TransformerException {
      Iterator var1 = this.models.values().iterator();

      while(var1.hasNext()) {
         ((ModelCategory)var1.next()).save();
      }

      XMLTools.writeDocument(this.file, this.doc);
   }
}
