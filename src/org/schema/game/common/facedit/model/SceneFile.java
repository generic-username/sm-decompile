package org.schema.game.common.facedit.model;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.schema.common.XMLTools;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class SceneFile {
   public File sceneFile;
   public List sceneNodes = new ObjectArrayList();
   public Document doc;

   public SceneFile(File var1, String var2) throws SAXException, IOException, ParserConfigurationException, ResourceException {
      this.parse(var1, var2);
   }

   public SceneFile() {
   }

   public String getSceneName() {
      return this.sceneFile.getName().substring(0, this.sceneFile.getName().lastIndexOf("."));
   }

   public void parse(File var1, String var2) throws SAXException, IOException, ParserConfigurationException, ResourceException {
      this.sceneFile = var1;

      try {
         DocumentBuilder var3 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
         BufferedInputStream var4 = new BufferedInputStream(new FileInputStream(this.sceneFile));
         Document var7 = var3.parse(var4);
         this.doc = var7;
         var4.close();
         this.parseRec(var7.getDocumentElement(), var2);
      } catch (SAXException var5) {
         System.err.println("ERROR IN FILE: " + var1.getAbsolutePath());
         throw var5;
      } catch (IOException var6) {
         System.err.println("ERROR IN FILE: " + var1.getAbsolutePath());
         throw var6;
      }
   }

   private void parseRec(Node var1, String var2) throws ResourceException, IOException {
      if (var1.getNodeType() == 1) {
         if (var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("node")) {
            SceneNode var5;
            (var5 = new SceneNode(this)).parse(var1, var2);
            this.sceneNodes.add(var5);
         } else {
            NodeList var3 = var1.getChildNodes();

            for(int var4 = 0; var4 < var3.getLength(); ++var4) {
               this.parseRec(var3.item(var4), var2);
            }

         }
      }
   }

   public void save(boolean var1) throws IOException, ResourceException, ParserConfigurationException, TransformerException {
      Iterator var2 = this.sceneNodes.iterator();

      while(var2.hasNext()) {
         ((SceneNode)var2.next()).save(var1);
      }

      XMLTools.writeDocument(this.sceneFile, this.doc);
   }
}
