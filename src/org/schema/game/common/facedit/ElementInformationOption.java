package org.schema.game.common.facedit;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import it.unimi.dsi.fastutil.shorts.ShortSet;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.swing.AbstractListModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.controller.damage.effects.InterEffectHandler;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.data.blockeffects.config.ConfigGroup;
import org.schema.game.common.data.blockeffects.config.ConfigPool;
import org.schema.game.common.data.element.BlockFactory;
import org.schema.game.common.data.element.ElementCategory;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.ElementReactorChange;
import org.schema.game.common.data.element.annotation.ElemType;
import org.schema.game.common.data.element.annotation.Element;
import org.schema.game.common.data.element.annotation.NodeDependency;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.schine.common.language.Lng;

public class ElementInformationOption extends JPanel implements Comparable {
   private static final long serialVersionUID = 1L;
   ElementInformation info;
   private ApplyInterface applyInterface;
   private Field field;
   public Component editComponent;
   private int order;
   private int localOrder;
   private JPanel mainPanel;
   public ElementInformationEditPanel editPanel;
   public boolean isSourceBlock = true;

   public ElementInformationOption(JFrame var1, Field var2, short var3, int var4, ElementInformationEditPanel var5) {
      this.setBorder(new EtchedBorder(1, (Color)null, (Color)null));
      this.editPanel = var5;
      this.info = ElementKeyMap.getInfo(var3);
      this.field = var2;
      Element var9 = (Element)var2.getAnnotation(Element.class);
      this.order = var9.order();
      GridBagLayout var10;
      (var10 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var10.rowHeights = new int[]{0, 0};
      var10.columnWeights = new double[]{0.0D, 1.0D, Double.MIN_VALUE};
      var10.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      this.setLayout(var10);
      JLabel var11;
      (var11 = new JLabel(((Element)var2.getAnnotation(Element.class)).parser().tag)).setFont(new Font("Tahoma", 0, 15));
      GridBagConstraints var12;
      (var12 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var12.gridx = 0;
      var12.gridy = 0;
      this.add(var11, var12);
      this.mainPanel = new JPanel();
      (var10 = new GridBagLayout()).columnWidths = new int[]{300};
      var10.rowHeights = new int[]{0};
      var10.columnWeights = new double[]{1.0D};
      var10.rowWeights = new double[]{1.0D};
      this.mainPanel.setLayout(var10);
      GridBagConstraints var13;
      (var13 = new GridBagConstraints()).gridx = 1;
      var13.gridy = 0;
      var13.weightx = 2.0D;
      var13.anchor = 13;
      this.add(this.mainPanel, var13);

      try {
         (var13 = new GridBagConstraints()).gridx = 0;
         var13.gridy = 0;
         var13.weightx = 1.0D;
         var13.fill = 1;
         this.editComponent = this.addContent(var1, var2, var13, var5);
         if (this.applyInterface != null) {
            Component var8;
            if ((var8 = this.editComponent) instanceof JScrollPane) {
               var8 = ((JScrollPane)var8).getViewport().getComponent(0);
            }

            this.addApplyListener(var8, false);
         }

         this.mainPanel.add(this.editComponent, var13);
      } catch (IllegalArgumentException var6) {
         var6.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var6);
      } catch (IllegalAccessException var7) {
         var7.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var7);
      }
   }

   private void addApplyListener(Component var1, final boolean var2) {
      var1.addKeyListener(new KeyListener() {
         public void keyTyped(KeyEvent var1) {
         }

         public void keyPressed(KeyEvent var1) {
         }

         public void keyReleased(KeyEvent var1) {
            if (var1.getKeyCode() == 10 || var2) {
               ElementInformationOption.this.apply();
            }

         }
      });
      var1.addFocusListener(new FocusListener() {
         public void focusGained(FocusEvent var1) {
            ElementInformationOption.this.apply();
         }

         public void focusLost(FocusEvent var1) {
            ElementInformationOption.this.apply();
         }
      });
   }

   private Component addContent(final JFrame var1, final Field var2, GridBagConstraints var3, final ElementInformationEditPanel var4) throws IllegalArgumentException, IllegalAccessException {
      Class var5 = var2.getType();
      final Element var6 = (Element)var2.getAnnotation(Element.class);
      if (var2.getAnnotation(Element.class) == null) {
         return null;
      } else {
         var3.insets = new Insets(0, 50, 0, 0);
         JPanel var7;
         JButton var8;
         final JTextPane var18;
         if (var6.parser() == ElemType.NAME) {
            try {
               var7 = new JPanel();
               (var18 = new JTextPane()).setEditable(false);
               var2.get(this.info);
               var18.setText(this.info.getName());
               (var8 = new JButton("Edit")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1x) {
                     String var2;
                     if ((var2 = (String)JOptionPane.showInputDialog(var1, "Choose a name:", "Name Block", -1, (Icon)null, (Object[])null, ElementInformationOption.this.info.getName())) != null && var2.length() > 0) {
                        ElementInformationOption.this.info.name = var2;
                        var18.setText(ElementInformationOption.this.info.getName());
                        var1.repaint();
                     }

                  }
               });
               var7.add(var18);
               var7.add(var8);
               return var7;
            } catch (Exception var10) {
               var10.printStackTrace();

               assert false;
            }
         } else {
            JScrollPane var22;
            JButton var44;
            final List var50;
            if (var6 != null && var6.configGroupSet()) {
               var7 = new JPanel();
               var22 = new JScrollPane();
               var50 = (List)var2.get(this.info);
               final ElementInformationOption.StringListModel var59 = new ElementInformationOption.StringListModel(var50);
               final JList var34 = new JList(var59);
               var22.setViewportView(var34);
               (var44 = new JButton("Add")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1) {
                     ConfigPool var5 = new ConfigPool();

                     try {
                        var5.readConfigFromFile(var5.getPath(true));
                        String[] var2 = new String[var5.pool.size()];

                        for(int var3 = 0; var3 < var2.length; ++var3) {
                           var2[var3] = ((ConfigGroup)var5.pool.get(var3)).id;
                        }

                        Arrays.sort(var2);
                        String var6;
                        if ((var6 = (String)JOptionPane.showInputDialog((Component)null, "Config Group", "Select a group to add", 3, (Icon)null, var2, var2[0])) != null) {
                           var50.add(var6.toLowerCase(Locale.ENGLISH));
                           var59.setChanged();
                        }

                     } catch (Exception var4) {
                        var4.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var4);
                     }
                  }
               });
               JButton var26;
               (var26 = new JButton("Remove")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1) {
                     try {
                        Object var3;
                        if ((var3 = var34.getSelectedValue()) != null) {
                           var50.remove(var3);
                        }

                        var59.setChanged();
                     } catch (Exception var2) {
                        var2.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var2);
                     }
                  }
               });
               var7.add(var22);
               var7.add(var44);
               var7.add(var26);
               return var7;
            }

            if (var2.getName().equals("buildIconNum")) {
               var7 = new JPanel();
               (var18 = new JTextPane()).setEditable(false);
               var2.getInt(this.info);
               var18.setText(String.valueOf(this.info.getBuildIconNum()));
               (var8 = new JButton("SetToFree")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1) {
                     AddElementEntryDialog.addedBuildIcons.remove((short)ElementInformationOption.this.info.getBuildIconNum());
                     ElementInformationOption.this.info.setBuildIconToFree();
                     var18.setText(String.valueOf(ElementInformationOption.this.info.getBuildIconNum()));
                     AddElementEntryDialog.addedBuildIcons.add((short)ElementInformationOption.this.info.getBuildIconNum());
                  }
               });
               var7.add(var18);
               var7.add(var8);
               return var7;
            }

            GridBagConstraints var11;
            JPanel var53;
            GridBagLayout var54;
            if (var2.getType().equals(ElementInformation.LodCollision.class)) {
               final ElementInformation.LodCollision var62 = (ElementInformation.LodCollision)var2.get(this.info);
               var53 = new JPanel();
               (var54 = new GridBagLayout()).columnWidths = new int[]{0, 0};
               var54.rowHeights = new int[4];
               var54.columnWeights = new double[]{0.0D, 1.0D};
               var54.rowWeights = new double[4];
               var53.setLayout(var54);
               final JTextField var57 = new JTextField();
               final JComboBox var32 = new JComboBox();
               final JComboBox var48 = new JComboBox();
               final JComboBox var13 = new JComboBox();

               for(int var17 = 0; var17 < ElementInformation.LodCollision.LodCollisionType.values().length; ++var17) {
                  ElementInformation.LodCollision.LodCollisionType var60 = ElementInformation.LodCollision.LodCollisionType.values()[var17];
                  var13.addItem(var60);
                  if (var62.type == var60) {
                     var13.setSelectedIndex(var17);
                  }
               }

               var13.setEditable(var6.editable());
               var13.addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1) {
                     try {
                        ElementInformation.LodCollision.LodCollisionType var3 = (ElementInformation.LodCollision.LodCollisionType)var13.getSelectedItem();
                        var62.type = var3;
                        switch(var62.type) {
                        case BLOCK_TYPE:
                           var57.setEditable(false);
                           var32.setEnabled(true);
                           var48.setEnabled(true);
                           var62.modelId = null;
                           var57.setText(var62.modelId != null ? var62.modelId : "");
                           return;
                        case CONVEX_HULL:
                           var57.setEditable(true);
                           var32.setEnabled(false);
                           var48.setEnabled(false);
                           return;
                        default:
                           throw new RuntimeException("UNKNOWN TYPE: " + var62.type.name());
                        }
                     } catch (IllegalArgumentException var2) {
                        var2.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var2);
                     }
                  }
               });
               GridBagConstraints var19;
               (var19 = new GridBagConstraints()).weightx = 1.0D;
               var19.gridx = 0;
               var19.gridy = 0;
               var53.add(var13, var19);

               int var14;
               for(var14 = 0; var14 < BlockStyle.values().length; ++var14) {
                  BlockStyle var21 = BlockStyle.values()[var14];
                  var32.addItem(var21);
                  if (var62.blockTypeToEmulate == var21) {
                     var32.setSelectedIndex(var14);
                  }
               }

               var32.setEditable(var6.editable());
               var32.addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1) {
                     try {
                        BlockStyle var3 = (BlockStyle)var32.getSelectedItem();
                        var62.blockTypeToEmulate = var3;
                     } catch (IllegalArgumentException var2) {
                        var2.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var2);
                     }
                  }
               });
               (var11 = new GridBagConstraints()).weightx = 1.0D;
               var11.gridx = 0;
               var11.gridy = 1;
               var53.add(var32, var11);

               for(var14 = 0; var14 < ElementInformation.slabStrings.length; ++var14) {
                  String var23 = ElementInformation.slabStrings[var14];
                  var48.addItem(var23);
                  if (var62.colslab == var14) {
                     var48.setSelectedIndex(var14);
                  }
               }

               var48.setEditable(var6.editable());
               var48.addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1) {
                     try {
                        String var4 = (String)var48.getSelectedItem();

                        for(int var2 = 0; var2 < ElementInformation.slabStrings.length; ++var2) {
                           if (ElementInformation.slabStrings[var2].equals(var4)) {
                              var62.colslab = var2;
                              return;
                           }
                        }

                     } catch (IllegalArgumentException var3) {
                        var3.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var3);
                     }
                  }
               });
               (var11 = new GridBagConstraints()).weightx = 1.0D;
               var11.gridx = 0;
               var11.gridy = 2;
               var53.add(var48, var11);
               var57.setText(var62.modelId != null ? var62.modelId : "");
               var57.setEditable(var62.type == ElementInformation.LodCollision.LodCollisionType.CONVEX_HULL);
               this.applyInterface = new ApplyInterface() {
                  public void afterApply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException {
                  }

                  public void apply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException {
                     var62.modelId = var57.getText().trim();
                     if (var62.modelId.length() == 0) {
                        var62.modelId = null;
                     }

                     System.err.println("APPLY TEXT");
                  }
               };
               (var11 = new GridBagConstraints()).fill = 2;
               var11.weightx = 1.0D;
               var11.gridx = 0;
               var11.gridy = 3;
               var53.add(var57, var11);
               this.addApplyListener(var57, true);
               switch(var62.type) {
               case BLOCK_TYPE:
                  var57.setEditable(false);
                  var32.setEnabled(true);
                  var48.setEnabled(true);
                  var62.modelId = null;
                  var57.setText(var62.modelId != null ? var62.modelId : "");
                  break;
               case CONVEX_HULL:
                  var57.setEditable(true);
                  var32.setEnabled(false);
                  var48.setEnabled(false);
                  break;
               default:
                  throw new RuntimeException("UNKNOWN TYPE: " + var62.type.name());
               }

               return var53;
            }

            if (var2.getType().equals(InterEffectSet.class)) {
               final InterEffectSet var61 = (InterEffectSet)var2.get(this.info);
               var53 = new JPanel();
               (var54 = new GridBagLayout()).columnWidths = new int[]{0, 0};
               var54.rowHeights = new int[InterEffectHandler.InterEffectType.values().length];
               var54.columnWeights = new double[]{0.0D, 1.0D};
               var54.rowWeights = new double[InterEffectHandler.InterEffectType.values().length];
               var53.setLayout(var54);
               final ObjectArrayList var55 = new ObjectArrayList();

               for(int var31 = 0; var31 < InterEffectHandler.InterEffectType.values().length; ++var31) {
                  final InterEffectHandler.InterEffectType var47 = InterEffectHandler.InterEffectType.values()[var31];
                  (var11 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
                  var11.gridx = 0;
                  var11.gridy = var31;
                  JLabel var15 = new JLabel(var47.id);
                  var53.add(var15, var11);
                  GridBagConstraints var58;
                  (var58 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
                  var58.gridx = 1;
                  var58.gridy = var31;
                  var58.fill = 2;
                  final JTextField var12;
                  (var12 = new JTextField()).setText(String.valueOf(var61.getStrength(var47)));
                  ApplyInterface var16 = new ApplyInterface() {
                     public void afterApply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException {
                        var12.setText(String.valueOf(var61.getStrength(var47)));
                     }

                     public void apply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException {
                        float var3 = Float.parseFloat(var12.getText());
                        var61.setStrength(var47, var3);
                        System.err.println("APPLIED SET: " + var61);
                     }
                  };
                  var55.add(var16);
                  var53.add(var12, var58);
                  this.addApplyListener(var12, false);
               }

               this.applyInterface = new ApplyInterface() {
                  public void afterApply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException {
                     Iterator var3 = var55.iterator();

                     while(var3.hasNext()) {
                        ((ApplyInterface)var3.next()).afterApply(var1, var2);
                     }

                  }

                  public void apply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException {
                     Iterator var3 = var55.iterator();

                     while(var3.hasNext()) {
                        ((ApplyInterface)var3.next()).apply(var1, var2);
                     }

                  }
               };
               return var53;
            }

            short[] var43;
            if (var2.getName().equals("textureId")) {
               var7 = new JPanel();
               var18 = new JTextPane();
               var43 = (short[])var2.get(this.info);
               var18.setText(Arrays.toString(var43));
               (var8 = new JButton("Edit")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1x) {
                     try {
                        TextureChoserDialog var10000 = new TextureChoserDialog(var1, ElementInformationOption.this.info, new ExecuteInterface() {
                           public void execute() {
                              Object var1x;
                              try {
                                 short[] var4x = (short[])var2.get(ElementInformationOption.this.info);
                                 var18.setText(Arrays.toString(var4x));
                              } catch (IllegalArgumentException var2x) {
                                 var1x = null;
                                 var2x.printStackTrace();
                              } catch (IllegalAccessException var3) {
                                 var1x = null;
                                 var3.printStackTrace();
                              }

                              var4.updateTextures();
                           }
                        });
                        var1x = null;
                        var10000.setVisible(true);
                     } catch (IllegalArgumentException var2x) {
                        var2x.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var2x);
                     }
                  }
               });
               var7.add(var18);
               var7.add(var8);
               return var7;
            }

            final JTextField var33;
            if (var5.equals(Class.class)) {
               (var33 = new JTextField()).setText(((ElementCategory)var2.get(this.info)).getCategory());
               var33.setEditable(false);
               return var33;
            }

            JButton var56;
            if (var6.elementSet()) {
               (var56 = new JButton("Edit")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1x) {
                     try {
                        ElementEditSetDialog var10000 = new ElementEditSetDialog(var1, (Set)var2.get(ElementInformationOption.this.info));
                        var1x = null;
                        var10000.setVisible(true);
                     } catch (IllegalArgumentException var2x) {
                        var2x.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var2x);
                     } catch (IllegalAccessException var3) {
                        var3.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var3);
                     }
                  }
               });
               return var56;
            }

            if (var6 != null && var6.consistence()) {
               return new FactoryResourceEditPanel(var1, "Consistence Resource List", this.info.getConsistence());
            }

            JButton var27;
            if (var6 != null && var6.selectBlock()) {
               var7 = new JPanel();
               var18 = new JTextPane();
               ElementInformation var52;
               if (this.field.equals(Short.TYPE)) {
                  var52 = ElementKeyMap.getInfoFast(var2.getShort(this.info));
               } else {
                  var52 = ElementKeyMap.getInfoFast(var2.getInt(this.info));
               }

               if (var52 != null) {
                  var18.setText(var52.getName() + "(" + var52.id + ")");
               } else {
                  var18.setText("   -   ");
               }

               (var8 = new JButton("Set")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1x) {
                     try {
                        ElementChoserDialog var10000 = new ElementChoserDialog(var1, new ElementChoseInterface() {
                           public void onEnter(ElementInformation var1x) {
                              try {
                                 if (ElementInformationOption.this.field.equals(Short.TYPE)) {
                                    ElementInformationOption.this.field.setShort(ElementInformationOption.this.info, var1x.id);
                                 } else {
                                    ElementInformationOption.this.field.setInt(ElementInformationOption.this.info, var1x.id);
                                 }
                              } catch (IllegalArgumentException var2x) {
                                 var2x.printStackTrace();
                              } catch (IllegalAccessException var3) {
                                 var3.printStackTrace();
                              }
                           }
                        });
                        var1x = null;
                        var10000.setVisible(true);
                     } catch (Exception var4) {
                        var4.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var4);
                     }

                     try {
                        ElementInformation var5;
                        if (ElementInformationOption.this.field.equals(Short.TYPE)) {
                           var5 = ElementKeyMap.getInfoFast(var2.getShort(ElementInformationOption.this.info));
                        } else {
                           var5 = ElementKeyMap.getInfoFast(var2.getInt(ElementInformationOption.this.info));
                        }

                        if (var5 != null) {
                           var18.setText(var5.getName() + "(" + var5.id + ")");
                        } else {
                           var18.setText("   -   ");
                        }
                     } catch (IllegalArgumentException var2x) {
                        var2x.printStackTrace();
                     } catch (IllegalAccessException var3) {
                        var3.printStackTrace();
                     }
                  }
               });
               (var27 = new JButton("Clear")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1) {
                     try {
                        if (ElementInformationOption.this.field.equals(Short.TYPE)) {
                           ElementInformationOption.this.field.setShort(ElementInformationOption.this.info, (short)0);
                        } else {
                           ElementInformationOption.this.field.setInt(ElementInformationOption.this.info, 0);
                        }

                        var18.setText("   -   ");
                     } catch (IllegalArgumentException var2) {
                        var2.printStackTrace();
                     } catch (IllegalAccessException var3) {
                        var3.printStackTrace();
                     }
                  }
               });
               var7.add(var18);
               if (var6.editable()) {
                  var7.add(var8);
                  var7.add(var27);
               }

               return var7;
            }

            if (var6 != null && var6.cubatomConsistence()) {
               return new FactoryResourceEditPanel(var1, "Cubatom Consistence Resource List", this.info.cubatomConsistence);
            }

            if (var6 != null && var6.stringSet()) {
               var7 = new JPanel();
               var18 = new JTextPane();
               var50 = (List)var2.get(this.info);
               var18.setText(var50.toString());
               (var8 = new JButton("Add")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1x) {
                     try {
                        String var3;
                        if ((var3 = JOptionPane.showInputDialog(var1, "Add Value")) != null) {
                           var50.add(var3.toLowerCase(Locale.ENGLISH));
                           var2.set(ElementInformationOption.this.info, var50);
                           var18.setText(var50.toString());
                        }

                     } catch (Exception var2x) {
                        var2x.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var2x);
                     }
                  }
               });
               (var27 = new JButton("Clear")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1) {
                     try {
                        if (var50 != null) {
                           List var3;
                           (var3 = (List)var2.get(ElementInformationOption.this.info)).clear();
                           var18.setText(var3.toString());
                        }

                     } catch (Exception var2x) {
                        var2x.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var2x);
                     }
                  }
               });
               var7.add(var18);
               var7.add(var8);
               var7.add(var27);
               return var7;
            }

            if (var5.equals(ElementReactorChange.class)) {
               (var56 = new JButton("Edit")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1x) {
                     try {
                        var2.set(ElementInformationOption.this.info, new ElementReactorChange(ElementInformationOption.this.info));
                        ((ElementReactorChange)var2.get(ElementInformationOption.this.info)).openDialog(var1);
                     } catch (Exception var2x) {
                        var2x.printStackTrace();
                     }
                  }
               });
               return var56;
            }

            if (var5.equals(List.class)) {
               (var56 = new JButton("Edit")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1x) {
                     try {
                        ElementEditSetDialog var10000 = new ElementEditSetDialog(var1, (List)var2.get(ElementInformationOption.this.info));
                        var1x = null;
                        var10000.setVisible(true);
                     } catch (IllegalArgumentException var2x) {
                        var2x.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var2x);
                     } catch (IllegalAccessException var3) {
                        var3.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var3);
                     }
                  }
               });
               return var56;
            }

            if (var5.equals(BlockFactory.class)) {
               var7 = new JPanel();
               var18 = new JTextPane();
               Object var49;
               if ((var49 = var2.get(this.info)) != null) {
                  BlockFactory var51 = (BlockFactory)var49;
                  var18.setText(var51.toString());
               } else {
                  var18.setText("   -   ");
               }

               (var8 = new JButton("Edit")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1x) {
                     try {
                        ElementEditFactoryProductsDialog var10000 = new ElementEditFactoryProductsDialog(var1, ElementInformationOption.this.info, new ExecuteInterface() {
                           public void execute() {
                              try {
                                 Object var1x;
                                 if ((var1x = var2.get(ElementInformationOption.this.info)) != null) {
                                    BlockFactory var4 = (BlockFactory)var1x;
                                    var18.setText(var4.toString());
                                 } else {
                                    var18.setText("   -   ");
                                 }
                              } catch (IllegalArgumentException var2x) {
                                 var2x.printStackTrace();
                                 GuiErrorHandler.processErrorDialogException(var2x);
                              } catch (IllegalAccessException var3) {
                                 var3.printStackTrace();
                                 GuiErrorHandler.processErrorDialogException(var3);
                              }
                           }
                        });
                        var1x = null;
                        var10000.setVisible(true);
                     } catch (IllegalArgumentException var2x) {
                        var2x.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var2x);
                     }
                  }
               });
               var7.add(var18);
               var7.add(var8);
               return var7;
            }

            if (var2.getName().toLowerCase(Locale.ENGLISH).equals("slabids")) {
               var7 = new JPanel();
               var18 = new JTextPane();
               if ((var43 = (short[])var2.get(this.info)) != null) {
                  var18.setText(Arrays.toString(var43));
               } else {
                  var18.setText("   -   ");
               }

               (var8 = new JButton("Create")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1x) {
                     try {
                        ElementKeyMap.deleteBlockSlabs(ElementInformationOption.this.info);
                        ElementKeyMap.createBlockSlabs(ElementInformationOption.this.info);
                        short[] var3;
                        if ((var3 = (short[])var2.get(ElementInformationOption.this.info)) != null) {
                           var18.setText(Arrays.toString(var3));
                        } else {
                           var18.setText("   -   ");
                        }

                        ((ElementEditorFrame)var1).reinitializeElements();
                     } catch (Exception var2x) {
                        var2x.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var2x);
                     }
                  }
               });
               (var27 = new JButton("Delete")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1x) {
                     try {
                        if (ElementInformationOption.this.info.slabIds != null) {
                           ElementKeyMap.deleteBlockSlabs(ElementInformationOption.this.info);
                           short[] var3;
                           if ((var3 = (short[])var2.get(ElementInformationOption.this.info)) != null) {
                              var18.setText(Arrays.toString(var3));
                           } else {
                              var18.setText("   -   ");
                           }

                           ((ElementEditorFrame)var1).reinitializeElements();
                        }

                     } catch (Exception var2x) {
                        var2x.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var2x);
                     }
                  }
               });
               var7.add(var18);
               if (var6.editable()) {
                  var7.add(var8);
                  var7.add(var27);
               }

               return var7;
            }

            if (var2.getName().toLowerCase(Locale.ENGLISH).equals("styleids")) {
               var7 = new JPanel();
               var18 = new JTextPane();
               if ((var43 = (short[])var2.get(this.info)) != null) {
                  var18.setText(Arrays.toString(var43));
               } else {
                  var18.setText("   -   ");
               }

               (var8 = new JButton("Generate by Name")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1x) {
                     try {
                        ElementKeyMap.createBlockStyleReferencesFromName(ElementInformationOption.this.info);
                        short[] var3;
                        if ((var3 = (short[])var2.get(ElementInformationOption.this.info)) != null) {
                           var18.setText(Arrays.toString(var3));
                        } else {
                           var18.setText("   -   ");
                        }

                        ((ElementEditorFrame)var1).reinitializeElements();
                     } catch (Exception var2x) {
                        var2x.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var2x);
                     }
                  }
               });
               (var27 = new JButton("Delete")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1x) {
                     try {
                        if (ElementInformationOption.this.info.styleIds != null) {
                           ElementKeyMap.deleteBlockStyleReferences(ElementInformationOption.this.info);
                           short[] var3;
                           if ((var3 = (short[])var2.get(ElementInformationOption.this.info)) != null) {
                              var18.setText(Arrays.toString(var3));
                           } else {
                              var18.setText("   -   ");
                           }

                           ((ElementEditorFrame)var1).reinitializeElements();
                        }

                     } catch (Exception var2x) {
                        var2x.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var2x);
                     }
                  }
               });
               var7.add(var18);
               if (var6.editable()) {
                  var7.add(var8);
                  var7.add(var27);
               }

               return var7;
            }

            if (var2.getName().toLowerCase(Locale.ENGLISH).equals("wildcardids")) {
               var7 = new JPanel();
               var18 = new JTextPane();
               if ((var43 = (short[])var2.get(this.info)) != null) {
                  var18.setText(Arrays.toString(var43));
               } else {
                  var18.setText("   -   ");
               }

               (var8 = new JButton("Add")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1x) {
                     try {
                        ElementChoserDialog var10000 = new ElementChoserDialog(var1, new ElementChoseInterface() {
                           public void onEnter(ElementInformation var1x) {
                              ShortArrayList var2x = new ShortArrayList();
                              short[] var3;
                              if (ElementInformationOption.this.info.wildcardIds != null) {
                                 int var4 = (var3 = ElementInformationOption.this.info.wildcardIds).length;

                                 for(int var5 = 0; var5 < var4; ++var5) {
                                    short var6 = var3[var5];
                                    var2x.add(var6);
                                 }
                              }

                              var1x.setSourceReference(ElementInformationOption.this.info.id);
                              var2x.add(var1x.id);
                              ElementInformationOption.this.info.wildcardIds = new short[var2x.size()];

                              for(int var9 = 0; var9 < var2x.size(); ++var9) {
                                 ElementInformationOption.this.info.wildcardIds[var9] = var2x.getShort(var9);
                              }

                              try {
                                 if ((var3 = (short[])var2.get(ElementInformationOption.this.info)) != null) {
                                    var18.setText(Arrays.toString(var3));
                                 } else {
                                    var18.setText("   -   ");
                                 }
                              } catch (IllegalArgumentException var7) {
                                 var7.printStackTrace();
                              } catch (IllegalAccessException var8) {
                                 var8.printStackTrace();
                              }

                              ((ElementEditorFrame)var1).reinitializeElements();
                           }
                        });
                        var1x = null;
                        var10000.setVisible(true);
                     } catch (Exception var2x) {
                        var2x.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var2x);
                     }
                  }
               });
               (var27 = new JButton("Create from inventory group")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1x) {
                     try {
                        ElementKeyMap.createBlockStyleReferencesFromInvGroup(ElementInformationOption.this.info);
                        short[] var3;
                        if ((var3 = (short[])var2.get(ElementInformationOption.this.info)) != null) {
                           var18.setText(Arrays.toString(var3));
                        } else {
                           var18.setText("   -   ");
                        }

                        ((ElementEditorFrame)var1).reinitializeElements();
                     } catch (Exception var2x) {
                        var2x.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var2x);
                     }
                  }
               });
               (var44 = new JButton("Clear")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1x) {
                     try {
                        if (ElementInformationOption.this.info.wildcardIds != null) {
                           ElementKeyMap.deleteWildCardReferences(ElementInformationOption.this.info);
                           short[] var3;
                           if ((var3 = (short[])var2.get(ElementInformationOption.this.info)) != null) {
                              var18.setText(Arrays.toString(var3));
                           } else {
                              var18.setText("   -   ");
                           }

                           ((ElementEditorFrame)var1).reinitializeElements();
                        }

                     } catch (Exception var2x) {
                        var2x.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var2x);
                     }
                  }
               });
               var7.add(var18);
               if (var6.editable()) {
                  var7.add(var8);
                  var7.add(var27);
                  var7.add(var44);
               }

               return var7;
            }

            if (var6 != null && var6.shortSet()) {
               var7 = new JPanel();
               var18 = new JTextPane();
               final ShortSet var41 = (ShortSet)var2.get(this.info);
               var18.setText(var41.toString());
               (var8 = new JButton("Add")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1x) {
                     try {
                        ElementChoserDialog var10000 = new ElementChoserDialog(var1, new ElementChoseInterface() {
                           public void onEnter(ElementInformation var1x) {
                              var41.add(var1x.id);
                              var18.setText(var41.toString());
                           }
                        });
                        var1x = null;
                        var10000.setVisible(true);
                     } catch (Exception var2) {
                        var2.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var2);
                     }
                  }
               });
               (var27 = new JButton("Remove")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1x) {
                     try {
                        ElementChoserDialog var10000 = new ElementChoserDialog(var1, new ElementChoseInterface() {
                           public void onEnter(ElementInformation var1x) {
                              var41.remove(var1x.id);
                              var18.setText(var41.toString());
                           }
                        });
                        var1x = null;
                        var10000.setVisible(true);
                     } catch (Exception var2) {
                        var2.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var2);
                     }
                  }
               });
               (var44 = new JButton("Clear")).addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1) {
                     try {
                        var41.clear();
                        var18.setText(var41.toString());
                     } catch (Exception var2) {
                        var2.printStackTrace();
                        GuiErrorHandler.processErrorDialogException(var2);
                     }
                  }
               });
               var7.add(var18);
               if (var6.editable()) {
                  var7.add(var8);
                  var7.add(var27);
                  var7.add(var44);
               }

               return var7;
            }

            if (var5.equals(String.class)) {
               if (var6 != null) {
                  if (var6.textArea()) {
                     final JTextArea var35;
                     (var35 = new JTextArea()).setEditable(var6.editable());
                     var35.setText(var2.get(this.info).toString().replace("\t", ""));
                     var3.insets = new Insets(0, 50, 0, 0);
                     this.applyInterface = new ApplyInterface() {
                        public void afterApply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException {
                        }

                        public void apply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException {
                           var1.set(var2, var35.getText());
                        }
                     };
                     (var22 = new JScrollPane(var35)).setPreferredSize(new Dimension(640, 200));
                     return var22;
                  }

                  (var33 = new JTextField()).setEditable(var6.editable());
                  var33.setText(var2.get(this.info).toString());
                  this.applyInterface = new ApplyInterface() {
                     public void afterApply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException {
                        var33.setText(var1.get(var2).toString());
                     }

                     public void apply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException {
                        var1.set(var2, var33.getText());
                     }
                  };
                  if (var6.modelSelect()) {
                     JButton var20;
                     (var20 = new JButton("select")).addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent var1x) {
                           String var4;
                           if ((var4 = ElementEditorFrame.openSelectModelDialog(var1, var6.modelSelectFilter())) != null) {
                              var33.setText(var4.trim());

                              try {
                                 ElementInformationOption.this.applyInterface.apply(var2, ElementInformationOption.this.info);
                                 return;
                              } catch (IllegalArgumentException var2x) {
                                 var2x.printStackTrace();
                                 return;
                              } catch (IllegalAccessException var3) {
                                 var3.printStackTrace();
                              }
                           }

                        }
                     });
                     JPanel var29 = new JPanel();
                     GridBagConstraints var38;
                     (var38 = new GridBagConstraints()).gridx = 0;
                     var38.gridy = 0;
                     var38.fill = 2;
                     GridBagConstraints var24;
                     (var24 = new GridBagConstraints()).gridx = 1;
                     var24.gridy = 0;
                     GridBagLayout var9;
                     (var9 = new GridBagLayout()).columnWidths = new int[]{0, 0};
                     var9.rowHeights = new int[]{0};
                     var9.columnWeights = new double[]{1.0D, 0.0D, Double.MIN_VALUE};
                     var9.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
                     var29.setLayout(var9);
                     var29.add(var33, var38);
                     var29.add(var20, var24);
                     return var29;
                  }

                  return var33;
               }
            } else {
               final JColorChooser var37;
               final float[] var40;
               if (var5.equals(Vector3f.class)) {
                  var7 = new JPanel();
                  final Vector3f var45 = (Vector3f)var2.get(this.info);
                  var37 = new JColorChooser(new Color(var45.x, var45.y, var45.z, 1.0F));
                  var7.add(var37);
                  var40 = new float[4];
                  var37.getSelectionModel().addChangeListener(new ChangeListener() {
                     public void stateChanged(ChangeEvent var1) {
                        var37.getColor().getComponents(var40);
                        Vector3f var2;
                        (var2 = new Vector3f()).x = var40[0];
                        var2.y = var40[1];
                        var2.z = var40[2];
                        var45.set(var2);
                     }
                  });
                  return var7;
               }

               if (var5.equals(Vector4f.class)) {
                  var7 = new JPanel();
                  final Vector4f var42 = (Vector4f)var2.get(this.info);
                  var37 = new JColorChooser(new Color(var42.x, var42.y, var42.z, var42.w));
                  var7.add(var37);
                  var40 = new float[4];
                  var37.getSelectionModel().addChangeListener(new ChangeListener() {
                     public void stateChanged(ChangeEvent var1) {
                        var37.getColor().getComponents(var40);
                        Vector4f var2;
                        (var2 = new Vector4f()).x = var40[0];
                        var2.y = var40[1];
                        var2.z = var40[2];
                        var2.w = var40[3];
                        var42.set(var2);
                     }
                  });
                  return var7;
               }

               final JComboBox var46;
               if (var5.equals(Boolean.TYPE)) {
                  (var46 = new JComboBox()).addItem(false);
                  var46.addItem(true);
                  var46.setEditable(var6.editable());
                  var46.setSelectedItem(var2.getBoolean(this.info));
                  var46.addActionListener(new ActionListener() {
                     public void actionPerformed(ActionEvent var1x) {
                        try {
                           Element var10000 = (Element)var2.getAnnotation(Element.class);
                           var1x = null;
                           if (var10000.parser().tag.toLowerCase(Locale.ENGLISH).equals("orientation") && ElementInformationOption.this.info.getBlockStyle() != BlockStyle.NORMAL && (Boolean)var46.getSelectedItem()) {
                              var2.setBoolean(ElementInformationOption.this.info, false);
                              JOptionPane.showOptionDialog(var1, Lng.ORG_SCHEMA_GAME_COMMON_FACEDIT_ELEMENTINFORMATIONOPTION_0, "Option not available", 0, 1, (Icon)null, new String[]{"Ok"}, "Ok");
                              var46.setSelectedItem(false);
                           } else {
                              var2.setBoolean(ElementInformationOption.this.info, (Boolean)var46.getSelectedItem());
                           }
                        } catch (IllegalArgumentException var2x) {
                           var2x.printStackTrace();
                           GuiErrorHandler.processErrorDialogException(var2x);
                        } catch (IllegalAccessException var3) {
                           var3.printStackTrace();
                           GuiErrorHandler.processErrorDialogException(var3);
                        }
                     }
                  });
                  return var46;
               }

               if (var5 == BlockStyle.class) {
                  var46 = new JComboBox();

                  for(int var39 = 0; var39 < BlockStyle.values().length; ++var39) {
                     var46.addItem(new ElementInformationOption.ValueClass(BlockStyle.values()[var39], BlockStyle.values()[var39].realName));
                     if (BlockStyle.values()[var39] == var2.get(this.info)) {
                        var46.setSelectedIndex(var39);
                     }
                  }

                  var46.setEditable(var6.editable());
                  var46.addActionListener(new ActionListener() {
                     public void actionPerformed(ActionEvent var1) {
                        try {
                           BlockStyle var4 = (BlockStyle)((ElementInformationOption.ValueClass)var46.getSelectedItem()).value;
                           var2.set(ElementInformationOption.this.info, var4);
                        } catch (IllegalArgumentException var2x) {
                           var2x.printStackTrace();
                           GuiErrorHandler.processErrorDialogException(var2x);
                        } catch (IllegalAccessException var3) {
                           var3.printStackTrace();
                           GuiErrorHandler.processErrorDialogException(var3);
                        }
                     }
                  });
                  var46.setToolTipText(BlockStyle.getDescs());
                  return var46;
               }

               final JTextField var25;
               final JComboBox var28;
               int var30;
               final Element var36;
               if (var5 == Integer.TYPE) {
                  if ((var36 = (Element)var2.getAnnotation(Element.class)) != null) {
                     if (var36.states().length > 0) {
                        var28 = new JComboBox();

                        for(var30 = 0; var30 < var36.states().length; ++var30) {
                           var28.addItem(new ElementInformationOption.ValueClass(Integer.parseInt(var36.states()[var30]), var36.stateDescs().length == var36.states().length ? var36.stateDescs()[var30] : var36.states()[var30]));
                           if (Integer.parseInt(var36.states()[var30]) == var2.getInt(this.info)) {
                              var28.setSelectedIndex(var30);
                           }
                        }

                        var28.setEditable(var6.editable());
                        var28.addActionListener(new ActionListener() {
                           public void actionPerformed(ActionEvent var1) {
                              try {
                                 int var4x = (Integer)((ElementInformationOption.ValueClass)var28.getSelectedItem()).value;
                                 var2.setInt(ElementInformationOption.this.info, var4x);
                                 if (var36.updateTextures()) {
                                    var4.updateTextures();
                                 }

                                 if (var36.parser().fac instanceof NodeDependency) {
                                    ((NodeDependency)var36.parser().fac).onSwitch(ElementInformationOption.this, ElementInformationOption.this.info, var36);
                                 }

                              } catch (IllegalArgumentException var2x) {
                                 var2x.printStackTrace();
                                 GuiErrorHandler.processErrorDialogException(var2x);
                              } catch (IllegalAccessException var3) {
                                 var3.printStackTrace();
                                 GuiErrorHandler.processErrorDialogException(var3);
                              }
                           }
                        });
                        return var28;
                     }

                     (var25 = new JTextField()).setEditable(var6.editable());
                     var25.setText(String.valueOf(var2.getInt(this.info)));
                     this.applyInterface = new ApplyInterface() {
                        public void afterApply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException {
                           var25.setText(String.valueOf(var1.getInt(var2)));
                        }

                        public void apply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException {
                           int var3 = Integer.parseInt(var25.getText());
                           if (var36.from() != -1 && var36.to() != -1) {
                              var3 = Math.max(var36.from(), Math.min(var36.to(), var3));
                           }

                           var1.setInt(var2, var3);
                        }
                     };
                     (new JPanel()).add(var25);
                     return var25;
                  }

                  (var25 = new JTextField()).setText(String.valueOf(var2.getInt(this.info)));
                  var25.setEditable(false);
                  return var25;
               }

               if (var5 == Long.TYPE) {
                  if ((var36 = (Element)var2.getAnnotation(Element.class)) != null) {
                     if (var36.states().length > 0) {
                        var28 = new JComboBox();

                        for(var30 = 0; var30 < var36.states().length; ++var30) {
                           var28.addItem(new ElementInformationOption.ValueClass(Long.parseLong(var36.states()[var30]), var36.stateDescs().length == var36.states().length ? var36.stateDescs()[var30] : var36.states()[var30]));
                           if (Long.parseLong(var36.states()[var30]) == var2.getLong(this.info)) {
                              var28.setSelectedIndex(var30);
                           }
                        }

                        var28.addActionListener(new ActionListener() {
                           public void actionPerformed(ActionEvent var1) {
                              try {
                                 long var2x = (Long)((ElementInformationOption.ValueClass)var28.getSelectedItem()).value;
                                 var2.setLong(ElementInformationOption.this.info, var2x);
                              } catch (IllegalArgumentException var4) {
                                 var4.printStackTrace();
                                 GuiErrorHandler.processErrorDialogException(var4);
                              } catch (IllegalAccessException var5) {
                                 var5.printStackTrace();
                                 GuiErrorHandler.processErrorDialogException(var5);
                              }
                           }
                        });
                        return var28;
                     }

                     (var25 = new JTextField()).setText(String.valueOf(var2.getLong(this.info)));
                     this.applyInterface = new ApplyInterface() {
                        public void afterApply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException {
                           var25.setText(String.valueOf(var1.getLong(var2)));
                        }

                        public void apply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException {
                           long var3 = Long.parseLong(var25.getText());
                           if (var36.from() != -1 && var36.to() != -1) {
                              var3 = Math.max((long)var36.from(), Math.min((long)var36.to(), var3));
                           }

                           var1.setLong(var2, var3);
                        }
                     };
                     return var25;
                  }

                  (var25 = new JTextField()).setText(String.valueOf(var2.getLong(this.info)));
                  var25.setEditable(false);
                  return var25;
               }

               if (var5 == Short.TYPE) {
                  if ((var36 = (Element)var2.getAnnotation(Element.class)) != null) {
                     if (var36.states().length > 0) {
                        var28 = new JComboBox();

                        for(var30 = 0; var30 < var36.states().length; ++var30) {
                           var28.addItem(new ElementInformationOption.ValueClass(Short.parseShort(var36.states()[var30]), var36.stateDescs().length == var36.states().length ? var36.stateDescs()[var30] : var36.states()[var30]));
                           if (Short.parseShort(var36.states()[var30]) == var2.getShort(this.info)) {
                              var28.setSelectedIndex(var30);
                           }
                        }

                        var28.addActionListener(new ActionListener() {
                           public void actionPerformed(ActionEvent var1) {
                              try {
                                 short var4 = (Short)((ElementInformationOption.ValueClass)var28.getSelectedItem()).value;
                                 var2.setShort(ElementInformationOption.this.info, var4);
                              } catch (IllegalArgumentException var2x) {
                                 var2x.printStackTrace();
                                 GuiErrorHandler.processErrorDialogException(var2x);
                              } catch (IllegalAccessException var3) {
                                 var3.printStackTrace();
                                 GuiErrorHandler.processErrorDialogException(var3);
                              }
                           }
                        });
                        return var28;
                     }

                     (var25 = new JTextField()).setText(String.valueOf(var2.getShort(this.info)));
                     this.applyInterface = new ApplyInterface() {
                        public void afterApply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException {
                           var25.setText(String.valueOf(var1.getShort(var2)));
                        }

                        public void apply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException {
                           short var3 = Short.parseShort(var25.getText());
                           if (var36.from() != -1 && var36.to() != -1) {
                              var3 = (short)Math.max(var36.from(), Math.min(var36.to(), var3));
                           }

                           var1.setShort(var2, var3);
                        }
                     };
                     return var25;
                  }

                  (var25 = new JTextField()).setText(String.valueOf(var2.getShort(this.info)));
                  var25.setEditable(false);
                  return var25;
               }

               if (var5 == Float.TYPE) {
                  if ((var36 = (Element)var2.getAnnotation(Element.class)) != null) {
                     if (var36.states().length > 0) {
                        var28 = new JComboBox();

                        for(var30 = 0; var30 < var36.states().length; ++var30) {
                           var28.addItem(new ElementInformationOption.ValueClass(Float.parseFloat(var36.states()[var30]), var36.stateDescs().length == var36.states().length ? var36.stateDescs()[var30] : var36.states()[var30]));
                           if (Float.parseFloat(var36.states()[var30]) == var2.getFloat(this.info)) {
                              var28.setSelectedIndex(var30);
                           }
                        }

                        var28.addActionListener(new ActionListener() {
                           public void actionPerformed(ActionEvent var1) {
                              try {
                                 float var4 = (Float)((ElementInformationOption.ValueClass)var28.getSelectedItem()).value;
                                 var2.setFloat(ElementInformationOption.this.info, var4);
                              } catch (IllegalArgumentException var2x) {
                                 var2x.printStackTrace();
                                 GuiErrorHandler.processErrorDialogException(var2x);
                              } catch (IllegalAccessException var3) {
                                 var3.printStackTrace();
                                 GuiErrorHandler.processErrorDialogException(var3);
                              }
                           }
                        });
                        return var28;
                     }

                     (var25 = new JTextField()).setText(String.valueOf(var2.getFloat(this.info)));
                     this.applyInterface = new ApplyInterface() {
                        public void afterApply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException {
                           var25.setText(String.valueOf(var1.getFloat(var2)));
                        }

                        public void apply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException {
                           float var3 = Float.parseFloat(var25.getText());
                           if (var36.from() != -1 && var36.to() != -1) {
                              var3 = Math.max((float)var36.from(), Math.min((float)var36.to(), var3));
                           }

                           var1.setFloat(var2, var3);
                        }
                     };
                     return var25;
                  }

                  (var25 = new JTextField()).setText(String.valueOf(var2.getShort(this.info)));
                  var25.setEditable(false);
                  return var25;
               }
            }
         }

         return new JLabel("Cannot parse " + var2.getName() + ": " + var5.getSimpleName());
      }
   }

   public void apply() {
      try {
         this.applyInterface.apply(this.field, this.info);
      } catch (IllegalArgumentException var4) {
         var4.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var4);
      } catch (IllegalAccessException var5) {
         var5.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var5);
      }

      try {
         this.applyInterface.afterApply(this.field, this.info);
      } catch (IllegalArgumentException var2) {
         var2.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var2);
      } catch (IllegalAccessException var3) {
         var3.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var3);
      }
   }

   public int compareTo(ElementInformationOption var1) {
      return this.order - var1.order;
   }

   public void setLocalOrder(int var1) {
      this.localOrder = var1;
      if (this.localOrder % 2 == 0) {
         this.setBackground(Color.gray.brighter());
         this.mainPanel.setBackground(Color.gray.brighter());
      }

   }

   class ValueClass {
      public Object value;
      public String desc;

      public ValueClass(Object var2, String var3) {
         this.value = var2;
         this.desc = var3;
      }

      public int hashCode() {
         return this.value.hashCode();
      }

      public boolean equals(Object var1) {
         return var1 instanceof ElementInformationOption.ValueClass ? this.value.equals(((ElementInformationOption.ValueClass)var1).value) : this.value.equals(var1);
      }

      public String toString() {
         return this.desc;
      }
   }

   class StringListModel extends AbstractListModel {
      private static final long serialVersionUID = 1L;
      private final List o;

      public StringListModel(List var2) {
         this.o = var2;
      }

      public int getSize() {
         return this.o.size();
      }

      public String getElementAt(int var1) {
         return (String)this.o.get(var1);
      }

      public void setChanged() {
         this.fireContentsChanged(this, 0, this.o.size());
      }
   }
}
