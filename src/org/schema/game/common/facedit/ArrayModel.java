package org.schema.game.common.facedit;

import com.bulletphysics.util.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractListModel;

public abstract class ArrayModel extends AbstractListModel {
   private static final long serialVersionUID = 1L;
   private final ObjectArrayList arrayList = new ObjectArrayList();

   public ArrayModel(Object[] var1) {
      for(int var2 = 0; var2 < var1.length; ++var2) {
         this.arrayList.add(var1[var2]);
      }

   }

   public abstract void changed(ObjectArrayList var1);

   public boolean add(Object var1) {
      boolean var2;
      if (var2 = this.arrayList.add(var1)) {
         int var3 = this.getIndexOf(var1);
         this.fireIntervalAdded(this, var3, var3 + 1);
      }

      this.changed(this.arrayList);
      return var2;
   }

   public List getCollection() {
      return this.arrayList;
   }

   public int getIndexOf(Object var1) {
      int var2 = 0;

      for(Iterator var3 = this.arrayList.iterator(); var3.hasNext(); ++var2) {
         if (var3.next().equals(var1)) {
            return var2;
         }
      }

      return -1;
   }

   public int getSize() {
      return this.arrayList == null ? 0 : this.arrayList.size();
   }

   public Object getElementAt(int var1) {
      if (var1 >= 0 && var1 < this.getSize()) {
         Iterator var5 = this.arrayList.iterator();

         for(int var3 = 0; var5.hasNext(); ++var3) {
            Object var4 = var5.next();
            if (var1 == var3) {
               return var4;
            }
         }

         return null;
      } else {
         String var2 = "index, " + var1 + ", is out of bounds for getSize() = " + this.getSize();
         throw new IllegalArgumentException(var2);
      }
   }

   public boolean remove(Object var1) {
      int var2;
      if ((var2 = this.getIndexOf(var1)) < 0) {
         return false;
      } else {
         boolean var3 = this.arrayList.remove(var1);
         this.fireIntervalRemoved(this, var2, var2 + 1);
         this.changed(this.arrayList);
         return var3;
      }
   }
}
