package org.schema.game.common.facedit;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.border.EmptyBorder;

public class ProductEditDialog extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();

   public ProductEditDialog(JFrame var1, ArrayList var2, ArrayList var3, final ExecuteInterface var4) {
      super(var1, true);
      this.setBounds(100, 100, 450, 435);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var5;
      (var5 = new GridBagLayout()).columnWidths = new int[]{125};
      var5.rowHeights = new int[]{25, 0};
      var5.columnWeights = new double[]{0.0D};
      var5.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      this.contentPanel.setLayout(var5);
      JSplitPane var8;
      (var8 = new JSplitPane()).setOrientation(0);
      GridBagConstraints var6;
      (var6 = new GridBagConstraints()).weighty = 1.0D;
      var6.weightx = 1.0D;
      var6.fill = 1;
      var6.anchor = 18;
      var6.gridx = 0;
      var6.gridy = 0;
      this.contentPanel.add(var8, var6);
      FactoryResourceEditPanel var7 = new FactoryResourceEditPanel(var1, "input", var2);
      var8.setLeftComponent(var7);
      var7 = new FactoryResourceEditPanel(var1, "output", var3);
      var8.setRightComponent(var7);
      var8.setDividerLocation(200);
      JPanel var9;
      (var9 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var9, "South");
      JButton var10;
      (var10 = new JButton("OK")).setActionCommand("OK");
      var9.add(var10);
      this.getRootPane().setDefaultButton(var10);
      var10.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            var4.execute();
            ProductEditDialog.this.dispose();
         }
      });
      (var10 = new JButton("Cancel")).setActionCommand("Cancel");
      var9.add(var10);
      var10.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ProductEditDialog.this.dispose();
         }
      });
   }
}
