package org.schema.game.common.facedit;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementReactorChange;

public class ElementReactorChangePanel extends JPanel implements Observer {
   private static final long serialVersionUID = 1L;
   private JLabel lblGeneral;
   private JLabel lblParent;
   private JCheckBox chckbxUpgrade;
   private final ElementReactorChange c;

   public ElementReactorChangePanel(final JDialog var1, final ElementReactorChange var2) {
      this.setLayout(new GridBagLayout());
      this.c = var2;
      var2.addObserver(this);
      JButton var3;
      (var3 = new JButton("General Chamber")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            (new ElementChoserDialog((JFrame)var1.getParent(), new ElementChoseInterface() {
               public void onEnter(ElementInformation var1x) {
                  var2.setRoot(var1x);
               }
            })).setVisible(true);
         }
      });
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).weighty = 1.0D;
      var4.weightx = 1.0D;
      var4.anchor = 18;
      var4.insets = new Insets(0, 0, 5, 5);
      var4.gridx = 0;
      var4.gridy = 0;
      this.add(var3, var4);
      (var3 = new JButton("Parent")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            (new ElementChoserDialog((JFrame)var1.getParent(), new ElementChoseInterface() {
               public void onEnter(ElementInformation var1x) {
                  var2.setParent(var1x);
               }
            })).setVisible(true);
         }
      });
      (var4 = new GridBagConstraints()).weighty = 1.0D;
      var4.weightx = 1.0D;
      var4.anchor = 18;
      var4.insets = new Insets(0, 0, 5, 5);
      var4.gridx = 1;
      var4.gridy = 0;
      this.add(var3, var4);
      this.chckbxUpgrade = new JCheckBox("upgrade");
      this.chckbxUpgrade.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            var2.setUpgrade(ElementReactorChangePanel.this.chckbxUpgrade.isSelected());
         }
      });
      GridBagConstraints var8;
      (var8 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var8.weightx = 1.0D;
      var8.anchor = 18;
      var8.gridx = 2;
      var8.gridy = 0;
      this.add(this.chckbxUpgrade, var8);
      this.lblGeneral = new JLabel("general");
      (var8 = new GridBagConstraints()).weighty = 1.0D;
      var8.anchor = 18;
      var8.insets = new Insets(0, 0, 5, 5);
      var8.gridx = 0;
      var8.gridy = 1;
      this.add(this.lblGeneral, var8);
      this.lblParent = new JLabel("parent");
      (var8 = new GridBagConstraints()).weighty = 1.0D;
      var8.anchor = 18;
      var8.insets = new Insets(0, 0, 5, 5);
      var8.gridx = 1;
      var8.gridy = 1;
      this.add(this.lblParent, var8);
      (var3 = new JButton("Ok")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            var2.apply();
            var1.dispose();
         }
      });
      JButton var9;
      (var9 = new JButton("Directly from General (no parent)")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            var2.parent = null;
            ElementReactorChangePanel.this.updateValues();
         }
      });
      GridBagConstraints var6;
      (var6 = new GridBagConstraints()).anchor = 17;
      var6.insets = new Insets(0, 0, 5, 5);
      var6.gridx = 1;
      var6.gridy = 2;
      this.add(var9, var6);
      var3.setVerticalAlignment(3);
      (var6 = new GridBagConstraints()).weighty = 1.0D;
      var6.anchor = 14;
      var6.insets = new Insets(0, 0, 0, 5);
      var6.gridx = 1;
      var6.gridy = 3;
      this.add(var3, var6);
      JButton var7;
      (var7 = new JButton("Cancel")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            var1.dispose();
         }
      });
      var7.setVerticalAlignment(3);
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).anchor = 14;
      var5.gridx = 2;
      var5.gridy = 3;
      this.add(var7, var5);
      this.updateValues();
   }

   public void update(Observable var1, Object var2) {
      this.updateValues();
   }

   private void updateValues() {
      this.lblGeneral.setText(this.c.root != null ? this.c.root.toString() : "not set");
      this.lblParent.setText(this.c.parent != null ? this.c.parent.toString() : "directly from general (no parent)");
      this.chckbxUpgrade.setSelected(this.c.upgrade);
   }
}
