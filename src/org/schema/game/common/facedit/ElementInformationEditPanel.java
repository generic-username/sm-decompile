package org.schema.game.common.facedit;

import it.unimi.dsi.fastutil.objects.Object2ObjectAVLTreeMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.Map.Entry;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.jdesktop.swingx.JXCollapsiblePane;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXPanel;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.annotation.ElemType;
import org.schema.game.common.data.element.annotation.Element;
import org.schema.game.common.data.element.annotation.NodeDependency;

public class ElementInformationEditPanel extends JPanel {
   private static final long serialVersionUID = 1L;
   private TexturePanel texture;
   SortedMap list;
   private Object2ObjectOpenHashMap opop;

   public ElementInformationEditPanel(JFrame var1, short var2) {
      Field[] var3 = ElementInformation.class.getDeclaredFields();
      this.opop = new Object2ObjectOpenHashMap(ElementInformation.EIC.values().length);

      for(int var4 = 0; var4 < var3.length; ++var4) {
         Element var5;
         if ((var5 = (Element)var3[var4].getAnnotation(Element.class)) != null) {
            this.list = (SortedMap)this.opop.get(var5.cat());
            if (this.list == null) {
               this.list = new Object2ObjectAVLTreeMap();
               this.opop.put(var5.cat(), this.list);
            }

            var3[var4].setAccessible(true);
            this.list.put(new ElementInformationEditPanel.ElemContainer(var5), new ElementInformationOption(var1, var3[var4], var2, var4, this));
         }
      }

      GridBagLayout var15;
      (var15 = new GridBagLayout()).columnWidths = new int[]{200};
      var15.rowHeights = new int[this.opop.size() + 2];

      for(int var17 = 0; var17 < var15.rowHeights.length; ++var17) {
         var15.rowHeights[var17] = 33;
      }

      var15.columnWeights = new double[]{0.0D};
      var15.rowWeights = new double[]{0.0D};
      this.setLayout(var15);
      JXLabel var20;
      (var20 = new JXLabel(ElementKeyMap.getInfo(var2).getName())).setFont(new Font("Arial", 0, 19));
      GridBagConstraints var11;
      (var11 = new GridBagConstraints()).anchor = 18;
      var11.gridx = 0;
      var11.gridy = 0;
      this.add(var20, var11);
      this.texture = new TexturePanel(ElementKeyMap.getInfo(var2), false);
      (var11 = new GridBagConstraints()).anchor = 18;
      var11.fill = 1;
      var11.gridx = 0;
      var11.gridy = 1;
      this.add(this.texture, var11);
      int var12 = 0;

      for(int var13 = 0; var13 < ElementInformation.EIC.values().length; ++var13) {
         final ElementInformation.EIC var14 = ElementInformation.EIC.values()[var13];
         SortedMap var16;
         Iterator var21 = (var16 = (SortedMap)this.opop.get(var14)).entrySet().iterator();

         while(var21.hasNext()) {
            Entry var6;
            if (((ElementInformationEditPanel.ElemContainer)(var6 = (Entry)var21.next()).getKey()).elem.parser().fac instanceof NodeDependency) {
               ((NodeDependency)((ElementInformationEditPanel.ElemContainer)var6.getKey()).elem.parser().fac).onSwitch((ElementInformationOption)var6.getValue(), ((ElementInformationOption)var6.getValue()).info, ((ElementInformationEditPanel.ElemContainer)var6.getKey()).elem);
            }
         }

         JXPanel var23;
         (var23 = new JXPanel()).setLayout(new BorderLayout());
         final JXCollapsiblePane var22;
         (var22 = new JXCollapsiblePane()).setCollapsed(var14.collapsed);
         JXPanel var7 = new JXPanel();
         GridBagLayout var8;
         (var8 = new GridBagLayout()).columnWidths = new int[]{400};
         var8.rowHeights = new int[var16.size()];

         int var9;
         for(var9 = 0; var9 < var8.rowHeights.length; ++var9) {
            var8.rowHeights[var9] = 33;
         }

         var8.columnWeights = new double[]{0.0D};
         var8.rowWeights = new double[]{0.0D};
         var7.setLayout(var8);
         var9 = 0;

         for(Iterator var18 = var16.values().iterator(); var18.hasNext(); ++var9) {
            ElementInformationOption var24;
            (var24 = (ElementInformationOption)var18.next()).setLocalOrder(var9);
            GridBagConstraints var10;
            (var10 = new GridBagConstraints()).anchor = 17;
            var10.fill = 1;
            var10.gridx = 0;
            var10.gridy = var9;
            var7.add(var24, var10);
         }

         var22.setContentPane(var7);
         GridBagConstraints var19;
         (var19 = new GridBagConstraints()).anchor = 18;
         var19.fill = 1;
         var19.weightx = 1.0D;
         var19.weighty = 1.0D;
         var19.gridx = 0;
         var19.gridy = var12 + 2;
         var22.addPropertyChangeListener("animationState", new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent var1) {
               var14.collapsed = var22.isCollapsed();
            }
         });
         JButton var25;
         (var25 = new JButton(var22.getActionMap().get("toggle"))).setText(var14.getName());
         var23.add(var25, "North");
         var23.add(var22, "South");
         this.add(var23, var19);
         ++var12;
      }

   }

   public void onSwitchActivationAnimationStyle(int var1) {
      for(int var2 = 0; var2 < ElementInformation.EIC.values().length; ++var2) {
         ElementInformation.EIC var3 = ElementInformation.EIC.values()[var2];
         Iterator var5 = ((SortedMap)this.opop.get(var3)).entrySet().iterator();

         while(var5.hasNext()) {
            Entry var4 = (Entry)var5.next();
            switch(var1) {
            case 1:
               if (((ElementInformationEditPanel.ElemContainer)var4.getKey()).elem.parser() == ElemType.LOD_SHAPE_ACTIVE) {
                  this.setEnabledComp(((ElementInformationOption)var4.getValue()).editComponent, true);
               }
               break;
            default:
               if (((ElementInformationEditPanel.ElemContainer)var4.getKey()).elem.parser() == ElemType.LOD_SHAPE_ACTIVE) {
                  this.setEnabledComp(((ElementInformationOption)var4.getValue()).editComponent, false);
               }
            }
         }
      }

   }

   private void setEnabledComp(Component var1, boolean var2) {
      var1.setEnabled(var2);
      if (var1 instanceof JComponent) {
         JComponent var4 = (JComponent)var1;

         for(int var3 = 0; var3 < var4.getComponentCount(); ++var3) {
            this.setEnabledComp(var4.getComponents()[var3], var2);
         }
      }

   }

   public void updateTextures() {
      this.texture.update();
   }

   public int getScroll() {
      return 0;
   }

   public void setScroll(int var1) {
   }

   class ElemContainer implements Comparable {
      public Element elem;
      public int order;

      public ElemContainer(Element var2) {
         this.elem = var2;
         this.order = var2.order();
      }

      public int compareTo(ElementInformationEditPanel.ElemContainer var1) {
         return this.order - var1.order;
      }
   }
}
