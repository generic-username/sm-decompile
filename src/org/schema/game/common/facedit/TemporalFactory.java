package org.schema.game.common.facedit;

import java.util.ArrayList;
import org.schema.game.common.data.element.BlockFactory;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;

public class TemporalFactory {
   public short enhancer;
   ArrayList temporalProducts = new ArrayList();

   public BlockFactory convertFromTemporal(ElementInformation var1) {
      BlockFactory var2;
      (var2 = new BlockFactory()).enhancer = this.enhancer;
      var2.output = new FactoryResource[this.temporalProducts.size()][];
      var2.input = new FactoryResource[this.temporalProducts.size()][];

      for(int var3 = 0; var3 < this.temporalProducts.size(); ++var3) {
         var2.input[var3] = new FactoryResource[((TemporalProduct)this.temporalProducts.get(var3)).input.size()];

         int var4;
         for(var4 = 0; var4 < ((TemporalProduct)this.temporalProducts.get(var3)).input.size(); ++var4) {
            var2.input[var3][var4] = (FactoryResource)((TemporalProduct)this.temporalProducts.get(var3)).input.get(var4);
         }

         var2.output[var3] = new FactoryResource[((TemporalProduct)this.temporalProducts.get(var3)).output.size()];

         for(var4 = 0; var4 < ((TemporalProduct)this.temporalProducts.get(var3)).output.size(); ++var4) {
            var2.output[var3][var4] = (FactoryResource)((TemporalProduct)this.temporalProducts.get(var3)).output.get(var4);
         }
      }

      var1.setFactory(var2);
      return var2;
   }

   public void setFromExistingInfo(short var1) {
      ElementInformation var2;
      if ((var2 = ElementKeyMap.getInfo(var1)).getFactory() != null) {
         this.enhancer = var2.getFactory().enhancer;

         for(int var3 = 0; var3 < var2.getFactory().input.length; ++var3) {
            TemporalProduct var4;
            (var4 = new TemporalProduct()).factoryId = var1;

            int var5;
            for(var5 = 0; var5 < var2.getFactory().input[var3].length; ++var5) {
               var4.input.add(var2.getFactory().input[var3][var5]);
            }

            for(var5 = 0; var5 < var2.getFactory().output[var3].length; ++var5) {
               var4.output.add(var2.getFactory().output[var3][var5]);
            }

            this.temporalProducts.add(var4);
         }
      }

   }

   public String toString() {
      return "Factory Products: " + this.temporalProducts.size();
   }
}
