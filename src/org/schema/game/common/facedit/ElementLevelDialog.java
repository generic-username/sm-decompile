package org.schema.game.common.facedit;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import org.schema.game.common.data.element.BlockLevel;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;

public class ElementLevelDialog extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private short currentId;
   private JLabel lblUndefined;
   private JSlider slider;

   public ElementLevelDialog(final JFrame var1, BlockLevel var2, ElementInformation var3, JTextPane var4) {
      super(var1, true);
      this.setTitle("Block Level Editor");
      this.setBounds(100, 100, 510, 184);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var5;
      (var5 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0};
      var5.rowHeights = new int[]{0, 0, 0};
      var5.columnWeights = new double[]{0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var5.rowWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      this.contentPanel.setLayout(var5);
      JLabel var6 = new JLabel("Base Element");
      GridBagConstraints var8;
      (var8 = new GridBagConstraints()).anchor = 17;
      var8.insets = new Insets(0, 0, 5, 5);
      var8.gridx = 0;
      var8.gridy = 0;
      this.contentPanel.add(var6, var8);
      this.currentId = var2 != null ? var2.getIdBase() : -1;
      this.lblUndefined = new JLabel(this.currentId > 0 ? ElementKeyMap.getInfo(this.currentId).toString() : "undefined");
      GridBagConstraints var7;
      (var7 = new GridBagConstraints()).weightx = 1.0D;
      var7.insets = new Insets(0, 0, 5, 5);
      var7.gridx = 1;
      var7.gridy = 0;
      this.contentPanel.add(this.lblUndefined, var7);
      JButton var9 = new JButton("Choose");
      (var8 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var8.anchor = 13;
      var8.gridx = 2;
      var8.gridy = 0;
      this.contentPanel.add(var9, var8);
      var9.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            (new ElementChoserDialog(var1, new ElementChoseInterface() {
               public void onEnter(ElementInformation var1x) {
                  ElementLevelDialog.this.currentId = var1x.getId();
                  ElementLevelDialog.this.lblUndefined.setText(ElementLevelDialog.this.currentId > 0 ? ElementKeyMap.getInfo(ElementLevelDialog.this.currentId).toString() : "undefined");
               }
            })).setVisible(true);
         }
      });
      var6 = new JLabel("Level");
      (var8 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var8.gridx = 0;
      var8.gridy = 1;
      this.contentPanel.add(var6, var8);
      this.slider = new JSlider();
      this.slider.setSnapToTicks(true);
      this.slider.setMajorTickSpacing(1);
      this.slider.setPaintTicks(true);
      this.slider.setPaintLabels(true);
      this.slider.setMinimum(1);
      this.slider.setMaximum(5);
      this.slider.setValue(var2 != null ? var2.getLevel() : 1);
      (var7 = new GridBagConstraints()).fill = 2;
      var7.weightx = 11.0D;
      var7.gridwidth = 2;
      var7.insets = new Insets(0, 0, 0, 5);
      var7.gridx = 1;
      var7.gridy = 1;
      this.contentPanel.add(this.slider, var7);
      JPanel var10;
      (var10 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var10, "South");
      JButton var11;
      (var11 = new JButton("Cancel")).setActionCommand("Cancel");
      var10.add(var11);
      var11.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ElementLevelDialog.this.dispose();
         }
      });
   }
}
