package org.schema.game.common.facedit;

import javax.swing.event.TreeSelectionListener;
import org.schema.game.common.data.element.ElementInformation;

public interface ElementTreeInterface extends TreeSelectionListener {
   void removeEntry(ElementInformation var1);

   boolean hasPopupMenu();

   void reinitializeElements();
}
