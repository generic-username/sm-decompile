package org.schema.game.common.facedit;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.DefaultMutableTreeNode;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;

public class ElementChoserDialog extends JDialog implements ElementTreeInterface {
   private static final long serialVersionUID = 1L;
   private final HashSet fullset = new HashSet();
   private JTextField textField;
   private JTabbedPane tabbedPane;
   private JPanel searchPanel;
   private JPanel treePanel;
   private ElementTreePanel factoryEditorElementOverviewPanel;
   private ElementChoseInterface iFace;
   private JList list;
   private ElementInformation currentlySelected;
   private JScrollPane scrollPane;

   public ElementChoserDialog(JFrame var1, ElementChoseInterface var2) {
      super(var1, true);
      this.iFace = var2;
      short[] var5;
      int var8 = (var5 = ElementKeyMap.typeList()).length;

      for(int var3 = 0; var3 < var8; ++var3) {
         short var4 = var5[var3];
         this.fullset.add(ElementKeyMap.getInfo(var4));
      }

      this.setAlwaysOnTop(true);
      this.setBounds(100, 100, 702, 440);
      this.getContentPane().setLayout(new BorderLayout());
      this.tabbedPane = new JTabbedPane(1);
      this.getContentPane().add(this.tabbedPane, "Center");
      this.searchPanel = new JPanel();
      this.tabbedPane.addTab(Lng.ORG_SCHEMA_GAME_COMMON_FACEDIT_ELEMENTCHOSERDIALOG_0, (Icon)null, this.searchPanel, (String)null);
      GridBagLayout var6;
      (var6 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var6.rowHeights = new int[]{0, 0, 0};
      var6.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var6.rowWeights = new double[]{0.0D, 1.0D, Double.MIN_VALUE};
      this.searchPanel.setLayout(var6);
      this.textField = new JTextField("");
      GridBagConstraints var9;
      (var9 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var9.fill = 2;
      var9.gridx = 0;
      var9.gridy = 0;
      this.searchPanel.add(this.textField, var9);
      this.textField.setColumns(10);
      this.textField.addKeyListener(new KeyListener() {
         public void keyTyped(KeyEvent var1) {
         }

         public void keyPressed(KeyEvent var1) {
            if (var1.getKeyCode() == 10) {
               ElementChoserDialog.this.apply();
            }

         }

         public void keyReleased(KeyEvent var1) {
            ElementChoserDialog.this.updateList();
         }
      });
      this.list = new JList();
      this.list.addMouseListener(new MouseListener() {
         public void mouseClicked(MouseEvent var1) {
            if (var1.getClickCount() >= 2) {
               ElementChoserDialog.this.apply();
            }

         }

         public void mouseEntered(MouseEvent var1) {
         }

         public void mouseExited(MouseEvent var1) {
         }

         public void mousePressed(MouseEvent var1) {
         }

         public void mouseReleased(MouseEvent var1) {
         }
      });
      this.list.setSelectionMode(0);
      (var9 = new GridBagConstraints()).fill = 1;
      var9.gridx = 0;
      var9.gridy = 1;
      this.searchPanel.add(new JScrollPane(this.list), var9);
      this.treePanel = new JPanel();
      this.tabbedPane.addTab(Lng.ORG_SCHEMA_GAME_COMMON_FACEDIT_ELEMENTCHOSERDIALOG_1, (Icon)null, this.treePanel, (String)null);
      this.treePanel.setLayout(new GridLayout(0, 1, 0, 0));
      this.scrollPane = new JScrollPane();
      this.treePanel.add(this.scrollPane);
      this.factoryEditorElementOverviewPanel = new ElementTreePanel(this);
      this.scrollPane.setViewportView(this.factoryEditorElementOverviewPanel);
      this.factoryEditorElementOverviewPanel.getTree().setEditable(false);
      this.factoryEditorElementOverviewPanel.getTree().addMouseListener(new MouseListener() {
         public void mouseClicked(MouseEvent var1) {
            if (var1.getClickCount() >= 2) {
               ElementChoserDialog.this.apply();
            }

         }

         public void mouseEntered(MouseEvent var1) {
         }

         public void mouseExited(MouseEvent var1) {
         }

         public void mousePressed(MouseEvent var1) {
         }

         public void mouseReleased(MouseEvent var1) {
         }
      });
      JPanel var7;
      (var7 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var7, "South");
      JButton var10;
      (var10 = new JButton("OK")).setActionCommand("OK");
      var7.add(var10);
      this.getRootPane().setDefaultButton(var10);
      var10.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ElementChoserDialog.this.apply();
         }
      });
      (var10 = new JButton("Cancel")).setActionCommand("Cancel");
      var10.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ElementChoserDialog.this.dispose();
         }
      });
      var7.add(var10);
      this.updateList();
      this.textField.requestFocus();
      this.textField.addAncestorListener(new AncestorListener() {
         public void ancestorRemoved(AncestorEvent var1) {
         }

         public void ancestorMoved(AncestorEvent var1) {
         }

         public void ancestorAdded(AncestorEvent var1) {
            SwingUtilities.invokeLater(new Runnable() {
               public void run() {
                  ElementChoserDialog.this.textField.requestFocusInWindow();
               }
            });
         }
      });
   }

   private void apply() {
      if (this.tabbedPane.getSelectedComponent() == this.searchPanel) {
         Object var2 = this.list.getSelectedValue();
         System.err.println("SELECTED FROM LIST " + var2);
         if (var2 != null && var2 instanceof ElementInformation) {
            this.iFace.onEnter((ElementInformation)var2);
            this.dispose();
         }

      } else if (this.tabbedPane.getSelectedComponent() == this.treePanel) {
         ElementInformation var1;
         if ((var1 = this.currentlySelected) != null) {
            this.iFace.onEnter(var1);
            this.dispose();
         }

      } else {
         System.err.println("NO TABBED PANE SELECTED " + this.tabbedPane.getSelectedComponent());
      }
   }

   private void updateList() {
      HashSet var1 = new HashSet(this.fullset);
      String var2;
      if ((var2 = this.textField.getText()).trim().length() != 0) {
         Iterator var3 = this.fullset.iterator();

         while(var3.hasNext()) {
            ElementInformation var4;
            if (!(var4 = (ElementInformation)var3.next()).getName().toLowerCase(Locale.ENGLISH).contains(var2.trim().toLowerCase(Locale.ENGLISH))) {
               var1.remove(var4);
            }
         }
      }

      Comparator var5 = new Comparator() {
         public int compare(ElementInformation var1, ElementInformation var2) {
            return var1.getName().compareTo(var2.getName());
         }
      };
      ElementInformation[] var6 = new ElementInformation[var1.size()];
      var1.toArray(var6);
      Arrays.sort(var6, var5);
      this.list.setListData(var6);
      if (this.list.getModel().getSize() > 0) {
         this.list.setSelectedIndex(0);
      }

   }

   public void valueChanged(TreeSelectionEvent var1) {
      DefaultMutableTreeNode var2;
      if ((var2 = (DefaultMutableTreeNode)var1.getPath().getLastPathComponent()).getUserObject() instanceof ElementInformation) {
         ElementInformation var3 = (ElementInformation)var2.getUserObject();
         this.currentlySelected = var3;
         System.err.println("NOW SELECTED " + this.currentlySelected);
      }

   }

   public void removeEntry(ElementInformation var1) {
   }

   public boolean hasPopupMenu() {
      return false;
   }

   public void reinitializeElements() {
   }
}
