package org.schema.game.common.facedit;

import java.awt.Component;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import org.schema.game.common.data.element.ElementInformation;

public class ElementTreeRenderer extends DefaultTreeCellRenderer {
   private static final long serialVersionUID = 1L;

   public Component getTreeCellRendererComponent(JTree var1, Object var2, boolean var3, boolean var4, boolean var5, int var6, boolean var7) {
      super.getTreeCellRendererComponent(var1, var2, var3, var4, var5, var6, var7);
      if (var5 && this.isInfo(var2)) {
         ElementInformation var8 = (ElementInformation)((DefaultMutableTreeNode)var2).getUserObject();
         this.setIcon(EditorTextureManager.getImage(var8.getTextureId(0)));
         this.setToolTipText("This book is in the Tutorial series.");
      } else {
         this.setToolTipText((String)null);
      }

      return this;
   }

   protected boolean isInfo(Object var1) {
      return ((DefaultMutableTreeNode)var1).getUserObject() instanceof ElementInformation;
   }
}
