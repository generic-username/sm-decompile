package org.schema.game.common.facedit;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.common.data.element.BlockFactory;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;

public class FactoryEditorController {
   private final HashMap elementMap = new HashMap();
   EditorData data = new EditorData();

   public TemporalElement convertExisting(short var1) {
      TemporalElement var2;
      (var2 = new TemporalElement()).setId(var1);
      ElementInformation var3 = ElementKeyMap.getInfo(var1);
      var2.setIconId(var3.getBuildIconNum());
      var2.setTextureId(Arrays.copyOf(var3.getTextureIds(), var3.getTextureIds().length));
      var2.setName(var3.getName());
      TemporalFactory var4;
      (var4 = new TemporalFactory()).enhancer = var3.getFactory().enhancer;

      for(int var5 = 0; var5 < var3.getFactory().input.length; ++var5) {
         TemporalProduct var6;
         (var6 = new TemporalProduct()).factoryId = var1;

         int var7;
         for(var7 = 0; var7 < var3.getFactory().input[var5].length; ++var7) {
            var6.input.add(var3.getFactory().input[var5][var7]);
         }

         for(var7 = 0; var7 < var3.getFactory().output[var5].length; ++var7) {
            var6.output.add(var3.getFactory().output[var5][var7]);
         }

         var4.temporalProducts.add(var6);
      }

      var2.setFactory(var4);
      this.elementMap.put(var1, var2);
      return var2;
   }

   public ElementInformation convertFromTemporal(TemporalElement var1) {
      ElementInformation var2 = new ElementInformation(var1.getId(), var1.getName(), ElementKeyMap.getCategoryHirarchy().find("factory"), var1.getTextureId());
      BlockFactory var3;
      (var3 = new BlockFactory()).enhancer = var1.getFactory().enhancer;
      var3.output = new FactoryResource[var1.getFactory().temporalProducts.size()][];
      var3.input = new FactoryResource[var1.getFactory().temporalProducts.size()][];

      for(int var4 = 0; var4 < var1.getFactory().temporalProducts.size(); ++var4) {
         var3.input[var4] = new FactoryResource[((TemporalProduct)var1.getFactory().temporalProducts.get(var4)).input.size()];

         int var5;
         for(var5 = 0; var5 < ((TemporalProduct)var1.getFactory().temporalProducts.get(var4)).input.size(); ++var5) {
            var3.input[var4][var5] = (FactoryResource)((TemporalProduct)var1.getFactory().temporalProducts.get(var4)).input.get(var5);
         }

         var3.output[var4] = new FactoryResource[((TemporalProduct)var1.getFactory().temporalProducts.get(var4)).output.size()];

         for(var5 = 0; var5 < ((TemporalProduct)var1.getFactory().temporalProducts.get(var4)).output.size(); ++var5) {
            var3.output[var4][var5] = (FactoryResource)((TemporalProduct)var1.getFactory().temporalProducts.get(var4)).output.get(var5);
         }
      }

      var2.setFactory(var3);
      return var2;
   }

   private void convertToTempoal() {
      Iterator var1 = ElementKeyMap.getFactorykeyset().iterator();

      while(var1.hasNext()) {
         (Short)var1.next();
         new TemporalProduct();
      }

   }

   public TemporalProduct createTemporalFactory() {
      return new TemporalProduct();
   }

   public void initialize() {
      if (!ElementKeyMap.initialized) {
         ElementKeyMap.initializeData(GameResourceLoader.getConfigInputFile());
      }

      this.convertToTempoal();
   }
}
