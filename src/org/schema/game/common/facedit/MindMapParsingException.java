package org.schema.game.common.facedit;

public class MindMapParsingException extends RuntimeException {
   private static final long serialVersionUID = 1L;

   public MindMapParsingException(String var1) {
      super(var1);
   }

   public MindMapParsingException(String var1, Exception var2) {
      super(var1, var2);
   }
}
