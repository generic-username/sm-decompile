package org.schema.game.common.facedit;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.Iterator;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.common.data.element.ElementCategory;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;

public class ElementTreePanel extends JPanel {
   private static final long serialVersionUID = 1L;
   private JTree tree;
   private ElementTreeInterface frame;
   private DefaultTreeModel defaultTreeModel;
   private TreeNode[] selectedLastPath;

   public ElementTreePanel(ElementTreeInterface var1) {
      this.setLayout(new GridLayout(0, 1, 0, 0));
      this.frame = var1;
      ElementKeyMap.initializeData(GameResourceLoader.getConfigInputFile());
      ElementCategory var2 = ElementKeyMap.getCategoryHirarchy();
      DefaultMutableTreeNode var3 = new DefaultMutableTreeNode("Root");
      this.createNodes(var3, var2);
      this.defaultTreeModel = new DefaultTreeModel(var3);
      this.tree = new JTree(this.defaultTreeModel);
      this.tree.setCellRenderer(new ElementTreeRenderer());
      this.add(this.tree);
      this.tree.getSelectionModel().setSelectionMode(1);
      this.tree.addTreeSelectionListener(var1);
      MouseAdapter var4 = new MouseAdapter() {
         public void mousePressed(MouseEvent var1) {
            int var2 = ElementTreePanel.this.tree.getRowForLocation(var1.getX(), var1.getY());
            TreePath var3 = ElementTreePanel.this.tree.getPathForLocation(var1.getX(), var1.getY());
            if (var2 != -1 && SwingUtilities.isRightMouseButton(var1)) {
               var2 = ElementTreePanel.this.tree.getClosestRowForLocation(var1.getX(), var1.getY());
               ElementTreePanel.this.tree.setSelectionRow(var2);
               DefaultMutableTreeNode var4 = (DefaultMutableTreeNode)var3.getLastPathComponent();
               ElementTreePanel.this.createPopup(var4, var1);
            }

         }
      };
      this.tree.addMouseListener(var4);
      if (ElementEditorFrame.currentInfo != null) {
         TreePath var5;
         if ((var5 = this.find(var3, ElementEditorFrame.currentInfo.getId())) != null) {
            System.err.println("FOUND LAST PATH IN TREE");
            this.tree.setSelectionPath(var5);
            this.tree.scrollPathToVisible(var5);
         } else {
            System.err.println("NOT FOUND LAST PATH IN TREE");
         }
      } else {
         System.err.println("NO CURRENT INFO");
      }
   }

   private TreePath find(DefaultMutableTreeNode var1, short var2) {
      Enumeration var4 = var1.depthFirstEnumeration();

      DefaultMutableTreeNode var3;
      do {
         if (!var4.hasMoreElements()) {
            return null;
         }
      } while(!((var3 = (DefaultMutableTreeNode)var4.nextElement()).getUserObject() instanceof ElementInformation) || ((ElementInformation)var3.getUserObject()).getId() != var2);

      return new TreePath(var3.getPath());
   }

   public static void main(String[] var0) {
   }

   private void createPopup(DefaultMutableTreeNode var1, MouseEvent var2) {
      if (this.frame.hasPopupMenu() && var1 != null && var1.getUserObject() != null) {
         JPopupMenu var3 = new JPopupMenu();
         Object var5;
         if ((var5 = var1.getUserObject()) instanceof ElementInformation) {
            final ElementInformation var6 = (ElementInformation)var5;
            JMenuItem var4 = new JMenuItem("Remove");
            var3.add(var4);
            var4.addActionListener(new ActionListener() {
               public void actionPerformed(ActionEvent var1) {
                  TreePath[] var5;
                  int var2 = (var5 = ElementTreePanel.this.tree.getSelectionPaths()).length;

                  for(int var3 = 0; var3 < var2; ++var3) {
                     TreePath var4 = var5[var3];
                     System.err.println(var4.getLastPathComponent());
                  }

                  ElementTreePanel.this.frame.removeEntry(var6);
               }
            });
            var3.show(var2.getComponent(), var2.getX(), var2.getY());
            return;
         }

         if (var5 instanceof ElementCategory) {
            ((ElementCategory)var5).addContextMenu(this.frame, var3, var2.getComponent());
            var3.show(var2.getComponent(), var2.getX(), var2.getY());
         }
      }

   }

   private DefaultMutableTreeNode createNodes(DefaultMutableTreeNode var1, ElementCategory var2) {
      Iterator var3 = var2.getChildren().iterator();

      DefaultMutableTreeNode var5;
      while(var3.hasNext()) {
         ElementCategory var4 = (ElementCategory)var3.next();
         var5 = new DefaultMutableTreeNode(var4);
         var1.add(this.createNodes(var5, var4));
      }

      var3 = var2.getInfoElements().iterator();

      while(var3.hasNext()) {
         ElementInformation var6 = (ElementInformation)var3.next();
         var5 = new DefaultMutableTreeNode(var6);
         var1.add(var5);
         if (ElementEditorFrame.currentInfo != null) {
            this.selectedLastPath = var5.getPath();
         }
      }

      return var1;
   }

   public ElementInformation getSelectedItem() {
      DefaultMutableTreeNode var1;
      return this.tree.getLastSelectedPathComponent() instanceof DefaultMutableTreeNode && (var1 = (DefaultMutableTreeNode)this.tree.getLastSelectedPathComponent()).getUserObject() instanceof ElementInformation ? (ElementInformation)var1.getUserObject() : null;
   }

   public JTree getTree() {
      return this.tree;
   }

   private void removeFromTree(DefaultMutableTreeNode var1, ElementInformation var2) {
      Enumeration var4 = var1.children();

      while(var4.hasMoreElements()) {
         DefaultMutableTreeNode var3;
         if ((var3 = (DefaultMutableTreeNode)var4.nextElement()).getUserObject() == var2) {
            this.defaultTreeModel.removeNodeFromParent(var3);
            return;
         }

         this.removeFromTree(var3, var2);
      }

   }

   public void removeFromTree(ElementInformation var1) {
      DefaultMutableTreeNode var2 = (DefaultMutableTreeNode)this.tree.getModel().getRoot();
      this.removeFromTree(var2, var1);
   }
}
