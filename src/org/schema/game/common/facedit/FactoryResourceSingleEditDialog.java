package org.schema.game.common.facedit;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;

public class FactoryResourceSingleEditDialog extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private short currentId;
   private JLabel lblUndefined;
   private JSpinner slider;
   private JLabel lblLevel;

   public FactoryResourceSingleEditDialog(final JFrame var1, final FactoryResource var2, final ArrayListModel var3) {
      super(var1, true);
      this.setTitle("Block Level Editor");
      this.setBounds(100, 100, 510, 184);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var4;
      (var4 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0};
      var4.rowHeights = new int[]{0, 0, 0};
      var4.columnWeights = new double[]{0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var4.rowWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      this.contentPanel.setLayout(var4);
      JLabel var6 = new JLabel("Base Element");
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).anchor = 17;
      var5.insets = new Insets(0, 0, 5, 5);
      var5.gridx = 0;
      var5.gridy = 0;
      this.contentPanel.add(var6, var5);
      this.currentId = var2 != null ? var2.type : -1;
      this.lblUndefined = new JLabel(this.currentId > 0 ? ElementKeyMap.getInfo(this.currentId).toString() : "undefined");
      GridBagConstraints var7;
      (var7 = new GridBagConstraints()).weightx = 1.0D;
      var7.insets = new Insets(0, 0, 5, 5);
      var7.gridx = 1;
      var7.gridy = 0;
      this.contentPanel.add(this.lblUndefined, var7);
      JButton var8 = new JButton("Choose");
      (var5 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var5.anchor = 13;
      var5.gridx = 2;
      var5.gridy = 0;
      this.contentPanel.add(var8, var5);
      var8.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            (new ElementChoserDialog(var1, new ElementChoseInterface() {
               public void onEnter(ElementInformation var1x) {
                  FactoryResourceSingleEditDialog.this.currentId = var1x.getId();
                  FactoryResourceSingleEditDialog.this.lblUndefined.setText(FactoryResourceSingleEditDialog.this.currentId > 0 ? ElementKeyMap.getInfo(FactoryResourceSingleEditDialog.this.currentId).toString() : "undefined");
               }
            })).setVisible(true);
         }
      });
      System.err.println("FAC: " + var2);
      this.lblLevel = new JLabel("Count " + (var2 != null ? String.valueOf(var2.count) : "0"));
      (var7 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var7.gridx = 0;
      var7.gridy = 1;
      this.contentPanel.add(this.lblLevel, var7);
      this.slider = new JSpinner();
      this.slider.setValue(var2 != null ? var2.count : 1);
      (var7 = new GridBagConstraints()).fill = 2;
      var7.weightx = 11.0D;
      var7.gridwidth = 2;
      var7.insets = new Insets(0, 0, 0, 5);
      var7.gridx = 1;
      var7.gridy = 1;
      this.contentPanel.add(this.slider, var7);
      (var5 = new GridBagConstraints()).fill = 2;
      var5.weightx = 11.0D;
      var5.gridwidth = 2;
      var5.insets = new Insets(0, 0, 0, 5);
      var5.gridx = 2;
      var5.gridy = 1;
      this.contentPanel.add(this.slider, var5);
      this.slider.addChangeListener(new ChangeListener() {
         public void stateChanged(ChangeEvent var1) {
            assert FactoryResourceSingleEditDialog.this.lblLevel != null;

            FactoryResourceSingleEditDialog.this.lblLevel.setText("Count " + String.valueOf(FactoryResourceSingleEditDialog.this.slider.getValue()));
         }
      });
      JPanel var9;
      (var9 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var9, "South");
      JButton var10;
      (var10 = new JButton("OK")).setActionCommand("OK");
      var9.add(var10);
      this.getRootPane().setDefaultButton(var10);
      var10.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            if (FactoryResourceSingleEditDialog.this.currentId > 0 && (Integer)FactoryResourceSingleEditDialog.this.slider.getValue() > 0) {
               if (var2 != null) {
                  var2.type = FactoryResourceSingleEditDialog.this.currentId;
                  var2.count = (Integer)FactoryResourceSingleEditDialog.this.slider.getValue();
                  var3.dataChanged(var2);
               } else {
                  var3.add(new FactoryResource((Integer)FactoryResourceSingleEditDialog.this.slider.getValue(), FactoryResourceSingleEditDialog.this.currentId));
               }

               FactoryResourceSingleEditDialog.this.repaint();
            }

            FactoryResourceSingleEditDialog.this.dispose();
         }
      });
      (var10 = new JButton("Cancel")).setActionCommand("Cancel");
      var9.add(var10);
      var10.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            FactoryResourceSingleEditDialog.this.dispose();
         }
      });
   }
}
