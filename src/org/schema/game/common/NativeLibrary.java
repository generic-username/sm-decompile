package org.schema.game.common;

final class NativeLibrary {
   private final String name;
   private final Platform platform;
   private final String pathInNativesJar;
   private final String extractedAsFileName;

   public final String getName() {
      return this.name;
   }

   public final Platform getPlatform() {
      return this.platform;
   }

   public final String getExtractedAsName() {
      return this.extractedAsFileName;
   }

   public final String getPathInNativesJar() {
      return this.pathInNativesJar;
   }

   public NativeLibrary(String var1, Platform var2, String var3, String var4) {
      this.name = var1;
      this.platform = var2;
      this.pathInNativesJar = var3;
      this.extractedAsFileName = var4;
   }

   public NativeLibrary(String var1, Platform var2, String var3) {
      this(var1, var2, var3, (String)null);
   }

   static final class Key {
      private final String name;
      private final Platform platform;

      public Key(String var1, Platform var2) {
         this.name = var1;
         this.platform = var2;
      }

      public final int hashCode() {
         return (395 + this.name.hashCode()) * 79 + this.platform.hashCode();
      }

      public final boolean equals(Object var1) {
         NativeLibrary.Key var2 = (NativeLibrary.Key)var1;
         if (!this.name.equals(var2.name)) {
            return false;
         } else {
            return this.platform == var2.platform;
         }
      }
   }
}
