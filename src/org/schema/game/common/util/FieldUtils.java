package org.schema.game.common.util;

import java.util.Arrays;
import java.util.List;

public class FieldUtils {
   private static final long serialVersionUID = 1L;

   public static List getAllFields(List var0, Class var1) {
      List var2 = Arrays.asList(var1.getDeclaredFields());
      var0.addAll(var2);
      if (var1.getSuperclass() != null) {
         getAllFields(var0, var1.getSuperclass());
      }

      return var0;
   }
}
