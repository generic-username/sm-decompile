package org.schema.game.common.util;

import it.unimi.dsi.fastutil.HashCommon;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import org.schema.common.FastMath;

public class FastCopyLongOpenHashSet extends LongOpenHashSet {
   private static final long serialVersionUID = 1L;

   public FastCopyLongOpenHashSet() {
   }

   public FastCopyLongOpenHashSet(int var1) {
      super(var1, 0.75F);
   }

   public static int maxFillFast(int var0, float var1) {
      return FastMath.fastCeil((float)var0 * var1);
   }

   public static int arraySize(int var0, float var1) {
      return nextPowerOfTwo(FastMath.fastCeil((float)var0 / var1));
   }

   public static int nextPowerOfTwo(int var0) {
      if (var0 == 0) {
         return 1;
      } else {
         --var0;
         return (var0 | var0 >> 1 | (var0 | var0 >> 1) >> 2 | (var0 | var0 >> 1 | (var0 | var0 >> 1) >> 2) >> 4 | (var0 | var0 >> 1 | (var0 | var0 >> 1) >> 2 | (var0 | var0 >> 1 | (var0 | var0 >> 1) >> 2) >> 4) >> 8 | (var0 | var0 >> 1 | (var0 | var0 >> 1) >> 2 | (var0 | var0 >> 1 | (var0 | var0 >> 1) >> 2) >> 4 | (var0 | var0 >> 1 | (var0 | var0 >> 1) >> 2 | (var0 | var0 >> 1 | (var0 | var0 >> 1) >> 2) >> 4) >> 8) >> 16) + 1;
      }
   }

   public void deepApplianceCopy(FastCopyLongOpenHashSet var1) {
      assert this.f == var1.f;

      this.mask = var1.mask;
      this.maxFill = var1.maxFill;
      this.n = var1.n;
      this.size = var1.size;
      if (this.used.length < var1.used.length) {
         this.used = new boolean[var1.used.length];
      }

      if (this.key.length < var1.key.length) {
         this.key = new long[var1.key.length];
      }

      System.arraycopy(var1.used, 0, this.used, 0, var1.used.length);
      System.arraycopy(var1.key, 0, this.key, 0, var1.key.length);
   }

   public boolean add(long var1) {
      int var3;
      for(var3 = (int)HashCommon.murmurHash3(var1) & this.mask; this.used[var3]; var3 = var3 + 1 & this.mask) {
         if (this.key[var3] == var1) {
            return false;
         }
      }

      this.used[var3] = true;
      this.key[var3] = var1;
      if (++this.size >= this.maxFill) {
         this.rehash(arraySize(this.size + 1, this.f));
      }

      return true;
   }

   protected void rehash(int var1) {
      int var2 = 0;
      boolean[] var4 = this.used;
      long[] var7 = this.key;
      int var8 = var1 - 1;
      long[] var9 = new long[var1];
      boolean[] var10 = new boolean[var1];

      for(int var11 = this.size; var11-- != 0; ++var2) {
         while(!var4[var2]) {
            ++var2;
         }

         int var3;
         long var5;
         for(var3 = (int)HashCommon.murmurHash3(var5 = var7[var2]) & var8; var10[var3]; var3 = var3 + 1 & var8) {
         }

         var10[var3] = true;
         var9[var3] = var5;
      }

      this.n = var1;
      this.mask = var8;
      this.maxFill = maxFillFast(this.n, this.f);
      this.key = var9;
      this.used = var10;
   }
}
