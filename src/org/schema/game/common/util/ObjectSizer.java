package org.schema.game.common.util;

import org.schema.game.common.data.element.Element;

public final class ObjectSizer {
   private static int fSAMPLE_SIZE = 100;
   private static long fSLEEP_INTERVAL = 100L;

   private static void collectGarbage() {
      try {
         System.gc();
         Thread.currentThread();
         Thread.sleep(fSLEEP_INTERVAL);
         System.runFinalization();
         Thread.currentThread();
         Thread.sleep(fSLEEP_INTERVAL);
      } catch (InterruptedException var0) {
         var0.printStackTrace();
      }
   }

   private static long getMemoryUse() {
      collectGarbage();
      collectGarbage();
      long var0 = Runtime.getRuntime().totalMemory();
      collectGarbage();
      collectGarbage();
      long var2 = Runtime.getRuntime().freeMemory();
      return var0 - var2;
   }

   public static long getObjectSize(Class var0) {
      long var1 = 0L;

      try {
         var0.getConstructor();
      } catch (NoSuchMethodException var7) {
         System.err.println(var0 + " does not have a no-argument constructor.");
         return 0L;
      }

      Object[] var3 = new Object[fSAMPLE_SIZE];

      try {
         long var4 = getMemoryUse();

         for(int var6 = 0; var6 < var3.length; ++var6) {
            var3[var6] = var0.newInstance();
         }

         var1 = (long)Math.round((float)(getMemoryUse() - var4) / 100.0F);
      } catch (Exception var8) {
         System.err.println("Cannot create object using " + var0);
      }

      return var1;
   }

   public static void main(String... var0) {
      Class var3 = Element.class;
      long var1 = getObjectSize(Element.class);
      System.out.println("Approximate size of " + var3 + " objects: " + var1);
   }

   private static void putOutTheGarbage() {
      collectGarbage();
      collectGarbage();
   }
}
