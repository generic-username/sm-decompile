package org.schema.game.common.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.schema.schine.resource.FileExt;

public class FolderZipper {
   private static void addFolderToZip(String var0, String var1, ZipOutputStream var2, String var3, byte[] var4, FolderZipper.ZipCallback var5, FileFilter var6) {
      var0.replace('\\', '/');
      var1.replace('\\', '/');
      FileExt var7 = new FileExt(var1);
      if (var6 == null || var6.accept(var7)) {
         if (var5 != null) {
            var5.update(var7);
         }

         String[] var8 = var7.list();

         try {
            for(int var9 = 0; var9 < var8.length; ++var9) {
               if (var3 == null || !var8[var9].startsWith(var3)) {
                  addToZip(var0 + "/" + var7.getName(), var1 + "/" + var8[var9], var2, var3, var4, var5, var6);
               }
            }

         } catch (Exception var10) {
         }
      }
   }

   private static void addToZip(String var0, String var1, ZipOutputStream var2, String var3, byte[] var4, FolderZipper.ZipCallback var5, FileFilter var6) {
      var0.replace('\\', '/');
      var1.replace('\\', '/');
      FileExt var7 = new FileExt(var1);
      if (var6 == null || var6.accept(var7)) {
         if (var7.isDirectory()) {
            addFolderToZip(var0, var1, var2, var3, var4, var5, var6);
         } else {
            if (var5 != null) {
               var5.update(new FileExt(var1));
            }

            try {
               FileInputStream var9 = new FileInputStream(var1);
               if ((var0 = var0 + "/" + var7.getName()).startsWith("/")) {
                  var0 = var0.substring(1);
               }

               ZipEntry var10 = new ZipEntry(var0);
               var2.putNextEntry(var10);

               int var11;
               while((var11 = var9.read(var4)) > 0) {
                  var2.write(var4, 0, var11);
               }

               var9.close();
            } catch (Exception var8) {
               var8.printStackTrace();
            }
         }
      }
   }

   public static void zipFolder(String var0, String var1, String var2, FileFilter var3) throws IOException {
      zipFolder(var0, var1, var2, (FolderZipper.ZipCallback)null, var3);
   }

   public static void zipFolder(String var0, String var1, String var2, FolderZipper.ZipCallback var3, String var4, FileFilter var5) throws IOException {
      zipFolder(var0, var1, var2, var3, var4, var5, false);
   }

   public static void zipFolder(String var0, String var1, String var2, FolderZipper.ZipCallback var3, String var4, FileFilter var5, boolean var6) throws IOException {
      System.out.println("[ZIP] Zipping folder: " + var0 + " to " + var1 + " (Filter: " + var2 + ")");
      var1.replace('\\', '/');
      var0.replace('\\', '/');
      FileExt var11 = new FileExt(var1);
      FileExt var8 = new FileExt(var0);
      System.out.println("[ZIP] Writing to " + var11.getAbsolutePath());
      BufferedOutputStream var7 = new BufferedOutputStream(new FileOutputStream(var11), 4096);
      ZipOutputStream var12 = new ZipOutputStream(var7);
      byte[] var9 = new byte[4096];
      if (!var6) {
         addFolderToZip(var4, var0, var12, var2, var9, var3, var5);
      } else {
         String[] var13 = var8.list();

         try {
            for(int var14 = 0; var14 < var13.length; ++var14) {
               if (var2 == null || !var13[var14].startsWith(var2)) {
                  addToZip("", var0 + "/" + var13[var14], var12, var2, var9, var3, var5);
               }
            }
         } catch (Exception var10) {
         }
      }

      var12.flush();
      var12.close();
      var7.close();
   }

   public static void zipFolder(String var0, String var1, String var2, FolderZipper.ZipCallback var3, FileFilter var4) throws IOException {
      zipFolder(var0, var1, var2, var3, "", var4);
   }

   public interface ZipCallback {
      void update(File var1);
   }
}
