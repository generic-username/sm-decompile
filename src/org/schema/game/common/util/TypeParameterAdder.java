package org.schema.game.common.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class TypeParameterAdder {
   public static void main(String[] var0) throws IOException {
      TypeParameterAdder var1;
      (var1 = new TypeParameterAdder()).handl(new File("./src"));
      var1.handl(new File("./schine/src"));
   }

   public void handl(File var1) throws IOException {
      if (var1.isDirectory()) {
         File[] var8;
         int var9 = (var8 = var1.listFiles()).length;

         for(int var7 = 0; var7 < var9; ++var7) {
            var1 = var8[var7];
            this.handl(var1);
         }

      } else {
         if (var1.getName().endsWith(".java") && !var1.getName().equals("TypeParameterAdder.java")) {
            System.err.println("HANDLE: " + var1);
            BufferedReader var2 = new BufferedReader(new FileReader(var1));
            StringBuffer var3 = new StringBuffer();
            String var4 = null;

            while((var4 = var2.readLine()) != null) {
               var3.append(var4);
               var3.append("\n");
            }

            var2.close();
            String var6;
            if ((var6 = this.modify(var3)) != null) {
               System.err.println("MOD: " + var1.getName());
               var1.delete();
               BufferedWriter var5;
               (var5 = new BufferedWriter(new FileWriter(var1))).write(var6);
               var5.close();
            }
         }

      }
   }

   private String modify(StringBuffer var1) {
      boolean var2 = false;

      for(String var3 = "StaticStates<>(false, true)"; var1.indexOf(var3) >= 0; var2 = true) {
         var1.replace(var1.indexOf(var3), var1.indexOf(var3) + var3.length(), "StaticStates<Boolean>(false, true)");
      }

      while(true) {
         int var7;
         while((var7 = var1.indexOf("<>")) >= 0) {
            int var4 = var1.indexOf(";", var7);

            int var5;
            for(var5 = var7; var5 > 0 && !var1.substring(var5 - 1, var5).equals(";") && !var1.substring(var5 - 1, var5).equals("{") && !var1.substring(var5 - 1, var5).equals("\n"); --var5) {
            }

            String var8 = var1.substring(var5, var4);
            System.err.println("LINE: " + var8.trim());
            if (var8.trim().startsWith("//")) {
               var1.replace(var7, var7 + 2, "< . >");
            } else if (var8.contains("=")) {
               String var6;
               if ((var6 = var8.split("=", 2)[0].trim()).contains(".")) {
                  var6 = var6.substring(var6.lastIndexOf(".") + 1).trim();
               }

               var4 = var6.indexOf("<");
               var5 = var6.lastIndexOf(">");
               if (var4 >= 0 && var5 >= 0) {
                  var6 = var6.substring(var4, var5 + 1);
                  var1.replace(var7, var7 + 2, var6);
                  var2 = true;
               } else {
                  this.search(var6, 0, var1, var7);
                  var2 = true;
               }
            } else {
               var1.replace(var7, var7 + 2, "");
               var2 = true;
            }
         }

         if (var2) {
            return var1.toString();
         }

         return null;
      }
   }

   private void search(String var1, int var2, StringBuffer var3, int var4) {
      while((var2 = var3.indexOf(var1.trim(), var2)) >= 0) {
         int var5;
         for(var5 = var2 + var1.trim().length(); var2 > 0 && !var3.substring(var2 - 1, var2).equals(";") && !var3.substring(var2 - 1, var2).equals("{"); --var2) {
         }

         int var6;
         if ((var6 = var3.indexOf(";", var2)) < 0) {
            this.search(var1, var5, var3, var4);
         }

         String var8;
         if (!(var8 = var3.substring(var2, var6)).contains("=") && var8.contains("<") && var8.contains(">")) {
            int var7 = var8.indexOf("<");
            var5 = var8.lastIndexOf(">");
            if (var7 >= 0 && var5 >= 0) {
               var1 = var8.substring(var7, var5 + 1);
               System.err.println("CONTENT2: " + var1);
               var3.replace(var4, var4 + 2, var1);
            }

            return;
         }

         var2 = var5;
         var1 = var1;
         this = this;
      }

      var3.replace(var4, var4 + 2, "");
   }
}
