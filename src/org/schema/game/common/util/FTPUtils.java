package org.schema.game.common.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class FTPUtils {
   public static void download(String var0, String var1, String var2, String var3, File var4) throws MalformedURLException, IOException {
      if (var0 != null && var3 != null && var4 != null) {
         StringBuffer var5 = new StringBuffer("ftp://");
         if (var1 != null && var2 != null) {
            var5.append(var1);
            var5.append(':');
            var5.append(var2);
            var5.append('@');
         }

         var5.append(var0);
         var5.append('/');
         var5.append(var3);
         var5.append(";type=i");
         BufferedInputStream var17 = null;
         BufferedOutputStream var18 = null;
         boolean var11 = false;

         try {
            var11 = true;
            URLConnection var19 = (new URL(var5.toString())).openConnection();
            var17 = new BufferedInputStream(var19.getInputStream());
            var18 = new BufferedOutputStream(new FileOutputStream(var4.getName()));

            int var20;
            while((var20 = var17.read()) != -1) {
               var18.write(var20);
            }

            var11 = false;
         } finally {
            if (var11) {
               if (var17 != null) {
                  try {
                     var17.close();
                  } catch (IOException var13) {
                     var13.printStackTrace();
                  }
               }

               if (var18 != null) {
                  try {
                     var18.close();
                  } catch (IOException var12) {
                     var12.printStackTrace();
                  }
               }

            }
         }

         try {
            var17.close();
         } catch (IOException var15) {
            var15.printStackTrace();
         }

         try {
            var18.close();
         } catch (IOException var14) {
            var14.printStackTrace();
         }
      } else {
         System.out.println("Input not available");
      }
   }

   public static void upload(String var0, String var1, String var2, String var3, File var4) throws MalformedURLException, IOException {
      if (var0 != null && var3 != null && var4 != null) {
         StringBuffer var5 = new StringBuffer("ftp://");
         if (var1 != null && var2 != null) {
            var5.append(var1);
            var5.append(':');
            var5.append(var2);
            var5.append('@');
         }

         var5.append(var0);
         var5.append('/');
         var5.append(var3);
         var5.append(";type=i");
         BufferedInputStream var17 = null;
         BufferedOutputStream var18 = null;
         boolean var11 = false;

         try {
            var11 = true;
            URLConnection var19 = (new URL(var5.toString())).openConnection();
            var18 = new BufferedOutputStream(var19.getOutputStream());
            var17 = new BufferedInputStream(new FileInputStream(var4));

            int var20;
            while((var20 = var17.read()) != -1) {
               var18.write(var20);
            }

            var11 = false;
         } finally {
            if (var11) {
               if (var17 != null) {
                  try {
                     var17.close();
                  } catch (IOException var13) {
                     var13.printStackTrace();
                  }
               }

               if (var18 != null) {
                  try {
                     var18.close();
                  } catch (IOException var12) {
                     var12.printStackTrace();
                  }
               }

            }
         }

         try {
            var17.close();
         } catch (IOException var15) {
            var15.printStackTrace();
         }

         try {
            var18.close();
         } catch (IOException var14) {
            var14.printStackTrace();
         }
      } else {
         System.out.println("Input not available.");
      }
   }
}
