package org.schema.game.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class DataUtil {
   public static final String dataPath;

   public static void copy(File var0, File var1) throws IOException {
      FileInputStream var4 = new FileInputStream(var0);
      FileOutputStream var5 = new FileOutputStream(var1);
      byte[] var2 = new byte[1024];

      int var3;
      while((var3 = var4.read(var2)) > 0) {
         var5.write(var2, 0, var3);
      }

      var4.close();
      var5.close();
   }

   static {
      dataPath = "data" + File.separator;
   }
}
