package org.schema.game.common.util;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.Desktop.Action;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import org.schema.game.common.crashreporter.CrashReporter;
import org.schema.schine.resource.FileExt;

public class GuiErrorHandler {
   public static void exitInfoDialog(String var0) {
      if (!GraphicsEnvironment.isHeadless()) {
         Object[] var1 = new Object[]{"OK"};
         String var2 = "Information";
         final JFrame var3;
         (var3 = new JFrame(var2)).setUndecorated(true);
         SwingUtilities.invokeLater(new Runnable() {
            public final void run() {
               var3.setVisible(true);
            }
         });
         Dimension var4 = Toolkit.getDefaultToolkit().getScreenSize();
         var3.setLocation(var4.width / 2, var4.height / 2);
         switch(JOptionPane.showOptionDialog(var3, var0, var2, 0, 1, (Icon)null, var1, var1[0])) {
         case 0:
            System.err.println("Exiting because of exit info dialog");

            try {
               CrashReporter.createThreadDump();
            } catch (IOException var5) {
               var5.printStackTrace();
            }

            try {
               throw new Exception("System.exit() called");
            } catch (Exception var6) {
               var6.printStackTrace();
               System.exit(0);
            }
         default:
            var3.dispose();
         }
      }
   }

   public static void processErrorDialogException(Exception var0) {
      if (!GraphicsEnvironment.isHeadless()) {
         Object[] var1 = new Object[]{"Continue", "Create a help ticket", "Exit"};
         var0.printStackTrace();
         String var2 = "Critical Error";
         JFrame var3;
         (var3 = new JFrame(var2)).setUndecorated(true);
         var3.setVisible(true);
         var3.setAlwaysOnTop(true);
         Dimension var4 = Toolkit.getDefaultToolkit().getScreenSize();
         String var5 = "";
         if (var0.getMessage() != null && var0.getMessage().contains("Database lock acquisition")) {
            var5 = var5 + "\n\nIMPORTANT NOTE: this crash happens when there is still an instance of the game running\ncheck your process manager for \"javaw.exe\" and terminate it, or restart your computer to solve this problem.";
         }

         if (var5.length() == 0) {
            var5 = "To get help, please create a ticket at help.star-made.org\n\nIf this message is about a File not Found, please try to repair or reinstall with your launcher";
         }

         var3.setLocation(var4.width / 2, var4.height / 2);
         switch(JOptionPane.showOptionDialog(var3, var0.getClass().getSimpleName() + ": " + var0.getMessage() + "\n\n " + var5 + "\n\n", var2, 1, 0, (Icon)null, var1, var1[0])) {
         case 0:
         case 3:
         default:
            break;
         case 1:
            try {
               openWebpage(new URL("http://help.star-made.org"));
            } catch (MalformedURLException var7) {
               var7.printStackTrace();
            }
            break;
         case 2:
            System.err.println("[GLFrame] Error Message: " + var0.getMessage());

            try {
               CrashReporter.createThreadDump();
            } catch (IOException var6) {
               var6.printStackTrace();
            }

            try {
               throw new Exception("System.exit() called");
            } catch (Exception var8) {
               var8.printStackTrace();
               System.exit(0);
            }
         }

         var3.dispose();
      }
   }

   public static void openWebpage(URI var0) {
      Desktop var10000 = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
      Desktop var1 = var10000;
      if (var10000 != null && var1.isSupported(Action.BROWSE)) {
         try {
            var1.browse(var0);
            return;
         } catch (Exception var2) {
            var2.printStackTrace();
         }
      }

   }

   public static void openWebpage(URL var0) {
      try {
         openWebpage(var0.toURI());
      } catch (URISyntaxException var1) {
         var1.printStackTrace();
      }
   }

   public static void processNormalErrorDialogException(Exception var0, boolean var1) {
      if (!GraphicsEnvironment.isHeadless()) {
         Object[] var2 = new Object[]{"Ok", "Exit"};
         var0.printStackTrace();
         String var3 = "Error";
         final JFrame var4;
         (var4 = new JFrame(var3)).setAlwaysOnTop(true);
         var4.setUndecorated(true);
         SwingUtilities.invokeLater(new Runnable() {
            public final void run() {
               var4.setVisible(true);
            }
         });
         Dimension var5 = Toolkit.getDefaultToolkit().getScreenSize();
         var4.setLocation(var5.width / 2, var5.height / 2);
         switch(JOptionPane.showOptionDialog(var4, (var1 ? var0.getClass().getSimpleName() : "") + var0.getMessage(), var3, 1, 0, (Icon)null, var2, var2[0])) {
         case 1:
            System.err.println("[GLFrame] Error Message: " + var0.getMessage());

            try {
               CrashReporter.createThreadDump();
            } catch (IOException var6) {
               var6.printStackTrace();
            }

            try {
               throw new Exception("System.exit() called");
            } catch (Exception var7) {
               var7.printStackTrace();
               System.exit(0);
            }
         case 0:
         case 2:
         default:
            var4.dispose();
         }
      }
   }

   public static void startCrashReporterInstance(final String[] var0) {
      SwingUtilities.invokeLater(new Runnable() {
         public final void run() {
            try {
               String var1 = "";

               for(int var2 = 0; var2 < var0.length; ++var2) {
                  var1 = var1 + " " + var0[var2];
               }

               String var7 = "./CrashAndBugReport.jar";
               FileExt var8 = new FileExt(var7);
               String[] var5 = new String[]{"java", "-jar", var8.getAbsolutePath(), var1};
               System.err.println("RUNNING COMMAND: " + Arrays.toString(var5));
               ProcessBuilder var6;
               (var6 = new ProcessBuilder(var5)).environment();
               var6.start();
               System.err.println("Exiting because of gui error handler start crash dialog");

               try {
                  CrashReporter.createThreadDump();
               } catch (IOException var3) {
                  var3.printStackTrace();
               }
            } catch (IOException var4) {
               var4.printStackTrace();
               GuiErrorHandler.processErrorDialogException(var4);
            }
         }
      });
   }
}
