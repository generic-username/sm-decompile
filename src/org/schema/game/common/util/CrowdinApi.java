package org.schema.game.common.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.commons.io.FileUtils;
import org.schema.schine.common.language.LanguageReader;
import org.schema.schine.common.language.Translation;
import org.schema.schine.resource.FileExt;
import org.xml.sax.SAXException;

public class CrowdinApi {
   public static final String URL = "https://api.crowdin.com/api/project/";
   public static final String FOLDER = "./crowdin/";
   public static final String DATA_FOLDER = "./data/language/";

   public static Properties loadProperties() throws IOException {
      Properties var0 = new Properties();
      FileInputStream var1 = null;
      boolean var5 = false;

      try {
         var5 = true;
         var1 = new FileInputStream("./crowdin.properties");
         var0.load(var1);
         var5 = false;
      } finally {
         if (var5) {
            if (var1 != null) {
               try {
                  var1.close();
               } catch (IOException var6) {
                  var6.printStackTrace();
               }
            }

         }
      }

      try {
         var1.close();
      } catch (IOException var7) {
         var7.printStackTrace();
      }

      return var0;
   }

   public static String getGETUrl(Properties var0, String var1) throws IOException {
      String var2;
      if ((var2 = var0.getProperty("PROJECT_ID")) == null) {
         throw new IOException("No project ID in properties");
      } else {
         return "https://api.crowdin.com/api/project/" + var2 + "/" + var1;
      }
   }

   public static String getApiKeyEncode(Properties var0) throws IOException {
      String var1;
      if ((var1 = var0.getProperty("API_KEY")) == null) {
         throw new IOException("No API_KEY in properties");
      } else {
         return URLEncoder.encode("key", "UTF-8") + "=" + URLEncoder.encode(var1, "UTF-8");
      }
   }

   public static void main(String[] var0) throws IOException, SAXException, ParserConfigurationException, TransformerException {
      export();

      try {
         updateLanguage("German", "de", (String)null);
      } catch (Exception var10) {
         var10.printStackTrace();
      }

      try {
         updateLanguage("Polish", "pl", (String)null);
      } catch (Exception var9) {
         var9.printStackTrace();
      }

      try {
         updateLanguage("Japanese", "ja", "data/font/NotoSansCJKtc-Regular.ttf");
      } catch (Exception var8) {
         var8.printStackTrace();
      }

      try {
         updateLanguage("Russian", "ru", "data/font/NotoSansCJKtc-Regular.ttf");
      } catch (Exception var7) {
         var7.printStackTrace();
      }

      try {
         updateLanguage("Chinese Traditional", "zh-TW", "data/font/NotoSansCJKtc-Regular.ttf");
      } catch (Exception var6) {
         var6.printStackTrace();
      }

      try {
         updateLanguage("French", "fr", (String)null);
      } catch (Exception var5) {
         var5.printStackTrace();
      }

      try {
         updateLanguage("Spanish", "es-ES", (String)null);
      } catch (Exception var4) {
         var4.printStackTrace();
      }

      try {
         updateLanguage("Portuguese Brazilian", "pt-BR", (String)null);
      } catch (Exception var3) {
         var3.printStackTrace();
      }

      try {
         updateLanguage("Chinese Simplified", "zh-CN", "data/font/NotoSansCJKtc-Regular.ttf");
      } catch (Exception var2) {
         var2.printStackTrace();
      }

      try {
         updateLanguage("Czech", "cs", (String)null);
      } catch (Exception var1) {
         var1.printStackTrace();
      }

      FileUtils.deleteDirectory(new FileExt("./crowdin/"));
   }

   public static void updateLanguage(String var0, String var1) throws IOException, SAXException, ParserConfigurationException, TransformerException {
      updateLanguage(var0, var1, (String)null);
   }

   public static void updateLanguage(String var0, String var1, String var2) throws IOException, SAXException, ParserConfigurationException, TransformerException {
      download(var1);
      ArrayList var3 = new ArrayList();
      FileExt var6 = new FileExt("./crowdin/" + var1 + ".zip");
      HashMap var4 = new HashMap();
      LanguageReader.loadLangFile(new FileExt("./data/language/defaultPack.xml"), var4);
      Iterator var7 = var4.values().iterator();

      while(var7.hasNext()) {
         Translation var5 = (Translation)var7.next();
         var3.add(var5);
      }

      LanguageReader.importFileXML(var6, var3);
      if (var3.isEmpty()) {
         throw new IOException("Nothing imported from " + var6.getName());
      } else {
         (new FileExt("./data/language/" + var0 + "/")).mkdirs();
         FileExt var8 = new FileExt("./data/language/" + var0 + "/pack.xml");
         System.err.println("EXPORTING TO SMLANG FORMAT: " + var8.getAbsolutePath() + " ...");
         LanguageReader.save(var8, var0, var2, var3);
         LanguageReader.importFileTutorial(var6, "./data/language/" + var0 + "/");
      }
   }

   public static void download(String var0) throws IOException {
      Properties var1 = loadProperties();
      String var2 = getGETUrl(var1, "download") + "/" + URLEncoder.encode(var0 + ".zip", "UTF-8") + "?" + getApiKeyEncode(var1);
      System.err.println("DOWNLOADING URL: " + var2);
      (new FileExt("./crowdin/")).mkdir();
      FileUtils.copyURLToFile(new URL(var2), new FileExt("./crowdin/" + var0 + ".zip"));
   }

   public static void export() throws IOException {
      Properties var0 = loadProperties();
      String var3 = getGETUrl(var0, "export") + "?" + getApiKeyEncode(var0);
      System.err.println("CALLING URL: " + var3);
      HttpURLConnection var4;
      (var4 = (HttpURLConnection)(new URL(var3)).openConnection()).setDoOutput(true);
      var4.setReadTimeout(520000);
      var4.setRequestMethod("GET");
      System.currentTimeMillis();
      var4.getResponseCode();
      BufferedReader var5 = new BufferedReader(new InputStreamReader(var4.getInputStream()));
      StringBuffer var2 = new StringBuffer();

      String var1;
      while((var1 = var5.readLine()) != null) {
         System.err.println(var1);
         var2.append(var1);
      }

      System.currentTimeMillis();
      var5.close();
   }
}
