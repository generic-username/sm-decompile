package org.schema.game.common.util;

import com.bulletphysics.collision.narrowphase.ManifoldPoint;
import org.schema.schine.network.objects.Sendable;

public interface Collisionable {
   boolean needsManifoldCollision();

   void onCollision(ManifoldPoint var1, Sendable var2);
}
