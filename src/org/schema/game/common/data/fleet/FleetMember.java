package org.schema.game.common.data.fleet;

import com.bulletphysics.linearmath.Transform;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.map.MapControllerManager;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.data.gamemap.entry.AbstractMapEntry;
import org.schema.game.client.view.effects.ConstantIndication;
import org.schema.game.client.view.effects.Indication;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionNewsPost;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.world.EntityUID;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.SelectableSprite;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.server.ServerStateInterface;

public class FleetMember implements SerialializationInterface {
   private final StateInterface state;
   private Vector3i sector;
   private long dockedToId;
   public String UID;
   public String name;
   public String command;
   private boolean changed;
   private long dockedToDbRootId;
   public long entityDbId;
   private int factionId;
   public Vector3i moveTarget;
   public int tmpListIndex;
   private FleetUnloadedAction currentAction;
   public final FleetMember.FleetMemberMapIndication mapEntry;
   private float lastShipPercent;
   private boolean attackedTriggered;
   private int dockedToFaction;

   public FleetMember(StateInterface var1) {
      this.sector = new Vector3i();
      this.command = "";
      this.dockedToDbRootId = -1L;
      this.state = var1;
      this.mapEntry = this.isOnServer() ? null : new FleetMember.FleetMemberMapIndication();
   }

   public FleetMember(SegmentController var1) {
      this(var1.getState());

      assert this.isOnServer();

      this.UID = var1.getUniqueIdentifier();
      this.name = var1.getRealName();
      this.entityDbId = var1.dbId;
      Sector var2 = ((GameServerState)this.state).getUniverse().getSector(var1.getSectorId());
      this.sector.set(var2.pos);
      this.setDockedToDbRootId(var1.railController.isDocked() ? var1.railController.getRoot().dbId : -1L);
      this.dockedToId = var1.railController.isDocked() ? var1.railController.previous.rail.getSegmentController().dbId : -1L;
   }

   public FleetMember(GameServerState var1, long var2, String var4, String var5, Vector3i var6, long var7, long var9) {
      this((StateInterface)var1);

      assert this.isOnServer();

      this.UID = var4;
      this.name = var5;
      this.entityDbId = var2;
      this.setDockedToDbRootId(var9);
      this.dockedToId = var7;
      this.sector.set(var6);
   }

   public boolean isLoaded() {
      return this.state.getLocalAndRemoteObjectContainer().getUidObjectMap().containsKey(this.UID);
   }

   public void updateServerData() {
      assert this.isOnServer();

      if (this.isOnServer()) {
         if (this.isLoaded()) {
            SimpleTransformableSendableObject var4 = (SimpleTransformableSendableObject)this.state.getLocalAndRemoteObjectContainer().getUidObjectMap().get(this.UID);
            RemoteSector var5 = (RemoteSector)this.state.getLocalAndRemoteObjectContainer().getLocalObjects().get(var4.getSectorId());
            if (!this.sector.equals(var5.getServerSector().pos)) {
               this.sector.set(var5.getServerSector().pos);
               this.changed = true;
            }

            if (!this.name.equals(var4.getRealName())) {
               this.name = var4.getRealName();
               this.changed = true;
            }

            return;
         }

         try {
            List var1;
            if ((var1 = ((GameServerState)this.state).getDatabaseIndex().getTableManager().getEntityTable().getByUIDExact(DatabaseEntry.removePrefixWOException(this.UID), 1)).size() > 0) {
               DatabaseEntry var2 = (DatabaseEntry)var1.get(0);
               if (!this.name.equals(var2.realName)) {
                  this.name = var2.realName;
                  this.changed = true;
               }

               if (!this.name.equals(var2.sectorPos)) {
                  this.sector.set(var2.sectorPos);
                  this.changed = true;
               }
            }

            return;
         } catch (SQLException var3) {
            var3.printStackTrace();
         }
      }

   }

   public Vector3i getSector() {
      return this.sector;
   }

   public int getFactionId() {
      SegmentController var1;
      if ((var1 = this.getLoaded()) != null) {
         this.factionId = var1.getFactionId();
      }

      return this.factionId;
   }

   public boolean isOnServer() {
      return this.state instanceof ServerStateInterface;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeUTF(this.name);
      var1.writeUTF(this.UID);
      var1.writeLong(this.entityDbId);
      var1.writeUTF(this.command);
      this.sector.serialize(var1);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.name = var1.readUTF();
      this.UID = var1.readUTF();
      this.entityDbId = var1.readLong();
      this.command = var1.readUTF();
      this.sector.deserialize(var1);
   }

   public int hashCode() {
      return 31 + (int)(this.entityDbId ^ this.entityDbId >>> 32);
   }

   public boolean equals(Object var1) {
      FleetMember var2 = (FleetMember)var1;
      return this.entityDbId == var2.entityDbId;
   }

   public String toString() {
      return "FleetMember [state=" + this.state + ", sector=" + this.sector + ", UID=" + this.UID + ", name=" + this.name + ", command=" + this.command + ", changed=" + this.changed + ", entityDbId=" + this.entityDbId + "]";
   }

   public void apply(FleetMember var1) {
      assert this.UID.equals(var1.UID);

      this.sector.set(var1.sector);
      this.name = var1.name;
      this.command = var1.command;
   }

   public SegmentController getLoaded() {
      Sendable var1;
      return (var1 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getUidObjectMap().get(this.UID)) != null && var1 instanceof SegmentController ? (SegmentController)var1 : null;
   }

   public String getName() {
      return this.name;
   }

   public void moveUnloadedTowardsGoal(Fleet var1, Vector3i var2) {
      assert var1.isCached();

      if (!this.isLoaded() && !this.isDocked()) {
         Vector3i var3 = new Vector3i();
         Vector3i var4 = new Vector3i();
         Vector3i var5 = new Vector3i();
         var3.set(this.getSector());
         double var6 = -1.0D;
         int var8 = -1;

         for(int var9 = 0; var9 < 6; ++var9) {
            var4.set(var3);
            var4.add(Element.DIRECTIONSi[var9]);
            var4.sub(var2);
            double var10 = var4.lengthSquaredDouble();
            if (var6 < 0.0D || var10 < var6) {
               var5.set(var3);
               var5.add(Element.DIRECTIONSi[var9]);
               var6 = var10;
               var8 = var9;
            }
         }

         if (var6 >= 0.0D) {
            this.sectorMoveUnsave(var1, var5, var8);
         }
      }

   }

   private void sectorMoveUnsave(Fleet var1, Vector3i var2, int var3) {
      assert var1.isCached();

      Vector3f var4;
      try {
         (var4 = new Vector3f()).set(Element.DIRECTIONSf[var3]);
         var4.negate();
         var4.scale(((GameStateInterface)this.state).getSectorSize() * 0.5F * 0.88F);
         ((GameServerState)this.state).getDatabaseIndex().getTableManager().getEntityTable().changeSectorForEntity(this.entityDbId, var2, var4, true);
      } catch (SQLException var5) {
         var4 = null;
         var5.printStackTrace();
      }

      if (!this.sector.equals(var2)) {
         this.onSectorChange(this.sector, var2);
         this.sector.set(var2);
         ((GameServerState)this.state).getFleetManager().submitSectorChangeToClients(this);
      }

      for(int var7 = 0; var7 < var1.getMembers().size(); ++var7) {
         FleetMember var6;
         if ((var6 = (FleetMember)var1.getMembers().get(var7)) != this && var6.getDockedToRootDbId() == this.entityDbId) {
            var6.sector.set(this.sector);
            ((GameServerState)this.state).getFleetManager().submitSectorChangeToClients(var6);
         }
      }

      this.checkLoadPhysicalWithDocks();
   }

   private void checkLoadPhysicalWithDocks() {
      Sector var1;
      if ((var1 = ((GameServerState)this.state).getUniverse().getSectorWithoutLoading(this.getSector())) != null) {
         System.err.println("[SERVER][FLEET_MEMBER] NOW SECTOR != NULL: NOW LOADING PHYSICALLY: " + this.UID);
         this.loadEntityInSectorWithDocks(var1);
      }

   }

   private boolean isDocked() {
      return this.dockedToDbRootId > 0L;
   }

   private void loadEntityInSectorWithDocks(Sector var1) {
      assert var1 != null;

      var1.setActive(true);

      try {
         EntityUID var2;
         (var2 = new EntityUID(this.UID, DatabaseEntry.getEntityType(this.UID), this.entityDbId)).spawnedOnlyInDb = true;
         Sendable var7 = var1.loadEntitiy((GameServerState)this.state, var2);
         System.err.println("[SERVER][FLEET_MEMBER] LOADED PHYSICALLY ROOT: " + var7);
         List var3 = ((GameServerState)this.state).getDatabaseIndex().getTableManager().getEntityTable().loadByDockedEntity(this.entityDbId);
         int var4 = 0;

         for(Iterator var8 = var3.iterator(); var8.hasNext(); ++var4) {
            EntityUID var5 = (EntityUID)var8.next();
            Sendable var9 = var1.loadEntitiy((GameServerState)this.state, var5);
            System.err.println("[SERVER][FLEET_MEMBER] LOADED PHYSICALLY DOCK #" + var4 + ": " + var9);
         }

         assert !(var7 instanceof SegmentController) || ((SegmentController)var7).needsPositionCheckOnLoad : var7;

      } catch (Exception var6) {
         System.err.println("FAILED TO LOAD ENTITY: " + this.UID);
         var6.printStackTrace();
      }
   }

   public void moveRequestUnloaded(Fleet var1, Vector3i var2) {
      assert var1.isCached();

      if (this.currentAction != null && this.currentAction instanceof FleetUnloadedActionMoveTo) {
         ((FleetUnloadedActionMoveTo)this.currentAction).setTarget(var2);
      } else {
         this.currentAction = new FleetUnloadedActionMoveTo(this, var1, var2);
      }

      this.currentAction.fleet = var1;
      ((FleetStateInterface)this.state).getFleetManager().addUpdateAction(this.currentAction);
   }

   public long getDockedToRootDbId() {
      SegmentController var1;
      if ((var1 = this.getLoaded()) != null) {
         this.setDockedToDbRootId(var1.railController.isDockedAndExecuted() ? var1.railController.getRoot().dbId : -1L);
         this.dockedToId = var1.railController.isDocked() ? var1.railController.previous.rail.getSegmentController().dbId : -1L;
         this.dockedToFaction = var1.railController.getRoot().getFactionId();
      }

      return this.dockedToDbRootId;
   }

   public String getPickupPoint() {
      if (this.isLoaded()) {
         return ((Ship)this.getLoaded()).lastPickupAreaUsed != Long.MIN_VALUE ? ElementCollection.getPosFromIndex(((Ship)this.getLoaded()).lastPickupAreaUsed, new Vector3i()).toStringPure() : "NONE";
      } else {
         return "N/A (unloaded)";
      }
   }

   public float getEngagementRange() {
      SegmentController var1;
      return (var1 = this.getLoaded()) != null ? ((ShipAIEntity)((Ship)var1).getAiConfiguration().getAiEntityState()).getShootingRange() : 0.0F;
   }

   public float getShipPercent() {
      SegmentController var1;
      return (var1 = this.getLoaded()) != null ? (float)((Ship)var1).getHpController().getHpPercent() : 0.0F;
   }

   public void setFactionId(int var1) {
      this.factionId = var1;
   }

   public boolean checkAttacked() {
      if (this.getLoaded() != null) {
         if (this.lastShipPercent <= 0.0F) {
            this.lastShipPercent = this.getShipPercent();
         } else if (this.getShipPercent() < this.lastShipPercent && !this.isAttackedTriggered()) {
            this.setAttackedTriggered(true);
            return true;
         }
      }

      return false;
   }

   public boolean isAttackedTriggered() {
      return this.attackedTriggered;
   }

   public void setAttackedTriggered(boolean var1) {
      this.attackedTriggered = var1;
   }

   public void onSectorChange(Vector3i var1, Vector3i var2) {
      Vector3i var3 = new Vector3i();
      Vector3i var4 = new Vector3i();
      StellarSystem.getPosFromSector(var1, var3);
      StellarSystem.getPosFromSector(var2, var4);
      if (!var3.equals(var4)) {
         try {
            StellarSystem var6;
            Faction var9;
            if ((var6 = ((GameServerState)this.state).getUniverse().getStellarSystemFromStellarPos(var4)).getOwnerFaction() != 0 && var6.getOwnerFaction() != this.getFactionId() && (var9 = ((GameServerState)this.state).getFactionManager().getFaction(var6.getOwnerFaction())) != null) {
               FactionRelation.RType var10 = ((GameServerState)this.state).getFactionManager().getRelation(var9.getIdFaction(), this.getFactionId());
               switch(var10) {
               case ENEMY:
                  String var11 = StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETMEMBER_1, var6.getName(), var6.getPos());
                  Object[] var7 = new Object[]{193, var6.getName(), var6.getPos()};
                  var9.broadcastMessage(var7, 2, (GameServerState)this.state);
                  FactionNewsPost var8;
                  (var8 = new FactionNewsPost()).set(var9.getIdFaction(), Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETMEMBER_2, System.currentTimeMillis(), Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETMEMBER_3, var11, 0);
                  ((GameServerState)this.state).getFactionManager().addNewsPostServer(var8);
                  return;
               case FRIEND:
                  var9.broadcastMessage(new Object[]{194, var6.getName(), var6.getPos()}, 1, (GameServerState)this.state);
                  return;
               case NEUTRAL:
                  var9.broadcastMessage(new Object[]{195, var6.getName(), var6.getPos()}, 1, (GameServerState)this.state);
               }
            }

            return;
         } catch (IOException var5) {
            var5.printStackTrace();
         }
      }

   }

   public void onRemovedFromFleet() {
      if (this.isLoaded()) {
         this.getLoaded().onRevealingAction();
      }

   }

   public void setDockedToDbRootId(long var1) {
      this.dockedToDbRootId = var1;
   }

   public class FleetMemberMapIndication extends AbstractMapEntry implements SelectableSprite {
      private Indication indication;
      private Vector4f color = new Vector4f(0.3F, 0.8F, 0.2F, 0.8F);
      private Vector3f posf = new Vector3f();
      private boolean drawIndication;
      private float selectDepth;

      public void drawPoint(boolean var1, int var2, Vector3i var3) {
         if (var1) {
            float var4 = 1.0F;
            if (!this.include(var2, var3)) {
               var4 = 0.1F;
            }

            GlUtil.glColor4f(0.9F, 0.1F, 0.1F, var4);
         }

         GL11.glBegin(0);
         GL11.glVertex3f(this.getPos().x, this.getPos().y, this.getPos().z);
         GL11.glEnd();
      }

      public Indication getIndication(Vector3i var1) {
         Vector3f var3 = this.getPos();
         if (this.indication == null) {
            Transform var2;
            (var2 = new Transform()).setIdentity();
            this.indication = new ConstantIndication(var2, FleetMember.this.name + " " + FleetMember.this.getSector());
         }

         this.indication.setText(FleetMember.this.name + " " + FleetMember.this.getSector());
         float var10001 = var3.x - 50.0F;
         float var10002 = var3.y - 50.0F;
         this.indication.getCurrentTransform().origin.set(var10001, var10002, var3.z - 50.0F);
         return this.indication;
      }

      public int getType() {
         return SimpleTransformableSendableObject.EntityType.SHIP.ordinal();
      }

      public void setType(byte var1) {
      }

      public boolean include(int var1, Vector3i var2) {
         return true;
      }

      public Vector4f getColor() {
         return this.color;
      }

      public float getScale(long var1) {
         return 0.1F;
      }

      public int getSubSprite(Sprite var1) {
         return SimpleTransformableSendableObject.EntityType.SHIP.mapSprite;
      }

      public boolean canDraw() {
         return true;
      }

      public Vector3f getPos() {
         this.posf.set((float)FleetMember.this.getSector().x / 16.0F * 100.0F, (float)FleetMember.this.getSector().y / 16.0F * 100.0F, (float)FleetMember.this.getSector().z / 16.0F * 100.0F);
         return this.posf;
      }

      public boolean isDrawIndication() {
         return this.drawIndication;
      }

      public void setDrawIndication(boolean var1) {
         this.drawIndication = var1;
      }

      protected void decodeEntryImpl(DataInputStream var1) throws IOException {
      }

      public void encodeEntryImpl(DataOutputStream var1) throws IOException {
      }

      public float getSelectionDepth() {
         return this.selectDepth;
      }

      public void onSelect(float var1) {
         this.setDrawIndication(true);
         this.selectDepth = var1;
         MapControllerManager.selected.add(this);
      }

      public void onUnSelect() {
         this.setDrawIndication(false);
         MapControllerManager.selected.remove(this);
      }

      public int hashCode() {
         return (int)FleetMember.this.entityDbId;
      }

      public boolean isSelectable() {
         return true;
      }

      public boolean equals(Object var1) {
         if (var1 instanceof FleetMember.FleetMemberMapIndication) {
            return ((FleetMember.FleetMemberMapIndication)var1).getEntityId() == this.getEntityId();
         } else {
            return false;
         }
      }

      private long getEntityId() {
         return FleetMember.this.entityDbId;
      }
   }
}
