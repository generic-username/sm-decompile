package org.schema.game.common.data.fleet;

import org.schema.schine.graphicsengine.core.Timer;

public abstract class FleetUnloadedAction {
   protected final FleetMember member;
   protected Fleet fleet;
   protected long creationTime;

   public FleetUnloadedAction(FleetMember var1, Fleet var2) {
      this.member = var1;
      this.fleet = var2;
      this.creationTime = System.currentTimeMillis();
   }

   public abstract boolean execute(Timer var1);

   protected void reset(Timer var1) {
      this.reset(var1.currentTime);
   }

   protected void reset(long var1) {
      this.creationTime = var1;
   }
}
