package org.schema.game.common.data.fleet;

import java.util.Arrays;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerButtonTilesInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.PlayerSectorInput;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.input.InputState;
import org.schema.schine.network.client.ClientState;

public enum FleetCommandTypes {
   IDLE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_0;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_1;
      }
   }, new FleetCommandTypes.FleetCommandDialog() {
      public final void clientSend(Fleet var1) {
         var1.sendFleetCommand(FleetCommandTypes.IDLE);
      }
   }, Transition.RESTART, new Class[0]),
   MOVE_FLEET(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_2;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_3;
      }
   }, new FleetCommandTypes.FleetCommandDialog() {
      public final void clientSend(final Fleet var1) {
         (new PlayerSectorInput((GameClientState)var1.getState(), Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_4, Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_5, "") {
            public void handleEnteredEmpty() {
               this.deactivate();
            }

            public void handleEntered(Vector3i var1x) {
               System.err.println("[CLIENT][FLEET] Sending fleet move command: " + var1x + "; on fleet " + var1);
               var1.sendFleetCommand(FleetCommandTypes.MOVE_FLEET, var1x);
            }

            public Object getSelectCoordinateButtonText() {
               return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_6;
            }
         }).activate();
      }
   }, Transition.MOVE_TO_SECTOR, new Class[]{Vector3i.class}),
   PATROL_FLEET(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_28;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_29;
      }
   }, (FleetCommandTypes.FleetCommandDialog)null, Transition.FLEET_PATROL, new Class[]{Vector3i.class}),
   TRADE_FLEET(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_30;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_27;
      }
   }, (FleetCommandTypes.FleetCommandDialog)null, Transition.FLEET_TRADE, new Class[]{Vector3i.class}),
   FLEET_ATTACK(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_7;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_8;
      }
   }, new FleetCommandTypes.FleetCommandDialog() {
      public final void clientSend(final Fleet var1) {
         (new PlayerSectorInput((GameClientState)var1.getState(), Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_14, Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_15, "") {
            public void handleEnteredEmpty() {
               this.deactivate();
            }

            public void handleEntered(Vector3i var1x) {
               System.err.println("[CLIENT][FLEET] Sending fleet ATTACK command: " + var1x + "; on fleet " + var1);
               var1.sendFleetCommand(FleetCommandTypes.FLEET_ATTACK, var1x);
            }

            public Object getSelectCoordinateButtonText() {
               return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_16;
            }
         }).activate();
      }
   }, Transition.FLEET_ATTACK, new Class[]{Vector3i.class}),
   FLEET_DEFEND(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_12;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_13;
      }
   }, new FleetCommandTypes.FleetCommandDialog() {
      public final void clientSend(final Fleet var1) {
         (new PlayerSectorInput((GameClientState)var1.getState(), Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_9, Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_10, "") {
            public void handleEnteredEmpty() {
               this.deactivate();
            }

            public void handleEntered(Vector3i var1x) {
               System.err.println("[CLIENT][FLEET] Sending fleet DEFEND command: " + var1x + "; on fleet " + var1);
               var1.sendFleetCommand(FleetCommandTypes.FLEET_DEFEND, var1x);
            }

            public Object getSelectCoordinateButtonText() {
               return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_11;
            }
         }).activate();
      }
   }, Transition.FLEET_DEFEND, new Class[]{Vector3i.class}),
   SENTRY_FORMATION(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_17;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_18;
      }
   }, new FleetCommandTypes.FleetCommandDialog() {
      public final void clientSend(Fleet var1) {
         var1.sendFleetCommand(FleetCommandTypes.SENTRY_FORMATION);
      }
   }, Transition.FLEET_SENTRY_FORMATION, new Class[0]),
   SENTRY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_19;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_20;
      }
   }, new FleetCommandTypes.FleetCommandDialog() {
      public final void clientSend(Fleet var1) {
         var1.sendFleetCommand(FleetCommandTypes.SENTRY);
      }
   }, Transition.FLEET_SENTRY, new Class[0]),
   FLEET_IDLE_FORMATION(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_21;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_22;
      }
   }, new FleetCommandTypes.FleetCommandDialog() {
      public final void clientSend(Fleet var1) {
         var1.sendFleetCommand(FleetCommandTypes.FLEET_IDLE_FORMATION);
      }
   }, Transition.FLEET_IDLE_FORMATION, new Class[0]),
   CALL_TO_CARRIER(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_23;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_24;
      }
   }, new FleetCommandTypes.FleetCommandDialog() {
      public final void clientSend(Fleet var1) {
         var1.sendFleetCommand(FleetCommandTypes.CALL_TO_CARRIER);
      }
   }, Transition.FLEET_RECALL_CARRIER, new Class[0]),
   MINE_IN_SECTOR(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_25;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_26;
      }
   }, new FleetCommandTypes.FleetCommandDialog() {
      public final void clientSend(Fleet var1) {
         var1.sendFleetCommand(FleetCommandTypes.MINE_IN_SECTOR);
      }
   }, Transition.FLEET_MINE, new Class[0]),
   CLOAK(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_31;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_32;
      }
   }, new FleetCommandTypes.FleetCommandDialog() {
      public final void clientSend(Fleet var1) {
         var1.sendFleetCommand(FleetCommandTypes.CLOAK);
      }
   }, Transition.FLEET_CLOAK, new Class[0]),
   UNCLOAK(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_33;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_34;
      }
   }, new FleetCommandTypes.FleetCommandDialog() {
      public final void clientSend(Fleet var1) {
         var1.sendFleetCommand(FleetCommandTypes.UNCLOAK);
      }
   }, Transition.FLEET_UNCLOAK, new Class[0]),
   JAM(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_35;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_36;
      }
   }, new FleetCommandTypes.FleetCommandDialog() {
      public final void clientSend(Fleet var1) {
         var1.sendFleetCommand(FleetCommandTypes.JAM);
      }
   }, Transition.FLEET_JAM, new Class[0]),
   UNJAM(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_37;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEETCOMMANDTYPES_38;
      }
   }, new FleetCommandTypes.FleetCommandDialog() {
      public final void clientSend(Fleet var1) {
         var1.sendFleetCommand(FleetCommandTypes.UNJAM);
      }
   }, Transition.FLEET_UNJAM, new Class[0]);

   private final Translatable name;
   private final Translatable description;
   public final Class[] args;
   public final FleetCommandTypes.FleetCommandDialog c;
   public final Transition transition;

   private FleetCommandTypes(Translatable var3, Translatable var4, FleetCommandTypes.FleetCommandDialog var5, Transition var6, Class... var7) {
      this.name = var3;
      this.args = var7;
      this.description = var4;
      this.c = var5;
      this.transition = var6;
   }

   public final String getName() {
      return this.name.getName(this);
   }

   public final String getDescription() {
      return this.description.getName(this);
   }

   public final boolean isAvailableOnClient() {
      return this.c != null;
   }

   public final void addTile(final PlayerButtonTilesInput var1, final Fleet var2) {
      if (this.isAvailableOnClient()) {
         final GUIActivationCallback var3 = new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return var2.isCommandUsable(FleetCommandTypes.this);
            }
         };
         var1.addTile(this.getName(), this.getDescription(), GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
            public boolean isOccluded() {
               return !var1.isActive() || var3 != null && !var3.isActive((ClientState)var2.getState());
            }

            public void callback(GUIElement var1x, MouseEvent var2x) {
               if (var2x.pressedLeftMouse()) {
                  FleetCommandTypes.this.c.clientSend(var2);
                  var1.deactivate();
               }

            }
         }, var3);
      }
   }

   public final void checkMatches(Object[] var1) {
      if (this.args.length != var1.length) {
         throw new IllegalArgumentException("Invalid argument count: Provided: " + Arrays.toString(var1) + ", but needs: " + Arrays.toString(this.args));
      } else {
         for(int var2 = 0; var2 < this.args.length; ++var2) {
            if (!var1[var2].getClass().equals(this.args[var2])) {
               System.err.println("Not Equal: " + var1[var2] + " and " + this.args[var2]);
               throw new IllegalArgumentException("Invalid argument on index " + var2 + ": Provided: " + Arrays.toString(var1) + "; cannot take " + var1[var2] + ":" + var1[var2].getClass() + ", it has to be type: " + this.args[var2].getClass());
            }
         }

      }
   }

   public interface FleetCommandDialog {
      void clientSend(Fleet var1);
   }
}
