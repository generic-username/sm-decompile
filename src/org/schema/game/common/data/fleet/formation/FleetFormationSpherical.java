package org.schema.game.common.data.fleet.formation;

import com.bulletphysics.linearmath.Transform;
import java.util.Iterator;
import java.util.List;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.schine.graphicsengine.forms.BoundingBox;

public class FleetFormationSpherical implements FleetFormation {
   void fibonacciSphere(List var1, float var2) {
      float var3 = (float)var1.size();
      var3 = 2.0F / var3;
      float var4 = 3.1415927F * (3.0F - FastMath.carmackSqrt(5.0F));

      for(int var5 = 0; var5 < var1.size(); ++var5) {
         float var6 = (float)var5 * var3 - 1.0F + var3 * 0.5F;
         float var7 = FastMath.carmackSqrt(1.0F - FastMath.pow(var6, 2.0F));
         float var8;
         float var9 = FastMath.cosFast(var8 = ((float)var5 + 0.0F) % (float)var1.size() * var4) * var7;
         var7 = FastMath.sinFast(var8) * var7;
         ((Transform)var1.get(var5)).origin.set(var9 * var2, var6 * var2, var7 * var2);
      }

   }

   public List getFormation(SegmentController var1, List var2, List var3) {
      if (var1.railController.isDockedAndExecuted()) {
         return null;
      } else {
         BoundingBox var4 = new BoundingBox();
         var1.getPhysicsDataContainer().getShape().getAabb(TransformTools.ident, var4.min, var4.max);
         float var7 = 0.0F;
         Iterator var8 = var2.iterator();

         while(var8.hasNext()) {
            Ship var5 = (Ship)var8.next();
            BoundingBox var6 = new BoundingBox();
            if (var5.getPhysicsDataContainer().getObject() != null) {
               var5.getPhysicsDataContainer().getShape().getAabb(TransformTools.ident, var6.min, var6.max);
            }

            float var9;
            if ((var9 = var6.maxSize()) > var7) {
               var7 = var9;
            }
         }

         this.fibonacciSphere(var3, (var4.maxSize() + var7 * 2.0F) * 1.7F);
         return var3;
      }
   }
}
