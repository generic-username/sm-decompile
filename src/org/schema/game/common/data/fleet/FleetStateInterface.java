package org.schema.game.common.data.fleet;

public interface FleetStateInterface {
   FleetManager getFleetManager();
}
