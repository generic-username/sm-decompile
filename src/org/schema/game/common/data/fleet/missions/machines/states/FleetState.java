package org.schema.game.common.data.fleet.missions.machines.states;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.FleetMember;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.common.states.SegmentControllerGameState;
import org.schema.game.server.ai.program.fleetcontrollable.FleetControllableProgram;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetAttackCycle;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetBreaking;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetIdleWaiting;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetMovingToSector;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetSeachingForTarget;
import org.schema.game.server.controller.SectorSwitch;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;

public abstract class FleetState extends State {
   private List loaded = new ObjectArrayList();
   private long attacked;
   private Vector3i attackedSector;
   private long lastSentry;

   public abstract FleetState.FleetStateType getType();

   public FleetState(Fleet var1) {
      super(var1);
   }

   public Fleet getEntityState() {
      return (Fleet)super.getEntityState();
   }

   public void restartAllLoaded() throws FSMException {
      Iterator var1 = this.getEntityState().getMembers().iterator();

      while(var1.hasNext()) {
         FleetMember var2;
         State var3;
         Ship var4;
         if ((var2 = (FleetMember)var1.next()).isLoaded() && !(var4 = (Ship)var2.getLoaded()).isCoreOverheating() && ((ShipAIEntity)var4.getAiConfiguration().getAiEntityState()).getCurrentProgram() != null && ((ShipAIEntity)var4.getAiConfiguration().getAiEntityState()).getCurrentProgram() instanceof FleetControllableProgram && !((var3 = ((ShipAIEntity)var4.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().getCurrentState()) instanceof FleetIdleWaiting) && !(var3 instanceof FleetBreaking)) {
            System.err.println("[FLEET] breaking ship: " + var4);
            ((ShipAIEntity)var4.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().stateTransition(Transition.FLEET_BREAKING);
         }
      }

   }

   public final boolean onEnter() {
      if (!this.getEntityState().missionString.equals(this.getType().getName())) {
         this.getEntityState().missionString = this.getType().getName();
         this.getEntityState().getFleetManager().submitMissionChangeToClients(this.getEntityState());
      }

      return this.onEnterFleetState();
   }

   public boolean onEnterFleetState() {
      return false;
   }

   public boolean isInAttackCycle(Ship var1) {
      return var1.getAiConfiguration() == null ? false : ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().getCurrentState() instanceof FleetAttackCycle;
   }

   public void allWaiting() throws FSMException {
      for(int var1 = 0; var1 < this.getEntityState().getMembers().size(); ++var1) {
         FleetMember var2;
         Ship var3;
         if ((var2 = (FleetMember)this.getEntityState().getMembers().get(var1)).isLoaded() && !(((ShipAIEntity)(var3 = (Ship)var2.getLoaded()).getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().getCurrentState() instanceof FleetIdleWaiting)) {
            ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().stateTransition(Transition.RESTART);
         }
      }

   }

   public boolean isState(Ship var1, Class var2) {
      return ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().getCurrentState().getClass().isAssignableFrom(var2);
   }

   public void fireTransitionIfNotState(Ship var1, Transition var2, Class var3) throws FSMException {
      if (!this.isState(var1, var3)) {
         ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().stateTransition(var2);
      }

   }

   public void moveToCurrentTarget() throws FSMException {
      FleetMember var1;
      if ((var1 = this.getEntityState().getFlagShip()) == null) {
         this.stateTransition(Transition.FLEET_EMPTY);
      } else if (this.getEntityState().getCurrentMoveTarget() == null) {
         this.stateTransition(Transition.TARGET_SECTOR_REACHED);
      } else {
         this.loaded.clear();
         int var2 = 0;

         for(int var3 = 0; var3 < this.getEntityState().getMembers().size(); ++var3) {
            FleetMember var4 = (FleetMember)this.getEntityState().getMembers().get(var3);
            if (var1.getSector().equals(this.getEntityState().getCurrentMoveTarget()) && var4.getSector().equals(var1.getSector())) {
               ++var2;
            } else if (var4.isLoaded()) {
               Ship var5 = (Ship)var4.getLoaded();
               this.loaded.add(var5);
            } else {
               var4.moveRequestUnloaded(this.getEntityState(), this.getEntityState().getCurrentMoveTarget());
            }
         }

         if (var2 == this.getEntityState().getMembers().size()) {
            this.getEntityState().removeCurrentMoveTarget();
            if (this.getEntityState().getCurrentMoveTarget() == null) {
               this.stateTransition(Transition.TARGET_SECTOR_REACHED);
            }

         } else {
            this.handleLoadedMoving(var1, this.getEntityState().getCurrentMoveTarget());
         }
      }
   }

   public void onHitBy(Damager var1) {
      if (var1 != null && this.isMovingSentry() && var1 instanceof SimpleTransformableSendableObject) {
         SimpleTransformableSendableObject var2 = (SimpleTransformableSendableObject)var1;
         Sector var3;
         if ((var3 = ((GameServerState)var1.getState()).getUniverse().getSector(var2.getSectorId())) != null) {
            this.attackedSector = new Vector3i(var3.pos);
            this.attacked = System.currentTimeMillis();
         }
      }

   }

   public boolean isMovingSentry() {
      return false;
   }

   public boolean isMovingAttacking() {
      return false;
   }

   private void handleLoadedMoving(FleetMember var1, Vector3i var2) throws FSMException {
      for(int var9 = 0; var9 < this.loaded.size(); ++var9) {
         final Ship var3;
         if (!(var3 = (Ship)this.loaded.get(var9)).isCoreOverheating() && ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram() != null && ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram() instanceof FleetControllableProgram) {
            assert var3.getAiConfiguration() != null : var3;

            assert ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram() != null : var3;

            assert ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine() != null : var3;

            assert ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm() != null : var3;

            assert ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().getCurrentState() != null : var3;

            Sector var4;
            if ((var4 = ((GameServerState)var3.getState()).getUniverse().getSector(var3.getSectorId())) != null) {
               boolean var5 = this.isInAttackCycle(var3);
               ShipAIEntity var6 = (ShipAIEntity)var3.getAiConfiguration().getAiEntityState();
               if (!var5) {
                  if (var6.currentSectorLoadedMove == null) {
                     var6.currentSectorLoadedMove = new Vector3i(var4.pos);
                     var6.inSameSector = 0L;
                     var6.lastInSameSector = System.currentTimeMillis();
                  } else if (var6.currentSectorLoadedMove.equals(var4.pos) && !var6.currentSectorLoadedMove.equals(var2)) {
                     var6.inSameSector += System.currentTimeMillis() - var6.lastInSameSector;
                     var6.lastInSameSector = System.currentTimeMillis();
                     if (var6.inSameSector > 180000L) {
                        System.err.println("[SERVER][AI] WARNING: ship has been too long in this sector while wanting to move (stuck?). warping towards goal");
                        Random var7 = new Random();
                        Vector3i var8;
                        Vector3i var10000 = var8 = new Vector3i(var4.pos);
                        var10000.x += var7.nextInt(5) - 2;
                        var8.y += var7.nextInt(5) - 2;
                        var8.z += var7.nextInt(5) - 2;
                        SectorSwitch var10;
                        if ((var10 = ((GameServerState)var3.getState()).getController().queueSectorSwitch(var3, var8, 1, false, true, true)) != null) {
                           var10.delay = System.currentTimeMillis() + 4000L;
                           var10.jumpSpawnPos = new Vector3f(var3.getWorldTransform().origin);
                           var10.executionGraphicsEffect = 2;
                           var10.keepJumpBasisWithJumpPos = true;
                        }

                        var6.currentSectorLoadedMove = null;
                     }
                  } else {
                     var6.currentSectorLoadedMove = null;
                  }
               } else {
                  var6.currentSectorLoadedMove = null;
               }

               if (!var5 && this.isMovingSentry() && System.currentTimeMillis() - this.attacked < 5000L && this.attackedSector != null) {
                  if (Sector.isNeighbor(var4.pos, this.attackedSector)) {
                     ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).engagingTimeoutQueued = new EngagingTimeout(this.attackedSector, 3.0F) {
                        public void onTimeout(SegmentControllerGameState var1) {
                           ((TargetProgram)((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram()).setSectorTarget(new Vector3i(this.attackedSector));

                           try {
                              var1.stateTransition(Transition.MOVE_TO_SECTOR);
                           } catch (FSMException var2) {
                              var2.printStackTrace();
                           }
                        }

                        public void onShot(SegmentControllerGameState var1) {
                           FleetState.this.attacked = System.currentTimeMillis();
                        }

                        public void onNoTargetFound(SegmentControllerGameState var1) {
                           try {
                              var1.stateTransition(Transition.MOVE_TO_SECTOR);
                           } catch (FSMException var2) {
                              var2.printStackTrace();
                           }
                        }
                     };
                     this.fireTransitionIfNotState(var3, Transition.SEARCH_FOR_TARGET, FleetSeachingForTarget.class);
                  } else {
                     ((TargetProgram)((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram()).setSectorTarget(new Vector3i(this.attackedSector));
                     this.fireTransitionIfNotState(var3, Transition.MOVE_TO_SECTOR, FleetMovingToSector.class);
                  }
               } else if (!var5 || !this.isMovingSentry() && !this.isMovingAttacking()) {
                  if (!var5 && this.isMovingAttacking() && System.currentTimeMillis() - this.lastSentry > 5000L) {
                     ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).engagingTimeoutQueued = new Timeout() {
                        public void onTimeout(SegmentControllerGameState var1) {
                        }

                        public void onShot(SegmentControllerGameState var1) {
                        }

                        public boolean checkTimeout(Vector3i var1) {
                           return false;
                        }

                        public void onNoTargetFound(SegmentControllerGameState var1) {
                           try {
                              var1.stateTransition(Transition.MOVE_TO_SECTOR);
                           } catch (FSMException var2) {
                              var2.printStackTrace();
                           }
                        }
                     };
                     ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().stateTransition(Transition.SEARCH_FOR_TARGET);
                     this.lastSentry = System.currentTimeMillis();
                  } else if (((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).engagingTimeoutQueued == null) {
                     this.moveLoadedToGoal(var3, var2);
                  }
               }
            }
         }
      }

   }

   private void moveLoadedToGoal(Ship var1, Vector3i var2) throws FSMException {
      ((TargetProgram)((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram()).setSectorTarget(new Vector3i(var2));
      if (!(((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().getCurrentState() instanceof FleetMovingToSector)) {
         ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().stateTransition(Transition.MOVE_TO_SECTOR);
      }

   }

   public Transition getMoveTrasition() {
      return Transition.MOVE_TO_SECTOR;
   }

   public static enum FleetStateType {
      IDLING(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_MISSIONS_MACHINES_STATES_FLEETSTATE_0;
         }
      }),
      SENTRY_IDLE(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_MISSIONS_MACHINES_STATES_FLEETSTATE_1;
         }
      }),
      FORMATION_IDLE(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_MISSIONS_MACHINES_STATES_FLEETSTATE_2;
         }
      }),
      MOVING(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_MISSIONS_MACHINES_STATES_FLEETSTATE_3;
         }
      }),
      ATTACKING(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_MISSIONS_MACHINES_STATES_FLEETSTATE_4;
         }
      }),
      SENTRY(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_MISSIONS_MACHINES_STATES_FLEETSTATE_5;
         }
      }),
      DEFENDING(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_MISSIONS_MACHINES_STATES_FLEETSTATE_6;
         }
      }),
      FORMATION_SENTRY(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_MISSIONS_MACHINES_STATES_FLEETSTATE_7;
         }
      }),
      CALLBACK_TO_CARRIER(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_MISSIONS_MACHINES_STATES_FLEETSTATE_8;
         }
      }),
      MINING(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_MISSIONS_MACHINES_STATES_FLEETSTATE_9;
         }
      }),
      PARTOLLING(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_MISSIONS_MACHINES_STATES_FLEETSTATE_10;
         }
      }),
      TRADING(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_MISSIONS_MACHINES_STATES_FLEETSTATE_11;
         }
      }),
      CLOAKING(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_MISSIONS_MACHINES_STATES_FLEETSTATE_12;
         }
      }),
      UNCLOAKING(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_MISSIONS_MACHINES_STATES_FLEETSTATE_13;
         }
      }),
      JAMMING(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_MISSIONS_MACHINES_STATES_FLEETSTATE_14;
         }
      }),
      UNJAMMING(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_MISSIONS_MACHINES_STATES_FLEETSTATE_15;
         }
      });

      private final Translatable name;

      private FleetStateType(Translatable var3) {
         this.name = var3;
      }

      public final String getName() {
         return this.name.getName(this);
      }
   }
}
