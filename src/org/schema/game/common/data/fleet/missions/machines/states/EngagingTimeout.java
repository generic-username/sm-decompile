package org.schema.game.common.data.fleet.missions.machines.states;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.ai.program.common.states.SegmentControllerGameState;

public abstract class EngagingTimeout extends Timeout {
   public final Vector3i attackedSector;
   public final float dist;

   public EngagingTimeout(Vector3i var1, float var2) {
      this.attackedSector = new Vector3i(var1);
      this.dist = var2;
   }

   public abstract void onTimeout(SegmentControllerGameState var1);

   public abstract void onShot(SegmentControllerGameState var1);

   public boolean checkTimeout(Vector3i var1) {
      if (var1 != null) {
         return Vector3i.getDisatance(var1, this.attackedSector) > this.dist;
      } else {
         return false;
      }
   }
}
