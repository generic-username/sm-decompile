package org.schema.game.common.data.fleet.missions.machines.states;

import org.schema.game.common.data.fleet.Fleet;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class Moving extends FleetState {
   public Moving(Fleet var1) {
      super(var1);
   }

   public boolean onExit() {
      return false;
   }

   public boolean onEnterFleetState() {
      try {
         this.restartAllLoaded();
      } catch (FSMException var1) {
         var1.printStackTrace();
      }

      return super.onEnterFleetState();
   }

   public void stateTransition(Transition var1, int var2) throws FSMException {
      super.stateTransition(var1, var2);
      if (var1 == Transition.TARGET_SECTOR_REACHED) {
         this.getEntityState().onCommandPartFinished(this);
      }

   }

   public boolean onUpdate() throws FSMException {
      this.moveToCurrentTarget();
      return false;
   }

   public FleetState.FleetStateType getType() {
      return FleetState.FleetStateType.MOVING;
   }
}
