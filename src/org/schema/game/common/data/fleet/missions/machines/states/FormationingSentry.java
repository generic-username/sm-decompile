package org.schema.game.common.data.fleet.missions.machines.states;

import com.bulletphysics.linearmath.Transform;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.FleetMember;
import org.schema.game.common.data.world.SectorTransformation;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.fleetcontrollable.FleetControllableProgram;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetFormationingAbstract;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetIdleWaiting;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public class FormationingSentry extends Formatoning {
   private static final long SEARCH_FOR_TARGET_DELAY = 1000L;

   public FormationingSentry(Fleet var1) {
      super(var1);
   }

   public boolean onUpdate() throws FSMException {
      if (this.getEntityState().getFlagShip() != null && this.getEntityState().getFlagShip().isLoaded()) {
         Ship var1 = (Ship)this.getEntityState().getFlagShip().getLoaded();
         if (!this.isInAttackCycle(var1) && ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram() != null && ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram() instanceof FleetControllableProgram) {
            ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).lastAttackStateSetByFleet = System.currentTimeMillis();
            ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().stateTransition(Transition.SEARCH_FOR_TARGET);
         }
      }

      return super.onUpdate();
   }

   public FleetState.FleetStateType getType() {
      return FleetState.FleetStateType.FORMATION_SENTRY;
   }

   public void onProximityToFlagshipSector(Ship var1, FleetMember var2, Transform var3) throws FSMException {
      State var4 = ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().getCurrentState();
      if (!this.isInAttackCycle(var1) && System.currentTimeMillis() - ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).lastAttackStateSetByFleet > 1000L) {
         ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).lastAttackStateSetByFleet = System.currentTimeMillis();
         ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().stateTransition(Transition.SEARCH_FOR_TARGET);
      } else if ((this.isInAttackCycle(var1) || var4 instanceof FleetIdleWaiting) && ((TargetProgram)((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram()).getTarget() == null && !(((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().getCurrentState() instanceof FleetFormationingAbstract)) {
         ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().stateTransition(Transition.FLEET_FORMATION);
      }

      if (!this.isInAttackCycle(var1)) {
         var2.getLoaded().getWorldTransform().transform(var3.origin);
         SectorTransformation var7 = new SectorTransformation(var3, var2.getLoaded().getSectorId());
         ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).fleetFormationPos.add(var7);

         assert var2.getLoaded().isOnServer();

         for(int var6 = 0; var6 < ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).fleetFormationPos.size() - 4; ++var6) {
            var7 = (SectorTransformation)((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).fleetFormationPos.get(var6);
            SectorTransformation var8 = (SectorTransformation)((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).fleetFormationPos.get(var6 + 1);
            SectorTransformation var5 = (SectorTransformation)((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).fleetFormationPos.get(var6 + 2);
            if (var7.sectorId == var8.sectorId && var7.sectorId == var5.sectorId && Vector3fTools.diffLength(var7.t.origin, var5.t.origin) < 3.0F && var7.t.basis.epsilonEquals(var8.t.basis, 0.3F)) {
               ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).fleetFormationPos.remove(var6 + 1);
            }
         }

         while(((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).fleetFormationPos.size() > 200) {
            ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).fleetFormationPos.remove(0);
         }
      }

   }
}
