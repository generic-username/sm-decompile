package org.schema.game.common.data.fleet.missions.machines.states;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class Defending extends Attacking {
   public Defending(Fleet var1) {
      super(var1);
   }

   public boolean onEnterFleetState() {
      try {
         this.restartAllLoaded();
      } catch (FSMException var1) {
         var1.printStackTrace();
      }

      return super.onEnterFleetState();
   }

   public boolean onUpdate() throws FSMException {
      if (this.getEntityState().isEmpty()) {
         this.stateTransition(Transition.FLEET_EMPTY);
         return false;
      } else if (Vector3i.getDisatance(this.getEntityState().getFlagShip().getSector(), this.getEntityState().getCurrentMoveTarget()) > 2.0F) {
         this.moveToCurrentTarget();
         return false;
      } else {
         return super.onUpdate();
      }
   }

   public FleetState.FleetStateType getType() {
      return FleetState.FleetStateType.DEFENDING;
   }
}
