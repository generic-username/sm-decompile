package org.schema.game.common.data.fleet.missions;

import java.util.HashMap;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.missions.machines.FleetFiniteStateMachine;
import org.schema.game.common.data.fleet.missions.machines.FleetFiniteStateMachineFactory;
import org.schema.schine.ai.MachineProgram;
import org.schema.schine.ai.stateMachines.AIConfiguationElementsInterface;
import org.schema.schine.ai.stateMachines.FSMException;

public class MissionProgram extends MachineProgram {
   public MissionProgram(Fleet var1, boolean var2) {
      super(var1, var2);
   }

   public void onAISettingChanged(AIConfiguationElementsInterface var1) throws FSMException {
   }

   protected String getStartMachine() {
      return "IDLE";
   }

   protected void initializeMachines(HashMap var1) {
      var1.put("IDLE", new FleetFiniteStateMachine((Fleet)this.getEntityState(), this, new FleetFiniteStateMachineFactory()));
   }

   public boolean isAlwaysOn() {
      return true;
   }
}
