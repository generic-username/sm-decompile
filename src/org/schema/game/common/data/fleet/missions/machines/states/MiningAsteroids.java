package org.schema.game.common.data.fleet.missions.machines.states;

import com.bulletphysics.linearmath.Transform;
import java.util.Iterator;
import java.util.List;
import org.schema.game.common.controller.FloatingRock;
import org.schema.game.common.controller.FloatingRockManaged;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.FleetMember;
import org.schema.game.common.data.fleet.formation.FleetFormationSpherical;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SectorTransformation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.fleetcontrollable.FleetControllableProgram;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetFormationingAbstract;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetMining;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.network.objects.Sendable;

public class MiningAsteroids extends Formatoning {
   private int currentId;
   private long miningTime;
   private long miningLastTime;

   public MiningAsteroids(Fleet var1) {
      super(var1);
   }

   public FleetState.FleetStateType getType() {
      return FleetState.FleetStateType.MINING;
   }

   public boolean onEnterFleetState() {
      this.miningTime = 0L;
      this.miningLastTime = System.currentTimeMillis();
      return super.onEnterFleetState();
   }

   public boolean onUpdate() throws FSMException {
      FleetMember var1 = this.getEntityState().getFlagShip();
      this.miningTime += System.currentTimeMillis() - this.miningLastTime;
      this.miningLastTime = System.currentTimeMillis();
      if (this.currentId > 0) {
         Sendable var3;
         if ((var3 = (Sendable)((GameServerState)this.getEntityState().getState()).getLocalAndRemoteObjectContainer().getLocalObjects().get(this.currentId)) != null && var3 instanceof SegmentController && ((SegmentController)var3).getTotalElements() > 0) {
            super.onUpdate();
         } else {
            System.err.println("[FLEET][MINING] CURRENT ID RESET SINCE OBJECT NO LONGER LOADED: " + this.currentId + "; Fleet: " + this.getEntityState());
            this.currentId = -1;
         }
      } else {
         this.currentId = -1;
         SegmentController var4;
         Sector var5;
         if (var1 != null && var1.isLoaded() && (var5 = ((GameServerState)(var4 = var1.getLoaded()).getState()).getUniverse().getSector(var4.getSectorId())) != null) {
            Iterator var6 = var5.getEntities().iterator();

            while(var6.hasNext()) {
               SimpleTransformableSendableObject var2;
               if ((var2 = (SimpleTransformableSendableObject)var6.next()) instanceof FloatingRock || var2 instanceof FloatingRockManaged) {
                  this.currentId = var2.getId();
                  break;
               }
            }

            if (this.currentId == -1) {
               this.stateTransition(Transition.RESTART);
               this.getEntityState().onCommandPartFinished(this);
            }
         }

         if (this.getEntityState().isNPCFleet() && (this.currentId == -1 || this.miningTime > 300000L)) {
            this.stateTransition(Transition.RESTART);
            this.getEntityState().onCommandPartFinished(this);
         }
      }

      return false;
   }

   public boolean needsFormationTransition(Ship var1) {
      State var2;
      return !((var2 = ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().getCurrentState()) instanceof FleetFormationingAbstract) && !(var2 instanceof FleetMining);
   }

   public Transition getEntityFormationTransition() {
      return Transition.FLEET_GET_TO_MINING_POS;
   }

   public void initFormation(Ship var1, List var2, List var3) {
      Sendable var4;
      if ((var4 = (Sendable)((GameServerState)this.getEntityState().getState()).getLocalAndRemoteObjectContainer().getLocalObjects().get(this.currentId)) == null || !(var4 instanceof SegmentController)) {
         var2.clear();
      }

      SegmentController var5 = (SegmentController)var4;
      (new FleetFormationSpherical()).getFormation(var5, var2, var3);
      Iterator var6 = var2.iterator();

      while(var6.hasNext()) {
         Ship var7;
         if (((ShipAIEntity)(var7 = (Ship)var6.next()).getAiConfiguration().getAiEntityState()).getCurrentProgram() != null && ((ShipAIEntity)var7.getAiConfiguration().getAiEntityState()).getCurrentProgram() instanceof FleetControllableProgram) {
            ((TargetProgram)((ShipAIEntity)var7.getAiConfiguration().getAiEntityState()).getCurrentProgram()).setTarget(var5);
         }
      }

   }

   public void transformAndAddLocalFormation(FleetMember var1, Ship var2, Transform var3) {
      Sendable var4;
      if ((var4 = (Sendable)((GameServerState)this.getEntityState().getState()).getLocalAndRemoteObjectContainer().getLocalObjects().get(this.currentId)) != null && var4 instanceof SegmentController) {
         SegmentController var5;
         (var5 = (SegmentController)var4).getWorldTransform().transform(var3.origin);
         SectorTransformation var6 = new SectorTransformation(var3, var5.getSectorId());
         ((ShipAIEntity)var2.getAiConfiguration().getAiEntityState()).fleetFormationPos.add(var6);
      }
   }
}
