package org.schema.game.common.data.fleet.missions.machines.states;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.ai.program.common.states.SegmentControllerGameState;

public abstract class Timeout {
   public boolean wasTimedOut;

   public abstract void onTimeout(SegmentControllerGameState var1);

   public abstract void onShot(SegmentControllerGameState var1);

   public boolean isTimeout(Vector3i var1) {
      boolean var2;
      if (var2 = this.checkTimeout(var1)) {
         this.wasTimedOut = var2;
      }

      return var2;
   }

   public abstract boolean checkTimeout(Vector3i var1);

   public abstract void onNoTargetFound(SegmentControllerGameState var1);
}
