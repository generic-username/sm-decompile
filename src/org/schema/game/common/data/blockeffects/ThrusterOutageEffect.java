package org.schema.game.common.data.blockeffects;

import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.elements.effectblock.EffectElementManager;
import org.schema.schine.graphicsengine.core.Timer;

public class ThrusterOutageEffect extends BlockEffect {
   private float force;

   public ThrusterOutageEffect(SendableSegmentController var1, float var2) {
      super(var1, BlockEffectTypes.THRUSTER_OUTAGE);
      this.force = var2;
   }

   public void update(Timer var1, FastSegmentControllerStatus var2) {
      var2.thrustPercent = this.force;
   }

   public boolean needsDeadUpdate() {
      return true;
   }

   public EffectElementManager.OffensiveEffects getMessage() {
      return EffectElementManager.OffensiveEffects.THRUSTER_OUTAGE;
   }

   public float getForce() {
      return this.force;
   }

   public boolean affectsMother() {
      return false;
   }
}
