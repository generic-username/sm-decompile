package org.schema.game.common.data.blockeffects;

import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.elements.effectblock.EffectElementManager;
import org.schema.schine.graphicsengine.core.Timer;

public class PowerRegenDownEffect extends BlockEffect {
   private float force;

   public PowerRegenDownEffect(SendableSegmentController var1, float var2) {
      super(var1, BlockEffectTypes.NO_POWER_RECHARGE);
      this.force = var2;
   }

   public void update(Timer var1, FastSegmentControllerStatus var2) {
      var2.powerRegenPercent = this.force;
   }

   public boolean needsDeadUpdate() {
      return true;
   }

   public EffectElementManager.OffensiveEffects getMessage() {
      return EffectElementManager.OffensiveEffects.NO_POWER_RECHARGE;
   }

   public float getForce() {
      return this.force;
   }

   public boolean affectsMother() {
      return false;
   }
}
