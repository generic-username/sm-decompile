package org.schema.game.common.data.blockeffects;

import org.schema.game.common.controller.SendableSegmentController;
import org.schema.schine.graphicsengine.core.Timer;

public class ControllessEffect extends BlockEffect {
   public ControllessEffect(SendableSegmentController var1) {
      super(var1, BlockEffectTypes.CONTROLLESS);
      this.durationMS = 3000L;
   }

   public void update(Timer var1, FastSegmentControllerStatus var2) {
   }

   public boolean needsDeadUpdate() {
      return false;
   }

   public boolean affectsMother() {
      return true;
   }
}
