package org.schema.game.common.data.blockeffects;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.power.PowerAddOn;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.schine.graphicsengine.core.Timer;

public class TakeOffEffect extends BlockEffect {
   private boolean alive = true;
   private boolean pushed;
   private float force;
   private Vector3f dir = new Vector3f(0.0F, 0.0F, 1.0F);

   public TakeOffEffect(SendableSegmentController var1, float var2, float var3, float var4, float var5) {
      super(var1, BlockEffectTypes.TAKE_OFF);
      this.setForce(var2);
      this.dir.set(var3, var4, var5);
   }

   public boolean isAlive() {
      return this.alive;
   }

   public void update(Timer var1, FastSegmentControllerStatus var2) {
      if (!this.pushed && (RigidBody)this.segmentController.getPhysicsDataContainer().getObject() != null) {
         RigidBody var10;
         Vector3f var11 = (var10 = (RigidBody)this.segmentController.getPhysicsDataContainer().getObject()).getLinearVelocity(new Vector3f());
         PowerAddOn var3 = ((PowerManagerInterface)((ManagedSegmentController)this.segmentController).getManagerContainer()).getPowerAddOn();
         if ((this.segmentController.isUsingPowerReactors() || var3.consumePowerInstantly((double)(VoidElementManager.EVADE_EFFECT_POWER_CONSUMPTION_MULT * this.force))) && this.segmentController.getPhysicsDataContainer().getObject() != null && this.segmentController.getPhysicsDataContainer().getObject() instanceof RigidBody) {
            var10.getWorldTransform(new Transform());
            Vector3f var12 = new Vector3f(this.dir);
            float var4 = this.getForce();
            var12.scale(var4);
            Vector3f var5;
            (var5 = new Vector3f(var11)).scaleAdd(var10.getInvMass(), var12, var5);
            float var6 = var4;

            Vector3f var8;
            for(int var7 = 0; var4 > 0.0F && var5.length() > this.getMaxVelocityAbsolute() && var5.length() > var11.length(); ++var7) {
               if (var7 > 10000) {
                  var4 *= 0.5F;
               }

               var4 = Math.max(0.0F, var4 - 1.0F);
               var12.normalize();
               var12.scale(var4);
               var5 = new Vector3f(var11);
               var8 = new Vector3f(var11);
               Vector3f var9;
               (var9 = new Vector3f(var12)).scale(var10.getInvMass());
               var5.add(var8, var9);
               if (var9.length() > this.getMaxVelocityAbsolute() * 3.0F) {
                  var4 *= 0.5F;
               } else if (var9.length() > this.getMaxVelocityAbsolute() * 3.0F) {
                  var4 = Math.max(0.0F, var4 - 10.0F);
               }
            }

            if (var4 != var6) {
               System.err.println(this.segmentController.getState() + " TAKING OFF CAPPED " + this.segmentController + " -> " + this.dir + " -> " + var12 + " (*" + var4 + ") Initial Force: " + this.getForce());
            } else {
               System.err.println(this.segmentController.getState() + " TAKING OFF " + this.segmentController + " -> " + this.dir + " -> " + var12 + " (*" + var4 + ") Initial Force: " + this.getForce());
            }

            if (var4 <= 0.0F) {
               this.pushed = true;
               return;
            }

            var10.applyCentralImpulse(var12);
            var8 = new Vector3f();
            var10.getLinearVelocity(var8);
            System.err.println(this.segmentController.getState() + " AFTER TAKING OFF " + this.segmentController + " -> " + var12 + " -> " + var4 + " -> speed: " + var8 + "; " + var8.length() + " m/s");
            var10.activate(true);
         }

         this.pushed = true;
      } else {
         this.alive = false;
      }
   }

   public void end() {
      this.alive = false;
   }

   public boolean needsDeadUpdate() {
      return false;
   }

   public float getForce() {
      return this.force;
   }

   public void setForce(float var1) {
      this.force = var1;
   }

   public boolean affectsMother() {
      return true;
   }

   public Vector3f getDirection() {
      return this.dir;
   }
}
