package org.schema.game.common.data.blockeffects;

import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.util.Iterator;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.blockeffects.updates.BlockEffectDeadUpdate;
import org.schema.game.common.data.blockeffects.updates.BlockEffectSpawnUpdate;
import org.schema.game.common.data.blockeffects.updates.BlockEffectUpdate;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.network.objects.NetworkSegmentController;
import org.schema.game.network.objects.remote.RemoteBlockEffectUpdate;
import org.schema.schine.graphicsengine.core.Timer;

public class BlockEffectManager {
   public final FastSegmentControllerStatus status = new FastSegmentControllerStatus();
   private final Short2ObjectOpenHashMap effects = new Short2ObjectOpenHashMap();
   private final ObjectOpenHashSet activeEffectTypes = new ObjectOpenHashSet();
   private final ObjectOpenHashSet activeEffectsSet = new ObjectOpenHashSet();
   private final SendableSegmentController segmentController;
   private final ObjectArrayFIFOQueue receivedUpdates = new ObjectArrayFIFOQueue();
   private short idGen;

   public BlockEffectManager(SendableSegmentController var1) {
      this.segmentController = var1;
   }

   public void addEffect(BlockEffect var1) {
      if (var1.affectsMother() && this.segmentController.railController.getRoot() != this.segmentController) {
         ((SendableSegmentController)this.segmentController.railController.getRoot()).getBlockEffectManager().addEffect(var1);
      } else {
         short var10003 = this.idGen;
         this.idGen = (short)(var10003 + 1);
         var1.setId(var10003);
         this.effects.put(var1.getId(), var1);
         this.refreshActiveEffects();
         var1.pendingBroadcastUpdates.add(this.getSpawnUpdate(var1));
      }
   }

   public ObjectOpenHashSet getActiveEffectTypes() {
      return this.activeEffectTypes;
   }

   public BlockEffect getEffect(BlockEffectTypes var1) {
      Iterator var2 = this.effects.values().iterator();

      BlockEffect var3;
      do {
         if (!var2.hasNext()) {
            return null;
         }
      } while((var3 = (BlockEffect)var2.next()).getType() != var1);

      return var3;
   }

   public BlockEffect getEffectByBlockIdentifyer(long var1) {
      Iterator var3 = this.effects.values().iterator();

      BlockEffect var4;
      do {
         if (!var3.hasNext()) {
            return null;
         }
      } while(ElementCollection.getPosIndexFrom4((var4 = (BlockEffect)var3.next()).getBlockAndTypeId4()) != var1);

      return var4;
   }

   public BlockEffectSpawnUpdate getSpawnUpdate(BlockEffect var1) {
      BlockEffectSpawnUpdate var2;
      (var2 = new BlockEffectSpawnUpdate(var1.getId(), var1.getBlockAndTypeId4())).effectType = (byte)var1.getTypeOrd();
      var2.sendBlockEffect = var1;

      assert var2.effectType == var1.getTypeOrd() : var2.effectType + "; " + var1.getTypeOrd() + ": " + var1;

      return var2;
   }

   public boolean hasEffect(BlockEffectTypes var1) {
      return this.getActiveEffectTypes().contains(var1);
   }

   private void refreshActiveEffects() {
      this.getActiveEffectTypes().clear();
      this.getActiveEffectsSet().clear();
      LongOpenHashSet var1 = (LongOpenHashSet)this.segmentController.getControlElementMap().getControllingMap().getAll().get(ElementCollection.getIndex(Ship.core));
      Iterator var2 = this.effects.values().iterator();

      while(true) {
         BlockEffect var3;
         do {
            if (!var2.hasNext()) {
               return;
            }
         } while(!(var3 = (BlockEffect)var2.next()).isAlive());

         if (this.segmentController.isOnServer()) {
            if (this.getActiveEffectTypes().contains(var3.getType())) {
               var3.end();
            }

            if (var3.getBlockAndTypeId4() != Long.MIN_VALUE && this.segmentController instanceof Ship && (var1 == null || !var1.contains(var3.getBlockAndTypeId4()))) {
               var3.end();
            }
         }

         this.getActiveEffectsSet().add(var3);
         this.getActiveEffectTypes().add(var3.getType());
      }
   }

   private void sendEffectUpdates() {
      BlockEffect var2;
      if (this.effects.size() > 0) {
         for(Iterator var1 = this.effects.values().iterator(); var1.hasNext(); var2.clearAllUpdates()) {
            if ((var2 = (BlockEffect)var1.next()).hasPendingBroadcastUpdates()) {
               var2.sendPendingBroadcastUpdates(this.segmentController);
            }

            if (var2.hasPendingUpdates() && var2.isAlive()) {
               var2.sendPendingUpdates(this.segmentController);
            }
         }
      }

   }

   public void updateClient(Timer var1) {
      this.status.reset();
      ObjectIterator var2 = this.effects.values().iterator();

      while(var2.hasNext()) {
         BlockEffect var3;
         (var3 = (BlockEffect)var2.next()).update(var1, this.status);
         if (!var3.isAlive()) {
            var2.remove();
         }
      }

      this.refreshActiveEffects();

      while(this.receivedUpdates.size() > 0) {
         ((BlockEffectUpdate)this.receivedUpdates.dequeue()).handleClientUpdate(this.effects, this.segmentController);
      }

   }

   public void updateFromNetworkObject(NetworkSegmentController var1) {
      for(int var2 = 0; var2 < var1.effectUpdateBuffer.getReceiveBuffer().size(); ++var2) {
         BlockEffectUpdate var3 = (BlockEffectUpdate)((RemoteBlockEffectUpdate)var1.effectUpdateBuffer.getReceiveBuffer().get(var2)).get();
         this.receivedUpdates.enqueue(var3);
      }

   }

   public void updateToFullNetworkObject(NetworkSegmentController var1) {
      ObjectIterator var2 = this.effects.values().iterator();

      while(var2.hasNext()) {
         BlockEffect var3 = (BlockEffect)var2.next();
         BlockEffectSpawnUpdate var4 = this.getSpawnUpdate(var3);
         var1.effectUpdateBuffer.add(new RemoteBlockEffectUpdate(var4, this.segmentController.isOnServer()));
      }

   }

   public void updateServer(Timer var1) {
      ObjectIterator var2 = this.effects.values().iterator();
      this.status.reset();
      this.refreshActiveEffects();

      while(var2.hasNext()) {
         BlockEffect var3;
         (var3 = (BlockEffect)var2.next()).update(var1, this.status);
         if (!var3.isAlive() && var3.needsDeadUpdate()) {
            BlockEffectDeadUpdate var4 = new BlockEffectDeadUpdate(var3.getId(), var3.getBlockAndTypeId4());
            var3.pendingBroadcastUpdates.add(var4);
         }
      }

      this.sendEffectUpdates();
      var2 = this.effects.values().iterator();

      while(var2.hasNext()) {
         if (!((BlockEffect)var2.next()).isAlive()) {
            var2.remove();
         }
      }

   }

   public ObjectOpenHashSet getActiveEffectsSet() {
      return this.activeEffectsSet;
   }
}
