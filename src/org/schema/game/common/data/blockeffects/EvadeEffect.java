package org.schema.game.common.data.blockeffects;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.power.PowerAddOn;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;

public class EvadeEffect extends BlockEffect {
   private boolean alive = true;
   private boolean pushed;
   private float force;

   public EvadeEffect(SendableSegmentController var1, float var2) {
      super(var1, BlockEffectTypes.EVADE);
      this.setForce(var2);
   }

   public float getMaxVelocity() {
      return this.segmentController instanceof Ship ? ((Ship)this.segmentController).getCurrentMaxVelocity() : 0.0F;
   }

   public boolean isAlive() {
      return this.alive;
   }

   public void update(Timer var1, FastSegmentControllerStatus var2) {
      if (!this.pushed && this.segmentController.getPhysicsDataContainer().getObject() != null && this.segmentController.getPhysicsDataContainer().getObject() instanceof RigidBody) {
         PowerAddOn var5 = ((PowerManagerInterface)((ManagedSegmentController)this.segmentController).getManagerContainer()).getPowerAddOn();
         RigidBody var8;
         Vector3f var3 = (var8 = (RigidBody)this.segmentController.getPhysicsDataContainer().getObject()).getLinearVelocity(new Vector3f());
         if (var5.consumePowerInstantly((double)(VoidElementManager.EVADE_EFFECT_POWER_CONSUMPTION_MULT * this.force))) {
            System.err.println(this.segmentController.getState() + " EVADING " + this.segmentController);
            Transform var6 = var8.getWorldTransform(new Transform());
            Vector3f var7;
            (var7 = GlUtil.getForwardVector(new Vector3f(), var6)).scale(this.getForce());
            var7.negate();
            Vector3f var4;
            (var4 = new Vector3f(var3)).scaleAdd(var8.getInvMass(), var7, var4);
            if (var4.length() > this.getMaxVelocityAbsolute() && var4.length() > var3.length()) {
               this.pushed = true;
               return;
            }

            var8.applyCentralImpulse(var7);
            var7 = new Vector3f();
            var8.getLinearVelocity(var7);
            if (var7.length() > this.getMaxVelocity()) {
               var7.normalize();
               var7.scale(this.getMaxVelocity());
               var8.setLinearVelocity(var7);
            }

            var8.getAngularVelocity(var7);
            if (var7.length() > 10.0F) {
               var7.normalize();
               var7.scale(10.0F);
               var8.setAngularVelocity(var7);
            }

            var8.activate(true);
         }

         this.pushed = true;
      } else {
         this.alive = false;
      }
   }

   public void end() {
      this.alive = false;
   }

   public boolean needsDeadUpdate() {
      return false;
   }

   public float getForce() {
      return this.force;
   }

   public void setForce(float var1) {
      this.force = var1;
   }

   public boolean affectsMother() {
      return true;
   }
}
