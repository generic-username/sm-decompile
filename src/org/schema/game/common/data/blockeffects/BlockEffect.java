package org.schema.game.common.data.blockeffects;

import java.util.ArrayList;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.effectblock.EffectElementManager;
import org.schema.game.common.data.blockeffects.updates.BlockEffectUpdate;
import org.schema.game.network.objects.remote.RemoteBlockEffectUpdate;
import org.schema.schine.graphicsengine.core.Timer;

public abstract class BlockEffect {
   protected final ArrayList pendingUpdates = new ArrayList();
   protected final ArrayList pendingBroadcastUpdates = new ArrayList();
   protected final ArrayList pendingClientUpdates = new ArrayList();
   protected final SendableSegmentController segmentController;
   private final boolean onServer;
   private final BlockEffectTypes type;
   protected long durationMS = -1L;
   private short id = -3131;
   private long start = System.currentTimeMillis();
   private long blockId = Long.MIN_VALUE;
   private long messageDisplayed;

   public BlockEffect(SendableSegmentController var1, BlockEffectTypes var2) {
      this.segmentController = var1;
      this.onServer = this.segmentController.isOnServer();
      this.type = var2;

      assert var2.getClazz().equals(this.getClass()) : var2 + "; " + this.getClass();

   }

   public void clearAllUpdates() {
      this.pendingUpdates.clear();
      this.pendingBroadcastUpdates.clear();
   }

   public float getMaxVelocityAbsolute() {
      return this.segmentController instanceof Ship ? ((Ship)this.segmentController).getMaxSpeedAbsolute() : 0.0F;
   }

   public String toString() {
      return "(" + this.type.getName() + "[ID: " + this.id + "])";
   }

   public long getDuration() {
      return this.durationMS;
   }

   public void setDuration(long var1) {
      this.durationMS = var1;
   }

   public short getId() {
      return this.id;
   }

   public void setId(short var1) {
      this.id = var1;
   }

   public long getStart() {
      return this.start;
   }

   public BlockEffectTypes getType() {
      return this.type;
   }

   public int getTypeOrd() {
      return this.type.ordinal();
   }

   public boolean hasPendingBroadcastUpdates() {
      return !this.pendingBroadcastUpdates.isEmpty();
   }

   public boolean hasPendingUpdates() {
      return !this.pendingUpdates.isEmpty();
   }

   public boolean isAlive() {
      return this.durationMS == -1L || System.currentTimeMillis() - this.getStart() < this.durationMS;
   }

   private boolean isOnServer() {
      return this.onServer;
   }

   public void sendPendingBroadcastUpdates(SendableSegmentController var1) {
      for(int var2 = 0; var2 < this.pendingBroadcastUpdates.size(); ++var2) {
         BlockEffectUpdate var3 = (BlockEffectUpdate)this.pendingBroadcastUpdates.get(var2);
         var1.getNetworkObject().effectUpdateBuffer.add(new RemoteBlockEffectUpdate(var3, this.isOnServer()));
      }

   }

   public void sendPendingUpdates(SendableSegmentController var1) {
      for(int var2 = 0; var2 < this.pendingUpdates.size(); ++var2) {
         BlockEffectUpdate var3 = (BlockEffectUpdate)this.pendingUpdates.get(var2);
         var1.getNetworkObject().effectUpdateBuffer.add(new RemoteBlockEffectUpdate(var3, this.isOnServer()));
      }

   }

   public abstract void update(Timer var1, FastSegmentControllerStatus var2);

   public long getBlockAndTypeId4() {
      return this.blockId;
   }

   public void end() {
      this.durationMS = 0L;
   }

   public void setBlockId(long var1) {
      this.blockId = var1;
   }

   public abstract boolean needsDeadUpdate();

   public boolean isMessageDisplayed() {
      return System.currentTimeMillis() - this.messageDisplayed < 3000L;
   }

   public void setMessageDisplayed(long var1) {
      this.messageDisplayed = var1 + (long)(Math.random() * 1000.0D);
   }

   public EffectElementManager.OffensiveEffects getMessage() {
      return null;
   }

   public abstract boolean affectsMother();
}
