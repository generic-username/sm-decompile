package org.schema.game.common.data.blockeffects;

import org.schema.game.common.controller.SendableSegmentController;
import org.schema.schine.graphicsengine.core.Timer;

public class NullEffect extends BlockEffect {
   public NullEffect(SendableSegmentController var1) {
      super(var1, BlockEffectTypes.NULL_EFFECT);
   }

   public void update(Timer var1, FastSegmentControllerStatus var2) {
   }

   public boolean needsDeadUpdate() {
      return false;
   }

   public boolean affectsMother() {
      return false;
   }
}
