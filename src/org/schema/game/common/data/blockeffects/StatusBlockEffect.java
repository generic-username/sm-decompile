package org.schema.game.common.data.blockeffects;

import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.graphicsengine.core.Timer;

public abstract class StatusBlockEffect extends BlockEffect {
   private final int blockCount;
   private long idServer;
   private float effectCap;
   private float powerConsumption;
   private float basicMultiplier;

   public StatusBlockEffect(SendableSegmentController var1, BlockEffectTypes var2, int var3, long var4, float var6, float var7, float var8) {
      super(var1, var2);
      this.blockCount = var3;
      this.effectCap = var6;
      this.idServer = var4;
      this.powerConsumption = var7;
      this.basicMultiplier = var8;
   }

   public void update(Timer var1, FastSegmentControllerStatus var2) {
      if (((PowerManagerInterface)((ManagedSegmentController)this.segmentController).getManagerContainer()).getPowerAddOn().consumePower((double)((float)this.getBlockCount() * this.getBasePoserConsumption()), var1) == 0.0D) {
         if (this.segmentController.isOnServer()) {
         }
      } else if (this.isAlive() && this.blockCount > 0) {
         this.setRatio(var2, Math.min(this.getMax(), (float)this.blockCount / (this.segmentController.getMassWithDocks() * VoidElementManager.DEVENSIVE_EFFECT_MAX_PERCENT_MASS_MULT)));
      }

      SegmentPiece var3;
      if (this.segmentController.isOnServer() && (var3 = this.segmentController.getSegmentBuffer().getPointUnsave(ElementCollection.getPosIndexFrom4(this.idServer))) != null && var3.getAbsoluteIndexWithType4() != this.idServer) {
         this.end();
         System.err.println("[SERVER][EFFECT] " + this + " ended because effect block has been removed");
      }

   }

   public boolean needsDeadUpdate() {
      return true;
   }

   protected float getMax() {
      return 1.0F;
   }

   public abstract void setRatio(FastSegmentControllerStatus var1, float var2);

   public int getBlockCount() {
      return this.blockCount;
   }

   public final float getBasePoserConsumption() {
      return this.powerConsumption;
   }

   public final float getBaseMultiplier() {
      return this.basicMultiplier;
   }

   public abstract float getRatio(FastSegmentControllerStatus var1);

   public long getPos() {
      return this.idServer;
   }

   public void setPos(long var1) {
      this.idServer = var1;
   }

   public float getEffectCap() {
      return this.effectCap;
   }

   public float getPowerConsumption() {
      return this.powerConsumption;
   }
}
