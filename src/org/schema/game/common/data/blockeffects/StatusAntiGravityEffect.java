package org.schema.game.common.data.blockeffects;

import org.schema.game.common.controller.SendableSegmentController;

public class StatusAntiGravityEffect extends StatusBlockEffect {
   public StatusAntiGravityEffect(SendableSegmentController var1, int var2, long var3, float var5, float var6, float var7) {
      super(var1, BlockEffectTypes.STATUS_ANTI_GRAVITY, var2, var3, var5, var6, var7);
   }

   public void setRatio(FastSegmentControllerStatus var1, float var2) {
      var1.antiGravity = Math.min(this.getEffectCap(), var2);
   }

   public float getRatio(FastSegmentControllerStatus var1) {
      return var1.antiGravity;
   }

   public boolean affectsMother() {
      return true;
   }
}
