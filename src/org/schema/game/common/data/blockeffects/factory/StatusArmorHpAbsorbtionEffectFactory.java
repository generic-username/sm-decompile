package org.schema.game.common.data.blockeffects.factory;

import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.blockeffects.StatusArmorHpAbsorptionBonusEffect;

public class StatusArmorHpAbsorbtionEffectFactory extends StatusEffectFactory {
   public StatusArmorHpAbsorptionBonusEffect getInstanceFromNT(SendableSegmentController var1) {
      return new StatusArmorHpAbsorptionBonusEffect(var1, this.blockCount, this.idServer, this.effectCap, this.powerConsumption, this.multiplier);
   }
}
