package org.schema.game.common.data.blockeffects.factory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.blockeffects.BlockEffectFactory;
import org.schema.game.common.data.blockeffects.StopEffect;

public class StopEffectFactory implements BlockEffectFactory {
   public float stoppingForce;

   public void decode(DataInputStream var1) throws IOException {
      this.stoppingForce = var1.readFloat();
   }

   public void encode(DataOutputStream var1) throws IOException {
      var1.writeFloat(this.stoppingForce);
   }

   public StopEffect getInstanceFromNT(SendableSegmentController var1) {
      return new StopEffect(var1, this.stoppingForce);
   }

   public void setFrom(StopEffect var1) {
      this.stoppingForce = var1.getStoppingForce();
   }
}
