package org.schema.game.common.data.blockeffects.factory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.blockeffects.BlockEffectFactory;
import org.schema.game.common.data.blockeffects.TakeOffEffect;

public class TakeOffEffectFactory implements BlockEffectFactory {
   private float force;
   private float x;
   private float y;
   private float z;

   public void decode(DataInputStream var1) throws IOException {
      this.force = var1.readFloat();
      this.x = var1.readFloat();
      this.y = var1.readFloat();
      this.z = var1.readFloat();
   }

   public void encode(DataOutputStream var1) throws IOException {
      var1.writeFloat(this.force);
      var1.writeFloat(this.x);
      var1.writeFloat(this.y);
      var1.writeFloat(this.z);
   }

   public TakeOffEffect getInstanceFromNT(SendableSegmentController var1) {
      return new TakeOffEffect(var1, this.force, this.x, this.y, this.z);
   }

   public void setFrom(TakeOffEffect var1) {
      this.force = var1.getForce();
      this.x = var1.getDirection().x;
      this.y = var1.getDirection().y;
      this.z = var1.getDirection().z;
   }
}
