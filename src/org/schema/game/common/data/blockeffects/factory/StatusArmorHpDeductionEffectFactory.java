package org.schema.game.common.data.blockeffects.factory;

import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.blockeffects.StatusArmorHpDeductionBonusEffect;

public class StatusArmorHpDeductionEffectFactory extends StatusEffectFactory {
   public StatusArmorHpDeductionBonusEffect getInstanceFromNT(SendableSegmentController var1) {
      return new StatusArmorHpDeductionBonusEffect(var1, this.blockCount, this.idServer, this.effectCap, this.powerConsumption, this.multiplier);
   }
}
