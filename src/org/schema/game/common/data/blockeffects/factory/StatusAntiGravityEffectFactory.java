package org.schema.game.common.data.blockeffects.factory;

import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.blockeffects.StatusAntiGravityEffect;

public class StatusAntiGravityEffectFactory extends StatusEffectFactory {
   public StatusAntiGravityEffect getInstanceFromNT(SendableSegmentController var1) {
      return new StatusAntiGravityEffect(var1, this.blockCount, this.idServer, this.effectCap, this.powerConsumption, this.multiplier);
   }
}
