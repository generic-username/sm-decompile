package org.schema.game.common.data.blockeffects.factory;

import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.blockeffects.StatusShieldHardenEffect;

public class StatusShieldHardenEffectFactory extends StatusEffectFactory {
   public StatusShieldHardenEffect getInstanceFromNT(SendableSegmentController var1) {
      return new StatusShieldHardenEffect(var1, this.blockCount, this.idServer, this.effectCap, this.powerConsumption, this.multiplier);
   }
}
