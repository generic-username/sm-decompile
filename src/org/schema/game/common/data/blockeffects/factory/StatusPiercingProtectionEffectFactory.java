package org.schema.game.common.data.blockeffects.factory;

import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.blockeffects.StatusPiercingProtectionEffect;

public class StatusPiercingProtectionEffectFactory extends StatusEffectFactory {
   public StatusPiercingProtectionEffect getInstanceFromNT(SendableSegmentController var1) {
      return new StatusPiercingProtectionEffect(var1, this.blockCount, this.idServer, this.effectCap, this.powerConsumption, this.multiplier);
   }
}
