package org.schema.game.common.data.blockeffects.factory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.blockeffects.BlockEffectFactory;
import org.schema.game.common.data.blockeffects.ControllessEffect;

public class ControllessEffectFactory implements BlockEffectFactory {
   public void decode(DataInputStream var1) throws IOException {
   }

   public void encode(DataOutputStream var1) throws IOException {
   }

   public ControllessEffect getInstanceFromNT(SendableSegmentController var1) {
      return new ControllessEffect(var1);
   }

   public void setFrom(ControllessEffect var1) {
   }
}
