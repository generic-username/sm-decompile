package org.schema.game.common.data.blockeffects.factory;

import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.blockeffects.StatusArmorHardenEffect;

public class StatusArmorHardenEffectFactory extends StatusEffectFactory {
   public StatusArmorHardenEffect getInstanceFromNT(SendableSegmentController var1) {
      return new StatusArmorHardenEffect(var1, this.blockCount, this.idServer, this.effectCap, this.powerConsumption, this.multiplier);
   }
}
