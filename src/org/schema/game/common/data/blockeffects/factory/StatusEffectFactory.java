package org.schema.game.common.data.blockeffects.factory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.data.blockeffects.BlockEffectFactory;
import org.schema.game.common.data.blockeffects.StatusBlockEffect;

public abstract class StatusEffectFactory implements BlockEffectFactory {
   protected int blockCount;
   protected long idServer;
   protected float effectCap;
   protected float powerConsumption;
   protected float multiplier;

   public void decode(DataInputStream var1) throws IOException {
      this.blockCount = var1.readInt();
      this.effectCap = var1.readFloat();
      this.powerConsumption = var1.readFloat();
      this.multiplier = var1.readFloat();
   }

   public void encode(DataOutputStream var1) throws IOException {
      var1.writeInt(this.blockCount);
      var1.writeFloat(this.effectCap);
      var1.writeFloat(this.powerConsumption);
      var1.writeFloat(this.multiplier);
   }

   public void setFrom(StatusBlockEffect var1) {
      this.effectCap = var1.getEffectCap();
      this.blockCount = var1.getBlockCount();
      this.idServer = var1.getPos();
      this.powerConsumption = var1.getPowerConsumption();
      this.multiplier = var1.getBaseMultiplier();
   }
}
