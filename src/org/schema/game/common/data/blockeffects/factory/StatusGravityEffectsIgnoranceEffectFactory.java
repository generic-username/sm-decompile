package org.schema.game.common.data.blockeffects.factory;

import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.blockeffects.StatusGravityEffectIgnoranceEffect;

public class StatusGravityEffectsIgnoranceEffectFactory extends StatusEffectFactory {
   public StatusGravityEffectIgnoranceEffect getInstanceFromNT(SendableSegmentController var1) {
      return new StatusGravityEffectIgnoranceEffect(var1, this.blockCount, this.idServer, this.effectCap, this.powerConsumption, this.multiplier);
   }
}
