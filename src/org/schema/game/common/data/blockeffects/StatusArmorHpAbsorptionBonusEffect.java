package org.schema.game.common.data.blockeffects;

import org.schema.game.common.controller.SendableSegmentController;

public class StatusArmorHpAbsorptionBonusEffect extends StatusBlockEffect {
   public StatusArmorHpAbsorptionBonusEffect(SendableSegmentController var1, int var2, long var3, float var5, float var6, float var7) {
      super(var1, BlockEffectTypes.STATUS_ARMOR_HP_ABSORPTION_BONUS, var2, var3, var5, var6, var7);
   }

   public void setRatio(FastSegmentControllerStatus var1, float var2) {
      var1.armorHPAbsorbtionBonus = Math.min(this.getEffectCap(), var2);
   }

   public float getRatio(FastSegmentControllerStatus var1) {
      return var1.armorHPAbsorbtionBonus;
   }

   public boolean affectsMother() {
      return false;
   }
}
