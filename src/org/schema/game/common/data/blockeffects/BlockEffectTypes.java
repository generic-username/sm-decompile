package org.schema.game.common.data.blockeffects;

import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.blockeffects.factory.BlockEffectFactoryFac;
import org.schema.game.common.data.blockeffects.factory.ControllessEffectFactory;
import org.schema.game.common.data.blockeffects.factory.EvadeEffectFactory;
import org.schema.game.common.data.blockeffects.factory.NullEffectFactory;
import org.schema.game.common.data.blockeffects.factory.PowerRegenDownEffectFactory;
import org.schema.game.common.data.blockeffects.factory.PullEffectFactory;
import org.schema.game.common.data.blockeffects.factory.PushEffectFactory;
import org.schema.game.common.data.blockeffects.factory.ShieldRegenDownEffectFactory;
import org.schema.game.common.data.blockeffects.factory.StatusAntiGravityEffectFactory;
import org.schema.game.common.data.blockeffects.factory.StatusArmorHardenEffectFactory;
import org.schema.game.common.data.blockeffects.factory.StatusArmorHpAbsorbtionEffectFactory;
import org.schema.game.common.data.blockeffects.factory.StatusArmorHpDeductionEffectFactory;
import org.schema.game.common.data.blockeffects.factory.StatusGravityEffectsIgnoranceEffectFactory;
import org.schema.game.common.data.blockeffects.factory.StatusPiercingProtectionEffectFactory;
import org.schema.game.common.data.blockeffects.factory.StatusPowerShieldEffectFactory;
import org.schema.game.common.data.blockeffects.factory.StatusShieldHardenEffectFactory;
import org.schema.game.common.data.blockeffects.factory.StatusTopSpeedEffectFactory;
import org.schema.game.common.data.blockeffects.factory.StopEffectFactory;
import org.schema.game.common.data.blockeffects.factory.TakeOffEffectFactory;
import org.schema.game.common.data.blockeffects.factory.ThrusterOutageEffectFactory;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;

public enum BlockEffectTypes {
   NULL_EFFECT(NullEffect.class, new BlockEffectFactoryFac() {
      public final BlockEffectFactory getInstance() {
         return new NullEffectFactory();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_0;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_1;
      }
   }, -9223372036854775798L),
   CONTROLLESS(ControllessEffect.class, new BlockEffectFactoryFac() {
      public final BlockEffectFactory getInstance() {
         return new ControllessEffectFactory();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_2;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_3;
      }
   }, -9223372036854775797L),
   THRUSTER_OUTAGE(ThrusterOutageEffect.class, new BlockEffectFactoryFac() {
      public final BlockEffectFactory getInstance() {
         return new ThrusterOutageEffectFactory();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_4;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_5;
      }
   }, -9223372036854775796L),
   NO_POWER_RECHARGE(PowerRegenDownEffect.class, new BlockEffectFactoryFac() {
      public final BlockEffectFactory getInstance() {
         return new PowerRegenDownEffectFactory();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_6;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_7;
      }
   }, -9223372036854775795L),
   NO_SHIELD_RECHARGE(ShieldRegenDownEffect.class, new BlockEffectFactoryFac() {
      public final BlockEffectFactory getInstance() {
         return new ShieldRegenDownEffectFactory();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_8;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_9;
      }
   }, -9223372036854775794L),
   PUSH(PushEffect.class, new BlockEffectFactoryFac() {
      public final BlockEffectFactory getInstance() {
         return new PushEffectFactory();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_10;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_11;
      }
   }, -9223372036854775793L),
   PULL(PullEffect.class, new BlockEffectFactoryFac() {
      public final BlockEffectFactory getInstance() {
         return new PullEffectFactory();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_12;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_13;
      }
   }, -9223372036854775792L),
   STOP(StopEffect.class, new BlockEffectFactoryFac() {
      public final BlockEffectFactory getInstance() {
         return new StopEffectFactory();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_14;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_15;
      }
   }, -9223372036854775791L),
   STATUS_ARMOR_HARDEN(StatusArmorHardenEffect.class, new BlockEffectFactoryFac() {
      public final BlockEffectFactory getInstance() {
         return new StatusArmorHardenEffectFactory();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_16;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_17;
      }
   }, -9223372036854775790L),
   STATUS_PIERCING_PROTECTION(StatusPiercingProtectionEffect.class, new BlockEffectFactoryFac() {
      public final BlockEffectFactory getInstance() {
         return new StatusPiercingProtectionEffectFactory();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_18;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_19;
      }
   }, -9223372036854775789L),
   STATUS_POWER_SHIELD(StatusPowerShieldEffect.class, new BlockEffectFactoryFac() {
      public final BlockEffectFactory getInstance() {
         return new StatusPowerShieldEffectFactory();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_20;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_21;
      }
   }, -9223372036854775788L),
   STATUS_SHIELD_HARDEN(StatusShieldHardenEffect.class, new BlockEffectFactoryFac() {
      public final BlockEffectFactory getInstance() {
         return new StatusShieldHardenEffectFactory();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_22;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_23;
      }
   }, -9223372036854775787L),
   STATUS_ARMOR_HP_DEDUCTION_BONUS(StatusArmorHpDeductionBonusEffect.class, new BlockEffectFactoryFac() {
      public final BlockEffectFactory getInstance() {
         return new StatusArmorHpDeductionEffectFactory();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_24;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_25;
      }
   }, -9223372036854775786L),
   STATUS_ARMOR_HP_ABSORPTION_BONUS(StatusArmorHpAbsorptionBonusEffect.class, new BlockEffectFactoryFac() {
      public final BlockEffectFactory getInstance() {
         return new StatusArmorHpAbsorbtionEffectFactory();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_26;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_27;
      }
   }, -9223372036854775785L),
   STATUS_TOP_SPEED(StatusTopSpeedEffect.class, new BlockEffectFactoryFac() {
      public final BlockEffectFactory getInstance() {
         return new StatusTopSpeedEffectFactory();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_28;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_29;
      }
   }, -9223372036854775784L),
   STATUS_ANTI_GRAVITY(StatusAntiGravityEffect.class, new BlockEffectFactoryFac() {
      public final BlockEffectFactory getInstance() {
         return new StatusAntiGravityEffectFactory();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_30;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_31;
      }
   }, -9223372036854775783L),
   STATUS_GRAVITY_EFFECT_IGNORANCE(StatusGravityEffectIgnoranceEffect.class, new BlockEffectFactoryFac() {
      public final BlockEffectFactory getInstance() {
         return new StatusGravityEffectsIgnoranceEffectFactory();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_32;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_33;
      }
   }, -9223372036854775782L),
   TAKE_OFF(TakeOffEffect.class, new BlockEffectFactoryFac() {
      public final BlockEffectFactory getInstance() {
         return new TakeOffEffectFactory();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_34;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_35;
      }
   }, -9223372036854775781L, true),
   EVADE(EvadeEffect.class, new BlockEffectFactoryFac() {
      public final BlockEffectFactory getInstance() {
         return new EvadeEffectFactory();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_38;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_BLOCKEFFECTTYPES_36;
      }
   }, -9223372036854775780L, true);

   public final BlockEffectFactoryFac effectFactory;
   private final Class clazz;
   private final Translatable name;
   private final Translatable description;
   private final long usableId;
   public final boolean oneTimeUse;

   private BlockEffectTypes(Class var3, BlockEffectFactoryFac var4, Translatable var5, Translatable var6, long var7) {
      this(var3, var4, var5, var6, var7, false);
   }

   private BlockEffectTypes(Class var3, BlockEffectFactoryFac var4, Translatable var5, Translatable var6, long var7, boolean var9) {
      this.effectFactory = var4;
      this.name = var5;
      this.description = var6;
      this.clazz = var3;
      this.usableId = var7;
      this.oneTimeUse = var9;
   }

   public final String getName() {
      return this.name.getName(this);
   }

   public final String getDescription() {
      return this.description.getName(this);
   }

   public final CharSequence getShopDescription() {
      return this.getName() + ":\n" + this.getDescription();
   }

   public final Class getClazz() {
      return this.clazz;
   }

   public final long getUsableId() {
      return this.usableId;
   }

   public final StatusEffectType getAssociatedStatusEffectType() {
      switch(this) {
      case EVADE:
         return StatusEffectType.THRUSTER_EVADE;
      case TAKE_OFF:
         return StatusEffectType.THRUSTER_BLAST;
      default:
         throw new RuntimeException("no corresponding StatusEffectType for " + this.getName());
      }
   }
}
