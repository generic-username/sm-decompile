package org.schema.game.common.data.blockeffects.config.elements;

import org.schema.common.util.StringTools;
import org.schema.schine.common.language.Lng;

public enum ModifierStackType {
   ADD("+"),
   MULT("x"),
   SET("");

   public final String sign;

   private ModifierStackType(String var3) {
      this.sign = var3;
   }

   public final String getVerbFloat(double var1, String var3, boolean var4, boolean var5, boolean var6) {
      if (var4) {
         switch(this) {
         case ADD:
            return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_CONFIG_ELEMENTS_MODIFIERSTACKTYPE_0, StringTools.formatPointZero(var1 * 100.0D), var3);
         case MULT:
            if (var1 < 1.0D) {
               return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_CONFIG_ELEMENTS_MODIFIERSTACKTYPE_1, var3, StringTools.formatPointZero(var1 * 100.0D));
            }

            return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_CONFIG_ELEMENTS_MODIFIERSTACKTYPE_2, var3, StringTools.formatPointZero(var1 * 100.0D));
         case SET:
            if (var1 == 1.0D && !var6) {
               return "";
            } else {
               String var7 = Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_CONFIG_ELEMENTS_MODIFIERSTACKTYPE_9;
               if (var5 && var1 < 0.0D) {
                  return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_CONFIG_ELEMENTS_MODIFIERSTACKTYPE_10, var3, var7);
               }

               return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_CONFIG_ELEMENTS_MODIFIERSTACKTYPE_3, var3, StringTools.formatPointZero(var1 * 100.0D));
            }
         default:
            throw new RuntimeException(this.name());
         }
      } else {
         return this.getVerb(StringTools.formatPointZeroZero(var1), var3);
      }
   }

   public final String getVerbInt(int var1, String var2) {
      return this.getVerb(String.valueOf(var1), var2);
   }

   public final String getVerb(String var1, String var2) {
      switch(this) {
      case ADD:
         return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_CONFIG_ELEMENTS_MODIFIERSTACKTYPE_4, var1, var2);
      case MULT:
         return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_CONFIG_ELEMENTS_MODIFIERSTACKTYPE_5, var2, var1);
      case SET:
         return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_CONFIG_ELEMENTS_MODIFIERSTACKTYPE_6, var2, var1);
      default:
         throw new RuntimeException(this.name());
      }
   }

   public final String getVerbBool(boolean var1, String var2) {
      switch(this) {
      case ADD:
         return "-illegal operation ADD-";
      case MULT:
         return "-illegal operation MULT-";
      case SET:
         if (var1) {
            return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_CONFIG_ELEMENTS_MODIFIERSTACKTYPE_7, var2);
         }

         return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_CONFIG_ELEMENTS_MODIFIERSTACKTYPE_8, var2);
      default:
         throw new RuntimeException(this.name());
      }
   }
}
