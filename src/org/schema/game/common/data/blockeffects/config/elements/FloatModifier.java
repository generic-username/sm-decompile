package org.schema.game.common.data.blockeffects.config.elements;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.data.blockeffects.config.EffectException;
import org.schema.game.common.data.blockeffects.config.parameter.StatusEffectParameterType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class FloatModifier extends EffectModifier {
   private float value;

   public float getValue() {
      return this.value;
   }

   public StatusEffectParameterType getType() {
      return StatusEffectParameterType.FLOAT;
   }

   public void parseValue(Node var1) {
      try {
         this.value = Float.parseFloat(var1.getTextContent().trim());
      } catch (NumberFormatException var2) {
         throw new EffectException(var2);
      }
   }

   public void serialize(DataOutput var1) throws IOException {
      var1.writeFloat(this.value);
   }

   public void deserialize(DataInput var1) throws IOException {
      this.value = var1.readFloat();
   }

   protected void writeValueToNode(Document var1, Element var2) {
      var2.setTextContent(String.valueOf(this.value));
   }

   public long valueHash() {
      return (long)Float.floatToIntBits(this.value);
   }

   public void set(float var1) {
      this.value = var1;
   }
}
