package org.schema.game.common.data.blockeffects.config.parameter;

public enum StatusEffectParameterNames {
   VALUE,
   WEAPON_TYPE;
}
