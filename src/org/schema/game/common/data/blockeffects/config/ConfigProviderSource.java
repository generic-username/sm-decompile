package org.schema.game.common.data.blockeffects.config;

import it.unimi.dsi.fastutil.shorts.ShortList;

public interface ConfigProviderSource {
   ShortList getAppliedConfigGroups(ShortList var1);

   long getSourceId();
}
