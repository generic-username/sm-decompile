package org.schema.game.common.data.blockeffects.config;

public interface ConfigPoolProvider {
   ConfigPool getConfigPool();
}
