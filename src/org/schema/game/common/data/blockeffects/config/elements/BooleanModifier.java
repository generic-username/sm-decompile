package org.schema.game.common.data.blockeffects.config.elements;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.data.blockeffects.config.EffectException;
import org.schema.game.common.data.blockeffects.config.parameter.StatusEffectParameterType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class BooleanModifier extends EffectModifier {
   private boolean value;

   public boolean getValue() {
      return this.value;
   }

   public StatusEffectParameterType getType() {
      return StatusEffectParameterType.BOOLEAN;
   }

   public void parseValue(Node var1) {
      try {
         this.value = Boolean.parseBoolean(var1.getTextContent().trim());
      } catch (NumberFormatException var2) {
         throw new EffectException(var2);
      }
   }

   public void serialize(DataOutput var1) throws IOException {
      var1.writeBoolean(this.value);
   }

   public void deserialize(DataInput var1) throws IOException {
      this.value = var1.readBoolean();
   }

   protected void writeValueToNode(Document var1, Element var2) {
      var2.setTextContent(String.valueOf(this.value));
   }

   public long valueHash() {
      return this.value ? 1231L : 214305L;
   }

   public void set(boolean var1) {
      this.value = var1;
   }
}
