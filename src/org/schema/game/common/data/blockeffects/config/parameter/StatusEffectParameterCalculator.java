package org.schema.game.common.data.blockeffects.config.parameter;

import java.util.List;
import org.schema.game.common.data.blockeffects.config.EffectModule;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;

public interface StatusEffectParameterCalculator {
   void calculate(EffectModule var1, StatusEffectType var2, StatusEffectParameterType var3, List var4);
}
