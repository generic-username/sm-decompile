package org.schema.game.common.data.blockeffects.config;

import com.bulletphysics.util.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import javax.vecmath.Vector3f;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.data.blockeffects.config.parameter.StatusEffectParameter;
import org.schema.game.common.data.blockeffects.config.parameter.StatusEffectParameterNames;
import org.schema.game.common.data.blockeffects.config.parameter.StatusEffectParameterType;

public class EffectModule {
   private float floatValue;
   private int intValue;
   private boolean booleanValue;
   private Vector3f vector3fValue;
   private final Object2ObjectMap weaponType = new Object2ObjectOpenHashMap();
   private StatusEffectType type;
   private StatusEffectParameterType valueType;
   private EffectModule parent;
   private DamageDealerType ownWeaponType;

   public void create(StatusEffectType var1, List var2) {
      this.type = var1;

      assert this.type != null;

      Class[] var3;
      int var4 = (var3 = var1.effectParameters).length;
      int var5 = 0;

      while(var5 < var4) {
         Class var6 = var3[var5];
         StatusEffectParameter var12 = var1.getInstance(var6);
         switch(var12.name) {
         case VALUE:
            ++var5;
            break;
         case WEAPON_TYPE:
            Object2ObjectOpenHashMap var8 = new Object2ObjectOpenHashMap();

            Iterator var7;
            EffectConfigElement var9;
            Object var11;
            for(var7 = var2.iterator(); var7.hasNext(); ((List)var11).add(var9)) {
               var9 = (EffectConfigElement)var7.next();
               if ((var11 = (List)var8.get(var9.getWeaponType())) == null) {
                  var11 = new ObjectArrayList();
                  var8.put(var9.getWeaponType(), var11);
               }
            }

            var7 = var8.entrySet().iterator();

            while(var7.hasNext()) {
               Entry var10 = (Entry)var7.next();
               EffectModule var13;
               (var13 = new EffectModule()).setParent(this, (DamageDealerType)var10.getKey());
               Collections.sort((List)var10.getValue());
               var13.calculateValue(var1, (List)var10.getValue());
               this.weaponType.put(var10.getKey(), var13);
            }

            return;
         default:
            throw new EffectException("Unknown name: " + var12.name.name());
         }
      }

      this.calculateValue(var1, var2);
   }

   private void calculateValue(StatusEffectType var1, List var2) {
      Class[] var3;
      int var4 = (var3 = var1.effectParameters).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         Class var6 = var3[var5];
         StatusEffectParameter var7;
         if ((var7 = var1.getInstance(var6)).name == StatusEffectParameterNames.VALUE) {
            this.calculateValue(var7.type, var2);
         }
      }

   }

   private void calculateValue(StatusEffectParameterType var1, List var2) {
      this.valueType = var1;
      var1.calculator.calculate(this, this.type, this.valueType, var2);
   }

   private void checkValueType(StatusEffectParameterType var1) {
      if (this.valueType != var1) {
         throw new IllegalArgumentException("Illegal value type. Expected " + var1.name() + ", but got " + this.valueType.name());
      }
   }

   public void setIntValue(int var1) {
      this.checkValueType(StatusEffectParameterType.INT);
      this.intValue = var1;
   }

   public void setFloatValue(float var1) {
      this.checkValueType(StatusEffectParameterType.FLOAT);
      this.floatValue = var1;
   }

   public void setBooleanValue(boolean var1) {
      this.checkValueType(StatusEffectParameterType.BOOLEAN);
      this.booleanValue = var1;
   }

   public void setVector3fValue(Vector3f var1) {
      this.checkValueType(StatusEffectParameterType.VECTOR3f);
      this.vector3fValue = var1;
   }

   public float getFloatValue() {
      this.checkValueType(StatusEffectParameterType.FLOAT);
      return this.floatValue;
   }

   public int getIntValue() {
      this.checkValueType(StatusEffectParameterType.INT);
      return this.intValue;
   }

   public boolean getBooleanValue() {
      this.checkValueType(StatusEffectParameterType.BOOLEAN);
      return this.booleanValue;
   }

   public Vector3f getVector3fValue() {
      this.checkValueType(StatusEffectParameterType.VECTOR3f);
      return this.vector3fValue;
   }

   public Object2ObjectMap getWeaponType() {
      return this.weaponType;
   }

   public StatusEffectType getType() {
      return this.type != null ? this.type : this.getParent().type;
   }

   public StatusEffectParameterType getValueType() {
      return this.valueType;
   }

   public String getValueString() {
      switch(this.valueType) {
      case BOOLEAN:
         return String.valueOf(this.getBooleanValue());
      case BYTE:
      case DOUBLE:
      case LONG:
      case SHORT:
      case VECTOR2f:
      case VECTOR3i:
      case VECTOR4f:
      case WEAPON_TYPE:
      default:
         return "UNDEF";
      case FLOAT:
         return String.valueOf(this.getFloatValue());
      case INT:
         return String.valueOf(this.getIntValue());
      case VECTOR3f:
         return String.valueOf(this.getVector3fValue());
      }
   }

   public String toString() {
      return "EffectModule[" + this.type.name() + " " + this.type.getName() + " -> " + this.getValueString() + "]";
   }

   public EffectModule getParent() {
      return this.parent;
   }

   public void setParent(EffectModule var1, DamageDealerType var2) {
      this.parent = var1;
      this.ownWeaponType = var2;
   }

   public String getOwnWeaponTypeString() {
      assert this.parent != null;

      assert this.ownWeaponType != null;

      return this.ownWeaponType.getName();
   }
}
