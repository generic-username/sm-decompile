package org.schema.game.common.data.gamemode;

import org.schema.game.common.controller.FactionChange;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.network.objects.NetworkGameState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;

public abstract class AbstractGameMode {
   public final GameServerState state;

   public AbstractGameMode(GameServerState var1) {
      this.state = var1;
   }

   public void initialize() throws GameModeException {
      System.err.println("[BATTLEMODE] initializing");
      this.initBegin();
      this.onFactionInitServer(this.state.getFactionManager());
      this.initEnd();
   }

   public abstract void updateToFullNT(NetworkGameState var1);

   protected abstract void initBegin() throws GameModeException;

   protected abstract void initEnd() throws GameModeException;

   public abstract void onFactionInitServer(FactionManager var1);

   public abstract void update(Timer var1) throws GameModeException;

   public abstract String getCurrentOutput();

   public abstract boolean allowedToSpawnBBShips(PlayerState var1, Faction var2);

   public abstract void announceKill(PlayerState var1, int var2);

   public abstract void onFactionChanged(PlayerState var1, FactionChange var2);
}
