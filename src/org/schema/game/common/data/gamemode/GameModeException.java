package org.schema.game.common.data.gamemode;

public class GameModeException extends Exception {
   private static final long serialVersionUID = 1L;

   public GameModeException() {
   }

   public GameModeException(String var1, Throwable var2) {
      super(var1, var2);
   }

   public GameModeException(String var1) {
      super(var1);
   }

   public GameModeException(Throwable var1) {
      super(var1);
   }
}
