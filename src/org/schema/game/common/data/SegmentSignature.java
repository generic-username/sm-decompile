package org.schema.game.common.data;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;

public class SegmentSignature {
   public boolean empty = false;
   public SegmentController context;
   private Vector3i pos;
   private long lastChanged;
   private short size;

   public SegmentSignature() {
   }

   public SegmentSignature(Vector3i var1, long var2, boolean var4, short var5) {
      this.pos = var1;
      this.lastChanged = var2;
      this.empty = var4;
      this.setSize(var5);
   }

   public long getLastChanged() {
      return this.lastChanged;
   }

   public void setLastChanged(long var1) {
      this.lastChanged = var1;
   }

   public Vector3i getPos() {
      return this.pos;
   }

   public void setPos(Vector3i var1) {
      this.pos = var1;
   }

   public short getSize() {
      return this.size;
   }

   public void setSize(short var1) {
      this.size = var1;
   }
}
