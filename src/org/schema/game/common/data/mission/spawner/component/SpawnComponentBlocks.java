package org.schema.game.common.data.mission.spawner.component;

import org.schema.game.common.data.mission.spawner.SpawnMarker;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class SpawnComponentBlocks implements SpawnComponent {
   short type;
   int count;

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.type = (Short)var2[0].getValue();
      this.count = (Integer)var2[1].getValue();
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.SHORT, (String)null, this.type), new Tag(Tag.Type.INT, (String)null, this.count), FinishTag.INST});
   }

   public void execute(SpawnMarker var1) {
   }

   public SpawnComponentType getType() {
      return SpawnComponentType.BLOCKS;
   }
}
