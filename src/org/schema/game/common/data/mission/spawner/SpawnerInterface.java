package org.schema.game.common.data.mission.spawner;

import java.util.List;
import org.schema.schine.resource.tag.TagSerializable;

public interface SpawnerInterface extends TagSerializable {
   boolean canSpawn(SpawnMarker var1);

   void spawn(SpawnMarker var1);

   boolean isAlive();

   void setAlive(boolean var1);

   List getComponents();

   List getConditions();
}
