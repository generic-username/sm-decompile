package org.schema.game.common.data.mission.spawner;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.List;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class SpawnController implements TagSerializable {
   public long lastUpdate;
   private List spawnMarkers = new ObjectArrayList(0);
   private SimpleTransformableSendableObject attachedTo;

   public SpawnController(SimpleTransformableSendableObject var1) {
      this.attachedTo = var1;
   }

   public void update(Timer var1) {
      if (!this.spawnMarkers.isEmpty() && System.currentTimeMillis() - this.lastUpdate > 500L) {
         for(int var3 = 0; var3 < this.spawnMarkers.size(); ++var3) {
            SpawnMarker var2;
            if ((var2 = (SpawnMarker)this.spawnMarkers.get(var3)).canSpawn()) {
               var2.canSpawn();
               var2.spawn();
            }

            if (!var2.isAlive()) {
               this.spawnMarkers.remove(var3);
               --var3;
            }
         }

         this.lastUpdate = System.currentTimeMillis();
      }

   }

   public void fromTagStructure(Tag var1) {
      Tag[] var4 = (Tag[])((Tag[])var1.getValue())[0].getValue();
      this.spawnMarkers = new ObjectArrayList(var4.length - 1);

      for(int var2 = 0; var2 < var4.length - 1; ++var2) {
         SpawnMarker var3;
         (var3 = new SpawnMarker()).setAttachedTo(this.attachedTo);
         var3.fromTagStructure(var4[var2]);
         this.spawnMarkers.add(var3);
      }

   }

   public Tag toTagStructure() {
      Tag[] var1;
      Tag[] var10000 = var1 = new Tag[this.spawnMarkers.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;

      for(int var2 = 0; var2 < this.spawnMarkers.size(); ++var2) {
         var1[var2] = ((SpawnMarker)this.spawnMarkers.get(var2)).toTagStructure();
      }

      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.STRUCT, (String)null, var1), FinishTag.INST});
   }

   public List getSpawnMarker() {
      return this.spawnMarkers;
   }

   public void removeAll() {
      this.spawnMarkers.clear();
   }
}
