package org.schema.game.common.data.mission.spawner.component;

import org.schema.game.common.data.mission.spawner.SpawnMarker;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class SpawnComponentDestroySpawnerAfterCount implements SpawnComponent {
   int count;
   private int maxCount;

   public SpawnComponentDestroySpawnerAfterCount(int var1) {
      this.maxCount = var1;
   }

   public SpawnComponentDestroySpawnerAfterCount() {
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.count = (Integer)var2[0].getValue();
      this.maxCount = (Integer)var2[1].getValue();
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.INT, (String)null, this.count), new Tag(Tag.Type.INT, (String)null, this.maxCount), FinishTag.INST});
   }

   public void execute(SpawnMarker var1) {
      ++this.count;
      if (this.count >= this.maxCount) {
         var1.getSpawner().setAlive(false);
      }

   }

   public SpawnComponentType getType() {
      return SpawnComponentType.DESTROY_SPAWNER_AFTER_COUNT;
   }
}
