package org.schema.game.common.data.mission.spawner.condition;

import java.util.Iterator;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.common.data.creature.AIRandomCompositeCreature;
import org.schema.game.common.data.mission.spawner.SpawnMarker;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class SpawnConditionCreatureCountOnAffinity implements SpawnCondition {
   private int count;

   public SpawnConditionCreatureCountOnAffinity() {
   }

   public SpawnConditionCreatureCountOnAffinity(int var1) {
      this.count = var1;
   }

   public boolean isSatisfied(SpawnMarker var1) {
      int var2 = 0;

      assert var1.attachedTo() != null;

      if (var1.attachedTo().getAttachedAffinity() == null) {
         System.err.println("[SERVER][SPAWNER] Cannot spawn yet: Attached Affinity null of " + var1.attachedTo());
         return false;
      } else {
         Iterator var3 = var1.attachedTo().getAttachedAffinity().iterator();

         while(var3.hasNext()) {
            if ((AICreature)var3.next() instanceof AIRandomCompositeCreature) {
               ++var2;
            }
         }

         if (var2 < this.count) {
            return true;
         } else {
            return false;
         }
      }
   }

   public SpawnConditionType getType() {
      return SpawnConditionType.CREATURE_COUNT_ON_AFFINITY;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.count = (Integer)var2[0].getValue();
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.INT, (String)null, this.count), FinishTag.INST});
   }
}
