package org.schema.game.common.data.mission.spawner.component;

public enum SpawnComponentType {
   CREATURE(SpawnComponentCreature.class),
   META_ITEM(SpawnComponentMetaItem.class),
   BLOCKS(SpawnComponentBlocks.class),
   DESTROY_SPAWNER_AFTER_COUNT(SpawnComponentDestroySpawnerAfterCount.class);

   private final Class clazz;

   private SpawnComponentType(Class var3) {
      this.clazz = var3;
   }

   public static SpawnComponent instantiate(SpawnComponentType var0) {
      try {
         SpawnComponent var1 = (SpawnComponent)var0.clazz.newInstance();

         assert var1.getType() == var0 : var0 + "; " + var0.clazz + " instantiated " + var1.getType() + "; " + var1.getClass();

         return var1;
      } catch (InstantiationException var2) {
         var2.printStackTrace();
         throw new RuntimeException(var2);
      } catch (IllegalAccessException var3) {
         var3.printStackTrace();
         throw new RuntimeException(var3);
      }
   }
}
