package org.schema.game.common.data.mission.spawner.component;

import org.schema.game.common.data.mission.spawner.SpawnMarker;
import org.schema.schine.resource.tag.TagSerializable;

public interface SpawnComponent extends TagSerializable {
   void execute(SpawnMarker var1);

   SpawnComponentType getType();
}
