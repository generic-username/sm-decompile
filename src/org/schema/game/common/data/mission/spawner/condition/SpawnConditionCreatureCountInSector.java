package org.schema.game.common.data.mission.spawner.condition;

import java.util.Iterator;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.common.data.mission.spawner.SpawnMarker;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class SpawnConditionCreatureCountInSector implements SpawnCondition {
   private int count;

   public SpawnConditionCreatureCountInSector() {
   }

   public SpawnConditionCreatureCountInSector(int var1) {
      this.count = var1;
   }

   public boolean isSatisfied(SpawnMarker var1) {
      int var2 = 0;
      Iterator var3 = var1.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

      while(var3.hasNext()) {
         Sendable var4;
         if ((var4 = (Sendable)var3.next()) instanceof AICreature && ((AICreature)var4).getSectorId() == var1.attachedTo().getSectorId()) {
            ++var2;
         }
      }

      if (var2 < this.count) {
         return true;
      } else {
         return false;
      }
   }

   public SpawnConditionType getType() {
      return SpawnConditionType.CREATURE_COUNT_IN_SECTOR;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.count = (Integer)var2[0].getValue();
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.INT, (String)null, this.count), FinishTag.INST});
   }
}
