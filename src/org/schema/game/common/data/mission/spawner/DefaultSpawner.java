package org.schema.game.common.data.mission.spawner;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import org.schema.game.common.data.mission.spawner.component.SpawnComponent;
import org.schema.game.common.data.mission.spawner.component.SpawnComponentType;
import org.schema.game.common.data.mission.spawner.condition.SpawnCondition;
import org.schema.game.common.data.mission.spawner.condition.SpawnConditionType;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class DefaultSpawner implements SpawnerInterface {
   private final List conditions = new ObjectArrayList();
   private final List components = new ObjectArrayList();
   private boolean alive = true;

   public static SpawnerInterface instantiate(Tag var0) {
      DefaultSpawner var1;
      (var1 = new DefaultSpawner()).fromTagStructure(var0);
      return var1;
   }

   public boolean canSpawn(SpawnMarker var1) {
      Iterator var2 = this.conditions.iterator();

      do {
         if (!var2.hasNext()) {
            return true;
         }
      } while(((SpawnCondition)var2.next()).isSatisfied(var1));

      return false;
   }

   public void spawn(SpawnMarker var1) {
      Iterator var2 = this.components.iterator();

      while(var2.hasNext()) {
         ((SpawnComponent)var2.next()).execute(var1);
      }

   }

   public boolean isAlive() {
      return this.alive;
   }

   public void setAlive(boolean var1) {
      this.alive = var1;
   }

   public List getComponents() {
      return this.components;
   }

   public List getConditions() {
      return this.conditions;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.getConditionFromTag(var2[0]);
      this.getComponentsFromTag(var2[1]);
      this.alive = (Byte)var2[2].getValue() != 0;
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{this.getConditionToTag(), this.getComponentsToTag(), new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.alive ? 1 : 0))), FinishTag.INST});
   }

   private Tag getComponentsToTag() {
      Tag[] var1;
      Tag[] var10000 = var1 = new Tag[this.components.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;

      for(int var2 = 0; var2 < this.components.size(); ++var2) {
         SpawnComponent var3 = (SpawnComponent)this.components.get(var2);
         var1[var2] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.INT, (String)null, var3.getType().ordinal()), var3.toTagStructure(), FinishTag.INST});
      }

      return new Tag(Tag.Type.STRUCT, (String)null, var1);
   }

   private Tag getConditionToTag() {
      Tag[] var1;
      Tag[] var10000 = var1 = new Tag[this.conditions.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;

      for(int var2 = 0; var2 < this.conditions.size(); ++var2) {
         SpawnCondition var3 = (SpawnCondition)this.conditions.get(var2);
         var1[var2] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.INT, (String)null, var3.getType().ordinal()), var3.toTagStructure(), FinishTag.INST});
      }

      return new Tag(Tag.Type.STRUCT, (String)null, var1);
   }

   private void getComponentsFromTag(Tag var1) {
      Tag[] var3 = (Tag[])var1.getValue();

      for(int var2 = 0; var2 < var3.length - 1; ++var2) {
         this.components.add(this.getComponentInstanceFromTag(var3[var2]));
      }

   }

   private SpawnComponent getComponentInstanceFromTag(Tag var1) {
      Tag[] var3 = (Tag[])var1.getValue();
      SpawnComponent var2;
      (var2 = SpawnComponentType.instantiate(SpawnComponentType.values()[(Integer)var3[0].getValue()])).fromTagStructure(var3[1]);
      return var2;
   }

   private SpawnCondition getConditionInstanceFromTag(Tag var1) {
      Tag[] var3 = (Tag[])var1.getValue();
      SpawnCondition var2;
      (var2 = SpawnConditionType.instantiate(SpawnConditionType.values()[(Integer)var3[0].getValue()])).fromTagStructure(var3[1]);
      return var2;
   }

   private void getConditionFromTag(Tag var1) {
      Tag[] var3 = (Tag[])var1.getValue();

      for(int var2 = 0; var2 < var3.length - 1; ++var2) {
         this.conditions.add(this.getConditionInstanceFromTag(var3[var2]));
      }

   }
}
