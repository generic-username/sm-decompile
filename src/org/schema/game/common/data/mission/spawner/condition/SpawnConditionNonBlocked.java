package org.schema.game.common.data.mission.spawner.condition;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.SegmentCollisionCheckerCallback;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.mission.spawner.SpawnMarker;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class SpawnConditionNonBlocked implements SpawnCondition {
   public Vector3i relativeToSpawnerPos = new Vector3i();

   public SpawnConditionNonBlocked(Vector3i var1) {
      this.relativeToSpawnerPos.set(var1);
   }

   public SpawnConditionNonBlocked() {
   }

   public boolean isSatisfied(SpawnMarker var1) {
      if (var1.attachedTo() instanceof EditableSendableSegmentController) {
         EditableSendableSegmentController var2 = (EditableSendableSegmentController)var1.attachedTo();
         Vector3i var3;
         (var3 = new Vector3i(var1.getPos())).add(this.relativeToSpawnerPos);
         SegmentPiece var4;
         return (var4 = var2.getSegmentBuffer().getPointUnsave(var3)) != null && !var2.getCollisionChecker().checkPieceCollision(var4, new SegmentCollisionCheckerCallback(), false);
      } else {
         return true;
      }
   }

   public SpawnConditionType getType() {
      return SpawnConditionType.POS_NON_BLOCKED;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.relativeToSpawnerPos = (Vector3i)var2[0].getValue();
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.VECTOR3i, (String)null, this.relativeToSpawnerPos), FinishTag.INST});
   }
}
