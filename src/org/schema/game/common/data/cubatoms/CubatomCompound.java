package org.schema.game.common.data.cubatoms;

public class CubatomCompound {
   private final CubatomFlavor[] compound;

   public CubatomCompound(CubatomFlavor[] var1) {
      assert var1 != null;

      if (var1.length != 0 && var1.length <= 4) {
         for(int var2 = 0; var2 < var1.length; ++var2) {
            for(int var3 = 0; var3 < var1.length; ++var3) {
               if (var2 != var3 && var1[var2].getState() == var1[var3].getState()) {
                  throw new IllegalArgumentException("Cubatom compound may not have 2 flavors of the same state");
               }
            }
         }

         this.compound = var1;
      } else {
         throw new IllegalArgumentException("Cubatom compound must have at least 1 and max 4 states");
      }
   }

   public CubatomFlavor[] getCompound() {
      return this.compound;
   }
}
