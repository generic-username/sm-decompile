package org.schema.game.common.data.cubatoms;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class CubatomInventory implements TagSerializable {
   private final byte[] inventory = new byte[256];
   private final byte[] lastInventory = new byte[256];
   private boolean clientChanged;

   public CubatomInventory(PlayerState var1) {
      Arrays.fill(this.inventory, (byte)-128);
      Arrays.fill(this.lastInventory, (byte)-128);
   }

   public boolean changed(int var1) {
      assert var1 >= 0 && var1 < 256 : var1;

      boolean var2 = this.inventory[var1] != this.lastInventory[var1];
      this.lastInventory[var1] = this.inventory[var1];
      return var2;
   }

   public void deserialize(DataInputStream var1) throws IOException {
      var1.readFully(this.inventory);
      this.setClientChanged(true);
   }

   public void fromTagStructure(Tag var1) {
      byte[] var3 = (byte[])var1.getValue();

      for(int var2 = 0; var2 < var3.length; ++var2) {
         this.inventory[var2] = var3[var2];
      }

   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.BYTE_ARRAY, "ci0", this.inventory);
   }

   public int getCount(int var1) {
      assert var1 >= 0 && var1 < 256 : var1;

      return this.inventory[var1] + 128;
   }

   public void inc(int var1, int var2) {
      assert var1 >= 0 && var1 < 256;

      if (var2 != 0) {
         if (this.inventory[var1] + 128 + var2 < 0) {
            this.set(var1, 0);
         } else if (this.inventory[var1] + 128 + var2 > 255) {
            this.set(var1, 255);
         } else {
            byte[] var10000 = this.inventory;
            var10000[var1] += (byte)var2;
            this.send();
         }
      }
   }

   public boolean isClientChanged() {
      return this.clientChanged;
   }

   public void setClientChanged(boolean var1) {
      this.clientChanged = var1;
   }

   private void send() {
   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.write(this.inventory);
   }

   public void set(int var1, int var2) {
      assert var1 >= 0 && var1 < 256 : var1;

      byte[] var10000 = this.inventory;
      this.inventory[var1] = (byte)(var2 - 128);
      this.send();
   }
}
