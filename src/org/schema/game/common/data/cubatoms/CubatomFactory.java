package org.schema.game.common.data.cubatoms;

import java.util.Random;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.data.element.DockingElement;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.HullElement;
import org.schema.game.common.data.element.LightElement;
import org.schema.game.common.data.element.ship.ShipElement;
import org.schema.game.common.data.element.terrain.MineralElement;

public class CubatomFactory {
   public static final Cubatom[] cubatoms = new Cubatom[256];
   public static final Cubatom[] cubatomsNatural = new Cubatom[81];
   public static final Cubatom[] cubatomsSpecial = new Cubatom[175];
   private static Random r = new Random();

   public static void generateAll() {
      int var0 = 0;
      int var1 = 0;
      int var2 = 0;

      for(int var3 = 0; var3 < 4; ++var3) {
         for(int var4 = 0; var4 < 4; ++var4) {
            for(int var5 = 0; var5 < 4; ++var5) {
               for(int var6 = 0; var6 < 4; ++var6) {
                  cubatoms[var0] = new Cubatom(new CubatomFlavor[]{CubatomFlavor.values()[var3], CubatomFlavor.values()[var4 + 4], CubatomFlavor.values()[var5 + 8], CubatomFlavor.values()[var6 + 12]});
                  if (cubatoms[var0].isNatural()) {
                     cubatomsNatural[var1] = cubatoms[var0];
                     ++var1;
                  } else {
                     cubatomsSpecial[var2] = cubatoms[var0];
                     ++var2;
                  }

                  ++var0;
               }
            }
         }
      }

   }

   public static Cubatom getRandomCubatom() {
      CubatomFlavor[] var0 = new CubatomFlavor[4];

      for(int var1 = 0; var1 < var0.length; ++var1) {
         int var2 = (var1 << 2) + r.nextInt(3);
         var0[var1] = CubatomFlavor.values()[var2];
      }

      return new Cubatom(var0);
   }

   public static CubatomCompound getRandomRecipe(short var0) {
      ElementInformation var6 = ElementKeyMap.getInfo(var0);
      byte var1;
      byte var7;
      if (ShipElement.class.equals(var6.getType())) {
         var7 = 4;
         var1 = 2;
      } else if (MineralElement.class.equals(var6.getType())) {
         var7 = 2;
         var1 = 1;
      } else if (SpaceStation.class.equals(var6.getType())) {
         var7 = 4;
         var1 = 2;
      } else if (HullElement.class.equals(var6.getType())) {
         var7 = 2;
         var1 = 1;
      } else if (LightElement.class.equals(var6.getType())) {
         var7 = 2;
         var1 = 1;
      } else {
         DockingElement.class.equals(var6.getType());
         var7 = 3;
         var1 = 1;
      }

      CubatomFlavor[] var2 = new CubatomFlavor[var7];
      int var3 = 0;

      int var4;
      int var5;
      for(var4 = 0; var4 < var2.length; ++var4) {
         var5 = (var4 << 2) + r.nextInt(3);
         var2[var4] = CubatomFlavor.values()[var5];
      }

      while(var3 < var1) {
         var4 = r.nextInt(var7);
         if (!var2[var4].isSpecial()) {
            var5 = (var4 << 2) + 3;
            var2[var4] = CubatomFlavor.values()[var5];
            ++var3;
         }
      }

      return new CubatomCompound(var2);
   }

   public static Cubatom refine(Cubatom var0, Cubatom var1) {
      CubatomFlavor[] var2 = new CubatomFlavor[4];

      int var3;
      for(var3 = 0; var3 < var0.getFlavors().length; ++var3) {
         var2[var3] = var0.getFlavors()[var3];
      }

      for(var3 = 0; var3 < var0.getFlavors().length; ++var3) {
         if (var2[var3].ordinal() == var1.getFlavors()[var3].ordinal() && var2[var3].ordinal() % 4 < 3) {
            var2[var3] = CubatomFlavor.values()[var2[var3].ordinal() + 1];
         } else if (var2[var3].ordinal() < var1.getFlavors()[var3].ordinal() && var2[var3].ordinal() % 4 < 3) {
            var2[var3] = CubatomFlavor.values()[var2[var3].ordinal() + 1];
         } else if (var2[var3].ordinal() > var1.getFlavors()[var3].ordinal() && var2[var3].ordinal() % 4 > 0) {
            var2[var3] = CubatomFlavor.values()[var2[var3].ordinal() - 1];
         }

         assert var2[var3].getState() == var0.getFlavors()[var3].getState();
      }

      return new Cubatom(var2);
   }

   static {
      generateAll();
   }
}
