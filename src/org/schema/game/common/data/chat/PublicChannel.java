package org.schema.game.common.data.chat;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.network.objects.ChatChannelModification;
import org.schema.game.network.objects.ChatMessage;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.ChatColorPalette;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.client.ClientState;

public class PublicChannel extends MuteEnabledChatChannel {
   private final List clientChannels = new ObjectArrayList();
   private final Set modsLowerCase = new ObjectOpenHashSet();
   private final Set bannedLowerCase = new ObjectOpenHashSet();
   private final String name;
   private boolean permanent;
   private String passwd;

   public PublicChannel(StateInterface var1, int var2, String var3, String var4, boolean var5, String[] var6) {
      super(var1, var2);
      this.name = var3;
      this.permanent = var5;
      this.passwd = var4;
      if (var1 instanceof ClientState) {
         this.color.set(ChatColorPalette.other);
      }

      this.addToModerator(var6);
   }

   public Collection getClientChannelsInChannel() {
      return this.clientChannels;
   }

   public boolean isPermanent() {
      return this.permanent;
   }

   public boolean hasChannelBanList() {
      return true;
   }

   public boolean hasChannelModList() {
      return true;
   }

   public boolean hasPossiblePassword() {
      return true;
   }

   protected boolean checkPassword(ChatChannelModification var1) {
      return this.passwd == null || this.passwd.equals(var1.joinPw);
   }

   public boolean hasPassword() {
      return this.passwd != null && this.passwd.length() > 0;
   }

   protected String[] getModerators() {
      String[] var1 = new String[this.modsLowerCase.size()];
      int var2 = 0;

      for(Iterator var3 = this.modsLowerCase.iterator(); var3.hasNext(); ++var2) {
         String var4 = (String)var3.next();
         var1[var2] = var4;
      }

      return var1;
   }

   public String[] getBanned() {
      String[] var1 = new String[this.bannedLowerCase.size()];
      int var2 = 0;

      for(Iterator var3 = this.bannedLowerCase.iterator(); var3.hasNext(); ++var2) {
         String var4 = (String)var3.next();
         var1[var2] = var4;
      }

      return var1;
   }

   public void onFactionChangedServer(ClientChannel var1) {
   }

   public void onLoginServer(ClientChannel var1) {
   }

   public void update() {
   }

   public boolean isAlive() {
      return this.permanent || this.clientChannels.size() > 0;
   }

   public ChatMessage process(ChatMessage var1) {
      return var1;
   }

   public String getUniqueChannelName() {
      return this.name;
   }

   public boolean canLeave() {
      return true;
   }

   public boolean isPublic() {
      return true;
   }

   public ChannelRouter.ChannelType getType() {
      return ChannelRouter.ChannelType.PUBLIC;
   }

   protected void setNewPassword(String var1) {
      this.passwd = var1;
   }

   protected void removeFromModerator(String... var1) {
      for(int var2 = 0; var2 < var1.length; ++var2) {
         this.modsLowerCase.remove(var1[var2].toLowerCase(Locale.ENGLISH));
      }

      this.setChanged();
      this.notifyObservers();
   }

   protected void addToModerator(String... var1) {
      for(int var2 = 0; var2 < var1.length; ++var2) {
         this.modsLowerCase.add(var1[var2].toLowerCase(Locale.ENGLISH));
      }

      this.setChanged();
      this.notifyObservers();
   }

   protected void removeFromBanned(String... var1) {
      for(int var2 = 0; var2 < var1.length; ++var2) {
         this.bannedLowerCase.remove(var1[var2].toLowerCase(Locale.ENGLISH));
      }

      this.setChanged();
      this.notifyObservers();
   }

   protected void addToBanned(String... var1) {
      for(int var2 = 0; var2 < var1.length; ++var2) {
         this.bannedLowerCase.add(var1[var2].toLowerCase(Locale.ENGLISH));
      }

      this.setChanged();
      this.notifyObservers();
   }

   protected boolean addToClientChannels(ClientChannel var1) {
      return !this.clientChannels.contains(var1) ? this.clientChannels.add(var1) : false;
   }

   protected boolean removeFromClientChannels(ClientChannel var1) {
      return this.clientChannels.remove(var1);
   }

   public String getPassword() {
      return this.passwd;
   }

   public String getName() {
      return this.name;
   }

   public Object getTitle() {
      return this.name;
   }

   public boolean isModerator(PlayerState var1) {
      return this.modsLowerCase.contains(var1.getName().toLowerCase(Locale.ENGLISH));
   }

   public boolean isBanned(PlayerState var1) {
      return this.bannedLowerCase.contains(var1.getName().toLowerCase(Locale.ENGLISH));
   }
}
