package org.schema.game.common.data.chat;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import javax.vecmath.Vector4f;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.network.objects.ChatChannelModification;
import org.schema.game.network.objects.ChatMessage;
import org.schema.game.network.objects.remote.RemoteChatChannel;
import org.schema.game.network.objects.remote.RemoteChatMessage;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.server.ServerStateInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public abstract class ChatChannel extends Observable implements ChatCallback {
   private static final byte CLIENT_TAG_VERSION = 1;
   private static final byte SERVER_TAG_VERSION = 2;
   protected final ObjectArrayList members = new ObjectArrayList();
   private final boolean onServer;
   private final StateInterface state;
   private final ObjectArrayList messageLog = new ObjectArrayList();
   private final ObjectArrayList visibleChatLog = new ObjectArrayList();
   private final int id;
   protected Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   private int read = 0;
   private boolean sticky = false;
   private boolean stickyFull = false;
   private GUIActivatableTextBar chatBar;
   private boolean clientOpen;
   private String clientCurrentPassword = "";

   public ChatChannel(StateInterface var1, int var2) {
      this.state = var1;
      this.onServer = var1 instanceof ServerStateInterface;
      this.id = var2;
   }

   public static PublicChannel loadServerChannel(GameServerState var0, Tag var1) {
      Tag[] var9;
      byte var2 = (Byte)(var9 = (Tag[])var1.getValue())[0].getValue();
      String var3 = (String)var9[1].getValue();
      Tag[] var4;
      String[] var5 = new String[(var4 = (Tag[])var9[2].getValue()).length - 1];

      int var6;
      for(var6 = 0; var6 < var5.length; ++var6) {
         var5[var6] = (String)var4[var6].getValue();
      }

      Tag[] var14;
      String[] var13 = new String[(var14 = (Tag[])var9[3].getValue()).length - 1];

      for(int var7 = 0; var7 < var13.length; ++var7) {
         var13[var7] = (String)var14[var7].getValue();
      }

      if (var2 <= 1) {
         String var17 = (String)var9[4].getValue();
         boolean var12 = (Byte)var9[5].getValue() > 0;
         PublicChannel var18;
         (var18 = new PublicChannel(var0, ++ChannelRouter.idGen, var3, var17, var12, var5)).addToBanned(var13);
         return var18;
      } else {
         Tag[] var15;
         String[] var11 = new String[(var15 = (Tag[])var9[4].getValue()).length - 1];

         for(var6 = 0; var6 < var11.length; ++var6) {
            var11[var6] = (String)var15[var6].getValue();
         }

         String var16 = (String)var9[5].getValue();
         boolean var10 = (Byte)var9[6].getValue() > 0;
         PublicChannel var8;
         (var8 = new PublicChannel(var0, ++ChannelRouter.idGen, var3, var16, var10, var5)).addToBanned(var13);
         var8.addToMuted(var11);
         return var8;
      }
   }

   public static LoadedClientChatChannel loadClientChannel(Tag var0) {
      Tag[] var5;
      String var1 = (String)(var5 = (Tag[])var0.getValue())[1].getValue();
      boolean var2 = (Byte)var5[2].getValue() > 0;
      boolean var3 = (Byte)var5[3].getValue() > 0;
      boolean var4 = (Byte)var5[4].getValue() > 0;
      String var6 = (String)var5[5].getValue();
      return new LoadedClientChatChannel(var1, var2, var3, var4, var6);
   }

   public abstract Collection getClientChannelsInChannel();

   public void markRead() {
      this.read = this.messageLog.size();
   }

   public int getUnread() {
      return this.messageLog.size() - this.read;
   }

   public void send(ChatMessage var1) {
      if (!this.isOnServer()) {
         if (this.checkPrefixClient(var1)) {
            ((GameClientState)this.getState()).getController().getClientChannel().getNetworkObject().chatBuffer.add(new RemoteChatMessage(var1, this.isOnServer()));
         }

      } else {
         Iterator var2 = this.getClientChannelsInChannel().iterator();

         while(var2.hasNext()) {
            ((ClientChannel)var2.next()).getNetworkObject().chatBuffer.add(new RemoteChatMessage(var1, this.isOnServer()));
         }

      }
   }

   public boolean checkPrefixClient(ChatMessage var1) {
      return var1.receiverType != ChatMessage.ChatMessageType.DIRECT && !this.isCheckForPrefixesOnClient() ? true : ((GameClientState)this.getState()).getChannelRouter().checkPrefixClient(var1, this);
   }

   public abstract boolean isPermanent();

   private void addToDefaults(ChatMessage var1) {
      if (!this.isOnServer()) {
         if (this.isSticky()) {
            ChatMessage var2;
            (var2 = new ChatMessage(var1)).setChannel(this);
            this.visibleChatLog.add(0, var2);
         } else {
            ((GameClientState)this.getState()).getChannelRouter().addToDefaultChannelVisibleChatLogOnClient(var1);
         }

         ((GameClientState)this.getState()).getChannelRouter().addToDefaultChannelChatLogOnClient(var1);
      }

   }

   public void receive(ChatMessage var1) {
      this.messageLog.add(var1);
      this.addToDefaults(var1);
      this.setChanged();
      this.notifyObservers();
   }

   protected void onClientChannelRemoveServer(ClientChannel var1) {
      assert this.isOnServer();

      if (this.members.remove(var1.getPlayer())) {
         System.err.println("[SERVER][ClientChannel] successfully removed player on logoff " + var1.getPlayer() + " from " + this.getUniqueChannelName());
         var1.getPlayer().getPlayerChannelManager().removedFromChat(this);
         this.sendLeftUpdateToClients(var1);
      } else {
         assert false : "Not send note to clients! " + this.getUniqueChannelName() + ", Members didnt contain: " + var1.getPlayer();
      }

      this.setChanged();
      this.notifyObservers();
   }

   public void requestChannelDeleteClient() {
      ChatChannelModification var1 = new ChatChannelModification(ChatChannelModification.ChannelModType.DELETE, this, new int[0]);
      this.sendToServer(var1);
   }

   public void sendJoinRequestToServer(ClientChannel var1, String var2) {
      this.setClientCurrentPassword(var2);
      ChatChannelModification var3;
      (var3 = new ChatChannelModification(ChatChannelModification.ChannelModType.JOINED, this, new int[]{var1.getPlayerId()})).joinPw = var2;
      var1.getNetworkObject().chatChannelBuffer.add(new RemoteChatChannel(var3, this.isOnServer()));
      System.err.println("[CLIENT] sending join request to server: " + this.getUniqueChannelName());
      var1.getPlayer().getPlayerChannelManager().onJoinChannelAttempt(this, var2);
   }

   public void sendLeaveRequestToServer(ClientChannel var1, String var2) {
      ChatChannelModification var3 = new ChatChannelModification(ChatChannelModification.ChannelModType.LEFT, this, new int[]{var1.getPlayerId()});
      var1.getNetworkObject().chatChannelBuffer.add(new RemoteChatChannel(var3, this.isOnServer()));
      System.err.println("[CLIENT] sending leave request to server: " + this.getUniqueChannelName());
   }

   public void sendModRequestToServer(ClientChannel var1, String var2, boolean var3) {
      ChatChannelModification var4;
      (var4 = new ChatChannelModification(var3 ? ChatChannelModification.ChannelModType.MOD_ADDED : ChatChannelModification.ChannelModType.MOD_REMOVED, this, new int[]{var1.getPlayerId()})).mods = new String[]{var2};
      var1.getNetworkObject().chatChannelBuffer.add(new RemoteChatChannel(var4, this.isOnServer()));
      System.err.println("[CLIENT] sending leave request to server: " + this.getUniqueChannelName());
   }

   public boolean hasChannelBanList() {
      return false;
   }

   public void sendBannedRequestToServer(ClientChannel var1, String var2, boolean var3) {
      ChatChannelModification var4;
      (var4 = new ChatChannelModification(var3 ? ChatChannelModification.ChannelModType.BANNED : ChatChannelModification.ChannelModType.UNBANNED, this, new int[]{var1.getPlayerId()})).banned = new String[]{var2};
      var1.getNetworkObject().chatChannelBuffer.add(new RemoteChatChannel(var4, this.isOnServer()));
      System.err.println("[CLIENT] sending leave request to server: " + this.getUniqueChannelName());
   }

   public boolean hasChannelModList() {
      return false;
   }

   private void sendLeftUpdateToClients(ClientChannel var1) {
      assert this.isOnServer();

      Iterator var2 = ((GameServerState)this.getState()).getPlayerStatesByName().values().iterator();

      while(var2.hasNext()) {
         PlayerState var3;
         if ((var3 = (PlayerState)var2.next()).getClientChannel() != null) {
            ClientChannel var4 = var3.getClientChannel();
            if (var1 != var4) {
               var4.getNetworkObject().chatChannelBuffer.add(new RemoteChatChannel(new ChatChannelModification(ChatChannelModification.ChannelModType.LEFT, this, new int[]{var1.getPlayerId()}), this.isOnServer()));
            }
         }
      }

      var1.getNetworkObject().chatChannelBuffer.add(new RemoteChatChannel(new ChatChannelModification(ChatChannelModification.ChannelModType.LEFT, this, new int[]{var1.getPlayerId()}), this.isOnServer()));
   }

   public boolean hasPossiblePassword() {
      return false;
   }

   private void sendDeleteUpdateToClients(ClientChannel var1) {
      assert this.isOnServer();

      var1.getNetworkObject().chatChannelBuffer.add(new RemoteChatChannel(new ChatChannelModification(ChatChannelModification.ChannelModType.DELETE, this, new int[0]), this.isOnServer()));
   }

   protected boolean checkPassword(ChatChannelModification var1) {
      return true;
   }

   public boolean hasPassword() {
      return false;
   }

   private void sendAddUpdateToClients(ClientChannel var1) {
      assert this.isOnServer();

      Collection var2 = this.getClientChannelsInChannel();
      Iterator var3 = ((GameServerState)this.getState()).getPlayerStatesByName().values().iterator();

      while(var3.hasNext()) {
         PlayerState var4;
         if ((var4 = (PlayerState)var3.next()).getClientChannel() != null) {
            ClientChannel var5 = var4.getClientChannel();
            if (var1 != var5) {
               var5.getNetworkObject().chatChannelBuffer.add(new RemoteChatChannel(new ChatChannelModification(ChatChannelModification.ChannelModType.JOINED, this, new int[]{var1.getPlayerId()}), this.isOnServer()));
            }
         }
      }

      int var7 = 0;
      int[] var8 = new int[var2.size()];

      for(Iterator var9 = var2.iterator(); var9.hasNext(); ++var7) {
         ClientChannel var6 = (ClientChannel)var9.next();
         var8[var7] = var6.getPlayerId();
      }

      ChatChannelModification var10;
      (var10 = new ChatChannelModification(ChatChannelModification.ChannelModType.CREATE, this, var8)).mods = this.getModerators();
      var10.banned = this.getBanned();
      var10.muted = this.getMuted();
      var1.getNetworkObject().chatChannelBuffer.add(new RemoteChatChannel(var10, this.isOnServer()));
   }

   protected String[] getModerators() {
      return null;
   }

   public void sendCreateUpdateToClient(ClientChannel var1) {
      assert this.isOnServer();

      Collection var2 = this.getClientChannelsInChannel();
      Iterator var3 = ((GameServerState)this.getState()).getPlayerStatesByName().values().iterator();

      while(var3.hasNext()) {
         PlayerState var4;
         if ((var4 = (PlayerState)var3.next()).getClientChannel() != null) {
            ClientChannel var5 = var4.getClientChannel();
            if (var1 != var5) {
               var5.getNetworkObject().chatChannelBuffer.add(new RemoteChatChannel(new ChatChannelModification(ChatChannelModification.ChannelModType.JOINED, this, new int[]{var1.getPlayerId()}), this.isOnServer()));
            }
         }
      }

      int var7 = 0;
      int[] var8 = new int[var2.size()];

      for(Iterator var9 = var2.iterator(); var9.hasNext(); ++var7) {
         ClientChannel var6 = (ClientChannel)var9.next();
         var8[var7] = var6.getPlayerId();
      }

      ChatChannelModification var10;
      (var10 = new ChatChannelModification(ChatChannelModification.ChannelModType.CREATE, this, var8)).mods = this.getModerators();
      var10.banned = this.getBanned();
      var10.muted = this.getMuted();
      var1.getNetworkObject().chatChannelBuffer.add(new RemoteChatChannel(var10, this.isOnServer()));
   }

   public String[] getBanned() {
      return null;
   }

   public void sendCreateWithoutJoinUpdateToClient(ClientChannel var1) {
      assert this.isOnServer();

      Collection var2 = this.getClientChannelsInChannel();
      int var3 = 0;
      int[] var4 = new int[var2.size()];

      for(Iterator var6 = var2.iterator(); var6.hasNext(); ++var3) {
         ClientChannel var5 = (ClientChannel)var6.next();
         var4[var3] = var5.getPlayerId();
      }

      ChatChannelModification var7;
      (var7 = new ChatChannelModification(ChatChannelModification.ChannelModType.CREATE, this, var4)).mods = this.getModerators();
      var7.banned = this.getBanned();
      var7.muted = this.getMuted();
      var1.getNetworkObject().chatChannelBuffer.add(new RemoteChatChannel(var7, this.isOnServer()));
   }

   protected void onClientChannelAddServer(ClientChannel var1) {
      assert this.isOnServer();

      var1.getPlayer().getPlayerChannelManager().addedToChat(this);
      boolean var2 = false;

      assert this.getClientChannelsInChannel().contains(var1);

      if (!this.members.contains(var1.getPlayer())) {
         var2 = this.members.add(var1.getPlayer());
      }

      if (var2) {
         this.sendAddUpdateToClients(var1);
      }

      this.setChanged();
      this.notifyObservers();
   }

   public abstract void onFactionChangedServer(ClientChannel var1);

   public abstract void onLoginServer(ClientChannel var1);

   public final void onLogoff(PlayerState var1) {
      if (var1.getClientChannel() != null && this.removeFromClientChannels(var1.getClientChannel())) {
         System.err.println("[SERVER][FactionChannel] removed logged out player from " + this.getUniqueChannelName() + ": " + var1);
         this.onClientChannelRemoveServer(var1.getClientChannel());
      }

   }

   public abstract void update();

   public void updateVisibleChat(Timer var1) {
      ChannelRouter.updateVisibleChat(var1, this.getVisibleChatLog());
   }

   public abstract boolean isAlive();

   public abstract ChatMessage process(ChatMessage var1);

   public StateInterface getState() {
      return this.state;
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public ObjectArrayList getMessageLog() {
      return this.messageLog;
   }

   public abstract String getUniqueChannelName();

   public int getId() {
      return this.id;
   }

   public List getChatLog() {
      return this.getMessageLog();
   }

   public ObjectArrayList getVisibleChatLog() {
      return this.visibleChatLog;
   }

   public boolean localChatOnClient(String var1) {
      assert !this.isOnServer();

      ChatMessage var2;
      (var2 = new ChatMessage()).sender = this.getName();
      var2.receiver = ((GameClientState)this.getState()).getPlayerName();
      var2.text = var1;
      boolean var3;
      if (var3 = this.checkPrefixClient(var2)) {
         this.getChatLog().add(var2);
         this.getVisibleChatLog().add(new ChatMessage(var2));
         this.addToDefaults(var2);
      }

      return var3;
   }

   public void chat(String var1) {
      ChatMessage var2;
      (var2 = new ChatMessage()).text = var1;
      var2.sender = ((GameClientState)this.getState()).getPlayerName();
      var2.receiver = this.getUniqueChannelName();
      var2.receiverType = ChatMessage.ChatMessageType.CHANNEL;
      this.send(var2);
   }

   public Observable getObservableMemberList() {
      return this;
   }

   public Collection getMemberPlayerStates() {
      return this.members;
   }

   public void onWindowDeactivate() {
   }

   public boolean isSticky() {
      return this.sticky;
   }

   public void setSticky(boolean var1) {
      this.sticky = var1;
   }

   public boolean isFullSticky() {
      return this.stickyFull;
   }

   public void setFullSticky(boolean var1) {
      this.stickyFull = var1;
   }

   public void leave(PlayerState var1) {
      if (this.getMemberPlayerStates().contains(var1)) {
         this.sendLeaveRequestToServer(var1.getClientChannel(), "");
      }

   }

   public abstract boolean canLeave();

   public int hashCode() {
      return this.id;
   }

   public boolean equals(Object var1) {
      return this.id == ((ChatChannel)var1).id;
   }

   public abstract boolean isPublic();

   public abstract ChannelRouter.ChannelType getType();

   public void handleMod(ChatChannelModification var1) {
      if (this.isOnServer()) {
         switch(var1.type) {
         case JOINED:
            assert false;
            break;
         case CREATE:
            assert false;
            break;
         case INVITED:
            return;
         case LEFT:
            assert false;
            break;
         case KICKED:
            this.handleModModeratorKickServer(var1);
            return;
         case BANNED:
            this.handleModBannedAddServer(var1);
            return;
         case UNBANNED:
            this.handleModBannedRemoveServer(var1);
            return;
         case MUTED:
            this.handleModMutedAddServer(var1);
            return;
         case UNMUTED:
            this.handleModMutedRemoveServer(var1);
            return;
         case IGNORED:
            this.handleModIgnoredAddServer(var1);
            return;
         case UNIGNORED:
            this.handleModIgnoredRemoveServer(var1);
            return;
         case UPDATE:
            return;
         case MOD_ADDED:
            this.handleModModeratorAddServer(var1);
            return;
         case MOD_REMOVED:
            this.handleModModeratorRemoveServer(var1);
            return;
         case REMOVED_ON_NOT_ALIVE:
            assert false;
            break;
         case PASSWD_CHANGE:
            this.handleModPasswordChangeServer(var1);
            return;
         default:
            return;
         }
      } else {
         label81:
         switch(var1.type) {
         case JOINED:
            this.handleModJoinClient(var1);
            break;
         case CREATE:
            this.handleModJoinClient(var1);

            assert this.getType() != ChannelRouter.ChannelType.PUBLIC || var1.createChannelType == ChannelRouter.ChannelType.PUBLIC : var1.createChannelType;

            assert this.getType() != ChannelRouter.ChannelType.PUBLIC || var1.mods != null : var1.createChannelType.name();

            this.addToModerator(var1.mods);
            this.addToBanned(var1.banned);
         case INVITED:
         case IGNORED:
         case UNIGNORED:
         case UPDATE:
         case REMOVED_ON_NOT_ALIVE:
         default:
            break;
         case LEFT:
         case KICKED:
            int[] var5;
            int var2 = (var5 = var1.user).length;
            int var3 = 0;

            while(true) {
               if (var3 >= var2) {
                  break label81;
               }

               int var4 = var5[var3];
               Sendable var6;
               if ((var6 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var4)) instanceof PlayerState) {
                  if (!this.members.remove(var6)) {
                     System.err.println("[CLIENT] WARNING: member in channel: " + this.getUniqueChannelName() + " could not be removed: " + var6 + ": " + this.members);
                  }

                  if (!this.isAvailableWithoutMyself()) {
                     ((PlayerState)var6).getPlayerChannelManager().removedFromChat(this);
                  }
               }

               ++var3;
            }
         case BANNED:
            this.handleModBannedAddClient(var1);
            break;
         case UNBANNED:
            this.handleModBannedRemoveClient(var1);
            break;
         case MUTED:
            this.handleModMutedAddClient(var1);
            break;
         case UNMUTED:
            this.handleModMutedRemoveClient(var1);
            break;
         case MOD_ADDED:
            this.handleModModeratorAddClient(var1);
            break;
         case MOD_REMOVED:
            this.handleModModeratorRemoveClient(var1);
            break;
         case PASSWD_CHANGE:
            this.setNewPassword(var1.changePasswd);
         }

         this.setChanged();
         this.notifyObservers();
      }

   }

   private void handleModJoinClient(ChatChannelModification var1) {
      int[] var5;
      int var2 = (var5 = var1.user).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         int var4 = var5[var3];
         Sendable var6;
         if ((var6 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var4)) instanceof PlayerState) {
            if (!this.members.contains(var6)) {
               this.members.add((PlayerState)var6);
            } else {
               System.err.println("[CLIENT] WARNING: member in channel: " + this.getUniqueChannelName() + " already existed");
            }
         }
      }

   }

   private void handleModModeratorAddClient(ChatChannelModification var1) {
      this.addToModerator(var1.mods);
   }

   private void handleModModeratorRemoveClient(ChatChannelModification var1) {
      this.removeFromModerator(var1.mods);
   }

   private void handleModBannedAddClient(ChatChannelModification var1) {
      this.addToBanned(var1.banned);
   }

   private void handleModBannedRemoveClient(ChatChannelModification var1) {
      this.removeFromBanned(var1.banned);
   }

   private void handleModMutedAddClient(ChatChannelModification var1) {
      this.addToMuted(var1.muted);
   }

   private void handleModMutedRemoveClient(ChatChannelModification var1) {
      this.removeFromMuted(var1.muted);
   }

   private void handleModModeratorKickServer(ChatChannelModification var1) {
      try {
         PlayerState var2 = ((GameServerState)this.getState()).getPlayerFromStateId(var1.sender);
         if (!this.isModerator(var2) && !((GameServerState)this.getState()).isAdmin(var2.getName())) {
            System.err.println("[SERVER][ChatChannel] not kick from request by client id " + var1.sender + "; player is not a moderator in this channel!");
         } else {
            Sendable var3;
            if ((var3 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var1.user[0])) instanceof PlayerState) {
               ((PlayerState)var3).sendServerMessagePlayerError(new Object[]{187, this.getName()});
               System.err.println("[SERVER][ChatChannel] " + var2 + " kicked " + var3 + " on channel " + this.getName() + " (" + this.getUniqueChannelName() + ")");
               this.log("[SERVER][ChatChannel] " + var2 + " kicked " + var3 + " on channel " + this.getName() + " (" + this.getUniqueChannelName() + ")");
               this.leaveOnServerAndSend(((PlayerState)var3).getClientChannel());
            } else {
               assert false;

            }
         }
      } catch (PlayerNotFountException var4) {
         System.err.println("[SERVER][ChatChannel] not kick from request by client id " + var1.sender + "; no player found!");
      }
   }

   private void handleModModeratorAddServer(ChatChannelModification var1) {
      try {
         PlayerState var2 = ((GameServerState)this.getState()).getPlayerFromStateId(var1.sender);
         if (!this.isModerator(var2) && !((GameServerState)this.getState()).isAdmin(var2.getName())) {
            System.err.println("[SERVER][ChatChannel] not adding mod from request by client id " + var1.sender + "; player is not a moderator in this channel!");
         } else {
            System.err.println("[SERVER][ChatChannel] " + var2 + " added mods: " + Arrays.toString(var1.mods) + " on channel " + this.getName() + " (" + this.getUniqueChannelName() + ")");
            this.log("[SERVER][ChatChannel] " + var2 + " added mods: " + Arrays.toString(var1.mods) + " on channel " + this.getName() + " (" + this.getUniqueChannelName() + ")");
            this.addToModerator(var1.mods);
         }
      } catch (PlayerNotFountException var3) {
         System.err.println("[SERVER][ChatChannel] not adding mod from request by client id " + var1.sender + "; no player found!");
      }

      this.sendToAllClients(var1);
   }

   private void handleModModeratorRemoveServer(ChatChannelModification var1) {
      try {
         PlayerState var2 = ((GameServerState)this.getState()).getPlayerFromStateId(var1.sender);
         if (!this.isModerator(var2) && !((GameServerState)this.getState()).isAdmin(var2.getName())) {
            System.err.println("[SERVER][ChatChannel] not removing mod from request by client id " + var1.sender + "; player is not a moderator in this channel!");
         } else {
            this.removeFromModerator(var1.mods);
            System.err.println("[SERVER][ChatChannel] " + var2 + " removed mods: " + Arrays.toString(var1.mods) + " on channel " + this.getName() + " (" + this.getUniqueChannelName() + ")");
            this.log("[SERVER][ChatChannel] " + var2 + " removed mods: " + Arrays.toString(var1.mods) + " on channel " + this.getName() + " (" + this.getUniqueChannelName() + ")");
         }
      } catch (PlayerNotFountException var3) {
         System.err.println("[SERVER][ChatChannel] not removing mod from request by client id " + var1.sender + "; no player found!");
      }

      this.sendToAllClients(var1);
   }

   private void handleModPasswordChangeServer(ChatChannelModification var1) {
      try {
         PlayerState var2 = ((GameServerState)this.getState()).getPlayerFromStateId(var1.sender);
         if (!this.isModerator(var2) && !((GameServerState)this.getState()).isAdmin(var2.getName())) {
            System.err.println("[SERVER][ChatChannel] not set password from request by client id " + var1.sender + "; player is not a moderator in this channel!");
         } else {
            this.setNewPassword(var1.changePasswd);
            var1.changePasswd = var1.changePasswd.length() > 0 ? "#" : "";
            System.err.println("[SERVER][ChatChannel] " + var2 + " changed password on channel " + this.getName() + " (" + this.getUniqueChannelName() + ")");
            this.log("[SERVER][ChatChannel] " + var2 + " changed password on channel " + this.getName() + " (" + this.getUniqueChannelName() + ")");
         }
      } catch (PlayerNotFountException var3) {
         System.err.println("[SERVER][ChatChannel] not set password from request by client id " + var1.sender + "; no player found!");
      }

      this.sendToAllClients(var1);
   }

   protected void setNewPassword(String var1) {
   }

   private void handleModBannedRemoveServer(ChatChannelModification var1) {
      try {
         PlayerState var2 = ((GameServerState)this.getState()).getPlayerFromStateId(var1.sender);
         if (!this.isModerator(var2) && !((GameServerState)this.getState()).isAdmin(var2.getName())) {
            System.err.println("[SERVER][ChatChannel] not removing banned from request by client id " + var1.sender + "; player is not a moderator in this channel!");
         } else {
            this.removeFromBanned(var1.banned);
            System.err.println("[SERVER][ChatChannel] " + var2 + " removed bans: " + Arrays.toString(var1.banned) + " on channel " + this.getName() + " (" + this.getUniqueChannelName() + ")");
            this.log("[SERVER][ChatChannel] " + var2 + " removed bans: " + Arrays.toString(var1.banned) + " on channel " + this.getName() + " (" + this.getUniqueChannelName() + ")");
         }
      } catch (PlayerNotFountException var3) {
         System.err.println("[SERVER][ChatChannel] not removing banned from request by client id " + var1.sender + "; no player found!");
      }

      this.sendToAllClients(var1);
   }

   private void handleModBannedAddServer(ChatChannelModification var1) {
      try {
         PlayerState var2 = ((GameServerState)this.getState()).getPlayerFromStateId(var1.sender);
         if (!this.isModerator(var2) && !((GameServerState)this.getState()).isAdmin(var2.getName())) {
            System.err.println("[SERVER][ChatChannel] not adding banned from request by client id " + var1.sender + "; player is not a moderator in this channel!");
         } else {
            this.addToBanned(var1.banned);
            System.err.println("[SERVER][ChatChannel] " + var2 + " added bans: " + Arrays.toString(var1.banned) + " on channel " + this.getName() + " (" + this.getUniqueChannelName() + ")");
            this.log("[SERVER][ChatChannel] " + var2 + " added bans: " + Arrays.toString(var1.banned) + " on channel " + this.getName() + " (" + this.getUniqueChannelName() + ")");
         }
      } catch (PlayerNotFountException var3) {
         System.err.println("[SERVER][ChatChannel] not adding banned from request by client id " + var1.sender + "; no player found!");
      }

      this.sendToAllClients(var1);
   }

   private void handleModMutedRemoveServer(ChatChannelModification var1) {
      try {
         PlayerState var2 = ((GameServerState)this.getState()).getPlayerFromStateId(var1.sender);
         if (!this.isModerator(var2) && !((GameServerState)this.getState()).isAdmin(var2.getName())) {
            System.err.println("[SERVER][ChatChannel] not removing muted from request by client id " + var1.sender + "; player is not a moderator in this channel!");
         } else {
            this.removeFromMuted(var1.muted);
            System.err.println("[SERVER][ChatChannel] " + var2 + " removed mutes: " + Arrays.toString(var1.muted) + " on channel " + this.getName() + " (" + this.getUniqueChannelName() + ")");
            this.log("[SERVER][ChatChannel] " + var2 + " removed mutes: " + Arrays.toString(var1.muted) + " on channel " + this.getName() + " (" + this.getUniqueChannelName() + ")");
         }
      } catch (PlayerNotFountException var3) {
         System.err.println("[SERVER][ChatChannel] not removing muted from request by client id " + var1.sender + "; no player found!");
      }

      this.sendToAllClients(var1);
   }

   private void handleModMutedAddServer(ChatChannelModification var1) {
      try {
         PlayerState var2 = ((GameServerState)this.getState()).getPlayerFromStateId(var1.sender);
         if (!this.isModerator(var2) && !((GameServerState)this.getState()).isAdmin(var2.getName())) {
            System.err.println("[SERVER][ChatChannel] not adding muted from request by client id " + var1.sender + "; player is not a moderator in this channel!");
         } else {
            this.addToMuted(var1.muted);
            System.err.println("[SERVER][ChatChannel] " + var2 + " added mutes: " + Arrays.toString(var1.muted) + " on channel " + this.getName() + " (" + this.getUniqueChannelName() + ")");
            this.log("[SERVER][ChatChannel] " + var2 + " added mutes: " + Arrays.toString(var1.muted) + " on channel " + this.getName() + " (" + this.getUniqueChannelName() + ")");
         }
      } catch (PlayerNotFountException var3) {
         System.err.println("[SERVER][ChatChannel] not adding muted from request by client id " + var1.sender + "; no player found!");
      }

      this.sendToAllClients(var1);
   }

   private void handleModIgnoredRemoveServer(ChatChannelModification var1) {
      try {
         PlayerState var2 = ((GameServerState)this.getState()).getPlayerFromStateId(var1.sender);
         this.removeFromIgnored(var2, var1.ignored);
         System.err.println("[SERVER][ChatChannel] " + var2 + " removed ignores: " + Arrays.toString(var1.ignored));
         this.log("[SERVER][ChatChannel] " + var2 + " removed ignores: " + Arrays.toString(var1.ignored));
      } catch (PlayerNotFountException var3) {
         System.err.println("[SERVER][ChatChannel] not removing ignored from request by client id " + var1.sender + "; no player found!");
      }

      this.sendToAllClients(var1);
   }

   private void handleModIgnoredAddServer(ChatChannelModification var1) {
      try {
         PlayerState var2 = ((GameServerState)this.getState()).getPlayerFromStateId(var1.sender);
         this.addToIgnored(var2, var1.ignored);
         System.err.println("[SERVER][ChatChannel] " + var2 + " added ignores: " + Arrays.toString(var1.ignored));
         this.log("[SERVER][ChatChannel] " + var2 + " added ignores: " + Arrays.toString(var1.ignored));
      } catch (PlayerNotFountException var3) {
         System.err.println("[SERVER][ChatChannel] not adding ignored from request by client id " + var1.sender + "; no player found!");
      }

      this.sendToAllClients(var1);
   }

   private void sendToAllClients(ChatChannelModification var1) {
      Iterator var2 = ((GameServerState)this.getState()).getPlayerStatesByName().values().iterator();

      while(var2.hasNext()) {
         PlayerState var3;
         if ((var3 = (PlayerState)var2.next()).getClientChannel() != null) {
            var3.getClientChannel().getNetworkObject().chatChannelBuffer.add(new RemoteChatChannel(var1, this.isOnServer()));
         }
      }

   }

   private void log(String var1) {
      PrintWriter var2 = null;
      boolean var5 = false;

      label60: {
         try {
            var5 = true;
            (var2 = new PrintWriter(new BufferedWriter(new FileWriter(ChannelRouter.CHAT_LOG_DIR + this.getUniqueChannelName() + ".txt", true)))).println(ChannelRouter.logDateFormatter.format(new Date(System.currentTimeMillis())) + " ** " + var1);
            var5 = false;
            break label60;
         } catch (IOException var6) {
            var1 = null;
            var6.printStackTrace();
            var5 = false;
         } finally {
            if (var5) {
               if (var2 != null) {
                  var2.close();
               }

            }
         }

         if (var2 != null) {
            var2.close();
            return;
         }

         return;
      }

      var2.close();
   }

   protected void removeFromModerator(String... var1) {
   }

   protected void addToModerator(String... var1) {
   }

   protected void removeFromBanned(String... var1) {
   }

   protected void addToBanned(String... var1) {
   }

   protected void removeFromMuted(String... var1) {
   }

   protected void addToMuted(String... var1) {
   }

   protected void removeFromIgnored(PlayerState var1, String... var2) {
      int var3 = (var2 = var2).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         String var5 = var2[var4];
         var1.removeIgnore(var5);
      }

   }

   protected void addToIgnored(PlayerState var1, String... var2) {
      int var3 = (var2 = var2).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         String var5 = var2[var4];
         var1.addIgnore(var5);
      }

   }

   protected boolean isAvailableWithoutMyself() {
      return true;
   }

   public void joinOnServerAndSend(ClientChannel var1) {
      if (!this.members.contains(var1.getPlayer())) {
         this.addToClientChannels(var1);
         this.members.add(var1.getPlayer());
         this.sendAddUpdateToClients(var1);
      }

   }

   public void leaveOnServerAndSend(ClientChannel var1) {
      this.removeFromClientChannels(var1);
      this.members.remove(var1.getPlayer());
      this.sendLeftUpdateToClients(var1);
   }

   public void deleteChannelOnServerAndSend() {
      Iterator var1 = ((GameServerState)this.getState()).getPlayerStatesByName().values().iterator();

      while(var1.hasNext()) {
         ClientChannel var2;
         if ((var2 = ((PlayerState)var1.next()).getClientChannel()) != null) {
            this.removeFromClientChannels(var2);
            this.members.remove(var2.getPlayer());
            var2.getPlayer().getPlayerChannelManager().removedFromChat(this);
            this.sendDeleteUpdateToClients(var2);
         }
      }

   }

   protected abstract boolean addToClientChannels(ClientChannel var1);

   protected abstract boolean removeFromClientChannels(ClientChannel var1);

   public boolean isAutoJoinFor(ClientChannel var1) {
      return false;
   }

   public boolean isCheckForPrefixesOnClient() {
      return true;
   }

   public Tag toServerTag() {
      System.err.println("[CLIENT] SAVING SERVER CHANNEL " + this.getUniqueChannelName() + " AS: " + this.isClientOpen());

      assert this instanceof PublicChannel;

      String[] var1 = this.getBanned();
      String[] var2 = this.getModerators();
      String[] var3 = this.getMuted();
      Tag var5 = Tag.listToTagStruct((Object[])var2, Tag.Type.STRING, (String)null);
      Tag var4 = Tag.listToTagStruct((Object[])var1, Tag.Type.STRING, (String)null);
      Tag var6 = Tag.listToTagStruct((Object[])var3, Tag.Type.STRING, (String)null);
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)2), new Tag(Tag.Type.STRING, (String)null, this.getUniqueChannelName()), var5, var4, var6, new Tag(Tag.Type.STRING, (String)null, this.getPassword()), new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.isPermanent() ? 1 : 0))), new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.isPublic() ? 1 : 0))), FinishTag.INST});
   }

   public Tag toClientTag() {
      System.err.println("[CLIENT] SAVING CLIENT CHANNEL " + this.getUniqueChannelName() + " Open: " + this.isClientOpen() + ";");
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)1), new Tag(Tag.Type.STRING, (String)null, this.getUniqueChannelName()), new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.isSticky() ? 1 : 0))), new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.isFullSticky() ? 1 : 0))), new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.isClientOpen() ? 1 : 0))), new Tag(Tag.Type.STRING, (String)null, this.getClientCurrentPassword()), FinishTag.INST});
   }

   private String getClientCurrentPassword() {
      return this.clientCurrentPassword;
   }

   public void setChatBar(GUIActivatableTextBar var1) {
      this.chatBar = var1;
   }

   public void setClientCurrentPassword(String var1) {
      this.clientCurrentPassword = var1;
   }

   public GUIActivatableTextBar getChatBar() {
      return this.chatBar;
   }

   public void handleClientLoadedChannel(PlayerState var1, LoadedClientChatChannel var2) {
      if (!this.members.contains(var1) && !this.isAutoJoinFor(var1.getClientChannel())) {
         this.sendJoinRequestToServer(var1.getClientChannel(), var2.password);
      }

      var2.joinRequestDone = true;
   }

   private void sendToServer(ChatChannelModification var1) {
      ((GameClientState)this.getState()).getPlayer().getClientChannel().getNetworkObject().chatChannelBuffer.add(new RemoteChatChannel(var1, this.isOnServer()));
   }

   public String getPassword() {
      return "";
   }

   public Vector4f getColor() {
      return this.color;
   }

   public boolean isClientOpen() {
      return this.clientOpen;
   }

   public void setClientOpen(boolean var1) {
      this.clientOpen = var1;
   }

   public void requestModUnmodOnClient(String var1, boolean var2) {
      ChatChannelModification var3;
      (var3 = new ChatChannelModification(var2 ? ChatChannelModification.ChannelModType.MOD_ADDED : ChatChannelModification.ChannelModType.MOD_REMOVED, this, new int[0])).mods = new String[]{var1.toLowerCase(Locale.ENGLISH)};
      this.sendToServer(var3);
   }

   public void requestPasswordChangeOnClient(String var1) {
      ChatChannelModification var2;
      (var2 = new ChatChannelModification(ChatChannelModification.ChannelModType.PASSWD_CHANGE, this, new int[0])).changePasswd = var1;
      this.sendToServer(var2);
   }

   public void requestKickOnClient(PlayerState var1) {
      ChatChannelModification var2 = new ChatChannelModification(ChatChannelModification.ChannelModType.KICKED, this, new int[]{var1.getId()});
      this.sendToServer(var2);
   }

   public void requestBanUnbanOnClient(String var1, boolean var2) {
      if (!((GameClientState)this.state).getPlayerName().toLowerCase(Locale.ENGLISH).equals(var1.toLowerCase(Locale.ENGLISH))) {
         ChatChannelModification var3;
         (var3 = new ChatChannelModification(var2 ? ChatChannelModification.ChannelModType.BANNED : ChatChannelModification.ChannelModType.UNBANNED, this, new int[0])).banned = new String[]{var1.toLowerCase(Locale.ENGLISH)};
         this.sendToServer(var3);
      }

   }

   public void requestMuteUnmuteOnClient(String var1, boolean var2) {
      if (!((GameClientState)this.state).getPlayerName().toLowerCase(Locale.ENGLISH).equals(var1.toLowerCase(Locale.ENGLISH))) {
         ChatChannelModification var3;
         (var3 = new ChatChannelModification(var2 ? ChatChannelModification.ChannelModType.MUTED : ChatChannelModification.ChannelModType.UNMUTED, this, new int[0])).muted = new String[]{var1.toLowerCase(Locale.ENGLISH)};
         this.sendToServer(var3);
      }

   }

   public void requestIgnoreUnignoreOnClient(String var1, boolean var2) {
      if (!((GameClientState)this.state).getPlayerName().toLowerCase(Locale.ENGLISH).equals(var1.toLowerCase(Locale.ENGLISH))) {
         ChatChannelModification var4;
         (var4 = new ChatChannelModification(var2 ? ChatChannelModification.ChannelModType.IGNORED : ChatChannelModification.ChannelModType.UNIGNORED, this, new int[0])).ignored = new String[]{var1.toLowerCase(Locale.ENGLISH)};
         PlayerState var3;
         if ((var3 = ((GameClientState)this.state).getPlayer()).isIgnored(var1)) {
            var3.removeIgnore(var1);
         } else {
            var3.addIgnore(var1);
         }

         this.sendToServer(var4);
      }

   }
}
