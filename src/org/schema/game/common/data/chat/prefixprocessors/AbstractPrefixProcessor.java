package org.schema.game.common.data.chat.prefixprocessors;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.chat.ChatChannel;
import org.schema.game.network.objects.ChatMessage;

public abstract class AbstractPrefixProcessor implements Comparable {
   private final String prefixCommon;

   public AbstractPrefixProcessor(String var1) {
      this.prefixCommon = var1;
   }

   public void localResponse(String var1, ChatChannel var2) {
      var2.localChatOnClient(var1);
   }

   public String getPrefixCommon() {
      return this.prefixCommon;
   }

   public void process(ChatMessage var1, ChatChannel var2, GameClientState var3) {
      StringBuffer var4;
      (var4 = new StringBuffer(var1.text)).delete(0, this.prefixCommon.length());
      int var5 = var4.indexOf(" ");
      String var6 = var4.substring(0, var5 >= 0 ? var5 + 1 : var4.length()).trim();
      String var7;
      if (var5 < 0) {
         var7 = "";
      } else {
         var7 = var4.substring(var5).trim();
      }

      this.process(var1, var6, var7, var2, var3);
   }

   protected abstract void process(ChatMessage var1, String var2, String var3, ChatChannel var4, GameClientState var5);

   public abstract boolean sendChatMessageAfterProcessing();

   public boolean fits(ChatMessage var1) {
      return var1.text.startsWith(this.prefixCommon);
   }

   public int compareTo(AbstractPrefixProcessor var1) {
      return this.prefixCommon.length() - var1.prefixCommon.length();
   }
}
