package org.schema.game.common.data.chat.prefixprocessors;

import java.util.Locale;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.chat.ChatChannel;
import org.schema.game.network.objects.ChatMessage;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;

public class PrefixProcessorEngineSettings extends AbstractPrefixProcessor {
   public PrefixProcessorEngineSettings() {
      super("//");
   }

   protected void process(ChatMessage var1, String var2, String var3, ChatChannel var4, GameClientState var5) {
      try {
         try {
            EngineSettings var10 = (EngineSettings)Enum.valueOf(EngineSettings.class, var2.toUpperCase(Locale.ENGLISH));

            try {
               var10.switchSetting(var3);
               this.localResponse("[COMMAND] \"" + var2 + "\" successful: " + var10.name() + " = " + var10.getCurrentState(), var4);
            } catch (StateParameterNotFoundException var6) {
               throw new IllegalArgumentException("[ERROR] STATE NOT KNOWN: " + var6.getMessage());
            }
         } catch (IllegalArgumentException var7) {
            throw new IllegalArgumentException(GameClientController.findCorrectedCommand(var2));
         }
      } catch (IllegalArgumentException var8) {
         if (!var8.getMessage().startsWith("[ERROR]")) {
            this.localResponse("[ERROR] UNKNOWN COMMAND: " + var2, var4);
         } else {
            this.localResponse(var8.getMessage(), var4);
         }
      } catch (IndexOutOfBoundsException var9) {
         this.localResponse(var9.getMessage(), var4);
      }
   }

   public boolean sendChatMessageAfterProcessing() {
      return false;
   }
}
