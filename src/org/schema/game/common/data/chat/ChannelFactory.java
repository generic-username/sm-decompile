package org.schema.game.common.data.chat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public interface ChannelFactory {
   ChatChannel instantiate(DataInputStream var1) throws IOException;

   void serialize(ChatChannel var1, DataOutputStream var2) throws IOException;
}
