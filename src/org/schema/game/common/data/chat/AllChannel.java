package org.schema.game.common.data.chat;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.network.objects.ChatMessage;
import org.schema.schine.network.StateInterface;

public class AllChannel extends MuteEnabledChatChannel {
   private final ObjectArrayList clientChannels = new ObjectArrayList();

   public AllChannel(StateInterface var1, int var2) {
      super(var1, var2);
   }

   public Collection getClientChannelsInChannel() {
      return this.clientChannels;
   }

   public boolean isPermanent() {
      return true;
   }

   public void onFactionChangedServer(ClientChannel var1) {
   }

   public void onLoginServer(ClientChannel var1) {
      assert this.isOnServer();

      this.clientChannels.add(var1);
      this.onClientChannelAddServer(var1);
   }

   public void update() {
   }

   public boolean isAlive() {
      return true;
   }

   public ChatMessage process(ChatMessage var1) {
      return var1;
   }

   public String getUniqueChannelName() {
      return "all";
   }

   public boolean isPublic() {
      return true;
   }

   public ChannelRouter.ChannelType getType() {
      return ChannelRouter.ChannelType.ALL;
   }

   public boolean canLeave() {
      return false;
   }

   protected boolean addToClientChannels(ClientChannel var1) {
      return !this.clientChannels.contains(var1) ? this.clientChannels.add(var1) : false;
   }

   protected boolean removeFromClientChannels(ClientChannel var1) {
      return this.clientChannels.remove(var1);
   }

   public boolean isAutoJoinFor(ClientChannel var1) {
      return true;
   }

   public String getName() {
      return "General";
   }

   public Object getTitle() {
      return "General Channel";
   }

   public boolean isModerator(PlayerState var1) {
      return false;
   }

   public boolean isBanned(PlayerState var1) {
      return false;
   }
}
