package org.schema.game.common.data.chat;

import java.util.Collection;
import java.util.List;
import java.util.Observable;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollMarkedReadInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;

public interface ChatCallback extends GUIScrollMarkedReadInterface {
   List getChatLog();

   List getVisibleChatLog();

   boolean localChatOnClient(String var1);

   void chat(String var1);

   Observable getObservableMemberList();

   Collection getMemberPlayerStates();

   String getName();

   Object getTitle();

   void onWindowDeactivate();

   boolean isSticky();

   void setSticky(boolean var1);

   boolean isFullSticky();

   void setFullSticky(boolean var1);

   void leave(PlayerState var1);

   boolean canLeave();

   GUIActivatableTextBar getChatBar();

   void setChatBar(GUIActivatableTextBar var1);

   boolean isClientOpen();

   void setClientOpen(boolean var1);

   boolean isModerator(PlayerState var1);

   boolean hasChannelBanList();

   boolean hasChannelMuteList();

   boolean hasChannelModList();

   boolean hasPossiblePassword();

   void requestModUnmodOnClient(String var1, boolean var2);

   void requestPasswordChangeOnClient(String var1);

   void requestKickOnClient(PlayerState var1);

   boolean isBanned(PlayerState var1);

   void requestBanUnbanOnClient(String var1, boolean var2);

   String[] getBanned();

   boolean isMuted(PlayerState var1);

   void requestMuteUnmuteOnClient(String var1, boolean var2);

   String[] getMuted();

   void requestIgnoreUnignoreOnClient(String var1, boolean var2);
}
