package org.schema.game.common.data.chat;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.network.objects.ChatMessage;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.ChatColorPalette;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.client.ClientState;

public class FactionChannel extends MuteEnabledChatChannel {
   private final ObjectArrayList clientChannels = new ObjectArrayList();
   private final int fid;

   public FactionChannel(StateInterface var1, int var2, int var3) {
      super(var1, var2);
      this.fid = var3;
      if (var1 instanceof ClientState) {
         this.color.set(ChatColorPalette.faction);
      }

   }

   public Collection getClientChannelsInChannel() {
      return this.clientChannels;
   }

   public boolean isPermanent() {
      return false;
   }

   public void onFactionChangedServer(ClientChannel var1) {
      if (var1.getPlayer().getFactionId() != this.fid && this.clientChannels.contains(var1)) {
         this.onClientChannelRemoveServer(var1);
         this.clientChannels.remove(var1);
      } else {
         if (var1.getPlayer().getFactionId() == this.fid && !this.clientChannels.contains(var1)) {
            System.err.println("[SERVER][FactionChannel] added player automatically to " + this.getUniqueChannelName() + ": " + var1.getPlayer());
            this.clientChannels.add(var1);
            this.onClientChannelAddServer(var1);
         }

      }
   }

   public void onLoginServer(ClientChannel var1) {
      assert this.isOnServer();

      if (var1.getPlayer().getFactionId() == this.fid && !this.clientChannels.contains(var1)) {
         System.err.println("[SERVER] ADDING CLIENT CHANNEL TO FACTION CHANNEL: " + this.getUniqueChannelName());
         this.clientChannels.add(var1);
         this.onClientChannelAddServer(var1);
      }

   }

   public void update() {
   }

   public boolean isAlive() {
      return this.clientChannels.size() > 0;
   }

   public ChatMessage process(ChatMessage var1) {
      return var1;
   }

   public String getUniqueChannelName() {
      return "Faction" + this.fid;
   }

   public boolean canLeave() {
      return false;
   }

   public boolean isPublic() {
      return false;
   }

   public ChannelRouter.ChannelType getType() {
      return ChannelRouter.ChannelType.FACTION;
   }

   protected boolean isAvailableWithoutMyself() {
      return false;
   }

   protected boolean addToClientChannels(ClientChannel var1) {
      return !this.clientChannels.contains(var1) ? this.clientChannels.add(var1) : false;
   }

   protected boolean removeFromClientChannels(ClientChannel var1) {
      return this.clientChannels.remove(var1);
   }

   public boolean isAutoJoinFor(ClientChannel var1) {
      return true;
   }

   public String getName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_CHAT_FACTIONCHANNEL_0;
   }

   public Object getTitle() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_CHAT_FACTIONCHANNEL_1;
   }

   public boolean isModerator(PlayerState var1) {
      return false;
   }

   public boolean isBanned(PlayerState var1) {
      return false;
   }

   public int getFactionId() {
      return this.fid;
   }
}
