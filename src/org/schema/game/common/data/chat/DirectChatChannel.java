package org.schema.game.common.data.chat;

import java.util.Collection;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.network.objects.ChatMessage;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.ChatColorPalette;
import org.schema.schine.network.StateInterface;

public class DirectChatChannel extends ChatChannel {
   private final PlayerState a;
   private final PlayerState b;

   public DirectChatChannel(StateInterface var1, int var2, PlayerState var3, PlayerState var4) {
      super(var1, var2);
      this.a = var3;
      this.b = var4;
      this.members.add(var3);
      this.members.add(var4);
      this.color.set(ChatColorPalette.whisper);
   }

   public static String getChannelName(PlayerState var0, PlayerState var1) {
      return getChannelName(var0.getName(), var1.getName());
   }

   public static String getChannelName(String var0, String var1) {
      if (var0.hashCode() < var1.hashCode()) {
         var0 = var0 + var1;
      } else {
         var0 = var1 + var0;
      }

      return "##" + var0;
   }

   public Collection getClientChannelsInChannel() {
      assert false : "this channel should exist on client only";

      return null;
   }

   public boolean isPermanent() {
      return false;
   }

   public void onFactionChangedServer(ClientChannel var1) {
   }

   public void onLoginServer(ClientChannel var1) {
   }

   public void update() {
   }

   public boolean isAlive() {
      return true;
   }

   public ChatMessage process(ChatMessage var1) {
      return var1;
   }

   public String getUniqueChannelName() {
      return getChannelName(this.a, this.b);
   }

   public void chat(String var1) {
      ChatMessage var2;
      (var2 = new ChatMessage()).text = var1;
      var2.sender = ((GameClientState)this.getState()).getPlayerName();
      var2.receiver = this.getOther().getName();
      var2.receiverType = ChatMessage.ChatMessageType.DIRECT;
      this.send(var2);
   }

   public void leave(PlayerState var1) {
      var1.getPlayerChannelManager().removeDirectChannel(this);
   }

   public boolean canLeave() {
      return true;
   }

   public boolean isPublic() {
      return false;
   }

   public ChannelRouter.ChannelType getType() {
      return ChannelRouter.ChannelType.DIRECT;
   }

   protected boolean addToClientChannels(ClientChannel var1) {
      return false;
   }

   protected boolean removeFromClientChannels(ClientChannel var1) {
      return false;
   }

   public String getName() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_CHAT_DIRECTCHATCHANNEL_0, this.getOther().getName());
   }

   public Object getTitle() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_CHAT_DIRECTCHATCHANNEL_1, this.getOther().getName());
   }

   public boolean isModerator(PlayerState var1) {
      return false;
   }

   public boolean hasChannelMuteList() {
      return false;
   }

   public boolean isBanned(PlayerState var1) {
      return false;
   }

   public boolean isMuted(PlayerState var1) {
      return false;
   }

   public String[] getMuted() {
      return null;
   }

   private PlayerState getOther() {
      return ((GameClientState)this.getState()).getPlayer() == this.a ? this.b : this.a;
   }
}
