package org.schema.game.common.data.event;

import org.schema.schine.event.Event;
import org.schema.schine.event.EventPayload;
import org.schema.schine.event.EventType;

public class ProjectileEvent extends Event {
   public final EventPayload damage = new EventPayload();
   public final EventPayload position = new EventPayload();
   public final EventPayload direction = new EventPayload();
   public final EventPayload damageType = new EventPayload();
   public final EventPayload[] parameters;

   public ProjectileEvent() {
      this.parameters = new EventPayload[]{this.damage, this.position, this.direction, this.damageType};
   }

   public EventType getType() {
      return EventType.PROJECTILE;
   }

   public EventPayload[] getParameters() {
      return this.parameters;
   }
}
