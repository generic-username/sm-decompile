package org.schema.game.common.data.event;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.damage.DamageDealerType;

public class EventFactory {
   public EventFactory(GameClientState var1) {
   }

   public void fireProjectileFiredEvent(int var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, DamageDealerType var9) {
   }

   public void fireProjectileHitEvent(int var1, int var2, float var3, float var4, float var5, float var6, DamageDealerType var7) {
   }
}
