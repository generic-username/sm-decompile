package org.schema.game.common.data;

import it.unimi.dsi.fastutil.bytes.ByteArrayList;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map.Entry;

public class BlockBulkSerialization {
   private static ObjectArrayFIFOQueue senderPoolReadyToSend = new ObjectArrayFIFOQueue();
   private static ObjectArrayFIFOQueue senderPool = new ObjectArrayFIFOQueue();
   private static ObjectArrayFIFOQueue receiverPool = new ObjectArrayFIFOQueue();
   public Long2ObjectOpenHashMap buffer;
   public int sentToCount;

   public static void freeBufferClient(ByteArrayList var0) {
      var0.clear();
      synchronized(receiverPool) {
         receiverPool.enqueue(var0);
      }
   }

   public static ByteArrayList getBufferClient() {
      synchronized(receiverPool) {
         if (receiverPool.size() > 0) {
            return (ByteArrayList)receiverPool.dequeue();
         }
      }

      return new ByteArrayList();
   }

   public static void freeBufferServer(ByteArrayList var0) {
      var0.clear();
      synchronized(senderPool) {
         senderPool.enqueue(var0);
      }
   }

   public static ByteArrayList getBufferServer() {
      synchronized(senderPoolReadyToSend) {
         ByteArrayList var1 = getBufferServerP();
         senderPoolReadyToSend.enqueue(var1);
         return var1;
      }
   }

   private static ByteArrayList getBufferServerP() {
      synchronized(senderPool) {
         if (senderPool.size() > 0) {
            return (ByteArrayList)senderPool.dequeue();
         }
      }

      return new ByteArrayList();
   }

   public static void freeUsedServerPool() {
      synchronized(senderPoolReadyToSend) {
         while(!senderPoolReadyToSend.isEmpty()) {
            freeBufferServer((ByteArrayList)senderPoolReadyToSend.dequeue());
         }

      }
   }

   public void deserialize(DataInputStream var1) throws IOException {
      int var2 = var1.readInt();
      this.buffer = new Long2ObjectOpenHashMap(var2);

      for(int var3 = 0; var3 < var2; ++var3) {
         long var4 = var1.readLong();
         int var6 = var1.readInt();
         ByteArrayList var7;
         (var7 = getBufferClient()).ensureCapacity(var6);

         for(int var8 = 0; var8 < var6; ++var8) {
            var7.add(var1.readByte());
         }

         this.buffer.put(var4, var7);
      }

   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.writeInt(this.buffer.size());
      Iterator var2 = this.buffer.entrySet().iterator();

      while(var2.hasNext()) {
         Entry var3 = (Entry)var2.next();
         var1.writeLong((Long)var3.getKey());
         int var4 = ((ByteArrayList)var3.getValue()).size();
         var1.writeInt(var4);

         for(int var5 = 0; var5 < var4; ++var5) {
            var1.writeByte(((ByteArrayList)var3.getValue()).get(var5));
         }
      }

   }
}
