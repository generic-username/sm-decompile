package org.schema.game.common.data.world;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.shorts.ShortList;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.rules.rules.SectorRuleEntityManager;
import org.schema.game.common.data.blockeffects.config.ConfigEntityManager;
import org.schema.game.common.data.blockeffects.config.ConfigManagerInterface;
import org.schema.game.common.data.blockeffects.config.ConfigProviderSource;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.inventory.FreeItem;
import org.schema.game.network.objects.NetworkSector;
import org.schema.game.network.objects.remote.RemoteItem;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.TopLevelType;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.server.ServerStateInterface;

public class RemoteSector implements ConfigManagerInterface, ConfigProviderSource, RuleEntityContainer, Sendable {
   private static final Vector3i tmpSys = new Vector3i();
   private static final Transform tmpTrns = new Transform();
   private static Vector3f tmpA = new Vector3f();
   private static Vector3f tmpB = new Vector3f();
   private final StateInterface state;
   private final boolean onServer;
   private final List itemsToAdd = new ObjectArrayList();
   private final List itemsToRemove = new ObjectArrayList();
   private int id = -1;
   private boolean markedForDeleteVolatile;
   private boolean markedForDeleteVolatileSent;
   private NetworkSector networkGameState;
   private Int2ObjectMap items = new Int2ObjectOpenHashMap();
   private Iterator iterator;
   private long lastFullUpdateTime;
   private Set currentPlayers = new ObjectOpenHashSet();
   private Sector serverSector;
   private SectorInformation.SectorType type;
   private boolean writtenForUnload;
   private SectorInformation.PlanetType planetType;
   private Vector3i clientPos = new Vector3i();
   private ConfigEntityManager configManager;
   private Object2ObjectOpenHashMap changedSlots = new Object2ObjectOpenHashMap();
   public long lag;
   public long lagEntities;
   private long lastLagSent;
   private long currentLag;
   private long lastLagReceived;
   private final List sectorProjectionSources = new ObjectArrayList();
   private SectorRuleEntityManager ruleEntityManager;

   public RemoteSector(StateInterface var1) {
      this.state = var1;
      this.onServer = var1 instanceof ServerStateInterface;
      this.ruleEntityManager = new SectorRuleEntityManager(this);
      if (!this.isOnServer()) {
         this.configManager = new ConfigEntityManager(0L, ConfigEntityManager.EffectEntityType.SECTOR, (GameClientState)this.state);
         this.configManager.entityName = "SECTOR";
      }

   }

   public static void getUpdateSet(GameServerState var0, Collection var1) {
      Iterator var2 = var0.getPlayerStatesByName().values().iterator();

      while(var2.hasNext()) {
         PlayerState var3 = (PlayerState)var2.next();
         Sendable var4;
         if ((var4 = (Sendable)var0.getLocalAndRemoteObjectContainer().getLocalObjects().get(var3.getCurrentSectorId())) != null) {
            assert var4 instanceof RemoteSector : var3 + " -> " + var3.getCurrentSectorId();

            var1.add((RemoteSector)var4);
         } else {
            System.err.println("[SERVER][REMOTESECTOR] WARNING: REMOTE SECTOR FOR " + var3 + " NOT FOUND: " + var3.getCurrentSectorId());
         }
      }

   }

   private void addItem(FreeItem var1) {
      synchronized(this.items) {
         this.itemsToAdd.add(var1);
      }
   }

   public void addItem(Vector3f var1, short var2, int var3, int var4) {
      if (var2 != 0) {
         this.addItem(new FreeItem(GameServerState.itemIds++, var2, var4, var3, var1));
      }

   }

   public void cleanUpOnEntityDelete() {
   }

   public void destroyPersistent() {
   }

   public NetworkSector getNetworkObject() {
      return this.networkGameState;
   }

   public boolean isPrivateNetworkObject() {
      return false;
   }

   public StateInterface getState() {
      return this.state;
   }

   public void initFromNetworkObject(NetworkObject var1) {
      this.setId(var1.id.get());
      byte var2 = (Byte)((NetworkSector)var1).type.get();
      this.type = SectorInformation.SectorType.values()[(byte)(var2 & 15)];
      this.planetType = SectorInformation.PlanetType.values()[(byte)(var2 >> 4 & 15)];
      this.getNetworkObject().pos.getVector(this.clientPos);
      this.configManager.initFromNetworkObject(this.getNetworkObject());
   }

   public void initialize() {
      if (!this.isOnServer()) {
         this.items = new Int2ObjectOpenHashMap();
      }

   }

   public boolean isMarkedForDeleteVolatile() {
      return this.markedForDeleteVolatile;
   }

   public void setMarkedForDeleteVolatile(boolean var1) {
      this.markedForDeleteVolatile = var1;
   }

   public boolean isMarkedForDeleteVolatileSent() {
      return this.markedForDeleteVolatileSent;
   }

   public void setMarkedForDeleteVolatileSent(boolean var1) {
      this.markedForDeleteVolatileSent = var1;
   }

   public boolean isMarkedForPermanentDelete() {
      return false;
   }

   public boolean isOkToAdd() {
      return true;
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public boolean isUpdatable() {
      return false;
   }

   public void markForPermanentDelete(boolean var1) {
   }

   public void newNetworkObject() {
      this.networkGameState = new NetworkSector(this.getState());
   }

   public void updateFromNetworkObject(NetworkObject var1, int var2) {
      this.handleReceivedNetworkItems((NetworkSector)var1);
      this.ruleEntityManager.receive(this.getNetworkObject());

      for(Iterator var5 = this.getNetworkObject().lagAnnouncement.getReceiveBuffer().iterator(); var5.hasNext(); this.lastLagReceived = System.currentTimeMillis()) {
         long var3 = (Long)var5.next();
         this.currentLag = var3;
      }

      this.configManager.updateFromNetworkObject(this.getNetworkObject());
   }

   public void updateLocal(Timer var1) {
      this.configManager.updateLocal(var1, this);
      this.ruleEntityManager.update(var1);
      this.sectorProjectionSources.clear();
      if (this.isOnServer()) {
         if (!this.getItems().isEmpty() || !this.itemsToAdd.isEmpty() || !this.itemsToRemove.isEmpty()) {
            Iterator var2 = ((GameServerState)this.getState()).getPlayerStatesByName().values().iterator();

            while(var2.hasNext()) {
               PlayerState var3;
               if ((var3 = (PlayerState)var2.next()) instanceof PlayerState && var3.getCurrentSectorId() == this.getId()) {
                  this.currentPlayers.add(var3);
               }
            }

            try {
               this.updateItems(var1, true);
            } catch (IOException var8) {
               var8.printStackTrace();
               throw new RuntimeException(var8);
            }

            this.currentPlayers.clear();
         }
      } else {
         GameClientState var9 = (GameClientState)this.state;
         Vector3i var4;
         Vector3i var10;
         Vector3i var5 = Galaxy.getLocalCoordinatesFromSystem(var4 = VoidSystem.getContainingSystem(var10 = this.clientPos(), tmpSys), new Vector3i());
         if (var9.getCurrentGalaxy().getSystemType(var5) == 2 && !var9.getCurrentGalaxy().isVoid(var5)) {
            var5 = var9.getCurrentGalaxy().getSunPositionOffset(var5, new Vector3i());
            Vector3i var6;
            (var6 = new Vector3i(8, 8, 8)).add(var5);
            if (!var4.equals(0, 0, 0) && !Sector.isTutorialSector(var10) && !Sector.isPersonalOrTestSector(var10)) {
               Sector.applyBlackHoleGrav(var9, var4, var6, var10, this.getId(), tmpTrns, tmpA, tmpB, var5, var1);
            }
         }

         try {
            this.updateItems(var1, false);
         } catch (IOException var7) {
            var7.printStackTrace();
            throw new RuntimeException(var7);
         }

         this.isClientInSector();
      }

      if (var1.currentTime - this.lastLagReceived > 7000L) {
         this.currentLag = 0L;
      }

   }

   public void updateToFullNetworkObject() {
      this.getNetworkObject().id.set(this.getId());
      this.sendAllItems();
      this.getNetworkObject().pos.set(this.getServerSector().pos);
      this.getNetworkObject().mode.set(this.getServerSector().getProtectionMode());
      this.getNetworkObject().active.set(this.getServerSector().isActive());
      this.getNetworkObject().type.set((byte)(this.type.ordinal() | this.planetType.ordinal() << 4));
      StellarSystem.debug = true;
      StellarSystem.isBorderSystem(this.getServerSector().pos);
      StellarSystem.isStarSystem(this.getServerSector().pos);
      StellarSystem.debug = false;
      this.configManager.updateToFullNetworkObject(this.getNetworkObject());
   }

   public void updateToNetworkObject() {
      if (this.isOnServer()) {
         this.getNetworkObject().id.set(this.getId());
         this.getNetworkObject().active.set(this.getServerSector().isActive());
         this.getNetworkObject().mode.set(this.getServerSector().getProtectionMode());
      }

      this.configManager.updateToNetworkObject(this.getNetworkObject());
   }

   public boolean isWrittenForUnload() {
      return this.writtenForUnload;
   }

   public void setWrittenForUnload(boolean var1) {
      this.writtenForUnload = var1;
   }

   public boolean clientActive() {
      return (Boolean)this.getNetworkObject().active.get();
   }

   public Vector3i clientPos() {
      assert !this.isOnServer();

      return this.clientPos;
   }

   public int getId() {
      return this.id;
   }

   public void setId(int var1) {
      this.id = var1;
   }

   public Int2ObjectMap getItems() {
      return this.items;
   }

   public void setItems(Int2ObjectMap var1) {
      this.items = var1;
      this.iterator = var1.values().iterator();
   }

   public Sector getServerSector() {
      assert this.state instanceof GameServerState;

      return this.serverSector;
   }

   private void handleReceivedNetworkItems(NetworkSector var1) {
      for(int var2 = 0; var2 < var1.itemBuffer.getReceiveBuffer().size(); ++var2) {
         RemoteItem var3;
         if ((var3 = (RemoteItem)var1.itemBuffer.getReceiveBuffer().get(var2)).isAdd()) {
            this.addItem((FreeItem)var3.get());
         } else {
            this.removeItem(((FreeItem)var3.get()).getId());
         }
      }

   }

   public boolean isClientInSector() {
      assert this.state instanceof GameClientState;

      return ((GameClientState)this.state).getCurrentSectorId() == this.id;
   }

   public void removeItem(int var1) {
      synchronized(this.items) {
         if ((FreeItem)this.items.get(var1) != null) {
            this.itemsToRemove.add(var1);
         } else {
            System.err.println((this.isOnServer() ? "[SERVER]" : "[CLIENT]") + "[RemoteSector] WARNING: trying to delete item id that doesn't exist: " + var1);
         }

      }
   }

   private void sendAllItems() {
      synchronized(this.items) {
         Iterator var2 = this.items.values().iterator();

         while(var2.hasNext()) {
            FreeItem var3 = (FreeItem)var2.next();
            this.getNetworkObject().itemBuffer.add(new RemoteItem(var3, true, this.getNetworkObject()));
         }

      }
   }

   public void setSector(Sector var1) {
      this.id = var1.getId();
      this.serverSector = var1;

      try {
         this.type = var1.getSectorType();
         this.planetType = var1.getPlanetType();
      } catch (IOException var2) {
         var2.printStackTrace();
      }

      assert var1.getDBId() > 0L;

      this.configManager = new ConfigEntityManager(var1.getDBId(), ConfigEntityManager.EffectEntityType.SECTOR, (GameServerState)this.getState());
      this.configManager.entityName = "SEC(" + var1.pos + ")";
      this.configManager.loadFromDatabase((GameServerState)this.getState());
   }

   public String toString() {
      if (this.isOnServer()) {
         return this.getServerSector() != null ? "[SERVER RemoteSector(" + this.id + ") " + this.getServerSector().isActive() + "; " + this.getServerSector().pos + "]" : "[SERVER RemoteSector(" + this.id + ") sector removed]";
      } else {
         return "[CLIENT ReSector(" + this.id + ") " + this.clientActive() + "]";
      }
   }

   private void updateItems(Timer var1, boolean var2) throws IOException {
      if (this.isOnServer()) {
         this.getServerSector().setChangedForDb(true);
         long var3 = System.currentTimeMillis();
         if (this.iterator != null && var3 - this.lastFullUpdateTime > 200L) {
            if (!this.iterator.hasNext()) {
               this.iterator = this.items.values().iterator();
            }

            for(int var9 = 0; this.iterator.hasNext() && var9 < 100; ++var9) {
               FreeItem var5;
               if (!(var5 = (FreeItem)this.iterator.next()).isAlive(var3)) {
                  this.removeItem(var5.getId());
               } else {
                  if (var5.checkFlagPhysics() && var5.doPhysicsTest(this.getServerSector()) && var2) {
                     this.getNetworkObject().itemBuffer.add(new RemoteItem(var5, true, this.getNetworkObject()));
                  }

                  Iterator var6 = this.currentPlayers.iterator();

                  while(var6.hasNext()) {
                     if (((PlayerState)var6.next()).checkItemInReach(var5, this.changedSlots)) {
                        if (this.isOnServer()) {
                           this.getServerSector().setChangedForDb(true);
                        }
                        break;
                     }
                  }
               }
            }

            if (!this.iterator.hasNext()) {
               this.lastFullUpdateTime = var3;
            }
         }

         Iterator var10 = this.changedSlots.entrySet().iterator();

         while(var10.hasNext()) {
            Entry var13;
            ((PlayerState)(var13 = (Entry)var10.next()).getKey()).sendInventoryModification((IntCollection)var13.getValue(), Long.MIN_VALUE);
         }

         this.changedSlots.clear();
      }

      FreeItem var11;
      if (!this.itemsToAdd.isEmpty()) {
         synchronized(this.items) {
            for(; !this.itemsToAdd.isEmpty(); this.iterator = this.items.values().iterator()) {
               FreeItem var4 = (FreeItem)this.itemsToAdd.remove(0);
               if ((var11 = (FreeItem)this.items.put(var4.getId(), var4)) != null) {
                  var4.setTimeSpawned(var11.getTimeSpawned());
                  System.err.println("[REMOTESECTOR] " + this.getState() + " ITEM change: " + var11 + " -> " + var4);
               }

               if (var2) {
                  this.getNetworkObject().itemBuffer.add(new RemoteItem(var4, true, this.getNetworkObject()));
               }
            }
         }
      }

      if (!this.itemsToRemove.isEmpty()) {
         synchronized(this.items) {
            for(; !this.itemsToRemove.isEmpty(); this.iterator = this.items.values().iterator()) {
               Integer var12 = (Integer)this.itemsToRemove.remove(0);
               var11 = (FreeItem)this.items.remove(var12);
               if (var2 && var11 != null) {
                  RemoteItem var14 = new RemoteItem(var11, false, this.getNetworkObject());
                  this.getNetworkObject().itemBuffer.add(var14);
               } else if (var11 == null) {
                  System.err.println("[SERVER][REMOTESECTOR] deleted invalid id: " + var12);
               }
            }

         }
      }
   }

   public int getSectorModeClient() {
      return this.getNetworkObject().mode.getInt();
   }

   public boolean isNoIndicationsClient() {
      return Sector.isMode(this.getSectorModeClient(), Sector.SectorMode.NO_INDICATIONS);
   }

   public boolean isPeaceClient() {
      return Sector.isMode(this.getSectorModeClient(), Sector.SectorMode.PROT_NO_SPAWN);
   }

   public boolean isProtectedClient() {
      return Sector.isMode(this.getSectorModeClient(), Sector.SectorMode.PROT_NO_ATTACK);
   }

   public boolean isNoEntryClient() {
      return Sector.isMode(this.getSectorModeClient(), Sector.SectorMode.LOCK_NO_ENTER);
   }

   public boolean isNoExitClient() {
      return Sector.isMode(this.getSectorModeClient(), Sector.SectorMode.LOCK_NO_EXIT);
   }

   public SectorInformation.SectorType getType() {
      return this.type;
   }

   public void announceLag(long var1) {
      if (System.currentTimeMillis() - this.lastLagSent > 1000L) {
         assert this.getState().isSynched();

         this.getNetworkObject().getLagAnnouncement().add(var1);

         assert this.getNetworkObject().isChanged();

         assert this.getNetworkObject().getLagAnnouncement().isChanged();

         this.lastLagSent = System.currentTimeMillis();
      }

   }

   public long getCurrentLag() {
      return this.currentLag;
   }

   public TopLevelType getTopLevelType() {
      return TopLevelType.SECTOR;
   }

   public ConfigEntityManager getConfigManager() {
      return this.configManager;
   }

   public ShortList getAppliedConfigGroups(ShortList var1) {
      return this.getConfigManager().applyMergeTo(true, true, var1);
   }

   public void registerTransientEffects(List var1) {
      var1.addAll(this.sectorProjectionSources);
   }

   public void entityUpdateInSector(SendableSegmentController var1) {
      var1.addSectorConfigProjection(this.sectorProjectionSources);
   }

   public void onAddedEntityFromSector(SendableSegmentController var1) {
   }

   public void onRemovedEntityFromSector(SendableSegmentController var1) {
   }

   public long getSourceId() {
      return -2L;
   }

   public SectorRuleEntityManager getRuleEntityManager() {
      return this.ruleEntityManager;
   }
}
