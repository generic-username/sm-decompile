package org.schema.game.common.data.world;

import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.schine.graphicsengine.core.Controller;

public class StaticObjectIntersection {
   public static final int TEST_LENGTH = 20;
   private static final float margin = 0.01F;
   Vector3b nearest = null;
   int side = -1;
   private Vector3f nearestIntersect = null;
   private Vector3f nMin = null;
   private Vector3f nMax = null;
   private boolean fromCameraViewer;

   public void getIntersect(Vector3f var1, Vector3f var2, SegmentData var3) {
      for(byte var4 = 0; var4 < 32; ++var4) {
         for(byte var5 = 0; var5 < 32; ++var5) {
            for(byte var6 = 0; var6 < 32; ++var6) {
               int var7 = SegmentData.getInfoIndex(var6, var5, var4);
               if (var3.contains(var7)) {
                  Vector3f var11 = new Vector3f((float)var6, (float)var5, (float)var4);
                  Vector3f var8 = new Vector3f(var11);
                  Vector3f var9 = new Vector3f(var11);
                  var8.x -= 0.5F;
                  var8.y -= 0.5F;
                  var8.z -= 0.5F;
                  var9.x += 0.5F;
                  var9.y += 0.5F;
                  var9.z += 0.5F;
                  var8.x -= 16.0F;
                  var8.y -= 16.0F;
                  var8.z -= 16.0F;
                  var9.x -= 16.0F;
                  var9.y -= 16.0F;
                  var9.z -= 16.0F;
                  float var10 = 0.0F;
                  if (this.fromCameraViewer) {
                     var10 = Controller.getCamera().getCameraOffset();
                  }

                  Vector3f var12;
                  if ((var12 = Vector3fTools.intersectsRayAABB(var1, var2, var8, var9, var10, 20.0F)) != null && (this.nearestIntersect == null || Vector3fTools.sub(var12, var1).length() < Vector3fTools.sub(this.nearestIntersect, var1).length())) {
                     System.err.println("intersection: " + var12);
                     this.nearestIntersect = var12;
                     this.nMin = var8;
                     this.nMax = var9;
                     if (this.nearest == null) {
                        this.nearest = new Vector3b();
                     }

                     this.nearest.set((byte)((int)var11.x), (byte)((int)var11.y), (byte)((int)var11.z));
                     System.err.println("nearest is: " + this.nearest + "; nearest intersection: " + this.nearestIntersect);
                  }
               }
            }
         }
      }

      if (this.nearest != null) {
         if (this.nearestIntersect.x >= this.nMax.x - 0.01F) {
            this.side = 4;
            return;
         }

         if (this.nearestIntersect.y >= this.nMax.y - 0.01F) {
            this.side = 2;
            return;
         }

         if (this.nearestIntersect.z >= this.nMax.z - 0.01F) {
            this.side = 0;
            return;
         }

         if (this.nearestIntersect.x <= this.nMin.x + 0.01F) {
            this.side = 5;
            return;
         }

         if (this.nearestIntersect.y <= this.nMin.y + 0.01F) {
            this.side = 3;
            return;
         }

         if (this.nearestIntersect.z <= this.nMin.z + 0.01F) {
            this.side = 1;
         }
      }

   }

   public boolean isIntersection() {
      return this.side >= 0;
   }
}
