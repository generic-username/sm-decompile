package org.schema.game.common.data.world.planet.texgen.palette;

import java.awt.Color;
import org.schema.common.FastMath;

public class GaussianTerrainRange implements ColorRange {
   private int m_minh;
   private int m_maxh;
   private float m_mediumh;
   private float m_varianceh;
   private int m_mins;
   private int m_maxs;
   private float m_mediums;
   private float m_variances;
   private int m_mint;
   private int m_maxt;
   private float m_mediumt;
   private float m_variancet;
   private Color m_color_terrain;
   private Color m_color_specular;

   public GaussianTerrainRange(int var1, int var2, float var3, float var4, int var5, int var6, float var7, float var8, int var9, int var10, float var11, float var12, Color var13, Color var14) {
      this.m_minh = var1;
      this.m_maxh = var2;
      this.m_mediumh = var3;
      this.m_varianceh = var4;
      this.m_mins = var5;
      this.m_maxs = var6;
      this.m_mediums = var7;
      this.m_variances = var8;
      this.m_mint = var9 + 257;
      this.m_maxt = var10 + 257;
      this.m_mediumt = var11 + 257.0F;
      this.m_variancet = var12;
      this.m_color_terrain = var13;
      this.m_color_specular = var14;
   }

   public float getFactor(int var1, int var2, int var3, int var4, float var5) {
      if (this.m_maxh != -1 && var3 > this.m_maxh) {
         return 0.0F;
      } else if (this.m_minh != -1 && var3 < this.m_minh) {
         return 0.0F;
      } else if (this.m_maxs != -1 && var4 > this.m_maxs) {
         return 0.0F;
      } else if (this.m_mins != -1 && var4 < this.m_mins) {
         return 0.0F;
      } else if (this.m_maxt != -1 && var5 > (float)this.m_maxt) {
         return 0.0F;
      } else if (this.m_mint != -1 && var5 < (float)this.m_mint) {
         return 0.0F;
      } else {
         float var7 = 0.0F;
         float var8 = 0.0F;
         float var6 = 0.0F;
         if ((double)this.m_varianceh != -1.0D) {
            var7 = Math.abs((this.m_mediumh - (float)var3) / this.m_varianceh);
         }

         if ((double)this.m_variances != -1.0D) {
            var8 = Math.abs((this.m_mediums - (float)var4) / this.m_variances);
         }

         if ((double)this.m_variancet != -1.0D) {
            var6 = Math.abs((this.m_mediumt - var5) / this.m_variancet);
         }

         return FastMath.exp(-(var7 + var8 + var6));
      }
   }

   public Color getTerrainColor() {
      return this.m_color_terrain;
   }

   public Color getTerrainSpecular() {
      return this.m_color_specular;
   }
}
