package org.schema.game.common.data.world.planet.texgen.palette;

public interface Palette {
   TerrainPalette.Colors getPointColor(ColorMixer var1, int var2, int var3, int var4, int var5, float var6);
}
