package org.schema.game.common.data.world.planet.texgen;

public class PerlinNoise {
   private int m_layerCount;
   private int m_width;
   private int m_height;
   private Randomizer3D2 randomizer;

   public PerlinNoise(int var1, int var2, int var3, int var4) {
      this.m_layerCount = var1;
      this.m_width = var2;
      this.m_height = var3;
      this.randomizer = new Randomizer3D2(var4);
   }

   private float getLayers(int var1, int var2, int var3) {
      int var4 = (this.m_height >> var3) - 1;
      int var5 = (this.m_width >> var3) - 1;
      int var6 = var1 >> var3;
      int var7;
      var4 &= (var7 = var2 >> var3) + 1;
      var5 &= var6 + 1;
      float var8 = this.randomizer.getPointRandom(var6, var7, var3);
      float var9 = this.randomizer.getPointRandom(var6, var4, var3);
      float var10 = this.randomizer.getPointRandom(var5, var7, var3);
      float var14 = this.randomizer.getPointRandom(var5, var4, var3);
      float var13 = 1.0F / (float)(1 << var3);
      float var11 = (float)var1 * var13 - (float)var6;
      float var12 = (float)var2 * var13 - (float)var7;
      return MathUtil.cosMix(MathUtil.cosMix(var8, var10, var11), MathUtil.cosMix(var9, var14, var11), var12);
   }

   public float[] getPerlin() {
      float[] var1 = new float[this.m_width * this.m_height];

      for(int var2 = 0; var2 < this.m_width; ++var2) {
         for(int var3 = 0; var3 < this.m_height; ++var3) {
            float var4 = 0.0F;
            int var5 = 0;

            for(int var6 = 1 << this.m_layerCount; var5 < this.m_layerCount; var6 >>= 1) {
               var4 += this.getLayers(var2, var3, var5) / (float)var6;
               ++var5;
            }

            var1[var2 + var3 * this.m_width] = var4;
         }
      }

      return var1;
   }

   public float getPerlinPointAt(int var1, int var2) {
      float var3 = 0.0F;
      float var4 = 1.0F / (float)(1 << this.m_layerCount);

      for(int var5 = 0; var5 < this.m_layerCount; ++var5) {
         var3 += this.getLayers(var1, var2, var5) * var4;
         var4 *= 2.0F;
      }

      return var3;
   }
}
