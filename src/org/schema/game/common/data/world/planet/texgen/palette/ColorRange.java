package org.schema.game.common.data.world.planet.texgen.palette;

import java.awt.Color;

public interface ColorRange {
   float getFactor(int var1, int var2, int var3, int var4, float var5);

   Color getTerrainColor();

   Color getTerrainSpecular();
}
