package org.schema.game.common.data.world.planet.texgen;

import org.schema.common.FastMath;
import org.schema.common.system.thread.ForRunnable;
import org.schema.game.common.data.world.planet.PlanetInformations;
import org.schema.game.common.data.world.planet.texgen.palette.ColorMixer;
import org.schema.game.common.data.world.planet.texgen.palette.Palette;
import org.schema.game.common.data.world.planet.texgen.palette.TerrainPalette;

public class ColorForRunnable implements ForRunnable {
   private ColorMixer mixer = new ColorMixer();
   private Palette palette;
   private int[] heightMap;
   private int[] specMap;
   private int[] colMap;
   private int shift;
   private PlanetInformations infos;
   private int height;
   private int halfheight;
   private int heightm1;
   private int width;
   private int widthm1;
   private int halfwidth;

   private ColorForRunnable() {
   }

   public ColorForRunnable(Palette var1, PlanetInformations var2, int var3, int var4, int[] var5, int[] var6, int[] var7) {
      this.infos = var2;
      this.palette = var1;
      this.heightMap = var5;
      this.specMap = var6;
      this.colMap = var7;
      this.width = var4;
      this.widthm1 = var4 - 1;
      this.halfwidth = var4 >> 1;
      this.height = var3;
      this.heightm1 = var3 - 1;

      for(this.halfheight = var3 >> 1; var4 != 1; ++this.shift) {
         var4 >>= 1;
      }

   }

   protected int at(int var1, int var2) {
      while(var1 < 0) {
         var1 += this.width;
      }

      if ((var2 &= (this.height << 1) - 1) > this.heightm1) {
         var2 = (this.heightm1 << 1) - var2;
         var1 += this.halfwidth;
      }

      if (var2 < 0) {
         var2 = -var2;
         var1 += this.width >> 1;
      }

      var1 &= this.widthm1;
      return var2 * this.width + var1;
   }

   public ForRunnable copy() {
      ColorForRunnable var1;
      (var1 = new ColorForRunnable()).infos = this.infos;
      var1.palette = this.palette;
      var1.shift = this.shift;
      var1.heightMap = this.heightMap;
      var1.specMap = this.specMap;
      var1.colMap = this.colMap;
      var1.height = this.height;
      var1.heightm1 = this.heightm1;
      var1.halfheight = this.halfheight;
      var1.width = this.width;
      var1.widthm1 = this.widthm1;
      var1.halfwidth = this.halfwidth;
      return var1;
   }

   public void run(int var1) {
      int var2 = var1 & this.widthm1;
      int var3 = var1 >> this.shift;
      int var4 = this.heightMap[var1];
      int var5 = this.getSlope(var2, var3);
      float var6 = this.getTemperature(var3, var4);
      TerrainPalette.Colors var7 = this.palette.getPointColor(this.mixer, var2, var3, var4, var5, var6);
      if (this.specMap != null) {
         this.specMap[var1] = var7.getSpecular().getRGB() | -16777216;
      }

      this.colMap[var1] = var7.getTerrain().getRGB() | -16777216;
   }

   protected int getSlope(int var1, int var2) {
      int var3 = Math.abs(this.heightMap[this.at(var1, var2)] - this.heightMap[this.at(var1 - 1, var2)]);
      int var4 = Math.abs(this.heightMap[this.at(var1, var2)] - this.heightMap[this.at(var1 + 1, var2)]);
      int var5 = Math.abs(this.heightMap[this.at(var1, var2)] - this.heightMap[this.at(var1, var2 - 1)]);
      var1 = Math.abs(this.heightMap[this.at(var1, var2)] - this.heightMap[this.at(var1, var2 + 1)]);
      return (int)MathUtil.max((float)var3, (float)var4, (float)var5, (float)var1);
   }

   protected float getTemperature(int var1, int var2) {
      float var3 = FastMath.cos((float)Math.abs(var1 - this.halfheight) / (float)this.height * 3.1415927F);
      return 257.0F + var3 * (float)(this.infos.getEquatorTemperature() - this.infos.getPoleTemperature()) + (float)this.infos.getPoleTemperature() - this.infos.getHeightFactor() * (float)Math.max(0, var2 - this.infos.getWaterLevel());
   }
}
