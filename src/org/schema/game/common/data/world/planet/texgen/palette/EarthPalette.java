package org.schema.game.common.data.world.planet.texgen.palette;

import java.awt.Color;
import org.schema.game.common.data.world.planet.PlanetInformations;

public class EarthPalette extends TerrainPalette {
   public EarthPalette(PlanetInformations var1) {
      super(var1);
   }

   public void initPalette() {
      GaussianTerrainRange var1 = new GaussianTerrainRange(this.getInformations().getWaterLevel(), -1, (float)this.getInformations().getWaterLevel(), 4.0F, -1, -1, 0.0F, 2.5F, -258, 80, 5.0F, 1.0F, new Color(4274728), Color.black);
      GaussianTerrainRange var2 = new GaussianTerrainRange(this.getInformations().getWaterLevel(), -1, (float)this.getInformations().getWaterLevel(), 3.0F, -1, -1, 0.0F, 2.0F, 0, 80, 18.0F, 3.5F, new Color(4672816), Color.black);
      GaussianTerrainRange var3 = new GaussianTerrainRange(this.getInformations().getWaterLevel(), -1, (float)this.getInformations().getWaterLevel() * 1.1F, 3.0F, -1, -1, 5.0F, 6.0F, -15, 50, 25.0F, 2.0F, new Color(2567958), Color.black);
      GaussianTerrainRange var4 = new GaussianTerrainRange(-1, this.getInformations().getWaterLevel(), 0.0F, 50.0F, -1, -1, 0.0F, 5.0F, -258, 100, 50.0F, 5.0F, new Color(1385520), Color.white);
      GaussianTerrainRange var5 = new GaussianTerrainRange(-1, this.getInformations().getWaterLevel(), (float)this.getInformations().getWaterLevel() * 0.5F, 50.0F, -1, -1, 0.0F, 5.0F, -258, 100, 50.0F, 5.0F, new Color(2111561), Color.white);
      GaussianTerrainRange var6 = new GaussianTerrainRange(-1, this.getInformations().getWaterLevel(), (float)this.getInformations().getWaterLevel(), 5.0F, -1, -1, 0.0F, 1.0F, -258, 100, 50.0F, 5.0F, new Color(5221032), Color.white);
      GaussianTerrainRange var7 = new GaussianTerrainRange(-1, -1, 0.0F, -1.0F, -1, -1, 0.0F, -1.0F, -258, 5, -50.0F, 3.5F, new Color(16777215), Color.white);
      GaussianTerrainRange var8 = new GaussianTerrainRange(this.getInformations().getWaterLevel(), -1, (float)this.getInformations().getWaterLevel() * 1.2F, 4.0F, -1, -1, 0.0F, 2.0F, -1, 100, 50.0F, 3.0F, new Color(15065005), Color.black);
      GaussianTerrainRange var9 = new GaussianTerrainRange(this.getInformations().getWaterLevel(), -1, (float)this.getInformations().getWaterLevel() * 1.3F, 4.0F, -1, -1, 0.0F, 2.0F, -1, 100, 50.0F, 3.0F, new Color(15065021), Color.black);
      new GaussianTerrainRange(this.getInformations().getWaterLevel(), -1, (float)this.getInformations().getWaterLevel() * 1.6F, 2.0F, -1, -1, 20.0F, 1.0F, -258, -258, 10.0F, 2.0F, new Color(7889998), Color.black);
      GaussianTerrainRange var10 = new GaussianTerrainRange(this.getInformations().getWaterLevel(), -1, (float)this.getInformations().getWaterLevel() * 2.0F, 4.0F, -1, -1, 10.0F, 1.5F, -258, -258, 5.0F, 3.0F, new Color(8873038), Color.black);
      this.attachTerrainRange(var1);
      this.attachTerrainRange(var2);
      this.attachTerrainRange(var3);
      this.attachTerrainRange(var5);
      this.attachTerrainRange(var6);
      this.attachTerrainRange(var7);
      this.attachTerrainRange(var4);
      this.attachTerrainRange(var8);
      this.attachTerrainRange(var9);
      this.attachTerrainRange(var10);
   }
}
