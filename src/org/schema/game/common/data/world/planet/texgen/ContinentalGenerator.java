package org.schema.game.common.data.world.planet.texgen;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import org.schema.common.system.thread.ForRunnable;
import org.schema.common.system.thread.MultiThreadUtil;
import org.schema.game.common.data.world.planet.PlanetInformations;
import org.schema.game.common.data.world.planet.texgen.palette.TerrainPalette;

public class ContinentalGenerator extends PlanetGenerator {
   private static final float[] m_voronoi_vectors = new float[]{8.5F, -3.0F, 1.0F};
   private Random m_random;

   public ContinentalGenerator(int var1, int var2, PlanetInformations var3, TerrainPalette var4) {
      super(var1, var2, var3, var4);
      this.m_random = new Random((long)var3.getSeed());
   }

   private void addSource(int var1, int var2, Set var3, int var4) {
      while(true) {
         if (var3 == null) {
            var3 = new HashSet();
         }

         int var5 = var1 - 1;
         int var6 = var1 + 1 & this.m_widthm1;
         int var7 = var2 - 1;
         int var8 = var2 + 1 & this.m_widthm1;
         if (var1 == 0) {
            var5 = this.m_widthm1;
         }

         if (var2 == 0) {
            var7 = this.m_widthm1;
         }

         int var9 = this.at(var1, var2);
         int var10 = this.at(var5, var7);
         int var11 = this.at(var1, var7);
         var7 = this.at(var6, var7);
         int var12 = this.at(var5, var2);
         var2 = this.at(var6, var2);
         var5 = this.at(var5, var8);
         var1 = this.at(var1, var8);
         var6 = this.at(var6, var8);
         if (this.m_heightMap[var9] == 0) {
            return;
         }

         if (((Set)var3).contains(var9) || this.m_heightMap[var9] > this.getInformations().getWaterLevel() - 1) {
            --var4;
            if (var4 > 0) {
               if (!(((Set)var3).contains(var10) & ((Set)var3).contains(var11) & ((Set)var3).contains(var7) & ((Set)var3).contains(var12) & ((Set)var3).contains(var2) & ((Set)var3).contains(var5) & ((Set)var3).contains(var1))) {
                  if (!((Set)var3).contains(var11) && this.m_heightMap[var11] < this.m_heightMap[var10]) {
                     var10 = var11;
                  }

                  if (!((Set)var3).contains(var7) && this.m_heightMap[var7] < this.m_heightMap[var10]) {
                     var10 = var7;
                  }

                  if (!((Set)var3).contains(var12) && this.m_heightMap[var12] < this.m_heightMap[var10]) {
                     var10 = var12;
                  }

                  if (!((Set)var3).contains(var2) && this.m_heightMap[var2] < this.m_heightMap[var10]) {
                     var10 = var2;
                  }

                  if (!((Set)var3).contains(var5) && this.m_heightMap[var5] < this.m_heightMap[var10]) {
                     var10 = var5;
                  }

                  if (!((Set)var3).contains(var1) && this.m_heightMap[var1] < this.m_heightMap[var10]) {
                     var10 = var1;
                  }

                  if (!((Set)var3).contains(var6) && this.m_heightMap[var6] < this.m_heightMap[var10]) {
                     var10 = var6;
                  }
               }

               ((Set)var3).add(var9);
               int[] var10000 = this.m_heightMap;
               var10000[var9] -= 2;
               var1 = var10 & this.m_widthm1;
               var2 = var10 >> this.m_shiftwidth;
               continue;
            }
         }

         int var10002 = this.m_heightMap[var9]--;
         return;
      }
   }

   protected int[] generateHeightmap() {
      int[] var1 = new int[this.getWidth() * this.getHeight()];
      PerlinNoise var2 = new PerlinNoise(7, this.getWidth(), this.getHeight(), this.getInformations().getSeed());
      VoronoiDiagram var3 = new VoronoiDiagram(this.getWidth(), this.getHeight(), 2.0F);

      int var5;
      int var6;
      for(int var4 = 0; var4 < 7; ++var4) {
         var5 = this.m_random.nextInt(this.getWidth());
         var6 = this.m_random.nextInt(this.getHeight());
         var3.addPoint(var5, var6);
      }

      float[] var10;
      var5 = (var10 = m_voronoi_vectors).length;

      for(var6 = 0; var6 < var5; ++var6) {
         float var7 = var10[var6];
         var3.addVector(var7);
      }

      int[] var11 = new int[256];
      float var12 = (float)var1.length * this.getInformations().getWaterInPercent();
      int var8;
      int var13;
      if (MultiThreadUtil.PROCESSORS_COUNT > 1) {
         var6 = this.getWidth() * this.getHeight();
         var13 = 0;

         for(var8 = this.getWidth(); var8 != 1; var8 >>= 1) {
            ++var13;
         }

         MultiThreadUtil.multiFor(0, var6, new ContinentalGenerator.ForHeightMap(var2, var13, var3, var1));

         for(int var9 = 0; var9 < var6; ++var9) {
            ++var11[var1[var9]];
         }
      } else {
         for(var6 = 0; var6 < this.getWidth(); ++var6) {
            for(var13 = 0; var13 < this.getHeight(); ++var13) {
               var8 = (int)(var2.getPerlinPointAt(var6, var13) * 255.0F * (1.0F - 0.8F * var3.getValueAt(var6, var13)));
               int var10002 = var11[var8]++;
               var1[var13 * this.getWidth() + var6] = var8;
            }
         }
      }

      var6 = 0;

      for(var13 = 0; (float)var6 < var12; var6 += var11[var13++]) {
      }

      this.getInformations().setWaterLevel(var13);
      this.m_heightMap = var1;

      for(var8 = 0; var8 < this.getHeight() / 5; ++var8) {
         this.addSource(this.m_random.nextInt(this.getWidth()), this.m_random.nextInt(this.getHeight()), (Set)null, 128);
      }

      return var1;
   }

   final class ForHeightMap implements ForRunnable {
      private PerlinNoise perlin;
      private int shift;
      private VoronoiDiagram diagram;
      private int[] heightmap;

      private ForHeightMap() {
      }

      public ForHeightMap(PerlinNoise var2, int var3, VoronoiDiagram var4, int[] var5) {
         this.perlin = var2;
         this.shift = var3;
         this.diagram = var4.clone();
         this.heightmap = var5;
      }

      public final ForRunnable copy() {
         ContinentalGenerator.ForHeightMap var1;
         (var1 = ContinentalGenerator.this.new ForHeightMap()).perlin = this.perlin;
         var1.shift = this.shift;
         var1.diagram = this.diagram.clone();
         var1.heightmap = this.heightmap;
         return var1;
      }

      public final void run(int var1) {
         int var2 = var1 & ContinentalGenerator.this.m_widthm1;
         int var3 = var1 >> this.shift;
         var2 = (int)(this.perlin.getPerlinPointAt(var2, var3) * 255.0F * (1.0F - 0.8F * this.diagram.getValueAt(var2, var3)));
         this.heightmap[var1] = var2;
      }
   }
}
