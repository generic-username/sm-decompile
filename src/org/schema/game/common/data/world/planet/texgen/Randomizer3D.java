package org.schema.game.common.data.world.planet.texgen;

public class Randomizer3D {
   private static final float INV_0X10000 = 1.5258789E-5F;
   private static final int a = 1664525;
   private static final int b = 1013904223;
   private int seed;

   public Randomizer3D(int var1) {
      this.seed = var1;
   }

   public float getPointRandom(int var1, int var2) {
      return this.getPointRandom(var1, var2, 0);
   }

   public float getPointRandom(int var1, int var2, int var3) {
      return (float)(this.turn(var1) * this.turn(this.seed * var2) * this.turn(this.seed * this.seed * var3) & '\uffff') * 1.5258789E-5F;
   }

   public int turn(int var1) {
      return var1 * 1664525 + 1013904223;
   }
}
