package org.schema.game.common.data.world.planet.texgen;

import org.schema.common.FastMath;

public final class MathUtil {
   private MathUtil() {
   }

   public static final float clamp(float var0, float var1, float var2) {
      return Math.max(var1, Math.min(var2, var0));
   }

   public static final int clamp(int var0, int var1, int var2) {
      return Math.max(var1, Math.min(var2, var0));
   }

   public static final float cosMix(float var0, float var1, float var2) {
      var2 = (1.0F - FastMath.cos(var2 * 3.1415927F)) * 0.5F;
      return var0 * (1.0F - var2) + var1 * var2;
   }

   public static final float linMix(float var0, float var1, float var2) {
      return var0 * (1.0F - var2) + var1 * var2;
   }

   public static final float max(float... var0) {
      float var1 = Float.MIN_VALUE;
      int var2 = (var0 = var0).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         float var4 = var0[var3];
         var1 = Math.max(var1, var4);
      }

      return var1;
   }

   public static final float min(float... var0) {
      float var1 = Float.MAX_VALUE;
      int var2 = (var0 = var0).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         float var4 = var0[var3];
         var1 = Math.min(var1, var4);
      }

      return var1;
   }
}
