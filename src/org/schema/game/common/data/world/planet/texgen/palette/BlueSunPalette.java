package org.schema.game.common.data.world.planet.texgen.palette;

import java.awt.Color;
import org.schema.game.common.data.world.planet.PlanetInformations;

public class BlueSunPalette extends TerrainPalette {
   public BlueSunPalette(PlanetInformations var1) {
      super(var1);
   }

   public void initPalette() {
      GaussianTerrainRange var1 = new GaussianTerrainRange(this.getInformations().getWaterLevel(), -1, (float)this.getInformations().getWaterLevel(), 4.0F, -1, -1, 0.0F, 0.8F, -258, 200, 130.0F, 5.5F, new Color(31208), Color.white);
      GaussianTerrainRange var2 = new GaussianTerrainRange(this.getInformations().getWaterLevel(), -1, (float)this.getInformations().getWaterLevel() * 1.1F, 3.0F, -1, -1, 5.0F, 6.0F, -15, 200, 150.0F, 2.0F, new Color(30964), Color.white);
      GaussianTerrainRange var3 = new GaussianTerrainRange(-1, this.getInformations().getWaterLevel(), 0.0F, 50.0F, -1, -1, 0.0F, 5.0F, -258, 200, 50.0F, 5.0F, new Color(131181), Color.white);
      GaussianTerrainRange var4 = new GaussianTerrainRange(-1, this.getInformations().getWaterLevel(), (float)this.getInformations().getWaterLevel() * 0.5F, 50.0F, -1, -1, 0.0F, 5.0F, -258, 200, 50.0F, 5.0F, new Color(1415), Color.white);
      GaussianTerrainRange var5 = new GaussianTerrainRange(-1, this.getInformations().getWaterLevel(), (float)this.getInformations().getWaterLevel(), 5.0F, -1, -1, 0.0F, 1.0F, -258, 200, 100.0F, 5.0F, new Color(16309), Color.white);
      GaussianTerrainRange var6 = new GaussianTerrainRange(-1, -1, 0.0F, -1.0F, -1, -1, 0.0F, -1.0F, -258, 5, -100.0F, 3.0F, new Color(16777215), Color.white);
      GaussianTerrainRange var7 = new GaussianTerrainRange(this.getInformations().getWaterLevel(), -1, (float)this.getInformations().getWaterLevel() * 1.2F, 4.0F, -1, -1, 0.0F, 2.0F, -258, 200, 160.0F, 3.0F, new Color(30950), Color.white);
      GaussianTerrainRange var8 = new GaussianTerrainRange(this.getInformations().getWaterLevel(), -1, (float)this.getInformations().getWaterLevel() * 1.3F, 4.0F, -1, -1, 0.0F, 2.0F, -1, 200, 180.0F, 3.0F, new Color(30950), Color.white);
      GaussianTerrainRange var9 = new GaussianTerrainRange(this.getInformations().getWaterLevel(), -1, (float)this.getInformations().getWaterLevel() * 2.0F, 4.0F, -1, -1, 10.0F, 1.5F, -258, 200, 200.0F, 3.0F, new Color(16777215), Color.white);
      this.attachTerrainRange(var1);
      this.attachTerrainRange(var2);
      this.attachTerrainRange(var4);
      this.attachTerrainRange(var5);
      this.attachTerrainRange(var6);
      this.attachTerrainRange(var3);
      this.attachTerrainRange(var7);
      this.attachTerrainRange(var8);
      this.attachTerrainRange(var9);
   }
}
