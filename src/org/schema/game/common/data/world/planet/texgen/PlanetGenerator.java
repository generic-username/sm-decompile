package org.schema.game.common.data.world.planet.texgen;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.MemoryImageSource;
import org.schema.common.FastMath;
import org.schema.common.system.thread.ForRunnable;
import org.schema.common.system.thread.MultiThreadUtil;
import org.schema.game.common.data.world.planet.PlanetInformations;
import org.schema.game.common.data.world.planet.texgen.palette.TerrainPalette;

public abstract class PlanetGenerator {
   public static final int MAP_COLOR = 1;
   public static final int MAP_SPECULAR = 2;
   public static final int MAP_CLOUD = 4;
   public static final int MAP_NIGHT = 8;
   public static final int MAP_HEIGHT = 16;
   public static final int MAP_NORMAL = 32;
   protected int[] m_heightMap;
   protected int[] m_specularMap;
   protected int[] m_colorMap;
   protected int[] m_cloudMap;
   protected int[] m_normalMap;
   protected int m_halfheight;
   protected int m_halfwidth;
   protected int m_widthm1;
   protected int m_heightm1;
   protected int m_shiftheight;
   protected int m_shiftwidth;
   private PlanetInformations m_informations;
   private TerrainPalette m_palette;
   private int m_width;
   private int m_height;

   public PlanetGenerator(int var1, int var2, PlanetInformations var3, TerrainPalette var4) {
      if (FastMath.isPowerOfTwo(var1) && FastMath.isPowerOfTwo(var2)) {
         this.m_palette = var4;
         this.m_width = var1;
         this.m_height = var2;
         this.m_informations = var3;
         this.m_halfheight = this.m_height >> 1;
         this.m_halfwidth = this.m_width >> 1;
         this.m_widthm1 = this.m_width - 1;

         for(this.m_heightm1 = this.m_height - 1; var1 != 1; ++this.m_shiftwidth) {
            var1 >>= 1;
         }

         while(var2 != 1) {
            var2 >>= 1;
            ++this.m_shiftheight;
         }

      } else {
         throw new RuntimeException("[ERROR] TextureNew height and width must be power of two...");
      }
   }

   protected int at(int var1, int var2) {
      while(var1 < 0) {
         var1 += this.m_width;
      }

      if ((var2 &= (this.m_height << 1) - 1) > this.m_heightm1) {
         var2 = (this.m_heightm1 << 1) - var2;
         var1 += this.m_halfwidth;
      }

      if (var2 < 0) {
         var2 = -var2;
         var1 += this.m_width >> 1;
      }

      var1 &= this.m_widthm1;
      return var2 * this.m_width + var1;
   }

   public final void generateAllMap(int var1) {
      this.getHeightMap();
      if ((var1 & 1) != 0) {
         this.getColorMap();
      }

      if ((var1 & 2) != 0) {
         this.getSpecularMap();
      }

   }

   @Deprecated
   protected int[] generateCloudMap() {
      return null;
   }

   protected void generateColorAndSpecularMap() {
      if (this.m_colorMap == null) {
         this.m_colorMap = new int[this.m_height * this.m_width];
         this.m_specularMap = new int[this.m_height * this.m_width];
         int[] var1 = this.getHeightMap();
         this.m_palette.initPalette();
         if (MultiThreadUtil.PROCESSORS_COUNT > 1) {
            MultiThreadUtil.multiFor(0, this.m_width * this.m_height, new ColorForRunnable(this.m_palette, this.m_informations, this.m_height, this.m_width, var1, this.m_specularMap, this.m_colorMap));
            return;
         }

         for(int var2 = 0; var2 < this.m_width; ++var2) {
            for(int var3 = 0; var3 < this.m_height; ++var3) {
               int var4 = this.at(var2, var3);
               int var5 = var1[var4];
               int var6 = this.getSlope(var2, var3);
               float var7 = this.getTemperature(var3, var5);
               TerrainPalette.Colors var8 = this.m_palette.getPointColor(var2, var3, var5, var6, var7);
               this.m_specularMap[var4] = var8.getSpecular().getRGB() | -16777216;
               this.m_colorMap[var4] = var8.getTerrain().getRGB() | -16777216;
            }
         }
      }

   }

   protected abstract int[] generateHeightmap();

   protected int[] generateSpecularMap() {
      return null;
   }

   public final synchronized int[] getColorMap() {
      if (this.m_colorMap == null) {
         this.generateColorAndSpecularMap();
      }

      return this.m_colorMap;
   }

   public BufferedImage getDebugImageMap(int var1) {
      int[] var2;
      switch(var1) {
      case 1:
         var2 = (int[])this.getColorMap().clone();
         break;
      case 2:
         var2 = (int[])this.getSpecularMap().clone();
         break;
      case 16:
         var2 = (int[])this.getHeightMap().clone();
         break;
      case 32:
         var2 = (int[])this.getNormalMap().clone();
         break;
      default:
         return null;
      }

      if (var1 == 16) {
         for(var1 = 0; var1 < var2.length; ++var1) {
            var2[var1] *= 65793;
         }
      }

      Image var3 = Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(this.m_width, this.m_height, var2, 0, this.m_width));
      BufferedImage var4;
      (var4 = new BufferedImage(this.m_width, this.m_height, 1)).getGraphics().drawImage(var3, 0, 0, (ImageObserver)null);
      return var4;
   }

   public int getHeight() {
      return this.m_height;
   }

   public final synchronized int[] getHeightMap() {
      if (this.m_heightMap == null) {
         this.m_heightMap = this.generateHeightmap();
      }

      return this.m_heightMap;
   }

   public PlanetInformations getInformations() {
      return this.m_informations;
   }

   public final synchronized int[] getNormalMap() {
      if (this.m_normalMap == null) {
         this.m_normalMap = new int[this.m_width * this.m_height];
         if (MultiThreadUtil.PROCESSORS_COUNT > 1) {
            long var1 = System.currentTimeMillis();
            MultiThreadUtil.multiFor(0, this.m_normalMap.length, new ForRunnable() {
               int[] heightMap = PlanetGenerator.this.getHeightMap();

               public ForRunnable copy() {
                  return this;
               }

               public void run(int var1) {
                  int var2 = var1 & PlanetGenerator.this.m_widthm1;
                  int var3 = var1 >> PlanetGenerator.this.m_shiftwidth;
                  int var4 = Math.max(PlanetGenerator.this.getInformations().getWaterLevel(), this.heightMap[PlanetGenerator.this.at(var2, var3)]);
                  int var5 = Math.max(PlanetGenerator.this.getInformations().getWaterLevel(), this.heightMap[PlanetGenerator.this.at(var2, var3 + 1)]);
                  var2 = Math.max(PlanetGenerator.this.getInformations().getWaterLevel(), this.heightMap[PlanetGenerator.this.at(var2 + 1, var3)]);
                  var3 = var5 - var4;
                  var2 -= var4;
                  float var7 = (float)var3 / 255.0F * 150.0F;
                  float var6 = (float)var2 / 255.0F * 150.0F;
                  float var8 = FastMath.sqrt(var7 * var7 + var6 * var6 + 150.0F);
                  var7 /= var8;
                  var6 /= var8;
                  var3 = (int)(var7 * 127.0F + 128.0F);
                  var2 = (int)(var6 * 127.0F + 128.0F);
                  PlanetGenerator.this.m_normalMap[var1] = -16777216 | var3 << 16 | var2 << 8 | 255;
               }
            });
            var1 = System.currentTimeMillis() - var1;
            System.out.println("Temps: " + var1);
         } else {
            int[] var7 = this.getHeightMap();

            for(int var2 = 0; var2 < this.m_width; ++var2) {
               for(int var3 = 0; var3 < this.m_height; ++var3) {
                  int var4 = Math.max(this.getInformations().getWaterLevel(), var7[this.at(var2, var3)]);
                  int var5 = Math.max(this.getInformations().getWaterLevel(), var7[this.at(var2, var3 + 1)]);
                  int var6 = Math.max(this.getInformations().getWaterLevel(), var7[this.at(var2 + 1, var3)]);
                  var5 -= var4;
                  var4 = var6 - var4;
                  float var9 = (float)var5 / 255.0F * 150.0F;
                  float var8 = (float)var4 / 255.0F * 150.0F;
                  float var10 = FastMath.sqrt(var9 * var9 + var8 * var8 + 150.0F);
                  var9 /= var10;
                  var8 /= var10;
                  var5 = (int)(var9 * 127.0F + 128.0F);
                  var4 = (int)(var8 * 127.0F + 128.0F);
                  this.m_normalMap[this.at(var2, var3)] = -16777216 | var5 << 16 | var4 << 8 | 255;
               }
            }
         }
      }

      return this.m_normalMap;
   }

   protected int getSlope(int var1, int var2) {
      int[] var3;
      int var4 = Math.abs((var3 = this.getHeightMap())[this.at(var1, var2)] - var3[this.at(var1 - 1, var2)]);
      int var5 = Math.abs(var3[this.at(var1, var2)] - var3[this.at(var1 + 1, var2)]);
      int var6 = Math.abs(var3[this.at(var1, var2)] - var3[this.at(var1, var2 - 1)]);
      var1 = Math.abs(var3[this.at(var1, var2)] - var3[this.at(var1, var2 + 1)]);
      return (int)MathUtil.max((float)var4, (float)var5, (float)var6, (float)var1);
   }

   public final synchronized int[] getSpecularMap() {
      if (this.m_specularMap == null) {
         this.generateColorAndSpecularMap();
      }

      return this.m_specularMap;
   }

   protected float getTemperature(int var1, int var2) {
      float var3 = FastMath.cos((float)Math.abs(var1 - this.m_halfheight) / (float)this.m_height * 3.1415927F);
      return 257.0F + var3 * (float)(this.m_informations.getEquatorTemperature() - this.m_informations.getPoleTemperature()) + (float)this.m_informations.getPoleTemperature() - this.m_informations.getHeightFactor() * MathUtil.max(0.0F, (float)(var2 - this.m_informations.getWaterLevel()));
   }

   public int getWidth() {
      return this.m_width;
   }
}
