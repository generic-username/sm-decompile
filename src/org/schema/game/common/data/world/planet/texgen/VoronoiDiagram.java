package org.schema.game.common.data.world.planet.texgen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class VoronoiDiagram implements Cloneable {
   private List voronoiPoints;
   private List coefs;
   private int width;
   private int height;
   private float m_power;
   private VoronoiDiagram.PointComparator m_comparator = new VoronoiDiagram.PointComparator();
   private VoronoiDiagram.Point pointTemp = new VoronoiDiagram.Point(0.0F, 0.0F);

   public VoronoiDiagram(int var1, int var2, float var3) {
      this.width = var1;
      this.height = var2;
      this.m_power = var3;
      this.voronoiPoints = new ArrayList();
      this.coefs = new ArrayList();
   }

   public void addPoint(int var1, int var2) {
      this.voronoiPoints.add(new VoronoiDiagram.Point((float)var1, (float)var2));
   }

   public void addVector(float var1) {
      this.coefs.add(var1);
   }

   public VoronoiDiagram clone() {
      VoronoiDiagram var1;
      (var1 = new VoronoiDiagram(this.width, this.height, this.m_power)).coefs = new ArrayList(this.coefs);
      var1.voronoiPoints = new ArrayList(this.voronoiPoints);
      return var1;
   }

   public int[] computeVoronoi() {
      int[] var1 = new int[this.width * this.height];

      for(int var2 = 0; var2 < this.width; ++var2) {
         for(int var3 = 0; var3 < this.height; ++var3) {
            VoronoiDiagram.Point var4 = new VoronoiDiagram.Point((float)var2, (float)var3);
            this.m_comparator.setPoint(var4);
            Collections.sort(this.voronoiPoints, this.m_comparator);

            for(int var5 = 0; var5 < this.coefs.size(); ++var5) {
               VoronoiDiagram.Point var6 = (VoronoiDiagram.Point)this.voronoiPoints.get(var5);
               int var10001 = var3 * this.width + var2;
               var1[var10001] = (int)((float)var1[var10001] + (Float)this.coefs.get(var5) * 255.0F * var6.distanceSquared(var4));
            }

            var1[var3 * this.width + var2] = Math.max(0, Math.min(255, var1[var3 * this.width + var2]));
         }
      }

      return var1;
   }

   public float getValueAt(int var1, int var2) {
      this.pointTemp.x = (float)var1 / (float)this.width;
      this.pointTemp.y = (float)var2 / (float)this.height;
      this.m_comparator.setPoint(this.pointTemp);
      Collections.sort(this.voronoiPoints, this.m_comparator);
      float var4 = 0.0F;

      for(var2 = 0; var2 < this.coefs.size(); ++var2) {
         VoronoiDiagram.Point var3 = (VoronoiDiagram.Point)this.voronoiPoints.get(var2);
         var4 += (Float)this.coefs.get(var2) * this.m_power * var3.distanceSquared(this.pointTemp);
      }

      return MathUtil.clamp(var4, 0.0F, 1.0F);
   }

   class PointComparator implements Comparator {
      private VoronoiDiagram.Point toCompare;

      public PointComparator() {
      }

      public int compare(VoronoiDiagram.Point var1, VoronoiDiagram.Point var2) {
         float var3 = var1.distanceSquared(this.toCompare);
         float var4 = var2.distanceSquared(this.toCompare);
         return var3 > var4 ? 1 : -1;
      }

      public void setPoint(VoronoiDiagram.Point var1) {
         this.toCompare = var1;
      }
   }

   final class Point {
      float x;
      float y;

      Point(float var2, float var3) {
         this.x = var2 / (float)VoronoiDiagram.this.width;
         this.y = var3 / (float)VoronoiDiagram.this.height;
      }

      final float distanceSquared(VoronoiDiagram.Point var1) {
         int var2 = VoronoiDiagram.this.width >> 1;
         int var3 = VoronoiDiagram.this.height >> 1;
         float var4 = (this.x - var1.x) * (this.x - var1.x) + (this.y - var1.y - (float)var2) * (this.y - var1.y - (float)var2);
         float var5 = (this.x - var1.x) * (this.x - var1.x) + (this.y - var1.y) * (this.y - var1.y);
         float var6 = (this.x - var1.x) * (this.x - var1.x) + (this.y - var1.y + (float)var2) * (this.y - var1.y + (float)var2);
         float var7 = (this.x - (var1.x + 1.0F)) * (this.x - (var1.x + 1.0F)) + (this.y - var1.y - (float)var2) * (this.y - var1.y - (float)var2);
         float var8 = (this.x - (var1.x + 1.0F)) * (this.x - (var1.x + 1.0F)) + (this.y - var1.y) * (this.y - var1.y);
         float var9 = (this.x - (var1.x + 1.0F)) * (this.x - (var1.x + 1.0F)) + (this.y - var1.y + (float)var2) * (this.y - var1.y + (float)var2);
         float var10 = (this.x - (var1.x - 1.0F)) * (this.x - (var1.x - 1.0F)) + (this.y - var1.y - (float)var2) * (this.y - var1.y - (float)var2);
         float var11 = (this.x - (var1.x - 1.0F)) * (this.x - (var1.x - 1.0F)) + (this.y - var1.y) * (this.y - var1.y);
         float var18 = (this.x - (var1.x - 1.0F)) * (this.x - (var1.x - 1.0F)) + (this.y - var1.y + (float)var2) * (this.y - var1.y + (float)var2);
         float var12 = (this.x - var1.x - (float)var3) * (this.x - var1.x - (float)var3) + (this.y - var1.y + 1.0F) * (this.y - var1.y + 1.0F);
         float var13 = (this.x - var1.x) * (this.x - var1.x) + (this.y - var1.y + 1.0F) * (this.y - var1.y + 1.0F);
         float var14 = (this.x - var1.x + (float)var3) * (this.x - var1.x + (float)var3) + (this.y - var1.y + 1.0F) * (this.y - var1.y + 1.0F);
         float var15 = (this.x - var1.x - (float)var3) * (this.x - var1.x - (float)var3) + (this.y - var1.y - 1.0F) * (this.y - var1.y - 1.0F);
         float var16 = (this.x - var1.x) * (this.x - var1.x) + (this.y - var1.y - 1.0F) * (this.y - var1.y - 1.0F);
         float var17 = (this.x - var1.x + (float)var3) * (this.x - var1.x + (float)var3) + (this.y - var1.y - 1.0F) * (this.y - var1.y - 1.0F);
         return MathUtil.min(var4, var5, var6, var7, var8, var9, var10, var11, var18, var12, var13, var14, var15, var16, var17);
      }

      public final String toString() {
         return "x= " + this.x + ", y = " + this.y;
      }
   }
}
