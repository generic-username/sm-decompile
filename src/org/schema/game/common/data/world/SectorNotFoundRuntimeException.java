package org.schema.game.common.data.world;

public class SectorNotFoundRuntimeException extends RuntimeException {
   private static final long serialVersionUID = 1L;

   public SectorNotFoundRuntimeException(int var1) {
      super("SECTOR: " + String.valueOf(var1));
   }
}
