package org.schema.game.common.data.world;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.data.DataUtil;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.physics.AABBVarSet;
import org.schema.game.common.data.physics.sweepandpruneaabb.SegmentAabbInterface;
import org.schema.schine.graphicsengine.forms.DebugBox;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.resource.FileExt;

public abstract class Segment implements SegmentAabbInterface {
   public static final int DIM_BITS = 5;
   public static final int DIM_BITS_X2 = 10;
   public static final byte DIM = 32;
   public static final byte HALF_DIM = 16;
   static final int HALF_SIZE = 1;
   public static boolean ALLOW_ADMIN_OVERRIDE = false;
   public static long debugTime = 0L;
   private static int idGen;
   public final float[] cachedTransform = new float[16];
   public final Vector3i pos = new Vector3i();
   public final Vector3i absPos = new Vector3i();
   public float cacheBBMinX;
   public float cacheBBMinY;
   public float cacheBBMinZ;
   public float cacheBBMaxX;
   public float cacheBBMaxY;
   public float cacheBBMaxZ;
   public short cacheDate;
   protected int id;
   private int size = 0;
   private SegmentData segmentData;
   private SegmentController segmentController;

   public Segment(SegmentController var1) {
      this.segmentController = var1;
      this.id = idGen++;
      this.cachedTransform[15] = 1.0F;
   }

   public static void debugDraw(Vector3i var0, Transform var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      var1 = new Transform(var1);
      Vector3f var8 = new Vector3f((float)var0.x, (float)var0.y, (float)var0.z);
      var1.basis.transform(var8);
      var1.origin.add(var8);
      DebugBox var9;
      (var9 = new DebugBox(new Vector3f(-16.0F - var2 - 0.5F, -16.0F - var2 - 0.5F, -16.0F - var2 - 0.5F), new Vector3f(var2 + 16.0F - 0.5F, var2 + 16.0F - 0.5F, var2 + 16.0F - 0.5F), new Transform(var1), var3, var4, var5, var6)).LIFETIME = (long)var7;
      DebugDrawer.boxes.add(var9);
   }

   public static void debugDraw(int var0, int var1, int var2, Vector3i var3, Transform var4, float var5, float var6, float var7, float var8, float var9, float var10) {
      var4 = new Transform(var4);
      Vector3f var11 = new Vector3f((float)(var3.x + var0 - 16), (float)(var3.y + var1 - 16), (float)(var3.z + var2 - 16));
      var4.basis.transform(var11);
      var4.origin.add(var11);
      DebugBox var12;
      (var12 = new DebugBox(new Vector3f(-0.5F - var5, -0.5F - var5, -0.5F - var5), new Vector3f(var5 + 0.5F, var5 + 0.5F, var5 + 0.5F), new Transform(var4), var6, var7, var8, var9)).LIFETIME = (long)var10;
      DebugDrawer.boxes.add(var12);
   }

   public static Vector3b getElementIndexFrom(int var0, int var1, int var2, Vector3b var3) {
      var3.set((byte)ByteUtil.modUSeg(var0), (byte)ByteUtil.modUSeg(var1), (byte)ByteUtil.modUSeg(var2));
      return var3;
   }

   public static Vector3i getSegmentIndexFromSegmentElement(int var0, int var1, int var2, Vector3i var3) {
      var3.set(ByteUtil.divUSeg(var0), ByteUtil.divUSeg(var1), ByteUtil.divUSeg(var2));
      return var3;
   }

   public static boolean isInRange(int var0, int var1, int var2, int var3, int var4, int var5, int var6) {
      var3 -= var0;
      var4 -= var2;
      var5 -= var2;
      return FastMath.sqrt((float)(var3 * var3 + var4 * var4 + var5 * var5)) < (float)var6;
   }

   public static void main(String[] var0) {
      for(int var1 = -64; var1 < 64; ++var1) {
         System.err.println("0, 0, " + var1 + " -> " + getElementIndexFrom(0, 0, var1, new Vector3b()));
      }

   }

   public static Vector3i getAbsoluteElemPos(Vector3i var0, byte var1, byte var2, byte var3, Vector3i var4) {
      var4.x = var0.x + var1;
      var4.y = var0.y + var2;
      var4.z = var0.z + var3;
      return var4;
   }

   public boolean addElement(short var1, Vector3b var2, int var3, boolean var4, short var5, SegmentController var6) {
      return this.addElement(var1, var2.x, var2.y, var2.z, var3, var4, var5, var6);
   }

   public boolean addElement(short var1, byte var2, byte var3, byte var4, int var5, boolean var6, short var7, SegmentController var8) {
      boolean var9 = false;
      boolean var10 = false;
      if (this.isEmpty()) {
         if (this.getSegmentData() == null) {
            this.getSegmentController().getSegmentProvider().getFreeSegmentData().assignData(this);
         }

         var10 = true;
      }

      SegmentData.getInfoIndex(var2, var3, var4);
      if (!this.getSegmentData().containsUnsave(var2, var3, var4)) {
         byte var13 = !var6 ? 0 : ElementInformation.defaultActive(var1);

         try {
            this.getSegmentData().setInfoElement(var2, var3, var4, var1, (byte)var5, var13, var7, true, this.getAbsoluteIndex(var2, var3, var4), var8.getState().getUpdateTime());
         } catch (SegmentDataWriteException var12) {
            SegmentDataWriteException.replaceData(this);

            try {
               this.getSegmentData().setInfoElement(var2, var3, var4, var1, (byte)var5, var13, var7, true, this.getAbsoluteIndex(var2, var3, var4), var8.getState().getUpdateTime());
            } catch (SegmentDataWriteException var11) {
               throw new RuntimeException(var11);
            }
         }

         var9 = true;
      }

      if (var10 && var9 && !this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getSegmentDrawer().forceAdd((DrawableRemoteSegment)this);
      }

      return var9;
   }

   public int calculateSurrrounding() {
      int var1 = 0;

      for(int var2 = 0; var2 < 6; ++var2) {
         if (this.getSegmentController().existsNeighborSegment(this.pos, var2)) {
            ++var1;
         }
      }

      return var1;
   }

   public abstract void dataChanged(boolean var1);

   public void debugDraw(float var1, float var2, float var3, float var4, float var5, float var6) {
      debugDraw(this.pos, this.getSegmentController().getWorldTransform(), var1, var2, var3, var4, var5, var6);
   }

   public void debugDrawPoint(int var1, int var2, int var3, float var4, float var5, float var6, float var7, float var8, float var9) {
      debugDraw(var1, var2, var3, this.pos, this.getSegmentController().getWorldTransform(), var4, var5, var6, var7, var8, var9);
   }

   public boolean equalsAbsoluteElemPos(Vector3i var1, byte var2, byte var3, byte var4) {
      return var1.equals(this.pos.x + var2, this.pos.y + var3, this.pos.z + var4);
   }

   public Vector3i getAbsoluteElemPos(byte var1, byte var2, byte var3, Vector3i var4) {
      var4.x = this.pos.x + var1;
      var4.y = this.pos.y + var2;
      var4.z = this.pos.z + var3;
      return var4;
   }

   public Vector3i getAbsoluteElemPos(int var1, Vector3i var2) {
      byte var3 = SegmentData.getPosXFromIndex(var1);
      byte var4 = SegmentData.getPosYFromIndex(var1);
      byte var5 = SegmentData.getPosZFromIndex(var1);
      var2.x = this.pos.x + var3;
      var2.y = this.pos.y + var4;
      var2.z = this.pos.z + var5;
      return var2;
   }

   public long getAbsoluteIndex(int var1) {
      byte var2 = SegmentData.getPosXFromIndex(var1);
      byte var3 = SegmentData.getPosYFromIndex(var1);
      byte var4 = SegmentData.getPosZFromIndex(var1);
      return ElementCollection.getIndex(this.pos.x + var2, this.pos.y + var3, this.pos.z + var4);
   }

   public Vector3f getAbsoluteElemPos(byte var1, byte var2, byte var3, Vector3f var4) {
      var4.x = (float)(this.pos.x + var1);
      var4.y = (float)(this.pos.y + var2);
      var4.z = (float)(this.pos.z + var3);
      return var4;
   }

   public Vector3i getAbsoluteElemPos(Vector3b var1, Vector3i var2) {
      return this.getAbsoluteElemPos(var1.x, var1.y, var1.z, var2);
   }

   public long getAbsoluteIndex(byte var1, byte var2, byte var3) {
      int var4 = this.pos.x + var1;
      int var5 = this.pos.y + var2;
      int var6 = this.pos.z + var3;
      return ElementCollection.getIndex(var4, var5, var6);
   }

   public long getIndex() {
      return ElementCollection.getIndex(this.pos);
   }

   public Vector3b getNearestIntersectingElementPosition(Vector3f var1, Vector3f var2) {
      StaticObjectIntersection var3;
      (var3 = new StaticObjectIntersection()).getIntersect(var1, var2, this.getSegmentData());
      if (var3.isIntersection()) {
         System.err.println("[SEGMENT] element to delete found at " + var3.nearest);
         return var3.nearest;
      } else {
         return null;
      }
   }

   public Vector3b getNearestIntersectionLocal(Vector3f var1, Vector3f var2) {
      StaticObjectIntersection var3;
      (var3 = new StaticObjectIntersection()).getIntersect(var1, var2, this.segmentData);
      if (var3.isIntersection()) {
         Vector3b var4 = new Vector3b(var3.nearest);
         switch(var3.side) {
         case 0:
            var4.z = (byte)((int)((float)var4.z + 1.0F));
            break;
         case 1:
            var4.z = (byte)((int)((float)var4.z - 1.0F));
            break;
         case 2:
            var4.y = (byte)((int)((float)var4.y + 1.0F));
            break;
         case 3:
            var4.y = (byte)((int)((float)var4.y - 1.0F));
            break;
         case 4:
            var4.x = (byte)((int)((float)var4.x + 1.0F));
            break;
         case 5:
            var4.x = (byte)((int)((float)var4.x - 1.0F));
            break;
         default:
            throw new IllegalArgumentException("unknown side: " + var3.side);
         }

         return var4;
      } else {
         return null;
      }
   }

   public SegmentController getSegmentController() {
      return this.segmentController;
   }

   public final void setSegmentController(SegmentController var1) {
      this.segmentController = var1;
   }

   public SegmentData getSegmentData() {
      return this.segmentData;
   }

   public void setSegmentData(SegmentData var1) {
      this.segmentData = var1;
   }

   public int getSize() {
      return this.size;
   }

   public void setSize(int var1) {
      this.size = var1;
   }

   public byte getVisablility(Vector3b var1) {
      return 63;
   }

   public Vector3f getWorldPosition(Vector3f var1) {
      var1.set((float)this.pos.x, (float)this.pos.y, (float)this.pos.z);
      this.getSegmentController().getWorldTransform().basis.transform(var1);
      var1.add(this.getSegmentController().getWorldTransform().origin);
      return var1;
   }

   public boolean hasBlockInRange(Vector3i var1, Vector3i var2) {
      if (var1.x < this.pos.x && var1.y < this.pos.y && var1.z < this.pos.z && var2.x > this.pos.x + 32 && var2.y > this.pos.y + 32 && var2.z > this.pos.z + 32) {
         return !this.isEmpty();
      } else {
         return this.getSegmentController().getCollisionChecker().hasSegmentPartialAABBBlock(this, var1, var2);
      }
   }

   public int hashCode() {
      return this.pos.hashCode() + this.segmentController.getId();
   }

   public boolean equals(Object var1) {
      return this == (Segment)var1;
   }

   public String toString() {
      return "DATA:" + this.segmentController.getId() + ":s" + this.getSize();
   }

   public boolean isEmpty() {
      return this.size == 0 || this.getSegmentData() == null;
   }

   public boolean removeElement(short var1, Vector3b var2, boolean var3) {
      return this.removeElement(var1, var2.x, var2.y, var2.z, var3);
   }

   public boolean removeElement(byte var1, byte var2, byte var3, boolean var4) {
      return this.removeElement((short)32767, var1, var2, var3, var4);
   }

   public boolean removeElement(short var1, byte var2, byte var3, byte var4, boolean var5) {
      return this.removeElement(var1, SegmentData.getInfoIndex(var2, var3, var4), var5);
   }

   public boolean removeElement(int var1, boolean var2) {
      return this.removeElement((short)32767, var1, var2);
   }

   public boolean removeElement(short var1, int var2, boolean var3) {
      boolean var4 = false;
      if (!this.isEmpty()) {
         var4 = this.getSegmentData().contains(var2);
         if (var1 == 32767 || this.getSegmentData().getType(var2) == var1) {
            byte var9 = SegmentData.getPosXFromIndex(var2);
            byte var5 = SegmentData.getPosYFromIndex(var2);
            byte var6 = SegmentData.getPosZFromIndex(var2);

            try {
               this.getSegmentData().setInfoElement(var9, var5, var6, (short)0, var3, this.getAbsoluteIndex(var2), this.segmentController.getState().getUpdateTime());
            } catch (SegmentDataWriteException var8) {
               SegmentDataWriteException.replaceData(this);

               try {
                  this.getSegmentData().setInfoElement(var9, var5, var6, (short)0, var3, this.getAbsoluteIndex(var2), this.segmentController.getState().getUpdateTime());
               } catch (SegmentDataWriteException var7) {
                  throw new RuntimeException(var7);
               }
            }
         }
      }

      if (this.isEmpty() && this.getSegmentData() != null) {
         this.getSegmentController().getSegmentBuffer().restructBBFast(this.getSegmentData());
         this.getSegmentController().getSegmentProvider().addToFreeSegmentData(this.getSegmentData());
      }

      CollisionObject var10;
      if ((var10 = this.getSegmentController().getPhysicsDataContainer().getObject()) != null) {
         var10.isActive();
         var10.activate(true);
         ((RigidBody)var10).applyGravity();

         assert var10.isActive();
      }

      return var4;
   }

   public boolean removeElement(Vector3b var1, boolean var2) {
      return this.removeElement((short)32767, var1, var2);
   }

   public void setChangedSurround(int var1, int var2, int var3) {
      Segment var4;
      if ((var4 = this.getSegmentController().getSegmentFromCache(this.pos.x + (var1 << 5), this.pos.y + (var2 << 5), this.pos.z + (var3 << 5))) != null) {
         var4.dataChanged(true);
      }

   }

   public void setPos(int var1, int var2, int var3) {
      this.pos.set(var1, var2, var3);
      this.absPos.set(ByteUtil.divUSeg(var1), ByteUtil.divUSeg(var2), ByteUtil.divUSeg(var3));
   }

   public void setPos(Vector3i var1) {
      this.setPos(var1.x, var1.y, var1.z);
   }

   public void setPos(long var1) {
      this.setPos(ElementCollection.getPosX(var1), ElementCollection.getPosY(var1), ElementCollection.getPosZ(var1));
   }

   public void writeToFile() throws IOException {
      BufferedOutputStream var1;
      (var1 = new BufferedOutputStream(new FileOutputStream(new FileExt(DataUtil.dataPath + "save.smseg")))).write(ByteUtil.intToByteArray(this.pos.x));
      var1.write(ByteUtil.intToByteArray(this.pos.y));
      var1.write(ByteUtil.intToByteArray(this.pos.z));
      var1.write(ByteUtil.intToByteArray(this.getSegmentData().getSize()));

      for(byte var2 = 0; var2 < 32; ++var2) {
         for(byte var3 = 0; var3 < 32; ++var3) {
            for(byte var4 = 0; var4 < 32; ++var4) {
               short var5;
               if ((var5 = this.getSegmentData().getType(var4, var3, var2)) != 0) {
                  var1.write(var4);
                  var1.write(var3);
                  var1.write(var2);
                  var1.write(ByteUtil.shortToByteArray(var5));
               }
            }
         }
      }

      var1.flush();
      var1.close();
   }

   public void getSegmentAabb(Segment var1, Transform var2, Vector3f var3, Vector3f var4, Vector3f var5, Vector3f var6, AABBVarSet var7) {
      assert false;

   }

   public void getAabb(Transform var1, Vector3f var2, Vector3f var3) {
      assert false;

   }

   public void getAabbUncached(Transform var1, Vector3f var2, Vector3f var3, boolean var4) {
      assert false;

   }

   public void getAabbIdent(Vector3f var1, Vector3f var2) {
      assert false;

   }
}
