package org.schema.game.common.data.world;

import org.schema.game.common.controller.rules.rules.RuleEntityManager;
import org.schema.game.network.objects.NTRuleInterface;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.TopLevelType;

public interface RuleEntityContainer {
   RuleEntityManager getRuleEntityManager();

   TopLevelType getTopLevelType();

   NTRuleInterface getNetworkObject();

   StateInterface getState();

   boolean isOnServer();
}
