package org.schema.game.common.data.world;

import java.io.IOException;

public class DeserializationException extends IOException {
   private static final long serialVersionUID = 1L;

   public DeserializationException(String var1) {
      super(var1);
   }
}
