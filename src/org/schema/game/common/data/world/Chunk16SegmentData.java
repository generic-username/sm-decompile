package org.schema.game.common.data.world;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PushbackInputStream;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.zip.DataFormatException;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;
import javax.vecmath.Vector3f;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.element.world.FastValidationContainer;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.io.SegmentSerializationBuffers;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.server.data.GameServerState;

public class Chunk16SegmentData implements SegmentDataInterface {
   public static final int lightBlockSize = 39;
   public static final int typeIndexStart = 0;
   public static final int typeIndexEnd = 11;
   public static final int hitpointsIndexStart = 11;
   public static final int hitpointsIndexEnd = 19;
   public static final int activeIndexStart = 19;
   public static final int activeIndexEnd = 20;
   public static final int orientationStart = 20;
   public static final int orientationEnd = 24;
   public static final int blockSize = 3;
   public static final byte ACTIVE_BIT = 8;
   public static final byte ACTIVE_BIT_HP = 16;
   public static final byte[] typeMap = new byte[24576];
   public static final int maxHp = 256;
   public static final int maxHpHalf = 128;
   public static final byte[] hpMap = new byte[768];
   public static final byte[] hpMapHalf = new byte[384];
   public static final int MAX_ORIENT = 16;
   public static final int FULL_ORIENT = 24;
   public static final byte[] orientMap = new byte[48];
   public static final int SEG = 16;
   public static final int SEG_HALF = 8;
   public static final float SEGf = 16.0F;
   public static final byte ANTI_BYTE = -16;
   public static final int SEG_MINUS_ONE = 15;
   public static final int SEG_TIMES_SEG = 256;
   public static final int SEG_TIMES_SEG_TIMES_SEG = 4096;
   public static final int BLOCK_COUNT = 4096;
   public static final int TOTAL_SIZE = 12288;
   public static final int TOTAL_SIZE_LIGHT = 159744;
   public static final int t = 255;
   public static final int PIECE_ADDED = 0;
   public static final int PIECE_REMOVED = 1;
   public static final int PIECE_CHANGED = 2;
   public static final int PIECE_UNCHANGED = 3;
   public static final int PIECE_ACTIVE_CHANGED = 4;
   public long lastChanged;
   static final long yDelim = 65536L;
   static final long zDelim = 4294967296L;
   private static final int MASK = 255;
   private static final int TYPE_SECOND_MASK = 7;
   private static final int[] lookUpSecType = createLookup();
   public static final int SHIFT_ = 8;
   public static final Vector3i SHIFT = new Vector3i(8, 8, 8);
   public final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
   private final Vector3b min = new Vector3b();
   private final Vector3b max = new Vector3b();
   private byte[] data = new byte[12288];
   private int size;
   private boolean revalidating;
   private boolean blockAddedForced;
   private FastValidationContainer fastValidationIdex;
   public final Vector3i segmentPos = new Vector3i();
   public static final byte CHUNK16VERSION = 2;

   public Chunk16SegmentData() {
      this.resetBB();
   }

   public static float byteArrayToFloat(byte[] var0) {
      int var1 = 0;
      int var2 = 0;

      for(int var3 = 3; var3 >= 0; --var3) {
         var1 |= (var0[var2] & 255) << (var3 << 3);
         ++var2;
      }

      return Float.intBitsToFloat(var1);
   }

   public static byte[] floatToByteArray(float var0) {
      return intToByteArray(Float.floatToRawIntBits(var0));
   }

   public static Vector3b getPositionFromIndex(int var0, Vector3b var1) {
      int var2 = (var0 /= 3) / 256;
      int var3 = (var0 -= var2 << 8) / 16;
      var0 -= var3 << 4;
      var1.set((byte)var0, (byte)var3, (byte)var2);
      return var1;
   }

   public static Vector3f getPositionFromIndexWithShift(int var0, Vector3i var1, Vector3f var2) {
      int var3 = (var0 /= 3) >> 8 & 15;
      int var4 = var0 >> 4 & 15;
      int var5 = var0 & 15;

      assert checkIndex(var5, var4, var3, var0) : var5 + ", " + var4 + ", " + var3;

      var2.set((float)(var1.x + var5 - 8), (float)(var1.y + var4 - 8), (float)(var1.z + var3 - 8));
      return var2;
   }

   private static boolean checkIndex(int var0, int var1, int var2, int var3) {
      int var4 = var3 / 256;
      int var5 = (var3 -= var4 << 8) / 16;
      var3 -= var5 << 4;
      return var0 == var3 && var1 == var5 && var2 == var4;
   }

   public static byte[] intToByteArray(int var0) {
      byte[] var1 = new byte[4];

      for(int var2 = 0; var2 < 4; ++var2) {
         int var3 = var1.length - 1 - var2 << 3;
         var1[var2] = (byte)(var0 >>> var3);
      }

      return var1;
   }

   public static void main(String[] var0) {
      byte[] var6;
      byte[] var10000 = var6 = new byte[3];
      var10000[0] = (byte)(var10000[0] | 8);

      assert (var6[0] & 8) != 0 : var6[0] + "; true";

      var6[0] &= -9;

      assert (var6[0] & 8) == 0;

      var6[0] = (byte)(var6[0] | 8);

      assert (var6[0] & 8) != 0;

      Random var1 = new Random();

      short var2;
      int var4;
      short var7;
      short var8;
      for(var2 = 0; var2 < 256; ++var2) {
         var6[0] = (byte)var1.nextInt(256);
         var6[1] = (byte)var1.nextInt(256);
         var6[2] = (byte)var1.nextInt(256);
         var4 = var2 * 3;
         var6[0] = (byte)(var6[0] & ~hpMap[hpMap.length - 3]);
         var6[1] = (byte)(var6[1] & ~hpMap[hpMap.length - 2]);
         var6[0] |= hpMap[var4];
         var6[1] |= hpMap[var4 + 1];
         var4 = var6[1] & 255;
         int var5 = var6[0] & 255;
         var7 = (short)(((var4 & hpMap[hpMap.length - 2]) >> 3) + ((var5 & hpMap[hpMap.length - 3]) << 5));
         var8 = (short)ByteUtil.extractInt(ByteUtil.intRead3ByteArray(var6, 0), 11, 19, new Object());
         System.err.println(var2 + " -> " + var7 + "; ; " + var8);

         assert var8 == var2;

         assert var8 == var7;
      }

      for(var2 = 0; var2 < 16; ++var2) {
         var6[0] = (byte)var1.nextInt(256);
         var6[1] = (byte)var1.nextInt(256);
         var6[2] = (byte)var1.nextInt(256);
         var4 = var2 * 3;
         var6[0] = (byte)(var6[0] & ~orientMap[orientMap.length - 3]);
         var6[1] = (byte)(var6[1] & ~orientMap[orientMap.length - 2]);
         var6[0] |= orientMap[var4];
         var6[1] |= orientMap[var4 + 1];
         var7 = (short)((var6[0] & 255 & orientMap[orientMap.length - 3]) >> 4);
         var8 = (short)ByteUtil.extractInt(ByteUtil.intRead3ByteArray(var6, 0), 20, 24, new Object());
         System.err.println(var2 + " -> " + var7 + "; ; " + var8);

         assert var8 == var2;

         assert var8 == var7;
      }

   }

   public static void setActive(int var0, boolean var1, byte[] var2) {
      if (!var1) {
         var2[var0] = (byte)(var2[var0] | 8);
      } else {
         var2[var0] &= -9;
      }
   }

   public static void setActiveByHp(int var0, boolean var1, byte[] var2) {
      if (!var1) {
         var2[var0] = (byte)(var2[var0] | 16);
      } else {
         var2[var0] &= -17;
      }
   }

   public static void setHitpoints(int var0, short var1, short var2, byte[] var3) {
      assert var1 >= 0 && var1 < 512;

      int var4 = var1 * 3;
      var3[var0] = (byte)(var3[var0] & ~hpMap[hpMap.length - 3]);
      var3[var0 + 1] = (byte)(var3[var0 + 1] & ~hpMap[hpMap.length - 2]);
      var3[var0] |= hpMap[var4];
      var3[var0 + 1] |= hpMap[var4 + 1];
   }

   public static void setOrientation(int var0, byte var1, byte[] var2) {
      assert var1 >= 0 && var1 < 16 : "NOT A SIDE INDEX";

      var2[var0] = (byte)(var2[var0] & ~orientMap[orientMap.length - 3]);
      var2[var0] |= orientMap[var1 * 3];
   }

   public static void setType(int var0, short var1, byte[] var2) {
      if (var1 < 2048) {
         int var3 = var1 * 3;
         var2[var0 + 2] = typeMap[var3 + 2];
         var2[var0 + 1] = (byte)(var2[var0 + 1] - (var2[var0 + 1] & 7));
         var2[var0 + 1] |= typeMap[var3 + 1];
      } else {
         System.err.println("ERROR: Type is invalied. must be < 4096 but was: " + var1);
      }
   }

   public static boolean valid(byte var0, byte var1, byte var2) {
      return ((var0 | var1 | var2) & -16) == 0;
   }

   public static boolean valid(int var0, int var1, int var2) {
      return ((var0 | var1 | var2) & -16) == 0;
   }

   public static boolean allNeighborsInside(byte var0, byte var1, byte var2) {
      return var0 < 15 && var1 < 15 && var2 < 15 && var0 > 0 && var1 > 0 && var2 > 0;
   }

   public static boolean allNeighborsInside(int var0, int var1, int var2) {
      return var0 < 15 && var1 < 15 && var2 < 15 && var0 > 0 && var1 > 0 && var2 > 0;
   }

   public static int getInfoIndex(byte var0, byte var1, byte var2) {
      return 3 * ((var2 << 8) + (var1 << 4) + var0);
   }

   public static int getInfoIndex(int var0, int var1, int var2) {
      return 3 * ((var2 << 8) + (var1 << 4) + var0);
   }

   public static int getInfoIndex(Vector3b var0) {
      return getInfoIndex(var0.x, var0.y, var0.z);
   }

   public static int getInfoIndex(Vector3i var0) {
      return getInfoIndex(var0.x, var0.y, var0.z);
   }

   public static int[] createLookup() {
      int[] var0 = new int[256];

      for(int var1 = -128; var1 <= 127; ++var1) {
         int var2 = var1 & 255;
         var0[var2] = (var2 & 7) << 8;
      }

      return var0;
   }

   public int arraySize() {
      return this.data.length;
   }

   public boolean checkEmpty() {
      for(int var1 = 0; var1 < this.data.length; var1 += 3) {
         if (this.getType(var1) != 0) {
            return false;
         }
      }

      return true;
   }

   public boolean contains(byte var1, byte var2, byte var3) {
      return valid(var1, var2, var3) ? this.containsUnsave(var1, var2, var3) : false;
   }

   public boolean contains(int var1) {
      return this.getType(var1) != 0;
   }

   public boolean contains(Vector3b var1) {
      return this.contains(var1.x, var1.y, var1.z);
   }

   public String toString() {
      return "(DATA: CHUNK16)";
   }

   public boolean containsFast(int var1) {
      return this.data[var1 + 2] != 0 || (this.data[var1 + 1] & 255 & 7) != 0;
   }

   public boolean containsFast(Vector3b var1) {
      return this.containsFast(getInfoIndex(var1));
   }

   public boolean containsFast(Vector3i var1) {
      return this.containsFast(getInfoIndex(var1));
   }

   public boolean containsUnblended(byte var1, byte var2, byte var3) {
      if (valid(var1, var2, var3)) {
         short var4;
         return (var4 = this.getType(var1, var2, var3)) != 0 && !ElementKeyMap.getInfo(var4).isBlended();
      } else {
         return false;
      }
   }

   public boolean containsUnblended(Vector3b var1) {
      return this.containsUnblended(var1.x, var1.y, var1.z);
   }

   public boolean containsUnsave(byte var1, byte var2, byte var3) {
      return this.containsFast(getInfoIndex(var1, var2, var3));
   }

   public boolean containsUnsave(int var1, int var2, int var3) {
      return this.containsFast(getInfoIndex(var1, var2, var3));
   }

   public boolean containsUnsave(int var1) {
      return this.getType(var1) != 0;
   }

   public void deserialize(DataInput var1, long var2) throws IOException {
      this.rwl.writeLock().lock();

      try {
         this.reset(var2);
         var1.readFully(this.data);
         this.setNeedsRevalidate(true);
      } finally {
         this.rwl.writeLock().unlock();
      }

   }

   public int[] getAsIntBuffer() {
      throw new IllegalArgumentException("Incompatible SegmentData Version");
   }

   public byte[] getAsOldByteBuffer() {
      return this.data;
   }

   public void migrateTo(int var1, SegmentDataInterface var2) {
      assert false;

   }

   public void setType(int var1, short var2) {
      setType(var1, var2, this.data);
   }

   public void setActive(int var1, boolean var2) {
      byte[] var10000;
      if (!var2) {
         var10000 = this.data;
         var10000[var1] = (byte)(var10000[var1] | 8);
      } else {
         var10000 = this.data;
         var10000[var1] &= -9;
      }

      assert var2 == this.isActive(var1) : var2 + "; " + this.isActive(var1);

   }

   public void setActiveByHp(int var1, boolean var2) {
      byte[] var10000;
      if (!var2) {
         var10000 = this.data;
         var10000[var1] = (byte)(var10000[var1] | 16);
      } else {
         var10000 = this.data;
         var10000[var1] &= -17;
      }

      assert var2 == this.isActiveByHp(var1) : var2 + "; " + this.isActiveByHp(var1);

   }

   public void setOrientation(int var1, byte var2) {
      assert var2 >= 0 && var2 < 16 : "NOT A SIDE INDEX";

      byte[] var10000 = this.data;
      var10000[var1] = (byte)(var10000[var1] & ~orientMap[orientMap.length - 3]);
      var10000 = this.data;
      var10000[var1] |= orientMap[var2 * 3];

      assert var2 == this.getOrientation(var1) : "failed orientation coding: " + var2 + " != result " + this.getOrientation(var1);

   }

   public short getType(int var1) {
      return (short)((this.data[var1 + 2] & 255) + lookUpSecType[this.data[var1 + 1] & 255]);
   }

   public short getHitpointsByte(int var1) {
      int var2 = this.data[var1 + 1] & 255;
      var1 = this.data[var1] & 255;
      return (short)(((var2 & hpMap[hpMap.length - 2]) >> 3) + ((var1 & hpMap[hpMap.length - 3]) << 5));
   }

   public boolean isActive(int var1) {
      return (this.data[var1] & 8) == 0;
   }

   public boolean isActiveByHp(int var1) {
      return (this.data[var1] & 16) == 0;
   }

   public byte getOrientation(int var1) {
      return (byte)((this.data[var1] & 255 & orientMap[orientMap.length - 3]) >> 4);
   }

   public Segment getSegment() {
      return null;
   }

   public SegmentController getSegmentController() {
      return null;
   }

   public void resetFast() {
      this.setSize(0);
      Arrays.fill(this.data, (byte)0);
      this.setSize(0);
      this.resetBB();
      this.setSize(0);
   }

   public Vector3b getMax() {
      return this.max;
   }

   public Vector3b getMin() {
      return this.min;
   }

   public byte[] getSegmentPieceData(int var1, byte[] var2) {
      int var3 = 0;

      for(int var4 = var1; var4 < var1 + 3; ++var4) {
         var2[var3++] = this.data[var4];
      }

      return var2;
   }

   public int getSize() {
      return this.size;
   }

   public void setSize(int var1) {
      this.size = var1;
   }

   public short getType(byte var1, byte var2, byte var3) {
      int var4 = getInfoIndex(var1, var2, var3);
      return this.getType(var4);
   }

   public short getType(Vector3b var1) {
      return this.getType(var1.x, var1.y, var1.z);
   }

   public byte getOrientation(Vector3b var1) {
      return this.getOrientation(var1.x, var1.y, var1.z);
   }

   public byte getOrientation(byte var1, byte var2, byte var3) {
      return this.getOrientation(getInfoIndex(var1, var2, var3));
   }

   public boolean isRevalidating() {
      return this.revalidating;
   }

   public boolean neighbors(byte var1, byte var2, byte var3) {
      if (!allNeighborsInside(var1, var2, var3)) {
         if (this.contains((byte)(var1 - 1), var2, var3)) {
            return true;
         }

         if (this.contains((byte)(var1 + 1), var2, var3)) {
            return true;
         }

         if (this.contains(var1, (byte)(var2 - 1), var3)) {
            return true;
         }

         if (this.contains(var1, (byte)(var2 + 1), var3)) {
            return true;
         }

         if (this.contains(var1, var2, (byte)(var3 - 1))) {
            return true;
         }

         if (this.contains(var1, var2, (byte)(var3 + 1))) {
            return true;
         }
      } else {
         if (this.containsUnsave((byte)(var1 - 1), var2, var3)) {
            return true;
         }

         if (this.containsUnsave((byte)(var1 + 1), var2, var3)) {
            return true;
         }

         if (this.containsUnsave(var1, (byte)(var2 - 1), var3)) {
            return true;
         }

         if (this.containsUnsave(var1, (byte)(var2 + 1), var3)) {
            return true;
         }

         if (this.containsUnsave(var1, var2, (byte)(var3 - 1))) {
            return true;
         }

         if (this.containsUnsave(var1, var2, (byte)(var3 + 1))) {
            return true;
         }
      }

      return false;
   }

   private void onAddingElementUnsynched(int var1, byte var2, byte var3, byte var4, short var5, boolean var6, boolean var7, long var8, long var10) {
      int var10000 = this.size;
      this.incSize();
      this.updateBB(var2, var3, var4, var6, true);
   }

   public void onRemovingElement(int var1, byte var2, byte var3, byte var4, short var5, boolean var6, byte var7, boolean var8, boolean var9, long var10) {
      if (var9) {
         this.rwl.writeLock().lock();
      }

      try {
         var1 = this.getSize();
         this.setSize(var1 - 1);
         this.updateBB(var2, var3, var4, var6, false);
      } finally {
         if (var9) {
            this.rwl.writeLock().unlock();
         }

      }

   }

   public void removeInfoElement(byte var1, byte var2, byte var3) {
      setType(getInfoIndex(var1, var2, var3), (short)0, this.data);
   }

   public void removeInfoElement(Vector3b var1) {
      this.removeInfoElement(var1.x, var1.y, var1.z);
   }

   public void reset(long var1) {
      this.rwl.writeLock().lock();

      try {
         Arrays.fill(this.data, (byte)0);
         this.setSize(0);
         this.resetBB();
      } finally {
         this.rwl.writeLock().unlock();
      }

   }

   public void resetBB() {
      assert this.size == 0;

      this.max.set((byte)-128, (byte)-128, (byte)-128);
      this.min.set((byte)127, (byte)127, (byte)127);
   }

   private void unvalidate(byte var1, byte var2, byte var3, int var4, long var5) {
      short var7 = this.getType(var4);
      byte var8 = this.getOrientation(var4);
      if (var7 != 0) {
         this.onRemovingElement(var4, var1, var2, var3, var7, false, var8, this.isActive(var4), false, var5);
      }

   }

   public void unvalidateData(long var1) {
      this.rwl.writeLock().lock();

      try {
         this.revalidating = true;
         int var3 = 0;

         for(byte var4 = 0; var4 < 16; ++var4) {
            for(byte var5 = 0; var5 < 16; ++var5) {
               for(byte var6 = 0; var6 < 16; ++var6) {
                  this.unvalidate(var6, var5, var4, var3, var1);
                  var3 += 3;
               }
            }
         }

         this.revalidating = false;
      } finally {
         this.rwl.writeLock().unlock();
      }
   }

   public boolean revalidateSuccess() {
      for(byte var1 = 0; var1 < 16; ++var1) {
         for(byte var2 = 0; var2 < 16; ++var2) {
            for(byte var3 = 0; var3 < 16; ++var3) {
               short var4;
               if ((var4 = this.getType(var3, var2, var1)) != 0 && !ElementKeyMap.exists(var4)) {
                  System.err.println("FAILED: " + var4 + "; " + var3 + ", " + var2 + ", " + var1);
                  return false;
               }
            }
         }
      }

      return true;
   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.write(this.data);
   }

   public void setHitpointsByte(int var1, int var2) {
      assert var2 >= 0 && var2 < 256 : var2;

      this.getType(var1);
      int var3 = var2 * 3;
      byte[] var10000 = this.data;
      var10000[var1] = (byte)(var10000[var1] & ~hpMap[hpMap.length - 3]);
      var10000 = this.data;
      var10000[var1 + 1] = (byte)(var10000[var1 + 1] & ~hpMap[hpMap.length - 2]);
      var10000 = this.data;
      var10000[var1] |= hpMap[var3];
      var10000 = this.data;
      var10000[var1 + 1] |= hpMap[var3 + 1];

      assert this.getHitpointsByte(var1) == var2;

   }

   public void setInfoElement(byte var1, byte var2, byte var3, short var4, byte var5, byte var6, boolean var7, long var8, long var10) {
      this.rwl.writeLock().lock();

      try {
         this.setInfoElementUnsynched(var1, var2, var3, var4, var5, var6, var7, var8, var10);
      } finally {
         this.rwl.writeLock().unlock();
      }

   }

   public void setInfoElement(byte var1, byte var2, byte var3, short var4, boolean var5, long var6, long var8) {
      this.setInfoElement(var1, var2, var3, var4, (byte)-1, (byte)-1, var5, var6, var8);
   }

   public void setInfoElement(Vector3b var1, short var2, boolean var3, long var4, long var6) {
      this.setInfoElement(var1.x, var1.y, var1.z, var2, var3, var4, var6);
   }

   public void setInfoElement(Vector3b var1, short var2, byte var3, byte var4, boolean var5, long var6, long var8) {
      this.setInfoElement(var1.x, var1.y, var1.z, var2, var3, var4, var5, var6, var8);
   }

   public void setInfoElementForcedAddUnsynched(byte var1, byte var2, byte var3, short var4, boolean var5) {
      this.setInfoElementForcedAddUnsynched(var1, var2, var3, var4, (byte)-1, ElementInformation.activateOnPlacement(var4), var5);
   }

   public void setInfoElementForcedAddUnsynched(byte var1, byte var2, byte var3, short var4, byte var5, byte var6, boolean var7) {
      if (var4 != 0) {
         int var8 = getInfoIndex(var1, var2, var3);
         this.setType(var8, var4);
         if (var5 >= 0) {
            this.setOrientation(var8, var5);
         }

         if (var6 >= 0) {
            this.setActive(var8, var6 != 0);
         }

         this.setBlockAddedForced(true);
         this.setHitpointsByte(var8, ElementKeyMap.getInfoFast(var4).getMaxHitPointsByte());
      }
   }

   public void setInfoElementUnsynched(byte var1, byte var2, byte var3, short var4, boolean var5, long var6, long var8) {
      this.setInfoElementUnsynched(var1, var2, var3, var4, (byte)-1, (byte)-1, var5, var6, var8);
   }

   public void setInfoElementUnsynched(byte var1, byte var2, byte var3, short var4, byte var5, byte var6, boolean var7, long var8, long var10) {
      int var12 = getInfoIndex(var1, var2, var3);
      short var13 = this.getType(var12);
      byte var14 = this.getOrientation(var12);
      setType(var12, var4, this.data);
      if (var5 >= 0) {
         this.setOrientation(var12, var5);
      }

      boolean var15 = this.isActive(var12);
      if (var6 >= 0) {
         this.setActive(var12, var6 != 0);
      }

      if (var4 == 0) {
         this.setActive(var12, false);
         this.setOrientation(var12, (byte)0);
         if (var13 != 0) {
            this.onRemovingElement(var12, var1, var2, var3, var13, var7, var14, var15, false, var10);
            return;
         }
      } else if (var13 == 0) {
         this.onAddingElementUnsynched(var12, var1, var2, var3, var4, var7, true, var8, var10);
         this.setHitpointsByte(var12, 127);
      }

   }

   public void setInfoElementUnsynched(Vector3b var1, short var2, boolean var3, long var4, long var6) {
      this.setInfoElementUnsynched(var1.x, var1.y, var1.z, var2, var3, var4, var6);
   }

   public void setNeedsRevalidate(boolean var1) {
   }

   public void incSize() {
      ++this.size;
   }

   private void updateBB(byte var1, byte var2, byte var3, boolean var4, boolean var5) {
      if (var5) {
         if (var1 >= this.max.x) {
            this.max.x = (byte)(var1 + 1);
         }

         if (var2 >= this.max.y) {
            this.max.y = (byte)(var2 + 1);
         }

         if (var3 >= this.max.z) {
            this.max.z = (byte)(var3 + 1);
         }

         if (var1 < this.min.x) {
            this.min.x = var1;
         }

         if (var2 < this.min.y) {
            this.min.y = var2;
         }

         if (var3 < this.min.z) {
            this.min.z = var3;
         }
      }

   }

   public void writeSingle(int var1, DataOutputStream var2) throws IOException {
      var2.write(this.data, var1, 3);
   }

   public void readSingle(int var1, DataInputStream var2) throws IOException {
      var2.read(this.data, var1, 3);
   }

   public boolean isBlockAddedForced() {
      return this.blockAddedForced;
   }

   public void setBlockAddedForced(boolean var1) {
      this.blockAddedForced = var1;
   }

   public boolean isBBValid() {
      return this.min.x <= this.max.x && this.min.y <= this.max.y && this.min.z <= this.max.z;
   }

   public void resetFastValidationIndex() {
      if (this.fastValidationIdex != null) {
         this.fastValidationIdex.clear();
      }

      this.fastValidationIdex = null;
   }

   public void createFastValidationIndex(FastValidationContainer var1) {
   }

   public Vector3i getSegmentPos() {
      return this.segmentPos;
   }

   public boolean deserialize(DataInputStream var1, int var2, boolean var3, boolean var4, long var5) throws IOException, DeserializationException {
      int var31 = 0;
      PushbackInputStream var25 = new PushbackInputStream(var1, 2);
      byte[] var7 = new byte[2];
      var25.read(var7);
      var25.unread(var7);
      DataInputStream var8 = new DataInputStream(var25);
      if (var7[0] == 31 && var7[1] == -117) {
         System.err.println("[SEGMENT] WARNING: Reading an old format segment! " + this.getSegmentController() + "; " + this.segmentPos);
         this.oldDeserialize(var8, var2, var3, var5);
         return true;
      } else {
         if (var7[0] < 0) {
            var31 = -var8.readByte();
         }

         var1 = var8;
         long var9 = var8.readLong();
         int var32 = var8.readInt();
         int var6 = var8.readInt();
         int var36 = var8.readInt();
         if (var3) {
            this.segmentPos.set(var32, var6, var36);
         }

         assert var3 || this.segmentPos.x == var32 && this.segmentPos.y == var6 && this.segmentPos.z == var36 : " deserialized " + var32 + ", " + var6 + ", " + var36 + "; toSerialize " + this.segmentPos + " on " + this.getSegmentController();

         byte var29;
         if ((var29 = var8.readByte()) == 1) {
            SegmentSerializationBuffers var27 = SegmentSerializationBuffers.get();

            try {
               Inflater var30 = var27.inflater;
               byte[] var33 = var27.SEGMENT_BUFFER;
               byte[] var34 = var27.SEGMENT_BYTE_FORMAT_BUFFER;
               var36 = var1.readInt();
               if (this.getSegmentController() != null && !this.getSegmentController().isOnServer()) {
                  GameClientState.dataReceived = GameClientState.dataReceived + (long)var36;
               } else {
                  GameServerState.dataReceived = GameServerState.dataReceived + (long)var36;
               }

               assert var36 <= var33.length : var36 + "/" + var33.length;

               int var26;
               if ((var26 = var1.read(var33, 0, var36)) != var36) {
                  throw new DeserializationException(var26 + "; " + var36 + "; " + var33.length + "; " + this + "; ");
               }

               var30.reset();
               var30.setInput(var33, 0, var36);

               try {
                  if (var31 >= 2) {
                     try {
                        var32 = this.inflate(var30, var34);
                     } catch (SegmentInflaterException var18) {
                        System.err.println("[INFLATER] Exception: " + this.getSegmentController().getState() + " size received: " + var36 + ": " + var18.inflate + "/" + var18.shouldBeInflate + " for " + this.getSegmentController() + " pos " + this.segmentPos);
                        var18.printStackTrace();
                        var32 = 0;
                     }
                  } else {
                     SegmentDataInterface var28;
                     if ((var28 = (SegmentDataInterface)var27.dummies.get(var31)) == null) {
                        System.err.println("[INFLATER] no dummy for version " + var31);
                     }

                     try {
                        var32 = var28.inflate(var30, var34);
                     } catch (SegmentInflaterException var21) {
                        if (this.getSegmentController() != null) {
                           System.err.println("[INFLATER] Exception: " + this.getSegmentController().getState() + " size received: " + var36 + ": " + var21.inflate + "/" + var21.shouldBeInflate + " for " + this.getSegmentController() + " pos " + this.segmentPos);
                        }

                        var21.printStackTrace();
                        var32 = 0;
                     } catch (SegmentDataWriteException var22) {
                        var22.printStackTrace();
                        throw new RuntimeException("this should be never be thrown as migration should always be toa normal segment data", var22);
                     }

                     for(; var31 < 2; ++var31) {
                        if (var31 <= 0) {
                           SegmentDataInterface var35 = (SegmentDataInterface)var27.dummies.get(var31 + 1);
                           var28.migrateTo(var31, var35);

                           try {
                              var28.resetFast();
                           } catch (SegmentDataWriteException var20) {
                              var8.close();
                              throw new RuntimeException(var20);
                           }

                           var28 = var35;
                        } else {
                           assert var31 == 1;

                           var28.migrateTo(var31, this);

                           try {
                              var28.resetFast();
                           } catch (SegmentDataWriteException var19) {
                              var8.close();
                              throw new RuntimeException(var19);
                           }
                        }
                     }
                  }

                  if (var32 == 0) {
                     System.err.println("WARNING: INFLATED BYTES 0: " + var30.needsInput() + " " + var30.needsDictionary());
                  }
               } catch (DataFormatException var23) {
                  var23.printStackTrace();
               }
            } finally {
               SegmentSerializationBuffers.free(var27);
            }
         } else {
            assert var29 == 2 : var29 + "/2: " + var32 + ", " + var6 + ", " + var36 + "; byte size: " + var2;
         }

         this.lastChanged = var9;
         return var31 < 2;
      }
   }

   public void oldDeserialize(DataInputStream var1, int var2, boolean var3, long var4) throws IOException, DeserializationException {
      if (var2 < 0) {
         var1 = new DataInputStream(new GZIPInputStream(var1));
      } else {
         var1 = new DataInputStream(new GZIPInputStream(var1, var2));
      }

      long var7 = var1.readLong();
      int var6 = var1.readInt();
      int var9 = var1.readInt();
      int var10 = var1.readInt();
      if (var3) {
         this.segmentPos.set(var6, var9, var10);
      }

      assert var3 || this.segmentPos.x == var6 && this.segmentPos.y == var9 && this.segmentPos.z == var10 : " deserialized " + var6 + ", " + var9 + ", " + var10 + "; toSerialize " + this.segmentPos;

      byte var13;
      if ((var13 = var1.readByte()) == 1) {
         this.deserialize(var1, var4);
      } else {
         assert var13 == 2;
      }

      this.lastChanged = var7;

      try {
         int var12;
         if ((var12 = var1.read()) != -1) {
            throw new DeserializationException("EoF not reached: " + var12 + " - size given: " + var2);
         }
      } catch (IOException var11) {
         var11.printStackTrace();
         throw new DeserializationException("[WARNING][DESERIALIZE] " + this.getSegmentController().getState() + ": " + this.getSegmentController() + ": " + this.segmentPos + ": " + var11.getMessage());
      }
   }

   public boolean isIntDataArray() {
      return false;
   }

   public int inflate(Inflater var1, byte[] var2) throws SegmentInflaterException, DataFormatException {
      int var3;
      if ((var3 = var1.inflate(this.data)) != this.data.length) {
         throw new SegmentInflaterException(var3, this.data.length);
      } else {
         return var3;
      }
   }

   public SegmentData doBitmapCompressionCheck(RemoteSegment var1) {
      return null;
   }

   public void setDataAt(int var1, int var2) throws SegmentDataWriteException {
   }

   static {
      int var0 = typeMap.length / 3;

      short var1;
      int var2;
      for(var1 = 0; var1 < var0; ++var1) {
         var2 = var1 * 3;
         ByteUtil.intWrite3ByteArray(ByteUtil.putRangedBitsOntoInt(ByteUtil.intRead3ByteArray(typeMap, var2), var1, 0, 11, (Object)null), typeMap, var2, (Object)null);
      }

      for(var1 = 0; var1 < 256; ++var1) {
         var2 = var1 * 3;
         ByteUtil.intWrite3ByteArray(ByteUtil.putRangedBitsOntoInt(ByteUtil.intRead3ByteArray(hpMap, var2), var1, 11, 19, (Object)null), hpMap, var2, (Object)null);
      }

      for(var1 = 0; var1 < 128; ++var1) {
         var2 = var1 * 3;
         ByteUtil.intWrite3ByteArray(ByteUtil.putRangedBitsOntoInt(ByteUtil.intRead3ByteArray(hpMapHalf, var2), var1, 11, 18, (Object)null), hpMapHalf, var2, (Object)null);
      }

      for(var1 = 0; var1 < 16; ++var1) {
         var2 = var1 * 3;
         ByteUtil.intWrite3ByteArray(ByteUtil.putRangedBitsOntoInt(ByteUtil.intRead3ByteArray(orientMap, var2), var1, 20, 24, (Object)null), orientMap, var2, (Object)null);
      }

   }
}
