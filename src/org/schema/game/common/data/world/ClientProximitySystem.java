package org.schema.game.common.data.world;

import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;

public class ClientProximitySystem {
   public static final int PROXIMITY_SIZE = 1;
   public static final int LEN = 3;
   public static final int LENxLEN = 9;
   public static final int ALEN = 27;
   private Vector3i basePosition = new Vector3i();
   private byte[] systemType = new byte[27];
   private PlayerState player;

   public ClientProximitySystem(PlayerState var1) {
      this.player = var1;
   }

   public void deserialize(DataInputStream var1) throws IOException {
      int var2 = var1.readInt();
      int var3 = var1.readInt();
      int var4 = var1.readInt();
      this.basePosition.set(var2, var3, var4);

      for(var2 = 0; var2 < 27; ++var2) {
         this.systemType[var2] = var1.readByte();
      }

   }

   public Vector3i getBasePosition() {
      return this.basePosition;
   }

   public int getFromAbsoluteSystemPos(Vector3i var1) {
      Vector3i var2 = new Vector3i(this.basePosition.x - var1.x + 1, this.basePosition.y - var1.y + 1, this.basePosition.z - var1.z + 1);

      assert var2.x >= 0 && var2.y >= 0 && var2.z >= 0 : var2 + ": " + var1 + " / " + this.basePosition;

      int var3 = var2.z * 9 + var2.y * 3 + var2.x;

      assert this.getPosFromIndex(var3, new Vector3i()).equals(var1) : this.getPosFromIndex(var3, new Vector3i()) + " / " + var1;

      return var3 >= 0 && var3 < 27 ? var3 : -1;
   }

   public Vector3i getPosFromIndex(int var1, Vector3i var2) {
      var2.set(this.basePosition.x - 1, this.basePosition.y - 1, this.basePosition.z - 1);
      int var3 = var1 / 9;
      int var4 = (var1 - var3 * 9) / 3;
      var1 -= var3 * 9 + var4 * 3;
      var2.add(var1, var4, var3);
      return var2;
   }

   public byte getType(int var1) {
      return this.systemType[var1];
   }

   public void serialize(DataOutput var1) throws IOException {
      var1.writeInt(this.basePosition.x);
      var1.writeInt(this.basePosition.y);
      var1.writeInt(this.basePosition.z);

      for(int var2 = 0; var2 < 27; ++var2) {
         var1.writeByte(this.systemType[var2]);
      }

   }

   public void updateServer() throws IOException {
      GameServerState var1 = (GameServerState)this.player.getState();
      Vector3i var2 = new Vector3i(this.player.getCurrentSector());
      this.basePosition.set(var2);
      var1.getUniverse().updateProximitySectorInformation(var2);
      int var3 = 0;
      var2 = new Vector3i(var2);
      StellarSystem var4 = var1.getUniverse().getStellarSystemFromSecPos(var2);
      this.basePosition.set(var4.getPos());

      for(int var9 = -1; var9 <= 1; ++var9) {
         for(int var5 = -1; var5 <= 1; ++var5) {
            for(int var6 = -1; var6 <= 1; ++var6) {
               var2.set(this.basePosition.x + var6, this.basePosition.y + var5, this.basePosition.z + var9);
               StellarSystem var7 = var1.getUniverse().getStellarSystemFromStellarPos(var2);
               this.systemType[var3] = (byte)var7.getCenterSectorType().ordinal();
               ++var3;
            }
         }
      }

      assert var3 == 27 : var3 + "/27";

      synchronized(this.player.getNetworkObject()) {
         this.player.getNetworkObject().proximitySystem.setChanged(true);
      }
   }
}
