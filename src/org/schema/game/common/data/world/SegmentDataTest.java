package org.schema.game.common.data.world;

import java.util.Random;
import org.junit.Assert;
import org.junit.Test;

public class SegmentDataTest {
   public static final int SEG = 32;
   public static final int TOTAL = 32768;
   public static final int BLOCK = 3;
   private final byte[] data = new byte[98304];
   private final int[] intData = new int['耀'];
   public static final int typeIndexStart = 0;
   public static final int hitpointsIndexStart = 11;
   public static final int activeIndexStart = 18;
   public static final int orientationStart = 19;
   public static final int typeMask;
   public static final int typeMaskNot;
   public static final int hpMask;
   public static final int hpMaskNot;
   public static final int activeMask;
   public static final int activeMaskNot;
   public static final int orientMask;
   public static final int orientMaskNot;

   public int getValue(int var1) {
      var1 *= 3;
      return (this.data[var1] & 255) + ((this.data[var1 + 1] & 255) << 8) + ((this.data[var1 + 2] & 255) << 16);
   }

   public int getType(int var1) {
      return typeMask & this.getValue(var1);
   }

   public void putType(int var1, short var2) {
      this.put(var1, this.getValue(var1) & typeMaskNot | var2);
   }

   public int getHitpoints(int var1) {
      return (hpMask & this.getValue(var1)) >> 11;
   }

   public void putHitpoints(int var1, byte var2) {
      this.put(var1, this.getValue(var1) & hpMaskNot | var2 << 11);
   }

   public boolean getActivation(int var1) {
      return (activeMask & this.getValue(var1)) > 0;
   }

   public void putActivation(int var1, boolean var2) {
      this.put(var1, var2 ? this.getValue(var1) | activeMask : this.getValue(var1) & activeMaskNot);
   }

   public int getOrientation(int var1) {
      return (orientMask & this.getValue(var1)) >> 19;
   }

   public void putOrientation(int var1, byte var2) {
      this.put(var1, this.getValue(var1) & orientMaskNot | var2 << 19);
   }

   public void put(int var1, int var2) {
      var1 *= 3;
      this.data[var1] = (byte)var2;
      this.data[var1 + 1] = (byte)(var2 >> 8);
      this.data[var1 + 2] = (byte)(var2 >> 16);
   }

   public int getValueInt(int var1) {
      return this.intData[var1];
   }

   public int getTypeInt(int var1) {
      return typeMask & this.intData[var1];
   }

   public void putIntTypeInt(int var1, short var2) {
      this.intData[var1] = this.intData[var1] & typeMaskNot | var2;
   }

   public int getHitpointsInt(int var1) {
      return (hpMask & this.intData[var1]) >> 11;
   }

   public void putIntHitpointsInt(int var1, byte var2) {
      this.intData[var1] = this.intData[var1] & hpMaskNot | var2 << 11;
   }

   public boolean getActivationInt(int var1) {
      return (activeMask & this.intData[var1]) > 0;
   }

   public void putIntActivationInt(int var1, boolean var2) {
      this.intData[var1] = var2 ? this.intData[var1] | activeMask : this.intData[var1] & activeMaskNot;
   }

   public int getOrientationInt(int var1) {
      return (orientMask & this.intData[var1]) >> 19;
   }

   public void putIntOrientationInt(int var1, byte var2) {
      this.intData[var1] = this.intData[var1] & orientMaskNot | var2 << 19;
   }

   @Test
   public void test() {
      short[] var1 = new short['耀'];
      byte[] var2 = new byte['耀'];
      boolean[] var3 = new boolean['耀'];
      byte[] var4 = new byte['耀'];
      long var5 = System.currentTimeMillis();

      int var9;
      byte var13;
      for(int var7 = 0; var7 < 1000; ++var7) {
         Random var8 = new Random((long)var7);

         for(var9 = 0; var9 < 32768; ++var9) {
            short var10 = (short)var8.nextInt(2048);
            byte var11 = (byte)var8.nextInt(128);
            boolean var12 = var8.nextBoolean();
            var13 = (byte)var8.nextInt(32);
            this.putType(var9, var10);
            this.putHitpoints(var9, var11);
            this.putActivation(var9, var12);
            this.putOrientation(var9, var13);
            var1[var9] = var10;
            var2[var9] = var11;
            var3[var9] = var12;
            var4[var9] = var13;
         }

         for(var9 = 0; var9 < 32768; ++var9) {
            Assert.assertEquals((long)this.getType(var9), (long)var1[var9]);
            Assert.assertEquals((long)this.getHitpoints(var9), (long)var2[var9]);
            Assert.assertEquals(this.getActivation(var9), var3[var9]);
            Assert.assertEquals((long)this.getOrientation(var9), (long)var4[var9]);
         }
      }

      long var16 = System.currentTimeMillis() - var5;
      var5 = System.currentTimeMillis();

      int var19;
      for(var9 = 0; var9 < 1000; ++var9) {
         Random var17 = new Random((long)var9);

         for(var19 = 0; var19 < 32768; ++var19) {
            short var20 = (short)var17.nextInt(2048);
            var13 = (byte)var17.nextInt(128);
            boolean var14 = var17.nextBoolean();
            byte var15 = (byte)var17.nextInt(32);
            this.putIntTypeInt(var19, var20);
            this.putIntHitpointsInt(var19, var13);
            this.putIntActivationInt(var19, var14);
            this.putIntOrientationInt(var19, var15);
            var1[var19] = var20;
            var2[var19] = var13;
            var3[var19] = var14;
            var4[var19] = var15;
         }

         for(var19 = 0; var19 < 32768; ++var19) {
            Assert.assertEquals((long)this.getTypeInt(var19), (long)var1[var19]);
            Assert.assertEquals((long)this.getHitpointsInt(var19), (long)var2[var19]);
            Assert.assertEquals(this.getActivationInt(var19), var3[var19]);
            Assert.assertEquals((long)this.getOrientationInt(var19), (long)var4[var19]);
         }
      }

      long var18 = System.currentTimeMillis() - var5;
      var5 = System.currentTimeMillis();

      for(var19 = 0; var19 < 1000; ++var19) {
         for(int var21 = 0; var21 < 32768; ++var21) {
            Assert.assertEquals((long)this.getType(var21), (long)var1[var21]);
            Assert.assertEquals((long)this.getHitpoints(var21), (long)var2[var21]);
            Assert.assertEquals(this.getActivation(var21), var3[var21]);
            Assert.assertEquals((long)this.getOrientation(var21), (long)var4[var21]);
         }
      }

      long var22 = System.currentTimeMillis() - var5;
      var5 = System.currentTimeMillis();

      for(int var23 = 0; var23 < 1000; ++var23) {
         for(int var25 = 0; var25 < 32768; ++var25) {
            Assert.assertEquals((long)this.getTypeInt(var25), (long)var1[var25]);
            Assert.assertEquals((long)this.getHitpointsInt(var25), (long)var2[var25]);
            Assert.assertEquals(this.getActivationInt(var25), var3[var25]);
            Assert.assertEquals((long)this.getOrientationInt(var25), (long)var4[var25]);
         }
      }

      long var24 = System.currentTimeMillis() - var5;
      System.err.println("3Byte: " + var16 + ";");
      System.err.println("Int  : " + var18 + ";");
      System.err.println("% " + (float)var18 / (float)var16);
      System.err.println("3ByteRead: " + var22 + ";");
      System.err.println("IntRead  : " + var24 + ";");
      System.err.println("% " + (float)var24 / (float)var22);
   }

   public static void main(String[] var0) {
      (new SegmentDataTest()).test();
   }

   static {
      typeMaskNot = ~(typeMask = Integer.parseInt("000000000000011111111111", 2));
      hpMaskNot = ~(hpMask = Integer.parseInt("000000111111100000000000", 2));
      activeMaskNot = ~(activeMask = Integer.parseInt("000001000000000000000000", 2));
      orientMaskNot = ~(orientMask = Integer.parseInt("111110000000000000000000", 2));
   }
}
