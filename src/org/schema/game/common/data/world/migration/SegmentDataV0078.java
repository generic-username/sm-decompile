package org.schema.game.common.data.world.migration;

import java.io.DataInput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.physics.octree.Octree;
import org.schema.game.common.data.world.Segment;
import org.schema.schine.network.StateInterface;

public class SegmentDataV0078 {
   public static final int occlusionIndex = 0;
   public static final int lightBlockSize = 12;
   public static final int typeIndex = 0;
   public static final int hitpointsIndex = 2;
   public static final int orientationVisLevel = 4;
   public static final int blockSize = 6;
   public static final int subLevelStart = 0;
   public static final int subLevelEnd = 6;
   public static final int subVisStart = 6;
   public static final int subVisEnd = 12;
   public static final int subOrientationStart = 12;
   public static final int subOrientationEnd = 16;
   public static final int BLOCK_COUNT = 32768;
   public static final int TOTAL_SIZE = 196608;
   public static final int TOTAL_SIZE_LIGHT = 393216;
   public static final int SEG_TIMES_SEG_TIMES_SEG = 4096;
   public static final int SEG_TIMES_SEG = 256;
   public static final int SEG = 16;
   public static final int PIECE_ADDED = 0;
   public static final int PIECE_REMOVED = 1;
   public static final int PIECE_CHANGED = 2;
   public static final int PIECE_UNCHANGED = 3;
   private static final int MASK = 255;
   Vector3b helperPos = new Vector3b();
   private Segment segment;
   private byte[] data;
   private byte[] lightData;
   private int size;
   private Vector3b min = new Vector3b();
   private Vector3b max = new Vector3b();
   private Octree octree;
   private boolean needsRevalidate = false;
   private boolean softReset;
   private boolean revalidating;

   public SegmentDataV0078(boolean var1) {
      this.octree = new Octree(2, var1);
      this.data = new byte[196608];
      if (var1) {
         this.lightData = new byte[393216];
      }

      this.resetBB();
   }

   public static int getInfoIndex(byte var0, byte var1, byte var2) {
      assert valid(var0, var1, var2) : var0 + ", " + var1 + ", " + var2 + ": DIM 32;";

      return 6 * ((var2 << 8) + (var1 << 4) + var0);
   }

   public static int getInfoIndex(Vector3b var0) {
      return getInfoIndex(var0.x, var0.y, var0.z);
   }

   public static int getLightInfoIndex(byte var0, byte var1, byte var2) {
      assert valid(var0, var1, var2) : var0 + ", " + var1 + ", " + var2 + ": DIM 32;";

      return 12 * ((var2 << 8) + (var1 << 4) + var0);
   }

   public static int getLightInfoIndex(Vector3b var0) {
      return getLightInfoIndex(var0.x, var0.y, var0.z);
   }

   public static int getLightInfoIndexFromIndex(int var0) {
      return var0 / 6 * 12;
   }

   public static boolean valid(byte var0, byte var1, byte var2) {
      return var0 < 16 && var1 < 16 && var2 < 16 && var0 >= 0 && var1 >= 0 && var2 >= 0;
   }

   public static float byteArrayToFloat(byte[] var0) {
      int var1 = 0;
      int var2 = 0;

      for(int var3 = 3; var3 >= 0; --var3) {
         var1 |= (var0[var2] & 255) << (var3 << 3);
         ++var2;
      }

      return Float.intBitsToFloat(var1);
   }

   public static byte[] floatToByteArray(float var0) {
      return intToByteArray(Float.floatToRawIntBits(var0));
   }

   public static byte[] intToByteArray(int var0) {
      byte[] var1 = new byte[4];

      for(int var2 = 0; var2 < 4; ++var2) {
         int var3 = var1.length - 1 - var2 << 3;
         var1[var2] = (byte)(var0 >>> var3);
      }

      return var1;
   }

   public int applySegmentData(Vector3b var1, byte[] var2) {
      synchronized(this) {
         int var4 = getInfoIndex(var1);
         int var5 = 0;
         short var6 = this.getType(var4);
         short var7 = this.getHitpoints(var4);
         byte var8 = this.getVis(var4);

         for(int var9 = var4; var9 < var4 + 6; ++var9) {
            this.data[var9] = var2[var5++];
         }

         this.setVis(var4, var8);
         short var12 = this.getType(var4);
         short var11 = this.getHitpoints(var4);
         if (var12 == var6) {
            return var7 != var11 ? 2 : 3;
         }

         System.err.println("APPLY SEGMENT DATA " + var6 + " - " + var12);
         if (var6 == 0 && var12 != 0) {
            this.onAddingElement(var4, var1.x, var1.y, var1.z, var12);
            return 0;
         }

         if (var6 != 0 && var12 == 0) {
            this.onRemovingElement(var4, var1.x, var1.y, var1.z, var6);
            return 1;
         }
      }

      return 2;
   }

   public int arraySize() {
      return this.data.length;
   }

   public boolean contains(byte var1, byte var2, byte var3) {
      return valid(var1, var2, var3) ? this.containsUnsave(var1, var2, var3) : false;
   }

   public boolean contains(int var1) {
      return this.getType(var1) != 0;
   }

   public boolean contains(Vector3b var1) {
      return this.contains(var1.x, var1.y, var1.z);
   }

   public boolean containsUnblended(byte var1, byte var2, byte var3) {
      return valid(var1, var2, var3) && this.getType(var1, var2, var3) > 0;
   }

   public boolean containsUnblended(Vector3b var1) {
      return this.containsUnblended(var1.x, var1.y, var1.z);
   }

   public boolean containsUnsave(byte var1, byte var2, byte var3) {
      return this.getType(var1, var2, var3) != 0;
   }

   public boolean containsUnsave(int var1) {
      return this.getType(var1) != 0;
   }

   public void createFromByteBuffer(byte[] var1, StateInterface var2) {
      ByteBuffer var5 = ByteBuffer.wrap(var1);
      synchronized(this) {
         if (this.data == null) {
            this.data = new byte[196608];
         }

         for(int var3 = 0; var3 < this.data.length; ++var3) {
            this.data[var3] = var5.get();
         }

      }
   }

   public void deserialize(DataInput var1) throws IOException {
      synchronized(this) {
         this.reset();
         if (!this.softReset) {
            var1.readFully(this.data);
            this.setNeedsRevalidate(true);
         } else {
            byte[] var3 = new byte[6];
            var1.readFully(var3);
         }

      }
   }

   public byte[] getAsBuffer() {
      return this.data;
   }

   public short getHitpoints(int var1) {
      return ByteUtil.shortReadByteArray(this.data, var1 + 2);
   }

   public Vector3b getMax() {
      return this.max;
   }

   public Vector3b getMin() {
      return this.min;
   }

   public byte getOcclusion(int var1, int var2) {
      return ByteUtil.getHex(this.lightData, var1, var2);
   }

   public Octree getOctree() {
      return this.octree;
   }

   public void setOctree(Octree var1) {
      this.octree = var1;
   }

   public byte getOrientation(int var1) {
      return (byte)ByteUtil.extractShort(ByteUtil.shortReadByteArray(this.data, var1 + 4), 12, 16, this);
   }

   public Vector3b getPosition(Vector3b var1, int var2) {
      int var3 = (var2 /= 6) / 1024 % 32;
      int var4 = var2 % 1024 / 32 % 32;
      var2 = var2 % 1024 % 32;
      var1.set((byte)var2, (byte)var4, (byte)var3);
      return var1;
   }

   public Segment getSegment() {
      return this.segment;
   }

   public void setSegment(Segment var1) {
      this.segment = var1;
   }

   public SegmentController getSegmentController() {
      return this.segment.getSegmentController();
   }

   public byte[] getSegmentPieceData(int var1, byte[] var2) {
      int var3 = 0;

      for(int var4 = var1; var4 < var1 + 6; ++var4) {
         var2[var3++] = this.data[var4];
      }

      return var2;
   }

   public int getSize() {
      return this.size;
   }

   public void setSize(int var1) {
      this.size = var1;
      if (this.segment != null) {
         this.segment.setSize(var1);
      }

      assert var1 >= 0 && var1 <= 32768 : "arraySize: 32768";
   }

   public short getType(byte var1, byte var2, byte var3) {
      int var4 = getInfoIndex(var1, var2, var3);
      return this.getType(var4);
   }

   public short getType(int var1) {
      return ByteUtil.shortReadByteArray(this.data, var1);
   }

   public short getType(Vector3b var1) {
      return this.getType(var1.x, var1.y, var1.z);
   }

   public byte getVis(byte var1, byte var2, byte var3) {
      int var4 = getInfoIndex(var1, var2, var3);
      return this.getVis(var4);
   }

   public byte getVis(int var1) {
      return (byte)ByteUtil.extractShort(ByteUtil.shortReadByteArray(this.data, var1 + 4), 6, 12, this);
   }

   public byte getVis(Vector3b var1) {
      return this.getVis(var1.x, var1.y, var1.z);
   }

   public boolean isRevalidating() {
      return this.revalidating;
   }

   public boolean needsRevalidate() {
      return this.needsRevalidate;
   }

   public boolean neighbors(byte var1, byte var2, byte var3) {
      if (this.contains((byte)(var1 - 1), var2, var3)) {
         return true;
      } else if (this.contains((byte)(var1 + 1), var2, var3)) {
         return true;
      } else if (this.contains(var1, (byte)(var2 - 1), var3)) {
         return true;
      } else if (this.contains(var1, (byte)(var2 + 1), var3)) {
         return true;
      } else if (this.contains(var1, var2, (byte)(var3 - 1))) {
         return true;
      } else {
         return this.contains(var1, var2, (byte)(var3 + 1));
      }
   }

   public void onAddingElement(int var1, byte var2, byte var3, byte var4, short var5) {
      synchronized(this) {
         this.getSize();
         this.setSize(this.getSize() + 1);
         this.getOctree().insert(var2, var3, var4);
         this.helperPos.set(var2, var3, var4);
         this.getSegmentController().onAddedElementSynched(var5, this.getOrientation(var1), var2, var3, var4, this.getSegment(), true, this.segment.getAbsoluteIndex(var2, var3, var4), 0L, false);
         if (!this.revalidating) {
            this.getSegment().dataChanged(true);
         }

         this.updateBB(var2, var3, var4);
      }
   }

   public void onRemovingElement(int var1, byte var2, byte var3, byte var4, short var5) {
      synchronized(this) {
         int var6 = this.getSize();
         this.setSize(this.getSize() - 1);
         this.getOctree().delete(var2, var3, var4);
         this.getSegmentController().onRemovedElementSynched(var5, var6, var2, var3, var4, (byte)0, this.getSegment(), false, 0L);
         if (!this.revalidating) {
            this.getSegment().dataChanged(true);
         }

         this.updateBB(var2, var3, var4);
      }
   }

   public void removeInfoElement(byte var1, byte var2, byte var3) {
      synchronized(this) {
         int var6 = getInfoIndex(var1, var2, var3);
         this.data[var6] = 0;
      }
   }

   public void removeInfoElement(Vector3b var1) {
      this.removeInfoElement(var1.x, var1.y, var1.z);
   }

   public void reset() {
      synchronized(this) {
         Arrays.fill(this.data, (byte)0);
         if (this.lightData != null) {
            Arrays.fill(this.lightData, (byte)0);
         }

         this.setSize(0);
         this.octree.reset();
         this.resetBB();
      }
   }

   public void resetBB() {
      this.getMax().set((byte)-128, (byte)-128, (byte)-128);
      this.getMin().set((byte)127, (byte)127, (byte)127);
   }

   public void resetLight() {
      Arrays.fill(this.lightData, (byte)0);
   }

   private void revalidate(byte var1, byte var2, byte var3) {
      int var4 = getInfoIndex(var1, var2, var3);
      short var5;
      if ((var5 = this.getType(var4)) != 0) {
         this.onAddingElement(var4, var1, var2, var3, var5);
      }

   }

   public void revalidateData() {
      synchronized(this) {
         this.revalidating = true;
         this.octree.reset();
         if (this.getSize() > 0) {
            System.err.println("[WARNING][SEGMENTDATA] segment not empty on revalidate. size was " + this.getSize() + " in " + this.getSegment().pos + " -> " + this.getSegmentController());
            this.reset();
         }

         for(byte var2 = 0; var2 < 32; ++var2) {
            for(byte var3 = 0; var3 < 32; ++var3) {
               for(byte var4 = 0; var4 < 32; ++var4) {
                  this.revalidate(var4, var3, var2);
               }
            }
         }

         this.setNeedsRevalidate(false);
         this.revalidating = false;
         this.getSegment().dataChanged(true);
      }
   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.write(this.data);
   }

   public void setHitpoints(int var1, short var2) {
      ByteUtil.shortWriteByteArray((short)Math.max(0, var2), this.data, var1 + 2);
   }

   public void setInfoElement(byte var1, byte var2, byte var3, short var4) {
      synchronized(this) {
         int var6 = getInfoIndex(var1, var2, var3);
         short var7 = ByteUtil.shortReadByteArray(this.data, var6);
         ByteUtil.shortWriteByteArray(var4, this.data, var6);
         if (var4 == 0) {
            if (var7 != 0) {
               this.onRemovingElement(var6, var1, var2, var3, var7);
            }
         } else if (var7 == 0) {
            this.onAddingElement(var6, var1, var2, var3, var4);
            this.setHitpoints(var6, (short)127);
         }

      }
   }

   public void setInfoElement(Vector3b var1, short var2) {
      this.setInfoElement(var1.x, var1.y, var1.z, var2);
   }

   public void setNeedsRevalidate(boolean var1) {
      this.needsRevalidate = var1;
   }

   public void setOcclusion(byte var1, byte var2, byte var3, byte[] var4) {
      int var5 = getLightInfoIndex(var1, var2, var3);
      this.setOcclusion(var5, var4);
   }

   public void setOcclusion(int var1, byte var2, int var3) {
      ByteUtil.writeHex(this.lightData, var1, var3, var2);
   }

   public void setOcclusion(int var1, byte[] var2) {
      for(int var3 = 0; var3 < 24; ++var3) {
         this.setOcclusion(var1, var2[var3], var3);
      }

   }

   public void setOcclusionZero(int var1) {
      Arrays.fill(this.lightData, var1, 11, (byte)0);
   }

   public void setOrientation(int var1, byte var2) {
      var2 = (byte)Math.max(0, Math.min(5, var2));

      assert var2 >= 0 && var2 < 6 : "NOT A SIDE INDEX";

      var2 = Element.orientationMapping[var2];
      ByteUtil.putRangedBitsOntoShort(this.data, var2, 12, 16, var1 + 4, this);

      assert var2 == this.getOrientation(var1) : "failed orientation coding: " + var2 + " != result " + this.getOrientation(var1);

   }

   public void setSoftReset(boolean var1) {
      this.softReset = var1;
   }

   public void setTypeForcedWithoutUpdate(int var1, short var2) {
      ByteUtil.shortWriteByteArray(var2, this.data, var1);
   }

   public void setVis(byte var1, byte var2, byte var3, byte var4) {
      int var5 = getInfoIndex(var1, var2, var3);
      this.setVis(var5, var4);
   }

   public void setVis(int var1, byte var2) {
      ByteUtil.putRangedBitsOntoShort(this.data, var2, 6, 12, var1 + 4, this);

      assert this.getVis(var1) == var2;

   }

   public void setVis(Vector3b var1, byte var2) {
      this.setVis(var1.x, var1.y, var1.z, var2);
   }

   private void updateBB(byte var1, byte var2, byte var3) {
      if (var1 > this.getMax().x) {
         this.getMax().x = var1;
      }

      if (var2 > this.getMax().y) {
         this.getMax().y = var2;
      }

      if (var3 > this.getMax().z) {
         this.getMax().z = var3;
      }

      if (var1 < this.getMin().x) {
         this.getMin().x = var1;
      }

      if (var2 < this.getMin().y) {
         this.getMin().y = var2;
      }

      if (var3 < this.getMin().z) {
         this.getMin().z = var3;
      }

      this.getSegmentController().getSegmentBuffer().updateBB(this.getSegment());
   }
}
