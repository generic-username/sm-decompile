package org.schema.game.common.data.world.migration;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;
import org.lwjgl.BufferUtils;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.io.SegmentDataFileUtils;
import org.schema.game.common.controller.io.SegmentDataIO16;
import org.schema.game.common.controller.io.SegmentDataIONew;
import org.schema.game.common.controller.io.SegmentRegionFileNew;
import org.schema.game.common.controller.io.SegmentSerializationBuffers;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.Chunk16SegmentData;
import org.schema.game.common.data.world.DeserializationException;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentData3Byte;
import org.schema.game.common.data.world.SegmentDataIntArray;
import org.schema.game.common.data.world.SegmentDataInterface;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.common.util.FolderZipper;
import org.schema.schine.resource.FileExt;

public class Chunk32Migration {
   private static final ByteBuffer dataByteBuffer = BufferUtils.createByteBuffer(1048576);
   private final String UID;
   private File dir;
   private String dirName;
   private Long2ObjectOpenHashMap map = new Long2ObjectOpenHashMap();
   private Long2ObjectOpenHashMap map32 = new Long2ObjectOpenHashMap();
   private final Vector3i min = new Vector3i(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);
   private final Vector3i max = new Vector3i(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
   long time = System.currentTimeMillis();

   public Chunk32Migration(String var1, String var2, File var3) {
      this.UID = var1.substring(0, var1.length() - 1);
      this.dir = var3;
      this.dirName = var2;
   }

   public static void test(String var0, String var1) throws DeserializationException, IOException {
      Vector3i var2 = new Vector3i(0, 0, 64);
      RemoteSegment var3 = new RemoteSegment((SegmentController)null);
      (new SegmentDataIntArray()).assignData(var3);
      System.err.println(" ---- --- CHECKING: " + var2);
      if (SegmentDataIONew.requestStatic(var2.x, var2.y, var2.z, var0, dataByteBuffer, var1, var3) == 0) {
         System.err.println("CHECK OK " + var2 + " :: " + var3.pos);

         assert var2.equals(var3.pos) : "; POS " + var2 + "; " + var3.pos;
      }

   }

   public static void main(String[] var0) throws Exception {
      processFolderRecursive("./blueprints-default/", true, (FolderZipper.ZipCallback)null, true);
   }

   private static void processFolderRecursive(String var0, boolean var1, FolderZipper.ZipCallback var2, boolean var3) {
      FileExt var4;
      if ((var4 = new FileExt(var0)).exists() && var4.isDirectory()) {
         String[] var8;
         int var5 = (var8 = var4.list()).length;

         for(int var6 = 0; var6 < var5; ++var6) {
            String var7;
            if ((var7 = var8[var6]).equals("DATA")) {
               processFolder(var0 + var7 + "/", var1, var2, var3);
            } else {
               processFolderRecursive(var0 + var7 + "/", var1, var2, var3);
            }
         }
      }

   }

   public static void testRandom() {
      try {
         Random var0 = new Random();
         RemoteSegment var1 = new RemoteSegment((SegmentController)null);
         SegmentDataIntArray var2;
         (var2 = new SegmentDataIntArray()).assignData(var1);
         System.err.println("WRITING RANDOM REGION FILE");
         SegmentRegionFileNew var3 = null;

         for(int var4 = -8; var4 < 8; ++var4) {
            for(int var5 = -8; var5 < 8; ++var5) {
               for(int var6 = -8; var6 < 8; ++var6) {
                  var1.pos.set(var6 << 5, var5 << 5, var4 << 5);
                  var2.resetFast();

                  for(int var7 = 0; var7 < 32768; ++var7) {
                     short var8;
                     for(var8 = (short)var0.nextInt(ElementKeyMap.highestType + 1); var8 != 0 && !ElementKeyMap.isValidType(var8); var8 = (short)var0.nextInt(ElementKeyMap.highestType + 1)) {
                     }

                     var2.setType(var7, var8);
                     var2.setHitpointsByte(var7, var0.nextInt(256));
                     var2.setOrientation(var7, (byte)var0.nextInt(16));
                     var2.setActive(var7, var0.nextBoolean());
                     var2.incSize();
                  }

                  FileExt var12 = new FileExt("segTestTmp.smd3");

                  try {
                     var3 = SegmentDataIONew.write(var1, System.currentTimeMillis(), var12, var3);
                  } catch (IOException var10) {
                     var10.printStackTrace();
                  }
               }
            }
         }

         if (var3 != null) {
            try {
               System.err.println("REGION FILE :::: " + var3.getFile().length());
               var3.close();
            } catch (IOException var9) {
               var9.printStackTrace();
            }

            (new FileExt("segTestTmp.smd3")).delete();
         }

      } catch (SegmentDataWriteException var11) {
         var11.printStackTrace();
         throw new RuntimeException("this should be never be thrown as migration should always be toa normal segment data", var11);
      }
   }

   public static void processFolder(String var0, boolean var1, FolderZipper.ZipCallback var2, boolean var3) {
      FileExt var4;
      if ((var4 = new FileExt(var0)).exists() && var4.isDirectory()) {
         String[] var5;
         Arrays.sort(var5 = var4.list());
         ObjectArrayList var6 = new ObjectArrayList();
         String var7 = null;
         String[] var8 = var5;
         int var9 = var5.length;

         int var10;
         String var11;
         for(var10 = 0; var10 < var9; ++var10) {
            if ((var11 = var8[var10]).endsWith(".smd2")) {
               if (var2 != null) {
                  var2.update((File)null);
               }

               if (var7 == null || !var11.startsWith(var7)) {
                  if (!var6.isEmpty()) {
                     try {
                        if (var7 == null) {
                           throw new NullPointerException();
                        }

                        processObject(var7, var0, var4, var6, var3);
                     } catch (Exception var13) {
                        var13.printStackTrace();
                     }

                     var6.clear();
                  }

                  var7 = var11.substring(0, var11.indexOf(".") + 1);
               }

               var6.add(var11);
            }
         }

         if (!var6.isEmpty()) {
            try {
               if (var7 == null) {
                  throw new NullPointerException();
               }

               processObject(var7, var0, var4, var6, var3);
            } catch (Exception var12) {
               var12.printStackTrace();
            }
         }

         if (var1) {
            var8 = var5;
            var9 = var5.length;

            for(var10 = 0; var10 < var9; ++var10) {
               if ((var11 = var8[var10]).endsWith(".smd2")) {
                  (new FileExt(var4, var11)).delete();
               }
            }
         }
      }

   }

   private static void processObject(String var0, String var1, File var2, ObjectArrayList var3, boolean var4) throws DeserializationException, IOException {
      Chunk32Migration var7 = new Chunk32Migration(var0, var1, var2);
      System.err.println("PROCESSING CHUNK16 FILES: " + var0 + "; File count: " + var3.size());
      Iterator var6 = var3.iterator();

      while(var6.hasNext()) {
         String var8 = (String)var6.next();
         var7.add(var8);
      }

      try {
         var7.buildChunk32();
      } catch (SegmentDataWriteException var5) {
         throw new RuntimeException("Should not happen at this point", var5);
      }

      var7.writeChunk32(var4);
   }

   private void writeChunk32(boolean var1) throws IOException {
      Object2ObjectOpenHashMap var7 = new Object2ObjectOpenHashMap();
      ObjectOpenHashSet var2 = new ObjectOpenHashSet();
      System.err.println("WRITING NEW CHUNK32 FORMAT. Count: " + this.map32.size());
      Iterator var3 = this.map32.values().iterator();

      while(var3.hasNext()) {
         Chunk32Migration.Chunk32FirstVersionRM var4;
         RemoteSegment var5;
         (var5 = (var4 = (Chunk32Migration.Chunk32FirstVersionRM)var3.next()).s).setSize(var4.migrated.getSize());
         String var8 = SegmentDataFileUtils.getSegFile(var5.pos.x, var5.pos.y, var5.pos.z, this.UID, (String)null, (Long2ObjectOpenHashMap)null, this.dirName);
         FileExt var6 = new FileExt(var8);
         if (!var2.contains(var8)) {
            var2.add(var8);
            var6.delete();
         }

         SegmentRegionFileNew var9;
         if ((var9 = SegmentDataIONew.write(var5, this.time, var6, (SegmentRegionFileNew)var7.get(var8))) != null) {
            var7.put(var8, var9);
         }
      }

      System.err.println("FLUSHING ALL WRITTEN FILES TO DISK. File count: " + var7.size());
      var3 = var7.values().iterator();

      while(var3.hasNext()) {
         ((SegmentRegionFileNew)var3.next()).close();
      }

   }

   private void buildChunk32() throws SegmentDataWriteException {
      System.err.println("BUILDING CHUNK32 IN RANGE: [" + this.min + ", " + this.max + "]");

      int var4;
      int var6;
      for(int var1 = this.min.z; var1 < this.max.z; ++var1) {
         for(int var2 = this.min.y; var2 < this.max.y; ++var2) {
            for(int var3 = this.min.x; var3 < this.max.x; ++var3) {
               var4 = ByteUtil.divU16(var3) << 4;
               int var5 = ByteUtil.divU16(var2) << 4;
               var6 = ByteUtil.divU16(var1) << 4;
               long var7 = ElementCollection.getIndex(var4, var5, var6);
               var4 = var3 + 8;
               var5 = var2 + 8;
               var6 = var1 + 8;
               int var9 = ByteUtil.divU32(var4) << 5;
               int var10 = ByteUtil.divU32(var5) << 5;
               int var11 = ByteUtil.divU32(var6) << 5;
               long var13 = ElementCollection.getIndex(var9, var10, var11);
               Chunk16SegmentData var23;
               if ((var23 = (Chunk16SegmentData)this.map.get(var7)) != null) {
                  int var8 = Chunk16SegmentData.getInfoIndex(ByteUtil.modU16(var3), ByteUtil.modU16(var2), ByteUtil.modU16(var1));
                  boolean var12;
                  if (var12 = var23.getType(var8) > 0) {
                     var4 = SegmentData3Byte.getInfoIndex(ByteUtil.modU32(var4), ByteUtil.modU32(var5), ByteUtil.modU32(var6));
                     Chunk32Migration.Chunk32FirstVersionRM var20;
                     if ((var20 = (Chunk32Migration.Chunk32FirstVersionRM)this.map32.get(var13)) == null) {
                        (var20 = new Chunk32Migration.Chunk32FirstVersionRM()).s.setPos(var9, var10, var11);
                        var20.dataOld = new SegmentData3Byte();
                        this.map32.put(var13, var20);
                     }

                     var20.dataOld.getAsOldByteBuffer()[var4] = var23.getAsOldByteBuffer()[var8];
                     var20.dataOld.getAsOldByteBuffer()[var4 + 1] = var23.getAsOldByteBuffer()[var8 + 1];
                     var20.dataOld.getAsOldByteBuffer()[var4 + 2] = var23.getAsOldByteBuffer()[var8 + 2];

                     assert var23.getType(var8) == var20.dataOld.getType(var4);

                     if (var12) {
                        var20.dataOld.incSize();
                     }
                  }
               }
            }
         }
      }

      SegmentSerializationBuffers var17 = SegmentSerializationBuffers.get();

      try {
         Iterator var18 = this.map32.values().iterator();

         while(true) {
            Chunk32Migration.Chunk32FirstVersionRM var19;
            Object var21;
            do {
               if (!var18.hasNext()) {
                  return;
               }

               var19 = (Chunk32Migration.Chunk32FirstVersionRM)var18.next();
               var4 = 2;

               for(var21 = var19.dataOld; var4 < 5; ++var4) {
                  SegmentDataInterface var22;
                  (var22 = (SegmentDataInterface)var17.dummies.get(var4 + 1)).resetFast();
                  ((SegmentDataInterface)var21).migrateTo(var4, var22);
                  var22.setSize(((SegmentDataInterface)var21).getSize());
                  ((SegmentDataInterface)var21).resetFast();
                  var21 = var22;
               }
            } while(var4 != 5);

            ((SegmentDataInterface)var21).migrateTo(var4, var19.migrated);
            ((SegmentDataInterface)var21).resetFast();

            for(var6 = 0; var6 < 32768; ++var6) {
               if (var19.migrated.getType(var6) != 0) {
                  var19.migrated.incSize();
               }
            }
         }
      } finally {
         SegmentSerializationBuffers.free(var17);
      }
   }

   public void add(String var1) throws DeserializationException, IOException {
      SegmentDataIO16.request(new FileExt(this.dir, var1), this.map, this.min, this.max, dataByteBuffer, this.time);
   }

   static {
      if (ElementKeyMap.keySet.isEmpty()) {
         ElementKeyMap.initializeData(GameResourceLoader.getConfigInputFile());
      }

      try {
         BlockShapeAlgorithm.initialize();
      } catch (IOException var0) {
         var0.printStackTrace();
      }
   }

   class Chunk32FirstVersionRM {
      public SegmentData3Byte dataOld;
      public final SegmentData migrated = new SegmentDataIntArray();
      public RemoteSegment s = new RemoteSegment((SegmentController)null);

      public Chunk32FirstVersionRM() {
         this.migrated.assignData(this.s);
      }
   }
}
