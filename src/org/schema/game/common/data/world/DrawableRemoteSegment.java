package org.schema.game.common.data.world;

import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.SegmentDrawer;
import org.schema.game.client.view.cubes.CubeData;
import org.schema.game.client.view.cubes.CubeMeshBufferContainer;
import org.schema.game.client.view.cubes.CubeMeshBufferContainerPool;
import org.schema.game.common.controller.SegmentBuffer;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.network.objects.container.TransformTimed;

public class DrawableRemoteSegment extends RemoteSegment {
   private static Matrix3f abs_b = new Matrix3f();
   private static Vector3f center = new Vector3f();
   private static Vector3f extent = new Vector3f();
   private static Vector3f tmp = new Vector3f();
   private static Vector3f localHalfExtents = new Vector3f();
   private static Vector3f localCenter = new Vector3f();
   public SegmentBuffer segmentBufferAABBHelperSorting;
   public SegmentBuffer segmentBufferAABBHelper;
   public long lastDrawn = -1L;
   public Object cubeMeshLock = new Object();
   public long occlusionFailTime;
   public boolean occlusionFailed;
   public boolean exceptDockingBlock;
   public boolean exceptDockingBlockMarked;
   public DrawableRemoteSegment.RequestState requestState;
   public int requestedRecursive;
   public boolean inLightingQueue;
   public int sortingId;
   public long lastLightingUpdateTime;
   public float camDist;
   public float lastSegmentDistSquared;
   public boolean cachedFrustumSet;
   public boolean cachedFrustum;
   private boolean needsMeshUpdate;
   private boolean needsVisUpdate;
   private boolean inUpdate;
   private boolean hasVisibleElements;
   private CubeMeshBufferContainer currentBufferContainer;
   private CubeData nextCubeMesh;
   private CubeData currentCubeMesh;
   private boolean active;
   private int sortingSerial;
   private Vector3f minAABBChached;
   private Vector3f maxAABBChached;
   private short frustumCacheNum;
   private boolean inFrustumCache;
   private float percentageDrawn;
   public int lightTries;
   public LongArrayList onEdge;
   private long clientTransformCacheTime;

   public DrawableRemoteSegment(SegmentController var1) {
      super(var1);
      this.requestState = DrawableRemoteSegment.RequestState.INACTIVE;
      this.requestedRecursive = 0;
      this.needsVisUpdate = true;
      this.inUpdate = false;
      this.hasVisibleElements = true;
      this.minAABBChached = new Vector3f();
      this.maxAABBChached = new Vector3f();
      this.inFrustumCache = false;
      this.percentageDrawn = 1.0F;
      this.clientTransformCacheTime = Long.MIN_VALUE;
   }

   public static int[] decodeAmbient(byte var0, int[] var1) {
      int var2 = var0 - -128;
      var1[0] = 3 & var2;
      var1[1] = 3 & var2 >> 2;
      var1[2] = 3 & var2 >> 4;
      var1[3] = 3 & var2 >> 6;
      return var1;
   }

   public static byte encodeAmbient(int[] var0) {
      int var1 = var0[0];
      int var2 = 4 * var0[1];
      int var3 = 16 * var0[2];
      int var4 = 64 * var0[3];
      return (byte)(-128 + var1 + var2 + var3 + var4);
   }

   public static void transformAabb(Vector3f var0, Vector3f var1, float var2, Transform var3, Vector3f var4, Vector3f var5) {
      assert var0.x <= var1.x;

      assert var0.y <= var1.y;

      assert var0.z <= var1.z;

      localHalfExtents.sub(var1, var0);
      localHalfExtents.scale(0.5F);
      Vector3f var10000 = localHalfExtents;
      var10000.x += var2;
      var10000 = localHalfExtents;
      var10000.y += var2;
      var10000 = localHalfExtents;
      var10000.z += var2;
      localCenter.add(var1, var0);
      localCenter.scale(0.5F);
      abs_b.set(var3.basis);
      MatrixUtil.absolute(abs_b);
      center.set(localCenter);
      var3.transform(center);
      abs_b.getRow(0, tmp);
      extent.x = tmp.dot(localHalfExtents);
      abs_b.getRow(1, tmp);
      extent.y = tmp.dot(localHalfExtents);
      abs_b.getRow(2, tmp);
      extent.z = tmp.dot(localHalfExtents);
      var4.sub(center, extent);
      var5.add(center, extent);
   }

   public void applyCurrent() {
      synchronized(this.cubeMeshLock) {
         CubeData var2 = this.getCurrentCubeMesh();

         assert this.getCurrentCubeMesh() != this.getNextCubeMesh();

         this.setCurrentCubeMesh(this.getNextCubeMesh());
         SegmentDrawer.dataPool.release(var2);
         this.setNextCubeMesh((CubeData)null);
      }
   }

   public void keepOld() {
      synchronized(this.cubeMeshLock) {
         this.getCurrentCubeMesh();
         if (this.getNextCubeMesh() != null) {
            SegmentDrawer.dataPool.release(this.getNextCubeMesh());
         }

         this.setNextCubeMesh((CubeData)null);
      }
   }

   public void dataChanged(boolean var1) {
      super.dataChanged(var1);
      this.setNeedsMeshUpdate(true);
      if (!((GameClientState)this.getSegmentController().getState()).isPassive()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getBuildModeDrawer().flagUpdate();
      }

   }

   public void disposeAll() {
      synchronized(this.cubeMeshLock) {
         this.disposeCurrent();
         this.disposeNext();
      }
   }

   public void disposeCurrent() {
      synchronized(this.cubeMeshLock) {
         SegmentDrawer.dataPool.release(this.getCurrentCubeMesh());
         this.setCurrentCubeMesh((CubeData)null);
      }
   }

   public void disposeNext() {
      synchronized(this.cubeMeshLock) {
         SegmentDrawer.dataPool.release(this.getNextCubeMesh());
         this.setNextCubeMesh((CubeData)null);
      }
   }

   public void getAABBClient(Vector3f var1, Vector3f var2, Vector3f var3) {
      TransformTimed var4;
      if ((var4 = this.getSegmentController().getWorldTransformOnClient()).lastChanged != this.clientTransformCacheTime) {
         var1.set((float)this.pos.x, (float)this.pos.y, (float)this.pos.z);
         var2.set(var1);
         var2.x += 16.0F;
         var2.y += 16.0F;
         var2.z += 16.0F;
         var1.x -= 16.0F;
         var1.y -= 16.0F;
         var1.z -= 16.0F;
         transformAabb(var1, var2, 0.0F, var4, this.minAABBChached, this.maxAABBChached);
         this.clientTransformCacheTime = var4.lastChanged;
      }

      var1.set(this.minAABBChached);
      var2.set(this.maxAABBChached);
      var3.set(var1.x + (var2.x - var1.x) * 0.5F, var1.y + (var2.y - var1.y) * 0.5F, var1.z + (var2.z - var1.z) * 0.5F);
   }

   public CubeMeshBufferContainer getContainerFromPool() {
      assert this.currentBufferContainer == null;

      CubeMeshBufferContainer var1 = CubeMeshBufferContainerPool.get();
      this.currentBufferContainer = var1;
      return var1;
   }

   public CubeMeshBufferContainer getCurrentBufferContainer() {
      return this.currentBufferContainer;
   }

   public CubeData getCurrentCubeMesh() {
      return this.currentCubeMesh;
   }

   public void setCurrentCubeMesh(CubeData var1) {
      this.currentCubeMesh = var1;
   }

   public CubeData getNextCubeMesh() {
      return this.nextCubeMesh;
   }

   public void setNextCubeMesh(CubeData var1) {
      this.nextCubeMesh = var1;
   }

   public Vector3i getPos() {
      return this.pos;
   }

   public int getSortingSerial() {
      return this.sortingSerial;
   }

   public void setSortingSerial(int var1) {
      this.sortingSerial = var1;
   }

   public boolean hasVisibleElements() {
      return this.hasVisibleElements;
   }

   public boolean isActive() {
      return this.active;
   }

   public void setActive(boolean var1) {
      if (this.active != var1) {
         this.getSegmentController().getSegmentBuffer().incActive(var1 ? 1 : -1, this);
      }

      this.active = var1;
   }

   public boolean isInUpdate() {
      return this.inUpdate;
   }

   public void setInUpdate(boolean var1) {
      this.inUpdate = var1;
   }

   public boolean isNeedsVisUpdate() {
      return this.needsVisUpdate;
   }

   public void setNeedsVisUpdate(boolean var1) {
      this.needsVisUpdate = var1;
   }

   public boolean needsMeshUpdate() {
      boolean var1 = this.needsMeshUpdate || this.getSegmentController().percentageDrawn != this.percentageDrawn;
      this.percentageDrawn = this.getSegmentController().percentageDrawn;
      return var1;
   }

   public void releaseContainerFromPool() {
      if (this.currentBufferContainer != null) {
         CubeMeshBufferContainerPool.release(this.currentBufferContainer);
         this.currentBufferContainer = null;
      }

   }

   public void setHasVisibleElements(boolean var1) {
      this.hasVisibleElements = var1;
   }

   public void setNeedsMeshUpdate(boolean var1) {
      if (var1) {
         this.hasVisibleElements = true;
      }

      this.needsMeshUpdate = var1;
   }

   public boolean isInViewFrustum(short var1) {
      if (this.frustumCacheNum == var1) {
         return this.inFrustumCache;
      } else {
         this.inFrustumCache = Controller.getCamera().isAABBInFrustum(this.minAABBChached, this.maxAABBChached);
         this.frustumCacheNum = var1;
         return this.inFrustumCache;
      }
   }

   public SegmentBuffer getSegmentBufferRegion() {
      return this.getSegmentController().getSegmentBuffer().getBuffer(this.pos);
   }

   public static enum RequestState {
      INACTIVE(1),
      JUST_ADDED(1),
      ALL_REQUESTS_DONE(0),
      TOO_FAR(0),
      INVALID(0);

      public int code;

      private RequestState(int var3) {
         this.code = var3;
      }
   }
}
