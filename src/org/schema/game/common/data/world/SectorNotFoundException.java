package org.schema.game.common.data.world;

public class SectorNotFoundException extends Exception {
   private static final long serialVersionUID = 1L;

   public SectorNotFoundException(int var1) {
      super("SECTOR: " + String.valueOf(var1));
   }
}
