package org.schema.game.common.data.world;

import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.schine.graphicsengine.forms.Light;

public interface LightTransformable extends SimpleTransformable {
   Light getLight();

   void setLight(Light var1);

   AbstractOwnerState getOwnerState();
}
