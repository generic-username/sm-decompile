package org.schema.game.common.data.world;

public class SegmentInflaterException extends Exception {
   private static final long serialVersionUID = 1L;
   public int inflate;
   public int shouldBeInflate;

   public SegmentInflaterException(int var1, int var2) {
      this.inflate = var1;
      this.shouldBeInflate = var2;
   }
}
