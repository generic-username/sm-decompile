package org.schema.game.common.data.world;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2BooleanOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2LongOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.DataInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Set;
import java.util.Map.Entry;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ShoppingAddOn;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.trade.TradeNodeClient;
import org.schema.game.common.controller.trade.TradeNodeStub;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.network.objects.FowRequestAndAwnser;
import org.schema.game.network.objects.GalaxyRequestAndAwnser;
import org.schema.game.network.objects.GalaxyZoneRequestAndAwnser;
import org.schema.game.network.objects.NetworkClientChannel;
import org.schema.game.network.objects.TradePrices;
import org.schema.game.network.objects.remote.RemoteFTLConnection;
import org.schema.game.network.objects.remote.RemoteFowRequestAndAwnser;
import org.schema.game.network.objects.remote.RemoteGalaxyRequest;
import org.schema.game.network.objects.remote.RemoteGalaxyZoneRequest;
import org.schema.game.network.objects.remote.RemoteTradeNode;
import org.schema.game.network.objects.remote.RemoteTradePrice;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.server.ServerStateInterface;

public class GalaxyManager {
   public static final int ZONE_SIZE = 32;
   private final StateInterface state;
   private final boolean onServer;
   private final Object2ObjectOpenHashMap ftlData = new Object2ObjectOpenHashMap();
   private final Object2ObjectOpenHashMap tradeData = new Object2ObjectOpenHashMap();
   private final Map zoneMap = new Object2ObjectOpenHashMap();
   private final Long2ObjectOpenHashMap tradeDataById = new Long2ObjectOpenHashMap();
   private final Vector3i zTmp = new Vector3i();
   private final Vector3i tmpPosSeverAwner = new Vector3i();
   Vector3i reqPos = new Vector3i();
   VoidSystem tmpVoidSystemServer = new VoidSystem();
   private ObjectArrayFIFOQueue serverRequestsAndClientAwnsers = new ObjectArrayFIFOQueue();
   private ObjectArrayFIFOQueue serverZoneRequestsAndClientAwnsers = new ObjectArrayFIFOQueue();
   private ObjectArrayFIFOQueue serverFowRequests = new ObjectArrayFIFOQueue();
   private ObjectArrayFIFOQueue clientSystemsToAdd = new ObjectArrayFIFOQueue();
   private Object2LongOpenHashMap zoneAwnsersOnClient = new Object2LongOpenHashMap();
   private Object2LongOpenHashMap requested = new Object2LongOpenHashMap();
   private ObjectOpenHashSet requestedZones = new ObjectOpenHashSet();
   private ObjectArrayFIFOQueue serverAwnsers = new ObjectArrayFIFOQueue();
   private ObjectArrayFIFOQueue serverFowAwnsers = new ObjectArrayFIFOQueue();
   private ObjectArrayFIFOQueue toAddFTLDataOnClient = new ObjectArrayFIFOQueue();
   private ObjectArrayFIFOQueue toAddTradeDataOnClient = new ObjectArrayFIFOQueue();
   private ObjectArrayFIFOQueue fowAwnsersOnClient = new ObjectArrayFIFOQueue();
   private ObjectArrayFIFOQueue serverZonesAwnsers = new ObjectArrayFIFOQueue();
   private Object2ObjectOpenHashMap clientData = new Object2ObjectOpenHashMap();
   private Vector3i cTmp = new Vector3i();
   public final GalaxyManager.TradeDataListener tradeDataListener = new GalaxyManager.TradeDataListener();
   private boolean shutdown;
   private final ObjectArrayFIFOQueue priceRequests = new ObjectArrayFIFOQueue();
   private final ObjectArrayFIFOQueue priceRequestsAwnsered = new ObjectArrayFIFOQueue();
   private final Object2BooleanOpenHashMap visibility = new Object2BooleanOpenHashMap();
   private final Set requestedVisibility = new ObjectOpenHashSet();
   Vector3i sysTmp = new Vector3i();

   public GalaxyManager(StateInterface var1) {
      this.state = var1;
      this.onServer = var1 instanceof ServerStateInterface;
      this.startThreads();
   }

   public static Vector3i getContainingZoneFromSector(Vector3i var0, int var1, Vector3i var2) {
      getContainingZoneFromSystem(StellarSystem.getPosFromSector(var0, new Vector3i()), var1, var2);
      return var2;
   }

   public static Vector3i getContainingZoneFromSystem(Vector3i var0, int var1, Vector3i var2) {
      var2.x = ByteUtil.divU32(var0.x) * var1;
      var2.y = ByteUtil.divU32(var0.y) * var1;
      var2.z = ByteUtil.divU32(var0.z) * var1;
      return var2;
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public VoidSystem getSystemOnClient(Vector3i var1) {
      assert !this.isOnServer();

      synchronized(this.state) {
         boolean var3 = !this.state.isSynched();

         VoidSystem var10;
         try {
            if (var3) {
               this.state.setSynched();
            }

            Vector3i var4 = StellarSystem.getPosFromSector(var1, this.cTmp);
            VoidSystem var5;
            if ((var5 = (VoidSystem)this.clientData.get(var4)) == null) {
               if (!this.requested.containsKey(var4)) {
                  this.clientRequest(((GameClientState)this.state).getController().getClientChannel().getNetworkObject(), var1);
               } else {
                  assert this.requested.getLong(var4) > 0L : this.requested.getLong(var4) + "; " + this.requested.get(var4);

                  assert System.currentTimeMillis() - this.requested.getLong(var4) <= 16000L : "NO AWNSER FOR " + var4 + "; " + this.requested.getLong(var4);
               }
            }

            if (!this.requestedZones.contains(getContainingZoneFromSystem(var4, 32, this.zTmp))) {
               this.clientRequestZone(((GameClientState)this.state).getController().getClientChannel().getNetworkObject(), var1);
            }

            var10 = var5;
         } finally {
            if (var3) {
               this.state.setUnsynched();
            }

         }

         return var10;
      }
   }

   public void checkSystemBySectorPos(Vector3i var1) {
      assert !this.isOnServer();

      for(int var2 = -1; var2 < 2; ++var2) {
         for(int var3 = -1; var3 < 2; ++var3) {
            for(int var4 = -1; var4 < 2; ++var4) {
               this.reqPos.set(var1);
               this.reqPos.add(var4 << 8, var3 << 8, var2 << 8);
               Vector3i var5;
               Vector3i var6 = getContainingZoneFromSystem(var5 = StellarSystem.getPosFromSector(this.reqPos, this.cTmp), 32, this.zTmp);
               Vector3i var7 = ((GameClientState)this.state).getCurrentGalaxy().galaxyPos;
               boolean var8 = var5.x < var7.x + Galaxy.halfSize && var5.x > var7.x - Galaxy.halfSize;
               boolean var9 = var5.y < var7.y + Galaxy.halfSize && var5.y > var7.y - Galaxy.halfSize;
               boolean var10 = var5.z < var7.z + Galaxy.halfSize && var5.z > var7.z - Galaxy.halfSize;
               if (var8 && var9 && var10 && !this.requestedZones.contains(var6)) {
                  this.clientRequestZone(((GameClientState)this.state).getController().getClientChannel().getNetworkObject(), new Vector3i(this.reqPos));
               }
            }
         }
      }

   }

   public void clientRequest(NetworkClientChannel var1, Vector3i var2) {
      synchronized(this.state) {
         boolean var4;
         if (var4 = !this.state.isSynched()) {
            this.state.setSynched();
         }

         GalaxyRequestAndAwnser var5;
         (var5 = new GalaxyRequestAndAwnser()).secX = var2.x;
         var5.secY = var2.y;
         var5.secZ = var2.z;
         var2 = StellarSystem.getPosFromSector(var2, new Vector3i());
         this.requested.put(var2, System.currentTimeMillis());
         synchronized(var1.galaxyRequests) {
            System.err.println("[CLIENT] REQUESTING SYSTEM: " + var2);
            var1.galaxyRequests.add(new RemoteGalaxyRequest(var5, var1));
         }

         if (var4) {
            this.state.setUnsynched();
         }

      }
   }

   public void clientRequestZone(NetworkClientChannel var1, Vector3i var2) {
      synchronized(this.state) {
         boolean var4;
         if (var4 = !this.state.isSynched()) {
            this.state.setSynched();
         }

         var2 = getContainingZoneFromSector(var2, 32, new Vector3i());
         this.requestedZones.add(var2);
         GalaxyZoneRequestAndAwnser var5;
         (var5 = new GalaxyZoneRequestAndAwnser()).zoneSize = 32;
         var5.startSystem = var2;
         synchronized(var1.galaxyZoneRequests) {
            var1.galaxyZoneRequests.add(new RemoteGalaxyZoneRequest(var5, var1));
         }

         if (var4) {
            this.state.setUnsynched();
         }

      }
   }

   public void updateFromNetwork(ClientChannel var1) {
      NetworkClientChannel var2;
      Iterator var4;
      RemoteGalaxyRequest var5;
      synchronized((var2 = var1.getNetworkObject()).galaxyRequests) {
         var4 = var2.galaxyRequests.getReceiveBuffer().iterator();

         while(true) {
            if (!var4.hasNext()) {
               break;
            }

            var5 = (RemoteGalaxyRequest)var4.next();
            this.enqueueRequestOrAwnser((GalaxyRequestAndAwnser)var5.get(), var2);
         }
      }

      synchronized(var2.galaxyZoneRequests) {
         var4 = var2.galaxyZoneRequests.getReceiveBuffer().iterator();

         while(true) {
            if (!var4.hasNext()) {
               break;
            }

            RemoteGalaxyZoneRequest var16 = (RemoteGalaxyZoneRequest)var4.next();
            this.enqueueZoneRequestOrAwnser((GalaxyZoneRequestAndAwnser)var16.get(), var2);
         }
      }

      synchronized(var2.galaxyServerMods) {
         var4 = var2.galaxyServerMods.getReceiveBuffer().iterator();

         while(true) {
            if (!var4.hasNext()) {
               break;
            }

            var5 = (RemoteGalaxyRequest)var4.next();

            assert !this.isOnServer();

            this.enqueueRequestOrAwnser((GalaxyRequestAndAwnser)var5.get(), var2);
         }
      }

      synchronized(var2.ftlUpdatesAndRequests) {
         var4 = var2.ftlUpdatesAndRequests.getReceiveBuffer().iterator();

         while(var4.hasNext()) {
            RemoteFTLConnection var17 = (RemoteFTLConnection)var4.next();

            assert !this.isOnServer();

            synchronized(this.toAddFTLDataOnClient) {
               this.toAddFTLDataOnClient.enqueue(var17.get());
            }
         }
      }

      synchronized(var2.tradeNodeUpdatesAndRequests) {
         var4 = var2.tradeNodeUpdatesAndRequests.getReceiveBuffer().iterator();

         while(var4.hasNext()) {
            RemoteTradeNode var18 = (RemoteTradeNode)var4.next();

            assert !this.isOnServer();

            synchronized(this.toAddTradeDataOnClient) {
               this.toAddTradeDataOnClient.enqueue(var18.get());
            }
         }
      }

      synchronized(var2.fogOfWarRequestsAndAwnsers) {
         var4 = var2.fogOfWarRequestsAndAwnsers.getReceiveBuffer().iterator();

         while(var4.hasNext()) {
            RemoteFowRequestAndAwnser var19;
            ((FowRequestAndAwnser)(var19 = (RemoteFowRequestAndAwnser)var4.next()).get()).receivedClientChannel = var1;
            if (this.isOnServer()) {
               this.enqueueFowRequestOrAwnser((FowRequestAndAwnser)var19.get(), var2);
            } else {
               synchronized(this.fowAwnsersOnClient) {
                  this.fowAwnsersOnClient.enqueue(var19.get());
               }
            }
         }

      }
   }

   private void enqueueRequestOrAwnser(GalaxyRequestAndAwnser var1, NetworkClientChannel var2) {
      synchronized(this.serverRequestsAndClientAwnsers) {
         this.serverRequestsAndClientAwnsers.enqueue(new GalaxyManager.Request(var2, var1));
         this.serverRequestsAndClientAwnsers.notify();
      }
   }

   private void enqueueFowRequestOrAwnser(FowRequestAndAwnser var1, NetworkClientChannel var2) {
      assert var1.receivedClientChannel != null;

      synchronized(this.serverFowRequests) {
         this.serverFowRequests.enqueue(var1);
         this.serverFowRequests.notify();
      }
   }

   private void enqueueZoneRequestOrAwnser(GalaxyZoneRequestAndAwnser var1, NetworkClientChannel var2) {
      synchronized(this.serverZoneRequestsAndClientAwnsers) {
         this.serverZoneRequestsAndClientAwnsers.enqueue(new GalaxyManager.ZoneRequest(var2, var1));
         this.serverZoneRequestsAndClientAwnsers.notify();
      }
   }

   public void shutdown() {
      this.shutdown = true;
      synchronized(this.serverRequestsAndClientAwnsers) {
         this.serverRequestsAndClientAwnsers.notify();
      }

      synchronized(this.serverZoneRequestsAndClientAwnsers) {
         this.serverZoneRequestsAndClientAwnsers.notify();
      }
   }

   public void resetClientVisibility() {
      this.visibility.clear();
      this.requestedVisibility.clear();
   }

   public void resetClientVisibilitySystem(Vector3i var1) {
      this.visibility.remove(var1);
      this.requestedVisibility.remove(var1);
   }

   public boolean isSystemVisiblyClient(Vector3i var1) {
      if (!this.requestedVisibility.contains(var1)) {
         ClientChannel var2;
         synchronized((var2 = ((GameClientState)this.state).getController().getClientChannel()).getNetworkObject().fogOfWarRequestsAndAwnsers) {
            FowRequestAndAwnser var4;
            (var4 = new FowRequestAndAwnser()).sysX = var1.x;
            var4.sysY = var1.y;
            var4.sysZ = var1.z;
            var2.getNetworkObject().fogOfWarRequestsAndAwnsers.add(new RemoteFowRequestAndAwnser(var4, false));
         }

         this.requestedVisibility.add(new Vector3i(var1));
      }

      return this.visibility.getBoolean(var1);
   }

   public boolean isSectorVisiblyClientIncludingLastVisited(Vector3i var1) {
      return this.visibility.getBoolean(VoidSystem.getContainingSystem(var1, this.sysTmp)) || ((GameClientState)this.state).getPlayer().getLastVisitedSectors().contains(var1);
   }

   public void startThreads() {
      if (this.isOnServer()) {
         (new Thread(new Runnable() {
            public void run() {
               assert GalaxyManager.this.isOnServer();

               while(!GalaxyManager.this.shutdown) {
                  FowRequestAndAwnser var1;
                  synchronized(GalaxyManager.this.serverFowRequests) {
                     while(GalaxyManager.this.serverFowRequests.isEmpty()) {
                        try {
                           GalaxyManager.this.serverFowRequests.wait();
                           if (!GalaxyManager.this.shutdown) {
                              continue;
                           }
                        } catch (InterruptedException var4) {
                           var4.printStackTrace();
                           throw new RuntimeException(var4);
                        }

                        return;
                     }

                     var1 = (FowRequestAndAwnser)GalaxyManager.this.serverFowRequests.dequeue();
                  }

                  var1.visible = var1.receivedClientChannel.getPlayer().getFogOfWar().isVisibleSystemServer(new Vector3i(var1.sysX, var1.sysY, var1.sysZ));
                  synchronized(GalaxyManager.this.serverFowAwnsers) {
                     GalaxyManager.this.serverFowAwnsers.enqueue(var1);
                  }
               }

            }
         }, "FogOfWarRequestThread" + (this.isOnServer() ? "Server" : "Client"))).start();
      }

      (new Thread(new Runnable() {
         public void run() {
            while(!GalaxyManager.this.shutdown) {
               GalaxyManager.Request var1;
               synchronized(GalaxyManager.this.serverRequestsAndClientAwnsers) {
                  while(true) {
                     if (GalaxyManager.this.serverRequestsAndClientAwnsers.isEmpty()) {
                        try {
                           GalaxyManager.this.serverRequestsAndClientAwnsers.wait();
                           if (!GalaxyManager.this.shutdown) {
                              continue;
                           }
                        } catch (InterruptedException var3) {
                           var3.printStackTrace();
                           throw new RuntimeException(var3);
                        }

                        return;
                     }

                     var1 = (GalaxyManager.Request)GalaxyManager.this.serverRequestsAndClientAwnsers.dequeue();
                     break;
                  }
               }

               if (GalaxyManager.this.isOnServer()) {
                  GalaxyManager.this.awnserRequestOnServer(var1);
               } else {
                  GalaxyManager.this.processAwnserOnClient(var1);
               }
            }

         }
      }, "GalaxyRequestThread" + (this.isOnServer() ? "Server" : "Client"))).start();
      (new Thread(new Runnable() {
         public void run() {
            while(!GalaxyManager.this.shutdown) {
               GalaxyManager.ZoneRequest var1;
               synchronized(GalaxyManager.this.serverZoneRequestsAndClientAwnsers) {
                  while(true) {
                     if (GalaxyManager.this.serverZoneRequestsAndClientAwnsers.isEmpty()) {
                        try {
                           GalaxyManager.this.serverZoneRequestsAndClientAwnsers.wait();
                           if (!GalaxyManager.this.shutdown) {
                              continue;
                           }
                        } catch (InterruptedException var3) {
                           var3.printStackTrace();
                           throw new RuntimeException(var3);
                        }

                        return;
                     }

                     var1 = (GalaxyManager.ZoneRequest)GalaxyManager.this.serverZoneRequestsAndClientAwnsers.dequeue();
                     break;
                  }
               }

               if (GalaxyManager.this.isOnServer()) {
                  GalaxyManager.this.awnserZoneRequestOnServer(var1);
               } else {
                  GalaxyManager.this.processZoneAwnserOnClient(var1);
               }
            }

         }
      }, "GalaxyZoneRequestThread" + (this.isOnServer() ? "Server" : "Client"))).start();
      if (this.isOnServer()) {
         (new Thread(new Runnable() {
            public void run() {
               if (GalaxyManager.this.isOnServer()) {
                  while(!GalaxyManager.this.shutdown) {
                     GalaxyManager.PriceRequest var1;
                     synchronized(GalaxyManager.this.priceRequests) {
                        while(true) {
                           if (GalaxyManager.this.priceRequests.isEmpty()) {
                              try {
                                 GalaxyManager.this.priceRequests.wait();
                                 if (!GalaxyManager.this.shutdown) {
                                    continue;
                                 }
                              } catch (InterruptedException var3) {
                                 var3.printStackTrace();
                                 throw new RuntimeException(var3);
                              }

                              return;
                           }

                           var1 = (GalaxyManager.PriceRequest)GalaxyManager.this.priceRequests.dequeue();
                           break;
                        }
                     }

                     if (GalaxyManager.this.isOnServer()) {
                        GalaxyManager.this.processPriceRequestThreadedDB(var1);
                     }
                  }
               }

            }
         }, "PricesRequestThread" + (this.isOnServer() ? "Server" : "Client"))).start();
      }

   }

   public void updateServer() {
      assert this.isOnServer();

      if (!this.serverAwnsers.isEmpty()) {
         synchronized(this.serverAwnsers) {
            while(!this.serverAwnsers.isEmpty()) {
               GalaxyRequestAndAwnser var2 = (GalaxyRequestAndAwnser)this.serverAwnsers.dequeue();
               System.err.println("[SERVER][GALAXY] SENDING SYSTEM BACK TO CLIENT: " + var2.secX + ", " + var2.secY + ", " + var2.secZ);
               var2.networkObjectOnServer.galaxyRequests.add(new RemoteGalaxyRequest(var2, this.isOnServer()));

               assert var2.networkObjectOnServer.galaxyRequests.hasChanged();

               assert var2.networkObjectOnServer.isChanged();
            }
         }
      }

      if (!this.serverFowAwnsers.isEmpty()) {
         synchronized(this.serverFowAwnsers) {
            while(!this.serverFowAwnsers.isEmpty()) {
               FowRequestAndAwnser var7;
               (var7 = (FowRequestAndAwnser)this.serverFowAwnsers.dequeue()).receivedClientChannel.getNetworkObject().fogOfWarRequestsAndAwnsers.add(new RemoteFowRequestAndAwnser(var7, this.isOnServer()));
            }
         }
      }

      if (!this.serverZonesAwnsers.isEmpty()) {
         synchronized(this.serverZonesAwnsers) {
            while(!this.serverZonesAwnsers.isEmpty()) {
               GalaxyZoneRequestAndAwnser var8 = (GalaxyZoneRequestAndAwnser)this.serverZonesAwnsers.dequeue();

               assert var8 != null;

               assert var8.networkObjectOnServer != null;

               var8.networkObjectOnServer.galaxyZoneRequests.add(new RemoteGalaxyZoneRequest(var8, this.isOnServer()));
            }
         }
      }

      if (!this.priceRequestsAwnsered.isEmpty()) {
         synchronized(this.priceRequestsAwnsered) {
            while(!this.priceRequestsAwnsered.isEmpty()) {
               GalaxyManager.PriceRequest var9 = (GalaxyManager.PriceRequest)this.priceRequestsAwnsered.dequeue();

               assert var9 != null;

               this.processPriceRequestAwnser(var9);
            }

         }
      }
   }

   private void processPriceRequestAwnser(GalaxyManager.PriceRequest var1) {
      assert this.isOnServer();

      var1.chan.getNetworkObject().pricesOfShopAwnser.add(new RemoteTradePrice(var1.response, this.isOnServer()));
   }

   public void broadcastPrices(TradePrices var1) {
      assert this.isOnServer();

      Iterator var2 = ((GameServerState)this.state).getClients().values().iterator();

      while(var2.hasNext()) {
         RegisteredClientOnServer var3;
         Sendable var4;
         if ((var4 = (Sendable)(var3 = (RegisteredClientOnServer)var2.next()).getLocalAndRemoteObjectContainer().getLocalObjects().get(0)) != null) {
            System.err.println("[SERVER] SENDING DIRTY DB PRICES ");
            ((ClientChannel)var4).getNetworkObject().pricesOfShopAwnser.add(new RemoteTradePrice(var1, this.isOnServer()));
         } else {
            System.err.println("[SENDABLEGAMESTATE] WARNING: Cannot send prices mod to " + var3 + ": no client channel!");
         }
      }

   }

   public void updateClient() {
      assert !this.isOnServer();

      if (!this.toAddFTLDataOnClient.isEmpty()) {
         synchronized(this.state) {
            synchronized(this.toAddFTLDataOnClient) {
               while(!this.toAddFTLDataOnClient.isEmpty()) {
                  FTLConnection var3;
                  if ((var3 = (FTLConnection)this.toAddFTLDataOnClient.dequeue()).to == null) {
                     this.ftlData.remove(var3.from);
                  } else {
                     this.ftlData.put(var3.from, var3);
                  }
               }
            }
         }
      }

      if (!this.fowAwnsersOnClient.isEmpty()) {
         synchronized(this.fowAwnsersOnClient) {
            while(!this.fowAwnsersOnClient.isEmpty()) {
               FowRequestAndAwnser var2 = (FowRequestAndAwnser)this.fowAwnsersOnClient.dequeue();
               this.visibility.put(new Vector3i(var2.sysX, var2.sysY, var2.sysZ), var2.visible);
            }
         }
      }

      if (!this.toAddTradeDataOnClient.isEmpty()) {
         synchronized(this.state) {
            synchronized(this.toAddTradeDataOnClient) {
               System.err.println("[CLIENT] RECEIVED TRADE NODES: " + this.toAddTradeDataOnClient.size());

               while(true) {
                  if (this.toAddTradeDataOnClient.isEmpty()) {
                     this.tradeDataListener.onChanged();
                     break;
                  }

                  TradeNodeStub var16 = (TradeNodeStub)this.toAddTradeDataOnClient.dequeue();

                  assert var16.getEntityDBId() > 0L : var16.getEntityDBId();

                  TradeNodeStub var4;
                  if (var16.remove) {
                     List var5;
                     if ((var4 = (TradeNodeStub)this.tradeDataById.remove(var16.getEntityDBId())) != null && (var5 = (List)this.tradeData.get(var4.getSystem())) != null) {
                        for(int var6 = 0; var6 < var5.size(); ++var6) {
                           if (((TradeNodeStub)var5.get(var6)).getEntityDBId() == var16.getEntityDBId()) {
                              var5.remove(var6);
                              break;
                           }
                        }
                     }
                  } else if ((var4 = (TradeNodeStub)this.tradeDataById.get(var16.getEntityDBId())) != null) {
                     Vector3i var21 = new Vector3i(var4.getSystem());
                     var4.updateWith(var16);
                     if (!var21.equals(var4.getSystem())) {
                        List var25;
                        if ((var25 = (List)this.tradeData.get(var21)) != null) {
                           for(int var17 = 0; var17 < var25.size(); ++var17) {
                              if (((TradeNodeStub)var25.get(var17)).getEntityDBId() == var4.getEntityDBId()) {
                                 var25.remove(var17);
                                 break;
                              }
                           }
                        }

                        Object var18;
                        if ((var18 = (List)this.tradeData.get(var4.getSystem())) == null) {
                           var18 = new ObjectArrayList();
                           this.tradeData.put(var4.getSystem(), var18);
                        }

                        ((List)var18).add(var4);
                     }
                  } else {
                     ((TradeNodeClient)var16).spawnPriceListener();
                     this.tradeDataById.put(var16.getEntityDBId(), var16);
                     Object var23;
                     if ((var23 = (List)this.tradeData.get(var16.getSystem())) == null) {
                        var23 = new ObjectArrayList();
                        this.tradeData.put(var16.getSystem(), var23);
                     }

                     ((List)var23).add(var16);
                  }

                  System.err.println("[CLIENT] Total nodes: " + this.tradeDataById.size());
               }
            }
         }
      }

      if (!this.zoneAwnsersOnClient.isEmpty()) {
         synchronized(this.zoneAwnsersOnClient) {
            ObjectIterator var15 = this.zoneAwnsersOnClient.entrySet().iterator();
            long var20 = System.currentTimeMillis();

            while(var15.hasNext()) {
               Entry var24;
               if ((Long)(var24 = (Entry)var15.next()).getValue() < var20) {
                  this.checkSystemBySectorPos(new Vector3i(((Vector3i)var24.getKey()).x << 4, ((Vector3i)var24.getKey()).y << 4, ((Vector3i)var24.getKey()).z << 4));
                  var15.remove();
               }
            }
         }
      }

      if (!this.clientSystemsToAdd.isEmpty()) {
         synchronized(this.state) {
            synchronized(this.clientSystemsToAdd) {
               while(!this.clientSystemsToAdd.isEmpty()) {
                  VoidSystem var22 = (VoidSystem)this.clientSystemsToAdd.dequeue();
                  VoidSystem var19 = (VoidSystem)this.clientData.put(var22.getPos(), var22);

                  assert this.requested.remove(var22.getPos()) == null || var19 == null || var19.getPos().equals(var22.getPos()) : var19.getPos() + "; " + var22.getPos();
               }
            }

         }
      }
   }

   private void processAwnserOnClient(GalaxyManager.Request var1) {
      VoidSystem var2 = new VoidSystem();
      StellarSystem.getPosFromSector(new Vector3i(var1.g.secX, var1.g.secY, var1.g.secZ), var2.getPos());
      var2.setOwnerFaction(var1.g.factionUID);
      var2.setOwnerUID(var1.g.ownerUID);
      var2.setOwnerPos(new Vector3i(var1.g.secX, var1.g.secY, var1.g.secZ));
      synchronized(this.clientSystemsToAdd) {
         this.clientSystemsToAdd.enqueue(var2);
      }
   }

   private void processZoneAwnserOnClient(GalaxyManager.ZoneRequest var1) {
      GalaxyZoneRequestAndAwnser var7;
      Iterator var2 = (var7 = var1.g).buffer.iterator();

      while(var2.hasNext()) {
         GalaxyRequestAndAwnser var3 = (GalaxyRequestAndAwnser)var2.next();
         VoidSystem var4 = new VoidSystem();
         StellarSystem.getPosFromSector(new Vector3i(var3.secX, var3.secY, var3.secZ), var4.getPos());
         var4.setOwnerFaction(var3.factionUID);
         var4.setOwnerUID(var3.ownerUID);
         var4.setOwnerPos(new Vector3i(var3.secX, var3.secY, var3.secZ));
         synchronized(this.clientSystemsToAdd) {
            this.clientSystemsToAdd.enqueue(var4);
         }
      }

      synchronized(this.zoneAwnsersOnClient) {
         this.zoneAwnsersOnClient.put(new Vector3i(var7.startSystem), System.currentTimeMillis() + 5000L + (long)(Math.random() * 2000.0D));
      }
   }

   private void awnserRequestOnServer(GalaxyManager.Request var1) {
      long var2 = System.currentTimeMillis();
      var1.getSystemPos(this.tmpPosSeverAwner);
      boolean var4 = ((GameServerState)this.state).getDatabaseIndex().getTableManager().getSystemTable().loadSystem((GameServerState)this.state, this.tmpPosSeverAwner, this.tmpVoidSystemServer);
      GalaxyRequestAndAwnser var5;
      (var5 = new GalaxyRequestAndAwnser()).secX = var1.g.secX;
      var5.secY = var1.g.secY;
      var5.secZ = var1.g.secZ;
      var5.networkObjectOnServer = var1.c;
      if (var4) {
         var5.factionUID = this.tmpVoidSystemServer.getOwnerFaction();
         var5.ownerUID = this.tmpVoidSystemServer.getOwnerUID();
      }

      System.err.println("[SERVER][GALAXY] REQUESTING SYSTEM " + this.tmpPosSeverAwner + "; exists in DB: " + var4 + "; TIME: " + (System.currentTimeMillis() - var2) + "ms: " + var5);
      synchronized(this.serverAwnsers) {
         this.serverAwnsers.enqueue(var5);
      }
   }

   private void awnserZoneRequestOnServer(GalaxyManager.ZoneRequest var1) {
      System.currentTimeMillis();
      GalaxyZoneRequestAndAwnser var2;
      synchronized(this.zoneMap) {
         if ((var2 = (GalaxyZoneRequestAndAwnser)this.zoneMap.get(var1.g.startSystem)) != null) {
            var2 = new GalaxyZoneRequestAndAwnser(var2);
         }
      }

      if (var2 == null) {
         (var2 = new GalaxyZoneRequestAndAwnser()).buffer = new ObjectArrayList();
         var2.startSystem = new Vector3i(var1.g.startSystem);
         var2.zoneSize = var1.g.zoneSize;
         ((GameServerState)this.state).getDatabaseIndex().getTableManager().getSystemTable().loadSystemZone(var1.g.startSystem, var1.g.zoneSize, var2.buffer);
         synchronized(this.zoneMap) {
            this.zoneMap.put(var2.startSystem, var2);
         }
      }

      var2.networkObjectOnServer = var1.c;
      synchronized(this.serverZonesAwnsers) {
         this.serverZonesAwnsers.enqueue(var2);
      }
   }

   public void markZoneDirty(Vector3i var1) {
      synchronized(this.zoneMap) {
         Vector3i var3 = getContainingZoneFromSystem(var1, 32, new Vector3i());
         if ((GalaxyZoneRequestAndAwnser)this.zoneMap.remove(var3) == null) {
            System.err.println("[SERVER][GALAXYMANAGER] dirty galaxy zone could not be removed from cache. Changed " + var1 + "; zoneStart: " + var3 + "; current zones cached: " + this.zoneMap.keySet());
         } else {
            System.err.println("[SERVER][GALAXYMANAGER] galaxy zone has been marked dirty and has been removed from cache. Changed " + var1 + ";");
         }

      }
   }

   public void markZoneDirty(StellarSystem var1) {
      this.markZoneDirty(var1.getPos());
   }

   public Map getClientData() {
      return this.clientData;
   }

   public void sendDirectTradeUpdateOnServer(long var1) {
      assert this.isOnServer();

      try {
         TradeNodeStub var3;
         boolean var4;
         int var12;
         if ((var3 = ((GameServerState)this.state).getDatabaseIndex().getTableManager().getTradeNodeTable().getTradeNode(var1)) == null) {
            TradeNodeStub var5;
            if ((var5 = (TradeNodeStub)this.tradeDataById.remove(var1)) != null) {
               if (FactionManager.isNPCFaction(var5.getFactionId())) {
                  try {
                     ((GameServerState)this.state).getController().broadcastMessageAdmin(new Object[]{416}, 3);
                     throw new Exception("REMOVED NPC TRADE NODE: " + var1 + ": " + var5);
                  } catch (Exception var8) {
                     var8.printStackTrace();
                  }
               }

               System.err.println("[SERVER][GALAXYMANAGER] removed trade node: " + var1 + ": " + var5);
               Sendable var6;

               assert (var6 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getDbObjects().get(var1)) == null || !(var6 instanceof SpaceStation) || !((SpaceStation)var6).isNPCHomeBase() : var6;

               List var7;
               if ((var7 = (List)this.tradeData.get(var5.getSystem())) != null) {
                  for(var12 = 0; var12 < var7.size(); ++var12) {
                     if (((TradeNodeStub)var7.get(var12)).getEntityDBId() == var1) {
                        var7.remove(var12);
                        break;
                     }
                  }
               }
            }

            var4 = true;
         } else {
            this.tradeDataById.put(var3.getEntityDBId(), var3);
            List var10;
            if ((var10 = (List)this.tradeData.get(var3.getSystem())) != null) {
               for(var12 = 0; var12 < var10.size(); ++var12) {
                  if (((TradeNodeStub)var10.get(var12)).getEntityDBId() == var1) {
                     var10.remove(var12);
                     break;
                  }
               }

               var10.add(var3);
            }

            var4 = false;
         }

         Iterator var11 = ((GameServerState)this.state).getClients().values().iterator();

         while(var11.hasNext()) {
            Sendable var13;
            RegisteredClientOnServer var14;
            if ((var13 = (Sendable)(var14 = (RegisteredClientOnServer)var11.next()).getLocalAndRemoteObjectContainer().getLocalObjects().get(0)) != null) {
               System.err.println("[SERVER] SENDING DIRTY TRADE NODES " + var3 + "; null is remove");
               if (var4) {
                  assert var3 == null;

                  TradeNodeStub var15 = new TradeNodeStub();

                  assert var1 > 0L : var1;

                  var15.setEntityDBId(var1);
                  var15.remove = true;
                  ((ClientChannel)var13).getNetworkObject().tradeNodeUpdatesAndRequests.add(new RemoteTradeNode(var15, ((ClientChannel)var13).getNetworkObject()));
               } else {
                  assert var3 != null;

                  ((ClientChannel)var13).getNetworkObject().tradeNodeUpdatesAndRequests.add(new RemoteTradeNode(var3, ((ClientChannel)var13).getNetworkObject()));
               }
            } else {
               System.err.println("[SENDABLEGAMESTATE] WARNING: Cannot send trade node mod to " + var14 + ": no client channel!");
            }
         }

      } catch (SQLException var9) {
         var9.printStackTrace();
      }
   }

   public void sendDirectFtlUpdateOnServer(Vector3i var1) {
      assert this.isOnServer();

      FTLConnection var2;
      if ((var2 = ((GameServerState)this.state).getDatabaseIndex().getTableManager().getFTLTable().getFtl(var1)) == null) {
         (var2 = new FTLConnection()).from = new Vector3i(var1);
         this.ftlData.remove(var2.from);
      } else {
         this.ftlData.put(var2.from, var2);
      }

      Iterator var3 = ((GameServerState)this.state).getClients().values().iterator();

      while(var3.hasNext()) {
         RegisteredClientOnServer var4;
         Sendable var5;
         if ((var5 = (Sendable)(var4 = (RegisteredClientOnServer)var3.next()).getLocalAndRemoteObjectContainer().getLocalObjects().get(0)) != null) {
            System.err.println("[SERVER] SENDING DIRTY FTL " + var1);
            ((ClientChannel)var5).getNetworkObject().ftlUpdatesAndRequests.add(new RemoteFTLConnection(var2, ((ClientChannel)var5).getNetworkObject()));
         } else {
            System.err.println("[SENDABLEGAMESTATE] WARNING: Cannot send ftl mod to " + var4 + ": no client channel!");
         }
      }

   }

   public void sendAllFtlDataTo(NetworkClientChannel var1) {
      Iterator var2 = this.ftlData.values().iterator();

      while(var2.hasNext()) {
         FTLConnection var3 = (FTLConnection)var2.next();
         var1.ftlUpdatesAndRequests.add(new RemoteFTLConnection(var3, var1));
      }

   }

   public void sendAllTradeStubsTo(NetworkClientChannel var1) {
      Iterator var2 = this.tradeDataById.values().iterator();

      while(var2.hasNext()) {
         TradeNodeStub var3 = (TradeNodeStub)var2.next();
         var1.tradeNodeUpdatesAndRequests.add(new RemoteTradeNode(var3, var1));
      }

   }

   public Object2ObjectOpenHashMap getFtlData() {
      return this.ftlData;
   }

   public void initializeOnServer() {
      assert this.isOnServer();

      ((GameServerState)this.state).getDatabaseIndex().getTableManager().getFTLTable().fillFTLData(this.ftlData);
      ((GameServerState)this.state).getDatabaseIndex().getTableManager().getTradeNodeTable().fillTradeNodeStubData(this.tradeData, this.tradeDataById);
   }

   public void enqueuePricesRequest(long var1, ClientChannel var3) {
      synchronized(this.priceRequests) {
         this.priceRequests.enqueue(new GalaxyManager.PriceRequest(var1, var3));
         this.priceRequests.notify();
      }
   }

   private void processPriceRequestThreadedDB(GalaxyManager.PriceRequest var1) {
      assert this.isOnServer();

      DataInputStream var2;
      if ((var2 = ((GameServerState)this.state).getDatabaseIndex().getTableManager().getTradeNodeTable().getTradePricesAsStream(var1.dbEnt)) != null) {
         try {
            TradePrices var3 = ShoppingAddOn.deserializeTradePrices(var2, true);
            var2.close();
            var1.response = var3;
            synchronized(this.priceRequestsAwnsered) {
               this.priceRequestsAwnsered.enqueue(var1);
            }
         } catch (IOException var5) {
            var5.printStackTrace();
         }
      } else {
         System.err.println("[SERVER][TRADE] trade prices request failed for entDbID: " + var1.dbEnt);
      }
   }

   public Object2ObjectOpenHashMap getTradeNodes() {
      return this.tradeData;
   }

   public void updatedPricesOnClient(TradePrices var1) {
      TradeNodeClient var2;
      if ((var2 = (TradeNodeClient)this.tradeDataById.get(var1.entDbId)) != null) {
         var2.receiveTradePrices(var1.getPrices());
      }

   }

   public Long2ObjectOpenHashMap getTradeNodeDataById() {
      return this.tradeDataById;
   }

   class PriceRequest {
      long dbEnt;
      ClientChannel chan;
      public TradePrices response;

      public PriceRequest(long var2, ClientChannel var4) {
         this.dbEnt = var2;
         this.chan = var4;
      }
   }

   class ZoneRequest {
      final NetworkClientChannel c;
      final GalaxyZoneRequestAndAwnser g;

      public ZoneRequest(NetworkClientChannel var2, GalaxyZoneRequestAndAwnser var3) {
         this.c = var2;
         this.g = var3;
      }
   }

   class Request {
      final NetworkClientChannel c;
      final GalaxyRequestAndAwnser g;

      public Request(NetworkClientChannel var2, GalaxyRequestAndAwnser var3) {
         this.c = var2;
         this.g = var3;
      }

      public void getSystemPos(Vector3i var1) {
         StellarSystem.getPosFromSector(new Vector3i(this.g.secX, this.g.secY, this.g.secZ), var1);
      }
   }

   public class TradeDataListener extends Observable {
      public void onChanged() {
         this.setChanged();
         this.notifyObservers(GalaxyManager.this);
      }
   }
}
