package org.schema.game.common.data.world;

import com.bulletphysics.linearmath.Transform;

public class SectorTransformation {
   public final Transform t;
   public final int sectorId;

   public SectorTransformation(Transform var1, int var2) {
      this.t = var1;
      this.sectorId = var2;
   }

   public String toString() {
      return "SectorTransformation [t=" + this.t.origin + ", sectorId=" + this.sectorId + "]";
   }
}
