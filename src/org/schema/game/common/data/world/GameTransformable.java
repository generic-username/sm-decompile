package org.schema.game.common.data.world;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.constraintsolver.SolverConstraint;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.schine.physics.Physical;

public interface GameTransformable extends SimpleTransformable, Physical {
   void engageWarp(String var1, boolean var2, long var3, Vector3i var5, int var6);

   void sendControllingPlayersServerMessage(Object[] var1, int var2);

   boolean handleCollision(int var1, RigidBody var2, RigidBody var3, SolverConstraint var4);

   int getSectorId();

   void getGravityAABB(Transform var1, Vector3f var2, Vector3f var3);

   int getId();
}
