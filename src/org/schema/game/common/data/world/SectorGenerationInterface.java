package org.schema.game.common.data.world;

import java.util.Random;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;

public interface SectorGenerationInterface {
   boolean staticSectorGeneration(GameServerState var1, int var2, int var3, int var4, StellarSystem var5, int var6, Galaxy var7, Random var8);

   void generate(GameServerState var1, int var2, int var3, int var4, StellarSystem var5, int var6, Galaxy var7, Random var8);

   boolean orbitTakenByGeneration(GameServerState var1, int var2, int var3, int var4, StellarSystem var5, int var6, Galaxy var7, Random var8);

   void definitePlanet(GameServerState var1, int var2, int var3, int var4, StellarSystem var5, int var6, Galaxy var7, Random var8);

   boolean onOrbitButNoPlanet(GameServerState var1, int var2, int var3, int var4, StellarSystem var5, int var6, Galaxy var7, Random var8);

   void onAsteroidBelt(GameServerState var1, int var2, int var3, int var4, StellarSystem var5, int var6, Galaxy var7, Random var8);
}
