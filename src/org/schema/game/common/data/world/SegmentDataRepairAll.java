package org.schema.game.common.data.world;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import javax.vecmath.Vector3f;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.PhysicsExt;
import org.schema.game.common.data.physics.octree.ArrayOctree;

public class SegmentDataRepairAll implements SegmentDataInterface {
   public static final byte ANTI_BYTE = -16;
   public static final int lightBlockSize = 39;
   public static final int typeIndexStart = 0;
   public static final int typeIndexEnd = 11;
   public static final int hitpointsIndexStart = 11;
   public static final int hitpointsIndexEnd = 19;
   public static final int activeIndexStart = 19;
   public static final int activeIndexEnd = 20;
   public static final int orientationStart = 20;
   public static final int orientationEnd = 24;
   public static final int blockSize = 3;
   public static final byte ACTIVE_BIT = 8;
   public static final byte[] typeMap = new byte[24576];
   public static final int maxHp = 256;
   public static final byte[] hpMap = new byte[768];
   public static final int maxOrient = 16;
   public static final byte[] orientMap = new byte[48];
   public static final int SEG = 16;
   public static final int BLOCK_COUNT = 4096;
   public static final int TOTAL_SIZE = 12288;
   public static final int TOTAL_SIZE_LIGHT = 159744;
   public static final int SEG_TIMES_SEG_TIMES_SEG = 4096;
   public static final int SEG_TIMES_SEG = 256;
   public static final int SEG_MINUS_ONE = 15;
   public static final int t = 255;
   public static final int PIECE_ADDED = 0;
   public static final int PIECE_REMOVED = 1;
   public static final int PIECE_CHANGED = 2;
   public static final int PIECE_UNCHANGED = 3;
   public static final int PIECE_ACTIVE_CHANGED = 4;
   public static final int SEG_HALF = 8;
   static final long yDelim = 65536L;
   static final long zDelim = 4294967296L;
   private static final int MASK = 255;
   private static final int TYPE_SECOND_MASK = 7;
   private static final int[] lookUpSecType = createLookup();
   private static final int[] raisedHP = new int[]{591, 598, 599, 600, 601, 602, 263, 311, 320, 402, 401, 608, 609, 610, 611, 612, 271, 319, 328, 384, 376, 507, 508, 509, 510, 511, 603, 604, 605, 606, 607, 264, 312, 321, 377, 369, 593, 594, 595, 596, 597, 628, 629, 630, 631, 632, 270, 318, 327, 383, 375, 522, 523, 524, 525, 526, 633, 634, 635, 636, 637, 431, 432, 433, 434, 435, 517, 518, 519, 520, 521, 638, 639, 640, 641, 642, 265, 313, 322, 378, 370, 512, 513, 514, 515, 516, 613, 614, 615, 616, 617, 266, 314, 323, 379, 371, 537, 538, 539, 540, 541, 618, 620, 619, 621, 622, 267, 315, 324, 380, 372, 532, 533, 534, 535, 536, 623, 624, 625, 626, 627, 268, 316, 325, 381, 373, 527, 528, 529, 530, 531, 643, 644, 645, 646, 647, 269, 317, 326, 382, 374, 63, 329, 330, 368, 367, 289, 7, 122, 588, 479, 418, 419, 420, 421, 424, 425, 422, 423, 460, 461, 462, 463, 464, 465, 466, 467, 476, 477, 123, 120, 331, 654, 655, 6, 16, 38, 32, 414, 415, 416, 417, 4, 24, 39, 30, 46, 40, 54, 48, 334, 335, 332, 333, 344, 345, 3, 478, 22, 15, 47, 544, 211, 113, 347};
   private static final IntOpenHashSet m;
   public static int MAX_INDEX;
   public final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
   private final Vector3b min = new Vector3b();
   private final Vector3b max = new Vector3b();
   private final ArrayOctree octree;
   private final boolean onServer;
   private Segment segment;
   private byte[] data;
   private int size;
   private boolean preserveControl;
   private boolean needsRevalidate = false;
   private boolean revalidating;
   private boolean blockAddedForced;

   public SegmentDataRepairAll(boolean var1) {
      this.onServer = !var1;
      this.octree = new ArrayOctree(this.onServer);
      this.data = new byte[12288];
      this.resetBB();
   }

   public static float byteArrayToFloat(byte[] var0) {
      int var1 = 0;
      int var2 = 0;

      for(int var3 = 3; var3 >= 0; --var3) {
         var1 |= (var0[var2] & 255) << (var3 << 3);
         ++var2;
      }

      return Float.intBitsToFloat(var1);
   }

   public static byte[] floatToByteArray(float var0) {
      return intToByteArray(Float.floatToRawIntBits(var0));
   }

   public static byte[] intToByteArray(int var0) {
      byte[] var1 = new byte[4];

      for(int var2 = 0; var2 < 4; ++var2) {
         int var3 = var1.length - 1 - var2 << 3;
         var1[var2] = (byte)(var0 >>> var3);
      }

      return var1;
   }

   public static void main(String[] var0) {
      byte[] var6;
      byte[] var10000 = var6 = new byte[3];
      var10000[0] = (byte)(var10000[0] | 8);

      assert (var6[0] & 8) != 0 : var6[0] + "; true";

      var6[0] &= -9;

      assert (var6[0] & 8) == 0;

      var6[0] = (byte)(var6[0] | 8);

      assert (var6[0] & 8) != 0;

      Random var1 = new Random();

      short var2;
      int var4;
      short var7;
      short var8;
      for(var2 = 0; var2 < 256; ++var2) {
         var6[0] = (byte)var1.nextInt(256);
         var6[1] = (byte)var1.nextInt(256);
         var6[2] = (byte)var1.nextInt(256);
         var4 = var2 * 3;
         var6[0] = (byte)(var6[0] & ~hpMap[hpMap.length - 3]);
         var6[1] = (byte)(var6[1] & ~hpMap[hpMap.length - 2]);
         var6[0] |= hpMap[var4];
         var6[1] |= hpMap[var4 + 1];
         var4 = var6[1] & 255;
         int var5 = var6[0] & 255;
         var7 = (short)(((var4 & hpMap[hpMap.length - 2]) >> 3) + ((var5 & hpMap[hpMap.length - 3]) << 5));
         var8 = (short)ByteUtil.extractInt(ByteUtil.intRead3ByteArray(var6, 0), 11, 19, new Object());
         System.err.println(var2 + " -> " + var7 + "; ; " + var8);

         assert var8 == var2;

         assert var8 == var7;
      }

      for(var2 = 0; var2 < 16; ++var2) {
         var6[0] = (byte)var1.nextInt(256);
         var6[1] = (byte)var1.nextInt(256);
         var6[2] = (byte)var1.nextInt(256);
         var4 = var2 * 3;
         var6[0] = (byte)(var6[0] & ~orientMap[orientMap.length - 3]);
         var6[1] = (byte)(var6[1] & ~orientMap[orientMap.length - 2]);
         var6[0] |= orientMap[var4];
         var6[1] |= orientMap[var4 + 1];
         var7 = (short)((var6[0] & 255 & orientMap[orientMap.length - 3]) >> 4);
         var8 = (short)ByteUtil.extractInt(ByteUtil.intRead3ByteArray(var6, 0), 20, 24, new Object());
         System.err.println(var2 + " -> " + var7 + "; ; " + var8);

         assert var8 == var2;

         assert var8 == var7;
      }

   }

   public static void setActive(int var0, boolean var1, byte[] var2) {
      if (!var1) {
         var2[var0] = (byte)(var2[var0] | 8);
      } else {
         var2[var0] &= -9;
      }
   }

   public static void setHitpoints(int var0, short var1, byte[] var2) {
      assert var1 >= 0 && var1 < 512;

      int var3 = var1 * 3;
      var2[var0] = (byte)(var2[var0] & ~hpMap[hpMap.length - 3]);
      var2[var0 + 1] = (byte)(var2[var0 + 1] & ~hpMap[hpMap.length - 2]);
      var2[var0] |= hpMap[var3];
      var2[var0 + 1] |= hpMap[var3 + 1];
   }

   public static void setOrientation(int var0, byte var1, byte[] var2) {
      assert var1 >= 0 && var1 < 16 : "NOT A SIDE INDEX";

      var2[var0] = (byte)(var2[var0] & ~orientMap[orientMap.length - 3]);
      var2[var0] |= orientMap[var1 * 3];
   }

   public static void setType(int var0, short var1, byte[] var2) {
      if (var1 < 2048) {
         int var3 = var1 * 3;
         var2[var0 + 2] = typeMap[var3 + 2];
         var2[var0 + 1] = (byte)(var2[var0 + 1] - (var2[var0 + 1] & 7));
         var2[var0 + 1] |= typeMap[var3 + 1];
      } else {
         System.err.println("ERROR: Type is invalied. must be < 4096 but was: " + var1);
      }
   }

   public static boolean valid(byte var0, byte var1, byte var2) {
      return ((var0 | var1 | var2) & -16) == 0;
   }

   public static boolean allNeighborsInside(byte var0, byte var1, byte var2) {
      return var0 < 15 && var1 < 15 && var2 < 15 && var0 > 0 && var1 > 0 && var2 > 0;
   }

   public static boolean allNeighborsInside(int var0, int var1, int var2) {
      return var0 < 15 && var1 < 15 && var2 < 15 && var0 > 0 && var1 > 0 && var2 > 0;
   }

   public static int getInfoIndex(byte var0, byte var1, byte var2) {
      return 3 * ((var2 << 8) + (var1 << 4) + var0);
   }

   public static int getInfoIndex(int var0, int var1, int var2) {
      return 3 * ((var2 << 8) + (var1 << 4) + var0);
   }

   public static int getInfoIndex(Vector3b var0) {
      return getInfoIndex(var0.x, var0.y, var0.z);
   }

   public static int getInfoIndex(Vector3i var0) {
      return getInfoIndex(var0.x, var0.y, var0.z);
   }

   public static int[] createLookup() {
      int[] var0 = new int[256];

      for(int var1 = -128; var1 <= 127; ++var1) {
         int var2 = var1 & 255;
         var0[var2] = (var2 & 7) << 8;
      }

      return var0;
   }

   public int applySegmentData(Vector3b var1, byte[] var2, int var3, boolean var4, long var5, boolean var7, long var8) {
      return this.applySegmentData(var1.x, var1.y, var1.z, var2, var3, var4, var5, var7, var8);
   }

   public int applySegmentData(byte var1, byte var2, byte var3, byte[] var4, int var5, boolean var6, long var7, boolean var9, long var10) {
      if (var6) {
         this.rwl.writeLock().lock();
      }

      try {
         int var12 = getInfoIndex(var1, var2, var3);
         int var13 = 0;
         boolean var14 = this.isActive(var12);
         short var15 = this.getType(var12);
         byte var16 = this.getOrientation(var12);
         int var17 = var12 + 3;

         for(int var18 = var12; var18 < var17; ++var18) {
            this.data[var18] = var4[var5 + var13++];
         }

         short var23 = this.getType(var12);
         short var21 = this.getHitpointsByte(var12);
         if (var23 == 0 && var15 == 0) {
            return 3;
         }

         if (var23 == var15) {
            if (this.getHitpointsByte(var12) != var21) {
               return 2;
            }

            boolean var22 = this.isActive(var12);
            if (var14 == var22) {
               return 3;
            }

            if (!ElementKeyMap.isDoor(var23)) {
               return 4;
            }

            if (var22 && !var14) {
               assert this.onServer || this.rwl.getWriteHoldCount() == 0;

               this.octree.insert(var1, var2, var3, var12);
               return 4;
            }

            if (!var22 && var14) {
               assert this.onServer || this.rwl.getWriteHoldCount() == 0;

               this.octree.delete(var1, var2, var3, var12, var23);
            }

            return 4;
         }

         if (var15 == 0 && var23 != 0) {
            this.onAddingElement(var12, var1, var2, var3, var23, var9, var6, var7, var10);
            return 0;
         }

         if (var15 == 0 || var23 != 0) {
            assert var23 != 0;

            return 2;
         }

         this.onRemovingElement(var12, var1, var2, var3, var15, var9, var16, var14, var6, var10);
      } finally {
         if (var6) {
            this.rwl.writeLock().unlock();
         }

      }

      return 1;
   }

   public int arraySize() {
      return this.data.length;
   }

   public void assignData(Segment var1) {
      this.setSegment(var1);
      this.resetBB();
   }

   public boolean checkEmpty() {
      for(int var1 = 0; var1 < this.data.length; var1 += 3) {
         if (this.getType(var1) != 0) {
            return false;
         }
      }

      return true;
   }

   public boolean contains(byte var1, byte var2, byte var3) {
      return valid(var1, var2, var3) ? this.containsUnsave(var1, var2, var3) : false;
   }

   public boolean contains(int var1) {
      return this.getType(var1) != 0;
   }

   public boolean contains(Vector3b var1) {
      return this.contains(var1.x, var1.y, var1.z);
   }

   public int hashCode() {
      return this.segment.hashCode();
   }

   public boolean equals(Object var1) {
      return this.segment.pos.equals(((SegmentDataRepairAll)var1).segment.pos);
   }

   public String toString() {
      return "(DATA: " + this.segment + ")";
   }

   public boolean containsFast(int var1) {
      return this.data[var1 + 2] != 0 || (this.data[var1 + 1] & 255 & 7) != 0;
   }

   public boolean containsFast(Vector3b var1) {
      return this.containsFast(getInfoIndex(var1));
   }

   public boolean containsFast(Vector3i var1) {
      return this.containsFast(getInfoIndex(var1));
   }

   public boolean containsUnblended(byte var1, byte var2, byte var3) {
      if (valid(var1, var2, var3)) {
         short var4;
         return (var4 = this.getType(var1, var2, var3)) != 0 && !ElementKeyMap.getInfo(var4).isBlended();
      } else {
         return false;
      }
   }

   public boolean containsUnblended(Vector3b var1) {
      return this.containsUnblended(var1.x, var1.y, var1.z);
   }

   public boolean containsUnsave(byte var1, byte var2, byte var3) {
      return this.containsFast(getInfoIndex(var1, var2, var3));
   }

   public boolean containsUnsave(int var1, int var2, int var3) {
      return this.containsFast(getInfoIndex(var1, var2, var3));
   }

   public boolean containsUnsave(int var1) {
      return this.getType(var1) != 0;
   }

   public void deserialize(DataInput var1, long var2) throws IOException {
      this.rwl.writeLock().lock();

      try {
         this.reset(var2);
         var1.readFully(this.data);
         this.setNeedsRevalidate(true);
      } finally {
         this.rwl.writeLock().unlock();
      }

   }

   public int[] getAsIntBuffer() {
      throw new IllegalArgumentException("Incompatible SegmentData Version");
   }

   public byte[] getAsOldByteBuffer() {
      return this.data;
   }

   public void migrateTo(int var1, SegmentDataInterface var2) {
      try {
         int var3;
         for(var1 = 0; var1 < 4096; ++var1) {
            var3 = var1 * 3;
            var2.setType(var3, this.getType(var3));
            var2.setHitpointsByte(var3, this.getHitpointsByte(var3));
            var2.setActive(var3, this.isActive(var3));
            var2.setOrientation(var3, this.getOrientation(var3));
         }

         for(var1 = 0; var1 < 4096; ++var1) {
            var3 = var1 * 3;
            short var4;
            if ((var4 = this.getType(var3)) != 0 && m.contains(var4)) {
               var2.setHitpointsByte(var3, 127);
            }
         }

         if (var2.getSegment() != null && ((RemoteSegment)var2.getSegment()).getSegmentController() != null) {
            ((RemoteSegment)var2.getSegment()).setLastChanged(System.currentTimeMillis());
         }

      } catch (SegmentDataWriteException var5) {
         var5.printStackTrace();
         throw new RuntimeException("this should be never be thrown as migration should always be toa normal segment data", var5);
      }
   }

   public void setType(int var1, short var2) {
      setType(var1, var2, this.data);
   }

   public void setHitpointsByte(int var1, int var2) {
      assert var2 >= 0 && var2 < 256 : var2;

      int var3 = var2 * 3;
      byte[] var10000 = this.data;
      var10000[var1] = (byte)(var10000[var1] & ~hpMap[hpMap.length - 3]);
      var10000 = this.data;
      var10000[var1 + 1] = (byte)(var10000[var1 + 1] & ~hpMap[hpMap.length - 2]);
      var10000 = this.data;
      var10000[var1] |= hpMap[var3];
      var10000 = this.data;
      var10000[var1 + 1] |= hpMap[var3 + 1];

      assert this.getHitpointsByte(var1) == var2;

   }

   public void setActive(int var1, boolean var2) {
      byte[] var10000;
      if (!var2) {
         var10000 = this.data;
         var10000[var1] = (byte)(var10000[var1] | 8);
      } else {
         var10000 = this.data;
         var10000[var1] &= -9;
      }

      assert var2 == this.isActive(var1) : var2 + "; " + this.isActive(var1);

   }

   public void setOrientation(int var1, byte var2) {
      assert var2 >= 0 && var2 < 16 : "NOT A SIDE INDEX";

      byte[] var10000 = this.data;
      var10000[var1] = (byte)(var10000[var1] & ~orientMap[orientMap.length - 3]);
      var10000 = this.data;
      var10000[var1] |= orientMap[var2 * 3];

      assert var2 == this.getOrientation(var1) : "failed orientation coding: " + var2 + " != result " + this.getOrientation(var1);

   }

   public short getType(int var1) {
      return (short)((this.data[var1 + 2] & 255) + lookUpSecType[this.data[var1 + 1] & 255]);
   }

   public short getHitpointsByte(int var1) {
      int var2 = this.data[var1 + 1] & 255;
      var1 = this.data[var1] & 255;
      return (short)(((var2 & hpMap[hpMap.length - 2]) >> 3) + ((var1 & hpMap[hpMap.length - 3]) << 5));
   }

   public boolean isActive(int var1) {
      return (this.data[var1] & 8) == 0;
   }

   public byte getOrientation(int var1) {
      return (byte)((this.data[var1] & 255 & orientMap[orientMap.length - 3]) >> 4);
   }

   public Segment getSegment() {
      return this.segment;
   }

   public SegmentController getSegmentController() {
      return this.segment.getSegmentController();
   }

   public void resetFast() {
      this.setSize(0);
      Arrays.fill(this.data, (byte)0);
      this.setSize(0);
      this.octree.reset();
      this.resetBB();
      this.setSize(0);
   }

   public void setSegment(Segment var1) {
      this.segment = var1;
   }

   public Vector3b getMax() {
      return this.max;
   }

   public Vector3b getMin() {
      return this.min;
   }

   public ArrayOctree getOctree() {
      return this.octree;
   }

   public byte[] getSegmentPieceData(int var1, byte[] var2) {
      int var3 = 0;

      for(int var4 = var1; var4 < var1 + 3; ++var4) {
         var2[var3++] = this.data[var4];
      }

      return var2;
   }

   public int getSize() {
      return this.size;
   }

   public void setSize(int var1) {
      assert var1 >= 0 && var1 <= 4096 : "Exception WARNING: SEGMENT SIZE WRONG " + var1 + " " + (this.segment != null ? this.segment.getSegmentController().getState() + ": " + this.segment.getSegmentController() + " " + this.segment : "");

      this.size = var1;
      if (this.segment != null) {
         this.segment.setSize(this.size);
      }

   }

   public short getType(byte var1, byte var2, byte var3) {
      int var4 = getInfoIndex(var1, var2, var3);
      return this.getType(var4);
   }

   public short getType(Vector3b var1) {
      return this.getType(var1.x, var1.y, var1.z);
   }

   public boolean isRevalidating() {
      return this.revalidating;
   }

   public boolean needsRevalidate() {
      return this.needsRevalidate;
   }

   public boolean neighbors(byte var1, byte var2, byte var3) {
      if (!allNeighborsInside(var1, var2, var3)) {
         if (this.contains((byte)(var1 - 1), var2, var3)) {
            return true;
         }

         if (this.contains((byte)(var1 + 1), var2, var3)) {
            return true;
         }

         if (this.contains(var1, (byte)(var2 - 1), var3)) {
            return true;
         }

         if (this.contains(var1, (byte)(var2 + 1), var3)) {
            return true;
         }

         if (this.contains(var1, var2, (byte)(var3 - 1))) {
            return true;
         }

         if (this.contains(var1, var2, (byte)(var3 + 1))) {
            return true;
         }
      } else {
         if (this.containsUnsave((byte)(var1 - 1), var2, var3)) {
            return true;
         }

         if (this.containsUnsave((byte)(var1 + 1), var2, var3)) {
            return true;
         }

         if (this.containsUnsave(var1, (byte)(var2 - 1), var3)) {
            return true;
         }

         if (this.containsUnsave(var1, (byte)(var2 + 1), var3)) {
            return true;
         }

         if (this.containsUnsave(var1, var2, (byte)(var3 - 1))) {
            return true;
         }

         if (this.containsUnsave(var1, var2, (byte)(var3 + 1))) {
            return true;
         }
      }

      return false;
   }

   private void onAddingElement(int var1, byte var2, byte var3, byte var4, short var5, boolean var6, boolean var7, long var8, long var10) {
      if (var7) {
         this.rwl.writeLock().lock();
      }

      try {
         this.onAddingElementUnsynched(var1, var2, var3, var4, var5, var6, true, var8, var10);
      } finally {
         if (var7) {
            this.rwl.writeLock().unlock();
         }

      }

   }

   private void onAddingForcedElementUnsynched(int var1, byte var2, byte var3, byte var4, short var5, boolean var6, boolean var7) {
      this.setBlockAddedForced(true);
   }

   private void onAddingElementUnsynched(int var1, byte var2, byte var3, byte var4, short var5, boolean var6, boolean var7, long var8, long var10) {
      int var10000 = this.size;
      this.incSize();
      if (!ElementKeyMap.infoArray[var5].isDoor() || (this.data[var1] & 8) == 0) {
         this.getOctree().insert(var2, var3, var4, var1);
      }

      this.getSegmentController().onAddedElementSynched(var5, this.getOrientation(var1), var2, var3, var4, this.getSegment(), var7, var8, var10, false);
      if (!this.revalidating) {
         this.getSegment().dataChanged(true);
      }

      this.updateBB(var2, var3, var4, var6, true);
   }

   public void onRemovingElement(int var1, byte var2, byte var3, byte var4, short var5, boolean var6, byte var7, boolean var8, boolean var9, long var10) {
      if (var9) {
         this.rwl.writeLock().lock();
      }

      try {
         var1 = this.getSize();
         this.setSize(var1 - 1);
         this.getSegmentController().onRemovedElementSynched(var5, var1, var2, var3, var4, var7, this.getSegment(), this.preserveControl, var10);
         if (!this.revalidating) {
            this.getSegment().dataChanged(true);
         }

         this.updateBB(var2, var3, var4, var6, false);
      } finally {
         if (var9) {
            this.rwl.writeLock().unlock();
         }

      }

   }

   public void removeInfoElement(byte var1, byte var2, byte var3) {
      setType(getInfoIndex(var1, var2, var3), (short)0, this.data);
   }

   public void removeInfoElement(Vector3b var1) {
      this.removeInfoElement(var1.x, var1.y, var1.z);
   }

   public void reset(long var1) {
      this.rwl.writeLock().lock();

      try {
         this.preserveControl = true;
         if (this.getSegment() != null) {
            long var3 = (long)(this.segment.pos.x & '\uffff');
            long var5 = (long)(this.segment.pos.y & '\uffff') << 16;
            long var7 = (long)(this.segment.pos.z & '\uffff') << 32;
            long var9 = var3;
            long var11 = var5;
            long var13 = var7;
            byte var20 = 0;

            while(true) {
               if (var20 >= 16) {
                  this.getSegment().setSize(0);
                  break;
               }

               for(byte var8 = 0; var8 < 16; ++var8) {
                  for(byte var15 = 0; var15 < 16; ++var15) {
                     long var16 = var9 + var11 + var13;
                     this.setInfoElementUnsynched(var15, var8, var20, (short)0, false, var16, var1);
                     ++var9;
                  }

                  var9 = var3;
                  var11 += 65536L;
               }

               var9 = var3;
               var11 = var5;
               var13 += 4294967296L;
               ++var20;
            }
         }

         this.preserveControl = false;
         this.setSize(0);
         this.octree.reset();
         this.resetBB();
      } finally {
         this.rwl.writeLock().unlock();
      }

   }

   public void resetBB() {
      assert this.size == 0;

      this.getMax().set((byte)-128, (byte)-128, (byte)-128);
      this.getMin().set((byte)127, (byte)127, (byte)127);
   }

   public void restructBB(boolean var1) {
      byte var2 = this.getMin().x;
      byte var3 = this.getMin().y;
      byte var4 = this.getMin().z;
      byte var5 = this.getMax().x;
      byte var6 = this.getMax().y;
      byte var10000 = this.getMax().z;
      this.getMax().set((byte)-128, (byte)-128, (byte)-128);
      this.getMin().set((byte)127, (byte)127, (byte)127);
      int var7 = 0;

      for(byte var8 = 0; var8 < 16; ++var8) {
         for(byte var9 = 0; var9 < 16; ++var9) {
            for(byte var10 = 0; var10 < 16; ++var10) {
               if (this.containsFast(var7)) {
                  this.updateBB(var10, var9, var8, false, true);
               }

               var7 += 3;
            }
         }
      }

      if (var1 && var2 == this.getMin().x && var3 == this.getMin().y && var4 == this.getMin().z && var5 == this.getMax().x && var6 == this.getMax().y) {
         var10000 = this.getMax().z;
      }

   }

   public void apply(RemoteSegment var1, boolean var2, SendableSegmentController var3, long var4) {
      if (var2) {
         this.rwl.writeLock().lock();
      }

      try {
         long var6 = (long)(var1.pos.x & '\uffff');
         long var8 = (long)(var1.pos.y & '\uffff') << 16;
         long var10 = (long)(var1.pos.z & '\uffff') << 32;
         long var12 = var6;
         long var14 = var8;
         long var16 = var10;
         int var23 = 0;

         for(byte var24 = 0; var24 < 16; ++var24) {
            for(byte var5 = 0; var5 < 16; ++var5) {
               for(byte var25 = 0; var25 < 16; ++var25) {
                  short var11 = this.getType(var23);
                  short var18 = this.getHitpointsByte(var23);
                  long var19 = var12 + var14 + var16;
                  if (!var3.isOnServer() && (var25 % 2 == 0 && var5 % 2 == 0 && var24 % 2 == 0 || Math.random() > 0.9D) && var11 > 0 && this.getType(var23) == 0 && var18 > 0 && this.getHitpointsByte(var23) == 0) {
                     Vector3f var26;
                     (var26 = new Vector3f()).set((float)ElementCollection.getPosX(var19), (float)ElementCollection.getPosY(var19), (float)ElementCollection.getPosZ(var19));
                     var26.x -= 8.0F;
                     var26.y -= 8.0F;
                     var26.z -= 8.0F;
                     ((GameClientState)var3.getState()).getWorldDrawer().getShards().voronoiBBShatterDelayed((PhysicsExt)((GameClientState)var3.getState()).getPhysics(), var26, var11, var3, var3.getGravity().source);
                  }

                  var23 += 3;
                  ++var12;
               }

               var12 = var6;
               var14 += 65536L;
            }

            var12 = var6;
            var14 = var8;
            var16 += 4294967296L;
         }

         this.restructBB(true);
      } finally {
         if (var2) {
            this.rwl.writeLock().unlock();
         }

      }
   }

   private void unvalidate(byte var1, byte var2, byte var3, int var4, long var5) {
      short var7 = this.getType(var4);
      byte var8 = this.getOrientation(var4);
      if (var7 != 0) {
         this.onRemovingElement(var4, var1, var2, var3, var7, false, var8, this.isActive(var4), false, var5);
      }

   }

   public void unvalidateData(long var1) {
      this.rwl.writeLock().lock();

      try {
         this.revalidating = true;
         int var3 = 0;

         for(byte var4 = 0; var4 < 16; ++var4) {
            for(byte var5 = 0; var5 < 16; ++var5) {
               for(byte var6 = 0; var6 < 16; ++var6) {
                  this.unvalidate(var6, var5, var4, var3, var1);
                  var3 += 3;
               }
            }
         }

         this.getSegmentController().getSegmentBuffer().updateBB(this.getSegment());
         this.revalidating = false;
         this.getSegment().dataChanged(true);
      } finally {
         this.rwl.writeLock().unlock();
      }
   }

   public void revalidateData(long var1) {
      this.rwl.writeLock().lock();

      try {
         this.revalidating = true;

         assert this.getSize() == 0 : " size is " + this.getSize() + " in " + this.getSegment().pos + " -> " + this.getSegmentController();

         int var3 = 0;
         long var4 = (long)(this.segment.pos.x & '\uffff');
         long var6 = (long)(this.segment.pos.y & '\uffff') << 16;
         long var8 = (long)(this.segment.pos.z & '\uffff') << 32;
         long var10 = var4;
         long var12 = var6;
         long var14 = var8;
         long var16 = System.currentTimeMillis();

         for(byte var18 = 0; var18 < 16; ++var18) {
            for(byte var19 = 0; var19 < 16; ++var19) {
               for(byte var23 = 0; var23 < 16; ++var23) {
                  int var9;
                  if ((var9 = (this.data[var3 + 2] & 255) + lookUpSecType[this.data[var3 + 1] & 255]) != 0) {
                     if (var9 <= ElementKeyMap.highestType && ElementKeyMap.validArray[var9]) {
                        short var20 = this.getHitpointsByte(var3);
                        if (this.getHitpointsByte(var3) > 127) {
                           this.setHitpointsByte(var3, Math.min(127, var20));
                        }

                        ++this.size;
                        this.segment.setSize(this.size);
                        if (!ElementKeyMap.infoArray[var9].isDoor() || (this.data[var3] & 8) == 0) {
                           this.getOctree().insert(var23, var19, var18, var3);
                        }

                        this.updateBB(var23, var19, var18, false, true);
                        this.getSegmentController().onAddedElementSynched((short)var9, this.getOrientation(var3), var23, var19, var18, this.getSegment(), true, var10 + var12 + var14, var1, false);
                     } else {
                        setType(var3, (short)0, this.data);

                        assert this.getType(var23, var19, var18) == 0 : "FAILED: " + var9 + "; " + var23 + ", " + var19 + ", " + var18;
                     }
                  }

                  var3 += 3;
                  ++var10;
               }

               var10 = var4;
               var12 += 65536L;
            }

            var10 = var4;
            var12 = var6;
            var14 += 4294967296L;
         }

         assert this.isBBValid();

         long var24;
         if ((var24 = System.currentTimeMillis() - var16) > 50L && !this.getSegmentController().isOnServer()) {
            System.err.println("[CLIENT] " + this.getSegment() + " WARNING: Revalidating took " + var24 + "ms (without locks)");
         }

         if (this.getSegmentController() instanceof EditableSendableSegmentController) {
            ((EditableSendableSegmentController)this.getSegmentController()).doDimExtensionIfNecessary(this.segment, this.getMin().x, this.getMin().y, this.getMin().z);
            ((EditableSendableSegmentController)this.getSegmentController()).doDimExtensionIfNecessary(this.segment, (byte)(this.getMax().x - 1), (byte)(this.getMax().y - 1), (byte)(this.getMax().z - 1));
         }

         this.getSegmentController().getSegmentBuffer().updateBB(this.getSegment());
         this.setNeedsRevalidate(false);
         this.revalidating = false;
         this.getSegment().dataChanged(true);
      } finally {
         this.rwl.writeLock().unlock();
      }

   }

   public boolean revalidateSuccess() {
      for(byte var1 = 0; var1 < 16; ++var1) {
         for(byte var2 = 0; var2 < 16; ++var2) {
            for(byte var3 = 0; var3 < 16; ++var3) {
               short var4;
               if ((var4 = this.getType(var3, var2, var1)) != 0 && !ElementKeyMap.exists(var4)) {
                  System.err.println("FAILED: " + var4 + "; " + var3 + ", " + var2 + ", " + var1);
                  return false;
               }
            }
         }
      }

      return true;
   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.write(this.data);
   }

   public void setInfoElement(byte var1, byte var2, byte var3, short var4, byte var5, byte var6, boolean var7, long var8, long var10) {
      this.rwl.writeLock().lock();

      try {
         this.setInfoElementUnsynched(var1, var2, var3, var4, var5, var6, var7, var8, var10);
      } finally {
         this.rwl.writeLock().unlock();
      }

   }

   public void setInfoElement(byte var1, byte var2, byte var3, short var4, boolean var5, long var6, long var8) {
      this.setInfoElement(var1, var2, var3, var4, (byte)-1, (byte)-1, var5, var6, var8);
   }

   public void setInfoElement(Vector3b var1, short var2, boolean var3, long var4, long var6) {
      this.setInfoElement(var1.x, var1.y, var1.z, var2, var3, var4, var6);
   }

   public void setInfoElement(Vector3b var1, short var2, byte var3, byte var4, boolean var5, long var6, long var8) {
      this.setInfoElement(var1.x, var1.y, var1.z, var2, var3, var4, var5, var6, var8);
   }

   public void setInfoElementForcedAddUnsynched(byte var1, byte var2, byte var3, short var4, boolean var5) {
      this.setInfoElementForcedAddUnsynched(var1, var2, var3, var4, (byte)-1, ElementInformation.activateOnPlacement(var4), var5);
   }

   public void setInfoElementForcedAddUnsynched(byte var1, byte var2, byte var3, short var4, byte var5, byte var6, boolean var7) {
      if (var4 != 0) {
         int var8;
         setType(var8 = getInfoIndex(var1, var2, var3), var4, this.data);
         if (var5 >= 0) {
            this.setOrientation(var8, var5);
         }

         if (var6 >= 0) {
            this.setActive(var8, var6 != 0);
         }

         this.setBlockAddedForced(true);
         this.setHitpointsByte(var8, 127);
      }
   }

   public void setInfoElementUnsynched(byte var1, byte var2, byte var3, short var4, boolean var5, long var6, long var8) {
      this.setInfoElementUnsynched(var1, var2, var3, var4, (byte)-1, (byte)-1, var5, var6, var8);
   }

   public void setInfoElementUnsynched(byte var1, byte var2, byte var3, short var4, byte var5, byte var6, boolean var7, long var8, long var10) {
      int var12 = getInfoIndex(var1, var2, var3);
      short var13 = this.getType(var12);
      byte var14 = this.getOrientation(var12);
      setType(var12, var4, this.data);
      if (var5 >= 0) {
         this.setOrientation(var12, var5);
      }

      boolean var15 = this.isActive(var12);
      if (var6 >= 0) {
         this.setActive(var12, var6 != 0);
      }

      if (var4 == 0) {
         this.setActive(var12, false);
         this.setOrientation(var12, (byte)0);
         if (var13 != 0) {
            this.onRemovingElement(var12, var1, var2, var3, var13, var7, var14, var15, false, var10);
            return;
         }
      } else if (var13 == 0) {
         this.onAddingElementUnsynched(var12, var1, var2, var3, var4, var7, true, var8, var10);
         this.setHitpointsByte(var12, 127);
      }

   }

   public void setInfoElementUnsynched(Vector3b var1, short var2, boolean var3, long var4, long var6) {
      this.setInfoElementUnsynched(var1.x, var1.y, var1.z, var2, var3, var4, var6);
   }

   public void setNeedsRevalidate(boolean var1) {
      this.needsRevalidate = var1;
   }

   public void incSize() {
      ++this.size;
      if (this.segment != null) {
         this.segment.setSize(this.size);
      }

   }

   private void updateBB(byte var1, byte var2, byte var3, boolean var4, boolean var5) {
      if (var5) {
         if (var1 >= this.getMax().x) {
            this.getMax().x = (byte)(var1 + 1);
         }

         if (var2 >= this.getMax().y) {
            this.getMax().y = (byte)(var2 + 1);
         }

         if (var3 >= this.getMax().z) {
            this.getMax().z = (byte)(var3 + 1);
         }

         if (var1 < this.getMin().x) {
            this.getMin().x = var1;
         }

         if (var2 < this.getMin().y) {
            this.getMin().y = var2;
         }

         if (var3 < this.getMin().z) {
            this.getMin().z = var3;
         }

         if (var4) {
            this.getSegmentController().getSegmentBuffer().updateBB(this.getSegment());
            return;
         }
      } else if (this.size == 0) {
         if (var4) {
            this.restructBB(var4);
            return;
         }
      } else if ((var1 + 1 >= this.getMax().x || var2 + 1 >= this.getMax().y || var3 + 1 >= this.getMax().z || var1 <= this.getMin().x || var2 <= this.getMin().y || var3 <= this.getMin().z) && var4) {
         this.restructBB(var4);
      }

   }

   public void writeSingle(int var1, DataOutputStream var2) throws IOException {
      var2.write(this.data, var1, 3);
   }

   public void readSingle(int var1, DataInputStream var2) throws IOException {
      var2.read(this.data, var1, 3);
   }

   public boolean isBlockAddedForced() {
      return this.blockAddedForced;
   }

   public void setBlockAddedForced(boolean var1) {
      this.blockAddedForced = var1;
   }

   public boolean isBBValid() {
      return this.min.x <= this.max.x && this.min.y <= this.max.y && this.min.z <= this.max.z;
   }

   public Vector3i getSegmentPos() {
      return null;
   }

   public boolean isIntDataArray() {
      return false;
   }

   public int inflate(Inflater var1, byte[] var2) throws SegmentInflaterException, DataFormatException {
      int var3;
      if ((var3 = var1.inflate(this.data)) != this.data.length) {
         throw new SegmentInflaterException(var3, this.data.length);
      } else {
         return var3;
      }
   }

   public SegmentData doBitmapCompressionCheck(RemoteSegment var1) {
      return null;
   }

   public void setDataAt(int var1, int var2) throws SegmentDataWriteException {
      throw new SegmentDataWriteException((SegmentData)null);
   }

   static {
      m = new IntOpenHashSet(raisedHP);
      MAX_INDEX = 12288;
      int var0 = typeMap.length / 3;

      short var1;
      int var2;
      for(var1 = 0; var1 < var0; ++var1) {
         var2 = var1 * 3;
         ByteUtil.intWrite3ByteArray(ByteUtil.putRangedBitsOntoInt(ByteUtil.intRead3ByteArray(typeMap, var2), var1, 0, 11, (Object)null), typeMap, var2, (Object)null);
      }

      for(var1 = 0; var1 < 256; ++var1) {
         var2 = var1 * 3;
         ByteUtil.intWrite3ByteArray(ByteUtil.putRangedBitsOntoInt(ByteUtil.intRead3ByteArray(hpMap, var2), var1, 11, 19, (Object)null), hpMap, var2, (Object)null);
      }

      for(var1 = 0; var1 < 16; ++var1) {
         var2 = var1 * 3;
         ByteUtil.intWrite3ByteArray(ByteUtil.putRangedBitsOntoInt(ByteUtil.intRead3ByteArray(orientMap, var2), var1, 20, 24, (Object)null), orientMap, var2, (Object)null);
      }

   }
}
