package org.schema.game.common.data.world.space;

import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.data.physics.CollisionType;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.NetworkSun;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkEntity;

public class Sun extends FixedSpaceEntity {
   private NetworkSun networkSun;

   public Sun(StateInterface var1) {
      super(var1);
   }

   public SimpleTransformableSendableObject.EntityType getType() {
      return SimpleTransformableSendableObject.EntityType.SUN;
   }

   public NetworkEntity getNetworkObject() {
      return this.networkSun;
   }

   public String getRealName() {
      return "name";
   }

   public void newNetworkObject() {
      this.networkSun = new NetworkSun(this.getState());
   }

   public void sendHitConfirm(byte var1) {
   }

   public CollisionType getCollisionType() {
      return CollisionType.SIMPLE;
   }

   public boolean isSegmentController() {
      return false;
   }

   public String getName() {
      return "Sun";
   }

   public AbstractOwnerState getOwnerState() {
      return null;
   }

   public void sendClientMessage(String var1, int var2) {
   }

   public void sendServerMessage(Object[] var1, int var2) {
   }

   public InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3) {
      return null;
   }

   public MetaWeaponEffectInterface getMetaWeaponEffect(long var1, DamageDealerType var3) {
      return null;
   }
}
