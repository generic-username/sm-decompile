package org.schema.game.common.data.world.space.textgen;

import java.awt.Color;
import org.schema.game.common.data.world.planet.PlanetInformations;
import org.schema.game.common.data.world.planet.texgen.palette.GaussianTerrainRange;

public class StandardSpacePalette extends SpacePalette {
   public StandardSpacePalette(PlanetInformations var1) {
      super(var1);
   }

   public void initPalette() {
      GaussianTerrainRange var1 = new GaussianTerrainRange(this.getInformations().getWaterLevel(), -1, (float)this.getInformations().getWaterLevel(), 20.0F, -1, -1, 2.0F, 0.3F, -258, 100, 50.0F, 5.5F, new Color(12237498), Color.black);
      GaussianTerrainRange var2 = new GaussianTerrainRange((int)((float)this.getInformations().getWaterLevel() * 1.1F), -1, (float)this.getInformations().getWaterLevel() * 1.1F, 20.0F, -1, -1, 5.0F, 6.0F, -258, 100, 5.0F, 5.0F, new Color(13290186), Color.black);
      GaussianTerrainRange var3 = new GaussianTerrainRange((int)((float)this.getInformations().getWaterLevel() * 1.2F), -1, (float)this.getInformations().getWaterLevel() * 2.0F, 3.0F, -1, -1, 10.0F, 1.5F, -258, 100, 5.0F, 3.0F, new Color(16777215), Color.black);
      GaussianTerrainRange var4 = new GaussianTerrainRange((int)((float)this.getInformations().getWaterLevel() * 1.2F), -1, (float)this.getInformations().getWaterLevel() * 1.2F, 3.0F, -1, -1, 0.0F, 2.0F, -258, 100, 10.0F, 3.0F, new Color(14342874), Color.black);
      GaussianTerrainRange var5 = new GaussianTerrainRange(-1, (int)((float)this.getInformations().getWaterLevel() * 1.7F), (float)this.getInformations().getWaterLevel(), 30.0F, -1, -1, 0.0F, 1.0F, -258, 100, 50.0F, 5.0F, new Color(5221032), Color.white);
      GaussianTerrainRange var6 = new GaussianTerrainRange(-1, (int)((float)this.getInformations().getWaterLevel() * 1.5F), (float)this.getInformations().getWaterLevel() * 0.5F, 50.0F, -1, -1, 0.0F, 5.0F, -258, 100, 50.0F, 5.0F, Color.black, Color.white);
      GaussianTerrainRange var7 = new GaussianTerrainRange(-1, (int)((float)this.getInformations().getWaterLevel() * 1.7F), 0.0F, 50.0F, -1, -1, 0.0F, 5.0F, -258, 100, 50.0F, 5.0F, Color.black, Color.white);
      this.attachTerrainRange(var1);
      this.attachTerrainRange(var2);
      this.attachTerrainRange(var6);
      this.attachTerrainRange(var5);
      this.attachTerrainRange(var7);
      this.attachTerrainRange(var4);
      this.attachTerrainRange(var3);
   }
}
