package org.schema.game.common.data.world.space.textgen;

import java.awt.Color;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.schema.game.common.data.world.planet.PlanetInformations;
import org.schema.game.common.data.world.planet.texgen.palette.ColorMixer;
import org.schema.game.common.data.world.planet.texgen.palette.ColorRange;
import org.schema.game.common.data.world.planet.texgen.palette.Palette;
import org.schema.game.common.data.world.planet.texgen.palette.TerrainPalette;

public abstract class SpacePalette implements Cloneable, Palette {
   private List colorRanges = new LinkedList();
   private ColorMixer mixer = new ColorMixer();
   private PlanetInformations infos;

   public SpacePalette(PlanetInformations var1) {
      this.infos = var1;
   }

   public void attachTerrainRange(ColorRange var1) {
      this.colorRanges.add(var1);
   }

   public PlanetInformations getInformations() {
      return this.infos;
   }

   public TerrainPalette.Colors getPointColor(ColorMixer var1, int var2, int var3, int var4, int var5, float var6) {
      Iterator var7 = this.colorRanges.iterator();

      while(var7.hasNext()) {
         ColorRange var8 = (ColorRange)var7.next();
         var1.attachColor(var8.getTerrainColor(), var8.getFactor(var2, var3, var4, var5, var6));
      }

      Color var10 = var1.getMixedColor();
      var1.clear();
      Iterator var11 = this.colorRanges.iterator();

      while(var11.hasNext()) {
         ColorRange var9 = (ColorRange)var11.next();
         var1.attachColor(var9.getTerrainSpecular(), var9.getFactor(var2, var3, var4, var5, var6));
      }

      Color var12 = var1.getMixedColor();
      var1.clear();
      return new TerrainPalette.Colors(var10, var12);
   }

   public TerrainPalette.Colors getPointColor(int var1, int var2, int var3, int var4, float var5) {
      return this.getPointColor(this.mixer, var1, var2, var3, var4, var5);
   }

   public abstract void initPalette();
}
