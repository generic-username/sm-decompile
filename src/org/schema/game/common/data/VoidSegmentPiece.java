package org.schema.game.common.data;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.List;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.CompareTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.element.ElementCollection;

public class VoidSegmentPiece extends SegmentPiece implements Comparable {
   public Vector3i voidPos = new Vector3i();
   public int senderId;
   public long controllerPos = Long.MIN_VALUE;
   public boolean onlyHitpointsChanged;
   public boolean onlyActiveChanged;

   public int compareTo(VoidSegmentPiece var1) {
      long var2 = this.getSegmentAbsoluteIndex();
      long var4 = var1.getSegmentAbsoluteIndex();
      return CompareTools.compare(var2, var4);
   }

   public void serialize(DataOutput var1) throws IOException {
      var1.writeInt(this.voidPos.x);
      var1.writeInt(this.voidPos.y);
      var1.writeInt(this.voidPos.z);
      serializeData(var1, this.getData());
   }

   public void setByValue(VoidSegmentPiece var1) {
      this.voidPos.set(var1.voidPos);
      this.controllerPos = var1.controllerPos;
      this.data = var1.data;
      this.x = var1.x;
      this.y = var1.y;
      this.z = var1.z;
      this.senderId = var1.senderId;
      this.onlyHitpointsChanged = var1.onlyHitpointsChanged;
      this.onlyActiveChanged = var1.onlyActiveChanged;
      this.setSegment(var1.getSegment());
   }

   public void reset() {
      super.reset();
      this.senderId = 0;
      this.onlyActiveChanged = false;
      this.onlyHitpointsChanged = false;
      this.controllerPos = Long.MIN_VALUE;
      this.voidPos.set(0, 0, 0);
   }

   public void deserialize(DataInput var1) throws IOException {
      this.voidPos.set(var1.readInt(), var1.readInt(), var1.readInt());
      this.setDataByReference(deserializeData(var1));
   }

   public int hashCode() {
      return this.voidPos.hashCode();
   }

   public long getAbsoluteIndex() {
      return ElementCollection.getIndex(this.voidPos);
   }

   public boolean equals(Object var1) {
      return ((SegmentPiece)var1).equalsPos(this.voidPos);
   }

   public boolean equalsPos(Vector3i var1) {
      return this.voidPos.equals(var1);
   }

   public long getSegmentAbsoluteIndex() {
      return ElementCollection.getIndex(ByteUtil.divUSeg(this.voidPos.x), ByteUtil.divUSeg(this.voidPos.y), ByteUtil.divUSeg(this.voidPos.z));
   }

   public static class VoidSegmentPiecePool {
      private final List pool = new ObjectArrayList();

      public void free(VoidSegmentPiece var1) {
         var1.reset();
         this.pool.add(var1);
      }

      public VoidSegmentPiece get() {
         return this.pool.isEmpty() ? new VoidSegmentPiece() : (VoidSegmentPiece)this.pool.remove(this.pool.size() - 1);
      }
   }
}
