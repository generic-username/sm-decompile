package org.schema.game.common.data;

import com.bulletphysics.linearmath.Transform;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Locale;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.effects.TimedIndication;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.common.data.physics.RigidBodySegmentController;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.world.GameTransformable;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.SectorInformation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.TransformaleObjectTmpVars;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.graphicsengine.forms.DebugBox;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.input.Keyboard;
import org.schema.schine.network.objects.Sendable;

public class DebugServerPhysicalObject extends DebugServerObject {
   private static final TransformaleObjectTmpVars v = new TransformaleObjectTmpVars();
   public final BoundingBox bb = new BoundingBox();
   public String name;
   public Transform serverTransform = new Transform();
   public SimpleTransformableSendableObject objOnServer;
   int sector = -1;

   public void setObject(GameTransformable var1) {
      this.serverTransform.set(var1.getWorldTransform());
      if (var1 instanceof AbstractCharacter) {
         this.bb.min.set(-1.0F, -1.0F, -1.0F);
         this.bb.max.set(1.0F, 1.0F, 1.0F);
      } else {
         var1.getGravityAABB(this.serverTransform, this.bb.min, this.bb.max);
      }

      this.name = "[ByObj]" + var1.toString();
      this.sector = var1.getSectorId();
   }

   public void setObject(RigidBodySegmentController var1, int var2) {
      var1.getWorldTransform(this.serverTransform);
      var1.getCollisionShape().getAabb(TransformTools.ident, this.bb.min, this.bb.max);
      this.name = "[ByPhy]" + var1.toString();
      this.sector = var2;
   }

   public byte getType() {
      return 0;
   }

   public void draw(GameClientState var1) {
      Sendable var2;
      if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn() && EngineSettings.N_TRANSMIT_RAW_DEBUG_POSITIONS.isOn() && (var2 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(this.sector)) != null && var2 instanceof RemoteSector) {
         RemoteSector var7 = (RemoteSector)var2;
         boolean var3 = var1.getCurrentRemoteSector().getType() == SectorInformation.SectorType.PLANET && var7.getType() != SectorInformation.SectorType.PLANET;
         boolean var4 = var1.getCurrentRemoteSector().getType() != SectorInformation.SectorType.PLANET && var7.getType() == SectorInformation.SectorType.PLANET;
         Transform var5 = new Transform();
         SimpleTransformableSendableObject.calcWorldTransformRelative(var1, var1.getCurrentRemoteSector().clientPos(), var7.clientPos(), var3, var4, this.serverTransform, var5, v);
         DebugBox var6 = null;
         if (!this.name.contains("Asteroid") || Keyboard.isKeyDown(4)) {
            if (this.name.toLowerCase(Locale.ENGLISH).contains("virt")) {
               if (!Keyboard.isKeyDown(2)) {
                  var6 = new DebugBox(this.bb.min, this.bb.max, var5, 1.0F, 0.1F, 0.3F, 1.0F);
               }
            } else if (!Keyboard.isKeyDown(3)) {
               --this.bb.min.x;
               --this.bb.min.y;
               --this.bb.min.z;
               ++this.bb.max.x;
               ++this.bb.max.y;
               ++this.bb.max.z;
               var6 = new DebugBox(this.bb.min, this.bb.max, var5, 0.8F, 0.4F, 0.7F, 1.0F);
            }
         }

         if (var6 != null) {
            DebugDrawer.boxes.add(var6);
            if (this.name.length() == 0) {
               this.name = "zeroLength";
            }

            TimedIndication var8 = new TimedIndication(var5, var7.clientPos() + this.name, 0.2F, 10000.0F);
            HudIndicatorOverlay.toDrawTexts.add(var8);
         }
      }

   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.writeUTF(this.name);
      this.bb.serialize(var1);
      var1.writeInt(this.sector);
      TransformTools.serializeFully(var1, this.serverTransform);
   }

   public void deserialize(DataInputStream var1) throws IOException {
      this.name = var1.readUTF();
      this.bb.deserialize(var1);
      this.sector = var1.readInt();
      TransformTools.deserializeFully(var1, this.serverTransform);
   }
}
