package org.schema.game.common.data.physics;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.List;

public abstract class BoundingSphereCollisionBroadphase {
   public final BoundingSphereCollisionManagerLocal man;
   public final List out = new ObjectArrayList();

   public BoundingSphereCollisionBroadphase(BoundingSphereCollisionManagerLocal var1) {
      this.man = var1;
   }

   public List calculate() {
      this.setupPhase(this.man.bbs);
      return this.broadPhase(this.man, this.out);
   }

   public void free() {
      this.man.freeAll(this.out);
   }

   protected abstract void setupPhase(List var1);

   protected abstract List broadPhase(BoundingSphereCollisionManagerLocal var1, List var2);
}
