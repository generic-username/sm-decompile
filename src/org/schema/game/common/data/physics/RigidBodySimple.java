package org.schema.game.common.data.physics;

import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.MotionState;
import javax.vecmath.Vector3f;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;

public class RigidBodySimple extends RigidBodyExt implements GamePhysicsObject {
   SimpleTransformableSendableObject simpelTransformable;

   public RigidBodySimple(SimpleTransformableSendableObject var1, float var2, MotionState var3, CollisionShape var4) {
      super(var1.getCollisionType(), var2, var3, var4);
      this.simpelTransformable = var1;
      this.interpolationWorldTransform.setIdentity();
   }

   public RigidBodySimple(SimpleTransformableSendableObject var1, float var2, MotionState var3, CollisionShape var4, Vector3f var5) {
      super(var1.getCollisionType(), var2, var3, var4, var5);
      this.simpelTransformable = var1;
      this.interpolationWorldTransform.setIdentity();
   }

   public RigidBodySimple(SimpleTransformableSendableObject var1, RigidBodyConstructionInfo var2) {
      super(var1.getCollisionType(), var2);
      this.simpelTransformable = var1;
      this.interpolationWorldTransform.setIdentity();
   }

   public SimpleTransformableSendableObject getSimpleTransformableSendableObject() {
      return this.simpelTransformable;
   }
}
