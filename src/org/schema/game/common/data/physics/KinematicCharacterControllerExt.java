package org.schema.game.common.data.physics;

import com.bulletphysics.collision.broadphase.BroadphasePair;
import com.bulletphysics.collision.broadphase.BroadphaseProxy;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.CollisionWorld;
import com.bulletphysics.collision.dispatch.CollisionWorld.ClosestConvexResultCallback;
import com.bulletphysics.collision.dispatch.CollisionWorld.LocalConvexResult;
import com.bulletphysics.collision.narrowphase.ManifoldPoint;
import com.bulletphysics.collision.narrowphase.PersistentManifold;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.dynamics.character.KinematicCharacterController;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectCollection;
import java.util.Iterator;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.AbstractCharacterInterface;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.server.ServerStateInterface;

public class KinematicCharacterControllerExt extends KinematicCharacterController {
   public static Vector3f[] upAxisDirectionDefault = new Vector3f[]{new Vector3f(1.0F, 0.0F, 0.0F), new Vector3f(0.0F, 1.0F, 0.0F), new Vector3f(0.0F, 0.0F, 1.0F)};
   private final float extendedCharacterHeight;
   private final Vector3f startedPosition = new Vector3f();
   private final Vector3f endedPosition = new Vector3f();
   private final Vector3f lastActualWalkingDist = new Vector3f();
   private final Vector3f minAabb = new Vector3f();
   private final Vector3f maxAabb = new Vector3f();
   public Vector3f[] upAxisDirection = new Vector3f[]{new Vector3f(1.0F, 0.0F, 0.0F), new Vector3f(0.0F, 1.0F, 0.0F), new Vector3f(0.0F, 0.0F, 1.0F)};
   Vector3f move = new Vector3f();
   Transform xform = new Transform();
   Transform start = new Transform();
   Transform end = new Transform();
   Vector3f distance2Vec = new Vector3f();
   Vector3f hitDistanceVec = new Vector3f();
   Vector3f hitDistanceDownPosVec = new Vector3f();
   Vector3f currentDir = new Vector3f();
   Transform tmp = new Transform();
   Vector3f before = new Vector3f();
   Vector3f after;
   int penetrationCounter;
   ObjectArrayList manifoldArray = new ObjectArrayList();
   Transform ttmp = new Transform();
   Vector3f normal = new Vector3f();
   private AbstractCharacterInterface obj;
   private CapsuleShapeExt capsule;
   private CapsuleShapeExt strafeCapsule;
   private long jumpingStarted = -1L;
   private Vector3f velocity = new Vector3f();
   private boolean lastStepDown;
   public boolean wasAttached = false;
   private Vector3f upTmp = new Vector3f();

   public KinematicCharacterControllerExt(AbstractCharacterInterface var1, PairCachingGhostObjectExt var2, ConvexShape var3, float var4) {
      super(var2, var3, var4);
      this.extendedCharacterHeight = var1.getCharacterHeight() + 0.27F;
      this.obj = var1;
      this.currentPosition.set(var2.getWorldTransform(new Transform()).origin);
   }

   public static void getAddRot(int var0, Matrix3f var1) {
      switch(var0) {
      case 0:
         var1.rotX(-1.5707964F);
         return;
      case 1:
         var1.rotX(1.5707964F);
         return;
      case 2:
         return;
      case 3:
         var1.rotX(3.1415927F);
         return;
      case 4:
         var1.rotZ(1.5707964F);
         return;
      case 5:
         var1.rotZ(-1.5707964F);
      default:
      }
   }

   private void adaptLocalAttached() {
      PairCachingGhostObjectAlignable var1;
      if ((var1 = (PairCachingGhostObjectAlignable)this.ghostObject).getAttached() != null) {
         Transform var2;
         (var2 = new Transform(var1.getAttached().getWorldTransform())).basis.mul(this.getAddRoation());
         Matrix4f var4 = var2.getMatrix(new Matrix4f());
         new Matrix4f(var4);
         (var4 = new Matrix4f(var4)).invert();
         Matrix4f var3 = this.xform.getMatrix(new Matrix4f());
         var4.mul(var3);
         if (this.obj.isSitting() && this.obj.getGravity().source != null) {
            if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
               DebugDrawer.debugDraw(this.obj.getSittingPos().x, this.obj.getSittingPos().y, this.obj.getSittingPos().z, 16, var1.getAttached());
               DebugDrawer.debugDraw(this.obj.getSittingPosTo().x, this.obj.getSittingPosTo().y, this.obj.getSittingPosTo().z, 16, var1.getAttached());
               DebugDrawer.debugDraw(this.obj.getSittingPosLegs().x, this.obj.getSittingPosLegs().y, this.obj.getSittingPosLegs().z, 16, var1.getAttached());
            }

            Vector3f var5 = new Vector3f((float)(this.obj.getSittingPos().x - 16), (float)(this.obj.getSittingPos().y - 16), (float)(this.obj.getSittingPos().z - 16));
            switch(var1.attachedOrientation) {
            case 0:
               var5.z -= 0.36F;
               break;
            case 1:
               var5.z += 0.36F;
               break;
            case 2:
               var5.y += 0.36F;
               break;
            case 3:
               var5.y -= 0.36F;
               break;
            case 4:
               var5.x -= 0.36F;
               break;
            case 5:
               var5.x += 0.36F;
               break;
            default:
               var5.y += 0.36F;
            }

            Matrix3f var6;
            (var6 = new Matrix3f(this.getAddRoation())).invert();
            var6.transform(var5);
            var1.localWorldTransform.origin.set(var5);
         } else {
            var1.localWorldTransform.set(var4);
         }
      } else {
         if (this.obj.isSitting()) {
            System.err.println("[KINEMATIC][ERROR] " + this.obj.getState() + "; " + this.obj + " Sitting gravity source");
         }

      }
   }

   public void loadAttached(long var1) {
      PairCachingGhostObjectAlignable var3;
      if ((var3 = (PairCachingGhostObjectAlignable)this.ghostObject).getAttached() != null && var3.getAttached().isMarkedForDeleteVolatile()) {
         System.err.println("[KINEMATIC] LOST " + this.obj + " ATTACHED TO :" + var3.getAttached() + " BECAUSE IS WAS DELETED");
         var3.setAttached((SegmentController)null);
      }

      boolean var4;
      if (var4 = var3.getAttached() != null) {
         var3.getAttached().getPhysicsDataContainer().updatePhysical(var1);
         Transform var5 = new Transform(var3.getAttached().getWorldTransform());
         if (var3.getAttached().getPhysicsDataContainer().getObject() != null) {
            var3.getAttached().getPhysicsDataContainer().getObject().activate(true);
         }

         var5.basis.mul(this.getAddRoation());
         GlUtil.getRightVector(this.upAxisDirection[0], var5);
         GlUtil.getUpVector(this.upAxisDirection[1], var5);
         GlUtil.getForwardVector(this.upAxisDirection[2], var5);
         Matrix4f var6 = var5.getMatrix(new Matrix4f());
         Matrix4f var2 = var3.localWorldTransform.getMatrix(new Matrix4f());
         (var6 = new Matrix4f(var6)).mul(var2);
         var3.setWorldTransform(new Transform(var6));
      }

      this.wasAttached = var4;
   }

   public void breakJump(Timer var1) {
      if (this.verticalVelocity > 0.0F && this.jumpingStarted != -1L) {
         if (System.currentTimeMillis() - this.jumpingStarted > 80L) {
            this.verticalVelocity = Math.max(0.0F, this.verticalVelocity - var1.getDelta() * 50.0F);
         }

      } else {
         this.jumpingStarted = -1L;
      }
   }

   public boolean isJumping() {
      return this.jumpingStarted != -1L;
   }

   public Matrix3f getAddRoation() {
      PairCachingGhostObjectAlignable var1 = (PairCachingGhostObjectAlignable)this.ghostObject;
      Matrix3f var2;
      (var2 = new Matrix3f()).setIdentity();
      if (var1.getAttached() != null) {
         getAddRot(var1.attachedOrientation, var2);
      }

      return var2;
   }

   public Vector3f getLinearVelocity(Vector3f var1) {
      var1.set(this.velocity);
      var1.scale(1000.0F);
      return var1;
   }

   public void resetVerticalVelocity() {
      this.verticalVelocity = 0.0F;
   }

   public float getVerticalVelocity() {
      return this.verticalVelocity;
   }

   public void setLocalJumpSpeed(float var1) {
   }

   public void setVelocityForTimeIntervalStacked(Vector3f var1, float var2) {
      this.useWalkDirection = false;
      this.walkDirection.add(var1);
      this.normal.set(this.walkDirection);
      this.normal.normalize();
      this.normalizedDirection.set(this.normal);
      this.velocityTimeInterval += var2;
   }

   public void setWalkDirectionStacked(Vector3f var1) {
      this.useWalkDirection = true;
      this.walkDirection.add(var1);
      this.normal.set(this.walkDirection);
      this.normal.normalize();
      this.normalizedDirection.set(this.normal);
   }

   private void createCapsule() {
      float var1 = this.obj.getCharacterWidth();
      float var2 = this.extendedCharacterHeight;
      this.capsule = new CapsuleShapeExt((SimpleTransformableSendableObject)this.obj, var1, var2);
      this.capsule.setMargin(0.1F);
   }

   public void stopJump() {
      this.verticalVelocity = 0.0F;
   }

   public void updateAction(CollisionWorld var1, float var2) {
      try {
         if (this.obj.isHidden()) {
            if (this.obj.isOnServer()) {
               System.err.println("[SERVER] ######## EXCEPTION: Object " + this.obj + " was hidden. Throwing NullPointer");
               throw new NullPointerException("Hidden object " + this.obj);
            } else {
               System.err.println("[CLIENT] Kinematic ######## WARNING: Object was hidden. Maybe it just died, and is not yet removed");
               throw new NullPointerException("Hidden object " + this.obj);
            }
         } else {
            this.preStep(var1);
            this.playerStep(var1, var2);
            this.obj.getPhysicsDataContainer().updatePhysical(this.obj.getState().getUpdateTime());
            if (!this.obj.isOnServer() && ((GameClientState)this.obj.getState()).getCharacter() == this.obj) {
               this.obj.setActionUpdate(true);
            }

         }
      } catch (NullPointerException var3) {
         System.err.println("[KINEMATICCONTROLLER][UPDATEACTION] " + this.obj.getState() + " NULLPOINTER EXCEPTION: CAUSED BY " + this.obj + ": In sector " + this.obj.getSectorId() + "; hidden: " + this.obj.isHidden());
         var3.printStackTrace();
      }
   }

   public void preStep(CollisionWorld var1) {
      this.loadAttached(this.obj.getState().getUpdateTime());
      int var2 = 0;
      Vector3f var3 = this.ghostObject.getWorldTransform(this.ttmp).origin;
      this.currentPosition.set(var3);
      this.touchingContact = false;

      while(this.recoverFromPenetration(var1)) {
         ++var2;
         this.touchingContact = true;
         if (var2 > 4) {
            break;
         }
      }

      this.targetPosition.set(this.currentPosition);
   }

   public void playerStep(CollisionWorld var1, float var2) {
      if (this.useWalkDirection || this.velocityTimeInterval > 0.0F) {
         PairCachingGhostObjectAlignable var3 = (PairCachingGhostObjectAlignable)this.ghostObject;
         this.wasOnGround = this.onGround();
         this.verticalVelocity -= this.gravity * var2;
         if (this.gravity == 0.0F && this.verticalVelocity > 0.0F) {
            this.verticalVelocity = 0.0F;
         }

         if ((double)this.verticalVelocity > 0.0D && this.verticalVelocity > this.jumpSpeed) {
            this.verticalVelocity = this.jumpSpeed;
         }

         if ((double)this.verticalVelocity < 0.0D && Math.abs(this.verticalVelocity) > Math.abs(this.fallSpeed)) {
            this.verticalVelocity = -Math.abs(this.fallSpeed);
         }

         this.verticalOffset = this.verticalVelocity * var2;
         var3.getWorldTransform(this.xform);
         this.startedPosition.set(this.xform.origin);
         this.velocity.set(this.xform.origin);
         this.stepUp(var1);
         this.lastStepDown = false;
         if (this.useWalkDirection) {
            this.stepForwardAndStrafe(var1, this.walkDirection);
         } else {
            float var4 = var2 < this.velocityTimeInterval ? var2 : this.velocityTimeInterval;
            this.velocityTimeInterval -= var2;
            this.move.scale(var4, this.walkDirection);
            this.stepForwardAndStrafe(var1, this.move);
         }

         if (this.gravity > 0.0F) {
            this.stepDown(var1, var2);
         }

         this.xform.origin.set(this.currentPosition);
         GlUtil.setRightVector(this.upAxisDirection[0], this.xform);
         GlUtil.setUpVector(this.upAxisDirection[1], this.xform);
         GlUtil.setForwardVector(this.upAxisDirection[2], this.xform);
         var3.setWorldTransform(this.xform);
         this.adaptLocalAttached();
         this.velocity.sub(this.xform.origin);
         this.velocity.negate();
         this.walkDirection.set(0.0F, 0.0F, 0.0F);
         this.velocityTimeInterval = 0.0F;
         this.endedPosition.set(this.xform.origin);
         this.getLastActualWalkingDist().sub(this.endedPosition, this.startedPosition);
      }
   }

   public void jump() {
      if (this.canJump()) {
         this.jumpingStarted = System.currentTimeMillis();
         this.verticalVelocity = this.jumpSpeed;
      }
   }

   protected boolean recoverFromPenetration(CollisionWorld var1) {
      if (!this.obj.isSitting() && !this.obj.isHidden()) {
         boolean var2 = false;
         this.convexShape.getAabb(this.ghostObject.getWorldTransform(new Transform()), this.minAabb, this.maxAabb);
         if (!var1.getCollisionObjectArray().contains(this.ghostObject)) {
            throw new NullPointerException(this.obj.getState() + " Object " + this.ghostObject + "; " + this.obj + " is not part of physics (" + this.obj.getSectorId() + ") Hidden: " + this.obj.isHidden());
         } else {
            var1.getBroadphase().setAabb(this.ghostObject.getBroadphaseHandle(), this.minAabb, this.maxAabb, var1.getDispatcher());
            this.ghostObject.activate(true);
            var1.getDispatcher().dispatchAllCollisionPairs(this.ghostObject.getOverlappingPairCache(), var1.getDispatchInfo(), var1.getDispatcher());
            this.currentPosition.set(this.ghostObject.getWorldTransform(this.ttmp).origin);
            float var10 = 0.0F;

            for(int var3 = 0; var3 < this.ghostObject.getOverlappingPairCache().getNumOverlappingPairs(); ++var3) {
               this.manifoldArray.clear();
               BroadphasePair var4;
               if ((var4 = (BroadphasePair)this.ghostObject.getOverlappingPairCache().getOverlappingPairArray().getQuick(var3)).algorithm != null) {
                  var4.algorithm.getAllContactManifolds(this.manifoldArray);
               }

               for(int var12 = 0; var12 < this.manifoldArray.size(); ++var12) {
                  PersistentManifold var5;
                  float var6 = (var5 = (PersistentManifold)this.manifoldArray.getQuick(var12)).getBody0() == this.ghostObject ? -1.0F : 1.0F;
                  CollisionObject var7;
                  if (((var7 = (CollisionObject)(var5.getBody0() == this.ghostObject ? var5.getBody1() : var5.getBody0())).getCollisionFlags() & 4) != 4) {
                     int var8;
                     if (var5.getNumContacts() > 0) {
                        var8 = this.obj.getId() == var5.getContactPoint(0).starMadeIdA ? var5.getContactPoint(0).starMadeIdB : var5.getContactPoint(0).starMadeIdA;
                        if (!this.obj.isOnServer() && var7 instanceof RigidBodySegmentController && this.obj.getGravity().isGravityOrAlignedOn() && this.obj.getGravity().source.getId() != var8) {
                           System.err.println("[CLIENT][CHARACTER] We hit something: " + ((RigidBodySegmentController)var7).getSegmentController() + "; contacts " + var5.getNumContacts() + " in alignment or gravity that is not the source. Ending alignment/gravity for " + this.obj);
                           this.obj.getGravity().differentObjectTouched = true;
                           ((GameClientState)this.obj.getState()).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PHYSICS_KINEMATICCHARACTERCONTROLLEREXT_1, KeyboardMappings.STUCK_PROTECT.getKeyChar()), 0.0F);
                        }
                     }

                     if (!this.obj.isVulnerable() && var7 instanceof RigidBodySegmentController && ((RigidBodySegmentController)var7).getSegmentController() instanceof Ship) {
                        if (((Ship)((RigidBodySegmentController)var7).getSegmentController()).isClientOwnObject()) {
                           ((GameClientState)this.obj.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PHYSICS_KINEMATICCHARACTERCONTROLLEREXT_0, 0.0F);
                        }
                     } else {
                        for(var8 = 0; var8 < var5.getNumContacts(); ++var8) {
                           float var9;
                           ManifoldPoint var13;
                           if ((var9 = (var13 = var5.getContactPoint(var8)).getDistance()) < 0.1F) {
                              if (var9 < var10) {
                                 var10 = var9;
                                 this.touchingNormal.set(var13.normalWorldOnB);
                                 this.touchingNormal.scale(var6);
                              }

                              this.currentPosition.scaleAdd(var6 * var9 * 0.2F, var13.normalWorldOnB, this.currentPosition);
                              var2 = true;
                           } else {
                              this.obj.getState();
                           }
                        }

                        var5.clearManifold();
                     }
                  }
               }
            }

            this.manifoldArray.clear();
            Transform var11;
            (var11 = this.ghostObject.getWorldTransform(this.ttmp)).origin.set(this.currentPosition);
            this.ghostObject.setWorldTransform(var11);
            return var2;
         }
      } else {
         return false;
      }
   }

   protected void stepUp(CollisionWorld var1) {
      if (!this.obj.isSitting()) {
         if (this.obj.isOnServer()) {
            SubsimplexCubesCovexCast.mode = "UP";
         }

         Transform var2 = new Transform();
         Transform var3 = new Transform();
         float var4 = this.gravity > 0.0F && this.wasOnGround ? this.stepHeight : 0.0F;
         if (this.gravity == 0.0F && this.lastStepDown) {
            var4 = this.stepHeight;
         }

         this.targetPosition.scaleAdd(var4 + ((double)this.verticalOffset > 0.0D ? this.verticalOffset : 0.0F), this.upAxisDirection[this.upAxis], this.currentPosition);
         (new Vector3f()).sub(this.targetPosition, this.currentPosition);
         if (this.capsule == null) {
            this.createCapsule();
         }

         var2.setIdentity();
         var3.setIdentity();

         assert this.ghostObject.getWorldTransform(new Transform()).getMatrix(new Matrix4f()).determinant() != 0.0F : this.ghostObject.getWorldTransform(new Transform()).getMatrix(new Matrix4f());

         var2.basis.set(this.ghostObject.getWorldTransform(new Transform()).basis);
         var3.basis.set(this.ghostObject.getWorldTransform(new Transform()).basis);
         var2.origin.set(this.currentPosition);
         var3.origin.set(this.targetPosition);
         Vector3f var5;
         (var5 = new Vector3f()).scale(-1.0F, this.upAxisDirection[this.upAxis]);
         KinematicCharacterControllerExt.KinematicClosestNotMeConvexResultCallback var9;
         (var9 = new KinematicCharacterControllerExt.KinematicClosestNotMeConvexResultCallback(this.ghostObject, var5, 0.1F)).collisionFilterGroup = this.ghostObject.getBroadphaseHandle().collisionFilterGroup;
         var9.collisionFilterMask = this.ghostObject.getBroadphaseHandle().collisionFilterMask;
         if (this.useGhostObjectSweepTest) {
            this.ghostObject.convexSweepTest(this.capsule, var2, var3, var9, var1.getDispatchInfo().allowedCcdPenetration);
         } else {
            var1.convexSweepTest(this.convexShape, var2, var3, var9);
         }

         boolean var6 = var9.hasHit();
         float var7 = var9.closestHitFraction;
         Segment var8 = var9.segment;
         Vector3b var10 = var9.cubePos;
         if (var6) {
            this.currentStepOffset = var4 * var7;
            this.currentPosition.interpolate(this.currentPosition, this.targetPosition, var7);
            this.verticalVelocity = -1.0E-8F;
            this.verticalOffset = 0.0F;
            this.jumpingStarted = -1L;
            if (var8 != null) {
               this.obj.handleCollision(new SegmentPiece(var8, var10), this.currentPosition);
               return;
            }
         } else {
            this.currentStepOffset = var4;
            this.currentPosition.set(this.targetPosition);
         }

      }
   }

   protected void stepForwardAndStrafe(CollisionWorld var1, Vector3f var2) {
      if (!this.obj.isSitting()) {
         if (this.obj.isOnServer()) {
            SubsimplexCubesCovexCast.mode = "FW";
         }

         this.targetPosition.add(this.currentPosition, var2);
         this.start.setIdentity();
         this.end.setIdentity();
         this.start.basis.set(this.ghostObject.getWorldTransform(new Transform()).basis);
         this.end.basis.set(this.ghostObject.getWorldTransform(new Transform()).basis);
         float var6 = 1.0F;
         this.distance2Vec.sub(this.targetPosition, this.currentPosition);
         if (!this.targetPosition.epsilonEquals(this.currentPosition, 1.1920929E-7F)) {
            float var10000 = this.distance2Vec.lengthSquared();
            float var3 = 0.0F;
            if (var10000 != 0.0F) {
               if (this.strafeCapsule == null) {
                  var3 = this.obj.getCharacterHeight();
                  this.strafeCapsule = new CapsuleShapeExt((SimpleTransformableSendableObject)this.obj, 0.3F, var3);
                  this.strafeCapsule.setMargin(0.1F);
               }

               int var4 = 10;
               this.before.set(this.currentPosition);
               this.lastStepDown = false;

               while(var6 > 0.01F && var4-- > 0) {
                  this.start.origin.set(this.currentPosition);
                  this.end.origin.set(this.targetPosition);
                  KinematicCharacterControllerExt.KinematicClosestNotMeConvexResultCallback var5;
                  (var5 = new KinematicCharacterControllerExt.KinematicClosestNotMeConvexResultCallback(this.ghostObject, this.upAxisDirection[this.upAxis], -1.0F)).collisionFilterGroup = this.ghostObject.getBroadphaseHandle().collisionFilterGroup;
                  var5.collisionFilterMask = this.ghostObject.getBroadphaseHandle().collisionFilterMask;
                  var3 = this.convexShape.getMargin();
                  this.convexShape.setMargin(var3 + this.addedMargin);
                  if (this.useGhostObjectSweepTest) {
                     this.ghostObject.convexSweepTest(this.strafeCapsule, this.start, this.end, var5, var1.getDispatchInfo().allowedCcdPenetration);
                  } else {
                     var1.convexSweepTest(this.convexShape, this.start, this.end, var5);
                  }

                  this.convexShape.setMargin(var3);
                  var6 -= var5.closestHitFraction;
                  if (var5.hasHit()) {
                     this.hitDistanceVec.sub(var5.hitPointWorld, this.currentPosition);
                     this.hitDistanceDownPosVec.scaleAdd(-this.extendedCharacterHeight / 2.0F, this.upAxisDirection[1], this.currentPosition);
                     this.hitDistanceDownPosVec.sub(var5.hitPointWorld);
                     if (this.hitDistanceDownPosVec.length() < 0.126F) {
                        this.lastStepDown = true;
                     }

                     this.hitDistanceVec.sub(var5.hitPointWorld, this.currentPosition);
                     this.hitDistanceVec.length();
                     this.updateTargetPositionBasedOnCollision(var5.hitNormalWorld);
                     this.currentDir.sub(this.targetPosition, this.currentPosition);
                     if (this.currentDir.lengthSquared() > 1.1920929E-7F) {
                        this.currentDir.normalize();
                        if (this.currentDir.dot(this.normalizedDirection) > 0.0F) {
                           continue;
                        }

                        if (var5.segment != null) {
                           this.obj.handleCollision(new SegmentPiece(var5.segment, var5.cubePos), this.currentPosition);
                        }
                        break;
                     }

                     if (var5.segment != null) {
                        this.obj.handleCollision(new SegmentPiece(var5.segment, var5.cubePos), this.currentPosition);
                     }
                     break;
                  } else {
                     this.currentPosition.set(this.targetPosition);
                  }
               }

               this.obj.getState();
               if (this.after == null) {
                  this.after = new Vector3f();
               }

               this.after.set(this.currentPosition);
            }
         }
      }
   }

   protected void stepDown(CollisionWorld var1, float var2) {
      if (!this.obj.isSitting()) {
         if (this.obj.isOnServer()) {
            SubsimplexCubesCovexCast.mode = "DWN";
         }

         Transform var3 = new Transform();
         Transform var4 = new Transform();
         float var5 = this.wasOnGround ? this.stepHeight : 0.0F;
         Vector3f var6;
         (var6 = new Vector3f()).scale(this.currentStepOffset + var5, this.upAxisDirection[this.upAxis]);
         var2 = (var5 == 0.0F && this.verticalVelocity < 0.0F ? -this.verticalVelocity : 0.0F) * var2;
         Vector3f var9;
         (var9 = new Vector3f()).scale(var2, this.upAxisDirection[this.upAxis]);
         this.targetPosition.sub(var6);
         this.targetPosition.sub(var9);
         var3.setIdentity();
         var4.setIdentity();
         var3.basis.set(this.ghostObject.getWorldTransform(new Transform()).basis);
         var4.basis.set(this.ghostObject.getWorldTransform(new Transform()).basis);
         var3.origin.set(this.currentPosition);
         var4.origin.set(this.targetPosition);
         if (!this.targetPosition.epsilonEquals(this.currentPosition, 1.1920929E-7F)) {
            var2 = this.ghostObject.getCollisionShape().getMargin();
            if (this.capsule == null) {
               this.createCapsule();
            }

            (var9 = new Vector3f()).sub(this.targetPosition, this.currentPosition);
            if (var9.length() != 0.0F) {
               new Vector3f(this.currentPosition);
               (var9 = new Vector3f(var9)).normalize();
               var9.scale(0.999F);
               var9.add(this.currentPosition);
               KinematicCharacterControllerExt.KinematicClosestNotMeConvexResultCallback var7;
               (var7 = new KinematicCharacterControllerExt.KinematicClosestNotMeConvexResultCallback(this.ghostObject, this.upAxisDirection[this.upAxis], this.maxSlopeCosine)).collisionFilterGroup = this.ghostObject.getBroadphaseHandle().collisionFilterGroup;
               var7.collisionFilterMask = this.ghostObject.getBroadphaseHandle().collisionFilterMask;
               if (this.useGhostObjectSweepTest) {
                  this.ghostObject.convexSweepTest(this.capsule, var3, var4, var7, var1.getDispatchInfo().allowedCcdPenetration);
               } else {
                  var1.convexSweepTest(this.capsule, var3, var4, var7);
               }

               boolean var10 = var7.hasHit();
               float var11 = var7.closestHitFraction;
               Segment var8 = var7.segment;
               Vector3b var12 = var7.cubePos;
               if (var10) {
                  this.currentPosition.interpolate(this.currentPosition, this.targetPosition, var11);
                  this.verticalVelocity = 0.0F;
                  this.verticalOffset = 0.0F;
                  if (var8 != null) {
                     this.obj.handleCollision(new SegmentPiece(var8, var12), this.currentPosition);
                  }
               } else {
                  if (this.targetPosition.epsilonEquals(this.currentPosition, 1.0E-7F)) {
                     this.verticalVelocity = 0.0F;
                     this.verticalOffset = 0.0F;
                  }

                  this.currentPosition.set(this.targetPosition);
               }

               this.ghostObject.getCollisionShape().setMargin(var2);
            }
         }
      }
   }

   private boolean checkHit(StateInterface var1, Vector3f var2, Vector3f var3, float var4) {
      boolean var5 = false;
      ObjectCollection var8;
      if (var1 instanceof ServerStateInterface) {
         var8 = var1.getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values();
      } else {
         var8 = ((GameClientState)var1).getCurrentSectorEntities().values();
      }

      Iterator var9 = var8.iterator();

      while(var9.hasNext()) {
         Sendable var6;
         SegmentController var10;
         if ((var6 = (Sendable)var9.next()) instanceof SegmentController && (var10 = (SegmentController)var6).getSectorId() == this.obj.getSectorId()) {
            Transform var7;
            (var7 = new Transform()).setIdentity();
            var7.origin.set(var2);
            var5 = var10.getCollisionChecker().checkSingleSegmentController(var10, var7, var4, true);
            var7.setIdentity();
            var7.origin.set(var2);
            this.upTmp.set(var3);
            this.upTmp.scale(0.25F);
            var7.origin.add(this.upTmp);
            var5 = var5 || var10.getCollisionChecker().checkSingleSegmentController(var10, var7, var4, true);
            var7.setIdentity();
            var7.origin.set(var2);
            this.upTmp.set(var3);
            this.upTmp.scale(-0.25F);
            var7.origin.add(this.upTmp);
            if (var5 = var5 || var10.getCollisionChecker().checkSingleSegmentController(var10, var7, var4, true)) {
               break;
            }
         }
      }

      return var5;
   }

   public void warpOutOfCollision(StateInterface var1, CollisionWorld var2, Transform var3, Vector3f var4) {
      System.err.println("[KINEMATIC] " + this.obj.getState() + " WARPING OUT OF COLLISION START for " + this.obj + ": FROM " + var3.origin + "; " + this.ghostObject.getWorldTransform(new Transform()).origin);
      this.warp(var3.origin);
      this.start.setIdentity();
      this.end.setIdentity();
      this.distance2Vec.sub(var3.origin, this.targetPosition);
      new Vector3f();
      boolean var5;
      if (!(var5 = this.checkHit(var1, var3.origin, var4, 0.1F))) {
         System.err.println("[KINEMATIC] WARP OUT DIDNT DETECT A HIT");
      } else {
         var2.getBroadphase().calculateOverlappingPairs(var2.getDispatcher());
         int var9 = 0;
         Vector3f var6;
         GlUtil.getForwardVector(var6 = new Vector3f(), var3);
         int var7 = 0;
         this.targetPosition.set(var3.origin);
         if (var5) {
            this.warp(this.targetPosition);
            if (!(var5 = this.checkHit(var1, this.targetPosition, var4, 0.1F))) {
               System.err.println("[WARPING OT OF COLLISION][CHECK_SINGLE] NOHIT -> can spawn here!! " + this.targetPosition + "; " + this.obj.getState());
            } else {
               System.err.println("[WARPING OT OF COLLISION][CHECK_SINGLE] HIT -> cannot spawn here!! " + this.targetPosition);
            }
         }

         for(; var5 && var7 < 10000; ++var7) {
            this.start.origin.set(this.targetPosition);
            switch(var9) {
            case 0:
               GlUtil.getUpVector(var6, var3);
               break;
            case 1:
               GlUtil.getBackVector(var6, var3);
               break;
            case 2:
               GlUtil.getForwardVector(var6, var3);
               break;
            case 3:
               GlUtil.getBottomVector(var6, var3);
               break;
            case 4:
               GlUtil.getRightVector(var6, var3);
               break;
            case 5:
               GlUtil.getLeftVector(var6, var3);
            }

            var9 = (var9 + 1) % 6;
            var6.scale(1.0F + (float)(var7 / 6) * 0.2F);
            this.targetPosition.add(var3.origin, var6);
            this.end.origin.set(this.targetPosition);
            KinematicCharacterControllerExt.KinematicClosestNotMeConvexResultCallback var10 = new KinematicCharacterControllerExt.KinematicClosestNotMeConvexResultCallback(this.ghostObject, this.upAxisDirection[this.upAxis], -1.0F);
            if (this.ghostObject == null) {
               System.err.println("Exception KinematikCharacter: warp out of collision ghost object " + this.ghostObject + " is null " + this.obj);
               return;
            }

            if (this.ghostObject.getBroadphaseHandle() == null) {
               System.err.println("Exception KinematikCharacter: warp out of collision broadphase handle of ghost object " + this.ghostObject + " is null " + this.obj);
               return;
            }

            var10.collisionFilterGroup = this.ghostObject.getBroadphaseHandle().collisionFilterGroup;
            var10.collisionFilterMask = this.ghostObject.getBroadphaseHandle().collisionFilterMask;
            this.warp(this.targetPosition);
            if (!(var5 = this.checkHit(var1, this.targetPosition, var4, 0.1F))) {
               System.err.println("[WARPING OT OF COLLISION][CHECK WARP] NOHIT -> can spawn here!! " + this.targetPosition + "; " + this.obj.getState());
            } else {
               System.err.println("[WARPING OT OF COLLISION][CHECK_SINGLE] HIT -> cannot spawn here!! " + this.targetPosition);
            }
         }

         if (var7 >= 10000) {
            try {
               throw new RuntimeException("Exceeded warping out of collision!!!!! " + var3.origin + "; " + var3.basis);
            } catch (Exception var8) {
               var8.printStackTrace();
            }
         }

      }
   }

   public Vector3f getLastActualWalkingDist() {
      return this.lastActualWalkingDist;
   }

   public int getUpdateNum() {
      return this.obj.getActionUpdateNum();
   }

   public void setUpdateNum(int var1) {
      this.obj.setActionUpdateNum(var1);
   }

   public String toString() {
      return super.toString() + "(" + this.obj + ")";
   }

   static class KinematicClosestNotMeConvexResultCallback extends ClosestConvexResultCallback {
      protected final Vector3f up;
      protected CollisionObject me;
      protected float minSlopeDot;
      Transform t = new Transform();
      private Segment segment;
      private Vector3b cubePos;

      public KinematicClosestNotMeConvexResultCallback(CollisionObject var1, Vector3f var2, float var3) {
         super(new Vector3f(), new Vector3f());
         this.me = var1;
         this.up = var2;
         this.minSlopeDot = var3;
      }

      public boolean needsCollision(BroadphaseProxy var1) {
         return var1.clientObject instanceof RigidDebrisBody ? false : super.needsCollision(var1);
      }

      public float addSingleResult(LocalConvexResult var1, boolean var2) {
         if (var1.hitCollisionObject == this.me) {
            return 1.0F;
         } else {
            Vector3f var3;
            if (var2) {
               var3 = var1.hitNormalLocal;
            } else {
               var3 = new Vector3f();
               this.hitCollisionObject.getWorldTransform(this.t).basis.transform(var1.hitNormalLocal, var3);
            }

            return this.up.dot(var3) < this.minSlopeDot ? 1.0F : super.addSingleResult(var1, var2);
         }
      }

      public void addSingleResult(LocalConvexResult var1, boolean var2, Segment var3, Vector3b var4) {
         this.addSingleResult(var1, var2);
         this.segment = var3;
         this.cubePos = new Vector3b(var4);
      }
   }
}
