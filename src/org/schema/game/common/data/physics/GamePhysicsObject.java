package org.schema.game.common.data.physics;

import org.schema.game.common.data.world.GameTransformable;

public interface GamePhysicsObject {
   GameTransformable getSimpleTransformableSendableObject();
}
