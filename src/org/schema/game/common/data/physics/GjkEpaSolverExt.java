package org.schema.game.common.data.physics;

import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.QuaternionUtil;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.linearmath.VectorUtil;
import com.bulletphysics.util.ArrayPool;
import com.bulletphysics.util.ObjectStackList;
import java.util.Arrays;
import javax.vecmath.Matrix3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;

public class GjkEpaSolverExt {
   protected final ArrayPool floatArrays;
   private final GjkEpaSolverVars q;
   protected final ObjectStackList stackMkv;
   protected final ObjectStackList stackHe;
   protected final ObjectStackList stackFace;
   private static final float cstInf = Float.MAX_VALUE;
   private static final float cst2Pi = 6.2831855F;
   private static final int GJK_maxiterations = 128;
   private static final int GJK_hashsize = 64;
   private static final int GJK_hashmask = 63;
   private static final float GJK_insimplex_eps = 1.0E-4F;
   private static final float GJK_sqinsimplex_eps = 9.999999E-9F;
   private static final int EPA_maxiterations = 256;
   private static final float EPA_inface_eps = 0.01F;
   private static final float EPA_accuracy = 0.001F;
   private final GjkEpaSolverExt.EPA epa;
   private final GjkEpaSolverExt.GJK gjk;
   private final GjkEpaSolverExt.Mkv swapTmp;
   private static int[] mod3 = new int[]{0, 1, 2, 0, 1};
   private static final int[][] tetrahedron_fidx = new int[][]{{2, 1, 0}, {3, 0, 1}, {3, 1, 2}, {3, 2, 0}};
   private static final int[][] tetrahedron_eidx = new int[][]{{0, 0, 2, 1}, {0, 1, 1, 1}, {0, 2, 3, 1}, {1, 0, 3, 2}, {2, 0, 1, 2}, {3, 0, 2, 2}};
   private static final int[][] hexahedron_fidx = new int[][]{{2, 0, 4}, {4, 1, 2}, {1, 4, 0}, {0, 3, 1}, {0, 2, 3}, {1, 3, 2}};
   private static final int[][] hexahedron_eidx = new int[][]{{0, 0, 4, 0}, {0, 1, 2, 1}, {0, 2, 1, 2}, {1, 1, 5, 2}, {1, 0, 2, 0}, {2, 2, 3, 2}, {3, 1, 5, 0}, {3, 0, 4, 2}, {5, 1, 4, 1}};

   public GjkEpaSolverExt() {
      this.floatArrays = ArrayPool.get(Float.TYPE);
      this.q = new GjkEpaSolverVars();
      this.stackMkv = new ObjectStackList(GjkEpaSolverExt.Mkv.class) {
         protected GjkEpaSolverExt.Mkv create() {
            return new GjkEpaSolverExt.Mkv();
         }
      };
      this.stackHe = new ObjectStackList(GjkEpaSolverExt.He.class) {
         protected GjkEpaSolverExt.He create() {
            return new GjkEpaSolverExt.He();
         }
      };
      this.stackFace = new ObjectStackList(GjkEpaSolverExt.Face.class) {
         protected GjkEpaSolverExt.Face create() {
            return new GjkEpaSolverExt.Face();
         }
      };
      this.epa = new GjkEpaSolverExt.EPA();
      this.gjk = new GjkEpaSolverExt.GJK();
      this.swapTmp = new GjkEpaSolverExt.Mkv();
   }

   protected void pushStack() {
      this.stackMkv.push();
      this.stackHe.push();
      this.stackFace.push();
   }

   protected void popStack() {
      this.stackMkv.pop();
      this.stackHe.pop();
      this.stackFace.pop();
   }

   public boolean collide(ConvexShape var1, Transform var2, ConvexShape var3, Transform var4, float var5, GjkEpaSolverExt.Results var6) {
      var6.witnesses[0].set(0.0F, 0.0F, 0.0F);
      var6.witnesses[1].set(0.0F, 0.0F, 0.0F);
      var6.normal.set(0.0F, 0.0F, 0.0F);
      var6.depth = 0.0F;
      var6.status = GjkEpaSolverExt.ResultsStatus.Separated;
      var6.epa_iterations = 0;
      var6.gjk_iterations = 0;
      this.gjk.init(var2.basis, var2.origin, var1, var4.basis, var4.origin, var3, var5 + 0.001F);

      try {
         boolean var9 = this.gjk.SearchOrigin();
         var6.gjk_iterations = this.gjk.iterations + 1;
         if (var9) {
            this.epa.reset(this.gjk);
            float var10 = this.epa.EvaluatePD();
            var6.epa_iterations = this.epa.iterations + 1;
            if (var10 > 0.0F) {
               var6.status = GjkEpaSolverExt.ResultsStatus.Penetrating;
               var6.normal.set(this.epa.normal);
               var6.depth = var10;
               var6.witnesses[0].set(this.epa.nearest[0]);
               var6.witnesses[1].set(this.epa.nearest[1]);
               return true;
            }

            if (this.epa.failed) {
               var6.status = GjkEpaSolverExt.ResultsStatus.EPA_Failed;
            }
         } else if (this.gjk.failed) {
            var6.status = GjkEpaSolverExt.ResultsStatus.GJK_Failed;
         }
      } finally {
         this.gjk.destroy();
      }

      return false;
   }

   public class EPA {
      public GjkEpaSolverExt.GJK gjk;
      public GjkEpaSolverExt.Face root;
      public int nfaces;
      public int iterations;
      public final Vector3f[][] features = new Vector3f[2][3];
      public final Vector3f[] nearest = new Vector3f[]{new Vector3f(), new Vector3f()};
      public final Vector3f normal = new Vector3f();
      public float depth;
      public boolean failed;
      private final GjkEpaSolverExt.Mkv[] basemkv;
      GjkEpaSolverExt.Face[] cf;
      GjkEpaSolverExt.Face[] ff;
      private final GjkEpaSolverExt.Face[] basefaces;

      public void reset(GjkEpaSolverExt.GJK var1) {
         this.root = null;
         this.nfaces = 0;
         this.iterations = 0;
         this.depth = 0.0F;
         this.failed = false;
         this.nearest[0].set(0.0F, 0.0F, 0.0F);
         this.nearest[1].set(0.0F, 0.0F, 0.0F);
         this.gjk = var1;
      }

      public EPA() {
         int var3;
         for(var3 = 0; var3 < this.features.length; ++var3) {
            for(int var2 = 0; var2 < this.features[var3].length; ++var2) {
               this.features[var3][var2] = new Vector3f();
            }
         }

         this.basemkv = new GjkEpaSolverExt.Mkv[5];

         for(var3 = 0; var3 < this.basemkv.length; ++var3) {
            this.basemkv[var3] = new GjkEpaSolverExt.Mkv();
         }

         this.cf = new GjkEpaSolverExt.Face[]{null};
         this.ff = new GjkEpaSolverExt.Face[]{null};
         this.basefaces = new GjkEpaSolverExt.Face[6];
      }

      public Vector3f GetCoordinates(GjkEpaSolverExt.Face var1, Vector3f var2) {
         Vector3f var3 = GjkEpaSolverExt.this.q.tmp28;
         Vector3f var4 = GjkEpaSolverExt.this.q.tmp29;
         Vector3f var5 = GjkEpaSolverExt.this.q.tmp30;
         Vector3f var6;
         (var6 = GjkEpaSolverExt.this.q.tmp31).scale(-var1.d, var1.n);
         float[] var7 = (float[])GjkEpaSolverExt.this.floatArrays.getFixed(3);
         var4.sub(var1.v[0].w, var6);
         var5.sub(var1.v[1].w, var6);
         var3.cross(var4, var5);
         var7[0] = FastMath.carmackLength(var3);
         var4.sub(var1.v[1].w, var6);
         var5.sub(var1.v[2].w, var6);
         var3.cross(var4, var5);
         var7[1] = FastMath.carmackLength(var3);
         var4.sub(var1.v[2].w, var6);
         var5.sub(var1.v[0].w, var6);
         var3.cross(var4, var5);
         var7[2] = FastMath.carmackLength(var3);
         float var8 = var7[0] + var7[1] + var7[2];
         var2.set(var7[1], var7[2], var7[0]);
         var2.scale(1.0F / (var8 > 0.0F ? var8 : 1.0F));
         GjkEpaSolverExt.this.floatArrays.release(var7);
         return var2;
      }

      public GjkEpaSolverExt.Face FindBest() {
         GjkEpaSolverExt.Face var1 = null;
         if (this.root != null) {
            GjkEpaSolverExt.Face var2 = this.root;
            float var3 = Float.MAX_VALUE;

            do {
               if (var2.d < var3) {
                  var3 = var2.d;
                  var1 = var2;
               }
            } while(null != (var2 = var2.next));
         }

         return var1;
      }

      public boolean Set(GjkEpaSolverExt.Face var1, GjkEpaSolverExt.Mkv var2, GjkEpaSolverExt.Mkv var3, GjkEpaSolverExt.Mkv var4) {
         Vector3f var5 = GjkEpaSolverExt.this.q.tmp32;
         Vector3f var6 = GjkEpaSolverExt.this.q.tmp33;
         Vector3f var7 = GjkEpaSolverExt.this.q.tmp34;
         Vector3f var8 = GjkEpaSolverExt.this.q.tmp35;
         var5.sub(var3.w, var2.w);
         var6.sub(var4.w, var2.w);
         var8.cross(var5, var6);
         float var9 = FastMath.carmackLength(var8);
         var5.cross(var2.w, var3.w);
         var6.cross(var3.w, var4.w);
         var7.cross(var4.w, var2.w);
         boolean var10 = var5.dot(var8) >= -0.01F && var6.dot(var8) >= -0.01F && var7.dot(var8) >= -0.01F;
         var1.v[0] = var2;
         var1.v[1] = var3;
         var1.v[2] = var4;
         var1.mark = 0;
         var1.n.scale(1.0F / (var9 > 0.0F ? var9 : Float.MAX_VALUE), var8);
         var1.d = Math.max(0.0F, -var1.n.dot(var2.w));
         return var10;
      }

      public GjkEpaSolverExt.Face NewFace(GjkEpaSolverExt.Mkv var1, GjkEpaSolverExt.Mkv var2, GjkEpaSolverExt.Mkv var3) {
         GjkEpaSolverExt.Face var4 = (GjkEpaSolverExt.Face)GjkEpaSolverExt.this.stackFace.get();
         if (this.Set(var4, var1, var2, var3)) {
            if (this.root != null) {
               this.root.prev = var4;
            }

            var4.prev = null;
            var4.next = this.root;
            this.root = var4;
            ++this.nfaces;
         } else {
            var4.prev = var4.next = null;
         }

         return var4;
      }

      public void Detach(GjkEpaSolverExt.Face var1) {
         if (var1.prev != null || var1.next != null) {
            --this.nfaces;
            if (var1 == this.root) {
               this.root = var1.next;
               this.root.prev = null;
            } else if (var1.next == null) {
               var1.prev.next = null;
            } else {
               var1.prev.next = var1.next;
               var1.next.prev = var1.prev;
            }

            var1.prev = var1.next = null;
         }

      }

      public void Link(GjkEpaSolverExt.Face var1, int var2, GjkEpaSolverExt.Face var3, int var4) {
         var1.f[var2] = var3;
         var3.e[var4] = var2;
         var3.f[var4] = var1;
         var1.e[var2] = var4;
      }

      public GjkEpaSolverExt.Mkv Support(Vector3f var1) {
         GjkEpaSolverExt.Mkv var2 = (GjkEpaSolverExt.Mkv)GjkEpaSolverExt.this.stackMkv.get();
         this.gjk.Support(var1, var2);
         return var2;
      }

      public int BuildHorizon(int var1, GjkEpaSolverExt.Mkv var2, GjkEpaSolverExt.Face var3, int var4, GjkEpaSolverExt.Face[] var5, GjkEpaSolverExt.Face[] var6) {
         int var7 = 0;
         if (var3.mark != var1) {
            var7 = GjkEpaSolverExt.mod3[var4 + 1];
            if (var3.n.dot(var2.w) + var3.d > 0.0F) {
               GjkEpaSolverExt.Face var8 = this.NewFace(var3.v[var7], var3.v[var4], var2);
               this.Link(var8, 0, var3, var4);
               if (var5[0] != null) {
                  this.Link(var5[0], 1, var8, 2);
               } else {
                  var6[0] = var8;
               }

               var5[0] = var8;
               var7 = 1;
            } else {
               int var9 = GjkEpaSolverExt.mod3[var4 + 2];
               this.Detach(var3);
               var3.mark = var1;
               var7 = 0 + this.BuildHorizon(var1, var2, var3.f[var7], var3.e[var7], var5, var6) + this.BuildHorizon(var1, var2, var3.f[var9], var3.e[var9], var5, var6);
            }
         }

         return var7;
      }

      public float EvaluatePD() {
         return this.EvaluatePD(0.001F);
      }

      public float EvaluatePD(float var1) {
         GjkEpaSolverExt.this.pushStack();

         float var14;
         try {
            Vector3f var2 = GjkEpaSolverExt.this.q.tmp36;
            GjkEpaSolverExt.Face var3 = null;
            int var4 = 1;
            this.depth = -3.4028235E38F;
            this.normal.set(0.0F, 0.0F, 0.0F);
            this.root = null;
            this.nfaces = 0;
            this.iterations = 0;
            this.failed = false;
            int var6;
            int var9;
            if (this.gjk.EncloseOrigin()) {
               int[][] var5 = null;
               var6 = 0;
               byte var7 = 0;
               int[][] var8 = null;
               var9 = 0;
               byte var10 = 0;
               Arrays.fill(this.basefaces, (Object)null);
               switch(this.gjk.order) {
               case 3:
                  var5 = GjkEpaSolverExt.tetrahedron_fidx;
                  var6 = 0;
                  var7 = 4;
                  var8 = GjkEpaSolverExt.tetrahedron_eidx;
                  var9 = 0;
                  var10 = 6;
                  break;
               case 4:
                  var5 = GjkEpaSolverExt.hexahedron_fidx;
                  var6 = 0;
                  var7 = 6;
                  var8 = GjkEpaSolverExt.hexahedron_eidx;
                  var9 = 0;
                  var10 = 9;
               }

               int var11;
               for(var11 = 0; var11 <= this.gjk.order; ++var11) {
                  this.basemkv[var11].set(this.gjk.simplex[var11]);
               }

               for(var11 = 0; var11 < var7; ++var6) {
                  this.basefaces[var11] = this.NewFace(this.basemkv[var5[var6][0]], this.basemkv[var5[var6][1]], this.basemkv[var5[var6][2]]);
                  ++var11;
               }

               for(var11 = 0; var11 < var10; ++var9) {
                  this.Link(this.basefaces[var8[var9][0]], var8[var9][1], this.basefaces[var8[var9][2]], var8[var9][3]);
                  ++var11;
               }
            }

            if (this.nfaces != 0) {
               GjkEpaSolverExt.Face var15;
               float var18;
               int var21;
               while(this.iterations < 256 && (var15 = this.FindBest()) != null) {
                  var2.negate(var15.n);
                  GjkEpaSolverExt.Mkv var17 = this.Support(var2);
                  var18 = var15.n.dot(var17.w) + var15.d;
                  var3 = var15;
                  if (var18 >= -var1) {
                     break;
                  }

                  this.cf[0] = null;
                  this.ff[0] = null;
                  var21 = 0;
                  this.Detach(var15);
                  ++var4;
                  var15.mark = var4;

                  for(var9 = 0; var9 < 3; ++var9) {
                     var21 += this.BuildHorizon(var4, var17, var15.f[var9], var15.e[var9], this.cf, this.ff);
                  }

                  if (var21 <= 2) {
                     break;
                  }

                  this.Link(this.cf[0], 1, this.ff[0], 2);
                  ++this.iterations;
               }

               if (var3 != null) {
                  Vector3f var16 = this.GetCoordinates(var3, GjkEpaSolverExt.this.q.tmp38);
                  this.normal.set(var3.n);
                  this.depth = Math.max(0.0F, var3.d);

                  for(var6 = 0; var6 < 2; ++var6) {
                     var18 = var6 != 0 ? -1.0F : 1.0F;

                     for(var21 = 0; var21 < 3; ++var21) {
                        var2.scale(var18, var3.v[var21].r);
                        this.gjk.LocalSupport(var2, var6, this.features[var6][var21]);
                     }
                  }

                  Vector3f var19 = GjkEpaSolverExt.this.q.tmp39;
                  Vector3f var20 = GjkEpaSolverExt.this.q.tmp40;
                  Vector3f var22 = GjkEpaSolverExt.this.q.tmp41;
                  var19.scale(var16.x, this.features[0][0]);
                  var20.scale(var16.y, this.features[0][1]);
                  var22.scale(var16.z, this.features[0][2]);
                  VectorUtil.add(this.nearest[0], var19, var20, var22);
                  var19.scale(var16.x, this.features[1][0]);
                  var20.scale(var16.y, this.features[1][1]);
                  var22.scale(var16.z, this.features[1][2]);
                  VectorUtil.add(this.nearest[1], var19, var20, var22);
               } else {
                  this.failed = true;
               }

               var14 = this.depth;
               return var14;
            }

            var14 = this.depth;
         } finally {
            GjkEpaSolverExt.this.popStack();
         }

         return var14;
      }
   }

   public static class Face {
      public final GjkEpaSolverExt.Mkv[] v = new GjkEpaSolverExt.Mkv[3];
      public final GjkEpaSolverExt.Face[] f = new GjkEpaSolverExt.Face[3];
      public final int[] e = new int[3];
      public final Vector3f n = new Vector3f();
      public float d;
      public int mark;
      public GjkEpaSolverExt.Face prev;
      public GjkEpaSolverExt.Face next;
   }

   public class GJK {
      public final GjkEpaSolverExt.He[] table;
      public final Matrix3f[] wrotations;
      public final Vector3f[] positions;
      public final ConvexShape[] shapes;
      public final GjkEpaSolverExt.Mkv[] simplex;
      public final Vector3f ray;
      public int order;
      public int iterations;
      public float margin;
      public boolean failed;

      public GJK() {
         this.table = new GjkEpaSolverExt.He[64];
         this.wrotations = new Matrix3f[]{new Matrix3f(), new Matrix3f()};
         this.positions = new Vector3f[]{new Vector3f(), new Vector3f()};
         this.shapes = new ConvexShape[2];
         this.simplex = new GjkEpaSolverExt.Mkv[5];
         this.ray = new Vector3f();

         for(int var2 = 0; var2 < this.simplex.length; ++var2) {
            this.simplex[var2] = new GjkEpaSolverExt.Mkv();
         }

      }

      public GJK(Matrix3f var2, Vector3f var3, ConvexShape var4, Matrix3f var5, Vector3f var6, ConvexShape var7) {
         this(var2, var3, var4, var5, var6, var7, 0.0F);
      }

      public GJK(Matrix3f var2, Vector3f var3, ConvexShape var4, Matrix3f var5, Vector3f var6, ConvexShape var7, float var8) {
         this.table = new GjkEpaSolverExt.He[64];
         this.wrotations = new Matrix3f[]{new Matrix3f(), new Matrix3f()};
         this.positions = new Vector3f[]{new Vector3f(), new Vector3f()};
         this.shapes = new ConvexShape[2];
         this.simplex = new GjkEpaSolverExt.Mkv[5];
         this.ray = new Vector3f();

         for(int var9 = 0; var9 < this.simplex.length; ++var9) {
            this.simplex[var9] = new GjkEpaSolverExt.Mkv();
         }

         this.init(var2, var3, var4, var5, var6, var7, var8);
      }

      public void init(Matrix3f var1, Vector3f var2, ConvexShape var3, Matrix3f var4, Vector3f var5, ConvexShape var6, float var7) {
         GjkEpaSolverExt.this.pushStack();
         this.wrotations[0].set(var1);
         this.positions[0].set(var2);
         this.shapes[0] = var3;
         this.wrotations[1].set(var4);
         this.positions[1].set(var5);
         this.shapes[1] = var6;
         this.margin = var7;
         this.failed = false;
      }

      public void destroy() {
         GjkEpaSolverExt.this.popStack();
      }

      public int Hash(Vector3f var1) {
         return ((int)(var1.x * 15461.0F) ^ (int)(var1.y * 83003.0F) ^ (int)(var1.z * 15473.0F)) * 169639 & 63;
      }

      public Vector3f LocalSupport(Vector3f var1, int var2, Vector3f var3) {
         Vector3f var4;
         MatrixUtil.transposeTransform(var4 = GjkEpaSolverExt.this.q.tmp0, var1, this.wrotations[var2]);
         this.shapes[var2].localGetSupportingVertex(var4, var3);
         this.wrotations[var2].transform(var3);
         var3.add(this.positions[var2]);
         return var3;
      }

      public void Support(Vector3f var1, GjkEpaSolverExt.Mkv var2) {
         var2.r.set(var1);
         Vector3f var3 = this.LocalSupport(var1, 0, GjkEpaSolverExt.this.q.tmp1);
         Vector3f var4;
         (var4 = GjkEpaSolverExt.this.q.tmp2).set(var1);
         var4.negate();
         var4 = this.LocalSupport(var4, 1, GjkEpaSolverExt.this.q.tmp3);
         var2.w.sub(var3, var4);
         var2.w.scaleAdd(this.margin, var1, var2.w);
      }

      public boolean FetchSupport() {
         int var1 = this.Hash(this.ray);

         GjkEpaSolverExt.He var2;
         for(var2 = this.table[var1]; var2 != null; var2 = var2.n) {
            if (var2.v.equals(this.ray)) {
               --this.order;
               return false;
            }
         }

         (var2 = (GjkEpaSolverExt.He)GjkEpaSolverExt.this.stackHe.get()).v.set(this.ray);
         var2.n = this.table[var1];
         this.table[var1] = var2;
         this.Support(this.ray, this.simplex[++this.order]);
         if (this.ray.dot(this.simplex[this.order].w) > 0.0F) {
            return true;
         } else {
            return false;
         }
      }

      public boolean SolveSimplex2(Vector3f var1, Vector3f var2) {
         if (var2.dot(var1) >= 0.0F) {
            Vector3f var3;
            (var3 = GjkEpaSolverExt.this.q.tmp4).cross(var2, var1);
            if (var3.lengthSquared() <= 9.999999E-9F) {
               return true;
            }

            this.ray.cross(var3, var2);
         } else {
            this.order = 0;
            this.simplex[0].set(this.simplex[1]);
            this.ray.set(var1);
         }

         return false;
      }

      public boolean SolveSimplex3(Vector3f var1, Vector3f var2, Vector3f var3) {
         Vector3f var4;
         (var4 = GjkEpaSolverExt.this.q.tmp5).cross(var2, var3);
         return this.SolveSimplex3a(var1, var2, var3, var4);
      }

      public boolean SolveSimplex3a(Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4) {
         Vector3f var5;
         (var5 = GjkEpaSolverExt.this.q.tmp6).cross(var4, var2);
         Vector3f var6;
         (var6 = GjkEpaSolverExt.this.q.tmp7).cross(var4, var3);
         if (var5.dot(var1) < -1.0E-4F) {
            this.order = 1;
            this.simplex[0].set(this.simplex[1]);
            this.simplex[1].set(this.simplex[2]);
            return this.SolveSimplex2(var1, var2);
         } else if (var6.dot(var1) > 1.0E-4F) {
            this.order = 1;
            this.simplex[1].set(this.simplex[2]);
            return this.SolveSimplex2(var1, var3);
         } else {
            float var7;
            if (Math.abs(var7 = var4.dot(var1)) > 1.0E-4F) {
               if (var7 > 0.0F) {
                  this.ray.set(var4);
               } else {
                  this.ray.negate(var4);
                  GjkEpaSolverExt.this.swapTmp.set(this.simplex[0]);
                  this.simplex[0].set(this.simplex[1]);
                  this.simplex[1].set(GjkEpaSolverExt.this.swapTmp);
               }

               return false;
            } else {
               return true;
            }
         }
      }

      public boolean SolveSimplex4(Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4) {
         Vector3f var5 = GjkEpaSolverExt.this.q.tmp8;
         Vector3f var6;
         (var6 = GjkEpaSolverExt.this.q.tmp9).cross(var2, var3);
         Vector3f var7;
         (var7 = GjkEpaSolverExt.this.q.tmp10).cross(var3, var4);
         Vector3f var8;
         (var8 = GjkEpaSolverExt.this.q.tmp11).cross(var4, var2);
         if (var6.dot(var1) > 1.0E-4F) {
            var5.set(var6);
            this.order = 2;
            this.simplex[0].set(this.simplex[1]);
            this.simplex[1].set(this.simplex[2]);
            this.simplex[2].set(this.simplex[3]);
            return this.SolveSimplex3a(var1, var2, var3, var5);
         } else if (var7.dot(var1) > 1.0E-4F) {
            var5.set(var7);
            this.order = 2;
            this.simplex[2].set(this.simplex[3]);
            return this.SolveSimplex3a(var1, var3, var4, var5);
         } else if (var8.dot(var1) > 1.0E-4F) {
            var5.set(var8);
            this.order = 2;
            this.simplex[1].set(this.simplex[0]);
            this.simplex[0].set(this.simplex[2]);
            this.simplex[2].set(this.simplex[3]);
            return this.SolveSimplex3a(var1, var4, var2, var5);
         } else {
            return true;
         }
      }

      public boolean SearchOrigin() {
         Vector3f var1;
         (var1 = GjkEpaSolverExt.this.q.tmp12).set(1.0F, 0.0F, 0.0F);
         return this.SearchOrigin(var1);
      }

      public boolean SearchOrigin(Vector3f var1) {
         Vector3f var2 = GjkEpaSolverExt.this.q.tmp13;
         Vector3f var3 = GjkEpaSolverExt.this.q.tmp14;
         Vector3f var4 = GjkEpaSolverExt.this.q.tmp15;
         Vector3f var5 = GjkEpaSolverExt.this.q.tmp16;
         this.iterations = 0;
         this.order = -1;
         this.failed = false;
         this.ray.set(var1);
         this.ray.normalize();
         Arrays.fill(this.table, (Object)null);
         this.FetchSupport();
         this.ray.negate(this.simplex[0].w);

         while(this.iterations < 128) {
            float var6 = FastMath.carmackLength(this.ray);
            this.ray.scale(1.0F / (var6 > 0.0F ? var6 : 1.0F));
            if (!this.FetchSupport()) {
               return false;
            }

            boolean var7 = false;
            switch(this.order) {
            case 1:
               var2.negate(this.simplex[1].w);
               var3.sub(this.simplex[0].w, this.simplex[1].w);
               var7 = this.SolveSimplex2(var2, var3);
               break;
            case 2:
               var2.negate(this.simplex[2].w);
               var3.sub(this.simplex[1].w, this.simplex[2].w);
               var4.sub(this.simplex[0].w, this.simplex[2].w);
               var7 = this.SolveSimplex3(var2, var3, var4);
               break;
            case 3:
               var2.negate(this.simplex[3].w);
               var3.sub(this.simplex[2].w, this.simplex[3].w);
               var4.sub(this.simplex[1].w, this.simplex[3].w);
               var5.sub(this.simplex[0].w, this.simplex[3].w);
               var7 = this.SolveSimplex4(var2, var3, var4, var5);
            }

            if (var7) {
               return true;
            }

            ++this.iterations;
         }

         this.failed = true;
         return false;
      }

      public boolean EncloseOrigin() {
         Vector3f var1 = GjkEpaSolverExt.this.q.tmp17;
         Vector3f var2 = GjkEpaSolverExt.this.q.tmp18;
         Vector3f var3 = GjkEpaSolverExt.this.q.tmp19;
         Vector3f var4;
         switch(this.order) {
         case 0:
         default:
            return false;
         case 1:
            (var4 = GjkEpaSolverExt.this.q.tmp20).sub(this.simplex[1].w, this.simplex[0].w);
            GjkEpaSolverExt.this.q.b[0].set(1.0F, 0.0F, 0.0F);
            GjkEpaSolverExt.this.q.b[1].set(0.0F, 1.0F, 0.0F);
            GjkEpaSolverExt.this.q.b[2].set(0.0F, 0.0F, 1.0F);
            GjkEpaSolverExt.this.q.b[0].cross(var4, GjkEpaSolverExt.this.q.b[0]);
            GjkEpaSolverExt.this.q.b[1].cross(var4, GjkEpaSolverExt.this.q.b[1]);
            GjkEpaSolverExt.this.q.b[2].cross(var4, GjkEpaSolverExt.this.q.b[2]);
            float var7 = GjkEpaSolverExt.this.q.b[0].lengthSquared();
            float var8 = GjkEpaSolverExt.this.q.b[1].lengthSquared();
            float var5 = GjkEpaSolverExt.this.q.b[2].lengthSquared();
            Quat4f var6 = GjkEpaSolverExt.this.q.tmp24;
            var1.normalize(var4);
            QuaternionUtil.setRotation(var6, var1, 2.0943952F);
            Matrix3f var9;
            MatrixUtil.setRotation(var9 = GjkEpaSolverExt.this.q.tmp25, var6);
            Vector3f var10;
            (var10 = GjkEpaSolverExt.this.q.tmp26).set(GjkEpaSolverExt.this.q.b[var7 > var8 ? (var7 > var5 ? 0 : 2) : (var8 > var5 ? 1 : 2)]);
            var1.normalize(var10);
            this.Support(var1, this.simplex[4]);
            var9.transform(var10);
            var1.normalize(var10);
            this.Support(var1, this.simplex[2]);
            var9.transform(var10);
            var1.normalize(var10);
            this.Support(var1, this.simplex[3]);
            var9.transform(var10);
            this.order = 4;
            return true;
         case 2:
            var2.sub(this.simplex[1].w, this.simplex[0].w);
            var3.sub(this.simplex[2].w, this.simplex[0].w);
            (var4 = GjkEpaSolverExt.this.q.tmp27).cross(var2, var3);
            var4.normalize();
            this.Support(var4, this.simplex[3]);
            var1.negate(var4);
            this.Support(var1, this.simplex[4]);
            this.order = 4;
            return true;
         case 3:
            return true;
         case 4:
            return true;
         }
      }
   }

   public static class He {
      public final Vector3f v = new Vector3f();
      public GjkEpaSolverExt.He n;
   }

   public static class Mkv {
      public final Vector3f w = new Vector3f();
      public final Vector3f r = new Vector3f();

      public void set(GjkEpaSolverExt.Mkv var1) {
         this.w.set(var1.w);
         this.r.set(var1.r);
      }
   }

   public static class Results {
      public GjkEpaSolverExt.ResultsStatus status;
      public final Vector3f[] witnesses = new Vector3f[]{new Vector3f(), new Vector3f()};
      public final Vector3f normal = new Vector3f();
      public float depth;
      public int epa_iterations;
      public int gjk_iterations;

      public void reset() {
         this.status = null;
         this.witnesses[0].set(0.0F, 0.0F, 0.0F);
         this.witnesses[1].set(0.0F, 0.0F, 0.0F);
         this.normal.set(0.0F, 0.0F, 0.0F);
         this.depth = 0.0F;
         this.epa_iterations = 0;
         this.gjk_iterations = 0;
      }
   }

   public static enum ResultsStatus {
      Separated,
      Penetrating,
      GJK_Failed,
      EPA_Failed;
   }
}
