package org.schema.game.common.data.physics;

import com.bulletphysics.collision.shapes.SphereShape;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;

public class ModifiedAABBSphereShape extends SphereShape {
   Vector3f extent = new Vector3f();
   private float ext;

   public ModifiedAABBSphereShape(float var1, float var2) {
      super(var1);
      this.ext = var2;
   }

   public void getAabb(Transform var1, Vector3f var2, Vector3f var3) {
      super.getAabb(var1, var2, var3);
      Vector3f var4 = var1.origin;
      this.extent.set(this.getMargin() + this.ext, this.getMargin() + this.ext, this.getMargin() + this.ext);
      var2.sub(var4, this.extent);
      var3.add(var4, this.extent);
   }
}
