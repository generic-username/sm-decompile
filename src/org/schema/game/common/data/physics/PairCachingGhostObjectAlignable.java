package org.schema.game.common.data.physics;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.linearmath.Transform;
import org.schema.game.common.data.world.GameTransformable;
import org.schema.schine.network.objects.container.PhysicsDataContainer;

public class PairCachingGhostObjectAlignable extends PairCachingGhostObjectExt implements GamePhysicsObject, RelativeBody {
   private final GameTransformable obj;
   public Transform localWorldTransform;
   public int attachedOrientation = 2;

   public PairCachingGhostObjectAlignable(CollisionType var1, PhysicsDataContainer var2, GameTransformable var3) {
      super(var1, var2);
      this.obj = var3;
   }

   public boolean checkCollideWith(CollisionObject var1) {
      return !(var1 instanceof PairCachingGhostObjectAlignable);
   }

   public String toString() {
      return "PCGhostObjExt(" + this.getUserPointer() + "->Attached(" + this.getAttached() + "))@" + this.hashCode();
   }

   public GameTransformable getSimpleTransformableSendableObject() {
      return this.obj;
   }

   public GameTransformable getObj() {
      return this.obj;
   }
}
