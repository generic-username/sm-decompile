package org.schema.game.common.data.physics;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.QuaternionUtil;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collections;
import java.util.Iterator;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.game.common.data.physics.qhull.DPoint3d;
import org.schema.game.common.data.physics.qhull.QuickHull3D;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.graphicsengine.forms.debug.DebugPoint;

public class VonoroiShatter {
   static Vector3fb curVoronoiPoint = new Vector3fb();

   public static Vector3fb sub(Vector3fb var0, Vector3fb var1, Vector3fb var2) {
      var2.sub(var0, var1);
      return var2;
   }

   static void getVerticesInsidePlanes(ObjectArrayList var0, ObjectArrayList var1, IntOpenHashSet var2) {
      var1.clear();
      var2.clear();
      int var3 = var0.size();
      System.err.println("PLANES: " + var0);

      for(int var4 = 0; var4 < var3; ++var4) {
         Vector3fb var5 = (Vector3fb)var0.get(var4);

         for(int var6 = var4 + 1; var6 < var3; ++var6) {
            Vector3fb var7 = (Vector3fb)var0.get(var6);
            Vector3fb var8;
            if ((var8 = new Vector3fb(var5.cross(var7))).lengthSquared() > 1.0E-4F) {
               for(int var9 = var6 + 1; var9 < var3; ++var9) {
                  Vector3fb var10 = (Vector3fb)var0.get(var9);
                  Vector3fb var11 = new Vector3fb(var7.cross(var10));
                  Vector3fb var12 = new Vector3fb(var10.cross(var5));
                  float var13;
                  if (var11.lengthSquared() > 1.0E-4F && var12.lengthSquared() > 1.0E-4F && Math.abs(var13 = var5.dot(var11)) > 1.0E-4F) {
                     var13 = -1.0F / var13;
                     Vector3fb var14 = new Vector3fb();
                     var11.scale(-var5.w);
                     var12.scale(-var7.w);
                     var8.scale(-var10.w);
                     var14.set(var11);
                     var14.add(var12);
                     var14.add(var8);
                     var14.scale(var13);

                     int var15;
                     for(var15 = 0; var15 < var3 && (var11 = (Vector3fb)var0.get(var15)).dot(var14) - var11.w <= 1.0E-6F; ++var15) {
                     }

                     System.err.println("POTENTIAL VERT: " + var14 + "; " + var3 + " ---- " + var15);
                     if (var15 == var3) {
                        var1.add(var14);
                        var2.add(var4);
                        var2.add(var6);
                        var2.add(var9);
                     }
                  }
               }
            }
         }
      }

   }

   public static float triple(Vector3f var0, Vector3f var1, Vector3f var2) {
      return var0.x * (var1.y * var2.z - var1.z * var2.y) + var0.y * (var1.z * var2.x - var1.x * var2.z) + var0.z * (var1.x * var2.y - var1.y * var2.x);
   }

   public static void voronoiBBShatter(PhysicsExt var0, ObjectArrayList var1, Vector3fb var2, Vector3fb var3, Quat4f var4, Vector3fb var5, float var6) {
      var5 = new Vector3fb(QuaternionUtil.quatRotate(var4, new Vector3fb(1.0F, 0.0F, 0.0F), new Vector3fb()));
      Vector3fb var28 = new Vector3fb(QuaternionUtil.quatRotate(var4, new Vector3fb(0.0F, 1.0F, 0.0F), new Vector3fb()));
      Vector3fb var7 = new Vector3fb(QuaternionUtil.quatRotate(var4, new Vector3fb(0.0F, 0.0F, 1.0F), new Vector3fb()));
      (new Quat4f(var4)).inverse();
      ObjectArrayList var27 = new ObjectArrayList();
      ObjectArrayList var10 = new ObjectArrayList(var1);
      ObjectArrayList var12 = new ObjectArrayList();
      IntOpenHashSet var13 = new IntOpenHashSet();
      int var16 = var1.size();
      System.err.println("ADDING: " + var16 + " SHARDS");

      for(int var14 = 0; var14 < var16; ++var14) {
         curVoronoiPoint.set((Vector3fb)var1.get(var14));
         Vector3fb var8 = new Vector3fb();
         Vector3fb var9 = new Vector3fb();
         var8.sub(curVoronoiPoint, var3);
         var9.sub(var2, curVoronoiPoint);
         var12.clear();

         for(int var17 = 0; var17 < 6; ++var17) {
            var12.add(new Vector3fb());
         }

         System.err.println("RBB: " + var8 + "; nrbb: " + var9);
         ((Vector3fb)var12.get(0)).set(var5);
         ((Vector3fb)var12.get(0)).w = -var8.x;
         ((Vector3fb)var12.get(1)).set(var28);
         ((Vector3fb)var12.get(1)).w = -var8.y;
         ((Vector3fb)var12.get(2)).set(var7);
         ((Vector3fb)var12.get(2)).w = -var8.z;
         ((Vector3fb)var12.get(3)).set(var5);
         ((Vector3fb)var12.get(3)).negate();
         ((Vector3fb)var12.get(3)).w = -var9.x;
         ((Vector3fb)var12.get(4)).set(var28);
         ((Vector3fb)var12.get(4)).negate();
         ((Vector3fb)var12.get(4)).w = -var9.y;
         ((Vector3fb)var12.get(5)).set(var7);
         ((Vector3fb)var12.get(5)).negate();
         ((Vector3fb)var12.get(5)).w = -var9.z;
         System.err.println("PLANE EQUATION FROM " + curVoronoiPoint + "; " + var12);
         float var30 = Float.MAX_VALUE;
         Collections.sort(var10, new PComp(curVoronoiPoint));
         Iterator var36 = var12.iterator();

         Vector3f var18;
         DebugPoint var29;
         while(var36.hasNext()) {
            var8 = (Vector3fb)var36.next();
            (var18 = new Vector3f(var8)).scale(var8.w);
            var18.add(curVoronoiPoint);
            if (var14 == 0) {
               (var29 = new DebugPoint(var18, new Vector4f(0.0F, 0.0F, 1.0F, 1.0F), 0.12F)).LIFETIME = 7000L;
               DebugDrawer.points.add(var29);
            }

            if (var14 == 1) {
               (var29 = new DebugPoint(var18, new Vector4f(0.0F, 1.0F, 0.0F, 1.0F), 0.12F)).LIFETIME = 7000L;
               DebugDrawer.points.add(var29);
            }
         }

         Vector3fb var11;
         int var15;
         float var31;
         int var32;
         int var35;
         int var38;
         for(var15 = 1; var15 < var16 && (var31 = (var11 = sub((Vector3fb)var10.get(var15), curVoronoiPoint, new Vector3fb())).length()) <= var30; ++var15) {
            (var9 = new Vector3fb(var11)).normalize();
            var9.w = -var31 / 2.0F;
            var12.add(var9);
            getVerticesInsidePlanes(var12, var27, var13);
            if (var27.size() == 0) {
               break;
            }

            if ((var32 = var13.size()) != var12.size()) {
               IntIterator var37 = var13.iterator();
               new ObjectArrayList();

               for(var35 = 0; var35 < var32; ++var35) {
                  var38 = var37.nextInt();
                  ((Vector3fb)var12.get(var35)).set((Vector3fb)var12.get(var38));
               }

               while(var12.size() > var32) {
                  var12.remove(var12.size() - 1);
               }
            }

            var30 = ((Vector3fb)var27.get(0)).length();

            for(var35 = 1; var35 < var27.size(); ++var35) {
               var31 = ((Vector3fb)var27.get(var35)).length();
               if (var30 < var31) {
                  var30 = var31;
               }
            }

            var36 = var12.iterator();

            while(var36.hasNext()) {
               var8 = (Vector3fb)var36.next();
               (var18 = new Vector3f(var8)).scale(var8.w);
               var18.add(curVoronoiPoint);
               (var29 = new DebugPoint(var18, new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), 0.11F)).LIFETIME = 7000L;
               DebugDrawer.points.add(var29);
            }

            var30 *= 2.0F;
         }

         if (var27.size() >= 4) {
            System.err.println("ADDING VERTS: " + var27.size());
            com.bulletphysics.util.ObjectArrayList var39;
            (var39 = new com.bulletphysics.util.ObjectArrayList(var27.size())).addAll(var27);
            System.err.println("VERTICES: " + var39);
            DPoint3d[] var33 = new DPoint3d[var39.size()];

            for(var38 = 0; var38 < var33.length; ++var38) {
               var33[var38] = new DPoint3d((double)((Vector3f)var39.get(var38)).x, (double)((Vector3f)var39.get(var38)).y, (double)((Vector3f)var39.get(var38)).z);
            }

            QuickHull3D var40;
            try {
               var40 = new QuickHull3D(var33);
            } catch (Exception var26) {
               var26.printStackTrace();
               continue;
            }

            var32 = var40.getNumFaces();
            float var20 = 0.0F;
            Vector3fb var21 = new Vector3fb(0.0F, 0.0F, 0.0F);

            for(var15 = 0; var15 < var32; ++var15) {
               int[] var22;
               int var34 = (var22 = var40.getFaces()[var15])[0];
               var35 = var22[1];
               int var23 = 2;

               for(int var19 = var22[2]; var19 < var22.length; var19 = var23) {
                  float var24 = triple(new Vector3fb(var40.getVertices()[var34]), new Vector3fb(var40.getVertices()[var35]), new Vector3fb(var40.getVertices()[var19]));
                  var20 += var24;
                  Vector3fb var25;
                  (var25 = new Vector3fb(var40.getVertices()[var34])).add(new Vector3fb(var40.getVertices()[var35]));
                  var25.add(new Vector3fb(var40.getVertices()[var19]));
                  var25.scale(var24);
                  var21.add(var25);
                  var23 += 2;
                  var35 = var23 - 1;
               }
            }

            var21.scale(1.0F / (var20 * 4.0F));
            int var41 = var39.size();

            for(var15 = 0; var15 < var41; ++var15) {
               ((Vector3f)var39.get(var15)).sub(var21);
            }

            ConvexHullShapeExt var42;
            (var42 = new ConvexHullShapeExt(var39)).setMargin(0.0F);
            Transform var43;
            (var43 = new Transform()).setIdentity();
            curVoronoiPoint.add(var21);
            var43.origin.set(curVoronoiPoint);
            curVoronoiPoint.sub(var21);
            RigidBody var44;
            (var44 = var0.getBodyFromShape(var42, 0.0F, var43)).getCollisionShape().setMargin(0.0F);

            assert !var44.getCollisionShape().isConcave();
         }
      }

   }
}
