package org.schema.game.common.data.physics;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.MotionState;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Matrix3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;

public class RigidBodySegmentController extends RigidBodyExt implements GamePhysicsObject, RelativeBody {
   private final SegmentController segmentController;
   private final Vector3f angularVeloTmp = new Vector3f();
   private final Vector3f linearVeloTmp = new Vector3f();
   private final Vector3f axis = new Vector3f();
   private final Matrix3f tmp = new Matrix3f();
   private final Matrix3f dmat = new Matrix3f();
   private final Quat4f dorn = new Quat4f();
   public String virtualString;
   public Vector3i virtualSec;
   public SegmentController undockingProtection;
   private boolean collisionException;
   private boolean changedShape;
   private int oldSize;
   private Vector3f tmpLin = new Vector3f();
   public boolean hadRecoil;
   private long lastRecoil;
   private long lastIntegrate;

   public RigidBodySegmentController(SegmentController var1, float var2, MotionState var3, CollisionShape var4) {
      super(CollisionType.CUBE_STRUCTURE, var2, var3, var4);
      this.segmentController = var1;
      this.interpolationWorldTransform.setIdentity();
   }

   public RigidBodySegmentController(SegmentController var1, float var2, MotionState var3, CollisionShape var4, Vector3f var5) {
      super(CollisionType.CUBE_STRUCTURE, var2, var3, var4, var5);
      this.segmentController = var1;
      this.interpolationWorldTransform.setIdentity();
   }

   public RigidBodySegmentController(SegmentController var1, RigidBodyConstructionInfo var2) {
      super(CollisionType.CUBE_STRUCTURE, var2);
      this.segmentController = var1;
      this.interpolationWorldTransform.setIdentity();
   }

   public void predictIntegratedTransform(float var1, Transform var2) {
      super.predictIntegratedTransform(var1, var2);
   }

   public SegmentController getSegmentController() {
      return this.segmentController;
   }

   public String toString() {
      String var1 = "";
      if (this.isCollisionException()) {
         var1 = "[EXCEPT]";
      }

      if (this.getCollisionShape() instanceof CubeShape) {
         String var2 = "" + this.segmentController.getSectorId();
         if (this.segmentController.isOnServer()) {
            Sector var3;
            if ((var3 = ((GameServerState)this.segmentController.getState()).getUniverse().getSector(this.segmentController.getSectorId())) != null) {
               var2 = var3.toString();
            } else {
               var2 = "null";
            }
         }

         return "{RigBEx" + (this.virtualString != null ? this.virtualString : "Orig") + "@" + this.hashCode() + this.segmentController + ";COLSHAPE:" + this.getCollisionShape() + ";AT" + this.getWorldTransform(new Transform()).origin + "(SID: " + var2 + ")}" + var1;
      } else {
         return "{RigBEx" + (this.virtualString != null ? this.virtualString : "Orig") + "@" + this.hashCode() + "(" + this.getCollisionShape() + ")}" + var1;
      }
   }

   public Sector getSectorOnServer() {
      assert this.segmentController.isOnServer();

      return ((GameServerState)this.segmentController.getState()).getUniverse().getSector(this.segmentController.getSectorId());
   }

   public SimpleTransformableSendableObject getSimpleTransformableSendableObject() {
      return this.segmentController;
   }

   public boolean isCollisionException() {
      return this.collisionException;
   }

   public void setCollisionException(boolean var1) {
      this.collisionException = var1;
   }

   public boolean isChangedShape() {
      return this.changedShape;
   }

   public void setChangedShape(boolean var1) {
      this.changedShape = var1;
   }

   public void setActivationState(int var1) {
      if (this.getSegmentController().getTotalElements() != this.oldSize) {
         this.setChangedShape(true);
         this.oldSize = this.getSegmentController().getTotalElements();
      }

      super.setActivationState(var1);
   }

   public void activate(boolean var1) {
      super.activate(var1);
   }

   public void setWorldTransform(Transform var1) {
      super.setWorldTransform(var1);
   }

   public void setInterpolationWorldTransform(Transform var1) {
      super.setInterpolationWorldTransform(var1);
   }

   public boolean checkCollideWith(CollisionObject var1) {
      return !this.isCollisionException() ? super.checkCollideWith(var1) : false;
   }

   public boolean isVirtual() {
      return this.virtualSec != null;
   }

   public void saveKinematicState(float var1) {
      if (var1 != 0.0F) {
         if (this.getMotionState() != null) {
            this.getMotionState().getWorldTransform(this.worldTransform);
         }

         this.getLinearVelocity(this.linearVeloTmp);
         this.getAngularVelocity(this.angularVeloTmp);
         if (this.linearVeloTmp.lengthSquared() > 0.0F || this.angularVeloTmp.lengthSquared() > 0.0F) {
            TransformTools.calculateVelocity(this.interpolationWorldTransform, this.worldTransform, var1, this.linearVeloTmp, this.angularVeloTmp, this.axis, this.tmp, this.dmat, this.dorn);
            this.setLinearVelocity(this.linearVeloTmp);
            this.setAngularVelocity(this.angularVeloTmp);
         }

         this.interpolationLinearVelocity.set(this.linearVeloTmp);
         this.interpolationAngularVelocity.set(this.angularVeloTmp);
         this.interpolationWorldTransform.set(this.worldTransform);
      }

   }

   public void setCenterOfMassTransform(Transform var1) {
      if (this.isStaticOrKinematicObject()) {
         this.interpolationWorldTransform.set(this.worldTransform);
      } else {
         this.interpolationWorldTransform.set(var1);
      }

      this.getLinearVelocity(this.interpolationLinearVelocity);
      this.getAngularVelocity(this.interpolationAngularVelocity);
      this.worldTransform.set(var1);
      this.updateInertiaTensor();
   }

   public void applyGravity() {
      assert !Vector3fTools.isNan(this.getLinearVelocity(this.tmpLin)) : this.getLinearVelocity(this.tmpLin);

      super.applyGravity();

      assert !Vector3fTools.isNan(this.getLinearVelocity(this.tmpLin)) : this.getLinearVelocity(this.tmpLin);

   }

   public void integrateVelocities(float var1) {
      this.getLinearVelocity(this.tmpLin).length();

      assert !Float.isNaN(this.getInvMass()) : this.getInvMass();

      assert !Vector3fTools.isNan(this.totalForce) : this.totalForce;

      assert !Vector3fTools.isNan(this.getLinearVelocity(this.tmpLin)) : this.getLinearVelocity(this.tmpLin);

      super.integrateVelocities(var1);

      assert !Vector3fTools.isNan(this.getLinearVelocity(this.tmpLin)) : this.getLinearVelocity(this.tmpLin);

      if (this.hadRecoil || this.lastRecoil == this.segmentController.getState().getUpdateTime()) {
         Vector3f var2;
         if ((var2 = this.getLinearVelocity(this.tmpLin)).length() > this.segmentController.getMaxServerSpeed()) {
            var2.normalize();
            var2.scale(this.segmentController.getMaxServerSpeed());
            this.setLinearVelocity(var2);
         }

         this.lastRecoil = this.segmentController.getState().getUpdateTime();
         this.hadRecoil = false;
      }

      this.lastIntegrate = this.segmentController.getState().getUpdateTime();
   }

   public void clearForces() {
      if (this.segmentController.getState().getUpdateTime() == this.lastIntegrate) {
         super.clearForces();
      }

   }

   public void applyDamping(float var1) {
      assert !Vector3fTools.isNan(this.getLinearVelocity(this.tmpLin)) : this.getLinearVelocity(this.tmpLin);

      super.applyDamping(var1);

      assert !Vector3fTools.isNan(this.getLinearVelocity(this.tmpLin)) : this.getLinearVelocity(this.tmpLin);

   }

   public void applyCentralForce(Vector3f var1) {
      assert !Vector3fTools.isNan(var1) : var1;

      super.applyCentralForce(var1);

      assert !Vector3fTools.isNan(this.totalForce) : this.totalForce;

      assert !Vector3fTools.isNan(this.getLinearVelocity(this.tmpLin));

   }

   public void applyTorque(Vector3f var1) {
      super.applyTorque(var1);

      assert !Vector3fTools.isNan(this.getAngularVelocity(this.tmpLin));

   }

   public void applyForce(Vector3f var1, Vector3f var2) {
      super.applyForce(var1, var2);

      assert !Vector3fTools.isNan(this.getLinearVelocity(this.tmpLin));

   }

   public void applyCentralImpulse(Vector3f var1) {
      super.applyCentralImpulse(var1);

      assert !Vector3fTools.isNan(this.getLinearVelocity(this.tmpLin));

   }

   public void applyTorqueImpulse(Vector3f var1) {
      super.applyTorqueImpulse(var1);

      assert !Vector3fTools.isNan(this.getLinearVelocity(this.tmpLin));

   }

   public void applyImpulse(Vector3f var1, Vector3f var2) {
      super.applyImpulse(var1, var2);

      assert !Vector3fTools.isNan(this.getLinearVelocity(this.tmpLin));

   }

   public void setLinearVelocity(Vector3f var1) {
      if (this.virtualSec == null) {
         assert !Vector3fTools.isNan(var1);

         super.setLinearVelocity(var1);
      }

   }

   public void setAngularVelocity(Vector3f var1) {
      if (this.virtualSec == null) {
         super.setAngularVelocity(var1);
      }

   }

   public boolean isRelatedTo(CollisionObject var1) {
      return var1 != null && var1 instanceof RigidBodySegmentController && ((RigidBodySegmentController)var1).getSegmentController() == this.getSegmentController();
   }

   public void setTorque(Vector3f var1) {
      this.totalTorque.set(var1);
   }
}
