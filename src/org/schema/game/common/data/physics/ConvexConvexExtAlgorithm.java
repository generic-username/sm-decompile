package org.schema.game.common.data.physics;

import com.bulletphysics.collision.broadphase.CollisionAlgorithm;
import com.bulletphysics.collision.broadphase.CollisionAlgorithmConstructionInfo;
import com.bulletphysics.collision.broadphase.DispatcherInfo;
import com.bulletphysics.collision.dispatch.CollisionAlgorithmCreateFunc;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.ConvexConvexAlgorithm;
import com.bulletphysics.collision.dispatch.ManifoldResult;
import com.bulletphysics.collision.narrowphase.ConvexPenetrationDepthSolver;
import com.bulletphysics.collision.narrowphase.SimplexSolverInterface;

public class ConvexConvexExtAlgorithm extends ConvexConvexAlgorithm {
   public void processCollision(CollisionObject var1, CollisionObject var2, DispatcherInfo var3, ManifoldResult var4) {
      if (!(var1.getCollisionShape() instanceof CubeShape) && !(var2.getCollisionShape() instanceof CubeShape)) {
         super.processCollision(var1, var2, var3, var4);
      } else {
         System.err.println("WARNING: tried imcompatible collision algorithm " + var1 + "; " + var2);
      }
   }

   public static class CreateFunc extends CollisionAlgorithmCreateFunc {
      private final com.bulletphysics.util.ObjectPool pool = com.bulletphysics.util.ObjectPool.get(ConvexConvexExtAlgorithm.class);
      public ConvexPenetrationDepthSolver pdSolver;
      public SimplexSolverInterface simplexSolver;

      public CreateFunc(SimplexSolverInterface var1, ConvexPenetrationDepthSolver var2) {
         this.simplexSolver = var1;
         this.pdSolver = var2;
      }

      public CollisionAlgorithm createCollisionAlgorithm(CollisionAlgorithmConstructionInfo var1, CollisionObject var2, CollisionObject var3) {
         ConvexConvexExtAlgorithm var4;
         (var4 = (ConvexConvexExtAlgorithm)this.pool.get()).init(var1.manifold, var1, var2, var3, this.simplexSolver, this.pdSolver);
         return var4;
      }

      public void releaseCollisionAlgorithm(CollisionAlgorithm var1) {
         this.pool.release((ConvexConvexExtAlgorithm)var1);
      }
   }
}
