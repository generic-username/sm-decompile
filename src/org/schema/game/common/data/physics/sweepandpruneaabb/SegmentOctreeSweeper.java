package org.schema.game.common.data.physics.sweepandpruneaabb;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import javax.vecmath.Vector3f;
import org.schema.game.common.data.physics.octree.IntersectionCallback;

public class SegmentOctreeSweeper extends Sweeper {
   private Vector3f outOuterMin = new Vector3f();
   private Vector3f outOuterMax = new Vector3f();

   protected int fillAxis(IntersectionCallback var1, Transform var2, SweepPoint[] var3, List var4, Comparator var5, int var6) {
      IntersectionCallback var10 = var1;
      IntArrayList var11;
      int var7 = (var11 = (IntArrayList)var4).size();
      var3 = this.ensureSize(var3, var7);

      int var9;
      for(var9 = 0; var9 < var7; ++var9) {
         int var8 = var11.getInt(var9);
         var10.getAabbOnly(var8, this.outOuterMin, this.outOuterMax);
         var3[var9] = this.getPoint(var8, this.outOuterMin, this.outOuterMax, var6 + var9);
      }

      Arrays.sort(var3, 0, var9, var5);
      return var9;
   }
}
