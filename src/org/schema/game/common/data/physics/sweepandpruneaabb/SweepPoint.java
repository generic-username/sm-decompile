package org.schema.game.common.data.physics.sweepandpruneaabb;

import javax.vecmath.Vector3f;

public class SweepPoint {
   public Object seg;
   public float minX;
   public float minY;
   public float minZ;
   public float maxX;
   public float maxY;
   public float maxZ;
   public int axis;
   public boolean hasXPair;
   public boolean hasYPair;
   public boolean hasZPair;
   private int hash;

   public void set(Object var1, Vector3f var2, Vector3f var3, int var4) {
      this.minX = var2.x;
      this.minY = var2.y;
      this.minZ = var2.z;
      this.maxX = var3.x;
      this.maxY = var3.y;
      this.maxZ = var3.z;
      this.seg = var1;
      this.hash = var4;
      this.hasXPair = false;
      this.hasYPair = false;
      this.hasZPair = false;
   }

   public int hashCode() {
      return this.hash;
   }

   public boolean equals(Object var1) {
      return this.hash == ((SweepPoint)var1).hash;
   }

   public String toString() {
      return "X(" + this.minX + ", " + this.maxX + ")";
   }

   public String toFullString() {
      return "SweepPoint [seg=" + this.seg + ", (" + this.minX + ", " + this.minY + ", " + this.minZ + "), (" + this.maxX + ", " + this.maxY + ", " + this.maxZ + "), hash=" + this.hash + "]";
   }

   public float getSort() {
      return this.minX;
   }

   public boolean equalsPos(SweepPoint var1) {
      return this.minX == var1.minX && this.minY == var1.minY && this.minZ == var1.minZ;
   }
}
