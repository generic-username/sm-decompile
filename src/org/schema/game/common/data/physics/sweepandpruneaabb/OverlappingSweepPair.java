package org.schema.game.common.data.physics.sweepandpruneaabb;

public class OverlappingSweepPair {
   public SweepPoint a;
   public SweepPoint b;
   private int hash;

   public OverlappingSweepPair() {
   }

   public OverlappingSweepPair(SweepPoint var1, SweepPoint var2) {
      this.set(var1, var2);
   }

   public static int getHashCode(SweepPoint var0, SweepPoint var1) {
      return var0.hashCode() + var1.hashCode() * 100000;
   }

   public boolean contains(SweepPoint var1) {
      return this.a.equals(var1) || this.b.equals(var1);
   }

   public void set(SweepPoint var1, SweepPoint var2) {
      this.a = var1;
      this.b = var2;
      this.hash = getHashCode(var1, var2);
   }

   public int hashCode() {
      return this.hash;
   }

   public boolean equals(Object var1) {
      OverlappingSweepPair var2 = (OverlappingSweepPair)var1;
      return this.hash == var2.hash;
   }

   public String toString() {
      return "Pair(" + this.a + ", " + this.b + ")";
   }

   public String toFullString() {
      return "PairHash{[" + this.hash + "](" + this.a.toFullString() + ", " + this.b.toFullString() + ")}";
   }
}
