package org.schema.game.common.data.physics.sweepandpruneaabb;

import com.bulletphysics.linearmath.Transform;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import javax.vecmath.Vector3f;
import org.schema.game.common.data.physics.CubesCompoundShape;
import org.schema.game.common.data.physics.CubesCompoundShapeChild;

public class CompoundCollisionObjectSweeper extends Sweeper {
   private Vector3f outOuterMin = new Vector3f();
   private Vector3f outOuterMax = new Vector3f();

   protected int fillAxis(CubesCompoundShape var1, Transform var2, SweepPoint[] var3, List var4, Comparator var5, int var6) {
      int var8 = var4.size();
      var3 = this.ensureSize(var3, var8);

      int var7;
      for(var7 = 0; var7 < var8; ++var7) {
         CubesCompoundShapeChild var9;
         (var9 = (CubesCompoundShapeChild)var4.get(var7)).tmpChildIndex = var7;
         var1.getAABB(var7, var2, this.outOuterMin, this.outOuterMax, this.varSet);
         var3[var7] = this.getPoint(var9, this.outOuterMin, this.outOuterMax, var6 + var7);
      }

      Arrays.sort(var3, 0, var7, var5);
      return var7;
   }
}
