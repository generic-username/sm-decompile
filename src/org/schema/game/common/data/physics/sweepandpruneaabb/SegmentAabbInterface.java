package org.schema.game.common.data.physics.sweepandpruneaabb;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.game.common.data.physics.AABBVarSet;
import org.schema.game.common.data.world.Segment;

public interface SegmentAabbInterface {
   void getSegmentAabb(Segment var1, Transform var2, Vector3f var3, Vector3f var4, Vector3f var5, Vector3f var6, AABBVarSet var7);

   void getAabb(Transform var1, Vector3f var2, Vector3f var3);

   void getAabbUncached(Transform var1, Vector3f var2, Vector3f var3, boolean var4);

   void getAabbIdent(Vector3f var1, Vector3f var2);
}
