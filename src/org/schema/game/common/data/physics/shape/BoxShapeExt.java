package org.schema.game.common.data.physics.shape;

import com.bulletphysics.collision.broadphase.BroadphaseNativeType;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.ScalarUtil;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.linearmath.VectorUtil;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

public class BoxShapeExt extends BoxShape {
   Vector4f pl = new Vector4f();
   Vector3f he = new Vector3f();
   Vector3f he1 = new Vector3f();
   Vector3f im = new Vector3f();
   private Vector3f marginV = new Vector3f();
   private Vector3f d = new Vector3f();

   public BoxShapeExt(Vector3f var1) {
      super(var1);
   }

   public Vector3f getHalfExtentsWithMargin(Vector3f var1) {
      Vector3f var2 = this.getHalfExtentsWithoutMargin(var1);
      Vector3f var3;
      (var3 = this.he).set(this.getMargin(), this.getMargin(), this.getMargin());
      var2.add(var3);
      return var1;
   }

   public Vector3f getHalfExtentsWithoutMargin(Vector3f var1) {
      var1.set(this.implicitShapeDimensions);
      return var1;
   }

   public BroadphaseNativeType getShapeType() {
      return BroadphaseNativeType.BOX_SHAPE_PROXYTYPE;
   }

   public Vector3f localGetSupportingVertex(Vector3f var1, Vector3f var2) {
      Vector3f var3 = this.getHalfExtentsWithoutMargin(var2);
      float var4 = this.getMargin();
      var3.x += var4;
      var3.y += var4;
      var3.z += var4;
      var2.set(ScalarUtil.fsel(var1.x, var3.x, -var3.x), ScalarUtil.fsel(var1.y, var3.y, -var3.y), ScalarUtil.fsel(var1.z, var3.z, -var3.z));
      return var2;
   }

   public Vector3f localGetSupportingVertexWithoutMargin(Vector3f var1, Vector3f var2) {
      Vector3f var3 = this.getHalfExtentsWithoutMargin(var2);
      var2.set(ScalarUtil.fsel(var1.x, var3.x, -var3.x), ScalarUtil.fsel(var1.y, var3.y, -var3.y), ScalarUtil.fsel(var1.z, var3.z, -var3.z));
      return var2;
   }

   public void batchedUnitVectorGetSupportingVertexWithoutMargin(Vector3f[] var1, Vector3f[] var2, int var3) {
      Vector3f var4 = this.getHalfExtentsWithoutMargin(this.he);

      for(int var5 = 0; var5 < var3; ++var5) {
         Vector3f var6 = var1[var5];
         var2[var5].set(ScalarUtil.fsel(var6.x, var4.x, -var4.x), ScalarUtil.fsel(var6.y, var4.y, -var4.y), ScalarUtil.fsel(var6.z, var4.z, -var4.z));
      }

   }

   public void setMargin(float var1) {
      Vector3f var2;
      (var2 = this.he).set(this.getMargin(), this.getMargin(), this.getMargin());
      Vector3f var3;
      (var3 = this.im).add(this.implicitShapeDimensions, var2);
      super.setMargin(var1);
      Vector3f var4;
      (var4 = this.he1).set(this.getMargin(), this.getMargin(), this.getMargin());
      this.implicitShapeDimensions.sub(var3, var4);
   }

   public void setLocalScaling(Vector3f var1) {
      Vector3f var2;
      (var2 = this.he).set(this.getMargin(), this.getMargin(), this.getMargin());
      Vector3f var3;
      (var3 = this.im).add(this.implicitShapeDimensions, var2);
      Vector3f var4;
      VectorUtil.div(var4 = this.he1, var3, this.localScaling);
      super.setLocalScaling(var1);
      VectorUtil.mul(this.implicitShapeDimensions, var4, this.localScaling);
      this.implicitShapeDimensions.sub(var2);
   }

   public void getAabb(Transform var1, Vector3f var2, Vector3f var3) {
      AabbUtil2.transformAabb(this.getHalfExtentsWithoutMargin(this.he), this.getMargin(), var1, var2, var3);
   }

   public void calculateLocalInertia(float var1, Vector3f var2) {
      Vector3f var3 = this.getHalfExtentsWithMargin(this.he);
      float var4 = 2.0F * var3.x;
      float var5 = 2.0F * var3.y;
      float var6 = 2.0F * var3.z;
      var2.set(var1 / 12.0F * (var5 * var5 + var6 * var6), var1 / 12.0F * (var4 * var4 + var6 * var6), var1 / 12.0F * (var4 * var4 + var5 * var5));
   }

   public void getPlane(Vector3f var1, Vector3f var2, int var3) {
      Vector4f var4 = this.pl;
      this.getPlaneEquation(var4, var3);
      var1.set(var4.x, var4.y, var4.z);
      Vector3f var5;
      (var5 = this.he1).negate(var1);
      this.localGetSupportingVertex(var5, var2);
   }

   public int getNumPlanes() {
      return 6;
   }

   public int getNumVertices() {
      return 8;
   }

   public int getNumEdges() {
      return 12;
   }

   public void getVertex(int var1, Vector3f var2) {
      Vector3f var3 = this.getHalfExtentsWithoutMargin(this.he);
      var2.set(var3.x * (float)(1 - (var1 & 1)) - var3.x * (float)(var1 & 1), var3.y * (float)(1 - ((var1 & 2) >> 1)) - var3.y * (float)((var1 & 2) >> 1), var3.z * (float)(1 - ((var1 & 4) >> 2)) - var3.z * (float)((var1 & 4) >> 2));
   }

   public void getPlaneEquation(Vector4f var1, int var2) {
      Vector3f var3 = this.getHalfExtentsWithoutMargin(this.he);
      switch(var2) {
      case 0:
         var1.set(1.0F, 0.0F, 0.0F, -var3.x);
         return;
      case 1:
         var1.set(-1.0F, 0.0F, 0.0F, -var3.x);
         return;
      case 2:
         var1.set(0.0F, 1.0F, 0.0F, -var3.y);
         return;
      case 3:
         var1.set(0.0F, -1.0F, 0.0F, -var3.y);
         return;
      case 4:
         var1.set(0.0F, 0.0F, 1.0F, -var3.z);
         return;
      case 5:
         var1.set(0.0F, 0.0F, -1.0F, -var3.z);
         return;
      default:
         assert false;

      }
   }

   public void getEdge(int var1, Vector3f var2, Vector3f var3) {
      byte var4 = 0;
      byte var5 = 0;
      switch(var1) {
      case 0:
         var4 = 0;
         var5 = 1;
         break;
      case 1:
         var4 = 0;
         var5 = 2;
         break;
      case 2:
         var4 = 1;
         var5 = 3;
         break;
      case 3:
         var4 = 2;
         var5 = 3;
         break;
      case 4:
         var4 = 0;
         var5 = 4;
         break;
      case 5:
         var4 = 1;
         var5 = 5;
         break;
      case 6:
         var4 = 2;
         var5 = 6;
         break;
      case 7:
         var4 = 3;
         var5 = 7;
         break;
      case 8:
         var4 = 4;
         var5 = 5;
         break;
      case 9:
         var4 = 4;
         var5 = 6;
         break;
      case 10:
         var4 = 5;
         var5 = 7;
         break;
      case 11:
         var4 = 6;
         var5 = 7;
         break;
      default:
         assert false;
      }

      this.getVertex(var4, var2);
      this.getVertex(var5, var3);
   }

   public boolean isInside(Vector3f var1, float var2) {
      Vector3f var3 = this.getHalfExtentsWithoutMargin(this.he);
      return var1.x <= var3.x + var2 && var1.x >= -var3.x - var2 && var1.y <= var3.y + var2 && var1.y >= -var3.y - var2 && var1.z <= var3.z + var2 && var1.z >= -var3.z - var2;
   }

   public String getName() {
      return "Box";
   }

   public int getNumPreferredPenetrationDirections() {
      return 6;
   }

   public void getPreferredPenetrationDirection(int var1, Vector3f var2) {
      switch(var1) {
      case 0:
         var2.set(1.0F, 0.0F, 0.0F);
         return;
      case 1:
         var2.set(-1.0F, 0.0F, 0.0F);
         return;
      case 2:
         var2.set(0.0F, 1.0F, 0.0F);
         return;
      case 3:
         var2.set(0.0F, -1.0F, 0.0F);
         return;
      case 4:
         var2.set(0.0F, 0.0F, 1.0F);
         return;
      case 5:
         var2.set(0.0F, 0.0F, -1.0F);
         return;
      default:
         assert false;

      }
   }

   public void setDimFromBB(Vector3f var1, Vector3f var2) {
      this.marginV.set(this.getMargin(), this.getMargin(), this.getMargin());
      this.d.sub(var2, var1);
      VectorUtil.mul(this.implicitShapeDimensions, this.d, this.localScaling);
      this.implicitShapeDimensions.sub(this.marginV);
   }

   public void setHalfSize(Vector3f var1) {
      this.implicitShapeDimensions.set(var1);
   }
}
