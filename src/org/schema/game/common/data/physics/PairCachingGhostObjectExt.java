package org.schema.game.common.data.physics;

import com.bulletphysics.collision.broadphase.BroadphaseProxy;
import com.bulletphysics.collision.broadphase.Dispatcher;
import com.bulletphysics.collision.broadphase.HashedOverlappingPairCache;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.PairCachingGhostObject;
import com.bulletphysics.collision.dispatch.CollisionWorld.ConvexResultCallback;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectArrayList;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.network.objects.container.PhysicsDataContainer;

public class PairCachingGhostObjectExt extends PairCachingGhostObject implements CollisionObjectInterface {
   private final Vector3f axis = new Vector3f();
   private final Matrix3f dmat = new Matrix3f();
   private final Quat4f dorn = new Quat4f();
   HashedOverlappingPairCache hashPairCache = new HashedOverlappingPairCacheExt();
   Vector3f castShapeAabbMin = new Vector3f();
   Vector3f castShapeAabbMax = new Vector3f();
   Transform convexFromTrans = new Transform();
   Transform convexToTrans = new Transform();
   Vector3f linVel = new Vector3f();
   Vector3f angVel = new Vector3f();
   Transform tmpTrans = new Transform();
   Vector3f collisionObjectAabbMin = new Vector3f();
   Vector3f collisionObjectAabbMax = new Vector3f();
   Vector3f hitNormal = new Vector3f();
   Transform R = new Transform();
   Quat4f quat = new Quat4f();
   private PhysicsDataContainer pCon;
   private SegmentController attached;
   private Matrix3f tmp = new Matrix3f();
   private final CollisionType type;

   public PairCachingGhostObjectExt(CollisionType var1, PhysicsDataContainer var2) {
      this.type = var1;
      this.pCon = var2;
   }

   private void setupSweepTest(Transform var1, Transform var2, ConvexShape var3) {
      this.convexFromTrans.set(var1);
      this.convexToTrans.set(var2);
      this.quat.x = 0.0F;
      this.quat.y = 0.0F;
      this.quat.x = 0.0F;
      this.quat.w = 0.0F;

      assert var1.getMatrix(new Matrix4f()).determinant() != 0.0F : var1.getMatrix(new Matrix4f());

      assert var2.getMatrix(new Matrix4f()).determinant() != 0.0F : var2.getMatrix(new Matrix4f());

      TransformTools.calculateVelocity(this.convexFromTrans, this.convexToTrans, 1.0F, this.linVel, this.angVel, this.axis, this.tmp, this.dmat, this.dorn);
      this.R.setIdentity();
      this.R.setRotation(this.convexFromTrans.getRotation(this.quat));
      var3.calculateTemporalAabb(this.R, this.linVel, this.angVel, 1.0F, this.castShapeAabbMin, this.castShapeAabbMax);
   }

   public void convexSweepTest(ConvexShape var1, Transform var2, Transform var3, ConvexResultCallback var4, float var5) {
      this.setupSweepTest(var2, var3, var1);
      long var6 = System.currentTimeMillis();
      ObjectArrayList var8 = this.overlappingObjects;
      boolean var9 = false;
      CollisionObject var10 = null;
      if (var4 instanceof ClosestConvexResultCallbackExt) {
         ClosestConvexResultCallbackExt var11;
         var10 = (var11 = (ClosestConvexResultCallbackExt)var4).ownerObject;
         if (var11.completeCheck) {
            var8 = var11.dynamicsWorld.getCollisionObjectArray();
            var9 = true;
         }
      }

      int var13;
      for(var13 = 0; var13 < var8.size(); ++var13) {
         CollisionObject var12 = (CollisionObject)var8.getQuick(var13);
         this.testObject(var12, var4, var2, var3, var1, var5, var9, var10);
      }

      if ((var13 = (int)(System.currentTimeMillis() - var6)) > 21) {
         System.err.println("[GHOST-OBJECT] SWEEP TEST TIME: " + var13);
      }

   }

   private void testObject(CollisionObject var1, ConvexResultCallback var2, Transform var3, Transform var4, ConvexShape var5, float var6, boolean var7, CollisionObject var8) {
      if (var1 != var8 && var1 != this.pCon.getObject() && (var8 == null || !(var8 instanceof RigidBodySegmentController) || !((RigidBodySegmentController)var8).isRelatedTo(var1))) {
         if (var2.needsCollision(var1.getBroadphaseHandle())) {
            var1.getCollisionShape().getAabb(var1.getWorldTransform(this.tmpTrans), this.collisionObjectAabbMin, this.collisionObjectAabbMax);
            AabbUtil2.aabbExpand(this.collisionObjectAabbMin, this.collisionObjectAabbMax, this.castShapeAabbMin, this.castShapeAabbMax);
            float[] var9 = new float[]{1.0F};
            this.hitNormal.set(0.0F, 0.0F, 0.0F);
            if (AabbUtil2.rayAabb(var3.origin, var4.origin, this.collisionObjectAabbMin, this.collisionObjectAabbMax, var9, this.hitNormal)) {
               ModifiedDynamicsWorld.objectQuerySingle(var5, this.convexFromTrans, this.convexToTrans, var1, var1.getCollisionShape(), var1.getWorldTransform(this.tmpTrans), var2, var6);
               if (var7) {
                  ((ClosestConvexResultCallbackExt)var2).overlapping.add(var1);
               }
            }
         }

      }
   }

   public SegmentController getAttached() {
      return this.attached;
   }

   public void setAttached(SegmentController var1) {
      this.attached = var1;
   }

   public void setWorldTransform(Transform var1) {
      this.worldTransform.set(var1);
   }

   public String toString() {
      return "PCGhostObjExt(" + this.getUserPointer() + ")@" + this.hashCode();
   }

   public void addOverlappingObjectInternal(BroadphaseProxy var1, BroadphaseProxy var2) {
      var2 = var2 != null ? var2 : this.getBroadphaseHandle();

      assert var2 != null;

      CollisionObject var3 = (CollisionObject)var1.clientObject;

      assert var3 != null;

      if (this.overlappingObjects.indexOf(var3) == -1) {
         this.overlappingObjects.add(var3);
         this.hashPairCache.addOverlappingPair(var2, var1);
      }

   }

   public void removeOverlappingObjectInternal(BroadphaseProxy var1, Dispatcher var2, BroadphaseProxy var3) {
      CollisionObject var4 = (CollisionObject)var1.clientObject;
      var3 = var3 != null ? var3 : this.getBroadphaseHandle();

      assert var3 != null;

      assert var4 != null;

      int var5;
      if ((var5 = this.overlappingObjects.indexOf(var4)) != -1) {
         this.overlappingObjects.setQuick(var5, this.overlappingObjects.getQuick(this.overlappingObjects.size() - 1));
         this.overlappingObjects.removeQuick(this.overlappingObjects.size() - 1);
         this.hashPairCache.removeOverlappingPair(var3, var1, var2);
      }

   }

   public HashedOverlappingPairCache getOverlappingPairCache() {
      return this.hashPairCache;
   }

   public CollisionType getType() {
      return this.type;
   }
}
