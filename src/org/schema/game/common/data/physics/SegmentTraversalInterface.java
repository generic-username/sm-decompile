package org.schema.game.common.data.physics;

public interface SegmentTraversalInterface {
   boolean handle(int var1, int var2, int var3, RayTraceGridTraverser var4);

   Object getContextObj();
}
