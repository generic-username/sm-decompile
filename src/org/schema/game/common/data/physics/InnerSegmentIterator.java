package org.schema.game.common.data.physics;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.CollisionWorld.LocalRayResult;
import com.bulletphysics.collision.dispatch.CollisionWorld.LocalShapeInfo;
import com.bulletphysics.collision.narrowphase.ConvexCast.CastResult;
import com.bulletphysics.collision.shapes.CompoundShape;
import com.bulletphysics.collision.shapes.CompoundShapeChild;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Matrix4fTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.graphicsengine.forms.debug.DebugPoint;
import org.schema.schine.physics.ClosestRayCastResultExt;

public class InnerSegmentIterator implements SegmentTraversalInterface {
   public ClosestRayCastResultExt rayResult;
   public CubeRayVariableSet v;
   public boolean debug;
   public int tests;
   public Segment currentSeg;
   public SegmentController segmentController;
   public Transform fromA;
   public Transform toA;
   public Transform testCubes;
   public CastResult result;
   public boolean hitSignal;
   protected CollisionObject collisionObject;
   private short lastUpdateNumDrawPP;

   protected boolean isZeroHpPhysical(SegmentData var1, int var2) {
      return this.rayResult.isZeroHpPhysical() || var1.getHitpointsByte(var2) > 0 || var1.getType(var2) != 1;
   }

   public boolean handle(int var1, int var2, int var3, RayTraceGridTraverser var4) {
      this.getContextObj();
      var1 = var1 - this.currentSeg.pos.x + 16;
      var2 = var2 - this.currentSeg.pos.y + 16;
      var3 = var3 - this.currentSeg.pos.z + 16;
      ++this.tests;
      SegmentData var17 = this.currentSeg.getSegmentData();
      if (var1 >= 0 && var1 < 32 && var2 >= 0 && var2 < 32 && var3 >= 0 && var3 < 32) {
         int var5 = SegmentData.getInfoIndex((byte)var1, (byte)var2, (byte)var3);
         short var6 = var17.getType(var5);
         if (this.debug) {
            System.err.println("[INNER] CHECKING " + var1 + "; " + var2 + "; " + var3 + ": TYPE " + var6 + "; Abs " + (this.currentSeg.pos.x + var1) + ", " + (this.currentSeg.pos.y + var2) + ", " + (this.currentSeg.pos.z + var3));
         }

         if (var6 > 0 && !this.rayResult.isIgnoreBlockType(var6) && (this.rayResult.isIgnoereNotPhysical() || ElementInformation.isPhysicalRayTests(var6, var17, var5) && this.isZeroHpPhysical(var17, var5))) {
            this.v.elemA.set((byte)var1, (byte)var2, (byte)var3);
            this.v.elemPosA.set((float)(this.v.elemA.x - 16), (float)(this.v.elemA.y - 16), (float)(this.v.elemA.z - 16));
            Vector3f var10000 = this.v.elemPosA;
            var10000.x += (float)this.currentSeg.pos.x;
            var10000 = this.v.elemPosA;
            var10000.y += (float)this.currentSeg.pos.y;
            var10000 = this.v.elemPosA;
            var10000.z += (float)this.currentSeg.pos.z;
            this.v.nA.set(this.v.elemPosA);
            this.v.tmpTrans3.set(this.testCubes);
            this.v.tmpTrans3.basis.transform(this.v.nA);
            this.v.tmpTrans3.origin.add(this.v.nA);
            ElementInformation var10 = ElementKeyMap.getInfoFast(var6);
            byte var13;
            byte var15 = var13 = this.currentSeg.getSegmentData().getOrientation(var5);
            if (var6 == 689) {
               var15 = 2;
               if (var13 == 4 && (this.currentSeg.getSegmentController().isOnServer() || !((GameClientState)this.currentSeg.getSegmentController().getState()).isInAnyStructureBuildMode())) {
                  return true;
               }

               var13 = 0;
            }

            float var7;
            if (this.rayResult.isSimpleRayTest() && !var10.blockStyle.solidBlockStyle && var10.getSlab(var13) == 0 && (var7 = Vector3fTools.length(this.fromA.origin, this.v.tmpTrans3.origin) / Vector3fTools.length(this.fromA.origin, this.toA.origin)) < this.rayResult.closestHitFraction) {
               if (this.rayResult.isHasCollidingBlockFilter()) {
                  assert var17 != null;

                  assert this.rayResult.getCollidingBlocks() != null;

                  if (((LongOpenHashSet)this.rayResult.getCollidingBlocks().get(this.v.cubesB.getSegmentBuffer().getSegmentController().getId())).contains(var17.getSegment().getAbsoluteIndex(this.v.elemA.x, this.v.elemA.y, this.v.elemA.z))) {
                     return true;
                  }
               }

               this.rayResult.closestHitFraction = var7;
               this.rayResult.setSegment(var17.getSegment());
               this.rayResult.getCubePos().set(this.v.elemA);
               this.rayResult.hitPointWorld.set(this.v.tmpTrans3.origin);
               this.rayResult.hitNormalWorld.sub(this.fromA.origin, this.toA.origin);
               FastMath.normalizeCarmack(this.rayResult.hitNormalWorld);
               this.rayResult.collisionObject = this.collisionObject;
               if (this.rayResult.isRecordAllBlocks()) {
                  this.v.record.blockAbsIndices.add(var17.getSegment().getAbsoluteIndex(this.v.elemA.x, this.v.elemA.y, this.v.elemA.z));
                  this.v.record.datas.add(var17);
                  this.v.record.blockLocalIndices.add(var5);
               }

               if (this.rayResult.isRecordAllBlocks() && this.v.record.size() < this.v.recordAmount) {
                  return true;
               }

               if (this.debug) {
                  System.err.println("COTR:: isRecordALl: " + this.rayResult.isRecordAllBlocks());
                  if (this.v.record != null) {
                     System.err.println("COTR REC:: isRecordALl: " + this.v.record.size() + "; " + this.v.recordAmount);
                  }
               }

               this.hitSignal = true;
               return false;
            }

            BlockStyle var20 = var10.getBlockStyle();
            this.currentSeg.getSegmentData().isActive(var5);
            this.v.simplexSolver.reset();
            this.v.box0.setMargin(0.01F);
            Object var8 = this.v.box0;
            if (var10.lodUseDetailCollision) {
               var8 = var10.lodDetailCollision.getShape(var6, (byte)var15, this.v.lodBlockTransform);
            } else if (var10.blockStyle.solidBlockStyle) {
               var8 = BlockShapeAlgorithm.getShape(var20, (byte)var15);
            }

            Transform var19 = this.v.tmpTrans3;
            if (var10.getSlab(var13) > 0) {
               (var19 = this.v.BT).set(this.v.tmpTrans3);
               this.v.orientTT.set(Element.DIRECTIONSf[Element.switchLeftRight(var15 % 6)]);
               var19.basis.transform(this.v.orientTT);
               switch(var10.getSlab(var13)) {
               case 1:
                  this.v.orientTT.scale(0.125F);
                  var8 = this.v.box34[var15 % 6];
                  break;
               case 2:
                  this.v.orientTT.scale(0.25F);
                  var8 = this.v.box12[var15 % 6];
                  break;
               case 3:
                  this.v.orientTT.scale(0.375F);
                  var8 = this.v.box14[var15 % 6];
               }

               var19.origin.sub(this.v.orientTT);
            }

            boolean var16 = false;
            if (var8 == null) {
               System.err.println("[PHYSICS][ERROR] InnerSegmentIterator: Shape null: UseDetail: " + var10.lodUseDetailCollision + "; Type: " + var10.lodDetailCollision.type.name() + "; Block: " + var10.name + " (" + var10.id + ")");
               var8 = this.v.box0;
            }

            if (var8 instanceof CompoundShape) {
               CompoundShape var11 = (CompoundShape)var8;

               for(var3 = 0; var3 < var11.getNumChildShapes() && !var16; ++var3) {
                  CompoundShapeChild var18 = (CompoundShapeChild)var11.getChildList().get(var3);
                  Matrix4fTools.transformMul(this.v.lodBlockTransform, var18.transform);
                  this.v.boxETransform.set(var19);
                  Matrix4fTools.transformMul(this.v.boxETransform, this.v.lodBlockTransform);
                  ConvexShape var21;
                  (var21 = (ConvexShape)var18.childShape).setMargin(0.0F);
                  var16 = (new ContinuousConvexCollision(this.v.shapeA, var21, this.v.simplexSolver, this.v.gjkEpaPenetrationDepthSolver)).calcTimeOfImpact(this.fromA, this.toA, this.v.boxETransform, this.v.boxETransform, this.result, this.v.gjkVar);
                  if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn() && !this.segmentController.isOnServer() && ((GameClientState)this.segmentController.getState()).getNumberOfUpdate() > this.lastUpdateNumDrawPP + 10) {
                     ConvexHullShapeExt var22 = (ConvexHullShapeExt)var21;
                     Vector4f var25 = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
                     if (var16) {
                        var25.set(1.0F, 0.0F, 0.0F, 1.0F);
                        Iterator var23 = var22.getPoints().iterator();

                        while(var23.hasNext()) {
                           Vector3f var9 = (Vector3f)var23.next();
                           var9 = new Vector3f(var9);
                           this.v.boxETransform.transform(var9);
                           DebugPoint var24;
                           (var24 = new DebugPoint(var9, var25, 0.1F)).LIFETIME = 1000L;
                           DebugDrawer.points.add(var24);
                        }
                     } else if (var3 == 0) {
                        var25.set(0.3F, 0.7F, 0.8F, 1.0F);
                     } else if (var3 == 1) {
                        var25.set(0.0F, 1.0F, 0.0F, 1.0F);
                     } else if (var3 == 2) {
                        var25.set(0.0F, 0.0F, 1.0F, 1.0F);
                     } else if (var3 == 3) {
                        var25.set(1.0F, 0.0F, 1.0F, 1.0F);
                     } else if (var3 == 4) {
                        var25.set(0.0F, 1.0F, 1.0F, 1.0F);
                     } else if (var3 == 6) {
                        var25.set(1.0F, 1.0F, 0.0F, 1.0F);
                     }
                  }
               }
            } else {
               if (this.v.shapeA == null || var8 == null) {
                  throw new NullPointerException("Physics shape null: " + this.v.shapeA + "; " + var8);
               }

               var16 = (new ContinuousConvexCollision(this.v.shapeA, (ConvexShape)var8, this.v.simplexSolver, this.v.gjkEpaPenetrationDepthSolver)).calcTimeOfImpact(this.fromA, this.toA, var19, var19, this.result, this.v.gjkVar);
            }

            if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn() && !this.segmentController.isOnServer() && ((GameClientState)this.segmentController.getState()).getNumberOfUpdate() > this.lastUpdateNumDrawPP + 10) {
               this.lastUpdateNumDrawPP = ((GameClientState)this.segmentController.getState()).getNumberOfUpdate();
            }

            if (var16) {
               if (this.rayResult.isHasCollidingBlockFilter()) {
                  assert var17 != null;

                  assert this.rayResult.getCollidingBlocks() != null;

                  LongOpenHashSet var12 = (LongOpenHashSet)this.rayResult.getCollidingBlocks().get(this.v.cubesB.getSegmentBuffer().getSegmentController().getId());

                  assert var12 != null;

                  assert var17.getSegment() != null;

                  assert this.v.elemA != null;

                  if (var12.contains(var17.getSegment().getAbsoluteIndex(this.v.elemA.x, this.v.elemA.y, this.v.elemA.z))) {
                     return true;
                  }
               }

               if (this.result.normal.lengthSquared() > 1.0E-6F) {
                  if (this.rayResult.isRecordAllBlocks()) {
                     this.v.record.blockAbsIndices.add(var17.getSegment().getAbsoluteIndex(this.v.elemA.x, this.v.elemA.y, this.v.elemA.z));
                     this.v.record.datas.add(var17);
                     this.v.record.blockLocalIndices.add(var5);
                  }

                  if (this.result.fraction < this.rayResult.closestHitFraction) {
                     assert var17.getSegment() != null : "SEGMENT NULL OF DATA: " + var17 + " ";

                     this.rayResult.setSegment(var17.getSegment());
                     this.rayResult.getCubePos().set(this.v.elemA);
                     this.fromA.basis.transform(this.result.normal);
                     this.result.normal.normalize();
                     LocalRayResult var14 = new LocalRayResult(this.collisionObject, (LocalShapeInfo)null, this.result.normal, this.result.fraction);
                     this.rayResult.addSingleResult(var14, true);

                     assert !this.rayResult.hasHit() || this.rayResult.getSegment() != null;

                     if (!this.rayResult.isRecordAllBlocks() || this.v.record.size() >= this.v.recordAmount) {
                        this.hitSignal = true;
                        return false;
                     }
                  }
               }
            }
         }
      }

      return true;
   }

   public SegmentController getContextObj() {
      return this.segmentController;
   }

   public boolean onOuterSegmentHitTest(Segment var1, boolean var2) {
      return true;
   }
}
