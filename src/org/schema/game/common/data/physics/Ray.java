package org.schema.game.common.data.physics;

import javax.vecmath.Vector3f;

public class Ray {
   public final Vector3f direction = new Vector3f();
   public final Vector3f position = new Vector3f();
}
