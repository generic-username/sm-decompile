package org.schema.game.common.data.physics;

import com.bulletphysics.collision.dispatch.CollisionObject;

public interface NonBlockHitCallback {
   boolean onHit(CollisionObject var1, float var2);
}
