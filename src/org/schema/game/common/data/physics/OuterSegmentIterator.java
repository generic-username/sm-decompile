package org.schema.game.common.data.physics;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.narrowphase.ConvexCast.CastResult;
import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.Segment;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.physics.ClosestRayCastResultExt;

public class OuterSegmentIterator implements SegmentTraversalInterface {
   SegmentController segmentController;
   Transform fromA;
   Transform toA;
   Transform testCubes;
   Transform toCubes;
   CastResult result;
   boolean hitSignal;
   public boolean hit;
   com.bulletphysics.util.ObjectPool pool4 = com.bulletphysics.util.ObjectPool.get(Vector4i.class);
   public boolean debug;
   private CubeRayVariableSet v;
   public ClosestRayCastResultExt rayResult;

   public OuterSegmentIterator(CubeRayVariableSet var1) {
      this.v = var1;
   }

   public boolean handle(Segment var1) {
      this.checkSegment(var1, this.fromA, this.toA, this.testCubes, this.toCubes, this.result);
      if (this.hit) {
         if (this.debug) {
            System.err.println("[OUTER] not continuing shot since it was marked as hit");
         }

         this.hitSignal = true;
         return false;
      } else {
         return true;
      }
   }

   public SegmentController getContextObj() {
      return this.segmentController;
   }

   public boolean handle(int var1, int var2, int var3, RayTraceGridTraverser var4) {
      SegmentController var6 = this.getContextObj();
      var1 <<= 5;
      var2 <<= 5;
      var3 <<= 5;
      if (!var6.isInboundAbs(var1, var2, var3)) {
         return true;
      } else {
         Segment var5;
         return var6.getSegmentBuffer().getSegmentState(var1, var2, var3) >= 0 && (var5 = var6.getSegmentBuffer().get(var1, var2, var3)) != null ? this.handle(var5) : true;
      }
   }

   private void checkSegment(Segment var1, Transform var2, Transform var3, Transform var4, Transform var5, CastResult var6) {
      if (var1.getSegmentData() != null && !var1.isEmpty()) {
         this.v.cubesB.getSegmentAabb(var1, var4, this.v.outMin, this.v.outMax, this.v.localMinOut, this.v.localMaxOut, this.v.aabbVarSet);
         this.v.hitLambda[0] = 1.0F;
         this.v.normal.set(0.0F, 0.0F, 0.0F);
         boolean var11 = AabbUtil2.rayAabb(var2.origin, var3.origin, this.v.outMin, this.v.outMax, this.v.hitLambda, this.v.normal) || BoundingBox.testPointAABB(var2.origin, this.v.outMin, this.v.outMax) || BoundingBox.testPointAABB(var3.origin, this.v.outMin, this.v.outMax);
         boolean var7 = false;
         if (var11) {
            var7 = this.performCastTestTrav(this.v.cubesCollisionObject, var1, var2, var3, var4, var6);
            int var8 = this.v.takenPoints.size();

            for(int var10 = 0; var10 < var8; ++var10) {
               this.pool4.release(this.v.takenPoints.get(var10));
            }

            this.v.sorted.clear();
            this.v.takenPoints.clear();
            this.hit = this.hit || var7;
         }

         boolean var9 = ((InnerSegmentIterator)this.rayResult.innerSegmentIterator).onOuterSegmentHitTest(var1, ((InnerSegmentIterator)this.rayResult.innerSegmentIterator).hitSignal);
         if (this.debug) {
            System.err.println("[OUTER] ON SEGEMNT " + var1.pos + ": hitBox: " + var11 + "; continue: " + var9 + "; cast: " + var7 + "; hit " + this.hit);
         }

         if (!var9) {
            this.hit = true;
         }

      }
   }

   private boolean performCastTestTrav(CollisionObject var1, Segment var2, Transform var3, Transform var4, Transform var5, CastResult var6) {
      this.v.segTrans.set(var5);
      this.v.segPos.set((float)(var2.pos.x - 16), (float)(var2.pos.y - 16), (float)(var2.pos.z - 16));
      this.v.segAABB.set(this.v.outMin, this.v.outMax);
      Vector3f var10000 = this.v.segAABB.min;
      var10000.x -= 16.0F;
      var10000 = this.v.segAABB.min;
      var10000.y -= 16.0F;
      var10000 = this.v.segAABB.min;
      var10000.z -= 16.0F;
      var10000 = this.v.segAABB.max;
      var10000.x += 16.0F;
      var10000 = this.v.segAABB.max;
      var10000.y += 16.0F;
      var10000 = this.v.segAABB.max;
      var10000.z += 16.0F;
      if (this.debug) {
         System.err.println("BB " + this.v.segAABB);
      }

      this.v.rayModFrom.set(var3.origin);
      this.v.rayModTo.set(var4.origin);
      boolean var7 = this.v.segAABB.isInside(var3.origin);
      boolean var8 = this.v.segAABB.isInside(var4.origin);
      if (!var7 || !var8) {
         if (!var8) {
            this.v.dirTmp.sub(var3.origin, var4.origin);
            this.v.dirTmp.normalize();
            if (!this.v.segAABB.getIntersection(var4.origin, this.v.dirTmp, this.v.outInt)) {
               return false;
            }

            this.v.rayModTo.set(this.v.outInt);
         }

         if (!var7) {
            this.v.dirTmp.sub(var4.origin, var3.origin);
            this.v.dirTmp.normalize();
            if (!this.v.segAABB.getIntersection(var3.origin, this.v.dirTmp, this.v.outInt)) {
               return false;
            }

            this.v.rayModFrom.set(this.v.outInt);
         }
      }

      InnerSegmentIterator var9;
      (var9 = (InnerSegmentIterator)this.rayResult.innerSegmentIterator).tests = 0;
      var9.currentSeg = var2;
      var9.collisionObject = var1;
      var9.fromA = var3;
      var9.toA = var4;
      var9.hitSignal = false;
      var9.result = var6;
      var9.testCubes = var5;
      var9.debug = this.debug;
      var9.v = this.v;
      var9.rayResult = this.rayResult;
      this.v.solveBlock.initializeBlockGranularity(this.v.rayModFrom, this.v.rayModTo, this.v.segTrans);
      if (!this.v.solve.ok) {
         System.err.println("[PHYSICS] CastTestTrav " + this.v.cubesB.getSegmentBuffer().getSegmentController().getState() + " " + var3.origin + " -> " + var4.origin + " for chubes: " + var5.origin + ": " + this.v.cubesB.getSegmentBuffer().getSegmentController());
      }

      var9.segmentController = this.v.cubesB.getSegmentBuffer().getSegmentController();
      this.v.solveBlock.traverseSegmentsOnRay(var9);
      var9.segmentController = null;
      if (this.debug) {
         System.err.println("[OUTER] AFTER INNER TEST. HIS SIGNAL: " + var9.hitSignal + "; InnerHit: " + var9.rayResult.hasHit() + "; InnerHitSeg: " + var9.rayResult.getSegment());
      }

      return var9.hitSignal;
   }
}
