package org.schema.game.common.data.physics;

import com.bulletphysics.collision.dispatch.CollisionObject;
import org.schema.schine.network.objects.container.PhysicsDataContainer;

public class PairCachingGhostObjectUncollidable extends PairCachingGhostObjectExt {
   public PairCachingGhostObjectUncollidable(CollisionType var1, PhysicsDataContainer var2) {
      super(var1, var2);
   }

   public boolean checkCollideWith(CollisionObject var1) {
      return !(var1 instanceof PairCachingGhostObjectUncollidable) && !(var1 instanceof RigidDebrisBody);
   }

   public String toString() {
      return "PCGhostObjUCMissile(" + this.getUserPointer() + ")";
   }
}
