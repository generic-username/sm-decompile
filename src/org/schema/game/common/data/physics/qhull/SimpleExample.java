package org.schema.game.common.data.physics.qhull;

public class SimpleExample {
   public static void main(String[] var0) {
      DPoint3d[] var4 = new DPoint3d[]{new DPoint3d(0.0D, 0.0D, 0.0D), new DPoint3d(1.0D, 0.5D, 0.0D), new DPoint3d(2.0D, 0.0D, 0.0D), new DPoint3d(0.5D, 0.5D, 0.5D), new DPoint3d(0.0D, 0.0D, 2.0D), new DPoint3d(0.1D, 0.2D, 0.3D), new DPoint3d(0.0D, 2.0D, 0.0D)};
      QuickHull3D var1;
      (var1 = new QuickHull3D()).build(var4);
      System.out.println("Vertices:");
      var4 = var1.getVertices();

      for(int var2 = 0; var2 < var4.length; ++var2) {
         DPoint3d var3 = var4[var2];
         System.out.println(var3.x + " " + var3.y + " " + var3.z);
      }

      System.out.println("Faces:");
      int[][] var6 = var1.getFaces();

      for(int var7 = 0; var7 < var4.length; ++var7) {
         for(int var5 = 0; var5 < var6[var7].length; ++var5) {
            System.out.print(var6[var7][var5] + " ");
         }

         System.out.println("");
      }

   }
}
