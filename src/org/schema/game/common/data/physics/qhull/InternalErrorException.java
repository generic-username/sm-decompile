package org.schema.game.common.data.physics.qhull;

public class InternalErrorException extends RuntimeException {
   private static final long serialVersionUID = 1L;

   public InternalErrorException(String var1) {
      super(var1);
   }
}
