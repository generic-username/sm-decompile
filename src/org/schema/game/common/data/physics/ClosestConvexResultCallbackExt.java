package org.schema.game.common.data.physics;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.CollisionWorld.ClosestConvexResultCallback;
import com.bulletphysics.util.ObjectArrayList;
import java.util.List;
import javax.vecmath.Vector3f;

public class ClosestConvexResultCallbackExt extends ClosestConvexResultCallback {
   public Object userData;
   public boolean checkHasHitOnly;
   public boolean completeCheck;
   public ModifiedDynamicsWorld dynamicsWorld;
   public ObjectArrayList overlapping = new ObjectArrayList();
   public CollisionObject ownerObject;
   public List sphereOverlapping;
   public List sphereOverlappingNormals;
   public boolean sphereDontHitOwner;

   public ClosestConvexResultCallbackExt(Vector3f var1, Vector3f var2) {
      super(var1, var2);
   }
}
