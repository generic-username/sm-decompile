package org.schema.game.common.data.physics;

import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.world.SegmentData;

public class SortEntry implements Comparable {
   byte x;
   byte y;
   byte z;
   BlockStyle blockStyle;
   byte orientation;
   float length;
   boolean active;
   byte slab;
   SegmentData segmentData;
   public ElementInformation info;

   public int compareTo(SortEntry var1) {
      return Float.compare(this.length, var1.length);
   }
}
