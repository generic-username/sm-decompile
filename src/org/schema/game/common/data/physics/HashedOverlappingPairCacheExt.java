package org.schema.game.common.data.physics;

import com.bulletphysics.BulletStats;
import com.bulletphysics.collision.broadphase.BroadphasePair;
import com.bulletphysics.collision.broadphase.BroadphaseProxy;
import com.bulletphysics.collision.broadphase.Dispatcher;
import com.bulletphysics.collision.broadphase.HashedOverlappingPairCache;
import com.bulletphysics.collision.broadphase.OverlapCallback;
import com.bulletphysics.collision.dispatch.CollisionObject;
import org.schema.game.common.controller.elements.power.reactor.StabilizerPath;
import org.schema.game.common.data.world.SectorNotFoundRuntimeException;

public class HashedOverlappingPairCacheExt extends HashedOverlappingPairCache {
   public boolean needsBroadphaseCollision(BroadphaseProxy var1, BroadphaseProxy var2) {
      if (var1.clientObject instanceof PairCachingGhostObjectUncollidable && var2.clientObject instanceof PairCachingGhostObjectUncollidable) {
         return false;
      } else if (var1.clientObject instanceof RigidDebrisBody && ((RigidDebrisBody)var1.clientObject).noCollision || var2.clientObject instanceof RigidDebrisBody && ((RigidDebrisBody)var2.clientObject).noCollision) {
         return false;
      } else if (var1.clientObject instanceof RigidDebrisBody && var2.clientObject instanceof PairCachingGhostObjectUncollidable || var2.clientObject instanceof RigidDebrisBody && var1.clientObject instanceof PairCachingGhostObjectUncollidable) {
         return false;
      } else if ((!(var1.clientObject instanceof CollisionObject) || !(((CollisionObject)var2.clientObject).getUserPointer() instanceof StabilizerPath)) && (!(var2.clientObject instanceof CollisionObject) || !(((CollisionObject)var1.clientObject).getUserPointer() instanceof StabilizerPath))) {
         if (var1.clientObject instanceof RigidDebrisBody && var2.clientObject instanceof PairCachingGhostObjectExt || var2.clientObject instanceof RigidDebrisBody && var1.clientObject instanceof PairCachingGhostObjectExt) {
            return false;
         } else {
            if (var1.clientObject instanceof RigidBodySegmentController && var2.clientObject instanceof RigidBodySegmentController) {
               RigidBodySegmentController var3 = (RigidBodySegmentController)var1.clientObject;
               RigidBodySegmentController var4 = (RigidBodySegmentController)var2.clientObject;
               if (var3.getSegmentController().isIgnorePhysics() || var4.getSegmentController().isIgnorePhysics()) {
                  return false;
               }

               if (var3.virtualSec != null && var4.virtualSec != null) {
                  return false;
               }

               if (var3.virtualSec != null && var4.virtualSec == null) {
                  if (var3.getCollisionShape() != var4.getCollisionShape()) {
                     return true;
                  }

                  return false;
               }

               if (var3.virtualSec == null && var4.virtualSec != null) {
                  if (var3.getCollisionShape() != var4.getCollisionShape()) {
                     return true;
                  }

                  return false;
               }
            }

            if (this.getOverlapFilterCallback() != null) {
               return this.getOverlapFilterCallback().needBroadphaseCollision(var1, var2);
            } else {
               return (var1.collisionFilterGroup & var2.collisionFilterMask) != 0 && (var2.collisionFilterGroup & var1.collisionFilterMask) != 0;
            }
         }
      } else {
         return false;
      }
   }

   public void processAllOverlappingPairs(OverlapCallback var1, Dispatcher var2) {
      int var3 = 0;

      while(var3 < this.getOverlappingPairArray().size()) {
         BroadphasePair var4 = (BroadphasePair)this.getOverlappingPairArray().getQuick(var3);

         try {
            if (var1.processOverlap(var4)) {
               this.removeOverlappingPair(var4.pProxy0, var4.pProxy1, var2);
               --BulletStats.gOverlappingPairs;
            } else {
               ++var3;
            }
         } catch (SectorNotFoundRuntimeException var5) {
            var5.printStackTrace();
            System.err.println("[SERVER][RECOVER] SECTOR NOT FOUND EXCEPTION CATCHED SUCCESSFULLY: Pair: " + var4.pProxy0.clientObject + ", " + var4.pProxy1.clientObject + "; Removing pair to recover");
            this.removeOverlappingPair(var4.pProxy0, var4.pProxy1, var2);
            --BulletStats.gOverlappingPairs;
         }
      }

   }
}
