package org.schema.game.common.data.physics;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.world.Segment;
import org.schema.schine.physics.ClosestRayCastResultExt;

public class CubeRayCastResult extends ClosestRayCastResultExt {
   private boolean recordAllBlocks;
   private Int2ObjectOpenHashMap recordedBlocks;
   private Int2ObjectOpenHashMap collidingBlocks;
   private boolean hasCollidingBlockFilter = false;
   private boolean ignoereNotPhysical = false;
   private boolean onlyCubeMeshes;
   private boolean debug;
   private boolean ignoreDebris;
   private int blockDeepness;
   private boolean zeroHpPhysical = true;
   private SegmentController[] filter;
   private Segment segment;
   private boolean damageTest;
   private boolean simpleRayTest;
   private boolean cubesOnly;
   private NonBlockHitCallback nonBlockHitCallback;
   private boolean checkStabilizerPath = true;
   public float power;
   public boolean filterModeSingleNot;
   private boolean recordArmor;

   public SegmentTraversalInterface newInnerSegmentIterator() {
      return new InnerSegmentIterator();
   }

   public CubeRayCastResult(Vector3f var1, Vector3f var2, Object var3, SegmentController... var4) {
      super(var1, var2);
      this.setOwner(var3);
      this.filter = var4;
   }

   public Vector3i getNextToAbsolutePosition() {
      assert this.getSegment() != null && this.hasHit();

      Vector3f var1 = new Vector3f(this.hitPointWorld);
      this.getSegment().getSegmentController().getWorldTransformInverse().transform(var1);
      Vector3i var2;
      Vector3i var10000 = var2 = new Vector3i(this.getSegment().pos.x, this.getSegment().pos.y, this.getSegment().pos.z);
      var10000.x += this.getCubePos().x - 16;
      var2.y += this.getCubePos().y - 16;
      var2.z += this.getCubePos().z - 16;
      IntOpenHashSet var3 = new IntOpenHashSet();

      for(int var4 = 0; var4 < 6; ++var4) {
         Vector3i var5 = Element.DIRECTIONSi[var4];
         SegmentPiece var7;
         if ((var7 = this.getSegment().getSegmentController().getSegmentBuffer().getPointUnsave(new Vector3i(var2.x + var5.x, var2.y + var5.y, var2.z + var5.z))) != null && var7.getType() != 0) {
            var3.add(var4);
         }
      }

      SegmentPiece var6 = this.getSegment().getSegmentController().getSegmentBuffer().getPointUnsave(new Vector3i(var2.x, var2.y, var2.z));
      int var8 = Element.getSide(var1, var6 == null ? null : var6.getAlgorithm(), var2, var6 != null ? var6.getType() : 0, var6 != null ? var6.getOrientation() : 0, var3);
      System.err.println("[GETNEXTTONEAREST] SIDE: " + Element.getSideString(var8) + ": " + var1 + "; " + var2);
      switch(var8) {
      case 0:
         var2.z = (int)((float)var2.z + 1.0F);
         break;
      case 1:
         var2.z = (int)((float)var2.z - 1.0F);
         break;
      case 2:
         var2.y = (int)((float)var2.y + 1.0F);
         break;
      case 3:
         var2.y = (int)((float)var2.y - 1.0F);
         break;
      case 4:
         var2.x = (int)((float)var2.x + 1.0F);
         break;
      case 5:
         var2.x = (int)((float)var2.x - 1.0F);
      }

      var2.add(16, 16, 16);
      return var2;
   }

   public void setSegment(Segment var1) {
      this.segment = var1;
   }

   public boolean isRecordAllBlocks() {
      return this.recordAllBlocks;
   }

   public void setRecordAllBlocks(boolean var1) {
      this.recordAllBlocks = var1;
   }

   public int getBlockDeepness() {
      return this.blockDeepness;
   }

   public Int2ObjectOpenHashMap getRecordedBlocks() {
      return this.recordedBlocks;
   }

   public boolean isHasCollidingBlockFilter() {
      return this.hasCollidingBlockFilter;
   }

   public void setHasCollidingBlockFilter(boolean var1) {
      this.hasCollidingBlockFilter = var1;
   }

   public Int2ObjectOpenHashMap getCollidingBlocks() {
      return this.collidingBlocks;
   }

   public void setCollidingBlocks(Int2ObjectOpenHashMap var1) {
      this.collidingBlocks = var1;
   }

   public boolean isIgnoreDebris() {
      return this.ignoreDebris;
   }

   public boolean isIgnoereNotPhysical() {
      return this.ignoereNotPhysical;
   }

   public Segment getSegment() {
      return this.segment;
   }

   public void setSegment(Object var1) {
      this.setSegment((Segment)var1);
   }

   public boolean isDebug() {
      return this.debug;
   }

   public void setDebug(boolean var1) {
      this.debug = var1;
   }

   public SegmentController[] getFilter() {
      return this.filter;
   }

   public void setFilter(SegmentController... var1) {
      if (var1 != null) {
         this.filter = var1;
      } else {
         this.filter = new SegmentController[0];
      }
   }

   public boolean isZeroHpPhysical() {
      return this.zeroHpPhysical;
   }

   public void setZeroHpPhysical(boolean var1) {
      this.zeroHpPhysical = var1;
   }

   public boolean isOnlyCubeMeshes() {
      return this.onlyCubeMeshes;
   }

   public void setOnlyCubeMeshes(boolean var1) {
      this.onlyCubeMeshes = var1;
   }

   public void setIgnoereNotPhysical(boolean var1) {
      this.ignoereNotPhysical = var1;
   }

   public void setIgnoreDebris(boolean var1) {
      this.ignoreDebris = var1;
   }

   public void setRecordedBlocks(Int2ObjectOpenHashMap var1, int var2) {
      this.recordedBlocks = var1;
      this.blockDeepness = var2;
   }

   public boolean isDamageTest() {
      return this.damageTest;
   }

   public void setDamageTest(boolean var1) {
      this.damageTest = var1;
   }

   public boolean isSimpleRayTest() {
      return this.simpleRayTest;
   }

   public void setSimpleRayTest(boolean var1) {
      this.simpleRayTest = var1;
   }

   public boolean isCubesOnly() {
      return this.cubesOnly;
   }

   public void setCubesOnly(boolean var1) {
      this.cubesOnly = var1;
   }

   public void setHitNonblockCallback(NonBlockHitCallback var1) {
      this.nonBlockHitCallback = var1;
   }

   public NonBlockHitCallback getHitNonblockCallback() {
      return this.nonBlockHitCallback;
   }

   public void setCheckStabilizerPaths(boolean var1) {
      this.checkStabilizerPath = var1;
   }

   public boolean isCheckStabilizerPath() {
      return this.checkStabilizerPath;
   }

   public boolean isFiltered(SegmentController var1) {
      if (this.filter != null && this.filter.length != 0) {
         int var2;
         if (this.filterModeSingleNot) {
            for(var2 = 0; var2 < this.filter.length; ++var2) {
               if (this.filter[var2] == var1) {
                  return true;
               }
            }

            return false;
         } else {
            for(var2 = 0; var2 < this.filter.length; ++var2) {
               if (this.filter[var2] == var1) {
                  return false;
               }
            }

            return true;
         }
      } else {
         return false;
      }
   }

   public boolean isFilteredRoot(SegmentController var1) {
      if (this.filter != null && this.filter.length != 0) {
         int var2;
         if (this.filterModeSingleNot) {
            for(var2 = 0; var2 < this.filter.length; ++var2) {
               if (this.filter[var2] == var1) {
                  return true;
               }
            }

            return false;
         } else {
            for(var2 = 0; var2 < this.filter.length; ++var2) {
               if (this.filter[var2].railController.getRoot() == var1) {
                  return false;
               }
            }

            return true;
         }
      } else {
         return false;
      }
   }
}
