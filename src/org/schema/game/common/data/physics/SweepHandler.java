package org.schema.game.common.data.physics;

public interface SweepHandler {
   void separation(Pair var1);

   void overlap(Pair var1);
}
