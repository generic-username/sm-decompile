package org.schema.game.common.data.physics;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.common.data.physics.sweepandpruneaabb.CompoundCollisionObjectSweeper;

public class CompoundCollisionVariableSet {
   public final com.bulletphysics.util.ObjectPool pool = new com.bulletphysics.util.ObjectPool(CompoundCollisionAlgorithmExt.class);
   public Transform tmpTrans = new Transform();
   public Transform orgTrans = new Transform();
   public Transform chieldTrans = new Transform();
   public Transform interpolationTrans = new Transform();
   public Transform newChildWorldTrans = new Transform();
   public Transform tmpTransO = new Transform();
   public Transform orgTransO = new Transform();
   public Transform chieldTransO = new Transform();
   public Transform interpolationTransO = new Transform();
   public Transform newChildWorldTransO = new Transform();
   public int instances;
   public CompoundCollisionObjectSweeper sweeper = new CompoundCollisionObjectSweeper();
   public Transform tmpTrans0 = new Transform();
   public Transform tmpTrans1 = new Transform();
}
