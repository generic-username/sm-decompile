package org.schema.game.common.data.physics;

import com.bulletphysics.collision.narrowphase.ConvexPenetrationDepthSolver;
import com.bulletphysics.collision.narrowphase.GjkPairDetector;
import com.bulletphysics.collision.narrowphase.PointCollector;
import com.bulletphysics.collision.narrowphase.SimplexSolverInterface;
import com.bulletphysics.collision.narrowphase.ConvexCast.CastResult;
import com.bulletphysics.collision.narrowphase.DiscreteCollisionDetectorInterface.ClosestPointInput;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.collision.shapes.StaticPlaneShape;
import com.bulletphysics.linearmath.IDebugDraw;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.common.util.linAlg.Vector3fTools;

public class ContinuousConvexCollision {
   static final int MAX_ITERATIONS = 64;
   protected final com.bulletphysics.util.ObjectPool pointInputsPool = com.bulletphysics.util.ObjectPool.get(ClosestPointInput.class);
   private SimplexSolverInterface simplexSolver;
   private ConvexPenetrationDepthSolver penetrationDepthSolver;
   private ConvexShape convexA;
   private ConvexShape convexB1;
   private StaticPlaneShape planeShape;
   private Vector3f linVelA = new Vector3f();
   private Vector3f angVelA = new Vector3f();
   private Vector3f linVelB = new Vector3f();
   private Vector3f angVelB = new Vector3f();
   private Vector3f relLinVel = new Vector3f();
   private Transform interpolatedTransA = new Transform();
   private Transform interpolatedTransB = new Transform();
   private final GjkPairDetector gjk = new GjkPairDetector();

   public ContinuousConvexCollision(ConvexShape var1, ConvexShape var2, SimplexSolverInterface var3, ConvexPenetrationDepthSolver var4) {
      this.simplexSolver = var3;
      this.penetrationDepthSolver = var4;
      this.convexA = var1;
      this.convexB1 = var2;
      this.planeShape = null;
   }

   public ContinuousConvexCollision(ConvexShape var1, StaticPlaneShape var2) {
      this.convexA = var1;
      this.planeShape = var2;
   }

   public boolean calcTimeOfImpact(Transform var1, Transform var2, Transform var3, Transform var4, CastResult var5, GjkPairDetectorVariables var6) {
      this.linVelA.set(0.0F, 0.0F, 0.0F);
      this.angVelA.set(0.0F, 0.0F, 0.0F);
      this.linVelB.set(0.0F, 0.0F, 0.0F);
      this.angVelB.set(0.0F, 0.0F, 0.0F);
      TransformTools.calculateVelocity(var1, var2, 1.0F, this.linVelA, this.angVelA, var6.axis, var6.tmp2, var6.dmat, var6.dorn);
      TransformTools.calculateVelocity(var3, var4, 1.0F, this.linVelB, this.angVelB, var6.axis, var6.tmp2, var6.dmat, var6.dorn);
      if (!Vector3fTools.isNan(this.linVelA) && !Vector3fTools.isNan(this.linVelB)) {
         float var13 = this.convexA.getAngularMotionDisc();
         float var14 = this.convexB1 != null ? this.convexB1.getAngularMotionDisc() : 0.0F;
         var13 = this.angVelA.length() * var13 + this.angVelB.length() * var14;
         this.relLinVel.set(0.0F, 0.0F, 0.0F);
         this.relLinVel.sub(this.linVelB, this.linVelA);
         if (this.relLinVel.length() + var13 == 0.0F) {
            return false;
         } else {
            var14 = 0.0F;
            new Vector3f(1.0F, 0.0F, 0.0F);
            Vector3f var7 = new Vector3f(0.0F, 0.0F, 0.0F);
            float var10 = 0.0F;
            int var11 = 0;
            PointCollector var12 = new PointCollector();
            this.computeClosestPoints(var1, var3, var12, var6);
            boolean var8 = var12.hasResult;
            Vector3f var9 = var12.pointInWorld;
            if (var8) {
               float var15 = var12.distance + var5.allowedPenetration;
               var7.set(var12.normalOnBInWorld);
               if (this.relLinVel.dot(var7) + var13 <= 1.1920929E-7F) {
                  return false;
               } else {
                  do {
                     if (var15 <= 0.001F) {
                        var5.fraction = var14;
                        var5.normal.set(var7);
                        var5.hitPoint.set(var9);
                        return true;
                     }

                     float var17;
                     if ((var17 = this.relLinVel.dot(var7)) + var13 <= 1.1920929E-7F) {
                        return false;
                     }

                     var15 /= var17 + var13;
                     if ((var14 += var15) > 1.0F) {
                        return false;
                     }

                     if (var14 < 0.0F) {
                        System.err.println("HAS RESULT: BUT LAMDA IS TO SMALL " + var14);
                        return false;
                     }

                     if (var14 <= var10) {
                        System.err.println("HAS RESULT: BUT LAST LAMDA IS <= LAST LAMBDA");
                        return false;
                     }

                     var10 = var14;
                     this.interpolatedTransA.setIdentity();
                     this.interpolatedTransB.setIdentity();
                     TransformTools.integrateTransform(var1, this.linVelA, this.angVelA, var14, this.interpolatedTransA, var6.iAxis, var6.iDorn, var6.iorn0, var6.iPredictOrn, var6.float4Temp);
                     TransformTools.integrateTransform(var3, this.linVelB, this.angVelB, var14, this.interpolatedTransB, var6.iAxis, var6.iDorn, var6.iorn0, var6.iPredictOrn, var6.float4Temp);
                     Transform var16;
                     (var16 = new Transform()).set(this.interpolatedTransB);
                     var16.inverse();
                     var16.mul(this.interpolatedTransA);
                     var12 = new PointCollector();
                     this.computeClosestPoints(this.interpolatedTransA, this.interpolatedTransB, var12, var6);
                     if (!var12.hasResult) {
                        System.err.println("POINT HAS NO RESULT: -1 " + var11);
                        return false;
                     }

                     var15 = var12.distance + var5.allowedPenetration;
                     var9 = var12.pointInWorld;
                     var7.set(var12.normalOnBInWorld);
                     ++var11;
                  } while(var11 <= 64);

                  return false;
               }
            } else {
               return false;
            }
         }
      } else {
         System.err.println("WARNING: LINEAR VELOCITY WAS NAN: " + this.linVelA + "; " + this.linVelB);
         return false;
      }
   }

   void computeClosestPoints(Transform var1, Transform var2, PointCollector var3, GjkPairDetectorVariables var4) {
      if (this.convexB1 != null) {
         this.simplexSolver.reset();
         this.gjk.init(this.convexA, this.convexB1, this.simplexSolver, this.penetrationDepthSolver);
         ClosestPointInput var14;
         (var14 = (ClosestPointInput)this.pointInputsPool.get()).init();
         var14.transformA.set(var1);
         var14.transformB.set(var2);
         this.gjk.getClosestPoints(var14, var3, (IDebugDraw)null);
         this.pointInputsPool.release(var14);
      } else {
         ConvexShape var12 = this.convexA;
         StaticPlaneShape var5;
         Vector3f var6 = (var5 = this.planeShape).getPlaneNormal(new Vector3f());
         float var15 = var5.getPlaneConstant();
         Transform var7;
         (var7 = new Transform()).set(var2);
         var7.inverse();
         var7.mul(var1);
         Transform var8;
         (var8 = new Transform()).set(var1);
         var8.inverse();
         var8.mul(var2);
         Matrix3f var9;
         (var9 = new Matrix3f()).set(var8.basis);
         Vector3f var18;
         (var18 = new Vector3f(var6)).scale(-1.0F);
         var9.transform(var18);
         Vector3f var10 = var12.localGetSupportingVertex(var18, new Vector3f());
         Vector3f var13;
         (var13 = new Vector3f()).set(var10);
         var7.transform(var10);
         float var11 = var6.dot(var13) - var15;
         Vector3f var16 = new Vector3f();
         Vector3f var17;
         (var17 = new Vector3f(var6)).scale(var11);
         var16.sub(var13, var17);
         var13 = new Vector3f(var16);
         var2.transform(var13);
         var16 = new Vector3f(var6);
         var2.basis.transform(var16);
         var3.addContactPoint(var16, var13, var11);
      }
   }
}
