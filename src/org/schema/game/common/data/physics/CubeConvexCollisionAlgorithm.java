package org.schema.game.common.data.physics;

import com.bulletphysics.collision.broadphase.CollisionAlgorithm;
import com.bulletphysics.collision.broadphase.CollisionAlgorithmConstructionInfo;
import com.bulletphysics.collision.broadphase.DispatcherInfo;
import com.bulletphysics.collision.dispatch.CollisionAlgorithmCreateFunc;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.ManifoldResult;
import com.bulletphysics.collision.narrowphase.ConvexPenetrationDepthSolver;
import com.bulletphysics.collision.narrowphase.PersistentManifold;
import com.bulletphysics.collision.narrowphase.SimplexSolverInterface;
import com.bulletphysics.collision.narrowphase.DiscreteCollisionDetectorInterface.ClosestPointInput;
import com.bulletphysics.collision.shapes.CompoundShape;
import com.bulletphysics.collision.shapes.CompoundShapeChild;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.controller.SegmentBufferIteratorInterface;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.controller.elements.TriggerManagerInterface;
import org.schema.game.common.controller.elements.trigger.TriggerCollectionManager;
import org.schema.game.common.data.Dodecahedron;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.ActivationTrigger;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.octree.ArrayOctree;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class CubeConvexCollisionAlgorithm extends CollisionAlgorithm {
   private static final float margin = 0.05F;
   private static ThreadLocal threadLocalPool = new ThreadLocal() {
      protected final ObjectArrayList initialValue() {
         ObjectArrayList var1 = new ObjectArrayList(512);

         for(int var2 = 0; var2 < 512; ++var2) {
            var1.add(new CubeConvexCollisionAlgorithm());
         }

         return var1;
      }
   };
   private static ThreadLocal threadLocal = new ThreadLocal() {
      protected final CubeConvexVariableSet initialValue() {
         return new CubeConvexVariableSet();
      }
   };
   protected final com.bulletphysics.util.ObjectPool pointInputsPool = com.bulletphysics.util.ObjectPool.get(ClosestPointInput.class);
   public boolean lowLevelOfDetail;
   GjkPairDetectorExt gjkPairDetector;
   private boolean ownManifold;
   private boolean onServer;
   private PersistentManifold manifoldPtr;
   private CubeConvexVariableSet v;
   private CubeConvexCollisionAlgorithm.OuterSegmentHandler outerSegmentHandler;
   public static ObjectArrayFIFOQueue pool = new ObjectArrayFIFOQueue();
   private CubeConvexBlockCallback[] callbackCache;
   private int convexObjectId = 0;
   private short lastUpdateNumDrawPP;

   public void processDistinctCollision(CubeShape var1, CollisionObject var2, SegmentData var3, Transform var4, Transform var5, DispatcherInfo var6, ManifoldResult var7) {
      if (!this.v.intersectionCallBackAwithB.initialized) {
         this.v.intersectionCallBackAwithB.createHitCache(4096);
      }

      this.v.intersectionCallBackAwithB.reset();
      if (this.v.dodecahedron != null) {
         this.v.intersectionCallBackAwithB = var3.getOctree().findIntersectingDodecahedron(this.v.oSet, this.v.intersectionCallBackAwithB, var3.getSegment(), var4, this.v.absolute, 0.05F, this.v.dodecahedron.dodecahedron, 1.0F);
      } else {
         this.v.intersectionCallBackAwithB = var3.getOctree().findIntersectingAABB(this.v.oSet, this.v.intersectionCallBackAwithB, var3.getSegment(), var4, this.v.absolute, 0.05F, this.v.shapeMin, this.v.shapeMax, 1.0F);
      }

      if (this.v.intersectionCallBackAwithB.hitCount == 0) {
         var7.refreshContactPoints();
      } else {
         for(int var8 = 0; var8 < this.v.intersectionCallBackAwithB.hitCount; ++var8) {
            this.v.intersectionCallBackAwithB.getHit(var8, this.v.hitMin, this.v.hitMax, this.v.startA, this.v.endA);

            assert this.v.startA.x < this.v.endA.x && this.v.startA.y < this.v.endA.y && this.v.startA.z < this.v.endA.z;

            this.doNarrowTest(var3, var2, this.v.startA, this.v.endA, var6, var7);
         }

      }
   }

   public static void releaseCB(CubeConvexBlockCallback[] var0) {
      synchronized(pool) {
         pool.enqueue(var0);
      }
   }

   public static CubeConvexBlockCallback[] getCB() {
      synchronized(pool) {
         if (!pool.isEmpty()) {
            return (CubeConvexBlockCallback[])pool.dequeue();
         } else {
            CubeConvexBlockCallback[] var1 = new CubeConvexBlockCallback[4096];

            for(int var2 = 0; var2 < var1.length; ++var2) {
               var1[var2] = new CubeConvexBlockCallback();
            }

            return var1;
         }
      }
   }

   private void doNarrowTest(SegmentData var1, CollisionObject var2, Vector3b var3, Vector3b var4, DispatcherInfo var5, ManifoldResult var6) {
      var2.getCollisionShape().getAabb(this.v.convexShapeTransform, this.v.otherminOut, this.v.othermaxOut);
      Vector3i var12 = var1.getSegment().pos;

      for(byte var13 = var3.x; var13 < var4.x; ++var13) {
         for(byte var14 = var3.y; var14 < var4.y; ++var14) {
            for(byte var7 = var3.z; var7 < var4.z; ++var7) {
               int var8 = SegmentData.getInfoIndex((byte)(var13 + 16), (byte)(var14 + 16), (byte)(var7 + 16));
               ElementInformation var9;
               if (var1.contains(var8) && ((var9 = ElementKeyMap.getInfo(var1.getType(var8))).isPhysical(var1.isActive(var8)) || var9.getId() == 411) && (!var9.hasLod() || !var9.lodCollisionPhysical)) {
                  byte var10 = var1.getOrientation(var8);
                  this.v.elemPosA.set((float)var13, (float)var14, (float)var7);
                  Vector3f var10000 = this.v.elemPosA;
                  var10000.x += (float)var12.x;
                  var10000 = this.v.elemPosA;
                  var10000.y += (float)var12.y;
                  var10000 = this.v.elemPosA;
                  var10000.z += (float)var12.z;
                  this.v.min.set(this.v.elemPosA);
                  this.v.max.set(this.v.elemPosA);
                  var10000 = this.v.min;
                  var10000.x -= 0.5F;
                  var10000 = this.v.min;
                  var10000.y -= 0.5F;
                  var10000 = this.v.min;
                  var10000.z -= 0.5F;
                  var10000 = this.v.max;
                  var10000.x += 0.5F;
                  var10000 = this.v.max;
                  var10000.y += 0.5F;
                  var10000 = this.v.max;
                  var10000.z += 0.5F;
                  AabbUtil2.transformAabb(this.v.min, this.v.max, 0.03F, this.v.cubeMeshTransform, this.v.minOut, this.v.maxOut);
                  boolean var11;
                  if (this.v.dodecahedron != null) {
                     var11 = this.v.dodecahedron.dodecahedron.testAABB(this.v.minOut, this.v.maxOut, this.v.dodecaOverlap);
                  } else {
                     var11 = AabbUtil2.testAabbAgainstAabb2(this.v.minOut, this.v.maxOut, this.v.shapeMin, this.v.shapeMax);
                  }

                  this.v.box0.setMargin(0.0F);
                  if (var11) {
                     this.growCache(this.v.cubeCallbackPointer);
                     this.v.boxTransformation.set(this.v.cubeMeshTransform);
                     this.v.nA.set(this.v.elemPosA);
                     this.v.boxTransformation.basis.transform(this.v.nA);
                     this.v.boxTransformation.origin.add(this.v.nA);
                     this.callbackCache[this.v.cubeCallbackPointer].blockPos.set(var13 + var1.getSegment().pos.x + 16, var14 + var1.getSegment().pos.y + 16, var7 + var1.getSegment().pos.z + 16);
                     this.callbackCache[this.v.cubeCallbackPointer].boxTransform.set(this.v.boxTransformation);
                     this.callbackCache[this.v.cubeCallbackPointer].blockInfo.set(var9.getId(), var10, var1.isActive(var8) ? 1 : 0);
                     if (this.v.dodecahedron != null) {
                        for(var8 = 0; var8 < this.v.dodecaOverlap.length; ++var8) {
                           for(int var15 = 0; var15 < this.v.dodecaOverlap[var8].length; ++var15) {
                              this.callbackCache[this.v.cubeCallbackPointer].dodecaOverlap[var8][var15] = this.v.dodecaOverlap[var8][var15];
                           }
                        }
                     }

                     ++this.v.cubeCallbackPointer;
                  }
               }
            }
         }
      }

   }

   private void growCache(int var1) {
      if (var1 >= this.callbackCache.length) {
         CubeConvexBlockCallback[] var3 = new CubeConvexBlockCallback[this.callbackCache.length << 1];

         int var2;
         for(var2 = 0; var2 < this.callbackCache.length; ++var2) {
            var3[var2] = this.callbackCache[var2];
         }

         for(var2 = this.callbackCache.length; var2 < var3.length; ++var2) {
            var3[var2] = new CubeConvexBlockCallback();
         }

         this.callbackCache = var3;
      }

   }

   private void doRegularCollision(ConvexShape var1, ConvexShape var2, Transform var3, Transform var4, ManifoldResult var5, PersistentManifold var6, DispatcherInfo var7, int var8, Vector3i var9, int var10) {
      ClosestPointInput var11;
      (var11 = (ClosestPointInput)this.pointInputsPool.get()).init();
      this.gjkPairDetector.setMinkowskiA(var1);
      this.gjkPairDetector.setMinkowskiB(var2);
      var11.maximumDistanceSquared = var1.getMargin() + var6.getContactBreakingThreshold();
      var11.maximumDistanceSquared *= var11.maximumDistanceSquared;
      var11.transformA.set(var3);
      var11.transformB.set(var4);
      this.v.blockA.set((float)var9.x, (float)var9.y, (float)var9.z);
      this.gjkPairDetector.getClosestPoints(var11, var5, var7.debugDraw, false, this.v.blockA, this.v.blockB, (short)var8, (short)0, var10, this.convexObjectId);
      this.pointInputsPool.release(var11);
      var5.refreshContactPoints();
   }

   public void init(CollisionAlgorithmConstructionInfo var1) {
      super.init(var1);
      this.manifoldPtr = var1.manifold;
   }

   public void destroy() {
   }

   public void processCollision(CollisionObject var1, CollisionObject var2, DispatcherInfo var3, ManifoldResult var4) {
      if (this.manifoldPtr == null) {
         this.ownManifold = true;
      }

      this.convexObjectId = 0;

      assert !(var1 instanceof RigidBodySegmentController) || !((RigidBodySegmentController)var1).isCollisionException() : var1;

      assert !(var2 instanceof RigidBodySegmentController) || !((RigidBodySegmentController)var2).isCollisionException() : var2;

      this.manifoldPtr = var4.getPersistentManifold();
      this.ownManifold = true;
      System.currentTimeMillis();
      if (!(var2 instanceof PairCachingGhostObjectUncollidable)) {
         this.v.cubeCallbackPointer = 0;
         var4.setPersistentManifold(this.manifoldPtr);
         CubeShape var5 = (CubeShape)var1.getCollisionShape();
         ConvexShape var6 = (ConvexShape)var2.getCollisionShape();
         this.v.cubesShape0 = var5;
         this.v.convexShape = var6;
         if (var2.getUserPointer() != null && var2.getUserPointer() instanceof Integer) {
            this.convexObjectId = (Integer)var2.getUserPointer();
         } else {
            this.convexObjectId = 0;
         }

         if (this.v.convexShape instanceof DodecahedronShapeExt) {
            this.v.dodecahedron = (DodecahedronShapeExt)this.v.convexShape;
         } else {
            this.v.dodecahedron = null;
         }

         this.onServer = var5.getSegmentBuffer().getSegmentController().isOnServer();
         this.v.oSet = ArrayOctree.getSet(var5.getSegmentBuffer().getSegmentController().isOnServer());
         this.v.cubeMeshTransform = var1.getWorldTransform(this.v.cubeMeshTransform);
         this.v.convexShapeTransform = var2.getWorldTransform(this.v.convexShapeTransform);
         this.v.cubeShapeTransformInv.set(this.v.cubeMeshTransform);
         this.v.cubeShapeTransformInv.inverse();
         this.v.absolute.set(this.v.cubeMeshTransform.basis);
         MatrixUtil.absolute(this.v.absolute);
         var5.setMargin(0.05F);
         this.v.convexShapeViewFromCubes.set(this.v.cubeShapeTransformInv);
         this.v.convexShapeViewFromCubes.mul(this.v.convexShapeTransform);
         var5.getAabb(TransformTools.ident, this.v.outMin, this.v.outMax);
         var6.getAabb(this.v.convexShapeViewFromCubes, this.v.inner.min, this.v.inner.max);
         var6.getAabb(this.v.convexShapeTransform, this.v.shapeMin, this.v.shapeMax);
         var5.setMargin(0.05F);
         this.outerSegmentHandler.col1 = var2;
         this.outerSegmentHandler.cubeShape0 = var5;
         this.outerSegmentHandler.dispatchInfo = var3;
         this.outerSegmentHandler.resultOut = var4;
         this.v.outer.min.set(this.v.outMin);
         this.v.outer.max.set(this.v.outMax);
         if (this.v.inner.getIntersection(this.v.outer, this.v.outBB) != null && this.v.outBB.isValid()) {
            this.v.minIntA.x = ByteUtil.divSeg((int)(this.v.outBB.min.x - 16.0F)) << 5;
            this.v.minIntA.y = ByteUtil.divSeg((int)(this.v.outBB.min.y - 16.0F)) << 5;
            this.v.minIntA.z = ByteUtil.divSeg((int)(this.v.outBB.min.z - 16.0F)) << 5;
            this.v.maxIntA.x = FastMath.fastCeil((this.v.outBB.max.x + 16.0F) / 32.0F) << 5;
            this.v.maxIntA.y = FastMath.fastCeil((this.v.outBB.max.y + 16.0F) / 32.0F) << 5;
            this.v.maxIntA.z = FastMath.fastCeil((this.v.outBB.max.z + 16.0F) / 32.0F) << 5;
            this.callbackCache = getCB();
            var5.getSegmentBuffer().iterateOverNonEmptyElementRange(this.outerSegmentHandler, this.v.minIntA, this.v.maxIntA, false);
            this.handleCubeCallbacks(var5, var6, var1, var2, var4, var3);
            releaseCB(this.callbackCache);
            this.callbackCache = null;
            this.outerSegmentHandler.col1 = null;
            this.outerSegmentHandler.cubeShape0 = null;
            this.outerSegmentHandler.dispatchInfo = null;
            this.outerSegmentHandler.resultOut = null;
         }
      }
   }

   public float calculateTimeOfImpact(CollisionObject var1, CollisionObject var2, DispatcherInfo var3, ManifoldResult var4) {
      System.err.println("CALCULATING CONVEX CUBE TOI");
      return 1.0F;
   }

   public void getAllContactManifolds(ObjectArrayList var1) {
      if (this.manifoldPtr != null && this.ownManifold) {
         var1.add(this.manifoldPtr);
      }

   }

   public void init(PersistentManifold var1, CollisionAlgorithmConstructionInfo var2, CollisionObject var3, CollisionObject var4, SimplexSolverInterface var5, ConvexPenetrationDepthSolver var6, boolean var7) {
      super.init(var2);
      this.manifoldPtr = var1;
      this.v = (CubeConvexVariableSet)threadLocal.get();
      this.gjkPairDetector = new GjkPairDetectorExt(this.v.gjkVars);
      this.gjkPairDetector.useExtraPenetrationCheck = false;
      this.gjkPairDetector.init((ConvexShape)null, (ConvexShape)null, var5, var6);
      if (this.manifoldPtr == null) {
         if (!var7) {
            this.manifoldPtr = this.dispatcher.getNewManifold(var3, var4);
         } else {
            this.manifoldPtr = this.dispatcher.getNewManifold(var4, var3);
         }

         this.ownManifold = true;
      } else {
         this.ownManifold = false;
      }

      this.outerSegmentHandler = new CubeConvexCollisionAlgorithm.OuterSegmentHandler();
   }

   private void handleCubeCallbacks(CubeShape var1, ConvexShape var2, CollisionObject var3, CollisionObject var4, ManifoldResult var5, DispatcherInfo var6) {
      int var7 = this.v.cubeCallbackPointer;

      for(int var8 = 0; var8 < var7; ++var8) {
         CubeConvexBlockCallback var9;
         Transform var10 = (var9 = this.callbackCache[var8]).boxTransform;
         Vector3i var11 = var9.blockInfo;
         Vector3i var12 = var9.blockPos;
         boolean[][] var13 = var9.dodecaOverlap;
         int var14;
         ElementInformation var15;
         BlockStyle var16 = (var15 = ElementKeyMap.getInfoFast((short)(var14 = var11.x))).getBlockStyle();
         int var17;
         if (var14 == 689) {
            if (var11.y == 4) {
               continue;
            }

            var17 = 2;
         } else {
            var17 = var11.y;
         }

         int var10000 = var11.z;
         Object var18 = this.v.box0;
         if (var15.lodUseDetailCollision) {
            var18 = var15.lodDetailCollision.getShape((short)var14, (byte)var17, this.v.lodBlockTransform);
         } else if (!var15.isNormalBlockStyle() && var16 != BlockStyle.SPRITE) {
            var18 = BlockShapeAlgorithm.getShape(var16, (byte)var17);
         }

         if (var15.getSlab(var11.y) > 0) {
            (var10 = this.v.BT).set(var9.boxTransform);
            this.v.orientTT.set(Element.DIRECTIONSf[Element.switchLeftRight(var17 % 6)]);
            var10.basis.transform(this.v.orientTT);
            switch(var15.getSlab(var11.y)) {
            case 1:
               this.v.orientTT.scale(0.125F);
               var18 = this.v.box34[var17 % 6];
               break;
            case 2:
               this.v.orientTT.scale(0.25F);
               var18 = this.v.box12[var17 % 6];
               break;
            case 3:
               this.v.orientTT.scale(0.375F);
               var18 = this.v.box14[var17 % 6];
            }

            var10.origin.sub(this.v.orientTT);
         }

         if (!(var18 instanceof CompoundShape)) {
            ConvexShape var20 = (ConvexShape)var18;
            this.doDetailedCollision(var3, var4, var20, var15, var2, var12, var10, var13, var5, var6, var14, var9, var1);
         } else {
            CompoundShape var19 = (CompoundShape)var18;

            for(int var21 = 0; var21 < var19.getNumChildShapes(); ++var21) {
               CompoundShapeChild var22 = (CompoundShapeChild)var19.getChildList().get(var21);
               this.v.lodBlockTransform.origin.set(0.0F, 0.0F, 0.0F);
               this.v.boxETransform.set(var10);
               this.v.boxETransform.mul(this.v.lodBlockTransform);
               this.v.boxETransform.mul(var22.transform);
               ConvexShape var23 = (ConvexShape)var22.childShape;
               this.doDetailedCollision(var3, var4, var23, var15, var2, var12, this.v.boxETransform, var13, var5, var6, var14, var9, var1);
            }

            if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn() && !var1.getSegmentController().isOnServer()) {
               this.lastUpdateNumDrawPP = ((GameClientState)var1.getSegmentController().getState()).getNumberOfUpdate();
            }
         }
      }

      if (this.ownManifold) {
         var5.refreshContactPoints();
      } else {
         assert false;

      }
   }

   private void doDetailedCollision(CollisionObject var1, CollisionObject var2, ConvexShape var3, ElementInformation var4, ConvexShape var5, Vector3i var6, Transform var7, boolean[][] var8, ManifoldResult var9, DispatcherInfo var10, int var11, CubeConvexBlockCallback var12, CubeShape var13) {
      if (var2 instanceof RigidDebrisBody) {
         this.doRegularCollision(var3, var5, var7, this.v.convexShapeTransform, var9, this.manifoldPtr, var10, var11, var12.blockPos, var13.getSegmentController().getId());
      } else {
         try {
            if (var4.getId() == 411) {
               this.v.dist.sub(var7.origin, this.v.convexShapeTransform.origin);
               if (this.v.dist.length() < 0.69F && !var13.getSegmentBuffer().getSegmentController().isOnServer()) {
                  ManifoldResult var18 = new ManifoldResult(var1, var2);
                  PersistentManifold var14 = new PersistentManifold();
                  var18.setPersistentManifold(var14);
                  this.doRegularCollision(var3, var5, var7, this.v.convexShapeTransform, var18, var14, var10, var11, var12.blockPos, var13.getSegmentController().getId());
                  ManagedSegmentController var19;
                  if (var14.getNumContacts() > 0 && var13.getSegmentBuffer().getSegmentController() instanceof ManagedSegmentController && (var19 = (ManagedSegmentController)var13.getSegmentBuffer().getSegmentController()).getManagerContainer() instanceof TriggerManagerInterface) {
                     ManagerModuleCollection var20 = ((TriggerManagerInterface)var19.getManagerContainer()).getTrigger();
                     long var15 = ElementCollection.getIndex(var6);
                     Iterator var21 = var20.getCollectionManagers().iterator();

                     while(var21.hasNext()) {
                        TriggerCollectionManager var23;
                        if ((var23 = (TriggerCollectionManager)var21.next()).rawCollection.contains(var15)) {
                           ActivationTrigger var24 = new ActivationTrigger(var23.getControllerIndex(), var2, var4.getId());
                           ActivationTrigger var25;
                           if ((var25 = (ActivationTrigger)var13.getSegmentBuffer().getSegmentController().getTriggers().get(var24)) == null) {
                              var13.getSegmentBuffer().getSegmentController().getTriggers().add(var24);
                           } else {
                              var25.ping();
                           }
                        }
                     }
                  }

                  return;
               }
            } else {
               if (this.v.dodecahedron != null) {
                  for(int var22 = 0; var22 < var8.length; ++var22) {
                     if (var8[var22][0]) {
                        for(int var26 = 1; var26 < var8[var22].length; ++var26) {
                           if (var8[var22][var26]) {
                              this.doRegularCollision(var3, this.v.dodecahedron.dodecahedron.shapes[var22][var26 - 1], var7, this.v.convexShapeTransform, var9, this.manifoldPtr, var10, var11, var12.blockPos, var13.getSegmentController().getId());
                           }
                        }
                     }
                  }

                  return;
               }

               this.doRegularCollision(var3, var5, var7, this.v.convexShapeTransform, var9, this.manifoldPtr, var10, var11, var12.blockPos, var13.getSegmentController().getId());
            }

         } catch (Exception var17) {
            var17.printStackTrace();
            System.err.println(this.v.box0 + ", " + var5 + ", " + var7 + ", " + this.v.convexShapeTransform + ", " + var9 + ", " + var10);
         }
      }
   }

   class OuterSegmentHandler implements SegmentBufferIteratorInterface {
      CubeShape cubeShape0;
      CollisionObject col1;
      DispatcherInfo dispatchInfo;
      ManifoldResult resultOut;

      private OuterSegmentHandler() {
      }

      public boolean handle(Segment var1, long var2) {
         if (var1.getSegmentData() != null && !var1.isEmpty()) {
            this.cubeShape0.getSegmentAabb(var1, CubeConvexCollisionAlgorithm.this.v.cubeMeshTransform, CubeConvexCollisionAlgorithm.this.v.outMin, CubeConvexCollisionAlgorithm.this.v.outMax, CubeConvexCollisionAlgorithm.this.v.localMinOut, CubeConvexCollisionAlgorithm.this.v.localMaxOut, CubeConvexCollisionAlgorithm.this.v.aabbVarSet);
            boolean var4;
            if (CubeConvexCollisionAlgorithm.this.v.dodecahedron != null) {
               var4 = false;
               Dodecahedron var3;
               if ((var3 = CubeConvexCollisionAlgorithm.this.v.dodecahedron.dodecahedron).intersectsOuterRadius(CubeConvexCollisionAlgorithm.this.v.convexShapeTransform, CubeConvexCollisionAlgorithm.this.v.outMin, CubeConvexCollisionAlgorithm.this.v.outMax, 1.0F)) {
                  var4 = var3.testAABB(CubeConvexCollisionAlgorithm.this.v.outMin, CubeConvexCollisionAlgorithm.this.v.outMax);
               }
            } else {
               var4 = AabbUtil2.testAabbAgainstAabb2(CubeConvexCollisionAlgorithm.this.v.shapeMin, CubeConvexCollisionAlgorithm.this.v.shapeMax, CubeConvexCollisionAlgorithm.this.v.outMin, CubeConvexCollisionAlgorithm.this.v.outMax);
            }

            if (var4) {
               CubeConvexCollisionAlgorithm.this.processDistinctCollision(this.cubeShape0, this.col1, var1.getSegmentData(), CubeConvexCollisionAlgorithm.this.v.cubeMeshTransform, CubeConvexCollisionAlgorithm.this.v.convexShapeTransform, this.dispatchInfo, this.resultOut);
            }

            return true;
         } else {
            return true;
         }
      }

      // $FF: synthetic method
      OuterSegmentHandler(Object var2) {
         this();
      }
   }

   public static class CreateFunc extends CollisionAlgorithmCreateFunc {
      private final ObjectArrayList pool;
      public SimplexSolverInterface simplexSolver;
      public ConvexPenetrationDepthSolver pdSolver;

      public CreateFunc(SimplexSolverInterface var1, ConvexPenetrationDepthSolver var2) {
         this.pool = (ObjectArrayList)CubeConvexCollisionAlgorithm.threadLocalPool.get();
         this.simplexSolver = var1;
         this.pdSolver = var2;
      }

      public CollisionAlgorithm createCollisionAlgorithm(CollisionAlgorithmConstructionInfo var1, CollisionObject var2, CollisionObject var3) {
         CubeConvexCollisionAlgorithm var4;
         if (this.pool.isEmpty()) {
            var4 = new CubeConvexCollisionAlgorithm();
         } else {
            var4 = (CubeConvexCollisionAlgorithm)this.pool.remove(this.pool.size() - 1);
         }

         var4.init(var1.manifold, var1, var2, var3, this.simplexSolver, this.pdSolver, this.swapped);
         return var4;
      }

      public void releaseCollisionAlgorithm(CollisionAlgorithm var1) {
         this.pool.add((CubeConvexCollisionAlgorithm)var1);
      }
   }
}
