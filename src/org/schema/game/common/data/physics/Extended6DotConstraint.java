package org.schema.game.common.data.physics;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.constraintsolver.Generic6DofConstraint;
import com.bulletphysics.dynamics.constraintsolver.RotationalLimitMotor;
import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.linearmath.VectorUtil;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.schine.network.StateInterface;

public class Extended6DotConstraint extends Generic6DofConstraint {
   public final Vector3f linearJointAxis = new Vector3f();
   public final Vector3f aJ = new Vector3f();
   public final Vector3f bJ = new Vector3f();
   public final Vector3f m_0MinvJt = new Vector3f();
   public final Vector3f m_1MinvJt = new Vector3f();
   protected final RotationalLimitMotorAbsVelocity[] angularLimits = new RotationalLimitMotorAbsVelocity[]{new RotationalLimitMotorAbsVelocity(), new RotationalLimitMotorAbsVelocity(), new RotationalLimitMotorAbsVelocity()};

   public Extended6DotConstraint(StateInterface var1, RigidBody var2, RigidBody var3, Transform var4, Transform var5, boolean var6) {
      super(var2, var3, var4, var5, var6);
   }

   private static float getMatrixElem(Matrix3f var0, int var1) {
      int var2 = var1 % 3;
      var1 /= 3;
      return var0.getElement(var2, var1);
   }

   private static boolean matrixToEulerXYZ(Matrix3f var0, Vector3f var1) {
      if (getMatrixElem(var0, 2) < 1.0F) {
         if (getMatrixElem(var0, 2) > -1.0F) {
            var1.x = (float)Math.atan2((double)(-getMatrixElem(var0, 5)), (double)getMatrixElem(var0, 8));
            var1.y = (float)Math.asin((double)getMatrixElem(var0, 2));
            var1.z = (float)Math.atan2((double)(-getMatrixElem(var0, 1)), (double)getMatrixElem(var0, 0));
            return true;
         } else {
            System.err.println("WARNING.  Not unique.  XA - ZA = -atan2(r10,r11)");
            var1.x = -((float)Math.atan2((double)getMatrixElem(var0, 3), (double)getMatrixElem(var0, 4)));
            var1.y = -1.5707964F;
            var1.z = 0.0F;
            return false;
         }
      } else {
         System.err.println("WARNING.  Not unique.  XAngle + ZAngle = atan2(r10,r11)");
         var1.x = (float)Math.atan2((double)getMatrixElem(var0, 3), (double)getMatrixElem(var0, 4));
         var1.y = 1.5707964F;
         var1.z = 0.0F;
         return false;
      }
   }

   protected void calculateAngleInfo() {
      Matrix3f var1 = new Matrix3f();
      Matrix3f var2 = new Matrix3f();
      var1.set(this.calculatedTransformA.basis);
      MatrixUtil.invert(var1);
      var2.mul(var1, this.calculatedTransformB.basis);
      matrixToEulerXYZ(var2, this.calculatedAxisAngleDiff);
      Vector3f var3 = new Vector3f();
      this.calculatedTransformB.basis.getColumn(0, var3);
      Vector3f var4 = new Vector3f();
      this.calculatedTransformA.basis.getColumn(2, var4);
      this.calculatedAxis[1].cross(var4, var3);
      this.calculatedAxis[0].cross(this.calculatedAxis[1], var4);
      this.calculatedAxis[2].cross(var3, this.calculatedAxis[1]);
   }

   protected void buildAngularJacobian(int var1, Vector3f var2) {
      Matrix3f var3;
      (var3 = this.rbA.getCenterOfMassTransform(new Transform()).basis).transpose();
      Matrix3f var4;
      (var4 = this.rbB.getCenterOfMassTransform(new Transform()).basis).transpose();
      if (!this.checkJacobian(var2, var3, var4, this.rbA.getInvInertiaDiagLocal(new Vector3f()), this.rbB.getInvInertiaDiagLocal(new Vector3f()))) {
         System.err.println("Exception: Jacobian Entry init failed!");
      } else {
         super.buildAngularJacobian(var1, var2);
      }
   }

   public boolean testAngularLimitMotor(int var1) {
      float var2 = VectorUtil.getCoord(this.calculatedAxisAngleDiff, var1);
      this.angularLimits[var1].testLimitValue(var2);
      return this.angularLimits[var1].needApplyTorques();
   }

   public void buildJacobian() {
      this.linearLimits.accumulatedImpulse.set(0.0F, 0.0F, 0.0F);

      for(int var1 = 0; var1 < 3; ++var1) {
         this.angularLimits[var1].accumulatedImpulse = 0.0F;
      }

      this.calculateTransforms();
      new Vector3f();
      this.calcAnchorPos();
      Vector3f var5 = new Vector3f(this.anchorPos);
      Vector3f var2 = new Vector3f(this.anchorPos);
      Vector3f var3 = new Vector3f();

      int var4;
      for(var4 = 0; var4 < 3; ++var4) {
         if (this.linearLimits.isLimited(var4)) {
            if (this.useLinearReferenceFrameA) {
               this.calculatedTransformA.basis.getColumn(var4, var3);
            } else {
               this.calculatedTransformB.basis.getColumn(var4, var3);
            }

            this.buildLinearJacobian(var4, var3, var5, var2);
         }
      }

      for(var4 = 0; var4 < 3; ++var4) {
         if (this.testAngularLimitMotor(var4)) {
            this.getAxis(var4, var3);
            this.buildAngularJacobian(var4, var3);
         }
      }

   }

   public void solveConstraint(float var1) {
      this.timeStep = var1;
      Vector3f var2 = new Vector3f(this.calculatedTransformA.origin);
      Vector3f var3 = new Vector3f(this.calculatedTransformB.origin);
      Vector3f var5 = new Vector3f();

      int var6;
      for(var6 = 0; var6 < 3; ++var6) {
         if (this.linearLimits.isLimited(var6)) {
            float var4 = 1.0F / this.jacLinear[var6].getDiagonal();
            if (this.useLinearReferenceFrameA) {
               this.calculatedTransformA.basis.getColumn(var6, var5);
            } else {
               this.calculatedTransformB.basis.getColumn(var6, var5);
            }

            this.linearLimits.solveLinearAxis(this.timeStep, var4, this.rbA, var2, this.rbB, var3, var6, var5, this.anchorPos);
         }
      }

      var2 = new Vector3f();

      for(var6 = 0; var6 < 3; ++var6) {
         if (this.angularLimits[var6].needApplyTorques()) {
            this.getAxis(var6, var2);
            float var7;
            if (this.jacAng[var6].getDiagonal() == 0.0F) {
               var7 = 0.1F;
            } else {
               var7 = 1.0F / this.jacAng[var6].getDiagonal();
            }

            this.angularLimits[var6].solveAngularLimits(this.timeStep, var2, var7, this.rbA, this.rbB);
         }
      }

   }

   public void setAngularLowerLimit(Vector3f var1) {
      this.angularLimits[0].loLimit = var1.x;
      this.angularLimits[1].loLimit = var1.y;
      this.angularLimits[2].loLimit = var1.z;
   }

   public void setAngularUpperLimit(Vector3f var1) {
      this.angularLimits[0].hiLimit = var1.x;
      this.angularLimits[1].hiLimit = var1.y;
      this.angularLimits[2].hiLimit = var1.z;
   }

   public RotationalLimitMotor getRotationalLimitMotor(int var1) {
      return this.angularLimits[var1];
   }

   public void setLimit(int var1, float var2, float var3) {
      if (var1 < 3) {
         VectorUtil.setCoord(this.linearLimits.lowerLimit, var1, var2);
         VectorUtil.setCoord(this.linearLimits.upperLimit, var1, var3);
      } else {
         this.angularLimits[var1 - 3].loLimit = var2;
         this.angularLimits[var1 - 3].hiLimit = var3;
      }
   }

   public boolean isLimited(int var1) {
      return var1 < 3 ? this.linearLimits.isLimited(var1) : this.angularLimits[var1 - 3].isLimited();
   }

   public boolean checkJacobian(Vector3f var1, Matrix3f var2, Matrix3f var3, Vector3f var4, Vector3f var5) {
      this.linearJointAxis.set(0.0F, 0.0F, 0.0F);
      this.aJ.set(var1);
      var2.transform(this.aJ);
      this.bJ.set(var1);
      this.bJ.negate();
      var3.transform(this.bJ);
      VectorUtil.mul(this.m_0MinvJt, var4, this.aJ);
      VectorUtil.mul(this.m_1MinvJt, var5, this.bJ);
      return this.m_0MinvJt.dot(this.aJ) + this.m_1MinvJt.dot(this.bJ) > 0.0F;
   }

   public void matrixToEuler(Matrix3f var1, Vector3f var2) {
      float var3;
      float var4;
      float var5;
      float var6;
      if ((double)FastMath.abs(var4 = (float)Math.cos((double)(var3 = (float)(-Math.asin((double)var1.m02))))) > 0.005D) {
         var5 = var1.m22 / var4;
         var6 = (float)Math.atan2((double)(-var1.m12 / var4), (double)var5);
         var5 = var1.m00 / var4;
         var4 = (float)Math.atan2((double)(-var1.m01 / var4), (double)var5);
      } else {
         var6 = 0.0F;
         var5 = var1.m11;
         var4 = (float)Math.atan2((double)var1.m10, (double)var5);
      }

      var6 = FastMath.clamp(var6, 0.0F, 6.2831855F);
      float var7 = FastMath.clamp(var3, 0.0F, 6.2831855F);
      var4 = FastMath.clamp(var4, 0.0F, 6.2831855F);
      var2.x = var6;
      var2.y = var7;
      var2.z = var4;
      System.err.println("Euler: " + var2);
   }
}
