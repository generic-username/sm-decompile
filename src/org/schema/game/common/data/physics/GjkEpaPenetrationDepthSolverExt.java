package org.schema.game.common.data.physics;

import com.bulletphysics.collision.narrowphase.ConvexPenetrationDepthSolver;
import com.bulletphysics.collision.narrowphase.SimplexSolverInterface;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.linearmath.IDebugDraw;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;

public class GjkEpaPenetrationDepthSolverExt extends ConvexPenetrationDepthSolver {
   private GjkEpaSolverExt gjkEpaSolver = new GjkEpaSolverExt();
   private GjkEpaSolverExt.Results results = new GjkEpaSolverExt.Results();

   public boolean calcPenDepth(SimplexSolverInterface var1, ConvexShape var2, ConvexShape var3, Transform var4, Transform var5, Vector3f var6, Vector3f var7, Vector3f var8, IDebugDraw var9) {
      this.results.reset();
      if (this.gjkEpaSolver.collide(var2, var4, var3, var5, 0.0F, this.results)) {
         var7.set(this.results.witnesses[0]);
         var8.set(this.results.witnesses[1]);
         return true;
      } else {
         return false;
      }
   }
}
