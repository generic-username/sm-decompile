package org.schema.game.common.data.physics;

import com.bulletphysics.collision.broadphase.BroadphaseNativeType;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Matrix4fTools;
import org.schema.game.common.controller.SegmentBufferInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.physics.sweepandpruneaabb.SegmentAabbInterface;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;

public class CubeShape extends CollisionShape implements SegmentAabbInterface {
   private static Vector3f localScaling = new Vector3f(1.0F, 1.0F, 1.0F);
   private final SegmentBufferInterface segmentBuffer;
   private final Vector3f min = new Vector3f();
   private final Vector3f max = new Vector3f();
   private final Vector3f minCached = new Vector3f();
   private final Vector3f maxCached = new Vector3f();
   private final Transform cachedTransform = new Transform();
   private float margin = 0.0F;
   private short cacheDate = -1;
   private float cachedMargin;
   private final AABBVarSet varSet;
   private static ThreadLocal threadLocal = new ThreadLocal() {
      protected final AABBVarSet initialValue() {
         return new AABBVarSet();
      }
   };

   public CubeShape(SegmentBufferInterface var1) {
      this.segmentBuffer = var1;
      this.cachedTransform.setIdentity();
      this.varSet = (AABBVarSet)threadLocal.get();
   }

   public static void transformAabb(Vector3f var0, Vector3f var1, float var2, Transform var3, Vector3f var4, Vector3f var5, AABBVarSet var6) {
      assert var0.x <= var1.x;

      assert var0.y <= var1.y;

      assert var0.z <= var1.z;

      Vector3f var7;
      (var7 = var6.localHalfExtents).sub(var1, var0);
      var7.scale(0.5F);
      var7.x += var2;
      var7.y += var2;
      var7.z += var2;
      Vector3f var9;
      (var9 = var6.localCenter).add(var1, var0);
      var9.scale(0.5F);
      Matrix3f var8;
      (var8 = var6.abs_b).set(var3.basis);
      MatrixUtil.absolute(var8);
      (var1 = var6.center).set(var9);
      var3.transform(var1);
      (var9 = var6.extent).x = var8.m00 * var7.x + var8.m01 * var7.y + var8.m02 * var7.z;
      var9.y = var8.m10 * var7.x + var8.m11 * var7.y + var8.m12 * var7.z;
      var9.z = var8.m20 * var7.x + var8.m21 * var7.y + var8.m22 * var7.z;
      var4.sub(var1, var9);
      var5.add(var1, var9);
   }

   public static void transformAabb(Vector3f var0, Vector3f var1, float var2, Transform var3, Vector3f var4, Vector3f var5, AABBVarSet var6, Matrix3f var7) {
      assert var0.x <= var1.x;

      assert var0.y <= var1.y;

      assert var0.z <= var1.z;

      Vector3f var8;
      (var8 = var6.localHalfExtents).sub(var1, var0);
      var8.scale(0.5F);
      var8.x += var2;
      var8.y += var2;
      var8.z += var2;
      Vector3f var10;
      (var10 = var6.localCenter).add(var1, var0);
      var10.scale(0.5F);
      Matrix3f var9;
      (var9 = var6.abs_b).set(var7);
      (var1 = var6.center).set(var10);
      var3.transform(var1);
      (var10 = var6.extent).x = var9.m00 * var8.x + var9.m01 * var8.y + var9.m02 * var8.z;
      var10.y = var9.m10 * var8.x + var9.m11 * var8.y + var9.m12 * var8.z;
      var10.z = var9.m20 * var8.x + var9.m21 * var8.y + var9.m22 * var8.z;
      var4.sub(var1, var10);
      var5.add(var1, var10);
   }

   public SegmentController getSegmentController() {
      return this.segmentBuffer.getSegmentController();
   }

   public void getAabb(Transform var1, float var2, Vector3f var3, Vector3f var4) {
      if (var2 == this.cachedMargin && this.segmentBuffer.getSegmentController().getState().getNumberOfUpdate() == this.cacheDate && var1.equals(this.cachedTransform)) {
         var3.set(this.minCached);
         var4.set(this.maxCached);
      } else {
         this.getAabbUncached(var1, var2, var3, var4, true);
      }
   }

   public void getAabb(Transform var1, Vector3f var2, Vector3f var3) {
      this.getAabb(var1, this.margin, var2, var3);
   }

   public BroadphaseNativeType getShapeType() {
      return BroadphaseNativeType.TERRAIN_SHAPE_PROXYTYPE;
   }

   public void setLocalScaling(Vector3f var1) {
      localScaling.absolute(var1);
   }

   public Vector3f getLocalScaling(Vector3f var1) {
      return localScaling;
   }

   public void calculateLocalInertia(float var1, Vector3f var2) {
      float var3 = this.segmentBuffer.getBoundingBox().max.x - this.segmentBuffer.getBoundingBox().min.x + this.margin;
      float var4 = this.segmentBuffer.getBoundingBox().max.y - this.segmentBuffer.getBoundingBox().min.y + this.margin;
      float var5 = this.segmentBuffer.getBoundingBox().max.z - this.segmentBuffer.getBoundingBox().min.z + this.margin;
      var2.set(var1 / 3.0F * (var4 * var4 + var5 * var5), var1 / 3.0F * (var3 * var3 + var5 * var5), var1 / 3.0F * (var3 * var3 + var4 * var4));
   }

   public float getMargin() {
      return this.margin;
   }

   public String getName() {
      return "CUBES_MESH";
   }

   private void setBB() {
      if (!this.segmentBuffer.getBoundingBox().isInitialized()) {
         this.min.set(0.0F, 0.0F, 0.0F);
         this.max.set(0.0F, 0.0F, 0.0F);
      } else {
         if (!this.segmentBuffer.getBoundingBox().min.equals(this.min) || !this.segmentBuffer.getBoundingBox().max.equals(this.max)) {
            this.min.set(this.segmentBuffer.getBoundingBox().min);
            this.max.set(this.segmentBuffer.getBoundingBox().max);
            if (this.min.x > this.max.x || this.min.y > this.max.y || this.min.z > this.max.z || Float.isNaN(this.min.x) || Float.isNaN(this.min.y) || Float.isNaN(this.min.z) || Float.isNaN(this.max.x) || Float.isNaN(this.max.y) || Float.isNaN(this.max.z)) {
               System.err.println("[EXCEPTION] " + this.segmentBuffer.getSegmentController().getState() + " WARNING. physics cube AABB is corrupt: " + this.segmentBuffer.getSegmentController() + "; " + this.min + "/" + this.max);
               this.min.set(0.0F, 0.0F, 0.0F);
               this.max.set(0.0F, 0.0F, 0.0F);
            }
         }

      }
   }

   public void getAabbUncached(Transform var1, Vector3f var2, Vector3f var3, boolean var4) {
      this.getAabbUncached(var1, this.margin, var2, var3, false);
   }

   public void getAabbUncached(Transform var1, float var2, Vector3f var3, Vector3f var4, boolean var5) {
      this.setBB();
      transformAabb(this.min, this.max, var2, var1, var3, var4, this.varSet);
      if (var5) {
         this.minCached.set(var3);
         this.maxCached.set(var4);
         this.cachedTransform.set(var1);
         this.cachedMargin = var2;
         this.cacheDate = this.segmentBuffer.getSegmentController().getState().getNumberOfUpdate();
      }

   }

   public void getAabbIdent(Vector3f var1, Vector3f var2) {
      this.setBB();
      var1.set(this.min);
      var2.set(this.max);
   }

   public void getSegmentAabb(Segment var1, Transform var2, Vector3f var3, Vector3f var4, Vector3f var5, Vector3f var6, AABBVarSet var7) {
      if (var1.cacheDate == this.segmentBuffer.getSegmentController().getState().getNumberOfUpdate() && Matrix4fTools.transformEquals(var2, var1.cachedTransform)) {
         var3.set(var1.cacheBBMinX, var1.cacheBBMinY, var1.cacheBBMinZ);
         var4.set(var1.cacheBBMaxX, var1.cacheBBMaxY, var1.cacheBBMaxZ);
      } else {
         if (!var1.isEmpty()) {
            SegmentData var8;
            if ((var8 = var1.getSegmentData()).getMin().x <= var8.getMax().x) {
               var5.set((float)(var1.pos.x + var8.getMin().x - 16) - 0.5F, (float)(var1.pos.y + var8.getMin().y - 16) - 0.5F, (float)(var1.pos.z + var8.getMin().z - 16) - 0.5F);
               var6.set((float)(var1.pos.x + var8.getMax().x - 16) - 0.5F, (float)(var1.pos.y + var8.getMax().y - 16) - 0.5F, (float)(var1.pos.z + var8.getMax().z - 16) - 0.5F);
               transformAabb(var5, var6, this.margin, var2, var3, var4, var7);
            } else {
               System.err.println("[CUBESHAPE] " + var8.getSegmentController().getState() + " WARNING: NON INIT SEGMENT DATA AABB REQUEST " + var8.getMin() + "; " + var8.getMax() + ": " + var8.getSegmentController() + ": RESTRUCTING AABB");
               var8.restructBB(true);
               var1.getSegmentController().getSegmentBuffer().restructBB();
               var4.set(0.0F, 0.0F, 0.0F);
               var3.set(0.0F, 0.0F, 0.0F);
            }
         } else {
            System.err.println("[CUBESHAPE] EMPTY SEGMENT DATA AABB REQUEST");
            var4.set(0.0F, 0.0F, 0.0F);
            var3.set(0.0F, 0.0F, 0.0F);
         }

         var1.cacheDate = this.segmentBuffer.getSegmentController().getState().getNumberOfUpdate();
         var1.cachedTransform[0] = var2.basis.m00;
         var1.cachedTransform[1] = var2.basis.m10;
         var1.cachedTransform[2] = var2.basis.m20;
         var1.cachedTransform[4] = var2.basis.m01;
         var1.cachedTransform[5] = var2.basis.m11;
         var1.cachedTransform[6] = var2.basis.m21;
         var1.cachedTransform[8] = var2.basis.m02;
         var1.cachedTransform[9] = var2.basis.m12;
         var1.cachedTransform[10] = var2.basis.m22;
         var1.cachedTransform[12] = var2.origin.x;
         var1.cachedTransform[13] = var2.origin.y;
         var1.cachedTransform[14] = var2.origin.z;
         var1.cacheBBMinX = var3.x;
         var1.cacheBBMinY = var3.y;
         var1.cacheBBMinZ = var3.z;
         var1.cacheBBMaxX = var4.x;
         var1.cacheBBMaxY = var4.y;
         var1.cacheBBMaxZ = var4.z;
      }
   }

   public SegmentBufferInterface getSegmentBuffer() {
      return this.segmentBuffer;
   }

   public String toString() {
      return "[CubesShape" + (this.segmentBuffer.getSegmentController().isOnServer() ? "|SER " : "|CLI ") + this.segmentBuffer.getSegmentController() + "]";
   }

   public Vector3f getHalfExtends(Transform var1) {
      Vector3f var2 = new Vector3f();
      Vector3f var3 = new Vector3f();
      var2.set(this.segmentBuffer.getBoundingBox().min);
      var3.set(this.segmentBuffer.getBoundingBox().max);
      Vector3f var4;
      (var4 = new Vector3f()).add(var2, var3);
      var4.scale(0.5F);
      (var2 = new Vector3f()).set(var4);
      var1.transform(var2);
      return var4;
   }

   public void setMargin(float var1) {
      this.margin = var1;
   }
}
