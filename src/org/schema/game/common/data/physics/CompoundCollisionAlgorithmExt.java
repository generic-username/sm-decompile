package org.schema.game.common.data.physics;

import com.bulletphysics.collision.broadphase.CollisionAlgorithm;
import com.bulletphysics.collision.broadphase.CollisionAlgorithmConstructionInfo;
import com.bulletphysics.collision.broadphase.DispatcherInfo;
import com.bulletphysics.collision.dispatch.CollisionAlgorithmCreateFunc;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.ManifoldResult;
import com.bulletphysics.collision.narrowphase.PersistentManifold;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.CompoundShape;
import com.bulletphysics.collision.shapes.CompoundShapeChild;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectArrayList;
import org.schema.common.util.linAlg.Matrix4fTools;
import org.schema.game.common.data.physics.sweepandpruneaabb.OverlappingSweepPair;

public class CompoundCollisionAlgorithmExt extends CollisionAlgorithm {
   private static ThreadLocal threadLocal = new ThreadLocal() {
      protected final CompoundCollisionVariableSet initialValue() {
         return new CompoundCollisionVariableSet();
      }
   };
   private final ObjectArrayList childCollisionAlgorithms = new ObjectArrayList();
   private final ObjectArrayList childCollisionAlgorithmsCOM = new ObjectArrayList();
   public boolean swapped;
   private CollisionObject compoundObject;
   private CollisionObject otherObject;
   private PersistentManifold manifoldPtr;
   private CompoundCollisionVariableSet v;

   public void init(CollisionAlgorithmConstructionInfo var1) {
      super.init(var1);
      this.manifoldPtr = var1.manifold;
      if (this.manifoldPtr == null) {
         this.manifoldPtr = this.dispatcher.getNewManifold(this.compoundObject, this.otherObject);
      }

      this.v = (CompoundCollisionVariableSet)threadLocal.get();
      ++this.v.instances;

      assert this.compoundObject.getCollisionShape().isCompound();

      CompoundShape var2;
      int var3 = (var2 = (CompoundShape)this.compoundObject.getCollisionShape()).getNumChildShapes();

      assert this.manifoldPtr != null;

      for(int var4 = 0; var4 < var3; ++var4) {
         CollisionShape var5 = this.compoundObject.getCollisionShape();
         CollisionShape var6 = var2.getChildShape(var4);
         this.compoundObject.internalSetTemporaryCollisionShape(var6);
         if (this.otherObject.getCollisionShape().isCompound()) {
            CompoundShape var10 = (CompoundShape)this.otherObject.getCollisionShape();

            for(int var7 = 0; var7 < var10.getNumChildShapes(); ++var7) {
               CollisionShape var8 = this.otherObject.getCollisionShape();
               CollisionShape var9 = var10.getChildShape(var7);
               this.otherObject.internalSetTemporaryCollisionShape(var9);
               CollisionAlgorithm var11 = var1.dispatcher1.findAlgorithm(this.compoundObject, this.otherObject, this.manifoldPtr);

               assert var11 != null : this.compoundObject + " ---------- " + this.otherObject;

               this.childCollisionAlgorithmsCOM.add(var11);
               this.otherObject.internalSetTemporaryCollisionShape(var8);
            }
         } else {
            this.childCollisionAlgorithms.add(var1.dispatcher1.findAlgorithm(this.compoundObject, this.otherObject, this.manifoldPtr));
         }

         this.compoundObject.internalSetTemporaryCollisionShape(var5);
      }

   }

   public void destroy() {
      if (this.manifoldPtr != null) {
         this.dispatcher.releaseManifold(this.manifoldPtr);
         this.manifoldPtr = null;
      } else {
         assert false : this.compoundObject;
      }

      int var1 = this.childCollisionAlgorithms.size();

      int var2;
      for(var2 = 0; var2 < var1; ++var2) {
         ((CollisionAlgorithm)this.childCollisionAlgorithms.get(var2)).destroy();
         this.dispatcher.freeCollisionAlgorithm((CollisionAlgorithm)this.childCollisionAlgorithms.getQuick(var2));
      }

      this.childCollisionAlgorithms.clear();
      var1 = this.childCollisionAlgorithmsCOM.size();

      for(var2 = 0; var2 < var1; ++var2) {
         this.dispatcher.freeCollisionAlgorithm((CollisionAlgorithm)this.childCollisionAlgorithmsCOM.getQuick(var2));
      }

      this.childCollisionAlgorithmsCOM.clear();
      this.compoundObject = null;
      this.otherObject = null;
      this.childCollisionAlgorithms.clear();
      this.childCollisionAlgorithmsCOM.clear();
      --this.v.instances;
   }

   public void processCollision(CollisionObject var1, CollisionObject var2, DispatcherInfo var3, ManifoldResult var4) {
      CollisionObject var5 = this.compoundObject;
      if (var1 == this.otherObject && var2 == var5) {
         var1 = var5;
         var2 = this.otherObject;
      }

      if (var1 != var5 || var2 != this.otherObject) {
         System.err.println("COMPOUND ALGORITHM MULTIUSE ?!?!\n---> " + var1 + ";         " + var5 + "\n---> " + var2 + ";         " + this.otherObject);
      }

      assert !(var1 instanceof RigidBodySegmentController) || !((RigidBodySegmentController)var1).isCollisionException() : var1 + " -> " + var2;

      assert !(var2 instanceof RigidBodySegmentController) || !((RigidBodySegmentController)var2).isCollisionException() : var2 + " -> " + var1;

      assert var1 == var5 : var1 + "; " + var5;

      assert var2 == this.otherObject : var2 + "; " + this.otherObject;

      if (this.manifoldPtr == null) {
         this.manifoldPtr = this.dispatcher.getNewManifold(var1, var2);
      }

      if (this.manifoldPtr.getBody0() != var1 || this.manifoldPtr.getBody1() != var2) {
         this.dispatcher.releaseManifold(this.manifoldPtr);
         System.err.println("[COMPOUNDECOLLISION] Exception: wrong manifold: \n----> " + this.manifoldPtr.getBody0() + " != " + var1 + " or \n----> " + this.manifoldPtr.getBody1() + " != " + var2);
         ObjectArrayList var6 = ((RigidBodySegmentController)var5).getSegmentController().getPhysics().getDynamicsWorld().getCollisionObjectArray();

         for(int var7 = 0; var7 < var6.size(); ++var7) {
            System.err.println("OBJECTS LISTED " + var6.getQuick(var7));
         }

         this.manifoldPtr = this.dispatcher.getNewManifold(var1, var2);
      }

      var4.setPersistentManifold(this.manifoldPtr);

      assert var5 != null;

      assert var5.getCollisionShape() != null;

      assert var5.getCollisionShape().isCompound() : var5 + "; other " + this.otherObject;

      if (!(var5.getCollisionShape() instanceof CompoundShape)) {
         System.err.println("ERROR: obj: " + var5 + "; other: " + this.otherObject);
      }

      CompoundShape var25 = (CompoundShape)var5.getCollisionShape();
      CollisionShape var12;
      CollisionShape var13;
      Transform var10000;
      Transform var16;
      Transform var17;
      CollisionShape var20;
      CollisionAlgorithm var33;
      if (var1.getCollisionShape() instanceof CubesCompoundShape && var2.getCollisionShape() instanceof CubesCompoundShape) {
         var5.getWorldTransform(this.v.tmpTrans0);
         this.otherObject.getWorldTransform(this.v.tmpTrans1);
         this.v.sweeper.debug = !((GamePhysicsObject)var5).getSimpleTransformableSendableObject().isOnServer();
         this.v.sweeper.fill((CubesCompoundShape)var1.getCollisionShape(), this.v.tmpTrans0, (CubesCompoundShape)var2.getCollisionShape(), this.v.tmpTrans1, ((CubesCompoundShape)var1.getCollisionShape()).getChildList(), ((CubesCompoundShape)var2.getCollisionShape()).getChildList());
         this.v.sweeper.getOverlapping();
         it.unimi.dsi.fastutil.objects.ObjectArrayList var27;
         int var22 = (var27 = this.v.sweeper.pairs).size();

         for(int var24 = 0; var24 < var22; ++var24) {
            OverlappingSweepPair var28 = (OverlappingSweepPair)var27.get(var24);
            var10000 = this.v.tmpTrans;
            Transform var29 = this.v.orgTrans;
            Transform var30 = this.v.chieldTrans;
            Transform var31 = this.v.interpolationTrans;
            Transform var32 = this.v.newChildWorldTrans;
            var13 = ((CompoundShapeChild)var28.a.seg).childShape;
            var5.getWorldTransform(var29);
            var5.getInterpolationWorldTransform(var31);
            var25.getChildTransform(((CubesCompoundShapeChild)var28.a.seg).tmpChildIndex, var30);
            var32.set(var29);
            Matrix4fTools.transformMul(var32, var30);
            var5.setWorldTransform(var32);
            var5.setInterpolationWorldTransform(var32);
            var12 = var5.getCollisionShape();
            var5.internalSetTemporaryCollisionShape(var13);
            var33 = (CollisionAlgorithm)this.childCollisionAlgorithmsCOM.getQuick(((CubesCompoundShapeChild)var28.b.seg).tmpChildIndex);
            var10000 = this.v.tmpTransO;
            Transform var35 = this.v.orgTransO;
            var30 = this.v.chieldTransO;
            var16 = this.v.interpolationTransO;
            var17 = this.v.newChildWorldTransO;
            CollisionShape var37 = ((CompoundShapeChild)var28.b.seg).childShape;
            this.otherObject.getWorldTransform(var35);
            this.otherObject.getInterpolationWorldTransform(var16);
            ((CompoundShape)this.otherObject.getCollisionShape()).getChildTransform(((CubesCompoundShapeChild)var28.b.seg).tmpChildIndex, var30);
            var17.set(var35);
            Matrix4fTools.transformMul(var17, var30);
            this.otherObject.setWorldTransform(var17);
            this.otherObject.setInterpolationWorldTransform(var17);
            var20 = this.otherObject.getCollisionShape();
            this.otherObject.internalSetTemporaryCollisionShape(var37);
            CollisionObject var38 = this.otherObject;

            assert var33 != null : var5 + " -----VS---- " + var38;

            var33.processCollision(var5, var38, var3, var4);
            this.otherObject.internalSetTemporaryCollisionShape(var20);
            this.otherObject.setWorldTransform(var35);
            this.otherObject.setInterpolationWorldTransform(var16);
            var5.internalSetTemporaryCollisionShape(var12);
            var5.setWorldTransform(var29);
            var5.setInterpolationWorldTransform(var31);
            var4.refreshContactPoints();
         }

      } else {
         var10000 = this.v.tmpTrans;
         Transform var21 = this.v.orgTrans;
         Transform var23 = this.v.chieldTrans;
         Transform var8 = this.v.interpolationTrans;
         Transform var26 = this.v.newChildWorldTrans;
         int var9 = var25.getNumChildShapes();
         int var11 = 0;

         for(int var10 = 0; var10 < var9; ++var10) {
            var13 = var25.getChildShape(var10);
            var5.getWorldTransform(var21);
            var5.getInterpolationWorldTransform(var8);
            var25.getChildTransform(var10, var23);
            var26.set(var21);
            Matrix4fTools.transformMul(var26, var23);
            var5.setWorldTransform(var26);
            var5.setInterpolationWorldTransform(var26);
            var12 = var5.getCollisionShape();
            var5.internalSetTemporaryCollisionShape(var13);
            if (this.otherObject.getCollisionShape().isCompound()) {
               CompoundShape var34 = (CompoundShape)this.otherObject.getCollisionShape();

               for(int var14 = 0; var14 < var34.getNumChildShapes(); ++var14) {
                  assert var11 < this.childCollisionAlgorithmsCOM.size() : var5 + " ---- " + this.otherObject + "  ->>>>>>>>> " + this.childCollisionAlgorithmsCOM;

                  CollisionAlgorithm var15 = (CollisionAlgorithm)this.childCollisionAlgorithmsCOM.getQuick(var11);
                  var10000 = this.v.tmpTransO;
                  var16 = this.v.orgTransO;
                  var17 = this.v.chieldTransO;
                  Transform var18 = this.v.interpolationTransO;
                  Transform var19 = this.v.newChildWorldTransO;
                  var20 = var34.getChildShape(var14);
                  this.otherObject.getWorldTransform(var16);
                  this.otherObject.getInterpolationWorldTransform(var18);
                  var34.getChildTransform(var14, var17);
                  var19.set(var16);
                  Matrix4fTools.transformMul(var19, var17);
                  this.otherObject.setWorldTransform(var19);
                  this.otherObject.setInterpolationWorldTransform(var19);
                  CollisionShape var36 = this.otherObject.getCollisionShape();
                  this.otherObject.internalSetTemporaryCollisionShape(var20);
                  CollisionObject var39 = this.otherObject;

                  assert var15 != null : var5 + " -----VS---- " + var39;

                  var15.processCollision(var5, var39, var3, var4);
                  this.otherObject.internalSetTemporaryCollisionShape(var36);
                  this.otherObject.setWorldTransform(var16);
                  this.otherObject.setInterpolationWorldTransform(var18);
                  ++var11;
               }
            } else {
               var33 = (CollisionAlgorithm)this.childCollisionAlgorithms.getQuick(var10);
               if (var5.getCollisionShape() instanceof CubeShape && this.otherObject.getCollisionShape() instanceof CubeShape) {
                  this.dispatcher.findAlgorithm(var5, this.otherObject, this.manifoldPtr).processCollision(var5, this.otherObject, var3, var4);
               } else {
                  var33.processCollision(var5, this.otherObject, var3, var4);
               }
            }

            var5.internalSetTemporaryCollisionShape(var12);
            var5.setWorldTransform(var21);
            var5.setInterpolationWorldTransform(var8);
            var4.refreshContactPoints();
         }

      }
   }

   public float calculateTimeOfImpact(CollisionObject var1, CollisionObject var2, DispatcherInfo var3, ManifoldResult var4) {
      assert this.compoundObject.getCollisionShape().isCompound();

      CompoundShape var13 = (CompoundShape)this.compoundObject.getCollisionShape();
      Transform var14 = this.v.tmpTrans;
      Transform var5 = this.v.orgTrans;
      Transform var6 = this.v.chieldTrans;
      float var7 = 1.0F;
      int var8 = this.childCollisionAlgorithms.size();

      for(int var9 = 0; var9 < var8; ++var9) {
         CollisionShape var10 = var13.getChildShape(var9);
         this.compoundObject.getWorldTransform(var5);
         var13.getChildTransform(var9, var6);
         var14.set(var5);
         var14.mul(var6);
         this.compoundObject.setWorldTransform(var14);
         CollisionShape var11 = this.compoundObject.getCollisionShape();
         this.compoundObject.internalSetTemporaryCollisionShape(var10);
         float var12;
         if ((var12 = ((CollisionAlgorithm)this.childCollisionAlgorithms.getQuick(var9)).calculateTimeOfImpact(this.compoundObject, this.otherObject, var3, var4)) < var7) {
            var7 = var12;
         }

         this.compoundObject.internalSetTemporaryCollisionShape(var11);
         this.compoundObject.setWorldTransform(var5);
      }

      return var7;
   }

   public void getAllContactManifolds(ObjectArrayList var1) {
      for(int var2 = 0; var2 < this.childCollisionAlgorithms.size(); ++var2) {
         ((CollisionAlgorithm)this.childCollisionAlgorithms.getQuick(var2)).getAllContactManifolds(var1);
      }

   }

   public String toString() {
      return "CompoundAlgo[" + this.compoundObject + "->" + this.otherObject + "]";
   }

   public static class SwappedCreateFunc extends CollisionAlgorithmCreateFunc {
      private final com.bulletphysics.util.ObjectPool pool = com.bulletphysics.util.ObjectPool.get(CompoundCollisionAlgorithmExt.class);

      public CollisionAlgorithm createCollisionAlgorithm(CollisionAlgorithmConstructionInfo var1, CollisionObject var2, CollisionObject var3) {
         CompoundCollisionAlgorithmExt var4;
         (var4 = new CompoundCollisionAlgorithmExt()).compoundObject = var3;
         var4.otherObject = var2;
         var4.init(var1);
         var4.swapped = true;
         return var4;
      }

      public void releaseCollisionAlgorithm(CollisionAlgorithm var1) {
      }
   }

   public static class CreateFunc extends CollisionAlgorithmCreateFunc {
      public CollisionAlgorithm createCollisionAlgorithm(CollisionAlgorithmConstructionInfo var1, CollisionObject var2, CollisionObject var3) {
         CompoundCollisionAlgorithmExt var4;
         (var4 = new CompoundCollisionAlgorithmExt()).compoundObject = var2;
         var4.otherObject = var3;
         var4.init(var1);
         return var4;
      }

      public void releaseCollisionAlgorithm(CollisionAlgorithm var1) {
      }
   }
}
