package org.schema.game.common.data.physics;

public class Pair {
   public Object a;
   public Object b;

   public Pair(Object var1, Object var2) {
      this.a = var1;
      this.b = var2;
   }

   public boolean contains(Object var1) {
      return this.a.equals(var1) || this.b.equals(var1);
   }

   public void set(Object var1, Object var2) {
      this.a = var1;
      this.b = var2;
   }

   public int hashCode() {
      return this.a.hashCode() + this.b.hashCode();
   }

   public boolean equals(Object var1) {
      Pair var2 = (Pair)var1;
      return this.a.equals(var2.a) && this.b.equals(var2.b);
   }

   public String toString() {
      return "Pair(" + this.a + ", " + this.b + ")";
   }
}
