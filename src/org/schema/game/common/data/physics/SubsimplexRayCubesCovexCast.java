package org.schema.game.common.data.physics;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.narrowphase.ConvexCast;
import com.bulletphysics.collision.narrowphase.SimplexSolverInterface;
import com.bulletphysics.collision.narrowphase.ConvexCast.CastResult;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.game.common.data.physics.octree.ArrayOctree;
import org.schema.schine.network.server.ServerStateInterface;
import org.schema.schine.physics.ClosestRayCastResultExt;

public class SubsimplexRayCubesCovexCast extends ConvexCast {
   private static ThreadLocal threadLocal = new ThreadLocal() {
      protected final CubeRayVariableSet initialValue() {
         return new CubeRayVariableSet();
      }
   };
   public boolean debug = false;
   com.bulletphysics.util.ObjectPool pool = com.bulletphysics.util.ObjectPool.get(Vector3b.class);
   com.bulletphysics.util.ObjectPool aabbpool = com.bulletphysics.util.ObjectPool.get(AABBb.class);
   OuterSegmentIterator outerSegmentIterator;
   int hitboxes = 0;
   int casts = 0;
   private ClosestRayCastResultExt rayResult;
   private CubeRayVariableSet v;

   public SubsimplexRayCubesCovexCast() {
   }

   public SubsimplexRayCubesCovexCast(ConvexShape var1, CollisionObject var2, SimplexSolverInterface var3, ClosestRayCastResultExt var4) {
      this.v = (CubeRayVariableSet)threadLocal.get();
      this.outerSegmentIterator = new OuterSegmentIterator(this.v);
      this.v.shapeA = var1;
      this.v.cubesB = (CubeShape)var2.getCollisionShape();
      this.v.cubesCollisionObject = var2;
      this.v.simplexSolver = var3;
      this.rayResult = var4;
      if (this.debug) {
         System.err.println("DEBUG INST: RECORD ALL BLOCKS: " + this.rayResult.isRecordAllBlocks());
      }

      this.v.box0.setMargin(0.15F);
   }

   public void init(ConvexShape var1, CollisionObject var2, SimplexSolverInterface var3, ClosestRayCastResultExt var4) {
      this.v = (CubeRayVariableSet)threadLocal.get();
      this.outerSegmentIterator = new OuterSegmentIterator(this.v);
      this.v.shapeA = var1;
      this.v.cubesB = (CubeShape)var2.getCollisionShape();
      this.v.cubesCollisionObject = var2;
      this.v.simplexSolver = var3;
      this.rayResult = var4;
      if (this.debug) {
         System.err.println("DEBUG INIT: RECORD ALL BLOCKS: " + this.rayResult.isRecordAllBlocks());
      }

      this.v.box0.setMargin(0.15F);
   }

   public boolean calcTimeOfImpact(Transform var1, Transform var2, Transform var3, Transform var4, CastResult var5) {
      if (this.rayResult.isDamageTest() && !this.v.cubesB.getSegmentController().isPhysicalForDamage()) {
         return false;
      } else {
         this.debug = this.rayResult.isDebug();
         if (this.debug) {
            System.err.println("[PHYSICS] CHECKING START: " + this.v.cubesB.getSegmentBuffer().getSegmentController());
         }

         if (this.rayResult instanceof CubeRayCastResult && ((CubeRayCastResult)this.rayResult).isFiltered(this.v.cubesB.getSegmentBuffer().getSegmentController())) {
            return false;
         } else {
            if (this.rayResult.isHasCollidingBlockFilter() && !this.rayResult.getCollidingBlocks().containsKey(this.v.cubesB.getSegmentBuffer().getSegmentController().getId())) {
               this.rayResult.getCollidingBlocks().put(this.v.cubesB.getSegmentBuffer().getSegmentController().getId(), new LongOpenHashSet());
            }

            if (this.rayResult.isRecordAllBlocks()) {
               assert this.rayResult.getRecordedBlocks() != null;

               BlockRecorder var6 = this.v.getBlockRecorder();
               this.rayResult.getRecordedBlocks().put(this.v.cubesB.getSegmentBuffer().getSegmentController().getId(), var6);
               this.v.record = var6;
               this.v.recordAmount = this.rayResult.getBlockDeepness();
            } else {
               this.v.record = null;
            }

            this.v.cubesB.getAabb(var3, this.v.outMin, this.v.outMax);
            this.v.hitLambda[0] = 1.0F;
            this.v.normal.set(0.0F, 0.0F, 0.0F);
            this.v.from.set(var1);
            this.v.to.set(var2);
            this.outerSegmentIterator.hit = false;
            if (this.v.oSet == null) {
               this.v.oSet = ArrayOctree.getSet(this.v.cubesB.getSegmentBuffer().getSegmentController().getState() instanceof ServerStateInterface);
            } else {
               assert this.v.oSet == ArrayOctree.getSet(this.v.cubesB.getSegmentBuffer().getSegmentController().isOnServer());
            }

            this.v.absolute.set(var3.basis);
            MatrixUtil.absolute(this.v.absolute);
            this.outerSegmentIterator.fromA = this.v.from;
            this.outerSegmentIterator.hitSignal = false;
            this.outerSegmentIterator.result = var5;
            this.outerSegmentIterator.debug = this.debug;
            this.outerSegmentIterator.rayResult = this.rayResult;
            this.outerSegmentIterator.testCubes = var3;
            this.outerSegmentIterator.toA = this.v.to;
            this.outerSegmentIterator.toCubes = var4;
            this.v.solve.initializeSegmentGranularity(this.v.from.origin, this.v.to.origin, var3);
            if (!this.v.solve.ok) {
               System.err.println("[PHYSICS] SegmentTrav " + this.v.cubesB.getSegmentBuffer().getSegmentController().getState() + " " + this.v.from.origin + " -> " + this.v.to.origin + " for cubes: " + var3.origin + ": " + this.v.cubesB.getSegmentBuffer().getSegmentController());
            }

            this.outerSegmentIterator.segmentController = this.v.cubesB.getSegmentBuffer().getSegmentController();
            this.v.solve.traverseSegmentsOnRay(this.outerSegmentIterator);
            this.outerSegmentIterator.segmentController = null;
            if (this.outerSegmentIterator.hitSignal) {
               if (this.debug) {
                  System.err.println("[PHYSICS] CHECKING HIT: " + this.v.cubesB.getSegmentBuffer().getSegmentController() + " -> " + this.rayResult.hasHit() + "; " + this.rayResult.getSegment());
               }

               return true;
            } else {
               if (this.debug) {
                  System.err.println("[PHYSICS] CHECKING: " + this.v.cubesB.getSegmentBuffer().getSegmentController() + " -> HIT: " + this.rayResult.hasHit() + "; " + this.rayResult.getSegment());
               }

               return this.outerSegmentIterator.hit;
            }
         }
      }
   }
}
