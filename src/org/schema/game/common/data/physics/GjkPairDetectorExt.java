package org.schema.game.common.data.physics;

import com.bulletphysics.BulletGlobals;
import com.bulletphysics.BulletStats;
import com.bulletphysics.collision.dispatch.ManifoldResult;
import com.bulletphysics.collision.narrowphase.ConvexPenetrationDepthSolver;
import com.bulletphysics.collision.narrowphase.DiscreteCollisionDetectorInterface;
import com.bulletphysics.collision.narrowphase.SimplexSolverInterface;
import com.bulletphysics.collision.narrowphase.DiscreteCollisionDetectorInterface.ClosestPointInput;
import com.bulletphysics.collision.narrowphase.DiscreteCollisionDetectorInterface.Result;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.linearmath.IDebugDraw;
import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;

public class GjkPairDetectorExt extends DiscreteCollisionDetectorInterface {
   private static final float REL_ERROR2 = 1.0E-6F;
   private final Vector3f cachedSeparatingAxis = new Vector3f();
   public ObjectArrayList log;
   public int contacts;
   public int lastUsedMethod;
   public int curIter;
   public int degenerateSimplex;
   public int catchDegeneracies;
   public boolean fastContactOnly;
   public float maxDepth;
   private ConvexPenetrationDepthSolver penetrationDepthSolver;
   private SimplexSolverInterface simplexSolver;
   private ConvexShape minkowskiA;
   private ConvexShape minkowskiB;
   private boolean ignoreMargin;
   private GjkPairDetectorVariables v;
   public boolean useExtraPenetrationCheck = false;

   public GjkPairDetectorExt(GjkPairDetectorVariables var1) {
      this.v = var1;
   }

   public void getClosestPoints(ClosestPointInput var1, Result var2, IDebugDraw var3, boolean var4) {
      this.contacts = 0;
      this.maxDepth = 0.0F;
      Vector3f var31 = this.v.tmp;
      float var5 = 0.0F;
      Vector3f var6;
      (var6 = this.v.normalInB).set(0.0F, 0.0F, 0.0F);
      Vector3f var7 = this.v.pointOnA;
      Vector3f var8 = this.v.pointOnB;
      Transform var9 = this.v.localTransA;
      Transform var10 = this.v.localTransB;
      var9.set(var1.transformA);
      var10.set(var1.transformB);
      Vector3f var11;
      (var11 = this.v.positionOffset).add(var9.origin, var10.origin);
      var11.scale(0.5F);
      var9.origin.sub(var11);
      var10.origin.sub(var11);
      float var12 = this.minkowskiA.getMargin();
      float var13 = this.minkowskiB.getMargin();
      ++BulletStats.gNumGjkChecks;
      if (this.ignoreMargin) {
         var12 = 0.0F;
         var13 = 0.0F;
      }

      this.curIter = 0;
      this.cachedSeparatingAxis.set(0.0F, 1.0F, 0.0F);
      boolean var14 = false;
      boolean var15 = false;
      boolean var16 = true;
      this.degenerateSimplex = 0;
      this.lastUsedMethod = -1;
      float var17 = Float.MAX_VALUE;
      float var19 = var12 + var13;
      this.simplexSolver.reset();
      Vector3f var20 = this.v.seperatingAxisInA;
      Vector3f var21 = this.v.seperatingAxisInB;
      Vector3f var22 = this.v.pInA;
      Vector3f var23 = this.v.qInB;
      Vector3f var24 = this.v.pWorld;
      Vector3f var25 = this.v.qWorld;
      Vector3f var26 = this.v.w;
      Vector3f var27 = this.v.tmpPointOnA;
      Vector3f var28 = this.v.tmpPointOnB;
      Vector3f var29 = this.v.tmpNormalInB;

      float var18;
      float var30;
      while(true) {
         var20.negate(this.cachedSeparatingAxis);
         MatrixUtil.transposeTransform(var20, var20, var1.transformA.basis);
         var21.set(this.cachedSeparatingAxis);
         MatrixUtil.transposeTransform(var21, var21, var1.transformB.basis);
         this.minkowskiA.localGetSupportingVertexWithoutMargin(var20, var22);
         this.minkowskiB.localGetSupportingVertexWithoutMargin(var21, var23);
         var24.set(var22);
         var9.transform(var24);
         var25.set(var23);
         var10.transform(var25);
         var26.sub(var24, var25);
         var18 = this.cachedSeparatingAxis.dot(var26);
         if (this.useExtraPenetrationCheck) {
            if (var18 > 0.0F && var18 * var18 > var17 * var1.maximumDistanceSquared) {
               this.degenerateSimplex = 10;
               var16 = true;
               break;
            }
         } else if (var18 > 0.0F && var18 * var18 > var17 * var1.maximumDistanceSquared) {
            var16 = false;
            break;
         }

         if (this.simplexSolver.inSimplex(var26)) {
            this.degenerateSimplex = 1;
            var15 = true;
            break;
         }

         var18 = var17 - var18;
         var30 = var17 * 1.0E-6F;
         if (var18 <= var30) {
            if (var18 <= 0.0F) {
               this.degenerateSimplex = 2;
            } else {
               this.degenerateSimplex = 11;
            }

            var15 = true;
            break;
         }

         this.simplexSolver.addVertex(var26, var24, var25);
         if (!this.simplexSolver.closest(this.cachedSeparatingAxis)) {
            this.degenerateSimplex = 3;
            var15 = true;
            break;
         }

         if (this.cachedSeparatingAxis.lengthSquared() < 1.0E-6F) {
            this.degenerateSimplex = 6;
            var15 = true;
            break;
         }

         var18 = var17;
         var17 = this.cachedSeparatingAxis.lengthSquared();
         if (var18 - var17 <= 1.1920929E-7F * var18) {
            this.simplexSolver.backup_closest(this.cachedSeparatingAxis);
            this.degenerateSimplex = 12;
            var15 = true;
            break;
         }

         if (this.curIter++ > 1000) {
            break;
         }

         if (this.simplexSolver.fullSimplex()) {
            this.degenerateSimplex = 13;
            this.simplexSolver.backup_closest(this.cachedSeparatingAxis);
            break;
         }
      }

      if (var15) {
         this.simplexSolver.compute_points(var7, var8);
         var6.sub(var7, var8);
         if ((var18 = this.cachedSeparatingAxis.lengthSquared()) < 1.0E-4F) {
            this.degenerateSimplex = 5;
         }

         if (var18 > 1.4210855E-14F) {
            var30 = FastMath.carmackInvSqrt(var18);
            var6.scale(var30);
            var18 = FastMath.carmackInvSqrt(var17);

            assert var18 > 0.0F;

            var31.scale(var12 * var18, this.cachedSeparatingAxis);
            var7.sub(var31);
            var31.scale(var13 * var18, this.cachedSeparatingAxis);
            var8.add(var31);
            var5 = 1.0F / var30 - var19;
            var14 = true;
            this.lastUsedMethod = 1;
         } else {
            this.lastUsedMethod = 2;
         }
      }

      boolean var32 = this.catchDegeneracies != 0 && this.penetrationDepthSolver != null && this.degenerateSimplex != 0 && var5 + var19 < 0.01F;
      if (var16 && (!var14 || var32) && this.penetrationDepthSolver != null) {
         ++BulletStats.gNumDeepPenetrationChecks;
         if (this.penetrationDepthSolver.calcPenDepth(this.simplexSolver, this.minkowskiA, this.minkowskiB, var9, var10, this.cachedSeparatingAxis, var27, var28, var3)) {
            var29.sub(var28, var27);
            if ((var18 = var29.lengthSquared()) > 1.4210855E-14F) {
               var29.scale(FastMath.carmackInvSqrt(var18));
               var31.sub(var27, var28);
               var18 = -var31.length();
               if (!var14 || var18 < var5) {
                  var5 = var18;
                  var7.set(var27);
                  var8.set(var28);
                  var6.set(var29);
                  var14 = true;
                  this.lastUsedMethod = 3;
               }
            } else {
               this.lastUsedMethod = 4;
            }
         } else {
            this.lastUsedMethod = 5;
         }
      }

      if (var14) {
         ++this.contacts;
         if (this.fastContactOnly) {
            return;
         }

         var31.add(var8, var11);
         var2.addContactPoint(var6, var31, var5);
         this.maxDepth = Math.max(var5, this.maxDepth);
      }

   }

   public void getClosestPoints(ClosestPointInput var1, Result var2, IDebugDraw var3, boolean var4, Vector3f var5, Vector3f var6, short var7, short var8, int var9, int var10) {
      this.contacts = 0;
      this.maxDepth = 0.0F;
      Vector3f var37 = this.v.tmp;
      float var11 = 0.0F;
      Vector3f var12;
      (var12 = this.v.normalInB).set(0.0F, 0.0F, 0.0F);
      Vector3f var13 = this.v.pointOnA;
      Vector3f var14 = this.v.pointOnB;
      Transform var15 = this.v.localTransA;
      Transform var16 = this.v.localTransB;
      var15.set(var1.transformA);
      var16.set(var1.transformB);
      Vector3f var17;
      (var17 = this.v.positionOffset).add(var15.origin, var16.origin);
      var17.scale(0.5F);
      var15.origin.sub(var17);
      var16.origin.sub(var17);
      float var18 = this.minkowskiA.getMargin();
      float var19 = this.minkowskiB.getMargin();
      ++BulletStats.gNumGjkChecks;
      if (this.ignoreMargin) {
         var18 = 0.0F;
         var19 = 0.0F;
      }

      this.curIter = 0;
      this.cachedSeparatingAxis.set(0.0F, 1.0F, 0.0F);
      boolean var20 = false;
      boolean var21 = false;
      boolean var22 = true;
      this.degenerateSimplex = 0;
      this.lastUsedMethod = -1;
      float var23 = Float.MAX_VALUE;
      float var25 = var18 + var19;
      this.simplexSolver.reset();
      Vector3f var26 = this.v.seperatingAxisInA;
      Vector3f var27 = this.v.seperatingAxisInB;
      Vector3f var28 = this.v.pInA;
      Vector3f var29 = this.v.qInB;
      Vector3f var30 = this.v.pWorld;
      Vector3f var31 = this.v.qWorld;
      Vector3f var32 = this.v.w;
      Vector3f var33 = this.v.tmpPointOnA;
      Vector3f var34 = this.v.tmpPointOnB;
      Vector3f var35 = this.v.tmpNormalInB;

      float var24;
      float var36;
      while(true) {
         var26.negate(this.cachedSeparatingAxis);
         MatrixUtil.transposeTransform(var26, var26, var1.transformA.basis);
         var27.set(this.cachedSeparatingAxis);
         MatrixUtil.transposeTransform(var27, var27, var1.transformB.basis);
         this.minkowskiA.localGetSupportingVertexWithoutMargin(var26, var28);
         this.minkowskiB.localGetSupportingVertexWithoutMargin(var27, var29);
         var30.set(var28);
         var15.transform(var30);
         var31.set(var29);
         var16.transform(var31);
         var32.sub(var30, var31);
         var24 = this.cachedSeparatingAxis.dot(var32);
         if (this.useExtraPenetrationCheck) {
            if (var24 > 0.0F && var24 * var24 > var23 * var1.maximumDistanceSquared) {
               this.degenerateSimplex = 10;
               var22 = true;
               break;
            }
         } else if (var24 > 0.0F && var24 * var24 > var23 * var1.maximumDistanceSquared) {
            var22 = false;
            break;
         }

         if (this.simplexSolver.inSimplex(var32)) {
            this.degenerateSimplex = 1;
            var21 = true;
            break;
         }

         var24 = var23 - var24;
         var36 = var23 * 1.0E-6F;
         if (var24 <= var36) {
            if (var24 <= 0.0F) {
               this.degenerateSimplex = 2;
            } else {
               this.degenerateSimplex = 11;
            }

            var21 = true;
            break;
         }

         this.simplexSolver.addVertex(var32, var30, var31);
         if (!this.simplexSolver.closest(this.cachedSeparatingAxis)) {
            this.degenerateSimplex = 3;
            var21 = true;
            break;
         }

         if (this.cachedSeparatingAxis.lengthSquared() < 1.0E-6F) {
            this.degenerateSimplex = 6;
            var21 = true;
            break;
         }

         var24 = var23;
         var23 = this.cachedSeparatingAxis.lengthSquared();
         if (var24 - var23 <= 1.1920929E-7F * var24) {
            this.simplexSolver.backup_closest(this.cachedSeparatingAxis);
            this.degenerateSimplex = 12;
            var21 = true;
            break;
         }

         if (this.curIter++ > 1000) {
            break;
         }

         if (this.simplexSolver.fullSimplex()) {
            this.degenerateSimplex = 13;
            this.simplexSolver.backup_closest(this.cachedSeparatingAxis);
            break;
         }
      }

      if (var21) {
         this.simplexSolver.compute_points(var13, var14);
         var12.sub(var13, var14);
         if ((var24 = this.cachedSeparatingAxis.lengthSquared()) < 1.0E-4F) {
            this.degenerateSimplex = 5;
         }

         if (var24 > 1.4210855E-14F) {
            var36 = FastMath.carmackInvSqrt(var24);
            var12.scale(var36);
            var24 = FastMath.carmackSqrt(var23);

            assert var24 > 0.0F;

            var37.scale(var18 / var24, this.cachedSeparatingAxis);
            var13.sub(var37);
            var37.scale(var19 / var24, this.cachedSeparatingAxis);
            var14.add(var37);
            var11 = 1.0F / var36 - var25;
            var20 = true;
            this.lastUsedMethod = 1;
         } else {
            this.lastUsedMethod = 2;
         }
      }

      boolean var38 = this.catchDegeneracies != 0 && this.penetrationDepthSolver != null && this.degenerateSimplex != 0 && var11 + var25 < 0.01F;
      if (var22 && (!var20 || var38) && this.penetrationDepthSolver != null) {
         ++BulletStats.gNumDeepPenetrationChecks;
         if (this.penetrationDepthSolver.calcPenDepth(this.simplexSolver, this.minkowskiA, this.minkowskiB, var15, var16, this.cachedSeparatingAxis, var33, var34, var3)) {
            var35.sub(var34, var33);
            if ((var24 = var35.lengthSquared()) > 1.4210855E-14F) {
               var35.scale(FastMath.carmackInvSqrt(var24));
               var37.sub(var33, var34);
               var24 = -var37.length();
               if (!var20 || var24 < var11) {
                  var11 = var24;
                  var13.set(var33);
                  var14.set(var34);
                  var12.set(var35);
                  var20 = true;
                  this.lastUsedMethod = 3;
               }
            } else {
               this.lastUsedMethod = 4;
            }
         } else {
            this.lastUsedMethod = 5;
         }
      }

      if (var20) {
         if (var11 <= BulletGlobals.getContactBreakingThreshold()) {
            ++this.contacts;
         }

         if (this.fastContactOnly) {
            return;
         }

         var37.add(var14, var17);
         ((ManifoldResult)var2).addContactPoint(var12, var37, var11, (int)var5.x, (int)var5.y, (int)var5.z, (int)var6.x, (int)var6.y, (int)var6.z, var7, var8, var9, var10);
         this.maxDepth = Math.max(var11, this.maxDepth);
      }

   }

   public void init(ConvexShape var1, ConvexShape var2, SimplexSolverInterface var3, ConvexPenetrationDepthSolver var4) {
      this.cachedSeparatingAxis.set(0.0F, 0.0F, 1.0F);
      this.ignoreMargin = false;
      this.lastUsedMethod = -1;
      this.catchDegeneracies = 1;
      this.penetrationDepthSolver = var4;
      this.simplexSolver = var3;
      this.minkowskiA = var1;
      this.minkowskiB = var2;
   }

   public void setCachedSeperatingAxis(Vector3f var1) {
      this.cachedSeparatingAxis.set(var1);
   }

   public void setIgnoreMargin(boolean var1) {
      this.ignoreMargin = var1;
   }

   public void setMinkowskiA(ConvexShape var1) {
      this.minkowskiA = var1;
   }

   public void setMinkowskiB(ConvexShape var1) {
      this.minkowskiB = var1;
   }

   public void setPenetrationDepthSolver(ConvexPenetrationDepthSolver var1) {
      this.penetrationDepthSolver = var1;
   }
}
