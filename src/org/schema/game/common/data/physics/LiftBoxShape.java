package org.schema.game.common.data.physics;

import com.bulletphysics.collision.shapes.BoxShape;
import javax.vecmath.Vector3f;

public class LiftBoxShape extends BoxShape {
   public LiftBoxShape(Vector3f var1) {
      super(var1);
   }
}
