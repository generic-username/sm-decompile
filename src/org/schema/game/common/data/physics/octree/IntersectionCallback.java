package org.schema.game.common.data.physics.octree;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.game.common.data.physics.AABBVarSet;
import org.schema.game.common.data.physics.sweepandpruneaabb.SegmentAabbInterface;
import org.schema.game.common.data.world.Segment;

public class IntersectionCallback implements SegmentAabbInterface {
   public int leafCalcs;
   public int hitCount;
   public boolean initialized = false;
   public long aabbTest;
   public long aabbRetrieve;
   private Vector3f[] hits;
   private Vector3b[] range;
   private int[] mask;
   private int[] nodeIndex;

   public void addHit(Vector3f var1, Vector3f var2, byte var3, byte var4, byte var5, byte var6, byte var7, byte var8, int var9, int var10) {
      this.hits[this.hitCount << 1].set(var1);
      this.hits[(this.hitCount << 1) + 1].set(var2);
      this.range[this.hitCount << 1].set(var3, var4, var5);
      this.range[(this.hitCount << 1) + 1].set(var6, var7, var8);
      this.mask[this.hitCount] = var9;
      this.nodeIndex[this.hitCount] = var10;
      ++this.hitCount;
   }

   public void createHitCache(int var1) {
      this.hits = new Vector3f[var1 << 1];
      this.range = new Vector3b[var1 << 1];

      for(int var2 = 0; var2 < this.hits.length; ++var2) {
         this.hits[var2] = new Vector3f();
         this.range[var2] = new Vector3b();
      }

      this.mask = new int[var1];
      this.nodeIndex = new int[var1];
      this.initialized = true;
   }

   public int getNodeIndexHit(int var1) {
      return this.nodeIndex[var1];
   }

   public void getAabbOnly(int var1, Vector3f var2, Vector3f var3) {
      var2.set(this.hits[var1 << 1]);
      var3.set(this.hits[(var1 << 1) + 1]);
   }

   public int getHit(int var1, Vector3f var2, Vector3f var3, Vector3b var4, Vector3b var5) {
      var2.set(this.hits[var1 << 1]);
      var3.set(this.hits[(var1 << 1) + 1]);
      var4.set(this.range[var1 << 1]);
      var5.set(this.range[(var1 << 1) + 1]);
      return this.mask[var1];
   }

   public void reset() {
      this.hitCount = 0;
      this.leafCalcs = 0;
      this.aabbTest = 0L;
      this.aabbRetrieve = 0L;
   }

   public void getSegmentAabb(Segment var1, Transform var2, Vector3f var3, Vector3f var4, Vector3f var5, Vector3f var6, AABBVarSet var7) {
   }

   public void getAabb(Transform var1, Vector3f var2, Vector3f var3) {
   }

   public void getAabbUncached(Transform var1, Vector3f var2, Vector3f var3, boolean var4) {
   }

   public void getAabbIdent(Vector3f var1, Vector3f var2) {
   }
}
