package org.schema.game.common.data.physics.octree;

import java.util.Arrays;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.game.common.data.world.SegmentData;

public class ArrayOctreeTraverse {
   public static byte[][][] tMap = new byte[256][][];
   public static byte[][][][] absoluteMap;
   public static int[][][] tIndexMap;
   public static final byte[][][] tAABBMap = new byte[256][2][3];

   public static void create() {
      Vector3b var0 = new Vector3b();
      Vector3b var1 = new Vector3b();

      int var2;
      byte var4;
      byte var5;
      int var7;
      int var8;
      int var9;
      for(var2 = 0; var2 < 256; ++var2) {
         int var3 = 0;

         byte var6;
         for(var4 = 0; var4 < 2; ++var4) {
            for(var5 = 0; var5 < 2; ++var5) {
               for(var6 = 0; var6 < 2; ++var6) {
                  var7 = (var4 % 2 << 2) + (var5 % 2 << 1) + var6 % 2;
                  var8 = 1 << var7;
                  if ((var2 & var8) == var8) {
                     ++var3;
                  }
               }
            }
         }

         tMap[var2] = new byte[var3][];
         int var11 = 0;

         byte var13;
         for(var5 = 0; var5 < 2; ++var5) {
            for(var6 = 0; var6 < 2; ++var6) {
               for(var13 = 0; var13 < 2; ++var13) {
                  var8 = (var5 % 2 << 2) + (var6 % 2 << 1) + var13 % 2;
                  var9 = 1 << var8;
                  if ((var2 & var9) == var9) {
                     tMap[var2][var11] = new byte[3];
                     tMap[var2][var11][0] = var13;
                     tMap[var2][var11][1] = var6;
                     tMap[var2][var11][2] = var5;
                     ++var11;
                  }
               }
            }
         }

         tAABBMap[var2][0][0] = 127;
         tAABBMap[var2][0][1] = 127;
         tAABBMap[var2][0][2] = 127;
         tAABBMap[var2][1][0] = -128;
         tAABBMap[var2][1][1] = -128;
         tAABBMap[var2][1][2] = -128;

         for(int var12 = 0; var12 < var3; ++var12) {
            var6 = tMap[var2][var12][0];
            var13 = tMap[var2][var12][1];
            byte var15 = tMap[var2][var12][2];
            tAABBMap[var2][0][0] = (byte)Math.min(tAABBMap[var2][0][0], var6);
            tAABBMap[var2][0][1] = (byte)Math.min(tAABBMap[var2][0][1], var13);
            tAABBMap[var2][0][2] = (byte)Math.min(tAABBMap[var2][0][2], var15);
            tAABBMap[var2][1][0] = (byte)Math.max(tAABBMap[var2][1][0], var6 + 1);
            tAABBMap[var2][1][1] = (byte)Math.max(tAABBMap[var2][1][1], var13 + 1);
            tAABBMap[var2][1][2] = (byte)Math.max(tAABBMap[var2][1][2], var15 + 1);
         }
      }

      tIndexMap = new int[ArrayOctree.NODES][256][];
      absoluteMap = new byte[ArrayOctree.NODES][256][][];
      var2 = 0;

      for(byte var10 = 0; var10 < 32; var10 = (byte)(var10 + 2)) {
         for(var4 = 0; var4 < 32; var4 = (byte)(var4 + 2)) {
            for(var5 = 0; var5 < 32; var5 = (byte)(var5 + 2)) {
               int var14;
               ArrayOctree.getBox(var14 = ArrayOctree.getIndex(var2, 3), var0, var1);

               for(var7 = 0; var7 < 256; ++var7) {
                  tIndexMap[var14][var7] = new int[tMap[var7].length];
                  absoluteMap[var14][var7] = new byte[tMap[var7].length][3];

                  for(var8 = 0; var8 < tMap[var7].length; ++var8) {
                     absoluteMap[var14][var7][var8][0] = (byte)(var0.x + tMap[var7][var8][0] + 16);
                     absoluteMap[var14][var7][var8][1] = (byte)(var0.y + tMap[var7][var8][1] + 16);
                     absoluteMap[var14][var7][var8][2] = (byte)(var0.z + tMap[var7][var8][2] + 16);
                     var9 = SegmentData.getInfoIndex(absoluteMap[var14][var7][var8][0], absoluteMap[var14][var7][var8][1], absoluteMap[var14][var7][var8][2]);
                     tIndexMap[var14][var7][var8] = var9;
                  }
               }

               ++var2;
            }
         }
      }

      absoluteMap = null;
   }

   public static void main(String[] var0) {
      System.currentTimeMillis();

      for(int var2 = 0; var2 < 256; ++var2) {
         System.err.println("############## " + var2 + " ##############");

         for(int var1 = 0; var1 < tMap[var2].length; ++var1) {
            System.err.println(Arrays.toString(tMap[var2][var1]));
         }
      }

   }

   public static int traverseExisting(SegmentData var0, SegmentDataTraverseInterface var1) {
      return 0;
   }

   public static int traverseEmpty(SegmentData var0, SegmentDataTraverseInterface var1) {
      return 0;
   }
}
