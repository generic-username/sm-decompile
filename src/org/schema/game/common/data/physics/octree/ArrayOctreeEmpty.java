package org.schema.game.common.data.physics.octree;

public class ArrayOctreeEmpty extends ArrayOctree {
   public ArrayOctreeEmpty(boolean var1) {
      super(var1);
   }

   public void insertAABB16(byte var1, byte var2, byte var3, int var4) {
      assert false;

   }

   public void insert(byte var1, byte var2, byte var3, int var4) {
      assert false;

   }

   public void resetAABB16() {
   }

   public void delete(byte var1, byte var2, byte var3, int var4, short var5) {
      throw new RuntimeException("Cannot delete from this");
   }

   public void reset() {
   }
}
