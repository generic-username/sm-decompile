package org.schema.game.common.data.physics.octree;

public interface SegmentDataTraverseInterface {
   void handle(byte var1, byte var2, byte var3, int var4, int var5);
}
