package org.schema.game.common.data.physics.octree;

import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.linearmath.VectorUtil;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.game.common.data.Dodecahedron;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.schine.graphicsengine.forms.BoundingBox;

public class ArrayOctree {
   public static final int POSSIBLE_LEAF_HITS = 4096;
   static final int MAX_DEEPNESS = 3;
   static final int COUNT_LVLS = 4;
   static final int NODES = 4681;
   private static byte[] mod16 = new byte[32];
   static final int BUFFER_SIZE;
   private static final IntOpenHashSet testHashSet;
   static final byte[] dimBuffer;
   static final int[] indexBuffer;
   static final int[] localIndexBuffer;
   static final short[] localIndexShiftedBuffer;
   static final int[] indexBufferIndexed;
   private static OctreeVariableSet serverSet;
   private static OctreeVariableSet clientSet;
   private byte[] chunk16aabb = new byte[48];
   private static final byte[] toBB16Map;
   private final short[] sizes;
   private final boolean[] hits;
   private final OctreeVariableSet set;

   public static void main(String[] var0) {
   }

   public ArrayOctree(boolean var1) {
      this.sizes = new short[NODES];
      this.hits = new boolean[NODES];
      if (var1) {
         this.set = serverSet;
      } else {
         this.set = clientSet;
      }

      this.resetAABB16();
   }

   public void resetAABB16() {
      for(int var1 = 0; var1 < this.chunk16aabb.length; var1 += 6) {
         this.chunk16aabb[var1] = 127;
         this.chunk16aabb[var1 + 1] = 127;
         this.chunk16aabb[var1 + 2] = 127;
         this.chunk16aabb[var1 + 3] = -128;
         this.chunk16aabb[var1 + 4] = -128;
         this.chunk16aabb[var1 + 5] = -128;
      }

   }

   public static void getBox(int var0, Vector3b var1, Vector3b var2) {
      var0 *= 6;
      var1.set(dimBuffer[var0], dimBuffer[var0 + 1], dimBuffer[var0 + 2]);
      var2.set(dimBuffer[var0 + 3], dimBuffer[var0 + 4], dimBuffer[var0 + 5]);
   }

   public static int getIndex(int var0, int var1) {
      if (var1 == 0) {
         return var0 + 1;
      } else {
         int var2 = 1;
         int var3 = 8;

         for(int var4 = 0; var4 < var1; ++var4) {
            var2 += var3;
            var3 <<= 3;
         }

         return var2 + var0;
      }
   }

   public static OctreeVariableSet getSet(boolean var0) {
      return var0 ? serverSet : clientSet;
   }

   public static byte getEnd(int var0, int var1) {
      var1 *= 6;
      return dimBuffer[var1 + 3 + var0];
   }

   public static byte getStart(int var0, int var1) {
      var1 *= 6;
      return dimBuffer[var1 + var0];
   }

   public static final int getLocalIndex(byte var0, byte var1, byte var2) {
      return (var2 % 2 << 2) + (var1 % 2 << 1) + var0 % 2;
   }

   public static final int getLocalIndex(int var0) {
      return localIndexBuffer[var0];
   }

   public static void deserializeOctreeCompression(SegmentData var0, FastByteArrayInputStream var1, DataInputStream var2, long var3) throws IOException {
      long var5 = 0L;
      int var15 = var2.readShort() * 3;
      int var4 = 0;

      while(var5 < (long)var15) {
         short var7 = var2.readShort();
         int var8 = var2.readByte() & 255;
         var5 += 3L;
         long var11 = var1.position();
         var1.position((long)(var15 + 2 + var4));
         byte var9 = getStart(0, var7);
         byte var10 = getStart(1, var7);
         byte var16 = getStart(2, var7);
         byte[][] var17 = ArrayOctreeTraverse.tMap[var8];

         for(int var13 = 0; var13 < var17.length; ++var13) {
            int var14 = SegmentData.getInfoIndex((byte)(var9 + var17[var13][0] + 16), (byte)(var10 + var17[var13][1] + 16), (byte)(var16 + var17[var13][2] + 16));
            var0.readSingle(var14, var2);
            var4 += 3;
         }

         var1.position(var11);
      }

   }

   public static void deserializeOctreeCompressionInterleaf(SegmentData var0, DataInputStream var1, long var2) throws IOException {
      long var4 = 0L;

      while(var4 < var2) {
         short var6 = var1.readShort();
         int var7 = var1.readByte() & 255;
         var4 += 3L;
         byte var8 = getStart(0, var6);
         byte var9 = getStart(1, var6);
         byte var12 = getStart(2, var6);
         byte[][] var13 = ArrayOctreeTraverse.tMap[var7];

         for(int var10 = 0; var10 < var13.length; ++var10) {
            int var11 = SegmentData.getInfoIndex((byte)(var8 + var13[var10][0] + 16), (byte)(var9 + var13[var10][1] + 16), (byte)(var12 + var13[var10][2] + 16));
            var0.readSingle(var11, var1);
            var4 += 3L;
         }
      }

   }

   private void addAABB2(short var1, Vector3b var2, Vector3b var3) {
      byte[][] var4 = ArrayOctreeTraverse.tAABBMap[var1];
      var3.set(var2);
      var2.add(var4[0][0], var4[0][1], var4[0][2]);
      var3.add(var4[1][0], var4[1][1], var4[1][2]);
   }

   private IntersectionCallback doIntersectingAABB(int var1, int var2, OctreeVariableSet var3, IntersectionCallback var4, Segment var5, Transform var6, Matrix3f var7, float var8, Vector3f var9, Vector3f var10, float var11, Vector3f var12, float var13) {
      ++var4.leafCalcs;
      int var22;
      getBox(var22 = getIndex(var1, var2), var3.min, var3.max);
      if (var2 == 0) {
         this.addAABB16(var1, var3.min, var3.max);
      } else if (var2 == 3) {
         this.addAABB2(this.sizes[var22], var3.min, var3.max);
      }

      Vector3f var19 = var3.tmpMin;
      Vector3f var14 = var3.tmpMax;
      Vector3f var15 = var3.tmpMinOut;
      Vector3f var16 = var3.tmpMaxOut;
      float var17 = (float)var5.pos.x - 0.5F;
      float var18 = (float)var5.pos.y - 0.5F;
      float var21 = (float)var5.pos.z - 0.5F;
      var19.x = (float)var3.min.x + var17;
      var19.y = (float)var3.min.y + var18;
      var19.z = (float)var3.min.z + var21;
      var14.x = (float)var3.max.x + var17;
      var14.y = (float)var3.max.y + var18;
      var14.z = (float)var3.max.z + var21;
      this.transformAabb(var3, var19, var14, var7, var8, var6, var15, var16);
      boolean var20;
      if (var12 != null) {
         var3.closest.set(var12);
         Vector3fTools.clamp(var3.closest, var15, var16);
         var3.closest.sub(var12);
         var20 = var3.closest.length() < var13;
      } else {
         var20 = AabbUtil2.testAabbAgainstAabb2(var15, var16, var9, var10);
      }

      this.setHasHit(var22, var20);
      if (var20 && var2 == 3) {
         assert this.sizes[var22] > 0;

         getBox(var22, var3.min, var3.max);
         var4.addHit(var15, var16, var3.min.x, var3.min.y, var3.min.z, var3.max.x, var3.max.y, var3.max.z, this.sizes[var22], var22);
      }

      return var4;
   }

   private IntersectionCallback doIntersectingDodecahedron(int var1, int var2, OctreeVariableSet var3, IntersectionCallback var4, Segment var5, Transform var6, Matrix3f var7, float var8, Dodecahedron var9, float var10) {
      ++var4.leafCalcs;
      getBox(var1 = getIndex(var1, var2), var3.min, var3.max);
      Vector3f var16 = var3.tmpMin;
      Vector3f var11 = var3.tmpMax;
      Vector3f var12 = var3.tmpMinOut;
      Vector3f var13 = var3.tmpMaxOut;
      float var14 = (float)var5.pos.x - 0.5F;
      float var15 = (float)var5.pos.y - 0.5F;
      float var17 = (float)var5.pos.z - 0.5F;
      var16.x = (float)var3.min.x + var14;
      var16.y = (float)var3.min.y + var15;
      var16.z = (float)var3.min.z + var17;
      var11.x = (float)var3.max.x + var14;
      var11.y = (float)var3.max.y + var15;
      var11.z = (float)var3.max.z + var17;
      this.transformAabb(var3, var16, var11, var7, var8, var6, var12, var13);
      boolean var18 = var9.testAABB(var12, var13);
      this.setHasHit(var1, var18);
      if (var18 && var2 == 3) {
         assert this.sizes[var1] > 0;

         var4.addHit(var12, var13, var3.min.x, var3.min.y, var3.min.z, var3.max.x, var3.max.y, var3.max.z, this.sizes[var1], var1);
      }

      return var4;
   }

   @Deprecated
   private IntersectionCallback doIntersectingRay(int var1, int var2, OctreeVariableSet var3, IntersectionCallback var4, Transform var5, Matrix3f var6, float var7, Segment var8, Vector3f var9, Vector3f var10, float var11) {
      var1 = getIndex(var1, var2);
      ++var4.leafCalcs;
      Vector3f var20 = var3.tmpMin;
      Vector3f var12 = var3.tmpMax;
      Vector3f var13 = var3.tmpMinOut;
      Vector3f var14 = var3.tmpMaxOut;
      getBox(var1, var3.min, var3.max);
      float var15 = (float)var8.pos.x - 0.5F;
      float var16 = (float)var8.pos.y - 0.5F;
      float var19 = (float)var8.pos.z - 0.5F;
      var20.x = (float)var3.min.x + var15;
      var20.y = (float)var3.min.y + var16;
      var20.z = (float)var3.min.z + var19;
      var12.x = (float)var3.max.x + var15;
      var12.y = (float)var3.max.y + var16;
      var12.z = (float)var3.max.z + var19;
      this.transformAabb(var3, var20, var12, var6, var7, var5, var13, var14);
      var3.param[0] = 1.0F;
      var3.normal.x = 0.0F;
      var3.normal.y = 0.0F;
      var3.normal.z = 0.0F;
      boolean var17 = this.rayAabb(var9, var10, var13, var14, var3.param, var3.normal);
      boolean var18 = false;
      if (!var17) {
         var18 = BoundingBox.testPointAABB(var10, var13, var14) || BoundingBox.testPointAABB(var9, var13, var14);
      }

      var17 = var17 || var18;
      this.setHasHit(var1, var17);
      if (var17 && var2 == 3) {
         assert this.sizes[var1] > 0;

         var4.addHit(var13, var14, var3.min.x, var3.min.y, var3.min.z, var3.max.x, var3.max.y, var3.max.z, this.sizes[var1], var1);
      }

      return var4;
   }

   private IntersectionCallback findIntersectingDodecahedron(int var1, int var2, OctreeVariableSet var3, IntersectionCallback var4, Segment var5, Transform var6, Matrix3f var7, float var8, Dodecahedron var9, float var10) {
      int var11 = getIndex(var1, var2);
      if (this.sizes[var11] > 0) {
         var4 = this.doIntersectingDodecahedron(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
         if (var2 < 3 && this.isHasHit(var11)) {
            var1 <<= 3;

            for(var11 = 0; var11 < 8; ++var11) {
               var4 = this.findIntersectingDodecahedron(var1 + var11, var2 + 1, var3, var4, var5, var6, var7, var8, var9, var10);
            }
         }
      } else {
         this.setHasHit(var11, false);
      }

      return var4;
   }

   private IntersectionCallback findIntersectingAABB(int var1, int var2, OctreeVariableSet var3, IntersectionCallback var4, Segment var5, Transform var6, Matrix3f var7, float var8, Vector3f var9, Vector3f var10, float var11, Vector3f var12, float var13) {
      int var14 = getIndex(var1, var2);
      if (this.sizes[var14] > 0) {
         var4 = this.doIntersectingAABB(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var13);
         if (var2 < 3 && this.isHasHit(var14)) {
            var1 <<= 3;

            for(var14 = 0; var14 < 8; ++var14) {
               var4 = this.findIntersectingAABB(var1 + var14, var2 + 1, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var13);
            }
         }
      } else {
         this.setHasHit(var14, false);
      }

      return var4;
   }

   public void insertAABB16(byte var1, byte var2, byte var3, int var4) {
      var4 = toBB16Map[var4] * 6;
      this.chunk16aabb[var4] = (byte)Math.min(mod16[var1], this.chunk16aabb[var4]);
      this.chunk16aabb[var4 + 1] = (byte)Math.min(mod16[var2], this.chunk16aabb[var4 + 1]);
      this.chunk16aabb[var4 + 2] = (byte)Math.min(mod16[var3], this.chunk16aabb[var4 + 2]);
      this.chunk16aabb[var4 + 3] = (byte)Math.max(mod16[var1] + 1, this.chunk16aabb[var4 + 3]);
      this.chunk16aabb[var4 + 4] = (byte)Math.max(mod16[var2] + 1, this.chunk16aabb[var4 + 4]);
      this.chunk16aabb[var4 + 5] = (byte)Math.max(mod16[var3] + 1, this.chunk16aabb[var4 + 5]);
   }

   private void addAABB16(int var1, Vector3b var2, Vector3b var3) {
      var1 *= 6;
      var3.set(var2);
      var2.add(this.chunk16aabb[var1], this.chunk16aabb[var1 + 1], this.chunk16aabb[var1 + 2]);
      var3.add(this.chunk16aabb[var1 + 3], this.chunk16aabb[var1 + 4], this.chunk16aabb[var1 + 5]);
   }

   public boolean getAABB(int var1, int var2, OctreeVariableSet var3, Segment var4, Transform var5, Matrix3f var6, float var7, Vector3f var8, Vector3f var9) {
      assert var2 == 0 : "only implemented for top lvl (16)";

      var2 = var1 + 1;
      if (this.sizes[var2] > 0) {
         assert var3 != null;

         getBox(var2, var3.min, var3.max);
         this.addAABB16(var1, var3.min, var3.max);
         Vector3f var14 = var3.tmpMin;
         Vector3f var15 = var3.tmpMax;
         Vector3f var10 = var3.tmpMinOut;
         Vector3f var11 = var3.tmpMaxOut;
         float var12 = (float)var4.pos.x - 0.5F;
         float var13 = (float)var4.pos.y - 0.5F;
         float var16 = (float)var4.pos.z - 0.5F;
         var14.x = (float)var3.min.x + var12;
         var14.y = (float)var3.min.y + var13;
         var14.z = (float)var3.min.z + var16;
         var15.x = (float)var3.max.x + var12;
         var15.y = (float)var3.max.y + var13;
         var15.z = (float)var3.max.z + var16;
         this.transformAabb(var3, var14, var15, var6, var7, var5, var10, var11);
         var8.set(var10);
         var9.set(var11);
         return true;
      } else {
         return false;
      }
   }

   public IntersectionCallback findIntersectingAABB(OctreeVariableSet var1, IntersectionCallback var2, Segment var3, Transform var4, Matrix3f var5, float var6, Vector3f var7, Vector3f var8, float var9) {
      return this.findIntersectingAABB(var1, var2, var3, var4, var5, var6, var7, var8, var9, (Vector3f)null, -1.0F);
   }

   public IntersectionCallback findIntersectingAABB(OctreeVariableSet var1, IntersectionCallback var2, Segment var3, Transform var4, Matrix3f var5, float var6, Vector3f var7, Vector3f var8, float var9, Vector3f var10, float var11) {
      for(int var12 = 0; var12 < 8; ++var12) {
         var2 = this.findIntersectingAABB(var12, 0, var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11);
      }

      return var2;
   }

   public IntersectionCallback findIntersectingAABBFromFirstLvl(int var1, OctreeVariableSet var2, IntersectionCallback var3, Segment var4, Transform var5, Matrix3f var6, float var7, Vector3f var8, Vector3f var9, float var10) {
      return this.findIntersectingAABB(var1, 0, var2, var3, var4, var5, var6, var7, var8, var9, var10, (Vector3f)null, -1.0F);
   }

   public IntersectionCallback findIntersectingDodecahedron(OctreeVariableSet var1, IntersectionCallback var2, Segment var3, Transform var4, Matrix3f var5, float var6, Dodecahedron var7, float var8) {
      for(int var9 = 0; var9 < 8; ++var9) {
         var2 = this.findIntersectingDodecahedron(var9, 0, var1, var2, var3, var4, var5, var6, var7, var8);
      }

      return var2;
   }

   @Deprecated
   private IntersectionCallback findIntersectingRay(int var1, int var2, OctreeVariableSet var3, IntersectionCallback var4, Transform var5, Matrix3f var6, float var7, Segment var8, Vector3f var9, Vector3f var10, float var11) {
      int var12 = getIndex(var1, var2);
      if (this.sizes[var12] <= 0 && var2 != 0) {
         this.setHasHit(var12, false);
      } else {
         var4 = this.doIntersectingRay(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11);
         if (var2 < 3 && this.isHasHit(var12)) {
            var1 <<= 3;

            for(var12 = 0; var12 < 8; ++var12) {
               var4 = this.findIntersectingRay(var1 + var12, var2 + 1, var3, var4, var5, var6, var7, var8, var9, var10, var11);
            }
         }
      }

      return var4;
   }

   @Deprecated
   public IntersectionCallback findIntersectingRay(OctreeVariableSet var1, IntersectionCallback var2, Transform var3, Matrix3f var4, float var5, Segment var6, Vector3f var7, Vector3f var8, float var9) {
      for(int var10 = 0; var10 < 8; ++var10) {
         var2 = this.findIntersectingRay(var10, 0, var1, var2, var3, var4, var5, var6, var7, var8, var9);
      }

      return var2;
   }

   public OctreeVariableSet getSet() {
      return this.set;
   }

   public void insert(byte var1, byte var2, byte var3, int var4) {
      short var5 = ++this.sizes[ArrayOctreeGenerator.getNodeIndex(var4, 0)];
      short var7 = ++this.sizes[ArrayOctreeGenerator.getNodeIndex(var4, 1)];
      short var8 = ++this.sizes[ArrayOctreeGenerator.getNodeIndex(var4, 2)];

      assert var5 > 0;

      assert var7 > 0;

      assert var8 > 0;

      int var6 = ArrayOctreeGenerator.getNodeIndex(var4, 3);
      var7 = localIndexShiftedBuffer[var4];
      short[] var10000 = this.sizes;
      var10000[var6] |= var7;

      assert this.sizes[var6] > 0;

   }

   public void delete(byte var1, byte var2, byte var3, int var4, short var5) {
      short var6 = --this.sizes[ArrayOctreeGenerator.getNodeIndex(var4, 0)];
      short var8 = --this.sizes[ArrayOctreeGenerator.getNodeIndex(var4, 1)];
      short var10 = --this.sizes[ArrayOctreeGenerator.getNodeIndex(var4, 2)];

      assert var6 >= 0;

      assert var8 >= 0;

      assert var10 >= 0;

      int var7 = ArrayOctreeGenerator.getNodeIndex(var4, 3);
      int var9 = getLocalIndex(var4);
      var8 = (short)(1 << var9);
      short[] var10000 = this.sizes;
      var10000[var7] = (short)(var10000[var7] & ~var8);
   }

   public boolean isHasHit(int var1) {
      return this.hits[var1];
   }

   public boolean rayAabb(Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4, float[] var5, Vector3f var6) {
      Vector3f var7 = this.set.aabbHalfExtent;
      Vector3f var8 = this.set.aabbCenter;
      Vector3f var9 = this.set.source;
      Vector3f var10 = this.set.target;
      Vector3f var11 = this.set.r;
      Vector3f var12 = this.set.hitNormal;
      var7.sub(var4, var3);
      var7.scale(0.5F);
      var8.add(var4, var3);
      var8.scale(0.5F);
      var9.sub(var1, var8);
      var10.sub(var2, var8);
      int var16 = AabbUtil2.outcode(var9, var7);
      int var17 = AabbUtil2.outcode(var10, var7);
      if ((var16 & var17) == 0) {
         float var18 = 0.0F;
         float var19 = var5[0];
         var11.sub(var10, var9);
         float var20 = 1.0F;
         var12.set(0.0F, 0.0F, 0.0F);
         int var21 = 1;

         for(int var13 = 0; var13 < 2; ++var13) {
            for(int var14 = 0; var14 != 3; ++var14) {
               float var15;
               if ((var16 & var21) != 0) {
                  var15 = (-VectorUtil.getCoord(var9, var14) - VectorUtil.getCoord(var7, var14) * var20) / VectorUtil.getCoord(var11, var14);
                  if (var18 <= var15) {
                     var18 = var15;
                     var12.set(0.0F, 0.0F, 0.0F);
                     VectorUtil.setCoord(var12, var14, var20);
                  }
               } else if ((var17 & var21) != 0) {
                  var15 = (-VectorUtil.getCoord(var9, var14) - VectorUtil.getCoord(var7, var14) * var20) / VectorUtil.getCoord(var11, var14);
                  var19 = Math.min(var19, var15);
               }

               var21 <<= 1;
            }

            var20 = -1.0F;
         }

         if (var18 <= var19) {
            var5[0] = var18;
            var6.set(var12);
            return true;
         }
      }

      return false;
   }

   public void reset() {
      Arrays.fill(this.sizes, (short)0);
      this.resetAABB16();
   }

   public void setHasHit(int var1, boolean var2) {
      this.hits[var1] = var2;
   }

   private void transformAabb(OctreeVariableSet var1, Vector3f var2, Vector3f var3, Matrix3f var4, float var5, Transform var6, Vector3f var7, Vector3f var8) {
      Vector3f var9;
      (var9 = var1.localCenter).add(var3, var2);
      var9.scale(0.5F);
      Vector3f var10;
      (var10 = var1.center).set(var9);
      var6.transform(var10);
      Vector3f var12 = var1.extend;
      Vector3f var11;
      (var11 = var1.localHalfExtents).sub(var3, var2);
      var11.scale(0.5F);
      var11.x += var5;
      var11.y += var5;
      var11.z += var5;
      var12.x = var4.m00 * var11.x + var4.m01 * var11.y + var4.m02 * var11.z;
      var12.y = var4.m10 * var11.x + var4.m11 * var11.y + var4.m12 * var11.z;
      var12.z = var4.m20 * var11.x + var4.m21 * var11.y + var4.m22 * var11.z;
      var7.sub(var10, var12);
      var8.add(var10, var12);
   }

   public int traverse(SegmentData var1, SegmentDataTraverseInterface var2, boolean var3) {
      int var4 = 0;

      for(int var5 = 0; var5 < 8; ++var5) {
         var4 += this.traverse(var5, 0, var2, var1, var3);
      }

      return var4;
   }

   private int traverse(int var1, int var2, SegmentDataTraverseInterface var3, SegmentData var4, boolean var5) {
      int var6 = 0;
      int var7 = getIndex(var1, var2);
      if (this.sizes[var7] > 0) {
         if (var2 < 3) {
            var1 <<= 3;

            for(var7 = 0; var7 < 8; ++var7) {
               var6 += this.traverse(var1 + var7, var2 + 1, var3, var4, var5);
            }
         } else {
            var6 = 0 + this.traverseLeafSolid(var1, var2, var3, var4);
         }
      }

      return var6;
   }

   public int compress(SegmentData var1, DataOutputStream var2, FastByteArrayOutputStream var3) throws IOException {
      int var4 = 0;
      var3.position(2L);

      for(int var5 = 0; var5 < 8; ++var5) {
         var4 += this.compress(var5, 0, var1, var2, false);
      }

      long var8 = var3.position() - 2L;
      var3.position(0L);

      assert var8 / 3L < 32767L;

      var2.writeShort((short)((int)(var8 / 3L)));
      var3.position(var8 + 2L);

      for(int var7 = 0; var7 < 8; ++var7) {
         this.compress(var7, 0, var1, var2, true);
      }

      return var4;
   }

   private int compress(int var1, int var2, SegmentData var3, DataOutputStream var4, boolean var5) throws IOException {
      int var6 = 0;
      int var7 = getIndex(var1, var2);
      if (this.sizes[var7] > 0) {
         if (var2 < 3) {
            var1 <<= 3;

            for(var7 = 0; var7 < 8; ++var7) {
               var6 += this.compress(var1 + var7, var2 + 1, var3, var4, var5);
            }
         } else {
            var6 = 0 + this.compressLeaf(var1, var2, var3, var4, var5);
         }
      }

      return var6;
   }

   private int compressLeaf(int var1, int var2, SegmentData var3, DataOutputStream var4, boolean var5) throws IOException {
      var1 = getIndex(var1, var2);
      short var10 = this.sizes[var1];
      byte[][] var6 = ArrayOctreeTraverse.tMap[var10];
      testHashSet.add(var10);
      byte var7 = getStart(0, var1);
      byte var8 = getStart(1, var1);
      byte var9 = getStart(2, var1);
      if (!var5) {
         assert var1 < 32767;

         var4.writeShort(var1);

         assert var10 < 256;

         var4.writeByte(var10);
      } else {
         for(var1 = 0; var1 < var6.length; ++var1) {
            var2 = SegmentData.getInfoIndex((byte)(var7 + var6[var1][0] + 16), (byte)(var8 + var6[var1][1] + 16), (byte)(var9 + var6[var1][2] + 16));
            var3.writeSingle(var2, var4);
         }
      }

      return var6.length;
   }

   public int compressInterleaf(SegmentData var1, DataOutputStream var2) throws IOException {
      int var3 = 0;

      for(int var4 = 0; var4 < 8; ++var4) {
         var3 += this.compressInterleaf(var4, 0, var1, var2);
      }

      return var3;
   }

   private int compressInterleaf(int var1, int var2, SegmentData var3, DataOutputStream var4) throws IOException {
      int var5 = 0;
      int var6 = getIndex(var1, var2);
      if (this.sizes[var6] > 0) {
         if (var2 < 3) {
            var1 <<= 3;

            for(var6 = 0; var6 < 8; ++var6) {
               var5 += this.compressInterleaf(var1 + var6, var2 + 1, var3, var4);
            }
         } else {
            var5 = 0 + this.compressLeafInterleaf(var1, var2, var3, var4);
         }
      }

      return var5;
   }

   private int compressLeafInterleaf(int var1, int var2, SegmentData var3, DataOutputStream var4) throws IOException {
      var1 = getIndex(var1, var2);
      short var9 = this.sizes[var1];
      byte[][] var5 = ArrayOctreeTraverse.tMap[var9];
      testHashSet.add(var9);
      byte var6 = getStart(0, var1);
      byte var7 = getStart(1, var1);
      byte var8 = getStart(2, var1);

      assert var1 < 32767;

      var4.writeShort(var1);

      assert var9 < 256;

      var4.writeByte(var9);

      for(var1 = 0; var1 < var5.length; ++var1) {
         var2 = SegmentData.getInfoIndex((byte)(var6 + var5[var1][0] + 16), (byte)(var7 + var5[var1][1] + 16), (byte)(var8 + var5[var1][2] + 16));
         var3.writeSingle(var2, var4);
      }

      return var5.length;
   }

   private int traverseLeafSolid(int var1, int var2, SegmentDataTraverseInterface var3, SegmentData var4) {
      getIndex(var1, var2);
      return 0;
   }

   static {
      for(byte var2 = 0; var2 < 32; ++var2) {
         mod16[var2] = (byte)ByteUtil.modU16(var2);
      }

      BUFFER_SIZE = NODES * 6;
      testHashSet = new IntOpenHashSet();
      dimBuffer = new byte[BUFFER_SIZE];
      indexBuffer = new int[131072];
      localIndexBuffer = new int[98304];
      localIndexShiftedBuffer = new short[98304];
      indexBufferIndexed = new int[393216];
      serverSet = new OctreeVariableSet();
      clientSet = new OctreeVariableSet();
      toBB16Map = new byte['耀'];
      ArrayOctreeGenerator.splitStart(new Vector3b(-16.0F, -16.0F, -16.0F), new Vector3b(16.0F, 16.0F, 16.0F), (byte)16);
      ArrayOctreeTraverse.create();
      Vector3b var0 = new Vector3b();
      Vector3b var1 = new Vector3b();
      int var7 = 0;

      for(int var3 = 0; var3 < 32; ++var3) {
         for(int var4 = 0; var4 < 32; ++var4) {
            for(int var5 = 0; var5 < 32; ++var5) {
               for(byte var6 = 1; var6 < 9; ++var6) {
                  getBox(var6, var0, var1);
                  var0.add((byte)16, (byte)16, (byte)16);
                  var1.add((byte)16, (byte)16, (byte)16);
                  if (var5 >= var0.x && var5 < var1.x && var4 >= var0.y && var4 < var1.y && var3 >= var0.z && var3 < var1.z) {
                     toBB16Map[var7] = (byte)(var6 - 1);
                     break;
                  }
               }

               ++var7;
            }
         }
      }

   }
}
