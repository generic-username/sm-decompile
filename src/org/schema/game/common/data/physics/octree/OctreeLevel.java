package org.schema.game.common.data.physics.octree;

public class OctreeLevel {
   public static final short START = 0;
   public static final short END = 1;
   public static final short DIM = 2;
   public static final short HALF = 3;
   byte level;
   int index;
   int id;

   public int hashCode() {
      return (this.level << 3) + this.index + this.id * 100000;
   }

   public boolean equals(Object var1) {
      return this.level == ((OctreeLevel)var1).level && this.id == ((OctreeLevel)var1).id && this.index == ((OctreeLevel)var1).index;
   }

   public String toString() {
      return "OctreeLevel [level=" + this.level + ", index=" + this.index + ", id=" + this.id + "]";
   }
}
