package org.schema.game.common.data.physics.octree;

import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.game.common.data.physics.BoxShapeExt;
import org.schema.game.common.data.world.Segment;

public class OctreeNode extends OctreeLeaf {
   private OctreeLeaf[] children;

   public OctreeNode(int var1, byte var2, int var3, boolean var4) {
      super(var1, var2, var3, var4);
   }

   public OctreeNode(Vector3b var1, Vector3b var2, int var3, byte var4, int var5, boolean var6) {
      super(var1, var2, var3, var4, var5, var6);
   }

   public void delete(byte var1, byte var2, byte var3, TreeCache var4, int var5) {
      super.delete(var1, var2, var3, var4, var5);
      if (var2 >= this.getStartX() && var2 < this.getEndY() - this.getHalfDimY()) {
         if (var1 >= this.getStartX() && var1 < this.getEndX() - this.getHalfDimX()) {
            if (var3 >= this.getStartZ() && var3 < this.getEndZ() - this.getHalfDimZ()) {
               var4.lvlToIndex[var5] = 0;
               this.children[0].delete(var1, var2, var3, var4, var5 + 1);
            } else {
               var4.lvlToIndex[var5] = 3;
               this.children[3].delete(var1, var2, var3, var4, var5 + 1);
            }
         } else if (var3 >= this.getStartZ() && var3 < this.getEndZ() - this.getHalfDimZ()) {
            var4.lvlToIndex[var5] = 1;
            this.children[1].delete(var1, var2, var3, var4, var5 + 1);
         } else {
            var4.lvlToIndex[var5] = 2;
            this.children[2].delete(var1, var2, var3, var4, var5 + 1);
         }
      } else if (var1 >= this.getStartX() && var1 < this.getEndX() - this.getHalfDimX()) {
         if (var3 >= this.getStartZ() && var3 < this.getEndZ() - this.getHalfDimZ()) {
            var4.lvlToIndex[var5] = 4;
            this.children[4].delete(var1, var2, var3, var4, var5 + 1);
         } else {
            var4.lvlToIndex[var5] = 7;
            this.children[7].delete(var1, var2, var3, var4, var5 + 1);
         }
      } else if (var3 >= this.getStartZ() && var3 < this.getEndZ() - this.getHalfDimZ()) {
         var4.lvlToIndex[var5] = 5;
         this.children[5].delete(var1, var2, var3, var4, var5 + 1);
      } else {
         var4.lvlToIndex[var5] = 6;
         this.children[6].delete(var1, var2, var3, var4, var5 + 1);
      }
   }

   public void deleteCached(TreeCache var1, int var2) {
      super.deleteCached(var1, var2 + 1);
      this.children[var1.lvlToIndex[var2]].deleteCached(var1, var2 + 1);
   }

   public void drawOctree(Vector3f var1, boolean var2) {
      if (this.isHasHit()) {
         super.drawOctree(var1, var2);

         for(int var3 = 0; var3 < this.children.length; ++var3) {
            this.children[var3].drawOctree(var1, false);
         }
      }

   }

   public IntersectionCallback findIntersecting(OctreeVariableSet var1, IntersectionCallback var2, Segment var3, Transform var4, Matrix3f var5, float var6, Vector3f var7, Vector3f var8, float var9, boolean var10) {
      var2 = this.doIntersecting(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
      if (this.isHasHit()) {
         for(int var11 = 0; var11 < this.children.length; ++var11) {
            if (!this.children[var11].isEmpty()) {
               var2 = this.children[var11].findIntersecting(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
            } else {
               this.children[var11].setHasHit(false);
            }
         }
      }

      return var2;
   }

   public IntersectionCallback findIntersectingCast(IntersectionCallback var1, Transform var2, BoxShapeExt var3, ConvexShape var4, float var5, Segment var6, Transform var7, Transform var8, float var9) {
      var1 = super.findIntersectingCast(var1, var2, var3, var4, var5, var6, var7, var8, var9);
      if (this.isHasHit()) {
         for(int var10 = 0; var10 < this.children.length; ++var10) {
            var1 = this.children[var10].findIntersectingCast(var1, var2, var3, var4, var5, var6, var7, var8, var9);
         }
      }

      return var1;
   }

   public IntersectionCallback findIntersectingRay(OctreeVariableSet var1, IntersectionCallback var2, Transform var3, Matrix3f var4, float var5, Segment var6, Vector3f var7, Vector3f var8, float var9) {
      var2 = super.findIntersectingRay(var1, var2, var3, var4, var5, var6, var7, var8, var9);
      if (this.isHasHit()) {
         for(int var10 = 0; var10 < this.children.length; ++var10) {
            if (!this.children[var10].isEmpty()) {
               var2 = this.children[var10].findIntersectingRay(var1, var2, var3, var4, var5, var6, var7, var8, var9);
            } else {
               this.children[var10].setHasHit(false);
            }
         }
      }

      return var2;
   }

   public void insert(byte var1, byte var2, byte var3, TreeCache var4, int var5) {
      super.insert(var1, var2, var3, var4, var5 + 1);
      byte var6 = this.getStartX();
      byte var7 = this.getStartY();
      byte var8 = this.getStartZ();
      byte var9 = this.getEndX();
      byte var10 = this.getEndY();
      byte var11 = this.getEndZ();
      if (var2 >= var7 && var2 < var10 - this.getHalfDimY()) {
         if (var1 >= var6 && var1 < var9 - this.getHalfDimX()) {
            if (var3 >= var8 && var3 < var11 - this.getHalfDimZ()) {
               var4.lvlToIndex[var5] = 0;
               this.children[0].insert(var1, var2, var3, var4, var5 + 1);
            } else {
               var4.lvlToIndex[var5] = 3;
               this.children[3].insert(var1, var2, var3, var4, var5 + 1);
            }
         } else if (var3 >= var8 && var3 < var11 - this.getHalfDimZ()) {
            var4.lvlToIndex[var5] = 1;
            this.children[1].insert(var1, var2, var3, var4, var5 + 1);
         } else {
            var4.lvlToIndex[var5] = 2;
            this.children[2].insert(var1, var2, var3, var4, var5 + 1);
         }
      } else if (var1 >= var6 && var1 < var9 - this.getHalfDimX()) {
         if (var3 >= var8 && var3 < var11 - this.getHalfDimZ()) {
            var4.lvlToIndex[var5] = 4;
            this.children[4].insert(var1, var2, var3, var4, var5 + 1);
         } else {
            var4.lvlToIndex[var5] = 7;
            this.children[7].insert(var1, var2, var3, var4, var5 + 1);
         }
      } else if (var3 >= var8 && var3 < var11 - this.getHalfDimZ()) {
         var4.lvlToIndex[var5] = 5;
         this.children[5].insert(var1, var2, var3, var4, var5 + 1);
      } else {
         var4.lvlToIndex[var5] = 6;
         this.children[6].insert(var1, var2, var3, var4, var5 + 1);
      }
   }

   public void insertCached(TreeCache var1, int var2) {
      super.insertCached(var1, var2 + 1);
      this.children[var1.lvlToIndex[var2]].insertCached(var1, var2 + 1);
   }

   protected boolean isLeaf() {
      return false;
   }

   public void reset() {
      super.reset();

      for(int var1 = 0; var1 < this.children.length; ++var1) {
         this.children[var1].reset();
      }

   }

   public OctreeLeaf[] getChildren() {
      return this.children;
   }

   public int split(int var1, int var2) {
      int var3 = 1;
      this.children = new OctreeLeaf[8];
      int var20;
      if (this.getSet().first) {
         Vector3b var4 = this.getStart(new Vector3b());
         Vector3b var5;
         (var5 = this.getHalfDim(new Vector3b())).add(var4);
         Vector3b var6 = new Vector3b(var4);
         Vector3b var7 = new Vector3b(var5);
         var6.add(this.getHalfDimX(), (byte)0, (byte)0);
         var7.add(this.getHalfDimX(), (byte)0, (byte)0);
         Vector3b var8 = new Vector3b(var4);
         Vector3b var9 = new Vector3b(var5);
         var8.add(this.getHalfDimX(), (byte)0, this.getHalfDimZ());
         var9.add(this.getHalfDimX(), (byte)0, this.getHalfDimZ());
         Vector3b var10 = new Vector3b(var4);
         Vector3b var11 = new Vector3b(var5);
         var10.add((byte)0, (byte)0, this.getHalfDimZ());
         var11.add((byte)0, (byte)0, this.getHalfDimZ());
         Vector3b var12 = new Vector3b(var4);
         Vector3b var13 = new Vector3b(var5);
         var12.add((byte)0, this.getHalfDimY(), (byte)0);
         var13.add((byte)0, this.getHalfDimY(), (byte)0);
         Vector3b var14 = new Vector3b(var4);
         Vector3b var15 = new Vector3b(var5);
         var14.add(this.getHalfDimX(), this.getHalfDimY(), (byte)0);
         var15.add(this.getHalfDimX(), this.getHalfDimY(), (byte)0);
         Vector3b var16 = new Vector3b(var4);
         Vector3b var17 = new Vector3b(var5);
         var16.add(this.getHalfDimX(), this.getHalfDimY(), this.getHalfDimZ());
         var17.add(this.getHalfDimX(), this.getHalfDimY(), this.getHalfDimZ());
         Vector3b var18 = new Vector3b(var4);
         Vector3b var19 = new Vector3b(var5);
         var18.add((byte)0, this.getHalfDimY(), this.getHalfDimZ());
         var19.add((byte)0, this.getHalfDimY(), this.getHalfDimZ());
         if (var2 < this.getMaxLevel()) {
            this.children[0] = new OctreeNode(var4, var5, var1 << 3, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
            this.children[1] = new OctreeNode(var6, var7, (var1 << 3) + 1, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
            this.children[2] = new OctreeNode(var8, var9, (var1 << 3) + 2, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
            this.children[3] = new OctreeNode(var10, var11, (var1 << 3) + 3, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
            this.children[4] = new OctreeNode(var12, var13, (var1 << 3) + 4, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
            this.children[5] = new OctreeNode(var14, var15, (var1 << 3) + 5, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
            this.children[6] = new OctreeNode(var16, var17, (var1 << 3) + 6, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
            this.children[7] = new OctreeNode(var18, var19, (var1 << 3) + 7, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());

            for(var20 = 0; var20 < this.children.length; ++var20) {
               var3 += ((OctreeNode)this.children[var20]).split((var1 << 3) + var20, var2 + 1);
            }
         } else {
            this.children[0] = new OctreeLeaf(var4, var5, var1 << 3, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
            this.children[1] = new OctreeLeaf(var6, var7, (var1 << 3) + 1, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
            this.children[2] = new OctreeLeaf(var8, var9, (var1 << 3) + 2, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
            this.children[3] = new OctreeLeaf(var10, var11, (var1 << 3) + 3, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
            this.children[4] = new OctreeLeaf(var12, var13, (var1 << 3) + 4, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
            this.children[5] = new OctreeLeaf(var14, var15, (var1 << 3) + 5, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
            this.children[6] = new OctreeLeaf(var16, var17, (var1 << 3) + 6, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
            this.children[7] = new OctreeLeaf(var18, var19, (var1 << 3) + 7, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
            var3 += 8;
         }
      } else if (var2 < this.getMaxLevel()) {
         this.children[0] = new OctreeNode(var1 << 3, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
         this.children[1] = new OctreeNode((var1 << 3) + 1, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
         this.children[2] = new OctreeNode((var1 << 3) + 2, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
         this.children[3] = new OctreeNode((var1 << 3) + 3, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
         this.children[4] = new OctreeNode((var1 << 3) + 4, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
         this.children[5] = new OctreeNode((var1 << 3) + 5, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
         this.children[6] = new OctreeNode((var1 << 3) + 6, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
         this.children[7] = new OctreeNode((var1 << 3) + 7, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());

         for(var20 = 0; var20 < this.children.length; ++var20) {
            var3 += ((OctreeNode)this.children[var20]).split((var1 << 3) + var20, var2 + 1);
         }
      } else {
         this.children[0] = new OctreeLeaf(var1 << 3, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
         this.children[1] = new OctreeLeaf((var1 << 3) + 1, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
         this.children[2] = new OctreeLeaf((var1 << 3) + 2, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
         this.children[3] = new OctreeLeaf((var1 << 3) + 3, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
         this.children[4] = new OctreeLeaf((var1 << 3) + 4, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
         this.children[5] = new OctreeLeaf((var1 << 3) + 5, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
         this.children[6] = new OctreeLeaf((var1 << 3) + 6, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
         this.children[7] = new OctreeLeaf((var1 << 3) + 7, (byte)(var2 + 1), this.getMaxLevel(), this.onServer());
         var3 += 8;
      }

      return var3;
   }
}
