package org.schema.game.common.data.physics.octree;

import javax.vecmath.Vector3f;

public interface ArrayOctreeAABBCallback {
   void handle(int var1, int var2, Vector3f var3, Vector3f var4);
}
