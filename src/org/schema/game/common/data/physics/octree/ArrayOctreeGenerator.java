package org.schema.game.common.data.physics.octree;

import org.schema.common.util.linAlg.Vector3b;
import org.schema.game.common.data.world.SegmentData;

public class ArrayOctreeGenerator {
   public static int getNodeIndex(int var0, int var1, int var2, int var3) {
      var0 = (var2 << 10) + (var1 << 5) + var0 << 2;
      return ArrayOctree.indexBuffer[var0 + var3];
   }

   public static int getNodeIndex(int var0, int var1) {
      return ArrayOctree.indexBufferIndexed[(var0 << 2) + var1];
   }

   public static void put(int var0, Vector3b var1, Vector3b var2) {
      var0 *= 6;
      ArrayOctree.dimBuffer[var0] = var1.x;
      ArrayOctree.dimBuffer[var0 + 1] = var1.y;
      ArrayOctree.dimBuffer[var0 + 2] = var1.z;
      ArrayOctree.dimBuffer[var0 + 3] = var2.x;
      ArrayOctree.dimBuffer[var0 + 4] = var2.y;
      ArrayOctree.dimBuffer[var0 + 5] = var2.z;
   }

   public static void putNodeIndex(int var0, int var1, int var2, int var3, int var4) {
      int var5 = (var2 << 10) + (var1 << 5) + var0 << 2;
      ArrayOctree.indexBuffer[var5 + var3] = var4;
      ArrayOctree.indexBufferIndexed[(SegmentData.getInfoIndex(var0, var1, var2) << 2) + var3] = var4;
      var3 = (var2 % 2 << 2) + (var1 % 2 << 1) + var0 % 2;
      ArrayOctree.localIndexBuffer[SegmentData.getInfoIndex(var0, var1, var2)] = var3;
      ArrayOctree.localIndexShiftedBuffer[SegmentData.getInfoIndex(var0, var1, var2)] = (short)(1 << var3);
   }

   public static void split(int var0, int var1, Vector3b var2, Vector3b var3, byte var4) {
      while(true) {
         int var5;
         put(var5 = ArrayOctree.getIndex(var0, var1), var2, var3);

         for(int var6 = var2.z + 16; var6 < var3.z + 16; ++var6) {
            for(int var7 = var2.y + 16; var7 < var3.y + 16; ++var7) {
               for(int var8 = var2.x + 16; var8 < var3.x + 16; ++var8) {
                  putNodeIndex(var8, var7, var6, var1, var5);
               }
            }
         }

         if (var1 >= 3) {
            return;
         }

         Vector3b var20 = new Vector3b(var2);
         Vector3b var21;
         (var21 = new Vector3b(var4, var4, var4)).add(var20);
         Vector3b var22 = new Vector3b(var20);
         var2 = new Vector3b(var21);
         var22.add(var4, (byte)0, (byte)0);
         var2.add(var4, (byte)0, (byte)0);
         var3 = new Vector3b(var20);
         Vector3b var19 = new Vector3b(var21);
         var3.add(var4, (byte)0, var4);
         var19.add(var4, (byte)0, var4);
         Vector3b var9 = new Vector3b(var20);
         Vector3b var10 = new Vector3b(var21);
         var9.add((byte)0, (byte)0, var4);
         var10.add((byte)0, (byte)0, var4);
         Vector3b var11 = new Vector3b(var20);
         Vector3b var12 = new Vector3b(var21);
         var11.add((byte)0, var4, (byte)0);
         var12.add((byte)0, var4, (byte)0);
         Vector3b var13 = new Vector3b(var20);
         Vector3b var14 = new Vector3b(var21);
         var13.add(var4, var4, (byte)0);
         var14.add(var4, var4, (byte)0);
         Vector3b var15 = new Vector3b(var20);
         Vector3b var16 = new Vector3b(var21);
         var15.add(var4, var4, var4);
         var16.add(var4, var4, var4);
         Vector3b var17 = new Vector3b(var20);
         Vector3b var18 = new Vector3b(var21);
         var17.add((byte)0, var4, var4);
         var18.add((byte)0, var4, var4);
         var0 <<= 3;
         var4 = (byte)(var4 / 2);
         split(var0, var1 + 1, var20, var21, var4);
         split(var0 + 1, var1 + 1, var22, var2, var4);
         split(var0 + 2, var1 + 1, var3, var19, var4);
         split(var0 + 3, var1 + 1, var9, var10, var4);
         split(var0 + 4, var1 + 1, var11, var12, var4);
         split(var0 + 5, var1 + 1, var13, var14, var4);
         split(var0 + 6, var1 + 1, var15, var16, var4);
         int var10000 = var0 + 7;
         int var10001 = var1 + 1;
         var3 = var18;
         var2 = var17;
         var1 = var10001;
         var0 = var10000;
      }
   }

   public static void splitStart(Vector3b var0, Vector3b var1, byte var2) {
      var0 = new Vector3b(var0);
      (var1 = new Vector3b(var2, var2, var2)).add(var0);
      Vector3b var3 = new Vector3b(var0);
      Vector3b var4 = new Vector3b(var1);
      var3.add(var2, (byte)0, (byte)0);
      var4.add(var2, (byte)0, (byte)0);
      Vector3b var5 = new Vector3b(var0);
      Vector3b var6 = new Vector3b(var1);
      var5.add(var2, (byte)0, var2);
      var6.add(var2, (byte)0, var2);
      Vector3b var7 = new Vector3b(var0);
      Vector3b var8 = new Vector3b(var1);
      var7.add((byte)0, (byte)0, var2);
      var8.add((byte)0, (byte)0, var2);
      Vector3b var9 = new Vector3b(var0);
      Vector3b var10 = new Vector3b(var1);
      var9.add((byte)0, var2, (byte)0);
      var10.add((byte)0, var2, (byte)0);
      Vector3b var11 = new Vector3b(var0);
      Vector3b var12 = new Vector3b(var1);
      var11.add(var2, var2, (byte)0);
      var12.add(var2, var2, (byte)0);
      Vector3b var13 = new Vector3b(var0);
      Vector3b var14 = new Vector3b(var1);
      var13.add(var2, var2, var2);
      var14.add(var2, var2, var2);
      Vector3b var15 = new Vector3b(var0);
      Vector3b var16 = new Vector3b(var1);
      var15.add((byte)0, var2, var2);
      var16.add((byte)0, var2, var2);
      var2 = (byte)(var2 / 2);
      split(0, 0, var0, var1, var2);
      split(1, 0, var3, var4, var2);
      split(2, 0, var5, var6, var2);
      split(3, 0, var7, var8, var2);
      split(4, 0, var9, var10, var2);
      split(5, 0, var11, var12, var2);
      split(6, 0, var13, var14, var2);
      split(7, 0, var15, var16, var2);
   }
}
