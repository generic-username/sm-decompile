package org.schema.game.common.data.physics;

import com.bulletphysics.collision.broadphase.CollisionAlgorithm;
import com.bulletphysics.collision.broadphase.CollisionAlgorithmConstructionInfo;
import com.bulletphysics.collision.broadphase.DispatcherInfo;
import com.bulletphysics.collision.dispatch.CollisionAlgorithmCreateFunc;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.ManifoldResult;
import com.bulletphysics.collision.narrowphase.ManifoldPoint;
import com.bulletphysics.collision.narrowphase.PersistentManifold;
import com.bulletphysics.collision.narrowphase.SimplexSolverInterface;
import com.bulletphysics.collision.narrowphase.DiscreteCollisionDetectorInterface.ClosestPointInput;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectArrayList;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.common.controller.FloatingRock;
import org.schema.game.common.controller.SegmentBufferIteratorInterface;
import org.schema.game.common.controller.SegmentBufferManager;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.controller.elements.TriggerManagerInterface;
import org.schema.game.common.controller.elements.trigger.TriggerCollectionManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.ActivationTrigger;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.octree.ArrayOctree;
import org.schema.game.common.data.physics.octree.ArrayOctreeTraverse;
import org.schema.game.common.data.physics.sweepandpruneaabb.OverlappingSweepPair;
import org.schema.game.common.data.physics.sweepandpruneaabb.SegmentAabbInterface;
import org.schema.game.common.data.physics.sweepandpruneaabb.SweepPoint;
import org.schema.game.common.data.world.SectorNotFoundRuntimeException;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.network.StateInterface;

public class CubeCubeCollisionAlgorithm extends CollisionAlgorithm {
   private static final long STUCK_TIME = 20000L;
   static float margin = 0.05F;
   private static ThreadLocal threadLocal = new ThreadLocal() {
      protected final CubeCubeCollisionVariableSet initialValue() {
         return new CubeCubeCollisionVariableSet();
      }
   };
   protected final ObjectPool pointInputsPool = ObjectPool.get(ClosestPointInput.class);
   public boolean lowLevelOfDetail;
   public CubeCubeCollisionVariableSet v;
   public boolean localSwap;
   private int distinctTests;
   private long distinctTime;
   private GjkPairDetectorExt gjkPairDetector;
   private BoxBoxDetector boxboxPairDetector = new BoxBoxDetector();
   private int slow_threashold;
   private boolean ownManifold;
   private PersistentManifold manifoldPtr;
   private long currentTime;
   private boolean stuck;
   private boolean abort;
   private float currentMaxDepth;
   private long stuckStarted;
   private int stuckTests;
   private boolean actStuck;
   private CubeCubeCollisionAlgorithm.InnerSegmentAddHandler innerHandler;
   private CubeCubeCollisionAlgorithm.OuterSegmentAddHandler outerHandler;
   private int hitCount;
   private Transform t0;
   private Transform t1;
   private float t0Mass;
   private float t1Mass;
   private boolean wasStuck;
   private short stuckUN;
   private int narrowTests;
   private boolean protectedByUndocking;
   private Vector3f proxVec;
   private static final boolean old = false;

   public CubeCubeCollisionAlgorithm() {
      this.slow_threashold = (Integer)ServerConfig.PHYSICS_SLOWDOWN_THRESHOLD.getCurrentState();
      this.hitCount = 0;
      this.t0 = new Transform();
      this.t1 = new Transform();
      this.proxVec = new Vector3f();
   }

   public void cleanUpDispatcher() {
      ((CollisionDispatcherExt)this.dispatcher).cleanNearCallback();
   }

   private void checkStuck(SegmentData var1, SegmentData var2, Transform var3, Transform var4, Vector3f var5) {
      this.v.elemPosTest.set(var5.x, var5.y, var5.z);
      var3.transform(this.v.elemPosTest);
      var4.transform(this.v.elemPosTest);
      this.v.elemPosCheck.set((int)this.v.elemPosTest.x + 16, (int)this.v.elemPosTest.y + 16, (int)this.v.elemPosTest.z + 16);
      int var6 = 0;

      for(int var7 = 0; var7 < 6; ++var7) {
         this.v.elemPosCheckD.add(this.v.elemPosCheck, Element.DIRECTIONSi[var7]);
         if (var2.getSegmentController().getSegmentBuffer().existsPointUnsave(this.v.elemPosCheckD)) {
            ++var6;
         }
      }

      if (var2.getSegmentController().getSegmentBuffer().existsPointUnsave(this.v.elemPosCheck)) {
         ++var6;
      }

      if (var6 >= 7) {
         this.actStuck = true;
         if (this.stuckStarted == 0L || System.currentTimeMillis() - this.stuckStarted > 20000L) {
            System.err.println("[CC] POSSIBLE STUCK (timeframe: 2.5 secs): " + var1.getSegmentController() + " <-> " + var2.getSegmentController() + ": from " + this.v.elemPosTest + " to " + this.v.elemPosCheck + "; existing surround: " + var6);
            this.stuckStarted = System.currentTimeMillis();
            return;
         }

         if (System.currentTimeMillis() - this.stuckStarted > 2500L) {
            this.stuck = true;
            this.stuckUN = var2.getSegmentController().getState().getNumberOfUpdate();
         }
      }

   }

   private int doExtCubeCubeCollision(BoxShape var1, BoxShape var2, Transform var3, Transform var4, ManifoldResult var5, DispatcherInfo var6, Vector3f var7, Vector3f var8, short var9, short var10, int var11, int var12) {
      ClosestPointInput var13;
      (var13 = (ClosestPointInput)this.pointInputsPool.get()).init();
      var13.maximumDistanceSquared = var1.getMargin() + var2.getMargin() + this.manifoldPtr.getContactBreakingThreshold();
      var13.maximumDistanceSquared *= var13.maximumDistanceSquared;
      var13.transformA.set(var3);
      var13.transformB.set(var4);
      this.boxboxPairDetector.GetClosestPoints(var1, var2, var13, var5, var6.debugDraw, false, var7, var8, var9, var10, var11, var12);
      this.pointInputsPool.release(var13);
      this.currentMaxDepth = this.boxboxPairDetector.maxDepth;
      return this.boxboxPairDetector.contacts;
   }

   private void doNarrowTest(SegmentData var1, SegmentData var2, Vector3b var3, Vector3b var4, Vector3b var5, Vector3b var6, int var7, int var8, int var9, int var10, BoundingBox var11, DispatcherInfo var12, ManifoldResult var13) {
      ++this.narrowTests;
      byte[][] var29 = ArrayOctreeTraverse.tMap[var7];
      byte[][] var30 = ArrayOctreeTraverse.tMap[var8];
      int[] var31 = ArrayOctreeTraverse.tIndexMap[var9][var7];
      int[] var32 = ArrayOctreeTraverse.tIndexMap[var10][var8];

      assert var29.length <= 8 : var29.length;

      assert var30.length <= 8 : var30.length;

      for(var9 = 0; var9 < var29.length; ++var9) {
         var10 = var3.x + var29[var9][0];
         int var33 = var3.y + var29[var9][1];
         int var14 = var3.z + var29[var9][2];
         int var15 = var31[var9];
         var13.getPersistentManifold().getNumContacts();
         ElementInformation var16;
         short var17;
         if ((var17 = var1.getType(var15)) != 0 && var17 == 411 | (var16 = ElementKeyMap.getInfo(var17)).isPhysical(var1.isActive(var15)) && var16.lodCollisionPhysical) {
            byte var34;
            byte var18 = var34 = var1.getOrientation(var15);
            if (var17 == 689) {
               if (var34 == 4) {
                  continue;
               }

               var18 = 2;
            }

            this.v.elemPosA.set((float)(var10 + var1.getSegment().pos.x), (float)(var33 + var1.getSegment().pos.y), (float)(var14 + var1.getSegment().pos.z));
            this.v.elemPosAAbs.set(this.v.elemPosA);
            this.v.tmpTrans0.transform(this.v.elemPosAAbs);
            this.v.min.set(this.v.elemPosA.x - 0.5F, this.v.elemPosA.y - 0.5F, this.v.elemPosA.z - 0.5F);
            this.v.max.set(this.v.elemPosA.x + 0.5F, this.v.elemPosA.y + 0.5F, this.v.elemPosA.z + 0.5F);

            for(int var19 = 0; var19 < var30.length; ++var19) {
               int var20 = var5.x + var30[var19][0];
               int var21 = var5.y + var30[var19][1];
               int var22 = var5.z + var30[var19][2];
               int var23 = var32[var19];
               ElementInformation var24;
               short var25;
               if ((var25 = var2.getType(var23)) != 0 && var25 == 411 | (var24 = ElementKeyMap.getInfo(var25)).isPhysical(var2.isActive(var23)) && var24.lodCollisionPhysical) {
                  byte var38;
                  byte var26 = var38 = var2.getOrientation(var23);
                  if (var16.cubeCubeCollision && var24.cubeCubeCollision) {
                     if (var25 == 689) {
                        if (var38 == 4) {
                           continue;
                        }

                        var26 = 2;
                     }

                     this.v.elemPosB.set((float)(var20 + var2.getSegment().pos.x), (float)(var21 + var2.getSegment().pos.y), (float)(var22 + var2.getSegment().pos.z));
                     this.v.elemPosBAbs.set(this.v.elemPosB);
                     this.v.tmpTrans1.transform(this.v.elemPosBAbs);
                     this.v.elemPosBAbs.sub(this.v.elemPosAAbs);
                     this.proxVec.add(this.v.elemPosBAbs);
                     if (FastMath.carmackLength(this.v.elemPosBAbs) < 1.733F) {
                        if (var1.getSegmentController().isOnServer()) {
                           if (var17 == 411 || var25 == 411) {
                              continue;
                           }
                        } else {
                           if (var17 == 411) {
                              this.handleTrigger(var1, this.v.col0, var10, var33, var14, var17);
                              continue;
                           }

                           if (var25 == 411) {
                              this.handleTrigger(var2, this.v.col1, var20, var21, var22, var25);
                              continue;
                           }
                        }

                        this.v.tmpTrans3.set(this.v.tmpTrans0);
                        this.v.tmpTrans4.set(this.v.tmpTrans1);
                        this.v.nA.set(this.v.elemPosA);
                        this.v.nB.set(this.v.elemPosB);
                        this.v.tmpTrans3.basis.transform(this.v.nA);
                        this.v.tmpTrans4.basis.transform(this.v.nB);
                        this.v.tmpTrans3.origin.add(this.v.nA);
                        this.v.tmpTrans4.origin.add(this.v.nB);
                        Object var35 = this.v.box0;
                        Object var36 = this.v.box1;
                        boolean var37 = false;
                        if (var16.blockStyle.solidBlockStyle) {
                           var35 = BlockShapeAlgorithm.getShape(var16.getBlockStyle(), var18);
                           var37 = true;
                        }

                        if (var24.blockStyle.solidBlockStyle) {
                           var36 = BlockShapeAlgorithm.getShape(var24.getBlockStyle(), var26);
                           var37 = true;
                        }

                        Transform var27 = this.v.tmpTrans3;
                        if (var16.getSlab(var34) > 0) {
                           (var27 = this.v.BT_A).set(this.v.tmpTrans3);
                           this.v.orientTT.set(Element.DIRECTIONSf[Element.switchLeftRight(var18 % 6)]);
                           var27.basis.transform(this.v.orientTT);
                           switch(var16.getSlab(var34)) {
                           case 1:
                              this.v.orientTT.scale(0.125F);
                              var35 = this.v.box34[var18 % 6];
                              break;
                           case 2:
                              this.v.orientTT.scale(0.25F);
                              var35 = this.v.box12[var18 % 6];
                              break;
                           case 3:
                              this.v.orientTT.scale(0.375F);
                              var35 = this.v.box14[var18 % 6];
                           }

                           var27.origin.sub(this.v.orientTT);
                           this.boxboxPairDetector.cachedBoxSize = false;
                        }

                        Transform var28 = this.v.tmpTrans4;
                        if (var24.getSlab(var38) > 0) {
                           (var28 = this.v.BT_B).set(this.v.tmpTrans4);
                           this.v.orientTT.set(Element.DIRECTIONSf[Element.switchLeftRight(var26 % 6)]);
                           var28.basis.transform(this.v.orientTT);
                           switch(var24.getSlab(var38)) {
                           case 1:
                              this.v.orientTT.scale(0.125F);
                              var36 = this.v.box34[var26 % 6];
                              break;
                           case 2:
                              this.v.orientTT.scale(0.25F);
                              var36 = this.v.box12[var26 % 6];
                              break;
                           case 3:
                              this.v.orientTT.scale(0.375F);
                              var36 = this.v.box14[var26 % 6];
                           }

                           var28.origin.sub(this.v.orientTT);
                           this.boxboxPairDetector.cachedBoxSize = false;
                        }

                        this.currentMaxDepth = 0.0F;
                        if (var37) {
                           var20 = this.doNonBlockCollision((ConvexShape)var35, (ConvexShape)var36, var27, var28, var13, var12, this.v.elemPosA, this.v.elemPosB, var17, var25, var1.getSegmentController().getId(), var2.getSegmentController().getId());
                        } else {
                           var20 = this.doExtCubeCubeCollision((BoxShape)var35, (BoxShape)var36, var27, var28, var13, var12, this.v.elemPosA, this.v.elemPosB, var17, var25, var1.getSegmentController().getId(), var2.getSegmentController().getId());
                        }

                        if (var20 > 0) {
                           var13.getPersistentManifold().getNumContacts();
                           ++this.hitCount;
                           if (this.protectedByUndocking) {
                              var13.getPersistentManifold().clearManifold();
                              this.abort = true;
                              return;
                           }
                        }

                        if (!this.actStuck && !this.stuck && this.currentMaxDepth > 0.65F) {
                           this.checkStuck(var1, var2, this.v.tmpTrans0, this.v.wtInv1, this.v.elemPosA);
                           this.checkStuck(var2, var1, this.v.tmpTrans1, this.v.wtInv0, this.v.elemPosB);
                        } else if (this.stuckTests > 0) {
                           --this.stuckTests;
                        }
                     }
                  }
               }
            }
         }
      }

   }

   private void handleTrigger(SegmentData var1, CollisionObject var2, int var3, int var4, int var5, short var6) {
      ManagedSegmentController var7;
      if (var1.getSegmentController() instanceof ManagedSegmentController && (var7 = (ManagedSegmentController)var1.getSegmentController()).getManagerContainer() instanceof TriggerManagerInterface) {
         ManagerModuleCollection var14 = ((TriggerManagerInterface)var7.getManagerContainer()).getTrigger();
         long var8 = ElementCollection.getIndex(var3 + var1.getSegment().pos.x + 16, var4 + var1.getSegment().pos.y + 16, var5 + var1.getSegment().pos.z + 16);
         Iterator var10 = var14.getCollectionManagers().iterator();

         while(var10.hasNext()) {
            TriggerCollectionManager var11;
            if ((var11 = (TriggerCollectionManager)var10.next()).rawCollection.contains(var8)) {
               ActivationTrigger var12 = new ActivationTrigger(var11.getControllerIndex(), var2, var6);
               ActivationTrigger var13;
               if ((var13 = (ActivationTrigger)var1.getSegmentController().getTriggers().get(var12)) == null) {
                  var1.getSegmentController().getTriggers().add(var12);
               } else {
                  var13.ping();
               }
            }
         }
      }

   }

   private int doNonBlockCollision(ConvexShape var1, ConvexShape var2, Transform var3, Transform var4, ManifoldResult var5, DispatcherInfo var6, Vector3f var7, Vector3f var8, short var9, short var10, int var11, int var12) {
      ClosestPointInput var13;
      (var13 = (ClosestPointInput)this.pointInputsPool.get()).init();
      var5.setPersistentManifold(this.manifoldPtr);
      this.gjkPairDetector.setMinkowskiA(var1);
      this.gjkPairDetector.setMinkowskiB(var2);
      var13.maximumDistanceSquared = var1.getMargin() + var2.getMargin() + this.manifoldPtr.getContactBreakingThreshold();
      var13.maximumDistanceSquared *= var13.maximumDistanceSquared;
      var13.transformA.set(var3);
      var13.transformB.set(var4);
      this.gjkPairDetector.getClosestPoints(var13, var5, var6.debugDraw, false, var7, var8, var9, var10, var11, var12);
      this.pointInputsPool.release(var13);
      this.currentMaxDepth = this.gjkPairDetector.maxDepth;
      return this.gjkPairDetector.contacts;
   }

   private void handleSweepAndPrune(CubeShape var1, CubeShape var2, DispatcherInfo var3, ManifoldResult var4) {
      this.distinctTests = 0;
      this.distinctTime = 0L;
      this.v.sweeper.fill(var1, this.v.tmpTrans0, var2, this.v.tmpTrans1, this.v.outerNonEmptySegments, this.v.innerNonEmptySegments);
      this.v.sweeper.getOverlapping();
      Iterator var5 = this.v.sweeper.pairs.iterator();

      while(var5.hasNext()) {
         OverlappingSweepPair var6;
         SweepPoint var7 = (var6 = (OverlappingSweepPair)var5.next()).a;
         SweepPoint var8 = var6.b;
         ++this.distinctTests;
         this.processDistinctCollision(var1, var2, ((Segment)var7.seg).getSegmentData(), ((Segment)var8.seg).getSegmentData(), this.v.tmpTrans0, this.v.tmpTrans1, var3, var4);
      }

      this.v.sweeper.pairs.clear();
      this.v.innerNonEmptySegments.clear();
      this.v.outerNonEmptySegments.clear();
      this.v.bbCacheInner.clear();
   }

   private void processDistinctCollision(SegmentAabbInterface var1, SegmentAabbInterface var2, SegmentData var3, SegmentData var4, Transform var5, Transform var6, DispatcherInfo var7, ManifoldResult var8) {
      this.v.octreeTopLevelSweeper.abs0 = this.v.absolute1;
      this.v.octreeTopLevelSweeper.abs1 = this.v.absolute2;
      this.v.octreeTopLevelSweeper.seg0 = var3.getSegment();
      this.v.octreeTopLevelSweeper.set = this.v.oSet;
      this.v.simpleListA.max = 0;
      this.v.simpleListB.max = 0;
      this.v.octreeTopLevelSweeper.debug = false;
      this.v.octreeTopLevelSweeper.fill(var3.getSegment(), var5, var4.getSegment(), var6, this.v.simpleListA, this.v.simpleListB);
      this.v.octreeTopLevelSweeper.getOverlapping();
      int var9 = this.v.octreeTopLevelSweeper.pairs.size();

      for(int var10 = 0; var10 < var9; ++var10) {
         OverlappingSweepPair var11;
         SweepPoint var12 = (var11 = (OverlappingSweepPair)this.v.octreeTopLevelSweeper.pairs.get(var10)).a;
         SweepPoint var13 = var11.b;
         this.handle16(var1, var2, var3, var4, var5, var6, var12, var13, var7, var8);
      }

   }

   private void handle16(SegmentAabbInterface var1, SegmentAabbInterface var2, SegmentData var3, SegmentData var4, Transform var5, Transform var6, SweepPoint var7, SweepPoint var8, DispatcherInfo var9, ManifoldResult var10) {
      int var11 = (Integer)var7.seg;
      int var12 = (Integer)var8.seg;
      if (!this.v.intersectionCallBackAwithB.initialized) {
         this.v.intersectionCallBackAwithB.createHitCache(4096);
         this.v.intersectionCallBackBwithA.createHitCache(4096);
      }

      this.v.intersectionCallBackAwithB.reset();
      this.v.intersectionCallBackBwithA.reset();
      this.v.bbSeg16a.min.set(var7.minX, var7.minY, var7.minZ);
      this.v.bbSeg16a.max.set(var7.maxX, var7.maxY, var7.maxZ);
      this.v.bbSeg16b.min.set(var8.minX, var8.minY, var8.minZ);
      this.v.bbSeg16b.max.set(var8.maxX, var8.maxY, var8.maxZ);
      this.v.intersectionCallBackAwithB = var3.getOctree().findIntersectingAABBFromFirstLvl(var11, this.v.oSet, this.v.intersectionCallBackAwithB, var3.getSegment(), var5, this.v.absolute1, 0.0F, this.v.bbSeg16b.min, this.v.bbSeg16b.max, 1.0F);
      if (this.v.intersectionCallBackAwithB.hitCount != 0) {
         this.v.intersectionCallBackBwithA.reset();
         this.v.oSet.debug = var4.getSegmentController().isOnServer();
         this.v.intersectionCallBackBwithA = var4.getOctree().findIntersectingAABBFromFirstLvl(var12, this.v.oSet, this.v.intersectionCallBackBwithA, var4.getSegment(), var6, this.v.absolute2, -0.001F, this.v.bbSeg16a.min, this.v.bbSeg16a.max, 1.0F);
         this.v.oSet.debug = false;
         if (this.v.intersectionCallBackBwithA.hitCount != 0) {
            this.v.simpleListA.max = this.v.intersectionCallBackAwithB.hitCount;
            this.v.simpleListB.max = this.v.intersectionCallBackBwithA.hitCount;
            this.v.octreeSweeper.fill(this.v.intersectionCallBackAwithB, (Transform)null, this.v.intersectionCallBackBwithA, (Transform)null, this.v.simpleListA, this.v.simpleListB);
            this.v.octreeSweeper.getOverlapping();
            this.v.tmpTrans3.set(this.v.tmpTrans0);
            this.v.tmpTrans4.set(this.v.tmpTrans1);
            var11 = this.v.octreeSweeper.pairs.size();

            for(var12 = 0; var12 < var11; ++var12) {
               OverlappingSweepPair var13;
               SweepPoint var10000 = (var13 = (OverlappingSweepPair)this.v.octreeSweeper.pairs.get(var12)).a;
               var10000 = var13.b;
               int var15 = this.v.intersectionCallBackAwithB.getHit((Integer)var13.a.seg, this.v.bbOuterOct.min, this.v.bbOuterOct.max, this.v.startA, this.v.endA);
               int var16 = this.v.intersectionCallBackAwithB.getNodeIndexHit((Integer)var13.a.seg);
               int var17 = this.v.intersectionCallBackBwithA.getHit((Integer)var13.b.seg, this.v.bbInnerOct.min, this.v.bbInnerOct.max, this.v.startB, this.v.endB);
               int var14 = this.v.intersectionCallBackBwithA.getNodeIndexHit((Integer)var13.b.seg);
               this.doNarrowTest(var3, var4, this.v.startA, this.v.endA, this.v.startB, this.v.endB, var15, var17, var16, var14, this.v.bbOctIntersection, var9, var10);
            }

         }
      }
   }

   private void processDistinctCollisionOld(CubeShape var1, CubeShape var2, SegmentData var3, SegmentData var4, Transform var5, Transform var6, DispatcherInfo var7, ManifoldResult var8) {
      if (!this.stuck) {
         if (!this.v.intersectionCallBackAwithB.initialized) {
            this.v.intersectionCallBackAwithB.createHitCache(4096);
            this.v.intersectionCallBackBwithA.createHitCache(4096);
         }

         this.v.intersectionCallBackAwithB.reset();
         this.v.intersectionCallBackBwithA.reset();
         this.v.intersectionCallBackAwithB = var3.getOctree().findIntersectingAABB(this.v.oSet, this.v.intersectionCallBackAwithB, var3.getSegment(), var5, this.v.absolute1, margin, this.v.bbSectorIntersection.min, this.v.bbSectorIntersection.max, 1.0F);
         if (this.v.intersectionCallBackAwithB.hitCount != 0) {
            this.v.intersectionCallBackBwithA.reset();
            this.v.oSet.debug = var4.getSegmentController().isOnServer();
            this.v.intersectionCallBackBwithA = var4.getOctree().findIntersectingAABB(this.v.oSet, this.v.intersectionCallBackBwithA, var4.getSegment(), var6, this.v.absolute2, margin, this.v.bbSectorIntersection.min, this.v.bbSectorIntersection.max, 1.0F);
            this.v.oSet.debug = false;
            if (this.v.intersectionCallBackBwithA.hitCount != 0) {
               for(int var11 = 0; var11 < this.v.intersectionCallBackAwithB.hitCount; ++var11) {
                  int var12 = this.v.intersectionCallBackAwithB.getHit(var11, this.v.bbOuterOct.min, this.v.bbOuterOct.max, this.v.startA, this.v.endA);
                  int var13 = this.v.intersectionCallBackAwithB.getNodeIndexHit(var11);

                  for(int var14 = 0; var14 < this.v.intersectionCallBackBwithA.hitCount; ++var14) {
                     int var9 = this.v.intersectionCallBackBwithA.getHit(var14, this.v.bbInnerOct.min, this.v.bbInnerOct.max, this.v.startB, this.v.endB);
                     int var10 = this.v.intersectionCallBackBwithA.getNodeIndexHit(var14);
                     if (AabbUtil2.testAabbAgainstAabb2(this.v.bbInnerOct.min, this.v.bbInnerOct.max, this.v.bbOuterOct.min, this.v.bbOuterOct.max)) {
                        this.v.bbInnerOct.getIntersection(this.v.bbOuterOct, this.v.bbOctIntersection);
                        this.doNarrowTest(var3, var4, this.v.startA, this.v.endA, this.v.startB, this.v.endB, var12, var9, var13, var10, this.v.bbOctIntersection, var7, var8);
                        if (this.abort) {
                           return;
                        }
                     }
                  }
               }

            }
         }
      }
   }

   public void init(CollisionAlgorithmConstructionInfo var1) {
      super.init(var1);
   }

   public void destroy() {
      if (this.ownManifold && this.manifoldPtr != null) {
         this.dispatcher.releaseManifold(this.manifoldPtr);
      }

      this.manifoldPtr = null;
   }

   public void processCollision(CollisionObject var1, CollisionObject var2, DispatcherInfo var3, ManifoldResult var4) {
      System.currentTimeMillis();
      this.abort = false;
      this.stuck = false;
      this.hitCount = 0;
      this.narrowTests = 0;
      this.actStuck = false;
      this.proxVec.set(0.0F, 0.0F, 0.0F);
      if (var1.getCollisionShape() instanceof CubeShape && var2.getCollisionShape() instanceof CubeShape) {
         assert !(var1 instanceof RigidBodySegmentController) || !((RigidBodySegmentController)var1).isCollisionException() : var1;

         assert !(var2 instanceof RigidBodySegmentController) || !((RigidBodySegmentController)var2).isCollisionException() : var2;

         if (!var1.isStaticObject() || !var2.isStaticObject()) {
            if (this.localSwap) {
               System.err.println("LOCAL SWAP IN CUBE CUBE");
               CollisionObject var5 = var1;
               var1 = var2;
               var2 = var5;
            }

            this.currentTime = System.currentTimeMillis();
            CubeShape var14 = (CubeShape)var1.getCollisionShape();
            CubeShape var6 = (CubeShape)var2.getCollisionShape();
            if (!this.wasStuck || this.stuckUN != var6.getSegmentBuffer().getSegmentController().getState().getNumberOfUpdate()) {
               assert var14.getSegmentBuffer().getSegmentController() != var6.getSegmentBuffer().getSegmentController() : (var6.getSegmentBuffer().getSegmentController().isOnServer() ? "[SERVER]" : "[CLIENT]") + "[CUBECUBE]\non\n" + var14.getSegmentBuffer().getSegmentController() + ": ANonECOUNT " + var14.getSegmentBuffer().getTotalNonEmptySize() + "; ATOTAl " + var14.getSegmentBuffer().getSegmentController().getTotalElements() + ";\n" + var6.getSegmentBuffer().getSegmentController() + ": BNonECOUNT " + var6.getSegmentBuffer().getTotalNonEmptySize() + "; BTOTAl " + var6.getSegmentBuffer().getSegmentController().getTotalElements() + "\n\n\n" + var1 + "\n <-> \n" + var2 + "\n";

               if (var14.getSegmentBuffer().getSegmentController().stuckFrom != null || var6.getSegmentBuffer().getSegmentController().stuckFrom != null) {
                  if (var14.getSegmentBuffer().getSegmentController().stuckFrom != var2) {
                     if (System.currentTimeMillis() - var14.getSegmentBuffer().getSegmentController().stuckFromTime < 20000L) {
                        return;
                     }
                  } else if (var6.getSegmentBuffer().getSegmentController().stuckFrom != var1 && System.currentTimeMillis() - var6.getSegmentBuffer().getSegmentController().stuckFromTime < 20000L) {
                     return;
                  }
               }

               var6.getSegmentBuffer().getSegmentController().stuckFrom = null;
               if (var14 == var6) {
                  System.err.println("[PHYSICS][CUBECUBE] WARNING!! EQUALCOL " + var14.getSegmentBuffer().getSegmentController());
               } else {
                  if (var14.getSegmentBuffer().getSegmentController().isOnServer()) {
                     GameServerState var7;
                     if ((var7 = (GameServerState)var14.getSegmentBuffer().getSegmentController().getState()).getUniverse().getSector(var14.getSegmentBuffer().getSegmentController().getSectorId()) == null) {
                        throw new SectorNotFoundRuntimeException(var14.getSegmentBuffer().getSegmentController().getSectorId());
                     }

                     if (var7.getUniverse().getSector(var6.getSegmentBuffer().getSegmentController().getSectorId()) == null) {
                        throw new SectorNotFoundRuntimeException(var6.getSegmentBuffer().getSegmentController().getSectorId());
                     }
                  }

                  this.v.oSet = ArrayOctree.getSet(var14.getSegmentBuffer().getSegmentController().isOnServer());
                  this.v.col0 = var1;
                  this.v.col1 = var2;
                  this.v.tmpTrans0 = var1.getWorldTransform(this.v.tmpTrans0);
                  this.v.tmpTrans1 = var2.getWorldTransform(this.v.tmpTrans1);
                  boolean var15 = this.v.tmpTrans0.origin.equals(this.t0.origin) && this.v.tmpTrans0.basis.epsilonEquals(this.t0.basis, 1.1920929E-6F);
                  boolean var8 = this.v.tmpTrans1.origin.equals(this.t1.origin) && this.v.tmpTrans1.basis.epsilonEquals(this.t1.basis, 1.1920929E-6F);
                  boolean var9 = var14.getSegmentBuffer().getSegmentController().getMass() == this.t0Mass;
                  boolean var10 = var6.getSegmentBuffer().getSegmentController().getMass() == this.t1Mass;
                  if (!var15 || !var8 || !var9 || !var10) {
                     this.v.absolute1.set(this.v.tmpTrans0.basis);
                     MatrixUtil.absolute(this.v.absolute1);
                     this.v.absolute2.set(this.v.tmpTrans1.basis);
                     MatrixUtil.absolute(this.v.absolute2);
                     var14.getAabb(this.v.tmpTrans0, this.v.outerWorld.min, this.v.outerWorld.max);
                     var6.getAabb(this.v.tmpTrans1, this.v.innerWorld.min, this.v.innerWorld.max);
                     this.v.box0.setMargin(margin);
                     this.v.box1.setMargin(margin);
                     var14.setMargin(margin);
                     var6.setMargin(margin);
                     this.t0.set(this.v.tmpTrans0);
                     this.t1.set(this.v.tmpTrans1);
                     this.t0Mass = var14.getSegmentBuffer().getSegmentController().getMass();
                     this.t1Mass = var6.getSegmentBuffer().getSegmentController().getMass();
                     var1.getWorldTransform(this.v.wtInv0);
                     this.v.wtInv0.inverse();
                     var2.getWorldTransform(this.v.wtInv1);
                     this.v.wtInv1.inverse();
                     if (!this.doAABBIntersection(var14, var6)) {
                        if (this.ownManifold) {
                           var4.refreshContactPoints();
                        }

                     } else {
                        Math.abs(ByteUtil.divSeg(this.v.maxIntA.x) - ByteUtil.divSeg(this.v.minIntA.x));
                        Math.abs(ByteUtil.divSeg(this.v.maxIntA.y) - ByteUtil.divSeg(this.v.minIntA.y));
                        Math.abs(ByteUtil.divSeg(this.v.maxIntA.z) - ByteUtil.divSeg(this.v.minIntA.z));
                        Math.abs(ByteUtil.divSeg(this.v.maxIntB.x) - ByteUtil.divSeg(this.v.minIntB.x));
                        Math.abs(ByteUtil.divSeg(this.v.maxIntB.y) - ByteUtil.divSeg(this.v.minIntB.y));
                        Math.abs(ByteUtil.divSeg(this.v.maxIntB.z) - ByteUtil.divSeg(this.v.minIntB.z));

                        assert this.v.minIntA.x <= this.v.maxIntA.x;

                        assert this.v.minIntA.y <= this.v.maxIntA.y;

                        assert this.v.minIntA.z <= this.v.maxIntA.z;

                        assert this.v.minIntB.x <= this.v.maxIntB.x;

                        assert this.v.minIntB.y <= this.v.maxIntB.y;

                        assert this.v.minIntB.z <= this.v.maxIntB.z;

                        int var16 = this.v.maxIntA.x - this.v.minIntA.x;
                        int var17 = this.v.maxIntA.y - this.v.minIntA.y;
                        int var18 = this.v.maxIntA.z - this.v.minIntA.z;
                        int var19 = this.v.maxIntB.x - this.v.minIntB.x;
                        int var11 = this.v.maxIntB.y - this.v.minIntB.y;
                        int var12 = this.v.maxIntB.z - this.v.minIntB.z;
                        System.currentTimeMillis();

                        assert this.v.innerNonEmptySegments.isEmpty();

                        assert this.v.outerNonEmptySegments.isEmpty();

                        assert this.v.bbCacheInner.isEmpty();

                        System.currentTimeMillis();

                        try {
                           ((SegmentBufferManager)var14.getSegmentBuffer()).lastIteration = 0;
                           ((SegmentBufferManager)var6.getSegmentBuffer()).lastIteration = 0;
                           ((SegmentBufferManager)var14.getSegmentBuffer()).lastDoneIteration = 0;
                           ((SegmentBufferManager)var6.getSegmentBuffer()).lastDoneIteration = 0;
                           if (var16 > 5000 || var17 > 5000 || var17 > 5000) {
                              System.err.println("Exception collision overlap to big " + var16 + "; " + var17 + "; " + var18 + "; for: " + var14.getSegmentBuffer().getSegmentController() + "; boundingBox: " + var14.getSegmentBuffer().getBoundingBox());
                              return;
                           }

                           if (var19 > 5000 || var11 > 5000 || var11 > 5000) {
                              System.err.println("Exception collision overlap to big " + var19 + "; " + var11 + "; " + var12 + "; for: " + var14.getSegmentBuffer().getSegmentController() + "; boundingBox: " + var14.getSegmentBuffer().getBoundingBox());
                              return;
                           }

                           this.v.innerMinBlock.set(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);
                           this.v.innerMaxBlock.set(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
                           this.v.outerMinBlock.set(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);
                           this.v.outerMaxBlock.set(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
                           System.currentTimeMillis();
                           var14.getSegmentBuffer().iterateOverNonEmptyElementRange(this.outerHandler, this.v.minIntA, this.v.maxIntA, false);
                           System.currentTimeMillis();
                           System.currentTimeMillis();
                           var6.getSegmentBuffer().iterateOverNonEmptyElementRange(this.innerHandler, this.v.minIntB, this.v.maxIntB, false);
                           System.currentTimeMillis();
                           System.currentTimeMillis();
                           this.handleSweepAndPrune(var14, var6, var3, var4);
                           System.currentTimeMillis();
                        } catch (Exception var13) {
                           var13.printStackTrace();
                           this.v.innerNonEmptySegments.clear();
                           this.v.outerNonEmptySegments.clear();
                           this.v.bbCacheInner.clear();
                        }

                        System.currentTimeMillis();
                        int var10000 = this.distinctTests;
                        long var20 = this.distinctTime;
                        if (this.narrowTests > 0) {
                           this.proxVec.negate();
                           this.proxVec.scale(1.0F / (float)this.narrowTests);
                           var14.getSegmentBuffer().getSegmentController().proximityVector.add(this.proxVec);
                           var14.getSegmentBuffer().getSegmentController().onProximity(var6.getSegmentBuffer().getSegmentController());
                           this.proxVec.negate();
                           var6.getSegmentBuffer().getSegmentController().onProximity(var14.getSegmentBuffer().getSegmentController());
                           var6.getSegmentBuffer().getSegmentController().proximityVector.add(this.proxVec);
                        }

                        (new StringBuilder()).append(var14.getSegmentBuffer().getSegmentController()).append("->-").append(var14.getSegmentBuffer().getSegmentController().getTotalElements()).toString();
                        (new StringBuilder()).append(var6.getSegmentBuffer().getSegmentController()).append("->#").append(var6.getSegmentBuffer().getSegmentController().getTotalElements()).toString();
                        System.currentTimeMillis();
                        if (this.stuck) {
                           var14.getSegmentBuffer().getSegmentController().getState();
                           this.solveStuck(var1, var14, var2, var6);
                        }

                        this.wasStuck = this.stuck;
                        if (!this.stuck) {
                           var14.getSegmentBuffer().getSegmentController().stuckFrom = null;
                           var6.getSegmentBuffer().getSegmentController().stuckFrom = null;
                        }

                        if (this.ownManifold) {
                           var4.refreshContactPoints();
                        }

                        this.v.col0 = null;
                        this.v.col1 = null;
                        if (this.protectedByUndocking && this.hitCount == 0) {
                           ((RigidBodySegmentController)var1).undockingProtection = null;
                           ((RigidBodySegmentController)var2).undockingProtection = null;
                        }

                     }
                  }
               }
            }
         }
      }
   }

   public float calculateTimeOfImpact(CollisionObject var1, CollisionObject var2, DispatcherInfo var3, ManifoldResult var4) {
      return 1.0F;
   }

   public void getAllContactManifolds(ObjectArrayList var1) {
      if (this.manifoldPtr != null && this.ownManifold) {
         var1.add(this.manifoldPtr);
      }

   }

   public void init(PersistentManifold var1, CollisionAlgorithmConstructionInfo var2, CollisionObject var3, CollisionObject var4, SimplexSolverInterface var5, GjkEpaPenetrationDepthSolverExt var6) {
      super.init(var2);
      this.v = (CubeCubeCollisionVariableSet)threadLocal.get();
      this.t0.setIdentity();
      this.t1.setIdentity();
      this.t0Mass = 0.0F;
      this.t1Mass = 0.0F;
      this.gjkPairDetector = new GjkPairDetectorExt(this.v.gjkVar);
      this.outerHandler = new CubeCubeCollisionAlgorithm.OuterSegmentAddHandler();
      this.innerHandler = new CubeCubeCollisionAlgorithm.InnerSegmentAddHandler();
      this.manifoldPtr = var1;
      this.gjkPairDetector.init((ConvexShape)null, (ConvexShape)null, var5, var6);
      if (this.manifoldPtr == null) {
         this.manifoldPtr = this.dispatcher.getNewManifold(var3, var4);
         this.ownManifold = true;
      } else {
         this.ownManifold = false;
      }
   }

   private void solveStuck(CollisionObject var1, CubeShape var2, CollisionObject var3, CubeShape var4) {
      if (!var1.isStaticOrKinematicObject() || !var3.isStaticOrKinematicObject()) {
         if (var1.isStaticOrKinematicObject()) {
            this.applyOutforce(var3, var4, var1, var2);
            return;
         }

         if (var3.isStaticOrKinematicObject()) {
            this.applyOutforce(var1, var2, var3, var4);
            return;
         }

         this.applyOutforce(var1, var2, var3, var4);
         this.applyOutforce(var3, var4, var1, var2);
      }

   }

   private void applyOutforce(CollisionObject var1, CubeShape var2, CollisionObject var3, CubeShape var4) {
      var2.getSegmentBuffer().getSegmentController().stuckFrom = var3;
      var2.getSegmentBuffer().getSegmentController().stuckFromTime = System.currentTimeMillis();
      if (!(var2.getSegmentBuffer().getSegmentController() instanceof FloatingRock) && !(var2.getSegmentBuffer().getSegmentController() instanceof Ship)) {
         if (var4.getSegmentBuffer().getSegmentController() instanceof FloatingRock || var4.getSegmentBuffer().getSegmentController() instanceof Ship) {
            var4.getSegmentBuffer().getSegmentController().onStuck();
         }
      } else {
         var2.getSegmentBuffer().getSegmentController().onStuck();
      }

      StateInterface var10 = var2.getSegmentBuffer().getSegmentController().getState();
      Transform var5 = new Transform();
      SegmentController var7;
      if (var4.getSegmentBuffer().getSegmentController().getDockingController().isDocked()) {
         SegmentController var6;
         if ((var6 = var4.getSegmentBuffer().getSegmentController().getDockingController().getDockedOn().to.getSegment().getSegmentController()).getDockingController().isDocked()) {
            var7 = var6.getDockingController().getDockedOn().to.getSegment().getSegmentController();
            var5.set(var7.getWorldTransform());
         } else {
            var5.set(var6.getWorldTransform());
         }
      } else {
         var5.set(var4.getSegmentBuffer().getSegmentController().getWorldTransform());
      }

      Transform var12 = new Transform();
      if (var2.getSegmentBuffer().getSegmentController().getDockingController().isDocked()) {
         if ((var7 = var2.getSegmentBuffer().getSegmentController().getDockingController().getDockedOn().to.getSegment().getSegmentController()).getDockingController().isDocked()) {
            SegmentController var8 = var7.getDockingController().getDockedOn().to.getSegment().getSegmentController();
            var12.set(var8.getWorldTransform());
         } else {
            var12.set(var7.getWorldTransform());
         }
      } else {
         var12.set(var2.getSegmentBuffer().getSegmentController().getWorldTransform());
      }

      Vector3f var13;
      (var13 = new Vector3f()).sub(var12.origin, var5.origin);
      if (Float.compare(var13.lengthSquared(), 0.0F) != 0) {
         var13.length();
         var13.normalize();
      } else {
         var13.set(new Vector3f(0.0F, 1.0F, 0.0F));
      }

      var13.scale(1.0F);
      var12.origin.add(var13);
      System.err.println("[CC] " + var10 + " Objects detected stuck --- applying outforce: " + var2.getSegmentBuffer().getSegmentController() + " from " + var4.getSegmentBuffer().getSegmentController() + " DIR: " + var13);
      Vector3f var14;
      (var14 = ((RigidBodySegmentController)var1).getLinearVelocity(new Vector3f())).add(var13);
      ((RigidBodySegmentController)var1).setLinearVelocity(var14);

      for(int var9 = 0; var9 < this.manifoldPtr.getNumContacts(); ++var9) {
         ManifoldPoint var11;
         (var11 = this.manifoldPtr.getContactPoint(var9)).distance1 = 100.0F;
         if (this.manifoldPtr.getBody0() == var1) {
            var11.normalWorldOnB.set(var13);
         } else {
            var13.negate();
            var11.normalWorldOnB.set(var13);
         }
      }

   }

   private boolean doAABBIntersection(CubeShape var1, CubeShape var2) {
      this.v.tmpAABBTrans0.set(this.v.wtInv0);
      this.v.tmpAABBTrans0.mul(this.v.tmpTrans1);
      var2.getAabb(this.v.tmpAABBTrans0, this.v.innerAABBforA.min, this.v.innerAABBforA.max);
      var1.getAabb(TransformTools.ident, this.v.intersection.min, this.v.intersection.max);
      BoundingBox var3;
      if ((var3 = this.v.intersection.getIntersection(this.v.innerAABBforA, this.v.intersectionInASpaceWithB)) == null) {
         return false;
      } else {
         this.v.minIntA.x = ByteUtil.divSeg((int)(var3.min.x - 16.0F)) << 5;
         this.v.minIntA.y = ByteUtil.divSeg((int)(var3.min.y - 16.0F)) << 5;
         this.v.minIntA.z = ByteUtil.divSeg((int)(var3.min.z - 16.0F)) << 5;
         this.v.maxIntA.x = FastMath.fastCeil((var3.max.x + 16.0F) / 32.0F) << 5;
         this.v.maxIntA.y = FastMath.fastCeil((var3.max.y + 16.0F) / 32.0F) << 5;
         this.v.maxIntA.z = FastMath.fastCeil((var3.max.z + 16.0F) / 32.0F) << 5;
         this.v.tmpAABBTrans1.set(this.v.wtInv1);
         this.v.tmpAABBTrans1.mul(this.v.tmpTrans0);
         var1.getAabb(this.v.tmpAABBTrans1, this.v.outerAABBforB.min, this.v.outerAABBforB.max);
         var2.getAabb(TransformTools.ident, this.v.intersection.min, this.v.intersection.max);
         if ((var3 = this.v.intersection.getIntersection(this.v.outerAABBforB, this.v.intersectionInBSpaceWithA)) == null) {
            return false;
         } else {
            this.v.minIntB.x = ByteUtil.divSeg((int)(var3.min.x - 16.0F)) << 5;
            this.v.minIntB.y = ByteUtil.divSeg((int)(var3.min.y - 16.0F)) << 5;
            this.v.minIntB.z = ByteUtil.divSeg((int)(var3.min.z - 16.0F)) << 5;
            this.v.maxIntB.x = FastMath.fastCeil((var3.max.x + 16.0F) / 32.0F) << 5;
            this.v.maxIntB.y = FastMath.fastCeil((var3.max.y + 16.0F) / 32.0F) << 5;
            this.v.maxIntB.z = FastMath.fastCeil((var3.max.z + 16.0F) / 32.0F) << 5;
            return true;
         }
      }
   }

   class OuterSegmentAddHandler implements SegmentBufferIteratorInterface {
      private OuterSegmentAddHandler() {
      }

      public boolean handle(Segment var1, long var2) {
         if (var1.getSegmentData() != null && !var1.isEmpty()) {
            CubeCubeCollisionAlgorithm.this.v.outerNonEmptySegments.add(var1);
            CubeCubeCollisionAlgorithm.this.v.outerMaxBlock.x = Math.max(CubeCubeCollisionAlgorithm.this.v.outerMaxBlock.x, var1.getSegmentData().getMax().x + var1.pos.x);
            CubeCubeCollisionAlgorithm.this.v.outerMaxBlock.y = Math.max(CubeCubeCollisionAlgorithm.this.v.outerMaxBlock.y, var1.getSegmentData().getMax().y + var1.pos.y);
            CubeCubeCollisionAlgorithm.this.v.outerMaxBlock.z = Math.max(CubeCubeCollisionAlgorithm.this.v.outerMaxBlock.z, var1.getSegmentData().getMax().z + var1.pos.z);
            CubeCubeCollisionAlgorithm.this.v.outerMinBlock.x = Math.min(CubeCubeCollisionAlgorithm.this.v.outerMinBlock.x, var1.getSegmentData().getMin().x + var1.pos.x);
            CubeCubeCollisionAlgorithm.this.v.outerMinBlock.y = Math.min(CubeCubeCollisionAlgorithm.this.v.outerMinBlock.y, var1.getSegmentData().getMin().y + var1.pos.y);
            CubeCubeCollisionAlgorithm.this.v.outerMinBlock.z = Math.min(CubeCubeCollisionAlgorithm.this.v.outerMinBlock.z, var1.getSegmentData().getMin().z + var1.pos.z);
            return true;
         } else {
            return true;
         }
      }

      // $FF: synthetic method
      OuterSegmentAddHandler(Object var2) {
         this();
      }
   }

   class InnerSegmentAddHandler implements SegmentBufferIteratorInterface {
      private InnerSegmentAddHandler() {
      }

      public boolean handle(Segment var1, long var2) {
         if (var1.getSegmentData() != null && !var1.isEmpty()) {
            CubeCubeCollisionAlgorithm.this.v.innerNonEmptySegments.add(var1);
            CubeCubeCollisionAlgorithm.this.v.innerMaxBlock.x = Math.max(CubeCubeCollisionAlgorithm.this.v.innerMaxBlock.x, var1.getSegmentData().getMax().x + var1.pos.x);
            CubeCubeCollisionAlgorithm.this.v.innerMaxBlock.y = Math.max(CubeCubeCollisionAlgorithm.this.v.innerMaxBlock.y, var1.getSegmentData().getMax().y + var1.pos.y);
            CubeCubeCollisionAlgorithm.this.v.innerMaxBlock.z = Math.max(CubeCubeCollisionAlgorithm.this.v.innerMaxBlock.z, var1.getSegmentData().getMax().z + var1.pos.z);
            CubeCubeCollisionAlgorithm.this.v.innerMinBlock.x = Math.min(CubeCubeCollisionAlgorithm.this.v.innerMinBlock.x, var1.getSegmentData().getMin().x + var1.pos.x);
            CubeCubeCollisionAlgorithm.this.v.innerMinBlock.y = Math.min(CubeCubeCollisionAlgorithm.this.v.innerMinBlock.y, var1.getSegmentData().getMin().y + var1.pos.y);
            CubeCubeCollisionAlgorithm.this.v.innerMinBlock.z = Math.min(CubeCubeCollisionAlgorithm.this.v.innerMinBlock.z, var1.getSegmentData().getMin().z + var1.pos.z);
            return true;
         } else {
            return true;
         }
      }

      // $FF: synthetic method
      InnerSegmentAddHandler(Object var2) {
         this();
      }
   }

   public static class CreateFunc extends CollisionAlgorithmCreateFunc {
      private final ObjectPool pool = ObjectPool.get(CubeCubeCollisionAlgorithm.class);
      public SimplexSolverInterface simplexSolver;
      public GjkEpaPenetrationDepthSolverExt pdSolver;

      public CreateFunc(SimplexSolverInterface var1, GjkEpaPenetrationDepthSolverExt var2) {
         this.simplexSolver = var1;
         this.pdSolver = var2;
      }

      public CollisionAlgorithm createCollisionAlgorithm(CollisionAlgorithmConstructionInfo var1, CollisionObject var2, CollisionObject var3) {
         CubeCubeCollisionAlgorithm var4;
         (var4 = (CubeCubeCollisionAlgorithm)this.pool.get()).init(var1.manifold, var1, var2, var3, this.simplexSolver, this.pdSolver);
         return var4;
      }

      public void releaseCollisionAlgorithm(CollisionAlgorithm var1) {
         ((CubeCubeCollisionAlgorithm)var1).cleanUpDispatcher();
         this.pool.release((CubeCubeCollisionAlgorithm)var1);
      }
   }
}
