package org.schema.game.common.data.physics;

import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.CompoundShape;
import com.bulletphysics.collision.shapes.CompoundShapeChild;
import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.linearmath.VectorUtil;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.network.objects.container.CenterOfMassInterface;

public class CubesCompoundShape extends CompoundShape implements GameShape, CenterOfMassInterface {
   private static final boolean ONLY_MASTER_CENTER_OF_MASS = true;
   private final SegmentController segmentController;
   private final Vector3f localAabbMin = new Vector3f();
   private final Vector3f localAabbMax = new Vector3f();
   private final Vector3f tmpLocalAabbMin = new Vector3f();
   private final Vector3f tmpLocalAabbMax = new Vector3f();
   private final Vector3f _localAabbMin = new Vector3f();
   private final Vector3f _localAabbMax = new Vector3f();
   private final Vector3f localCenterTmp = new Vector3f();
   private final Vector3f localHalfExtentsTmp = new Vector3f();
   private final Matrix3f abs_bTmp = new Matrix3f();
   private final Vector3f centerTmp = new Vector3f();
   private final Vector3f tmp = new Vector3f();
   private final Vector3f extentTmp = new Vector3f();
   private final Vector3f centerOfMass = new Vector3f();
   private final Transform oTmp = new Transform();
   private final Transform oTmp2 = new Transform();
   Vector3f localAabbMinLast = new Vector3f(1.0E30F, 1.0E30F, 1.0E30F);
   Vector3f localAabbMaxLast = new Vector3f(-1.0E30F, -1.0E30F, -1.0E30F);
   private Transform lastTrans = new Transform();
   private short lastUpdateNum;
   private boolean changedAabb = false;
   Matrix3f tensor = new Matrix3f();
   Transform principal = new Transform();
   public final Vector3f lastPhysicsAABBMin = new Vector3f();
   public final Vector3f lastPhysicsAABBMax = new Vector3f();

   public CubesCompoundShape(SegmentController var1) {
      this.segmentController = var1;
   }

   public Vector3f getCenterOfMass() {
      if (((GameStateInterface)this.segmentController.getState()).getGameState().isWeightedCenterOfMass() && this.segmentController.getTotalPhysicalMass() > 0.0F && this.segmentController instanceof Ship) {
         this.centerOfMass.set(this.segmentController.getCenterOfMassUnweighted());
         this.centerOfMass.scale(1.0F / this.segmentController.getTotalPhysicalMass());
         return this.centerOfMass;
      } else {
         this.centerOfMass.set(0.0F, 0.0F, 0.0F);
         return this.centerOfMass;
      }
   }

   public boolean isVirtual() {
      return !this.segmentController.isOnServer() && this.segmentController.hasClientVirtual();
   }

   public float getTotalPhysicalMass() {
      return this.segmentController.getTotalPhysicalMass();
   }

   public void addChildShape(Transform var1, CollisionShape var2) {
      CubesCompoundShapeChild var3;
      (var3 = new CubesCompoundShapeChild()).transform.set(var1);
      var3.transformPivoted.set(var1);
      var3.childShape = var2;
      var3.childShapeType = var2.getShapeType();
      var3.childMargin = var2.getMargin();
      this.getChildList().add(var3);
      var2.getAabb(var1, this._localAabbMin, this._localAabbMax);
      VectorUtil.setMin(this.localAabbMin, this._localAabbMin);
      VectorUtil.setMax(this.localAabbMax, this._localAabbMax);
   }

   public void removeChildShape(CollisionShape var1) {
      super.removeChildShape(var1);
   }

   public Transform getChildTransform(int var1, Transform var2) {
      CubesCompoundShapeChild var3 = (CubesCompoundShapeChild)this.getChildList().getQuick(var1);

      assert var3 != null : var1 + "; " + this.segmentController;

      if (var3 != null) {
         var2.set(var3.transform);
         if (this.segmentController.getTotalPhysicalMass() > 0.0F) {
            Vector3f var4 = this.getCenterOfMass();
            var2.origin.sub(var4);
         }
      } else {
         var2.setIdentity();
         System.err.println("[PHYSICS][WARNING] no child shape for " + this.segmentController);
      }

      return var2;
   }

   public void getAabb(Transform var1, Vector3f var2, Vector3f var3) {
      if (!this.lastTrans.equals(var1) || this.changedAabb || this.lastUpdateNum != this.getSegmentController().getState().getNumberOfUpdate()) {
         this.localHalfExtentsTmp.sub(this.localAabbMax, this.localAabbMin);
         this.localHalfExtentsTmp.scale(0.5F);
         Vector3f var10000 = this.localHalfExtentsTmp;
         var10000.x += this.getMargin();
         var10000 = this.localHalfExtentsTmp;
         var10000.y += this.getMargin();
         var10000 = this.localHalfExtentsTmp;
         var10000.z += this.getMargin();
         this.localCenterTmp.add(this.localAabbMax, this.localAabbMin);
         this.localCenterTmp.scale(0.5F);
         this.abs_bTmp.set(var1.basis);
         MatrixUtil.absolute(this.abs_bTmp);
         this.centerTmp.set(this.localCenterTmp);
         var1.transform(this.centerTmp);
         this.abs_bTmp.getRow(0, this.tmp);
         this.extentTmp.x = this.tmp.dot(this.localHalfExtentsTmp);
         this.abs_bTmp.getRow(1, this.tmp);
         this.extentTmp.y = this.tmp.dot(this.localHalfExtentsTmp);
         this.abs_bTmp.getRow(2, this.tmp);
         this.extentTmp.z = this.tmp.dot(this.localHalfExtentsTmp);
         this.lastTrans.set(var1);
         this.lastUpdateNum = this.getSegmentController().getState().getNumberOfUpdate();
      }

      var2.sub(this.centerTmp, this.extentTmp);
      var3.add(this.centerTmp, this.extentTmp);
   }

   public void recalculateLocalAabb() {
      this.localAabbMin.set(1.0E30F, 1.0E30F, 1.0E30F);
      this.localAabbMax.set(-1.0E30F, -1.0E30F, -1.0E30F);
      this.changedAabb = false;

      for(int var1 = 0; var1 < this.getChildList().size(); ++var1) {
         Transform var2 = this.getChildTransform(var1, this.oTmp);
         ((CompoundShapeChild)this.getChildList().getQuick(var1)).childShape.getAabb(var2, this.tmpLocalAabbMin, this.tmpLocalAabbMax);

         for(int var3 = 0; var3 < 3; ++var3) {
            if (VectorUtil.getCoord(this.localAabbMin, var3) > VectorUtil.getCoord(this.tmpLocalAabbMin, var3)) {
               VectorUtil.setCoord(this.localAabbMin, var3, VectorUtil.getCoord(this.tmpLocalAabbMin, var3));
            }

            if (VectorUtil.getCoord(this.localAabbMax, var3) < VectorUtil.getCoord(this.tmpLocalAabbMax, var3)) {
               VectorUtil.setCoord(this.localAabbMax, var3, VectorUtil.getCoord(this.tmpLocalAabbMax, var3));
            }
         }
      }

      if (!this.localAabbMinLast.equals(this.localAabbMin) || !this.localAabbMaxLast.equals(this.localAabbMax)) {
         this.localAabbMinLast.set(this.localAabbMin);
         this.localAabbMaxLast.set(this.localAabbMax);
         this.changedAabb = true;
      }

   }

   public void getAABB(int var1, Transform var2, Vector3f var3, Vector3f var4, AABBVarSet var5) {
      this.oTmp.set(var2);
      CollisionShape var6 = this.getChildShape(var1);
      this.getChildTransform(var1, this.oTmp2);
      this.oTmp.mul(this.oTmp2);
      var6.getAabb(this.oTmp, var3, var4);
   }

   public void calculateLocalInertia(float var1, Vector3f var2) {
      this.principal.setIdentity();
      this.tensor.set(this.getSegmentController().tensor);
      MatrixUtil.diagonalize(this.tensor, this.principal.basis, 1.0E-5F, 20);
      var2.set(Math.max(0.0F, this.tensor.m00), Math.max(0.0F, this.tensor.m11), Math.max(0.0F, this.tensor.m22));
   }

   public SegmentController getSegmentController() {
      return this.segmentController;
   }

   public boolean isCompound() {
      return true;
   }

   public String toString() {
      return "[CCS" + (this.segmentController.isOnServer() ? "|SER " : "|CLI ") + this.segmentController + "]";
   }

   public SimpleTransformableSendableObject getSimpleTransformableSendableObject() {
      return this.segmentController;
   }
}
