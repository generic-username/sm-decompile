package org.schema.game.common.data.physics;

import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.MotionState;
import javax.vecmath.Vector3f;
import org.schema.game.client.view.shards.Shard;

public class RigidDebrisBody extends RigidBodyExt {
   public Shard shard;
   Vector3f linTmp = new Vector3f();
   Vector3f angTmp = new Vector3f();
   public boolean noCollision;

   public RigidDebrisBody(float var1, MotionState var2, CollisionShape var3) {
      super(CollisionType.DEBRIS, var1, var2, var3);
   }

   public RigidDebrisBody(float var1, MotionState var2, CollisionShape var3, Vector3f var4) {
      super(CollisionType.DEBRIS, var1, var2, var3, var4);
   }

   public RigidDebrisBody(RigidBodyConstructionInfo var1) {
      super(CollisionType.DEBRIS, var1);
   }

   public void activate(boolean var1) {
      if (var1) {
         this.setActivationState(1);
         this.deactivationTime = 0.0F;
      }

   }

   public void updateDeactivation(float var1) {
      if (this.getActivationState() != 2 && this.getActivationState() != 4) {
         float var2 = this.getLinearSleepingThreshold() * this.getLinearSleepingThreshold();
         float var3 = this.getAngularSleepingThreshold() * this.getAngularSleepingThreshold();
         if (this.getLinearVelocity(this.linTmp).lengthSquared() < var2 && this.getAngularVelocity(this.angTmp).lengthSquared() < var3) {
            this.deactivationTime += var1;
         } else {
            this.deactivationTime = 0.0F;
            this.setActivationState(0);
         }
      }
   }
}
