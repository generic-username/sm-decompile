package org.schema.game.common.data.physics;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;

public class RayCubeGridSolver {
   private final Vector3f inputStart = new Vector3f();
   private final Vector3f inputEnd = new Vector3f();
   private final Vector3f dir = new Vector3f();
   private final RayTraceGridTraverser tra = new RayTraceGridTraverser();
   private final Ray r = new Ray();
   private final Transform inv = new Transform();
   public boolean ok;
   private float MIN_DEPTH;
   private RayCubeGridSolver.Granularity gran;
   private static long lastExceptionMinS;
   private static long lastExceptionMin;
   private static long lastException;
   private static final float oneSegInv = 0.03125F;
   public static float sectorInv;
   public static float sectorHalf;

   private void initialize(Vector3f var1, Vector3f var2, Transform var3, RayCubeGridSolver.Granularity var4) {
      this.ok = true;
      this.gran = var4;
      if (var4 == RayCubeGridSolver.Granularity.SEGMENT) {
         this.MIN_DEPTH = 3.0F;
      } else {
         this.MIN_DEPTH = 0.01F;
      }

      this.inputStart.set(var1);
      this.inputEnd.set(var2);
      this.inv.set(var3);
      this.inv.inverse();
      this.inv.transform(this.inputStart);
      this.inv.transform(this.inputEnd);
      float var6 = 0.0F;
      if (var4 == RayCubeGridSolver.Granularity.BLOCK) {
         var6 = 0.5F;
      } else if (var4 == RayCubeGridSolver.Granularity.SEGMENT) {
         var6 = 16.5F;
      } else if (var4 == RayCubeGridSolver.Granularity.SECTOR) {
         assert sectorHalf > 0.0F;

         var6 = sectorHalf;
      }

      Vector3f var10000 = this.inputStart;
      var10000.x += var6;
      var10000 = this.inputStart;
      var10000.y += var6;
      var10000 = this.inputStart;
      var10000.z += var6;
      var10000 = this.inputEnd;
      var10000.x += var6;
      var10000 = this.inputEnd;
      var10000.y += var6;
      var10000 = this.inputEnd;
      var10000.z += var6;
      if (var4 == RayCubeGridSolver.Granularity.SEGMENT) {
         var10000 = this.inputStart;
         var10000.x *= 0.03125F;
         var10000 = this.inputStart;
         var10000.y *= 0.03125F;
         var10000 = this.inputStart;
         var10000.z *= 0.03125F;
         var10000 = this.inputEnd;
         var10000.x *= 0.03125F;
         var10000 = this.inputEnd;
         var10000.y *= 0.03125F;
         var10000 = this.inputEnd;
         var10000.z *= 0.03125F;
      } else if (var4 == RayCubeGridSolver.Granularity.SECTOR) {
         assert sectorInv != 0.0F;

         var10000 = this.inputStart;
         var10000.x *= sectorInv;
         var10000 = this.inputStart;
         var10000.y *= sectorInv;
         var10000 = this.inputStart;
         var10000.z *= sectorInv;
         var10000 = this.inputEnd;
         var10000.x *= sectorInv;
         var10000 = this.inputEnd;
         var10000.y *= sectorInv;
         var10000 = this.inputEnd;
         var10000.z *= sectorInv;
      }

      this.dir.sub(this.inputEnd, this.inputStart);
      int var7;
      if ((var7 = FastMath.fastCeil(var6 = this.dir.length() + 1.0F)) > 10000 && System.currentTimeMillis() > lastExceptionMinS + 2000L) {
         try {
            throw new Exception("RAYTRACE [WARNING] Trace > 10000:::  DIR: " + this.dir + ";; LEN/MAX " + var6 + "/" + var7 + "; start " + this.inputStart + "; end " + this.inputEnd + "; ORIG: " + var1 + " -> " + var2);
         } catch (Exception var5) {
            var5.printStackTrace();
            lastExceptionMinS = System.currentTimeMillis();
         }
      }

      if (this.dir.lengthSquared() == 0.0F) {
         System.err.println("[TRAVERSE] Exception: Tried to traverse on zero length direction from " + this.inputStart + " -> " + this.inputEnd);
         this.ok = false;
      } else {
         this.dir.scale(1.2F);
         if (this.dir.length() < this.MIN_DEPTH) {
            this.dir.normalize();
            this.dir.scale(this.MIN_DEPTH + 0.5F);
         }

         this.r.direction.set(this.dir);
         this.r.position.set(this.inputStart);
      }
   }

   public void initializeBlockGranularity(Vector3f var1, Vector3f var2, Transform var3) {
      this.initialize(var1, var2, var3, RayCubeGridSolver.Granularity.BLOCK);
   }

   public void initializeSegmentGranularity(Vector3f var1, Vector3f var2, Transform var3) {
      this.initialize(var1, var2, var3, RayCubeGridSolver.Granularity.SEGMENT);
   }

   public void initializeSectorGranularity(Vector3f var1, Vector3f var2, Transform var3) {
      this.initialize(var1, var2, var3, RayCubeGridSolver.Granularity.SECTOR);
   }

   public void traverseSegmentsOnRay(SegmentTraversalInterface var1) {
      if (this.ok) {
         float var2;
         int var3;
         if (!Float.isInfinite((float)(var3 = FastMath.fastCeil(var2 = this.dir.length() + 1.0F))) && !Float.isNaN((float)var3)) {
            if (var3 > 10000) {
               if (System.currentTimeMillis() > lastExceptionMin + 200L) {
                  System.err.println("RAYTRACE [WARNING] Trace > 10000:::  DIR: " + this.dir + ";; LEN/MAX " + var2 + "/" + var3 + "; start " + this.inputStart + "; end " + this.inputEnd + "; ");
                  lastExceptionMin = System.currentTimeMillis();
               }

               if (System.currentTimeMillis() > lastException + 4000L) {
                  try {
                     throw new Exception("RAYTRACE [WARNING] Trace > 10000:::  DIR: " + this.dir + ";; LEN/MAX " + var2 + "/" + var3 + "; start " + this.inputStart + "; end " + this.inputEnd + "; ");
                  } catch (Exception var4) {
                     var4.printStackTrace();
                     lastException = System.currentTimeMillis();
                  }
               }

               var3 = 10000;
            }

            this.tra.getCellsOnRay(this.r, (int)Math.max(this.MIN_DEPTH, (float)var3), var1);
         } else {
            System.err.println("RAYTRACE [ERROR]::: Not calculating: DIR not a number " + this.dir + ";; " + var2 + "/" + var3 + "; " + this.inputStart + "; " + this.inputEnd + "; ");
         }
      }
   }

   public static enum Granularity {
      BLOCK,
      SEGMENT,
      SECTOR;
   }
}
