package org.schema.game.common.data.physics;

import com.bulletphysics.collision.broadphase.BroadphaseProxy;
import com.bulletphysics.collision.broadphase.DbvtAabbMm;
import com.bulletphysics.collision.broadphase.DbvtBroadphase;
import com.bulletphysics.collision.broadphase.DbvtProxy;
import com.bulletphysics.collision.broadphase.Dispatcher;
import com.bulletphysics.collision.broadphase.HashedOverlappingPairCache;
import com.bulletphysics.collision.broadphase.OverlappingPairCache;
import javax.vecmath.Vector3f;

public class DbvtBroadphaseExt extends DbvtBroadphase {
   private final Vector3f delta;
   private final Vector3f centerOut;
   private DbvtAabbMm tmpAabb;

   public DbvtBroadphaseExt() {
      this((OverlappingPairCache)null);
   }

   public DbvtBroadphaseExt(OverlappingPairCache var1) {
      this.delta = new Vector3f();
      this.centerOut = new Vector3f();
      this.tmpAabb = new DbvtAabbMm();
      this.sets[0] = new DbvtExt();
      this.sets[1] = new DbvtExt();
      this.releasepaircache = var1 == null;
      this.predictedframes = 2.0F;
      this.stageCurrent = 0;
      this.fupdates = 1;
      this.dupdates = 1;
      this.paircache = (OverlappingPairCache)(var1 != null ? var1 : new HashedOverlappingPairCache());
      this.gid = 0;
      this.pid = 0;

      for(int var2 = 0; var2 <= 2; ++var2) {
         this.stageRoots[var2] = null;
      }

   }

   private static DbvtProxy listappend(DbvtProxy var0, DbvtProxy var1) {
      var0.links[0] = null;
      var0.links[1] = var1;
      if (var1 != null) {
         var1.links[0] = var0;
      }

      return var0;
   }

   private static DbvtProxy listremove(DbvtProxy var0, DbvtProxy var1) {
      if (var0.links[0] != null) {
         var0.links[0].links[1] = var0.links[1];
      } else {
         var1 = var0.links[1];
      }

      if (var0.links[1] != null) {
         var0.links[1].links[0] = var0.links[0];
      }

      return var1;
   }

   public void setAabb(BroadphaseProxy var1, Vector3f var2, Vector3f var3, Dispatcher var4) {
      DbvtProxy var5 = (DbvtProxy)var1;
      DbvtAabbMm var6 = DbvtAabbMm.FromMM(var2, var3, this.tmpAabb);
      if (var5.stage == 2) {
         this.sets[1].remove(var5.leaf);
         var5.leaf = this.sets[0].insert(var6, var5);
      } else if (DbvtAabbMm.Intersect(var5.leaf.volume, var6)) {
         this.delta.add(var2, var3);
         this.delta.scale(0.5F);
         this.delta.sub(var5.aabb.Center(this.centerOut));
         this.delta.scale(this.predictedframes);
         this.sets[0].update(var5.leaf, var6, this.delta, 0.05F);
      } else {
         this.sets[0].update(var5.leaf, var6);
      }

      this.stageRoots[var5.stage] = listremove(var5, this.stageRoots[var5.stage]);
      var5.aabb.set(var6);
      var5.stage = this.stageCurrent;
      this.stageRoots[this.stageCurrent] = listappend(var5, this.stageRoots[this.stageCurrent]);
   }

   public void clean() {
      this.sets[0] = null;
      this.sets[1] = null;
   }
}
