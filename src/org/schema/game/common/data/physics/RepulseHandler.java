package org.schema.game.common.data.physics;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.shapes.SphereShape;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectArrayList;
import it.unimi.dsi.fastutil.longs.Long2ByteOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ByteMap.Entry;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.graphicsengine.core.Timer;

public class RepulseHandler implements PowerConsumer {
   private final Ship ship;
   private final SphereShape sphere = new SphereShape(5.0F);
   private float thrustToRepul;
   public static final int MAX_REPULSE_BLOCKS = 32;
   private final Long2ByteOpenHashMap repulseBlocks = new Long2ByteOpenHashMap(32);
   private Vector3f p = new Vector3f();
   private Transform trans = new Transform();
   private float prefDist;
   private float force;
   private float powered;
   private long lastActive;
   private boolean inUse;

   public RepulseHandler(Ship var1) {
      this.ship = var1;
   }

   public void add(long var1, byte var3) {
      long var4 = ElementCollection.getPosIndexFrom4(var1);
      this.repulseBlocks.add(var4, var3);
   }

   public void remove(long var1) {
      long var3 = ElementCollection.getPosIndexFrom4(var1);
      this.repulseBlocks.remove(var3);
   }

   public int getRepulsorSize() {
      return this.repulseBlocks.size();
   }

   public boolean hasRepulsors() {
      return this.repulseBlocks.size() > 0;
   }

   public float getThrustToRepul() {
      return this.thrustToRepul;
   }

   public void setThrustToRepul(float var1) {
      this.thrustToRepul = Math.min(1.0F, Math.max(0.0F, var1));
   }

   public void handle(Timer var1) {
      this.force = this.ship.getManagerContainer().getThrusterElementManager().getActualThrust() * VoidElementManager.REPULSE_MULT * this.thrustToRepul / (float)this.getRepulsorSize() * this.getPowered();
      Iterator var2 = this.repulseBlocks.long2ByteEntrySet().iterator();

      while(var2.hasNext()) {
         Entry var3 = (Entry)var2.next();
         this.handleSingle(var1, var3.getLongKey(), var3.getByteValue());
      }

   }

   public void handleSingle(Timer var1, long var2, byte var4) {
      PhysicsExt var5 = this.ship.getPhysics();
      CollisionObject var6 = this.ship.getPhysicsDataContainer().getObject();
      if (var5 != null && var6 != null && var6 instanceof RigidBodySegmentController) {
         ElementCollection.getPosFromIndex(var2, this.p);
         Vector3f var10000 = this.p;
         var10000.x -= 16.0F;
         var10000 = this.p;
         var10000.y -= 16.0F;
         var10000 = this.p;
         var10000.z -= 16.0F;
         this.trans.set(this.ship.getWorldTransform());
         this.trans.basis.transform(this.p);
         this.trans.origin.add(this.p);
         ClosestConvexResultCallbackExt var9;
         (var9 = new ClosestConvexResultCallbackExt(this.trans.origin, this.trans.origin)).ownerObject = var6;
         var9.sphereOverlapping = new ObjectArrayList();
         var9.sphereOverlappingNormals = new ObjectArrayList();
         var9.sphereDontHitOwner = true;
         var5.getDynamicsWorld().convexSweepTest(this.sphere, this.trans, this.trans, var9);
         if (var9.hasHit()) {
            this.inUse = true;
            this.lastActive = var1.currentTime;
            RigidBodySegmentController var8 = (RigidBodySegmentController)var6;
            Vector3f var3 = new Vector3f(Element.DIRECTIONSf[var4]);
            this.trans.basis.transform(var3);
            this.prefDist = 5.0F;
            float var13 = Math.min(this.prefDist + 0.5F, this.sphere.getRadius() + 0.5F);
            Vector3f var14 = new Vector3f((Vector3f)var9.sphereOverlappingNormals.get(0));
            float var15 = Math.min(Vector3fTools.diffLength((Vector3f)var9.sphereOverlapping.get(0), this.trans.origin), var13);
            Iterator var10 = var9.sphereOverlapping.iterator();

            float var7;
            while(var10.hasNext()) {
               var7 = Vector3fTools.diffLength((Vector3f)var10.next(), this.trans.origin);
               if (var15 > var7) {
                  var15 = var7;
               }
            }

            float var11 = this.force + this.force * FastMath.cos(var15 * 3.1415927F / var13);
            var15 = 2.0F * this.force * FastMath.cos(var15 * 3.1415927F / var13);
            var7 = FastMath.cos(FastMath.acos(Vector3fTools.dot(var3, var14)));
            var13 = 0.2F;
            if (!this.ship.isConrolledByActivePlayer()) {
               var13 = 0.4F;
            }

            var3 = new Vector3f();
            var8.getGravity(var3);
            float var12;
            if (var3.length() != 0.0F) {
               var7 = Math.max(0.0F, var7);
               var11 *= var7;
               var12 = 0.2F * var7;
               var13 *= var7;
            } else {
               var11 = var15 * var7;
               var12 = 0.2F * var7;
               var13 *= var7;
            }

            var14.scale(var11);
            var8.setDamping(var12, var13);
            var8.applyForce(var14, this.p);
         }
      }

   }

   public double getPowerConsumedPerSecondResting() {
      return this.ship.getManagerContainer().getThrusterElementManager().getPowerConsumedPerSecondResting() * (double)this.thrustToRepul;
   }

   public double getPowerConsumedPerSecondCharging() {
      return this.ship.getManagerContainer().getThrusterElementManager().getPowerConsumedPerSecondCharging() * (double)this.thrustToRepul;
   }

   public boolean isPowerCharging(long var1) {
      if (this.inUse && var1 - this.lastActive > 500L) {
         this.lastActive = var1;
         this.inUse = false;
      }

      return this.inUse;
   }

   public void setPowered(float var1) {
      this.powered = var1;
   }

   public float getPowered() {
      return this.powered;
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.THRUST;
   }

   public void reloadFromReactor(double var1, Timer var3, float var4, boolean var5, float var6) {
   }

   public boolean isPowerConsumerActive() {
      return true;
   }

   public String getName() {
      return "Repulse";
   }

   public void dischargeFully() {
   }
}
