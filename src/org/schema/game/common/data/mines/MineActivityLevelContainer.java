package org.schema.game.common.data.mines;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.schema.game.common.controller.elements.mines.MineController;
import org.schema.schine.graphicsengine.core.Timer;

public class MineActivityLevelContainer {
   private final MineActivityLevelContainer.MineHashSet[] actLevelMines = new MineActivityLevelContainer.MineHashSet[MineActivityLevelContainer.ActiveLevel.values().length];
   private final long[] lastUpdateActLevel = new long[MineActivityLevelContainer.ActiveLevel.values().length];
   private final List tmpMineUpdate = new ObjectArrayList();
   private int lastSecEntities;

   public MineActivityLevelContainer() {
      MineActivityLevelContainer.ActiveLevel[] var1;
      int var2 = (var1 = MineActivityLevelContainer.ActiveLevel.values()).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         MineActivityLevelContainer.ActiveLevel var4 = var1[var3];
         this.actLevelMines[var4.ordinal()] = new MineActivityLevelContainer.MineHashSet();
      }

   }

   public void updateActiveLevel(Mine var1, MineActivityLevelContainer.ActiveLevel var2, MineActivityLevelContainer.ActiveLevel var3) {
      if (var2 != var3) {
         this.actLevelMines[var2.ordinal()].remove(var1);
         this.actLevelMines[var3.ordinal()].add(var1);
      }

   }

   public void updateLocal(Timer var1, MineController var2) {
      MineActivityLevelContainer.ActiveLevel[] var3 = MineActivityLevelContainer.ActiveLevel.values();

      for(int var4 = 0; var4 < var3.length; ++var4) {
         MineActivityLevelContainer.ActiveLevel var5;
         if ((var5 = var3[var4]).updateMilliSecs >= 0 && var1.currentTime - this.lastUpdateActLevel[var4] > (long)var5.updateMilliSecs) {
            this.tmpMineUpdate.addAll(this.actLevelMines[var4]);
            var2.handleMineUpdate(var1, this.tmpMineUpdate);
            if (var5 == MineActivityLevelContainer.ActiveLevel.COLLISION_CHECK) {
               var2.handleMineCollisions(var1, this.tmpMineUpdate);
            }

            this.lastUpdateActLevel[var4] = var1.currentTime;
         }

         this.tmpMineUpdate.clear();
      }

   }

   public void onSectorChange(Collection var1, Collection var2) {
      Mine var3;
      Iterator var4;
      if (var1.isEmpty()) {
         var4 = var2.iterator();

         while(var4.hasNext()) {
            if ((var3 = (Mine)var4.next()).getActiveLevel() != MineActivityLevelContainer.ActiveLevel.INACTIVE) {
               var3.setActiveLevel(MineActivityLevelContainer.ActiveLevel.INACTIVE);
            }
         }
      } else if (this.lastSecEntities == 0) {
         var4 = var2.iterator();

         while(var4.hasNext()) {
            if ((var3 = (Mine)var4.next()).getActiveLevel() == MineActivityLevelContainer.ActiveLevel.INACTIVE) {
               var3.updateActivityLevelOnly(var1);
            }
         }
      }

      this.lastSecEntities = var1.size();
   }

   public void add(Mine var1) {
      this.actLevelMines[var1.getActiveLevel().ordinal()].add(var1);
   }

   public void clearActivity() {
      this.lastSecEntities = 0;
   }

   public void remove(Mine var1) {
      this.actLevelMines[var1.getActiveLevel().ordinal()].remove(var1);
   }

   public void clear() {
      this.clearActivity();
      MineActivityLevelContainer.MineHashSet[] var1;
      int var2 = (var1 = this.actLevelMines).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         var1[var3].clear();
      }

   }

   public static enum ActiveLevel {
      INACTIVE(Float.POSITIVE_INFINITY, 5000),
      STANDBY(2200.0F, 2000),
      WAKING(1000.0F, 1000),
      AWAKE(700.0F, 700),
      ACTIVE(500.0F, 120),
      ALERT(250.0F, 60),
      COLLISION_CHECK(Float.NEGATIVE_INFINITY, 30);

      public final float distance;
      public final int updateMilliSecs;

      private ActiveLevel(float var3, int var4) {
         this.distance = var3;
         this.updateMilliSecs = var4;
      }
   }

   public static class MineHashSet extends ObjectOpenHashSet {
      private static final long serialVersionUID = 753118082558116039L;
   }
}
