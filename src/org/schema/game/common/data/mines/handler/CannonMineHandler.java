package org.schema.game.common.data.mines.handler;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.projectile.ProjectileController;
import org.schema.game.common.controller.elements.power.reactor.MainReactorUnit;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.mines.Mine;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.Mesh;

public class CannonMineHandler extends MineHandler {
   public int damage;
   public InterEffectSet attackEffect;
   public long shotFreqMilli = 500L;
   public float projectileSpeed = 1.0F;
   public boolean pointDefense;
   public final Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   public int shootAtTargetCount = -1;
   private long lastShot;
   private final Vector3f targetTmp = new Vector3f();
   private final List outputs = new ObjectArrayList();
   private int shotNumber;
   private Transform fromTrans = new Transform();
   private Vector3f outputPos = new Vector3f();

   public CannonMineHandler(Mine var1) {
      super(var1);
   }

   public void handleActive(Timer var1, List var2) {
      if (var1.currentTime - this.lastShot > this.shotFreqMilli) {
         if (this.mine.getAmmo() > 0) {
            if (this.mine.isOnServer()) {
               GameServerState var3 = (GameServerState)this.mine.getState();
               this.shootOnServer(var3, var1, var2);
            } else {
               GameClientState var4 = (GameClientState)this.mine.getState();
               this.shootOnClient(var4, var1, var2);
            }
         }

         this.lastShot = var1.currentTime;
         if (this.mine.isOnServer()) {
            this.mine.setAmmoServer((short)(this.mine.getAmmo() - 1));
            return;
         }

         this.mine.setAmmo((short)(this.mine.getAmmo() - 1));
      }

   }

   protected void shootOnClient(GameClientState var1, Timer var2, List var3) {
      int var4 = 0;
      Iterator var6 = var3.iterator();

      while(var6.hasNext()) {
         SimpleGameObject var5 = (SimpleGameObject)var6.next();
         this.shootAt(var2, var5, this.mine.getWorldTransformOnClient(), var5.getWorldTransformOnClient(), var1.getParticleController());
         ++var4;
         if (this.shootAtTargetCount > 0 && var4 >= this.shootAtTargetCount) {
            break;
         }
      }

   }

   private void shootAt(Timer var1, SimpleGameObject var2, Transform var3, Transform var4, ProjectileController var5) {
      this.targetTmp.set(0.0F, 0.0F, 0.0F);
      if (var2 instanceof SegmentController && ((SegmentController)var2).hasActiveReactors()) {
         List var9 = ((ManagedSegmentController)var2).getManagerContainer().getPowerInterface().getMainReactors();
         long var7 = Long.MIN_VALUE;
         Iterator var10 = var9.iterator();

         while(var10.hasNext()) {
            MainReactorUnit var6;
            if ((var6 = (MainReactorUnit)var10.next()).getSignificator() > var7) {
               var7 = var6.getSignificator();
            }
         }

         ElementCollection.getPosFromIndex(var7, this.targetTmp);
         Vector3f var10000 = this.targetTmp;
         var10000.x -= 16.0F;
         var10000 = this.targetTmp;
         var10000.y -= 16.0F;
         var10000 = this.targetTmp;
         var10000.z -= 16.0F;
      }

      this.fromTrans.set(var3);
      var4.transform(this.targetTmp);
      this.targetTmp.sub(this.fromTrans.origin);
      if (this.targetTmp.lengthSquared() > 0.0F) {
         this.targetTmp.normalize();
         this.targetTmp.scale(this.projectileSpeed);

         assert this.mine != null : var2 + "; " + var5;

         assert var5 != null : this.mine;

         assert var3 != null : this.mine;

         if (!var5.isOnServer()) {
            AbstractSceneNode var11;
            if (!this.outputs.isEmpty()) {
               var11 = (AbstractSceneNode)this.outputs.get(this.shotNumber % this.outputs.size());
               this.outputPos.set(var11.getInitionPos());
               this.fromTrans.basis.transform(this.outputPos);
               this.fromTrans.origin.add(this.outputPos);
            } else {
               Iterator var12 = ((AbstractSceneNode)Controller.getResLoader().getMesh("MineTypeA-Active").getChilds().get(0)).getChilds().iterator();

               while(var12.hasNext()) {
                  if (!((var11 = (AbstractSceneNode)var12.next()) instanceof Mesh)) {
                     this.outputs.add(var11);
                  }
               }

               Collections.sort(this.outputs, new Comparator() {
                  public int compare(AbstractSceneNode var1, AbstractSceneNode var2) {
                     return var1.getName().toLowerCase(Locale.ENGLISH).compareTo(var2.getName().toLowerCase(Locale.ENGLISH));
                  }
               });
            }
         }

         var5.addProjectile(this.mine, this.fromTrans.origin, this.targetTmp, (float)this.damage, ((GameStateInterface)this.mine.getState()).getGameState().getWeaponRangeReference() / 2.0F, 0, 5.0F, 10, 10.0F, -9223372036854775776L, this.color);
         ++this.shotNumber;
      }

   }

   protected void shootOnServer(GameServerState var1, Timer var2, List var3) {
      Iterator var5 = var3.iterator();

      while(var5.hasNext()) {
         SimpleGameObject var6 = (SimpleGameObject)var5.next();
         ProjectileController var4 = this.mine.serverInfo.sector.getParticleController();
         if (this.mine.serverInfo.sector.isActive() && var4 != null) {
            this.shootAt(var2, var6, this.mine.getWorldTransform(), var6.getWorldTransform(), var4);
         }
      }

   }

   public void onBecomingActive(List var1) {
   }

   public void handleInactive(Timer var1) {
   }

   public void onBecomingInactive() {
   }

   public InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3) {
      return this.attackEffect;
   }

   public boolean isPointDefense() {
      return this.pointDefense;
   }
}
