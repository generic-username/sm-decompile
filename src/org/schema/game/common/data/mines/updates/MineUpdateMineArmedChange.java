package org.schema.game.common.data.mines.updates;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.common.controller.elements.mines.MineController;

public class MineUpdateMineArmedChange extends MineUpdateMineChange {
   public boolean armed;

   public MineUpdate.MineUpdateType getType() {
      return MineUpdate.MineUpdateType.MINE_ARM;
   }

   protected void serializeData(DataOutput var1, boolean var2) throws IOException {
      super.serializeData(var1, var2);
      var1.writeBoolean(this.armed);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.armed = var1.readBoolean();
   }

   public void execute(ClientChannel var1, MineController var2) {
      var2.changeMineArmedClient(this.mineId, this.armed);
   }
}
