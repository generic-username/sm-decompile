package org.schema.game.common.data.mines.updates;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.common.controller.elements.mines.MineController;

public class MineUpdateMineHit extends MineUpdateMineChange {
   public int sectorId;

   public MineUpdate.MineUpdateType getType() {
      return MineUpdate.MineUpdateType.MINE_HIT;
   }

   protected void serializeData(DataOutput var1, boolean var2) throws IOException {
      super.serializeData(var1, var2);
      var1.writeInt(this.sectorId);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.sectorId = var1.readInt();
   }

   public void execute(ClientChannel var1, MineController var2) {
      var2.mineHitServer(this.mineId, this.sectorId);
   }
}
