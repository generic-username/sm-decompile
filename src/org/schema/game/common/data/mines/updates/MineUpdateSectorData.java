package org.schema.game.common.data.mines.updates;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.elements.mines.MineController;
import org.schema.game.common.data.mines.Mine;
import org.schema.game.common.data.mines.ServerMineManager;
import org.schema.schine.network.SerialializationInterface;

public class MineUpdateSectorData extends MineUpdate {
   private MineUpdateSectorData.MineData[] mineData;
   private int sectorId;

   public MineUpdate.MineUpdateType getType() {
      return MineUpdate.MineUpdateType.SECTOR_DATA;
   }

   protected void serializeData(DataOutput var1, boolean var2) throws IOException {
      var1.writeInt(this.sectorId);
      var1.writeInt(this.mineData.length);

      for(int var3 = 0; var3 < this.mineData.length; ++var3) {
         this.mineData[var3].serialize(var1, var2);
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.sectorId = var1.readInt();
      this.mineData = new MineUpdateSectorData.MineData[var1.readInt()];

      for(int var4 = 0; var4 < this.mineData.length; ++var4) {
         this.mineData[var4] = new MineUpdateSectorData.MineData();
         this.mineData[var4].deserialize(var1, var2, var3);
      }

   }

   public MineUpdateSectorData.MineData[] getMineData() {
      return this.mineData;
   }

   public void setMineData(MineUpdateSectorData.MineData[] var1) {
      this.mineData = var1;
   }

   public int getSectorId() {
      return this.sectorId;
   }

   public void setSectorId(int var1) {
      this.sectorId = var1;
   }

   public void execute(ClientChannel var1, MineController var2) {
      MineUpdateSectorData.MineData[] var7;
      int var3 = (var7 = this.mineData).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         MineUpdateSectorData.MineData var5 = var7[var4];

         try {
            var2.addMineClient(var5, this.sectorId);
         } catch (Mine.MineDataException var6) {
            var6.printStackTrace();
         }
      }

   }

   public void setFrom(ServerMineManager var1) {
      this.mineData = new MineUpdateSectorData.MineData[var1.getMines().size()];
      int var2 = 0;

      for(Iterator var3 = var1.getMines().values().iterator(); var3.hasNext(); ++var2) {
         Mine var4 = (Mine)var3.next();
         MineUpdateSectorData.MineData var5;
         (var5 = new MineUpdateSectorData.MineData()).setFrom(var4);
         this.mineData[var2] = var5;
      }

      this.sectorId = var1.getSectorId();
   }

   public static class MineData implements SerialializationInterface {
      public int id;
      public short hp;
      public short ammo;
      public long ownerId;
      public int factionId;
      public short[] composition;
      public Vector3f localPos;
      public boolean armed;

      public void setFrom(Mine var1) {
         this.id = var1.getId();
         this.hp = var1.getHp();
         this.ownerId = var1.getOwnerId();
         this.factionId = var1.getFactionId();
         this.composition = new short[6];
         this.localPos = new Vector3f(var1.getWorldTransform().origin);
         this.armed = var1.isArmed();
         this.ammo = var1.getAmmo();
         var1.getComposition(this.composition);
      }

      public Mine initDeserialization(GameClientState var1, int var2, Vector3i var3) {
         Mine var4;
         (var4 = new Mine(var1)).setId(this.id);
         var4.setHp(this.hp);
         var4.setOwnerId(this.ownerId);
         var4.setFactionId(this.factionId);
         var4.setCompositionByVal(this.composition);
         var4.setSectorId(var2);
         var4.setSectorPos(new Vector3i(var3));
         var4.getWorldTransform().origin.set(this.localPos);
         var4.setArmed(this.armed);
         var4.setAmmo(this.ammo);
         return var4;
      }

      public void serialize(DataOutput var1, boolean var2) throws IOException {
         var1.writeInt(this.id);
         var1.writeShort(this.hp);
         var1.writeLong(this.ownerId);
         var1.writeInt(this.factionId);
         var1.writeBoolean(this.armed);
         var1.writeShort(this.ammo);
         Vector3fTools.serialize(this.localPos, var1);

         for(int var3 = 0; var3 < 6; ++var3) {
            var1.writeShort(this.composition[var3]);
         }

      }

      public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
         this.id = var1.readInt();
         this.hp = var1.readShort();
         this.ownerId = var1.readLong();
         this.factionId = var1.readInt();
         this.armed = var1.readBoolean();
         this.ammo = var1.readShort();
         this.localPos = Vector3fTools.deserialize(new Vector3f(), var1);
         this.composition = new short[6];

         for(var2 = 0; var2 < 6; ++var2) {
            this.composition[var2] = var1.readShort();
         }

      }
   }
}
