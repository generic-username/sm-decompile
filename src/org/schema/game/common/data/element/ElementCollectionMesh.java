package org.schema.game.common.data.element;

import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.bytes.Byte2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.bytes.ByteArrayList;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ByteOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongCollection;
import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.longs.LongList;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap.Entry;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.junit.Test;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.shader.CubeMeshQuadsShader13;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.util.WorldToScreenConverter;
import org.schema.schine.input.Keyboard;

public class ElementCollectionMesh {
   public static final int FACE_COUNT = 6;
   public static final int VERTS_PER_FACE = 6;
   public static final int VERT_DIMENSION = USE_INT_ATT() ? 2 : 3;
   public static final int TYPE_BYTE_COUNT = 4;
   public static final int VERT_ATTRIB_INDEX = 0;
   private static final Vector4f DEFAULT_COLOR = new Vector4f(1.0F, 1.0F, 1.0F, 0.2F);
   private static final int THREADS = 5;
   private static final ThreadPoolExecutor threadPool = (ThreadPoolExecutor)Executors.newFixedThreadPool(5, new ThreadFactory() {
      public final Thread newThread(Runnable var1) {
         return new Thread(var1, "CollectionMeshCreationThread");
      }
   });
   public static final List setPool = new ObjectArrayList();
   private ByteBuffer buffer;
   private ByteArrayList vises = new ByteArrayList();
   private LongArrayList poses = new LongArrayList();
   private float[] triangles;
   private Vector3i posTmp = new Vector3i();
   private boolean init;
   private boolean startedInit;
   private boolean inInit;
   private int rawDataSize;
   private int bufferId;
   private int rawVerticesCount;
   private Vector3f localMin = new Vector3f();
   private Vector3f localMax = new Vector3f();
   private Vector3f min = new Vector3f();
   private Vector3f max = new Vector3f();
   private final LongOpenHashSet filled = new LongOpenHashSet();
   private final Long2ByteOpenHashMap pContain = new Long2ByteOpenHashMap();
   private boolean draw;
   private final Vector4f color;
   private ElementCollectionMesh.OptimizedMesh optimizedMesh;
   private int optTriangleFloatCount;
   private Vector3f firstVertex;
   public static final ElementCollectionMesh.DrawMode DRAW_MODE;
   public static ElementCollectionMesh.DebugModes dbm;
   public static int meshesInUse;
   public static final List sidePool;
   private ElementCollectionMesh.OptimizedMesh debug;
   private WorldToScreenConverter wtc;

   public static void freeSet(LongOpenHashSet var0) {
      var0.clear();
      synchronized(setPool) {
         setPool.add(var0);
         setPool.notifyAll();
      }
   }

   public static LongOpenHashSet getSet() {
      synchronized(setPool) {
         while(setPool.isEmpty()) {
            try {
               setPool.wait();
            } catch (InterruptedException var2) {
               var2.printStackTrace();
            }
         }

         return (LongOpenHashSet)setPool.remove(setPool.size() - 1);
      }
   }

   public static void freeSideInstance(ElementCollectionMesh.Side var0) {
      var0.clear();
      synchronized(sidePool) {
         sidePool.add(var0);
      }
   }

   public static ElementCollectionMesh.Side getSideInstance() {
      synchronized(sidePool) {
         return sidePool.isEmpty() ? new ElementCollectionMesh.Side() : (ElementCollectionMesh.Side)sidePool.remove(sidePool.size() - 1);
      }
   }

   public static boolean isCanceled(ElementCollection var0, long var1) {
      return var0 != null && var0.elementCollectionManager.cancelUpdateStatus == var1;
   }

   public ElementCollectionMesh() {
      this.color = new Vector4f(DEFAULT_COLOR);
      this.wtc = new WorldToScreenConverter();
   }

   public void calculate(ElementCollection var1, long var2, LongList var4) {
      long var5 = System.currentTimeMillis();
      LongOpenHashSet var7;
      (var7 = getSet()).addAll(var4);
      this.localMin.set(1.0E7F, 1.0E7F, 1.0E7F);
      this.localMax.set(-1.0E7F, -1.0E7F, -1.0E7F);

      assert var4.size() > 0;

      int var8 = var4.size();

      int var9;
      long var10;
      for(var9 = 0; var9 < var8; ++var9) {
         var10 = var4.getLong(var9);
         this.addFill(var10, var7);
         if (isCanceled(var1, var2)) {
            break;
         }
      }

      var7.addAll(this.filled);

      for(var9 = 0; var9 < var8; ++var9) {
         var10 = var4.getLong(var9);
         this.addCheck(var10, var7);
         if (isCanceled(var1, var2)) {
            break;
         }
      }

      LongIterator var15 = this.filled.iterator();

      while(var15.hasNext()) {
         this.addCheck(var15.nextLong(), var7);
         if (isCanceled(var1, var2)) {
            break;
         }
      }

      var10 = System.currentTimeMillis() - var5;
      this.rawVerticesCount = this.poses.size() * 6 * 6;
      this.rawDataSize = (this.rawVerticesCount << 2) * VERT_DIMENSION;
      this.pContain.clear();
      this.filled.clear();
      freeSet(var7);
      if (this.optimizedMesh == null) {
         this.optimizedMesh = new ElementCollectionMesh.OptimizedMesh();
      } else {
         this.optimizedMesh.clear();
      }

      this.optimizedMesh.fill(this.poses, this.vises);
      this.optimizedMesh.optimize(var1, var2);

      int var14;
      for(var14 = 256; var14 < this.optimizedMesh.triangleFloatCount; var14 <<= 1) {
      }

      if (this.triangles == null || this.triangles.length < var14) {
         this.triangles = new float[var14];
      }

      this.optTriangleFloatCount = this.optimizedMesh.apply(this.triangles);
      this.firstVertex = new Vector3f(this.triangles[0], this.triangles[1], this.triangles[2]);

      assert this.optTriangleFloatCount == this.optimizedMesh.triangleFloatCount;

      this.optimizedMesh.clear();
      long var12;
      if ((var12 = System.currentTimeMillis() - var5) > 10L) {
         System.err.println("[ElementCollectionMesh] WARNING: Mesh calculation took long: " + var12 + "ms; Adding took: " + var10 + "ms");
      }

   }

   private void addFill(long var1, LongOpenHashSet var3) {
      ElementCollection.getPosFromIndex(var1, this.posTmp);

      for(int var8 = 0; var8 < 6; ++var8) {
         Vector3i var2 = Element.DIRECTIONSi[var8];
         int var4 = this.posTmp.x + var2.x;
         int var5 = this.posTmp.y + var2.y;
         int var9 = this.posTmp.z + var2.z;
         long var6 = ElementCollection.getIndex(var4, var5, var9);
         if (!var3.contains(var6) && (byte)(this.pContain.add(var6, (byte)1) + 1) >= 5) {
            this.filled.add(var6);
         }
      }

   }

   private void addCheck(long var1, LongOpenHashSet var3) {
      ElementCollection.getPosFromIndex(var1, this.posTmp);
      byte var4 = Element.FULLVIS;

      for(int var5 = 0; var5 < 6; ++var5) {
         Vector3i var6 = Element.DIRECTIONSi[var5];
         int var7 = this.posTmp.x + var6.x;
         int var8 = this.posTmp.y + var6.y;
         int var11 = this.posTmp.z + var6.z;
         this.localMin.x = Math.min(this.localMin.x, (float)(var7 - 16));
         this.localMin.y = Math.min(this.localMin.y, (float)(var8 - 16));
         this.localMin.z = Math.min(this.localMin.z, (float)(var11 - 16));
         this.localMax.x = Math.max(this.localMax.x, (float)(var7 - 16));
         this.localMax.y = Math.max(this.localMax.y, (float)(var8 - 16));
         this.localMax.z = Math.max(this.localMax.z, (float)(var11 - 16));
         long var9 = ElementCollection.getIndex(var7, var8, var11);
         if (var3.contains(var9)) {
            var4 = (byte)(var4 - Element.SIDE_FLAG[var5]);
         }
      }

      if (var4 > 0) {
         this.vises.add(var4);
         this.poses.add(var1);
      }

   }

   public void clear() {
      while(this.inInit) {
         System.err.println("THREAD WAS IN INIT. CLEAN UP HAS TO WAIT");

         try {
            Thread.sleep(10L);
         } catch (InterruptedException var1) {
            var1.printStackTrace();
         }
      }

      this.color.set(DEFAULT_COLOR);
      this.draw = false;
      this.init = false;
      this.startedInit = false;
      this.vises.clear();
      this.poses.clear();
      this.optTriangleFloatCount = 0;
      if (this.buffer != null) {
         this.buffer.clear();
      }

      if (this.bufferId != 0) {
         GL15.glDeleteBuffers(this.bufferId);
         this.bufferId = 0;
      }

      this.optimizedMesh = null;
   }

   public void destroyBuffer() {
      if (this.buffer != null) {
         try {
            GlUtil.destroyDirectByteBuffer(this.buffer);
            return;
         } catch (IllegalArgumentException var1) {
            var1.printStackTrace();
            return;
         } catch (IllegalAccessException var2) {
            var2.printStackTrace();
            return;
         } catch (InvocationTargetException var3) {
            var3.printStackTrace();
            return;
         } catch (SecurityException var4) {
            var4.printStackTrace();
            return;
         } catch (NoSuchMethodException var5) {
            var5.printStackTrace();
         }
      }

   }

   public boolean isVisibleFrustum(Transform var1) {
      AabbUtil2.transformAabb(this.localMin, this.localMax, 3.0F, var1, this.min, this.max);
      return Controller.getCamera().isAABBInFrustum(this.min, this.max);
   }

   public void drawDebug() {
      if (this.draw) {
         this.draw = false;
         int var1 = Keyboard.getNumberPressed();
         boolean var2 = false;
         if (var1 >= 0) {
            var2 = dbm.step(var1);
         }

         System.err.println("DRAW DEBUG " + dbm.step);
         if (this.debug == null || var2) {
            this.debug = new ElementCollectionMesh.OptimizedMesh();
            this.debug.fill(this.poses, this.vises);
            this.debug.optimize((ElementCollection)null, 0L);
         }

         this.debug.drawItermediateSides();
         this.debug.drawItermediateTriangledSides();
      }
   }

   public void draw() {
      try {
         if (this.draw) {
            this.wtc.storeCurrentModelviewProjection();
            if (this.debug != null) {
               this.debug.clear();
               this.debug = null;
            }

            this.draw = false;
            if (!this.init) {
               if (!this.startedInit) {
                  this.initializeMeshThreaded();
                  this.startedInit = true;
               }

            } else {
               GL11.glColor4f(this.color.x, this.color.y, this.color.z, this.color.w);
               if (this.bufferId == 0) {
                  this.bufferId = GL15.glGenBuffers();
                  GlUtil.glBindBuffer(34962, this.bufferId);
                  GL15.glBufferData(34962, this.buffer, 35044);
               } else {
                  GlUtil.glBindBuffer(34962, this.bufferId);
               }

               if (USE_INT_ATT()) {
                  GlUtil.glVertexAttribIPointer(0, VERT_DIMENSION, 5124, 0, 0L);
               } else if (DRAW_MODE == ElementCollectionMesh.DrawMode.RAW) {
                  GL11.glVertexPointer(VERT_DIMENSION, 5126, 0, 0L);
               } else {
                  GL11.glVertexPointer(3, 5126, 0, 0L);
               }

               if (DRAW_MODE == ElementCollectionMesh.DrawMode.RAW) {
                  GL11.glDrawArrays(4, 0, this.rawVerticesCount);
               } else {
                  GL11.glDrawArrays(4, 0, this.optTriangleFloatCount / 3);
               }

               GlUtil.glBindBuffer(34962, 0);
            }
         }
      } catch (Throwable var2) {
         throw var2;
      }
   }

   public void initializeMesh() {
      int var1 = 256;

      for(int var2 = DRAW_MODE == ElementCollectionMesh.DrawMode.RAW ? this.rawDataSize : this.optTriangleFloatCount << 2; var1 < var2; var1 <<= 1) {
      }

      var1 <<= 1;
      if (this.buffer == null || this.buffer.capacity() < var1) {
         long var3 = 0L;
         long var5;
         if (this.buffer != null) {
            try {
               var5 = System.currentTimeMillis();
               GlUtil.destroyDirectByteBuffer(this.buffer);
               var3 = System.currentTimeMillis() - var5;
            } catch (IllegalArgumentException var11) {
               var11.printStackTrace();
            } catch (IllegalAccessException var12) {
               var12.printStackTrace();
            } catch (InvocationTargetException var13) {
               var13.printStackTrace();
            } catch (SecurityException var14) {
               var14.printStackTrace();
            } catch (NoSuchMethodException var15) {
               var15.printStackTrace();
            }
         }

         var5 = System.currentTimeMillis();
         this.buffer = BufferUtils.createByteBuffer(var1);
         long var7 = System.currentTimeMillis() - var5;
         long var9;
         if ((var9 = var3 + var7) > 10L) {
            System.err.println("[ElementCollectionMesh] WARNING: REFRESH: Mesh buffer data destruction->recreation took long: " + var9 + " ms (destr: " + var3 + "ms; create: " + var7 + "ms)");
         }
      }

      this.buffer.clear();
      this.fillBuffer(this.buffer);
      this.buffer.flip();
      this.init = true;
      this.inInit = false;
   }

   private void initializeMeshThreaded() {
      this.inInit = true;
      threadPool.execute(new Runnable() {
         public void run() {
            ElementCollectionMesh.this.initializeMesh();
         }
      });
   }

   public static boolean USE_INT_ATT() {
      return DRAW_MODE == ElementCollectionMesh.DrawMode.RAW && EngineSettings.G_ELEMENT_COLLECTION_INT_ATT.isOn() && GraphicsContext.INTEGER_VERTICES;
   }

   private void fillBuffer(ByteBuffer var1) {
      var1.asFloatBuffer().put(this.triangles, 0, this.optTriangleFloatCount);
      var1.position(this.optTriangleFloatCount << 2);
   }

   public void markDraw() {
      this.draw = true;
   }

   public void setColor(float var1, float var2, float var3, float var4) {
      this.color.set(var1, var2, var3, var4);
   }

   public void setColor(Vector4f var1) {
      this.color.set(var1);
   }

   public boolean isDraw() {
      return this.draw;
   }

   public Vector3f getFirstVertexScreenCoord(Transform var1, Vector3f var2) {
      Vector3f var3 = new Vector3f(this.firstVertex);
      var1.transform(var3);
      this.wtc.convert(var3, var2, true);
      return var2;
   }

   public Vector3f getFirstVertex() {
      return this.firstVertex;
   }

   public void setFirstVertex(Vector3f var1) {
      this.firstVertex = var1;
   }

   public int getVertexCount() {
      return DRAW_MODE == ElementCollectionMesh.DrawMode.RAW ? this.rawVerticesCount : this.optTriangleFloatCount / 3;
   }

   static {
      int var0;
      for(var0 = 0; var0 < 64; ++var0) {
         setPool.add(new LongOpenHashSet(1024));
      }

      for(var0 = 0; var0 < 5; ++var0) {
         threadPool.execute(new Runnable() {
            public final void run() {
               try {
                  Thread.sleep(2000L);
               } catch (InterruptedException var1) {
                  var1.printStackTrace();
               }
            }
         });
      }

      DRAW_MODE = ElementCollectionMesh.DrawMode.OPTIMIZED;
      dbm = new ElementCollectionMesh.DebugModes();
      sidePool = new ObjectArrayList();
   }

   public static enum DrawMode {
      RAW,
      OPTIMIZED;
   }

   static class OptimizedMesh {
      private final long[] tmpSc;
      private Int2ObjectOpenHashMap[] sArray;
      public int triangleFloatCount;

      private OptimizedMesh() {
         this.tmpSc = new long[4];
         this.sArray = new Int2ObjectOpenHashMap[]{new Int2ObjectOpenHashMap(), new Int2ObjectOpenHashMap(), new Int2ObjectOpenHashMap(), new Int2ObjectOpenHashMap(), new Int2ObjectOpenHashMap(), new Int2ObjectOpenHashMap()};
      }

      public void optimize(ElementCollection var1, long var2) {
         this.triangleFloatCount = 0;

         ElementCollectionMesh.Side var6;
         for(int var4 = 0; var4 < 6; ++var4) {
            for(Iterator var5 = this.sArray[var4].values().iterator(); var5.hasNext(); this.triangleFloatCount += var6.triangulator.triangles.size() * 3) {
               var6 = (ElementCollectionMesh.Side)var5.next();
               if (ElementCollectionMesh.isCanceled(var1, var2)) {
                  return;
               }

               var6.optimize(var1, var2);
            }
         }

      }

      public void fill(LongArrayList var1, ByteArrayList var2) {
         for(int var3 = 0; var3 < var1.size(); ++var3) {
            long var4 = var1.getLong(var3);
            byte var6 = var2.getByte(var3);
            float var7 = (float)(ElementCollection.getPosX(var4) - 16);
            float var8 = (float)(ElementCollection.getPosY(var4) - 16);
            float var24 = (float)(ElementCollection.getPosZ(var4) - 16);

            for(int var5 = 0; var5 < 6; ++var5) {
               Vector3f var9 = CubeMeshQuadsShader13.quadPosMark[var5];
               Vector3f var10 = Element.DIRECTIONSf[var5];
               Int2ObjectOpenHashMap var11 = this.sArray[var5];
               int var12 = Element.SIDE_FLAG[var5];
               if ((var6 & var12) == var12) {
                  var12 = Element.SIGNIFICANT_COORD[var5];
                  int var13 = Integer.MAX_VALUE;
                  switch(var12) {
                  case 0:
                     var13 = (int)var7;
                     break;
                  case 1:
                     var13 = (int)var8;
                     break;
                  case 2:
                     var13 = (int)var24;
                  }

                  var13 = (int)((float)var13 + (Element.COORD_DIR[var5] + 1.0F) / 2.0F);
                  ElementCollectionMesh.Side var14;
                  if ((var14 = (ElementCollectionMesh.Side)var11.get(var13)) == null) {
                     (var14 = ElementCollectionMesh.getSideInstance()).index = var13;
                     var14.sideId = var5;
                     var11.put(var13, var14);
                  }

                  short var25;
                  for(var25 = 0; var25 < 4; ++var25) {
                     float var20 = (float)var25 * 0.25F;
                     float var16 = (float)((int)(var20 * var9.x) & 1);
                     float var17 = (float)((int)(var20 * var9.y) & 1);
                     float var22 = (float)((int)(var20 * var9.z) & 1);
                     float var23 = -0.5F - Math.abs(var10.x) * -0.5F + var16;
                     float var18 = -0.5F - Math.abs(var10.y) * -0.5F + var17;
                     float var19 = -0.5F - Math.abs(var10.z) * -0.5F + var22;
                     float var21 = var7 + var23 + var10.x * 0.5F;
                     float var26 = var8 + var18 + var10.y * 0.5F;
                     float var15 = var24 + var19 + var10.z * 0.5F;
                     var21 += 0.5F;
                     var26 += 0.5F;
                     var15 += 0.5F;
                     int var28 = 0;
                     int var29 = 0;
                     switch(var12) {
                     case 0:
                        var28 = FastMath.round(var26);
                        var29 = FastMath.round(var15);
                        break;
                     case 1:
                        var28 = FastMath.round(var21);
                        var29 = FastMath.round(var15);
                        break;
                     case 2:
                        var28 = FastMath.round(var21);
                        var29 = FastMath.round(var26);
                     }

                     this.tmpSc[var25] = ElementCollectionMesh.Side.getIndex(var28, var29);
                  }

                  for(var25 = 0; var25 < 4; ++var25) {
                     long var34;
                     var13 = ElementCollectionMesh.Side.getPosX(var34 = this.tmpSc[var25]);
                     int var27 = ElementCollectionMesh.Side.getPosY(var34);
                     ElementCollectionMesh.VSet var30;
                     if ((var30 = (ElementCollectionMesh.VSet)var14.pSet.get(var34)) == null) {
                        var30 = ElementCollectionMesh.VSet.getInst();
                        var14.pSet.put(var34, var30);
                     }

                     for(short var32 = 0; var32 < 4; ++var32) {
                        long var35;
                        int var31 = ElementCollectionMesh.Side.getPosX(var35 = this.tmpSc[var32]);
                        int var33 = ElementCollectionMesh.Side.getPosY(var35);
                        if (var35 != var34 && (var13 == var31 && var27 != var33 || var13 != var31 && var27 == var33)) {
                           var30.set.add(var35);

                           assert var30.set.size() <= 4;
                        } else if (var35 != var34) {
                           var30.diagonals.add(var35);
                        }
                     }
                  }
               }
            }
         }

      }

      public void drawItermediateTriangledSides() {
         GL11.glPointSize(4.0F);
         GL11.glBegin(0);

         int var1;
         Int2ObjectOpenHashMap var2;
         int var3;
         ElementCollectionMesh.Side var4;
         long var7;
         int var9;
         int var10;
         Iterator var13;
         for(var1 = 0; var1 < 6; ++var1) {
            if (var1 == 0) {
               var2 = this.sArray[var1];
               var3 = Element.SIGNIFICANT_COORD[var1];
               var13 = var2.values().iterator();

               while(var13.hasNext()) {
                  Iterator var5 = (var4 = (ElementCollectionMesh.Side)var13.next()).triangulator.triangles.iterator();

                  while(var5.hasNext()) {
                     var9 = ElementCollectionMesh.Side.getPosX(var7 = (Long)var5.next());
                     var10 = ElementCollectionMesh.Side.getPosY(var7);
                     float var11 = Element.COORD_DIR[var1] * 0.03F;
                     GlUtil.glColor4f(Element.SIDE_COLORS[var1]);
                     if (var3 == 0) {
                        GL11.glVertex3f((float)var4.index + var11, (float)var9, (float)var10);
                     } else if (var3 == 1) {
                        GL11.glVertex3f((float)var9, (float)var4.index + var11, (float)var10);
                     } else {
                        GL11.glVertex3f((float)var9, (float)var10, (float)var4.index + var11);
                     }
                  }
               }
            }
         }

         GL11.glEnd();
         GL11.glBegin(1);

         for(var1 = 0; var1 < 6; ++var1) {
            if (var1 == 0) {
               var2 = this.sArray[var1];
               var3 = Element.SIGNIFICANT_COORD[var1];
               var13 = var2.values().iterator();

               while(var13.hasNext()) {
                  var4 = (ElementCollectionMesh.Side)var13.next();

                  for(int var14 = 0; var14 < var4.triangulator.triangles.size(); var14 += 3) {
                     var7 = var4.triangulator.triangles.getLong(var14);
                     long var16 = var4.triangulator.triangles.getLong(var14 + 1);
                     long var17 = var4.triangulator.triangles.getLong(var14 + 2);
                     int var6 = ElementCollectionMesh.Side.getPosX(var7);
                     int var15 = ElementCollectionMesh.Side.getPosY(var7);
                     int var8 = ElementCollectionMesh.Side.getPosX(var16);
                     var9 = ElementCollectionMesh.Side.getPosY(var16);
                     var10 = ElementCollectionMesh.Side.getPosX(var17);
                     int var18 = ElementCollectionMesh.Side.getPosY(var17);
                     GlUtil.glColor4f(Element.SIDE_COLORS[var1]);
                     if (var3 == 0) {
                        GL11.glVertex3f((float)var4.index, (float)var6, (float)var15);
                        GL11.glVertex3f((float)var4.index, (float)var8, (float)var9);
                     } else if (var3 == 1) {
                        GL11.glVertex3f((float)var8, (float)var4.index, (float)var9);
                        GL11.glVertex3f((float)var10, (float)var4.index, (float)var18);
                     } else {
                        GL11.glVertex3f((float)var10, (float)var18, (float)var4.index);
                        GL11.glVertex3f((float)var6, (float)var15, (float)var4.index);
                     }
                  }
               }
            }
         }

         GL11.glEnd();
      }

      public void drawItermediateSides() {
         GL11.glPointSize(4.0F);
         GL11.glBegin(0);

         int var1;
         Int2ObjectOpenHashMap var2;
         int var3;
         ElementCollectionMesh.Side var4;
         Iterator var5;
         int var7;
         long var8;
         Iterator var13;
         int var16;
         for(var1 = 0; var1 < 6; ++var1) {
            if (var1 == 0) {
               var2 = this.sArray[var1];
               var3 = Element.SIGNIFICANT_COORD[var1];
               var13 = var2.values().iterator();

               while(var13.hasNext()) {
                  var5 = (var4 = (ElementCollectionMesh.Side)var13.next()).pSet.long2ObjectEntrySet().iterator();

                  while(var5.hasNext()) {
                     var7 = ElementCollectionMesh.Side.getPosX(var8 = ((Entry)var5.next()).getLongKey());
                     var16 = ElementCollectionMesh.Side.getPosY(var8);
                     float var6 = Element.COORD_DIR[var1] * 0.03F;
                     GlUtil.glColor4f(Element.SIDE_COLORS[var1]);
                     if (var3 == 0) {
                        GL11.glVertex3f((float)var4.index + var6, (float)var7, (float)var16);
                     } else if (var3 == 1) {
                        GL11.glVertex3f((float)var7, (float)var4.index + var6, (float)var16);
                     } else {
                        GL11.glVertex3f((float)var7, (float)var16, (float)var4.index + var6);
                     }
                  }
               }
            }
         }

         GL11.glEnd();
         GL11.glBegin(1);

         for(var1 = 0; var1 < 6; ++var1) {
            if (var1 == 0) {
               var2 = this.sArray[var1];
               var3 = Element.SIGNIFICANT_COORD[var1];
               var13 = var2.values().iterator();

               while(var13.hasNext()) {
                  var5 = (var4 = (ElementCollectionMesh.Side)var13.next()).pSet.long2ObjectEntrySet().iterator();

                  while(var5.hasNext()) {
                     Entry var14;
                     var7 = ElementCollectionMesh.Side.getPosX(var8 = (var14 = (Entry)var5.next()).getLongKey());
                     var16 = ElementCollectionMesh.Side.getPosY(var8);
                     GlUtil.glColor4f(Element.SIDE_COLORS[((ElementCollectionMesh.VSet)var14.getValue()).groupId % 6]);
                     Iterator var15 = ((ElementCollectionMesh.VSet)var14.getValue()).set.iterator();

                     while(var15.hasNext()) {
                        long var11;
                        int var9 = ElementCollectionMesh.Side.getPosX(var11 = (Long)var15.next());
                        int var10 = ElementCollectionMesh.Side.getPosY(var11);
                        if (var3 == 0) {
                           GL11.glVertex3f((float)var4.index + 0.0F, (float)var7, (float)var16);
                           GL11.glVertex3f((float)var4.index + 0.0F, (float)var9, (float)var10);
                        } else if (var3 == 1) {
                           GL11.glVertex3f((float)var7, (float)var4.index + 0.0F, (float)var16);
                           GL11.glVertex3f((float)var9, (float)var4.index + 0.0F, (float)var10);
                        } else {
                           GL11.glVertex3f((float)var7, (float)var16, (float)var4.index + 0.0F);
                           GL11.glVertex3f((float)var9, (float)var10, (float)var4.index + 0.0F);
                        }
                     }
                  }
               }
            }
         }

         GL11.glEnd();
      }

      public void clear() {
         Int2ObjectOpenHashMap[] var1;
         int var2 = (var1 = this.sArray).length;

         for(int var3 = 0; var3 < var2; ++var3) {
            Int2ObjectOpenHashMap var4;
            Iterator var5 = (var4 = var1[var3]).values().iterator();

            while(var5.hasNext()) {
               ElementCollectionMesh.freeSideInstance((ElementCollectionMesh.Side)var5.next());
            }

            var4.clear();
         }

      }

      private static int aTri(float var0, float var1, float var2, float[] var3, int var4) {
         var3[var4] = var0 - 0.5F;
         var3[var4 + 1] = var1 - 0.5F;
         var3[var4 + 2] = var2 - 0.5F;
         return var4 + 3;
      }

      public int apply(float[] var1) {
         int var2 = 0;

         for(int var3 = 0; var3 < 6; ++var3) {
            Int2ObjectOpenHashMap var4 = this.sArray[var3];
            int var5 = Element.SIGNIFICANT_COORD[var3];
            Iterator var15 = var4.values().iterator();

            while(var15.hasNext()) {
               ElementCollectionMesh.Side var6 = (ElementCollectionMesh.Side)var15.next();

               for(int var7 = 0; var7 < var6.triangulator.triangles.size(); var7 += 3) {
                  long var9 = var6.triangulator.triangles.getLong(var7);
                  long var11 = var6.triangulator.triangles.getLong(var7 + 1);
                  long var13 = var6.triangulator.triangles.getLong(var7 + 2);
                  int var8 = ElementCollectionMesh.Side.getPosX(var9);
                  int var16 = ElementCollectionMesh.Side.getPosY(var9);
                  int var10 = ElementCollectionMesh.Side.getPosX(var11);
                  int var17 = ElementCollectionMesh.Side.getPosY(var11);
                  int var12 = ElementCollectionMesh.Side.getPosX(var13);
                  int var18 = ElementCollectionMesh.Side.getPosY(var13);
                  if (var5 == 0) {
                     var2 = aTri((float)var6.index, (float)var8, (float)var16, var1, var2);
                     var2 = aTri((float)var6.index, (float)var10, (float)var17, var1, var2);
                     var2 = aTri((float)var6.index, (float)var12, (float)var18, var1, var2);
                  } else if (var5 == 1) {
                     var2 = aTri((float)var8, (float)var6.index, (float)var16, var1, var2);
                     var2 = aTri((float)var10, (float)var6.index, (float)var17, var1, var2);
                     var2 = aTri((float)var12, (float)var6.index, (float)var18, var1, var2);
                  } else {
                     var2 = aTri((float)var8, (float)var16, (float)var6.index, var1, var2);
                     var2 = aTri((float)var10, (float)var17, (float)var6.index, var1, var2);
                     var2 = aTri((float)var12, (float)var18, (float)var6.index, var1, var2);
                  }
               }
            }
         }

         return var2;
      }

      // $FF: synthetic method
      OptimizedMesh(Object var1) {
         this();
      }
   }

   static class GridTriangulator {
      private static final boolean[] CLOCK_WISE = new boolean[]{true, false, false, true, true, false};
      private Long2ObjectOpenHashMap pFull;
      private Byte2ObjectOpenHashMap groups;
      private LongArrayList pointsToInsert;
      private LongArrayList edgesToInsert;
      private LongOpenHashSet closed;
      private LongArrayList triangles;

      private GridTriangulator() {
         this.pFull = new Long2ObjectOpenHashMap();
         this.groups = new Byte2ObjectOpenHashMap();
         this.pointsToInsert = new LongArrayList();
         this.edgesToInsert = new LongArrayList();
         this.closed = new LongOpenHashSet();
         this.triangles = new LongArrayList();
      }

      public void triangulate(int var1, Long2ObjectOpenHashMap var2) {
         Iterator var3 = this.groups.values().iterator();

         while(var3.hasNext()) {
            Long2ObjectOpenHashMap var4 = (Long2ObjectOpenHashMap)var3.next();
            if (ElementCollectionMesh.dbm.quadrulate) {
               this.quadrulate(var4, var2);
            }

            if (ElementCollectionMesh.dbm.retriangulate) {
               this.triangulateQuads(var1, var4);
            }
         }

      }

      private void triangulateQuads(int var1, Long2ObjectOpenHashMap var2) {
         Iterator var3 = var2.long2ObjectEntrySet().iterator();

         while(true) {
            Entry var4;
            long var6;
            do {
               if (!var3.hasNext()) {
                  return;
               }

               var6 = (var4 = (Entry)var3.next()).getLongKey();
            } while(this.closed.contains(var6));

            ElementCollectionMesh.VSet var18 = (ElementCollectionMesh.VSet)var4.getValue();
            int var5 = ElementCollectionMesh.Side.getPosX(var6);
            int var8 = ElementCollectionMesh.Side.getPosY(var6);
            boolean var9 = false;
            boolean var10 = false;
            Iterator var19 = var18.set.iterator();

            while(var19.hasNext()) {
               long var11;
               int var13 = ElementCollectionMesh.Side.getPosX(var11 = (Long)var19.next());
               int var14 = ElementCollectionMesh.Side.getPosY(var11);
               if (var13 == var5 && var14 < var8) {
                  assert !var10;

                  var10 = true;
               }

               if (var13 > var5 && var14 == var8) {
                  assert !var9;

                  var9 = true;
               }
            }

            assert this.pFull.containsKey(var6) : this.pFull.size();

            if (var9 && var10 && !((ElementCollectionMesh.VSet)this.pFull.get(var6)).diagonals.contains(ElementCollectionMesh.Side.getIndex(var5 - 1, var8 - 1))) {
               long var12 = this.find(var2, var6, 1, 0, true, 0, -1);
               long var20 = this.find(var2, var12, 0, -1, true, -1, 0);
               long var16 = this.find(var2, var20, -1, 0, true, 0, 1);
               if (CLOCK_WISE[var1]) {
                  this.triangles.add(var20);
                  this.triangles.add(var12);
                  this.triangles.add(var6);
                  this.triangles.add(var16);
                  this.triangles.add(var20);
                  this.triangles.add(var6);
               } else {
                  this.triangles.add(var6);
                  this.triangles.add(var12);
                  this.triangles.add(var20);
                  this.triangles.add(var6);
                  this.triangles.add(var20);
                  this.triangles.add(var16);
               }
            }
         }
      }

      private long find(LongCollection var1, int var2, int var3, int var4, int var5) {
         Iterator var10 = var1.iterator();

         int var6;
         long var7;
         int var9;
         do {
            if (!var10.hasNext()) {
               return Long.MAX_VALUE;
            }

            var6 = ElementCollectionMesh.Side.getPosX(var7 = (Long)var10.next());
            var9 = ElementCollectionMesh.Side.getPosY(var7);
         } while((var4 <= 0 || var6 <= var2 || var9 != var3) && (var4 >= 0 || var6 >= var2 || var9 != var3) && (var5 <= 0 || var9 <= var3 || var6 != var2) && (var5 >= 0 || var9 >= var3 || var6 != var2));

         return var7;
      }

      private long find(Long2ObjectOpenHashMap var1, long var2, int var4, int var5, boolean var6, int var7, int var8) {
         ElementCollectionMesh.VSet var9 = (ElementCollectionMesh.VSet)var1.get(var2);
         long var10 = var2;
         int var12 = ElementCollectionMesh.Side.getPosX(var2);
         int var17 = ElementCollectionMesh.Side.getPosY(var2);

         while(true) {
            do {
               long var18;
               if ((var18 = this.find(var9.set, var12, var17, var4, var5)) == Long.MAX_VALUE) {
                  return var10;
               }

               var10 = var18;
               var9 = (ElementCollectionMesh.VSet)var1.get(var18);
               var12 = ElementCollectionMesh.Side.getPosX(var18);
               var17 = ElementCollectionMesh.Side.getPosY(var10);

               assert var9 != null : var10 + "; " + var12 + ", " + var17;
            } while(!var6);

            Iterator var3 = var9.set.iterator();

            while(var3.hasNext()) {
               long var15;
               int var13 = ElementCollectionMesh.Side.getPosX(var15 = (Long)var3.next());
               int var14 = ElementCollectionMesh.Side.getPosY(var15);
               if (var7 > 0 && var13 > var12 && var14 == var17 || var7 < 0 && var13 < var12 && var14 == var17 || var8 > 0 && var14 > var17 && var13 == var12 || var8 < 0 && var14 < var17 && var13 == var12) {
                  return var10;
               }
            }
         }
      }

      private void quadrulate(Long2ObjectOpenHashMap var1, Long2ObjectOpenHashMap var2) {
         byte var3 = -1;
         Iterator var4 = var1.long2ObjectEntrySet().iterator();

         label162:
         while(true) {
            int var5;
            long var7;
            ElementCollectionMesh.VSet var9;
            int var10;
            long var12;
            do {
               Entry var6;
               do {
                  if (!var4.hasNext()) {
                     assert var3 != -1;

                     var4 = this.pointsToInsert.iterator();

                     long var22;
                     while(var4.hasNext()) {
                        var22 = (Long)var4.next();
                        ElementCollectionMesh.VSet var8;
                        (var8 = ElementCollectionMesh.VSet.getInst()).groupId = var3;
                        var1.put(var22, var8);
                        var9 = (ElementCollectionMesh.VSet)var2.put(var22, var8);

                        assert var9 == null;

                        var5 = ElementCollectionMesh.Side.getPosX(var22);
                        var10 = ElementCollectionMesh.Side.getPosY(var22);

                        assert ElementCollectionMesh.Side.getPosX(var22) == var5;

                        assert ElementCollectionMesh.Side.getPosY(var22) == var10;

                        int var25 = var10 + 1;
                        var12 = ElementCollectionMesh.Side.getIndex(var5, var25);

                        long var26;
                        while(!var1.containsKey(var12)) {
                           assert this.pFull.containsKey(var12) : var5 + "; " + var25;

                           var26 = var12;
                           ++var25;
                           var12 = ElementCollectionMesh.Side.getIndex(var5, var25);

                           assert ((ElementCollectionMesh.VSet)this.pFull.get(var26)).set.contains(var12);
                        }

                        var26 = var12;
                        var25 = var10 - 1;
                        var12 = ElementCollectionMesh.Side.getIndex(var5, var25);

                        while(!var1.containsKey(var12)) {
                           assert this.pFull.containsKey(var12) : var5 + "; " + var25;

                           long var27 = var12;
                           --var25;
                           var12 = ElementCollectionMesh.Side.getIndex(var5, var25);

                           assert ((ElementCollectionMesh.VSet)this.pFull.get(var27)).set.contains(var12);
                        }

                        ((ElementCollectionMesh.VSet)var1.get(var26)).set.remove(var12);
                        ((ElementCollectionMesh.VSet)var1.get(var12)).set.remove(var26);
                        ((ElementCollectionMesh.VSet)var1.get(var22)).set.add(var12);
                        ((ElementCollectionMesh.VSet)var1.get(var22)).set.add(var26);
                        ((ElementCollectionMesh.VSet)var1.get(var26)).set.add(var22);
                        ((ElementCollectionMesh.VSet)var1.get(var12)).set.add(var22);
                     }

                     for(int var20 = 0; var20 < this.edgesToInsert.size(); var20 += 2) {
                        var22 = this.edgesToInsert.getLong(var20);
                        long var23 = this.edgesToInsert.getLong(var20 + 1);

                        assert var1.containsKey(var23);

                        assert var1.containsKey(var22);

                        ((ElementCollectionMesh.VSet)var1.get(var22)).set.add(var23);
                        ((ElementCollectionMesh.VSet)var1.get(var23)).set.add(var22);
                     }

                     this.edgesToInsert.clear();
                     this.pointsToInsert.clear();
                     this.closed.clear();
                     return;
                  }

                  var7 = (var6 = (Entry)var4.next()).getLongKey();
               } while(this.closed.contains(var7));

               var9 = (ElementCollectionMesh.VSet)var6.getValue();

               assert var3 == -1 || var3 == var9.groupId;

               var3 = var9.groupId;
            } while(var9.diagonals.size() != 3);

            this.closed.add(var7);
            var5 = ElementCollectionMesh.Side.getPosX(var7);
            var10 = ElementCollectionMesh.Side.getPosY(var7);

            assert var9.set.size() == 2;

            Iterator var24 = var9.set.iterator();

            while(true) {
               int var14;
               int var15;
               do {
                  if (!var24.hasNext()) {
                     continue label162;
                  }

                  var14 = ElementCollectionMesh.Side.getPosX(var12 = (Long)var24.next());
                  var15 = ElementCollectionMesh.Side.getPosY(var12);
               } while(var10 != var15);

               int var16 = var14 > var5 ? -1 : 1;
               boolean var17 = false;
               int var21 = var5 + var16;
               boolean var11 = false;
               long var18 = ElementCollectionMesh.Side.getIndex(var5, var10);

               while(!var11 && this.pFull.containsKey(var18) && ((ElementCollectionMesh.VSet)this.pFull.get(var18)).set.contains(ElementCollectionMesh.Side.getIndex(var21, var10))) {
                  int var28 = var21;
                  var21 += var16;
                  var18 = ElementCollectionMesh.Side.getIndex(var28, var10);
                  if (var1.containsKey(var18)) {
                     this.closed.add(var18);
                     this.edgesToInsert.add(var7);
                     this.edgesToInsert.add(var18);
                     var11 = true;
                  }
               }

               if (!var11) {
                  this.pointsToInsert.add(var18);
                  this.edgesToInsert.add(var7);
                  this.edgesToInsert.add(var18);
               }
            }
         }
      }

      public void addPoint(long var1, ElementCollectionMesh.VSet var3) {
         Long2ObjectOpenHashMap var4;
         if ((var4 = (Long2ObjectOpenHashMap)this.groups.get(var3.groupId)) == null) {
            var4 = new Long2ObjectOpenHashMap();
            this.groups.put(var3.groupId, var4);
         }

         var4.put(var1, var3);
      }

      public void clear() {
         this.pFull.clear();
         Iterator var1 = this.pFull.values().iterator();

         while(var1.hasNext()) {
            ElementCollectionMesh.VSet.freeInst((ElementCollectionMesh.VSet)var1.next());
         }

         this.groups.clear();
         this.pointsToInsert.clear();
         this.closed.clear();
         this.edgesToInsert.clear();
         this.triangles.clear();
      }

      // $FF: synthetic method
      GridTriangulator(Object var1) {
         this();
      }
   }

   static class Side {
      private int sideId;
      private int index;
      private LongArrayList inner;
      private LongArrayList innerCon;
      private LongArrayList open;
      private LongOpenHashSet closed;
      private LongArrayList closedSubSet;
      private long[] lTmp;
      private Long2ObjectOpenHashMap pSet;
      private ElementCollectionMesh.GridTriangulator triangulator;

      private Side() {
         this.inner = new LongArrayList();
         this.innerCon = new LongArrayList();
         this.open = new LongArrayList();
         this.closed = new LongOpenHashSet();
         this.closedSubSet = new LongArrayList();
         this.lTmp = new long[]{Long.MAX_VALUE, Long.MAX_VALUE};
         this.pSet = new Long2ObjectOpenHashMap();
         this.triangulator = new ElementCollectionMesh.GridTriangulator();
      }

      public void clear() {
         this.pSet.clear();
         Iterator var1 = this.pSet.values().iterator();

         while(var1.hasNext()) {
            ElementCollectionMesh.VSet.freeInst((ElementCollectionMesh.VSet)var1.next());
         }

         this.inner.clear();
         this.innerCon.clear();
         this.closed.clear();
         this.closedSubSet.clear();
         this.triangulator.clear();
      }

      public static long getIndex(int var0, int var1) {
         return ((long)var0 & 4294967295L) + (((long)var1 & 4294967295L) << 32);
      }

      public static int getPosX(long var0) {
         return (int)var0;
      }

      public static int getPosY(long var0) {
         return (int)(var0 >> 32);
      }

      @Test
      public static void test() {
         for(int var0 = -32000; var0 < 32000; ++var0) {
            for(int var1 = -32000; var1 < 32000; ++var1) {
               long var2 = getIndex(var0, var1);

               assert var0 == getPosX(var2) : "X " + var0 + " != " + getPosX(var2) + "; " + var2 + "; (" + var0 + ", " + var1 + ")";

               assert var1 == getPosY(var2) : "Y " + var1 + " != " + getPosY(var2) + "; " + var2 + "; (" + var0 + ", " + var1 + ")";
            }
         }

      }

      private void markInnerVertices() {
         LongIterator var1 = this.pSet.keySet().iterator();

         while(var1.hasNext()) {
            long var2 = var1.nextLong();
            ElementCollectionMesh.VSet var4;
            LongOpenHashSet var5 = (var4 = (ElementCollectionMesh.VSet)this.pSet.get(var2)).set;

            assert var5.size() <= 4;

            int var23 = getPosX(var2);
            int var6 = getPosY(var2);
            long var7 = getIndex(var23 + 1, var6);
            long var9 = getIndex(var23 - 1, var6);
            long var11 = getIndex(var23, var6 + 1);
            long var13 = getIndex(var23, var6 - 1);
            long var15 = getIndex(var23 + 1, var6 + 1);
            long var17 = getIndex(var23 + 1, var6 - 1);
            long var19 = getIndex(var23 - 1, var6 + 1);
            long var21 = getIndex(var23 - 1, var6 - 1);
            this.pSet.get(var7);
            this.pSet.get(var9);
            this.pSet.get(var11);
            this.pSet.get(var13);
            if (var4.diagonals.size() == 4) {
               this.inner.add(var2);
            } else {
               this.lTmp[0] = Long.MAX_VALUE;
               this.lTmp[1] = Long.MAX_VALUE;
               var23 = 0;
               if (var4.diagonals.contains(var15) && var4.diagonals.contains(var17)) {
                  var23 = this.putInnerCon(var2, var7, 0);
               }

               if (var4.diagonals.contains(var19) && var4.diagonals.contains(var21)) {
                  var23 = this.putInnerCon(var2, var9, var23);
               }

               if (var4.diagonals.contains(var15) && var4.diagonals.contains(var19)) {
                  var23 = this.putInnerCon(var2, var11, var23);
               }

               if (var4.diagonals.contains(var17) && var4.diagonals.contains(var21)) {
                  var23 = this.putInnerCon(var2, var13, var23);
               }

               if (var23 > 0) {
                  this.innerCon.add(var2);
                  this.innerCon.add(this.lTmp[0]);
                  this.innerCon.add(this.lTmp[1]);
               }
            }
         }

      }

      private int putInnerCon(long var1, long var3, int var5) {
         assert var5 < 2 : var5;

         this.lTmp[var5] = var3;
         ++var5;
         return var5;
      }

      private void removeColinearVertices() {
         LongIterator var1 = this.pSet.keySet().iterator();

         while(true) {
            while(var1.hasNext()) {
               long var2;
               int var4 = getPosX(var2 = var1.nextLong());
               int var5 = getPosY(var2);
               ElementCollectionMesh.VSet var6;
               LongOpenHashSet var7 = (var6 = (ElementCollectionMesh.VSet)this.pSet.get(var2)).set;
               long var8 = Long.MAX_VALUE;
               long var10 = Long.MAX_VALUE;
               long var12 = Long.MAX_VALUE;
               long var14 = Long.MAX_VALUE;
               Iterator var16 = var7.iterator();

               while(true) {
                  long var17;
                  while(var16.hasNext()) {
                     int var19 = getPosX(var17 = (Long)var16.next());
                     int var20 = getPosY(var17);
                     if (var19 == var4 && var20 > var5) {
                        var8 = var17;
                     } else if (var19 == var4 && var20 < var5) {
                        var10 = var17;
                     } else if (var19 > var4 && var20 == var5) {
                        var12 = var17;
                     } else if (var19 < var4 && var20 == var5) {
                        var14 = var17;
                     }
                  }

                  ElementCollectionMesh.VSet var21;
                  ElementCollectionMesh.VSet var22;
                  if (var8 != Long.MAX_VALUE && var10 != Long.MAX_VALUE && (var14 == Long.MAX_VALUE || var12 == Long.MAX_VALUE)) {
                     var16 = var7.iterator();

                     while(var16.hasNext()) {
                        var17 = (Long)var16.next();
                        LongOpenHashSet var24;
                        if ((var24 = ((ElementCollectionMesh.VSet)this.pSet.get(var17)).set) != null) {
                           var24.remove(var2);
                        }
                     }

                     var1.remove();
                     ElementCollectionMesh.VSet.freeInst(var6);
                     if ((var21 = (ElementCollectionMesh.VSet)this.pSet.get(var8)) != null) {
                        var21.set.add(var10);
                     }

                     if ((var22 = (ElementCollectionMesh.VSet)this.pSet.get(var10)) != null) {
                        var22.set.add(var8);
                     }
                     break;
                  }

                  if (var14 != Long.MAX_VALUE && var12 != Long.MAX_VALUE && (var8 == Long.MAX_VALUE || var10 == Long.MAX_VALUE)) {
                     var16 = var7.iterator();

                     while(var16.hasNext()) {
                        var17 = (Long)var16.next();
                        ElementCollectionMesh.VSet var23;
                        if ((var23 = (ElementCollectionMesh.VSet)this.pSet.get(var17)) != null) {
                           var23.set.remove(var2);
                        }
                     }

                     var1.remove();
                     ElementCollectionMesh.VSet.freeInst(var6);
                     if ((var21 = (ElementCollectionMesh.VSet)this.pSet.get(var12)) != null) {
                        var21.set.add(var14);
                     }

                     if ((var22 = (ElementCollectionMesh.VSet)this.pSet.get(var14)) != null) {
                        var22.set.add(var12);
                     }
                  }
                  break;
               }
            }

            return;
         }
      }

      public void optimize(ElementCollection var1, long var2) {
         this.groupVertices(this.triangulator);
         if (!ElementCollectionMesh.isCanceled(var1, var2)) {
            if (ElementCollectionMesh.dbm.removeInner) {
               this.markInnerVertices();
               if (ElementCollectionMesh.isCanceled(var1, var2)) {
                  return;
               }

               this.removeInnerVertices();
               if (ElementCollectionMesh.isCanceled(var1, var2)) {
                  return;
               }
            }

            if (ElementCollectionMesh.dbm.removeColinear) {
               this.removeColinearVertices();
               if (ElementCollectionMesh.isCanceled(var1, var2)) {
                  return;
               }
            }

            if (ElementCollectionMesh.dbm.triangulate) {
               this.triangulate();
            }

         }
      }

      private void groupVertices(ElementCollectionMesh.GridTriangulator var1) {
         this.closed.clear();
         this.open.clear();
         this.open.addAll(this.pSet.keySet());
         this.closedSubSet.ensureCapacity(this.open.size());
         byte var2 = 1;

         label40:
         while(true) {
            long var3;
            do {
               if (this.open.isEmpty()) {
                  this.open.clear();
                  this.closed.clear();
                  return;
               }

               var3 = this.open.removeLong(this.open.size() - 1);
            } while(this.closed.contains(var3));

            this.closedSubSet.add(var3);

            while(true) {
               long var5;
               ElementCollectionMesh.VSet var9;
               do {
                  if (this.closedSubSet.isEmpty()) {
                     ++var2;
                     this.closedSubSet.clear();
                     continue label40;
                  }

                  var5 = this.closedSubSet.removeLong(this.closedSubSet.size() - 1);
                  this.closed.add(var5);
               } while((var9 = (ElementCollectionMesh.VSet)this.pSet.get(var5)) == null);

               var1.pFull.put(var5, ElementCollectionMesh.VSet.getInst(var9));
               var9.groupId = var2;
               Iterator var10 = var9.set.iterator();

               while(var10.hasNext()) {
                  long var7 = (Long)var10.next();
                  if (!this.closed.contains(var7)) {
                     this.closedSubSet.add(var7);
                  }
               }
            }
         }
      }

      private void triangulate() {
         Iterator var1 = this.pSet.long2ObjectEntrySet().iterator();

         while(var1.hasNext()) {
            Entry var2 = (Entry)var1.next();
            this.triangulator.addPoint(var2.getLongKey(), (ElementCollectionMesh.VSet)var2.getValue());
         }

         this.triangulator.triangulate(this.sideId, this.pSet);
      }

      private void removeInnerVertices() {
         Iterator var1 = this.inner.iterator();

         while(true) {
            long var2;
            ElementCollectionMesh.VSet var4;
            do {
               if (!var1.hasNext()) {
                  this.inner.clear();
                  int var8 = this.innerCon.size();

                  for(int var9 = 0; var9 < var8; var9 += 3) {
                     long var3 = this.innerCon.getLong(var9);
                     if ((var4 = (ElementCollectionMesh.VSet)this.pSet.get(var3)) != null) {
                        var4.set.remove(this.innerCon.getLong(var9 + 1));
                        var4.set.remove(this.innerCon.getLong(var9 + 2));
                     }
                  }

                  this.innerCon.clear();
                  return;
               }

               var2 = (Long)var1.next();
               ElementCollectionMesh.VSet.freeInst(var4 = (ElementCollectionMesh.VSet)this.pSet.remove(var2));
            } while(var4 == null);

            Iterator var10 = var4.set.iterator();

            while(var10.hasNext()) {
               long var6 = (Long)var10.next();
               ElementCollectionMesh.VSet var5;
               if ((var5 = (ElementCollectionMesh.VSet)this.pSet.get(var6)) != null) {
                  var5.set.remove(var2);
               }
            }
         }
      }

      // $FF: synthetic method
      Side(Object var1) {
         this();
      }
   }

   static class VSet {
      public static List pool = new ObjectArrayList();
      byte groupId = -1;
      final LongOpenHashSet set = new LongOpenHashSet(4);
      final LongOpenHashSet diagonals = new LongOpenHashSet(4);

      public static void freeInst(ElementCollectionMesh.VSet var0) {
         var0.clear();
         synchronized(pool) {
            pool.add(var0);
         }
      }

      private void clear() {
         this.set.clear();
         this.diagonals.clear();
         this.groupId = -1;
      }

      public static ElementCollectionMesh.VSet getInst() {
         synchronized(pool) {
            if (!pool.isEmpty()) {
               return (ElementCollectionMesh.VSet)pool.remove(pool.size() - 1);
            }
         }

         return new ElementCollectionMesh.VSet();
      }

      public static ElementCollectionMesh.VSet getInst(ElementCollectionMesh.VSet var0) {
         ElementCollectionMesh.VSet var1 = null;
         synchronized(pool) {
            if (!pool.isEmpty()) {
               var1 = (ElementCollectionMesh.VSet)pool.remove(pool.size() - 1);
            }
         }

         if (var1 == null) {
            var1 = new ElementCollectionMesh.VSet();
         }

         var1.set(var0);
         return var1;
      }

      public void set(ElementCollectionMesh.VSet var1) {
         this.set.addAll(var1.set);
         this.diagonals.addAll(var1.diagonals);
         this.groupId = var1.groupId;
      }

      private VSet(ElementCollectionMesh.VSet var1) {
         this.set(var1);
      }

      private VSet() {
      }
   }

   public static class DebugModes {
      public boolean removeInner = true;
      public boolean removeColinear = true;
      public boolean triangulate = true;
      public boolean quadrulate = true;
      public boolean retriangulate = true;
      private int step;

      public boolean step(int var1) {
         boolean var2 = this.step != var1;
         this.step = var1;
         switch(var1) {
         case 0:
            this.step0();
            break;
         case 1:
            this.step1();
            break;
         case 2:
            this.step2();
            break;
         case 3:
            this.step3();
            break;
         case 4:
            this.step4();
            break;
         case 5:
            this.step5();
         }

         return var2;
      }

      public void step0() {
         this.removeInner = false;
         this.removeColinear = false;
         this.triangulate = false;
         this.quadrulate = false;
         this.retriangulate = false;
      }

      public void step1() {
         this.removeInner = true;
         this.removeColinear = false;
         this.triangulate = false;
         this.quadrulate = false;
         this.retriangulate = false;
      }

      public void step2() {
         this.removeInner = true;
         this.removeColinear = true;
         this.triangulate = false;
         this.quadrulate = false;
         this.retriangulate = false;
      }

      public void step3() {
         this.removeInner = true;
         this.removeColinear = true;
         this.triangulate = true;
         this.quadrulate = false;
         this.retriangulate = false;
      }

      public void step4() {
         this.removeInner = true;
         this.removeColinear = true;
         this.triangulate = true;
         this.quadrulate = true;
         this.retriangulate = false;
      }

      public void step5() {
         this.removeInner = true;
         this.removeColinear = true;
         this.triangulate = true;
         this.quadrulate = true;
         this.retriangulate = true;
      }
   }
}
