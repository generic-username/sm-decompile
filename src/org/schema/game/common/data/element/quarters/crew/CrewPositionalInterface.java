package org.schema.game.common.data.element.quarters.crew;

import org.schema.game.common.controller.SegmentController;

public interface CrewPositionalInterface {
   SegmentController getSegmentController();
}
