package org.schema.game.common.data.element.quarters;

import org.schema.common.util.linAlg.Vector3i;

public class Area {
   public final Vector3i min = new Vector3i();
   public final Vector3i max = new Vector3i();
}
