package org.schema.game.common.data.element.mediawiki;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpRetryException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import javax.security.auth.login.AccountLockedException;
import javax.security.auth.login.CredentialException;
import javax.security.auth.login.CredentialExpiredException;
import javax.security.auth.login.CredentialNotFoundException;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;

public class Wiki {
   public static final int MEDIA_NAMESPACE = -2;
   public static final int SPECIAL_NAMESPACE = -1;
   public static final int MAIN_NAMESPACE = 0;
   public static final int TALK_NAMESPACE = 1;
   public static final int USER_NAMESPACE = 2;
   public static final int USER_TALK_NAMESPACE = 3;
   public static final int PROJECT_NAMESPACE = 4;
   public static final int PROJECT_TALK_NAMESPACE = 5;
   public static final int FILE_NAMESPACE = 6;
   public static final int FILE_TALK_NAMESPACE = 7;
   public static final int MEDIAWIKI_NAMESPACE = 8;
   public static final int MEDIAWIKI_TALK_NAMESPACE = 9;
   public static final int TEMPLATE_NAMESPACE = 10;
   public static final int TEMPLATE_TALK_NAMESPACE = 11;
   public static final int HELP_NAMESPACE = 12;
   public static final int HELP_TALK_NAMESPACE = 13;
   public static final int CATEGORY_NAMESPACE = 14;
   public static final int CATEGORY_TALK_NAMESPACE = 15;
   public static final int ALL_NAMESPACES = 167317762;
   public static final String ALL_LOGS = "";
   public static final String USER_CREATION_LOG = "newusers";
   public static final String UPLOAD_LOG = "upload";
   public static final String DELETION_LOG = "delete";
   public static final String MOVE_LOG = "move";
   public static final String BLOCK_LOG = "block";
   public static final String PROTECTION_LOG = "protect";
   public static final String USER_RIGHTS_LOG = "rights";
   public static final String USER_RENAME_LOG = "renameuser";
   public static final String IMPORT_LOG = "import";
   public static final String PATROL_LOG = "patrol";
   public static final String NO_PROTECTION = "all";
   public static final String SEMI_PROTECTION = "autoconfirmed";
   public static final String FULL_PROTECTION = "sysop";
   public static final int ASSERT_NONE = 0;
   public static final int ASSERT_USER = 1;
   public static final int ASSERT_BOT = 2;
   public static final int ASSERT_NO_MESSAGES = 4;
   public static final int ASSERT_SYSOP = 8;
   public static final int HIDE_ANON = 1;
   public static final int HIDE_BOT = 2;
   public static final int HIDE_SELF = 4;
   public static final int HIDE_MINOR = 8;
   public static final int HIDE_PATROLLED = 16;
   public static final long NEXT_REVISION = -1L;
   public static final long CURRENT_REVISION = -2L;
   public static final long PREVIOUS_REVISION = -3L;
   private static final String version = "0.30";
   private static final int CONNECTION_CONNECT_TIMEOUT_MSEC = 30000;
   private static final int CONNECTION_READ_TIMEOUT_MSEC = 180000;
   private static final int LOG2_CHUNK_SIZE = 22;
   protected String query;
   protected String base;
   protected String apiUrl;
   protected String scriptPath;
   private String domain;
   private boolean wgCapitalLinks;
   private String timezone;
   private HashMap cookies;
   private Wiki.User user;
   private int statuscounter;
   private HashMap namespaces;
   private ArrayList watchlist;
   private int max;
   private int slowmax;
   private int throttle;
   private int maxlag;
   private int assertion;
   private transient int statusinterval;
   private String useragent;
   private boolean zipped;
   private boolean markminor;
   private boolean markbot;
   private boolean resolveredirect;
   private boolean retry;

   public Wiki() {
      this("starmadepedia.net", "");
   }

   public Wiki(String var1) {
      this(var1, "");
   }

   public Wiki(String var1, String var2) {
      this.scriptPath = "/w";
      this.wgCapitalLinks = true;
      this.timezone = "UTC";
      this.cookies = new HashMap(12);
      this.statuscounter = 0;
      this.namespaces = null;
      this.watchlist = null;
      this.max = 500;
      this.slowmax = 50;
      this.throttle = 1000;
      this.maxlag = 5;
      this.assertion = 0;
      this.statusinterval = 100;
      this.useragent = "Wiki.java 0.30(https://code.google.com/p/wiki-java/)";
      this.zipped = false;
      this.markminor = false;
      this.markbot = false;
      this.resolveredirect = false;
      this.retry = true;
      if (var1 == null || var1.isEmpty()) {
         var1 = "starmadepedia.net";
      }

      this.domain = var1;
      this.scriptPath = var2;
      this.log(Level.CONFIG, "<init>", "Using Wiki.java 0.30");
      this.initVars();
   }

   public static String[] intersection(String[] var0, String[] var1) {
      ArrayList var2;
      (var2 = new ArrayList(5000)).addAll(Arrays.asList(var0));
      var2.retainAll(Arrays.asList(var1));
      return (String[])var2.toArray(new String[var2.size()]);
   }

   public static String[] relativeComplement(String[] var0, String[] var1) {
      ArrayList var2;
      (var2 = new ArrayList(5000)).addAll(Arrays.asList(var0));
      var2.removeAll(Arrays.asList(var1));
      return (String[])var2.toArray(new String[var2.size()]);
   }

   protected void initVars() {
      StringBuilder var1;
      (var1 = new StringBuilder("https://")).append(this.domain);
      var1.append(this.scriptPath);
      StringBuilder var2;
      (var2 = new StringBuilder(var1)).append("/api.php?format=xml&");
      if (this.maxlag >= 0) {
         var2.append("maxlag=");
         var2.append(this.maxlag);
         var2.append("&");
         var1.append("/index.php?maxlag=");
         var1.append(this.maxlag);
         var1.append("&title=");
      } else {
         var1.append("/index.php?title=");
      }

      this.base = var1.toString();
      if ((this.assertion & 2) == 2) {
         var2.append("assert=bot&");
      } else if ((this.assertion & 1) == 1) {
         var2.append("assert=user&");
      }

      this.apiUrl = var2.toString();
      var2.append("action=query&");
      if (this.resolveredirect) {
         var2.append("redirects&");
      }

      this.query = var2.toString();
   }

   public String getDomain() {
      return this.domain;
   }

   public int getThrottle() {
      return this.throttle;
   }

   public void setThrottle(int var1) {
      this.throttle = var1;
      this.log(Level.CONFIG, "setThrottle", "Throttle set to " + var1 + " milliseconds");
   }

   @Deprecated
   public String getScriptPath() throws IOException {
      return (String)this.getSiteInfo().get("scriptpath");
   }

   @Deprecated
   public boolean isUsingCapitalLinks() throws IOException {
      return (Boolean)this.getSiteInfo().get("usingcapitallinks");
   }

   public HashMap getSiteInfo() throws IOException {
      HashMap var1 = new HashMap();
      String var2 = this.fetch(this.query + "action=query&meta=siteinfo", "getSiteInfo");
      this.wgCapitalLinks = this.parseAttribute(var2, "case", 0).equals("first-letter");
      var1.put("usingcapitallinks", this.wgCapitalLinks);
      this.scriptPath = this.parseAttribute(var2, "scriptpath", 0);
      var1.put("scriptpath", this.scriptPath);
      this.timezone = this.parseAttribute(var2, "timezone", 0);
      var1.put("timezone", this.timezone);
      var1.put("version", this.parseAttribute(var2, "generator", 0));
      this.initVars();
      return var1;
   }

   public String getUserAgent() {
      return this.useragent;
   }

   public void setUserAgent(String var1) {
      this.useragent = var1;
   }

   public boolean isUsingCompressedRequests() {
      return this.zipped;
   }

   public void setUsingCompressedRequests(boolean var1) {
      this.zipped = var1;
   }

   public boolean isResolvingRedirects() {
      return this.resolveredirect;
   }

   public void setResolveRedirects(boolean var1) {
      this.resolveredirect = var1;
      this.initVars();
   }

   public boolean isMarkBot() {
      return this.markbot;
   }

   public void setMarkBot(boolean var1) {
      this.markbot = var1;
   }

   public boolean isMarkMinor() {
      return this.markminor;
   }

   public void setMarkMinor(boolean var1) {
      this.markminor = var1;
   }

   public boolean equals(Object var1) {
      return !(var1 instanceof Wiki) ? false : this.domain.equals(((Wiki)var1).domain);
   }

   public int hashCode() {
      return this.domain.hashCode() * this.maxlag - this.throttle;
   }

   public int getMaxLag() {
      return this.maxlag;
   }

   public String toString() {
      StringBuilder var1;
      (var1 = new StringBuilder("Wiki[domain=")).append(this.domain);
      var1.append(",user=");
      var1.append(this.user != null ? this.user.toString() : "null");
      var1.append(",");
      var1.append("throttle=");
      var1.append(this.throttle);
      var1.append(",maxlag=");
      var1.append(this.maxlag);
      var1.append(",assertionMode=");
      var1.append(this.assertion);
      var1.append(",statusCheckInterval=");
      var1.append(this.statusinterval);
      var1.append(",cookies=");
      var1.append(this.cookies);
      var1.append("]");
      return var1.toString();
   }

   public void setMaxLag(int var1) {
      this.maxlag = var1;
      this.log(Level.CONFIG, "setMaxLag", "Setting maximum allowable database lag to " + var1);
      this.initVars();
   }

   public int getAssertionMode() {
      return this.assertion;
   }

   public void setAssertionMode(int var1) {
      this.assertion = var1;
      this.log(Level.CONFIG, "setAssertionMode", "Set assertion mode to " + var1);
      this.initVars();
   }

   public int getStatusCheckInterval() {
      return this.statusinterval;
   }

   public void setStatusCheckInterval(int var1) {
      this.statusinterval = var1;
      this.log(Level.CONFIG, "setStatusCheckInterval", "Status check interval set to " + var1);
   }

   public synchronized void login(String var1, char[] var2) throws IOException, FailedLoginException {
      var1 = this.normalize(var1);
      StringBuilder var3;
      (var3 = new StringBuilder(500)).append("lgname=");
      var3.append(URLEncoder.encode(var1, "UTF-8"));
      var3.append("&lgpassword=");
      var3.append(URLEncoder.encode(new String(var2), "UTF-8"));
      String var5 = this.post(this.apiUrl + "action=login", var3.toString(), "login");
      var5 = this.parseAttribute(var5, "token", 0);
      System.err.println("#######TOKEN: " + var5);
      var3.append("&lgtoken=");
      var3.append(URLEncoder.encode(var5, "UTF-8"));
      if ((var5 = this.post(this.apiUrl + "action=login", var3.toString(), "login")).contains("result=\"Success\"")) {
         this.user = new Wiki.User(var1);
         boolean var6;
         if (var6 = this.user.isAllowedTo("apihighlimits")) {
            this.max = 5000;
            this.slowmax = 500;
         }

         this.log(Level.INFO, "login", "Successfully logged in as " + var1 + ", highLimit = " + var6);
      } else {
         this.log(Level.WARNING, "login", "Failed to log in as " + var1);

         try {
            Thread.sleep(20000L);
         } catch (InterruptedException var4) {
         }

         if (!var5.contains("WrongPass") && !var5.contains("WrongPluginPass")) {
            if (var5.contains("NotExists")) {
               throw new FailedLoginException("Login failed: user does not exist.");
            } else {
               throw new FailedLoginException("Login failed: unknown reason.");
            }
         } else {
            throw new FailedLoginException("Login failed: incorrect password.");
         }
      }
   }

   public synchronized void login(String var1, String var2) throws IOException, FailedLoginException {
      this.login(var1, var2.toCharArray());
   }

   public synchronized void logout() {
      this.cookies.clear();
      this.user = null;
      this.max = 500;
      this.slowmax = 50;
      this.log(Level.INFO, "logout", "Logged out");
   }

   public synchronized void logoutServerSide() throws IOException {
      this.fetch(this.apiUrl + "action=logout", "logoutServerSide");
      this.logout();
   }

   public boolean hasNewMessages() throws IOException {
      String var1 = this.query + "meta=userinfo&uiprop=hasmsg";
      return this.fetch(var1, "hasNewMessages").contains("messages=\"\"");
   }

   public int getCurrentDatabaseLag() throws IOException {
      String var1 = this.fetch(this.query + "meta=siteinfo&siprop=dbrepllag", "getCurrentDatabaseLag");
      var1 = this.parseAttribute(var1, "lag", 0);
      this.log(Level.INFO, "getCurrentDatabaseLag", "Current database replication lag is " + var1 + " seconds");
      return Integer.parseInt(var1);
   }

   public HashMap getSiteStatistics() throws IOException {
      String var1 = this.fetch(this.query + "meta=siteinfo&siprop=statistics", "getSiteStatistics");
      HashMap var2;
      (var2 = new HashMap(20)).put("pages", Integer.parseInt(this.parseAttribute(var1, "pages", 0)));
      var2.put("articles", Integer.parseInt(this.parseAttribute(var1, "articles", 0)));
      var2.put("files", Integer.parseInt(this.parseAttribute(var1, "images", 0)));
      var2.put("users", Integer.parseInt(this.parseAttribute(var1, "users", 0)));
      var2.put("activeusers", Integer.parseInt(this.parseAttribute(var1, "activeusers", 0)));
      var2.put("admins", Integer.parseInt(this.parseAttribute(var1, "admins", 0)));
      var2.put("jobs", Integer.parseInt(this.parseAttribute(var1, "jobs", 0)));
      return var2;
   }

   @Deprecated
   public String version() throws IOException {
      return (String)this.getSiteInfo().get("version");
   }

   public String parse(String var1) throws IOException {
      int var2 = (var1 = this.post(this.apiUrl + "action=parse", "prop=text&text=" + URLEncoder.encode(var1, "UTF-8"), "parse")).indexOf(62, var1.indexOf("<text")) + 1;
      int var3 = var1.indexOf("</text>");
      return this.decode(var1.substring(var2, var3));
   }

   protected String parseAndCleanup(String var1) throws IOException {
      int var2 = (var1 = this.parse(var1).replace("<p>", "").replace("</p>", "").replace("\n", "")).indexOf("<!--");
      return var1.substring(0, var2);
   }

   public String random() throws IOException {
      return this.random(0);
   }

   public String random(int... var1) throws IOException {
      StringBuilder var2;
      (var2 = new StringBuilder(this.query)).append("list=random");
      this.constructNamespaceString(var2, "rn", var1);
      String var3 = this.fetch(var2.toString(), "random");
      return this.decode(this.parseAttribute(var3, "title", 0));
   }

   public String getTalkPage(String var1) throws IOException {
      int var2;
      if ((var2 = this.namespace(var1)) % 2 == 1) {
         throw new IllegalArgumentException("Cannot fetch talk page of a talk page!");
      } else if (var2 < 0) {
         throw new IllegalArgumentException("Special: and Media: pages do not have talk pages!");
      } else {
         if (var2 != 0) {
            var1 = var1.substring(var1.indexOf(58) + 1);
         }

         return this.namespaceIdentifier(var2 + 1) + ":" + var1;
      }
   }

   public HashMap getPageInfo(String var1) throws IOException {
      return this.getPageInfo(new String[]{var1})[0];
   }

   public HashMap[] getPageInfo(String[] var1) throws IOException {
      HashMap[] var2 = new HashMap[var1.length];
      StringBuilder var3;
      (var3 = new StringBuilder(this.query)).append("prop=info&intoken=edit%7Cwatch&inprop=protection%7Cdisplaytitle%7Cwatchers&titles=");
      String[] var4;
      int var5 = (var4 = this.constructTitleString(var1)).length;

      for(int var6 = 0; var6 < var5; ++var6) {
         String var7 = var4[var6];

         for(int var8 = (var7 = this.fetch(var3.toString() + var7, "getPageInfo")).indexOf("<page "); var8 > 0; var8 = var7.indexOf("<page ", var8)) {
            int var9 = var7.indexOf("</page>", var8);
            String var16 = var7.substring(var8, var9);
            HashMap var10 = new HashMap(15);
            boolean var11 = !var16.contains("missing=\"\"");
            var10.put("exists", var11);
            if (var11) {
               var10.put("lastpurged", this.timestampToCalendar(this.parseAttribute(var16, "touched", 0), true));
               var10.put("lastrevid", Long.parseLong(this.parseAttribute(var16, "lastrevid", 0)));
               var10.put("size", Integer.parseInt(this.parseAttribute(var16, "length", 0)));
            } else {
               var10.put("lastedited", (Object)null);
               var10.put("lastrevid", -1L);
               var10.put("size", -1);
            }

            HashMap var12 = new HashMap();

            for(int var13 = var16.indexOf("<pr "); var13 > 0; var13 = var16.indexOf("<pr ", var13)) {
               String var14 = this.parseAttribute(var16, "type", var13);
               String var15 = this.parseAttribute(var16, "level", var13);
               var12.put(var14, var15);
               if ((var15 = this.parseAttribute(var16, "expiry", var13)).equals("infinity")) {
                  var12.put(var14 + "expiry", (Object)null);
               } else {
                  var12.put(var14 + "expiry", this.timestampToCalendar(var15, true));
               }

               if (var16.contains("source=\"")) {
                  var12.put("cascadesource", this.parseAttribute(var16, "source", var13));
               }

               ++var13;
            }

            String var17 = this.decode(this.parseAttribute(var16, "title", 0));
            if (this.namespace(var17) == 8) {
               var12.put("edit", "sysop");
               var12.put("move", "sysop");
               if (!var11) {
                  var12.put("create", "sysop");
               }
            }

            var12.put("cascade", var16.contains("cascade=\"\""));
            var10.put("protection", var12);
            var10.put("displaytitle", this.parseAttribute(var16, "displaytitle", 0));
            var10.put("token", this.parseAttribute(var16, "edittoken", 0));
            var10.put("timestamp", this.makeCalendar());
            if (this.user != null) {
               var10.put("watchtoken", this.parseAttribute(var16, "watchtoken", 0));
            }

            if (var16.contains("watchers=\"")) {
               var10.put("watchers", Integer.parseInt(this.parseAttribute(var16, "watchers", 0)));
            }

            for(int var18 = 0; var18 < var1.length; ++var18) {
               if (this.normalize(var1[var18]).equals(var17)) {
                  var2[var18] = var10;
               }
            }

            ++var8;
         }
      }

      this.log(Level.INFO, "getPageInfo", "Successfully retrieved page info for " + Arrays.toString(var1));
      return var2;
   }

   public int namespace(String var1) throws IOException {
      if (this.namespaces == null) {
         this.populateNamespaceCache();
      }

      if (!var1.contains(":")) {
         return 0;
      } else if ((var1 = var1.substring(0, var1.indexOf(58))).equals("Project_talk")) {
         return 5;
      } else if (var1.equals("Project")) {
         return 4;
      } else {
         return !this.namespaces.containsKey(var1) ? 0 : (Integer)this.namespaces.get(var1);
      }
   }

   public String namespaceIdentifier(int var1) throws IOException {
      if (this.namespaces == null) {
         this.populateNamespaceCache();
      }

      if (!this.namespaces.containsValue(var1)) {
         return "";
      } else {
         Iterator var2 = this.namespaces.entrySet().iterator();

         Entry var3;
         do {
            if (!var2.hasNext()) {
               return "";
            }
         } while(!((Integer)(var3 = (Entry)var2.next()).getValue()).equals(var1));

         return (String)var3.getKey();
      }
   }

   public HashMap getNamespaces() throws IOException {
      if (this.namespaces == null) {
         this.populateNamespaceCache();
      }

      return (HashMap)this.namespaces.clone();
   }

   protected void populateNamespaceCache() throws IOException {
      String var1 = this.fetch(this.query + "meta=siteinfo&siprop=namespaces", "namespace");
      this.namespaces = new HashMap(30);

      for(int var2 = var1.indexOf("<ns "); var2 > 0; var2 = var1.indexOf("<ns ", var2)) {
         String var3 = this.parseAttribute(var1, "id", var2);
         int var4 = var1.indexOf(62, var2) + 1;
         int var5 = var1.indexOf(60, var4);
         this.namespaces.put(this.normalize(this.decode(var1.substring(var4, var5))), new Integer(var3));
         ++var2;
      }

      this.log(Level.INFO, "namespace", "Successfully retrieved namespace list (" + this.namespaces.size() + " namespaces)");
   }

   public boolean[] exists(String[] var1) throws IOException {
      boolean[] var2 = new boolean[var1.length];
      HashMap[] var3 = this.getPageInfo(var1);

      for(int var4 = 0; var4 < var1.length; ++var4) {
         var2[var4] = (Boolean)var3[var4].get("exists");
      }

      return var2;
   }

   public String getPageText(String var1) throws IOException {
      if (this.namespace(var1) < 0) {
         throw new UnsupportedOperationException("Cannot retrieve Special: or Media: pages!");
      } else {
         String var2 = this.base + URLEncoder.encode(this.normalize(var1), "UTF-8") + "&action=raw";
         var2 = this.fetch(var2, "getPageText");
         this.log(Level.INFO, "getPageText", "Successfully retrieved text of " + var1);
         return var2;
      }
   }

   public String getSectionText(String var1, int var2) throws IOException {
      StringBuilder var3;
      (var3 = new StringBuilder(this.query)).append("prop=revisions&rvprop=content&titles=");
      var3.append(URLEncoder.encode(var1, "UTF-8"));
      var3.append("&rvsection=");
      var3.append(var2);
      if (!(var1 = this.fetch(var3.toString(), "getSectionText")).contains("</rev>")) {
         return "";
      } else {
         var2 = var1.indexOf("xml:space=\"preserve\">") + 21;
         int var4 = var1.indexOf("</rev>", var2);
         return this.decode(var1.substring(var2, var4));
      }
   }

   public String getRenderedText(String var1) throws IOException {
      return this.parse("{{:" + var1 + "}}");
   }

   public void edit(String var1, String var2, String var3) throws IOException, LoginException {
      this.edit(var1, var2, var3, this.markminor, this.markbot, -2, (Calendar)null);
   }

   public void edit(String var1, String var2, String var3, Calendar var4) throws IOException, LoginException {
      this.edit(var1, var2, var3, this.markminor, this.markbot, -2, var4);
   }

   public void edit(String var1, String var2, String var3, int var4) throws IOException, LoginException {
      this.edit(var1, var2, var3, this.markminor, this.markbot, var4, (Calendar)null);
   }

   public void edit(String var1, String var2, String var3, int var4, Calendar var5) throws IOException, LoginException {
      this.edit(var1, var2, var3, this.markminor, this.markbot, var4, var5);
   }

   public synchronized void edit(String var1, String var2, String var3, boolean var4, boolean var5, int var6, Calendar var7) throws IOException, LoginException {
      long var8 = System.currentTimeMillis();
      HashMap var10 = this.getPageInfo(var1);
      if (!this.checkRights(var10, "edit") || (Boolean)var10.get("exists") && !this.checkRights(var10, "create")) {
         CredentialException var15 = new CredentialException("Permission denied: page is protected.");
         this.log(Level.WARNING, "edit", "Cannot edit - permission denied. " + var15);
         throw var15;
      } else {
         String var11 = (String)var10.get("token");
         StringBuilder var12;
         (var12 = new StringBuilder(300000)).append("title=");
         var12.append(URLEncoder.encode(this.normalize(var1), "UTF-8"));
         var12.append("&text=");
         var12.append(URLEncoder.encode(var2, "UTF-8"));
         var12.append("&summary=");
         var12.append(URLEncoder.encode(var3, "UTF-8"));
         var12.append("&token=");
         var12.append(URLEncoder.encode(var11, "UTF-8"));
         if (var7 != null) {
            var12.append("&starttimestamp=");
            var12.append(this.calendarToTimestamp((Calendar)var10.get("timestamp")));
            var12.append("&basetimestamp=");
            var12.append(this.calendarToTimestamp(var7));
         }

         if (var4) {
            var12.append("&minor=1");
         }

         if (var5 && this.user.isAllowedTo("bot")) {
            var12.append("&bot=1");
         }

         if (var6 == -1) {
            var12.append("&section=new");
         } else if (var6 != -2) {
            var12.append("&section=");
            var12.append(var6);
         }

         String var14;
         if ((var14 = this.post(this.apiUrl + "action=edit", var12.toString(), "edit")).contains("error code=\"editconflict\"")) {
            this.log(Level.WARNING, "edit", "Edit conflict on " + var1);
         } else {
            try {
               this.checkErrorsAndUpdateStatus(var14, "edit");
            } catch (IOException var13) {
               if (!this.retry) {
                  this.log(Level.SEVERE, "edit", "EXCEPTION: " + var13);
                  throw var13;
               }

               this.retry = false;
               this.log(Level.WARNING, "edit", "Exception: " + var13.getMessage() + " Retrying...");
               this.edit(var1, var2, var3, var4, var5, var6, var7);
            }

            if (this.retry) {
               this.log(Level.INFO, "edit", "Successfully edited " + var1);
            }

            this.retry = true;
            this.throttle(var8);
         }
      }
   }

   public void newSection(String var1, String var2, String var3, boolean var4, boolean var5) throws IOException, LoginException {
      this.edit(var1, var3, var2, var4, var5, -1, (Calendar)null);
   }

   public void prepend(String var1, String var2, String var3, boolean var4, boolean var5) throws IOException, LoginException {
      StringBuilder var6;
      (var6 = new StringBuilder(100000)).append(var2);
      var6.append(this.getSectionText(var1, 0));
      this.edit(var1, var6.toString(), var3, var4, var5, 0, (Calendar)null);
   }

   public synchronized void delete(String var1, String var2) throws IOException, LoginException {
      long var3 = System.currentTimeMillis();
      if (this.user != null && this.user.isAllowedTo("delete")) {
         HashMap var5;
         if (!(Boolean)(var5 = this.getPageInfo(var1)).get("exists")) {
            this.log(Level.INFO, "delete", "Page \"" + var1 + "\" does not exist.");
         } else {
            String var8 = (String)var5.get("token");
            StringBuilder var6;
            (var6 = new StringBuilder(500)).append("title=");
            var6.append(URLEncoder.encode(this.normalize(var1), "UTF-8"));
            var6.append("&reason=");
            var6.append(URLEncoder.encode(var2, "UTF-8"));
            var6.append("&token=");
            var6.append(URLEncoder.encode(var8, "UTF-8"));
            var8 = this.post(this.apiUrl + "action=delete", var6.toString(), "delete");

            try {
               if (!var8.contains("<delete title=")) {
                  this.checkErrorsAndUpdateStatus(var8, "delete");
               }
            } catch (IOException var7) {
               if (!this.retry) {
                  this.log(Level.SEVERE, "delete", "EXCEPTION: " + var7);
                  throw var7;
               }

               this.retry = false;
               this.log(Level.WARNING, "delete", "Exception: " + var7.getMessage() + " Retrying...");
               this.delete(var1, var2);
            }

            if (this.retry) {
               this.log(Level.INFO, "delete", "Successfully deleted " + var1);
            }

            this.retry = true;
            this.throttle(var3);
         }
      } else {
         throw new CredentialNotFoundException("Cannot delete: Permission denied");
      }
   }

   public synchronized void undelete(String var1, String var2, Wiki.Revision... var3) throws IOException, LoginException {
      long var4 = System.currentTimeMillis();
      if (this.user != null && this.user.isAllowedTo("undelete")) {
         String var6 = URLEncoder.encode(this.normalize(var1), "UTF-8");
         String var7;
         if (!(var7 = this.query + "action=query&list=deletedrevs&drlimit=1&drprop=token&titles=" + var6).contains("token=\"")) {
            this.log(Level.WARNING, "undelete", "Page \"" + var1 + "\" has no deleted revisions!");
         } else {
            var7 = this.parseAttribute(var7, "token", 0);
            StringBuilder var8;
            (var8 = new StringBuilder("title=")).append(var6);
            var8.append("&reason=");
            var8.append(URLEncoder.encode(var2, "UTF-8"));
            var8.append("&token=");
            var8.append(URLEncoder.encode(var7, "UTF-8"));
            if (var3.length != 0) {
               var8.append("&timestamps=");

               for(int var10 = 0; var10 < var3.length - 1; ++var10) {
                  var8.append(this.calendarToTimestamp(var3[var10].getTimestamp()));
                  var8.append("%7C");
               }

               var8.append(this.calendarToTimestamp(var3[var3.length - 1].getTimestamp()));
            }

            var6 = this.post(this.apiUrl + "action=undelete", var8.toString(), "undelete");

            try {
               if (!var6.contains("<undelete title=")) {
                  this.checkErrorsAndUpdateStatus(var6, "undelete");
               }
            } catch (IOException var9) {
               if (!this.retry) {
                  this.log(Level.SEVERE, "undelete", "EXCEPTION: " + var9);
                  throw var9;
               }

               this.retry = false;
               this.log(Level.WARNING, "undelete", "Exception: " + var9.getMessage() + " Retrying...");
               this.delete(var1, var2);
            }

            if (this.retry) {
               this.log(Level.INFO, "undelete", "Successfully undeleted " + var1);
            }

            this.retry = true;
            this.throttle(var4);
         }
      } else {
         throw new CredentialNotFoundException("Cannot undelete: Permission denied");
      }
   }

   public void purge(boolean var1, String... var2) throws IOException {
      StringBuilder var3;
      (var3 = new StringBuilder(this.apiUrl)).append("action=purge");
      if (var1) {
         var3.append("&forcelinkupdate");
      }

      String[] var7;
      int var4 = (var7 = this.constructTitleString(var2)).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         String var6 = var7[var5];
         this.post(var3.toString(), "&titles=" + var6, "purge");
      }

      this.log(Level.INFO, "purge", "Successfully purged " + var2.length + " pages.");
   }

   public String[] getImagesOnPage(String var1) throws IOException {
      String var2 = this.query + "prop=images&imlimit=max&titles=" + URLEncoder.encode(this.normalize(var1), "UTF-8");
      var2 = this.fetch(var2, "getImagesOnPage");
      ArrayList var3 = new ArrayList(750);

      int var4;
      for(var4 = var2.indexOf("<im "); var4 > 0; var4 = var2.indexOf("<im ", var4)) {
         var3.add(this.decode(this.parseAttribute(var2, "title", var4)));
         ++var4;
      }

      var4 = var3.size();
      this.log(Level.INFO, "getImagesOnPage", "Successfully retrieved images used on " + var1 + " (" + var4 + " images)");
      return (String[])var3.toArray(new String[var4]);
   }

   public String[] getCategories(String var1) throws IOException {
      return this.getCategories(var1, false, false);
   }

   public String[] getCategories(String var1, boolean var2, boolean var3) throws IOException {
      StringBuilder var4;
      (var4 = new StringBuilder(this.query)).append("prop=categories&cllimit=max");
      if (var2 || var3) {
         var4.append("&clprop=sortkey%7Chidden");
      }

      var4.append("&titles=");
      var4.append(URLEncoder.encode(var1, "UTF-8"));
      String var9 = this.fetch(var4.toString(), "getCategories");
      ArrayList var5 = new ArrayList(750);

      int var7;
      for(int var6 = var9.indexOf("<cl "); var6 > 0; var6 = var7) {
         var7 = var9.indexOf("<cl ", var6 + 1);
         if (!var3 || !var9.substring(var6, var7 > 0 ? var7 : var9.length()).contains("hidden")) {
            String var8 = this.decode(this.parseAttribute(var9, "title", var6));
            if (var2) {
               var8 = var8 + "|" + this.parseAttribute(var9, "sortkeyprefix", var6);
            }

            var5.add(var8);
         }
      }

      int var10 = var5.size();
      this.log(Level.INFO, "getCategories", "Successfully retrieved categories of " + var1 + " (" + var10 + " categories)");
      return (String[])var5.toArray(new String[var10]);
   }

   public String[] getTemplates(String var1, int... var2) throws IOException {
      StringBuilder var3;
      (var3 = new StringBuilder(this.query)).append("prop=templates&tllimit=max&titles=");
      var3.append(URLEncoder.encode(this.normalize(var1), "UTF-8"));
      this.constructNamespaceString(var3, "tl", var2);
      String var5 = this.fetch(var3.toString(), "getTemplates");
      ArrayList var6 = new ArrayList(750);

      int var4;
      for(var4 = var5.indexOf("<tl "); var4 > 0; var4 = var5.indexOf("<tl ", var4)) {
         var6.add(this.decode(this.parseAttribute(var5, "title", var4)));
         ++var4;
      }

      var4 = var6.size();
      this.log(Level.INFO, "getTemplates", "Successfully retrieved templates used on " + var1 + " (" + var4 + " templates)");
      return (String[])var6.toArray(new String[var4]);
   }

   public HashMap getInterWikiLinks(String var1) throws IOException {
      String var2 = this.query + "prop=langlinks&lllimit=max&titles=" + URLEncoder.encode(this.normalize(var1), "UTF-8");
      var2 = this.fetch(var2, "getInterwikiLinks");
      HashMap var3 = new HashMap(750);

      for(int var4 = var2.indexOf("<ll "); var4 > 0; var4 = var2.indexOf("<ll ", var4)) {
         String var5 = this.parseAttribute(var2, "lang", var4);
         int var6 = var2.indexOf(62, var4) + 1;
         int var7 = var2.indexOf(60, var6);
         String var8 = this.decode(var2.substring(var6, var7));
         var3.put(var5, var8);
         ++var4;
      }

      this.log(Level.INFO, "getInterWikiLinks", "Successfully retrieved interwiki links on " + var1);
      return var3;
   }

   public String[] getLinksOnPage(String var1) throws IOException {
      StringBuilder var2;
      (var2 = new StringBuilder(this.query)).append("prop=links&pllimit=max&titles=");
      var2.append(URLEncoder.encode(this.normalize(var1), "UTF-8"));
      String var3 = null;
      ArrayList var4 = new ArrayList(750);

      do {
         String var5;
         if (var3 == null) {
            var5 = this.fetch(var2.toString(), "getLinksOnPage");
         } else {
            var5 = this.fetch(var2.toString() + "&plcontinue=" + URLEncoder.encode(var3, "UTF-8"), "getLinksOnPage");
         }

         var3 = this.parseAttribute(var5, "plcontinue", 0);

         for(int var6 = var5.indexOf("<pl "); var6 > 0; var6 = var5.indexOf("<pl ", var6)) {
            var4.add(this.decode(this.parseAttribute(var5, "title", var6)));
            ++var6;
         }
      } while(var3 != null);

      int var7 = var4.size();
      this.log(Level.INFO, "getLinksOnPage", "Successfully retrieved links used on " + var1 + " (" + var7 + " links)");
      return (String[])var4.toArray(new String[var7]);
   }

   public String[] getExternalLinksOnPage(String var1) throws IOException {
      StringBuilder var2;
      (var2 = new StringBuilder(this.query)).append("prop=extlinks&ellimit=max&titles=");
      var2.append(URLEncoder.encode(this.normalize(var1), "UTF-8"));
      String var3 = null;
      ArrayList var4 = new ArrayList(750);

      do {
         String var5;
         if (var3 == null) {
            var5 = this.fetch(var2.toString(), "getExternalLinksOnPage");
         } else {
            var5 = this.fetch(var2.toString() + "&eloffset=" + URLEncoder.encode(var3, "UTF-8"), "getExternalLinksOnPage");
         }

         var3 = this.parseAttribute(var5, "eloffset", 0);

         for(int var6 = var5.indexOf("<el "); var6 > 0; var6 = var5.indexOf("<el ", var6)) {
            int var7 = var5.indexOf(62, var6) + 1;
            int var8 = var5.indexOf("</el>", var7);
            var4.add(this.decode(var5.substring(var7, var8)));
            ++var6;
         }
      } while(var3 != null);

      int var9 = var4.size();
      this.log(Level.INFO, "getExternalLinksOnPage", "Successfully retrieved external links used on " + var1 + " (" + var9 + " links)");
      return (String[])var4.toArray(new String[var9]);
   }

   public LinkedHashMap getSectionMap(String var1) throws IOException {
      String var2 = this.apiUrl + "action=parse&text={{:" + URLEncoder.encode(var1, "UTF-8") + "}}__TOC__&prop=sections";
      var2 = this.fetch(var2, "getSectionMap");
      LinkedHashMap var3 = new LinkedHashMap(30);

      for(int var4 = var2.indexOf("<s "); var4 > 0; var4 = var2.indexOf("<s ", var4)) {
         String var5 = this.decode(this.parseAttribute(var2, "line", var4));
         String var6 = this.parseAttribute(var2, "number", var4);
         var3.put(var6, var5);
         ++var4;
      }

      this.log(Level.INFO, "getSectionMap", "Successfully retrieved section map for " + var1);
      return var3;
   }

   public Wiki.Revision getTopRevision(String var1) throws IOException {
      StringBuilder var2;
      (var2 = new StringBuilder(this.query)).append("prop=revisions&rvlimit=1&rvtoken=rollback&titles=");
      var2.append(URLEncoder.encode(this.normalize(var1), "UTF-8"));
      var2.append("&rvprop=timestamp%7Cuser%7Cids%7Cflags%7Csize%7Ccomment");
      String var5;
      int var3 = (var5 = this.fetch(var2.toString(), "getTopRevision")).indexOf("<rev ");
      int var4 = var5.indexOf("/>", var3);
      return var3 < 0 ? null : this.parseRevision(var5.substring(var3, var4), var1);
   }

   public Wiki.Revision getFirstRevision(String var1) throws IOException {
      StringBuilder var2;
      (var2 = new StringBuilder(this.query)).append("prop=revisions&rvlimit=1&rvdir=newer&titles=");
      var2.append(URLEncoder.encode(this.normalize(var1), "UTF-8"));
      var2.append("&rvprop=timestamp%7Cuser%7Cids%7Cflags%7Csize%7Ccomment");
      String var5;
      int var3 = (var5 = this.fetch(var2.toString(), "getFirstRevision")).indexOf("<rev ");
      int var4 = var5.indexOf("/>", var3);
      return var3 < 0 ? null : this.parseRevision(var5.substring(var3, var4), var1);
   }

   public String resolveRedirect(String var1) throws IOException {
      return this.resolveRedirects(new String[]{var1})[0];
   }

   public String[] resolveRedirects(String[] var1) throws IOException {
      StringBuilder var2 = new StringBuilder(this.query);
      if (!this.resolveredirect) {
         var2.append("redirects&");
      }

      var2.append("titles=");
      String[] var3 = new String[var1.length];
      String[] var4;
      int var5 = (var4 = this.constructTitleString(var1)).length;

      for(int var6 = 0; var6 < var5; ++var6) {
         String var7 = var4[var6];

         for(int var8 = (var7 = this.fetch(var2.toString() + var7, "resolveRedirects")).indexOf("<r "); var8 > 0; var8 = var7.indexOf("<r ", var8)) {
            String var9 = this.decode(this.parseAttribute(var7, "from", var8));

            for(int var10 = 0; var10 < var1.length; ++var10) {
               if (this.normalize(var1[var10]).equals(var9)) {
                  var3[var10] = this.parseAttribute(var7, "to", var8);
               }
            }

            ++var8;
         }
      }

      return var3;
   }

   public Wiki.Revision[] getPageHistory(String var1) throws IOException {
      return this.getPageHistory(var1, (Calendar)null, (Calendar)null, false);
   }

   public Wiki.Revision[] getPageHistory(String var1, Calendar var2, Calendar var3, boolean var4) throws IOException {
      StringBuilder var5;
      (var5 = new StringBuilder(this.query)).append("prop=revisions&rvlimit=max&titles=");
      var5.append(URLEncoder.encode(this.normalize(var1), "UTF-8"));
      var5.append("&rvprop=timestamp%7Cuser%7Cids%7Cflags%7Csize%7Ccomment");
      if (var4) {
         var5.append("&rvdir=newer");
      }

      if (var2 != null) {
         var5.append(var4 ? "&rvstart=" : "&rvend=");
         var5.append(this.calendarToTimestamp(var2));
      }

      if (var3 != null) {
         var5.append(var4 ? "&rvend=" : "&rvstart=");
         var5.append(this.calendarToTimestamp(var3));
      }

      String var8 = null;
      ArrayList var9 = new ArrayList(1500);

      int var7;
      do {
         String var10;
         if (var8 == null) {
            var10 = this.fetch(var5.toString(), "getPageHistory");
         } else {
            var10 = this.fetch(var5.toString() + "&rvcontinue=" + var8, "getPageHistory");
         }

         var8 = this.parseAttribute(var10, "rvcontinue", 0);

         for(int var6 = var10.indexOf("<rev "); var6 > 0; var6 = var10.indexOf("<rev ", var6)) {
            var7 = var10.indexOf("/>", var6);
            var9.add(this.parseRevision(var10.substring(var6, var7), var1));
            ++var6;
         }
      } while(var8 != null);

      int var11 = var9.size();
      Wiki.Revision[] var12 = (Wiki.Revision[])var9.toArray(new Wiki.Revision[var11]);

      for(var7 = 0; var7 < var11; ++var7) {
         if (var7 != 0) {
            var12[var7].next = var12[var7 - 1].revid;
         }

         if (var7 != var11 - 1) {
            var12[var7].sizediff = var12[var7].size - var12[var7 + 1].size;
         } else {
            var12[var7].sizediff = var12[var7].size;
         }
      }

      this.log(Level.INFO, "getPageHistory", "Successfully retrieved page history of " + var1 + " (" + var11 + " revisions)");
      return var12;
   }

   public Wiki.Revision[] getDeletedHistory(String var1) throws IOException, CredentialNotFoundException {
      return this.deletedRevs("", var1, (Calendar)null, (Calendar)null, false, 167317762);
   }

   public Wiki.Revision[] getDeletedHistory(String var1, Calendar var2, Calendar var3, boolean var4) throws IOException, CredentialNotFoundException {
      return this.deletedRevs("", var1, var2, var3, var4, 167317762);
   }

   public Wiki.Revision[] deletedContribs(String var1) throws IOException, CredentialNotFoundException {
      return this.deletedRevs(var1, "", (Calendar)null, (Calendar)null, false, 167317762);
   }

   public Wiki.Revision[] deletedContribs(String var1, Calendar var2, Calendar var3, boolean var4, int var5) throws IOException, CredentialNotFoundException {
      return this.deletedRevs(var1, "", var2, var3, false, var5);
   }

   protected Wiki.Revision[] deletedRevs(String var1, String var2, Calendar var3, Calendar var4, boolean var5, int var6) throws IOException, CredentialNotFoundException {
      if (!this.user.isAllowedTo("deletedhistory")) {
         throw new CredentialNotFoundException("Permission denied: not able to view deleted history");
      } else {
         StringBuilder var7;
         (var7 = new StringBuilder(this.query)).append("list=deletedrevs&drprop=revid%7Cparentid%7Clen%7Cminor%7Ccomment%7Cuser&drlimit=max");
         if (var5) {
            var7.append("&drdir=newer");
         }

         if (var3 != null) {
            var7.append(var5 ? "&drstart=" : "&drend=");
            var7.append(this.calendarToTimestamp(var3));
         }

         if (var4 != null) {
            var7.append(var5 ? "&drend=" : "&drstart=");
            var7.append(this.calendarToTimestamp(var4));
         }

         if (var2.isEmpty()) {
            var7.append("&druser=");
            var7.append(URLEncoder.encode(var1, "UTF-8"));
            if (var6 != 167317762) {
               var7.append("&drnamespace=");
               var7.append(var6);
            }
         } else {
            var7.append("&titles=");
            var7.append(URLEncoder.encode(var2, "UTF-8"));
         }

         var1 = null;
         var2 = null;
         ArrayList var11 = new ArrayList(500);

         do {
            String var12;
            if (var1 != null) {
               var12 = this.fetch(var7.toString() + "&drcontinue=" + URLEncoder.encode(var1, "UTF-8"), "deletedRevs");
            } else if (var2 != null) {
               var12 = this.fetch(var7.toString() + "&drstart=" + var2, "deletedRevs");
            } else {
               var12 = this.fetch(var7.toString(), "deletedRevs");
            }

            var1 = this.parseAttribute(var12, "drcontinue", 0);
            var2 = this.parseAttribute(var12, "drstart", 0);
            int var13;
            if ((var13 = var12.indexOf("<deletedrevs>")) < 0) {
               break;
            }

            for(var13 = var12.indexOf("<page ", var13); var13 > 0; var13 = var12.indexOf("<page ", var13)) {
               String var15 = this.parseAttribute(var12, "title", var13);
               int var8 = var12.indexOf("</page>", var13);

               for(int var9 = var12.indexOf("<rev ", var13); var9 < var8 && var9 >= 0; var9 = var12.indexOf("<rev ", var9)) {
                  int var10 = var12.indexOf(" />", var9);
                  var11.add(this.parseRevision(var12.substring(var9, var10), var15));
                  ++var9;
               }

               ++var13;
            }
         } while(var1 != null || var2 != null);

         int var14 = var11.size();
         this.log(Level.INFO, "Successfully fetched " + var14 + " deleted revisions.", "deletedRevs");
         return (Wiki.Revision[])var11.toArray(new Wiki.Revision[var14]);
      }
   }

   public String[] deletedPrefixIndex(String var1, int var2) throws IOException, CredentialNotFoundException {
      if (this.user.isAllowedTo("deletedhistory") && this.user.isAllowedTo("deletedtext")) {
         StringBuilder var3;
         (var3 = new StringBuilder(this.query)).append("list=deletedrevs&drlimit=max&drunique=1&drdir=newer&drprefix=");
         var3.append(URLEncoder.encode(var1, "UTF-8"));
         var3.append("&drnamespace=");
         var3.append(var2);
         var1 = null;
         ArrayList var6 = new ArrayList();

         do {
            String var4;
            if (var1 == null) {
               var4 = this.fetch(var3.toString(), "deletedPrefixIndex");
            } else {
               var4 = this.fetch(var3.toString() + "&drcontinue=" + URLEncoder.encode(var1, "UTF-8"), "deletedPrefixIndex");
            }

            var1 = this.parseAttribute(var4, var1, 0);

            for(int var5 = var4.indexOf("<page ", 0); var5 > 0; var5 = var4.indexOf("<page ", var5)) {
               var6.add(this.parseAttribute(var4, "title", var5));
               ++var5;
            }
         } while(var1 != null);

         int var7 = var6.size();
         this.log(Level.INFO, "deletedPrefixIndex", "Successfully retrieved deleted page list (" + var7 + " items).");
         return (String[])var6.toArray(new String[var7]);
      } else {
         throw new CredentialNotFoundException("Permission denied: not able to view deleted history or text.");
      }
   }

   public String getDeletedText(String var1) throws IOException, CredentialNotFoundException {
      if (this.user.isAllowedTo("deletedhistory") && this.user.isAllowedTo("deletedtext")) {
         StringBuilder var2;
         (var2 = new StringBuilder(this.query)).append("list=deletedrevs&drlimit=1&drprop=content&titles=");
         var2.append(URLEncoder.encode(var1, "UTF-8"));
         int var4 = (var1 = this.fetch(var2.toString(), "getDeletedText")).indexOf("<rev ");
         var4 = var1.indexOf(">", var4) + 1;
         int var3 = var1.indexOf("</rev>", var4);
         return var1.substring(var4, var3);
      } else {
         throw new CredentialNotFoundException("Permission denied: not able to view deleted history or text.");
      }
   }

   public void move(String var1, String var2, String var3) throws IOException, LoginException {
      this.move(var1, var2, var3, false, true, false);
   }

   public synchronized void move(String var1, String var2, String var3, boolean var4, boolean var5, boolean var6) throws IOException, LoginException {
      long var7 = System.currentTimeMillis();
      if (this.user != null && this.user.isAllowedTo("move")) {
         if (this.namespace(var1) == 14) {
            throw new UnsupportedOperationException("Tried to move a category.");
         } else {
            HashMap var12;
            if (!(Boolean)(var12 = this.getPageInfo(var1)).get("exists")) {
               throw new IllegalArgumentException("Tried to move a non-existant page!");
            } else if (!this.checkRights(var12, "move")) {
               CredentialException var14 = new CredentialException("Permission denied: page is protected.");
               this.log(Level.WARNING, "move", "Cannot move - permission denied. " + var14);
               throw var14;
            } else {
               String var13 = (String)var12.get("token");
               StringBuilder var10;
               (var10 = new StringBuilder(10000)).append("from=");
               var10.append(URLEncoder.encode(var1, "UTF-8"));
               var10.append("&to=");
               var10.append(URLEncoder.encode(var2, "UTF-8"));
               var10.append("&reason=");
               var10.append(URLEncoder.encode(var3, "UTF-8"));
               var10.append("&token=");
               var10.append(URLEncoder.encode(var13, "UTF-8"));
               if (var5) {
                  var10.append("&movetalk=1");
               }

               if (var4 && this.user.isAllowedTo("suppressredirect")) {
                  var10.append("&noredirect=1");
               }

               if (var6 && this.user.isAllowedTo("move-subpages")) {
                  var10.append("&movesubpages=1");
               }

               var13 = this.post(this.apiUrl + "action=move", var10.toString(), "move");

               try {
                  if (!var13.contains("move from")) {
                     this.checkErrorsAndUpdateStatus(var13, "move");
                  }
               } catch (IOException var11) {
                  if (!this.retry) {
                     this.log(Level.SEVERE, "move", "EXCEPTION: " + var11);
                     throw var11;
                  }

                  this.retry = false;
                  this.log(Level.WARNING, "move", "Exception: " + var11.getMessage() + " Retrying...");
                  this.move(var1, var2, var3, var4, var5, var6);
               }

               if (this.retry) {
                  this.log(Level.INFO, "move", "Successfully moved " + var1 + " to " + var2);
               }

               this.retry = true;
               this.throttle(var7);
            }
         }
      } else {
         CredentialNotFoundException var9 = new CredentialNotFoundException("Permission denied: cannot move pages.");
         this.log(Level.SEVERE, "move", "Cannot move - permission denied: " + var9);
         throw var9;
      }
   }

   public synchronized void protect(String var1, HashMap var2, String var3) throws IOException, LoginException {
      if (this.user != null && this.user.isAllowedTo("protect")) {
         long var4 = System.currentTimeMillis();
         String var6 = (String)this.getPageInfo(var1).get("token");
         StringBuilder var7;
         (var7 = new StringBuilder("title=")).append(URLEncoder.encode(var1, "UTF-8"));
         var7.append("&reason=");
         var7.append(URLEncoder.encode(var3, "UTF-8"));
         var7.append("&token=");
         var7.append(URLEncoder.encode(var6, "UTF-8"));
         if (var2.containsKey("cascade")) {
            var7.append("&cascade=1");
         }

         var7.append("&protections=");
         StringBuilder var12 = new StringBuilder();
         Iterator var8 = var2.entrySet().iterator();

         while(var8.hasNext()) {
            Entry var9;
            String var10;
            if (!(var10 = (String)(var9 = (Entry)var8.next()).getKey()).contains("expiry") && !var10.equals("cascade")) {
               var7.append(var10);
               var7.append("=");
               var7.append(var9.getValue());
               Calendar var14 = (Calendar)var2.get(var10 + "expiry");
               var12.append(var14 == null ? "never" : this.calendarToTimestamp(var14));
               var7.append("%7C");
               var12.append("%7C");
            }
         }

         var7.delete(var7.length() - 3, var7.length());
         var12.delete(var12.length() - 3, var12.length());
         var7.append("&expiry=");
         var7.append(var12);
         System.out.println(var7);
         String var13 = this.post(this.apiUrl + "action=protect", var7.toString(), "protect");

         try {
            if (!var13.contains("<protect ")) {
               this.checkErrorsAndUpdateStatus(var13, "protect");
            }
         } catch (IOException var11) {
            if (!this.retry) {
               this.log(Level.SEVERE, "protect", "EXCEPTION: " + var11);
               throw var11;
            }

            this.retry = false;
            this.log(Level.WARNING, "protect", "Exception: " + var11.getMessage() + " Retrying...");
            this.protect(var1, var2, var3);
         }

         if (this.retry) {
            this.log(Level.INFO, "edit", "Successfully protected " + var1);
         }

         this.retry = true;
         this.throttle(var4);
      } else {
         throw new CredentialNotFoundException("Cannot protect: permission denied.");
      }
   }

   public void unprotect(String var1, String var2) throws IOException, LoginException {
      HashMap var3;
      (var3 = new HashMap()).put("edit", "all");
      var3.put("move", "all");
      if (this.namespace(var1) == 6) {
         var3.put("upload", "all");
      }

      var3.put("create", "all");
      this.protect(var1, var3, var2);
   }

   public String export(String var1) throws IOException {
      return this.fetch(this.query + "export&exportnowrap&titles=" + URLEncoder.encode(this.normalize(var1), "UTF-8"), "export");
   }

   public Wiki.Revision getRevision(long var1) throws IOException {
      return this.getRevisions(new long[]{var1})[0];
   }

   public Wiki.Revision[] getRevisions(long[] var1) throws IOException {
      StringBuilder var2;
      (var2 = new StringBuilder(this.query)).append("prop=revisions&rvprop=ids%7Ctimestamp%7Cuser%7Ccomment%7Cflags%7Csize&revids=");
      String[] var3 = new String[var1.length / this.slowmax + 1];
      StringBuilder var4 = new StringBuilder();

      for(int var5 = 0; var5 < var1.length; ++var5) {
         var4.append(var1[var5]);
         if (var5 != var1.length - 1 && var5 != this.slowmax - 1) {
            var4.append("%7C");
         } else {
            var3[var5 / this.slowmax] = var4.toString();
            var4 = new StringBuilder();
         }
      }

      Wiki.Revision[] var17 = new Wiki.Revision[var1.length];
      int var16 = (var3 = var3).length;

      for(int var6 = 0; var6 < var16; ++var6) {
         String var7 = var3[var6];

         for(int var8 = (var7 = this.fetch(var2.toString() + var7, "getRevision")).indexOf("<page "); var8 > 0; var8 = var7.indexOf("<page ", var8)) {
            int var9 = var7.indexOf("</page>", var8);
            String var10 = this.parseAttribute(var7, "title", var8);

            for(int var11 = var7.indexOf("<rev ", var8); var11 > 0 && var11 < var9; var11 = var7.indexOf("<rev ", var11)) {
               int var12 = var7.indexOf("/>", var11);
               String var18 = var7.substring(var11, var12);
               Wiki.Revision var19;
               long var14 = (var19 = this.parseRevision(var18, var10)).getRevid();

               for(int var13 = 0; var13 < var1.length; ++var13) {
                  if (var1[var13] == var14) {
                     var17[var13] = var19;
                  }
               }

               ++var11;
            }

            ++var8;
         }
      }

      return var17;
   }

   public void rollback(Wiki.Revision var1) throws IOException, LoginException {
      this.rollback(var1, this.markbot, "");
   }

   public synchronized void rollback(Wiki.Revision var1, boolean var2, String var3) throws IOException, LoginException {
      if (this.user != null && this.user.isAllowedTo("rollback")) {
         Wiki.Revision var4;
         if (!(var4 = this.getTopRevision(var1.getPage())).equals(var1)) {
            this.log(Level.INFO, "rollback", "Rollback failed: revision is not the most recent");
         } else {
            String var7 = URLEncoder.encode(var4.getRollbackToken(), "UTF-8");
            StringBuilder var5;
            (var5 = new StringBuilder(10000)).append("title=");
            var5.append(URLEncoder.encode(this.normalize(var1.getPage()), "UTF-8"));
            var5.append("&user=");
            var5.append(URLEncoder.encode(this.normalize(var1.getUser()), "UTF-8"));
            var5.append("&token=");
            var5.append(var7);
            if (var2 && this.user.isAllowedTo("markbotedits")) {
               var5.append("&markbot=1");
            }

            if (!var3.isEmpty()) {
               var5.append("&summary=");
               var5.append(var3);
            }

            var7 = this.post(this.apiUrl + "action=rollback", var5.toString(), "rollback");

            try {
               if (var7.contains("alreadyrolled")) {
                  this.log(Level.INFO, "rollback", "Edit has already been rolled back.");
               } else if (var7.contains("onlyauthor")) {
                  this.log(Level.INFO, "rollback", "Cannot rollback as the page only has one author.");
               } else if (!var7.contains("rollback title=")) {
                  this.checkErrorsAndUpdateStatus(var7, "rollback");
               }
            } catch (IOException var6) {
               if (!this.retry) {
                  this.log(Level.SEVERE, "rollback", "EXCEPTION: " + var6);
                  throw var6;
               }

               this.retry = false;
               this.log(Level.WARNING, "rollback", "Exception: " + var6.getMessage() + " Retrying...");
               this.rollback(var1, var2, var3);
            }

            if (this.retry) {
               this.log(Level.INFO, "rollback", "Successfully reverted edits by " + this.user + " on " + var1.getPage());
            }

            this.retry = true;
         }
      } else {
         throw new CredentialNotFoundException("Permission denied: cannot rollback.");
      }
   }

   public synchronized void revisionDelete(Boolean var1, Boolean var2, Boolean var3, String var4, Boolean var5, Wiki.Revision[] var6) throws IOException, LoginException {
      long var7 = System.currentTimeMillis();
      if (this.user != null && this.user.isAllowedTo("deleterevision") && this.user.isAllowedTo("deletelogentry")) {
         String var9 = (String)this.getPageInfo(var6[0].getPage()).get("token");
         StringBuilder var10;
         (var10 = new StringBuilder("reason=")).append(URLEncoder.encode(var4, "UTF-8"));
         var10.append("&type=revision");
         var10.append("&ids=");

         for(int var11 = 0; var11 < var6.length - 1; ++var11) {
            var10.append(var6[var11].getRevid());
            var10.append("%7C");
         }

         var10.append(var6[var6.length - 1].getRevid());
         var10.append("&token=");
         var10.append(URLEncoder.encode(var9, "UTF-8"));
         if (this.user.isAllowedTo("suppressrevision") && var5 != null) {
            if (var5) {
               var10.append("&suppress=yes");
            } else {
               var10.append("&suppress=no");
            }
         }

         var10.append("&hide=");
         StringBuilder var17 = new StringBuilder("&show=");
         if (var1 == Boolean.TRUE) {
            var10.append("content%7C");
         } else if (var1 == Boolean.FALSE) {
            var17.append("content%7C");
         }

         if (var2 == Boolean.TRUE) {
            var10.append("user%7C");
         } else if (var2 == Boolean.FALSE) {
            var17.append("user%7C");
         }

         if (var3 == Boolean.TRUE) {
            var10.append("comment");
         } else if (var3 == Boolean.FALSE) {
            var17.append("comment");
         }

         if (var10.lastIndexOf("%7C") == var10.length() - 2) {
            var10.delete(var10.length() - 2, var10.length());
         }

         if (var17.lastIndexOf("%7C") == var17.length() - 2) {
            var17.delete(var17.length() - 2, var17.length());
         }

         var10.append(var17);
         var9 = this.post(this.apiUrl + "action=revisiondelete", var10.toString(), "revisionDelete");

         try {
            if (!var9.contains("<revisiondelete ")) {
               this.checkErrorsAndUpdateStatus(var9, "move");
            }
         } catch (IOException var12) {
            if (!this.retry) {
               this.log(Level.SEVERE, "revisionDelete", "EXCEPTION: " + var12);
               throw var12;
            }

            this.retry = false;
            this.log(Level.WARNING, "revisionDelete", "Exception: " + var12.getMessage() + " Retrying...");
            this.revisionDelete(var1, var2, var3, var4, var5, var6);
         }

         if (this.retry) {
            this.log(Level.INFO, "revisionDelete", "Successfully (un)deleted " + var6.length + " revisions.");
         }

         this.retry = true;
         Wiki.Revision[] var16 = var6;
         int var13 = var6.length;

         for(int var14 = 0; var14 < var13; ++var14) {
            Wiki.Revision var15 = var16[var14];
            if (var2 != null) {
               var15.userDeleted = var2;
            }

            if (var3 != null) {
               var15.summaryDeleted = var3;
            }
         }

         this.throttle(var7);
      } else {
         throw new CredentialNotFoundException("Permission denied: cannot revision delete.");
      }
   }

   public synchronized void undo(Wiki.Revision var1, Wiki.Revision var2, String var3, boolean var4, boolean var5) throws IOException, LoginException {
      long var6 = System.currentTimeMillis();
      if (var2 != null && !var1.getPage().equals(var2.getPage())) {
         throw new IllegalArgumentException("Cannot undo - the revisions supplied are not on the same page!");
      } else {
         HashMap var8 = this.getPageInfo(var1.getPage());
         if (!this.checkRights(var8, "edit")) {
            CredentialException var12 = new CredentialException("Permission denied: page is protected.");
            this.log(Level.WARNING, "undo", "Cannot edit - permission denied." + var12);
            throw var12;
         } else {
            String var11 = (String)var8.get("token");
            StringBuilder var9;
            (var9 = new StringBuilder(10000)).append("title=");
            var9.append(var1.getPage());
            if (!var3.isEmpty()) {
               var9.append("&summary=");
               var9.append(var3);
            }

            var9.append("&undo=");
            var9.append(var1.getRevid());
            if (var2 != null) {
               var9.append("&undoafter=");
               var9.append(var2.getRevid());
            }

            if (var4) {
               var9.append("&minor=1");
            }

            if (var5) {
               var9.append("&bot=1");
            }

            var9.append("&token=");
            var9.append(URLEncoder.encode(var11, "UTF-8"));
            var11 = this.post(this.apiUrl + "action=edit", var9.toString(), "undo");

            try {
               this.checkErrorsAndUpdateStatus(var11, "undo");
            } catch (IOException var10) {
               if (!this.retry) {
                  this.log(Level.SEVERE, "undo", "EXCEPTION: " + var10);
                  throw var10;
               }

               this.retry = false;
               this.log(Level.WARNING, "undo", "Exception: " + var10.getMessage() + " Retrying...");
               this.undo(var1, var2, var3, var4, var5);
            }

            if (this.retry) {
               var11 = "Successfully undid revision(s) " + var1.getRevid();
               if (var2 != null) {
                  var11 = var11 + " - " + var2.getRevid();
               }

               this.log(Level.INFO, "undo", var11);
            }

            this.retry = true;
            this.throttle(var6);
         }
      }
   }

   protected Wiki.Revision parseRevision(String var1, String var2) {
      long var3 = Long.parseLong(this.parseAttribute(var1, " revid", 0));
      Calendar var5 = this.timestampToCalendar(this.parseAttribute(var1, "timestamp", 0), true);
      if (var2.isEmpty()) {
         var2 = this.decode(this.parseAttribute(var1, "title", 0));
      }

      String var6 = null;
      if (var1.contains("comment=\"")) {
         var6 = this.decode(this.parseAttribute(var1, "comment", 0));
      }

      String var7 = null;
      if (var1.contains("user=\"")) {
         var7 = this.decode(this.parseAttribute(var1, "user", 0));
      }

      boolean var8 = var1.contains("minor=\"\"");
      boolean var9 = var1.contains("bot=\"\"");
      boolean var10 = var1.contains("new=\"\"");
      int var11 = 0;
      if (var1.contains("newlen=")) {
         var11 = Integer.parseInt(this.parseAttribute(var1, "newlen", 0));
      } else if (var1.contains("size=\"")) {
         var11 = Integer.parseInt(this.parseAttribute(var1, "size", 0));
      } else if (var1.contains("len=\"")) {
         var11 = Integer.parseInt(this.parseAttribute(var1, "len", 0));
      }

      Wiki.Revision var12 = new Wiki.Revision(var3, var5, var2, var6, var7, var8, var9, var10, var11);
      if (var1.contains("rcid=\"")) {
         var12.setRcid(Long.parseLong(this.parseAttribute(var1, "rcid", 0)));
      }

      if (var1.contains("rollbacktoken=\"")) {
         var12.setRollbackToken(this.parseAttribute(var1, "rollbacktoken", 0));
      }

      if (var1.contains("parentid")) {
         var12.previous = Long.parseLong(this.parseAttribute(var1, "parentid", 0));
      } else if (var1.contains("old_revid")) {
         var12.previous = Long.parseLong(this.parseAttribute(var1, "old_revid", 0));
      }

      if (var1.contains("oldlen=\"")) {
         var12.sizediff = var12.size - Integer.parseInt(this.parseAttribute(var1, "oldlen", 0));
      } else if (var1.contains("sizediff=\"")) {
         var12.sizediff = Integer.parseInt(this.parseAttribute(var1, "sizediff", 0));
      }

      var12.summaryDeleted = var1.contains("commenthidden=\"");
      var12.userDeleted = var1.contains("userhidden=\"");
      return var12;
   }

   public byte[] getImage(String var1) throws IOException {
      return this.getImage(var1, -1, -1);
   }

   public byte[] getImage(String var1, int var2, int var3) throws IOException {
      var1 = var1.replaceFirst("^(File|Image|" + this.namespaceIdentifier(6) + "):", "");
      StringBuilder var4;
      (var4 = new StringBuilder(this.query)).append("prop=imageinfo&iiprop=url&titles=");
      var4.append(URLEncoder.encode(this.normalize("File:" + var1), "UTF-8"));
      var4.append("&iiurlwidth=");
      var4.append(var2);
      var4.append("&iiurlheight=");
      var4.append(var3);
      String var5;
      if (!(var5 = this.fetch(var4.toString(), "getImage")).contains("<imageinfo>")) {
         return null;
      } else {
         var5 = this.parseAttribute(var5, "url", 0);
         this.logurl(var5, "getImage");
         URLConnection var6 = (new URL(var5)).openConnection();
         this.setCookies(var6);
         var6.connect();
         BufferedInputStream var7 = new BufferedInputStream(var6.getInputStream());
         ByteArrayOutputStream var8 = new ByteArrayOutputStream();

         int var9;
         while((var9 = var7.read()) != -1) {
            var8.write(var9);
         }

         this.log(Level.INFO, "getImage", "Successfully retrieved image \"" + var1 + "\"");
         return var8.toByteArray();
      }
   }

   public HashMap getFileMetadata(String var1) throws IOException {
      var1 = var1.replaceFirst("^(File|Image|" + this.namespaceIdentifier(6) + "):", "");
      var1 = this.query + "prop=imageinfo&iiprop=size%7Cmime%7Cmetadata&titles=" + URLEncoder.encode(this.normalize("File:" + var1), "UTF-8");
      if ((var1 = this.fetch(var1, "getFileMetadata")).contains("missing=\"\"")) {
         return null;
      } else {
         HashMap var2;
         (var2 = new HashMap(30)).put("size", new Integer(this.parseAttribute(var1, "size", 0)));
         var2.put("width", new Integer(this.parseAttribute(var1, "width", 0)));
         var2.put("height", new Integer(this.parseAttribute(var1, "height", 0)));
         var2.put("mime", this.parseAttribute(var1, "mime", 0));

         while(var1.contains("metadata name=\"")) {
            int var3 = var1.indexOf("name=\"") + 6;
            var3 = var1.indexOf(34, var3);
            String var4 = this.parseAttribute(var1, "name", 0);
            String var5 = this.parseAttribute(var1, "value", 0);
            var2.put(var4, var5);
            var1 = var1.substring(var3);
         }

         return var2;
      }
   }

   public String[] getDuplicates(String var1) throws IOException {
      var1 = var1.replaceFirst("^(File|Image|" + this.namespaceIdentifier(6) + "):", "");
      String var2 = this.query + "prop=duplicatefiles&dflimit=max&titles=" + URLEncoder.encode(this.normalize("File:" + var1), "UTF-8");
      if ((var2 = this.fetch(var2, "getDuplicates")).contains("missing=\"\"")) {
         return new String[0];
      } else {
         ArrayList var3 = new ArrayList(10);

         int var4;
         for(var4 = var2.indexOf("<df "); var4 > 0; var4 = var2.indexOf("<df ", var4)) {
            var3.add("File:" + this.decode(this.parseAttribute(var2, "name", var4)));
            ++var4;
         }

         var4 = var3.size();
         this.log(Level.INFO, "getDuplicates", "Successfully retrieved duplicates of File:" + var1 + " (" + var4 + " files)");
         return (String[])var3.toArray(new String[var4]);
      }
   }

   public Wiki.LogEntry[] getImageHistory(String var1) throws IOException {
      var1 = var1.replaceFirst("^(File|Image|" + this.namespaceIdentifier(6) + "):", "");
      String var2 = this.query + "prop=imageinfo&iiprop=timestamp%7Cuser%7Ccomment&iilimit=max&titles=" + URLEncoder.encode(this.normalize("File:" + var1), "UTF-8");
      if ((var2 = this.fetch(var2, "getImageHistory")).contains("missing=\"\"")) {
         return new Wiki.LogEntry[0];
      } else {
         ArrayList var3 = new ArrayList(40);
         var1 = this.namespaceIdentifier(6) + ":" + var1;

         int var4;
         Wiki.LogEntry var6;
         for(var4 = var2.indexOf("<ii "); var4 > 0; var4 = var2.indexOf("<ii ", var4)) {
            int var5 = var2.indexOf(62, var4);
            (var6 = this.parseLogEntry(var2.substring(var4, var5))).target = var1;
            var6.type = "upload";
            var6.action = "overwrite";
            var3.add(var6);
            ++var4;
         }

         var4 = var3.size();
         (var6 = (Wiki.LogEntry)var3.get(var4 - 1)).action = "upload";
         var3.set(var4 - 1, var6);
         return (Wiki.LogEntry[])var3.toArray(new Wiki.LogEntry[var4]);
      }
   }

   public byte[] getOldImage(Wiki.LogEntry var1) throws IOException {
      if (!var1.getType().equals("upload")) {
         throw new IllegalArgumentException("You must provide an upload log entry!");
      } else {
         String var2 = var1.getTarget();
         String var3 = this.query + "prop=imageinfo&iilimit=max&iiprop=timestamp%7Curl%7Carchivename&titles=" + URLEncoder.encode(var2, "UTF-8");

         String var4;
         for(int var9 = (var4 = this.fetch(var3, "getOldImage")).indexOf("<ii "); var9 > 0; var9 = var4.indexOf("<ii ", var9)) {
            if (this.convertTimestamp(this.parseAttribute(var4, "timestamp", var9)).equals(this.calendarToTimestamp(var1.getTimestamp()))) {
               var3 = this.parseAttribute(var4, "url", var9);
               this.logurl(var3, "getOldImage");
               URLConnection var6 = (new URL(var3)).openConnection();
               this.setCookies(var6);
               var6.connect();
               BufferedInputStream var7 = new BufferedInputStream(var6.getInputStream());
               ByteArrayOutputStream var10 = new ByteArrayOutputStream();

               int var5;
               while((var5 = var7.read()) != -1) {
                  var10.write(var5);
               }

               String var8;
               if ((var8 = this.parseAttribute(var4, "archivename", 0)) == null) {
                  var8 = var2;
               }

               this.log(Level.INFO, "getOldImage", "Successfully retrieved old image \"" + var8 + "\"");
               return var10.toByteArray();
            }

            ++var9;
         }

         return null;
      }
   }

   public Wiki.LogEntry[] getUploads(Wiki.User var1) throws IOException {
      return this.getUploads(var1, (Calendar)null, (Calendar)null);
   }

   public Wiki.LogEntry[] getUploads(Wiki.User var1, Calendar var2, Calendar var3) throws IOException {
      StringBuilder var4;
      (var4 = new StringBuilder(this.query)).append("list=allimages&ailimit=max&aisort=timestamp&aiprop=timestamp%7Ccomment&aiuser=");
      var4.append(URLEncoder.encode(var1.getUsername(), "UTF-8"));
      if (var2 != null) {
         var4.append("&aistart=");
         var4.append(this.calendarToTimestamp(var2));
      }

      if (var3 != null) {
         var4.append("&aiend=");
         var4.append(this.calendarToTimestamp(var3));
      }

      ArrayList var8 = new ArrayList();
      String var9 = null;

      do {
         String var5;
         if (var9 == null) {
            var5 = this.fetch(var4.toString(), "getUploads");
         } else {
            var5 = this.fetch(var4.toString() + "&aicontinue=" + var9, "getUploads");
         }

         var9 = this.parseAttribute(var5, "aicontinue", 0);

         for(int var6 = var5.indexOf("<img "); var6 > 0; var6 = var5.indexOf("<img ", var6)) {
            int var7 = var5.indexOf("/>", var6);
            Wiki.LogEntry var11;
            (var11 = this.parseLogEntry(var5.substring(var6, var7))).type = "upload";
            var11.action = "upload";
            var11.user = var1;
            var8.add(var11);
            ++var6;
         }
      } while(var9 != null);

      int var10 = var8.size();
      this.log(Level.INFO, "getUploads", "Successfully retrieved uploads of " + var1.getUsername() + " (" + var10 + " uploads)");
      return (Wiki.LogEntry[])var8.toArray(new Wiki.LogEntry[var10]);
   }

   public synchronized void upload(File var1, String var2, String var3, String var4) throws IOException, LoginException {
      long var5 = System.currentTimeMillis();
      if (this.user != null && this.user.isAllowedTo("upload")) {
         var2 = var2.replaceFirst("^(File|Image|" + this.namespaceIdentifier(6) + "):", "");
         HashMap var19 = this.getPageInfo("File:" + var2);
         if (!this.checkRights(var19, "upload")) {
            CredentialException var22 = new CredentialException("Permission denied: page is protected.");
            this.log(Level.WARNING, "upload", "Cannot upload - permission denied." + var22);
            throw var22;
         } else {
            String var20 = (String)var19.get("token");
            long var9;
            long var11 = ((var9 = var1.length()) >> 22) + 1L;
            FileInputStream var8 = new FileInputStream(var1);
            String var13 = "";

            for(int var14 = 0; (long)var14 < var11; ++var14) {
               HashMap var15;
               (var15 = new HashMap(50)).put("filename", var2);
               var15.put("token", var20);
               var15.put("ignorewarnings", "true");
               if (var11 == 1L) {
                  var15.put("text", var3);
                  if (!var4.isEmpty()) {
                     var15.put("comment", var4);
                  }

                  byte[] var16 = new byte[var8.available()];
                  var8.read(var16);
                  var15.put("file\"; filename=\"" + var1.getName(), var16);
               } else {
                  long var25 = (long)(var14 << 22);
                  var15.put("stash", "1");
                  var15.put("offset", String.valueOf(var25));
                  var15.put("filesize", String.valueOf(var9));
                  if (var14 != 0) {
                     var15.put("filekey", var13);
                  }

                  byte[] var21 = new byte[(int)Math.min(4194304L, var9 - var25)];
                  var8.read(var21);
                  var15.put("chunk\"; filename=\"" + var1.getName(), var21);
                  var20 = (String)this.getPageInfo("File:" + var2).get("token");
               }

               String var26 = this.multipartPost(this.apiUrl + "action=upload", var15, "upload");

               try {
                  if (var11 > 1L) {
                     if (!var26.contains("filekey=\"")) {
                        throw new IOException("No filekey present! Server response was " + var26);
                     }

                     var13 = this.parseAttribute(var26, "filekey", 0);
                  } else {
                     if (var26.contains("error code=\"fileexists-shared-forbidden\"")) {
                        CredentialException var17 = new CredentialException("Cannot overwrite file hosted on central repository.");
                        this.log(Level.WARNING, "upload", "Cannot upload - permission denied." + var17);
                        throw var17;
                     }

                     this.checkErrorsAndUpdateStatus(var26, "upload");
                  }
               } catch (IOException var18) {
                  var8.close();
                  this.log(Level.SEVERE, "upload", "EXCEPTION: " + var18);
                  throw var18;
               }
            }

            var8.close();
            if (var11 > 1L) {
               HashMap var23;
               (var23 = new HashMap(50)).put("filename", var2);
               var23.put("token", var20);
               var23.put("text", var3);
               if (!var4.isEmpty()) {
                  var23.put("comment", var4);
               }

               var23.put("ignorewarnings", "true");
               var23.put("filekey", var13);
               String var24 = this.multipartPost(this.apiUrl + "action=upload", var23, "upload");
               this.checkErrorsAndUpdateStatus(var24, "upload");
            }

            this.throttle(var5);
            this.log(Level.INFO, "upload", "Successfully uploaded to File:" + var2 + ".");
         }
      } else {
         CredentialNotFoundException var7 = new CredentialNotFoundException("Permission denied: cannot upload files.");
         this.log(Level.SEVERE, "upload", "Cannot upload - permission denied." + var7);
         throw var7;
      }
   }

   public boolean userExists(String var1) throws IOException {
      var1 = URLEncoder.encode(this.normalize(var1), "UTF-8");
      return this.fetch(this.query + "list=users&ususers=" + var1, "userExists").contains("userid=\"");
   }

   public String[] allUsers(String var1, int var2) throws IOException {
      return this.allUsers(var1, var2, "");
   }

   public String[] allUsersWithPrefix(String var1) throws IOException {
      return this.allUsers("", -1, var1);
   }

   public String[] allUsers(String var1, int var2, String var3) throws IOException {
      StringBuilder var4;
      (var4 = new StringBuilder(this.query)).append("list=allusers&aulimit=");
      String var5 = "";
      if (var3.isEmpty()) {
         var4.append(var2 > this.slowmax ? this.slowmax : var2);
         var5 = URLEncoder.encode(var1, "UTF-8");
      } else {
         var4.append(this.slowmax);
         var4.append("&auprefix=");
         var4.append(URLEncoder.encode(this.normalize(var3), "UTF-8"));
      }

      ArrayList var7 = new ArrayList(6667);

      do {
         var3 = var4.toString();
         if (!var5.isEmpty()) {
            var3 = var3 + "&aufrom=" + URLEncoder.encode(var5, "UTF-8");
         }

         var3 = this.fetch(var3, "allUsers");
         var5 = this.parseAttribute(var3, "aufrom", 0);

         for(int var6 = var3.indexOf("<u "); var6 > 0; var6 = var3.indexOf("<u ", var6)) {
            var7.add(this.decode(this.parseAttribute(var3, "name", var6)));
            if (var7.size() == var2) {
               var5 = null;
               break;
            }

            ++var6;
         }
      } while(var5 != null);

      int var8 = var7.size();
      this.log(Level.INFO, "allUsers", "Successfully retrieved user list (" + var8 + " users)");
      return (String[])var7.toArray(new String[var8]);
   }

   public Wiki.User getUser(String var1) throws IOException {
      return this.userExists(var1) ? new Wiki.User(this.normalize(var1)) : null;
   }

   public Wiki.User getCurrentUser() {
      return this.user;
   }

   public Wiki.Revision[] contribs(String var1, int... var2) throws IOException {
      return this.contribs(var1, "", (Calendar)null, (Calendar)null, var2);
   }

   @Deprecated
   public Wiki.Revision[] rangeContribs(String var1) throws IOException {
      int var2;
      if ((var2 = var1.indexOf(47)) < 7) {
         throw new NumberFormatException("Not a valid CIDR range!");
      } else {
         int var3 = Integer.parseInt(var1.substring(var2 + 1));
         String[] var4;
         if ((var4 = var1.substring(0, var2).split("\\.")).length != 4) {
            throw new NumberFormatException("Not a valid CIDR range!");
         } else {
            switch(var3) {
            case 8:
               return this.contribs("", var4[0] + ".", (Calendar)null, (Calendar)null);
            case 16:
               return this.contribs("", var4[0] + "." + var4[1] + ".", (Calendar)null, (Calendar)null);
            case 24:
               return this.contribs("", var4[0] + "." + var4[1] + "." + var4[2] + ".", (Calendar)null, (Calendar)null);
            case 32:
               return this.contribs(var1.substring(0, var1.length() - 3), "", (Calendar)null, (Calendar)null);
            default:
               throw new NumberFormatException("Range is not supported.");
            }
         }
      }
   }

   public Wiki.Revision[] contribs(String var1, String var2, Calendar var3, Calendar var4, int... var5) throws IOException {
      StringBuilder var6;
      (var6 = new StringBuilder(this.query)).append("list=usercontribs&uclimit=max&ucprop=title%7Ctimestamp%7Cflags%7Ccomment%7Cids%7Csize%7Csizediff&");
      if (var2.isEmpty()) {
         var6.append("ucuser=");
         var6.append(URLEncoder.encode(this.normalize(var1), "UTF-8"));
      } else {
         var6.append("ucuserprefix=");
         var6.append(var2);
      }

      this.constructNamespaceString(var6, "uc", var5);
      if (var3 != null) {
         var6.append("&ucend=");
         var6.append(this.calendarToTimestamp(var3));
      }

      ArrayList var10 = new ArrayList(7500);
      String var13 = "";
      String var7 = "";
      if (var4 != null) {
         var6.append("&ucstart=");
         var6.append(this.calendarToTimestamp(var4));
      }

      do {
         String var11;
         if ((var11 = this.fetch(var6.toString() + var13 + var7, "contribs")).contains("uccontinue")) {
            var13 = "&uccontinue=" + URLEncoder.encode(this.parseAttribute(var11, "uccontinue", 0), "UTF-8");
         } else {
            var13 = null;
         }

         for(int var8 = var11.indexOf("<item "); var8 > 0; var8 = var11.indexOf("<item ", var8)) {
            int var9 = var11.indexOf(" />", var8);
            var10.add(this.parseRevision(var11.substring(var8, var9), ""));
            ++var8;
         }
      } while(var13 != null);

      int var12 = var10.size();
      this.log(Level.INFO, "contribs", "Successfully retrived contributions for " + (var2.isEmpty() ? var1 : var2) + " (" + var12 + " edits)");
      return (Wiki.Revision[])var10.toArray(new Wiki.Revision[var12]);
   }

   public synchronized void emailUser(Wiki.User var1, String var2, String var3, boolean var4) throws IOException, LoginException {
      long var5 = System.currentTimeMillis();
      if (this.user != null && this.user.isAllowedTo("sendemail")) {
         if (!(Boolean)var1.getUserInfo().get("emailable")) {
            this.log(Level.WARNING, "emailUser", "User " + var1.getUsername() + " is not emailable");
         } else {
            String var7;
            if ((var7 = (String)this.getPageInfo("User:" + var1.getUsername()).get("token")).equals("\\+")) {
               this.log(Level.SEVERE, "emailUser", "Cookies have expired.");
               this.logout();
               throw new CredentialExpiredException("Cookies have expired.");
            } else {
               StringBuilder var8;
               (var8 = new StringBuilder(20000)).append("token=");
               var8.append(URLEncoder.encode(var7, "UTF-8"));
               var8.append("&target=");
               var8.append(URLEncoder.encode(var1.getUsername(), "UTF-8"));
               if (var4) {
                  var8.append("&ccme=true");
               }

               var8.append("&text=");
               var8.append(URLEncoder.encode(var2, "UTF-8"));
               var8.append("&subject=");
               var8.append(URLEncoder.encode(var3, "UTF-8"));
               var2 = this.post(this.apiUrl + "action=emailuser", var8.toString(), "emailUser");
               this.checkErrorsAndUpdateStatus(var2, "email");
               if (var2.contains("error code=\"cantsend\"")) {
                  throw new UnsupportedOperationException("Email is disabled for this wiki or you do not have a confirmed email address.");
               } else {
                  this.throttle(var5);
                  this.log(Level.INFO, "emailUser", "Successfully emailed " + var1.getUsername() + ".");
               }
            }
         }
      } else {
         throw new CredentialNotFoundException("Permission denied: cannot email.");
      }
   }

   public void watch(String... var1) throws IOException, CredentialNotFoundException {
      this.watchInternal(false, var1);
      this.watchlist.addAll(Arrays.asList(var1));
   }

   public void unwatch(String... var1) throws IOException, CredentialNotFoundException {
      this.watchInternal(true, var1);
      this.watchlist.removeAll(Arrays.asList(var1));
   }

   protected void watchInternal(boolean var1, String... var2) throws IOException, CredentialNotFoundException {
      String var3 = var1 ? "unwatch" : "watch";
      if (this.watchlist == null) {
         this.getRawWatchlist();
      }

      HashMap[] var4 = this.getPageInfo(var2);

      for(int var5 = 0; var5 < var2.length; ++var5) {
         StringBuilder var6;
         (var6 = new StringBuilder("title=")).append(URLEncoder.encode(this.normalize(var2[var5]), "UTF-8"));
         if (var1) {
            var6.append("&unwatch");
         }

         var6.append("&token=");
         String var7 = (String)var4[var5].get("watchtoken");
         var6.append(URLEncoder.encode(var7, "UTF-8"));
         this.post(this.apiUrl + "action=watch", var6.toString(), var3);
      }

      this.log(Level.INFO, var3, "Successfully " + var3 + "ed " + Arrays.toString(var2));
   }

   public String[] getRawWatchlist() throws IOException, CredentialNotFoundException {
      return this.getRawWatchlist(true);
   }

   public String[] getRawWatchlist(boolean var1) throws IOException, CredentialNotFoundException {
      if (this.user == null) {
         throw new CredentialNotFoundException("The watchlist is available for registered users only.");
      } else if (this.watchlist != null && var1) {
         return (String[])this.watchlist.toArray(new String[this.watchlist.size()]);
      } else {
         String var6 = this.query + "list=watchlistraw&wrlimit=max";
         String var2 = null;
         this.watchlist = new ArrayList(750);

         do {
            String var3;
            if (var2 == null) {
               var3 = this.fetch(var6, "getRawWatchlist");
            } else {
               var3 = this.fetch(var6 + "&wrcontinue=" + URLEncoder.encode(var2, "UTF-8"), "getRawWatchlist");
            }

            var2 = this.parseAttribute(var3, "wrcontinue", 0);

            for(int var4 = var3.indexOf("<wr "); var4 > 0; var4 = var3.indexOf("<wr ", var4)) {
               String var5 = this.parseAttribute(var3, "title", var4);
               if (this.namespace(var5) % 2 == 0) {
                  this.watchlist.add(var5);
               }

               ++var4;
            }
         } while(var2 != null);

         int var7 = this.watchlist.size();
         this.log(Level.INFO, "getRawWatchlist", "Successfully retrieved raw watchlist (" + var7 + " items)");
         return (String[])this.watchlist.toArray(new String[var7]);
      }
   }

   public boolean isWatched(String var1) throws IOException, CredentialNotFoundException {
      if (this.watchlist == null) {
         this.getRawWatchlist();
      }

      return this.watchlist.contains(var1);
   }

   public Wiki.Revision[] watchlist() throws IOException, CredentialNotFoundException {
      return this.watchlist(false);
   }

   public Wiki.Revision[] watchlist(boolean var1, int... var2) throws IOException, CredentialNotFoundException {
      if (this.user == null) {
         throw new CredentialNotFoundException("Not logged in");
      } else {
         StringBuilder var3;
         (var3 = new StringBuilder(this.query)).append("list=watchlist&wlprop=ids%7Ctitle%7Ctimestamp%7Cuser%7Ccomment%7Csizes&wllimit=max");
         if (var1) {
            var3.append("&wlallrev=true");
         }

         this.constructNamespaceString(var3, "wl", var2);
         ArrayList var7 = new ArrayList(667);
         String var8 = "";

         do {
            String var4 = this.fetch(var3.toString() + "&wlstart=" + var8, "watchlist");
            var8 = this.parseAttribute(var4, "wlstart", 0);

            for(int var5 = var4.indexOf("<item "); var5 > 0; var5 = var4.indexOf("<item ", var5)) {
               int var6 = var4.indexOf("/>", var5);
               var7.add(this.parseRevision(var4.substring(var5, var6), ""));
               ++var5;
            }
         } while(var8 != null);

         int var9 = var7.size();
         this.log(Level.INFO, "watchlist", "Successfully retrieved watchlist (" + var9 + " items)");
         return (Wiki.Revision[])var7.toArray(new Wiki.Revision[var9]);
      }
   }

   public String[][] search(String var1, int... var2) throws IOException {
      if (var2.length == 0) {
         var2 = new int[]{0};
      }

      StringBuilder var3;
      (var3 = new StringBuilder(this.query)).append("list=search&srwhat=text&srprop=snippet%7Csectionsnippet&srlimit=max&srsearch=");
      var3.append(URLEncoder.encode(var1, "UTF-8"));
      this.constructNamespaceString(var3, "sr", var2);
      var3.append("&sroffset=");
      boolean var8 = false;
      ArrayList var4 = new ArrayList(5000);

      while(!var8) {
         String var5;
         if (!(var5 = this.fetch(var3.toString() + var4.size(), "search")).contains("sroffset=\"")) {
            var8 = true;
         }

         for(int var6 = var5.indexOf("<p "); var6 > 0; var6 = var5.indexOf("<p ", var6)) {
            String[] var7;
            (var7 = new String[3])[0] = this.parseAttribute(var5, "title", var6);
            if (var5.contains("sectionsnippet=\"")) {
               var7[1] = this.decode(this.parseAttribute(var5, "sectionsnippet", var6));
            } else {
               var7[1] = "";
            }

            var7[2] = this.decode(this.parseAttribute(var5, "snippet", var6));
            var4.add(var7);
            ++var6;
         }
      }

      this.log(Level.INFO, "search", "Successfully searched for string \"" + var1 + "\" (" + var4.size() + " items found)");
      return (String[][])var4.toArray(new String[0][0]);
   }

   public String[] imageUsage(String var1, int... var2) throws IOException {
      StringBuilder var3 = new StringBuilder(this.query);
      var1 = var1.replaceFirst("^(File|Image|" + this.namespaceIdentifier(6) + "):", "");
      var3.append("list=imageusage&iulimit=max&iutitle=");
      var3.append(URLEncoder.encode(this.normalize("File:" + var1), "UTF-8"));
      this.constructNamespaceString(var3, "iu", var2);
      ArrayList var7 = new ArrayList(1333);
      String var4 = "";

      do {
         if (!var7.isEmpty()) {
            var4 = "&iucontinue=" + var4;
         }

         String var5 = this.fetch(var3 + var4, "imageUsage");
         var4 = this.parseAttribute(var5, "iucontinue", 0);

         for(int var6 = var5.indexOf("<iu "); var6 > 0; var6 = var5.indexOf("<iu ", var6)) {
            var7.add(this.decode(this.parseAttribute(var5, "title", var6)));
            ++var6;
         }
      } while(var4 != null);

      int var8 = var7.size();
      this.log(Level.INFO, "imageUsage", "Successfully retrieved usages of File:" + var1 + " (" + var8 + " items)");
      return (String[])var7.toArray(new String[var8]);
   }

   public String[] whatLinksHere(String var1, int... var2) throws IOException {
      return this.whatLinksHere(var1, false, var2);
   }

   public String[] whatLinksHere(String var1, boolean var2, int... var3) throws IOException {
      StringBuilder var4;
      (var4 = new StringBuilder(this.query)).append("list=backlinks&bllimit=max&bltitle=");
      var4.append(URLEncoder.encode(this.normalize(var1), "UTF-8"));
      this.constructNamespaceString(var4, "bl", var3);
      if (var2) {
         var4.append("&blfilterredir=redirects");
      }

      ArrayList var8 = new ArrayList(6667);
      String var5 = null;

      do {
         String var6;
         if (var5 == null) {
            var6 = this.fetch(var4.toString(), "whatLinksHere");
         } else {
            var6 = this.fetch(var4.toString() + "&blcontinue=" + var5, "whatLinksHere");
         }

         var5 = this.parseAttribute(var6, "blcontinue", 0);

         for(int var7 = var6.indexOf("<bl "); var7 > 0; var7 = var6.indexOf("<bl ", var7)) {
            var8.add(this.decode(this.parseAttribute(var6, "title", var7)));
            ++var7;
         }
      } while(var5 != null);

      int var9 = var8.size();
      this.log(Level.INFO, "whatLinksHere", "Successfully retrieved " + (var2 ? "redirects to " : "links to ") + var1 + " (" + var9 + " items)");
      return (String[])var8.toArray(new String[var9]);
   }

   public String[] whatTranscludesHere(String var1, int... var2) throws IOException {
      StringBuilder var3;
      (var3 = new StringBuilder(this.query)).append("list=embeddedin&eilimit=max&eititle=");
      var3.append(URLEncoder.encode(this.normalize(var1), "UTF-8"));
      this.constructNamespaceString(var3, "ei", var2);
      ArrayList var7 = new ArrayList(6667);
      String var4 = null;

      do {
         String var5;
         if (var4 == null) {
            var5 = this.fetch(var3.toString(), "whatTranscludesHere");
         } else {
            var5 = this.fetch(var3.toString() + "&eicontinue=" + var4, "whatTranscludesHere");
         }

         var4 = this.parseAttribute(var5, "eicontinue", 0);

         for(int var6 = var5.indexOf("<ei "); var6 > 0; var6 = var5.indexOf("<ei ", var6)) {
            var7.add(this.decode(this.parseAttribute(var5, "title", var6)));
            ++var6;
         }
      } while(var4 != null);

      int var8 = var7.size();
      this.log(Level.INFO, "whatTranscludesHere", "Successfully retrieved transclusions of " + var1 + " (" + var8 + " items)");
      return (String[])var7.toArray(new String[var8]);
   }

   public String[] getCategoryMembers(String var1, int... var2) throws IOException {
      return this.getCategoryMembers(var1, false, var2);
   }

   public String[] getCategoryMembers(String var1, boolean var2, int... var3) throws IOException {
      var1 = var1.replaceFirst("^(Category|" + this.namespaceIdentifier(14) + "):", "");
      StringBuilder var4;
      (var4 = new StringBuilder(this.query)).append("list=categorymembers&cmprop=title&cmlimit=max&cmtitle=");
      var4.append(URLEncoder.encode(this.normalize("Category:" + var1), "UTF-8"));
      boolean var5 = var3.length != 0;
      if (var2 && var5) {
         for(int var6 = 0; var5 && var6 < var3.length; ++var6) {
            var5 = var3[var6] != 14;
         }

         if (var5) {
            int[] var12;
            (var12 = Arrays.copyOf(var3, var3.length + 1))[var3.length] = 14;
            this.constructNamespaceString(var4, "cm", var12);
         } else {
            this.constructNamespaceString(var4, "cm", var3);
         }
      } else {
         this.constructNamespaceString(var4, "cm", var3);
      }

      ArrayList var13 = new ArrayList();
      String var7 = "";

      do {
         if (!var7.isEmpty()) {
            var7 = "&cmcontinue=" + URLEncoder.encode(var7, "UTF-8");
         }

         String var8 = this.fetch(var4.toString() + var7, "getCategoryMembers");
         var7 = this.parseAttribute(var8, "cmcontinue", 0);

         for(int var9 = var8.indexOf("<cm "); var9 > 0; var9 = var8.indexOf("<cm ", var9)) {
            String var10 = this.decode(this.parseAttribute(var8, "title", var9));
            boolean var11 = this.namespace(var10) == 14;
            if (var2 && var11) {
               var13.addAll(Arrays.asList(this.getCategoryMembers(var10, false, var3)));
            }

            if (!var2 || !var5 || !var11) {
               var13.add(var10);
            }

            ++var9;
         }
      } while(var7 != null);

      int var14 = var13.size();
      this.log(Level.INFO, "getCategoryMembers", "Successfully retrieved contents of Category:" + var1 + " (" + var14 + " items)");
      return (String[])var13.toArray(new String[var14]);
   }

   public ArrayList[] linksearch(String var1) throws IOException {
      return this.linksearch(var1, "http");
   }

   public ArrayList[] linksearch(String var1, String var2, int... var3) throws IOException {
      StringBuilder var4;
      (var4 = new StringBuilder(this.query)).append("list=exturlusage&euprop=title%7curl&eulimit=max&euquery=");
      var4.append(var1);
      var4.append("&euprotocol=");
      var4.append(var2);
      this.constructNamespaceString(var4, "eu", var3);
      var4.append("&euoffset=");
      boolean var9 = false;
      ArrayList[] var5 = new ArrayList[]{new ArrayList(667), new ArrayList(667)};

      while(!var9) {
         String var6;
         if (!(var6 = this.fetch(var4.toString() + var5[0].size(), "linksearch")).contains("euoffset=\"")) {
            var9 = true;
         }

         for(int var7 = var6.indexOf("<eu"); var7 > 0; var7 = var6.indexOf("<eu ", var7)) {
            String var8 = this.parseAttribute(var6, "url", var7);
            var5[0].add(this.decode(this.parseAttribute(var6, "title", var7)));
            if (var8.charAt(0) == '/') {
               var5[1].add(new URL(var2 + ":" + var8));
            } else {
               var5[1].add(new URL(var8));
            }

            ++var7;
         }
      }

      this.log(Level.INFO, "linksearch", "Successfully returned instances of external link " + var1 + " (" + var5[0].size() + " links)");
      return var5;
   }

   public Wiki.LogEntry[] getIPBlockList(String var1) throws IOException {
      return this.getIPBlockList(var1, (Calendar)null, (Calendar)null);
   }

   public Wiki.LogEntry[] getIPBlockList(Calendar var1, Calendar var2) throws IOException {
      return this.getIPBlockList("", var1, var2);
   }

   protected Wiki.LogEntry[] getIPBlockList(String var1, Calendar var2, Calendar var3) throws IOException {
      if (var2 != null && var3 != null && var2.before(var3)) {
         throw new IllegalArgumentException("Specified start date is before specified end date!");
      } else {
         String var4 = this.calendarToTimestamp(var2 == null ? this.makeCalendar() : var2);
         StringBuilder var5;
         (var5 = new StringBuilder(this.query)).append("list=blocks&bklimit=");
         var5.append(this.max);
         if (var3 != null) {
            var5.append("&bkend=");
            var5.append(this.calendarToTimestamp(var3));
         }

         if (!var1.isEmpty()) {
            var5.append("&bkusers=");
            var5.append(var1);
         }

         var5.append("&bkstart=");
         ArrayList var6 = new ArrayList(1333);

         int var8;
         do {
            String var7 = this.fetch(var5.toString() + var4, "getIPBlockList");
            var4 = this.parseAttribute(var7, "bkstart", 0);

            for(var8 = var7.indexOf("<block "); var8 > 0; var8 = var7.indexOf("<block ", var8)) {
               int var9 = var7.indexOf("/>", var8);
               String var12 = var7.substring(var8, var9);
               Wiki.LogEntry var10;
               (var10 = this.parseLogEntry(var12)).type = "block";
               var10.action = "block";
               if (var10.user == null) {
                  var10.target = "#" + this.parseAttribute(var12, "id", 0);
               } else {
                  var10.target = this.namespaceIdentifier(2) + ":" + var10.user.username;
               }

               var10.user = new Wiki.User(this.decode(this.parseAttribute(var12, "by", 0)));
               var6.add(var10);
               ++var8;
            }
         } while(var4 != null);

         StringBuilder var11 = new StringBuilder("Successfully fetched IP block list ");
         if (!var1.isEmpty()) {
            var11.append(" for ");
            var11.append(var1);
         }

         if (var2 != null) {
            var11.append(" from ");
            var11.append(var2.getTime().toString());
         }

         if (var3 != null) {
            var11.append(" to ");
            var11.append(var3.getTime().toString());
         }

         var8 = var6.size();
         var11.append(" (");
         var11.append(var8);
         var11.append(" entries)");
         this.log(Level.INFO, "getIPBlockList", var11.toString());
         return (Wiki.LogEntry[])var6.toArray(new Wiki.LogEntry[var8]);
      }
   }

   public Wiki.LogEntry[] getLogEntries(int var1) throws IOException {
      return this.getLogEntries((Calendar)null, (Calendar)null, var1, "", "", (Wiki.User)null, "", 167317762);
   }

   public Wiki.LogEntry[] getLogEntries(Wiki.User var1) throws IOException {
      return this.getLogEntries((Calendar)null, (Calendar)null, Integer.MAX_VALUE, "", "", var1, "", 167317762);
   }

   public Wiki.LogEntry[] getLogEntries(String var1) throws IOException {
      return this.getLogEntries((Calendar)null, (Calendar)null, Integer.MAX_VALUE, "", "", (Wiki.User)null, var1, 167317762);
   }

   public Wiki.LogEntry[] getLogEntries(Calendar var1, Calendar var2) throws IOException {
      return this.getLogEntries(var1, var2, Integer.MAX_VALUE, "", "", (Wiki.User)null, "", 167317762);
   }

   public Wiki.LogEntry[] getLogEntries(int var1, String var2, String var3) throws IOException {
      return this.getLogEntries((Calendar)null, (Calendar)null, var1, var2, var3, (Wiki.User)null, "", 167317762);
   }

   public Wiki.LogEntry[] getLogEntries(Calendar var1, Calendar var2, int var3, String var4, String var5, Wiki.User var6, String var7, int var8) throws IOException {
      StringBuilder var9;
      (var9 = new StringBuilder(this.query)).append("list=logevents&leprop=title%7Ctype%7Cuser%7Ctimestamp%7Ccomment%7Cdetails&lelimit=");
      if (var3 <= 0) {
         throw new IllegalArgumentException("Tried to retrieve less than one log entry!");
      } else {
         var9.append(var3 > this.max ? this.max : var3);
         if (!var4.equals("")) {
            if (var5.isEmpty()) {
               var9.append("&letype=");
               var9.append(var4);
            } else {
               var9.append("&leaction=");
               var9.append(var4);
               var9.append("/");
               var9.append(var5);
            }
         }

         if (var8 != 167317762) {
            var9.append("&lenamespace=");
            var9.append(var8);
         }

         if (var6 != null) {
            var9.append("&leuser=");
            var9.append(URLEncoder.encode(var6.getUsername(), "UTF-8"));
         }

         if (!var7.isEmpty()) {
            var9.append("&letitle=");
            var9.append(URLEncoder.encode(this.normalize(var7), "UTF-8"));
         }

         var5 = "";
         if (var1 != null) {
            if (var2 != null && var1.before(var2)) {
               throw new IllegalArgumentException("Specified start date is before specified end date!");
            }

            var5 = this.calendarToTimestamp(var1);
         }

         if (var2 != null) {
            var9.append("&leend=");
            var9.append(this.calendarToTimestamp(var2));
         }

         ArrayList var10 = new ArrayList(6667);

         int var13;
         do {
            String var11 = this.fetch(var9.toString() + "&lestart=" + var5, "getLogEntries");

            int var14;
            for(var5 = this.parseAttribute(var11, "lestart", 0); var11.contains("<item") && var10.size() < var3; var11 = var11.substring(var14)) {
               var13 = var11.indexOf("<item");
               if ((var14 = var11.indexOf("><item", var13)) < 0) {
                  var14 = var11.length();
               }

               var10.add(this.parseLogEntry(var11.substring(var13, var14)));
            }
         } while(var10.size() < var3 && var5 != null);

         StringBuilder var12;
         (var12 = new StringBuilder("Successfully retrieved log (type=")).append(var4);
         var13 = var10.size();
         var12.append(", ");
         var12.append(var13);
         var12.append(" entries)");
         this.log(Level.INFO, "getLogEntries", var12.toString());
         return (Wiki.LogEntry[])var10.toArray(new Wiki.LogEntry[var13]);
      }
   }

   protected Wiki.LogEntry parseLogEntry(String var1) {
      String var2 = "";
      String var3 = "";
      if (var1.contains("type=\"")) {
         var2 = this.parseAttribute(var1, "type", 0);
         if (!var1.contains("actionhidden=\"")) {
            var3 = this.parseAttribute(var1, "action", 0);
         }
      }

      String var4;
      if (var1.contains("commenthidden=\"")) {
         var4 = null;
      } else if (var2.equals("newusers")) {
         var4 = "";
      } else if (var1.contains("reason=\"")) {
         var4 = this.parseAttribute(var1, "reason", 0);
      } else {
         var4 = this.parseAttribute(var1, "comment", 0);
      }

      Wiki.User var5 = null;
      if (var1.contains("user=\"")) {
         var5 = new Wiki.User(this.decode(this.parseAttribute(var1, "user", 0)));
      }

      String var6 = null;
      if (var1.contains("title=\"")) {
         var6 = this.decode(this.parseAttribute(var1, "title", 0));
      }

      String var7 = this.convertTimestamp(this.parseAttribute(var1, "timestamp", 0));
      Object var8 = null;
      if (var1.contains("commenthidden")) {
         var8 = null;
      } else if (var2.equals("move")) {
         var8 = this.decode(this.parseAttribute(var1, "new_title", 0));
      } else {
         int var9;
         if (!var2.equals("block") && !var1.contains("<block")) {
            int var14;
            if (var2.equals("protect")) {
               if (var3.equals("unprotect")) {
                  var8 = null;
               } else {
                  var9 = var1.indexOf("<param>") + 7;
                  var14 = var1.indexOf("</param>", var9);
                  var8 = var1.substring(var9, var14);
               }
            } else if (var2.equals("renameuser")) {
               var9 = var1.indexOf("<param>") + 7;
               var14 = var1.indexOf("</param>", var9);
               var8 = this.decode(var1.substring(var9, var14));
            } else if (var2.equals("rights")) {
               var9 = var1.indexOf("new=\"") + 5;
               var14 = var1.indexOf(34, var9);
               StringTokenizer var12 = new StringTokenizer(var1.substring(var9, var14), ", ");
               ArrayList var15 = new ArrayList(10);

               while(var12.hasMoreTokens()) {
                  var15.add(var12.nextToken());
               }

               var8 = var15.toArray(new String[var15.size()]);
            }
         } else {
            var9 = var1.indexOf("<block") + 7;
            String var10 = var1.substring(var9);
            int var11;
            if ((var11 = var1.contains("expiry=\"") ? var10.indexOf("expiry=") + 8 : var10.indexOf("duration=") + 10) > 10) {
               int var13 = var10.indexOf(34, var11);
               var8 = new Object[]{var10.contains("anononly"), var10.contains("nocreate"), var10.contains("noautoblock"), var10.contains("noemail"), var10.contains("nousertalk"), var10.substring(var11, var13)};
            }
         }
      }

      return new Wiki.LogEntry(var2, var3, var4, var5, var6, var7, var8);
   }

   public String[] prefixIndex(String var1) throws IOException {
      return this.listPages(var1, (HashMap)null, 167317762, -1, -1);
   }

   public String[] shortPages(int var1) throws IOException {
      return this.listPages("", (HashMap)null, 0, -1, var1);
   }

   public String[] shortPages(int var1, int var2) throws IOException {
      return this.listPages("", (HashMap)null, var2, -1, var1);
   }

   public String[] longPages(int var1) throws IOException {
      return this.listPages("", (HashMap)null, 0, var1, -1);
   }

   public String[] longPages(int var1, int var2) throws IOException {
      return this.listPages("", (HashMap)null, var2, var1, -1);
   }

   public String[] listPages(String var1, HashMap var2, int var3) throws IOException {
      return this.listPages(var1, var2, var3, -1, -1);
   }

   public String[] listPages(String var1, HashMap var2, int var3, int var4, int var5) throws IOException {
      StringBuilder var6;
      (var6 = new StringBuilder(this.query)).append("list=allpages&aplimit=max");
      if (!var1.isEmpty()) {
         var3 = this.namespace(var1);
         if (var1.contains(":") && var3 != 0) {
            var1 = var1.substring(var1.indexOf(58) + 1);
         }

         var6.append("&apprefix=");
         var6.append(URLEncoder.encode(this.normalize(var1), "UTF-8"));
      } else if (var3 == 167317762) {
         throw new UnsupportedOperationException("ALL_NAMESPACES not supported in MediaWiki API.");
      }

      var6.append("&apnamespace=");
      var6.append(var3);
      if (var2 != null) {
         StringBuilder var11 = new StringBuilder("&apprtype=");
         StringBuilder var7 = new StringBuilder("&apprlevel=");
         Iterator var8 = var2.entrySet().iterator();

         while(var8.hasNext()) {
            Entry var9;
            String var10;
            if ((var10 = (String)(var9 = (Entry)var8.next()).getKey()).equals("cascade")) {
               var6.append("&apprfiltercascade=");
               var6.append((Boolean)var9.getValue() ? "cascading" : "noncascading");
            } else if (!var10.contains("expiry")) {
               var11.append(var10);
               var11.append("%7C");
               var7.append((String)var9.getValue());
               var7.append("%7C");
            }
         }

         var11.delete(var11.length() - 3, var11.length());
         var7.delete(var7.length() - 3, var7.length());
         var6.append(var11);
         var6.append(var7);
      }

      if (var4 != -1) {
         var6.append("&apminsize=");
         var6.append(var4);
      }

      if (var5 != -1) {
         var6.append("&apmaxsize=");
         var6.append(var5);
      }

      ArrayList var12 = new ArrayList(6667);
      String var13 = "";

      do {
         String var14 = var6.toString();
         if (!var13.isEmpty()) {
            var14 = var14 + "&apcontinue=" + URLEncoder.encode(var13, "UTF-8");
         }

         String var15 = this.fetch(var14, "listPages");
         if (var5 < 0 && var4 < 0 && var1.isEmpty() && var2 == null) {
            var13 = null;
         } else if (var15.contains("apcontinue=")) {
            var13 = this.parseAttribute(var15, "apcontinue", 0);
         }

         for(int var17 = var15.indexOf("<p "); var17 > 0; var17 = var15.indexOf("<p ", var17)) {
            var12.add(this.decode(this.parseAttribute(var15, "title", var17)));
            ++var17;
         }
      } while(var13 != null);

      int var16 = var12.size();
      this.log(Level.INFO, "listPages", "Successfully retrieved page list (" + var16 + " pages)");
      return (String[])var12.toArray(new String[var16]);
   }

   public String[] queryPage(String var1) throws IOException, CredentialNotFoundException {
      if (!var1.equals("Unwatchedpages") || this.user != null && this.user.isAllowedTo("unwatchedpages")) {
         String var2 = this.query + "action=query&list=querypage&qplimit=max&qppage=" + var1 + "&qpcontinue=";
         String var3 = "";
         ArrayList var4 = new ArrayList(1333);

         do {
            String var5 = this.fetch(var2 + var3, "queryPage");
            var3 = this.parseAttribute(var5, "qpoffset", 0);

            for(int var6 = var5.indexOf("<page "); var6 > 0; var6 = var5.indexOf("<page ", var6)) {
               var4.add(this.decode(this.parseAttribute(var5, "title", var6)));
               ++var6;
            }
         } while(var3 != null);

         int var7 = var4.size();
         this.log(Level.INFO, "queryPage", "Successfully retrieved [[Special:" + var1 + "]] (" + var7 + " pages)");
         return (String[])var4.toArray(new String[var7]);
      } else {
         throw new CredentialNotFoundException("User does not have the \"unwatchedpages\" permission.");
      }
   }

   public Wiki.Revision[] newPages(int var1) throws IOException {
      return this.recentChanges(var1, 0, true, 0);
   }

   public Wiki.Revision[] newPages(int var1, int var2) throws IOException {
      return this.recentChanges(var1, var2, true, 0);
   }

   public Wiki.Revision[] newPages(int var1, int var2, int... var3) throws IOException {
      return this.recentChanges(var1, var2, true, var3);
   }

   public Wiki.Revision[] recentChanges(int var1) throws IOException {
      return this.recentChanges(var1, 0, false, 0);
   }

   public Wiki.Revision[] recentChanges(int var1, int... var2) throws IOException {
      return this.recentChanges(var1, 0, false, var2);
   }

   public Wiki.Revision[] recentChanges(int var1, int var2, int... var3) throws IOException {
      return this.recentChanges(var1, var2, false, var3);
   }

   protected Wiki.Revision[] recentChanges(int var1, int var2, boolean var3, int... var4) throws IOException {
      StringBuilder var5;
      (var5 = new StringBuilder(this.query)).append("list=recentchanges&rcprop=title%7Cids%7Cuser%7Ctimestamp%7Cflags%7Ccomment%7Csizes&rclimit=max");
      this.constructNamespaceString(var5, "rc", var4);
      if (var3) {
         var5.append("&rctype=new");
      }

      if (var2 > 0) {
         var5.append("&rcshow=");
         if ((var2 & 1) == 1) {
            var5.append("!anon%7C");
         }

         if ((var2 & 4) == 4) {
            var5.append("!self%7C");
         }

         if ((var2 & 8) == 8) {
            var5.append("!minor%7C");
         }

         if ((var2 & 16) == 16) {
            var5.append("!patrolled%7C");
         }

         if ((var2 & 2) == 2) {
            var5.append("!bot%7C");
         }

         var5.delete(var5.length() - 3, var5.length());
      }

      var5.append("&rcstart=");
      String var9 = this.calendarToTimestamp(this.makeCalendar());
      ArrayList var10 = new ArrayList(750);

      do {
         String var6 = var5.toString();
         var6 = this.fetch(var6 + var9, var3 ? "newPages" : "recentChanges");
         var9 = this.parseAttribute(var6, "rcstart", 0);

         for(int var7 = var6.indexOf("<rc "); var7 > 0 && var10.size() < var1; var7 = var6.indexOf("<rc ", var7)) {
            int var8 = var6.indexOf("/>", var7);
            var10.add(this.parseRevision(var6.substring(var7, var8), ""));
            ++var7;
         }
      } while(var10.size() < var1);

      int var11 = var10.size();
      this.log(Level.INFO, "recentChanges", "Successfully retrieved recent changes (" + var11 + " revisions)");
      return (Wiki.Revision[])var10.toArray(new Wiki.Revision[var11]);
   }

   public String[][] getInterWikiBacklinks(String var1) throws IOException {
      return this.getInterWikiBacklinks(var1, "|");
   }

   public String[][] getInterWikiBacklinks(String var1, String var2) throws IOException {
      if (var2.equals("|") && var1.isEmpty()) {
         throw new IllegalArgumentException("Interwiki backlinks: title specified without prefix!");
      } else {
         StringBuilder var3;
         (var3 = new StringBuilder(this.query)).append("list=iwbacklinks&iwbllimit=max&iwblprefix=");
         var3.append(var1);
         if (!var2.equals("|")) {
            var3.append("&iwbltitle=");
            var3.append(var2);
         }

         var3.append("&iwblprop=iwtitle%7Ciwprefix");
         var1 = "";
         ArrayList var6 = new ArrayList(500);

         do {
            String var4;
            if (var1.isEmpty()) {
               var4 = this.fetch(var3.toString(), "getInterWikiBacklinks");
            } else {
               var4 = this.fetch(var3.toString() + "&iwblcontinue=" + var1, "getInterWikiBacklinks");
            }

            var1 = this.parseAttribute(var4, "iwblcontinue", 0);

            for(int var5 = var4.indexOf("<iw "); var5 > 0; var5 = var4.indexOf("<iw ", var5)) {
               var6.add(new String[]{this.parseAttribute(var4, "title", var5), this.parseAttribute(var4, "iwprefix", var5) + ':' + this.parseAttribute(var4, "iwtitle", var5)});
               ++var5;
            }
         } while(var1 != null);

         this.log(Level.INFO, "getInterWikiBacklinks", "Successfully retrieved interwiki backlinks (" + var6.size() + " interwikis)");
         return (String[][])var6.toArray(new String[0][0]);
      }
   }

   protected String fetch(String var1, String var2) throws IOException {
      this.logurl(var1, var2);
      URLConnection var3;
      (var3 = (new URL(var1)).openConnection()).setConnectTimeout(30000);
      var3.setReadTimeout(180000);
      this.setCookies(var3);
      var3.connect();
      this.grabCookies(var3);
      int var4;
      if ((var4 = var3.getHeaderFieldInt("X-Database-Lag", -5)) > this.maxlag) {
         try {
            synchronized(this) {
               int var9 = var3.getHeaderFieldInt("Retry-After", 10);
               this.log(Level.WARNING, var2, "Current database lag " + var4 + " s exceeds " + this.maxlag + " s, waiting " + var9 + " s.");
               Thread.sleep((long)(var9 * 1000));
            }
         } catch (InterruptedException var7) {
         }

         return this.fetch(var1, var2);
      } else {
         BufferedReader var5 = new BufferedReader(new InputStreamReader((InputStream)(this.zipped ? new GZIPInputStream(var3.getInputStream()) : var3.getInputStream()), "UTF-8"));
         StringBuilder var10 = new StringBuilder(100000);

         String var8;
         while((var8 = var5.readLine()) != null) {
            var10.append(var8);
            var10.append("\n");
         }

         var5.close();
         if ((var1 = var10.toString()).contains("<error code=")) {
            if ((this.assertion & 2) == 2 && var1.contains("error code=\"assertbotfailed\"")) {
               throw new AssertionError("Bot privileges missing or revoked, or session expired.");
            }

            if ((this.assertion & 1) == 1 && var1.contains("error code=\"assertuserfailed\"")) {
               throw new AssertionError("Session expired.");
            }

            if (!var1.matches("code=\"(rvnosuchsection)")) {
               throw new UnknownError("MW API error. Server response was: " + var1);
            }
         }

         return var1;
      }
   }

   protected String post(String var1, String var2, String var3) throws IOException {
      this.logurl(var1, var3);
      System.err.println("URL: " + var1 + "; CAÃ–Ã–:: " + var2 + "; CALL: " + var3);
      URLConnection var4 = (new URL(var1)).openConnection();
      this.setCookies(var4);
      var4.setDoOutput(true);
      var4.setConnectTimeout(30000);
      var4.setReadTimeout(180000);
      var4.connect();
      OutputStreamWriter var6;
      (var6 = new OutputStreamWriter(var4.getOutputStream(), "UTF-8")).write(var2);
      var6.close();
      BufferedReader var5 = new BufferedReader(new InputStreamReader((InputStream)(this.zipped ? new GZIPInputStream(var4.getInputStream()) : var4.getInputStream()), "UTF-8"));
      this.grabCookies(var4);
      StringBuilder var7 = new StringBuilder(100000);

      while((var1 = var5.readLine()) != null) {
         var7.append(var1);
         var7.append("\n");
      }

      var5.close();
      return var7.toString();
   }

   protected String multipartPost(String var1, Map var2, String var3) throws IOException {
      this.logurl(var1, var3);
      URLConnection var9 = (new URL(var1)).openConnection();
      var3 = "----------NEXT PART----------";
      var9.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + var3);
      this.setCookies(var9);
      var9.setDoOutput(true);
      var9.setConnectTimeout(30000);
      var9.setReadTimeout(180000);
      var9.connect();
      var3 = "--" + var3 + "\r\n";
      ByteArrayOutputStream var4 = new ByteArrayOutputStream();
      DataOutputStream var5;
      (var5 = new DataOutputStream(var4)).writeBytes(var3);
      Iterator var10 = var2.entrySet().iterator();

      String var7;
      while(var10.hasNext()) {
         Entry var6;
         var7 = (String)(var6 = (Entry)var10.next()).getKey();
         Object var8 = var6.getValue();
         var5.writeBytes("Content-Disposition: form-data; name=\"" + var7 + "\"\r\n");
         if (var8 instanceof String) {
            var5.writeBytes("Content-Type: text/plain; charset=UTF-8\r\n\r\n");
            var5.write(((String)var8).getBytes("UTF-8"));
         } else {
            if (!(var8 instanceof byte[])) {
               throw new UnsupportedOperationException("Unrecognized data type");
            }

            var5.writeBytes("Content-Type: application/octet-stream\r\n\r\n");
            var5.write((byte[])var8);
         }

         var5.writeBytes("\r\n");
         var5.writeBytes(var3);
      }

      var5.writeBytes("--\r\n");
      var5.close();
      OutputStream var11;
      (var11 = var9.getOutputStream()).write(var4.toByteArray());
      var11.close();
      BufferedReader var12 = new BufferedReader(new InputStreamReader((InputStream)(this.zipped ? new GZIPInputStream(var9.getInputStream()) : var9.getInputStream()), "UTF-8"));
      this.grabCookies(var9);
      StringBuilder var13 = new StringBuilder(100000);

      while((var7 = var12.readLine()) != null) {
         var13.append(var7);
         var13.append("\n");
      }

      var12.close();
      return var13.toString();
   }

   protected void checkErrorsAndUpdateStatus(String var1, String var2) throws IOException, LoginException {
      if (this.statuscounter > this.statusinterval) {
         this.user.getUserInfo();
         if ((this.assertion & 8) == 8 && !this.user.isA("sysop")) {
            throw new AssertionError("Sysop privileges missing or revoked, or session expired");
         }

         if ((this.assertion & 4) == 4 && this.hasNewMessages()) {
            throw new AssertionError("User has new messages");
         }

         this.statuscounter = 0;
      } else {
         ++this.statuscounter;
      }

      if (!var1.contains("result=\"Success\"")) {
         if (var1.isEmpty()) {
            throw new UnknownError("Received empty response from server!");
         } else if ((this.assertion & 2) == 2 && var1.contains("error code=\"assertbotfailed\"")) {
            throw new AssertionError("Bot privileges missing or revoked, or session expired.");
         } else if ((this.assertion & 1) == 1 && var1.contains("error code=\"assertuserfailed\"")) {
            throw new AssertionError("Session expired.");
         } else if (var1.contains("error code=\"permissiondenied\"")) {
            throw new CredentialNotFoundException("Permission denied.");
         } else if (var1.contains("error code=\"ratelimited\"")) {
            this.log(Level.WARNING, var2, "Server-side throttle hit.");
            throw new HttpRetryException("Action throttled.", 503);
         } else if (!var1.contains("error code=\"blocked") && !var1.contains("error code=\"autoblocked\"")) {
            if (var1.contains("error code=\"readonly\"")) {
               this.log(Level.WARNING, var2, "Database locked!");
               throw new HttpRetryException("Database locked!", 503);
            } else if (var1.contains("error code=\"unknownerror\"")) {
               throw new UnknownError("Unknown MediaWiki API error, response was " + var1);
            } else {
               throw new IOException("MediaWiki error, response was " + var1);
            }
         } else {
            this.log(Level.SEVERE, var2, "Cannot " + var2 + " - user is blocked!.");
            throw new AccountLockedException("Current user is blocked!");
         }
      }
   }

   protected String decode(String var1) {
      return var1.replace("&lt;", "<").replace("&gt;", ">").replace("&amp;", "&").replace("&quot;", "\"").replace("&#039;", "'");
   }

   private String parseAttribute(String var1, String var2, int var3) {
      System.err.println("LOGIN RET: " + var1);
      if (var1.contains(var2 + "=\"")) {
         int var4 = var1.indexOf(var2 + "=\"", var3) + var2.length() + 2;
         var3 = var1.indexOf(34, var4);
         return var1.substring(var4, var3);
      } else {
         return null;
      }
   }

   protected void constructNamespaceString(StringBuilder var1, String var2, int... var3) {
      int var4;
      if ((var4 = var3.length) != 0) {
         var1.append("&");
         var1.append(var2);
         var1.append("namespace=");

         for(int var5 = 0; var5 < var4 - 1; ++var5) {
            var1.append(var3[var5]);
            var1.append("%7C");
         }

         var1.append(var3[var4 - 1]);
      }
   }

   protected String[] constructTitleString(String[] var1) throws IOException {
      String[] var2 = new String[var1.length / this.slowmax + 1];
      StringBuilder var3 = new StringBuilder();

      for(int var4 = 0; var4 < var1.length; ++var4) {
         var3.append(this.normalize(var1[var4]));
         if (var4 != var1.length - 1 && var4 != this.slowmax - 1) {
            var3.append("|");
         } else {
            var2[var4 / this.slowmax] = URLEncoder.encode(var3.toString(), "UTF-8");
            var3 = new StringBuilder();
         }
      }

      return var2;
   }

   public String normalize(String var1) throws IOException {
      if (var1.isEmpty()) {
         return var1;
      } else {
         char[] var2 = var1.toCharArray();
         int var3;
         if (this.wgCapitalLinks) {
            if ((var3 = this.namespace(var1)) == 0) {
               var2[0] = Character.toUpperCase(var2[0]);
            } else {
               var3 = this.namespaceIdentifier(var3).length() + 1;
               var2[var3] = Character.toUpperCase(var2[var3]);
            }
         }

         var3 = 0;

         while(var3 < var2.length) {
            switch(var2[var3]) {
            case '<':
            case '>':
            case '[':
            case ']':
            case '{':
            case '|':
            case '}':
               throw new IllegalArgumentException(var1 + " is an illegal title");
            case '_':
               var2[var3] = ' ';
            default:
               ++var3;
            }
         }

         return Normalizer.normalize((new String(var2)).trim().replaceAll("\\s+", " "), Form.NFC);
      }
   }

   private synchronized void throttle(long var1) {
      try {
         long var3;
         if ((var3 = (long)this.throttle - System.currentTimeMillis() + var1) > 0L) {
            Thread.sleep(var3);
         }

      } catch (InterruptedException var5) {
      }
   }

   protected boolean checkRights(HashMap var1, String var2) throws IOException {
      if ((var1 = (HashMap)var1.get("protection")).containsKey(var2)) {
         if ((var2 = (String)var1.get(var2)).equals("autoconfirmed")) {
            return this.user.isAllowedTo("autoconfirmed");
         }

         if (var2.equals("sysop")) {
            return this.user.isAllowedTo("editprotected");
         }
      }

      return (Boolean)var1.get("cascade") == Boolean.TRUE ? this.user.isAllowedTo("editprotected") : true;
   }

   protected void setCookies(URLConnection var1) {
      StringBuilder var2 = new StringBuilder(100);
      Iterator var3 = this.cookies.entrySet().iterator();

      while(var3.hasNext()) {
         Entry var4 = (Entry)var3.next();
         var2.append((String)var4.getKey());
         var2.append("=");
         var2.append((String)var4.getValue());
         var2.append("; ");
      }

      var1.setRequestProperty("Cookie", var2.toString());
      if (this.zipped) {
         var1.setRequestProperty("Accept-encoding", "gzip");
      }

      var1.setRequestProperty("User-Agent", this.useragent);
   }

   private void grabCookies(URLConnection var1) {
      String var2;
      for(int var3 = 1; (var2 = var1.getHeaderFieldKey(var3)) != null; ++var3) {
         if (var2.equals("Set-Cookie")) {
            String var4 = (var2 = (var2 = var1.getHeaderField(var3)).substring(0, var2.indexOf(59))).substring(0, var2.indexOf(61));
            if (!(var2 = var2.substring(var2.indexOf(61) + 1, var2.length())).equals("deleted")) {
               this.cookies.put(var4, var2);
            }
         }
      }

   }

   protected void log(Level var1, String var2, String var3) {
      Logger.getLogger("wiki").logp(var1, "Wiki", var2, "[{0}] {1}", new Object[]{this.domain, var3});
   }

   protected void logurl(String var1, String var2) {
      Logger.getLogger("wiki").logp(Level.INFO, "Wiki", var2, "Fetching URL {0}", var1);
   }

   public Calendar makeCalendar() {
      return new GregorianCalendar(TimeZone.getTimeZone(this.timezone));
   }

   protected String calendarToTimestamp(Calendar var1) {
      return String.format("%04d%02d%02d%02d%02d%02d", var1.get(1), var1.get(2) + 1, var1.get(5), var1.get(11), var1.get(12), var1.get(13));
   }

   protected final Calendar timestampToCalendar(String var1, boolean var2) {
      Calendar var3 = this.makeCalendar();
      if (var2) {
         var1 = this.convertTimestamp(var1);
      }

      int var9 = Integer.parseInt(var1.substring(0, 4));
      int var4 = Integer.parseInt(var1.substring(4, 6)) - 1;
      int var5 = Integer.parseInt(var1.substring(6, 8));
      int var6 = Integer.parseInt(var1.substring(8, 10));
      int var7 = Integer.parseInt(var1.substring(10, 12));
      int var8 = Integer.parseInt(var1.substring(12, 14));
      var3.set(var9, var4, var5, var6, var7, var8);
      return var3;
   }

   protected String convertTimestamp(String var1) {
      StringBuilder var2;
      (var2 = new StringBuilder(var1.substring(0, 4))).append(var1.substring(5, 7));
      var2.append(var1.substring(8, 10));
      var2.append(var1.substring(11, 13));
      var2.append(var1.substring(14, 16));
      var2.append(var1.substring(17, 19));
      return var2.toString();
   }

   private void writeObject(ObjectOutputStream var1) throws IOException {
      var1.defaultWriteObject();
   }

   private void readObject(ObjectInputStream var1) throws IOException, ClassNotFoundException {
      var1.defaultReadObject();
      this.statuscounter = this.statusinterval;
   }

   public class Revision implements Comparable {
      private boolean minor;
      private boolean bot;
      private boolean rvnew;
      private String summary;
      private long revid;
      private long rcid = -1L;
      private long previous = 0L;
      private long next = 0L;
      private Calendar timestamp;
      private String user;
      private String title;
      private String rollbacktoken = null;
      private int size = 0;
      private int sizediff = 0;
      private boolean summaryDeleted = false;
      private boolean userDeleted = false;

      public Revision(long var2, Calendar var4, String var5, String var6, String var7, boolean var8, boolean var9, boolean var10, int var11) {
         this.revid = var2;
         this.timestamp = var4;
         this.summary = var6;
         this.minor = var8;
         this.user = var7;
         this.title = var5;
         this.bot = var9;
         this.rvnew = var10;
         this.size = var11;
      }

      public String getText() throws IOException {
         if (this.revid < 1L) {
            throw new IllegalArgumentException("Log entries have no valid content!");
         } else {
            String var1 = Wiki.this.base + URLEncoder.encode(this.title, "UTF-8") + "&oldid=" + this.revid + "&action=raw";
            var1 = Wiki.this.fetch(var1, "Revision.getText");
            Wiki.this.log(Level.INFO, "Revision.getText", "Successfully retrieved text of revision " + this.revid);
            return Wiki.this.decode(var1);
         }
      }

      public String getRenderedText() throws IOException {
         if (this.revid < 1L) {
            throw new IllegalArgumentException("Log entries have no valid content!");
         } else {
            String var1 = Wiki.this.base + URLEncoder.encode(this.title, "UTF-8") + "&oldid=" + this.revid + "&action=render";
            var1 = Wiki.this.fetch(var1, "Revision.getRenderedText");
            Wiki.this.log(Level.INFO, "Revision.getRenderedText", "Successfully retrieved rendered text of revision " + this.revid);
            return Wiki.this.decode(var1);
         }
      }

      public String diff(Wiki.Revision var1) throws IOException {
         return this.diff(var1.revid, "");
      }

      public String diff(String var1) throws IOException {
         return this.diff(0L, var1);
      }

      public String diff(long var1) throws IOException {
         return this.diff(var1, "");
      }

      protected String diff(long var1, String var3) throws IOException {
         StringBuilder var4;
         (var4 = new StringBuilder("revids=")).append(this.revid);
         if (var1 == -1L) {
            var4.append("&rvdiffto=next");
         } else if (var1 == -2L) {
            var4.append("&rvdiffto=cur");
         } else if (var1 == -3L) {
            var4.append("&rvdiffto=prev");
         } else if (var1 == 0L) {
            var4.append("&rvdifftotext=");
            var4.append(var3);
         } else {
            var4.append("&rvdiffto=");
            var4.append(var1);
         }

         String var5;
         if ((var5 = Wiki.this.post(Wiki.this.query + "prop=revisions", var4.toString(), "Revision.diff")).contains("</diff>")) {
            int var2 = var5.indexOf("<diff");
            var2 = var5.indexOf(">", var2) + 1;
            int var6 = var5.indexOf("</diff>", var2);
            return Wiki.this.decode(var5.substring(var2, var6));
         } else {
            return null;
         }
      }

      public boolean isMinor() {
         return this.minor;
      }

      public boolean equals(Object var1) {
         if (!(var1 instanceof Wiki.Revision)) {
            return false;
         } else {
            return this.revid == ((Wiki.Revision)var1).revid;
         }
      }

      public boolean isBot() {
         return this.bot;
      }

      public int hashCode() {
         return ((int)this.revid << 1) - Wiki.this.hashCode();
      }

      public boolean isNew() {
         return this.rvnew;
      }

      public String getSummary() {
         return this.summary;
      }

      public boolean isSummaryDeleted() {
         return this.summaryDeleted;
      }

      public String getUser() {
         return this.user;
      }

      public boolean isUserDeleted() {
         return this.userDeleted;
      }

      public String getPage() {
         return this.title;
      }

      public long getRevid() {
         return this.revid;
      }

      public Calendar getTimestamp() {
         return this.timestamp;
      }

      public int getSize() {
         return this.size;
      }

      public int getSizeDiff() {
         return this.sizediff;
      }

      public Wiki.Revision getPrevious() throws IOException {
         return this.previous == 0L ? null : Wiki.this.getRevision(this.previous);
      }

      public Wiki.Revision getNext() throws IOException {
         return this.next == 0L ? null : Wiki.this.getRevision(this.next);
      }

      public long getRcid() {
         return this.rcid;
      }

      public String toString() {
         StringBuilder var1;
         (var1 = new StringBuilder("Revision[oldid=")).append(this.revid);
         var1.append(",page=\"");
         var1.append(this.title);
         var1.append("\",user=");
         var1.append(this.user == null ? "[hidden]" : this.user);
         var1.append(",userdeleted=");
         var1.append(this.userDeleted);
         var1.append(",timestamp=");
         var1.append(Wiki.this.calendarToTimestamp(this.timestamp));
         var1.append(",summary=\"");
         var1.append(this.summary == null ? "[hidden]" : this.summary);
         var1.append("\",summarydeleted=");
         var1.append(this.summaryDeleted);
         var1.append(",minor=");
         var1.append(this.minor);
         var1.append(",bot=");
         var1.append(this.bot);
         var1.append(",size=");
         var1.append(this.size);
         var1.append(",rcid=");
         var1.append(this.rcid == -1L ? "unset" : this.rcid);
         var1.append(",previous=");
         var1.append(this.previous);
         var1.append(",next=");
         var1.append(this.next);
         var1.append(",rollbacktoken=");
         var1.append(this.rollbacktoken == null ? "null" : this.rollbacktoken);
         var1.append("]");
         return var1.toString();
      }

      public void setRcid(long var1) {
         this.rcid = var1;
      }

      public int compareTo(Wiki.Revision var1) {
         if (this.timestamp.equals(var1.timestamp)) {
            return 0;
         } else {
            return this.timestamp.after(var1.timestamp) ? 1 : -1;
         }
      }

      public String getRollbackToken() {
         return this.rollbacktoken;
      }

      public void setRollbackToken(String var1) {
         this.rollbacktoken = var1;
      }

      public void rollback() throws IOException, LoginException {
         Wiki.this.rollback(this, false, "");
      }

      public void rollback(boolean var1, String var2) throws IOException, LoginException {
         Wiki.this.rollback(this, var1, var2);
      }
   }

   public class LogEntry implements Comparable {
      private String type;
      private String action;
      private String reason;
      private Wiki.User user;
      private String target;
      private Calendar timestamp;
      private Object details;

      protected LogEntry(String var2, String var3, String var4, Wiki.User var5, String var6, String var7, Object var8) {
         this.type = var2;
         this.action = var3;
         this.reason = var4;
         this.user = var5;
         this.target = var6;
         this.timestamp = Wiki.this.timestampToCalendar(var7, false);
         this.details = var8;
      }

      public String getType() {
         return this.type;
      }

      public String getAction() {
         return this.action;
      }

      public String getReason() {
         return this.reason;
      }

      public Wiki.User getUser() {
         return this.user;
      }

      public String getTarget() {
         return this.target;
      }

      public Calendar getTimestamp() {
         return this.timestamp;
      }

      public Object getDetails() {
         return this.details;
      }

      public int compareTo(Wiki.LogEntry var1) {
         if (this.timestamp.equals(var1.timestamp)) {
            return 0;
         } else {
            return this.timestamp.after(var1.timestamp) ? 1 : -1;
         }
      }

      public String toString() {
         StringBuilder var1;
         (var1 = new StringBuilder("LogEntry[type=")).append(this.type);
         var1.append(",action=");
         var1.append(this.action == null ? "[hidden]" : this.action);
         var1.append(",user=");
         var1.append(this.user == null ? "[hidden]" : this.user.getUsername());
         var1.append(",timestamp=");
         var1.append(Wiki.this.calendarToTimestamp(this.timestamp));
         var1.append(",target=");
         var1.append(this.target == null ? "[hidden]" : this.target);
         var1.append(",reason=\"");
         var1.append(this.reason == null ? "[hidden]" : this.reason);
         var1.append("\",details=");
         if (this.details instanceof Object[]) {
            var1.append(Arrays.asList((Object[])this.details));
         } else {
            var1.append(this.details);
         }

         var1.append("]");
         return var1.toString();
      }
   }

   public class User implements Cloneable {
      private String username;
      private String[] rights = null;
      private String[] groups = null;

      protected User(String var2) {
         this.username = var2;
      }

      public String getUsername() {
         return this.username;
      }

      public HashMap getUserInfo() throws IOException {
         String var1 = Wiki.this.fetch(Wiki.this.query + "list=users&usprop=editcount%7Cgroups%7Crights%7Cemailable%7Cblockinfo%7Cgender%7Cregistration&ususers=" + URLEncoder.encode(this.username, "UTF-8"), "getUserInfo");
         HashMap var2;
         (var2 = new HashMap(10)).put("blocked", var1.contains("blockedby=\""));
         var2.put("emailable", var1.contains("emailable=\""));
         var2.put("editcount", Integer.parseInt(Wiki.this.parseAttribute(var1, "editcount", 0)));
         var2.put("gender", Wiki.Gender.valueOf(Wiki.this.parseAttribute(var1, "gender", 0)));
         var2.put("created", Wiki.this.timestampToCalendar(Wiki.this.parseAttribute(var1, "registration", 0), true));
         ArrayList var3 = new ArrayList(50);

         int var4;
         int var5;
         for(var4 = var1.indexOf("<g>"); var4 > 0; var4 = var1.indexOf("<g>", var4)) {
            var5 = var1.indexOf("</g>", var4);
            var3.add(var1.substring(var4 + 3, var5));
            ++var4;
         }

         String[] var6 = (String[])var3.toArray(new String[var3.size()]);
         if (this.equals(Wiki.this.getCurrentUser())) {
            this.groups = var6;
         }

         var2.put("groups", var6);
         var3.clear();

         for(var5 = var1.indexOf("<r>"); var5 > 0; var5 = var1.indexOf("<r>", var5)) {
            var4 = var1.indexOf("</r>", var5);
            var3.add(var1.substring(var5 + 3, var4));
            ++var5;
         }

         var6 = (String[])var3.toArray(new String[var3.size()]);
         if (this.equals(Wiki.this.getCurrentUser())) {
            this.rights = var6;
         }

         var2.put("rights", var6);
         return var2;
      }

      public boolean isAllowedTo(String var1) throws IOException {
         if (this.rights == null) {
            this.rights = (String[])this.getUserInfo().get("rights");
         }

         String[] var2;
         int var3 = (var2 = this.rights).length;

         for(int var4 = 0; var4 < var3; ++var4) {
            if (var2[var4].equals(var1)) {
               return true;
            }
         }

         return false;
      }

      public boolean isA(String var1) throws IOException {
         if (this.groups == null) {
            this.groups = (String[])this.getUserInfo().get("groups");
         }

         String[] var2;
         int var3 = (var2 = this.groups).length;

         for(int var4 = 0; var4 < var3; ++var4) {
            if (var2[var4].equals(var1)) {
               return true;
            }
         }

         return false;
      }

      public Wiki.LogEntry[] blockLog() throws IOException {
         return Wiki.this.getLogEntries((Calendar)null, (Calendar)null, Integer.MAX_VALUE, "block", "", (Wiki.User)null, "User:" + this.username, 2);
      }

      public boolean isBlocked() throws IOException {
         return Wiki.this.getIPBlockList(this.username, (Calendar)null, (Calendar)null).length != 0;
      }

      public int countEdits() throws IOException {
         return (Integer)this.getUserInfo().get("editcount");
      }

      public Wiki.Revision[] contribs(int... var1) throws IOException {
         return Wiki.this.contribs(this.username, var1);
      }

      public Wiki.User clone() {
         try {
            return (Wiki.User)super.clone();
         } catch (CloneNotSupportedException var1) {
            return null;
         }
      }

      public boolean equals(Object var1) {
         return var1 instanceof Wiki.User && this.username.equals(((Wiki.User)var1).username);
      }

      public String toString() {
         StringBuilder var1;
         (var1 = new StringBuilder("User[username=")).append(this.username);
         var1.append("groups=");
         var1.append(this.groups != null ? Arrays.toString(this.groups) : "unset");
         var1.append("]");
         return var1.toString();
      }

      public int hashCode() {
         return (this.username.hashCode() << 1) + 1;
      }
   }

   public static enum Gender {
      male,
      female,
      unknown;
   }
}
