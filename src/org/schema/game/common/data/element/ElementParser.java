package org.schema.game.common.data.element;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.zip.GZIPInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.data.cubatoms.CubatomFlavor;
import org.schema.game.common.data.cubatoms.CubatomState;
import org.schema.game.common.data.element.annotation.ElemType;
import org.schema.game.common.updater.FileUtil;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.schine.resource.FileExt;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ElementParser {
   public static Properties properties;
   private final ObjectArrayList infoElements = new ObjectArrayList();
   private static final Object2ObjectOpenHashMap elemTypeLowerCase = new Object2ObjectOpenHashMap();
   private ElementCategory rootCategory;
   private FixedRecipes fixedRecipes;
   public static String currentName;
   private Document doc;

   public static String[] getTypeStringArray() {
      return new String[]{"General", "Power", "Terrain", "Ship", "Element", "SpaceStation", "General", "DeathStar", "Factory", "Manufacturing", "Mineral", "Hulls", "Docking", "Doors", "Light", "Effect", "Logic", "Decoration"};
   }

   public FixedRecipes getFixedRecipes() {
      return this.fixedRecipes;
   }

   public void setFixedRecipes(FixedRecipes var1) {
      this.fixedRecipes = var1;
   }

   public ObjectArrayList getInfoElements() {
      return this.infoElements;
   }

   public ElementCategory getRootCategory() {
      return this.rootCategory;
   }

   private void handleBaicElementNode(Node var1, ElementCategory var2) throws ElementParserException {
      if (!var1.hasAttributes()) {
         this.handleTypeNodeRead(var1, var2);
      } else {
         this.handleElementNode(var1, var2);
      }
   }

   private void handleElementNode(Node var1, ElementCategory var2) throws ElementParserException {
      var1.getParentNode().getNodeName();
      NamedNodeMap var3;
      if ((var3 = var1.getAttributes()) != null && var3.getLength() != 4) {
         throw new ElementParserException("Element has wrong attribute count (" + var3.getLength() + ", but should be 4)");
      } else {
         Node var4 = ElemType.parseType(var1, var3, "type");
         Node var5 = ElemType.parseType(var1, var3, "icon");
         Node var6 = ElemType.parseType(var1, var3, "textureId");
         Node var11 = ElemType.parseType(var1, var3, "name");
         String var8;
         if ((var8 = properties.getProperty(var4.getNodeValue())) == null) {
            throw new ElementParserException("The value of \"" + var4.getNodeName() + "\" has not been found in " + var11.getNodeValue() + " (" + var4.getNodeValue() + ")");
         } else {
            short var7;
            try {
               var7 = (short)Integer.parseInt(var8);
            } catch (NumberFormatException var9) {
               throw new ElementParserException("The property " + var8 + " has to be an Integer value in " + var1.getNodeName() + "<-" + var1.getParentNode().getNodeName() + "; node value: " + var4.getNodeValue());
            }

            currentName = var11.getNodeValue();
            ElementInformation var12;
            (var12 = new ElementInformation(var7, var11.getNodeValue(), var2, this.parseTextureIdAttribute(var6))).idName = var4.getNodeValue().trim();
            var12.parsed.add("BuildIcon");
            var12.parsed.add("Texture");
            var12.parsed.add("Name");
            if (var12.getId() >= 0 && var12.getId() < 2048) {
               if (var12.getTextureId(0) >= 0 && var12.getTextureId(0) < 2048) {
                  var12.setBuildIconNum(ElemType.parseInt(var5));
                  if (var12.getBuildIconNum() >= 0 && var12.getBuildIconNum() < 2048) {
                     this.parseInfoNode(var1, var12);

                     assert var12.getBlockStyle() != null : var12;

                     int var10;
                     for(var10 = 0; var10 < 6; ++var10) {
                        if (var12.getTextureId(var10) < 0) {
                           var12.normalizeTextureIds();
                           break;
                        }
                     }

                     for(var10 = 0; var10 < 6; ++var10) {
                        assert var12.getTextureId(var10) >= 0;
                     }

                     var12.recreateTextureMapping();
                     if (var12.getBlockStyle() != BlockStyle.NORMAL) {
                        var12.setOrientatable(false);
                     }

                     this.infoElements.add(var12);
                     var2.getInfoElements().add(var12);
                  } else {
                     throw new ElementParserException("Icon has to be between [0, 2048[ for " + var1.getNodeName() + " but was " + var12.getBuildIconNum());
                  }
               } else {
                  throw new ElementParserException("Texture Id has to be between [0, 2048[ for " + var1.getNodeName() + "; but was: " + var12.getTextureId(0));
               }
            } else {
               throw new ElementParserException("Element type has to be between [0, 2048[ for " + var1.getNodeName());
            }
         }
      }
   }

   private void handleRecipeProductNode(Node var1, FixedRecipe var2) throws ElementParserException {
      ArrayList var3 = new ArrayList();
      ArrayList var4 = new ArrayList();
      NamedNodeMap var5;
      if ((var5 = var1.getAttributes()) != null && var5.getLength() != 3) {
         throw new ElementParserException(var1.getNodeName() + ": Recipe has wrong attribute count (" + var5.getLength() + ", but should be 2)");
      } else {
         Node var6 = ElemType.parseType(var1, var5, "costType");
         Node var7 = ElemType.parseType(var1, var5, "costAmount");
         Node var14 = ElemType.parseType(var1, var5, "name");
         short var16;
         if (var6.getNodeValue().toLowerCase(Locale.ENGLISH).equals("credits")) {
            var16 = -1;
         } else {
            String var8;
            if ((var8 = properties.getProperty(var6.getNodeValue())) == null) {
               throw new ElementParserException("The value of \"" + var6.getNodeName() + "\" has not been found. was: " + var6.getNodeValue());
            }

            try {
               var16 = (short)Integer.parseInt(var8);
            } catch (NumberFormatException var13) {
               throw new ElementParserException("The property " + var8 + " has to be an Integer value");
            }
         }

         int var19;
         try {
            var19 = Integer.parseInt(var7.getNodeValue());
         } catch (NumberFormatException var12) {
            throw new ElementParserException("The property 'amount' " + var7.getParentNode().getNodeName() + " has to be an Integer value");
         }

         var2.costAmount = var19;
         var2.costType = var16;
         var2.name = var14.getNodeValue();
         NodeList var15 = var1.getChildNodes();

         int var17;
         for(var17 = 0; var17 < var15.getLength(); ++var17) {
            if ((var7 = var15.item(var17)).getNodeType() == 1) {
               if (!var7.getNodeName().toLowerCase(Locale.ENGLISH).equals("product")) {
                  throw new ElementParserException("All child nodes of " + var7.getNodeName() + " have to be \"product\" but is " + var7.getNodeName() + " (" + var1.getParentNode().getNodeName() + ")");
               }

               NodeList var18 = var7.getChildNodes();
               FactoryResource[] var20 = null;
               FactoryResource[] var9 = null;

               for(int var10 = 0; var10 < var18.getLength(); ++var10) {
                  Node var11;
                  if ((var11 = var18.item(var10)).getNodeType() == 1) {
                     if (!var11.getNodeName().toLowerCase(Locale.ENGLISH).equals("output") && !var11.getNodeName().toLowerCase(Locale.ENGLISH).equals("input")) {
                        throw new ElementParserException("All child nodes of " + var1.getNodeName() + " have to be \"output\" or \"input\" but is " + var11.getNodeName() + " (" + var1.getParentNode().getNodeName() + ")");
                     }

                     if (var11.getNodeName().toLowerCase(Locale.ENGLISH).equals("input")) {
                        var20 = ElemType.parseResource(var11);
                     }

                     if (var11.getNodeName().toLowerCase(Locale.ENGLISH).equals("output")) {
                        var9 = ElemType.parseResource(var11);
                     }
                  }
               }

               if (var20 == null) {
                  throw new ElementParserException("No input defined for " + var1.getNodeName() + " in (" + var1.getParentNode().getNodeName() + ")");
               }

               if (var9 == null) {
                  throw new ElementParserException("No output defined for " + var1.getNodeName() + " in (" + var1.getParentNode().getNodeName() + ")");
               }

               var3.add(var20);
               var4.add(var9);
            }
         }

         if (var3.size() != var4.size()) {
            throw new ElementParserException("Factory Parsing failed for " + var1.getNodeName() + " in (" + var1.getParentNode().getNodeName() + ")");
         } else {
            var2.recipeProducts = new FixedRecipeProduct[var3.size()];

            for(var17 = 0; var17 < var2.recipeProducts.length; ++var17) {
               var2.recipeProducts[var17] = new FixedRecipeProduct();
               var2.recipeProducts[var17].input = (FactoryResource[])var3.get(var17);
               var2.recipeProducts[var17].output = (FactoryResource[])var4.get(var17);
            }

         }
      }
   }

   private void handleRecipesNode(Node var1, FixedRecipes var2) throws ElementParserException {
      if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("recipe")) {
         var1.getChildNodes();
         FixedRecipe var3 = new FixedRecipe();
         this.handleRecipeProductNode(var1, var3);
         var2.recipes.add(var3);
      } else if (var1.getNodeType() == 1) {
         throw new ElementParserException("only 'recipe' nodes allowed in 'recipes'. this was: " + var1.getNodeName().toLowerCase(Locale.ENGLISH));
      }
   }

   private void handleTypeNode(Node var1, ElementCategory var2) throws ElementParserException {
      if (var1.getNodeType() == 1) {
         ElementCategory var3 = new ElementCategory(var1.getNodeName(), var2);
         var2.getChildren().add(var3);
         NodeList var4 = var1.getChildNodes();

         for(int var5 = 0; var5 < var4.getLength(); ++var5) {
            if (var4.item(var5).getNodeType() == 1 && !var4.item(var5).hasAttributes()) {
               this.handleTypeNode(var4.item(var5), var3);
            }
         }
      }

   }

   private void handleTypeNodeRead(Node var1, ElementCategory var2) throws ElementParserException {
      if (var1.getNodeType() == 1) {
         var2 = var2.getChild(var1.getNodeName());
         NodeList var4 = var1.getChildNodes();

         for(int var3 = 0; var3 < var4.getLength(); ++var3) {
            if (var4.item(var3).getNodeType() == 1) {
               this.handleBaicElementNode(var4.item(var3), var2);
            }
         }
      }

   }

   public void loadAndParseCustomXML(File var1, boolean var2, String var3, File var4) throws SAXException, IOException, ParserConfigurationException, ElementParserException {
      this.read(var1, var2, var3 == null ? "./data/config/BlockTypes.properties" : var3, var4);
      this.parse();
   }

   public void loadAndParseDefault(File var1) throws SAXException, IOException, ParserConfigurationException, ElementParserException {
      try {
         this.read(new FileExt("./data/config/BlockConfig.xml"), false, "./data/config/BlockTypes.properties", var1);
      } catch (ElementParserException var2) {
         var2.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var2);
      } catch (Exception var3) {
         var3.printStackTrace();
         this.read(new FileExt("./data/config/BlockConfig.xml"), true, "./data/config/BlockTypes.properties", var1);
      }

      this.parse();
   }

   private void parseHierarchy(org.w3c.dom.Element var1) {
      this.rootCategory = new ElementCategory("Element", (ElementCategory)null);
      if (!var1.getNodeName().equals("Config")) {
         throw new ElementParserException("No correct root element found");
      } else {
         NodeList var5 = var1.getChildNodes();

         for(int var2 = 0; var2 < var5.getLength(); ++var2) {
            Node var3;
            if ((var3 = var5.item(var2)).getNodeName().equals("Element")) {
               NodeList var6 = var3.getChildNodes();

               for(int var4 = 0; var4 < var6.getLength(); ++var4) {
                  if (var6.item(var4).getNodeType() == 1 && !var6.item(var4).hasAttributes()) {
                     this.handleTypeNode(var6.item(var4), this.rootCategory);
                  }
               }
            }
         }

      }
   }

   public void parse() throws ElementParserException {
      this.fixedRecipes = new FixedRecipes();
      org.w3c.dom.Element var1 = this.doc.getDocumentElement();
      this.parseHierarchy(var1);
      int var2;
      NodeList var6;
      if (var1.getNodeName().equals("Element")) {
         var6 = var1.getChildNodes();

         for(var2 = 0; var2 < var6.getLength(); ++var2) {
            this.handleBaicElementNode(var6.item(var2), this.rootCategory);
         }

      } else if (!var1.getNodeName().equals("Config")) {
         throw new ElementParserException("No correct root element found");
      } else {
         var6 = var1.getChildNodes();

         for(var2 = 0; var2 < var6.getLength(); ++var2) {
            Node var3;
            NodeList var4;
            int var5;
            if ((var3 = var6.item(var2)).getNodeName().equals("Element")) {
               var4 = var3.getChildNodes();

               for(var5 = 0; var5 < var4.getLength(); ++var5) {
                  this.handleBaicElementNode(var4.item(var5), this.rootCategory);
               }
            }

            if (var3.getNodeName().equals("Recipes")) {
               var4 = var3.getChildNodes();

               for(var5 = 0; var5 < var4.getLength(); ++var5) {
                  var3 = var4.item(var5);
                  this.handleRecipesNode(var3, this.fixedRecipes);
               }
            }
         }

      }
   }

   private static List parseList(Node var0, String var1) throws ElementParserException {
      ObjectArrayList var2 = new ObjectArrayList();
      NodeList var3 = var0.getChildNodes();

      for(int var4 = 0; var4 < var3.getLength(); ++var4) {
         Node var5;
         if ((var5 = var3.item(var4)).getNodeType() == 1) {
            if (!var5.getNodeName().equals(var1)) {
               throw new ElementParserException("All child nodes of " + var0.getNodeName() + " have to be \"" + var1 + "\" but is " + var5.getNodeName() + " (" + var0.getParentNode().getNodeName() + ")");
            }

            var2.add(var5.getTextContent());
         }
      }

      return var2;
   }

   private CubatomFlavor[] parseCubatomCompound(Node var1) {
      NodeList var2 = var1.getChildNodes();
      ArrayList var3 = new ArrayList(4);

      for(int var4 = 0; var4 < var2.getLength(); ++var4) {
         Node var5;
         if ((var5 = var2.item(var4)).getNodeType() == 1) {
            boolean var6 = false;

            for(int var7 = 0; var7 < CubatomState.values().length; ++var7) {
               if (var5.getNodeName().toLowerCase(Locale.ENGLISH).equals(CubatomState.values()[var7].name().toLowerCase(Locale.ENGLISH))) {
                  CubatomFlavor var8;
                  if ((var8 = CubatomFlavor.valueOf(var5.getTextContent().toUpperCase(Locale.ENGLISH))) == null) {
                     throw new ElementParserException("[CUBATOM] The value of " + var5.getTextContent() + " has not been found in " + var1.getParentNode().getNodeName() + " must be one of: " + Arrays.toString(CubatomFlavor.values()));
                  }

                  if (var8.getState() != CubatomState.values()[var7]) {
                     throw new ElementParserException("[CUBATOM] The value of " + var5.getTextContent() + " is not of that particular state " + var5.getNodeName() + "(" + CubatomState.values()[var4] + "); must be one of: " + Arrays.toString(CubatomState.values()[var7].getFlavors()));
                  }

                  var6 = true;
                  var3.add(var8);
               }
            }

            if (!var6) {
               throw new ElementParserException("[CUBATOM] state not found " + var5.getNodeName() + "; must be one of: " + Arrays.toString(CubatomState.values()));
            }
         }
      }

      if (var3.size() != 0 && var3.size() <= 4) {
         CubatomFlavor[] var9 = new CubatomFlavor[var3.size()];

         for(int var10 = 0; var10 < var9.length; ++var10) {
            var9[var10] = (CubatomFlavor)var3.get(var10);
         }

         Arrays.sort(var9);
         return var9;
      } else {
         throw new ElementParserException("[CUBATOM] Wrong flavor count: " + var3.size() + " must be between [1, and 4]. Node: " + var1.getParentNode().getNodeName() + "; " + var1.getParentNode().getParentNode().getNodeName());
      }
   }

   private void parseInfoNode(Node var1, ElementInformation var2) throws ElementParserException {
      NodeList var3 = var1.getChildNodes();

      for(int var4 = 0; var4 < var3.getLength(); ++var4) {
         Node var5;
         if ((var5 = var3.item(var4)).getNodeType() == 1) {
            var2.parsed.add(var5.getNodeName());
            String var6;
            if ((var6 = var5.getNodeName().toLowerCase(Locale.ENGLISH)).equals("slabreference")) {
               var6 = ElemType.SOURCE_REFERENCE.tag.toLowerCase(Locale.ENGLISH);
            }

            ElemType var7;
            if ((var7 = (ElemType)elemTypeLowerCase.get(var6)) != null) {
               var7.fac.parse(var5, var2);
            } else {
               try {
                  throw new ElementParserException(currentName + ": Element Info <" + var5.getNodeName() + "> not found. location: <" + var1.getNodeName() + "> :: " + var6);
               } catch (Exception var8) {
                  var8.printStackTrace();

                  assert false;
               }
            }
         }
      }

      if (var2.volume < 0.0F) {
         var2.volume = var2.mass;
      }

      var2.structureHP = 0;
   }

   private short[] parseTextureIdAttribute(Node var1) throws ElementParserException {
      try {
         String[] var2 = var1.getNodeValue().split(",");
         short[] var3 = new short[]{-1, -1, -1, -1, -1, -1};

         for(int var4 = 0; var4 < var2.length; ++var4) {
            var3[var4] = Short.parseShort(var2[var4].trim());
         }

         assert var3[0] >= 0;

         return var3;
      } catch (NumberFormatException var5) {
         var5.printStackTrace();
         throw new ElementParserException(currentName + ": The value of " + var1.getNodeName() + " has to be an Integer value for " + var1.getParentNode().getNodeName());
      }
   }

   public static void merge(Document var0, Document var1, File var2) {
      ElementParser var3;
      (var3 = new ElementParser()).doc = var0;
      var3.parse();
      ElementParser var16;
      (var16 = new ElementParser()).doc = var1;
      var16.parse();

      for(int var17 = 0; var17 < var3.infoElements.size(); ++var17) {
         boolean var4 = false;
         ElementInformation var5 = (ElementInformation)var3.infoElements.get(var17);
         System.err.println("[MERGE] Processing custom entry: " + var5);

         for(int var6 = 0; var6 < var16.infoElements.size(); ++var6) {
            ElementInformation var7 = (ElementInformation)var16.infoElements.get(var6);
            if (var5.getId() == var7.getId()) {
               Iterator var18 = var5.parsed.iterator();

               while(var18.hasNext()) {
                  String var19 = (String)var18.next();
                  Field[] var8 = ElementInformation.class.getFields();

                  for(int var9 = 0; var9 < var8.length; ++var9) {
                     Field var10;
                     (var10 = var8[var9]).setAccessible(true);
                     org.schema.game.common.data.element.annotation.Element var11;
                     if ((var11 = (org.schema.game.common.data.element.annotation.Element)var10.getAnnotation(org.schema.game.common.data.element.annotation.Element.class)) != null && var11.parser().tag.toLowerCase(Locale.ENGLISH).equals(var19.toLowerCase(Locale.ENGLISH))) {
                        try {
                           System.err.println(var7.getName() + "(" + var7.getId() + ") buildIcon " + var7.getBuildIconNum() + " MERGING " + var10.getName());
                           var10.set(var7, var10.get(var5));
                           System.err.println(var7.getName() + "(" + var7.getId() + ") buildIcon " + var7.getBuildIconNum() + " AFTER MERGING " + var10.getName());
                        } catch (IllegalArgumentException var14) {
                           var14.printStackTrace();
                        } catch (IllegalAccessException var15) {
                           var15.printStackTrace();
                        }
                     }
                  }
               }

               var4 = true;
               break;
            }
         }

         if (!var4) {
            System.err.println("[MERGE] NO EXISTING ENTRY FOUND FOR: " + var5.getId() + "; " + var5 + "; ADDING AS NEW TO ENTRIES");
            var16.infoElements.add(var5);
         } else {
            System.err.println("[MERGE] EXISTING ENTRY WAS MODIFIED WITH: " + var5);
         }
      }

      System.err.println("[CONFIG] Merging Categories");
      var16.rootCategory.merge(var3.rootCategory);
      System.err.println("[CONFIG] Writing merged config");
      ElementKeyMap.writeDocument(var2, var16.rootCategory, var16.fixedRecipes);

      try {
         ElementKeyMap.configHash = FileUtil.getSha1Checksum(var2);
      } catch (NoSuchAlgorithmException var12) {
         var12.printStackTrace();
      } catch (IOException var13) {
         var13.printStackTrace();
      }
   }

   public static void mergeApplyFromTargetField(Document var0, Document var1, File var2, String... var3) {
      ElementParser var4;
      (var4 = new ElementParser()).doc = var0;
      var4.parse();
      ElementParser var19;
      (var19 = new ElementParser()).doc = var1;
      var19.parse();

      for(int var20 = 0; var20 < var4.infoElements.size(); ++var20) {
         boolean var5 = false;
         ElementInformation var6 = (ElementInformation)var4.infoElements.get(var20);

         for(int var7 = 0; var7 < var19.infoElements.size(); ++var7) {
            ElementInformation var8 = (ElementInformation)var19.infoElements.get(var7);
            if (var6.getId() == var8.getId()) {
               Iterator var21 = var6.parsed.iterator();

               while(var21.hasNext()) {
                  String var22 = (String)var21.next();
                  Field[] var9 = ElementInformation.class.getFields();

                  for(int var10 = 0; var10 < var9.length; ++var10) {
                     Field var11;
                     (var11 = var9[var10]).setAccessible(true);
                     org.schema.game.common.data.element.annotation.Element var12 = (org.schema.game.common.data.element.annotation.Element)var11.getAnnotation(org.schema.game.common.data.element.annotation.Element.class);
                     String[] var13 = var3;
                     int var14 = var3.length;

                     for(int var15 = 0; var15 < var14; ++var15) {
                        String var16 = var13[var15];
                        if (var12 != null && var12.parser().tag.toLowerCase(Locale.ENGLISH).equals(var16.toLowerCase(Locale.ENGLISH)) && var12.parser().tag.toLowerCase(Locale.ENGLISH).equals(var22.toLowerCase(Locale.ENGLISH))) {
                           try {
                              System.err.println("SETTING FIELD: " + var12.parser().tag);
                              var11.set(var8, var11.get(var6));
                           } catch (IllegalArgumentException var17) {
                              var17.printStackTrace();
                           } catch (IllegalAccessException var18) {
                              var18.printStackTrace();
                           }
                        }
                     }
                  }
               }

               var5 = true;
               break;
            }
         }

         if (!var5) {
            var19.infoElements.add(var6);
         }
      }

      System.err.println("[CONFIG] Merging Categories");
      var19.rootCategory.merge(var4.rootCategory);
      System.err.println("[CONFIG] Writing merged config " + var2.getAbsolutePath());
      ElementKeyMap.writeDocument(var2, var19.rootCategory, var19.fixedRecipes);
   }

   public static void main(String[] var0) throws IOException, ParserConfigurationException, SAXException {
      ElementParser var2 = new ElementParser();
      File var1 = new File("./Merge.xml");
      var2.mergeOnField("Animated", new FileExt("./data/config/BlockConfig.xml"), false, "./data/config/BlockTypes.properties", var1);
   }

   private void mergeOnField(String var1, File var2, boolean var3, String var4, File var5) throws IOException, ParserConfigurationException, SAXException {
      properties = new Properties();
      BufferedInputStream var11 = new BufferedInputStream(new FileInputStream(var4));
      properties.load(var11);
      var11.close();
      DocumentBuilder var12 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      ElementKeyMap.properties = properties;
      ElementKeyMap.propertiesPath = var4;
      if (var5 != null && var5.exists()) {
         if (!var3) {
            this.doc = var12.parse(new BufferedInputStream(new FileInputStream(var2), 4096));
         } else {
            GZIPInputStream var6 = new GZIPInputStream(new BufferedInputStream(new FileInputStream(var2), 4096));
            this.doc = var12.parse(var6);
            var6.close();
         }

         System.err.println("[CONFIG] CONFIG FILE IMPORT FOUND: " + var5.getAbsolutePath());
         mergeApplyFromTargetField(var12.parse(var5), this.doc, var2, "Animated");
         System.err.println("[CONFIG] CONFIG FILE IMPORT DONE AND MERGED");
      }

      long var13 = System.currentTimeMillis();
      if (!var3) {
         this.doc = var12.parse(new BufferedInputStream(new FileInputStream(var2), 4096));
      } else {
         GZIPInputStream var8 = new GZIPInputStream(new BufferedInputStream(new FileInputStream(var2), 4096));
         this.doc = var12.parse(var8);
         var8.close();
      }

      long var14 = System.currentTimeMillis() - var13;
      System.err.println("[BLOCKCONFIG] READING TOOK " + var14 + "ms");

      try {
         ElementKeyMap.propertiesHash = FileUtil.getSha1Checksum(var4);
         ElementKeyMap.configHash = FileUtil.getSha1Checksum(var2);
      } catch (NoSuchAlgorithmException var10) {
         var10.printStackTrace();
      }
   }

   public void read(File var1, boolean var2, String var3, File var4) throws SAXException, IOException, ParserConfigurationException {
      properties = new Properties();
      BufferedInputStream var5 = new BufferedInputStream(new FileInputStream(var3));
      properties.load(var5);
      var5.close();
      DocumentBuilder var11 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      ElementKeyMap.properties = properties;
      ElementKeyMap.propertiesPath = var3;
      ElementKeyMap.configFile = var1;

      try {
         ElementKeyMap.propertiesHash = FileUtil.getSha1Checksum(var3);
         ElementKeyMap.configHash = FileUtil.getSha1Checksum(var1);
      } catch (NoSuchAlgorithmException var10) {
         var10.printStackTrace();
      }

      if (var4 != null && var4.exists()) {
         if (!var2) {
            BufferedInputStream var6 = new BufferedInputStream(new FileInputStream(var1), 4096);
            this.doc = var11.parse(var6);
            var6.close();
         } else {
            GZIPInputStream var12 = new GZIPInputStream(new BufferedInputStream(new FileInputStream(var1), 4096));
            this.doc = var11.parse(var12);
            var12.close();
         }

         System.err.println("[CONFIG] CONFIG FILE IMPORT FOUND: " + var4.getAbsolutePath());
         merge(var11.parse(var4), this.doc, var1);
         System.err.println("[CONFIG] CONFIG FILE IMPORT DONE AND MERGED");
      }

      long var13 = System.currentTimeMillis();
      if (!var2) {
         this.doc = var11.parse(new BufferedInputStream(new FileInputStream(var1), 4096));
      } else {
         GZIPInputStream var8 = new GZIPInputStream(new BufferedInputStream(new FileInputStream(var1), 4096));
         this.doc = var11.parse(var8);
         var8.close();
      }

      long var14 = System.currentTimeMillis() - var13;
      System.err.println("[BLOCKCONFIG] READING TOOK " + var14 + "ms");
   }

   static {
      ElemType[] var0;
      int var1 = (var0 = ElemType.values()).length;

      for(int var2 = 0; var2 < var1; ++var2) {
         ElemType var3 = var0[var2];
         elemTypeLowerCase.put(var3.tag.toLowerCase(Locale.ENGLISH), var3);
      }

   }
}
