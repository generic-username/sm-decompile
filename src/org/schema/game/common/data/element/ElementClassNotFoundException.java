package org.schema.game.common.data.element;

public class ElementClassNotFoundException extends RuntimeException {
   private static final long serialVersionUID = 1L;

   public ElementClassNotFoundException(short var1) {
      super("class for type " + var1 + " not found");
   }
}
