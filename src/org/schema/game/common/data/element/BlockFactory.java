package org.schema.game.common.data.element;

public class BlockFactory {
   public short enhancer;
   public FactoryResource[][] input;
   public FactoryResource[][] output;

   public String toString() {
      return this.input != null ? "Block Factory Products: " + this.input.length : "INPUT";
   }
}
