package org.schema.game.common.data.element.meta.weapon;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.damage.effects.InterEffectHandler;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.data.element.beam.BeamReloadCallback;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class SniperRifle extends Weapon {
   public int damage = 100;
   public float speed = 70.0F;
   public float distance = 400.0F;
   public float reload = 5.0F;
   public BeamReloadCallback reloadCallback = new BeamReloadCallback() {
      private long nextShot;

      public void setShotReloading(long var1) {
         this.nextShot = System.currentTimeMillis() + var1;
      }

      public boolean canUse(long var1, boolean var3) {
         return this.getNextShoot() < var1;
      }

      public boolean isInitializing(long var1) {
         return false;
      }

      public long getNextShoot() {
         return this.nextShot;
      }

      public long getCurrentReloadTime() {
         return (long)(SniperRifle.this.reload * 1000.0F);
      }

      public boolean consumePower(float var1) {
         return true;
      }

      public boolean canConsumePower(float var1) {
         return true;
      }

      public double getPower() {
         return 1.0D;
      }

      public boolean isUsingPowerReactors() {
         return true;
      }

      public void flagBeamFiredWithoutTimeout() {
      }
   };
   private Vector4f color = new Vector4f(Math.min(1.0F, (float)Math.random() + (float)Math.random()), Math.min(1.0F, (float)Math.random() + (float)Math.random()), Math.min(1.0F, (float)Math.random() + (float)Math.random()), 1.0F);
   private Vector3f dirTmp = new Vector3f();

   public SniperRifle(int var1) {
      super(var1, Weapon.WeaponSubType.SNIPER_RIFLE.type);
   }

   public void deserialize(DataInputStream var1) throws IOException {
      this.damage = var1.readInt();
      this.speed = var1.readFloat();
      this.color.set(var1.readFloat(), var1.readFloat(), var1.readFloat(), var1.readFloat());
      this.distance = var1.readFloat();
      this.reload = var1.readFloat();
   }

   public String getName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_SNIPERRIFLE_0;
   }

   public void fromTag(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.damage = (Integer)var2[0].getValue();
      this.speed = (Float)var2[1].getValue();
      this.reload = (Float)var2[2].getValue();
      this.color = (Vector4f)var2[3].getValue();
      this.distance = (Float)var2[4].getValue();
   }

   public Tag getBytesTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.INT, (String)null, this.damage), new Tag(Tag.Type.FLOAT, (String)null, this.speed), new Tag(Tag.Type.FLOAT, (String)null, this.reload), new Tag(Tag.Type.VECTOR4f, (String)null, this.color), new Tag(Tag.Type.FLOAT, (String)null, this.distance), FinishTag.INST});
   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.writeInt(this.damage);
      var1.writeFloat(this.speed);
      var1.writeFloat(this.color.x);
      var1.writeFloat(this.color.y);
      var1.writeFloat(this.color.z);
      var1.writeFloat(this.color.w);
      var1.writeFloat(this.distance);
      var1.writeFloat(this.reload);
   }

   public float hasZoomFunction() {
      return 0.25F;
   }

   public void fire(AbstractCharacter var1, AbstractOwnerState var2, boolean var3, boolean var4, Timer var5) {
      if (var3) {
         Vector3f var6 = var2.getForward(this.dirTmp);
         this.fire(var1, var2, var6, var3, var4, var5);
      }

   }

   public void fire(AbstractCharacter var1, AbstractOwnerState var2, Vector3f var3, boolean var4, boolean var5, Timer var6) {
      var3.scale(this.speed);
      if (var2.isOnServer() || ((GameClientState)var2.getState()).getCurrentSectorId() == var1.getSectorId()) {
         if (var1 instanceof PlayerCharacter) {
            ((PlayerCharacter)var1).shootSniperBeam((ControllerStateUnit)((PlayerState)var1.getOwnerState()).getControllerState().getUnits().iterator().next(), (float)this.damage, this.reload, this.distance, this, var4, var5);
         }

      }
   }

   protected String toDetailedString() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_SNIPERRIFLE_1, this.damage, this.speed, this.color, this.distance);
   }

   public Vector4f getColor() {
      return this.color;
   }

   public void setColor(Vector4f var1) {
      this.color = var1;
   }

   public boolean equalsObject(MetaObject var1) {
      return super.equalsTypeAndSubId(var1) && this.damage == ((SniperRifle)var1).damage && this.color.equals(((SniperRifle)var1).color) && this.speed == ((SniperRifle)var1).speed;
   }

   protected void setupEffectSet(InterEffectSet var1) {
      var1.setStrength(InterEffectHandler.InterEffectType.KIN, 0.5F);
      var1.setStrength(InterEffectHandler.InterEffectType.HEAT, 0.0F);
      var1.setStrength(InterEffectHandler.InterEffectType.EM, 0.5F);
   }
}
