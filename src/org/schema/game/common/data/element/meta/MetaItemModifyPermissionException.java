package org.schema.game.common.data.element.meta;

public class MetaItemModifyPermissionException extends Exception {
   private static final long serialVersionUID = 1L;

   public MetaItemModifyPermissionException(String var1) {
      super(var1);
   }
}
