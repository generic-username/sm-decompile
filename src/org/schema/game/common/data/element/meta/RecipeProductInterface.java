package org.schema.game.common.data.element.meta;

import org.schema.game.common.data.element.FactoryResource;

public interface RecipeProductInterface {
   FactoryResource[] getInputResource();

   FactoryResource[] getOutputResource();
}
