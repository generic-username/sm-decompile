package org.schema.game.common.data.element.meta;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.SimplePlayerCommands;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.resource.tag.Tag;

public class Helmet extends MetaObject {
   public static final byte STANDARD = 0;
   private byte type = 0;
   private long lastHelmetRequest;

   public Helmet(int var1) {
      super(var1);
   }

   public void deserialize(DataInputStream var1) throws IOException {
      this.type = var1.readByte();
   }

   public void fromTag(Tag var1) {
      this.type = (Byte)var1.getValue();
   }

   public Tag getBytesTag() {
      return new Tag(Tag.Type.BYTE, (String)null, this.type);
   }

   public DialogInput getEditDialog(GameClientState var1, AbstractControlManager var2, Inventory var3) {
      return new PlayerGameOkCancelInput("Helmet_editDialog", var1, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_HELMET_0, this.toDetailedString()) {
         public boolean isOccluded() {
            return false;
         }

         public void onDeactivate() {
         }

         public void pressedOK() {
            this.deactivate();
         }
      };
   }

   public String getName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_HELMET_1;
   }

   public MetaObjectManager.MetaObjectType getObjectBlockType() {
      return MetaObjectManager.MetaObjectType.HELMET;
   }

   public int getPermission() {
      return 0;
   }

   public boolean isValidObject() {
      return true;
   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.writeByte(this.type);
   }

   public void onMouseAction(AbstractCharacter var1, ControllerStateUnit var2, int var3, int var4, Timer var5) {
      if (var2.isMouseButtonDown(0) && System.currentTimeMillis() - this.lastHelmetRequest > 4000L) {
         var2.playerState.sendSimpleCommand(SimplePlayerCommands.PUT_ON_HELMET);
         this.lastHelmetRequest = System.currentTimeMillis();
      }

   }

   private String toDetailedString() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_HELMET_2;
   }

   public String toString() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_HELMET_3;
   }

   public boolean equalsObject(MetaObject var1) {
      return super.equalsTypeAndSubId(var1);
   }
}
