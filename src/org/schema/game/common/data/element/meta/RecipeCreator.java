package org.schema.game.common.data.element.meta;

import it.unimi.dsi.fastutil.objects.Object2FloatOpenHashMap;
import it.unimi.dsi.fastutil.shorts.Short2IntOpenHashMap;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.data.element.DockingElement;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.HullElement;
import org.schema.game.common.data.element.LightElement;
import org.schema.game.common.data.element.factory.ManufacturingElement;
import org.schema.game.common.data.element.ship.ShipElement;
import org.schema.game.common.data.element.terrain.MineralElement;
import org.schema.game.common.data.element.terrain.TerrainElement;
import org.schema.game.common.data.world.Universe;

public class RecipeCreator {
   public static Recipe getRecipeFor(short var0) throws InvalidFactoryParameterException {
      new ArrayList();
      ElementInformation var1 = ElementKeyMap.getInfo(var0);
      Object2FloatOpenHashMap var2 = new Object2FloatOpenHashMap();
      if (var0 == 123 || var0 == 2 || var0 == 56) {
         var2.put(ManufacturingElement.class, 1.0F);
         var2.put(MineralElement.class, 0.3F);
         var2.put(HullElement.class, 0.1F);
         var2.put(TerrainElement.class, 0.1F);
      }

      int var9;
      if (ShipElement.class.equals(var1.getType())) {
         var2.put(ManufacturingElement.class, 2.0F);
         var2.put(MineralElement.class, 0.2F);
         var2.put(HullElement.class, 0.7F);
         var2.put(TerrainElement.class, 0.1F);
         var9 = 4;
      } else if (MineralElement.class.equals(var1.getType())) {
         var2.put(TerrainElement.class, 1.0F);
         var9 = 2;
      } else if (SpaceStation.class.equals(var1.getType())) {
         var2.put(ManufacturingElement.class, 2.0F);
         var2.put(MineralElement.class, 0.2F);
         var2.put(HullElement.class, 0.7F);
         var2.put(TerrainElement.class, 0.1F);
         var9 = 4;
      } else if (HullElement.class.equals(var1.getType())) {
         var2.put(MineralElement.class, 2.0F);
         var2.put(TerrainElement.class, 0.5F);
         var9 = 2;
      } else if (LightElement.class.equals(var1.getType())) {
         var2.put(ManufacturingElement.class, 2.0F);
         var2.put(MineralElement.class, 0.2F);
         var9 = 2;
      } else if (DockingElement.class.equals(var1.getType())) {
         var2.put(ManufacturingElement.class, 2.0F);
         var2.put(MineralElement.class, 0.2F);
         var9 = 3;
      } else {
         var2.put(MineralElement.class, 0.5F);
         var2.put(TerrainElement.class, 0.5F);
         var9 = 3;
      }

      float var3 = 0.0F;

      Float var5;
      for(Iterator var4 = var2.values().iterator(); var4.hasNext(); var3 += var5 * var5) {
         var5 = (Float)var4.next();
      }

      float var14 = (float)(1.0D / Math.sqrt((double)var3));
      Iterator var20 = var2.entrySet().iterator();

      while(var20.hasNext()) {
         Entry var12 = (Entry)var20.next();
         var2.put(var12.getKey(), (Float)var12.getValue() * var14);
      }

      Universe.getRandom().nextInt(10);
      ShortOpenHashSet var13 = new ShortOpenHashSet();
      Iterator var10 = var2.entrySet().iterator();

      int var24;
      while(var10.hasNext()) {
         Entry var17 = (Entry)var10.next();
         ShortArrayList var21 = new ShortArrayList();
         Iterator var6 = ElementKeyMap.keySet.iterator();

         short var7;
         while(var6.hasNext()) {
            ElementInformation var8 = ElementKeyMap.getInfo(var7 = (Short)var6.next());
            if (var7 != var0 && var8.getType().equals(var17.getKey()) && var8.isInRecipe()) {
               var21.add(var7);
            }
         }

         while((double)var13.size() < Math.ceil((double)((Float)var17.getValue() * 10.0F))) {
            var24 = Universe.getRandom().nextInt(var21.size() - 1);
            var7 = var21.get(var24);
            if (Universe.getRandom().nextFloat() < Math.max(0.05F, (Float)var17.getValue())) {
               var13.add(var7);
            }
         }
      }

      int var11 = var9 - 1;

      for(int var18 = 0; var18 < var11 && Universe.getRandom().nextInt(2) == 0; ++var18) {
         --var9;
      }

      System.err.println("[RECIPE] slots used: " + var9);
      ShortArrayList var22;
      (var22 = new ShortArrayList()).addAll(var13);
      Short2IntOpenHashMap var23 = new Short2IntOpenHashMap();

      for(var24 = 0; var24 < var9; ++var24) {
         int var26 = Universe.getRandom().nextInt(var22.size() - 1);
         short var28 = var22.get(var26);
         var11 = 1;

         for(int var15 = 1; var15 < 4; ++var15) {
            if (Universe.getRandom().nextInt(var15) == 0) {
               var11 += var15;
            }
         }

         var23.add(var28, var11);
      }

      Recipe var25 = (Recipe)MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.RECIPE, (short)-1, true);
      short[] var27 = new short[var23.size()];
      short[] var29 = new short[var23.size()];
      var11 = 0;

      for(Iterator var19 = var23.short2IntEntrySet().iterator(); var19.hasNext(); ++var11) {
         it.unimi.dsi.fastutil.shorts.Short2IntMap.Entry var16 = (it.unimi.dsi.fastutil.shorts.Short2IntMap.Entry)var19.next();
         var27[var11] = var16.getShortKey();
         var29[var11] = (short)var16.getIntValue();
      }

      var25.setRecipe(var27, var29, new short[]{var0}, new short[]{1});
      return var25;
   }
}
