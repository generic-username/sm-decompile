package org.schema.game.common.data.element.meta.weapon;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.damage.acid.AcidDamageFormula;
import org.schema.game.common.controller.damage.effects.InterEffectHandler;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class LaserWeapon extends Weapon {
   public int damage = 10;
   float speed = 120.0F;
   float distance = 200.0F;
   private int reload = 150;
   private long vol_lastShot = 0L;
   private float projectileWidth = 1.0F;
   private float impactForce = 0.0F;
   private int penetrationDepth = 9;
   private Vector4f color = new Vector4f(Math.min(1.0F, (float)Math.random() + (float)Math.random()), Math.min(1.0F, (float)Math.random() + (float)Math.random()), Math.min(1.0F, (float)Math.random() + (float)Math.random()), 1.0F);

   public LaserWeapon(int var1) {
      super(var1, Weapon.WeaponSubType.LASER.type);
   }

   public String getName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_LASERWEAPON_0;
   }

   public void deserialize(DataInputStream var1) throws IOException {
      this.damage = var1.readInt();
      this.speed = var1.readFloat();
      this.color.set(var1.readFloat(), var1.readFloat(), var1.readFloat(), var1.readFloat());
   }

   public void fromTag(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.damage = (Integer)var2[0].getValue();
      this.damage = 0;
      this.speed = (Float)var2[1].getValue();
      this.reload = (Integer)var2[2].getValue();
      if (var2[3].getType() == Tag.Type.VECTOR4f) {
         this.color = (Vector4f)var2[3].getValue();
      }

   }

   public Tag getBytesTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.INT, (String)null, this.damage), new Tag(Tag.Type.FLOAT, (String)null, this.speed), new Tag(Tag.Type.INT, (String)null, this.reload), new Tag(Tag.Type.VECTOR4f, (String)null, this.color), FinishTag.INST});
   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.writeInt(this.damage);
      var1.writeFloat(this.speed);
      var1.writeFloat(this.color.x);
      var1.writeFloat(this.color.y);
      var1.writeFloat(this.color.z);
      var1.writeFloat(this.color.w);
   }

   public void fire(AbstractCharacter var1, AbstractOwnerState var2, boolean var3, boolean var4, Timer var5) {
      Vector3f var6 = var2.getForward(new Vector3f());
      this.fire(var1, var2, var6, var3, var4, var5);
   }

   public void fire(AbstractCharacter var1, AbstractOwnerState var2, Vector3f var3, boolean var4, boolean var5, Timer var6) {
      var3.scale(this.speed);
      if (var2.isOnServer() || ((GameClientState)var2.getState()).getCurrentSectorId() == var1.getSectorId()) {
         AcidDamageFormula.AcidFormulaType var9 = AcidDamageFormula.AcidFormulaType.EQUAL_DIST;
         long var7;
         if ((var7 = System.currentTimeMillis()) - this.vol_lastShot > (long)this.reload) {
            var1.getParticleController().addProjectile(var1, new Vector3f(var1.getShoulderWorldTransform().origin), var3, (float)this.damage, this.distance, var9.ordinal(), this.projectileWidth, this.penetrationDepth, this.impactForce, this.getWeaponUsableId(), this.color);
            this.vol_lastShot = var7;
         }

      }
   }

   protected String toDetailedString() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_LASERWEAPON_1, this.damage, this.speed, this.reload, this.color);
   }

   public Vector4f getColor() {
      return this.color;
   }

   public void setColor(Vector4f var1) {
      this.color = var1;
   }

   public boolean equalsObject(MetaObject var1) {
      return super.equalsTypeAndSubId(var1) && this.damage == ((LaserWeapon)var1).damage && this.color.equals(((LaserWeapon)var1).color) && this.speed == ((LaserWeapon)var1).speed;
   }

   protected void setupEffectSet(InterEffectSet var1) {
      var1.setStrength(InterEffectHandler.InterEffectType.KIN, 0.8F);
      var1.setStrength(InterEffectHandler.InterEffectType.HEAT, 0.0F);
      var1.setStrength(InterEffectHandler.InterEffectType.EM, 0.2F);
   }
}
