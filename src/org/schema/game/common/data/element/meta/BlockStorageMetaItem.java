package org.schema.game.common.data.element.meta;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.client.controller.PlayerBlockStorageMetaDialog;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButton;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class BlockStorageMetaItem extends MetaObject {
   public ElementCountMap storage = new ElementCountMap();

   public BlockStorageMetaItem(int var1) {
      super(var1);
   }

   public void deserialize(DataInputStream var1) throws IOException {
      this.storage.resetAll();
      this.storage.deserialize(var1);
   }

   public void fromTag(Tag var1) {
      this.storage.resetAll();
      Tag[] var2 = (Tag[])var1.getValue();
      this.storage.readByteArray((byte[])var2[0].getValue());
   }

   public Tag getBytesTag() {
      byte[] var1 = this.storage.getByteArray();
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE_ARRAY, (String)null, var1), FinishTag.INST});
   }

   public DialogInput getEditDialog(GameClientState var1, AbstractControlManager var2, Inventory var3) {
      return new PlayerBlockStorageMetaDialog(var1, this, var3);
   }

   protected GUIHorizontalButton[] getButtons(GameClientState var1, Inventory var2) {
      return new GUIHorizontalButton[]{this.getCustomEditButton(var1, var2, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_BLOCKSTORAGEMETAITEM_0), this.getDeleteButton(var1, var2)};
   }

   public String getName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_BLOCKSTORAGEMETAITEM_1;
   }

   public MetaObjectManager.MetaObjectType getObjectBlockType() {
      return MetaObjectManager.MetaObjectType.BLOCK_STORAGE;
   }

   public int getPermission() {
      return 1;
   }

   public boolean isValidObject() {
      return true;
   }

   public void serialize(DataOutputStream var1) throws IOException {
      this.storage.serialize(var1);
   }

   public String toString() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_BLOCKSTORAGEMETAITEM_2;
   }

   public boolean equalsObject(MetaObject var1) {
      return super.equalsTypeAndSubId(var1) && this.storage.equals(((BlockStorageMetaItem)var1).storage);
   }
}
