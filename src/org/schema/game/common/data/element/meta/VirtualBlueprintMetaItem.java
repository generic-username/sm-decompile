package org.schema.game.common.data.element.meta;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.common.FastMath;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ShipyardManagerContainerInterface;
import org.schema.game.common.controller.elements.shipyard.ShipyardCollectionManager;
import org.schema.game.common.controller.io.SegmentDataFileUtils;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButton;
import org.schema.schine.input.InputState;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class VirtualBlueprintMetaItem extends MetaObject {
   public static final int MAX_LENGTH = 50;
   public String virtualName = "no name";
   public String UID = "ERROR_undef";
   long lastLoadAction;

   public VirtualBlueprintMetaItem(int var1) {
      super(var1);
   }

   public void deserialize(DataInputStream var1) throws IOException {
      this.UID = var1.readUTF();
      this.virtualName = var1.readUTF();
   }

   public void fromTag(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.UID = (String)var2[0].getValue();
      this.virtualName = (String)var2[1].getValue();
   }

   public Tag getBytesTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.STRING, (String)null, this.UID), new Tag(Tag.Type.STRING, (String)null, this.virtualName), FinishTag.INST});
   }

   public DialogInput getInfoDialog(GameClientState var1, AbstractControlManager var2, Inventory var3) {
      PlayerGameOkCancelInput var4;
      (var4 = new PlayerGameOkCancelInput("METAINFO_" + this.getObjectBlockID(), var1, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_VIRTUALBLUEPRINTMETAITEM_0, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_VIRTUALBLUEPRINTMETAITEM_1) {
         public void onDeactivate() {
         }

         public boolean isOccluded() {
            return false;
         }

         public void pressedOK() {
            this.deactivate();
         }
      }).getInputPanel().setCancelButton(false);
      return var4;
   }

   public DialogInput getEditDialog(GameClientState var1, final AbstractControlManager var2, Inventory var3) {
      return new PlayerGameTextInput("ENTER_NAME", var1, 50, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_VIRTUALBLUEPRINTMETAITEM_2, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_VIRTUALBLUEPRINTMETAITEM_3) {
         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1, TextCallback var2x, String var3) throws PrefixNotFoundException {
            return null;
         }

         public void onFailedTextCheck(String var1) {
         }

         public boolean isOccluded() {
            return false;
         }

         public void onDeactivate() {
            if (var2 != null) {
               var2.suspend(false);
            }

         }

         public boolean onInput(String var1) {
            VirtualBlueprintMetaItem var2x;
            (var2x = new VirtualBlueprintMetaItem(VirtualBlueprintMetaItem.this.getId())).virtualName = var1 != null ? var1 : "";
            var2x.UID = VirtualBlueprintMetaItem.this.UID;

            try {
               this.getState().getMetaObjectManager().modifyRequest(this.getState().getController().getClientChannel().getNetworkObject(), var2x);
            } catch (MetaItemModifyPermissionException var3) {
               this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_VIRTUALBLUEPRINTMETAITEM_4, 0.0F);
            }

            return true;
         }
      };
   }

   public void drawPossibleOverlay(GUIOverlay var1, Inventory var2) {
      if (this.isInventoryLocked(var2)) {
         int var3 = (int)FastMath.floor(FastMath.clamp(2.0F, 2.0F, 10.0F));
         var1.setSpriteSubIndex(var3);
         var1.draw();
      }

   }

   public String getName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_VIRTUALBLUEPRINTMETAITEM_5;
   }

   public boolean drawUsingReloadIcon() {
      return true;
   }

   public MetaObjectManager.MetaObjectType getObjectBlockType() {
      return MetaObjectManager.MetaObjectType.VIRTUAL_BLUEPRINT;
   }

   public int getPermission() {
      return 1;
   }

   public boolean isValidObject() {
      return true;
   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.writeUTF(this.UID);
      var1.writeUTF(this.virtualName);
   }

   protected GUIHorizontalButton getLoadDesignButton(final GameClientState var1, final Inventory var2) {
      return new GUIHorizontalButton(var1, GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_VIRTUALBLUEPRINTMETAITEM_6, new GUICallback() {
         public void callback(GUIElement var1x, MouseEvent var2x) {
            ShipyardCollectionManager var3;
            if (var2x.pressedLeftMouse() && (var3 = VirtualBlueprintMetaItem.this.getShipyardFromInventory(var2)) != null) {
               VirtualBlueprintMetaItem.this.lastLoadAction = System.currentTimeMillis();
               var3.sendShipyardCommandToServer(var1.getPlayer().getFactionId(), ShipyardCollectionManager.ShipyardCommandType.LOAD_DESIGN, VirtualBlueprintMetaItem.this.getId());
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }, (GUIActiveInterface)null, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            ShipyardCollectionManager var2x;
            return (var2x = VirtualBlueprintMetaItem.this.getShipyardFromInventory(var2)) != null && var2x.getCurrentDocked() == null && var2x.getCurrentDesignObject() == null && var2x.isCommandUsable(ShipyardCollectionManager.ShipyardCommandType.LOAD_DESIGN) && System.currentTimeMillis() - VirtualBlueprintMetaItem.this.lastLoadAction > 3000L;
         }
      });
   }

   protected GUIHorizontalButton getUnloadDesignButton(final GameClientState var1, final Inventory var2) {
      return new GUIHorizontalButton(var1, GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_VIRTUALBLUEPRINTMETAITEM_7, new GUICallback() {
         public void callback(GUIElement var1x, MouseEvent var2x) {
            ShipyardCollectionManager var3;
            if (var2x.pressedLeftMouse() && (var3 = VirtualBlueprintMetaItem.this.getShipyardFromInventory(var2)) != null) {
               var3.sendShipyardCommandToServer(var1.getPlayer().getFactionId(), ShipyardCollectionManager.ShipyardCommandType.UNLOAD_DESIGN);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }, (GUIActiveInterface)null, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            ShipyardCollectionManager var2x;
            return (var2x = VirtualBlueprintMetaItem.this.getShipyardFromInventory(var2)) != null && var2x.getCurrentDocked() != null && var2x.getCurrentDocked().isVirtualBlueprint() && var2x.getCurrentDesignObject() == VirtualBlueprintMetaItem.this && var2x.isCommandUsable(ShipyardCollectionManager.ShipyardCommandType.UNLOAD_DESIGN);
         }
      });
   }

   protected GUIHorizontalButton getDeleteButton(final GameClientState var1, final Inventory var2) {
      return new GUIHorizontalButton(var1, GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, "DISPOSE", new GUICallback() {
         public void callback(GUIElement var1x, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               (new PlayerGameOkCancelInput("CONFIRM", var1, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_VIRTUALBLUEPRINTMETAITEM_9, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_VIRTUALBLUEPRINTMETAITEM_10) {
                  public boolean isOccluded() {
                     return false;
                  }

                  public void onDeactivate() {
                  }

                  public void pressedOK() {
                     if (var2.getSlotFromMetaId(VirtualBlueprintMetaItem.this.getId()) >= 0) {
                        var2.removeMetaItem(VirtualBlueprintMetaItem.this);
                     }

                     this.deactivate();
                  }
               }).activate();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }, (GUIActiveInterface)null, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return VirtualBlueprintMetaItem.this.canDisposeItem(var2);
         }
      });
   }

   protected void onDelete(boolean var1, StateInterface var2) {
      if (var1) {
         Sendable var4;
         if ((var4 = (Sendable)var2.getLocalAndRemoteObjectContainer().getUidObjectMap().get(this.UID)) != null && var4 instanceof SegmentController) {
            var4.markForPermanentDelete(true);
            var4.setMarkedForDeleteVolatile(true);
         } else {
            try {
               SegmentDataFileUtils.deleteEntitiyFileAndAllData(this.UID);
            } catch (Exception var3) {
               var3.printStackTrace();
            }
         }

         System.err.println("[SERVER][DESIGN][REMOVE] deleting Design UID '" + this.UID + "'");
      }

   }

   protected GUIHorizontalButton[] getButtons(GameClientState var1, Inventory var2) {
      return new GUIHorizontalButton[]{this.getInfoButton(var1, var2), this.getEditButton(var1, var2), this.getDeleteButton(var1, var2), this.getLoadDesignButton(var1, var2), this.getUnloadDesignButton(var1, var2)};
   }

   public ShipyardCollectionManager getShipyardFromInventory(Inventory var1) {
      return var1.getInventoryHolder() != null && var1.getParameter() != Long.MIN_VALUE && var1.getInventoryHolder() instanceof ShipyardManagerContainerInterface ? (ShipyardCollectionManager)((ShipyardManagerContainerInterface)var1.getInventoryHolder()).getShipyard().getCollectionManagersMap().get(var1.getParameter()) : null;
   }

   public boolean isInventoryLocked(Inventory var1) {
      ShipyardCollectionManager var2;
      if ((var2 = this.getShipyardFromInventory(var1)) != null) {
         return var2.getCurrentDesignObject() == this;
      } else {
         return System.currentTimeMillis() - this.lastLoadAction < 3000L;
      }
   }

   public String toString() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_VIRTUALBLUEPRINTMETAITEM_8, this.virtualName);
   }

   public boolean isDrawnOverlayInHotbar() {
      return true;
   }

   public boolean isDrawnOverlayInInventory() {
      return true;
   }

   public boolean equalsObject(MetaObject var1) {
      return super.equalsTypeAndSubId(var1) && this.UID.equals(((VirtualBlueprintMetaItem)var1).UID) && this.virtualName.equals(((VirtualBlueprintMetaItem)var1).virtualName);
   }
}
