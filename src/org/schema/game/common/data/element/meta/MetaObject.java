package org.schema.game.common.data.element.meta;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContextPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.input.InputState;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.tag.Tag;

public abstract class MetaObject {
   public static final int NO_EDIT_PERMISSION = 0;
   public static final int MODIFIABLE_CLIENT = 1;
   public static final long SALVAGE_TOOL_ID = -100100L;
   private int id;

   public MetaObject(int var1) {
      this.id = var1;
   }

   public boolean clientMayModify() {
      return (this.getPermission() & 1) == 1;
   }

   public abstract void deserialize(DataInputStream var1) throws IOException;

   public abstract void fromTag(Tag var1);

   public abstract Tag getBytesTag();

   public abstract DialogInput getEditDialog(GameClientState var1, AbstractControlManager var2, Inventory var3);

   public DialogInput getInfoDialog(GameClientState var1, AbstractControlManager var2, Inventory var3) {
      return this.getEditDialog(var1, var2, var3);
   }

   public short[] getSubTypes() {
      throw new NullPointerException("No subtypes for " + this);
   }

   public int getId() {
      return this.id;
   }

   public void setId(int var1) {
      this.id = var1;
   }

   public abstract MetaObjectManager.MetaObjectType getObjectBlockType();

   public short getObjectBlockID() {
      return this.getObjectBlockType().type;
   }

   public int hashCode() {
      return this.id;
   }

   public boolean equals(Object var1) {
      return this.id == ((MetaObject)var1).id;
   }

   public abstract int getPermission();

   public short getSubObjectId() {
      return -1;
   }

   public abstract boolean isValidObject();

   public abstract void serialize(DataOutputStream var1) throws IOException;

   public int getExtraBuildIconIndex() {
      return 0;
   }

   public void drawPossibleOverlay(GUIOverlay var1, Inventory var2) {
   }

   public float hasZoomFunction() {
      return -1.0F;
   }

   public void onMouseAction(AbstractCharacter var1, ControllerStateUnit var2, int var3, int var4, Timer var5) {
   }

   public boolean drawUsingReloadIcon() {
      return false;
   }

   protected GUIHorizontalButton getDeleteButton(final GameClientState var1, final Inventory var2) {
      return new GUIHorizontalButton(var1, GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, "DISPOSE", new GUICallback() {
         public void callback(GUIElement var1x, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               (new PlayerGameOkCancelInput("CONFIRM", var1, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_METAOBJECT_0, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_METAOBJECT_1) {
                  public boolean isOccluded() {
                     return false;
                  }

                  public void onDeactivate() {
                  }

                  public void pressedOK() {
                     if (var2.getSlotFromMetaId(MetaObject.this.getId()) >= 0) {
                        var2.removeMetaItem(MetaObject.this);
                     }

                     this.deactivate();
                  }
               }).activate();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }, (GUIActiveInterface)null, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return MetaObject.this.canDisposeItem(var2);
         }
      });
   }

   protected boolean canDisposeItem(Inventory var1) {
      return !this.isInventoryLocked(var1);
   }

   public boolean isInventoryLocked(Inventory var1) {
      return false;
   }

   protected GUIHorizontalButton getInfoButton(final GameClientState var1, final Inventory var2) {
      return new GUIHorizontalButton(var1, GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_METAOBJECT_2, new GUICallback() {
         public void callback(GUIElement var1x, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               DialogInput var3;
               if ((var3 = MetaObject.this.getInfoDialog(var1, var1.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager(), var2)) == null) {
                  var1.getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_METAOBJECT_3, 0.0F);
                  return;
               }

               var3.activate();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }, (GUIActiveInterface)null, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
   }

   protected GUIHorizontalButton getEditButton(GameClientState var1, Inventory var2) {
      return this.getCustomEditButton(var1, var2, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_METAOBJECT_4);
   }

   protected GUIHorizontalButton getCustomEditButton(final GameClientState var1, final Inventory var2, String var3) {
      return new GUIHorizontalButton(var1, GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, var3, new GUICallback() {
         public void callback(GUIElement var1x, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               var1.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().suspend(true);
               DialogInput var3;
               if ((var3 = MetaObject.this.getEditDialog(var1, var1.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager(), var2)) == null) {
                  var1.getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_METAOBJECT_5, 0.0F);
                  return;
               }

               var3.activate();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }, (GUIActiveInterface)null, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return MetaObject.this.clientMayModify() && !MetaObject.this.isInventoryLocked(var2);
         }
      });
   }

   protected GUIHorizontalButton[] getButtons(GameClientState var1, Inventory var2) {
      return new GUIHorizontalButton[]{this.getInfoButton(var1, var2), this.getEditButton(var1, var2), this.getDeleteButton(var1, var2)};
   }

   protected void onDelete(boolean var1, StateInterface var2) {
   }

   public abstract String getName();

   public GUIContextPane createContextPane(GameClientState var1, Inventory var2, GUIElement var3) {
      GUIHorizontalButton[] var6 = this.getButtons(var1, var2);
      GUIContextPane var7;
      (var7 = new GUIContextPane(var1, 100.0F, (float)(var6.length * 25))).onInit();
      GUIHorizontalButtonTablePane var5;
      (var5 = new GUIHorizontalButtonTablePane(var1, 1, var6.length, var7)).onInit();

      for(int var4 = 0; var4 < var6.length; ++var4) {
         var6[var4].activeInterface = var5.activeInterface;
         var5.addButton(var6[var4], 0, var4);
      }

      var7.attach(var5);
      System.err.println("[CLIENT][GUI] contect pane for meta item " + this);
      return var7;
   }

   public boolean canClientRemove() {
      return true;
   }

   public boolean isDrawnOverlayInHotbar() {
      return true;
   }

   public boolean isDrawnOverlayInInventory() {
      return false;
   }

   public boolean update(Timer var1) {
      return false;
   }

   public double getVolume() {
      return 1.0D;
   }

   public boolean equalsTypeAndSubId(MetaObject var1) {
      return this.getObjectBlockType() == var1.getObjectBlockType() && this.getSubObjectId() == var1.getSubObjectId();
   }

   public abstract boolean equalsObject(MetaObject var1);
}
