package org.schema.game.common.data.element.meta.weapon;

import com.bulletphysics.linearmath.Transform;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.common.controller.damage.effects.InterEffectHandler;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.elements.missile.MissileController;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class RocketLauncherWeapon extends Weapon {
   public int damage = 200;
   public float speed = 30.0F;
   public float radius = 7.5F;
   public float distance = 300.0F;
   public int reload = 13000;
   private Vector4f color = new Vector4f(Math.min(1.0F, (float)Math.random() + (float)Math.random()), Math.min(1.0F, (float)Math.random() + (float)Math.random()), Math.min(1.0F, (float)Math.random() + (float)Math.random()), 1.0F);
   private long vol_lastShot = 0L;
   private Vector3f dirTmp = new Vector3f();

   public RocketLauncherWeapon(int var1) {
      super(var1, Weapon.WeaponSubType.ROCKET_LAUNCHER.type);
   }

   public void deserialize(DataInputStream var1) throws IOException {
      this.damage = var1.readInt();
      this.speed = var1.readFloat();
      this.color.set(var1.readFloat(), var1.readFloat(), var1.readFloat(), var1.readFloat());
      this.radius = var1.readFloat();
      this.distance = var1.readFloat();
      this.reload = var1.readInt();
   }

   public String getName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_ROCKETLAUNCHERWEAPON_0;
   }

   public void fromTag(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.damage = (Integer)var2[0].getValue();
      this.speed = (Float)var2[1].getValue();
      this.reload = (Integer)var2[2].getValue();
      this.color = (Vector4f)var2[3].getValue();
      this.radius = (Float)var2[4].getValue();
      this.distance = (Float)var2[5].getValue();
   }

   public Tag getBytesTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.INT, (String)null, this.damage), new Tag(Tag.Type.FLOAT, (String)null, this.speed), new Tag(Tag.Type.INT, (String)null, this.reload), new Tag(Tag.Type.VECTOR4f, (String)null, this.color), new Tag(Tag.Type.FLOAT, (String)null, this.radius), new Tag(Tag.Type.FLOAT, (String)null, this.distance), FinishTag.INST});
   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.writeInt(this.damage);
      var1.writeFloat(this.speed);
      var1.writeFloat(this.color.x);
      var1.writeFloat(this.color.y);
      var1.writeFloat(this.color.z);
      var1.writeFloat(this.color.w);
      var1.writeFloat(this.radius);
      var1.writeFloat(this.distance);
      var1.writeInt(this.reload);
   }

   public void fire(AbstractCharacter var1, AbstractOwnerState var2, boolean var3, boolean var4, Timer var5) {
      this.fire(var1, var2, var2.getForward(this.dirTmp), var3, var4, var5);
   }

   public void fire(AbstractCharacter var1, AbstractOwnerState var2, Vector3f var3, boolean var4, boolean var5, Timer var6) {
      var3.scale(this.speed);
      long var7;
      if ((var7 = System.currentTimeMillis()) - this.vol_lastShot > (long)this.reload) {
         if (var2.isOnServer()) {
            MissileController var11 = ((GameServerState)var2.getState()).getController().getMissileController();
            System.err.println("[SERVER] Character fireing missile launcher: " + var1.getWorldTransform().origin + " -> " + var3);
            long var9 = (long)this.getId();
            var11.addDumbMissile(var1, new Transform(var1.getShoulderWorldTransform()), var3, this.speed, (float)this.damage, this.distance, var9, (short)0).selfDamage = true;
         }

         this.vol_lastShot = var7;
      }

   }

   protected String toDetailedString() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_ROCKETLAUNCHERWEAPON_2, this.damage, this.speed, this.reload, this.color, this.distance);
   }

   public Vector4f getColor() {
      return this.color;
   }

   public void setColor(Vector4f var1) {
      this.color = var1;
   }

   public long getVol_lastShot() {
      return this.vol_lastShot;
   }

   public boolean equalsObject(MetaObject var1) {
      return super.equalsTypeAndSubId(var1) && this.damage == ((RocketLauncherWeapon)var1).damage && this.color.equals(((RocketLauncherWeapon)var1).color) && this.speed == ((RocketLauncherWeapon)var1).speed;
   }

   protected void setupEffectSet(InterEffectSet var1) {
      var1.setStrength(InterEffectHandler.InterEffectType.KIN, 0.2F);
      var1.setStrength(InterEffectHandler.InterEffectType.HEAT, 0.8F);
      var1.setStrength(InterEffectHandler.InterEffectType.EM, 0.0F);
   }
}
