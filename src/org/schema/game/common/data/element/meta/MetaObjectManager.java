package org.schema.game.common.data.element.meta;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.MetaObjectState;
import org.schema.game.common.data.element.meta.weapon.Weapon;
import org.schema.game.common.data.player.inventory.FreeItem;
import org.schema.game.common.data.player.inventory.InvalidMetaItemException;
import org.schema.game.network.objects.NetworkClientChannel;
import org.schema.game.network.objects.remote.RemoteMetaObject;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.server.ServerStateInterface;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class MetaObjectManager {
   public static final String FILENAME;
   public static final IntOpenHashSet subIdTypes;
   private static int idGenServer;
   private static int idGenClient;
   private final Int2ObjectOpenHashMap map = new Int2ObjectOpenHashMap();
   private final StateInterface state;
   private final Object2ObjectOpenHashMap archive = new Object2ObjectOpenHashMap();
   public static boolean oldLoad;
   public final Set updatableObjects = new ObjectOpenHashSet();
   private static final byte VERSION = 0;

   public static short[] getSubTypes(MetaObjectManager.MetaObjectType var0) {
      switch(var0) {
      case WEAPON:
         return Weapon.WeaponSubType.getTypes();
      default:
         return null;
      }
   }

   public MetaObjectManager(StateInterface var1) {
      this.state = var1;
   }

   public static MetaObject deserializeStatic(DataInputStream var0) throws IOException {
      int var1 = var0.readInt();
      short var2 = var0.readShort();
      short var3 = -1;
      if (subIdTypes.contains(var2)) {
         var3 = var0.readShort();
      }

      MetaObject var4;
      (var4 = instantiate(var2, 0, var3)).setId(var1);
      var4.deserialize(var0);
      return var4;
   }

   private static synchronized int getNewId(boolean var0) {
      return var0 ? idGenServer++ : idGenClient++;
   }

   public static MetaObject instantiate(short var0, int var1, short var2) {
      switch(MetaObjectManager.MetaObjectType.getById(var0)) {
      case WEAPON:
         return Weapon.instantiate(var1, var2);
      case LOG_BOOK:
         return new Logbook(var1);
      case RECIPE:
         return new Recipe(var1);
      case HELMET:
         return new Helmet(var1);
      case BLUEPRINT:
         return new BlueprintMetaItem(var1);
      case VIRTUAL_BLUEPRINT:
         return new VirtualBlueprintMetaItem(var1);
      case BUILD_PROHIBITER:
         return new BuildProhibiter(var1);
      case FLASH_LIGHT:
         return new FlashLight(var1);
      case BLOCK_STORAGE:
         return new BlockStorageMetaItem(var1);
      default:
         throw new IllegalArgumentException("UNKNOWN OID: " + var0);
      }
   }

   public static MetaObject instantiate(MetaObjectManager.MetaObjectType var0, short var1, boolean var2) {
      return instantiate(var0.type, var1, var2);
   }

   public static MetaObject instantiate(short var0, short var1, boolean var2) {
      return instantiate(var0, getNewId(var2), var1);
   }

   public static void serialize(DataOutputStream var0, MetaObject var1) throws IOException {
      var0.writeInt(var1.getId());
      var0.writeShort(var1.getObjectBlockID());

      assert subIdTypes.contains(var1.getObjectBlockID()) || var1.getSubObjectId() == -1;

      if (var1.getSubObjectId() >= 0) {
         assert subIdTypes.contains(var1.getObjectBlockID());

         var0.writeShort(var1.getSubObjectId());
      }

      var1.serialize(var0);
   }

   public void archive(Vector3i var1, FreeItem var2) {
      MetaObject var3 = (MetaObject)this.map.remove(var2.getMetaId());
      this.archive(var1, var3);
   }

   public void archive(Vector3i var1, MetaObject var2) {
      if (var2 != null) {
         synchronized(this.archive) {
            ObjectArrayList var4;
            if ((var4 = (ObjectArrayList)this.archive.get(var1)) == null) {
               var4 = new ObjectArrayList();
               this.archive.put(var1, var4);
            }

            var4.add(var2);
            System.err.println("[METAITEM] Archived meta item " + var1 + " -> " + var2.getId() + "; LIST: " + var4);
         }
      }
   }

   public void awnserRequestTo(int var1, NetworkClientChannel var2) {
      if (this.map.containsKey(var1)) {
         MetaObject var3 = (MetaObject)this.map.get(var1);
         var2.metaObjectBuffer.add(new RemoteMetaObject(var3, this, true));
      } else {
         assert false : "requested meta object does not exist " + var1 + ": " + this.map;

      }
   }

   public void checkAvailable(int var1, MetaObjectState var2) {
      if (!this.map.containsKey(var1)) {
         System.err.println("Metaobject " + var1 + " is not available: requesting from server");
         var2.requestMetaObject(var1);
      }

   }

   public void deserialize(DataInputStream var1) throws IOException {
      int var2 = var1.readInt();
      short var3 = var1.readShort();
      short var4 = -1;
      if (subIdTypes.contains(var3)) {
         var4 = var1.readShort();
      }

      MetaObject var5;
      if ((var5 = (MetaObject)this.map.get(var2)) == null) {
         (var5 = instantiate(var3, 0, var4)).setId(var2);
         this.map.put(var2, var5);
      }

      var5.deserialize(var1);
   }

   public void deserializeRequest(DataInputStream var1) throws IOException {
      MetaObject var2;
      if ((var2 = deserializeStatic(var1)).clientMayModify() && var2.isValidObject()) {
         this.map.put(var2.getId(), var2);
         ((GameServerState)this.state).getGameState().announceMetaObject(var2);
      }

   }

   public void getFromArchive(Vector3i var1, Map var2) {
      if (!var2.isEmpty()) {
         synchronized(this.archive) {
            if (!this.archive.containsKey(var1)) {
               Iterator var10 = var2.values().iterator();

               while(var10.hasNext()) {
                  if (((FreeItem)var10.next()).getType() < 0) {
                     var10.remove();
                  }
               }

            } else {
               ObjectArrayList var9 = (ObjectArrayList)this.archive.remove(var1);
               Iterator var11 = var2.values().iterator();

               while(true) {
                  FreeItem var4;
                  do {
                     if (!var11.hasNext()) {
                        return;
                     }
                  } while((var4 = (FreeItem)var11.next()).getType() >= 0);

                  boolean var5 = false;

                  for(int var6 = 0; var6 < var9.size(); ++var6) {
                     MetaObject var7;
                     if ((var7 = (MetaObject)var9.get(var6)).getId() == var4.getMetaId()) {
                        if (oldLoad) {
                           var7.setId(getNewId(this.isOnServer()));
                        }

                        var4.setMetaId(var7.getId());
                        var5 = true;
                        this.putServer(var7);
                     }
                  }

                  if (!var5) {
                     System.err.println("[METAOBJECT] WARNING: NOT found in archive: " + var4.getMetaId());
                     var11.remove();
                  }
               }
            }
         }
      }
   }

   public boolean isOnServer() {
      return this.state instanceof ServerStateInterface;
   }

   public MetaObject getObject(int var1) {
      return (MetaObject)this.map.get(var1);
   }

   public void load() throws FileNotFoundException, IOException {
      Tag var1 = Tag.readFrom(new BufferedInputStream(new FileInputStream(new FileExt(FILENAME))), true, false);
      synchronized(this.archive) {
         Tag[] var13;
         if ("moi".equals(var1.getName())) {
            Tag[] var3;
            (Byte)(var3 = (Tag[])var1.getValue())[0].getValue();
            idGenServer = (Integer)var3[1].getValue();
            var13 = (Tag[])var3[2].getValue();
         } else {
            var13 = (Tag[])var1.getValue();
            oldLoad = true;
            System.err.println("[SERVER] Warning using old way of loading Meta Items");
         }

         for(int var14 = 0; var14 < var13.length - 1; ++var14) {
            Tag[] var4;
            Vector3i var5 = (Vector3i)(var4 = (Tag[])var13[var14].getValue())[0].getValue();
            var4 = (Tag[])var4[1].getValue();

            for(int var6 = 0; var6 < var4.length - 1; ++var6) {
               Tag[] var7;
               int var8 = (Integer)(var7 = (Tag[])var4[var6].getValue())[0].getValue();
               short var9 = (Short)var7[1].getValue();
               short var10 = -1;
               if (var7[3].getType() == Tag.Type.SHORT) {
                  var10 = (Short)var7[3].getValue();
               }

               MetaObject var15;
               try {
                  (var15 = instantiate(var9, -1, var10)).setId(var8);
                  var15.fromTag(var7[2]);
                  this.archive(var5, var15);
               } catch (InvalidMetaItemException var11) {
                  var15 = null;
                  var11.printStackTrace();
               }
            }
         }

      }
   }

   public void updateLocal(Timer var1) {
      if (this.updatableObjects.size() > 0) {
         Iterator var2 = this.updatableObjects.iterator();

         while(var2.hasNext()) {
            if (!((MetaObject)var2.next()).update(var1)) {
               var2.remove();
            }
         }
      }

   }

   public void modifyRequest(NetworkClientChannel var1, MetaObject var2) throws MetaItemModifyPermissionException {
      if ((var2.getPermission() & 1) == 1) {
         var1.metaObjectModifyRequestBuffer.add(new RemoteMetaObject(var2, this, true));
      } else {
         throw new MetaItemModifyPermissionException("Cannot modify item " + var2 + " (Permission denied)");
      }
   }

   public void putServer(MetaObject var1) {
      this.map.put(var1.getId(), var1);
   }

   public void checkCollisionServer(MetaObject var1) {
      MetaObject var2;
      if ((var2 = (MetaObject)this.map.get(var1.getId())) != null && !var1.equalsObject(var2)) {
         System.err.println("WARNING: MetaItem ID collision: " + this + " and " + var1);
         var1.setId(getNewId(true));
      }

   }

   public void receivedAnnoucedMetaObject(MetaObject var1) {
      if (this.map.containsKey(var1.getId())) {
         this.map.put(var1.getId(), var1);
      }

   }

   public void save() throws FileNotFoundException, IOException {
      synchronized(this.archive) {
         Tag[] var2;
         Tag[] var10000 = var2 = new Tag[this.archive.size() + 1];
         var10000[var10000.length - 1] = FinishTag.INST;
         int var3 = 0;

         for(Iterator var4 = this.archive.entrySet().iterator(); var4.hasNext(); ++var3) {
            Entry var5;
            Tag[] var6;
            var10000 = var6 = new Tag[((ObjectArrayList)(var5 = (Entry)var4.next()).getValue()).size() + 1];
            var10000[var10000.length - 1] = FinishTag.INST;

            for(int var7 = 0; var7 < ((ObjectArrayList)var5.getValue()).size(); ++var7) {
               MetaObject var8 = (MetaObject)((ObjectArrayList)var5.getValue()).get(var7);
               var6[var7] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.INT, (String)null, var8.getId()), new Tag(Tag.Type.SHORT, (String)null, var8.getObjectBlockID()), var8.getBytesTag(), new Tag(Tag.Type.SHORT, (String)null, var8.getSubObjectId()), FinishTag.INST});
            }

            var2[var3] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.VECTOR3i, (String)null, var5.getKey()), new Tag(Tag.Type.STRUCT, (String)null, var6), FinishTag.INST});
         }

         Tag var10 = new Tag(Tag.Type.STRUCT, "moi", new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.INT, (String)null, idGenServer + 10), new Tag(Tag.Type.STRUCT, "floatingItems", var2), FinishTag.INST});
         FileExt var11;
         if (!(var11 = new FileExt(FILENAME + ".tmp")).exists()) {
            var11.getParentFile().mkdirs();
            var11.createNewFile();
         }

         var10.writeTo(new BufferedOutputStream(new FileOutputStream(var11)), true);
         FileExt var12 = new FileExt(FILENAME);
         FileExt var13;
         if ((var13 = new FileExt(FILENAME + ".old")).exists()) {
            var13.delete();
         }

         if (var12.exists()) {
            var12.renameTo(var13);
         }

         var12 = new FileExt(FILENAME);
         var11.renameTo(var12);
      }
   }

   public void serverRemoveObject(int var1) {
      System.err.println("[SERVER][META] removing metaID: " + var1);
      MetaObject var2;
      if ((var2 = (MetaObject)this.map.remove(var1)) != null) {
         var2.onDelete(true, this.state);
      }

   }

   public double getVolume(int var1) {
      MetaObject var2;
      return (var2 = this.getObject(var1)) != null ? var2.getVolume() : 1.0D;
   }

   public void clientRemoveObject(int var1) {
      System.err.println("[CLIENT][META] removing metaID: " + var1);
      MetaObject var2;
      if ((var2 = (MetaObject)this.map.remove(var1)) != null) {
         var2.onDelete(false, this.state);
      }

   }

   static {
      FILENAME = GameServerState.ENTITY_DATABASE_PATH + "FLOATING_ITEMS_ARCHIVE.ent";
      subIdTypes = new IntOpenHashSet();
      idGenServer = 100000;
      idGenClient = 100;
      subIdTypes.add(MetaObjectManager.MetaObjectType.WEAPON.type);
   }

   public static enum MetaObjectType {
      BLUEPRINT((short)-9),
      RECIPE((short)-10),
      LOG_BOOK((short)-11),
      HELMET((short)-12),
      BUILD_PROHIBITER((short)-13),
      FLASH_LIGHT((short)-14),
      VIRTUAL_BLUEPRINT((short)-15),
      BLOCK_STORAGE((short)-16),
      WEAPON((short)-32);

      public final short type;

      private MetaObjectType(short var3) {
         this.type = var3;
      }

      public static MetaObjectManager.MetaObjectType getById(short var0) {
         MetaObjectManager.MetaObjectType[] var1;
         int var2 = (var1 = values()).length;

         for(int var3 = 0; var3 < var2; ++var3) {
            MetaObjectManager.MetaObjectType var4;
            if ((var4 = var1[var3]).type == var0) {
               return var4;
            }
         }

         throw new NullPointerException("Illegal meta id: " + var0);
      }

      public static MetaObjectManager.MetaObjectType getByIdWOExcept(short var0) {
         MetaObjectManager.MetaObjectType[] var1;
         int var2 = (var1 = values()).length;

         for(int var3 = 0; var3 < var2; ++var3) {
            MetaObjectManager.MetaObjectType var4;
            if ((var4 = var1[var3]).type == var0) {
               return var4;
            }
         }

         return null;
      }
   }
}
