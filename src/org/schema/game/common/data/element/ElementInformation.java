package org.schema.game.common.data.element;

import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.CompoundShape;
import com.bulletphysics.linearmath.Transform;
import com.mxgraph.layout.mxParallelEdgeLayout;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import it.unimi.dsi.fastutil.shorts.ShortIterator;
import it.unimi.dsi.fastutil.shorts.ShortList;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import it.unimi.dsi.fastutil.shorts.ShortSet;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.Map.Entry;
import javax.swing.JPanel;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.gui.GUIBlockSprite;
import org.schema.game.client.view.gui.shop.shopnew.ShopItemElement;
import org.schema.game.client.view.tools.SingleBlockDrawer;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.damage.effects.InterEffectHandler;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.config.ConfigGroup;
import org.schema.game.common.data.blockeffects.config.ConfigPool;
import org.schema.game.common.data.element.annotation.ElemType;
import org.schema.game.common.data.element.meta.RecipeInterface;
import org.schema.game.common.data.physics.ConvexHullShapeExt;
import org.schema.game.common.data.physics.octree.ArrayOctree;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.facedit.AddElementEntryDialog;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.MeshGroup;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraph;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraphElement;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyboardMappings;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ElementInformation implements Comparable {
   public static final String[] rDesc = new String[]{"ore", "plant", "basic", "Cubatom-Splittable", "manufactory", "advanced", "capsule"};
   public static final int FAC_NONE = 0;
   public static final int FAC_CAPSULE = 1;
   public static final int FAC_MICRO = 2;
   public static final int FAC_BASIC = 3;
   public static final int FAC_STANDARD = 4;
   public static final int FAC_ADVANCED = 5;
   public static final int RT_ORE = 0;
   public static final int RT_PLANT = 1;
   public static final int RT_BASIC = 2;
   public static final int RT_CUBATOM_SPLITTABLE = 3;
   public static final int RT_MANUFACTORY = 4;
   public static final int RT_ADVANCED = 5;
   public static final int RT_CAPSULE = 6;
   public static final int CHAMBER_APPLIES_TO_SELF = 0;
   public static final int CHAMBER_APPLIES_TO_SECTOR = 1;
   @org.schema.game.common.data.element.annotation.Element(
      writeAsTag = false,
      canBulkChange = false,
      parser = ElemType.ID,
      cat = ElementInformation.EIC.BASICS,
      order = -1
   )
   public final short id;
   @org.schema.game.common.data.element.annotation.Element(
      writeAsTag = false,
      canBulkChange = false,
      parser = ElemType.TEXTURE,
      cat = ElementInformation.EIC.TEXTURES,
      order = 0
   )
   private short[] textureId;
   @org.schema.game.common.data.element.annotation.Element(
      consistence = true,
      parser = ElemType.CONSISTENCE,
      cat = ElementInformation.EIC.CRAFTING_ECONOMY,
      order = 0
   )
   public final List consistence = new ObjectArrayList();
   @org.schema.game.common.data.element.annotation.Element(
      cubatomConsistence = true,
      parser = ElemType.CUBATON_CONSISTENCE,
      cat = ElementInformation.EIC.DEPRECATED,
      order = 0
   )
   public final List cubatomConsistence = new ObjectArrayList();
   @org.schema.game.common.data.element.annotation.Element(
      collectionElementTag = "Element",
      elementSet = true,
      collectionType = "blockTypes",
      parser = ElemType.CONTROLLED_BY,
      cat = ElementInformation.EIC.FEATURES,
      order = 10
   )
   public final ShortSet controlledBy = new ShortOpenHashSet();
   @org.schema.game.common.data.element.annotation.Element(
      collectionElementTag = "Element",
      elementSet = true,
      collectionType = "blockTypes",
      parser = ElemType.CONTROLLING,
      cat = ElementInformation.EIC.FEATURES,
      order = 11
   )
   public final ShortSet controlling = new ShortOpenHashSet();
   @org.schema.game.common.data.element.annotation.Element(
      collectionElementTag = "Element",
      elementSet = true,
      collectionType = "blockTypes",
      parser = ElemType.RECIPE_BUY_RESOURCE,
      cat = ElementInformation.EIC.CRAFTING_ECONOMY,
      order = 4
   )
   public final ShortList recipeBuyResources = new ShortArrayList();
   public final ObjectOpenHashSet parsed = new ObjectOpenHashSet(2048);
   private final int[] textureLayerMapping = new int[6];
   private final int[] textureIndexLocalMapping = new int[6];
   private final int[] textureLayerMappingActive = new int[6];
   private final int[] textureIndexLocalMappingActive = new int[6];
   @org.schema.game.common.data.element.annotation.Element(
      from = 0,
      to = 10000000,
      parser = ElemType.ARMOR_VALUE,
      cat = ElementInformation.EIC.HP_ARMOR,
      order = 6
   )
   public float armorValue;
   @org.schema.game.common.data.element.annotation.Element(
      writeAsTag = false,
      canBulkChange = false,
      parser = ElemType.NAME,
      cat = ElementInformation.EIC.BASICS,
      order = 0
   )
   public String name = "n/a";
   public ElementCategory type;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.EFFECT_ARMOR,
      canBulkChange = true,
      cat = ElementInformation.EIC.HP_ARMOR,
      order = 7
   )
   public InterEffectSet effectArmor = new InterEffectSet();
   @org.schema.game.common.data.element.annotation.Element(
      writeAsTag = false,
      canBulkChange = false,
      parser = ElemType.BUILD_ICON,
      cat = ElementInformation.EIC.BASICS,
      order = 4
   )
   public int buildIconNum = 62;
   @org.schema.game.common.data.element.annotation.Element(
      canBulkChange = false,
      parser = ElemType.FULL_NAME,
      cat = ElementInformation.EIC.BASICS,
      order = 1
   )
   public String fullName = "";
   @org.schema.game.common.data.element.annotation.Element(
      from = 0,
      to = Integer.MAX_VALUE,
      parser = ElemType.PRICE,
      cat = ElementInformation.EIC.CRAFTING_ECONOMY,
      order = 1
   )
   public long price = 100L;
   @org.schema.game.common.data.element.annotation.Element(
      textArea = true,
      parser = ElemType.DESCRIPTION,
      cat = ElementInformation.EIC.BASICS,
      order = 2
   )
   public String description = "undefined description";
   @org.schema.game.common.data.element.annotation.Element(
      states = {"0", "1", "2", "3", "4", "5", "6"},
      stateDescs = {"ore", "plant", "basic", "Cubatom-Splittable", "manufactory", "advanced", "capsule"},
      parser = ElemType.BLOCK_RESOURCE_TYPE,
      cat = ElementInformation.EIC.CRAFTING_ECONOMY,
      order = 5
   )
   public int blockResourceType = 2;
   @org.schema.game.common.data.element.annotation.Element(
      states = {"0", "1", "2", "3", "4", "5"},
      stateDescs = {"none", "capsule refinery", "micro assembler", "basic factory", "standard factory", "advanced factory"},
      parser = ElemType.PRODUCED_IN_FACTORY,
      cat = ElementInformation.EIC.CRAFTING_ECONOMY,
      order = 6
   )
   public int producedInFactory = 0;
   @org.schema.game.common.data.element.annotation.Element(
      type = true,
      parser = ElemType.BASIC_RESOURCE_FACTORY,
      cat = ElementInformation.EIC.CRAFTING_ECONOMY,
      order = 7
   )
   public short basicResourceFactory = 0;
   @org.schema.game.common.data.element.annotation.Element(
      from = 0,
      to = 1000000,
      parser = ElemType.FACTORY_BAKE_TIME,
      cat = ElementInformation.EIC.CRAFTING_ECONOMY,
      order = 8
   )
   public float factoryBakeTime = 5.0F;
   @org.schema.game.common.data.element.annotation.Element(
      inventoryGroup = true,
      parser = ElemType.INVENTORY_GROUP,
      cat = ElementInformation.EIC.BASICS,
      order = 3
   )
   public String inventoryGroup = "";
   @org.schema.game.common.data.element.annotation.Element(
      factory = true,
      parser = ElemType.FACTORY,
      cat = ElementInformation.EIC.CRAFTING_ECONOMY,
      order = 9
   )
   public BlockFactory factory;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.ANIMATED,
      cat = ElementInformation.EIC.CRAFTING_ECONOMY,
      order = 7
   )
   public boolean animated;
   @org.schema.game.common.data.element.annotation.Element(
      from = 0,
      to = Integer.MAX_VALUE,
      parser = ElemType.STRUCTURE_HP,
      cat = ElementInformation.EIC.FEATURES,
      order = 0
   )
   public int structureHP = 0;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.TRANSPARENCY,
      cat = ElementInformation.EIC.FEATURES,
      order = 1
   )
   public boolean blended;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.IN_SHOP,
      cat = ElementInformation.EIC.CRAFTING_ECONOMY,
      order = 2
   )
   public boolean shoppable = true;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.ORIENTATION,
      cat = ElementInformation.EIC.BASICS,
      order = 6
   )
   public boolean orientatable;
   @org.schema.game.common.data.element.annotation.Element(
      selectBlock = true,
      parser = ElemType.BLOCK_COMPUTER_REFERENCE,
      cat = ElementInformation.EIC.BASICS,
      order = 7
   )
   public int computerType;
   public static final String[] slabStrings = new String[]{"full block", "3/4 block", "1/2 block", "1/4 block"};
   @org.schema.game.common.data.element.annotation.Element(
      states = {"0", "1", "2", "3"},
      stateDescs = {"full block", "3/4 block", "1/2 block", "1/4 block"},
      parser = ElemType.SLAB,
      cat = ElementInformation.EIC.BASICS,
      order = 8
   )
   public int slab = 0;
   @org.schema.game.common.data.element.annotation.Element(
      canBulkChange = false,
      parser = ElemType.SLAB_IDS,
      cat = ElementInformation.EIC.BASICS,
      order = 9
   )
   public short[] slabIds = null;
   @org.schema.game.common.data.element.annotation.Element(
      canBulkChange = false,
      parser = ElemType.STYLE_IDS,
      cat = ElementInformation.EIC.BASICS,
      order = 10
   )
   public short[] styleIds = null;
   @org.schema.game.common.data.element.annotation.Element(
      canBulkChange = false,
      parser = ElemType.WILDCARD_IDS,
      cat = ElementInformation.EIC.BASICS,
      order = 11
   )
   public short[] wildcardIds = null;
   public short[] blocktypeIds;
   @org.schema.game.common.data.element.annotation.Element(
      canBulkChange = false,
      editable = false,
      parser = ElemType.SOURCE_REFERENCE,
      cat = ElementInformation.EIC.BASICS,
      order = 12
   )
   public int sourceReference = 0;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.GENERAL_CHAMBER,
      cat = ElementInformation.EIC.POWER_REACTOR,
      order = 0
   )
   public boolean chamberGeneral;
   @org.schema.game.common.data.element.annotation.Element(
      writeAsTag = false,
      parser = ElemType.EDIT_REACTOR,
      cat = ElementInformation.EIC.POWER_REACTOR,
      order = 1
   )
   public ElementReactorChange change;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.CHAMBER_CAPACITY,
      cat = ElementInformation.EIC.POWER_REACTOR,
      order = 2
   )
   public float chamberCapacity = 0.0F;
   @org.schema.game.common.data.element.annotation.Element(
      editable = false,
      selectBlock = true,
      parser = ElemType.CHAMBER_ROOT,
      cat = ElementInformation.EIC.POWER_REACTOR,
      order = 3
   )
   public int chamberRoot;
   @org.schema.game.common.data.element.annotation.Element(
      editable = false,
      selectBlock = true,
      parser = ElemType.CHAMBER_PARENT,
      cat = ElementInformation.EIC.POWER_REACTOR,
      order = 4
   )
   public int chamberParent;
   @org.schema.game.common.data.element.annotation.Element(
      editable = false,
      selectBlock = true,
      parser = ElemType.CHAMBER_UPGRADES_TO,
      cat = ElementInformation.EIC.POWER_REACTOR,
      order = 5
   )
   public int chamberUpgradesTo;
   public static final int CHAMBER_PERMISSION_ANY = 0;
   public static final int CHAMBER_PERMISSION_SHIP = 1;
   public static final int CHAMBER_PERMISSION_STATION = 2;
   public static final int CHAMBER_PERMISSION_PLANET = 4;
   @org.schema.game.common.data.element.annotation.Element(
      states = {"0", "1", "6", "2", "4"},
      stateDescs = {"Any", "Ship Only", "Station/Planet Only", "Station Only", "Planet Only"},
      parser = ElemType.CHAMBER_PERMISSION,
      cat = ElementInformation.EIC.POWER_REACTOR,
      order = 6
   )
   public int chamberPermission = 0;
   @org.schema.game.common.data.element.annotation.Element(
      editable = false,
      canBulkChange = false,
      shortSet = true,
      parser = ElemType.CHAMBER_PREREQUISITES,
      cat = ElementInformation.EIC.POWER_REACTOR,
      order = 7
   )
   public final ShortSet chamberPrerequisites = new ShortOpenHashSet();
   @org.schema.game.common.data.element.annotation.Element(
      editable = true,
      canBulkChange = true,
      shortSet = true,
      parser = ElemType.CHAMBER_MUTUALLY_EXCLUSIVE,
      cat = ElementInformation.EIC.POWER_REACTOR,
      order = 8
   )
   public final ShortSet chamberMutuallyExclusive = new ShortOpenHashSet();
   @org.schema.game.common.data.element.annotation.Element(
      canBulkChange = false,
      editable = false,
      shortSet = true,
      parser = ElemType.CHAMBER_CHILDREN,
      cat = ElementInformation.EIC.POWER_REACTOR,
      order = 9
   )
   public final ShortSet chamberChildren = new ShortOpenHashSet();
   @org.schema.game.common.data.element.annotation.Element(
      collectionElementTag = "Element",
      configGroupSet = true,
      collectionType = "String",
      stringSet = true,
      parser = ElemType.CHAMBER_CONFIG_GROUPS,
      cat = ElementInformation.EIC.POWER_REACTOR,
      order = 10
   )
   public List chamberConfigGroupsLowerCase = new ObjectArrayList();
   @org.schema.game.common.data.element.annotation.Element(
      states = {"0", "1"},
      stateDescs = {"self", "sector"},
      parser = ElemType.CHAMBER_APPLIES_TO,
      cat = ElementInformation.EIC.POWER_REACTOR,
      order = 11
   )
   public int chamberAppliesTo = 0;
   @org.schema.game.common.data.element.annotation.Element(
      editable = true,
      canBulkChange = true,
      parser = ElemType.REACTOR_HP,
      cat = ElementInformation.EIC.POWER_REACTOR,
      order = 12
   )
   public int reactorHp;
   @org.schema.game.common.data.element.annotation.Element(
      editable = true,
      canBulkChange = true,
      parser = ElemType.REACTOR_GENERAL_ICON_INDEX,
      cat = ElementInformation.EIC.POWER_REACTOR,
      order = 13
   )
   public int reactorGeneralIconIndex;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.ENTERABLE,
      cat = ElementInformation.EIC.FEATURES,
      order = 2
   )
   public boolean enterable;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.MASS,
      cat = ElementInformation.EIC.HP_ARMOR,
      order = 0
   )
   public float mass = 0.1F;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.VOLUME,
      cat = ElementInformation.EIC.HP_ARMOR,
      order = 1
   )
   public float volume = -1.0F;
   @org.schema.game.common.data.element.annotation.Element(
      from = 1,
      to = Integer.MAX_VALUE,
      parser = ElemType.HITPOINTS,
      cat = ElementInformation.EIC.HP_ARMOR,
      order = 2
   )
   public int maxHitPointsFull = 100;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.PLACABLE,
      cat = ElementInformation.EIC.FEATURES,
      order = 3
   )
   public boolean placable = true;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.IN_RECIPE,
      cat = ElementInformation.EIC.CRAFTING_ECONOMY,
      order = 3
   )
   public boolean inRecipe;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.CAN_ACTIVATE,
      cat = ElementInformation.EIC.FEATURES,
      order = 4
   )
   public boolean canActivate;
   @org.schema.game.common.data.element.annotation.Element(
      states = {"1", "3", "6"},
      updateTextures = true,
      parser = ElemType.INDIVIDUAL_SIDES,
      cat = ElementInformation.EIC.TEXTURES,
      order = 7
   )
   public int individualSides;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.SIDE_TEXTURE_POINT_TO_ORIENTATION,
      cat = ElementInformation.EIC.TEXTURES,
      order = 8
   )
   public boolean sideTexturesPointToOrientation;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.HAS_ACTIVE_TEXTURE,
      cat = ElementInformation.EIC.TEXTURES,
      order = 9
   )
   public boolean hasActivationTexure;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.MAIN_COMBINATION_CONTROLLER,
      cat = ElementInformation.EIC.FEATURES,
      order = 12
   )
   public boolean mainCombinationController;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.SUPPORT_COMBINATION_CONTROLLER,
      cat = ElementInformation.EIC.FEATURES,
      order = 13
   )
   public boolean supportCombinationController;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.EFFECT_COMBINATION_CONTROLLER,
      cat = ElementInformation.EIC.FEATURES,
      order = 14
   )
   public boolean effectCombinationController;
   @org.schema.game.common.data.element.annotation.Element(
      editable = true,
      canBulkChange = true,
      parser = ElemType.BEACON,
      cat = ElementInformation.EIC.FEATURES,
      order = 15
   )
   public boolean beacon;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.PHYSICAL,
      cat = ElementInformation.EIC.FEATURES,
      order = 5
   )
   public boolean physical;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.BLOCK_STYLE,
      cat = ElementInformation.EIC.BASICS,
      order = 5
   )
   public BlockStyle blockStyle;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.LIGHT_SOURCE,
      cat = ElementInformation.EIC.FEATURES,
      order = 8
   )
   public boolean lightSource;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.DOOR,
      cat = ElementInformation.EIC.FEATURES,
      order = 6
   )
   public boolean door;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.SENSOR_INPUT,
      cat = ElementInformation.EIC.FEATURES,
      order = 7
   )
   public boolean sensorInput;
   @org.schema.game.common.data.element.annotation.Element(
      editable = true,
      canBulkChange = true,
      parser = ElemType.DRAW_LOGIC_CONNECTION,
      cat = ElementInformation.EIC.FEATURES,
      order = 18
   )
   public boolean drawLogicConnection;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.DEPRECATED,
      cat = ElementInformation.EIC.BASICS,
      order = 13
   )
   public boolean deprecated;
   public long dynamicPrice;
   @org.schema.game.common.data.element.annotation.Element(
      parser = ElemType.RESOURCE_INJECTION,
      cat = ElementInformation.EIC.CRAFTING_ECONOMY,
      order = 10
   )
   public ElementInformation.ResourceInjectionType resourceInjection;
   @org.schema.game.common.data.element.annotation.Element(
      from = 0,
      to = 100000,
      parser = ElemType.EXPLOSION_ABSOBTION,
      cat = ElementInformation.EIC.HP_ARMOR,
      order = 8
   )
   public float explosionAbsorbtion;
   @org.schema.game.common.data.element.annotation.Element(
      vector4f = true,
      parser = ElemType.LIGHT_SOURCE_COLOR,
      cat = ElementInformation.EIC.TEXTURES,
      order = 9
   )
   public final Vector4f lightSourceColor;
   @org.schema.game.common.data.element.annotation.Element(
      editable = true,
      canBulkChange = true,
      parser = ElemType.EXTENDED_TEXTURE_4x4,
      cat = ElementInformation.EIC.TEXTURES,
      order = 10
   )
   public boolean extendedTexture;
   @org.schema.game.common.data.element.annotation.Element(
      editable = true,
      canBulkChange = true,
      parser = ElemType.ONLY_DRAW_IN_BUILD_MODE,
      cat = ElementInformation.EIC.BASICS,
      order = 14
   )
   public boolean drawOnlyInBuildMode;
   @org.schema.game.common.data.element.annotation.Element(
      editable = true,
      canBulkChange = true,
      parser = ElemType.LOD_SHAPE,
      modelSelect = true,
      modelSelectFilter = "lod",
      cat = ElementInformation.EIC.MODELS,
      order = 2
   )
   public String lodShapeString;
   @org.schema.game.common.data.element.annotation.Element(
      states = {"0", "1", "2"},
      stateDescs = {"None", "Switch Model", "Keyframe Animation (not implemented yet)"},
      parser = ElemType.LOD_ACTIVATION_ANIMATION_STYLE,
      cat = ElementInformation.EIC.MODELS,
      order = 3
   )
   public int lodActivationAnimationStyle;
   @org.schema.game.common.data.element.annotation.Element(
      editable = true,
      canBulkChange = true,
      parser = ElemType.LOD_SHAPE_ACTIVE,
      modelSelect = true,
      modelSelectFilter = "lod",
      cat = ElementInformation.EIC.MODELS,
      order = 4
   )
   public String lodShapeStringActive;
   @org.schema.game.common.data.element.annotation.Element(
      editable = true,
      canBulkChange = true,
      parser = ElemType.LOD_COLLISION_PHYSICAL,
      modelSelect = true,
      modelSelectFilter = "lod",
      cat = ElementInformation.EIC.MODELS,
      order = 5
   )
   public boolean lodCollisionPhysical;
   @org.schema.game.common.data.element.annotation.Element(
      states = {"0", "1", "2"},
      stateDescs = {"solid block", "sprite", "invisible"},
      parser = ElemType.LOD_SHAPE_FROM_FAR,
      cat = ElementInformation.EIC.MODELS,
      order = 6
   )
   public int lodShapeStyle;
   @org.schema.game.common.data.element.annotation.Element(
      editable = true,
      canBulkChange = true,
      parser = ElemType.LOD_COLLISION,
      cat = ElementInformation.EIC.MODELS,
      order = 7
   )
   public ElementInformation.LodCollision lodCollision;
   @org.schema.game.common.data.element.annotation.Element(
      editable = true,
      canBulkChange = true,
      parser = ElemType.CUBE_CUBE_COLLISION,
      cat = ElementInformation.EIC.MODELS,
      order = 8
   )
   public boolean cubeCubeCollision;
   @org.schema.game.common.data.element.annotation.Element(
      editable = true,
      canBulkChange = true,
      parser = ElemType.LOD_USE_DETAIL_COLLISION,
      cat = ElementInformation.EIC.MODELS,
      order = 9
   )
   public boolean lodUseDetailCollision;
   @org.schema.game.common.data.element.annotation.Element(
      editable = true,
      canBulkChange = true,
      parser = ElemType.LOD_DETAIL_COLLISION,
      cat = ElementInformation.EIC.MODELS,
      order = 10
   )
   public ElementInformation.LodCollision lodDetailCollision;
   @org.schema.game.common.data.element.annotation.Element(
      editable = true,
      canBulkChange = true,
      parser = ElemType.LOW_HP_SETTING,
      cat = ElementInformation.EIC.DEPRECATED,
      order = 0
   )
   public boolean lowHpSetting;
   @org.schema.game.common.data.element.annotation.Element(
      from = 1,
      to = 127,
      editable = false,
      parser = ElemType.OLD_HITPOINTS,
      cat = ElementInformation.EIC.DEPRECATED,
      order = 0
   )
   public short oldHitpoints;
   @org.schema.game.common.data.element.annotation.Element(
      editable = true,
      canBulkChange = true,
      parser = ElemType.SYSTEM_BLOCK,
      cat = ElementInformation.EIC.BASICS,
      order = 15
   )
   public boolean systemBlock;
   @org.schema.game.common.data.element.annotation.Element(
      editable = true,
      canBulkChange = true,
      parser = ElemType.LOGIC_BLOCK,
      cat = ElementInformation.EIC.BASICS,
      order = 16
   )
   public boolean signal;
   @org.schema.game.common.data.element.annotation.Element(
      editable = true,
      canBulkChange = true,
      parser = ElemType.LOGIC_SIGNALED_BY_RAIL,
      cat = ElementInformation.EIC.BASICS,
      order = 17
   )
   public boolean signaledByRail;
   @org.schema.game.common.data.element.annotation.Element(
      editable = true,
      canBulkChange = true,
      parser = ElemType.LOGIC_BUTTON,
      cat = ElementInformation.EIC.BASICS,
      order = 18
   )
   public boolean button;
   public int wildcardIndex;
   private FixedRecipe productionRecipe;
   private double maxHitpointsInverse;
   private double maxHitpointsByteToFull;
   private double maxHitpointsFullToByte;
   private ShopItemElement shopItemElement;
   private boolean createdTX;
   public String idName;
   private boolean specialBlock;
   private final List rawConsistence;
   private final List totalConsistence;
   private final ElementCountMap rawBlocks;
   public boolean isSourceBlockTmp;
   private static final float AB = 0.007874016F;

   public ElementInformation(ElementInformation var1, short var2, String var3) {
      this.inRecipe = this.shoppable;
      this.individualSides = 1;
      this.sideTexturesPointToOrientation = false;
      this.physical = true;
      this.blockStyle = BlockStyle.NORMAL;
      this.drawLogicConnection = false;
      this.dynamicPrice = -1L;
      this.resourceInjection = ElementInformation.ResourceInjectionType.OFF;
      this.lightSourceColor = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
      this.lodShapeString = "";
      this.lodActivationAnimationStyle = 0;
      this.lodShapeStringActive = "";
      this.lodCollisionPhysical = true;
      this.lodShapeStyle = 0;
      this.lodCollision = new ElementInformation.LodCollision();
      this.cubeCubeCollision = true;
      this.lodDetailCollision = new ElementInformation.LodCollision();
      this.createdTX = false;
      this.specialBlock = true;
      this.rawConsistence = new ObjectArrayList();
      this.totalConsistence = new ObjectArrayList();
      this.rawBlocks = new ElementCountMap();
      this.name = new String(var3);
      this.type = var1.type;
      this.setTextureId(var1.textureId);
      this.id = var2;
      Field[] var8;
      int var9 = (var8 = ElementInformation.class.getFields()).length;

      for(int var4 = 0; var4 < var9; ++var4) {
         Field var5 = var8[var4];

         try {
            if (var5.get(var1) != null && !Modifier.isFinal(var5.getModifiers()) && !var5.getName().equals("name")) {
               var5.set(this, var5.get(var1));
            }
         } catch (IllegalArgumentException var6) {
            var6.printStackTrace();
         } catch (IllegalAccessException var7) {
            var7.printStackTrace();
         }
      }

      assert var1.getBlockStyle() == this.getBlockStyle();

   }

   public ElementInformation(short var1, String var2, ElementCategory var3, short[] var4) {
      this.inRecipe = this.shoppable;
      this.individualSides = 1;
      this.sideTexturesPointToOrientation = false;
      this.physical = true;
      this.blockStyle = BlockStyle.NORMAL;
      this.drawLogicConnection = false;
      this.dynamicPrice = -1L;
      this.resourceInjection = ElementInformation.ResourceInjectionType.OFF;
      this.lightSourceColor = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
      this.lodShapeString = "";
      this.lodActivationAnimationStyle = 0;
      this.lodShapeStringActive = "";
      this.lodCollisionPhysical = true;
      this.lodShapeStyle = 0;
      this.lodCollision = new ElementInformation.LodCollision();
      this.cubeCubeCollision = true;
      this.lodDetailCollision = new ElementInformation.LodCollision();
      this.createdTX = false;
      this.specialBlock = true;
      this.rawConsistence = new ObjectArrayList();
      this.totalConsistence = new ObjectArrayList();
      this.rawBlocks = new ElementCountMap();
      this.name = var2;
      this.type = var3;
      this.id = var1;
      this.setTextureId(var4);
   }

   public static BlockOrientation convertOrientation(ElementInformation var0, byte var1) {
      BlockOrientation var2;
      (var2 = new BlockOrientation()).blockOrientation = var1;
      var2.activateBlock = var0.activateOnPlacement();

      assert var2.blockOrientation < 24;

      if (var2.blockOrientation > 15 && var0.getBlockStyle() == BlockStyle.NORMAL) {
         System.err.println("[CLIENT][PLACEBLOCK] Exception: Block orientation was set over 8 on block style " + var0 + ": " + var2.blockOrientation);
         var2.blockOrientation = 0;
      }

      return var2;
   }

   public static byte defaultActive(short var0) {
      return (byte)(var0 != 16 && var0 != 32 && var0 != 48 && var0 != 40 && var0 != 30 && var0 != 24 ? 1 : 0);
   }

   public static String getKeyId(short var0) {
      Set var1 = ElementKeyMap.properties.entrySet();
      String var2 = null;
      Iterator var4 = var1.iterator();

      while(var4.hasNext()) {
         Entry var3;
         if ((var3 = (Entry)var4.next()).getValue().equals(String.valueOf(var0))) {
            var2 = var3.getKey().toString();
            break;
         }
      }

      return var2;
   }

   private static boolean calcIsSignal(short var0) {
      return var0 == 405 || var0 == 408 || var0 == 407 || var0 == 410 || var0 == 412 || var0 == 406 || var0 == 979 || ElementKeyMap.isButton(var0) || var0 == 667 || var0 == 668 || var0 == 670 || var0 == 409;
   }

   public static boolean isPhysical(SegmentPiece var0) {
      return isAlwaysPhysical(var0.getType()) || ElementKeyMap.getInfo(var0.getType()).isPhysical(var0.isActive());
   }

   public static boolean isAlwaysPhysical(short var0) {
      return !ElementKeyMap.isDoor(var0);
   }

   public static boolean isPhysical(short var0, SegmentData var1, int var2) {
      ElementInformation var3 = ElementKeyMap.getInfoFast(var0);
      return (!ElementKeyMap.isDoor(var0) || var1.isActive(var2)) && (!var3.hasLod() || var3.lodShapeStyle != 2);
   }

   public static boolean isPhysicalRayTests(short var0, SegmentData var1, int var2) {
      ElementKeyMap.getInfoFast(var0);
      return !ElementKeyMap.isDoor(var0) || var1.isActive(var2);
   }

   public static boolean isPhysicalFast(short var0, SegmentData var1, int var2) {
      return !ElementKeyMap.infoArray[var0].isDoor() || var1.isActive(var2);
   }

   public static void updatePhysical(short var0, short var1, boolean var2, byte var3, byte var4, byte var5, ArrayOctree var6, SegmentData var7, int var8) {
      if (var1 != var0 && ElementKeyMap.isDoor(var0)) {
         boolean var9;
         if ((var9 = var7.isActive(var8)) && !var2) {
            assert var0 != 938;

            var6.insert(var3, var4, var5, var8);
            return;
         }

         if (!var9 && var2) {
            var6.delete(var3, var4, var5, var8, var0);
         }
      }

   }

   public static boolean isBlendedSpecial(short var0, boolean var1) {
      return ElementKeyMap.isDoor(var0) && !var1 || var0 == 689;
   }

   public static boolean isVisException(ElementInformation var0, short var1, boolean var2) {
      return var0.isDoor() && var1 < 0;
   }

   public static byte activateOnPlacement(short var0) {
      return (byte)(var0 > 0 && ElementKeyMap.getInfo(var0).activateOnPlacement() ? 1 : 0);
   }

   public static boolean canBeControlledByAny(short var0) {
      if (!ElementKeyMap.isValidType(var0)) {
         return false;
      } else {
         return ElementKeyMap.getInfoFast(var0).getControlledBy().size() > 0 || var0 == 679 || var0 == 689 || var0 == 66 || ElementKeyMap.getInfoFast(var0).isLightSource() || ElementKeyMap.getInfoFast(var0).isInventory() || ElementKeyMap.getInfoFast(var0).isInventory() || ElementKeyMap.getInfoFast(var0).isInventory() || ElementKeyMap.getInfoFast(var0).isSensorInput() || var0 == 405 || var0 == 408 || var0 == 409 || var0 == 405 || ElementKeyMap.getInfoFast(var0).isSignal() || var0 == 685 || var0 == 120 || var0 == 120 || ElementKeyMap.getInfo(var0).isRailTrack() || var0 == 405 || ElementKeyMap.getInfo(var0).isSignal() || ElementKeyMap.getInfo(var0).isSignal() || ElementKeyMap.getInfoFast(var0).isSignal() || var0 == 663 || ElementKeyMap.getInfoFast(var0).canActivate() || ElementKeyMap.getInfoFast(var0).isRailTrack() || isLightConnectAny(var0) || ElementKeyMap.getInfoFast(var0).isMainCombinationControllerB() || ElementKeyMap.getInfoFast(var0).isMainCombinationControllerB();
      }
   }

   public static boolean canBeControlled(short var0, short var1) {
      if (ElementKeyMap.isValidType(var1) && ElementKeyMap.isValidType(var0)) {
         return var0 == 677 && var1 == 679 || var0 == 347 && var1 == 689 || var0 == 66 && var1 == 66 || var0 == 1 && ElementKeyMap.getInfoFast(var1).isLightSource() || ElementKeyMap.isToStashConnectable(var0) && ElementKeyMap.getInfoFast(var1).isInventory() || ElementKeyMap.getInfoFast(var0).isInventory() && ElementKeyMap.getInfoFast(var1).isInventory() || ElementKeyMap.getInfoFast(var0).isRailTrack() && ElementKeyMap.getInfoFast(var1).isInventory() || var0 == 980 && ElementKeyMap.getInfoFast(var1).isSensorInput() || var0 == 120 && var1 == 405 || var0 == 120 && var1 == 408 || var0 == 120 && var1 == 409 || ElementKeyMap.getInfo(var0).isRailRotator() && var1 == 405 || var0 == 685 && ElementKeyMap.getInfoFast(var1).isSignal() || ElementKeyMap.getInfoFast(var0).isSignal() && var1 == 685 || ElementKeyMap.getFactorykeyset().contains(var0) && var1 == 120 || var0 == 672 && ElementKeyMap.getInfo(var1).isRailTrack() || var0 == 672 && var1 == 405 || var0 == 413 && ElementKeyMap.getInfo(var1).isSignal() || var0 == 685 && ElementKeyMap.getInfo(var1).isSignal() || ElementKeyMap.getInfoFast(var0).isRailDockable() && ElementKeyMap.getInfoFast(var1).isSignal() || var0 == 360 && ElementKeyMap.getInfoFast(var1).isSignal() || ElementKeyMap.getInfoFast(var0).isSignal() && var1 == 663 || ElementKeyMap.getInfoFast(var0).isSignal() && ElementKeyMap.getInfoFast(var1).canActivate() || ElementKeyMap.getInfoFast(var0).isSignal() && ElementKeyMap.getInfoFast(var1).isRailTrack() || ElementKeyMap.getInfoFast(var0).isLightConnect(var1) || ElementKeyMap.getInfoFast(var0).isMainCombinationControllerB() && ElementKeyMap.getInfoFast(var1).isMainCombinationControllerB() || ElementKeyMap.getInfoFast(var0).isSupportCombinationControllerB() && ElementKeyMap.getInfoFast(var1).isMainCombinationControllerB();
      } else {
         return false;
      }
   }

   private boolean isSensorInput() {
      return this.isInventory() || this.isDoor() || this.sensorInput;
   }

   private static CharSequence getFactoryResourceString(ElementInformation var0) {
      if (var0.getFactory() == null) {
         return "CANNOT DISPLAY RESOURCES: NOT A FACTORY";
      } else {
         StringBuffer var1 = new StringBuffer();
         if (var0.getFactory().input != null) {
            var1.append("----------Factory Production--------------\n\n");

            assert var0.getFactory().input != null : var0;

            for(int var2 = 0; var2 < var0.getFactory().input.length; ++var2) {
               var1.append("----------Product-<" + (var2 + 1) + ">--------------\n");
               var1.append("--- Required Resources:\n");
               FactoryResource[] var3;
               int var4 = (var3 = var0.getFactory().input[var2]).length;

               int var5;
               FactoryResource var6;
               for(var5 = 0; var5 < var4; ++var5) {
                  var6 = var3[var5];
                  var1.append(var6.count + "x " + ElementKeyMap.getInfo(var6.type).getName() + "\n");
               }

               var1.append("\n\n--- Produces Resources:\n");
               var4 = (var3 = var0.getFactory().output[var2]).length;

               for(var5 = 0; var5 < var4; ++var5) {
                  var6 = var3[var5];
                  var1.append(var6.count + "x " + ElementKeyMap.getInfo(var6.type).getName() + "\n");
               }

               var1.append("\n");
            }

            var1.append("\n---------------------------------------------\n\n");
         }

         return var1.toString();
      }
   }

   public static boolean allowsMultiConnect(short var0) {
      return ElementKeyMap.isValidType(var0) && ElementKeyMap.getInfoFast(var0).isMultiControlled() && !ElementKeyMap.getInfoFast(var0).isRestrictedMultiControlled();
   }

   public static boolean isMedical(short var0) {
      return var0 == 446 || var0 == 445;
   }

   public boolean isProducedIn(short var1) {
      return !this.deprecated && (var1 == 213 && this.producedInFactory == 1 || var1 == 215 && this.producedInFactory == 2 || var1 == 211 && this.producedInFactory == 3 || var1 == 217 && this.producedInFactory == 4 || var1 == 259 && this.producedInFactory == 5);
   }

   public short getProducedInFactoryType() {
      switch(this.producedInFactory) {
      case 1:
         return 213;
      case 2:
         return 215;
      case 3:
         return 211;
      case 4:
         return 217;
      case 5:
         return 259;
      default:
         return 0;
      }
   }

   private String getDivString(float var1) {
      for(float var2 = 0.0F; var2 < 8.0F; ++var2) {
         if (var2 * 0.125F == var1) {
            return (int)var2 + "/8";
         }
      }

      if (var1 - (float)Math.round(var1) == 0.0F) {
         return String.valueOf(var1);
      } else {
         return StringTools.formatPointZeroZero(var1);
      }
   }

   public GUIGraphElement getGraphElement(InputState var1, String var2, int var3, int var4, int var5, int var6) {
      return this.getGraphElement(var1, var2, var3, var4, var5, var6, new Vector4f(0.17F, 0.27F, 0.37F, 1.0F));
   }

   public GUIGraphElement getGraphElement(InputState var1, String var2, int var3, int var4, int var5, int var6, Vector4f var7) {
      GUIColoredRectangle var11;
      (var11 = new GUIColoredRectangle(var1, (float)var5, (float)var6, var7)).rounded = 4.0F;
      GUITextOverlay var8;
      (var8 = new GUITextOverlay(var5, var6, FontLibrary.getBoldArial12White(), var1)).setColor(1.0F, 1.0F, 1.0F, 1.0F);
      var8.setTextSimple(var2);
      var8.setPos(5.0F, 5.0F, 0.0F);
      GUIBlockSprite var10;
      (var10 = new GUIBlockSprite(var1, this.getId())).getScale().set(0.5F, 0.5F, 0.5F);
      var10.getPos().x = (float)(var5 - 32);
      var10.getPos().y = (float)(var6 - 32);
      var11.attach(var10);
      var11.attach(var8);
      GUIGraphElement var9;
      (var9 = new GUIGraphElement(var1, var11)).setPos((float)var3, (float)var4, 0.0F);
      return var9;
   }

   public ShopItemElement getShopItemElement(InputState var1) {
      if (this.shopItemElement == null || this.shopItemElement.getState() != var1) {
         this.shopItemElement = new ShopItemElement(var1, this);
         this.shopItemElement.onInit();
      }

      return this.shopItemElement;
   }

   public GUIGraph getRecipeGraph(InputState var1) {
      GUIGraph var2 = new GUIGraph(var1);
      String var3 = this.getName();
      Vector4f var4 = new Vector4f(0.17F, 0.27F, 0.37F, 1.0F);
      if (this.getBasicResourceFactory() != 0) {
         if (this.getBasicResourceFactory() == 215) {
            var4.set(0.37F, 0.27F, 0.17F, 1.0F);
         } else {
            var4.set(0.37F, 0.17F, 0.27F, 1.0F);
         }

         var3 = var3 + StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_ELEMENTINFORMATION_2, ElementKeyMap.getInfo(this.getBasicResourceFactory()).getName());
      }

      GUIGraphElement var8 = var2.addVertex(this.getGraphElement(var1, var3, 20, 20, 130, 80, var4));
      int var9 = 0;
      Int2IntOpenHashMap var5 = new Int2IntOpenHashMap();

      for(Iterator var6 = this.consistence.iterator(); var6.hasNext(); ++var9) {
         FactoryResource var7;
         ElementKeyMap.getInfo((var7 = (FactoryResource)var6.next()).type).addRecipeGraph(var5, 1, var9, 130, 80, var2, var1, var8, (float)var7.count, (float)var7.count);
         var5.put(0, 1);
      }

      return var2;
   }

   private void addRecipeGraph(Int2IntOpenHashMap var1, int var2, int var3, int var4, int var5, GUIGraph var6, InputState var7, GUIGraphElement var8, float var9, float var10) {
      var3 = var1.get(var2);
      String var13 = this.getDivString(var9);
      String var11 = this.getDivString(var10);
      var13 = StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_ELEMENTINFORMATION_3, this.getName(), var13, var11);
      Vector4f var15 = new Vector4f(0.17F, 0.27F, 0.37F, 1.0F);
      if (this.getBasicResourceFactory() != 0) {
         if (this.getBasicResourceFactory() == 215) {
            var15.set(0.37F, 0.27F, 0.17F, 1.0F);
         } else {
            var15.set(0.37F, 0.17F, 0.27F, 1.0F);
         }

         var13 = var13 + StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_ELEMENTINFORMATION_4, ElementKeyMap.getInfo(this.getBasicResourceFactory()).getName());
      }

      GUIGraphElement var14 = var6.addVertex(this.getGraphElement(var7, var13, var3 * (var4 + 10), var2 * ((var5 << 1) + 10), var4, var5, var15));
      var6.addConnection(var14, var8);
      var1.put(var2, var3 + 1);
      var3 = 0;

      for(Iterator var12 = this.consistence.iterator(); var12.hasNext(); ++var3) {
         FactoryResource var16 = (FactoryResource)var12.next();
         System.err.println("ADD CONSISTENCE NORMAL " + var16);
         ElementKeyMap.getInfo(var16.type).addRecipeGraph(var1, var2 + 1, var3, var4, var5, var6, var7, var14, (float)var16.count, (float)var16.count * var10);
      }

   }

   private void addGraph(Int2IntOpenHashMap var1, int var2, int var3, int var4, int var5, mxGraph var6, Object var7, Object var8, float var9, float var10) {
      var3 = var1.get(var2);
      String var15 = this.getDivString(var9);
      String var11 = this.getDivString(var10);
      var15 = this.getName() + "\n(x" + var15 + ")\ntot(x" + var11 + ")";
      Object var16 = var6.insertVertex(var8, (String)null, var15, (double)(var2 * ((var4 << 1) + 10)), (double)(var3 * (var5 + 10)), (double)var4, (double)var5);
      var6.insertEdge(var8, (String)null, "", var16, var7);
      var1.put(var2, var3 + 1);
      var3 = 0;
      if (this.isCapsule()) {
         System.err.println("CAPSULE " + this + "; Consistence " + this.consistence);
         if (this.consistence.size() > 0) {
            FactoryResource var10000 = (FactoryResource)this.consistence.get(0);
            var7 = null;
            ElementInformation var17 = ElementKeyMap.getInfo(var10000.type);
            System.err.println("CAPSULE " + this + "; Consistence " + this.consistence + " split-> " + var17.cubatomConsistence);
            Iterator var12 = var17.cubatomConsistence.iterator();

            while(var12.hasNext()) {
               FactoryResource var13;
               if ((var13 = (FactoryResource)var12.next()).type == this.getId()) {
                  System.err.println("ADDING CUBATOM CONSISTENS FOR " + this + " -> " + ElementKeyMap.getInfo(var13.type));
                  var17.addGraph(var1, var2 + 1, 0, var4, var5, var6, var16, var8, 1.0F / (float)var13.count, 1.0F / (float)var13.count * var10);
               }
            }

            return;
         }
      } else {
         for(Iterator var14 = this.consistence.iterator(); var14.hasNext(); ++var3) {
            FactoryResource var18 = (FactoryResource)var14.next();
            System.err.println("ADD CONSISTENCE NORMAL " + var18);
            ElementKeyMap.getInfo(var18.type).addGraph(var1, var2 + 1, var3, var4, var5, var6, var16, var8, (float)var18.count, (float)var18.count * var10);
         }
      }

   }

   public JPanel getGraph() {
      JPanel var1 = new JPanel(new GridBagLayout());
      mxGraph var2;
      Object var3 = (var2 = new mxGraph()).getDefaultParent();
      var2.setCellsEditable(false);
      var2.setConnectableEdges(false);
      var2.getModel().beginUpdate();

      try {
         Object var4 = var2.insertVertex(var3, String.valueOf(this.getId()), this.getName(), 20.0D, 20.0D, 100.0D, 180.0D);
         int var5 = 0;
         Int2IntOpenHashMap var6 = new Int2IntOpenHashMap();
         if (this.isCapsule()) {
            System.err.println("CAPSULE " + this + "; Consistence " + this.consistence);
            if (this.consistence.size() > 0) {
               FactoryResource var10000 = (FactoryResource)this.consistence.get(0);
               FactoryResource var7 = null;
               ElementInformation var8 = ElementKeyMap.getInfo(var10000.type);
               System.err.println("CAPSULE " + this + "; Consistence " + this.consistence + " split-> " + var8.cubatomConsistence);
               Iterator var12 = var8.cubatomConsistence.iterator();

               while(var12.hasNext()) {
                  if ((var7 = (FactoryResource)var12.next()).type == this.getId()) {
                     System.err.println("ADDING CUBATOM CONSISTENS FOR " + this + " -> " + ElementKeyMap.getInfo(var7.type));
                     var8.addGraph(var6, 1, 0, 100, 180, var2, var4, var3, 1.0F / (float)var7.count, 1.0F / (float)var7.count);
                  }
               }
            }
         } else {
            for(Iterator var14 = this.consistence.iterator(); var14.hasNext(); ++var5) {
               FactoryResource var15;
               ElementKeyMap.getInfo((var15 = (FactoryResource)var14.next()).type).addGraph(var6, 1, var5, 100, 180, var2, var4, var3, (float)var15.count, (float)var15.count);
               var6.put(0, 1);
            }
         }
      } finally {
         var2.getModel().endUpdate();
      }

      mxGraphComponent var11 = new mxGraphComponent(var2);
      GridBagConstraints var13;
      (var13 = new GridBagConstraints()).anchor = 12;
      var13.gridx = 0;
      var13.gridy = 0;
      var13.weightx = 1.0D;
      var13.weighty = 1.0D;
      var13.fill = 1;
      var1.add(var11, var13);
      return var1;
   }

   private void addChamberGraph(Int2IntOpenHashMap var1, int var2, int var3, int var4, int var5, mxGraph var6, Object var7, Object var8) {
      var3 = var1.get(var2);
      String var9 = "[" + this.getName() + "]";

      String var11;
      for(Iterator var10 = this.chamberConfigGroupsLowerCase.iterator(); var10.hasNext(); var9 = var9 + "\n" + var11) {
         var11 = (String)var10.next();
      }

      Object var14 = var6.insertVertex(var8, (String)null, var9, (double)(var2 * (var4 + 35)), (double)(var3 * (var5 + 10)), (double)var4, (double)var5);
      var6.insertEdge(var8, (String)null, "", var7, var14);
      var1.put(var2, var3 + 1);
      var3 = 0;

      for(Iterator var12 = this.chamberChildren.iterator(); var12.hasNext(); ++var3) {
         short var10000 = (Short)var12.next();
         boolean var13 = false;
         ElementInformation var15 = ElementKeyMap.getInfo(var10000);
         var9 = null;
         var15.addChamberGraph(var1, var2 + 1, var3, var4, var5, var6, var14, var8);
         var1.put(0, 1);
      }

   }

   public JPanel getChamberGraph() {
      assert this.isReactorChamberGeneral();

      JPanel var1 = new JPanel(new GridBagLayout());
      mxGraph var2;
      Object var3 = (var2 = new mxGraph()).getDefaultParent();
      var2.setCellsEditable(false);
      var2.setConnectableEdges(true);
      var2.getModel().beginUpdate();
      var2.setAllowLoops(false);
      var2.getStylesheet().getDefaultEdgeStyle().put(mxConstants.STYLE_EDGE, "elbowEdgeStyle");

      try {
         Object var4 = var2.insertVertex(var3, String.valueOf(this.getId()), this.getName(), 20.0D, 20.0D, 230.0D, 30.0D);
         int var5 = 0;
         Int2IntOpenHashMap var6 = new Int2IntOpenHashMap();

         for(Iterator var7 = this.chamberChildren.iterator(); var7.hasNext(); ++var5) {
            ElementKeyMap.getInfo((Short)var7.next()).addChamberGraph(var6, 1, var5, 230, 30, var2, var4, var3);
            var6.put(0, 1);
         }
      } finally {
         var2.getModel().endUpdate();
      }

      (new mxParallelEdgeLayout(var2)).execute(var3);
      mxGraphComponent var10 = new mxGraphComponent(var2);
      GridBagConstraints var11;
      (var11 = new GridBagConstraints()).anchor = 12;
      var11.gridx = 0;
      var11.gridy = 0;
      var11.weightx = 1.0D;
      var11.weighty = 1.0D;
      var11.fill = 1;
      var1.add(var10, var11);
      return var1;
   }

   public long calculateDynamicPrice() {
      if (this.dynamicPrice < 0L) {
         long var1 = 0L;
         if (!this.consistence.isEmpty()) {
            Iterator var3 = this.consistence.iterator();

            while(var3.hasNext()) {
               FactoryResource var4 = (FactoryResource)var3.next();
               var1 += (long)var4.count * ElementKeyMap.getInfo(var4.type).calculateDynamicPrice();
               if (ElementKeyMap.getInfo(var4.type).consistence.isEmpty()) {
                  var1 = (long)((float)var1 * (Float)ServerConfig.DYNAMIC_RECIPE_PRICE_MODIFIER.getCurrentState());
               }
            }
         } else {
            var1 = this.price;
         }

         this.dynamicPrice = var1;
      }

      return this.dynamicPrice;
   }

   public boolean isCapsule() {
      return this.blockResourceType == 6;
   }

   public boolean isOre() {
      return this.blockResourceType == 0;
   }

   public void appendXML(Document var1, org.w3c.dom.Element var2) throws CannotAppendXMLException {
      String var3 = "Block";
      org.w3c.dom.Element var18 = var1.createElement(var3);
      String var4;
      if ((var4 = getKeyId(this.getId())) == null) {
         throw new CannotAppendXMLException("Cannot find property key for Block ID " + this.getId() + "; Check your Block properties file");
      } else {
         var18.setAttribute("type", var4);
         var18.setAttribute("icon", String.valueOf(this.getBuildIconNum()));
         var18.setAttribute("textureId", String.valueOf(StringTools.getCommaSeperated(this.getTextureIds())));
         var18.setAttribute("name", this.name);
         Field[] var19;
         int var5 = (var19 = ElementInformation.class.getFields()).length;

         for(int var6 = 0; var6 < var5; ++var6) {
            Field var7 = var19[var6];

            try {
               var7.setAccessible(true);
               if (var7.get(this) == null) {
                  continue;
               }
            } catch (IllegalArgumentException var16) {
               var16.printStackTrace();
               throw new CannotAppendXMLException(var16.getMessage());
            } catch (IllegalAccessException var17) {
               var17.printStackTrace();
               throw new CannotAppendXMLException(var17.getMessage());
            }

            org.schema.game.common.data.element.annotation.Element var8;
            if ((var8 = (org.schema.game.common.data.element.annotation.Element)var7.getAnnotation(org.schema.game.common.data.element.annotation.Element.class)) != null && var8.writeAsTag()) {
               org.w3c.dom.Element var9 = var1.createElement(var8.parser().tag);

               try {
                  int var10;
                  org.w3c.dom.Element var13;
                  if (var8.factory()) {
                     if (this.getFactory().input == null) {
                        var9.setTextContent("INPUT");
                     } else {
                        for(var10 = 0; var10 < this.getFactory().input.length; ++var10) {
                           org.w3c.dom.Element var11 = var1.createElement("Product");
                           org.w3c.dom.Element var12 = var1.createElement("Input");
                           var13 = var1.createElement("Output");

                           int var14;
                           FactoryResource var20;
                           for(var14 = 0; var14 < this.getFactory().input[var10].length; ++var14) {
                              var20 = this.getFactory().input[var10][var14];
                              var12.appendChild(var20.getNode(var1));
                           }

                           for(var14 = 0; var14 < this.getFactory().output[var10].length; ++var14) {
                              var20 = this.getFactory().output[var10][var14];
                              var13.appendChild(var20.getNode(var1));
                           }

                           var11.appendChild(var12);
                           var11.appendChild(var13);
                           var9.appendChild(var11);
                        }
                     }
                  } else if (var8.type()) {
                     short var23 = var7.getShort(this);
                     var9.setTextContent(String.valueOf(var23));
                  } else if (!var8.cubatom()) {
                     FactoryResource var24;
                     if (var8.consistence()) {
                        for(var10 = 0; var10 < this.getConsistence().size(); ++var10) {
                           var24 = (FactoryResource)this.getConsistence().get(var10);
                           var9.appendChild(var24.getNode(var1));
                        }
                     } else if (var8.cubatomConsistence()) {
                        for(var10 = 0; var10 < this.cubatomConsistence.size(); ++var10) {
                           var24 = (FactoryResource)this.cubatomConsistence.get(var10);
                           var9.appendChild(var24.getNode(var1));
                        }
                     } else if (var8.vector3f()) {
                        Vector3f var26 = (Vector3f)var7.get(this);
                        var9.setTextContent(var26.x + "," + var26.y + "," + var26.z);
                     } else if (var8.vector4f()) {
                        Vector4f var28 = (Vector4f)var7.get(this);
                        var9.setTextContent(var28.x + "," + var28.y + "," + var28.z + "," + var28.w);
                     } else {
                        StringBuffer var25;
                        int var27;
                        if (var7.getType().equals(short[].class)) {
                           short[] var30 = (short[])var7.get(this);
                           var25 = new StringBuffer();

                           for(var27 = 0; var27 < var30.length; ++var27) {
                              var25.append(var30[var27]);
                              if (var27 < var30.length - 1) {
                                 var25.append(", ");
                              }
                           }

                           var9.setTextContent(var25.toString());
                        } else if (var7.getType().equals(int[].class)) {
                           int[] var32 = (int[])var7.get(this);
                           var25 = new StringBuffer();

                           for(var27 = 0; var27 < var32.length; ++var27) {
                              var25.append(var32[var27]);
                              if (var27 < var32.length - 1) {
                                 var25.append(", ");
                              }
                           }

                           var9.setTextContent(var25.toString());
                        } else {
                           Iterator var29;
                           Collection var33;
                           if (var8.collectionType().toLowerCase(Locale.ENGLISH).equals("blocktypes")) {
                              if ((var33 = (Collection)var7.get(this)).isEmpty()) {
                                 continue;
                              }

                              var29 = var33.iterator();

                              while(var29.hasNext()) {
                                 Short var35 = (Short)var29.next();
                                 var13 = var1.createElement(var8.collectionElementTag());
                                 String var40;
                                 if ((var40 = getKeyId(var35)) == null) {
                                    throw new CannotAppendXMLException("[BlockSet] " + var7.getName() + " Cannot find property key for Block ID " + var35 + "; Check your Block properties file");
                                 }

                                 var13.setTextContent(var40);
                                 var9.appendChild(var13);
                              }
                           } else if (var7.getType().equals(InterEffectSet.class)) {
                              InterEffectSet var36 = (InterEffectSet)var7.get(this);
                              InterEffectHandler.InterEffectType[] var31;
                              var27 = (var31 = InterEffectHandler.InterEffectType.values()).length;

                              for(int var34 = 0; var34 < var27; ++var34) {
                                 InterEffectHandler.InterEffectType var42 = var31[var34];
                                 org.w3c.dom.Element var21 = var1.createElement(var42.id);
                                 String var22 = String.valueOf(var36.getStrength(var42));
                                 var21.setTextContent(var22);
                                 var9.appendChild(var21);
                              }
                           } else if (var8.collectionType().toLowerCase(Locale.ENGLISH).equals("string")) {
                              if ((var33 = (Collection)var7.get(this)).isEmpty()) {
                                 continue;
                              }

                              var29 = var33.iterator();

                              while(var29.hasNext()) {
                                 String var37 = (String)var29.next();
                                 (var13 = var1.createElement(var8.collectionElementTag())).setTextContent(var37);
                                 var9.appendChild(var13);
                              }
                           } else if (var7.getType() == BlockStyle.class) {
                              BlockStyle var38 = (BlockStyle)var7.get(this);
                              var9.setTextContent(String.valueOf(var38.id));
                              var9.appendChild(var1.createComment(var38.realName + ": " + var38.desc));
                           } else if (var7.getType() == ElementInformation.LodCollision.class) {
                              ((ElementInformation.LodCollision)var7.get(this)).write(var9, var1);
                           } else if (var7.getType() == ElementInformation.ResourceInjectionType.class) {
                              ElementInformation.ResourceInjectionType var39 = (ElementInformation.ResourceInjectionType)var7.get(this);
                              var9.setTextContent(String.valueOf(var39.ordinal()));
                              var9.appendChild(var1.createComment(var39.name().toLowerCase(Locale.ENGLISH)));
                           } else {
                              String var41 = var7.get(this).toString();
                              if (var8.textArea()) {
                                 var41 = var41.replace("\n", "\\n\\r").replaceAll("\\r", "\r");
                              }

                              if (var41.length() == 0) {
                                 continue;
                              }

                              var9.setTextContent(var41);
                           }
                        }
                     }
                  }
               } catch (Exception var15) {
                  var15.printStackTrace();
                  throw new CannotAppendXMLException(var15.getMessage());
               }

               var18.appendChild(var9);
            }
         }

         var2.appendChild(var18);
      }
   }

   public short[] getTextureIds() {
      return this.textureId;
   }

   public boolean canActivate() {
      return this.canActivate;
   }

   public int compareTo(ElementInformation var1) {
      return this.name.compareTo(var1.name);
   }

   public BlockStyle getBlockStyle() {
      return this.blockStyle;
   }

   public void setBlockStyle(int var1) throws ElementParserException {
      this.blockStyle = BlockStyle.getById(var1);
   }

   public int getBuildIconNum() {
      return this.buildIconNum;
   }

   public void setBuildIconNum(int var1) {
      this.buildIconNum = var1;
   }

   public Set getControlledBy() {
      return this.controlledBy;
   }

   public Set getControlling() {
      return this.controlling;
   }

   public String getDescription() {
      String var1;
      return (var1 = (String)ElementKeyMap.descriptionTranslations.get(this.id)) != null ? var1 : this.description;
   }

   public void setDescription(String var1) {
      this.description = var1;
   }

   public byte getExtraOrientation() {
      return 0;
   }

   public BlockFactory getFactory() {
      return this.factory;
   }

   public void setFactory(BlockFactory var1) {
      this.factory = var1;
   }

   public String getFullName() {
      return this.fullName == null ? this.getName() : this.fullName;
   }

   public void setFullName(String var1) {
      this.fullName = var1;
   }

   public short getId() {
      return this.id;
   }

   public int getIndividualSides() {
      return this.individualSides;
   }

   public void setIndividualSides(int var1) {
      this.individualSides = var1;
   }

   public Vector4f getLightSourceColor() {
      return this.lightSourceColor;
   }

   public int getMaxHitPointsFull() {
      return this.maxHitPointsFull;
   }

   public String getName() {
      String var1;
      return (var1 = (String)ElementKeyMap.nameTranslations.get(this.id)) != null ? var1 : this.name;
   }

   public String getNameUntranslated() {
      return this.name;
   }

   public long getPrice(boolean var1) {
      return !var1 ? this.price : this.dynamicPrice;
   }

   public List getRecipeBuyResources() {
      return this.recipeBuyResources;
   }

   public short getTextureId(int var1) {
      return this.textureId[var1];
   }

   public ElementCategory getType() {
      return this.type;
   }

   public int hashCode() {
      return this.getId();
   }

   public boolean equals(Object var1) {
      return ((ElementInformation)var1).getId() == this.getId();
   }

   public String toString() {
      return this.getName() + "(" + this.getId() + ")";
   }

   public boolean isAnimated() {
      return this.animated;
   }

   public void setAnimated(boolean var1) {
      this.animated = var1;
   }

   public boolean isBlended() {
      return this.blended;
   }

   public void setBlended(boolean var1) {
      this.blended = var1;
   }

   public boolean isController() {
      return !this.getControlling().isEmpty() || this.controlsAll();
   }

   public boolean isOldDockable() {
      return this.getId() == 7 || this.getId() == 289;
   }

   public boolean isEnterable() {
      return this.enterable;
   }

   public void setEnterable(boolean var1) {
      this.enterable = var1;
   }

   public boolean isInRecipe() {
      return this.inRecipe;
   }

   public void setInRecipe(boolean var1) {
      this.inRecipe = var1;
   }

   public boolean isLightSource() {
      return this.lightSource;
   }

   public void setLightSource(boolean var1) {
      this.lightSource = var1;
   }

   public boolean isOrientatable() {
      return this.orientatable;
   }

   public void setOrientatable(boolean var1) {
      this.orientatable = var1;
   }

   public boolean isPhysical() {
      return this.physical;
   }

   public void setPhysical(boolean var1) {
      this.physical = var1;
   }

   public boolean isPhysical(boolean var1) {
      return this.isDoor() ? var1 : this.physical;
   }

   public boolean isPlacable() {
      return this.placable;
   }

   public void setPlacable(boolean var1) {
      this.placable = var1;
   }

   public boolean isShoppable() {
      return this.shoppable;
   }

   public void setShoppable(boolean var1) {
      this.shoppable = var1;
   }

   public void setCanActivate(boolean var1) {
      this.canActivate = var1;
   }

   public void setPrice(long var1) {
      this.price = var1;
   }

   public int getDefaultOrientation() {
      if (this.getId() == 689) {
         return 4;
      } else if (this.getId() == 688) {
         return 2;
      } else if (this.getBlockStyle() != BlockStyle.SPRITE && this.getIndividualSides() != 3) {
         if (this.getBlockStyle() == BlockStyle.NORMAL24) {
            return 14;
         } else {
            return this.getId() == 56 ? 3 : 0;
         }
      } else {
         return 2;
      }
   }

   public boolean isBlendBlockStyle() {
      return this.getBlockStyle().blendedBlockStyle || this.hasLod() && this.lodShapeStyle == 1;
   }

   public boolean controlsAll() {
      return this.isSignal();
   }

   public boolean isSignal() {
      return this.signal;
   }

   public boolean isHasActivationTexure() {
      return this.hasActivationTexure;
   }

   public void setHasActivationTexure(boolean var1) {
      this.hasActivationTexure = var1;
   }

   public boolean drawConnection() {
      return EngineSettings.G_DRAW_ALL_CONNECTIONS.isOn() || EngineSettings.G_DRAW_ANY_CONNECTIONS.isOn() && this.drawLogicConnection;
   }

   public boolean isSupportCombinationControllerB() {
      return this.supportCombinationController;
   }

   public void setSupportCombinationController(boolean var1) {
      this.supportCombinationController = var1;
   }

   public boolean isMainCombinationControllerB() {
      return this.mainCombinationController;
   }

   public void setMainCombinationController(boolean var1) {
      this.mainCombinationController = var1;
   }

   public boolean isCombiConnectSupport(short var1) {
      if (ElementKeyMap.isValidType(var1)) {
         ElementInformation var2 = ElementKeyMap.getInfo(var1);
         if (this.isMainCombinationControllerB() && var2.isMainCombinationControllerB()) {
            return true;
         }

         if (this.isSupportCombinationControllerB() && var2.isMainCombinationControllerB()) {
            return true;
         }
      }

      return false;
   }

   public boolean isCombiConnectEffect(short var1) {
      ElementInformation var2 = ElementKeyMap.getInfo(var1);
      return this.isMainCombinationControllerB() && var2.isEffectCombinationController();
   }

   public boolean isEffectCombinationController() {
      return this.effectCombinationController;
   }

   public void setEffectCombinationController(boolean var1) {
      this.effectCombinationController = var1;
   }

   public boolean isCombiConnectAny(short var1) {
      return this.isCombiConnectSupport(var1) || this.isCombiConnectEffect(var1) || this.isLightConnect(var1);
   }

   public static boolean isLightConnectAny(short var0) {
      return ElementKeyMap.isValidType(var0) && ElementKeyMap.getInfo(var0).isLightSource();
   }

   public boolean isLightConnect(short var1) {
      if (ElementKeyMap.isValidType(var1)) {
         ElementInformation var2 = ElementKeyMap.getInfo(var1);
         if (this.isMainCombinationControllerB() && var2.isLightSource()) {
            return true;
         }
      }

      return false;
   }

   public boolean activateOnPlacement() {
      return !this.isSignal() && (ElementKeyMap.getFactorykeyset().contains(this.id) || this.isLightSource() || this.isDoor() || this.getId() == 120 || this.getId() == 937);
   }

   public String[] parseDescription(GameClientState var1) {
      String var2 = this.getDescription();
      var2 = Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_ELEMENTINFORMATION_5 + "<?> \n\n" + var2;
      var2 = Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_ELEMENTINFORMATION_6 + this.getMaxHitPointsFull() + " \n" + var2;
      String var10000 = Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_ELEMENTINFORMATION_7;
      var2 = Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_ELEMENTINFORMATION_8 + this.structureHP + " \n" + var2;
      String[] var4 = (Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_ELEMENTINFORMATION_9 + StringTools.formatPointZeroZero(this.getMass()) + " \n" + var2).split("\\n");

      for(int var3 = 0; var3 < var4.length; ++var3) {
         var4[var3] = var4[var3].replace("$ACTIVATE", KeyboardMappings.ACTIVATE.getKeyChar());
         if (var4[var3].contains("$RESOURCES")) {
            var4[var3] = var4[var3].replace("$RESOURCES", getFactoryResourceString(this));
         }

         if (var4[var3].contains("$ARMOUR")) {
            var4[var3] = var4[var3].replace("$ARMOUR", "");
         }

         if (var4[var3].contains("$HP")) {
            var4[var3] = var4[var3].replace("$HP", String.valueOf(this.getMaxHitPointsFull()));
         }

         if (var4[var3].contains("$EFFECT")) {
            if ((new ShipManagerContainer(var1, new Ship(var1))).getEffect(this.getId()) != null) {
               var4[var3] = var4[var3].replace("$EFFECT", StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_ELEMENTINFORMATION_0, KeyboardMappings.SELECT_MODULE.getKeyChar(), KeyboardMappings.CONNECT_MODULE.getKeyChar(), "[PLACEHOLDER]"));
            } else {
               var4[var3] = var4[var3].replace("$EFFECT", "NO_EFFECT(invalid $ var)");
            }
         }

         if (var4[var3].contains("$MAINCOMBI")) {
            var4[var3] = var4[var3].replace("$MAINCOMBI", StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_ELEMENTINFORMATION_1, KeyboardMappings.SELECT_MODULE.getKeyChar(), KeyboardMappings.CONNECT_MODULE.getKeyChar()));
         }
      }

      return var4;
   }

   public boolean isDeprecated() {
      return this.deprecated;
   }

   public void setDeprecated(boolean var1) {
      this.deprecated = var1;
   }

   public List getConsistence() {
      return this.consistence;
   }

   public RecipeInterface getProductionRecipe() {
      if (this.productionRecipe == null) {
         this.productionRecipe = new FixedRecipe();
         this.productionRecipe.costAmount = -1;
         this.productionRecipe.costType = -1;
         if (this.consistence.isEmpty()) {
            this.productionRecipe.recipeProducts = new FixedRecipeProduct[0];
         } else if (this.isCapsule()) {
            ElementInformation var1 = ElementKeyMap.getInfo(((FactoryResource)this.consistence.get(0)).type);
            this.productionRecipe.recipeProducts = new FixedRecipeProduct[1];
            this.productionRecipe.recipeProducts[0] = new FixedRecipeProduct();
            this.productionRecipe.recipeProducts[0].input = new FactoryResource[1];
            this.productionRecipe.recipeProducts[0].input[0] = new FactoryResource(1, var1.getId());
            this.productionRecipe.recipeProducts[0].output = new FactoryResource[var1.cubatomConsistence.size()];
            int var2 = 0;

            for(Iterator var3 = var1.cubatomConsistence.iterator(); var3.hasNext(); ++var2) {
               FactoryResource var4 = (FactoryResource)var3.next();
               this.productionRecipe.recipeProducts[0].output[var2] = var4;
            }
         } else {
            this.productionRecipe.recipeProducts = new FixedRecipeProduct[1];
            this.productionRecipe.recipeProducts[0] = new FixedRecipeProduct();
            this.productionRecipe.recipeProducts[0].input = new FactoryResource[this.consistence.size()];
            int var5 = 0;

            for(Iterator var6 = this.consistence.iterator(); var6.hasNext(); ++var5) {
               FactoryResource var7 = (FactoryResource)var6.next();

               assert var7 != null;

               this.productionRecipe.recipeProducts[0].input[var5] = var7;
            }

            this.productionRecipe.recipeProducts[0].output = new FactoryResource[1];
            this.productionRecipe.recipeProducts[0].output[0] = new FactoryResource(1, this.getId());
         }
      }

      return this.productionRecipe;
   }

   public double getMaxHitpointsFullInverse() {
      return this.maxHitpointsInverse;
   }

   public double getMaxHitpointsByteToFull() {
      return this.maxHitpointsByteToFull;
   }

   public double getMaxHitpointsFullToByte() {
      return this.maxHitpointsFullToByte;
   }

   public short getBasicResourceFactory() {
      return this.basicResourceFactory;
   }

   public void setBasicResourceFactory(short var1) {
      this.basicResourceFactory = var1;
   }

   public float getExplosionAbsorbtion() {
      return this.explosionAbsorbtion;
   }

   public void setExplosionAbsorbtion(float var1) {
      this.explosionAbsorbtion = var1;
   }

   public float getFactoryBakeTime() {
      return this.factoryBakeTime;
   }

   public void setFactoryBakeTime(float var1) {
      this.factoryBakeTime = var1;
   }

   public boolean isMultiControlled() {
      return this.isSignal() || this.isInventory() || this.isRailTrack() || this.isSensorInput() || this.getId() == 479;
   }

   public boolean isRestrictedMultiControlled() {
      return this.getId() == 689;
   }

   public ShortArrayList getRestrictedMultiControlled() {
      if (this.getId() == 689) {
         return ElementKeyMap.inventoryTypes;
      } else {
         assert false : this;

         return null;
      }
   }

   public String createWikiStub() {
      StringBuffer var1;
      (var1 = new StringBuffer()).append("{{infobox block\n");
      var1.append("  |type=" + this.getName() + "\n");
      var1.append("\t|hp=" + this.getMaxHitPointsFull() + "\n");
      var1.append("\t|armor=-\n");
      var1.append("\t|light=" + (this.isLightSource() ? "yes" : "no") + "\n");
      if (this.isLightSource()) {
         var1.append("\t|lightColor=" + this.getLightSourceColor() + "\n");
      }

      var1.append("\t|dv=" + this.getId() + "\n");
      var1.append("}}\n\n");
      return var1.toString();
   }

   public boolean isDoor() {
      return this.door;
   }

   public void setDoor(boolean var1) {
      this.door = var1;
   }

   public int getProducedInFactory() {
      return this.producedInFactory;
   }

   public void setProducedInFactory(int var1) {
      this.producedInFactory = var1;
   }

   public String getInventoryGroup() {
      return this.inventoryGroup;
   }

   public void setInventoryGroup(String var1) {
      this.inventoryGroup = var1.trim();
   }

   public boolean hasInventoryGroup() {
      return this.inventoryGroup.length() > 0;
   }

   public boolean isNormalBlockStyle() {
      return this.blockStyle.cube;
   }

   public boolean isRailTrack() {
      return this.blockStyle == BlockStyle.NORMAL24 && !this.hasLod() && this.id != 663 && this.id != 665 && this.id != 679 && this.id != 937 && this.id != 678;
   }

   public boolean isRailShipyardCore() {
      return this.id == 679;
   }

   public boolean isRailRotator() {
      return this.id == 664 || this.id == 669;
   }

   public boolean isRailTurret() {
      return this.id == 665;
   }

   public boolean isRailDockable() {
      return this.isRailTrack() || this.isRailRotator() || this.isRailTurret();
   }

   void recreateTextureMapping() {
      for(byte var1 = 0; var1 < 6; ++var1) {
         this.textureLayerMapping[var1] = this.calcTextureLayerCode(false, var1);
         this.textureIndexLocalMapping[var1] = this.calcTextureIndexLocalCode(false, var1);
         this.textureLayerMappingActive[var1] = this.calcTextureLayerCode(true, var1);
         this.textureIndexLocalMappingActive[var1] = this.calcTextureIndexLocalCode(true, var1);
      }

      this.createdTX = true;
   }

   public byte getTextureLayer(boolean var1, byte var2) {
      assert this.createdTX;

      return (byte)(var1 ? this.textureLayerMappingActive[var2] : this.textureLayerMapping[var2]);
   }

   public short getTextureIndexLocal(boolean var1, byte var2) {
      assert this.createdTX;

      return (short)(var1 ? this.textureIndexLocalMappingActive[var2] : this.textureIndexLocalMapping[var2]);
   }

   private byte calcTextureLayerCode(boolean var1, byte var2) {
      return (byte)(Math.abs(this.getTextureId(var1, var2)) / 256);
   }

   private short calcTextureIndexLocalCode(boolean var1, byte var2) {
      return (short)(this.getTextureId(var1, var2) % 256);
   }

   private short getTextureId(boolean var1, int var2) {
      assert this.id != 405 || this.hasActivationTexure;

      return this.hasActivationTexure && !var1 ? (short)(this.textureId[var2] + 1) : this.textureId[var2];
   }

   public void setTextureId(short[] var1) {
      this.textureId = Arrays.copyOf(var1, var1.length);
      this.createdTX = false;
   }

   public void setTextureId(int var1, short var2) {
      this.textureId[var1] = var2;
      this.createdTX = false;
   }

   public void normalizeTextureIds() {
      short var1 = this.textureId[0];
      int var2;
      if (this.individualSides >= 6) {
         for(var2 = 0; var2 < 6; ++var2) {
            this.textureId[var2] = (short)(var1 + var2);
         }
      } else if (this.individualSides == 3) {
         this.textureId[2] = var1;
         this.textureId[3] = (short)(var1 + 1);
         this.textureId[5] = (short)(var1 + 2);
         this.textureId[4] = (short)(var1 + 2);
         this.textureId[0] = (short)(var1 + 2);
         this.textureId[1] = (short)(var1 + 2);
      } else {
         for(var2 = 1; var2 < 6; ++var2) {
            this.textureId[var2] = var1;
         }
      }

      this.recreateTextureMapping();
   }

   public boolean isRailDocker() {
      return this.id == 663;
   }

   public boolean isRailSpeedActivationConnect(short var1) {
      return this.id == 672 && var1 == 405;
   }

   public boolean isRailSpeedTrackConnect(short var1) {
      return this.id == 672 && ElementKeyMap.isValidType(var1) && ElementKeyMap.getInfo(var1).isRailTrack();
   }

   public boolean needsCoreConnectionToWorkOnHotbar() {
      return this.id != 663 && this.id != 670 && this.id != 978;
   }

   public float getMass() {
      return this.mass;
   }

   public int getSlab() {
      return this.slab;
   }

   public int getSlab(int var1) {
      return this.id == 689 ? var1 : this.slab;
   }

   public float getVolume() {
      return this.volume;
   }

   public boolean isInventory() {
      return this.id == 120 || this.id == 211 || this.id == 677 || this.id == 215 || this.id == 217 || this.id == 259 || this.id == 347 || this.getFactory() != null;
   }

   public boolean isSpecialBlock() {
      return this.specialBlock;
   }

   public void setSpecialBlock(boolean var1) {
      this.specialBlock = var1;
   }

   public boolean isDrawnOnlyInBuildMode() {
      assert !this.drawOnlyInBuildMode || this.blended;

      return this.drawOnlyInBuildMode;
   }

   public void setDrawOnlyInBuildMode(boolean var1) {
      this.drawOnlyInBuildMode = var1;
   }

   public boolean isInOctree() {
      return this.id != 937 && this.id != 939 && this.id != 938;
   }

   public boolean isLightPassOnBlockItself() {
      return EngineSettings.CUBE_LIGHT_NORMALIZER_NEW_M.isOn() && (this.blockStyle.solidBlockStyle || this.slab > 0);
   }

   public void onInit() {
      this.calculateDynamicPrice();
   }

   public boolean hasLod() {
      return this.lodShapeString.length() > 0;
   }

   public int getModelCount(boolean var1) {
      Mesh var2;
      if (var1 && this.lodShapeStringActive != null && this.lodShapeStringActive.length() > 0) {
         var2 = Controller.getResLoader().getMesh(this.lodShapeStringActive);
      } else {
         var2 = Controller.getResLoader().getMesh(this.lodShapeString);
      }

      return var2.getChilds().size();
   }

   public Mesh getModel(int var1, boolean var2) {
      Mesh var4;
      if (var2 && this.lodShapeStringActive != null && this.lodShapeStringActive.length() > 0) {
         var4 = Controller.getResLoader().getMesh(this.lodShapeStringActive);
      } else {
         var4 = Controller.getResLoader().getMesh(this.lodShapeString);
      }

      if (var4 == null) {
         throw new RuntimeException("Model " + this.lodShapeString + " not found");
      } else {
         Mesh var3 = (Mesh)var4.getChilds().get(var1);

         assert var3 != null : this.lodShapeString;

         return var3;
      }
   }

   private void recalcRawConsistenceRec(FactoryResource var1, int var2) {
      ElementInformation var3;
      if ((var3 = ElementKeyMap.getInfoFast(var1.type)).getConsistence().isEmpty()) {
         this.rawConsistence.add(var1);
         this.rawBlocks.inc(var1.type, var2);
      } else {
         Iterator var4 = var3.consistence.iterator();

         while(var4.hasNext()) {
            FactoryResource var5 = (FactoryResource)var4.next();
            this.recalcRawConsistenceRec(var5, var5.count * var2);
         }

      }
   }

   private void recalcTotalConsistenceRec(FactoryResource var1) {
      ElementInformation var2 = ElementKeyMap.getInfoFast(var1.type);
      this.getTotalConsistence().add(var1);
      Iterator var3 = var2.consistence.iterator();

      while(var3.hasNext()) {
         FactoryResource var4 = (FactoryResource)var3.next();
         this.recalcTotalConsistenceRec(var4);
      }

   }

   public void recalcTotalConsistence() {
      this.rawBlocks.checkArraySize();
      this.rawConsistence.clear();
      this.getTotalConsistence().clear();
      List var1 = this.consistence;
      if (this.getSourceReference() != 0 && ElementKeyMap.isValidType(this.getSourceReference())) {
         var1 = ElementKeyMap.getInfo(this.getSourceReference()).getConsistence();
      }

      Iterator var2 = var1.iterator();

      FactoryResource var3;
      while(var2.hasNext()) {
         var3 = (FactoryResource)var2.next();
         this.recalcRawConsistenceRec(var3, var3.count);
      }

      var2 = var1.iterator();

      while(var2.hasNext()) {
         var3 = (FactoryResource)var2.next();
         this.recalcTotalConsistenceRec(var3);
      }

   }

   public ElementCountMap getRawBlocks() {
      return this.rawBlocks;
   }

   public List getRawConsistence() {
      return this.rawConsistence;
   }

   public List getTotalConsistence() {
      return this.totalConsistence;
   }

   public boolean isExtendedTexture() {
      return this.extendedTexture;
   }

   public void setBuildIconToFree() {
      short var1 = 0;
      ShortOpenHashSet var2 = new ShortOpenHashSet();
      short[] var3;
      int var4 = (var3 = ElementKeyMap.typeList()).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         Short var6 = var3[var5];
         var1 = (short)Math.max(var1, ElementKeyMap.getInfo(var6).getBuildIconNum());
         var2.add((short)ElementKeyMap.getInfo(var6).getBuildIconNum());
      }

      Iterator var7 = AddElementEntryDialog.addedBuildIcons.iterator();

      while(var7.hasNext()) {
         Short var9 = (Short)var7.next();
         var1 = (short)Math.max(var1, var9);
         var2.add(var9);
      }

      for(short var8 = 0; var8 < var1 + 2; ++var8) {
         if (!var2.contains(var8)) {
            this.setBuildIconNum(var8);
            return;
         }
      }

   }

   public boolean isReactorChamberAny() {
      return this.isReactorChamberGeneral() || this.isReactorChamberSpecific();
   }

   public boolean isReactorChamberGeneral() {
      return this.chamberGeneral;
   }

   public boolean isReactorChamberSpecific() {
      return this.chamberRoot != 0;
   }

   public short getComputer() {
      return (short)this.computerType;
   }

   public boolean needsComputer() {
      return ElementKeyMap.isValidType(this.computerType);
   }

   public ShortSet getChamberChildrenOnLevel(ShortSet var1) {
      var1.addAll(this.chamberChildren);
      ElementInformation var2;
      if (this.chamberParent != 0 && (var2 = ElementKeyMap.getInfo(this.chamberParent)).chamberUpgradesTo == this.id) {
         var2.getChamberChildrenOnLevel(var1);
         var1.remove((short)var2.chamberUpgradesTo);
      }

      return var1;
   }

   public short getChamberUpgradedRoot() {
      if (this.chamberParent != 0) {
         ElementInformation var1;
         return (var1 = ElementKeyMap.getInfo(this.chamberParent)).chamberUpgradesTo == this.id ? var1.getChamberUpgradedRoot() : this.id;
      } else {
         return this.id;
      }
   }

   public boolean isChamberChildrenUpgradableContains(short var1) {
      if (this.chamberChildren.contains(var1)) {
         return true;
      } else {
         ElementInformation var2;
         return this.chamberParent != 0 && (var2 = ElementKeyMap.getInfo(this.chamberParent)).chamberUpgradesTo == this.id && var2.isChamberChildrenUpgradableContains(var1);
      }
   }

   public boolean isChamberUpgraded() {
      return ElementKeyMap.isValidType(this.chamberParent) && ElementKeyMap.getInfo(this.chamberParent).chamberUpgradesTo == this.id;
   }

   public void sanatizeReactorValues() {
      if (this.chamberParent != 0 && (!ElementKeyMap.isValidType(this.chamberParent) || !ElementKeyMap.isChamber((short)this.chamberParent))) {
         System.err.println("SANATIZED REACTOR chamberParent " + this.getName() + " -> " + ElementKeyMap.toString(this.chamberParent));
         this.chamberParent = 0;
      }

      if (this.chamberRoot != 0 && (!ElementKeyMap.isValidType(this.chamberRoot) || !ElementKeyMap.isChamber((short)this.chamberRoot))) {
         System.err.println("SANATIZED REACTOR chamberRoot " + this.getName() + " -> " + ElementKeyMap.toString(this.chamberRoot));
         this.chamberRoot = 0;
      }

      if (this.chamberUpgradesTo != 0 && (!ElementKeyMap.isValidType(this.chamberUpgradesTo) || !ElementKeyMap.isChamber((short)this.chamberUpgradesTo))) {
         System.err.println("SANATIZED REACTOR chamberUpgradesTo " + this.getName() + " -> " + ElementKeyMap.toString(this.chamberUpgradesTo));
         this.chamberUpgradesTo = 0;
      }

      ShortIterator var1 = this.chamberPrerequisites.iterator();

      while(true) {
         short var2;
         do {
            do {
               if (!var1.hasNext()) {
                  var1 = this.chamberChildren.iterator();

                  while(true) {
                     do {
                        do {
                           if (!var1.hasNext()) {
                              if (!this.isReactorChamberSpecific() && this.getId() != 1008 && this.getId() != 1010 && this.getId() != 1009) {
                                 this.reactorHp = 0;
                              } else if (this.reactorHp == 0) {
                                 this.reactorHp = 10;
                                 return;
                              }

                              return;
                           }
                        } while((var2 = var1.nextShort()) == 0);
                     } while(ElementKeyMap.isValidType(var2) && ElementKeyMap.isChamber(var2));

                     System.err.println("SANATIZED REACTOR chamberChildren " + this.getName() + " -> " + ElementKeyMap.toString(var2));
                     var1.remove();
                  }
               }
            } while((var2 = var1.nextShort()) == 0);
         } while(ElementKeyMap.isValidType(var2) && ElementKeyMap.isChamber(var2));

         System.err.println("SANATIZED REACTOR chamberPrereq " + this.getName() + " -> " + ElementKeyMap.toString(var2));
         var1.remove();
      }
   }

   public String getChamberEffectInfo(ConfigPool var1) {
      if (this.chamberConfigGroupsLowerCase.isEmpty()) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_ELEMENTINFORMATION_10;
      } else {
         StringBuffer var2 = new StringBuffer();
         Iterator var3 = this.chamberConfigGroupsLowerCase.iterator();

         while(var3.hasNext()) {
            String var4 = (String)var3.next();
            ConfigGroup var5;
            if ((var5 = (ConfigGroup)var1.poolMapLowerCase.get(var4)) != null) {
               var2.append(var5.getEffectDescription());
            }
         }

         return var2.toString().trim();
      }
   }

   private float getChamberCapacityBranchRec(float var1) {
      while(ElementKeyMap.isValidType(this.chamberParent)) {
         ElementInformation var2;
         ElementInformation var10000 = var2 = ElementKeyMap.getInfoFast(this.chamberParent);
         var1 += var2.chamberCapacity;
         this = var10000;
      }

      return var1;
   }

   public float getChamberCapacityBranch() {
      return this.getChamberCapacityBranchRec(this.chamberCapacity);
   }

   public int getSourceReference() {
      return this.chamberRoot != 0 ? this.chamberRoot : this.sourceReference;
   }

   public void setSourceReference(int var1) {
      this.sourceReference = var1;
   }

   public float getChamberCapacityWithUpgrades() {
      float var1 = this.chamberCapacity;
      if (this.isChamberUpgraded() && ElementKeyMap.isValidType(this.chamberParent)) {
         var1 += ElementKeyMap.getInfo(this.chamberParent).getChamberCapacityWithUpgrades();
      }

      return var1;
   }

   public boolean isChamberPermitted(SimpleTransformableSendableObject.EntityType var1) {
      if (this.chamberPermission == 0) {
         return true;
      } else {
         switch(var1) {
         case PLANET_CORE:
         case PLANET_ICO:
         case PLANET_SEGMENT:
            if ((this.chamberPermission & 4) == 4) {
               return true;
            }

            return false;
         case SHIP:
            if ((this.chamberPermission & 1) == 1) {
               return true;
            }

            return false;
         case SHOP:
         case SPACE_STATION:
            if ((this.chamberPermission & 2) == 2) {
               return true;
            }

            return false;
         default:
            return false;
         }
      }
   }

   public String getDescriptionIncludingChamberUpgraded() {
      return this.isChamberUpgraded() ? ElementKeyMap.getInfo(this.chamberParent).getDescription() : this.getDescription();
   }

   public boolean isThisOrParentChamberMutuallyExclusive(short var1) {
      if (this.chamberMutuallyExclusive.contains(var1)) {
         return true;
      } else {
         return ElementKeyMap.isValidType(this.chamberParent) ? ElementKeyMap.getInfoFast(this.chamberParent).isThisOrParentChamberMutuallyExclusive(var1) : false;
      }
   }

   public boolean isArmor() {
      return this.armorValue > 0.0F;
   }

   public float getMaxHitPointsOneDivByByte() {
      return 0.007874016F;
   }

   public byte getMaxHitPointsByte() {
      return 127;
   }

   public void setMaxHitPointsE(int var1) {
      assert var1 > 0;

      this.maxHitPointsFull = var1;
      this.maxHitpointsInverse = 1.0D / (double)var1;
      this.maxHitpointsFullToByte = 127.0D / (double)var1;
      this.maxHitpointsByteToFull = (double)var1 / 127.0D;
   }

   public short convertToByteHp(int var1) {
      return (short)((int)Math.max(0L, Math.min(127L, Math.round((double)var1 * this.maxHitpointsFullToByte))));
   }

   public int convertToFullHp(short var1) {
      return (int)((double)var1 * this.maxHitpointsByteToFull);
   }

   public void setHpOldByte(short var1) {
      this.oldHitpoints = var1;
   }

   public short getHpOldByte() {
      return this.oldHitpoints;
   }

   public float getArmorValue() {
      return this.armorValue;
   }

   public void setArmorValue(float var1) {
      this.armorValue = var1;
   }

   public boolean isMineAddOn() {
      return this.id == 365 || this.id == 364 || this.id == 366 || this.id == 363;
   }

   public boolean isMineType() {
      return this.id == 355 || this.id == 356 || this.id == 358;
   }

   public boolean isDecorative() {
      return !this.isArmor() && !this.systemBlock;
   }

   public static boolean canBeControlledOld(short var0, short var1) {
      if (var0 != 0 && var1 != 0) {
         ElementInformation var2 = ElementKeyMap.getInfo(var0);
         ElementInformation var3 = ElementKeyMap.getInfo(var1);
         return var2.getId() == 1 && var3.isEffectCombinationController();
      } else {
         return false;
      }
   }

   public boolean isBeacon() {
      return this.beacon;
   }

   public static class LodCollision {
      public String modelId;
      public MeshGroup meshGroup;
      public ConvexHullShapeExt[] hulls;
      public CollisionShape shape;
      public boolean valid = true;
      public ElementInformation.LodCollision.LodCollisionType type;
      public BlockStyle blockTypeToEmulate;
      public int colslab;
      private Vector3f localPosTmp;
      private Transform tmpTrans0;
      private Transform tmpTrans1;
      private Matrix3f rotTmp;

      public LodCollision() {
         this.type = ElementInformation.LodCollision.LodCollisionType.BLOCK_TYPE;
         this.blockTypeToEmulate = BlockStyle.NORMAL;
         this.localPosTmp = new Vector3f();
         this.tmpTrans0 = new Transform();
         this.tmpTrans1 = new Transform();
         this.rotTmp = new Matrix3f();
      }

      public void load() {
         if (this.type != ElementInformation.LodCollision.LodCollisionType.NONE) {
            if (this.type != ElementInformation.LodCollision.LodCollisionType.BLOCK_TYPE) {
               this.meshGroup = (MeshGroup)Controller.getResLoader().getMesh(this.modelId);
               if (this.meshGroup == null) {
                  throw new RuntimeException("Collision Mesh Not found: '" + this.modelId + "'");
               } else {
                  int var1 = 0;
                  Iterator var2 = this.meshGroup.getChilds().iterator();

                  while(var2.hasNext()) {
                     if ((AbstractSceneNode)var2.next() instanceof Mesh) {
                        ++var1;
                     }
                  }

                  this.hulls = new ConvexHullShapeExt[var1];
                  CompoundShape var7 = new CompoundShape();
                  int var3 = 0;
                  Iterator var6 = this.meshGroup.getChilds().iterator();

                  while(var6.hasNext()) {
                     AbstractSceneNode var4;
                     if ((var4 = (AbstractSceneNode)var6.next()) instanceof Mesh) {
                        Mesh var8 = (Mesh)var4;
                        ConvexHullShapeExt var5 = new ConvexHullShapeExt(var8.getVerticesListInstance());
                        this.hulls[var3] = var5;
                        Transform var9 = var8.getInitialTransformWithoutScale(new Transform());
                        var7.addChildShape(var9, var5);
                        ++var3;
                     }
                  }

                  this.shape = var7;
               }
            }
         }
      }

      public boolean isValid() {
         return this.valid;
      }

      public String toString() {
         return this.type == ElementInformation.LodCollision.LodCollisionType.CONVEX_HULL ? "LOD_COLLSISION(Type: " + this.type.type + "; Model: '" + this.modelId + "')" : "LOD_COLLSISION(Type: " + this.type.type + "; Model: '" + this.blockTypeToEmulate + "')";
      }

      public void parse(Node var1) {
         NodeList var2 = var1.getChildNodes();
         Node var3;
         if ((var3 = var1.getAttributes().getNamedItem("type")) != null) {
            String var5 = var3.getNodeValue();

            int var4;
            for(var4 = 0; var4 < ElementInformation.LodCollision.LodCollisionType.values().length; ++var4) {
               if (ElementInformation.LodCollision.LodCollisionType.values()[var4].type.toLowerCase(Locale.ENGLISH).equals(var5.toLowerCase(Locale.ENGLISH))) {
                  this.type = ElementInformation.LodCollision.LodCollisionType.values()[var4];
                  break;
               }
            }

            if (this.type == null) {
               throw new RuntimeException("NO TYPE: " + var5);
            } else {
               label36:
               switch(this.type) {
               case NONE:
                  break;
               case BLOCK_TYPE:
                  this.colslab = Integer.parseInt(var1.getAttributes().getNamedItem("slab").getNodeValue());
                  this.blockTypeToEmulate = BlockStyle.parse((org.w3c.dom.Element)var1);
                  break;
               case CONVEX_HULL:
                  var4 = 0;

                  while(true) {
                     if (var4 >= var2.getLength()) {
                        break label36;
                     }

                     if ((var1 = var2.item(var4)).getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("mesh")) {
                        this.modelId = var1.getTextContent();
                     }

                     ++var4;
                  }
               default:
                  throw new RuntimeException("UNKNOWN TYPE: " + this.type.name());
               }

               this.valid = true;
            }
         }
      }

      public void write(org.w3c.dom.Element var1, Document var2) {
         Attr var3;
         (var3 = var2.createAttribute("type")).setValue(this.type.type);
         var1.getAttributes().setNamedItem(var3);
         switch(this.type) {
         case NONE:
            return;
         case BLOCK_TYPE:
            this.blockTypeToEmulate.write(var1, var2);
            Attr var5;
            (var5 = var2.createAttribute("slab")).setNodeValue(String.valueOf(this.colslab));
            var1.getAttributes().setNamedItem(var5);
            return;
         case CONVEX_HULL:
            org.w3c.dom.Element var4;
            (var4 = var2.createElement("Mesh")).setTextContent(this.modelId);
            var1.appendChild(var4);
            return;
         default:
            throw new RuntimeException("UNKNOWN TYPE: " + this.type.name());
         }
      }

      public CollisionShape getShape(short var1, byte var2, Transform var3) {
         switch(this.type) {
         case NONE:
            return null;
         case BLOCK_TYPE:
            return BlockShapeAlgorithm.getShape(this.blockTypeToEmulate, var2);
         case CONVEX_HULL:
            ElementInformation var4 = ElementKeyMap.getInfoFast(var1);
            Oriencube var5 = (Oriencube)BlockShapeAlgorithm.algorithms[5][var4.blockStyle == BlockStyle.SPRITE ? var2 % 6 << 2 : var2];
            var3.set(var5.getBasicTransform());
            if (var4.getBlockStyle() == BlockStyle.SPRITE) {
               this.rotTmp.setIdentity();
               this.rotTmp.rotX((float)SingleBlockDrawer.timesR * 1.5707964F);
               var3.basis.mul(this.rotTmp);
            }

            var3.origin.set(0.0F, 0.0F, 0.0F);
            return this.shape;
         default:
            throw new RuntimeException("UNKNOWN TYPE: " + this.type.name());
         }
      }

      public static enum LodCollisionType {
         BLOCK_TYPE("BlockType"),
         CONVEX_HULL("ConvexHull"),
         NONE("None");

         public final String type;

         private LodCollisionType(String var3) {
            this.type = var3;
         }

         public final String toString() {
            return this.type;
         }
      }
   }

   public static enum ResourceInjectionType {
      OFF(0),
      ORE(1),
      FLORA(17);

      public final int index;

      private ResourceInjectionType(int var3) {
         this.index = var3;
      }
   }

   public static enum EIC {
      BASICS("Basics"),
      MODELS("Models"),
      TEXTURES("Textures"),
      FEATURES("Features"),
      HP_ARMOR("Hp & Armor"),
      CRAFTING_ECONOMY("Crafting & Economy"),
      POWER_REACTOR("Power & Reactor"),
      DEPRECATED("Deprecated");

      public boolean collapsed = true;
      private final String name;

      private EIC(String var3) {
         this.name = var3;
      }

      public final String getName() {
         return this.name;
      }
   }
}
