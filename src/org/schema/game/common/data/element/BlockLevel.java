package org.schema.game.common.data.element;

public class BlockLevel {
   private final short id;
   private final int level;

   public BlockLevel(short var1, int var2) {
      this.id = var1;
      this.level = var2;
   }

   public short getIdBase() {
      return this.id;
   }

   public int getLevel() {
      return this.level;
   }

   public String toString() {
      return "Level: " + this.level + "; Base: " + ElementKeyMap.getInfo(this.id);
   }
}
