package org.schema.game.common.data.element;

import javax.vecmath.Vector4f;

public interface ColorBeamInterface {
   Vector4f getColor();

   Vector4f getDefaultColor();

   boolean hasCustomColor();
}
