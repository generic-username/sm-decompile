package org.schema.game.common.data.element;

import java.util.Iterator;
import java.util.Observable;
import javax.swing.JDialog;
import javax.swing.JFrame;
import org.schema.game.common.facedit.ElementReactorChangePanel;

public class ElementReactorChange extends Observable {
   public final ElementInformation info;
   public ElementInformation parent;
   public ElementInformation root;
   public boolean upgrade;

   public ElementReactorChange(ElementInformation var1) {
      this.info = var1;
      this.parent = ElementKeyMap.isValidType(this.info.chamberParent) ? ElementKeyMap.getInfo(this.info.chamberParent) : null;
      this.root = ElementKeyMap.isValidType(this.info.chamberRoot) ? ElementKeyMap.getInfo(this.info.chamberRoot) : null;
      this.upgrade = this.parent != null && this.parent.chamberUpgradesTo == this.info.id;
   }

   public void openDialog(JFrame var1) {
      JDialog var3;
      (var3 = new JDialog(var1, "Reactor", true)).setSize(500, 300);
      ElementReactorChangePanel var2 = new ElementReactorChangePanel(var3, this);
      var3.setContentPane(var2);
      var3.setVisible(true);
   }

   public void setParent(ElementInformation var1) {
      this.parent = var1;
      this.setChanged();
      this.notifyObservers();
   }

   public void setUpgrade(boolean var1) {
      this.upgrade = var1;
      this.setChanged();
      this.notifyObservers();
   }

   public void setRoot(ElementInformation var1) {
      this.root = var1;
      this.setChanged();
      this.notifyObservers();
   }

   private void reinitGeneral(ElementInformation var1) {
      Iterator var2 = ElementKeyMap.keySet.iterator();

      while(var2.hasNext()) {
         short var3;
         ElementInformation var4;
         if ((var4 = ElementKeyMap.getInfo(var3 = (Short)var2.next())).chamberParent == 0 && var4.chamberRoot == var1.id) {
            var1.chamberChildren.add(var3);
         }
      }

   }

   public void clear() {
      Iterator var1 = ElementKeyMap.keySet.iterator();

      while(var1.hasNext()) {
         short var2;
         ElementInformation var3;
         if ((var2 = (Short)var1.next()) != this.info.id && !(var3 = ElementKeyMap.getInfo(var2)).isReactorChamberGeneral()) {
            var3.chamberChildren.remove(this.info.id);
            var3.chamberPrerequisites.remove(this.info.id);
            if (var3.chamberUpgradesTo == this.info.id) {
               var3.chamberUpgradesTo = 0;
            }
         }
      }

      this.info.chamberChildren.clear();
      this.info.chamberPrerequisites.clear();
      this.info.chamberUpgradesTo = 0;
      this.info.chamberRoot = 0;
      this.info.chamberParent = 0;
   }

   public void apply() {
      if (this.root == null) {
         System.err.println("NOT APPLIED. MISSING FIELDS");
      } else {
         this.clear();
         this.info.chamberRoot = this.root.id;
         this.info.chamberParent = this.parent != null ? this.parent.id : 0;
         if (this.parent != null) {
            this.parent.chamberChildren.add(this.info.id);
            this.info.chamberPrerequisites.add(this.parent.id);
            if (this.upgrade) {
               this.parent.chamberUpgradesTo = this.info.id;
            }
         }

         Iterator var1 = ElementKeyMap.keySet.iterator();

         while(var1.hasNext()) {
            ElementInformation var2;
            if ((var2 = ElementKeyMap.getInfo((Short)var1.next())).isReactorChamberGeneral()) {
               var2.chamberChildren.clear();
               var2.chamberPrerequisites.clear();
               var2.chamberUpgradesTo = 0;
               var2.chamberParent = 0;
               this.reinitGeneral(var2);
            }
         }

      }
   }
}
