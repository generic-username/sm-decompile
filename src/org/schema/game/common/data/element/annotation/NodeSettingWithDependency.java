package org.schema.game.common.data.element.annotation;

public interface NodeSettingWithDependency extends NodeDependency, NodeSetting {
}
