package org.schema.game.common.data.element;

public class ElementNotFoundException extends Exception {
   private static final long serialVersionUID = 1L;
}
