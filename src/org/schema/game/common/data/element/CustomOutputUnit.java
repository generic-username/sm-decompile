package org.schema.game.common.data.element;

import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import java.util.Iterator;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.FireingUnit;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.graphicsengine.core.Timer;

public abstract class CustomOutputUnit extends FireingUnit {
   private Vector3i output = new Vector3i(0, 0, 0);
   private final ShootContainer shootContainer = new ShootContainer();
   private Vector3i controlledFrom = new Vector3i();
   private Vector3i centeralizedControlledFromPos = new Vector3i();

   public Vector3i getOutput() {
      return this.output;
   }

   public void calculateExtraDataAfterCreationThreaded(long var1, LongOpenHashSet var3) {
      super.calculateExtraDataAfterCreationThreaded(var1, var3);
      SegmentPiece var7 = new SegmentPiece();
      boolean var2 = false;
      Iterator var8 = this.getNeighboringCollection().iterator();

      while(var8.hasNext()) {
         long var5 = (Long)var8.next();
         SegmentPiece var4;
         if ((var4 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var5, var7)) != null && var4.isActive()) {
            ElementCollection.getPosFromIndex(var5, this.output);
            var2 = true;
            break;
         }
      }

      if (!var2) {
         this.getSignificator(this.output);
      }

   }

   public void setMainPiece(SegmentPiece var1, boolean var2) {
      if (var2) {
         if (var1.getSegment().getSegmentController().isOnServer()) {
            SegmentPiece var3 = new SegmentPiece();
            Vector3i var4 = new Vector3i();
            Iterator var5 = this.getNeighboringCollection().iterator();

            while(var5.hasNext()) {
               getPosFromIndex((Long)var5.next(), var4);
               if (!var1.equalsPos(var4)) {
                  var1.getSegment().getSegmentController().getSegmentBuffer().getPointUnsave(var4, var3);
                  if (var3.isActive()) {
                     (new Vector4i()).set(var4.x, var4.y, var4.z, -2);
                     ((SendableSegmentController)this.getSegmentController()).getBlockActivationBuffer().enqueue(ElementCollection.getDeactivation(var4.x, var4.y, var4.z, false, false));
                  }
               }
            }
         }

         System.err.println(this.getSegmentController() + "; " + this.getSegmentController().getState() + " NEW OUTPUT SET: " + var1.getAbsolutePos(new Vector3i()) + "; ACTIVE: " + var2);
         this.output.set(var1.getAbsolutePos(new Vector3i()));
      }

   }

   public void fire(ControllerStateInterface var1, Timer var2) {
      super.fire(var1, var2);
      Vector3i var3 = this.getOutput();
      this.shootContainer.weapontOutputWorldPos.set((float)(var3.x - 16), (float)(var3.y - 16), (float)(var3.z - 16));
      if (this.getSegmentController().isOnServer()) {
         this.getSegmentController().getWorldTransform().transform(this.shootContainer.weapontOutputWorldPos);
      } else {
         this.getSegmentController().getWorldTransformOnClient().transform(this.shootContainer.weapontOutputWorldPos);
      }

      if (var1.getParameter(new Vector3i()).equals(Ship.core)) {
         var1.getControlledFrom(this.shootContainer.controlledFromOrig);
      } else {
         var1.getParameter(this.shootContainer.controlledFromOrig);
      }

      this.centeralizedControlledFromPos.set(this.shootContainer.controlledFromOrig);
      this.shootContainer.camPos.set(this.getSegmentController().getAbsoluteElementWorldPositionShifted(this.centeralizedControlledFromPos, this.shootContainer.tmpCampPos));
      var1.addCockpitOffset(this.shootContainer.camPos);
      this.doShot(var1, var2, this.shootContainer);
   }

   public abstract void doShot(ControllerStateInterface var1, Timer var2, ShootContainer var3);
}
