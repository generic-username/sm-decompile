package org.schema.game.common.data.element;

import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.ModuleExplosion;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.config.ConfigEntityManager;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.Universe;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.BoundingBox;

public abstract class ElementCollection {
   private static final ObjectArrayList volatileCollectionPool = new ObjectArrayList();
   public static final long l = 281474976710655L;
   public static final long SHORT_MAX2 = 65534L;
   public static final long SHORT_MAX2x2 = 4294705156L;
   public static final short INACTIVE = 1;
   public static final short INACTIVE_NO_DELEGATE = 2;
   public static final short ACTIVE = 3;
   public static final short ACTIVE_NO_DELEGATE = 4;
   public static final short FROM_DELIGATE_ECODE = 10;
   public static final short SENT_FROM_WIRELESS = 100;
   static final long yDelim = 65536L;
   static final long zDelim = 4294967296L;
   private static final long LONG_MIN = getIndex(-32767, -32767, -32767);
   private LongArrayList neighboringCollection;
   public boolean valid = true;
   public boolean prevalid = true;
   public int indexDebug = 0;
   public long idPos;
   public final int[] touching;
   public ElementCollectionManager elementCollectionManager;
   protected long significator;
   private int minX;
   private int minY;
   private int minZ;
   private int maxX;
   private int maxY;
   private int maxZ;
   private SegmentController controller;
   private short clazzId;
   private SegmentPiece id;
   private boolean idChanged;
   private long randomlySelectedFromLastThreadUpdate;
   private int size;
   private ElementCollectionMesh mesh;
   private double integrity;
   private long[] nonDetailedCollectionRandoms;
   public static final List meshPool = new ObjectArrayList();
   private static final int MAX_MESH_POOL = 512;
   public static int takenCollections = 0;

   public ElementCollection() {
      this.idPos = LONG_MIN;
      this.touching = new int[7];
      this.significator = LONG_MIN;
      this.minX = Integer.MAX_VALUE;
      this.minY = Integer.MAX_VALUE;
      this.minZ = Integer.MAX_VALUE;
      this.maxX = Integer.MIN_VALUE;
      this.maxY = Integer.MIN_VALUE;
      this.maxZ = Integer.MIN_VALUE;
      this.id = null;
      this.idChanged = true;
   }

   public String getName() {
      return this.getClass().getSimpleName() + "[" + this.idPos + "]";
   }

   public static long getIndex(byte var0, byte var1, byte var2, Segment var3) {
      return getIndex(var3.pos.x + var0, var3.pos.y + var1, var3.pos.z + var2);
   }

   public PowerInterface getPowerInterface() {
      return ((ManagedSegmentController)this.getSegmentController()).getManagerContainer().getPowerInterface();
   }

   public boolean isIntegrityUsed() {
      return true;
   }

   public static final long getIndex4(long var0, short var2) {
      return (long)(var2 & '\uffff') << 48 | var0 & 281474976710655L;
   }

   public ConfigEntityManager getConfigManager() {
      return this.getSegmentController().getConfigManager();
   }

   public static final long getIndex4(Vector3i var0, short var1) {
      return getIndex4((short)var0.x, (short)var0.y, (short)var0.z, var1);
   }

   public static final long getIndex4(short var0, short var1, short var2, short var3) {
      return ((long)(var3 & '\uffff') << 48) + ((long)(var2 & '\uffff') << 32) + ((long)(var1 & '\uffff') << 16) + (long)(var0 & '\uffff');
   }

   public boolean isUsingPowerReactors() {
      return this.getSegmentController().isUsingPowerReactors();
   }

   public static final long getIndex(int var0, int var1, int var2) {
      return ((long)(var2 & '\uffff') << 32) + ((long)(var1 & '\uffff') << 16) + (long)(var0 & '\uffff');
   }

   public static final long getIndex(Vector3i var0) {
      return getIndex(var0.x, var0.y, var0.z);
   }

   public static long getPosIndexFrom4(long var0) {
      return var0 & 281474976710655L;
   }

   public static long getSide(long var0, int var2) {
      return getIndex(Math.min(32767, Math.max(-32768, getPosX(var0) + Element.DIRECTIONSi[var2].x)), Math.min(32767, Math.max(-32768, getPosY(var0) + Element.DIRECTIONSi[var2].y)), Math.min(32767, Math.max(-32768, getPosZ(var0) + Element.DIRECTIONSi[var2].z)));
   }

   public void fire(ControllerStateInterface var1, Timer var2) {
   }

   public static long getSide(int var0, int var1, int var2, int var3) {
      return getIndex(Math.min(32767, Math.max(-32768, var0 + Element.DIRECTIONSi[var3].x)), Math.min(32767, Math.max(-32768, var1 + Element.DIRECTIONSi[var3].y)), Math.min(32767, Math.max(-32768, var2 + Element.DIRECTIONSi[var3].z)));
   }

   public static Vector3i getPosFromIndex(long var0, Vector3i var2) {
      var2.set(getPosX(var0), getPosY(var0), getPosZ(var0));

      assert getIndex(var2) == getPosIndexFrom4(var0) : getPosIndexFrom4(var0) + " != " + getIndex(var2) + "; " + var2;

      return var2;
   }

   public static Vector3f getPosFromIndex(long var0, Vector3f var2) {
      var2.set((float)getPosX(var0), (float)getPosY(var0), (float)getPosZ(var0));
      return var2;
   }

   public static int getPosX(long var0) {
      return (short)((int)(var0 & 65535L));
   }

   public static String getPosString(long var0) {
      return getPosX(var0) + ", " + getPosY(var0) + ", " + getPosZ(var0);
   }

   public static int getPosY(long var0) {
      return (short)((int)(var0 >> 16 & 65535L));
   }

   public static int getPosZ(long var0) {
      return (short)((int)(var0 >> 32 & 65535L));
   }

   public static int getType(long var0) {
      return (short)((int)(var0 >> 48 & 65535L));
   }

   public static void main(String[] var0) {
      System.err.println(65520L);
      System.err.println(65521L);
      System.err.println(65522L);
      System.err.println("LL 65536");
      System.err.println("MAX 2 65536");
      System.err.println(4294049792L);
      System.err.println(4294115328L);
      System.err.println(0L);
      System.err.println(getIndex(0, 0, 0));
      System.err.println(getIndex(-32768, -32768, -32768));
      System.err.println(getIndex(32767, 32767, 32767));

      for(int var23 = -1600; var23 < 1600; var23 += 16) {
         for(int var1 = -1600; var1 < 1600; var1 += 16) {
            for(int var2 = -1600; var2 < 1600; var2 += 16) {
               int var3 = var2;
               int var4 = var1;
               int var5 = var23;
               long var7 = (long)(var2 & '\uffff');
               long var9 = (long)(var1 & '\uffff') << 16;
               long var11 = (long)(var23 & '\uffff') << 32;
               long var13 = var7;
               long var15 = var9;
               long var17 = var11;

               for(byte var6 = 0; var6 < 32; ++var6) {
                  for(byte var24 = 0; var24 < 32; ++var24) {
                     for(byte var12 = 0; var12 < 32; ++var12) {
                        long var19 = var13 + var15 + var17;
                        long var21 = getIndex(var3 + var12, var4 + var24, var5 + var6);

                        assert var21 == var19 : "\n" + var3 + "; " + var4 + ", " + var5 + ";\n" + var12 + "; " + var24 + ", " + var6 + ";\n" + var13 + ", " + var15 + " " + var17 + ";\n" + (long)(var3 + var12 & '\uffff') + ", " + ((long)(var4 + var24 & '\uffff') << 16) + " " + ((long)(var5 + var6 & '\uffff') << 32) + ";\n" + var21 + "; " + var19;

                        ++var13;
                     }

                     var13 = var7;
                     var15 += 65536L;
                  }

                  var13 = var7;
                  var15 = var9;
                  var17 += 4294967296L;
               }
            }
         }
      }

   }

   public static void writeIndexAsShortPos(long var0, DataOutput var2) throws IOException {
      var2.writeShort((short)getPosX(var0));
      var2.writeShort((short)getPosY(var0));
      var2.writeShort((short)getPosZ(var0));
   }

   public float getExtraConsume() {
      return 1.0F;
   }

   public int getEffectBonus() {
      return 1;
   }

   public static long getEncodeActivation(SegmentPiece var0, boolean var1, boolean var2, boolean var3) {
      return getIndex4(var0.getAbsoluteIndex(), (short)((var2 ? (var1 ? 3 : 4) : (var1 ? 1 : 2)) + (var3 ? 10 : 0)));
   }

   public static long getDeactivation(int var0, int var1, int var2, boolean var3, boolean var4) {
      return getIndex4((short)var0, (short)var1, (short)var2, (short)((var3 ? 1 : 2) + (var4 ? 10 : 0)));
   }

   public static long getActivation(int var0, int var1, int var2, boolean var3, boolean var4) {
      return getIndex4((short)var0, (short)var1, (short)var2, (short)((var3 ? 3 : 4) + (var4 ? 10 : 0)));
   }

   public static long getDeactivation(long var0, boolean var2, boolean var3) {
      return getIndex4(var0, (short)((var2 ? 1 : 2) + (var3 ? 10 : 0)));
   }

   public static long getActivation(long var0, boolean var2, boolean var3) {
      return getIndex4(var0, (short)((var2 ? 3 : 4) + (var3 ? 10 : 0)));
   }

   public static long getDeactivationWireless(long var0, boolean var2, boolean var3) {
      return getIndex4(var0, (short)((var2 ? 1 : 2) + (var3 ? 10 : 0) + 100));
   }

   public static long getActivationWireless(long var0, boolean var2, boolean var3) {
      return getIndex4(var0, (short)((var2 ? 3 : 4) + (var3 ? 10 : 0) + 100));
   }

   public static boolean isActiveFromActivationIndex(long var0) {
      long var2;
      return (var2 = (long)getType(var0)) == 3L || var2 == 4L;
   }

   public static boolean isActiveFromType(long var0) {
      return var0 == 3L || var0 == 4L;
   }

   public static boolean isDeligateFromActivationIndex(long var0) {
      long var2;
      return (var2 = (long)getType(var0)) == 1L || var2 == 3L;
   }

   public boolean isValid() {
      return this.valid;
   }

   public void calculateExtraDataAfterCreationThreaded(long var1, LongOpenHashSet var3) {
   }

   public void onClear() {
      if (this.mesh != null) {
         ElementCollectionMesh var1 = this.mesh;
         this.mesh = null;
         freeMesh(var1);
      }

   }

   public boolean hasMesh() {
      return true;
   }

   public void calculateMesh(long var1, boolean var3) {
      assert !this.getSegmentController().isOnServer();

      if (this.elementCollectionManager.cancelUpdateStatus != var1) {
         ElementCollectionMesh var4;
         (var4 = getMeshInstance()).calculate(this, var1, this.getNeighboringCollectionUnsave());
         this.mesh = var4;
         if (var3 && this.mesh != null) {
            long var5 = System.currentTimeMillis();
            this.mesh.initializeMesh();
            this.mesh.markDraw();
            System.currentTimeMillis();
            if (var5 > 10L) {
               System.err.println("[MESH] time to pre-initialize mesh took long " + var5 + " ms");
            }
         }

      }
   }

   public static void freeMesh(ElementCollectionMesh var0) {
      var0.clear();
      synchronized(meshPool) {
         --ElementCollectionMesh.meshesInUse;
         if (meshPool.size() < 512) {
            meshPool.add(var0);
            return;
         }
      }

      long var1 = System.currentTimeMillis();
      var0.destroyBuffer();
      long var3;
      if ((var3 = System.currentTimeMillis() - var1) > 10L) {
         System.err.println("[ElementCollection] WARNING: Mesh buffer data destruction took long: " + var3 + " ms");
      }

   }

   public static ElementCollectionMesh getMeshInstance() {
      synchronized(meshPool) {
         ++ElementCollectionMesh.meshesInUse;
         return meshPool.isEmpty() ? new ElementCollectionMesh() : (ElementCollectionMesh)meshPool.remove(meshPool.size() - 1);
      }
   }

   public void setValid(boolean var1) {
      this.valid = var1;
   }

   public void decode4(long var1, Vector4i var3) {
      short var4 = (short)((int)(var1 >> 48 & 65535L));
      short var5 = (short)((int)(var1 >> 32 & 65535L));
      short var6 = (short)((int)(var1 >> 16 & 65535L));
      short var7 = (short)((int)(var1 & 65535L));
      var3.set(var7, var6, var5, var4);
   }

   public void addElement(long var1, int var3, int var4, int var5) {
      this.neighboringCollection.add(var1);
      this.updateBB(var3, var4, var5, var1);
   }

   public void cleanUp() {
      this.onClear();
      this.clear();
   }

   public void clear() {
      assert this.isDetailedNeighboringCollection() || this.neighboringCollection == null : this.getClass().getSimpleName() + "; " + this.isDetailedNeighboringCollection() + "; " + (this.neighboringCollection != null ? "EXISTS COL" : "NULL");

      if (this.isDetailedNeighboringCollection()) {
         this.neighboringCollection.clear();
      }

      Arrays.fill(this.touching, 0);
      this.size = 0;
      this.integrity = 0.0D;
      this.nonDetailedCollectionRandoms = null;
   }

   public boolean contains(long var1) {
      if (this.neighboringCollection == null) {
         throw new NullPointerException("Class: " + this.getClass().getSimpleName() + " Overwrite isDetailedElementCollections() to enable this collection");
      } else {
         return this.neighboringCollection.contains(var1);
      }
   }

   public boolean contains(Vector3i var1) {
      if (this.neighboringCollection == null) {
         throw new NullPointerException("Class: " + this.getClass().getSimpleName() + " Overwrite isDetailedElementCollections() to enable this collection");
      } else {
         return this.neighboringCollection.contains(getIndex(var1));
      }
   }

   public short getClazzId() {
      return this.clazzId;
   }

   public SegmentController getSegmentController() {
      return this.controller;
   }

   public SegmentPiece getElementCollectionId() {
      if (this.idChanged) {
         SegmentPiece var1;
         if ((var1 = this.controller.getSegmentBuffer().getPointUnsave(getPosIndexFrom4(this.idPos))) != null) {
            this.id = var1;
            this.idChanged = false;
         } else {
            this.idChanged = true;
         }
      }

      return this.id;
   }

   public Vector3i getMax(Vector3i var1) {
      var1.set(this.maxX, this.maxY, this.maxZ);
      return var1;
   }

   public Vector3i getMin(Vector3i var1) {
      var1.set(this.minX, this.minY, this.minZ);
      return var1;
   }

   public LongArrayList getNeighboringCollectionUnsave() {
      return this.neighboringCollection;
   }

   public LongArrayList getNeighboringCollection() {
      if (!this.elementCollectionManager.isDetailedElementCollections()) {
         throw new IllegalArgumentException("Can't access details for this elementCollection. For memory reason it only saves size and bounding box (" + this.getClass().getName() + ")");
      } else {
         return this.neighboringCollection;
      }
   }

   public long getSignificator() {
      return this.significator;
   }

   public Vector3i getSignificator(Vector3i var1) {
      if (this.significator == LONG_MIN) {
         var1.set(-32767, -32767, -32767);
      }

      getPosFromIndex(this.significator, var1);
      return var1;
   }

   public void initialize(short var1, ElementCollectionManager var2, SegmentController var3) {
      this.elementCollectionManager = var2;
      this.controller = var3;
      this.clazzId = var1;
      if (this.elementCollectionManager.isDetailedElementCollections()) {
         this.neighboringCollection = new LongArrayList();
      } else {
         this.neighboringCollection = null;
      }
   }

   public void takeVolatileCollection() {
      this.neighboringCollection = this.getVolatileCollection();
   }

   private LongArrayList getVolatileCollection() {
      synchronized(volatileCollectionPool) {
         ++takenCollections;
         return volatileCollectionPool.isEmpty() ? new LongArrayList() : (LongArrayList)volatileCollectionPool.remove(volatileCollectionPool.size() - 1);
      }
   }

   public int getStoreRandomBlocksForNonDetailedAmount() {
      return VoidElementManager.COLLECTION_INTEGRITY_EXPLOSION_AMOUNT;
   }

   public void storeRandomBlocksForNonDetailed() {
      int var1 = Math.min(this.neighboringCollection.size(), this.getStoreRandomBlocksForNonDetailedAmount());
      this.nonDetailedCollectionRandoms = new long[var1];
      Random var2 = new Random();

      for(int var3 = 0; var3 < var1; ++var3) {
         long var4 = this.neighboringCollection.getLong(var2.nextInt(this.neighboringCollection.size()));
         this.nonDetailedCollectionRandoms[var3] = var4;
      }

   }

   public void freeVolatileCollection() {
      assert !this.isDetailedNeighboringCollection() : this;

      this.neighboringCollection.size();
      this.neighboringCollection.clear();
      synchronized(volatileCollectionPool) {
         --takenCollections;
         if (volatileCollectionPool.size() < 16) {
            volatileCollectionPool.add(this.neighboringCollection);
         }
      }

      this.neighboringCollection = null;
   }

   public boolean onChangeFinished() {
      this.idChanged = true;
      if (this.elementCollectionManager.isUsingIntegrity()) {
         double var1 = VoidElementManager.COLLECTION_INTEGRITY_START_VALUE + (double)this.touching[0] * VoidElementManager.COLLECTION_INTEGRITY_BASE_TOUCHING_0 + (double)this.touching[1] * VoidElementManager.COLLECTION_INTEGRITY_BASE_TOUCHING_1 + (double)this.touching[2] * VoidElementManager.COLLECTION_INTEGRITY_BASE_TOUCHING_2 + (double)this.touching[3] * VoidElementManager.COLLECTION_INTEGRITY_BASE_TOUCHING_3 + (double)this.touching[4] * VoidElementManager.COLLECTION_INTEGRITY_BASE_TOUCHING_4 + (double)this.touching[5] * VoidElementManager.COLLECTION_INTEGRITY_BASE_TOUCHING_5 + (double)this.touching[6] * VoidElementManager.COLLECTION_INTEGRITY_BASE_TOUCHING_6;
         this.integrity = var1;
      } else {
         this.integrity = Double.POSITIVE_INFINITY;
      }

      this.elementCollectionManager.getElementManager().getManagerContainer().flagElementChanged();
      return true;
   }

   public void resetAABB() {
      this.significator = LONG_MIN;
      this.minX = Integer.MAX_VALUE;
      this.minY = Integer.MAX_VALUE;
      this.minZ = Integer.MAX_VALUE;
      this.maxX = Integer.MIN_VALUE;
      this.maxY = Integer.MIN_VALUE;
      this.maxZ = Integer.MIN_VALUE;
      this.id = null;
      this.idPos = LONG_MIN;
      this.idChanged = true;
   }

   public void onIntegrityHitForced(Damager var1) {
      assert this.getSegmentController().isOnServer();

      long var2;
      long var4 = getPosIndexFrom4(var2 = this.getRandomlySelectedFromLastThreadUpdate());
      short var6 = (short)getType(var2);
      this.onIntegrityHit(var4, var6, var1);
   }

   public void onIntegrityHit(long var1, short var3, Damager var4) {
      assert this.getSegmentController().isOnServer();

      if (this.getSegmentController().isOnServer() && this.elementCollectionManager.isExplosiveStructure()) {
         long var5 = (long)(VoidElementManager.COLLECTION_INTEGRITY_DAMAGE_PER_BLOCKS * (double)this.size);
         int var7 = (int)Math.min(VoidElementManager.COLLECTION_INTEGRITY_DAMAGE_MAX, (double)Math.min(2147483647L, Math.max(0L, var5)));
         this.explodeOnServer(VoidElementManager.COLLECTION_INTEGRITY_EXPLOSION_AMOUNT, var1, var3, VoidElementManager.COLLECTION_INTEGRITY_EXPLOSION_RATE, VoidElementManager.COLLECTION_INTEGRITY_EXPLOSION_RADIUS, var7, true, ModuleExplosion.ExplosionCause.INTEGRITY, var4);
      }
   }

   protected void significatorUpdate(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, long var10) {
      this.significator = getIndex(var7 - (var7 - var4) / 2, var8 - (var8 - var5) / 2, var9 - (var9 - var6) / 2);
   }

   public void significatorUpdateMin(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, long var10) {
      long var12;
      if ((var12 = getIndex(var1, var2, var3)) > this.significator || this.significator == LONG_MIN) {
         this.significator = var12;
      }

   }

   protected void significatorUpdateZ(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, long var10) {
      if (this.significator == LONG_MIN || var3 > getPosZ(this.significator)) {
         assert var3 == getPosZ(var10);

         this.significator = var10;
      }

   }

   public int size() {
      return this.size;
   }

   public void setSize(int var1) {
      this.size = var1;
   }

   public int getBBTotalSize() {
      return this.maxX - this.minX + (this.maxY - this.minY) + (this.maxZ - this.minZ);
   }

   public int getAbsBBMult() {
      return Math.abs(this.maxX - this.minX) * Math.abs(this.maxY - this.minY) * Math.abs(this.maxZ - this.minZ);
   }

   public String toString() {
      return this.getClass().getSimpleName() + this.hashCode() + "[" + this.getMin(new Vector3i()) + "/" + this.getMax(new Vector3i()) + "](" + (this.neighboringCollection != null ? this.neighboringCollection.size() : "volatile") + "; " + this.size + "; " + super.toString() + ")";
   }

   public void updateBB(int var1, int var2, int var3, long var4) {
      long var6 = (long)getPosX(this.idPos);
      long var8 = (long)getPosY(this.idPos);
      long var10 = (long)getPosZ(this.idPos);
      if (this.idPos == LONG_MIN || (long)var1 < var6 || (long)var1 == var6 && (long)var2 < var8 || (long)var1 == var6 && (long)var2 == var8 && (long)var3 < var10) {
         this.idPos = var4;
         this.idChanged = true;
      }

      this.minX = Math.min(var1, this.minX);
      this.minY = Math.min(var2, this.minY);
      this.minZ = Math.min(var3, this.minZ);
      this.maxX = Math.max(var1 + 1, this.maxX);
      this.maxY = Math.max(var2 + 1, this.maxY);
      this.maxZ = Math.max(var3 + 1, this.maxZ);
      this.significatorUpdate(var1, var2, var3, this.minX, this.minY, this.minZ, this.maxX, this.maxY, this.maxZ, var4);
   }

   public abstract ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3);

   public Vector3i getIdPos(Vector3i var1) {
      if (this.idPos == LONG_MIN) {
         var1.set(-32767, -32767, -32767);
      }

      getPosFromIndex(this.idPos, var1);
      return var1;
   }

   public String getInvalidReason() {
      return null;
   }

   public void setInvalidReason(String var1) {
   }

   public final boolean isDetailedNeighboringCollection() {
      return this.elementCollectionManager.isDetailedElementCollections();
   }

   public long getRandomlySelectedFromLastThreadUpdate() {
      return this.randomlySelectedFromLastThreadUpdate;
   }

   public void setRandomlySelectedFromLastThreadUpdate(long var1) {
      this.randomlySelectedFromLastThreadUpdate = var1;
   }

   public static long shiftIndex(long var0, int var2, int var3, int var4) {
      return getIndex(getPosX(var0) + var2, getPosY(var0) + var3, getPosZ(var0) + var4);
   }

   public static long shiftIndex4(long var0, int var2, int var3, int var4) {
      return getIndex4((short)(getPosX(var0) + var2), (short)(getPosY(var0) + var3), (short)(getPosZ(var0) + var4), (short)getType(var0));
   }

   public void explodeOnServer(int var1, long var2, short var4, long var5, int var7, int var8, boolean var9, ModuleExplosion.ExplosionCause var10, Damager var11) {
      assert this.getSegmentController().isOnServer();

      if (!this.getSegmentController().isOnServer()) {
         try {
            throw new Exception("Invalid Explosion call on client (not a crash)");
         } catch (Exception var12) {
            var12.printStackTrace();
         }
      } else {
         LongArrayList var13 = new LongArrayList(var1);
         int var3 = Math.min(this.size(), Math.max(1, this.size() / var1));
         int var14;
         if (this.isDetailedNeighboringCollection()) {
            for(var14 = 0; var14 < var1; ++var14) {
               var13.add(this.getNeighboringCollection().get(Universe.getRandom().nextInt(this.getNeighboringCollection().size())));
            }
         } else if (this.nonDetailedCollectionRandoms != null) {
            var14 = Math.min(VoidElementManager.COLLECTION_INTEGRITY_EXPLOSION_AMOUNT, this.nonDetailedCollectionRandoms.length);

            for(int var16 = 0; var16 < var14; ++var16) {
               var13.add(this.nonDetailedCollectionRandoms[var16]);
            }
         }

         System.err.println("[SERVER] Module Explosion " + this.getSegmentController() + ": amount " + var1 + " (actual " + var13.size() + ", rate " + var3 + ") of " + this.size() + ", delay " + var5 + ", radius " + var7 + ", damage " + var8);
         ModuleExplosion var15;
         (var15 = new ModuleExplosion(var13, var5, var7, var8, this.idPos, var10, new BoundingBox(new Vector3i(this.minX, this.minY, this.minZ), new Vector3i(this.maxX, this.maxY, this.maxZ)))).setChain(var9);
         ((ManagedSegmentController)this.getSegmentController()).getManagerContainer().addModuleExplosions(var15);
      }
   }

   public static boolean overlaps(ElementCollection var0, ElementCollection var1, int var2, int var3) {
      int var4 = var0.maxX + var2;
      int var5 = var0.maxY + var2;
      int var6 = var0.maxZ + var2;
      int var7 = var0.minX - var2;
      int var8 = var0.minY - var2;
      int var13 = var0.minZ - var2;
      var2 = var1.maxX + var3;
      int var9 = var1.maxY + var3;
      int var10 = var1.maxZ + var3;
      int var11 = var1.minX - var3;
      int var12 = var1.minY - var3;
      int var14 = var1.minZ - var3;
      return var7 <= var2 && var4 >= var11 && var8 <= var9 && var5 >= var12 && var13 <= var10 && var6 >= var14;
   }

   public ElementCollectionMesh getMesh() {
      return this.mesh;
   }

   public void markDraw() {
      if (this.getMesh() != null) {
         this.mesh.markDraw();
      }

   }

   public void setDrawColor(Vector4f var1) {
      if (this.getMesh() != null) {
         this.getMesh().setColor(var1);
      }

   }

   public void setDrawColor(float var1, float var2, float var3, float var4) {
      if (this.getMesh() != null) {
         this.getMesh().setColor(var1, var2, var3, var4);
      }

   }

   public double getIntegrity() {
      return this.integrity;
   }

   public boolean containsAABB(long var1) {
      int var3 = getPosX(var1);
      int var4 = getPosY(var1);
      int var5 = getPosZ(var1);
      return var3 >= this.minX && var3 < this.maxX && var4 >= this.minY && var4 < this.maxY && var5 >= this.minZ && var5 < this.maxZ;
   }
}
