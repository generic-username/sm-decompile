package org.schema.game.common.data.element;

import org.schema.game.common.controller.elements.ManagerModuleCollection;

public interface ScannerManagerInterface {
   ManagerModuleCollection getScanner();
}
