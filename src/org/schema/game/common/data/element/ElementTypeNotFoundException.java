package org.schema.game.common.data.element;

public class ElementTypeNotFoundException extends RuntimeException {
   private static final long serialVersionUID = 1L;

   public ElementTypeNotFoundException(String var1) {
      super(var1);
   }
}
