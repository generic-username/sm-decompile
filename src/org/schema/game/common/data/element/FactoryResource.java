package org.schema.game.common.data.element;

import org.w3c.dom.Document;

public class FactoryResource {
   public int count;
   public short type;

   public FactoryResource(int var1, short var2) {
      this.count = var1;
      this.type = var2;
   }

   public org.w3c.dom.Element getNode(Document var1) throws CannotAppendXMLException {
      org.w3c.dom.Element var3;
      (var3 = var1.createElement("Item")).setAttribute("count", String.valueOf(this.count));
      String var2;
      if ((var2 = ElementInformation.getKeyId(this.type)) == null) {
         throw new CannotAppendXMLException("[RecipeResource] Cannot find property key for Block ID " + this.type + "; Check your Block properties file");
      } else {
         var3.setTextContent(var2);
         return var3;
      }
   }

   public String toString() {
      return this.count + " of " + ElementKeyMap.toString(this.type);
   }
}
