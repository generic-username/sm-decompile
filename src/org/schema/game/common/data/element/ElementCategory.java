package org.schema.game.common.data.element;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import org.schema.common.ParseException;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.controller.damage.effects.InterEffectHandler;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.facedit.ElementTreeInterface;
import org.schema.game.common.util.GuiErrorHandler;

public class ElementCategory {
   private final String category;
   private final List children = new ObjectArrayList();
   private final List infoElements = new ObjectArrayList();
   private ElementCategory parent;

   public ElementCategory(String var1, ElementCategory var2) {
      this.category = var1;
      this.parent = var2;
   }

   public void clear() {
      this.children.clear();
      this.infoElements.clear();
   }

   public String find(ElementInformation var1) {
      if (this.equals(var1.getType())) {
         return this.category;
      } else {
         Iterator var2 = this.getChildren().iterator();

         String var3;
         do {
            if (!var2.hasNext()) {
               return null;
            }
         } while((var3 = ((ElementCategory)var2.next()).find(var1)) == null);

         return var3;
      }
   }

   public ElementCategory find(String var1) {
      if (this.category.toLowerCase(Locale.ENGLISH).equals(var1.toLowerCase(Locale.ENGLISH))) {
         return this;
      } else {
         Iterator var2 = this.getChildren().iterator();

         ElementCategory var3;
         do {
            if (!var2.hasNext()) {
               return null;
            }
         } while((var3 = ((ElementCategory)var2.next()).find(var1)) == null);

         return var3;
      }
   }

   public String getCategory() {
      return this.category;
   }

   public List getChildren() {
      return this.children;
   }

   public List getInfoElements() {
      return this.infoElements;
   }

   public List getInfoElementsRecursive(List var1) {
      var1.addAll(this.infoElements);
      Iterator var2 = this.getChildren().iterator();

      while(var2.hasNext()) {
         ((ElementCategory)var2.next()).getInfoElementsRecursive(var1);
      }

      return var1;
   }

   public boolean hasChildren() {
      return !this.getChildren().isEmpty();
   }

   public boolean insertRecusrive(ElementInformation var1) {
      if (this.equals(var1.getType())) {
         this.infoElements.add(var1);
         return true;
      } else {
         Iterator var2 = this.getChildren().iterator();

         do {
            if (!var2.hasNext()) {
               return false;
            }
         } while(!((ElementCategory)var2.next()).insertRecusrive(var1));

         return true;
      }
   }

   public int hashCode() {
      return this.category.hashCode();
   }

   public boolean equals(Object var1) {
      return this.category.equals(((ElementCategory)var1).category) && (this.parent == null && ((ElementCategory)var1).parent == null || this.parent.equals(((ElementCategory)var1).parent));
   }

   public String toString() {
      return this.category.toString();
   }

   public boolean isRoot() {
      return this.parent == null;
   }

   public void print() {
      this.printRec(1);
   }

   private void printItems(int var1) {
      StringBuilder var2 = new StringBuilder();

      int var3;
      for(var3 = 0; var3 < var1; ++var3) {
         var2.append("-");
      }

      for(var3 = 0; var3 < this.getInfoElements().size(); ++var3) {
         ElementInformation var4 = (ElementInformation)this.getInfoElements().get(var3);
         System.err.println(var2.toString() + " " + var4.getName());
      }

   }

   private void printRec(int var1) {
      StringBuilder var2 = new StringBuilder();

      int var3;
      for(var3 = 0; var3 < var1; ++var3) {
         var2.append("#");
      }

      System.err.println(var2.toString() + " " + this.category.toString());
      this.printItems(var1);

      for(var3 = 0; var3 < this.children.size(); ++var3) {
         ((ElementCategory)this.children.get(var3)).printRec(var1 + 1);
      }

   }

   public void removeRecursive(ElementInformation var1) {
      if (this.infoElements.contains(var1)) {
         this.infoElements.remove(var1);
      } else {
         Iterator var2 = this.getChildren().iterator();

         while(var2.hasNext()) {
            ((ElementCategory)var2.next()).removeRecursive(var1);
         }

      }
   }

   public boolean hasParent(String var1) {
      if (this.category.toLowerCase(Locale.ENGLISH).equals(var1.toLowerCase(Locale.ENGLISH))) {
         return true;
      } else {
         return this.parent != null ? this.parent.hasParent(var1) : false;
      }
   }

   public ElementCategory getChild(String var1) {
      for(int var2 = 0; var2 < this.children.size(); ++var2) {
         if (((ElementCategory)this.children.get(var2)).getCategory().equals(var1)) {
            return (ElementCategory)this.children.get(var2);
         }
      }

      throw new NullPointerException(var1);
   }

   public void merge(ElementCategory var1) {
      if (this.category.equals(var1.category)) {
         for(int var2 = 0; var2 < var1.infoElements.size(); ++var2) {
            if (!this.infoElements.contains(var1.infoElements.get(var2))) {
               this.infoElements.add(var1.infoElements.get(var2));
            }
         }

         boolean var5 = false;

         for(int var3 = 0; var3 < var1.children.size(); ++var3) {
            for(int var4 = 0; var4 < this.children.size(); ++var4) {
               if (((ElementCategory)var1.children.get(var3)).category.toLowerCase(Locale.ENGLISH).equals(((ElementCategory)this.children.get(var4)).category.toLowerCase(Locale.ENGLISH))) {
                  ((ElementCategory)this.children.get(var4)).merge((ElementCategory)var1.children.get(var3));
                  var5 = true;
                  break;
               }
            }

            if (!var5) {
               this.appendRecursive((ElementCategory)var1.children.get(var3));
            }
         }
      }

   }

   private void appendRecursive(ElementCategory var1) {
      ElementCategory var2 = new ElementCategory(var1.category, this);
      Iterator var3 = var1.infoElements.iterator();

      while(var3.hasNext()) {
         ElementInformation var4;
         (var4 = (ElementInformation)var3.next()).type = var2;
         var2.infoElements.add(var4);
      }

      this.children.add(var2);

      for(int var5 = 0; var5 < var1.children.size(); ++var5) {
         this.appendRecursive((ElementCategory)var1.children.get(var5));
      }

   }

   public void addContextMenu(final ElementTreeInterface var1, JPopupMenu var2, Component var3) {
      ObjectArrayList var4 = new ObjectArrayList();
      this.getInfoElementsRecursive(var4);
      JMenuItem var5;
      (var5 = new JMenuItem("Sort By Reactor Chamber Root")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            Comparator var2 = new Comparator() {
               public int compare(ElementInformation var1x, ElementInformation var2) {
                  int var3;
                  return (var3 = var1x.chamberRoot - var2.chamberRoot) == 0 ? var1x.getName().compareTo(var2.getName()) : var3;
               }
            };
            Collections.sort(ElementCategory.this.infoElements, var2);
            var1.reinitializeElements();
         }
      });
      var2.add(var5);
      (var5 = new JMenuItem("Bulk Create Slabs")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            Object[] var4 = new Object[]{"Ok", "Cancel"};
            String var2 = "Create Slabs";
            final JFrame var5;
            (var5 = new JFrame(var2)).setUndecorated(true);
            SwingUtilities.invokeLater(new Runnable() {
               public void run() {
                  var5.setVisible(true);
               }
            });
            Dimension var3 = Toolkit.getDefaultToolkit().getScreenSize();
            var5.setLocation(var3.width / 2, var3.height / 2);
            switch(JOptionPane.showOptionDialog(var5, "Are you sure you want to create slabs\nfor all items in this category and its subcategories?", "Confirm", 2, 2, (Icon)null, var4, var4[1])) {
            case 0:
               ElementCategory.this.bulkCreateSlabs(false);
            default:
            }
         }
      });
      var2.add(var5);
      (var5 = new JMenuItem("Bulk Reinitialize Existing Slabs")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            Object[] var4 = new Object[]{"Ok", "Cancel"};
            String var2 = "Reinitialize slabs";
            final JFrame var5;
            (var5 = new JFrame(var2)).setUndecorated(true);
            SwingUtilities.invokeLater(new Runnable() {
               public void run() {
                  var5.setVisible(true);
               }
            });
            Dimension var3 = Toolkit.getDefaultToolkit().getScreenSize();
            var5.setLocation(var3.width / 2, var3.height / 2);
            switch(JOptionPane.showOptionDialog(var5, "This will reinitialize all existing slabs", "Confirm", 2, 2, (Icon)null, var4, var4[1])) {
            case 0:
               ElementCategory.this.bulkCreateSlabs(true);
            default:
            }
         }
      });
      var2.add(var5);
      Field[] var9 = ElementInformation.class.getFields();

      for(int var8 = 0; var8 < var9.length; ++var8) {
         Field var6;
         (var6 = var9[var8]).setAccessible(true);
         org.schema.game.common.data.element.annotation.Element var7;
         if ((var7 = (org.schema.game.common.data.element.annotation.Element)var6.getAnnotation(org.schema.game.common.data.element.annotation.Element.class)) != null) {
            this.addByType(var7, var6, var2, var3, var4);
         }
      }

   }

   private void bulkCreateSlabs(boolean var1) {
      Iterator var2 = this.getInfoElementsRecursive(new ObjectArrayList()).iterator();

      while(true) {
         ElementInformation var3;
         do {
            do {
               do {
                  if (!var2.hasNext()) {
                     return;
                  }
               } while((var3 = (ElementInformation)var2.next()).getSlab() != 0);
            } while(var3.getBlockStyle() != BlockStyle.NORMAL);
         } while(var1 && var3.slabIds == null);

         try {
            ElementKeyMap.deleteBlockSlabs(var3);
            System.err.println((var1 ? "REINITIALIZING " : "CREATING ") + "SLABS FOR: " + var3);
            ElementKeyMap.createBlockSlabs(var3);
         } catch (ParseException var4) {
            var4.printStackTrace();
         }
      }
   }

   private void addByType(final org.schema.game.common.data.element.annotation.Element var1, final Field var2, JPopupMenu var3, final Component var4, final List var5) {
      try {
         if (var1.canBulkChange() && (var2.getType() == Boolean.TYPE || var2.getType() == Float.TYPE || var2.getType() == Long.TYPE || var2.getType() == Short.TYPE || var2.getType() == Integer.TYPE || var2.getType() == Byte.TYPE || var2.getType() == Double.TYPE || var2.getType().equals(InterEffectSet.class))) {
            JMenuItem var6 = new JMenuItem("Bulk Set " + var1.parser().tag);
            if (var2.getType().equals(InterEffectSet.class)) {
               var6.addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1x) {
                     InterEffectHandler.InterEffectType[] var10;
                     int var2x = (var10 = InterEffectHandler.InterEffectType.values()).length;

                     for(int var3 = 0; var3 < var2x; ++var3) {
                        InterEffectHandler.InterEffectType var4x = var10[var3];
                        String var5x;
                        if ((var5x = (String)JOptionPane.showInputDialog(var4, "Enter new value for " + var4x.id, "Bulk Set " + var1.parser().tag + " -> " + var4x.id, -1, (Icon)null, (Object[])null, (Object)null)) != null && var5x.length() > 0) {
                           Iterator var6 = var5.iterator();

                           while(var6.hasNext()) {
                              ElementInformation var7 = (ElementInformation)var6.next();

                              try {
                                 ((InterEffectSet)var2.get(var7)).setStrength(var4x, Float.parseFloat(var5x));
                              } catch (IllegalArgumentException var8) {
                                 var8.printStackTrace();
                              } catch (IllegalAccessException var9) {
                                 var9.printStackTrace();
                              }
                           }
                        }
                     }

                  }
               });
            } else {
               var6.addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent var1x) {
                     String var6;
                     if ((var6 = (String)JOptionPane.showInputDialog(var4, "Enter new value", "Bulk Set " + var1.parser().tag, -1, (Icon)null, (Object[])null, (Object)null)) != null && var6.length() > 0) {
                        try {
                           Iterator var4x;
                           ElementInformation var8;
                           if (var2.getType() == Boolean.TYPE) {
                              boolean var14 = Boolean.parseBoolean(var6);
                              var4x = var5.iterator();

                              while(var4x.hasNext()) {
                                 var8 = (ElementInformation)var4x.next();
                                 var2.setBoolean(var8, var14);
                              }

                           } else if (var2.getType() == Float.TYPE) {
                              float var13 = Float.parseFloat(var6);
                              var4x = var5.iterator();

                              while(var4x.hasNext()) {
                                 var8 = (ElementInformation)var4x.next();
                                 var2.setFloat(var8, var13);
                              }

                           } else if (var2.getType() == Integer.TYPE) {
                              int var12 = Integer.parseInt(var6);
                              var4x = var5.iterator();

                              while(var4x.hasNext()) {
                                 var8 = (ElementInformation)var4x.next();
                                 var2.setInt(var8, var12);
                              }

                           } else {
                              ElementInformation var2x;
                              Iterator var7;
                              if (var2.getType() == Long.TYPE) {
                                 long var11 = Long.parseLong(var6);
                                 var7 = var5.iterator();

                                 while(var7.hasNext()) {
                                    var2x = (ElementInformation)var7.next();
                                    var2.setLong(var2x, var11);
                                 }

                              } else if (var2.getType() == Short.TYPE) {
                                 short var10 = Short.parseShort(var6);
                                 var4x = var5.iterator();

                                 while(var4x.hasNext()) {
                                    var8 = (ElementInformation)var4x.next();
                                    var2.setShort(var8, var10);
                                 }

                              } else if (var2.getType() == Byte.TYPE) {
                                 byte var9 = Byte.parseByte(var6);
                                 var4x = var5.iterator();

                                 while(var4x.hasNext()) {
                                    var8 = (ElementInformation)var4x.next();
                                    var2.setByte(var8, var9);
                                 }

                              } else if (var2.getType() != Double.TYPE) {
                                 throw new IllegalArgumentException("Unknown type: " + var2.getType());
                              } else {
                                 double var3 = Double.parseDouble(var6);
                                 var7 = var5.iterator();

                                 while(var7.hasNext()) {
                                    var2x = (ElementInformation)var7.next();
                                    var2.setDouble(var2x, var3);
                                 }

                              }
                           }
                        } catch (Exception var5x) {
                           var5x.printStackTrace();
                           GuiErrorHandler.processNormalErrorDialogException(var5x, true);
                        }
                     }
                  }
               });
            }

            var3.add(var6);
         }

      } catch (Exception var7) {
         var7.printStackTrace();
         GuiErrorHandler.processNormalErrorDialogException(var7, true);
      }
   }
}
