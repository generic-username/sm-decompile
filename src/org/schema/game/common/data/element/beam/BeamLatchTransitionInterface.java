package org.schema.game.common.data.element.beam;

import javax.vecmath.Vector3f;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.data.SegmentPiece;

public interface BeamLatchTransitionInterface {
   SegmentPiece selectNextToLatch(BeamState var1, short var2, long var3, long var5, Vector3f var7, Vector3f var8, AbstractBeamHandler var9, SegmentController var10);
}
