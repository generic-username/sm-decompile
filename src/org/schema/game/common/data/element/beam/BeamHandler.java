package org.schema.game.common.data.element.beam;

import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.controller.elements.BeamState;

public abstract class BeamHandler extends AbstractBeamHandler {
   protected static final float BEAM_TIMEOUT_IN_SECS = 0.05F;
   private final BeamLatchTransitionInterface latchTransitionInterface = this.initLatchTransitionInterface();

   protected BeamLatchTransitionInterface initLatchTransitionInterface() {
      return new DefaultLatchTransitionInterface();
   }

   public BeamHandler(SegmentController var1, BeamHandlerContainer var2) {
      super(var2, var1);
   }

   public void transform(BeamState var1) {
      if (!((SegmentController)this.getBeamShooter()).isOnServer()) {
         ((SegmentController)this.getBeamShooter()).getWorldTransformOnClient().transform(var1.from);
      } else {
         ((SegmentController)this.getBeamShooter()).getWorldTransform().transform(var1.from);
      }
   }

   public InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3) {
      return ((SegmentController)this.getBeamShooter()).getAttackEffectSet(var1, var3);
   }

   public MetaWeaponEffectInterface getMetaWeaponEffect(long var1, DamageDealerType var3) {
      return ((SegmentController)this.getBeamShooter()).getMetaWeaponEffect(var1, var3);
   }

   public boolean isUsingOldPower() {
      return ((SegmentController)this.getBeamShooter()).isUsingOldPower();
   }

   public BeamLatchTransitionInterface getBeamLatchTransitionInterface() {
      return this.latchTransitionInterface;
   }

   public int getSectorId() {
      return ((SegmentController)this.getBeamShooter()).getSectorId();
   }
}
