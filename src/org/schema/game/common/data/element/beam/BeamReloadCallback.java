package org.schema.game.common.data.element.beam;

public interface BeamReloadCallback {
   void setShotReloading(long var1);

   boolean canUse(long var1, boolean var3);

   boolean isInitializing(long var1);

   long getNextShoot();

   long getCurrentReloadTime();

   boolean consumePower(float var1);

   boolean canConsumePower(float var1);

   double getPower();

   boolean isUsingPowerReactors();

   void flagBeamFiredWithoutTimeout();
}
