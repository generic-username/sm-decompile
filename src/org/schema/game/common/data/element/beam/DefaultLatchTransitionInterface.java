package org.schema.game.common.data.element.beam;

import java.util.Random;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.graphicsengine.forms.debug.DebugLine;

public class DefaultLatchTransitionInterface implements BeamLatchTransitionInterface {
   private final Vector3f toTmp = new Vector3f();
   private final SegmentPiece tmp = new SegmentPiece();

   public SegmentPiece selectNextToLatch(BeamState var1, short var2, long var3, long var5, Vector3f var7, Vector3f var8, AbstractBeamHandler var9, SegmentController var10) {
      SegmentPiece var18 = new SegmentPiece();
      int var19 = ElementCollection.getPosX(var5);
      int var4 = ElementCollection.getPosY(var5);
      int var20 = ElementCollection.getPosZ(var5);
      Vector3f var6;
      (var6 = new Vector3f(var8)).sub(var7);
      var6.normalize();
      float var21 = 1.0F;
      Random var11 = new Random();

      for(int var12 = 1; var12 <= 6; ++var12) {
         for(int var13 = 0; var13 < Element.DIRECTIONSi.length; var13 += 2) {
            int var14 = Element.DIRECTIONSi[var13].x * var12;
            int var15 = Element.DIRECTIONSi[var13].y * var12;
            int var16 = Element.DIRECTIONSi[var13].z * var12;
            Vector3i var23 = new Vector3i(var14, var15, var16);
            if (var11.nextInt(2) == 0) {
               var23.scale(-1);
            }

            var15 = var19 + var23.x;
            var16 = var4 + var23.y;
            int var17 = var20 + var23.z;
            SegmentPiece var27;
            if ((var27 = var10.getSegmentBuffer().getPointUnsave(var15, var16, var17, this.tmp)) != null) {
               Vector3f var24 = new Vector3f((float)var23.x, (float)var23.y, (float)var23.z);
               float var25 = Math.abs(var6.dot(var24));
               if (var21 > var25 && var27.isValid() && var27.isAlive()) {
                  var21 = var25;
                  var18.setByReference(var27);
               }
            }
         }

         if (var18.isValid() && var18.isAlive()) {
            var18.getWorldPos(this.toTmp, var9.getBeamShooter().getSectorId());
            CubeRayCastResult var22 = var9.testRayOnOvelappingSectors(var7, this.toTmp, var1, var9.getBeamShooter());
            if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
               DebugLine var26 = new DebugLine(new Vector3f(var7), new Vector3f(this.toTmp));
               if (var22.hasHit() && var22.getSegment() != null) {
                  if (var18.equalsSegmentPos(var22.getSegment(), var22.getCubePos())) {
                     var26.setColor(new Vector4f(1.0F, 0.0F, 0.0F, 1.0F));
                  } else {
                     var26.setColor(new Vector4f(0.0F, 1.0F, 0.0F, 1.0F));
                  }
               } else {
                  var26.setColor(new Vector4f(0.0F, 1.0F, 1.0F, 1.0F));
               }

               DebugDrawer.lines.add(var26);
            }

            if (var22.hasHit() && var22.getSegment() != null && !var18.equalsSegmentPos(var22.getSegment(), var22.getCubePos())) {
               if (var1.checkLatchConnection) {
                  return null;
               }

               var18.setByReference(var22.getSegment(), var22.getCubePos());
               break;
            }
         }
      }

      return var18.getSegment() != null ? var18 : null;
   }
}
