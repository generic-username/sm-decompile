package org.schema.game.common.data.element;

import it.unimi.dsi.fastutil.ints.IntCollection;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.FinalVector3i;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;

public abstract class Element {
   public static final float BLOCK_SIZE = 1.0F;
   public static final short TYPE_BLENDED_START = -1;
   public static final short TYPE_NONE = 0;
   public static final short TYPE_ALL = 32767;
   public static final short TYPE_SIGNAL = 30000;
   public static final short TYPE_RAIL_TRACK = 29999;
   public static final short TYPE_RAIL_INV = 29998;
   public static final int FRONT = 0;
   public static final int BACK = 1;
   public static final int TOP = 2;
   public static final int BOTTOM = 3;
   public static final int RIGHT = 4;
   public static final int LEFT = 5;
   public static final int[] OPPOSITE_SIDE = new int[]{1, 0, 3, 2, 5, 4};
   public static final int[][] dirs26 = new int[][]{{-1, -1, -1}, {1, -1, -1}, {-1, -1, 1}, {1, -1, 1}, {-1, 1, -1}, {1, 1, -1}, {-1, 1, 1}, {1, 1, 1}};
   public static final int FLAG_FRONT = 1;
   public static final int FLAG_BACK = 2;
   public static final int FLAG_TOP = 4;
   public static final int FLAG_BOTTOM = 8;
   public static final int FLAG_RIGHT = 16;
   public static final int FLAG_LEFT = 32;
   public static final int VIS_ALL = 63;
   public static final int[] SIDE_FLAG = new int[]{1, 2, 4, 8, 16, 32};
   public static final Vector3b[] DIRECTIONSb = new Vector3b[]{new Vector3b(0.0F, 0.0F, 1.0F), new Vector3b(0.0F, 0.0F, -1.0F), new Vector3b(0.0F, 1.0F, 0.0F), new Vector3b(0.0F, -1.0F, 0.0F), new Vector3b(1.0F, 0.0F, 0.0F), new Vector3b(-1.0F, 0.0F, 0.0F)};
   public static final Vector3i[] DIRECTIONSi = new Vector3i[]{new FinalVector3i(0, 0, 1), new FinalVector3i(0, 0, -1), new FinalVector3i(0, 1, 0), new FinalVector3i(0, -1, 0), new FinalVector3i(1, 0, 0), new FinalVector3i(-1, 0, 0)};
   public static final int[] SIGNIFICANT_COORD = new int[]{2, 2, 1, 1, 0, 0};
   public static final float[] COORD_DIR = new float[]{1.0F, -1.0F, 1.0F, -1.0F, 1.0F, -1.0F};
   public static final Vector4f[] SIDE_COLORS = new Vector4f[]{new Vector4f(1.0F, 0.0F, 0.0F, 1.0F), new Vector4f(0.0F, 1.0F, 0.0F, 1.0F), new Vector4f(0.0F, 0.0F, 1.0F, 1.0F), new Vector4f(1.0F, 0.0F, 1.0F, 1.0F), new Vector4f(0.0F, 1.0F, 1.0F, 1.0F), new Vector4f(1.0F, 1.0F, 0.0F, 1.0F)};
   public static final Vector3f[] DIRECTIONSf;
   public static final int SIDE_INDEX_COUNT = 4;
   public static final int INDEX_BOTTOM = 0;
   public static final int OLDRIGHT = 0;
   public static final int OLDLEFT = 1;
   public static final int OLDTOP = 2;
   public static final int OLDBOTTOM = 3;
   public static final int OLDFRONT = 4;
   public static final int OLDBACK = 5;
   public static final int OLDBACK_BACK = 6;
   public static final int OLDBACK_LEFT = 7;
   private static final float margin = 0.001F;
   public static byte[] orientationMapping;
   public static byte[] orientationBackMapping;
   public static byte FULLVIS;
   private static Vector3f sumTemp;
   private static int[] s;

   public static int countBits(int var0) {
      return ((var0 = ((var0 = ((var0 = ((var0 = (var0 >>> 1 & 1431655765) + (var0 & 1431655765)) >>> 2 & 858993459) + (var0 & 858993459)) >>> 4 & 252645135) + (var0 & 252645135)) >>> 8 & 16711935) + (var0 & 16711935)) >>> 16) + (var0 & '\uffff');
   }

   private static void createOrientationMapping() {
      (orientationMapping = new byte[24])[4] = 0;
      orientationMapping[5] = 1;
      orientationMapping[2] = 2;
      orientationMapping[3] = 3;
      orientationMapping[0] = 4;
      orientationMapping[1] = 5;
      orientationMapping[6] = 6;
      orientationMapping[7] = 7;
      orientationMapping[12] = 8;
      orientationMapping[13] = 9;
      orientationMapping[10] = 10;
      orientationMapping[11] = 11;
      orientationMapping[8] = 12;
      orientationMapping[9] = 13;
      orientationMapping[14] = 14;
      orientationMapping[15] = 15;
      orientationMapping[20] = 16;
      orientationMapping[21] = 17;
      orientationMapping[18] = 18;
      orientationMapping[19] = 19;
      orientationMapping[16] = 20;
      orientationMapping[17] = 21;
      orientationMapping[22] = 22;
      orientationMapping[23] = 23;
      (orientationBackMapping = new byte[24])[0] = 4;
      orientationBackMapping[1] = 5;
      orientationBackMapping[2] = 2;
      orientationBackMapping[3] = 3;
      orientationBackMapping[4] = 0;
      orientationBackMapping[5] = 1;
      orientationBackMapping[6] = 6;
      orientationBackMapping[7] = 7;
      orientationBackMapping[8] = 12;
      orientationBackMapping[9] = 13;
      orientationBackMapping[10] = 10;
      orientationBackMapping[11] = 11;
      orientationBackMapping[12] = 8;
      orientationBackMapping[13] = 9;
      orientationBackMapping[14] = 14;
      orientationBackMapping[15] = 15;
      orientationBackMapping[16] = 20;
      orientationBackMapping[17] = 21;
      orientationBackMapping[18] = 18;
      orientationBackMapping[19] = 19;
      orientationBackMapping[20] = 16;
      orientationBackMapping[21] = 17;
      orientationBackMapping[22] = 22;
      orientationBackMapping[23] = 23;
   }

   public static int getOpposite(int var0) {
      switch(var0) {
      case 0:
         return 1;
      case 1:
         return 0;
      case 2:
         return 3;
      case 3:
         return 2;
      case 4:
         return 5;
      case 5:
         return 4;
      default:
         throw new RuntimeException("SIDE NOT FOUND: " + var0);
      }
   }

   public static int getClockWiseZ(int var0) {
      switch(var0) {
      case 0:
         return 0;
      case 1:
         return 1;
      case 2:
         return 4;
      case 3:
         return 5;
      case 4:
         return 3;
      case 5:
         return 2;
      default:
         throw new RuntimeException("SIDE NOT FOUND: " + var0);
      }
   }

   public static int getCounterClockWiseZ(int var0) {
      switch(var0) {
      case 0:
         return 0;
      case 1:
         return 1;
      case 2:
         return 5;
      case 3:
         return 4;
      case 4:
         return 2;
      case 5:
         return 3;
      default:
         throw new RuntimeException("SIDE NOT FOUND: " + var0);
      }
   }

   public static int getClockWiseX(int var0) {
      switch(var0) {
      case 0:
         return 3;
      case 1:
         return 2;
      case 2:
         return 0;
      case 3:
         return 1;
      case 4:
         return 4;
      case 5:
         return 5;
      default:
         throw new RuntimeException("SIDE NOT FOUND: " + var0);
      }
   }

   public static int getCounterClockWiseX(int var0) {
      switch(var0) {
      case 0:
         return 2;
      case 1:
         return 3;
      case 2:
         return 1;
      case 3:
         return 0;
      case 4:
         return 0;
      case 5:
         return 5;
      default:
         throw new RuntimeException("SIDE NOT FOUND: " + var0);
      }
   }

   public static int getCounterClockWiseY(int var0) {
      switch(var0) {
      case 0:
         return 4;
      case 1:
         return 5;
      case 2:
         return 2;
      case 3:
         return 3;
      case 4:
         return 1;
      case 5:
         return 0;
      default:
         throw new RuntimeException("SIDE NOT FOUND: " + var0);
      }
   }

   public static int getClockWiseY(int var0) {
      switch(var0) {
      case 0:
         return 5;
      case 1:
         return 4;
      case 2:
         return 2;
      case 3:
         return 3;
      case 4:
         return 0;
      case 5:
         return 1;
      default:
         throw new RuntimeException("SIDE NOT FOUND: " + var0);
      }
   }

   public static Matrix3f getRotationPerSide(int var0) {
      Matrix3f var1;
      (var1 = new Matrix3f()).setIdentity();
      switch(var0) {
      case 0:
         var1.setIdentity();
         break;
      case 1:
         var1.rotZ(-3.1415927F);
         break;
      case 2:
         var1.rotX(1.5707964F);
         break;
      case 3:
         var1.rotX(-1.5707964F);
         break;
      case 4:
         var1.rotZ(1.5707964F);
         break;
      case 5:
         var1.rotZ(-1.5707964F);
      }

      return var1;
   }

   public static Matrix3f getRotationPerSideTopBase(int var0) {
      Matrix3f var1;
      (var1 = new Matrix3f()).setIdentity();
      switch(var0) {
      case 0:
         var1.rotX(1.5707964F);
         break;
      case 1:
         var1.rotX(-1.5707964F);
         break;
      case 2:
         var1.setIdentity();
         break;
      case 3:
         var1.rotZ(-3.1415927F);
         break;
      case 4:
         var1.rotZ(1.5707964F);
         break;
      case 5:
         var1.rotZ(-1.5707964F);
      }

      return var1;
   }

   public static int getRelativeOrientation(Vector3f var0) {
      byte var1 = 0;
      if (Math.abs(var0.x) >= Math.abs(var0.y) && Math.abs(var0.x) >= Math.abs(var0.z)) {
         if (var0.x >= 0.0F) {
            var1 = 4;
         } else {
            var1 = 5;
         }
      } else if (Math.abs(var0.y) >= Math.abs(var0.x) && Math.abs(var0.y) >= Math.abs(var0.z)) {
         if (var0.y >= 0.0F) {
            var1 = 2;
         } else {
            var1 = 3;
         }
      } else if (Math.abs(var0.z) >= Math.abs(var0.y) && Math.abs(var0.z) >= Math.abs(var0.x)) {
         if (var0.z >= 0.0F) {
            var1 = 0;
         } else {
            var1 = 1;
         }
      }

      return var1;
   }

   public static int getSide(Vector3f var0, BlockShapeAlgorithm var1, Vector3i var2, short var3, int var4) {
      return getSide(var0, var1, var2, var3, var4, (IntCollection)null);
   }

   public static int getSide(Vector3f var0, BlockShapeAlgorithm var1, Vector3i var2, short var3, int var4, IntCollection var5) {
      return getSide(var0, var1, var2, 0.001F, var3, var4, var5);
   }

   public static int getSide(Vector3f var0, BlockShapeAlgorithm var1, Vector3i var2, float var3, short var4, int var5, IntCollection var6) {
      while(true) {
         int var7 = -1;
         float var9;
         float var12;
         if (var1 != null && var1.hasValidShape()) {
            var1.getShapeCenter(sumTemp);
            sumTemp.add(new Vector3f(0.5F, 0.5F, 0.5F));
            Vector3f var14 = new Vector3f(var0.x - 0.5F, var0.y - 0.5F, var0.z - 0.5F);
            Vector3f var8;
            if (Math.abs((var8 = new Vector3f(var14.x - (float)Math.floor((double)var14.x), var14.y - (float)Math.floor((double)var14.y), var14.z - (float)Math.floor((double)var14.z))).x - 0.5F) <= 0.45F && Math.abs(var8.y - 0.5F) <= 0.45F && Math.abs(var8.z - 0.5F) <= 0.45F) {
               var8.sub(sumTemp);
               var8.normalize();
               var8.negate();
               var9 = Float.MAX_VALUE;

               for(int var10 = 0; var10 < 6; ++var10) {
                  if ((var12 = DIRECTIONSf[var10].dot(var8)) < var9 && (var6 == null || !var6.contains(var10))) {
                     var9 = var12;
                     var7 = var10;
                  }
               }
            }
         }

         if (var7 == -1) {
            float var15 = (float)var2.x - 0.5F;
            float var16 = (float)var2.y - 0.5F;
            var9 = (float)var2.z - 0.5F;
            float var17 = (float)var2.x + 0.5F;
            float var11 = (float)var2.y + 0.5F;
            var12 = (float)var2.z + 0.5F;
            int var13;
            if (ElementKeyMap.isValidType(var4) && (var13 = ElementKeyMap.getInfoFast(var4).getSlab()) > 0) {
               float var18 = 0.5F - (float)var13 * 0.25F;
               switch(switchLeftRight(var5 % 6)) {
               case 0:
                  var15 = (float)var2.x - 0.5F;
                  var16 = (float)var2.y - 0.5F;
                  var9 = (float)var2.z - 0.5F;
                  var17 = (float)var2.x + 0.5F;
                  var11 = (float)var2.y + 0.5F;
                  var12 = (float)var2.z + var18;
                  break;
               case 1:
                  var15 = (float)var2.x - 0.5F;
                  var16 = (float)var2.y - 0.5F;
                  var9 = (float)var2.z - var18;
                  var17 = (float)var2.x + 0.5F;
                  var11 = (float)var2.y + 0.5F;
                  var12 = (float)var2.z + 0.5F;
                  break;
               case 2:
                  var15 = (float)var2.x - 0.5F;
                  var16 = (float)var2.y - 0.5F;
                  var9 = (float)var2.z - 0.5F;
                  var17 = (float)var2.x + 0.5F;
                  var11 = (float)var2.y + var18;
                  var12 = (float)var2.z + 0.5F;
                  break;
               case 3:
                  var15 = (float)var2.x - 0.5F;
                  var16 = (float)var2.y - var18;
                  var9 = (float)var2.z - 0.5F;
                  var17 = (float)var2.x + 0.5F;
                  var11 = (float)var2.y + 0.5F;
                  var12 = (float)var2.z + 0.5F;
                  break;
               case 4:
                  var15 = (float)var2.x - 0.5F;
                  var16 = (float)var2.y - 0.5F;
                  var9 = (float)var2.z - 0.5F;
                  var17 = (float)var2.x + var18;
                  var11 = (float)var2.y + 0.5F;
                  var12 = (float)var2.z + 0.5F;
                  break;
               case 5:
                  var15 = (float)var2.x - var18;
                  var16 = (float)var2.y - 0.5F;
                  var9 = (float)var2.z - 0.5F;
                  var17 = (float)var2.x + 0.5F;
                  var11 = (float)var2.y + 0.5F;
                  var12 = (float)var2.z + 0.5F;
               }
            }

            if (var0.x >= var17 - var3 && (var6 == null || !var6.contains(4))) {
               return 4;
            }

            if (var0.y >= var11 - var3 && (var6 == null || !var6.contains(2))) {
               return 2;
            }

            if (var0.z >= var12 - var3 && (var6 == null || !var6.contains(0))) {
               return 0;
            }

            if (var0.x <= var15 + var3 && (var6 == null || !var6.contains(5))) {
               return 5;
            }

            if (var0.y <= var16 + var3 && (var6 == null || !var6.contains(3))) {
               return 3;
            }

            if (var0.z <= var9 + var3 && (var6 == null || !var6.contains(1))) {
               return 1;
            }

            if (var7 < 0 && var3 < 0.5F) {
               var3 *= 2.0F;
               var1 = null;
               var0 = var0;
               continue;
            }
         }

         return var7;
      }
   }

   public static String getSideString(int var0) {
      switch(var0) {
      case 0:
         return "FRONT";
      case 1:
         return "BACK";
      case 2:
         return "TOP";
      case 3:
         return "BOTTOM";
      case 4:
         return "RIGHT";
      case 5:
         return "LEFT";
      default:
         return "[WARNING] UNKNOWN SIDE " + var0;
      }
   }

   public static String getSideStringLng(int var0) {
      switch(var0) {
      case 0:
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_ELEMENT_4;
      case 1:
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_ELEMENT_5;
      case 2:
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_ELEMENT_2;
      case 3:
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_ELEMENT_3;
      case 4:
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_ELEMENT_1;
      case 5:
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_ELEMENT_0;
      default:
         return "[WARNING] UNKNOWN SIDE " + var0;
      }
   }

   public static int getSide(Vector3i var0) {
      assert Math.abs(var0.x) + Math.abs(var0.y) + Math.abs(var0.z) == 1;

      if (var0.x == 1) {
         return 4;
      } else if (var0.x == -1) {
         return 5;
      } else if (var0.y == 1) {
         return 2;
      } else if (var0.y == -1) {
         return 3;
      } else if (var0.z == 1) {
         return 0;
      } else if (var0.z == -1) {
         return 1;
      } else {
         throw new RuntimeException("ERROR ON DIR: " + var0);
      }
   }

   public static void getRelativeForward(int var0, int var1, Vector3f var2) {
      if (var0 == 2) {
         var2.set(DIRECTIONSf[var1]);
      } else if (var0 == 3) {
         if (var1 != 5 && var1 != 4) {
            var2.set(DIRECTIONSf[getOpposite(var1)]);
         } else {
            var2.set(DIRECTIONSf[var1]);
         }
      } else if (var0 != 0 && var0 != 1) {
         if (var0 == 4 || var0 == 5) {
            if (var0 == 5) {
               var1 = getOpposite(var1);
            }

            switch(var1) {
            case 0:
               var2.set(DIRECTIONSf[var0 == 4 ? 0 : 1]);
               return;
            case 1:
               var2.set(DIRECTIONSf[var0 == 4 ? 1 : 0]);
               return;
            case 2:
               var2.set(DIRECTIONSf[5]);
               return;
            case 3:
               var2.set(DIRECTIONSf[4]);
               return;
            }
         }

      } else {
         if (var0 == 1) {
            var1 = getOpposite(var1);
         }

         switch(var1) {
         case 2:
            var2.set(DIRECTIONSf[1]);
            return;
         case 3:
            var2.set(DIRECTIONSf[0]);
            return;
         case 4:
            var2.set(DIRECTIONSf[var0 == 0 ? 4 : 5]);
            return;
         case 5:
            var2.set(DIRECTIONSf[var0 == 0 ? 5 : 4]);
            return;
         default:
         }
      }
   }

   public String toString() {
      return "ELEMENT";
   }

   public static byte switchLeftRight(int var0) {
      if (var0 == 5) {
         return 4;
      } else {
         return var0 == 4 ? 5 : (byte)var0;
      }
   }

   public static int getDirectionFromCoords(int var0, int var1, int var2) {
      assert Math.abs(var0) < 2;

      assert Math.abs(var1) < 2;

      assert Math.abs(var2) < 2;

      assert Math.abs(var0) + Math.abs(var1) + Math.abs(var2) == 1 : var0 + ", " + var1 + ", " + var2;

      return s[var0 + 1 + (var1 + 1) * 3 + (var2 + 1) * 9];
   }

   static {
      DIRECTIONSf = GlUtil.DIRECTIONSf;
      FULLVIS = 63;
      sumTemp = new Vector3f(0.0F, 0.0F, 0.0F);
      createOrientationMapping();
      s = new int[27];

      int var0;
      Vector3i var1;
      for(var0 = 0; var0 < DIRECTIONSi.length; s[var1.x + 1 + (var1.y + 1) * 3 + (var1.z + 1) * 9] = var0++) {
         var1 = DIRECTIONSi[var0];

         assert s[var1.x + 1 + (var1.y + 1) * 3 + (var1.z + 1) * 9] == 0;
      }

      for(var0 = 0; var0 < DIRECTIONSi.length; ++var0) {
         var1 = DIRECTIONSi[var0];

         assert getDirectionFromCoords(var1.x, var1.y, var1.z) == var0;
      }

   }
}
