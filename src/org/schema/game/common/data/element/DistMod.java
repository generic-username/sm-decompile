package org.schema.game.common.data.element;

import org.schema.common.util.linAlg.Vector3i;

public class DistMod {
   public Vector3i controllerPos;
   public long idPos;
   public byte effectId;
   public int dist;
   public boolean onServer;
}
