package org.schema.game.common.data.element;

import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongCollection;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongSet;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap.Entry;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap.FastEntrySet;
import it.unimi.dsi.fastutil.objects.ObjectCollection;
import it.unimi.dsi.fastutil.objects.ObjectSet;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.schine.resource.tag.SerializableTagElement;

public class ControlElementMapper implements Long2ObjectMap, SerializableTagElement {
   private Long2ObjectOpenHashMap backing = new Long2ObjectOpenHashMap();
   private Long2ObjectOpenHashMap all = new Long2ObjectOpenHashMap();
   private int deserializeShift;

   public void clearAndTrim() {
      this.clear();
      this.all.trim();
      this.backing.trim();
   }

   public Long2ObjectOpenHashMap getAll() {
      return this.all;
   }

   public int getAllElementsSize() {
      int var1 = 0;

      LongOpenHashSet var3;
      for(Iterator var2 = this.getAll().values().iterator(); var2.hasNext(); var1 += var3.size()) {
         var3 = (LongOpenHashSet)var2.next();
      }

      return var1;
   }

   public byte getFactoryId() {
      return 0;
   }

   public void writeToTag(DataOutput var1) throws IOException {
      ControlElementMap.serializeForDisk(var1, this);
   }

   public boolean put(long var1, long var3, short var5) {
      assert var5 != 0;

      assert var5 != 32767;

      Short2ObjectOpenHashMap var6;
      if (!this.containsKey(var1)) {
         var6 = new Short2ObjectOpenHashMap();
         this.put(var1, var6);
      } else {
         var6 = this.get(var1);
      }

      FastCopyLongOpenHashSet var7;
      if (!var6.containsKey(var5)) {
         var7 = new FastCopyLongOpenHashSet();
         var6.put(var5, var7);
      } else {
         var7 = (FastCopyLongOpenHashSet)var6.get(var5);
      }

      long var8 = ElementCollection.getIndex4(var3, var5);
      FastCopyLongOpenHashSet var10;
      if (!this.getAll().containsKey(var1)) {
         var10 = new FastCopyLongOpenHashSet();
         this.getAll().put(var1, var10);
      } else {
         var10 = (FastCopyLongOpenHashSet)this.getAll().get(var1);
      }

      var10.add(var8);
      return var7.add(var8);
   }

   public boolean put(Vector3i var1, long var2, short var4) {
      long var5 = ElementCollection.getIndex(var1);
      return this.put(var5, var2, var4);
   }

   public void putAll(long var1, short[] var3, short var4) {
      if (ElementKeyMap.exists(var4)) {
         Short2ObjectOpenHashMap var5;
         if (!this.containsKey(var1)) {
            var5 = new Short2ObjectOpenHashMap(var3.length / 3);
            this.put(var1, var5);
         } else {
            var5 = this.get(var1);
         }

         FastCopyLongOpenHashSet var6;
         if (!var5.containsKey(var4)) {
            var6 = new FastCopyLongOpenHashSet(var3.length / 3);
            var5.put(var4, var6);
         } else {
            var6 = (FastCopyLongOpenHashSet)var5.get(var4);
         }

         FastCopyLongOpenHashSet var10;
         if (!this.getAll().containsKey(var1)) {
            var10 = new FastCopyLongOpenHashSet(var3.length / 3);
            this.getAll().put(var1, var10);
         } else {
            var10 = (FastCopyLongOpenHashSet)this.getAll().get(var1);
         }

         int var9 = var3.length;

         for(int var2 = 0; var2 < var9; var2 += 3) {
            long var7 = ElementCollection.getIndex4(var3[var2], var3[var2 + 1], var3[var2 + 2], var4);
            var10.add(var7);
            var6.add(var7);
         }

      }
   }

   public boolean remove(long var1, long var3, short var5) {
      if (this.containsKey(var1) && this.get(var1).containsKey(var5)) {
         long var6 = ElementCollection.getIndex4(var3, var5);
         ((FastCopyLongOpenHashSet)this.getAll().get(var1)).remove(var6);
         return ((FastCopyLongOpenHashSet)this.get(var1).get(var5)).remove(var6);
      } else {
         return false;
      }
   }

   public Short2ObjectOpenHashMap remove(Vector3i var1) {
      long var2 = ElementCollection.getIndex(var1);
      this.getAll().remove(var2);
      return (Short2ObjectOpenHashMap)this.backing.remove(var2);
   }

   public boolean remove(Vector3i var1, Vector3i var2, short var3) {
      long var4 = ElementCollection.getIndex(var1);
      long var6 = ElementCollection.getIndex(var2);
      return this.remove(var4, var6, var3);
   }

   public void setFromMap(ControlElementMapper var1) {
      this.backing = new Long2ObjectOpenHashMap(var1.backing.size());
      this.all = new Long2ObjectOpenHashMap(var1.all.size());
      Iterator var2 = var1.long2ObjectEntrySet().iterator();

      while(var2.hasNext()) {
         Entry var3 = (Entry)var2.next();
         Short2ObjectOpenHashMap var4 = new Short2ObjectOpenHashMap(((Short2ObjectOpenHashMap)var3.getValue()).size());
         this.put(var3.getLongKey(), var4);
         FastCopyLongOpenHashSet var5 = new FastCopyLongOpenHashSet(((FastCopyLongOpenHashSet)var1.all.get(var3.getLongKey())).size());
         Iterator var6 = ((Short2ObjectOpenHashMap)var3.getValue()).short2ObjectEntrySet().iterator();

         while(var6.hasNext()) {
            it.unimi.dsi.fastutil.shorts.Short2ObjectMap.Entry var7 = (it.unimi.dsi.fastutil.shorts.Short2ObjectMap.Entry)var6.next();
            FastCopyLongOpenHashSet var8 = new FastCopyLongOpenHashSet(1);
            var4.put(var7.getShortKey(), var8);
            var8.deepApplianceCopy((FastCopyLongOpenHashSet)var7.getValue());
            var5.addAll((LongCollection)var7.getValue());
         }

         this.all.put(var3.getLongKey(), var5);
      }

   }

   public Short2ObjectOpenHashMap put(long var1, Short2ObjectOpenHashMap var3) {
      return (Short2ObjectOpenHashMap)this.backing.put(var1, var3);
   }

   public Short2ObjectOpenHashMap get(long var1) {
      return (Short2ObjectOpenHashMap)this.backing.get(var1);
   }

   public Short2ObjectOpenHashMap remove(long var1) {
      this.getAll().remove(var1);
      return (Short2ObjectOpenHashMap)this.backing.remove(var1);
   }

   public boolean containsKey(long var1) {
      return this.backing.containsKey(var1);
   }

   public void defaultReturnValue(Short2ObjectOpenHashMap var1) {
      this.backing.defaultReturnValue(var1);
   }

   public Short2ObjectOpenHashMap defaultReturnValue() {
      return (Short2ObjectOpenHashMap)this.backing.defaultReturnValue();
   }

   public ObjectSet entrySet() {
      return this.backing.entrySet();
   }

   public FastEntrySet long2ObjectEntrySet() {
      return this.backing.long2ObjectEntrySet();
   }

   public LongSet keySet() {
      return this.backing.keySet();
   }

   public ObjectCollection values() {
      return this.backing.values();
   }

   public Short2ObjectOpenHashMap put(Long var1, Short2ObjectOpenHashMap var2) {
      return (Short2ObjectOpenHashMap)this.backing.put(var1, var2);
   }

   public Short2ObjectOpenHashMap get(Object var1) {
      return (Short2ObjectOpenHashMap)this.backing.get(var1);
   }

   public boolean containsKey(Object var1) {
      return this.backing.containsKey(var1);
   }

   public Short2ObjectOpenHashMap remove(Object var1) {
      return (Short2ObjectOpenHashMap)this.backing.remove(var1);
   }

   public int size() {
      return this.backing.size();
   }

   public void clear() {
      this.all.clear();
      this.backing.clear();
   }

   public Short2ObjectOpenHashMap get(Long var1) {
      return (Short2ObjectOpenHashMap)this.backing.get(var1);
   }

   public boolean isEmpty() {
      return this.backing.isEmpty();
   }

   public boolean containsValue(Object var1) {
      return this.backing.containsValue(var1);
   }

   public void putAll(Map var1) {
      this.backing.putAll(var1);
   }

   public boolean trim() {
      return this.backing.trim();
   }

   public boolean trim(int var1) {
      return this.backing.trim(var1);
   }

   public int hashCode() {
      return this.backing.hashCode();
   }

   public boolean equals(Object var1) {
      return this.backing.equals(var1);
   }

   public Long2ObjectOpenHashMap clone() {
      return this.backing.clone();
   }

   public void deserialize(DataInput var1) throws IOException {
      int var2;
      if ((var2 = var1.readInt()) >= 0) {
         this.deserializeShift = 8;
         this.deserializeFromDisk(var1, var2);
         this.deserializeShift = 0;
      } else if ((var2 = -var2) > 1024) {
         var2 = var1.readInt();
         this.deserializeFromDisk(var1, var2);
      } else {
         if (var2 < 2) {
            this.deserializeShift = 8;
         }

         var2 = var1.readInt();
         if (this.backing.isEmpty()) {
            if (var2 < 0 || var2 > 100000000) {
               throw new IOException("KEYSIZE INVALID: Control element keySize negative or too big (more than 100 million): " + var2);
            }

            this.backing = new Long2ObjectOpenHashMap(var2);
         }

         for(int var3 = 0; var3 < var2; ++var3) {
            short var4 = (short)(var1.readShort() + this.deserializeShift);
            short var5 = (short)(var1.readShort() + this.deserializeShift);
            short var6 = (short)(var1.readShort() + this.deserializeShift);
            long var8 = ElementCollection.getIndex(var4, var5, var6);
            int var19 = var1.readInt();

            for(int var20 = 0; var20 < var19; ++var20) {
               var6 = var1.readShort();
               int var7 = var1.readInt();
               boolean var10 = var1.readBoolean();
               boolean var11 = var1.readBoolean();
               boolean var12 = var1.readBoolean();
               short var13 = var1.readShort();
               short var14 = var1.readShort();
               short var15 = var1.readShort();
               if (var7 < 0 || var7 > 100000000) {
                  throw new IOException("Control element size negative or too big (more than 100 million): " + var7);
               }

               short[] var16 = new short[var7 * 3];
               int var17 = 0;

               for(int var18 = 0; var18 < var7; ++var18) {
                  var16[var17] = (short)((short)((var10 ? var1.readShort() : (short)var1.readByte()) + var13) + this.deserializeShift);
                  var16[var17 + 1] = (short)((short)((var11 ? var1.readShort() : (short)var1.readByte()) + var14) + this.deserializeShift);
                  var16[var17 + 2] = (short)((short)((var12 ? var1.readShort() : (short)var1.readByte()) + var15) + this.deserializeShift);
                  var17 += 3;
               }

               this.putAll(var8, var16, var6);
            }
         }

         this.deserializeShift = 0;
      }
   }

   public void deserializeFromDisk(DataInput var1, int var2) throws IOException {
      if (this.backing.isEmpty()) {
         this.backing = new Long2ObjectOpenHashMap(var2);
      }

      for(int var3 = 0; var3 < var2; ++var3) {
         short var4 = (short)(var1.readShort() + this.deserializeShift);
         short var5 = (short)(var1.readShort() + this.deserializeShift);
         short var6 = (short)(var1.readShort() + this.deserializeShift);
         long var7 = ElementCollection.getIndex(var4, var5, var6);
         int var13 = var1.readInt();

         for(int var14 = 0; var14 < var13; ++var14) {
            var6 = var1.readShort();
            int var9;
            if ((var9 = var1.readInt()) < 0 || var9 > 100000000) {
               throw new IOException("Control element size negative or too big (more than 100 million): " + var9);
            }

            short[] var10 = new short[var9 * 3];
            int var11 = 0;

            for(int var12 = 0; var12 < var9; ++var12) {
               var10[var11] = (short)(var1.readShort() + this.deserializeShift);
               var10[var11 + 1] = (short)(var1.readShort() + this.deserializeShift);
               var10[var11 + 2] = (short)(var1.readShort() + this.deserializeShift);
               var11 += 3;
            }

            this.putAll(var7, var10, var6);
         }
      }

      this.deserializeShift = 0;
   }
}
