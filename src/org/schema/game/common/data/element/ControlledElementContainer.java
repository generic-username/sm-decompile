package org.schema.game.common.data.element;

public class ControlledElementContainer {
   public long from;
   public long to;
   public short controlledType;
   public boolean add = false;
   public boolean send;

   public ControlledElementContainer(long var1, long var3, short var5, boolean var6, boolean var7) {
      this.from = var1;
      this.to = var3;
      this.controlledType = var5;
      this.add = var6;
      this.send = var7;
   }

   public int hashCode() {
      return (int)(this.from + this.to * 10000000000L);
   }

   public boolean equals(Object var1) {
      return ((ControlledElementContainer)var1).add == this.add && ((ControlledElementContainer)var1).from == this.from && ((ControlledElementContainer)var1).to == this.to && ((ControlledElementContainer)var1).controlledType == this.controlledType;
   }

   public String toString() {
      return "ControlledElementContainer[" + this.from + " -> " + this.to + "; type: " + this.controlledType + "; ADD: " + this.add + "]";
   }
}
