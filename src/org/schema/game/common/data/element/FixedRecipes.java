package org.schema.game.common.data.element;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;

public class FixedRecipes {
   public final ObjectArrayList recipes = new ObjectArrayList();

   public void appendDoc(org.w3c.dom.Element var1, Document var2) throws DOMException, CannotAppendXMLException {
      Iterator var3 = this.recipes.iterator();

      while(var3.hasNext()) {
         FixedRecipe var4 = (FixedRecipe)var3.next();
         var1.appendChild(var4.getNode(var2));
      }

   }
}
