package org.schema.game.common.data.element;

public class ElementParserException extends RuntimeException {
   private static final long serialVersionUID = 1L;

   public ElementParserException(String var1) {
      super(var1);
   }
}
