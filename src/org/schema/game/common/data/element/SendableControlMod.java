package org.schema.game.common.data.element;

public class SendableControlMod {
   public long from;
   public long to;
   public short controlledType;
   public boolean add;

   public SendableControlMod() {
   }

   public SendableControlMod(long var1, long var3, short var5, boolean var6) {
      this.from = var1;
      this.to = var3;
      this.controlledType = var5;
      this.add = var6;
   }
}
