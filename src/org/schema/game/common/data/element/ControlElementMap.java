package org.schema.game.common.data.element;

import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import java.io.ByteArrayInputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.AbstractBuildControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.PositionControl;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.io.SegmentSerializationBuffers;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.game.network.objects.remote.RemoteControlMod;
import org.schema.game.network.objects.remote.RemoteControlModBuffer;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.DataOutputStreamPositional;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class ControlElementMap implements TagSerializable {
   public static final boolean USE_SMART_COMPRESSION = true;
   public static final int SERIALIZATION_VERSION = 2;
   public static final int blockSize = 14;
   private final Long2ObjectOpenHashMap positionControlMapChache = new Long2ObjectOpenHashMap();
   private final Long2ObjectOpenHashMap positionControlMapChacheDirect = new Long2ObjectOpenHashMap();
   private final ArrayList failedDelayedUpdates = new ArrayList();
   private final LongOpenHashSet failedControllerBlocks = new LongOpenHashSet();
   private final List delayedNTUpdates = new ObjectArrayList();
   private ControlElementMapper delayedNTUpdatesMap = new ControlElementMapper();
   private final LongOpenHashSet loopMap = new LongOpenHashSet();
   private final Vector3i p0 = new Vector3i();
   private final Vector3i po0 = new Vector3i();
   private final Vector3i p1 = new Vector3i();
   private final Vector3i po1 = new Vector3i();
   private final SegmentPiece tmpPiece = new SegmentPiece();
   SegmentPiece pTmp = new SegmentPiece();
   SegmentPiece pointUnsaveTmp = new SegmentPiece();
   private ControlElementMapper controllingMap = new ControlElementMapper();
   private SendableSegmentController sendableSegmentController;
   private ControlledElementContainer addedDouble;
   private boolean needsControllerUpdates;
   private LongIterator controllingOldMapCheck;
   private AbstractBuildControlManager currentBuildController;
   private boolean structureCompleteChangeFromNT;
   private boolean receivedNT;
   private boolean initialStructureReceived;
   private boolean flagRequested;
   private long lastFailed;
   private boolean loadedFromChunk16;
   public boolean receivedInitialClient;

   public static void deserialize(DataInput var0, ControlElementMapper var1) throws IOException {
      var1.deserialize(var0);
   }

   public static ControlElementMapper mapFromTag(Tag var0, ControlElementMapper var1, boolean var2) {
      byte var3 = 0;
      if (var2) {
         var3 = 8;
      }

      Tag[] var4;
      Vector3i var5;
      int var6;
      Tag[] var13;
      int var14;
      if ("cs0".equals(var0.getName())) {
         System.err.println("[ControlElementMap] WARNING: OLD TAG NAME (cs0): " + var0.getName());
         var13 = (Tag[])var0.getValue();

         for(var14 = 0; var14 < var13.length - 1; ++var14) {
            (var5 = (Vector3i)(var4 = (Tag[])var13[var14].getValue())[0].getValue()).add(var3, var3, var3);
            byte[] var15;
            var6 = (var15 = (byte[])var4[1].getValue()).length / 14;
            DataInputStream var17 = new DataInputStream(new ByteArrayInputStream(var15));

            try {
               for(int var18 = 0; var18 < var6; ++var18) {
                  int var16 = var17.readInt() + var3;
                  int var9 = var17.readInt() + var3;
                  int var10 = var17.readInt() + var3;
                  short var11;
                  if (ElementKeyMap.exists(var11 = var17.readShort())) {
                     var1.put(var5, ElementCollection.getIndex(var16, var9, var10), var11);
                  }
               }
            } catch (IOException var12) {
               var12.printStackTrace();
            }
         }

         return var1;
      } else if ("cs1".equals(var0.getName())) {
         return (ControlElementMapper)var0.getValue();
      } else {
         System.err.println("[ControlElementMap] WARNING: OLD TAG NAME: " + var0.getName());
         var13 = (Tag[])var0.getValue();

         for(var14 = 0; var14 < var13.length && var13[var14].getType() != Tag.Type.FINISH; ++var14) {
            (var5 = (Vector3i)(var4 = (Tag[])var13[var14].getValue())[0].getValue()).add(var3, var3, var3);
            var4 = (Tag[])var4[1].getValue();

            for(var6 = 0; var6 < var4.length && var4[var6].getType() != Tag.Type.FINISH; ++var6) {
               Tag[] var7;
               if (ElementKeyMap.exists((Short)(var7 = (Tag[])var4[var6].getValue())[1].getValue())) {
                  Vector3i var8;
                  (var8 = (Vector3i)var7[0].getValue()).add(var3, var3, var3);
                  var1.put(var5, ElementCollection.getIndex(var8), (Short)var7[1].getValue());
               }
            }
         }

         return var1;
      }
   }

   public static Tag mapToTag(ControlElementMapper var0) {
      return new Tag(Tag.Type.SERIALIZABLE, "cs1", var0);
   }

   public static void serializeOptimized(DataOutputStreamPositional var0, ControlElementMapper var1, ControlElementMapOptimizer var2) throws IOException {
      var0.writeInt(-2);
      var0.writeInt(var1.size());
      Iterator var3 = var1.entrySet().iterator();

      while(var3.hasNext()) {
         Entry var4;
         ElementCollection.writeIndexAsShortPos((Long)(var4 = (Entry)var3.next()).getKey(), var0);
         int var5 = ((Short2ObjectOpenHashMap)var4.getValue()).size();
         var0.writeInt(var5);
         Iterator var7 = ((Short2ObjectOpenHashMap)var4.getValue()).entrySet().iterator();

         while(var7.hasNext()) {
            Entry var8 = (Entry)var7.next();
            var0.writeShort((Short)var8.getKey());
            int var6 = ((FastCopyLongOpenHashSet)var8.getValue()).size();
            var0.writeInt(var6);
            var2.serialize(var1, var8, var6, var0);
         }
      }

      assert !ControlElementMapOptimizer.CHECK_SANITY || var2.checkSanity(var0, var1);

   }

   public static void serializeForDisk(DataOutput var0, ControlElementMapper var1) throws IOException {
      var0.writeInt(-1026);
      var0.writeInt(var1.size());
      Iterator var4 = var1.entrySet().iterator();

      while(var4.hasNext()) {
         Entry var2;
         ElementCollection.writeIndexAsShortPos((Long)(var2 = (Entry)var4.next()).getKey(), var0);
         int var3 = ((Short2ObjectOpenHashMap)var2.getValue()).size();
         var0.writeInt(var3);
         Iterator var5 = ((Short2ObjectOpenHashMap)var2.getValue()).entrySet().iterator();

         while(var5.hasNext()) {
            Entry var6 = (Entry)var5.next();
            var0.writeShort((Short)var6.getKey());
            var0.writeInt(((FastCopyLongOpenHashSet)var6.getValue()).size());
            Iterator var7 = ((FastCopyLongOpenHashSet)var6.getValue()).iterator();

            while(var7.hasNext()) {
               ElementCollection.writeIndexAsShortPos((Long)var7.next(), var0);
            }
         }
      }

   }

   private boolean addControl(ControlledElementContainer var1) {
      return this.addControl(var1.from, var1.to, var1.controlledType, var1.send);
   }

   private boolean addControl(long var1, long var3, short var5, boolean var6) {
      if (var1 == var3) {
         System.err.println("WARNING: tried to add controlled element that is equal with the controlling");
         if (!this.sendableSegmentController.isOnServer()) {
            ((GameClientState)this.sendableSegmentController.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_CONTROLELEMENTMAP_0, 0.0F);
         }

         return false;
      } else if (var5 < 0) {
         throw new IllegalArgumentException("tried to send illegal controller: " + var5);
      } else {
         ElementCollection.getIndex4(var3, var5);
         boolean var7 = this.getControllingMap().put(var1, var3, var5);
         if (var6 && this.sendableSegmentController != null) {
            this.send(var1, var3, var5, true);
         }

         this.clearCache(var5);
         if (var7 && this.needsControllerUpdates) {
            assert var5 != 1;

            this.onAddedConnectionForManagerContainer(var1, var3, var5, false);
         }

         if (!this.sendableSegmentController.isOnServer() && ElementKeyMap.getInfo(var5).drawConnection() && ((GameClientState)this.sendableSegmentController.getState()).getWorldDrawer() != null) {
            ((GameClientState)this.sendableSegmentController.getState()).getWorldDrawer().getConnectionDrawerManager().onConnectionChanged(this.sendableSegmentController);
         }

         return var7;
      }
   }

   private void onRemovedConnectionForManagerContainer(long var1, long var3, short var5, boolean var6) {
      if (var5 != 32767) {
         assert this.getSegmentController().getState().isSynched() : "State should be synched here. Else running conditions might occur";

         ManagedSegmentController var7 = (ManagedSegmentController)this.sendableSegmentController;
         SegmentPiece var8;
         if ((var8 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var1, this.tmpPiece)) != null) {
            if (var8.getType() != 0) {
               var7.getManagerContainer().onConnectionRemoved(ElementCollection.getPosFromIndex(var1, this.p0), var8.getType(), ElementCollection.getPosFromIndex(var3, this.po0), var5);
               return;
            }

            System.err.println("[ERROR] Exception: not executing onRemoveConnection for 0 type " + var8 + " (can happen if controller gets shot down with the controlled)");
            return;
         }

         this.failedDelayedUpdates.add(new ControlledElementContainer(var1, var3, var5, true, var6));
      }

   }

   private void onAddedConnectionForManagerContainer(long var1, long var3, short var5, boolean var6) {
      if (var5 != 32767) {
         assert this.getSegmentController().getState().isSynched() : "State should be synched here. Else running conditions might occur";

         ManagedSegmentController var7 = (ManagedSegmentController)this.sendableSegmentController;
         SegmentPiece var8;
         if ((var8 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var1, this.tmpPiece)) != null) {
            if (var8.getType() != 0) {
               var7.getManagerContainer().onConnectionAdded(ElementCollection.getPosFromIndex(var1, this.p1), var8.getType(), ElementCollection.getPosFromIndex(var3, this.po1), var5);
               return;
            }

            System.err.println("[ERROR] Exception: removed executing onAddConnection for 0 type " + var8 + " (can happen if controller gets shot down with the controlled)");
            return;
         }

         this.failedDelayedUpdates.add(new ControlledElementContainer(var1, var3, var5, true, var6));
      }

   }

   private void addControlChain(long var1, short var3, PositionControl var4, boolean var5) {
      Short2ObjectOpenHashMap var6;
      if ((var6 = this.getControllingMap().get(var1)) != null) {
         LongOpenHashSet var9;
         if (var3 == 32767) {
            var9 = (LongOpenHashSet)this.getControllingMap().getAll().get(var1);
            var4.addControlled(var9);
            if (var5 && var9 != null) {
               Iterator var10 = var9.iterator();

               while(var10.hasNext()) {
                  long var7 = (Long)var10.next();
                  if (!this.loopMap.contains(var7)) {
                     this.loopMap.add(var7);
                     this.addControlChain(ElementCollection.getPosIndexFrom4(var7), var3, var4, var5);
                  }
               }
            }

            return;
         }

         if (var6.containsKey(var3)) {
            var9 = (LongOpenHashSet)this.getControllingMap().get(var1).get(var3);
            var4.addControlled(var9);
         }
      }

   }

   public void addControllerForElement(long var1, long var3, short var5) {
      SegmentPiece var6;
      if ((var6 = this.sendableSegmentController.getSegmentBuffer().getPointUnsave(var1, this.pointUnsaveTmp)) != null && var6.getType() != 0) {
         if ((var6 = this.sendableSegmentController.getSegmentBuffer().getPointUnsave(var3, this.pointUnsaveTmp)) != null && var6.getType() != 0) {
            this.addControl(var1, var3, var5, true);
            if (this.currentBuildController != null) {
               this.currentBuildController.notifyElementChanged();
            }

         } else {
            System.err.println("[" + this.getSegmentController().getState() + "][ERROR] " + this.getSegmentController() + " add controller failed: " + var6);
         }
      } else {
         System.err.println("[" + this.getSegmentController().getState() + "][ERROR] " + this.getSegmentController() + " add controller failed: " + var6);
      }
   }

   public void addControllerForElement(Vector3i var1, Vector3i var2, short var3) throws IOException {
      this.addControllerForElement(ElementCollection.getIndex(var1), ElementCollection.getIndex(var2), var3);
   }

   public void addDelayed(ControlledElementContainer var1) {
      synchronized(this.delayedNTUpdates) {
         assert this.checkContainer(var1) : var1;

         this.delayedNTUpdates.add(var1);
      }
   }

   private boolean checkContainer(ControlledElementContainer var1) {
      if (this.delayedNTUpdates.contains(var1)) {
         try {
            throw new Exception(var1.toString());
         } catch (Exception var2) {
            var2.printStackTrace();
         }
      }

      return true;
   }

   private void cache(Vector3i var1, short var2, PositionControl var3, boolean var4) {
      long var5 = ElementCollection.getIndex(var1);
      Long2ObjectOpenHashMap var7;
      if (var4) {
         var7 = this.positionControlMapChache;
      } else {
         var7 = this.positionControlMapChacheDirect;
      }

      Short2ObjectOpenHashMap var8;
      if ((var8 = (Short2ObjectOpenHashMap)var7.get(var5)) == null) {
         var8 = new Short2ObjectOpenHashMap();
         var7.put(var5, var8);
      }

      var8.put(var3.getType(), var3);
   }

   private void clearCache(short var1) {
      Iterator var2 = this.positionControlMapChache.entrySet().iterator();

      Entry var3;
      while(var2.hasNext()) {
         if (((Short2ObjectOpenHashMap)(var3 = (Entry)var2.next()).getValue()).containsKey(var1)) {
            ((PositionControl)((Short2ObjectOpenHashMap)var3.getValue()).get(var1)).clear();
         }
      }

      this.positionControlMapChache.clear();
      var2 = this.positionControlMapChacheDirect.entrySet().iterator();

      while(var2.hasNext()) {
         if (((Short2ObjectOpenHashMap)(var3 = (Entry)var2.next()).getValue()).containsKey(var1)) {
            ((PositionControl)((Short2ObjectOpenHashMap)var3.getValue()).get(var1)).clear();
         }
      }

      this.positionControlMapChacheDirect.clear();
   }

   public void deserializeNT(byte[] var1, int var2, int var3) throws IOException {
      FastByteArrayInputStream var4 = new FastByteArrayInputStream(var1, var2, var3);
      DataInputStream var5 = new DataInputStream(var4);
      this.deserializeNT(var5);
   }

   public void deserializeNT(DataInputStream var1) throws IOException {
      this.receivedNT = true;
      long var2 = System.currentTimeMillis();
      if (var1.readInt() > 0) {
         assert false;

      } else {
         int var4 = var1.readInt();

         assert !this.getSegmentController().isOnServer();

         synchronized(this.delayedNTUpdatesMap) {
            int var6 = 0;

            while(true) {
               if (var6 >= var4) {
                  break;
               }

               short var7 = var1.readShort();
               short var8 = var1.readShort();
               short var9 = var1.readShort();
               long var10 = ElementCollection.getIndex(var7, var8, var9);
               int var14 = var1.readInt();

               for(int var15 = 0; var15 < var14; ++var15) {
                  var9 = var1.readShort();
                  int var12 = var1.readInt();
                  ((GameStateInterface)this.getSegmentController().getState()).getControlOptimizer().deserialize(var9, var10, var12, this.delayedNTUpdatesMap, var1);
                  this.structureCompleteChangeFromNT = true;
               }

               ++var6;
            }
         }

         long var5;
         if ((var5 = System.currentTimeMillis() - var2) > 20L) {
            System.err.println("RECEIVED " + var4 + " CONTROL ELEMENT MAP ENTRIES FOR " + this.getSegmentController() + " TOOK " + var5);
         }

      }
   }

   public void deserializeZipped(DataInputStream var1) throws IOException {
      int var2 = var1.readInt();
      int var3 = var1.readInt();
      int var4 = var1.readInt();
      SegmentSerializationBuffers var5 = SegmentSerializationBuffers.get();

      try {
         var5.ensureSegmentBufferSize(var3);
         Inflater var6 = var5.inflater;
         byte[] var7 = var5.SEGMENT_BUFFER;
         byte[] var8 = var5.getStaticArray(var3);

         assert var4 <= var7.length : var4 + "/" + var7.length;

         int var15 = var1.read(var7, 0, var4);

         assert var15 == var4 : var15 + "/" + var4;

         if (var4 == 0) {
            System.err.println("[CONTROLSTRUCTURE] WARNING: controlstructure deserializing with 0 data " + this.getSegmentController());
         }

         var6.reset();
         var6.setInput(var7, 0, var4);

         try {
            var15 = var6.inflate(var8, 0, var3);

            assert var15 == var3 : var15 + " / " + var3 + "; deflated " + var4 + "; " + var2 + "; " + this.sendableSegmentController;

            this.deserializeNT(var8, 0, var3);
            if (var15 == 0) {
               System.err.println("[CONTROLSTRUCTURE] WARNING: INFLATED BYTES 0: " + var6.needsInput() + " " + var6.needsDictionary());
            }
         } catch (DataFormatException var12) {
            System.err.println("Exception in " + this.getSegmentController());
            var12.printStackTrace();
         } catch (IOException var13) {
            System.err.println("Exception in " + this.getSegmentController() + "; DEBUG-ID: " + var2);
            throw var13;
         }
      } finally {
         SegmentSerializationBuffers.free(var5);
      }

   }

   public void removeFromRemovedBlock(long var1) {
      long var3 = ElementCollection.getPosIndexFrom4(var1);
      this.removeControlledFromAllControllers(var3, (short)ElementCollection.getType(var1), true, false);
      Short2ObjectOpenHashMap var10000 = this.getControllingMap().remove(var3);
      Map var5 = null;
      if (var10000 != null) {
         var5 = (Map)this.positionControlMapChache.remove(var3);
         this.positionControlMapChacheDirect.remove(var3);
         System.err.println("[ControlElementMap] RemoveControlBlock: REMOVED CACHE: " + var3 + " ---> " + var5 + "; " + this.positionControlMapChache);
      }

      if (this.currentBuildController != null) {
         this.currentBuildController.notifyElementChanged();
      }

   }

   public void fromTagStructure(Tag var1) {
      if ("cs1".equals(var1.getName())) {
         this.controllingMap = (ControlElementMapper)var1.getValue();
      } else if ("cs0".equals(var1.getName())) {
         mapFromTag(var1, this.controllingMap, true);
      } else {
         mapFromTag(var1, this.controllingMap, true);
         this.controllingOldMapCheck = this.controllingMap.keySet().iterator();
         System.err.println("[SERVER][CONTROL-ELEMENT-MAP][TAG][OLD] ADDED CONTROLLER FROM TAG. MAP NOW: " + this.getSegmentController() + ". CONTROLLER MAP SIZE: " + this.delayedNTUpdates.size());
      }
   }

   public Tag toTagStructure() {
      return mapToTag(this.controllingMap);
   }

   public LongOpenHashSet getAllControlledElements(short var1) {
      LongOpenHashSet var2 = new LongOpenHashSet();
      Iterator var3 = this.getControllingMap().keySet().iterator();

      while(true) {
         long var4;
         LongOpenHashSet var8;
         do {
            if (!var3.hasNext()) {
               return var2;
            }

            var4 = (Long)var3.next();
         } while((var8 = (LongOpenHashSet)this.getControllingMap().get(var4).get(var1)) == null);

         Iterator var9 = var8.iterator();

         while(var9.hasNext()) {
            long var6;
            if (ElementCollection.getType(var6 = (Long)var9.next()) == var1) {
               var2.add(var6);
            }
         }
      }
   }

   private PositionControl getChached(Vector3i var1, short var2, boolean var3) {
      return this.getChached(ElementCollection.getIndex(var1), var2, var3);
   }

   private PositionControl getChached(long var1, short var3, boolean var4) {
      Short2ObjectOpenHashMap var5;
      if (var4) {
         var5 = (Short2ObjectOpenHashMap)this.positionControlMapChache.get(var1);
      } else {
         var5 = (Short2ObjectOpenHashMap)this.positionControlMapChacheDirect.get(var1);
      }

      return (PositionControl)var5.get(var3);
   }

   public PositionControl getDirectControlledElements(short var1, Vector3i var2) {
      try {
         if (this.isCached(var2, var1, false)) {
            return this.getChached(var2, var1, false);
         }
      } catch (NullPointerException var4) {
         var4.printStackTrace();
         System.err.println("Exception successfully catched. rebuilding cache");
      }

      PositionControl var3;
      (var3 = new PositionControl()).setType(var1);
      this.loopMap.clear();
      this.addControlChain(ElementCollection.getIndex(var2), var1, var3, false);
      this.cache(var2, var1, var3, false);
      return var3;
   }

   public PositionControl getControlledElements(short var1, Vector3i var2) {
      try {
         if (this.isCached(var2, var1, true)) {
            return this.getChached(var2, var1, true);
         }
      } catch (NullPointerException var4) {
         var4.printStackTrace();
         System.err.println("Exception successfully catched. rebuilding cache");
      }

      PositionControl var3;
      (var3 = new PositionControl()).setType(var1);
      this.loopMap.clear();
      this.addControlChain(ElementCollection.getIndex(var2), var1, var3, true);
      this.cache(var2, var1, var3, true);
      return var3;
   }

   public ControlElementMapper getControllingMap() {
      return this.controllingMap;
   }

   public SendableSegmentController getSegmentController() {
      return this.sendableSegmentController;
   }

   public void handleReceived() {
      if (this.getSegmentController().isOnServer() || this.initialStructureReceived || this.flagRequested) {
         RemoteControlModBuffer var1 = this.sendableSegmentController.getNetworkObject().controlledByBuffer;

         for(int var2 = 0; var2 < var1.getReceiveBuffer().size(); ++var2) {
            SendableControlMod var3 = (SendableControlMod)((RemoteControlMod)var1.getReceiveBuffer().get(var2)).get();
            ControlledElementContainer var4 = new ControlledElementContainer(var3.from, var3.to, var3.controlledType, var3.add, this.sendableSegmentController.isOnServer());
            this.addDelayed(var4);
         }

         var1.getReceiveBuffer().clear();
      }
   }

   private boolean isCached(Vector3i var1, short var2, boolean var3) {
      return this.isCached(ElementCollection.getIndex(var1), var2, var3);
   }

   private boolean isCached(long var1, short var3, boolean var4) {
      Short2ObjectOpenHashMap var5;
      if (var4) {
         var5 = (Short2ObjectOpenHashMap)this.positionControlMapChache.get(var1);
      } else {
         var5 = (Short2ObjectOpenHashMap)this.positionControlMapChacheDirect.get(var1);
      }

      return var5 != null && var5.containsKey(var3);
   }

   public boolean isControlling(long var1, long var3, short var5) {
      if (this.getControllingMap().containsKey(var1)) {
         long var6 = ElementCollection.getIndex4(var3, var5);
         return this.getControllingMap().get(var1).containsKey(var5) && ((FastCopyLongOpenHashSet)this.getControllingMap().get(var1).get(var5)).contains(var6);
      } else {
         return false;
      }
   }

   public boolean isControlling(Vector3i var1, Vector3i var2, short var3) {
      return this.isControlling(ElementCollection.getIndex(var1), var2, var3);
   }

   public boolean isControlling(long var1, Vector3i var3, short var4) {
      if (this.getControllingMap().containsKey(var1)) {
         long var5 = ElementCollection.getIndex4(ElementCollection.getIndex(var3), var4);
         return this.getControllingMap().get(var1).containsKey(var4) && ((FastCopyLongOpenHashSet)this.getControllingMap().get(var1).get(var4)).contains(var5);
      } else {
         return false;
      }
   }

   public boolean isControlling(Vector3i var1, Vector3i var2, ShortArrayList var3) {
      return this.isControlling(ElementCollection.getIndex(var1), var2, var3);
   }

   public boolean isControlling(long var1, Vector3i var3, ShortArrayList var4) {
      if (this.getControllingMap().containsKey(var1)) {
         Iterator var8 = var4.iterator();

         while(var8.hasNext()) {
            short var5 = (Short)var8.next();
            long var6 = ElementCollection.getIndex4(ElementCollection.getIndex(var3), var5);
            if (this.getControllingMap().get(var1).containsKey(var5) && ((FastCopyLongOpenHashSet)this.getControllingMap().get(var1).get(var5)).contains(var6)) {
               return true;
            }
         }
      }

      return false;
   }

   public boolean isFlagRequested() {
      return this.flagRequested;
   }

   public void setFlagRequested(boolean var1) {
      this.flagRequested = var1;
   }

   public void onRemoveElement(long var1, short var3) {
      this.removeFromRemovedBlock(ElementCollection.getIndex4(var1, var3));
      if (!this.sendableSegmentController.isOnServer() && ElementKeyMap.isValidType(var3) && ElementKeyMap.getInfo(var3).drawConnection() && ((GameClientState)this.sendableSegmentController.getState()).getWorldDrawer() != null) {
         ((GameClientState)this.sendableSegmentController.getState()).getWorldDrawer().getConnectionDrawerManager().onConnectionChanged(this.sendableSegmentController);
      }

   }

   private boolean removeControlled(long var1, long var3, short var5, boolean var6) {
      boolean var7 = false;
      if (this.getControllingMap().containsKey(var1) && this.getControllingMap().get(var1).containsKey(var5) && (var7 = this.getControllingMap().remove(var1, var3, var5))) {
         if (!this.sendableSegmentController.isOnServer() && ElementKeyMap.getInfo(var5).drawConnection() && ((GameClientState)this.sendableSegmentController.getState()).getWorldDrawer() != null) {
            ((GameClientState)this.sendableSegmentController.getState()).getWorldDrawer().getConnectionDrawerManager().onConnectionChanged(this.sendableSegmentController);
         }

         if (this.needsControllerUpdates) {
            assert var5 != 1;

            this.onRemovedConnectionForManagerContainer(var1, var3, var5, var6);
         }
      }

      if (var6 && var7 && this.sendableSegmentController != null) {
         this.send(var1, var3, var5, false);
      }

      if (var7) {
         this.clearCache(var5);
      }

      if (var5 != 32767) {
         this.removeControlled(var1, var3, (short)32767, false);
      }

      return var7;
   }

   public void removeControlledFromAll(long var1, short var3, boolean var4) {
      this.removeControlledFromAllControllers(var1, var3, false, var4);
      if (this.currentBuildController != null) {
         this.currentBuildController.notifyElementChanged();
      }

   }

   private boolean removeControlledFromAllControllers(long var1, short var3, boolean var4, boolean var5) {
      if (!var4 && ElementInformation.allowsMultiConnect(var3)) {
         return false;
      } else {
         var4 = false;
         if (ElementKeyMap.isValidType(var3)) {
            ElementInformation var6;
            boolean var7 = (var6 = ElementKeyMap.getInfoFast(var3)).isRestrictedMultiControlled();
            Iterator var8 = this.getControllingMap().keySet().iterator();

            while(true) {
               SegmentPiece var9;
               long var10;
               ShortArrayList var12;
               do {
                  label42:
                  do {
                     while(var8.hasNext()) {
                        var10 = (Long)var8.next();
                        if (var7) {
                           var9 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var10);
                           var12 = var6.getRestrictedMultiControlled();
                           continue label42;
                        }

                        var4 |= this.removeControlled(var10, var1, var3, var5 || this.getSegmentController().isOnServer());
                     }

                     return var4;
                  } while(var9 == null);
               } while(!var12.contains(var9.getType()));

               var4 |= this.removeControlled(var10, var1, var3, var5 || this.getSegmentController().isOnServer());
            }
         } else {
            return var4;
         }
      }
   }

   public void removeControllerForElement(long var1, long var3, short var5) {
      this.removeControlled(var1, var3, var5, true);
      if (this.currentBuildController != null) {
         this.currentBuildController.notifyElementChanged();
      }

   }

   private void send(long var1, long var3, short var5, boolean var6) {
      synchronized(this.sendableSegmentController.getState()) {
         boolean var8;
         if (var8 = !this.sendableSegmentController.getState().isSynched()) {
            try {
               throw new Exception("Debug Warning: synch");
            } catch (Exception var9) {
               var9.printStackTrace();
               this.sendableSegmentController.getState().setSynched();
            }
         }

         if (var5 < 0) {
            throw new IllegalArgumentException("tried to send illegal controller: " + var5);
         } else {
            RemoteControlMod var11 = new RemoteControlMod(new SendableControlMod(var1, var3, var5, var6), this.sendableSegmentController.isOnServer());

            assert !this.controlBufferContains(var11) : var11;

            this.sendableSegmentController.getNetworkObject().controlledByBuffer.add(var11);
            if (var8) {
               this.sendableSegmentController.getState().setUnsynched();
            }

         }
      }
   }

   private boolean controlBufferContains(RemoteControlMod var1) {
      Iterator var2 = this.sendableSegmentController.getNetworkObject().controlledByBuffer.iterator();

      do {
         if (!var2.hasNext()) {
            return false;
         }
      } while(!((SendableControlMod)((RemoteControlMod)var2.next()).get()).equals(var1.get()));

      return true;
   }

   public long serializeForNT(FastByteArrayOutputStream var1) throws IOException {
      DataOutputStreamPositional var2 = new DataOutputStreamPositional(var1);
      long var3 = var1.position();
      serializeOptimized(var2, this.getControllingMap(), ((GameStateInterface)this.getSegmentController().getState()).getControlOptimizer());
      return var1.position() - var3;
   }

   public int serializeZippedForNT(DataOutputStream var1) throws IOException {
      var1.writeInt(this.getSegmentController().getId());
      SegmentSerializationBuffers var4 = SegmentSerializationBuffers.get();

      try {
         FastByteArrayOutputStream var5 = var4.getStaticArrayOutput();
         int var6 = (int)this.serializeForNT(var5);
         var4.ensureSegmentBufferSize(var6);
         byte[] var3 = var4.SEGMENT_BUFFER;
         Deflater var2 = var4.deflater;
         var1.writeInt(var6);
         var2.reset();
         var2.setInput(var5.array, 0, var6);
         var2.finish();
         int var9 = var2.deflate(var3);
         var1.writeInt(var9);
         var1.write(var3, 0, var9);
      } finally {
         SegmentSerializationBuffers.free(var4);
      }

      return 1;
   }

   public void setFromMap(ControlElementMapper var1) {
      this.controllingMap.setFromMap(var1);
   }

   public void setObs(AbstractBuildControlManager var1) {
      this.currentBuildController = var1;
   }

   public void setSendableSegmentController(SendableSegmentController var1) {
      this.needsControllerUpdates = var1 instanceof ManagedSegmentController;
      this.sendableSegmentController = var1;
   }

   public void switchControllerForElement(long var1, long var3, short var5) {
      if (this.isControlling(var1, var3, var5)) {
         this.removeControllerForElement(var1, var3, var5);
      } else {
         if (var1 != var3) {
            this.removeControlledFromAll(var3, var5, true);
         }

         this.addControllerForElement(var1, var3, var5);
      }

      if (this.currentBuildController != null) {
         this.currentBuildController.notifyElementChanged();
      }

   }

   public void updateLocal(Timer var1) {
      long var2 = System.currentTimeMillis();
      long var7;
      if (!this.failedControllerBlocks.isEmpty() && var2 - this.lastFailed > 600L) {
         Vector3i var4 = new Vector3i();
         LongIterator var5 = this.failedControllerBlocks.iterator();
         ManagedSegmentController var6 = (ManagedSegmentController)this.sendableSegmentController;

         while(true) {
            while(var5.hasNext()) {
               Vector3i var14 = ElementCollection.getPosFromIndex(var7 = var5.nextLong(), var4);
               SegmentPiece var9;
               short var10;
               if ((var9 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var14, this.tmpPiece)) != null && (var10 = var9.getType()) > 0) {
                  if (!ElementKeyMap.isValidType(var10)) {
                     System.err.println("Exception: not adding controller block. type unknown:  " + var10 + "; " + ElementCollection.getPosFromIndex(var7, new Vector3i()) + "; " + this.getSegmentController());
                     var5.remove();
                  } else {
                     var6.getManagerContainer().addControllerBlockWithAddingBlock(var10, var9.getAbsoluteIndex(), var9.getSegment(), false);
                     var5.remove();
                  }
               } else if (var9 != null) {
                  System.err.println("Exception: not adding controller block. not a type (0); " + ElementCollection.getPosFromIndex(var7, new Vector3i()) + "; " + this.getSegmentController());
                  var5.remove();
               }
            }

            this.lastFailed = var2;
            break;
         }
      }

      if (this.receivedNT) {
         this.receivedInitialClient = true;
         this.receivedNT = false;
      }

      if (this.structureCompleteChangeFromNT) {
         this.receivedInitialClient = true;
         synchronized(this.delayedNTUpdatesMap) {
            assert !this.sendableSegmentController.isOnServer();

            this.controllingMap.clear();
            this.controllingMap.setFromMap(this.delayedNTUpdatesMap);
            this.delayedNTUpdatesMap.clearAndTrim();
            this.structureCompleteChangeFromNT = false;
            if (!this.sendableSegmentController.isOnServer() && ((GameClientState)this.sendableSegmentController.getState()).getWorldDrawer() != null && ((GameClientState)this.sendableSegmentController.getState()).getWorldDrawer().getConnectionDrawerManager() != null) {
               ((GameClientState)this.sendableSegmentController.getState()).getWorldDrawer().getConnectionDrawerManager().onConnectionChanged(this.sendableSegmentController);
            }

            if (!(this.sendableSegmentController instanceof ManagedSegmentController)) {
               System.err.println("Exception: Cannot update controlstructure of " + this.sendableSegmentController + ": This object doesn't use connections! Please include this object in any bug report");
            } else {
               ManagedSegmentController var20 = (ManagedSegmentController)this.sendableSegmentController;
               Long2ObjectOpenHashMap var22 = this.controllingMap.getAll();
               new Vector3i();
               new Vector3i();
               Iterator var15 = var22.long2ObjectEntrySet().iterator();

               label133:
               while(true) {
                  while(true) {
                     if (!var15.hasNext()) {
                        break label133;
                     }

                     it.unimi.dsi.fastutil.longs.Long2ObjectMap.Entry var26 = (it.unimi.dsi.fastutil.longs.Long2ObjectMap.Entry)var15.next();
                     SegmentPiece var27 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var26.getLongKey(), this.tmpPiece);
                     this.controllingMap.getAll().get(var26.getLongKey());
                     short var17;
                     if (var27 != null && ElementKeyMap.isValidType(var17 = var27.getType())) {
                        var20.getManagerContainer().addControllerBlockWithAddingBlock(var17, var27.getAbsoluteIndex(), var27.getSegment(), false);
                     } else {
                        this.failedControllerBlocks.add(var26.getLongKey());
                     }
                  }
               }
            }

            this.initialStructureReceived = true;
         }
      }

      ControlledElementContainer var16;
      if (!this.delayedNTUpdates.isEmpty()) {
         int var18 = this.delayedNTUpdates.size();
         long var21 = System.currentTimeMillis();
         synchronized(this.delayedNTUpdates) {
            Iterator var8 = this.delayedNTUpdates.iterator();

            while(true) {
               if (!var8.hasNext()) {
                  this.delayedNTUpdates.clear();
                  break;
               }

               if ((var16 = (ControlledElementContainer)var8.next()).add) {
                  this.addControl(var16);
               } else {
                  this.removeControlled(var16.from, var16.to, var16.controlledType, var16.send);
               }
            }
         }

         if ((var7 = System.currentTimeMillis() - var21) > 5L) {
            System.err.println("[CONTROLELEMENTMAP][" + this.getSegmentController().getState() + "] NTUPDATE " + this.getSegmentController() + " took " + var7 + " for " + var18 + " elements");
         }
      }

      if (this.addedDouble != null) {
         System.err.println("[WARNING][CONTROLELEMENTMAP] DOUBLE CONTROL SENT " + this.sendableSegmentController.getState() + ": " + this.sendableSegmentController + "; " + this.addedDouble.from + "; " + this.addedDouble.to + "; ty " + this.addedDouble.controlledType + "; add " + this.addedDouble.add + "; send " + this.addedDouble.send);
         this.addedDouble = null;
      }

      if (!this.failedDelayedUpdates.isEmpty()) {
         System.currentTimeMillis();
         if (this.needsControllerUpdates) {
            int var23 = this.failedDelayedUpdates.size();
            SendableSegmentController var10000 = this.sendableSegmentController;

            for(int var25 = 0; var25 < var23; ++var25) {
               if ((var16 = (ControlledElementContainer)this.failedDelayedUpdates.get(var25)).add) {
                  this.onAddedConnectionForManagerContainer(var16.from, var16.to, var16.controlledType, var16.send);
               } else {
                  this.onRemovedConnectionForManagerContainer(var16.from, var16.to, var16.controlledType, var16.send);
               }
            }
         }

         this.failedDelayedUpdates.clear();
      }

      if (this.controllingOldMapCheck != null) {
         try {
            if (this.controllingOldMapCheck.hasNext()) {
               long var19 = (Long)this.controllingOldMapCheck.next();
               SegmentPiece var24;
               if ((var24 = this.sendableSegmentController.getSegmentBuffer().getPointUnsave(ElementCollection.getPosFromIndex(var19, new Vector3i()), this.pointUnsaveTmp)) == null) {
                  this.controllingOldMapCheck = this.getControllingMap().keySet().iterator();
                  return;
               }

               if (var24.getType() == 0) {
                  System.err.println("Exception: REMOVING DUE TO CONTROLLING MAP CHECK: " + var19);
                  this.controllingOldMapCheck.remove();
               }

               return;
            }

            this.controllingOldMapCheck = null;
            return;
         } catch (ConcurrentModificationException var11) {
            this.controllingOldMapCheck = this.getControllingMap().keySet().iterator();
         }
      }

   }

   public void clear() {
      this.controllingMap.clearAndTrim();
      this.positionControlMapChache.clear();
      this.positionControlMapChacheDirect.clear();
      this.positionControlMapChache.trim();
      this.positionControlMapChacheDirect.trim();
   }

   public void checkControllerOnServer(long var1) {
      SegmentPiece var3;
      if ((var3 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var1)) != null && var3.getType() == 0) {
         System.err.println("[SERVER] Client marked a controller as faulty -> confirmed on server -> removing controller");
         this.removeFromRemovedBlock(var1);
      } else {
         System.err.println("[SERVER] Client marked a controller as faulty -> exits on server -> not removing controller");
      }
   }

   public boolean isLoadedFromChunk16() {
      return this.loadedFromChunk16;
   }

   public void setLoadedFromChunk16(boolean var1) {
      this.loadedFromChunk16 = var1;
   }

   public void disconnectAllLightBlocks(long var1, long var3) {
      Short2ObjectOpenHashMap var5;
      if ((var5 = this.getControllingMap().get(var1)) != null) {
         Iterator var6 = ElementKeyMap.lightTypes.iterator();

         while(true) {
            short var7;
            FastCopyLongOpenHashSet var10;
            do {
               if (!var6.hasNext()) {
                  return;
               }

               var7 = (Short)var6.next();
            } while((var10 = (FastCopyLongOpenHashSet)var5.get(var7)) == null);

            Iterator var11 = (new LongArrayList(var10)).iterator();

            while(var11.hasNext()) {
               long var8;
               if ((var8 = (Long)var11.next()) != var3) {
                  this.getControllingMap().remove(var1, ElementCollection.getPosIndexFrom4(var8), (short)ElementCollection.getType(var8));
               }
            }
         }
      }
   }
}
