package org.schema.game.common.data.element;

public class CannotAppendXMLException extends Exception {
   private static final long serialVersionUID = 1L;

   public CannotAppendXMLException(String var1) {
      super(var1);
   }
}
