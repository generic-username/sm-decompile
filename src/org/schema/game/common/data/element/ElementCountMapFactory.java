package org.schema.game.common.data.element;

import java.io.DataInput;
import java.io.IOException;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.schine.resource.tag.SerializableTagFactory;

public class ElementCountMapFactory implements SerializableTagFactory {
   public Object create(DataInput var1) throws IOException {
      ElementCountMap var2;
      (var2 = new ElementCountMap()).deserialize(var1);
      return var2;
   }
}
