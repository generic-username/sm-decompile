package org.schema.game.common.data.creature;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.meta.weapon.Weapon;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.PlayerInventory;

public class AICompositeCreaturePlayer extends AIPlayer {
   int naturalDamage = 3;
   private long targetMoveCommand;
   private long moveTimeout;
   private Transform inv = new Transform();
   private Transform wt = new Transform();
   private Vector3f d = new Vector3f();
   private long vol_lastShot;
   private long reload = 200L;
   private Vector4f color = new Vector4f(0.5F, 0.0F, 0.0F, 1.0F);
   private float distance = 1.0F;
   private float speed = 100.0F;

   public AICompositeCreaturePlayer(AICreature var1) {
      super(var1);
   }

   protected boolean isFlying() {
      return !this.getCreature().getGravity().isGravityOn();
   }

   protected Inventory initInventory() {
      return new PlayerInventory(this, 0L);
   }

   public void resetTargetToMove() {
      assert this.isOnServer();

      this.setTarget((Transform)null);
      this.getNetworkObject().hasTarget.set(false);
   }

   public boolean isTargetReachTimeout() {
      return this.getTarget() != null && this.moveTimeout > 0L && System.currentTimeMillis() - this.targetMoveCommand > this.moveTimeout;
   }

   public boolean isTargetReachLocalTimeout() {
      return this.getTarget() != null && this.moveTimeout > 0L && System.currentTimeMillis() - this.lastTargetSet > 1000L;
   }

   public void setTargetToMove(Vector3f var1, long var2) {
      this.targetMoveCommand = System.currentTimeMillis();
      this.moveTimeout = var2;
      Transform var4;
      (var4 = new Transform()).setIdentity();
      var4.origin.set(var1);
      this.setTarget(var4);

      assert this.isOnServer();

      this.getNetworkObject().target.set(var4.origin);
      this.getNetworkObject().hasTarget.set(true);
   }

   public float getMaxHealth() {
      return 85.0F;
   }

   public boolean isAtTarget() {
      if (this.getTarget() == null) {
         return true;
      } else {
         if (this.getCreature().getAffinity() != null) {
            this.inv.set(this.getCreature().getAffinity().getWorldTransform());
            this.wt.set(this.getWorldTransform());
            this.inv.inverse();
            this.inv.mul(this.wt);
            this.wt.set(this.inv);
         } else {
            this.wt.setIdentity();
         }

         this.d.sub(this.getTarget().origin, this.wt.origin);
         return this.d.length() < 0.5F;
      }
   }

   protected void updateServer() {
      this.getTarget();
   }

   protected void updateClient() {
   }

   public boolean hasNaturalWeapon() {
      return true;
   }

   public void fireNaturalWeapon(AbstractCharacter var1, AbstractOwnerState var2, Vector3f var3) {
      var3.scale(this.speed);
      if (var2.isOnServer() || ((GameClientState)var2.getState()).getCurrentSectorId() == var1.getSectorId()) {
         long var4;
         if ((var4 = System.currentTimeMillis()) - this.vol_lastShot > this.reload) {
            var1.getParticleController().addProjectile(var1, new Vector3f(var1.getShoulderWorldTransform().origin), var3, (float)this.naturalDamage, this.distance, 0, 1.0F, 1, 1.0F, Long.MIN_VALUE, this.color);
            this.vol_lastShot = var4;
         }

         this.getCreature().lastAttack = System.currentTimeMillis();

         assert this.getCreature().isAttacking();

      }
   }

   public boolean isFactoryInUse() {
      return false;
   }

   public boolean isHarvestingButton() {
      return false;
   }

   protected void onNoSlotFree(short var1, int var2) {
   }

   public void onFiredWeapon(Weapon var1) {
      this.getCreature().lastAttack = System.currentTimeMillis();
   }

   public String getConversationScript() {
      if (this.conversationScript == null) {
         this.conversationScript = "creature-general.lua";
      }

      return this.conversationScript;
   }

   public void interactClient(AbstractOwnerState var1) {
      ((GameClientState)this.getState()).getPlayer().getPlayerConversationManager().clientInteracted(this);
   }

   public long getDbId() {
      return Long.MIN_VALUE;
   }
}
