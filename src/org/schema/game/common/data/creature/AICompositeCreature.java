package org.schema.game.common.data.creature;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.game.common.controller.ai.AIRandomCreatureConfiguration;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.network.objects.TargetableAICreatureNetworkObject;
import org.schema.game.server.ai.CreatureAIEntity;
import org.schema.game.server.ai.CreatureDefaultAIEntity;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.FileExt;

public abstract class AICompositeCreature extends AICreature {
   private final AIRandomCreatureConfiguration aiRandomCreatureConfiguration;
   private float characterMargin = 0.1F;
   private TargetableAICreatureNetworkObject networkPlayerCharacterObject;

   public AICompositeCreature(StateInterface var1) {
      super(var1);
      this.aiRandomCreatureConfiguration = new AIRandomCreatureConfiguration(var1, this);
   }

   public void cleanUpOnEntityDelete() {
      super.cleanUpOnEntityDelete();
      System.err.println("[DELETE] Cleaning up playerCharacter for " + this.getOwnerState() + " on " + this.getState());
   }

   public void destroyPersistent() {
      assert this.isOnServer();

      (new FileExt(GameServerState.ENTITY_DATABASE_PATH + this.getUniqueIdentifier() + ".ent")).delete();
   }

   public Transform getShoulderWorldTransform() {
      Transform var1 = new Transform(super.getWorldTransform());
      Vector3f var2;
      (var2 = GlUtil.getUpVector(new Vector3f(), var1)).scale(0.385F);
      var1.origin.add(var2);
      return var1;
   }

   protected float getCharacterMargin() {
      return this.characterMargin;
   }

   protected AICompositeCreaturePlayer instantiateOwnerState() {
      return new AICompositeCreaturePlayer(this);
   }

   public String toNiceString() {
      Faction var1 = ((FactionState)this.getState()).getFactionManager().getFaction(this.getFactionId());
      return super.toNiceString() + (var1 != null && var1.isShowInHub() ? "[" + var1.getName() + "]" : "") + (this.getGravity().isGravityOn() ? "(" + this.getGravity().source + ")" + this.getGravity().getAcceleration() + "[" + this.getMass() + "]" : "");
   }

   public CreatureAIEntity instantiateAIEntity() {
      return new CreatureDefaultAIEntity("CRE_AI", this);
   }

   public TargetableAICreatureNetworkObject getNetworkObject() {
      return this.networkPlayerCharacterObject;
   }

   public void updateLocal(Timer var1) {
      super.updateLocal(var1);
      ((AICompositeCreaturePlayer)this.getOwnerState()).updateLocal(var1);
   }

   public Transform getHeadWorldTransform() {
      Transform var1 = new Transform(super.getWorldTransform());
      Vector3f var2;
      (var2 = GlUtil.getUpVector(new Vector3f(), var1)).scale(0.485F);
      var1.origin.add(var2);
      return var1;
   }

   public String toString() {
      return "AIComCreature[(" + this.getUniqueIdentifier() + ")(" + this.getId() + ")]";
   }

   public AIRandomCreatureConfiguration getAiConfiguration() {
      return this.aiRandomCreatureConfiguration;
   }

   public void newNetworkObject() {
      this.ntInit(new TargetableAICreatureNetworkObject(this.getState(), this.getOwnerState()));
   }

   protected void ntInit(TargetableAICreatureNetworkObject var1) {
      this.networkPlayerCharacterObject = var1;
   }
}
