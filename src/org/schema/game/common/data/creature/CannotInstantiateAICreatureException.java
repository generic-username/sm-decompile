package org.schema.game.common.data.creature;

public class CannotInstantiateAICreatureException extends Exception {
   private static final long serialVersionUID = 1L;
}
