package org.schema.game.common.data.creature;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Quat4Util;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.Destroyable;
import org.schema.game.common.data.player.InteractionInterface;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.PersonalFactoryInventory;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.Universe;
import org.schema.game.network.objects.TargetableAICreatureNetworkObject;
import org.schema.game.server.ai.CreatureAIEntity;
import org.schema.game.server.ai.program.creature.character.AICreatureProgramInterface;
import org.schema.game.server.controller.SegmentPathCallback;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.MachineProgram;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.UniqueInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public abstract class AIPlayer extends AbstractOwnerState implements InteractionInterface, SegmentPathCallback, UniqueInterface {
   private static final int PROXIMITY_CHECKPERIOD = 5000;
   private static final float DEFAULT_HEALTH = 500.0F;
   private final AICreature creature;
   private final ObjectArrayFIFOQueue aiMessages = new ObjectArrayFIFOQueue();
   private final Transform orientation = new Transform();
   private final Transform orientationTo = new Transform();
   private final Quat4f targetQuat = new Quat4f();
   private final ObjectArrayList proximityEntities = new ObjectArrayList();
   private final Object2ObjectOpenHashMap conversationStates = new Object2ObjectOpenHashMap();
   protected Vector3f dir = new Vector3f();
   protected long lastTargetSet;
   private float health = this.getMaxHealth();
   private Transform target;
   private List currentPath;
   private float rotationSpeed = 10.0F;
   private long lastCheck;
   private long currentLag;
   private long lastLagReceived;

   public AIPlayer(AICreature var1) {
      this.creature = var1;
      this.orientation.setIdentity();
      this.orientationTo.setIdentity();
      this.setInventory(this.initInventory());
      this.setPersonalFactoryInventoryCapsule(new PersonalFactoryInventory(this, 1L, (short)213));
      this.setPersonalFactoryInventoryMicro(new PersonalFactoryInventory(this, 2L, (short)215));
      this.setPersonalFactoryInventoryMacroBlock(new PersonalFactoryInventory(this, 3L, (short)211));
   }

   protected abstract boolean isFlying();

   protected abstract Inventory initInventory();

   public void sendServerMessage(Object[] var1, int var2) {
   }

   public abstract void resetTargetToMove();

   public int getSectorId() {
      return this.getAbstractCharacterObject().getSectorId();
   }

   public abstract boolean isTargetReachTimeout();

   public byte getFactionRights() {
      return 0;
   }

   public abstract boolean isTargetReachLocalTimeout();

   public abstract void setTargetToMove(Vector3f var1, long var2);

   public void damage(float var1, Destroyable var2, Damager var3) {
      this.health -= var1;
      if (var1 > 0.0F) {
         var3.sendHitConfirm((byte)3);
      }

      if (this.health <= 0.0F) {
         this.dieOnServer(var3);
      }

   }

   public void heal(float var1, Destroyable var2, Damager var3) {
      this.health = Math.min(this.getMaxHealth(), this.health + var1);
   }

   public float getMaxHealth() {
      return 500.0F;
   }

   public float getHealth() {
      return this.health;
   }

   public Vector3f getRight(Vector3f var1) {
      return GlUtil.getRightVector(var1, this.orientation);
   }

   public Vector3f getUp(Vector3f var1) {
      return GlUtil.getUpVector(var1, this.orientation);
   }

   public Vector3f getForward(Vector3f var1) {
      return GlUtil.getForwardVector(var1, this.orientation);
   }

   public boolean isInvisibilityMode() {
      return false;
   }

   public boolean isOnServer() {
      return this.creature.isOnServer();
   }

   public TargetableAICreatureNetworkObject getNetworkObject() {
      return this.creature.getNetworkObject();
   }

   public boolean isVulnerable() {
      return this.getCreature().getAffinity() == null || !(this.getCreature().getAffinity() instanceof SegmentController) || ((SegmentController)this.getCreature().getAffinity()).isVulnerable();
   }

   public void updateLocal(Timer var1) {
      super.updateLocal(var1);
      this.dir.set(0.0F, 0.0F, 0.0F);
      this.updateInventory();
      if (this.isOnServer()) {
         this.checkProximityObjects();
         this.handleAIMessages();
         this.updateServer();
      } else {
         this.updateClient();
      }

      this.moveToTarget(var1);
      this.interpolateOrientation(var1);
      if (var1.currentTime - this.lastLagReceived > 7000L) {
         this.currentLag = 0L;
      }

   }

   public String toString() {
      return "AIPlayer(" + this.getName() + ", " + this.getId() + ")";
   }

   public AbstractCharacter getAbstractCharacterObject() {
      return this.getCreature();
   }

   public int getId() {
      return this.creature.getId();
   }

   public void updateToFullNetworkObject() {
      super.updateToFullNetworkObject();
      this.getNetworkObject().health.set(this.getHealth());
      this.getNetworkObject().sittingState.getLongArray();
      int var10000 = this.sittingOnId;
      this.getNetworkObject().sittingState.set(0, (long)this.sittingOnId);
      this.getNetworkObject().sittingState.set(1, ElementCollection.getIndex(this.sittingPos));
      this.getNetworkObject().sittingState.set(2, ElementCollection.getIndex(this.sittingPosTo));
      this.getNetworkObject().sittingState.set(3, ElementCollection.getIndex(this.sittingPosLegs));
   }

   private void destroy() {
      this.getCreature().markForPermanentDelete(true);
      this.getCreature().setMarkedForDeleteVolatile(true);
   }

   private void dieOnServer(Damager var1) {
      System.err.println(var1 + " killed AI " + this);
      this.destroy();
   }

   private void adjustForward(Vector3f var1) {
      (var1 = new Vector3f(var1)).normalize();
      Vector3f var2 = new Vector3f(0.0F, 1.0F, 0.0F);
      Vector3f var3;
      (var3 = new Vector3f()).cross(var2, var1);
      var3.normalize();
      var2.cross(var1, var3);
      var2.normalize();
      var1.normalize();
      GlUtil.setForwardVector(var1, this.orientationTo);
      GlUtil.setUpVector(var2, this.orientationTo);
      GlUtil.setRightVector(var3, this.orientationTo);
      this.getCreature().getPhysicsDataContainer().getObject().setWorldTransform(this.getWorldTransform());
   }

   private void interpolateOrientation(Timer var1) {
      Quat4f var2 = new Quat4f();
      Quat4fTools.set(this.orientation.basis, var2);
      Quat4f var3 = new Quat4f();
      Quat4fTools.set(this.orientationTo.basis, var3);
      Quat4f var4 = new Quat4f();
      Quat4Util.slerp(var2, var3, Math.min(1.0F, var1.getDelta() * this.rotationSpeed), var4);
      var4.normalize();
      if (var4.w != 0.0F) {
         this.orientation.setRotation(var4);
      }

   }

   public boolean isAtTarget() {
      if (this.target != null) {
         Transform var1 = new Transform(this.target);
         Transform var2;
         if (this.getCreature().getAffinity() != null) {
            var2 = new Transform(this.getCreature().getAffinity().getWorldTransform());
         } else {
            (var2 = new Transform()).setIdentity();
         }

         Transform var3 = new Transform(this.getWorldTransform());
         var2.inverse();
         var2.mul(var3);
         var3.set(var2);
         Vector3f var4;
         (var4 = new Vector3f()).sub(var1.origin, var3.origin);
         return var4.length() < 0.5F;
      } else {
         return true;
      }
   }

   public void moveToTarget(Timer var1) {
      if (this.getCreature().getAffinity() == null) {
         if (this.creature.isOnServer()) {
            this.resetTargetToMove();
         } else {
            if (this.target != null) {
               System.err.println("[CLIENT][AICREATURE] AI " + this + " has no affinity, but target. waiting...");
            }

         }
      } else if (!this.isSitting()) {
         boolean var2 = false;
         Vector3f var3;
         Transform var6;
         if ((var3 = this.creature.getLookDir()).length() > 0.0F) {
            var2 = true;
            Vector3f var4 = new Vector3f();
            GlUtil.project(this.getUp(new Vector3f()), var3, var4);
            var4.normalize();
            Vector3f var5;
            (var5 = new Vector3f()).cross(var4, this.getUp(new Vector3f()));
            var5.normalize();
            (var6 = new Transform()).setIdentity();
            GlUtil.setForwardVector(var4, var6);
            GlUtil.setUpVector(this.getUp(new Vector3f()), var6);
            GlUtil.setRightVector(var5, var6);
            Quat4fTools.set(var6.basis, this.targetQuat);
         }

         if (this.target != null) {
            Transform var7 = new Transform(this.target);
            Transform var8 = new Transform(this.getCreature().getAffinity().getWorldTransform());
            var6 = new Transform(this.getWorldTransform());
            var8.inverse();
            var8.mul(var6);
            var6.set(var8);
            if (this.isFlying()) {
               this.dir.sub(var7.origin, var6.origin);
               this.dir.normalize();
               this.getCreature().getAffinity().getWorldTransform().basis.transform(this.dir);
               this.dir.scale(var1.getDelta() * this.creature.getSpeed());
               if (this.dir.length() > 0.0F) {
                  this.creature.getCharacterController().setWalkDirectionStacked(this.dir);
               }
            } else {
               this.dir.sub(var7.origin, var6.origin);
               this.dir.normalize();
               this.getCreature().getAffinity().getWorldTransform().basis.transform(this.dir);
               this.dir.y = 0.0F;
               if (var7.origin.y - var6.origin.y > 0.5F && this.getCreature().getCharacterController().canJump()) {
                  this.getCreature().getCharacterController().jump();
               }

               if (var6.origin.y - var7.origin.y > 0.5F && this.getCreature().getCharacterController().isJumping()) {
                  this.getCreature().getCharacterController().breakJump(var1);
               }

               this.dir.scale(var1.getDelta() * this.creature.getSpeed());
               if (this.dir.length() > 0.0F) {
                  this.creature.getCharacterController().setWalkDirectionStacked(this.dir);
               }
            }

            if (var2) {
               this.adjustForward(var3);
            } else {
               this.adjustForward(this.dir);
            }
         } else {
            if (var2) {
               this.adjustForward(var3);
            }

         }
      }
   }

   public Vector3f getMovingDir() {
      return this.dir;
   }

   public String getName() {
      return this.creature.getRealName();
   }

   public StateInterface getState() {
      return this.creature.getState();
   }

   public void initFromNetworkObject() {
      this.health = this.getNetworkObject().health.getFloat();
      this.sittingOnId = (int)this.getNetworkObject().sittingState.getLongArray()[0];
      ElementCollection.getPosFromIndex(this.getNetworkObject().sittingState.getLongArray()[1], this.sittingPos);
      ElementCollection.getPosFromIndex(this.getNetworkObject().sittingState.getLongArray()[2], this.sittingPosTo);
      ElementCollection.getPosFromIndex(this.getNetworkObject().sittingState.getLongArray()[3], this.sittingPosLegs);
   }

   public void plotInstantPath() {
      if (this.creature.getAffinity() != null) {
         Vector3i var1 = this.creature.getPosInAffinity(new Vector3i());
         SegmentPiece var8;
         if ((var8 = ((SegmentController)this.getCreature().getAffinity()).getSegmentBuffer().getPointUnsave(var1)) == null) {
            System.err.println("[AIPlayer] cannot plot random path for " + this.creature + ": " + this.creature.getAffinity() + "; from segment piece is null");
         } else {
            int var2 = (Integer)this.creature.getAiConfiguration().get(Types.ORIGIN_X).getCurrentState();
            int var3 = (Integer)this.creature.getAiConfiguration().get(Types.ORIGIN_Y).getCurrentState();
            int var4 = (Integer)this.creature.getAiConfiguration().get(Types.ORIGIN_Z).getCurrentState();
            int var5 = (Integer)this.creature.getAiConfiguration().get(Types.ROAM_X).getCurrentState();
            int var6 = (Integer)this.creature.getAiConfiguration().get(Types.ROAM_Y).getCurrentState();
            int var7 = (Integer)this.creature.getAiConfiguration().get(Types.ROAM_Z).getCurrentState();
            Vector3i var9 = new Vector3i(var2, var3, var4);
            BoundingBox var10;
            if (var5 > 0 && var6 > 0 && var7 > 0) {
               var10 = new BoundingBox(new Vector3f((float)(-var5), (float)(-var6), (float)(-var7)), new Vector3f((float)var5, (float)var6, (float)var7));
            } else {
               var10 = new BoundingBox(new Vector3f(-20.0F, -10.0F, -20.0F), new Vector3f(20.0F, 10.0F, 20.0F));
            }

            Vector3i var11;
            if (this.creature.getGravity().isGravityOn()) {
               var11 = new Vector3i(Universe.getRandom().nextInt(3) - 1, 0, Universe.getRandom().nextInt(3) - 1);
               ((GameServerState)this.creature.getState()).getController().queueSegmentRandomGroundPath(var8, var9, var10, var11, this);
            } else {
               var11 = new Vector3i(Universe.getRandom().nextInt(3) - 1, Universe.getRandom().nextInt(3) - 1, Universe.getRandom().nextInt(3) - 1);
               ((GameServerState)this.creature.getState()).getController().queueSegmentRandomPath(var8, var9, var10, var11, this);
            }
         }
      } else {
         System.err.println("[AIPlayer] cannot plot random path for " + this.creature + ": " + this.creature.getAffinity());
      }
   }

   public void plotPath(SegmentPiece var1, boolean var2) {
      if (var2 && var1.getSegment().getSegmentController() != this.creature.getAffinity()) {
         this.creature.setAffinity(var1.getSegment().getSegmentController());
      }

      if (this.creature.getAffinity() != null && this.creature.getAffinity() == var1.getSegment().getSegmentController()) {
         Vector3i var3 = this.creature.getPosInAffinity(new Vector3i());
         SegmentPiece var4;
         if ((var4 = var1.getSegment().getSegmentController().getSegmentBuffer().getPointUnsave(var3)) != null) {
            System.err.println(this + " QUEUING PATH " + var4 + " -> " + var1);
            if (this.creature.getGravity().isGravityOn()) {
               ((GameServerState)this.creature.getState()).getController().queueSegmentGroundPath(var4, var1, this);
            } else {
               ((GameServerState)this.creature.getState()).getController().queueSegmentPath(var4, var1, this);
            }
         } else {
            System.err.println("[AIPlayer] FROM SEGMENT PIECE NULL");
         }
      } else {
         System.err.println("[AIPlayer] cannot plot path for " + this.creature + ": " + this.creature.getAffinity() + "; to " + var1 + " of " + var1.getSegment().getSegmentController());
      }
   }

   public void plotPath(Vector3i var1) {
      Vector3i var2 = this.creature.getPosInAffinity(new Vector3i());
      System.err.println(this + " QUEUING ABSOLUTE (UNSAVE) PATH " + var2 + " -> " + var1 + "; Affinity: " + this.creature.getAffinity());
      if (this.creature.getGravity().isGravityOn()) {
         ((GameServerState)this.creature.getState()).getController().queueSegmentGroundPath(var2, var1, (SegmentController)this.creature.getAffinity(), this);
      } else {
         ((GameServerState)this.creature.getState()).getController().queueSegmentPath(var2, var1, (SegmentController)this.creature.getAffinity(), this);
      }
   }

   public void updateFromNetworkObject() {
      super.handleInventoryNT();
      if (!this.creature.isOnServer()) {
         this.health = this.getNetworkObject().health.getFloat();
         this.sittingOnId = (int)this.getNetworkObject().sittingState.getLongArray()[0];
         ElementCollection.getPosFromIndex(this.getNetworkObject().sittingState.getLongArray()[1], this.sittingPos);
         ElementCollection.getPosFromIndex(this.getNetworkObject().sittingState.getLongArray()[2], this.sittingPosTo);
         ElementCollection.getPosFromIndex(this.getNetworkObject().sittingState.getLongArray()[3], this.sittingPosLegs);
         if ((Boolean)this.getNetworkObject().hasTarget.get()) {
            if (this.target == null) {
               this.target = new Transform();
               this.target.setIdentity();
            }

            this.target.origin.set(this.getNetworkObject().target.getVector());
         } else {
            this.target = null;
         }
      }

      for(Iterator var1 = this.getNetworkObject().lagAnnouncement.getReceiveBuffer().iterator(); var1.hasNext(); this.lastLagReceived = System.currentTimeMillis()) {
         long var2 = (Long)var1.next();
         this.currentLag = var2;
      }

   }

   public void updateToNetworkObject() {
      if (this.isOnServer()) {
         this.getNetworkObject().health.set(this.getHealth());
         this.getNetworkObject().sittingState.getLongArray();
         int var10000 = this.sittingOnId;
         this.getNetworkObject().sittingState.set(0, (long)this.sittingOnId);
         this.getNetworkObject().sittingState.set(1, ElementCollection.getIndex(this.sittingPos));
         this.getNetworkObject().sittingState.set(2, ElementCollection.getIndex(this.sittingPosTo));
         this.getNetworkObject().sittingState.set(3, ElementCollection.getIndex(this.sittingPosLegs));
      }

   }

   public Transform getWorldTransform() {
      return this.creature.getWorldTransform();
   }

   public AICreature getCreature() {
      return this.creature;
   }

   private void handleAIMessages() {
      if (!this.aiMessages.isEmpty()) {
         synchronized(this.aiMessages) {
            MachineProgram var2;
            if ((var2 = ((CreatureAIEntity)this.getCreature().getAiConfiguration().getAiEntityState()).getCurrentProgram()) != null && var2 instanceof AICreatureProgramInterface) {
               AICreatureProgramInterface var4 = (AICreatureProgramInterface)var2;

               while(!this.aiMessages.isEmpty()) {
                  ((AICreatureMessage)this.aiMessages.dequeue()).handle(var4);
               }
            }

         }
      }
   }

   public void sendAIMessage(AICreatureMessage var1) {
      synchronized(this.aiMessages) {
         this.aiMessages.enqueue(var1);
      }
   }

   private void checkProximityObjects() {
      if (System.currentTimeMillis() - this.lastCheck > 5000L) {
         this.getProximityEntities().clear();
         synchronized(this.getState().getLocalAndRemoteObjectContainer().getLocalObjects()) {
            Iterator var2 = this.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

            while(var2.hasNext()) {
               Sendable var3;
               if ((var3 = (Sendable)var2.next()) instanceof SimpleTransformableSendableObject && this.isEnemy(var3) && this.getCreature().isInShootingRange((SimpleTransformableSendableObject)var3)) {
                  this.getProximityEntities().add((SimpleTransformableSendableObject)var3);
               }
            }
         }

         if (this.getProximityEntities().size() > 0) {
            final SimpleTransformableSendableObject var1 = (SimpleTransformableSendableObject)this.getProximityEntities().get(Universe.getRandom().nextInt(this.getProximityEntities().size()));
            this.sendAIMessage(new AICreatureMessage() {
               public void handle(AICreatureProgramInterface var1x) {
                  try {
                     var1x.enemyProximity(var1);
                  } catch (FSMException var2) {
                     var2.printStackTrace();
                  }
               }

               public AICreatureMessage.MessageType getType() {
                  return AICreatureMessage.MessageType.PROXIMITY;
               }
            });
         }

         this.lastCheck = System.currentTimeMillis();
      }

   }

   protected boolean isEnemy(Sendable var1) {
      if (var1 != this && var1 != this.creature) {
         if (var1 instanceof SimpleTransformableSendableObject) {
            return var1 instanceof SegmentController && !this.getCreature().getAiConfiguration().isAttackStructures() ? false : ((FactionState)this.getState()).getFactionManager().isEnemy(this.getFactionId(), ((SimpleTransformableSendableObject)var1).getFactionId());
         } else {
            return false;
         }
      } else {
         return false;
      }
   }

   protected abstract void updateServer();

   protected abstract void updateClient();

   public Transform getTarget() {
      return this.target;
   }

   public void setTarget(Transform var1) {
      this.target = var1;
      this.lastTargetSet = System.currentTimeMillis();
   }

   public boolean hasNaturalWeapon() {
      return false;
   }

   public void fireNaturalWeapon(AbstractCharacter var1, AbstractOwnerState var2, Vector3f var3) {
   }

   public void pathFinished(boolean var1, List var2) {
      if (var1) {
         this.setCurrentPath(var2);
      } else {
         this.setCurrentPath((List)null);
         this.sendAIMessage(new AICreatureMessage() {
            public void handle(AICreatureProgramInterface var1) {
               try {
                  var1.onNoPath();
               } catch (FSMException var2) {
                  var2.printStackTrace();
               }
            }

            public AICreatureMessage.MessageType getType() {
               return AICreatureMessage.MessageType.NO_PATH;
            }
         });
      }
   }

   public Vector3i getObjectSize() {
      return this.getCreature().getBlockDim();
   }

   public List getCurrentPath() {
      return this.currentPath;
   }

   public void setCurrentPath(List var1) {
      this.currentPath = var1;
   }

   public int getFactionId() {
      return this.creature.getFactionId();
   }

   public String getUniqueIdentifier() {
      return "AIPlayerOf" + this.creature.getUniqueIdentifier();
   }

   private void setFactionId(int var1) {
      this.creature.setFactionId(var1);
   }

   public boolean isVolatile() {
      return true;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var3 = (Tag[])var1.getValue();
      this.setFactionId((Integer)var3[0].getValue());
      this.getInventory().fromTagStructure(var3[1]);
      this.health = (Float)var3[2].getValue();
      String var2;
      if (var3[3].getType() != Tag.Type.FINISH && !(var2 = (String)var3[3].getValue()).equals("none")) {
         this.conversationScript = var2;
      }

      if (var3.length > 4 && var3[4].getType() != Tag.Type.FINISH) {
         this.fromSittingTag(var3[4]);
      }

   }

   public Tag toTagStructure() {
      Tag var1 = new Tag(Tag.Type.INT, (String)null, this.getFactionId());
      Tag var2 = new Tag(Tag.Type.FLOAT, (String)null, this.health);
      Tag var3 = this.getInventory().toTagStructure();
      Tag var4 = new Tag(Tag.Type.STRING, (String)null, this.getConversationScript() == null ? "none" : this.getConversationScript());
      Tag var5 = this.toSittingTag();
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, var3, var2, var4, var5, FinishTag.INST});
   }

   public ObjectArrayList getProximityEntities() {
      return this.proximityEntities;
   }

   public void setConversationState(PlayerState var1, String var2) {
      System.err.println("[SERVER] CONVERSATION STATE SET FOR " + var1 + " -> " + var2);
      this.conversationStates.put(var1, var2);
   }

   public String getConversationState(PlayerState var1) {
      return !this.conversationStates.containsKey(var1) ? "none" : (String)this.conversationStates.get(var1);
   }

   public long getCurrentLag() {
      return this.currentLag;
   }
}
