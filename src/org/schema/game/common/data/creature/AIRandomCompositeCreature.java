package org.schema.game.common.data.creature;

import java.util.List;
import java.util.Random;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.network.objects.TargetableDynamicAICreatureNetworkObject;
import org.schema.game.server.ai.AIControllerStateUnit;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.resource.CreatureStructure;
import org.schema.schine.resource.ResourceLoadEntry;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class AIRandomCompositeCreature extends AICompositeCreature {
   private static final int MAX_PARTS = 2;
   private float speed;
   private float scale;
   private float height;
   private float width;
   private Vector3i blockDim = new Vector3i(1, 1, 1);
   private CreaturePartNode main;
   private boolean meelee = true;

   public AIRandomCompositeCreature(StateInterface var1, float var2, float var3, float var4, float var5, Vector3i var6) {
      super(var1);
   }

   public AIRandomCompositeCreature(StateInterface var1) {
      super(var1);
   }

   public static AIRandomCompositeCreature instantiate(StateInterface var0, float var1, float var2, float var3, float var4, Vector3i var5, CreaturePartNode var6) {
      AIRandomCompositeCreature var7;
      (var7 = new AIRandomCompositeCreature(var0)).setSpeed(var1);
      var7.scale = var2;
      var7.height = var3;
      var7.width = var4;
      var7.blockDim = new Vector3i(var5);
      var7.main = var6;

      assert var7.main != null;

      return var7;
   }

   private static String getRandomMesh(StateInterface var0, Random var1, CreatureStructure.PartType var2) {
      List var3;
      if ((var3 = var0.getResourceMap().getType(var2)).isEmpty()) {
         throw new IllegalArgumentException("no resources for : " + var2.name());
      } else {
         return (String)var3.get(var1.nextInt(var3.size()));
      }
   }

   private static void attach(StateInterface var0, Random var1, int var2, int var3, CreaturePartNode var4) {
      while(true) {
         String var5 = getRandomMesh(var0, var1, CreatureStructure.PartType.values()[var2]);
         ResourceLoadEntry var6;
         CreaturePartNode var7;
         if ((var6 = var0.getResourceMap().get(var5)).texture != null) {
            String var8 = (String)var6.texture.getDiffuseMapNames().get(var1.nextInt(var6.texture.getDiffuseMapNames().size()));
            var7 = new CreaturePartNode(var4.type == CreatureStructure.PartType.BOTTOM ? CreatureStructure.PartType.MIDDLE : CreatureStructure.PartType.TOP, var0, var5, var8);
         } else {
            var7 = new CreaturePartNode(var4.type == CreatureStructure.PartType.BOTTOM ? CreatureStructure.PartType.MIDDLE : CreatureStructure.PartType.TOP, var0, var5, (String)null);
         }

         var4.attach(var0, var7, CreaturePartNode.AttachmentType.MAIN);
         if (var2 >= var3) {
            return;
         }

         int var10002 = var2 + 1;
         var4 = var7;
         var3 = var3;
         var2 = var10002;
         var1 = var1;
         var0 = var0;
      }
   }

   public static CreaturePartNode getRandomHierarchy(StateInterface var0, Random var1, int var2) {
      String var3 = getRandomMesh(var0, var1, CreatureStructure.PartType.BOTTOM);
      ResourceLoadEntry var4;
      String var6;
      if ((var4 = var0.getResourceMap().get(var3)).texture != null) {
         assert !var4.texture.getDiffuseMapNames().isEmpty() : var3;

         var6 = (String)var4.texture.getDiffuseMapNames().get(var1.nextInt(var4.texture.getDiffuseMapNames().size()));
      } else {
         var6 = null;
      }

      CreaturePartNode var5 = new CreaturePartNode(CreatureStructure.PartType.MIDDLE, var0, var3, var6);
      attach(var0, var1, 1, var2 - 1, var5);
      return var5;
   }

   public static AIRandomCompositeCreature random(StateInterface var0) {
      Random var1 = new Random();
      CreaturePartNode var2 = getRandomHierarchy(var0, var1, 2);
      CreatureStructure var3;
      float var4 = Math.max((var3 = var0.getResourceMap().get(var2.meshName).creature).maxScale.z, Math.max(var3.maxScale.x, var3.maxScale.y));
      var4 = var1.nextFloat() * (var4 - 1.0F) + 1.0F;
      return instantiate(var0, 2.0F + var1.nextFloat() * 4.0F, var4, 0.2F, 0.3F, var3.dim, var2);
   }

   public void initialize() {
      super.initialize();
   }

   public TargetableDynamicAICreatureNetworkObject getNetworkObject() {
      return (TargetableDynamicAICreatureNetworkObject)super.getNetworkObject();
   }

   public void newNetworkObject() {
      this.ntInit(new TargetableDynamicAICreatureNetworkObject(this.getState(), this.getOwnerState()));
   }

   private void fromTagStructureCompo(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.setSpeed((Float)var2[0].getValue());
      this.scale = (Float)var2[1].getValue();
      this.width = (Float)var2[2].getValue();
      this.height = (Float)var2[3].getValue();
      this.blockDim = (Vector3i)var2[4].getValue();
      this.main = new CreaturePartNode(CreatureStructure.PartType.BOTTOM);
      this.main.fromTagStructure(var2[5]);
   }

   private Tag toTagStructureCompo() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.FLOAT, (String)null, this.getSpeed()), new Tag(Tag.Type.FLOAT, (String)null, this.scale), new Tag(Tag.Type.FLOAT, (String)null, this.width), new Tag(Tag.Type.FLOAT, (String)null, this.height), new Tag(Tag.Type.VECTOR3i, (String)null, this.blockDim), this.main.toTagStructure(), FinishTag.INST});
   }

   public float getCharacterHeightOffset() {
      return 0.0F;
   }

   public float getCharacterHeight() {
      return this.height * (1.0F + (this.scale - 0.5F));
   }

   public float getCharacterWidth() {
      return this.width * (1.0F + (this.scale - 0.5F));
   }

   public Vector3i getBlockDim() {
      return this.blockDim;
   }

   public float getSpeed() {
      return this.speed;
   }

   public void setSpeed(float var1) {
      this.speed = var1;
   }

   public boolean isMeleeAttacker() {
      return this.meelee;
   }

   public void updateToNetworkObject() {
      super.updateToNetworkObject();
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      super.fromTagStructure(var2[0]);
      this.fromTagStructureCompo(var2[1]);
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{super.toTagStructure(), this.toTagStructureCompo(), FinishTag.INST});
   }

   public void initialFillInventory() {
   }

   public void initFromNetworkObject(NetworkObject var1) {
      super.initFromNetworkObject(var1);
      if (!this.isOnServer()) {
         this.setSpeed(this.getNetworkObject().speed.getFloat());
         this.scale = this.getNetworkObject().scale.getFloat();
         this.height = this.getNetworkObject().height.getFloat();
         this.width = this.getNetworkObject().width.getFloat();
         this.blockDim = this.getNetworkObject().boxDim.getVector();
         this.main = (CreaturePartNode)this.getNetworkObject().creatureCode.get();
      }

   }

   public void updateFromNetworkObject(NetworkObject var1, int var2) {
      super.updateFromNetworkObject(var1, var2);
   }

   public void updateToFullNetworkObject() {
      super.updateToFullNetworkObject();
      this.getNetworkObject().speed.set(this.getSpeed());
      this.getNetworkObject().scale.set(this.scale);
      this.getNetworkObject().height.set(this.height);
      this.getNetworkObject().width.set(this.width);
      this.getNetworkObject().boxDim.set(this.blockDim);
      this.getNetworkObject().creatureCode.set(this.main);
   }

   public CreaturePartNode getCreatureNode() {
      return this.main;
   }

   public float getScale() {
      return this.scale;
   }

   public void handleControl(Timer var1, AIControllerStateUnit var2) {
   }
}
