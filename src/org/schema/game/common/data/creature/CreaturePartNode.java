package org.schema.game.common.data.creature;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.client.view.character.BoneLocationInterface;
import org.schema.schine.graphicsengine.animation.AnimationChannel;
import org.schema.schine.graphicsengine.animation.AnimationController;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.CreatureStructure;
import org.schema.schine.resource.ResourceLoadEntry;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class CreaturePartNode implements BoneLocationInterface, TagSerializable {
   public final CreatureStructure.PartType type;
   private final ObjectArrayList chields = new ObjectArrayList();
   public String meshName;
   public byte attachedTo = -1;
   public String texture;
   private CreaturePartNode parent;
   private byte attachedFromType = -1;
   private String mainBone;
   private String torsoBone;
   private String heldBone;

   public CreaturePartNode(CreatureStructure.PartType var1) {
      this.type = var1;
   }

   public CreaturePartNode(CreatureStructure.PartType var1, StateInterface var2, String var3, String var4) {
      this.type = var1;
      this.meshName = var3;
      ResourceLoadEntry var5 = var2.getResourceMap().get(var3);

      assert var5 != null : var3;

      if (var5.creature != null) {
         this.attachedFromType = (byte)CreaturePartNode.AttachmentType.MAIN.ordinal();
      } else {
         System.err.println("[CreaturePart] no creature info for: " + var3);
      }

      this.texture = var4;

      assert var5.creature.mainBone != null;

      this.mainBone = var5.creature.mainBone;
      this.torsoBone = var5.creature.upperBody;
      this.heldBone = var5.creature.heldBone;
   }

   public void serialize(DataOutput var1) throws IOException {
      var1.writeUTF(this.meshName);
      var1.writeUTF(this.texture != null ? this.texture : "");
      var1.writeByte(this.attachedTo);
      var1.writeByte(this.getChields().size());

      for(int var2 = 0; var2 < this.getChields().size(); ++var2) {
         ((CreaturePartNode)this.getChields().get(var2)).serialize(var1);
      }

   }

   public void deserialize(DataInput var1) throws IOException {
      this.meshName = var1.readUTF();
      this.texture = var1.readUTF();
      this.texture = this.texture != null ? this.texture : null;
      this.attachedTo = var1.readByte();
      byte var2 = var1.readByte();

      for(int var3 = 0; var3 < var2; ++var3) {
         CreaturePartNode var4;
         (var4 = new CreaturePartNode(this.type == CreatureStructure.PartType.BOTTOM ? CreatureStructure.PartType.MIDDLE : CreatureStructure.PartType.TOP)).deserialize(var1);
         this.getChields().add(var4);
      }

   }

   public boolean isRoot() {
      return this.parent == null;
   }

   public void attach(StateInterface var1, CreaturePartNode var2, CreaturePartNode.AttachmentType var3) {
      var2.attachedTo = (byte)var3.ordinal();
      var2.parent = this;
      this.getChields().add(var2);
   }

   public void attach(StateInterface var1, CreaturePartNode var2) {
      if (this.attachedFromType >= 0) {
         this.attach(var1, var2, CreaturePartNode.AttachmentType.values()[this.attachedFromType]);
      } else {
         throw new NullPointerException();
      }
   }

   public void detach(CreaturePartNode var1) {
      var1.parent = null;
      var1.attachedTo = -1;
      this.getChields().remove(var1);
   }

   public ObjectArrayList getChields() {
      return this.chields;
   }

   public CreaturePartNode getParent() {
      return this.parent;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var4 = (Tag[])var1.getValue();
      this.meshName = (String)var4[0].getValue();
      this.texture = (String)var4[1].getValue();
      this.attachedTo = (Byte)var4[2].getValue();
      this.texture = this.texture != null ? this.texture : null;
      var4 = (Tag[])var4[3].getValue();

      for(int var2 = 0; var2 < var4.length - 1; ++var2) {
         CreaturePartNode var3;
         (var3 = new CreaturePartNode(this.type == CreatureStructure.PartType.BOTTOM ? CreatureStructure.PartType.MIDDLE : CreatureStructure.PartType.TOP)).fromTagStructure(var4[var2]);
         this.getChields().add(var3);
      }

   }

   public Tag toTagStructure() {
      Tag var1 = new Tag(Tag.Type.STRING, (String)null, this.meshName);
      Tag var2 = new Tag(Tag.Type.STRING, (String)null, this.texture != null ? this.texture : "");
      Tag var3 = new Tag(Tag.Type.BYTE, (String)null, this.attachedTo);
      Tag[] var4;
      Tag[] var10000 = var4 = new Tag[this.getChields().size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;

      for(int var5 = 0; var5 < this.getChields().size(); ++var5) {
         var4[var5] = ((CreaturePartNode)this.getChields().get(var5)).toTagStructure();
      }

      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, var2, var3, new Tag(Tag.Type.STRUCT, (String)null, var4), FinishTag.INST});
   }

   public String getRootBoneName() {
      return this.mainBone;
   }

   public String getRootTorsoBoneName() {
      return this.torsoBone;
   }

   public void initializeListeners(AnimationController var1, AnimationChannel var2, AnimationChannel var3) {
   }

   public String getHeldBoneName() {
      return this.heldBone;
   }

   public void loadClientBones(StateInterface var1) {
      ResourceLoadEntry var2 = var1.getResourceMap().get(this.meshName);

      assert var2 != null : this.meshName;

      if (var2.creature != null) {
         this.attachedFromType = (byte)CreaturePartNode.AttachmentType.MAIN.ordinal();
      } else {
         System.err.println("[CreaturePart] no creature info for: " + this.meshName);
      }

      this.mainBone = var2.creature.mainBone;
      this.torsoBone = var2.creature.upperBody;
      this.heldBone = var2.creature.heldBone;
   }

   public static enum AttachmentType {
      MAIN,
      WEAPON;
   }
}
