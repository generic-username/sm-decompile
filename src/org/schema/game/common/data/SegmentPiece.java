package org.schema.game.common.data;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.List;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentData3Byte;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.network.objects.remote.RemoteSegmentPiece;
import org.schema.schine.graphicsengine.forms.DebugBox;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class SegmentPiece {
   public byte x;
   public byte y;
   public byte z;
   public boolean forceClientSegmentAdd;
   protected int data;
   private Segment segment;
   protected static final byte tagVersion = 0;

   public SegmentPiece() {
   }

   public static final int deserializeData(DataInput var0) throws IOException {
      return SegmentData.convert3ByteToIntValue(var0.readByte(), var0.readByte(), var0.readByte());
   }

   public static final void serializeData(DataOutput var0, int var1) throws IOException {
      var0.writeByte((byte)var1);
      var0.writeByte((byte)(var1 >> 8));
      var0.writeByte((byte)(var1 >> 16));
   }

   public SegmentPiece(Segment var1, int var2) {
      this(var1, SegmentData.getPosXFromIndex(var2), SegmentData.getPosYFromIndex(var2), SegmentData.getPosZFromIndex(var2));
   }

   public void setByReference(Segment var1, int var2) {
      this.setByReference(var1, SegmentData.getPosXFromIndex(var2), SegmentData.getPosYFromIndex(var2), SegmentData.getPosZFromIndex(var2));
   }

   public SegmentPiece(Segment var1, byte var2, byte var3, byte var4) {
      this.x = var2;
      this.y = var3;
      this.z = var4;
      this.setSegment(var1);
      if (var1 != null && !var1.isEmpty() && var1.getSegmentData() != null) {
         this.setDataByReference(var1.getSegmentData().getDataAt(SegmentData.getInfoIndex(var2, var3, var4)));
      }

   }

   public SegmentPiece(SegmentPiece var1) {
      this(var1.segment, var1.x, var1.y, var1.z);
   }

   public void setByReference(SegmentPiece var1) {
      this.setByReference(var1.segment, var1.x, var1.y, var1.z);
   }

   public SegmentPiece(Segment var1, Vector3b var2) {
      this.setByReference(var1, var2);
   }

   public SegmentPiece(Segment var1, Vector3b var2, int var3) {
      this.setSegment(var1);
      this.setPos(var2);
      this.setDataByReference(var3);
   }

   public static synchronized float getWorldDistance(SegmentPiece var0, SegmentPiece var1) {
      Vector3i var2 = new Vector3i();
      Vector3i var3 = new Vector3i();
      Vector3f var4 = new Vector3f();
      Vector3f var5 = new Vector3f();
      var0.getAbsolutePos(var2);
      var1.getAbsolutePos(var3);
      var4.set((float)var2.x, (float)var2.y, (float)var2.z);
      var5.set((float)var3.x, (float)var3.y, (float)var3.z);
      var0.getSegment().getSegmentController().getWorldTransform().basis.transform(var4);
      var1.getSegment().getSegmentController().getWorldTransform().basis.transform(var5);
      var4.add(var0.getSegment().getSegmentController().getWorldTransform().origin);
      var5.add(var1.getSegment().getSegmentController().getWorldTransform().origin);
      var4.sub(var5);
      return var4.length();
   }

   public static VoidUniqueSegmentPiece getFromUniqueTag(Tag var0, int var1) {
      Tag[] var3 = (Tag[])var0.getValue();
      VoidUniqueSegmentPiece var2 = new VoidUniqueSegmentPiece();
      if (var3[0].getType() == Tag.Type.BYTE) {
         var3[0].getByte();
         var2.uniqueIdentifierSegmentController = (String)var3[1].getValue();
         var2.voidPos.set((Vector3i)var3[2].getValue());
         var2.setType((Short)var3[3].getValue());
         var2.setOrientation((Byte)var3[4].getValue());
         var2.setActive((Byte)var3[5].getValue() > 0);
         var2.setHitpointsByte(var3[6].getByte());
      } else {
         var2.uniqueIdentifierSegmentController = (String)var3[0].getValue();
         var2.voidPos.set((Vector3i)var3[1].getValue());
         var2.voidPos.add(var1, var1, var1);
         var2.setType((Short)var3[2].getValue());
         var2.setOrientation((Byte)var3[3].getValue());
         var2.setActive((Byte)var3[4].getValue() > 0);
         var2.setHitpointsByte(var3[5].getByte());
         SegmentData3Byte.migratePiece(var2);
      }

      return var2;
   }

   public void reset() {
      this.data = 0;
      this.segment = null;
      this.forceClientSegmentAdd = false;
      this.x = 0;
      this.y = 0;
      this.z = 0;
   }

   public boolean equalsPos(Vector3i var1) {
      return this.getSegment().equalsAbsoluteElemPos(var1, this.x, this.y, this.z);
   }

   public boolean equalsPos(int var1, int var2, int var3) {
      return ElementCollection.getIndex(var1, var2, var3) == this.getAbsoluteIndex();
   }

   public long getAbsoluteIndex() {
      return this.getSegment().getAbsoluteIndex(this.x, this.y, this.z);
   }

   public long getAbsoluteIndexWithType4() {
      return ElementCollection.getIndex4(this.getSegment().getAbsoluteIndex(this.x, this.y, this.z), this.getType());
   }

   public long getTextBlockIndex() {
      return ElementCollection.getIndex4(this.getSegment().getAbsoluteIndex(this.x, this.y, this.z), (short)this.getOrientation());
   }

   public Vector3i getAbsolutePos(Vector3i var1) {
      if (this.getSegment() != null) {
         return this.getSegment().getAbsoluteElemPos(this.x, this.y, this.z, var1);
      } else {
         return this instanceof VoidSegmentPiece ? ((VoidSegmentPiece)this).voidPos : null;
      }
   }

   public Vector3f getAbsolutePos(Vector3f var1) {
      if (this.getSegment() != null) {
         return this.getSegment().getAbsoluteElemPos(this.x, this.y, this.z, var1);
      } else if (this instanceof VoidSegmentPiece) {
         var1.set((float)((VoidSegmentPiece)this).voidPos.x, (float)((VoidSegmentPiece)this).voidPos.y, (float)((VoidSegmentPiece)this).voidPos.z);
         return var1;
      } else {
         return null;
      }
   }

   public byte getCornerSegmentsDiffs() {
      byte var1 = 0;
      if (this.x == 0) {
         var1 = 32;
      } else if (this.x == 31) {
         var1 = 16;
      }

      if (this.y == 0) {
         var1 = (byte)(var1 + 8);
      } else if (this.y == 31) {
         var1 = (byte)(var1 + 4);
      }

      if (this.z == 0) {
         var1 = (byte)(var1 + 2);
      } else if (this.z == 31) {
         ++var1;
      }

      return var1;
   }

   public int getData() {
      return this.data;
   }

   public void setDataByReference(int var1) {
      this.data = var1;
   }

   public void setType(short var1) {
      this.data = this.data & -2048 | var1;
   }

   public void setHitpointsFull(int var1) {
      this.setHitpointsByte((int)((double)var1 * this.getInfo().getMaxHitpointsFullToByte()));
   }

   public void setHitpointsByte(int var1) {
      assert var1 >= 0 && var1 < 128 : var1;

      short var2 = (short)Math.min(127, var1);
      this.data = this.data & -260097 | var2 << 11;
   }

   public void setActive(boolean var1) {
      this.data = var1 ? this.data | 262144 : this.data & -262145;
   }

   public void setOrientation(byte var1) {
      assert var1 >= 0 && var1 < 32 : "NOT A SIDE INDEX";

      this.data = this.data & -16252929 | var1 << 19;
   }

   public short getType() {
      return getType(this.data);
   }

   public static short getType(int var0) {
      return (short)(2047 & var0);
   }

   public int getHitpointsFull() {
      return this.getType() != 0 ? this.getInfo().convertToFullHp(this.getHitpointsByte()) : this.getHitpointsByte();
   }

   public short getHitpointsByte() {
      return (short)((260096 & this.data) >> 11);
   }

   public byte getOrientation() {
      return (byte)((16252928 & this.data) >> 19);
   }

   public boolean isActive() {
      return (262144 & this.data) > 0;
   }

   public Vector3b getPos(Vector3b var1) {
      var1.set(this.x, this.y, this.z);
      return var1;
   }

   public Segment getSegment() {
      return this.segment;
   }

   public void setSegment(Segment var1) {
      this.segment = var1;
   }

   public SegmentController getSegmentController() {
      return this.segment.getSegmentController();
   }

   public void getTransform(Transform var1) {
      var1.set(this.getSegment().getSegmentController().getWorldTransform());
      Vector3i var2 = this.getAbsolutePos(new Vector3i());
      Vector3f var3 = new Vector3f((float)(var2.x - 16), (float)(var2.y - 16), (float)(var2.z - 16));
      var1.basis.transform(var3);
      var1.origin.add(var3);
   }

   public int hashCode() {
      return this.getSegment().pos.hashCode() + this.posHashCode();
   }

   public boolean equals(Object var1) {
      if (var1 != null && var1 instanceof SegmentPiece) {
         SegmentPiece var2;
         return (var2 = (SegmentPiece)var1).getSegmentController() == this.getSegmentController() && var2.getSegment().pos.equals(this.getSegment().pos) && var2.x == this.x && var2.y == this.y && var2.z == this.z;
      } else {
         return false;
      }
   }

   public String toString() {
      return this.getAbsolutePos(new Vector3i()).toString() + "[" + (this.getType() != 0 ? ElementKeyMap.getInfo(this.getType()).getName() : "NONE") + "]o[" + Element.getSideString(this.getOrientation()) + "][" + (this.isActive() ? "active" : "inactive") + "][" + this.getHitpointsByte() + "Bhp; " + this.getHitpointsFull() + " Fhp]" + this.getBlockStyleString();
   }

   public boolean isEdgeOfSegment() {
      return this.x == 0 || this.y == 0 || this.z == 0 || this.x == 31 || this.y == 31 || this.z == 31;
   }

   public void populate(Segment var1, byte var2, byte var3, byte var4) {
      this.segment = var1;
      this.x = var2;
      this.y = var3;
      this.z = var4;
      if (!var1.isEmpty() && var1.getSegmentData() != null) {
         this.setDataByReference(var1.getSegmentData().getDataAt(SegmentData.getInfoIndex(var2, var3, var4)));
      } else {
         this.data = 0;
      }
   }

   public void populate(Segment var1, Vector3b var2) {
      this.populate(var1, var2.x, var2.y, var2.z);
   }

   public int posHashCode() {
      long var1 = 7L + (long)this.x;
      var1 = 7L * var1 + (long)this.y;
      long var10000 = 7L * var1 + (long)this.z;
      return (byte)((int)(var10000 ^ var10000 >> 8));
   }

   public void refresh() {
      this.populate(this.segment, this.x, this.y, this.z);
   }

   public void setByValue(SegmentPiece var1) {
      this.setByReference(var1);
      this.data = var1.data;
   }

   public void setByReference(Segment var1, byte var2, byte var3, byte var4) {
      this.setSegment(var1);
      this.setPos(var2, var3, var4);
      if (var1.isEmpty()) {
         this.data = 0;
      } else {
         this.setDataByReference(var1.getSegmentData().getDataAt(SegmentData.getInfoIndex(var2, var3, var4)));
      }
   }

   public void setByReference(Segment var1, Vector3b var2) {
      this.setByReference(var1, var2.x, var2.y, var2.z);
   }

   public void setPos(byte var1, byte var2, byte var3) {
      this.x = var1;
      this.y = var2;
      this.z = var3;
   }

   public void setPos(Vector3b var1) {
      this.x = var1.x;
      this.y = var1.y;
      this.z = var1.z;
   }

   public void setWithoutData(Segment var1, Vector3b var2) {
      this.setSegment(var1);
      this.setPos(var2);
   }

   public String getBlockStyleString() {
      if (ElementKeyMap.isValidType(this.getType())) {
         ElementInformation var1;
         return (var1 = ElementKeyMap.getInfo(this.getType())).getBlockStyle() == BlockStyle.NORMAL ? "[BLOCK]" : "[" + ((BlockShape)BlockShapeAlgorithm.getAlgo(var1.getBlockStyle(), this.getOrientation()).getClass().getAnnotation(BlockShape.class)).name() + "]";
      } else {
         return "";
      }
   }

   public int getInfoIndex() {
      return SegmentData.getInfoIndex(this.x, this.y, this.z);
   }

   public Tag getUniqueTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.STRING, (String)null, this.getSegmentController().getUniqueIdentifier()), new Tag(Tag.Type.VECTOR3i, (String)null, this.getAbsolutePos(new Vector3f())), new Tag(Tag.Type.SHORT, (String)null, this.getType()), new Tag(Tag.Type.BYTE, (String)null, this.getOrientation()), new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.isActive() ? 1 : 0))), new Tag(Tag.Type.BYTE, (String)null, (byte)this.getHitpointsByte()), FinishTag.INST});
   }

   public BlockShapeAlgorithm getAlgorithm() {
      return this.getAlgorithm(this.getType());
   }

   public BlockShapeAlgorithm getAlgorithm(short var1) {
      return ElementKeyMap.isValidType(var1) && ElementKeyMap.getInfoFast(var1).getBlockStyle() != BlockStyle.NORMAL ? BlockShapeAlgorithm.getAlgo(ElementKeyMap.getInfoFast(var1).getBlockStyle(), this.getOrientation()) : null;
   }

   public int getFullOrientation() {
      return this.getOrientation();
   }

   public void debugDrawPoint(Transform var1, float var2, float var3, float var4, float var5, float var6, long var7) {
      debugDrawPoint(this.getAbsolutePos(new Vector3i()), var1, var2, var3, var4, var5, var6, var7);
   }

   public static void debugDrawPoint(Vector3i var0, Transform var1, float var2, float var3, float var4, float var5, float var6, long var7) {
      var1 = new Transform(var1);
      Vector3f var9 = new Vector3f((float)(var0.x - 16), (float)(var0.y - 16), (float)(var0.z - 16));
      var1.basis.transform(var9);
      var1.origin.add(var9);
      DebugBox var10;
      (var10 = new DebugBox(new Vector3f(-0.5F - var2, -0.5F - var2, -0.5F - var2), new Vector3f(var2 + 0.5F, var2 + 0.5F, var2 + 0.5F), new Transform(var1), var3, var4, var5, var6)).LIFETIME = var7;
      DebugDrawer.boxes.add(var10);
   }

   public ElementInformation getInfo() {
      return ElementKeyMap.getInfo(this.getType());
   }

   public boolean isValid() {
      return ElementKeyMap.isValidType(this.getType());
   }

   public int getAbsolutePosX() {
      return ElementCollection.getPosX(this.getAbsoluteIndex());
   }

   public int getAbsolutePosY() {
      return ElementCollection.getPosY(this.getAbsoluteIndex());
   }

   public int getAbsolutePosZ() {
      return ElementCollection.getPosZ(this.getAbsoluteIndex());
   }

   public boolean equalsUniqueIdentifier(String var1) {
      return this.getSegmentController().getUniqueIdentifier().equals(var1);
   }

   public Vector3f getWorldPos(Vector3f var1, int var2) {
      this.getSegmentController().getAbsoluteElementWorldPositionShifted(this.getAbsolutePosX(), this.getAbsolutePosY(), this.getAbsolutePosZ(), var2, var1);
      return var1;
   }

   public boolean equalsSegmentPos(Segment var1, Vector3b var2) {
      return var1.equals(this.segment) && var2.x == this.x && var2.y == this.y && var2.z == this.z;
   }

   public void applyToSegment(boolean var1) {
      if (this.getSegment() != null && this.getSegment().getSegmentData() != null) {
         boolean var2 = ElementKeyMap.isValidType(this.getSegment().getSegmentData().getType(this.getInfoIndex()));
         boolean var3 = ElementKeyMap.isValidType(this.getType());

         try {
            this.getSegment().getSegmentData().applySegmentData(this.x, this.y, this.z, this.data, 0, false, this.getAbsoluteIndex(), var2 && !var3, var3, this.getSegmentController().getState().getUpdateTime());
            if (var1) {
               if (var3) {
                  this.refresh();
                  this.getSegmentController().sendBlockMod(new RemoteSegmentPiece(this, this.getSegmentController().isOnServer()));
                  return;
               }

               if (var2) {
                  this.getSegmentController().sendBlockKill(this);
               }
            }

            return;
         } catch (SegmentDataWriteException var4) {
            SegmentDataWriteException.replaceData(this.getSegment());
            this.applyToSegment(var1);
         }
      }

   }

   public boolean isDead() {
      return !this.isAlive();
   }

   public boolean isAlive() {
      return this.getHitpointsByte() > 0;
   }

   public static class SegmentPiecePool {
      private final List pool = new ObjectArrayList();

      public void free(SegmentPiece var1) {
         this.pool.add(var1);
      }

      public SegmentPiece get() {
         return this.pool.isEmpty() ? new SegmentPiece() : (SegmentPiece)this.pool.remove(this.pool.size() - 1);
      }
   }
}
