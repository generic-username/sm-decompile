package org.schema.game.common.data;

import com.bulletphysics.linearmath.Transform;
import java.util.Random;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.network.StateInterface;

public interface SimpleGameObject extends Transformable {
   byte SIMPLE_TRANSFORMABLE_SENSABLE_OBJECT = 0;
   byte MISSILE = 1;
   byte MINABLE = 2;
   byte MINE = 3;

   boolean existsInState();

   StateInterface getState();

   int getSectorId();

   void calcWorldTransformRelative(int var1, Vector3i var2);

   Transform getClientTransform();

   Transform getClientTransformCenterOfMass(Transform var1);

   Vector3f getCenterOfMass(Vector3f var1);

   AbstractOwnerState getOwnerState();

   Vector3f getLinearVelocity(Vector3f var1);

   boolean isInPhysics();

   boolean isHidden();

   int getAsTargetId();

   byte getTargetType();

   Transform getWorldTransformOnClient();

   void transformAimingAt(Vector3f var1, Damager var2, SimpleGameObject var3, Random var4, float var5);

   int getFactionId();

   long getOwnerId();
}
