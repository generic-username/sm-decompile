package org.schema.game.common.data.player;

import com.bulletphysics.linearmath.Transform;
import java.io.IOException;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Quat4Util;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.LightTransformable;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.Light;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.container.TransformTimed;
import org.schema.schine.network.objects.remote.RemoteTransformation;

public class BuildModePosition implements LightTransformable {
   private Transform buildModePositionQueue;
   private PlayerState playerState;
   private final Transform lastPosition = new Transform();
   private final Transform currentPosition = new Transform();
   private final Transform currentServerTrans = new Transform();
   private final Transform clientTrans = new Transform();
   private boolean serverReceived = true;
   private boolean toSendLastPosition;
   private float accTime;
   private float lerpSpeed = 4.0F;
   private boolean wasDisplayed;
   private Light light;

   public BuildModePosition(PlayerState var1) {
      this.playerState = var1;
      this.lastPosition.setIdentity();
      this.currentServerTrans.setIdentity();
      this.clientTrans.setIdentity();
      this.currentPosition.setIdentity();
   }

   public void update(Timer var1) {
      if (this.playerState.isClientOwnPlayer()) {
         this.playerState.getNetworkObject().isInBuildMode.forceClientUpdates();
         SimpleTransformableSendableObject var2;
         if ((var2 = this.playerState.getFirstControlledTransformableWOExc()) instanceof SegmentController && ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().isInAnyStructureBuildMode()) {
            SegmentController var4 = (SegmentController)var2;
            this.playerState.getNetworkObject().isInBuildMode.set(true);
            TransformTimed var6 = Controller.getCamera().getWorldTransform();
            Transform var3;
            (var3 = new Transform(var4.getWorldTransformInverse())).mul(var6);
            if (!this.lastPosition.basis.epsilonEquals(var3.basis, 0.001F) || !this.lastPosition.origin.epsilonEquals(var3.origin, 0.001F)) {
               this.lastPosition.set(var3);
               this.toSendLastPosition = true;
            }
         } else {
            this.playerState.getNetworkObject().isInBuildMode.set(false);
         }
      } else {
         Transform var7;
         if (this.isOnServer()) {
            if (this.buildModePositionQueue != null) {
               var7 = this.buildModePositionQueue;
               this.currentServerTrans.set(var7);
               this.serverReceived = true;
               this.buildModePositionQueue = null;
            }
         } else if (this.playerState.isClientOwnPlayer()) {
            this.buildModePositionQueue = null;
         } else {
            if (this.buildModePositionQueue != null) {
               var7 = this.buildModePositionQueue;
               this.buildModePositionQueue = null;
               this.currentPosition.set(this.clientTrans);
               this.lastPosition.set(var7);
               this.accTime = 0.0F;
            }

            this.accTime += var1.getDelta() * this.lerpSpeed;
            if (!this.isDisplayed()) {
               this.wasDisplayed = false;
            }

            if (!this.wasDisplayed) {
               this.accTime = 1.0F;
               this.wasDisplayed = true;
            }

            if (this.accTime < 1.0F) {
               Vector3f var8 = new Vector3f();
               Vector3fTools.lerp(this.currentPosition.origin, this.lastPosition.origin, this.accTime, var8);
               this.currentPosition.origin.set(var8);
               Quat4f var5 = new Quat4f();
               Quat4f var10 = Quat4fTools.set(this.currentPosition.basis, new Quat4f());
               Quat4f var9 = Quat4fTools.set(this.lastPosition.basis, new Quat4f());
               Quat4Util.slerp(var10, var9, this.accTime, var5);
               this.currentPosition.basis.set(var5);
            } else {
               this.currentPosition.set(this.lastPosition);
            }

            this.clientTrans.set(this.currentPosition);
         }
      }

      if (this.playerState.isClientOwnPlayer() && EngineSettings.CAMERA_DRONE_FLASHLIGHT_ON.isOn() != this.playerState.getNetworkObject().isBuildModeSpotlight.getBoolean()) {
         this.playerState.getNetworkObject().isBuildModeSpotlight.set(EngineSettings.CAMERA_DRONE_FLASHLIGHT_ON.isOn(), true);
      }

   }

   public TransformTimed getWorldTransform() {
      SimpleTransformableSendableObject var1;
      TransformTimed var2;
      SegmentController var3;
      if (this.isOnServer()) {
         if ((var1 = this.playerState.getFirstControlledTransformableWOExc()) instanceof SegmentController) {
            var3 = (SegmentController)var1;
            (var2 = new TransformTimed(var3.getWorldTransform())).mul(this.currentServerTrans);
            return var2;
         } else {
            System.err.println("[SERVER][ERROR][BUILDPOS] Error. not in entity: " + this.playerState);
            (var2 = new TransformTimed()).setIdentity();
            return var2;
         }
      } else if (this.playerState.isClientOwnPlayer()) {
         return Controller.getCamera().getWorldTransform();
      } else if ((var1 = this.playerState.getFirstControlledTransformableWOExc()) instanceof SegmentController) {
         var3 = (SegmentController)var1;
         (var2 = new TransformTimed(var3.getWorldTransformOnClient())).mul(this.clientTrans);
         return var2;
      } else {
         System.err.println("[CLIENT][ERROR][BUILDPOS] Error. not in entity: " + this.playerState);
         (var2 = new TransformTimed()).setIdentity();
         return var2;
      }
   }

   public boolean isDisplayed() {
      GameClientState var1 = (GameClientState)this.getState();
      return (!this.playerState.isClientOwnPlayer() || this.isOwnVisible()) && this.playerState.getNetworkObject().isInBuildMode.getBoolean() && var1.getController().isNeighborToClientSector(this.playerState.getSectorId());
   }

   public void updateToNetworkObject() {
      if (this.isOnServer() && this.serverReceived) {
         this.playerState.getNetworkObject().buildModePositionBuffer.add(new RemoteTransformation(this.currentServerTrans, this.playerState.getNetworkObject()));
         this.serverReceived = false;
      } else {
         if (this.toSendLastPosition) {
            this.playerState.getNetworkObject().buildModePositionBuffer.add(new RemoteTransformation(this.lastPosition, this.playerState.getNetworkObject()));
            this.toSendLastPosition = false;
         }

      }
   }

   public boolean isOnServer() {
      return this.playerState.isOnServer();
   }

   public StateInterface getState() {
      return this.playerState.getState();
   }

   public void updateToFullNetworkObject() {
      this.updateToNetworkObject();
   }

   public void initFromNetworkObject() {
      this.updateFromNetworkObject();
   }

   public void updateFromNetworkObject() {
      for(int var1 = 0; var1 < this.playerState.getNetworkObject().buildModePositionBuffer.getReceiveBuffer().size(); ++var1) {
         RemoteTransformation var2 = (RemoteTransformation)this.playerState.getNetworkObject().buildModePositionBuffer.getReceiveBuffer().get(var1);
         this.buildModePositionQueue = (Transform)var2.get();
      }

   }

   public TransformTimed getWorldTransformOnClient() {
      return this.getWorldTransform();
   }

   public Light getLight() {
      return this.light;
   }

   public void setLight(Light var1) {
      this.light = var1;
   }

   public AbstractOwnerState getOwnerState() {
      return this.playerState;
   }

   public boolean isSpotLightOn() {
      if (this.playerState.isClientOwnPlayer()) {
         return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().isInAnyStructureBuildMode() && this.playerState.getNetworkObject().isBuildModeSpotlight.getBoolean();
      } else {
         return this.isDisplayed() && this.playerState.getNetworkObject().isBuildModeSpotlight.getBoolean();
      }
   }

   public void setOwnVisible(boolean var1) {
      EngineSettings.CAMERA_DRONE_OWN_VISIBLE.setCurrentState(var1);

      try {
         EngineSettings.write();
      } catch (IOException var2) {
         var2.printStackTrace();
      }
   }

   public boolean isOwnVisible() {
      return EngineSettings.CAMERA_DRONE_OWN_VISIBLE.isOn();
   }

   public void setFlashlightOnClient(boolean var1) {
      EngineSettings.CAMERA_DRONE_FLASHLIGHT_ON.setCurrentState(var1);
      this.playerState.getNetworkObject().isBuildModeSpotlight.set(var1, true);

      try {
         EngineSettings.write();
      } catch (IOException var2) {
         var2.printStackTrace();
      }
   }

   public boolean isFlashlightOn() {
      return this.playerState.getNetworkObject().isBuildModeSpotlight.getBoolean();
   }

   public boolean isClientOwnPlayer() {
      return this.playerState.isClientOwnPlayer();
   }

   public boolean isDrawNames() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getBuildToolsManager().isCameraDroneDisplayName();
   }
}
