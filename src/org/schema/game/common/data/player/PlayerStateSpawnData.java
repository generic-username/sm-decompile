package org.schema.game.common.data.player;

import com.bulletphysics.linearmath.Transform;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class PlayerStateSpawnData {
   private static final byte VERSION = 1;
   private static int spawnPointRotationId;
   public final List hosts = new ArrayList();
   private final PlayerState playerState;
   public long thisLogin = 0L;
   public long lastLogin = 0L;
   public long lastLogout = 0L;
   public PlayerStateSpawnData.Spawn deathSpawn = new PlayerStateSpawnData.Spawn();
   public PlayerStateSpawnData.Spawn logoutSpawn = new PlayerStateSpawnData.Spawn();
   public Vector3i preSpecialSector = null;
   public Transform preSpecialSectorTransform = null;

   public PlayerStateSpawnData(PlayerState var1) {
      this.playerState = var1;
      this.thisLogin = System.currentTimeMillis();
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2;
      (Byte)(var2 = (Tag[])var1.getValue())[0].getValue();
      this.deathSpawn.fromTagStructure(var2[1]);
      this.logoutSpawn.fromTagStructure(var2[2]);
      if (var2.length > 3 && var2[3].getType() == Tag.Type.VECTOR3f) {
         if (this.preSpecialSectorTransform == null) {
            this.preSpecialSectorTransform = new Transform();
         }

         this.preSpecialSectorTransform.origin.set((Vector3f)var2[3].getValue());
      }

      if (var2.length > 4 && var2[4].getType() == Tag.Type.VECTOR3i) {
         if (this.preSpecialSector == null) {
            this.preSpecialSector = new Vector3i();
         }

         this.preSpecialSector.set((Vector3i)var2[4].getValue());
      }

   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)1), this.deathSpawn.toTagStructure(), this.logoutSpawn.toTagStructure(), this.preSpecialSectorTransform == null ? new Tag(Tag.Type.BYTE, (String)null, (byte)0) : new Tag(Tag.Type.VECTOR3f, "pretutpoint", this.preSpecialSectorTransform.origin), this.preSpecialSector == null ? new Tag(Tag.Type.BYTE, (String)null, (byte)0) : new Tag(Tag.Type.VECTOR3i, "pretutsector", this.preSpecialSector), FinishTag.INST});
   }

   public void fromNetworkObject() {
   }

   public void updateToNetworkObject() {
   }

   public void updateToFullNetworkObject() {
   }

   public void initFromNetworkObject() {
   }

   public void updateLocal(Timer var1) {
      if (this.playerState.isOnServer()) {
         this.setLogoutSpawnToPlayerPos();
      }

   }

   public Vector3f[] getDefaultSpawnPoints() {
      return new Vector3f[]{new Vector3f((Float)ServerConfig.DEFAULT_SPAWN_POINT_X_1.getCurrentState(), (Float)ServerConfig.DEFAULT_SPAWN_POINT_Y_1.getCurrentState(), (Float)ServerConfig.DEFAULT_SPAWN_POINT_Z_1.getCurrentState()), new Vector3f((Float)ServerConfig.DEFAULT_SPAWN_POINT_X_2.getCurrentState(), (Float)ServerConfig.DEFAULT_SPAWN_POINT_Y_2.getCurrentState(), (Float)ServerConfig.DEFAULT_SPAWN_POINT_Z_2.getCurrentState()), new Vector3f((Float)ServerConfig.DEFAULT_SPAWN_POINT_X_3.getCurrentState(), (Float)ServerConfig.DEFAULT_SPAWN_POINT_Y_3.getCurrentState(), (Float)ServerConfig.DEFAULT_SPAWN_POINT_Z_3.getCurrentState()), new Vector3f((Float)ServerConfig.DEFAULT_SPAWN_POINT_X_4.getCurrentState(), (Float)ServerConfig.DEFAULT_SPAWN_POINT_Y_4.getCurrentState(), (Float)ServerConfig.DEFAULT_SPAWN_POINT_Z_4.getCurrentState())};
   }

   public void setForNewPlayerOnLogin() throws IOException {
      System.out.println("[SERVER] Not Found Saved State: " + this.playerState + " : UID " + this.playerState.getUniqueIdentifier());
      Sector var1 = ((GameServerState)this.playerState.getState()).getUniverse().getSector(new Vector3i((Integer)ServerConfig.DEFAULT_SPAWN_SECTOR_X.getCurrentState(), (Integer)ServerConfig.DEFAULT_SPAWN_SECTOR_Y.getCurrentState(), (Integer)ServerConfig.DEFAULT_SPAWN_SECTOR_Z.getCurrentState()));
      Vector3f[] var2 = this.getDefaultSpawnPoints();
      Vector3f var3 = new Vector3f(var2[spawnPointRotationId]);
      spawnPointRotationId = (spawnPointRotationId + 1) % 4;
      if (ServerConfig.USE_PERSONAL_SECTORS.isOn()) {
         this.usePersonalSector(this.playerState, var3);
      } else {
         this.deathSpawn.localPos.set(var3);
         this.deathSpawn.absolutePosBackup.set(var3);
         this.deathSpawn.absoluteSector.set(var1.pos);
         this.logoutSpawn.localPos.set(var3);
         this.logoutSpawn.absolutePosBackup.set(var3);
         this.logoutSpawn.absoluteSector.set(var1.pos);
         this.playerState.setCurrentSectorId(var1.getId());
         this.playerState.getCurrentSector().set(var1.pos);
      }
   }

   public void setFromLoadedSpawnPoint() throws IOException {
      if (ServerConfig.USE_PERSONAL_SECTORS.isOn()) {
         this.usePersonalSector(this.playerState, new Vector3f(0.0F, 0.0F, 0.0F));
      }

      Sector var1 = this.getSpawnSector(false);
      this.playerState.setCurrentSectorId(var1.getId());
      this.playerState.setCurrentSector(var1.pos);
   }

   private void usePersonalSector(PlayerState var1, Vector3f var2) throws IOException {
      System.err.println("[SERVER] using personal sector for player " + var1 + " -> " + var1.personalSector);
      this.deathSpawn.localPos.set(var2);
      this.deathSpawn.absolutePosBackup.set(var2);
      Sector var3;
      (var3 = ((GameServerState)this.playerState.getState()).getUniverse().getSector(new Vector3i(var1.personalSector.x, var1.personalSector.y, var1.personalSector.z))).noEnter(true);
      var3.noExit(true);
      var1.setCurrentSectorId(var3.getId());
      var1.getCurrentSector().set(var3.pos);
      this.deathSpawn.absoluteSector.set(var3.pos);
   }

   public void onLoggedOut() {
      this.setLogoutSpawnToPlayerPos();
   }

   public void setDeathSpawnToPlayerPos() {
      this.setSpawnToPlayerPos(this.deathSpawn);
   }

   public void setLogoutSpawnToPlayerPos() {
      this.setSpawnToPlayerPos(this.logoutSpawn);
   }

   public void setDeathSpawnTo(SegmentController var1, Vector3i var2) {
      this.setSpawnTo(this.deathSpawn, var1, var2);
   }

   public void setSpawnTo(PlayerStateSpawnData.Spawn var1, SegmentController var2, Vector3i var3) {
      var1.localPos.set((float)(var3.x - 16), (float)(var3.y - 16), (float)(var3.z - 16));
      var1.UID = var2.getUniqueIdentifier();
      var1.absolutePosBackup.set(var2.getWorldTransform().origin);
      this.playerState.sendServerMessagePlayerInfo(new Object[]{406, var2.toNiceString()});
   }

   public void setSpawnToPlayerPos(PlayerStateSpawnData.Spawn var1) {
      SimpleTransformableSendableObject var2;
      if ((var2 = this.playerState.getFirstControlledTransformableWOExc()) != null) {
         if (var2 instanceof SegmentController) {
            Object var3;
            if ((var3 = ((ControllerStateUnit)this.playerState.getControllerState().getUnits().iterator().next()).parameter) != null && var3 instanceof Vector3i) {
               Vector3i var4 = (Vector3i)var3;
               var1.localPos.set((float)(var4.x - 16), (float)(var4.y - 16), (float)(var4.z - 16));
               var1.UID = var2.getUniqueIdentifier();
            }
         } else if (var2 instanceof PlayerCharacter) {
            if (((PlayerCharacter)var2).getGravity().source != null) {
               SimpleTransformableSendableObject var5 = ((PlayerCharacter)var2).getGravity().source;
               var1.UID = var5.getUniqueIdentifier();
               var1.localPos.set(((PlayerCharacter)var2).getWorldTransform().origin);
               Transform var6;
               (var6 = new Transform(var5.getWorldTransform())).inverse();
               var6.transform(var1.localPos);
            } else {
               var1.UID = "";
               var1.localPos.set(((PlayerCharacter)var2).getWorldTransform().origin);
            }
         }

         var1.absoluteSector.set(this.playerState.getCurrentSector());
         var1.absolutePosBackup.set(var2.getWorldTransform().origin);
      }

   }

   public void setSpawnGravity(PlayerCharacter var1, boolean var2) {
      this.getSpawn(var2).setSpawnGravity(var1);
   }

   public PlayerStateSpawnData.Spawn getSpawn(boolean var1) {
      return var1 ? this.deathSpawn : this.logoutSpawn;
   }

   public void onSpawnPlayerCharacter(PlayerCharacter var1, boolean var2) throws IOException {
      GameServerState var3 = (GameServerState)this.playerState.getState();
      PlayerStateSpawnData.Spawn var6 = this.getSpawn(var2);
      Sector var4 = var3.getUniverse().getSector(var6.absoluteSector);
      var1.setSectorId(var4.getId());
      if (!this.playerState.getCurrentSector().equals(var4.pos)) {
         System.err.println("[SERVER] spawn player character: doing sector switch to " + var4.pos);
         this.playerState.setCurrentSector(var4.pos);
         this.playerState.setCurrentSectorId(var4.getId());
      }

      var1.getInitialTransform().setIdentity();
      Sendable var5;
      if ((var5 = (Sendable)var3.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var6.UID)) != null && var5 instanceof SimpleTransformableSendableObject) {
         Sector var7;
         if ((var7 = var3.getUniverse().getSector(var5.getId())) != null && var7 != var4) {
            this.playerState.setCurrentSector(var7.pos);
            this.playerState.setCurrentSectorId(var7.getId());
         }

         SimpleTransformableSendableObject var8 = (SimpleTransformableSendableObject)var5;
         Vector3f var9 = new Vector3f(var6.localPos);
         var1.spawnOnObjectId = var5.getId();
         var1.spawnOnObjectLocalPos = new Vector3f(var6.localPos);
         var8.getWorldTransform().transform(var9);
         var1.getInitialTransform().origin.set(var9);
         System.err.println("[SERVER][SPAWN] " + this.playerState + "; Relative: " + var8 + " -> " + var9 + "; objOrigin: " + var8.getWorldTransform().origin);
      } else {
         var1.getInitialTransform().origin.set(var6.localPos);
         System.err.println("[SERVER][SPAWN] " + this.playerState + "; ABSOLUTE :::: " + var6.localPos);
      }

      this.playerState.spawnData.setSpawnGravity(var1, this.playerState.spawnedOnce);
   }

   public Sector getSpawnSector(boolean var1) throws IOException {
      GameServerState var2 = (GameServerState)this.playerState.getState();
      PlayerStateSpawnData.Spawn var6;
      if (Sector.isTutorialSector((var6 = this.getSpawn(var1)).absoluteSector)) {
         var6.UID = "";
         if (this.preSpecialSector != null && this.preSpecialSectorTransform != null) {
            var6.absoluteSector.set(this.preSpecialSector);
            var6.localPos.set(this.preSpecialSectorTransform.origin);
            var6.absolutePosBackup.set(var6.localPos);
         } else {
            var6.absoluteSector.set((Integer)ServerConfig.DEFAULT_SPAWN_SECTOR_X.getCurrentState(), (Integer)ServerConfig.DEFAULT_SPAWN_SECTOR_Y.getCurrentState(), (Integer)ServerConfig.DEFAULT_SPAWN_SECTOR_Z.getCurrentState());
            Vector3f[] var3 = this.getDefaultSpawnPoints();
            var6.localPos.set(var3[spawnPointRotationId]);
            var6.absolutePosBackup.set(var6.localPos);
         }
      }

      Sector var7;
      Sendable var8;
      if ((var8 = (Sendable)var2.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var6.UID)) != null && var8 instanceof SimpleTransformableSendableObject) {
         SimpleTransformableSendableObject var10 = (SimpleTransformableSendableObject)var8;
         if ((var7 = var2.getUniverse().getSector(var10.getSectorId())) != null) {
            var6.absoluteSector.set(var7.pos);
         }

         System.err.println("[SERVER][PLAYERSTATE] Set spawn sector from loaded object: " + var7);
      } else {
         Vector3i var9 = null;

         try {
            if (var6.UID.length() > 0) {
               List var4;
               if ((var4 = var2.getDatabaseIndex().getTableManager().getEntityTable().getByUIDExact(DatabaseEntry.removePrefixWOException(var6.UID), -1)).size() == 1) {
                  var9 = new Vector3i(((DatabaseEntry)var4.get(0)).sectorPos);
               } else {
                  System.err.println("[SERVER][PLAYERSTATE] " + this + ": The ship the player logged out on does no longer exist! Spawning at last known position...");
                  this.playerState.sendServerMessagePlayerError(new Object[]{407});
                  var6.UID = "";
                  var6.localPos.set(var6.absolutePosBackup);
               }
            }
         } catch (SQLException var5) {
            var5.printStackTrace();
         }

         if (var9 == null) {
            var9 = new Vector3i(var6.absoluteSector);
         }

         var7 = var2.getUniverse().getSector(var9);
         var6.absoluteSector.set(var7.pos);
         System.err.println("[SERVER][PLAYERSTATE] Set spawn sector from unloaded object: " + var7);
      }

      return var7;
   }

   public void onSpawnPreparePlayerCharacter(boolean var1) throws IOException {
      GameServerState var2 = (GameServerState)this.playerState.getState();
      this.getSpawnSector(var1);
      var2.getSpawnRequestsReady().add(this.playerState);
   }

   public String toString() {
      return "PlayerStateSpawnData [thisLogin=" + this.thisLogin + ", lastLogin=" + this.lastLogin + ", lastLogout=" + this.lastLogout + ", deathSpawn=" + this.deathSpawn + ", logoutSpawn=" + this.logoutSpawn + "]";
   }

   public class Spawn {
      public String UID = "";
      public final Vector3i absoluteSector = new Vector3i();
      public final Vector3f localPos = new Vector3f();
      public final Vector3f absolutePosBackup = new Vector3f();
      public final Vector3f gravityAcceleration = new Vector3f();

      public void setSpawnGravity(PlayerCharacter var1) {
         GameServerState var2;
         Sendable var3;
         if ((var3 = (Sendable)(var2 = (GameServerState)PlayerStateSpawnData.this.playerState.getState()).getLocalAndRemoteObjectContainer().getUidObjectMap().get(this.UID)) != null && var3 instanceof SimpleTransformableSendableObject) {
            System.err.println("[SERVER][SPAWN] Gravity object to use for " + PlayerStateSpawnData.this.playerState + " set to: " + this.UID);
            Vector3f var4 = new Vector3f();
            if (var2.getCurrentGravitySources().contains(var3)) {
               var4.y = -9.89F;
            }

            var1.scheduleGravityServerForced(var4, (SimpleTransformableSendableObject)var3);
         } else {
            System.err.println("[SERVER][SPAWN] Gravity object to use for " + PlayerStateSpawnData.this.playerState + " not found. UID: " + this.UID);
         }
      }

      public Tag toTagStructure() {
         return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.STRING, (String)null, this.UID), new Tag(Tag.Type.VECTOR3i, (String)null, this.absoluteSector), new Tag(Tag.Type.VECTOR3f, (String)null, this.localPos), new Tag(Tag.Type.VECTOR3f, (String)null, this.gravityAcceleration), new Tag(Tag.Type.VECTOR3f, (String)null, this.absolutePosBackup), FinishTag.INST});
      }

      public void fromTagStructure(Tag var1) {
         Tag[] var2 = (Tag[])var1.getValue();
         this.UID = (String)var2[0].getValue();
         this.absoluteSector.set((Vector3i)var2[1].getValue());
         this.localPos.set((Vector3f)var2[2].getValue());
         this.gravityAcceleration.set((Vector3f)var2[3].getValue());
         if (var2.length > 4 && var2[4].getType() == Tag.Type.VECTOR3f) {
            this.absolutePosBackup.set((Vector3f)var2[4].getValue());
         }

      }

      public String toString() {
         return "Spawn [UID=" + this.UID + ", absoluteSector=" + this.absoluteSector + ", localPos=" + this.localPos + ", absolutePosBackup=" + this.absolutePosBackup + ", gravityAcceleration=" + this.gravityAcceleration + "]";
      }
   }

   public static enum SpawnType {
      ABSOLUTE,
      ON_ENTITY;
   }
}
