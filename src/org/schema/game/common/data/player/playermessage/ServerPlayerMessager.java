package org.schema.game.common.data.player.playermessage;

import java.sql.SQLException;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.PlayerNotFountException;

public class ServerPlayerMessager {
   private final GameServerState state;

   public ServerPlayerMessager(GameServerState var1) {
      this.state = var1;
   }

   public void send(String var1, String var2, String var3, String var4) {
      if (var2 != null && var2.trim().length() > 0) {
         try {
            PlayerState var5;
            if ((var5 = this.state.getPlayerFromName(var2)).getClientChannel() != null) {
               var5.getClientChannel().getPlayerMessageController().serverSend(var1, var2, var3, var4);
            }

            return;
         } catch (PlayerNotFountException var7) {
            try {
               System.err.println("[SERVER] Message to " + var2 + " couldnt be delivered and will be stored in the database");
               this.state.getDatabaseIndex().getTableManager().getPlayerMessagesTable().updateOrInsertMessage(PlayerMessageController.getNew(var1, var2, var3, var4));
               return;
            } catch (SQLException var6) {
               var6.printStackTrace();
            }
         }
      }

   }
}
