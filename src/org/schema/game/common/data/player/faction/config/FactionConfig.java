package org.schema.game.common.data.player.faction.config;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Field;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Locale;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.common.XMLTools;
import org.schema.common.config.ConfigParserException;
import org.schema.common.config.ConfigurationElement;
import org.schema.game.common.controller.elements.ReactorLevelCalcStyle;
import org.schema.game.common.controller.elements.UnitCalcStyle;
import org.schema.game.common.controller.elements.combination.Combinable;
import org.schema.game.common.data.blockeffects.BlockEffectTypes;
import org.schema.game.common.updater.FileUtil;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.server.ServerStateInterface;
import org.schema.schine.resource.FileExt;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public abstract class FactionConfig {
   public static final String factionConfigPath;
   public static final String factionModConfigPath;
   public static final String factionConfigPathDEFAULT;
   public static final String factionConfigPathHOWTO;
   public static ObjectOpenHashSet initializedServer;
   public static ObjectOpenHashSet initializedClient;
   private final boolean onServer;

   public FactionConfig(StateInterface var1) {
      this.onServer = var1 instanceof ServerStateInterface;
   }

   public static void load(StateInterface var0) throws SAXException, IOException, ParserConfigurationException, IllegalArgumentException, IllegalAccessException, ConfigParserException {
      try {
         load(var0, factionConfigPath);
      } catch (ConfigParserException var4) {
         var4.printStackTrace();
         FileExt var1 = new FileExt(factionConfigPath);
         FileExt var2 = new FileExt("." + File.separator + "customFactionConfig" + File.separator + "FactionConfigError.xml");
         FileUtil.copyFile(var1, var2);
         var1.delete();
         (new FileExt(factionModConfigPath)).delete();
         FileUtil.copyFile(new FileExt(factionConfigPathDEFAULT), new FileExt(factionConfigPath));
         load(var0, factionConfigPath);
         if (var0 instanceof GameServerState) {
            try {
               ((GameServerState)var0).setFactionConfigCheckSum(FileUtil.getSha1Checksum("./data/config/FactionConfig.xml"));
               RandomAccessFile var5;
               byte[] var6 = new byte[(int)(var5 = new RandomAccessFile("./data/config/FactionConfig.xml", "r")).length()];
               var5.read(var6);
               var5.close();
               ((GameServerState)var0).setFactionConfigFile(var6);
               return;
            } catch (NoSuchAlgorithmException var3) {
               var3.printStackTrace();
            }
         }

      }
   }

   public static void load(StateInterface var0, String var1) throws SAXException, IOException, ParserConfigurationException, IllegalArgumentException, IllegalAccessException, ConfigParserException {
      ObjectArrayList var2;
      (var2 = new ObjectArrayList(10)).add(new FactionActivityConfig(var0));
      var2.add(new FactionPointGalaxyConfig(var0));
      var2.add(new FactionPointIncomeConfig(var0));
      var2.add(new FactionPointSpendingConfig(var0));
      var2.add(new FactionPointsGeneralConfig(var0));
      var2.add(new FactionSystemOwnerBonusConfig(var0));

      assert (new FileExt(var1)).exists();

      Document var3 = XMLTools.loadXML(new FileExt(var1));

      for(int var4 = 0; var4 < var2.size(); ++var4) {
         ((FactionConfig)var2.get(var4)).parse(var3);
      }

   }

   public boolean isOnServer() {
      return this.onServer;
   }

   protected abstract String getTag();

   public void parse(Document var1) throws IllegalArgumentException, IllegalAccessException, ConfigParserException {
      if (this.isOnServer() && !initializedServer.contains(this.getClass()) || !this.isOnServer() && !initializedClient.contains(this.getClass())) {
         NodeList var21 = var1.getDocumentElement().getChildNodes();
         Field[] var2 = this.getClass().getDeclaredFields();
         boolean var3 = false;
         ObjectOpenHashSet var4 = new ObjectOpenHashSet();

         int var8;
         for(int var5 = 0; var5 < var21.getLength(); ++var5) {
            Node var6;
            if ((var6 = var21.item(var5)).getNodeType() == 1 && var6.getNodeName().toLowerCase(Locale.ENGLISH).equals(this.getTag().toLowerCase(Locale.ENGLISH))) {
               NodeList var7 = var6.getChildNodes();
               var3 = true;

               for(var8 = 0; var8 < var7.getLength(); ++var8) {
                  Node var9;
                  if ((var9 = var7.item(var8)).getNodeType() == 1) {
                     if (!var9.getNodeName().toLowerCase(Locale.ENGLISH).equals("basicvalues")) {
                        if (!var9.getNodeName().toLowerCase(Locale.ENGLISH).equals("combination")) {
                           throw new ConfigParserException("[CustomFactionConfig] tag \"" + var6.getNodeName() + " -> " + var9.getNodeName() + "\" unknown in this context (has to be either \"BasicValues\" or \"Combinable\")");
                        }

                        if (!(this instanceof Combinable)) {
                           throw new ConfigParserException(var6.getNodeName() + " -> " + var9.getNodeName() + " class is not combinable " + this + ", but has combinable tag");
                        }

                        ((Combinable)this).getAddOn().parse(var9);
                     } else {
                        NodeList var10 = var9.getChildNodes();

                        for(int var11 = 0; var11 < var10.getLength(); ++var11) {
                           Node var12;
                           if ((var12 = var10.item(var11)).getNodeType() == 1) {
                              boolean var13 = false;
                              boolean var14 = false;
                              Field[] var15 = var2;
                              int var16 = var2.length;

                              for(int var17 = 0; var17 < var16; ++var17) {
                                 Field var18;
                                 (var18 = var15[var17]).setAccessible(true);
                                 ConfigurationElement var19;
                                 if ((var19 = (ConfigurationElement)var18.getAnnotation(ConfigurationElement.class)) != null) {
                                    var13 = true;
                                    if (var19.name().toLowerCase(Locale.ENGLISH).equals(var12.getNodeName().toLowerCase(Locale.ENGLISH))) {
                                       try {
                                          if (var18.getType() == Boolean.TYPE) {
                                             var18.setBoolean(this, Boolean.parseBoolean(var12.getTextContent()));
                                             var14 = true;
                                          } else if (var18.getType() == Integer.TYPE) {
                                             var18.setInt(this, Integer.parseInt(var12.getTextContent()));
                                             var14 = true;
                                          } else if (var18.getType() == Short.TYPE) {
                                             var18.setShort(this, Short.parseShort(var12.getTextContent()));
                                             var14 = true;
                                          } else if (var18.getType() == Byte.TYPE) {
                                             var18.setByte(this, Byte.parseByte(var12.getTextContent()));
                                             var14 = true;
                                          } else if (var18.getType() == Float.TYPE) {
                                             var18.setFloat(this, Float.parseFloat(var12.getTextContent()));
                                             var14 = true;
                                          } else if (var18.getType() == Double.TYPE) {
                                             var18.setDouble(this, Double.parseDouble(var12.getTextContent()));
                                             var14 = true;
                                          } else if (var18.getType() == Long.TYPE) {
                                             var18.setLong(this, Long.parseLong(var12.getTextContent()));
                                             var14 = true;
                                          } else if (var18.getType().equals(BlockEffectTypes.class)) {
                                             BlockEffectTypes var26;
                                             if ((var26 = BlockEffectTypes.valueOf(var12.getTextContent())) == null) {
                                                throw new ConfigParserException("[CustomFactionConfig] Cannot parse enum field: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType() + "; enum unkown (possible: " + Arrays.toString(BlockEffectTypes.values()) + ")");
                                             }

                                             var18.set(this, var26);
                                             var14 = true;
                                          } else if (var18.getType().equals(UnitCalcStyle.class)) {
                                             UnitCalcStyle var27;
                                             if ((var27 = UnitCalcStyle.valueOf(var12.getTextContent())) == null) {
                                                throw new ConfigParserException("[CustomFactionConfig] Cannot parse enum field: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType() + "; enum unkown (possible: " + Arrays.toString(UnitCalcStyle.values()) + ")");
                                             }

                                             var18.set(this, var27);
                                             var14 = true;
                                          } else if (var18.getType().equals(ReactorLevelCalcStyle.class)) {
                                             ReactorLevelCalcStyle var28;
                                             if ((var28 = ReactorLevelCalcStyle.valueOf(var12.getTextContent())) == null) {
                                                throw new ConfigParserException("[CustomFactionConfig] Cannot parse enum field: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType() + "; enum unkown (possible: " + Arrays.toString(UnitCalcStyle.values()) + ")");
                                             }

                                             var18.set(this, var28);
                                             var14 = true;
                                          } else {
                                             if (!var18.getType().equals(String.class)) {
                                                throw new ConfigParserException("[CustomFactionConfig] Cannot parse field: " + var18.getName() + "; " + var18.getType());
                                             }

                                             String var29 = var12.getTextContent().replaceAll("\\r\\n|\\r|\\n", "").replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r").replaceAll("\\\\t", "\t");
                                             var18.set(this, var29);
                                             var14 = true;
                                          }
                                       } catch (NumberFormatException var20) {
                                          throw new ConfigParserException("[CustomFactionConfig] Cannot parse field: " + var18.getName() + "; " + var18.getType() + "; with " + var12.getTextContent(), var20);
                                       }
                                    }

                                    if (var14) {
                                       var4.add(var18);
                                       break;
                                    }
                                 }
                              }

                              if (var13 && !var14) {
                                 throw new ConfigParserException(var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": No appropriate field found for tag: " + var12.getNodeName());
                              }
                           }
                        }
                     }
                  }
               }
            }
         }

         if (!var3) {
            throw new ConfigParserException("[CustomFactionConfig] Tag \"" + this.getTag() + "\" not found in configuation. Please create it (case insensitive)");
         } else {
            this.getClass().getAnnotations();
            Field[] var22 = var2;
            int var23 = var2.length;

            for(var8 = 0; var8 < var23; ++var8) {
               Field var24;
               (var24 = var22[var8]).setAccessible(true);
               ConfigurationElement var25;
               if ((var25 = (ConfigurationElement)var24.getAnnotation(ConfigurationElement.class)) != null && !var4.contains(var24)) {
                  throw new ConfigParserException("[CustomFactionConfig] virtual field " + var24.getName() + " (" + var25.name() + ") not found. Please define a tag \"" + var25.name() + "\" inside the <BasicValues> of \"" + this.getTag() + "\"");
               }
            }

            if (this.isOnServer()) {
               initializedServer.add(this.getClass());
            } else {
               initializedClient.add(this.getClass());
            }
         }
      } else {
         if (this.isOnServer()) {
            assert initializedServer.size() > 0;
         } else {
            assert initializedClient.size() > 0;
         }

      }
   }

   static {
      factionConfigPath = "." + File.separator + "data" + File.separator + "config" + File.separator + "FactionConfig.xml";
      factionModConfigPath = "." + File.separator + "customFactionConfig" + File.separator + "FactionConfig.xml";
      factionConfigPathDEFAULT = "." + File.separator + "customFactionConfig" + File.separator + "FactionConfigTemplate.xml";
      factionConfigPathHOWTO = "." + File.separator + "data" + File.separator + "config" + File.separator + "customFactionConfigHOWTO.txt";
      initializedServer = new ObjectOpenHashSet();
      initializedClient = new ObjectOpenHashSet();
   }
}
