package org.schema.game.common.data.player.faction.config;

import org.schema.common.config.ConfigurationElement;
import org.schema.schine.network.StateInterface;

public class FactionPointsGeneralConfig extends FactionConfig {
   @ConfigurationElement(
      name = "FactionPointDeathProtectionMinutes"
   )
   public static float FACTION_POINT_DEATH_PROTECTION_MIN = 0.0F;
   @ConfigurationElement(
      name = "IncomeExpensePeriodMinutes"
   )
   public static float INCOME_EXPENSE_PERIOD_MINUTES = 3.0F;
   @ConfigurationElement(
      name = "InitialFactionPoints"
   )
   public static float INITIAL_FACTION_POINTS = 0.0F;

   public FactionPointsGeneralConfig(StateInterface var1) {
      super(var1);
   }

   protected String getTag() {
      return "FactionPointGeneral";
   }
}
