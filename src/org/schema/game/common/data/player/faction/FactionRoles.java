package org.schema.game.common.data.player.faction;

import java.util.Arrays;
import org.schema.common.util.StringTools;
import org.schema.schine.common.language.Lng;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class FactionRoles implements TagSerializable {
   public static final int ROLE_COUNT = 5;
   public static final byte INDEX_DEFAULT_ROLE = 0;
   public static final byte INDEX_ADMIN_ROLE = 4;
   public static final int PERSONAL_RANK = -1;
   public static final int NOT_SET_RANK = -2;
   public static final byte LOWEST_RANK = 0;
   public int factionId;
   public int senderId = -1;
   private FactionRole[] roles;

   public FactionRoles() {
      this.roles = new FactionRole[]{new FactionRole(0L, Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONROLES_0, 0), new FactionRole(0L, Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONROLES_1, 1), new FactionRole(0L, Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONROLES_2, 2), new FactionRole(0L, Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONROLES_3, 3), new FactionRole(FactionPermission.ADMIN_PERMISSIONS, Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONROLES_4, 4)};
   }

   public static String getRoleName(byte var0) {
      switch(var0) {
      case -2:
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONROLES_5;
      case -1:
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONROLES_6;
      case 4:
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONROLES_7;
      default:
         return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONROLES_8, 4 - var0);
      }
   }

   public void apply(FactionRoles var1) {
      for(int var2 = 0; var2 < 5; ++var2) {
         assert this.factionId == var1.factionId;

         this.roles[var2].name = var1.roles[var2].name;
         this.roles[var2].role = var1.roles[var2].role;
      }

   }

   public void fromTagStructure(Tag var1) {
      if (!var1.getName().equals("0")) {
         assert false;

      } else {
         Tag[] var4 = (Tag[])var1.getValue();
         this.factionId = (Integer)var4[0].getValue();
         Tag[] var2 = (Tag[])var4[1].getValue();
         var4 = (Tag[])var4[2].getValue();

         for(int var3 = 0; var3 < 5; ++var3) {
            this.getRoles()[var3].role = (Long)var2[var3].getValue();
            this.getRoles()[var3].name = (String)var4[var3].getValue();
         }

         this.getRoles()[4].role &= ~FactionPermission.PermType.KICK_ON_FRIENDLY_FIRE.value;
      }
   }

   public Tag toTagStructure() {
      Tag var1 = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.LONG, (String)null, this.getRoles()[0].role), new Tag(Tag.Type.LONG, (String)null, this.getRoles()[1].role), new Tag(Tag.Type.LONG, (String)null, this.getRoles()[2].role), new Tag(Tag.Type.LONG, (String)null, this.getRoles()[3].role), new Tag(Tag.Type.LONG, (String)null, this.getRoles()[4].role), FinishTag.INST});
      Tag var2 = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.STRING, (String)null, this.getRoles()[0].name), new Tag(Tag.Type.STRING, (String)null, this.getRoles()[1].name), new Tag(Tag.Type.STRING, (String)null, this.getRoles()[2].name), new Tag(Tag.Type.STRING, (String)null, this.getRoles()[3].name), new Tag(Tag.Type.STRING, (String)null, this.getRoles()[4].name), FinishTag.INST});
      return new Tag(Tag.Type.STRUCT, "0", new Tag[]{new Tag(Tag.Type.INT, (String)null, this.factionId), var1, var2, FinishTag.INST});
   }

   public FactionRole[] getRoles() {
      return this.roles;
   }

   public boolean hasRelationshipPermission(int var1) {
      return (this.roles[var1].role & FactionPermission.PermType.RELATIONSHIPS_EDIT.value) == FactionPermission.PermType.RELATIONSHIPS_EDIT.value;
   }

   public boolean hasInvitePermission(int var1) {
      return (this.roles[var1].role & FactionPermission.PermType.INVITE_PERMISSION.value) == FactionPermission.PermType.INVITE_PERMISSION.value;
   }

   public boolean hasClaimSystemPermission(int var1) {
      return (this.roles[var1].role & FactionPermission.PermType.MAY_CLAIM_SYSTEM.value) == FactionPermission.PermType.MAY_CLAIM_SYSTEM.value;
   }

   public boolean hasKickPermission(int var1) {
      return (this.roles[var1].role & FactionPermission.PermType.KICK_PERMISSION.value) == FactionPermission.PermType.KICK_PERMISSION.value;
   }

   public boolean hasDescriptionAndNewsPostPermission(int var1) {
      return (this.roles[var1].role & FactionPermission.PermType.NEWS_POST_PERMISSION.value) == FactionPermission.PermType.NEWS_POST_PERMISSION.value;
   }

   public boolean hasHomebasePermission(int var1) {
      return (this.roles[var1].role & FactionPermission.PermType.HOMEBASE_PERMISSION.value) == FactionPermission.PermType.HOMEBASE_PERMISSION.value;
   }

   public boolean hasKickOnFriendlyFire(int var1) {
      return (this.roles[var1].role & FactionPermission.PermType.KICK_ON_FRIENDLY_FIRE.value) == FactionPermission.PermType.KICK_ON_FRIENDLY_FIRE.value;
   }

   public boolean hasPermissionEditPermission(int var1) {
      return (this.roles[var1].role & FactionPermission.PermType.FACTION_EDIT_PERMISSION.value) == FactionPermission.PermType.FACTION_EDIT_PERMISSION.value;
   }

   public boolean hasFogOfWarPermission(int var1) {
      return (this.roles[var1].role & FactionPermission.PermType.FOG_OF_WAR_SHARE.value) == FactionPermission.PermType.FOG_OF_WAR_SHARE.value;
   }

   public boolean hasPermission(int var1, FactionPermission.PermType var2) {
      return (this.roles[var1].role & var2.value) == var2.value;
   }

   public void setPermission(int var1, FactionPermission.PermType var2, boolean var3) {
      if (var3) {
         this.roles[var1].role |= var2.value;
      } else {
         this.roles[var1].role &= ~var2.value;
      }
   }

   public String toString() {
      return "FactionRoles(Faction " + this.factionId + ")(sendId " + this.senderId + ")[" + Arrays.toString(this.roles) + "]";
   }
}
