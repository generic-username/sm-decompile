package org.schema.game.common.data.player.faction;

import java.util.Locale;
import javax.vecmath.Vector3f;
import org.schema.common.util.TranslatableEnum;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteIntegerArray;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class FactionRelation implements TagSerializable {
   public int a;
   public int b;
   public byte rel;

   public FactionRelation() {
   }

   public FactionRelation(int var1, int var2, byte var3) {
      this.set(var1, var2);
      this.rel = var3;
   }

   public static long getCode(int var0, int var1) {
      int var2 = Math.min(Math.abs(var0), Math.abs(var1));
      var0 = Math.max(Math.abs(var0), Math.abs(var1));
      return (long)var2 * 2147483647L + (long)var0;
   }

   public static byte getRelationFromString(String var0) {
      if (var0.toLowerCase(Locale.ENGLISH).equals("enemy")) {
         return FactionRelation.RType.ENEMY.code;
      } else if (!var0.toLowerCase(Locale.ENGLISH).equals("fiend") && !var0.toLowerCase(Locale.ENGLISH).equals("ally")) {
         if (var0.toLowerCase(Locale.ENGLISH).equals("neutral")) {
            return FactionRelation.RType.NEUTRAL.code;
         } else {
            throw new IllegalArgumentException();
         }
      } else {
         return FactionRelation.RType.FRIEND.code;
      }
   }

   public int hashCode() {
      return this.a + this.b * 100000 + this.rel * 100023;
   }

   public boolean equals(Object var1) {
      if (var1 != null && var1 instanceof FactionRelation) {
         FactionRelation var2 = (FactionRelation)var1;
         return this.a == var2.a && this.b == var2.b && this.rel == var2.rel;
      } else {
         return false;
      }
   }

   public String toString() {
      return "Rel[a=" + this.a + ", b=" + this.b + ", rel=" + this.getRelation().name() + "]";
   }

   public boolean contains(int var1) {
      return this.a == var1 || this.b == var1;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.a = (Integer)var2[0].getValue();
      this.b = (Integer)var2[1].getValue();
      this.rel = (Byte)var2[2].getValue();
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.INT, (String)null, this.a), new Tag(Tag.Type.INT, (String)null, this.b), new Tag(Tag.Type.BYTE, (String)null, this.rel), FinishTag.INST});
   }

   public long getCode() {
      return getCode(this.a, this.b);
   }

   public FactionRelation.RType getRelation() {
      return FactionRelation.RType.values()[this.rel];
   }

   public void setRelation(byte var1) {
      this.rel = var1;
   }

   public RemoteIntegerArray getRemoteArray(NetworkObject var1) {
      RemoteIntegerArray var2;
      (var2 = new RemoteIntegerArray(3, var1)).set(0, (Integer)this.a);
      var2.set(1, (Integer)this.b);
      var2.set(2, (Integer)Integer.valueOf(this.rel));
      return var2;
   }

   public boolean isEnemy() {
      return this.rel == FactionRelation.RType.ENEMY.code;
   }

   public boolean isFriend() {
      return this.rel == FactionRelation.RType.FRIEND.code;
   }

   public boolean isNeutral() {
      return this.rel == FactionRelation.RType.NEUTRAL.code;
   }

   public void set(int var1, int var2) {
      assert var1 != var2;

      assert var1 != 0 && var2 != 0;

      this.a = Math.min(var1, var2);
      this.b = Math.max(var1, var2);
   }

   public void setEnemy() {
      this.rel = FactionRelation.RType.ENEMY.code;
   }

   public void setFriend() {
      this.rel = FactionRelation.RType.FRIEND.code;
   }

   public void setNeutral() {
      this.rel = FactionRelation.RType.NEUTRAL.code;
   }

   public static enum RType implements TranslatableEnum {
      NEUTRAL(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONRELATION_0;
         }
      }, 0, new Vector3f(0.5F, 0.7F, 0.9F), (byte)0),
      ENEMY(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONRELATION_1;
         }
      }, -1, new Vector3f(1.0F, 0.0F, 0.0F), (byte)1),
      FRIEND(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONRELATION_2;
         }
      }, 1, new Vector3f(0.0F, 1.0F, 0.0F), (byte)2);

      private final Translatable name;
      public final int sortWeight;
      public final Vector3f defaultColor;
      public final byte code;

      private RType(Translatable var3, int var4, Vector3f var5, byte var6) {
         this.name = var3;
         this.sortWeight = var4;
         this.defaultColor = var5;
         this.code = var6;
      }

      public final String getName() {
         return this.name.getName(this);
      }
   }

   public static enum AttackType {
      NONE(0),
      OWNER(1),
      NEUTRAL(2),
      ENEMY(4),
      FACTION(8),
      ALLY(16);

      private static int all = 0;
      public final int code;

      public static int get(FactionRelation.AttackType... var0) {
         int var1 = 0;
         int var2 = (var0 = var0).length;

         for(int var3 = 0; var3 < var2; ++var3) {
            FactionRelation.AttackType var4 = var0[var3];
            var1 |= var4.code;
         }

         return var1;
      }

      public static int getAll() {
         return all;
      }

      public static boolean isAttacking(int var0, FactionRelation.AttackType var1) {
         return (var0 & var1.code) == var1.code;
      }

      private AttackType(int var3) {
         this.code = var3;
      }

      static {
         FactionRelation.AttackType[] var0;
         int var1 = (var0 = values()).length;

         for(int var2 = 0; var2 < var1; ++var2) {
            FactionRelation.AttackType var3 = var0[var2];
            all |= var3.code;
         }

      }
   }
}
