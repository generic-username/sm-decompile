package org.schema.game.common.data.player.faction;

public interface FactionAddCallback {
   void callback();
}
