package org.schema.game.common.data.player.faction;

import java.util.Locale;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.config.FactionActivityConfig;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class FactionPermission implements TagSerializable {
   public static final long ADMIN_PERMISSIONS;
   public static final long DEFAULT_PERMISSION = 0L;
   public String playerUID;
   public byte role;
   public long activeMemberTime;
   public long lastSeenTime;
   public Vector3i lastSeenPosition;

   public FactionPermission() {
      this.lastSeenPosition = new Vector3i(0, 0, 0);
   }

   public FactionPermission(FactionPermission var1) {
      this(var1.playerUID, var1.role, var1.activeMemberTime);
   }

   public FactionPermission(PlayerState var1, byte var2, long var3) {
      this.lastSeenPosition = new Vector3i(0, 0, 0);
      this.playerUID = var1.getName();
      this.role = var2;
      this.activeMemberTime = var3;
   }

   public FactionPermission(String var1, byte var2, long var3) {
      this.lastSeenPosition = new Vector3i(0, 0, 0);
      this.playerUID = var1;
      this.role = var2;
      this.activeMemberTime = var3;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.playerUID = (String)var2[0].getValue();
      this.role = (Byte)var2[1].getValue();
      if (var2[2].getType() != Tag.Type.FINISH) {
         this.activeMemberTime = (Long)var2[2].getValue();
      }

      if (var2.length > 3 && var2[3].getType() != Tag.Type.FINISH) {
         this.lastSeenPosition.set((Vector3i)var2[3].getValue());
      }

      if (var2.length > 4 && var2[4].getType() != Tag.Type.FINISH) {
         this.lastSeenTime = (Long)var2[4].getValue();
      }

   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.STRING, (String)null, this.playerUID), new Tag(Tag.Type.BYTE, (String)null, this.role), new Tag(Tag.Type.LONG, (String)null, this.activeMemberTime), new Tag(Tag.Type.VECTOR3i, (String)null, this.lastSeenPosition), new Tag(Tag.Type.LONG, (String)null, this.lastSeenTime), FinishTag.INST});
   }

   public int hashCode() {
      return this.playerUID.toLowerCase(Locale.ENGLISH).hashCode();
   }

   public boolean equals(Object var1) {
      return this.playerUID.toLowerCase(Locale.ENGLISH).equals(((FactionPermission)var1).playerUID.toLowerCase(Locale.ENGLISH));
   }

   public String toString() {
      return "FactionPermission [playerUID=" + this.playerUID + ", roleID=" + this.role + "]";
   }

   public boolean hasRelationshipPermission(Faction var1) {
      return var1.getRoles().hasRelationshipPermission(this.role);
   }

   public boolean hasInvitePermission(Faction var1) {
      return var1.getRoles().hasInvitePermission(this.role);
   }

   public boolean hasClaimSystemPermission(Faction var1) {
      return var1.getRoles().hasClaimSystemPermission(this.role);
   }

   public boolean hasHomebasePermission(Faction var1) {
      return var1.getRoles().hasHomebasePermission(this.role);
   }

   public boolean hasKickPermission(Faction var1) {
      return var1.getRoles().hasKickPermission(this.role);
   }

   public boolean hasDescriptionAndNewsPostPermission(Faction var1) {
      return var1.getRoles().hasDescriptionAndNewsPostPermission(this.role);
   }

   public boolean hasPermissionEditPermission(Faction var1) {
      return var1.getRoles().hasPermissionEditPermission(this.role);
   }

   public boolean hasFogOfWarPermission(Faction var1) {
      return var1.getRoles().hasFogOfWarPermission(this.role);
   }

   public void setRole(byte var1, boolean var2) {
      this.role = var1;
   }

   public boolean isActiveMember() {
      return (float)(System.currentTimeMillis() - this.activeMemberTime) < FactionActivityConfig.SET_INACTIVE_AFTER_HOURS * 60.0F * 60.0F * 1000.0F;
   }

   public String toString(Faction var1) {
      return "FactionPermission for " + var1 + " [playerUID=" + this.playerUID + ", roleID=" + this.role + " claim " + this.hasClaimSystemPermission(var1) + ", kick " + this.hasKickPermission(var1) + ", descNews " + this.hasDescriptionAndNewsPostPermission(var1) + ", invite " + this.hasInvitePermission(var1) + ", editPerms " + this.hasPermissionEditPermission(var1) + ", relationship " + this.hasRelationshipPermission(var1) + ", homebase " + this.hasHomebasePermission(var1) + "]";
   }

   public boolean isFounder(Faction var1) {
      return this.role >= var1.getRoles().getRoles().length - 1;
   }

   public boolean isOverInactiveLimit(GameStateInterface var1) {
      return System.currentTimeMillis() - this.lastSeenTime > var1.getGameState().getFactionKickInactiveTimeLimitMs();
   }

   static {
      ADMIN_PERMISSIONS = FactionPermission.PermType.RELATIONSHIPS_EDIT.value | FactionPermission.PermType.KICK_PERMISSION.value | FactionPermission.PermType.INVITE_PERMISSION.value | FactionPermission.PermType.FACTION_EDIT_PERMISSION.value | FactionPermission.PermType.MAY_CLAIM_SYSTEM.value | FactionPermission.PermType.HOMEBASE_PERMISSION.value | FactionPermission.PermType.NEWS_POST_PERMISSION.value | FactionPermission.PermType.FOG_OF_WAR_SHARE.value | FactionPermission.PermType.UNDEF5.value | FactionPermission.PermType.UNDEF6.value;
   }

   public static enum PermType {
      RELATIONSHIPS_EDIT(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONPERMISSION_0;
         }
      }, 1L, true),
      KICK_PERMISSION(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONPERMISSION_1;
         }
      }, 2L, true),
      INVITE_PERMISSION(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONPERMISSION_2;
         }
      }, 4L, true),
      FACTION_EDIT_PERMISSION(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONPERMISSION_3;
         }
      }, 8L, true),
      KICK_ON_FRIENDLY_FIRE(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONPERMISSION_4;
         }
      }, 16L, true),
      MAY_CLAIM_SYSTEM(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONPERMISSION_5;
         }
      }, 32L, true),
      HOMEBASE_PERMISSION(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONPERMISSION_6;
         }
      }, 64L, true),
      NEWS_POST_PERMISSION(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONPERMISSION_7;
         }
      }, 128L, true),
      FOG_OF_WAR_SHARE(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONPERMISSION_8;
         }
      }, 256L, true),
      UNDEF5(Translatable.DEFAULT, 512L, false),
      UNDEF6(Translatable.DEFAULT, 1024L, false);

      private final Translatable name;
      public final long value;
      public final boolean active;

      private PermType(Translatable var3, long var4, boolean var6) {
         this.name = var3;
         this.value = var4;
         this.active = var6;
      }

      public final String getName() {
         return this.name.getName(this);
      }
   }
}
