package org.schema.game.common.data.player.faction;

import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class FactionNewsPost implements Comparable, TagSerializable {
   private int factionId;
   private String op;
   private long date;
   private String message;
   private int permission;
   private String topic = "No Topic";
   private boolean delete;

   public int compareTo(FactionNewsPost var1) {
      return (int)(this.date - var1.date);
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      if ("fp-v0".equals(var1.getName())) {
         this.factionId = (Integer)var2[0].getValue();
         this.op = (String)var2[1].getValue();
         this.date = (Long)var2[2].getValue();
         this.message = (String)var2[3].getValue();
         this.permission = (Integer)var2[4].getValue();
         if (var2[5].getType() != Tag.Type.FINISH) {
            this.topic = (String)var2[5].getValue();
            return;
         }
      } else {
         assert false : var1.getName();
      }

   }

   public Tag toTagStructure() {
      Tag var1 = new Tag(Tag.Type.INT, "id", this.factionId);
      Tag var2 = new Tag(Tag.Type.STRING, "op", this.op);
      Tag var3 = new Tag(Tag.Type.LONG, "dt", this.date);
      Tag var4 = new Tag(Tag.Type.STRING, "msg", this.message);
      Tag var5 = new Tag(Tag.Type.INT, "perm", this.permission);
      Tag var6 = new Tag(Tag.Type.STRING, "top", this.topic);
      return new Tag(Tag.Type.STRUCT, "fp-v0", new Tag[]{var1, var2, var3, var4, var5, var6, FinishTag.INST});
   }

   public long getDate() {
      return this.date;
   }

   public int getFactionId() {
      return this.factionId;
   }

   public String getMessage() {
      return this.message;
   }

   public String getOp() {
      return this.op;
   }

   public int getPermission() {
      return this.permission;
   }

   public int hashCode() {
      return (int)((long)this.factionId + this.date + (long)this.op.hashCode() + (long)this.message.hashCode());
   }

   public boolean equals(Object var1) {
      FactionNewsPost var2 = (FactionNewsPost)var1;
      return this.factionId == var2.factionId && this.date == var2.date && this.op.equals(var2.op) && this.message.equals(var2.message);
   }

   public String toString() {
      return "FactionNewsPost [factionId=" + this.factionId + ", op=" + this.op + ", date=" + this.date + ", message=" + this.message.replaceAll("\n", " ") + ", permission=" + this.permission + "]";
   }

   public void set(int var1, String var2, long var3, String var5, String var6, int var7, boolean var8) {
      this.factionId = var1;
      this.op = var2;
      this.date = var3;
      this.message = var6;
      this.permission = var7;
      this.topic = var5;
      this.setDelete(var8);
   }

   public void set(int var1, String var2, long var3, String var5, String var6, int var7) {
      this.set(var1, var2, var3, var5, var6, var7, false);
   }

   public String getTopic() {
      return this.topic;
   }

   public boolean isDelete() {
      return this.delete;
   }

   public void setDelete(boolean var1) {
      this.delete = var1;
   }
}
