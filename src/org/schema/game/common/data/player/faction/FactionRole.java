package org.schema.game.common.data.player.faction;

public class FactionRole {
   public final int index;
   public long role;
   public String name;

   public FactionRole(long var1, String var3, int var4) {
      this.role = var1;
      this.name = var3;
      this.index = var4;
   }

   public boolean hasPermission(FactionPermission.PermType var1) {
      return (this.role & var1.value) == var1.value;
   }

   public void setPermission(FactionPermission.PermType var1, boolean var2) {
      if (var2) {
         this.role |= var1.value;
      } else {
         this.role &= ~var1.value;
      }
   }

   public int hashCode() {
      return this.index;
   }

   public boolean equals(Object var1) {
      return var1 instanceof FactionRole && this.index == ((FactionRole)var1).index;
   }

   public String getRoleString() {
      StringBuffer var1 = new StringBuffer();
      FactionPermission.PermType[] var2 = FactionPermission.PermType.values();

      for(int var3 = 0; var3 < var2.length; ++var3) {
         FactionPermission.PermType var4 = var2[var3];
         if (this.hasPermission(var4)) {
            var1.append("+" + var4.name());
         } else {
            var1.append("-" + var4.name());
         }

         if (var3 < var2.length - 1) {
            var1.append(", ");
         }
      }

      return var1.toString();
   }

   public String toString() {
      return "FactionRole [index=" + this.index + ", role=" + this.getRoleString() + ", name=" + this.name + "]";
   }
}
