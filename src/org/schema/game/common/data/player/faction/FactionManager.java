package org.schema.game.common.data.player.faction;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap.Entry;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import org.schema.common.LogUtil;
import org.schema.common.config.ConfigParserException;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.data.SendableGameState;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.config.FactionConfig;
import org.schema.game.common.data.player.faction.config.FactionPointsGeneralConfig;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.network.objects.NetworkGameState;
import org.schema.game.network.objects.remote.RemoteFaction;
import org.schema.game.network.objects.remote.RemoteFactionInvitation;
import org.schema.game.network.objects.remote.RemoteFactionNewsPost;
import org.schema.game.network.objects.remote.RemoteFactionPointUpdate;
import org.schema.game.network.objects.remote.RemoteFactionRoles;
import org.schema.game.network.objects.remote.RemoteSimpelCommand;
import org.schema.game.network.objects.remote.RemoteSystemOwnershipChange;
import org.schema.game.network.objects.remote.SimpleCommand;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.controller.SectorListener;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.game.server.data.ServerConfig;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.game.server.data.simulation.npc.NPCFactionConfig;
import org.schema.game.server.data.simulation.npc.NPCFactionControlCommand;
import org.schema.game.server.data.simulation.npc.NPCFactionPresetManager;
import org.schema.game.server.data.simulation.npc.diplomacy.DiplomacyAction;
import org.schema.game.server.data.simulation.npc.diplomacy.NPCDiplomacy;
import org.schema.game.server.data.simulation.npc.news.NPCFactionNews;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.objects.remote.RemoteArray;
import org.schema.schine.network.objects.remote.RemoteField;
import org.schema.schine.network.objects.remote.RemoteIntegerArray;
import org.schema.schine.network.objects.remote.RemoteStringArray;
import org.schema.schine.network.server.ServerMessage;
import org.schema.schine.resource.DiskWritable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class FactionManager extends Observable implements SectorListener, DiskWritable {
   public static final int CODE_MIN_LENGTH = 6;
   public static final int CODE_MAX_LENGTH = 24;
   public static final int PIRATES_ID = -1;
   public static final int TRAIDING_GUILD_ID = -2;
   public static final int[] FAUNA_GROUP_ENEMY = new int[]{-1000, -1001, -1002, -1003, -1004, -1005, -1006, -1007};
   public static final int[] FAUNA_GROUP_NEUTRAL = new int[]{-2000, -2001, -2002, -2003, -2004, -2005, -2006, -2007};
   public static final int NPC_FACTION_START = -10000000;
   public static final int NPC_FACTION_END = -10000;
   public static final int OTHER_POWER0 = -3;
   public static final int OTHER_POWER1 = -4;
   public static final int OTHER_POWER2 = -5;
   public static final int OTHER_POWER3 = -6;
   public static final int OTHER_POWER4 = -7;
   public static final int OTHER_POWER5 = -8;
   public static final int OTHER_POWER6 = -9;
   public static final int OTHER_POWER7 = -10;
   public static final int OTHER_POWER8 = -11;
   public static final int OTHER_POWER9 = -12;
   public static final String[] RESERVED_CODES = new String[]{"pirates", "traiding guild", "NEUTRAL"};
   public static final int ID_NEUTRAL = 0;
   private static final String factionVersion0 = "factions-v0";
   private static final String factionVersion1 = "factions-v1";
   private static final String factionVersion2 = "factions-v2";
   public static Random random = new Random();
   private static int FACTION_ID_GEN = 10000;
   final List toAddFactionNewsPosts = new ArrayList();
   private final Set factionInvitations = new HashSet();
   private final Int2ObjectMap news = new Int2ObjectOpenHashMap();
   private final Long2ObjectOpenHashMap relationShipOffers = new Long2ObjectOpenHashMap();
   private final Long2ObjectOpenHashMap relations = new Long2ObjectOpenHashMap();
   private final ArrayList relationsToAdd = new ArrayList();
   private final ArrayList relationOffersToAdd = new ArrayList();
   private final ObjectArrayFIFOQueue relationRolesToMod = new ObjectArrayFIFOQueue();
   private final ArrayList factionHomebaseToMod = new ArrayList();
   private final ArrayList factionSystemOwnerToMod = new ArrayList();
   private final Int2ObjectOpenHashMap factionMap = new Int2ObjectOpenHashMap();
   private final SendableGameState gameState;
   private final ArrayList toAddFactions = new ArrayList();
   private final ArrayList toDelFactions = new ArrayList();
   private final ArrayList toAddFactionRelationOfferAccepts = new ArrayList();
   private final ArrayList toAddFactionInvites = new ArrayList();
   private final ArrayList toDelFactionInvites = new ArrayList();
   private final ObjectArrayList toModPersonalEnemies = new ObjectArrayList();
   private final ArrayList toKickMember = new ArrayList();
   private final ArrayList changedFactions = new ArrayList();
   private final ArrayList changedMembersFactions = new ArrayList();
   private final ArrayList failedChangedMembersFactions = new ArrayList();
   private final ObjectArrayFIFOQueue simpleCommandQueue = new ObjectArrayFIFOQueue();
   long lastupdate;
   private boolean flagCheckFactions;
   private boolean factionInvitationsChanged;
   private boolean changedFactionAspect;
   private boolean changedFactionNewsDeletedAspect;
   private boolean factionOffersChanged;
   private Object2IntOpenHashMap galaxyMapCounts = new Object2IntOpenHashMap();
   private ObjectArrayFIFOQueue toAddFactionPointMods = new ObjectArrayFIFOQueue();
   private boolean npcFactionChanged;
   private final ObjectArrayFIFOQueue turnSchedule = new ObjectArrayFIFOQueue();
   public NPCFactionPresetManager npcFactionPresetManager;
   private byte VERSION = 0;
   private final NPCFactionNews npcFactionNews;
   public int otherFaction;
   public String otherPlayer;
   public long otherDbId;
   private IntOpenHashSet actionCheck = new IntOpenHashSet();
   public final Set markedChangedContingentFactions = new ObjectOpenHashSet();
   public ObjectOpenHashSet needsSendAll = new ObjectOpenHashSet();
   public ObjectOpenHashSet diplomacyChanged = new ObjectOpenHashSet();
   private boolean wasNpcDebugMode;
   public int currentFactionIdCreator = -10000000;
   private NPCFaction currentTurn;
   private long lastNPCFactionTurnUpdate;
   private Faction flagHomebaseChanged;

   public StateInterface getState() {
      return this.gameState.getState();
   }

   public FactionManager(SendableGameState var1) {
      this.gameState = var1;
      this.npcFactionNews = new NPCFactionNews(this);

      try {
         FactionConfig.load(var1.getState());
      } catch (Exception var7) {
         throw new RuntimeException(var7);
      }

      if (var1.isOnServer()) {
         this.npcFactionPresetManager = new NPCFactionPresetManager();
         this.npcFactionPresetManager.readNpcPresets();

         try {
            Tag var8 = ((GameServerState)var1.getState()).getController().readEntity("FACTIONS", "fac");
            this.fromTagStructure(var8);
         } catch (IOException var4) {
            var4.printStackTrace();
         } catch (EntityNotFountException var5) {
            System.err.println("[SERVER] NO FACTIONS FOUND ON DISK: " + var5.getMessage());
            this.initializeNewDefautFactionManager();
         } catch (FactionFileOutdatedException var6) {
            System.err.println("[SERVER] NO FACTIONS FOUND ON DISK (outdated): " + var6.getMessage());
            this.initializeNewDefautFactionManager();
         }

         if (!this.relations.containsKey(FactionRelation.getCode(-1, -2))) {
            this.relationsToAdd.add(new FactionRelation(-1, -2, FactionRelation.RType.ENEMY.code));
         }

         Faction var2;
         int var9;
         for(var9 = 0; var9 < FAUNA_GROUP_NEUTRAL.length; ++var9) {
            if (!this.factionMap.containsKey(FAUNA_GROUP_NEUTRAL[var9])) {
               (var2 = new Faction(this.getState(), FAUNA_GROUP_NEUTRAL[var9], "Neutral Fauna Fac " + var9, "A Neutral Fanua Faction")).setShowInHub(false);
               this.factionMap.put(var2.getIdFaction(), var2);
            }
         }

         for(var9 = 0; var9 < FAUNA_GROUP_ENEMY.length; ++var9) {
            if (!this.factionMap.containsKey(FAUNA_GROUP_ENEMY[var9])) {
               (var2 = new Faction(this.getState(), FAUNA_GROUP_ENEMY[var9], "Enemy Fauna Fac " + var9, "An Enemy Fanua Faction")).setAttackNeutral(true);
               var2.setShowInHub(false);
               this.factionMap.put(var2.getIdFaction(), var2);
            }

            Iterator var10 = this.factionMap.values().iterator();

            while(var10.hasNext()) {
               Faction var3;
               if ((var3 = (Faction)var10.next()).getIdFaction() > 0 && !this.relations.containsKey(FactionRelation.getCode(var3.getIdFaction(), FAUNA_GROUP_ENEMY[var9]))) {
                  this.relationsToAdd.add(new FactionRelation(var3.getIdFaction(), FAUNA_GROUP_ENEMY[var9], FactionRelation.RType.ENEMY.code));
               }
            }

            if (!this.relations.containsKey(FactionRelation.getCode(-1, FAUNA_GROUP_ENEMY[var9]))) {
               this.relationsToAdd.add(new FactionRelation(-1, FAUNA_GROUP_ENEMY[var9], FactionRelation.RType.ENEMY.code));
            }

            if (!this.relations.containsKey(FactionRelation.getCode(-2, FAUNA_GROUP_ENEMY[var9]))) {
               this.relationsToAdd.add(new FactionRelation(-2, FAUNA_GROUP_ENEMY[var9], FactionRelation.RType.ENEMY.code));
            }
         }
      }

   }

   public static synchronized int getNewId() {
      return FACTION_ID_GEN++;
   }

   public void updateFactionPoints() {
      GameServerState var1 = (GameServerState)this.getGameState().getState();
      this.galaxyMapCounts.clear();
      if ((float)(System.currentTimeMillis() - this.lastupdate) > FactionPointsGeneralConfig.INCOME_EXPENSE_PERIOD_MINUTES * 60.0F * 1000.0F) {
         long var2 = System.currentTimeMillis();
         System.err.println("[FACTIONMANAGER] MAKING FACTION TURN: " + new Date(this.lastupdate) + "; " + this.factionMap.size() + "; Turn: " + FactionPointsGeneralConfig.INCOME_EXPENSE_PERIOD_MINUTES);
         Iterator var4 = this.factionMap.values().iterator();

         while(var4.hasNext()) {
            Faction var5;
            if ((var5 = (Faction)var4.next()).getIdFaction() > 0) {
               var5.handleActivityServer(this);
               var5.handleFactionPointGainServer(this);
               var5.handleFactionPointExpensesServer(this, this.galaxyMapCounts);
               var5.hanldeDeficit(this, this.galaxyMapCounts);
               var5.sendFactionPointUpdate(this.getGameState());
            }
         }

         System.err.println("[FACTIONMANAGER] faction update took: " + (System.currentTimeMillis() - var2) + "ms");
         this.lastupdate = System.currentTimeMillis();
      }

      var1.getGameState().getNetworkObject().lastFactionPointTurn.set(this.lastupdate);
   }

   public void addFaction(Faction var1) {
      synchronized(this.toAddFactions) {
         this.toAddFactions.add(var1);
      }
   }

   public void addFactionInvitation(FactionInvite var1) {
      synchronized(this.toAddFactionInvites) {
         this.toAddFactionInvites.add(var1);
      }
   }

   private void checkFactions() {
      Iterator var1 = ((GameServerState)this.getGameState().getState()).getPlayerStatesByName().values().iterator();

      while(var1.hasNext()) {
         PlayerState var2 = (PlayerState)var1.next();
         Iterator var3 = this.getFactionCollection().iterator();

         while(var3.hasNext()) {
            Faction var4;
            if ((var4 = (Faction)var3.next()).getIdFaction() != var2.getFactionId() && var4.getMembersUID().containsKey(var2.getName())) {
               var4.removeMember(var2.getName(), this.getGameState());
               if (var4.getMembersUID().size() == 0 && !var4.isFactionMode(2) && !var4.isFactionMode(1) && !var4.isFactionMode(4)) {
                  this.removeFaction(var4);
               }
            }
         }
      }

   }

   public boolean existsFaction(int var1) {
      return this.factionMap.containsKey(var1);
   }

   public void flagCheckFactions() {
      this.flagCheckFactions = true;
   }

   private void fromRelationshipTag(Tag var1) {
      Tag[] var4 = (Tag[])var1.getValue();

      for(int var2 = 0; var2 < var4.length - 1; ++var2) {
         FactionRelation var3;
         (var3 = new FactionRelation()).fromTagStructure(var4[var2]);
         if (var3.a == var3.b) {
            System.err.println("[SERVER][FACTION][WARNING] not adding self-relation");
         } else if ((var3.a <= 0 || this.existsFaction(var3.a)) && (var3.b <= 0 || this.existsFaction(var3.b))) {
            this.relations.put(var3.getCode(), var3);
         } else {
            System.err.println("[SERVER][FACTION][WARNING] not adding faction relation for non existent faction: " + var3);
         }
      }

   }

   private void fromRelationshipOfferTag(Tag var1) {
      Tag[] var4 = (Tag[])var1.getValue();

      for(int var2 = 0; var2 < var4.length - 1; ++var2) {
         FactionRelationOffer var3;
         (var3 = new FactionRelationOffer()).fromTagStructure(var4[var2]);
         if ((var3.a <= 0 || this.existsFaction(var3.a)) && (var3.b <= 0 || this.existsFaction(var3.b))) {
            System.err.println("[SERVER][FACION][TAG] loaded relation offer " + var3);
            this.relationShipOffers.put(var3.getCode(), var3);
         } else {
            System.err.println("[SERVER][FACTION][WARNING] not adding faction relation offer for non existent faction: " + var3);
         }
      }

   }

   private void fromInvitesTag(Tag var1) {
      Tag[] var4 = (Tag[])var1.getValue();

      for(int var2 = 0; var2 < var4.length - 1; ++var2) {
         FactionInvite var3;
         (var3 = new FactionInvite()).fromTagStructure(var4[var2]);
         if ((Faction)this.factionMap.get(var3.getFactionUID()) != null) {
            this.factionInvitations.add(var3);
         }
      }

   }

   private void fromNewsTag(Tag var1) {
      Tag[] var8 = (Tag[])var1.getValue();

      for(int var2 = 0; var2 < var8.length - 1; ++var2) {
         if (var8[var2].getType() == Tag.Type.STRUCT) {
            Tag[] var3 = (Tag[])var8[var2].getValue();

            for(int var4 = 0; var4 < var3.length - 1; ++var4) {
               FactionNewsPost var5;
               (var5 = new FactionNewsPost()).fromTagStructure(var3[var4]);
               Faction var6;
               if ((var6 = (Faction)this.factionMap.get(var5.getFactionId())) != null && !var6.isNPC()) {
                  TreeSet var7;
                  if ((var7 = (TreeSet)this.getNews().get(var6.getIdFaction())) == null) {
                     var7 = new TreeSet();
                     this.getNews().put(var6.getIdFaction(), var7);
                  }

                  var7.add(var5);
               }
            }
         }
      }

      this.printNewsStats();
   }

   private void printNewsStats() {
      System.err.println("[SERVER][FACTION] News Statistics:");
      Iterator var1 = this.getNews().int2ObjectEntrySet().iterator();

      while(var1.hasNext()) {
         Entry var2 = (Entry)var1.next();
         System.err.println("[SERVER][FACTION][NEWS] " + this.getFaction(var2.getIntKey()) + ": Amount: " + ((TreeSet)var2.getValue()).size());
      }

   }

   public void diplomacyAction(DiplomacyAction.DiplActionType var1, int var2, long var3) {
      Faction var5;
      if ((var5 = (Faction)this.factionMap.get(var2)) instanceof NPCFaction) {
         ((NPCFaction)var5).diplomacyAction(var1, var3);
      }

   }

   public static Tag getNPCFactionConfigTagTag(Collection var0) {
      ObjectArrayList var1 = new ObjectArrayList();
      Iterator var4 = var0.iterator();

      while(var4.hasNext()) {
         Faction var2;
         if ((var2 = (Faction)var4.next()) instanceof NPCFaction) {
            var1.add((NPCFaction)var2);
         }
      }

      Tag[] var5;
      Tag[] var10000 = var5 = new Tag[var1.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;

      for(int var6 = 0; var6 < var1.size(); ++var6) {
         NPCFaction var3 = (NPCFaction)var1.get(var6);
         Tag var7 = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.INT, (String)null, var3.getIdFaction()), var3.getConfig().toTagStructure(), FinishTag.INST});
         var5[var6] = var7;
      }

      return new Tag(Tag.Type.STRUCT, (String)null, var5);
   }

   public static Int2ObjectOpenHashMap fromTagConfigs(Tag var0, NPCFactionPresetManager var1) throws IllegalArgumentException, IllegalAccessException, ConfigParserException {
      Int2ObjectOpenHashMap var2 = new Int2ObjectOpenHashMap();
      Tag[] var7 = var0.getStruct();

      for(int var3 = 0; var3 < var7.length - 1; ++var3) {
         Tag[] var4;
         int var5 = (var4 = var7[var3].getStruct())[0].getInt();
         NPCFactionConfig var6;
         (var6 = new NPCFactionConfig()).fromTagStructure(var4[1], var1);
         var6.initialize();
         var2.put(var5, var6);
      }

      return var2;
   }

   public void fromTagStructure(Tag var1) {
      if (!"factions-v2".equals(var1.getName())) {
         if (!"factions-v0".equals(var1.getName()) && !"factions-v1".equals(var1.getName())) {
            throw new FactionFileOutdatedException();
         } else {
            this.parseOldVersion(var1);
         }
      } else {
         new Int2ObjectOpenHashMap();
         Tag[] var14;
         (var14 = (Tag[])var1.getValue())[0].getByte();
         this.npcFactionPresetManager.fromTagStructure(var14[1]);
         Int2ObjectOpenHashMap var2 = new Int2ObjectOpenHashMap();

         try {
            var2 = fromTagConfigs(var14[2], this.npcFactionPresetManager);
         } catch (IllegalArgumentException var11) {
            var11.printStackTrace();
         } catch (IllegalAccessException var12) {
            var12.printStackTrace();
         } catch (ConfigParserException var13) {
            var13.printStackTrace();
         }

         Tag[] var3 = var14[3].getStruct();
         int var4 = FACTION_ID_GEN;

         for(int var5 = 0; var5 < var3.length - 1; ++var5) {
            Tag var6;
            int var8 = ((Tag[])(var6 = var3[var5]).getValue())[0].getInt();
            Tag var7;
            String var9 = (var7 = var6.getStruct()[1]).getStruct()[1].getString();
            Object var15;
            if (isNPCFaction(var8)) {
               ((Faction)(var15 = new NPCFaction(this.getState(), var8))).setName(var9);
               NPCFactionConfig var16 = (NPCFactionConfig)var2.get(var8);

               assert var16 != null : var8 + "; ::: " + var2;

               assert var16.getPreset() != null;

               var16.generate((NPCFaction)var15);
               ((NPCFaction)var15).setConfig(var16);

               assert ((NPCFaction)var15).getConfig() != null;

               assert ((NPCFaction)var15).getConfig() == var16;

               assert ((NPCFaction)var15).getConfig().getPreset() != null;
            } else {
               ((Faction)(var15 = new Faction(this.getState()))).setName(var9);
            }

            try {
               ((Faction)var15).initializeWithState((GameServerState)this.gameState.getState());
            } catch (Exception var10) {
               throw new RuntimeException(var10);
            }

            ((Faction)var15).fromTagStructure(var7);
            ((Faction)var15).setHomeParamsFromUID((GameServerState)this.gameState.getState());

            assert !this.getFactionMap().containsKey(((Faction)var15).getIdFaction());

            if (((Faction)var15).getMembersUID().isEmpty() && ((Faction)var15).getIdFaction() >= 0) {
               System.err.println("[SERVER][FACTION] not adding empty faction: " + var15 + ": " + ((Faction)var15).getMembersUID());
            } else {
               this.factionMap.put(((Faction)var15).getIdFaction(), var15);
               if (var15 instanceof NPCFaction) {
                  Inventory var17 = this.gameState.getInventories().put((long)((Faction)var15).getIdFaction(), ((NPCFaction)var15).getInventory());

                  assert var17 == null;

                  System.err.println("[SERVER][FACTION] LOADED NPC INV: " + var15 + " " + ((NPCFaction)var15).getInventory());
               }

               var4 = Math.max(var4, ((Faction)var15).getIdFaction());
            }

            assert !(var15 instanceof NPCFaction) || ((NPCFaction)var15).getConfig() != null : var15;

            assert !(var15 instanceof NPCFaction) || ((NPCFaction)var15).getConfig().getPreset() != null : var15;

            ((Faction)var15).initialize();
         }

         FACTION_ID_GEN = var4 + 1;
         this.fromInvitesTag(var14[4]);
         this.fromNewsTag(var14[5]);
         this.fromRelationshipTag(var14[6]);
         this.fromRelationshipOfferTag(var14[7]);
         this.lastupdate = (Long)var14[8].getValue();
         this.npcFactionNews.fromTag(var14[9]);
         if (var14[10].getType() != Tag.Type.FINISH) {
            this.currentFactionIdCreator = var14[10].getInt();
         }

      }
   }

   public Tag toTagStructure() {
      int var2 = 0;
      ObjectArrayList var4 = new ObjectArrayList();
      synchronized(this.factionMap) {
         var4.addAll(this.factionMap.values());
      }

      Tag[] var1 = new Tag[this.getFactionMap().size() + 1];
      Tag[] var3 = new Tag[this.getFactionMap().size() + 1];

      for(Iterator var5 = var4.iterator(); var5.hasNext(); ++var2) {
         Faction var6 = (Faction)var5.next();
         var1[var2] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.INT, (String)null, var6.getIdFaction()), var6.toTagStructure(), FinishTag.INST});
         TreeSet var7;
         if ((var7 = (TreeSet)this.getNews().get(var6.getIdFaction())) != null && !var6.isNPC()) {
            var3[var2] = Tag.listToTagStruct((Collection)var7, "FN");
         } else {
            var3[var2] = new Tag(Tag.Type.BYTE, "0FN", (byte)0);
         }
      }

      var1[this.getFactionMap().size()] = FinishTag.INST;
      var3[this.getFactionMap().size()] = FinishTag.INST;
      Tag[] var18;
      synchronized(this.relations) {
         var2 = 0;
         var18 = new Tag[this.relations.size() + 1];

         for(Iterator var20 = this.relations.values().iterator(); var20.hasNext(); ++var2) {
            FactionRelation var8 = (FactionRelation)var20.next();
            var18[var2] = var8.toTagStructure();
         }

         var18[this.relations.size()] = FinishTag.INST;
      }

      Tag[] var19;
      synchronized(this.getRelationShipOffers()) {
         var2 = 0;
         var19 = new Tag[this.getRelationShipOffers().size() + 1];
         Iterator var22 = this.getRelationShipOffers().values().iterator();

         while(true) {
            if (!var22.hasNext()) {
               var19[this.getRelationShipOffers().size()] = FinishTag.INST;
               break;
            }

            FactionRelationOffer var9 = (FactionRelationOffer)var22.next();
            var19[var2] = var9.toTagStructure();
            ++var2;
         }
      }

      Tag[] var21;
      synchronized(this.factionInvitations) {
         var21 = new Tag[this.factionInvitations.size() + 1];
         var2 = 0;
         Iterator var24 = this.factionInvitations.iterator();

         while(true) {
            if (!var24.hasNext()) {
               var21[this.factionInvitations.size()] = FinishTag.INST;
               break;
            }

            FactionInvite var10 = (FactionInvite)var24.next();
            var21[var2] = var10.toTagStructure();
            ++var2;
         }
      }

      Tag var23 = new Tag(Tag.Type.STRUCT, "NStruct", var3);
      Tag var25 = new Tag(Tag.Type.STRUCT, (String)null, var1);
      Tag var26 = new Tag(Tag.Type.STRUCT, (String)null, var21);
      Tag var15 = new Tag(Tag.Type.STRUCT, (String)null, var18);
      Tag var17 = new Tag(Tag.Type.STRUCT, (String)null, var19);
      Tag var16 = new Tag(Tag.Type.LONG, (String)null, this.lastupdate);
      return new Tag(Tag.Type.STRUCT, "factions-v2", new Tag[]{new Tag(Tag.Type.BYTE, (String)null, this.VERSION), this.npcFactionPresetManager.toTagStructure(), getNPCFactionConfigTagTag(var4), var25, var26, var23, var15, var17, var16, this.npcFactionNews.toTag(), new Tag(Tag.Type.INT, (String)null, this.currentFactionIdCreator), FinishTag.INST});
   }

   private void parseOldVersion(Tag var1) {
      Tag[] var2;
      Tag[] var3 = (Tag[])(var2 = (Tag[])var1.getValue())[0].getValue();
      int var4 = FACTION_ID_GEN;

      for(int var5 = 0; var5 < var3.length - 1; ++var5) {
         Tag var6 = var3[var5];
         Tag var7;
         Faction var10;
         if ("factions-v1".equals(var1.getName())) {
            int var8 = (Integer)((Tag[])var6.getValue())[0].getValue();
            var7 = ((Tag[])var6.getValue())[1];
            if (isNPCFaction(var8)) {
               continue;
            }

            var10 = new Faction(this.getState());
         } else {
            var7 = var6;
            var10 = new Faction(this.getState());
         }

         try {
            var10.initializeWithState((GameServerState)this.gameState.getState());
         } catch (Exception var9) {
            throw new RuntimeException(var9);
         }

         var10.fromTagStructure(var7);
         var10.setHomeParamsFromUID((GameServerState)this.gameState.getState());

         assert !this.getFactionMap().containsKey(var10.getIdFaction());

         if (var10.getMembersUID().isEmpty() && var10.getIdFaction() >= 0) {
            System.err.println("[SERVER][FACTION] not adding empty faction: " + var10 + ": " + var10.getMembersUID());
         } else {
            this.factionMap.put(var10.getIdFaction(), var10);
            if (var10 instanceof NPCFaction) {
               ((GameStateInterface)this.getState()).getGameState().getInventories().put((long)var10.getIdFaction(), ((NPCFaction)var10).getInventory());
            }

            var4 = Math.max(var4, var10.getIdFaction());
         }

         var10.initialize();
      }

      FACTION_ID_GEN = var4 + 1;
      this.fromInvitesTag(var2[1]);
      this.fromNewsTag(var2[2]);
      this.fromRelationshipTag(var2[3]);
      this.fromRelationshipOfferTag(var2[4]);
      if (var2[5].getType() != Tag.Type.FINISH) {
         this.lastupdate = (Long)var2[5].getValue();
      }

   }

   public Faction getFaction(int var1) {
      return (Faction)this.factionMap.get(var1);
   }

   public Collection getFactionCollection() {
      return this.factionMap.values();
   }

   public Set getFactionInvitations() {
      return this.factionInvitations;
   }

   public Int2ObjectOpenHashMap getFactionMap() {
      return this.factionMap;
   }

   public SendableGameState getGameState() {
      return this.gameState;
   }

   public Int2ObjectMap getNews() {
      return this.news;
   }

   public FactionRelation.RType getRelation(String var1, int var2, int var3) {
      Faction var4 = this.getFaction(var3);
      return var2 != var3 && var4 != null && var4.getPersonalEnemies().contains(var1.toLowerCase(Locale.ENGLISH)) ? FactionRelation.RType.ENEMY : this.getRelation(var2, var3);
   }

   public FactionRelation.RType getRelation(int var1, int var2) {
      if (var1 == 0 && var2 == 0) {
         return FactionRelation.RType.NEUTRAL;
      } else {
         Faction var5;
         if (var1 != 0 && var2 == 0) {
            if ((var5 = this.getFaction(var1)) != null) {
               if (var5.isFactionMode(4)) {
                  return FactionRelation.RType.NEUTRAL;
               } else if (var5.isAttackNeutral()) {
                  return FactionRelation.RType.ENEMY;
               } else {
                  return var5.isAllyNeutral() ? FactionRelation.RType.FRIEND : FactionRelation.RType.NEUTRAL;
               }
            } else {
               return FactionRelation.RType.NEUTRAL;
            }
         } else if (var2 != 0 && var1 == 0) {
            if ((var5 = this.getFaction(var2)) != null) {
               if (var5.isFactionMode(4)) {
                  return FactionRelation.RType.NEUTRAL;
               } else if (var5.isAttackNeutral()) {
                  return FactionRelation.RType.ENEMY;
               } else {
                  return var5.isAllyNeutral() ? FactionRelation.RType.FRIEND : FactionRelation.RType.NEUTRAL;
               }
            } else {
               return FactionRelation.RType.NEUTRAL;
            }
         } else if (var1 == var2) {
            return this.getFaction(var1) != null && this.getFaction(var1).isFactionMode(2) ? FactionRelation.RType.ENEMY : FactionRelation.RType.FRIEND;
         } else if ((this.getFaction(var1) == null || !this.getFaction(var1).isFactionMode(4)) && (this.getFaction(var2) == null || !this.getFaction(var2).isFactionMode(4))) {
            long var3 = FactionRelation.getCode(var1, var2);
            return !this.relations.containsKey(var3) ? FactionRelation.RType.NEUTRAL : ((FactionRelation)this.relations.get(var3)).getRelation();
         } else {
            return FactionRelation.RType.NEUTRAL;
         }
      }
   }

   public FactionRelation.RType getRelation(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2) {
      int var3;
      if (var1 instanceof PlayerControllable && !((PlayerControllable)var1).getAttachedPlayers().isEmpty()) {
         var3 = ((PlayerState)((PlayerControllable)var1).getAttachedPlayers().get(0)).getFactionId();
      } else {
         var3 = var1.getFactionId();
      }

      int var4;
      if (var2 instanceof PlayerControllable && !((PlayerControllable)var2).getAttachedPlayers().isEmpty()) {
         var4 = ((PlayerState)((PlayerControllable)var2).getAttachedPlayers().get(0)).getFactionId();
      } else {
         var4 = var2.getFactionId();
      }

      if (var1.getOwnerState() != null && this.existsFaction(var4) && this.getFaction(var4).getPersonalEnemies().contains(var1.getOwnerState().getName().toLowerCase(Locale.ENGLISH))) {
         return FactionRelation.RType.ENEMY;
      } else {
         return var2.getOwnerState() != null && this.existsFaction(var3) && this.getFaction(var3).getPersonalEnemies().contains(var2.getOwnerState().getName().toLowerCase(Locale.ENGLISH)) ? FactionRelation.RType.ENEMY : this.getRelation(var3, var4);
      }
   }

   public Long2ObjectOpenHashMap getRelationShipOffers() {
      return this.relationShipOffers;
   }

   public String getUniqueIdentifier() {
      return "FACTIONS";
   }

   public boolean isVolatile() {
      return false;
   }

   public void initFromNetworkObject(NetworkGameState var1) {
      this.npcFactionNews.initFromNetworkObject(var1);
   }

   public void initializeNewDefautFactionManager() {
      Faction var1;
      (var1 = new Faction(this.getState(), -1, Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONMANAGER_27, Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONMANAGER_28)).setAttackNeutral(true);
      Faction var2 = new Faction(this.getState(), -2, Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONMANAGER_29, Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONMANAGER_30);
      this.factionMap.put(var2.getIdFaction(), var2);
      this.factionMap.put(var1.getIdFaction(), var1);
      this.relationsToAdd.add(new FactionRelation(-1, -2, FactionRelation.RType.ENEMY.code));
   }

   private void initializeRelationShips(Faction var1) {
      if (var1.getIdFaction() != -1) {
         this.relationsToAdd.add(new FactionRelation(-1, var1.getIdFaction(), FactionRelation.RType.ENEMY.code));
      }

      for(int var2 = 0; var2 < FAUNA_GROUP_ENEMY.length; ++var2) {
         if (var1.getIdFaction() != FAUNA_GROUP_ENEMY[var2]) {
            this.relationsToAdd.add(new FactionRelation(FAUNA_GROUP_ENEMY[var2], var1.getIdFaction(), FactionRelation.RType.ENEMY.code));
         }
      }

   }

   public boolean isEnemy(int var1, AbstractOwnerState var2) {
      long var3 = FactionRelation.getCode(var1, var2.getFactionId());
      Faction var5;
      if (var1 == 0) {
         return (var5 = this.getFaction(var2.getFactionId())) != null && var5.isAttackNeutral();
      } else if (var2.getFactionId() != 0) {
         if (var1 == var2.getFactionId()) {
            return false;
         } else {
            return this.relations.containsKey(var3) && ((FactionRelation)this.relations.get(var3)).isEnemy();
         }
      } else {
         return (var5 = this.getFaction(var1)) != null && (var5.isAttackNeutral() || var5.getPersonalEnemies().contains(var2.getName().toLowerCase(Locale.ENGLISH)));
      }
   }

   public boolean isEnemy(int var1, int var2) {
      long var3 = FactionRelation.getCode(var1, var2);
      if (var1 != 0 && var2 != 0) {
         if (var1 == var2) {
            return false;
         } else {
            return this.relations.containsKey(var3) && ((FactionRelation)this.relations.get(var3)).isEnemy();
         }
      } else {
         Faction var5;
         return (var5 = this.getFaction(var1 == 0 ? var2 : var1)) != null && var5.isAttackNeutral();
      }
   }

   public boolean isEnemy(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2) {
      PlayerState var3 = null;
      PlayerState var4 = null;
      Faction var5 = null;
      Faction var6 = null;
      if (var1 instanceof PlayerControllable && !((PlayerControllable)var1).getAttachedPlayers().isEmpty()) {
         var3 = (PlayerState)((PlayerControllable)var1).getAttachedPlayers().get(0);
      }

      if (var2 instanceof PlayerControllable && !((PlayerControllable)var2).getAttachedPlayers().isEmpty()) {
         var4 = (PlayerState)((PlayerControllable)var2).getAttachedPlayers().get(0);
      }

      if (var3 != null) {
         var5 = this.getFaction(var3.getFactionId());
      }

      if (var4 != null) {
         var6 = this.getFaction(var4.getFactionId());
      }

      if (var5 == null) {
         var5 = this.getFaction(var1.getFactionId());
      }

      if (var6 == null) {
         var6 = this.getFaction(var2.getFactionId());
      }

      if (var6 != null && var6 != null && var5 == var6) {
         return false;
      } else if (var5 != null && var4 != null && var5.getPersonalEnemies().contains(var4.getName().toLowerCase(Locale.ENGLISH))) {
         return true;
      } else if (var6 != null && var3 != null && var6.getPersonalEnemies().contains(var3.getName().toLowerCase(Locale.ENGLISH))) {
         return true;
      } else {
         return this.getRelation(var1, var2) == FactionRelation.RType.ENEMY;
      }
   }

   public boolean isFriend(int var1, int var2) {
      long var3 = FactionRelation.getCode(var1, var2);
      if (var1 != 0 && var2 != 0) {
         if (var1 == var2) {
            return true;
         } else {
            return this.relations.containsKey(var3) && ((FactionRelation)this.relations.get(var3)).isFriend();
         }
      } else {
         Faction var5;
         return (var5 = this.getFaction(var1 == 0 ? var2 : var1)) != null && var5.isAllyNeutral();
      }
   }

   public boolean isFriend(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2) {
      return this.getRelation(var1, var2) == FactionRelation.RType.FRIEND;
   }

   public boolean isInSameFaction(FactionInterface var1, FactionInterface var2) {
      return var1.getFactionId() == var2.getFactionId();
   }

   public boolean isNeutral(int var1, int var2) {
      long var3 = FactionRelation.getCode(var1, var2);
      return !this.relations.containsKey(var3) || ((FactionRelation)this.relations.get(var3)).isNeutral();
   }

   public boolean isNeutral(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2) {
      return this.getRelation(var1, var2) == FactionRelation.RType.NEUTRAL;
   }

   public void relationShipOfferServer(FactionRelationOffer var1) {
      synchronized(this.relationOffersToAdd) {
         this.relationOffersToAdd.add(var1);
      }
   }

   public void removeFaction(Faction var1) {
      synchronized(this.toDelFactions) {
         this.toDelFactions.add(var1);
      }
   }

   public void removeFaction(int var1) throws FactionNotFoundException {
      Faction var2;
      if ((var2 = (Faction)this.factionMap.get(var1)) == null) {
         throw new FactionNotFoundException(var1);
      } else {
         if (this.isOnServer()) {
            Iterator var3 = ((GameServerState)this.getState()).getUniverse().getGalaxies().iterator();

            while(var3.hasNext()) {
               ((Galaxy)var3.next()).getNpcFactionManager().removeFaction(var2);
            }
         }

         this.removeFaction(var2);
      }
   }

   public void removeFactionInvitation(FactionInvite var1) {
      synchronized(this.toDelFactionInvites) {
         this.toDelFactionInvites.add(var1);
      }
   }

   public void removeFactionInvitationClient(FactionInvite var1) {
      this.gameState.getNetworkObject().factionInviteDel.add(new RemoteFactionInvitation(var1, this.gameState.getNetworkObject()));
   }

   public void removeMemberOfFaction(int var1, PlayerState var2) {
      assert this.getGameState().isOnServer();

      synchronized(this.changedMembersFactions) {
         this.changedMembersFactions.add(new FactionMemberMod(var1, var2.getName(), false));
      }
   }

   public void sendFactionRoles(FactionRoles var1) {
      this.getGameState().getNetworkObject().factionRolesBuffer.add(new RemoteFactionRoles(var1, this.getGameState().getNetworkObject()));
   }

   public void sendClientHomeBaseChange(String var1, int var2, String var3) {
      RemoteStringArray var4;
      (var4 = new RemoteStringArray(7, this.getGameState().getNetworkObject())).set(0, (String)var1);
      var4.set(1, (String)String.valueOf(var2));
      var4.set(2, (String)var3);
      this.getGameState().getNetworkObject().factionHomeBaseChangeBuffer.add((RemoteArray)var4);
   }

   public void sendHomeBaseChange(String var1, int var2, String var3, Vector3i var4, String var5) {
      RemoteStringArray var6;
      (var6 = new RemoteStringArray(7, this.getGameState().getNetworkObject())).set(0, (String)(new String(var1)));
      var6.set(1, (String)String.valueOf(var2));
      var6.set(2, (String)(new String(var3)));
      var6.set(3, (String)String.valueOf(var4.x));
      var6.set(4, (String)String.valueOf(var4.y));
      var6.set(5, (String)String.valueOf(var4.z));
      var6.set(6, (String)(new String(var5)));
      this.getGameState().getNetworkObject().factionHomeBaseChangeBuffer.add((RemoteArray)var6);
   }

   public void sendClientSystemOwnerChange(String var1, int var2, SimpleTransformableSendableObject var3) {
      Vector3i var4;
      if ((var4 = var3.getClientSector()) != null) {
         FactionSystemOwnerChange var5 = new FactionSystemOwnerChange(var1, var2, var3.getUniqueIdentifier(), var4, VoidSystem.getContainingSystem(var4, new Vector3i()), var3.getRealName());
         this.getGameState().getNetworkObject().factionClientSystemOwnerChangeBuffer.add(new RemoteSystemOwnershipChange(var5, this.getGameState().getNetworkObject()));
      }

   }

   public void sendInviteClient(FactionInvite var1) {
      this.gameState.getNetworkObject().factionInviteAdd.add(new RemoteFactionInvitation(var1, this.gameState.getNetworkObject()));
   }

   public void sendPersonalEnemyAdd(String var1, Faction var2, String var3) {
      RemoteStringArray var4;
      (var4 = new RemoteStringArray(3, this.gameState.getNetworkObject())).set(0, (String)String.valueOf(var2.getIdFaction()));
      var4.set(1, (String)String.valueOf(var1));
      var4.set(2, (String)String.valueOf(var3));
      this.gameState.getNetworkObject().personalElemiesAdd.add((RemoteArray)var4);
   }

   public void sendPersonalEnemyRemove(String var1, Faction var2, String var3) {
      RemoteStringArray var4;
      (var4 = new RemoteStringArray(3, this.gameState.getNetworkObject())).set(0, (String)String.valueOf(var2.getIdFaction()));
      var4.set(1, (String)String.valueOf(var1));
      var4.set(2, (String)String.valueOf(var3));
      this.gameState.getNetworkObject().personalElemiesDel.add((RemoteArray)var4);
   }

   public void sendRelationshipAccept(String var1, FactionRelationOffer var2, boolean var3) {
      RemoteStringArray var4;
      (var4 = new RemoteStringArray(3, this.getGameState().getNetworkObject())).set(0, (String)var1);
      var4.set(1, (String)String.valueOf(var2.getCode()));
      var4.set(2, (String)String.valueOf(var3));
      this.getGameState().getNetworkObject().factionRelationshipAcceptBuffer.add((RemoteArray)var4);
   }

   public void sendRelationshipOffer(String var1, int var2, int var3, byte var4, String var5, boolean var6) {
      FactionRelationOffer var7;
      (var7 = new FactionRelationOffer()).set(var1, var2, var3, var4, var5, var6);
      this.getGameState().getNetworkObject().factionRelationshipOffer.add((RemoteArray)var7.getRemoteArrayOffer(this.getGameState().getNetworkObject()));
   }

   public void setAllRelations(byte var1) {
      Iterator var2 = this.factionMap.keySet().iterator();

      while(var2.hasNext()) {
         int var3 = (Integer)var2.next();
         Iterator var4 = this.factionMap.keySet().iterator();

         while(var4.hasNext()) {
            int var5 = (Integer)var4.next();
            if (var3 > 0 && var5 > 0 && var3 != var5) {
               this.setRelationServer(var3, var5, var1);
            }
         }
      }

   }

   public void setEnemy(FactionInterface var1, FactionInterface var2) {
      long var3 = FactionRelation.getCode(var1.getFactionId(), var2.getFactionId());
      if (!this.relations.containsKey(var3)) {
         FactionRelation var5;
         (var5 = new FactionRelation()).set(var1.getFactionId(), var2.getFactionId());
         this.relations.put(var3, var5);
      }

      ((FactionRelation)this.relations.get(var3)).setEnemy();
   }

   public void setFriend(FactionInterface var1, FactionInterface var2) {
      long var3 = FactionRelation.getCode(var1.getFactionId(), var2.getFactionId());
      if (!this.relations.containsKey(var3)) {
         FactionRelation var5;
         (var5 = new FactionRelation()).set(var1.getFactionId(), var2.getFactionId());
         this.relations.put(var3, var5);
      }

      ((FactionRelation)this.relations.get(var3)).setFriend();
   }

   public void setNeutral(FactionInterface var1, FactionInterface var2) {
      long var3 = FactionRelation.getCode(var1.getFactionId(), var2.getFactionId());
      if (!this.relations.containsKey(var3)) {
         FactionRelation var5;
         (var5 = new FactionRelation()).set(var1.getFactionId(), var2.getFactionId());
         this.relations.put(var3, var5);
      }

      ((FactionRelation)this.relations.get(var3)).setNeutral();
   }

   public void setRelationServer(int var1, int var2, byte var3) {
      synchronized(this.relationsToAdd) {
         this.relationsToAdd.add(new FactionRelation(var1, var2, var3));
      }
   }

   public String toString() {
      return this.factionMap.values().toString();
   }

   public void updateFromNetworkObject(NetworkGameState var1) {
      this.npcFactionNews.updateFromNetworkObject(var1);

      int var2;
      for(var2 = 0; var2 < var1.factionNewsPosts.getReceiveBuffer().size(); ++var2) {
         RemoteFactionNewsPost var3 = (RemoteFactionNewsPost)var1.factionNewsPosts.getReceiveBuffer().get(var2);
         synchronized(this.toAddFactionNewsPosts) {
            this.toAddFactionNewsPosts.add(var3.get());
         }
      }

      for(var2 = 0; var2 < var1.simpleCommandQueue.getReceiveBuffer().size(); ++var2) {
         SimpleCommand var28 = (SimpleCommand)((RemoteSimpelCommand)var1.simpleCommandQueue.getReceiveBuffer().get(var2)).get();
         synchronized(this.simpleCommandQueue) {
            this.simpleCommandQueue.enqueue((NPCFactionControlCommand)var28);
         }
      }

      RemoteFactionInvitation var29;
      for(var2 = 0; var2 < var1.factionInviteAdd.getReceiveBuffer().size(); ++var2) {
         var29 = (RemoteFactionInvitation)var1.factionInviteAdd.getReceiveBuffer().get(var2);
         synchronized(this.toAddFactionInvites) {
            this.toAddFactionInvites.add(var29.get());
         }
      }

      for(var2 = 0; var2 < var1.factionPointMod.getReceiveBuffer().size(); ++var2) {
         RemoteFactionPointUpdate var30 = (RemoteFactionPointUpdate)var1.factionPointMod.getReceiveBuffer().get(var2);
         synchronized(this.toAddFactionPointMods) {
            if (this.gameState.isOnServer()) {
               throw new RuntimeException();
            }

            this.toAddFactionPointMods.enqueue(var30.get());
         }
      }

      RemoteStringArray var31;
      for(var2 = 0; var2 < var1.personalElemiesAdd.getReceiveBuffer().size(); ++var2) {
         var31 = (RemoteStringArray)var1.personalElemiesAdd.getReceiveBuffer().get(var2);
         synchronized(this.getToModPersonalEnemies()) {
            this.getToModPersonalEnemies().add(new PersonalEnemyMod((String)var31.get(1).get(), (String)var31.get(2).get(), Integer.parseInt((String)var31.get(0).get()), true));
         }
      }

      for(var2 = 0; var2 < var1.personalElemiesDel.getReceiveBuffer().size(); ++var2) {
         var31 = (RemoteStringArray)var1.personalElemiesDel.getReceiveBuffer().get(var2);
         synchronized(this.getToModPersonalEnemies()) {
            this.getToModPersonalEnemies().add(new PersonalEnemyMod((String)var31.get(1).get(), (String)var31.get(2).get(), Integer.parseInt((String)var31.get(0).get()), false));
         }
      }

      for(var2 = 0; var2 < var1.factionRelationshipAcceptBuffer.getReceiveBuffer().size(); ++var2) {
         var31 = (RemoteStringArray)var1.factionRelationshipAcceptBuffer.getReceiveBuffer().get(var2);
         synchronized(this.toAddFactionRelationOfferAccepts) {
            String var5 = (String)var31.get(0).get();
            long var6 = Long.parseLong((String)var31.get(1).get());
            boolean var8 = Boolean.parseBoolean((String)var31.get(2).get());
            synchronized(this.toAddFactionRelationOfferAccepts) {
               this.toAddFactionRelationOfferAccepts.add(new FactionRelationOfferAcceptOrDecline(var5, var6, var8));
            }
         }
      }

      for(var2 = 0; var2 < var1.factionkickMemberRequests.getReceiveBuffer().size(); ++var2) {
         var31 = (RemoteStringArray)var1.factionkickMemberRequests.getReceiveBuffer().get(var2);
         synchronized(this.toKickMember) {
            System.err.println("[FactionManager] Received Faction Kick on " + this.getGameState().getState());
            this.toKickMember.add(new FactionKick((String)var31.get(2).get(), Integer.parseInt((String)var31.get(1).get()), (String)var31.get(0).get()));
         }
      }

      for(var2 = 0; var2 < var1.factionInviteDel.getReceiveBuffer().size(); ++var2) {
         var29 = (RemoteFactionInvitation)var1.factionInviteDel.getReceiveBuffer().get(var2);
         synchronized(this.toDelFactionInvites) {
            System.err.println("[FactionManager] Received Faction Invite DELETE on " + this.getGameState().getState());
            this.toDelFactionInvites.add(var29.get());
         }
      }

      String var4;
      String var7;
      int var33;
      RemoteField[] var34;
      String var35;
      for(var2 = 0; var2 < var1.factionMod.getReceiveBuffer().size(); ++var2) {
         var4 = (String)(var34 = (RemoteField[])((RemoteStringArray)var1.factionMod.getReceiveBuffer().get(var2)).get())[0].get();
         var33 = Integer.parseInt((String)var34[1].get());
         var35 = (String)var34[2].get();
         var7 = (String)var34[3].get();
         FactionMod var40;
         (var40 = new FactionMod()).initiator = var4;
         var40.factionId = var33;
         var40.option = var35;
         var40.setting = var7;
         synchronized(this.changedFactions) {
            this.changedFactions.add(var40);
         }
      }

      RemoteFaction var36;
      for(var2 = 0; var2 < var1.factionAdd.getReceiveBuffer().size(); ++var2) {
         var36 = (RemoteFaction)var1.factionAdd.getReceiveBuffer().get(var2);
         this.addFaction((Faction)var36.get());
      }

      for(var2 = 0; var2 < var1.factionDel.getReceiveBuffer().size(); ++var2) {
         var36 = (RemoteFaction)var1.factionDel.getReceiveBuffer().get(var2);
         this.removeFaction((Faction)var36.get());
      }

      for(var2 = 0; var2 < var1.factionMemberMod.getReceiveBuffer().size(); ++var2) {
         var4 = (String)(var34 = (RemoteField[])((RemoteStringArray)var1.factionMemberMod.getReceiveBuffer().get(var2)).get())[0].get();
         var33 = Integer.parseInt((String)var34[1].get());
         var35 = (String)var34[2].get();
         var7 = (String)var34[3].get();
         long var42 = Long.parseLong((String)var34[4].get());
         FactionMemberMod var10 = new FactionMemberMod();
         if (var35.equals("r")) {
            var10.addOrMod = false;
            var10.id = var33;
            var10.playerState = var4;
            var10.initiator = var7;
         } else {
            byte var38 = Byte.parseByte(var35);
            var10.addOrMod = true;
            var10.id = var33;
            var10.playerState = var4;
            var10.permissions = var38;
            var10.initiator = var7;
            var10.lastActiveTime = var42;
         }

         synchronized(this.changedMembersFactions) {
            this.changedMembersFactions.add(var10);
         }
      }

      int var32;
      int var37;
      for(var2 = 0; var2 < var1.factionRelationships.getReceiveBuffer().size(); ++var2) {
         var32 = (Integer)(var34 = (RemoteField[])((RemoteIntegerArray)var1.factionRelationships.getReceiveBuffer().get(var2)).get())[0].get();
         var33 = (Integer)var34[1].get();
         var37 = (Integer)var34[2].get();
         if (var32 != var33) {
            FactionRelation var39;
            (var39 = new FactionRelation()).set(var32, var33);
            var39.setRelation((byte)var37);
            synchronized(this.relationsToAdd) {
               this.relationsToAdd.add(var39);
            }
         } else {
            System.err.println("[FACTION] Exception: received relationship with self: " + var32);
         }
      }

      for(var2 = 0; var2 < var1.factionRelationshipOffer.getReceiveBuffer().size(); ++var2) {
         var32 = Integer.parseInt((String)(var34 = (RemoteField[])((RemoteStringArray)var1.factionRelationshipOffer.getReceiveBuffer().get(var2)).get())[0].get());
         var33 = Integer.parseInt((String)var34[1].get());
         var37 = Integer.parseInt((String)var34[2].get());
         var7 = (String)var34[3].get();
         String var44 = (String)var34[4].get();
         boolean var9 = Boolean.parseBoolean((String)var34[5].get());
         FactionRelationOffer var49;
         (var49 = new FactionRelationOffer()).set(var44, var32, var33, (byte)var37, var7, var9);
         synchronized(this.relationOffersToAdd) {
            this.relationOffersToAdd.add(var49);
         }
      }

      for(var2 = 0; var2 < var1.factionHomeBaseChangeBuffer.getReceiveBuffer().size(); ++var2) {
         var31 = (RemoteStringArray)var1.factionHomeBaseChangeBuffer.getReceiveBuffer().get(var2);
         var4 = new String((String)var31.get(0).get());
         var33 = Integer.parseInt((String)var31.get(1).get());
         var35 = new String((String)var31.get(2).get());
         FactionHomebaseChange var41;
         if (this.gameState.isOnServer()) {
            var41 = new FactionHomebaseChange(var4, var33, var35, (Vector3i)null, (String)null);
         } else {
            int var46 = Integer.parseInt((String)var31.get(3).get());
            int var47 = Integer.parseInt((String)var31.get(4).get());
            int var50 = Integer.parseInt((String)var31.get(5).get());
            String var43 = new String((String)var31.get(6).get());
            var41 = new FactionHomebaseChange(var4, var33, var35, new Vector3i(var46, var47, var50), var43);
         }

         synchronized(this.factionHomebaseToMod) {
            this.factionHomebaseToMod.add(var41);
         }
      }

      for(var2 = 0; var2 < var1.factionClientSystemOwnerChangeBuffer.getReceiveBuffer().size(); ++var2) {
         RemoteSystemOwnershipChange var45 = (RemoteSystemOwnershipChange)var1.factionClientSystemOwnerChangeBuffer.getReceiveBuffer().get(var2);
         synchronized(this.factionSystemOwnerToMod) {
            this.factionSystemOwnerToMod.add(var45.get());
         }
      }

      for(var2 = 0; var2 < var1.factionRolesBuffer.getReceiveBuffer().size(); ++var2) {
         RemoteFactionRoles var48 = (RemoteFactionRoles)var1.factionRolesBuffer.getReceiveBuffer().get(var2);
         synchronized(this.relationRolesToMod) {
            this.relationRolesToMod.enqueue(var48.get());
         }
      }

   }

   public void serverRevokeFactionHome(int var1) {
      if (this.gameState.isOnServer()) {
         String var2 = new String("");
         String var3 = "";
         FactionHomebaseChange var5;
         (var5 = new FactionHomebaseChange(var2, var1, var3, (Vector3i)null, (String)null)).admin = true;
         synchronized(this.factionHomebaseToMod) {
            this.factionHomebaseToMod.add(var5);
         }
      }
   }

   public void scheduleTurn(NPCFaction var1) {
      assert this.getState() instanceof GameServerState;

      synchronized(this.turnSchedule) {
         this.turnSchedule.enqueue(var1);
      }
   }

   public void updateLocal(Timer var1) {
      if (this.flagCheckFactions) {
         this.checkFactions();
         this.flagCheckFactions = false;
      }

      if (this.gameState.isOnServer()) {
         this.updateFactionPoints();
      }

      if (!this.simpleCommandQueue.isEmpty()) {
         synchronized(this.simpleCommandQueue) {
            while(!this.simpleCommandQueue.isEmpty()) {
               NPCFactionControlCommand var3 = (NPCFactionControlCommand)this.simpleCommandQueue.dequeue();
               this.executeSimpleCommand(var3);
            }
         }
      }

      if (!this.toAddFactionNewsPosts.isEmpty()) {
         synchronized(this.toAddFactionNewsPosts) {
            while(!this.toAddFactionNewsPosts.isEmpty()) {
               FactionNewsPost var32;
               if ((var32 = (FactionNewsPost)this.toAddFactionNewsPosts.remove(0)).isDelete()) {
                  TreeSet var4;
                  if ((var4 = (TreeSet)this.getNews().get(var32.getFactionId())) != null) {
                     var4.remove(var32);
                     if (this.gameState.isOnServer()) {
                        this.gameState.getNetworkObject().factionNewsPosts.add(new RemoteFactionNewsPost(var32, this.gameState.getNetworkObject()));
                     }

                     this.changedFactionNewsDeletedAspect = true;
                  }

                  if (!this.gameState.isOnServer() && ((GameClientState)this.gameState.getState()).getController().getClientChannel() != null) {
                     ((GameClientState)this.gameState.getState()).getController().getClientChannel().getFactionNews().remove(var32);
                     this.changedFactionNewsDeletedAspect = true;
                  }
               } else {
                  if (!this.getNews().containsKey(var32.getFactionId())) {
                     this.getNews().put(var32.getFactionId(), new TreeSet());
                  }

                  if (this.existsFaction(var32.getFactionId()) && !isNPCFaction(var32.getFactionId())) {
                     ((TreeSet)this.getNews().get(var32.getFactionId())).add(var32);
                     if (this.gameState.isOnServer()) {
                        this.gameState.getNetworkObject().factionNewsPosts.add(new RemoteFactionNewsPost(var32, this.gameState.getNetworkObject()));
                     } else {
                        PlayerState var36 = ((GameClientState)this.getGameState().getState()).getPlayer();
                        if (var32.getDate() > var36.getCreationTime() && var32.getFactionId() == var36.getFactionId()) {
                           ((GameClientState)this.getGameState().getState()).getController().popupGameTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONMANAGER_0, var32.getMessage().substring(0, Math.min(var32.getMessage().length() - 1, 20))), 0.0F);
                        }
                     }

                     System.err.println("[FACTIONMANAGER] updated news on " + this.getGameState().getState() + " for factionID " + var32.getFactionId() + "; delete: " + var32.isDelete());
                     this.changedFactionAspect = true;
                  }
               }
            }
         }
      }

      Iterator var2;
      if (this.flagHomebaseChanged != null) {
         var2 = this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

         while(var2.hasNext()) {
            Sendable var34;
            if ((var34 = (Sendable)var2.next()) instanceof SegmentController && ((SegmentController)var34).getRuleEntityManager() != null) {
               ((SegmentController)var34).getRuleEntityManager().triggerHomebaseChanged();
            }
         }

         this.flagHomebaseChanged = null;
      }

      FactionInvite var35;
      if (!this.toAddFactionInvites.isEmpty()) {
         synchronized(this.toAddFactionInvites) {
            while(!this.toAddFactionInvites.isEmpty()) {
               var35 = (FactionInvite)this.toAddFactionInvites.remove(0);
               synchronized(this.factionInvitations) {
                  this.factionInvitations.remove(var35);
                  this.factionInvitations.add(var35);
               }

               this.factionInvitationsChanged = true;
               if (this.gameState.isOnServer()) {
                  this.gameState.getNetworkObject().factionInviteAdd.add(new RemoteFactionInvitation(var35, this.gameState.getNetworkObject()));
                  System.err.println("[FACTIONMANAGER] added invite: from " + var35.getFromPlayerName() + " to " + var35.getToPlayerName() + " to faction " + var35.getFactionUID());
               }
            }
         }
      }

      if (!this.toDelFactionInvites.isEmpty()) {
         synchronized(this.toDelFactionInvites) {
            while(!this.toDelFactionInvites.isEmpty()) {
               var35 = (FactionInvite)this.toDelFactionInvites.remove(0);
               synchronized(this.factionInvitations) {
                  boolean var5 = this.factionInvitations.remove(var35);
                  System.err.println("[FactionManager] Removing faction invitation " + var35 + " on " + this.getGameState().getState() + "; success: " + var5);
                  if (!var5) {
                     System.err.println("[FactionManager] Failed to delete invitation: " + var35 + ": " + this.factionInvitations);
                  }
               }

               this.factionInvitationsChanged = true;
               if (this.gameState.isOnServer()) {
                  this.gameState.getNetworkObject().factionInviteDel.add(new RemoteFactionInvitation(var35, this.gameState.getNetworkObject()));
               }
            }
         }
      }

      if (this.factionInvitationsChanged) {
         System.err.println("[FACTIONMANAGER] Faction Invites Changed");
         this.setChanged();
         this.notifyObservers("INVITATION_UPDATE");
         this.factionInvitationsChanged = false;
      }

      var2 = this.factionMap.values().iterator();

      while(var2.hasNext()) {
         ((Faction)var2.next()).updateLocal(var1);
      }

      Faction var38;
      FactionPermission var41;
      if (!this.changedFactions.isEmpty()) {
         synchronized(this.changedFactions) {
            for(; !this.changedFactions.isEmpty(); this.changedFactionAspect = true) {
               System.err.println("########### APPLYING FACTION CHANGE ");
               FactionMod var37 = (FactionMod)this.changedFactions.remove(0);
               if ((var38 = (Faction)this.factionMap.get(var37.factionId)) != null) {
                  if ("MCN".equals(var37.option)) {
                     var41 = (FactionPermission)var38.getMembersUID().get(var37.initiator);
                     System.err.println("[FACTIONMANAGER] Faction " + var38 + " set change member name: " + var37.initiator + " -> " + var37.setting);
                     if (var41 != null) {
                        var41.playerUID = var37.setting;
                     } else {
                        System.err.println("[FACTIONMANAGER] member not found " + var37.initiator);
                     }

                     if (this.isOnServer()) {
                        var38.getRuleEntityManager().triggerFactionMemberMod();
                     }
                  } else if ("OTJ".equals(var37.option)) {
                     var38.setOpenToJoin(Boolean.parseBoolean(var37.setting));
                     System.err.println("[FACTIONMANAGER] Faction " + var38 + " set open to join: " + var37.setting);
                  } else if ("DES".equals(var37.option)) {
                     var38.setDescription(var37.setting);
                     System.err.println("[FACTIONMANAGER] Faction " + var38 + " set description: " + var37.setting);
                  } else if ("NAM".equals(var37.option)) {
                     var38.setName(var37.setting);
                     System.err.println("[FACTIONMANAGER] Faction " + var38 + " set name: " + var37.setting);
                  } else if ("ATN".equals(var37.option)) {
                     var38.setAttackNeutral(Boolean.parseBoolean(var37.setting));
                     System.err.println("[FACTIONMANAGER] Faction " + var38 + " set attack neutral: " + var37.setting);
                  } else if ("ADW".equals(var37.option)) {
                     var38.setAutoDeclareWar(Boolean.parseBoolean(var37.setting));
                     System.err.println("[FACTIONMANAGER] Faction " + var38 + " set auto attack: " + var37.setting);
                  } else {
                     System.err.println("[CLIENT] Exception: unknown faction mod command: " + var37.option + " -> " + var37.setting);

                     assert false;
                  }
               }
            }
         }
      }

      FactionRelation var8;
      Faction var39;
      Iterator var40;
      Faction var43;
      if (!this.toDelFactions.isEmpty()) {
         synchronized(this.toDelFactions) {
            for(; !this.toDelFactions.isEmpty(); this.changedFactionAspect = true) {
               var39 = (Faction)this.toDelFactions.remove(0);
               synchronized(this.factionMap) {
                  ((GameStateInterface)this.getState()).getGameState().getInventories().remove((long)var39.getIdFaction());
                  var43 = (Faction)this.factionMap.remove(var39.getIdFaction());
                  System.err.println("[FACTIONMANAGER] " + this.getGameState().getState() + " Faction " + var43 + " has been deleted");
                  if (var43 == null) {
                     System.err.println("[FACTION][WARNING] " + this.getGameState().getState() + " could not delete " + var39 + ". ID NOT FOUND");
                  } else {
                     ArrayList var6 = new ArrayList();
                     Iterator var7 = this.relations.values().iterator();

                     while(var7.hasNext()) {
                        if ((var8 = (FactionRelation)var7.next()).contains(var43.getIdFaction())) {
                           var6.add(var8);
                        }
                     }

                     this.relations.values().removeAll(var6);
                  }
               }

               if (this.gameState.isOnServer()) {
                  this.gameState.getNetworkObject().factionDel.add(new RemoteFaction(var39, this.gameState.getNetworkObject()));
                  var40 = this.factionMap.values().iterator();

                  while(var40.hasNext()) {
                     if ((var43 = (Faction)var40.next()).isNPC() && var43 != var39) {
                        ((NPCFaction)var43).getDiplomacy().onDeletedFaction(var39);
                     }
                  }
               }
            }
         }
      }

      Iterator var46;
      if (!this.toAddFactions.isEmpty()) {
         synchronized(this.toAddFactions) {
            for(; !this.toAddFactions.isEmpty(); this.changedFactionAspect = true) {
               var39 = (Faction)this.toAddFactions.remove(0);
               this.initializeRelationShips(var39);
               synchronized(this.factionMap) {
                  if (this.gameState.isOnServer()) {
                     var46 = var39.getMembersUID().entrySet().iterator();

                     while(var46.hasNext()) {
                        java.util.Map.Entry var48 = (java.util.Map.Entry)var46.next();
                        System.err.println("[FACTION] Added to members " + (String)var48.getKey() + " perm(" + ((FactionPermission)var48.getValue()).role + ") of " + var39 + " on " + this.gameState.getState());
                     }
                  }

                  this.factionMap.put(var39.getIdFaction(), var39);
                  if (var39 instanceof NPCFaction) {
                     ((GameStateInterface)this.getState()).getGameState().getInventories().put((long)var39.getIdFaction(), ((NPCFaction)var39).getInventory());
                  }
               }

               if (var39.addHook != null) {
                  var39.addHook.callback();
                  var39.addHook = null;
               }

               if (this.isOnServer()) {
                  this.gameState.getNetworkObject().factionAdd.add(new RemoteFaction(var39, this.gameState.getNetworkObject()));

                  for(int var42 = 0; var42 < this.failedChangedMembersFactions.size(); ++var42) {
                     FactionMemberMod var51;
                     if ((var51 = (FactionMemberMod)this.failedChangedMembersFactions.get(var42)).id == var39.getIdFaction()) {
                        this.changedMembersFactions.add(var51);
                        this.failedChangedMembersFactions.remove(var42);
                        --var42;
                     }
                  }

                  for(var40 = this.factionMap.values().iterator(); var40.hasNext(); var43.getRuleEntityManager().trigger(-1L)) {
                     if ((var43 = (Faction)var40.next()).isNPC() && var43 != var39) {
                        ((NPCFaction)var43).getDiplomacy().onAddedFaction(var39);
                     }
                  }
               }
            }
         }
      }

      if (!this.getToModPersonalEnemies().isEmpty()) {
         synchronized(this.getToModPersonalEnemies()) {
            while(true) {
               while(true) {
                  while(true) {
                     while(!this.getToModPersonalEnemies().isEmpty()) {
                        PersonalEnemyMod var44 = (PersonalEnemyMod)this.getToModPersonalEnemies().remove(0);
                        var38 = this.getFaction(var44.fid);
                        if (this.gameState.isOnServer()) {
                           if (var38 != null) {
                              var41 = (FactionPermission)var38.getMembersUID().get(var44.initiator);
                              GameServerState var49 = (GameServerState)this.gameState.getState();
                              if (!var38.isNPC() && !var49.isAdmin(var44.initiator) && (var41 == null || !var41.hasRelationshipPermission(var38))) {
                                 System.err.println("[SERVER] Exception: cannot add personal enemy: permission denied for " + var44.initiator);
                              } else if (var44.add) {
                                 System.err.println("[FACTIONMANAGER] ADDING PERSONAL ENEMY OF FACTION: " + var44.enemyPlayerName);
                                 var38.getPersonalEnemies().add(var44.enemyPlayerName.toLowerCase(Locale.ENGLISH));
                                 this.sendPersonalEnemyAdd(var44.initiator, var38, var44.enemyPlayerName.toLowerCase(Locale.ENGLISH));
                                 this.npcFactionNews.war(var38.getIdFaction(), var44.enemyPlayerName);
                              } else {
                                 System.err.println("[FACTIONMANAGER] REMOVING PERSONAL ENEMY OF FACTION: " + var44.enemyPlayerName);
                                 var38.getPersonalEnemies().remove(var44.enemyPlayerName.toLowerCase(Locale.ENGLISH));
                                 this.sendPersonalEnemyRemove(var44.initiator, var38, var44.enemyPlayerName.toLowerCase(Locale.ENGLISH));
                                 this.npcFactionNews.peace(var38.getIdFaction(), var44.enemyPlayerName);
                              }
                           } else {
                              System.err.println("[SERVER] Exception: cannot add personal enemy: faction does not exist");
                           }
                        } else if (var38 != null) {
                           if (var44.add) {
                              var38.getPersonalEnemies().add(var44.enemyPlayerName.toLowerCase(Locale.ENGLISH));
                           } else {
                              var38.getPersonalEnemies().remove(var44.enemyPlayerName.toLowerCase(Locale.ENGLISH));
                           }
                        } else {
                           System.err.println("[CLIENT] Exception: cannot add personal enemy: faction does not exist");
                        }
                     }
                  }
               }
            }
         }
      }

      FactionPermission var53;
      PlayerState var60;
      if (!this.changedMembersFactions.isEmpty()) {
         synchronized(this.changedMembersFactions) {
            while(!this.changedMembersFactions.isEmpty()) {
               FactionMemberMod var45 = (FactionMemberMod)this.changedMembersFactions.remove(0);
               synchronized(this.factionMap) {
                  var43 = (Faction)this.factionMap.get(var45.id);
                  if (this.getGameState().isOnServer() && var45.initiator != null) {
                     var43.getRuleEntityManager().triggerFactionMemberMod();
                     if ((var53 = (FactionPermission)var43.getMembersUID().get(var45.initiator)) == null) {
                        System.err.println("[SERVER][ERROR] Could not hande faction mod! unknown member: " + var45.initiator);

                        assert false;
                     } else {
                        label842: {
                           FactionPermission var54 = (FactionPermission)var43.getMembersUID().get(var45.playerState);
                           if ((!var45.playerState.toLowerCase(Locale.ENGLISH).equals(var45.initiator.toLowerCase(Locale.ENGLISH)) || var53.role != 4) && var54 != null && var53.role <= var54.role) {
                              System.err.println("[SERVER][ERROR] Failed to set permission (role too low: initiator: " + var53.role + "; target " + var54.role + ")! initiator: " + var45.initiator);

                              try {
                                 (var60 = ((GameServerState)this.getGameState().getState()).getPlayerFromName(var45.initiator)).sendServerMessage(new ServerMessage(new Object[]{268}, 3, var60.getId()));
                              } catch (PlayerNotFountException var13) {
                                 var8 = null;
                                 var13.printStackTrace();
                              }
                              continue;
                           }

                           if (!var45.addOrMod || var53 != null && var53.hasPermissionEditPermission(var43)) {
                              if (var45.addOrMod || var53 != null && var53.hasKickPermission(var43)) {
                                 break label842;
                              }

                              System.err.println("[SERVER][ERROR] Failed to kick! initiator: " + var45.initiator);

                              try {
                                 (var60 = ((GameServerState)this.getGameState().getState()).getPlayerFromName(var45.initiator)).sendServerMessage(new ServerMessage(new Object[]{270}, 3, var60.getId()));
                              } catch (PlayerNotFountException var11) {
                                 var8 = null;
                                 var11.printStackTrace();
                              }
                              continue;
                           }

                           System.err.println("[SERVER][ERROR] Failed to set permission! initiator: " + var45.initiator);

                           try {
                              (var60 = ((GameServerState)this.getGameState().getState()).getPlayerFromName(var45.initiator)).sendServerMessage(new ServerMessage(new Object[]{269}, 3, var60.getId()));
                           } catch (PlayerNotFountException var12) {
                              var8 = null;
                              var12.printStackTrace();
                           }
                           continue;
                        }
                     }
                  }

                  if (var45.addOrMod) {
                     if (var43 != null) {
                        var43.addOrModifyMember(var45.playerState, var45.playerState, var45.permissions, var45.lastActiveTime, this.gameState, true);
                     } else {
                        System.err.println("[FACTIONMANAGER][ERROR] adding member failed: " + var45.playerState + " from " + var45.id);
                        if (!var45.failed) {
                           var45.failTime = System.currentTimeMillis();
                           this.failedChangedMembersFactions.add(var45);
                        }
                     }
                  } else if (var43 != null) {
                     System.err.println("[FACTIONMANAGER] removing member: " + var45.playerState + " from " + var43 + "; on " + this.gameState.getState());
                     var43.removeMember(var45.playerState, this.gameState);
                     if (this.gameState.isOnServer() && var43.getMembersUID().isEmpty()) {
                        System.err.println("[FACTIONMANAGER] Removed last member: Faction Empty -> Removing Faction " + var43);
                        if (var43.getFactionMode() == 0) {
                           this.removeFaction(var43);
                        } else {
                           System.err.println("[FACTIONMANAGER] faction was not removed because of 0 members because it has a mode flag (battle)");
                        }
                     }
                  } else {
                     System.err.println("[FactionManager][ERROR] removing member failed: " + var45.playerState + " from " + var45.id);
                     if (!var45.failed) {
                        var45.failTime = System.currentTimeMillis();
                        this.failedChangedMembersFactions.add(var45);
                     }

                     var45.failed = true;
                  }

                  this.changedFactionAspect = true;
               }
            }
         }
      }

      if (!this.toKickMember.isEmpty()) {
         synchronized(this.toKickMember) {
            label569:
            while(true) {
               while(true) {
                  FactionKick var47;
                  do {
                     if (this.toKickMember.isEmpty()) {
                        break label569;
                     }

                     var47 = (FactionKick)this.toKickMember.remove(0);
                  } while((var38 = this.getFaction(var47.faction)) == null);

                  if (this.isOnServer()) {
                     var38.getRuleEntityManager().triggerFactionMemberMod();
                  }

                  var41 = (FactionPermission)var38.getMembersUID().get(var47.initiator);
                  var53 = (FactionPermission)var38.getMembersUID().get(var47.player);
                  if (var41 != null && var53 != null && var41.hasKickPermission(var38) && var53.role < var41.role && var53.role < var38.getRoles().getRoles().length - 1) {
                     FactionMemberMod var58 = new FactionMemberMod(var38.getIdFaction(), var47.player, false);

                     try {
                        (var60 = ((GameServerState)this.gameState.getState()).getPlayerFromName(var47.player)).getFactionController().setFactionId(0);
                        System.err.println("[FACTIONMANAGER] Member has been kicked from faction " + var38 + ": " + var60);
                        var60.sendServerMessage(new ServerMessage(new Object[]{272, var38.getName()}, 1, var60.getId()));
                     } catch (PlayerNotFountException var9) {
                        System.err.println("[FACTION][KICK] player not found (could be offline)");
                     }

                     this.changedMembersFactions.add(var58);
                  } else {
                     try {
                        PlayerState var56;
                        (var56 = ((GameServerState)this.gameState.getState()).getPlayerFromName(var47.initiator)).getFactionController().setFactionId(0);
                        var56.sendServerMessage(new ServerMessage(new Object[]{271}, 3, var56.getId()));
                     } catch (PlayerNotFountException var10) {
                     }
                  }
               }
            }
         }
      }

      if (!this.relationsToAdd.isEmpty()) {
         synchronized(this.relationsToAdd) {
            while(!this.relationsToAdd.isEmpty()) {
               FactionRelation var50 = (FactionRelation)this.relationsToAdd.remove(0);

               assert !(var50 instanceof FactionRelationOffer) : var50;

               this.relations.put(var50.getCode(), var50);
               var38 = (Faction)this.factionMap.get(var50.a);
               var43 = (Faction)this.factionMap.get(var50.b);
               if (this.isOnServer()) {
                  if (var38 != null) {
                     var38.getRuleEntityManager().triggerFactionMemberMod();
                  }

                  if (var43 != null) {
                     var43.getRuleEntityManager().triggerFactionMemberMod();
                  }
               }

               this.changedFactionAspect = true;
               if (this.getGameState().isOnServer()) {
                  this.gameState.getNetworkObject().factionRelationships.add((RemoteArray)var50.getRemoteArray(this.gameState.getNetworkObject()));
               }
            }
         }
      }

      var2 = this.actionCheck.iterator();

      while(var2.hasNext()) {
         int var52 = (Integer)var2.next();
         if ((var38 = (Faction)this.factionMap.get(var52)) != null) {
            var38.checkActions();
         }
      }

      this.actionCheck.clear();
      if (!this.toAddFactionPointMods.isEmpty()) {
         synchronized(this.toAddFactionPointMods) {
            while(!this.toAddFactionPointMods.isEmpty()) {
               assert !this.getGameState().isOnServer();

               FactionPointMod var55 = (FactionPointMod)this.toAddFactionPointMods.dequeue();
               if ((var38 = (Faction)this.factionMap.get(var55.factionId)) != null) {
                  if (this.isOnServer()) {
                     var38.getRuleEntityManager().triggerFactionPointsChange();
                  }

                  var55.apply(var38);
               } else {
                  System.err.println("[FACTIONMANAGER] Exception: faction not found for point mod: " + var55.factionId);
               }
            }
         }
      }

      if (!this.relationRolesToMod.isEmpty()) {
         synchronized(this.relationRolesToMod) {
            while(true) {
               while(!this.relationRolesToMod.isEmpty()) {
                  FactionRoles var57 = (FactionRoles)this.relationRolesToMod.dequeue();
                  if ((var38 = this.getFaction(var57.factionId)) != null) {
                     var46 = var38.getMembersUID().values().iterator();

                     while(var46.hasNext()) {
                        var53 = (FactionPermission)var46.next();
                        var38.fogOfWarCheckServer(var53, var38.getRoles());
                     }

                     var38.getRoles().apply(var57);
                     System.err.println("[SERVER][FACTIONMANAGER] Applied Faction Roles: Faction: " + var38.getName() + "(" + var38.getIdFaction() + "); SenderID: " + var57.senderId + "; Roles: " + var38.getRoles());
                     if (this.isOnServer()) {
                        this.sendFactionRoles(var38.getRoles());
                     }
                  } else {
                     System.err.println("[FactionManager][ERROR] could not find factionto apply rule " + var57);
                  }
               }
            }
         }
      }

      this.handleFactionHomebaseMods();
      this.handleFactionSystemOwnerMods();
      this.handleRelationShipOffers();
      this.handleFactionOffersAccept();
      if (this.factionOffersChanged) {
         this.setChanged();
         this.notifyObservers("RELATIONSHIP_OFFER");
         this.factionOffersChanged = false;
      }

      if (this.changedFactionNewsDeletedAspect) {
         this.setChanged();
         this.notifyObservers("FACTION_NEWS_DELETED");
         this.changedFactionNewsDeletedAspect = false;
      }

      if (this.changedFactionAspect) {
         this.setChanged();
         this.notifyObservers();
         this.changedFactionAspect = false;
      }

      this.handleNPCFactionTurns(var1);
      if (this.markedChangedContingentFactions.size() > 0) {
         var2 = this.markedChangedContingentFactions.iterator();

         while(var2.hasNext()) {
            if (!((NPCFaction)var2.next()).structure.updateChangedSystems(var1.currentTime)) {
               var2.remove();
            }
         }
      }

      this.npcFactionNews.updateLocal(var1);
      ObjectIterator var33 = this.needsSendAll.iterator();

      while(true) {
         do {
            if (!var33.hasNext()) {
               Iterator var59 = this.diplomacyChanged.iterator();

               while(var59.hasNext()) {
                  ((NPCDiplomacy)var59.next()).checkNPCFactionSending(this.getGameState(), false);
               }

               this.diplomacyChanged.clear();
               if (this.isOnServer() && ServerConfig.NPC_DEBUG_MODE.isOn()) {
                  if (!this.wasNpcDebugMode) {
                     this.checkNPCFactionSendingDebug(true);
                  } else {
                     this.checkNPCFactionSendingDebug(false);
                  }
               }

               this.wasNpcDebugMode = ServerConfig.NPC_DEBUG_MODE.isOn();
               return;
            }
         } while(((PlayerState)var33.next()).getClientChannel() == null);

         var40 = this.factionMap.values().iterator();

         while(var40.hasNext()) {
            if ((var43 = (Faction)var40.next()).isNPC()) {
               ((NPCFaction)var43).getDiplomacy().checkNPCFactionSending(this.getGameState(), true);
            }
         }

         var33.remove();
      }
   }

   private void handleNPCFactionTurns(Timer var1) {
      if (this.currentTurn != null && !this.existsFaction(this.currentTurn.getIdFaction())) {
         this.currentTurn = null;
      }

      if (this.currentTurn != null) {
         if (var1.currentTime - this.lastNPCFactionTurnUpdate > 100L) {
            if (this.currentTurn.turn(var1.currentTime)) {
               this.currentTurn = null;
            }

            this.lastNPCFactionTurnUpdate = var1.currentTime;
            return;
         }
      } else if (!this.turnSchedule.isEmpty()) {
         System.currentTimeMillis();
         synchronized(this.turnSchedule) {
            if (!this.turnSchedule.isEmpty()) {
               NPCFaction var2 = (NPCFaction)this.turnSchedule.dequeue();
               this.currentTurn = var2;
            }

            return;
         }
      }

   }

   public boolean isOnServer() {
      return this.gameState.isOnServer();
   }

   private void executeSimpleCommand(NPCFactionControlCommand var1) {
      Faction var2;
      if ((var2 = this.getFaction(var1.factionId)) != null && var2.isNPC()) {
         ((NPCFaction)var2).executeFactionCommand(var1);
      } else {
         assert false : var1 + "; " + var2;

      }
   }

   private void handleFactionOffersAccept() {
      int var1 = 0;
      if (!this.toAddFactionRelationOfferAccepts.isEmpty()) {
         this.factionOffersChanged = true;
         synchronized(this.toAddFactionRelationOfferAccepts) {
            while(!this.toAddFactionRelationOfferAccepts.isEmpty()) {
               FactionRelationOfferAcceptOrDecline var3 = (FactionRelationOfferAcceptOrDecline)this.toAddFactionRelationOfferAccepts.remove(0);
               FactionRelationOffer var4;
               if ((var4 = (FactionRelationOffer)this.getRelationShipOffers().remove(var3.code)) != null && this.getGameState().isOnServer()) {
                  if (var3.accept) {
                     FactionRelation var5;
                     (var5 = new FactionRelation()).set(var4.a, var4.b);
                     if (var4.isEnemy()) {
                        this.npcFactionNews.war(var4.a, this.getFactionName(var4.b));
                        var5.setEnemy();
                     } else if (var4.isFriend()) {
                        this.npcFactionNews.ally(var4.a, this.getFactionName(var4.b));
                        var5.setFriend();
                     } else if (var4.isNeutral()) {
                        this.npcFactionNews.peace(var4.a, this.getFactionName(var4.b));
                        var5.setNeutral();
                     } else {
                        assert false;
                     }

                     String var6 = "[FACTION] Relationship Offer accepted: " + var3.code + " between " + var5 + "; " + ((Faction)this.factionMap.get(var5.a)).getName() + " AND " + ((Faction)this.factionMap.get(var5.b)).getName() + "; CODEA: " + FactionRelationOffer.getOfferCode(var4.a, var4.b) + "; CODEB: " + FactionRelationOffer.getOfferCode(var4.b, var4.a);
                     LogUtil.log().fine(var6);
                     synchronized(this.relationsToAdd) {
                        this.relationsToAdd.add(var5);
                     }
                  }

                  this.sendRelationshipAccept(var3.initiator, var4, var3.accept);
                  ++var1;
               }
            }
         }
      }

      if (var1 > 10) {
         System.err.println("[FACTION] more than 10 relationship offers in this update: " + var1);
      }

   }

   public void handleRelationShipOffers() {
      if (!this.relationOffersToAdd.isEmpty()) {
         synchronized(this.relationOffersToAdd) {
            while(true) {
               while(!this.relationOffersToAdd.isEmpty()) {
                  this.factionOffersChanged = true;
                  FactionRelationOffer var2;
                  if ((var2 = (FactionRelationOffer)this.relationOffersToAdd.remove(0)).a == var2.b) {
                     try {
                        throw new NullPointerException("tried to have relation with self " + var2.a + "; " + var2.b);
                     } catch (Exception var9) {
                        var9.printStackTrace();
                     }
                  } else {
                     Faction var3 = this.getFaction(var2.a);
                     Faction var4 = this.getFaction(var2.b);
                     if (var3 != null && var4 != null) {
                        FactionRelation var5;
                        if (!var2.revoke) {
                           if (this.getGameState().isOnServer()) {
                              FactionNewsPost var11;
                              if (var2.isEnemy()) {
                                 (var11 = new FactionNewsPost()).set(var4.getIdFaction(), var3.getName(), System.currentTimeMillis(), "Declaration of War", var3.getName() + " declared war!\n" + var2.getMessage(), 0);
                                 this.sendServerFactionMail(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONMANAGER_23, var3.getName()), StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONMANAGER_24, var3.getName(), var2.getMessage()), var4);
                                 this.sendServerFactionMail(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONMANAGER_25, StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONMANAGER_26, var3.getName(), var4.getName()), var3);
                                 this.diplomacyAction(DiplomacyAction.DiplActionType.DECLARATION_OF_WAR, var4.getIdFaction(), (long)var3.getIdFaction());
                                 this.npcFactionNews.war(var3.getIdFaction(), var4.getName());
                                 this.toAddFactionNewsPosts.add(var11);
                              } else if (var2.isFriend()) {
                                 (var11 = new FactionNewsPost()).set(var4.getIdFaction(), var3.getName(), System.currentTimeMillis(), "Alliance Offer", var3.getName() + " offered an Alliance!\n" + var2.getMessage(), 0);
                                 this.diplomacyAction(DiplomacyAction.DiplActionType.ALLIANCE_REQUEST, var4.getIdFaction(), (long)var3.getIdFaction());
                                 this.toAddFactionNewsPosts.add(var11);
                              } else if (var2.isNeutral()) {
                                 (var11 = new FactionNewsPost()).set(var4.getIdFaction(), var3.getName(), System.currentTimeMillis(), "Peace Offer", var3.getName() + " offered a peace treaty!\n" + var2.getMessage(), 0);
                                 this.diplomacyAction(DiplomacyAction.DiplActionType.PEACE_OFFER, var4.getIdFaction(), (long)var3.getIdFaction());
                                 this.toAddFactionNewsPosts.add(var11);
                              } else {
                                 assert false;
                              }
                           }

                           if (var2.isEnemy()) {
                              if (this.getGameState().isOnServer()) {
                                 (var5 = new FactionRelation()).set(var3.getIdFaction(), var4.getIdFaction());
                                 var5.setEnemy();
                                 synchronized(this.relationsToAdd) {
                                    this.relationsToAdd.add(var5);
                                 }
                              }
                           } else {
                              this.getRelationShipOffers().put(var2.getCode(), var2);
                              if (this.getGameState().isOnServer()) {
                                 this.gameState.getNetworkObject().factionRelationshipOffer.add((RemoteArray)var2.getRemoteArrayOffer(this.gameState.getNetworkObject()));
                              }
                           }
                        } else {
                           if (var2.rel == FactionRelation.RType.FRIEND.code && this.getRelation(var3.getIdFaction(), var4.getIdFaction()) != null && this.getRelation(var3.getIdFaction(), var4.getIdFaction()) == FactionRelation.RType.FRIEND) {
                              System.err.println("[FACTIONMANAGER] revoked alliance " + var2 + " on " + this.getGameState().getState());
                              (var5 = new FactionRelation()).set(var3.getIdFaction(), var4.getIdFaction());
                              var5.setNeutral();
                              synchronized(this.relationsToAdd) {
                                 this.relationsToAdd.add(var5);
                              }

                              if (this.getGameState().isOnServer()) {
                                 this.diplomacyAction(DiplomacyAction.DiplActionType.ALLIANCE_CANCEL, var4.getIdFaction(), (long)var3.getIdFaction());
                              }
                           } else {
                              System.err.println("[FACTIONMANAGER] remove relationship offer " + var2 + " on " + this.getGameState().getState());
                              this.getRelationShipOffers().remove(var2.getCode());
                           }

                           if (this.getGameState().isOnServer()) {
                              this.gameState.getNetworkObject().factionRelationshipOffer.add((RemoteArray)var2.getRemoteArrayOffer(this.gameState.getNetworkObject()));
                           }
                        }
                     }
                  }
               }

               return;
            }
         }
      }
   }

   public void sendServerSystemFactionOwnerChange(String var1, int var2, String var3, Vector3i var4, Vector3i var5, String var6) {
      assert var3.length() > 0 : "only valid stations allowed since this is a server change";

      assert this.gameState.isOnServer();

      FactionSystemOwnerChange var8 = new FactionSystemOwnerChange(var1, var2, var3, var4, var5, var6);
      synchronized(this.factionSystemOwnerToMod) {
         this.factionSystemOwnerToMod.add(var8);
      }
   }

   private void handleFactionSystemOwnerMods() {
      if (!this.factionSystemOwnerToMod.isEmpty()) {
         synchronized(this.factionSystemOwnerToMod) {
            while(true) {
               while(true) {
                  FactionSystemOwnerChange var2;
                  Faction var3;
                  do {
                     if (this.factionSystemOwnerToMod.isEmpty()) {
                        return;
                     }

                     var2 = (FactionSystemOwnerChange)this.factionSystemOwnerToMod.remove(0);
                     var3 = this.getFaction(var2.factionId);
                  } while(!this.getGameState().isOnServer());

                  PlayerState var24;
                  if (var2.sender != 0) {
                     RegisteredClientOnServer var4;
                     if ((var4 = (RegisteredClientOnServer)((GameServerState)this.getState()).getClients().get(var2.sender)).getPlayerObject() == null || !(var4.getPlayerObject() instanceof PlayerState)) {
                        System.err.println("[SERVER] SYSTEM OWNERSHIP CHANGE ERROR: no sender");
                        continue;
                     }

                     PlayerState var5 = (PlayerState)var4.getPlayerObject();
                     if (this.getFaction(var5.getFactionId()) == null) {
                        var5.sendServerMessagePlayerError(new Object[]{273});
                        continue;
                     }

                     Faction var6;
                     FactionPermission var7 = (FactionPermission)(var6 = this.getFaction(var5.getFactionId())).getMembersUID().get(var2.initiator);
                     System.err.println("[FACTIONMANAGER] System ownership called " + this.gameState.getState() + ": New Values: FactionId " + var2.factionId + "; System " + var2.targetSystem + " UID(" + var2.baseUID + ")");
                     if (var2.baseUID.length() > 0 && var3 != null && var6.factionPoints < 0.0F) {
                        System.err.println("[FACTIONMANAGER] Cannot give ownership to " + this.gameState.getState() + ": " + var2.factionId + " -> " + var2.baseUID + "; faction points negative");
                        var6.broadcastMessage(new Object[]{274}, 3, (GameServerState)this.getGameState().getState());
                        continue;
                     }

                     if (!var2.forceOverwrite && (var7 == null || !var7.hasClaimSystemPermission(var6))) {
                        try {
                           (var24 = ((GameServerState)this.gameState.getState()).getPlayerFromName(var2.initiator)).sendServerMessage(new ServerMessage(new Object[]{275}, 1, var24.getId()));
                        } catch (PlayerNotFountException var14) {
                           System.err.println("[FACTION][HOMEBASE] player not found " + var2.initiator);
                        }

                        System.err.println("[FACTION][HOMEBASE][ERROR] no right to claim/unclaim system " + var2.initiator);
                        continue;
                     }

                     if (var2.baseUID.length() > 0 && !var2.baseUID.startsWith(SimpleTransformableSendableObject.EntityType.SPACE_STATION.dbPrefix) && !var2.baseUID.startsWith(SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT.dbPrefix) && !var2.baseUID.equals("UNCLAIMABLE")) {
                        System.err.println("[FACTION][SYSTEMOWNER][ERROR] cannot take ownership with base '" + var2.baseUID + "'");
                        var2.baseUID = "";
                        var6.broadcastMessage(new Object[]{276}, 3, (GameServerState)this.getGameState().getState());
                     }
                  }

                  int var17 = 0;

                  try {
                     StellarSystem var19;
                     Vector3i var20 = (var19 = ((GameServerState)this.gameState.getState()).getUniverse().getStellarSystemFromStellarPos(var2.targetSystem)).getPos();
                     String var21 = var19.getName();
                     Sendable var8;
                     if (!var2.initiator.equals("ADMIN") && var19.getOwnerUID() != null && var19.getOwnerUID().equals("UNCLAIMABLE") && var19.getOwnerFaction() == -2) {
                        try {
                           (var24 = ((GameServerState)this.getGameState().getState()).getPlayerFromName(var2.initiator)).sendServerMessage(new ServerMessage(new Object[]{277}, 3, var24.getId()));
                        } catch (PlayerNotFountException var13) {
                           var8 = null;
                           var13.printStackTrace();
                        }
                     } else if ((var8 = (Sendable)this.gameState.getState().getLocalAndRemoteObjectContainer().getUidObjectMap().get(var2.baseUID)) != null && ((GameServerState)this.gameState.getState()).getUniverse().getSector(((SimpleTransformableSendableObject)var8).getSectorId()) != null) {
                        SimpleTransformableSendableObject var26 = (SimpleTransformableSendableObject)var8;
                        Sector var23;
                        if ((var23 = ((GameServerState)this.gameState.getState()).getUniverse().getSector(var26.getSectorId())) == null) {
                           throw new RuntimeException("Cannot set system ownership on server: sector for active object " + var26 + " wasnt loaded");
                        }

                        int var27 = var19.getOwnerFaction();
                        if (var3 != null) {
                           var19.setOwnerUID(new String(var2.baseUID));
                           var19.getOwnerPos().set(var23.pos);
                           var19.setOwnerFaction(var3.getIdFaction());
                           var17 = var2.factionId;
                        } else {
                           var17 = var19.getOwnerFaction();
                           var19.setOwnerUID((String)null);
                           var19.getOwnerPos().set(0, 0, 0);
                           var19.setOwnerFaction(0);
                        }

                        ((GameServerState)this.gameState.getState()).getDatabaseIndex().getTableManager().getSystemTable().updateOrInsertSystemIfChanged(var19, true);
                        ((GameServerState)this.gameState.getState()).getUniverse().getGalaxyFromSystemPos(var19.getPos()).getNpcFactionManager().onSystemOwnershipChanged(var27, var19.getOwnerFaction(), var19.getPos());
                        this.getGameState().sendGalaxyModToClients(var19, var23.pos);
                     } else {
                        try {
                           Vector3i var9 = var2.stationSector;
                           if (var2.baseUID.length() > 0) {
                              String var22 = DatabaseEntry.removePrefix(var2.baseUID);
                              List var10;
                              if ((var10 = ((GameServerState)this.gameState.getState()).getDatabaseIndex().getTableManager().getEntityTable().getByUIDExact(var22, 1)).size() == 1) {
                                 DatabaseEntry var18 = (DatabaseEntry)var10.get(0);
                                 var19 = ((GameServerState)this.gameState.getState()).getUniverse().getStellarSystemFromSecPos(var18.sectorPos);
                                 var9 = var18.sectorPos;
                              } else {
                                 System.err.println("Cannot set system ownership on server: " + var2.baseUID + " was not found in sql database");
                              }
                           }

                           if (var3 != null) {
                              System.err.println("[SERVER][FACTION] Territory: SET SYSTEM " + var19 + " to faction " + var3.getName());
                              var19.setOwnerUID(new String(var2.baseUID));
                              var19.getOwnerPos().set(var9);
                              var19.setOwnerFaction(var3.getIdFaction());
                              var17 = var2.factionId;
                           } else {
                              System.err.println("[SERVER][FACTION] Territory: REMOVE SYSTEM " + var19 + " OWNERSHIP FROM FACTION " + this.getFaction(var19.getOwnerFaction()));
                              var17 = var19.getOwnerFaction();
                              var19.setOwnerUID((String)null);
                              var19.getOwnerPos().set(0, 0, 0);
                              var19.setOwnerFaction(0);
                           }

                           ((GameServerState)this.gameState.getState()).getDatabaseIndex().getTableManager().getSystemTable().updateOrInsertSystemIfChanged(var19, true);
                           this.getGameState().sendGalaxyModToClients(var19, var9);
                        } catch (SQLException var11) {
                           var11.printStackTrace();
                           ((GameServerState)this.gameState.getState()).getController().broadcastMessage(new Object[]{278, var11.getClass().getSimpleName(), var11.getMessage()}, 3);
                           throw new RuntimeException("SYSTEM OWNERSHIP FAIL: Cannot set ownership  on server: " + var2.baseUID + " was not found in sql database");
                        } catch (IOException var12) {
                           var12.printStackTrace();
                           ((GameServerState)this.gameState.getState()).getController().broadcastMessage(new Object[]{279, var12.getClass().getSimpleName(), var12.getMessage()}, 3);
                           throw new RuntimeException("SYSTEM OWNERSHIP FAIL: Cannot set ownership on server: " + var2.baseUID + " was not found in sql database");
                        }
                     }

                     FactionNewsPost var25;
                     if (var3 != null) {
                        (var25 = new FactionNewsPost()).set(var17, var2.initiator, System.currentTimeMillis(), "System claimed", "System " + var21 + var20 + " has been claimed by " + var2.initiator + ".", 0);
                        this.toAddFactionNewsPosts.add(var25);
                     } else {
                        var25 = new FactionNewsPost();
                        String var28 = var2.initiator;
                        if ("SYSTEM".equals(var28)) {
                           var28 = "reset of faction on that structure";
                        }

                        var25.set(var17, var28, System.currentTimeMillis(), "System Ownership Revoked", "Ownership of System " + var21 + var20 + "\nhas been revoked by " + var28 + ".", 0);
                        this.toAddFactionNewsPosts.add(var25);
                     }
                  } catch (Exception var15) {
                     ((GameServerState)this.gameState.getState()).getController().broadcastMessage(new Object[]{280, var15.getClass().getSimpleName(), var15.getMessage()}, 3);
                     var15.printStackTrace();
                  }
               }
            }
         }
      }
   }

   private void handleFactionHomebaseMods() {
      if (!this.factionHomebaseToMod.isEmpty()) {
         synchronized(this.factionHomebaseToMod) {
            while(true) {
               while(true) {
                  while(!this.factionHomebaseToMod.isEmpty()) {
                     FactionHomebaseChange var2 = (FactionHomebaseChange)this.factionHomebaseToMod.remove(0);
                     Faction var3;
                     if ((var3 = this.getFaction(var2.factionId)) != null) {
                        FactionPermission var4;
                        if (this.getGameState().isOnServer() && !var2.admin && ((var4 = (FactionPermission)var3.getMembersUID().get(var2.initiator)) == null || !var4.hasHomebasePermission(var3))) {
                           try {
                              PlayerState var15;
                              (var15 = ((GameServerState)this.gameState.getState()).getPlayerFromName(var2.initiator)).sendServerMessage(new ServerMessage(new Object[]{281}, 1, var15.getId()));
                           } catch (PlayerNotFountException var7) {
                              System.err.println("[FACTION][HOMEBASE] player not found " + var2.initiator);
                           }

                           System.err.println("[FACTION][HOMEBASE][ERROR] no right to set base " + var2.initiator);
                        } else {
                           if (!var2.baseUID.startsWith("ENTITY_SPACESTATION") && !var2.baseUID.startsWith("ENTITY_PLANET")) {
                              System.err.println("[FACTION][HOMEBASE][ERROR] cannot make a home base " + var2.baseUID);
                              var2.baseUID = "";
                           }

                           if (this.getGameState().isOnServer()) {
                              FactionNewsPost var14;
                              if (var2.baseUID.length() <= 0) {
                                 var3.setHomebaseUID("");
                                 var3.getHomeSector().set(0, 0, 0);
                                 var3.setHomebaseRealName("");
                                 this.sendHomeBaseChange(var2.initiator, var2.factionId, "", new Vector3i(), "");
                                 var14 = new FactionNewsPost();
                                 if (var2.admin) {
                                    var14.set(var2.factionId, "ADMIN", System.currentTimeMillis(), Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONMANAGER_19, Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONMANAGER_18, 0);
                                 } else {
                                    var14.set(var2.factionId, var2.initiator, System.currentTimeMillis(), Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONMANAGER_17, StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONMANAGER_20, var2.initiator), 0);
                                 }

                                 this.toAddFactionNewsPosts.add(var14);
                              } else {
                                 try {
                                    Sendable var10;
                                    if ((var10 = (Sendable)this.gameState.getState().getLocalAndRemoteObjectContainer().getUidObjectMap().get(var2.baseUID)) != null && ((GameServerState)this.gameState.getState()).getUniverse().getSector(((SimpleTransformableSendableObject)var10).getSectorId()) != null) {
                                       SimpleTransformableSendableObject var12 = (SimpleTransformableSendableObject)var10;
                                       Sector var13 = ((GameServerState)this.gameState.getState()).getUniverse().getSector(var12.getSectorId());
                                       var3.setHomebaseUID(new String(var2.baseUID));
                                       var3.getHomeSector().set(new Vector3i(var13.pos));
                                       var3.setHomebaseRealName(new String(var12.getRealName()));
                                       this.sendHomeBaseChange(var2.initiator, var2.factionId, var2.baseUID, var3.getHomeSector(), var3.getHomebaseRealName());
                                    } else {
                                       try {
                                          List var11;
                                          if ((var11 = ((GameServerState)this.gameState.getState()).getDatabaseIndex().getTableManager().getEntityTable().getByUIDExact(DatabaseEntry.removePrefix(var2.baseUID), 1)).size() != 1) {
                                             throw new RuntimeException("Cannot set homebase on server: " + var2.baseUID + " was not found in sql database");
                                          }

                                          DatabaseEntry var5 = (DatabaseEntry)var11.get(0);
                                          var3.setHomebaseUID(new String(var2.baseUID));
                                          var3.getHomeSector().set(new Vector3i(var5.sectorPos));
                                          var3.setHomebaseRealName(new String(var5.realName));
                                          this.sendHomeBaseChange(var2.initiator, var2.factionId, var2.baseUID, var3.getHomeSector(), var3.getHomebaseRealName());
                                       } catch (SQLException var6) {
                                          var6.printStackTrace();
                                          ((GameServerState)this.gameState.getState()).getController().broadcastMessage(new Object[]{282}, 3);
                                          throw new RuntimeException("HOMEBASE CREATION FAIL: Cannot set homebase on server: " + var2.baseUID + " was not found in sql database");
                                       }
                                    }
                                 } catch (RuntimeException var8) {
                                    var4 = null;
                                    var8.printStackTrace();
                                 }

                                 (var14 = new FactionNewsPost()).set(var2.factionId, var2.initiator, System.currentTimeMillis(), "New Faction Home", "Faction Home set to " + var3.getHomebaseRealName() + " by " + var2.initiator + ".", 0);
                                 this.toAddFactionNewsPosts.add(var14);
                              }
                           } else {
                              var3.setHomebaseUID(new String(var2.baseUID));
                              var3.getHomeSector().set(new Vector3i(var2.homeVector));
                              var3.setHomebaseRealName(new String(var2.realName));
                              if (((GameClientState)this.getGameState().getState()).getPlayer().getFactionId() == var2.factionId) {
                                 if (var2.baseUID.length() > 0) {
                                    ((GameClientState)this.getGameState().getState()).getController().popupGameTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONMANAGER_15, var2.baseUID), 0.0F);
                                 } else {
                                    ((GameClientState)this.getGameState().getState()).getController().popupGameTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONMANAGER_16, 0.0F);
                                 }
                              }
                           }
                        }
                     } else {
                        System.err.println("[FACTION][HOMEBASE][ERROR] faction not found " + var2.factionId);
                     }
                  }

                  return;
               }
            }
         }
      }
   }

   private void sendServerFactionMail(String var1, String var2, Faction var3) {
      assert this.getGameState().getState() instanceof GameServerState;

      GameServerState var4 = (GameServerState)this.getGameState().getState();
      Iterator var6 = var3.getMembersUID().keySet().iterator();

      while(var6.hasNext()) {
         String var5 = (String)var6.next();
         var4.getServerPlayerMessager().send("[FACTION]", var5, var1, var2);
      }

   }

   public void updateToFullNetworkObject(NetworkGameState var1) {
      Iterator var3;
      synchronized(this.factionMap) {
         var3 = this.factionMap.values().iterator();

         while(true) {
            if (!var3.hasNext()) {
               break;
            }

            Faction var4 = (Faction)var3.next();
            var1.factionAdd.add(new RemoteFaction(var4, var1));
            this.getNews().get(var4.getIdFaction());
         }
      }

      synchronized(this.factionInvitations) {
         var3 = this.factionInvitations.iterator();

         while(true) {
            if (!var3.hasNext()) {
               break;
            }

            FactionInvite var9 = (FactionInvite)var3.next();
            this.gameState.getNetworkObject().factionInviteAdd.add(new RemoteFactionInvitation(var9, this.gameState.getNetworkObject()));
         }
      }

      synchronized(this.relationShipOffers) {
         var3 = this.relationShipOffers.values().iterator();

         while(true) {
            if (!var3.hasNext()) {
               break;
            }

            FactionRelationOffer var10 = (FactionRelationOffer)var3.next();
            this.gameState.getNetworkObject().factionRelationshipOffer.add((RemoteArray)var10.getRemoteArrayOffer(this.gameState.getNetworkObject()));
         }
      }

      synchronized(this.relations) {
         var3 = this.relations.values().iterator();

         while(true) {
            if (!var3.hasNext()) {
               break;
            }

            FactionRelation var11;
            if ((var11 = (FactionRelation)var3.next()).rel != FactionRelation.RType.NEUTRAL.code) {
               this.gameState.getNetworkObject().factionRelationships.add((RemoteArray)var11.getRemoteArray(this.gameState.getNetworkObject()));
            }
         }
      }

      this.npcFactionNews.updateToFullNetworkObject(var1);
   }

   public void updateToNetworkObject(NetworkGameState var1) {
      this.npcFactionNews.updateFromNetworkObject(var1);
   }

   public ArrayList getChangedFactions() {
      return this.changedFactions;
   }

   public void forceFactionPointTurn() {
      this.lastupdate = 0L;
   }

   public void resetAllActivity(int var1) throws FactionNotFoundException {
      if (!this.existsFaction(var1)) {
         throw new FactionNotFoundException(var1);
      } else {
         Faction var4;
         Iterator var2 = (var4 = this.getFaction(var1)).getMembersUID().values().iterator();

         while(var2.hasNext()) {
            FactionPermission var3;
            (var3 = (FactionPermission)var2.next()).activeMemberTime = 0L;
            var4.addOrModifyMember("ADMIN", var3.playerUID, var3.role, var3.activeMemberTime, this.getGameState(), true);
         }

      }
   }

   public void addNewsPostServer(FactionNewsPost var1) {
      assert this.getGameState().isOnServer();

      synchronized(this.toAddFactionNewsPosts) {
         this.toAddFactionNewsPosts.add(var1);
      }
   }

   public void addFactionSystemOwnerChangeServer(FactionSystemOwnerChange var1) {
      assert this.getGameState().isOnServer();

      synchronized(this.factionSystemOwnerToMod) {
         this.factionSystemOwnerToMod.add(var1);
      }
   }

   public boolean isInFaction(String var1, int var2) {
      Faction var3;
      return (var3 = this.getFaction(var2)) != null && var3.getMembersUID().containsKey(var1);
   }

   public String getFactionName(int var1) {
      return this.existsFaction(var1) ? this.getFaction(var1).getName() : Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONMANAGER_21;
   }

   public void setNPCFactionChanged() {
      this.npcFactionChanged = true;
   }

   public void checkNPCFactionSendingDebug(boolean var1) {
      if (this.npcFactionChanged || var1) {
         Iterator var2 = this.factionMap.values().iterator();

         while(var2.hasNext()) {
            Faction var3;
            if ((var3 = (Faction)var2.next()) instanceof NPCFaction) {
               ((NPCFaction)var3).checkNPCFactionSendingDebug(((GameStateInterface)this.getState()).getGameState(), var1);
            }
         }

         if (!var1) {
            this.npcFactionChanged = false;
         }
      }

   }

   public static boolean isNPCFactionOrPirateOrTrader(int var0) {
      return var0 < 0;
   }

   public static boolean isNPCFaction(int var0) {
      return var0 >= -10000000 && var0 < -10000;
   }

   public void scheduleActionCheck(int var1) {
      this.actionCheck.add(var1);
   }

   public ArrayList getToAddFactionRelationOfferAccepts() {
      return this.toAddFactionRelationOfferAccepts;
   }

   public ObjectArrayList getToModPersonalEnemies() {
      return this.toModPersonalEnemies;
   }

   public ArrayList getRelationOffersToAdd() {
      return this.relationOffersToAdd;
   }

   public NPCFactionNews getNpcFactionNews() {
      return this.npcFactionNews;
   }

   public void onSectorAdded(Sector var1) {
   }

   public void onSectorRemoved(Sector var1) {
   }

   public void onSectorEntityAdded(SimpleTransformableSendableObject var1, Sector var2) {
      Iterator var3 = this.getFactionCollection().iterator();

      while(var3.hasNext()) {
         ((Faction)var3.next()).onAddedSectorSynched(var2);
      }

   }

   public void onSectorEntityRemoved(SimpleTransformableSendableObject var1, Sector var2) {
      Iterator var3 = this.getFactionCollection().iterator();

      while(var3.hasNext()) {
         ((Faction)var3.next()).onRemovedSectorSynched(var2);
      }

   }

   public static String getFactionName(SimpleTransformableSendableObject var0) {
      Faction var1;
      return (var1 = ((FactionState)var0.getState()).getFactionManager().getFaction(var0.getFactionId())) != null ? var1.getName() : Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTIONMANAGER_31;
   }

   public void flagHomeBaseChanged(Faction var1) {
      this.flagHomebaseChanged = var1;
   }
}
