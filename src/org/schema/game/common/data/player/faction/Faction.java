package org.schema.game.common.data.player.faction;

import it.unimi.dsi.fastutil.ints.Int2LongOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectAVLTreeMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.awt.Color;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.config.ConfigParserException;
import org.schema.common.util.ColorTools;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.controller.database.FogOfWarReceiver;
import org.schema.game.common.controller.rules.rules.FactionRuleEntityManager;
import org.schema.game.common.data.SendableGameState;
import org.schema.game.common.data.player.FogOfWarController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.config.FactionPointGalaxyConfig;
import org.schema.game.common.data.player.faction.config.FactionPointIncomeConfig;
import org.schema.game.common.data.player.faction.config.FactionPointSpendingConfig;
import org.schema.game.common.data.player.faction.config.FactionPointsGeneralConfig;
import org.schema.game.common.data.world.RuleEntityContainer;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.network.objects.NTRuleInterface;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.game.server.data.simulation.npc.diplomacy.DiplomacyAction;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.TopLevelType;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.objects.remote.RemoteArray;
import org.schema.schine.network.objects.remote.RemoteBoolean;
import org.schema.schine.network.objects.remote.RemoteStringArray;
import org.schema.schine.network.objects.remote.RemoteVector3i;
import org.schema.schine.network.objects.remote.Streamable;
import org.schema.schine.network.server.ServerMessage;
import org.schema.schine.network.server.ServerStateInterface;
import org.schema.schine.resource.UniqueInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.ListSpawnObjectCallback;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class Faction implements FogOfWarReceiver, RuleEntityContainer, UniqueInterface, TagSerializable {
   public static final int MODE_FIGHTERS_TEAM = 1;
   public static final int MODE_FIGHTERS_FFA = 2;
   public static final int MODE_SPECTATORS = 4;
   public static final String NT_CODE_MEMBER_NAME_CHANGE = "MCN";
   public static final String NT_CODE_OPEN_TO_JOIN = "OTJ";
   public static final String NT_CODE_DESCRIPTION = "DES";
   public static final String NT_CODE_NAME = "NAM";
   public static final String NT_CODE_ATTACK_NEUTRAL = "ATN";
   public static final String NT_CODE_AUTO_DECLARE_WAR = "ADW";
   private static int colorGen = 16;
   public final Int2LongOpenHashMap sentWarDeclaration;
   private final Map membersUID;
   private final FactionRoles roles;
   private final Vector3i homeSector;
   private final ObjectOpenHashSet personalEnemies;
   public float factionPoints;
   public float lastPointsFromOnline;
   public float lastPointsFromOffline;
   public int lastinactivePlayer;
   public float lastPointsSpendOnCenterDistance;
   public float lastPointsSpendOnDistanceToHome;
   public float lastPointsSpendOnBaseRate;
   public float lastGalaxyRadius;
   public FactionAddCallback addHook;
   public int lastCountDeaths;
   public float lastLostPointAtDeaths;
   public int serverDeaths;
   public List lastSystemSectors;
   public final Set differenceSystemSectorsAdd;
   public final Set differenceSystemSectorsRemove;
   public int clientLastTurnSytemsCount;
   private String name;
   private String description;
   private int factionId;
   private long dateCreated;
   private String password;
   private boolean attackNeutral;
   private boolean allyNeutral;
   private boolean openToJoin;
   private boolean showInHub;
   private boolean autoDeclareWar;
   private String homebaseUID;
   private int factionMode;
   private Vector3f color;
   private String homebaseRealName;
   private float serverLostDeathPoints;
   private List lastSystemUIDs;
   private List lastSystems;
   private FogOfWarController fow;
   private StateInterface state;
   public final LongOpenHashSet attackedBy;
   private List enemiesCache;
   private List friendsCache;
   private FactionRuleEntityManager ruleEntityManager;

   public Faction(StateInterface var1) {
      this.sentWarDeclaration = new Int2LongOpenHashMap();
      this.membersUID = new Object2ObjectAVLTreeMap(String.CASE_INSENSITIVE_ORDER);
      this.homeSector = new Vector3i();
      this.personalEnemies = new ObjectOpenHashSet();
      this.lastSystemSectors = new ObjectArrayList();
      this.differenceSystemSectorsAdd = new ObjectOpenHashSet();
      this.differenceSystemSectorsRemove = new ObjectOpenHashSet();
      this.clientLastTurnSytemsCount = -1;
      this.password = "";
      this.showInHub = true;
      this.homebaseUID = "";
      this.color = new Vector3f(0.0F, 0.0F, 0.0F);
      this.homebaseRealName = "";
      this.lastSystemUIDs = new ObjectArrayList();
      this.lastSystems = new ObjectArrayList();
      this.attackedBy = new LongOpenHashSet();
      this.enemiesCache = new ObjectArrayList();
      this.friendsCache = new ObjectArrayList();
      this.roles = new FactionRoles();
      this.fow = new FogOfWarController(this);
      this.ruleEntityManager = new FactionRuleEntityManager(this);
      this.state = var1;
   }

   public void checkActions() {
      Iterator var1 = this.attackedBy.iterator();

      while(var1.hasNext()) {
         long var2 = (Long)var1.next();
         ((FactionState)this.getState()).getFactionManager().diplomacyAction(DiplomacyAction.DiplActionType.ATTACK, this.getIdFaction(), var2);
         Iterator var4 = this.getEnemies().iterator();

         while(var4.hasNext()) {
            Faction var5 = (Faction)var4.next();
            ((FactionState)this.getState()).getFactionManager().diplomacyAction(DiplomacyAction.DiplActionType.ATTACK_ENEMY, var5.factionId, var2);
         }
      }

      this.attackedBy.clear();
   }

   public List getEnemies() {
      this.enemiesCache.clear();
      Iterator var1 = ((FactionState)this.getState()).getFactionManager().getFactionMap().values().iterator();

      while(var1.hasNext()) {
         Faction var2 = (Faction)var1.next();
         if (((FactionState)this.getState()).getFactionManager().isEnemy(this.getIdFaction(), var2.getIdFaction())) {
            this.enemiesCache.add(var2);
         }
      }

      return this.enemiesCache;
   }

   public List getFriends() {
      this.friendsCache.clear();
      Iterator var1 = ((FactionState)this.getState()).getFactionManager().getFactionMap().values().iterator();

      while(var1.hasNext()) {
         Faction var2 = (Faction)var1.next();
         if (((FactionState)this.getState()).getFactionManager().isFriend(this.getIdFaction(), var2.getIdFaction())) {
            this.friendsCache.add(var2);
         }
      }

      return this.friendsCache;
   }

   public Faction(StateInterface var1, int var2, String var3, String var4) {
      this(var1);
      this.factionId = var2;
      this.roles.factionId = var2;
      this.name = var3;
      this.description = var4;
      this.dateCreated = System.currentTimeMillis();
      this.factionPoints = FactionPointsGeneralConfig.INITIAL_FACTION_POINTS;
      this.color = getNewColor();
   }

   public static synchronized Vector3f getNewColor() {
      Color var0 = ColorTools.getColor(colorGen++);
      return new Vector3f((float)var0.getRed() / 255.0F, (float)var0.getGreen() / 255.0F, (float)var0.getBlue() / 255.0F);
   }

   public int hashCode() {
      return this.factionId;
   }

   public boolean equals(Object var1) {
      return var1 instanceof Faction && this.factionId == ((Faction)var1).factionId;
   }

   public String toString() {
      return "Faction [id=" + this.factionId + ", name=" + this.name + ", description=" + this.description + ", size: " + this.membersUID.size() + "; FP: " + (int)this.factionPoints + "]";
   }

   public void addOrModifyMember(String var1, String var2, byte var3, long var4, SendableGameState var6, boolean var7) {
      synchronized(this.membersUID) {
         FactionPermission var9;
         if ((var9 = (FactionPermission)this.membersUID.get(var2)) == null) {
            this.membersUID.put(var2, new FactionPermission(var2, var3, var4));
            if (var6.isOnServer()) {
               System.err.println("[FACTION] Added to members " + var2 + " perm(" + var3 + ") of " + this + " on " + var6.getState());
            }
         } else {
            boolean var10 = var9.hasFogOfWarPermission(this);
            var9.role = var3;
            boolean var11 = var9.hasFogOfWarPermission(this);
            if (!var6.isOnServer()) {
               var9.activeMemberTime = var4;
            } else if (var10 && !var11) {
               this.fogOfWarUpdateServer(var9);
            }
         }
      }

      if (var7 && var6.isOnServer()) {
         RemoteStringArray var8;
         (var8 = new RemoteStringArray(5, var6.getNetworkObject())).set(0, (String)var2);
         var8.set(1, (String)String.valueOf(this.factionId));
         var8.set(2, (String)String.valueOf(var3));
         var8.set(3, (String)var1);
         var8.set(4, (String)String.valueOf(((FactionPermission)this.membersUID.get(var2)).activeMemberTime));
         var6.getNetworkObject().factionMemberMod.add((RemoteArray)var8);
      }

   }

   public FactionRelation.RType getRelationshipWithFactionOrPlayer(long var1) {
      assert this.isOnServer();

      if (var1 >= 2147483647L) {
         if (this.isOnServer()) {
            String var3 = ((GameServerState)this.getState()).getPlayerNameFromDbIdLowerCase(var1);
            if (this.getPersonalEnemies().contains(var3)) {
               return FactionRelation.RType.ENEMY;
            }
         } else {
            Iterator var5 = ((GameClientState)this.getState()).getLocalAndRemoteObjectContainer().getLocalObjectsByTopLvlType(TopLevelType.PLAYER).values().iterator();

            while(var5.hasNext()) {
               Sendable var4;
               if ((var4 = (Sendable)var5.next()) instanceof PlayerState && ((PlayerState)var4).getDbId() == var1 && this.getPersonalEnemies().contains(((PlayerState)var4).getName().toLowerCase(Locale.ENGLISH))) {
                  return FactionRelation.RType.ENEMY;
               }
            }
         }
      } else {
         Faction var6;
         if ((var6 = ((FactionState)this.getState()).getFactionManager().getFaction((int)var1)) != null) {
            return ((FactionState)this.getState()).getFactionManager().getRelation(this.getIdFaction(), var6.getIdFaction());
         }
      }

      return FactionRelation.RType.NEUTRAL;
   }

   public boolean isFactionMode(int var1) {
      return (this.factionMode & var1) == var1;
   }

   public void setFactionMode(int var1, boolean var2) {
      if (var2) {
         this.factionMode |= var1;
      } else {
         this.factionMode &= ~var1;
      }
   }

   public void addOrModifyMemberClientRequest(String var1, byte var2, SendableGameState var3) {
      RemoteStringArray var4;
      (var4 = new RemoteStringArray(5, var3.getNetworkObject())).set(0, (String)var1);
      var4.set(1, (String)String.valueOf(this.factionId));
      var4.set(2, (String)String.valueOf(var2));
      var4.set(3, (String)var1);
      var4.set(4, (String)"0");
      var3.getNetworkObject().factionMemberMod.add((RemoteArray)var4);
   }

   public void addOrModifyMemberClientRequest(String var1, String var2, byte var3, SendableGameState var4) {
      System.err.println("[CLIENT][Faction] Sending modding of member " + var2 + " from " + this);
      RemoteStringArray var5;
      (var5 = new RemoteStringArray(5, var4.getNetworkObject())).set(0, (String)var2);
      var5.set(1, (String)String.valueOf(this.factionId));
      var5.set(2, (String)String.valueOf(var3));
      var5.set(3, (String)var1);
      var5.set(4, (String)"0");
      var4.getNetworkObject().factionMemberMod.add((RemoteArray)var5);
   }

   public void clientRequestAttackNeutral(PlayerState var1, boolean var2) {
      var1.getNetworkObject().requestAttackNeutral.add((Streamable)(new RemoteBoolean(var2, var1.getNetworkObject())));
   }

   public void clientRequestAutoDeclareWar(PlayerState var1, boolean var2) {
      var1.getNetworkObject().requestAutoDeclareWar.add((Streamable)(new RemoteBoolean(var2, var1.getNetworkObject())));
   }

   public void clientRequestOpenFaction(PlayerState var1, boolean var2) {
      var1.getNetworkObject().requestFactionOpenToJoin.add((Streamable)(new RemoteBoolean(var2, var1.getNetworkObject())));
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var6;
      (var6 = (Tag[])var1.getValue())[0].getString();
      this.name = var6[1].getString();
      this.setDescription(var6[2].getString());
      this.dateCreated = var6[3].getLong();
      Tag[] var2 = var6[4].getStruct();

      for(int var3 = 0; var3 < var2.length - 1; ++var3) {
         FactionPermission var4;
         (var4 = new FactionPermission()).fromTagStructure(var2[var3]);
         this.membersUID.put(var4.playerUID, var4);
      }

      this.setOpenToJoin((Byte)var6[5].getValue() == 1);
      this.getRoles().fromTagStructure(var6[6]);
      this.setHomebaseUID((String)var6[7].getValue());
      if (FactionManager.isNPCFaction(this.getIdFaction()) && (String)var6[7].getValue() != null) {
         this.setHomebaseUID(SimpleTransformableSendableObject.EntityType.SPACE_STATION.dbPrefix + DatabaseEntry.removePrefixWOException((String)var6[7].getValue()));
      }

      this.password = (String)var6[8].getValue();
      this.factionId = (Integer)var6[9].getValue();
      this.setAllyNeutral(var6[10].getBoolean());
      this.setAttackNeutral(var6[11].getBoolean());
      this.getHomeSector().set(var6[12].getVector3i());
      if (var6.length > 13 && var6[13].getType() == Tag.Type.BYTE) {
         this.setAutoDeclareWar(var6[13].getBoolean());
      }

      if (var6.length > 14 && var6[14].getType() != Tag.Type.FINISH) {
         Tag.listFromTagStruct((Collection)this.personalEnemies, (Tag[])var6[14].getStruct(), (ListSpawnObjectCallback)(new ListSpawnObjectCallback() {
            public String get(Object var1) {
               return ((String)var1).toLowerCase(Locale.ENGLISH);
            }
         }));
      }

      if (var6.length > 15 && var6[15].getType() != Tag.Type.FINISH) {
         this.factionMode = var6[15].getInt();
      }

      if (var6.length > 16 && var6[16].getType() != Tag.Type.FINISH) {
         this.color = var6[16].getVector3f();
         if (this.color.length() == 0.0F || this.color.x == 0.5019608F && (double)this.color.y == 1.0D && (double)this.color.z == 1.0D) {
            this.color = getNewColor();
         }
      }

      if (var6.length > 17 && var6[17].getType() != Tag.Type.FINISH) {
         this.showInHub = var6[17].getBoolean();
      }

      if (var6.length > 25 && var6[25].getType() != Tag.Type.FINISH) {
         this.factionPoints = var6[18].getFloat();
         this.lastinactivePlayer = var6[19].getInt();
         this.lastGalaxyRadius = var6[20].getFloat();
         this.lastPointsFromOffline = var6[21].getFloat();
         this.lastPointsFromOnline = var6[22].getFloat();
         this.lastPointsSpendOnBaseRate = var6[23].getFloat();
         this.lastPointsSpendOnCenterDistance = var6[24].getFloat();
         this.lastPointsSpendOnDistanceToHome = var6[25].getFloat();
      }

      if (var6.length > 27 && var6[27].getType() != Tag.Type.FINISH) {
         this.lastCountDeaths = var6[26].getInt();
         this.lastLostPointAtDeaths = var6[27].getFloat();
      }

      if (var6.length > 28 && var6[28].getType() != Tag.Type.FINISH) {
         Tag.listFromTagStruct(this.lastSystems, (Tag)var6[28]);
      }

      if (var6.length > 29 && var6[29].getType() != Tag.Type.FINISH) {
         try {
            this.fromTagAdditionalInfo(var6[29]);
         } catch (Exception var5) {
            throw new RuntimeException(var5);
         }
      }
   }

   public Tag toTagStructure() {
      Tag[] var1;
      (var1 = new Tag[31])[0] = new Tag(Tag.Type.STRING, (String)null, "");
      var1[1] = new Tag(Tag.Type.STRING, (String)null, this.name);
      var1[2] = new Tag(Tag.Type.STRING, (String)null, this.getDescription());
      var1[3] = new Tag(Tag.Type.LONG, (String)null, this.dateCreated);
      ObjectArrayList var2 = new ObjectArrayList(this.membersUID.values());
      var1[4] = Tag.listToTagStruct((List)var2, "mem");
      var1[5] = new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.isOpenToJoin() ? 1 : 0)));
      var1[6] = this.getRoles().toTagStructure();
      var1[7] = new Tag(Tag.Type.STRING, "home", this.getHomebaseUID());
      var1[8] = new Tag(Tag.Type.STRING, "pw", this.password);
      var1[9] = new Tag(Tag.Type.INT, "id", this.factionId);
      var1[10] = new Tag(Tag.Type.BYTE, "fn", Byte.valueOf((byte)(this.isAllyNeutral() ? 1 : 0)));
      var1[11] = new Tag(Tag.Type.BYTE, "en", Byte.valueOf((byte)(this.isAttackNeutral() ? 1 : 0)));
      var1[12] = new Tag(Tag.Type.VECTOR3i, (String)null, this.getHomeSector());
      var1[13] = new Tag(Tag.Type.BYTE, "aw", Byte.valueOf((byte)(this.isAutoDeclareWar() ? 1 : 0)));
      var2 = new ObjectArrayList(this.personalEnemies);
      var1[14] = Tag.listToTagStruct((List)var2, Tag.Type.STRING, "mem");
      var1[15] = new Tag(Tag.Type.INT, (String)null, this.factionMode);
      var1[16] = new Tag(Tag.Type.VECTOR3f, (String)null, this.color);
      var1[17] = new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.showInHub ? 1 : 0)));
      var1[18] = new Tag(Tag.Type.FLOAT, (String)null, this.factionPoints);
      var1[19] = new Tag(Tag.Type.INT, (String)null, this.lastinactivePlayer);
      var1[20] = new Tag(Tag.Type.FLOAT, (String)null, this.lastGalaxyRadius);
      var1[21] = new Tag(Tag.Type.FLOAT, (String)null, this.lastPointsFromOffline);
      var1[22] = new Tag(Tag.Type.FLOAT, (String)null, this.lastPointsFromOnline);
      var1[23] = new Tag(Tag.Type.FLOAT, (String)null, this.lastPointsSpendOnBaseRate);
      var1[24] = new Tag(Tag.Type.FLOAT, (String)null, this.lastPointsSpendOnCenterDistance);
      var1[25] = new Tag(Tag.Type.FLOAT, (String)null, this.lastPointsSpendOnDistanceToHome);
      var1[26] = new Tag(Tag.Type.INT, (String)null, this.lastCountDeaths);
      var1[27] = new Tag(Tag.Type.FLOAT, (String)null, this.lastLostPointAtDeaths);
      var2 = new ObjectArrayList(this.lastSystems);
      var1[28] = Tag.listToTagStruct((List)var2, Tag.Type.VECTOR3i, (String)null);
      var1[29] = this.toTagAdditionalInfo();
      var1[30] = FinishTag.INST;
      return new Tag(Tag.Type.STRUCT, "f0", var1);
   }

   protected void fromTagAdditionalInfo(Tag var1) throws IllegalArgumentException, IllegalAccessException, ConfigParserException {
   }

   protected Tag toTagAdditionalInfo() {
      return new Tag(Tag.Type.BYTE, (String)null, (byte)0);
   }

   public void initializeWithState(GameServerState var1) throws IllegalArgumentException, IllegalAccessException, ConfigParserException {
   }

   public void setHomeParamsFromUID(GameServerState var1) {
      if (this.getHomebaseUID().length() > 0) {
         try {
            try {
               List var4;
               if ((var4 = var1.getDatabaseIndex().getTableManager().getEntityTable().getByUIDExact(DatabaseEntry.removePrefixWOException(this.getHomebaseUID()), 1)).size() == 1) {
                  DatabaseEntry var5 = (DatabaseEntry)var4.get(0);
                  this.getHomeSector().set(new Vector3i(var5.sectorPos));
                  this.setHomebaseRealName(new String(var5.realName));
                  return;
               }

               throw new RuntimeException("Cannot set homebase on server: " + this.getHomebaseUID() + " was not found in sql database");
            } catch (SQLException var2) {
               var2.printStackTrace();
               throw new RuntimeException("Cannot set homebase on server: " + this.getHomebaseUID() + " was not found in sql database");
            }
         } catch (RuntimeException var3) {
            var3.printStackTrace();
            this.setHomebaseUID("");
            this.getHomeSector().set(0, 0, 0);
            this.setHomebaseRealName("");
         }
      }

   }

   public String getDescription() {
      return this.description;
   }

   public void setDescription(String var1) {
      this.description = var1;
   }

   public String getHomebaseUID() {
      return this.homebaseUID;
   }

   public void setHomebaseUID(String var1) {
      this.homebaseUID = var1;
      if (this.getState() instanceof FactionState && ((FactionState)this.getState()).getFactionManager() != null) {
         ((FactionState)this.getState()).getFactionManager().flagHomeBaseChanged(this);
      }

   }

   public Vector3i getHomeSector() {
      return this.homeSector;
   }

   public int getIdFaction() {
      return this.factionId;
   }

   public void setIdFaction(int var1) {
      this.factionId = var1;
      this.roles.factionId = var1;
   }

   public Map getMembersUID() {
      return this.membersUID;
   }

   public String getName() {
      return this.name;
   }

   public void setName(String var1) {
      this.name = var1;
   }

   public FactionRoles getRoles() {
      return this.roles;
   }

   public String getUniqueIdentifier() {
      return "FACTION_" + this.getIdFaction();
   }

   public boolean isVolatile() {
      return false;
   }

   public boolean isAllyNeutral() {
      return this.allyNeutral;
   }

   public void setAllyNeutral(boolean var1) {
      this.allyNeutral = var1;
   }

   public boolean isAttackNeutral() {
      return this.attackNeutral;
   }

   public void setAttackNeutral(boolean var1) {
      this.attackNeutral = var1;
   }

   public boolean isAutoDeclareWar() {
      return this.autoDeclareWar;
   }

   public void setAutoDeclareWar(boolean var1) {
      this.autoDeclareWar = var1;
   }

   public boolean isOpenToJoin() {
      return this.openToJoin;
   }

   public void setOpenToJoin(boolean var1) {
      this.openToJoin = var1;
   }

   public void kickMemberClientRequest(String var1, String var2, SendableGameState var3) {
      System.err.println("[CLIENT][Faction] Sending removal of member " + var2 + " from " + this);
      RemoteStringArray var4;
      (var4 = new RemoteStringArray(3, var3.getNetworkObject())).set(0, (String)var2);
      var4.set(1, (String)String.valueOf(this.factionId));
      var4.set(2, (String)var1);
      var3.getNetworkObject().factionkickMemberRequests.add((RemoteArray)var4);
   }

   public int getFactionIdOfAttacker(Damager var1) {
      int var2;
      if ((var2 = ((SimpleTransformableSendableObject)var1).getFactionId()) == 0 && var1.getOwnerState() != null) {
         var2 = var1.getOwnerState().getFactionId();
      }

      return var2;
   }

   public void onAttackOnServer(Damager var1) {
      assert this.isOnServer();

      if (var1 instanceof SimpleTransformableSendableObject) {
         this.kickMemberOnFriendlyFire(var1);
         if (this.isAutoDeclareWar()) {
            this.declareWarOrPersonalEnemyOnHostileAction(var1);
         }

         if (var1 != null) {
            long var2;
            if (var1.getOwnerState() != null && var1.getOwnerState() instanceof PlayerState) {
               var2 = ((PlayerState)var1.getOwnerState()).getDbId();
            } else {
               var2 = (long)var1.getFactionId();
            }

            this.attackedBy.add(var2);
            ((FactionState)this.getState()).getFactionManager().scheduleActionCheck(this.getIdFaction());
         }
      }

   }

   public void kickMemberOnFriendlyFire(Damager var1) {
      assert this.isOnServer();

      if (var1.getOwnerState() != null && var1.getOwnerState() instanceof PlayerState && var1.getOwnerState().getFactionId() == this.getIdFaction()) {
         PlayerState var2 = (PlayerState)var1.getOwnerState();
         if (this.getRoles().hasKickOnFriendlyFire(((PlayerState)var1.getOwnerState()).getFactionRights()) && System.currentTimeMillis() - var2.lastFriendlyFireStrike > 1000L) {
            if (var2.friendlyFireStrikes >= 2) {
               var2.sendServerMessagePlayerError(new Object[]{257});
               Object[] var3 = new Object[]{258, var2.getName()};
               ((GameServerState)var1.getState()).getController().broadcastMessage(var3, 3);
               ((GameServerState)var1.getState()).getFactionManager().removeMemberOfFaction(this.getIdFaction(), var2);
            } else {
               ++var2.friendlyFireStrikes;
               var2.sendServerMessagePlayerError(new Object[]{259, var2.friendlyFireStrikes, 3});
            }

            var2.lastFriendlyFireStrike = System.currentTimeMillis();
         }
      }

   }

   public void declareWarOrPersonalEnemyOnHostileAction(Damager var1) {
      assert this.isOnServer();

      FactionManager var2 = ((FactionState)var1.getState()).getFactionManager();
      int var3;
      if ((var3 = this.getFactionIdOfAttacker(var1)) != -2) {
         if (this.getIdFaction() != var3 && var2.isNeutral(this.getIdFaction(), var3)) {
            if (var3 == 0 || var2.getFaction(var3) == null) {
               this.addPersonalElemy(var1);
               return;
            }

            this.declareFactionWarOnHostileAction(var1);
         }

      }
   }

   public void declareWarAgainstEntity(long var1) {
      assert this.isOnServer();

      if (var1 >= 2147483647L) {
         String var7 = ((GameServerState)this.state).getPlayerNameFromDbIdLowerCase(var1);
         this.personalEnemies.add(var7.toLowerCase(Locale.ENGLISH));
         PlayerState var6;
         if ((var6 = (PlayerState)((GameServerState)this.state).getPlayerStatesByDbId().get(var1)) != null) {
            var6.sendServerMessagePlayerError(new Object[]{260, this.getName()});
         }

      } else {
         Faction var3;
         if ((var3 = ((GameServerState)this.state).getFactionManager().getFaction((int)var1)) != null && this.getRelationshipWithFactionOrPlayer(var1) != FactionRelation.RType.ENEMY) {
            this.sentWarDeclaration.put(var3.getIdFaction(), System.currentTimeMillis());
            FactionRelationOffer var5 = new FactionRelationOffer();
            String var2 = Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTION_12;
            Object[] var4 = new Object[]{261, this.getName(), var3.getName()};
            var3.broadcastMessage(var4, 3, (GameServerState)this.state);
            var5.set("ADMIN", this.getIdFaction(), var3.getIdFaction(), FactionRelation.RType.ENEMY.code, var2, false);
            ((GameServerState)this.state).getFactionManager().relationShipOfferServer(var5);
         }

      }
   }

   public void declareFactionWarOnHostileAction(Damager var1) {
      assert this.isOnServer();

      FactionManager var2 = ((FactionState)var1.getState()).getFactionManager();
      int var3;
      if ((var3 = this.getFactionIdOfAttacker(var1)) != -2) {
         Faction var4;
         if ((var4 = var2.getFaction(var3)) == null) {
            System.err.println("ERROR: Attacker had no faction. should be personal enemy: " + var1);

            assert false;

         } else {
            long var5;
            if (((var5 = this.sentWarDeclaration.get(var3)) <= 0L || System.currentTimeMillis() - var5 >= 5000L) && !var2.isEnemy(this.getIdFaction(), var4.getIdFaction()) && !var2.isFriend(this.getIdFaction(), var4.getIdFaction())) {
               this.sentWarDeclaration.put(var3, System.currentTimeMillis());
               FactionRelationOffer var8 = new FactionRelationOffer();
               String var6 = StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTION_4, var1);
               if (var1 instanceof PlayerControllable && ((PlayerControllable)var1).getAttachedPlayers().size() > 0) {
                  var6 = var6 + StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTION_5, ((PlayerState)((PlayerControllable)var1).getAttachedPlayers().get(0)).getName());
               }

               Object[] var7 = new Object[]{262, this.getName(), var4.getName()};
               ((GameServerState)var1.getState()).getController().broadcastMessage(var7, 3);
               var8.set("ADMIN", this.getIdFaction(), var3, FactionRelation.RType.ENEMY.code, var6, false);
               System.err.println("[SERVER][FACTION] Hostlie action detected from " + var1 + " of Faction " + var4 + ": DECLARING WAR");
               var2.relationShipOfferServer(var8);
            }

         }
      }
   }

   public void addPersonalElemy(Damager var1) {
      assert this.isOnServer();

      FactionManager var2 = ((FactionState)var1.getState()).getFactionManager();
      if (var1.getOwnerState() != null && var1.getOwnerState() instanceof PlayerState && var1.getOwnerState().getFactionId() != this.getIdFaction() && !this.personalEnemies.contains(var1.getOwnerState().getName().toLowerCase(Locale.ENGLISH))) {
         this.personalEnemies.add(var1.getOwnerState().getName().toLowerCase(Locale.ENGLISH));
         var2.sendPersonalEnemyAdd("SERVER", this, var1.getOwnerState().getName().toLowerCase(Locale.ENGLISH));
         Object[] var3 = new Object[]{263, this.getName(), var1.getOwnerState().getName()};
         ((GameServerState)var1.getState()).getController().broadcastMessage(var3, 3);
      }

   }

   public boolean isOnServer() {
      return this.state instanceof ServerStateInterface;
   }

   public void removeMember(String var1, SendableGameState var2) {
      synchronized(this.membersUID) {
         FactionPermission var10000 = (FactionPermission)this.membersUID.remove(var1);
         RemoteStringArray var4 = null;
         if (var10000 != null) {
            if (var2.isOnServer()) {
               System.err.println("[SERVER][Faction] Sending removal of member " + var1 + " from " + this);
               (var4 = new RemoteStringArray(5, var2.getNetworkObject())).set(0, (String)var1);
               var4.set(1, (String)String.valueOf(this.factionId));
               var4.set(2, (String)"r");
               var4.set(3, (String)var1);
               var4.set(4, (String)"0");
               var2.getNetworkObject().factionMemberMod.add((RemoteArray)var4);
            }
         } else {
            System.err.println("[Faction] WARNING: could not remove " + var1 + " from " + this.membersUID + " on " + var2.getState());
         }

      }
   }

   public void removeMemberClientRequest(String var1, String var2, SendableGameState var3) {
      System.err.println("[CLIENT][Faction] Sending removal of member " + var2 + " from " + this);
      RemoteStringArray var4;
      (var4 = new RemoteStringArray(5, var3.getNetworkObject())).set(0, (String)var2);
      var4.set(1, (String)String.valueOf(this.factionId));
      var4.set(2, (String)"r");
      var4.set(3, (String)var1);
      var4.set(4, (String)"0");
      var3.getNetworkObject().factionMemberMod.add((RemoteArray)var4);
   }

   public void sendAttackNeutralMod(String var1, boolean var2, SendableGameState var3) {
      this.sendMod(var1, "ATN", String.valueOf(var2), var3);
   }

   public void sendAutoDeclareWar(String var1, boolean var2, SendableGameState var3) {
      this.sendMod(var1, "ADW", String.valueOf(var2), var3);
   }

   public void sendDescriptionMod(String var1, String var2, SendableGameState var3) {
      this.sendMod(var1, "DES", var2, var3);
   }

   public void sendMod(String var1, String var2, String var3, SendableGameState var4) {
      RemoteStringArray var5;
      (var5 = new RemoteStringArray(4, var4.getNetworkObject())).set(0, (String)var1);
      var5.set(1, (String)String.valueOf(this.factionId));
      var5.set(2, (String)var2);
      var5.set(3, (String)var3);
      var4.getNetworkObject().factionMod.add((RemoteArray)var5);
   }

   public void sendFactionPointUpdate(SendableGameState var1) {
      FactionPointMod.send(this, var1);
   }

   public void sendNameMod(String var1, String var2, SendableGameState var3) {
      this.sendMod(var1, "NAM", var2, var3);
   }

   public void sendOpenToJoinMod(String var1, boolean var2, SendableGameState var3) {
      this.sendMod(var1, "OTJ", String.valueOf(var2), var3);
   }

   public void sendMemberNameChangeMod(String var1, String var2, SendableGameState var3) {
      this.sendMod(var1, "MCN", var2, var3);
   }

   public void deserializeMembers(DataInputStream var1) throws IOException {
      int var2 = var1.readInt();

      for(int var3 = 0; var3 < var2; ++var3) {
         String var4 = var1.readUTF();
         byte var5 = var1.readByte();
         long var6 = var1.readLong();
         FactionPermission var8 = new FactionPermission(var4, var5, var6);
         this.membersUID.put(var8.playerUID, var8);
      }

   }

   public void serializeMembers(DataOutputStream var1) throws IOException {
      var1.writeInt(this.membersUID.size());
      Iterator var2 = this.membersUID.values().iterator();

      while(var2.hasNext()) {
         FactionPermission var3 = (FactionPermission)var2.next();
         var1.writeUTF(var3.playerUID);
         var1.writeByte(var3.role);
         var1.writeLong(var3.activeMemberTime);
      }

   }

   public void deserializePersonalEnemies(DataInputStream var1) throws IOException {
      int var2 = var1.readInt();

      for(int var3 = 0; var3 < var2; ++var3) {
         String var4 = var1.readUTF();
         this.personalEnemies.add(var4.toLowerCase(Locale.ENGLISH));
      }

   }

   public void serializePersonalEmenies(DataOutputStream var1) throws IOException {
      var1.writeInt(this.personalEnemies.size());
      Iterator var2 = this.personalEnemies.iterator();

      while(var2.hasNext()) {
         String var3 = (String)var2.next();
         var1.writeUTF(var3.toLowerCase(Locale.ENGLISH));
      }

   }

   public String getHomebaseRealName() {
      return this.homebaseRealName;
   }

   public void setHomebaseRealName(String var1) {
      this.homebaseRealName = var1;
   }

   public ObjectOpenHashSet getPersonalEnemies() {
      return this.personalEnemies;
   }

   public int getFactionMode() {
      return this.factionMode;
   }

   public void setFactionMode(int var1) {
      this.factionMode = var1;
   }

   public void setServerOpenForJoin(GameServerState var1, boolean var2) {
      this.setOpenToJoin(var2);
      RemoteStringArray var3;
      (var3 = new RemoteStringArray(4, var1.getGameState().getNetworkObject())).set(0, (String)"ADMIN");
      var3.set(1, (String)String.valueOf(this.getIdFaction()));
      var3.set(2, (String)"OTJ");
      var3.set(3, (String)String.valueOf(var2));
      System.err.println("SERVER SENDING FACTION CHANGE ###### OPEN TO JOIN " + var2);
      var1.getGameState().getNetworkObject().factionMod.add((RemoteArray)var3);
   }

   public Vector3f getColor() {
      return this.color;
   }

   public void setColor(Vector3f var1) {
      this.color = var1;
   }

   public void broadcastMessage(Object[] var1, int var2, GameServerState var3) {
      Iterator var5 = var3.getPlayerStatesByName().values().iterator();

      while(var5.hasNext()) {
         PlayerState var4;
         if ((var4 = (PlayerState)var5.next()).getFactionId() == this.getIdFaction()) {
            var4.sendServerMessage(new ServerMessage(var1, var2, var4.getId()));
         }
      }

   }

   public boolean isShowInHub() {
      return this.showInHub;
   }

   public void setShowInHub(boolean var1) {
      this.showInHub = var1;
   }

   public void handleActivityServer(FactionManager var1) {
      var1.getGameState().getState();
      this.lastCountDeaths = this.serverDeaths;
      this.lastLostPointAtDeaths = this.serverLostDeathPoints;
      this.serverLostDeathPoints = 0.0F;
      this.serverDeaths = 0;
   }

   public void handleFactionPointGainServer(FactionManager var1) {
      GameServerState var7 = (GameServerState)var1.getGameState().getState();
      float var2 = 0.0F;
      float var3 = 0.0F;
      int var4 = 0;
      Iterator var5 = this.membersUID.keySet().iterator();

      while(var5.hasNext()) {
         String var6 = (String)var5.next();
         if ((PlayerState)var7.getPlayerStatesByNameLowerCase().get(var6.toLowerCase(Locale.ENGLISH)) != null) {
            var2 += FactionPointIncomeConfig.FACTION_POINTS_PER_ONLINE_MEMBER;
         } else if (((FactionPermission)this.membersUID.get(var6)).isActiveMember()) {
            var3 += FactionPointIncomeConfig.FACTION_POINTS_PER_MEMBER;
         } else {
            ++var4;
         }
      }

      this.lastPointsFromOnline = var2;
      this.lastPointsFromOffline = var3;
      this.lastinactivePlayer = var4;
      float var8 = var3 + var2;
      this.factionPoints += var8;
   }

   public void handleFactionPointExpensesServer(FactionManager var1, Object2IntOpenHashMap var2) {
      float var3 = 0.0F;
      float var4 = 0.0F;
      float var5 = 0.0F;
      Vector3i var6 = StellarSystem.getPosFromSector(this.homeSector, new Vector3i());
      GameServerState var17 = (GameServerState)var1.getGameState().getState();
      this.differenceSystemSectorsAdd.clear();
      this.differenceSystemSectorsRemove.clear();
      this.differenceSystemSectorsRemove.addAll(this.lastSystemSectors);
      this.lastSystemSectors.clear();
      this.lastSystems.clear();
      this.lastSystemUIDs.clear();
      List var7 = var17.getDatabaseIndex().getTableManager().getSystemTable().getSystemsByFaction(this.factionId, this.lastSystems, this.lastSystemSectors, this.lastSystemUIDs);
      boolean var8 = this.getHomebaseUID().length() > 0;

      for(int var9 = 0; var9 < var7.size(); ++var9) {
         Vector3i var10 = (Vector3i)var7.get(var9);
         Vector3i var11 = (Vector3i)this.lastSystemSectors.get(var9);
         String var12;
         if ((var12 = (String)this.lastSystemUIDs.get(var9)).startsWith(SimpleTransformableSendableObject.EntityType.SPACE_STATION.dbPrefix)) {
            boolean var13 = true;
            Sendable var14;
            if ((var14 = (Sendable)var17.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var12)) != null && var14 instanceof SpaceStation) {
               SpaceStation var19 = (SpaceStation)var14;
               Sector var15;
               if ((var15 = var17.getUniverse().getSector(var19.getSectorId())) != null && var15.pos.equals(var11)) {
                  var13 = false;
               }
            }

            if (var13) {
               try {
                  List var21;
                  if ((var21 = var17.getDatabaseIndex().getTableManager().getEntityTable().getByUIDExact(DatabaseEntry.removePrefixWOException(var12), -1)).size() > 0 && ((DatabaseEntry)var21.get(0)).sectorPos.equals(var11)) {
                     var13 = false;
                  }
               } catch (SQLException var16) {
                  var16.printStackTrace();
               }
            }

            if (var13) {
               System.err.println("[SERVER][FACTION] Station for system " + var10 + " was no longer found. removing ownership from faction " + this);
               this.broadcastMessage(new Object[]{264, var10}, 3, var17);
               VoidSystem var26 = new VoidSystem();
               var17.getDatabaseIndex().getTableManager().getSystemTable().loadSystem(var17, new Vector3i(var10), var26);
               FactionSystemOwnerChange var25 = new FactionSystemOwnerChange("ADMIN(Auto Revoke on moved or destroyed Station)", 0, "", var26.getOwnerPos(), var26.getPos(), var26.getName());
               var17.getFactionManager().addFactionSystemOwnerChangeServer(var25);
               this.lastSystemSectors.remove(var9);
               this.lastSystems.remove(var9);
               this.lastSystemUIDs.remove(var9);
               --var9;
               continue;
            }
         }

         if (!var8) {
            var6.set(var10);
            var8 = true;
         }

         if (!this.differenceSystemSectorsRemove.remove(var11)) {
            this.differenceSystemSectorsAdd.add(var11);
         }

         Vector3i var20 = Galaxy.getContainingGalaxyFromSystemPos(var10, new Vector3i());
         Vector3i var22 = new Vector3i(var20.x * Galaxy.size - (Galaxy.halfSize - 1), var20.y * Galaxy.size - (Galaxy.halfSize - 1), var20.z * Galaxy.size - (Galaxy.halfSize - 1));
         Vector3i var23 = new Vector3i(var20.x * Galaxy.size + Galaxy.halfSize, var20.y * Galaxy.size + Galaxy.halfSize, var20.z * Galaxy.size + Galaxy.halfSize);
         int var24;
         if (!var2.containsKey(var20)) {
            var24 = var17.getDatabaseIndex().getTableManager().getSystemTable().getOwnedSystemCount(var22.x, var22.y, var22.z, var23.x, var23.y, var23.z);
            var2.put(var20, var24);
         } else {
            var24 = var2.getInt(var20);
         }

         if (!FactionPointGalaxyConfig.FREE_HOMEBASE || !var6.equals(var10)) {
            var3 += this.getSpendingDisToCenter(var10, var24);
            var4 += this.getSpendingDisToHome(var6, var10);
            var5 += FactionPointSpendingConfig.FACTION_POINTS_PER_CONTROLLED_SYSTEM;
         }
      }

      this.lastPointsSpendOnCenterDistance = var3;
      this.lastPointsSpendOnDistanceToHome = var4;
      this.lastPointsSpendOnBaseRate = var5;
      float var18 = var3 + var4 + var5 + FactionPointSpendingConfig.BASIC_FLAT_COST;
      this.factionPoints -= var18;
   }

   private float getSpendingDisToHome(Vector3i var1, Vector3i var2) {
      return Vector3fTools.length(var1, var2) * FactionPointGalaxyConfig.PENALTY_PER_DISTANCE_UNIT_FROM_HOMEBASE;
   }

   private float getSpendingDisToCenter(Vector3i var1, int var2) {
      (var1 = Galaxy.getLocalCoordinatesFromSystem(var1, new Vector3i())).sub(Galaxy.halfSize, Galaxy.halfSize, Galaxy.halfSize);
      float var4 = FastMath.carmackSqrt(FastMath.pow((float)var2, 2.0F) / 3.1415927F);
      this.lastGalaxyRadius = var4;
      float var3;
      if ((var3 = var1.length()) < var4) {
         return 0.0F;
      } else {
         var3 = (var3 - var4) * FactionPointGalaxyConfig.PENALTY_FROM_CENTER_MULT;

         assert var3 >= 0.0F : var3 + "; radius: " + var4;

         return var3;
      }
   }

   public void onPlayerDied(PlayerState var1, Damager var2) {
      if (var1 == var2) {
         System.err.println("[DEATH][FACTION] " + var1 + " Suicide is free");
         var1.sendServerMessage(new ServerMessage(new Object[]{265}, 0, var1.getId()));
      } else {
         float var3;
         if ((float)(System.currentTimeMillis() - var1.getLastDeathNotSuicideFactionProt()) < FactionPointsGeneralConfig.FACTION_POINT_DEATH_PROTECTION_MIN * 60.0F * 1000.0F) {
            var3 = (FactionPointsGeneralConfig.FACTION_POINT_DEATH_PROTECTION_MIN * 60.0F * 1000.0F - (float)(System.currentTimeMillis() - var1.getLastDeathNotSuicideFactionProt())) / 1000.0F / 60.0F;
            var1.sendServerMessage(new ServerMessage(new Object[]{266, StringTools.formatPointZeroZero(var3)}, 0, var1.getId()));
         } else {
            var3 = FactionPointSpendingConfig.FACTION_POINT_ABS_LOSS_PER_DEATH;
            float var4 = FactionPointSpendingConfig.FACTION_POINT_MULT_BY_MEMBERS_LOSS_PER_DEATH * (float)this.getMembersUID().size();
            this.serverLostDeathPoints += var3 + var4;
            this.factionPoints -= var3 + var4;
            FactionNewsPost var5;
            (var5 = new FactionNewsPost()).set(this.getIdFaction(), "Faction Point Reporter", System.currentTimeMillis(), Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTION_14, StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTION_15, var1.getName(), Math.round(var3 + var4), var2 == null ? "unknown" : var2.getName()), 0);
            ((GameServerState)var1.getState()).getFactionManager().addNewsPostServer(var5);
            ++this.serverDeaths;
            this.sendFactionPointUpdate(((GameServerState)var1.getState()).getGameState());
            var1.sendServerMessage(new ServerMessage(new Object[]{267, Math.round(var3 + var4)}, 0, var1.getId()));
         }
      }
   }

   public void hanldeDeficit(FactionManager var1, Object2IntOpenHashMap var2) {
      GameServerState var10 = (GameServerState)var1.getGameState().getState();
      if (this.factionPoints < 0.0F) {
         Vector3i var3 = null;
         float var4 = -1.0F;
         Vector3i var5 = StellarSystem.getPosFromSector(this.homeSector, new Vector3i());
         boolean var6 = this.getHomebaseUID().length() > 0;
         Iterator var7 = this.lastSystems.iterator();

         while(true) {
            Vector3i var8;
            float var9;
            do {
               if (!var7.hasNext()) {
                  if (var3 != null) {
                     VoidSystem var11 = new VoidSystem();
                     var10.getDatabaseIndex().getTableManager().getSystemTable().loadSystem(var10, var3, var11);
                     FactionSystemOwnerChange var12 = new FactionSystemOwnerChange("ADMIN", 0, "", var11.getOwnerPos(), var11.getPos(), var11.getName());
                     FactionNewsPost var13;
                     (var13 = new FactionNewsPost()).set(this.getIdFaction(), "Faction Point Reporter", System.currentTimeMillis(), Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTION_16, StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTION_17, var3.toString()), 0);
                     var1.addNewsPostServer(var13);
                     var1.addFactionSystemOwnerChangeServer(var12);
                     return;
                  }

                  (new FactionNewsPost()).set(this.getIdFaction(), "Faction Point Reporter", System.currentTimeMillis(), Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTION_18, Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FACTION_FACTION_19, 0);
                  return;
               }

               var8 = (Vector3i)var7.next();
               if (!var6) {
                  var5.set(var8);
                  var6 = true;
               }

               var9 = Vector3fTools.length(var8, var5);
            } while(var3 != null && var9 <= var4);

            var4 = var9;
            var3 = var8;
         }
      }
   }

   public FogOfWarController getFogOfWar() {
      return this.fow;
   }

   public long getFogOfWarId() {
      return (long)this.getIdFaction();
   }

   public StateInterface getState() {
      return this.state;
   }

   public void shareFogOfWar(Faction var1) {
      var1.getFogOfWar().merge(this);
   }

   private void fogOfWarUpdateServer(FactionPermission var1) {
      if (this.state instanceof GameServerState) {
         try {
            long var2;
            if ((var2 = ((GameServerState)this.state).getDatabaseIndex().getTableManager().getPlayerTable().getPlayerId(var1.playerUID)) >= 0L) {
               ((GameServerState)this.state).getDatabaseIndex().getTableManager().getVisibilityTable().mergeVisibility((long)this.getIdFaction(), var2);
            }

         } catch (SQLException var4) {
            var4.printStackTrace();
         }
      } else {
         ((GameClientState)this.state).getController().getClientChannel().getGalaxyManagerClient().resetClientVisibility();
      }
   }

   public void fogOfWarCheckServer(FactionPermission var1, FactionRoles var2) {
      if (this.getRoles().hasFogOfWarPermission(var1.role) && var2.hasFogOfWarPermission(var1.role)) {
         System.err.println("[SERVER][FACTION] fog of war check. Role " + var1.playerUID + " lost fog of war of faction. sharing current data");
         this.fogOfWarUpdateServer(var1);
      }

   }

   public void clearFogOfWar() {
      if (this.state instanceof GameServerState) {
         try {
            ((GameServerState)this.state).getDatabaseIndex().getTableManager().getVisibilityTable().clearVisibility(this.getFogOfWarId());
            return;
         } catch (SQLException var1) {
            var1.printStackTrace();
         }
      }

   }

   public void sendFowResetToClient(Vector3i var1) {
      Iterator var2 = this.getMembersUID().values().iterator();

      while(var2.hasNext()) {
         FactionPermission var3 = (FactionPermission)var2.next();

         try {
            ((GameServerState)this.state).getPlayerFromNameIgnoreCase(var3.playerUID).getNetworkObject().resetFowBuffer.add((Streamable)(new RemoteVector3i(var1, true)));
         } catch (PlayerNotFountException var4) {
            System.err.println("NOT ONLINE: " + var3.playerUID);
         }
      }

   }

   public void initialize() {
   }

   public void onEntityDestroyedServer(SegmentController var1) {
   }

   public void onEntityOverheatingServer(SegmentController var1) {
   }

   public void onAddedSectorSynched(Sector var1) {
   }

   public void onRemovedSectorSynched(Sector var1) {
   }

   public boolean isNPC() {
      return false;
   }

   public void serializeExtra(DataOutputStream var1) throws IOException {
   }

   public void deserializeExtra(DataInputStream var1) throws IOException {
   }

   public boolean isPlayerFaction() {
      return this.factionId > 0;
   }

   public List getOnlinePlayers() {
      assert this.isOnServer();

      if (this.isNPC()) {
         return new ObjectArrayList(0);
      } else {
         ObjectArrayList var1 = new ObjectArrayList();
         Iterator var2 = this.getMembersUID().values().iterator();

         while(var2.hasNext()) {
            FactionPermission var3 = (FactionPermission)var2.next();
            PlayerState var4;
            if ((var4 = ((GameServerState)this.getState()).getPlayerFromNameIgnoreCaseWOException(var3.playerUID.toLowerCase(Locale.ENGLISH))) != null) {
               var1.add(var4);
            }
         }

         return var1;
      }
   }

   public FactionRuleEntityManager getRuleEntityManager() {
      return this.ruleEntityManager;
   }

   public TopLevelType getTopLevelType() {
      return TopLevelType.FACTION;
   }

   public NTRuleInterface getNetworkObject() {
      return ((GameStateInterface)this.getState()).getGameState().getNetworkObject();
   }

   public void updateLocal(Timer var1) {
      this.ruleEntityManager.update(var1);
   }
}
