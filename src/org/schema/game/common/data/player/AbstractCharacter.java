package org.schema.game.common.data.player;

import com.bulletphysics.collision.dispatch.CollisionWorld.ClosestRayResultCallback;
import com.bulletphysics.collision.narrowphase.ManifoldPoint;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.Salvager;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.ShopperInterface;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.damage.HitReceiverType;
import org.schema.game.common.controller.damage.Hittable;
import org.schema.game.common.controller.damage.beam.DamageBeamHitHandler;
import org.schema.game.common.controller.damage.beam.DamageBeamHitHandlerCharacter;
import org.schema.game.common.controller.damage.beam.DamageBeamHittable;
import org.schema.game.common.controller.damage.effects.InterEffectContainer;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.controller.damage.projectile.ProjectileController;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.controller.elements.ParticleHandler;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.data.MetaObjectState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.blockeffects.config.ConfigEntityManager;
import org.schema.game.common.data.element.ActivationTrigger;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.BuildProhibiter;
import org.schema.game.common.data.element.meta.FlashLight;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.physics.CapsuleShapeExt;
import org.schema.game.common.data.physics.CollisionType;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.game.common.data.physics.KinematicCharacterControllerExt;
import org.schema.game.common.data.physics.PairCachingGhostObjectAlignable;
import org.schema.game.common.data.physics.PhysicsExt;
import org.schema.game.common.data.physics.RigidBodySegmentController;
import org.schema.game.common.data.world.GameTransformable;
import org.schema.game.common.data.world.LightTransformable;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.Universe;
import org.schema.game.common.util.Collisionable;
import org.schema.game.network.CharacterBlockActivation;
import org.schema.game.network.objects.NetworkPlayerCharacter;
import org.schema.game.network.objects.remote.RemoteCharacterBlockActivation;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndex;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndexElement;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.graphicsengine.forms.debug.DebugLine;
import org.schema.schine.network.Identifiable;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.TopLevelType;
import org.schema.schine.network.objects.LocalSectorTransition;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.server.ServerMessage;
import org.schema.schine.physics.Physical;
import org.schema.schine.resource.tag.TagSerializable;

public abstract class AbstractCharacter extends SimpleTransformableSendableObject implements BeamHandlerContainer, Salvager, ShopperInterface, Hittable, DamageBeamHittable, ParticleHandler, AbstractCharacterInterface, Destroyable, LightTransformable, Collisionable, Identifiable, TagSerializable {
   public static final long MEDICAL_REUSE_MS = 20000L;
   public static final long MEDICAL_HP = 10L;
   private final PersonalBeamHandler beamHandler;
   private final Set shopsInDistance = new HashSet();
   private final ObjectArrayFIFOQueue blockActivationReactions = new ObjectArrayFIFOQueue();
   public boolean actionUpdate;
   protected float stepHeight = 0.388F;
   protected KinematicCharacterControllerExt characterController;
   protected long lastAlignRequest;
   protected long lastAttach;
   private boolean hasSpawnedOnServer = false;
   private String uniqueIdentifier;
   private AnimationIndexElement animationState;
   private ConvexShape capsule;
   private long lastCollisionHurt;
   private long warpOutOfCollisionRequest;
   private PairCachingGhostObjectAlignable ghostObject;
   private long activatedStuckProtection;
   private long lastHit;
   private Vector3b tmpLocalPos;
   private int actionUpdateNum;
   private FlashLight flashLightActive;
   public boolean neverSpawnedBefore;
   private final ObjectArrayList listeners;
   private CharacterProvider sendableSegmentProvider;
   private DamageBeamHitHandler damageBeamHitHandler;

   public CollisionType getCollisionType() {
      return CollisionType.CHARACTER;
   }

   protected InterEffectContainer setupEffectContainer() {
      return new AbstractCharacter.CharacterEffectSet();
   }

   public void addListener(CharacterProvider var1) {
      this.listeners.add(var1);
   }

   public SimpleTransformableSendableObject getShootingEntity() {
      return this;
   }

   public CharacterProvider createNetworkListenEntity() {
      this.sendableSegmentProvider = new CharacterProvider(this.getState());
      this.sendableSegmentProvider.initialize();
      return this.sendableSegmentProvider;
   }

   public List getListeners() {
      return this.listeners;
   }

   public AbstractCharacter(StateInterface var1) {
      super(var1);
      this.animationState = AnimationIndex.IDLING_FLOATING;
      this.warpOutOfCollisionRequest = -1L;
      this.tmpLocalPos = new Vector3b();
      this.actionUpdateNum = -1;
      this.neverSpawnedBefore = true;
      this.listeners = new ObjectArrayList();
      this.damageBeamHitHandler = new DamageBeamHitHandlerCharacter();
      this.beamHandler = new PersonalBeamHandler(this, this);
   }

   public byte getFactionRights() {
      return this.getOwnerState().getFactionRights();
   }

   public byte getOwnerFactionRights() {
      return this.getOwnerState().getFactionRights();
   }

   public Vector4f getTint() {
      return this.getOwnerState().getTint();
   }

   public void setActionUpdate(boolean var1) {
      this.actionUpdate = var1;
   }

   public boolean isHit() {
      return this.getNetworkObject() != null && (Boolean)this.getNetworkObject().hit.get();
   }

   public boolean isSitting() {
      return this.getOwnerState().isSitting();
   }

   public Vector3i getSittingPos() {
      return this.getOwnerState().sittingPos;
   }

   public Vector3i getSittingPosTo() {
      return this.getOwnerState().sittingPosTo;
   }

   public Vector3i getSittingPosLegs() {
      return this.getOwnerState().sittingPosLegs;
   }

   public boolean isAnyBlockSelected() {
      return this.getOwnerState().getInventory().getType(this.getOwnerState().getSelectedBuildSlot()) > 0;
   }

   public boolean isAnyMetaObjectSelected() {
      return this.getOwnerState().getInventory().getType(this.getOwnerState().getSelectedBuildSlot()) < 0;
   }

   public Vector3f getOrientationUp() {
      return this.getCharacterController().upAxisDirection[1];
   }

   public Vector3f getOrientationRight() {
      return this.getCharacterController().upAxisDirection[0];
   }

   public Vector3f getOrientationForward() {
      return this.getCharacterController().upAxisDirection[2];
   }

   public boolean isMetaObjectSelected(MetaObjectManager.MetaObjectType var1, int var2) {
      int var3 = this.getOwnerState().getInventory().getMeta(this.getOwnerState().getSelectedBuildSlot());
      MetaObject var4;
      return (var4 = ((MetaObjectState)this.getState()).getMetaObjectManager().getObject(var3)) != null && (var4.getSubObjectId() == -1 || var2 == -1 || var2 == var4.getSubObjectId());
   }

   public void getClientAABB(Vector3f var1, Vector3f var2) {
      this.getPhysicsDataContainer().getShape().getAabb(this.getWorldTransformOnClient(), var1, var2);
   }

   public Vector3f getLinearVelocity(Vector3f var1) {
      return this.getCharacterController().getLinearVelocity(var1);
   }

   public abstract boolean isClientOwnObject();

   public abstract NetworkPlayerCharacter getNetworkObject();

   public void initFromNetworkObject(NetworkObject var1) {
      super.initFromNetworkObject(var1);
      NetworkPlayerCharacter var2 = (NetworkPlayerCharacter)var1;
      this.setUniqueIdentifier((String)var2.uniqueId.get());
   }

   public void updateFromNetworkObject(NetworkObject var1, int var2) {
      super.updateFromNetworkObject(var1, var2);
      ObjectArrayList var5;
      if (!(var5 = this.getNetworkObject().blockActivationsWithReaction.getReceiveBuffer()).isEmpty()) {
         synchronized(this.blockActivationReactions) {
            for(int var3 = 0; var3 < var5.size(); ++var3) {
               this.blockActivationReactions.enqueue(((RemoteCharacterBlockActivation)var5.get(var3)).get());
            }

         }
      }
   }

   public void updateLocal(Timer var1) {
      super.updateLocal(var1);
      if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
         this.drawDebugTransform();
      }

      this.beamHandler.update(var1);
      this.actionUpdate = false;
      this.getPhysicsDataContainer().getObject().activate(true);
      this.getWorldTransformInverse().set(this.getWorldTransform());
      this.getWorldTransformInverse().inverse();
      if (this.isOnServer() && var1.currentTime - this.lastHit > 400L && (Boolean)this.getNetworkObject().hit.get()) {
         this.getNetworkObject().hit.set(false);
      }

      this.setProhibitingBuildingAroundOrigin(0.0F);
      this.flashLightActive = null;

      for(int var2 = 0; var2 < this.getOwnerState().getInventory().getActiveSlotsMax(); ++var2) {
         int var3 = this.getOwnerState().getInventory().getMeta(var2);
         MetaObject var6;
         if ((var6 = ((MetaObjectState)this.getState()).getMetaObjectManager().getObject(var3)) != null && var6 instanceof BuildProhibiter && ((BuildProhibiter)var6).active) {
            this.setProhibitingBuildingAroundOrigin(((BuildProhibiter)var6).rangeRadius);
         }

         if (var6 != null && var6 instanceof FlashLight && ((FlashLight)var6).active) {
            this.flashLightActive = (FlashLight)var6;
         }
      }

      if (!this.blockActivationReactions.isEmpty()) {
         synchronized(this.blockActivationReactions) {
            while(!this.blockActivationReactions.isEmpty()) {
               CharacterBlockActivation var7 = (CharacterBlockActivation)this.blockActivationReactions.dequeue();
               this.handleBlockActivationReaction(var7);
            }
         }
      }

      if (this.warpOutOfCollisionRequest > 0L && var1.currentTime > this.warpOutOfCollisionRequest) {
         if (this.getPhysics().containsObject(this.ghostObject)) {
            Transform var5 = this.ghostObject.getWorldTransform(new Transform());
            System.err.println("SCHEDULED WARP CHECK: OLD POSITION: " + var5.origin);
            Vector3f var8 = new Vector3f(0.0F, 1.0F, 0.0F);
            if (this.scheduledGravity != null && this.scheduledGravity.source != null) {
               GlUtil.getUpVector(var8, (Transform)this.scheduledGravity.source.getWorldTransform());
               System.err.println("[SHEDULEDWARPOUT] took up from sheduled grav up " + var8);
            } else if (this.getGravity() != null && this.getGravity().source != null) {
               GlUtil.getUpVector(var8, (Transform)this.getGravity().source.getWorldTransform());
               System.err.println("[SHEDULEDWARPOUT] took up from current grav up " + var8);
            } else {
               System.err.println("[SHEDULEDWARPOUT] default grav up " + var8);
            }

            this.characterController.warpOutOfCollision(this.getState(), this.getPhysics().getDynamicsWorld(), var5, var8);
            Transform var9 = this.ghostObject.getWorldTransform(new Transform());
            if (!var5.origin.equals(var9.origin)) {
               this.warpTransformable(var9, true, false, (LocalSectorTransition)null);
               this.ghostObject.setWorldTransform(var9);
               System.err.println(this.getState() + "[PLAYERCHARACTER] WARPING OUT OF COLLISION " + var5.origin + " -> " + var9.origin);
               if (this.isClientOwnObject()) {
                  ((GameClientState)this.getState()).getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_ABSTRACTCHARACTER_0, 0.0F);
               }
            } else {
               System.err.println("NO WARP NECESSARY");
            }
         }

         this.warpOutOfCollisionRequest = -1L;
      }

   }

   public void updateToFullNetworkObject() {
      super.updateToFullNetworkObject();
      this.getNetworkObject().uniqueId.set(this.getUniqueIdentifier(), true);
   }

   public void setFactionId(int var1) {
      super.setFactionId(var1);
   }

   public void getGravityAABB(Vector3f var1, Vector3f var2) {
      this.getPhysicsDataContainer().getObject().getWorldTransform(new Transform());
      AabbUtil2.transformAabb(new Vector3f(-1.0F, -1.0F, -1.0F), new Vector3f(1.0F, 1.0F, 1.0F), 32.0F, this.getWorldTransform(), var1, var2);
   }

   public void getGravityAABB(Transform var1, Vector3f var2, Vector3f var3) {
      AabbUtil2.transformAabb(new Vector3f(-1.0F, -1.0F, -1.0F), new Vector3f(1.0F, 1.0F, 1.0F), 32.0F, var1, var2, var3);
   }

   protected void handleGravity() {
      if (this.isHidden()) {
         if (this.getGravity().source == null && this.getPhysicsDataContainer().getObject() != null && this.ghostObject.getAttached() != null) {
            System.err.println("[PlayerCharacter] " + this.getState() + "; " + this + " Cleaned up physics grvity. attached to was not null but no gravity source");
            this.ghostObject.setAttached((SegmentController)null);
         }

      } else {
         Transform var5;
         Matrix3f var6;
         if (!this.getGravity().isGravityOn() && !this.getGravity().isAligedOnly()) {
            if (this.getGravity().isChanged()) {
               this.sendOwnerMessage(new Object[]{231}, 1);
               System.err.println(this.getState() + " " + this + " (handleGravity()) Deactivating gravity ");
               var5 = new Transform();
               this.ghostObject.getWorldTransform(var5);
               Transform var10000 = this.ghostObject.localWorldTransform;
               if (this.isClientOwnObject()) {
                  (var6 = new Matrix3f()).setIdentity();
                  GlUtil.setRightVector(this.getOwnerState().getRight(new Vector3f()), var6);
                  GlUtil.setUpVector(this.getOwnerState().getUp(new Vector3f()), var6);
                  GlUtil.setForwardVector(this.getOwnerState().getForward(new Vector3f()), var6);
                  var5.basis.set(var6);
                  Controller.getCamera().getLookAlgorithm().lookTo(var5);
               }

               this.ghostObject.localWorldTransform = null;
               this.ghostObject.attachedOrientation = 2;
               this.ghostObject.setAttached((SegmentController)null);
               this.getCharacterController().setGravity(0.0F);
               this.getCharacterController().upAxisDirection[0].set(KinematicCharacterControllerExt.upAxisDirectionDefault[0]);
               this.getCharacterController().upAxisDirection[1].set(KinematicCharacterControllerExt.upAxisDirectionDefault[1]);
               this.getCharacterController().upAxisDirection[2].set(KinematicCharacterControllerExt.upAxisDirectionDefault[2]);
               this.getCharacterController().resetVerticalVelocity();
            }

         } else {
            SegmentController var1;
            if (!(var1 = (SegmentController)this.getGravity().source).getPhysicsDataContainer().isInitialized()) {
               this.getGravity().pendingUpdate = var1;
            } else if (this.getGravity().isChanged() || this.getGravity().pendingUpdate == this.getGravity().source) {
               this.getGravity().pendingUpdate = null;
               this.ghostObject.localWorldTransform = null;
               this.ghostObject.attachedOrientation = 2;
               this.ghostObject.setAttached((SegmentController)null);
               this.getCharacterController().setGravity(0.0F);
               this.getCharacterController().upAxisDirection[0].set(KinematicCharacterControllerExt.upAxisDirectionDefault[0]);
               this.getCharacterController().upAxisDirection[1].set(KinematicCharacterControllerExt.upAxisDirectionDefault[1]);
               this.getCharacterController().upAxisDirection[2].set(KinematicCharacterControllerExt.upAxisDirectionDefault[2]);
               this.getCharacterController().upAxisDirection[0].set(KinematicCharacterControllerExt.upAxisDirectionDefault[0]);
               this.getCharacterController().upAxisDirection[1].set(KinematicCharacterControllerExt.upAxisDirectionDefault[1]);
               this.getCharacterController().upAxisDirection[2].set(KinematicCharacterControllerExt.upAxisDirectionDefault[2]);
               GlUtil.getRightVector(this.getCharacterController().upAxisDirection[0], (Transform)var1.getWorldTransform());
               GlUtil.getUpVector(this.getCharacterController().upAxisDirection[1], (Transform)var1.getWorldTransform());
               GlUtil.getForwardVector(this.getCharacterController().upAxisDirection[2], (Transform)var1.getWorldTransform());
               byte var2 = -1;
               if (this.getGravity().getAcceleration().y < 0.0F) {
                  var2 = 2;
               } else if (this.getGravity().getAcceleration().y > 0.0F) {
                  var2 = 3;
               } else if (this.getGravity().getAcceleration().x < 0.0F) {
                  var2 = 4;
               } else if (this.getGravity().getAcceleration().x > 0.0F) {
                  var2 = 5;
               } else if (this.getGravity().getAcceleration().z < 0.0F) {
                  var2 = 0;
               } else if (this.getGravity().getAcceleration().z > 0.0F) {
                  var2 = 1;
               }

               GlUtil.setRightVector(this.getCharacterController().upAxisDirection[0], (Transform)this.getWorldTransform());
               GlUtil.setUpVector(this.getCharacterController().upAxisDirection[1], (Transform)this.getWorldTransform());
               GlUtil.setForwardVector(this.getCharacterController().upAxisDirection[2], (Transform)this.getWorldTransform());
               this.ghostObject.attachedOrientation = var2;
               this.ghostObject.setAttached(var1);
               this.ghostObject.localWorldTransform = new Transform();
               var6 = this.getCharacterController().getAddRoation();
               (var5 = new Transform(var1.getWorldTransform())).basis.mul(var6);
               var5.inverse();
               Transform var3;
               (var3 = new Transform(this.getWorldTransform())).basis.mul(var6);
               if (this.isClientOwnObject()) {
                  (var6 = new Matrix3f()).setIdentity();
                  GlUtil.setRightVector(this.getOwnerState().getRight(new Vector3f()), var6);
                  GlUtil.setUpVector(this.getOwnerState().getUp(new Vector3f()), var6);
                  GlUtil.setForwardVector(this.getOwnerState().getForward(new Vector3f()), var6);
                  Matrix3f var4;
                  (var4 = new Matrix3f(var5.basis)).mul(var6);
                  Transform var7;
                  (var7 = new Transform(this.getWorldTransform())).basis.set(var4);
                  var7.origin.set(0.0F, 0.0F, 0.0F);
                  Controller.getCamera().getLookAlgorithm().lookTo(var7);
               }

               this.ghostObject.localWorldTransform.set(var5);
               this.ghostObject.localWorldTransform.mul(var3);
               this.inPlaceAttachedUpdate(this.getState().getUpdateTime());
               this.getRemoteTransformable().setEqualCounter(0);
            }

            if (this.getGravity().getAcceleration().y < 0.0F) {
               this.getCharacterController().setGravity(-this.getGravity().getAcceleration().y);
            } else if (this.getGravity().getAcceleration().y > 0.0F) {
               this.getCharacterController().setGravity(this.getGravity().getAcceleration().y);
            } else if (this.getGravity().getAcceleration().x < 0.0F) {
               this.getCharacterController().setGravity(-this.getGravity().getAcceleration().x);
            } else if (this.getGravity().getAcceleration().x > 0.0F) {
               this.getCharacterController().setGravity(this.getGravity().getAcceleration().x);
            } else if (this.getGravity().getAcceleration().z < 0.0F) {
               this.getCharacterController().setGravity(-this.getGravity().getAcceleration().z);
            } else if (this.getGravity().getAcceleration().z > 0.0F) {
               this.getCharacterController().setGravity(this.getGravity().getAcceleration().z);
            } else {
               this.getCharacterController().setGravity(0.0F);
            }
         }
      }
   }

   protected boolean hasVirtual() {
      return false;
   }

   protected boolean isCheckSectorActive() {
      return !this.isHidden() && System.currentTimeMillis() - this.lastAttach > 6000L;
   }

   public void onPhysicsAdd() {
      super.onPhysicsAdd();
      PhysicsExt var1 = this.getPhysics();
      if (this.isClientOwnObject()) {
         PhysicsExt var2 = this.getPhysics();
         GlUtil.getForwardVector(new Vector3f(), (Transform)this.getWorldTransform());
         Transform var3 = new Transform(this.getWorldTransform());
         if (this.neverSpawnedBefore) {
            this.neverSpawnedBefore = false;
            this.setWarpToken(false);
         }

         if (this.isWarpToken()) {
            System.err.println("NOT WARPING OUT OF COLLISION, BECAUSE WARP TOKEN WAS SET");
            this.setWarpToken(false);
         } else {
            Vector3f var4 = new Vector3f(0.0F, 1.0F, 0.0F);
            if (this.scheduledGravity != null && this.scheduledGravity.source != null) {
               GlUtil.getUpVector(var4, (Transform)this.scheduledGravity.source.getWorldTransform());
               System.err.println("[WARPOUT] took up from sheduled grav up " + var4);
            } else if (this.getGravity() != null && this.getGravity().source != null) {
               GlUtil.getUpVector(var4, (Transform)this.getGravity().source.getWorldTransform());
               System.err.println("[WARPOUT] took up from current grav up " + var4);
            } else {
               System.err.println("[WARPOUT] default grav up " + var4);
            }

            this.characterController.warpOutOfCollision(this.getState(), var2.getDynamicsWorld(), this.getWorldTransform(), var4);
            System.err.println("[PLAYERCHARACTER][ONPHYSICSADD][" + this.getState() + "] WARPING OUT OF COLLISION: " + this + ": " + var3.origin + " -> " + this.ghostObject.getWorldTransform(new Transform()).origin);
         }

         Transform var6 = this.ghostObject.getWorldTransform(new Transform());
         Vector3f var5;
         (var5 = new Vector3f()).sub(var3.origin, var6.origin);
         if (var5.length() > 50.0F) {
            ((GameClientState)this.getState()).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_ABSTRACTCHARACTER_2, var5.length()), 0.0F);
            System.err.println("[PLAYERCHARACTER][ONPHYSICSADD][" + this.getState() + "] Exception while WARPING OUT OF COLLISION: " + this + ": " + var3.origin + " -> " + var6.origin);
         }

         this.getRemoteTransformable().warp(var6, false);
      }

      if ((this.isOnServer() || this.getSectorId() == ((GameClientState)this.getState()).getCurrentSectorId()) && !var1.containsAction(this.characterController)) {
         var1.getDynamicsWorld().addAction(this.characterController);
      }

      if (this.getGravity().source != null) {
         this.getGravity().setChanged(true);
         this.handleGravity();
      }

   }

   public void onPhysicsRemove() {
      super.onPhysicsRemove();
      if (!this.isOnServer() || ((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId()) != null) {
         this.getPhysics().getDynamicsWorld().removeAction(this.characterController);
      }

   }

   public void sitDown(ClosestRayResultCallback var1, Vector3f var2, Vector3f var3, float var4) {
      CubeRayCastResult var14;
      if (var1 != null && var1.hasHit() && var1 instanceof CubeRayCastResult && (var14 = (CubeRayCastResult)var1).getSegment() != null && var14.getSegment().getSegmentController() instanceof EditableSendableSegmentController) {
         var3 = new Vector3f(var3);
         Vector3f var5 = new Vector3f();
         var3.normalize();
         var3.scale(var4);
         var5.add(var2, var3);
         CubeRayCastResult var19;
         (var19 = new CubeRayCastResult(var2, var5, false, new SegmentController[0])).setOnlyCubeMeshes(true);
         var19.setIgnoereNotPhysical(true);
         ClosestRayResultCallback var16 = this.getPhysics().testRayCollisionPoint(var2, var5, var19, false);
         if (this.getGravity().source == null || this.getGravity().source != var14.getSegment().getSegmentController()) {
            SegmentController var15 = var14.getSegment().getSegmentController();
            this.scheduleGravity(new Vector3f(0.0F, 0.0F, 0.0F), var15);
            if (!this.isOnServer()) {
               ((GameClientState)this.getState()).getController().popupInfoTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_ABSTRACTCHARACTER_3, var15.toNiceString()), 0.0F);
            }
         }

         if (var16 != null && var16.hasHit() && var16 instanceof CubeRayCastResult) {
            if ((var14 = (CubeRayCastResult)var16).getSegment() == null) {
               System.err.println("CUBERESULT SEGMENT NULL");
               return;
            }

            Vector3i var20 = new Vector3i(var14.getSegment().pos.x, var14.getSegment().pos.y, var14.getSegment().pos.z);
            Vector3i var10000 = new Vector3i();
            Transform var21 = null;
            var10000.set(var14.getSegment().pos.x + var14.getCubePos().x, var14.getSegment().pos.y + var14.getCubePos().y, var14.getSegment().pos.z + var14.getCubePos().z);
            var20.x += var14.getCubePos().x - 16;
            var20.y += var14.getCubePos().y - 16;
            var20.z += var14.getCubePos().z - 16;
            if (!this.isOnServer() && ((GameClientState)this.getState()).getCurrentSectorId() != var14.getSegment().getSegmentController().getSectorId()) {
               (var21 = new Transform(var14.getSegment().getSegmentController().getWorldTransformOnClient())).inverse();
               var21.transform(var16.hitPointWorld);
            } else {
               var14.getSegment().getSegmentController().getWorldTransformInverse().transform(var16.hitPointWorld);
            }

            int var22 = Element.getSide(var14.hitPointWorld, (BlockShapeAlgorithm)null, var20, var14.getSegment().getSegmentData().getType(var14.getCubePos()), var14.getSegment().getSegmentData().getOrientation(var14.getCubePos()));
            System.err.println("[SIT] SIDE: " + Element.getSideString(var22) + ": " + var14.hitPointWorld + "; " + var20);
            var2 = new Vector3f(var14.hitPointWorld.x - (float)var20.x, var14.hitPointWorld.y - (float)var20.y, var14.hitPointWorld.z - (float)var20.z);
            Vector3i var23 = new Vector3i();
            Vector3i var6 = new Vector3i(var20);
            ElementInformation var7;
            if ((var7 = ElementKeyMap.getInfo(var14.getSegment().getSegmentData().getType(var14.getCubePos()))).getBlockStyle() != BlockStyle.WEDGE && !var7.getBlockStyle().cube) {
               if (!this.isOnServer()) {
                  ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_ABSTRACTCHARACTER_4, 0.0F);
               }

               return;
            }

            boolean var8 = false;
            if (!var7.isNormalBlockStyle()) {
               int var9 = SegmentData.getInfoIndex(var14.getCubePos());
               BlockShapeAlgorithm var10 = BlockShapeAlgorithm.getAlgo(var7.getBlockStyle(), var14.getSegment().getSegmentData().getOrientation(var9));
               if ((this.getGhostObject().getAttached() == null || this.getGhostObject().attachedOrientation < 0) && var22 != 2 && var22 != 1 && var22 != 4) {
                  System.err.println("[SIT] Can only go to top when unattached(or not in gravity)");
                  if (!this.isOnServer()) {
                     ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_ABSTRACTCHARACTER_7, 0.0F);
                  }

                  return;
               }

               if (this.getGhostObject().getAttached() != null && this.getGhostObject().attachedOrientation >= 0 && this.getGhostObject().attachedOrientation != 2) {
                  if (this.getGhostObject().attachedOrientation != 2 && this.getGhostObject().attachedOrientation != 3) {
                     var22 = Element.getOpposite(this.getGhostObject().attachedOrientation);
                  } else {
                     var22 = this.getGhostObject().attachedOrientation;
                  }
               } else {
                  var22 = 2;
                  var8 = true;
               }

               byte var11;
               if (!var8 && this.getGhostObject().attachedOrientation != 2 && this.getGhostObject().attachedOrientation != 3) {
                  var11 = var10.getWedgeGravityValidDir((byte)var22);
               } else {
                  var11 = var10.getWedgeGravityValidDir((byte)Element.getOpposite(var22));
               }

               if (var11 < 0) {
                  if (this.getGhostObject().getAttached() != null && this.getGhostObject().attachedOrientation >= 0) {
                     if (!this.isOnServer()) {
                        ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_ABSTRACTCHARACTER_9, 0.0F);
                     }
                  } else if (!this.isOnServer()) {
                     ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_ABSTRACTCHARACTER_8, 0.0F);
                     return;
                  }

                  return;
               }

               var2.set(Element.DIRECTIONSf[var11]);
            } else {
               if ((this.getGhostObject().getAttached() == null || this.getGhostObject().attachedOrientation < 0) && var22 != 2) {
                  System.err.println("[SIT] Can only go to top when unattached(or not in gravity)");
                  if (!this.isOnServer()) {
                     ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_ABSTRACTCHARACTER_5, 0.0F);
                  }

                  return;
               }

               if (this.getGhostObject().getAttached() != null && this.getGhostObject().attachedOrientation >= 0 && this.getGhostObject().attachedOrientation != Element.getOpposite(var22) && (this.getGhostObject().attachedOrientation != 2 || var22 != 2) && (this.getGhostObject().attachedOrientation != 3 || var22 != 3)) {
                  System.err.println("[SIT] " + this.getState() + " opposite orientation doesnt meet relative top: AttOrient: " + Element.getSideString(this.getGhostObject().attachedOrientation) + "; OppositeRaySide: " + Element.getSideString(Element.getOpposite(var22)));
                  if (!this.isOnServer()) {
                     ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_ABSTRACTCHARACTER_6, 0.0F);
                  }

                  return;
               }
            }

            switch(var22) {
            case 0:
               var20.z = (int)((float)var20.z + 1.0F);
               var23.set(var20);
               if (Math.abs(var2.y) > Math.abs(var2.x)) {
                  var23.y = (int)((float)var23.y + Math.signum(var2.y));
                  var6.y = (int)((float)var6.y + Math.signum(var2.y));
               } else {
                  var23.x = (int)((float)var23.x + Math.signum(var2.x));
                  var6.x = (int)((float)var6.x + Math.signum(var2.x));
               }
               break;
            case 1:
               var20.z = (int)((float)var20.z - 1.0F);
               var23.set(var20);
               if (Math.abs(var2.y) > Math.abs(var2.x)) {
                  var23.y = (int)((float)var23.y + Math.signum(var2.y));
                  var6.y = (int)((float)var6.y + Math.signum(var2.y));
               } else {
                  var23.x = (int)((float)var23.x + Math.signum(var2.x));
                  var6.x = (int)((float)var6.x + Math.signum(var2.x));
               }
               break;
            case 2:
               var20.y = (int)((float)var20.y + 1.0F);
               var23.set(var20);
               if (Math.abs(var2.z) > Math.abs(var2.x)) {
                  var23.z = (int)((float)var23.z + Math.signum(var2.z));
                  var6.z = (int)((float)var6.z + Math.signum(var2.z));
               } else {
                  var23.x = (int)((float)var23.x + Math.signum(var2.x));
                  var6.x = (int)((float)var6.x + Math.signum(var2.x));
               }
               break;
            case 3:
               var20.y = (int)((float)var20.y - 1.0F);
               var23.set(var20);
               if (Math.abs(var2.z) > Math.abs(var2.x)) {
                  var23.z = (int)((float)var23.z + Math.signum(var2.z));
                  var6.z = (int)((float)var6.z + Math.signum(var2.z));
               } else {
                  var23.x = (int)((float)var23.x + Math.signum(var2.x));
                  var6.x = (int)((float)var6.x + Math.signum(var2.x));
               }
               break;
            case 4:
               var20.x = (int)((float)var20.x + 1.0F);
               var23.set(var20);
               if (Math.abs(var2.z) > Math.abs(var2.y)) {
                  var23.z = (int)((float)var23.z + Math.signum(var2.z));
                  var6.z = (int)((float)var6.z + Math.signum(var2.z));
               } else {
                  var23.y = (int)((float)var23.y + Math.signum(var2.y));
                  var6.y = (int)((float)var6.y + Math.signum(var2.y));
               }
               break;
            case 5:
               var20.x = (int)((float)var20.x - 1.0F);
               var23.set(var20);
               if (Math.abs(var2.z) > Math.abs(var2.y)) {
                  var23.z = (int)((float)var23.z + Math.signum(var2.z));
                  var6.z = (int)((float)var6.z + Math.signum(var2.z));
               } else {
                  var23.y = (int)((float)var23.y + Math.signum(var2.y));
                  var6.y = (int)((float)var6.y + Math.signum(var2.y));
               }
            }

            if (var7.getBlockStyle() == BlockStyle.WEDGE) {
               Vector3i var25;
               (var25 = new Vector3i()).sub(var6, var23);
               var20.add(var25);
               var23.add(var25);
               var6.add(var25);
               int var27 = SegmentData.getInfoIndex(var14.getCubePos());
               BlockShapeAlgorithm var29 = BlockShapeAlgorithm.getAlgo(var7.getBlockStyle(), var14.getSegment().getSegmentData().getOrientation(var27));
               System.err.println("[SIT] Sitting on wedge " + var25 + "; " + var29 + " ");
            }

            var20.add(16, 16, 16);
            var23.add(16, 16, 16);
            var6.add(16, 16, 16);
            SegmentPiece var26 = var14.getSegment().getSegmentController().getSegmentBuffer().getPointUnsave(var20);
            SegmentPiece var28 = var14.getSegment().getSegmentController().getSegmentBuffer().getPointUnsave(var23);
            SegmentPiece var30 = var14.getSegment().getSegmentController().getSegmentBuffer().getPointUnsave(var6);
            if (var26 == null || var28 == null || var30 == null) {
               System.err.println("[SIT] one or more blocks null");
               if (!this.isOnServer()) {
                  ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_ABSTRACTCHARACTER_10, 0.0F);
               }

               return;
            }

            Vector3i var17;
            ++(var17 = new Vector3i(var20)).y;
            SegmentPiece var12;
            if ((var12 = var14.getSegment().getSegmentController().getSegmentBuffer().getPointUnsave(var17)) != null && var12.getType() != 0) {
               System.err.println("[SIT] one or more blocks filled. Block to sit on: " + var20.toString() + "  Block above: " + var17.toString());
               if (!this.isOnServer()) {
                  ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_ABSTRACTCHARACTER_11, 0.0F);
               }

               return;
            }

            if (var26.getType() != 0 && var7.getBlockStyle().cube || var28.getType() != 0) {
               boolean var18 = false;
               if (var26.getType() != 0 && var7.getBlockStyle() == BlockStyle.WEDGE && var28.getType() != 0 && ElementKeyMap.getInfo(var28.getType()).getBlockStyle() == BlockStyle.WEDGE) {
                  System.err.println("[SIT] Sitting on wedge with wedge in front");
                  int var31 = SegmentData.getInfoIndex(var14.getCubePos());
                  BlockShapeAlgorithm var32 = BlockShapeAlgorithm.getAlgo(var7.getBlockStyle(), var14.getSegment().getSegmentData().getOrientation(var31));
                  int var13 = var28.getInfoIndex();
                  BlockShapeAlgorithm var33 = BlockShapeAlgorithm.getAlgo(ElementKeyMap.getInfo(var28.getType()).getBlockStyle(), var28.getSegment().getSegmentData().getOrientation(var13));
                  byte var24;
                  if (!var8 && this.getGhostObject().attachedOrientation != 2 && this.getGhostObject().attachedOrientation != 3) {
                     var24 = var32.getWedgeGravityValidDir((byte)var22);
                  } else {
                     var24 = var32.getWedgeGravityValidDir((byte)Element.getOpposite(var22));
                  }

                  if (var24 == Element.getOpposite(var33.getWedgeOrientation()[0]) || var24 == Element.getOpposite(var33.getWedgeOrientation()[1])) {
                     var18 = true;
                  }
               }

               if (!var18) {
                  System.err.println("[SIT] one or more blocks filled " + var7 + ": style " + var7.getBlockStyle());
                  if (!this.isOnServer()) {
                     ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_ABSTRACTCHARACTER_12, 0.0F);
                  }

                  if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
                     DebugDrawer.debugDraw(var20.x, var20.y, var20.z, 16, var14.getSegment().getSegmentController());
                     DebugDrawer.debugDraw(var23.x, var23.y, var23.z, 16, var14.getSegment().getSegmentController());
                     DebugDrawer.debugDraw(var6.x, var6.y, var6.z, 16, var14.getSegment().getSegmentController());
                  }

                  return;
               }
            }

            System.err.println("[SIT] " + this.getState() + ": " + var20 + " -> " + var23 + "; " + var26 + ", " + var28 + ", " + var30);
            if (!this.isOnServer() && this.getOwnerState() instanceof PlayerState) {
               ((PlayerState)this.getOwnerState()).sendSimpleCommand(SimplePlayerCommands.SIT_DOWN, var14.getSegment().getSegmentController().getId(), ElementCollection.getIndex(var20), ElementCollection.getIndex(var23), ElementCollection.getIndex(var6));
               return;
            }

            System.err.println("[SIT] " + this.getState() + " Creature sitting down!");
            this.getOwnerState().sitDown(var14.getSegment().getSegmentController(), var20, var23, var6);
         }
      }

   }

   public void createConstraint(Physical var1, Physical var2, Object var3) {
   }

   public void getTransformedAABB(Vector3f var1, Vector3f var2, float var3, Vector3f var4, Vector3f var5, Transform var6) {
      float var7 = this.capsule.getMargin();
      this.capsule.setMargin(var3);
      if (var6 != null) {
         this.capsule.getAabb(var6, var1, var2);
      } else {
         this.capsule.getAabb(this.getWorldTransform(), var1, var2);
      }

      this.capsule.setMargin(var7);
   }

   public void initPhysics() {
      this.ghostObject = new PairCachingGhostObjectAlignable(CollisionType.CHARACTER, this.getPhysicsDataContainer(), this);
      this.ghostObject.setWorldTransform(this.getInitialTransform());
      this.capsule = new CapsuleShapeExt(this, this.getCharacterWidth(), this.getCharacterHeight());
      this.capsule.setMargin(this.getCharacterMargin());
      this.ghostObject.setCollisionShape(this.capsule);
      this.ghostObject.setCollisionFlags(16);
      this.ghostObject.setUserPointer(this.getId());
      this.characterController = new KinematicCharacterControllerExt(this, this.ghostObject, this.capsule, this.stepHeight);
      this.getPhysicsDataContainer().collisionGroup = 32;
      this.getPhysicsDataContainer().collisionMask = -9;
      this.getPhysicsDataContainer().setObject(this.ghostObject);
      this.getPhysicsDataContainer().setShape(this.capsule);
      this.getPhysicsDataContainer().updatePhysical(0L);
      this.characterController.setGravity(0.0F);
      this.setFlagPhysicsInit(true);
      if (!this.isOnServer()) {
         this.activatedStuckProtection = System.currentTimeMillis();
      }

   }

   public void damage(float var1, Damager var2) {
      if (this.canAttack(var2)) {
         if (this.getOwnerState().isVulnerable()) {
            this.getNetworkObject().hit.set(true);
            this.lastHit = this.getState().getUpdateTime();
            AbstractOwnerState var3;
            if ((var3 = this.getOwnerState()) != null) {
               var3.damage(var1, this, var2);
               return;
            }

            System.err.println("Exception: trying to Damage: Warning " + this + " has no owner state " + this.getState());
         }

      }
   }

   public void destroy(Damager var1) {
      if (this.isOnServer() && !this.isMarkedForDeleteVolatile()) {
         System.err.println("[SERVER] character " + this + " has been deleted by " + var1);
         this.setMarkedForDeleteVolatile(true);
      }

   }

   public abstract void destroyPersistent();

   private void flagActivatedStuckProtection() {
      this.activatedStuckProtection = System.currentTimeMillis();
   }

   public void flagWapOutOfClient(int var1) {
      this.warpOutOfCollisionRequest = System.currentTimeMillis() + (long)var1;
   }

   public long getActivatedStuckProtection() {
      return this.activatedStuckProtection;
   }

   public void setActivatedStuckProtection(long var1) {
      this.activatedStuckProtection = var1;
   }

   public KinematicCharacterControllerExt getCharacterController() {
      return this.characterController;
   }

   public abstract float getCharacterHeightOffset();

   public abstract float getCharacterHeight();

   public abstract float getCharacterWidth();

   public abstract Vector3i getBlockDim();

   public abstract Transform getHeadWorldTransform();

   public SegmentPiece getNearestPiece(boolean var1) {
      try {
         return this.getNearestPiece(5.0F, var1);
      } catch (Exception var2) {
         var2.printStackTrace();
         return null;
      }
   }

   public abstract ArrayList getAttachedPlayers();

   public GameTransformable getNearestEntity(boolean var1) {
      try {
         return this.getNearestEntity(5.0F, var1);
      } catch (Exception var2) {
         var2.printStackTrace();
         return null;
      }
   }

   public GameTransformable getNearestEntity(float var1, boolean var2) {
      if (this.getAttachedPlayers().isEmpty()) {
         return null;
      } else {
         AbstractOwnerState var3 = (AbstractOwnerState)this.getAttachedPlayers().get(0);
         Vector3f var4 = new Vector3f(this.getHeadWorldTransform().origin);
         Vector3f var5 = new Vector3f();
         Vector3f var8;
         (var8 = var3.getForward(new Vector3f())).scale(var1);
         var5.add(var4, var8);
         CubeRayCastResult var6;
         (var6 = new CubeRayCastResult(var4, var5, false, new SegmentController[0])).setOnlyCubeMeshes(false);
         var6.setIgnoereNotPhysical(var2);
         ClosestRayResultCallback var7;
         if ((var7 = this.getPhysics().testRayCollisionPoint(var4, var5, var6, false)).hasHit() && var7.collisionObject != null) {
            if (var7.collisionObject instanceof RigidBodySegmentController) {
               return ((RigidBodySegmentController)var7.collisionObject).getSegmentController();
            }

            if (var7.collisionObject instanceof PairCachingGhostObjectAlignable) {
               return ((PairCachingGhostObjectAlignable)var7.collisionObject).getObj();
            }
         }

         return null;
      }
   }

   public SegmentPiece getNearestPiece(float var1, boolean var2) {
      if (this.getAttachedPlayers().isEmpty()) {
         System.err.println("Exception: NO nearest piece (no player attached)");
         return null;
      } else {
         AbstractOwnerState var3 = (AbstractOwnerState)this.getAttachedPlayers().get(0);
         Vector3f var4 = new Vector3f(this.getHeadWorldTransform().origin);
         Vector3f var5 = new Vector3f();
         Vector3f var10;
         (var10 = var3.getForward(new Vector3f())).scale(var1);
         var5.add(var4, var10);
         CubeRayCastResult var6;
         (var6 = new CubeRayCastResult(var4, var5, false, new SegmentController[0])).setOnlyCubeMeshes(true);
         var6.setIgnoereNotPhysical(var2);
         ClosestRayResultCallback var7;
         if ((var7 = this.getPhysics().testRayCollisionPoint(var4, var5, var6, false)).hasHit() && var7.collisionObject != null && var7 instanceof CubeRayCastResult && ((CubeRayCastResult)var7).getSegment() != null) {
            SegmentController var8 = (var6 = (CubeRayCastResult)var7).getSegment().getSegmentData().getSegmentController();
            Segment var11 = var6.getSegment();
            SegmentPiece var9 = new SegmentPiece(var11, var6.getCubePos());
            System.err.println("HIT RESULT near: " + var9 + ", on " + var8);
            return var9;
         } else {
            return null;
         }
      }
   }

   public ProjectileController getParticleController() {
      return !this.isOnServer() ? ((GameClientState)this.getState()).getParticleController() : ((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId()).getParticleController();
   }

   public Set getShopsInDistance() {
      return this.shopsInDistance;
   }

   public abstract Transform getShoulderWorldTransform();

   public String getUniqueIdentifier() {
      return this.uniqueIdentifier;
   }

   public boolean isVolatile() {
      return false;
   }

   public void setUniqueIdentifier(String var1) {
      this.uniqueIdentifier = var1;
   }

   public void handleCollision(SegmentPiece var1, Vector3f var2) {
      if (this.isOnServer()) {
         if (var1.getType() == 80 && System.currentTimeMillis() - this.lastCollisionHurt > 500L) {
            Iterator var4 = this.getAttachedPlayers().iterator();

            while(var4.hasNext()) {
               ((AbstractOwnerState)var4.next()).damage(10.0F, (Destroyable)null, var1.getSegment().getSegmentController());
            }

            this.lastCollisionHurt = System.currentTimeMillis();
            return;
         }
      } else if (var1.getType() == 412) {
         ActivationTrigger var5 = new ActivationTrigger(var1.getAbsoluteIndex(), this.getPhysicsDataContainer().getObject(), var1.getType());
         ActivationTrigger var3;
         if ((var3 = (ActivationTrigger)var1.getSegment().getSegmentController().getTriggers().get(var5)) == null) {
            System.err.println("ADDING CHARACTER TRIGGER: " + var5 + "; " + var1.getSegmentController().getTriggers().size());
            var1.getSegment().getSegmentController().getTriggers().add(var5);
            return;
         }

         var3.ping();
      }

   }

   public void sendOwnerMessage(Object[] var1, int var2) {
      if (this.isOnServer() && this.getAttachedPlayers().size() > 0 && this.getAttachedPlayers().get(0) instanceof PlayerState) {
         PlayerState var3;
         (var3 = (PlayerState)this.getAttachedPlayers().get(0)).sendServerMessage(new ServerMessage(var1, var2, var3.getId()));
      }

   }

   public boolean canAttack(Damager var1) {
      if (this.isSpectator() || var1 != null && var1 instanceof SimpleTransformableSendableObject && ((SimpleTransformableSendableObject)var1).isSpectator()) {
         if (var1 instanceof SimpleTransformableSendableObject && ((SimpleTransformableSendableObject)var1).isClientOwnObject()) {
            ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_ABSTRACTCHARACTER_15, 0.0F);
         }

         return false;
      } else {
         Sector var2;
         if (this.isOnServer() && (var2 = ((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId())) != null && var2.isProtected()) {
            if (var1 != null && var1 instanceof PlayerControllable) {
               List var4 = ((PlayerControllable)var1).getAttachedPlayers();

               for(int var5 = 0; var5 < var4.size(); ++var5) {
                  PlayerState var3 = (PlayerState)var4.get(var5);
                  if (this.getState().getUpdateTime() - var3.lastSectorProtectedMsgSent > 5000L) {
                     var3.lastSectorProtectedMsgSent = System.currentTimeMillis();
                     var3.sendServerMessage(new ServerMessage(new Object[]{232}, 2, var3.getId()));
                  }
               }
            }

            return false;
         } else {
            return true;
         }
      }
   }

   public boolean isVulnerable() {
      return this.getOwnerState().isVulnerable();
   }

   public boolean checkAttack(Damager var1, boolean var2, boolean var3) {
      return this.canAttack(var1);
   }

   public void drawDebugTransform() {
      Transform var1 = new Transform();
      if (this.isOnServer() && GameClientState.staticSector == this.getSectorId()) {
         this.ghostObject.getWorldTransform(var1);
      } else {
         this.ghostObject.getWorldTransform(var1);
      }

      Vector3f var2 = GlUtil.getUpVector(new Vector3f(), var1);
      Vector3f var3 = GlUtil.getRightVector(new Vector3f(), var1);
      Vector3f var4 = GlUtil.getForwardVector(new Vector3f(), var1);
      Vector3f var5;
      if (this.isOnServer()) {
         (var5 = new Vector3f()).add(var2);
         var5.add(var3);
         var5.add(var4);
         var5.normalize();
         var5.scale(4.0F);
         var5.add(var1.origin);
         DebugLine var6 = new DebugLine(new Vector3f(var1.origin), var5, new Vector4f(1.0F, 0.0F, 1.0F, 1.0F));
         if (!DebugDrawer.lines.contains(var6)) {
            DebugDrawer.lines.add(var6);
         }
      }

      var5 = new Vector3f(var2);
      Vector3f var14 = new Vector3f(var3);
      Vector3f var7 = new Vector3f(var4);
      var2.set(0.0F, 0.0F, 0.0F);
      var3.set(0.0F, 0.0F, 0.0F);
      var4.set(0.0F, 0.0F, 0.0F);
      var5.scale(3.0F);
      var14.scale(3.0F);
      var7.scale(3.0F);
      var2.add(var1.origin);
      var3.add(var1.origin);
      var4.add(var1.origin);
      var5.add(var1.origin);
      var14.add(var1.origin);
      var7.add(var1.origin);
      Vector4f var10 = new Vector4f(0.0F, 1.0F, 0.0F, 1.0F);
      Vector4f var8 = new Vector4f(1.0F, 0.0F, 0.0F, 1.0F);
      Vector4f var9 = new Vector4f(0.0F, 0.0F, 1.0F, 1.0F);
      if (this.isOnServer()) {
         var10.x += 0.8F;
         var8.z += 0.8F;
         var9.y += 0.8F;
      }

      DebugLine var11 = new DebugLine(var2, var5, var10);
      DebugLine var12 = new DebugLine(var3, var14, var8);
      DebugLine var13 = new DebugLine(var4, var7, var9);
      if (!DebugDrawer.lines.contains(var11)) {
         DebugDrawer.lines.add(var11);
      }

      if (!DebugDrawer.lines.contains(var12)) {
         DebugDrawer.lines.add(var12);
      }

      if (!DebugDrawer.lines.contains(var13)) {
         DebugDrawer.lines.add(var13);
      }

   }

   protected abstract float getCharacterMargin();

   public void inPlaceAttachedUpdate(long var1) {
      this.characterController.loadAttached(var1);
      this.getPhysicsDataContainer().updatePhysical(var1);
   }

   public boolean isHasSpawnedOnServer() {
      return this.hasSpawnedOnServer;
   }

   public void setHasSpawnedOnServer(boolean var1) {
      this.hasSpawnedOnServer = var1;
   }

   public boolean needsManifoldCollision() {
      return false;
   }

   public void onCollision(ManifoldPoint var1, Sendable var2) {
      if (var1.combinedRestitution > 0.0F || var1.appliedImpulseLateral1 > 0.0F || var1.appliedImpulseLateral2 > 0.0F) {
         System.err.println("Restitution " + var1.combinedRestitution + "; A " + var1.appliedImpulseLateral1 + "; B " + var1.appliedImpulseLateral2 + "; Com " + var1.appliedImpulse);
      }

   }

   public String toString() {
      return "PlayerCharacter[(" + this.uniqueIdentifier + ")(" + this.getId() + ")]";
   }

   public void sendHitConfirm(byte var1) {
      this.getOwnerState().sendHitConfirm(var1);
   }

   public boolean isSegmentController() {
      return false;
   }

   public String getName() {
      return this.getOwnerState().getName();
   }

   public Vector3f getForward() {
      return this.getOwnerState().getForward(new Vector3f());
   }

   public Vector3f getRight() {
      return this.getOwnerState().getRight(new Vector3f());
   }

   public Vector3f getUp() {
      return this.getOwnerState().getUp(new Vector3f());
   }

   public abstract AbstractOwnerState getOwnerState();

   protected void handleBlockActivationReaction(CharacterBlockActivation var1) {
      Sendable var2;
      SegmentPiece var3;
      SendableSegmentController var10;
      if ((var2 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var1.objectId)) != null && var2 instanceof SendableSegmentController && (var3 = (var10 = (SendableSegmentController)var2).getSegmentBuffer().getPointUnsave(var1.location)) != null && ElementInformation.isMedical(var3.getType())) {
         if (var10.getCooldownBlocks().containsKey(var1.location)) {
            long var4 = var10.getCooldownBlocks().get(var1.location);
            long var6 = System.currentTimeMillis() - var4;
            long var8 = 20000L - var6;
            this.sendControllingPlayersServerMessage(new Object[]{233, var8 / 1000L}, 3);
            return;
         }

         this.getOwnerState().heal(10.0F, this, var10);
         var10.getCooldownBlocks().put(var1.location, System.currentTimeMillis());
         this.sendControllingPlayersServerMessage(new Object[]{234, 10L, 20L}, 1);
      }

   }

   public abstract float getSpeed();

   public AnimationIndexElement getAnimationState() {
      return this.animationState;
   }

   public void setAnimationState(AnimationIndexElement var1) {
      this.animationState = var1;
   }

   public boolean onGround() {
      return this.characterController.onGround();
   }

   public PlayerState getConversationPartner() {
      return this.getOwnerState().getConversationPartner();
   }

   public void transformBeam(BeamState var1) {
      this.getWorldTransform().transform(var1.from);
   }

   public void popupOwnClientMessage(String var1, int var2) {
      if (this.isClientOwnObject()) {
         GameClientController var3 = ((GameClientState)this.getState()).getController();
         switch(var2) {
         case 0:
            var3.getState().chat(var3.getState().getChat(), var1, "[MESSAGE]", false);
            return;
         case 1:
            var3.popupInfoTextMessage(var1, 0.0F);
            return;
         case 2:
            var3.popupGameTextMessage(var1, 0.0F);
            return;
         case 3:
            var3.popupAlertTextMessage(var1, 0.0F);
            return;
         default:
            assert false;
         }
      }

   }

   public int handleSalvage(BeamState var1, int var2, BeamHandlerContainer var3, Vector3f var4, SegmentPiece var5, Timer var6, Collection var7) {
      if (var5.getSegment() == null) {
         assert false;

         return 0;
      } else if (var3.getOwnerState() != null && var3.getOwnerState() instanceof PlayerState && var3.getOwnerState().getFactionId() != 0 && var3.getOwnerState().getFactionId() == var5.getSegment().getSegmentController().getFactionId() && !var5.getSegment().getSegmentController().isSufficientFactionRights((PlayerState)var3.getOwnerState())) {
         this.popupOwnClientMessage("You don't have the rank\nin your faction to\nsalvage this", 3);
         return 0;
      } else {
         if (this.isOnServer() && var2 > 0) {
            byte var10 = var5.getOrientation();
            short var11;
            if (ElementKeyMap.isValidType(var11 = var5.getType()) && ElementKeyMap.isValidType(ElementKeyMap.getInfoFast(var11).getSourceReference())) {
               var11 = (short)ElementKeyMap.getInfoFast(var11).getSourceReference();
            }

            if (var5.getSegmentController().isScrap()) {
               if (Universe.getRandom().nextFloat() > 0.5F) {
                  var11 = 546;
               } else {
                  var11 = 547;
               }
            }

            AbstractOwnerState var12;
            boolean var13 = (var12 = (AbstractOwnerState)this.getAttachedPlayers().get(0)).getInventory().canPutIn(var11, 1);
            int var8 = 0;
            boolean var9 = false;
            if (var13 && ElementKeyMap.hasResourceInjected(var11, var10)) {
               float var14;
               var8 = (int)(var14 = VoidElementManager.PERSONAL_SALVAGE_BEAM_BONUS);
               var14 -= (float)var8;
               if (Math.random() < (double)var14) {
                  var8 += 2;
                  var9 = true;
               }

               int var15 = this.getMiningBonus(var5.getSegmentController());
               var8 *= var15;
               var13 = var12.getInventory().canPutIn(ElementKeyMap.orientationToResIDMapping[var10], var8);
            }

            if (var13) {
               if (var5.getSegment().removeElement(var5.getPos(this.tmpLocalPos), false)) {
                  var7.add(var5.getSegment());
                  ((RemoteSegment)var5.getSegment()).setLastChanged(System.currentTimeMillis());
                  var5.refresh();

                  assert var5.getType() == 0;

                  var5.setHitpointsByte(1);
                  var5.getSegmentController().sendBlockSalvage(var5);
                  if (ElementKeyMap.isValidType(var11)) {
                     var5.getSegmentController().getHpController().onManualRemoveBlock(ElementKeyMap.getInfo(var11));
                  }

                  if (this.getAttachedPlayers().size() > 0) {
                     var12.modDelayPersonalInventory(var11, 1);
                     if (ElementKeyMap.hasResourceInjected(var11, var10)) {
                        if (var9) {
                           this.sendOwnerMessage(new Object[]{235, var8}, 1);
                        }

                        var12.modDelayPersonalInventory(ElementKeyMap.orientationToResIDMapping[var10], var8);
                     }
                  }
               }
            } else {
               ((PlayerState)var12).sendServerMessage(new ServerMessage(new Object[]{236}, 3, var12.getId()));
            }
         }

         return var2;
      }
   }

   public PersonalBeamHandler getHandler() {
      return this.beamHandler;
   }

   public int getActionUpdateNum() {
      return this.actionUpdateNum;
   }

   public void setActionUpdateNum(int var1) {
      this.actionUpdateNum = var1;
   }

   public PairCachingGhostObjectAlignable getGhostObject() {
      return this.ghostObject;
   }

   public void transformAimingAt(Vector3f var1, Damager var2, SimpleGameObject var3, Random var4) {
      var1.set(0.0F, 0.0F, 0.0F);
      var1.add(var3.getClientTransform().origin);
   }

   public FlashLight getFlashLightActive() {
      return this.flashLightActive;
   }

   public void setFlashLightActive(FlashLight var1) {
      this.flashLightActive = var1;
   }

   public void sendClientMessage(String var1, int var2) {
      if (this.isClientOwnObject()) {
         switch(var2) {
         case 1:
            ((GameClientState)this.getState()).getController().popupInfoTextMessage(var1, 0.0F);
            return;
         default:
            ((GameClientState)this.getState()).getController().popupAlertTextMessage(var1, 0.0F);
         }
      }

   }

   public void sendServerMessage(Object[] var1, int var2) {
      if (this.getOwnerState() != null) {
         this.getOwnerState().sendServerMessage(var1, var2);
      }

   }

   public float getDamageGivenMultiplier() {
      return 1.0F;
   }

   public TopLevelType getTopLevelType() {
      return TopLevelType.ASTRONAUT;
   }

   public InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3) {
      InterEffectSet var4 = this.getOwnerState().getAttackEffectSet(var1, var3);

      assert var4 != null;

      return var4;
   }

   public MetaWeaponEffectInterface getMetaWeaponEffect(long var1, DamageDealerType var3) {
      return this.getOwnerState().getMetaWeaponEffect(var1, var3);
   }

   public DamageBeamHitHandler getDamageBeamHitHandler() {
      return this.damageBeamHitHandler;
   }

   public boolean canBeDamagedBy(Damager var1, DamageDealerType var2) {
      return true;
   }

   static class CharacterEffectSet extends InterEffectContainer {
      private CharacterEffectSet() {
      }

      public InterEffectSet[] setupEffectSets() {
         InterEffectSet[] var1 = new InterEffectSet[1];

         for(int var2 = 0; var2 < var1.length; ++var2) {
            var1[var2] = new InterEffectSet();
         }

         return var1;
      }

      public InterEffectSet get(HitReceiverType var1) {
         if (var1 != HitReceiverType.CHARACTER) {
            throw new RuntimeException("illegal hit received " + var1.name());
         } else {
            return this.sets[0];
         }
      }

      public void update(ConfigEntityManager var1) {
      }

      // $FF: synthetic method
      CharacterEffectSet(Object var1) {
         this();
      }
   }
}
