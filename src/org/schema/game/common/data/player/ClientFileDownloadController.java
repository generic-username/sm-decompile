package org.schema.game.common.data.player;

import java.io.File;
import java.io.IOException;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.UploadInProgressException;
import org.schema.game.server.controller.CatalogEntryNotFoundException;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.resource.FileExt;

public class ClientFileDownloadController extends UploadController {
   private boolean needsUpdate;
   private String currentDownloadPlayerName;

   public ClientFileDownloadController(ClientChannel var1) {
      super(var1);
   }

   public ClientChannel getChannel() {
      return (ClientChannel)this.sendable;
   }

   public String getCurrentDownloadName() {
      return this.currentDownloadPlayerName;
   }

   public void setCurrentDownloadName(String var1) {
      this.currentDownloadPlayerName = var1;
   }

   protected File getFileToUpload(String var1) throws IOException, CatalogEntryNotFoundException {
      return new FileExt(var1);
   }

   protected String getNewFileName() {
      return this.currentDownloadPlayerName;
   }

   protected RemoteBuffer getUploadBuffer() {
      return this.getChannel().getNetworkObject().downloadBuffer;
   }

   protected void handleUploadedFile(File var1) {
      System.err.println("[CLIENT] successfully received file from server: " + var1.getAbsolutePath());
      ((GameClientState)this.getChannel().getState()).getController().getTextureSynchronizer().handleDownloaded(var1);
      this.setNeedsUpdate(false);
   }

   public void upload(String var1) throws IOException, UploadInProgressException {
      super.upload(var1);
      this.setNeedsUpdate(true);
   }

   public boolean isNeedsUpdate() {
      return this.needsUpdate;
   }

   public void setNeedsUpdate(boolean var1) {
      this.needsUpdate = var1;
   }
}
