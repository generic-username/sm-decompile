package org.schema.game.common.data.player;

import java.util.Date;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class PlayerInfoHistory implements Comparable, TagSerializable {
   public long time;
   public String ip;
   public String starmadeName;

   public PlayerInfoHistory() {
   }

   public PlayerInfoHistory(long var1, String var3, String var4) {
      this.time = var1;
      this.ip = var3;
      this.starmadeName = var4;
   }

   public String toString() {
      return "[time=" + new Date(this.time) + ", ip=" + this.ip + ", starmadeName=" + this.starmadeName + "]";
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.time = (Long)var2[0].getValue();
      this.ip = (String)var2[1].getValue();
      this.starmadeName = (String)var2[2].getValue();
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.LONG, (String)null, this.time), new Tag(Tag.Type.STRING, (String)null, this.ip), new Tag(Tag.Type.STRING, (String)null, this.starmadeName != null ? this.starmadeName : ""), FinishTag.INST});
   }

   public int compareTo(PlayerInfoHistory var1) {
      return (int)(this.time - var1.time);
   }
}
