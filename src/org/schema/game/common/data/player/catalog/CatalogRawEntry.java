package org.schema.game.common.data.player.catalog;

import org.schema.game.common.controller.elements.EntityIndexScore;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;
import org.schema.game.server.data.blueprintnw.BlueprintType;

public class CatalogRawEntry {
   public final String name;
   public final long price;
   public final BlueprintType entityType;
   public final float mass;
   public final EntityIndexScore score;
   BlueprintClassification classification;

   public CatalogRawEntry(String var1, long var2, EntityIndexScore var4, BlueprintType var5, BlueprintClassification var6, float var7) {
      this.name = var1;
      this.price = var2;
      this.classification = var6;
      this.entityType = var5;
      this.mass = var7;
      this.score = var4;
   }

   public int hashCode() {
      return this.name.hashCode();
   }

   public boolean equals(Object var1) {
      return this.name.equals(((CatalogRawEntry)var1).name);
   }
}
