package org.schema.game.common.data.player.catalog;

import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.objects.Object2IntArrayMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import java.util.Map.Entry;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import org.schema.common.LogUtil;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.io.SegmentSerializationBuffers;
import org.schema.game.common.data.SendableGameState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Universe;
import org.schema.game.network.objects.NetworkGameState;
import org.schema.game.network.objects.remote.RemoteCatalogEntry;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.controller.CatalogEntryNotFoundException;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.controller.ImportFailedException;
import org.schema.game.server.controller.MayImportCallback;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.game.server.data.ServerConfig;
import org.schema.game.server.data.blueprint.BluePrintWriteQueueElement;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.game.server.data.blueprintnw.BlueprintType;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.network.objects.remote.RemoteArray;
import org.schema.schine.network.objects.remote.RemoteByteArrayDyn;
import org.schema.schine.network.objects.remote.RemoteStringArray;
import org.schema.schine.network.server.ServerMessage;
import org.schema.schine.resource.DiskWritable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class CatalogManager extends Observable implements DiskWritable {
   public static final String CALAOG_FILE_NAME = "CATALOG";
   public static final String CALAOG_FILE_EXTENSION = "cat";
   public static final String CALAOG_FILE_NAME_FULL = "CATALOG.cat";
   private final SendableGameState gameState;
   private final HashMap rawBlueprints = new HashMap();
   private final HashMap serverCatalog = new HashMap();
   private final HashMap serverCatalogRatings = new HashMap();
   private final ArrayList catalogRatingsToAddClient = new ArrayList();
   private final ArrayList catalogToAddClient = new ArrayList();
   private final ArrayList catalogToRemoveClient = new ArrayList();
   private final ArrayList catalogChangeRequests = new ArrayList();
   private final ArrayList catalogDeleteRequests = new ArrayList();
   private final ObjectOpenHashSet clientCatalogPermissions = new ObjectOpenHashSet();
   private boolean flagServerCatalogChanged;
   private boolean hasAnyEnemySpawnable;
   private long lastSendEnemySpawnMsg;
   private boolean onServer;
   private boolean flagServerCatalogAddedOrRemoved;
   private static FastByteArrayOutputStream b = new FastByteArrayOutputStream(1048576);
   private static long lastModified;
   private static long lastCached = -1L;
   private static byte[] currentCatalogDeflatedArray;

   public CatalogManager(SendableGameState var1) {
      this.gameState = var1;
      this.onServer = var1.isOnServer();
      if (var1.isOnServer()) {
         this.refreshServerRawBlueprints();

         try {
            Tag var4 = ((GameServerState)var1.getState()).getController().readEntity("CATALOG", "cat");
            this.fromTagStructure(var4);
            this.initializeNewDefautCatalog(false);
         } catch (IOException var2) {
            var2.printStackTrace();
         } catch (EntityNotFountException var3) {
            System.err.println("[SERVER] NO DB CATALOG FOUND ON DISK: " + var3.getMessage());
            this.initializeNewDefautCatalog(true);
         }
      }

      this.lastSendEnemySpawnMsg = System.currentTimeMillis();
   }

   public static boolean isValidCatalogType(BlueprintType var0) {
      return var0 == BlueprintType.SHIP || var0 == BlueprintType.SPACE_STATION;
   }

   public synchronized void addObserver(Observer var1) {
      super.addObserver(var1);
   }

   private void addRawCatalogEntry(BlueprintEntry var1) {
      this.rawBlueprints.put(var1.getName(), new CatalogRawEntry(var1.getName(), var1.getPrice(), var1.getScore(), var1.getEntityType(), var1.getClassification(), (float)var1.getMass()));
   }

   public void addServerEntry(BlueprintEntry var1, String var2, boolean var3) {
      CatalogPermission var4;
      (var4 = new CatalogPermission()).setUid(var1.getName());
      var4.ownerUID = var2;
      var4.type = var1.getEntityType();
      var4.setClassification(var1.getClassification());
      var4.score = var1.getScore();
      if (var4.date == 0L) {
         var4.date = System.currentTimeMillis();
      }

      System.err.println("[SERVER][CATALOG] ADDING ENTRY FROM RAW BLUEPRINT: " + var1.getName() + " for " + var2 + "; price: " + var1.getPrice());
      var4.price = var1.getPrice();
      var4.mass = (float)var1.getMass();
      if (var3) {
         var4.permission &= -2;
         var4.permission &= -3;
         var4.permission &= -5;
      }

      var4.description = Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_CATALOG_CATALOGMANAGER_9;
      var4.permission &= -9;
      this.addRawCatalogEntry(var1);
      this.serverCatalog.put(var4.getUid(), var4);
      if (isValidCatalogType(var1.getEntityType())) {
         this.gameState.getNetworkObject().catalogBuffer.add(new RemoteCatalogEntry(var4, this.gameState.getNetworkObject()));
      }

      this.flagServerCatalogChanged = true;
      this.flagServerCatalogAddedOrRemoved = true;
      lastModified = System.currentTimeMillis();
   }

   public boolean canEdit(CatalogPermission var1) {
      String var2 = var1.ownerUID;
      CatalogPermission var3 = (CatalogPermission)this.serverCatalog.get(var1.getUid());
      boolean var4 = var1.changeFlagForced;
      if (var3 == null) {
         ((GameServerState)this.gameState.getState()).getController().sendPlayerMessage(var2, new Object[]{240}, 3);
         return false;
      } else if (!var3.ownerUID.toLowerCase(Locale.ENGLISH).equals(var2.toLowerCase(Locale.ENGLISH)) && !var4) {
         ((GameServerState)this.gameState.getState()).getController().sendPlayerMessage(var2, new Object[]{241}, 3);
         return false;
      } else {
         System.err.println("[CATALOG] CHANGING REQUEST by " + var2 + ": " + var1 + "; ADMIN: " + var4);
         return true;
      }
   }

   public void clientRate(String var1, String var2, int var3) {
      var3 = Math.max(0, Math.min(CatalogPermission.MAX_RATING, var3));
      RemoteStringArray var4;
      (var4 = new RemoteStringArray(3, this.gameState.getNetworkObject())).set(0, (String)var1);
      var4.set(1, (String)var2);
      var4.set(2, (String)String.valueOf(var3));
      this.gameState.getNetworkObject().catalogRatingBuffer.add((RemoteArray)var4);
   }

   public void clientRequestCatalogEdit(CatalogPermission var1) {
      this.gameState.getNetworkObject().catalogChangeRequestBuffer.add(new RemoteCatalogEntry(var1, this.gameState.getNetworkObject()));
   }

   public void clientRequestCatalogRemove(CatalogPermission var1) {
      this.gameState.getNetworkObject().catalogDeleteRequestBuffer.add(new RemoteCatalogEntry(var1, this.gameState.getNetworkObject()));
   }

   public void fromTagStructure(Tag var1) {
      if ("cv0".equals(var1.getName())) {
         Tag[] var8;
         Tag[] var2 = (Tag[])(var8 = (Tag[])var1.getValue())[0].getValue();

         for(int var3 = 0; var3 < var2.length - 1; ++var3) {
            CatalogPermission var4;
            (var4 = new CatalogPermission()).fromTagStructure(var2[var3]);
            if (!this.rawBlueprints.containsKey(var4.getUid())) {
               System.err.println("[CATALOG][ERROR] not found in raw Catalog: " + var4.getUid());
            } else {
               BlueprintEntry var5;
               if (var4.mass == 0.0F || var4.type == null) {
                  try {
                     var5 = BluePrintController.active.getBlueprint(var4.getUid());
                     if (var4.mass == 0.0F) {
                        var4.mass = (float)var5.getMass();
                     }

                     if (var4.type == null) {
                        var4.type = var5.getEntityType();
                     }
                  } catch (EntityNotFountException var7) {
                     var5 = null;
                     var7.printStackTrace();
                     System.err.println("[Exception] catched: NOT ADDING ENTRY: " + var4.getUid() + " to catalog (name not found)");
                     continue;
                  }
               }

               try {
                  var5 = BluePrintController.active.getBlueprint(var4.getUid());
                  var4.score = var5.getScore();
               } catch (EntityNotFountException var6) {
                  var5 = null;
                  var6.printStackTrace();
               }

               this.serverCatalog.put(var4.getUid(), var4);
               if (var4.enemyUsable() && var4.type.enemySpawnable()) {
                  this.hasAnyEnemySpawnable = true;
               }
            }
         }

         Tag[] var9 = (Tag[])var8[1].getValue();

         for(int var10 = 0; var10 < var9.length - 1; ++var10) {
            this.ratingEntryFromTag(var9[var10]);
         }

         this.recalcAllRatings();
      }

   }

   public Tag toTagStructure() {
      Tag[] var1;
      (var1 = new Tag[this.serverCatalog.size() + 1])[this.serverCatalog.size()] = FinishTag.INST;
      int var2 = 0;

      for(Iterator var3 = this.serverCatalog.values().iterator(); var3.hasNext(); ++var2) {
         CatalogPermission var4 = (CatalogPermission)var3.next();
         var1[var2] = var4.toTagStructure();
      }

      Tag var8 = new Tag(Tag.Type.STRUCT, "pv0", var1);
      Tag[] var9;
      (var9 = new Tag[this.serverCatalogRatings.size() + 1])[this.serverCatalogRatings.size()] = new Tag(Tag.Type.FINISH, (String)null, (Tag[])null);
      var2 = 0;

      for(Iterator var6 = this.serverCatalogRatings.entrySet().iterator(); var6.hasNext(); ++var2) {
         Entry var5 = (Entry)var6.next();
         var9[var2] = this.ratingEntryToTag((String)var5.getKey(), (Object2IntArrayMap)var5.getValue());
      }

      Tag var7 = new Tag(Tag.Type.STRUCT, "r0", var9);
      return new Tag(Tag.Type.STRUCT, "cv0", new Tag[]{var8, var7, FinishTag.INST});
   }

   public Collection getCatalog() {
      return (Collection)(this.gameState.isOnServer() ? this.serverCatalog.values() : this.clientCatalogPermissions);
   }

   public String getUniqueIdentifier() {
      return "CATALOG";
   }

   public boolean isVolatile() {
      return false;
   }

   public void importEntry(File var1, final String var2) throws ImportFailedException, IOException {
      BluePrintController.active.importFile(var1, new MayImportCallback() {
         public void callbackOnImportDenied(BlueprintEntry var1) {
            ((GameServerState)CatalogManager.this.gameState.getState()).getController().sendPlayerMessage(var2, new Object[]{242}, 3);
            LogUtil.log().fine("[BLUEPRINT] " + var2 + ": IMPORT OF BLUEPRINT DENIED: " + var1.getName());
         }

         public boolean mayImport(BlueprintEntry var1) {
            return !CatalogManager.this.serverCatalog.containsKey(var1.getName());
         }

         public void onImport(BlueprintEntry var1) {
            CatalogManager.this.addServerEntry(var1, var2, ServerConfig.BLUEPRINT_DEFAULT_PRIVATE.isOn());
            LogUtil.log().fine("[BLUEPRINT] " + var2 + ": IMPORT OF BLUEPRINT DONE: " + var1.getName());
         }
      });
   }

   public void initFromNetworkObject(NetworkGameState var1) {
   }

   private void initializeNewDefautCatalog(boolean var1) {
      assert this.gameState.isOnServer();

      ArrayList var4;
      (var4 = new ArrayList(this.rawBlueprints.size())).addAll(this.rawBlueprints.values());
      Collections.sort(var4, new Comparator() {
         public int compare(CatalogRawEntry var1, CatalogRawEntry var2) {
            if (var1.price == var2.price) {
               return 0;
            } else {
               return var1.price > var2.price ? 1 : -1;
            }
         }
      });
      Iterator var5 = var4.iterator();

      while(var5.hasNext()) {
         CatalogRawEntry var2 = (CatalogRawEntry)var5.next();
         if (!this.serverCatalog.containsKey(var2.name)) {
            CatalogPermission var3;
            CatalogPermission var10000 = var3 = new CatalogPermission();
            var10000.permission |= CatalogPermission.getDefaultCatalogPermission();
            var3.type = var2.entityType;
            var3.setClassification(var2.classification);
            var3.ownerUID = "(unknown)";
            var3.setUid(var2.name);
            var3.score = var2.score;
            var3.price = var2.price;
            var3.mass = var2.mass;
            var3.date = System.currentTimeMillis();
            var3.description = "no description given";
            this.serverCatalog.put(var3.getUid(), var3);
         }
      }

      BluePrintController.active.setImportedByDefault(false);
      this.flagServerCatalogChanged = true;
   }

   private void ratingEntryFromTag(Tag var1) {
      String var2 = var1.getName();
      Object2IntArrayMap var3 = new Object2IntArrayMap();
      this.serverCatalogRatings.put(var2, var3);
      Tag[] var5 = (Tag[])var1.getValue();

      for(int var6 = 0; var6 < var5.length - 1; ++var6) {
         Tag[] var4 = (Tag[])var5[var6].getValue();
         var3.put((String)var4[0].getValue(), ((Byte)var4[1].getValue()).intValue());
      }

   }

   private Tag ratingEntryToTag(String var1, Object2IntArrayMap var2) {
      Tag[] var3;
      (var3 = new Tag[var2.size() + 1])[var2.size()] = FinishTag.INST;
      int var4 = 0;

      for(Iterator var6 = var2.entrySet().iterator(); var6.hasNext(); ++var4) {
         Entry var5 = (Entry)var6.next();
         var3[var4] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.STRING, (String)null, var5.getKey()), new Tag(Tag.Type.BYTE, (String)null, ((Integer)var5.getValue()).byteValue()), FinishTag.INST});
      }

      return new Tag(Tag.Type.STRUCT, var1, var3);
   }

   public void recalcAllRatings() {
      Iterator var1 = this.serverCatalogRatings.entrySet().iterator();

      while(var1.hasNext()) {
         Entry var2 = (Entry)var1.next();
         CatalogPermission var3;
         if ((var3 = (CatalogPermission)this.serverCatalog.get(var2.getKey())) != null) {
            var3.recalculateRating((Object2IntArrayMap)var2.getValue());
         }
      }

   }

   public void refreshServerRawBlueprints() {
      System.currentTimeMillis();
      List var1 = BluePrintController.active.readBluePrints();
      this.rawBlueprints.clear();
      System.err.println("[SERVER][CATALOG] READ RAW BLUEPRINTS: " + var1.size());
      Iterator var3 = var1.iterator();

      while(var3.hasNext()) {
         BlueprintEntry var2 = (BlueprintEntry)var3.next();
         this.addRawCatalogEntry(var2);
      }

   }

   public boolean serverDeletEntry(String var1) {
      if (this.serverCatalog.containsKey(var1)) {
         CatalogPermission var2;
         (var2 = new CatalogPermission()).setUid(var1);
         var2.ownerUID = "ADMIN";
         var2.changeFlagForced = true;
         synchronized(this.catalogDeleteRequests) {
            this.catalogDeleteRequests.add(var2);
            return true;
         }
      } else {
         return false;
      }
   }

   public String serverGetInfo(String var1) {
      CatalogPermission var6;
      if ((var6 = (CatalogPermission)this.serverCatalog.get(var1)) != null) {
         try {
            BlueprintEntry var2 = BluePrintController.active.getBlueprint(var6.getUid());
            StringBuffer var3;
            (var3 = new StringBuffer()).append("UID: " + var6.getUid());
            var3.append("\n");
            var3.append("Owner: " + var6.ownerUID);
            var3.append("\n");
            var3.append("DateMS: " + var6.date);
            var3.append("\n");
            var3.append("DateReadable: " + (new Date(var6.date)).toString());
            var3.append("\n");
            var3.append("Description: " + var6.description);
            var3.append("\n");
            var3.append("Mass: " + var6.mass);
            var3.append("\n");
            var3.append("SpawnCount: " + var6.timesSpawned);
            var3.append("\n");
            var3.append("Price: " + var6.price);
            var3.append("\n");
            var3.append("Rating: " + var6.rating);
            var3.append("\n");
            var3.append("Blocks: " + var2.getElementMap().getTotalAmount());
            var3.append("\n");
            var3.append("BlocksInclChilds: " + var2.getElementCountMapWithChilds().getTotalAmount());
            var3.append("\n");
            var3.append("DockCountOnMother: " + var2.getChilds().size());
            var3.append("\n");
            BoundingBox var4 = new BoundingBox();
            var2.calculateTotalBb(var4);
            var3.append("DimensionInclChilds: " + var4);
            var3.append("\n");
            var3.append("PermissionMask: " + var6.permission);
            var3.append("\n");
            var3.append("PermissionFaction: " + var6.faction());
            var3.append("\n");
            var3.append("PermissionHomeOnly: " + var6.homeOnly());
            var3.append("\n");
            var3.append("PermissionOthers: " + var6.others());
            var3.append("\n");
            var3.append("PermissionEnemyUsable: " + var6.enemyUsable());
            var3.append("\n");
            var3.append("PermissionLocked: " + var6.locked());
            var3.append("\n");
            return var3.toString();
         } catch (EntityNotFountException var5) {
            var5.printStackTrace();
         }
      }

      return null;
   }

   public boolean serverChangeOwner(String var1, String var2) {
      if (this.serverCatalog.containsKey(var1)) {
         CatalogPermission var3;
         (var3 = new CatalogPermission((CatalogPermission)this.serverCatalog.get(var1))).setUid(var1);
         var3.ownerUID = var2;
         var3.changeFlagForced = true;
         synchronized(this.catalogChangeRequests) {
            this.catalogChangeRequests.add(var3);
            return true;
         }
      } else {
         return false;
      }
   }

   public void updateFromNetworkObject(NetworkGameState var1) {
      Iterator var2 = var1.catalogRatingBuffer.getReceiveBuffer().iterator();

      int var28;
      while(var2.hasNext()) {
         RemoteStringArray var3 = (RemoteStringArray)var2.next();

         assert this.gameState.isOnServer();

         String var4 = (String)var3.get(0).get();
         String var5 = (String)var3.get(1).get();
         var28 = Integer.parseInt((String)var3.get(2).get());
         CatalogRating var6 = new CatalogRating(var4, var5, var28);
         synchronized(this.catalogRatingsToAddClient) {
            this.catalogRatingsToAddClient.add(var6);
         }
      }

      var2 = var1.catalogBufferDeflated.getReceiveBuffer().iterator();

      while(var2.hasNext()) {
         RemoteByteArrayDyn var29 = (RemoteByteArrayDyn)var2.next();
         SegmentSerializationBuffers var31 = SegmentSerializationBuffers.get();

         try {
            assert !this.gameState.isOnServer();

            byte[] var34 = (byte[])var29.get();
            byte[] var30 = var31.SEGMENT_BUFFER;
            Inflater var36 = var31.inflater;
            DataInputStream var35;
            int var7 = (var35 = new DataInputStream(new FastByteArrayInputStream(var34))).readInt();
            int var8 = var35.readInt();
            System.err.println("[CLIENT][CATALOG] Received compressed catalog: Compressed " + var8 / 1024 + " kb; uncompressed: " + var7 / 1024 + "kb");
            byte[] var9 = var31.getStaticArray(var7);
            int var10 = var35.read(var30, 0, var8);

            assert var10 == var8 : var10 + "/" + var8;

            var36.reset();
            var36.setInput(var30, 0, var8);
            var28 = var36.inflate(var9, 0, var7);

            assert var28 == var7 : var28 + " / " + var7;

            DataInputStream var37;
            var8 = (var37 = new DataInputStream(new FastByteArrayInputStream(var9, 0, var7))).readInt();
            synchronized(this.catalogToAddClient) {
               var10 = 0;

               while(true) {
                  if (var10 >= var8) {
                     break;
                  }

                  CatalogPermission var11;
                  (var11 = new CatalogPermission()).deserialize(var37, 0, this.isOnServer());
                  this.catalogToAddClient.add(var11);
                  ++var10;
               }
            }

            var37.close();
            if (var28 == 0) {
               System.err.println("[PRICES] WARNING: INFLATED BYTES 0: " + var36.needsInput() + " " + var36.needsDictionary());
            }

            var35.close();
         } catch (Exception var26) {
            var26.printStackTrace();
            throw new RuntimeException(var26);
         } finally {
            SegmentSerializationBuffers.free(var31);
         }
      }

      var2 = var1.catalogBuffer.getReceiveBuffer().iterator();

      RemoteCatalogEntry var32;
      CatalogPermission var33;
      while(var2.hasNext()) {
         var32 = (RemoteCatalogEntry)var2.next();

         assert !this.gameState.isOnServer();

         var33 = (CatalogPermission)var32.get();

         assert var33.getUid() != null;

         synchronized(this.catalogToAddClient) {
            this.catalogToAddClient.add(var33);
         }
      }

      var2 = var1.catalogDeleteBuffer.getReceiveBuffer().iterator();

      while(var2.hasNext()) {
         var32 = (RemoteCatalogEntry)var2.next();

         assert !this.gameState.isOnServer();

         var33 = (CatalogPermission)var32.get();
         synchronized(this.catalogToRemoveClient) {
            this.catalogToRemoveClient.add(var33);
         }
      }

      var2 = var1.catalogChangeRequestBuffer.getReceiveBuffer().iterator();

      while(var2.hasNext()) {
         var32 = (RemoteCatalogEntry)var2.next();

         assert this.gameState.isOnServer();

         var33 = (CatalogPermission)var32.get();
         System.err.println("[SERVER] received catalog change request: " + var33);
         synchronized(this.catalogChangeRequests) {
            this.catalogChangeRequests.add(var33);
         }
      }

      var2 = var1.catalogDeleteRequestBuffer.getReceiveBuffer().iterator();

      while(var2.hasNext()) {
         var32 = (RemoteCatalogEntry)var2.next();

         assert this.gameState.isOnServer();

         var33 = (CatalogPermission)var32.get();
         System.err.println("[SERVER] received catalog delete request: " + var33);
         synchronized(this.catalogDeleteRequests) {
            this.catalogDeleteRequests.add(var33);
         }
      }

   }

   public void updateLocal() {
      boolean var1 = false;
      CatalogPermission var3;
      CatalogPermission var4;
      if (!this.gameState.isOnServer()) {
         if (!this.catalogToAddClient.isEmpty()) {
            synchronized(this.catalogToAddClient) {
               for(; !this.catalogToAddClient.isEmpty(); var1 = true) {
                  var3 = (CatalogPermission)this.catalogToAddClient.remove(0);
                  if ((var4 = (CatalogPermission)this.clientCatalogPermissions.get(var3)) != null) {
                     var4.apply(var3);
                  } else {
                     this.clientCatalogPermissions.add(var3);
                  }
               }
            }
         }

         if (!this.catalogToRemoveClient.isEmpty()) {
            synchronized(this.catalogToRemoveClient) {
               while(!this.catalogToRemoveClient.isEmpty()) {
                  var3 = (CatalogPermission)this.catalogToRemoveClient.remove(0);
                  this.clientCatalogPermissions.remove(var3);
                  var1 = true;
               }
            }
         }
      } else {
         if (!this.hasAnyEnemySpawnable && System.currentTimeMillis() - this.lastSendEnemySpawnMsg > 300000L) {
            this.lastSendEnemySpawnMsg = System.currentTimeMillis() + 7200000L;
            ((GameServerState)this.gameState.getState()).getController().broadcastMessageAdmin(new Object[]{243}, 3);
         }

         if (!this.catalogRatingsToAddClient.isEmpty()) {
            synchronized(this.catalogRatingsToAddClient) {
               while(!this.catalogRatingsToAddClient.isEmpty()) {
                  CatalogRating var15 = (CatalogRating)this.catalogRatingsToAddClient.remove(0);
                  if ((var4 = (CatalogPermission)this.serverCatalog.get(var15.catalogEntry)) != null) {
                     Object2IntArrayMap var14;
                     if ((var14 = (Object2IntArrayMap)this.serverCatalogRatings.get(var15.catalogEntry)) == null) {
                        var14 = new Object2IntArrayMap();
                        this.serverCatalogRatings.put(var15.catalogEntry, var14);
                     }

                     int var5 = var14.put(var15.initiator, var15.rating);
                     var4.recalculateRating(var14);
                     this.gameState.getNetworkObject().catalogBuffer.add(new RemoteCatalogEntry(var4, this.gameState.getNetworkObject()));
                     ((GameServerState)this.gameState.getState()).getController().sendPlayerMessage(var15.initiator, new Object[]{244, var4.getUid(), var15.rating, var5}, 1);
                     var1 = true;
                  } else {
                     System.err.println("[SERVER][CATALOG][ERROR] cannot rate: " + var15.catalogEntry + ": entry not found");
                  }
               }
            }
         }

         if (!this.catalogChangeRequests.isEmpty()) {
            synchronized(this.catalogChangeRequests) {
               while(!this.catalogChangeRequests.isEmpty()) {
                  var3 = (CatalogPermission)this.catalogChangeRequests.remove(0);
                  if (this.canEdit(var3)) {
                     var4 = (CatalogPermission)this.serverCatalog.get(var3.getUid());
                     var3.rating = var4.rating;
                     System.err.println("[SERVER][CATLOG] permission granted to change: " + var3);
                     this.serverCatalog.put(var3.getUid(), var3);
                     this.gameState.getNetworkObject().catalogBuffer.add(new RemoteCatalogEntry(var3, this.gameState.getNetworkObject()));
                     var1 = true;
                  }
               }
            }
         }

         if (!this.catalogDeleteRequests.isEmpty()) {
            synchronized(this.catalogDeleteRequests) {
               while(!this.catalogDeleteRequests.isEmpty()) {
                  var3 = (CatalogPermission)this.catalogDeleteRequests.remove(0);
                  System.err.println("[SERVER][CATALOG] handling delete request: " + var3);
                  if (this.canEdit(var3)) {
                     var4 = (CatalogPermission)this.serverCatalog.get(var3.getUid());
                     var3.rating = var4.rating;
                     System.err.println("[SERVER][CATLOG] permission granted to delete: " + var3);
                     this.serverCatalog.remove(var3.getUid());
                     this.serverCatalogRatings.remove(var3.getUid());
                     lastModified = System.currentTimeMillis();
                     this.flagServerCatalogAddedOrRemoved = true;
                     BlueprintEntry var16 = new BlueprintEntry(var3.getUid());

                     try {
                        BluePrintController.active.export(var16.getName());
                        System.err.println("[SERVER] PHYSICALLY DELETING BLUEPRINT ENTRY (backup export): " + var16.getName());
                        BluePrintController.active.removeBluePrint(var16);
                     } catch (IOException var7) {
                        ((GameServerState)this.gameState.getState()).getController().sendPlayerMessage(var3.ownerUID, new Object[]{245}, 3);
                        var7.printStackTrace();
                     } catch (CatalogEntryNotFoundException var8) {
                        ((GameServerState)this.gameState.getState()).getController().sendPlayerMessage(var3.ownerUID, new Object[]{246}, 3);
                        var8.printStackTrace();
                     }

                     this.gameState.getNetworkObject().catalogDeleteBuffer.add(new RemoteCatalogEntry(var3, this.gameState.getNetworkObject()));
                     var1 = true;
                  }
               }
            }
         }

         if (this.flagServerCatalogChanged) {
            var1 = true;
            this.flagServerCatalogChanged = false;
         }
      }

      if (var1) {
         this.hasAnyEnemySpawnable = false;
         Iterator var2 = this.serverCatalog.values().iterator();

         while(var2.hasNext()) {
            if (((CatalogPermission)var2.next()).enemyUsable()) {
               this.hasAnyEnemySpawnable = true;
               break;
            }
         }

         this.setChanged();
         this.notifyObservers();
      }

      if (this.flagServerCatalogAddedOrRemoved) {
         if (this.isOnServer()) {
            try {
               System.err.println("[SERVER][CATALOGMANAGER]WRITING CATALOG TO DISK");
               this.writeToDisk();
            } catch (IOException var6) {
               var6.printStackTrace();
            }
         }

         this.flagServerCatalogAddedOrRemoved = false;
      }

   }

   public void updateToFullNetworkObject(NetworkGameState var1) {
      try {
         synchronized(b) {
            if (lastModified != lastCached) {
               lastCached = lastModified;
               b.reset();
               DataOutputStream var3 = new DataOutputStream(b);
               ObjectArrayList var4 = new ObjectArrayList();
               Iterator var5 = this.serverCatalog.values().iterator();

               while(var5.hasNext()) {
                  CatalogPermission var6 = (CatalogPermission)var5.next();
                  CatalogRawEntry var7;
                  if ((var7 = (CatalogRawEntry)this.rawBlueprints.get(var6.getUid())) != null && isValidCatalogType(var7.entityType)) {
                     var4.add(var6);
                  }
               }

               var3.writeInt(var4.size());
               var5 = var4.iterator();

               while(var5.hasNext()) {
                  ((CatalogPermission)var5.next()).serialize(var3, this.isOnServer());
               }

               SegmentSerializationBuffers var16 = SegmentSerializationBuffers.get();

               try {
                  byte[] var17 = var16.SEGMENT_BUFFER;
                  Deflater var18 = var16.deflater;
                  int var15 = (int)b.position();
                  var18.reset();
                  var18.setInput(b.array, 0, var15);
                  var18.finish();
                  int var19 = var18.deflate(var17);
                  System.err.println("[SERVER][CATALOG] Deflated Catalog from " + var15 + " kb to " + var19 + " kb");
                  b.reset();
                  var3.writeInt(var15);
                  var3.writeInt(var19);
                  var3.write(var17, 0, var19);
                  byte[] var14 = new byte[(int)b.position()];
                  System.arraycopy(b.array, 0, var14, 0, (int)b.position());
                  currentCatalogDeflatedArray = var14;
               } finally {
                  SegmentSerializationBuffers.free(var16);
               }
            }
         }

         var1.catalogBufferDeflated.add(new RemoteByteArrayDyn(currentCatalogDeflatedArray, var1));
      } catch (IOException var13) {
         var13.printStackTrace();
      }
   }

   public void updateToNetworkObject(NetworkGameState var1) {
   }

   public void writeEntryClient(BluePrintWriteQueueElement var1, String var2) throws IOException {
      assert !this.isOnServer();

      assert var1.local;

      var1.segmentController.setAllTouched(true);
      var1.segmentController.writeAllBufferedSegmentsToDatabase(true, true, true);
      BluePrintController.active.writeBluePrint(var1.segmentController, var1.name, var1.local, var1.classification);
   }

   public void writeEntryServer(BluePrintWriteQueueElement var1, String var2) throws IOException {
      assert this.isOnServer();

      var1.segmentController.setAllTouched(true);
      if (!ServerConfig.CATALOG_NAME_COLLISION_HANDLING.isOn()) {
         List var5;
         int var8;
         if (this.existsLowerCase(var1.name)) {
            Iterator var6 = this.serverCatalog.entrySet().iterator();

            Entry var9;
            do {
               if (!var6.hasNext()) {
                  var1.segmentController.writeAllBufferedSegmentsToDatabase(true, true, false);
                  BluePrintController.active.writeBluePrint(var1.segmentController, var1.name, var1.local, var1.classification);
                  var5 = BluePrintController.active.readBluePrints();

                  for(var8 = 0; var8 < var5.size(); ++var8) {
                     if (((BlueprintEntry)var5.get(var8)).getName().equals(var1.name)) {
                        this.updateServerEntry(var1.name, (BlueprintEntry)var5.get(var8), var2, ServerConfig.BLUEPRINT_DEFAULT_PRIVATE.isOn(), false);
                        return;
                     }
                  }

                  return;
               }
            } while(!((String)(var9 = (Entry)var6.next()).getKey()).toLowerCase(Locale.ENGLISH).equals(var1.name.toLowerCase(Locale.ENGLISH)) || ((CatalogPermission)var9.getValue()).ownerUID.toLowerCase(Locale.ENGLISH).equals(var2.toLowerCase(Locale.ENGLISH)));

            ((GameServerState)this.gameState.getState()).getController().sendPlayerMessage(var2, new Object[]{247}, 3);
         } else {
            var1.segmentController.writeAllBufferedSegmentsToDatabase(true, true, false);
            BluePrintController.active.writeBluePrint(var1.segmentController, var1.name, var1.local, var1.classification);
            var5 = BluePrintController.active.readBluePrints();

            for(var8 = 0; var8 < var5.size(); ++var8) {
               if (((BlueprintEntry)var5.get(var8)).getName().equals(var1.name)) {
                  this.addServerEntry((BlueprintEntry)var5.get(var8), var2, ServerConfig.BLUEPRINT_DEFAULT_PRIVATE.isOn());
                  return;
               }
            }

         }
      } else {
         int var3 = 0;

         String var4;
         for(var4 = var1.name; this.existsLowerCase(var4); ++var3) {
            var4 = var1.name + String.valueOf(var3);
         }

         var1.name = var4;
         var1.segmentController.writeAllBufferedSegmentsToDatabase(true, true, false);
         BluePrintController.active.writeBluePrint(var1.segmentController, var1.name, var1.local, var1.classification);
         List var7 = BluePrintController.active.readBluePrints();

         for(var3 = 0; var3 < var7.size(); ++var3) {
            if (((BlueprintEntry)var7.get(var3)).getName().equals(var1.name)) {
               this.addServerEntry((BlueprintEntry)var7.get(var3), var2, ServerConfig.BLUEPRINT_DEFAULT_PRIVATE.isOn());
               return;
            }
         }

      }
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public boolean writeEntryAdmin(SegmentController var1, String var2, String var3, BlueprintClassification var4, boolean var5) throws IOException {
      var1.setAllTouched(true);
      var1.writeAllBufferedSegmentsToDatabase(true, false, false);
      BluePrintController.active.writeBluePrint(var1, var2, false, var4);
      List var10 = BluePrintController.active.readBluePrints();

      for(int var6 = 0; var6 < var10.size(); ++var6) {
         if (((BlueprintEntry)var10.get(var6)).getName().equals(var2)) {
            if (!this.existsLowerCase(var2)) {
               this.addServerEntry((BlueprintEntry)var10.get(var6), var3, ServerConfig.BLUEPRINT_DEFAULT_PRIVATE.isOn());
               return true;
            }

            boolean var9;
            if (!(var9 = this.updateServerEntry(var2, (BlueprintEntry)var10.get(var6), var3, ServerConfig.BLUEPRINT_DEFAULT_PRIVATE.isOn(), var5))) {
               try {
                  PlayerState var8;
                  (var8 = ((GameServerState)var1.getState()).getPlayerFromName(var3)).sendServerMessage(new ServerMessage(new Object[]{248}, 3, var8.getId()));
               } catch (PlayerNotFountException var7) {
                  var7.printStackTrace();
               }
            }

            return var9;
         }
      }

      return false;
   }

   private boolean updateServerEntry(String var1, BlueprintEntry var2, String var3, boolean var4, boolean var5) {
      Iterator var6 = this.serverCatalog.entrySet().iterator();

      Entry var7;
      do {
         if (!var6.hasNext()) {
            return false;
         }
      } while(!((String)(var7 = (Entry)var6.next()).getKey()).toLowerCase(Locale.ENGLISH).equals(var1.toLowerCase(Locale.ENGLISH)));

      CatalogPermission var8;
      if (!(var8 = (CatalogPermission)var7.getValue()).ownerUID.toLowerCase(Locale.ENGLISH).equals(var3.toLowerCase(Locale.ENGLISH)) && !var5) {
         return false;
      } else {
         assert var1.toLowerCase(Locale.ENGLISH).equals(var8.getUid().toLowerCase(Locale.ENGLISH));

         System.err.println("[SERVER][CATALOG] UPDATING ENTRY FROM RAW BLUEPRINT: " + var1 + " for " + var3 + "; price: " + var2.getPrice());
         var8.price = var2.getPrice();
         var8.mass = (float)var2.getMass();
         var8.score = var2.getScore();
         if (var4) {
            var8.permission &= -2;
            var8.permission &= -3;
            var8.permission &= -5;
         }

         var8.description = Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_CATALOG_CATALOGMANAGER_10;
         var8.permission &= -9;
         if (isValidCatalogType(var2.getEntityType())) {
            this.gameState.getNetworkObject().catalogBuffer.add(new RemoteCatalogEntry(var8, this.gameState.getNetworkObject()));
         }

         this.flagServerCatalogChanged = true;
         lastModified = System.currentTimeMillis();
         return true;
      }
   }

   private boolean existsLowerCase(String var1) {
      Iterator var2 = this.serverCatalog.keySet().iterator();

      do {
         if (!var2.hasNext()) {
            return false;
         }
      } while(!((String)var2.next()).toLowerCase(Locale.ENGLISH).equals(var1.toLowerCase(Locale.ENGLISH)));

      return true;
   }

   public void writeToDisk() throws IOException {
      Universe.write(this, "CATALOG.cat");
   }
}
