package org.schema.game.common.data.player.catalog;

import it.unimi.dsi.fastutil.objects.Object2IntArrayMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.schema.game.common.controller.elements.EntityIndexScore;
import org.schema.game.server.data.ServerConfig;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;
import org.schema.game.server.data.blueprintnw.BlueprintType;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class CatalogPermission implements SerialializationInterface, TagSerializable {
   public static final int P_BUY_FACTION = 1;
   public static final int P_BUY_OTHERS = 2;
   public static final int P_BUY_HOME_ONLY = 4;
   public static final int P_LOCKED = 8;
   public static final int P_ENEMY_USABLE = 16;
   public static int MAX_RATING = 10;
   public boolean changeFlagForced;
   private String uid;
   public String ownerUID;
   public String description = "";
   public float rating;
   public int timesSpawned;
   public long date;
   public long price;
   public int permission = getDefaultCatalogPermission() | 8;
   public float mass;
   public final List wavePermissions = new ObjectArrayList();
   public BlueprintType type;
   public EntityIndexScore score;
   private BlueprintClassification classification;

   public CatalogPermission() {
   }

   public CatalogPermission(CatalogPermission var1) {
      this.apply(var1);
   }

   public static int getDefaultCatalogPermission() {
      int var0 = ServerConfig.DEFAULT_BLUEPRINT_ENEMY_USE.isOn() ? 16 : 0;
      int var1 = ServerConfig.DEFAULT_BLUEPRINT_FACTION_BUY.isOn() ? 1 : 0;
      int var2 = ServerConfig.DEFAULT_BLUEPRINT_HOME_BASE_BUY.isOn() ? 4 : 0;
      int var3 = ServerConfig.DEFAULT_BLUEPRINT_OTHERS_BUY.isOn() ? 2 : 0;
      return var0 | var1 | var2 | var3;
   }

   public boolean enemyUsable() {
      return (this.permission & 16) == 16;
   }

   public boolean faction() {
      return (this.permission & 1) == 1;
   }

   public BlueprintType getEntry() {
      assert this.type != null;

      return this.type;
   }

   public BlueprintClassification getClassification() {
      if (this.classification == null) {
         this.classification = this.type.type.getDefaultClassification();
      }

      return this.classification;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var7 = (Tag[])var1.getValue();
      this.setUid((String)var7[0].getValue());
      this.ownerUID = (String)var7[1].getValue();
      this.permission = (Integer)var7[2].getValue();
      if (var7[3].getType() == Tag.Type.INT) {
         this.price = (long)(Integer)var7[3].getValue();
      } else {
         this.price = (Long)var7[3].getValue();
      }

      this.description = (String)var7[4].getValue();
      this.date = (Long)var7[5].getValue();
      if (this.date == 0L) {
         this.date = System.currentTimeMillis();
      }

      this.timesSpawned = (Integer)var7[6].getValue();
      if (var7[7].getType() == Tag.Type.FLOAT) {
         this.mass = (Float)var7[7].getValue();
      }

      if (var7.length > 8 && var7[8].getType() != Tag.Type.FINISH) {
         this.type = BlueprintType.values()[(Integer)var7[8].getValue()];
      }

      if (var7.length > 9 && var7[9].getType() != Tag.Type.FINISH) {
         Tag[] var2;
         int var3 = (var2 = (Tag[])var7[9].getValue()).length;

         for(int var4 = 0; var4 < var3; ++var4) {
            Tag var5;
            if ((var5 = var2[var4]).getType() != Tag.Type.FINISH) {
               CatalogWavePermission var6;
               (var6 = new CatalogWavePermission()).fromTagStructure(var5);
               this.wavePermissions.add(var6);
            }
         }
      }

      if (var7.length > 10 && var7[10].getType() != Tag.Type.FINISH) {
         byte var8 = var7[10].getByte();
         this.classification = BlueprintClassification.values()[var8];
      }

   }

   public Tag toTagStructure() {
      Tag var1 = new Tag(Tag.Type.STRING, (String)null, this.getUid());
      Tag var2 = new Tag(Tag.Type.STRING, (String)null, this.ownerUID);
      Tag var3 = new Tag(Tag.Type.INT, (String)null, this.permission);
      Tag var4 = new Tag(Tag.Type.LONG, (String)null, this.price);
      Tag var5 = new Tag(Tag.Type.STRING, (String)null, this.description);
      Tag var6 = new Tag(Tag.Type.LONG, (String)null, this.date);
      Tag var7 = new Tag(Tag.Type.INT, (String)null, this.timesSpawned);
      Tag var8 = new Tag(Tag.Type.FLOAT, (String)null, this.mass);
      Tag var9 = new Tag(Tag.Type.INT, (String)null, this.type.ordinal());
      Tag[] var10 = new Tag[this.wavePermissions.size() + 1];

      for(int var11 = 0; var11 < this.wavePermissions.size(); ++var11) {
         var10[var11] = ((CatalogWavePermission)this.wavePermissions.get(var11)).toTagStructure();
      }

      var10[var10.length - 1] = FinishTag.INST;
      Tag var13 = new Tag(Tag.Type.STRUCT, (String)null, var10);
      Tag var12 = new Tag(Tag.Type.BYTE, (String)null, (byte)this.getClassification().ordinal());
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, var2, var3, var4, var5, var6, var7, var8, var9, var13, var12, FinishTag.INST});
   }

   public int hashCode() {
      return this.getUid().hashCode();
   }

   public boolean equals(Object var1) {
      return this.getUid().equals(((CatalogPermission)var1).getUid());
   }

   public String toString() {
      return "CatalogPermission [catUID=" + this.getUid() + ", ownerUID=" + this.ownerUID + ", description=" + this.description + ", price=" + this.price + ", permission=" + this.permission + "]";
   }

   public boolean homeOnly() {
      return (this.permission & 4) == 4;
   }

   public boolean locked() {
      return (this.permission & 8) == 8;
   }

   public boolean others() {
      return (this.permission & 2) == 2;
   }

   public void recalculateRating(Object2IntArrayMap var1) {
      if (var1.isEmpty()) {
         this.rating = -1.0F;
      } else {
         float var2 = (float)var1.size();
         float var3 = 0.0F;

         int var4;
         for(Iterator var5 = var1.values().iterator(); var5.hasNext(); var3 += (float)var4) {
            var4 = (Integer)var5.next();
         }

         this.rating = var3 / var2;
      }
   }

   public void setPermission(boolean var1, int var2) {
      if (var1) {
         this.permission |= var2;
      } else {
         this.permission &= ~var2;
      }

      System.err.println("SET PERMISSIONS: " + var2 + ": " + var1 + ": " + this.permission + " fac: " + this.faction() + "; others: " + this.others() + "; home " + this.homeOnly() + "; enemy " + this.enemyUsable());
   }

   public void apply(CatalogPermission var1) {
      this.changeFlagForced = var1.changeFlagForced;
      this.setUid(new String(var1.getUid()));
      this.ownerUID = new String(var1.ownerUID);
      this.description = new String(var1.description);
      this.price = var1.price;
      this.mass = var1.mass;
      this.permission = var1.permission;
      this.classification = var1.classification;
      this.type = var1.type;
      this.wavePermissions.clear();
      this.wavePermissions.addAll(var1.wavePermissions);
   }

   public String getUid() {
      return this.uid;
   }

   public void setUid(String var1) {
      this.uid = var1;
   }

   public void setClassification(BlueprintClassification var1) {
      this.classification = var1;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeUTF(this.getUid());
      var1.writeUTF(this.ownerUID);
      var1.writeUTF(this.description);
      var1.writeInt(this.permission);
      var1.writeLong(this.price);
      var1.writeBoolean(this.changeFlagForced);
      var1.writeFloat(this.rating);
      var1.writeFloat(this.mass);
      var1.writeByte((byte)this.type.ordinal());
      var1.writeLong(this.date);
      var1.writeByte(this.getClassification().ordinal());
      boolean var3 = this.score != null;
      var1.writeBoolean(var3);
      if (var3) {
         this.score.serialize(var1, var2);
      }

      var1.writeInt(this.wavePermissions.size());

      for(int var4 = 0; var4 < this.wavePermissions.size(); ++var4) {
         ((CatalogWavePermission)this.wavePermissions.get(var4)).serialize(var1);
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      String var4 = var1.readUTF();
      String var5 = var1.readUTF();
      String var6 = var1.readUTF();
      int var7 = var1.readInt();
      long var8 = var1.readLong();
      boolean var10 = var1.readBoolean();
      float var11 = var1.readFloat();
      float var12 = var1.readFloat();
      byte var13 = var1.readByte();
      long var14 = var1.readLong();
      byte var16 = var1.readByte();
      this.setUid(var4);
      this.ownerUID = var5;
      this.permission = var7;
      this.description = var6;
      this.price = var8;
      this.date = var14;
      this.changeFlagForced = var10;
      this.rating = var11;
      this.mass = var12;
      this.type = BlueprintType.values()[var13];
      this.setClassification(BlueprintClassification.values()[var16]);
      if (var1.readBoolean()) {
         this.score = new EntityIndexScore();
         this.score.deserialize(var1, var2, var3);
      }

      this.wavePermissions.clear();
      var2 = var1.readInt();

      for(int var17 = 0; var17 < var2; ++var17) {
         CatalogWavePermission var18;
         (var18 = new CatalogWavePermission()).deserialize(var1);
         this.wavePermissions.add(var18);
      }

   }
}
