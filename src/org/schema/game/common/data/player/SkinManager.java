package org.schema.game.common.data.player;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import org.schema.common.util.image.ImageAnalysisResult;
import org.schema.common.util.image.ImageUtil;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.UploadInProgressException;
import org.schema.game.common.updater.FileUtil;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.network.objects.remote.RemoteArray;
import org.schema.schine.network.objects.remote.RemoteLongArray;
import org.schema.schine.network.objects.remote.RemoteString;
import org.schema.schine.resource.FileExt;

public class SkinManager {
   public static final String defaultFile = "defaultMale";
   public static final String serverDB = "./server-skins/";
   private static final String extractTmpPath;
   private final PlayerState player;
   public String clientDB;
   private String fileName = "";
   private PlayerSkin textureId;
   private boolean textureIdchanged;
   private boolean openRequest;
   private ArrayList filesReceivedClient = new ArrayList();
   private ArrayList sendQueue = new ArrayList();
   private ArrayList filesReceivedServer = new ArrayList();
   private boolean updateTexture;

   public SkinManager(PlayerState var1) {
      this.player = var1;
      if (var1.isOnServer()) {
         this.clientDB = null;
         (new FileExt("./server-skins/")).mkdirs();
      }

   }

   private void checkReceivedFilesOnClient() throws IOException {
      if (!this.filesReceivedClient.isEmpty()) {
         synchronized(this.filesReceivedClient) {
            File var2;
            long var3;
            if ((var3 = FileUtil.getExtractedFilesSize(var2 = (File)this.filesReceivedClient.get(this.filesReceivedClient.size() - 1))) > 1073741824L) {
               throw new IOException("Extracted files too big (possible zip bomb through sparse files) for " + var2.getAbsolutePath() + ": " + var3 / 1024L / 1024L + " MB");
            } else {
               FileUtil.extract(var2, this.clientDB + "/tmp/");
               FileExt var6;
               if ((var6 = new FileExt(this.clientDB + this.player.getName() + ".smskin")).exists()) {
                  var6.delete();
               }

               FileUtil.copyFile(var2, new FileExt(this.clientDB + this.player.getName() + ".smskin"));
               FileUtil.deleteDir(new FileExt(this.clientDB + "/tmp/"));
               this.filesReceivedClient.clear();
            }
         }
      }
   }

   public void flagUpdateTexture() {
      this.updateTexture = true;
   }

   public String getFileName() {
      return this.fileName;
   }

   public void setFileName(String var1) {
      this.fileName = var1;
   }

   public PlayerSkin getTextureId() {
      return this.textureId;
   }

   public long getTimeStamp() {
      return -1L;
   }

   public void handleReceivedFileOnClient(File var1) {
      synchronized(this.filesReceivedClient) {
         this.filesReceivedClient.add(var1);
      }
   }

   public void handleReceivedFileOnServer(File var1) {
      synchronized(this.filesReceivedServer) {
         this.filesReceivedServer.add(var1);
      }
   }

   public void handleServerNT() {
      for(int var1 = 0; var1 < this.player.getNetworkObject().skinRequestBuffer.getReceiveBuffer().size(); ++var1) {
         ((RemoteString)this.player.getNetworkObject().skinRequestBuffer.getReceiveBuffer().get(var1)).get();
         synchronized(this.sendQueue) {
            this.sendQueue.add("./server-skins/" + this.player.getName() + ".smskin");
         }
      }

   }

   public void initFromNetworkObject() {
      if (!this.player.isClientOwnPlayer()) {
         System.err.println("[SKINMANAGER] " + this.player + " " + this.player.getState() + " received skin filename " + (String)this.player.getNetworkObject().skinName.get());
         this.setFileName((String)this.player.getNetworkObject().skinName.get());
      }

   }

   private void refreshOrLoadTexture() throws IOException {
      if (!this.player.getState().isPassive()) {
         FileExt var1;
         if ((var1 = new FileExt(extractTmpPath)).exists()) {
            FileUtil.deleteRecursive(var1);
         }

         var1.mkdir();
         if (!this.getFileName().equals("defaultMale")) {
            if (this.player.isClientOwnPlayer()) {
               FileExt var2;
               if (!(var2 = new FileExt(this.getFileName())).exists() && var2.getName().endsWith(".smskin")) {
                  throw new RuntimeException("Can't find skin image: " + this.getFileName() + "\n Please change your skin option to a valid file");
               }

               FileUtil.extract(var2, extractTmpPath);
               this.textureId = PlayerSkin.create(var1, this.player.getName());
               this.textureIdchanged = true;
               System.err.println("[SKIN] OWN REFRESH LOADING " + this.getFileName() + " AS " + this.player.getName());
            } else {
               System.err.println("[SKIN] REFRESH LOADING " + this.clientDB + this.player.getName() + ".smskin AS " + this.player.getName());
               FileUtil.extract(new FileExt(this.clientDB + this.player.getName() + ".smskin"), extractTmpPath);
               this.textureId = PlayerSkin.create(new FileExt(extractTmpPath), this.player.getName());
               this.textureIdchanged = true;
            }
         } else {
            System.err.println("[SKIN] USING DEFAULT FOR PLAYER " + this.player);
            this.textureId = null;
            this.textureIdchanged = true;
         }

         System.err.println("[SKIN] REMOVING TMP PATH: " + var1.getAbsolutePath());
         FileUtil.deleteRecursive(var1);
      }

   }

   private void transferFromServerToClient(File var1) {
   }

   public void updateFromNetworkObject() {
      if (this.player.isOnServer() || !this.player.isClientOwnPlayer()) {
         this.setFileName((String)this.player.getNetworkObject().skinName.get());
      }

      this.handleServerNT();
   }

   public void updateOnClient() {
      if (this.clientDB == null && !this.player.isOnServer()) {
         String var1 = ((GameClientState)this.player.getState()).getController().getConnection().getHost();
         this.clientDB = "." + File.separator + "client-skins" + File.separator + ((GameClientState)this.player.getState()).getPlayer().getName() + File.separator + var1 + File.separator;
         (new FileExt(this.clientDB)).mkdirs();
      }

      if (this.openRequest) {
         try {
            this.checkReceivedFilesOnClient();
         } catch (IOException var4) {
            var4.printStackTrace();
         }
      }

      if (this.updateTexture) {
         try {
            this.refreshOrLoadTexture();
         } catch (Exception var3) {
            var3.printStackTrace();
            this.fileName = "defaultMale";

            try {
               this.refreshOrLoadTexture();
            } catch (IOException var2) {
               var2.printStackTrace();
            }
         }

         this.updateTexture = false;
      }

   }

   public void updateServer() {
      String var2;
      if (this.sendQueue.size() > 0) {
         synchronized(this.sendQueue) {
            while(this.sendQueue.size() > 0) {
               var2 = (String)this.sendQueue.remove(0);
               FileExt var3;
               if ((var3 = new FileExt(var2)).exists() && ServerConfig.SKIN_ALLOW_UPLOAD.isOn()) {
                  System.err.println("[SERVER][SkinManager] " + this.player + " requested: " + var2);
                  this.transferFromServerToClient(var3);
               }
            }
         }
      }

      if (!this.filesReceivedServer.isEmpty()) {
         synchronized(this.filesReceivedServer) {
            try {
               File var14 = (File)this.filesReceivedServer.get(this.filesReceivedServer.size() - 1);

               assert var14.exists() : var14.getAbsolutePath();

               long var15 = FileUtil.getExtractedFilesSize(var14);
               long var5 = 1048576L * ((Integer)ServerConfig.ALLOWED_UNPACKED_FILE_UPLOAD_IN_MB.getCurrentState()).longValue();
               if (var15 > var5) {
                  throw new IOException("Extracted files too big (possible zip bomb through sparse files) for " + var14.getAbsolutePath() + ": " + var15 / 1024L / 1024L + " MB");
               }

               FileUtil.extract(var14, "." + File.separator + "tmp" + this.player.getName() + File.separator);
               var14.delete();
               var14 = (new FileExt("." + File.separator + "tmp" + this.player.getName() + File.separator + "tmpUpload" + File.separator)).listFiles()[0];
               String var16 = "./server-skins/" + File.separator + this.player.getName() + File.separator;
               FileExt var4;
               if ((var4 = new FileExt(var16)).exists() && var4.isDirectory()) {
                  FileUtil.deleteRecursive(var4);
               } else if (var4.exists() && !var4.isDirectory()) {
                  var4.delete();
               }

               boolean var19 = true;
               FileUtil.extract(var14, var16);
               File[] var17 = (new FileExt(var16)).listFiles();

               for(int var18 = 0; var18 < var17.length; ++var18) {
                  File var6;
                  if ((var6 = var17[var18]).isDirectory()) {
                     var19 = false;
                  } else if (!var6.getName().equals("skin_main_em.png") && !var6.getName().equals("skin_main_diff.png") && !var6.getName().equals("skin_helmet_em.png") && !var6.getName().equals("skin_helmet_diff.png")) {
                     var19 = false;
                  } else {
                     try {
                        ImageAnalysisResult var7;
                        if (!(var7 = ImageUtil.analyzeImage(var6)).isImage()) {
                           System.err.println("[SERVER][SKIN] file " + var6.getName() + " is not a valid image file! Uploaded by " + this.player.getName());
                           var19 = false;
                        } else if (var7.isTruncated()) {
                           System.err.println("[SERVER][SKIN] file " + var6.getName() + " is not a valid PNG file! Uploaded by " + this.player.getName());
                           var19 = false;
                        } else if (!var7.isPowerOfTwo()) {
                           System.err.println("[SERVER][SKIN] file " + var6.getName() + " is not a valid PNG file (dimensions are not power of 2)! Uploaded by " + this.player.getName());
                           this.player.sendServerMessagePlayerWarning(new Object[]{411});
                           var19 = false;
                        }
                     } catch (NoSuchAlgorithmException var8) {
                        System.err.println("[SERVER][SKIN] file " + var6.getName() + " is not a valid PNG file! Uploaded by " + this.player.getName());
                        var8.printStackTrace();
                        var19 = false;
                     } catch (IOException var9) {
                        System.err.println("[SERVER][SKIN] file " + var6.getName() + " is not a valid PNG file! Uploaded by " + this.player.getName());
                        var9.printStackTrace();
                        var19 = false;
                     } catch (UnsatisfiedLinkError var10) {
                        var10.printStackTrace();
                        ((GameServerState)this.player.getState()).getController().broadcastMessageAdmin(new Object[]{412}, 3);
                     }
                  }
               }

               if (var19) {
                  if ((var4 = new FileExt("./server-skins/" + File.separator + this.player.getName() + ".smskin")).exists()) {
                     var4.delete();
                  }

                  var4 = new FileExt("./server-skins/" + File.separator + this.player.getName() + ".smskin");
                  FileUtil.copyFile(var14, var4);
                  var4.setLastModified(System.currentTimeMillis());
                  this.setFileName(var14.getName());
                  FileUtil.deleteDir(new FileExt("." + File.separator + "tmp" + this.player.getName() + File.separator));
                  RemoteLongArray var20;
                  (var20 = new RemoteLongArray(2, this.player.getNetworkObject())).set(0, (Long)var4.lastModified());
                  var20.set(1, (Long)var4.length());
                  this.player.getNetworkObject().textureChangedBroadcastBuffer.add((RemoteArray)var20);
               } else {
                  ((GameServerState)this.player.getState()).getController().broadcastMessageAdmin(new Object[]{413, this.player.getName()}, 3);
                  var14.delete();
                  FileUtil.deleteDir(new FileExt("." + File.separator + "tmp" + this.player.getName() + File.separator));
               }
            } catch (IOException var11) {
               var2 = null;
               var11.printStackTrace();
            }

            this.filesReceivedServer.clear();
         }
      }
   }

   public void updateToNetworkObject() {
      if (this.player.isOnServer()) {
         this.player.getNetworkObject().skinName.set(this.getFileName());
      } else {
         if (this.player.isClientOwnPlayer()) {
            this.player.getNetworkObject().skinName.forceClientUpdates();
            this.player.getNetworkObject().skinName.set(this.getFileName());
         }

      }
   }

   public void uploadFromClient(String var1) throws IOException, UploadInProgressException {
      this.setFileName(var1);
      FileExt var2;
      if ((var2 = new FileExt(this.clientDB + this.player.getName() + ".smskin")).exists()) {
         var2.delete();
      }

      FileExt var3;
      FileUtil.copyFile(var3 = new FileExt(var1), var2);
      var2.setLastModified(var3.lastModified());
      this.player.getSkinUploadController().upload(this.getFileName());
   }

   public boolean isTextureIdchanged() {
      return this.textureIdchanged;
   }

   public void setTextureIdchanged(boolean var1) {
      this.textureIdchanged = var1;
   }

   static {
      extractTmpPath = "." + File.separator + "texTmp" + File.separator;
   }
}
