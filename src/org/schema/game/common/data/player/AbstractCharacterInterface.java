package org.schema.game.common.data.player;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.world.GravityState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndexElement;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.container.PhysicsDataContainer;

public interface AbstractCharacterInterface {
   Vector4f getTint();

   boolean isSitting();

   boolean isHit();

   boolean isHidden();

   boolean isAnyMetaObjectSelected();

   boolean isMetaObjectSelected(MetaObjectManager.MetaObjectType var1, int var2);

   boolean isAnyBlockSelected();

   Transform getWorldTransform();

   GravityState getGravity();

   int getId();

   void getClientAABB(Vector3f var1, Vector3f var2);

   float getCharacterHeightOffset();

   Vector3f getOrientationUp();

   Vector3f getOrientationRight();

   Vector3f getOrientationForward();

   boolean isInClientRange();

   Vector3i getSittingPos();

   Vector3i getSittingPosTo();

   Vector3i getSittingPosLegs();

   float getCharacterHeight();

   StateInterface getState();

   float getCharacterWidth();

   boolean isOnServer();

   PhysicsDataContainer getPhysicsDataContainer();

   void setActionUpdate(boolean var1);

   int getSectorId();

   boolean isVulnerable();

   void handleCollision(SegmentPiece var1, Vector3f var2);

   int getActionUpdateNum();

   void setActionUpdateNum(int var1);

   void scheduleGravity(Vector3f var1, SimpleTransformableSendableObject var2);

   Vector3f getForward();

   Vector3f getRight();

   Vector3f getUp();

   AnimationIndexElement getAnimationState();

   void setAnimationState(AnimationIndexElement var1);

   boolean onGround();

   PlayerState getConversationPartner();
}
