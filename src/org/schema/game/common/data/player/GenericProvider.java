package org.schema.game.common.data.player;

import org.schema.game.common.controller.NetworkListenerEntity;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.TopLevelType;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.server.ServerStateInterface;

public abstract class GenericProvider implements NetworkListenerEntity {
   protected int id = -123123;
   protected final StateInterface state;
   protected final boolean onServer;
   private boolean markedForDeleteVolatile;
   private boolean markedForDeleteSent;
   private int clientId;
   private SimpleTransformableSendableObject providedObject;
   private boolean listen;

   public GenericProvider(StateInterface var1) {
      this.state = var1;
      this.onServer = var1 instanceof ServerStateInterface;
   }

   public boolean isConnectionReady() {
      return this.getSegmentController() != null && this.getNetworkObject() != null && (Boolean)this.getNetworkObject().connectionReady.get();
   }

   public void cleanUpOnEntityDelete() {
   }

   public void destroyPersistent() {
   }

   public StateInterface getState() {
      return this.state;
   }

   public void initFromNetworkObject(NetworkObject var1) {
      this.setId(this.getNetworkObject().id.get());
      this.setClientId(this.getNetworkObject().clientId.get());
      if (this.getSegmentController() != null) {
         this.getSegmentController().addListener(this);
         this.listen = true;
      }

   }

   public void setProvidedObject(SimpleTransformableSendableObject var1) {
      this.providedObject = var1;
   }

   public SimpleTransformableSendableObject getSegmentController() {
      if (this.providedObject == null) {
         this.providedObject = (SimpleTransformableSendableObject)this.state.getLocalAndRemoteObjectContainer().getLocalObjects().get(this.id);
      }

      return this.providedObject;
   }

   public void initialize() {
   }

   public boolean isMarkedForDeleteVolatile() {
      return this.markedForDeleteVolatile;
   }

   public void setMarkedForDeleteVolatile(boolean var1) {
      this.markedForDeleteVolatile = var1;
   }

   public boolean isMarkedForDeleteVolatileSent() {
      return this.markedForDeleteSent;
   }

   public void setMarkedForDeleteVolatileSent(boolean var1) {
      this.markedForDeleteSent = var1;
   }

   public boolean isMarkedForPermanentDelete() {
      return false;
   }

   public boolean isOkToAdd() {
      return true;
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public boolean isUpdatable() {
      return true;
   }

   public void markForPermanentDelete(boolean var1) {
   }

   public void updateFromNetworkObject(NetworkObject var1, int var2) {
      if (this.getSegmentController() != null && this.getSegmentController().getRemoteTransformable() != null && this.getNetworkObject() != null) {
         this.getSegmentController().getRemoteTransformable().receiveTransformation(this.getNetworkObject());
      } else {
         try {
            if (this.getSegmentController() == null) {
               throw new Exception("segmentController of this provider is null (" + this.id + ") Maybe it was removed on server");
            } else if (this.getSegmentController().getRemoteTransformable() == null) {
               throw new Exception("segmentController.remoteTransformable of this provider is null (" + this.id + ")");
            }
         } catch (Exception var3) {
            var3.printStackTrace();
         }
      }
   }

   public void updateLocal(Timer var1) {
      if (!this.listen && this.getSegmentController() != null) {
         this.getSegmentController().addListener(this);
         this.listen = true;
      }

   }

   public void updateToFullNetworkObject() {
      this.getNetworkObject().id.set(this.getId());
      this.getNetworkObject().clientId.set(this.getClientId());
      if (this.isOnServer()) {
         this.getNetworkObject().connectionReady.set(true);
      }

   }

   public void updateToNetworkObject() {
      this.getNetworkObject().id.set(this.getId());
      if (this.isOnServer()) {
         this.getNetworkObject().connectionReady.set(true);
      }

   }

   public boolean isWrittenForUnload() {
      return true;
   }

   public void setWrittenForUnload(boolean var1) {
   }

   public void announceLag(long var1) {
   }

   public long getCurrentLag() {
      return 0L;
   }

   public void setClientId(int var1) {
      this.clientId = var1;
   }

   public int getClientId() {
      return this.clientId;
   }

   public int getId() {
      return this.id;
   }

   public void setId(int var1) {
      this.id = var1;
   }

   public boolean isSendTo() {
      if (!this.isOnServer()) {
         return true;
      } else {
         RegisteredClientOnServer var1;
         if ((var1 = (RegisteredClientOnServer)((GameServerState)this.getState()).getClients().get(this.getClientId())) != null) {
            PlayerState var3 = (PlayerState)var1.getPlayerObject();
            Sector var2;
            return (var2 = ((GameServerState)this.getState()).getUniverse().getSector(this.getSegmentController().getSectorId())) != null && Sector.isNeighbor(var3.getCurrentSector(), var2.pos);
         } else {
            return false;
         }
      }
   }

   public TopLevelType getTopLevelType() {
      return TopLevelType.SEG_PROVIDER;
   }
}
