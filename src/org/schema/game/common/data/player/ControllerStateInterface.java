package org.schema.game.common.data.player;

import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ShootContainer;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.input.KeyboardMappings;

public interface ControllerStateInterface {
   int ALWAYS_SELECTED_FOR_AI = Integer.MIN_VALUE;

   Vector3i getParameter(Vector3i var1);

   PlayerState getPlayerState();

   Vector3f getForward(Vector3f var1);

   Vector3f getUp(Vector3f var1);

   Vector3f getRight(Vector3f var1);

   boolean isMouseButtonDown(int var1);

   boolean wasMouseButtonDownServer(int var1);

   boolean isUnitInPlayerSector();

   Vector3i getControlledFrom(Vector3i var1);

   boolean isFlightControllerActive();

   int getCurrentShipControllerSlot();

   boolean isKeyDown(KeyboardMappings var1);

   boolean isSelected(PlayerUsableInterface var1, ManagerContainer var2);

   void handleJoystickDir(Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4);

   SimpleTransformableSendableObject getAquiredTarget();

   boolean getShootingDir(SegmentController var1, ShootContainer var2, float var3, float var4, Vector3i var5, boolean var6, boolean var7);

   boolean isSelected(SegmentPiece var1, Vector3i var2);

   boolean isAISelected(SegmentPiece var1, Vector3i var2, int var3, int var4, ElementCollectionManager var5);

   boolean canFlyShip();

   boolean canRotateShip();

   float getBeamTimeout();

   boolean isPrimaryShootButtonDown();

   boolean isSecondaryShootButtonDown();

   boolean canFocusWeapon();

   boolean clickedOnce(int var1);

   void addCockpitOffset(Vector3f var1);
}
