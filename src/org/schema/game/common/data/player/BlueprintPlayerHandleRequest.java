package org.schema.game.common.data.player;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.objects.Sendable;

public class BlueprintPlayerHandleRequest implements SerialializationInterface {
   public String catalogName;
   public String entitySpawnName;
   public boolean save;
   public boolean directBuy;
   public int toSaveShip = -1;
   public boolean setOwnFaction;
   public BlueprintClassification classification;
   public int spawnOnId = -1;
   public long spawnOnBlock = Long.MIN_VALUE;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeUTF(this.catalogName);
      var1.writeUTF(this.entitySpawnName);
      var1.writeBoolean(this.save);
      var1.writeBoolean(this.directBuy);
      var1.writeInt(this.toSaveShip);
      var1.writeBoolean(this.setOwnFaction);
      if (this.classification != null) {
         var1.writeByte(this.classification.ordinal());
      } else {
         var1.writeByte(-1);
      }

      var1.writeInt(this.spawnOnId);
      var1.writeLong(this.spawnOnBlock);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.catalogName = var1.readUTF();
      this.entitySpawnName = var1.readUTF();
      this.save = var1.readBoolean();
      this.directBuy = var1.readBoolean();
      this.toSaveShip = var1.readInt();
      this.setOwnFaction = var1.readBoolean();
      byte var4;
      if ((var4 = var1.readByte()) >= 0) {
         this.classification = BlueprintClassification.values()[var4];
      }

      this.spawnOnId = var1.readInt();
      this.spawnOnBlock = var1.readLong();
   }

   public SegmentPiece getToSpawnOnRail(PlayerState var1, GameServerState var2) {
      SegmentPiece var3 = null;
      if (this.spawnOnId > 0) {
         Sendable var4;
         if (!((var4 = (Sendable)var2.getLocalAndRemoteObjectContainer().getLocalObjects().get(this.spawnOnId)) instanceof SegmentController)) {
            var1.sendServerMessagePlayerError(new Object[]{238});
            return null;
         }

         SegmentPiece var5;
         if ((var5 = ((SegmentController)var4).getSegmentBuffer().getPointUnsave(this.spawnOnBlock)) == null || !var5.isValid() || !var5.getInfo().isRailDockable()) {
            var1.sendServerMessagePlayerError(new Object[]{239});
            return null;
         }

         var3 = var5;
      }

      return var3;
   }
}
