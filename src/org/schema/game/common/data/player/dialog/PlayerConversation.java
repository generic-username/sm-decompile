package org.schema.game.common.data.player.dialog;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import javax.script.ScriptException;
import org.luaj.vm2.LuaError;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerDialogInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.creature.AIPlayer;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.server.ServerStateInterface;

public class PlayerConversation {
   private PlayerState playerState;
   private AbstractOwnerState converationPartner;
   private PlayerConversationManager manager;
   private State currentState;
   private String title;
   private Object[] message;
   private AICreatureDialogAI dialogAI;
   private AwnserContainer[] answers;
   private PlayerDialogInput clientPlayerInput;
   private boolean changed;

   public PlayerConversation(PlayerState var1, AbstractOwnerState var2, PlayerConversationManager var3) {
      this.setPlayerState(var1);
      this.setConverationPartner(var2);
      this.setManager(var3);
      this.dialogAI = new AICreatureDialogAI("aiState", var1, var2);
   }

   public PlayerConversation() {
   }

   public void update(Timer var1) {
      if (this.getPlayerState().isOnServer()) {
         this.updateServer(var1);
      }
   }

   private void updateClient(Timer var1) {
   }

   private void updateServer(Timer var1) {
      try {
         this.dialogAI.updateOnActive(var1);
      } catch (Exception var2) {
         var1 = null;
         var2.printStackTrace();
      }

      if (this.currentState != this.dialogAI.getStateCurrent()) {
         if (this.dialogAI.getStateCurrent() instanceof DialogTextState) {
            DialogTextState var3 = (DialogTextState)this.dialogAI.getStateCurrent();
            this.title = StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_DIALOG_PLAYERCONVERSATION_1, this.getConverationPartner().getName());
            this.message = var3.getMessage();
            this.answers = var3.getAwnsers();
            this.setChanged(true);
         }

         this.currentState = this.dialogAI.getStateCurrent();
         if (this.currentState != null && this.currentState instanceof DialogCancelState) {
            System.err.println("[SERVER] Cancel state reached. Ending Conversation here");
            this.manager.cancelCurrentConversationOnServer();
            this.setChanged(false);
         }
      }

   }

   public PlayerState getPlayerState() {
      return this.playerState;
   }

   public void setPlayerState(PlayerState var1) {
      this.playerState = var1;
   }

   public AbstractOwnerState getConverationPartner() {
      return this.converationPartner;
   }

   public void setConverationPartner(AbstractOwnerState var1) {
      this.converationPartner = var1;
   }

   public void serialize(DataOutput var1) throws IOException {
      var1.writeUTF(this.title);
      var1.writeByte(this.message.length);

      int var2;
      for(var2 = 0; var2 < this.message.length; ++var2) {
         var1.writeUTF(this.message[var2].toString());
      }

      var1.writeByte((byte)this.answers.length);

      for(var2 = 0; var2 < this.answers.length; ++var2) {
         var1.writeByte(this.answers[var2].awnser.length);

         for(int var3 = 0; var3 < this.answers[var2].awnser.length; ++var3) {
            var1.writeUTF(this.answers[var2].awnser[var3].toString());
         }
      }

   }

   public void handleClientSelect(int var1) {
      assert this.getState() instanceof ServerStateInterface;

      System.err.println("[SERVER] Received selection " + var1 + " for Current: " + this.currentState);
      if (this.currentState != null) {
         if (this.currentState instanceof DialogTextState) {
            DialogTextState var2 = (DialogTextState)this.currentState;
            if (var1 == this.answers.length - 1) {
               System.err.println("[SERVER] " + this.getPlayerState() + " canceling conversation with " + this.getConverationPartner());
               this.manager.cancelCurrentConversationOnServer();
               return;
            }

            try {
               var2.stateTransition(Transition.DIALOG_AWNSER, var1);
               return;
            } catch (FSMException var3) {
               var3.printStackTrace();
               return;
            }
         }

         if (this.currentState instanceof DialogCancelState) {
            this.manager.cancelCurrentConversationOnServer();
         }
      }

   }

   public void deserialize(DataInput var1) throws IOException {
      this.title = var1.readUTF();
      byte var2 = var1.readByte();
      this.message = new String[var2];

      int var3;
      for(var3 = 0; var3 < var2; ++var3) {
         this.message[var3] = var1.readUTF();
      }

      this.answers = new AwnserContainer[var1.readByte()];

      for(var3 = 0; var3 < this.answers.length; ++var3) {
         var2 = var1.readByte();
         this.answers[var3] = new AwnserContainer(new Object[var2]);

         for(int var4 = 0; var4 < var2; ++var4) {
            this.answers[var3].awnser[var4] = var1.readUTF();
         }
      }

   }

   public void deactivateClient() {
      if (this.playerState.isClientOwnPlayer() && this.clientPlayerInput != null) {
         this.clientPlayerInput.deactivate();
         ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().setDelayedActive(true);
         ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().hinderInteraction(500);
      }

   }

   public Object[] getFormatedAwnsers() {
      Object[] var1 = new Object[this.answers.length];

      for(int var2 = 0; var2 < this.answers.length; ++var2) {
         var1[var2] = this.getFormatedAwnser(var2);
      }

      return var1;
   }

   public String getFormatedAwnser(int var1) {
      return getFormatedMessage(this.answers[var1].awnser);
   }

   public static String getFormatedMessage(Object[] var0) {
      String var1;
      if ((var1 = (String)PlayerConversationManager.translations.get(var0[0].toString().trim())) != null) {
         var0[0] = var1;
      }

      if (var0.length == 1) {
         return var0[0].toString();
      } else {
         switch(var0.length) {
         case 2:
            return String.format(var0[0].toString(), var0[1]);
         case 3:
            return String.format(var0[0].toString(), var0[1], var0[2]);
         case 4:
            return String.format(var0[0].toString(), var0[1], var0[2], var0[3]);
         case 5:
            return String.format(var0[0].toString(), var0[1], var0[2], var0[3], var0[4]);
         case 6:
            return String.format(var0[0].toString(), var0[1], var0[2], var0[3], var0[4], var0[5]);
         case 7:
            return String.format(var0[0].toString(), var0[1], var0[2], var0[3], var0[4], var0[5]);
         default:
            return String.format(var0[0].toString(), var0[1], var0[2], var0[3], var0[4], var0[5], var0[6]);
         }
      }
   }

   public String getFormatedMessage() {
      return getFormatedMessage(this.message);
   }

   public void activateClient() {
      if (this.playerState.isClientOwnPlayer()) {
         GameClientState var1 = (GameClientState)this.getState();
         this.clientPlayerInput = new PlayerDialogInput(var1, this.title, this.getFormatedMessage(), this.getFormatedAwnsers()) {
            public void cancel() {
               PlayerConversation.this.manager.pressedSelection(PlayerConversation.this.answers.length - 1);
            }

            public boolean isOccluded() {
               return false;
            }

            public void pressedAwnser(int var1) {
               PlayerConversation.this.manager.pressedSelection(var1);
            }

            public void onDeactivate() {
            }

            public void pressedOK() {
            }
         };
         this.clientPlayerInput.activate();
         System.err.println("ACTIVATING CLIENT CONVERSATION");
      }

   }

   protected void okPressed() {
      System.err.println("PRESSED OK ");
   }

   public StateInterface getState() {
      return this.playerState.getState();
   }

   public PlayerConversationManager getManager() {
      return this.manager;
   }

   public void setManager(PlayerConversationManager var1) {
      this.manager = var1;
   }

   public void createOnServer() {
      if (this.converationPartner instanceof AIPlayer) {
         AbstractOwnerState var10000 = this.converationPartner;

         DialogSystem var1;
         try {
            var1 = DialogSystem.load(this.dialogAI);
            HashMap var2 = new HashMap();
            DialogProgram var3 = new DialogProgram(this.manager, false, var2);
            DialogStateMachine var4;
            (var4 = new DialogStateMachine(this.dialogAI, var3, var1.getFromState())).setDirectStates(var1.getDirectStates());
            var2.put("default", var4);
            var3.reinit(var2);
            this.dialogAI.setCurrentProgram(var3);
            if (this.converationPartner instanceof AIPlayer) {
               System.err.println("[CONVERSATION] CURRENT CONVERSATION STATE: parter " + this.converationPartner + "; player " + this.playerState + ": conversationState: " + ((AIPlayer)this.converationPartner).getConversationState(this.playerState) + "; " + var1.getDirectStates());
               String var9 = ((AIPlayer)this.converationPartner).getConversationState(this.playerState);
               if (var1.getDirectStates().containsKey(var9)) {
                  var4.setStartingState((State)var1.getDirectStates().get(var9));
               } else {
                  System.err.println("[CONVERSATION] Concersation State: " + var9 + " not found. using root state");
               }
            }
         } catch (FileNotFoundException var5) {
            var1 = null;
            var5.printStackTrace();
         } catch (ScriptException var6) {
            var1 = null;
            var6.printStackTrace();
         } catch (LuaError var7) {
            var1 = null;
            var7.printStackTrace();
            ((GameServerState)this.getState()).getController().broadcastMessageAdmin(new Object[]{254}, 3);
         } catch (IOException var8) {
            var1 = null;
            var8.printStackTrace();
         }

         System.err.println("[CONVERSATION][LUA] " + this.playerState + " -> " + this.converationPartner + ": SCRIPT FINISHED CREATING: " + this.converationPartner.getConversationScript());
      }

   }

   public boolean isChanged() {
      return this.changed;
   }

   public void setChanged(boolean var1) {
      this.changed = var1;
   }
}
