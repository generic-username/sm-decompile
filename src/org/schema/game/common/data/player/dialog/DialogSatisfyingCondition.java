package org.schema.game.common.data.player.dialog;

import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.network.StateInterface;

public abstract class DialogSatisfyingCondition extends State {
   private final StateInterface gameState;
   protected boolean next;
   protected boolean skipIfSatisfiedAtEnter;
   private Object[] message;
   private boolean forcedSatisfied;

   public DialogSatisfyingCondition(AiEntityStateInterface var1, Object[] var2, StateInterface var3) {
      super(var1);
      this.gameState = var3;
      this.setMessage(var2);
   }

   private boolean checkBasicCondition() {
      return this.next;
   }

   protected abstract boolean checkSatisfyingCondition() throws FSMException;

   public void forceSatisfied() {
      System.err.println("Forcing satisfied on " + this);
      this.forcedSatisfied = true;
   }

   public StateInterface getGameState() {
      return this.gameState;
   }

   public Object[] getMessage() {
      return this.message;
   }

   public void setMessage(Object[] var1) {
      this.message = var1;
   }

   public void satisfy() {
      this.next = true;
   }

   public String toString() {
      return super.toString() + ": " + this.getMessage()[0].toString().substring(0, Math.min(10, this.getMessage()[0].toString().length() - 1)) + "...";
   }

   public boolean onEnter() {
      this.next = false;
      this.forcedSatisfied = false;

      try {
         if (this.skipIfSatisfiedAtEnter && this.checkSatisfyingCondition()) {
            System.err.println("AUTO SKIP STATE " + this);
            this.forcedSatisfied = true;
         }
      } catch (FSMException var1) {
         var1.printStackTrace();
      }

      boolean var10000 = this.forcedSatisfied;
      return true;
   }

   public boolean onExit() {
      return true;
   }

   public boolean onUpdate() throws FSMException {
      if ((!this.checkBasicCondition() || !this.checkSatisfyingCondition()) && !this.forcedSatisfied) {
         boolean var10000 = this.next;
         return true;
      } else {
         System.err.println("FORCED SATISFIED");
         this.forcedSatisfied = false;
         this.getEntityState().getMachine().getFsm().stateTransition(Transition.CONDITION_SATISFIED);
         return false;
      }
   }
}
