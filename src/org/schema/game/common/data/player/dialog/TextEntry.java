package org.schema.game.common.data.player.dialog;

import javax.script.Bindings;
import org.luaj.vm2.LuaFunction;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.network.StateInterface;

public class TextEntry extends DialogStateMachineFactoryEntry {
   private long timeShown;
   private Object[] message;

   public TextEntry(String var1, long var2) {
      this.message = new Object[]{var1};
      this.timeShown = var2;
   }

   public TextEntry(Object[] var1, long var2) {
      this.message = var1;
      this.timeShown = var2;
   }

   public State getState(AiEntityStateInterface var1, StateInterface var2, Bindings var3) {
      DialogTextEntryHook var4 = null;
      if (this.getHook() != null) {
         assert var3 != null : "TextEntry: Binding null";

         System.err.println("[LUA] Getting bindings for: " + this.getHook().getHandleFunction());
         LuaFunction var5 = (LuaFunction)var3.get(this.getHook().getHandleFunction());
         var4 = new DialogTextEntryHook(var5, this.getHook().getArguments(), this.getHook().getStartState(), this.getHook().getEndState(), this.getHook().getFollowUp(), this.getHook().getCondition(), var3);
      }

      return new DialogTextState(var1, var2, var4, this.message, this.timeShown);
   }

   public String toString() {
      return "(textState: " + this.message + ")";
   }
}
