package org.schema.game.common.data.player.dialog;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map.Entry;
import javax.script.Bindings;
import org.schema.game.client.controller.tutorial.factory.AbstractFSMFactory;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.network.StateInterface;

public abstract class DialogStateMachineFactoryEntry {
   private final ObjectArrayList childs = new ObjectArrayList();
   int transitionsMade = 0;
   private DialogTextEntryHookLua hook;
   private Object2ObjectOpenHashMap awnsers = new Object2ObjectOpenHashMap();
   private DialogStateMachineFactoryEntry parent;
   private State s;
   private String entryMarking;
   private boolean awnserEntry = true;

   public void add(DialogStateMachineFactoryEntry var1, String var2) {
      this.add(var1, new Object[]{var2});
   }

   public void add(DialogStateMachineFactoryEntry var1, Object[] var2) {
      var1.parent = this;
      this.childs.add(var1);
      System.err.println("PUT AWNSERS: " + var1 + "; " + Arrays.toString(var2));
      this.awnsers.put(var1, var2);
   }

   public boolean isRoot() {
      return this.parent == null;
   }

   public void addReaction(DialogTextEntryHookLua var1, int var2, DialogStateMachineFactoryEntry var3) {
      assert var1 == this.hook : "HOOK DIFFERENT (DialogStateMachineFactoryEntry) " + var1 + "; " + this.hook;

      if (var1 != null) {
         var1.getHookChilds().put(var2, var3);
      } else {
         assert false : var1;

      }
   }

   public DialogTextEntryHookLua getHook() {
      return this.hook;
   }

   public void setHook(DialogTextEntryHookLua var1) {
      this.hook = var1;
   }

   public abstract State getState(AiEntityStateInterface var1, StateInterface var2, Bindings var3);

   public void createFrom(Object2ObjectOpenHashMap var1, ObjectOpenHashSet var2, State var3, State var4, DialogStateMachineFactoryEntry var5, Object[] var6, AiEntityStateInterface var7, StateInterface var8, AbstractFSMFactory var9, Bindings var10) {
      if (this.s == null) {
         this.s = this.getState(var7, var8, var10);
         if (this.getEntryMarking() != null) {
            var1.put(this.getEntryMarking(), this.s);
         }
      }

      this.simpleAdd(var3, this.s, var5, var6, var9, var10);
      Iterator var11 = this.childs.iterator();

      while(var11.hasNext()) {
         if ((var5 = (DialogStateMachineFactoryEntry)var11.next()).s == null) {
            assert var5 != this;

            var5.createFrom(var1, var2, this.s, var4, this, (Object[])this.awnsers.get(var5), var7, var8, var9, var10);
         } else {
            var5.simpleAdd(this.s, var5.s, this, (Object[])this.awnsers.get(var5), var9, var10);
         }
      }

      if (this.hook != null) {
         var11 = this.hook.getHookChilds().entrySet().iterator();

         while(var11.hasNext()) {
            Entry var12;
            int var13 = (Integer)(var12 = (Entry)var11.next()).getKey();
            if ((var5 = (DialogStateMachineFactoryEntry)var12.getValue()).s == null) {
               assert var5 != this;

               var5.createHook(var1, var2, this.s, var4, this, var13, var7, var8, var9, var10);
            } else {
               var5.hookAdd(this.s, var5.s, this, var13, var9, var10);
            }
         }
      }

      if (this.childs.isEmpty()) {
         var9.transition(this.s, var4, this.s);
      }

   }

   public void createHook(Object2ObjectOpenHashMap var1, ObjectOpenHashSet var2, State var3, State var4, DialogStateMachineFactoryEntry var5, int var6, AiEntityStateInterface var7, StateInterface var8, AbstractFSMFactory var9, Bindings var10) {
      if (this.s == null) {
         this.s = this.getState(var7, var8, var10);
         if (this.getEntryMarking() != null) {
            var1.put(this.getEntryMarking(), this.s);
         }
      }

      this.hookAdd(var3, this.s, var5, var6, var9, var10);
      Iterator var11 = this.childs.iterator();

      while(var11.hasNext()) {
         if ((var5 = (DialogStateMachineFactoryEntry)var11.next()).s == null) {
            assert var5 != this;

            var5.createFrom(var1, var2, this.s, var4, this, (Object[])this.awnsers.get(var5), var7, var8, var9, var10);
         } else {
            var5.simpleAdd(this.s, var5.s, this, (Object[])this.awnsers.get(var5), var9, var10);
         }
      }

      if (this.hook != null) {
         var11 = this.hook.getHookChilds().entrySet().iterator();

         while(var11.hasNext()) {
            Entry var12;
            var6 = (Integer)(var12 = (Entry)var11.next()).getKey();
            if ((var5 = (DialogStateMachineFactoryEntry)var12.getValue()).s == null) {
               assert var5 != this;

               var5.createHook(var1, var2, this.s, var4, this, var6, var7, var8, var9, var10);
            } else {
               var5.hookAdd(this.s, var5.s, this, var6, var9, var10);
            }
         }
      }

      if (this.childs.isEmpty()) {
         var9.transition(this.s, var4, this.s);
      }

   }

   private void hookAdd(State var1, State var2, DialogStateMachineFactoryEntry var3, int var4, AbstractFSMFactory var5, Bindings var6) {
      var5.transition(var1, var2, var1);
      var1.addTransition(Transition.DIALOG_HOOK, var2, var4, (Object)null);
   }

   private void simpleAdd(State var1, State var2, DialogStateMachineFactoryEntry var3, Object[] var4, AbstractFSMFactory var5, Bindings var6) {
      if (!this.isRoot() && !(var1 instanceof VoidState)) {
         var5.transition(var1, var2, var1);
         if (var4 != null && this.isAwnserEntry()) {
            var1.addTransition(Transition.DIALOG_AWNSER, var2, var3.transitionsMade++, var4);
         }

      } else {
         var1.addTransition(Transition.NEXT, var2);
      }
   }

   public String getEntryMarking() {
      return this.entryMarking;
   }

   public void setEntryMarking(String var1) {
      this.entryMarking = var1;
   }

   public boolean isAwnserEntry() {
      return this.awnserEntry;
   }

   public void setAwnserEntry(boolean var1) {
      this.awnserEntry = var1;
   }
}
