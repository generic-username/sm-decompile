package org.schema.game.common.data.player.dialog;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import java.util.Arrays;

public class DialogTextEntryHookLua {
   private final String handleFunction;
   private final Int2ObjectOpenHashMap hookChilds = new Int2ObjectOpenHashMap();
   private final Object[] arguments;
   private String startState = "NONE";
   private String endState = "NONE";
   private DialogTextEntryHookLua followUp;
   private DialogTextEntryHookLua condition;

   public DialogTextEntryHookLua(String var1, Object... var2) {
      this.handleFunction = var1;
      System.err.println("[LUA] ARGUMENTS for " + var1 + ": " + Arrays.toString(var2));
      this.arguments = var2;
   }

   public String getHandleFunction() {
      return this.handleFunction;
   }

   public Int2ObjectOpenHashMap getHookChilds() {
      return this.hookChilds;
   }

   public Object[] getArguments() {
      return this.arguments;
   }

   public String getStartState() {
      return this.startState;
   }

   public void setStartState(String var1) {
      this.startState = var1;
   }

   public String getEndState() {
      return this.endState;
   }

   public void setEndState(String var1) {
      this.endState = var1;
   }

   public DialogTextEntryHookLua getFollowUp() {
      return this.followUp;
   }

   public void setFollowUp(DialogTextEntryHookLua var1) {
      this.followUp = var1;
   }

   public DialogTextEntryHookLua getCondition() {
      return this.condition;
   }

   public void setCondition(DialogTextEntryHookLua var1) {
      this.condition = var1;
   }
}
