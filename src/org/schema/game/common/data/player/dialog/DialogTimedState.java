package org.schema.game.common.data.player.dialog;

import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.network.StateInterface;

public abstract class DialogTimedState extends DialogSatisfyingCondition {
   protected long entered;

   public DialogTimedState(AiEntityStateInterface var1, Object[] var2, StateInterface var3) {
      super(var1, var2, var3);
   }

   protected boolean checkSatisfyingCondition() {
      return this.next;
   }

   public boolean onEnter() {
      this.entered = System.currentTimeMillis();
      return super.onEnter();
   }

   public abstract long getDurationInMs();
}
