package org.schema.game.common.data.player.dialog;

import javax.script.Bindings;
import org.luaj.vm2.LuaFunction;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class DialogTextEntryHook {
   private final LuaFunction handleFunction;
   private final Varargs args;
   private String endState;
   private String startState;
   private DialogTextEntryHookLua followUp;
   private DialogTextEntryHookLua condition;
   private Bindings bindings;

   public DialogTextEntryHook(LuaFunction var1, Object[] var2, String var3, String var4, DialogTextEntryHookLua var5, DialogTextEntryHookLua var6, Bindings var7) {
      this.handleFunction = var1;
      this.startState = var3;
      this.endState = var4;
      this.followUp = var5;
      this.condition = var6;
      this.bindings = var7;
      LuaValue[] var8 = new LuaValue[var2.length];

      for(int var9 = 0; var9 < var2.length; ++var9) {
         if (var2[var9] instanceof Integer) {
            var8[var9] = LuaValue.valueOf((Integer)var2[var9]);
         } else if (var2[var9] instanceof Boolean) {
            var8[var9] = LuaValue.valueOf((Boolean)var2[var9]);
         } else if (var2[var9] instanceof Double) {
            var8[var9] = LuaValue.valueOf((Double)var2[var9]);
         } else {
            if (!(var2[var9] instanceof String)) {
               System.err.println("[LUA][ERROR] argument is ivalid: " + var2[var9] + "; " + var2[var9].getClass());
               throw new IllegalArgumentException(var2[var9] + "; " + var2[var9].getClass());
            }

            var8[var9] = LuaValue.valueOf((String)var2[var9]);
         }

         System.err.println("[LUA] added hook argument: " + var2[var9]);
      }

      this.args = LuaValue.varargsOf(var8);
   }

   public boolean handleAsCondition(AiEntityStateInterface var1) {
      LuaValue var2 = CoerceJavaToLua.coerce(var1);

      assert this.handleFunction != null : "DialogTextEntryHook: handle function null";

      return this.handleFunction.invoke(var2, this.args).checkboolean(1);
   }

   public void handle(AiEntityStateInterface var1, boolean var2) {
      try {
         System.err.println("[LUA] calling hook " + var1);
         ((AICreatureDialogAI)var1).setConversationState(this.startState);

         assert this.condition == null || this.followUp != null : "DialogTextEntryHook: Follow up not null but condition null";

         assert this.followUp == null || this.condition != null : "DialogTextEntryHook: condition not null but followUp null";

         LuaValue var3 = CoerceJavaToLua.coerce(var1);

         assert this.handleFunction != null : "DialogTextEntryHook: handle function null";

         Varargs var7 = this.handleFunction.invoke(var3, this.args);
         System.err.println("[LUA] return value with args " + this.args + " is " + var7 + ": " + var7.checkint(1) + "; isFollowUp " + var2);
         int var8 = var7.checkint(1);

         try {
            if (!var2 && !(var1.getStateCurrent() instanceof DialogCancelState)) {
               var1.getStateCurrent().stateTransition(Transition.DIALOG_HOOK, var8);
            }
         } catch (FSMException var5) {
            var5.printStackTrace();

            try {
               throw new Exception("System.exit() called");
            } catch (Exception var4) {
               var4.printStackTrace();
               System.exit(0);
            }
         }

         if (this.condition != null) {
            ((AICreatureDialogAI)var1).setConversationState(this.endState);
            ((AICreatureDialogAI)var1).addDelayedConditionFollowUpHook(this, this.condition, this.followUp, this.bindings);
         } else {
            ((AICreatureDialogAI)var1).setConversationState(this.endState);
         }
      } catch (Exception var6) {
         var6.printStackTrace();
      }
   }
}
