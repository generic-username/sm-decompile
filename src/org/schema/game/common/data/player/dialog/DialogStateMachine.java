package org.schema.game.common.data.player.dialog;

import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.MachineProgram;
import org.schema.schine.ai.stateMachines.FiniteStateMachine;
import org.schema.schine.ai.stateMachines.Message;
import org.schema.schine.ai.stateMachines.State;

public class DialogStateMachine extends FiniteStateMachine {
   public DialogStateMachine(AiEntityStateInterface var1, MachineProgram var2, State var3) {
      super(var1, var2, "", var3);
   }

   public void createFSM(String var1) {
   }

   public void onMsg(Message var1) {
   }
}
