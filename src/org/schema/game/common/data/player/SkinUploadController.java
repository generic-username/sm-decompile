package org.schema.game.common.data.player;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import org.schema.common.LogUtil;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.updater.FileUtil;
import org.schema.game.common.util.FolderZipper;
import org.schema.game.server.controller.CatalogEntryNotFoundException;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.RemoteLongArray;
import org.schema.schine.resource.FileExt;

public class SkinUploadController extends UploadController {
   public SkinUploadController(PlayerState var1) {
      super(var1);
   }

   protected void displayTitleMessage() {
      if (this.getPlayer().isClientOwnPlayer()) {
         super.displayTitleMessage();
      }

   }

   protected File getFileToUpload(String var1) throws IOException, CatalogEntryNotFoundException, FileUploadTooBigException {
      String var2 = "client_skin_upload_package.zip";
      FileExt var5;
      if ((var5 = new FileExt(var1)).length() > 262144L) {
         throw new FileUploadTooBigException(var5);
      } else {
         FileExt var3;
         (var3 = new FileExt("./tmpUpload/")).mkdirs();
         FileExt var4;
         (var4 = new FileExt("./tmpUpload/" + this.getPlayer().getName() + ".smskin")).delete();
         var4.createNewFile();
         FileUtil.copyFile(var5, var4);
         FolderZipper.zipFolder("./tmpUpload/", var2, (String)null, (FileFilter)null);
         FileUtil.deleteDir(var3);
         return new FileExt(var2);
      }
   }

   protected String getNewFileName() {
      return this.getPlayer().getName() + "_skin_upload_tmp.zip";
   }

   protected RemoteBuffer getUploadBuffer() {
      return this.getPlayer().getNetworkObject().skinUploadBuffer;
   }

   protected void handleUploadedFile(File var1) {
      try {
         if (var1 != null) {
            assert var1.exists();

            this.getPlayer().getSkinManager().handleReceivedFileOnServer(var1);
            LogUtil.log().fine("[UPLOAD] " + this.getPlayer() + " finished SKIN upload: " + this.getNewFileName());
         }

      } catch (Exception var2) {
         var2.printStackTrace();
         ((GameServerState)this.getPlayer().getState()).getController().broadcastMessage(new Object[]{414, var2.getClass().getSimpleName()}, 3);
      }
   }

   public void handleUploadNT(int var1) {
      if (ServerConfig.SKIN_ALLOW_UPLOAD.isOn()) {
         super.handleUploadNT(var1);
         if (!this.getPlayer().isOnServer()) {
            for(var1 = 0; var1 < this.getPlayer().getNetworkObject().textureChangedBroadcastBuffer.getReceiveBuffer().size(); ++var1) {
               RemoteLongArray var2 = (RemoteLongArray)this.getPlayer().getNetworkObject().textureChangedBroadcastBuffer.getReceiveBuffer().get(var1);
               GameClientState var3 = (GameClientState)this.getPlayer().getState();
               System.err.println("[CLIENT][SKINUPDATE] received texture change broadcast from " + this.getPlayer() + " on Client Player " + var3.getPlayer());
               var3.getController().getTextureSynchronizer().fileChangeBroadcaseted(this.getPlayer(), (Long)var2.get(0).get(), (Long)var2.get(1).get());
            }
         }
      }

   }

   protected void onUploadFinished() {
      (new FileExt(this.getNewFileName())).delete();
      String var1 = "client_skin_upload_package.zip";
      (new FileExt(var1)).delete();
   }

   public PlayerState getPlayer() {
      return (PlayerState)this.sendable;
   }
}
