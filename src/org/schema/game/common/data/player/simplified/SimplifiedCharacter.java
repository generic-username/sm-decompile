package org.schema.game.common.data.player.simplified;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.constraintsolver.SolverConstraint;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.physics.CapsuleShapeExt;
import org.schema.game.common.data.physics.CollisionType;
import org.schema.game.common.data.physics.KinematicCharacterControllerExt;
import org.schema.game.common.data.physics.PairCachingGhostObjectAlignable;
import org.schema.game.common.data.physics.PhysicsExt;
import org.schema.game.common.data.player.AbstractCharacterInterface;
import org.schema.game.common.data.player.ForcedAnimation;
import org.schema.game.common.data.player.PlayerSkin;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.GameTransformable;
import org.schema.game.common.data.world.GravityState;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SectorNotFoundRuntimeException;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndexElement;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.container.PhysicsDataContainer;
import org.schema.schine.network.objects.container.TransformTimed;
import org.schema.schine.physics.Physical;
import org.schema.schine.physics.PhysicsState;

public class SimplifiedCharacter implements AbstractCharacterInterface, GameTransformable {
   private final Vector4f tint = new Vector4f();
   private boolean sitting;
   private PairCachingGhostObjectAlignable ghostObject;
   public PlayerState conversationPartner;
   private final Transform initialTransform = new Transform();
   private final TransformTimed t = new TransformTimed();
   private int id;
   private GravityState gravityState;
   private final Vector3i sittingPos;
   private final Vector3i sittingPosTo;
   private final Vector3i sittingPosLegs;
   private final Vector3f up;
   private final Vector3f forward;
   private final Vector3f right;
   protected KinematicCharacterControllerExt characterController;
   private int actionUpdateNum;
   private int sectorId;
   private final PhysicsDataContainer physicsDataContainer;
   private GravityState scheduledGravity;
   private CapsuleShapeExt capsule;
   private float stepHeight;
   private AnimationIndexElement animationState;
   private PlayerSkin playerSkin;
   private Vector3f movingDir;
   public ForcedAnimation forcedAnimation;

   public SimplifiedCharacter() {
      this.t.setIdentity();
      this.initialTransform.setIdentity();
      this.sittingPos = new Vector3i();
      this.sittingPosTo = new Vector3i();
      this.sittingPosLegs = new Vector3i();
      this.up = new Vector3f();
      this.forward = new Vector3f();
      this.right = new Vector3f();
      this.physicsDataContainer = new PhysicsDataContainer();
      this.stepHeight = 0.388F;
      this.movingDir = new Vector3f();
   }

   public Vector4f getTint() {
      return this.tint;
   }

   public boolean isSitting() {
      return this.sitting;
   }

   public boolean isHit() {
      return false;
   }

   public boolean isHidden() {
      return false;
   }

   public boolean isAnyMetaObjectSelected() {
      return false;
   }

   public boolean isMetaObjectSelected(MetaObjectManager.MetaObjectType var1, int var2) {
      return false;
   }

   public boolean isAnyBlockSelected() {
      return false;
   }

   public TransformTimed getWorldTransform() {
      return this.t;
   }

   public GravityState getGravity() {
      return this.gravityState;
   }

   public int getId() {
      return this.id;
   }

   public void getClientAABB(Vector3f var1, Vector3f var2) {
   }

   public float getCharacterHeightOffset() {
      return 0.0F;
   }

   public Vector3f getForward() {
      GlUtil.getUpVector(this.forward, (Transform)this.t);
      return this.forward;
   }

   public Vector3f getRight() {
      GlUtil.getRightVector(this.right, (Transform)this.t);
      return this.right;
   }

   public Vector3f getUp() {
      GlUtil.getUpVector(this.up, (Transform)this.t);
      return this.up;
   }

   public Vector3f getOrientationUp() {
      return this.characterController.upAxisDirection[1];
   }

   public Vector3f getOrientationRight() {
      return this.characterController.upAxisDirection[0];
   }

   public Vector3f getOrientationForward() {
      return this.characterController.upAxisDirection[2];
   }

   public Vector3i getSittingPos() {
      return this.sittingPos;
   }

   public Vector3i getSittingPosTo() {
      return this.sittingPosTo;
   }

   public Vector3i getSittingPosLegs() {
      return this.sittingPosLegs;
   }

   public void initPhysics() {
      this.ghostObject = new PairCachingGhostObjectAlignable(CollisionType.CHARACTER_SIMPLE, this.getPhysicsDataContainer(), this);
      this.ghostObject.setWorldTransform(this.getInitialTransform());

      assert false : "TODO";

      this.capsule.setMargin(this.getCharacterMargin());
      this.ghostObject.setCollisionShape(this.capsule);
      this.ghostObject.setCollisionFlags(16);
      this.ghostObject.setUserPointer(this.getId());
      this.characterController = new KinematicCharacterControllerExt(this, this.ghostObject, this.capsule, this.stepHeight);
      this.getPhysicsDataContainer().setObject(this.ghostObject);
      this.getPhysicsDataContainer().setShape(this.capsule);
      this.getPhysicsDataContainer().updatePhysical(this.getState().getUpdateTime());
      this.characterController.setGravity(0.0F);
   }

   private float getCharacterMargin() {
      return 0.1F;
   }

   public float getCharacterHeight() {
      return 0.0F;
   }

   public float getCharacterWidth() {
      return 0.0F;
   }

   public StateInterface getState() {
      return null;
   }

   public boolean isOnServer() {
      return false;
   }

   public PhysicsDataContainer getPhysicsDataContainer() {
      return this.physicsDataContainer;
   }

   public void setActionUpdate(boolean var1) {
   }

   public int getSectorId() {
      return this.sectorId;
   }

   public boolean isVulnerable() {
      return true;
   }

   public void handleCollision(SegmentPiece var1, Vector3f var2) {
   }

   public int getActionUpdateNum() {
      return this.actionUpdateNum;
   }

   public void setActionUpdateNum(int var1) {
      this.actionUpdateNum = var1;
   }

   public void scheduleGravity(Vector3f var1, SimpleTransformableSendableObject var2) {
      this.scheduledGravity = new GravityState();
      this.scheduledGravity.getAcceleration().set(var1);
      this.scheduledGravity.source = var2;
      this.scheduledGravity.forcedFromServer = false;
   }

   public void scheduleGravityWithBlockBelow(Vector3f var1, SimpleTransformableSendableObject var2) {
      this.scheduledGravity = new GravityState();
      this.scheduledGravity.getAcceleration().set(var1);
      this.scheduledGravity.source = var2;
      this.scheduledGravity.forcedFromServer = false;
      this.scheduledGravity.withBlockBelow = true;
   }

   public void scheduleGravityServerForced(Vector3f var1, SimpleTransformableSendableObject var2) {
      this.scheduledGravity = new GravityState();
      this.scheduledGravity.getAcceleration().set(var1);
      this.scheduledGravity.source = var2;
      this.scheduledGravity.forcedFromServer = true;
   }

   public void createConstraint(Physical var1, Physical var2, Object var3) {
   }

   public Transform getInitialTransform() {
      return this.initialTransform;
   }

   public float getMass() {
      return 0.0F;
   }

   public void setPhysicsDataContainer(PhysicsDataContainer var1) {
   }

   public void getTransformedAABB(Vector3f var1, Vector3f var2, float var3, Vector3f var4, Vector3f var5, Transform var6) {
   }

   public void engageWarp(String var1, boolean var2, long var3, Vector3i var5, int var6) {
   }

   public void sendControllingPlayersServerMessage(Object[] var1, int var2) {
   }

   public boolean handleCollision(int var1, RigidBody var2, RigidBody var3, SolverConstraint var4) {
      return false;
   }

   public void getGravityAABB(Transform var1, Vector3f var2, Vector3f var3) {
   }

   public PhysicsExt getPhysics() throws SectorNotFoundRuntimeException {
      return (PhysicsExt)this.getPhysicsState().getPhysics();
   }

   public PhysicsState getPhysicsState() throws SectorNotFoundRuntimeException {
      if (this.isOnServer()) {
         Sector var1;
         if ((var1 = ((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId())) == null) {
            System.err.println("[ERROR][FATAL] Fatal Exception: SECTOR NULL FOR " + this + " " + this.getSectorId());
            throw new SectorNotFoundRuntimeException(this.getSectorId());
         } else {
            return var1;
         }
      } else {
         return (GameClientState)this.getState();
      }
   }

   public TransformTimed getWorldTransformOnClient() {
      return this.t;
   }

   public void onPhysicsAdd() {
      this.getPhysicsDataContainer().onPhysicsAdd();

      assert !this.isOnServer() || (Sector)this.getPhysics().getState() == ((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId());

      PhysicsExt var1 = this.getPhysics();
      if ((this.isOnServer() || this.isClientSectorIdValidForSpawning(this.getSectorId())) && this.isOnServer()) {
         assert (Sector)this.getPhysics().getState() == ((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId());

         assert this.getPhysics().getState() == this.getPhysicsState();

         this.getPhysics().addObject(this.getPhysicsDataContainer().getObject(), this.getPhysicsDataContainer().collisionGroup, this.getPhysicsDataContainer().collisionMask);
      }

      if ((this.isOnServer() || this.getSectorId() == ((GameClientState)this.getState()).getCurrentSectorId()) && !var1.containsAction(this.characterController)) {
         var1.getDynamicsWorld().addAction(this.characterController);
      }

      if (this.getGravity().source != null) {
         this.getGravity().setChanged(true);
      }

   }

   public boolean isClientSectorIdValidForSpawning(int var1) {
      return var1 == ((GameClientState)this.getState()).getCurrentSectorId() || this.isPlayerNeighbor(var1);
   }

   public boolean isPlayerNeighbor(int var1) {
      RemoteSector var2;
      return (var2 = (RemoteSector)this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var1)) != null && Sector.isNeighbor(var2.clientPos(), ((GameClientState)this.getState()).getPlayer().getCurrentSector());
   }

   public void onPhysicsRemove() {
      this.getPhysicsDataContainer().onPhysicsRemove();
      if (!this.isOnServer() || ((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId()) != null) {
         this.getPhysics().removeObject(this.getPhysicsDataContainer().getObject());
      }

   }

   public AnimationIndexElement getAnimationState() {
      return this.animationState;
   }

   public void setAnimationState(AnimationIndexElement var1) {
      this.animationState = var1;
   }

   public PlayerSkin getSkin() {
      if (this.playerSkin == null) {
         this.playerSkin = GameResourceLoader.traidingSkin[0];
      }

      return this.playerSkin;
   }

   public boolean onGround() {
      return this.characterController.onGround();
   }

   public Vector3f getMovingDir() {
      return this.movingDir;
   }

   public PlayerState getConversationPartner() {
      return this.conversationPartner;
   }

   public boolean isInClientRange() {
      return true;
   }
}
