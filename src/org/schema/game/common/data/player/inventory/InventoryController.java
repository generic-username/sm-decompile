package org.schema.game.common.data.player.inventory;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.objects.remote.RemoteArray;
import org.schema.schine.network.objects.remote.RemoteIntegerArray;

public class InventoryController {
   private PlayerState player;

   public InventoryController(PlayerState var1) {
      this.player = var1;
   }

   public void buy(short var1, int var2) {
      RemoteIntegerArray var3;
      (var3 = new RemoteIntegerArray(2, this.player.getNetworkObject())).set(0, (Integer)var2);
      var3.set(1, (Integer)Integer.valueOf(var1));
      this.player.getNetworkObject().buyBuffer.add((RemoteArray)var3);
   }

   public void fillInventory() {
      ((GameServerState)this.player.getState()).getGameConfig().fillInventory(this.player);
   }

   public void resetInventory() {
      this.player.getInventory((Vector3i)null).clear();
      this.fillInventory();
   }

   public void sell(short var1, int var2) {
      RemoteIntegerArray var3;
      (var3 = new RemoteIntegerArray(2, this.player.getNetworkObject())).set(0, (Integer)var2);
      var3.set(1, (Integer)Integer.valueOf(var1));
      this.player.getNetworkObject().sellBuffer.add((RemoteArray)var3);
   }

   public void delete(short var1, int var2, int var3) {
      RemoteIntegerArray var4;
      (var4 = new RemoteIntegerArray(3, this.player.getNetworkObject())).set(0, (Integer)var2);
      var4.set(1, (Integer)Integer.valueOf(var1));
      var4.set(2, (Integer)var3);
      this.player.getNetworkObject().deleteBuffer.add((RemoteArray)var4);
   }
}
