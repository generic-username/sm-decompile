package org.schema.game.common.data.player.inventory;

import org.schema.game.common.data.element.ElementKeyMap;

public class NotEnoughBlocksInInventoryException extends Exception {
   private static final long serialVersionUID = 1L;
   public final short type;
   public final int requested;
   public final int available;

   public NotEnoughBlocksInInventoryException(short var1, int var2, int var3) {
      super(ElementKeyMap.exists(var1) ? ElementKeyMap.getInfo(var1).getName() : "unknown(" + var1 + ") wanted " + var2 + " but had: " + var3);
      this.type = var1;
      this.requested = var2;
      this.available = var3;
   }
}
