package org.schema.game.common.data.player.inventory;

import org.schema.game.client.controller.element.world.ClientSegmentProvider;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class StashInventory extends Inventory {
   private final InventoryFilter filter = new InventoryFilter();
   private short production;
   private int productionLimit;
   private String customName = "";

   public StashInventory(InventoryHolder var1, long var2) {
      super(var1, var2);
      this.filter.inventoryId = var2;
   }

   public static int getInventoryType() {
      return 3;
   }

   public Tag toMetaData() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.SHORT, (String)null, this.getProduction()), this.filter.toTagStructure(), new Tag(Tag.Type.STRING, (String)null, this.getCustomName()), new Tag(Tag.Type.INT, (String)null, this.getProductionLimit()), FinishTag.INST});
   }

   public void fromMetaData(Tag var1) {
      Tag[] var2;
      if ((var2 = var1.getStruct())[1].getType() != Tag.Type.FINISH) {
         this.filter.fromTagStructure(var2[1]);
      }

      this.setProduction((Short)var2[0].getValue());
      if (var2.length > 2 && var2[2].getType() != Tag.Type.FINISH) {
         this.setCustomName(var2[2].getString());
      }

      if (var2.length > 3 && var2[3].getType() != Tag.Type.FINISH) {
         this.setProductionLimit(var2[3].getInt());
      }

   }

   public void fromTagStructure(Tag var1) {
      if ("stash".equals(var1.getName())) {
         Tag[] var2 = (Tag[])var1.getValue();
         this.fromMetaData(var2[0]);
         super.fromTagStructure(var2[1]);
      } else {
         super.fromTagStructure(var1);
      }
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, "stash", new Tag[]{this.toMetaData(), super.toTagStructure(), FinishTag.INST});
   }

   public int getActiveSlotsMax() {
      return 0;
   }

   public int getLocalInventoryType() {
      return 3;
   }

   public short getProduction() {
      return this.production;
   }

   public void setProduction(short var1) {
      this.production = var1;
   }

   public InventoryFilter getFilter() {
      return this.filter;
   }

   public String getCustomName() {
      return this.customName;
   }

   public void setCustomName(String var1) {
      this.customName = var1;
   }

   public int getProductionLimit() {
      return this.productionLimit;
   }

   public void setProductionLimit(int var1) {
      this.productionLimit = var1;
   }

   public void requestClient(GameClientState var1) {
      if (this.getInventoryHolder() instanceof ManagerContainer) {
         ((ClientSegmentProvider)((ManagerContainer)this.getInventoryHolder()).getSegmentController().getSegmentProvider()).getSendableSegmentProvider().getNetworkObject().inventoryDetailRequests.add(this.getParameter());
         System.err.println("REQUEST INVENTORY: " + this);
      }

      super.requestClient(var1);
   }
}
