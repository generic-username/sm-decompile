package org.schema.game.common.data.player.inventory;

import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class InventoryFilter {
   private static final byte VERSION = 0;
   public TypeAmountFastMap filter = new TypeAmountFastMap();
   public TypeAmountFastMap fillUpTo = new TypeAmountFastMap();
   public long inventoryId;

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), this.filter.toTagStructure(), this.fillUpTo.toTagStructure(), FinishTag.INST});
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2;
      if ((var2 = var1.getStruct()).length > 0) {
         if (var2[0].getType() == Tag.Type.BYTE) {
            var2[0].getByte();
            this.filter.fromTagStructure(var2[1]);
            this.fillUpTo.fromTagStructure(var2[2]);
            return;
         }

         this.filter.fromTagStructure(var1);
      }

   }

   public void received(InventoryFilter var1) {
      this.filter = var1.filter;
      this.fillUpTo = var1.fillUpTo;
   }
}
