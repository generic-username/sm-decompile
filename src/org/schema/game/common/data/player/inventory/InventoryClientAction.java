package org.schema.game.common.data.player.inventory;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.schine.network.SerialializationInterface;

public class InventoryClientAction implements SerialializationInterface {
   public int ownInventoryOwnerId;
   public long ownInventoryPosId;
   public int otherInventoryOwnerId;
   public long otherInventoryPosId;
   public int slot;
   public int otherSlot;
   public int subSlot;
   public int count;

   public InventoryClientAction() {
   }

   public InventoryClientAction(Inventory var1, Inventory var2, int var3, int var4, int var5, int var6) {
      this.ownInventoryOwnerId = var1.getInventoryHolder().getId();
      if (var1.getParameter() != Long.MIN_VALUE) {
         this.ownInventoryPosId = var1.getParameter();
      } else {
         this.ownInventoryPosId = Long.MIN_VALUE;
      }

      this.otherInventoryOwnerId = var2.getInventoryHolder().getId();
      if (var2.getParameter() != Long.MIN_VALUE) {
         this.otherInventoryPosId = var2.getParameter();
      } else {
         this.otherInventoryPosId = Long.MIN_VALUE;
      }

      this.slot = var3;
      this.otherSlot = var4;
      this.subSlot = var5;
      this.count = var6;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeInt(this.ownInventoryOwnerId);
      var1.writeLong(this.ownInventoryPosId);
      var1.writeInt(this.otherInventoryOwnerId);
      var1.writeLong(this.otherInventoryPosId);
      var1.writeInt(this.slot);
      var1.writeInt(this.otherSlot);
      var1.writeInt(this.subSlot);
      var1.writeInt(this.count);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.ownInventoryOwnerId = var1.readInt();
      this.ownInventoryPosId = var1.readLong();
      this.otherInventoryOwnerId = var1.readInt();
      this.otherInventoryPosId = var1.readLong();
      this.slot = var1.readInt();
      this.otherSlot = var1.readInt();
      this.subSlot = var1.readInt();
      this.count = var1.readInt();
   }

   public String toString() {
      return "InventoryClientAction [ownInventoryOwnerId=" + this.ownInventoryOwnerId + ", ownInventoryPosId=" + this.ownInventoryPosId + ", otherInventoryOwnerId=" + this.otherInventoryOwnerId + ", otherInventoryPosId=" + this.otherInventoryPosId + ", slot=" + this.slot + ", otherSlot=" + this.otherSlot + ", subSlot=" + this.subSlot + ", count=" + this.count + "]";
   }
}
