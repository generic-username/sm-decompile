package org.schema.game.common.data.player.inventory;

public class PlayerInventory extends Inventory {
   public PlayerInventory(InventoryHolder var1, long var2) {
      super(var1, var2);
   }

   public static int getInventoryType() {
      return 2;
   }

   public int getActiveSlotsMax() {
      return 10;
   }

   public int getLocalInventoryType() {
      return 2;
   }

   public String getCustomName() {
      return "";
   }
}
