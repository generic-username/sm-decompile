package org.schema.game.common.data.player.inventory;

import it.unimi.dsi.fastutil.shorts.Short2IntOpenHashMap;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import it.unimi.dsi.fastutil.shorts.ShortList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map.Entry;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class TypeAmountFastMap implements SerialializationInterface {
   private final int[] am;
   private final Short2IntOpenHashMap extra;
   private final ShortArrayList indices;

   public TypeAmountFastMap() {
      this.am = new int[ElementKeyMap.highestType + 1];
      this.extra = new Short2IntOpenHashMap();
      this.indices = new ShortArrayList();
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var4 = (Tag[])var1.getValue();

      for(int var2 = 0; var2 < var4.length - 1; ++var2) {
         Tag[] var3 = (Tag[])var4[var2].getValue();
         this.put((Short)var3[0].getValue(), (Integer)var3[1].getValue());
      }

   }

   public Tag toTagStructure() {
      Tag[] var1;
      Tag[] var10000 = var1 = new Tag[this.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;
      int var2 = 0;

      Iterator var3;
      short var5;
      int var6;
      for(var3 = this.indices.iterator(); var3.hasNext(); ++var2) {
         short var4;
         var5 = var4 = (Short)var3.next();
         var6 = this.am[var4];
         var1[var2] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.SHORT, (String)null, var5), new Tag(Tag.Type.INT, (String)null, var6), FinishTag.INST});
      }

      for(var3 = this.getExtra().entrySet().iterator(); var3.hasNext(); ++var2) {
         Entry var7;
         var5 = (Short)(var7 = (Entry)var3.next()).getKey();
         var6 = (Integer)var7.getValue();
         var1[var2] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.SHORT, (String)null, var5), new Tag(Tag.Type.INT, (String)null, var6), FinishTag.INST});
      }

      return new Tag(Tag.Type.STRUCT, (String)null, var1);
   }

   public boolean containsKey(short var1) {
      if (!ElementKeyMap.isValidType(var1)) {
         return this.getExtra().containsKey(var1);
      } else {
         return this.am[var1] > 0;
      }
   }

   public int get(short var1) {
      return !ElementKeyMap.isValidType(var1) ? this.getExtra().get(var1) : this.am[var1];
   }

   public int put(short var1, int var2) {
      if (!ElementKeyMap.isValidType(var1)) {
         return var1 < 0 ? this.getExtra().put(var1, var2) : 0;
      } else if (var2 <= 0) {
         return this.remove(var1);
      } else {
         int var3 = this.am[var1];
         this.am[var1] = var2;
         if (var3 == 0 && var2 > 0) {
            this.indices.add(var1);
         }

         return var3;
      }
   }

   public int remove(short var1) {
      if (!ElementKeyMap.isValidType(var1)) {
         return this.getExtra().remove(var1);
      } else {
         int var2;
         if ((var2 = this.am[var1]) > 0) {
            this.am[var1] = 0;
            int var3;
            if ((var3 = this.indices.indexOf(var1)) >= 0) {
               this.indices.removeShort(var3);
            }
         }

         return var2;
      }
   }

   public void clear() {
      Arrays.fill(this.am, 0);
      this.indices.clear();
      this.getExtra().clear();
   }

   public boolean containsKey(Object var1) {
      if (!ElementKeyMap.isValidType((Short)var1)) {
         return this.getExtra().containsKey(var1);
      } else {
         return this.am[(Short)var1] > 0;
      }
   }

   public Integer getObj(Object var1) {
      return !ElementKeyMap.isValidType((Short)var1) ? this.getExtra().get(var1) : this.am[(Short)var1];
   }

   public Integer putInt(Short var1, Integer var2) {
      if (!ElementKeyMap.isValidType(var1)) {
         return var1 < 0 ? this.getExtra().put(var1, var2) : 0;
      } else if (var2 <= 0) {
         return this.remove(var1);
      } else {
         int var3 = this.am[var1];
         this.am[var1] = var2;
         if (var3 == 0 && var2 > 0) {
            this.indices.add(var1);
         }

         return var3;
      }
   }

   public Integer removeObj(Object var1) {
      if (!ElementKeyMap.isValidType((Short)var1)) {
         return this.getExtra().remove(var1);
      } else {
         int var2;
         if ((var2 = this.am[(Short)var1]) > 0) {
            this.am[(Short)var1] = 0;
            int var3;
            if ((var3 = this.indices.indexOf(var1)) >= 0) {
               this.indices.removeShort(var3);
            }
         }

         return var2;
      }
   }

   public int size() {
      return this.indices.size() + this.extra.size();
   }

   public boolean isEmpty() {
      return this.indices.isEmpty() && this.extra.isEmpty();
   }

   public ShortList getTypes() {
      return this.indices;
   }

   public Short2IntOpenHashMap getMapInstance() {
      Short2IntOpenHashMap var1 = new Short2IntOpenHashMap(this.size());
      Iterator var2 = this.indices.iterator();

      while(var2.hasNext()) {
         short var3 = (Short)var2.next();
         var1.put(var3, this.am[var3]);
      }

      var1.putAll(this.getExtra());
      return var1;
   }

   public void putMap(Short2IntOpenHashMap var1) {
      Iterator var3 = var1.entrySet().iterator();

      while(var3.hasNext()) {
         Entry var2 = (Entry)var3.next();
         this.put((Short)var2.getKey(), (Integer)var2.getValue());
      }

   }

   public Short2IntOpenHashMap getExtra() {
      return this.extra;
   }

   public void handleLoop(TypeAmountLoopHandle var1) {
      int var2 = this.indices.size();

      for(int var3 = 0; var3 < var2; ++var3) {
         short var4 = this.indices.getShort(var3);
         int var5 = this.am[var4];
         var1.handle(var4, var5);
      }

      Iterator var7 = this.extra.short2IntEntrySet().iterator();

      while(var7.hasNext()) {
         it.unimi.dsi.fastutil.shorts.Short2IntMap.Entry var6 = (it.unimi.dsi.fastutil.shorts.Short2IntMap.Entry)var7.next();
         var1.handle(var6.getShortKey(), var6.getIntValue());
      }

   }

   public void serialize(final DataOutput var1, boolean var2) throws IOException {
      var1.writeShort(this.size());
      this.handleLoop(new TypeAmountLoopHandle() {
         public void handle(short var1x, int var2) {
            try {
               var1.writeShort(var1x);
               var1.writeInt(var2);
            } catch (IOException var3) {
               var3.printStackTrace();
            }
         }
      });
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      short var4;
      if ((var4 = var1.readShort()) > 0) {
         for(int var5 = 0; var5 < var4; ++var5) {
            this.put(var1.readShort(), var1.readInt());
         }
      }

   }

   public String toString() {
      StringBuffer var1;
      (var1 = new StringBuffer()).append("[");

      for(int var2 = 0; var2 < this.indices.size(); ++var2) {
         var1.append(ElementKeyMap.toString(this.indices.getShort(var2)) + "->" + this.am[this.indices.getShort(var2)]);
         if (var2 < this.indices.size() - 1) {
            var1.append(", ");
         }
      }

      var1.append("]");
      return var1.toString();
   }
}
