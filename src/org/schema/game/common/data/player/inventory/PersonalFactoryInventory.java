package org.schema.game.common.data.player.inventory;

import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class PersonalFactoryInventory extends Inventory {
   private short factoryType;

   public PersonalFactoryInventory(InventoryHolder var1, long var2, short var4) {
      super(var1, var2);
      this.setFactoryType(var4);
   }

   public static int getInventoryType() {
      return 2;
   }

   public Tag toMetaData() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.SHORT, (String)null, this.getFactoryType()), FinishTag.INST});
   }

   private void fromMetaData(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.setFactoryType((Short)var2[0].getValue());
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.fromMetaData(var2[0]);
      super.fromTagStructure(var2[1]);
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{this.toMetaData(), super.toTagStructure(), FinishTag.INST});
   }

   public int getActiveSlotsMax() {
      return 0;
   }

   public int getLocalInventoryType() {
      return 2;
   }

   public String getCustomName() {
      return "";
   }

   public short getFactoryType() {
      return this.factoryType;
   }

   public void setFactoryType(short var1) {
      this.factoryType = var1;
   }
}
