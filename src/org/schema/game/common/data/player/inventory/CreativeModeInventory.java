package org.schema.game.common.data.player.inventory;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.schine.resource.tag.Tag;

public class CreativeModeInventory extends Inventory {
   boolean fillMode = false;

   public CreativeModeInventory(InventoryHolder var1, long var2) {
      super(var1, var2);
      List var5;
      Iterator var6 = (var5 = ElementKeyMap.getCategoryHirarchy().getInfoElementsRecursive(new ObjectArrayList())).iterator();

      while(var6.hasNext()) {
         ((ElementInformation)var6.next()).isShoppable();
      }

      this.fillMode = true;
      var6 = var5.iterator();

      while(var6.hasNext()) {
         ElementInformation var3 = (ElementInformation)var6.next();

         try {
            if (var3.isShoppable() && var3.getSourceReference() == 0) {
               super.incExistingOrNextFreeSlot(var3.getId(), 1, -1, this.getActiveSlotsMax());
            }
         } catch (NoSlotFreeException var4) {
            var4.printStackTrace();
         }
      }

      this.fillMode = false;
   }

   public void fromTagStructure(Tag var1) {
   }

   public Tag toTagStructure() {
      return super.toTagStructure();
   }

   public static int getInventoryType() {
      return 6;
   }

   public int getActiveSlotsMax() {
      return 10;
   }

   public int getLocalInventoryType() {
      return 6;
   }

   public String getCustomName() {
      return "";
   }

   public void clear() {
   }

   public void clear(IntOpenHashSet var1) {
   }

   public void decreaseBatch(ElementCountMap var1, IntOpenHashSet var2) throws NotEnoughBlocksInInventoryException {
   }

   public void decreaseBatch(short var1, int var2, IntOpenHashSet var3) {
   }

   public boolean isInfinite() {
      return true;
   }

   public void handleReceived(InventoryMultMod var1, NetworkInventoryInterface var2) {
   }

   public void inc(int var1, short var2, int var3) {
      if (this.fillMode) {
         super.inc(var1, var2, var3);
      }

   }

   public int incExisting(short var1, int var2) throws NoSlotFreeException {
      return this.fillMode ? super.incExisting(var1, var2) : -1;
   }

   public int incExistingAndSend(short var1, int var2, NetworkInventoryInterface var3) throws NoSlotFreeException {
      return this.fillMode ? super.incExistingAndSend(var1, var2, var3) : -1;
   }

   public int incExistingOrNextFreeSlot(short var1, int var2) {
      return this.fillMode ? super.incExistingOrNextFreeSlot(var1, var2) : -1;
   }

   public int incExistingOrNextFreeSlotWithoutException(short var1, int var2) {
      return this.fillMode ? super.incExistingOrNextFreeSlotWithoutException(var1, var2) : -1;
   }

   public int incExistingOrNextFreeSlot(short var1, int var2, int var3) {
      return this.fillMode ? super.incExistingOrNextFreeSlot(var1, var2, var3) : -1;
   }

   public int incExistingOrNextFreeSlotWithoutException(short var1, int var2, int var3) {
      return this.fillMode ? super.incExistingOrNextFreeSlotWithoutException(var1, var2, var3) : -1;
   }

   public int putNextFreeSlot(short var1, int var2, int var3, int var4) {
      return this.fillMode ? super.putNextFreeSlot(var1, var2, var3, var4) : -1;
   }

   public int putNextFreeSlotWithoutException(short var1, int var2, int var3) {
      return this.fillMode ? super.putNextFreeSlotWithoutException(var1, var2, var3) : -1;
   }

   public int putNextFreeSlotWithoutException(short var1, int var2, int var3, int var4) {
      return this.fillMode ? super.putNextFreeSlotWithoutException(var1, var2, var3, var4) : -1;
   }

   public int putNextFreeSlot(short var1, int var2, int var3) {
      return this.fillMode ? super.putNextFreeSlot(var1, var2, var3) : -1;
   }

   public void sendAll() {
   }

   public void serialize(DataOutput var1) throws IOException {
   }

   public void doSwitchSlotsOrCombine(int var1, int var2, int var3, Inventory var4, int var5, Object2ObjectOpenHashMap var6) throws InventoryExceededException {
      super.doSwitchSlotsOrCombine(var1, var2, var3, var4, var5, var6);
   }

   public void splitUpMulti(int var1) {
   }

   public void removeMetaItem(MetaObject var1) {
   }

   public boolean conatainsMetaItem(MetaObject var1) {
      return false;
   }
}
