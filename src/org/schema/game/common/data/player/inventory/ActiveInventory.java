package org.schema.game.common.data.player.inventory;

import org.schema.schine.graphicsengine.core.Timer;

public abstract class ActiveInventory extends Inventory {
   private boolean activate;

   public ActiveInventory(InventoryHolder var1, long var2) {
      super(var1, var2);
   }

   public void activate() {
      this.activate = true;
   }

   public void deactivate() {
      this.activate = false;
   }

   public boolean isActivated() {
      return this.activate;
   }

   public String toString() {
      return "ActiveInventory: " + this.inventoryMap.toString();
   }

   public abstract void updateLocal(Timer var1);
}
