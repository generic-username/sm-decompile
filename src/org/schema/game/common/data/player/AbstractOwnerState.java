package org.schema.game.common.data.player;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.Short2IntOpenHashMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.controller.elements.FactoryAddOn;
import org.schema.game.common.controller.elements.InventoryMap;
import org.schema.game.common.controller.elements.cargo.CargoElementManager;
import org.schema.game.common.controller.elements.factory.FactoryProducerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.MetaObjectState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.creature.AIPlayer;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FixedRecipe;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.RecipeInterface;
import org.schema.game.common.data.element.meta.weapon.Weapon;
import org.schema.game.common.data.player.faction.FactionInterface;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventoryClientAction;
import org.schema.game.common.data.player.inventory.InventoryExceededException;
import org.schema.game.common.data.player.inventory.InventoryHolder;
import org.schema.game.common.data.player.inventory.InventoryMultMod;
import org.schema.game.common.data.player.inventory.InventorySlotRemoveMod;
import org.schema.game.common.data.player.inventory.NetworkInventoryInterface;
import org.schema.game.common.data.player.inventory.PersonalFactoryInventory;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.remote.RemoteInventoryClientAction;
import org.schema.game.network.objects.remote.RemoteInventoryMultMod;
import org.schema.game.network.objects.remote.RemoteInventorySlotRemove;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.TopLevelType;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.server.ServerMessage;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public abstract class AbstractOwnerState implements Damager, FactoryProducerInterface, FactionInterface, InventoryHolder, TagSerializable {
   public static final int NORM_INV = 0;
   public static final int CAPSULE_INV = 1;
   public static final int MICRO_INV = 2;
   public static final int MACRO_BLOCK_INV = 3;
   public static final int NORM_FORCE = -1;
   public static final long FACTORY_TIME = 10000L;
   public final Vector3i sittingPos = new Vector3i();
   public final Vector3i sittingPosTo = new Vector3i();
   public final Vector3i sittingPosLegs = new Vector3i();
   private final ArrayList ntInventoryMultMods = new ArrayList();
   public String sittingOnLoadedUID;
   public int sittingOnId = -1;
   public PlayerState conversationPartner;
   protected Inventory inventory;
   protected Inventory creativeInventory;
   protected Inventory virtualCreativeInventory;
   protected PersonalFactoryInventory personalFactoryInventoryCapsule;
   protected PersonalFactoryInventory personalFactoryInventoryMicro;
   protected PersonalFactoryInventory personalFactoryInventoryMacroBlock;
   protected String conversationScript;
   protected long sittingStarted;
   protected String sittingUIDServer;
   protected String sittingUIDServerInitial = "none";
   private ObjectArrayFIFOQueue clientInventoryActions = new ObjectArrayFIFOQueue();
   private Short2IntOpenHashMap delayedInventoryMods = new Short2IntOpenHashMap();
   private boolean writtenForUnload;
   private RecipeInterface currentRecipe;
   private Vector4f tint = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   private final Object2ObjectOpenHashMap changedSet = new Object2ObjectOpenHashMap();
   private long lastFactoryTime;
   private final ObjectArrayFIFOQueue ntInventorySlotRemoveMods = new ObjectArrayFIFOQueue();
   private final InventoryMap invMap = new InventoryMap();
   private long lastLagSent;
   public List activeManualTrades = new ObjectArrayList();

   public int getSelectedBuildSlot() {
      return this.getNetworkObject().getBuildSlot().getByte();
   }

   public Inventory getPersonalInventory() {
      return this.inventory;
   }

   public void setSelectedBuildSlot(int var1) {
      this.getNetworkObject().getBuildSlot().set((byte)Math.min(this.getInventory().getActiveSlotsMax(), Math.max(0, var1)), true);
   }

   public boolean isCreativeModeEnabled() {
      return false;
   }

   public boolean isPrivateNetworkObject() {
      return false;
   }

   public abstract void damage(float var1, Destroyable var2, Damager var3);

   public abstract void heal(float var1, Destroyable var2, Damager var3);

   public abstract float getMaxHealth();

   public abstract byte getFactionRights();

   public abstract float getHealth();

   public abstract Vector3f getRight(Vector3f var1);

   public abstract Vector3f getUp(Vector3f var1);

   public abstract Vector3f getForward(Vector3f var1);

   public abstract boolean isInvisibilityMode();

   public abstract boolean isOnServer();

   public abstract boolean isFactoryInUse();

   public abstract NetworkPlayerInterface getNetworkObject();

   public PlayerState getConversationPartner() {
      return this.conversationPartner;
   }

   public SimpleTransformableSendableObject getShootingEntity() {
      return null;
   }

   public void handleDelayedModifications() {
      if (!this.delayedInventoryMods.isEmpty()) {
         synchronized(this.delayedInventoryMods) {
            IntOpenHashSet var2 = new IntOpenHashSet();
            Iterator var3 = this.delayedInventoryMods.keySet().iterator();

            while(var3.hasNext()) {
               short var4 = (Short)var3.next();
               int var6 = this.getInventory().incExistingOrNextFreeSlot(var4, this.delayedInventoryMods.get(var4));
               var2.add(var6);
            }

            this.sendInventoryModification(var2, Long.MIN_VALUE);
            this.delayedInventoryMods.clear();
         }
      }
   }

   public abstract boolean isHarvestingButton();

   protected abstract void onNoSlotFree(short var1, int var2);

   public void modDelayPersonalInventory(short var1, int var2) {
      int var3 = this.delayedInventoryMods.containsKey(var1) ? this.delayedInventoryMods.get(var1) : 0;
      this.delayedInventoryMods.put(var1, var3 + var2);
   }

   public Vector4f getTint() {
      return this.tint;
   }

   public void sendHitConfirm(byte var1) {
   }

   public boolean isSegmentController() {
      return false;
   }

   public AbstractOwnerState getOwnerState() {
      return this;
   }

   public abstract boolean isVulnerable();

   public void updateLocal(Timer var1) {
      this.manufactureStep(var1);
      if (!this.clientInventoryActions.isEmpty()) {
         assert this.isOnServer();

         Object2ObjectOpenHashMap var8 = new Object2ObjectOpenHashMap();
         synchronized(this.clientInventoryActions) {
            while(!this.clientInventoryActions.isEmpty()) {
               try {
                  InventoryClientAction var3 = (InventoryClientAction)this.clientInventoryActions.dequeue();

                  assert var3.ownInventoryOwnerId == this.getId();

                  Sendable var4;
                  if ((var4 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var3.otherInventoryOwnerId)) != null) {
                     Object var15;
                     if (var4 instanceof ManagedSegmentController) {
                        var15 = ((ManagedSegmentController)var4).getManagerContainer();
                     } else {
                        var15 = (InventoryHolder)var4;
                     }

                     Inventory var5;
                     if ((var5 = this.getInventory(var3.ownInventoryPosId)) != null) {
                        System.err.println("[SERVER] execute action: " + var3);
                        var5.doSwitchSlotsOrCombine(var3.slot, var3.otherSlot, var3.subSlot, ((InventoryHolder)var15).getInventory(var3.otherInventoryPosId), var3.count, var8);
                     } else {
                        assert false;
                     }
                  } else {
                     assert false;
                  }
               } catch (InventoryExceededException var6) {
                  var6.printStackTrace();
               }
            }
         }

         if (!var8.isEmpty()) {
            Iterator var2 = var8.entrySet().iterator();

            while(var2.hasNext()) {
               Entry var13;
               ((Inventory)(var13 = (Entry)var2.next()).getKey()).sendInventoryModification((IntCollection)var13.getValue());
            }
         }
      }

      if (this.isOnServer()) {
         if (!this.sittingUIDServerInitial.equals("none")) {
            Object var9;
            if ((var9 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getUidObjectMap().get(this.sittingUIDServerInitial)) == null && this instanceof AIPlayer) {
               var9 = ((AIPlayer)this).getCreature().getAffinity();
            }

            if (this.getAbstractCharacterObject() != null && var9 != null && var9 instanceof SegmentController) {
               this.sittingUIDServerInitial = "none";
               SegmentController var10 = (SegmentController)var9;
               System.err.println("[SERVER] read sitting from tag. found UID: " + var9 + "; " + this.sittingPos + "; " + this.sittingPosTo + "; " + this.sittingPosLegs);
               if (this.getAbstractCharacterObject().getGravity().source == null || this.getAbstractCharacterObject().getGravity().source != var10) {
                  this.getAbstractCharacterObject().scheduleGravity(new Vector3f(0.0F, 0.0F, 0.0F), var10);
               }

               this.sitDown(var10, this.sittingPos, this.sittingPosTo, this.sittingPosLegs);
            }
         }

         if (this.sittingOnId >= 0) {
            Sendable var11 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(this.sittingOnId);
            boolean var12 = false;
            if (var11 != null && var11 instanceof SegmentController) {
               SegmentController var14;
               if ((var14 = (SegmentController)var11).isHidden()) {
                  System.err.println("[AbstractOwner] " + this.getState() + " " + this + " Character sitting but hidden");
               } else {
                  label178: {
                     boolean var10000;
                     SegmentPiece var16;
                     if ((var16 = var14.getSegmentBuffer().getPointUnsave(this.sittingPos)) == null) {
                        var10000 = true;
                     } else if (var16.getType() == 0) {
                        Vector3i var18;
                        (var18 = new Vector3i()).sub(this.sittingPosLegs, this.sittingPosTo);
                        var18.add(this.sittingPos);
                        SegmentPiece var17;
                        if ((var17 = var14.getSegmentBuffer().getPointUnsave(var18)) != null && var17.getType() == 0) {
                           System.err.println("[AbstractOwner] " + this.getState() + " Character sitting pos invalid");
                           break label178;
                        }

                        var10000 = true;
                     } else {
                        var10000 = ElementKeyMap.isValidType(var16.getType()) && ElementKeyMap.getInfo(var16.getType()).getBlockStyle() == BlockStyle.WEDGE;
                     }

                     var12 = var10000;
                  }
               }

               this.sittingUIDServer = var14.getUniqueIdentifier();
            }

            if (System.currentTimeMillis() - this.sittingStarted > 5000L && (this.getAbstractCharacterObject() == null || this.getAbstractCharacterObject().getGravity().source == null || this.getAbstractCharacterObject().getGravity().source.getId() != this.sittingOnId)) {
               if (this.getAbstractCharacterObject() == null) {
                  System.err.println("[AbstractOwner] " + this.getState() + " " + this + " Character has to stand up: No character assigned");
               } else if (this.getAbstractCharacterObject().getGravity().source == null) {
                  System.err.println("[AbstractOwner] " + this.getState() + " " + this + " Character has to stand up: No gravity source");
               } else {
                  System.err.println("[AbstractOwner] " + this.getState() + " " + this + " Character has to stand up: gravity source id doesnt match sitting id");
               }

               var12 = false;
            }

            if (!var12) {
               if (this instanceof PlayerState) {
                  ((PlayerState)this).sendServerMessage(new ServerMessage(new Object[]{237}, 3, this.getId()));
               }

               System.err.println("[AbstractOwner] " + this.getState() + " Character cannot longer sit there");
               this.sittingOnId = -1;
               this.sittingUIDServer = "none";
            }

            return;
         }

         this.sittingUIDServer = "none";
      }

   }

   public void standUp() {
      this.sittingOnId = -1;
      this.sittingStarted = 0L;
      this.sittingUIDServer = "none";
   }

   public void sitDown(SegmentController var1, Vector3i var2, Vector3i var3, Vector3i var4) {
      this.sittingOnId = var1.getId();
      this.sittingPos.set(var2);
      this.sittingPosTo.set(var3);
      this.sittingPosLegs.set(var4);
      this.sittingStarted = System.currentTimeMillis();
   }

   public void fromSittingTag(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.sittingPos.set((Vector3i)var2[0].getValue());
      this.sittingPosTo.set((Vector3i)var2[1].getValue());
      this.sittingPosLegs.set((Vector3i)var2[2].getValue());
      this.sittingUIDServerInitial = (String)var2[3].getValue();
   }

   public Tag toSittingTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.VECTOR3i, (String)null, this.sittingPos), new Tag(Tag.Type.VECTOR3i, (String)null, this.sittingPosTo), new Tag(Tag.Type.VECTOR3i, (String)null, this.sittingPosLegs), new Tag(Tag.Type.STRING, (String)null, this.sittingUIDServer), FinishTag.INST});
   }

   public String toString() {
      return "AbstractState(" + this.getName() + ", " + this.getId() + ")";
   }

   public abstract AbstractCharacter getAbstractCharacterObject();

   private void manufactureStep(Timer var1) {
      long var2;
      if (this.isOnServer() && this.isFactoryInUse() && (var2 = this.getState().getController().getServerRunningTime() / 10000L) > this.lastFactoryTime) {
         this.changedSet.clear();
         this.produce(this.getPersonalFactoryInventoryCapsule(), this.changedSet);
         this.produce(this.getPersonalFactoryInventoryMicro(), this.changedSet);
         this.produce(this.getPersonalFactoryInventoryMacroBlock(), this.changedSet);
         Iterator var6 = this.changedSet.entrySet().iterator();

         while(var6.hasNext()) {
            Entry var4;
            if (!((IntOpenHashSet)(var4 = (Entry)var6.next()).getValue()).isEmpty()) {
               IntOpenHashSet var5;
               (var5 = new IntOpenHashSet()).addAll((IntCollection)var4.getValue());
               ((Inventory)var4.getKey()).sendInventoryModification(var5);
               ((IntOpenHashSet)var4.getValue()).clear();
            }
         }

         this.lastFactoryTime = var2;
      }

   }

   private void produce(PersonalFactoryInventory var1, Object2ObjectOpenHashMap var2) {
      assert this.isOnServer();

      if (var1.getFactoryType() == 213) {
         this.setCurrentRecipe(ElementKeyMap.personalCapsuleRecipe);
      } else if (var1.getFactoryType() == 215) {
         this.setCurrentRecipe(ElementKeyMap.microAssemblerRecipe);
      } else if (ElementKeyMap.isMacroFactory(var1.getFactoryType())) {
         this.setCurrentRecipe(ElementKeyMap.macroBlockRecipe);
      } else {
         assert false;

         this.setCurrentRecipe((FixedRecipe)null);
      }

      if (this.getCurrentRecipe() != null) {
         int var3 = FactoryAddOn.getProductCount(this.getCurrentRecipe());

         for(int var4 = 0; var4 < var3; ++var4) {
            IntOpenHashSet var5;
            if ((var5 = (IntOpenHashSet)var2.get(var1)) == null) {
               var5 = new IntOpenHashSet();
               var2.put(var1, var5);
            }

            FactoryAddOn.produce(this.getCurrentRecipe(), var4, var1, this, var5, (GameServerState)this.getState());
         }
      }

   }

   public InventoryMap getInventories() {
      this.invMap.put(0L, this.getInventory());
      return this.invMap;
   }

   public Inventory getInventory(Vector3i var1) {
      return var1 == null ? this.getInventory(0L) : this.getInventory((long)var1.z);
   }

   public Inventory getInventory(long var1) {
      if (var1 != Long.MIN_VALUE && var1 != 0L) {
         if (var1 == -1L) {
            return this.getPersonalInventory();
         } else if (var1 == 1L) {
            return this.getPersonalFactoryInventoryCapsule();
         } else if (var1 == 3L) {
            return this.getPersonalFactoryInventoryMacroBlock();
         } else {
            assert var1 == 2L : var1;

            return this.getPersonalFactoryInventoryMicro();
         }
      } else {
         return this.getInventory();
      }
   }

   public boolean isInfiniteInventoryVolume() {
      return false;
   }

   public double getCapacityFor(Inventory var1) {
      if (var1 == this.getInventory()) {
         return this.isInfiniteInventoryVolume() ? 9.9999999999999E13D : CargoElementManager.PERSONAL_INVENTORY_BASE_CAPACITY;
      } else if (var1 == this.getPersonalFactoryInventoryCapsule()) {
         return CargoElementManager.PERSONAL_FACTORY_BASE_CAPACITY;
      } else if (var1 == this.getPersonalFactoryInventoryMacroBlock()) {
         return CargoElementManager.PERSONAL_FACTORY_BASE_CAPACITY;
      } else {
         return var1 == this.getPersonalFactoryInventoryMicro() ? CargoElementManager.PERSONAL_FACTORY_BASE_CAPACITY : 0.0D;
      }
   }

   public void volumeChanged(double var1, double var3) {
   }

   public NetworkInventoryInterface getInventoryNetworkObject() {
      return this.getNetworkObject();
   }

   public void sendInventoryErrorMessage(Object[] var1, Inventory var2) {
   }

   public String printInventories() {
      return this.getInventory().toString();
   }

   public void sendInventoryModification(IntCollection var1, long var2) {
      Inventory var4 = this.getInventory(var2);
      InventoryMultMod var5 = new InventoryMultMod(var1, var4, var2);
      this.getNetworkObject().getInventoryMultModBuffer().add(new RemoteInventoryMultMod(var5, this.isOnServer()));
   }

   public void sendInventoryModification(int var1, long var2) {
      IntArrayList var4;
      (var4 = new IntArrayList(1)).add(var1);
      this.sendInventoryModification(var4, var2);
   }

   public abstract int getId();

   public Inventory getInventory() {
      return this.inventory;
   }

   public void setInventory(Inventory var1) {
      this.inventory = var1;
   }

   public abstract void onFiredWeapon(Weapon var1);

   public void handleInventoryNT() {
      ObjectArrayList var1 = this.getInventoryNetworkObject().getInventoryMultModBuffer().getReceiveBuffer();

      int var2;
      for(var2 = 0; var2 < var1.size(); ++var2) {
         RemoteInventoryMultMod var3 = (RemoteInventoryMultMod)var1.get(var2);
         synchronized(this.ntInventoryMultMods) {
            this.ntInventoryMultMods.add(var3.get());
         }
      }

      var1 = this.getInventoryNetworkObject().getInventorySlotRemoveRequestBuffer().getReceiveBuffer();

      for(var2 = 0; var2 < var1.size(); ++var2) {
         InventorySlotRemoveMod var9 = (InventorySlotRemoveMod)((RemoteInventorySlotRemove)var1.get(var2)).get();
         synchronized(this.ntInventorySlotRemoveMods) {
            this.ntInventorySlotRemoveMods.enqueue(var9);
         }
      }

      if (this.getInventoryNetworkObject().getInventoryClientActionBuffer().getReceiveBuffer().size() > 0) {
         Iterator var8 = this.getInventoryNetworkObject().getInventoryClientActionBuffer().getReceiveBuffer().iterator();

         while(var8.hasNext()) {
            InventoryClientAction var10 = (InventoryClientAction)((RemoteInventoryClientAction)var8.next()).get();
            synchronized(this.clientInventoryActions) {
               this.clientInventoryActions.enqueue(var10);
            }
         }
      }

   }

   public void updateInventory() {
      if (this.isOnServer()) {
         this.handleDelayedModifications();
      } else {
         this.getInventory().clientUpdate();
         this.getPersonalFactoryInventoryCapsule().clientUpdate();
         this.getPersonalFactoryInventoryMicro().clientUpdate();
         this.getPersonalFactoryInventoryMacroBlock().clientUpdate();
      }

      if (!this.ntInventoryMultMods.isEmpty()) {
         synchronized(this.ntInventoryMultMods) {
            ArrayList var2 = new ArrayList();

            while(true) {
               while(!this.ntInventoryMultMods.isEmpty()) {
                  InventoryMultMod var3;
                  long var4 = (var3 = (InventoryMultMod)this.ntInventoryMultMods.remove(0)).parameter;
                  if (var3.parameter != Long.MIN_VALUE && var4 != 0L) {
                     if (var4 == -1L) {
                        this.getPersonalInventory().handleReceived(var3, this.getInventoryNetworkObject());
                     } else if (var4 == 1L) {
                        this.getPersonalFactoryInventoryCapsule().handleReceived(var3, this.getInventoryNetworkObject());
                     } else if (var4 == 2L) {
                        this.getPersonalFactoryInventoryMicro().handleReceived(var3, this.getInventoryNetworkObject());
                     } else if (var4 == 3L) {
                        this.getPersonalFactoryInventoryMacroBlock().handleReceived(var3, this.getInventoryNetworkObject());
                     }
                  } else {
                     this.getInventory().handleReceived(var3, this.getInventoryNetworkObject());
                  }
               }

               if (!var2.isEmpty()) {
                  this.ntInventoryMultMods.addAll(var2);
               }
               break;
            }
         }
      }

      if (!this.ntInventorySlotRemoveMods.isEmpty()) {
         synchronized(this.ntInventorySlotRemoveMods) {
            while(!this.ntInventorySlotRemoveMods.isEmpty()) {
               InventorySlotRemoveMod var8 = (InventorySlotRemoveMod)this.ntInventorySlotRemoveMods.dequeue();
               Inventory var9;
               if ((var9 = this.getInventory(var8.parameter)) != null) {
                  boolean var10 = this.isOnServer();
                  var9.removeSlot(var8.slot, var10);
               }
            }

         }
      }
   }

   public void updateToFullNetworkObject() {
      this.getInventory().sendAll();
      this.getPersonalFactoryInventoryCapsule().sendAll();
      this.getPersonalFactoryInventoryMicro().sendAll();
      this.getPersonalFactoryInventoryMacroBlock().sendAll();
   }

   public abstract String getConversationScript();

   public void setConversationScript(String var1) {
      this.conversationScript = var1;
   }

   public boolean isWrittenForUnload() {
      return this.writtenForUnload;
   }

   public void setWrittenForUnload(boolean var1) {
      this.writtenForUnload = var1;
   }

   public PersonalFactoryInventory getPersonalFactoryInventoryCapsule() {
      return this.personalFactoryInventoryCapsule;
   }

   public void setPersonalFactoryInventoryCapsule(PersonalFactoryInventory var1) {
      this.personalFactoryInventoryCapsule = var1;
   }

   public PersonalFactoryInventory getPersonalFactoryInventoryMicro() {
      return this.personalFactoryInventoryMicro;
   }

   public void setPersonalFactoryInventoryMicro(PersonalFactoryInventory var1) {
      this.personalFactoryInventoryMicro = var1;
   }

   public RecipeInterface getCurrentRecipe() {
      return this.currentRecipe;
   }

   public int getFactoryCapability() {
      return 1;
   }

   public void setCurrentRecipe(FixedRecipe var1) {
      this.currentRecipe = var1;
   }

   public PersonalFactoryInventory getPersonalFactoryInventoryMacroBlock() {
      return this.personalFactoryInventoryMacroBlock;
   }

   public void setPersonalFactoryInventoryMacroBlock(PersonalFactoryInventory var1) {
      this.personalFactoryInventoryMacroBlock = var1;
   }

   public void sendInventorySlotRemove(int var1, long var2) {
      if (this.getInventory(var2) != null) {
         this.getNetworkObject().getInventorySlotRemoveRequestBuffer().add(new RemoteInventorySlotRemove(new InventorySlotRemoveMod(var1, var2), this.isOnServer()));
      } else {
         try {
            throw new IllegalArgumentException("[INVENTORY] Exception: tried to send inventory " + var2);
         } catch (Exception var4) {
            var4.printStackTrace();
         }
      }
   }

   public boolean isSitting() {
      return this.sittingOnId >= 0;
   }

   public void announceLag(long var1) {
      if (System.currentTimeMillis() - this.lastLagSent > 1000L) {
         assert this.getState().isSynched();

         this.getNetworkObject().getLagAnnouncement().add(var1);
         this.lastLagSent = System.currentTimeMillis();
      }

   }

   public void sendClientMessage(String var1, int var2) {
      if (!this.isOnServer()) {
         switch(var2) {
         case 1:
            ((GameClientState)this.getState()).getController().popupInfoTextMessage(var1, 0.0F);
            return;
         default:
            ((GameClientState)this.getState()).getController().popupAlertTextMessage(var1, 0.0F);
         }
      }

   }

   public float getDamageGivenMultiplier() {
      return 1.0F;
   }

   public TopLevelType getTopLevelType() {
      return TopLevelType.PLAYER;
   }

   public boolean isControllingCore() {
      return false;
   }

   public InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3) {
      MetaObject var5;
      if ((var5 = ((MetaObjectState)this.getState()).getMetaObjectManager().getObject((int)var1)) instanceof Weapon) {
         InterEffectSet var4 = ((Weapon)var5).getEffectSet();

         assert var4 != null : var1 + "; " + var5;

         return var4;
      } else {
         assert false : "WEP: " + var1;

         return null;
      }
   }

   public MetaWeaponEffectInterface getMetaWeaponEffect(long var1, DamageDealerType var3) {
      MetaObject var4;
      return (var4 = ((MetaObjectState)this.getState()).getMetaObjectManager().getObject((int)var1)) instanceof Weapon ? ((Weapon)var4).getMetaWeaponEffect() : null;
   }

   public abstract long getDbId();

   public boolean isSpawnProtected() {
      return false;
   }
}
