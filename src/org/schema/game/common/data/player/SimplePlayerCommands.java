package org.schema.game.common.data.player;

import java.util.Arrays;

public enum SimplePlayerCommands {
   SEARCH_LAST_ENTERED_SHIP(new Class[0]),
   PUT_ON_HELMET(new Class[0]),
   HIRE_CREW(new Class[0]),
   REPAIR_STATION(new Class[]{Integer.class, String.class}),
   SIT_DOWN(new Class[]{Integer.class, Long.class, Long.class, Long.class}),
   SCAN(new Class[]{Integer.class}),
   WARP_TO_TUTORIAL_SECTOR(new Class[0]),
   DESTROY_TUTORIAL_ENTITY(new Class[]{String.class}),
   BACKUP_INVENTORY(new Class[]{Boolean.class}),
   RESTORE_INVENTORY(new Class[0]),
   END_TUTORIAL(new Class[0]),
   END_SHIPYARD_TEST(new Class[0]),
   ADD_BLUEPRINT_META_SINGLE(new Class[]{Integer.class, Short.class, Integer.class}),
   ADD_BLUEPRINT_META_ALL(new Class[]{Integer.class}),
   SPAWN_BLUEPRINT_META(new Class[]{Integer.class, String.class, Integer.class, Integer.class, Integer.class, Integer.class, Boolean.class, Integer.class, Long.class}),
   REQUEST_BLUEPRINT_ITEM_LIST(new Class[]{String.class}),
   SET_FACTION_RANK_ON_OBJ(new Class[]{Integer.class, Byte.class}),
   FAILED_TO_JOIN_CHAT_INVALLID_PASSWD(new Class[]{String.class}),
   REBOOT_STRUCTURE(new Class[]{Integer.class, Boolean.class}),
   REPAIR_ARMOR(new Class[]{Integer.class, Boolean.class}),
   REBOOT_STRUCTURE_REQUEST_TIME(new Class[]{Integer.class}),
   SPAWN_SHOPKEEP(new Class[]{Integer.class}),
   SEND_ALL_DESTINATIONS_OF_ENTITY(new Class[]{Integer.class}),
   SET_SPAWN(new Class[]{Integer.class, Long.class}),
   GET_BLOCK_STORAGE_META_SINGLE(new Class[]{Integer.class, Short.class, Integer.class, Integer.class, Integer.class, Integer.class, Integer.class}),
   ADD_BLOCK_STORAGE_META_SINGLE(new Class[]{Integer.class, Short.class, Integer.class, Integer.class, Integer.class, Integer.class, Integer.class}),
   GET_BLOCK_STORAGE_META_ALL(new Class[]{Integer.class, Integer.class, Integer.class, Integer.class, Integer.class}),
   ADD_BLOCK_STORAGE_META_ALL(new Class[]{Integer.class, Integer.class, Integer.class, Integer.class, Integer.class}),
   CLIENT_TO_SERVER_LOG(new Class[]{String.class}),
   VERIFY_FACTION_ID(new Class[]{Integer.class}),
   SET_FREE_WARP_TARGET(new Class[]{Integer.class, Long.class, Integer.class, Integer.class, Integer.class});

   public final Class[] args;

   private SimplePlayerCommands(Class... var3) {
      this.args = var3;
   }

   public final void checkMatches(Object[] var1) {
      if (this.args.length != var1.length) {
         throw new IllegalArgumentException("Invalid argument count: Provided: " + Arrays.toString(var1) + ", but needs: " + Arrays.toString(this.args));
      } else {
         for(int var2 = 0; var2 < this.args.length; ++var2) {
            if (!var1[var2].getClass().equals(this.args[var2])) {
               System.err.println("Not Equal: " + var1[var2] + " and " + this.args[var2]);
               throw new IllegalArgumentException("Invalid argument on index " + var2 + ": Provided: " + Arrays.toString(var1) + "; cannot take " + var1[var2] + ":" + var1[var2].getClass() + ", it has to be type: " + this.args[var2].getClass());
            }
         }

      }
   }
}
