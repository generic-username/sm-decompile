package org.schema.game.common.data.player;

public class DefferedDownload {
   private final String downloadName;
   private final long timeDeffered;
   private final int delay;

   public DefferedDownload(String var1, long var2, int var4) {
      this.downloadName = var1;
      this.timeDeffered = var2;
      this.delay = var4;
   }

   public String getDownloadName() {
      return this.downloadName;
   }

   public boolean timeUp(long var1) {
      return var1 - this.timeDeffered > (long)this.delay;
   }
}
