package org.schema.game.common.data.player;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.navigation.NavigationControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.Starter;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.ControllerUnitRequest;
import org.schema.game.network.objects.NetworkPlayer;
import org.schema.game.network.objects.remote.RemoteControllerUnitRequest;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.server.ServerMessage;
import org.schema.schine.sound.AudioEntity;

public class ControllerState {
   public static final long DELAY_ORIENTATION_MS = 500L;
   public final ObjectArrayFIFOQueue requests = new ObjectArrayFIFOQueue();
   private final Set units = new HashSet();
   private final Set unitsToRemoveTmp = new HashSet();
   private final PlayerState owner;
   ControllerStateUnit controllerStateUnitTmp = new ControllerStateUnit();
   private boolean suspended;
   private Transform lastTransform;
   private long lastDeferralLog;
   private int lastDeferralsAttempts;

   public ControllerState(PlayerState var1) {
      this.owner = var1;
   }

   private void addUnit(PlayerState var1, PlayerControllable var2, Vector3i var3, Sendable var4, Vector3i var5) {
      ControllerStateUnit var6;
      (var6 = new ControllerStateUnit()).playerControllable = var2;
      var6.parameter = var3;
      var6.playerState = var1;
      boolean var7 = this.units.add(var6);
      if (!var2.getAttachedPlayers().contains(var1)) {
         var2.getAttachedPlayers().add(var1);
         if (var4 != null && var4 instanceof PlayerControllable) {
            ((PlayerControllable)var4).onPlayerDetachedFromThis(var1, var2);
         }

         var2.onAttachPlayer(var1, var4, var5, var3);
         if (!this.owner.isOnServer() && this.owner == ((GameClientState)this.owner.getState()).getPlayer() && var2 instanceof AudioEntity) {
            Controller.getAudioManager().switchSoundInside((AudioEntity)var2, ((AudioEntity)var2).getInsideSoundVolume(), ((AudioEntity)var2).getInsideSoundVolume());
         }
      }

      if (var7) {
         if (var2 instanceof Ship) {
            var1.setLastEnteredShip((Ship)var2);
         }

         System.err.println("[CONTROLLER][ADD-UNIT] (" + var1.getState() + "): " + var1 + " Added to controllers: " + var2);
         if (this.owner.isClientOwnPlayer()) {
            ((GameClientState)this.owner.getState()).getController().updateCurrentControlledEntity(this);
         }

         Starter.modManager.onPlayerChangedContol(var1, var2, var3, var4, var5);
      }

   }

   public void checkPlayerControllers() {
      assert this.owner.isOnServer();

      if (!ServerConfig.USE_STRUCTURE_HP.isOn()) {
         Iterator var1 = this.getUnits().iterator();

         while(true) {
            ControllerStateUnit var2;
            Vector3i var4;
            SegmentPiece var5;
            do {
               do {
                  do {
                     if (!var1.hasNext()) {
                        return;
                     }
                  } while(!((var2 = (ControllerStateUnit)var1.next()).playerControllable instanceof SegmentController));

                  SegmentController var3 = (SegmentController)var2.playerControllable;
                  var4 = (Vector3i)var2.parameter;
                  var5 = var3.getSegmentBuffer().getPointUnsave(var4);
                  System.err.println("PARAMETER " + var2.parameter + ": POINT " + var5);
               } while(var5 == null);
            } while(var5.isValid() && !var5.isDead());

            PlayerCharacter var6 = this.owner.getAssingedPlayerCharacter();
            System.err.println("FORCING PLAYER OUT OF SHIP " + this.owner + ": " + this + " from " + var2.playerControllable + ": " + var6);

            assert var6 != null;

            this.requestControl(var2.playerControllable, var6, var4, new Vector3i(), false);
         }
      }
   }

   public boolean controls(ControllerStateUnit var1) {
      return var1.playerState == this.owner && this.isControlling(var1);
   }

   public ControllerStateUnit getControllerByClass(Class var1, PlayerState var2) {
      Iterator var3 = this.units.iterator();

      ControllerStateUnit var4;
      do {
         if (!var3.hasNext()) {
            return null;
         }
      } while((var4 = (ControllerStateUnit)var3.next()).playerState != var2 || !var4.playerControllable.equals(var1));

      return var4;
   }

   public PlayerState getOwner() {
      return this.owner;
   }

   public Set getUnits() {
      return this.units;
   }

   public void handleControl(Timer var1, PlayerState var2) {
      Iterator var3 = this.units.iterator();

      while(var3.hasNext()) {
         ControllerStateUnit var4;
         if ((var4 = (ControllerStateUnit)var3.next()).playerState == var2) {
            if (!var2.isOnServer() && var4.playerControllable instanceof SimpleTransformableSendableObject && var4.playerState == ((GameClientState)var2.getState()).getPlayer()) {
               ((GameClientState)var2.getState()).setCurrentPlayerObject((SimpleTransformableSendableObject)var4.playerControllable);
            }

            assert var4.getPlayerState() != null;

            var4.playerControllable.handleControl(var1, var4);
         }
      }

   }

   public void handleMouseEvent(MouseEvent var1) {
      PlayerState var2 = this.owner;
      Iterator var3 = this.units.iterator();

      while(var3.hasNext()) {
         ControllerStateUnit var4;
         if ((var4 = (ControllerStateUnit)var3.next()).playerState == var2) {
            if (!var2.isOnServer() && var4.playerControllable instanceof SimpleTransformableSendableObject && var4.playerState == ((GameClientState)var2.getState()).getPlayer()) {
               ((GameClientState)var2.getState()).setCurrentPlayerObject((SimpleTransformableSendableObject)var4.playerControllable);
            }

            if (var4.isFlightControllerActive() && var4.isUnitInPlayerSector()) {
               var4.playerControllable.handleMouseEvent(var4, var1);
            }
         }
      }

   }

   public boolean isOnServer() {
      return this.owner.isOnServer();
   }

   public void handleKeyEvent(KeyboardMappings var1) {
      PlayerState var2 = this.owner;
      Iterator var3 = this.units.iterator();

      while(var3.hasNext()) {
         ControllerStateUnit var4;
         if ((var4 = (ControllerStateUnit)var3.next()).playerState == var2) {
            if (!var2.isOnServer() && var4.playerControllable instanceof SimpleTransformableSendableObject && var4.playerState == ((GameClientState)var2.getState()).getPlayer()) {
               ((GameClientState)var2.getState()).setCurrentPlayerObject((SimpleTransformableSendableObject)var4.playerControllable);
            }

            if (var4.isFlightControllerActive() && var4.isUnitInPlayerSector()) {
               var4.playerControllable.handleKeyEvent(var4, var1);
            }
         }
      }

   }

   public void handleControllerStateFromNT(NetworkPlayer var1) {
      for(int var2 = 0; var2 < var1.controlRequestParameterBuffer.getReceiveBuffer().size(); ++var2) {
         ControllerUnitRequest var3 = (ControllerUnitRequest)((RemoteControllerUnitRequest)var1.controlRequestParameterBuffer.getReceiveBuffer().get(var2)).get();
         System.err.println("[CONTROLLERSTATE] INCOMING REQUEST (NT) " + this.owner.getState() + "; " + this.getOwner() + " CONTROLLER REQUEST RECEIVED  " + var3);

         assert var3.fromParam != null;

         assert var3.toParam != null;

         this.requests.enqueue(var3);
      }

   }

   public boolean isAdminException() {
      boolean var1;
      if (var1 = ((GameServerState)this.owner.getState()).isAdmin(this.owner.getName()) && ServerConfig.ADMINS_CIRCUMVENT_STRUCTURE_CONTROL.isOn()) {
         this.owner.sendServerMessage(new ServerMessage(new Object[]{249}, 1, this.owner.getId()));
      }

      return var1;
   }

   public boolean isFactionException(SegmentController var1, Vector3i var2, int var3) {
      if (var2 != null && var1 != null) {
         if (var1 instanceof Ship && var2.equals(Ship.core)) {
            return false;
         }

         Vector3i var4 = new Vector3i();

         for(int var5 = 0; var5 < 6; ++var5) {
            var4.add(var2, Element.DIRECTIONSi[var5]);
            SegmentPiece var6;
            if ((var6 = var1.getSegmentBuffer().getPointUnsave(var4)) != null && var6.getType() == 346 || var6.getType() == 936 && var6.getSegmentController().getFactionId() == var3) {
               return true;
            }
         }
      }

      return false;
   }

   private boolean canHandleRequest(ControllerUnitRequest var1) {
      if (this.owner.isOnServer()) {
         return true;
      } else {
         Vector3i var10000 = var1.fromParam;
         var10000 = var1.toParam;
         boolean var3 = var1.hideExitedObject;
         this.owner.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var1.fromId);
         Sendable var2 = (Sendable)this.owner.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var1.toId);
         if (var1.toParam != null && var2 instanceof SegmentController) {
            return ((SegmentController)var2).getSegmentBuffer().getPointUnsave(var1.toParam) != null;
         } else {
            return true;
         }
      }
   }

   private void handleRequest(ControllerUnitRequest var1) {
      this.owner.inControlTransition = System.currentTimeMillis();

      assert var1.toParam != null;

      Vector3i var2 = var1.fromParam;
      Vector3i var3 = var1.toParam;
      boolean var4 = var1.hideExitedObject;
      Sendable var5 = (Sendable)this.owner.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var1.fromId);
      Object var10 = (Sendable)this.owner.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var1.toId);
      if (this.owner.isOnServer() && var10 != null && var10 instanceof PlayerControllable) {
         SegmentController var6;
         if (var10 instanceof SegmentController && (var6 = (SegmentController)var10).getFactionId() != 0 && ((FactionState)this.owner.getState()).getFactionManager().existsFaction(var6.getFactionId()) && var6.getFactionId() != this.owner.getFactionId() && !this.isFactionException(var6, var3, this.owner.getFactionId()) && !this.isAdminException()) {
            ((GameServerState)this.owner.getState()).getController().broadcastMessageAdmin(new Object[]{250, this.owner.getName(), var10}, 3);
            var10 = this.owner.getAssingedPlayerCharacter();
         }

         if (var10 instanceof PlayerControllable && ((PlayerControllable)var10).getAttachedPlayers().size() > 0) {
            Iterator var11 = ((PlayerControllable)var10).getAttachedPlayers().iterator();

            while(var11.hasNext()) {
               PlayerState var7;
               Iterator var8 = (var7 = (PlayerState)var11.next()).getControllerState().getUnits().iterator();

               while(var8.hasNext()) {
                  ControllerStateUnit var9;
                  if ((var9 = (ControllerStateUnit)var8.next()).parameter != null && var9.parameter.equals(var3)) {
                     if (var7 != this.owner) {
                        if (!this.isAdminException()) {
                           ((GameServerState)this.owner.getState()).getController().broadcastMessageAdmin(new Object[]{251, this.owner.getName(), var10, var9.playerState}, 3);
                           var10 = this.owner.getAssingedPlayerCharacter();
                           if (var5 == var10) {
                              return;
                           }
                        }
                     } else {
                        System.err.println("[SERVER] WARNING Duplicate entry: " + this.owner + " - " + this.getUnits() + "; request: " + var5 + "(" + var2 + ") -> " + var10 + "(" + var3 + ")");

                        assert false;
                     }
                  }
               }
            }
         }
      }

      this.detach(var5, var2, var4);
      if (var10 != null) {
         if (var10 instanceof SimpleTransformableSendableObject) {
            this.owner.setLastOrientation(((SimpleTransformableSendableObject)var10).getWorldTransform());
         }

         if (var10 instanceof PlayerControllable) {
            PlayerControllable var12 = (PlayerControllable)var10;
            this.addUnit(this.owner, var12, var3, var5, var2);
         }
      } else {
         this.units.clear();
      }

      if (this.owner.isOnServer()) {
         this.sendControlRequest((PlayerControllable)var5, (PlayerControllable)var10, var2, var3, var4);
      }

   }

   private void detach(Sendable var1, Vector3i var2, boolean var3) {
      if (var1 != null) {
         if (var1 instanceof PlayerControllable) {
            PlayerControllable var4 = (PlayerControllable)var1;
            this.removeUnit(var4, var2, this.owner, var3);
         }

         if (var1 instanceof EditableSendableSegmentController) {
            System.err.println("[CONTROLLERSTATE] CHECKING EE: " + this.owner + ", " + this.owner.getState());

            assert !NavigationControllerManager.isPlayerInCore(this.owner.getAssingedPlayerCharacter()) : "DETACH FROM: " + var1 + "; " + this.getUnits();
         }
      }

   }

   private void handleRequests() {
      if (!this.requests.isEmpty()) {
         while(!this.requests.isEmpty()) {
            if (!this.canHandleRequest((ControllerUnitRequest)this.requests.first())) {
               ++this.lastDeferralsAttempts;
               if (System.currentTimeMillis() - this.lastDeferralLog > 2000L) {
                  this.lastDeferralLog = System.currentTimeMillis();
                  System.err.println((this.isOnServer() ? "[SERVER]" : "[CLIENT]") + " DEFERRING CONTROLLER REQUEST SINCE BLOCK IS NOT LOADED: TOTAL ATTEMPTS: " + this.lastDeferralsAttempts + "; " + this.requests.first());
               }

               return;
            }

            this.lastDeferralsAttempts = 0;
            this.handleRequest((ControllerUnitRequest)this.requests.dequeue());
         }
      }

   }

   public boolean isControlling(ControllerStateUnit var1) {
      return this.units.contains(var1);
   }

   public boolean isControlling(PlayerState var1, PlayerControllable var2, Vector3i var3) {
      this.controllerStateUnitTmp.playerState = var1;
      this.controllerStateUnitTmp.playerControllable = var2;
      this.controllerStateUnitTmp.parameter = var3;
      return this.units.contains(this.controllerStateUnitTmp);
   }

   public boolean isOwnerControlling(PlayerControllable var1) {
      Iterator var2 = this.units.iterator();

      do {
         if (!var2.hasNext()) {
            return false;
         }
      } while(((ControllerStateUnit)var2.next()).playerControllable != var1);

      return true;
   }

   public boolean isOwnerControlling(PlayerControllable var1, Vector3i var2) {
      return this.isControlling(this.getOwner(), var1, var2);
   }

   public boolean isSuspended() {
      return this.suspended;
   }

   public void setSuspended(boolean var1) {
      this.suspended = var1;
   }

   public void onDestroyedElement(SegmentPiece var1) {
      if (var1.getSegmentController() instanceof PlayerControllable) {
         ControllerStateUnit var2;
         (var2 = new ControllerStateUnit()).playerControllable = (PlayerControllable)var1.getSegment().getSegmentController();
         var2.playerState = this.owner;
         var2.parameter = var1.getAbsolutePos(new Vector3i());
         if (this.controls(var2)) {
            System.err.println(var1.getSegmentController().getState() + " onDestroyedElement: Forcing " + this + " to leave a controllable element of " + var1.getSegment().getSegmentController() + ": " + var1);
            if (this.isOnServer()) {
               this.requestControlServerAndSend(var2.playerControllable, this.owner.getAssingedPlayerCharacter(), (Vector3i)var2.parameter, (Vector3i)null, false);
               return;
            }

            this.requestControl(var2.playerControllable, this.owner.getAssingedPlayerCharacter(), (Vector3i)var2.parameter, (Vector3i)null, false);
         }
      }

   }

   public void forcePlayerOutOfSegmentControllers() {
      Iterator var1 = this.units.iterator();

      while(var1.hasNext()) {
         ControllerStateUnit var2;
         if ((var2 = (ControllerStateUnit)var1.next()).playerControllable instanceof SegmentController) {
            System.err.println("[SERVER] forceOutOfSegmentController: Forcing " + this + " to leave a controllable element of " + var2.playerControllable);
            var2.playerState.sendServerMessage(new ServerMessage(new Object[]{252}, 3, var2.playerState.getId()));
            this.requestControlServerAndSend(var2.playerControllable, this.owner.getAssingedPlayerCharacter(), (Vector3i)var2.parameter, (Vector3i)null, false);
         }
      }

   }

   public void forcePlayerOutOfShips() {
      Iterator var1 = this.units.iterator();

      while(var1.hasNext()) {
         ControllerStateUnit var2;
         if ((var2 = (ControllerStateUnit)var1.next()).playerControllable instanceof Ship) {
            System.err.println("[SERVER] forceOutOfShip: Forcing " + this + " to leave a controllable element of " + var2.playerControllable);
            var2.playerState.sendServerMessage(new ServerMessage(new Object[]{253}, 3, var2.playerState.getId()));
            this.requestControlServerAndSend(var2.playerControllable, this.owner.getAssingedPlayerCharacter(), (Vector3i)var2.parameter, (Vector3i)null, false);
         }
      }

   }

   public void removeAllUnitsFromPlayer(PlayerState var1, boolean var2) {
      Iterator var3 = this.units.iterator();

      while(var3.hasNext()) {
         ControllerStateUnit var4;
         if ((var4 = (ControllerStateUnit)var3.next()).playerState == var1) {
            var3.remove();
            var4.playerControllable.getAttachedPlayers().remove(var1);
            var4.playerControllable.onDetachPlayer(var1, var2, (Vector3i)var4.parameter);
            if (var4.playerControllable instanceof Ship) {
               ((Ship)var4.playerControllable).setFlagNameChange(true);
            }

            if (!this.owner.isOnServer() && this.owner == ((GameClientState)this.owner.getState()).getPlayer() && var4.playerControllable instanceof AudioEntity) {
               Controller.getAudioManager().switchSoundInside((AudioEntity)var4.playerControllable, ((AudioEntity)var4.playerControllable).getInsideSoundVolume(), ((AudioEntity)var4.playerControllable).getInsideSoundPitch());
            }
         }
      }

      this.setLastTransform((Transform)null);
      System.err.println("[ControllerState] REMOVED ALL UNITS OF " + this.owner + " ON " + this.owner.getState() + "! LEFT " + this.units);
      System.err.println("[NOTIFIED] REMOVED ALL UNITS OF " + this.owner + " ON " + this.owner.getState() + "! LEFT " + this.units);
   }

   private Transform removeUnit(PlayerControllable var1, Vector3i var2, PlayerState var3, boolean var4) {
      this.controllerStateUnitTmp.playerControllable = var1;
      this.controllerStateUnitTmp.parameter = var2;
      this.controllerStateUnitTmp.playerState = var3;
      Transform var5;
      (var5 = new Transform()).set(((SimpleTransformableSendableObject)var1).getWorldTransform());
      if (var2 != null) {
         Vector3f var6 = new Vector3f((float)(var2.x - 16), (float)(var2.y - 16), (float)(var2.z - 16));
         var5.transform(var6);
         var5.origin.set(var6);
      }

      System.err.println("[CONTROLLERSTATE][REMOVE-UNIT] " + var3.getState() + "; REMOVING CONTROLLER UNIT FROM " + var3 + ": " + var1 + "; detach pos: " + var5.origin);
      var1.getAttachedPlayers().remove(var3);
      var1.onDetachPlayer(var3, var4, var2);
      if (!var3.isOnServer() && var3 == ((GameClientState)var3.getState()).getPlayer() && var1 instanceof AudioEntity) {
         Controller.getAudioManager().switchSoundOutside((AudioEntity)var1, 1.0F, 1.0F);
      }

      if (this.units.remove(this.controllerStateUnitTmp) && var3.isClientOwnPlayer()) {
         ((GameClientState)var3.getState()).getController().updateCurrentControlledEntity(this);
      }

      return var5;
   }

   public void requestControlServerAndSend(PlayerControllable var1, PlayerControllable var2, Vector3i var3, Vector3i var4, boolean var5) {
      assert this.owner.isOnServer();

      System.err.println("[SERVER]: " + this.owner + " request control: " + var1 + " -> " + var2 + " [[[FORCE TO CLIENT]]]");
      ControllerUnitRequest var6;
      (var6 = new ControllerUnitRequest()).setFrom(var1);
      var6.setTo(var2);
      var6.setFromParam(var3);
      var6.setToParam(var4 == null ? new Vector3i() : var4);
      var6.setHideExitedObject(var5);
      this.requests.enqueue(var6);
      this.sendControlRequest(var1, var2, var3, var4, var5);
   }

   public void requestControl(PlayerControllable var1, PlayerControllable var2, Vector3i var3, Vector3i var4, boolean var5) {
      if (this.owner.isOnServer()) {
         System.err.println("[SERVER]: " + this.owner + " request control: " + var1 + " -> " + var2);
         ControllerUnitRequest var7;
         (var7 = new ControllerUnitRequest()).setFrom(var1);
         var7.setTo(var2);
         var7.setFromParam(var3);
         var7.setToParam(var4 == null ? new Vector3i() : var4);
         var7.setHideExitedObject(var5);
         this.requests.enqueue(var7);
      } else {
         this.owner.inControlTransition = System.currentTimeMillis();
         if (var2 instanceof SimpleTransformableSendableObject) {
            this.owner.setLastOrientation(((SimpleTransformableSendableObject)var2).getWorldTransform());
         }

         SegmentPiece var6 = ((GameClientState)this.owner.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getEntered();
         System.err.println("[CLIENT]: " + this.owner + " request control: " + var1 + " -> " + var2 + ";  Entered: " + (var6 != null ? var6.getSegmentController() + "; " + var6 : "null"));
         this.sendControlRequest(var1, var2, var3, var4, var5);
      }
   }

   public void sendAll() {
      Iterator var1 = this.getUnits().iterator();

      while(var1.hasNext()) {
         ControllerStateUnit var2 = (ControllerStateUnit)var1.next();
         ControllerUnitRequest var3;
         (var3 = new ControllerUnitRequest()).setFrom((PlayerControllable)null);
         var3.setFromParam((Vector3i)null);
         var3.setTo(var2.playerControllable);
         var3.setToParam((Vector3i)var2.parameter);
         var3.setHideExitedObject(var2.playerControllable.isHidden());
         this.sendRequest(var3);
      }

   }

   public StateInterface getState() {
      return this.owner.getState();
   }

   public void sendRequest(ControllerUnitRequest var1) {
      this.owner.getNetworkObject().controlRequestParameterBuffer.add(new RemoteControllerUnitRequest(var1, this.owner.getNetworkObject()));
   }

   private void sendControlRequest(PlayerControllable var1, PlayerControllable var2, Vector3i var3, Vector3i var4, boolean var5) {
      ControllerUnitRequest var6;
      (var6 = new ControllerUnitRequest()).setFrom(var1);
      var6.setTo(var2);
      var6.setFromParam(var3);
      var6.setToParam(var4);
      var6.setHideExitedObject(var5);
      this.sendRequest(var6);
   }

   public void update(Timer var1) {
      Iterator var2 = this.units.iterator();
      if (this.units.size() > 0 && this.lastTransform == null) {
         this.lastTransform = new Transform();
         this.lastTransform.setIdentity();
      }

      while(var2.hasNext()) {
         ControllerStateUnit var3;
         if ((var3 = (ControllerStateUnit)var2.next()).playerControllable instanceof SimpleTransformableSendableObject) {
            this.lastTransform.set(((SimpleTransformableSendableObject)var3.playerControllable).getWorldTransform());
            if (var3.parameter != null && var3.parameter instanceof Vector3i) {
               Vector3f var4 = new Vector3f((float)(((Vector3i)var3.parameter).x - 16), (float)(((Vector3i)var3.parameter).y - 16), (float)(((Vector3i)var3.parameter).z - 16));
               this.lastTransform.transform(var4);
               this.lastTransform.origin.set(var4);
            }
         }

         if (!this.owner.getState().getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(var3.playerControllable.getId())) {
            var2.remove();
         }
      }

      this.handleRequests();
      if (!this.suspended) {
         this.handleControl(var1, this.getOwner());
      }

   }

   public Transform getLastTransform() {
      return this.lastTransform;
   }

   public void setLastTransform(Transform var1) {
      this.lastTransform = var1;
   }
}
