package org.schema.game.common.data.player;

public class AbstractHumanoidCharacter {
   private float speed = 4.0F;

   public float getSpeed() {
      return this.speed;
   }
}
