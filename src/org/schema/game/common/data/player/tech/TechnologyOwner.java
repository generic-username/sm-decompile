package org.schema.game.common.data.player.tech;

import org.schema.schine.network.StateInterface;

public interface TechnologyOwner {
   StateInterface getState();

   boolean isOnServer();
}
