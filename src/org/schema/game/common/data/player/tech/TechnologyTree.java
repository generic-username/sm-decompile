package org.schema.game.common.data.player.tech;

import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import org.schema.game.client.data.GameStateInterface;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class TechnologyTree implements TagSerializable {
   private final TechnologyOwner owner;

   public TechnologyTree(TechnologyOwner var1) {
      this.owner = var1;
   }

   public void fromTagStructure(Tag var1) {
   }

   public Tag toTagStructure() {
      return null;
   }

   public StateInterface getState() {
      return this.getOwner().getState();
   }

   public TechnologyOwner getOwner() {
      return this.owner;
   }

   public Short2ObjectOpenHashMap getAllTechs() {
      return ((GameStateInterface)this.getState()).getAllTechs();
   }
}
