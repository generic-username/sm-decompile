package org.schema.game.common.data.player;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.resource.FileExt;

public class PlayerSkin {
   public static final String EXTENSION = ".smskin";
   public static final String mainPatt = "_main_diff.png";
   public static final String mainEMPatt = "_main_em.png";
   public static final String helmetPatt = "_helmet_diff.png";
   public static final String helmetEMPatt = "_helmet_em.png";
   public String mainName;
   public String helmetName;
   public int mainTexId;
   public int mainEMId;
   public int helmetTexId;
   public int helmetEMId;

   public static PlayerSkin create(File var0, String var1) throws IOException {
      PlayerSkin var2 = new PlayerSkin();
      File[] var3 = var0.listFiles();
      var2.mainTexId = load(var3, var1, "_main_diff.png");
      var2.mainEMId = load(var3, var1, "_main_em.png");
      var2.helmetTexId = load(var3, var1, "_helmet_diff.png");
      var2.helmetEMId = load(var3, var1, "_helmet_em.png");
      return var2;
   }

   private static int load(File[] var0, String var1, String var2) throws IOException {
      for(int var3 = 0; var3 < var0.length; ++var3) {
         if (var0[var3].getName().endsWith(var2)) {
            Controller.getResLoader().getImageLoader().loadImage(var0[var3].getAbsolutePath(), var1 + var2, true);

            try {
               return Controller.getResLoader().getSprite(var1 + var2).getMaterial().getTexture().getTextureId();
            } catch (Exception var4) {
               var4.printStackTrace();
            }
         }
      }

      throw new FileNotFoundException("Cannot find vital texture of skin ending with: " + var2);
   }

   public static File createSkinFile(File var0, String var1, String var2, String var3, String var4) throws IOException {
      FileExt var8 = new FileExt(var1);
      FileExt var9 = new FileExt(var2);
      FileExt var10 = new FileExt(var3);
      FileExt var11 = new FileExt(var4);
      String var7 = var0.getAbsolutePath();
      FileOutputStream var5 = new FileOutputStream(var7);
      ZipOutputStream var6 = new ZipOutputStream(var5);
      System.out.println("Output to Zip : " + var7);
      add(var6, var8, "skin_main_diff.png");
      add(var6, var9, "skin_main_em.png");
      add(var6, var10, "skin_helmet_diff.png");
      add(var6, var11, "skin_helmet_em.png");
      var6.close();
      System.out.println("Done");
      return new FileExt(var7);
   }

   private static void add(ZipOutputStream var0, File var1, String var2) throws IOException {
      byte[] var3 = new byte[1024];
      System.out.println("File Added : " + var2);
      ZipEntry var5 = new ZipEntry(var2);
      var0.putNextEntry(var5);
      FileInputStream var4 = new FileInputStream(var1);

      int var6;
      while((var6 = var4.read(var3)) > 0) {
         var0.write(var3, 0, var6);
      }

      var4.close();
      var0.closeEntry();
   }

   public boolean containsMesh(Mesh var1) {
      return var1.getName().equals("PlayerMdl") || var1.getName().equals("AccHelmet");
   }

   public int getDiffuseIdFor(Mesh var1) {
      return var1.getName().equals("PlayerMdl") ? this.mainTexId : this.helmetTexId;
   }

   public int getEmissiveIdFor(Mesh var1) {
      return var1.getName().equals("PlayerMdl") ? this.mainEMId : this.helmetEMId;
   }
}
