package org.schema.game.common.data.explosion;

import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2FloatOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2LongOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.longs.Long2IntOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import javax.vecmath.Vector3f;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.HitReceiverType;
import org.schema.game.common.controller.damage.HitType;
import org.schema.game.common.controller.damage.effects.InterEffectContainer;
import org.schema.game.common.controller.damage.effects.InterEffectHandler;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.blockeffects.config.ConfigManagerInterface;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.world.SectorNotFoundException;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.network.objects.Sendable;

public class ExplosionCollisionSegmentCallback implements ExplosionDamageInterface {
   public ExplosionCollisionSegmentCallback.ArmorMode armorMode;
   public final Vector3f centerOfExplosion;
   public final ExplosionDataHandler explosionDataStructure;
   public final IntOpenHashSet sentDamage;
   public final ObjectOpenHashSet hitSegments;
   public final Object2ObjectOpenHashMap hitSendSegments;
   public final Int2IntOpenHashMap railMap;
   public final Int2IntOpenHashMap railRootMap;
   public final Int2ObjectOpenHashMap shieldHitPosMap;
   public final Int2ObjectOpenHashMap hitPosMap;
   public final Int2DoubleOpenHashMap shieldMap;
   public final Int2LongOpenHashMap shieldLocalMap;
   public final Int2DoubleOpenHashMap shieldMapPercent;
   public final Int2FloatOpenHashMap appliedDamageMap;
   public final Int2DoubleOpenHashMap shieldMapBef;
   public final Int2ObjectOpenHashMap interEffectMap;
   public final Int2FloatOpenHashMap environmentalProtectMultMap;
   public final Int2FloatOpenHashMap extraDamageTakenMap;
   private final InterEffectSet defenseTmp;
   private final ShortOpenHashSet closedList;
   private final ShortOpenHashSet cpy;
   public float explosionRadius;
   public DamageDealerType damageType;
   public long weaponId;
   public float shieldDamageBonus;
   public float[] explosionDamageBuffer;
   public ExplosionCubeConvexBlockCallback[] callbackCache;
   public int cubeCallbackPointer;
   public boolean ignoreShieldsGlobal;
   public ObjectOpenHashSet ownLockedSegments;
   LongArrayList collidingSegments;
   private Long2IntOpenHashMap explosionBlockMapA;
   private Long2IntOpenHashMap explosionBlockMapB;
   private Long2IntOpenHashMap explosionBlockMapC;
   private Long2IntOpenHashMap explosionBlockMapD;
   private Vector3f tmp;
   public final IntOpenHashSet entitiesToIgnoreShieldsOn;
   public boolean useLocalShields;
   private boolean ignoreShield;
   public HitType hitType;
   public InterEffectSet attack;

   public ExplosionCollisionSegmentCallback(ExplosionDataHandler var1) {
      this.armorMode = ExplosionCollisionSegmentCallback.ArmorMode.PREEMTIVE;
      this.centerOfExplosion = new Vector3f();
      this.sentDamage = new IntOpenHashSet();
      this.hitSegments = new ObjectOpenHashSet();
      this.hitSendSegments = new Object2ObjectOpenHashMap();
      this.railMap = new Int2IntOpenHashMap();
      this.railRootMap = new Int2IntOpenHashMap();
      this.shieldHitPosMap = new Int2ObjectOpenHashMap();
      this.hitPosMap = new Int2ObjectOpenHashMap();
      this.shieldMap = new Int2DoubleOpenHashMap();
      this.shieldLocalMap = new Int2LongOpenHashMap();
      this.shieldMapPercent = new Int2DoubleOpenHashMap();
      this.appliedDamageMap = new Int2FloatOpenHashMap();
      this.shieldMapBef = new Int2DoubleOpenHashMap();
      this.interEffectMap = new Int2ObjectOpenHashMap();
      this.environmentalProtectMultMap = new Int2FloatOpenHashMap();
      this.extraDamageTakenMap = new Int2FloatOpenHashMap();
      this.defenseTmp = new InterEffectSet();
      this.shieldDamageBonus = VoidElementManager.EXPLOSION_SHIELD_DAMAGE_BONUS;
      this.callbackCache = new ExplosionCubeConvexBlockCallback[4096];
      this.ownLockedSegments = new ObjectOpenHashSet(128);
      this.collidingSegments = new LongArrayList();
      this.explosionBlockMapA = new Long2IntOpenHashMap(4096);
      this.explosionBlockMapB = new Long2IntOpenHashMap(4096);
      this.explosionBlockMapC = new Long2IntOpenHashMap(4096);
      this.explosionBlockMapD = new Long2IntOpenHashMap(4096);
      this.tmp = new Vector3f();
      this.entitiesToIgnoreShieldsOn = new IntOpenHashSet();
      this.explosionBlockMapA.defaultReturnValue(-1);
      this.explosionBlockMapB.defaultReturnValue(-1);
      this.explosionBlockMapC.defaultReturnValue(-1);
      this.explosionBlockMapD.defaultReturnValue(-1);
      this.extraDamageTakenMap.defaultReturnValue(1.0F);
      this.environmentalProtectMultMap.defaultReturnValue(1.0F);
      this.explosionDataStructure = var1;
      this.explosionDamageBuffer = new float[var1.bigLength * var1.bigLength * var1.bigLength];
      this.closedList = new ShortOpenHashSet(1024);
      this.cpy = new ShortOpenHashSet(1024);

      for(int var2 = 0; var2 < this.callbackCache.length; ++var2) {
         this.callbackCache[var2] = new ExplosionCubeConvexBlockCallback();
      }

   }

   public void updateCallbacks() {
      ReentrantReadWriteLock var1 = null;

      for(int var2 = 0; var2 < this.cubeCallbackPointer; ++var2) {
         var1 = this.callbackCache[var2].update(var1);
      }

      if (var1 != null) {
         var1.readLock().unlock();
      }

   }

   public void addCharacterHittable(AbstractCharacter var1) {
      this.addControllerSub(var1, -0.3F);
      this.addControllerSub(var1, 0.0F);
      this.addControllerSub(var1, 0.3F);
   }

   private void addControllerSub(AbstractCharacter var1, float var2) {
      ExplosionCubeConvexBlockCallback var3;
      (var3 = this.callbackCache[this.cubeCallbackPointer]).type = 1;
      var3.segEntityId = var1.getId();
      var3.blockHpOrig = (short)((int)var1.getOwnerState().getHealth());
      var3.blockHp = (short)((int)var1.getOwnerState().getHealth());
      var3.boxTransform.set(var1.getWorldTransform());
      GlUtil.getUpVector(this.tmp, var3.boxTransform);
      this.tmp.scale(var2);
      var3.boxTransform.origin.add(this.tmp);
      this.tmp.sub(var3.boxTransform.origin, this.centerOfExplosion);
      var3.boxPosToCenterOfExplosion.set(this.tmp);
      ++this.cubeCallbackPointer;
   }

   public void sortInsertShieldAndArmorValues(Vector3f var1, int var2, List var3) throws SectorNotFoundException {
      this.shieldMap.clear();
      this.shieldLocalMap.clear();
      this.shieldMapBef.clear();
      this.shieldHitPosMap.clear();
      this.hitPosMap.clear();
      this.interEffectMap.clear();
      this.environmentalProtectMultMap.clear();
      this.railMap.clear();
      this.railRootMap.clear();
      this.appliedDamageMap.clear();
      this.extraDamageTakenMap.clear();
      Iterator var6 = var3.iterator();

      while(var6.hasNext()) {
         Sendable var4;
         if ((var4 = (Sendable)var6.next()) instanceof SimpleTransformableSendableObject) {
            this.interEffectMap.put(var4.getId(), ((SimpleTransformableSendableObject)var4).getEffectContainer());
         }

         if (var4 instanceof ConfigManagerInterface) {
            float var5 = ((ConfigManagerInterface)var4).getConfigManager().apply(StatusEffectType.ARMOR_DEFENSE_ENVIRONMENTAL, 1.0F);
            this.environmentalProtectMultMap.put(var4.getId(), var5);
         }

         if (var4 instanceof SegmentController) {
            SegmentController var7 = (SegmentController)var4;
            if (!this.ignoreShield && var7 instanceof ManagedSegmentController && ((ManagedSegmentController)var7).getManagerContainer() instanceof ShieldContainerInterface) {
               ((ShieldContainerInterface)((ManagedSegmentController)var7).getManagerContainer()).getShieldAddOn().fillShieldsMapRecursive(var1, var2, this.shieldMap, this.shieldMapBef, this.shieldMapPercent, this.railMap, this.railRootMap, this.shieldLocalMap);
            }

            this.extraDamageTakenMap.put(var7.getId(), var7.getDamageTakenMultiplier(this.damageType));
         }
      }

   }

   public void sortAndInsertCallbackCache() {
      this.explosionBlockMapA.clear();
      this.explosionBlockMapB.clear();
      this.explosionBlockMapC.clear();
      this.explosionBlockMapD.clear();

      for(int var1 = 0; var1 < this.cubeCallbackPointer; ++var1) {
         ExplosionCubeConvexBlockCallback var2;
         int var3 = Math.round((var2 = this.callbackCache[var1]).boxPosToCenterOfExplosion.x);
         int var4 = Math.round(var2.boxPosToCenterOfExplosion.y);
         int var7 = Math.round(var2.boxPosToCenterOfExplosion.z);
         long var5 = ElementCollection.getIndex(var3, var4, var7);
         if (!this.explosionBlockMapA.containsKey(var5)) {
            this.explosionBlockMapA.put(var5, var1);
         } else if (!this.explosionBlockMapB.containsKey(var5)) {
            this.explosionBlockMapB.put(var5, var1);
         } else if (!this.explosionBlockMapC.containsKey(var5)) {
            this.explosionBlockMapC.put(var5, var1);
         } else if (!this.explosionBlockMapD.containsKey(var5)) {
            this.explosionBlockMapD.put(var5, var1);
         } else {
            assert false;
         }
      }

   }

   public void reset() {
      this.cubeCallbackPointer = 0;
      this.ownLockedSegments.clear();
      this.hitSegments.clear();
      this.hitSendSegments.clear();
      this.entitiesToIgnoreShieldsOn.clear();
      this.armorMode = ExplosionCollisionSegmentCallback.ArmorMode.PREEMTIVE;
   }

   public void growCache(int var1) {
      if (var1 >= this.callbackCache.length) {
         ExplosionCubeConvexBlockCallback[] var3 = new ExplosionCubeConvexBlockCallback[this.callbackCache.length << 1];

         int var2;
         for(var2 = 0; var2 < this.callbackCache.length; ++var2) {
            var3[var2] = this.callbackCache[var2];
         }

         for(var2 = this.callbackCache.length; var2 < var3.length; ++var2) {
            var3[var2] = new ExplosionCubeConvexBlockCallback();
         }

         this.callbackCache = var3;
      }

   }

   private float damageBlock(int var1, int var2, int var3, float var4, ExplosionCubeConvexBlockCallback var5) {
      if (var5.blockHp == 0) {
         return var4;
      } else {
         float var8 = 0.0F;
         this.appliedDamageMap.put(var5.segEntityId, this.appliedDamageMap.get(var5.segEntityId) + var4);
         if (this.damageType == DamageDealerType.PULSE) {
            var4 = var8 = var4 * 0.5F;
         }

         if (this.hitShield(var4, var5)) {
            return var8;
         } else {
            InterEffectContainer var9 = (InterEffectContainer)this.interEffectMap.get(var5.segEntityId);
            ElementInformation var11 = null;
            if (this.armorMode == ExplosionCollisionSegmentCallback.ArmorMode.ON_ALL_BLOCKS && var5.blockId != 0) {
               HitReceiverType var6 = (var11 = ElementKeyMap.getInfoFast(var5.blockId)).isArmor() ? HitReceiverType.ARMOR : HitReceiverType.BLOCK;
               InterEffectSet var10 = var9.get(var6);
               InterEffectSet var7;
               (var7 = this.defenseTmp).setDefenseFromInfo(var11);
               if (this.hitType == HitType.ENVIROMENTAL) {
                  var7.scaleAdd(var10, this.environmentalProtectMultMap.get(var5.segEntityId));
               } else {
                  var7.add(var10);
               }

               var4 = InterEffectHandler.handleEffects(var4, this.attack, var7, this.hitType, this.damageType, var6, var5.blockId);
            }

            int var12 = var5.blockHp;
            var2 = Math.max(0, var12 - Math.round(var4 * this.extraDamageTakenMap.get(var5.segEntityId)));
            int var13 = Math.max(0, var2);
            ExplosionCubeConvexBlockCallback var10000 = (ExplosionCubeConvexBlockCallback)this.hitPosMap.get(var5.segEntityId);
            var9 = null;
            if (var10000 == null) {
               this.hitPosMap.put(var5.segEntityId, var5);
            }

            if (var4 < (float)var12) {
               Math.max(0.0F, var4);
               var5.blockHp = var13;
               return var8;
            } else {
               var4 = Math.max(0.0F, var4 - (float)var12);
               var5.blockHp = 0;
               return var4 + var8;
            }
         }
      }
   }

   @Deprecated
   private int findShieldShare(float var1, int var2, int var3) {
      double var4;
      while((var4 = this.shieldMap.get(var3)) <= 0.0D || var2 != var3) {
         if (var2 != var3 && this.shieldMapPercent.get(var3) > VoidElementManager.SHIELD_DOCK_TRANSFER_LIMIT && (double)var1 < var4) {
            return var3;
         }

         if (!this.railMap.containsKey(var3)) {
            return -1;
         }

         var3 = this.railMap.get(var3);
         var2 = var2;
         var1 = var1;
         this = this;
      }

      return var2;
   }

   private boolean hitShield(float var1, ExplosionCubeConvexBlockCallback var2) {
      if (!this.ignoreShieldsGlobal && !this.entitiesToIgnoreShieldsOn.contains(var2.segEntityId)) {
         int var3;
         if (this.useLocalShields) {
            int var4;
            if ((var4 = this.railRootMap.get(var2.segEntityId)) == 0) {
               var4 = var2.segEntityId;
            }

            if (this.shieldMap.get(var4) < (double)var1) {
               var3 = -1;
            } else {
               var3 = var4;
            }
         } else {
            var3 = this.findShieldShare(var1, var2.segEntityId, var2.segEntityId);
         }

         double var11;
         if (var3 >= 0 && (var11 = this.shieldMap.get(var3)) > 0.0D) {
            HitReceiverType var6 = HitReceiverType.SHIELD;
            ElementInformation var7 = ElementKeyMap.getInfoFast(var2.blockId);
            InterEffectContainer var8 = (InterEffectContainer)this.interEffectMap.get(var2.segEntityId);
            InterEffectSet var9;
            (var9 = this.defenseTmp).setEffect(VoidElementManager.shieldEffectConfiguration);
            var9.add(var8.get(var6));
            if (VoidElementManager.individualBlockEffectArmorOnShieldHit) {
               var9.add(var7.effectArmor);
            }

            float var10000 = InterEffectHandler.handleEffects(var1, this.attack, var9, this.hitType, this.damageType, var6, var2.blockId);
            float var12 = var10000 + var10000 * this.shieldDamageBonus;
            var11 = Math.max(0.0D, var11 - (double)var12);
            this.shieldMap.put(var3, var11 == 0.0D ? -1.0D : var11);
            ExplosionCubeConvexBlockCallback var13 = (ExplosionCubeConvexBlockCallback)this.shieldHitPosMap.get(var3);
            Object var10 = null;
            if (var13 == null) {
               this.shieldHitPosMap.put(var3, var2);
            }

            return true;
         } else {
            return false;
         }
      } else {
         return false;
      }
   }

   public float modifyDamageBasedOnBlockArmor(int var1, int var2, int var3, float var4) {
      if ((var1 = this.explosionBlockMapA.get(ElementCollection.getIndex(var1, var2, var3))) >= 0 && ElementKeyMap.isValidType(this.callbackCache[var1].blockId)) {
         ExplosionCubeConvexBlockCallback var7 = this.callbackCache[var1];
         InterEffectContainer var8 = (InterEffectContainer)this.interEffectMap.get(var7.segEntityId);
         ElementInformation var9;
         HitReceiverType var5 = (var9 = ElementKeyMap.getInfoFast(var7.blockId)).isArmor() ? HitReceiverType.ARMOR : HitReceiverType.BLOCK;
         InterEffectSet var6;
         (var6 = this.defenseTmp).setDefenseFromInfo(var9);
         if (this.hitType == HitType.ENVIROMENTAL) {
            var6.scaleAdd(var8.get(var5), this.environmentalProtectMultMap.get(var7.segEntityId));
         } else {
            var6.add(var8.get(var5));
         }

         var4 = InterEffectHandler.handleEffects(var4, this.attack, var6, this.hitType, this.damageType, var5, var7.blockId);
      }

      return var4;
   }

   public float damageBlock(int var1, int var2, int var3, float var4) {
      int var5;
      if ((var5 = this.explosionBlockMapA.get(ElementCollection.getIndex(var1, var2, var3))) >= 0) {
         var4 = this.damageBlock(var1, var2, var3, var4, this.callbackCache[var5]);
         if ((var5 = this.explosionBlockMapB.get(ElementCollection.getIndex(var1, var2, var3))) >= 0) {
            var4 = this.damageBlock(var1, var2, var3, var4, this.callbackCache[var5]);
            if ((var5 = this.explosionBlockMapC.get(ElementCollection.getIndex(var1, var2, var3))) >= 0) {
               var4 = this.damageBlock(var1, var2, var3, var4, this.callbackCache[var5]);
               if ((var5 = this.explosionBlockMapD.get(ElementCollection.getIndex(var1, var2, var3))) >= 0) {
                  var4 = this.damageBlock(var1, var2, var3, var4, this.callbackCache[var5]);
               }
            }
         }
      }

      return var4;
   }

   public void setDamage(int var1, int var2, int var3, float var4) {
      var1 += this.explosionDataStructure.bigLengthHalf;
      var2 += this.explosionDataStructure.bigLengthHalf;
      var1 += (var3 + this.explosionDataStructure.bigLengthHalf) * this.explosionDataStructure.bigLengthQuad + var2 * this.explosionDataStructure.bigLength;
      this.explosionDamageBuffer[var1] = var4;
   }

   public float getDamage(int var1, int var2, int var3) {
      var1 += this.explosionDataStructure.bigLengthHalf;
      var2 += this.explosionDataStructure.bigLengthHalf;
      int var4 = (var3 += this.explosionDataStructure.bigLengthHalf) * this.explosionDataStructure.bigLengthQuad + var2 * this.explosionDataStructure.bigLength + var1;

      assert var4 < this.explosionDamageBuffer.length : var1 + ", " + var2 + ", " + var3;

      return this.explosionDamageBuffer[var4];
   }

   public void resetDamage() {
      Arrays.fill(this.explosionDamageBuffer, 0.0F);
      this.closedList.clear();
      this.cpy.clear();
   }

   public ShortOpenHashSet getClosedList() {
      return this.closedList;
   }

   public ShortOpenHashSet getCpy() {
      return this.cpy;
   }

   public Vector3f getExplosionCenter() {
      return this.centerOfExplosion;
   }

   public static enum ArmorMode {
      ON_ALL_BLOCKS,
      PREEMTIVE,
      NONE;
   }
}
