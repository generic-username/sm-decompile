package org.schema.game.common.data.explosion;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.zip.ZipInputStream;
import org.schema.common.util.StringTools;
import org.schema.schine.common.language.Lng;
import org.schema.schine.resource.FileExt;

public class ExplosionDataHandler {
   public static final int spade = 5;
   public int bigLength;
   public int bigLengthHalf;
   public int bigLengthQuad;
   private short[] explosionAggegation;
   private int[] explosionOrder;
   private int sizeAggr;
   private int sizeAggr2;
   private int sizeRayInfo;
   private int sizeRayInfo2;
   private boolean loaded;
   private ExplosionRayInfoArray explosionRayData;

   public static void main(String[] var0) {
      ExplosionDataHandler var4;
      (var4 = new ExplosionDataHandler()).loadData();
      ExplosionCollisionSegmentCallback var1 = new ExplosionCollisionSegmentCallback(var4);
      long var2 = System.currentTimeMillis();
      var4.applyDamage(var1, 16, 1000.0F);
      System.err.println("TOOK:: " + (System.currentTimeMillis() - var2));
      var4.printGrid(var1);
   }

   public int getIndex(int var1, int var2, int var3) {
      return var3 * this.sizeAggr2 + var2 * this.sizeAggr + var1;
   }

   int getIndexRay(int var1, int var2, int var3) {
      return var3 * this.sizeRayInfo2 + var2 * this.sizeRayInfo + var1;
   }

   public void loadData() {
      if (!this.loaded) {
         try {
            ZipInputStream var2;
            DataInputStream var1 = new DataInputStream(var2 = new ZipInputStream(new BufferedInputStream(new FileInputStream(new FileExt("./data/sphereExplosionData.zip")), 16384)));
            var2.getNextEntry();
            this.sizeAggr = var1.readInt();
            this.sizeAggr2 = this.sizeAggr * this.sizeAggr;
            this.bigLength = this.sizeAggr;
            this.bigLengthQuad = this.bigLength * this.bigLength;
            this.bigLengthHalf = this.bigLength / 2;
            if (this.sizeAggr != 128) {
               var1.close();
               throw new RuntimeException(Lng.ORG_SCHEMA_GAME_COMMON_DATA_EXPLOSION_EXPLOSIONDATAHANDLER_1);
            } else {
               System.err.println("EXPLOSION DATA: aggregation data: dim size: " + this.sizeAggr + "; total Memory used: " + StringTools.readableFileSize((long)(this.sizeAggr * this.sizeAggr * this.sizeAggr << 1)) + "; max memory: " + StringTools.readableFileSize(Runtime.getRuntime().maxMemory()));
               this.explosionAggegation = new short[this.sizeAggr * this.sizeAggr * this.sizeAggr];

               int var3;
               for(int var10 = 0; var10 < this.sizeAggr; ++var10) {
                  for(var3 = 0; var3 < this.sizeAggr; ++var3) {
                     for(int var4 = 0; var4 < this.sizeAggr; ++var4) {
                        this.explosionAggegation[this.getIndex(var4, var3, var10)] = var1.readShort();
                     }
                  }
               }

               this.sizeRayInfo = var1.readInt();
               this.sizeRayInfo2 = this.sizeRayInfo * this.sizeRayInfo;
               System.err.println("[EXPLOSION] DATA: ray info data: dim size: " + this.sizeRayInfo + "; total instances used: " + this.sizeRayInfo * this.sizeRayInfo * this.sizeRayInfo);
               this.explosionRayData = ExplosionRayInfoArray.read(this.sizeRayInfo, 13484, this, var1);
               var3 = var1.readInt();
               System.err.println("[EXPLOSION] DATA: sizeOrder size: " + var3 + " -> " + StringTools.readableFileSize((long)(var3 << 2)));
               long var13 = System.nanoTime();
               byte[] var11 = new byte[var3 << 2];
               var1.readFully(var11);
               IntBuffer var12 = ByteBuffer.wrap(var11).order(ByteOrder.BIG_ENDIAN).asIntBuffer();
               this.explosionOrder = new int[var12.remaining()];
               var12.get(this.explosionOrder);
               long var6 = (System.nanoTime() - var13) / 1000000L;
               System.err.println("[EXPLOSION] 'Order' File read took " + var6 + " ms");
               var1.close();
               this.loaded = true;
            }
         } catch (FileNotFoundException var8) {
            var8.printStackTrace();
         } catch (IOException var9) {
            var9.printStackTrace();
         }
      }
   }

   public void applyDamage(ExplosionDamageInterface var1, int var2, float var3) {
      var1.resetDamage();
      var3 = var1.modifyDamageBasedOnBlockArmor(0, 0, 0, var3);
      float var4 = var1.damageBlock(0, 0, 0, var3);
      var1.setDamage(0, 0, 0, var4);
      if (var4 > 0.0F) {
         int var10 = 1;

         for(int var5 = this.sizeRayInfo; this.explosionOrder[var10 * 5 + 3] < var2; ++var10) {
            int var6 = this.explosionOrder[var10 * 5];
            int var7 = this.explosionOrder[var10 * 5 + 1];
            int var8 = this.explosionOrder[var10 * 5 + 2];
            int var9 = this.explosionOrder[var10 * 5 + 4];
            if (var6 + this.sizeRayInfo / 2 >= 0 && var7 + this.sizeRayInfo / 2 >= 0 && var8 + this.sizeRayInfo / 2 >= 0 && var6 + this.sizeRayInfo / 2 < var5 && var7 + this.sizeRayInfo / 2 < var5 && var8 + this.sizeRayInfo / 2 < var5) {
               this.handleDetail(var6, var7, var8, var10, var9, var1, var3);
            } else {
               this.handleMacro(var6, var7, var8, var10, var9, var1, var3);
            }
         }

      }
   }

   public void printGrid(ExplosionDamageInterface var1) {
      for(int var2 = -this.sizeAggr / 2 + 1; var2 < this.sizeAggr / 2 - 1; ++var2) {
         for(int var3 = -this.sizeAggr / 2 + 1; var3 < this.sizeAggr / 2 - 1; ++var3) {
            int var4 = (int)var1.getDamage(0, var2, var3);
            System.out.printf("%5d ", var4);
         }

         System.out.println();
      }

   }

   private void handleMacro(int var1, int var2, int var3, int var4, int var5, ExplosionDamageInterface var6, float var7) {
      var4 = this.explosionOrder[var5 * 5];
      int var10 = this.explosionOrder[var5 * 5 + 1];
      var5 = this.explosionOrder[var5 * 5 + 2];
      float var8;
      if ((var8 = var6.getDamage(var4, var10, var5)) > 0.0F) {
         short var9 = this.explosionAggegation[this.getIndex(var1 + this.sizeAggr / 2, var2 + this.sizeAggr / 2, var3 + this.sizeAggr / 2)];
         if (!var6.getClosedList().contains(var9)) {
            var8 = var6.damageBlock(var1, var2, var3, var8);
            var6.setDamage(var1, var2, var3, var8);
            if (var8 <= 0.0F) {
               var6.getClosedList().add(var9);
            }

         }
      }
   }

   private void handleDetail(int var1, int var2, int var3, int var4, int var5, ExplosionDamageInterface var6, float var7) {
      int var8 = this.getIndexRay(var1 + this.sizeRayInfo / 2, var2 + this.sizeRayInfo / 2, var3 + this.sizeRayInfo / 2);
      if (this.explosionRayData.hasAny(var8)) {
         var4 = this.explosionOrder[var5 * 5];
         int var12 = this.explosionOrder[var5 * 5 + 1];
         var5 = this.explosionOrder[var5 * 5 + 2];
         float var9;
         if ((var9 = var6.getDamage(var4, var12, var5)) > 0.0F) {
            this.explosionRayData.addAllTo(var8, var6.getCpy());
            var6.getCpy().removeAll(var6.getClosedList());
            int var10 = var6.getCpy().size();
            var6.getCpy().clear();
            if (var10 > 0) {
               var4 = this.getIndexRay(var4 + this.sizeRayInfo / 2, var12 + this.sizeRayInfo / 2, var5 + this.sizeRayInfo / 2);
               var4 = this.explosionRayData.getSize(var4);
               float var11 = (float)var10 / (float)var4;
               var11 = var6.damageBlock(var1, var2, var3, var11 * var9);
               var6.setDamage(var1, var2, var3, var11);

               assert var6.getDamage(var1, var2, var3) == var11;

               if (var11 <= 0.0F) {
                  this.explosionRayData.addAllTo(var8, var6.getClosedList());
               }
            }

         }
      } else {
         this.handleMacro(var1, var2, var3, var4, var5, var6, var7);
      }
   }
}
