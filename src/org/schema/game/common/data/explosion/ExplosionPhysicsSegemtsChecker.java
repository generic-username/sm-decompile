package org.schema.game.common.data.explosion;

import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.game.common.controller.SegmentBufferIteratorInterface;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.CubeConvexVariableSet;
import org.schema.game.common.data.physics.CubeShape;
import org.schema.game.common.data.physics.GjkPairDetectorExt;
import org.schema.game.common.data.physics.octree.ArrayOctree;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentDataWriteException;

public class ExplosionPhysicsSegemtsChecker {
   private static final float margin = 0.05F;
   private static ThreadLocal threadLocal = new ThreadLocal() {
      protected final CubeConvexVariableSet initialValue() {
         return new CubeConvexVariableSet();
      }
   };
   GjkPairDetectorExt gjkPairDetector;
   private CubeConvexVariableSet v;
   private ExplosionPhysicsSegemtsChecker.OuterSegmentHandler outerSegmentHandler = new ExplosionPhysicsSegemtsChecker.OuterSegmentHandler();

   private void processDistinctCollision(CubeShape var1, SegmentData var2, Transform var3, Transform var4, ExplosionCollisionSegmentCallback var5) {
      assert var5 != null;

      if (!this.v.intersectionCallBackAwithB.initialized) {
         this.v.intersectionCallBackAwithB.createHitCache(4096);
      }

      this.v.intersectionCallBackAwithB.reset();

      assert var5.explosionRadius > 0.0F;

      this.v.intersectionCallBackAwithB = var2.getOctree().findIntersectingAABB(this.v.oSet, this.v.intersectionCallBackAwithB, var2.getSegment(), var3, this.v.absolute, 0.05F, this.v.shapeMin, this.v.shapeMax, 1.0F, var5.centerOfExplosion, var5.explosionRadius);
      if (this.v.intersectionCallBackAwithB.hitCount != 0) {
         for(int var6 = 0; var6 < this.v.intersectionCallBackAwithB.hitCount; ++var6) {
            this.v.intersectionCallBackAwithB.getHit(var6, this.v.hitMin, this.v.hitMax, this.v.startA, this.v.endA);

            assert this.v.startA.x < this.v.endA.x && this.v.startA.y < this.v.endA.y && this.v.startA.z < this.v.endA.z;

            this.doNarrowTest(var2, this.v.startA, this.v.endA, var5);
         }

      }
   }

   private void doNarrowTest(SegmentData var1, Vector3b var2, Vector3b var3, ExplosionCollisionSegmentCallback var4) {
      for(byte var5 = var2.x; var5 < var3.x; ++var5) {
         for(byte var6 = var2.y; var6 < var3.y; ++var6) {
            for(byte var7 = var2.z; var7 < var3.z; ++var7) {
               int var8 = SegmentData.getInfoIndex((byte)(var5 + 16), (byte)(var6 + 16), (byte)(var7 + 16));
               if (var1.contains(var8)) {
                  try {
                     var1.checkWritable();
                  } catch (SegmentDataWriteException var11) {
                     var11.printStackTrace();

                     assert false;
                  }

                  ElementInformation var9 = ElementKeyMap.getInfoFast(var1.getType(var8));
                  this.v.elemPosA.set((float)var5, (float)var6, (float)var7);
                  Vector3f var10000 = this.v.elemPosA;
                  var10000.x += (float)var1.getSegment().pos.x;
                  var10000 = this.v.elemPosA;
                  var10000.y += (float)var1.getSegment().pos.y;
                  var10000 = this.v.elemPosA;
                  var10000.z += (float)var1.getSegment().pos.z;
                  this.v.min.set(this.v.elemPosA);
                  this.v.max.set(this.v.elemPosA);
                  var10000 = this.v.min;
                  var10000.x -= 0.5F;
                  var10000 = this.v.min;
                  var10000.y -= 0.5F;
                  var10000 = this.v.min;
                  var10000.z -= 0.5F;
                  var10000 = this.v.max;
                  var10000.x += 0.5F;
                  var10000 = this.v.max;
                  var10000.y += 0.5F;
                  var10000 = this.v.max;
                  var10000.z += 0.5F;
                  AabbUtil2.transformAabb(this.v.min, this.v.max, 0.03F, this.v.cubeMeshTransform, this.v.minOut, this.v.maxOut);
                  this.v.closest.set(var4.centerOfExplosion);
                  Vector3fTools.clamp(this.v.closest, this.v.minOut, this.v.maxOut);
                  this.v.closest.sub(var4.centerOfExplosion);
                  boolean var10 = this.v.closest.length() < var4.explosionRadius;
                  this.v.box0.setMargin(0.0F);
                  if (var10) {
                     var4.growCache(var4.cubeCallbackPointer);
                     this.v.boxTransformation.set(this.v.cubeMeshTransform);
                     this.v.nA.set(this.v.elemPosA);
                     this.v.boxTransformation.basis.transform(this.v.nA);
                     this.v.boxTransformation.origin.add(this.v.nA);
                     var4.callbackCache[var4.cubeCallbackPointer].blockPos.set(var5 + var1.getSegment().pos.x + 16, var6 + var1.getSegment().pos.y + 16, var7 + var1.getSegment().pos.z + 16);
                     var4.callbackCache[var4.cubeCallbackPointer].segmentPos.set(var1.getSegment().pos);
                     var4.callbackCache[var4.cubeCallbackPointer].type = 0;
                     var4.callbackCache[var4.cubeCallbackPointer].segDataIndex = var8;
                     var4.callbackCache[var4.cubeCallbackPointer].segPosX = (byte)(var5 + 16);
                     var4.callbackCache[var4.cubeCallbackPointer].segPosY = (byte)(var6 + 16);
                     var4.callbackCache[var4.cubeCallbackPointer].segPosZ = (byte)(var7 + 16);
                     var4.callbackCache[var4.cubeCallbackPointer].boxTransform.set(this.v.boxTransformation);
                     this.v.closest.sub(this.v.boxTransformation.origin, var4.centerOfExplosion);
                     var4.callbackCache[var4.cubeCallbackPointer].boxPosToCenterOfExplosion.set(this.v.closest);
                     var4.callbackCache[var4.cubeCallbackPointer].boxDistToCenterOfExplosion = FastMath.carmackLength(this.v.closest);
                     var4.callbackCache[var4.cubeCallbackPointer].blockId = var9.getId();
                     var4.callbackCache[var4.cubeCallbackPointer].blockActive = var1.isActive(var8);
                     var4.callbackCache[var4.cubeCallbackPointer].blockHp = var9.convertToFullHp(var1.getHitpointsByte(var8));
                     var4.callbackCache[var4.cubeCallbackPointer].blockHpOrig = var4.callbackCache[var4.cubeCallbackPointer].blockHp;
                     var4.callbackCache[var4.cubeCallbackPointer].segEntityId = var1.getSegmentController().getId();
                     var4.callbackCache[var4.cubeCallbackPointer].data = var1;
                     ++var4.cubeCallbackPointer;
                  }
               }
            }
         }
      }

   }

   public void processCollision(CubeShape var1, Transform var2, ConvexShape var3, Transform var4, ExplosionCollisionSegmentCallback var5) {
      this.v = (CubeConvexVariableSet)threadLocal.get();
      this.v.cubesShape0 = var1;
      this.v.convexShape = var3;
      this.v.oSet = ArrayOctree.getSet(var1.getSegmentBuffer().getSegmentController().isOnServer());
      this.v.cubeMeshTransform.set(var2);
      this.v.convexShapeTransform.set(var4);
      this.v.cubeShapeTransformInv.set(this.v.cubeMeshTransform);
      this.v.cubeShapeTransformInv.inverse();
      this.v.absolute.set(this.v.cubeMeshTransform.basis);
      MatrixUtil.absolute(this.v.absolute);
      var1.setMargin(0.05F);
      this.v.convexShapeViewFromCubes.set(this.v.cubeShapeTransformInv);
      this.v.convexShapeViewFromCubes.mul(this.v.convexShapeTransform);
      var1.getAabb(TransformTools.ident, this.v.outMin, this.v.outMax);
      var3.getAabb(this.v.convexShapeViewFromCubes, this.v.inner.min, this.v.inner.max);
      var3.getAabb(this.v.convexShapeTransform, this.v.shapeMin, this.v.shapeMax);
      var1.setMargin(0.05F);
      this.outerSegmentHandler.cubeShape0 = var1;
      this.v.outer.min.set(this.v.outMin);
      this.v.outer.max.set(this.v.outMax);
      if (this.v.inner.getIntersection(this.v.outer, this.v.outBB) != null && this.v.outBB.isValid()) {
         this.v.minIntA.x = ByteUtil.divSeg((int)(this.v.outBB.min.x - 16.0F)) << 5;
         this.v.minIntA.y = ByteUtil.divSeg((int)(this.v.outBB.min.y - 16.0F)) << 5;
         this.v.minIntA.z = ByteUtil.divSeg((int)(this.v.outBB.min.z - 16.0F)) << 5;
         this.v.maxIntA.x = FastMath.fastCeil((this.v.outBB.max.x + 16.0F) / 32.0F) << 5;
         this.v.maxIntA.y = FastMath.fastCeil((this.v.outBB.max.y + 16.0F) / 32.0F) << 5;
         this.v.maxIntA.z = FastMath.fastCeil((this.v.outBB.max.z + 16.0F) / 32.0F) << 5;
         this.outerSegmentHandler.res = var5;
         var1.getSegmentBuffer().iterateOverNonEmptyElementRange(this.outerSegmentHandler, this.v.minIntA, this.v.maxIntA, false);
         this.outerSegmentHandler.res = null;
         this.v = null;
      }
   }

   class OuterSegmentHandler implements SegmentBufferIteratorInterface {
      CubeShape cubeShape0;
      ExplosionCollisionSegmentCallback res;

      private OuterSegmentHandler() {
      }

      public boolean handle(Segment var1, long var2) {
         assert this.res != null;

         if (var1.getSegmentData() != null && !var1.isEmpty()) {
            this.cubeShape0.getSegmentAabb(var1, ExplosionPhysicsSegemtsChecker.this.v.cubeMeshTransform, ExplosionPhysicsSegemtsChecker.this.v.outMin, ExplosionPhysicsSegemtsChecker.this.v.outMax, ExplosionPhysicsSegemtsChecker.this.v.localMinOut, ExplosionPhysicsSegemtsChecker.this.v.localMaxOut, ExplosionPhysicsSegemtsChecker.this.v.aabbVarSet);
            boolean var10000 = AabbUtil2.testAabbAgainstAabb2(ExplosionPhysicsSegemtsChecker.this.v.shapeMin, ExplosionPhysicsSegemtsChecker.this.v.shapeMax, ExplosionPhysicsSegemtsChecker.this.v.outMin, ExplosionPhysicsSegemtsChecker.this.v.outMax);
            boolean var5 = false;
            if (var10000) {
               try {
                  var1.getSegmentData().checkWritable();
               } catch (SegmentDataWriteException var4) {
                  SegmentDataWriteException.replaceData(var1);
               }

               int var6 = this.res.cubeCallbackPointer;
               ExplosionPhysicsSegemtsChecker.this.processDistinctCollision(this.cubeShape0, var1.getSegmentData(), ExplosionPhysicsSegemtsChecker.this.v.cubeMeshTransform, ExplosionPhysicsSegemtsChecker.this.v.convexShapeTransform, this.res);
               if (this.res.cubeCallbackPointer > var6) {
                  this.res.ownLockedSegments.add(var1);
               }
            }

            return true;
         } else {
            return true;
         }
      }

      // $FF: synthetic method
      OuterSegmentHandler(Object var2) {
         this();
      }
   }
}
