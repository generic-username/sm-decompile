package org.schema.game.common.data;

import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.longs.Long2BooleanOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2FloatOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.ClientStatics;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.view.gui.leaderboard.GUIScrollableLoaderboardList;
import org.schema.game.common.controller.FactionChange;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.activities.RaceManager;
import org.schema.game.common.controller.elements.InventoryMap;
import org.schema.game.common.controller.rules.RulePropertyContainer;
import org.schema.game.common.controller.rules.RuleSetManager;
import org.schema.game.common.controller.trade.TradeManager;
import org.schema.game.common.controller.trade.manualtrade.ManualTradeManager;
import org.schema.game.common.data.blockeffects.config.ConfigPool;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.fleet.FleetStateInterface;
import org.schema.game.common.data.gamemode.AbstractGameMode;
import org.schema.game.common.data.gamemode.battle.KillerEntity;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.catalog.CatalogManager;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventoryHolder;
import org.schema.game.common.data.player.inventory.InventoryMultMod;
import org.schema.game.common.data.player.inventory.NPCFactionInventory;
import org.schema.game.common.data.player.inventory.NetworkInventoryInterface;
import org.schema.game.common.data.player.inventory.StashInventory;
import org.schema.game.common.data.world.RuleEntityContainer;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.network.objects.GalaxyRequestAndAwnser;
import org.schema.game.network.objects.LongBooleanPair;
import org.schema.game.network.objects.NetworkGameState;
import org.schema.game.network.objects.remote.RemoteGalaxyRequest;
import org.schema.game.network.objects.remote.RemoteInventory;
import org.schema.game.network.objects.remote.RemoteInventoryMultMod;
import org.schema.game.network.objects.remote.RemoteLeaderboard;
import org.schema.game.network.objects.remote.RemoteLongBoolean;
import org.schema.game.network.objects.remote.RemoteMetaObjectStateLess;
import org.schema.game.network.objects.remote.RemoteNPCSystem;
import org.schema.game.network.objects.remote.RemoteRuleSetManager;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.game.server.data.simulation.npc.geo.NPCSystemStub;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.TopLevelType;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.objects.remote.RemoteArray;
import org.schema.schine.network.objects.remote.RemoteSerializableObject;
import org.schema.schine.network.objects.remote.RemoteStringArray;
import org.schema.schine.network.server.ServerStateInterface;
import org.schema.schine.resource.FileExt;

public class SendableGameState implements InventoryHolder, Sendable {
   public static final int TEXT_BLOCK_LIMIT = 240;
   public static final int TEXT_BLOCK_LINE_LIMIT = 10;
   private final StateInterface state;
   private final boolean onServer;
   private final ObjectArrayFIFOQueue debugObjects = new ObjectArrayFIFOQueue();
   private final FactionManager factionManager;
   private final CatalogManager catalogManager;
   private final ObjectArrayFIFOQueue metaObjectsToAnnounce = new ObjectArrayFIFOQueue();
   private final ObjectArrayList gameModes = new ObjectArrayList();
   private final IntOpenHashSet frozenSectors = new IntOpenHashSet();
   public boolean leaderBoardChanged;
   public GUIScrollableLoaderboardList leaderboardGUI;
   public float sunMinIntensityDamageRange = 1.42F;
   public double massLimitShip = -1.0D;
   public double massLimitPlanet = -1.0D;
   public double massLimitStation = -1.0D;
   public int blockLimitShip = -1;
   public int blockLimitPlanet = -1;
   public int blockLimitStation = -1;
   private RulePropertyContainer ruleProperties;
   public final InventoryMap imap;
   String battlemodeDesc = "[What is this?]\nBattleMode is a first test for StarMade to\nhandle all kinds of game modes in the future!\n\nTo join battle, you have to build a ship and \njoin a battle faction before the countdown ends!\nYour ship will copied before you enter battle,\nso you don't have to care about resources!\nAfter a winner is determined, the countdown resets.\n\nMay the best player win!\n";
   float cachedYear;
   private int id;
   private boolean markedForDeleteVolatile;
   private boolean markedForDeleteVolatileSent;
   private NetworkGameState networkGameState;
   private float maxGalaxySpeed;
   private float linearDamping;
   private float turnSpeedDivisor;
   private float rotationalDamping;
   private Object2ObjectOpenHashMap clientLeaderboard = new Object2ObjectOpenHashMap();
   private Object2IntOpenHashMap clientDeadCount = new Object2IntOpenHashMap();
   private boolean additiveProjectiles;
   private boolean dynamicPrices;
   private String serverMessage;
   private final ManualTradeManager manualTradeManager;
   private String serverDescription;
   private String serverName;
   private Integer recipeBlockCost;
   private boolean ignoreDockingArea;
   private float relativeProjectiles;
   private float weaponRangeReference;
   private boolean writtenForUnload;
   private boolean serverDeployedBlockBehavior;
   private long updateTime;
   private float sectorSize;
   private int maxBuildArea;
   private String currentGameModeOutput;
   private String lastClientShownGameModeMessage;
   private String clientBattlemodeSettings;
   private short lastUpdateRotationProg;
   private float planetSizeMean;
   private float planetSizeDeviation;
   private boolean lockFactionShips;
   private boolean buyBBWithCredits;
   private boolean allowOldDockingBeam;
   private boolean weightedCenterOfMass;
   private float shopRebootCostPerSecond;
   private float shopArmorRepairPerSecond;
   private int sectorsToExploreForSystemScan;
   private int stationCostClient;
   private int factionKickInactiveTimeLimit;
   private final RaceManager raceManager;
   private boolean allowPersonalInvOverCap;
   private int maxChainDocking;
   private boolean onlyAddFactionToFleet;
   private final TradeManager tradeManager;
   private ObjectArrayFIFOQueue ntInventoryAdds;
   private ObjectArrayFIFOQueue ntInventoryRemoves;
   private final ObjectArrayFIFOQueue ntInventoryMultMods;
   private final Object2ObjectOpenHashMap clientNPCSystemMap;
   private final Object2FloatOpenHashMap clientNPCStatusMap;
   private final ObjectArrayFIFOQueue clientNPCSystemToAdd;
   private boolean npcDebug;
   private float npcFleetSpeedLoaded;
   private boolean fow;
   private String npcDebugShopOwners;
   private final ConfigPool configPool;
   private boolean allowOldPowerSystem;
   private final Long2BooleanOpenHashMap modulesEnabledByDefault;
   private int spawnProtection;
   private RuleSetManager ruleManager;
   private final ObjectArrayFIFOQueue receivedRuleSetManagers;
   private int aiWeaponSwitchDelay;
   private boolean allowFactoriesOnShips;
   private boolean shipyardIgnoreStructure;

   public SendableGameState(StateInterface var1) {
      this.serverDescription = ServerConfig.SERVER_LIST_DESCRIPTION.getCurrentState().toString().substring(0, Math.min(ServerConfig.SERVER_LIST_DESCRIPTION.getCurrentState().toString().length(), 128));
      this.serverName = ServerConfig.SERVER_LIST_NAME.getCurrentState().toString().substring(0, Math.min(ServerConfig.SERVER_LIST_NAME.getCurrentState().toString().length(), 64));
      this.updateTime = System.currentTimeMillis();
      this.currentGameModeOutput = "";
      this.clientBattlemodeSettings = "";
      this.maxChainDocking = 25;
      this.ntInventoryAdds = new ObjectArrayFIFOQueue();
      this.ntInventoryRemoves = new ObjectArrayFIFOQueue();
      this.ntInventoryMultMods = new ObjectArrayFIFOQueue();
      this.clientNPCSystemMap = new Object2ObjectOpenHashMap();
      this.clientNPCStatusMap = new Object2FloatOpenHashMap();
      this.clientNPCSystemToAdd = new ObjectArrayFIFOQueue();
      this.npcDebugShopOwners = "";
      this.modulesEnabledByDefault = new Long2BooleanOpenHashMap();
      this.receivedRuleSetManagers = new ObjectArrayFIFOQueue();
      this.aiWeaponSwitchDelay = 1000;
      this.clientNPCStatusMap.defaultReturnValue(-2.0F);
      this.state = var1;
      this.onServer = var1 instanceof ServerStateInterface;
      this.imap = new InventoryMap();
      if (this.onServer) {
         this.setMaxGalaxySpeed((float)(Integer)ServerConfig.THRUST_SPEED_LIMIT.getCurrentState());
         this.sectorSize = ((GameServerState)var1).getSectorSizeWithoutMargin();
      }

      this.factionManager = new FactionManager(this);
      if (this.onServer) {
         ((GameServerState)var1).getController().getSectorListeners().add(this.factionManager);
      }

      this.configPool = new ConfigPool();

      try {
         this.configPool.readConfigFromFile(this.configPool.getPath(true));
      } catch (Exception var3) {
         throw new RuntimeException(var3);
      }

      this.catalogManager = new CatalogManager(this);
      this.raceManager = new RaceManager(this);
      this.tradeManager = new TradeManager(this.getState());
      this.manualTradeManager = new ManualTradeManager(this.getState());
      this.ruleManager = new RuleSetManager((GameStateInterface)this.getState());
      if (this.onServer) {
         this.ruleProperties = new RulePropertyContainer(this.ruleManager);
         this.ruleProperties.state = (GameStateInterface)this.getState();
      } else {
         this.ruleProperties = null;
      }

      if (this.onServer) {
         try {
            this.readServerMessage();
         } catch (IOException var2) {
            var2.printStackTrace();
            this.serverMessage = "";
         }

         this.ruleManager.initializeOnServer();
         this.getRuleProperties().initializeOnServer();
         this.modulesEnabledByDefault.put(-9223372036854775801L, (Boolean)ServerConfig.SHORT_RANGE_SCAN_DRIVE_ENABLED_BY_DEFAULT.getCurrentState());
         this.modulesEnabledByDefault.put(-9223372036854775802L, (Boolean)ServerConfig.JUMP_DRIVE_ENABLED_BY_DEFAULT.getCurrentState());
         this.linearDamping = (Float)ServerConfig.PHYSICS_LINEAR_DAMPING.getCurrentState();
         this.rotationalDamping = (Float)ServerConfig.PHYSICS_ROTATIONAL_DAMPING.getCurrentState();
         this.recipeBlockCost = (Integer)ServerConfig.RECIPE_BLOCK_COST.getCurrentState();
         this.maxChainDocking = (Integer)ServerConfig.MAX_CHAIN_DOCKING.getCurrentState();
         this.additiveProjectiles = ServerConfig.PROJECTILES_ADDITIVE_VELOCITY.isOn();
         this.dynamicPrices = ServerConfig.USE_DYNAMIC_RECIPE_PRICES.isOn();
         this.buyBBWithCredits = ServerConfig.BUY_BLUEPRINTS_WITH_CREDITS.isOn();
         this.relativeProjectiles = (Float)ServerConfig.PROJECTILES_VELOCITY_MULTIPLIER.getCurrentState();
         this.weaponRangeReference = (Float)ServerConfig.WEAPON_RANGE_REFERENCE.getCurrentState();
         this.ignoreDockingArea = ServerConfig.IGNORE_DOCKING_AREA.isOn();
         this.turnSpeedDivisor = (Float)ServerConfig.TURNING_DIMENSION_SCALE.getCurrentState();
         this.planetSizeMean = (Float)ServerConfig.PLANET_SIZE_MEAN_VALUE.getCurrentState();
         this.planetSizeDeviation = (Float)ServerConfig.PLANET_SIZE_DEVIATION_VALUE.getCurrentState();
         this.sectorsToExploreForSystemScan = (Integer)ServerConfig.SECTORS_TO_EXPLORE_FOR_SYS.getCurrentState();
         this.allowOldDockingBeam = (Boolean)ServerConfig.ALLOW_OLD_DOCKING_BEAM.getCurrentState();
         this.allowOldPowerSystem = (Boolean)ServerConfig.ALLOW_OLD_POWER_SYSTEM.getCurrentState();
         this.weightedCenterOfMass = (Boolean)ServerConfig.WEIGHTED_CENTER_OF_MASS.getCurrentState();
         this.shopRebootCostPerSecond = (Float)ServerConfig.SHOP_REBOOT_COST_PER_SECOND.getCurrentState();
         this.shopArmorRepairPerSecond = (Float)ServerConfig.SHOP_ARMOR_REPAIR_COST_PER_HITPOINT.getCurrentState();
         this.lockFactionShips = (Boolean)ServerConfig.LOCK_FACTION_SHIPS.getCurrentState();
         this.allowPersonalInvOverCap = (Boolean)ServerConfig.ALLOW_PERSONAL_INVENTORY_OVER_CAPACITY.getCurrentState();
         this.onlyAddFactionToFleet = (Boolean)ServerConfig.ONLY_ALLOW_FACTION_SHIPS_ADDED_TO_FLEET.getCurrentState();
         this.factionKickInactiveTimeLimit = (Integer)ServerConfig.FACTION_FOUNDER_KICKABLE_AFTER_DAYS_INACTIVITY.getCurrentState();
         this.aiWeaponSwitchDelay = (Integer)ServerConfig.AI_WEAPON_SWITCH_DELAY.getCurrentState();
         this.sunMinIntensityDamageRange = ((GameServerState)var1).getGameConfig().sunMinIntensityDamageRange;
         this.massLimitShip = ((GameServerState)var1).getGameConfig().massLimitShip;
         this.massLimitPlanet = ((GameServerState)var1).getGameConfig().massLimitPlanet;
         this.massLimitStation = ((GameServerState)var1).getGameConfig().massLimitStation;
         this.blockLimitShip = ((GameServerState)var1).getGameConfig().blockLimitShip;
         this.blockLimitPlanet = ((GameServerState)var1).getGameConfig().blockLimitPlanet;
         this.blockLimitStation = ((GameServerState)var1).getGameConfig().blockLimitStation;
         this.allowFactoriesOnShips = ServerConfig.ALLOW_FACTORY_ON_SHIPS.isOn();
         this.shipyardIgnoreStructure = ServerConfig.SHIPYARD_IGNORE_STRUCTURE.isOn();
         this.spawnProtection = (Integer)ServerConfig.SPAWN_PROTECTION.getCurrentState();
      }

   }

   public void announceMetaObject(MetaObject var1) {
      synchronized(this.metaObjectsToAnnounce) {
         this.metaObjectsToAnnounce.enqueue(var1);
      }
   }

   public void cleanUpOnEntityDelete() {
   }

   public void destroyPersistent() {
   }

   public NetworkGameState getNetworkObject() {
      return this.networkGameState;
   }

   public StateInterface getState() {
      return this.state;
   }

   public void initFromNetworkObject(NetworkObject var1) {
      this.setId(var1.id.get());
      this.getFactionManager().initFromNetworkObject((NetworkGameState)var1);
      this.getCatalogManager().initFromNetworkObject((NetworkGameState)var1);
      this.getRaceManager().initFromNetworkObject((NetworkGameState)var1);
      this.getTradeManager().initFromNetworkObject((NetworkGameState)var1);
      ((FleetStateInterface)this.state).getFleetManager().updateFromNetworkObject((NetworkGameState)var1);
      this.ruleManager.initFromNetworkObject((NetworkGameState)var1);
      if (!this.isOnServer()) {
         this.handleFromInventoryNT();
         this.readLeaderboard();
         this.setMaxGalaxySpeed((Float)this.getNetworkObject().serverMaxSpeed.get());
         this.clientBattlemodeSettings = (String)this.getNetworkObject().battlemodeInfo.get();
         this.linearDamping = (Float)this.getNetworkObject().linearDamping.get();
         this.spawnProtection = (Integer)this.getNetworkObject().spawnProtection.get();
         this.rotationalDamping = (Float)this.getNetworkObject().rotationalDamping.get();
         this.turnSpeedDivisor = (Float)this.getNetworkObject().turnSpeedDivisor.get();
         this.recipeBlockCost = (Integer)this.getNetworkObject().recipeBlockCost.get();
         this.maxChainDocking = (Integer)this.getNetworkObject().maxChainDocking.get();
         this.maxBuildArea = (Integer)this.getNetworkObject().maxBuildArea.get();
         this.currentGameModeOutput = (String)this.getNetworkObject().gameModeMessage.get();
         this.dynamicPrices = (Boolean)this.getNetworkObject().dynamicPrices.get();
         this.buyBBWithCredits = (Boolean)this.getNetworkObject().buyBBWIthCredits.get();
         this.additiveProjectiles = (Boolean)this.getNetworkObject().additiveProjectiles.get();
         this.relativeProjectiles = (Float)this.getNetworkObject().relativeProjectiles.get();
         this.weaponRangeReference = (Float)this.getNetworkObject().weaponRangeReference.get();
         this.ignoreDockingArea = (Boolean)this.getNetworkObject().ignoreDockingArea.get();
         this.setSectorSize((Float)this.getNetworkObject().sectorSize.get());
         this.sectorsToExploreForSystemScan = this.getNetworkObject().sectorsToExploreForSystemScan.getInt();
         this.stationCostClient = this.getNetworkObject().stationCost.getInt();
         this.planetSizeMean = this.getNetworkObject().planetSizeMean.getFloat();
         this.planetSizeDeviation = this.getNetworkObject().planetSizeDeviation.getFloat();
         this.allowOldDockingBeam = this.getNetworkObject().allowOldDockingBeam.getBoolean();
         this.allowOldPowerSystem = this.getNetworkObject().allowOldPowerSystem.getBoolean();
         this.weightedCenterOfMass = this.getNetworkObject().weightedCenterOfMass.getBoolean();
         this.npcDebug = this.getNetworkObject().npcDebug.getBoolean();
         this.fow = this.getNetworkObject().fow.getBoolean();
         this.npcFleetSpeedLoaded = this.getNetworkObject().npcFleetSpeedLoaded.getFloat();
         this.npcDebugShopOwners = (String)this.getNetworkObject().npcShopOwnersDebug.get();
         this.shopRebootCostPerSecond = this.getNetworkObject().shopRebootCostPerSecond.getFloat();
         this.shopArmorRepairPerSecond = this.getNetworkObject().shopArmorRepairPerSecond.getFloat();
         this.lockFactionShips = this.getNetworkObject().lockFactionShips.getBoolean();
         this.allowPersonalInvOverCap = this.getNetworkObject().allowPersonalInvOverCap.getBoolean();
         this.onlyAddFactionToFleet = this.getNetworkObject().onlyAddFactionToFleet.getBoolean();
         this.allowFactoriesOnShips = this.getNetworkObject().allowFactoriesOnShip.getBoolean();
         this.shipyardIgnoreStructure = this.getNetworkObject().shipyardIgnoreStructure.getBoolean();
         this.factionKickInactiveTimeLimit = this.getNetworkObject().factionKickInactiveTimeLimit.getInt();
         this.aiWeaponSwitchDelay = this.getNetworkObject().aiWeaponSwitchDelay.getInt();
         this.massLimitShip = (double)this.getNetworkObject().massLimitShip.getFloat();
         this.massLimitPlanet = (double)this.getNetworkObject().massLimitPlanet.getFloat();
         this.massLimitStation = (double)this.getNetworkObject().massLimitStation.getFloat();
         this.blockLimitShip = this.getNetworkObject().blockLimitShip.getInt();
         this.blockLimitPlanet = this.getNetworkObject().blockLimitPlanet.getInt();
         this.blockLimitStation = this.getNetworkObject().blockLimitStation.getInt();

         int var6;
         for(var6 = 0; var6 < this.networkGameState.ruleSetManagerBuffer.getReceiveBuffer().size(); ++var6) {
            RuleSetManager var2 = (RuleSetManager)((RemoteRuleSetManager)this.networkGameState.ruleSetManagerBuffer.getReceiveBuffer().get(var6)).get();
            this.receivedRuleSetManagers.enqueue(var2);
            synchronized(this.getState()) {
               Iterator var3 = this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

               while(var3.hasNext()) {
                  Sendable var4;
                  if ((var4 = (Sendable)var3.next()) instanceof RuleEntityContainer) {
                     ((RuleEntityContainer)var4).getRuleEntityManager().flagRuleChangePrepare();
                  }
               }
            }
         }

         for(var6 = 0; var6 < this.getNetworkObject().modulesEnabledByDefault.getReceiveBuffer().size(); ++var6) {
            LongBooleanPair var7 = (LongBooleanPair)((RemoteLongBoolean)this.getNetworkObject().modulesEnabledByDefault.getReceiveBuffer().get(var6)).get();
            this.modulesEnabledByDefault.put(var7.l, var7.b);
         }

         for(var6 = 0; var6 < this.getNetworkObject().npcSystemBuffer.getReceiveBuffer().size(); ++var6) {
            NPCSystemStub var8 = (NPCSystemStub)((RemoteNPCSystem)this.getNetworkObject().npcSystemBuffer.getReceiveBuffer().get(var6)).get();
            this.clientNPCSystemToAdd.enqueue(var8);
         }
      }

   }

   public void initialize() {
   }

   public boolean isMarkedForDeleteVolatile() {
      return this.markedForDeleteVolatile;
   }

   public void setMarkedForDeleteVolatile(boolean var1) {
      this.markedForDeleteVolatile = var1;
   }

   public boolean isMarkedForDeleteVolatileSent() {
      return this.markedForDeleteVolatileSent;
   }

   public void setMarkedForDeleteVolatileSent(boolean var1) {
      this.markedForDeleteVolatileSent = var1;
   }

   public boolean isMarkedForPermanentDelete() {
      return false;
   }

   public boolean isOkToAdd() {
      return true;
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public boolean isUpdatable() {
      return true;
   }

   public void markForPermanentDelete(boolean var1) {
   }

   public void newNetworkObject() {
      this.networkGameState = new NetworkGameState(this, this.getState());
   }

   private void updateLocalInventory() {
      Inventory var2;
      if (!this.ntInventoryAdds.isEmpty()) {
         synchronized(this.ntInventoryAdds) {
            while(!this.ntInventoryAdds.isEmpty()) {
               var2 = (Inventory)this.ntInventoryAdds.dequeue();

               assert var2.getParameterIndex() != 0L;

               if (!this.isOnServer()) {
                  Iterator var3 = this.getInventories().inventoriesList.iterator();

                  while(var3.hasNext()) {
                     Inventory var4 = (Inventory)var3.next();

                     assert var4 != null;
                  }
               }

               assert var2.getParameterIndex() != Long.MIN_VALUE;

               this.getInventories().put(var2.getParameterIndex(), var2);
               Faction var9;
               if ((var9 = this.getFactionManager().getFaction((int)var2.getParameterIndex())) != null) {
                  ((NPCFaction)var9).setInventory((NPCFactionInventory)var2);
               }

               if (!this.isOnServer()) {
                  var2.requestMissingMetaObjects();
               }
            }
         }
      }

      if (!this.ntInventoryRemoves.isEmpty()) {
         synchronized(this.ntInventoryRemoves) {
            while(!this.ntInventoryRemoves.isEmpty()) {
               var2 = (Inventory)this.ntInventoryRemoves.dequeue();
               var2 = this.getInventories().remove(var2.getParameterIndex());
               if (!this.isOnServer() && var2 != null) {
                  this.volumeChanged(var2.getVolume(), 0.0D);
                  if (var2 instanceof StashInventory && ((StashInventory)var2).getCustomName() != null) {
                     ((StashInventory)var2).getCustomName().length();
                  }
               }
            }
         }
      }

      if (!this.ntInventoryMultMods.isEmpty()) {
         synchronized(this.ntInventoryMultMods) {
            while(!this.ntInventoryMultMods.isEmpty()) {
               InventoryMultMod var8 = (InventoryMultMod)this.ntInventoryMultMods.dequeue();

               assert var8.parameter != Long.MIN_VALUE : var8;

               Inventory var10;
               if ((var10 = this.getInventory(var8.parameter)) != null) {
                  assert var10.getInventoryHolder() != null : var10;

                  if (this.isOnServer()) {
                     assert false : "Client should not send modifications to faction inventory";
                  } else {
                     var10.handleReceived(var8, this.getNetworkObject());
                  }
               } else {
                  assert false : var10;
               }
            }

         }
      }
   }

   private void handleFromInventoryNT() {
      ObjectArrayList var1 = this.getNetworkObject().getInventoriesChangeBuffer().getReceiveBuffer();

      int var2;
      for(var2 = 0; var2 < var1.size(); ++var2) {
         Inventory var3 = (Inventory)((RemoteInventory)var1.get(var2)).get();
         if (((RemoteInventory)var1.get(var2)).isAdd()) {
            synchronized(this.ntInventoryAdds) {
               this.ntInventoryAdds.enqueue(var3);
            }
         } else {
            synchronized(this.ntInventoryRemoves) {
               this.ntInventoryRemoves.enqueue(var3);
            }
         }
      }

      var1 = this.getNetworkObject().getInventoryMultModBuffer().getReceiveBuffer();

      for(var2 = 0; var2 < var1.size(); ++var2) {
         RemoteInventoryMultMod var8 = (RemoteInventoryMultMod)var1.get(var2);
         synchronized(this.ntInventoryMultMods) {
            this.ntInventoryMultMods.enqueue(var8.get());
         }
      }

   }

   public void updateFromNetworkObject(NetworkObject var1, int var2) {
      ((FleetStateInterface)this.state).getFleetManager().updateFromNetworkObject((NetworkGameState)var1);
      this.getTradeManager().updateFromNetworkObject((NetworkGameState)var1);

      for(var2 = 0; var2 < this.networkGameState.ruleSetManagerBuffer.getReceiveBuffer().size(); ++var2) {
         RuleSetManager var3 = (RuleSetManager)((RemoteRuleSetManager)this.networkGameState.ruleSetManagerBuffer.getReceiveBuffer().get(var2)).get();
         this.receivedRuleSetManagers.enqueue(var3);
         synchronized(this.getState()) {
            Iterator var4 = this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

            while(var4.hasNext()) {
               Sendable var5;
               if ((var5 = (Sendable)var4.next()) instanceof RuleEntityContainer) {
                  ((RuleEntityContainer)var5).getRuleEntityManager().flagRuleChangePrepare();
               }
            }
         }
      }

      if (this.ruleProperties != null) {
         this.ruleProperties.updateFromNetworkObject((NetworkGameState)var1);
      }

      Iterator var8 = this.getFactionManager().getFactionCollection().iterator();

      while(var8.hasNext()) {
         ((Faction)var8.next()).getRuleEntityManager().receive((NetworkGameState)var1);
      }

      if (!this.onServer) {
         this.handleFromInventoryNT();
         this.readLeaderboard();
         this.clientBattlemodeSettings = (String)this.getNetworkObject().battlemodeInfo.get();
         ServerConfig.deserialize((NetworkGameState)var1);
         this.aiWeaponSwitchDelay = this.getNetworkObject().aiWeaponSwitchDelay.get();
         this.ignoreDockingArea = (Boolean)this.getNetworkObject().ignoreDockingArea.get();
         this.setSectorSize((Float)this.getNetworkObject().sectorSize.get());
         this.npcDebug = this.getNetworkObject().npcDebug.getBoolean();
         this.fow = this.getNetworkObject().fow.getBoolean();
         this.npcFleetSpeedLoaded = this.getNetworkObject().npcFleetSpeedLoaded.getFloat();
         this.npcDebugShopOwners = (String)this.getNetworkObject().npcShopOwnersDebug.get();

         for(var2 = 0; var2 < this.getNetworkObject().frozenSectorRequests.getReceiveBuffer().size(); ++var2) {
            int var9;
            if ((var9 = this.getNetworkObject().frozenSectorRequests.getReceiveBuffer().getInt(var2)) > 0) {
               this.frozenSectors.add(var9);
            } else {
               this.frozenSectors.remove(Math.abs(var9));
            }
         }

         for(var2 = 0; var2 < this.getNetworkObject().npcSystemBuffer.getReceiveBuffer().size(); ++var2) {
            NPCSystemStub var10;
            if ((var10 = (NPCSystemStub)((RemoteNPCSystem)this.getNetworkObject().npcSystemBuffer.getReceiveBuffer().get(var2)).get()).abandoned) {
               this.getClientNPCSystemMap().remove(var10.system);
            } else {
               this.getClientNPCSystemMap().put(var10.system, var10);
            }
         }

         this.currentGameModeOutput = (String)this.getNetworkObject().gameModeMessage.get();

         for(var2 = 0; var2 < this.getNetworkObject().deployBlockBehaviorChecksum.getReceiveBuffer().size(); ++var2) {
            RemoteStringArray var11 = (RemoteStringArray)this.getNetworkObject().deployBlockBehaviorChecksum.getReceiveBuffer().get(var2);
            ((GameClientState)this.getState()).setBlockBehaviorCheckSum((String)var11.get(var2).get());

            try {
               byte[] var12 = ((String)var11.get(1).get()).getBytes("UTF-8");
               String var13 = ClientStatics.ENTITY_DATABASE_PATH + ((GameClientState)this.getState()).getController().getConnection().getHost() + "-block-behavior.xml";
               BufferedOutputStream var15;
               (var15 = new BufferedOutputStream(new FileOutputStream(var13))).write(var12);
               var15.flush();
               var15.close();
               System.err.println("[CLIENT] received deployed block behavior from server: " + var13);
               ((GameClientState)this.getState()).getController().parseBlockBehavior(var13);
               ((GameClientState)this.getState()).getController().reapplyBlockBehavior();
            } catch (Exception var6) {
               var6.printStackTrace();
               GLFrame.processErrorDialogExceptionWithoutReport(var6, this.getState());
            }
         }

         for(var2 = 0; var2 < this.getNetworkObject().metaObjectStateLessBuffer.getReceiveBuffer().size(); ++var2) {
            RemoteMetaObjectStateLess var14 = (RemoteMetaObjectStateLess)this.getNetworkObject().metaObjectStateLessBuffer.getReceiveBuffer().get(var2);
            ((MetaObjectState)this.getState()).getMetaObjectManager().receivedAnnoucedMetaObject((MetaObject)var14.get());
         }
      }

      if (!this.getNetworkObject().debugPhysical.getReceiveBuffer().isEmpty()) {
         for(var2 = 0; var2 < this.getNetworkObject().debugPhysical.getReceiveBuffer().size(); ++var2) {
            this.debugObjects.enqueue(((RemoteSerializableObject)this.getNetworkObject().debugPhysical.getReceiveBuffer().get(var2)).get());
         }
      }

      this.getFactionManager().updateFromNetworkObject(this.getNetworkObject());
      this.getCatalogManager().updateFromNetworkObject(this.getNetworkObject());
      this.getRaceManager().updateFromNetworkObject(this.getNetworkObject());
   }

   public void updateLocal(Timer var1) {
      this.updateTime = System.currentTimeMillis();
      this.getFactionManager().updateLocal(var1);
      this.getCatalogManager().updateLocal();
      this.getTradeManager().updateLocal(var1);
      if (this.isOnServer() && this.isServerDeployedBlockBehavior()) {
         RemoteStringArray var2;
         (var2 = new RemoteStringArray(2, this.isOnServer())).set(0, (String)((GameServerState)this.state).getBlockBehaviorChecksum());
         var2.set(1, (String)(new String(((GameServerState)this.state).getBlockBehaviorFile())));
         this.getNetworkObject().deployBlockBehaviorChecksum.add((RemoteArray)var2);
         ((GameServerState)this.getState()).getController().broadcastMessage(new Object[]{415}, 1);
         this.setServerDeployedBlockBehavior(false);
      }

      this.updateLocalInventory();

      for(; !this.receivedRuleSetManagers.isEmpty(); this.getRuleManager().flagChanged(this.getState())) {
         System.err.println(this.getState() + " RECEIVED RULESET MANAGER. APPLYING");
         RuleSetManager var8 = (RuleSetManager)this.receivedRuleSetManagers.dequeue();
         this.ruleManager = var8;
         this.ruleManager.setState((GameStateInterface)this.getState());
         if (this.ruleManager.receivedFullRuleChange != null) {
            this.ruleProperties = this.ruleManager.receivedFullRuleChange;
            this.ruleProperties.state = (GameStateInterface)this.getState();
            this.ruleManager.receivedFullRuleChange = null;
            if (this.isOnServer()) {
               try {
                  System.err.println("[SERVER][RULES] Received ruleset and property change. Saving to disk.");
                  this.ruleManager.writeToDisk();
                  this.ruleProperties.saveToDisk(RulePropertyContainer.getPropertiesPath());
               } catch (ParserConfigurationException var5) {
                  var5.printStackTrace();
               } catch (TransformerException var6) {
                  var6.printStackTrace();
               } catch (IOException var7) {
                  var7.printStackTrace();
               }

               System.err.println("[SERVER][RULES] Delegating new ruleset to clients.");
               this.ruleManager.includePropertiesInSendAndSaveOnServer = true;
               this.networkGameState.ruleSetManagerBuffer.add(new RemoteRuleSetManager(this.ruleManager, this.getNetworkObject()));
            }

            this.ruleManager.receivedInitialOnClient = true;
         }
      }

      if (this.ruleProperties != null) {
         this.ruleProperties.update();
      } else {
         System.err.println("No rule properties yet " + this.isOnServer());
      }

      this.manualTradeManager.updateLocal(var1);
      if (this.leaderBoardChanged) {
         this.getClientDeadCount().clear();
         Iterator var9 = this.clientLeaderboard.entrySet().iterator();

         while(var9.hasNext()) {
            Iterator var3 = ((ObjectArrayList)((Entry)var9.next()).getValue()).iterator();

            while(var3.hasNext()) {
               KillerEntity var4 = (KillerEntity)var3.next();
               this.getClientDeadCount().put(var4.deadPlayerName, this.getClientDeadCount().getInt(var4.deadPlayerName) + 1);
            }
         }

         if (this.leaderboardGUI != null) {
            this.leaderboardGUI.updateEntries();
         }

         this.leaderBoardChanged = false;
      }

      while(!this.clientNPCSystemToAdd.isEmpty()) {
         NPCSystemStub var10 = (NPCSystemStub)this.clientNPCSystemToAdd.dequeue();
         this.getClientNPCSystemMap().put(var10.system, var10);
      }

      if (this.isOnServer() && !this.metaObjectsToAnnounce.isEmpty()) {
         while(!this.metaObjectsToAnnounce.isEmpty()) {
            this.getNetworkObject().metaObjectStateLessBuffer.add(new RemoteMetaObjectStateLess((MetaObject)this.metaObjectsToAnnounce.dequeue(), this.getNetworkObject()));
         }
      }

      if (!this.isOnServer() && !this.debugObjects.isEmpty()) {
         while(!this.debugObjects.isEmpty()) {
            ((DebugServerObject)this.debugObjects.dequeue()).draw((GameClientState)this.getState());
         }
      }

      if (!this.isOnServer()) {
         if (this.currentGameModeOutput.length() > 0) {
            this.lastClientShownGameModeMessage = this.currentGameModeOutput;
            ((GameClientState)this.getState()).getController().showBigTitleMessage("gameMode", this.currentGameModeOutput, 0.0F);
         } else if (this.lastClientShownGameModeMessage != null) {
            ((GameClientState)this.getState()).getController().timeOutBigTitleMessage("gameMode");
         }
      }

      if (this.isOnServer()) {
         this.raceManager.updateServer(var1);
      } else {
         this.raceManager.updateClient(var1);
      }
   }

   public void updateToFullNetworkObject() {
      this.getNetworkObject().id.set(this.getId());

      assert this.serverMessage != null;

      this.getNetworkObject().serverMessage.set(this.serverMessage);
      ((FleetStateInterface)this.state).getFleetManager().updateToFullNetworkObject(this.getNetworkObject());
      this.getTradeManager().updateToFullNetworkObject(this.getNetworkObject());
      this.ruleManager.includePropertiesInSendAndSaveOnServer = true;
      this.ruleManager.updateToFullNetworkObject(this.getNetworkObject());
      if (this.onServer) {
         ServerConfig.serialize(this.getNetworkObject());
         Iterator var1 = this.getGameModes().iterator();

         while(var1.hasNext()) {
            ((AbstractGameMode)var1.next()).updateToFullNT(this.getNetworkObject());
         }

         var1 = this.frozenSectors.iterator();

         while(var1.hasNext()) {
            int var2 = (Integer)var1.next();
            this.getNetworkObject().frozenSectorRequests.add(var2);
         }

         var1 = this.imap.inventoriesList.iterator();

         while(var1.hasNext()) {
            ((Inventory)var1.next()).sendAll();
         }

         if (ServerConfig.NPC_DEBUG_MODE.isOn()) {
            this.getFactionManager().checkNPCFactionSendingDebug(true);
         }

         this.getNetworkObject().manCalcCancelOn.set(ServerConfig.MANAGER_CALC_CANCEL_ON.isOn());
         this.getNetworkObject().maxBuildArea.set((Integer)ServerConfig.PLAYER_MAX_BUILD_AREA.getCurrentState());
         this.getNetworkObject().saveSlotsAllowed.set((Integer)ServerConfig.CATALOG_SLOTS_PER_PLAYER.getCurrentState());
         this.getNetworkObject().buyBBWIthCredits.set(this.buyBBWithCredits);
         this.getNetworkObject().serverMaxSpeed.set(this.getMaxGalaxySpeed());
         this.getNetworkObject().linearDamping.set(this.linearDamping);
         this.getNetworkObject().spawnProtection.set(this.spawnProtection);
         this.getNetworkObject().gameModeMessage.set(this.currentGameModeOutput);
         this.getNetworkObject().recipeBlockCost.set(this.recipeBlockCost);
         this.getNetworkObject().maxChainDocking.set(this.maxChainDocking);
         this.getNetworkObject().rotationalDamping.set(this.rotationalDamping);
         this.getNetworkObject().turnSpeedDivisor.set(this.turnSpeedDivisor);
         this.getNetworkObject().sectorSize.set(this.sectorSize);
         this.getNetworkObject().additiveProjectiles.set(this.additiveProjectiles);
         this.getNetworkObject().dynamicPrices.set(this.dynamicPrices);
         this.getNetworkObject().relativeProjectiles.set(this.relativeProjectiles);
         this.getNetworkObject().weaponRangeReference.set(this.weaponRangeReference);
         this.getNetworkObject().ignoreDockingArea.set(this.ignoreDockingArea);
         this.getNetworkObject().seed.set(this.getUniverseSeed());
         this.getNetworkObject().serverStartTime.set(((GameServerState)this.getState()).getServerStartTime());
         this.getNetworkObject().universeDayDuration.set((long)(Integer)ServerConfig.UNIVERSE_DAY_IN_MS.getCurrentState());
         this.getNetworkObject().stationCost.set(this.getStationCost());
         this.getNetworkObject().planetSizeMean.set(this.planetSizeMean);
         this.getNetworkObject().sectorsToExploreForSystemScan.set(this.sectorsToExploreForSystemScan);
         this.getNetworkObject().planetSizeDeviation.set(this.planetSizeDeviation);
         this.getNetworkObject().allowOldDockingBeam.set(this.allowOldDockingBeam);
         this.getNetworkObject().allowOldPowerSystem.set(this.allowOldPowerSystem);
         this.getNetworkObject().weightedCenterOfMass.set(this.weightedCenterOfMass);
         this.getNetworkObject().npcDebug.set(this.isNpcDebug());
         this.getNetworkObject().fow.set(this.isFow());
         this.getNetworkObject().npcFleetSpeedLoaded.set(this.getNPCFleetSpeedLoaded());
         this.getNetworkObject().npcShopOwnersDebug.set(this.getNPCShopOwnersDebug());
         this.getNetworkObject().shopRebootCostPerSecond.set(this.shopRebootCostPerSecond);
         this.getNetworkObject().shopArmorRepairPerSecond.set(this.shopArmorRepairPerSecond);
         this.getNetworkObject().lockFactionShips.set(this.lockFactionShips);
         this.getNetworkObject().allowPersonalInvOverCap.set(this.allowPersonalInvOverCap);
         this.getNetworkObject().onlyAddFactionToFleet.set(this.onlyAddFactionToFleet);
         this.getNetworkObject().allowFactoriesOnShip.set(this.allowFactoriesOnShips);
         this.getNetworkObject().shipyardIgnoreStructure.set(this.shipyardIgnoreStructure);
         this.getNetworkObject().factionKickInactiveTimeLimit.set(this.factionKickInactiveTimeLimit);
         this.getNetworkObject().aiWeaponSwitchDelay.set(this.aiWeaponSwitchDelay);
         this.getNetworkObject().massLimitShip.set((float)this.massLimitShip);
         this.getNetworkObject().massLimitPlanet.set((float)this.massLimitPlanet);
         this.getNetworkObject().massLimitStation.set((float)this.massLimitStation);
         this.getNetworkObject().blockLimitShip.set(this.blockLimitShip);
         this.getNetworkObject().blockLimitPlanet.set(this.blockLimitPlanet);
         this.getNetworkObject().blockLimitStation.set(this.blockLimitStation);
         var1 = this.modulesEnabledByDefault.long2BooleanEntrySet().iterator();

         while(var1.hasNext()) {
            it.unimi.dsi.fastutil.longs.Long2BooleanMap.Entry var3 = (it.unimi.dsi.fastutil.longs.Long2BooleanMap.Entry)var1.next();
            this.getNetworkObject().modulesEnabledByDefault.add(new RemoteLongBoolean(new LongBooleanPair(var3.getLongKey(), var3.getBooleanValue()), this.getNetworkObject()));
         }
      }

      this.getFactionManager().updateToFullNetworkObject(this.getNetworkObject());
      this.getCatalogManager().updateToFullNetworkObject(this.getNetworkObject());
      this.getRaceManager().updateToFullNetworkObject(this.getNetworkObject());
   }

   public void setWeaponRangeReference(float var1) throws IOException {
      ServerConfig.WEAPON_RANGE_REFERENCE.setCurrentState(var1);
      ServerConfig.write();
      this.weaponRangeReference = var1;
      this.getNetworkObject().weaponRangeReference.set(this.weaponRangeReference);
   }

   public void updateToNetworkObject() {
      this.getNetworkObject().id.set(this.getId());
      this.getTradeManager().updateToNetworkObject(this.getNetworkObject());
      this.ruleManager.updateToNetworkObject(this.getNetworkObject());
      if (this.onServer) {
         this.getNetworkObject().serverModTime.set(((GameServerState)this.getState()).getServerTimeMod());
         this.getNetworkObject().ignoreDockingArea.set(this.ignoreDockingArea);
         this.getNetworkObject().sectorSize.set(this.sectorSize);
         this.getNetworkObject().npcDebug.set(this.isNpcDebug());
         this.getNetworkObject().fow.set(this.isFow());
         this.getNetworkObject().npcFleetSpeedLoaded.set(this.getNPCFleetSpeedLoaded());
         this.getNetworkObject().npcShopOwnersDebug.set(this.getNPCShopOwnersDebug());
         this.getNetworkObject().aiWeaponSwitchDelay.set(this.aiWeaponSwitchDelay);
      }

      this.getFactionManager().updateToNetworkObject(this.getNetworkObject());
      this.getCatalogManager().updateToNetworkObject(this.getNetworkObject());
      this.getRaceManager().updateToNetworkObject(this.getNetworkObject());
   }

   public boolean isWrittenForUnload() {
      return this.writtenForUnload;
   }

   public void setWrittenForUnload(boolean var1) {
      this.writtenForUnload = var1;
   }

   public CatalogManager getCatalogManager() {
      return this.catalogManager;
   }

   public FactionManager getFactionManager() {
      return this.factionManager;
   }

   public int getId() {
      return this.id;
   }

   public void setId(int var1) {
      this.id = var1;
   }

   public float getLinearDamping() {
      return this.linearDamping;
   }

   public float getMaxGalaxySpeed() {
      return this.maxGalaxySpeed;
   }

   public void setMaxGalaxySpeed(float var1) {
      this.maxGalaxySpeed = var1;
   }

   public Integer getRecipeBlockCost() {
      return this.recipeBlockCost;
   }

   public float getRotationalDamping() {
      return this.rotationalDamping;
   }

   public String getServerDescription() {
      return this.serverDescription;
   }

   public String getServerName() {
      return this.serverName;
   }

   public float getTurnSpeedDivisor() {
      return this.turnSpeedDivisor;
   }

   public float getPlanetSizeMean() {
      return this.planetSizeMean;
   }

   public float getPlanetSizeDeviation() {
      return this.planetSizeDeviation;
   }

   public boolean getLockFactionShips() {
      return this.lockFactionShips;
   }

   public boolean isAdditiveProjectiles() {
      return this.additiveProjectiles;
   }

   public boolean isIgnoreDockingArea() {
      return this.ignoreDockingArea;
   }

   public void setIgnoreDockingArea(Boolean var1) {
      this.ignoreDockingArea = var1;
   }

   public float isRelativeProjectiles() {
      return this.relativeProjectiles;
   }

   public void readServerMessage() throws IOException {
      FileExt var1;
      if (!(var1 = new FileExt("./server-message.txt")).exists()) {
         var1.createNewFile();
         this.serverMessage = "";
      } else {
         BufferedReader var4 = new BufferedReader(new FileReader(var1));
         String var2 = null;
         StringBuffer var3 = new StringBuffer();

         while((var2 = var4.readLine()) != null) {
            var3.append(var2 + "\n");
         }

         var4.close();
         this.serverMessage = var3.toString();
      }
   }

   public String toString() {
      return "SenableGameState(" + this.id + ")";
   }

   private void readLeaderboard() {
      if (this.getNetworkObject().leaderBoardBuffer.getReceiveBuffer().size() > 0) {
         this.clientLeaderboard.clear();
         this.leaderBoardChanged = true;
      }

      for(int var1 = 0; var1 < this.getNetworkObject().leaderBoardBuffer.getReceiveBuffer().size(); ++var1) {
         this.clientLeaderboard.putAll((Map)((RemoteLeaderboard)this.getNetworkObject().leaderBoardBuffer.getReceiveBuffer().get(var1)).get());
      }

   }

   public long getUniverseSeed() {
      return this.isOnServer() ? ((GameServerState)this.getState()).getUniverse().getSeed() : this.getNetworkObject().seed.getLong();
   }

   public boolean isServerDeployedBlockBehavior() {
      return this.serverDeployedBlockBehavior;
   }

   public void setServerDeployedBlockBehavior(boolean var1) {
      this.serverDeployedBlockBehavior = var1;
   }

   public float getRotationProgession() {
      if (this.state.getNumberOfUpdate() != this.lastUpdateRotationProg) {
         float var1 = 0.0F;
         if (this.state.getController().getUniverseDayInMs() > 0L) {
            var1 = (float)((double)((this.updateTime - this.state.getController().calculateStartTime()) % this.state.getController().getUniverseDayInMs()) / (double)this.state.getController().getUniverseDayInMs());
         }

         this.lastUpdateRotationProg = this.state.getNumberOfUpdate();
         this.cachedYear = var1;
      }

      return this.cachedYear;
   }

   public float getSectorSize() {
      return this.sectorSize;
   }

   public float getWeaponRangeReference() {
      return this.weaponRangeReference;
   }

   public void setSectorSize(float var1) {
      this.sectorSize = var1;
      if (!this.isOnServer()) {
         this.getNetworkObject().sectorSize.set(var1);
      }

   }

   public int getMaxBuildArea() {
      return this.maxBuildArea;
   }

   public ObjectArrayList getGameModes() {
      return this.gameModes;
   }

   public String getCurrentGameModeOutput() {
      return this.currentGameModeOutput;
   }

   public void setCurrentGameModeOutput(String var1) {
      this.currentGameModeOutput = var1;
   }

   public IntOpenHashSet getFrozenSectors() {
      return this.frozenSectors;
   }

   public void serverRequestFrosenSector(int var1, boolean var2) {
      synchronized(this.frozenSectors) {
         if (var2) {
            if (this.frozenSectors.add(var1)) {
               this.getNetworkObject().frozenSectorRequests.add(var1);
            }
         } else if (this.frozenSectors.remove(var1)) {
            this.getNetworkObject().frozenSectorRequests.add(-var1);
         }

      }
   }

   public boolean allowedToSpawnBBShips(PlayerState var1, Faction var2) {
      for(int var3 = 0; var3 < this.getGameModes().size(); ++var3) {
         if (!((AbstractGameMode)this.getGameModes().get(var3)).allowedToSpawnBBShips(var1, var2)) {
            return false;
         }
      }

      return true;
   }

   public void announceKill(PlayerState var1, int var2) {
      for(int var3 = 0; var3 < this.getGameModes().size(); ++var3) {
         ((AbstractGameMode)this.getGameModes().get(var3)).announceKill(var1, var2);
      }

   }

   public GUIElement getLoaderBoardGUI() {
      return new GUIScrollableLoaderboardList((ClientState)this.state, 820, 402, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
         }

         public boolean isOccluded() {
            return false;
         }
      });
   }

   public Object2ObjectOpenHashMap getClientLeaderboard() {
      return this.clientLeaderboard;
   }

   public void setClientLeaderboard(Object2ObjectOpenHashMap var1) {
      this.clientLeaderboard = var1;
   }

   public String getClientBattlemodeSettings() {
      return this.clientBattlemodeSettings.length() > 0 ? "BATTLE MODE\nCurrent Settings and Status: \n" + this.clientBattlemodeSettings + "\n\n\n" + this.battlemodeDesc : this.clientBattlemodeSettings;
   }

   public Object2IntOpenHashMap getClientDeadCount() {
      return this.clientDeadCount;
   }

   public void setClientDeadCount(Object2IntOpenHashMap var1) {
      this.clientDeadCount = var1;
   }

   public boolean isDynamicPrices() {
      return this.dynamicPrices;
   }

   public void onFactionChangedServer(PlayerState var1, FactionChange var2) {
      Iterator var3 = this.getGameModes().iterator();

      while(var3.hasNext()) {
         ((AbstractGameMode)var3.next()).onFactionChanged(var1, var2);
      }

   }

   public void sendGalaxyModToClients(StellarSystem var1, Vector3i var2) {
      assert this.isOnServer();

      ((GameServerState)this.getState()).getUniverse().getGalaxyManager().markZoneDirty(var1);
      Iterator var3 = ((GameServerState)this.getState()).getClients().values().iterator();

      while(var3.hasNext()) {
         RegisteredClientOnServer var4 = (RegisteredClientOnServer)var3.next();
         GalaxyRequestAndAwnser var5;
         (var5 = new GalaxyRequestAndAwnser()).factionUID = var1.getOwnerFaction();
         var5.ownerUID = var1.getOwnerUID();
         var5.secX = var2.x;
         var5.secY = var2.y;
         var5.secZ = var2.z;
         Sendable var6;
         if ((var6 = (Sendable)var4.getLocalAndRemoteObjectContainer().getLocalObjects().get(0)) != null) {
            assert var6 != null;

            var5.networkObjectOnServer = ((ClientChannel)var6).getNetworkObject();
            var5.networkObjectOnServer.galaxyServerMods.add(new RemoteGalaxyRequest(var5, true));
         } else {
            System.err.println("[SENDABLEGAMESTATE] WARNING: Cannot send galaxy mod to " + var4 + ": no client channel!");
         }
      }

   }

   public void sendGalaxyModToClients(int var1, String var2, Vector3i var3) {
      assert this.isOnServer();

      Vector3i var4 = VoidSystem.getPosFromSector(var3, new Vector3i());
      ((GameServerState)this.getState()).getUniverse().getGalaxyManager().markZoneDirty(var4);
      Iterator var8 = ((GameServerState)this.getState()).getClients().values().iterator();

      while(var8.hasNext()) {
         RegisteredClientOnServer var5 = (RegisteredClientOnServer)var8.next();
         GalaxyRequestAndAwnser var6;
         (var6 = new GalaxyRequestAndAwnser()).factionUID = var1;
         var6.ownerUID = var2;
         var6.secX = var3.x;
         var6.secY = var3.y;
         var6.secZ = var3.z;
         Sendable var7;
         if ((var7 = (Sendable)var5.getLocalAndRemoteObjectContainer().getLocalObjects().get(0)) != null) {
            assert var7 != null;

            var6.networkObjectOnServer = ((ClientChannel)var7).getNetworkObject();
            var6.networkObjectOnServer.galaxyServerMods.add(new RemoteGalaxyRequest(var6, true));
         } else {
            System.err.println("[SENDABLEGAMESTATE] WARNING: Cannot send galaxy mod to " + var5 + ": no client channel!");
         }
      }

   }

   public boolean isBuyBBWithCredits() {
      return this.buyBBWithCredits;
   }

   public int getStationCost() {
      return this.isOnServer() ? (Integer)ServerConfig.STATION_CREDIT_COST.getCurrentState() : this.stationCostClient;
   }

   public boolean isAllowOldDockingBeam() {
      return this.allowOldDockingBeam;
   }

   public boolean isAllowOldPowerSystem() {
      return this.allowOldPowerSystem;
   }

   public boolean isWeightedCenterOfMass() {
      return this.weightedCenterOfMass;
   }

   public float getShopRebootCostPerSecond() {
      return this.shopRebootCostPerSecond;
   }

   public float getShopArmorRepairPerSecond() {
      return this.shopArmorRepairPerSecond;
   }

   public int getFactionKickInactiveTimeLimitDays() {
      return this.factionKickInactiveTimeLimit;
   }

   public long getFactionKickInactiveTimeLimitMs() {
      return (long)this.factionKickInactiveTimeLimit * 1000L * 60L * 60L * 24L;
   }

   public void announceLag(long var1) {
   }

   public long getCurrentLag() {
      return 0L;
   }

   public boolean isMassOk(SegmentController var1, double var2) {
      double var4;
      return (var4 = this.getMassLimit(var1)) <= 0.0D || var2 <= var4;
   }

   public boolean isBlocksOk(SegmentController var1, int var2) {
      int var3;
      return (var3 = this.getBlockLimit(var1)) <= 0 || var2 <= var3;
   }

   public double getMassLimit(SegmentController var1) {
      if (var1 instanceof Ship) {
         return this.massLimitShip;
      } else if (var1 instanceof Planet) {
         return this.massLimitPlanet;
      } else {
         return var1 instanceof SpaceStation ? this.massLimitStation : 0.0D;
      }
   }

   public int getBlockLimit(SegmentController var1) {
      if (var1 instanceof Ship) {
         return this.blockLimitShip;
      } else if (var1 instanceof Planet) {
         return this.blockLimitPlanet;
      } else {
         return var1 instanceof SpaceStation ? this.blockLimitStation : 0;
      }
   }

   public RaceManager getRaceManager() {
      return this.raceManager;
   }

   public boolean isAllowPersonalInvOverCap() {
      return this.allowPersonalInvOverCap;
   }

   public int getMaxChainDocking() {
      return this.maxChainDocking;
   }

   public boolean isOnlyAddFactionToFleet() {
      return this.onlyAddFactionToFleet;
   }

   public TradeManager getTradeManager() {
      return this.tradeManager;
   }

   public void onStop() {
      this.tradeManager.onStop();
   }

   public ManualTradeManager getManualTradeManager() {
      return this.manualTradeManager;
   }

   public int getSectorsToExploreForSystemScan() {
      return this.sectorsToExploreForSystemScan;
   }

   public InventoryMap getInventories() {
      return this.imap;
   }

   public Inventory getInventory(long var1) {
      Faction var3;
      return (var3 = ((FactionState)this.state).getFactionManager().getFaction((int)var1)) != null && var3 instanceof NPCFaction ? ((NPCFaction)var3).getInventory() : null;
   }

   public NetworkInventoryInterface getInventoryNetworkObject() {
      return this.networkGameState;
   }

   public String printInventories() {
      return "<SENDABLEGAMESTATEFACTIONINVENTORIES>";
   }

   public void sendInventoryModification(IntCollection var1, long var2) {
      Inventory var4;
      if ((var4 = this.getInventory(var2)) != null) {
         InventoryMultMod var6 = new InventoryMultMod(var1, var4, var2);
         this.getNetworkObject().getInventoryMultModBuffer().add(new RemoteInventoryMultMod(var6, this.getNetworkObject()));
      } else {
         assert false;

         try {
            throw new IllegalArgumentException("[INVENTORY] Exception: tried to send inventory " + var2);
         } catch (Exception var5) {
            var5.printStackTrace();
         }
      }
   }

   public void sendInventoryModification(int var1, long var2) {
   }

   public void sendInventorySlotRemove(int var1, long var2) {
   }

   public double getCapacityFor(Inventory var1) {
      return 1.0E14D;
   }

   public void volumeChanged(double var1, double var3) {
   }

   public void sendInventoryErrorMessage(Object[] var1, Inventory var2) {
   }

   public String getName() {
      return "SendableGameState";
   }

   public Object2ObjectOpenHashMap getClientNPCSystemMap() {
      return this.clientNPCSystemMap;
   }

   public void putClientNPCSystemStatus(Vector3i var1, float var2) {
      this.clientNPCStatusMap.put(var1, var2);
   }

   public float getClientNPCSystemStatus(int var1, Vector3i var2) {
      float var3;
      if ((var3 = this.clientNPCStatusMap.getFloat(var2)) >= 0.0F) {
         return var3;
      } else {
         if ((double)var3 < -1.1D) {
            Faction var4;
            if ((var4 = ((FactionState)this.getState()).getFactionManager().getFaction(var1)) != null && var4.isNPC()) {
               ((NPCFaction)var4).sendCommand(NPCFaction.NPCFactionControlCommandType.REQUEST_STATUS, var2.x, var2.y, var2.z);
            }

            this.clientNPCStatusMap.put(new Vector3i(var2), -1.0F);
         }

         return -1.0F;
      }
   }

   public boolean isNpcDebug() {
      return this.isOnServer() ? (Boolean)ServerConfig.NPC_DEBUG_MODE.getCurrentState() : this.npcDebug;
   }

   public boolean isFow() {
      return this.isOnServer() ? (Boolean)ServerConfig.USE_FOW.getCurrentState() : this.fow;
   }

   public float getNPCFleetSpeedLoaded() {
      return this.isOnServer() ? (Float)ServerConfig.NPC_LOADED_SHIP_MAX_SPEED_MULT.getCurrentState() : this.npcFleetSpeedLoaded;
   }

   public String getNPCShopOwnersDebug() {
      return this.isOnServer() ? ServerConfig.NPC_DEBUG_SHOP_OWNERS.getCurrentState().toString() : this.npcDebugShopOwners;
   }

   public Set getNPCShopOwnersDebugSet() {
      ObjectOpenHashSet var1 = new ObjectOpenHashSet();
      if (this.getNPCShopOwnersDebug().trim().length() > 0) {
         String[] var2;
         int var3 = (var2 = this.getNPCShopOwnersDebug().split(",")).length;

         for(int var4 = 0; var4 < var3; ++var4) {
            String var5 = var2[var4];
            var1.add(var5.trim().toLowerCase(Locale.ENGLISH));
         }
      }

      return var1;
   }

   public TopLevelType getTopLevelType() {
      return TopLevelType.GENERAL;
   }

   public boolean isPrivateNetworkObject() {
      return false;
   }

   public boolean isManCalcCancelOn() {
      return this.isOnServer() ? ServerConfig.MANAGER_CALC_CANCEL_ON.isOn() : this.getNetworkObject().manCalcCancelOn.getBoolean();
   }

   public ConfigPool getConfigPool() {
      return this.configPool;
   }

   public boolean isModuleEnabledByDefault(long var1) {
      return this.modulesEnabledByDefault.get(var1);
   }

   public int getSpawnProtectionSec() {
      return this.spawnProtection;
   }

   public RuleSetManager getRuleManager() {
      return this.ruleManager;
   }

   public RulePropertyContainer getRuleProperties() {
      return this.ruleProperties;
   }

   public void setAIWeaponSwitchDelayMS(int var1) {
      this.aiWeaponSwitchDelay = var1;
   }

   public long getAIWeaponSwitchDelayMS() {
      return (long)this.aiWeaponSwitchDelay;
   }

   public boolean isAllowFactoryOnShips() {
      return this.allowFactoriesOnShips;
   }

   public boolean isShipyardIgnoreStructure() {
      return this.shipyardIgnoreStructure;
   }
}
