package org.schema.game.common.data.missile;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;

public abstract class StraightFlyingMissile extends Missile {
   private static Vector3f clientTmp = new Vector3f();
   private static Vector3f serverTmp = new Vector3f();
   private static Transform t = new Transform();

   public StraightFlyingMissile(StateInterface var1) {
      super(var1);
   }

   public float updateTransform(Timer var1, Transform var2, Vector3f var3, Transform var4, boolean var5) {
      if (var3.lengthSquared() == 0.0F) {
         return var1.getDelta();
      } else {
         assert var3.lengthSquared() != 0.0F;

         assert !Float.isNaN(var3.x);

         Vector3f var6;
         (var6 = new Vector3f(var3)).normalize();

         assert !Float.isNaN(var6.x) : var3;

         var6.scale(var1.getDelta() * this.getSpeed());
         var4.set(var2);
         var4.origin.add(var6);

         assert !Float.isNaN(var6.x);

         return var6.length();
      }
   }

   public void updateClient(Timer var1) {
      this.distanceMade += this.updateTransform(var1, this.getWorldTransform(), this.getDirection(clientTmp), t, false);
      this.setTransformMissile(t);
   }

   public void updateServer(Timer var1) {
      super.updateServer(var1);
      this.distanceMade += this.updateTransform(var1, this.getWorldTransform(), this.getDirection(serverTmp), t, false);
      this.setTransformMissile(t);
   }
}
