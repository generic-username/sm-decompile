package org.schema.game.common.data.missile.updates;

import it.unimi.dsi.fastutil.bytes.Byte2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.missile.ActivationMissileInterface;
import org.schema.game.common.data.missile.BombMissile;
import org.schema.game.common.data.missile.DumbMissile;
import org.schema.game.common.data.missile.FafoMissile;
import org.schema.game.common.data.missile.HeatMissile;
import org.schema.game.common.data.missile.Missile;
import org.schema.game.common.data.missile.TargetChasingMissile;
import org.schema.schine.network.StateInterface;

public class MissileSpawnUpdate extends MissileUpdate {
   public static final Byte2ObjectOpenHashMap typeMap = new Byte2ObjectOpenHashMap();
   public Vector3f position = new Vector3f();
   public Vector3f dir = new Vector3f();
   public float speed;
   public int target;
   public MissileSpawnUpdate.MissileType missileType;
   public short colorType;
   public int sectorId = -666;
   public long spawnTime;
   public Vector3f targetPos;
   public int startTicks;
   public Vector3f relativePos;
   public float bombActivationTime;
   public long weaponId;

   public MissileSpawnUpdate(byte var1, short var2) {
      super(var1, var2);

      assert var1 == 1;

   }

   public MissileSpawnUpdate(short var1) {
      super((byte)1, var1);
   }

   public MissileSpawnUpdate(short var1, int var2, Vector3f var3, Vector3f var4, float var5, int var6, MissileSpawnUpdate.MissileType var7, int var8, long var9, short var11, long var12, Vector3f var14, int var15, Vector3f var16, float var17) {
      super((byte)1, var1);
      this.missileType = var7;
      this.position.set(var3);
      this.dir.set(var4);
      this.target = var6;
      this.sectorId = var8;
      this.speed = var5;
      this.weaponId = var9;
      this.colorType = var11;
      this.spawnTime = var12;
      this.targetPos = var14;
      this.startTicks = var15;
      this.relativePos = var16;
      this.bombActivationTime = var17;
   }

   protected void decode(DataInputStream var1) throws IOException {
      this.missileType = (MissileSpawnUpdate.MissileType)typeMap.get(var1.readByte());
      this.position.set(var1.readFloat(), var1.readFloat(), var1.readFloat());
      this.dir.set(var1.readFloat(), var1.readFloat(), var1.readFloat());
      this.target = var1.readInt();
      this.sectorId = var1.readInt();
      this.colorType = var1.readShort();
      this.weaponId = var1.readLong();
      this.speed = var1.readFloat();
      this.spawnTime = var1.readLong();
      this.startTicks = var1.readInt();
      if (this.missileType.activationTime) {
         this.bombActivationTime = var1.readFloat();
      }

      if (this.missileType.targetChasing) {
         this.targetPos = Vector3fTools.deserialize(var1);
         this.relativePos = Vector3fTools.deserialize(var1);
      }

   }

   protected void encode(DataOutputStream var1) throws IOException {
      var1.writeByte(this.missileType.id);
      var1.writeFloat(this.position.x);
      var1.writeFloat(this.position.y);
      var1.writeFloat(this.position.z);
      var1.writeFloat(this.dir.x);
      var1.writeFloat(this.dir.y);
      var1.writeFloat(this.dir.z);
      var1.writeInt(this.target);
      var1.writeInt(this.sectorId);
      var1.writeShort(this.colorType);
      var1.writeLong(this.weaponId);
      var1.writeFloat(this.speed);
      var1.writeLong(this.spawnTime);
      var1.writeInt(this.startTicks);
      if (this.missileType.activationTime) {
         var1.writeFloat(this.bombActivationTime);
      }

      if (this.missileType.targetChasing) {
         Vector3fTools.serialize(this.targetPos, var1);
         Vector3fTools.serialize(this.relativePos, var1);
      }

   }

   public void handleClientUpdate(GameClientState var1, Short2ObjectOpenHashMap var2, ClientChannel var3) {
      if (!var2.containsKey(this.id)) {
         Missile var4 = this.getClientMissile(var1);
         var2.put(var4.getId(), var4);
         var4.startTrail();
      }

   }

   public Missile getClientMissile(StateInterface var1) {
      Missile var2 = this.missileType.fac.instance(var1);

      assert !var2.getType().activationTime || var2 instanceof ActivationMissileInterface;

      assert !var2.getType().targetChasing || var2 instanceof TargetChasingMissile;

      var2.setFromSpawnUpdate(this);
      return var2;
   }

   static {
      MissileSpawnUpdate.MissileType[] var0;
      int var1 = (var0 = MissileSpawnUpdate.MissileType.values()).length;

      for(int var2 = 0; var2 < var1; ++var2) {
         MissileSpawnUpdate.MissileType var3 = var0[var2];

         assert !typeMap.containsKey(var3.id) : "DOUBLE ID: " + var3 + "; " + ((MissileSpawnUpdate.MissileType)typeMap.get(var3.id)).name();

         typeMap.put(var3.id, var3);
      }

   }

   public static enum MissileType {
      DUMB((byte)1, 0, false, false, new MissileSpawnUpdate.MissileFactory() {
         public final Missile instance(StateInterface var1) {
            return new DumbMissile(var1);
         }
      }),
      HEAT((byte)2, 1, true, false, new MissileSpawnUpdate.MissileFactory() {
         public final Missile instance(StateInterface var1) {
            return new HeatMissile(var1);
         }
      }),
      FAFO((byte)3, 2, true, false, new MissileSpawnUpdate.MissileFactory() {
         public final Missile instance(StateInterface var1) {
            return new FafoMissile(var1);
         }
      }),
      BOMB((byte)4, 3, false, true, new MissileSpawnUpdate.MissileFactory() {
         public final Missile instance(StateInterface var1) {
            return new BombMissile(var1);
         }
      });

      public final byte id;
      public final boolean targetChasing;
      public final boolean activationTime;
      public final MissileSpawnUpdate.MissileFactory fac;
      public final int configIndex;

      private MissileType(byte var3, int var4, boolean var5, boolean var6, MissileSpawnUpdate.MissileFactory var7) {
         this.id = var3;
         this.targetChasing = var5;
         this.activationTime = var6;
         this.fac = var7;
         this.configIndex = var4;
      }
   }

   interface MissileFactory {
      Missile instance(StateInterface var1);
   }
}
