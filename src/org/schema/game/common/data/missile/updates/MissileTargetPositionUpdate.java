package org.schema.game.common.data.missile.updates;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.GameClientState;

public class MissileTargetPositionUpdate extends MissileUpdate {
   public Transform objectTrans;
   public int ticks;
   public int objId;
   public int targetSectorId;

   public MissileTargetPositionUpdate(byte var1, short var2) {
      super(var1, var2);
      this.objectTrans = new Transform();

      assert var1 == 8;

   }

   public MissileTargetPositionUpdate(short var1) {
      this((byte)8, var1);
   }

   protected void decode(DataInputStream var1) throws IOException {
      TransformTools.deserializeFully(var1, this.objectTrans);
      this.ticks = var1.readInt();
      this.objId = var1.readInt();
      this.targetSectorId = var1.readInt();
   }

   protected void encode(DataOutputStream var1) throws IOException {
      TransformTools.serializeFully(var1, this.objectTrans);
      var1.writeInt(this.ticks);
      var1.writeInt(this.objId);
      var1.writeInt(this.targetSectorId);
   }

   public void handleClientUpdate(GameClientState var1, Short2ObjectOpenHashMap var2, ClientChannel var3) {
      var1.getController().getClientMissileManager().receivedPosUpdate(this);
   }
}
