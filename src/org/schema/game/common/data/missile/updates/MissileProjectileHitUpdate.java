package org.schema.game.common.data.missile.updates;

import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.missile.Missile;

public class MissileProjectileHitUpdate extends MissileUpdate {
   public float percent;

   public MissileProjectileHitUpdate(byte var1, short var2) {
      super(var1, var2);

      assert var1 == 9;

   }

   public MissileProjectileHitUpdate(short var1) {
      this((byte)9, var1);
   }

   protected void decode(DataInputStream var1) throws IOException {
      this.percent = (float)(var1.readByte() & 255) / 255.0F;
   }

   protected void encode(DataOutputStream var1) throws IOException {
      var1.writeByte((int)(this.percent * 255.0F));
   }

   public void encodeMissile(DataOutputStream var1) throws IOException {
      super.encodeMissile(var1);

      assert this.type == 9;

   }

   public void handleClientUpdate(GameClientState var1, Short2ObjectOpenHashMap var2, ClientChannel var3) {
      Missile var4;
      if ((var4 = (Missile)var2.get(this.id)) != null) {
         var4.onClientProjectileHit(this.percent);
      } else {
         System.err.println("[CLIENT][MISSILEUPDATE][PROJECTILEHIT] MISSILE CANNOT BE FOUND: " + this.id);
      }
   }
}
