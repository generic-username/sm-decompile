package org.schema.game.common.data.missile.updates;

import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.GameClientState;

public abstract class MissileUpdate {
   public static final byte SPAWN = 1;
   public static final byte POSITION = 2;
   public static final byte DEAD = 3;
   public static final byte TARGET_CHANGED = 4;
   public static final byte SECTOR_CHANGED = 5;
   public static final byte DIRECTION_AND_POS = 6;
   public static final byte LOCKSTEP = 7;
   public static final byte OBJECT_TRANS = 8;
   public static final byte PROJECTILE_HIT = 9;
   public final short id;
   public final byte type;
   public long timeStampServerSentToClient;

   public MissileUpdate(byte var1, short var2) {
      this.type = var1;
      this.id = var2;

      assert var1 != 0;

   }

   public static MissileUpdate decodeMissile(DataInputStream var0) throws IOException {
      byte var1 = var0.readByte();
      short var2 = var0.readShort();
      Object var3;
      switch(var1) {
      case 1:
         var3 = new MissileSpawnUpdate(var1, var2);
         break;
      case 2:
         var3 = new MissilePositionUpdate(var1, var2);
         break;
      case 3:
         var3 = new MissileDeadUpdate(var1, var2);
         break;
      case 4:
         var3 = new MissileTargetUpdate(var1, var2);
         break;
      case 5:
         var3 = new MissileSectorChangeUpdate(var1, var2);
         break;
      case 6:
         var3 = new MissileDirectionAndPosUpdate(var1, var2);
         break;
      case 7:
         var3 = new MissileLockStepUpdate(var1, var2);
         break;
      case 8:
         var3 = new MissileTargetPositionUpdate(var1, var2);
         break;
      case 9:
         var3 = new MissileProjectileHitUpdate(var1, var2);
         break;
      default:
         throw new IllegalArgumentException("Missile Update type not found " + var1);
      }

      ((MissileUpdate)var3).decode(var0);
      return (MissileUpdate)var3;
   }

   protected abstract void decode(DataInputStream var1) throws IOException;

   protected abstract void encode(DataOutputStream var1) throws IOException;

   public void encodeMissile(DataOutputStream var1) throws IOException {
      assert this.type != 0;

      var1.writeByte(this.type);
      var1.writeShort(this.id);
      this.encode(var1);
   }

   public abstract void handleClientUpdate(GameClientState var1, Short2ObjectOpenHashMap var2, ClientChannel var3);
}
