package org.schema.game.common.data.missile;

public interface MissileControllerInterface {
   MissileManagerInterface getMissileManager();
}
