package org.schema.game.common.data.missile;

import org.schema.game.common.data.missile.updates.MissileSpawnUpdate;
import org.schema.game.common.data.missile.updates.MissileTargetUpdate;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;

public class FafoMissile extends TargetChasingMissile {
   private SimpleTransformableSendableObject designatedTarget;

   public FafoMissile(StateInterface var1) {
      super(var1);
   }

   public SimpleTransformableSendableObject getDesignatedTarget() {
      return this.designatedTarget;
   }

   public void setDesignatedTarget(SimpleTransformableSendableObject var1) {
      this.designatedTarget = var1;
   }

   public MissileSpawnUpdate.MissileType getType() {
      return MissileSpawnUpdate.MissileType.FAFO;
   }

   public void onSpawn() {
      super.onSpawn();
      this.setTarget(this.designatedTarget);
   }

   public String toString() {
      return "Fafo" + super.toString() + ")->" + this.getTarget();
   }

   public void updateTarget(Timer var1) {
      this.setTarget(this.designatedTarget);
      if (this.getTarget() != null && !this.validTarget(this.getTarget())) {
         this.setTarget((SimpleTransformableSendableObject)null);
         this.designatedTarget = null;
         MissileTargetUpdate var2;
         (var2 = new MissileTargetUpdate(this.getId())).target = -1;
         this.pendingBroadcastUpdates.add(var2);
      }

   }

   private boolean validTarget(SimpleTransformableSendableObject var1) {
      if (var1.isHidden()) {
         return false;
      } else if (!var1.isNeighbor(this.getSectorId(), var1.getSectorId())) {
         return false;
      } else {
         return this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(var1.getId());
      }
   }
}
