package org.schema.game.common.data.missile;

import org.schema.common.util.StringTools;
import org.schema.game.common.data.missile.updates.MissileSpawnUpdate;
import org.schema.schine.network.StateInterface;

public class BombMissile extends StraightFlyingMissile implements ActivationMissileInterface {
   private float activationTimer;

   public BombMissile(StateInterface var1) {
      super(var1);
      this.selfDamage = true;
   }

   public MissileSpawnUpdate.MissileType getType() {
      return MissileSpawnUpdate.MissileType.BOMB;
   }

   public void onSpawn() {
   }

   public String toString() {
      return "Bomb" + super.toString();
   }

   public float getActivationTimer() {
      return this.activationTimer;
   }

   public void setActivationTimer(float var1) {
      this.activationTimer = var1;
   }

   public void onDud() {
      this.sendServerMessage(new Object[]{227, StringTools.formatPointZero(this.activationTimer)}, 3);
   }

   protected boolean isDud() {
      return this.getLifetime() < this.activationTimer;
   }

   protected boolean canHitSelf() {
      return this.getLifetime() > this.activationTimer;
   }

   protected boolean isIgnoreShieldsSelf() {
      return true;
   }

   protected boolean isIgnoreShields() {
      return true;
   }

   protected long getMissileTimeoutMs() {
      return 60000L;
   }
}
