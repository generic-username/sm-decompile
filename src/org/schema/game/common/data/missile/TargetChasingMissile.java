package org.schema.game.common.data.missile;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.util.List;
import java.util.Random;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.elements.missile.dumb.DumbMissileElementManager;
import org.schema.game.common.data.missile.updates.MissileSpawnUpdate;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.graphicsengine.forms.debug.DebugPoint;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;

public abstract class TargetChasingMissile extends Missile {
   private final Vector3f tmpDir = new Vector3f();
   private final Vector3f dirScaled = new Vector3f();
   private final Vector3f tmp0 = new Vector3f();
   private SimpleTransformableSendableObject target;
   private Transform t = new Transform();
   private Transform tmp = new Transform();
   private Vector3f projected = new Vector3f();
   private Vector3f dd = new Vector3f();
   private Random random;
   private int updateCount = 0;
   public int steps = 1;
   public int ticks = 0;
   public static long UPDATE_MS = 8L;
   public static final long UPDATE_LEN = 280L;
   private static float TSTP;
   public final List targetPositions = new ObjectArrayList();
   private MissileTargetPosition currentTar;
   private long accumulatedTime;
   private long lastUpdate;
   private long lastReferenceTime;
   int startTicks;
   Vector3f relativeTargetPos = new Vector3f();
   private boolean canTurnFromBack = true;
   private int stalling;
   public static boolean debug;
   private static final Short2ObjectOpenHashMap debugClient;
   private static final Short2ObjectOpenHashMap debugServer;

   public TargetChasingMissile(StateInterface var1) {
      super(var1);
      this.t.setIdentity();
      this.random = new Random();
      this.lastUpdate = System.currentTimeMillis();
   }

   private boolean resetDebug() {
      if (debug && this.isOnServer()) {
         synchronized(debugClient) {
            debugClient.put(this.getId(), new ObjectArrayList());
            debugServer.put(this.getId(), new ObjectArrayList());
         }
      }

      return true;
   }

   public void setId(short var1) {
      super.setId(var1);

      assert this.resetDebug();

   }

   public MissileSpawnUpdate.MissileType getType() {
      return null;
   }

   public SimpleTransformableSendableObject getTarget() {
      return this.target;
   }

   public void onSpawn() {
   }

   public void setTarget(int var1) {
      if (var1 > 0) {
         Sendable var2;
         if ((var2 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().get(var1)) != null && var2 instanceof SimpleTransformableSendableObject) {
            this.setTarget((SimpleTransformableSendableObject)var2);
         }

      } else {
         this.setTarget((SimpleTransformableSendableObject)null);
      }
   }

   public abstract void updateTarget(Timer var1);

   public Vector3f getTargetRelativePosition(Vector3f var1) {
      assert this.isOnServer();

      if (this.getTarget() == null) {
         var1.set(0.0F, 0.0F, 0.0F);
      } else {
         this.random.setSeed((long)(this.getTarget().getId() + this.getId()));
         this.getTarget().getAimingAtRelativePos(var1, this.getOwner(), this.getTarget(), this.random, 0.0F);
      }

      return var1;
   }

   public Transform getTargetPosition(Transform var1) {
      assert this.isOnServer();

      var1.setIdentity();
      if (this.getTarget() == null) {
         var1.origin.set(this.getWorldTransform().origin);
         Vector3f var2;
         (var2 = this.getDirection(new Vector3f())).normalize();
         var2.scale(1000.0F);
         var1.origin.add(var2);
      } else {
         Sector var3;
         if ((var3 = ((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId())) != null) {
            this.getTarget().calcWorldTransformRelative(var3.getId(), var3.pos);
            return this.target.getClientTransformCenterOfMass(var1);
         }

         this.setAlive(false);
      }

      return var1;
   }

   private boolean onTSEmpty() {
      MissileTargetManager var1;
      if (this.isOnServer()) {
         var1 = ((GameServerState)this.getState()).getController().getMissileController().getMissileManager().targetManager;
      } else {
         var1 = ((GameClientState)this.getState()).getController().getClientMissileManager().targetManager;
      }

      Vector3f var2 = null;
      if (this.target != null) {
         int var3 = this.startTicks + this.ticks;
         if (!var1.hasPosForTick(this.target.getId(), var3)) {
            ++this.stalling;
            if (this.stalling % 500 == 0) {
               System.err.println(this.getState() + " Missile " + this.getId() + " stalling for position: amount: " + this.stalling);
            }

            if (this.isOnServer() && this.stalling > 1000) {
               System.err.println("[SERVER] MISSILE STALLING TOO LONG. KILLING MISSILE. MISSING TICK: " + var3 + "; next tick available: " + var1.hasPosForTick(this.target.getId(), var3 + 1));
               this.setAlive(false);
            }

            return false;
         }

         this.stalling = 0;
         var2 = var1.getPositionFor(this.target.getId(), this.relativeTargetPos, var3, this.getSectorId(), this.getState());
      }

      if (var2 == null) {
         var2 = new Vector3f(this.getWorldTransform().origin);
         Vector3f var4;
         (var4 = this.getDirection(new Vector3f())).scale(500.0F);
         var2.add(var4);
      }

      MissileTargetPosition var5;
      (var5 = new MissileTargetPosition()).targetPosition = var2;
      var5.time = this.spawnTime + (long)(this.steps + 1) * UPDATE_LEN;
      ++this.steps;
      this.targetPositions.add(var5);
      return true;
   }

   public float updateTransform(Timer var1, Transform var2, Vector3f var3, Transform var4, boolean var5) {
      this.accumulatedTime += var1.currentTime - this.lastUpdate;
      this.lastUpdate = var1.currentTime;
      var4.set(var2);
      if (this.currentTar == null || this.currentTar.isExecuted(this.lastReferenceTime)) {
         if (this.isOnServer()) {
            this.updateTarget(var1);
         }

         if (this.targetPositions.isEmpty()) {
            if (this.isOnServer()) {
               ((GameServerState)this.getState()).getController().getMissileController().getMissileManager().stalling.add(this.getId());
            } else {
               ((GameClientState)this.getState()).getController().getClientMissileManager().stalling.add(this.getId());
            }

            this.onTSEmpty();
            return 0.0F;
         }

         if (this.currentTar == null) {
            this.lastReferenceTime = this.spawnTime;
         } else {
            this.lastReferenceTime = this.currentTar.time;
         }

         this.currentTar = (MissileTargetPosition)this.targetPositions.remove(0);
         if (this.targetPositions.isEmpty() && !this.onTSEmpty()) {
            return 0.0F;
         }

         ++this.ticks;
      }

      long var6 = this.currentTar.time - this.lastReferenceTime;

      assert var6 == UPDATE_LEN : var6 + "; " + UPDATE_LEN;

      long var8;

      assert (var8 = var6 - this.currentTar.executedTime) > 0L || this.currentTar.isExecuted(this.lastReferenceTime);

      float var10 = 0.0F;
      if (var8 > 0L) {
         while(this.accumulatedTime >= UPDATE_MS && !this.currentTar.isExecuted(this.lastReferenceTime)) {
            this.dd.sub(this.currentTar.targetPosition, var4.origin);
            if (this.dd.lengthSquared() != 0.0F) {
               float var11 = this.dd.length();
               Vector3f var12;
               (var12 = new Vector3f(var3)).normalize();
               this.dd.normalize();
               float var14 = this.dd.dot(var12);
               GlUtil.project(var12, new Vector3f(this.dd), this.projected);

               assert !Float.isNaN(this.projected.x);

               if (this.projected.lengthSquared() > 0.0F) {
                  this.projected.normalize();

                  assert !Float.isNaN(this.projected.x);

                  float var13 = TSTP;
                  boolean var7 = false;
                  if (var14 < 0.0F) {
                     if ((double)var11 > 300.0D) {
                        this.canTurnFromBack = true;
                     }

                     if (this.canTurnFromBack) {
                        this.projected.scale(var13 * DumbMissileElementManager.CHASING_TURN_SPEED_WITH_TARGET_IN_BACK);
                        var7 = true;
                     }
                  } else {
                     this.projected.scale(var13 * DumbMissileElementManager.CHASING_TURN_SPEED_WITH_TARGET_IN_FRONT);
                     this.canTurnFromBack = false;
                     var7 = true;
                  }

                  if (var7) {
                     assert !Float.isNaN(this.projected.x);

                     var3.normalize();
                     var3.add(this.projected);
                  }
               }

               var3.normalize();
               this.dirScaled.set(var3);
               this.dirScaled.normalize();
               this.dirScaled.scale(TSTP * this.getSpeed());

               assert !Float.isNaN(this.dirScaled.x);

               var4.origin.add(this.dirScaled);
               var10 += this.dirScaled.length();
               var3.set(this.dirScaled);
               var3.normalize();
               var3.scale(this.getSpeed());
               this.setDirection(var3);
               if (this.isOnServer() && EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
                  DebugDrawer.points.add(new DebugPoint(var4.origin, new Vector4f(0.0F, 1.0F, 0.0F, 1.0F)));
               }
            }

            this.accumulatedTime -= UPDATE_MS;
            MissileTargetPosition var10000 = this.currentTar;
            var10000.executedTime += UPDATE_MS;

            assert this.checkUpdate(var4);

            ++this.updateCount;
         }
      } else if (this.isOnServer()) {
         System.err.println("SERVER: NO TIME -> MISSILE DIE: " + var8 + "; exec: " + this.currentTar.executedTime + "; time: " + this.currentTar.time + "; last ref: " + this.lastReferenceTime);
         this.setAlive(false);
      }

      return var10;
   }

   private boolean checkUpdate(Transform var1) {
      Throwable var10000;
      label135: {
         if (debug) {
            synchronized(debugClient){}

            boolean var10001;
            int var3;
            int var12;
            try {
               if (this.isOnServer()) {
                  ((ObjectArrayList)debugServer.get(this.getId())).add(new Vector3f(var1.origin));
               } else {
                  ((ObjectArrayList)debugClient.get(this.getId())).add(new Vector3f(var1.origin));
               }

               var12 = Math.min(((ObjectArrayList)debugClient.get(this.getId())).size(), ((ObjectArrayList)debugServer.get(this.getId())).size());
               var3 = 0;
            } catch (Throwable var10) {
               var10000 = var10;
               var10001 = false;
               break label135;
            }

            while(true) {
               label124:
               try {
                  if (var3 < var12) {
                     Vector3f var4 = (Vector3f)((ObjectArrayList)debugClient.get(this.getId())).get(var3);
                     Vector3f var5;
                     if ((var5 = (Vector3f)((ObjectArrayList)debugServer.get(this.getId())).get(var3)).epsilonEquals(var4, 0.3F)) {
                        break label124;
                     }

                     System.err.println("ASSERT FAIL: Failed on update " + (this.updateCount + 1) + ": Client: " + var4 + ", Server: " + var5);
                     return false;
                  }
                  break;
               } catch (Throwable var11) {
                  var10000 = var11;
                  var10001 = false;
                  break label135;
               }

               ++var3;
            }
         }

         return true;
      }

      Throwable var13 = var10000;
      throw var13;
   }

   public void setFromSpawnUpdate(MissileSpawnUpdate var1) {
      super.setFromSpawnUpdate(var1);
      this.setTarget(var1.target);
      MissileTargetPosition var2;
      (var2 = new MissileTargetPosition()).time = this.spawnTime + UPDATE_LEN;
      var2.targetPosition = new Vector3f(var1.targetPos);
      this.relativeTargetPos.set(var1.relativePos);
      this.targetPositions.add(var2);
      this.startTicks = var1.startTicks;
   }

   public void updateClient(Timer var1) {
      this.distanceMade += this.updateTransform(var1, this.getWorldTransform(), this.getDirection(this.tmpDir), this.t, false);
      this.setTransformMissile(this.t);
   }

   public void updateServer(Timer var1) {
      this.distanceMade += this.updateTransform(var1, this.getWorldTransform(), this.getDirection(this.tmpDir), this.t, false);
      this.setTransformMissile(this.t);
      super.updateServer(var1);
   }

   public void setTarget(SimpleTransformableSendableObject var1) {
      MissileTargetManager var2;
      if (this.isOnServer()) {
         var2 = ((GameServerState)this.getState()).getController().getMissileController().getMissileManager().targetManager;
      } else {
         var2 = ((GameClientState)this.getState()).getController().getClientMissileManager().targetManager;
      }

      var2.registerTarget(this.target, var1, this.getState());
      this.target = var1;
   }

   protected void onDeadServer() {
      assert this.isOnServer();

      super.onDeadServer();
      if (this.getTarget() != null) {
         ((GameServerState)this.getState()).getController().getMissileController().getMissileManager().targetManager.unregisterOne(this.getTarget().getId());
      }

      ((GameServerState)this.getState()).getController().getMissileController().getMissileManager().targetManager.checkAllAlive(this.getState());
   }

   private boolean removeClientDebug() {
      if (debug) {
         synchronized(debugClient) {
            debugClient.remove(this.getId());
            debugServer.remove(this.getId());
         }
      }

      return true;
   }

   public void onClientDie(int var1) {
      super.onClientDie(var1);
      if (this.getTarget() != null) {
         ((GameClientState)this.getState()).getController().getClientMissileManager().targetManager.unregisterOne(this.getTarget().getId());
      }

      ((GameClientState)this.getState()).getController().getClientMissileManager().targetManager.checkAllAlive(this.getState());

      assert this.removeClientDebug();

   }

   static {
      TSTP = (float)UPDATE_MS * 0.001F;
      debug = false;
      debugClient = new Short2ObjectOpenHashMap();
      debugServer = new Short2ObjectOpenHashMap();
   }
}
