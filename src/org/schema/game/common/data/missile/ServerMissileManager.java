package org.schema.game.common.data.missile;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectBidirectionalIterator;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import it.unimi.dsi.fastutil.objects.ObjectRBTreeSet;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import it.unimi.dsi.fastutil.shorts.ShortSet;
import java.util.Comparator;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.util.CompareTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.missile.updates.MissileSpawnUpdate;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SectorNotFoundRuntimeException;
import org.schema.game.network.objects.NetworkClientChannel;
import org.schema.game.network.objects.remote.RemoteMissileUpdate;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.objects.remote.RemoteShort;

public class ServerMissileManager implements MissileManagerInterface {
   private final Short2ObjectOpenHashMap missiles = new Short2ObjectOpenHashMap();
   private ObjectRBTreeSet sortedByDamage = new ObjectRBTreeSet(new Comparator() {
      public int compare(Missile var1, Missile var2) {
         int var3;
         return (var3 = CompareTools.compare(var1.getDamage(), var2.getDamage())) == 0 ? CompareTools.compare(var1.getId(), var2.getId()) : var3;
      }
   });
   private final GameServerState state;
   private int ticks;
   long lastUpdate = -1L;
   public final MissileTargetManager targetManager = new MissileTargetManager();
   private long accumulated;
   private int lastTickRecorded = -1;
   private int missilesSpawned;
   private long recordMissileSpawed;
   public ShortSet stalling = new ShortOpenHashSet();
   private long lastStallingMsg;
   private Transform tmp = new Transform();
   private Vector3f dist = new Vector3f();

   public ServerMissileManager(GameServerState var1) {
      this.state = var1;
   }

   public void addMissile(Missile var1) {
      var1.initPhysics();
      this.missiles.put(var1.getId(), var1);
      this.sortedByDamage.add(var1);
      var1.onSpawn();
      var1.pendingBroadcastUpdates.add(this.getSpawnUpdate(var1));
   }

   public void fromNetwork(NetworkClientChannel var1) {
      for(int var2 = 0; var2 < var1.missileMissingRequestBuffer.getReceiveBuffer().size(); ++var2) {
         short var3 = (Short)((RemoteShort)var1.missileMissingRequestBuffer.getReceiveBuffer().get(var2)).get();
         Missile var4;
         if ((var4 = (Missile)this.missiles.get(var3)) != null) {
            System.err.println("[SERVER] Adding Requested Spawn " + var4);
            var1.missileUpdateBuffer.add(new RemoteMissileUpdate(this.getSpawnUpdate(var4), var1));
         }
      }

   }

   public Missile getLowestDamage(Damager var1, Vector3f var2, float var3) {
      if (!this.sortedByDamage.isEmpty()) {
         ObjectBidirectionalIterator var4 = this.sortedByDamage.iterator();

         while(var4.hasNext()) {
            Missile var5 = (Missile)var4.next();
            if (this.checkMissileRange(var5, var1, var2, var3)) {
               return var5;
            }
         }
      }

      return null;
   }

   public Missile getHighestDamage(Damager var1, Vector3f var2, float var3) {
      if (!this.sortedByDamage.isEmpty()) {
         ObjectBidirectionalIterator var4 = this.sortedByDamage.iterator(this.sortedByDamage.last());

         while(var4.hasPrevious()) {
            Missile var5 = (Missile)var4.previous();
            if (this.checkMissileRange(var5, var1, var2, var3)) {
               return var5;
            }
         }
      }

      return null;
   }

   private boolean checkMissileRange(Missile var1, Damager var2, Vector3f var3, float var4) {
      Sector var5;
      if ((var5 = this.state.getUniverse().getSector(var2.getSectorId())) != null && var1.isInNeighborSecorToServer(var5.pos)) {
         if (var1.getOwner().isSegmentController() && var2 instanceof SegmentController && ((SegmentController)var1.getOwner()).railController.isInAnyRailRelationWith((SegmentController)var2) || var2.getFactionId() != 0 && var1.getOwner().getFactionId() == var2.getFactionId()) {
            return false;
         } else {
            Transform var6 = var1.getWorldTransformRelativeToSector(var2.getSectorId(), this.tmp);
            this.dist.sub(var6.origin, var3);
            return this.dist.length() < var4;
         }
      } else {
         return false;
      }
   }

   public Missile getLowestDamage(Vector3i var1) {
      ObjectBidirectionalIterator var2 = this.sortedByDamage.iterator();

      Missile var3;
      do {
         if (!var2.hasNext()) {
            return null;
         }
      } while(!(var3 = (Missile)var2.next()).isInNeighborSecorToServer(var1));

      return var3;
   }

   public Missile getHighestDamage(Vector3i var1) {
      ObjectBidirectionalIterator var2 = this.sortedByDamage.iterator(this.sortedByDamage.last());

      Missile var3;
      do {
         if (!var2.hasPrevious()) {
            return null;
         }
      } while(!(var3 = (Missile)var2.previous()).isInNeighborSecorToServer(var1));

      return var3;
   }

   public MissileSpawnUpdate getSpawnUpdate(Missile var1) {
      int var2 = -1;
      ++this.missilesSpawned;
      if (var1 instanceof TargetChasingMissile && ((TargetChasingMissile)var1).getTarget() != null) {
         var2 = ((TargetChasingMissile)var1).getTarget().getId();
      }

      Transform var3;
      (var3 = new Transform()).setIdentity();
      Vector3f var4 = new Vector3f();
      Vector3f var5 = new Vector3f();
      if (var1.getType().targetChasing) {
         ((TargetChasingMissile)var1).startTicks = this.getTicks();
         var5 = ((TargetChasingMissile)var1).getTargetRelativePosition(var5);
         ((TargetChasingMissile)var1).relativeTargetPos = var5;
         var3 = ((TargetChasingMissile)var1).getTargetPosition(var3);
         var4.set(var5);
         var3.transform(var4);
         MissileTargetPosition var7;
         (var7 = new MissileTargetPosition()).targetPosition = new Vector3f(var4);
         var7.time = var1.spawnTime + TargetChasingMissile.UPDATE_LEN;
         ((TargetChasingMissile)var1).targetPositions.add(var7);
      }

      float var8 = 0.0F;
      if (var1.getType().activationTime) {
         var8 = ((ActivationMissileInterface)var1).getActivationTimer();
      }

      MissileSpawnUpdate var6 = new MissileSpawnUpdate(var1.getId(), var1.getDamage(), var1.getInitialTransform().origin, var1.getDirection(new Vector3f()), var1.getSpeed(), var2, var1.getType(), var1.getSectorId(), var1.getWeaponId(), var1.getColorType(), var1.spawnTime, var4, this.getTicks(), var5, var8);

      assert var6.missileType == var1.getType();

      return var6;
   }

   public GameServerState getState() {
      return this.state;
   }

   public void sendMissileUpdates() {
      this.targetManager.sendPending(this.state);

      Missile var2;
      for(Iterator var1 = this.missiles.values().iterator(); var1.hasNext(); var2.clearAllUpdates()) {
         Iterator var3;
         PlayerState var4;
         RegisteredClientOnServer var5;
         ClientChannel var6;
         if ((var2 = (Missile)var1.next()).hasPendingBroadcastUpdates()) {
            var3 = this.state.getPlayerStatesByName().values().iterator();

            label48:
            while(true) {
               while(true) {
                  while(true) {
                     if (!var3.hasNext()) {
                        break label48;
                     }

                     var4 = (PlayerState)var3.next();
                     if ((var5 = (RegisteredClientOnServer)this.getState().getClients().get(var4.getClientId())) != null) {
                        Sendable var8;
                        if ((var8 = (Sendable)var5.getLocalAndRemoteObjectContainer().getLocalObjects().get(0)) != null && var8 instanceof ClientChannel) {
                           var6 = (ClientChannel)var8;
                           var2.sendPendingBroadcastUpdates(var6);
                        } else {
                           System.err.println("[SERVER] BROADCAST MISSILE UPDATE FAILED FOR " + var4 + ": NO CLIENT CHANNEL");
                        }
                     } else {
                        System.err.println("[SEVRER][MISSILEMAN] client for player not found: " + var4);
                     }
                  }
               }
            }
         }

         if (var2.hasPendingUpdates() && var2.isAlive()) {
            var3 = var2.nearPlayers().iterator();

            while(var3.hasNext()) {
               var4 = (PlayerState)var3.next();
               if ((var5 = (RegisteredClientOnServer)this.getState().getClients().get(var4.getClientId())) != null) {
                  var4.getCurrentSector();
                  Sendable var7;
                  if ((var7 = (Sendable)var5.getLocalAndRemoteObjectContainer().getLocalObjects().get(0)) != null && var7 instanceof ClientChannel) {
                     var6 = (ClientChannel)var7;
                     var2.sendPendingUpdates(var6);
                  }
               } else {
                  System.err.println("[SEVRER][MISSILEMAN] broadcast client for player not found: " + var4);
               }
            }
         }
      }

   }

   public void updateServer(Timer var1) {
      if (this.lastUpdate > 0L) {
         this.accumulated += var1.currentTime - this.lastUpdate;
         this.ticks = (int)(this.accumulated / TargetChasingMissile.UPDATE_LEN);
      }

      this.lastUpdate = var1.currentTime;
      if (this.lastTickRecorded >= 0) {
         for(int var2 = this.lastTickRecorded + 1; var2 <= this.ticks; ++var2) {
            this.targetManager.record(this.state, var2);
         }
      } else {
         this.targetManager.record(this.state, this.ticks);
      }

      this.lastTickRecorded = this.ticks;
      this.targetManager.record(this.state, this.ticks + 1);
      if (this.missilesSpawned > 0 && this.recordMissileSpawed == 0L) {
         this.recordMissileSpawed = System.currentTimeMillis();
      } else if (this.recordMissileSpawed > 0L && System.currentTimeMillis() - this.recordMissileSpawed > 20000L) {
         System.err.println("[SERVER] MISSILE REPORT: Spawned missiles in the last 20 seconds: " + this.missilesSpawned);
         this.missilesSpawned = 0;
         this.recordMissileSpawed = 0L;
      }

      if (this.stalling.size() > 0 && System.currentTimeMillis() - this.lastStallingMsg > 5000L) {
         System.err.println("[SERVER] Stalling missiles in the last 5 sec: " + this.stalling.size());
         this.stalling.clear();
         this.lastStallingMsg = System.currentTimeMillis();
      }

      ObjectIterator var10 = this.missiles.values().iterator();
      long var3 = System.currentTimeMillis();

      while(true) {
         while(var10.hasNext()) {
            Missile var5;
            if ((var5 = (Missile)var10.next()).isAlive()) {
               var5.updateServer(var1);
               var5.nearPlayers().clear();
               Iterator var6 = this.state.getPlayerStatesByName().values().iterator();

               while(var6.hasNext()) {
                  PlayerState var7;
                  if ((var7 = (PlayerState)var6.next()).getCurrentSectorId() == var5.getSectorId()) {
                     var5.nearPlayers().add(var7);
                  } else {
                     Sector var8;
                     if ((var8 = this.state.getUniverse().getSector(var5.getSectorId())) != null) {
                        if (Math.abs(var8.pos.x - var7.getCurrentSector().x) < 2 && Math.abs(var8.pos.y - var7.getCurrentSector().y) < 2 && Math.abs(var8.pos.z - var7.getCurrentSector().z) < 2) {
                           var5.nearPlayers().add(var7);
                        }
                     } else {
                        var5.setAlive(false);
                     }
                  }
               }
            } else {
               try {
                  var5.removeFromSectorPhysicsForProjectileCheck(var5.currentProjectileSector);
               } catch (SectorNotFoundRuntimeException var9) {
                  System.err.println("[SERVER][MISSILEMAN] WARNING: Physics for missile " + var5 + " has not been removed: Sector not loaded: " + var9.getMessage());
               }

               var10.remove();
               this.sortedByDamage.remove(var5);
            }
         }

         long var11;
         if ((var11 = System.currentTimeMillis() - var3) > 50L) {
            System.err.println("[SERVER][MISSILECONTROLLER] WARNING: update of " + this.missiles.size() + " took " + var11 + " ms");
         }

         this.sendMissileUpdates();
         return;
      }
   }

   public Missile hasHit(short var1, int var2, Vector3f var3, Vector3f var4) {
      Missile var5;
      return (var5 = (Missile)this.missiles.get(var1)) != null && !var5.hadHit(var2) && var5.hasHit(var3, var4) ? var5 : null;
   }

   public Short2ObjectOpenHashMap getMissiles() {
      return this.missiles;
   }

   public int getTicks() {
      return this.ticks;
   }
}
