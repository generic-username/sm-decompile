package org.schema.game.common;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.Desktop.Action;
import java.io.IOException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.ClientStatics;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.gui.ConnectionDialog;
import org.schema.game.common.gui.LoginDialog;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.game.common.version.Version;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.LoginFailedException;
import org.schema.schine.network.client.HostPortLoginName;
import org.schema.schine.network.commands.Login;
import org.schema.schine.network.server.ServerController;
import org.schema.schine.resource.FileExt;

public class ClientRunnable implements Runnable {
   private HostPortLoginName server;
   private static ConnectionDialog connectDialog;
   private GameClientState gameClientState;
   private GameClientController gameClientController;
   private GraphicsContext context;

   public ClientRunnable(HostPortLoginName var1, boolean var2, GraphicsContext var3) {
      this.server = var1;
      this.context = var3;
      if (this.context == null) {
         throw new NullPointerException("Starting Client without Graphics Context");
      } else {
         if (connectDialog == null) {
            connectDialog = new ConnectionDialog();
         }

         if (var2) {
            connectDialog.setVisible(true);
         }

      }
   }

   public static void openWebpage(URI var0) {
      Desktop var10000 = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
      Desktop var1 = var10000;
      if (var10000 != null && var1.isSupported(Action.BROWSE)) {
         try {
            var1.browse(var0);
            return;
         } catch (Exception var2) {
            var2.printStackTrace();
         }
      }

   }

   public void run() {
      System.err.println("[CLIENT] initializing " + Version.VERSION + " (" + Version.build + ")");
      KeyboardMappings.read();

      try {
         KeyboardMappings.write();
      } catch (IOException var19) {
         var19.printStackTrace();
         GuiErrorHandler.processNormalErrorDialogException(var19, true);
      }

      (new FileExt(ClientStatics.SEGMENT_DATA_DATABASE_PATH)).mkdirs();
      boolean var1 = true;

      try {
         Exception var2;
         String var3;
         try {
            synchronized(Starter.serverLock) {
               while(!Starter.serverInitFinished) {
                  if (this.context != null) {
                     this.context.setLoadMessage(Lng.ORG_SCHEMA_GAME_COMMON_CLIENTRUNNABLE_0);
                  }

                  System.err.println("[CLIENT] WAITING FOR SERVER");
                  Starter.serverLock.wait(60000L);
                  if (Starter.startupException != null) {
                     System.err.println("[CLIENT] Intercepted exception " + Starter.startupException.getClass().getSimpleName());
                     Exception var33 = Starter.startupException;
                     Starter.startupException = null;
                     throw var33;
                  }
               }
            }

            if (this.context != null) {
               this.context.setLoadMessage(Lng.ORG_SCHEMA_GAME_COMMON_CLIENTRUNNABLE_8);
            }

            if (GameServerState.isCreated()) {
               System.err.println("[LOCAL GAME] CHANGED PORT TO " + ServerController.port);
               this.server.port = ServerController.port;
            }

            if (Starter.startupException != null) {
               System.err.println("[CLIENT] Intercepted exception " + Starter.startupException.getClass().getSimpleName());
               var2 = Starter.startupException;
               Starter.startupException = null;
               throw var2;
            }

            this.gameClientState = new GameClientState();
            this.gameClientController = new GameClientController(this.gameClientState, this.context, connectDialog);
            System.err.println("[CLIENT] Client State and Controller successfully created");
            if (GameServerState.isCreated()) {
               GameClientState.singleplayerCreativeMode = EngineSettings.G_SINGLEPLAYER_CREATIVE_MODE.isOn() ? 1 : 2;
               System.err.println("[CLIENT] Singleplayer Creative mode: " + EngineSettings.G_SINGLEPLAYER_CREATIVE_MODE.isOn() + ": " + GameClientState.singleplayerCreativeMode);
            }

            if (this.context != null) {
               this.context.setLoadMessage(Lng.ORG_SCHEMA_GAME_COMMON_CLIENTRUNNABLE_1);
            }

            (new Thread(new Runnable() {
               public void run() {
                  int var1 = 0;

                  while(var1 < 10) {
                     if (ClientRunnable.this.context != null && Starter.startupException != null) {
                        System.err.println("[CLIENT] Intercepted exception " + Starter.startupException.getClass().getSimpleName());
                        Exception var2 = Starter.startupException;
                        Starter.startupException = null;
                        ClientRunnable.this.context.handleError(var2);
                     }

                     ++var1;

                     try {
                        Thread.sleep(1000L);
                     } catch (InterruptedException var3) {
                        var3.printStackTrace();
                     }
                  }

               }
            })).start();
            this.gameClientController.connect(this.server);
            System.err.println("[CLIENT] Client State and Controller successfully connected");
            this.gameClientController.initialize();
            System.err.println("[CLIENT] Client State and Controller successfully initialized");
            EngineSettings.print();
            var1 = this.gameClientController.startGraphics(this.context);
            System.err.println("[CLIENT] AFTER GRAPHICS RUN!");
            return;
         } catch (LoginFailedException var26) {
            LoginFailedException var29 = var26;

            try {
               var29.printStackTrace();
               Login.LoginCode var4 = Login.LoginCode.getById(var29.getErrorCode());
               var3 = var4.errorMessage() + this.gameClientState.getExtraLoginFailReason();
               if (var4 == Login.LoginCode.ERROR_WRONG_CLIENT_VERSION) {
                  var3 = StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CLIENTRUNNABLE_9, this.server, Version.VERSION, GameClientState.serverVersion);
                  if (Version.compareVersion(GameClientState.serverVersion) < 0) {
                     var3 = var3 + "\n\n" + Lng.ORG_SCHEMA_GAME_COMMON_CLIENTRUNNABLE_10;
                  } else {
                     var3 = var3 + "\n\n" + Lng.ORG_SCHEMA_GAME_COMMON_CLIENTRUNNABLE_11;
                  }
               }

               if (this.context != null) {
                  var1 = false;
                  Starter.stopClient(this.context);
                  System.err.println("[LOGINERROR] " + var3);
                  this.context.handleError(var3);
               } else {
                  JFrame var30;
                  (var30 = new JFrame("LoginError")).setAlwaysOnTop(true);
                  int var31;
                  if (var4 == Login.LoginCode.ERROR_AUTHENTICATION_FAILED_REQUIRED) {
                     var31 = JOptionPane.showOptionDialog(var30, var3, "Login Failed", 1, 0, (Icon)null, new String[]{"Back To Login Screen", "EXIT", "Uplink", "Register on www.star-made.org"}, "Back To Login Screen");
                  } else {
                     var31 = JOptionPane.showOptionDialog(var30, var3, "Login Failed", 1, 0, (Icon)null, new String[]{"Back To Login Screen", "EXIT"}, "Back To Login Screen");
                  }

                  switch(var31) {
                  case 0:
                     connectDialog.exit = false;
                     (new Thread(new Runnable() {
                        public void run() {
                           Starter.clientStartup();
                        }
                     })).start();
                     break;
                  case 1:
                  default:
                     System.err.println("[CLIENT] Exiting because of login failed");

                     try {
                        throw new Exception("System.exit() called");
                     } catch (Exception var24) {
                        var24.printStackTrace();
                        System.exit(0);
                        break;
                     }
                  case 2:
                     LoginDialog var32 = new LoginDialog(new JFrame(), this);
                     connectDialog.setAlwaysOnTop(false);
                     var32.setAlwaysOnTop(true);
                     var32.setDefaultCloseOperation(2);
                     var32.setVisible(true);
                     var1 = false;
                     break;
                  case 3:
                     try {
                        openWebpage((new URL("https://registry.star-made.org/users/sign_up")).toURI());
                     } catch (URISyntaxException var17) {
                        var17.printStackTrace();
                     } catch (MalformedURLException var18) {
                        var18.printStackTrace();
                     }
                  }
               }
            } catch (Exception var25) {
               var3 = null;
               var25.printStackTrace();
            }
         } catch (Exception var27) {
            var2 = var27;

            try {
               var2.printStackTrace();
               if (GameServerState.isCreated()) {
                  var3 = Lng.ORG_SCHEMA_GAME_COMMON_CLIENTRUNNABLE_2;
               } else {
                  var3 = var2.getClass().getSimpleName() + ": " + var2.getMessage();
                  if (var2.getCause() != null) {
                     var3 = var3 + StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CLIENTRUNNABLE_3, var2.getCause().getClass().getSimpleName(), var2.getCause().getMessage());
                  }

                  if (var2 instanceof ConnectException) {
                     var3 = var3 + StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CLIENTRUNNABLE_4, this.server.host, this.server.port);
                  }
               }

               if (this.context != null) {
                  var1 = false;
                  Starter.stopClient(this.context);
                  this.context.handleError(var3);
                  System.err.println("[LOGINERROR] " + var3);
                  return;
               }

               switch(JOptionPane.showOptionDialog((Component)null, var3, "ERROR", 1, 0, (Icon)null, new String[]{Lng.ORG_SCHEMA_GAME_COMMON_CLIENTRUNNABLE_5, Lng.ORG_SCHEMA_GAME_COMMON_CLIENTRUNNABLE_7}, Lng.ORG_SCHEMA_GAME_COMMON_CLIENTRUNNABLE_6)) {
               case 0:
                  connectDialog.exit = false;
                  Starter.clientStartup();
                  return;
               case 1:
                  System.err.println("Exiting because of failed conenct");

                  try {
                     throw new Exception("System.exit() called");
                  } catch (Exception var22) {
                     var22.printStackTrace();
                     System.exit(0);
                     return;
                  }
               case 2:
                  System.err.println("Exiting because of failed conenct");

                  try {
                     throw new Exception("System.exit() called");
                  } catch (Exception var21) {
                     var21.printStackTrace();
                     System.exit(0);
                  }
               }
            } catch (Exception var23) {
               var3 = null;
               var23.printStackTrace();
            }

            return;
         }
      } finally {
         if (var1) {
            System.err.println("[CLIENT] REMOVING CONNECTION DIALOG");
            connectDialog.dispose();
         }

      }

   }

   public void callback() {
      this.run();
   }

   public void stopClient() {
      if (Controller.getResLoader() != null) {
         Controller.getResLoader().onStopClient();
      }

      if (this.gameClientController != null) {
         this.gameClientController.onStopClient();
      }

   }
}
