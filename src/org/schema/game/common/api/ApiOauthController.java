package org.schema.game.common.api;

import com.sun.net.ssl.KeyManager;
import com.sun.net.ssl.SSLContext;
import com.sun.net.ssl.TrustManager;
import com.sun.net.ssl.X509TrustManager;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import javax.net.ssl.HttpsURLConnection;
import javax.swing.JDialog;
import javax.swing.JFrame;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.schema.schine.auth.exceptions.BadUplinkTokenException;
import org.schema.schine.auth.exceptions.WrongUserNameOrPasswordException;

public class ApiOauthController {
   public static final String REQUEST_SELF_URL = "https://registry.star-made.org/api/v1/users/me.json";
   public static final String AUTHTOKEN_REQ_URL = "https://registry.star-made.org/api/v1/users/login_request.json";
   public static final String AUTHTOKEN_VERIFY_URL = "https://registry.star-made.org/api/v1/servers/login_request";
   public static final String APIURL_BLUEPRINT = "https://registry.star-made.org/api/v1/blueprints.json";
   public static final String URL = "https://registry.star-made.org";
   public static final String TOKEN_SERVER_URL = "https://registry.star-made.org/oauth/token";
   public static final String DEDICATED_SERVER_TOKEN_SERVER = "https://registry.star-made.org/tbd";
   public static String testUser = "test";
   public static String testPW = "thisisatestaccount";
   static TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
      public final X509Certificate[] getAcceptedIssuers() {
         return null;
      }

      public final void checkClientTrusted(X509Certificate[] var1, String var2) {
      }

      public final void checkServerTrusted(X509Certificate[] var1, String var2) {
      }

      public final boolean isClientTrusted(X509Certificate[] var1) {
         return true;
      }

      public final boolean isServerTrusted(X509Certificate[] var1) {
         return true;
      }
   }};

   public static String uplinkDedicatedServer(String var0) throws IOException, BadUplinkTokenException {
      var0 = "";
      HttpURLConnection var1;
      (var1 = (HttpURLConnection)(new URL("https://registry.star-made.org/tbd")).openConnection()).setDoOutput(true);
      var1.setReadTimeout(20000);
      var1.setRequestMethod("POST");
      OutputStreamWriter var2;
      (var2 = new OutputStreamWriter(var1.getOutputStream())).write(var0);
      var2.flush();
      if (var1.getResponseCode() == 401) {
         throw new BadUplinkTokenException("An incorrect secure uplink token has been entered in the server.cfg file. Please check 'SECURE_UPLINK_TOKEN' is correct.");
      } else {
         BufferedReader var3 = new BufferedReader(new InputStreamReader(var1.getInputStream()));
         StringBuilder var6 = new StringBuilder();

         String var4;
         while((var4 = var3.readLine()) != null) {
            var6.append(var4);
         }

         JSONObject var5 = new JSONObject(var6.toString());
         var3.close();
         return var5.get("access_token").toString();
      }
   }

   public static String login(String var0, String var1) throws IOException, WrongUserNameOrPasswordException {
      String var2 = "";
      var2 = var2 + URLEncoder.encode("grant_type", "UTF-8") + "=" + URLEncoder.encode("password", "UTF-8");
      var2 = var2 + "&" + URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(var0, "UTF-8");
      var2 = var2 + "&" + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(var1, "UTF-8");
      var2 = var2 + "&" + URLEncoder.encode("scope", "UTF-8") + "=" + URLEncoder.encode("public read_citizen_info client", "UTF-8");
      HttpURLConnection var3;
      (var3 = (HttpURLConnection)(new URL("https://registry.star-made.org/oauth/token")).openConnection()).setDoOutput(true);
      var3.setReadTimeout(20000);
      var3.setRequestMethod("POST");
      System.currentTimeMillis();
      OutputStreamWriter var5;
      (var5 = new OutputStreamWriter(var3.getOutputStream())).write(var2);
      var5.flush();
      if (var3.getResponseCode() == 401) {
         throw new WrongUserNameOrPasswordException("Your credentials are invalid. If you need to reset them, visit registry.star-made.org.");
      } else {
         BufferedReader var4 = new BufferedReader(new InputStreamReader(var3.getInputStream()));
         StringBuffer var7 = new StringBuffer();

         while((var1 = var4.readLine()) != null) {
            var7.append(var1);
         }

         System.currentTimeMillis();
         JSONObject var6 = new JSONObject(var7.toString());
         var4.close();
         return var6.get("access_token").toString();
      }
   }

   public static HttpURLConnection getTokenConnection(String var0) throws IOException {
      HttpsURLConnection var1;
      (var1 = (HttpsURLConnection)(new URL(var0)).openConnection()).setDoOutput(true);
      var1.setReadTimeout(20000);
      return var1;
   }

   public static JSONObject getResponse(HttpURLConnection var0) throws IOException {
      var0.getResponseCode();
      BufferedReader var3 = new BufferedReader(new InputStreamReader(var0.getInputStream()));
      StringBuffer var2 = new StringBuffer();

      String var1;
      while((var1 = var3.readLine()) != null) {
         var2.append(var1);
      }

      var3.close();
      return new JSONObject(var2.toString());
   }

   public static JSONObject requestSelf(String var0) throws IOException {
      HttpURLConnection var1;
      (var1 = getTokenConnection("https://registry.star-made.org/api/v1/users/me.json")).setRequestProperty("Authorization", "Bearer " + var0);
      var1.setRequestMethod("GET");
      return getResponse(var1);
   }

   public static JSONObject getAllUploads(String var0, JFrame var1) throws IOException {
      HttpURLConnection var2;
      (var2 = getTokenConnection("https://registry.star-made.org/api/v1/blueprints.json")).setRequestProperty("Authorization", "Bearer " + var0);
      var2.setRequestMethod("GET");
      return getResponse(var2);
   }

   public static void upload(String var0, File var1, String var2, int var3, String var4, String var5, JDialog var6) throws IOException {
      CloseableHttpClient var11 = HttpClientBuilder.create().build();
      HttpPost var12;
      (var12 = new HttpPost("https://registry.star-made.org/api/v1/blueprints.json")).addHeader("Authorization", "Bearer " + var0);
      MultipartEntityBuilder var7;
      (var7 = MultipartEntityBuilder.create()).setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
      FileBody var10 = new FileBody(var1);
      var7.addPart("file", var10);
      var7.addPart("blueprint_type", new StringBody(String.valueOf(var3), ContentType.DEFAULT_TEXT));
      var7.addPart("name", new StringBody(var2, ContentType.DEFAULT_TEXT));
      var7.addPart("description", new StringBody(var4, ContentType.DEFAULT_TEXT));
      HttpEntity var8 = var7.build();
      var12.setEntity(var8);
      HttpResponse var9 = var11.execute(var12);
      var0 = (new BasicResponseHandler()).handleResponse(var9);
      System.err.println("UPLOAD RESPONSE: " + var0);
      new JSONObject(var0);
   }

   public static String requestAuthToken(String var0) throws IOException {
      System.err.println("[OAuth2] Requesting Auth Token https://registry.star-made.org/api/v1/users/login_request.json");
      HttpURLConnection var1;
      (var1 = getTokenConnection("https://registry.star-made.org/api/v1/users/login_request.json")).setRequestMethod("POST");
      var1.setRequestProperty("Authorization", "Bearer " + var0);
      JSONObject var2;
      if ((var2 = getResponse(var1)).has("error")) {
         System.err.println("[OAuth2] Error has been detected in response " + var2.toString());
      } else {
         System.err.println("[OAuth2] server_auth token aquired");
      }

      return var2.get("token").toString();
   }

   public static JSONObject verifyAuthToken(String var0, String var1) throws IOException {
      System.err.println("[OAuth2] Verifying Auth Token '" + var0 + "'");
      HttpURLConnection var2;
      (var2 = getTokenConnection("https://registry.star-made.org/api/v1/servers/login_request")).setRequestMethod("POST");
      String var3 = "";
      var3 = var3 + URLEncoder.encode("token", "UTF-8") + "=" + URLEncoder.encode(var0, "UTF-8");
      var3 = var3 + "&" + URLEncoder.encode("address", "UTF-8") + "=" + URLEncoder.encode(var1, "UTF-8");
      OutputStreamWriter var4;
      (var4 = new OutputStreamWriter(var2.getOutputStream())).write(var3);
      var4.flush();
      JSONObject var5;
      (var5 = getResponse(var2)).toString();
      var4.close();
      return var5;
   }

   public static void main(String[] var0) throws IOException {
      if (var0 != null && var0.length == 2) {
         testUser = var0[0];
         testPW = var0[1];
      }

      String var1 = URLEncoder.encode("grant_type", "UTF-8") + "=" + URLEncoder.encode("password", "UTF-8");
      var1 = var1 + "&" + URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(testUser, "UTF-8");
      var1 = var1 + "&" + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(testPW, "UTF-8");
      System.err.println(var1);
      HttpURLConnection var7;
      (var7 = (HttpURLConnection)(new URL("https://registry.star-made.org/oauth/token")).openConnection()).setDoOutput(true);
      var7.setReadTimeout(20000);
      var7.setRequestMethod("POST");
      long var3 = System.currentTimeMillis();
      OutputStreamWriter var2;
      (var2 = new OutputStreamWriter(var7.getOutputStream())).write(var1);
      var2.flush();
      var7.getResponseCode();
      BufferedReader var9 = new BufferedReader(new InputStreamReader(var7.getInputStream()));
      StringBuffer var10 = new StringBuffer();

      String var8;
      while((var8 = var9.readLine()) != null) {
         System.err.println(var8);
         var10.append(var8);
      }

      long var5 = System.currentTimeMillis() - var3;
      var8 = (new JSONObject(var10.toString())).get("access_token").toString();
      System.err.println("TOKEN: " + var8 + " (TIME: " + var5 + "ms)");
      var9.close();
      HttpURLConnection var11;
      (var11 = (HttpURLConnection)(new URL("https://registry.star-made.org/api/v1/users/me.json")).openConnection()).setDoOutput(true);
      var11.setReadTimeout(20000);
      var11.setRequestProperty("Authorization", "Bearer " + var8);
      var11.getResponseCode();
      BufferedReader var12 = new BufferedReader(new InputStreamReader(var11.getInputStream()));

      String var4;
      while((var4 = var12.readLine()) != null) {
         System.err.println(var4);
      }

      var12.close();
   }

   static {
      try {
         SSLContext var0;
         (var0 = SSLContext.getInstance("SSL")).init((KeyManager[])null, trustAllCerts, new SecureRandom());
         HttpsURLConnection.setDefaultSSLSocketFactory(var0.getSocketFactory());
      } catch (Exception var1) {
         System.out.println(var1);
      }
   }
}
