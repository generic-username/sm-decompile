package org.schema.game.common.api.exceptions;

public class AlreadyLoggedInException extends Exception {
   private static final long serialVersionUID = 1L;
}
