package org.schema.game.common.api.exceptions;

public class LoginFailedException extends Exception {
   private static final long serialVersionUID = 1L;

   public LoginFailedException(int var1) {
      super("server response code: " + var1);
   }
}
