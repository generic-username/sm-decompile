package org.schema.game.common.api;

public class FileInfo {
   public String uri;
   public String fid;
   public String name;
   public String uid;
   public int size;

   public String toString() {
      return "FileInfo [uri=" + this.uri + ", fid=" + this.fid + "]";
   }
}
