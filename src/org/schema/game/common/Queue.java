package org.schema.game.common;

import java.util.LinkedList;

class Queue {
   private LinkedList queue = new LinkedList();
   private boolean shutdown;

   public void shutdown() {
      this.shutdown = true;
      synchronized(this.queue) {
         this.queue.notify();
      }
   }

   void add(Object var1) {
      synchronized(this.queue) {
         this.queue.add(var1);
         this.queue.notify();
      }
   }

   boolean contains(Object var1) {
      synchronized(this.queue) {
         return this.queue.contains(var1);
      }
   }

   Object take() throws InterruptedException {
      synchronized(this.queue) {
         do {
            if (!this.queue.isEmpty()) {
               return this.queue.removeFirst();
            }

            this.queue.wait();
         } while(!this.shutdown);

         return null;
      }
   }
}
