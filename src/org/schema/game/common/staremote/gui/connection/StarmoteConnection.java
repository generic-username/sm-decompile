package org.schema.game.common.staremote.gui.connection;

public class StarmoteConnection {
   public String username;
   public String url;
   public int port;

   public StarmoteConnection(String var1, int var2, String var3) {
      this.url = var1;
      this.port = var2;
      this.username = var3;
   }

   public int hashCode() {
      return this.username.hashCode() + this.url.hashCode() + this.port;
   }

   public boolean equals(Object var1) {
      return this.username.equals(((StarmoteConnection)var1).username) && this.url.equals(((StarmoteConnection)var1).url) && this.port == ((StarmoteConnection)var1).port;
   }

   public String toString() {
      return this.username + "@" + this.url + ":" + this.port;
   }
}
