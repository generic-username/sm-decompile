package org.schema.game.common.staremote.gui.connection;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.schema.common.ListAction;
import org.schema.game.common.gui.LoginDialog;
import org.schema.game.common.staremote.Staremote;

public class StarmoteServerPanel extends JPanel {
   private static final long serialVersionUID = 1L;
   Boolean started = false;
   private Object o = new Object();
   private JButton btnRemoveConnection;
   private JButton btnEditConnection;

   public StarmoteServerPanel(final JFrame var1, final Staremote var2) {
      GridBagLayout var3;
      (var3 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var3.rowHeights = new int[]{0, 0, 0};
      var3.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var3.rowWeights = new double[]{1.0D, 1.0D, Double.MIN_VALUE};
      this.setLayout(var3);
      JPanel var9 = new JPanel();
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).weighty = 5.0D;
      var4.weightx = 1.0D;
      var4.insets = new Insets(0, 0, 5, 0);
      var4.fill = 1;
      var4.gridx = 0;
      var4.gridy = 0;
      this.add(var9, var4);
      GridBagLayout var11;
      (var11 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var11.rowHeights = new int[]{0, 0};
      var11.columnWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var11.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var9.setLayout(var11);
      JPanel var12 = new JPanel();
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).weightx = 1.0D;
      var5.insets = new Insets(0, 0, 5, 0);
      var5.fill = 1;
      var5.gridx = 0;
      var5.gridy = 0;
      var9.add(var12, var5);
      GridBagLayout var14;
      (var14 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var14.rowHeights = new int[]{0, 0};
      var14.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var14.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var12.setLayout(var14);
      JScrollPane var16 = new JScrollPane();
      GridBagConstraints var6;
      (var6 = new GridBagConstraints()).fill = 1;
      var6.gridx = 0;
      var6.gridy = 0;
      var12.add(var16, var6);
      final StarmodeConnectionListModel var13 = new StarmodeConnectionListModel();
      final JList var18 = new JList(var13);
      var16.setViewportView(var18);
      JPanel var17 = new JPanel();
      GridBagConstraints var7;
      (var7 = new GridBagConstraints()).weightx = 0.001D;
      var7.fill = 1;
      var7.gridx = 1;
      var7.gridy = 0;
      var9.add(var17, var7);
      (var3 = new GridBagLayout()).columnWidths = new int[]{0};
      var3.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
      var3.columnWeights = new double[]{Double.MIN_VALUE};
      var3.rowWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var17.setLayout(var3);
      final JButton var10;
      (var10 = new JButton("Connect")).setEnabled(false);
      var10.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            Object var2x;
            if ((var2x = var18.getSelectedValue()) != null) {
               StarmoteConnection var3 = (StarmoteConnection)var2x;
               var1.dispose();
               var2.connect(var3);
            }

         }
      });
      (var7 = new GridBagConstraints()).fill = 2;
      var7.anchor = 13;
      var7.insets = new Insets(0, 0, 5, 0);
      var7.gridx = 0;
      var7.gridy = 0;
      var17.add(var10, var7);
      JButton var20;
      (var20 = new JButton("Add Connection")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            (new StarmoteAddConnectionDialog(var1, var13, (StarmoteConnection)null)).setVisible(true);
         }
      });
      GridBagConstraints var8;
      (var8 = new GridBagConstraints()).fill = 2;
      var8.anchor = 13;
      var8.insets = new Insets(0, 0, 5, 0);
      var8.gridx = 0;
      var8.gridy = 2;
      var17.add(var20, var8);
      this.btnEditConnection = new JButton("Edit Connection ");
      this.btnEditConnection.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            Object var2;
            if ((var2 = var18.getSelectedValue()) != null) {
               StarmoteConnection var3 = (StarmoteConnection)var2;
               var10.setEnabled(false);
               StarmoteServerPanel.this.btnRemoveConnection.setEnabled(false);
               StarmoteServerPanel.this.btnEditConnection.setEnabled(false);
               (new StarmoteAddConnectionDialog(var1, var13, var3)).setVisible(true);
            }

         }
      });
      this.btnEditConnection.setEnabled(false);
      (var7 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var7.fill = 2;
      var7.anchor = 13;
      var7.gridx = 0;
      var7.gridy = 3;
      var17.add(this.btnEditConnection, var7);
      this.btnRemoveConnection = new JButton("Remove Connection");
      this.btnRemoveConnection.setEnabled(false);
      this.btnRemoveConnection.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            Object var2;
            if ((var2 = var18.getSelectedValue()) != null) {
               var10.setEnabled(false);
               StarmoteServerPanel.this.btnRemoveConnection.setEnabled(false);
               StarmoteServerPanel.this.btnEditConnection.setEnabled(false);
               StarmoteConnection var3 = (StarmoteConnection)var2;
               var13.remove(var3);
            }

         }
      });
      JButton var15;
      (var15 = new JButton("Uplink Settings")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            LoginDialog var2;
            (var2 = new LoginDialog(var1)).setDefaultCloseOperation(2);
            var2.setVisible(true);
         }
      });
      (var7 = new GridBagConstraints()).anchor = 13;
      var7.fill = 2;
      var7.insets = new Insets(0, 0, 5, 0);
      var7.gridx = 0;
      var7.gridy = 5;
      var17.add(var15, var7);
      (var4 = new GridBagConstraints()).fill = 2;
      var4.anchor = 13;
      var4.gridx = 0;
      var4.gridy = 7;
      var17.add(this.btnRemoveConnection, var4);
      var12 = new JPanel();
      (var5 = new GridBagConstraints()).weighty = 1.0D;
      var5.weightx = 0.2D;
      var5.fill = 1;
      var5.gridx = 0;
      var5.gridy = 1;
      this.add(var12, var5);
      (var14 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var14.rowHeights = new int[]{0, 0};
      var14.columnWeights = new double[]{0.0D, Double.MIN_VALUE};
      var14.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var12.setLayout(var14);
      JButton var19;
      (var19 = new JButton("   Exit   ")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            var1.dispose();
         }
      });
      (var7 = new GridBagConstraints()).insets = new Insets(5, 0, 5, 5);
      var7.weightx = 1.0D;
      var7.anchor = 13;
      var7.gridx = 0;
      var7.gridy = 0;
      var12.add(var19, var7);
      var18.addListSelectionListener(new ListSelectionListener() {
         public void valueChanged(ListSelectionEvent var1) {
            var10.setEnabled(var18.getSelectedIndex() >= 0);
            StarmoteServerPanel.this.btnRemoveConnection.setEnabled(var18.getSelectedIndex() >= 0);
            StarmoteServerPanel.this.btnEditConnection.setEnabled(var18.getSelectedIndex() >= 0);
         }
      });
      var18.addMouseListener(new ListAction(var18, new Action() {
         public Object getValue(String var1x) {
            return null;
         }

         public void actionPerformed(ActionEvent var1x) {
            synchronized(StarmoteServerPanel.this.o) {
               if (StarmoteServerPanel.this.started) {
                  return;
               }

               StarmoteServerPanel.this.started = true;
            }

            StarmoteConnection var4 = (StarmoteConnection)var18.getSelectedValue();
            var1.dispose();
            var2.connect(var4);
         }

         public void putValue(String var1x, Object var2x) {
         }

         public void addPropertyChangeListener(PropertyChangeListener var1x) {
         }

         public boolean isEnabled() {
            return false;
         }

         public void removePropertyChangeListener(PropertyChangeListener var1x) {
         }

         public void setEnabled(boolean var1x) {
         }
      }));
   }
}
