package org.schema.game.common.staremote.gui.connection;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.AbstractListModel;
import org.schema.game.common.staremote.Staremote;
import org.schema.schine.resource.FileExt;

public class StarmodeConnectionListModel extends AbstractListModel {
   private static final long serialVersionUID = 1L;
   public final ArrayList list = new ArrayList();

   public StarmodeConnectionListModel() {
      try {
         this.load();
      } catch (NumberFormatException var1) {
         var1.printStackTrace();
      } catch (IOException var2) {
         var2.printStackTrace();
      }
   }

   public void add(StarmoteConnection var1) {
      this.list.add(var1);
      this.save();

      try {
         this.load();
      } catch (NumberFormatException var2) {
         var2.printStackTrace();
      } catch (IOException var3) {
         var3.printStackTrace();
      }
   }

   public int getSize() {
      return this.list.size();
   }

   public Object getElementAt(int var1) {
      return this.list.get(var1);
   }

   public void load() throws NumberFormatException, IOException {
      this.list.clear();
      FileExt var1;
      if (!(var1 = new FileExt(Staremote.getConnectionFilePath())).exists()) {
         throw new FileNotFoundException();
      } else {
         BufferedReader var5 = new BufferedReader(new FileReader(var1));

         String var2;
         while((var2 = var5.readLine()) != null) {
            String[] var6;
            String var3 = (var6 = var2.split(",", 21))[0];
            String var4 = (var6 = var6[1].split(":", 2))[0];
            int var7 = Integer.parseInt(var6[1]);
            StarmoteConnection var8 = new StarmoteConnection(var4, var7, var3);
            this.list.add(var8);
         }

         var5.close();
         this.fireContentsChanged(this, 0, this.list.size() - 1);
      }
   }

   public void remove(StarmoteConnection var1) {
      this.list.remove(var1);
      this.save();

      try {
         this.load();
      } catch (NumberFormatException var2) {
         var2.printStackTrace();
      } catch (IOException var3) {
         var3.printStackTrace();
      }
   }

   public void save() {
      try {
         FileExt var1;
         if (!(var1 = new FileExt(Staremote.getConnectionFilePath())).exists()) {
            var1.createNewFile();
         }

         BufferedWriter var5 = new BufferedWriter(new FileWriter(var1));
         Iterator var2 = this.list.iterator();

         while(var2.hasNext()) {
            StarmoteConnection var3 = (StarmoteConnection)var2.next();
            var5.append(var3.username + "," + var3.url + ":" + var3.port + "\n");
         }

         var5.flush();
         var5.close();
      } catch (Exception var4) {
         var4.printStackTrace();
      }
   }
}
