package org.schema.game.common.staremote.gui.player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import javax.swing.AbstractListModel;
import javax.swing.SwingUtilities;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.network.objects.Sendable;

public class StarmotePlayerList extends AbstractListModel {
   private static final long serialVersionUID = 1L;
   private ArrayList players = new ArrayList();
   private GameClientState state;

   public StarmotePlayerList(GameClientState var1) {
      this.state = var1;
      this.recalcList();
   }

   public int getSize() {
      return this.players.size();
   }

   public Object getElementAt(int var1) {
      try {
         return this.players.get(var1);
      } catch (Exception var2) {
         var2.printStackTrace();
         return "Exception";
      }
   }

   public void recalcList() {
      SwingUtilities.invokeLater(new Runnable() {
         public void run() {
            StarmotePlayerList.this.players.clear();
            synchronized(StarmotePlayerList.this.state) {
               Iterator var2 = StarmotePlayerList.this.state.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

               while(true) {
                  if (!var2.hasNext()) {
                     break;
                  }

                  Sendable var3;
                  if ((var3 = (Sendable)var2.next()) instanceof PlayerState) {
                     StarmotePlayerList.this.players.add((PlayerState)var3);
                  }
               }
            }

            Collections.sort(StarmotePlayerList.this.players, new Comparator() {
               public int compare(PlayerState var1, PlayerState var2) {
                  return var1.getName().compareTo(var2.getName());
               }
            });
            StarmotePlayerList.this.fireContentsChanged(this, 0, StarmotePlayerList.this.players.size());
         }
      });
   }
}
