package org.schema.game.common.staremote.gui.player;

import java.util.Date;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.staremote.gui.entity.StarmoteEntitySettingsPanel;
import org.schema.game.common.staremote.gui.settings.StarmoteSettingButtonTrigger;
import org.schema.game.common.staremote.gui.settings.StarmoteSettingTextLabel;
import org.schema.game.server.data.admin.AdminCommands;

public class StarmotePlayerSettingPanel extends StarmoteEntitySettingsPanel {
   private static final long serialVersionUID = 1L;

   public StarmotePlayerSettingPanel(PlayerState var1) {
      super(var1);
   }

   public void initializeSettings() {
      super.initializeSettings();
      final PlayerState var1 = (PlayerState)this.getSendable();
      this.addSetting(new StarmoteSettingTextLabel("NAME") {
         private static final long serialVersionUID = 1L;

         public String getValue() {
            return "NAME: " + var1.getName();
         }
      });
      this.addSetting(new StarmoteSettingTextLabel("CREDITS") {
         private static final long serialVersionUID = 1L;

         public Object getValue() {
            return var1.getCredits();
         }
      });
      this.addSetting(new StarmoteSettingTextLabel("HEALTH") {
         private static final long serialVersionUID = 1L;

         public Object getValue() {
            return var1.getHealth();
         }
      });
      this.addSetting(new StarmoteSettingTextLabel("SECTOR") {
         private static final long serialVersionUID = 1L;

         public Object getValue() {
            return var1.getCurrentSector().toString();
         }
      });
      this.addSetting(new StarmoteSettingTextLabel("CREATED") {
         private static final long serialVersionUID = 1L;

         public Object getValue() {
            return new Date(var1.getCreationTime());
         }
      });
      this.addSetting(new StarmoteSettingButtonTrigger("KICK") {
         private static final long serialVersionUID = 1L;

         public Object getValue() {
            return "";
         }

         public void trigger() {
            ((GameClientState)var1.getState()).getController().sendAdminCommand(AdminCommands.KICK, var1.getName());
         }
      });
      this.addSetting(new StarmoteSettingButtonTrigger("BAN BY NAME") {
         private static final long serialVersionUID = 1L;

         public Object getValue() {
            return "";
         }

         public void trigger() {
            ((GameClientState)var1.getState()).getController().sendAdminCommand(AdminCommands.BAN, var1.getName(), false);
         }
      });
   }
}
