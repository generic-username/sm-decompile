package org.schema.game.common.staremote.gui.console;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JPanel;
import org.schema.game.client.data.GameClientState;

public class StarmoteConsole extends JPanel {
   private static final long serialVersionUID = 1L;

   public StarmoteConsole(GameClientState var1) {
      GridBagLayout var2;
      (var2 = new GridBagLayout()).columnWidths = new int[]{220, 10};
      var2.rowHeights = new int[]{10, 0, 0};
      var2.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var2.rowWeights = new double[]{0.0D, 1.0D, Double.MIN_VALUE};
      this.setLayout(var2);
      StarmoteChatbar var5 = new StarmoteChatbar(var1);
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.weightx = 1.0D;
      var3.fill = 2;
      var3.anchor = 18;
      var3.gridx = 0;
      var3.gridy = 0;
      this.add(var5, var3);
      StarmoteConsoleOutput var4 = new StarmoteConsoleOutput(var1);
      GridBagConstraints var6;
      (var6 = new GridBagConstraints()).fill = 1;
      var6.gridx = 0;
      var6.gridy = 1;
      this.add(var4, var6);
   }
}
