package org.schema.game.common.staremote.gui.console;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.core.ChatListener;
import org.schema.schine.graphicsengine.core.ChatMessageInterface;

public class StarmoteConsoleOutput extends JPanel implements ChatListener {
   private static final long serialVersionUID = 1L;
   private JTextArea editorPane;

   public StarmoteConsoleOutput(GameClientState var1) {
      this.setLayout(new BorderLayout(0, 0));
      JScrollPane var2 = new JScrollPane();
      this.add(var2, "Center");
      this.editorPane = new JTextArea();
      ((DefaultCaret)this.editorPane.getCaret()).setUpdatePolicy(2);
      var2.setViewportView(this.editorPane);
      var2.setAutoscrolls(true);

      assert var1 != null;

      var1.getChatListeners().add(this);
   }

   public void notifyOfChat(ChatMessageInterface var1) {
      this.editorPane.append(var1.toString() + "\n");
   }
}
