package org.schema.game.common.staremote.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.border.EmptyBorder;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.staremote.Staremote;
import org.schema.game.common.staremote.gui.console.StarmoteConsole;
import org.schema.game.network.StarMadeServerStats;

public class StarmoteFrame extends JFrame {
   private static final long serialVersionUID = 1L;
   public static JFrame self;
   private JPanel contentPane;
   private JLabel lblServerstatus;
   private JLabel lblMemory;
   private Staremote starmote;

   public StarmoteFrame(GameClientState var1, Staremote var2) {
      self = this;
      this.starmote = var2;
      this.setTitle("StarMote (StarMade Remote Admin Tool)");
      this.setDefaultCloseOperation(2);
      this.setBounds(100, 100, 1024, 768);
      this.contentPane = new JPanel();
      this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.setContentPane(this.contentPane);
      GridBagLayout var3;
      (var3 = new GridBagLayout()).columnWidths = new int[]{424, 0};
      var3.rowHeights = new int[]{252, 30};
      var3.columnWeights = new double[]{1.0D};
      var3.rowWeights = new double[]{5.0D, 1.0D};
      this.contentPane.setLayout(var3);
      JSplitPane var10;
      (var10 = new JSplitPane()).setDividerSize(3);
      var10.setOrientation(0);
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).weighty = 30.0D;
      var4.insets = new Insets(0, 0, 5, 0);
      var4.fill = 1;
      var4.gridx = 0;
      var4.gridy = 0;
      this.contentPane.add(var10, var4);
      StarmoteMainTabsPanel var7;
      (var7 = new StarmoteMainTabsPanel(var1, var2)).setPreferredSize(new Dimension(104, 300));
      var10.setLeftComponent(var7);
      StarmoteConsole var5 = new StarmoteConsole(var1);
      var10.setRightComponent(var5);
      var10.setDividerLocation(500);
      JPanel var6;
      (var6 = new JPanel()).setMaximumSize(new Dimension(60, 20));
      var6.setMinimumSize(new Dimension(10, 20));
      var6.setPreferredSize(new Dimension(10, 20));
      GridBagConstraints var8;
      (var8 = new GridBagConstraints()).weighty = 1.0D;
      var8.anchor = 11;
      var8.fill = 1;
      var8.gridx = 0;
      var8.gridy = 1;
      this.contentPane.add(var6, var8);
      GridBagLayout var9;
      (var9 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var9.rowHeights = new int[]{0, 0, 0};
      var9.columnWeights = new double[]{0.0D, Double.MIN_VALUE};
      var9.rowWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var6.setLayout(var9);
      this.lblServerstatus = new JLabel("ServerStatus: ");
      this.lblServerstatus.setPreferredSize(new Dimension(180, 14));
      this.lblServerstatus.setMinimumSize(new Dimension(180, 40));
      this.lblServerstatus.setMaximumSize(new Dimension(180, 40));
      (var8 = new GridBagConstraints()).fill = 2;
      var8.weighty = 1.0D;
      var8.anchor = 18;
      var8.insets = new Insets(0, 5, 3, 0);
      var8.gridx = 0;
      var8.gridy = 0;
      var6.add(this.lblServerstatus, var8);
      this.lblMemory = new JLabel("Memory");
      this.lblMemory.setMinimumSize(new Dimension(38, 180));
      this.lblMemory.setMaximumSize(new Dimension(38, 180));
      (var8 = new GridBagConstraints()).fill = 2;
      var8.insets = new Insets(0, 5, 0, 0);
      var8.weighty = 1.0D;
      var8.anchor = 18;
      var8.gridx = 0;
      var8.gridy = 1;
      var6.add(this.lblMemory, var8);
   }

   public void dispose() {
      this.starmote.exit();
      super.dispose();
   }

   public void updateStats(StarMadeServerStats var1) {
      this.lblServerstatus.setText("SERVER PING: " + var1.ping + " ms");
      this.lblMemory.setText("SERVER MEMORY: " + var1.takenMemory / 1024L / 1024L + " / " + var1.totalMemory / 1024L / 1024L + " MB");
   }
}
