package org.schema.game.common.staremote.gui.catalog.edit;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.game.common.staremote.gui.catalog.StarmoteCatalogTableModel;

public class StarmoteCatalogEditDialog extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private final ButtonGroup buttonGroup = new ButtonGroup();
   private final CatalogPermission p;
   private final GameClientState gState;
   private JTextField nameField;
   private JTextField ownerField;
   private JSpinner priceSpinner;
   private JTextField descField;
   private JCheckBox chckbxSpawnableFromHombases;
   private JCheckBox chckbxSpawnableByFaction;
   private JCheckBox chckbxSpawnableByEnemies;
   private JCheckBox chckbxSpawnableByOthers;

   public StarmoteCatalogEditDialog(JFrame var1, final CatalogPermission var2, GameClientState var3, final StarmoteCatalogTableModel var4, final JTable var5) {
      super(var1, true);
      this.setAlwaysOnTop(true);
      this.gState = var3;
      this.p = var2;
      this.setTitle("Edit Catalog Entry");
      this.setDefaultCloseOperation(2);
      this.setBounds(400, 400, 550, 500);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var6;
      (var6 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var6.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
      var6.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var6.rowWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D, 1.0D, 0.0D, Double.MIN_VALUE};
      this.contentPanel.setLayout(var6);
      JPanel var7;
      (var7 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Name", 4, 2, (Font)null, new Color(0, 0, 0)));
      GridBagConstraints var8;
      (var8 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var8.fill = 1;
      var8.gridx = 0;
      var8.gridy = 0;
      this.contentPanel.add(var7, var8);
      GridBagLayout var10;
      (var10 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var10.rowHeights = new int[]{0, 0};
      var10.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var10.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var7.setLayout(var10);
      this.nameField = new JTextField();
      (var8 = new GridBagConstraints()).fill = 2;
      var8.gridx = 0;
      var8.gridy = 0;
      var7.add(this.nameField, var8);
      this.nameField.setColumns(10);
      (var7 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Owner", 4, 2, (Font)null, new Color(0, 0, 0)));
      (var8 = new GridBagConstraints()).fill = 1;
      var8.insets = new Insets(0, 0, 5, 0);
      var8.gridx = 0;
      var8.gridy = 1;
      this.contentPanel.add(var7, var8);
      (var10 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var10.rowHeights = new int[]{0, 0};
      var10.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var10.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var7.setLayout(var10);
      this.ownerField = new JTextField();
      (var8 = new GridBagConstraints()).fill = 2;
      var8.gridx = 0;
      var8.gridy = 0;
      var7.add(this.ownerField, var8);
      this.ownerField.setColumns(10);
      (var7 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Price", 4, 2, (Font)null, new Color(0, 0, 0)));
      (var8 = new GridBagConstraints()).fill = 1;
      var8.insets = new Insets(0, 0, 5, 0);
      var8.gridx = 0;
      var8.gridy = 2;
      this.contentPanel.add(var7, var8);
      (var10 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var10.rowHeights = new int[]{0, 0};
      var10.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var10.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var7.setLayout(var10);
      this.priceSpinner = new JSpinner();
      (var8 = new GridBagConstraints()).fill = 2;
      var8.gridx = 0;
      var8.gridy = 0;
      var7.add(this.priceSpinner, var8);
      (var7 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Description", 4, 2, (Font)null, new Color(0, 0, 0)));
      (var8 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var8.fill = 1;
      var8.gridx = 0;
      var8.gridy = 3;
      this.contentPanel.add(var7, var8);
      (var10 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var10.rowHeights = new int[]{0, 0};
      var10.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var10.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var7.setLayout(var10);
      this.descField = new JTextField();
      (var8 = new GridBagConstraints()).fill = 2;
      var8.gridx = 0;
      var8.gridy = 0;
      var7.add(this.descField, var8);
      this.descField.setColumns(10);
      (var7 = new JPanel()).setBorder(new TitledBorder((Border)null, "Permissions", 4, 2, (Font)null, (Color)null));
      (var8 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var8.fill = 1;
      var8.gridx = 0;
      var8.gridy = 4;
      this.contentPanel.add(var7, var8);
      (var10 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var10.rowHeights = new int[]{0, 0, 0, 0, 0};
      var10.columnWeights = new double[]{0.0D, Double.MIN_VALUE};
      var10.rowWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var7.setLayout(var10);
      this.chckbxSpawnableFromHombases = new JCheckBox("Spawnable from hombases");
      (var8 = new GridBagConstraints()).anchor = 17;
      var8.insets = new Insets(0, 0, 5, 0);
      var8.gridx = 0;
      var8.gridy = 0;
      var7.add(this.chckbxSpawnableFromHombases, var8);
      this.chckbxSpawnableByFaction = new JCheckBox("Spawnable by faction ");
      (var8 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var8.anchor = 17;
      var8.gridx = 0;
      var8.gridy = 1;
      var7.add(this.chckbxSpawnableByFaction, var8);
      this.chckbxSpawnableByEnemies = new JCheckBox("Spawnable by enemies");
      (var8 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var8.anchor = 17;
      var8.gridx = 0;
      var8.gridy = 2;
      var7.add(this.chckbxSpawnableByEnemies, var8);
      this.chckbxSpawnableByOthers = new JCheckBox("Spawnable by others");
      (var8 = new GridBagConstraints()).anchor = 17;
      var8.gridx = 0;
      var8.gridy = 3;
      var7.add(this.chckbxSpawnableByOthers, var8);
      JButton var9;
      (var9 = new JButton("Delete Entry")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            StarmoteCatalogEditDialog.this.gState.getCatalogManager().clientRequestCatalogRemove(var2);
         }
      });
      (var8 = new GridBagConstraints()).gridx = 0;
      var8.gridy = 5;
      this.contentPanel.add(var9, var8);
      (var7 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var7, "South");
      JButton var11;
      (var11 = new JButton("OK")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            StarmoteCatalogEditDialog.this.apply();
            var4.rebuild(StarmoteCatalogEditDialog.this.gState);
            StarmoteCatalogEditDialog.this.dispose();
            var5.removeColumnSelectionInterval(0, var5.getColumnCount());
            var5.invalidate();
            var5.validate();
            var5.repaint();
         }
      });
      var11.setActionCommand("OK");
      var7.add(var11);
      this.getRootPane().setDefaultButton(var11);
      (var11 = new JButton("Cancel")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            StarmoteCatalogEditDialog.this.dispose();
         }
      });
      var11.setActionCommand("Cancel");
      var7.add(var11);
      this.setOriginalSettings();
   }

   public void apply() {
      CatalogPermission var1;
      (var1 = new CatalogPermission(this.p)).setUid(this.nameField.getText().trim());
      var1.ownerUID = this.ownerField.getText().trim();
      var1.price = (long)(Integer)this.priceSpinner.getValue();
      var1.description = this.descField.getText();
      var1.setPermission(this.chckbxSpawnableByEnemies.isSelected(), 16);
      var1.setPermission(this.chckbxSpawnableByFaction.isSelected(), 1);
      var1.setPermission(this.chckbxSpawnableByOthers.isSelected(), 2);
      var1.setPermission(this.chckbxSpawnableFromHombases.isSelected(), 4);
      this.gState.getCatalogManager().clientRequestCatalogEdit(var1);
   }

   private void setOriginalSettings() {
      this.nameField.setText(this.p.getUid());
      this.ownerField.setText(this.p.ownerUID);
      this.priceSpinner.setValue(this.p.price);
      this.descField.setText(this.p.description);
      this.chckbxSpawnableByEnemies.setSelected(this.p.enemyUsable());
      this.chckbxSpawnableByFaction.setSelected(this.p.faction());
      this.chckbxSpawnableByOthers.setSelected(this.p.others());
      this.chckbxSpawnableFromHombases.setSelected(this.p.homeOnly());
   }
}
