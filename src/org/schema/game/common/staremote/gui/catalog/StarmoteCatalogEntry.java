package org.schema.game.common.staremote.gui.catalog;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.Locale;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.game.common.staremote.gui.StarmoteFrame;
import org.schema.game.common.staremote.gui.catalog.edit.StarmoteCatalogEditDialog;

public class StarmoteCatalogEntry implements Comparable {
   private final CatalogPermission p;
   private final GameClientState state;
   private StarmoteCatalogTableModel model;

   public StarmoteCatalogEntry(CatalogPermission var1, GameClientState var2, StarmoteCatalogTableModel var3) {
      this.p = var1;
      this.state = var2;
      this.model = var3;
   }

   public int compareTo(StarmoteCatalogEntry var1) {
      return this.p.getUid().toLowerCase(Locale.ENGLISH).compareTo(var1.p.getUid().toLowerCase(Locale.ENGLISH));
   }

   public Color getColor(int var1) {
      switch(var1) {
      case 0:
         return null;
      case 1:
         return null;
      case 2:
         return null;
      case 3:
         return null;
      case 4:
         return null;
      case 5:
         return null;
      case 6:
         return null;
      case 7:
         if (this.p.enemyUsable()) {
            return Color.GREEN;
         }

         return Color.RED;
      case 8:
         if (this.p.faction()) {
            return Color.GREEN;
         }

         return Color.RED;
      case 9:
         if (this.p.homeOnly()) {
            return Color.GREEN;
         }

         return Color.RED;
      case 10:
         if (this.p.others()) {
            return Color.GREEN;
         }

         return Color.RED;
      case 11:
         return null;
      default:
         return null;
      }
   }

   public Component getComponent(int var1, final JTable var2) {
      switch(var1) {
      case 0:
         return new JLabel(this.p.getUid());
      case 1:
         return new JLabel(this.p.ownerUID);
      case 2:
         return new JLabel(String.valueOf(this.p.price));
      case 3:
         return new JLabel(String.valueOf(this.p.rating));
      case 4:
         return new JLabel(String.valueOf(this.p.description));
      case 5:
         return new JLabel((new Date(this.p.date)).toString());
      case 6:
         return new JLabel(String.valueOf(this.p.timesSpawned));
      case 7:
         return this.getPermissionBox(16);
      case 8:
         return this.getPermissionBox(1);
      case 9:
         return this.getPermissionBox(4);
      case 10:
         return this.getPermissionBox(2);
      case 11:
         JButton var3;
         (var3 = new JButton("Edit")).setPreferredSize(new Dimension(40, 20));
         var3.setHorizontalTextPosition(2);
         var3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
               (new StarmoteCatalogEditDialog(StarmoteFrame.self, StarmoteCatalogEntry.this.p, StarmoteCatalogEntry.this.state, StarmoteCatalogEntry.this.model, var2)).setVisible(true);
            }
         });
         return var3;
      default:
         return new JLabel("-");
      }
   }

   private JCheckBox getPermissionBox(final int var1) {
      JCheckBox var2;
      (var2 = new JCheckBox()).setSelected((this.p.permission & var1) == var1);
      ActionListener var3 = new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            boolean var3 = ((AbstractButton)var1x.getSource()).getModel().isSelected();
            CatalogPermission var2;
            (var2 = new CatalogPermission(StarmoteCatalogEntry.this.p)).setPermission(var3, var1);
            System.err.println("SET TO " + var3);
            StarmoteCatalogEntry.this.state.getCatalogManager().clientRequestCatalogEdit(var2);
         }
      };
      var2.addActionListener(var3);
      return var2;
   }

   public boolean update() {
      return false;
   }
}
