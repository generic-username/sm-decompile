package org.schema.game.common.staremote.gui.catalog;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import org.schema.game.client.data.GameClientState;

public class StarmoteCatalogPanel extends JPanel implements Observer {
   private static final long serialVersionUID = 1L;
   private StarmoteCatalogTableModel model;
   private GameClientState state;
   private JTable table;

   public StarmoteCatalogPanel(GameClientState var1) {
      this.state = var1;
      var1.getCatalogManager().addObserver(this);
      var1.getPlayer().getCatalog().addObserver(this);
      GridBagLayout var3;
      (var3 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var3.rowHeights = new int[]{0, 0};
      var3.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var3.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      this.setLayout(var3);
      JScrollPane var4 = new JScrollPane();
      GridBagConstraints var2;
      (var2 = new GridBagConstraints()).fill = 1;
      var2.gridx = 0;
      var2.gridy = 0;
      this.add(var4, var2);
      this.model = new StarmoteCatalogTableModel();
      this.table = new JTable(this.model);
      this.table.setDefaultRenderer(StarmoteCatalogEntry.class, new StarmoteCatalogTableCellRenderer());
      this.table.setDefaultRenderer(String.class, new StarmoteCatalogTableCellRenderer());
      this.table.setDefaultEditor(StarmoteCatalogEntry.class, new StarmoteCatalogTableCellEditor());
      this.table.setDefaultEditor(String.class, new StarmoteCatalogTableCellEditor());
      var4.setViewportView(this.table);
      this.buildTable();
      this.model.setAutoWidth(this.table);
   }

   private void buildTable() {
      this.table.clearSelection();
      this.table.requestFocus();
      this.table.removeAll();
      this.model.rebuild(this.state);
      this.table.repaint();
      this.table.requestFocus();
      this.table.invalidate();
      this.table.validate();
      this.table.selectAll();
      this.table.clearSelection();
   }

   public void update(Observable var1, Object var2) {
      this.buildTable();
   }
}
