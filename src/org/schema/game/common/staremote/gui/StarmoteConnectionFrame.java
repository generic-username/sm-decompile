package org.schema.game.common.staremote.gui;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import org.schema.game.common.staremote.Staremote;
import org.schema.game.common.staremote.gui.connection.StarmoteServerPanel;

public class StarmoteConnectionFrame extends JFrame {
   private static final long serialVersionUID = 1L;
   private JPanel contentPane;

   public StarmoteConnectionFrame(Staremote var1) {
      this.setTitle("Starmote Connection");
      this.setDefaultCloseOperation(2);
      this.setBounds(100, 100, 468, 359);
      this.contentPane = new JPanel();
      this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.contentPane.setLayout(new BorderLayout(0, 0));
      this.setContentPane(this.contentPane);
      StarmoteServerPanel var2 = new StarmoteServerPanel(this, var1);
      this.contentPane.add(var2, "Center");
   }
}
