package org.schema.game.common.staremote.gui.faction;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import org.schema.game.common.data.player.faction.Faction;

public class StarmoteFactionListRenderer extends JLabel implements ListCellRenderer {
   private static final long serialVersionUID = 1L;

   public Component getListCellRendererComponent(JList var1, Object var2, int var3, boolean var4, boolean var5) {
      Faction var6 = (Faction)var2;

      assert var6 != null;

      this.setText(var6.getName());
      this.setOpaque(true);
      if (var4) {
         this.setForeground(UIManager.getColor("List.selectionForeground"));
         this.setBackground(UIManager.getColor("List.selectionBackground"));
      } else {
         this.setBackground(UIManager.getColor("List.background"));
      }

      return this;
   }
}
