package org.schema.game.common.staremote.gui.faction;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.staremote.gui.faction.edit.StarmoteFactionAddMemberDialog;
import org.schema.game.common.staremote.gui.faction.edit.StarmoteFactionRolesEditDialog;
import org.schema.game.server.data.admin.AdminCommands;

public class StarmoteFactionConfigPanel extends JPanel {
   private static final long serialVersionUID = 1L;
   private JTextField textField;

   public StarmoteFactionConfigPanel(final GameClientState var1, final Faction var2) {
      GridBagLayout var3;
      (var3 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var3.rowHeights = new int[]{0, 0, 0, 0, 0};
      var3.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var3.rowWeights = new double[]{0.0D, 1.0D, 0.0D, 1.0D, Double.MIN_VALUE};
      this.setLayout(var3);
      this.textField = new JTextField();
      this.textField.setText(var2.getName());
      GridBagConstraints var7;
      (var7 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var7.fill = 2;
      var7.gridx = 0;
      var7.gridy = 0;
      this.add(this.textField, var7);
      this.textField.setColumns(10);
      final JTextArea var8;
      (var8 = new JTextArea()).setText(var2.getDescription());
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var4.fill = 1;
      var4.gridx = 0;
      var4.gridy = 1;
      this.add(var8, var4);
      JPanel var9 = new JPanel();
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).fill = 2;
      var5.anchor = 11;
      var5.insets = new Insets(0, 0, 5, 0);
      var5.gridx = 0;
      var5.gridy = 2;
      this.add(var9, var5);
      GridBagLayout var11;
      (var11 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0};
      var11.rowHeights = new int[]{0, 0};
      var11.columnWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var11.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var9.setLayout(var11);
      JButton var13;
      (var13 = new JButton("Apply Name/Description")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            var1.getController().sendAdminCommand(AdminCommands.FACTION_EDIT, var2.getIdFaction(), StarmoteFactionConfigPanel.this.textField.getText(), var8.getText());
         }
      });
      (var7 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var7.gridx = 0;
      var7.gridy = 0;
      var9.add(var13, var7);
      JButton var10;
      (var10 = new JButton("Add Member")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            (new StarmoteFactionAddMemberDialog(var1, var2)).setVisible(true);
         }
      });
      (var5 = new GridBagConstraints()).anchor = 18;
      var5.insets = new Insets(0, 0, 0, 5);
      var5.gridx = 2;
      var5.gridy = 0;
      var9.add(var10, var5);
      (var10 = new JButton("Edit Roles")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            (new StarmoteFactionRolesEditDialog(var1, var2)).setVisible(true);
         }
      });
      (var5 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var5.gridx = 3;
      var5.gridy = 0;
      var9.add(var10, var5);
      (var10 = new JButton("Delete Faction")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            var1.getController().sendAdminCommand(AdminCommands.FACTION_DELETE, var2.getIdFaction());
         }
      });
      (var5 = new GridBagConstraints()).anchor = 18;
      var5.gridx = 5;
      var5.gridy = 0;
      var9.add(var10, var5);
      JScrollPane var12 = new JScrollPane();
      (var4 = new GridBagConstraints()).weighty = 1.0D;
      var4.fill = 1;
      var4.gridx = 0;
      var4.gridy = 3;
      this.add(var12, var4);
      StarmoteFactionRelationPanel var6 = new StarmoteFactionRelationPanel(var1, var2);
      var12.setViewportView(var6);
   }
}
