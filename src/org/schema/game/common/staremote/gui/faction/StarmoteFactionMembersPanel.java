package org.schema.game.common.staremote.gui.faction;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;

public class StarmoteFactionMembersPanel extends JPanel implements Observer {
   private static final long serialVersionUID = 1L;
   private StarmoteFactionTableModel model;
   private GameClientState state;
   private JTable table;

   public StarmoteFactionMembersPanel(GameClientState var1, Faction var2) {
      this.state = var1;
      var1.getFactionManager().addObserver(this);
      var1.getPlayer().getFactionController().addObserver(this);
      GridBagLayout var3;
      (var3 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var3.rowHeights = new int[]{0, 0};
      var3.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var3.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      this.setLayout(var3);
      JScrollPane var5 = new JScrollPane();
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).fill = 1;
      var4.gridx = 0;
      var4.gridy = 0;
      this.add(var5, var4);
      this.model = new StarmoteFactionTableModel(var1, var2);
      this.table = new JTable(this.model);
      this.table.setDefaultRenderer(StarmoteFactionMemberEntry.class, new StarmoteFactionTableCellRenderer());
      this.table.setDefaultRenderer(String.class, new StarmoteFactionTableCellRenderer());
      this.table.setDefaultEditor(StarmoteFactionMemberEntry.class, new StarmoteFactionTableCellEditor());
      this.table.setDefaultEditor(String.class, new StarmoteFactionTableCellEditor());
      var5.setViewportView(this.table);
      this.buildTable();
      this.model.setAutoWidth(this.table);
   }

   private void buildTable() {
      this.table.clearSelection();
      this.table.requestFocus();
      this.table.removeAll();
      this.model.rebuild(this.state);
      this.table.repaint();
      this.table.requestFocus();
      this.table.invalidate();
      this.table.validate();
      this.table.selectAll();
      this.table.clearSelection();
   }

   public void update(Observable var1, Object var2) {
      this.buildTable();
   }
}
