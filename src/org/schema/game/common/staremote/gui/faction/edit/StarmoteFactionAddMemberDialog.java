package org.schema.game.common.staremote.gui.faction.edit;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.staremote.gui.StarmoteFrame;
import org.schema.game.server.data.admin.AdminCommands;

public class StarmoteFactionAddMemberDialog extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private JTextField textField;

   public StarmoteFactionAddMemberDialog(final GameClientState var1, final Faction var2) {
      super(StarmoteFrame.self, true);
      this.setDefaultCloseOperation(2);
      this.setTitle("Add Member to Faction " + var2.getName());
      this.setBounds(100, 100, 449, 106);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var3;
      (var3 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var3.rowHeights = new int[]{0, 0};
      var3.columnWeights = new double[]{0.0D, 1.0D, Double.MIN_VALUE};
      var3.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      this.contentPanel.setLayout(var3);
      JLabel var5 = new JLabel("Enter Player Name");
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var4.anchor = 13;
      var4.gridx = 0;
      var4.gridy = 0;
      this.contentPanel.add(var5, var4);
      this.textField = new JTextField();
      GridBagConstraints var6;
      (var6 = new GridBagConstraints()).fill = 2;
      var6.gridx = 1;
      var6.gridy = 0;
      this.contentPanel.add(this.textField, var6);
      this.textField.setColumns(10);
      JPanel var7;
      (var7 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var7, "South");
      JButton var8;
      (var8 = new JButton("OK")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            var1.getController().sendAdminCommand(AdminCommands.FACTION_JOIN_ID, StarmoteFactionAddMemberDialog.this.textField.getText().trim(), var2.getIdFaction());
            StarmoteFactionAddMemberDialog.this.dispose();
         }
      });
      var8.setActionCommand("OK");
      var7.add(var8);
      this.getRootPane().setDefaultButton(var8);
      (var8 = new JButton("Cancel")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            StarmoteFactionAddMemberDialog.this.dispose();
         }
      });
      var8.setActionCommand("Cancel");
      var7.add(var8);
   }
}
