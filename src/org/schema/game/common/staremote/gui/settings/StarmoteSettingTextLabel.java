package org.schema.game.common.staremote.gui.settings;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.UIManager;

public abstract class StarmoteSettingTextLabel extends JLabel implements StarmoteSettingElement {
   private static final long serialVersionUID = 1L;
   private final String valueName;
   private Object lastValue;

   public StarmoteSettingTextLabel(String var1) {
      this.valueName = var1;
      this.lastValue = this.getValue();
   }

   public JComponent getComponent() {
      this.setText(this.getValue().toString());
      this.setOpaque(true);
      this.setBackground(UIManager.getColor("List.background"));
      return this;
   }

   public String getValueName() {
      return this.valueName;
   }

   public boolean isEditable() {
      return false;
   }

   public boolean update() {
      Object var1;
      if (!(var1 = this.getValue()).equals(this.lastValue)) {
         this.setText(var1.toString());
         this.lastValue = var1;
         return true;
      } else {
         return false;
      }
   }

   public abstract Object getValue();
}
