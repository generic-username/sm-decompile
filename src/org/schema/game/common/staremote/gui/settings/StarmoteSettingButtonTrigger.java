package org.schema.game.common.staremote.gui.settings;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComponent;

public abstract class StarmoteSettingButtonTrigger extends JButton implements StarmoteSettingElement {
   private static final long serialVersionUID = 1L;
   private final String valueName;
   private Object lastValue;

   public StarmoteSettingButtonTrigger(String var1) {
      this.valueName = var1;
      this.lastValue = this.getValue();
      this.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            StarmoteSettingButtonTrigger.this.trigger();
         }
      });
   }

   public JComponent getComponent() {
      this.setText(this.getValueName() + "   " + this.getValue());
      this.setOpaque(true);
      return this;
   }

   public String getValueName() {
      return this.valueName;
   }

   public boolean isEditable() {
      return true;
   }

   public boolean update() {
      Object var1;
      if (!(var1 = this.getValue()).equals(this.lastValue)) {
         this.setText(this.getValueName() + "   " + this.getValue());
         this.lastValue = var1;
         return true;
      } else {
         return false;
      }
   }

   public abstract Object getValue();

   public abstract void trigger();
}
