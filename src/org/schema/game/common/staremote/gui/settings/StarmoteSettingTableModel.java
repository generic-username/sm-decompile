package org.schema.game.common.staremote.gui.settings;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public class StarmoteSettingTableModel extends AbstractTableModel {
   private static final long serialVersionUID = 1L;
   private final ArrayList list = new ArrayList();

   public String getColumnName(int var1) {
      switch(var1) {
      case 0:
         return "Setting";
      case 1:
         return "Value";
      default:
         return "-";
      }
   }

   public Class getColumnClass(int var1) {
      return var1 == 0 ? String.class : StarmoteSettingElement.class;
   }

   public boolean isCellEditable(int var1, int var2) {
      return var2 > 0 && ((StarmoteSettingElement)this.list.get(var1)).isEditable();
   }

   public ArrayList getList() {
      return this.list;
   }

   public int getRowCount() {
      return this.list.size();
   }

   public int getColumnCount() {
      return 2;
   }

   public Object getValueAt(int var1, int var2) {
      return this.list.get(var1);
   }

   public void updateElements() {
      for(int var1 = 0; var1 < this.list.size(); ++var1) {
         if (((StarmoteSettingElement)this.list.get(var1)).update()) {
            this.fireTableCellUpdated(var1, 1);
         }
      }

   }
}
