package org.schema.game.common.staremote.gui.settings;

import javax.swing.JComponent;

public interface StarmoteSettingElement {
   JComponent getComponent();

   String getValueName();

   boolean isEditable();

   boolean update();
}
