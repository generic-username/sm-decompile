package org.schema.game.common.staremote.gui.sector.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.staremote.gui.StarmoteFrame;
import org.schema.game.common.staremote.gui.sector.StarmoteSectorSelectionPanel;
import org.schema.game.server.data.admin.AdminCommands;

public class StarmoteSectorDespawnDialog extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private final ButtonGroup buttonGroup = new ButtonGroup();
   private JTextField textField;
   private StarmoteSectorSelectionPanel starmodeSectorSelectionPanel;
   private JCheckBox chckbxShipsOnly;
   private JCheckBox chckbxAllUniverse;
   private JRadioButton rdbtnUsed;
   private JRadioButton rdbtnUnused;
   private JRadioButton rdbtnAll;

   public StarmoteSectorDespawnDialog(final GameClientState var1) {
      super(StarmoteFrame.self, true);
      this.setDefaultCloseOperation(2);
      this.setTitle("Change Sector");
      this.setBounds(100, 100, 541, 251);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var2;
      (var2 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var2.rowHeights = new int[]{0, 0, 0, 0, 0};
      var2.columnWeights = new double[]{0.0D, 1.0D, Double.MIN_VALUE};
      var2.rowWeights = new double[]{0.0D, 0.0D, 1.0D, 1.0D, Double.MIN_VALUE};
      this.contentPanel.setLayout(var2);
      JLabel var4;
      (var4 = new JLabel("WARNING: Changes made are PERMANENT! Use with care and backup beforehand!")).setForeground(new Color(139, 0, 0));
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).anchor = 17;
      var3.gridwidth = 2;
      var3.insets = new Insets(0, 0, 5, 5);
      var3.gridx = 0;
      var3.gridy = 0;
      this.contentPanel.add(var4, var3);
      var4 = new JLabel("Enter First Letters");
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var3.anchor = 13;
      var3.gridx = 0;
      var3.gridy = 1;
      this.contentPanel.add(var4, var3);
      this.textField = new JTextField();
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var5.fill = 2;
      var5.gridx = 1;
      var5.gridy = 1;
      this.contentPanel.add(this.textField, var5);
      this.textField.setColumns(10);
      var4 = new JLabel("Options");
      (var3 = new GridBagConstraints()).anchor = 17;
      var3.insets = new Insets(0, 0, 5, 5);
      var3.gridx = 0;
      var3.gridy = 2;
      this.contentPanel.add(var4, var3);
      JPanel var6 = new JPanel();
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.fill = 1;
      var3.gridx = 1;
      var3.gridy = 2;
      this.contentPanel.add(var6, var3);
      GridBagLayout var7;
      (var7 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0};
      var7.rowHeights = new int[]{0, 0, 0};
      var7.columnWeights = new double[]{0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var7.rowWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var6.setLayout(var7);
      this.chckbxShipsOnly = new JCheckBox("Ships only");
      this.chckbxShipsOnly.setSelected(true);
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var3.gridx = 0;
      var3.gridy = 0;
      var6.add(this.chckbxShipsOnly, var3);
      this.chckbxAllUniverse = new JCheckBox("All Universe");
      this.chckbxAllUniverse.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            StarmoteSectorDespawnDialog.this.starmodeSectorSelectionPanel.setEnabled(!StarmoteSectorDespawnDialog.this.chckbxAllUniverse.isSelected());
         }
      });
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var3.gridx = 1;
      var3.gridy = 0;
      var6.add(this.chckbxAllUniverse, var3);
      this.rdbtnUsed = new JRadioButton("Edited Only");
      this.buttonGroup.add(this.rdbtnUsed);
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var3.gridx = 0;
      var3.gridy = 1;
      var6.add(this.rdbtnUsed, var3);
      this.rdbtnUsed.setSelected(true);
      this.rdbtnUnused = new JRadioButton("Unedited Only");
      this.buttonGroup.add(this.rdbtnUnused);
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var3.gridx = 1;
      var3.gridy = 1;
      var6.add(this.rdbtnUnused, var3);
      this.rdbtnAll = new JRadioButton("Edited & Unedited");
      this.buttonGroup.add(this.rdbtnAll);
      (var3 = new GridBagConstraints()).gridx = 2;
      var3.gridy = 1;
      var6.add(this.rdbtnAll, var3);
      (var6 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Sector", 4, 2, (Font)null, new Color(0, 0, 0)));
      (var3 = new GridBagConstraints()).fill = 1;
      var3.gridwidth = 2;
      var3.gridx = 0;
      var3.gridy = 3;
      this.contentPanel.add(var6, var3);
      (var7 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var7.rowHeights = new int[]{0, 0};
      var7.columnWeights = new double[]{0.0D, 1.0D, Double.MIN_VALUE};
      var7.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var6.setLayout(var7);
      this.starmodeSectorSelectionPanel = new StarmoteSectorSelectionPanel();
      this.starmodeSectorSelectionPanel.setMinimumSize(new Dimension(400, 50));
      this.starmodeSectorSelectionPanel.setPreferredSize(new Dimension(400, 50));
      (var3 = new GridBagConstraints()).weightx = 1.0D;
      var3.anchor = 18;
      var3.fill = 2;
      var3.gridwidth = 2;
      var3.insets = new Insets(0, 0, 0, 5);
      var3.gridx = 0;
      var3.gridy = 0;
      var6.add(this.starmodeSectorSelectionPanel, var3);
      (var2 = new GridBagLayout()).columnWidths = new int[]{0};
      var2.rowHeights = new int[]{0};
      var2.columnWeights = new double[]{Double.MIN_VALUE};
      var2.rowWeights = new double[]{Double.MIN_VALUE};
      this.starmodeSectorSelectionPanel.setLayout(var2);
      (var6 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var6, "South");
      JButton var8;
      (var8 = new JButton("OK")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            boolean var5 = StarmoteSectorDespawnDialog.this.chckbxShipsOnly.isSelected();
            String var2 = StarmoteSectorDespawnDialog.this.rdbtnAll.isSelected() ? "all" : (StarmoteSectorDespawnDialog.this.rdbtnUsed.isSelected() ? "used" : "unused");
            String var3 = StarmoteSectorDespawnDialog.this.textField.getText().trim();
            Vector3i var4 = StarmoteSectorDespawnDialog.this.starmodeSectorSelectionPanel.getCoord();
            if (StarmoteSectorDespawnDialog.this.chckbxAllUniverse.isSelected()) {
               var1.getController().sendAdminCommand(AdminCommands.DESPAWN_ALL, var3, var2, var5);
            } else {
               var1.getController().sendAdminCommand(AdminCommands.DESPAWN_SECTOR, var3, var2, var5, var4.x, var4.y, var4.z);
            }

            StarmoteSectorDespawnDialog.this.dispose();
         }
      });
      var8.setActionCommand("OK");
      var6.add(var8);
      this.getRootPane().setDefaultButton(var8);
      (var8 = new JButton("Cancel")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            StarmoteSectorDespawnDialog.this.dispose();
         }
      });
      var8.setActionCommand("Cancel");
      var6.add(var8);
   }
}
