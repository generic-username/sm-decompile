package org.schema.game.common.staremote;

import java.awt.Component;
import java.awt.EventQueue;
import java.io.IOException;
import java.net.ConnectException;
import java.util.Observer;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.Starter;
import org.schema.game.common.staremote.gui.StarmoteConnectionFrame;
import org.schema.game.common.staremote.gui.StarmoteFrame;
import org.schema.game.common.staremote.gui.connection.StarmoteConnection;
import org.schema.game.common.staremote.gui.entity.StarmoteEntitySettingsPanel;
import org.schema.game.common.staremote.gui.player.StarmoteOfflinePlayerList;
import org.schema.game.common.version.Version;
import org.schema.game.network.StarMadePlayerStats;
import org.schema.game.network.StarMadeServerStats;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.network.LoginFailedException;
import org.schema.schine.network.client.HostPortLoginName;
import org.schema.schine.network.commands.Login;

public class Staremote {
   public static StarmoteEntitySettingsPanel currentlyVisiblePanel;
   public boolean exit = false;
   private GameClientState state;
   private StarmoteFrame guiFrame;
   private StarmoteOfflinePlayerList playersRequest;

   public static String getConnectionFilePath() {
      return "./.starmotecon";
   }

   public static void main(String[] var0) {
      Version.loadVersion();
      EngineSettings.read();

      try {
         Starter.initialize(true);
      } catch (SecurityException var1) {
         var0 = null;
         var1.printStackTrace();
      } catch (IOException var2) {
         var0 = null;
         var2.printStackTrace();
      }

      Staremote var3;
      (var3 = new Staremote()).exit = true;
      var3.startConnectionGUI();
   }

   public void connect(StarmoteConnection var1) {
      final HostPortLoginName var2 = new HostPortLoginName(var1.url, var1.port, (byte)1, var1.username);
      (new Thread(new Runnable() {
         public void run() {
            try {
               String var2x;
               try {
                  Staremote.this.state = new GameClientState(true);
                  GameClientController var16;
                  (var16 = new GameClientController(Staremote.this.state, (GraphicsContext)null, (Observer)null)).connect(var2);
                  var16.initialize();
                  Staremote.this.startGUI(Staremote.this.state);
                  Timer var17;
                  (var17 = new Timer()).initialize(false);

                  while(true) {
                     var16.update(var17);
                     var17.updateFPS(false);
                     Thread.sleep(1000L);
                     Staremote.this.update(Staremote.this.state);
                  }
               } catch (LoginFailedException var12) {
                  var12.printStackTrace();
                  Login.LoginCode var1;
                  var2x = (var1 = Login.LoginCode.getById(var12.getErrorCode())).errorMessage();
                  if (var1 == Login.LoginCode.ERROR_WRONG_CLIENT_VERSION) {
                     var2x = "Server: your client version is not allowed to connect.\nPlease download the correct version from www.star-made.org\nClient Version: " + Version.VERSION + "\nServer Version: " + GameClientState.serverVersion;
                     if (Version.compareVersion(GameClientState.serverVersion) < 0) {
                        var2x = var2x + "\n\nThe server has not yet updated to your version.\nPlease wait for the server to update and then try again.";
                     } else {
                        var2x = var2x + "\n\nYour version is older then the server-version. Please update.";
                     }
                  }

                  JFrame var15;
                  (var15 = new JFrame("LoginError")).setAlwaysOnTop(true);
                  switch(JOptionPane.showOptionDialog(var15, var2x, "Login Failed", 1, 0, (Icon)null, new String[]{"Back To Login Screen", "EXIT"}, "Back To Login Screen")) {
                  case 0:
                     return;
                  case 1:
                     System.err.println("Exiting because of login failed");

                     try {
                        throw new Exception("System.exit() called");
                     } catch (Exception var10) {
                        var10.printStackTrace();
                        System.exit(0);
                     }
                  default:
                  }
               } catch (Exception var13) {
                  var2x = var13.getClass().getSimpleName() + ": " + var13.getMessage();
                  if (var13 instanceof ConnectException) {
                     var2x = var2x + ", \n\nfailed to connect to \"" + var2.host + ":" + var2.port + "\".\nEither server is down, blocking, or the adress is wrong!";
                  }

                  var13.printStackTrace();
                  switch(JOptionPane.showOptionDialog((Component)null, var2x, "ERROR", 1, 0, (Icon)null, new String[]{"Ok", "Exit"}, "Exit")) {
                  case 0:
                     return;
                  case 1:
                     System.err.println("Exiting because of failed conenct");

                     try {
                        throw new Exception("System.exit() called");
                     } catch (Exception var9) {
                        var9.printStackTrace();
                        System.exit(0);
                        return;
                     }
                  case 2:
                     System.err.println("Exiting because of failed conenct");

                     try {
                        throw new Exception("System.exit() called");
                     } catch (Exception var11) {
                        var11.printStackTrace();
                        System.exit(0);
                     }
                  default:
                  }
               }
            } catch (Throwable var14) {
               throw var14;
            }
         }
      })).start();
   }

   public void disconnect() {
      try {
         this.state.disconnect();
      } catch (Exception var1) {
         var1.printStackTrace();
      }
   }

   public void exit() {
      this.disconnect();
      if (this.exit) {
         try {
            throw new Exception("System.exit() called");
         } catch (Exception var1) {
            var1.printStackTrace();
            System.exit(0);
         }
      }

   }

   public void requestAllPlayers(StarmoteOfflinePlayerList var1) {
      this.playersRequest = var1;
   }

   public void startConnectionGUI() {
      EventQueue.invokeLater(new Runnable() {
         public void run() {
            StarmoteConnectionFrame var1;
            (var1 = new StarmoteConnectionFrame(Staremote.this)).setVisible(true);
            var1.setDefaultCloseOperation(2);
            var1.requestFocus();
         }
      });
   }

   private void startGUI(final GameClientState var1) {
      EventQueue.invokeLater(new Runnable() {
         public void run() {
            assert var1 != null;

            Staremote.this.guiFrame = new StarmoteFrame(var1, Staremote.this);
            Staremote.this.guiFrame.setVisible(true);
            Staremote.this.guiFrame.setDefaultCloseOperation(2);
         }
      });
   }

   public void update(GameClientState var1) {
      if (currentlyVisiblePanel != null) {
         currentlyVisiblePanel.updateSettings();
      }

      try {
         StarMadeServerStats var2 = var1.getController().requestServerStats();
         this.guiFrame.updateStats(var2);
         if (this.playersRequest != null) {
            StarMadePlayerStats var5 = var1.getController().requestPlayerStats(0);
            this.playersRequest.update(var5);
            this.playersRequest = null;
         }

      } catch (IOException var3) {
         var3.printStackTrace();
      } catch (InterruptedException var4) {
         var4.printStackTrace();
      }
   }
}
