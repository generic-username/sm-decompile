package org.schema.game.network;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.tag.Tag;

public class StarMadePlayerStats {
   public static final int ONLINE_ONLY = 1;
   public static final int INFO_PLAYER_DETAILS = 4;
   public static final int INFO_LAST_LOGIN = 8;
   public static final int INFO_LOGGED_IP = 16;
   private static int paramSize = 4;
   public ReceivedPlayer[] receivedPlayers;

   public static StarMadePlayerStats decode(Object[] var0, int var1) {
      int var2 = var0.length / paramSize;
      StarMadePlayerStats var3;
      (var3 = new StarMadePlayerStats()).receivedPlayers = new ReceivedPlayer[var2];

      for(int var4 = 0; var4 < var2; ++var4) {
         var3.receivedPlayers[var4] = new ReceivedPlayer();
         var3.receivedPlayers[var4].decode(var0, var4 * paramSize, var1);
      }

      return var3;
   }

   public static Object[] encode(GameServerState var0, int var1) {
      File[] var11 = (new FileExt(GameServerState.ENTITY_DATABASE_PATH)).listFiles(new FilenameFilter() {
         public final boolean accept(File var1, String var2) {
            return var2.startsWith("ENTITY_PLAYERSTATE");
         }
      });
      long var4 = System.currentTimeMillis();
      System.err.println("[StarMadePlayerStates] READING " + var11.length + " Player States");
      ArrayList var2 = new ArrayList();

      for(int var3 = 0; var3 < var11.length; ++var3) {
         Tag var6;
         try {
            var6 = Tag.readFrom(new BufferedInputStream(new FileInputStream(var11[var3])), true, false);
            PlayerState var7;
            (var7 = new PlayerState(var0)).initialize();
            var7.fromTagStructure(var6);
            String var15 = var11[var3].getName();
            var7.setName(var15.substring(19, var15.lastIndexOf(".")));
            var2.add(var7);
         } catch (FileNotFoundException var8) {
            var6 = null;
            var8.printStackTrace();
         } catch (IOException var9) {
            var6 = null;
            var9.printStackTrace();
         }
      }

      System.err.println("[StarMadePlayerStates] READING " + var11.length + " Player States took: " + (System.currentTimeMillis() - var4) + " ms");
      Object[] var13 = new Object[var2.size() * paramSize];

      int var16;
      for(var16 = 0; var16 < var2.size(); ++var16) {
         PlayerState var17 = (PlayerState)var2.get(var16);
         int var10 = var16 * paramSize;
         StringBuilder var12 = new StringBuilder();
         ArrayList var14;
         (var14 = new ArrayList()).addAll(var17.getHosts());

         for(int var5 = 0; var5 < var14.size(); ++var5) {
            var12.append(var14.get(var5));
            if (var5 < var17.getHosts().size() - 1) {
               var12.append(",");
            }
         }

         var13[var10] = var17.getName();
         var13[var10 + 1] = var17.getLastLogin();
         var13[var10 + 2] = var17.getLastLogout();
         var13[var10 + 3] = var12.toString();
      }

      for(var16 = 0; var16 < var13.length; ++var16) {
         assert var13[var16] != null : Arrays.toString(var13);
      }

      return var13;
   }
}
