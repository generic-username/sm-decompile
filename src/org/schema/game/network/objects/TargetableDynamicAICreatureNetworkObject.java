package org.schema.game.network.objects;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.creature.CreaturePartNode;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.network.objects.remote.RemoteCreaturePart;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.remote.RemoteFloatPrimitive;
import org.schema.schine.network.objects.remote.RemoteVector3i;
import org.schema.schine.resource.CreatureStructure;

public class TargetableDynamicAICreatureNetworkObject extends TargetableAICreatureNetworkObject {
   public RemoteFloatPrimitive scale = new RemoteFloatPrimitive(1.0F, this);
   public RemoteFloatPrimitive height = new RemoteFloatPrimitive(1.0F, this);
   public RemoteFloatPrimitive width = new RemoteFloatPrimitive(1.0F, this);
   public RemoteFloatPrimitive speed = new RemoteFloatPrimitive(4.0F, this);
   public RemoteVector3i boxDim = new RemoteVector3i(new Vector3i(1, 1, 1), this);
   public RemoteCreaturePart creatureCode;

   public TargetableDynamicAICreatureNetworkObject(StateInterface var1, AbstractOwnerState var2) {
      super(var1, var2);
      this.creatureCode = new RemoteCreaturePart(new CreaturePartNode(CreatureStructure.PartType.BOTTOM), this);
   }
}
