package org.schema.game.network.objects;

public class StringLongLongPair {
   public String playerName;
   public long timeStamp;
   public long size;

   public StringLongLongPair() {
   }

   public StringLongLongPair(String var1, long var2, long var4) {
      this.playerName = var1;
      this.timeStamp = var2;
      this.size = var4;
   }

   public StringLongLongPair(StringLongLongPair var1) {
      this.playerName = var1.playerName;
      this.timeStamp = var1.timeStamp;
      this.size = var1.size;
   }
}
