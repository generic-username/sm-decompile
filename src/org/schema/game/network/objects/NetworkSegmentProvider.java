package org.schema.game.network.objects;

import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentProvider;
import org.schema.game.network.objects.remote.RemoteBitsetBuffer;
import org.schema.game.network.objects.remote.RemoteBlockBulkBuffer;
import org.schema.game.network.objects.remote.RemoteCompressedCoordBuffer;
import org.schema.game.network.objects.remote.RemoteControlStructureBuffer;
import org.schema.game.network.objects.remote.RemoteInventoryBuffer;
import org.schema.game.network.objects.remote.RemoteInventoryFilterBuffer;
import org.schema.game.network.objects.remote.RemoteManualMouseEventBuffer;
import org.schema.game.network.objects.remote.RemoteSegmentPieceBuffer;
import org.schema.game.network.objects.remote.RemoteSegmentRemoteObjBuffer;
import org.schema.game.network.objects.remote.RemoteServerMessageBuffer;
import org.schema.game.network.objects.remote.RemoteTextBlockBuffer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.remote.RemoteBoolean;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.RemoteLongBuffer;
import org.schema.schine.network.objects.remote.RemoteShortBuffer;
import org.schema.schine.network.objects.remote.RemoteVector3f;

public class NetworkSegmentProvider extends NetworkEntityProvider {
   public static final int BATCH_SIZE = 512;
   public RemoteLongBuffer segmentBufferRequestBuffer = new RemoteLongBuffer(this, 512);
   public RemoteBitsetBuffer segmentBufferAwnserBuffer = new RemoteBitsetBuffer(this);
   public RemoteLongBuffer segmentClientToServerCombinedRequestBuffer = new RemoteLongBuffer(this, 512);
   public RemoteLongBuffer signatureEmptyBuffer = new RemoteLongBuffer(this, 512);
   public RemoteLongBuffer signatureOkBuffer = new RemoteLongBuffer(this, 512);
   public RemoteSegmentRemoteObjBuffer segmentBuffer;
   public RemoteBoolean requestedInitialControlMap = new RemoteBoolean(this);
   public RemoteLongBuffer blockActivationBuffer = new RemoteLongBuffer(this);
   public RemoteSegmentPieceBuffer modificationBuffer = new RemoteSegmentPieceBuffer(this, ((GameStateInterface)this.getState()).getSegmentPieceQueueSize());
   public RemoteBlockBulkBuffer modificationBulkBuffer = new RemoteBlockBulkBuffer(this);
   public RemoteControlStructureBuffer initialControlMap;
   public RemoteInventoryBuffer invetoryBuffer;
   public RemoteTextBlockBuffer textBlockResponsesAndChangeRequests = new RemoteTextBlockBuffer(this);
   public RemoteLongBuffer textBlockRequests = new RemoteLongBuffer(this);
   public RemoteLongBuffer textBlockChangeInLongRange = new RemoteLongBuffer(this);
   public RemoteBoolean signalDelete = new RemoteBoolean(false, this);
   public RemoteShortBuffer killBuffer = new RemoteShortBuffer(this, ((GameStateInterface)this.getState()).getSegmentPieceQueueSize() << 2);
   public RemoteLongBuffer beamLatchBuffer = new RemoteLongBuffer(this, 60);
   public RemoteShortBuffer activeChangedTrueBuffer = new RemoteShortBuffer(this, ((GameStateInterface)this.getState()).getSegmentPieceQueueSize() * 3);
   public RemoteShortBuffer activeChangedFalseBuffer = new RemoteShortBuffer(this, ((GameStateInterface)this.getState()).getSegmentPieceQueueSize() * 3);
   public RemoteCompressedCoordBuffer salvageBuffer = new RemoteCompressedCoordBuffer(this, ((GameStateInterface)this.getState()).getSegmentPieceQueueSize() * 3);
   public RemoteServerMessageBuffer messagesToBlocks = new RemoteServerMessageBuffer(this);
   public RemoteBuffer explosions = new RemoteBuffer(RemoteVector3f.class, this);
   public RemoteLongBuffer inventoryDetailRequests = new RemoteLongBuffer(this);
   public RemoteInventoryFilterBuffer inventoryDetailAnswers = new RemoteInventoryFilterBuffer(this);
   public RemoteManualMouseEventBuffer manualMouseEventBuffer = new RemoteManualMouseEventBuffer(this);

   public NetworkSegmentProvider(StateInterface var1, SendableSegmentProvider var2) {
      super(var1);
      this.initialControlMap = new RemoteControlStructureBuffer(var2, this);
      this.segmentBuffer = new RemoteSegmentRemoteObjBuffer(this, (SegmentController)var2.getSegmentController());
      this.explosions.MAX_BATCH = 16;
   }

   public void onDelete(StateInterface var1) {
   }

   public void onInit(StateInterface var1) {
   }

   public void setBatchSize(int var1) {
      this.segmentClientToServerCombinedRequestBuffer.MAX_BATCH = var1;
      this.segmentBufferAwnserBuffer.MAX_BATCH = var1;
      this.signatureEmptyBuffer.MAX_BATCH = var1;
      this.signatureOkBuffer.MAX_BATCH = var1;
   }
}
