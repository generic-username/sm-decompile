package org.schema.game.network.objects.valueUpdate;

import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;

public class BatteryActiveValueClientToServerUpdate extends BooleanValueUpdate {
   public BatteryActiveValueClientToServerUpdate() {
      this.deligateToClient = true;
   }

   public boolean applyClient(ManagerContainer var1) {
      ((PowerManagerInterface)var1).getPowerAddOn().setBatteryActive(this.val);
      return true;
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.val = ((PowerManagerInterface)var1).getPowerAddOn().isBatteryActive();
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.POWER_BATTERY_ACTIVE;
   }
}
