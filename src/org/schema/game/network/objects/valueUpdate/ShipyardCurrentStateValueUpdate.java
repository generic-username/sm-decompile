package org.schema.game.network.objects.valueUpdate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ShipyardManagerContainerInterface;
import org.schema.game.common.controller.elements.shipyard.ShipyardCollectionManager;
import org.schema.game.common.controller.elements.shipyard.ShipyardElementManager;

public class ShipyardCurrentStateValueUpdate extends ParameterValueUpdate {
   private double completion;
   private byte state;
   private int designLoaded;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      super.serialize(var1, var2);
      if (this.completion < 0.0D) {
         var1.writeByte((byte)((int)this.completion));
      } else {
         var1.writeByte((byte)((int)Math.min(100.0D, Math.max(0.0D, this.completion * 100.0D))));
      }

      var1.writeByte(this.state);
      var1.writeInt(this.designLoaded);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.completion = (double)((float)var1.readByte() / 100.0F);
      this.state = var1.readByte();
      this.designLoaded = var1.readInt();
   }

   public boolean applyClient(ManagerContainer var1) {
      ShipyardCollectionManager var2;
      if ((var2 = (ShipyardCollectionManager)((ShipyardElementManager)((ShipyardManagerContainerInterface)var1).getShipyard().getElementManager()).getCollectionManagersMap().get(this.parameter)) != null) {
         var2.setCompletionOrderPercent(this.completion);
         var2.currentClientState = this.state;
         var2.setCurrentDesign(this.designLoaded);
         return true;
      } else {
         return false;
      }
   }

   public void setServer(ManagerContainer var1, long var2) {
      ShipyardCollectionManager var4;
      if ((var4 = (ShipyardCollectionManager)((ShipyardElementManager)((ShipyardManagerContainerInterface)var1).getShipyard().getElementManager()).getCollectionManagersMap().get(var2)) != null) {
         this.completion = var4.getCompletionOrderPercent();
         this.state = var4.getStateByteOnServer();
         this.designLoaded = var4.getCurrentDesign();
      } else {
         this.state = -1;

         assert false : ((ShipyardElementManager)((ShipyardManagerContainerInterface)var1).getShipyard().getElementManager()).getCollectionManagersMap() + "; " + var2;
      }

      this.parameter = var2;
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.SHIPYARD_STATE_UPDATE;
   }
}
