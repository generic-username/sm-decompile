package org.schema.game.network.objects.valueUpdate;

import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ShieldContainerInterface;

public class ShieldExpectedValueUpdate extends IntValueUpdate {
   public boolean applyClient(ManagerContainer var1) {
      ((ShieldContainerInterface)var1).getShieldAddOn().setExpectedShieldClient(this.val);
      return true;
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.val = ((ShieldContainerInterface)var1).getShieldAddOn().getExpectedShieldSize();
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.SHIELD_EXPECTED;
   }
}
