package org.schema.game.network.objects.valueUpdate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ShieldContainerInterface;

public class ShieldLocalSingleValueUpdate extends DoubleValueUpdate {
   public long shieldId;

   public boolean applyClient(ManagerContainer var1) {
      ((ShieldContainerInterface)var1).getShieldAddOn().getShieldLocalAddOn().receivedShieldSingle(this.shieldId, this.val);
      return true;
   }

   public void setServer(ManagerContainer var1, long var2) {
      throw new RuntimeException("illegal call");
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      super.serialize(var1, var2);
      var1.writeLong(this.shieldId);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.shieldId = var1.readLong();
   }

   public void setServer(ManagerContainer var1, long var2, double var4) {
      this.val = var4;
      this.shieldId = var2;
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.SHIELD_LOCAL;
   }
}
