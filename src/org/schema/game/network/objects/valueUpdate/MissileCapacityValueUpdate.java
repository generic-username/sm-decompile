package org.schema.game.network.objects.valueUpdate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.controller.elements.ManagerContainer;

public class MissileCapacityValueUpdate extends FloatValueUpdate {
   float timer;

   public boolean applyClient(ManagerContainer var1) {
      var1.setMissileCapacity(this.val, this.timer, false);
      return true;
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.val = var1.getMissileCapacity();
      this.timer = var1.getMissileCapacityTimer();
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.MISSILE_CAPACITY_UPDATE;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      super.serialize(var1, var2);
      var1.writeFloat(this.timer);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.timer = var1.readFloat();
   }
}
