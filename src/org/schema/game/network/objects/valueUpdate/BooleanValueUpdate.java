package org.schema.game.network.objects.valueUpdate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public abstract class BooleanValueUpdate extends ValueUpdate {
   protected boolean val;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeBoolean(this.val);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      boolean var4 = var1.readBoolean();
      this.val = var4;
   }
}
