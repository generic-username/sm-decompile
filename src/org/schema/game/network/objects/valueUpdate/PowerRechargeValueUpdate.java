package org.schema.game.network.objects.valueUpdate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;

public class PowerRechargeValueUpdate extends ValueUpdate {
   boolean val;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeBoolean(this.val);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.val = var1.readBoolean();
   }

   public boolean applyClient(ManagerContainer var1) {
      ((PowerManagerInterface)var1).getPowerAddOn().setRechargeEnabled(this.val);
      return true;
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.val = ((PowerManagerInterface)var1).getPowerAddOn().isRechargeEnabled();
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.POWER_REGEN_ENABLED;
   }
}
