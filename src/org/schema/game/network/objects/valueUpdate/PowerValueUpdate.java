package org.schema.game.network.objects.valueUpdate;

import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;

public class PowerValueUpdate extends DoubleValueUpdate {
   public boolean applyClient(ManagerContainer var1) {
      ((PowerManagerInterface)var1).getPowerAddOn().setPower(this.val);
      return true;
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.val = ((PowerManagerInterface)var1).getPowerAddOn().getPowerSimple();
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.POWER;
   }
}
