package org.schema.game.network.objects.valueUpdate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public abstract class ByteValueUpdate extends ValueUpdate {
   public byte val;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeByte(this.val);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.val = var1.readByte();
   }
}
