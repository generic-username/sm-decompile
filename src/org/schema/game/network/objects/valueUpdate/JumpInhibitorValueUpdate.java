package org.schema.game.network.objects.valueUpdate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.controller.elements.JumpProhibiterModuleInterface;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.jumpprohibiter.JumpInhibitorCollectionManager;

public class JumpInhibitorValueUpdate extends ParameterValueUpdate {
   boolean active;

   public boolean applyClient(ManagerContainer var1) {
      JumpInhibitorCollectionManager var2;
      if ((var2 = (JumpInhibitorCollectionManager)((JumpProhibiterModuleInterface)var1).getJumpProhibiter().getCollectionManagersMap().get(this.parameter)) != null) {
         var2.activeInhibitor = this.active;
         return true;
      } else {
         return false;
      }
   }

   public void setServer(ManagerContainer var1, long var2) {
      JumpInhibitorCollectionManager var4;
      if ((var4 = (JumpInhibitorCollectionManager)((JumpProhibiterModuleInterface)var1).getJumpProhibiter().getCollectionManagersMap().get(var2)) != null) {
         this.active = var4.activeInhibitor;
      }

      this.parameter = var2;
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.JUMP_INHIBITOR;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      super.serialize(var1, var2);
      var1.writeBoolean(this.active);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.active = var1.readBoolean();
   }

   public String toString() {
      return "JumpInhibitorValueUpdate [parameter=" + this.parameter + ", active=" + this.active + "]";
   }
}
