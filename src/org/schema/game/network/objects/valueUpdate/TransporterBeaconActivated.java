package org.schema.game.network.objects.valueUpdate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.TransporterModuleInterface;
import org.schema.game.common.controller.elements.transporter.TransporterCollectionManager;

public class TransporterBeaconActivated extends ParameterValueUpdate {
   public int entityId = -1;

   public boolean applyClient(ManagerContainer var1) {
      TransporterCollectionManager var2;
      if ((var2 = (TransporterCollectionManager)((TransporterModuleInterface)var1).getTransporter().getCollectionManagersMap().get(this.parameter)) != null) {
         assert this.entityId != -1;

         var2.transporterBeaconActivatedReceived(this.entityId);
         if (var2.getSegmentController().isOnServer()) {
            var2.sendTransporterUsage();
         }

         return true;
      } else {
         return false;
      }
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      super.serialize(var1, var2);

      assert this.entityId != -1;

      var1.writeInt(this.entityId);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.entityId = var1.readInt();
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.parameter = var2;
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.TRANSPORTER_BEACON_ACTIVATED;
   }
}
