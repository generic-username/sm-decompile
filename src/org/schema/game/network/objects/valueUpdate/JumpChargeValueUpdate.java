package org.schema.game.network.objects.valueUpdate;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.controller.elements.jumpdrive.JumpDriveCollectionManager;
import org.schema.game.server.data.GameServerState;

public class JumpChargeValueUpdate extends FloatModuleValueUpdate {
   public boolean applyClient(ManagerContainer var1) {
      JumpDriveCollectionManager var2;
      if ((var2 = (JumpDriveCollectionManager)((ShipManagerContainer)var1).getJumpDrive().getCollectionManagersMap().get(this.parameter)) != null) {
         if (var1.isOnServer() && this.val > var2.getCharge()) {
            float var3 = var2.getCharge() / var2.getChargeNeededForJump();
            if (this.val / var2.getChargeNeededForJump() - var3 > 0.5F) {
               ((GameServerState)var1.getState()).getController().broadcastMessageAdmin(new Object[]{452, var1.getSegmentController(), var1.getSegmentController().getSector(new Vector3i())}, 3);
               return true;
            }
         }

         var2.setCharge(this.val);
         return true;
      } else {
         return false;
      }
   }

   public void setServer(ManagerContainer var1, long var2) {
      JumpDriveCollectionManager var4;
      if ((var4 = (JumpDriveCollectionManager)((ShipManagerContainer)var1).getJumpDrive().getCollectionManagersMap().get(var2)) != null) {
         this.val = var4.getCharge();
      }

      this.parameter = var2;
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.JUMP_CHARGE;
   }

   public String toString() {
      return "JumpChargeValueUpdate [parameter=" + this.parameter + ", val=" + this.val + "]";
   }
}
