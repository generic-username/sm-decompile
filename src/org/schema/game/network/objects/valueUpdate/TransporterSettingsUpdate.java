package org.schema.game.network.objects.valueUpdate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.TransporterModuleInterface;
import org.schema.game.common.controller.elements.transporter.TransporterCollectionManager;

public class TransporterSettingsUpdate extends ParameterValueUpdate {
   public String name;
   public byte publicAccess;

   public boolean applyClient(ManagerContainer var1) {
      TransporterCollectionManager var2;
      if ((var2 = (TransporterCollectionManager)((TransporterModuleInterface)var1).getTransporter().getCollectionManagersMap().get(this.parameter)) != null) {
         System.err.println(var2.getState() + " Received Transporter Settings: " + this.name + " pa " + this.publicAccess);
         var2.setTransporterSettings(this.name, this.publicAccess);
         if (var2.getSegmentController().isOnServer()) {
            var2.sendSettingsUpdate();
         }

         return true;
      } else {
         return false;
      }
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      super.serialize(var1, var2);
      var1.writeUTF(this.name);
      var1.writeByte(this.publicAccess);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.name = var1.readUTF();
      this.publicAccess = var1.readByte();
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.parameter = var2;

      assert this.name != null;

   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.TRANSPORTER_SETTINGS_UPDATE;
   }
}
