package org.schema.game.network.objects.valueUpdate;

import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.TransporterModuleInterface;
import org.schema.game.common.controller.elements.transporter.TransporterCollectionManager;

public class TransporterClientStateRequestUpdate extends ParameterValueUpdate {
   public boolean applyClient(ManagerContainer var1) {
      TransporterCollectionManager var2;
      if ((var2 = (TransporterCollectionManager)((TransporterModuleInterface)var1).getTransporter().getCollectionManagersMap().get(this.parameter)) != null) {
         var2.sendStateUpdateToClients();
         return true;
      } else {
         return false;
      }
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.parameter = var2;
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.TRANSPORTER_CLIENT_STATE_REQUEST;
   }
}
