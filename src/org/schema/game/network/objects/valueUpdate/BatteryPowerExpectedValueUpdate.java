package org.schema.game.network.objects.valueUpdate;

import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;

public class BatteryPowerExpectedValueUpdate extends IntValueUpdate {
   public boolean applyClient(ManagerContainer var1) {
      ((PowerManagerInterface)var1).getPowerAddOn().setExpectedBatteryClient(this.val);
      return true;
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.val = ((PowerManagerInterface)var1).getPowerAddOn().getExpectedBatterySize();
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.POWER_BATTERY_EXPECTED;
   }
}
