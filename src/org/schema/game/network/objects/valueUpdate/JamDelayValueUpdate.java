package org.schema.game.network.objects.valueUpdate;

import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.controller.elements.jamming.JammingElementManager;

public class JamDelayValueUpdate extends IntValueUpdate {
   public boolean applyClient(ManagerContainer var1) {
      ((JammingElementManager)((ShipManagerContainer)var1).getJamming().getElementManager()).setDelay(this.val);
      return true;
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.val = ((JammingElementManager)((ShipManagerContainer)var1).getJamming().getElementManager()).getDelay();
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.JAM_DELAY;
   }
}
