package org.schema.game.network.objects;

import it.unimi.dsi.fastutil.shorts.Short2IntOpenHashMap;
import javax.vecmath.Vector4f;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.blockeffects.config.EffectConfigNetworkObjectInterface;
import org.schema.game.network.objects.remote.RemoteBlockCount;
import org.schema.game.network.objects.remote.RemoteBlockEffectUpdateBuffer;
import org.schema.game.network.objects.remote.RemoteControlModBuffer;
import org.schema.game.network.objects.remote.RemoteInterconnectStructureBuffer;
import org.schema.game.network.objects.remote.RemoteRailMoveRequestBuffer;
import org.schema.game.network.objects.remote.RemoteRailRequestBuffer;
import org.schema.game.network.objects.remote.RemoteRuleStateChangeBuffer;
import org.schema.game.network.objects.remote.RemoteShipKeyConfigBuffer;
import org.schema.game.network.objects.remote.RemoteTextBlockBuffer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkEntity;
import org.schema.schine.network.objects.remote.RemoteBoolean;
import org.schema.schine.network.objects.remote.RemoteBooleanPrimitive;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.RemoteByteBuffer;
import org.schema.schine.network.objects.remote.RemoteBytePrimitive;
import org.schema.schine.network.objects.remote.RemoteFloatBuffer;
import org.schema.schine.network.objects.remote.RemoteIntBuffer;
import org.schema.schine.network.objects.remote.RemoteIntPrimitive;
import org.schema.schine.network.objects.remote.RemoteLongBuffer;
import org.schema.schine.network.objects.remote.RemoteLongPrimitive;
import org.schema.schine.network.objects.remote.RemoteShortBuffer;
import org.schema.schine.network.objects.remote.RemoteString;
import org.schema.schine.network.objects.remote.RemoteVector3f;
import org.schema.schine.network.objects.remote.RemoteVector3s;
import org.schema.schine.network.objects.remote.RemoteVector4f;
import org.schema.schine.network.objects.remote.RemoteVector4i;

public class NetworkSegmentController extends NetworkEntity implements EffectConfigNetworkObjectInterface, NTRuleInterface {
   public RemoteVector3s minSize = new RemoteVector3s(this);
   public RemoteVector3s maxSize = new RemoteVector3s(this);
   public RemoteRailMoveRequestBuffer railMoveToPos = new RemoteRailMoveRequestBuffer(this);
   public RemoteIntPrimitive lastModifiedClientId = new RemoteIntPrimitive(0, this);
   public RemoteString uniqueIdentifier = new RemoteString(this);
   public RemoteString realName = new RemoteString(this);
   public RemoteBlockCount relevantBlockCounts = new RemoteBlockCount(new Short2IntOpenHashMap(), this);
   public RemoteLongPrimitive coreDestructionStarted = new RemoteLongPrimitive(-1L, this);
   public RemoteLongPrimitive coreDestructionDuration = new RemoteLongPrimitive(-1L, this);
   public RemoteBooleanPrimitive useHpLong = new RemoteBooleanPrimitive(false, this);
   public RemoteIntPrimitive hpInt = new RemoteIntPrimitive(0, this);
   public RemoteIntPrimitive hpMaxInt = new RemoteIntPrimitive(0, this);
   public RemoteLongPrimitive hpLong = new RemoteLongPrimitive(0L, this);
   public RemoteLongPrimitive hpMaxLong = new RemoteLongPrimitive(0L, this);
   public RemoteBooleanPrimitive rebootRecover = new RemoteBooleanPrimitive(false, this);
   public RemoteLongPrimitive rebootStartTime = new RemoteLongPrimitive(0L, this);
   public RemoteLongPrimitive rebootDuration = new RemoteLongPrimitive(0L, this);
   public RemoteLongBuffer initialPower = new RemoteLongBuffer(this);
   public RemoteLongBuffer initialBatteryPower = new RemoteLongBuffer(this);
   public RemoteLongBuffer initialShields = new RemoteLongBuffer(this);
   public RemoteControlModBuffer controlledByBuffer = new RemoteControlModBuffer(this);
   public RemoteIntPrimitive expectedNonEmptySegmentsFromLoad = new RemoteIntPrimitive(0, this);
   public RemoteString dockedTo = new RemoteString("NONE", this);
   public RemoteBuffer dockClientUndockRequests = new RemoteBuffer(RemoteBoolean.class, this);
   public RemoteVector3f dockingSize = new RemoteVector3f(this);
   public RemoteVector4i dockedElement = new RemoteVector4i(this);
   public RemoteBlockEffectUpdateBuffer effectUpdateBuffer = new RemoteBlockEffectUpdateBuffer(this);
   public RemoteVector4f dockingOrientation = new RemoteVector4f(this);
   public RemoteVector4f dockingTrans = new RemoteVector4f(new Vector4f(0.0F, 0.0F, 0.0F, 1.0F), this);
   public RemoteBuffer railTurretTransPrimary = new RemoteBuffer(RemoteVector4f.class, this);
   public RemoteBuffer railTurretTransSecondary = new RemoteBuffer(RemoteVector4f.class, this);
   public RemoteTextBlockBuffer textBlockChangeBuffer = new RemoteTextBlockBuffer(this);
   public RemoteBytePrimitive creatorId = new RemoteBytePrimitive((byte)0, this);
   public RemoteBytePrimitive classification = new RemoteBytePrimitive((byte)0, this);
   public RemoteBooleanPrimitive scrap = new RemoteBooleanPrimitive(this);
   public RemoteBooleanPrimitive vulnerable = new RemoteBooleanPrimitive(this);
   public RemoteBooleanPrimitive minable = new RemoteBooleanPrimitive(this);
   public RemoteBooleanPrimitive virtualBlueprint = new RemoteBooleanPrimitive(this);
   public RemoteBytePrimitive factionRigths = new RemoteBytePrimitive((byte)-1, this);
   public RemoteRailRequestBuffer railRequestBuffer = new RemoteRailRequestBuffer(this);
   public RemoteInterconnectStructureBuffer structureInterconnectRequestBuffer = new RemoteInterconnectStructureBuffer(this);
   public RemoteShipKeyConfigBuffer slotKeyBuffer = new RemoteShipKeyConfigBuffer(this);
   public RemoteBuffer shieldHits = new RemoteBuffer(RemoteVector4f.class, this);
   public RemoteBuffer hits = new RemoteBuffer(RemoteVector4f.class, this);
   public RemoteBooleanPrimitive additionalBlueprintData = new RemoteBooleanPrimitive(this);
   public RemoteLongBuffer clientToServerCheckEmptyConnection = new RemoteLongBuffer(this);
   public RemoteString currentOwner = new RemoteString(this);
   public RemoteBuffer currentOwnerChangeRequest = new RemoteBuffer(RemoteString.class, this);
   public RemoteString lastDockerPlayerServerLowerCase = new RemoteString(this);
   public RemoteLongPrimitive dbId = new RemoteLongPrimitive(-1L, this);
   public RemoteLongPrimitive lastAllowed = new RemoteLongPrimitive(0L, this);
   public RemoteBytePrimitive pullPermission;
   public RemoteLongBuffer pullPermissionAskAnswerBuffer;
   public RemoteByteBuffer pullPermissionChangeBuffer;
   public RemoteShortBuffer effectAddBuffer;
   public RemoteShortBuffer effectRemoveBuffer;
   public RemoteLongBuffer convertRequestBuffer;
   public RemoteLongBuffer bootRequestBuffer;
   public RemoteLongPrimitive activeReactor;
   public RemoteFloatBuffer blockDelayTimers;
   public RemoteBuffer ruleIndividualAddRemoveBuffer;
   public RemoteRuleStateChangeBuffer ruleChangeBuffer;
   public RemoteIntBuffer ruleStateRequestBuffer;

   public NetworkSegmentController(StateInterface var1, SendableSegmentController var2) {
      super(var1);
      this.pullPermission = new RemoteBytePrimitive((byte)SegmentController.PullPermission.ASK.ordinal(), this);
      this.pullPermissionAskAnswerBuffer = new RemoteLongBuffer(this);
      this.pullPermissionChangeBuffer = new RemoteByteBuffer(this);
      this.effectAddBuffer = new RemoteShortBuffer(this, 128);
      this.effectRemoveBuffer = new RemoteShortBuffer(this, 128);
      this.convertRequestBuffer = new RemoteLongBuffer(this);
      this.bootRequestBuffer = new RemoteLongBuffer(this);
      this.activeReactor = new RemoteLongPrimitive(0L, this);
      this.blockDelayTimers = new RemoteFloatBuffer(this);
      this.ruleIndividualAddRemoveBuffer = new RemoteBuffer(RemoteString.class, this);
      this.ruleChangeBuffer = new RemoteRuleStateChangeBuffer(this);
      this.ruleStateRequestBuffer = new RemoteIntBuffer(this);
   }

   public void onDelete(StateInterface var1) {
   }

   public void onInit(StateInterface var1) {
   }

   public RemoteShortBuffer getEffectAddBuffer() {
      return this.effectAddBuffer;
   }

   public RemoteShortBuffer getEffectRemoveBuffer() {
      return this.effectRemoveBuffer;
   }

   public RemoteRuleStateChangeBuffer getRuleStateChangeBuffer() {
      return this.ruleChangeBuffer;
   }

   public RemoteIntBuffer getRuleStateRequestBuffer() {
      return this.ruleStateRequestBuffer;
   }

   public RemoteBuffer getRuleIndividualAddRemoveBuffer() {
      return this.ruleIndividualAddRemoveBuffer;
   }
}
