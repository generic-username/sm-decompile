package org.schema.game.network.objects;

import org.schema.game.network.objects.remote.RemoteRuleStateChangeBuffer;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.RemoteIntBuffer;

public interface NTRuleInterface {
   RemoteRuleStateChangeBuffer getRuleStateChangeBuffer();

   RemoteIntBuffer getRuleStateRequestBuffer();

   RemoteBuffer getRuleIndividualAddRemoveBuffer();
}
