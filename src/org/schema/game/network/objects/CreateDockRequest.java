package org.schema.game.network.objects;

import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.data.VoidUniqueSegmentPiece;

public class CreateDockRequest {
   public VoidUniqueSegmentPiece core;
   public VoidUniqueSegmentPiece docker;
   public VoidUniqueSegmentPiece rail;
   public String name;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      this.core.serialize(var1);
      this.docker.serialize(var1);
      this.rail.serialize(var1);
      var1.writeUTF(this.name);
   }

   public void deserialize(DataInputStream var1, int var2, boolean var3) throws IOException {
      this.core = new VoidUniqueSegmentPiece();
      this.core.deserialize(var1);
      this.docker = new VoidUniqueSegmentPiece();
      this.docker.deserialize(var1);
      this.rail = new VoidUniqueSegmentPiece();
      this.rail.deserialize(var1);
      this.name = var1.readUTF();
   }
}
