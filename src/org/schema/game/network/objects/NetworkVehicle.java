package org.schema.game.network.objects;

import org.schema.game.common.controller.Vehicle;
import org.schema.schine.network.StateInterface;

public class NetworkVehicle extends NetworkSegmentController {
   public NetworkVehicle(StateInterface var1, Vehicle var2) {
      super(var1, var2);
   }
}
