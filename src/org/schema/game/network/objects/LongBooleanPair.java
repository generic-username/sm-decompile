package org.schema.game.network.objects;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.schine.network.SerialializationInterface;

public class LongBooleanPair implements SerialializationInterface {
   public long l;
   public boolean b;

   public LongBooleanPair() {
   }

   public LongBooleanPair(long var1, boolean var3) {
      this.l = var1;
      this.b = var3;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeLong(this.l);
      var1.writeBoolean(this.b);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.l = var1.readLong();
      this.b = var1.readBoolean();
   }
}
