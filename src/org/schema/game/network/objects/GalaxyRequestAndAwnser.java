package org.schema.game.network.objects;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class GalaxyRequestAndAwnser {
   public String ownerUID;
   public int factionUID;
   public int secX;
   public int secY;
   public int secZ;
   public NetworkClientChannel networkObjectOnServer;

   public void deserialize(DataInput var1, boolean var2) throws IOException {
      if (var2) {
         this.secX = var1.readInt();
         this.secY = var1.readInt();
         this.secZ = var1.readInt();
      } else {
         this.secX = var1.readInt();
         this.secY = var1.readInt();
         this.secZ = var1.readInt();
         if (var1.readByte() != 0) {
            this.ownerUID = var1.readUTF();
            this.factionUID = var1.readInt();
         }

      }
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      if (var2) {
         var1.writeInt(this.secX);
         var1.writeInt(this.secY);
         var1.writeInt(this.secZ);
         if (this.ownerUID != null) {
            var1.writeByte(1);
            var1.writeUTF(this.ownerUID);
            var1.writeInt(this.factionUID);
         } else {
            var1.writeByte(0);
         }
      } else {
         var1.writeInt(this.secX);
         var1.writeInt(this.secY);
         var1.writeInt(this.secZ);
      }
   }

   public String toString() {
      return "GalaxyRequestAndAwnser [ownerUID=" + this.ownerUID + ", factionUID=" + this.factionUID + ", secX=" + this.secX + ", secY=" + this.secY + ", secZ=" + this.secZ + ", networkObjectOnServer=" + this.networkObjectOnServer + "]";
   }
}
