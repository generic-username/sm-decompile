package org.schema.game.network.objects;

import org.schema.game.common.controller.ai.AINetworkInterface;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.ForcedAnimation;
import org.schema.game.common.data.player.NetworkPlayerInterface;
import org.schema.game.common.data.player.inventory.NetworkInventoryInterface;
import org.schema.game.network.objects.remote.RemoteForcedAnimation;
import org.schema.game.network.objects.remote.RemoteInventoryBuffer;
import org.schema.game.network.objects.remote.RemoteInventoryClientActionBuffer;
import org.schema.game.network.objects.remote.RemoteInventoryMultModBuffer;
import org.schema.game.network.objects.remote.RemoteInventorySlotRemoveBuffer;
import org.schema.game.network.objects.remote.RemoteLongStringBuffer;
import org.schema.game.network.objects.remote.RemoteShortIntPairBuffer;
import org.schema.schine.graphicsengine.animation.LoopMode;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndexElement;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.remote.RemoteArrayBuffer;
import org.schema.schine.network.objects.remote.RemoteBoolean;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.RemoteBytePrimitive;
import org.schema.schine.network.objects.remote.RemoteFloatPrimitive;
import org.schema.schine.network.objects.remote.RemoteIntPrimitive;
import org.schema.schine.network.objects.remote.RemoteLongBuffer;
import org.schema.schine.network.objects.remote.RemoteLongIntPair;
import org.schema.schine.network.objects.remote.RemoteLongPrimitiveArray;
import org.schema.schine.network.objects.remote.RemoteString;
import org.schema.schine.network.objects.remote.RemoteStringArray;
import org.schema.schine.network.objects.remote.RemoteVector3f;
import org.schema.schine.resource.CreatureStructure;

public class TargetableAICreatureNetworkObject extends NetworkPlayerCharacter implements AINetworkInterface, NetworkPlayerInterface, NetworkInventoryInterface {
   public RemoteVector3f target = new RemoteVector3f(this);
   public RemoteBoolean hasTarget = new RemoteBoolean(this);
   public RemoteString affinity = new RemoteString(this);
   public RemoteInventoryBuffer inventoryBuffer;
   public RemoteInventoryClientActionBuffer inventoryClientActionBuffer = new RemoteInventoryClientActionBuffer(this);
   public RemoteInventoryMultModBuffer inventoryMultModBuffer = new RemoteInventoryMultModBuffer(this);
   public RemoteString debugState = new RemoteString("", this);
   public RemoteArrayBuffer aiSettingsModification = new RemoteArrayBuffer(2, RemoteStringArray.class, this);
   public RemoteForcedAnimation forcedAnimation = new RemoteForcedAnimation(new ForcedAnimation((CreatureStructure.PartType)null, (AnimationIndexElement)null, (LoopMode)null, 0.0F, false), this);
   public RemoteBytePrimitive buildSlot = new RemoteBytePrimitive((byte)0, this);
   public RemoteVector3f targetPosition = new RemoteVector3f(this);
   public RemoteIntPrimitive targetId = new RemoteIntPrimitive(-1, this);
   public RemoteBytePrimitive targetType = new RemoteBytePrimitive((byte)-1, this);
   public RemoteVector3f targetVelocity = new RemoteVector3f(this);
   public RemoteString realName = new RemoteString(this);
   public RemoteLongBuffer inventoryProductionBuffer = new RemoteLongBuffer(this);
   public RemoteShortIntPairBuffer inventoryFilterBuffer = new RemoteShortIntPairBuffer(this);
   public RemoteFloatPrimitive health = new RemoteFloatPrimitive(4264.0F, this);
   public RemoteLongPrimitiveArray sittingState = new RemoteLongPrimitiveArray(4, this);
   public RemoteInventorySlotRemoveBuffer inventorySlotRemoveRequestBuffer = new RemoteInventorySlotRemoveBuffer(this);
   public RemoteShortIntPairBuffer inventoryFillBuffer = new RemoteShortIntPairBuffer(this);
   public RemoteBuffer inventoryProductionLimitBuffer = new RemoteBuffer(RemoteLongIntPair.class, this);

   public RemoteShortIntPairBuffer getInventoryFillBuffer() {
      return this.inventoryFillBuffer;
   }

   public RemoteBuffer getInventoryProductionLimitBuffer() {
      return this.inventoryProductionLimitBuffer;
   }

   public RemoteInventorySlotRemoveBuffer getInventorySlotRemoveRequestBuffer() {
      return this.inventorySlotRemoveRequestBuffer;
   }

   public TargetableAICreatureNetworkObject(StateInterface var1, AbstractOwnerState var2) {
      super(var1);
      this.inventoryBuffer = new RemoteInventoryBuffer(var2, this);
   }

   public RemoteInventoryBuffer getInventoriesChangeBuffer() {
      return this.inventoryBuffer;
   }

   public RemoteInventoryClientActionBuffer getInventoryClientActionBuffer() {
      return this.inventoryClientActionBuffer;
   }

   public RemoteInventoryMultModBuffer getInventoryMultModBuffer() {
      return this.inventoryMultModBuffer;
   }

   public RemoteLongBuffer getInventoryProductionBuffer() {
      return this.inventoryProductionBuffer;
   }

   public RemoteShortIntPairBuffer getInventoryFilterBuffer() {
      return this.inventoryFilterBuffer;
   }

   public RemoteLongStringBuffer getInventoryCustomNameModBuffer() {
      return null;
   }

   public RemoteArrayBuffer getAiSettingsModification() {
      return this.aiSettingsModification;
   }

   public RemoteString getDebugState() {
      return this.debugState;
   }

   public RemoteBytePrimitive getBuildSlot() {
      return this.buildSlot;
   }
}
