package org.schema.game.network.objects;

import org.schema.game.common.data.element.ElementInformation;

public interface TradePriceInterface {
   short getType();

   int getAmount();

   int getPrice();

   boolean isSell();

   boolean isBuy();

   int getLimit();

   ElementInformation getInfo();

   void setAmount(int var1);
}
