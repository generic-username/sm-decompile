package org.schema.game.network.objects;

import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkEntity;
import org.schema.schine.network.objects.remote.RemoteFloatPrimitive;
import org.schema.schine.network.objects.remote.RemoteString;

public class NetworkPlanetCore extends NetworkEntity {
   public RemoteFloatPrimitive radius = new RemoteFloatPrimitive(200.0F, this);
   public RemoteString uid = new RemoteString("", this);
   public RemoteFloatPrimitive hp = new RemoteFloatPrimitive(1.0F, this);

   public NetworkPlanetCore(StateInterface var1) {
      super(var1);
   }

   public void onDelete(StateInterface var1) {
   }

   public void onInit(StateInterface var1) {
   }
}
