package org.schema.game.network.objects;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.client.controller.ClientChannel;
import org.schema.schine.network.SerialializationInterface;

public class FowRequestAndAwnser implements SerialializationInterface {
   public int sysX;
   public int sysY;
   public int sysZ;
   public boolean visible = false;
   public ClientChannel receivedClientChannel;

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      if (var3) {
         this.sysX = var1.readInt();
         this.sysY = var1.readInt();
         this.sysZ = var1.readInt();
      } else {
         this.sysX = var1.readInt();
         this.sysY = var1.readInt();
         this.sysZ = var1.readInt();
         this.visible = var1.readBoolean();
      }
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      if (var2) {
         var1.writeInt(this.sysX);
         var1.writeInt(this.sysY);
         var1.writeInt(this.sysZ);
         var1.writeBoolean(this.visible);
      } else {
         var1.writeInt(this.sysX);
         var1.writeInt(this.sysY);
         var1.writeInt(this.sysZ);
      }
   }
}
