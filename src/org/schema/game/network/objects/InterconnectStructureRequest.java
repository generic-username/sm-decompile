package org.schema.game.network.objects;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.VoidUniqueSegmentPiece;
import org.schema.schine.network.SerialializationInterface;

public class InterconnectStructureRequest implements SerialializationInterface {
   public VoidUniqueSegmentPiece fromPiece;
   public VoidUniqueSegmentPiece toPiece;
   public int playerId = -1;

   public InterconnectStructureRequest() {
      this.fromPiece = new VoidUniqueSegmentPiece();
      this.toPiece = new VoidUniqueSegmentPiece();
   }

   public InterconnectStructureRequest(SegmentPiece var1, SegmentPiece var2, int var3) {
      this.fromPiece = new VoidUniqueSegmentPiece(var1);
      this.toPiece = new VoidUniqueSegmentPiece(var2);
      this.playerId = var3;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      this.fromPiece.serialize(var1);
      this.toPiece.serialize(var1);
      var1.writeInt(this.playerId);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.fromPiece.deserialize(var1);
      this.toPiece.deserialize(var1);
      this.playerId = var1.readInt();
   }
}
