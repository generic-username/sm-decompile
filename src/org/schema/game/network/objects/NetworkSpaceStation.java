package org.schema.game.network.objects;

import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.controller.ShopNetworkInterface;
import org.schema.game.common.controller.ai.AINetworkInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.player.inventory.NetworkInventoryInterface;
import org.schema.game.network.objects.remote.RemoteCompressedShopPricesBuffer;
import org.schema.game.network.objects.remote.RemoteInventoryBuffer;
import org.schema.game.network.objects.remote.RemoteInventoryClientActionBuffer;
import org.schema.game.network.objects.remote.RemoteInventoryMultModBuffer;
import org.schema.game.network.objects.remote.RemoteInventorySlotRemoveBuffer;
import org.schema.game.network.objects.remote.RemoteLongStringBuffer;
import org.schema.game.network.objects.remote.RemoteReactorBonusUpdateBuffer;
import org.schema.game.network.objects.remote.RemoteReactorPriorityQueueBuffer;
import org.schema.game.network.objects.remote.RemoteReactorSetBuffer;
import org.schema.game.network.objects.remote.RemoteReactorTreeBuffer;
import org.schema.game.network.objects.remote.RemoteShopOptionBuffer;
import org.schema.game.network.objects.remote.RemoteShortIntPairBuffer;
import org.schema.game.network.objects.remote.RemoteTradePriceBuffer;
import org.schema.game.network.objects.remote.RemoteTradePriceSingleBuffer;
import org.schema.game.network.objects.remote.RemoteValueUpdateBuffer;
import org.schema.game.network.objects.valueUpdate.NTValueUpdateInterface;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.remote.RemoteArrayBuffer;
import org.schema.schine.network.objects.remote.RemoteBooleanPrimitive;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.RemoteByteBuffer;
import org.schema.schine.network.objects.remote.RemoteFloatBuffer;
import org.schema.schine.network.objects.remote.RemoteIntArrayBuffer;
import org.schema.schine.network.objects.remote.RemoteLongBuffer;
import org.schema.schine.network.objects.remote.RemoteLongIntPair;
import org.schema.schine.network.objects.remote.RemoteLongPrimitive;
import org.schema.schine.network.objects.remote.RemoteShortBuffer;
import org.schema.schine.network.objects.remote.RemoteString;
import org.schema.schine.network.objects.remote.RemoteStringArray;
import org.schema.schine.network.objects.remote.RemoteVector4i;

public class NetworkSpaceStation extends NetworkSegmentController implements ShopNetworkInterface, AINetworkInterface, NetworkInventoryInterface, NetworkDoorInterface, NetworkLiftInterface, PowerInterfaceNetworkObject, NTValueUpdateInterface {
   public RemoteInventoryMultModBuffer inventoryMultModBuffer = new RemoteInventoryMultModBuffer(this);
   public RemoteBuffer liftActivate = new RemoteBuffer(RemoteVector4i.class, this);
   public RemoteInventoryBuffer inventoryBuffer;
   public RemoteInventoryClientActionBuffer inventoryClientActionBuffer = new RemoteInventoryClientActionBuffer(this);
   public RemoteBuffer doorActivate = new RemoteBuffer(RemoteVector4i.class, this);
   public RemoteString debugState = new RemoteString("", this);
   public RemoteArrayBuffer aiSettingsModification = new RemoteArrayBuffer(2, RemoteStringArray.class, this);
   public RemoteIntArrayBuffer inventoryActivateBuffer = new RemoteIntArrayBuffer(3, this);
   public RemoteBooleanPrimitive shopInfiniteSupply = new RemoteBooleanPrimitive(this);
   public RemoteBooleanPrimitive tradeNodeOn = new RemoteBooleanPrimitive(this);
   public RemoteByteBuffer tradeNodeOnRequests = new RemoteByteBuffer(this);
   public RemoteLongPrimitive shopCredits = new RemoteLongPrimitive(0L, this);
   public RemoteLongBuffer inventoryProductionBuffer = new RemoteLongBuffer(this);
   public RemoteValueUpdateBuffer valueUpdateBuffer;
   public RemoteShortIntPairBuffer inventoryFilterBuffer = new RemoteShortIntPairBuffer(this);
   public RemoteLongStringBuffer customNameModBuffer = new RemoteLongStringBuffer(this);
   public RemoteCompressedShopPricesBuffer compressedPricesBuffer;
   public RemoteInventorySlotRemoveBuffer inventorySlotRemoveRequestBuffer = new RemoteInventorySlotRemoveBuffer(this);
   public RemoteLongPrimitive tradePermission = new RemoteLongPrimitive(0L, this);
   public RemoteLongPrimitive localPermission = new RemoteLongPrimitive(0L, this);
   public RemoteTradePriceSingleBuffer pricesModifyBuffer = new RemoteTradePriceSingleBuffer(this);
   public RemoteShopOptionBuffer shopOptionBuffer = new RemoteShopOptionBuffer(this);
   public RemoteTradePriceBuffer pricesFullBuffer = new RemoteTradePriceBuffer(this);
   public RemoteReactorSetBuffer reactorSetBuffer;
   public RemoteReactorTreeBuffer reactorTreeBuffer;
   public RemoteShortBuffer reactorRecalibrateBuffer = new RemoteShortBuffer(this, 8);
   public RemoteBuffer reactorChangeBuffer = new RemoteBuffer(RemoteLongIntPair.class, this);
   public RemoteReactorPriorityQueueBuffer reactorPriorityQueueBuffer;
   public RemoteShortIntPairBuffer inventoryFillBuffer = new RemoteShortIntPairBuffer(this);
   public RemoteBuffer inventoryProductionLimitBuffer = new RemoteBuffer(RemoteLongIntPair.class, this);
   public RemoteFloatBuffer reactorCooldownBuffer = new RemoteFloatBuffer(this);
   public RemoteFloatBuffer energyStreamCooldownBuffer = new RemoteFloatBuffer(this);
   public RemoteReactorBonusUpdateBuffer reactorBonusMatrixUpdateBuffer = new RemoteReactorBonusUpdateBuffer(this);

   public RemoteShortIntPairBuffer getInventoryFillBuffer() {
      return this.inventoryFillBuffer;
   }

   public RemoteBuffer getInventoryProductionLimitBuffer() {
      return this.inventoryProductionLimitBuffer;
   }

   public RemoteInventorySlotRemoveBuffer getInventorySlotRemoveRequestBuffer() {
      return this.inventorySlotRemoveRequestBuffer;
   }

   public NetworkSpaceStation(StateInterface var1, SendableSegmentController var2) {
      super(var1, var2);
      this.inventoryBuffer = new RemoteInventoryBuffer(((ManagedSegmentController)var2).getManagerContainer(), this);
      this.valueUpdateBuffer = new RemoteValueUpdateBuffer(this, ((ManagedSegmentController)var2).getManagerContainer());
      this.compressedPricesBuffer = new RemoteCompressedShopPricesBuffer(((ShopInterface)((ManagedSegmentController)var2).getManagerContainer()).getShoppingAddOn(), this);
      this.reactorSetBuffer = new RemoteReactorSetBuffer(this, ((ManagedSegmentController)var2).getManagerContainer().getPowerInterface());
      this.reactorTreeBuffer = new RemoteReactorTreeBuffer(this, ((ManagedSegmentController)var2).getManagerContainer().getPowerInterface());
      this.reactorPriorityQueueBuffer = new RemoteReactorPriorityQueueBuffer(this, ((ManagedSegmentController)var2).getManagerContainer().getPowerInterface());
   }

   public RemoteArrayBuffer getAiSettingsModification() {
      return this.aiSettingsModification;
   }

   public RemoteString getDebugState() {
      return this.debugState;
   }

   public RemoteBuffer getDoorActivate() {
      return this.doorActivate;
   }

   public RemoteInventoryBuffer getInventoriesChangeBuffer() {
      return this.inventoryBuffer;
   }

   public RemoteInventoryClientActionBuffer getInventoryClientActionBuffer() {
      return this.inventoryClientActionBuffer;
   }

   public RemoteInventoryMultModBuffer getInventoryMultModBuffer() {
      return this.inventoryMultModBuffer;
   }

   public RemoteLongBuffer getInventoryProductionBuffer() {
      return this.inventoryProductionBuffer;
   }

   public RemoteShortIntPairBuffer getInventoryFilterBuffer() {
      return this.inventoryFilterBuffer;
   }

   public RemoteLongStringBuffer getInventoryCustomNameModBuffer() {
      return this.customNameModBuffer;
   }

   public RemoteBuffer getLiftActivate() {
      return this.liftActivate;
   }

   public RemoteTradePriceSingleBuffer getPriceModifyBuffer() {
      return this.pricesModifyBuffer;
   }

   public RemoteShopOptionBuffer getShopOptionBuffer() {
      return this.shopOptionBuffer;
   }

   public RemoteTradePriceBuffer getPricesUpdateBuffer() {
      return this.pricesFullBuffer;
   }

   public RemoteCompressedShopPricesBuffer getCompressedPricesUpdateBuffer() {
      return this.compressedPricesBuffer;
   }

   public RemoteLongPrimitive getShopCredits() {
      return this.shopCredits;
   }

   public RemoteBooleanPrimitive getInfiniteSupply() {
      return this.shopInfiniteSupply;
   }

   public RemoteValueUpdateBuffer getValueUpdateBuffer() {
      return this.valueUpdateBuffer;
   }

   public RemoteBooleanPrimitive getTradeNodeOn() {
      return this.tradeNodeOn;
   }

   public RemoteByteBuffer getTradeNodeOnRequest() {
      return this.tradeNodeOnRequests;
   }

   public RemoteLongPrimitive getTradePermission() {
      return this.tradePermission;
   }

   public RemoteLongPrimitive getLocalPermission() {
      return this.localPermission;
   }

   public RemoteReactorSetBuffer getReactorSetBuffer() {
      return this.reactorSetBuffer;
   }

   public RemoteReactorTreeBuffer getReactorTreeBuffer() {
      return this.reactorTreeBuffer;
   }

   public RemoteLongBuffer getConvertRequestBuffer() {
      return this.convertRequestBuffer;
   }

   public RemoteLongBuffer getBootRequestBuffer() {
      return this.bootRequestBuffer;
   }

   public RemoteLongPrimitive getActiveReactor() {
      return this.activeReactor;
   }

   public RemoteShortBuffer getRecalibrateRequestBuffer() {
      return this.reactorRecalibrateBuffer;
   }

   public RemoteBuffer getReactorChangeBuffer() {
      return this.reactorChangeBuffer;
   }

   public RemoteReactorPriorityQueueBuffer getReactorPrioQueueBuffer() {
      return this.reactorPriorityQueueBuffer;
   }

   public RemoteFloatBuffer getReactorCooldownBuffer() {
      return this.reactorCooldownBuffer;
   }

   public RemoteFloatBuffer getEnergyStreamCooldownBuffer() {
      return this.energyStreamCooldownBuffer;
   }

   public RemoteReactorBonusUpdateBuffer getReactorBonusMatrixUpdateBuffer() {
      return this.reactorBonusMatrixUpdateBuffer;
   }
}
