package org.schema.game.network.objects;

import org.schema.schine.network.objects.remote.RemoteBuffer;

public interface NetworkLiftInterface {
   RemoteBuffer getLiftActivate();
}
