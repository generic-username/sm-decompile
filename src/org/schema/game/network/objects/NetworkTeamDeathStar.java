package org.schema.game.network.objects;

import org.schema.game.common.controller.TeamDeathStar;
import org.schema.schine.network.StateInterface;

public class NetworkTeamDeathStar extends NetworkSegmentController {
   public NetworkTeamDeathStar(StateInterface var1, TeamDeathStar var2) {
      super(var1, var2);
   }
}
