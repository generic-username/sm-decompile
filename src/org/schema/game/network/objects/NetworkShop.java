package org.schema.game.network.objects;

import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.controller.ShopNetworkInterface;
import org.schema.game.common.data.player.inventory.InventoryHolder;
import org.schema.game.common.data.player.inventory.NetworkInventoryInterface;
import org.schema.game.network.objects.remote.RemoteCompressedShopPricesBuffer;
import org.schema.game.network.objects.remote.RemoteInventoryBuffer;
import org.schema.game.network.objects.remote.RemoteInventoryClientActionBuffer;
import org.schema.game.network.objects.remote.RemoteInventoryMultModBuffer;
import org.schema.game.network.objects.remote.RemoteInventorySlotRemoveBuffer;
import org.schema.game.network.objects.remote.RemoteLongStringBuffer;
import org.schema.game.network.objects.remote.RemoteShopOptionBuffer;
import org.schema.game.network.objects.remote.RemoteShortIntPairBuffer;
import org.schema.game.network.objects.remote.RemoteTradePriceBuffer;
import org.schema.game.network.objects.remote.RemoteTradePriceSingleBuffer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.remote.RemoteBooleanPrimitive;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.RemoteByteBuffer;
import org.schema.schine.network.objects.remote.RemoteLongBuffer;
import org.schema.schine.network.objects.remote.RemoteLongIntPair;
import org.schema.schine.network.objects.remote.RemoteLongPrimitive;

public class NetworkShop extends NetworkSegmentController implements ShopNetworkInterface, NetworkInventoryInterface {
   public RemoteInventoryMultModBuffer inventoryMultModBuffer = new RemoteInventoryMultModBuffer(this);
   public RemoteInventoryBuffer inventoryBuffer;
   public RemoteLongPrimitive shopCredits = new RemoteLongPrimitive(0L, this);
   public RemoteLongBuffer inventoryProductionBuffer = new RemoteLongBuffer(this);
   public RemoteBooleanPrimitive shopInfiniteSupply = new RemoteBooleanPrimitive(this);
   public RemoteBooleanPrimitive tradeNodeOn = new RemoteBooleanPrimitive(this);
   public RemoteByteBuffer tradeNodeOnRequests = new RemoteByteBuffer(this);
   public RemoteShortIntPairBuffer inventoryFilterBuffer = new RemoteShortIntPairBuffer(this);
   public RemoteInventoryClientActionBuffer inventoryClientActionBuffer = new RemoteInventoryClientActionBuffer(this);
   public RemoteCompressedShopPricesBuffer compressedPricesBuffer;
   public RemoteInventorySlotRemoveBuffer inventorySlotRemoveRequestBuffer = new RemoteInventorySlotRemoveBuffer(this);
   public RemoteTradePriceSingleBuffer pricesModifyBuffer = new RemoteTradePriceSingleBuffer(this);
   public RemoteShopOptionBuffer shopOptionBuffer = new RemoteShopOptionBuffer(this);
   public RemoteTradePriceBuffer pricesFullBuffer = new RemoteTradePriceBuffer(this);
   public RemoteLongPrimitive tradePermission = new RemoteLongPrimitive(0L, this);
   public RemoteLongPrimitive localPermission = new RemoteLongPrimitive(0L, this);
   public RemoteShortIntPairBuffer inventoryFillBuffer = new RemoteShortIntPairBuffer(this);
   public RemoteBuffer inventoryProductionLimitBuffer = new RemoteBuffer(RemoteLongIntPair.class, this);

   public RemoteShortIntPairBuffer getInventoryFillBuffer() {
      return this.inventoryFillBuffer;
   }

   public RemoteBuffer getInventoryProductionLimitBuffer() {
      return this.inventoryProductionLimitBuffer;
   }

   public RemoteTradePriceSingleBuffer getPriceModifyBuffer() {
      return this.pricesModifyBuffer;
   }

   public RemoteShopOptionBuffer getShopOptionBuffer() {
      return this.shopOptionBuffer;
   }

   public RemoteTradePriceBuffer getPricesUpdateBuffer() {
      return this.pricesFullBuffer;
   }

   public RemoteInventorySlotRemoveBuffer getInventorySlotRemoveRequestBuffer() {
      return this.inventorySlotRemoveRequestBuffer;
   }

   public NetworkShop(StateInterface var1, SendableSegmentController var2) {
      super(var1, var2);
      this.inventoryBuffer = new RemoteInventoryBuffer((InventoryHolder)var2, this);
      this.compressedPricesBuffer = new RemoteCompressedShopPricesBuffer(((ShopInterface)var2).getShoppingAddOn(), this);
   }

   public RemoteInventoryBuffer getInventoriesChangeBuffer() {
      return this.inventoryBuffer;
   }

   public RemoteInventoryClientActionBuffer getInventoryClientActionBuffer() {
      return this.inventoryClientActionBuffer;
   }

   public RemoteInventoryMultModBuffer getInventoryMultModBuffer() {
      return this.inventoryMultModBuffer;
   }

   public RemoteLongBuffer getInventoryProductionBuffer() {
      return this.inventoryProductionBuffer;
   }

   public RemoteShortIntPairBuffer getInventoryFilterBuffer() {
      return this.inventoryFilterBuffer;
   }

   public RemoteLongStringBuffer getInventoryCustomNameModBuffer() {
      return null;
   }

   public RemoteCompressedShopPricesBuffer getCompressedPricesUpdateBuffer() {
      return this.compressedPricesBuffer;
   }

   public RemoteLongPrimitive getShopCredits() {
      return this.shopCredits;
   }

   public RemoteBooleanPrimitive getInfiniteSupply() {
      return this.shopInfiniteSupply;
   }

   public RemoteBooleanPrimitive getTradeNodeOn() {
      return this.tradeNodeOn;
   }

   public RemoteByteBuffer getTradeNodeOnRequest() {
      return this.tradeNodeOnRequests;
   }

   public RemoteLongPrimitive getTradePermission() {
      return this.tradePermission;
   }

   public RemoteLongPrimitive getLocalPermission() {
      return this.localPermission;
   }
}
