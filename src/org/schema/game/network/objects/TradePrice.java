package org.schema.game.network.objects;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.network.SerialializationInterface;

public class TradePrice implements TradePriceInterface, SerialializationInterface {
   private short type;
   private int amount;
   private int price;
   private int limit;
   private boolean sell;

   public TradePrice(short var1, int var2, int var3, int var4, boolean var5) {
      this.type = var1;
      this.amount = var2;
      this.price = var3;
      this.sell = var5;
      this.limit = var4;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeShort(this.type);
      var1.writeInt(this.amount);
      var1.writeInt(this.price);
      var1.writeInt(this.limit);
      var1.writeBoolean(this.sell);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.type = var1.readShort();
      this.amount = var1.readInt();
      this.price = var1.readInt();
      this.limit = var1.readInt();
      this.sell = var1.readBoolean();
   }

   public short getType() {
      return this.type;
   }

   public int getAmount() {
      return this.amount;
   }

   public int getPrice() {
      return this.price;
   }

   public int getLimit() {
      return this.limit;
   }

   public boolean isSell() {
      return this.sell;
   }

   public int hashCode() {
      return (this.isSell() ? 100000 : 1) * this.getType();
   }

   public boolean equals(Object var1) {
      return ((TradePriceInterface)var1).isSell() == this.isSell() && ((TradePriceInterface)var1).getType() == this.getType();
   }

   public ElementInformation getInfo() {
      return ElementKeyMap.getInfoFast(this.type);
   }

   public void setAmount(int var1) {
      this.amount = var1;
   }

   public boolean isBuy() {
      return !this.sell;
   }

   public void setLimit(int var1) {
      this.limit = var1;
   }

   public void setPrice(int var1) {
      this.price = var1;
   }

   public void setSell(boolean var1) {
      this.sell = var1;
   }
}
