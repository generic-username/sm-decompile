package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.mines.updates.MineUpdate;
import org.schema.schine.network.client.ClientProcessor;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteBuffer;

public class RemoteMineUpdateBuffer extends RemoteBuffer {
   public RemoteMineUpdateBuffer(NetworkObject var1) {
      super(RemoteMineUpdate.class, var1);
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      int var3 = var1.readInt();

      for(int var4 = 0; var4 < var3; ++var4) {
         MineUpdate var5 = MineUpdate.deserializeStatic(var1, var2, this.onServer);
         RemoteMineUpdate var6 = new RemoteMineUpdate(var5, this.onServer);
         if (!this.onServer) {
            var5.timeStampServerSent = ((ClientProcessor)GameClientState.instance.getProcessor()).getServerPacketSentTimestamp();
         }

         this.getReceiveBuffer().add(var6);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      byte var2 = 0;
      synchronized((ObjectArrayList)this.get()) {
         var1.writeInt(((ObjectArrayList)this.get()).size());
         int var7 = var2 + 4;

         RemoteMineUpdate var5;
         for(Iterator var4 = ((ObjectArrayList)this.get()).iterator(); var4.hasNext(); var7 += var5.toByteStream(var1)) {
            var5 = (RemoteMineUpdate)var4.next();
         }

         ((ObjectArrayList)this.get()).clear();
         return var7;
      }
   }

   protected void cacheConstructor() {
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }
}
