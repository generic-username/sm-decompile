package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.data.player.playermessage.PlayerMessage;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemotePlayerMessage extends RemoteField {
   public RemotePlayerMessage(PlayerMessage var1, boolean var2) {
      super(var1, var2);
   }

   public RemotePlayerMessage(PlayerMessage var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      this.set(PlayerMessage.decode(var1));
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      ((PlayerMessage)this.get()).encode(var1);
      return 1;
   }
}
