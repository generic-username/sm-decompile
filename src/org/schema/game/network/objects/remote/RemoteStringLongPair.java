package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.network.objects.StringLongLongPair;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteStringLongPair extends RemoteField {
   public RemoteStringLongPair(StringLongLongPair var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteStringLongPair(StringLongLongPair var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((StringLongLongPair)this.get()).playerName = var1.readUTF();
      ((StringLongLongPair)this.get()).timeStamp = var1.readLong();
      ((StringLongLongPair)this.get()).size = var1.readLong();
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeUTF(((StringLongLongPair)this.get()).playerName);
      var1.writeLong(((StringLongLongPair)this.get()).timeStamp);
      var1.writeLong(((StringLongLongPair)this.get()).size);
      return 1;
   }
}
