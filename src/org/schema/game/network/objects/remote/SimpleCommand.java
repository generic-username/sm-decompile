package org.schema.game.network.objects.remote;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.schine.network.Command;
import org.schema.schine.network.SerialializationInterface;

public abstract class SimpleCommand implements SerialializationInterface {
   private Object[] args;
   private int command;
   private int updateSenderStateId;

   public SimpleCommand(Enum var1, Object[] var2) {
      this.setArgs(var2);
      this.setCommand(var1.ordinal());
      this.checkMatches(var1, var2);
   }

   public SimpleCommand() {
   }

   protected abstract void checkMatches(Enum var1, Object[] var2);

   public void serialize(DataOutput var1) throws IOException {
      var1.writeInt(this.getCommand());
      Command.serialize(this.getArgs(), var1);
   }

   public void deserialize(DataInput var1, int var2) throws IOException {
      this.setCommand(var1.readInt());
      this.setArgs(Command.deserialize(var1));
      this.updateSenderStateId = var2;
   }

   public int getCommand() {
      return this.command;
   }

   public void setCommand(int var1) {
      this.command = var1;
   }

   public Object[] getArgs() {
      return this.args;
   }

   public void setArgs(Object[] var1) {
      this.args = var1;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      this.serialize(var1);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.deserialize(var1, var2);
   }

   public int getUpdateSenderStateId() {
      return this.updateSenderStateId;
   }
}
