package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.data.element.SendableControlMod;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteControlMod extends RemoteField {
   public RemoteControlMod(SendableControlMod var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteControlMod(SendableControlMod var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((SendableControlMod)this.get()).add = var1.readBoolean();
      ((SendableControlMod)this.get()).controlledType = var1.readShort();
      ((SendableControlMod)this.get()).from = var1.readLong();
      ((SendableControlMod)this.get()).to = var1.readLong();
      if (((SendableControlMod)this.get()).controlledType <= 0) {
         System.err.println("Exception: received invalid controller from ID: " + var2 + "; controller " + ((SendableControlMod)this.get()).controlledType);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeBoolean(((SendableControlMod)this.get()).add);
      if (((SendableControlMod)this.get()).controlledType <= 0) {
         System.err.println("Exception: sending invalid controller: " + ((SendableControlMod)this.get()).controlledType);
      }

      var1.writeShort(((SendableControlMod)this.get()).controlledType);
      var1.writeLong(((SendableControlMod)this.get()).from);
      var1.writeLong(((SendableControlMod)this.get()).to);
      return 1;
   }
}
