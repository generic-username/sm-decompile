package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.VoidSegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteSegmentPiece extends RemoteField {
   public static final int FULL = 0;
   public static final int ACTIVATE = 16;
   public static final int DEACTIVATE = 32;
   public static final int REMOVE = 64;
   public static final int REMOVE_SOFT = 128;
   private static final int addBits = 8;
   private static final int shortBits = 4;
   public int senderId;
   public long controllerPos = Long.MIN_VALUE;
   public int simpleMode = 0;

   public RemoteSegmentPiece(SegmentPiece var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteSegmentPiece(SegmentPiece var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 1;
   }

   public int hashCode() {
      return ((SegmentPiece)this.get()).hashCode();
   }

   public boolean equals(Object var1) {
      return ((SegmentPiece)this.get()).equals(((RemoteSegmentPiece)var1).get());
   }

   public void fromByteStream(VoidSegmentPiece var1, DataInputStream var2, int var3) throws IOException {
      int var4;
      boolean var5;
      if ((var4 = var2.readByte() & 255) >= 8) {
         var4 -= 8;
         var5 = true;
      } else {
         var5 = false;
      }

      if (var4 >= 4) {
         var4 -= 4;
         this.controllerPos = ElementCollection.getIndex(var2.readShort(), var2.readShort(), var2.readShort());
      } else {
         this.controllerPos = Long.MIN_VALUE;
      }

      int var6;
      int var7;
      if (var4 == 0) {
         var4 = var2.readByte();
         var6 = var2.readByte();
         var7 = var2.readByte();
      } else if (var4 == 1) {
         var4 = var2.readShort();
         var6 = var2.readShort();
         var7 = var2.readShort();
      } else {
         var4 = var2.readInt();
         var6 = var2.readInt();
         var7 = var2.readInt();
      }

      int var8 = SegmentPiece.deserializeData(var2);
      var1.forceClientSegmentAdd = var5;
      var1.senderId = var3;
      var1.controllerPos = this.controllerPos;
      var1.setDataByReference(var8);
      var1.voidPos.set(var4, var6, var7);
      this.set(var1);
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      VoidSegmentPiece var3 = new VoidSegmentPiece();
      this.fromByteStream(var3, var1, var2);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      assert this.get() != null;

      if (((SegmentPiece)this.get()).getSegment() != null) {
         this.writeDynamicPosition(((SegmentPiece)this.get()).x + ((SegmentPiece)this.get()).getSegment().pos.x, ((SegmentPiece)this.get()).y + ((SegmentPiece)this.get()).getSegment().pos.y, ((SegmentPiece)this.get()).z + ((SegmentPiece)this.get()).getSegment().pos.z, ((SegmentPiece)this.get()).forceClientSegmentAdd, var1);
      }

      SegmentPiece.serializeData(var1, ((SegmentPiece)this.get()).getData());
      return 1;
   }

   public void writeDynamicPosition(int var1, int var2, int var3, boolean var4, DataOutputStream var5) throws IOException {
      int var6 = 0;
      if (var4) {
         var6 += 8;
      }

      if (this.controllerPos != Long.MIN_VALUE) {
         var6 += 4;
      }

      if (var1 >= -128 && var2 >= -128 && var3 >= -128 && var1 <= -128 && var2 <= -128 && var3 <= 127) {
         var5.writeByte(var6);
         if (this.controllerPos != Long.MIN_VALUE) {
            var5.writeShort(ElementCollection.getPosX(this.controllerPos));
            var5.writeShort(ElementCollection.getPosY(this.controllerPos));
            var5.writeShort(ElementCollection.getPosZ(this.controllerPos));
         }

         var5.writeByte(var1);
         var5.writeByte(var2);
         var5.writeByte(var3);
      } else if (var1 >= -32768 && var2 >= -32768 && var3 >= -32768 && var1 <= -32768 && var2 <= -32768 && var3 <= 32767) {
         var5.writeByte(var6 + 1);
         if (this.controllerPos != Long.MIN_VALUE) {
            var5.writeShort(ElementCollection.getPosX(this.controllerPos));
            var5.writeShort(ElementCollection.getPosY(this.controllerPos));
            var5.writeShort(ElementCollection.getPosZ(this.controllerPos));
         }

         var5.writeShort(var1);
         var5.writeShort(var2);
         var5.writeShort(var3);
      } else {
         var5.writeByte(var6 + 2);
         if (this.controllerPos != Long.MIN_VALUE) {
            var5.writeShort(ElementCollection.getPosX(this.controllerPos));
            var5.writeShort(ElementCollection.getPosY(this.controllerPos));
            var5.writeShort(ElementCollection.getPosZ(this.controllerPos));
         }

         var5.writeInt(var1);
         var5.writeInt(var2);
         var5.writeInt(var3);
      }
   }
}
