package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.network.objects.DockingRequest;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteDockLand extends RemoteField {
   public RemoteDockLand(DockingRequest var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteDockLand(DockingRequest var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 12 + ((DockingRequest)this.get()).id.length() + 4 + 1;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      boolean var7 = var1.readBoolean();
      String var3 = var1.readUTF();
      int var4 = var1.readInt();
      int var5 = var1.readInt();
      int var6 = var1.readInt();
      ((DockingRequest)this.get()).set(var7, var3, new Vector3i(var4, var5, var6));
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeBoolean(((DockingRequest)this.get()).dock);
      var1.writeUTF(((DockingRequest)this.get()).id);
      var1.writeInt(((DockingRequest)this.get()).pos.x);
      var1.writeInt(((DockingRequest)this.get()).pos.y);
      var1.writeInt(((DockingRequest)this.get()).pos.z);
      return this.byteLength();
   }
}
