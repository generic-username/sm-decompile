package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.network.objects.TradePrices;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteTradePrice extends RemoteField {
   public RemoteTradePrice(TradePrices var1, boolean var2) {
      super(var1, var2);

      assert !var2 || var1.entDbId != Long.MIN_VALUE;

   }

   public RemoteTradePrice(TradePrices var1, NetworkObject var2) {
      super(var1, var2);

      assert !var2.isOnServer() || var1.entDbId != Long.MIN_VALUE;

   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((TradePrices)this.get()).deserialize(var1, var2, this.onServer);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      ((TradePrices)this.get()).serialize(var1, this.onServer);
      return 1;
   }
}
