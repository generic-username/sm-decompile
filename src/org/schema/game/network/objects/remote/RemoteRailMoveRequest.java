package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.network.objects.RailMove;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteRailMoveRequest extends RemoteField {
   public RemoteRailMoveRequest(RailMove var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteRailMoveRequest(RailMove var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((RailMove)this.get()).deserialize(var1, var2, this.onServer);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      ((RailMove)this.get()).serialize(var1, this.onServer);
      return 1;
   }
}
