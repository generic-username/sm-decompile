package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.vecmath.Vector3f;
import org.schema.game.common.data.player.inventory.FreeItem;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteItem extends RemoteField {
   private boolean add;

   public RemoteItem(FreeItem var1, Boolean var2, boolean var3) {
      super(var1, var3);
      this.setAdd(var2);
   }

   public RemoteItem(FreeItem var1, Boolean var2, NetworkObject var3) {
      super(var1, var3);
      this.setAdd(var2);
   }

   public int byteLength() {
      return 1;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      var2 = var1.readInt();
      this.setAdd(var1.readBoolean());
      if (this.isAdd()) {
         short var3 = var1.readShort();
         int var4 = var1.readInt();
         float var5 = var1.readFloat();
         float var6 = var1.readFloat();
         float var7 = var1.readFloat();
         int var8 = var1.readInt();
         ((FreeItem)this.get()).set(var2, var3, var4, var8, new Vector3f(var5, var6, var7));
      } else {
         ((FreeItem)this.get()).set(var2, (short)-1, 0, -1, (Vector3f)null);
      }
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeInt(((FreeItem)this.get()).getId());
      var1.writeBoolean(this.isAdd());
      int var2 = 5;
      if (this.isAdd()) {
         var1.writeShort(((FreeItem)this.get()).getType());
         var1.writeInt(((FreeItem)this.get()).getCount());
         var1.writeFloat(((FreeItem)this.get()).getPos().x);
         var1.writeFloat(((FreeItem)this.get()).getPos().y);
         var1.writeFloat(((FreeItem)this.get()).getPos().z);
         var1.writeInt(((FreeItem)this.get()).getMetaId());
         var2 += 22;
      }

      return var2;
   }

   public boolean isAdd() {
      return this.add;
   }

   public void setAdd(boolean var1) {
      this.add = var1;
   }
}
