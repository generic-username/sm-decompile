package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.data.player.faction.FactionInvite;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteFactionInvitation extends RemoteField {
   public RemoteFactionInvitation(FactionInvite var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteFactionInvitation(FactionInvite var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      String var7 = var1.readUTF();
      String var3 = var1.readUTF();
      int var4 = var1.readInt();
      long var5 = var1.readLong();
      ((FactionInvite)this.get()).set(var7, var3, var4, var5);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeUTF(((FactionInvite)this.get()).getFromPlayerName());
      var1.writeUTF(((FactionInvite)this.get()).getToPlayerName());
      var1.writeInt(((FactionInvite)this.get()).getFactionUID());
      var1.writeLong(((FactionInvite)this.get()).getDate());
      return this.byteLength();
   }
}
