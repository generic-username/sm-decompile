package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.vecmath.Vector3f;
import org.schema.game.common.controller.ParticleEntry;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteParticleEntry extends RemoteField {
   public RemoteParticleEntry(ParticleEntry var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteParticleEntry(ParticleEntry var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 1;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((ParticleEntry)this.get()).setParticleName(var1.readUTF());
      ((ParticleEntry)this.get()).setOrigin(new Vector3f(var1.readFloat(), var1.readFloat(), var1.readFloat()));
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeUTF(((ParticleEntry)this.get()).getParticleName());
      var1.writeFloat(((ParticleEntry)this.get()).getOrigin().x);
      var1.writeFloat(((ParticleEntry)this.get()).getOrigin().y);
      var1.writeFloat(((ParticleEntry)this.get()).getOrigin().z);
      return this.byteLength();
   }
}
