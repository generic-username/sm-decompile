package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteBuffer;

public class RemoteFactionBuffer extends RemoteBuffer {
   private StateInterface state;

   public RemoteFactionBuffer(NetworkObject var1, StateInterface var2) {
      super(RemoteFaction.class, var1);
      this.state = var2;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      int var3 = var1.readInt();

      for(int var4 = 0; var4 < var3; ++var4) {
         int var5;
         Object var7;
         if (FactionManager.isNPCFaction(var5 = var1.readInt())) {
            var7 = new NPCFaction(this.state, var5);
         } else {
            var7 = new Faction(this.state);
         }

         RemoteFaction var6;
         (var6 = new RemoteFaction((Faction)var7, this.onServer)).fromByteStream(var1, var2);
         ((Faction)var7).initialize();
         this.getReceiveBuffer().add(var6);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeInt(((ObjectArrayList)this.get()).size());
      Iterator var2 = ((ObjectArrayList)this.get()).iterator();

      while(var2.hasNext()) {
         RemoteFaction var3 = (RemoteFaction)var2.next();
         var1.writeInt(((Faction)var3.get()).getIdFaction());
         var3.toByteStream(var1);
      }

      ((ObjectArrayList)this.get()).clear();
      return 1;
   }

   protected void cacheConstructor() {
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }
}
