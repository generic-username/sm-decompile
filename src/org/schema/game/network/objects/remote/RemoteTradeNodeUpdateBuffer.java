package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.trade.TradeNode;
import org.schema.game.common.controller.trade.TradeNodeClient;
import org.schema.game.common.controller.trade.TradeNodeStub;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteBuffer;

public class RemoteTradeNodeUpdateBuffer extends RemoteBuffer {
   private StateInterface state;

   public RemoteTradeNodeUpdateBuffer(NetworkObject var1, StateInterface var2) {
      super(RemoteTradeNode.class, var1);
      this.state = var2;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      int var3 = var1.readInt();

      for(int var4 = 0; var4 < var3; ++var4) {
         boolean var5 = var1.readBoolean();
         Object var6 = !this.onServer ? new TradeNodeClient((GameClientState)this.state) : (var5 ? new TradeNode() : new TradeNodeStub());
         RemoteTradeNode var7;
         (var7 = new RemoteTradeNode((TradeNodeStub)var6, this.onServer)).fromByteStream(var1, var2);
         this.getReceiveBuffer().add(var7);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      byte var2 = 0;
      synchronized((ObjectArrayList)this.get()) {
         var1.writeInt(((ObjectArrayList)this.get()).size());
         int var7 = var2 + 4;

         RemoteTradeNode var5;
         for(Iterator var4 = ((ObjectArrayList)this.get()).iterator(); var4.hasNext(); var7 += var5.toByteStream(var1)) {
            var5 = (RemoteTradeNode)var4.next();
            var1.writeBoolean(var5.get() instanceof TradeNode);
         }

         ((ObjectArrayList)this.get()).clear();
         return var7;
      }
   }

   protected void cacheConstructor() {
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }
}
