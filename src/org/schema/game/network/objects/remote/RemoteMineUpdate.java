package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.mines.updates.MineUpdate;
import org.schema.schine.network.client.ClientProcessor;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteMineUpdate extends RemoteField {
   public RemoteMineUpdate(MineUpdate var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteMineUpdate(MineUpdate var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      this.set(MineUpdate.deserializeStatic(var1, var2, this.onServer));
      ((MineUpdate)this.get()).timeStampServerSent = ((ClientProcessor)GameClientState.instance.getProcessor()).getServerPacketSentTimestamp();

      assert ((MineUpdate)this.get()).timeStampServerSent > 0L;

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      ((MineUpdate)this.get()).serialize(var1, this.onServer);
      return 1;
   }
}
