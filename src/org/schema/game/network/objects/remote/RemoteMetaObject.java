package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteMetaObject extends RemoteField {
   private final MetaObjectManager man;

   public RemoteMetaObject(MetaObject var1, MetaObjectManager var2, boolean var3) {
      super(var1, var3);
      this.man = var2;
   }

   public RemoteMetaObject(MetaObject var1, MetaObjectManager var2, NetworkObject var3) {
      super(var1, var3);
      this.man = var2;
   }

   public int byteLength() {
      return 1;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      this.man.deserialize(var1);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      MetaObjectManager.serialize(var1, (MetaObject)this.get());
      return this.byteLength();
   }
}
