package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.data.player.faction.FactionRoles;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteFactionRoles extends RemoteField {
   public RemoteFactionRoles(FactionRoles var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteFactionRoles(FactionRoles var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((FactionRoles)this.get()).factionId = var1.readInt();
      ((FactionRoles)this.get()).senderId = var2;

      for(var2 = 0; var2 < 5; ++var2) {
         ((FactionRoles)this.get()).getRoles()[var2].role = var1.readLong();
         ((FactionRoles)this.get()).getRoles()[var2].name = var1.readUTF();
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeInt(((FactionRoles)this.get()).factionId);

      for(int var2 = 0; var2 < 5; ++var2) {
         var1.writeLong(((FactionRoles)this.get()).getRoles()[var2].role);
         var1.writeUTF(((FactionRoles)this.get()).getRoles()[var2].name);
      }

      return this.byteLength();
   }
}
