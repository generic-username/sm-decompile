package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteBuffer;

public class RemoteSegmentRemoteObjBuffer extends RemoteBuffer {
   private final SegmentController attachedController;

   public RemoteSegmentRemoteObjBuffer(NetworkObject var1, SegmentController var2) {
      super(RemoteSegmentRemoteObj.class, var1);
      this.attachedController = var2;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      int var3 = var1.readInt();

      for(int var4 = 0; var4 < var3; ++var4) {
         RemoteSegmentRemoteObj.decode(var1, var2, this.onServer, this.attachedController);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeInt(((ObjectArrayList)this.get()).size());
      Iterator var2 = ((ObjectArrayList)this.get()).iterator();

      while(var2.hasNext()) {
         ((RemoteSegmentRemoteObj)var2.next()).toByteStream(var1);
      }

      ((ObjectArrayList)this.get()).clear();
      return 1;
   }

   public void clearReceiveBuffer() {
      assert this.receiveBuffer.size() == 0;

   }

   public ObjectArrayList getReceiveBuffer() {
      return null;
   }
}
