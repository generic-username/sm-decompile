package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.element.DistMod;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteDistMod extends RemoteField {
   public RemoteDistMod(DistMod var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteDistMod(DistMod var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((DistMod)this.get()).controllerPos = new Vector3i(var1.readInt(), var1.readInt(), var1.readInt());
      ((DistMod)this.get()).idPos = var1.readLong();
      ((DistMod)this.get()).effectId = var1.readByte();
      ((DistMod)this.get()).dist = var1.readInt();
      ((DistMod)this.get()).onServer = var1.readBoolean();
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeInt(((DistMod)this.get()).controllerPos.x);
      var1.writeInt(((DistMod)this.get()).controllerPos.y);
      var1.writeInt(((DistMod)this.get()).controllerPos.z);
      var1.writeLong(((DistMod)this.get()).idPos);
      var1.writeByte(((DistMod)this.get()).effectId);
      var1.writeInt(((DistMod)this.get()).dist);
      var1.writeBoolean(((DistMod)this.get()).onServer);
      return 1;
   }
}
