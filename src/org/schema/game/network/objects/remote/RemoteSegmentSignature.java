package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.SegmentSignature;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteSegmentSignature extends RemoteField {
   static int len = 24;

   public RemoteSegmentSignature(boolean var1) {
      super(new SegmentSignature(), var1);
   }

   public RemoteSegmentSignature(NetworkObject var1) {
      super(new SegmentSignature(), var1);
   }

   public RemoteSegmentSignature(SegmentSignature var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteSegmentSignature(SegmentSignature var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return len;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      long var3 = var1.readLong();
      boolean var9 = false;
      if (var3 < 0L) {
         var3 = Math.abs(var3 - 1L);
         var9 = true;
      }

      short var5 = var1.readShort();
      short var6 = var1.readShort();
      short var7 = var1.readShort();
      short var8 = -1;
      if (!var9) {
         var8 = var1.readShort();
      }

      assert var3 >= 0L;

      this.set(new SegmentSignature(new Vector3i(var5, var6, var7), var3, var9, var8));
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      assert ((SegmentSignature)this.get()).getLastChanged() >= 0L;

      var1.writeLong(((SegmentSignature)this.get()).empty ? -(((SegmentSignature)this.get()).getLastChanged() + 1L) : ((SegmentSignature)this.get()).getLastChanged());
      var1.writeShort(((SegmentSignature)this.get()).getPos().x);
      var1.writeShort(((SegmentSignature)this.get()).getPos().y);
      var1.writeShort(((SegmentSignature)this.get()).getPos().z);
      if (!((SegmentSignature)this.get()).empty) {
         var1.writeShort(((SegmentSignature)this.get()).getSize());
      }

      return 1;
   }
}
