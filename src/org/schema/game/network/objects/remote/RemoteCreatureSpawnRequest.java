package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.server.data.CreatureSpawn;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteCreatureSpawnRequest extends RemoteField {
   public RemoteCreatureSpawnRequest(CreatureSpawn var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteCreatureSpawnRequest(CreatureSpawn var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 1;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      this.set(new CreatureSpawn());
      ((CreatureSpawn)this.get()).deserialize(var1);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      ((CreatureSpawn)this.get()).serialize(var1);
      return this.byteLength();
   }
}
