package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.VoidSegmentPiece;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteBuffer;

public class RemoteSegmentPieceBuffer extends RemoteBuffer {
   private static final int BYTE = 0;
   private static final int SHORT = 1;
   private static final int INT = 2;
   private final int queueSize;
   private int UPDATE_TYPE = 0;
   private static ThreadLocal poolGen = new ThreadLocal() {
      public final RemoteSegmentPieceBuffer.RemoteSegmentPiecePool initialValue() {
         return new RemoteSegmentPieceBuffer.RemoteSegmentPiecePool();
      }
   };
   private final RemoteSegmentPieceBuffer.RemoteSegmentPiecePool pool;

   public RemoteSegmentPieceBuffer(boolean var1, int var2) {
      super(RemoteSegmentPiece.class, var1);
      this.pool = (RemoteSegmentPieceBuffer.RemoteSegmentPiecePool)poolGen.get();
      this.queueSize = var2;
      if (var2 <= 0) {
         throw new IllegalArgumentException("QUEUESIZE INVALID: " + var2);
      } else if (var2 > 32767) {
         this.UPDATE_TYPE = 2;
      } else if (var2 > 127) {
         this.UPDATE_TYPE = 1;
      } else {
         this.UPDATE_TYPE = 0;
      }
   }

   public RemoteSegmentPieceBuffer(NetworkObject var1, int var2) {
      super(RemoteSegmentPiece.class, var1);
      this.pool = (RemoteSegmentPieceBuffer.RemoteSegmentPiecePool)poolGen.get();
      this.queueSize = var2;
      if (var2 <= 0) {
         throw new IllegalArgumentException("QUEUESIZE INVALID: " + var2);
      } else if (var2 > 32767) {
         this.UPDATE_TYPE = 2;
      } else if (var2 > 127) {
         this.UPDATE_TYPE = 1;
      } else {
         this.UPDATE_TYPE = 0;
      }
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      int var3;
      switch(this.UPDATE_TYPE) {
      case 0:
         var3 = var1.readByte();
         break;
      case 1:
         var3 = var1.readShort();
         break;
      case 2:
      default:
         var3 = var1.readInt();
      }

      for(int var4 = 0; var4 < var3; ++var4) {
         RemoteSegmentPiece var5;
         RemoteSegmentPiece var10000 = var5 = this.pool.get(this.onServer);
         var10000.fromByteStream((VoidSegmentPiece)var10000.get(), var1, var2);
         var5.senderId = var2;
         this.getReceiveBuffer().add(var5);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      int var2 = 4;
      int var3 = Math.min(this.queueSize, ((ObjectArrayList)this.get()).size());
      switch(this.UPDATE_TYPE) {
      case 0:
         var1.writeByte(var3);
         break;
      case 1:
         var1.writeShort((short)var3);
         break;
      case 2:
      default:
         var1.writeInt(var3);
      }

      for(int var4 = 0; var4 < var3; ++var4) {
         RemoteSegmentPiece var5;
         (var5 = (RemoteSegmentPiece)((ObjectArrayList)this.get()).remove(0)).setChanged(false);
         var2 += var5.toByteStream(var1);
      }

      this.keepChanged = !((ObjectArrayList)this.get()).isEmpty();
      return var2;
   }

   protected void cacheConstructor() {
   }

   public void clearReceiveBuffer() {
      ObjectArrayList var1;
      int var2 = (var1 = this.getReceiveBuffer()).size();

      for(int var3 = 0; var3 < var2; ++var3) {
         this.pool.free((RemoteSegmentPiece)var1.get(var3));
      }

      var1.clear();
   }

   public static class RemoteSegmentPiecePool {
      private final List pool = new ObjectArrayList();

      public void free(RemoteSegmentPiece var1) {
         ((SegmentPiece)var1.get()).reset();
         this.pool.add(var1);
      }

      public RemoteSegmentPiece get(boolean var1) {
         return this.pool.isEmpty() ? new RemoteSegmentPiece(new VoidSegmentPiece(), var1) : (RemoteSegmentPiece)this.pool.remove(this.pool.size() - 1);
      }
   }
}
