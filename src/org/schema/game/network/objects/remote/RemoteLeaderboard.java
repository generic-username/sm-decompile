package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.data.gamemode.battle.BattleMode;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteLeaderboard extends RemoteField {
   public RemoteLeaderboard(Object2ObjectOpenHashMap var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteLeaderboard(Object2ObjectOpenHashMap var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      this.set(new Object2ObjectOpenHashMap());
      BattleMode.deserializeLeaderboard(var1, (Object2ObjectOpenHashMap)this.get());
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      BattleMode.serializeLeaderboard(var1, (Object2ObjectOpenHashMap)this.get());
      return 1;
   }
}
