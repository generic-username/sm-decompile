package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.shorts.Short2IntOpenHashMap;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map.Entry;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteBlockCount extends RemoteField {
   public RemoteBlockCount(Short2IntOpenHashMap var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteBlockCount(Short2IntOpenHashMap var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      var2 = var1.readInt();

      for(int var3 = 0; var3 < var2; ++var3) {
         short var4 = var1.readShort();
         int var5 = var1.readInt();
         ((Short2IntOpenHashMap)this.get()).put(var4, var5);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeInt(((Short2IntOpenHashMap)this.get()).size());
      Iterator var2 = ((Short2IntOpenHashMap)this.get()).entrySet().iterator();

      while(var2.hasNext()) {
         Entry var3 = (Entry)var2.next();
         var1.writeShort((Short)var3.getKey());
         var1.writeInt((Integer)var3.getValue());
      }

      return this.byteLength();
   }
}
