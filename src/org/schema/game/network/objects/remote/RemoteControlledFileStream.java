package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.util.FileStreamSegment;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteControlledFileStream extends RemoteField {
   public RemoteControlledFileStream(boolean var1, int var2) {
      super(new FileStreamSegment(var2), var1);
   }

   public RemoteControlledFileStream(FileStreamSegment var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteControlledFileStream(FileStreamSegment var1, NetworkObject var2) {
      super(var1, var2);
   }

   public RemoteControlledFileStream(NetworkObject var1, int var2) {
      super(new FileStreamSegment(var2), var1);
   }

   public int byteLength() {
      return ((FileStreamSegment)this.get()).length;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((FileStreamSegment)this.get()).last = var1.readBoolean();
      ((FileStreamSegment)this.get()).length = var1.readShort();
      var1.readFully(((FileStreamSegment)this.get()).buffer, 0, ((FileStreamSegment)this.get()).length);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeBoolean(((FileStreamSegment)this.get()).last);
      var1.writeShort(((FileStreamSegment)this.get()).length);
      var1.write(((FileStreamSegment)this.get()).buffer, 0, ((FileStreamSegment)this.get()).length);
      return this.byteLength();
   }
}
