package org.schema.game.network.objects.remote;

import com.googlecode.javaewah.EWAHCompressedBitmap;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.network.objects.BitsetResponse;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteBitset extends RemoteField {
   public RemoteBitset(BitsetResponse var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteBitset(BitsetResponse var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((BitsetResponse)this.get()).segmentBufferIndex = var1.readLong();
      ((BitsetResponse)this.get()).pos = new Vector3i();
      ((BitsetResponse)this.get()).pos.set(var1.readInt(), var1.readInt(), var1.readInt());
      ((BitsetResponse)this.get()).data = var1.readBoolean();
      if (((BitsetResponse)this.get()).data) {
         ((BitsetResponse)this.get()).bitmap = new EWAHCompressedBitmap();
         ((BitsetResponse)this.get()).bitmap.deserialize(var1);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeLong(((BitsetResponse)this.get()).segmentBufferIndex);
      var1.writeInt(((BitsetResponse)this.get()).pos.x);
      var1.writeInt(((BitsetResponse)this.get()).pos.y);
      var1.writeInt(((BitsetResponse)this.get()).pos.z);
      if (((BitsetResponse)this.get()).data) {
         var1.writeBoolean(true);
         ((BitsetResponse)this.get()).bitmap.serialize(var1);
      } else {
         var1.writeBoolean(false);
      }

      return this.byteLength();
   }
}
