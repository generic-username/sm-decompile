package org.schema.game.network.objects.remote;

import org.schema.game.common.controller.elements.shipyard.ShipyardCollectionManager;

public class ShipyardCommand extends SimpleCommand {
   public int factionId;

   public ShipyardCommand(int var1, ShipyardCollectionManager.ShipyardCommandType var2, Object... var3) {
      super(var2, var3);
      this.factionId = var1;
   }

   public ShipyardCommand() {
   }

   protected void checkMatches(ShipyardCollectionManager.ShipyardCommandType var1, Object[] var2) {
      var1.checkMatches(var2);
   }
}
