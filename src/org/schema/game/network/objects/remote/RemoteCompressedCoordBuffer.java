package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.bytes.ByteArrayList;
import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import it.unimi.dsi.fastutil.shorts.ShortCollection;
import it.unimi.dsi.fastutil.shorts.ShortIterator;
import it.unimi.dsi.fastutil.shorts.ShortList;
import it.unimi.dsi.fastutil.shorts.ShortListIterator;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import org.schema.common.util.ByteUtil;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.RemoteBufferInterface;
import org.schema.schine.network.objects.remote.NetworkChangeObserver;
import org.schema.schine.network.objects.remote.RemoteField;
import org.schema.schine.network.objects.remote.Streamable;

public class RemoteCompressedCoordBuffer extends RemoteField implements ShortList, RemoteBufferInterface {
   private final ShortArrayList receiveBuffer = new ShortArrayList();
   private static final Long2ObjectOpenHashMap ordMap = new Long2ObjectOpenHashMap();
   public int MAX_BATCH = 16;
   private static ObjectArrayFIFOQueue l = new ObjectArrayFIFOQueue();

   public RemoteCompressedCoordBuffer(boolean var1, int var2) {
      super(new ShortArrayList(), var1);
      this.MAX_BATCH = var2;
   }

   public RemoteCompressedCoordBuffer(NetworkObject var1, int var2) {
      super(new ShortArrayList(), var1);
      this.MAX_BATCH = var2;
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      var2 = var1.readInt();

      for(int var3 = 0; var3 < var2; ++var3) {
         int var4 = var1.readShort() << 8;
         int var5 = var1.readShort() << 8;
         int var6 = var1.readShort() << 8;
         short var7 = var1.readShort();

         for(int var8 = 0; var8 < var7; var8 += 3) {
            int var9 = var1.readByte() & 255;
            short var10 = (short)(var4 + var9);
            this.receiveBuffer.add(var10);
            var9 = var1.readByte() & 255;
            var10 = (short)(var5 + var9);
            this.receiveBuffer.add(var10);
            var9 = var1.readByte() & 255;
            var10 = (short)(var6 + var9);
            this.receiveBuffer.add(var10);
         }
      }

   }

   public static void main(String[] var0) {
      ShortArrayList var7;
      (var7 = new ShortArrayList()).add((short)-15);
      var7.add((short)-15);
      var7.add((short)-15);
      var7.add((short)0);
      var7.add((short)0);
      var7.add((short)0);
      var7.add((short)55);
      var7.add((short)55);
      var7.add((short)55);
      var7.add((short)256);
      var7.add((short)256);
      var7.add((short)256);
      var7.add((short)-256);
      var7.add((short)-256);
      var7.add((short)-256);
      var7.add((short)-1231);
      var7.add((short)1231);
      var7.add((short)-1231);
      RemoteCompressedCoordBuffer var1;
      (var1 = new RemoteCompressedCoordBuffer(true, 128)).observer = new NetworkChangeObserver() {
         public final void update(Streamable var1) {
         }

         public final boolean isSynched() {
            return false;
         }
      };
      var1.addAll((ShortList)var7);
      FastByteArrayOutputStream var2 = new FastByteArrayOutputStream(new byte[1000]);

      try {
         var1.toByteStream(new DataOutputStream(var2));
         FastByteArrayInputStream var8 = new FastByteArrayInputStream(var2.array, 0, var2.length);
         var1.fromByteStream(new DataInputStream(var8), 0);

         for(int var9 = 0; var9 < var1.getReceiveBuffer().size(); var9 += 3) {
            short var3 = var1.getReceiveBuffer().get(var9);
            short var4 = var1.getReceiveBuffer().get(var9 + 1);
            short var5 = var1.getReceiveBuffer().get(var9 + 2);
            System.err.println("IN: " + var7.getShort(var9) + ", " + var7.getShort(var9 + 1) + ", " + var7.getShort(var9 + 2) + "; OUT: " + var3 + ", " + var4 + ", " + var5 + ";");
         }

      } catch (IOException var6) {
         var6.printStackTrace();
      }
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      int var2 = Math.min(this.MAX_BATCH, ((ShortArrayList)this.get()).size());
      synchronized(ordMap) {
         short var8;
         for(int var4 = 0; var4 < var2; var4 += 3) {
            short var5 = ((ShortArrayList)this.get()).removeShort(0);
            short var6 = ((ShortArrayList)this.get()).removeShort(0);
            short var7 = ((ShortArrayList)this.get()).removeShort(0);
            var8 = (short)ByteUtil.divU256(var5);
            short var9 = (short)ByteUtil.divU256(var6);
            short var10 = (short)ByteUtil.divU256(var7);
            long var11 = ElementCollection.getIndex(var8, var9, var10);
            ByteArrayList var17;
            if ((var17 = (ByteArrayList)ordMap.get(var11)) == null) {
               var17 = getByteList();
               ordMap.put(var11, var17);
            }

            var17.add((byte)ByteUtil.modU256(var5));
            var17.add((byte)ByteUtil.modU256(var6));
            var17.add((byte)ByteUtil.modU256(var7));
         }

         var1.writeInt(ordMap.size());
         Iterator var14 = ordMap.entrySet().iterator();

         while(true) {
            if (!var14.hasNext()) {
               ordMap.clear();
               break;
            }

            Entry var15;
            long var16 = (Long)(var15 = (Entry)var14.next()).getKey();
            var1.writeShort(ElementCollection.getPosX(var16));
            var1.writeShort(ElementCollection.getPosY(var16));
            var1.writeShort(ElementCollection.getPosZ(var16));
            var8 = (short)((ByteArrayList)var15.getValue()).size();
            var1.writeShort(var8);

            for(int var18 = 0; var18 < var8; ++var18) {
               byte var19 = ((ByteArrayList)var15.getValue()).getByte(var18);
               var1.writeByte(var19);
            }

            ((ByteArrayList)var15.getValue()).clear();
            freeByteList((ByteArrayList)var15.getValue());
         }
      }

      this.keepChanged = !((ShortArrayList)this.get()).isEmpty();
      return 4;
   }

   private static ByteArrayList getByteList() {
      synchronized(l) {
         return l.isEmpty() ? new ByteArrayList() : (ByteArrayList)l.dequeue();
      }
   }

   private static void freeByteList(ByteArrayList var0) {
      synchronized(l) {
         l.enqueue(var0);
      }
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }

   public ShortArrayList getReceiveBuffer() {
      return this.receiveBuffer;
   }

   public ShortListIterator iterator() {
      return ((ShortArrayList)this.get()).iterator();
   }

   public ShortListIterator shortListIterator() {
      return ((ShortArrayList)this.get()).shortListIterator();
   }

   public ShortListIterator shortListIterator(int var1) {
      return ((ShortArrayList)this.get()).shortListIterator(var1);
   }

   public ShortListIterator listIterator() {
      return ((ShortArrayList)this.get()).listIterator();
   }

   public ShortListIterator listIterator(int var1) {
      return ((ShortArrayList)this.get()).listIterator(var1);
   }

   public ShortList shortSubList(int var1, int var2) {
      return ((ShortArrayList)this.get()).shortSubList(var1, var2);
   }

   public ShortList subList(int var1, int var2) {
      return ((ShortArrayList)this.get()).subList(var1, var2);
   }

   public void size(int var1) {
      ((ShortArrayList)this.get()).size();
   }

   public void getElements(int var1, short[] var2, int var3, int var4) {
      ((ShortArrayList)this.get()).getElements(var1, var2, var3, var4);
   }

   public void removeElements(int var1, int var2) {
      ((ShortArrayList)this.get()).removeElements(var1, var2);
   }

   public void addElements(int var1, short[] var2) {
      this.setChanged(true);
      this.observer.update(this);
      ((ShortArrayList)this.get()).addElements(var1, var2);
   }

   public void addElements(int var1, short[] var2, int var3, int var4) {
      this.setChanged(true);
      this.observer.update(this);
      ((ShortArrayList)this.get()).addElements(var1, var2, var3, var4);
   }

   public boolean addCoord(short var1, short var2, short var3) {
      boolean var4 = ((ShortArrayList)this.get()).add(var1) | ((ShortArrayList)this.get()).add(var2) | ((ShortArrayList)this.get()).add(var3);
      this.setChanged(var4);
      this.observer.update(this);
      return var4;
   }

   public boolean add(short var1) {
      boolean var2 = ((ShortArrayList)this.get()).add(var1);
      this.setChanged(var2);
      this.observer.update(this);
      return var2;
   }

   public void add(int var1, short var2) {
      ((ShortArrayList)this.get()).add(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
   }

   public boolean addAll(int var1, ShortCollection var2) {
      boolean var3;
      if (var3 = ((ShortArrayList)this.get()).addAll(var1, var2)) {
         this.setChanged(var3);
         this.observer.update(this);
      }

      return var3;
   }

   public boolean addAll(int var1, ShortList var2) {
      this.setChanged(true);
      this.observer.update(this);
      return ((ShortArrayList)this.get()).addAll(var1, var2);
   }

   public boolean addAll(ShortList var1) {
      this.setChanged(true);
      this.observer.update(this);
      return ((ShortArrayList)this.get()).addAll(var1);
   }

   public short getShort(int var1) {
      return ((ShortArrayList)this.get()).getShort(var1);
   }

   public int indexOf(short var1) {
      return ((ShortArrayList)this.get()).indexOf(var1);
   }

   public int lastIndexOf(short var1) {
      return ((ShortArrayList)this.get()).lastIndexOf(var1);
   }

   public short removeShort(int var1) {
      return ((ShortArrayList)this.get()).removeShort(var1);
   }

   public short set(int var1, short var2) {
      short var3 = ((ShortArrayList)this.get()).set(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
      return var3;
   }

   public int size() {
      return ((ShortArrayList)this.get()).size();
   }

   public boolean isEmpty() {
      return ((ShortArrayList)this.get()).isEmpty();
   }

   public boolean contains(Object var1) {
      return ((ShortArrayList)this.get()).contains(var1);
   }

   public Object[] toArray() {
      return ((ShortArrayList)this.get()).toArray();
   }

   public Object[] toArray(Object[] var1) {
      return ((ShortArrayList)this.get()).toArray(var1);
   }

   public boolean add(Short var1) {
      boolean var2 = ((ShortArrayList)this.get()).add(var1);
      this.setChanged(var2);
      this.observer.update(this);
      return var2;
   }

   public boolean remove(Object var1) {
      return ((ShortArrayList)this.get()).remove(var1);
   }

   public boolean containsAll(Collection var1) {
      return ((ShortArrayList)this.get()).containsAll(var1);
   }

   public boolean addAll(Collection var1) {
      boolean var2;
      if (var2 = ((ShortArrayList)this.get()).addAll(var1)) {
         this.setChanged(var2);
         this.observer.update(this);
      }

      return var2;
   }

   public boolean addAll(int var1, Collection var2) {
      boolean var3;
      if (var3 = ((ShortArrayList)this.get()).addAll(var1, var2)) {
         this.setChanged(var3);
         this.observer.update(this);
      }

      return var3;
   }

   public boolean removeAll(Collection var1) {
      return ((ShortArrayList)this.get()).removeAll(var1);
   }

   public boolean retainAll(Collection var1) {
      return ((ShortArrayList)this.get()).retainAll(var1);
   }

   public void clear() {
      ((ShortArrayList)this.get()).clear();
   }

   public Short get(int var1) {
      return ((ShortArrayList)this.get()).get(var1);
   }

   public Short set(int var1, Short var2) {
      short var3 = ((ShortArrayList)this.get()).set(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
      return var3;
   }

   public void add(int var1, Short var2) {
      ((ShortArrayList)this.get()).add(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
   }

   public Short remove(int var1) {
      return ((ShortArrayList)this.get()).remove(var1);
   }

   public int indexOf(Object var1) {
      return ((ShortArrayList)this.get()).indexOf(var1);
   }

   public int lastIndexOf(Object var1) {
      return ((ShortArrayList)this.get()).lastIndexOf(var1);
   }

   public String toString() {
      return "(" + this.getClass().toString() + ": HOLD: " + this.get() + "; RECEIVED: " + this.getReceiveBuffer() + ")";
   }

   public int compareTo(List var1) {
      return ((ShortArrayList)this.get()).compareTo(var1);
   }

   public ShortIterator shortIterator() {
      return ((ShortArrayList)this.get()).shortIterator();
   }

   public boolean contains(short var1) {
      return ((ShortArrayList)this.get()).contains(var1);
   }

   public short[] toShortArray() {
      return ((ShortArrayList)this.get()).toShortArray();
   }

   public short[] toShortArray(short[] var1) {
      return ((ShortArrayList)this.get()).toShortArray(var1);
   }

   public short[] toArray(short[] var1) {
      return ((ShortArrayList)this.get()).toArray(var1);
   }

   public boolean rem(short var1) {
      return ((ShortArrayList)this.get()).rem(var1);
   }

   public boolean addAll(ShortCollection var1) {
      boolean var2;
      if (var2 = ((ShortArrayList)this.get()).addAll(var1)) {
         this.setChanged(var2);
         this.observer.update(this);
      }

      return var2;
   }

   public boolean containsAll(ShortCollection var1) {
      return ((ShortArrayList)this.get()).containsAll(var1);
   }

   public boolean removeAll(ShortCollection var1) {
      return ((ShortArrayList)this.get()).removeAll(var1);
   }

   public boolean retainAll(ShortCollection var1) {
      return ((ShortArrayList)this.get()).retainAll(var1);
   }
}
