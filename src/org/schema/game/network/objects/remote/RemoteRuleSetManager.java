package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.controller.rules.RuleSetManager;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteRuleSetManager extends RemoteField {
   public RemoteRuleSetManager(RuleSetManager var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteRuleSetManager(RuleSetManager var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((RuleSetManager)this.get()).deserialize(var1, var2, this.onServer);
      System.err.println("[RULE][NT] rulesetmanager received on " + (this.onServer ? "SERVER" : "CLIENT") + " from sender id " + var2);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      ((RuleSetManager)this.get()).serialize(var1, this.onServer);
      return 1;
   }
}
