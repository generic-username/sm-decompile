package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteTextBlockPair extends RemoteField {
   public RemoteTextBlockPair(TextBlockPair var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteTextBlockPair(TextBlockPair var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((TextBlockPair)this.get()).text = var1.readUTF();
      ((TextBlockPair)this.get()).block = var1.readLong();
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeUTF(((TextBlockPair)this.get()).text);
      var1.writeLong(((TextBlockPair)this.get()).block);
      return 1;
   }
}
