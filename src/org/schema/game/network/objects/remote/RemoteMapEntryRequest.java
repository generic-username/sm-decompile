package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.gamemap.requests.GameMapRequest;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteMapEntryRequest extends RemoteField {
   public RemoteMapEntryRequest(GameMapRequest var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteMapEntryRequest(GameMapRequest var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 1;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((GameMapRequest)this.get()).pos = new Vector3i(var1.readInt(), var1.readInt(), var1.readInt());
      ((GameMapRequest)this.get()).type = var1.readByte();
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeInt(((GameMapRequest)this.get()).pos.x);
      var1.writeInt(((GameMapRequest)this.get()).pos.y);
      var1.writeInt(((GameMapRequest)this.get()).pos.z);
      var1.writeByte(((GameMapRequest)this.get()).type);
      return this.byteLength();
   }
}
