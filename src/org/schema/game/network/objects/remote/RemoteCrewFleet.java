package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.data.player.CrewFleetRequest;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteCrewFleet extends RemoteField {
   public RemoteCrewFleet(CrewFleetRequest var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteCrewFleet(CrewFleetRequest var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((CrewFleetRequest)this.get()).mode = var1.readByte();
      ((CrewFleetRequest)this.get()).type = var1.readByte();
      ((CrewFleetRequest)this.get()).ai = var1.readUTF();
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeByte(((CrewFleetRequest)this.get()).mode);
      var1.writeByte(((CrewFleetRequest)this.get()).type);
      var1.writeUTF(((CrewFleetRequest)this.get()).ai);
      return 1;
   }
}
