package org.schema.game.network.objects;

import org.schema.game.network.objects.remote.RemoteReactorBonusUpdateBuffer;
import org.schema.game.network.objects.remote.RemoteReactorPriorityQueueBuffer;
import org.schema.game.network.objects.remote.RemoteReactorSetBuffer;
import org.schema.game.network.objects.remote.RemoteReactorTreeBuffer;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.RemoteFloatBuffer;
import org.schema.schine.network.objects.remote.RemoteLongBuffer;
import org.schema.schine.network.objects.remote.RemoteLongPrimitive;
import org.schema.schine.network.objects.remote.RemoteShortBuffer;

public interface PowerInterfaceNetworkObject {
   RemoteReactorPriorityQueueBuffer getReactorPrioQueueBuffer();

   RemoteReactorSetBuffer getReactorSetBuffer();

   RemoteReactorTreeBuffer getReactorTreeBuffer();

   RemoteLongBuffer getConvertRequestBuffer();

   RemoteLongBuffer getBootRequestBuffer();

   RemoteFloatBuffer getReactorCooldownBuffer();

   RemoteLongPrimitive getActiveReactor();

   RemoteShortBuffer getRecalibrateRequestBuffer();

   RemoteBuffer getReactorChangeBuffer();

   RemoteFloatBuffer getEnergyStreamCooldownBuffer();

   RemoteReactorBonusUpdateBuffer getReactorBonusMatrixUpdateBuffer();
}
