package org.schema.game.network.objects;

import org.schema.game.common.controller.PlanetIco;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.remote.RemoteLongPrimitive;

public class NetworkPlanetIco extends NetworkSpaceStation {
   public RemoteLongPrimitive seed = new RemoteLongPrimitive(0L, this);

   public NetworkPlanetIco(StateInterface var1, PlanetIco var2) {
      super(var1, var2);
   }
}
