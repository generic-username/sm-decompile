package org.schema.game.network;

import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.server.ServerProcessor;

public class StarMadeServerStats {
   public long totalMemory;
   public long freeMemory;
   public long takenMemory;
   public int totalPackagesQueued;
   public int lastAllocatedSegmentData;
   public int playerCount;
   public long ping;

   public static StarMadeServerStats decode(Object[] var0) {
      StarMadeServerStats var1;
      (var1 = new StarMadeServerStats()).totalMemory = (Long)var0[0];
      var1.freeMemory = (Long)var0[1];
      var1.takenMemory = var1.totalMemory - var1.freeMemory;
      var1.totalPackagesQueued = (Integer)var0[2];
      var1.lastAllocatedSegmentData = (Integer)var0[3];
      var1.playerCount = (Integer)var0[4];
      return var1;
   }

   public static Object[] encode(GameServerState var0) {
      long var1 = Runtime.getRuntime().totalMemory();
      long var3 = Runtime.getRuntime().freeMemory();
      return new Object[]{var1, var3, ServerProcessor.totalPackagesQueued, GameServerState.lastAllocatedSegmentData, var0.getPlayerStatesByName().size()};
   }
}
