package org.schema.game.network.commands;

import java.io.IOException;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.admin.AdminCommands;
import org.schema.schine.network.Command;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.exception.NetworkObjectNotFoundException;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.network.server.ServerStateInterface;

public class AdminCommand extends Command {
   public void clientAnswerProcess(Object[] var1, ClientStateInterface var2, short var3) throws NetworkObjectNotFoundException, IOException, Exception {
   }

   public void serverProcess(ServerProcessor var1, Object[] var2, ServerStateInterface var3, short var4) throws NetworkObjectNotFoundException, IOException, Exception {
      if (!((GameServerState)var3).getController().isAdmin(var1.getClient())) {
         var1.getClient().serverMessage("[ADMIN COMMAND] [ERROR] YOU ARE NOT AN ADMIN!");
      } else {
         RegisteredClientOnServer var8 = var1.getClient();
         Integer var5 = (Integer)var2[0];
         AdminCommands var9 = AdminCommands.values()[var5];
         if (!((GameServerState)var3).getController().allowedToExecuteAdminCommand(var8, var9)) {
            var1.getClient().serverMessage("[ADMIN COMMAND] [ERROR] You are an admin but not allowed to execute this command. Please ask your server admin or super admin!");
         } else {
            Object[] var7 = new Object[var2.length - 1];

            for(int var6 = 0; var6 < var7.length; ++var6) {
               var7[var6] = var2[var6 + 1];
            }

            ((GameServerState)var3).getController().enqueueAdminCommand(var8, var9, var7);
         }
      }
   }
}
