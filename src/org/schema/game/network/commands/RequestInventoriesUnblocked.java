package org.schema.game.network.commands;

import java.io.IOException;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentProvider;
import org.schema.schine.network.Command;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.exception.NetworkObjectNotFoundException;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.network.server.ServerStateInterface;

public class RequestInventoriesUnblocked extends Command {
   public RequestInventoriesUnblocked() {
      this.mode = 0;
   }

   public void clientAnswerProcess(Object[] var1, ClientStateInterface var2, short var3) throws NetworkObjectNotFoundException, IOException {
   }

   public void serverProcess(ServerProcessor var1, Object[] var2, ServerStateInterface var3, short var4) throws Exception {
      int var7 = (Integer)var2[0];

      try {
         RegisteredClientOnServer var10000 = var1.getClient();
         var1 = null;
         SendableSegmentProvider var6;
         if ((SegmentController)(var6 = (SendableSegmentProvider)var10000.getLocalAndRemoteObjectContainer().getLocalObjects().get(var7)).getSegmentController() == null) {
            throw new IllegalArgumentException("[SERVER] Could NOT find the segment controller ID " + var7 + ". This CAN happen, when the SegmentController for this SendableSegmentProvider was deleted and the PRIVATE sendable segment controller was still udpating");
         } else {
            var6.sendServerInventories();
         }
      } catch (Exception var5) {
         var5.printStackTrace();
      }
   }
}
