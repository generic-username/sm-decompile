package org.schema.game.network.commands;

import org.schema.game.network.StarMadeServerStats;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.Command;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.exception.NetworkObjectNotFoundException;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.network.server.ServerStateInterface;

public class RequestServerStats extends Command {
   public RequestServerStats() {
      this.mode = 1;
   }

   public void clientAnswerProcess(Object[] var1, ClientStateInterface var2, short var3) throws NetworkObjectNotFoundException {
      var2.arrivedReturn(var3, var1);
   }

   public void serverProcess(ServerProcessor var1, Object[] var2, ServerStateInterface var3, short var4) throws Exception {
      GameServerState var5;
      if (!(var5 = (GameServerState)var3).isAdmin(var1.getClient().getPlayerName())) {
         var5.getController().sendLogout(var1.getClient().getId(), "Only Admins Allowed");
      }

      this.createReturnToClient(var3, var1, var4, StarMadeServerStats.encode(var5));
   }
}
