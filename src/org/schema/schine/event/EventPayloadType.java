package org.schema.schine.event;

public enum EventPayloadType {
   BYTE,
   SHORT,
   INT,
   LONG,
   FLOAT,
   DOUBLE,
   STRING,
   VECTOR3i,
   VECTOR3f,
   VECTOR4f,
   VECTOR4i,
   ARRAY,
   LIST,
   CUSTOM;
}
