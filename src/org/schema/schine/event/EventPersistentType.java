package org.schema.schine.event;

public enum EventPersistentType {
   NONE,
   SPACIAL,
   SITUATION;
}
