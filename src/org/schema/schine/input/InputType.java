package org.schema.schine.input;

public enum InputType {
   MOUSE,
   KEYBOARD,
   JOYSTICK,
   BLOCK;
}
