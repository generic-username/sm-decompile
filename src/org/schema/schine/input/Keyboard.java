package org.schema.schine.input;

import org.schema.common.ParseException;
import org.schema.schine.graphicsengine.core.GraphicsContext;

public class Keyboard {
   private static boolean created;

   public static boolean isKeyDown(int var0) {
      return org.lwjgl.input.Keyboard.isKeyDown(var0);
   }

   public static String getKeyName(int var0) {
      return org.lwjgl.input.Keyboard.getKeyName(var0);
   }

   public static int getKeyFromName(String var0) throws ParseException {
      int var1;
      if ((var1 = org.lwjgl.input.Keyboard.getKeyIndex(var0)) == 0) {
         throw new ParseException(var0 + " key not known");
      } else {
         return var1;
      }
   }

   public static void enableRepeatEvents(boolean var0) {
      org.lwjgl.input.Keyboard.enableRepeatEvents(var0);
   }

   public static boolean isRepeatEvent() {
      return org.lwjgl.input.Keyboard.isRepeatEvent();
   }

   public static boolean isCreated() {
      if (GraphicsContext.initialized && org.lwjgl.input.Keyboard.isCreated()) {
         created = true;
      }

      return created || GraphicsContext.initialized && org.lwjgl.input.Keyboard.isCreated();
   }

   public static int getNumberPressed() {
      if (isKeyDown(11)) {
         return 0;
      } else if (isKeyDown(2)) {
         return 1;
      } else if (isKeyDown(3)) {
         return 2;
      } else if (isKeyDown(4)) {
         return 3;
      } else if (isKeyDown(5)) {
         return 4;
      } else if (isKeyDown(6)) {
         return 5;
      } else if (isKeyDown(7)) {
         return 6;
      } else if (isKeyDown(8)) {
         return 7;
      } else if (isKeyDown(9)) {
         return 8;
      } else {
         return isKeyDown(10) ? 9 : -1;
      }
   }
}
