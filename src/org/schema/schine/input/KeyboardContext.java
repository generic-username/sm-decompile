package org.schema.schine.input;

import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;

public enum KeyboardContext {
   GENERAL((KeyboardContext)null, 0, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_INPUT_KEYBOARDCONTEXT_0;
      }
   }),
   PLAYER(GENERAL, 1, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_INPUT_KEYBOARDCONTEXT_1;
      }
   }),
   SHIP(GENERAL, 1, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_INPUT_KEYBOARDCONTEXT_2;
      }
   }),
   BUILD(SHIP, 1, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_INPUT_KEYBOARDCONTEXT_3;
      }
   });

   private final Translatable description;
   private final KeyboardContext parent;
   private final int lvl;

   private KeyboardContext(KeyboardContext var3, int var4, Translatable var5) {
      this.parent = var3;
      this.lvl = var4;
      this.description = var5;
   }

   public final String getDesc() {
      return this.description.getName(this);
   }

   public final int getLvl() {
      return this.lvl;
   }

   public final boolean isRoot() {
      return this.parent == null;
   }

   public final KeyboardContext getParent() {
      return this.parent;
   }
}
