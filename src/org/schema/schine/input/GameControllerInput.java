package org.schema.schine.input;

import org.lwjgl.input.Controllers;

public class GameControllerInput {
   private GameController[] joysticks;
   private GameController activeController;
   private boolean init;

   public void init() {
      new Controllers();

      try {
         Controllers.create();
         if (Controllers.getControllerCount() > 0) {
            System.out.println("[CLIENT][GAMEINPUT] TOTAL INPUT DEVICES: " + Controllers.getControllerCount());
            this.joysticks = new GameController[Controllers.getControllerCount()];

            for(int var1 = 0; var1 < Controllers.getControllerCount(); ++var1) {
               this.joysticks[var1] = new GameController(Controllers.getController(var1));
            }
         } else {
            this.joysticks = null;
         }
      } catch (Exception var2) {
         var2.printStackTrace();
         System.err.println("Couldn't initialize Controllers: " + var2.getMessage());
      }

      this.init = true;
   }

   public int getSize() {
      return this.joysticks != null ? this.joysticks.length : 0;
   }

   public String getName(int var1) {
      return this.joysticks[var1].joystick.getName();
   }

   public void delesect() {
      this.setActiveController((GameController)null);
   }

   public void select(int var1) {
      if (this.joysticks != null && var1 >= 0 && var1 < this.joysticks.length) {
         this.setActiveController(this.joysticks[var1]);
      } else {
         System.err.println("[CONTROLLERINPUT] resetcontroller (tried " + var1 + ")");
         this.setActiveController((GameController)null);
      }
   }

   public void poll() {
      if (this.getActiveController() != null) {
         this.getActiveController().joystick.poll();
      }

   }

   public GameController getActiveController() {
      return this.activeController;
   }

   public void setActiveController(GameController var1) {
      this.activeController = var1;
   }

   public boolean initialized() {
      return this.init;
   }
}
