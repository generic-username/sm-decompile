package org.schema.schine.input;

import java.util.List;
import org.schema.schine.common.TextCallback;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;

public interface InputState {
   InputController getController();

   short getNumberOfUpdate();

   List getGeneralChatLog();

   List getVisibleChatLog();

   void onSwitchedSetting(EngineSettings var1);

   String onAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException;

   void flagChangedForObservers(Object var1);

   String getGUIPath();

   GraphicsContext getGraphicsContext();

   void setActiveSubtitles(List var1);

   void setInTextBox(boolean var1);

   boolean isInTextBox();
}
