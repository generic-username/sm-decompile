package org.schema.schine.graphicsengine.animation;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import org.schema.common.FastMath;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Bone;

public final class AnimationChannel {
   private static final float DEFAULT_BLEND_TIME = 0.15F;
   private static final int MAX_BLEND_QUEUE = 1;
   private final ArrayList blendQueue = new ArrayList();
   private AnimationController control;
   private BitSet affectedBones;
   private IntOpenHashSet affectedBonesIndexSet;
   private Animation animation;
   private float time;
   private float speed;
   private LoopMode loopMode;
   private boolean overwritePreviousAnimation;

   AnimationChannel(AnimationController var1) {
      this.control = var1;
   }

   private static float clampWrapTime(float var0, float var1, LoopMode var2) {
      if (var1 == Float.POSITIVE_INFINITY) {
         return var0;
      } else {
         if (var0 < 0.0F) {
            switch(var2) {
            case DONT_LOOP_DEACTIVATE:
               return 0.0F;
            case DONT_LOOP:
               return 0.0F;
            case CYCLE:
               return var0;
            case LOOP:
               return var1 - var0;
            }
         } else if (var0 > var1) {
            switch(var2) {
            case DONT_LOOP_DEACTIVATE:
               return var1;
            case DONT_LOOP:
               return var1;
            case CYCLE:
               return -(var1 * 2.0F - var0);
            case LOOP:
               return var0 - var1;
            }
         }

         return var0;
      }
   }

   public final void addAllBones() {
      this.affectedBones = null;
   }

   public final void addBone(Bone var1) {
      int var2 = var1.boneID;
      if (this.affectedBones == null) {
         this.affectedBones = new BitSet(this.control.getSkeleton().getBoneCount());
         this.affectedBonesIndexSet = new IntOpenHashSet(this.control.getSkeleton().getBoneCount());
      }

      this.affectedBones.set(var2);
      this.affectedBonesIndexSet.add(var2);
   }

   public final void addBone(String var1) {
      this.addBone(this.control.getSkeleton().getBone(var1));
   }

   public final void addFromRootBone(Bone var1) {
      this.addBone(var1);
      if (var1.getChilds() != null) {
         Iterator var3 = var1.getChilds().iterator();

         while(var3.hasNext()) {
            Bone var2 = (Bone)var3.next();
            this.addFromRootBone(var2);
         }

      }
   }

   public final void addFromRootBone(String var1) {
      this.addFromRootBone(this.control.getSkeleton().getBone(var1));
   }

   public final void addToRootBone(Bone var1) {
      this.addBone(var1);

      while(var1.getParent() != null) {
         var1 = var1.getParent();
         this.addBone(var1);
      }

   }

   public final void addToRootBone(String var1) {
      this.addToRootBone(this.control.getSkeleton().getBone(var1));
   }

   public final BitSet getAffectedBones() {
      return this.affectedBones;
   }

   public final IntOpenHashSet getAffectedBonesIndexSet() {
      return this.affectedBonesIndexSet;
   }

   public final String getAnimationName() {
      return this.animation != null ? this.animation.getName() : "no animation";
   }

   public final float getAnimMaxTime() {
      return this.animation != null ? this.animation.getLength() : 0.0F;
   }

   public final AnimationController getControl() {
      return this.control;
   }

   public final LoopMode getLoopMode() {
      return this.loopMode;
   }

   public final void setLoopMode(LoopMode var1) {
      this.loopMode = var1;
   }

   public final float getSpeed() {
      return this.speed;
   }

   public final void setSpeed(float var1) {
      this.speed = var1;
   }

   public final float getTime() {
      return this.time;
   }

   public final void setTime(float var1) {
      this.time = FastMath.clamp(var1, 0.0F, this.getAnimMaxTime());
   }

   public final boolean isActive() {
      return this.animation != null;
   }

   public final boolean isOverwritePreviousAnimation() {
      return this.overwritePreviousAnimation;
   }

   public final void setOverwritePreviousAnimation(boolean var1) {
      this.overwritePreviousAnimation = var1;
   }

   public final void setAnim(String var1) {
      this.setAnim(var1, 0.15F);
   }

   public final void setAnim(String var1, float var2) {
      if (var1 == null) {
         this.animation = null;
      } else if (var2 < 0.0F) {
         throw new IllegalArgumentException("blendTime cannot be less than zero");
      } else {
         Animation var3;
         if ((var3 = this.control.getAnim(var1)) == null) {
            throw new IllegalArgumentException("Cannot find animation named: '" + var1 + "' in " + this.control.getAnimationNames());
         } else {
            this.control.notifyAnimChange(this, var1);
            if (this.animation != null && var2 > 0.0F) {
               AnimationChannel.Blend var4;
               (var4 = new AnimationChannel.Blend()).blendFrom = this.animation;
               var4.timeBlendFrom = this.time;
               var4.speedBlendFrom = this.speed;
               var4.loopModeBlendFrom = this.loopMode;
               var4.blendAmount = 0.0F;
               var4.blendRate = 1.0F / var2;
               this.blendQueue.add(0, var4);

               while(this.blendQueue.size() > 1) {
                  this.blendQueue.remove(this.blendQueue.size() - 1);
               }
            }

            this.animation = var3;
            this.time = 0.0F;
            this.speed = 1.0F;
            this.loopMode = LoopMode.LOOP;
         }
      }
   }

   final void update(Timer var1) {
      if (this.isActive()) {
         float var5 = var1 != null ? var1.getDelta() : 0.0F;
         float var2 = 1.0F;

         for(int var3 = 0; var3 < this.blendQueue.size(); ++var3) {
            AnimationChannel.Blend var4;
            (var4 = (AnimationChannel.Blend)this.blendQueue.get(var3)).blendFrom.setTime(var4.timeBlendFrom, 1.0F - var4.blendAmount, this.control, this);
            var4.timeBlendFrom = var5 * var4.speedBlendFrom;
            var4.timeBlendFrom = clampWrapTime(var4.timeBlendFrom, var4.blendFrom.getLength(), var4.loopModeBlendFrom);
            if (var4.timeBlendFrom < 0.0F) {
               var4.timeBlendFrom = -var4.timeBlendFrom;
               var4.speedBlendFrom = -var4.speedBlendFrom;
            }

            var4.blendAmount = var5 * var4.blendRate;
            if (var4.blendAmount > 1.0F) {
               var4.blendAmount = 1.0F;
               this.blendQueue.remove(var3);
               --var3;
            }

            var2 = Math.min(var4.blendAmount, var2);
         }

         this.animation.setTime(this.time, var2, this.control, this);
         this.time += var5 * this.speed;
         if (this.animation.getLength() > 0.0F) {
            if (this.time >= this.animation.getLength()) {
               this.control.notifyAnimCycleDone(this, this.animation.getName());
            } else if (this.time < 0.0F) {
               this.control.notifyAnimCycleDone(this, this.animation.getName());
            }
         }

         if (this.isActive()) {
            this.time = clampWrapTime(this.time, this.animation.getLength(), this.loopMode);
            if (this.loopMode == LoopMode.DONT_LOOP_DEACTIVATE && this.time == this.animation.getLength()) {
               this.animation = null;
            }

            if (this.time < 0.0F) {
               this.time = -this.time;
               this.speed = -this.speed;
            }

         }
      }
   }

   class Blend {
      private Animation blendFrom;
      private float timeBlendFrom;
      private float speedBlendFrom;
      private float blendAmount;
      private float blendRate;
      private LoopMode loopModeBlendFrom;

      private Blend() {
         this.blendAmount = 1.0F;
         this.blendRate = 0.0F;
      }

      // $FF: synthetic method
      Blend(Object var2) {
         this();
      }
   }
}
