package org.schema.schine.graphicsengine.animation;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public class Animation {
   private final HashMap tracks = new HashMap();
   public float animationLength;
   protected boolean loop = true;
   private String name;

   public float getLength() {
      return this.animationLength;
   }

   public String getName() {
      return this.name;
   }

   public void setName(String var1) {
      this.name = var1;
   }

   public HashMap getTracks() {
      return this.tracks;
   }

   public boolean isLoop() {
      return this.loop;
   }

   public void setLoop(boolean var1) {
      this.loop = var1;
   }

   public void setTime(float var1, float var2, AnimationController var3, AnimationChannel var4) {
      if (this.tracks.isEmpty()) {
         System.err.println("No tracks in animation");
      }

      Iterator var5 = this.tracks.entrySet().iterator();

      while(var5.hasNext()) {
         ((AnimationTrack)((Entry)var5.next()).getValue()).setTime(var1, var2, var3, var4);
      }

   }

   public String toString() {
      return "Anim[" + this.name + "]";
   }
}
