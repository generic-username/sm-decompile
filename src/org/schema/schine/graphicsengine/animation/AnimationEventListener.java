package org.schema.schine.graphicsengine.animation;

public abstract class AnimationEventListener {
   public abstract void onAnimChange(AnimationController var1, AnimationChannel var2, String var3);

   public abstract void onAnimCycleDone(AnimationController var1, AnimationChannel var2, String var3);

   public abstract boolean removeOnFinished();
}
