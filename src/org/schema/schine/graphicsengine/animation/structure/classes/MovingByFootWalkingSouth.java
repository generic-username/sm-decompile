package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingByFootWalkingSouth extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_BYFOOT_WALKING_SOUTH;
   }
}
