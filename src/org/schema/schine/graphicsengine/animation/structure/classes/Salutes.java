package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class Salutes extends AnimationStructSet {
   public final SalutesSalute salutesSalute = new SalutesSalute();

   public void checkAnimations(String var1) {
      if (!this.salutesSalute.parsed) {
         this.salutesSalute.parse((Node)null, var1, this);
      }

      this.children.add(this.salutesSalute);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null && var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("salute")) {
         this.salutesSalute.parse(var1, var2, this);
      }

   }
}
