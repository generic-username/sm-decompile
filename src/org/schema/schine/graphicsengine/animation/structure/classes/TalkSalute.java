package org.schema.schine.graphicsengine.animation.structure.classes;

public class TalkSalute extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.TALK_SALUTE;
   }
}
