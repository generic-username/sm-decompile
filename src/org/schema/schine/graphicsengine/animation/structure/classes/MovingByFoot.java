package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class MovingByFoot extends AnimationStructSet {
   public final MovingByFootCrawling movingByFootCrawling = new MovingByFootCrawling();
   public final MovingByFootSlowWalking movingByFootSlowWalking = new MovingByFootSlowWalking();
   public final MovingByFootWalking movingByFootWalking = new MovingByFootWalking();
   public final MovingByFootRunning movingByFootRunning = new MovingByFootRunning();

   public void checkAnimations(String var1) {
      if (!this.movingByFootCrawling.parsed) {
         this.movingByFootCrawling.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootCrawling);
      if (!this.movingByFootSlowWalking.parsed) {
         this.movingByFootSlowWalking.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootSlowWalking);
      if (!this.movingByFootWalking.parsed) {
         this.movingByFootWalking.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootWalking);
      if (!this.movingByFootRunning.parsed) {
         this.movingByFootRunning.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootRunning);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("crawling")) {
            this.movingByFootCrawling.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("slowwalking")) {
            this.movingByFootSlowWalking.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("walking")) {
            this.movingByFootWalking.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("running")) {
            this.movingByFootRunning.parse(var1, var2, this);
         }
      }

   }
}
