package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class MovingJumping extends AnimationStructSet {
   public final MovingJumpingJumpUp movingJumpingJumpUp = new MovingJumpingJumpUp();
   public final MovingJumpingJumpDown movingJumpingJumpDown = new MovingJumpingJumpDown();

   public void checkAnimations(String var1) {
      if (!this.movingJumpingJumpUp.parsed) {
         this.movingJumpingJumpUp.parse((Node)null, var1, this);
      }

      this.children.add(this.movingJumpingJumpUp);
      if (!this.movingJumpingJumpDown.parsed) {
         this.movingJumpingJumpDown.parse((Node)null, var1, this);
      }

      this.children.add(this.movingJumpingJumpDown);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("jumpup")) {
            this.movingJumpingJumpUp.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("jumpdown")) {
            this.movingJumpingJumpDown.parse(var1, var2, this);
         }
      }

   }
}
