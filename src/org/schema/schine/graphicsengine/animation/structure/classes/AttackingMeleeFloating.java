package org.schema.schine.graphicsengine.animation.structure.classes;

public class AttackingMeleeFloating extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.ATTACKING_MEELEE_FLOATING;
   }
}
