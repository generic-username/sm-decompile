package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class AnimationStructure extends AnimationStructSet {
   public final Attacking attacking = new Attacking();
   public final Death death = new Death();
   public final Sitting sitting = new Sitting();
   public final Hit hit = new Hit();
   public final Dancing dancing = new Dancing();
   public final Idling idling = new Idling();
   public final Salutes salutes = new Salutes();
   public final Talk talk = new Talk();
   public final Moving moving = new Moving();
   public final Helmet helmet = new Helmet();
   public final UpperBody upperBody = new UpperBody();

   public void checkAnimations(String var1) {
      if (!this.attacking.parsed) {
         this.attacking.parse((Node)null, var1, this);
      }

      this.children.add(this.attacking);
      if (!this.death.parsed) {
         this.death.parse((Node)null, var1, this);
      }

      this.children.add(this.death);
      if (!this.sitting.parsed) {
         this.sitting.parse((Node)null, var1, this);
      }

      this.children.add(this.sitting);
      if (!this.hit.parsed) {
         this.hit.parse((Node)null, var1, this);
      }

      this.children.add(this.hit);
      if (!this.dancing.parsed) {
         this.dancing.parse((Node)null, var1, this);
      }

      this.children.add(this.dancing);
      if (!this.idling.parsed) {
         this.idling.parse((Node)null, var1, this);
      }

      this.children.add(this.idling);
      if (!this.salutes.parsed) {
         this.salutes.parse((Node)null, var1, this);
      }

      this.children.add(this.salutes);
      if (!this.talk.parsed) {
         this.talk.parse((Node)null, var1, this);
      }

      this.children.add(this.talk);
      if (!this.moving.parsed) {
         this.moving.parse((Node)null, var1, this);
      }

      this.children.add(this.moving);
      if (!this.helmet.parsed) {
         this.helmet.parse((Node)null, var1, this);
      }

      this.children.add(this.helmet);
      if (!this.upperBody.parsed) {
         this.upperBody.parse((Node)null, var1, this);
      }

      this.children.add(this.upperBody);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("attacking")) {
            this.attacking.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("death")) {
            this.death.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("sitting")) {
            this.sitting.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("hit")) {
            this.hit.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("dancing")) {
            this.dancing.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("idling")) {
            this.idling.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("salutes")) {
            this.salutes.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("talk")) {
            this.talk.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("moving")) {
            this.moving.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("helmet")) {
            this.helmet.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("upperbody")) {
            this.upperBody.parse(var1, var2, this);
         }
      }

   }
}
