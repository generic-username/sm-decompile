package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class UpperBodyGun extends AnimationStructSet {
   public final UpperBodyGunIdle upperBodyGunIdle = new UpperBodyGunIdle();
   public final UpperBodyGunIdleIn upperBodyGunIdleIn = new UpperBodyGunIdleIn();
   public final UpperBodyGunDraw upperBodyGunDraw = new UpperBodyGunDraw();
   public final UpperBodyGunAway upperBodyGunAway = new UpperBodyGunAway();
   public final UpperBodyGunFire upperBodyGunFire = new UpperBodyGunFire();
   public final UpperBodyGunFireHeavy upperBodyGunFireHeavy = new UpperBodyGunFireHeavy();
   public final UpperBodyGunMelee upperBodyGunMelee = new UpperBodyGunMelee();

   public void checkAnimations(String var1) {
      if (!this.upperBodyGunIdle.parsed) {
         this.upperBodyGunIdle.parse((Node)null, var1, this);
      }

      this.children.add(this.upperBodyGunIdle);
      if (!this.upperBodyGunIdleIn.parsed) {
         this.upperBodyGunIdleIn.parse((Node)null, var1, this);
      }

      this.children.add(this.upperBodyGunIdleIn);
      if (!this.upperBodyGunDraw.parsed) {
         this.upperBodyGunDraw.parse((Node)null, var1, this);
      }

      this.children.add(this.upperBodyGunDraw);
      if (!this.upperBodyGunAway.parsed) {
         this.upperBodyGunAway.parse((Node)null, var1, this);
      }

      this.children.add(this.upperBodyGunAway);
      if (!this.upperBodyGunFire.parsed) {
         this.upperBodyGunFire.parse((Node)null, var1, this);
      }

      this.children.add(this.upperBodyGunFire);
      if (!this.upperBodyGunFireHeavy.parsed) {
         this.upperBodyGunFireHeavy.parse((Node)null, var1, this);
      }

      this.children.add(this.upperBodyGunFireHeavy);
      if (!this.upperBodyGunMelee.parsed) {
         this.upperBodyGunMelee.parse((Node)null, var1, this);
      }

      this.children.add(this.upperBodyGunMelee);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("idle")) {
            this.upperBodyGunIdle.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("idlein")) {
            this.upperBodyGunIdleIn.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("draw")) {
            this.upperBodyGunDraw.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("away")) {
            this.upperBodyGunAway.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("fire")) {
            this.upperBodyGunFire.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("fireheavy")) {
            this.upperBodyGunFireHeavy.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("meelee")) {
            this.upperBodyGunMelee.parse(var1, var2, this);
         }
      }

   }
}
