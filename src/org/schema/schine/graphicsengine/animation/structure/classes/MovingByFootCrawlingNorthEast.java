package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingByFootCrawlingNorthEast extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_BYFOOT_CRAWLING_NORTHEAST;
   }
}
