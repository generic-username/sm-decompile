package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingByFootSlowWalkingSouth extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_BYFOOT_SLOWWALKING_SOUTH;
   }
}
