package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingByFootCrawlingSouth extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_BYFOOT_CRAWLING_SOUTH;
   }
}
