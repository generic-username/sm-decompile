package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingByFootCrawlingSouthEast extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_BYFOOT_CRAWLING_SOUTHEAST;
   }
}
