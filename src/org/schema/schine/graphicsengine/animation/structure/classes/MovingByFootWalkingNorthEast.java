package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingByFootWalkingNorthEast extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_BYFOOT_WALKING_NORTHEAST;
   }
}
