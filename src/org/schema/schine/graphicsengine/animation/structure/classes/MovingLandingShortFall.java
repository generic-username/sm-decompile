package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingLandingShortFall extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_LANDING_SHORTFALL;
   }
}
