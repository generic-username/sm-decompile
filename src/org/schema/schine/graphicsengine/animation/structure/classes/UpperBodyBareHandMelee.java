package org.schema.schine.graphicsengine.animation.structure.classes;

public class UpperBodyBareHandMelee extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.UPPERBODY_BAREHAND_MEELEE;
   }
}
