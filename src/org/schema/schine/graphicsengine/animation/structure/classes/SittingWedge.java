package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class SittingWedge extends AnimationStructSet {
   public final SittingWedgeNoFloor sittingWedgeNoFloor = new SittingWedgeNoFloor();
   public final SittingWedgeFloor sittingWedgeFloor = new SittingWedgeFloor();

   public void checkAnimations(String var1) {
      if (!this.sittingWedgeNoFloor.parsed) {
         this.sittingWedgeNoFloor.parse((Node)null, var1, this);
      }

      this.children.add(this.sittingWedgeNoFloor);
      if (!this.sittingWedgeFloor.parsed) {
         this.sittingWedgeFloor.parse((Node)null, var1, this);
      }

      this.children.add(this.sittingWedgeFloor);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("nofloor")) {
            this.sittingWedgeNoFloor.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("floor")) {
            this.sittingWedgeFloor.parse(var1, var2, this);
         }
      }

   }
}
