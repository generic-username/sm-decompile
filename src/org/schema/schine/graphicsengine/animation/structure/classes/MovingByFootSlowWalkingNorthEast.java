package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingByFootSlowWalkingNorthEast extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_BYFOOT_SLOWWALKING_NORTHEAST;
   }
}
