package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingByFootCrawlingNorthWest extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_BYFOOT_CRAWLING_NORTHWEST;
   }
}
