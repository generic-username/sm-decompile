package org.schema.schine.graphicsengine.animation.structure.classes;

public class DeathFloating extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.DEATH_FLOATING;
   }
}
