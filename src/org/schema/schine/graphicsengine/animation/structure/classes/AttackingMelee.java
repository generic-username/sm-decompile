package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class AttackingMelee extends AnimationStructSet {
   public final AttackingMeleeFloating attackingMeleeFloating = new AttackingMeleeFloating();
   public final AttackingMeleeGravity attackingMeleeGravity = new AttackingMeleeGravity();

   public void checkAnimations(String var1) {
      if (!this.attackingMeleeFloating.parsed) {
         this.attackingMeleeFloating.parse((Node)null, var1, this);
      }

      this.children.add(this.attackingMeleeFloating);
      if (!this.attackingMeleeGravity.parsed) {
         this.attackingMeleeGravity.parse((Node)null, var1, this);
      }

      this.children.add(this.attackingMeleeGravity);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("floating")) {
            this.attackingMeleeFloating.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("gravity")) {
            this.attackingMeleeGravity.parse(var1, var2, this);
         }
      }

   }
}
