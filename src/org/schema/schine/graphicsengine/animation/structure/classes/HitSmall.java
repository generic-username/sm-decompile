package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class HitSmall extends AnimationStructSet {
   public final HitSmallFloating hitSmallFloating = new HitSmallFloating();
   public final HitSmallGravity hitSmallGravity = new HitSmallGravity();

   public void checkAnimations(String var1) {
      if (!this.hitSmallFloating.parsed) {
         this.hitSmallFloating.parse((Node)null, var1, this);
      }

      this.children.add(this.hitSmallFloating);
      if (!this.hitSmallGravity.parsed) {
         this.hitSmallGravity.parse((Node)null, var1, this);
      }

      this.children.add(this.hitSmallGravity);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("floating")) {
            this.hitSmallFloating.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("gravity")) {
            this.hitSmallGravity.parse(var1, var2, this);
         }
      }

   }
}
