package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class MovingByFootSlowWalking extends AnimationStructSet {
   public final MovingByFootSlowWalkingNorth movingByFootSlowWalkingNorth = new MovingByFootSlowWalkingNorth();
   public final MovingByFootSlowWalkingSouth movingByFootSlowWalkingSouth = new MovingByFootSlowWalkingSouth();
   public final MovingByFootSlowWalkingWest movingByFootSlowWalkingWest = new MovingByFootSlowWalkingWest();
   public final MovingByFootSlowWalkingEast movingByFootSlowWalkingEast = new MovingByFootSlowWalkingEast();
   public final MovingByFootSlowWalkingNorthEast movingByFootSlowWalkingNorthEast = new MovingByFootSlowWalkingNorthEast();
   public final MovingByFootSlowWalkingNorthWest movingByFootSlowWalkingNorthWest = new MovingByFootSlowWalkingNorthWest();
   public final MovingByFootSlowWalkingSouthWest movingByFootSlowWalkingSouthWest = new MovingByFootSlowWalkingSouthWest();
   public final MovingByFootSlowWalkingSouthEast movingByFootSlowWalkingSouthEast = new MovingByFootSlowWalkingSouthEast();

   public void checkAnimations(String var1) {
      if (!this.movingByFootSlowWalkingNorth.parsed) {
         this.movingByFootSlowWalkingNorth.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootSlowWalkingNorth);
      if (!this.movingByFootSlowWalkingSouth.parsed) {
         this.movingByFootSlowWalkingSouth.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootSlowWalkingSouth);
      if (!this.movingByFootSlowWalkingWest.parsed) {
         this.movingByFootSlowWalkingWest.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootSlowWalkingWest);
      if (!this.movingByFootSlowWalkingEast.parsed) {
         this.movingByFootSlowWalkingEast.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootSlowWalkingEast);
      if (!this.movingByFootSlowWalkingNorthEast.parsed) {
         this.movingByFootSlowWalkingNorthEast.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootSlowWalkingNorthEast);
      if (!this.movingByFootSlowWalkingNorthWest.parsed) {
         this.movingByFootSlowWalkingNorthWest.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootSlowWalkingNorthWest);
      if (!this.movingByFootSlowWalkingSouthWest.parsed) {
         this.movingByFootSlowWalkingSouthWest.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootSlowWalkingSouthWest);
      if (!this.movingByFootSlowWalkingSouthEast.parsed) {
         this.movingByFootSlowWalkingSouthEast.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootSlowWalkingSouthEast);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("north")) {
            this.movingByFootSlowWalkingNorth.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("south")) {
            this.movingByFootSlowWalkingSouth.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("west")) {
            this.movingByFootSlowWalkingWest.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("east")) {
            this.movingByFootSlowWalkingEast.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("northeast")) {
            this.movingByFootSlowWalkingNorthEast.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("northwest")) {
            this.movingByFootSlowWalkingNorthWest.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("southwest")) {
            this.movingByFootSlowWalkingSouthWest.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("southeast")) {
            this.movingByFootSlowWalkingSouthEast.parse(var1, var2, this);
         }
      }

   }
}
