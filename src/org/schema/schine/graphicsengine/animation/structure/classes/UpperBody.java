package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class UpperBody extends AnimationStructSet {
   public final UpperBodyGun upperBodyGun = new UpperBodyGun();
   public final UpperBodyFabricator upperBodyFabricator = new UpperBodyFabricator();
   public final UpperBodyBareHand upperBodyBareHand = new UpperBodyBareHand();

   public void checkAnimations(String var1) {
      if (!this.upperBodyGun.parsed) {
         this.upperBodyGun.parse((Node)null, var1, this);
      }

      this.children.add(this.upperBodyGun);
      if (!this.upperBodyFabricator.parsed) {
         this.upperBodyFabricator.parse((Node)null, var1, this);
      }

      this.children.add(this.upperBodyFabricator);
      if (!this.upperBodyBareHand.parsed) {
         this.upperBodyBareHand.parse((Node)null, var1, this);
      }

      this.children.add(this.upperBodyBareHand);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("gun")) {
            this.upperBodyGun.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("fabricator")) {
            this.upperBodyFabricator.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("barehand")) {
            this.upperBodyBareHand.parse(var1, var2, this);
         }
      }

   }
}
