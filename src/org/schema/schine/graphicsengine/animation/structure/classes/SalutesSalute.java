package org.schema.schine.graphicsengine.animation.structure.classes;

public class SalutesSalute extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.SALUTES_SALUTE;
   }
}
