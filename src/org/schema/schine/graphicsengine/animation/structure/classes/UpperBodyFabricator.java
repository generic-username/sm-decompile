package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class UpperBodyFabricator extends AnimationStructSet {
   public final UpperBodyFabricatorIdle upperBodyFabricatorIdle = new UpperBodyFabricatorIdle();
   public final UpperBodyFabricatorDraw upperBodyFabricatorDraw = new UpperBodyFabricatorDraw();
   public final UpperBodyFabricatorAway upperBodyFabricatorAway = new UpperBodyFabricatorAway();
   public final UpperBodyFabricatorFire upperBodyFabricatorFire = new UpperBodyFabricatorFire();
   public final UpperBodyFabricatorPumpAction upperBodyFabricatorPumpAction = new UpperBodyFabricatorPumpAction();

   public void checkAnimations(String var1) {
      if (!this.upperBodyFabricatorIdle.parsed) {
         this.upperBodyFabricatorIdle.parse((Node)null, var1, this);
      }

      this.children.add(this.upperBodyFabricatorIdle);
      if (!this.upperBodyFabricatorDraw.parsed) {
         this.upperBodyFabricatorDraw.parse((Node)null, var1, this);
      }

      this.children.add(this.upperBodyFabricatorDraw);
      if (!this.upperBodyFabricatorAway.parsed) {
         this.upperBodyFabricatorAway.parse((Node)null, var1, this);
      }

      this.children.add(this.upperBodyFabricatorAway);
      if (!this.upperBodyFabricatorFire.parsed) {
         this.upperBodyFabricatorFire.parse((Node)null, var1, this);
      }

      this.children.add(this.upperBodyFabricatorFire);
      if (!this.upperBodyFabricatorPumpAction.parsed) {
         this.upperBodyFabricatorPumpAction.parse((Node)null, var1, this);
      }

      this.children.add(this.upperBodyFabricatorPumpAction);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("idle")) {
            this.upperBodyFabricatorIdle.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("draw")) {
            this.upperBodyFabricatorDraw.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("away")) {
            this.upperBodyFabricatorAway.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("fire")) {
            this.upperBodyFabricatorFire.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("pumpaction")) {
            this.upperBodyFabricatorPumpAction.parse(var1, var2, this);
         }
      }

   }
}
