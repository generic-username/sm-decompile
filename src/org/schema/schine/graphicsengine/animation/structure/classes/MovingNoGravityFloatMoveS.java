package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingNoGravityFloatMoveS extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_NOGRAVITY_FLOATMOVES;
   }
}
