package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingFallingStandard extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_FALLING_STANDARD;
   }
}
