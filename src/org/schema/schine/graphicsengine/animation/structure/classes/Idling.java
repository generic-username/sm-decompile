package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class Idling extends AnimationStructSet {
   public final IdlingFloating idlingFloating = new IdlingFloating();
   public final IdlingGravity idlingGravity = new IdlingGravity();

   public void checkAnimations(String var1) {
      if (!this.idlingFloating.parsed) {
         this.idlingFloating.parse((Node)null, var1, this);
      }

      this.children.add(this.idlingFloating);
      if (!this.idlingGravity.parsed) {
         this.idlingGravity.parse((Node)null, var1, this);
      }

      this.children.add(this.idlingGravity);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("floating")) {
            this.idlingFloating.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("gravity")) {
            this.idlingGravity.parse(var1, var2, this);
         }
      }

   }
}
