package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingByFootCrawlingNorth extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_BYFOOT_CRAWLING_NORTH;
   }
}
