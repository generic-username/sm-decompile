package org.schema.schine.graphicsengine.animation.structure.classes;

public class UpperBodyGunAway extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.UPPERBODY_GUN_AWAY;
   }
}
