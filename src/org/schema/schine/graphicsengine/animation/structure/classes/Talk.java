package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class Talk extends AnimationStructSet {
   public final TalkSalute talkSalute = new TalkSalute();

   public void checkAnimations(String var1) {
      if (!this.talkSalute.parsed) {
         this.talkSalute.parse((Node)null, var1, this);
      }

      this.children.add(this.talkSalute);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null && var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("salute")) {
         this.talkSalute.parse(var1, var2, this);
      }

   }
}
