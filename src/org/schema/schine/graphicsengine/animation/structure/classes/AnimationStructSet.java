package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.ArrayList;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class AnimationStructSet {
   public static final boolean DEBUG = false;
   public AnimationStructSet parent;
   protected boolean parsed;
   protected ArrayList children = new ArrayList();

   public static String printParentPath(AnimationStructSet var0) {
      return var0.parent != null ? printParentPath(var0.parent) + " -> " + var0.getClass().getSimpleName() : var0.getClass().getSimpleName();
   }

   public void parse(Node var1, String var2, AnimationStructSet var3) {
      this.parent = var3;
      if (var1 != null) {
         NodeList var6 = var1.getChildNodes();

         for(int var7 = 0; var7 < var6.getLength(); ++var7) {
            Node var4;
            if ((var4 = var6.item(var7)).getNodeType() == 1) {
               Node var5;
               if ((var5 = var6.item(var7).getAttributes().getNamedItem("default")) != null) {
                  var2 = var5.getNodeValue();
               }

               this.parseAnimation(var4, var2);
            }
         }

         this.checkAnimations(var2);
      } else {
         this.parseAnimation((Node)null, var2);
         this.checkAnimations(var2);
      }

      this.parsed = true;
   }

   public abstract void checkAnimations(String var1);

   public abstract void parseAnimation(Node var1, String var2);

   public boolean isType(Class var1) {
      if (this.getClass().isAssignableFrom(var1)) {
         return true;
      } else {
         return this.parent != null ? this.parent.isType(var1) : false;
      }
   }
}
