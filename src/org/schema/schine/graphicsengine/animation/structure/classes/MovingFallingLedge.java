package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingFallingLedge extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_FALLING_LEDGE;
   }
}
