package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class SittingBlock extends AnimationStructSet {
   public final SittingBlockNoFloor sittingBlockNoFloor = new SittingBlockNoFloor();
   public final SittingBlockFloor sittingBlockFloor = new SittingBlockFloor();

   public void checkAnimations(String var1) {
      if (!this.sittingBlockNoFloor.parsed) {
         this.sittingBlockNoFloor.parse((Node)null, var1, this);
      }

      this.children.add(this.sittingBlockNoFloor);
      if (!this.sittingBlockFloor.parsed) {
         this.sittingBlockFloor.parse((Node)null, var1, this);
      }

      this.children.add(this.sittingBlockFloor);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("nofloor")) {
            this.sittingBlockNoFloor.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("floor")) {
            this.sittingBlockFloor.parse(var1, var2, this);
         }
      }

   }
}
