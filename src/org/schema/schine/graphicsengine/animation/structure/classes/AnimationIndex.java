package org.schema.schine.graphicsengine.animation.structure.classes;

public class AnimationIndex {
   public static final AnimationIndexElement ATTACKING_MEELEE_FLOATING = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.attacking.attackingMelee.attackingMeleeFloating;
      }

      public final String toString() {
         return "ATTACKING_MEELEE_FLOATING";
      }

      public final boolean isType(Class var1) {
         if (var1.equals(AttackingMeleeFloating.class)) {
            return true;
         } else if (var1.equals(AttackingMelee.class)) {
            return true;
         } else {
            return var1.equals(Attacking.class);
         }
      }
   };
   public static final AnimationIndexElement ATTACKING_MEELEE_GRAVITY = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.attacking.attackingMelee.attackingMeleeGravity;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(AttackingMeleeGravity.class)) {
            return true;
         } else if (var1.equals(AttackingMelee.class)) {
            return true;
         } else {
            return var1.equals(Attacking.class);
         }
      }

      public final String toString() {
         return "ATTACKING_MEELEE_GRAVITY";
      }
   };
   public static final AnimationIndexElement DEATH_FLOATING = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.death.deathFloating;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(DeathFloating.class)) {
            return true;
         } else {
            return var1.equals(Death.class);
         }
      }

      public final String toString() {
         return "DEATH_FLOATING";
      }
   };
   public static final AnimationIndexElement DEATH_GRAVITY = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.death.deathGravity;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(DeathGravity.class)) {
            return true;
         } else {
            return var1.equals(Death.class);
         }
      }

      public final String toString() {
         return "DEATH_GRAVITY";
      }
   };
   public static final AnimationIndexElement SITTING_BLOCK_NOFLOOR_IDLE = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.sitting.sittingBlock.sittingBlockNoFloor.sittingBlockNoFloorIdle;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(SittingBlockNoFloorIdle.class)) {
            return true;
         } else if (var1.equals(SittingBlockNoFloor.class)) {
            return true;
         } else if (var1.equals(SittingBlock.class)) {
            return true;
         } else {
            return var1.equals(Sitting.class);
         }
      }

      public final String toString() {
         return "SITTING_BLOCK_NOFLOOR_IDLE";
      }
   };
   public static final AnimationIndexElement SITTING_BLOCK_FLOOR_IDLE = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.sitting.sittingBlock.sittingBlockFloor.sittingBlockFloorIdle;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(SittingBlockFloorIdle.class)) {
            return true;
         } else if (var1.equals(SittingBlockFloor.class)) {
            return true;
         } else if (var1.equals(SittingBlock.class)) {
            return true;
         } else {
            return var1.equals(Sitting.class);
         }
      }

      public final String toString() {
         return "SITTING_BLOCK_FLOOR_IDLE";
      }
   };
   public static final AnimationIndexElement SITTING_WEDGE_NOFLOOR_IDLE = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.sitting.sittingWedge.sittingWedgeNoFloor.sittingWedgeNoFloorIdle;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(SittingWedgeNoFloorIdle.class)) {
            return true;
         } else if (var1.equals(SittingWedgeNoFloor.class)) {
            return true;
         } else if (var1.equals(SittingWedge.class)) {
            return true;
         } else {
            return var1.equals(Sitting.class);
         }
      }

      public final String toString() {
         return "SITTING_WEDGE_NOFLOOR_IDLE";
      }
   };
   public static final AnimationIndexElement SITTING_WEDGE_FLOOR_IDLE = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.sitting.sittingWedge.sittingWedgeFloor.sittingWedgeFloorIdle;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(SittingWedgeFloorIdle.class)) {
            return true;
         } else if (var1.equals(SittingWedgeFloor.class)) {
            return true;
         } else if (var1.equals(SittingWedge.class)) {
            return true;
         } else {
            return var1.equals(Sitting.class);
         }
      }

      public final String toString() {
         return "SITTING_WEDGE_FLOOR_IDLE";
      }
   };
   public static final AnimationIndexElement HIT_SMALL_FLOATING = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.hit.hitSmall.hitSmallFloating;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(HitSmallFloating.class)) {
            return true;
         } else if (var1.equals(HitSmall.class)) {
            return true;
         } else {
            return var1.equals(Hit.class);
         }
      }

      public final String toString() {
         return "HIT_SMALL_FLOATING";
      }
   };
   public static final AnimationIndexElement HIT_SMALL_GRAVITY = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.hit.hitSmall.hitSmallGravity;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(HitSmallGravity.class)) {
            return true;
         } else if (var1.equals(HitSmall.class)) {
            return true;
         } else {
            return var1.equals(Hit.class);
         }
      }

      public final String toString() {
         return "HIT_SMALL_GRAVITY";
      }
   };
   public static final AnimationIndexElement DANCING_GRAVITY = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.dancing.dancingGravity;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(DancingGravity.class)) {
            return true;
         } else {
            return var1.equals(Dancing.class);
         }
      }

      public final String toString() {
         return "DANCING_GRAVITY";
      }
   };
   public static final AnimationIndexElement IDLING_FLOATING = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.idling.idlingFloating;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(IdlingFloating.class)) {
            return true;
         } else {
            return var1.equals(Idling.class);
         }
      }

      public final String toString() {
         return "IDLING_FLOATING";
      }
   };
   public static final AnimationIndexElement IDLING_GRAVITY = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.idling.idlingGravity;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(IdlingGravity.class)) {
            return true;
         } else {
            return var1.equals(Idling.class);
         }
      }

      public final String toString() {
         return "IDLING_GRAVITY";
      }
   };
   public static final AnimationIndexElement SALUTES_SALUTE = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.salutes.salutesSalute;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(SalutesSalute.class)) {
            return true;
         } else {
            return var1.equals(Salutes.class);
         }
      }

      public final String toString() {
         return "SALUTES_SALUTE";
      }
   };
   public static final AnimationIndexElement TALK_SALUTE = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.talk.talkSalute;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(TalkSalute.class)) {
            return true;
         } else {
            return var1.equals(Talk.class);
         }
      }

      public final String toString() {
         return "TALK_SALUTE";
      }
   };
   public static final AnimationIndexElement MOVING_JUMPING_JUMPUP = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingJumping.movingJumpingJumpUp;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingJumpingJumpUp.class)) {
            return true;
         } else if (var1.equals(MovingJumping.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_JUMPING_JUMPUP";
      }
   };
   public static final AnimationIndexElement MOVING_JUMPING_JUMPDOWN = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingJumping.movingJumpingJumpDown;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingJumpingJumpDown.class)) {
            return true;
         } else if (var1.equals(MovingJumping.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_JUMPING_JUMPDOWN";
      }
   };
   public static final AnimationIndexElement MOVING_FALLING_STANDARD = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingFalling.movingFallingStandard;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingFallingStandard.class)) {
            return true;
         } else if (var1.equals(MovingFalling.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_FALLING_STANDARD";
      }
   };
   public static final AnimationIndexElement MOVING_FALLING_LEDGE = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingFalling.movingFallingLedge;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingFallingLedge.class)) {
            return true;
         } else if (var1.equals(MovingFalling.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_FALLING_LEDGE";
      }
   };
   public static final AnimationIndexElement MOVING_LANDING_SHORTFALL = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingLanding.movingLandingShortFall;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingLandingShortFall.class)) {
            return true;
         } else if (var1.equals(MovingLanding.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_LANDING_SHORTFALL";
      }
   };
   public static final AnimationIndexElement MOVING_LANDING_MIDDLEFALL = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingLanding.movingLandingMiddleFall;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingLandingMiddleFall.class)) {
            return true;
         } else if (var1.equals(MovingLanding.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_LANDING_MIDDLEFALL";
      }
   };
   public static final AnimationIndexElement MOVING_LANDING_LONGFALL = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingLanding.movingLandingLongFall;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingLandingLongFall.class)) {
            return true;
         } else if (var1.equals(MovingLanding.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_LANDING_LONGFALL";
      }
   };
   public static final AnimationIndexElement MOVING_NOGRAVITY_GRAVTONOGRAV = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingNoGravity.movingNoGravityGravToNoGrav;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingNoGravityGravToNoGrav.class)) {
            return true;
         } else if (var1.equals(MovingNoGravity.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_NOGRAVITY_GRAVTONOGRAV";
      }
   };
   public static final AnimationIndexElement MOVING_NOGRAVITY_NOGRAVTOGRAV = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingNoGravity.movingNoGravityNoGravToGrav;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingNoGravityNoGravToGrav.class)) {
            return true;
         } else if (var1.equals(MovingNoGravity.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_NOGRAVITY_NOGRAVTOGRAV";
      }
   };
   public static final AnimationIndexElement MOVING_NOGRAVITY_FLOATMOVEN = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingNoGravity.movingNoGravityFloatMoveN;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingNoGravityFloatMoveN.class)) {
            return true;
         } else if (var1.equals(MovingNoGravity.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_NOGRAVITY_FLOATMOVEN";
      }
   };
   public static final AnimationIndexElement MOVING_NOGRAVITY_FLOATMOVES = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingNoGravity.movingNoGravityFloatMoveS;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingNoGravityFloatMoveS.class)) {
            return true;
         } else if (var1.equals(MovingNoGravity.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_NOGRAVITY_FLOATMOVES";
      }
   };
   public static final AnimationIndexElement MOVING_NOGRAVITY_FLOATMOVEUP = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingNoGravity.movingNoGravityFloatMoveUp;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingNoGravityFloatMoveUp.class)) {
            return true;
         } else if (var1.equals(MovingNoGravity.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_NOGRAVITY_FLOATMOVEUP";
      }
   };
   public static final AnimationIndexElement MOVING_NOGRAVITY_FLOATMOVEDOWN = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingNoGravity.movingNoGravityFloatMoveDown;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingNoGravityFloatMoveDown.class)) {
            return true;
         } else if (var1.equals(MovingNoGravity.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_NOGRAVITY_FLOATMOVEDOWN";
      }
   };
   public static final AnimationIndexElement MOVING_NOGRAVITY_FLOATROT = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingNoGravity.movingNoGravityFloatRot;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingNoGravityFloatRot.class)) {
            return true;
         } else if (var1.equals(MovingNoGravity.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_NOGRAVITY_FLOATROT";
      }
   };
   public static final AnimationIndexElement MOVING_NOGRAVITY_FLOATHIT = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingNoGravity.movingNoGravityFloatHit;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingNoGravityFloatHit.class)) {
            return true;
         } else if (var1.equals(MovingNoGravity.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_NOGRAVITY_FLOATHIT";
      }
   };
   public static final AnimationIndexElement MOVING_NOGRAVITY_FLOATDEATH = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingNoGravity.movingNoGravityFloatDeath;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingNoGravityFloatDeath.class)) {
            return true;
         } else if (var1.equals(MovingNoGravity.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_NOGRAVITY_FLOATDEATH";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_CRAWLING_NORTH = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootCrawling.movingByFootCrawlingNorth;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootCrawlingNorth.class)) {
            return true;
         } else if (var1.equals(MovingByFootCrawling.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_CRAWLING_NORTH";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_CRAWLING_SOUTH = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootCrawling.movingByFootCrawlingSouth;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootCrawlingSouth.class)) {
            return true;
         } else if (var1.equals(MovingByFootCrawling.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_CRAWLING_SOUTH";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_CRAWLING_WEST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootCrawling.movingByFootCrawlingWest;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootCrawlingWest.class)) {
            return true;
         } else if (var1.equals(MovingByFootCrawling.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_CRAWLING_WEST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_CRAWLING_EAST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootCrawling.movingByFootCrawlingEast;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootCrawlingEast.class)) {
            return true;
         } else if (var1.equals(MovingByFootCrawling.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_CRAWLING_EAST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_CRAWLING_NORTHEAST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootCrawling.movingByFootCrawlingNorthEast;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootCrawlingNorthEast.class)) {
            return true;
         } else if (var1.equals(MovingByFootCrawling.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_CRAWLING_NORTHEAST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_CRAWLING_NORTHWEST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootCrawling.movingByFootCrawlingNorthWest;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootCrawlingNorthWest.class)) {
            return true;
         } else if (var1.equals(MovingByFootCrawling.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_CRAWLING_NORTHWEST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_CRAWLING_SOUTHWEST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootCrawling.movingByFootCrawlingSouthWest;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootCrawlingSouthWest.class)) {
            return true;
         } else if (var1.equals(MovingByFootCrawling.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_CRAWLING_SOUTHWEST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_CRAWLING_SOUTHEAST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootCrawling.movingByFootCrawlingSouthEast;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootCrawlingSouthEast.class)) {
            return true;
         } else if (var1.equals(MovingByFootCrawling.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_CRAWLING_SOUTHEAST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_SLOWWALKING_NORTH = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootSlowWalking.movingByFootSlowWalkingNorth;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootSlowWalkingNorth.class)) {
            return true;
         } else if (var1.equals(MovingByFootSlowWalking.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_SLOWWALKING_NORTH";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_SLOWWALKING_SOUTH = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootSlowWalking.movingByFootSlowWalkingSouth;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootSlowWalkingSouth.class)) {
            return true;
         } else if (var1.equals(MovingByFootSlowWalking.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_SLOWWALKING_SOUTH";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_SLOWWALKING_WEST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootSlowWalking.movingByFootSlowWalkingWest;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootSlowWalkingWest.class)) {
            return true;
         } else if (var1.equals(MovingByFootSlowWalking.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_SLOWWALKING_WEST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_SLOWWALKING_EAST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootSlowWalking.movingByFootSlowWalkingEast;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootSlowWalkingEast.class)) {
            return true;
         } else if (var1.equals(MovingByFootSlowWalking.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_SLOWWALKING_EAST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_SLOWWALKING_NORTHEAST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootSlowWalking.movingByFootSlowWalkingNorthEast;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootSlowWalkingNorthEast.class)) {
            return true;
         } else if (var1.equals(MovingByFootSlowWalking.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_SLOWWALKING_NORTHEAST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_SLOWWALKING_NORTHWEST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootSlowWalking.movingByFootSlowWalkingNorthWest;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootSlowWalkingNorthWest.class)) {
            return true;
         } else if (var1.equals(MovingByFootSlowWalking.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_SLOWWALKING_NORTHWEST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_SLOWWALKING_SOUTHWEST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootSlowWalking.movingByFootSlowWalkingSouthWest;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootSlowWalkingSouthWest.class)) {
            return true;
         } else if (var1.equals(MovingByFootSlowWalking.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_SLOWWALKING_SOUTHWEST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_SLOWWALKING_SOUTHEAST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootSlowWalking.movingByFootSlowWalkingSouthEast;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootSlowWalkingSouthEast.class)) {
            return true;
         } else if (var1.equals(MovingByFootSlowWalking.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_SLOWWALKING_SOUTHEAST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_WALKING_NORTH = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootWalking.movingByFootWalkingNorth;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootWalkingNorth.class)) {
            return true;
         } else if (var1.equals(MovingByFootWalking.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_WALKING_NORTH";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_WALKING_SOUTH = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootWalking.movingByFootWalkingSouth;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootWalkingSouth.class)) {
            return true;
         } else if (var1.equals(MovingByFootWalking.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_WALKING_SOUTH";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_WALKING_WEST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootWalking.movingByFootWalkingWest;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootWalkingWest.class)) {
            return true;
         } else if (var1.equals(MovingByFootWalking.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_WALKING_WEST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_WALKING_EAST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootWalking.movingByFootWalkingEast;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootWalkingEast.class)) {
            return true;
         } else if (var1.equals(MovingByFootWalking.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_WALKING_EAST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_WALKING_NORTHEAST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootWalking.movingByFootWalkingNorthEast;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootWalkingNorthEast.class)) {
            return true;
         } else if (var1.equals(MovingByFootWalking.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_WALKING_NORTHEAST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_WALKING_NORTHWEST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootWalking.movingByFootWalkingNorthWest;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootWalkingNorthWest.class)) {
            return true;
         } else if (var1.equals(MovingByFootWalking.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_WALKING_NORTHWEST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_WALKING_SOUTHWEST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootWalking.movingByFootWalkingSouthWest;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootWalkingSouthWest.class)) {
            return true;
         } else if (var1.equals(MovingByFootWalking.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_WALKING_SOUTHWEST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_WALKING_SOUTHEAST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootWalking.movingByFootWalkingSouthEast;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootWalkingSouthEast.class)) {
            return true;
         } else if (var1.equals(MovingByFootWalking.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_WALKING_SOUTHEAST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_RUNNING_NORTH = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootRunning.movingByFootRunningNorth;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootRunningNorth.class)) {
            return true;
         } else if (var1.equals(MovingByFootRunning.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_RUNNING_NORTH";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_RUNNING_SOUTH = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootRunning.movingByFootRunningSouth;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootRunningSouth.class)) {
            return true;
         } else if (var1.equals(MovingByFootRunning.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_RUNNING_SOUTH";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_RUNNING_WEST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootRunning.movingByFootRunningWest;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootRunningWest.class)) {
            return true;
         } else if (var1.equals(MovingByFootRunning.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_RUNNING_WEST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_RUNNING_EAST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootRunning.movingByFootRunningEast;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootRunningEast.class)) {
            return true;
         } else if (var1.equals(MovingByFootRunning.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_RUNNING_EAST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_RUNNING_NORTHEAST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootRunning.movingByFootRunningNorthEast;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootRunningNorthEast.class)) {
            return true;
         } else if (var1.equals(MovingByFootRunning.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_RUNNING_NORTHEAST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_RUNNING_NORTHWEST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootRunning.movingByFootRunningNorthWest;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootRunningNorthWest.class)) {
            return true;
         } else if (var1.equals(MovingByFootRunning.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_RUNNING_NORTHWEST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_RUNNING_SOUTHWEST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootRunning.movingByFootRunningSouthWest;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootRunningSouthWest.class)) {
            return true;
         } else if (var1.equals(MovingByFootRunning.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_RUNNING_SOUTHWEST";
      }
   };
   public static final AnimationIndexElement MOVING_BYFOOT_RUNNING_SOUTHEAST = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.moving.movingByFoot.movingByFootRunning.movingByFootRunningSouthEast;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(MovingByFootRunningSouthEast.class)) {
            return true;
         } else if (var1.equals(MovingByFootRunning.class)) {
            return true;
         } else if (var1.equals(MovingByFoot.class)) {
            return true;
         } else {
            return var1.equals(Moving.class);
         }
      }

      public final String toString() {
         return "MOVING_BYFOOT_RUNNING_SOUTHEAST";
      }
   };
   public static final AnimationIndexElement HELMET_ON = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.helmet.helmetOn;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(HelmetOn.class)) {
            return true;
         } else {
            return var1.equals(Helmet.class);
         }
      }

      public final String toString() {
         return "HELMET_ON";
      }
   };
   public static final AnimationIndexElement HELMET_OFF = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.helmet.helmetOff;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(HelmetOff.class)) {
            return true;
         } else {
            return var1.equals(Helmet.class);
         }
      }

      public final String toString() {
         return "HELMET_OFF";
      }
   };
   public static final AnimationIndexElement UPPERBODY_GUN_IDLE = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.upperBody.upperBodyGun.upperBodyGunIdle;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(UpperBodyGunIdle.class)) {
            return true;
         } else if (var1.equals(UpperBodyGun.class)) {
            return true;
         } else {
            return var1.equals(UpperBody.class);
         }
      }

      public final String toString() {
         return "UPPERBODY_GUN_IDLE";
      }
   };
   public static final AnimationIndexElement UPPERBODY_GUN_IDLEIN = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.upperBody.upperBodyGun.upperBodyGunIdleIn;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(UpperBodyGunIdleIn.class)) {
            return true;
         } else if (var1.equals(UpperBodyGun.class)) {
            return true;
         } else {
            return var1.equals(UpperBody.class);
         }
      }

      public final String toString() {
         return "UPPERBODY_GUN_IDLEIN";
      }
   };
   public static final AnimationIndexElement UPPERBODY_GUN_DRAW = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.upperBody.upperBodyGun.upperBodyGunDraw;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(UpperBodyGunDraw.class)) {
            return true;
         } else if (var1.equals(UpperBodyGun.class)) {
            return true;
         } else {
            return var1.equals(UpperBody.class);
         }
      }

      public final String toString() {
         return "UPPERBODY_GUN_DRAW";
      }
   };
   public static final AnimationIndexElement UPPERBODY_GUN_AWAY = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.upperBody.upperBodyGun.upperBodyGunAway;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(UpperBodyGunAway.class)) {
            return true;
         } else if (var1.equals(UpperBodyGun.class)) {
            return true;
         } else {
            return var1.equals(UpperBody.class);
         }
      }

      public final String toString() {
         return "UPPERBODY_GUN_AWAY";
      }
   };
   public static final AnimationIndexElement UPPERBODY_GUN_FIRE = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.upperBody.upperBodyGun.upperBodyGunFire;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(UpperBodyGunFire.class)) {
            return true;
         } else if (var1.equals(UpperBodyGun.class)) {
            return true;
         } else {
            return var1.equals(UpperBody.class);
         }
      }

      public final String toString() {
         return "UPPERBODY_GUN_FIRE";
      }
   };
   public static final AnimationIndexElement UPPERBODY_GUN_FIREHEAVY = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.upperBody.upperBodyGun.upperBodyGunFireHeavy;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(UpperBodyGunFireHeavy.class)) {
            return true;
         } else if (var1.equals(UpperBodyGun.class)) {
            return true;
         } else {
            return var1.equals(UpperBody.class);
         }
      }

      public final String toString() {
         return "UPPERBODY_GUN_FIREHEAVY";
      }
   };
   public static final AnimationIndexElement UPPERBODY_GUN_MEELEE = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.upperBody.upperBodyGun.upperBodyGunMelee;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(UpperBodyGunMelee.class)) {
            return true;
         } else if (var1.equals(UpperBodyGun.class)) {
            return true;
         } else {
            return var1.equals(UpperBody.class);
         }
      }

      public final String toString() {
         return "UPPERBODY_GUN_MEELEE";
      }
   };
   public static final AnimationIndexElement UPPERBODY_FABRICATOR_IDLE = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.upperBody.upperBodyFabricator.upperBodyFabricatorIdle;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(UpperBodyFabricatorIdle.class)) {
            return true;
         } else if (var1.equals(UpperBodyFabricator.class)) {
            return true;
         } else {
            return var1.equals(UpperBody.class);
         }
      }

      public final String toString() {
         return "UPPERBODY_FABRICATOR_IDLE";
      }
   };
   public static final AnimationIndexElement UPPERBODY_FABRICATOR_DRAW = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.upperBody.upperBodyFabricator.upperBodyFabricatorDraw;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(UpperBodyFabricatorDraw.class)) {
            return true;
         } else if (var1.equals(UpperBodyFabricator.class)) {
            return true;
         } else {
            return var1.equals(UpperBody.class);
         }
      }

      public final String toString() {
         return "UPPERBODY_FABRICATOR_DRAW";
      }
   };
   public static final AnimationIndexElement UPPERBODY_FABRICATOR_AWAY = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.upperBody.upperBodyFabricator.upperBodyFabricatorAway;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(UpperBodyFabricatorAway.class)) {
            return true;
         } else if (var1.equals(UpperBodyFabricator.class)) {
            return true;
         } else {
            return var1.equals(UpperBody.class);
         }
      }

      public final String toString() {
         return "UPPERBODY_FABRICATOR_AWAY";
      }
   };
   public static final AnimationIndexElement UPPERBODY_FABRICATOR_FIRE = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.upperBody.upperBodyFabricator.upperBodyFabricatorFire;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(UpperBodyFabricatorFire.class)) {
            return true;
         } else if (var1.equals(UpperBodyFabricator.class)) {
            return true;
         } else {
            return var1.equals(UpperBody.class);
         }
      }

      public final String toString() {
         return "UPPERBODY_FABRICATOR_FIRE";
      }
   };
   public static final AnimationIndexElement UPPERBODY_FABRICATOR_PUMPACTION = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.upperBody.upperBodyFabricator.upperBodyFabricatorPumpAction;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(UpperBodyFabricatorPumpAction.class)) {
            return true;
         } else if (var1.equals(UpperBodyFabricator.class)) {
            return true;
         } else {
            return var1.equals(UpperBody.class);
         }
      }

      public final String toString() {
         return "UPPERBODY_FABRICATOR_PUMPACTION";
      }
   };
   public static final AnimationIndexElement UPPERBODY_BAREHAND_MEELEE = new AnimationIndexElement() {
      public final AnimationStructEndPoint get(AnimationStructure var1) {
         return var1.upperBody.upperBodyBareHand.upperBodyBareHandMelee;
      }

      public final boolean isType(Class var1) {
         if (var1.equals(UpperBodyBareHandMelee.class)) {
            return true;
         } else if (var1.equals(UpperBodyBareHand.class)) {
            return true;
         } else {
            return var1.equals(UpperBody.class);
         }
      }

      public final String toString() {
         return "UPPERBODY_BAREHAND_MEELEE";
      }
   };
   public static AnimationIndexElement[] animations;

   static {
      (animations = new AnimationIndexElement[78])[0] = ATTACKING_MEELEE_FLOATING;
      animations[1] = ATTACKING_MEELEE_GRAVITY;
      animations[2] = DEATH_FLOATING;
      animations[3] = DEATH_GRAVITY;
      animations[4] = SITTING_BLOCK_NOFLOOR_IDLE;
      animations[5] = SITTING_BLOCK_FLOOR_IDLE;
      animations[6] = SITTING_WEDGE_NOFLOOR_IDLE;
      animations[7] = SITTING_WEDGE_FLOOR_IDLE;
      animations[8] = HIT_SMALL_FLOATING;
      animations[9] = HIT_SMALL_GRAVITY;
      animations[10] = DANCING_GRAVITY;
      animations[11] = IDLING_FLOATING;
      animations[12] = IDLING_GRAVITY;
      animations[13] = SALUTES_SALUTE;
      animations[14] = TALK_SALUTE;
      animations[15] = MOVING_JUMPING_JUMPUP;
      animations[16] = MOVING_JUMPING_JUMPDOWN;
      animations[17] = MOVING_FALLING_STANDARD;
      animations[18] = MOVING_FALLING_LEDGE;
      animations[19] = MOVING_LANDING_SHORTFALL;
      animations[20] = MOVING_LANDING_MIDDLEFALL;
      animations[21] = MOVING_LANDING_LONGFALL;
      animations[22] = MOVING_NOGRAVITY_GRAVTONOGRAV;
      animations[23] = MOVING_NOGRAVITY_NOGRAVTOGRAV;
      animations[24] = MOVING_NOGRAVITY_FLOATMOVEN;
      animations[25] = MOVING_NOGRAVITY_FLOATMOVES;
      animations[26] = MOVING_NOGRAVITY_FLOATMOVEUP;
      animations[27] = MOVING_NOGRAVITY_FLOATMOVEDOWN;
      animations[28] = MOVING_NOGRAVITY_FLOATROT;
      animations[29] = MOVING_NOGRAVITY_FLOATHIT;
      animations[30] = MOVING_NOGRAVITY_FLOATDEATH;
      animations[31] = MOVING_BYFOOT_CRAWLING_NORTH;
      animations[32] = MOVING_BYFOOT_CRAWLING_SOUTH;
      animations[33] = MOVING_BYFOOT_CRAWLING_WEST;
      animations[34] = MOVING_BYFOOT_CRAWLING_EAST;
      animations[35] = MOVING_BYFOOT_CRAWLING_NORTHEAST;
      animations[36] = MOVING_BYFOOT_CRAWLING_NORTHWEST;
      animations[37] = MOVING_BYFOOT_CRAWLING_SOUTHWEST;
      animations[38] = MOVING_BYFOOT_CRAWLING_SOUTHEAST;
      animations[39] = MOVING_BYFOOT_SLOWWALKING_NORTH;
      animations[40] = MOVING_BYFOOT_SLOWWALKING_SOUTH;
      animations[41] = MOVING_BYFOOT_SLOWWALKING_WEST;
      animations[42] = MOVING_BYFOOT_SLOWWALKING_EAST;
      animations[43] = MOVING_BYFOOT_SLOWWALKING_NORTHEAST;
      animations[44] = MOVING_BYFOOT_SLOWWALKING_NORTHWEST;
      animations[45] = MOVING_BYFOOT_SLOWWALKING_SOUTHWEST;
      animations[46] = MOVING_BYFOOT_SLOWWALKING_SOUTHEAST;
      animations[47] = MOVING_BYFOOT_WALKING_NORTH;
      animations[48] = MOVING_BYFOOT_WALKING_SOUTH;
      animations[49] = MOVING_BYFOOT_WALKING_WEST;
      animations[50] = MOVING_BYFOOT_WALKING_EAST;
      animations[51] = MOVING_BYFOOT_WALKING_NORTHEAST;
      animations[52] = MOVING_BYFOOT_WALKING_NORTHWEST;
      animations[53] = MOVING_BYFOOT_WALKING_SOUTHWEST;
      animations[54] = MOVING_BYFOOT_WALKING_SOUTHEAST;
      animations[55] = MOVING_BYFOOT_RUNNING_NORTH;
      animations[56] = MOVING_BYFOOT_RUNNING_SOUTH;
      animations[57] = MOVING_BYFOOT_RUNNING_WEST;
      animations[58] = MOVING_BYFOOT_RUNNING_EAST;
      animations[59] = MOVING_BYFOOT_RUNNING_NORTHEAST;
      animations[60] = MOVING_BYFOOT_RUNNING_NORTHWEST;
      animations[61] = MOVING_BYFOOT_RUNNING_SOUTHWEST;
      animations[62] = MOVING_BYFOOT_RUNNING_SOUTHEAST;
      animations[63] = HELMET_ON;
      animations[64] = HELMET_OFF;
      animations[65] = UPPERBODY_GUN_IDLE;
      animations[66] = UPPERBODY_GUN_IDLEIN;
      animations[67] = UPPERBODY_GUN_DRAW;
      animations[68] = UPPERBODY_GUN_AWAY;
      animations[69] = UPPERBODY_GUN_FIRE;
      animations[70] = UPPERBODY_GUN_FIREHEAVY;
      animations[71] = UPPERBODY_GUN_MEELEE;
      animations[72] = UPPERBODY_FABRICATOR_IDLE;
      animations[73] = UPPERBODY_FABRICATOR_DRAW;
      animations[74] = UPPERBODY_FABRICATOR_AWAY;
      animations[75] = UPPERBODY_FABRICATOR_FIRE;
      animations[76] = UPPERBODY_FABRICATOR_PUMPACTION;
      animations[77] = UPPERBODY_BAREHAND_MEELEE;
   }
}
