package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingByFootSlowWalkingNorth extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_BYFOOT_SLOWWALKING_NORTH;
   }
}
