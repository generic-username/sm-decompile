package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class SittingBlockNoFloor extends AnimationStructSet {
   public final SittingBlockNoFloorIdle sittingBlockNoFloorIdle = new SittingBlockNoFloorIdle();

   public void checkAnimations(String var1) {
      if (!this.sittingBlockNoFloorIdle.parsed) {
         this.sittingBlockNoFloorIdle.parse((Node)null, var1, this);
      }

      this.children.add(this.sittingBlockNoFloorIdle);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null && var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("idle")) {
         this.sittingBlockNoFloorIdle.parse(var1, var2, this);
      }

   }
}
