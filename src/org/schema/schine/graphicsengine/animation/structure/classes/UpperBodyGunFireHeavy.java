package org.schema.schine.graphicsengine.animation.structure.classes;

public class UpperBodyGunFireHeavy extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.UPPERBODY_GUN_FIREHEAVY;
   }
}
