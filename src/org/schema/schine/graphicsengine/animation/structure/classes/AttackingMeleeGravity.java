package org.schema.schine.graphicsengine.animation.structure.classes;

public class AttackingMeleeGravity extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.ATTACKING_MEELEE_GRAVITY;
   }
}
