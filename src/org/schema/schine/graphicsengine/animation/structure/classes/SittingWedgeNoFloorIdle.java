package org.schema.schine.graphicsengine.animation.structure.classes;

public class SittingWedgeNoFloorIdle extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.SITTING_WEDGE_NOFLOOR_IDLE;
   }
}
