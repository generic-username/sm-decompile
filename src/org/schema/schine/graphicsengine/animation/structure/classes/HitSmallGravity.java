package org.schema.schine.graphicsengine.animation.structure.classes;

public class HitSmallGravity extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.HIT_SMALL_GRAVITY;
   }
}
