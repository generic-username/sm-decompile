package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class Helmet extends AnimationStructSet {
   public final HelmetOn helmetOn = new HelmetOn();
   public final HelmetOff helmetOff = new HelmetOff();

   public void checkAnimations(String var1) {
      if (!this.helmetOn.parsed) {
         this.helmetOn.parse((Node)null, var1, this);
      }

      this.children.add(this.helmetOn);
      if (!this.helmetOff.parsed) {
         this.helmetOff.parse((Node)null, var1, this);
      }

      this.children.add(this.helmetOff);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("on")) {
            this.helmetOn.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("off")) {
            this.helmetOff.parse(var1, var2, this);
         }
      }

   }
}
