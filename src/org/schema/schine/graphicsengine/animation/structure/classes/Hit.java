package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class Hit extends AnimationStructSet {
   public final HitSmall hitSmall = new HitSmall();

   public void checkAnimations(String var1) {
      if (!this.hitSmall.parsed) {
         this.hitSmall.parse((Node)null, var1, this);
      }

      this.children.add(this.hitSmall);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null && var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("small")) {
         this.hitSmall.parse(var1, var2, this);
      }

   }
}
