package org.schema.schine.graphicsengine.animation.structure.classes;

public class SittingBlockNoFloorIdle extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.SITTING_BLOCK_NOFLOOR_IDLE;
   }
}
