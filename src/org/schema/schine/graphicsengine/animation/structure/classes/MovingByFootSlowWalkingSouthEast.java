package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingByFootSlowWalkingSouthEast extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_BYFOOT_SLOWWALKING_SOUTHEAST;
   }
}
