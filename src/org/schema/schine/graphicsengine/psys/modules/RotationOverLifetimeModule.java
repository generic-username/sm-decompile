package org.schema.schine.graphicsengine.psys.modules;

import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Quat4Util;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.psys.ParticleContainer;
import org.schema.schine.graphicsengine.psys.ParticleSystemConfiguration;
import org.schema.schine.graphicsengine.psys.modules.iface.ParticleUpdateInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.PSCurveVariable;
import org.schema.schine.graphicsengine.psys.modules.variable.VarInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.XMLSerializable;

public class RotationOverLifetimeModule extends ParticleSystemModule implements ParticleUpdateInterface {
   @XMLSerializable(
      name = "rotation",
      type = "float"
   )
   float rotation = 0.1F;
   @XMLSerializable(
      name = "rotCurve",
      type = "curve"
   )
   PSCurveVariable rotCurve = new PSCurveVariable() {
      public String getName() {
         return "rotationCurve";
      }

      public Color getColor() {
         return Color.RED;
      }
   };

   public RotationOverLifetimeModule(ParticleSystemConfiguration var1) {
      super(var1);
   }

   protected JPanel getConfigPanel() {
      JPanel var1;
      (var1 = new JPanel()).setLayout(new GridBagLayout());
      this.addRow(var1, 0, new VarInterface() {
         public String getName() {
            return "multiplier";
         }

         public Float get() {
            return RotationOverLifetimeModule.this.rotation;
         }

         public void set(String var1) {
            RotationOverLifetimeModule.this.rotation = Float.parseFloat(var1);
         }

         public Float getDefault() {
            return 0.1F;
         }
      });
      this.addRow(var1, 1, new PSCurveVariable[]{this.rotCurve});
      return var1;
   }

   public String getName() {
      return "Rotation over Lifetime";
   }

   public void onParticleUpdate(Timer var1, ParticleContainer var2) {
      Quat4Util.mult(Quat4Util.fromAngleAxis(this.rotCurve.get(1.0F - var2.lifetime / var2.lifetimeTotal) * this.rotation, new Vector3f(0.0F, 0.0F, 1.0F), new Quat4f()), var2.rotation, var2.rotation);
   }
}
