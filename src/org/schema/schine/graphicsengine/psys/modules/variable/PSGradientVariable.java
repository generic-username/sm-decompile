package org.schema.schine.graphicsengine.psys.modules.variable;

import it.unimi.dsi.fastutil.floats.Float2ObjectRBTreeMap;
import it.unimi.dsi.fastutil.floats.Float2ObjectMap.Entry;
import java.awt.Color;
import java.util.Iterator;
import java.util.Locale;
import javax.vecmath.Vector4f;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class PSGradientVariable implements PSVariable {
   public Float2ObjectRBTreeMap color = new Float2ObjectRBTreeMap();
   public Vector4f c = new Vector4f();

   public PSGradientVariable() {
      this.init();
   }

   private static void parseColor(Node var0, PSGradientVariable var1) {
      float var2 = Float.parseFloat(var0.getAttributes().getNamedItem("percent").getNodeValue());
      NodeList var9 = var0.getChildNodes();
      int var3 = 0;
      int var4 = 0;
      int var5 = 0;
      int var6 = 0;

      for(int var7 = 0; var7 < var9.getLength(); ++var7) {
         Node var8;
         if ((var8 = var9.item(var7)).getNodeType() == 1) {
            if (var8.getNodeName().toLowerCase(Locale.ENGLISH).equals("red")) {
               var3 = Integer.parseInt(var8.getTextContent());
            }

            if (var8.getNodeName().toLowerCase(Locale.ENGLISH).equals("green")) {
               var4 = Integer.parseInt(var8.getTextContent());
            }

            if (var8.getNodeName().toLowerCase(Locale.ENGLISH).equals("blue")) {
               var5 = Integer.parseInt(var8.getTextContent());
            }

            if (var8.getNodeName().toLowerCase(Locale.ENGLISH).equals("alpha")) {
               var6 = Integer.parseInt(var8.getTextContent());
            }
         }
      }

      var1.color.put(var2, new Color(var3, var4, var5, var6));
   }

   public void init() {
      this.color.put(0.0F, Color.RED);
      this.color.put(1.0F, Color.BLUE);
   }

   public Vector4f get(float var1) {
      if (this.color.containsKey(var1)) {
         Color var7 = (Color)this.color.get(var1);
         this.c.set((float)var7.getRed() / 255.0F, (float)var7.getGreen() / 255.0F, (float)var7.getBlue() / 255.0F, (float)var7.getAlpha() / 255.0F);
         return this.c;
      } else {
         float var2 = 0.0F;
         float var3 = 1.0F;

         Entry var5;
         for(Iterator var4 = this.color.float2ObjectEntrySet().iterator(); var4.hasNext(); var2 = var5.getFloatKey()) {
            if ((var5 = (Entry)var4.next()).getFloatKey() >= var1) {
               var3 = var5.getFloatKey();
               break;
            }
         }

         Color var8 = (Color)this.color.get(var2);
         Color var9 = (Color)this.color.get(var3);
         if ((var2 = var3 - var2) == 0.0F) {
            this.c.set((float)var8.getRed() / 255.0F, (float)var8.getGreen() / 255.0F, (float)var8.getBlue() / 255.0F, (float)var8.getAlpha() / 255.0F);
            return this.c;
         } else {
            var1 /= var2;
            var2 = (float)var8.getRed() + var1 * (float)(var9.getRed() - var8.getRed());
            var3 = (float)var8.getGreen() + var1 * (float)(var9.getGreen() - var8.getGreen());
            float var6 = (float)var8.getBlue() + var1 * (float)(var9.getBlue() - var8.getBlue());
            var1 = (float)var8.getAlpha() + var1 * (float)(var9.getAlpha() - var8.getAlpha());
            this.c.set(var2 / 255.0F, var3 / 255.0F, var6 / 255.0F, var1 / 255.0F);
            return this.c;
         }
      }
   }

   public void set(PSVariable var1) {
      this.color.clear();
      this.color.putAll(((PSGradientVariable)var1).color);
   }

   public void appendXML(Object var1, Element var2) {
      Element var10 = var2.getOwnerDocument().createElement("Gradient");
      Iterator var3 = this.color.entrySet().iterator();

      while(var3.hasNext()) {
         java.util.Map.Entry var4 = (java.util.Map.Entry)var3.next();
         Element var5;
         (var5 = var2.getOwnerDocument().createElement("Color")).setAttribute("percent", String.valueOf(var4.getKey()));
         Element var6 = var2.getOwnerDocument().createElement("red");
         Element var7 = var2.getOwnerDocument().createElement("green");
         Element var8 = var2.getOwnerDocument().createElement("blue");
         Element var9 = var2.getOwnerDocument().createElement("alpha");
         var10.appendChild(var5);
         var5.appendChild(var6);
         var5.appendChild(var7);
         var5.appendChild(var8);
         var5.appendChild(var9);
         var6.setTextContent(String.valueOf(((Color)var4.getValue()).getRed()));
         var7.setTextContent(String.valueOf(((Color)var4.getValue()).getGreen()));
         var8.setTextContent(String.valueOf(((Color)var4.getValue()).getBlue()));
         var9.setTextContent(String.valueOf(((Color)var4.getValue()).getAlpha()));
      }

      var2.appendChild(var10);
   }

   public void parse(Node var1) {
      this.color.clear();
      NodeList var4 = var1.getChildNodes();

      for(int var2 = 0; var2 < var4.getLength(); ++var2) {
         Node var3;
         if ((var3 = var4.item(var2)).getNodeType() == 1 && var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("gradient")) {
            this.parseGradient(var3, this);
         }
      }

      if (this.color.size() < 2) {
         throw new IllegalArgumentException("Color gradient cannot be made from less then 2 colors");
      }
   }

   public abstract String getName();

   private void parseGradient(Node var1, PSGradientVariable var2) {
      NodeList var4 = var1.getChildNodes();

      for(int var5 = 0; var5 < var4.getLength(); ++var5) {
         Node var3;
         if ((var3 = var4.item(var5)).getNodeType() == 1 && var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("color")) {
            parseColor(var3, this);
         }
      }

   }
}
