package org.schema.schine.graphicsengine.psys.modules.variable;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public interface PSVariable {
   Object get(float var1);

   void set(PSVariable var1);

   void appendXML(Object var1, Element var2);

   void parse(Node var1);
}
