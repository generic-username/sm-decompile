package org.schema.schine.graphicsengine.psys.modules.variable;

import java.awt.Color;
import java.awt.geom.Point2D.Double;
import java.util.ArrayList;
import java.util.Locale;
import org.schema.schine.graphicsengine.spline.Spline2DAlt;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class PSCurveVariable implements PSVariable {
   private boolean useIntegral;
   private ArrayList points;
   private ArrayList pointsSecond;
   private float base;
   private Spline2DAlt sA;
   private Spline2DAlt sB;

   public PSCurveVariable() {
      this.points = new ArrayList();
      this.pointsSecond = new ArrayList();
      this.base = 1.0F;
      this.reset();

      assert this.sA != null;

      assert this.sB != null;

   }

   public PSCurveVariable(double var1) {
      this();
      this.base = (float)var1;
   }

   private static void parsePoints(Node var0, ArrayList var1) {
      NodeList var4 = var0.getChildNodes();

      for(int var2 = 0; var2 < var4.getLength(); ++var2) {
         Node var3;
         if ((var3 = var4.item(var2)).getNodeType() == 1 && var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("point")) {
            var1.add(parsePoint(var3));
         }
      }

   }

   private static Double parsePoint(Node var0) {
      Integer.parseInt(var0.getAttributes().getNamedItem("index").getNodeValue());
      NodeList var4 = var0.getChildNodes();
      Double var1 = new Double();

      for(int var2 = 0; var2 < var4.getLength(); ++var2) {
         Node var3;
         if ((var3 = var4.item(var2)).getNodeType() == 1) {
            if (var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("x")) {
               var1.x = (double)Float.parseFloat(var3.getTextContent());
            }

            if (var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("y")) {
               var1.y = (double)Float.parseFloat(var3.getTextContent());
            }
         }
      }

      return var1;
   }

   protected boolean valid() {
      return this.sA != null && this.sB != null;
   }

   public Float get(float var1) {
      assert this.sA != null;

      assert this.sB != null;

      if (this.useIntegral) {
         float var2 = (float)this.sA.getValueY((double)var1);
         float var3;
         if ((var1 = (float)this.sB.getValueY((double)var1)) > var2) {
            var3 = var2;
            var2 = var1;
            var1 = var3;
         }

         var3 = var2 - var1;
         var2 = (float)(Math.random() * (double)var3);
         var1 += var2;
         return this.getBase() * var1;
      } else {
         return (float)((double)this.getBase() * this.sA.getValueY((double)var1));
      }
   }

   public void set(PSVariable var1) {
   }

   public void appendXML(Object var1, Element var2) {
      var2.setAttribute("integral", String.valueOf(this.isUseIntegral()));
      var2.setAttribute("base", String.valueOf(this.getBase()));
      Element var8 = var2.getOwnerDocument().createElement("Points0");

      int var3;
      Double var4;
      Element var5;
      Element var6;
      Element var7;
      for(var3 = 0; var3 < this.getPoints().size(); ++var3) {
         var4 = (Double)this.getPoints().get(var3);
         (var5 = var2.getOwnerDocument().createElement("Point")).setAttribute("index", String.valueOf(var3));
         var6 = var2.getOwnerDocument().createElement("X");
         var7 = var2.getOwnerDocument().createElement("Y");
         var8.appendChild(var5);
         var5.appendChild(var6);
         var5.appendChild(var7);
         var6.setTextContent(String.valueOf((float)var4.x));
         var7.setTextContent(String.valueOf((float)var4.y));
      }

      var2.appendChild(var8);
      var8 = var2.getOwnerDocument().createElement("Points1");

      for(var3 = 0; var3 < this.getPointsSecond().size(); ++var3) {
         var4 = (Double)this.getPointsSecond().get(var3);
         (var5 = var2.getOwnerDocument().createElement("Point")).setAttribute("index", String.valueOf(var3));
         var6 = var2.getOwnerDocument().createElement("X");
         var7 = var2.getOwnerDocument().createElement("Y");
         var8.appendChild(var5);
         var5.appendChild(var6);
         var5.appendChild(var7);
         var6.setTextContent(String.valueOf((float)var4.x));
         var7.setTextContent(String.valueOf((float)var4.y));
      }

      var2.appendChild(var8);
   }

   public void parse(Node var1) {
      this.useIntegral = Boolean.parseBoolean(var1.getAttributes().getNamedItem("integral").getNodeValue());
      this.base = Float.parseFloat(var1.getAttributes().getNamedItem("base").getNodeValue());
      this.getPoints().clear();
      if (this.useIntegral) {
         this.getPointsSecond().clear();
      }

      NodeList var4 = var1.getChildNodes();

      for(int var2 = 0; var2 < var4.getLength(); ++var2) {
         Node var3;
         if ((var3 = var4.item(var2)).getNodeType() == 1) {
            System.err.println("PARSING: " + var3.getNodeName());
            if (var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("points0")) {
               parsePoints(var3, this.getPoints());
            }

            if (var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("points1")) {
               parsePoints(var3, this.getPointsSecond());
            }
         }
      }

      this.revalidate();
   }

   public abstract String getName();

   public abstract Color getColor();

   public String toString() {
      return this.getName();
   }

   public boolean isUseIntegral() {
      return this.useIntegral;
   }

   public void setUseIntegral(boolean var1) {
      this.useIntegral = var1;
   }

   public void reset() {
      this.getPoints().clear();
      this.getPointsSecond().clear();
      this.initPoints();
      this.revalidate();
   }

   public void initPoints() {
      this.getPoints().add(new Double(0.0D, 1.0D));
      this.getPoints().add(new Double(1.0D, 1.0D));
      this.getPointsSecond().add(new Double(0.0D, 0.0D));
      this.getPointsSecond().add(new Double(1.0D, 0.0D));
      this.revalidate();
   }

   public float getBase() {
      return this.base;
   }

   public void setBase(float var1) {
      this.base = var1;
   }

   public void revalidate() {
      if (this.points.size() < 2) {
         throw new IllegalArgumentException("points0 have to be more then 1, but are " + this.points.size());
      } else if (this.pointsSecond.size() < 2) {
         throw new IllegalArgumentException("points1 have to be more then 1, but are " + this.pointsSecond.size());
      } else {
         this.sA = new Spline2DAlt(this.getPoints());
         this.sB = new Spline2DAlt(this.getPointsSecond());
      }
   }

   public ArrayList getPoints() {
      return this.points;
   }

   public void setPoints(ArrayList var1) {
      this.points = var1;
      this.revalidate();
   }

   public ArrayList getPointsSecond() {
      return this.pointsSecond;
   }

   public void setPointsSecond(ArrayList var1) {
      this.pointsSecond = var1;
      this.revalidate();
   }
}
