package org.schema.schine.graphicsengine.psys.modules.variable;

public class StringPair {
   public String string;
   public int val;

   public StringPair(String var1, int var2) {
      this.string = var1;
      this.val = var2;
   }

   public String toString() {
      return this.string;
   }
}
