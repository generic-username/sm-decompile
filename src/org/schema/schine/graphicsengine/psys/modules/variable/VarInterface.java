package org.schema.schine.graphicsengine.psys.modules.variable;

public interface VarInterface {
   String getName();

   Object get();

   void set(String var1);

   Object getDefault();
}
