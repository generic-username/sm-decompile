package org.schema.schine.graphicsengine.psys.modules.variable;

public interface BooleanInterface {
   boolean get();

   void set(boolean var1);

   String getName();
}
