package org.schema.schine.graphicsengine.psys.modules;

import it.unimi.dsi.fastutil.floats.Float2IntOpenHashMap;
import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.psys.ParticleSystemConfiguration;
import org.schema.schine.graphicsengine.psys.modules.variable.BooleanInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.DropDownInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.PSCurveVariable;
import org.schema.schine.graphicsengine.psys.modules.variable.StringPair;
import org.schema.schine.graphicsengine.psys.modules.variable.VarInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.XMLSerializable;

public class EmissionModule extends ParticleSystemModule {
   public static int RATE_BY_SECOND = 0;
   public static int RATE_BY_METER = 1;
   @XMLSerializable(
      name = "useCurve",
      type = "boolean"
   )
   boolean useCurve;
   @XMLSerializable(
      name = "type",
      type = "int"
   )
   int type;
   Float2IntOpenHashMap bursts = new Float2IntOpenHashMap();
   @XMLSerializable(
      name = "rate",
      type = "int"
   )
   private int rate = 10;
   @XMLSerializable(
      name = "rateCurve",
      type = "curve"
   )
   private PSCurveVariable rateCurve = new PSCurveVariable() {
      public String getName() {
         return "rate curve";
      }

      public Color getColor() {
         return Color.BLUE;
      }
   };
   private long nextSpawn = 0L;

   public EmissionModule(ParticleSystemConfiguration var1) {
      super(var1);
   }

   protected JPanel getConfigPanel() {
      JPanel var1;
      (var1 = new JPanel()).setLayout(new GridBagLayout());
      this.addRow(var1, 0, new VarInterface() {
         public String getName() {
            return "rate";
         }

         public Integer get() {
            return EmissionModule.this.rate;
         }

         public void set(String var1) {
            EmissionModule.this.rate = Math.max(0, Integer.parseInt(var1));
         }

         public Integer getDefault() {
            return 10;
         }
      });
      this.addRow(var1, 1, new DropDownInterface(new StringPair[]{new StringPair("per second", RATE_BY_SECOND), new StringPair("per meter", RATE_BY_METER)}) {
         public String getName() {
            return "Type";
         }

         public int getCurrentIndex() {
            return EmissionModule.this.type;
         }

         public void set(StringPair var1) {
            EmissionModule.this.type = var1.val;
         }
      });
      this.addRow(var1, 2, new BooleanInterface() {
         public boolean get() {
            return EmissionModule.this.useCurve;
         }

         public void set(boolean var1) {
            EmissionModule.this.useCurve = var1;
         }

         public String getName() {
            return "use curve";
         }
      });
      this.addRow(var1, 3, new PSCurveVariable[]{this.rateCurve});
      return var1;
   }

   public String getName() {
      return "Emission";
   }

   public int getParticlesToSpawn(Timer var1) {
      if (this.rate <= 0) {
         return 0;
      } else {
         int var2;
         if (!this.useCurve) {
            var2 = 0;
            if (var1.currentTime >= this.nextSpawn) {
               var2 = Math.min(this.sys.getMaxParticles() - this.sys.getParticleCount(), 1);
               this.nextSpawn = var1.currentTime + 1000L / (long)this.rate;
            }

            return var2;
         } else {
            var2 = 0;
            if (var1.currentTime >= this.nextSpawn) {
               var2 = Math.min(this.sys.getMaxParticles() - this.sys.getParticleCount(), 1);
               float var3;
               if ((var3 = (float)this.rate * this.rateCurve.get(this.sys.getSystemTime() / this.sys.getParticleSystemDuration())) != 0.0F) {
                  this.nextSpawn = var1.currentTime + (long)((int)Math.floor((double)(1000.0F / var3)));
               } else {
                  this.nextSpawn = -1L;
               }
            }

            return var2;
         }
      }
   }
}
