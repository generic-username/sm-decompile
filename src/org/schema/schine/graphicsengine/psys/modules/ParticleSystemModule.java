package org.schema.schine.graphicsengine.psys.modules;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Locale;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.border.LineBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import org.schema.schine.graphicsengine.psys.ParticleSystemConfiguration;
import org.schema.schine.graphicsengine.psys.modules.variable.BooleanInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.DropDownInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.PSCurveVariable;
import org.schema.schine.graphicsengine.psys.modules.variable.PSGradientVariable;
import org.schema.schine.graphicsengine.psys.modules.variable.PSVariable;
import org.schema.schine.graphicsengine.psys.modules.variable.StringPair;
import org.schema.schine.graphicsengine.psys.modules.variable.VarInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.XMLSerializable;
import org.schema.schine.tools.curve.SplineDisplay;
import org.schema.schine.tools.gradient.LinearGradientChooser;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class ParticleSystemModule {
   public static final int SPACE_WORLD = 0;
   public static final int SPACE_LOCAL = 1;
   public static int QUALITY_LOW = 0;
   public static int QUALITY_MID = 1;
   public static int QUALITY_HIGH = 2;
   protected final ParticleSystemConfiguration sys;
   private boolean enabled;

   public ParticleSystemModule(ParticleSystemConfiguration var1) {
      this.sys = var1;
   }

   public boolean isEnabled() {
      return this.enabled;
   }

   public boolean canDisable() {
      return true;
   }

   public void setEnabled(boolean var1) {
      if (var1 != this.enabled) {
         this.sys.setModuleEnabledChanged(true);
      }

      this.enabled = var1;
   }

   public Element serialize(Document var1) throws IllegalArgumentException, IllegalAccessException {
      System.out.println("[XML] Writing module: [" + this.getName() + "] as [" + this.getTagName() + "]");
      Element var7;
      (var7 = var1.createElement(this.getTagName())).setAttribute("enabled", String.valueOf(this.enabled));
      Field[] var2 = this.getClass().getDeclaredFields();

      for(int var3 = 0; var3 < var2.length; ++var3) {
         var2[var3].setAccessible(true);
         Annotation[] var4 = var2[var3].getAnnotations();

         for(int var5 = 0; var5 < var4.length; ++var5) {
            if (var4[var5] instanceof XMLSerializable) {
               XMLSerializable var6;
               if ((var6 = (XMLSerializable)var4[var5]).type().toLowerCase(Locale.ENGLISH).equals("int")) {
                  this.append(var6.name(), var2[var3].getInt(this), var7);
               } else if (var6.type().toLowerCase(Locale.ENGLISH).equals("float")) {
                  this.append(var6.name(), var2[var3].getFloat(this), var7);
               } else if (var6.type().toLowerCase(Locale.ENGLISH).equals("boolean")) {
                  this.append(var6.name(), var2[var3].getBoolean(this), var7);
               } else if (var6.type().toLowerCase(Locale.ENGLISH).equals("gradient")) {
                  this.append(var6.name(), var2[var3].get(this), var7);
               } else if (var6.type().toLowerCase(Locale.ENGLISH).equals("curve")) {
                  this.append(var6.name(), var2[var3].get(this), var7);
               } else if (var6.type().toLowerCase(Locale.ENGLISH).equals("string")) {
                  this.append(var6.name(), var2[var3].get(this), var7);
               } else {
                  if (!var6.type().toLowerCase(Locale.ENGLISH).equals("long")) {
                     throw new IllegalArgumentException(var6.type());
                  }

                  this.append(var6.name(), var2[var3].get(this), var7);
               }
            }
         }
      }

      return var7;
   }

   public String getTagName() {
      return this.getName().replaceAll(" ", "");
   }

   public void deserialize(Node var1) throws NumberFormatException, IllegalArgumentException, IllegalAccessException, DOMException {
      this.enabled = Boolean.parseBoolean(var1.getAttributes().getNamedItem("enabled").getNodeValue());
      NodeList var10 = var1.getChildNodes();
      Field[] var2 = this.getClass().getDeclaredFields();

      for(int var3 = 0; var3 < var10.getLength(); ++var3) {
         Node var4;
         if ((var4 = var10.item(var3)).getNodeType() == 1) {
            for(int var5 = 0; var5 < var2.length; ++var5) {
               var2[var5].setAccessible(true);
               Annotation[] var6 = var2[var5].getAnnotations();

               for(int var7 = 0; var7 < var6.length; ++var7) {
                  if (var6[var7] instanceof XMLSerializable) {
                     try {
                        XMLSerializable var8 = (XMLSerializable)var6[var7];
                        if (var4.getNodeName().toLowerCase(Locale.ENGLISH).equals(var8.name().toLowerCase(Locale.ENGLISH))) {
                           if (var8.type().toLowerCase(Locale.ENGLISH).equals("int")) {
                              var2[var5].setInt(this, Integer.parseInt(var4.getTextContent()));
                           } else if (var8.type().toLowerCase(Locale.ENGLISH).equals("float")) {
                              var2[var5].setFloat(this, Float.parseFloat(var4.getTextContent()));
                           } else if (var8.type().toLowerCase(Locale.ENGLISH).equals("boolean")) {
                              var2[var5].setBoolean(this, Boolean.parseBoolean(var4.getTextContent()));
                           } else if (var8.type().toLowerCase(Locale.ENGLISH).equals("gradient")) {
                              ((PSVariable)var2[var5].get(this)).parse(var4);
                           } else if (var8.type().toLowerCase(Locale.ENGLISH).equals("curve")) {
                              ((PSVariable)var2[var5].get(this)).parse(var4);
                           } else if (var8.type().toLowerCase(Locale.ENGLISH).equals("string")) {
                              var2[var5].set(this, var4.getTextContent());
                           } else {
                              if (!var8.type().toLowerCase(Locale.ENGLISH).equals("long")) {
                                 throw new IllegalArgumentException(var8.type());
                              }

                              var2[var5].setLong(this, Long.parseLong(var4.getTextContent()));
                           }
                        }
                     } catch (RuntimeException var9) {
                        var9.printStackTrace();
                        System.err.println("Exception happened on node: " + var4.getNodeName());
                        throw var9;
                     }
                  }
               }
            }
         }
      }

   }

   protected void append(String var1, Object var2, Element var3) {
      Element var4 = var3.getOwnerDocument().createElement(var1);
      if (var2 instanceof PSVariable) {
         ((PSVariable)var2).appendXML(var2, var4);
      } else {
         var4.setTextContent(var2.toString());
      }

      var3.appendChild(var4);
   }

   protected void addRow(JPanel var1, int var2, String var3, JComponent var4) {
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).weighty = 0.0D;
      var5.weightx = 1.0D;
      var5.anchor = 17;
      var5.fill = 0;
      var5.gridx = 0;
      var5.gridy = var2;
      var1.add(new JLabel(var3), var5);
      (var5 = new GridBagConstraints()).weighty = 0.0D;
      var5.weightx = 1.0D;
      var5.anchor = 13;
      var5.fill = 0;
      var5.gridx = 1;
      var5.gridy = var2;
      var1.add(var4, var5);
   }

   protected void addRowTitledBorder(JPanel var1, int var2, String[] var3, Color[] var4, JComponent var5) {
      JPanel var6;
      (var6 = new JPanel()).setLayout(new GridBagLayout());
      var6.setBorder(LineBorder.createBlackLineBorder());
      JPanel var7;
      (var7 = new JPanel()).setLayout(new GridBagLayout());

      for(int var8 = 0; var8 < var3.length; ++var8) {
         JLabel var9;
         (var9 = new JLabel(var3[var8])).setForeground(var4[var8]);
         GridBagConstraints var10;
         (var10 = new GridBagConstraints()).weighty = 0.0D;
         var10.weightx = 0.0D;
         var10.insets = new Insets(0, 0, 3, 10);
         var10.anchor = 18;
         var10.fill = 2;
         var10.gridx = var8;
         var10.gridy = 0;
         var7.add(var9, var10);
      }

      GridBagConstraints var12;
      (var12 = new GridBagConstraints()).weighty = 0.0D;
      var12.weightx = 0.0D;
      var12.anchor = 16;
      var12.fill = 0;
      var12.gridx = 0;
      var12.gridy = 0;
      var6.add(var7, var12);
      (var12 = new GridBagConstraints()).weighty = 0.0D;
      var12.weightx = 0.0D;
      var12.anchor = 13;
      var12.fill = 1;
      var12.gridx = 0;
      var12.gridy = 1;
      var6.add(var5, var12);
      GridBagConstraints var11;
      (var11 = new GridBagConstraints()).weighty = 0.0D;
      var11.weightx = 1.0D;
      var11.anchor = 13;
      var11.fill = 1;
      var11.gridx = 1;
      var11.gridwidth = 1;
      var11.gridy = var2;
      var1.add(var6, var11);
   }

   public void addRow(JPanel var1, int var2, final DropDownInterface var3) {
      final JComboBox var4;
      (var4 = new JComboBox(var3)).setPreferredSize(new Dimension(150, 24));
      var4.setSelectedIndex(var3.getCurrentIndex());
      var4.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            if (var4.getSelectedItem() != null) {
               var3.set((StringPair)var4.getSelectedItem());
            }

         }
      });
      this.addRow(var1, var2, var3.getName(), var4);
   }

   public void addRow(JPanel var1, int var2, PSGradientVariable var3) {
      LinearGradientChooser var4 = new LinearGradientChooser(var3);
      this.addRowTitledBorder(var1, var2, new String[]{var3.getName()}, new Color[]{Color.BLACK}, var4);
   }

   public void addRow(JPanel var1, int var2, PSCurveVariable... var3) {
      final SplineDisplay var4 = new SplineDisplay(var3);
      String[] var5 = new String[var3.length];
      Color[] var6 = new Color[var3.length];

      for(int var7 = 0; var7 < var3.length; ++var7) {
         var5[var7] = var3[var7].getName();
         var6[var7] = var3[var7].getColor();
      }

      JLabel var13;
      (var13 = new JLabel()).setLayout(new GridBagLayout());

      for(int var8 = 0; var8 < var3.length; ++var8) {
         final PSCurveVariable var9 = var3[var8];
         final JCheckBox var10 = new JCheckBox("IN");
         final JTextField var11 = new JTextField(var9.getBase() + "     ");
         JButton var12 = new JButton("X");
         var10.setForeground(var9.getColor());
         var12.setForeground(var9.getColor());
         var11.addCaretListener(new CaretListener() {
            public void caretUpdate(CaretEvent var1) {
               try {
                  var9.setBase(Float.parseFloat(var11.getText()));
               } catch (NumberFormatException var2) {
                  var11.setText("1.0");
               }
            }
         });
         var10.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
               var9.setUseIntegral(var10.isSelected());
               var4.repaint();
            }
         });
         var12.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
               var9.reset();
               var4.repaint();
            }
         });
         GridBagConstraints var15;
         (var15 = new GridBagConstraints()).weighty = 0.0D;
         var15.weightx = 0.0D;
         var15.insets = new Insets(0, 0, 3, 3);
         var15.anchor = 18;
         var15.fill = 2;
         var15.gridx = 0;
         var15.gridy = var8;
         var13.add(var10, var15);
         (var15 = new GridBagConstraints()).weighty = 0.0D;
         var15.weightx = 0.0D;
         var15.insets = new Insets(0, 0, 3, 3);
         var15.anchor = 18;
         var15.fill = 2;
         var15.gridx = 1;
         var15.gridy = var8;
         var13.add(var11, var15);
         (var15 = new GridBagConstraints()).weighty = 0.0D;
         var15.weightx = 0.0D;
         var15.insets = new Insets(0, 0, 3, 3);
         var15.anchor = 18;
         var15.fill = 2;
         var15.gridx = 2;
         var15.gridy = var8;
         var13.add(var12, var15);
      }

      GridBagConstraints var14;
      (var14 = new GridBagConstraints()).weighty = 0.0D;
      var14.weightx = 0.0D;
      var14.anchor = 18;
      var14.fill = 1;
      var14.gridx = 0;
      var14.gridy = var2;
      var1.add(var13, var14);
      this.addRowTitledBorder(var1, var2, var5, var6, var4);
   }

   public void addRow(JPanel var1, int var2, final BooleanInterface var3) {
      final JCheckBox var4;
      (var4 = new JCheckBox()).setSelected(var3.get());
      var4.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            var3.set(var4.isSelected());
         }
      });
      this.addRow(var1, var2, var3.getName(), var4);
   }

   public void addRow(JPanel var1, int var2, final VarInterface var3) {
      final JTextField var4;
      (var4 = new JTextField()).setText(String.valueOf(var3.get()));
      var4.setColumns(18);
      var4.addCaretListener(new CaretListener() {
         public void caretUpdate(CaretEvent var1) {
            try {
               var3.set(var4.getText());
               System.err.println("input set to " + var3.get());
            } catch (Exception var2) {
               var2.printStackTrace();
               var4.setText(String.valueOf(var3.getDefault()));
            }
         }
      });
      this.addRow(var1, var2, var3.getName(), var4);
   }

   protected abstract JPanel getConfigPanel();

   private JPanel getTabPanel() {
      final JPanel var1;
      (var1 = new JPanel()).setLayout(new GridBagLayout());
      var1.setBorder(LineBorder.createBlackLineBorder());
      GridBagConstraints var2;
      (var2 = new GridBagConstraints()).weighty = 0.0D;
      var2.weightx = 0.0D;
      var2.anchor = 17;
      var2.fill = 0;
      var2.gridx = 0;
      var2.gridy = 0;
      final JCheckBox var3 = new JCheckBox();
      var1.add(var3, var2);
      var3.setSelected(this.isEnabled());
      if (!this.canDisable()) {
         this.setEnabled(true);
         var3.setSelected(true);
         var3.setEnabled(false);
      }

      var3.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            if (ParticleSystemModule.this.canDisable()) {
               ParticleSystemModule.this.setEnabled(var3.isSelected());
               var3.setSelected(ParticleSystemModule.this.isEnabled());
            }

         }
      });
      (var2 = new GridBagConstraints()).weighty = 0.0D;
      var2.weightx = 1.0D;
      var2.anchor = 17;
      var2.fill = 0;
      var2.gridx = 1;
      var2.gridy = 0;
      var1.add(new JLabel(this.getName()), var2);
      (var2 = new GridBagConstraints()).weighty = 0.0D;
      var2.weightx = 1.0D;
      var2.anchor = 13;
      var2.fill = 0;
      var2.gridx = 2;
      var2.gridy = 0;
      final JToggleButton var4;
      (var4 = new JToggleButton("Show")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            if (var4.isSelected()) {
               var4.setText("Hide");
               var1.firePropertyChange("hidden", false, true);
            } else {
               var4.setText("Show");
               var1.firePropertyChange("hidden", true, false);
            }
         }
      });
      var1.add(var4, var2);
      return var1;
   }

   public String getName() {
      return "undefined(" + this.getClass().getSimpleName() + ")";
   }

   public JPanel getPanel() {
      final JPanel var1;
      (var1 = new JPanel()).setBorder(LineBorder.createBlackLineBorder());
      var1.setLayout(new GridBagLayout());
      JPanel var2;
      (var2 = this.getTabPanel()).setBackground(Color.LIGHT_GRAY);
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).weighty = 0.0D;
      var3.weightx = 1.0D;
      var3.anchor = 17;
      var3.fill = 2;
      var3.gridx = 0;
      var3.gridy = 0;
      var1.add(var2, var3);
      final JPanel var4 = this.getConfigPanel();
      var2.addPropertyChangeListener("hidden", new PropertyChangeListener() {
         public void propertyChange(PropertyChangeEvent var1x) {
            if (!(Boolean)var1x.getNewValue()) {
               var1.remove(var4);
            } else {
               GridBagConstraints var2;
               (var2 = new GridBagConstraints()).weighty = 0.0D;
               var2.weightx = 1.0D;
               var2.fill = 1;
               var2.gridx = 0;
               var2.gridy = 1;
               var1.add(var4, var2);
            }
         }
      });
      return var1;
   }
}
