package org.schema.schine.graphicsengine.psys.modules;

import com.bulletphysics.linearmath.Transform;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.psys.ParticleContainer;
import org.schema.schine.graphicsengine.psys.ParticleSystemConfiguration;
import org.schema.schine.graphicsengine.psys.modules.iface.ParticleDeathInterface;
import org.schema.schine.graphicsengine.psys.modules.iface.ParticleMoveInterface;
import org.schema.schine.graphicsengine.psys.modules.iface.ParticleStartInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.DropDownInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.StringPair;
import org.schema.schine.graphicsengine.psys.modules.variable.VarInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.XMLSerializable;
import org.schema.schine.physics.ClosestRayCastResultExt;
import org.schema.schine.physics.Physics;

public class CollisionModule extends ParticleSystemModule implements ParticleDeathInterface, ParticleMoveInterface, ParticleStartInterface {
   private final Vector3f velocityTmp = new Vector3f();
   @XMLSerializable(
      name = "dampen",
      type = "float"
   )
   float dampen;
   @XMLSerializable(
      name = "bounce",
      type = "float"
   )
   float bounce;
   @XMLSerializable(
      name = "lifetimeLoss",
      type = "float"
   )
   float lifetimeLoss;
   @XMLSerializable(
      name = "minKillSpeed",
      type = "float"
   )
   float minKillSpeed;
   @XMLSerializable(
      name = "particleRadius",
      type = "float"
   )
   float particleRadius;
   @XMLSerializable(
      name = "quality",
      type = "int"
   )
   int quality;

   public CollisionModule(ParticleSystemConfiguration var1) {
      super(var1);
      this.quality = QUALITY_MID;
   }

   protected JPanel getConfigPanel() {
      JPanel var1;
      (var1 = new JPanel()).setLayout(new GridBagLayout());
      this.addRow(var1, 0, new VarInterface() {
         public String getName() {
            return "dampen";
         }

         public Float get() {
            return CollisionModule.this.dampen;
         }

         public void set(String var1) {
            CollisionModule.this.dampen = Math.max(0.0F, Math.min(1.0F, Float.parseFloat(var1)));
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      this.addRow(var1, 1, new VarInterface() {
         public String getName() {
            return "bounce";
         }

         public Float get() {
            return CollisionModule.this.bounce;
         }

         public void set(String var1) {
            CollisionModule.this.bounce = Math.max(0.0F, Math.min(1.0F, Float.parseFloat(var1)));
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      this.addRow(var1, 2, new VarInterface() {
         public String getName() {
            return "lifetime loss";
         }

         public Float get() {
            return CollisionModule.this.lifetimeLoss;
         }

         public void set(String var1) {
            CollisionModule.this.lifetimeLoss = Math.max(0.0F, Math.min(1.0F, Float.parseFloat(var1)));
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      this.addRow(var1, 3, new VarInterface() {
         public String getName() {
            return "Min kill speed";
         }

         public Float get() {
            return CollisionModule.this.minKillSpeed;
         }

         public void set(String var1) {
            CollisionModule.this.minKillSpeed = Math.max(0.0F, Math.min(1000.0F, Float.parseFloat(var1)));
         }

         public Float getDefault() {
            return 0.0F;
         }
      });
      this.addRow(var1, 4, new VarInterface() {
         public String getName() {
            return "Particle Radius";
         }

         public Float get() {
            return CollisionModule.this.particleRadius;
         }

         public void set(String var1) {
            CollisionModule.this.particleRadius = Math.max(0.0F, Math.min(1000.0F, Float.parseFloat(var1)));
         }

         public Float getDefault() {
            return 0.0F;
         }
      });
      this.addRow(var1, 5, new DropDownInterface(new StringPair[]{new StringPair("low", QUALITY_LOW), new StringPair("middle", QUALITY_MID), new StringPair("high", QUALITY_HIGH)}) {
         public String getName() {
            return "Quality";
         }

         public int getCurrentIndex() {
            return CollisionModule.this.quality;
         }

         public void set(StringPair var1) {
            CollisionModule.this.quality = var1.val;
         }
      });
      return var1;
   }

   public String getName() {
      return "Collision";
   }

   public void onParticleSpawn(ParticleContainer var1, Transform var2) {
   }

   public void onParticleMove(Physics var1, Timer var2, ParticleContainer var3) {
      Vector3f var4;
      (var4 = new Vector3f(var3.velocity)).normalize();
      this.velocityTmp.set(var3.velocity);
      this.velocityTmp.scale(var2.getDelta());
      this.velocityTmp.add(var3.position);
      if (this.velocityTmp.lengthSquared() != var3.position.lengthSquared()) {
         var4.scale(this.particleRadius);
         this.velocityTmp.add(var4);
         ClosestRayCastResultExt var7 = new ClosestRayCastResultExt(var3.position, this.velocityTmp) {
            public Object newInnerSegmentIterator() {
               return null;
            }
         };
         var1.getDynamicsWorld().rayTest(var3.position, this.velocityTmp, var7);
         if (var7.hasHit()) {
            Vector3f var6;
            (var6 = var7.hitPointWorld).sub(var3.position);
            float var5 = var6.length() / this.velocityTmp.length();
            var6.scale(var2.getDelta());
            var3.position.add(var6);
            (var6 = var7.hitNormalWorld).normalize();
            (var4 = new Vector3f()).set(var3.velocity.x * (1.0F - var5), var3.velocity.y * (1.0F - var5), var3.velocity.z * (1.0F - var5));
            var4.scale(var2.getDelta());
            if (this.bounce > 0.0F) {
               var5 = var4.dot(var6) * 2.0F;
               var6.scale(var5);
               var4.sub(var6);
               var4.scale(1.0F / var2.getDelta());
               var4.scale(this.bounce);
               var3.velocity.set(var4);
            } else {
               var6.scale(-1.0F);
               var6.scale(var4.dot(var6));
               var4.sub(var6);
               var4.scale(1.0F / var2.getDelta());
               var4.scale(this.dampen);
               var3.velocity.set(var4);
            }

            this.velocityTmp.set(var3.velocity);
            this.velocityTmp.scale(var2.getDelta());
            var3.position.add(this.velocityTmp);
         } else {
            this.velocityTmp.set(var3.velocity);
            this.velocityTmp.scale(var2.getDelta());
            var3.position.add(this.velocityTmp);
         }
      }
   }

   public void onParticleDeath(ParticleContainer var1) {
   }
}
