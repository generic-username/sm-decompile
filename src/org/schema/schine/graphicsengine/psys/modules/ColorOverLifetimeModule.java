package org.schema.schine.graphicsengine.psys.modules;

import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.psys.ParticleContainer;
import org.schema.schine.graphicsengine.psys.ParticleSystemConfiguration;
import org.schema.schine.graphicsengine.psys.modules.iface.ParticleColorInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.PSGradientVariable;
import org.schema.schine.graphicsengine.psys.modules.variable.XMLSerializable;

public class ColorOverLifetimeModule extends ParticleSystemModule implements ParticleColorInterface {
   @XMLSerializable(
      name = "color",
      type = "gradient"
   )
   PSGradientVariable color = new PSGradientVariable() {
      public void init() {
         this.color.put(0.0F, new Color(255, 255, 255, 254));
         this.color.put(1.0F, new Color(255, 255, 255, 200));
      }

      public String getName() {
         return "color";
      }
   };

   public ColorOverLifetimeModule(ParticleSystemConfiguration var1) {
      super(var1);
      this.setEnabled(true);
   }

   protected JPanel getConfigPanel() {
      JPanel var1;
      (var1 = new JPanel()).setLayout(new GridBagLayout());
      this.addRow(var1, 0, this.color);
      return var1;
   }

   public String getName() {
      return "Color over Lifetime";
   }

   public void onParticleColor(Vector4f var1, ParticleContainer var2) {
      Vector4f var3 = this.color.get(1.0F - var2.lifetime / var2.lifetimeTotal);
      var1.x *= var3.x;
      var1.y *= var3.y;
      var1.z *= var3.z;
      var1.w *= var3.w;
   }
}
