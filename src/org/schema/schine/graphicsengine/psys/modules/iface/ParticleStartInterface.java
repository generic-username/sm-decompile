package org.schema.schine.graphicsengine.psys.modules.iface;

import com.bulletphysics.linearmath.Transform;
import org.schema.schine.graphicsengine.psys.ParticleContainer;

public interface ParticleStartInterface {
   void onParticleSpawn(ParticleContainer var1, Transform var2);
}
