package org.schema.schine.graphicsengine.psys.modules.iface;

import org.schema.schine.graphicsengine.psys.ParticleContainer;

public interface ParticleDeathInterface {
   void onParticleDeath(ParticleContainer var1);
}
