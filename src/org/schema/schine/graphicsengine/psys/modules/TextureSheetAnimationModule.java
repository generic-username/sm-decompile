package org.schema.schine.graphicsengine.psys.modules;

import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import org.schema.schine.graphicsengine.psys.ParticleSystemConfiguration;
import org.schema.schine.graphicsengine.psys.modules.variable.BooleanInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.PSCurveVariable;
import org.schema.schine.graphicsengine.psys.modules.variable.VarInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.XMLSerializable;

public class TextureSheetAnimationModule extends ParticleSystemModule {
   @XMLSerializable(
      name = "singleRow",
      type = "boolean"
   )
   boolean singleRow;
   @XMLSerializable(
      name = "randomRow",
      type = "boolean"
   )
   boolean randomRow;
   @XMLSerializable(
      name = "frameOverTime",
      type = "curve"
   )
   PSCurveVariable frameOverTime = new PSCurveVariable() {
      public String getName() {
         return "frame over time";
      }

      public Color getColor() {
         return Color.RED;
      }
   };
   @XMLSerializable(
      name = "cycles",
      type = "float"
   )
   float cycles;

   public TextureSheetAnimationModule(ParticleSystemConfiguration var1) {
      super(var1);
   }

   protected JPanel getConfigPanel() {
      JPanel var1;
      (var1 = new JPanel()).setLayout(new GridBagLayout());
      this.addRow(var1, 0, new BooleanInterface() {
         public boolean get() {
            return TextureSheetAnimationModule.this.singleRow;
         }

         public void set(boolean var1) {
            TextureSheetAnimationModule.this.singleRow = var1;
         }

         public String getName() {
            return "use single row";
         }
      });
      this.addRow(var1, 1, new PSCurveVariable[]{this.frameOverTime});
      this.addRow(var1, 2, new VarInterface() {
         public String getName() {
            return "cycles";
         }

         public Float get() {
            return TextureSheetAnimationModule.this.cycles;
         }

         public void set(String var1) {
            TextureSheetAnimationModule.this.cycles = Math.max(0.0F, Float.parseFloat(var1));
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      return var1;
   }

   public String getName() {
      return "Texture Sheet Animation";
   }
}
