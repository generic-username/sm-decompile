package org.schema.schine.graphicsengine.psys.modules;

import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.psys.ParticleContainer;
import org.schema.schine.graphicsengine.psys.ParticleSystemConfiguration;
import org.schema.schine.graphicsengine.psys.modules.iface.ParticleColorInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.PSGradientVariable;
import org.schema.schine.graphicsengine.psys.modules.variable.VarInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.XMLSerializable;

public class ColorBySpeedModule extends ParticleSystemModule implements ParticleColorInterface {
   @XMLSerializable(
      name = "color",
      type = "gradient"
   )
   PSGradientVariable color = new PSGradientVariable() {
      public String getName() {
         return "color";
      }
   };
   @XMLSerializable(
      name = "minSpeed",
      type = "float"
   )
   float minSpeed = 0.0F;
   @XMLSerializable(
      name = "maxSpeed",
      type = "float"
   )
   float maxSpeed = 1.0F;

   public ColorBySpeedModule(ParticleSystemConfiguration var1) {
      super(var1);
   }

   protected JPanel getConfigPanel() {
      JPanel var1;
      (var1 = new JPanel()).setLayout(new GridBagLayout());
      this.addRow(var1, 0, this.color);
      this.addRow(var1, 1, new VarInterface() {
         public String getName() {
            return "min speed";
         }

         public Float get() {
            return ColorBySpeedModule.this.minSpeed;
         }

         public void set(String var1) {
            ColorBySpeedModule.this.minSpeed = Math.max(0.0F, Math.min(1.0F, Float.parseFloat(var1)));
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      this.addRow(var1, 2, new VarInterface() {
         public String getName() {
            return "max speed";
         }

         public Float get() {
            return ColorBySpeedModule.this.maxSpeed;
         }

         public void set(String var1) {
            ColorBySpeedModule.this.maxSpeed = Math.max(0.0F, Math.min(1.0F, Float.parseFloat(var1)));
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      return var1;
   }

   public String getName() {
      return "Color By Speed";
   }

   public void onParticleColor(Vector4f var1, ParticleContainer var2) {
      if (this.maxSpeed - this.minSpeed != 0.0F) {
         Vector4f var3 = this.color.get(Math.min(this.maxSpeed, Math.max(this.minSpeed, var2.velocity.length())) / (this.maxSpeed - this.minSpeed));
         var1.x *= var3.x;
         var1.y *= var3.y;
         var1.z *= var3.z;
         var1.w *= var3.w;
      }

   }
}
