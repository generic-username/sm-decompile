package org.schema.schine.graphicsengine.psys;

import javax.vecmath.Vector3f;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;
import org.schema.schine.graphicsengine.core.Controller;

public class Particle {
   private static final Vector4f pos = new Vector4f();
   private static final Vector4f res = new Vector4f();

   public static void getParticle(int var0, float[] var1, ParticleContainer var2) {
      ParticleProperty.getIndexRaw(var0);
      var2.angularVelocity.set(ParticleProperty.getAngularVelocity(var0, var1));
      var2.color.set(ParticleProperty.getColor(var0, var1));
      var2.lifetime = ParticleProperty.getLifetime(var0, var1);
      var2.lifetimeTotal = ParticleProperty.getLifetimeTotal(var0, var1);
      var2.position.set(ParticleProperty.getPosition(var0, var1));
      var2.randomSeed = ParticleProperty.getRandomSeed(var0, var1);
      var2.rotation.set(ParticleProperty.getRotation(var0, var1));
      var2.size.set(ParticleProperty.getSize(var0, var1));
      var2.startLifetime = ParticleProperty.getStartLifetime(var0, var1);
      var2.velocity.set(ParticleProperty.getVelocity(var0, var1));
      var2.camDist = ParticleProperty.getCameraDistance(var0, var1);
   }

   public static void setParticle(int var0, float[] var1, ParticleContainer var2) {
      ParticleProperty.setAngularVelocity(var0, var1, var2.angularVelocity);
      ParticleProperty.setColor(var0, var1, var2.color);
      ParticleProperty.setLifetime(var0, var1, var2.lifetime);
      ParticleProperty.setLifetimeTotal(var0, var1, var2.lifetimeTotal);
      ParticleProperty.setPos(var0, var1, var2.position);
      ParticleProperty.setRandomSeed(var0, var1, var2.randomSeed);
      ParticleProperty.setRotation(var0, var1, var2.rotation);
      ParticleProperty.setSize(var0, var1, var2.size);
      ParticleProperty.setStartLifetime(var0, var1, var2.startLifetime);
      ParticleProperty.setVelocity(var0, var1, var2.velocity);
      ParticleProperty.setCamDist(var0, var1, var2.camDist);
   }

   public static void switchIndex(int var0, int var1, float[] var2) {
      assert var0 >= 0 : var0;

      assert var1 >= 0 : var1;

      var0 = ParticleProperty.getIndexRaw(var0);
      var1 = ParticleProperty.getIndexRaw(var1);

      for(int var3 = 0; var3 < ParticleProperty.getPropertyCount(); ++var3) {
         float var4 = var2[var0 + var3];
         var2[var0 + var3] = var2[var1 + var3];
         var2[var1 + var3] = var4;
      }

   }

   public static void add(ParticleContainer var0, ParticleSystemConfiguration var1, float[] var2) {
      setParticle(var1.getParticleCount(), var2, var0);
      var1.setParticleCount(var1.getParticleCount() + 1);
   }

   public static void remove(int var0, ParticleSystemConfiguration var1, float[] var2) {
      assert var1.getParticleCount() > 0 : var1.getParticleCount();

      if (var1.getParticleCount() > 0 && var0 < var1.getParticleCount()) {
         switchIndex(var0, var1.getParticleCount() - 1, var2);
      }

      var1.setParticleCount(var1.getParticleCount() - 1);
   }

   public static void updateDistance(int var0, float[] var1) {
      Vector3f var2 = ParticleProperty.getPosition(var0, var1);
      pos.set(var2.x, var2.y, var2.z, 0.0F);
      Matrix4f.transform(Controller.modelviewMatrix, pos, res);
      ParticleProperty.setCamDist(var0, var1, -res.z);
   }

   static void quickSort(ISortableParticles var0, int var1, int var2) {
      while(var1 != var2) {
         assert var1 < var2 : var1 + "; " + var2;

         int var3 = partition(var0, var1, var2);
         quickSort(var0, var1, var3);
         var1 = var3 + 1;
         var0 = var0;
      }

   }

   static int partition(ISortableParticles var0, int var1, int var2) {
      float var3 = var0.get(var1);
      int var4 = var1;
      int var5 = var2;

      assert var2 > 0;

      while(true) {
         while(var0.get(var5) <= var3 || var5 < var1) {
            while(var0.get(var4) < var3 && var4 <= var2) {
               ++var4;
            }

            if (var4 >= var5) {
               return var5;
            }

            var0.switchVal(var4, var5);
            ++var4;
            --var5;
         }

         --var5;
      }
   }

   public static void quickSort(ISortableParticles var0) {
      if (var0.getSize() > 0) {
         quickSort(var0, 0, var0.getSize() - 1);
      }

   }
}
