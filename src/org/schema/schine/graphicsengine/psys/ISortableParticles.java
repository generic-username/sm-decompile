package org.schema.schine.graphicsengine.psys;

public interface ISortableParticles {
   float get(int var1);

   void switchVal(int var1, int var2);

   int getSize();
}
