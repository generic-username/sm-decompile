package org.schema.schine.graphicsengine.psys;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.network.objects.container.TransformTimed;
import org.schema.schine.physics.Physics;

public class ParticleSystem implements Drawable, Transformable {
   private final ParticleSystemConfiguration config;
   private final Transformable transformable;
   private final ParticleContainer tmp = new ParticleContainer();
   private final ParticleVertexBuffer buffer = new ParticleVertexBuffer();
   private float[] rawParticles;
   private boolean running;
   private boolean started;
   private boolean stopped;

   public ParticleSystem(ParticleSystemConfiguration var1, Transformable var2) {
      this.config = var1;
      this.transformable = var2;
      this.rawParticles = new float[var1.getMaxParticles() * ParticleProperty.getPropertyCount()];
   }

   public void resize() {
      float[] var1;
      int var2 = (var1 = this.rawParticles).length / ParticleProperty.getPropertyCount();
      this.rawParticles = new float[this.config.getMaxParticles() * ParticleProperty.getPropertyCount()];

      for(int var3 = 0; var3 < var1.length && var3 < this.rawParticles.length; ++var3) {
         this.rawParticles[var3] = var1[var3];
      }

      if (this.config.getMaxParticles() < var2) {
         this.config.setParticleCount(Math.min(this.config.getParticleCount(), this.config.getMaxParticles()));
      }

   }

   public void init() {
   }

   public void start() {
      this.started = true;
      this.running = true;
      this.config.getRandom().setSeed(this.config.getSeed());
      if (this.config.getParticleSystemDuration() <= 0.0F) {
         this.started = false;
         this.running = false;
      }

      this.config.start();
   }

   public void stop() {
      this.stopped = true;
      this.started = false;
      this.running = false;
   }

   public void pause() {
      this.running = !this.running;
   }

   public void update(Physics var1, Timer var2) {
      if (this.started && this.running) {
         this.spawnPhase(var2);
         this.updatePhase(var1, var2);
      }

   }

   private void spawnPhase(Timer var1) {
      for(boolean var2 = this.config.spawn(var1, this.rawParticles, this.getWorldTransform()); !var2; var2 = this.config.spawn(var1, this.rawParticles, this.getWorldTransform())) {
         this.resize();
      }

   }

   private void updatePhase(Physics var1, Timer var2) {
      int var3 = this.config.getParticleCount();

      for(int var4 = 0; var4 < var3; ++var4) {
         Particle.getParticle(var4, this.rawParticles, this.tmp);
         this.config.handleParticleUpdate(var1, var2, this.tmp);
         if (this.tmp.lifetime <= 0.0F) {
            Particle.remove(var4, this.config, this.rawParticles);
            --var3;
         } else {
            Particle.setParticle(var4, this.rawParticles, this.tmp);
         }
      }

   }

   public void sort(int var1) {
      Particle.quickSort(new ParticleSort(this.rawParticles, var1));
   }

   public void cleanUp() {
   }

   public void draw() {
      this.config.draw(this, this.buffer);
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
   }

   public TransformTimed getWorldTransform() {
      return this.transformable.getWorldTransform();
   }

   public Transformable getTransformable() {
      return this.transformable;
   }

   public boolean isStarted() {
      return this.started;
   }

   public boolean isRunning() {
      return this.running;
   }

   public boolean isCompletelyDead() {
      return this.stopped;
   }

   public int getParticleCount() {
      return this.config.getParticleCount();
   }

   public float[] getRawParticles() {
      return this.rawParticles;
   }

   public void getColor(Vector4f var1, ParticleContainer var2) {
      this.config.getColor(var1, var2);
   }
}
