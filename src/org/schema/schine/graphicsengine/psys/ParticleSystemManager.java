package org.schema.schine.graphicsengine.psys;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import it.unimi.dsi.fastutil.objects.ObjectListIterator;
import java.awt.EventQueue;
import java.util.Collections;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.network.objects.container.TransformTimed;
import org.schema.schine.physics.Physics;

public class ParticleSystemManager implements ISortableParticles {
   static Vector4f res = new Vector4f();
   public ObjectList systems = new ObjectArrayList();

   public void startParticleSystemWorld(ParticleSystemConfiguration var1, Transform var2) {
      assert var2 != null;

      ParticleSystem var3 = new ParticleSystem(var1, new ParticleSystemManager.WorldTransformable(var2));
      this.systems.add(var3);
      var3.start();
   }

   public void stopParticleSystemsWorld() {
      Iterator var1 = this.systems.iterator();

      while(var1.hasNext()) {
         ((ParticleSystem)var1.next()).stop();
      }

   }

   public void pauseParticleSystemsWorld() {
      Iterator var1 = this.systems.iterator();

      while(var1.hasNext()) {
         ((ParticleSystem)var1.next()).pause();
      }

   }

   public void update(Physics var1, Timer var2) {
      ObjectListIterator var3 = this.systems.iterator();

      while(var3.hasNext()) {
         ParticleSystem var4;
         (var4 = (ParticleSystem)var3.next()).update(var1, var2);
         if (var4.isCompletelyDead()) {
            var3.remove();
         }
      }

   }

   public void draw() {
      Particle.quickSort(this);
      Iterator var1 = this.systems.iterator();

      while(var1.hasNext()) {
         ((ParticleSystem)var1.next()).draw();
      }

   }

   public void openGUI(final ClientState var1) {
      EventQueue.invokeLater(new Runnable() {
         public void run() {
            try {
               ParticleSystemGUI var1x = new ParticleSystemGUI(var1);
               CameraMouseState.ungrabForced = !CameraMouseState.ungrabForced;
               var1x.setAlwaysOnTop(true);
               var1x.setDefaultCloseOperation(2);
               var1x.setVisible(true);
            } catch (Exception var2) {
               var2.printStackTrace();
            }
         }
      });
   }

   public float get(int var1) {
      Vector3f var2 = ((ParticleSystem)this.systems.get(var1)).getWorldTransform().origin;
      Matrix4f.transform(Controller.modelviewMatrix, new Vector4f(var2.x, var2.y, var2.z, 1.0F), res);
      return res.z;
   }

   public void switchVal(int var1, int var2) {
      Collections.swap(this.systems, var1, var2);
   }

   public int getSize() {
      return this.systems.size();
   }

   class WorldTransformable implements Transformable {
      TransformTimed transform = new TransformTimed();

      public WorldTransformable(Transform var2) {
         this.transform.set(var2);
      }

      public TransformTimed getWorldTransform() {
         return this.transform;
      }
   }
}
