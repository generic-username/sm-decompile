package org.schema.schine.graphicsengine.psys;

import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

public class ParticleContainer {
   public final Vector3f angularVelocity = new Vector3f();
   public final Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   public final Vector3f position = new Vector3f();
   public final Quat4f rotation = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
   public final Vector3f size = new Vector3f(1.0F, 1.0F, 1.0F);
   public final Vector3f velocity = new Vector3f();
   public float lifetime;
   public float lifetimeTotal;
   public float randomSeed;
   public float camDist = 0.0F;
   float startLifetime;

   public void reset() {
      this.angularVelocity.set(0.0F, 0.0F, 0.0F);
      this.color.set(1.0F, 1.0F, 1.0F, 1.0F);
      this.lifetime = 0.0F;
      this.lifetimeTotal = 0.0F;
      this.position.set(0.0F, 0.0F, 0.0F);
      this.randomSeed = 0.0F;
      this.rotation.set(0.0F, 0.0F, 0.0F, 1.0F);
      this.size.set(1.0F, 1.0F, 1.0F);
      this.startLifetime = 0.0F;
      this.velocity.set(0.0F, 0.0F, 0.0F);
      this.camDist = 0.0F;
   }
}
