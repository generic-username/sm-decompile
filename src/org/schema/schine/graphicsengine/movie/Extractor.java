package org.schema.schine.graphicsengine.movie;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.schema.schine.graphicsengine.movie.craterstudio.io.Streams;
import org.schema.schine.graphicsengine.movie.craterstudio.text.Text;
import org.schema.schine.resource.FileExt;

public class Extractor {
   public static final boolean isWindows;
   public static final boolean isMac;
   public static final boolean isLinux;
   public static final boolean is64bit;

   public static void extractNativeLibrary(String var0) throws IOException {
      String[] var10000 = Text.split(System.getProperty("java.library.path"), File.pathSeparatorChar);
      String var1 = var10000[var10000.length - 1];
      var1 = var1 + '/' + Text.afterLast(var0, '/');
      extractResource(var0, new FileExt(var1));
   }

   public static void extractResource(String var0, File var1) throws IOException {
      if (!var1.exists()) {
         File var2;
         if (!(var2 = var1.getParentFile()).exists() && !var2.mkdirs()) {
            throw new IllegalStateException("failed to create dir: " + var2.getAbsolutePath());
         } else {
            InputStream var3;
            if ((var3 = FFmpeg.class.getResourceAsStream(var0)) == null) {
               throw new IllegalStateException("failed to find resource: " + var0);
            } else {
               System.out.println("Extracting " + var0 + " to " + var1.getAbsolutePath());
               Streams.copy(new BufferedInputStream(var3), new BufferedOutputStream(new FileOutputStream(var1)));
            }
         }
      }
   }

   static {
      String var0 = System.getProperty("os.name");
      String var1 = System.getProperty("os.arch");
      isWindows = var0.contains("Windows");
      isMac = var0.contains("Mac");
      isLinux = !isWindows && !isMac;
      if ((var0 = System.getProperty("sun.arch.data.model")) != null) {
         is64bit = Integer.parseInt(var0) == 64;
      } else {
         is64bit = var1.equals("amd64") || var1.equals("x86_64");
      }
   }
}
