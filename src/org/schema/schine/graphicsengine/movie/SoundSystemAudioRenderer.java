package org.schema.schine.graphicsengine.movie;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.sound.sampled.AudioFormat.Encoding;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.movie.craterstudio.math.EasyMath;
import org.schema.schine.graphicsengine.movie.craterstudio.util.concur.SimpleBlockingQueue;
import paulscode.sound.SoundSystemConfig;

public class SoundSystemAudioRenderer extends AudioRenderer {
   private int lastBuffersProcessed = 0;
   private boolean hasMoreSamples = true;
   final SimpleBlockingQueue pendingActions = new SimpleBlockingQueue();
   private float volume = 1.0F;
   private AudioRenderer.State state;
   private ByteBuffer samples;
   private byte[] buff;

   public SoundSystemAudioRenderer(File var1) {
      this.state = AudioRenderer.State.INIT;
   }

   public void setVolume(float var1) {
      if (!EasyMath.isBetween(var1, 0.0F, 1.0F)) {
         throw new IllegalArgumentException();
      } else {
         this.volume = var1;
         this.pendingActions.put(new SoundSystemAudioRenderer.Action(SoundSystemAudioRenderer.ActionType.ADJUST_VOLUME, var1));
      }
   }

   public float getVolume() {
      return this.volume;
   }

   public void pause() {
      this.pendingActions.put(new SoundSystemAudioRenderer.Action(SoundSystemAudioRenderer.ActionType.PAUSE_AUDIO, (Object)null));
   }

   public void resume() {
      this.pendingActions.put(new SoundSystemAudioRenderer.Action(SoundSystemAudioRenderer.ActionType.RESUME_AUDIO, (Object)null));
   }

   public void stop() {
      this.pendingActions.put(new SoundSystemAudioRenderer.Action(SoundSystemAudioRenderer.ActionType.STOP_AUDIO, (Object)null));
   }

   public AudioRenderer.State getState() {
      return this.state;
   }

   private void init(Movie var1) throws UnsupportedAudioFileException, IOException {
      int var2 = SoundSystemConfig.getStreamingBufferSize();
      this.buff = new byte[var2];
      System.err.println("FPD:::: " + this.frameRate + "; " + this.audioStream.byteRate);
      AudioFormat var3 = new AudioFormat(Encoding.PCM_SIGNED, (float)this.audioStream.sampleRate, this.audioStream.bytesPerSample << 3, this.audioStream.numChannels, this.audioStream.bytesPerSample * this.audioStream.numChannels, this.frameRate, false);
      Controller.getAudioManager().rawDataStream(var3, true, "movie", 0.0F, 0.0F, 0.0F, 1, 20.0F);
      Controller.getAudioManager().play("movie");
   }

   private void buffer() {
      for(int var1 = 0; var1 < 5 || (float)var1 < this.frameRate * 0.1F; ++var1) {
         this.enqueueNextSamples();
      }

   }

   public boolean tick(Movie var1) {
      switch(this.state) {
      case INIT:
         try {
            this.init(var1);
         } catch (UnsupportedAudioFileException var3) {
            var3.printStackTrace();
         } catch (IOException var4) {
            var4.printStackTrace();
         }

         this.state = AudioRenderer.State.BUFFERING;
         return true;
      case BUFFERING:
         this.buffer();
         this.state = AudioRenderer.State.PLAYING;
         return true;
      case CLOSED:
         return false;
      default:
         SoundSystemAudioRenderer.Action var2;
         while((var2 = (SoundSystemAudioRenderer.Action)this.pendingActions.poll()) != null) {
            switch(var2.type) {
            case ADJUST_VOLUME:
               break;
            case PAUSE_AUDIO:
               this.state = AudioRenderer.State.PAUSED;
               return true;
            case RESUME_AUDIO:
               this.state = AudioRenderer.State.PLAYING;
               break;
            case STOP_AUDIO:
               this.state = AudioRenderer.State.CLOSED;
               break;
            default:
               throw new IllegalStateException();
            }
         }

         switch(this.state) {
         case PLAYING:
            if (this.samples == null) {
               try {
                  this.close();
               } catch (IOException var5) {
                  var5.printStackTrace();
               }

               return false;
            } else {
               while(this.samples.remaining() > 0) {
                  this.samples.get(this.buff, 0, Math.min(this.samples.remaining(), this.buff.length));
                  System.err.println("PLAY ");
                  Controller.audioManager.feedRaw("movie", this.buff);
               }

               System.err.println("PLAY ##################");
               if (this.samples.remaining() == 0) {
                  this.enqueueNextSamples();
               }

               var1.onRenderedAudioBuffer();
            }
         case CLOSED:
            int var6 = 1 - this.lastBuffersProcessed;
            this.lastBuffersProcessed = 1;
            if (var6 == 0) {
               return true;
            } else {
               if (var6 < 0) {
                  throw new IllegalStateException();
               }

               return true;
            }
         case PAUSED:
            return true;
         default:
            throw new IllegalStateException();
         }
      }
   }

   private void enqueueNextSamples() {
      if (this.hasMoreSamples) {
         this.samples = super.loadNextSamples();
         if (this.samples == null) {
            this.hasMoreSamples = false;
         }
      }
   }

   public void await() throws IOException {
   }

   public void close() throws IOException {
      this.state = AudioRenderer.State.CLOSED;
      Controller.getAudioManager().closeStream("movie");
      super.close();
   }

   static class Action {
      final SoundSystemAudioRenderer.ActionType type;

      public Action(SoundSystemAudioRenderer.ActionType var1, Object var2) {
         this.type = var1;
      }
   }

   static enum ActionType {
      ADJUST_VOLUME,
      PAUSE_AUDIO,
      RESUME_AUDIO,
      STOP_AUDIO;
   }
}
