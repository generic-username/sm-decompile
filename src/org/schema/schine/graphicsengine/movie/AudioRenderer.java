package org.schema.schine.graphicsengine.movie;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import org.schema.schine.graphicsengine.core.GlUtil;

public abstract class AudioRenderer implements Closeable {
   protected AudioStream audioStream;
   protected float frameRate;
   private byte[] largest;
   private final int[] samplesInBuffers = new int[2];
   protected final ByteBuffer[] bufferDuo = new ByteBuffer[2];
   private final ByteList bufferIndexList = new ByteList();
   private int loadIndex = 0;

   public void init(AudioStream var1, float var2) {
      this.audioStream = var1;
      this.frameRate = var2;
      if (this.audioStream.numChannels != 2) {
         throw new IllegalStateException();
      } else if (this.audioStream.bytesPerSample != 2) {
         throw new IllegalStateException();
      } else {
         this.samplesInBuffers[0] = (int)Math.floor((double)((float)this.audioStream.sampleRate / var2));
         this.samplesInBuffers[1] = (int)Math.ceil((double)((float)this.audioStream.sampleRate / var2));
         double var3 = (double)this.audioStream.sampleRate / (double)var2;
         this.calcSyncPattern(var3);

         for(int var5 = 0; var5 < this.bufferDuo.length; ++var5) {
            this.bufferDuo[var5] = ByteBuffer.allocateDirect(this.samplesInBuffers[var5] * (this.audioStream.numChannels + this.audioStream.bytesPerSample));
         }

         this.largest = new byte[Math.max(this.bufferDuo[0].capacity(), this.bufferDuo[1].capacity())];
      }
   }

   private void calcSyncPattern(double var1) {
      double var3 = 2.147483647E9D;
      int var5 = -1;
      double var6 = 1.0D;

      for(int var8 = 0; var8 < 10000; ++var8) {
         double var9;
         int var11 = (var9 = var1 * (double)(var8 + 1) % 1.0D) > var6 ? 0 : 1;
         var6 = var9;
         this.bufferIndexList.add((byte)var11);
         double var12;
         if ((var12 = this.calcHourlyError()) < var3) {
            var3 = var12;
            var5 = var8;
         }

         if (var12 < 0.01D) {
            break;
         }
      }

      while(this.bufferIndexList.size() > var5 + 1) {
         this.bufferIndexList.removeLast();
      }

   }

   private double calcHourlyError() {
      int var1 = 0;

      for(int var2 = 0; var2 < this.bufferIndexList.size(); ++var2) {
         var1 += this.samplesInBuffers[this.bufferIndexList.get(var2)];
      }

      double var4 = (double)((float)this.audioStream.sampleRate / this.frameRate);
      return ((double)var1 / (double)this.bufferIndexList.size() - var4) * 3600.0D / (double)this.audioStream.sampleRate;
   }

   public abstract AudioRenderer.State getState();

   public abstract void pause();

   public abstract void resume();

   public abstract void stop();

   public abstract float getVolume();

   public abstract void setVolume(float var1);

   public abstract boolean tick(Movie var1);

   public ByteBuffer loadNextSamples() {
      try {
         ByteBuffer var1 = this.bufferDuo[this.bufferIndexList.get(this.loadIndex++ % this.bufferIndexList.size())];
         this.audioStream.readSamples(this.largest, 0, var1.capacity());
         var1.clear();
         var1.put(this.largest, 0, var1.capacity());
         var1.flip();
         return var1;
      } catch (IOException var2) {
         return null;
      }
   }

   public void close() throws IOException {
      this.audioStream.close();

      for(int var1 = 0; var1 < this.bufferDuo.length; ++var1) {
         try {
            GlUtil.destroyDirectByteBuffer(this.bufferDuo[var1]);
         } catch (IllegalArgumentException var2) {
            var2.printStackTrace();
         } catch (IllegalAccessException var3) {
            var3.printStackTrace();
         } catch (InvocationTargetException var4) {
            var4.printStackTrace();
         } catch (SecurityException var5) {
            var5.printStackTrace();
         } catch (NoSuchMethodException var6) {
            var6.printStackTrace();
         }
      }

   }

   public static enum State {
      INIT,
      BUFFERING,
      PLAYING,
      PAUSED,
      CLOSED;
   }
}
