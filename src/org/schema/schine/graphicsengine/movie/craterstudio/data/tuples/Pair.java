package org.schema.schine.graphicsengine.movie.craterstudio.data.tuples;

public class Pair {
   private final Object a;
   private final Object b;

   public Pair(Object var1, Object var2) {
      this.a = var1;
      this.b = var2;
   }

   public Object first() {
      return this.a;
   }

   public Object second() {
      return this.b;
   }

   public int hashCode() {
      int var1 = this.a == null ? 0 : this.a.hashCode();
      int var2 = this.b == null ? 0 : this.b.hashCode();
      return var1 ^ var2 * 37;
   }

   public String toString() {
      return "Pair[" + this.a + ", " + this.b + "]";
   }

   public boolean equals(Object var1) {
      if (!(var1 instanceof Pair)) {
         return false;
      } else {
         Pair var2 = (Pair)var1;
         return eq(this.a, var2.a) && eq(this.b, var2.b);
      }
   }

   private static final boolean eq(Object var0, Object var1) {
      if (var0 == var1) {
         return true;
      } else if (var0 == null ^ var1 == null) {
         return false;
      } else if (var0 != null) {
         return var0.equals(var1);
      } else {
         return var1 != null ? var1.equals(var0) : false;
      }
   }
}
