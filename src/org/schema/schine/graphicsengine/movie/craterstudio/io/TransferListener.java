package org.schema.schine.graphicsengine.movie.craterstudio.io;

import java.io.IOException;

public interface TransferListener {
   void transferInitiated(int var1);

   void transfered(int var1);

   void transferFinished(IOException var1);
}
