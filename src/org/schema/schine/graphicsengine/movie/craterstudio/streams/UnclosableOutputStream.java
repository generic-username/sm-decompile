package org.schema.schine.graphicsengine.movie.craterstudio.streams;

import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;

public class UnclosableOutputStream extends OutputStream {
   private final OutputStream out;
   private boolean closed;

   public UnclosableOutputStream(OutputStream var1) {
      this.out = var1;
      this.closed = false;
   }

   public void write(int var1) throws IOException {
      if (this.closed) {
         throw new EOFException();
      } else {
         this.out.write(var1);
      }
   }

   public void write(byte[] var1) throws IOException {
      if (this.closed) {
         throw new EOFException();
      } else {
         this.out.write(var1);
      }
   }

   public void write(byte[] var1, int var2, int var3) throws IOException {
      if (this.closed) {
         throw new EOFException();
      } else {
         this.out.write(var1, var2, var3);
      }
   }

   public void flush() throws IOException {
      if (this.closed) {
         throw new EOFException();
      } else {
         this.out.flush();
      }
   }

   public void close() throws IOException {
      if (this.closed) {
         throw new EOFException();
      } else {
         this.closed = true;
         this.out.flush();
      }
   }
}
