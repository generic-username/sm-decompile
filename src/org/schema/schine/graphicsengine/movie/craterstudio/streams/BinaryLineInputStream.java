package org.schema.schine.graphicsengine.movie.craterstudio.streams;

import java.io.InputStream;
import org.schema.schine.graphicsengine.movie.craterstudio.io.Streams;

public class BinaryLineInputStream extends AbstractInputStream {
   public BinaryLineInputStream(InputStream var1) {
      super(var1);
   }

   public String readLine() {
      return Streams.binaryReadLineAsString(this.backing);
   }
}
