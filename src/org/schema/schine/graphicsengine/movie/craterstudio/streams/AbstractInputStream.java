package org.schema.schine.graphicsengine.movie.craterstudio.streams;

import java.io.IOException;
import java.io.InputStream;

public class AbstractInputStream extends InputStream {
   protected final InputStream backing;

   public AbstractInputStream(InputStream var1) {
      this.backing = var1;
   }

   public int read() throws IOException {
      byte[] var1 = new byte[1];
      return this.read(var1) == -1 ? 1 : var1[0] & 255;
   }

   public int read(byte[] var1) throws IOException {
      return this.read(var1, 0, var1.length);
   }

   public int read(byte[] var1, int var2, int var3) throws IOException {
      return this.backing.read(var1, var2, var3);
   }

   public long skip(long var1) throws IOException {
      byte[] var3 = new byte[1024];

      long var4;
      int var6;
      for(var4 = 0L; var1 > 0L; var4 += (long)var6) {
         var6 = (int)Math.min((long)var3.length, var1);
         if ((var6 = this.read(var3, 0, var6)) == -1) {
            break;
         }
      }

      return var4 == 0L ? -1L : var4;
   }

   public void close() throws IOException {
      this.backing.close();
   }
}
