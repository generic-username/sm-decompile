package org.schema.schine.graphicsengine.movie.craterstudio.text;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.schema.schine.graphicsengine.movie.craterstudio.data.ByteList;
import org.schema.schine.graphicsengine.movie.craterstudio.data.tuples.Pair;

public class Text {
   private static final char[] table = "0123456789abcdef".toCharArray();
   private static final char[] generatecode_default_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567890".toCharArray();
   private static final int UUID_LENGTH = 16;
   private static final int UUID_MAX_ELEMENTS = 1024;
   private static Set UUID_SET = new HashSet();
   private static LinkedList UUID_LIST = new LinkedList();
   public static final int ALIGN_LEFT = 0;
   public static final int ALIGN_CENTER = 1;
   public static final int ALIGN_RIGHT = 2;
   private static final String encoding = "ISO-8859-1";

   public static boolean isNullOrEmpty(String var0) {
      return var0 == null || var0.isEmpty();
   }

   public static String trimToNull(String var0) {
      if (var0 == null) {
         return null;
      } else {
         return (var0 = var0.trim()).isEmpty() ? null : var0;
      }
   }

   public static String nullToEmpty(String var0) {
      return var0 == null ? "" : var0;
   }

   public static String concat(String... var0) {
      int var1 = 0;

      for(int var2 = 0; var2 < var0.length; ++var2) {
         var1 += (var0[var2] = var0[var2] == null ? "null" : var0[var2]).length();
      }

      char[] var5 = new char[var1];
      var1 = 0;

      for(int var3 = 0; var1 < var0.length; ++var1) {
         for(int var4 = 0; var4 < var0[var1].length(); ++var4) {
            var5[var3++] = var0[var1].charAt(var4);
         }
      }

      return new String(var5);
   }

   public static String hashAsHex(byte[] var0) {
      char[] var1 = table;
      char[] var2 = new char[var0.length << 1];

      for(int var3 = 0; var3 < var0.length; ++var3) {
         byte var4 = var0[var3];
         var2[var3 << 1 | 1] = var1[var4 & 15];
         var2[var3 << 1] = var1[(var4 & 240) >> 4];
      }

      return new String(var2);
   }

   public static String generateRandomCode(int var0) {
      return generateRandomCode(var0, generatecode_default_chars);
   }

   public static String generateRandomCode(int var0, char[] var1) {
      return generateRandomCode(var0, var1, new SecureRandom());
   }

   public static String generateRandomCode(int var0, char[] var1, Random var2) {
      char[] var4 = new char[var0];

      for(int var3 = 0; var3 < var4.length; ++var3) {
         var4[var3] = var1[var2.nextInt(var1.length)];
      }

      return new String(var4);
   }

   public static final synchronized String generateUniqueId() {
      String var0;
      do {
         var0 = generateRandomCode(16);
      } while(!UUID_SET.add(var0));

      UUID_LIST.addLast(var0);
      if (UUID_LIST.size() > 1024) {
         UUID_SET.remove(UUID_LIST.removeFirst());
      }

      return var0;
   }

   public static final String formatString(String var0, int var1) {
      return formatString(var0, var1, 0, ' ');
   }

   public static final String formatString(String var0, int var1, char var2) {
      return formatString(var0, var1, 0, var2);
   }

   public static final String formatString(String var0, int var1, int var2) {
      return formatString(var0, var1, var2, ' ');
   }

   public static final String formatString(String var0, int var1, int var2, char var3) {
      if (var0.length() > var1) {
         return var0.length() >= 3 ? var0.substring(0, var1 - 3) + "..." : "<?>";
      } else {
         char[] var4 = new char[var1];
         int var5;
         int var6;
         if (var2 == 0) {
            var5 = var0.length();

            for(var6 = 0; var6 < var5; ++var6) {
               var4[var6] = var0.charAt(var6);
            }

            for(var6 = var5; var6 < var1; ++var6) {
               var4[var6] = var3;
            }
         }

         if (var2 == 1) {
            throw new UnsupportedOperationException();
         } else {
            if (var2 == 2) {
               var5 = var1 - var0.length();

               for(var6 = 0; var6 < var5; ++var6) {
                  var4[var6] = var3;
               }

               for(var6 = var5; var6 < var1; ++var6) {
                  var4[var6] = var0.charAt(var6 - var5);
               }
            }

            return new String(var4);
         }
      }
   }

   public static final boolean onlyNumbers(String var0) {
      char[] var3 = var0.toCharArray();

      for(int var1 = 0; var1 < var3.length; ++var1) {
         char var2;
         if ((var2 = var3[var1]) < '0' || var2 > '9') {
            return false;
         }
      }

      return true;
   }

   public static final boolean onlyLetters(String var0) {
      char[] var3 = var0.toCharArray();

      for(int var1 = 0; var1 < var3.length; ++var1) {
         char var2;
         if ((var2 = var3[var1]) >= 'a' && var2 <= 'z' || var2 >= 'A' && var2 <= 'Z') {
            return false;
         }
      }

      return true;
   }

   public static final String ascii(byte[] var0, int var1, int var2) {
      char[] var3 = new char[var2];

      for(int var4 = 0; var4 < var2; ++var4) {
         var3[var4] = (char)var0[var1 + var4];
      }

      return new String(var3);
   }

   public static final String ascii(byte[] var0) {
      char[] var1 = new char[var0.length];

      for(int var2 = 0; var2 < var1.length; ++var2) {
         var1[var2] = (char)var0[var2];
      }

      return new String(var1);
   }

   public static final byte[] ascii(String var0) {
      byte[] var1 = new byte[var0.length()];

      for(int var2 = 0; var2 < var1.length; ++var2) {
         var1[var2] = (byte)var0.charAt(var2);
      }

      return var1;
   }

   public static final void ascii(OutputStream var0, byte[] var1, String var2) throws IOException {
      int var3 = 0;

      for(int var4 = 0; var4 < var2.length(); ++var4) {
         var1[var3++] = (byte)var2.charAt(var4);
         if (var3 == var1.length) {
            var0.write(var1, 0, var3);
            var3 = 0;
         }
      }

      var0.write(var1, 0, var3);
   }

   public static String toLower(String var0) {
      char[] var2 = var0.toCharArray();

      for(int var1 = 0; var1 < var2.length; ++var1) {
         var2[var1] = toLower(var2[var1]);
      }

      return new String(var2);
   }

   public static String toUpper(String var0) {
      char[] var2 = var0.toCharArray();

      for(int var1 = 0; var1 < var2.length; ++var1) {
         var2[var1] = toUpper(var2[var1]);
      }

      return new String(var2);
   }

   public static char toLower(char var0) {
      return var0 >= 'A' && var0 <= 'Z' ? (char)(var0 + 32) : var0;
   }

   public static char toUpper(char var0) {
      return var0 >= 'a' && var0 <= 'z' ? (char)(var0 - 32) : var0;
   }

   public static final String utf8(byte[] var0) {
      return utf8(var0, 0, var0.length);
   }

   public static final String utf8(byte[] var0, int var1, int var2) {
      try {
         return new String(var0, var1, var2, "UTF-8");
      } catch (UnsupportedEncodingException var3) {
         throw new IllegalStateException();
      }
   }

   public static final byte[] utf8(String var0) {
      try {
         return var0.getBytes("UTF-8");
      } catch (UnsupportedEncodingException var1) {
         throw new IllegalStateException();
      }
   }

   public static final String convert(byte[] var0) {
      return convert(var0, "ISO-8859-1");
   }

   public static final String convert(byte[] var0, String var1) {
      try {
         return new String(var0, var1);
      } catch (Exception var2) {
         return null;
      }
   }

   public static final String convert(byte[] var0, int var1, int var2) {
      try {
         return new String(var0, var1, var2, "ISO-8859-1");
      } catch (Exception var3) {
         return null;
      }
   }

   public static final byte[] convert(String var0) {
      try {
         return var0.getBytes("ISO-8859-1");
      } catch (Exception var1) {
         return null;
      }
   }

   public static String chopLast(String var0, int var1) {
      if (var1 >= 0 && var1 <= var0.length()) {
         return var0.substring(0, var0.length() - var1);
      } else {
         throw new IllegalArgumentException("invalid chars: " + var1 + " / " + var0.length());
      }
   }

   public static StringBuilder chopLast(StringBuilder var0, int var1) {
      if (var1 >= 0 && var1 <= var0.length()) {
         var0.setLength(var0.length() - var1);
         return var0;
      } else {
         throw new IllegalArgumentException("invalid chars: " + var1 + " / " + var0.length());
      }
   }

   public static final String flip(String var0) {
      char[] var5;
      int var1 = (var5 = var0.toCharArray()).length / 2;
      int var2 = var5.length - 1;

      for(int var3 = 0; var3 < var1; ++var3) {
         char var4 = var5[var3];
         var5[var3] = var5[var2 - var3];
         var5[var2 - var3] = var4;
      }

      return String.valueOf(var5);
   }

   public static String firstOccurence(String var0, String... var1) {
      int var2 = Integer.MAX_VALUE;
      String var3 = null;
      int var4 = (var1 = var1).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         String var6 = var1[var5];
         int var7;
         if ((var7 = var0.indexOf(var6)) != -1 && var7 < var2) {
            var2 = var7;
            var3 = var6;
         }
      }

      return var3;
   }

   public static Pair firstOccurenceWithIndex(String var0, String... var1) {
      int var2 = Integer.MAX_VALUE;
      String var3 = null;
      int var4 = (var1 = var1).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         String var6 = var1[var5];
         int var7;
         if ((var7 = var0.indexOf(var6)) != -1 && var7 < var2) {
            var2 = var7;
            var3 = var6;
         }
      }

      if (var3 == null) {
         return null;
      } else {
         return new Pair(var3, var2);
      }
   }

   public static final String[] depthAwareSplit(String var0, char var1, char var2, char var3) {
      ArrayList var4 = new ArrayList();
      int var5 = 0;
      int var6 = 0;

      for(int var7 = 0; var7 < var0.length(); ++var7) {
         char var8;
         if ((var8 = var0.charAt(var7)) == var2) {
            ++var5;
         } else if (var8 == var3) {
            --var5;
         } else if (var8 == var1 && var5 == 0) {
            var4.add(var0.substring(var6, var7));
            var6 = var7 + 1;
         }
      }

      if (var6 != var0.length()) {
         var4.add(var0.substring(var6));
      }

      return (String[])var4.toArray(new String[var4.size()]);
   }

   public static final String normalizeLinebreaks(String var0) {
      return replace(replace(var0, "\r\n", "\n"), '\r', '\n');
   }

   public static final String[] splitOnLines(String var0) {
      return split(normalizeLinebreaks(var0), '\n');
   }

   public static final String[] split(String var0, char var1) {
      String[] var2 = new String[count(var0, var1) + 1];
      split(var0, var1, var2);
      return var2;
   }

   public static final String[] splitOnUnicodeWhitespace(String var0) {
      ArrayList var1 = new ArrayList();
      StringBuilder var2 = new StringBuilder();

      for(int var3 = 0; var3 < var0.length(); ++var3) {
         if (Character.isWhitespace(var0.charAt(var3))) {
            var1.add(var2.toString());
            var2.setLength(0);
         } else {
            var2.append(var0.charAt(var3));
         }
      }

      var1.add(var2.toString());
      var2.setLength(0);
      return (String[])var1.toArray(new String[var1.size()]);
   }

   public static final String[] split(String var0, String var1) {
      ArrayList var2 = new ArrayList();

      String[] var4;
      for(String[] var3 = new String[2]; (var4 = splitPair(var0, var1, var3)) != null; var0 = var4[1]) {
         var2.add(var4[0]);
      }

      var2.add(var0);
      return (String[])var2.toArray(new String[var2.size()]);
   }

   public static final int split(String var0, char var1, String[] var2) {
      int var3 = 0;
      int var4 = var0.length();
      int var5 = 0;

      for(int var6 = 0; var6 < var4; ++var6) {
         if (var0.charAt(var6) == var1) {
            var2[var3++] = var0.substring(var5, var6);
            var5 = var6 + 1;
         }
      }

      var2[var3++] = var0.substring(var5, var4);
      return var3;
   }

   public static final String[] splitPair(String var0, char var1) {
      return splitPair(var0, var1, new String[2]);
   }

   public static final String[] splitPair(String var0, String var1) {
      return splitPair(var0, var1, new String[2]);
   }

   public static final String[] splitPair(String var0, char var1, String[] var2) {
      int var3;
      if ((var3 = var0.indexOf(var1)) == -1) {
         return null;
      } else {
         var2[0] = var0.substring(0, var3);
         var2[1] = var0.substring(var3 + 1);
         return var2;
      }
   }

   public static final String[] splitPair(String var0, String var1, String[] var2) {
      int var3;
      if ((var3 = var0.indexOf(var1)) == -1) {
         return null;
      } else {
         var2[0] = var0.substring(0, var3);
         var2[1] = var0.substring(var3 + var1.length());
         return var2;
      }
   }

   public static final boolean splitPair(String var0, char var1, Map var2) {
      int var3;
      if ((var3 = var0.indexOf(var1)) == -1) {
         return false;
      } else {
         var2.put(var0.substring(0, var3), var0.substring(var3 + 1));
         return true;
      }
   }

   public static final boolean splitPair(String var0, String var1, Map var2) {
      int var3;
      if ((var3 = var0.indexOf(var1)) == -1) {
         return false;
      } else {
         var2.put(var0.substring(0, var3), var0.substring(var3 + var1.length()));
         return true;
      }
   }

   public static final String[] splitOnUppercase(String var0) {
      StringBuilder var1 = new StringBuilder();
      ArrayList var2 = new ArrayList();
      char[] var6;
      int var3 = (var6 = var0.toCharArray()).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         char var5;
         if (Character.isUpperCase(var5 = var6[var4])) {
            if (var1.length() > 0) {
               var2.add(var1.toString());
            }

            var1.setLength(0);
         }

         var1.append(var5);
      }

      if (var1.length() > 0) {
         var2.add(var1.toString());
      }

      return (String[])var2.toArray(new String[var2.size()]);
   }

   public static final String[] chop(String var0, int var1) {
      int var2 = var0.length() / var1;
      int var3 = var0.length() % var1 != 0 ? 1 : 0;
      String[] var4 = new String[var2 + var3];

      for(int var5 = 0; var5 < var2; ++var5) {
         var4[var5] = var0.substring(var5 * var1, (var5 + 1) * var1);
      }

      if (var3 != 0) {
         var4[var2] = var0.substring(var2 * var1);
      }

      return var4;
   }

   public static final String join(String[] var0) {
      return join(var0, 0, var0.length);
   }

   public static final String join(String[] var0, char var1) {
      return join(var0, 0, var0.length, var1);
   }

   public static final String join(String[] var0, String var1) {
      return join(var0, 0, var0.length, var1);
   }

   public static final String join(String[] var0, int var1, int var2) {
      StringBuilder var3 = new StringBuilder();

      for(int var4 = 0; var4 < var2; ++var4) {
         var3.append(var0[var1 + var4]);
      }

      return var3.toString();
   }

   public static final String join(String[] var0, int var1, int var2, char var3) {
      StringBuilder var4 = new StringBuilder();

      for(int var5 = 0; var5 < var2; ++var5) {
         var4.append(var0[var1 + var5]);
         if (var5 != var2 - 1) {
            var4.append(var3);
         }
      }

      return var4.toString();
   }

   public static final String join(String[] var0, int var1, int var2, String var3) {
      StringBuilder var4 = new StringBuilder();

      for(int var5 = 0; var5 < var2; ++var5) {
         var4.append(var0[var1 + var5]);
         if (var5 != var2 - 1) {
            var4.append(var3);
         }
      }

      return var4.toString();
   }

   public static final String[] multiSplit(String var0, char... var1) {
      int[] var2 = new int[var1.length];
      int var3 = 0;

      for(int var4 = 0; var4 < var2.length; ++var4) {
         if ((var3 |= var2[var4] = var0.indexOf(var1[var4], var4 == 0 ? 0 : var2[var4 - 1] + 1)) < 0) {
            return null;
         }
      }

      String[] var6 = new String[var1.length + 1];

      for(int var5 = 0; var5 < var6.length - 1; ++var5) {
         var6[var5] = var0.substring(var5 == 0 ? 0 : var2[var5 - 1] + 1, var2[var5]);
      }

      var6[var6.length - 1] = var0.substring(var2[var2.length - 1] + 1);
      return var6;
   }

   public static final String[] multiSplit(String var0, String... var1) {
      int[] var2 = new int[var1.length];
      int var3 = 0;

      for(int var4 = 0; var4 < var1.length; ++var4) {
         if ((var3 |= var2[var4] = var0.indexOf(var1[var4], var4 == 0 ? 0 : var2[var4 - 1] + var1[var4 - 1].length())) < 0) {
            return null;
         }
      }

      String[] var5 = new String[var1.length + 1];

      for(var3 = 0; var3 < var5.length - 1; ++var3) {
         var5[var3] = var0.substring(var3 == 0 ? 0 : var2[var3 - 1] + var1[var3 - 1].length(), var2[var3]);
      }

      var5[var5.length - 1] = var0.substring(var2[var2.length - 1] + var1[var1.length - 1].length());
      return var5;
   }

   public static final String[] multiSplitAll(String var0, char... var1) {
      ArrayList var2 = new ArrayList();

      while(true) {
         while(var0 != null) {
            String[] var3;
            if ((var3 = multiSplit(var0, var1)) == null) {
               var2.add(var0);
               var0 = null;
            } else {
               for(int var4 = 0; var4 < var3.length - 1; ++var4) {
                  var2.add(var3[var4]);
               }

               var0 = var3[var3.length - 1];
            }
         }

         return (String[])var2.toArray(new String[var2.size()]);
      }
   }

   public static final String[] multiSplitLoop_bugged(String var0, char... var1) {
      ArrayList var2 = new ArrayList();

      while(true) {
         while(var0 != null) {
            String[] var3;
            if ((var3 = multiSplit(var0, var1)) == null) {
               var2.add(var0);
               var0 = null;
            } else {
               for(int var4 = 0; var4 < var3.length - 1; ++var4) {
                  var2.add(var3[var4]);
               }

               var0 = var3[var3.length - 1];
            }
         }

         return (String[])var2.toArray(new String[var2.size()]);
      }
   }

   public static final String[] multiSplitLoop_bugged(String var0, String... var1) {
      ArrayList var2 = new ArrayList();

      while(true) {
         while(var0 != null) {
            String[] var3;
            if ((var3 = multiSplit(var0, var1)) == null) {
               var2.add(var0);
               var0 = null;
            } else {
               for(int var4 = 0; var4 < var3.length - 1; ++var4) {
                  var2.add(var3[var4]);
               }

               var0 = var3[var3.length - 1];
            }
         }

         return (String[])var2.toArray(new String[var2.size()]);
      }
   }

   public static String before(String var0, char var1) {
      int var2;
      return (var2 = var0.indexOf(var1)) == -1 ? null : var0.substring(0, var2);
   }

   public static String before(String var0, String var1) {
      int var2;
      return (var2 = var0.indexOf(var1)) == -1 ? null : var0.substring(0, var2);
   }

   public static String beforeLast(String var0, char var1) {
      int var2;
      return (var2 = var0.lastIndexOf(var1)) == -1 ? null : var0.substring(0, var2);
   }

   public static String beforeLast(String var0, String var1) {
      int var2;
      return (var2 = var0.lastIndexOf(var1)) == -1 ? null : var0.substring(0, var2);
   }

   public static String after(String var0, char var1) {
      int var2;
      return (var2 = var0.indexOf(var1)) == -1 ? null : var0.substring(var2 + 1);
   }

   public static String after(String var0, String var1) {
      int var2;
      return (var2 = var0.indexOf(var1)) == -1 ? null : var0.substring(var2 + var1.length());
   }

   public static String afterLast(String var0, char var1) {
      int var2;
      return (var2 = var0.lastIndexOf(var1)) == -1 ? null : var0.substring(var2 + 1);
   }

   public static String afterLast(String var0, String var1) {
      int var2;
      return (var2 = var0.lastIndexOf(var1)) == -1 ? null : var0.substring(var2 + var1.length());
   }

   public static String betweenOuter(String var0, char var1, char var2) {
      return (var0 = after(var0, var1)) == null ? null : beforeLast(var0, var2);
   }

   public static String betweenOuter(String var0, String var1, String var2) {
      return (var0 = after(var0, var1)) == null ? null : beforeLast(var0, var2);
   }

   public static String between(String var0, char var1, char var2) {
      return (var0 = after(var0, var1)) == null ? null : before(var0, var2);
   }

   public static String between(String var0, String var1, String var2) {
      return (var0 = after(var0, var1)) == null ? null : before(var0, var2);
   }

   public static String between(String var0, char var1, char var2, String var3) {
      return replaceOnNull(between(var0, var1, var2), var3);
   }

   public static String between(String var0, String var1, String var2, String var3) {
      return replaceOnNull(between(var0, var1, var2), var3);
   }

   public static String[] multiBetween(String var0, String var1, String var2) {
      ArrayList var3;
      String[] var4;
      for(var3 = new ArrayList(); (var0 = after(var0, var1)) != null && (var4 = splitPair(var0, var2)) != null; var0 = var4[1]) {
         var3.add(var4[0]);
      }

      return (String[])var3.toArray(new String[var3.size()]);
   }

   private static String replaceOnNull(String var0, String var1) {
      return var0 == null ? var1 : var0;
   }

   public static String beforeIfAny(String var0, char var1) {
      return replaceOnNull(before(var0, var1), var0);
   }

   public static String beforeIfAny(String var0, String var1) {
      return replaceOnNull(before(var0, var1), var0);
   }

   public static String beforeLastIfAny(String var0, char var1) {
      return replaceOnNull(beforeLast(var0, var1), var0);
   }

   public static String beforeLastIfAny(String var0, String var1) {
      return replaceOnNull(beforeLast(var0, var1), var0);
   }

   public static String afterIfAny(String var0, char var1) {
      return replaceOnNull(after(var0, var1), var0);
   }

   public static String afterIfAny(String var0, String var1) {
      return replaceOnNull(after(var0, var1), var0);
   }

   public static String afterLastIfAny(String var0, char var1) {
      return replaceOnNull(afterLast(var0, var1), var0);
   }

   public static String afterLastIfAny(String var0, String var1) {
      return replaceOnNull(afterLast(var0, var1), var0);
   }

   public static String before(String var0, char var1, String var2) {
      return replaceOnNull(before(var0, var1), var2);
   }

   public static String before(String var0, String var1, String var2) {
      return replaceOnNull(before(var0, var1), var2);
   }

   public static String beforeLast(String var0, char var1, String var2) {
      return replaceOnNull(beforeLast(var0, var1), var2);
   }

   public static String beforeLast(String var0, String var1, String var2) {
      return replaceOnNull(beforeLast(var0, var1), var2);
   }

   public static String after(String var0, char var1, String var2) {
      return replaceOnNull(after(var0, var1), var2);
   }

   public static String after(String var0, String var1, String var2) {
      return replaceOnNull(after(var0, var1), var2);
   }

   public static String afterLast(String var0, char var1, String var2) {
      return replaceOnNull(afterLast(var0, var1), var2);
   }

   public static String afterLast(String var0, String var1, String var2) {
      return replaceOnNull(afterLast(var0, var1), var2);
   }

   public static final int[] indicesOf(String var0, char var1) {
      int[] var2 = new int[count(var0, var1)];
      int var3 = 0;

      for(int var4 = 0; var3 < var0.length(); ++var3) {
         if (var0.charAt(var3) == var1) {
            var2[var4++] = var3;
         }
      }

      return var2;
   }

   public static final int[] indicesOf(String var0, String var1) {
      if (var1.length() == 0) {
         throw new IllegalArgumentException("empty match");
      } else {
         int[] var2 = new int[count(var0, var1)];
         int var3 = 0;

         for(int var4 = 0; (var4 = var0.indexOf(var1, var4)) != -1; var4 += var1.length()) {
            var2[var3++] = var4;
         }

         return var2;
      }
   }

   public static final int count(String var0, char var1) {
      int var2 = var0.length();
      int var3 = 0;

      for(int var4 = 0; var4 < var2; ++var4) {
         if (var0.charAt(var4) == var1) {
            ++var3;
         }
      }

      return var3;
   }

   public static final int count(String var0, String var1) {
      int var2 = 0;

      for(int var3 = 0; (var3 = var0.indexOf(var1, var3)) != -1; var3 += var1.length()) {
         ++var2;
      }

      return var2;
   }

   public static String repeat(String var0, int var1) {
      if (var1 < 0) {
         throw new IllegalArgumentException();
      } else if (var1 == 0) {
         return "";
      } else if (var1 == 1) {
         return var0;
      } else {
         StringBuilder var2 = new StringBuilder();

         for(int var3 = 0; var3 < var1; ++var3) {
            var2.append(var0);
         }

         return var2.toString();
      }
   }

   public static String repeat(char var0, int var1) {
      if (var1 < 0) {
         throw new IllegalArgumentException();
      } else if (var1 == 0) {
         return "";
      } else if (var1 == 1) {
         return String.valueOf(var0);
      } else {
         char[] var2 = new char[var1];

         for(int var3 = 0; var3 < var1; ++var3) {
            var2[var3] = var0;
         }

         return new String(var2);
      }
   }

   public static final String replaceBetweenIfAny(String var0, String var1, String var2, String var3) {
      int var4;
      if ((var4 = var0.indexOf(var1)) != -1 && var0.indexOf(var2, var4 + var1.length()) != -1) {
         String var6;
         if ((var6 = before(var0, var1)) == null) {
            return var0;
         } else {
            String var5;
            return (var5 = after(after(var0, var1), var2)) == null ? var0 : var6 + var1 + var3 + var2 + var5;
         }
      } else {
         return var0;
      }
   }

   public static final String replaceAllBetween(String var0, String var1, String var2, String var3) {
      if (var0.indexOf(var1) != -1 && var0.indexOf(var2) != -1) {
         StringBuilder var4;
         int var5;
         int var6;
         for(var4 = new StringBuilder(); (var5 = var0.indexOf(var1)) != -1 && (var6 = var0.indexOf(var2, var5 + var1.length())) != -1; var0 = var0.substring(var6 + var2.length())) {
            String var7 = var0.substring(0, var5);
            var4.append(var7);
            if (var3 != null) {
               var4.append(var3);
            }
         }

         return var4.append(var0).toString();
      } else {
         return var0;
      }
   }

   public static final String replace(String var0, char var1, char var2) {
      if (var0.indexOf(var1) == -1) {
         return var0;
      } else {
         char[] var4 = var0.toCharArray();

         for(int var3 = 0; var3 < var4.length; ++var3) {
            if (var4[var3] == var1) {
               var4[var3] = var2;
            }
         }

         return new String(var4);
      }
   }

   public static final String replace(String var0, String var1, String var2) {
      if (var0.indexOf(var1) == -1) {
         return var0;
      } else {
         StringBuilder var3 = new StringBuilder();
         int var4 = var1.length();

         int var5;
         int var6;
         for(var5 = 0; (var6 = var0.indexOf(var1, var5)) != -1; var5 += var6 - var5 + var4) {
            var3.append(var0.substring(var5, var6));
            var3.append(var2);
         }

         var3.append(var0.substring(Math.min(var5, var0.length())));
         return var3.toString();
      }
   }

   public static final String replaceIfEquals(String var0, String var1, String var2) {
      if (var0 == null) {
         return var1 == null ? var2 : var0;
      } else {
         return var0.equals(var1) ? var2 : var0;
      }
   }

   public static final String removeInnerIfAny(String var0, String var1, String var2) {
      String var3;
      if ((var3 = before(var0, var1)) == null) {
         return var0;
      } else {
         String var4;
         if ((var4 = after(var0, var1)).startsWith(var2)) {
            return var0;
         } else {
            return (var4 = after(var4, var2)) == null ? var0 : var3 + var1 + var2 + var4;
         }
      }
   }

   public static final String removeOuterIfAny(String var0, String var1, String var2) {
      String var3;
      if ((var3 = before(var0, var1)) == null) {
         return var0;
      } else {
         return (var1 = after(after(var0, var1), var2)) == null ? var0 : var3 + var1;
      }
   }

   public static final String removeAllInner(String var0, String var1, String var2) {
      String var3;
      while((var3 = removeInnerIfAny(var0, var1, var2)) != var0) {
         var0 = var3;
      }

      return var0;
   }

   public static final String removeAllOuter(String var0, String var1, String var2) {
      String var3;
      while((var3 = removeOuterIfAny(var0, var1, var2)) != var0) {
         var0 = var3;
      }

      return var0;
   }

   public static final String remove(String var0, char var1) {
      int var2;
      if ((var2 = count(var0, var1)) == 0) {
         return var0;
      } else {
         char[] var5;
         char[] var6 = new char[(var5 = var0.toCharArray()).length - var2];
         int var3 = 0;

         for(int var4 = 0; var3 < var5.length; ++var3) {
            if (var5[var3] != var1) {
               var6[var4++] = var5[var3];
            }
         }

         return new String(var6);
      }
   }

   public static final String remove(String var0, char... var1) {
      int var2 = 0;
      char[] var3 = var1;
      int var4 = var1.length;

      int var5;
      for(var5 = 0; var5 < var4; ++var5) {
         char var6 = var3[var5];
         var2 += count(var0, var6);
      }

      if (var2 == 0) {
         return var0;
      } else {
         char[] var8 = new char[(var3 = var0.toCharArray()).length - var2];
         var5 = 0;

         label30:
         for(int var9 = 0; var5 < var3.length; ++var5) {
            for(int var7 = 0; var7 < var1.length; ++var7) {
               if (var3[var5] == var1[var7]) {
                  continue label30;
               }
            }

            var8[var9++] = var3[var5];
         }

         return new String(var8);
      }
   }

   public static final String remove(String var0, String var1) {
      return replace(var0, var1, "");
   }

   public static final String convertWhiteSpaceTo(String var0, char var1) {
      char[] var3 = var0.toCharArray();

      for(int var2 = 0; var2 < var3.length; ++var2) {
         if (var3[var2] <= ' ') {
            var3[var2] = var1;
         }
      }

      return new String(var3);
   }

   public static final int indexOfWhiteSpace(String var0) {
      for(int var1 = 0; var1 < var0.length(); ++var1) {
         if (var0.charAt(var1) <= ' ') {
            return var1;
         }
      }

      return -1;
   }

   public static final int lastIndexOfWhiteSpace(String var0) {
      for(int var1 = var0.length() - 1; var1 >= 0; --var1) {
         if (var0.charAt(var1) <= ' ') {
            return var1;
         }
      }

      return -1;
   }

   public static final String[] splitOnWhiteSpace(String var0) {
      return split(removeDuplicates(convertWhiteSpaceTo(var0, ' '), ' '), ' ');
   }

   public static final String[] splitPairOnWhiteSpace(String var0) {
      return splitPair(removeDuplicates(convertWhiteSpaceTo(var0, ' '), ' '), ' ');
   }

   public static final String trimBefore(String var0) {
      for(int var1 = 0; var1 < var0.length(); ++var1) {
         if (var0.charAt(var1) > ' ') {
            return var0.substring(var1);
         }
      }

      return "";
   }

   public static final String removeDuplicates(String var0, char var1) {
      if (var0.indexOf(var1) == -1) {
         return var0;
      } else if (var0.indexOf(var1) == var0.lastIndexOf(var1)) {
         return var0;
      } else {
         char[] var5 = var0.toCharArray();
         StringBuilder var2 = new StringBuilder();
         boolean var3 = false;

         for(int var4 = 0; var4 < var5.length; ++var4) {
            if (var5[var4] != var1) {
               var2.append(var5[var4]);
               var3 = false;
            } else if (!var3) {
               var2.append(var5[var4]);
               var3 = true;
            }
         }

         return var2.toString();
      }
   }

   public static final boolean contains(String[] var0, String var1) {
      int var2;
      int var3;
      if (var1 == null) {
         var2 = (var0 = var0).length;

         for(var3 = 0; var3 < var2; ++var3) {
            if (var0[var3] == null) {
               return true;
            }
         }
      } else {
         var2 = (var0 = var0).length;

         for(var3 = 0; var3 < var2; ++var3) {
            String var4 = var0[var3];
            if (var1.equals(var4)) {
               return true;
            }
         }
      }

      return false;
   }

   public static final String capitalize(String var0) {
      if (var0.length() == 0) {
         return var0;
      } else {
         return var0.length() == 1 ? var0.toUpperCase(Locale.ENGLISH) : var0.substring(0, 1).toUpperCase(Locale.ENGLISH) + var0.substring(1);
      }
   }

   public static final String decapitalize(String var0) {
      if (var0.length() == 0) {
         return var0;
      } else {
         return var0.length() == 1 ? var0.toLowerCase(Locale.ENGLISH) : var0.substring(0, 1).toLowerCase(Locale.ENGLISH) + var0.substring(1);
      }
   }

   public static final Map parseProperties(String var0) {
      HashMap var1 = new HashMap();
      String[] var6;
      int var2 = (var6 = splitOnLines(var0)).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         String var4;
         String[] var7;
         if ((var4 = beforeIfAny(var6[var3], '#').trim()).length() != 0 && (var7 = splitPair(var4, '=')) != null) {
            String var5 = var7[0].trim();
            var4 = var7[1].trim();
            var1.put(var5, var4);
         }
      }

      return var1;
   }

   public static final Map parseArgs(String var0, String var1, String var2) {
      String[] var7 = split(var0, var1);
      String[] var8 = new String[2];
      HashMap var3 = new HashMap();
      int var4 = (var7 = var7).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         String var6;
         if ((var6 = var7[var5]).contains(String.valueOf(var2))) {
            splitPair(var6, var2, var8);
            var3.put(var8[0], var8[1]);
         }
      }

      return var3;
   }

   public static final Map parseArgs(String var0, char var1, char var2) {
      String[] var7 = split(var0, var1);
      String[] var8 = new String[2];
      HashMap var3 = new HashMap();
      int var4 = (var7 = var7).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         String var6;
         if (splitPair(var6 = var7[var5], var2, var8) == null) {
            var3.put(var6, "");
         } else {
            var3.put(var8[0], var8[1]);
         }
      }

      return var3;
   }

   public static final String joinArgs(Map var0, String var1, String var2) {
      StringBuilder var3 = new StringBuilder();
      Iterator var4 = var0.keySet().iterator();

      while(var4.hasNext()) {
         String var5 = (String)var4.next();
         var3.append(var5);
         var3.append(var2);
         var3.append((String)var0.get(var5));
         var3.append(var1);
      }

      if (var3.length() > 0) {
         var3.setLength(var3.length() - 1);
      }

      return var3.toString();
   }

   public static final String joinArgs(Map var0, char var1, char var2) {
      StringBuilder var3 = new StringBuilder();
      Iterator var4 = var0.keySet().iterator();

      while(var4.hasNext()) {
         String var5 = (String)var4.next();
         var3.append(var5);
         var3.append(var2);
         var3.append((String)var0.get(var5));
         var3.append(var1);
      }

      if (var3.length() > 0) {
         var3.setLength(var3.length() - 1);
      }

      return var3.toString();
   }

   public static final String decodeURL(String var0, String var1) {
      return decodeURL(var0, var1, true);
   }

   public static final String decodeURL(String var0, String var1, boolean var2) {
      if (var2) {
         var0 = replace(var0, '+', ' ');
      }

      StringBuilder var11 = new StringBuilder();
      ByteList var3 = new ByteList();
      Charset var4 = Charset.forName(var1);
      String[] var10 = split(var0, '%');

      for(int var5 = 0; var5 < var10.length; ++var5) {
         String var6 = var10[var5];
         if (var5 == 0) {
            var11.append(var6);
         } else if (!var6.isEmpty()) {
            int var7;
            if (var6.startsWith("u")) {
               if (var3.size() > 0) {
                  var11.append(var4.decode(ByteBuffer.wrap(var3.toArray())).toString());
                  var3.clear();
               }

               try {
                  var7 = 0 | chr2oct(var6.charAt(1)) << 12 | chr2oct(var6.charAt(2)) << 8 | chr2oct(var6.charAt(3)) << 4 | chr2oct(var6.charAt(4));
                  var11.append((char)var7);
                  var11.append(var6.substring(5));
               } catch (IllegalStateException var8) {
                  var11.append(var6);
               }
            } else {
               try {
                  var7 = 0 | chr2oct(var6.charAt(0)) << 4 | chr2oct(var6.charAt(1));
                  var3.add((byte)var7);
                  if (var6.length() == 2) {
                     continue;
                  }
               } catch (IllegalStateException var9) {
                  var11.append(var6);
               }

               if (var3.size() > 0) {
                  var11.append(Charset.forName(var1).decode(ByteBuffer.wrap(var3.toArray())).toString());
                  var3.clear();
               }

               var11.append(var6.substring(2));
            }
         }
      }

      if (var3.size() > 0) {
         var11.append(Charset.forName(var1).decode(ByteBuffer.wrap(var3.toArray())).toString());
         var3.clear();
      }

      return var11.toString();
   }

   private static int chr2oct(char var0) {
      if (var0 >= '0' && var0 <= '9') {
         return var0 - 48;
      } else if (var0 >= 'a' && var0 <= 'f') {
         return var0 + 10 - 97;
      } else if (var0 >= 'A' && var0 <= 'F') {
         return var0 + 10 - 65;
      } else {
         throw new IllegalStateException();
      }
   }

   public static final void mainArgsLookup(String[] var0, Map var1) {
      Iterator var2 = var1.keySet().iterator();

      while(var2.hasNext()) {
         String var3 = (String)var2.next();
         String var4;
         if ((var4 = mainArgsLookup(var0, var3)) == null) {
            if (var1.get(var3) == null) {
               throw new IllegalStateException("parameter for -" + var3 + " not found, and no default value");
            }
         } else {
            var1.put(var3, var4);
         }
      }

   }

   public static final String mainArgsLookup(String[] var0, String var1) {
      return mainArgsLookup(var0, var1, (String)null);
   }

   public static final String mainArgsLookup(String[] var0, String var1, String var2) {
      return mainArgsLookup(var0, var1, var2, 0);
   }

   public static final List mainArgsLookups(String[] var0, String var1) {
      var1 = "-" + var1;
      ArrayList var2 = new ArrayList();

      for(int var3 = 0; var3 < var0.length - 1; ++var3) {
         if (var0[var3].equals(var1)) {
            ++var3;
            var2.add(var0[var3]);
         }
      }

      return var2;
   }

   public static final String mainArgsLookup(String[] var0, String var1, String var2, int var3) {
      var1 = "-" + var1;

      for(int var4 = 0; var4 < var0.length - 1; ++var4) {
         if (var0[var4].equals(var1) && var3-- == 0) {
            return var0[var4 + 1];
         }
      }

      return var2;
   }

   public static boolean areCharsAt(String var0, char var1, int var2, int var3) {
      return var0.charAt(var2) == var1 && var0.charAt(var3) == var1;
   }

   public static boolean areDigits(String var0, int var1, int var2) {
      for(var2 += var1; var1 < var2; ++var1) {
         if (var0.charAt(var1) < '0' || var0.charAt(var1) > '9') {
            return false;
         }
      }

      return true;
   }

   public static final String rot13(String var0) {
      char[] var6 = var0.toCharArray();
      int var1 = 0;
      char[] var2 = var6;
      int var3 = var6.length;

      for(int var4 = 0; var4 < var3; ++var4) {
         char var5;
         if ((var5 = var2[var4]) >= 'a' && var5 <= 'z') {
            var5 = (char)((char)((char)((char)(var5 - 97) + 13) % 26) + 97);
         } else if (var5 >= 'A' && var5 <= 'Z') {
            var5 = (char)((char)((char)((char)(var5 - 65) + 13) % 26) + 65);
         } else if (var5 >= '0' && var5 <= '9') {
            var5 = (char)((char)((char)((char)(var5 - 48) + 5) % 10) + 48);
         }

         var6[var1++] = var5;
      }

      return new String(var6);
   }
}
