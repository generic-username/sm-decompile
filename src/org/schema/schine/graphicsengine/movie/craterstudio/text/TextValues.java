package org.schema.schine.graphicsengine.movie.craterstudio.text;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;

public class TextValues {
   private static char[] hexTableUpper = "0123456789ABCDEF".toCharArray();
   private static final String zeros = new String("0000000000000000000000000000000000");
   private static final String[] mags = new String[]{"n", "u", "m", "", "k", "M", "G", "T", "P"};

   public static boolean isBoolean(String var0) {
      if (var0 == null) {
         return false;
      } else if (var0.toLowerCase(Locale.ENGLISH).equals("true")) {
         return true;
      } else {
         return var0.toLowerCase(Locale.ENGLISH).equals("false");
      }
   }

   public static boolean isInt(String var0) {
      if (var0 == null) {
         return false;
      } else {
         try {
            Integer.parseInt(var0);
            return true;
         } catch (NumberFormatException var1) {
            return false;
         }
      }
   }

   public static boolean isLong(String var0) {
      if (var0 == null) {
         return false;
      } else {
         try {
            Long.parseLong(var0);
            return true;
         } catch (NumberFormatException var1) {
            return false;
         }
      }
   }

   public static boolean isFloat(String var0) {
      if (var0 == null) {
         return false;
      } else {
         try {
            Float.parseFloat(var0);
            return true;
         } catch (NumberFormatException var1) {
            return false;
         }
      }
   }

   public static boolean isDouble(String var0) {
      if (var0 == null) {
         return false;
      } else {
         try {
            Double.parseDouble(var0);
            return true;
         } catch (NumberFormatException var1) {
            return false;
         }
      }
   }

   public static boolean tryParseBoolean(String var0, boolean var1) {
      return var0 == null ? var1 : Boolean.parseBoolean(var0);
   }

   public static int tryParseInt(String var0, int var1) {
      if (var0 == null) {
         return var1;
      } else {
         try {
            return Integer.parseInt(var0);
         } catch (NumberFormatException var2) {
            return var1;
         }
      }
   }

   public static long tryParseLong(String var0, long var1) {
      if (var0 == null) {
         return var1;
      } else {
         try {
            return Long.parseLong(var0);
         } catch (NumberFormatException var3) {
            return var1;
         }
      }
   }

   public static float tryParseFloat(String var0, float var1) {
      if (var0 == null) {
         return var1;
      } else {
         try {
            return Float.parseFloat(var0);
         } catch (NumberFormatException var2) {
            return var1;
         }
      }
   }

   public static double tryParseDouble(String var0, double var1) {
      if (var0 == null) {
         return var1;
      } else {
         try {
            return Double.parseDouble(var0);
         } catch (NumberFormatException var3) {
            return var1;
         }
      }
   }

   public static final String[] trim(String[] var0) {
      String[] var1 = new String[var0.length];

      for(int var2 = 0; var2 < var1.length; ++var2) {
         var1[var2] = var0[var2].trim();
      }

      return var1;
   }

   public static final boolean[] parseBooleans(String[] var0) {
      boolean[] var1 = new boolean[var0.length];

      for(int var2 = 0; var2 < var1.length; ++var2) {
         if (var0[var2].equals("true")) {
            var1[var2] = true;
         } else {
            if (!var0[var2].equals("false")) {
               throw new IllegalArgumentException("value: " + var0[var2]);
            }

            var1[var2] = false;
         }
      }

      return var1;
   }

   public static final long[] parseLongs(String[] var0) {
      long[] var1 = new long[var0.length];

      for(int var2 = 0; var2 < var1.length; ++var2) {
         var1[var2] = Long.parseLong(var0[var2], 10);
      }

      return var1;
   }

   public static final double[] parseDoubles(String[] var0) {
      double[] var1 = new double[var0.length];

      for(int var2 = 0; var2 < var1.length; ++var2) {
         var1[var2] = Double.parseDouble(var0[var2]);
      }

      return var1;
   }

   public static final int[] parseInts(String[] var0) {
      return parseInts(var0, 0, var0.length, 10);
   }

   public static final int[] parseInts(String[] var0, int var1) {
      return parseInts(var0, 0, var0.length, var1);
   }

   public static final int[] parseInts(String[] var0, int var1, int var2) {
      return parseInts(var0, var1, var2, 10);
   }

   public static final int[] parseInts(String[] var0, int var1, int var2, int var3) {
      int[] var4 = new int[var2];

      for(int var5 = 0; var5 < var2; ++var5) {
         var4[var5] = Integer.parseInt(var0[var1 + var5], var3);
      }

      return var4;
   }

   public static final float[] parseFloats(String[] var0) {
      return parseFloats(var0, 0, var0.length);
   }

   public static final float[] parseFloats(String[] var0, int var1, int var2) {
      float[] var3 = new float[var2];

      for(int var4 = 0; var4 < var2; ++var4) {
         var3[var4] = Float.parseFloat(var0[var1 + var4]);
      }

      return var3;
   }

   public static final String nanosToTime(long var0) {
      if (var0 == 0L) {
         return "n/a";
      } else if (var0 < 10000L) {
         return var0 + "ns";
      } else {
         long var2;
         if ((var2 = var0 / 1000L) < 10000L) {
            return var2 + "us";
         } else {
            long var4;
            return (var4 = var2 / 1000L) < 10000L ? var4 + "ms" : var4 / 1000L + "s";
         }
      }
   }

   public static final String nanosAsMicroSeconds(long var0) {
      return formatNumber((double)var0 / 1.0E9D, 6) + "s";
   }

   public static final String valueOf(int[] var0) {
      return valueOf((int[])var0, 0, var0.length);
   }

   public static final String valueOf(float[] var0) {
      return valueOf((float[])var0, 0, var0.length);
   }

   public static final String valueOf(int[] var0, int var1, int var2) {
      if (var2 == 0) {
         return "";
      } else {
         StringBuffer var3 = new StringBuffer();

         for(int var4 = 0; var4 < var2; ++var4) {
            var3.append(var0[var1 + var4]);
            var3.append(", ");
         }

         var3.setLength(var3.length() - 2);
         return var3.toString();
      }
   }

   public static final String valueOf(float[] var0, int var1, int var2) {
      if (var2 == 0) {
         return "";
      } else {
         StringBuffer var3 = new StringBuffer();

         for(int var4 = 0; var4 < var2; ++var4) {
            var3.append(var0[var1 + var4]);
            var3.append(", ");
         }

         var3.setLength(var3.length() - 2);
         return var3.toString();
      }
   }

   private static final int h2i(int var0) {
      if (var0 >= 48 && var0 <= 57) {
         return var0 - 48;
      } else if (var0 >= 97 && var0 <= 102) {
         return var0 - 97 + 10;
      } else if (var0 >= 65 && var0 <= 70) {
         return var0 - 65 + 10;
      } else {
         throw new IllegalArgumentException("cur=" + var0);
      }
   }

   public static final String hexDecode(String var0) {
      if (var0 == null) {
         return null;
      } else if (var0.length() % 2 != 0) {
         throw new IllegalArgumentException();
      } else {
         char[] var1 = new char[var0.length() / 2];
         int var2 = 0;

         for(int var3 = 0; var2 < var1.length; ++var2) {
            var1[var2] = (char)(h2i(var0.charAt(var3++)) << 4 | h2i(var0.charAt(var3++)));
         }

         return new String(var1);
      }
   }

   public static final String hexEncode(String var0) {
      if (var0 == null) {
         return null;
      } else {
         char[] var1 = new char[var0.length() << 1];
         int var2 = 0;

         for(int var3 = 0; var2 < var0.length(); var3 += 2) {
            char var4 = var0.charAt(var2);
            var1[var3] = hexTableUpper[(var4 & 240) >> 4];
            var1[var3 + 1] = hexTableUpper[var4 & 15];
            ++var2;
         }

         return new String(var1);
      }
   }

   public static int hexDecodeDigit(char var0) {
      if (var0 >= '0' && var0 <= '9') {
         return var0 - 48;
      } else if (var0 >= 'a' && var0 <= 'f') {
         return 10 + (var0 - 97);
      } else if (var0 >= 'A' && var0 <= 'F') {
         return 10 + (var0 - 65);
      } else {
         throw new IllegalStateException();
      }
   }

   public static final String hexRawEncode(byte[] var0) {
      char[] var1 = new char[var0.length << 1];

      for(int var2 = 0; var2 < var0.length; ++var2) {
         for(int var3 = 0; var3 < 2; ++var3) {
            var1[(var2 << 1) + (1 - var3)] = hexTableUpper[(var0[var2] & 255) >>> (var3 << 2) & 15];
         }
      }

      return new String(var1);
   }

   public static final byte[] hexRawDecode(String var0) {
      if (var0.length() % 2 != 0) {
         throw new IllegalStateException();
      } else {
         byte[] var1 = new byte[var0.length() >> 1];

         for(int var2 = 0; var2 < var1.length; ++var2) {
            var1[var2] = (byte)Integer.parseInt(var0.substring(var2 << 1, (var2 << 1) + 2), 16);
         }

         return var1;
      }
   }

   public static final String hexValueOf(byte var0) {
      char[] var1 = new char[2];

      for(int var2 = 0; var2 < 2; ++var2) {
         var1[1 - var2] = hexTableUpper[var0 >>> (var2 << 2) & 15];
      }

      return new String(var1);
   }

   public static final String hexValueOf(short var0) {
      char[] var1 = new char[4];

      for(int var2 = 0; var2 < 4; ++var2) {
         var1[3 - var2] = hexTableUpper[var0 >>> (var2 << 2) & 15];
      }

      return new String(var1);
   }

   public static final String hexValueOf(int var0) {
      char[] var1 = new char[8];

      for(int var2 = 0; var2 < 8; ++var2) {
         var1[7 - var2] = hexTableUpper[var0 >>> (var2 << 2) & 15];
      }

      return new String(var1);
   }

   public static final String hexValueOf(long var0) {
      char[] var2 = new char[16];

      for(int var3 = 0; var3 < 16; ++var3) {
         var2[15 - var3] = hexTableUpper[(int)(var0 >>> (var3 << 2)) & 15];
      }

      return new String(var2);
   }

   public static final String binValueOf(byte var0) {
      char[] var1 = new char[8];

      for(int var2 = 0; var2 < 8; ++var2) {
         var1[7 - var2] = (char)(48 + (var0 >>> var2 & 1));
      }

      return new String(var1);
   }

   public static final String binValueOf(short var0) {
      char[] var1 = new char[3216];

      for(int var2 = 0; var2 < 16; ++var2) {
         var1[15 - var2] = (char)(48 + (var0 >>> var2 & 1));
      }

      return new String(var1);
   }

   public static final String binValueOf(int var0) {
      char[] var1 = new char[32];

      for(int var2 = 0; var2 < 32; ++var2) {
         var1[31 - var2] = (char)(48 + (var0 >>> var2 & 1));
      }

      return new String(var1);
   }

   public static final String binValueOf(long var0) {
      char[] var2 = new char[64];

      for(int var3 = 0; var3 < 64; ++var3) {
         var2[63 - var3] = (char)((int)(48L + (var0 >>> var3 & 1L)));
      }

      return new String(var2);
   }

   public static final int parseIntMagnitude(String var0) {
      long var1;
      if ((var1 = parseLongMagnitude(var0)) >= -2147483648L && var1 <= 2147483647L) {
         return (int)var1;
      } else {
         throw new IllegalStateException("value too large too fit in integer");
      }
   }

   public static final long parseLongMagnitude(String var0) {
      if ((var0 = var0.trim()).isEmpty()) {
         return 0L;
      } else {
         long var1 = 1L;
         switch(var0.charAt(var0.length() - 1)) {
         case 'T':
            var1 = 1024L;
         case 'G':
            var1 <<= 10;
         case 'M':
            var1 <<= 10;
         case 'K':
            var1 <<= 10;
            break;
         case 'y':
            var1 = 365L;
         case 'w':
            var1 *= 7L;
         case 'd':
            var1 *= 24L;
         case 'h':
            var1 *= 60L;
         case 'm':
            var1 *= 60L;
         case 's':
            var1 *= 1000L;
         }

         if (var1 != 1L) {
            var0 = var0.substring(0, var0.length() - 1);
         }

         return Long.parseLong(var0) * var1;
      }
   }

   public static final String magnitudePeriods(int var0, char var1) {
      boolean var2;
      if (var2 = var0 < 0) {
         var0 = -var0;
      }

      String var4 = String.valueOf(var0);

      String var3;
      for(var3 = ""; var4.length() > 3; var4 = var4.substring(0, var4.length() - 3)) {
         var3 = var1 + var4.substring(var4.length() - 3) + var3;
      }

      return (var2 ? "-" : "") + var4 + var3;
   }

   public static final String formatCurrencyInCents(int var0, boolean var1, boolean var2) {
      int var3 = var0 / 100;
      var0 %= 100;
      int var4 = var1 ? 44 : 46;
      int var6 = var1 ? 46 : 44;
      String var5 = var6 + formatNumber(var0, 2);
      return var2 ? magnitudePeriods(var3, (char)var4) + var5 : var3 + var5;
   }

   public static final String formatNumber(int var0, int var1) {
      boolean var2 = var0 < 0;
      String var3 = "";
      if (var2) {
         var3 = "-";
         var0 = -var0;
      }

      String var4;
      if ((var4 = String.valueOf(var0)).length() < var1) {
         var4 = zeros.substring(0, var1 - var4.length()) + var4;
      }

      return var3 + var4;
   }

   public static final String formatNumber(double var0, int var2) {
      return formatNumber(var0, 1, var2);
   }

   public static final String formatNumber(double var0, int var2, int var3) {
      if (Double.isNaN(var0)) {
         return "NaN";
      } else if (Double.isInfinite(var0)) {
         return "Infinity";
      } else {
         NumberFormat var4;
         (var4 = DecimalFormat.getInstance()).setGroupingUsed(false);
         var4.setMinimumFractionDigits(var3);
         var4.setMaximumFractionDigits(var3);
         var4.setMinimumIntegerDigits(var0 < 0.0D ? var2 - 1 : var2);
         var4.setMaximumIntegerDigits(Integer.MAX_VALUE);
         return var4.format(var0);
      }
   }

   public static final String formatedValueOf(float[] var0, int var1, int var2) {
      if (var0.length == 0) {
         return "";
      } else {
         StringBuffer var3 = new StringBuffer();

         for(int var4 = 0; var4 < var0.length; ++var4) {
            var3.append(formatNumber((double)var0[var4], var1, var2));
            var3.append(", ");
         }

         var3.setLength(var3.length() - 2);
         return var3.toString();
      }
   }

   public static final String formatWithMagnitude(double var0, int var2) {
      return formatWithMagnitude(var0, var2, 1000.0D);
   }

   public static final String formatWithMagnitudeBytes(double var0, int var2) {
      return formatWithMagnitude(var0, var2, 1024.0D);
   }

   private static final String formatWithMagnitude(double var0, int var2, double var3) {
      if (var0 == 0.0D) {
         return formatNumber(var0, var2);
      } else {
         int var5 = 3;
         boolean var6;
         if (var6 = var0 < 0.0D) {
            var0 = -var0;
         }

         if (var0 > 1.0D) {
            while(var0 / var3 > 1.0D) {
               ++var5;
               var0 /= var3;
            }
         } else {
            do {
               --var5;
            } while((var0 *= var3) * var3 < 1.0D);
         }

         if (var6) {
            var0 = -var0;
         }

         return formatNumber(var0, var2) + mags[var5];
      }
   }

   public static final String getCurrentDate() {
      return formatDateTime(System.currentTimeMillis(), "Y-M-D");
   }

   public static final String getCurrentTime() {
      return formatDateTime(System.currentTimeMillis(), "h:m:s");
   }

   public static final String getCurrentDateTime() {
      return formatDateTime(System.currentTimeMillis(), "Y-M-D h:m:s");
   }

   public static final String formatDateTime(long var0, String var2) {
      Calendar var3;
      (var3 = Calendar.getInstance()).setTimeInMillis(var0);
      int var10 = var3.get(1);
      int var1 = var3.get(2) + 1;
      int var4 = var3.get(5);
      int var5 = var3.get(11);
      int var6 = var3.get(12);
      int var7 = var3.get(13);
      int var13 = var3.get(14);
      String var11 = formatNumber((double)var10, 4, 0);
      String var12 = formatNumber((double)var1, 2, 0);
      String var15 = formatNumber((double)var4, 2, 0);
      String var16 = formatNumber((double)var5, 2, 0);
      String var17 = formatNumber((double)var6, 2, 0);
      String var18 = formatNumber((double)var7, 2, 0);
      String var14 = formatNumber((double)var13, 3, 0);
      StringBuffer var8 = new StringBuffer();

      for(int var9 = 0; var9 < var2.length(); ++var9) {
         switch(var2.charAt(var9)) {
         case 'D':
            var8.append(var15);
            break;
         case 'M':
            var8.append(var12);
            break;
         case 'S':
            var8.append(var14);
            break;
         case 'Y':
            var8.append(var11);
            break;
         case 'h':
            var8.append(var16);
            break;
         case 'm':
            var8.append(var17);
            break;
         case 's':
            var8.append(var18);
            break;
         default:
            var8.append(var2.charAt(var9));
         }
      }

      return var8.toString();
   }

   public static final String formatElapsedTime(long var0, String var2) {
      long var3;
      long var5 = (var3 = var0 / 1000L) % 60L;
      long var7 = var3 / 60L % 60L;
      long var9 = var3 / 60L / 60L;
      String var11 = formatNumber((double)var5, 2, 0);
      String var1 = formatNumber((double)var7, 2, 0);
      String var12 = formatNumber((double)var9, 2, 0);
      StringBuffer var4 = new StringBuffer();

      for(int var13 = 0; var13 < var2.length(); ++var13) {
         switch(var2.charAt(var13)) {
         case 'H':
            var4.append(var12);
            break;
         case 'M':
            var4.append(var1);
            break;
         case 'S':
            var4.append(var11);
            break;
         default:
            var4.append(var2.charAt(var13));
         }
      }

      return var4.toString();
   }
}
