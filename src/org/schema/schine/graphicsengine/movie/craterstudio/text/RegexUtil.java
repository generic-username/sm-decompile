package org.schema.schine.graphicsengine.movie.craterstudio.text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtil {
   public static String getMatchRegion(String var0, Matcher var1) {
      return var0.substring(var1.start(), var1.end());
   }

   public static String getMatchRegion(String var0, Matcher var1, int var2) {
      return var0.substring(var1.start(var2), var1.end(var2));
   }

   public static String replace(String var0, Pattern var1, RegexUtil.RegexCallback var2) {
      Matcher var5 = var1.matcher(var0);
      int var3 = 0;

      StringBuilder var4;
      for(var4 = new StringBuilder(); var5.find(); var3 = var5.end()) {
         var4.append(var0.substring(var3, var5.start()));
         String var6;
         if ((var6 = var2.replace(var0, var5)) == null) {
            var6 = var0.substring(var5.start(), var5.end());
         }

         var4.append(var6);
      }

      var4.append(var0.substring(var3));
      return var4.toString();
   }

   public static String findFirst(String var0, Pattern var1, int var2) {
      Matcher var3;
      return !(var3 = var1.matcher(var0)).find() ? null : var0.substring(var3.start(var2), var3.end(var2));
   }

   public static String[] find(String var0, Pattern var1, int... var2) {
      Matcher var5;
      if (!(var5 = var1.matcher(var0)).find()) {
         return null;
      } else {
         String[] var3 = new String[var2.length];

         for(int var4 = 0; var4 < var3.length; ++var4) {
            var3[var4] = var0.substring(var5.start(var2[var4]), var5.end(var2[var4]));
         }

         return var3;
      }
   }

   public interface RegexCallback {
      String replace(String var1, Matcher var2);
   }
}
