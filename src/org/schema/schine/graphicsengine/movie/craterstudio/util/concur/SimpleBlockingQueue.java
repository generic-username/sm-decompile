package org.schema.schine.graphicsengine.movie.craterstudio.util.concur;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import org.schema.schine.graphicsengine.movie.craterstudio.util.HighLevel;

public class SimpleBlockingQueue {
   private static final Object NULL_VALUE = new Object();
   private final BlockingQueue backing;
   private final int capacity;

   public SimpleBlockingQueue() {
      this(new LinkedBlockingQueue(), Integer.MAX_VALUE);
   }

   public SimpleBlockingQueue(int var1) {
      this(new ArrayBlockingQueue(var1), var1);
   }

   private SimpleBlockingQueue(BlockingQueue var1, int var2) {
      this.backing = var1;
      this.capacity = var2;
   }

   public void clear() {
      this.backing.clear();
   }

   public boolean isEmpty() {
      return this.backing.isEmpty();
   }

   public int size() {
      return this.backing.size();
   }

   public int remaining() {
      return this.capacity - this.size();
   }

   public int capacity() {
      return this.capacity;
   }

   public void put(Object var1) {
      var1 = this.wrap(var1);

      try {
         while(true) {
            try {
               this.backing.put(var1);
               return;
            } catch (InterruptedException var2) {
            }
         }
      } catch (IllegalMonitorStateException var3) {
         HighLevel.sleep();
      }
   }

   public boolean offer(Object var1) {
      var1 = this.wrap(var1);

      try {
         return this.backing.offer(var1);
      } catch (IllegalMonitorStateException var2) {
         HighLevel.sleep();
         return false;
      }
   }

   public boolean offer(Object var1, long var2) {
      var1 = this.wrap(var1);

      try {
         return this.backing.offer(var1, var2, TimeUnit.MILLISECONDS);
      } catch (InterruptedException var4) {
         return false;
      } catch (IllegalMonitorStateException var5) {
         HighLevel.sleep();
         return false;
      }
   }

   public Object peek() {
      try {
         return this.unwrap(this.backing.peek());
      } catch (IllegalMonitorStateException var1) {
         HighLevel.sleep();
         return null;
      }
   }

   public Object peek_inefficient(long var1, boolean var3) {
      try {
         long var4 = System.nanoTime() + var1 * 1000000L;

         while(this.backing.peek() == null && System.nanoTime() <= var4) {
            if (var3) {
               Thread.yield();
            } else {
               HighLevel.sleep(1L);
            }
         }

         return this.peek();
      } catch (IllegalMonitorStateException var6) {
         HighLevel.sleep();
         return null;
      }
   }

   public Object poll() {
      try {
         return this.unwrap(this.backing.poll());
      } catch (IllegalMonitorStateException var1) {
         HighLevel.sleep();
         return null;
      }
   }

   public Object poll(long var1) {
      try {
         return this.unwrap(this.backing.poll(var1, TimeUnit.MILLISECONDS));
      } catch (InterruptedException var3) {
         return null;
      } catch (IllegalMonitorStateException var4) {
         HighLevel.sleep();
         return null;
      }
   }

   public Object take() {
      while(true) {
         try {
            return this.unwrap(this.backing.take());
         } catch (InterruptedException var1) {
         } catch (IllegalMonitorStateException var2) {
            HighLevel.sleep();
            return null;
         }
      }
   }

   Object wrap(Object var1) {
      return var1 == null ? NULL_VALUE : var1;
   }

   Object unwrap(Object var1) {
      return var1 == NULL_VALUE ? null : var1;
   }
}
