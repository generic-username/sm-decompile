package org.schema.schine.graphicsengine.movie.craterstudio.util;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicLong;
import javax.swing.JDialog;
import org.schema.schine.graphicsengine.movie.craterstudio.text.Text;
import org.schema.schine.resource.FileExt;

public class HighLevel {
   private static final AtomicLong threadOff = new AtomicLong();

   public static String convertThrowableToString(Throwable var0) {
      ByteArrayOutputStream var1 = new ByteArrayOutputStream();
      PrintStream var2 = new PrintStream(var1);
      var0.printStackTrace(var2);
      return Text.utf8(var1.toByteArray());
   }

   public static List lookupClassPath() {
      ClassLoader var0 = HighLevel.class.getClassLoader();
      ArrayList var1 = new ArrayList();
      if (var0 instanceof URLClassLoader) {
         URL[] var5;
         int var2 = (var5 = ((URLClassLoader)var0).getURLs()).length;

         for(int var3 = 0; var3 < var2; ++var3) {
            String var4;
            if ((var4 = var5[var3].toExternalForm()).startsWith("file:")) {
               var1.add(new FileExt(Text.after(var4, "file:")));
            }
         }
      }

      return var1;
   }

   public static Runnable createMainLauncher(String var0, String var1, String... var2) throws Exception {
      return createMainLauncher(Text.split(var0, File.pathSeparatorChar), var1, var2);
   }

   public static Runnable createMainLauncher(String[] var0, String var1, final String... var2) throws Exception {
      URL[] var3 = new URL[var0.length];

      try {
         for(int var4 = 0; var4 < var3.length; ++var4) {
            var3[var4] = (new FileExt(var0[var4])).toURI().toURL();
         }
      } catch (MalformedURLException var5) {
         throw new IllegalArgumentException(var5);
      }

      URLClassLoader var8 = new URLClassLoader(var3);
      Class[] var6 = new Class[]{String[].class};
      final Method var7 = var8.loadClass(var1).getMethod("main", var6);
      var8.close();
      return new Runnable() {
         public final void run() {
            try {
               var7.invoke((Object)null, var2);
            } catch (InvocationTargetException var2x) {
               if (!(var2x.getCause() instanceof ThreadDeath)) {
                  throw new IllegalStateException(var2x.getCause());
               }
            } catch (Exception var3) {
               var3.printStackTrace();
            }
         }
      };
   }

   public static final int iteratorElements(Iterator var0) {
      int var1;
      for(var1 = 0; var0.hasNext(); ++var1) {
         var0.next();
      }

      return var1;
   }

   public static final Object iteratorElement(Iterator var0, int var1) {
      if (var1 < 0) {
         throw new IllegalArgumentException("n (" + var1 + ") must be >= 0");
      } else {
         Object var3;
         do {
            if (!var0.hasNext()) {
               throw new NoSuchElementException("element @ " + var1 + " not available");
            }

            var3 = var0.next();
         } while(var1-- != 0);

         return var3;
      }
   }

   public static final int indexOfIterator(Iterator var0, Object var1) {
      for(int var2 = 0; var0.hasNext(); ++var2) {
         if (var0.next().equals(var1)) {
            return var2;
         }
      }

      return -1;
   }

   public static final int lastIndexOfIterator(Iterator var0, Object var1) {
      int var2 = -1;

      for(int var3 = 0; var0.hasNext(); ++var3) {
         if (var0.next().equals(var1)) {
            var2 = var3;
         }
      }

      return var2;
   }

   public static final Thread startDaemon(Runnable var0) {
      Thread var1;
      (var1 = new Thread(var0)).setDaemon(true);
      var1.start();
      return var1;
   }

   public static final Thread start(Runnable var0) {
      Thread var1;
      (var1 = new Thread(var0)).start();
      return var1;
   }

   public static final Thread start(Runnable var0, long var1) {
      return start(var0, "CustomStackSize-" + threadOff.getAndIncrement(), var1);
   }

   public static final Thread start(Runnable var0, String var1, long var2) {
      Thread var4;
      (var4 = new Thread(Thread.currentThread().getThreadGroup(), var0, var1, var2)).start();
      return var4;
   }

   public static final void join(Thread var0) {
      while(true) {
         try {
            var0.join();
            return;
         } catch (InterruptedException var1) {
         }
      }
   }

   public static final void wait(Object var0) {
      try {
         var0.wait();
      } catch (InterruptedException var1) {
      }
   }

   public static final void wait(Object var0, long var1) {
      try {
         var0.wait(var1);
      } catch (InterruptedException var3) {
      }
   }

   public static final void notify(Object var0) {
      synchronized(var0) {
         var0.notify();
      }
   }

   public static final void notifyAll(Object var0) {
      synchronized(var0) {
         var0.notifyAll();
      }
   }

   public static final String fieldToString(Class param0, int param1, String param2) {
      // $FF: Couldn't be decompiled
   }

   public static final List fieldToStrings(Class var0, int var1) {
      try {
         LinkedList var2 = new LinkedList();
         Field[] var3;
         int var4 = (var3 = var0.getFields()).length;

         int var5;
         Field var6;
         for(var5 = 0; var5 < var4; ++var5) {
            if ((var6 = var3[var5]).getInt((Object)null) == var1) {
               var2.add(var6.getName());
            }
         }

         var4 = (var3 = var0.getDeclaredFields()).length;

         for(var5 = 0; var5 < var4; ++var5) {
            if (Modifier.isPublic((var6 = var3[var5]).getModifiers()) && var6.getInt((Object)null) == var1) {
               var2.add(var6.getName());
            }
         }

         if (!var2.isEmpty()) {
            return var2;
         }
      } catch (Exception var7) {
         throw new IllegalArgumentException(var7);
      }

      throw new IllegalArgumentException("Field-value (" + var1 + ") not found in class " + var0.getName());
   }

   public static final int stringToField(Class var0, String var1, int var2) {
      try {
         Field var4;
         if ((var4 = var0.getField(var1)) == null) {
            throw new IllegalStateException();
         } else if (var4.getType() != Integer.TYPE) {
            throw new IllegalStateException();
         } else {
            return (Integer)var4.get((Object)null);
         }
      } catch (Exception var3) {
         return var2;
      }
   }

   public static final void setPropertyByPath(Object var0, String var1, Object var2) {
      propertyPathBy(var0, var1, var2, true);
   }

   public static final Object getPropertyByPath(Object var0, String var1) {
      return propertyPathBy(var0, var1, (Object)null, false);
   }

   private static final Object propertyPathBy(Object var0, String var1, Object var2, boolean var3) {
      int var5;
      for(String var4 = var1; (var5 = var1.indexOf(46)) != -1; var1 = var1.substring(var5 + 1)) {
         if ((var0 = getProperty(var0, var1.substring(0, var5))) == null) {
            throw new NullPointerException("\"" + var1.substring(0, var5) + "\"==null in \"" + var4 + "\"");
         }
      }

      if (var3) {
         setProperty(var0, var1, var2);
         return null;
      } else {
         return getProperty(var0, var1);
      }
   }

   public static final void setProperty(Object var0, String var1, Object var2) {
      String var3 = "set" + Text.capitalize(var1);
      Method[] var4;
      int var5 = (var4 = var0.getClass().getMethods()).length;

      for(int var6 = 0; var6 < var5; ++var6) {
         Method var7;
         if ((var7 = var4[var6]).getName().equals(var3)) {
            if (var7.getReturnType() != Void.TYPE) {
               throw new IllegalStateException("Invalid property: " + var1 + ", return-type must be void");
            }

            if (var7.getParameterTypes().length != 1) {
               throw new IllegalStateException("Invalid property: " + var1 + ", must have 1 parameter");
            }

            try {
               var7.invoke(var0, var2);
               return;
            } catch (IllegalArgumentException var8) {
               throw new IllegalStateException(var8.getMessage() + " prop=" + var1 + ", val=" + var2.getClass().getSimpleName() + ", par=" + var7.getParameterTypes()[0].getSimpleName());
            } catch (Exception var9) {
               throw new IllegalStateException(var9);
            }
         }
      }

      throw new IllegalStateException("Couldn't find property: " + var1 + " as " + var3);
   }

   public static final Object getProperty(Object var0, String var1) {
      for(int var2 = 0; var2 < 2; ++var2) {
         String var3 = (var2 == 0 ? "get" : "is") + Text.capitalize(var1);
         Method[] var4;
         int var5 = (var4 = var0.getClass().getMethods()).length;

         for(int var6 = 0; var6 < var5; ++var6) {
            Method var7;
            if ((var7 = var4[var6]).getName().equals(var3)) {
               if (var7.getReturnType() == Void.TYPE) {
                  throw new IllegalStateException("Invalid property: " + var1 + ", return-type must not be void");
               }

               if (var7.getParameterTypes().length != 0) {
                  throw new IllegalStateException("Invalid property: " + var1 + ", must have 0 parameters");
               }

               try {
                  return var7.invoke(var0);
               } catch (Exception var8) {
                  throw new IllegalStateException(var8);
               }
            }
         }
      }

      throw new IllegalStateException("Couldn't find property: " + var1);
   }

   public static final long gc() {
      long var0 = memUsed();

      for(int var2 = 0; var2 < 4; ++var2) {
         System.gc();
      }

      long var4 = memUsed();
      return var0 - var4;
   }

   public static long memUsed() {
      Runtime var0;
      return (var0 = Runtime.getRuntime()).totalMemory() - var0.freeMemory();
   }

   public static final void busySleep(long var0) {
      busyNanoSleep(var0 * 1000000L);
   }

   public static final void busyNanoSleep(long var0) {
      long var2 = System.nanoTime() + var0;

      while(System.nanoTime() < var2) {
         Thread.yield();
      }

   }

   public static final void sleep() {
      try {
         Thread.sleep(315360000000L);
      } catch (InterruptedException var0) {
      }
   }

   public static final void sleep(long var0) {
      sleep(var0, false);
   }

   public static final void sleep(long var0, boolean var2) {
      try {
         Thread.sleep(var0);
      } catch (InterruptedException var3) {
         if (var2) {
            Thread.currentThread().interrupt();
            Thread.interrupted();
         }

      }
   }

   public static final int sizeof(Class var0) {
      if (var0 != Byte.TYPE && var0 != Boolean.TYPE) {
         if (var0 != Short.TYPE && var0 != Character.TYPE) {
            if (var0 != Integer.TYPE && var0 != Float.TYPE) {
               return var0 != Long.TYPE && var0 != Double.TYPE ? -1 : 8;
            } else {
               return 4;
            }
         } else {
            return 2;
         }
      } else {
         return 1;
      }
   }

   public static JDialog createDialogFromComponentParent(Component var0, boolean var1, boolean var2) {
      Object var3 = null;

      while(((Component)var0).getParent() != null) {
         if ((var0 = ((Component)var0).getParent()) instanceof Frame) {
            var3 = var0;
            if (!var2) {
               break;
            }
         }

         if (var0 instanceof Dialog) {
            var3 = var0;
            if (!var2) {
               break;
            }
         }
      }

      if (var3 instanceof Frame) {
         return new JDialog((Frame)var3, var1);
      } else if (var3 instanceof Dialog) {
         return new JDialog((Dialog)var3, var1);
      } else {
         throw new IllegalStateException("Couldn't find root surface");
      }
   }

   public static Field findDeclaredField(Class var0, String var1) {
      do {
         Field[] var2;
         int var3 = (var2 = var0.getDeclaredFields()).length;

         for(int var4 = 0; var4 < var3; ++var4) {
            Field var5;
            if ((var5 = var2[var4]).getName().equals(var1)) {
               return var5;
            }
         }
      } while((var0 = var0.getSuperclass()) != null);

      return null;
   }

   public static Field[] findDeclaredFields(Class var0, String var1) {
      ArrayList var2 = new ArrayList();

      do {
         Field[] var3;
         int var4 = (var3 = var0.getDeclaredFields()).length;

         for(int var5 = 0; var5 < var4; ++var5) {
            Field var6;
            if ((var6 = var3[var5]).getName().equals(var1)) {
               var2.add(var6);
            }
         }
      } while((var0 = var0.getSuperclass()) != null);

      return (Field[])var2.toArray(new Field[var2.size()]);
   }

   public static Method findDeclaredMethod(Class var0, String var1) {
      do {
         Method[] var2;
         int var3 = (var2 = var0.getDeclaredMethods()).length;

         for(int var4 = 0; var4 < var3; ++var4) {
            Method var5;
            if ((var5 = var2[var4]).getName().equals(var1)) {
               return var5;
            }
         }
      } while((var0 = var0.getSuperclass()) != null);

      return null;
   }

   public static Method[] findDeclaredMethods(Class var0, String var1) {
      ArrayList var3 = new ArrayList();

      int var6;
      try {
         do {
            Method[] var4;
            int var12 = (var4 = var0.getDeclaredMethods()).length;

            label45:
            for(var6 = 0; var6 < var12; ++var6) {
               Method var13;
               if ((var13 = var4[var6]).getName().equals(var1)) {
                  Iterator var14 = var3.iterator();

                  while(var14.hasNext()) {
                     Method var9 = (Method)var14.next();
                     if (var13.getReturnType().equals(var9.getReturnType())) {
                        Class[] var10 = var13.getParameterTypes();
                        Class[] var15 = var9.getParameterTypes();
                        if (Arrays.equals(var10, var15)) {
                           continue label45;
                        }
                     }
                  }

                  var3.add(var13);
               }
            }
         } while((var0 = var0.getSuperclass()) != null);
      } catch (AccessControlException var11) {
         var3.clear();
         Method[] var5;
         var6 = (var5 = var0.getMethods()).length;

         for(int var7 = 0; var7 < var6; ++var7) {
            Method var8;
            if ((var8 = var5[var7]).getName().equals(var1)) {
               var3.add(var8);
            }
         }
      }

      return (Method[])var3.toArray(new Method[var3.size()]);
   }

   public static Field[] forceAccess(Field... var0) {
      try {
         Field[] var1 = var0;
         int var2 = var0.length;

         for(int var3 = 0; var3 < var2; ++var3) {
            Field var4;
            if (!(var4 = var1[var3]).isAccessible()) {
               var4.setAccessible(true);
            }
         }
      } catch (AccessControlException var5) {
      }

      return var0;
   }

   public static Method[] forceAccess(Method... var0) {
      try {
         Method[] var1 = var0;
         int var2 = var0.length;

         for(int var3 = 0; var3 < var2; ++var3) {
            Method var4;
            if (!(var4 = var1[var3]).isAccessible()) {
               var4.setAccessible(true);
            }
         }

         return var0;
      } catch (AccessControlException var5) {
         return var0;
      }
   }

   public static List getBootClassPath() {
      String var0 = System.getProperty("sun.boot.class.path");
      String var1 = System.getProperty("path.separator");
      ArrayList var2 = new ArrayList();
      String[] var6;
      int var7 = (var6 = Text.split(var0, var1)).length;

      for(int var3 = 0; var3 < var7; ++var3) {
         String var4 = var6[var3];

         try {
            var2.add((new FileExt(var4)).toURI().toURL());
         } catch (IOException var5) {
            var5.printStackTrace();
         }
      }

      return var2;
   }

   public static List getClassPath() {
      String var0 = System.getProperty("java.class.path");
      String var1 = System.getProperty("path.separator");
      ArrayList var2 = new ArrayList();
      String[] var6;
      int var7 = (var6 = Text.split(var0, var1)).length;

      for(int var3 = 0; var3 < var7; ++var3) {
         String var4 = var6[var3];

         try {
            var2.add((new FileExt(var4)).toURI().toURL());
         } catch (IOException var5) {
            var5.printStackTrace();
         }
      }

      return var2;
   }

   public static List getClassPath0() {
      ClassLoader var0;
      try {
         if ((var0 = HighLevel.class.getClassLoader()) instanceof URLClassLoader) {
            URLClassLoader var6 = (URLClassLoader)var0;

            List var1;
            try {
               var1 = Arrays.asList(var6.getURLs());
            } finally {
               var6.close();
            }

            return var1;
         }
      } catch (Exception var5) {
         var0 = null;
         var5.printStackTrace();
      }

      return null;
   }

   public static List getJarsOnClassPath() {
      ArrayList var0 = new ArrayList();
      URLClassLoader var1 = null;

      try {
         ClassLoader var2;
         try {
            if ((var2 = HighLevel.class.getClassLoader()) instanceof URLClassLoader) {
               URL[] var14;
               int var3 = (var14 = (var1 = (URLClassLoader)var2).getURLs()).length;

               for(int var4 = 0; var4 < var3; ++var4) {
                  String var5;
                  if ((var5 = var14[var4].toString()).startsWith("file:")) {
                     var5 = Text.after(var5, "file:");
                     FileExt var15;
                     if ((var15 = new FileExt(var5)).exists() && !var15.isDirectory()) {
                        var0.add(var15);
                     }
                  }
               }
            }
         } catch (Exception var12) {
            var2 = null;
            var12.printStackTrace();
         }
      } finally {
         if (var1 != null) {
            try {
               var1.close();
            } catch (IOException var11) {
               var11.printStackTrace();
            }
         }

      }

      return var0;
   }
}
