package org.schema.schine.graphicsengine.movie.subtitles;

import java.io.File;
import java.util.Locale;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.resource.FileExt;

public class SubtitleFactory {
   public static AbstractSubtitleManager getManagerFromVideoFile(File var0) {
      File var1;
      if ((var1 = var0.getParentFile()) != null) {
         String var8 = var0.getName().substring(0, var0.getName().lastIndexOf("."));
         String var2 = EngineSettings.LANGUAGE_PACK.getCurrentState().toString();
         AbstractSubtitleManager var3 = null;
         if (!var2.toLowerCase(Locale.ENGLISH).equals("english")) {
            File[] var4;
            int var5 = (var4 = (new File("./data/language/")).listFiles()).length;

            for(int var6 = 0; var6 < var5; ++var6) {
               File var7;
               if ((var7 = var4[var6]).isDirectory() && var7.getName().toLowerCase(Locale.ENGLISH).equals(var2.toLowerCase(Locale.ENGLISH))) {
                  var3 = getSBV(var7, var8);
                  System.err.println("[CLIENT][MOVIE] Trying to use translated subtitle file from dir: " + var7.getAbsolutePath());
                  break;
               }
            }
         }

         if (var3 == null) {
            System.err.println("[CLIENT][MOVIE] Using default subtitles: " + var1.getAbsolutePath());
            var3 = getSBV(var1, var8);
         }

         if (var3 != null) {
            return var3;
         }
      }

      return null;
   }

   private static AbstractSubtitleManager getSBV(File var0, String var1) {
      FileExt var3;
      if ((var3 = new FileExt(var0, var1 + ".sbv")).exists()) {
         try {
            return new SvbSubtitleManager(var3);
         } catch (SubtitleParseException var2) {
            var2.printStackTrace();
         }
      }

      return null;
   }
}
