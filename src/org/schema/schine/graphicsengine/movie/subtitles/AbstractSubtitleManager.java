package org.schema.schine.graphicsengine.movie.subtitles;

import it.unimi.dsi.fastutil.longs.Long2ObjectRBTreeMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.File;
import java.util.Iterator;
import java.util.List;

public abstract class AbstractSubtitleManager {
   public long time = -1L;
   private final Long2ObjectRBTreeMap map;
   private final List activeSubtitles = new ObjectArrayList();

   public AbstractSubtitleManager(File var1) throws SubtitleParseException {
      this.map = this.parseSubtitles(var1);
   }

   public void updateSubtitles(long var1) {
      this.getSubtitlesStartedBetween(this.time, var1);
      this.time = var1;

      for(int var3 = 0; var3 < this.activeSubtitles.size(); ++var3) {
         Subtitle var4 = (Subtitle)this.activeSubtitles.get(var3);
         if (var1 > var4.endTime || var1 < var4.startTime) {
            this.activeSubtitles.remove(var3);
            --var3;
         }
      }

   }

   private void getSubtitlesStartedBetween(long var1, long var3) {
      Iterator var5 = this.map.keySet().iterator();

      while(var5.hasNext()) {
         long var6;
         if ((var6 = (Long)var5.next()) > var1 && var6 <= var3) {
            this.activeSubtitles.add(this.map.get(var6));
         }
      }

   }

   protected abstract Long2ObjectRBTreeMap parseSubtitles(File var1) throws SubtitleParseException;

   public List getActiveSubtitles() {
      return this.activeSubtitles;
   }
}
