package org.schema.schine.graphicsengine.movie.subtitles;

import it.unimi.dsi.fastutil.longs.Long2ObjectRBTreeMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector4f;
import org.schema.schine.input.KeyboardMappings;

public class SvbSubtitleManager extends AbstractSubtitleManager {
   public SvbSubtitleManager(File var1) throws SubtitleParseException {
      super(var1);
   }

   private Subtitle parseCaption(List var1) throws SubtitleParseException {
      if (var1.isEmpty()) {
         throw new SubtitleParseException("No input");
      } else {
         String var2 = (String)var1.remove(0);
         SvbSubtitleManager.TimeDuration var5 = new SvbSubtitleManager.TimeDuration(var2);
         StringBuffer var3 = new StringBuffer();

         while(!var1.isEmpty() && ((String)var1.get(0)).trim().length() > 0) {
            String var4 = KeyboardMappings.formatText(((String)var1.remove(0)).trim());
            var3.append(var4 + "\n");
         }

         return new Subtitle(var3.toString(), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), var5.startTime, var5.endTime);
      }
   }

   protected Long2ObjectRBTreeMap parseSubtitles(File var1) throws SubtitleParseException {
      BufferedReader var2 = null;
      StringBuffer var3 = new StringBuffer();
      boolean var13 = false;

      Long2ObjectRBTreeMap var22;
      try {
         var13 = true;
         var2 = new BufferedReader(new InputStreamReader(new FileInputStream(var1), "UTF-8"));
         ObjectArrayList var4 = new ObjectArrayList();

         while(true) {
            String var5;
            if ((var5 = var2.readLine()) == null) {
               System.err.println("PARSED SUBTITLES: " + var1.getAbsolutePath() + ":\n" + var3);
               List var20 = this.parseSubtitles((List)var4);
               Long2ObjectRBTreeMap var23 = new Long2ObjectRBTreeMap();
               Iterator var21 = var20.iterator();

               while(var21.hasNext()) {
                  Subtitle var6;
                  for(var6 = (Subtitle)var21.next(); var23.containsKey(var6.startTime); var6 = new Subtitle(var6.text, var6.color, var6.startTime + 1L, var6.endTime + 1L)) {
                  }

                  var23.put(var6.startTime, var6);
               }

               var22 = var23;
               var13 = false;
               break;
            }

            var3.append(var5 + "\n");
            var4.add(var5);
         }
      } catch (FileNotFoundException var16) {
         System.err.println("PARSED IN FILE: " + var3);
         throw new SubtitleParseException(var1.getAbsolutePath(), var16);
      } catch (IOException var17) {
         System.err.println("PARSED IN FILE: " + var3);
         throw new SubtitleParseException(var1.getAbsolutePath(), var17);
      } catch (Exception var18) {
         System.err.println("PARSED IN FILE: " + var3);
         throw new SubtitleParseException(var1.getAbsolutePath(), var18);
      } finally {
         if (var13) {
            if (var2 != null) {
               try {
                  var2.close();
               } catch (IOException var14) {
                  var14.printStackTrace();
               }
            }

         }
      }

      try {
         var2.close();
      } catch (IOException var15) {
         var15.printStackTrace();
      }

      return var22;
   }

   private List parseSubtitles(List var1) throws SubtitleParseException {
      ObjectArrayList var2 = new ObjectArrayList();

      while(!var1.isEmpty()) {
         if (((String)var1.get(0)).trim().isEmpty()) {
            var1.remove(0);
         }

         if (!var1.isEmpty()) {
            var2.add(this.parseCaption(var1));
         }
      }

      if (var2.isEmpty()) {
         throw new SubtitleParseException("Nothing parsed");
      } else {
         return var2;
      }
   }

   public static long parseTimeCode(String var0) throws SubtitleParseException {
      try {
         String[] var10;
         long var1 = Long.parseLong((var10 = var0.split(":"))[0]);
         long var3 = Long.parseLong(var10[1]);
         long var5 = Long.parseLong((var10 = var10[2].split("\\."))[0]);
         long var7 = Long.parseLong(var10[1]);
         return (var1 * 60L * 60L + var3 * 60L + var5) * 1000L + var7;
      } catch (Exception var9) {
         throw new SubtitleParseException(var9);
      }
   }

   class TimeDuration {
      long startTime;
      long endTime;

      public TimeDuration(String var2) throws SubtitleParseException {
         String[] var3;
         if ((var3 = var2.split(",")).length != 2) {
            throw new SubtitleParseException("Time code wrong format: " + var2);
         } else {
            this.startTime = SvbSubtitleManager.parseTimeCode(var3[0]);
            this.endTime = SvbSubtitleManager.parseTimeCode(var3[1]);
         }
      }
   }
}
