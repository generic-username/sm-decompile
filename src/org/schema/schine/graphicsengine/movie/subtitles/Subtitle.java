package org.schema.schine.graphicsengine.movie.subtitles;

import javax.vecmath.Vector4f;

public class Subtitle {
   public final String text;
   public final Vector4f color;
   public final long startTime;
   public final long endTime;

   public Subtitle(String var1, Vector4f var2, long var3, long var5) {
      this.text = var1;
      this.color = var2;
      this.startTime = var3;
      this.endTime = var5;
   }

   public int hashCode() {
      return 31 + (int)(this.startTime ^ this.startTime >>> 32);
   }

   public boolean equals(Object var1) {
      if (this == var1) {
         return true;
      } else if (var1 == null) {
         return false;
      } else if (!(var1 instanceof Subtitle)) {
         return false;
      } else {
         Subtitle var2 = (Subtitle)var1;
         return this.startTime == var2.startTime;
      }
   }
}
