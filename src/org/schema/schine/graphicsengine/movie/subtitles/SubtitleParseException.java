package org.schema.schine.graphicsengine.movie.subtitles;

public class SubtitleParseException extends Exception {
   private static final long serialVersionUID = 1L;

   public SubtitleParseException() {
   }

   public SubtitleParseException(String var1, Throwable var2, boolean var3, boolean var4) {
      super(var1, var2, var3, var4);
   }

   public SubtitleParseException(String var1, Throwable var2) {
      super(var1, var2);
   }

   public SubtitleParseException(String var1) {
      super(var1);
   }

   public SubtitleParseException(Throwable var1) {
      super(var1);
   }
}
