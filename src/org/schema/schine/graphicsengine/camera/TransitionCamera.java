package org.schema.schine.graphicsengine.camera;

import javax.vecmath.Matrix4f;
import org.schema.schine.graphicsengine.core.Timer;

public class TransitionCamera extends Camera {
   private final Camera oldCamera;
   private final float transitionTime;
   private final Camera newCamera;
   Matrix4f oldM = new Matrix4f();
   Matrix4f newM = new Matrix4f();
   Matrix4f subM = new Matrix4f();
   float t = 0.0F;

   public TransitionCamera(Camera var1, Camera var2, float var3) {
      super(var1.state, var1.getViewable());
      this.oldCamera = var1;
      this.transitionTime = var3;
      this.newCamera = var2;
   }

   public boolean isActive() {
      return this.t < this.transitionTime;
   }

   public void update(Timer var1, boolean var2) {
      this.t += var1.getDelta();
      float var3 = this.t / this.transitionTime;
      this.oldCamera.getWorldTransform().getMatrix(this.oldM);
      this.newCamera.getWorldTransform().getMatrix(this.newM);
      this.subM.sub(this.newM, this.oldM);
      this.subM.mul(var3);
      this.oldM.add(this.subM);
      this.getWorldTransform().set(this.oldM);
   }
}
