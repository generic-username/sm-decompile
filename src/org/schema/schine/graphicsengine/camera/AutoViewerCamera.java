package org.schema.schine.graphicsengine.camera;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.camera.viewer.FixedViewer;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;

public class AutoViewerCamera extends Camera {
   Vector3f dir = new Vector3f();
   private Vector3f flyTo;

   public AutoViewerCamera(StateInterface var1, FixedViewer var2, Vector3f var3, Vector3f var4) {
      super(var1, var2);
      this.flyTo = var3;
      this.dir = new Vector3f(var4);

      assert var4.lengthSquared() != 0.0F && !Float.isNaN(var4.x);

      this.getWorldTransform().setIdentity();
   }

   public void update(Timer var1, boolean var2) {
      if (var1.getDelta() > 0.0F) {
         Vector3f var3;
         Vector3f var4;
         Vector3f var5;
         if (this.flyTo != null) {
            var5 = new Vector3f();
            (var3 = new Vector3f()).sub(this.flyTo, this.getWorldTransform().origin);
            if (var3.lengthSquared() == 0.0F) {
               var3.set(0.0F, 0.0F, 1.0F);
            }

            (var4 = new Vector3f(var3)).normalize();
            if (this.dir.lengthSquared() == 0.0F) {
               this.dir.set(0.0F, 0.0F, 1.0F);
            }

            var4.sub(this.dir);
            if (var4.lengthSquared() == 0.0F) {
               var4.set(0.0F, 0.0F, 1.0F);
            }

            if (var4.length() > 0.1F) {
               GlUtil.project(new Vector3f(this.dir), new Vector3f(var3), var5);
               var5.normalize();
               var5.scale((float)((double)var1.getDelta() * 0.35D));
               this.dir.add(var5);
               this.dir.normalize();
            }
         }

         (var5 = new Vector3f(this.dir)).scale(var1.getDelta() * 55.0F);
         ((FixedViewer)this.getViewable()).getEntity().getWorldTransform().origin.add(var5);
         var3 = GlUtil.getUpVector(new Vector3f(), (Transform)this.getWorldTransform());

         assert var3.lengthSquared() > 0.0F;

         var3.x += 0.05F;
         var3.y += 0.05F;
         var3.normalize();
         (var4 = new Vector3f()).cross(this.dir, var3);
         var4.normalize();
         var3.cross(var4, this.dir);
         var3.normalize();
         GlUtil.setForwardVector(this.dir, (Transform)this.getWorldTransform());
         GlUtil.setUpVector(var3, (Transform)this.getWorldTransform());
         GlUtil.setRightVector(var4, (Transform)this.getWorldTransform());

         assert var4.lengthSquared() > 0.0F : this.dir + ", " + var3;

         this.updateViewer(var1);
      }
   }
}
