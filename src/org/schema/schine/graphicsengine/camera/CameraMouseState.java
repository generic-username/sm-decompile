package org.schema.schine.graphicsengine.camera;

import java.util.Iterator;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.Mouse;
import org.schema.schine.network.client.ClientStateInterface;

public class CameraMouseState {
   private static boolean grabbed = false;
   public static boolean ungrabForced = false;
   public boolean button2;
   public int dWheel;
   public int dx;
   public int dy;
   public int x;
   public int y;
   public boolean button0;
   public boolean button1;

   public static boolean isInMouseControl(ClientStateInterface var0) {
      if (var0.isDebugKeyDown()) {
         return false;
      } else if (ungrabForced && EngineSettings.S_MOUSE_LOCK.isOn()) {
         return false;
      } else {
         return isGrabbed() && EngineSettings.S_MOUSE_LOCK.isOn();
      }
   }

   public void clear() {
      this.button2 = false;
      this.dWheel = 0;
      this.dx = 0;
      this.dy = 0;
      this.button0 = false;
      this.button1 = false;
   }

   public void updateMouseState(ClientStateInterface var1) {
      this.clear();
      this.button0 = Mouse.isButtonDown(0);
      this.button1 = Mouse.isButtonDown(1);
      this.button2 = Mouse.isButtonDown(2);
      boolean var2 = isInMouseControl(var1);
      if (Mouse.isGrabbed() != var2) {
         Mouse.setGrabbed(var2);
      }

      Mouse.setClipMouseCoordinatesToWindow(!Mouse.isGrabbed() && !Keyboard.isKeyDown(56));
      this.dx = 0;
      this.dy = 0;
      this.dWheel = 0;

      MouseEvent var4;
      for(Iterator var3 = var1.getController().getInputController().getMouseEvents().iterator(); var3.hasNext(); this.y = var4.y) {
         var4 = (MouseEvent)var3.next();
         this.dWheel += var4.dWheel;
         this.dx += var4.dx;
         this.dy += var4.dy;
         this.x = var4.x;
      }

   }

   public static boolean isGrabbed() {
      return grabbed;
   }

   public static void setGrabbed(boolean var0) {
      grabbed = var0;
   }
}
