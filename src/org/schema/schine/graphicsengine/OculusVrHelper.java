package org.schema.schine.graphicsengine;

import org.terasology.TeraOVR;

public final class OculusVrHelper {
   public static final int OCCULUS_NONE = 0;
   public static final int OCCULUS_RIGHT = 1;
   public static final int OCCULUS_LEFT = 2;
   private static final float[] DISTORTION_PARAMS = new float[]{1.0F, 0.22F, 0.24F, 0.0F};
   private static float verticalRes = 800.0F;
   private static float horizontalRes = 1280.0F;
   private static float verticalScreenSize = 0.0935F;
   private static float horizontalScreenSize = 0.14976F;
   private static float eyeToScreenDistance = 0.041F;
   private static float lensSeparationDistance = 0.0635F;
   private static float interpupillaryDistance = 0.064F;
   private static float halfScreenDistance;
   private static float viewCenter;
   private static float aspectRatio;
   private static float eyeProjectionShift;
   private static float projectionCenterOffset;
   private static float lensOffset;
   private static float lensShift;
   private static float lensViewportShift;
   private static float scaleFactor;
   private static float percievedHalfRTDistance;
   private static float yFov;
   private static boolean nativeLibraryLoaded;
   private static float lastPitch;
   private static float lastYaw;
   private static float lastRoll;

   private OculusVrHelper() {
   }

   public static void loadNatives() {
      if (System.getProperty("os.arch").contains("64")) {
         System.loadLibrary("tera-ovr64");
         System.err.println("loaded tera-ovr64");
         nativeLibraryLoaded = true;
      } else {
         System.loadLibrary("tera-ovr");
         System.err.println("loaded tera-ovr");
         nativeLibraryLoaded = true;
      }
   }

   public static void updateFromDevice() {
      if (nativeLibraryLoaded) {
         verticalRes = (float)TeraOVR.getVResolution();
         horizontalRes = (float)TeraOVR.getHResolution();
         verticalScreenSize = TeraOVR.getVScreenSize();
         horizontalScreenSize = TeraOVR.getHScreenSize();
         eyeToScreenDistance = TeraOVR.getEyeToScreenDistance();
         lensSeparationDistance = TeraOVR.getLensSeparationDistance();
         interpupillaryDistance = TeraOVR.getInterpupillaryDistance();
         DISTORTION_PARAMS[0] = TeraOVR.getDistortitionK0();
         DISTORTION_PARAMS[1] = TeraOVR.getDistortitionK1();
         DISTORTION_PARAMS[2] = TeraOVR.getDistortitionK2();
         DISTORTION_PARAMS[3] = TeraOVR.getDistortitionK3();
         lastPitch = TeraOVR.getPitch();
         lastRoll = TeraOVR.getRoll();
         lastYaw = TeraOVR.getYaw();
      }

      updateHelperVariables();
   }

   private static void updateHelperVariables() {
      halfScreenDistance = horizontalScreenSize * 0.5F;
      aspectRatio = horizontalRes * 0.5F / verticalRes;
      eyeProjectionShift = (viewCenter = horizontalScreenSize * 0.25F) - lensSeparationDistance * 0.5F;
      projectionCenterOffset = 4.0F * eyeProjectionShift / horizontalScreenSize;
      lensOffset = lensSeparationDistance * 0.5F;
      lensShift = horizontalScreenSize * 0.25F - lensOffset;
      lensViewportShift = 4.0F * lensShift / horizontalScreenSize;
      scaleFactor = 1.0F;
      percievedHalfRTDistance = verticalScreenSize / 2.0F * scaleFactor;
      yFov = 2.0F * (float)Math.atan((double)(percievedHalfRTDistance / eyeToScreenDistance));
   }

   public static float getVerticalRes() {
      return verticalRes;
   }

   public static float getHorizontalRes() {
      return horizontalRes;
   }

   public static float getVerticalScreenSize() {
      return verticalScreenSize;
   }

   public static float getHorizontalScreenSize() {
      return horizontalScreenSize;
   }

   public static float getEyeToScreenDistance() {
      return eyeToScreenDistance;
   }

   public static float getLensSeparationDistance() {
      return lensSeparationDistance;
   }

   public static float getInterpupillaryDistance() {
      return interpupillaryDistance;
   }

   public static float[] getDistortionParams() {
      return DISTORTION_PARAMS;
   }

   public static float getHalfScreenDistance() {
      return halfScreenDistance;
   }

   public static float getViewCenter() {
      return viewCenter;
   }

   public static float getAspectRatio() {
      return aspectRatio;
   }

   public static float getEyeProjectionShift() {
      return eyeProjectionShift;
   }

   public static float getProjectionCenterOffset() {
      return projectionCenterOffset;
   }

   public static float getLensOffset() {
      return lensOffset;
   }

   public static float getLensShift() {
      return lensShift;
   }

   public static float getLensViewportShift() {
      return lensViewportShift;
   }

   public static float getScaleFactor() {
      return scaleFactor;
   }

   public static float getPercievedHalfRTDistance() {
      return percievedHalfRTDistance;
   }

   public static float getyFov() {
      return yFov;
   }

   public static boolean isNativeLibraryLoaded() {
      return nativeLibraryLoaded;
   }

   public static float getYawRelativeToLastCall() {
      if (nativeLibraryLoaded) {
         float var0;
         float var1 = (var0 = TeraOVR.getYaw()) - lastYaw;
         lastYaw = var0;
         return var1;
      } else {
         return 0.0F;
      }
   }

   public static float getPitchRelativeToLastCall() {
      if (nativeLibraryLoaded) {
         float var0;
         float var1 = (var0 = TeraOVR.getPitch()) - lastPitch;
         lastPitch = var0;
         return var1;
      } else {
         return 0.0F;
      }
   }

   public static float getRollRelativeToLastCall() {
      if (nativeLibraryLoaded) {
         float var0;
         float var1 = (var0 = TeraOVR.getRoll()) - lastRoll;
         lastRoll = var0;
         return var1;
      } else {
         return 0.0F;
      }
   }

   static {
      updateHelperVariables();
   }
}
