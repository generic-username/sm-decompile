package org.schema.schine.graphicsengine.forms;

import java.util.ArrayList;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;

public class TerrainMeshOptimizer {
   private static final float TERRAIN_OPTIMIZE_HEIGHT_THRESHOLD = 1.0F;
   private static final int TERRAIN_OPTIMIZE_MIN_QUAD_LENGTH = 8;
   private static final int TERRAIN_OPTIMIZE_MAX_AFFECTED = 1;
   private OldTerrain m;
   private int w;
   private int h;
   private Vector3f[][] subQuads;

   public TerrainMeshOptimizer(OldTerrain var1, int var2, int var3) {
      this.m = var1;
      this.w = var2;
      this.h = var3;
   }

   public static void optimize(OldTerrain var0, int var1, int var2, Vector3f[][] var3, float var4, float var5, float var6, float var7) {
      ArrayList var8 = new ArrayList();
      ArrayList var9 = new ArrayList();
      ArrayList var10 = new ArrayList();
      optimizeQuad(var0, 0.0F, 0.0F, (float)var1, (float)var2, var1, var2, 0, var0.vertices, var8, var9, var10, var3);
      var0.vertCount = var8.size();
      var0.faceCount = var10.size();
      var0.vertices = new Vector3f[var0.vertCount];
      var0.normals = new Vector3f[var0.vertCount];
      var0.texCoords = new Vector3f[var0.vertCount];
      var0.faces = new Mesh.Face[var0.faceCount];
      float var11 = (float)var1 / var6;
      float var12 = (float)var2 / var7;
      float var13 = var4 * var11;
      var6 = var5 * var12;
      System.err.println("factor: " + var11 + ", " + var12 + "   -start:   " + var13 + ", " + var6 + "   -off:   " + var4 + ", " + var5);

      int var14;
      for(var14 = 0; var14 < var0.vertCount; ++var14) {
         var0.vertices[var14] = (Vector3f)var8.get(var14);
         var0.normals[var14] = new Vector3f();
         Vector3f var15;
         Vector3f var10000 = var15 = new Vector3f((Vector3f)var9.get(var14));
         var10000.set(var10000.x * var11, var15.y * var12, 0.0F);
         var15.set(var15.x + var13, var15.y + var6, 0.0F);
         var0.texCoords[var14] = var15;
      }

      for(var14 = 0; var14 < var0.faceCount; ++var14) {
         var0.faces[var14] = (Mesh.Face)var10.get(var14);
      }

   }

   private static void optimizeQuad(OldTerrain var0, float var1, float var2, float var3, float var4, int var5, int var6, int var7, Vector3f[] var8, ArrayList var9, ArrayList var10, ArrayList var11, Vector3f[][] var12) {
      float var13 = 0.0F;
      int var14 = 0;
      int var15 = Math.max((int)FastMath.floor(var2) - 1, 0);
      int var16 = Math.max((int)FastMath.floor(var1) - 1, 0);

      int var17;
      for(var17 = var15; (float)var17 < Math.min((float)var6, (float)var15 + FastMath.ceil(var4) + 1.0F); ++var17) {
         for(int var18 = var16; (float)var18 < Math.min((float)var6, (float)var16 + FastMath.ceil(var3) + 1.0F); ++var18) {
            var13 += var12[var17][var18].y;
            ++var14;
         }
      }

      if (var14 == 0) {
         throw new ArithmeticException("zero at " + var1 + "," + var2 + "," + var3 + ", " + var4);
      } else {
         var13 /= (float)var14;
         var17 = 0;
         boolean var28 = false;

         for(var14 = var15; (float)var14 < Math.min((float)var6, (float)var15 + FastMath.ceil(var4) + 1.0F); ++var14) {
            for(int var19 = var16; (float)var19 < Math.min((float)var6, (float)var16 + FastMath.ceil(var3) + 1.0F); ++var19) {
               if (FastMath.abs(var12[var14][var19].y - var13) > 1.0F) {
                  ++var17;
               }

               if (var17 > 1) {
                  var28 = true;
                  break;
               }
            }

            if (var28) {
               break;
            }
         }

         if (var28 && FastMath.abs(var1 - var3) >= 8.0F && FastMath.abs(var4 - var2) >= 8.0F) {
            optimizeQuadTree(var0, var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12);
         } else {
            Mesh.Face var25 = new Mesh.Face();
            Mesh.Face var29 = new Mesh.Face();
            int var20 = var9.size();
            var25.m_normalIndex = new int[3];
            var25.m_texCoordsIndex = new int[3];
            var25.m_vertsIndex = new int[3];
            var29.m_normalIndex = new int[3];
            var29.m_texCoordsIndex = new int[3];
            var29.m_vertsIndex = new int[3];
            var25.m_vertsIndex[0] = var20;
            var25.m_vertsIndex[1] = var20 + 1;
            var25.m_vertsIndex[2] = var20 + 2;
            var25.m_normalIndex[0] = var20;
            var25.m_normalIndex[1] = var20 + 1;
            var25.m_normalIndex[2] = var20 + 2;
            var25.m_texCoordsIndex[0] = var20;
            var25.m_texCoordsIndex[1] = var20 + 1;
            var25.m_texCoordsIndex[2] = var20 + 2;
            var29.m_vertsIndex[0] = var20 + 2;
            var29.m_vertsIndex[1] = var20 + 1;
            var29.m_vertsIndex[2] = var20 + 3;
            var29.m_normalIndex[0] = var20 + 2;
            var29.m_normalIndex[1] = var20 + 1;
            var29.m_normalIndex[2] = var20 + 3;
            var29.m_texCoordsIndex[0] = var20 + 2;
            var29.m_texCoordsIndex[1] = var20 + 1;
            var29.m_texCoordsIndex[2] = var20 + 3;
            float var21 = 1.0F / ((float)var5 * 100.0F * (float)OldTerrain.scalefactor);
            float var22 = 1.0F / ((float)var6 * 100.0F * (float)OldTerrain.scalefactor);
            Vector3f var23 = new Vector3f(var12[(int)var2][(int)var1]);
            Vector3f var24 = new Vector3f(var12[(int)var2][Math.min(var5 - 1, (int)var3)]);
            Vector3f var26 = new Vector3f(var12[Math.min(var6 - 1, (int)var4)][(int)var1]);
            Vector3f var27 = new Vector3f(var12[Math.min(var6 - 1, (int)var4)][Math.min(var5 - 1, (int)var3)]);
            var10.add(new Vector3f(var23.x * var21, var23.z * var22, 0.0F));
            var10.add(new Vector3f(var24.x * var21, var24.z * var22, 0.0F));
            var10.add(new Vector3f(var26.x * var21, var26.z * var22, 0.0F));
            var10.add(new Vector3f(var27.x * var21, var27.z * var22, 0.0F));
            var9.add(new Vector3f(var12[(int)var2][(int)var1]));
            var9.add(new Vector3f(var12[(int)var2][Math.min(var5 - 1, (int)var3)]));
            var9.add(new Vector3f(var12[Math.min(var6 - 1, (int)var4)][(int)var1]));
            var9.add(new Vector3f(var12[Math.min(var6 - 1, (int)var4)][Math.min(var5 - 1, (int)var3)]));
            var11.add(var25);
            var11.add(var29);
         }
      }
   }

   private static void optimizeQuadTree(OldTerrain var0, float var1, float var2, float var3, float var4, int var5, int var6, int var7, Vector3f[] var8, ArrayList var9, ArrayList var10, ArrayList var11, Vector3f[][] var12) {
      var3 = FastMath.abs(var3 - var1);
      var4 = FastMath.abs(var4 - var2);
      optimizeQuad(var0, var1, var2, var1 + var3 / 2.0F, var2 + var4 / 2.0F, var5, var6, var7 + 1, var8, var9, var10, var11, var12);
      optimizeQuad(var0, var1 + var3 / 2.0F, var2, var1 + var3, var2 + var4 / 2.0F, var5, var6, var7 + 1, var8, var9, var10, var11, var12);
      optimizeQuad(var0, var1, var2 + var4 / 2.0F, var1 + var3 / 2.0F, var2 + var4, var5, var6, var7 + 1, var8, var9, var10, var11, var12);
      optimizeQuad(var0, var1 + var3 / 2.0F, var2 + var4 / 2.0F, var1 + var3, var2 + var4, var5, var6, var7 + 1, var8, var9, var10, var11, var12);
   }

   public void optimize(int var1, int var2, int var3, int var4) {
      this.subQuads = new Vector3f[this.w][this.h];
      int var5 = 0;

      for(int var6 = 0; var6 < this.h; ++var6) {
         for(int var7 = 0; var7 < this.w; ++var7) {
            this.subQuads[var6][var7] = this.m.getOrigVerts()[var5];
            ++var5;
         }
      }

      optimize(this.m, this.w, this.h, this.subQuads, (float)var1, (float)var2, (float)var3, (float)var4);
   }
}
