package org.schema.schine.graphicsengine.forms;

import org.schema.schine.graphicsengine.core.Drawable;

public abstract class MediaObject extends AbstractSceneNode implements Drawable {
   public static final int PLAYING = 1;
   public static final int STOPPED = 0;
   public static final int PAUSED = 2;
   protected int state = 0;
   protected boolean mediaVisible = true;
   protected Sprite sprite;

   public abstract void next();

   public abstract void previous();

   public abstract void startMedia();

   public abstract void stopMedia();
}
