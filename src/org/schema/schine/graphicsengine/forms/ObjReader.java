package org.schema.schine.graphicsengine.forms;

import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Iterator;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.schema.schine.graphicsengine.camera.Camera;
import org.schema.schine.graphicsengine.camera.viewer.PositionableViewer;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.texture.Texture;
import org.schema.schine.graphicsengine.texture.TextureLoader;
import org.schema.schine.network.StateInterface;

public class ObjReader {
   public String path;
   private String name;
   private FloatArrayList vertices = new FloatArrayList();
   private FloatArrayList texCoords = new FloatArrayList();
   private FloatArrayList normals = new FloatArrayList();
   private Object2ObjectOpenHashMap faces = new Object2ObjectOpenHashMap();
   private final Object2ObjectOpenHashMap mats = new Object2ObjectOpenHashMap();
   private int vertexBufferId;
   private int texCoordBufferId;
   private int normalBufferId;
   private Camera camera;

   public ObjReader(String var1, String var2) {
      this.name = var2;
      this.path = var1.endsWith("/") ? var1 : var1 + "/";
   }

   public void load() throws IOException, ResourceException {
      this.readMat();
      this.readObj();
      this.camera = new Camera((StateInterface)null, new PositionableViewer());
   }

   public void draw() {
      GlUtil.glPushMatrix();
      GL11.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glEnable(3553);
      GlUtil.glEnable(2896);
      GL11.glClear(16640);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      this.camera.lookAt(true);
      this.camera.updateFrustum();
      AbstractScene.mainLight.draw();
      GlUtil.glEnable(2884);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glEnableClientState(32884);
      GlUtil.glBindBuffer(34962, this.vertexBufferId);
      GL11.glVertexPointer(3, 5126, 0, 0L);
      GlUtil.glEnableClientState(32888);
      GlUtil.glBindBuffer(34962, this.texCoordBufferId);
      GL11.glTexCoordPointer(2, 5126, 0, 0L);
      GlUtil.glEnableClientState(32885);
      GlUtil.glBindBuffer(34962, this.normalBufferId);
      GL11.glNormalPointer(5126, 0, 0L);
      Iterator var1 = this.faces.keySet().iterator();

      while(var1.hasNext()) {
         String var2 = (String)var1.next();
         ((ObjReader.Mtl)this.mats.get(var2)).draw();
      }

      GlUtil.glBindBuffer(34962, 0);
      GlUtil.glDisableClientState(32884);
      GlUtil.glDisableClientState(32888);
      GlUtil.glDisableClientState(32885);
      GlUtil.glPopMatrix();
      GlUtil.glPopMatrix();
   }

   public void readObj() throws IOException {
      File var1 = new File(this.path + this.name);
      BufferedReader var2 = null;
      boolean var8 = false;

      try {
         var8 = true;
         var2 = new BufferedReader(new FileReader(var1));
         String var3 = null;

         label90:
         while(true) {
            String var10;
            do {
               if ((var10 = var2.readLine()) == null) {
                  var8 = false;
                  break label90;
               }

               this.parseCoord(var10, 3, "v ", this.vertices);
               this.parseCoord(var10, 2, "vt ", this.texCoords);
               this.parseCoord(var10, 3, "vn ", this.normals);
               if (var10.startsWith("usemtl ")) {
                  var3 = var10.substring(7);
                  this.faces.put(var3, new IntArrayList());
               }
            } while(!var10.startsWith("f "));

            assert var3 != null;

            IntArrayList var4 = (IntArrayList)this.faces.get(var3);
            String[] var11 = var10.substring(2).split("\\s+");

            for(int var5 = 0; var5 < 3; ++var5) {
               String[] var6 = var11[var5].split("/");
               var4.add(Integer.parseInt(var6[0]));
            }
         }
      } finally {
         if (var8) {
            if (var2 != null) {
               var2.close();
            }

         }
      }

      var2.close();
      this.createVBO();
   }

   private static void createBuffer(int var0, FloatArrayList var1) {
      FloatBuffer var2;
      (var2 = GlUtil.getDynamicByteBuffer(var1.size() << 2, 0).asFloatBuffer()).rewind();

      for(int var3 = 0; var3 < var1.size(); ++var3) {
         var2.put((float)var1.size());
      }

      var2.flip();
      GlUtil.glBindBuffer(34962, var0);
      GL15.glBufferData(34962, var2, 35044);
      GlUtil.glBindBuffer(34962, 0);
   }

   private void createVBO() {
      this.vertexBufferId = GL15.glGenBuffers();
      createBuffer(this.vertexBufferId, this.vertices);
      this.texCoordBufferId = GL15.glGenBuffers();
      createBuffer(this.texCoordBufferId, this.texCoords);
      this.normalBufferId = GL15.glGenBuffers();
      createBuffer(this.normalBufferId, this.normals);
      Iterator var1 = this.mats.values().iterator();

      while(true) {
         ObjReader.Mtl var2;
         IntArrayList var3;
         do {
            if (!var1.hasNext()) {
               return;
            }

            var2 = (ObjReader.Mtl)var1.next();
         } while((var3 = (IntArrayList)this.faces.get(var2.name)) == null);

         var2.indexBufferId = GL15.glGenBuffers();
         var2.faceCount = var3.size() / 3;
         IntBuffer var4;
         (var4 = GlUtil.getDynamicByteBuffer(var3.size() << 2, 0).asIntBuffer()).rewind();

         for(int var5 = 0; var5 < var3.size(); ++var5) {
            var4.put(var3.size());
         }

         var4.flip();
         GlUtil.glBindBuffer(34963, var2.indexBufferId);
         GL15.glBufferData(34963, var4, 35040);
         GlUtil.glBindBuffer(34963, 0);
      }
   }

   private void parseCoord(String var1, int var2, String var3, FloatArrayList var4) {
      if (var1.startsWith(var3)) {
         String[] var5 = var1.substring(var3.length()).split("\\s+");

         for(int var6 = 0; var6 < var2; ++var6) {
            var4.add(Float.parseFloat(var5[var6]));
         }
      }

   }

   public void readMat() throws IOException, ResourceException {
      File var1 = new File(this.path + this.name);
      BufferedReader var2 = null;
      boolean var5 = false;

      try {
         var5 = true;
         var2 = new BufferedReader(new FileReader(var1));
         ObjReader.Mtl var3 = null;

         String var7;
         while((var7 = var2.readLine()) != null) {
            if (var7.startsWith("newmtl")) {
               var3 = new ObjReader.Mtl(var7.substring(7));
               this.mats.put(var3.name, var3);
            }

            if (var7.startsWith("map_Kd") && var3 != null) {
               var3.fileName = var7.substring(7);
            }
         }

         Iterator var8 = this.mats.values().iterator();

         while(var8.hasNext()) {
            ((ObjReader.Mtl)var8.next()).load();
         }

         var5 = false;
      } finally {
         if (var5) {
            if (var2 != null) {
               var2.close();
            }

         }
      }

      var2.close();
   }

   public void cleanUp() {
      GL15.glDeleteBuffers(this.vertexBufferId);
      GL15.glDeleteBuffers(this.texCoordBufferId);
      GL15.glDeleteBuffers(this.normalBufferId);
      Iterator var1 = this.mats.values().iterator();

      while(var1.hasNext()) {
         ((ObjReader.Mtl)var1.next()).cleanUp();
      }

   }

   class Mtl {
      private int indexBufferId;
      int faceCount;
      String fileName;
      Texture texture;
      private String name;

      public Mtl(String var2) {
         this.name = var2;
      }

      public void draw() {
         this.attach();
         GlUtil.glBindBuffer(34963, this.indexBufferId);
         GL11.glDrawElements(4, this.faceCount * 3, 5125, 0L);
         GlUtil.glBindBuffer(34963, 0);
         this.detach();
      }

      public void load() throws IOException, ResourceException {
         this.texture = TextureLoader.getTexture2D(ObjReader.this.path + this.fileName, 3553, 6408, 9729, 9729, true, false);
      }

      public void attach() {
         GlUtil.glEnable(3553);
         GlUtil.glBindTexture(3553, this.texture.getTextureId());
      }

      public void detach() {
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glDisable(3553);
      }

      public void cleanUp() {
         GL15.glDeleteBuffers(this.indexBufferId);
         this.texture.cleanUp();
      }
   }
}
