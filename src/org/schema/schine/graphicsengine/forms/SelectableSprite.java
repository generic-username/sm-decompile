package org.schema.schine.graphicsengine.forms;

public interface SelectableSprite {
   float getSelectionDepth();

   boolean isSelectable();

   void onSelect(float var1);

   void onUnSelect();
}
