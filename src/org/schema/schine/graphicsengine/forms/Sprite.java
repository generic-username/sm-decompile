package org.schema.schine.graphicsengine.forms;

import java.nio.BufferOverflowException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3D;
import org.schema.schine.graphicsengine.camera.Camera;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.texture.Material;
import org.schema.schine.graphicsengine.texture.Texture;
import org.schema.schine.input.Mouse;

public class Sprite extends SceneNode {
   public static final int BLEND_SEPERATE_NORMAL = 1;
   public static final int BLEND_ADDITIVE = 2;
   public static int lastTexId = -1;
   static int sSizeVerts = 12;
   static int sSizeTex = 8;
   static int sSizeNorm = 12;
   private static FloatBuffer vertices;
   private static FloatBuffer texCoords;
   private static FloatBuffer tempModelviewBuffer = BufferUtils.createFloatBuffer(16);
   private static IntBuffer viewportTemp = BufferUtils.createIntBuffer(16);
   private static FloatBuffer projectionTemp = BufferUtils.createFloatBuffer(16);
   private static FloatBuffer modelviewTemp = BufferUtils.createFloatBuffer(16);
   private static FloatBuffer coord = BufferUtils.createFloatBuffer(3);
   private static FloatBuffer depth = BufferUtils.createFloatBuffer(1);
   public int blendFunc;
   public boolean useShader;
   protected int animationNumber;
   private int multiSpriteMax;
   private int selectedMultiSprite;
   private boolean positionCenter;
   private boolean depthTest;
   private boolean blend;
   private Vector4f tint;
   private int width;
   private int height;
   private boolean flip;
   private boolean billboard;
   private int VBOverts;
   private IntBuffer VBOTexCoords;
   private boolean orthogonal;
   private boolean VBOActive;
   private boolean firstDraw;
   private int multiSpriteMaxX;
   private int multiSpriteMaxY;
   private float selectionAreaLength;
   private static Vector3f rightTmp;
   private static Vector3f upTmp;
   private static org.lwjgl.util.vector.Vector4f outTmp;
   private static org.lwjgl.util.vector.Vector4f ss;
   private static org.lwjgl.util.vector.Vector4f outTmpCp;

   public Sprite(int var1, int var2) {
      this.useShader = true;
      this.multiSpriteMax = 1;
      this.selectedMultiSprite = 0;
      this.positionCenter = false;
      this.depthTest = true;
      this.blend = true;
      this.flip = false;
      this.billboard = false;
      this.orthogonal = false;
      this.VBOActive = true;
      this.firstDraw = true;
      this.material = new Material();
      this.width = var1;
      this.height = var2;
   }

   public Sprite(Texture var1) {
      this(var1.getWidth(), var1.getHeight());
      this.getMaterial().setTexture(var1);
   }

   public static void doDraw(Sprite var0) {
      if (var0.getTint() != null) {
         GlUtil.glColor4f(var0.getTint().x, var0.getTint().y, var0.getTint().z, var0.getTint().w);
      } else {
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      }

      if (var0.getScale() != null) {
         GL11.glScalef(var0.getScale().x, var0.getScale().y, var0.getScale().z);
      } else {
         GL11.glScalef(1.0F, 1.0F, 1.0F);
      }

      if (lastTexId != var0.getSelectedMultiSprite()) {
         GlUtil.glBindBuffer(34962, var0.VBOTexCoords.get(var0.getSelectedMultiSprite()));
         GL11.glTexCoordPointer(2, 5126, 0, 0L);
         lastTexId = var0.getSelectedMultiSprite();
      }

      GL11.glDrawArrays(7, 0, 4);
   }

   public static void draw(Sprite var0, int var1, Positionable... var2) {
      if (var0.firstDraw) {
         var0.onInit();
      }

      if (!var0.isInvisible()) {
         if (EngineSettings.G_CULLING_ACTIVE.isOn()) {
            var0.activateCulling();
         }

         GlUtil.glPushMatrix();
         GlUtil.glDisable(2896);
         if (var0.depthTest) {
            GlUtil.glEnable(2929);
         } else {
            GlUtil.glDisable(2929);
         }

         if (var0.isBlend()) {
            GlUtil.glEnable(3042);
            GlUtil.glBlendFunc(770, 771);
         } else {
            GlUtil.glDisable(3042);
         }

         var0.material.getTexture().attach(0);
         if (var0.VBOActive && EngineSettings.G_USE_SPRITE_VBO.isOn()) {
            GlUtil.glEnableClientState(32884);
            GlUtil.glEnableClientState(32888);
            GlUtil.glBindBuffer(34962, var0.VBOverts);
            GL11.glVertexPointer(3, 5126, 0, 0L);
            GlUtil.glActiveTexture(33984);
            if (var0.getTint() != null) {
               GlUtil.glColor4f(var0.getTint().x, var0.getTint().y, var0.getTint().z, var0.getTint().w);
            } else {
               GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            }

            GlUtil.glBindBuffer(34962, var0.VBOTexCoords.get(var0.getSelectedMultiSprite()));
            GL11.glTexCoordPointer(2, 5126, 0, 0L);
            if (var1 <= 0) {
               GlUtil.glPushMatrix();
               if (var0.billboard) {
                  GlUtil.translateModelview(var0.getPos());
                  tempModelviewBuffer.put(0, var0.getScale().x);
                  tempModelviewBuffer.put(5, -var0.getScale().y);
                  tempModelviewBuffer.put(10, var0.getScale().z);
                  tempModelviewBuffer.put(12, Controller.modelviewMatrix.m30);
                  tempModelviewBuffer.put(13, Controller.modelviewMatrix.m31);
                  tempModelviewBuffer.put(14, Controller.modelviewMatrix.m32);
                  tempModelviewBuffer.rewind();
                  GlUtil.glLoadMatrix(tempModelviewBuffer);
               }

               GL11.glDrawArrays(7, 0, 4);
               GlUtil.glPopMatrix();
            } else {
               GlUtil.glPushMatrix();

               for(int var3 = 0; var3 < var1; ++var3) {
                  GlUtil.glPushMatrix();
                  Positionable var4;
                  GL11.glTranslatef((var4 = var2[var3]).getPos().x, var4.getPos().y, var4.getPos().z);
                  if (var0.billboard) {
                     GL11.glLoadMatrix(var0.getBillboardSphericalBeginMatrix());
                  }

                  if (var0.isFlip()) {
                     GL11.glScalef(var0.getScale().x, -var0.getScale().y, var0.getScale().z);
                  } else {
                     GL11.glScalef(var0.getScale().x, var0.getScale().y, var0.getScale().z);
                  }

                  GL11.glDrawArrays(7, 0, 4);
                  GlUtil.glPopMatrix();
               }

               GlUtil.glPopMatrix();
            }

            GlUtil.glDisableClientState(32884);
            GlUtil.glDisableClientState(32888);
            GlUtil.glDisableClientState(32885);
         } else {
            float var5 = (float)var0.material.getTexture().getWidth() * var0.getScale().x;
            float var6 = (float)var0.material.getTexture().getHeight() * var0.getScale().y;
            GL11.glBegin(7);
            if (var0.positionCenter) {
               GL11.glTexCoord2f(0.0F, 0.0F);
               GL11.glVertex3f(-var5 / 2.0F, -var6 / 2.0F, 0.0F);
               GL11.glTexCoord2f(0.0F, 1.0F);
               GL11.glVertex3f(-var5 / 2.0F, var6 / 2.0F, 0.0F);
               GL11.glTexCoord2f(1.0F, 1.0F);
               GL11.glVertex3f(var5 / 2.0F, var6 / 2.0F, 0.0F);
               GL11.glTexCoord2f(1.0F, 0.0F);
               GL11.glVertex3f(var5 / 2.0F, -var6 / 2.0F, 0.0F);
            } else {
               GL11.glTexCoord2f(0.0F, 0.0F);
               GL11.glVertex3f(0.0F, 0.0F, 0.0F);
               GL11.glTexCoord2f(0.0F, 1.0F);
               GL11.glVertex3f(0.0F, var6, 0.0F);
               GL11.glTexCoord2f(1.0F, 1.0F);
               GL11.glVertex3f(var5, var6, 0.0F);
               GL11.glTexCoord2f(1.0F, 0.0F);
               GL11.glVertex3f(var5, 0.0F, 0.0F);
            }

            GL11.glEnd();
         }

         var0.material.getTexture().detach();
         GlUtil.glPopMatrix();
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GlUtil.glDisable(3042);
         GlUtil.glEnable(2929);
         GlUtil.glDisable(2903);
         GlUtil.glEnable(2896);
         if (EngineSettings.G_CULLING_ACTIVE.isOn()) {
            var0.activateCulling();
         }

      }
   }

   public static void drawRaw(Sprite var0) {
      GlUtil.glDisable(2896);
      if (var0.depthTest) {
         GlUtil.glEnable(2929);
      } else {
         GlUtil.glDisable(2929);
      }

      if (var0.isBlend()) {
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
      } else {
         GlUtil.glDisable(3042);
      }

      GlUtil.glEnableClientState(32884);
      GlUtil.glEnableClientState(32888);
      GlUtil.glBindBuffer(34962, var0.VBOverts);
      GL11.glVertexPointer(3, 5126, 0, 0L);
      GlUtil.glActiveTexture(33984);
      var0.material.getTexture().attach(0);
      if (var0.getTint() != null) {
         GlUtil.glColor4f(var0.getTint().x, var0.getTint().y, var0.getTint().z, var0.getTint().w);
      } else {
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      }

      GlUtil.glBindBuffer(34962, var0.VBOTexCoords.get(var0.getSelectedMultiSprite()));
      GL11.glTexCoordPointer(2, 5126, 0, 0L);
      GL11.glDrawArrays(7, 0, 4);
      GlUtil.glDisableClientState(32884);
      GlUtil.glDisableClientState(32888);
   }

   public static void draw(Sprite var0, int var1, PositionableSubSprite... var2) {
      if (var0.firstDraw) {
         var0.onInit();
      }

      if (!var0.isInvisible()) {
         if (EngineSettings.G_CULLING_ACTIVE.isOn()) {
            var0.activateCulling();
         }

         boolean var3 = var2.length > 0 && var2[0] instanceof PositionableSubColorSprite;
         GlUtil.glPushMatrix();
         GlUtil.glDisable(2896);
         if (var0.depthTest) {
            GlUtil.glEnable(2929);
         } else {
            GlUtil.glDisable(2929);
         }

         if (var0.isBlend()) {
            GlUtil.glEnable(3042);
            GlUtil.glBlendFunc(770, 771);
         } else {
            GlUtil.glDisable(3042);
         }

         var0.material.getTexture().attach(0);
         if (var0.VBOActive && EngineSettings.G_USE_SPRITE_VBO.isOn()) {
            GlUtil.glEnableClientState(32884);
            GlUtil.glEnableClientState(32888);
            GlUtil.glBindBuffer(34962, var0.VBOverts);
            GL11.glVertexPointer(3, 5126, 0, 0L);
            GlUtil.glActiveTexture(33984);
            if (var0.getTint() != null) {
               GlUtil.glColor4f(var0.getTint().x, var0.getTint().y, var0.getTint().z, var0.getTint().w);
            } else {
               GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            }

            GlUtil.glBindBuffer(34962, var0.VBOTexCoords.get(var0.getSelectedMultiSprite()));
            GL11.glTexCoordPointer(2, 5126, 0, 0L);
            int var4 = -1;
            if (var1 <= 0) {
               GlUtil.glPushMatrix();
               if (var0.billboard) {
                  GlUtil.translateModelview(var0.getPos());
                  tempModelviewBuffer.put(0, var0.getScale().x);
                  tempModelviewBuffer.put(5, -var0.getScale().y);
                  tempModelviewBuffer.put(10, var0.getScale().z);
                  tempModelviewBuffer.put(12, Controller.modelviewMatrix.m30);
                  tempModelviewBuffer.put(13, Controller.modelviewMatrix.m31);
                  tempModelviewBuffer.put(14, Controller.modelviewMatrix.m32);
                  tempModelviewBuffer.rewind();
                  GlUtil.glLoadMatrix(tempModelviewBuffer);
               }

               GL11.glDrawArrays(7, 0, 4);
               GlUtil.glPopMatrix();
            } else {
               GlUtil.glPushMatrix();

               for(int var5 = 0; var5 < var1; ++var5) {
                  GlUtil.glPushMatrix();
                  PositionableSubSprite var6 = var2[var5];
                  if (var3) {
                     PositionableSubColorSprite var9 = (PositionableSubColorSprite)var6;
                     if (GlUtil.loadedShader == ShaderLibrary.selectionShader) {
                        GlUtil.updateShaderFloat(ShaderLibrary.selectionShader, "texMult", 1.0F);
                        GlUtil.updateShaderVector4f(ShaderLibrary.selectionShader, "selectionColor", var9.getColor().x, var9.getColor().y, var9.getColor().z, var9.getColor().w);
                     }

                     GlUtil.glColor4f(var9.getColor().x, var9.getColor().y, var9.getColor().z, var9.getColor().w);
                  }

                  GL11.glTranslatef(var6.getPos().x, var6.getPos().y, var6.getPos().z);
                  var0.setSelectedMultiSprite(var6.getSubSprite(var0));
                  if (var0.getSelectedMultiSprite() != var4) {
                     GlUtil.glBindBuffer(34962, var0.VBOTexCoords.get(var0.getSelectedMultiSprite()));
                     GL11.glTexCoordPointer(2, 5126, 0, 0L);
                     var4 = var0.getSelectedMultiSprite();
                  }

                  if (var0.billboard) {
                     GL11.glLoadMatrix(var0.getBillboardSphericalBeginMatrix());
                  }

                  if (var0.isFlip()) {
                     GL11.glScalef(var0.getScale().x, -var0.getScale().y, var0.getScale().z);
                  } else {
                     GL11.glScalef(var0.getScale().x, var0.getScale().y, var0.getScale().z);
                  }

                  GL11.glDrawArrays(7, 0, 4);
                  GlUtil.glPopMatrix();
               }

               GlUtil.glPopMatrix();
            }

            GlUtil.glDisableClientState(32884);
            GlUtil.glDisableClientState(32888);
         } else {
            float var7 = (float)var0.material.getTexture().getWidth() * var0.getScale().x;
            float var8 = (float)var0.material.getTexture().getHeight() * var0.getScale().y;
            GL11.glBegin(7);
            if (var0.positionCenter) {
               GL11.glTexCoord2f(0.0F, 0.0F);
               GL11.glVertex3f(-var7 / 2.0F, -var8 / 2.0F, 0.0F);
               GL11.glTexCoord2f(0.0F, 1.0F);
               GL11.glVertex3f(-var7 / 2.0F, var8 / 2.0F, 0.0F);
               GL11.glTexCoord2f(1.0F, 1.0F);
               GL11.glVertex3f(var7 / 2.0F, var8 / 2.0F, 0.0F);
               GL11.glTexCoord2f(1.0F, 0.0F);
               GL11.glVertex3f(var7 / 2.0F, -var8 / 2.0F, 0.0F);
            } else {
               GL11.glTexCoord2f(0.0F, 0.0F);
               GL11.glVertex3f(0.0F, 0.0F, 0.0F);
               GL11.glTexCoord2f(0.0F, 1.0F);
               GL11.glVertex3f(0.0F, var8, 0.0F);
               GL11.glTexCoord2f(1.0F, 1.0F);
               GL11.glVertex3f(var7, var8, 0.0F);
               GL11.glTexCoord2f(1.0F, 0.0F);
               GL11.glVertex3f(var7, 0.0F, 0.0F);
            }

            GL11.glEnd();
         }

         var0.material.getTexture().detach();
         GlUtil.glPopMatrix();
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GlUtil.glDisable(3042);
         GlUtil.glEnable(2929);
         GlUtil.glDisable(2903);
         GlUtil.glEnable(2896);
         if (EngineSettings.G_CULLING_ACTIVE.isOn() && var0.isFlipCulling()) {
            GlUtil.glEnable(2884);
            GL11.glCullFace(1029);
         }

      }
   }

   public static void endDraw(Sprite var0) {
      GlUtil.glDisableClientState(32884);
      GlUtil.glDisableClientState(32888);
      GlUtil.glDisableClientState(32885);
      var0.material.getTexture().detach();
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GL11.glScalef(1.0F, 1.0F, 1.0F);
      GlUtil.glDisable(3042);
      GlUtil.glEnable(2929);
      GlUtil.glDisable(2903);
      GlUtil.glEnable(2896);
      if (EngineSettings.G_CULLING_ACTIVE.isOn() && var0.isFlipCulling()) {
         GlUtil.glEnable(2884);
         GL11.glCullFace(1029);
      }

   }

   public static void startDraw(Sprite var0) {
      lastTexId = -1;
      if (var0.firstDraw) {
         var0.onInit();
      }

      if (EngineSettings.G_CULLING_ACTIVE.isOn()) {
         var0.activateCulling();
      }

      GlUtil.glDisable(2896);
      if (var0.depthTest) {
         GlUtil.glEnable(2929);
      } else {
         GlUtil.glDisable(2929);
      }

      if (var0.isBlend()) {
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
      } else {
         GlUtil.glDisable(3042);
      }

      var0.material.getTexture().attach(0);
      if (var0.VBOActive && EngineSettings.G_USE_SPRITE_VBO.isOn()) {
         GlUtil.glEnableClientState(32884);
         GlUtil.glEnableClientState(32888);
         GlUtil.glBindBuffer(34962, var0.VBOverts);
         GL11.glVertexPointer(3, 5126, 0, 0L);
         GlUtil.glActiveTexture(33984);
         if (var0.getTint() != null) {
            GlUtil.glColor4f(var0.getTint().x, var0.getTint().y, var0.getTint().z, var0.getTint().w);
         } else {
            GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         }

         GlUtil.glBindBuffer(34962, var0.VBOTexCoords.get(var0.getSelectedMultiSprite()));
         GL11.glTexCoordPointer(2, 5126, 0, 0L);
      } else {
         assert false;

      }
   }

   public static void draw3D(Sprite var0, Collection var1, Camera var2) {
      if (var0.firstDraw) {
         var0.onInit();
      }

      if (!var0.isInvisible()) {
         SelectableSprite var3 = null;
         boolean var4;
         boolean var5;
         if (var1 instanceof List) {
            var4 = var1.size() > 0 && ((List)var1).get(0) instanceof PositionableSubColorSprite;
            var5 = var1.size() > 0 && ((List)var1).get(0) instanceof SelectableSprite;
         } else {
            var4 = var1.size() > 0 && var1.iterator().next() instanceof PositionableSubColorSprite;
            var5 = var1.size() > 0 && var1.iterator().next() instanceof SelectableSprite;
         }

         if (EngineSettings.G_CULLING_ACTIVE.isOn()) {
            var0.activateCulling();
         }

         GlUtil.glPushMatrix();
         GlUtil.glDisable(2896);
         if (var0.depthTest) {
            GlUtil.glEnable(2929);
         } else {
            GlUtil.glDisable(2929);
         }

         if (var0.isBlend()) {
            GlUtil.glEnable(3042);
            if (var0.blendFunc == 0) {
               GlUtil.glBlendFunc(770, 771);
            } else if (var0.blendFunc == 2) {
               GlUtil.glBlendFunc(770, 1);
            } else {
               assert var0.blendFunc == 1;

               GlUtil.glBlendFuncSeparate(770, 1, 1, 771);
            }
         } else {
            GlUtil.glDisable(3042);
         }

         float var6 = 0.0F;
         float var7 = 0.0F;
         if (var5) {
            var6 = (float)Mouse.getX();
            var7 = (float)Mouse.getY();
         }

         var0.material.getTexture().attach(0);
         if (var0.VBOActive && EngineSettings.G_USE_SPRITE_VBO.isOn()) {
            if (var0.useShader) {
               ShaderLibrary.selectionShader.loadWithoutUpdate();
               GlUtil.updateShaderFloat(ShaderLibrary.selectionShader, "texMult", 1.0F);
               GlUtil.updateShaderVector4f(ShaderLibrary.selectionShader, "selectionColor", 1.0F, 1.0F, 1.0F, 1.0F);
               GlUtil.updateShaderInt(ShaderLibrary.selectionShader, "mainTexA", 0);
            }

            GlUtil.glEnableClientState(32884);
            GlUtil.glEnableClientState(32888);
            GlUtil.glBindBuffer(34962, var0.VBOverts);
            GL11.glVertexPointer(3, 5126, 0, 0L);
            GlUtil.glActiveTexture(33984);
            if (var0.getTint() != null) {
               GlUtil.glColor4f(var0.getTint().x, var0.getTint().y, var0.getTint().z, var0.getTint().w);
            } else {
               GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            }

            GlUtil.glPushMatrix();
            int var8 = -1;
            Vector3f var9 = var2.getUp(upTmp);
            Vector3f var10 = var2.getRight(rightTmp);
            var9.scale(0.3F);
            var10.scale(0.3F);
            long var19 = System.currentTimeMillis();
            if (var1 instanceof List) {
               List var18;
               int var20 = (var18 = (List)var1).size();

               for(int var22 = 0; var22 < var20; ++var22) {
                  PositionableSubSprite var23 = (PositionableSubSprite)var18.get(var22);
                  if (var2.isPointInFrustrum(var23.getPos()) && var23.getSubSprite(var0) >= 0 && var23.canDraw()) {
                     var0.setSelectedMultiSprite(var23.getSubSprite(var0));
                     if (var0.getSelectedMultiSprite() != var8) {
                        GlUtil.glBindBuffer(34962, var0.VBOTexCoords.get(var0.getSelectedMultiSprite()));
                        GL11.glTexCoordPointer(2, 5126, 0, 0L);
                        var8 = var0.getSelectedMultiSprite();
                     }

                     if (var4) {
                        PositionableSubColorSprite var16 = (PositionableSubColorSprite)var23;
                        if (var0.useShader) {
                           GlUtil.updateShaderVector4f(ShaderLibrary.selectionShader, "selectionColor", var16.getColor().x, var16.getColor().y, var16.getColor().z, var16.getColor().w);
                        }

                        GlUtil.glColor4f(var16.getColor().x, var16.getColor().y, var16.getColor().z, var16.getColor().w);
                     }

                     GlUtil.glPushMatrix();
                     if (!var0.positionCenter) {
                        GlUtil.translateModelview(var23.getPos().x + var9.x + var10.x, var23.getPos().y + var9.y + var10.y, var23.getPos().z + var9.z + var10.z);
                     } else {
                        GlUtil.translateModelview(var23.getPos().x, var23.getPos().y, var23.getPos().z);
                     }

                     float var24 = var23.getScale(var19);
                     tempModelviewBuffer.put(0, var24);
                     tempModelviewBuffer.put(5, -var24);
                     tempModelviewBuffer.put(10, var24);
                     tempModelviewBuffer.put(12, Controller.modelviewMatrix.m30);
                     tempModelviewBuffer.put(13, Controller.modelviewMatrix.m31);
                     tempModelviewBuffer.put(14, Controller.modelviewMatrix.m32);
                     tempModelviewBuffer.rewind();
                     GlUtil.glLoadMatrix(tempModelviewBuffer);
                     if (var5 && ((SelectableSprite)var23).isSelectable()) {
                        Matrix4f.transform(Controller.modelviewMatrix, ss, outTmp);
                        outTmpCp.set(outTmp);
                        Matrix4f.transform(Controller.projectionMatrix, outTmpCp, outTmp);
                        var24 = outTmp.z / outTmp.w * 0.5F + 0.5F;
                        if (getMousePosition(var0, var6, var7, var24)) {
                           if (var3 != null) {
                              if (var3.getSelectionDepth() > ((SelectableSprite)var23).getSelectionDepth()) {
                                 var3.onUnSelect();
                                 ((SelectableSprite)var23).onSelect(var24);
                                 var3 = (SelectableSprite)var23;
                              } else {
                                 ((SelectableSprite)var23).onUnSelect();
                              }
                           } else {
                              ((SelectableSprite)var23).onSelect(var24);
                              var3 = (SelectableSprite)var23;
                           }
                        } else {
                           ((SelectableSprite)var23).onUnSelect();
                        }
                     }

                     GL11.glDrawArrays(7, 0, 4);
                     GlUtil.glPopMatrix();
                  }
               }
            } else {
               Iterator var17 = var1.iterator();

               while(var17.hasNext()) {
                  PositionableSubSprite var13 = (PositionableSubSprite)var17.next();
                  if (var2.isPointInFrustrum(var13.getPos()) && var13.getSubSprite(var0) >= 0 && var13.canDraw()) {
                     var0.setSelectedMultiSprite(var13.getSubSprite(var0));
                     if (var0.getSelectedMultiSprite() != var8) {
                        GlUtil.glBindBuffer(34962, var0.VBOTexCoords.get(var0.getSelectedMultiSprite()));
                        GL11.glTexCoordPointer(2, 5126, 0, 0L);
                        var8 = var0.getSelectedMultiSprite();
                     }

                     if (var4) {
                        PositionableSubColorSprite var14 = (PositionableSubColorSprite)var13;
                        if (var0.useShader) {
                           GlUtil.updateShaderVector4f(ShaderLibrary.selectionShader, "selectionColor", var14.getColor().x, var14.getColor().y, var14.getColor().z, var14.getColor().w);
                        }

                        GlUtil.glColor4f(var14.getColor().x, var14.getColor().y, var14.getColor().z, var14.getColor().w);
                     }

                     GlUtil.glPushMatrix();
                     if (!var0.positionCenter) {
                        GlUtil.translateModelview(var13.getPos().x + var9.x + var10.x, var13.getPos().y + var9.y + var10.y, var13.getPos().z + var9.z + var10.z);
                     } else {
                        GlUtil.translateModelview(var13.getPos().x, var13.getPos().y, var13.getPos().z);
                     }

                     float var21 = var13.getScale(var19);
                     tempModelviewBuffer.put(0, var21);
                     tempModelviewBuffer.put(5, -var21);
                     tempModelviewBuffer.put(10, var21);
                     tempModelviewBuffer.put(12, Controller.modelviewMatrix.m30);
                     tempModelviewBuffer.put(13, Controller.modelviewMatrix.m31);
                     tempModelviewBuffer.put(14, Controller.modelviewMatrix.m32);
                     tempModelviewBuffer.rewind();
                     GlUtil.glLoadMatrix(tempModelviewBuffer);
                     if (var5 && ((SelectableSprite)var13).isSelectable()) {
                        Matrix4f.transform(Controller.modelviewMatrix, ss, outTmp);
                        outTmpCp.set(outTmp);
                        Matrix4f.transform(Controller.projectionMatrix, outTmpCp, outTmp);
                        float var15 = outTmp.z / outTmp.w * 0.5F + 0.5F;
                        if (getMousePosition(var0, var6, var7, var15)) {
                           if (var3 != null) {
                              if (var3.getSelectionDepth() > ((SelectableSprite)var13).getSelectionDepth()) {
                                 var3.onUnSelect();
                                 ((SelectableSprite)var13).onSelect(var15);
                                 var3 = (SelectableSprite)var13;
                              } else {
                                 ((SelectableSprite)var13).onUnSelect();
                              }
                           } else {
                              ((SelectableSprite)var13).onSelect(var15);
                              var3 = (SelectableSprite)var13;
                           }
                        } else {
                           ((SelectableSprite)var13).onUnSelect();
                        }
                     }

                     GL11.glDrawArrays(7, 0, 4);
                     GlUtil.glPopMatrix();
                  }
               }
            }

            GlUtil.glPopMatrix();
            if (var0.useShader) {
               ShaderLibrary.selectionShader.unloadWithoutExit();
            }

            GlUtil.glDisableClientState(32884);
            GlUtil.glDisableClientState(32888);
            GlUtil.glDisableClientState(32885);
         } else {
            float var11 = (float)var0.material.getTexture().getWidth() * var0.getScale().x;
            float var12 = (float)var0.material.getTexture().getHeight() * var0.getScale().y;
            GL11.glBegin(7);
            if (var0.positionCenter) {
               GL11.glTexCoord2f(0.0F, 0.0F);
               GL11.glVertex3f(-var11 / 2.0F, -var12 / 2.0F, 0.0F);
               GL11.glTexCoord2f(0.0F, 1.0F);
               GL11.glVertex3f(-var11 / 2.0F, var12 / 2.0F, 0.0F);
               GL11.glTexCoord2f(1.0F, 1.0F);
               GL11.glVertex3f(var11 / 2.0F, var12 / 2.0F, 0.0F);
               GL11.glTexCoord2f(1.0F, 0.0F);
               GL11.glVertex3f(var11 / 2.0F, -var12 / 2.0F, 0.0F);
            } else {
               GL11.glTexCoord2f(0.0F, 0.0F);
               GL11.glVertex3f(0.0F, 0.0F, 0.0F);
               GL11.glTexCoord2f(0.0F, 1.0F);
               GL11.glVertex3f(0.0F, var12, 0.0F);
               GL11.glTexCoord2f(1.0F, 1.0F);
               GL11.glVertex3f(var11, var12, 0.0F);
               GL11.glTexCoord2f(1.0F, 0.0F);
               GL11.glVertex3f(var11, 0.0F, 0.0F);
            }

            GL11.glEnd();
         }

         var0.material.getTexture().detach();
         GlUtil.glPopMatrix();
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GlUtil.glDisable(3042);
         GlUtil.glEnable(2929);
         GlUtil.glDisable(2903);
         GlUtil.glEnable(2896);
         if (EngineSettings.G_CULLING_ACTIVE.isOn()) {
            var0.activateCulling();
         }

      }
   }

   public static void draw3D(Sprite var0, PositionableSubSprite[] var1, Camera var2) {
      if (var0.firstDraw) {
         var0.onInit();
      }

      if (!var0.isInvisible()) {
         boolean var3 = var1.length > 0 && var1[0] instanceof PositionableSubColorSprite;
         boolean var4 = var1.length > 0 && var1[0] instanceof SelectableSprite;
         if (EngineSettings.G_CULLING_ACTIVE.isOn()) {
            var0.activateCulling();
         }

         GlUtil.glPushMatrix();
         SelectableSprite var5 = null;
         GlUtil.glDisable(2896);
         if (var0.depthTest) {
            GlUtil.glEnable(2929);
         } else {
            GlUtil.glDisable(2929);
         }

         if (var0.isBlend()) {
            GlUtil.glEnable(3042);
            if (var0.blendFunc == 0) {
               GlUtil.glBlendFunc(770, 771);
            } else {
               GlUtil.glBlendFuncSeparate(770, 1, 1, 771);
            }
         } else {
            GlUtil.glDisable(3042);
         }

         float var6 = 0.0F;
         float var7 = 0.0F;
         if (var4) {
            var6 = (float)Mouse.getX();
            var7 = (float)Mouse.getY();
         }

         var0.material.getTexture().attach(0);
         if (var0.VBOActive && EngineSettings.G_USE_SPRITE_VBO.isOn()) {
            ShaderLibrary.selectionShader.loadWithoutUpdate();
            GlUtil.updateShaderFloat(ShaderLibrary.selectionShader, "texMult", 1.0F);
            GlUtil.updateShaderVector4f(ShaderLibrary.selectionShader, "selectionColor", 1.0F, 1.0F, 1.0F, 1.0F);
            GlUtil.updateShaderInt(ShaderLibrary.selectionShader, "mainTexA", 0);
            GlUtil.glEnableClientState(32884);
            GlUtil.glEnableClientState(32888);
            GlUtil.glBindBuffer(34962, var0.VBOverts);
            GL11.glVertexPointer(3, 5126, 0, 0L);
            GlUtil.glActiveTexture(33984);
            if (var0.getTint() != null) {
               GlUtil.glColor4f(var0.getTint().x, var0.getTint().y, var0.getTint().z, var0.getTint().w);
            } else {
               GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            }

            GlUtil.glPushMatrix();
            int var8 = -1;
            Vector3f var9 = var2.getUp(upTmp);
            Vector3f var10 = var2.getRight(rightTmp);
            var9.scale(0.3F);
            var10.scale(0.3F);
            long var11 = System.currentTimeMillis();

            for(int var16 = 0; var16 < var1.length; ++var16) {
               PositionableSubSprite var14 = var1[var16];
               if (var2.isPointInFrustrum(var14.getPos()) && var14.canDraw()) {
                  var0.setSelectedMultiSprite(var14.getSubSprite(var0));
                  if (var0.getSelectedMultiSprite() != var8) {
                     GlUtil.glBindBuffer(34962, var0.VBOTexCoords.get(var0.getSelectedMultiSprite()));
                     GL11.glTexCoordPointer(2, 5126, 0, 0L);
                     var8 = var0.getSelectedMultiSprite();
                  }

                  if (var3) {
                     PositionableSubColorSprite var15 = (PositionableSubColorSprite)var14;
                     GlUtil.updateShaderVector4f(ShaderLibrary.selectionShader, "selectionColor", var15.getColor().x, var15.getColor().y, var15.getColor().z, var15.getColor().w);
                     GlUtil.glColor4f(var15.getColor().x, var15.getColor().y, var15.getColor().z, var15.getColor().w);
                  }

                  GlUtil.glPushMatrix();
                  if (!var0.positionCenter) {
                     GlUtil.translateModelview(var14.getPos().x + var9.x + var10.x, var14.getPos().y + var9.y + var10.y, var14.getPos().z + var9.z + var10.z);
                  } else {
                     GlUtil.translateModelview(var14.getPos().x, var14.getPos().y, var14.getPos().z);
                  }

                  float var17 = var14.getScale(var11);
                  tempModelviewBuffer.put(0, var17);
                  tempModelviewBuffer.put(5, -var17);
                  tempModelviewBuffer.put(10, var17);
                  tempModelviewBuffer.put(12, Controller.modelviewMatrix.m30);
                  tempModelviewBuffer.put(13, Controller.modelviewMatrix.m31);
                  tempModelviewBuffer.put(14, Controller.modelviewMatrix.m32);
                  tempModelviewBuffer.rewind();
                  GlUtil.glLoadMatrix(tempModelviewBuffer);
                  if (var4 && ((SelectableSprite)var14).isSelectable()) {
                     Matrix4f.transform(Controller.modelviewMatrix, ss, outTmp);
                     outTmpCp.set(outTmp);
                     Matrix4f.transform(Controller.projectionMatrix, outTmpCp, outTmp);
                     var17 = outTmp.z / outTmp.w * 0.5F + 0.5F;
                     if (getMousePosition(var0, var6, var7, var17)) {
                        if (var5 != null) {
                           if (var5.getSelectionDepth() > ((SelectableSprite)var14).getSelectionDepth()) {
                              var5.onUnSelect();
                              ((SelectableSprite)var14).onSelect(var17);
                              var5 = (SelectableSprite)var14;
                           } else {
                              ((SelectableSprite)var14).onUnSelect();
                           }
                        } else {
                           ((SelectableSprite)var14).onSelect(var17);
                           var5 = (SelectableSprite)var14;
                        }
                     } else {
                        ((SelectableSprite)var14).onUnSelect();
                     }
                  }

                  GL11.glDrawArrays(7, 0, 4);
                  GlUtil.glPopMatrix();
               }
            }

            GlUtil.glPopMatrix();
            ShaderLibrary.selectionShader.unloadWithoutExit();
            GlUtil.glDisableClientState(32884);
            GlUtil.glDisableClientState(32888);
         } else {
            float var12 = (float)var0.material.getTexture().getWidth() * var0.getScale().x;
            float var13 = (float)var0.material.getTexture().getHeight() * var0.getScale().y;
            GL11.glBegin(7);
            if (var0.positionCenter) {
               GL11.glTexCoord2f(0.0F, 0.0F);
               GL11.glVertex3f(-var12 / 2.0F, -var13 / 2.0F, 0.0F);
               GL11.glTexCoord2f(0.0F, 1.0F);
               GL11.glVertex3f(-var12 / 2.0F, var13 / 2.0F, 0.0F);
               GL11.glTexCoord2f(1.0F, 1.0F);
               GL11.glVertex3f(var12 / 2.0F, var13 / 2.0F, 0.0F);
               GL11.glTexCoord2f(1.0F, 0.0F);
               GL11.glVertex3f(var12 / 2.0F, -var13 / 2.0F, 0.0F);
            } else {
               GL11.glTexCoord2f(0.0F, 0.0F);
               GL11.glVertex3f(0.0F, 0.0F, 0.0F);
               GL11.glTexCoord2f(0.0F, 1.0F);
               GL11.glVertex3f(0.0F, var13, 0.0F);
               GL11.glTexCoord2f(1.0F, 1.0F);
               GL11.glVertex3f(var12, var13, 0.0F);
               GL11.glTexCoord2f(1.0F, 0.0F);
               GL11.glVertex3f(var12, 0.0F, 0.0F);
            }

            GL11.glEnd();
         }

         var0.material.getTexture().detach();
         GlUtil.glPopMatrix();
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GlUtil.glDisable(3042);
         GlUtil.glEnable(2929);
         GlUtil.glDisable(2903);
         GlUtil.glEnable(2896);
         if (EngineSettings.G_CULLING_ACTIVE.isOn()) {
            var0.activateCulling();
         }

      }
   }

   public static void draw3D(Sprite var0, TransformableSubSprite[] var1, int var2, Camera var3) {
      if (var0.firstDraw) {
         var0.onInit();
      }

      if (!var0.isInvisible()) {
         if (EngineSettings.G_CULLING_ACTIVE.isOn()) {
            var0.activateCulling();
         }

         boolean var9 = var1.length > 0 && var1[0] instanceof PositionableSubColorSprite;
         GlUtil.glPushMatrix();
         GlUtil.glDisable(2896);
         if (var0.depthTest) {
            GlUtil.glEnable(2929);
         } else {
            GlUtil.glDisable(2929);
         }

         if (var0.isBlend()) {
            GlUtil.glEnable(3042);
            GlUtil.glBlendFunc(770, 771);
         } else {
            GlUtil.glDisable(3042);
         }

         var0.material.getTexture().attach(0);
         if (var0.VBOActive && EngineSettings.G_USE_SPRITE_VBO.isOn()) {
            GlUtil.glEnableClientState(32884);
            GlUtil.glEnableClientState(32888);
            GlUtil.glBindBuffer(34962, var0.VBOverts);
            GL11.glVertexPointer(3, 5126, 0, 0L);
            GlUtil.glActiveTexture(33984);
            if (var0.getTint() != null) {
               GlUtil.glColor4f(var0.getTint().x, var0.getTint().y, var0.getTint().z, var0.getTint().w);
            } else {
               GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            }

            GlUtil.glBindBuffer(34962, var0.VBOTexCoords.get(var0.getSelectedMultiSprite()));
            GL11.glTexCoordPointer(2, 5126, 0, 0L);
            int var4 = -1;
            if (var2 <= 0) {
               GlUtil.glPushMatrix();
               if (var0.billboard) {
                  GlUtil.translateModelview(var0.getPos());
                  tempModelviewBuffer.put(0, var0.getScale().x);
                  tempModelviewBuffer.put(5, -var0.getScale().y);
                  tempModelviewBuffer.put(10, var0.getScale().z);
                  tempModelviewBuffer.put(12, Controller.modelviewMatrix.m30);
                  tempModelviewBuffer.put(13, Controller.modelviewMatrix.m31);
                  tempModelviewBuffer.put(14, Controller.modelviewMatrix.m32);
                  tempModelviewBuffer.rewind();
                  GlUtil.glLoadMatrix(tempModelviewBuffer);
               }

               GL11.glDrawArrays(7, 0, 4);
               GlUtil.glPopMatrix();
            } else {
               GlUtil.glPushMatrix();

               for(int var5 = 0; var5 < var2; ++var5) {
                  GlUtil.glPushMatrix();
                  TransformableSubSprite var6 = var1[var5];
                  if (var9) {
                     PositionableSubColorSprite var10 = (PositionableSubColorSprite)var6;
                     if (GlUtil.loadedShader == ShaderLibrary.selectionShader) {
                        GlUtil.updateShaderFloat(ShaderLibrary.selectionShader, "texMult", 1.0F);
                        GlUtil.updateShaderVector4f(ShaderLibrary.selectionShader, "selectionColor", var10.getColor().x, var10.getColor().y, var10.getColor().z, var10.getColor().w);
                     }

                     GlUtil.glColor4f(var10.getColor().x, var10.getColor().y, var10.getColor().z, var10.getColor().w);
                  }

                  GlUtil.glMultMatrix(var6.getWorldTransform());
                  var0.setSelectedMultiSprite(var6.getSubSprite(var0));
                  if (var0.getSelectedMultiSprite() != var4) {
                     GlUtil.glBindBuffer(34962, var0.VBOTexCoords.get(var0.getSelectedMultiSprite()));
                     GL11.glTexCoordPointer(2, 5126, 0, 0L);
                     var4 = var0.getSelectedMultiSprite();
                  }

                  if (var0.billboard) {
                     GlUtil.glLoadMatrix(var0.getBillboardSphericalBeginMatrix());
                  }

                  GlUtil.scaleModelview(var6.getScale(0L), var6.getScale(0L), var6.getScale(0L));
                  GL11.glDrawArrays(7, 0, 4);
                  GlUtil.glPopMatrix();
               }

               GlUtil.glPopMatrix();
            }

            GlUtil.glDisableClientState(32884);
            GlUtil.glDisableClientState(32888);
         } else {
            float var7 = (float)var0.material.getTexture().getWidth() * var0.getScale().x;
            float var8 = (float)var0.material.getTexture().getHeight() * var0.getScale().y;
            GL11.glBegin(7);
            if (var0.positionCenter) {
               GL11.glTexCoord2f(0.0F, 0.0F);
               GL11.glVertex3f(-var7 / 2.0F, -var8 / 2.0F, 0.0F);
               GL11.glTexCoord2f(0.0F, 1.0F);
               GL11.glVertex3f(-var7 / 2.0F, var8 / 2.0F, 0.0F);
               GL11.glTexCoord2f(1.0F, 1.0F);
               GL11.glVertex3f(var7 / 2.0F, var8 / 2.0F, 0.0F);
               GL11.glTexCoord2f(1.0F, 0.0F);
               GL11.glVertex3f(var7 / 2.0F, -var8 / 2.0F, 0.0F);
            } else {
               GL11.glTexCoord2f(0.0F, 0.0F);
               GL11.glVertex3f(0.0F, 0.0F, 0.0F);
               GL11.glTexCoord2f(0.0F, 1.0F);
               GL11.glVertex3f(0.0F, var8, 0.0F);
               GL11.glTexCoord2f(1.0F, 1.0F);
               GL11.glVertex3f(var7, var8, 0.0F);
               GL11.glTexCoord2f(1.0F, 0.0F);
               GL11.glVertex3f(var7, 0.0F, 0.0F);
            }

            GL11.glEnd();
         }

         var0.material.getTexture().detach();
         GlUtil.glPopMatrix();
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GlUtil.glDisable(3042);
         GlUtil.glEnable(2929);
         GlUtil.glDisable(2903);
         GlUtil.glEnable(2896);
         if (EngineSettings.G_CULLING_ACTIVE.isOn() && var0.isFlipCulling()) {
            GlUtil.glEnable(2884);
            GL11.glCullFace(1029);
         }

      }
   }

   public static boolean getMousePosition(Sprite var0, float var1, float var2, float var3) {
      Matrix4f var4 = Controller.modelviewMatrix;
      Matrix4f var5 = Controller.projectionMatrix;
      viewportTemp.rewind();
      viewportTemp.put(Controller.viewport);
      Controller.viewport.rewind();
      viewportTemp.rewind();
      viewportTemp.put(0, 0);
      viewportTemp.put(1, 0);
      viewportTemp.put(2, viewportTemp.get(2));
      viewportTemp.put(3, viewportTemp.get(3));
      if (modelviewTemp == null) {
         projectionTemp = BufferUtils.createFloatBuffer(16);
         modelviewTemp = BufferUtils.createFloatBuffer(16);
      }

      modelviewTemp.rewind();
      var4.store(modelviewTemp);
      modelviewTemp.rewind();
      projectionTemp.rewind();
      var5.store(projectionTemp);
      projectionTemp.rewind();
      GLU.gluUnProject(var1, var2, var3, modelviewTemp, projectionTemp, viewportTemp, coord);
      var1 = coord.get(0);
      var2 = coord.get(1);
      coord.get(2);
      if (var0.getSelectionAreaLength() > 0.0F) {
         return FastMath.carmackSqrt(var1 * var1 + var2 * var2) < var0.getSelectionAreaLength();
      } else {
         var1 += (float)(var0.getWidth() / 2);
         var2 += (float)(var0.getHeight() / 2);
         boolean var7 = var1 < (float)var0.getWidth() && var1 > 0.0F;
         boolean var6 = var2 < (float)var0.getHeight() && var2 > 0.0F;
         return var7 && var6;
      }
   }

   public float getSelectionAreaLength() {
      return this.selectionAreaLength;
   }

   public void setSelectionAreaLength(float var1) {
      this.selectionAreaLength = var1;
   }

   public void draw() {
      GlUtil.glPushMatrix();
      if (this.isFlip()) {
         GL11.glScalef(1.0F, -1.0F, 1.0F);
      }

      this.transform();
      draw(this, 0, (PositionableSubSprite[])());
      GlUtil.glPopMatrix();
   }

   public void drawRaw() {
      if (GUIElement.translateOnlyMode) {
         this.translate();
      } else {
         GlUtil.glPushMatrix();
         if (this.isFlip()) {
            GL11.glScalef(1.0F, -1.0F, 1.0F);
         }

         this.transform();
      }

      drawRaw(this);
      if (GUIElement.translateOnlyMode) {
         this.translateBack();
      } else {
         GlUtil.glPopMatrix();
      }
   }

   public void onInit() {
      if (this.firstDraw) {
         if (this.VBOActive) {
            int var1 = 1;
            int var2 = 1;
            if (this.getMultiSpriteMax() > 1) {
               var1 = this.multiSpriteMaxX;
               var2 = this.multiSpriteMaxY;
            }

            float var3 = 1.0F / (float)var1;
            float var4 = 1.0F / (float)var2;
            if (vertices == null) {
               vertices = BufferUtils.createFloatBuffer(sSizeVerts);
               texCoords = BufferUtils.createFloatBuffer(sSizeTex);
            } else {
               vertices.rewind();
               texCoords.rewind();
            }

            Vector3D[] var5;
            int var7;
            try {
               var5 = new Vector3D[4];
               Vector3D[] var6 = new Vector3D[4];
               if (this.isPositionCenter()) {
                  var5[3] = new Vector3D((float)(-this.width / 2), (float)(-this.height / 2), 0.0F);
                  var6[3] = new Vector3D(0.0F, 0.0F, 1.0F);
                  var5[2] = new Vector3D((float)(this.width / 2), (float)(-this.height / 2), 0.0F);
                  var6[2] = new Vector3D(0.0F, 0.0F, 1.0F);
                  var5[1] = new Vector3D((float)(this.width / 2), (float)(this.height / 2), 0.0F);
                  var6[1] = new Vector3D(0.0F, 0.0F, 1.0F);
                  var5[0] = new Vector3D((float)(-this.width / 2), (float)(this.height / 2), 0.0F);
                  var6[0] = new Vector3D(0.0F, 0.0F, 1.0F);
               } else {
                  var5[3] = new Vector3D(0.0F, 0.0F, 0.0F);
                  var6[3] = new Vector3D(0.0F, 0.0F, 1.0F);
                  var5[2] = new Vector3D((float)this.width, 0.0F, 0.0F);
                  var6[2] = new Vector3D(0.0F, 0.0F, 1.0F);
                  var5[1] = new Vector3D((float)this.width, (float)this.height, 0.0F);
                  var6[1] = new Vector3D(0.0F, 0.0F, 1.0F);
                  var5[0] = new Vector3D(0.0F, (float)this.height, 0.0F);
                  var6[0] = new Vector3D(0.0F, 0.0F, 1.0F);
               }

               for(var7 = 0; var7 < var5.length; ++var7) {
                  Vector3D var8 = var5[var7];
                  vertices.put(var8.getX());
                  vertices.put(var8.getY());
                  vertices.put(var8.getZ());
               }
            } catch (BufferOverflowException var10) {
               var5 = null;
               var10.printStackTrace();
            }

            vertices.rewind();
            this.VBOverts = GL15.glGenBuffers();
            Controller.loadedVBOBuffers.add(this.VBOverts);
            GlUtil.glBindBuffer(34962, this.VBOverts);
            GL15.glBufferData(34962, vertices, 35044);
            this.VBOTexCoords = BufferUtils.createIntBuffer(this.getMultiSpriteMax());
            GL15.glGenBuffers(this.VBOTexCoords);
            Controller.loadedVBOBuffers.add(this.VBOTexCoords.get(0));
            int var11 = 0;

            for(int var12 = 0; var12 < var2; ++var12) {
               for(var7 = 0; var7 < var1; ++var7) {
                  Vector3f[] var13 = this.getTexcoords((float)var7 * var3, (float)(var7 + 1) * var3, (float)var12 * var4, (float)(var12 + 1) * var4);
                  texCoords.rewind();

                  for(int var9 = 0; var9 < var13.length; ++var9) {
                     texCoords.put(var13[var9].x);
                     texCoords.put(var13[var9].y);
                  }

                  texCoords.rewind();
                  GlUtil.glBindBuffer(34962, this.VBOTexCoords.get(var11));
                  GL15.glBufferData(34962, texCoords, 35044);
                  ++var11;
               }
            }
         }

         this.firstDraw = false;
      }
   }

   public int getHeight() {
      return this.height;
   }

   public void setHeight(int var1) {
      this.height = var1;
   }

   public int getMultiSpriteMax() {
      return this.multiSpriteMax;
   }

   public int getSelectedMultiSprite() {
      return this.selectedMultiSprite;
   }

   public void setSelectedMultiSprite(int var1) {
      assert var1 < this.getMultiSpriteMax() && var1 >= 0 : "tried to set " + var1 + " / " + this.getMultiSpriteMax();

      this.selectedMultiSprite = var1;
   }

   private Vector3f[] getTexcoords(float var1, float var2, float var3, float var4) {
      Vector3f[] var5;
      (var5 = new Vector3f[4])[3] = new Vector3f(var1, var3, 0.0F);
      var5[2] = new Vector3f(var2, var3, 0.0F);
      var5[1] = new Vector3f(var2, var4, 0.0F);
      var5[0] = new Vector3f(var1, var4, 0.0F);
      return var5;
   }

   public Vector4f getTint() {
      return this.tint;
   }

   public void setTint(Vector4f var1) {
      this.tint = var1;
   }

   public int getWidth() {
      return this.width;
   }

   public void setWidth(int var1) {
      this.width = var1;
   }

   public boolean isBillboard() {
      return this.billboard;
   }

   public void setBillboard(boolean var1) {
      this.billboard = var1;
   }

   public boolean isBlend() {
      return this.blend;
   }

   public void setBlend(boolean var1) {
      this.blend = var1;
   }

   public boolean isDepthTest() {
      return this.depthTest;
   }

   public void setDepthTest(boolean var1) {
      this.depthTest = var1;
   }

   public boolean isFlip() {
      return this.flip;
   }

   public void setFlip(boolean var1) {
      this.flip = var1;
   }

   public boolean isOrthogonal() {
      return this.orthogonal;
   }

   public void setOrthogonal(boolean var1) {
      this.orthogonal = var1;
   }

   public boolean isPositionCenter() {
      return this.positionCenter;
   }

   public void setPositionCenter(boolean var1) {
      this.positionCenter = var1;
   }

   public void setMultiSpriteMax(int var1, int var2) {
      this.multiSpriteMax = var1 * var2;
      this.multiSpriteMaxX = var1;
      this.multiSpriteMaxY = var2;
   }

   public void update(Timer var1) {
      System.err.println("Cannot update static Sprite");
   }

   public int getMultiSpriteMaxX() {
      return this.multiSpriteMaxX;
   }

   public int getMultiSpriteMaxY() {
      return this.multiSpriteMaxY;
   }

   static {
      Matrix4f var0;
      (var0 = new Matrix4f()).setIdentity();
      var0.scale(new org.lwjgl.util.vector.Vector3f(0.01F, -0.01F, 0.01F));
      var0.store(tempModelviewBuffer);
      tempModelviewBuffer.rewind();
      rightTmp = new Vector3f();
      upTmp = new Vector3f();
      outTmp = new org.lwjgl.util.vector.Vector4f();
      ss = new org.lwjgl.util.vector.Vector4f(0.0F, 0.0F, 0.0F, 1.0F);
      outTmpCp = new org.lwjgl.util.vector.Vector4f();
   }
}
