package org.schema.schine.graphicsengine.forms.simple;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;

public class Box {
   public static Vector2f[][] getTexCoords() {
      Vector2f[][] var0 = new Vector2f[6][4];

      for(int var1 = 0; var1 < var0.length; ++var1) {
         var0[var1][0] = new Vector2f(0.0F, 0.0F);
         var0[var1][1] = new Vector2f(1.0F, 0.0F);
         var0[var1][2] = new Vector2f(1.0F, 1.0F);
         var0[var1][3] = new Vector2f(0.0F, 1.0F);
      }

      return var0;
   }

   public static Vector2f[][] getTexCoords(float var0, Vector2f[][] var1) {
      for(int var2 = 0; var2 < var1.length; ++var2) {
         var1[var2][0] = new Vector2f(0.0F, 0.0F);
         var1[var2][1] = new Vector2f(var0, 0.0F);
         var1[var2][2] = new Vector2f(var0, var0);
         var1[var2][3] = new Vector2f(0.0F, var0);
      }

      return var1;
   }

   public static Vector3f[][] getVertices(Vector3f var0, Vector3f var1) {
      return getVertices(var0, var1, init());
   }

   public static Vector3f[][] getVertices(Vector3f var0, Vector3f var1, Vector3f[][] var2) {
      assert var2 != null;

      assert var2.length == 6 : var2.length;

      assert var2[0].length == 4 : var2[0].length;

      var2[0][0].set(var1.x, var0.y, var0.z);
      var2[0][1].set(var1.x, var1.y, var0.z);
      var2[0][2].set(var1.x, var1.y, var1.z);
      var2[0][3].set(var1.x, var0.y, var1.z);
      var2[1][3].set(var0.x, var0.y, var0.z);
      var2[1][2].set(var0.x, var1.y, var0.z);
      var2[1][1].set(var0.x, var1.y, var1.z);
      var2[1][0].set(var0.x, var0.y, var1.z);
      var2[2][3].set(var0.x, var1.y, var0.z);
      var2[2][2].set(var1.x, var1.y, var0.z);
      var2[2][1].set(var1.x, var1.y, var1.z);
      var2[2][0].set(var0.x, var1.y, var1.z);
      var2[3][0].set(var0.x, var0.y, var0.z);
      var2[3][1].set(var1.x, var0.y, var0.z);
      var2[3][2].set(var1.x, var0.y, var1.z);
      var2[3][3].set(var0.x, var0.y, var1.z);
      var2[4][0].set(var0.x, var0.y, var1.z);
      var2[4][1].set(var1.x, var0.y, var1.z);
      var2[4][2].set(var1.x, var1.y, var1.z);
      var2[4][3].set(var0.x, var1.y, var1.z);
      var2[5][3].set(var0.x, var0.y, var0.z);
      var2[5][2].set(var1.x, var0.y, var0.z);
      var2[5][1].set(var1.x, var1.y, var0.z);
      var2[5][0].set(var0.x, var1.y, var0.z);
      return var2;
   }

   public static Vector3f[][] init() {
      Vector3f[][] var0 = new Vector3f[6][4];

      for(int var1 = 0; var1 < var0.length; ++var1) {
         for(int var2 = 0; var2 < var0[var1].length; ++var2) {
            var0[var1][var2] = new Vector3f();
         }
      }

      return var0;
   }
}
