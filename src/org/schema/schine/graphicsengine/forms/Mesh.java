package org.schema.schine.graphicsengine.forms;

import com.bulletphysics.collision.shapes.ConvexHullShape;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.schema.common.util.data.DataUtil;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.texture.Material;
import org.schema.schine.input.Keyboard;
import org.schema.schine.physics.Physical;
import org.schema.schine.resource.FileExt;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class Mesh extends Geometry {
   private static final int MAX_SIMUL_VBO_BUILDING = 1;
   public static final int TYPE_SOFTWARE = 0;
   public static final int TYPE_DISPLAY_LIST = 1;
   public static final int TYPE_VERTEX_ARRAY = 2;
   public static final int TYPE_VERTEX_BUFFER_OBJ = 3;
   public static final int BUFFERTYPE_Index = 1;
   public static final int BUFFERTYPE_Position = 2;
   public static final int BUFFERTYPE_Normal = 3;
   public static final int BUFFERTYPE_Tangent = 4;
   public static final int BUFFERTYPE_Binormal = 5;
   public static final int BUFFERTYPE_TexCoord = 6;
   private static final int BUFFERTYPE_IndexNormals = 7;
   private static final int BUFFERTYPE_IndexTexcords = 8;
   private static int buildingVBOCount;
   private static int cycles;
   public ConvexShape shape;
   public Mesh.Face[] faces;
   public Vector3f[] texCoords;
   public Vector3f[] normals;
   public IntBuffer VBOindex = BufferUtils.createIntBuffer(1);
   public IntBuffer normalIndexBuffer;
   public IntBuffer texCoordIndexBuffer;
   public FloatBuffer texCoordsBuffer;
   public FloatBuffer normalsBuffer;
   public int texCoordSetCount;
   public int currentTexCoordSet;
   protected int faceCount;
   private boolean indicedNormals = false;
   private IntBuffer VBOVertices = BufferUtils.createIntBuffer(1);
   private IntBuffer VBOTexCoords = BufferUtils.createIntBuffer(1);
   private IntBuffer VBOTexCoordsindex = BufferUtils.createIntBuffer(1);
   private IntBuffer VBONormalindex = BufferUtils.createIntBuffer(1);
   private IntBuffer VBONormals = BufferUtils.createIntBuffer(1);
   private IntBuffer VBOTangents = BufferUtils.createIntBuffer(1);
   private IntBuffer VBOBinormals = BufferUtils.createIntBuffer(1);
   private int currentBufferIndex = 0;
   private int type;
   private int texCoordCount;
   private boolean collisionObject;
   private int normalCount;
   private boolean firstDraw;
   private VertexBoneWeight[] vertexBoneAssignments;
   private boolean boneDrawMode;
   private Vector3f collisionVec;
   private Vector3f collisionFaceNormal;
   private boolean updated;
   private Skin skin;
   private boolean drawingWireframe;
   private boolean staticMesh;
   private int phase = 0;
   private int drawMode = 4;
   private boolean meshPointersLoaded;
   private Vector3f cTmp = new Vector3f();
   private boolean vboLoaded;
   public Vector3f[] tangents;
   public Vector3f[] binormals;
   public IntBuffer tangentIndexBuffer;
   public IntBuffer binormalIndexBuffer;
   public FloatBuffer tangentsBuffer;
   public FloatBuffer binormalsBuffer;
   public boolean hasTangents;
   public boolean hasBinormals;
   public IntArrayList recordedIndices = new IntArrayList();
   public ObjectArrayList recordedVectices = new ObjectArrayList();
   public ObjectArrayList recordedNormals = new ObjectArrayList();
   public ObjectArrayList recordedTextcoords = new ObjectArrayList();

   public Mesh() {
      this.setFirstDraw(true);
      this.material = new Material();
   }

   private static void addTrianglesRecursive(Mesh var0, MeshGroup var1, int var2, float var3) {
      Iterator var8 = var0.getChilds().iterator();

      while(true) {
         AbstractSceneNode var4;
         do {
            if (!var8.hasNext()) {
               return;
            }
         } while(!((var4 = (AbstractSceneNode)var8.next()) instanceof Mesh));

         Mesh var9;
         Mesh[] var5 = getTriangles(var9 = (Mesh)var4, var2, var3);

         for(int var6 = 0; var6 < var5.length; var6 += var2) {
            Mesh var7;
            if ((var7 = var5[var6]) != null) {
               var1.attach(var7);
            }
         }

         addTrianglesRecursive(var9, var1, var2, var3);
      }
   }

   public static void buildVBOs(Mesh var0) throws Exception {
      if (var0.phase != 0 || buildingVBOCount <= 0) {
         if (var0.phase == 0) {
            ++buildingVBOCount;
         }

         ++cycles;
         if (var0.phase == 0 || cycles % 5 == 0) {
            int var1 = var0.getFaceCount() * 3 * 3;
            int var2 = (var0.getFaceCount() << 1) * 3;
            int var3 = var0.getFaceCount() * 3 * 3;
            System.currentTimeMillis();
            if (var0.phase == 0) {
               ++var0.phase;
            } else if (var0.phase == 1) {
               ++var0.phase;
            } else if (var0.phase == 2) {
               ++var0.phase;
            } else {
               System.currentTimeMillis();
               System.currentTimeMillis();
               if (var0.phase == 3) {
                  ++var0.phase;
               } else {
                  System.currentTimeMillis();
                  System.currentTimeMillis();
                  System.currentTimeMillis();
                  System.currentTimeMillis();
                  if (var0.phase == 4) {
                     assert var0.verticesBuffer.position() == 0;

                     createVertexBuffers(2, var0, var1);
                     ++var0.phase;
                  } else if (var0.phase == 5) {
                     assert var0.texCoordsBuffer.position() == 0;

                     createVertexBuffers(6, var0, var2);
                     ++var0.phase;
                  } else if (var0.phase == 6) {
                     assert var0.normalsBuffer.position() == 0;

                     createVertexBuffers(3, var0, var3);
                     ++var0.phase;
                  } else if (var0.phase == 7) {
                     if (var0.hasTangents) {
                        assert var0.tangentsBuffer.position() == 0;

                        createVertexBuffers(4, var0, var3);
                     }

                     ++var0.phase;
                  } else if (var0.phase == 8) {
                     if (var0.hasBinormals) {
                        assert var0.binormalsBuffer.position() == 0;

                        createVertexBuffers(5, var0, var3);
                     }

                     ++var0.phase;
                  } else if (var0.phase == 9) {
                     assert var0.getIndexBuffer().position() == 0;

                     createVertexBuffers(1, var0, var0.getIndexBuffer().capacity());
                     ++var0.phase;
                  } else {
                     System.currentTimeMillis();
                     if (var0.phase == 10) {
                        var0.setType(3);
                        --buildingVBOCount;
                        var0.setLoaded(true);
                     }

                     GlUtil.glBindBuffer(34962, 0);
                  }
               }
            }
         }
      }
   }

   public static BoundingBox calculateBoundingBox(Mesh var0) {
      Vector3f var1 = new Vector3f(Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE);
      Vector3f var2 = new Vector3f(Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY);

      for(int var3 = 0; var3 < var0.vertCount; ++var3) {
         var1.x = Math.min(var1.x, var0.vertices[var3].x);
         var2.x = Math.max(var2.x, var0.vertices[var3].x);
         var1.y = Math.min(var1.y, var0.vertices[var3].y);
         var2.y = Math.max(var2.y, var0.vertices[var3].y);
         var1.z = Math.min(var1.z, var0.vertices[var3].z);
         var2.z = Math.max(var2.z, var0.vertices[var3].z);
      }

      return new BoundingBox(var1, var2);
   }

   private static void createVertexBuffers(int var0, Mesh var1, int var2) {
      switch(var0) {
      case 1:
         if (var1.VBOindex.get(0) == 0) {
            GL15.glGenBuffers(var1.VBOindex);
            Controller.loadedVBOBuffers.add(var1.VBOindex.get(0));
         }

         GlUtil.glBindBuffer(34963, var1.VBOindex.get(0));
         GL15.glBufferData(34963, var1.getIndexBuffer(), 35040);
         GlUtil.glBindBuffer(34963, 0);
         break;
      case 2:
         GL15.glGenBuffers(var1.VBOVertices);
         Controller.loadedVBOBuffers.add(var1.VBOVertices.get(0));
         GlUtil.glBindBuffer(34962, var1.VBOVertices.get(0));
         GL15.glBufferData(34962, var1.verticesBuffer, 35044);
         GlUtil.glBindBuffer(34962, 0);
         break;
      case 3:
         GL15.glGenBuffers(var1.VBONormals);
         Controller.loadedVBOBuffers.add(var1.VBONormals.get(0));
         GlUtil.glBindBuffer(34962, var1.VBONormals.get(0));
         GL15.glBufferData(34962, var1.normalsBuffer, 35044);
         GlUtil.glBindBuffer(34962, 0);
         break;
      case 4:
         GL15.glGenBuffers(var1.VBOTangents);
         Controller.loadedVBOBuffers.add(var1.VBOTangents.get(0));
         GlUtil.glBindBuffer(34962, var1.VBOTangents.get(0));
         GL15.glBufferData(34962, var1.tangentsBuffer, 35044);
         GlUtil.glBindBuffer(34962, 0);
         break;
      case 5:
         GL15.glGenBuffers(var1.VBOBinormals);
         Controller.loadedVBOBuffers.add(var1.VBOBinormals.get(0));
         GlUtil.glBindBuffer(34962, var1.VBOBinormals.get(0));
         GL15.glBufferData(34962, var1.binormalsBuffer, 35044);
         GlUtil.glBindBuffer(34962, 0);
         break;
      case 6:
         GL15.glGenBuffers(var1.VBOTexCoords);
         Controller.loadedVBOBuffers.add(var1.VBOTexCoords.get(0));
         GlUtil.glBindBuffer(34962, var1.VBOTexCoords.get(0));
         GL15.glBufferData(34962, var1.texCoordsBuffer, 35044);
         GlUtil.glBindBuffer(34962, 0);
         break;
      case 7:
         if (var1.VBOindex.get(0) == 0) {
            GL15.glGenBuffers(var1.VBOindex);
            Controller.loadedVBOBuffers.add(var1.VBOindex.get(0));
         }

         GlUtil.glBindBuffer(34963, var1.VBONormalindex.get(0));
         GL15.glBufferData(34963, var1.normalIndexBuffer, 35040);
         GlUtil.glBindBuffer(34963, 0);
         break;
      case 8:
         if (var1.VBOindex.get(0) == 0) {
            GL15.glGenBuffers(var1.VBOindex);
            Controller.loadedVBOBuffers.add(var1.VBOindex.get(0));
         }

         GlUtil.glBindBuffer(34963, var1.VBOTexCoordsindex.get(0));
         GL15.glBufferData(34963, var1.texCoordIndexBuffer, 35040);
         GlUtil.glBindBuffer(34963, 0);
      }

      GlUtil.printGlErrorCritical();
   }

   private static Mesh[] getTriangles(Mesh var0, int var1, float var2) {
      Mesh[] var3 = new Mesh[var0.getFaceCount()];

      String var4;
      try {
         var4 = "<PhysicsObject><ShapeType>triangle</ShapeType><CylinderOrientation>x</CylinderOrientation></PhysicsObject>";
         DocumentBuilder var5 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
         InputSource var6;
         (var6 = new InputSource()).setCharacterStream(new StringReader(var4));
         Document var16 = var5.parse(var6);

         for(int var17 = 0; var17 < var0.getFaceCount(); var17 += var1) {
            Mesh.Face var18 = var0.faces[var17];
            Vector3f var7 = var0.faces[var17].getCentroid(var0);
            Vector3f var8 = Vector3fTools.sub(var0.vertices[var18.m_vertsIndex[1]], var0.vertices[var18.m_vertsIndex[0]]);
            Vector3f var9 = Vector3fTools.sub(var0.vertices[var18.m_vertsIndex[2]], var0.vertices[var18.m_vertsIndex[0]]);
            Vector3f var10 = Vector3fTools.sub(var0.vertices[var18.m_vertsIndex[2]], var0.vertices[var18.m_vertsIndex[1]]);
            if (var8.length() + var9.length() + var10.length() >= var2) {
               var3[var17] = new Mesh();
               var3[var17].setIndicedNormals(true);
               var3[var17].setUserData(var16);
               var3[var17].vertCount = 6;
               var3[var17].vertices = new Vector3f[var3[var17].vertCount];
               var3[var17].normalCount = 6;
               var3[var17].normals = new Vector3f[var3[var17].normalCount];
               var3[var17].texCoordCount = 6;
               var3[var17].texCoords = new Vector3f[var3[var17].texCoordCount];
               var3[var17].faceCount = 8;
               var3[var17].faces = new Mesh.Face[var3[var17].faceCount];
               var8 = Vector3fTools.sub(var0.getInitionPos(), var7);
               var3[var17].vertices[0] = Vector3fTools.add(var8, var0.vertices[var18.m_vertsIndex[0]]);
               var3[var17].normals[0] = new Vector3f(var0.normals[var18.m_normalIndex[0]]);
               var3[var17].texCoords[0] = new Vector3f(var0.texCoords[var18.m_texCoordsIndex[0]]);
               var3[var17].vertices[1] = Vector3fTools.add(var8, var0.vertices[var18.m_vertsIndex[1]]);
               var3[var17].normals[1] = new Vector3f(var0.normals[var18.m_normalIndex[1]]);
               var3[var17].texCoords[1] = new Vector3f(var0.texCoords[var18.m_texCoordsIndex[1]]);
               var3[var17].vertices[2] = Vector3fTools.add(var8, var0.vertices[var18.m_vertsIndex[2]]);
               var3[var17].normals[2] = new Vector3f(var0.normals[var18.m_normalIndex[2]]);
               var3[var17].texCoords[2] = new Vector3f(var0.texCoords[var18.m_texCoordsIndex[2]]);
               var3[var17].vertices[3] = new Vector3f(var3[var17].vertices[0]);
               var3[var17].normals[3] = new Vector3f(var3[var17].normals[0]);
               var3[var17].texCoords[3] = new Vector3f(var3[var17].texCoords[0]);
               var3[var17].vertices[4] = new Vector3f(var3[var17].vertices[1]);
               var3[var17].normals[4] = new Vector3f(var3[var17].normals[1]);
               var3[var17].texCoords[4] = new Vector3f(var3[var17].texCoords[1]);
               var3[var17].vertices[5] = new Vector3f(var3[var17].vertices[2]);
               var3[var17].normals[5] = new Vector3f(var3[var17].normals[2]);
               var3[var17].texCoords[5] = new Vector3f(var3[var17].texCoords[2]);
               Vector3f var19 = Vector3fTools.sub(var7, var3[var17].vertices[0]);
               var8 = Vector3fTools.sub(var7, var3[var17].vertices[1]);
               var9 = Vector3fTools.sub(var7, var3[var17].vertices[2]);
               var19.normalize();
               var8.normalize();
               var9.normalize();
               var19.scale(1.5F);
               var8.scale(1.5F);
               var9.scale(1.5F);
               var3[var17].normals[0].scale(-1.0F);
               var3[var17].normals[1].scale(-1.0F);
               var3[var17].normals[2].scale(-1.0F);
               var10 = new Vector3f(var3[var17].normals[0]);
               Vector3f var11 = new Vector3f(var3[var17].normals[1]);
               Vector3f var12 = new Vector3f(var3[var17].normals[2]);
               var10.scale(-1.5F);
               var11.scale(-1.5F);
               var12.scale(-1.5F);
               var3[var17].vertices[0].add(var10);
               var3[var17].vertices[1].add(var11);
               var3[var17].vertices[2].add(var12);
               var3[var17].vertices[0].add(var19);
               var3[var17].vertices[1].add(var8);
               var3[var17].vertices[2].add(var9);
               var3[var17].vertices[3].add(var10);
               var3[var17].vertices[4].add(var11);
               var3[var17].vertices[5].add(var12);
               var3[var17].vertices[3].add(var19);
               var3[var17].vertices[4].add(var8);
               var3[var17].vertices[5].add(var9);
               var3[var17].setScale(1.0F, 1.0F, 1.0F);
               var3[var17].setInitionPos(var7);
               var3[var17].setInitialScale(new Vector3f(1.0F, 1.0F, 1.0F));
               var3[var17].setInitialQuadRot(new Vector4f(0.0F, 0.0F, 0.0F, 1.0F));

               for(int var20 = 0; var20 < var3[var17].faceCount; ++var20) {
                  var3[var17].faces[var20] = new Mesh.Face();
               }

               int[] var21 = new int[8];
               int[] var22 = new int[8];
               int[] var23 = new int[8];
               var21[0] = 0;
               var22[0] = 1;
               var23[0] = 2;
               var21[1] = 3;
               var22[1] = 5;
               var23[1] = 4;
               var21[2] = 5;
               var22[2] = 0;
               var23[2] = 2;
               var21[3] = 5;
               var22[3] = 3;
               var23[3] = 0;
               var21[4] = 3;
               var22[4] = 4;
               var23[4] = 0;
               var21[5] = 0;
               var22[5] = 4;
               var23[5] = 1;
               var21[6] = 2;
               var22[6] = 4;
               var23[6] = 5;
               var21[7] = 2;
               var22[7] = 1;
               var23[7] = 4;

               for(int var24 = 0; var24 < var3[var17].faceCount; ++var24) {
                  var3[var17].faces[var24].setVertIndex(var23[var24], var22[var24], var21[var24]);
                  var3[var17].faces[var24].setNormalIndex(var23[var24], var22[var24], var21[var24]);
                  var3[var17].faces[var24].setTexCoordIndex(var23[var24], var22[var24], var21[var24]);
               }

               var3[var17].setBoundingBox(calculateBoundingBox(var3[var17]));
               var3[var17].setPivot(var7);
            }
         }
      } catch (SAXException var13) {
         var4 = null;
         var13.printStackTrace();
      } catch (IOException var14) {
         var4 = null;
         var14.printStackTrace();
      } catch (ParserConfigurationException var15) {
         var4 = null;
         var15.printStackTrace();
      }

      return var3;
   }

   public static Mesh loadObj(String var0) throws FileNotFoundException {
      Mesh var1;
      (var1 = new Mesh()).setName(var0);
      int var2 = 0;
      int var3 = 0;
      int var4 = 0;
      int var5 = 0;
      FileExt var17;
      if (!(var17 = new FileExt(var0)).exists()) {
         throw new FileNotFoundException(var17.getPath());
      } else {
         BufferedReader var6 = new BufferedReader(new FileReader(var17));

         try {
            String var7;
            while(var6.ready()) {
               if ((var7 = var6.readLine()).contains("vn ")) {
                  ++var1.normalCount;
               }

               if (var7.contains("vt ")) {
                  var1.setTexCoordCount(var1.getTexCoordCount() + 1);
               }

               if (var7.contains("clazz ")) {
                  var1.setVertCount(var1.getVertCount() + 1);
               }

               if (var7.contains("f ")) {
                  var1.setFaceCount(var1.getFaceCount() + 1);
               }
            }

            var1.normals = new Vector3f[var1.normalCount];
            var1.texCoords = new Vector3f[var1.getTexCoordCount()];
            var1.vertices = new Vector3f[var1.getVertCount()];
            var1.faces = new Mesh.Face[var1.getFaceCount()];

            int var19;
            for(var19 = 0; var19 < var1.faces.length; ++var19) {
               var1.faces[var19] = new Mesh.Face();
            }

            var6.close();
            var6 = new BufferedReader(new FileReader(var17));

            while(true) {
               String[] var18;
               do {
                  if (!var6.ready()) {
                     return var1;
                  }

                  if ((var7 = var6.readLine()).contains("vn ")) {
                     var18 = var7.split("[\\s]+");
                     var1.normals[var3] = new Vector3f(Float.parseFloat(var18[1]), Float.parseFloat(var18[2]), Float.parseFloat(var18[3]));
                     ++var3;
                  }

                  if (var7.contains("vt ")) {
                     var18 = var7.split("[\\s]+");
                     var1.texCoords[var4] = new Vector3f(Float.parseFloat(var18[1]), Float.parseFloat(var18[2]), Float.parseFloat(var18[3]));
                     ++var4;
                  }

                  if (var7.contains("clazz ")) {
                     var18 = var7.split("[\\s]+");
                     var1.vertices[var2] = new Vector3f(Float.parseFloat(var18[1]), Float.parseFloat(var18[2]), Float.parseFloat(var18[3]));
                     ++var2;
                  }
               } while(!var7.contains("f "));

               var18 = var7.split("[\\s]+");
               var1.setIndicedNormals(true);

               for(var19 = 1; var19 < var18.length; ++var19) {
                  String[] var8 = var18[var19].split("/");
                  var1.faces[var5].m_vertsIndex[var19 - 1] = Integer.parseInt(var8[0]) - 1;
                  var1.faces[var5].m_texCoordsIndex[var19 - 1] = Integer.parseInt(var8[1]) - 1;
                  var1.faces[var5].m_normalIndex[var19 - 1] = Integer.parseInt(var8[2]) - 1;
               }

               ++var5;
            }
         } catch (IOException var15) {
            var15.printStackTrace();
         } finally {
            try {
               var6.close();
            } catch (IOException var14) {
               var14.printStackTrace();
            }

         }

         return var1;
      }
   }

   public static void main(String[] var0) {
      try {
         loadObj(DataUtil.dataPath + "test.obj");
      } catch (FileNotFoundException var1) {
         var1.printStackTrace();
      }
   }

   public static void setMaterialToAllChildrenRecusivly(AbstractSceneNode var0, Material var1) {
      var0.setMaterial(var1);
      Iterator var2 = var0.getChilds().iterator();

      while(var2.hasNext()) {
         setMaterialToAllChildrenRecusivly((AbstractSceneNode)var2.next(), var1);
      }

   }

   public static MeshGroup splitTriangles(Mesh var0) {
      MeshGroup var1 = new MeshGroup();
      if (var0 instanceof Mesh) {
         addTrianglesRecursive(var0, var1, 1, 30.0F);
      }

      var1.setBoundingBox(var1.makeBiggestBoundingBox(new Vector3f(), new Vector3f()));
      return var1;
   }

   public static void arrayStaticVBODraw(Collection var0, Mesh var1, boolean var2) {
      if (var1.isFirstDraw()) {
         var1.onInit();
         var1.setFirstDraw(false);
      }

      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glColor4f(0.9F, 0.9F, 0.9F, 1.0F);
      GlUtil.glDisable(3042);
      GlUtil.glEnable(2903);
      GlUtil.glEnable(2896);
      GlUtil.glEnable(2929);
      var1.material.attach(0);
      switch(var1.getType()) {
      case 3:
         GlUtil.glEnableClientState(32884);
         GlUtil.glEnableClientState(32888);
         GlUtil.glEnableClientState(32885);
         GlUtil.glBindBuffer(34962, var1.getVBOVertices().get(0));
         GL11.glVertexPointer(3, 5126, 0, 0L);
         GlUtil.glBindBuffer(34962, var1.getVBOTexCoords().get(0));
         GL11.glTexCoordPointer(2, 5126, 0, 0L);
         if (var1.material.isMaterialBumpMapped()) {
            GlUtil.glActiveTexture(33985);
            GlUtil.glBindBuffer(34962, var1.getVBOTexCoords().get(0));
            GL11.glTexCoordPointer(2, 5126, 0, 0L);
         }

         if (var1.material.isSpecularMapped()) {
            GlUtil.glActiveTexture(33986);
            GlUtil.glBindBuffer(34962, var1.getVBOTexCoords().get(0));
            GL11.glTexCoordPointer(2, 5126, 0, 0L);
         }

         GlUtil.glBindBuffer(34962, var1.getVBONormals().get(0));
         GL11.glNormalPointer(5126, 0, 0L);
         int var5 = var1.getFaceCount() * 3;
         var1.setCollisionObject(false);
         Iterator var4 = var0.iterator();

         while(var4.hasNext()) {
            SimplePosElement var3 = (SimplePosElement)var4.next();
            GlUtil.glPushMatrix();
            GL11.glTranslatef((float)var3.x, (float)var3.y, (float)var3.z);
            GL11.glDrawArrays(4, 0, var5);
            GlUtil.glPopMatrix();
         }

         GlUtil.glDisableClientState(32884);
         GlUtil.glDisableClientState(32888);
         var1.deactivateCulling();
         GlUtil.glDisableClientState(32885);
         Iterator var6 = var1.getChilds().iterator();

         while(var6.hasNext()) {
            ((AbstractSceneNode)var6.next()).draw();
         }

         var1.deactivateCulling();
         var1.material.detach();
         return;
      default:
         throw new IllegalArgumentException("this Mesh is no Vertex Buffer Object: " + var1.toString() + " is " + var1.getType());
      }
   }

   public static void drawFastVBOInstancedPositionOnly(Collection var0, Mesh var1) {
      if (var1 instanceof MeshGroup) {
         var1 = (Mesh)((MeshGroup)var1).getChilds().iterator().next();
      }

      if (!var1.isInvisible() && !var0.isEmpty()) {
         if (var1.isFirstDraw()) {
            var1.onInit();
            var1.setFirstDraw(false);
         }

         GlUtil.glDisable(3553);
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GlUtil.glDisable(3042);
         GlUtil.glDisable(2903);
         GlUtil.glEnable(2896);
         GlUtil.glEnable(2929);
         var1.material.attach(0);
         GlUtil.glEnable(3553);
         switch(var1.type) {
         case 3:
            GlUtil.glEnableClientState(32884);
            GlUtil.glEnableClientState(32888);
            GlUtil.glEnableClientState(32885);
            GlUtil.glBindBuffer(34962, var1.getVBOVertices().get(0));
            GL11.glVertexPointer(3, 5126, 0, 0L);
            GlUtil.glBindBuffer(34962, var1.getVBOTexCoords().get(0));
            GL11.glTexCoordPointer(2, 5126, 0, 0L);
            if (var1.material.isMaterialBumpMapped()) {
               GlUtil.glActiveTexture(33985);
               GlUtil.glBindBuffer(34962, var1.getVBOTexCoords().get(0));
               GL11.glTexCoordPointer(2, 5126, 0, 0L);
            }

            if (var1.material.isSpecularMapped()) {
               GlUtil.glActiveTexture(33986);
               GlUtil.glBindBuffer(34962, var1.getVBOTexCoords().get(0));
               GL11.glTexCoordPointer(2, 5126, 0, 0L);
            }

            GlUtil.glBindBuffer(34962, var1.getVBONormals().get(0));
            GL11.glNormalPointer(5126, 0, 0L);
            var1.activateCulling();
            GlUtil.glPushMatrix();
            Vector3f var2 = new Vector3f();
            Vector3f var3 = new Vector3f();
            Iterator var5 = var0.iterator();

            while(var5.hasNext()) {
               Positionable var4 = (Positionable)var5.next();
               var3.set(var4.getPos());
               var3.sub(var2);
               GL11.glTranslatef(var3.x, var3.y, var3.z);
               GL11.glDrawArrays(4, 0, var1.getFaceCount() * 3);
               var2.set(var4.getPos());
            }

            GlUtil.glPopMatrix();
            GlUtil.glDisableClientState(32884);
            GlUtil.glDisableClientState(32888);
            GlUtil.glDisableClientState(32885);
            Iterator var6 = var1.getChilds().iterator();

            while(var6.hasNext()) {
               ((AbstractSceneNode)var6.next()).draw();
            }

            var1.deactivateCulling();
            var1.material.detach();
            return;
         default:
            throw new IllegalArgumentException("this Mesh is no Vertex Buffer Object " + var1.toString() + ": " + var1.type);
         }
      }
   }

   public static void staticVBOMultidraw(HashSet var0, Mesh var1) {
      if (!var1.isInvisible() && !var0.isEmpty()) {
         if (var1.isFirstDraw()) {
            var1.onInit();
            var1.setFirstDraw(false);
         }

         GlUtil.glDisable(3553);
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GlUtil.glDisable(3042);
         GlUtil.glDisable(2903);
         GlUtil.glEnable(2896);
         GlUtil.glEnable(2929);
         var1.material.attach(0);
         GlUtil.glEnable(3553);
         switch(var1.type) {
         case 3:
            GlUtil.glEnableClientState(32884);
            GlUtil.glEnableClientState(32888);
            GlUtil.glEnableClientState(32885);
            GlUtil.glBindBuffer(34962, var1.getVBOVertices().get(0));
            GL11.glVertexPointer(3, 5126, 0, 0L);
            GlUtil.glBindBuffer(34962, var1.getVBOTexCoords().get(0));
            GL11.glTexCoordPointer(2, 5126, 0, 0L);
            if (var1.material.isMaterialBumpMapped()) {
               GlUtil.glActiveTexture(33985);
               GlUtil.glBindBuffer(34962, var1.getVBOTexCoords().get(0));
               GL11.glTexCoordPointer(2, 5126, 0, 0L);
            }

            if (var1.material.isSpecularMapped()) {
               GlUtil.glActiveTexture(33986);
               GlUtil.glBindBuffer(34962, var1.getVBOTexCoords().get(0));
               GL11.glTexCoordPointer(2, 5126, 0, 0L);
            }

            GlUtil.glBindBuffer(34962, var1.getVBONormals().get(0));
            GL11.glNormalPointer(5126, 0, 0L);
            Iterator var3 = var0.iterator();

            while(var3.hasNext()) {
               Physical var2 = (Physical)var3.next();
               GlUtil.glPushMatrix();
               var1.activateCulling();
               if (var2.getPhysicsDataContainer().isInitialized()) {
                  var1.setCollisionObject(true);
               } else {
                  var1.setCollisionObject(false);
               }

               var1.transform();
               GL11.glDrawArrays(4, 0, var1.getFaceCount() * 3);
               GlUtil.glPopMatrix();
            }

            GlUtil.glDisableClientState(32884);
            GlUtil.glDisableClientState(32888);
            GlUtil.glDisableClientState(32885);
            var3 = var1.getChilds().iterator();

            while(var3.hasNext()) {
               ((AbstractSceneNode)var3.next()).draw();
            }

            var1.deactivateCulling();
            var1.material.detach();
            return;
         default:
            throw new IllegalArgumentException("this Mesh is no Vertex Buffer Oject " + var1.toString());
         }
      }
   }

   public void loadPhysicsMeeshConvexHull() {
      this.verticesBuffer.rewind();
      int var1 = this.verticesBuffer.limit() / 3;
      com.bulletphysics.util.ObjectArrayList var2 = new com.bulletphysics.util.ObjectArrayList(var1);

      for(int var3 = 0; var3 < var1; ++var3) {
         var2.add(new Vector3f(this.verticesBuffer.get() * 0.75F, this.verticesBuffer.get() * 0.75F, this.verticesBuffer.get() * 0.75F));
      }

      ConvexHullShape var4;
      (var4 = new ConvexHullShape(var2)).setMargin(0.0F);
      this.shape = var4;
      this.verticesBuffer.rewind();
   }

   public void retainVertices() {
      com.bulletphysics.util.ObjectArrayList var1 = this.getVerticesListInstanceFromBuffer();
      this.vertices = (Vector3f[])var1.toArray(new Vector3f[var1.size()]);
      this.vertCount = this.vertices.length;
   }

   public com.bulletphysics.util.ObjectArrayList getVerticesListInstanceFromBuffer() {
      this.verticesBuffer.rewind();
      int var1 = this.verticesBuffer.limit() / 3;
      com.bulletphysics.util.ObjectArrayList var2 = new com.bulletphysics.util.ObjectArrayList(var1);

      for(int var3 = 0; var3 < var1; ++var3) {
         var2.add(new Vector3f(this.verticesBuffer.get(), this.verticesBuffer.get(), this.verticesBuffer.get()));
      }

      this.verticesBuffer.rewind();
      return var2;
   }

   public com.bulletphysics.util.ObjectArrayList getVerticesListInstance() {
      if (this.vertices == null) {
         throw new RuntimeException("No Vertices retained. used 'dedicated' physics mesh flag in mainConfig.xml to prevent vertices from being disarded in non graphics memory");
      } else {
         com.bulletphysics.util.ObjectArrayList var1 = new com.bulletphysics.util.ObjectArrayList(this.vertCount);

         for(int var2 = 0; var2 < this.vertCount; ++var2) {
            var1.add(this.vertices[var2]);
         }

         return var1;
      }
   }

   public List getVertices(List var1) {
      this.verticesBuffer.rewind();
      int var2 = this.verticesBuffer.limit() / 3;

      for(int var3 = 0; var3 < var2; ++var3) {
         var1.add(new Vector3f(this.verticesBuffer.get(), this.verticesBuffer.get(), this.verticesBuffer.get()));
      }

      this.verticesBuffer.rewind();
      return var1;
   }

   public void cleanUp() {
      if (this.getMaterial() != null) {
         this.getMaterial().cleanUp();
      }

      if (this.getType() == 3) {
         this.getVBOVertices().rewind();
         this.getVBOTexCoords().rewind();
         this.getVBONormals().rewind();
         GL15.glDeleteBuffers(this.getVBOVertices());
         GL15.glDeleteBuffers(this.getVBOTexCoords());
         GL15.glDeleteBuffers(this.getVBONormals());
      }

      Iterator var1 = this.getChilds().iterator();

      while(var1.hasNext()) {
         ((AbstractSceneNode)var1.next()).cleanUp();
      }

   }

   public void draw() {
      if (!this.isInvisible()) {
         if (EngineSettings.G_WIREFRAMED.isOn()) {
            this.drawingWireframe = true;
            GL11.glPolygonMode(1032, 6913);
         }

         this.preparedraw();
         switch(this.getType()) {
         case 0:
         case 1:
         default:
            System.err.println("SOFTWARE " + this);
            this.softwaredraw();
            break;
         case 2:
            throw new UnsupportedOperationException();
         case 3:
            assert this.VBOindex.get(0) != 0;

            this.drawVBO(true);
         }

         Iterator var1 = this.getChilds().iterator();

         while(var1.hasNext()) {
            ((AbstractSceneNode)var1.next()).draw();
         }

         this.finishdraw();
         if (EngineSettings.G_WIREFRAMED.isOn()) {
            GL11.glPolygonMode(1032, 6914);
            this.drawingWireframe = false;
         }

      }
   }

   public void onInit() {
   }

   public void clearBuffer(int var1) {
      switch(var1) {
      case 1:
         this.getIndexBuffer().clear();
         return;
      case 2:
         this.verticesBuffer.clear();
         return;
      case 3:
         this.normalsBuffer.clear();
      case 4:
      case 5:
      default:
         return;
      case 6:
         this.texCoordsBuffer.clear();
      }
   }

   public AbstractSceneNode clone() {
      return null;
   }

   public void drawVBO() {
      this.drawVBO(true);
   }

   public void drawVBO(boolean var1) {
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      this.loadVBO(var1);
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      this.renderVBO();
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical("DRAW MODE: " + this.drawMode + "; " + this.getName() + "; " + (this.getParent() != null ? this.getParent().getName() : "No Parent"));
      }

      this.unloadVBO(var1);
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

   }

   public void drawVBOAttributed() {
      assert this.meshPointersLoaded;

      GL11.glDrawElements(this.drawMode, this.faceCount * 3, 5125, 0L);
   }

   public void drawVBOInterleaved() {
      GlUtil.glEnableClientState(32884);
      GlUtil.glEnableClientState(32888);
      GlUtil.glEnableClientState(32885);
      GlUtil.glBindBuffer(34962, this.getVBOVertices().get(this.getCurrentBufferIndex()));
      GL11.glVertexPointer(3, 5126, 32, 0L);
      GlUtil.glActiveTexture(33984);
      GL11.glTexCoordPointer(2, 5126, 32, 12L);
      if (this.material.isMaterialBumpMapped()) {
         GlUtil.glActiveTexture(33985);
         GL11.glTexCoordPointer(2, 5126, 32, 12L);
      }

      if (this.material.isSpecularMapped()) {
         GlUtil.glActiveTexture(33986);
         GL11.glTexCoordPointer(2, 5126, 32, 12L);
      }

      GlUtil.glActiveTexture(33984);
      GL11.glNormalPointer(5126, 32, 20L);
      GlUtil.glBindBuffer(34963, this.VBOindex.get(0));
      GL11.glDrawElements(this.drawMode, this.faceCount * 3, 5125, 0L);
      GlUtil.glBindBuffer(34963, 0);
      GlUtil.glBindBuffer(34962, 0);
      GlUtil.glDisableClientState(32884);
      GlUtil.glDisableClientState(32888);
      GlUtil.glDisableClientState(32885);
   }

   private void finishdraw() {
      GlUtil.glPopMatrix();
      this.deactivateCulling();
      this.setCollisionObject(false);
      this.material.detach();
   }

   public Vector3f getCollisionFaceNormal() {
      return this.collisionFaceNormal;
   }

   public Vector3f getCollisionVec() {
      return this.collisionVec;
   }

   public void setCollisionVec(Vector3f var1) {
      this.collisionVec = var1;
   }

   public int getCurrentBufferIndex() {
      return this.currentBufferIndex;
   }

   public void setCurrentBufferIndex(int var1) {
      this.currentBufferIndex = var1;
   }

   public int getDrawMode() {
      return this.drawMode;
   }

   public void setDrawMode(int var1) {
      this.drawMode = var1;
   }

   public int getFaceCount() {
      return this.faceCount;
   }

   public void setFaceCount(int var1) {
      this.faceCount = var1;
   }

   public Skin getSkin() {
      return this.skin;
   }

   public void setSkin(Skin var1) {
      this.skin = var1;
   }

   public int getTexCoordCount() {
      return this.texCoordCount;
   }

   public void setTexCoordCount(int var1) {
      this.texCoordCount = var1;
   }

   public int getType() {
      return this.type;
   }

   public void setType(int var1) {
      this.type = var1;
   }

   public IntBuffer getVBONormals() {
      return this.VBONormals;
   }

   public void setVBONormals(IntBuffer var1) {
      this.VBONormals = var1;
   }

   public IntBuffer getVBOTexCoords() {
      return this.VBOTexCoords;
   }

   public void setVBOTexCoords(IntBuffer var1) {
      this.VBOTexCoords = var1;
   }

   public IntBuffer getVBOVertices() {
      return this.VBOVertices;
   }

   public void setVBOVertices(IntBuffer var1) {
      this.VBOVertices = var1;
   }

   public VertexBoneWeight[] getVertexBoneAssignments() {
      return this.vertexBoneAssignments;
   }

   public void setVertexBoneAssignments(VertexBoneWeight[] var1) {
      this.vertexBoneAssignments = var1;
   }

   public boolean isCollisionObject() {
      return this.collisionObject;
   }

   public void setCollisionObject(boolean var1) {
      this.collisionObject = var1;
   }

   public boolean isFirstDraw() {
      return this.firstDraw;
   }

   public void setFirstDraw(boolean var1) {
      this.firstDraw = var1;
   }

   public boolean isIndicedNormals() {
      return this.indicedNormals;
   }

   public void setIndicedNormals(boolean var1) {
      this.indicedNormals = var1;
   }

   public boolean isPivotCentered() {
      this.getBoundingBox().getCenter(this.cTmp);
      this.cTmp.sub(this.getPivot());
      return this.cTmp.length() < 2.0F;
   }

   public boolean isStaticMesh() {
      return this.staticMesh;
   }

   public void setStaticMesh(boolean var1) {
      this.staticMesh = var1;
   }

   public boolean isUpdated() {
      return this.updated;
   }

   public void setUpdated(boolean var1) {
      this.updated = var1;
   }

   public void loadMeshPointers() {
      assert !this.meshPointersLoaded : "Type is: " + this.getType();

      assert this.getType() == 3 : "Type is: " + this.getType();

      assert this.getVBOVertices().get(this.getCurrentBufferIndex()) != 0;

      assert this.getVBOTexCoords().get(this.getCurrentBufferIndex()) != 0;

      assert this.getVBONormals().get(this.getCurrentBufferIndex()) != 0;

      assert this.VBOindex.get(0) != 0;

      GL20.glEnableVertexAttribArray(0);
      GL20.glEnableVertexAttribArray(1);
      GL20.glEnableVertexAttribArray(2);
      GlUtil.glBindBuffer(34962, this.getVBOVertices().get(this.getCurrentBufferIndex()));
      GL20.glVertexAttribPointer(0, 3, 5126, false, 0, 0L);
      GlUtil.glBindBuffer(34962, this.getVBONormals().get(this.getCurrentBufferIndex()));
      GL20.glVertexAttribPointer(1, 3, 5126, false, 0, 0L);
      GlUtil.glBindBuffer(34962, this.getVBOTexCoords().get(this.getCurrentBufferIndex()));
      GL20.glVertexAttribPointer(2, 2, 5126, false, 0, 0L);
      GlUtil.glBindBuffer(34963, this.VBOindex.get(0));
      this.meshPointersLoaded = true;
   }

   public void loadVBO(boolean var1) {
      if (var1) {
         GlUtil.glEnableClientState(32884);
      }

      assert this.getVBOVertices().get(this.getCurrentBufferIndex()) != 0;

      GlUtil.glBindBuffer(34962, this.getVBOVertices().get(this.getCurrentBufferIndex()));
      GL11.glVertexPointer(3, 5126, 0, 0L);
      if (var1) {
         GlUtil.glEnableClientState(32885);
      }

      assert this.getVBONormals().get(this.getCurrentBufferIndex()) != 0;

      GlUtil.glBindBuffer(34962, this.getVBONormals().get(this.getCurrentBufferIndex()));
      GL11.glNormalPointer(5126, 0, 0L);
      if (var1) {
         GlUtil.glEnableClientState(32888);
      }

      assert this.getVBOTexCoords().get(this.getCurrentBufferIndex()) != 0;

      GlUtil.glBindBuffer(34962, this.getVBOTexCoords().get(this.getCurrentBufferIndex()));
      if (this.texCoordSetCount > 1) {
         GL11.glTexCoordPointer(2, 5126, this.texCoordSetCount << 3, (long)(this.currentTexCoordSet << 3));
      } else {
         GL11.glTexCoordPointer(2, 5126, 0, 0L);
      }

      if (this.hasTangents) {
         GlUtil.glBindBuffer(34962, this.VBOTangents.get(this.getCurrentBufferIndex()));
         GL11.glColorPointer(3, 5126, 0, 0L);
         if (var1) {
            GlUtil.glEnableClientState(32886);
         }
      }

      if (this.skin != null) {
         this.skin.loadVBO();
      }

      GlUtil.glBindBuffer(34963, this.VBOindex.get(0));
      this.vboLoaded = true;
   }

   private void preparedraw() {
      if (!this.isInvisible()) {
         if (this.isFirstDraw()) {
            this.onInit();
            this.setFirstDraw(false);
         }

         if (this.getParent() != null) {
            this.drawingWireframe = ((Mesh)this.getParent()).drawingWireframe;
         }

         GlUtil.glPushMatrix();
         GlUtil.glDisable(3553);
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GlUtil.glEnable(2896);
         GlUtil.glEnable(2929);
         if (this.drawingWireframe) {
            GlUtil.glDisable(3042);
            GlUtil.glDisable(2896);
            GlUtil.glDisable(3553);
            GlUtil.glEnable(2903);
            GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         } else {
            this.material.attach(0);
            GlUtil.glEnable(3553);
         }
      }
   }

   public void renderVBO() {
      GL11.glDrawElements(this.drawMode, this.faceCount * 3, 5125, 0L);
   }

   public void setBuffer(int var1, int var2, Buffer var3) {
      switch(var1) {
      case 1:
         this.setIndexBuffer((IntBuffer)var3);
         break;
      case 2:
         System.err.println("verticesBuffer set");
         this.verticesBuffer = (FloatBuffer)var3;
         break;
      case 3:
         this.normalsBuffer = (FloatBuffer)var3;
      case 4:
      case 5:
      default:
         break;
      case 6:
         this.texCoordsBuffer = (FloatBuffer)var3;
      }

      var3.rewind();
      createVertexBuffers(var1, this, var3.capacity());
   }

   private void softwaredraw() {
      if (this.skin != null && this.boneDrawMode) {
         GlUtil.glDisable(3553);
         GlUtil.glEnable(2903);
      }

      GL11.glBegin(4);
      if (this.faces != null && this.faces.length > 0) {
         for(int var1 = 0; var1 < this.faces.length; ++var1) {
            if (this.texCoords != null && this.faces[var1].m_texCoordsIndex != null) {
               GL11.glTexCoord2f(this.texCoords[this.faces[var1].m_texCoordsIndex[0]].x, this.texCoords[this.faces[var1].m_texCoordsIndex[0]].y);
            }

            if (this.indicedNormals) {
               GL11.glNormal3f(this.normals[this.faces[var1].m_normalIndex[0]].x, this.normals[this.faces[var1].m_normalIndex[0]].y, this.normals[this.faces[var1].m_normalIndex[0]].z);
            } else {
               GL11.glNormal3f(this.faces[var1].m_normals[0].x, this.faces[var1].m_normals[0].y, this.faces[var1].m_normals[0].z);
            }

            Vector3f[] var2;
            GL11.glVertex3f((var2 = this.vertices)[this.faces[var1].m_vertsIndex[0]].x, var2[this.faces[var1].m_vertsIndex[0]].y, var2[this.faces[var1].m_vertsIndex[0]].z);
            if (this.texCoords != null && this.faces[var1].m_texCoordsIndex != null) {
               GL11.glTexCoord2f(this.texCoords[this.faces[var1].m_texCoordsIndex[1]].x, this.texCoords[this.faces[var1].m_texCoordsIndex[1]].y);
            }

            if (this.indicedNormals) {
               GL11.glNormal3f(this.normals[this.faces[var1].m_normalIndex[1]].x, this.normals[this.faces[var1].m_normalIndex[1]].y, this.normals[this.faces[var1].m_normalIndex[1]].z);
            } else {
               GL11.glNormal3f(this.faces[var1].m_normals[1].x, this.faces[var1].m_normals[1].y, this.faces[var1].m_normals[1].z);
            }

            GL11.glVertex3f(var2[this.faces[var1].m_vertsIndex[1]].x, var2[this.faces[var1].m_vertsIndex[1]].y, var2[this.faces[var1].m_vertsIndex[1]].z);
            if (this.texCoords != null && this.faces[var1].m_texCoordsIndex != null) {
               GL11.glTexCoord2f(this.texCoords[this.faces[var1].m_texCoordsIndex[2]].x, this.texCoords[this.faces[var1].m_texCoordsIndex[2]].y);
            }

            if (this.indicedNormals) {
               GL11.glNormal3f(this.normals[this.faces[var1].m_normalIndex[2]].x, this.normals[this.faces[var1].m_normalIndex[2]].y, this.normals[this.faces[var1].m_normalIndex[2]].z);
            } else {
               GL11.glNormal3f(this.faces[var1].m_normals[2].x, this.faces[var1].m_normals[2].y, this.faces[var1].m_normals[2].z);
            }

            GL11.glVertex3f(var2[this.faces[var1].m_vertsIndex[2]].x, var2[this.faces[var1].m_vertsIndex[2]].y, var2[this.faces[var1].m_vertsIndex[2]].z);
         }

         GL11.glEnd();
      } else {
         throw new IllegalArgumentException("Mesh " + this.getName() + " has no faces");
      }
   }

   public Document toOrgeXML() throws ParserConfigurationException {
      Document var1;
      Element var2 = (var1 = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument()).createElement("mesh");
      var1.appendChild(var2);
      Element var3 = var1.createElement("submeshes");
      var2.appendChild(var3);
      Element var4;
      (var4 = var1.createElement("submesh")).setAttribute("material", this.material.getName());
      var4.setAttribute("usesharedvertices", "false");
      var4.setAttribute("use32bitindexes", "false");
      var4.setAttribute("operationtype", "triangle_list");
      var3.appendChild(var4);
      (var3 = var1.createElement("faces")).setAttribute("count", String.valueOf(this.faceCount));
      var4.appendChild(var3);

      Element var6;
      for(int var5 = 0; var5 < this.faceCount; ++var5) {
         (var6 = var1.createElement("face")).setAttribute("v1", String.valueOf(this.faces[var5].m_vertsIndex[0]));
         var6.setAttribute("v2", String.valueOf(this.faces[var5].m_vertsIndex[1]));
         var6.setAttribute("v3", String.valueOf(this.faces[var5].m_vertsIndex[2]));
         var3.appendChild(var6);
      }

      Element var11;
      (var11 = var1.createElement("geometry")).setAttribute("vertexcount", String.valueOf(this.vertCount));
      var4.appendChild(var11);
      (var6 = var1.createElement("vertexbuffer")).setAttribute("positions", "true");
      var6.setAttribute("normals", "true");
      var6.setAttribute("texture_coord_dimensions_0", "2");
      var6.setAttribute("texture_coords", "1");

      for(int var10 = 0; var10 < this.vertCount; ++var10) {
         var4 = var1.createElement("vertex");
         Element var7 = var1.createElement("position");
         Element var8 = var1.createElement("normal");
         Element var9 = var1.createElement("texcoord");
         var7.setAttribute("x", String.valueOf(this.vertices[var10].x));
         var7.setAttribute("y", String.valueOf(this.vertices[var10].y));
         var7.setAttribute("z", String.valueOf(this.vertices[var10].z));
         var8.setAttribute("x", String.valueOf(this.normals[var10].x));
         var8.setAttribute("y", String.valueOf(this.normals[var10].y));
         var8.setAttribute("z", String.valueOf(this.normals[var10].z));
         var9.setAttribute("u", String.valueOf(this.texCoords[var10].x));
         var9.setAttribute("clazz", String.valueOf(this.texCoords[var10].y));
         var4.appendChild(var7);
         var4.appendChild(var8);
         var4.appendChild(var9);
         var6.appendChild(var4);
      }

      var11.appendChild(var6);
      var3 = var1.createElement("submeshnames");
      var2.appendChild(var3);
      (var4 = var1.createElement("submeshname")).setAttribute("description", "submesh0");
      var4.setAttribute("index", "0");
      var3.appendChild(var4);
      return var1;
   }

   public void unbindBuffers() {
      GlUtil.glBindBuffer(34963, 0);
      GlUtil.glBindBuffer(34962, 0);
   }

   public void unloadMeshPointers() {
      this.unbindBuffers();
      GL20.glDisableVertexAttribArray(0);
      GL20.glDisableVertexAttribArray(1);
      GL20.glDisableVertexAttribArray(2);
      this.meshPointersLoaded = false;
   }

   public void unloadVBO(boolean var1) {
      GlUtil.glBindBuffer(34962, 0);
      if (var1) {
         GlUtil.glDisableClientState(32884);
         GlUtil.glDisableClientState(32888);
         GlUtil.glDisableClientState(32885);
         GlUtil.glDisableClientState(32886);
      }

      if (this.skin != null) {
         this.skin.unloadVBO();
      }

      GlUtil.glBindBuffer(34963, 0);
      this.vboLoaded = false;
   }

   public void updateBound() {
      float var1 = -2.14748365E9F;
      float var2 = 2.14748365E9F;
      float var3 = -2.14748365E9F;
      float var4 = 2.14748365E9F;
      float var5 = -2.14748365E9F;
      float var6 = 2.14748365E9F;
      if (this.vertices != null) {
         for(int var7 = 0; var7 < this.vertices.length; ++var7) {
            var1 = this.vertices[var7].x > var1 ? this.vertices[var7].x : var1;
            var2 = this.vertices[var7].x < var2 ? this.vertices[var7].x : var2;
            var3 = this.vertices[var7].y > var3 ? this.vertices[var7].y : var3;
            var4 = this.vertices[var7].y < var4 ? this.vertices[var7].y : var4;
            var5 = this.vertices[var7].z > var5 ? this.vertices[var7].z : var5;
            var6 = this.vertices[var7].z < var6 ? this.vertices[var7].z : var6;
         }
      }

      if (this.verticesBuffer != null) {
         while(this.verticesBuffer.hasRemaining()) {
            float var10 = this.verticesBuffer.get();
            float var8 = this.verticesBuffer.get();
            float var9 = this.verticesBuffer.get();
            var1 = var10 > var1 ? var10 : var1;
            var2 = var10 < var2 ? var10 : var2;
            var3 = var8 > var3 ? var8 : var3;
            var4 = var8 < var4 ? var8 : var4;
            var5 = var9 > var5 ? var9 : var5;
            var6 = var9 < var6 ? var9 : var6;
         }

         this.verticesBuffer.rewind();
      }

      this.setBoundingBox(new BoundingBox(new Vector3f(var2, var4, var6), new Vector3f(var1, var3, var5)));
   }

   public boolean isVboLoaded() {
      return this.vboLoaded;
   }

   public Transform getInitialTransformWithoutScale(Transform var1) {
      var1.setIdentity();
      var1.basis.set(this.getInitialQuadRot());
      var1.origin.set(this.getInitionPos());
      return var1;
   }

   public static class Face {
      public int[] m_vertsIndex = new int[3];
      public int[] m_normalIndex = new int[3];
      public Vector3f[] m_normals;
      public int[] m_texCoordsIndex = new int[3];

      public Vector3f getCentroid(Mesh var1) {
         Vector3f var2 = var1.vertices[this.m_vertsIndex[0]];
         Vector3f var3 = var1.vertices[this.m_vertsIndex[1]];
         Vector3f var4 = var1.vertices[this.m_vertsIndex[2]];
         return new Vector3f(0.33333334F * (var2.x + var3.x + var4.x), 0.33333334F * (var2.y + var3.y + var4.y), 0.33333334F * (var2.z + var3.z + var4.z));
      }

      public String getVertString(Mesh var1) {
         return "[Face] " + var1.vertices[this.m_vertsIndex[0]] + ", " + var1.vertices[this.m_vertsIndex[1]] + ", " + var1.vertices[this.m_vertsIndex[2]];
      }

      public void setNormalIndex(int var1, int var2, int var3) {
         this.m_normalIndex[0] = var1;
         this.m_normalIndex[1] = var2;
         this.m_normalIndex[2] = var3;
      }

      public void setTexCoordIndex(int var1, int var2, int var3) {
         this.m_texCoordsIndex[0] = var1;
         this.m_texCoordsIndex[1] = var2;
         this.m_texCoordsIndex[2] = var3;
      }

      public void setVertIndex(int var1, int var2, int var3) {
         this.m_vertsIndex[0] = var1;
         this.m_vertsIndex[1] = var2;
         this.m_vertsIndex[2] = var3;
      }
   }
}
