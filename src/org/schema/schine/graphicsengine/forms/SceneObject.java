package org.schema.schine.graphicsengine.forms;

import java.util.Iterator;
import java.util.Vector;

public class SceneObject {
   private Vector childs = new Vector();
   private Vector forms = new Vector();

   public void draw() {
      Iterator var1 = this.forms.iterator();

      while(var1.hasNext()) {
         AbstractSceneNode var2;
         (var2 = (AbstractSceneNode)var1.next()).getMaterial().attach(0);
         var2.draw();
      }

      var1 = this.childs.iterator();

      while(var1.hasNext()) {
         ((SceneObject)var1.next()).draw();
      }

   }
}
