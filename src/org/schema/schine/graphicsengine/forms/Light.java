package org.schema.schine.graphicsengine.forms;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.nio.FloatBuffer;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.GlUtil;

public class Light extends SceneNode implements KeyListener {
   private static int lightNumGen;
   private static float[] temp = new float[4];
   public static final FloatBuffer tempBuffer = BufferUtils.createFloatBuffer(4);
   private static Vector3f spotDirDefault;
   private static boolean defaultValues;
   private static float spotCutOffDefault;
   private static float spotExponentDefault;
   private static float constAttenuationDefault;
   private static float linearAttenuationDefault;
   private static float quadAttenuationDefault;
   public Vector3f spotDirection = new Vector3f();
   public float constAttenuation;
   public float linearAttenuation;
   public float quadAttenuation;
   public Vector3f spotUp = new Vector3f();
   float[] lightDir = new float[]{0.0F, 1.0F, 0.0F, 0.0F};
   private int lightNum;
   private Vector4f ambience;
   private Vector4f diffuse;
   private Vector4f specular;
   private float spotCutoff = 45.0F;
   private float spotExponent = 4.0F;
   private float[] shininess;
   private boolean init;

   public Light() {
      this.reassign();
      this.init();
   }

   private void init() {
      this.setAmbience(new Vector4f(0.2F, 0.2F, 0.2F, 1.0F));
      this.setDiffuse(new Vector4f(0.6F, 0.6F, 0.6F, 1.0F));
      this.setSpecular(new Vector4f(0.9F, 0.9F, 0.9F, 1.0F));
      this.getPos().set(0.0F, 0.0F, 0.0F);
      this.setShininess(new float[]{32.0F});
   }

   public Light(int var1) {
      this.lightNum = var1;
      this.init();
   }

   public void setExtraValuesToDefault() {
      this.spotDirection.set(spotDirDefault);
      this.spotCutoff = spotCutOffDefault;
      this.spotExponent = spotExponentDefault;
      this.constAttenuation = constAttenuationDefault;
      this.linearAttenuation = linearAttenuationDefault;
      this.quadAttenuation = quadAttenuationDefault;
   }

   public void reassign() {
      if (lightNumGen > 7) {
         throw new IllegalArgumentException("too many Lights in scene");
      } else {
         this.lightNum = lightNumGen++;
      }
   }

   public void cleanUp() {
      GlUtil.glDisable(this.getLightID());
      --lightNumGen;
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.attach();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      if (!defaultValues) {
         GL11.glGetLight(16384, 4612, tempBuffer);
         tempBuffer.rewind();
         spotDirDefault = new Vector3f(tempBuffer.get(0), tempBuffer.get(1), tempBuffer.get(2));
         tempBuffer.rewind();
         GL11.glGetLight(16384, 4614, tempBuffer);
         spotCutOffDefault = tempBuffer.get(0);
         tempBuffer.rewind();
         GL11.glGetLight(16384, 4613, tempBuffer);
         spotExponentDefault = tempBuffer.get(0);
         tempBuffer.rewind();
         GL11.glGetLight(16384, 4615, tempBuffer);
         constAttenuationDefault = tempBuffer.get(0);
         tempBuffer.rewind();
         GL11.glGetLight(16384, 4616, tempBuffer);
         linearAttenuationDefault = tempBuffer.get(0);
         tempBuffer.rewind();
         GL11.glGetLight(16384, 4617, tempBuffer);
         quadAttenuationDefault = tempBuffer.get(0);
         defaultValues = true;
      }

      this.setExtraValuesToDefault();
      this.init = true;
   }

   public void attachSimple() {
      this.getPos().get(temp);
      temp[3] = 1.0F;
      tempBuffer.rewind();
      tempBuffer.put(temp);
      tempBuffer.rewind();
      GL11.glLight(this.getLightID(), 4611, tempBuffer);
      this.getAmbience().get(temp);
      tempBuffer.rewind();
      tempBuffer.put(temp);
      tempBuffer.rewind();
      GL11.glLight(this.getLightID(), 4608, tempBuffer);
      this.getDiffuse().get(temp);
      tempBuffer.rewind();
      tempBuffer.put(temp);
      tempBuffer.rewind();
      GL11.glLight(this.getLightID(), 4609, tempBuffer);
      this.getSpecular().get(temp);
      tempBuffer.rewind();
      tempBuffer.put(temp);
      tempBuffer.rewind();
      GL11.glLight(this.getLightID(), 4610, tempBuffer);
      GlUtil.glEnable(2896);
      GlUtil.glEnable(this.getLightID());
   }

   private void attach() {
      this.getAmbience().get(temp);
      tempBuffer.rewind();
      tempBuffer.put(temp);
      tempBuffer.rewind();
      GL11.glLight(this.getLightID(), 4608, tempBuffer);
      this.getDiffuse().get(temp);
      tempBuffer.rewind();
      tempBuffer.put(temp);
      tempBuffer.rewind();
      GL11.glLight(this.getLightID(), 4609, tempBuffer);
      this.getPos().get(temp);
      temp[3] = 1.0F;
      tempBuffer.rewind();
      tempBuffer.put(temp);
      tempBuffer.rewind();
      GL11.glLight(this.getLightID(), 4611, tempBuffer);
      this.getSpecular().get(temp);
      tempBuffer.rewind();
      tempBuffer.put(temp);
      tempBuffer.rewind();
      GL11.glLight(this.getLightID(), 4610, tempBuffer);
      this.spotDirection.get(temp);
      temp[3] = 1.0F;
      tempBuffer.rewind();
      tempBuffer.put(temp);
      tempBuffer.rewind();
      GL11.glLight(this.getLightID(), 4612, tempBuffer);
      GL11.glLightf(this.getLightID(), 4614, this.spotCutoff);
      GL11.glLightf(this.getLightID(), 4613, this.spotExponent);
      GL11.glLightf(this.getLightID(), 4615, this.constAttenuation);
      GL11.glLightf(this.getLightID(), 4616, this.linearAttenuation);
      GL11.glLightf(this.getLightID(), 4617, this.quadAttenuation);
      GlUtil.glEnable(2896);
      GlUtil.glEnable(this.getLightID());
   }

   public Vector4f getAmbience() {
      return this.ambience;
   }

   public void setAmbience(Vector4f var1) {
      this.ambience = var1;
   }

   public Vector4f getDiffuse() {
      return this.diffuse;
   }

   public void setDiffuse(Vector4f var1) {
      this.diffuse = var1;
   }

   private int getLightID() {
      switch(this.lightNum) {
      case 0:
         return 16384;
      case 1:
         return 16385;
      case 2:
         return 16386;
      case 3:
         return 16387;
      case 4:
         return 16388;
      case 5:
         return 16389;
      case 6:
         return 16390;
      case 7:
         return 16391;
      default:
         return 0;
      }
   }

   public float[] getShininess() {
      return this.shininess;
   }

   public void setShininess(float[] var1) {
      this.shininess = var1;
   }

   public Vector4f getSpecular() {
      return this.specular;
   }

   public void setSpecular(Vector4f var1) {
      this.specular = var1;
   }

   public void keyTyped(KeyEvent var1) {
   }

   public void keyPressed(KeyEvent var1) {
      float var2 = 100.0F;
      if (var1.isShiftDown()) {
         var2 = 1000.0F;
      }

      if (var1.isControlDown()) {
         var2 = 0.5F;
      }

      Vector3f var10000;
      switch(var1.getKeyCode()) {
      case 33:
         var10000 = this.getPos();
         var10000.y += var2;
         return;
      case 34:
         var10000 = this.getPos();
         var10000.y -= var2;
      case 35:
      case 36:
      default:
         return;
      case 37:
         var10000 = this.getPos();
         var10000.x += var2;
         return;
      case 38:
         var10000 = this.getPos();
         var10000.z += var2;
         return;
      case 39:
         var10000 = this.getPos();
         var10000.x -= var2;
         return;
      case 40:
         var10000 = this.getPos();
         var10000.z -= var2;
      }
   }

   public void keyReleased(KeyEvent var1) {
   }

   public void deactivate() {
      GlUtil.glDisable(this.getLightID());
   }

   public static void resetLightAssignment() {
      lightNumGen = 0;
   }
}
