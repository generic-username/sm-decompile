package org.schema.schine.graphicsengine.forms;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.vecmath.Vector3f;
import org.schema.common.util.data.DataUtil;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.shader.HoffmannSky;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.texture.Texture;
import org.schema.schine.graphicsengine.texture.TextureLoader;
import org.schema.schine.resource.ResourceLoader;

public class OldTerrain extends Mesh {
   public static final int FIELD_SIZE = 100;
   public static final int MAX_TEXTURE_SIZE = 512;
   public static final float terrainSubZero = 240.0F;
   public static final float pixelOccupiedThreshold = 20.0F;
   public static final float TERRAIN_CLIFF_SIZE = 50.0F;
   public static final int MAX_TERRAIN_DIMENSION = 128;
   private static final int TERRAIN_REGION_DIRT = 0;
   private static final int TERRAIN_REGION_GRASS = 1;
   private static final int TERRAIN_REGION_ROCK = 2;
   private static final int TERRAIN_REGION_SNOW = 3;
   private static final int rimY = 0;
   private static final int rimX = 0;
   public static int drawnTerrains;
   public static float widthScale = 1.0F;
   public static int stepsize = 256;
   public static int pixelOccupiedDistance = 5;
   static int scalefactor = 1;
   private static OldTerrain.TerrainRegion[] regions;
   private static int m_width = 1024;
   private static int m_height = 1024;
   private static int m_halfwidth;
   private static int m_widthm1;
   private static int m_heightm1;
   protected int heightMapPointer;
   private float[][] heightFields;
   private float scale;
   private float tiling_factor;
   private BufferedImage heightMap;
   private TerrainMeshOptimizer optimizer;
   private Vector3f[] origVerts;
   private int normalMapPointer;
   private int specularMapPointer;
   private OldTerrain.TerrainShaderable terrainShaderable;
   private boolean asVertexBufferObject;

   public OldTerrain() {
      this.scale = (float)scalefactor;
      this.tiling_factor = 3.0F;
      this.setTerrainShaderable(new OldTerrain.TerrainShaderable());
   }

   protected static int at(int var0, int var1) {
      while(var0 < 0) {
         var0 += m_width;
      }

      if ((var1 &= (m_height << 1) - 1) > m_heightm1) {
         var1 = (m_heightm1 << 1) - var1;
         var0 += m_halfwidth;
      }

      if (var1 < 0) {
         var1 = -var1;
         var0 += m_width >> 1;
      }

      var0 &= m_widthm1;
      return var1 * m_width + var0;
   }

   public static OldTerrain createTerrain(int var0, int var1, float[][] var2, int var3, int var4, int var5, int var6, int var7) {
      OldTerrain var8;
      (var8 = new OldTerrain()).scale = (float)scalefactor;
      System.out.println("[OldTerrain] creating from clientState " + var0 + ", " + var1);
      initializeMesh(var8, var0, var1);
      var8.heightFields = var2;
      int var9 = 0;
      float var10 = -2.14748365E9F;
      float var11 = 2.14748365E9F;
      float var12 = -2.14748365E9F;
      float var13 = 2.14748365E9F;
      float var14 = -2.14748365E9F;
      float var15 = 2.14748365E9F;

      for(int var16 = 0; var16 < var1; ++var16) {
         for(int var17 = 0; var17 < var0; ++var17) {
            float var18 = 0.0F;

            try {
               var18 = var2[var16][var17];
            } catch (IndexOutOfBoundsException var19) {
            }

            var18 -= 240.0F;
            var8.vertices[var9].x = (float)(var17 * var3) * widthScale;
            var8.vertices[var9].y = var18 * (float)var3;
            var8.vertices[var9].z = (float)(var16 * var3) * widthScale;
            var10 = var8.vertices[var9].x > var10 ? var8.vertices[var9].x : var10;
            var11 = var8.vertices[var9].x < var11 ? var8.vertices[var9].x : var11;
            var12 = var8.vertices[var9].y > var12 ? var8.vertices[var9].y : var12;
            var13 = var8.vertices[var9].y < var13 ? var8.vertices[var9].y : var13;
            var14 = var8.vertices[var9].z > var14 ? var8.vertices[var9].z : var14;
            var15 = var8.vertices[var9].z < var15 ? var8.vertices[var9].z : var15;
            ++var9;
         }
      }

      var8.setBoundingBox(new BoundingBox(new Vector3f(var11, var13, var15), new Vector3f(var10, var12, var14)));
      var8.setOrigVerts(Vector3fTools.clone(var8.vertices));
      var8.optimizer = new TerrainMeshOptimizer(var8, var0, var1);
      var8.optimizer.optimize(var4, var5, var6, var7);
      var8.setIndicedNormals(true);
      var8.setOrigVerts((Vector3f[])null);
      var8.setHeightFields((float[][])null);
      makeNormals(var8);
      var8.optimizer = null;
      var8.getMaterial().setAmbient(new float[]{0.6F, 0.6F, 0.6F, 1.0F});
      var8.getMaterial().setDiffuse(new float[]{0.8F, 0.8F, 0.8F, 1.0F});
      var8.getMaterial().setSpecular(new float[]{0.9F, 0.9F, 0.9F, 1.0F});
      return var8;
   }

   public static MeshGroup createTerrainWorld(float[][] var0, boolean var1) throws Exception {
      MeshGroup var2 = new MeshGroup();
      int var3 = var0[0].length;
      int var4 = var0.length;
      int var5 = var3 / 128;
      int var6 = var4 / 128;

      for(int var7 = 0; var7 < var6; ++var7) {
         for(int var8 = 0; var8 < var5; ++var8) {
            float[][] var9 = new float[129][129];
            int var10 = var8 << 7;
            int var11 = var7 << 7;
            var10 = Math.max(0, var10 - 1);
            var11 = Math.max(0, var11 - 1);

            for(int var12 = 0; var12 <= 128; ++var12) {
               for(int var13 = 0; var13 <= 128; ++var13) {
                  var9[var12][var13] = var0[var11 + var12][var10 + var13];
               }
            }

            OldTerrain var14;
            (var14 = createTerrain(129, 129, var9, 100, var8, var7, var3, var4)).makeTerrainRegions("/maps/files/standartMapRegion.png");
            if (var1) {
               Mesh.buildVBOs(var14);
            }

            Vector3f var15;
            (var15 = new Vector3f()).x = (float)(var10 * 100 - var8 * 100);
            var15.z = (float)(var11 * 100 - var7 * 100);
            var14.setPos(var15);
            var2.attach(var14);
         }
      }

      return var2;
   }

   public static MeshGroup createTerrainWorld(final int[] var0, final int var1, final int var2, final int var3, final int var4, final int var5, final boolean var6) throws Exception {
      final MeshGroup var7 = new MeshGroup();
      m_width = var1;
      m_height = var2;
      m_halfwidth = m_width >> 1;
      m_widthm1 = m_width - 1;
      m_heightm1 = m_height - 1;
      final int var8 = var1 / 128;
      final int var9 = var2 / 128;
      final float[][] var10 = new float[128][128];
      (new Thread(new Runnable() {
         public final void run() {
            for(int var1x = 0; var1x < var9; ++var1x) {
               for(int var2x = 0; var2x < var8; ++var2x) {
                  int var3x = var1x;
                  int var4x = var2x;
                  int var5x = var2x << 7;
                  int var6x = var1x << 7;

                  for(int var7x = 0; var7x < 128; ++var7x) {
                     for(int var8x = 0; var8x < 128; ++var8x) {
                        var10[var7x][var8x] = (float)var0[OldTerrain.at((var4x << 7) + var8x, (var3x << 7) + var7x)];
                     }
                  }

                  OldTerrain var9x;
                  (var9x = OldTerrain.createTerrain(128, 128, var10, 100, var4x, var3x, var1, var2)).heightMapPointer = var3;
                  var9x.asVertexBufferObject = var6;
                  var9x.normalMapPointer = var4;
                  var9x.specularMapPointer = var5;
                  Vector3f var10x;
                  (var10x = new Vector3f()).x = (float)(var5x * 100 - var4x * 100);
                  var10x.z = (float)(var6x * 100 - var3x * 100);
                  var9x.setPos(var10x);
                  var7.attach(var9x);
               }
            }

            var7.setLoaded(true);
         }
      })).start();
      return var7;
   }

   public static MeshGroup createTerrainWorldFromImage(BufferedImage var0, boolean var1) throws Exception {
      float[][] var2 = new float[var0.getHeight()][var0.getWidth()];

      for(int var3 = 0; var3 < var2.length; ++var3) {
         while(0 < var2[var3].length) {
            var2[var3][0] = getPixelValue(var0, 0, var3);
            ++var3;
         }
      }

      return createTerrainWorld(var2, var1);
   }

   public static void createThreaded(int[] var0, int var1, int var2, int var3, int var4, int var5, boolean var6) {
   }

   public static float getPixelValue(BufferedImage var0, int var1, int var2) {
      if (var1 >= var0.getWidth() || var2 >= var0.getHeight() || var1 < 0 || var2 < 0) {
         var1 = Math.min(Math.max(0, var1), 511);
         var2 = Math.min(Math.max(0, var2), 511);
      }

      int var3 = 0;

      try {
         var3 = var0.getRGB((int)((float)var1 / widthScale), (int)((float)var2 / widthScale));
      } catch (ArrayIndexOutOfBoundsException var4) {
         System.err.println("out of bounds " + (int)((float)var1 / widthScale) + "," + (int)((float)var2 / widthScale) + "  maxSize = " + var0.getWidth() + "x" + var0.getHeight());
      }

      int var5 = var3 >> 16 & 255;
      var1 = var3 >> 8 & 255;
      var2 = var3 & 255;
      return (float)(var5 + var1 + var2);
   }

   private static void initializeMesh(OldTerrain var0, int var1, int var2) {
      var0.vertCount = var1 * var2;
      var0.faceCount = var0.vertCount << 1;
      var0.vertices = new Vector3f[var0.getVertCount()];
      var0.faces = new Mesh.Face[var0.getFaceCount()];
      var0.normals = new Vector3f[var0.getVertCount()];
      var0.texCoords = new Vector3f[var0.getVertCount()];

      for(var1 = 0; var1 < var0.vertCount; ++var1) {
         var0.vertices[var1] = new Vector3f();
         var0.texCoords[var1] = new Vector3f();
         var0.normals[var1] = new Vector3f(0.0F, 1.0F, 0.0F);
      }

      System.err.println("heightField faces: " + var0.faceCount);

      for(var1 = 0; var1 < var0.faceCount; ++var1) {
         var0.faces[var1] = new Mesh.Face();
      }

   }

   public static MeshGroup loadTerrainFromXML(String var0, String var1, int var2, int var3) throws ResourceException {
      (new StringBuilder()).append(var1).append(File.separator).toString();
      var0 = var0.replaceAll(".mesh", "").replaceAll(".xml", "");
      System.out.println("[TERRAIN] loading " + var0);
      MeshGroup var8 = new MeshGroup();
      int var4 = 0;

      for(int var5 = 0; var5 < var2; ++var5) {
         for(int var6 = 0; var6 < var3; ++var6) {
            (new StringBuilder()).append(var0).append(var4).append(".mesh").toString();
            null.getMaterial().setName(var0);
            null.setIndicedNormals(true);
            ((OldTerrain)null).scale = (float)scalefactor;
            null.setPos((float)((var5 << 7) * 100 - var5 * 100), 0.0F, (float)((var6 << 7) * 100 - var6 * 100));
            var8.attach((AbstractSceneNode)null);
            ++var4;
         }
      }

      var4 = 0;
      Iterator var9 = var8.getChilds().iterator();

      while(var9.hasNext()) {
         AbstractSceneNode var10;
         if ((var10 = (AbstractSceneNode)var9.next()) instanceof OldTerrain) {
            OldTerrain var7;
            (var7 = (OldTerrain)var10).makeTerrainRegions("./maps/files/" + var7.getMaterial().getName() + var4 + ".png");
            ++var4;
         }
      }

      return var8;
   }

   private static void makeNormals(OldTerrain var0) {
      int var1;
      for(var1 = 0; var1 < var0.faceCount; ++var1) {
         Vector3f var2 = var0.vertices[var0.faces[var1].m_vertsIndex[0]];
         Vector3f var3 = var0.vertices[var0.faces[var1].m_vertsIndex[1]];
         Vector3f var4 = var0.vertices[var0.faces[var1].m_vertsIndex[2]];
         var3 = Vector3fTools.sub(var3, var2);
         (var2 = Vector3fTools.sub(var4, var2)).scale(-1.0F);
         var2 = Vector3fTools.crossProduct(var3, var2);
         var0.normals[var0.faces[var1].m_vertsIndex[0]].add(var2);
         var0.normals[var0.faces[var1].m_vertsIndex[1]].add(var2);
         var0.normals[var0.faces[var1].m_vertsIndex[2]].add(var2);
      }

      for(var1 = 0; var1 < var0.vertCount; ++var1) {
         var0.normals[var1].normalize();
      }

   }

   private static void triangulate(OldTerrain var0, int var1, int var2) {
      int var3 = 0;

      int var4;
      int var5;
      for(var4 = 0; var4 < var2 - 1; ++var4) {
         for(var5 = 0; var5 < var1 - 1; ++var5) {
            var0.faces[var3].m_vertsIndex[2] = var4 * var1 + var5;
            var0.faces[var3].m_vertsIndex[1] = var4 * var1 + var5 + 1;
            var0.faces[var3].m_vertsIndex[0] = var4 * var1 + var5 + var1;
            var0.faces[var3].m_normalIndex[2] = var4 * var1 + var5;
            var0.faces[var3].m_normalIndex[1] = var4 * var1 + var5 + 1;
            var0.faces[var3].m_normalIndex[0] = var4 * var1 + var5 + var1;
            var0.faces[var3].m_texCoordsIndex[2] = var4 * var1 + var5;
            var0.faces[var3].m_texCoordsIndex[1] = var4 * var1 + var5 + 1;
            var0.faces[var3].m_texCoordsIndex[0] = var4 * var1 + var5 + var1;
            ++var3;
         }
      }

      for(var4 = var2 - 1; var4 > 0; --var4) {
         for(var5 = var1 - 1; var5 > 0; --var5) {
            var0.faces[var3].m_vertsIndex[2] = var4 * var1 + var5;
            var0.faces[var3].m_vertsIndex[1] = var4 * var1 + var5 - 1;
            var0.faces[var3].m_vertsIndex[0] = var4 * var1 + var5 - var1;
            var0.faces[var3].m_normalIndex[2] = var4 * var1 + var5;
            var0.faces[var3].m_normalIndex[1] = var4 * var1 + var5 - 1;
            var0.faces[var3].m_normalIndex[0] = var4 * var1 + var5 - var1;
            var0.faces[var3].m_texCoordsIndex[2] = var4 * var1 + var5;
            var0.faces[var3].m_texCoordsIndex[1] = var4 * var1 + var5 - 1;
            var0.faces[var3].m_texCoordsIndex[0] = var4 * var1 + var5 - var1;
            ++var3;
         }
      }

   }

   public void draw() {
      if (!this.isLoaded() && this.asVertexBufferObject) {
         try {
            Mesh.buildVBOs(this);
         } catch (Exception var1) {
            var1.printStackTrace();
         }
      } else {
         if (this.isFirstDraw()) {
            this.onInit();
            this.setFirstDraw(false);
            this.vertices = null;
            this.texCoords = null;
            this.normals = null;
            this.faces = null;
         }

         if (this.isVisibleBoundingBox()) {
            super.draw();
            ++drawnTerrains;
         }

      }
   }

   public void onInit() {
      super.onInit();
      this.makeRegions();
      this.material.setTexture(new Texture(3553, this.heightMapPointer, "terrain"));
   }

   public void drawOnTheFly(BufferedImage var1) {
      ShaderLibrary.terrainShader.setShaderInterface(this.getTerrainShaderable());
      ShaderLibrary.terrainShader.load();
      int var2 = var1.getWidth();
      int var3;
      int var4 = (var3 = var1.getHeight()) / stepsize + 1;
      int var5 = var2 / stepsize + 1;
      this.scale = (float)scalefactor;
      int var6 = 0;
      int var7 = stepsize;

      for(int var8 = 0; var8 < var4; ++var8) {
         for(int var9 = 0; var9 < var5; ++var9) {
            float var10 = getPixelValue(var1, var9 * var7, var8 * var7) - 240.0F;
            this.vertices[var6].x = (float)(var9 * var7) * widthScale;
            this.vertices[var6].y = var10;
            this.vertices[var6].z = (float)(var8 * var7) * widthScale;
            this.texCoords[var6].x = (float)var9 * (float)var7 / (float)var2 / 2.0F;
            this.texCoords[var6].y = (float)var8 * (float)var7 / (float)var3 / 2.0F;
            ++var6;
         }
      }

      this.setIndicedNormals(true);
      triangulate(this, var5, var4);
      makeNormals(this);
      super.draw();
      ShaderLibrary.terrainShader.unload();
   }

   public float getHeight(float var1, float var2) {
      try {
         if (this.heightMap != null) {
            return this.getPixelValue((int)(var1 / 100.0F), (int)(var2 / 100.0F)) / 1.534F - 240.0F;
         } else {
            throw new NullPointerException("not implemented");
         }
      } catch (IndexOutOfBoundsException var3) {
         return 0.0F;
      }
   }

   public float[][] getHeightFields() {
      return this.heightFields;
   }

   public void setHeightFields(float[][] var1) {
      this.heightFields = var1;
   }

   public BufferedImage getHeightMap() {
      return this.heightMap;
   }

   public void setHeightMap(BufferedImage var1) {
      this.heightMap = var1;
   }

   public Vector3f[] getOrigVerts() {
      return this.origVerts;
   }

   public void setOrigVerts(Vector3f[] var1) {
      this.origVerts = var1;
   }

   public float getPixelValue(int var1, int var2) {
      if (var1 >= this.heightMap.getWidth() || var2 >= this.heightMap.getHeight() || var1 < 0 || var2 < 0) {
         var1 = Math.min(Math.max(0, var1), 511);
         var2 = Math.min(Math.max(0, var2), 511);
      }

      var2 = (var1 = this.heightMap.getRGB((int)((float)var1 / widthScale), (int)((float)var2 / widthScale))) >> 16 & 255;
      int var3 = var1 >> 8 & 255;
      var1 &= 255;
      return (float)(var2 + var3 + var1);
   }

   public OldTerrain.TerrainShaderable getTerrainShaderable() {
      return this.terrainShaderable;
   }

   public void setTerrainShaderable(OldTerrain.TerrainShaderable var1) {
      this.terrainShaderable = var1;
   }

   public boolean isVisibleBoundingBox() {
      return true;
   }

   private void makeRegions() {
      if (regions == null) {
         regions = new OldTerrain.TerrainRegion[4];

         try {
            regions[0] = new OldTerrain.TerrainRegion(Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "/heightmaps/dirt.png", true), 0.0F, 80.0F);
            regions[1] = new OldTerrain.TerrainRegion(Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "/heightmaps/grass.png", true), 80.0F, 130.0F);
            regions[2] = new OldTerrain.TerrainRegion(Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "/heightmaps/rock.png", true), 130.0F, 180.0F);
            regions[3] = new OldTerrain.TerrainRegion(Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "/heightmaps/snow.png", true), 180.0F, 255.0F);
            return;
         } catch (IOException var1) {
            var1.printStackTrace();
         }
      }

   }

   public void makeTerrainRegions(BufferedImage var1) {
      Controller.getTexLoader();
      this.material.setTexture(TextureLoader.getTexture(var1, "terainRegion", 3553, 6408, 9729, 9729, true, true));
      this.heightMap = var1;
      this.makeRegions();

      try {
         this.normalMapPointer = Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "heightmaps/ground-normalMap.png", true).getTextureId();
         this.specularMapPointer = Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "heightmaps/ground-specular.png", true).getTextureId();
      } catch (IOException var2) {
         var2.printStackTrace();
      }
   }

   public void makeTerrainRegions(String var1) {
      try {
         this.heightMap = ImageIO.read(ResourceLoader.resourceUtil.getResourceURL(DataUtil.dataPath + var1));
      } catch (IOException var2) {
         var2.printStackTrace();
      } catch (ResourceException var3) {
         var3.printStackTrace();
      }

      this.makeTerrainRegions(this.heightMap);
   }

   public void makeTerrainRegionsFlat() {
      this.makeRegions();
   }

   public void reloadShader() {
      ShaderLibrary.loadShaders();
   }

   public void updateHeightMapMesh() {
      int var1 = this.heightFields.length;
      int var2 = this.heightFields[0].length;
      int var3 = 0;

      for(int var4 = 0; var4 < var1; ++var4) {
         for(int var5 = 0; var5 < var2; ++var5) {
            this.getOrigVerts()[var3].x = (float)(var5 * 100) * widthScale;
            this.getOrigVerts()[var3].y = this.heightFields[var4][var5];
            this.getOrigVerts()[var3].z = (float)(var4 * 100) * widthScale;
            ++var3;
         }
      }

      this.setIndicedNormals(true);
      if (this.optimizer == null) {
         this.optimizer = new TerrainMeshOptimizer(this, var2, var1);
      }

      this.optimizer.optimize(0, 0, var2, var1);
      makeNormals(this);
   }

   public void updateTexture(BufferedImage var1) {
      this.material.getTexture().updateTexture(var1);
   }

   public class TerrainShaderable extends HoffmannSky {
      public void onExit() {
         if (!OldTerrain.this.isFirstDraw()) {
            OldTerrain.this.getMaterial().getTexture().unbindFromIndex();
            OldTerrain.regions[0].getTexture().unbindFromIndex();
            OldTerrain.regions[1].getTexture().unbindFromIndex();
            OldTerrain.regions[2].getTexture().unbindFromIndex();
            OldTerrain.regions[3].getTexture().unbindFromIndex();
            GlUtil.glActiveTexture(33989);
            GlUtil.glDisable(3553);
            GlUtil.glBindTexture(3553, 0);
            GlUtil.glActiveTexture(33990);
            GlUtil.glDisable(3553);
            GlUtil.glBindTexture(3553, 0);
            GlUtil.glActiveTexture(33984);
         }
      }

      public void updateShader(DrawableScene var1) {
      }

      public void updateShaderParameters(Shader var1) {
         if (!OldTerrain.this.isFirstDraw()) {
            super.updateShaderParameters(var1);
            OldTerrain.this.material.attach(0);
            GlUtil.updateShaderFloat(var1, "terrainTilingFactor", OldTerrain.this.tiling_factor);
            GlUtil.updateShaderFloat(var1, "heightMapScale", OldTerrain.this.scale);
            GlUtil.updateShaderFloat(var1, "dirtRegion.max", OldTerrain.regions[0].getMax());
            GlUtil.updateShaderFloat(var1, "dirtRegion.min", OldTerrain.regions[0].getMin());
            GlUtil.updateShaderFloat(var1, "grassRegion.max", OldTerrain.regions[1].getMax());
            GlUtil.updateShaderFloat(var1, "grassRegion.min", OldTerrain.regions[1].getMin());
            GlUtil.updateShaderFloat(var1, "rockRegion.max", OldTerrain.regions[2].getMax());
            GlUtil.updateShaderFloat(var1, "rockRegion.min", OldTerrain.regions[2].getMin());
            GlUtil.updateShaderFloat(var1, "snowRegion.max", OldTerrain.regions[3].getMax());
            GlUtil.updateShaderFloat(var1, "snowRegion.min", OldTerrain.regions[3].getMin());
            GlUtil.updateShaderVector4f(var1, "light.ambient", AbstractScene.mainLight.getAmbience());
            GlUtil.updateShaderVector4f(var1, "light.diffuse", AbstractScene.mainLight.getDiffuse());
            GlUtil.updateShaderVector4f(var1, "light.specular", AbstractScene.mainLight.getSpecular());
            GlUtil.updateShaderFloat(var1, "shininess", AbstractScene.mainLight.getShininess()[0]);
            OldTerrain.regions[0].getTexture().bindOnShader(33984, 0, "dirtColorMap", var1);
            OldTerrain.regions[1].getTexture().bindOnShader(33985, 1, "grassColorMap", var1);
            OldTerrain.regions[2].getTexture().bindOnShader(33986, 2, "rockColorMap", var1);
            OldTerrain.regions[3].getTexture().bindOnShader(33987, 3, "snowColorMap", var1);
            OldTerrain.this.getMaterial().getTexture().bindOnShader(33988, 4, "heightMap", var1);
            GlUtil.glActiveTexture(33989);
            GlUtil.glBindTexture(3553, OldTerrain.this.normalMapPointer);
            GlUtil.updateShaderInt(var1, "normalMap", 5);
            GlUtil.glActiveTexture(33990);
            GlUtil.glBindTexture(3553, OldTerrain.this.specularMapPointer);
            GlUtil.updateShaderInt(var1, "specularMap", 6);
            GlUtil.glActiveTexture(33991);
         }
      }
   }

   class TerrainRegion {
      private Texture texture;
      private float min;
      private float max;

      public TerrainRegion(Texture var2, float var3, float var4) {
         this.texture = var2;
         this.min = var3;
         this.max = var4;
      }

      public float getMax() {
         return this.max;
      }

      public float getMin() {
         return this.min;
      }

      public Texture getTexture() {
         return this.texture;
      }
   }
}
