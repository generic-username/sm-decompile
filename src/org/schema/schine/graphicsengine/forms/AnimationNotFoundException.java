package org.schema.schine.graphicsengine.forms;

public class AnimationNotFoundException extends Exception {
   private static final long serialVersionUID = 1L;

   public AnimationNotFoundException(String var1) {
      super(var1);
   }
}
