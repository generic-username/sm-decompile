package org.schema.schine.graphicsengine.forms;

import javax.vecmath.AxisAngle4f;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class Line extends Geometry {
   public void cleanUp() {
   }

   public void draw() {
      if (EngineSettings.G_DEBUG_LINE_DRAWING_ACTIVATED.isOn()) {
         System.err.println("TODO: no line drawing yet implemented");
      }

   }

   public void onInit() {
   }

   public AbstractSceneNode clone() {
      return null;
   }

   public AxisAngle4f getAxis() {
      return null;
   }
}
