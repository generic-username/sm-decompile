package org.schema.schine.graphicsengine.forms;

import java.util.HashSet;
import java.util.Iterator;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.GlUtil;

public class MeshGroup extends Mesh {
   private MeshGroup splitMesh;

   public static void staticVBOMultidraw(HashSet var0, Mesh var1) {
      if (!var1.isInvisible()) {
         GlUtil.glPushMatrix();
         Iterator var3 = var1.getChilds().iterator();

         while(var3.hasNext()) {
            AbstractSceneNode var2 = (AbstractSceneNode)var3.next();
            Mesh.staticVBOMultidraw(var0, (Mesh)var2);
         }

         GlUtil.glPopMatrix();
      }
   }

   public void cleanUp() {
      if (this.getSplitMesh() != null) {
         this.getSplitMesh().cleanUp();
      }

      super.cleanUp();
   }

   public void draw() {
      if (!this.isLoaded()) {
         AbstractScene.infoList.add("Loading " + this.getName());
      } else if (!this.isInvisible()) {
         GlUtil.glPushMatrix();
         this.transform();
         Iterator var1 = this.getChilds().iterator();

         while(var1.hasNext()) {
            ((AbstractSceneNode)var1.next()).draw();
         }

         GlUtil.glPopMatrix();
      }
   }

   public void onInit() {
      System.err.println("-- OGL: new MeshGroup created");
   }

   public MeshGroup getSplitMesh() {
      return this.splitMesh;
   }

   public void setSplitMesh(MeshGroup var1) {
      this.splitMesh = var1;
   }

   public void transform() {
      if (!this.isCollisionObject()) {
         AbstractSceneNode.transform(this);
      }

   }

   public void drawRaw() {
      if (!this.isInvisible()) {
         GlUtil.glPushMatrix();
         this.transform();
         Iterator var1 = this.getChilds().iterator();

         while(var1.hasNext()) {
            AbstractSceneNode var2;
            if ((var2 = (AbstractSceneNode)var1.next()) instanceof Mesh) {
               GlUtil.glPushMatrix();
               ((Mesh)var2).transform();
               ((Mesh)var2).loadVBO(true);
               ((Mesh)var2).renderVBO();
               ((Mesh)var2).unloadVBO(true);
               GlUtil.glPopMatrix();
            } else {
               var2.draw();
            }
         }

         GlUtil.glPopMatrix();
      }
   }
}
