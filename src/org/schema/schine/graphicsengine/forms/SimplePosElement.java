package org.schema.schine.graphicsengine.forms;

public class SimplePosElement {
   public static final int FRONT = 0;
   public static final int BACK = 1;
   public static final int TOP = 2;
   public static final int BOTTOM = 3;
   public static final int RIGHT = 4;
   public static final int LEFT = 5;
   public static final int FLAG_FRONT = 1;
   public static final int FLAG_BACK = 2;
   public static final int FLAG_TOP = 4;
   public static final int FLAG_BOTTOM = 8;
   public static final int FLAG_RIGHT = 16;
   public static final int FLAG_LEFT = 32;
   public static int[] SIDE_FLAG = new int[]{1, 2, 4, 8, 16, 32};
   public static byte FULLVIS = 63;
   public byte x = 0;
   public byte y = 0;
   public byte z = 0;
   public byte visMask;

   public SimplePosElement() {
      this.visMask = FULLVIS;
   }

   public static int countBits(int var0) {
      return ((var0 = ((var0 = ((var0 = ((var0 = (var0 >>> 1 & 1431655765) + (var0 & 1431655765)) >>> 2 & 858993459) + (var0 & 858993459)) >>> 4 & 252645135) + (var0 & 252645135)) >>> 8 & 16711935) + (var0 & 16711935)) >>> 16) + (var0 & '\uffff');
   }

   public String toString() {
      return "SPE(" + this.x + ", " + this.y + ", " + this.z + " [v" + this.visMask + "])";
   }
}
