package org.schema.schine.graphicsengine.forms.debug;

import com.bulletphysics.linearmath.Transform;
import java.util.Iterator;
import java.util.Vector;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.DebugBoundingBox;
import org.schema.schine.graphicsengine.forms.DebugBox;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.graphicsengine.forms.simple.Box;

public class DebugDrawer {
   private static final Vector3f[][] verts = Box.init();
   public static Vector boundingBoxes = new Vector();
   public static Vector boxes = new Vector();
   public static Vector boundingXses = new Vector();
   public static Vector points = new Vector();
   public static Vector lines = new Vector();
   static long lastClear = 0L;
   static long lastClearPoints = 0L;
   static long lastClearLines = 0L;
   static long lastClearXses = 0L;

   public static void debugDraw(int var0, int var1, int var2, int var3, Transformable var4) {
      if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
         Transform var7 = new Transform(var4.getWorldTransform());
         Vector3f var5;
         (var5 = new Vector3f()).set((float)var0, (float)var1, (float)var2);
         var5.x -= (float)var3;
         var5.y -= (float)var3;
         var5.z -= (float)var3;
         var7.basis.transform(var5);
         var7.origin.add(var5);
         DebugBox var6;
         (var6 = new DebugBox(new Vector3f(-0.51F, -0.51F, -0.51F), new Vector3f(0.51F, 0.51F, 0.51F), var7, 1.0F, 0.0F, 0.0F, 1.0F)).LIFETIME = 200L;
         boxes.add(var6);
      }

   }

   public static void addArrowFromTransform(Transform var0) {
      Vector3f var1 = GlUtil.getUpVector(new Vector3f(), var0);
      Vector3f var2 = GlUtil.getRightVector(new Vector3f(), var0);
      Vector3f var3 = GlUtil.getForwardVector(new Vector3f(), var0);
      Vector3f var4 = new Vector3f(var1);
      Vector3f var5 = new Vector3f(var2);
      Vector3f var6 = new Vector3f(var3);
      var1.set(0.0F, 0.0F, 0.0F);
      var2.set(0.0F, 0.0F, 0.0F);
      var3.set(0.0F, 0.0F, 0.0F);
      var4.scale(3.0F);
      var5.scale(3.0F);
      var6.scale(4.0F);
      var1.add(var0.origin);
      var2.add(var0.origin);
      var3.add(var0.origin);
      var4.add(var0.origin);
      var5.add(var0.origin);
      var6.add(var0.origin);
      Vector4f var9 = new Vector4f(0.0F, 0.0F, 1.0F, 1.0F);
      Vector4f var7 = new Vector4f(1.0F, 0.0F, 0.0F, 1.0F);
      Vector4f var8 = new Vector4f(0.0F, 1.0F, 0.0F, 1.0F);
      DebugLine var10 = new DebugLine(var1, var4, var9);
      DebugLine var11 = new DebugLine(var2, var5, var7);
      DebugLine var12 = new DebugLine(var3, var6, var8);
      if (!lines.contains(var10)) {
         lines.add(var10);
      }

      if (!lines.contains(var11)) {
         lines.add(var11);
      }

      if (!lines.contains(var12)) {
         lines.add(var12);
      }

   }

   public static void clear() {
      boundingXses.clear();

      int var0;
      for(var0 = 0; var0 < boundingBoxes.size(); ++var0) {
         if (!((DebugGeometry)boundingBoxes.get(var0)).isAlive()) {
            boundingBoxes.remove(var0);
            --var0;
         }
      }

      for(var0 = 0; var0 < boxes.size(); ++var0) {
         if (!((DebugGeometry)boxes.get(var0)).isAlive()) {
            boxes.remove(var0);
            --var0;
         }
      }

      for(var0 = 0; var0 < points.size(); ++var0) {
         if (!((DebugGeometry)points.get(var0)).isAlive()) {
            points.remove(var0);
            --var0;
         }
      }

      for(var0 = 0; var0 < lines.size(); ++var0) {
         if (!((DebugGeometry)lines.get(var0)).isAlive()) {
            lines.remove(var0);
            --var0;
         }
      }

   }

   public static void drawBoundingBoxes() {
      for(int var0 = 0; var0 < boundingBoxes.size(); ++var0) {
         DebugBoundingBox var1 = (DebugBoundingBox)boundingBoxes.get(var0);

         assert var1.bb.min != null && var1.bb.max != null;

         Vector3f[][] var2 = Box.getVertices(var1.bb.min, var1.bb.max, verts);
         GL11.glPolygonMode(1032, 6913);
         GlUtil.glDisable(2896);
         GlUtil.glDisable(2884);
         GlUtil.glEnable(2903);
         GlUtil.glDisable(32879);
         GlUtil.glDisable(3553);
         GlUtil.glDisable(3552);
         GlUtil.glEnable(3042);
         GlUtil.glColor4f(var1.getColor().x, var1.getColor().y, var1.getColor().z, var1.getColor().w);
         GL11.glBegin(7);

         for(int var4 = 0; var4 < var2.length; ++var4) {
            for(int var3 = 0; var3 < var2[var4].length; ++var3) {
               GL11.glVertex3f(var2[var4][var3].x, var2[var4][var3].y, var2[var4][var3].z);
            }
         }

         GL11.glEnd();
         GlUtil.glEnable(2929);
         GlUtil.glEnable(2896);
         GlUtil.glDisable(2903);
         GlUtil.glEnable(2884);
         GlUtil.glDisable(3042);
         GL11.glPolygonMode(1032, 6914);
      }

   }

   public static void drawBoundingXses() {
      Iterator var0 = boundingXses.iterator();

      while(var0.hasNext()) {
         DebugBoundingBox var1 = (DebugBoundingBox)var0.next();
         GL11.glPolygonMode(1032, 6913);
         GlUtil.glDisable(2896);
         GlUtil.glDisable(2884);
         GlUtil.glEnable(2903);
         GlUtil.glDisable(32879);
         GlUtil.glDisable(3553);
         GlUtil.glDisable(3552);
         GlUtil.glEnable(3042);
         GlUtil.glColor4f(var1.getColor().x, var1.getColor().y, var1.getColor().z, var1.getColor().w);
         Vector3f var2;
         (var2 = new Vector3f(var1.bb.min)).x = var1.bb.max.x;
         var2.z = var1.bb.max.z;
         Vector3f var3;
         (var3 = new Vector3f(var1.bb.max)).x = var1.bb.min.x;
         var3.z = var1.bb.min.z;
         GL11.glBegin(1);
         GL11.glVertex3f(var2.x, var2.y, var2.z);
         GL11.glVertex3f(var3.x, var3.y, var3.z);
         GL11.glVertex3f(var1.bb.min.x, var1.bb.min.y, var1.bb.min.z);
         GL11.glVertex3f(var1.bb.max.x, var1.bb.max.y, var1.bb.max.z);
         GL11.glEnd();
         GlUtil.glEnable(2929);
         GlUtil.glEnable(2896);
         GlUtil.glDisable(2903);
         GlUtil.glEnable(2884);
         GlUtil.glDisable(3042);
         GL11.glPolygonMode(1032, 6914);
      }

   }

   public static void drawBoxes() {
      synchronized(boxes) {
         for(int var1 = boxes.size() - 1; var1 >= 0; --var1) {
            ((DebugBox)boxes.get(var1)).draw();
         }

      }
   }

   public static void drawLines() {
      synchronized(lines) {
         Iterator var1 = lines.iterator();

         while(var1.hasNext()) {
            ((DebugLine)var1.next()).draw();
         }

      }
   }

   public static void drawPoints() {
      synchronized(points) {
         for(int var1 = points.size() - 1; var1 >= 0; --var1) {
            ((DebugPoint)points.get(var1)).draw();
         }

      }
   }
}
