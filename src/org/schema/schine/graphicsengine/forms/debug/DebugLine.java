package org.schema.schine.graphicsengine.forms.debug;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;

public class DebugLine extends DebugGeometry {
   private Vector3f pointA;
   private Vector3f pointB;

   public DebugLine(Vector3f var1, Vector3f var2) {
      this.pointA = var1;
      this.pointB = var2;
   }

   public DebugLine(Vector3f var1, Vector3f var2, Vector4f var3) {
      this(var1, var2);
      this.color = var3;
   }

   public void drawRaw() {
      if (this.color != null) {
         GlUtil.glColor4f(this.color.x, this.color.y, this.color.z, this.color.w * this.getAlpha());
      } else {
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, this.getAlpha());
      }

      GL11.glBegin(1);
      GL11.glVertex3f(this.pointA.x, this.pointA.y, this.pointA.z);
      GL11.glVertex3f(this.pointB.x, this.pointB.y, this.pointB.z);
      GL11.glEnd();
   }

   public void draw() {
      GlUtil.glDisable(3553);
      GlUtil.glEnable(2903);
      GlUtil.glDisable(2896);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      this.drawRaw();
      GlUtil.glDisable(3042);
      GlUtil.glDisable(2903);
      GlUtil.glEnable(2896);
      GlUtil.glEnable(3553);
   }

   public int hashCode() {
      return this.pointA.hashCode() + this.pointB.hashCode();
   }

   public boolean equals(Object var1) {
      return ((DebugLine)var1).pointA.equals(this.pointA) && ((DebugLine)var1).pointB.equals(this.pointB);
   }

   public static DebugLine[] getCross(Transform var0, Vector3f var1, float var2, float var3, float var4, boolean var5) {
      Vector3f var6 = GlUtil.getUpVector(new Vector3f(), var0);
      Vector3f var7 = GlUtil.getRightVector(new Vector3f(), var0);
      Vector3f var8 = GlUtil.getForwardVector(new Vector3f(), var0);
      Vector3f var9 = new Vector3f(var6);
      Vector3f var10 = new Vector3f(var7);
      Vector3f var11 = new Vector3f(var8);
      var6.set(0.0F, 0.0F, 0.0F);
      var7.set(0.0F, 0.0F, 0.0F);
      var8.set(0.0F, 0.0F, 0.0F);
      var9.scale(var2);
      var10.scale(var3);
      var11.scale(var4);
      var6.add(var0.origin);
      var7.add(var0.origin);
      var8.add(var0.origin);
      var9.add(var0.origin);
      var10.add(var0.origin);
      var11.add(var0.origin);
      if (var5) {
         var6.sub(var9);
         var7.sub(var10);
         var8.sub(var11);
      }

      var1 = new Vector3f(var1);
      var0.basis.transform(var1);
      var6.add(var1);
      var7.add(var1);
      var8.add(var1);
      var9.add(var1);
      var10.add(var1);
      var11.add(var1);
      Vector4f var12 = new Vector4f(0.0F, 1.0F, 0.0F, 1.0F);
      Vector4f var14 = new Vector4f(1.0F, 0.0F, 0.0F, 1.0F);
      Vector4f var16 = new Vector4f(0.0F, 0.0F, 1.0F, 1.0F);
      DebugLine var13 = new DebugLine(var6, var9, var12);
      DebugLine var15 = new DebugLine(var7, var10, var14);
      DebugLine var17 = new DebugLine(var8, var11, var16);
      return new DebugLine[]{var13, var15, var17};
   }

   public static DebugLine[] getArrow(Vector3f var0, Vector3f var1, Vector4f var2, float var3, float var4, float var5, float var6, Transform var7) {
      DebugLine var8 = new DebugLine(var0, var1, var2);
      Vector3f var9;
      (var9 = new Vector3f()).sub(var0, var1);
      var9.normalize();
      var0 = Controller.getCamera().getUp(new Vector3f());
      Vector3f var10;
      (var10 = new Vector3f(var1)).add(var0);
      var10.add(var9);
      Vector3f var11 = new Vector3f(var1);
      var0.negate();
      var11.add(var0);
      var11.add(var9);
      (var0 = new Vector3f()).sub(var10, var1);
      var0.normalize();
      (var9 = new Vector3f()).sub(var11, var1);
      var9.normalize();
      Transform var12 = TransformTools.ident;
      if (var7 != null) {
         var12 = var7;
      }

      Vector3f var15 = new Vector3f(var1);
      var12.transform(var15);
      float var16 = Vector3fTools.diffLength(Controller.getCamera().getPos(), var15);
      float var17 = 1.0F;

      assert var5 <= var6;

      if (var16 <= var5) {
         var17 = var3;
      } else if (var16 >= var6) {
         var17 = var4;
      } else {
         var6 -= var5;
         var5 = var16 - var5;
         if (var6 > 0.0F) {
            var5 /= var6;
            var4 -= var3;
            var17 = var3 + var4 * var5;
         }
      }

      var0.scale(var17);
      var9.scale(var17);
      var10.add(var1, var0);
      var11.add(var1, var9);
      DebugLine var14 = new DebugLine(var1, var10, var2);
      DebugLine var13 = new DebugLine(var1, var11, var2);
      return new DebugLine[]{var8, var14, var13};
   }
}
