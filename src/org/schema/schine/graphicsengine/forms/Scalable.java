package org.schema.schine.graphicsengine.forms;

import javax.vecmath.Vector3f;

public interface Scalable {
   Vector3f getScale();

   void setScale(Vector3f var1);
}
