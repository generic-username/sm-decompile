package org.schema.schine.graphicsengine.forms;

public class VertexBoneWeight {
   public int vertexIndex;
   public float weight;
   public int boneIndex;

   public VertexBoneWeight(int var1, int var2, float var3) {
      this.vertexIndex = var1;
      this.weight = var3;
      this.boneIndex = var2;
   }

   public String toString() {
      return "VertexBoneWeight [vertexIndex=" + this.vertexIndex + ", weight=" + this.weight + ", boneIndex=" + this.boneIndex + "]";
   }
}
