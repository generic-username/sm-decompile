package org.schema.schine.graphicsengine.forms;

public class BoundingSphere {
   public float radius;

   public void setFrom(BoundingBox var1) {
      this.radius = 0.0F;
      this.radius = Math.max(var1.max.length(), var1.min.length());
   }
}
