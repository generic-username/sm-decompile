package org.schema.schine.graphicsengine.forms.gui;

import org.schema.schine.input.InputState;

public interface GUIActivationHighlightCallback extends GUIActivationCallback {
   boolean isHighlighted(InputState var1);
}
