package org.schema.schine.graphicsengine.forms.gui;

import org.schema.schine.input.InputState;

public abstract class GUIResizableElement extends GUIElement {
   public GUIResizableElement(InputState var1) {
      super(var1);
   }

   public abstract void setWidth(float var1);

   public abstract void setHeight(float var1);

   public void setWidth(int var1) {
      this.setWidth((float)var1);
   }

   public void setHeight(int var1) {
      this.setHeight((float)var1);
   }
}
