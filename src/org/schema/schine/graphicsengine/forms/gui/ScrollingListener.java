package org.schema.schine.graphicsengine.forms.gui;

public interface ScrollingListener {
   boolean activeScrolling();
}
