package org.schema.schine.graphicsengine.forms.gui;

import java.util.Iterator;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUINewScrollBar;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Keyboard;

public class GUIScrollablePanel extends GUIResizableElement implements GUIScrollableInterface, TooltipProvider {
   private static final int SCROLL_AMOUNT = 30;
   private static final float clipInset = 16.0F;
   public static int SCROLLABLE_NONE = 0;
   public static int SCROLLABLE_VERTICAL = 1;
   public static int SCROLLABLE_HORIZONTAL = 2;
   private final Vector4f clip;
   public GUIElement dependent;
   public int dependendHeightDiff;
   public int dependendWidthDiff;
   public boolean setLeftRightClipOnly;
   private GUIElement content;
   private float scrollY;
   private float scrollX;
   private GUIElement verticalScrollBar;
   private GUIElement horizontalScrollBar;
   private int scrollable;
   private ScrollingListener scrollingListener;
   private float width;
   private float height;
   private boolean firstDraw;
   private boolean verticalActive;
   private boolean horizontalActive;
   private boolean scrollLocking;
   private Vector3f origPos;

   public GUIScrollablePanel(float var1, float var2, GUIElement var3, InputState var4) {
      super(var4);
      this.scrollX = 0.0F;
      this.scrollable = SCROLLABLE_VERTICAL;
      this.firstDraw = true;
      this.origPos = new Vector3f();
      this.clip = new Vector4f(0.0F, var1, 0.0F, var2);
      this.dependent = var3;
      this.setWidth(var1);
      this.setHeight(var2);
   }

   public GUIScrollablePanel(float var1, float var2, InputState var3) {
      this(var1, var2, (GUIElement)null, var3);
   }

   public void checkMouseInside() {
      super.checkMouseInside();
      if (this.isInside() && (this.getScrollingListener() == null || this.getScrollingListener().activeScrolling()) && this.isActive()) {
         Iterator var1 = this.getState().getController().getInputController().getMouseEvents().iterator();

         while(true) {
            while(true) {
               MouseEvent var2;
               do {
                  if (!var1.hasNext()) {
                     return;
                  }
               } while((var2 = (MouseEvent)var1.next()).dWheel == 0);

               int var3 = -((int)Math.signum((float)var2.dWheel) * 30);
               if (!Keyboard.isKeyDown(42) && !Keyboard.isKeyDown(54) && this.isVerticalActive()) {
                  this.scrollVertical((float)var3);
               } else {
                  this.scrollHorizontal((float)var3);
               }
            }
         }
      }
   }

   protected void doOrientation() {
      if ((this.scrollable & SCROLLABLE_VERTICAL) == SCROLLABLE_VERTICAL) {
         this.verticalScrollBar.orientate(2, 0, 0, (int)this.getWidth(), (int)this.getHeight());
      }

      if ((this.scrollable & SCROLLABLE_HORIZONTAL) == SCROLLABLE_HORIZONTAL) {
         this.horizontalScrollBar.orientate(9, 0, 0, (int)this.getWidth(), (int)this.getHeight());
      }

   }

   public float getHeight() {
      return this.height;
   }

   public float getWidth() {
      return this.width;
   }

   public boolean isPositionCenter() {
      return false;
   }

   public boolean isActive() {
      return this.dependent != null ? this.dependent.isActive() : true;
   }

   public void setWidth(float var1) {
      this.width = var1;
      this.clip.set(0.0F, var1, 0.0F, this.height);
   }

   public void setHeight(float var1) {
      this.height = var1;
      this.clip.set(0.0F, this.width, 0.0F, var1);
   }

   private void checkScrollSize() {
   }

   public void cleanUp() {
      if (this.content != null) {
         this.content.cleanUp();
      }

      if (this.verticalScrollBar != null) {
         this.verticalScrollBar.cleanUp();
      }

      if (this.horizontalScrollBar != null) {
         this.horizontalScrollBar.cleanUp();
      }

   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      if (this.dependent != null) {
         this.setWidth(this.dependent.getWidth() + (float)this.dependendWidthDiff);
         this.setHeight(this.dependent.getHeight() + (float)this.dependendHeightDiff);
      }

      this.scrollHorizontal(0.0F);
      this.scrollVertical(0.0F);
      this.doOrientation();
      GlUtil.glPushMatrix();
      this.clip.set(0.0F, this.getWidth(), 0.0F, this.getHeight());
      Vector4f var10000;
      if (this.verticalActive) {
         var10000 = this.clip;
         var10000.y -= 16.0F;
      }

      if (this.horizontalActive) {
         var10000 = this.clip;
         var10000.w -= 16.0F;
      }

      this.origPos.set(this.content.getPos());
      Vector3f var3 = this.content.getPos();
      var3.x -= this.scrollX;
      var3 = this.content.getPos();
      var3.y -= this.scrollY;
      float var1 = this.scrollX;
      float var2 = this.scrollY;
      GlUtil.addScroll(var1, var2);
      this.transform();
      if (this.setLeftRightClipOnly) {
         GlUtil.pushClipLR(this.getClip());
      } else {
         GlUtil.pushClipSubtract(this.getClip());
      }

      this.checkMouseInside();
      if (this.scrollLocking && this.isInside()) {
         this.scrollLock();
      }

      GlUtil.glPushMatrix();
      this.content.draw();
      GlUtil.subScroll(var1, var2);
      if (this.setLeftRightClipOnly) {
         GlUtil.popClipLR(this.getClip());
      } else {
         GlUtil.popClip();
      }

      GlUtil.glPopMatrix();
      if ((this.scrollable & SCROLLABLE_VERTICAL) == SCROLLABLE_VERTICAL) {
         if (this.content.getHeight() > this.getHeight()) {
            this.verticalActive = true;
            this.verticalScrollBar.draw();
         } else {
            this.verticalActive = false;
            this.scrollY = 0.0F;
         }
      } else {
         this.verticalActive = false;
      }

      if ((this.scrollable & SCROLLABLE_HORIZONTAL) == SCROLLABLE_HORIZONTAL) {
         if (this.content.getWidth() > this.getWidth()) {
            this.horizontalActive = true;
            this.horizontalScrollBar.draw();
         } else {
            this.horizontalActive = false;
            this.scrollX = 0.0F;
         }
      } else {
         this.horizontalActive = false;
      }

      this.content.setPos(this.origPos);
      GlUtil.glPopMatrix();
   }

   public float getContentToPanelPercentageY() {
      return this.content.getHeight() / this.getHeight();
   }

   public float getContentToPanelPercentageX() {
      return this.content.getWidth() / this.getWidth();
   }

   public void onInit() {
      if (this.firstDraw) {
         this.getContent().onInit();
         if ((this.scrollable & SCROLLABLE_VERTICAL) == SCROLLABLE_VERTICAL) {
            this.verticalScrollBar = new GUINewScrollBar(this.getState(), this, SCROLLABLE_VERTICAL, false);
            this.verticalScrollBar.onInit();
         }

         if ((this.scrollable & SCROLLABLE_HORIZONTAL) == SCROLLABLE_HORIZONTAL) {
            this.horizontalScrollBar = new GUINewScrollBar(this.getState(), this, SCROLLABLE_HORIZONTAL, false);
            this.horizontalScrollBar.onInit();
         }

         this.doOrientation();
         this.firstDraw = false;
      }
   }

   public Vector4f getClip() {
      return this.clip;
   }

   public GUIElement getContent() {
      return this.content;
   }

   public void setContent(GUIElement var1) {
      if (this.content != null && this.getChilds().contains(this.content)) {
         this.detach(this.content);
      }

      this.content = var1;
      this.attach(var1);
   }

   public float getScolledPercentHorizontal() {
      return this.scrollX / (this.content.getWidth() - this.getWidth());
   }

   public float getScolledPercentVertical() {
      return this.scrollY / (this.content.getHeight() - this.getHeight());
   }

   public void scrollHorizontal(float var1) {
      if (!this.isScrollLocked()) {
         if (this.content.getWidth() > this.getWidth()) {
            this.scrollX = Math.min(Math.max(0.0F, this.scrollX + var1), this.content.getWidth() - this.getWidth());
         }

      }
   }

   public boolean isScrollLocked() {
      return this.getState().getController().getInputController().isScrollLockOn(this);
   }

   public void scrollVertical(float var1) {
      if (!this.isScrollLocked()) {
         if (this.content.getHeight() > this.getHeight()) {
            this.scrollY = Math.min(Math.max(0.0F, this.scrollY + var1), this.content.getHeight() - this.getHeight());
         }

      }
   }

   public ScrollingListener getScrollingListener() {
      return this.scrollingListener;
   }

   public void scrollHorizontalPercent(float var1) {
      if (!this.isScrollLocked()) {
         if (this.content.getWidth() > this.getWidth()) {
            this.scrollX = var1 * (this.content.getWidth() - this.getWidth());
         }

      }
   }

   public void scrollVerticalPercent(float var1) {
      if (!this.isScrollLocked()) {
         if (this.content.getHeight() > this.getHeight()) {
            this.scrollY = var1 * (this.content.getHeight() - this.getHeight());
         }

      }
   }

   public float getScrollBarHeight() {
      return this.getHeight();
   }

   public float getScrollBarWidth() {
      return this.getWidth();
   }

   public void scrollHorizontalPercentTmp(float var1) {
   }

   public void scrollVerticalPercentTmp(float var1) {
   }

   public boolean isVerticalActive() {
      return this.verticalActive;
   }

   public void setScrollingListener(ScrollingListener var1) {
      this.scrollingListener = var1;
   }

   public float getScrollX() {
      return this.scrollX;
   }

   public float getScrollY() {
      return this.scrollY;
   }

   public void reset() {
      this.scrollX = 0.0F;
      this.scrollY = 0.0F;
   }

   public void drawToolTip() {
      if (this.getContent() != null && this.getContent() instanceof TooltipProvider) {
         ((TooltipProvider)this.getContent()).drawToolTip();
      }

   }

   public int getScrollable() {
      return this.scrollable;
   }

   public void setScrollable(int var1) {
      this.scrollable = var1;
   }

   public float getClipWidth() {
      return this.getWidth() - (this.verticalActive ? 16.0F : 0.0F);
   }

   public float setClipHeight() {
      return this.getHeight() - (this.horizontalActive ? 16.0F : 0.0F);
   }

   public boolean isInsideScrollBar() {
      return this.verticalScrollBar != null && (this.scrollable & SCROLLABLE_VERTICAL) == SCROLLABLE_VERTICAL && this.verticalScrollBar.isInside() || this.horizontalScrollBar != null && (this.scrollable & SCROLLABLE_HORIZONTAL) == SCROLLABLE_HORIZONTAL && this.horizontalScrollBar.isInside();
   }

   public void scrollLock() {
      this.getState().getController().getInputController().scrollLockOn(this);
   }

   public void setScrollLocking(boolean var1) {
      this.scrollLocking = var1;
   }
}
