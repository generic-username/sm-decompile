package org.schema.schine.graphicsengine.forms.gui;

import org.schema.schine.input.InputState;

public interface GUIActivationCallback {
   boolean isVisible(InputState var1);

   boolean isActive(InputState var1);
}
