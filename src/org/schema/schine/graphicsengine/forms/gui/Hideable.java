package org.schema.schine.graphicsengine.forms.gui;

public interface Hideable {
   void hide();

   void unhide();
}
