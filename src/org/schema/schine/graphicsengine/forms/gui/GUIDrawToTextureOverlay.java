package org.schema.schine.graphicsengine.forms.gui;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.texture.TextureLoader;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.network.client.ClientStateInterface;

public abstract class GUIDrawToTextureOverlay extends GUIOverlay {
   protected int texWidth = 256;
   protected int texHeight = 256;
   private boolean firstdraw = true;

   public GUIDrawToTextureOverlay(int var1, int var2, ClientState var3) {
      super(new Sprite(var1, var2), var3);
      this.texWidth = var1;
      this.texHeight = var2;
   }

   public void draw() {
      if (this.firstdraw) {
         this.onInit();
      }

      this.sprite.setBlend(true);
      super.draw();
      this.sprite.setBlend(false);
   }

   public void onInit() {
      this.firstdraw = false;
      this.sprite.getMaterial().setTexture(TextureLoader.getEmptyTexture(this.texWidth, this.texHeight));
      this.sprite.setHeight(this.texHeight);
      this.sprite.setWidth(this.texWidth);
      this.sprite.onInit();
   }

   public void updateGUI(ClientStateInterface var1) {
      if (this.firstdraw) {
         this.onInit();
      }

      GL11.glViewport(0, 0, this.texWidth, this.texHeight);
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      GlUtil.glMatrixMode(5889);
      GlUtil.glDisable(2896);
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      GLU.gluOrtho2D(0.0F, (float)this.texWidth, 0.0F, (float)this.texHeight);
      GL11.glClearColor(0.4F, 0.4F, 0.4F, 1.0F);
      GL11.glClear(16640);
      this.drawOverlayTexture(var1);
      GlUtil.glEnable(3553);
      GlUtil.glBindTexture(3553, this.sprite.getMaterial().getTexture().getTextureId());
      GL11.glCopyTexImage2D(3553, 0, 6408, 0, 0, this.texWidth, this.texHeight, 0);
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5888);
      GlUtil.glEnable(2929);
      GlUtil.glDisable(3553);
      GlUtil.glEnable(2896);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glPopMatrix();
      GL11.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
      GL11.glClear(16640);
      GL11.glViewport(Controller.viewport.get(0), Controller.viewport.get(1), Controller.viewport.get(2), Controller.viewport.get(3));
   }

   public abstract void drawOverlayTexture(ClientStateInterface var1);
}
