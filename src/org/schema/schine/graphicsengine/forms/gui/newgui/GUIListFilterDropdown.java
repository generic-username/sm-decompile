package org.schema.schine.graphicsengine.forms.gui.newgui;

public abstract class GUIListFilterDropdown implements GuiListFilter {
   public final Object[] values;
   protected Object value;

   public GUIListFilterDropdown(Object... var1) {
      assert var1 != null;

      this.values = var1;
   }

   public Object getFilter() {
      return this.value;
   }

   public void setFilter(Object var1) {
      this.value = var1;
   }
}
