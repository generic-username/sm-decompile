package org.schema.schine.graphicsengine.forms.gui.newgui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.List;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallbackBlocking;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;
import org.schema.schine.network.client.ClientState;

public class GUIHorizontalButtonExpandable extends GUIHorizontalButton implements GUIActivationHighlightCallback, GUICallback {
   private final List children = new ObjectArrayList();
   private boolean expanded;

   public void addButton(GUIHorizontalButton var1) {
      this.children.add(var1);
   }

   public void addButton(Object var1, GUIHorizontalArea.HButtonType var2, GUICallback var3, GUIActivationCallback var4) {
      GUIHorizontalButtonExpandable.CB var6 = new GUIHorizontalButtonExpandable.CB(var3);
      GUIHorizontalButton var5 = new GUIHorizontalButton(this.getState(), var2, var1, var6, this.activeInterface, var4);
      this.children.add(var5);
   }

   public GUIHorizontalButtonExpandable(ClientState var1, GUIHorizontalArea.HButtonType var2, Object var3, GUIActiveInterface var4) {
      super(var1, (GUIHorizontalArea.HButtonType)var2, var3, (GUICallback)null, var4, (GUIActivationCallback)null);
      this.callback = this;
      this.actCallback = this;
   }

   public void draw() {
      this.isInside();
      if (this.actCallback == null || this.actCallback.isVisible(this.getState())) {
         if (this.actCallback != null) {
            this.actCallback.isActive(this.getState());
         }

         boolean var1 = this.actCallback != null && this.actCallback instanceof GUIActivationHighlightCallback && ((GUIActivationHighlightCallback)this.actCallback).isHighlighted(this.getState());
         super.draw();
         if (var1) {
            for(int var3 = 0; var3 < this.children.size(); ++var3) {
               GUIHorizontalButton var2;
               (var2 = (GUIHorizontalButton)this.children.get(var3)).setPos(0.0F, this.getHeight() * (float)(var3 + 1), 0.0F);
               var2.setWidth(this.getWidth());
               var2.draw();
            }
         }
      }

   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         this.expanded = !this.expanded;
      }

   }

   public boolean isOccluded() {
      return this.activeInterface != null && !this.activeInterface.isActive();
   }

   public boolean isVisible(InputState var1) {
      return true;
   }

   public boolean isActive(InputState var1) {
      return this.activeInterface == null || this.activeInterface.isActive();
   }

   public boolean isHighlighted(InputState var1) {
      return this.expanded;
   }

   class CB implements GUICallbackBlocking {
      private final GUICallback callback;

      public CB(GUICallback var2) {
         this.callback = var2;
      }

      public boolean isOccluded() {
         return this.callback.isOccluded();
      }

      public void callback(GUIElement var1, MouseEvent var2) {
         this.callback.callback(var1, var2);
      }

      public boolean isBlocking() {
         return true;
      }

      public void onBlockedCallbackExecuted() {
      }

      public void onBlockedCallbackNotExecuted(boolean var1) {
      }
   }
}
