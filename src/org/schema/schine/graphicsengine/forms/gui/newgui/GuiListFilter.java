package org.schema.schine.graphicsengine.forms.gui.newgui;

public interface GuiListFilter {
   boolean isOk(Object var1, Object var2);

   Object getFilter();
}
