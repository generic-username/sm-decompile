package org.schema.schine.graphicsengine.forms.gui.newgui;

public abstract class GUIListFilterText implements GuiListFilter {
   protected String string;

   public String getFilter() {
      return this.string;
   }

   public void setFilter(String var1) {
      this.string = var1;
   }
}
