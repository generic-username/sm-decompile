package org.schema.schine.graphicsengine.forms.gui.newgui;

public interface GUISelectable {
   boolean isSelected();

   boolean isActive();
}
