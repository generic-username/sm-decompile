package org.schema.schine.graphicsengine.forms.gui.newgui;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public abstract class GUIInnerWindow extends GUIElement {
   public int cornerUpperOffsetX;
   public int cornerUpperOffsetY;
   public int cornerDistanceX;
   public int cornerDistanceTopY;
   public int cornerDistanceBottomY;
   public boolean upperCap = true;
   public boolean hasBackground = true;
   boolean init = false;
   private GUIElement p;
   private GUITexDrawableArea top;
   private final Vector4f tint = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   private GUITexDrawableArea bottom;
   private GUITexDrawableArea left;
   private GUITexDrawableArea right;
   private GUITexDrawableArea bg;
   private Sprite cs;
   private GUITexDrawableArea topLeft;
   private GUITexDrawableArea topRight;
   private GUITexDrawableArea bottomLeft;
   private GUITexDrawableArea bottomRight;
   public int extraHeight;

   public GUIInnerWindow(InputState var1, GUIElement var2, int var3) {
      super(var1);
      this.cornerDistanceX = var3;
      this.cornerDistanceTopY = var3;
      this.cornerDistanceBottomY = var3;
      this.p = var2;
   }

   public void cleanUp() {
      if (this.top != null) {
         this.top.cleanUp();
      }

      if (this.bottom != null) {
         this.bottom.cleanUp();
      }

      if (this.left != null) {
         this.left.cleanUp();
      }

      if (this.right != null) {
         this.right.cleanUp();
      }

      if (this.bg != null) {
         this.bg.cleanUp();
      }

      if (this.topLeft != null) {
         this.topLeft.cleanUp();
      }

      if (this.topRight != null) {
         this.topRight.cleanUp();
      }

      if (this.bottomLeft != null) {
         this.bottomLeft.cleanUp();
      }

      if (this.bottomRight != null) {
         this.bottomRight.cleanUp();
      }

   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      this.setPos((float)(this.cornerDistanceX + this.cornerUpperOffsetX), (float)(this.cornerDistanceTopY + this.cornerUpperOffsetY), 0.0F);
      GlUtil.glPushMatrix();
      this.transform();
      this.checkMouseInside();
      this.drawWindow();
      int var1 = this.getChilds().size();

      for(int var2 = 0; var2 < var1; ++var2) {
         ((AbstractSceneNode)this.getChilds().get(var2)).draw();
      }

      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.cs = Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getCorners());
      this.topLeft = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getCorners()).getMaterial().getTexture(), 0.0F, 0.0F);
      this.topLeft.setSpriteSubIndex(this.getLeftTop(), this.cs.getMultiSpriteMaxX(), this.cs.getMultiSpriteMaxY());
      this.topLeft.onInit();
      this.topRight = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getCorners()).getMaterial().getTexture(), 0.0F, 0.0F);
      this.topRight.setSpriteSubIndex(this.getRightTop(), this.cs.getMultiSpriteMaxX(), this.cs.getMultiSpriteMaxY());
      this.topRight.onInit();
      this.bottomLeft = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getCorners()).getMaterial().getTexture(), 0.0F, 0.0F);
      this.bottomLeft.setSpriteSubIndex(this.getBottomLeft(), this.cs.getMultiSpriteMaxX(), this.cs.getMultiSpriteMaxY());
      this.bottomLeft.onInit();
      this.bottomRight = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getCorners()).getMaterial().getTexture(), 0.0F, 0.0F);
      this.bottomRight.setSpriteSubIndex(this.getBottomRight(), this.cs.getMultiSpriteMaxX(), this.cs.getMultiSpriteMaxY());
      this.bottomRight.onInit();
      this.top = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getHorizontal()).getMaterial().getTexture(), 0.0F, this.getTopOffset());
      this.bottom = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getHorizontal()).getMaterial().getTexture(), 0.0F, this.getBottomOffset());
      this.left = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getVertical()).getMaterial().getTexture(), this.getLeftOffset(), 0.0F);
      this.right = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getVertical()).getMaterial().getTexture(), this.getRightOffset(), 0.0F);
      this.bg = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getBackground()).getMaterial().getTexture(), 0.0F, 0.0F);
      this.top.onInit();
      this.bottom.onInit();
      this.left.onInit();
      this.right.onInit();
      this.init = true;
   }

   private void drawWindow() {
      startStandardDraw();
      this.topLeft.setColor(this.tint);
      this.topRight.setColor(this.tint);
      this.bottomLeft.setColor(this.tint);
      this.bottomRight.setColor(this.tint);
      this.bg.setColor(this.tint);
      this.bottom.setColor(this.tint);
      this.top.setColor(this.tint);
      this.left.setColor(this.tint);
      this.right.setColor(this.tint);
      this.topLeft.setPos(0.0F, 0.0F, 0.0F);
      this.topLeft.setHeight((int)Math.min((float)this.cs.getHeight(), this.getHeight() / 2.0F));
      this.topLeft.setWidth((int)Math.min((float)this.cs.getWidth(), this.getWidth()));
      this.topRight.setPos(this.getWidth() - this.topLeft.getWidth(), 0.0F, 0.0F);
      this.topRight.setWidth((int)Math.min((float)this.cs.getWidth(), this.getWidth()));
      this.topRight.setHeight((int)Math.min((float)this.cs.getHeight(), this.getHeight() / 2.0F));
      if (this.upperCap) {
         this.topLeft.drawRaw();
         this.topRight.drawRaw();
      }

      this.bottomLeft.setPos(0.0F, this.getHeight() - this.topLeft.getHeight(), 0.0F);
      this.bottomLeft.setWidth((int)Math.min((float)this.cs.getWidth(), this.getWidth()));
      this.bottomLeft.setHeight((int)Math.min((float)this.cs.getHeight(), this.getHeight() / 2.0F));
      this.bottomLeft.drawRaw();
      this.bottomRight.setPos(this.getWidth() - this.bottomLeft.getWidth(), this.getHeight() - this.topLeft.getHeight(), 0.0F);
      this.bottomRight.setWidth((int)Math.min((float)this.cs.getWidth(), this.getWidth()));
      this.bottomRight.setHeight((int)Math.min((float)this.cs.getHeight(), this.getHeight() / 2.0F));
      this.bottomRight.drawRaw();
      if (this.upperCap) {
         this.bg.setPos(this.topLeft.getWidth(), this.topLeft.getHeight(), 0.0F);
         this.bg.setWidth((int)Math.max(0.0F, this.getWidth() - (this.bottomLeft.getWidth() + this.bottomRight.getWidth())));
         this.bg.setHeight((int)Math.max(0.0F, this.getHeight() - (this.topRight.getHeight() + this.bottomRight.getHeight())));
      } else {
         this.bg.setPos(this.topLeft.getWidth(), 0.0F, 0.0F);
         this.bg.setWidth((int)Math.max(0.0F, this.getWidth() - (this.bottomLeft.getWidth() + this.bottomRight.getWidth())));
         this.bg.setHeight((int)Math.max(0.0F, this.getHeight() - this.bottomRight.getHeight()));
      }

      if (this.hasBackground) {
         this.bg.drawRaw();
      }

      this.left.setWidth((int)this.topLeft.getWidth());
      if (this.upperCap) {
         this.left.setPos(0.0F, this.topLeft.getHeight(), 0.0F);
         this.left.setHeight((int)Math.max(0.0F, this.getHeight() - (this.topLeft.getHeight() + this.bottomLeft.getHeight())));
      } else {
         this.left.setPos(0.0F, 0.0F, 0.0F);
         this.left.setHeight((int)Math.max(0.0F, this.getHeight() - this.bottomLeft.getHeight()));
      }

      this.left.drawRaw();
      this.right.setWidth((int)this.topRight.getWidth());
      if (this.upperCap) {
         this.right.setPos(this.getWidth() - this.topRight.getWidth(), this.topRight.getHeight(), 0.0F);
         this.right.setHeight((int)Math.max(0.0F, this.getHeight() - (this.topRight.getHeight() + this.bottomRight.getHeight())));
      } else {
         this.right.setPos(this.getWidth() - this.topRight.getWidth(), 0.0F, 0.0F);
         this.right.setHeight((int)Math.max(0.0F, this.getHeight() - this.bottomRight.getHeight()));
      }

      this.right.drawRaw();
      this.top.setPos(this.topLeft.getWidth(), 0.0F, 0.0F);
      this.top.setHeight((int)this.topLeft.getHeight());
      this.top.setWidth((int)Math.max(0.0F, this.getWidth() - (this.topLeft.getWidth() + this.topRight.getWidth())));
      if (this.upperCap) {
         this.top.drawRaw();
      }

      this.bottom.setPos(this.bottomLeft.getWidth(), this.getHeight() - this.bottomLeft.getHeight(), 0.0F);
      this.bottom.setHeight((int)this.bottomLeft.getHeight());
      this.bottom.setWidth((int)Math.max(0.0F, this.getWidth() - (this.bottomLeft.getWidth() + this.bottomRight.getWidth())));
      this.bottom.drawRaw();
      this.topLeft.setColor((Vector4f)null);
      this.topRight.setColor((Vector4f)null);
      this.bottomLeft.setColor((Vector4f)null);
      this.bottomRight.setColor((Vector4f)null);
      this.bg.setColor((Vector4f)null);
      this.bottom.setColor((Vector4f)null);
      this.top.setColor((Vector4f)null);
      this.left.setColor((Vector4f)null);
      this.right.setColor((Vector4f)null);
      endStandardDraw();
   }

   protected abstract int getLeftTop();

   protected abstract int getRightTop();

   protected abstract int getBottomLeft();

   protected abstract int getBottomRight();

   protected abstract String getCorners();

   protected abstract String getVertical();

   protected abstract String getHorizontal();

   protected abstract String getBackground();

   protected abstract float getTopOffset();

   protected abstract float getBottomOffset();

   protected abstract float getLeftOffset();

   protected abstract float getRightOffset();

   public float getHeight() {
      return this.p.getHeight() - (float)(this.cornerDistanceTopY + this.cornerDistanceBottomY) - (float)this.cornerUpperOffsetY + (float)this.extraHeight;
   }

   public float getWidth() {
      return this.p.getWidth() - (float)(this.cornerDistanceX << 1) - (float)this.cornerUpperOffsetX;
   }

   public void setTint(float var1, float var2, float var3, float var4) {
      this.tint.set(var1, var2, var3, var4);
   }

   public Vector4f getTint() {
      return this.tint;
   }
}
