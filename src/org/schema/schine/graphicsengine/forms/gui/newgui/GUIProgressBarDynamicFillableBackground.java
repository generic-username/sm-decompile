package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.input.InputState;

public class GUIProgressBarDynamicFillableBackground extends GUIFilledArea {
   public GUIProgressBarDynamicFillableBackground(InputState var1, int var2, int var3) {
      super(var1, var2, var3);
   }

   protected int getLeftTop() {
      return 0;
   }

   protected int getRightTop() {
      return 1;
   }

   protected int getBottomLeft() {
      return 2;
   }

   protected int getBottomRight() {
      return 3;
   }

   protected String getCorners() {
      return "UI 8px Corners-8x8-gui-";
   }

   protected String getVertical() {
      return "UI 8px Vertical-32x1-gui-";
   }

   protected String getHorizontal() {
      return "UI 8px Horizontal-1x32-gui-";
   }

   protected String getBackground() {
      return "UI 8px Center_Background-gui-";
   }

   protected float getTopOffset() {
      return 0.0F;
   }

   protected float getBottomOffset() {
      return 0.03125F;
   }

   protected float getLeftOffset() {
      return 0.0F;
   }

   protected float getRightOffset() {
      return 0.03125F;
   }
}
