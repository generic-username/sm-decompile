package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.input.InputState;

public class GUITile extends GUIInnerTextbox {
   protected Object userData;

   public GUITile(InputState var1, float var2, float var3, Object var4) {
      super(var1);
      this.setWidth(var2);
      this.setHeight(var3);
      this.userData = var4;
      this.onInit();
   }
}
