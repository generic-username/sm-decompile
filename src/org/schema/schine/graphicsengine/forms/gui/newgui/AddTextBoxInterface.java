package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public interface AddTextBoxInterface {
   int getHeight();

   GUIElement createAndAttach(GUIAncor var1);
}
