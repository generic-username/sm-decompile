package org.schema.schine.graphicsengine.forms.gui.newgui;

import java.util.List;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public class ControllerElement {
   private ControllerElement.FilterRowStyle mode;
   private ControllerElement.FilterPos pos;
   public GUIElement gui;

   public ControllerElement(GUIElement var1) {
      this.mode = ControllerElement.FilterRowStyle.FULL;
      this.pos = ControllerElement.FilterPos.BOTTOM;
      this.gui = var1;
   }

   public ControllerElement.FilterRowStyle getMode() {
      return this.mode;
   }

   public void setMode(ControllerElement.FilterRowStyle var1) {
      this.mode = var1;
   }

   public ControllerElement.FilterPos getPos() {
      return this.pos;
   }

   public void setPos(ControllerElement.FilterPos var1) {
      this.pos = var1;
   }

   public static void drawFilterElements(boolean var0, int var1, List var2) {
      int var3;
      ControllerElement var4;
      if (var0) {
         for(var3 = var2.size() - 1; var3 >= 0; --var3) {
            var4 = (ControllerElement)var2.get(var3);
            if (!var0 && !(var4.gui instanceof GUIDropDownList)) {
               var4.gui.draw();
            } else if (var0 && var4.gui instanceof GUIDropDownList) {
               var4.gui.draw();
            }
         }

      } else {
         for(var3 = 0; var3 < var2.size(); ++var3) {
            (var4 = (ControllerElement)var2.get(var3)).gui.getPos().y = (float)var1;
            if (!var0 && !(var4.gui instanceof GUIDropDownList)) {
               var4.gui.draw();
            } else if (var0 && var4.gui instanceof GUIDropDownList) {
               var4.gui.draw();
            }

            assert var4.getMode() != ControllerElement.FilterRowStyle.LEFT || var3 != var2.size() - 1;

            assert var4.getMode() != ControllerElement.FilterRowStyle.LEFT || var3 <= 0 || ((ControllerElement)var2.get(var3 - 1)).getMode() == ControllerElement.FilterRowStyle.FULL || ((ControllerElement)var2.get(var3 - 1)).getMode() == ControllerElement.FilterRowStyle.RIGHT;

            assert var4.getMode() != ControllerElement.FilterRowStyle.RIGHT || var3 <= 0 || ((ControllerElement)var2.get(var3 - 1)).getMode() == ControllerElement.FilterRowStyle.LEFT;

            if (var4.getMode() == ControllerElement.FilterRowStyle.FULL || var4.getMode() == ControllerElement.FilterRowStyle.RIGHT) {
               var1 = (int)((float)var1 + var4.gui.getHeight());
            }
         }

      }
   }

   public static enum FilterRowStyle {
      FULL,
      LEFT,
      RIGHT;
   }

   public static enum FilterPos {
      TOP,
      BOTTOM;
   }
}
