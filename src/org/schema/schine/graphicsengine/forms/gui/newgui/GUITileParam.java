package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.input.InputState;

public class GUITileParam extends GUITile {
   public GUITileParam(InputState var1, float var2, float var3, Object var4) {
      super(var1, var2, var3, var4);
   }

   public Object getUserData() {
      return this.userData;
   }
}
