package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUITextOverlayTableDropDown extends GUITextOverlay {
   public GUITextOverlayTableDropDown(int var1, int var2, InputState var3) {
      super(var1, var2, ScrollableTableList.dropdownFontSize.getFont(), var3);
   }
}
