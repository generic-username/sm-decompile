package org.schema.schine.graphicsengine.forms.gui.newgui;

import PolygonStatsInterface.PolygonStatsEditableInterface;
import com.bulletphysics.util.ObjectArrayList;
import java.awt.Polygon;
import java.util.List;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.common.FastMath;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.Mouse;

public class GUIPolygonStats extends GUIElement {
   private static final float minVal = 0.05F;
   private final PolygonStatsInterface p;
   private final List texts;
   private int height;
   private int width;
   private boolean recreate;
   private boolean init;
   private float innerWidthSub;
   private float innerHeightSub;
   private Vector4f bgPolyColor;
   private Vector4f bgPolyOutlineColor;
   private Vector4f bgPolyInnerColor;
   private Vector4f bgPolyInnerOutlineColor;
   private int diaplayListIndex;
   private PolygonStatsEditableInterface editable;
   private boolean wasDown;
   private boolean pIns;

   public GUIPolygonStats(InputState var1, PolygonStatsInterface var2) {
      this(var1, var2, 128, 128);
   }

   public GUIPolygonStats(InputState var1, PolygonStatsInterface var2, int var3, int var4) {
      super(var1);
      this.texts = new ObjectArrayList();
      this.recreate = true;
      this.innerWidthSub = 10.0F;
      this.innerHeightSub = 10.0F;
      this.bgPolyColor = new Vector4f(0.4F, 0.4F, 0.4F, 1.0F);
      this.bgPolyOutlineColor = new Vector4f(0.0F, 0.0F, 0.0F, 1.0F);
      this.bgPolyInnerColor = new Vector4f(0.4F, 0.8F, 0.4F, 0.5F);
      this.bgPolyInnerOutlineColor = new Vector4f(0.1F, 0.1F, 0.1F, 0.7F);
      this.width = var3;
      this.height = var4;
      this.p = var2;
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (Keyboard.isKeyDown(29)) {
         this.recreate = true;
      }

      if (this.recreate) {
         this.recreate();
      }

      GlUtil.glPushMatrix();
      this.transform();
      if (this.isEditable()) {
         this.checkMouseInside();
      }

      GlUtil.glPushMatrix();
      GlUtil.translateModelview((float)((int)(this.getWidth() / 2.0F)), (float)((int)(this.getHeight() / 2.0F)), 0.0F);
      GlUtil.glDisable(2896);
      GlUtil.glEnable(3042);
      GlUtil.glDisable(3553);
      GlUtil.glBlendFunc(770, 771);
      GL11.glCallList(this.diaplayListIndex);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glEnable(2896);
      GlUtil.glEnable(3553);
      GlUtil.glDisable(3042);
      this.udpateTextsPos();

      for(int var1 = 0; var1 < this.texts.size(); ++var1) {
         ((GUITextOverlay)this.texts.get(var1)).draw();
      }

      GlUtil.glPopMatrix();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      if (!this.init) {
         if (this.recreate) {
            this.recreate();
         }

         this.init = true;
      }
   }

   private void recreate() {
      this.p.getDataPointsNum();
      if (this.diaplayListIndex != 0) {
         GL11.glDeleteLists(this.diaplayListIndex, 1);
      }

      this.diaplayListIndex = GL11.glGenLists(1);
      GL11.glNewList(this.diaplayListIndex, 4864);
      GL11.glBegin(9);
      GlUtil.glColor4f(this.bgPolyColor);
      if (this.isEditable()) {
         this.doOutlineCycle(false);
      } else {
         this.doOutline(false);
      }

      GL11.glEnd();
      GL11.glBegin(2);
      GlUtil.glColor4f(this.bgPolyOutlineColor);
      if (this.isEditable()) {
         this.doOutlineCycle(false);
      } else {
         this.doOutline(false);
      }

      GL11.glEnd();
      GL11.glBegin(1);
      GlUtil.glColor4f(this.bgPolyInnerOutlineColor);
      this.doCross(false);
      GL11.glEnd();
      GL11.glBegin(9);
      GlUtil.glColor4f(this.bgPolyInnerColor);
      this.doOutline(true);
      GL11.glEnd();
      GL11.glBegin(2);
      GlUtil.glColor4f(this.bgPolyInnerOutlineColor);
      this.doOutline(true);
      GL11.glEnd();
      this.recreate = false;
      GL11.glEndList();
      this.updateTextOutline();
   }

   public void updateTextOutline() {
      this.texts.clear();
      int var1 = this.p.getDataPointsNum();

      for(int var2 = 0; var2 < var1; ++var2) {
         GUITextOverlay var3;
         (var3 = new GUITextOverlay(1, 1, FontLibrary.getBlenderProMedium13(), this.getState())).setTextSimple(this.p.getValueName(var2));
         var3.onInit();
         this.texts.add(var3);
      }

      this.udpateTextsPos();
   }

   private void udpateTextsPos() {
      int var1 = this.p.getDataPointsNum();
      float var6 = 6.2831855F / (float)var1;
      int var2 = 0;

      for(float var3 = 0.0F; var3 < 6.2831855F; var3 += var6) {
         ((GUITextOverlay)this.texts.get(var2)).setColor(1.0F, 1.0F, 1.0F, 0.0F);
         ((GUITextOverlay)this.texts.get(var2)).draw();
         float var4 = -FastMath.cos(var3) * (this.getWidth() / 2.0F - this.innerWidthSub) * this.getP(var2, false);
         float var5 = FastMath.sin(var3) * (this.getHeight() / 2.0F - this.innerHeightSub) * this.getP(var2, false);
         if (Math.abs(var4) < 5.0F) {
            var4 = (float)((int)(var4 - (float)(((GUITextOverlay)this.texts.get(var2)).getMaxLineWidth() / 2)));
         } else if (var4 < 0.0F) {
            var4 = (float)((int)(var4 - (float)((GUITextOverlay)this.texts.get(var2)).getMaxLineWidth()));
         }

         ((GUITextOverlay)this.texts.get(var2)).setPos((float)((int)var4), (float)((int)(var5 - (float)(((GUITextOverlay)this.texts.get(var2)).getTextHeight() / 2))), 0.0F);
         ((GUITextOverlay)this.texts.get(var2)).draw();
         ((GUITextOverlay)this.texts.get(var2)).setColor(1.0F, 1.0F, 1.0F, 1.0F);
         ++var2;
      }

   }

   public void doOutline(boolean var1) {
      int var2 = this.p.getDataPointsNum();
      float var7 = 6.2831855F / (float)var2;
      int var3 = 0;

      for(float var4 = 0.0F; var4 < 6.2831855F; var4 += var7) {
         float var5 = -FastMath.cos(var4) * (this.getWidth() / 2.0F - this.innerWidthSub) * this.getP(var3, var1);
         float var6 = FastMath.sin(var4) * (this.getHeight() / 2.0F - this.innerHeightSub) * this.getP(var3, var1);
         GL11.glVertex2f((float)((int)var5), (float)((int)var6));
         ++var3;
      }

   }

   public void doOutlineCycle(boolean var1) {
      this.p.getDataPointsNum();
      int var2 = 0;

      for(float var3 = 0.0F; var3 < 6.2831855F; var3 += 0.19634955F) {
         float var4 = -FastMath.cos(var3) * (this.getWidth() / 2.0F - this.innerWidthSub) * this.getP(var2, var1);
         float var5 = FastMath.sin(var3) * (this.getHeight() / 2.0F - this.innerHeightSub) * this.getP(var2, var1);
         GL11.glVertex2f((float)((int)var4), (float)((int)var5));
         ++var2;
      }

   }

   public void doEditableCross(boolean var1) {
      int var3 = this.p.getDataPointsNum();

      assert var3 == 3;

      float var4 = this.getWidth() / 2.0F;
      float var2 = this.getHeight() / 2.0F;
      (new Vector2d((double)var4, (double)var2)).length();
      this.p.getDataPointsNum();
   }

   public void doCross(boolean var1) {
      int var2 = this.p.getDataPointsNum();
      float var7 = 6.2831855F / (float)var2;
      int var3 = 0;

      for(float var4 = 0.0F; var4 < 6.2831855F; var4 += var7) {
         GL11.glVertex2f(0.0F, 0.0F);
         float var5 = -FastMath.cos(var4) * (this.getWidth() / 2.0F - this.innerWidthSub) * this.getP(var3, var1);
         float var6 = FastMath.sin(var4) * (this.getHeight() / 2.0F - this.innerHeightSub) * this.getP(var3, var1);
         GL11.glVertex2f((float)((int)var5), (float)((int)var6));
         ++var3;
      }

   }

   private float getP(int var1, boolean var2) {
      return var2 ? Math.max(0.05F, (float)this.p.getPercent(var1)) : 1.0F;
   }

   public float getHeight() {
      return (float)this.height;
   }

   public float getWidth() {
      return (float)this.width;
   }

   public void setWidth(int var1) {
      this.recreate = true;
      this.width = var1;
   }

   public void setHeight(int var1) {
      this.recreate = true;
      this.height = var1;
   }

   public boolean isEditable() {
      return this.editable != null;
   }

   public void setEditable(PolygonStatsEditableInterface var1) {
      this.editable = var1;
      if (var1 != null) {
         this.setCallback(new GUIPolygonStats.EditableCallBack());
      }

   }

   public void update() {
      if (this.isEditable() && this.isInside() && this.pIns) {
         float var1 = this.getWidth() / 2.0F;
         float var2 = this.getHeight() / 2.0F;
         float var3 = Math.abs(this.getRelMousePos().x - var1);
         float var4 = Math.abs(this.getRelMousePos().y - var2);
         float var5 = this.getWidth() / 2.0F - this.innerWidthSub;
         if (FastMath.carmackSqrt(var3 * var3 + var4 * var4) < var5 + 3.0F) {
            Polygon var13 = new Polygon();
            int var14 = this.p.getDataPointsNum();
            var4 = 6.2831855F / (float)var14;
            int var15 = 0;

            float var6;
            float var7;
            float var8;
            float var9;
            float var10;
            float var11;
            float var12;
            for(var6 = 0.0F; var6 < 6.2831855F; var6 += var4) {
               var7 = -FastMath.cos(var6) * (this.getWidth() / 2.0F - this.innerWidthSub) * this.getP(var15, false);
               var8 = FastMath.sin(var6) * (this.getHeight() / 2.0F - this.innerHeightSub) * this.getP(var15, false);
               var9 = -FastMath.cos(var6 + var4) * (this.getWidth() / 2.0F - this.innerWidthSub) * this.getP(var15, false);
               var10 = FastMath.sin(var6 + var4) * (this.getHeight() / 2.0F - this.innerHeightSub) * this.getP(var15, false);
               var11 = var7 - var9;
               var12 = var8 - var10;
               FastMath.sqrt(var11 * var11 + var12 * var12);
               var13.addPoint((int)(var1 + var7), (int)(var2 + var8));
               float var10000 = this.getRelMousePos().x;
               var10000 = this.getRelMousePos().y;
               ++var15;
            }

            var15 = 0;
            var6 = 0.0F;

            for(var7 = 0.0F; var7 < 6.2831855F; var7 += var4) {
               var8 = -FastMath.cos(var7) * (this.getWidth() / 2.0F - this.innerWidthSub) * this.getP(var15, false) + var1;
               var9 = FastMath.sin(var7) * (this.getHeight() / 2.0F - this.innerHeightSub) * this.getP(var15, false) + var2;
               var10 = var8 - this.getRelMousePos().x;
               var11 = var9 - this.getRelMousePos().y;
               var12 = FastMath.sqrt(var10 * var10 + var11 * var11);
               var6 += var12;
               ++var15;
            }

            var6 = (var6 /= 3.0F) + 0.5F * var6;
            var15 = 0;

            for(var8 = 0.0F; var8 < 6.2831855F; var8 += var4) {
               var9 = -FastMath.cos(var8) * (this.getWidth() / 2.0F - this.innerWidthSub) * this.getP(var15, false) + var1;
               var10 = FastMath.sin(var8) * (this.getHeight() / 2.0F - this.innerHeightSub) * this.getP(var15, false) + var2;
               var11 = var9 - this.getRelMousePos().x;
               var12 = var10 - this.getRelMousePos().y;
               var3 = this.getRelMousePos().x - var1;
               var7 = this.getRelMousePos().y - var2;
               if (Mouse.isButtonDown(0)) {
                  if (Math.sqrt((double)(var3 * var3 + var7 * var7)) <= Math.sqrt((double)(var9 * var9 + var10 * var10))) {
                     this.wasDown = true;
                  }

                  if (this.wasDown && Math.sqrt((double)(var3 * var3 + var7 * var7)) > Math.sqrt((double)(var9 * var9 + var10 * var10))) {
                     var3 = (float)((double)var3 / Math.sqrt((double)(var3 * var3 + var7 * var7)));
                     var7 = (float)((double)var7 / Math.sqrt((double)(var3 * var3 + var7 * var7)));
                     var3 = (float)((double)var3 * Math.sqrt((double)(var9 * var9 + var10 * var10)));
                     var7 = (float)((double)var7 * Math.sqrt((double)(var9 * var9 + var10 * var10)));
                     var11 = var9 - (var3 + var1);
                     var12 = var10 - (var7 + var2);
                  }

                  if (Math.sqrt((double)(var3 * var3 + var7 * var7)) <= Math.sqrt((double)(var9 * var9 + var10 * var10))) {
                     var3 = FastMath.sqrt(var11 * var11 + var12 * var12) / var6;
                     this.editable.setPercent(var15, var3);
                     this.recreate = true;
                  }
               } else {
                  this.wasDown = false;
               }

               ++var15;
            }
         }

      } else {
         if (!this.isInside() && !Mouse.isButtonDown(0)) {
            this.pIns = false;
         }

      }
   }

   class EditableCallBack implements GUICallback {
      private EditableCallBack() {
      }

      public void callback(GUIElement var1, MouseEvent var2) {
         if (var2.pressedLeftMouse()) {
            GUIPolygonStats.this.pIns = true;
         } else {
            if (var2.releasedLeftMouse()) {
               GUIPolygonStats.this.pIns = false;
            }

         }
      }

      public boolean isOccluded() {
         return false;
      }

      // $FF: synthetic method
      EditableCallBack(Object var2) {
         this();
      }
   }
}
