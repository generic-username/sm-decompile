package org.schema.schine.graphicsengine.forms.gui.newgui;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredAncor;
import org.schema.schine.input.InputState;

public abstract class GUILeftRightArea extends GUIColoredAncor {
   boolean init = false;
   private Vector4f color;
   private GUITexDrawableArea left;
   private GUITexDrawableArea right;

   public GUILeftRightArea(InputState var1, int var2, int var3) {
      super(var1, (float)var2, (float)var3);
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.checkMouseInside();
      this.drawWindow();
      int var1 = this.getChilds().size();

      for(int var2 = 0; var2 < var1; ++var2) {
         ((AbstractSceneNode)this.getChilds().get(var2)).draw();
      }

      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.left = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getVertical()).getMaterial().getTexture(), this.getLeftOffset(), 0.0F);
      this.right = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getVertical()).getMaterial().getTexture(), this.getRightOffset(), 0.0F);
      this.left.onInit();
      this.right.onInit();
      this.init = true;
   }

   private void drawWindow() {
      startStandardDraw();
      this.left.setColor(this.color);
      this.left.setWidth(this.getPXWidth());
      this.left.setPos(0.0F, 0.0F, 0.0F);
      this.left.setHeight((int)Math.max(0.0F, this.getHeight()));
      this.left.drawRaw();
      this.right.setColor(this.color);
      this.right.setWidth(this.getPXWidth());
      this.right.setPos(this.getWidth() - (float)this.getPXWidth(), 0.0F, 0.0F);
      this.right.setHeight((int)this.getHeight());
      this.right.drawRaw();
      endStandardDraw();
   }

   public abstract int getPXWidth();

   public abstract int getPXHeight();

   protected abstract String getVertical();

   protected abstract float getLeftOffset();

   protected abstract float getRightOffset();

   public Vector4f getColor() {
      return this.color;
   }

   public void setColor(Vector4f var1) {
      this.color = var1;
   }
}
