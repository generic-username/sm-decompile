package org.schema.schine.graphicsengine.forms.gui.newgui.settingsnew;

import javax.vecmath.Vector4f;
import org.newdawn.slick.UnicodeFont;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIEngineSettingsCheckBox;
import org.schema.schine.graphicsengine.forms.gui.GUISettingsElement;
import org.schema.schine.graphicsengine.forms.gui.TooltipProvider;
import org.schema.schine.input.InputState;

public class GUISettingsElementPanelNew extends GUIAncor implements TooltipProvider {
   private GUIElement settingElement;
   private boolean init;
   private boolean backGroundShade;
   private GUIColoredRectangle rec;
   private boolean horizontal;

   public GUISettingsElementPanelNew(InputState var1, int var2, int var3, GUIElement var4, boolean var5, boolean var6) {
      this(var1, var2, var3, FontLibrary.getBoldArialWhite14(), var4, var5, var6);
   }

   public GUISettingsElementPanelNew(InputState var1, int var2, int var3, UnicodeFont var4, GUIElement var5, boolean var6, boolean var7) {
      super(var1);
      this.horizontal = var7;
      this.settingElement = var5;
      this.backGroundShade = var6;
   }

   public GUISettingsElementPanelNew(InputState var1, GUIElement var2, boolean var3, boolean var4) {
      this(var1, 300, 60, var2, var3, var4);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      if (this.backGroundShade) {
         this.rec.draw();
      }

      if (this.settingElement instanceof GUIEngineSettingsCheckBox) {
         this.settingElement.getPos().x = 10.0F;
         if (isNewHud()) {
            this.settingElement.getPos().y = 6.0F;
         }
      } else {
         if (isNewHud()) {
            this.settingElement.getPos().y = -2.0F;
         }

         if (this.horizontal) {
            this.settingElement.getPos().y = 30.0F;
         }
      }

      this.settingElement.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.settingElement.onInit();
      if (this.backGroundShade) {
         if (this.settingElement instanceof GUIEngineSettingsCheckBox) {
            this.rec = new GUIColoredRectangle(this.getState(), 486.0F, this.getHeight(), new Vector4f(0.068F, 0.068F, 0.068F, 0.3F));
         } else {
            this.rec = new GUIColoredRectangle(this.getState(), this.getWidth(), this.getHeight(), new Vector4f(0.068F, 0.068F, 0.068F, 0.3F));
         }
      }

      this.init = true;
   }

   public float getHeight() {
      return this.settingElement.getHeight() + 5.0F;
   }

   public float getWidth() {
      return this.settingElement.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void update(Timer var1) {
      this.settingElement.update(var1);
      super.update(var1);
   }

   public void drawToolTip() {
      if (this.settingElement instanceof GUISettingsElement) {
         ((GUISettingsElementPanelNew)this.settingElement).drawToolTip();
      }

   }
}
