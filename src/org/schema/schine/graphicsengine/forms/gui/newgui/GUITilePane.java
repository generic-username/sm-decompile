package org.schema.schine.graphicsengine.forms.gui.newgui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.List;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUITilePane extends GUIElement {
   public final int tileWidth;
   public final int tileHeight;
   public final int tileWidthS;
   public final int tileHeightS;
   private int height;
   private int width;
   private boolean cachedPosition;
   private float cachePositionX;
   private float cachePositionXP;
   private float cachePositionY;
   private float cachePositionYP;
   private float cachedScrollX = -1.0F;
   private float cachedScrollY = -1.0F;
   private int cachedIndex;
   public GUIScrollablePanel scrollPanel;
   private final List tiles = new ObjectArrayList();
   private GUIElement dependent;
   public GUIActiveInterface activeInterface;
   private boolean tileDirty;
   private float lastDepWidth;
   private float lastDepHeight;
   public int spacingX = 5;
   public int spacingY = 5;

   public GUITilePane(InputState var1, GUIElement var2, int var3, int var4) {
      super(var1);
      this.tileWidth = var3;
      this.tileHeight = var4;
      this.tileWidthS = var3 - 4;
      this.tileHeightS = var4 - 4;
      this.width = var3;
      this.height = var4;

      assert var2 != null;

      this.dependent = var2;
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.isInvisible()) {
         if (this.tileDirty || this.dependent.getWidth() != this.lastDepWidth || this.dependent.getHeight() != this.lastDepHeight) {
            this.resort();
            this.tileDirty = false;
            this.lastDepWidth = this.dependent.getWidth();
            this.lastDepHeight = this.dependent.getHeight();
         }

         if (this.scrollPanel == null) {
            super.drawAttached();
         } else {
            this.drawScrolled(true, false);
         }
      }
   }

   private void drawScrolled(boolean var1, boolean var2) {
      if (this.cachedScrollX != this.scrollPanel.getScrollX() || this.cachedScrollY != this.scrollPanel.getScrollY()) {
         this.cachedPosition = false;
      }

      GlUtil.glPushMatrix();
      this.setInside(false);
      this.transform();
      if (this.isMouseUpdateEnabled()) {
         this.checkMouseInside();
      }

      if (this.scrollPanel != null && (this.cachedScrollX != this.scrollPanel.getScrollX() || this.cachedScrollY != this.scrollPanel.getScrollY())) {
         this.cachedPosition = false;
      }

      float var13 = 0.0F;
      float var14 = 0.0F;
      float var3 = 0.0F;
      float var4 = 0.0F;
      int var5 = 0;
      int var6 = this.getChilds().size();
      boolean var7 = false;
      if (this.cachedPosition) {
         var13 = this.cachePositionX;
         var14 = this.cachePositionY;
         var3 = this.cachePositionXP;
         var4 = this.cachePositionYP;
         var5 = this.cachedIndex;
      }

      while(var5 < var6) {
         GUITile var8 = (GUITile)this.getChilds().get(var5);
         int var9 = 0;
         int var10 = 0;
         GUITile var12;
         if (!var7) {
            for(int var11 = var5; var11 >= Math.max(var5 - 1, 0); --var11) {
               for(var12 = (GUITile)this.getChilds().get(var11); var11 - 1 >= Math.max(var5 - 1, 0) && ((GUITile)this.getChilds().get(var11 - 1)).getPos().y == var8.getPos().y; --var11) {
               }

               var9 = (int)((float)var9 + var12.getScale().x * var12.getWidth());
               var10 = (int)((float)var10 + var12.getScale().y * var12.getHeight());
            }
         }

         boolean var15 = true;
         if (var7 && (var13 > this.scrollPanel.getScrollX() + this.scrollPanel.getWidth() || var14 > this.scrollPanel.getScrollY() + this.scrollPanel.getHeight())) {
            break;
         }

         if (!var7 && (var13 < this.scrollPanel.getScrollX() - (float)var9 || var14 < this.scrollPanel.getScrollY() - (float)var10)) {
            var15 = false;
         }

         if (var15) {
            GlUtil.translateModelview(var3, var4, 0.0F);
            if (!var7) {
               this.cachePositionX = var13;
               this.cachePositionXP = var3;
               this.cachePositionY = var14;
               this.cachePositionYP = var4;
               this.cachedScrollX = this.scrollPanel != null ? this.scrollPanel.getScrollX() : 0.0F;
               this.cachedScrollY = this.scrollPanel != null ? this.scrollPanel.getScrollY() : 0.0F;
               this.cachedIndex = var5;
               this.cachedPosition = true;
               var7 = true;
            }

            var3 = 0.0F;
            var4 = 0.0F;
            var8.drawWithoutTransform();

            while(var5 + 1 < this.getChilds().size() && (var12 = (GUITile)this.getChilds().get(var5 + 1)).getPos().y == var8.getPos().y) {
               GlUtil.glPushMatrix();
               GlUtil.translateModelview(var12.getPos().x, 0.0F, 0.0F);
               var12.drawWithoutTransform();
               GlUtil.glPopMatrix();
               ++var5;
            }
         }

         var14 += var8.getScale().y * var8.getHeight();
         var4 += var8.getScale().y * var8.getHeight();
         ++var5;
      }

      GlUtil.glPopMatrix();
   }

   private void resort() {
      int var1 = 0;
      int var2 = this.spacingY;
      this.detachAll();

      for(int var3 = 0; var3 < this.getTiles().size(); ++var3) {
         GUITile var4 = (GUITile)this.getTiles().get(var3);

         assert this.dependent != null;

         if (var1 > 0 && (float)(var1 + this.tileWidth) > this.dependent.getWidth()) {
            var1 = 0;
            var2 += this.spacingY + this.tileHeight;
         }

         var1 += this.spacingX;
         var4.setPos((float)var1, (float)var2, 0.0F);
         this.attach(var4);
         var1 += this.tileWidth;
      }

      this.width = var1 + this.tileWidth;
      this.height = var2 + this.tileHeight;
   }

   public void onInit() {
   }

   public void removeTile(GUIElement var1) {
      for(int var2 = 0; var2 < this.getTiles().size(); ++var2) {
         GUITile var3;
         if ((var3 = (GUITile)this.getTiles().get(var2)).getContent().getChilds().contains(var1)) {
            this.tileDirty = true;
            var3.cleanUp();
            this.getTiles().remove(var2);
            return;
         }
      }

   }

   public GUITile addButtonTile(String var1, String var2, GUIHorizontalArea.HButtonColor var3, GUICallback var4, final GUIActivationCallback var5) {
      GUIHorizontalButton var6;
      (var6 = new GUIHorizontalButton(this.getState(), var3, var1, var4, this.activeInterface, var5)).onInit();
      var6.setWidth(this.tileWidthS);
      GUIAncor var8 = new GUIAncor(this.getState(), (float)(this.tileWidthS - 4), (float)this.tileHeightS - (var6.getHeight() + 2.0F + 4.0F));
      GUITextOverlay var9;
      (var9 = new GUITextOverlay(this.tileWidthS, (int)((float)this.tileHeightS - (var6.getHeight() + 2.0F)), FontLibrary.getBlenderProMedium15(), this.getState()) {
         public void draw() {
            if (var5 != null && !var5.isActive(this.getState())) {
               this.setColor(0.9F, 0.4F, 0.4F, 0.4F);
            } else {
               this.setColor(1.0F, 1.0F, 1.0F, 1.0F);
            }

            super.draw();
         }
      }).setTextSimple(var2);
      var9.autoWrapOn = var8;
      var9.wrapSimple = false;
      var8.attach(var9);
      var8.setPos(2.0F, 2.0F + var6.getHeight() + 2.0F, 0.0F);
      GUITileButtonDesc var7;
      (var7 = new GUITileButtonDesc(this.getState(), (float)this.tileWidth, (float)this.tileHeight)).onInit();
      var7.getContent().attach(var8);
      var7.getContent().attach(var6);
      this.getTiles().add(var7);
      this.tileDirty = true;
      var7.button = var6;
      var7.descriptionText = var9;
      return var7;
   }

   public void addTile(GUIElement var1, Object var2) {
      GUITileParam var3;
      (var3 = new GUITileParam(this.getState(), (float)this.tileWidth, (float)this.tileHeight, var2)).onInit();
      var3.getContent().attach(var1);
      this.getTiles().add(var3);
      this.tileDirty = true;
   }

   public float getWidth() {
      return (float)this.width;
   }

   public float getHeight() {
      return (float)this.height;
   }

   public void clear() {
      for(int var1 = 0; var1 < this.getTiles().size(); ++var1) {
         ((GUITileParam)this.getTiles().get(var1)).cleanUp();
      }

      this.getTiles().clear();
      this.tileDirty = true;
   }

   public List getTiles() {
      return this.tiles;
   }
}
