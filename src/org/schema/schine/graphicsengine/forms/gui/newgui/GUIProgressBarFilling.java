package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.input.InputState;

public class GUIProgressBarFilling extends GUIFilledArea {
   public GUIProgressBarFilling(InputState var1, int var2, int var3) {
      super(var1, var2, var3);
   }

   protected int getLeftTop() {
      return 26;
   }

   protected int getRightTop() {
      return 27;
   }

   protected int getBottomLeft() {
      return 28;
   }

   protected int getBottomRight() {
      return 29;
   }

   protected String getCorners() {
      return "UI 8px Corners-8x8-gui-";
   }

   protected String getVertical() {
      return "UI 8px Vertical-32x1-gui-";
   }

   protected String getHorizontal() {
      return "UI 8px Horizontal-1x32-gui-";
   }

   protected String getBackground() {
      return "UI 8px Center_Progress-gui-";
   }

   protected float getTopOffset() {
      return 0.375F;
   }

   protected float getBottomOffset() {
      return 0.40625F;
   }

   protected float getLeftOffset() {
      return 0.40625F;
   }

   protected float getRightOffset() {
      return 0.4375F;
   }
}
