package org.schema.schine.graphicsengine.forms.gui.newgui;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.input.InputState;

public abstract class ScrollableTilePane extends GUIElement implements Observer {
   private final FilterController filterController;
   protected Set toAddTmp = new ObjectOpenHashSet();
   private boolean dirty = true;
   private GUIScrollablePanel scrollPanel;
   private GUIElement dependend;
   private GUITilePane tiles;
   boolean sortedYet;
   private boolean init;
   protected final int tileWidth;
   protected final int tileHeight;
   private boolean usedClose;

   public ScrollableTilePane(InputState var1, GUIElement var2, int var3, int var4) {
      super(var1);
      this.dependend = var2;
      this.filterController = new FilterController(this);
      this.scrollPanel = new GUIScrollablePanel(100.0F, 100.0F, var2, var1) {
         public boolean isActive() {
            return ScrollableTilePane.this.isActive();
         }
      };
      this.tileWidth = var3;
      this.tileHeight = var4;
      this.tiles = new GUITilePane(var1, var2, var3, var4);
      this.tiles.scrollPanel = this.scrollPanel;
   }

   protected void addTile(final Object var1, GUIElement var2, boolean var3) {
      final GUITileParam var4;
      (var4 = new GUITileParam(this.getState(), (float)this.tileWidth, (float)this.tileHeight, var1)).activationInterface = new GUIActiveInterface() {
         public boolean isActive() {
            return ScrollableTilePane.this.isActive();
         }
      };
      var2.setPos(2.0F, 2.0F, 0.0F);
      var4.attach(var2);
      if (var3) {
         this.usedClose = true;
         GUIOverlay var5;
         (var5 = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 16px-8x8-gui-"), this.getState())).setPos((float)this.tileWidth - (var5.getWidth() + 1.0F), 1.0F, 0.0F);
         var5.setCallback(new GUICallback() {
            public boolean isOccluded() {
               return !ScrollableTilePane.this.isActive();
            }

            public void callback(GUIElement var1x, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  ScrollableTilePane.this.onClosePressed(var4, var1);
               }

            }
         });
         var4.attach(var5);
      }

      this.tiles.addTile(var4, var1);
   }

   public void onClosePressed(GUITileParam var1, Object var2) {
      assert !this.usedClose : "onClosePressed() has to be overwritten if close crosses are used";

   }

   public abstract void updateListEntries(GUITilePane var1, Set var2);

   public void flagDirty() {
      this.dirty = true;
   }

   public void clear() {
      this.tiles.clear();
      this.dirty = true;
   }

   public void update(Observable var1, Object var2) {
      this.dirty = true;
   }

   public void cleanUp() {
      this.clear();
   }

   public void addBottomButton(GUICallback var1, String var2, ControllerElement.FilterRowStyle var3, ControllerElement.FilterPos var4) {
      this.filterController.addButton(var1, var2, var3, var4);
   }

   public void addDropdownFilter(GUIListFilterDropdown var1, CreateGUIElementInterface var2, ControllerElement.FilterRowStyle var3) {
      this.filterController.addDropdownFilter(var1, var2, var3);
   }

   public void addDropdownFilter(GUIListFilterDropdown var1, CreateGUIElementInterface var2, ControllerElement.FilterRowStyle var3, ControllerElement.FilterPos var4) {
      this.filterController.addDropdownFilter(var1, var2, var3, var4);
   }

   public void addTextFilter(GUIListFilterText var1, String var2, ControllerElement.FilterRowStyle var3) {
      this.filterController.addTextFilter(var1, var2, var3);
   }

   public void addTextFilter(GUIListFilterText var1, String var2, ControllerElement.FilterRowStyle var3, ControllerElement.FilterPos var4) {
      this.filterController.addTextFilter(var1, var2, var3, var4);
   }

   public void addTextFilter(GUIListFilterText var1, ControllerElement.FilterRowStyle var2) {
      this.filterController.addTextFilter(var1, var2);
   }

   protected boolean isFiltered(Object var1) {
      return this.filterController.isFiltered(var1);
   }

   private void handleDirty() {
      if (this.dirty) {
         this.toAddTmp.clear();
         Collection var1 = this.getElementList();
         this.toAddTmp.addAll(var1);

         for(int var4 = 0; var4 < this.tiles.getTiles().size(); ++var4) {
            GUITileParam var2 = (GUITileParam)this.tiles.getTiles().get(var4);

            assert var2.getUserData() != null;

            Object var5 = var2.getUserData();
            boolean var3 = this.toAddTmp.remove(var5);
            if (this.isFiltered(var5) || !var3) {
               this.tiles.getTiles().remove(var4);
               --var4;
            }
         }

         Iterator var6 = this.toAddTmp.iterator();

         while(var6.hasNext()) {
            if (this.isFiltered(var6.next())) {
               var6.remove();
            }
         }

         if (!this.sortedYet && this.filterController.currentSorter != null) {
            this.filterController.currentSorter.sort(this.tiles.getTiles());
            this.sortedYet = true;
         }

         this.updateListEntries(this.tiles, this.toAddTmp);
         if (this.filterController.currentSorter != null) {
            this.filterController.currentSorter.sort(this.tiles.getTiles());
         }

         this.dirty = false;
      }

   }

   protected abstract Collection getElementList();

   public void onInit() {
      this.scrollPanel.setContent(this.tiles);
      this.filterController.calcInit();
      this.init = true;
   }

   public float getHeight() {
      return (float)this.filterController.filterHeightBottom + this.scrollPanel.getHeight();
   }

   public void draw() {
      assert this.init;

      this.handleDirty();
      GlUtil.glPushMatrix();
      this.transform();
      this.filterController.drawTop(0);
      this.filterController.drawContent(this.scrollPanel, 0);
      this.filterController.drawBottom(this.scrollPanel, 0);
      Iterator var1 = this.getChilds().iterator();

      while(var1.hasNext()) {
         ((AbstractSceneNode)var1.next()).draw();
      }

      if (this.isMouseUpdateEnabled()) {
         this.checkMouseInside();
      }

      GlUtil.glPopMatrix();
   }

   public float getWidth() {
      return this.scrollPanel.getWidth();
   }

   public boolean isActive() {
      return this.dependend.isActive();
   }
}
