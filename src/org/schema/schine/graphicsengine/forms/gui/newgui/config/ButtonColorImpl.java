package org.schema.schine.graphicsengine.forms.gui.newgui.config;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.forms.gui.ColorPalletteInterface;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;

public class ButtonColorImpl implements ColorPalletteInterface {
   public void setColorPallete(GUITextButton.ColorPalette var1, GUITextButton var2) {
      assert var2 != null;

      assert var1 != null;

      switch(var1) {
      case OK:
         var2.getBackgroundColor().set(ButtonColorPalette.ok);
         var2.getBackgroundColorMouseOverlay().set(ButtonColorPalette.okMouseOver);
         var2.getBackgroundColorMouseOverlayPressed().set(ButtonColorPalette.okPressed);
         var2.getColorText().set(ButtonColorPalette.okText);
         var2.getColorTextPressed().set(ButtonColorPalette.okTextPressed);
         var2.getColorTextMouseOver().set(ButtonColorPalette.okTextMouseOver);
         return;
      case CANCEL:
         var2.getBackgroundColor().set(ButtonColorPalette.cancel);
         var2.getBackgroundColorMouseOverlay().set(ButtonColorPalette.cancelMouseOver);
         var2.getBackgroundColorMouseOverlayPressed().set(ButtonColorPalette.cancelPressed);
         var2.getColorText().set(ButtonColorPalette.cancelText);
         var2.getColorTextPressed().set(ButtonColorPalette.cancelTextPressed);
         var2.getColorTextMouseOver().set(ButtonColorPalette.cancelTextMouseOver);
         return;
      case FRIENDLY:
         var2.getBackgroundColor().set(ButtonColorPalette.friendly);
         var2.getBackgroundColorMouseOverlay().set(ButtonColorPalette.friendlyMouseOver);
         var2.getBackgroundColorMouseOverlayPressed().set(ButtonColorPalette.friendlyPressed);
         var2.getColorText().set(ButtonColorPalette.friendlyText);
         var2.getColorTextPressed().set(ButtonColorPalette.friendlyTextPressed);
         var2.getColorTextMouseOver().set(ButtonColorPalette.friendlyTextMouseOver);
         return;
      case HOSTILE:
         var2.getBackgroundColor().set(ButtonColorPalette.hostile);
         var2.getBackgroundColorMouseOverlay().set(ButtonColorPalette.hostileMouseOver);
         var2.getBackgroundColorMouseOverlayPressed().set(ButtonColorPalette.hostilePressed);
         var2.getColorText().set(ButtonColorPalette.hostileText);
         var2.getColorTextPressed().set(ButtonColorPalette.hostileTextPressed);
         var2.getColorTextMouseOver().set(ButtonColorPalette.hostileTextMouseOver);
         return;
      case TRANSPARENT:
         var2.getBackgroundColor().set(new Vector4f(0.0F, 0.0F, 0.0F, 0.0F));
         var2.getBackgroundColorMouseOverlay().set(new Vector4f(0.0F, 0.0F, 0.0F, 0.0F));
         var2.getBackgroundColorMouseOverlayPressed().set(new Vector4f(0.0F, 0.0F, 0.0F, 0.0F));
         var2.getColorText().set(new Vector4f(0.6F, 0.6F, 0.6F, 1.0F));
         var2.getColorTextPressed().set(new Vector4f(0.95F, 0.95F, 1.0F, 1.0F));
         var2.getColorTextMouseOver().set(new Vector4f(0.9F, 0.9F, 0.9F, 1.0F));
         return;
      case NEUTRAL:
         var2.getBackgroundColor().set(ButtonColorPalette.neutral);
         var2.getBackgroundColorMouseOverlay().set(ButtonColorPalette.neutralMouseOver);
         var2.getBackgroundColorMouseOverlayPressed().set(ButtonColorPalette.neutralPressed);
         var2.getColorText().set(ButtonColorPalette.neutralText);
         var2.getColorTextPressed().set(ButtonColorPalette.neutralTextPressed);
         var2.getColorTextMouseOver().set(ButtonColorPalette.neutralTextMouseOver);
         return;
      case TUTORIAL:
         var2.getBackgroundColor().set(ButtonColorPalette.tutorial);
         var2.getBackgroundColorMouseOverlay().set(ButtonColorPalette.tutorialMouseOver);
         var2.getBackgroundColorMouseOverlayPressed().set(ButtonColorPalette.tutorialPressed);
         var2.getColorText().set(ButtonColorPalette.tutorialText);
         var2.getColorTextPressed().set(ButtonColorPalette.tutorialTextPressed);
         var2.getColorTextMouseOver().set(ButtonColorPalette.tutorialTextMouseOver);
      default:
      }
   }
}
