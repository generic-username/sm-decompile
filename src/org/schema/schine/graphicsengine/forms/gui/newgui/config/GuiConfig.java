package org.schema.schine.graphicsengine.forms.gui.newgui.config;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.common.XMLTools;
import org.schema.common.config.ConfigParserException;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.schine.resource.FileExt;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public abstract class GuiConfig {
   public static final String configPath;

   public static void load(String var0) throws SAXException, IOException, ParserConfigurationException, IllegalArgumentException, IllegalAccessException, ConfigParserException {
      loadPath(configPath + var0);
   }

   private static void loadPath(String var0) throws SAXException, IOException, ParserConfigurationException, IllegalArgumentException, IllegalAccessException, ConfigParserException {
      ObjectArrayList var1;
      (var1 = new ObjectArrayList(10)).add(new ButtonColorPalette());
      var1.add(new ListColorPalette());
      var1.add(new ChatColorPalette());
      var1.add(new PlayerStatusColorPalette());
      var1.add(new GuiDateFormats());
      var1.add(new MainWindowFramePalette());

      assert (new FileExt(var0)).exists();

      Document var3 = XMLTools.loadXML(new FileExt(var0));

      for(int var2 = 0; var2 < var1.size(); ++var2) {
         ((GuiConfig)var1.get(var2)).parse(var3);
      }

   }

   protected abstract String getTag();

   public void parse(Document var1) throws IllegalArgumentException, IllegalAccessException, ConfigParserException {
      NodeList var24 = var1.getDocumentElement().getChildNodes();
      Field[] var2 = this.getClass().getDeclaredFields();
      boolean var3 = false;
      ObjectOpenHashSet var4 = new ObjectOpenHashSet();

      int var8;
      for(int var5 = 0; var5 < var24.getLength(); ++var5) {
         Node var6;
         if ((var6 = var24.item(var5)).getNodeType() == 1 && var6.getNodeName().toLowerCase(Locale.ENGLISH).equals(this.getTag().toLowerCase(Locale.ENGLISH))) {
            NodeList var7 = var6.getChildNodes();
            var3 = true;

            for(var8 = 0; var8 < var7.getLength(); ++var8) {
               Node var9;
               if ((var9 = var7.item(var8)).getNodeType() == 1) {
                  if (!var9.getNodeName().toLowerCase(Locale.ENGLISH).equals("basicvalues")) {
                     throw new ConfigParserException("tag \"" + var6.getNodeName() + " -> " + var9.getNodeName() + "\" unknown in this context (has to be either \"BasicValues\" or \"Combinable\")");
                  }

                  NodeList var10 = var9.getChildNodes();

                  for(int var11 = 0; var11 < var10.getLength(); ++var11) {
                     Node var12;
                     if ((var12 = var10.item(var11)).getNodeType() == 1) {
                        boolean var13 = false;
                        boolean var14 = false;
                        Field[] var15 = var2;
                        int var16 = var2.length;

                        for(int var17 = 0; var17 < var16; ++var17) {
                           Field var18;
                           (var18 = var15[var17]).setAccessible(true);
                           ConfigurationElement var19;
                           if ((var19 = (ConfigurationElement)var18.getAnnotation(ConfigurationElement.class)) != null) {
                              var13 = true;
                              if (var19.name().toLowerCase(Locale.ENGLISH).equals(var12.getNodeName().toLowerCase(Locale.ENGLISH))) {
                                 try {
                                    if (var18.getType() == Boolean.TYPE) {
                                       var18.setBoolean(this, Boolean.parseBoolean(var12.getTextContent()));
                                       var14 = true;
                                    } else if (var18.getType() == Integer.TYPE) {
                                       var18.setInt(this, Integer.parseInt(var12.getTextContent()));
                                       var14 = true;
                                    } else if (var18.getType() == Short.TYPE) {
                                       var18.setShort(this, Short.parseShort(var12.getTextContent()));
                                       var14 = true;
                                    } else if (var18.getType() == Byte.TYPE) {
                                       var18.setByte(this, Byte.parseByte(var12.getTextContent()));
                                       var14 = true;
                                    } else if (var18.getType() == Float.TYPE) {
                                       var18.setFloat(this, Float.parseFloat(var12.getTextContent()));
                                       var14 = true;
                                    } else if (var18.getType() == Double.TYPE) {
                                       var18.setDouble(this, Double.parseDouble(var12.getTextContent()));
                                       var14 = true;
                                    } else if (var18.getType() == Long.TYPE) {
                                       var18.setLong(this, Long.parseLong(var12.getTextContent()));
                                       var14 = true;
                                    } else {
                                       String[] var29;
                                       if (var18.getType().equals(Vector2f.class)) {
                                          if ((var29 = var12.getTextContent().split(",")) == null || var29.length != 2) {
                                             throw new ConfigParserException("Must be 2 int values seperated by comma: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType() + "; ");
                                          }

                                          try {
                                             Vector2f var32 = new Vector2f(Float.parseFloat(var29[0].trim()), Float.parseFloat(var29[1].trim()));
                                             var18.set(this, var32);
                                             var14 = true;
                                          } catch (NumberFormatException var22) {
                                             var22.printStackTrace();
                                             throw new ConfigParserException("Values must be numbers: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType());
                                          }
                                       } else if (var18.getType().equals(Vector4i.class)) {
                                          if ((var29 = var12.getTextContent().split(",")) == null || var29.length != 4) {
                                             throw new ConfigParserException("Must be 4 int values seperated by comma: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType());
                                          }

                                          try {
                                             Vector4i var31 = new Vector4i(Integer.parseInt(var29[0].trim()), Integer.parseInt(var29[1].trim()), Integer.parseInt(var29[2].trim()), Integer.parseInt(var29[3].trim()));
                                             var18.set(this, var31);
                                             var14 = true;
                                          } catch (NumberFormatException var21) {
                                             var21.printStackTrace();
                                             throw new ConfigParserException("Values must be numbers: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType());
                                          }
                                       } else if (var18.getType().equals(Vector4f.class)) {
                                          if ((var29 = var12.getTextContent().split(",")) == null || var29.length != 4) {
                                             throw new ConfigParserException("Must be 4 int values seperated by comma: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType());
                                          }

                                          try {
                                             Vector4f var30 = new Vector4f(Float.parseFloat(var29[0].trim()) / 255.0F, Float.parseFloat(var29[1].trim()) / 255.0F, Float.parseFloat(var29[2].trim()) / 255.0F, Float.parseFloat(var29[3].trim()) / 255.0F);
                                             var18.set(this, var30);
                                             var14 = true;
                                          } catch (NumberFormatException var20) {
                                             var20.printStackTrace();
                                             throw new ConfigParserException("Values must be numbers: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType());
                                          }
                                       } else if (var18.getType().equals(DateFormat.class)) {
                                          var18.set(this, new SimpleDateFormat(var12.getTextContent()));
                                          var14 = true;
                                       } else {
                                          if (!var18.getType().equals(String.class)) {
                                             throw new ConfigParserException("Cannot parse field: " + var18.getName() + "; " + var18.getType());
                                          }

                                          var18.set(this, var12.getTextContent());
                                          var14 = true;
                                       }
                                    }
                                 } catch (NumberFormatException var23) {
                                    throw new ConfigParserException("Cannot parse field: " + var18.getName() + "; " + var18.getType() + "; with " + var12.getTextContent(), var23);
                                 }
                              }

                              if (var14) {
                                 var4.add(var18);
                                 break;
                              }
                           }
                        }

                        if (var13 && !var14) {
                           throw new ConfigParserException(var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": No appropriate field found for tag: " + var12.getNodeName());
                        }
                     }
                  }
               }
            }
         }
      }

      if (!var3) {
         throw new ConfigParserException("Tag \"" + this.getTag() + "\" not found in configuation. Please create it (case insensitive)");
      } else {
         this.getClass().getAnnotations();
         Field[] var25 = var2;
         int var26 = var2.length;

         for(var8 = 0; var8 < var26; ++var8) {
            Field var27;
            (var27 = var25[var8]).setAccessible(true);
            ConfigurationElement var28;
            if ((var28 = (ConfigurationElement)var27.getAnnotation(ConfigurationElement.class)) != null && !var4.contains(var27)) {
               throw new ConfigParserException("virtual field '" + var27.getName() + "' ('" + var28.name() + "') not found. Please define a tag \"" + var28.name() + "\" inside the <BasicValues> of \"" + this.getTag() + "\"");
            }
         }

      }
   }

   static {
      configPath = "." + File.separator + "data" + File.separator + "config" + File.separator + "gui" + File.separator;
   }
}
