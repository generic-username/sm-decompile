package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.input.InputState;

public class GUIPlainWindow extends GUIAbstractPlainWindow {
   public static final int INNER_FB_INSET = 29;
   public int xInnerOffset = 8;
   public int yInnerOffset = 8;
   public float innerHeightSubstraction = 64.0F;
   public float innerWidthSubstraction = 32.0F;
   boolean init = false;
   private int topDist = 0;
   public int insetCornerDistTop = 0;
   public int insetCornerDistBottom = 0;
   private boolean closable;

   public GUIPlainWindow(InputState var1, int var2, int var3, String var4) {
      super(var1, var2, var3, var4);
   }

   public GUIPlainWindow(InputState var1, int var2, int var3, int var4, int var5, String var6) {
      super(var1, var2, var3, var4, var5, var6);
   }

   public void onInit() {
      if (!this.init) {
         super.onInit();
         this.init = true;
      }
   }

   public int getTopDist() {
      return this.topDist;
   }

   public void setTopDist(int var1) {
      this.topDist = var1;
   }

   public int getInnerCornerDistX() {
      return 0;
   }

   public int getInnerCornerTopDistY() {
      return this.insetCornerDistTop;
   }

   public int getInnerCornerBottomDistY() {
      return this.insetCornerDistBottom;
   }

   public int getInnerHeigth() {
      return (int)(this.getHeight() - this.innerHeightSubstraction);
   }

   public int getInnerWidth() {
      return (int)(this.getWidth() - this.innerWidthSubstraction);
   }

   public int getInnerOffsetX() {
      return this.xInnerOffset;
   }

   public int getInnerOffsetY() {
      return this.yInnerOffset;
   }

   public int getInset() {
      return 29;
   }

   protected void drawContent(GUIContentPane var1) {
      var1.draw();
      if (this.isMouseUpdateEnabled()) {
         this.checkMouseInside();
      }

      if (this.closable) {
         this.drawCross(-30, 16);
      }

   }

   public void setClosable(boolean var1) {
      this.closable = var1;
   }
}
