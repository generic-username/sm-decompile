package org.schema.schine.graphicsengine.forms.gui.newgui;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.vbogui.VBOAccess;
import org.schema.schine.graphicsengine.texture.Texture;
import org.schema.schine.input.InputState;

public class GUITexDrawableArea extends GUIElement {
   private static final Int2ObjectOpenHashMap textureToMapping = new Int2ObjectOpenHashMap(1024);
   private static final int MAX_TEXTMAPPINGS_PER_TEXTURE = 256;
   protected static boolean USE_DISPLAYLIST;
   public float xOffset;
   public float yOffset;
   protected Vector4f color;
   protected Texture texture;
   private int height;
   private int width;
   protected boolean sizeChanged = true;
   protected int diaplayListIndex;
   private VBOAccess currentTestAccess;

   public GUITexDrawableArea(InputState var1, Texture var2, float var3, float var4) {
      super(var1);
      this.texture = var2;
      this.width = var2.getWidth();
      this.height = var2.getHeight();
      this.xOffset = var3;
      this.yOffset = var4;

      assert this.getState() != null;

   }

   public void setOffset(float var1, float var2) {
      this.xOffset = var1;
      this.yOffset = var2;
   }

   public void cleanUp() {
      if (this.diaplayListIndex != 0) {
         GL11.glDeleteLists(this.diaplayListIndex, 1);
      }

   }

   public void drawRaw() {
      if (this.getWidth() > 0.0F && this.getHeight() > 0.0F) {
         if (this.sizeChanged && USE_DISPLAYLIST) {
            this.genTListOld();
         }

         VBOAccess var1 = null;
         if (!USE_DISPLAYLIST) {
            var1 = this.gen();
         }

         GlUtil.glTranslatef(this.getPos().x, this.getPos().y, this.getPos().z);
         if (GlUtil.isTextureChaching()) {
            GlUtil.loadTextureCached(this.texture.getTextureId());
         } else {
            this.texture.attach(0);
         }

         if (this.color != null) {
            GlUtil.glColor4f(this.color);
         }

         if (this.isRenderable()) {
            if (USE_DISPLAYLIST) {
               GL11.glCallList(this.diaplayListIndex);
            } else {
               var1.render();
            }
         }

         if (this.isMouseUpdateEnabled()) {
            this.checkMouseInside();
         }

         GlUtil.glTranslatef(-this.getPos().x, -this.getPos().y, -this.getPos().z);
      }
   }

   public void draw() {
      if (this.getWidth() > 0.0F && this.getHeight() > 0.0F) {
         if (this.sizeChanged && USE_DISPLAYLIST) {
            this.genTListOld();
         }

         VBOAccess var1 = null;
         if (!USE_DISPLAYLIST) {
            var1 = this.gen();
         }

         GlUtil.glPushMatrix();
         this.transform();
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         GlUtil.glEnable(2903);
         GlUtil.glDisable(2929);
         GlUtil.glDisable(2896);
         this.texture.attach(0);
         if (this.color != null) {
            GlUtil.glColor4f(this.color);
         }

         if (this.isRenderable()) {
            if (USE_DISPLAYLIST) {
               GL11.glCallList(this.diaplayListIndex);
            } else {
               var1.render();
            }
         }

         this.texture.detach();
         GlUtil.glDisable(2903);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GlUtil.glDisable(3042);
         GlUtil.glEnable(2929);
         if (this.isMouseUpdateEnabled()) {
            this.checkMouseInside();
         }

         GlUtil.glPopMatrix();
      }
   }

   public void onInit() {
      if (this.getWidth() > 0.0F && this.getHeight() > 0.0F) {
         this.gen();
      }
   }

   protected void genTListOld() {
      if (this.diaplayListIndex != 0) {
         GL11.glDeleteLists(this.diaplayListIndex, 1);
      }

      this.diaplayListIndex = GL11.glGenLists(1);
      GL11.glNewList(this.diaplayListIndex, 4864);
      GL11.glBegin(7);
      GL11.glTexCoord2f(this.xOffset, this.yOffset);
      GL11.glVertex2f(0.0F, 0.0F);
      GL11.glTexCoord2f(this.xOffset, this.yOffset + this.getHeight() / (float)this.texture.getHeight());
      GL11.glVertex2f(0.0F, this.getHeight());
      GL11.glTexCoord2f(this.xOffset + this.getWidth() / (float)this.texture.getWidth(), this.yOffset + this.getHeight() / (float)this.texture.getHeight());
      GL11.glVertex2f(this.getWidth(), this.getHeight());
      GL11.glTexCoord2f(this.xOffset + this.getWidth() / (float)this.texture.getWidth(), this.yOffset);
      GL11.glVertex2f(this.getWidth(), 0.0F);
      GL11.glEnd();
      GL11.glEndList();
      this.sizeChanged = false;
   }

   protected VBOAccess gen() {
      GUITexDrawableArea.Mappings var1;
      if ((var1 = (GUITexDrawableArea.Mappings)textureToMapping.get(this.texture.getTextureId())) == null) {
         var1 = new GUITexDrawableArea.Mappings();
         textureToMapping.put(this.texture.getTextureId(), var1);
      }

      if (this.currentTestAccess == null) {
         this.currentTestAccess = new VBOAccess(this.texture.getWidth(), this.texture.getHeight());
      }

      this.currentTestAccess.setFromThis(this);
      VBOAccess var2;
      if ((var2 = (VBOAccess)var1.accessMap.get(this.currentTestAccess)) != null) {
         var2.lastTouched = (long)this.getState().getNumberOfUpdate();
         return var2;
      } else {
         var2 = null;
         if (var1.accessMap.size() > 256) {
            ObjectIterator var3 = var1.accessMap.iterator();

            while(var3.hasNext()) {
               VBOAccess var4;
               if ((var4 = (VBOAccess)var3.next()).lastTouched < (long)(this.getState().getNumberOfUpdate() - 2)) {
                  var2 = var4;
                  var3.remove();
                  break;
               }
            }
         }

         boolean var5 = false;
         if (var2 == null) {
            var2 = new VBOAccess(this.texture.getWidth(), this.texture.getHeight());
            var5 = true;
         }

         var2.setFromThis(this);

         assert var2.tAccess.xOffset == this.xOffset : var2.tAccess.xOffset + "; " + this.xOffset;

         assert var2.tAccess.yOffset == this.yOffset;

         assert var2.vAccess.width == this.width;

         assert var2.vAccess.height == this.height;

         assert var2.tAccess.xOffset == this.xOffset : var2.tAccess.xOffset + "; " + this.xOffset;

         assert var2.tAccess.yOffset == this.yOffset;

         assert var2.vAccess.width == this.width;

         assert var2.vAccess.height == this.height;

         assert this.currentTestAccess.equals(var2) : "\n" + this.currentTestAccess + "; \n" + var2 + "\n" + this.width + ", " + this.height + "; " + this.xOffset + ", " + this.yOffset + "; newAc: " + var5;

         var2.generate(this);
         boolean var6 = var1.accessMap.add(var2);

         assert var6 : var2;

         var2.lastTouched = (long)this.getState().getNumberOfUpdate();
         return var2;
      }
   }

   public float getHeight() {
      return (float)this.height;
   }

   public float getWidth() {
      return (float)this.width;
   }

   public void setWidth(int var1) {
      if (this.width != var1) {
         this.sizeChanged = true;
         this.width = var1;
      }

   }

   public void setHeight(int var1) {
      if (this.height != var1) {
         this.sizeChanged = true;
         this.height = var1;
      }

   }

   public void setSpriteSubIndex(int var1, int var2, int var3) {
      int var4 = var1 % var2;
      var1 /= var2;
      float var5 = 1.0F / (float)var2;
      float var6 = 1.0F / (float)var3;
      this.xOffset = (float)var4 * var5;
      this.yOffset = (float)var1 * var6;
      this.sizeChanged = true;
   }

   public Vector4f getColor() {
      return this.color;
   }

   public void setColor(Vector4f var1) {
      this.color = var1;
   }

   public void setSizeChanged(boolean var1) {
      this.sizeChanged = var1;
   }

   static {
      USE_DISPLAYLIST = EngineSettings.GUI_USE_DISPLAY_LISTS.isOn();
   }

   class Mappings {
      private ObjectOpenHashSet accessMap;

      private Mappings() {
         this.accessMap = new ObjectOpenHashSet();
      }

      // $FF: synthetic method
      Mappings(Object var2) {
         this();
      }
   }
}
