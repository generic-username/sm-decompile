package org.schema.schine.graphicsengine.forms.gui.newgui;

import java.text.SimpleDateFormat;
import org.lwjgl.opengl.GL11;
import org.schema.common.util.StringTools;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public abstract class GUIStatisticsGraph extends GUIElement implements DropDownCallback, GUICallback {
   static SimpleDateFormat dateFormat;
   private static long timeFrameInMS;
   protected final GUITabbedContent ps;
   GUITextOverlay maxText;
   GUITextOverlay startTimeText;
   GUITextOverlay endTimeText;
   private GUIElement dependend;
   private StatisticsGraphListInterface[] e;
   private boolean[] selected;

   public GUIStatisticsGraph(InputState var1, GUITabbedContent var2, GUIElement var3, StatisticsGraphListInterface... var4) {
      super(var1);
      this.dependend = var3;
      this.e = var4;
      this.ps = var2;
      this.selected = new boolean[var4.length];
      this.maxText = new GUITextOverlay(1, 1, var1);
      this.startTimeText = new GUITextOverlay(1, 1, var1);
      this.endTimeText = new GUITextOverlay(1, 1, var1);
      this.setMouseUpdateEnabled(true);
      this.setCallback(this);
   }

   public void cleanUp() {
   }

   public void draw() {
      long var1 = 0L;
      long var3 = 0L;
      long var5 = System.currentTimeMillis();

      int var7;
      for(var7 = 0; var7 < this.e.length; ++var7) {
         if (this.e[var7].getSize() != 0) {
            long var8 = var5 - timeFrameInMS;
            var1 = Math.max(var1, this.e[var7].getMaxAplitude(var8, var5));
            if (var7 == 0) {
               var3 = this.e[var7].getAmplitudeAtIndex(0);
            }
         }
      }

      this.maxText.setTextSimple(this.formatMax(var1, var3));
      this.startTimeText.setTextSimple(dateFormat.format(var5 - timeFrameInMS));
      this.endTimeText.setTextSimple(dateFormat.format(var5));

      for(var7 = 0; var7 < this.e.length; ++var7) {
         this.draw(this.e[var7], var7, var1);
      }

      this.maxText.setPos((float)((int)(this.getWidth() / 2.0F - (float)(this.maxText.getMaxLineWidth() / 2) - 10.0F)), 0.0F, 0.0F);
      this.startTimeText.draw();
      this.endTimeText.setPos(this.getWidth() - (float)this.endTimeText.getMaxLineWidth(), 0.0F, 0.0F);
      this.endTimeText.draw();
      this.maxText.draw();
   }

   public abstract String formatMax(long var1, long var3);

   public void onInit() {
   }

   public void draw(StatisticsGraphListInterface var1, int var2, long var3) {
      if (var1.getSize() != 0) {
         GlUtil.glDisable(2896);
         GlUtil.glEnable(3042);
         GlUtil.glDisable(3553);
         GlUtil.glEnable(2903);
         GL11.glLineWidth(1.0F);
         GlUtil.glBlendFunc(770, 771);
         GlUtil.glEnable(2848);
         GL11.glHint(3154, 4354);
         long var7 = System.currentTimeMillis() - timeFrameInMS;
         int var5 = var1.getStartIndexFrom(var7);
         GlUtil.glPushMatrix();
         this.transform();
         GlUtil.glColor4f(var1.getColor());
         GL11.glBegin(3);
         this.getWidth();
         double var9 = (double)(var1.getTimeAtIndex(0) - var1.getTimeAtIndex(var5));

         int var6;
         double var13;
         for(var6 = 0; var6 < var5 + 1; ++var6) {
            double var11 = (double)(var1.getTimeAtIndex(0) - var1.getTimeAtIndex(var6));
            var13 = (double)(this.getWidth() + 40.0F) * (var11 / var9);
            GL11.glVertex2d((double)(this.getWidth() + 20.0F - (float)((int)var13)), (double)(this.getHeight() - (float)((int)(var1.getAmplitudePercentAtIndex(var6, var3) * (double)this.getHeight()))));
         }

         GL11.glEnd();
         this.selected[var2] = false;
         if (this.ps == null) {
            var6 = 0;
         } else {
            var6 = this.ps.getSelectedTab();
         }

         int var17 = this.getMouseIndex();

         for(int var12 = 0; var12 < var5 + 1; ++var12) {
            double var15;
            if (var1.isSelected(var12)) {
               GlUtil.glColor4f(var1.getColor());
               var13 = (double)(var1.getTimeAtIndex(0) - var1.getTimeAtIndex(var12));
               var15 = (double)(this.getWidth() + 40.0F) * (var13 / var9);
               GL11.glBegin(1);
               GL11.glVertex2d((double)(this.getWidth() + 20.0F - (float)((int)var15)), 0.0D);
               GL11.glVertex2d((double)(this.getWidth() + 20.0F - (float)((int)var15)), (double)this.getHeight());
               GL11.glEnd();
               this.selected[var2] = true;
            } else if (var2 == var6) {
               if (var12 == var17) {
                  GlUtil.glColor4f(0.7F, 0.7F, 0.7F, 0.7F);
                  var13 = (double)(var1.getTimeAtIndex(0) - var1.getTimeAtIndex(var12));
                  var15 = (double)(this.getWidth() + 40.0F) * (var13 / var9);
                  GL11.glBegin(1);
                  GL11.glVertex2d((double)(this.getWidth() + 20.0F - (float)((int)var15)), 0.0D);
                  GL11.glVertex2d((double)(this.getWidth() + 20.0F - (float)((int)var15)), (double)this.getHeight());
                  GL11.glEnd();
               } else {
                  GlUtil.glColor4f(0.3F, 0.3F, 0.3F, 0.3F);
                  var13 = (double)(var1.getTimeAtIndex(0) - var1.getTimeAtIndex(var12));
                  var15 = (double)(this.getWidth() + 40.0F) * (var13 / var9);
                  GL11.glBegin(1);
                  GL11.glVertex2d((double)(this.getWidth() + 20.0F - (float)((int)var15)), 0.0D);
                  GL11.glVertex2d((double)(this.getWidth() + 20.0F - (float)((int)var15)), (double)this.getHeight());
                  GL11.glEnd();
               }
            }
         }

         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         this.checkMouseInside();
         GlUtil.glPopMatrix();
         GlUtil.glDisable(3042);
         GlUtil.glDisable(2903);
      }
   }

   public float getHeight() {
      return this.dependend.getHeight();
   }

   public float getWidth() {
      return this.dependend.getWidth();
   }

   private int getMouseIndex() {
      int var1;
      if (this.ps == null) {
         var1 = 0;
      } else {
         var1 = this.ps.getSelectedTab();
      }

      long var2 = System.currentTimeMillis();
      long var4 = Math.max(this.e[var1].getTimeAtIndex(this.e[var1].getSize() - 1), var2 - timeFrameInMS);
      long var6 = var2;
      int var12 = this.e[var1].getStartIndexFrom(var4);
      this.e[var1].getTimeAtIndex(0);
      this.e[var1].getTimeAtIndex(var12);
      double var8 = (double)this.getRelMousePos().x / ((double)this.getWidth() + 40.0D) * (double)(var6 - var4);
      long var10 = var4 + (long)var8;
      return this.e[var1].getClosestIndexFrom(var10);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse() && this.ps != null) {
         this.e[this.ps.getSelectedTab()].select(this.getMouseIndex());
         this.e[this.ps.getSelectedTab()].notifyGUI();
      }

   }

   public boolean isOccluded() {
      return false;
   }

   public boolean[] isSelected() {
      return this.selected;
   }

   public void onSelectionChanged(GUIListElement var1) {
      if (var1.getContent().getUserPointer() != null && var1.getContent().getUserPointer() instanceof Long) {
         timeFrameInMS = (Long)var1.getContent().getUserPointer();
      }

   }

   static {
      dateFormat = StringTools.getSimpleDateFormat(Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_FORMS_GUI_NEWGUI_GUISTATISTICSGRAPH_0, "HH:mm:ss");
      timeFrameInMS = 120000L;
   }
}
