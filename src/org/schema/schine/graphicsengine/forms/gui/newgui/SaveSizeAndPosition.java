package org.schema.schine.graphicsengine.forms.gui.newgui;

import javax.vecmath.Vector3f;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class SaveSizeAndPosition implements TagSerializable {
   private final Vector3f posS = new Vector3f();
   public String id;
   public boolean newPanel;
   private int heightS;
   private int widthS;
   public boolean expanded = false;
   public boolean hidden;

   public SaveSizeAndPosition() {
   }

   public SaveSizeAndPosition(String var1) {
      this.id = var1;
      this.newPanel = true;
   }

   public void applyTo(GUIResizableGrabbableWindow var1) {
      var1.setWidth((float)this.widthS);
      var1.setHeight((float)this.heightS);
      var1.getPos().set(this.posS);
      if (var1 instanceof GUIExpandableWindow) {
         ((GUIExpandableWindow)var1).expanded = this.expanded;
      }

   }

   public void setFrom(float var1, float var2, Vector3f var3, boolean var4) {
      this.widthS = (int)var1;
      this.heightS = (int)var2;
      this.posS.set(var3);
      this.expanded = var4;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.widthS = var2[0].getInt();
      this.heightS = var2[1].getInt();
      this.posS.set(var2[2].getVector3f());
      this.id = var2[3].getString();
      this.expanded = var2.length > 4 && var2[4].getType() == Tag.Type.BYTE && var2[4].getByte() != 0;
      this.hidden = var2.length > 5 && var2[5].getType() == Tag.Type.BYTE && var2[5].getByte() != 0;
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.INT, (String)null, this.widthS), new Tag(Tag.Type.INT, (String)null, this.heightS), new Tag(Tag.Type.VECTOR3f, (String)null, this.posS), new Tag(Tag.Type.STRING, (String)null, this.id), new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.expanded ? 1 : 0))), new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.hidden ? 1 : 0))), FinishTag.INST});
   }
}
