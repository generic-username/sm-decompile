package org.schema.schine.graphicsengine.forms.gui.newgui.settingsnew;

import java.util.ArrayList;
import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUISettingsElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.SettingsInterface;
import org.schema.schine.network.client.ClientState;

public class GUISettingSelectorNew extends GUISettingsElement implements GUICallback {
   private GUITextOverlay settingName;
   private GUIOverlay leftArrow;
   private GUIOverlay rightArrow;
   private boolean checked;
   private SettingsInterface setting;
   private boolean init;

   public GUISettingSelectorNew(ClientState var1, int var2, int var3, SettingsInterface var4) {
      super(var1);
      this.checked = false;
      this.setMouseUpdateEnabled(true);
      this.setCallback(this);
      this.setting = var4;
      this.leftArrow = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "tools-16x16-gui-"), this.getState());
      this.rightArrow = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "tools-16x16-gui-"), this.getState());
      this.settingName = new GUITextOverlay(var2, var3, FontLibrary.getBoldArialWhite14(), this.getState());
   }

   public GUISettingSelectorNew(ClientState var1, SettingsInterface var2) {
      this(var1, 140, 30, var2);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.getEventButtonState() && var2.getEventButton() == 0) {
         this.checked = !this.checked;

         try {
            this.setting.switchSetting();
            return;
         } catch (StateParameterNotFoundException var3) {
            var3.printStackTrace();
            GLFrame.processErrorDialogException(var3, this.getState());
         }
      }

   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.settingName.draw();
      this.leftArrow.draw();
      this.rightArrow.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.settingName.setText(new ArrayList());
      this.settingName.getText().add(this.setting.getCurrentState().toString());
      this.settingName.onInit();
      Vector3f var10000 = this.settingName.getPos();
      var10000.y += 8.0F;
      this.leftArrow.setMouseUpdateEnabled(true);
      this.rightArrow.setMouseUpdateEnabled(true);
      var10000 = this.leftArrow.getPos();
      var10000.y += 3.0F;
      var10000 = this.rightArrow.getPos();
      var10000.y += 3.0F;
      this.leftArrow.setCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.getEventButtonState() && var2.getEventButton() == 0) {
               GUISettingSelectorNew.this.checked = !GUISettingSelectorNew.this.checked;

               try {
                  GUISettingSelectorNew.this.setting.switchSettingBack();
                  GUISettingSelectorNew.this.setting.onSwitchedSetting(GUISettingSelectorNew.this.getState());
                  GUISettingSelectorNew.this.updateText();
                  return;
               } catch (StateParameterNotFoundException var3) {
                  var3.printStackTrace();
                  GLFrame.processErrorDialogException(var3, GUISettingSelectorNew.this.getState());
               }
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.rightArrow.setCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.getEventButtonState() && var2.getEventButton() == 0) {
               GUISettingSelectorNew.this.checked = !GUISettingSelectorNew.this.checked;

               try {
                  GUISettingSelectorNew.this.setting.switchSetting();
                  GUISettingSelectorNew.this.setting.onSwitchedSetting(GUISettingSelectorNew.this.getState());
                  GUISettingSelectorNew.this.updateText();
                  return;
               } catch (StateParameterNotFoundException var3) {
                  var3.printStackTrace();
                  GLFrame.processErrorDialogException(var3, GUISettingSelectorNew.this.getState());
               }
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.leftArrow.setSpriteSubIndex(21);
      this.rightArrow.setSpriteSubIndex(20);
      this.settingName.getPos().x = this.leftArrow.getWidth();
      this.rightArrow.getPos().x = this.leftArrow.getWidth() + this.settingName.getWidth();
      this.init = true;
   }

   protected void doOrientation() {
   }

   public float getHeight() {
      return 30.0F;
   }

   public float getWidth() {
      return this.settingName.getWidth() + this.leftArrow.getWidth() + this.rightArrow.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public boolean isOccluded() {
      return false;
   }

   private void updateText() {
      this.settingName.getText().set(0, this.setting.getCurrentState());
   }
}
