package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollableInterface;
import org.schema.schine.input.InputState;

public class GUINewScrollBar extends GUIAbstractNewScrollBar {
   public GUINewScrollBar(InputState var1, GUIScrollableInterface var2, int var3, boolean var4) {
      super(var1, var2, var3, var4);
   }

   public GUINewScrollBar(InputState var1, GUIScrollableInterface var2, boolean var3) {
      super(var1, var2, var3);
   }

   protected boolean isLaneRepeatable() {
      return false;
   }

   protected String getLaneTex() {
      return "UI 16px-8x8-gui-";
   }

   protected String getStartEndTex() {
      return "UI 16px-8x8-gui-";
   }

   protected String getBarTex() {
      return "UI 32px Corners-8x8-gui-";
   }

   protected int getVerticalStart() {
      return 9;
   }

   protected int getVerticalEnd() {
      return 11;
   }

   protected int getVerticalLane() {
      return 10;
   }

   protected int getVerticalBar() {
      return 20;
   }

   protected int getHorizontalStart() {
      return 12;
   }

   protected int getHorizontalEnd() {
      return 14;
   }

   protected int getHorizontalLane() {
      return 13;
   }

   protected int getHorizontalBar() {
      return 36;
   }

   protected boolean hasSeperateArrows() {
      return false;
   }

   protected GUIOverlay getSeperateArrowBottom() {
      return null;
   }

   protected GUIOverlay getSeperateArrowTop() {
      return null;
   }

   protected int getSpriteSize() {
      return 16;
   }

   protected float getBarHorizontalYSubstract() {
      return 9.0F;
   }

   protected float getBarVerticalXSubstract() {
      return 7.0F;
   }
}
