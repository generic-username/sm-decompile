package org.schema.schine.graphicsengine.forms.gui.newgui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;

public class FilterController {
   int filterHeightTop;
   public int filterHeightBottom;
   public GuiListSorter currentSorter;
   private final List bottomElements = new ObjectArrayList();
   private final List topElements = new ObjectArrayList();
   private final Set generalListFilter = new ObjectOpenHashSet();
   private final GUIElement g;

   public FilterController(GUIElement var1) {
      this.g = var1;
   }

   public void addButton(GUICallback var1, String var2, ControllerElement.FilterRowStyle var3, ControllerElement.FilterPos var4) {
      this.addButton(var1, var2, GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, var3, var4);
   }

   public void addButton(GUICallback var1, String var2, final GUIHorizontalArea.HButtonType var3, ControllerElement.FilterRowStyle var4, ControllerElement.FilterPos var5) {
      final GUITextOverlay var6;
      final int var7 = (var6 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium15(), this.g.getState())).getFont().getWidth(var2);
      var6.setTextSimple(var2);
      GUIHorizontalArea var9;
      (var9 = new GUIHorizontalArea(this.g.getState(), var3, 10) {
         public void draw() {
            if (this.leftDependentHalf) {
               this.setWidth((int)(FilterController.this.g.getWidth() / 2.0F));
               this.getPos().x = 0.0F;
            } else if (this.rightDependentHalf) {
               this.setWidth((int)(FilterController.this.g.getWidth() / 2.0F));
               this.getPos().x = (float)((int)(FilterController.this.g.getWidth() / 2.0F));
            } else {
               this.setWidth(FilterController.this.g.getWidth());
               this.getPos().x = 0.0F;
            }

            boolean var1 = FilterController.this.g.isActive();
            GUIHorizontalArea.HButtonType var2 = GUIHorizontalArea.HButtonType.getType(var3, this.isInside(), var1, false);
            this.setType(var2);
            var6.setPos((float)((int)(this.getWidth() / 2.0F - (float)(var7 / 2))), 4.0F, 0.0F);
            this.setMouseUpdateEnabled(var1);
            super.draw();
         }
      }).attach(var6);
      var9.setCallback(var1);
      ControllerElement var8;
      (var8 = new ControllerElement(var9)).setMode(var4);
      var8.setPos(var5);
      if (var5 == ControllerElement.FilterPos.BOTTOM) {
         this.bottomElements.add(var8);
      } else {
         this.topElements.add(var8);
      }

      if (var4 == ControllerElement.FilterRowStyle.LEFT) {
         var9.leftDependentHalf = true;
      } else {
         if (var4 == ControllerElement.FilterRowStyle.RIGHT) {
            var9.rightDependentHalf = true;
         }

      }
   }

   public void addDropdownFilter(GUIListFilterDropdown var1, CreateGUIElementInterface var2, ControllerElement.FilterRowStyle var3) {
      this.addDropdownFilter(var1, var2, var3, ControllerElement.FilterPos.BOTTOM);
   }

   public void addDropdownSorter(final GUIListSorterDropdown var1, CreateGUIElementInterface var2, ControllerElement.FilterRowStyle var3, ControllerElement.FilterPos var4) {
      GUIElement[] var5;
      GUIElement var6;
      int var7;
      Comparator var8;
      if ((var6 = var2.createNeutral()) != null) {
         (var5 = new GUIElement[var1.values.length + 1])[0] = var6;

         for(var7 = 0; var7 < var1.values.length; ++var7) {
            var8 = var1.values[var7];
            var5[var7 + 1] = var2.create(var8);
         }
      } else {
         var5 = new GUIElement[var1.values.length];

         for(var7 = 0; var7 < var1.values.length; ++var7) {
            var8 = var1.values[var7];
            var5[var7] = var2.create(var8);
         }
      }

      GUIDropDownList var9;
      (var9 = new GUIDropDownList(this.g.getState(), 100, 24, 108, new DropDownCallback() {
         public void onSelectionChanged(GUIListElement var1x) {
            if (var1x.getContent().getUserPointer() == null) {
               FilterController.this.currentSorter = var1;
               ((Observer)FilterController.this.g).update((Observable)null, (Object)null);
            } else {
               var1.setSorter((Comparator)var1x.getContent().getUserPointer());
               ((Observer)FilterController.this.g).update((Observable)null, (Object)null);
               FilterController.this.currentSorter = var1;
            }
         }
      }, var5)).dependend = this.g;
      if (var6 == null) {
         var1.setSorter((Comparator)var5[0].getUserPointer());
      }

      var9.onInit();
      ControllerElement var10;
      (var10 = new ControllerElement(var9)).setMode(var3);
      var10.setPos(var4);
      if (var4 == ControllerElement.FilterPos.BOTTOM) {
         this.bottomElements.add(var10);
      } else {
         this.topElements.add(var10);
      }

      if (var3 == ControllerElement.FilterRowStyle.LEFT) {
         var9.leftDependentHalf = true;
      } else {
         if (var3 == ControllerElement.FilterRowStyle.RIGHT) {
            var9.rightDependentHalf = true;
         }

      }
   }

   public void addDropdownFilter(final GUIListFilterDropdown var1, CreateGUIElementInterface var2, ControllerElement.FilterRowStyle var3, ControllerElement.FilterPos var4) {
      GUIElement[] var5;
      GUIElement var6;
      int var7;
      Object var8;
      if ((var6 = var2.createNeutral()) != null) {
         (var5 = new GUIElement[var1.values.length + 1])[0] = var6;

         for(var7 = 0; var7 < var1.values.length; ++var7) {
            var8 = var1.values[var7];
            var5[var7 + 1] = var2.create(var8);
         }
      } else {
         var5 = new GUIElement[var1.values.length];

         for(var7 = 0; var7 < var1.values.length; ++var7) {
            var8 = var1.values[var7];
            var5[var7] = var2.create(var8);
         }
      }

      GUIDropDownList var9;
      (var9 = new GUIDropDownList(this.g.getState(), 100, 24, 108, new DropDownCallback() {
         public void onSelectionChanged(GUIListElement var1x) {
            if (var1x.getContent().getUserPointer() == null) {
               FilterController.this.generalListFilter.remove(var1);
               ((Observer)FilterController.this.g).update((Observable)null, (Object)null);
            } else {
               var1.setFilter(var1x.getContent().getUserPointer());
               ((Observer)FilterController.this.g).update((Observable)null, (Object)null);
               FilterController.this.generalListFilter.add(var1);
            }
         }
      }, var5)).dependend = this.g;
      if (var6 == null) {
         var1.setFilter(var5[0].getUserPointer());
      }

      var9.onInit();
      ControllerElement var10;
      (var10 = new ControllerElement(var9)).setMode(var3);
      var10.setPos(var4);
      if (var4 == ControllerElement.FilterPos.BOTTOM) {
         this.bottomElements.add(var10);
      } else {
         this.topElements.add(var10);
      }

      if (var3 == ControllerElement.FilterRowStyle.LEFT) {
         var9.leftDependentHalf = true;
      } else {
         if (var3 == ControllerElement.FilterRowStyle.RIGHT) {
            var9.rightDependentHalf = true;
         }

      }
   }

   public void addTextFilter(GUIListFilterText var1, String var2, ControllerElement.FilterRowStyle var3) {
      this.addTextFilter(var1, var2, var3, ControllerElement.FilterPos.BOTTOM);
   }

   public void addTextFilter(final GUIListFilterText var1, String var2, ControllerElement.FilterRowStyle var3, ControllerElement.FilterPos var4) {
      GUISearchBar var5 = new GUISearchBar(this.g.getState(), var2 != null ? var2 : "SEARCH", this.g, new TextCallback() {
         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public void onFailedTextCheck(String var1) {
         }

         public void onTextEnter(String var1, boolean var2, boolean var3) {
         }

         public void newLine() {
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1x) {
            if (var1x.trim().isEmpty()) {
               FilterController.this.generalListFilter.remove(var1);
            } else {
               FilterController.this.generalListFilter.add(var1);
               var1.setFilter(var1x);
            }

            ((Observer)FilterController.this.g).update((Observable)null, (Object)null);
            return var1x;
         }
      });
      if (var3 == ControllerElement.FilterRowStyle.LEFT) {
         var5.leftDependentHalf = true;
      } else if (var3 == ControllerElement.FilterRowStyle.RIGHT) {
         var5.rightDependentHalf = true;
      }

      var5.onInit();
      ControllerElement var6;
      (var6 = new ControllerElement(var5)).setMode(var3);
      var6.setPos(var4);
      if (var4 == ControllerElement.FilterPos.BOTTOM) {
         this.bottomElements.add(var6);
      } else {
         this.topElements.add(var6);
      }
   }

   public void addTextFilter(GUIListFilterText var1, ControllerElement.FilterRowStyle var2) {
      this.addTextFilter(var1, Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_FORMS_GUI_NEWGUI_FILTERCONTROLLER_0, var2);
   }

   public boolean isFiltered(Object var1) {
      Iterator var2 = this.generalListFilter.iterator();

      GuiListFilter var10000;
      do {
         if (!var2.hasNext()) {
            return false;
         }

         var10000 = (GuiListFilter)var2.next();
      } while(var10000.isOk(var10000.getFilter(), var1));

      return true;
   }

   public void calcInit() {
      this.filterHeightBottom = 0;
      this.filterHeightTop = 0;

      int var1;
      for(var1 = 0; var1 < this.bottomElements.size(); ++var1) {
         if (((ControllerElement)this.bottomElements.get(var1)).getMode() != ControllerElement.FilterRowStyle.LEFT) {
            this.filterHeightBottom = (int)((float)this.filterHeightBottom + ((ControllerElement)this.bottomElements.get(var1)).gui.getHeight());
         }
      }

      for(var1 = 0; var1 < this.topElements.size(); ++var1) {
         if (((ControllerElement)this.topElements.get(var1)).getMode() != ControllerElement.FilterRowStyle.LEFT) {
            this.filterHeightTop = (int)((float)this.filterHeightTop + ((ControllerElement)this.topElements.get(var1)).gui.getHeight());
         }
      }

   }

   public void drawTop(int var1) {
      ControllerElement.drawFilterElements(false, var1, this.topElements);
      ControllerElement.drawFilterElements(true, var1, this.topElements);
      GlUtil.glTranslatef(0.0F, (float)this.filterHeightTop, 0.0F);
   }

   public void drawContent(GUIScrollablePanel var1, int var2) {
      GlUtil.glTranslatef(0.0F, (float)(-this.filterHeightTop), 0.0F);
      var1.dependendHeightDiff = -(var2 + this.filterHeightBottom + this.filterHeightTop);
      var1.setPos(0.0F, (float)(var2 + this.filterHeightTop), 0.0F);
      var1.draw();
   }

   public void drawBottom(GUIScrollablePanel var1, int var2) {
      ControllerElement.drawFilterElements(false, (int)var1.getHeight() + var2 + this.filterHeightTop, this.bottomElements);
      ControllerElement.drawFilterElements(true, (int)var1.getHeight() + var2 + this.filterHeightTop, this.bottomElements);
   }
}
