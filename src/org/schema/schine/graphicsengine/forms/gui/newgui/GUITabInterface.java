package org.schema.schine.graphicsengine.forms.gui.newgui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;

public interface GUITabInterface extends GUIWindowInterface {
   ObjectArrayList getTabs();

   int getSelectedTab();

   void setSelectedTab(int var1);

   int getInnerWidthTab();
}
