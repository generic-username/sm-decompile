package org.schema.schine.graphicsengine.forms.gui.newgui.vbogui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.nio.FloatBuffer;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITexDrawableArea;
import org.schema.schine.input.Keyboard;

public class VBOAccess {
   private static final ObjectArrayList buffers = new ObjectArrayList();
   private final int textureHeight;
   private final int textureWidth;
   public VBOAccess.VectexAccess vAccess = new VBOAccess.VectexAccess();
   public VBOAccess.TextureAccess tAccess = new VBOAccess.TextureAccess();
   public long lastTouched = 0L;

   public VBOAccess(int var1, int var2) {
      this.textureHeight = var2;
      this.textureWidth = var1;
   }

   public void render() {
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.glEnableClientState(32884);
      GlUtil.glEnableClientState(32888);
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      assert this.vAccess.vertexIndex != 0;

      GlUtil.glBindBuffer(34962, this.vAccess.vertexIndex);
      GL11.glVertexPointer(2, 5126, 0, this.vAccess.byteOffset);
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      assert this.tAccess.texIndex != 0;

      GlUtil.glBindBuffer(34962, this.tAccess.texIndex);
      GL11.glTexCoordPointer(2, 5126, 0, this.tAccess.byteOffset);
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      GL11.glDrawArrays(7, 0, 4);
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.glBindBuffer(34962, 0);
      GlUtil.glDisableClientState(32884);
      GlUtil.glDisableClientState(32888);
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

   }

   public void generate(GUITexDrawableArea var1) {
      this.vAccess.generate(var1);
      this.tAccess.generate(var1);
   }

   public void setFromThis(GUITexDrawableArea var1) {
      this.vAccess.width = (int)var1.getWidth();
      this.vAccess.height = (int)var1.getHeight();
      this.tAccess.xOffset = var1.xOffset;
      this.tAccess.yOffset = var1.yOffset;
   }

   public int hashCode() {
      return this.vAccess.hashCode() + 10000000 * this.tAccess.hashCode();
   }

   public boolean equals(Object var1) {
      VBOAccess var2 = (VBOAccess)var1;
      return this.vAccess.width == var2.vAccess.width && this.vAccess.height == var2.vAccess.height && this.tAccess.xOffset == var2.tAccess.xOffset && this.tAccess.yOffset == var2.tAccess.yOffset;
   }

   public String toString() {
      return "VBOAccess [vAccess=" + this.vAccess + ", tAccess=" + this.tAccess + ", lastTouched=" + this.lastTouched + "]";
   }

   class VBO {
      public static final int MAX_FILLED_BYTES = 4194304;
      private int id;
      private long filledBytes;

      public VBO(int var2) {
         this.id = var2;
      }
   }

   public class TextureAccess {
      public static final int BYTE_SIZE = 32;
      public float xOffset;
      public float yOffset;
      public int texIndex;
      private long byteOffset;

      private void generate(GUITexDrawableArea var1) {
         if (Keyboard.isKeyDown(60)) {
            GlUtil.printGlErrorCritical();
         }

         if (this.texIndex == 0) {
            if (VBOAccess.buffers.isEmpty() || 4194304L - ((VBOAccess.VBO)VBOAccess.buffers.get(VBOAccess.buffers.size() - 1)).filledBytes < 32L) {
               int var2 = GL15.glGenBuffers();
               GL15.glBindBuffer(34962, var2);
               GL15.glBufferData(34962, 4194304L, 35044);
               VBOAccess.buffers.add(VBOAccess.this.new VBO(var2));
            }

            this.texIndex = ((VBOAccess.VBO)VBOAccess.buffers.get(VBOAccess.buffers.size() - 1)).id;
            this.byteOffset = ((VBOAccess.VBO)VBOAccess.buffers.get(VBOAccess.buffers.size() - 1)).filledBytes;
            ((VBOAccess.VBO)VBOAccess.buffers.get(VBOAccess.buffers.size() - 1)).filledBytes = 32L;
         }

         if (Keyboard.isKeyDown(60)) {
            GlUtil.printGlErrorCritical();
         }

         GL15.glBindBuffer(34962, this.texIndex);
         if (Keyboard.isKeyDown(60)) {
            GlUtil.printGlErrorCritical();
         }

         FloatBuffer var3;
         (var3 = GlUtil.getDynamicByteBuffer(32, 0).asFloatBuffer()).put(var1.xOffset);
         var3.put(var1.yOffset);
         var3.put(var1.xOffset);
         var3.put(var1.yOffset + var1.getHeight() / (float)VBOAccess.this.textureHeight);
         var3.put(var1.xOffset + var1.getWidth() / (float)VBOAccess.this.textureWidth);
         var3.put(var1.yOffset + var1.getHeight() / (float)VBOAccess.this.textureHeight);
         var3.put(var1.xOffset + var1.getWidth() / (float)VBOAccess.this.textureWidth);
         var3.put(var1.yOffset);
         var3.flip();
         GL15.glBufferSubData(34962, this.byteOffset, var3);
         if (Keyboard.isKeyDown(60)) {
            GlUtil.printGlErrorCritical();
         }

         GL15.glBindBuffer(34962, 0);
         if (Keyboard.isKeyDown(60)) {
            GlUtil.printGlErrorCritical();
         }

      }

      public int hashCode() {
         return (int)(this.xOffset * (float)VBOAccess.this.textureWidth) + 100000 * (int)(this.yOffset * (float)VBOAccess.this.textureHeight);
      }

      public String toString() {
         return "TextureAccess [xOffset=" + this.xOffset + ", yOffset=" + this.yOffset + ", texIndex=" + this.texIndex + "]";
      }
   }

   public class VectexAccess {
      public static final int BYTE_SIZE = 32;
      public int width;
      public int height;
      public int vertexIndex;
      private long byteOffset;

      private void generate(GUITexDrawableArea var1) {
         if (Keyboard.isKeyDown(60)) {
            GlUtil.printGlErrorCritical();
         }

         if (this.vertexIndex == 0) {
            if (VBOAccess.buffers.isEmpty() || 4194304L - ((VBOAccess.VBO)VBOAccess.buffers.get(VBOAccess.buffers.size() - 1)).filledBytes < 32L) {
               int var2 = GL15.glGenBuffers();
               GL15.glBindBuffer(34962, var2);
               GL15.glBufferData(34962, 4194304L, 35044);
               VBOAccess.buffers.add(VBOAccess.this.new VBO(var2));
            }

            this.vertexIndex = ((VBOAccess.VBO)VBOAccess.buffers.get(VBOAccess.buffers.size() - 1)).id;
            this.byteOffset = ((VBOAccess.VBO)VBOAccess.buffers.get(VBOAccess.buffers.size() - 1)).filledBytes;
            ((VBOAccess.VBO)VBOAccess.buffers.get(VBOAccess.buffers.size() - 1)).filledBytes = 32L;
         }

         if (Keyboard.isKeyDown(60)) {
            GlUtil.printGlErrorCritical();
         }

         GL15.glBindBuffer(34962, this.vertexIndex);
         if (Keyboard.isKeyDown(60)) {
            GlUtil.printGlErrorCritical();
         }

         FloatBuffer var3;
         (var3 = GlUtil.getDynamicByteBuffer(32, 0).asFloatBuffer()).put(0.0F);
         var3.put(0.0F);
         var3.put(0.0F);
         var3.put(var1.getHeight());
         var3.put(var1.getWidth());
         var3.put(var1.getHeight());
         var3.put(var1.getWidth());
         var3.put(0.0F);
         var3.flip();
         GL15.glBufferSubData(34962, this.byteOffset, var3);
         if (Keyboard.isKeyDown(60)) {
            GlUtil.printGlErrorCritical();
         }

         GL15.glBindBuffer(34962, 0);
         if (Keyboard.isKeyDown(60)) {
            GlUtil.printGlErrorCritical();
         }

      }

      public int hashCode() {
         return this.width + 100000 * this.height;
      }

      public boolean equals(Object var1) {
         return this.width == ((VBOAccess.VectexAccess)var1).width && this.height == ((VBOAccess.VectexAccess)var1).height;
      }

      public String toString() {
         return "VectexAccess [width=" + this.width + ", height=" + this.height + ", vertexIndex=" + this.vertexIndex + "]";
      }
   }
}
