package org.schema.schine.graphicsengine.forms.gui.newgui;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class GUIListSorterDropdown implements GuiListSorter {
   public final Comparator[] values;
   protected Comparator value;
   private Comparator comp;

   public GUIListSorterDropdown(Comparator... var1) {
      assert var1 != null;

      this.values = var1;
      this.comp = new Comparator() {
         public int compare(GUITileParam var1, GUITileParam var2) {
            return GUIListSorterDropdown.this.value.compare(var1.getUserData(), var2.getUserData());
         }
      };
   }

   public Comparator getSorter() {
      return this.value;
   }

   public void setSorter(Comparator var1) {
      this.value = var1;
      this.comp = new Comparator() {
         public int compare(GUITileParam var1, GUITileParam var2) {
            return GUIListSorterDropdown.this.value.compare(var1.getUserData(), var2.getUserData());
         }
      };
   }

   public void sort(List var1) {
      Collections.sort(var1, this.comp);
   }
}
