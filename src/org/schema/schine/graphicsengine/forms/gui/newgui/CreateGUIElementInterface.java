package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public interface CreateGUIElementInterface {
   GUIElement create(Object var1);

   GUIElement createNeutral();
}
