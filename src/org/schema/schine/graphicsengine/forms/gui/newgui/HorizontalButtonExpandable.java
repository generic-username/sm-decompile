package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.network.client.ClientState;

public class HorizontalButtonExpandable extends GUIHorizontalArea {
   private GUIActivationCallback actCallback;
   private GUITextOverlay overlay;
   public GUIActiveInterface activeInterface;
   private Vector3i sizeHelp;
   private GUIHorizontalArea.HButtonType defaultType;

   public HorizontalButtonExpandable(ClientState var1, GUIHorizontalArea.HButtonType var2, Object var3, GUICallback var4, GUIActiveInterface var5, GUIActivationCallback var6) {
      super(var1, (GUIHorizontalArea.HButtonType)var2, 10);
      this.defaultType = var2;
      this.setCallback(var4);
      this.actCallback = var6;
      this.activeInterface = var5;
      this.sizeHelp = new Vector3i();
      this.overlay = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM.getFont(), this.getState()) {
         public void onDirty() {
            HorizontalButtonExpandable.this.sizeHelp.x = this.getFont().getWidth(this.getText().get(0).toString());
         }
      };
      this.sizeHelp.x = this.overlay.getFont().getWidth(var3.toString());
      this.overlay.setTextSimple(var3);
      this.setMouseUpdateEnabled(true);
      GUIScrollablePanel var7;
      (var7 = new GUIScrollablePanel(this.getWidth(), this.getHeight(), this, this.getState())).setScrollable(0);
      var7.setLeftRightClipOnly = true;
      var7.setContent(this.overlay);
      this.attach(var7);
   }

   public void draw() {
      this.isInside();
      if (this.actCallback == null || this.actCallback.isVisible(this.getState())) {
         boolean var1 = this.actCallback == null || this.actCallback.isActive(this.getState());
         boolean var2 = this.actCallback != null && this.actCallback instanceof GUIActivationHighlightCallback && ((GUIActivationHighlightCallback)this.actCallback).isHighlighted(this.getState());
         if (!var1) {
            this.overlay.setColor(0.78F, 0.78F, 0.78F, 1.0F);
         } else {
            this.overlay.setColor(1.0F, 1.0F, 1.0F, 1.0F);
         }

         this.setMouseUpdateEnabled(var1);
         if ((this.activeInterface == null || this.activeInterface.isActive()) && (!this.getCallback().isOccluded() || !var1)) {
            this.setType(GUIHorizontalArea.HButtonType.getType(this.defaultType, this.isInside() && this.getState().getController().getInputController().getCurrentActiveDropdown() == null && this.getState().getController().getInputController().getCurrentContextPane() == this.getState().getController().getInputController().getCurrentContextPaneDrawing(), var1, var2));
         }

         this.overlay.setPos((float)((int)(this.getWidth() / 2.0F - (float)(this.sizeHelp.x / 2))), 4.0F, 0.0F);
         super.draw();
      }

   }
}
