package org.schema.schine.graphicsengine.forms.gui.newgui;

public interface GUIEnterableListOnExtendedCallback {
   void extended();

   void collapsed();
}
