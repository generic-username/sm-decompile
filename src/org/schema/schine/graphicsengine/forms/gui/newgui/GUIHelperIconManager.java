package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.input.InputState;
import org.schema.schine.input.InputType;
import org.schema.schine.input.KeyboardMappings;

public class GUIHelperIconManager {
   private static final FontLibrary.FontSize onSize;
   private static final FontLibrary.FontSize afterSize;

   public static GUIHelperIcon get(InputState var0, InputType var1, int var2) {
      return get(var0, var1, var2, onSize, afterSize);
   }

   public static GUIHelperIcon get(InputState var0, InputType var1, int var2, FontLibrary.FontSize var3, FontLibrary.FontSize var4) {
      GUIHelperTextureType var5 = GUIHelperTextureType.SINGLE;
      switch(var1) {
      case BLOCK:
         var5 = GUIHelperTextureType.NONE;
         return new GUIHelperIcon(var0, var5, var3.getFont(), var4.getFont());
      case JOYSTICK:
         throw new RuntimeException("TODO");
      case KEYBOARD:
         switch(var2) {
         case 14:
            var5 = GUIHelperTextureType.ONEANDHALF;
            var3 = var3.smaller();
            break;
         case 15:
            var5 = GUIHelperTextureType.ONEANDHALF;
            break;
         case 28:
            var5 = GUIHelperTextureType.ONEANDHALF;
            var3 = var3.smaller();
            break;
         case 29:
            var5 = GUIHelperTextureType.TWO;
            var3 = var3.smaller();
            break;
         case 42:
            var5 = GUIHelperTextureType.TWO;
            var3 = var3.smaller();
            break;
         case 54:
            var5 = GUIHelperTextureType.TWO;
            var3 = var3.smaller();
            break;
         case 56:
            var5 = GUIHelperTextureType.TWO;
            var3 = var3.smaller();
            break;
         case 57:
            var5 = GUIHelperTextureType.TWO;
            var3 = var3.smaller();
            break;
         case 157:
            var5 = GUIHelperTextureType.TWO;
            var3 = var3.smaller();
            break;
         case 184:
            var5 = GUIHelperTextureType.TWO;
            var3 = var3.smaller();
         }

         GUIHelperIcon var6;
         (var6 = new GUIHelperIcon(var0, var5, var3.getFont(), var4.getFont())).setTextOn(KeyboardMappings.getKeyChar(var2));
         return var6;
      case MOUSE:
         var5 = GUIHelperTextureType.MOUSE_LEFT;
         if (var2 == 1) {
            var5 = GUIHelperTextureType.MOUSE_RIGHT;
         } else if (var2 == 2) {
            var5 = GUIHelperTextureType.MOUSE_MID;
         } else if (var2 == 3) {
            var5 = GUIHelperTextureType.MOUSE_WUP;
         } else if (var2 == 4) {
            var5 = GUIHelperTextureType.MOUSE_WDOWN;
         }

         return new GUIHelperIcon(var0, var5, var3.getFont(), var4.getFont());
      default:
         throw new RuntimeException("TODO");
      }
   }

   static {
      onSize = FontLibrary.FontSize.MEDIUM;
      afterSize = FontLibrary.FontSize.MEDIUM;
   }
}
