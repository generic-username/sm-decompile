package org.schema.schine.graphicsengine.forms.gui.newgui;

public interface PolygonStatsInterface {
   int getDataPointsNum();

   double getPercent(int var1);

   double getValue(int var1);

   String getValueName(int var1);
}
