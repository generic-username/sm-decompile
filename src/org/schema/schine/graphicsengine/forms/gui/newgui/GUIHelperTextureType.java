package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.newdawn.slick.geom.Vector2f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.texture.Texture;
import org.schema.schine.input.InputState;

public enum GUIHelperTextureType {
   NONE(0, 0, 0.0F, 1.0F, false),
   SINGLE(0, 0, 1.0F, 1.0F, true),
   ONEANDHALF(1, 0, 2.0F, 1.0F, true),
   TWO(3, 0, 2.0F, 1.0F, true),
   MOUSE_LEFT(5, 0, 1.0F, 1.0F, false),
   MOUSE_RIGHT(6, 0, 1.0F, 1.0F, false),
   MOUSE_MID(7, 0, 1.0F, 1.0F, false),
   MOUSE_WUP(8, 0, 1.0F, 1.0F, false),
   MOUSE_WDOWN(9, 0, 1.0F, 1.0F, false);

   public static final String tex = "helpers-16x16-gui-";
   public static final int res = 64;
   private final int indexX;
   public final boolean hasText;
   private final int indexY;
   private final float width;
   private final float height;
   private static final GUITexDrawableArea[] buffer = new GUITexDrawableArea[values().length];

   private GUIHelperTextureType(int var3, int var4, float var5, float var6, boolean var7) {
      this.indexX = var3;
      this.indexY = var4;
      this.width = var5;
      this.height = var6;
      this.hasText = var7;
   }

   public static Texture getTexture() {
      return getSprite().getMaterial().getTexture();
   }

   public static Sprite getSprite() {
      return Controller.getResLoader().getSprite("helpers-16x16-gui-");
   }

   public final GUITexDrawableArea get(InputState var1) {
      if (buffer[this.ordinal()] == null) {
         float var2 = 64.0F / (float)getTexture().getWidth() * (float)this.indexX;
         float var3 = 64.0F / (float)getTexture().getWidth() * (float)this.indexY;
         GUITexDrawableArea var4;
         (var4 = new GUITexDrawableArea(var1, getTexture(), var2, var3)).setWidth((int)(this.width * 64.0F));
         var4.setHeight((int)(this.height * 64.0F));
         buffer[this.ordinal()] = var4;
      }

      return buffer[this.ordinal()];
   }

   public final Vector2f getCenter(Vector2f var1) {
      var1.set((float)((int)(this.width * 64.0F * 0.5F)), (float)((int)(this.height * 64.0F * 0.5F)));
      return var1;
   }
}
