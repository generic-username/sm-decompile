package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.input.InputState;

public class GUILeftRigthShadow extends GUILeftRightArea {
   public GUILeftRigthShadow(InputState var1, int var2, int var3) {
      super(var1, var2, var3);
   }

   public int getPXWidth() {
      return 32;
   }

   public int getPXHeight() {
      return 32;
   }

   protected String getVertical() {
      return "UI 32px Vertical-8x1-gui-";
   }

   protected float getLeftOffset() {
      return 0.0F;
   }

   protected float getRightOffset() {
      return 0.125F;
   }
}
