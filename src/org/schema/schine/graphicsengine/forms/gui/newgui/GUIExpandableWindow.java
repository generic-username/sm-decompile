package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.IconDatabase;
import org.schema.schine.input.InputState;

public class GUIExpandableWindow extends GUIPlainWindow {
   boolean init = false;
   private GUITextButton title;
   public boolean expanded;
   public int unexpendedHeight = 34;
   public int clickableTopPadding = -2;
   public int clickableBottomPadding = 2;
   public int clickableTopPaddingUnexp = 0;
   public int clickableBottomPaddingUnexp = 0;
   private GUIOverlay rightArrow;
   private GUIOverlay downArrow;

   public GUIExpandableWindow(InputState var1, int var2, int var3, String var4, boolean var5) {
      super(var1, var2, var3, var4);
      if (var4 == null || !windowMap.containsKey(var4)) {
         this.expanded = var5;
      }

   }

   public GUIExpandableWindow(InputState var1, int var2, int var3, int var4, int var5, String var6, boolean var7) {
      super(var1, var2, var3, var4, var5, var6);
      if (this.savedSizeAndPosition != null && this.savedSizeAndPosition.newPanel) {
         this.expanded = var7;
      }

   }

   protected void drawContent(GUIContentPane var1) {
      if (this.expanded) {
         super.drawContent(var1);
      }

   }

   public float getHeight() {
      return (float)((int)(this.expanded ? super.getHeight() : (float)this.unexpendedHeight));
   }

   protected void setSavedSizeAndPosFrom() {
      this.savedSizeAndPosition.setFrom(this.width, this.height, this.getPos(), this.expanded);
   }

   public void setTitle(final String var1, final ExpandableCallback var2) {
      if (this.title != null) {
         this.detach(this.title);
      }

      this.title = new GUITextButton(this.getState(), 10, 10, FontLibrary.getBlenderProHeavy20(), new Object() {
         public String toString() {
            return "  " + var1;
         }
      }, new GUICallback() {
         public boolean isOccluded() {
            return !GUIExpandableWindow.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               GUIExpandableWindow.this.expand(var2, !GUIExpandableWindow.this.expanded);
            }

         }
      }) {
         public void draw() {
            if (!GUIExpandableWindow.this.expanded) {
               this.setPos(0.0F, (float)GUIExpandableWindow.this.clickableTopPaddingUnexp, 0.0F);
               this.setHeight(GUIExpandableWindow.this.unexpendedHeight - 1 - GUIExpandableWindow.this.clickableBottomPaddingUnexp);
            } else {
               this.setPos(0.0F, (float)GUIExpandableWindow.this.clickableTopPadding, 0.0F);
               this.setHeight(GUIExpandableWindow.this.unexpendedHeight - 1 - GUIExpandableWindow.this.clickableBottomPadding);
            }

            if (GUIExpandableWindow.this.expanded) {
               this.setTextPos(4, 6 - GUIExpandableWindow.this.clickableTopPadding);
            } else {
               this.setTextPos(4, 6 - GUIExpandableWindow.this.clickableTopPaddingUnexp);
            }

            this.setWidth(GUIExpandableWindow.this.getWidth());
            super.draw();
         }
      };
      this.title.getBackgroundColorMouseOverlayPressed().w = 0.35F;
      this.title.getBackgroundColorMouseOverlay().w = 0.2F;
      this.title.getBackgroundColor().w = 0.0F;
      this.attachSuper(this.title);
      this.expand(var2, this.expanded);
   }

   public void expand(ExpandableCallback var1, boolean var2) {
      this.expanded = var2;
      var1.onExpandedChanged();
      if (this.expanded) {
         this.title.removeBeforeIcon(this.rightArrow);
         this.title.addBeforeIcon(this.downArrow);
      } else {
         this.title.removeBeforeIcon(this.downArrow);
         this.title.addBeforeIcon(this.rightArrow);
      }

      this.title.setBeforOrAfterIconPos(this.rightArrow, 2, 3);
      this.title.setBeforOrAfterIconPos(this.downArrow, 1, 2);
   }

   public void onInit() {
      if (!this.init) {
         super.onInit();
         this.rightArrow = IconDatabase.getRightArrowInstance16(this.getState());
         this.downArrow = IconDatabase.getDownArrowInstance16(this.getState());
         this.init = true;
      }
   }

   public int getInnerHeigth() {
      return !this.expanded ? 0 : super.getInnerHeigth();
   }
}
