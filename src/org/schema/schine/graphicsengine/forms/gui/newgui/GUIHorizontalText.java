package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.ColoredInterface;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUIHorizontalText extends GUIAbstractHorizontalArea {
   private static final float HEIGHT = 24.0F;
   private GUITextOverlay overlay;
   private Vector3i sizeHelp;
   private boolean initHelp;
   private ColoredInterface colorIface;
   private int orientation = 32;

   private void init(Object var1, FontLibrary.FontSize var2, ColoredInterface var3) {
      this.sizeHelp = new Vector3i();
      this.colorIface = var3;
      this.overlay = new GUITextOverlay(10, 10, var2.getFont(), this.getState()) {
         public void onDirty() {
            GUIHorizontalText.this.sizeHelp.x = this.getFont().getWidth(this.getText().get(0).toString());
         }
      };
      this.overlay.setTextSimple(var1);
      this.setMouseUpdateEnabled(true);
      GUIScrollablePanel var4;
      (var4 = new GUIScrollablePanel(this.getWidth(), this.getHeight(), this, this.getState())).setScrollable(0);
      var4.setLeftRightClipOnly = true;
      var4.setContent(this.overlay);
      this.attach(var4);
   }

   public GUIHorizontalText(InputState var1, Object var2, FontLibrary.FontSize var3, ColoredInterface var4) {
      super(var1);
      this.init(var2, var3, var4);
   }

   public String getTextToString() {
      return this.overlay.getText().toString();
   }

   public void cleanUp() {
      this.overlay.cleanUp();
   }

   public void draw() {
      this.isInside();
      if (this.actCallback == null || this.actCallback.isVisible(this.getState())) {
         if (!this.initHelp) {
            this.sizeHelp.x = this.overlay.getFont().getWidth(this.overlay.getText().get(0).toString());
            this.initHelp = true;
         }

         if (this.colorIface != null) {
            this.overlay.setColor(this.colorIface.getColor());
         } else {
            this.overlay.setColor(1.0F, 1.0F, 1.0F, 1.0F);
         }

         if ((this.orientation & 1) == 1) {
            this.overlay.setPos(4.0F, 4.0F, 0.0F);
         } else if ((this.orientation & 2) == 2) {
            this.overlay.setPos(this.getWidth() - (float)this.sizeHelp.x - 4.0F, 4.0F, 0.0F);
         } else {
            this.overlay.setPos((float)((int)(this.getWidth() / 2.0F - (float)(this.sizeHelp.x / 2))), 4.0F, 0.0F);
         }

         this.drawAttached();
      }

   }

   public void setAlign(int var1) {
      this.orientation = var1;
   }

   public void onInit() {
   }

   protected void adjustWidth() {
   }

   public void setHeight(float var1) {
   }

   public float getHeight() {
      return 24.0F;
   }
}
