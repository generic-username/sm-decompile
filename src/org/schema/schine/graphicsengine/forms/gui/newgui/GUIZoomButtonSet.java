package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.input.InputState;

public abstract class GUIZoomButtonSet extends GUIElement {
   GUITextButton zoomIn;
   GUITextButton zoomOut;
   private int minZoom;
   private int maxZoom;
   private int zoom;
   private boolean zoomChanged = true;
   private int distBetweenButtons = 3;
   private boolean init;

   public GUIZoomButtonSet(InputState var1, int var2, int var3, int var4) {
      super(var1);
      this.minZoom = var3;
      this.maxZoom = var4;
      this.zoom = Math.min(Math.max(var3, var2), var4);
      this.zoomIn = new GUITextButton(var1, 24, 24, "+", new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUIZoomButtonSet.this.zoom = Math.min(GUIZoomButtonSet.this.maxZoom, GUIZoomButtonSet.this.zoom + 1);
               GUIZoomButtonSet.this.zoomChanged = true;
            }

         }

         public boolean isOccluded() {
            return !GUIZoomButtonSet.this.isActive();
         }
      });
      this.zoomIn.activationInterface = new GUIActiveInterface() {
         public boolean isActive() {
            return GUIZoomButtonSet.this.isActive() && GUIZoomButtonSet.this.zoom < GUIZoomButtonSet.this.maxZoom;
         }
      };
      this.zoomOut = new GUITextButton(var1, 24, 24, "-", new GUICallback() {
         public boolean isOccluded() {
            return !GUIZoomButtonSet.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUIZoomButtonSet.this.zoom = Math.max(GUIZoomButtonSet.this.minZoom, GUIZoomButtonSet.this.zoom - 1);
               GUIZoomButtonSet.this.zoomChanged = true;
            }

         }
      });
      this.zoomOut.activationInterface = new GUIActiveInterface() {
         public boolean isActive() {
            return GUIZoomButtonSet.this.isActive() && GUIZoomButtonSet.this.zoom > GUIZoomButtonSet.this.minZoom;
         }
      };
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.zoomChanged) {
         this.zoomChanged(this.zoom);
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.zoomOut.draw();
      this.zoomIn.setPos(this.zoomOut.getPos().x + this.zoomOut.getWidth() + (float)this.distBetweenButtons, this.zoomOut.getPos().y, 0.0F);
      this.zoomIn.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      if (!this.init) {
         this.zoomIn.onInit();
         this.zoomOut.onInit();
         this.init = true;
      }

   }

   public abstract void zoomChanged(int var1);

   public float getHeight() {
      return this.zoomIn.getHeight();
   }

   public float getWidth() {
      return this.zoomIn.getWidth() + (float)this.distBetweenButtons + this.zoomOut.getWidth();
   }
}
