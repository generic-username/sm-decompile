package org.schema.schine.graphicsengine.forms.gui.newgui.config;

import javax.vecmath.Vector2f;

public interface WindowPaletteInterface {
   Vector2f getTopModifierOffset();

   Vector2f getRightModifierOffset();

   Vector2f getBottomModifierOffset();

   Vector2f getLeftModifierOffset();

   Vector2f getMoveModifierOffset();
}
