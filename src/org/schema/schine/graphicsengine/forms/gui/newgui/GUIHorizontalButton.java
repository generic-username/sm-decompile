package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.newdawn.slick.UnicodeFont;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUIHorizontalButton extends GUIHorizontalArea {
   private GUITextOverlay overlay;
   private Vector3i sizeHelp;
   private GUIHorizontalArea.HButtonType defaultType;
   private boolean initHelp;

   private void init(Object var1, FontLibrary.FontSize var2, GUICallback var3, GUIActiveInterface var4, GUIActivationCallback var5) {
      this.setDefaultType(this.type);
      this.setCallback(var3);
      this.actCallback = var5;
      this.activeInterface = var4;
      this.sizeHelp = new Vector3i();
      this.overlay = new GUITextOverlay(10, 10, var2.getFont(), this.getState()) {
         public void onDirty() {
            GUIHorizontalButton.this.sizeHelp.x = this.getFont().getWidth(this.getText().get(0).toString());
         }
      };
      this.overlay.setTextSimple(var1);
      this.setMouseUpdateEnabled(true);
      GUIScrollablePanel var6;
      (var6 = new GUIScrollablePanel(this.getWidth(), this.getHeight(), this, this.getState())).setScrollable(0);
      var6.setLeftRightClipOnly = true;
      var6.setContent(this.overlay);
      this.attach(var6);
   }

   public GUIHorizontalButton(InputState var1, GUIHorizontalArea.HButtonColor var2, Object var3, GUICallback var4, GUIActiveInterface var5, GUIActivationCallback var6) {
      this(var1, var2, FontLibrary.FontSize.MEDIUM, var3, var4, var5, var6);
   }

   public GUIHorizontalButton(InputState var1, GUIHorizontalArea.HButtonColor var2, FontLibrary.FontSize var3, Object var4, GUICallback var5, GUIActiveInterface var6, GUIActivationCallback var7) {
      super(var1, (GUIHorizontalArea.HButtonColor)var2, 10);
      this.init(var4, var3, var5, var6, var7);
   }

   public GUIHorizontalButton(InputState var1, GUIHorizontalArea.HButtonType var2, Object var3, GUICallback var4, GUIActiveInterface var5, GUIActivationCallback var6) {
      this(var1, var2, FontLibrary.FontSize.MEDIUM, var3, var4, var5, var6);
   }

   public GUIHorizontalButton(InputState var1, GUIHorizontalArea.HButtonType var2, FontLibrary.FontSize var3, Object var4, GUICallback var5, GUIActiveInterface var6, GUIActivationCallback var7) {
      super(var1, (GUIHorizontalArea.HButtonType)var2, 10);
      this.init(var4, var3, var5, var6, var7);
   }

   public String getTextToString() {
      return this.overlay.getText().toString();
   }

   public void cleanUp() {
      super.cleanUp();
      this.overlay.cleanUp();
   }

   public void draw() {
      this.isInside();
      if (this.actCallback == null || this.actCallback.isVisible(this.getState())) {
         if (!this.initHelp) {
            this.sizeHelp.x = this.overlay.getFont().getWidth(this.overlay.getText().get(0).toString());
            this.initHelp = true;
         }

         boolean var1 = this.actCallback == null || this.actCallback.isActive(this.getState());
         boolean var2 = this.actCallback != null && this.actCallback instanceof GUIActivationHighlightCallback && ((GUIActivationHighlightCallback)this.actCallback).isHighlighted(this.getState());
         if (!var1) {
            this.overlay.setColor(0.78F, 0.78F, 0.78F, 1.0F);
         } else {
            this.overlay.setColor(1.0F, 1.0F, 1.0F, 1.0F);
         }

         this.setMouseUpdateEnabled(var1 && (this.activeInterface == null || this.activeInterface.isActive()));
         this.setType(GUIHorizontalArea.HButtonType.getType(this.getDefaultType(), var1 && this.isInside() && this.getState().getController().getInputController().getCurrentActiveDropdown() == null && this.getState().getController().getInputController().getCurrentContextPane() == this.getState().getController().getInputController().getCurrentContextPaneDrawing(), var1, var2));
         this.overlay.setPos((float)((int)(this.getWidth() / 2.0F - (float)(this.sizeHelp.x / 2))), 4.0F, 0.0F);
         super.draw();
      }

   }

   public GUIHorizontalArea.HButtonType getDefaultType() {
      return this.defaultType;
   }

   public void setDefaultType(GUIHorizontalArea.HButtonType var1) {
      this.defaultType = var1;
   }

   public void setFont(UnicodeFont var1) {
      this.overlay.setFont(var1);
   }
}
