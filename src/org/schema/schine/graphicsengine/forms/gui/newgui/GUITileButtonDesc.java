package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUITileButtonDesc extends GUITileParam {
   public OnDrawInterface onDrawInterface;
   public GUIHorizontalButton button;
   public GUITextOverlay descriptionText;
   protected Object userData;

   public GUITileButtonDesc(InputState var1, float var2, float var3) {
      super(var1, var2, var3, (Object)null);
   }

   public void draw() {
      if (this.onDrawInterface != null && this.button != null & this.descriptionText != null) {
         this.onDrawInterface.onDraw(this.button, this.descriptionText);
      }

      super.draw();
   }
}
