package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;

public interface DialogInterface {
   GUIElement getInputPanel();

   boolean allowChat();

   void deactivate();

   boolean checkDeactivated();

   void handleKeyEvent(KeyEventInterface var1);

   void updateDeacivated();

   long getDeactivationTime();

   void update(Timer var1);
}
