package org.schema.schine.graphicsengine.forms.gui.newgui;

public interface GUIDrawnTimerInterface {
   float getTimeDrawn();

   void setTimeDrawn(float var1);
}
