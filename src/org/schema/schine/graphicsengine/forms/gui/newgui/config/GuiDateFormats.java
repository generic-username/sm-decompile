package org.schema.schine.graphicsengine.forms.gui.newgui.config;

import java.text.DateFormat;
import org.schema.common.config.ConfigurationElement;

public class GuiDateFormats extends GuiConfig {
   public static final DateFormat longFormat = DateFormat.getDateInstance(1);
   public static final DateFormat mediumFormat = DateFormat.getDateInstance(2);
   public static final DateFormat shortFormat = DateFormat.getDateInstance(3);
   @ConfigurationElement(
      name = "Custom0"
   )
   public static DateFormat custom0;
   @ConfigurationElement(
      name = "NetworkStatTime"
   )
   public static DateFormat ntStatTime;
   @ConfigurationElement(
      name = "MailTime"
   )
   public static DateFormat mailTime;
   @ConfigurationElement(
      name = "MessageLogTime"
   )
   public static DateFormat messageLogTime;
   @ConfigurationElement(
      name = "FactionNewsTime"
   )
   public static DateFormat factionNewsTime;
   @ConfigurationElement(
      name = "CatalogEntryCreated"
   )
   public static DateFormat catalogEntryCreated;
   @ConfigurationElement(
      name = "FactionMemberLastSeenTime"
   )
   public static DateFormat factionMemberLastSeenTime;

   protected String getTag() {
      return "GuiDateFormats";
   }
}
