package org.schema.schine.graphicsengine.forms.gui.newgui.settingsnew;

import org.newdawn.slick.UnicodeFont;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.gui.GUICheckBox;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.IconDatabase;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.InputState;

public abstract class GUICheckBoxTextPairNew extends GUIElement {
   private boolean init;
   private Object text;
   private UnicodeFont font;
   public int distanceAfterText = 3;
   boolean checkboxBeforeText = true;
   private int width;
   private int height;
   private GUICheckBox checkBox;
   private GUITextOverlay l;
   public GUIActiveInterface activeInterface;

   public GUICheckBoxTextPairNew(InputState var1, Object var2, UnicodeFont var3) {
      super(var1);
      this.font = var3;
      this.text = var2;
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      Sprite var1 = IconDatabase.getIcons16(this.getState());
      this.width = this.l.getMaxLineWidth() + this.distanceAfterText + var1.getWidth();
      this.height = Math.max(this.l.getTextHeight(), var1.getHeight());
      this.checkBox.setPos(this.checkboxBeforeText ? 0.0F : (float)(this.l.getMaxLineWidth() + this.distanceAfterText), (float)((int)((float)this.height * 0.5F - (float)var1.getHeight() * 0.5F)), 0.0F);
      this.l.setPos(this.checkboxBeforeText ? (float)(var1.getWidth() + this.distanceAfterText) : 0.0F, (float)((int)((float)this.height * 0.5F - (float)this.l.getTextHeight() * 0.5F)), 0.0F);
      this.drawAttached();
   }

   public boolean isActive() {
      return super.isActive() && (this.activeInterface == null || this.activeInterface.isActive());
   }

   public void onInit() {
      if (!this.init) {
         this.l = new GUITextOverlay(10, 10, this.font, this.getState());
         this.l.setTextSimple(this.text);
         this.attach(this.l);
         this.checkBox = new GUICheckBox(this.getState()) {
            protected void activate() throws StateParameterNotFoundException {
               GUICheckBoxTextPairNew.this.activate();
            }

            protected void deactivate() throws StateParameterNotFoundException {
               GUICheckBoxTextPairNew.this.deactivate();
            }

            protected boolean isActivated() {
               return GUICheckBoxTextPairNew.this.isChecked();
            }
         };
         this.checkBox.activeInterface = new GUIActiveInterface() {
            public boolean isActive() {
               return GUICheckBoxTextPairNew.this.isActive();
            }
         };
         this.attach(this.checkBox);
         this.init = true;
      }
   }

   public abstract void activate();

   public abstract void deactivate();

   public abstract boolean isChecked();

   public void cleanUp() {
      if (this.checkBox != null) {
         this.checkBox.cleanUp();
      }

      if (this.l != null) {
         this.l.cleanUp();
      }

   }

   public float getHeight() {
      return (float)this.height;
   }

   public float getWidth() {
      return (float)this.width;
   }
}
