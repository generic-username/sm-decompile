package org.schema.schine.graphicsengine.forms.gui;

public interface ColorPalletteInterface {
   void setColorPallete(GUITextButton.ColorPalette var1, GUITextButton var2);
}
