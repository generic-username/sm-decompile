package org.schema.schine.graphicsengine.forms.gui;

import java.util.Iterator;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.InputState;

public class GUIAncor extends GUIResizableElement {
   public GUIActiveInterface activationInterface;
   protected float height;
   protected float width;

   public GUIAncor(InputState var1) {
      super(var1);
   }

   public GUIAncor(InputState var1, float var2, float var3) {
      this(var1);
      this.width = var2;
      this.height = var3;
   }

   public void cleanUp() {
      Iterator var1 = this.getChilds().iterator();

      while(var1.hasNext()) {
         ((AbstractSceneNode)var1.next()).cleanUp();
      }

   }

   public void draw() {
      if (translateOnlyMode) {
         this.translate();
      } else {
         GlUtil.glPushMatrix();
         this.transform();
      }

      int var1 = this.getChilds().size();

      for(int var2 = 0; var2 < var1; ++var2) {
         ((AbstractSceneNode)this.getChilds().get(var2)).draw();
      }

      this.setInside(false);
      if (this.isRenderable() && this.isMouseUpdateEnabled()) {
         this.checkMouseInside();
      }

      if (translateOnlyMode) {
         this.translateBack();
      } else {
         GlUtil.glPopMatrix();
      }
   }

   public void drawWithoutTransform() {
      GlUtil.glPushMatrix();
      int var1 = this.getChilds().size();

      for(int var2 = 0; var2 < var1; ++var2) {
         ((AbstractSceneNode)this.getChilds().get(var2)).draw();
      }

      if (this.isRenderable() && this.isMouseUpdateEnabled()) {
         this.checkMouseInside();
      }

      GlUtil.glPopMatrix();
   }

   public void onInit() {
   }

   protected void doOrientation() {
   }

   public float getHeight() {
      return this.height;
   }

   public void setHeight(float var1) {
      this.height = var1;
   }

   public float getWidth() {
      return this.width;
   }

   public void setWidth(float var1) {
      this.width = var1;
   }

   public boolean isPositionCenter() {
      return false;
   }

   public boolean isActive() {
      return this.activationInterface == null ? super.isActive() : this.activationInterface.isActive();
   }

   public String toString() {
      return "GUIAncor" + super.toString();
   }
}
