package org.schema.schine.graphicsengine.forms.gui;

public interface TooltipProvider {
   void drawToolTip();
}
