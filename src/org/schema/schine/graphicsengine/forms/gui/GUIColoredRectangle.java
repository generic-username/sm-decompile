package org.schema.schine.graphicsengine.forms.gui;

import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.input.InputState;

public class GUIColoredRectangle extends GUIColoredAncor implements TooltipProviderCallback {
   public float rounded;
   protected int diaplayListIndex;
   protected boolean generated;
   private Vector4f color;
   private GUIResizableElement dependend;
   private GUIToolTip toolTip;

   public GUIColoredRectangle(InputState var1, float var2, float var3, Vector4f var4) {
      this(var1, var2, var3, (GUIResizableElement)null, var4);
   }

   public GUIColoredRectangle(InputState var1, float var2, float var3, GUIResizableElement var4, Vector4f var5) {
      super(var1, var2, var3);
      this.setColor(var5);
      this.dependend = var4;
   }

   protected void drawRect() {
      GlUtil.glColor4f(this.getColor().x, this.getColor().y, this.getColor().z, this.getColor().w);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glEnable(3042);
      GlUtil.glDisable(2929);
      GlUtil.glDisable(2896);
      GlUtil.glEnable(2903);
      GlUtil.glDisable(3553);
      GL11.glCallList(this.diaplayListIndex);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glDisable(3042);
      GlUtil.glEnable(2929);
      GlUtil.glDisable(2903);
   }

   public void draw() {
      if (this.dependend != null) {
         this.setWidth(this.dependend.getWidth());
         this.setHeight(this.dependend.getHeight());
      }

      if (!this.generated) {
         this.generateDisplayList();
      }

      GlUtil.glPushMatrix();
      this.transform();
      if (this.getColor().w > 0.0F) {
         assert this.generated;

         if (this.isRenderable()) {
            this.doDrawRect();
         }
      }

      GlUtil.glPopMatrix();
      this.drawSuper();
   }

   public void cleanUp() {
      if (this.generated && this.diaplayListIndex != 0) {
         GL11.glDeleteLists(this.diaplayListIndex, 1);
      }

      super.cleanUp();
   }

   public void onInit() {
      super.onInit();
      this.generateDisplayList();
   }

   public void setHeight(float var1) {
      if (this.height != var1) {
         this.generated = false;
      }

      this.height = var1;
   }

   public void setWidth(float var1) {
      if (this.width != var1) {
         this.generated = false;
      }

      this.width = var1;
   }

   protected void doDrawRect() {
      this.drawRect();
   }

   public void drawSuper() {
      super.draw();
   }

   protected void generateDisplayList() {
      if (this.diaplayListIndex != 0) {
         GL11.glDeleteLists(this.diaplayListIndex, 1);
      }

      this.diaplayListIndex = GL11.glGenLists(1);
      GL11.glNewList(this.diaplayListIndex, 4864);
      if (this.rounded == 0.0F) {
         GL11.glBegin(7);
         GL11.glVertex2f(0.0F, 0.0F);
         GL11.glVertex2f(0.0F, this.getHeight());
         GL11.glVertex2f(this.getWidth(), this.getHeight());
         GL11.glVertex2f(this.getWidth(), 0.0F);
      } else {
         GL11.glBegin(9);
         GL11.glVertex2f(0.0F, this.rounded);
         GL11.glVertex2f(0.0F, this.getHeight() - this.rounded);
         GL11.glVertex2f(this.rounded, this.getHeight());
         GL11.glVertex2f(this.getWidth() - this.rounded, this.getHeight());
         GL11.glVertex2f(this.getWidth(), this.getHeight() - this.rounded);
         GL11.glVertex2f(this.getWidth(), this.rounded);
         GL11.glVertex2f(this.getWidth() - this.rounded, 0.0F);
         GL11.glVertex2f(this.rounded, 0.0F);
         GL11.glVertex2f(this.rounded, this.rounded);
      }

      GL11.glEnd();
      GL11.glEndList();
      this.generated = true;
   }

   public Vector4f getColor() {
      return this.color;
   }

   public void setColor(Vector4f var1) {
      this.color = var1;
   }

   public void setWidth(int var1) {
      if (this.width != (float)var1) {
         this.generated = false;
      }

      this.width = (float)var1;
   }

   public void setHeight(int var1) {
      if (this.height != (float)var1) {
         this.generated = false;
      }

      this.height = (float)var1;
   }

   public GUIToolTip getToolTip() {
      return this.toolTip;
   }

   public void setToolTip(GUIToolTip var1) {
      this.toolTip = var1;
      if (this.getCallback() == null) {
         this.setCallback(new GUICallback() {
            public boolean isOccluded() {
               return !GUIColoredRectangle.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
            }
         });
      }

   }
}
