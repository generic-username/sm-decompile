package org.schema.schine.graphicsengine.forms.gui;

import javax.vecmath.Vector3f;

public interface GUIScrollableInterface {
   float getScolledPercentHorizontal();

   float getScolledPercentVertical();

   boolean isInside();

   void scrollHorizontal(float var1);

   void scrollVertical(float var1);

   ScrollingListener getScrollingListener();

   Vector3f getRelMousePos();

   void scrollHorizontalPercent(float var1);

   void scrollVerticalPercent(float var1);

   float getScrollBarHeight();

   float getScrollBarWidth();

   void scrollHorizontalPercentTmp(float var1);

   void scrollVerticalPercentTmp(float var1);

   boolean isVerticalActive();

   float getContentToPanelPercentageY();

   float getContentToPanelPercentageX();

   boolean isActive();
}
