package org.schema.schine.graphicsengine.forms.gui;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUILeftRigthShadow;
import org.schema.schine.input.InputState;

public class GUIColoredRectangleLeftRightShadow extends GUIColoredRectangle {
   private GUILeftRigthShadow lwshadow = new GUILeftRigthShadow(this.getState(), (int)this.getWidth(), (int)this.getHeight());

   public GUIColoredRectangleLeftRightShadow(InputState var1, float var2, float var3, Vector4f var4) {
      super(var1, var2, var3, var4);
      this.lwshadow.onInit();
   }

   protected void doDrawRect() {
      super.doDrawRect();
      this.lwshadow.setWidth(this.getWidth());
      this.lwshadow.setHeight(this.getHeight());
      this.lwshadow.draw();
   }
}
