package org.schema.schine.graphicsengine.forms.gui;

import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Mouse;

public class DraggableAncor extends GUIAncor {
   public static boolean globalDragging;
   private boolean dragging;
   private Vector3i draggingStartPos = new Vector3i();
   private GUIElement affected;
   private boolean mouseDown;

   public DraggableAncor(InputState var1, float var2, float var3, GUIElement var4) {
      super(var1, var2, var3);
      this.setAffected(var4);
   }

   public void draw() {
      this.setMouseUpdateEnabled(true);
      super.draw();
      if (this.isInside() && Mouse.isButtonDown(0) && !this.mouseDown && !this.dragging) {
         this.dragging = true;
         globalDragging = true;
         this.draggingStartPos.set(Mouse.getX(), GLFrame.getHeight() - Mouse.getY(), 0);
      }

      this.mouseDown = Mouse.isButtonDown(0);
      if (this.dragging) {
         Vector3i var1;
         (var1 = new Vector3i(Mouse.getX(), GLFrame.getHeight() - Mouse.getY(), 0)).sub(this.draggingStartPos);
         Vector3f var10000 = this.getAffected().getPos();
         var10000.x += (float)var1.x;
         var10000 = this.getAffected().getPos();
         var10000.y += (float)var1.y;
         this.draggingStartPos.set(Mouse.getX(), GLFrame.getHeight() - Mouse.getY(), 0);
         if (!Mouse.isButtonDown(0)) {
            this.dragging = false;
            globalDragging = false;
         }
      }

      this.getAffected().orientateInsideFrame();
   }

   public GUIElement getAffected() {
      return this.affected;
   }

   public void setAffected(GUIElement var1) {
      this.affected = var1;
   }
}
