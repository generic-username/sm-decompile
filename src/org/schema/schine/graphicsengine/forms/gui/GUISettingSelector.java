package org.schema.schine.graphicsengine.forms.gui;

import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.NamedValueInterface;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.InputState;

public class GUISettingSelector extends GUISettingsElement implements GUICallback {
   private GUITextOverlay settingName;
   private GUIOverlay leftArrow;
   private GUIOverlay rightArrow;
   private boolean checked;
   private SettingsInterface setting;
   public GUIElement dependent;
   private boolean init;
   private GUIActiveInterface activeInterface;

   public GUISettingSelector(InputState var1, int var2, int var3, GUIActiveInterface var4, SettingsInterface var5) {
      super(var1);
      this.checked = false;
      this.setMouseUpdateEnabled(true);
      this.setCallback(this);
      this.setting = var5;
      this.leftArrow = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "tools-16x16-gui-"), this.getState());
      this.rightArrow = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "tools-16x16-gui-"), this.getState());
      this.settingName = new GUITextOverlay(var2, var3, FontLibrary.getBoldArialWhite14(), this.getState());
      this.activeInterface = var4;

      assert this.setting != null;

      assert this.setting.getCurrentState() != null;

   }

   public GUISettingSelector(InputState var1, GUIActiveInterface var2, SettingsInterface var3) {
      this(var1, 140, 30, var2, var3);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.getEventButtonState() && var2.getEventButton() == 0) {
         this.checked = !this.checked;

         try {
            this.setting.switchSetting();
            return;
         } catch (StateParameterNotFoundException var3) {
            var3.printStackTrace();
            GLFrame.processErrorDialogException(var3, this.getState());
         }
      }

   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      if (this.dependent != null) {
         this.rightArrow.getPos().x = this.dependent.getWidth() - this.rightArrow.getWidth() - 12.0F;
      } else {
         this.rightArrow.getPos().x = this.leftArrow.getWidth() + this.settingName.getWidth();
      }

      this.settingName.getPos().x = (float)((int)(this.leftArrow.getWidth() + (this.rightArrow.getPos().x - this.leftArrow.getWidth()) / 2.0F - (float)(this.settingName.getMaxLineWidth() / 2)));
      this.settingName.draw();
      this.leftArrow.draw();
      this.rightArrow.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.settingName.setTextSimple(new Object() {
         public String toString() {
            assert GUISettingSelector.this.setting != null;

            assert GUISettingSelector.this.setting.getCurrentState() != null;

            return GUISettingSelector.this.setting.getCurrentState() instanceof NamedValueInterface ? ((NamedValueInterface)GUISettingSelector.this.setting.getCurrentState()).getName() : GUISettingSelector.this.setting.getCurrentState().toString();
         }
      });
      this.settingName.onInit();
      Vector3f var10000 = this.settingName.getPos();
      var10000.y += 13.0F;
      this.leftArrow.setMouseUpdateEnabled(true);
      this.rightArrow.setMouseUpdateEnabled(true);
      var10000 = this.leftArrow.getPos();
      var10000.y += 3.0F;
      var10000 = this.rightArrow.getPos();
      var10000.y += 3.0F;
      this.leftArrow.setCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.getEventButtonState() && var2.getEventButton() == 0) {
               GUISettingSelector.this.checked = !GUISettingSelector.this.checked;

               try {
                  GUISettingSelector.this.setting.switchSettingBack();
                  GUISettingSelector.this.setting.onSwitchedSetting(GUISettingSelector.this.getState());
                  return;
               } catch (StateParameterNotFoundException var3) {
                  var3.printStackTrace();
                  GLFrame.processErrorDialogException(var3, GUISettingSelector.this.getState());
               }
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.rightArrow.setCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.getEventButtonState() && var2.getEventButton() == 0) {
               GUISettingSelector.this.checked = !GUISettingSelector.this.checked;

               try {
                  GUISettingSelector.this.setting.switchSetting();
                  GUISettingSelector.this.setting.onSwitchedSetting(GUISettingSelector.this.getState());
                  return;
               } catch (StateParameterNotFoundException var3) {
                  var3.printStackTrace();
                  GLFrame.processErrorDialogException(var3, GUISettingSelector.this.getState());
               }
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.leftArrow.setSpriteSubIndex(21);
      this.rightArrow.setSpriteSubIndex(20);
      this.init = true;
   }

   protected void doOrientation() {
   }

   public float getHeight() {
      return 30.0F;
   }

   public float getWidth() {
      return this.settingName.getWidth() + this.leftArrow.getWidth() + this.rightArrow.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public boolean isOccluded() {
      return this.activeInterface == null || !this.activeInterface.isActive();
   }
}
