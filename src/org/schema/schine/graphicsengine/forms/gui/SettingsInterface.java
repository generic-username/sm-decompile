package org.schema.schine.graphicsengine.forms.gui;

import org.schema.schine.graphicsengine.core.settings.EngineSettingsChangeListener;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.input.InputState;

public interface SettingsInterface {
   void switchSetting() throws StateParameterNotFoundException;

   Object getCurrentState();

   void setCurrentState(Object var1);

   void switchSettingBack() throws StateParameterNotFoundException;

   void onSwitchedSetting(InputState var1);

   void addChangeListener(EngineSettingsChangeListener var1);

   void removeChangeListener(EngineSettingsChangeListener var1);
}
