package org.schema.schine.graphicsengine.forms.gui;

import java.util.List;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.network.client.ClientState;

public class GUIChatLogPanel extends GUIElement {
   private GUIColoredRectangle background;
   private GUIScrollableTextPanel scroll;
   private GUITextOverlay text;
   private GUIAncor textAnc;

   public GUIChatLogPanel(int var1, int var2, FontLibrary.FontSize var3, GUIResizableElement var4, ClientState var5) {
      super(var5);
      if (isNewHud()) {
         this.text = new GUITextOverlay(var1, var2, var3, var5) {
            public float getHeight() {
               return (float)this.getTextHeight();
            }
         };
         if (var4 != null) {
            this.text.autoWrapOn = var4;
         }
      } else {
         this.text = new GUITextOverlay(var1, var2, var3, var5);
      }

      this.text.setLimitTextDraw(100);
      this.text.setText(this.getState().getGeneralChatLog());
      this.textAnc = new GUIAncor(this.getState());
      this.textAnc.attach(this.text);
      this.scroll = new GUIScrollableTextPanel((float)var1, (float)var2, var4, var5) {
         public boolean isScrollLock() {
            return true;
         }
      };
      this.scroll.setScrollable(0);
      this.background = new GUIColoredRectangle(var5, (float)var1, (float)var2, var4, new Vector4f(1.0F, 1.0F, 1.0F, 0.1F));
      this.background.rounded = 1.0F;
   }

   public void setText(List var1) {
      this.text.setText(var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      this.background.draw();
   }

   public void onInit() {
      this.background.rounded = 5.0F;
      this.background.attach(this.scroll);
      this.scroll.setContent(this.text);
      this.background.onInit();
      this.scroll.onInit();
      this.text.onInit();
   }

   public float getHeight() {
      return 0.0F;
   }

   public float getWidth() {
      return 0.0F;
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void onChatDirty() {
      this.text.updateTextSize();
   }
}
