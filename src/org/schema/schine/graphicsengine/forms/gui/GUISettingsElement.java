package org.schema.schine.graphicsengine.forms.gui;

import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.input.InputState;

public abstract class GUISettingsElement extends GUIElement implements TooltipProvider {
   private GUIToolTip toolTip = this.initToolTip();

   public GUISettingsElement(InputState var1) {
      super(var1);
   }

   public GUIToolTip initToolTip() {
      return null;
   }

   public void settingChanged(Object var1) {
   }

   public void drawToolTip() {
      if (this.toolTip != null) {
         GlUtil.glPushMatrix();
         this.toolTip.draw();
         GlUtil.glPopMatrix();
      }

   }
}
