package org.schema.schine.graphicsengine.forms.gui.graph;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class GUIGraph extends GUIElement {
   float width;
   float height;
   public float arrowSize = 7.0F;
   private ObjectArrayList elements = new ObjectArrayList();
   private ObjectArrayList connections = new ObjectArrayList();

   public GUIGraph(InputState var1) {
      super(var1);
   }

   public List getConnections(GUIGraphElement var1) {
      ObjectArrayList var2 = new ObjectArrayList();
      Iterator var3 = this.connections.iterator();

      while(var3.hasNext()) {
         GUIGraphConnection var4;
         if ((var4 = (GUIGraphConnection)var3.next()).a == var1) {
            var2.add(var4.b);
         }
      }

      return var2;
   }

   public GUIGraphElement addVertex(GUIGraphElement var1) {
      this.elements.add(var1);
      this.width = Math.max(this.width, var1.getPos().x + var1.getWidth());
      this.height = Math.max(this.height, var1.getPos().y + var1.getHeight());
      return var1;
   }

   public GUIGraphConnection addConnection(GUIGraphElement var1, GUIGraphElement var2) {
      GUIGraphConnection var3 = new GUIGraphConnection(var1, var2);
      this.connections.add(var3);
      return var3;
   }

   public GUIGraphConnection addConnection(GUIGraphElement var1, GUIGraphElement var2, Vector4f var3) {
      GUIGraphConnection var4 = new GUIGraphConnection(var1, var2, var3);
      this.connections.add(var4);
      return var4;
   }

   public void cleanUp() {
      Iterator var1 = this.elements.iterator();

      while(var1.hasNext()) {
         ((GUIGraphElement)var1.next()).cleanUp();
      }

      var1 = this.connections.iterator();

      while(var1.hasNext()) {
         ((GUIGraphConnection)var1.next()).cleanUp();
      }

   }

   public void draw() {
      GlUtil.glPushMatrix();
      this.transform();
      this.drawGraph();
      Iterator var1 = this.getChilds().iterator();

      while(var1.hasNext()) {
         ((AbstractSceneNode)var1.next()).draw();
      }

      if (this.isMouseUpdateEnabled()) {
         this.checkMouseInside();
      }

      GlUtil.glPopMatrix();
   }

   public void onInit() {
   }

   private void drawGraph() {
      this.drawConnections();
      this.drawNodes();
   }

   private void drawNodes() {
      Iterator var1 = this.elements.iterator();

      while(var1.hasNext()) {
         ((GUIElement)var1.next()).draw();
      }

   }

   private void drawConnections() {
      GUIGraphConnection.err();
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
      Iterator var1 = this.connections.iterator();

      while(var1.hasNext()) {
         GUIGraphConnection var2 = (GUIGraphConnection)var1.next();

         assert var2.a != null;

         assert var2.b != null;

         var2.draw(this.arrowSize);
      }

      GUIGraphConnection.afterDraw();
      GlUtil.glDisable(3042);
      GUIGraphConnection.err();
   }

   public float getHeight() {
      return this.height;
   }

   public float getWidth() {
      return this.width;
   }

   public boolean isEmptyGraph() {
      return this.elements.isEmpty();
   }
}
