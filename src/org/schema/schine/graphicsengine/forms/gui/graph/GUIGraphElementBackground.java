package org.schema.schine.graphicsengine.forms.gui.graph;

import org.schema.schine.graphicsengine.forms.gui.newgui.GUIFilledArea;
import org.schema.schine.input.InputState;

public abstract class GUIGraphElementBackground extends GUIFilledArea {
   public GUIGraphElementBackground(InputState var1, int var2, int var3) {
      super(var1, var2, var3);
   }
}
