package org.schema.schine.graphicsengine.forms.gui.graph;

import org.schema.schine.input.InputState;

public abstract class GUIGraphElementGraphicsGlobal {
   private final InputState state;

   public GUIGraphElementGraphicsGlobal(InputState var1) {
      this.state = var1;
   }

   public InputState getState() {
      return this.state;
   }

   public abstract boolean isActive();
}
