package org.schema.schine.graphicsengine.forms.gui;

import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.input.InputState;

public class GUIColoredGradientRectangle extends GUIAncor {
   public Vector4f gradient;
   public float rounded;
   protected int diaplayListIndex;
   protected boolean generated;
   private Vector4f color;
   private Vector4f lastColor = new Vector4f();
   private Vector4f lastGradient = new Vector4f();

   public GUIColoredGradientRectangle(InputState var1, float var2, float var3, Vector4f var4) {
      super(var1, var2, var3);
      this.setColor(var4);
      this.gradient = new Vector4f(var4);
      this.lastColor.set(var4);
      this.lastGradient.set(var4);
   }

   public void draw() {
      if (!this.generated) {
         this.generateDisplayList();
      }

      GlUtil.glPushMatrix();
      this.transform();
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glEnable(3042);
      GlUtil.glDisable(2929);
      GlUtil.glDisable(2896);
      GlUtil.glEnable(2903);
      GlUtil.glDisable(3553);
      GlUtil.glColor4f(this.getColor().x, this.getColor().y, this.getColor().z, this.getColor().w);

      assert this.generated;

      GL11.glCallList(this.diaplayListIndex);
      GlUtil.glDisable(2903);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glDisable(3042);
      GlUtil.glEnable(2929);
      GlUtil.glPopMatrix();
      this.drawSuper();
      if (!this.lastGradient.equals(this.gradient)) {
         this.generated = false;
      }

      GlUtil.glColor4f(0.3F, 0.3F, 0.3F, 0.3F);
   }

   public void onInit() {
      super.onInit();
      this.generateDisplayList();
   }

   public void drawSuper() {
      super.draw();
   }

   protected void generateDisplayList() {
      if (this.diaplayListIndex != 0) {
         GL11.glDeleteLists(this.diaplayListIndex, 1);
      }

      this.diaplayListIndex = GL11.glGenLists(1);
      GL11.glNewList(this.diaplayListIndex, 4864);
      if (this.rounded == 0.0F) {
         GL11.glBegin(7);
         GL11.glVertex2f(0.0F, 0.0F);
         GL11.glVertex2f(0.0F, this.getHeight());
         GlUtil.glColor4f(this.gradient.x, this.gradient.y, this.gradient.z, this.gradient.w);
         GL11.glVertex2f(this.getWidth(), this.getHeight());
         GL11.glVertex2f(this.getWidth(), 0.0F);
      } else {
         GL11.glBegin(9);
         GL11.glVertex2f(0.0F, this.rounded);
         GL11.glVertex2f(0.0F, this.getHeight() - this.rounded);
         GL11.glVertex2f(this.rounded, this.getHeight());
         GL11.glVertex2f(this.getWidth() - this.rounded, this.getHeight());
         GL11.glVertex2f(this.getWidth(), this.getHeight() - this.rounded);
         GlUtil.glColor4f(this.gradient.x, this.gradient.y, this.gradient.z, this.gradient.w);
         GL11.glVertex2f(this.getWidth(), this.rounded);
         GL11.glVertex2f(this.getWidth() - this.rounded, 0.0F);
         GL11.glVertex2f(this.rounded, 0.0F);
         GL11.glVertex2f(this.rounded, this.rounded);
      }

      GL11.glEnd();
      GL11.glEndList();
      this.lastColor.set(this.color);
      this.lastGradient.set(this.gradient);
      this.generated = true;
   }

   public Vector4f getColor() {
      return this.color;
   }

   public void setColor(Vector4f var1) {
      this.color = var1;
   }

   public void setWidth(int var1) {
      this.width = (float)var1;
      this.generated = false;
   }

   public void setHeight(int var1) {
      this.height = (float)var1;
      this.generated = false;
   }
}
