package org.schema.schine.graphicsengine.forms.gui;

import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.input.InputState;

public class GUIColoredRectangleOutline extends GUIAncor {
   protected int diaplayListIndex;
   protected boolean generated;
   private Vector4f color;
   private int inset;

   public GUIColoredRectangleOutline(InputState var1, float var2, float var3, int var4, Vector4f var5) {
      super(var1, var2, var3);
      this.setColor(var5);
      this.setInset(var4);
   }

   public void draw() {
      if (!this.generated) {
         this.generateDisplayList();
      }

      GlUtil.glPushMatrix();
      this.transform();
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glEnable(3042);
      GlUtil.glDisable(2929);
      GlUtil.glDisable(2896);
      GlUtil.glEnable(2903);
      GlUtil.glDisable(3553);
      GlUtil.glColor4f(this.getColor().x, this.getColor().y, this.getColor().z, this.getColor().w);

      assert this.generated;

      if (this.isRenderable()) {
         GL11.glCallList(this.diaplayListIndex);
      }

      GlUtil.glDisable(2903);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glDisable(3042);
      GlUtil.glEnable(2929);
      GlUtil.glPopMatrix();
      this.drawSuper();
   }

   public void onInit() {
      super.onInit();
      this.generateDisplayList();
   }

   public void drawSuper() {
      super.draw();
   }

   public void cleanUp() {
      super.cleanUp();
      if (this.diaplayListIndex != 0) {
         GL11.glDeleteLists(this.diaplayListIndex, 1);
      }

   }

   protected void generateDisplayList() {
      if (this.diaplayListIndex != 0) {
         GL11.glDeleteLists(this.diaplayListIndex, 1);
      }

      this.diaplayListIndex = GL11.glGenLists(1);
      GL11.glNewList(this.diaplayListIndex, 4864);
      GL11.glBegin(7);
      GL11.glVertex2f(0.0F, 0.0F);
      GL11.glVertex2f(0.0F, (float)this.inset);
      GL11.glVertex2f(this.getWidth(), (float)this.inset);
      GL11.glVertex2f(this.getWidth(), 0.0F);
      GL11.glVertex2f(0.0F, this.getHeight() - (float)this.inset);
      GL11.glVertex2f(0.0F, this.getHeight());
      GL11.glVertex2f(this.getWidth(), this.getHeight());
      GL11.glVertex2f(this.getWidth(), this.getHeight() - (float)this.inset);
      GL11.glVertex2f(0.0F, (float)this.inset);
      GL11.glVertex2f(0.0F, this.getHeight() - (float)this.inset);
      GL11.glVertex2f((float)this.inset, this.getHeight() - (float)this.inset);
      GL11.glVertex2f((float)this.inset, (float)this.inset);
      GL11.glVertex2f(this.getWidth() - (float)this.inset, (float)this.inset);
      GL11.glVertex2f(this.getWidth() - (float)this.inset, this.getHeight() - (float)this.inset);
      GL11.glVertex2f(this.getWidth(), this.getHeight() - (float)this.inset);
      GL11.glVertex2f(this.getWidth(), (float)this.inset);
      GL11.glEnd();
      GL11.glEndList();
      this.generated = true;
   }

   public Vector4f getColor() {
      return this.color;
   }

   public void setColor(Vector4f var1) {
      this.color = var1;
   }

   public int getInset() {
      return this.inset;
   }

   public void setInset(int var1) {
      this.inset = var1;
   }

   public void setWidth(int var1) {
      this.width = (float)var1;
      this.generated = false;
   }

   public void setHeight(int var1) {
      this.height = (float)var1;
      this.generated = false;
   }
}
