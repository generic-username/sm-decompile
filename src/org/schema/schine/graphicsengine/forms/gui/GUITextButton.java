package org.schema.schine.graphicsengine.forms.gui;

import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.List;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.newdawn.slick.Color;
import org.newdawn.slick.UnicodeFont;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUINewButtonBackground;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Mouse;

public class GUITextButton extends GUIAncor implements GUIButtonInterface, TooltipProviderCallback {
   public static ColorPalletteInterface cp;
   private final Vector4f foregroundColorText;
   private final Vector4f backgroundColor;
   private final Vector4f inactiveColor;
   private final Vector4f mouseOverlayBackgroundColor;
   private final List texts;
   private final List beforeIcons;
   private final List afterIcons;
   public int centeredOffsetY;
   protected GUIColoredAncor background;
   private GUITextOverlay text;
   private UnicodeFont font;
   private Vector4f selectColorText;
   private Vector4f pressedColorText;
   private boolean init;
   private Vector3f textPos;
   private Suspendable suspendable;
   private boolean active;
   private boolean centered;
   private boolean updateTextPos;
   private Vector4f mouseOverlayPressedBackgroundColor;
   private GUIToolTip toolTip;
   private Object2IntOpenHashMap befAftPosesX;
   private Object2IntOpenHashMap befAftPosesY;

   public GUITextButton(InputState var1, int var2, int var3, Object var4, GUICallback var5) {
      this(var1, var2, var3, new Vector4f(0.3F, 0.3F, 0.6F, 0.9F), new Vector4f(0.99F, 0.99F, 0.99F, 1.0F), isNewHud() ? FontLibrary.getBlenderProMedium14() : FontLibrary.getRegularArial15White(), var4, var5);
   }

   public GUITextButton(InputState var1, int var2, int var3, Object var4, GUICallback var5, Suspendable var6) {
      this(var1, var2, var3, new Vector4f(0.3F, 0.3F, 0.6F, 0.9F), new Vector4f(0.99F, 0.99F, 0.99F, 1.0F), isNewHud() ? FontLibrary.getBlenderProMedium14() : FontLibrary.getRegularArial15White(), var4, var5, var6);
   }

   public GUITextButton(InputState var1, int var2, int var3, UnicodeFont var4, Object var5, GUICallback var6) {
      this(var1, var2, var3, new Vector4f(0.3F, 0.3F, 0.6F, 0.9F), new Vector4f(0.99F, 0.99F, 0.99F, 1.0F), var4, var5, var6);
   }

   public GUITextButton(InputState var1, int var2, int var3, Vector4f var4, Vector4f var5, UnicodeFont var6, Object var7, GUICallback var8) {
      this(var1, var2, var3, var4, var5, var6, var7, var8, (Suspendable)null);
   }

   public GUITextButton(InputState var1, int var2, int var3, Vector4f var4, Vector4f var5, UnicodeFont var6, Object var7, GUICallback var8, Suspendable var9) {
      super(var1, (float)var2, (float)var3);
      this.inactiveColor = new Vector4f(0.3F, 0.3F, 0.3F, 0.9F);
      this.texts = new ObjectArrayList();
      this.beforeIcons = new ObjectArrayList();
      this.afterIcons = new ObjectArrayList();
      this.centeredOffsetY = -1;
      this.textPos = new Vector3f();
      this.active = true;
      this.centered = true;
      this.updateTextPos = true;
      this.befAftPosesX = new Object2IntOpenHashMap();
      this.befAftPosesY = new Object2IntOpenHashMap();
      this.backgroundColor = var4;
      this.mouseOverlayBackgroundColor = new Vector4f();
      this.mouseOverlayBackgroundColor.set(var4);
      this.mouseOverlayPressedBackgroundColor = new Vector4f();
      this.mouseOverlayPressedBackgroundColor.set(var4);
      this.foregroundColorText = var5;
      this.selectColorText = new Vector4f(0.8F, 0.8F, 1.0F, 1.0F);
      this.pressedColorText = new Vector4f(1.0F, 0.8F, 0.8F, 1.0F);
      this.font = var6;
      this.setCallback(var8);
      this.setMouseUpdateEnabled(true);
      this.texts.add(var7);
      this.suspendable = var9;
   }

   public GUITextButton(InputState var1, int var2, int var3, GUITextButton.ColorPalette var4, UnicodeFont var5, Object var6, GUICallback var7) {
      this(var1, var2, var3, (GUITextButton.ColorPalette)var4, (UnicodeFont)var5, (Object)var6, (GUICallback)var7, (Suspendable)null);
   }

   public GUITextButton(InputState var1, int var2, int var3, GUITextButton.ColorPalette var4, UnicodeFont var5, Object var6, GUICallback var7, Suspendable var8) {
      super(var1, (float)var2, (float)var3);
      this.inactiveColor = new Vector4f(0.3F, 0.3F, 0.3F, 0.9F);
      this.texts = new ObjectArrayList();
      this.beforeIcons = new ObjectArrayList();
      this.afterIcons = new ObjectArrayList();
      this.centeredOffsetY = -1;
      this.textPos = new Vector3f();
      this.active = true;
      this.centered = true;
      this.updateTextPos = true;
      this.befAftPosesX = new Object2IntOpenHashMap();
      this.befAftPosesY = new Object2IntOpenHashMap();
      this.backgroundColor = new Vector4f();
      this.mouseOverlayBackgroundColor = new Vector4f();
      this.mouseOverlayPressedBackgroundColor = new Vector4f();
      this.foregroundColorText = new Vector4f();
      this.selectColorText = new Vector4f();
      this.pressedColorText = new Vector4f();
      this.font = var5;
      this.setCallback(var7);
      this.setMouseUpdateEnabled(true);
      this.texts.add(var6);
      this.suspendable = var8;
      this.setColorPalette(var4);
   }

   public GUITextButton(InputState var1, int var2, int var3, GUITextButton.ColorPalette var4, Object var5, GUICallback var6, Suspendable var7) {
      this(var1, var2, var3, var4, FontLibrary.getBlenderProMedium14(), var5, var6, var7);
   }

   public GUITextButton(InputState var1, int var2, int var3, GUITextButton.ColorPalette var4, Object var5, GUICallback var6) {
      this(var1, var2, var3, (GUITextButton.ColorPalette)var4, (UnicodeFont)FontLibrary.getBlenderProMedium14(), (Object)var5, (GUICallback)var6, (Suspendable)null);
   }

   public void setColorPalette(GUITextButton.ColorPalette var1) {
      assert cp != null;

      cp.setColorPallete(var1, this);
   }

   public void removeBeforeIcon(GUIElement var1) {
      this.beforeIcons.remove(var1);
      this.detach(var1);
      this.removeBeforOrAfterIconPos(var1);
   }

   public void setBeforOrAfterIconPos(GUIElement var1, int var2, int var3) {
      this.befAftPosesX.put(var1, var2);
      this.befAftPosesY.put(var1, var3);
   }

   public void removeBeforOrAfterIconPos(GUIElement var1) {
      this.befAftPosesX.remove(var1);
      this.befAftPosesY.remove(var1);
   }

   public void addBeforeIcon(GUIElement var1) {
      this.beforeIcons.add(var1);
      this.attach(var1);
   }

   public void addAfterIcon(GUIElement var1) {
      this.afterIcons.add(var1);
      this.attach(var1);
   }

   public void removeAfterIcon(GUIElement var1) {
      this.afterIcons.remove(var1);
      this.detach(var1);
      this.removeBeforOrAfterIconPos(var1);
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (!this.isActive()) {
         this.text.setColor(this.getColorText());
         Color var10000 = this.text.getColor();
         var10000.a /= 2.0F;
         this.setMouseUpdateEnabled(false);
      } else if (this.suspendable == null || !this.suspendable.isSuspended() && this.suspendable.isActive() && !this.suspendable.isHinderedInteraction()) {
         if (!this.isActive()) {
            this.background.setColor(this.inactiveColor);
            this.text.setColor(this.getColorText());
         } else if (this.isInside() && (this.getCallback() == null || !this.getCallback().isOccluded())) {
            if (Mouse.isButtonDown(0)) {
               this.background.setColor(this.mouseOverlayPressedBackgroundColor);
               this.text.setColor(this.pressedColorText);
            } else {
               this.background.setColor(this.getBackgroundColorMouseOverlay());
               this.text.setColor(this.selectColorText);
            }
         } else {
            this.background.setColor(this.getBackgroundColor());
            this.text.setColor(this.getColorText());
         }
      } else {
         this.text.setColor(this.getColorText());
         this.setMouseUpdateEnabled(false);
      }

      if (this.updateTextPos) {
         this.updateTextPos = false;
         int var1 = 0;
         int var2 = 0;

         int var3;
         for(var3 = 0; var3 < this.beforeIcons.size(); ++var3) {
            GUIElement var4 = (GUIElement)this.beforeIcons.get(var3);
            var2 = (int)((float)var2 + var4.getWidth());
            var1 = (int)Math.max((float)var1, var4.getHeight());
         }

         int var5;
         int var6;
         int var7;
         GUIElement var8;
         int var9;
         if (this.centered) {
            var3 = 0;

            for(var9 = 0; var9 < this.afterIcons.size(); ++var9) {
               this.afterIcons.get(var9);
               var3 = (int)((float)var3 + ((GUIElement)this.afterIcons.get(var9)).getWidth());
            }

            var9 = var2 + this.text.getFont().getWidth(this.texts.get(0).toString()) + var3;
            var1 = this.text.getFont().getWidth(this.texts.get(0).toString());
            var2 = this.text.getFont().getHeight(this.texts.get(0).toString());
            var5 = (int)(this.width / 2.0F - (float)(var9 / 2));
            var6 = (int)(this.height / 2.0F - (float)(var2 / 2)) + this.centeredOffsetY;

            for(var7 = 0; var7 < this.beforeIcons.size(); ++var7) {
               var8 = (GUIElement)this.beforeIcons.get(var7);
               var3 = this.befAftPosesX.getInt(var8);
               var9 = this.befAftPosesY.getInt(var8);
               var8.setPos((float)(var5 + var3), (float)(var6 + var9), 0.0F);
               var5 = (int)((float)var5 + var8.getWidth());
            }

            this.text.setPos((float)var5, (float)var6, 0.0F);
            var5 += var1;

            for(var7 = 0; var7 < this.afterIcons.size(); ++var7) {
               var8 = (GUIElement)this.afterIcons.get(var7);
               var3 = this.befAftPosesX.getInt(var8);
               var9 = this.befAftPosesY.getInt(var8);
               var8.setPos((float)(var5 + var3), (float)(var6 + var9), 0.0F);
               var5 = (int)((float)var5 + var8.getWidth());
            }
         } else {
            var3 = (int)this.textPos.x;
            var9 = (int)this.textPos.y;

            for(var1 = 0; var1 < this.beforeIcons.size(); ++var1) {
               var8 = (GUIElement)this.beforeIcons.get(var1);
               var5 = this.befAftPosesX.getInt(var8);
               var6 = this.befAftPosesY.getInt(var8);
               var8.setPos((float)(var3 + var5), (float)(var9 + var6), 0.0F);
               var3 = (int)((float)var3 + var8.getWidth());
            }

            this.text.setPos((float)var3, (float)var9, 0.0F);
            var1 = this.text.getFont().getWidth(this.texts.get(0).toString());
            var3 += var1;

            for(var2 = 0; var2 < this.afterIcons.size(); ++var2) {
               GUIElement var10 = (GUIElement)this.afterIcons.get(var2);
               var6 = this.befAftPosesX.getInt(var10);
               var7 = this.befAftPosesY.getInt(var10);
               var10.setPos((float)(var3 + var6), (float)(var9 + var7), 0.0F);
               var3 = (int)((float)var3 + var10.getWidth());
            }
         }
      }

      super.draw();
      this.setMouseUpdateEnabled(true);
   }

   public void onInit() {
      if (!this.init) {
         this.text = new GUITextOverlay((int)this.width, (int)this.height, this.font, this.getState());
         this.text.setText(this.texts);
         this.background = new GUINewButtonBackground(this.getState(), (int)this.width, (int)this.height);
         this.background.setColor(this.getBackgroundColor());
         this.background.attach(this.text);
         this.attach(this.background);
         super.onInit();
         this.init = true;
      }
   }

   public void onResize() {
      this.background.setWidth(this.width);
      this.background.setHeight(this.height);
   }

   public void setHeight(float var1) {
      super.setHeight(var1);
      if (this.background != null) {
         this.background.setHeight(var1);
      }

   }

   public void setWidth(float var1) {
      super.setWidth(var1);
      if (this.background != null) {
         this.background.setWidth(var1);
      }

   }

   public boolean isActive() {
      return this.active;
   }

   public void setActive(boolean var1) {
      this.active = var1;
   }

   public Vector4f getBackgroundColor() {
      return this.backgroundColor;
   }

   public Vector4f getColorText() {
      return this.foregroundColorText;
   }

   public void setText(Object var1) {
      if (!this.texts.get(0).equals(var1)) {
         this.texts.set(0, var1);
         this.updateTextPos = true;
      }

   }

   public void setTextPos(int var1, int var2) {
      if (this.centered || this.textPos.x != (float)var1 || this.textPos.y != (float)var2) {
         this.centered = false;
         this.textPos.set((float)var1, (float)var2, 0.0F);
         this.updateTextPos = true;
      }

   }

   public void setTextCentered() {
      if (!this.centered) {
         this.centered = true;
         this.updateTextPos = true;
      }

   }

   public List getTextList() {
      return this.text.getText();
   }

   public Vector4f getBackgroundColorMouseOverlay() {
      return this.mouseOverlayBackgroundColor;
   }

   public void setInvisible(boolean var1) {
   }

   public Vector4f getBackgroundColorMouseOverlayPressed() {
      return this.mouseOverlayPressedBackgroundColor;
   }

   public Vector4f getColorTextPressed() {
      return this.pressedColorText;
   }

   public Vector4f getColorTextMouseOver() {
      return this.selectColorText;
   }

   public GUIToolTip getToolTip() {
      return this.toolTip;
   }

   public void setToolTip(GUIToolTip var1) {
      this.toolTip = var1;
   }

   public static enum ColorPalette {
      OK,
      CANCEL,
      TUTORIAL,
      FRIENDLY,
      HOSTILE,
      NEUTRAL,
      TRANSPARENT;
   }
}
