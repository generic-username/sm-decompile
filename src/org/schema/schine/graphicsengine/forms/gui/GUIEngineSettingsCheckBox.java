package org.schema.schine.graphicsengine.forms.gui;

import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.InputState;

public class GUIEngineSettingsCheckBox extends GUICheckBox {
   private EngineSettings booleanSettings;

   public GUIEngineSettingsCheckBox(InputState var1, GUIActiveInterface var2, EngineSettings var3) {
      super(var1);
      this.booleanSettings = var3;
      this.activeInterface = var2;

      assert var3.getCurrentState() instanceof Boolean;

   }

   protected void activate() throws StateParameterNotFoundException {
      this.booleanSettings.switchSetting();
      this.booleanSettings.onSwitchedBoolean(this.getState());
   }

   protected void deactivate() throws StateParameterNotFoundException {
      this.booleanSettings.switchSetting();
      this.booleanSettings.onSwitchedBoolean(this.getState());
   }

   protected boolean isActivated() {
      return this.booleanSettings.isOn();
   }
}
