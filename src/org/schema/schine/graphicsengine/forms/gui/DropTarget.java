package org.schema.schine.graphicsengine.forms.gui;

import org.schema.schine.graphicsengine.core.MouseEvent;

public interface DropTarget {
   void checkTarget(MouseEvent var1);

   boolean isTarget(Draggable var1);

   void onDrop(Draggable var1);
}
