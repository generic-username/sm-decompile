package org.schema.schine.graphicsengine.forms.gui;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector4f;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.input.InputState;

public class GUITintScreenElement extends GUIElement {
   private final Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 0.5F);
   private int diaplayListIndex;
   private boolean generated;
   private int border = -1;

   public GUITintScreenElement(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      GlUtil.glColor4f(this.color.x, this.color.y, this.color.z, this.color.w);

      assert this.generated;

      GL11.glCallList(this.diaplayListIndex);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
   }

   public void onInit() {
      this.generateDisplayList();
   }

   protected void doOrientation() {
   }

   public float getHeight() {
      return (float)GLFrame.getHeight();
   }

   public float getWidth() {
      return (float)GLFrame.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   private void generateDisplayList() {
      this.diaplayListIndex = GL11.glGenLists(1);
      GL11.glNewList(this.diaplayListIndex, 4864);
      GlUtil.glEnable(3042);
      GlUtil.glDisable(3553);
      GlUtil.glEnable(2903);
      GlUtil.glDisable(2896);
      GlUtil.glBlendFunc(770, 771);
      GL11.glBegin(7);
      if (this.border >= 0) {
         GL11.glVertex2f(0.0F, 0.0F);
         GL11.glVertex2f(0.0F, (float)GLFrame.getHeight());
         GL11.glVertex2f((float)this.border, (float)GLFrame.getHeight());
         GL11.glVertex2f((float)this.border, 0.0F);
         GL11.glVertex2f((float)(GLFrame.getWidth() - this.border), 0.0F);
         GL11.glVertex2f((float)(GLFrame.getWidth() - this.border), (float)GLFrame.getHeight());
         GL11.glVertex2f((float)GLFrame.getWidth(), (float)GLFrame.getHeight());
         GL11.glVertex2f((float)GLFrame.getWidth(), 0.0F);
         GL11.glVertex2f((float)this.border, 0.0F);
         GL11.glVertex2f((float)this.border, (float)this.border);
         GL11.glVertex2f((float)(GLFrame.getWidth() - this.border), (float)this.border);
         GL11.glVertex2f((float)(GLFrame.getWidth() - this.border), 0.0F);
         GL11.glVertex2f((float)this.border, (float)(GLFrame.getHeight() - this.border));
         GL11.glVertex2f((float)this.border, (float)GLFrame.getHeight());
         GL11.glVertex2f((float)(GLFrame.getWidth() - this.border), (float)GLFrame.getHeight());
         GL11.glVertex2f((float)(GLFrame.getWidth() - this.border), (float)(GLFrame.getHeight() - this.border));
      } else {
         GL11.glVertex2f(0.0F, 0.0F);
         GL11.glVertex2f(0.0F, (float)GLFrame.getHeight());
         GL11.glVertex2f((float)GLFrame.getWidth(), (float)GLFrame.getHeight());
         GL11.glVertex2f((float)GLFrame.getWidth(), 0.0F);
      }

      GL11.glEnd();
      GlUtil.glDisable(3042);
      GL11.glEndList();
      this.generated = true;
   }

   public Vector4f getColor() {
      return this.color;
   }

   public void setBorder(int var1) {
      this.border = var1;
   }
}
