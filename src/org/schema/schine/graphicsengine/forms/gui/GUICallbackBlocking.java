package org.schema.schine.graphicsengine.forms.gui;

public interface GUICallbackBlocking extends GUICallback {
   boolean isBlocking();

   void onBlockedCallbackExecuted();

   void onBlockedCallbackNotExecuted(boolean var1);
}
