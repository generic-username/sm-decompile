package org.schema.schine.graphicsengine.forms.gui;

import java.util.ArrayList;
import javax.vecmath.Vector4f;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.UnicodeFont;
import org.schema.schine.common.TextAreaInput;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.input.InputState;

public class GUITextInput extends GUIElement implements GUICallback {
   public GUIAncor dependend;
   public int dependendWidthOffset;
   private GUITextOverlay carrier;
   private GUITextOverlay inputBox;
   private TextAreaInput system;
   private boolean firstDraw;
   private String preText;
   private float time;
   private String lastText;
   private boolean mouseActive;
   private boolean drawCarrier;
   private GUIHorizontalArea box;
   private boolean textBox;
   private Vector4f colorNormal;
   private GUIScrollablePanel boxScroll;
   private boolean displayAsPassword;

   public GUITextInput(int var1, int var2, InputState var3) {
      this(var1, var2, var3, false);
   }

   public GUITextInput(int var1, int var2, InputState var3, boolean var4) {
      super(var3);
      this.firstDraw = true;
      this.preText = "";
      this.mouseActive = false;
      this.drawCarrier = true;
      this.colorNormal = new Vector4f(0.98F, 0.98F, 0.98F, 1.0F);
      this.carrier = new GUITextOverlay(var1, var2, var3);
      this.inputBox = new GUITextOverlay(var1, var2, var3);
      if (isNewHud()) {
         this.box = new GUIHorizontalArea(var3, GUIHorizontalArea.HButtonType.TEXT_FIELD, var1);
      }

      this.mouseActive = var4;
   }

   public GUITextInput(int var1, int var2, FontLibrary.FontSize var3, InputState var4) {
      this(var1, var2, var3, var4, false);
   }

   public GUITextInput(int var1, int var2, FontLibrary.FontSize var3, InputState var4, boolean var5) {
      super(var4);
      this.firstDraw = true;
      this.preText = "";
      this.mouseActive = false;
      this.drawCarrier = true;
      this.colorNormal = new Vector4f(0.98F, 0.98F, 0.98F, 1.0F);
      this.carrier = new GUITextOverlay(var1, var2, var3, var4);
      this.inputBox = new GUITextOverlay(var1, var2, var3, var4);
      this.mouseActive = var5;
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         int var7 = 0;
         int var8 = 0;
         StringBuffer var3 = new StringBuffer(this.system.getCache());
         int var4 = 0;

         int var5;
         for(var5 = this.inputBox.getFont().getLineHeight(); (float)var8 < this.getRelMousePos().y - 3.0F; ++var4) {
            var8 = var5 * var4;
         }

         var4 -= 2;
         var8 = Math.max(0, Math.min(this.system.getLineIndex(), var4));
         var5 = 0;

         int var6;
         for(var4 = 0; var4 < var8; ++var4) {
            if ((var6 = var3.indexOf("\n", var5 + 1)) >= 0) {
               var5 = var6;
            }
         }

         if (var5 > 0) {
            var5 = Math.max(0, Math.min(this.system.getCache().length(), var5 + 1));
         } else {
            var5 = 0;
         }

         for(var4 = 0; (float)var7 < this.getRelMousePos().x - 5.0F && var4 < var3.length() && var5 + var4 < var3.length(); ++var4) {
            var7 = this.carrier.getFont().getWidth(this.preText + var3.substring(var5, var5 + var4));
         }

         var6 = var5 + Math.max(0, var4);
         var6 = Math.max(0, Math.min(this.system.getCache().length(), var6));
         this.system.setChatCarrier(var6);
         this.system.setBufferChanged();
         this.system.resetSelection();
      }

   }

   public boolean isOccluded() {
      return false;
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.system.getLinewrap();
      String var1 = this.system.getCache();
      if (isNewHud() && this.isTextBox() && this.dependend != null) {
         this.box.setWidth((int)(this.dependend.getWidth() + (float)this.dependendWidthOffset));
      }

      int var3;
      if (this.displayAsPassword) {
         StringBuffer var2 = new StringBuffer(var1.length());

         for(var3 = 0; var3 < var1.length(); ++var3) {
            var2.append("*");
         }

         var1 = this.preText + var2.toString();
      } else {
         var1 = this.preText + var1;
      }

      if (isNewHud() && this.isTextBox()) {
         this.box.draw();
         GlUtil.glPushMatrix();
         GlUtil.translateModelview(4.0F, 4.0F, 0.0F);
      }

      if (this.system.getCacheSelect().length() > 0) {
         var3 = 0 + this.carrier.getFont().getHeight(this.preText + this.system.getCacheSelectStart() + this.system.getCacheSelect());
         int var4 = this.carrier.getFont().getWidth(this.preText + this.system.getCacheSelectStart());
         int var5 = this.carrier.getFont().getWidth(this.preText + this.system.getCacheSelectStart() + this.system.getCacheSelect());
         this.drawOrthogonalQuad(var4 - 1, -1, var5 + 1, var3 + 1);
         this.inputBox.getText().set(0, var1);
         this.inputBox.setColor(this.colorNormal);
         if (this.boxScroll != null) {
            this.boxScroll.setContent(this.inputBox);
            this.boxScroll.draw();
         } else {
            this.inputBox.draw();
         }

         var1 = this.system.getCacheSelect();
         if (this.displayAsPassword) {
            StringBuilder var6 = new StringBuilder(var1.length());

            for(int var7 = 0; var7 < var6.length(); ++var7) {
               var6.append("*");
            }

            var1 = var6.toString();
         }

         var5 = (int)this.inputBox.getPos().x;
         this.inputBox.getPos().x = (float)var4;
         this.inputBox.getText().set(0, var1);
         this.inputBox.draw();
         this.inputBox.getPos().x = (float)var5;
      } else {
         this.inputBox.getText().set(0, var1);
         this.inputBox.setColor(this.colorNormal);
         if (this.boxScroll != null) {
            this.boxScroll.setContent(this.inputBox);
            this.boxScroll.draw();
         } else {
            this.inputBox.draw();
         }
      }

      float var8 = this.carrier.getPos().x;
      if (!this.system.getCacheCarrier().equals(this.lastText)) {
         String var9 = (var3 = this.system.getCacheCarrier().lastIndexOf("\n")) >= 0 ? this.system.getCacheCarrier().substring(var3 + 1) : this.system.getCacheCarrier();
         this.carrier.getPos().x = (float)(this.carrier.getFont().getWidth(this.preText + var9) - 2);
         this.carrier.getPos().y = (float)this.inputBox.getCurrentLineHeight(this.system.getCarrierLineIndex());
         this.lastText = new String(this.system.getCacheCarrier());
      }

      if (var8 != this.carrier.getPos().x) {
         this.time = 0.0F;
         this.carrier.getColor().a = 1.0F;
      }

      if (this.drawCarrier) {
         this.getState().setInTextBox(true);
         if (this.boxScroll != null) {
            this.boxScroll.setContent(this.carrier);
            this.boxScroll.draw();
         } else {
            if (Keyboard.isKeyDown(15)) {
               this.carrier.setColor(1.0F, 0.0F, 0.1F, 1.0F);
            } else {
               this.carrier.setColor(this.colorNormal);
            }

            this.carrier.draw();
         }
      }

      if (isNewHud() && this.isTextBox()) {
         GlUtil.glPopMatrix();
      }

      if (this.isMouseUpdateEnabled()) {
         this.checkMouseInside();
      }

      GlUtil.glPopMatrix();
   }

   public void onInit() {
      if (this.firstDraw) {
         if (this.isTextBox()) {
            this.boxScroll = new GUIScrollablePanel(10.0F, 10.0F, this, this.getState());
            this.boxScroll.setContent(this.inputBox);
            this.boxScroll.setLeftRightClipOnly = true;
            this.boxScroll.setScrollable(0);
         }

         this.carrier.setLimitTextDraw(1);
         this.carrier.setText(new ArrayList(1));
         this.carrier.getText().add("|");
         this.carrier.onInit();
         this.attach(this.carrier);
         this.inputBox.setLimitTextDraw(1);
         this.inputBox.setText(new ArrayList(1));
         this.inputBox.getText().add("");
         this.inputBox.onInit();
         this.attach(this.inputBox);
         if (this.mouseActive) {
            this.setMouseUpdateEnabled(true);
            this.setCallback(this);
         }

         this.firstDraw = false;
      }
   }

   protected void doOrientation() {
   }

   public float getHeight() {
      return this.isTextBox() && isNewHud() ? this.box.getHeight() : this.inputBox.getHeight();
   }

   public void setHeight(int var1) {
      if (this.isTextBox() && isNewHud()) {
         this.box.setHeight(var1);
      } else {
         this.inputBox.setHeight(var1);
      }
   }

   public float getWidth() {
      return this.isTextBox() && isNewHud() ? this.box.getWidth() : this.inputBox.getWidth();
   }

   public void setWidth(int var1) {
      if (this.isTextBox() && isNewHud()) {
         this.box.setWidth(var1);
      } else {
         this.inputBox.setWidth(var1);
      }
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void drawOrthogonalQuad(int var1, int var2, int var3, int var4) {
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      GL11.glBegin(7);
      GL11.glColor4f(0.2784F, 0.2784F, 0.2784F, 0.9F);
      GL11.glVertex2i(var1, var2);
      GL11.glVertex2i(var1, var4);
      GL11.glVertex2i(var3, var4);
      GL11.glVertex2i(var3, var2);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GL11.glEnd();
      GlUtil.glDisable(3042);
   }

   public String getName() {
      return "textInput";
   }

   public void update(Timer var1) {
      this.time += var1.getDelta();
      if ((double)this.time > 0.3D) {
         if (this.carrier.getColor().a > 0.0F) {
            this.carrier.getColor().a = 0.0F;
         } else {
            this.carrier.getColor().a = 1.0F;
         }

         this.time = 0.0F;
      }

   }

   public String getPreText() {
      return this.preText;
   }

   public void setPreText(String var1) {
      this.preText = var1;
   }

   public void setDrawCarrier(boolean var1) {
      this.drawCarrier = var1;
   }

   public GUITextOverlay getInputBox() {
      return this.inputBox;
   }

   public void setInputBox(GUITextOverlay var1) {
      this.inputBox = var1;
   }

   public GUITextOverlay getCarrier() {
      return this.carrier;
   }

   public void setCarrier(GUITextOverlay var1) {
      this.carrier = var1;
   }

   public TextAreaInput getTextInput() {
      return this.system;
   }

   public void setTextInput(TextAreaInput var1) {
      this.system = var1;
   }

   public boolean isTextBox() {
      return this.textBox;
   }

   public void setTextBox(boolean var1) {
      this.textBox = var1;
   }

   public UnicodeFont getFont() {
      return this.inputBox.getFont();
   }

   public void setColor(float var1, float var2, float var3, float var4) {
      this.colorNormal.set(var1, var2, var3, var4);
   }

   public int getMaxLineWidth() {
      return this.inputBox.getMaxLineWidth();
   }

   public int getTextHeight() {
      return this.inputBox.getTextHeight();
   }

   public void setDisplayAsPassword(boolean var1) {
      this.displayAsPassword = var1;
   }

   public GUIHorizontalArea getBox() {
      return this.box;
   }

   public void setBox(GUIHorizontalArea var1) {
      this.box = var1;
   }
}
