package org.schema.schine.graphicsengine.forms.gui;

public interface GUIUniqueExpandableInterface {
   void drawExpanded();

   boolean isScrollBarInside();

   void setExpanded(boolean var1);
}
