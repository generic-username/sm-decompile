package org.schema.schine.graphicsengine.forms.gui;

import javax.vecmath.Vector4f;
import org.schema.schine.input.InputState;

public abstract class GUIColoredAncor extends GUIAncor {
   public GUIColoredAncor(InputState var1, float var2, float var3) {
      super(var1, var2, var3);
   }

   public GUIColoredAncor(InputState var1) {
      super(var1);
   }

   public abstract Vector4f getColor();

   public abstract void setColor(Vector4f var1);
}
