package org.schema.schine.graphicsengine.forms;

import javax.vecmath.Vector4f;

public interface PositionableSubColorSprite extends PositionableSubSprite {
   Vector4f getColor();
}
