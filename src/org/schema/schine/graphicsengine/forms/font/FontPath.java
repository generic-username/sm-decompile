package org.schema.schine.graphicsengine.forms.font;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import java.util.Iterator;

public class FontPath {
   public static final String font_Path_default = "font/Monda-Regular.ttf";
   public static String font_Path = "font/Monda-Regular.ttf";
   public static int offsetStartSize = 10;
   public static int offsetDividedBy = -3;
   public static int offsetFixed = -2;
   public static String chars;

   public static void addChars(IntOpenHashSet var0) {
      if (var0 == null) {
         chars = null;
      } else {
         StringBuffer var1 = new StringBuffer(var0.size());
         Iterator var3 = var0.iterator();

         while(var3.hasNext()) {
            int var2 = (Integer)var3.next();
            var1.appendCodePoint(var2);
         }

         chars = var1.toString();
      }
   }
}
