package org.schema.schine.graphicsengine.forms;

import org.schema.common.util.linAlg.Vector4D;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class ParticleSystemSmokeShaded extends ParticleSystem implements Shaderable {
   private float cover;
   private Vector4D camPos = new Vector4D();
   private Vector4D lightPos = new Vector4D();
   private Shader shader;

   public ParticleSystemSmokeShaded(int var1, int var2, Sprite... var3) {
      super(var1, var2, 10, 2, 5, var3);
   }

   public void draw() {
      this.shader.setShaderInterface(this);
      this.shader.load();
      super.draw();
      this.shader.unload();
   }

   public void onExit() {
   }

   public void updateShader(DrawableScene var1) {
      this.lightPos.set(var1.getLight().getPos().x, var1.getLight().getPos().y, var1.getLight().getPos().z, 1.0F);
      this.camPos.set(Controller.getCamera().getPos().x, Controller.getCamera().getPos().y, Controller.getCamera().getPos().z, 0.0F);
   }

   public void updateShaderParameters(Shader var1) {
      this.cover = 5.0F;
      Vector4D var2 = new Vector4D(this.getPos().x, this.getPos().y, this.getPos().z, 0.0F);
      GlUtil.updateShaderVector4D(var1, "source", var2);
      GlUtil.updateShaderFloat(var1, "cCover", this.cover * 2.0F);
      GlUtil.updateShaderVector4D(var1, "lightPos", this.lightPos);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(32879, this.getSprite()[0].getMaterial().getTexture().getTextureId());
      GlUtil.updateShaderInt(var1, "tex", 0);
   }
}
