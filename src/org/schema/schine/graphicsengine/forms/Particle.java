package org.schema.schine.graphicsengine.forms;

import javax.vecmath.Vector3f;

public class Particle implements Positionable {
   public Vector3f dir;
   public float lifeTime;
   public float speed;
   public float force;
   private Vector3f pos;

   public Vector3f getPos() {
      return this.pos;
   }

   public void setPos(Vector3f var1) {
      this.pos = var1;
   }

   public void setPos(float var1, float var2, float var3) {
      this.pos.set(this.pos);
   }
}
