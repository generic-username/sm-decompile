package org.schema.schine.graphicsengine.forms;

import com.bulletphysics.linearmath.Transform;

public interface TransformableSubSprite {
   float getScale(long var1);

   int getSubSprite(Sprite var1);

   boolean canDraw();

   Transform getWorldTransform();
}
