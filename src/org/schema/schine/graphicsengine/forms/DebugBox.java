package org.schema.schine.graphicsengine.forms;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.debug.DebugGeometry;
import org.schema.schine.graphicsengine.forms.simple.Box;

public class DebugBox extends DebugGeometry {
   private static Vector3f[][] verts = Box.init();
   private Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   private Transform t;
   private Vector3f start;
   private Vector3f end;

   public DebugBox(Vector3f var1, Vector3f var2, Transform var3, float var4, float var5, float var6, float var7) {
      this.color.set(var4, var5, var6, var7);
      this.t = var3;
      this.start = var1;
      this.end = var2;
   }

   public static void draw(Vector3f var0, Vector3f var1, Vector4f var2, Transform var3, Vector3f[][] var4) {
      GL11.glPolygonMode(1032, 6913);
      GlUtil.glDisable(2896);
      GlUtil.glDisable(2884);
      GlUtil.glEnable(2903);
      GlUtil.glDisable(32879);
      GlUtil.glDisable(3553);
      GlUtil.glDisable(3552);
      GlUtil.glEnable(3042);
      GlUtil.glColor4f(var2.x, var2.y, var2.z, var2.w);
      GlUtil.glPushMatrix();
      GlUtil.glMultMatrix(var3);
      GL11.glBegin(7);

      for(int var5 = 0; var5 < var4.length; ++var5) {
         for(int var6 = 0; var6 < var4[var5].length; ++var6) {
            GL11.glVertex3f(var4[var5][var6].x, var4[var5][var6].y, var4[var5][var6].z);
         }
      }

      GL11.glEnd();
      GlUtil.glPopMatrix();
      GlUtil.glEnable(2929);
      GlUtil.glEnable(2896);
      GlUtil.glDisable(2903);
      GlUtil.glEnable(2884);
      GlUtil.glDisable(3042);
      GL11.glPolygonMode(1032, 6914);
   }

   public void draw() {
      Vector3f[][] var1 = Box.getVertices(this.start, this.end, verts);
      draw(this.start, this.end, this.color, this.t, var1);
   }

   public Vector4f getColor() {
      return this.color;
   }

   public void setColor(Vector4f var1) {
      this.color = var1;
   }
}
