package org.schema.schine.graphicsengine.forms.particle.trail;

import com.bulletphysics.linearmath.Transform;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.particle.ParticleContainer;
import org.schema.schine.graphicsengine.forms.particle.ParticleController;

public class ParticleTrailDrawerVBO implements Drawable {
   public static final float DIST_BETWEEN_SECTIONS = 0.5F;
   private static final int MAX_PARTICLES_DRAWN = 512;
   protected static float[] tCoords = new float[]{0.0F, 0.5F, 0.75F, 0.25F};
   private static FloatBuffer vertexBuffer;
   private static FloatBuffer attribBuffer;
   Vector3f d1;
   Vector3f d2;
   Vector3f h1;
   Vector3f h2;
   private ParticleController particleController;
   private IntBuffer vertexVBOName;
   private IntBuffer parametersVBOName;
   private float spriteSize;
   private Vector3f up;
   private Vector3f right;
   private Vector3f forward;
   private Vector3f posHelper;
   private Vector3f posHelper2;
   private Vector3f axis;
   private Vector3f lastAxis;
   private Vector3f correction;
   private Vector4f posHelperAndTime;
   private Vector3f[] c;
   private Vector3f[] cor;
   private Vector3f[] cOld;
   private Transform t;
   private int vertexCount;
   private Vector4f attribHelper;

   public ParticleTrailDrawerVBO(ParticleController var1) {
      this.d1 = new Vector3f();
      this.d2 = new Vector3f();
      this.h1 = new Vector3f();
      this.h2 = new Vector3f();
      this.vertexVBOName = BufferUtils.createIntBuffer(1);
      this.parametersVBOName = BufferUtils.createIntBuffer(1);
      this.spriteSize = 1.0F;
      this.up = new Vector3f();
      this.right = new Vector3f();
      this.forward = new Vector3f();
      this.posHelper = new Vector3f();
      this.posHelper2 = new Vector3f();
      this.axis = new Vector3f();
      this.lastAxis = new Vector3f();
      this.correction = new Vector3f();
      this.posHelperAndTime = new Vector4f();
      this.c = new Vector3f[4];
      this.cor = new Vector3f[4];
      this.cOld = new Vector3f[4];
      this.t = new Transform();
      this.attribHelper = new Vector4f();
      this.setParticleController(var1);
      this.t.setIdentity();

      for(int var2 = 0; var2 < this.c.length; ++var2) {
         this.c[var2] = new Vector3f();
         this.cOld[var2] = new Vector3f();
         this.cor[var2] = new Vector3f();
      }

   }

   public ParticleTrailDrawerVBO(ParticleController var1, float var2) {
      this(var1);
      this.setSpriteSize(var2);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.getParticleController().getParticleCount() > 1 && this.updateBuffers()) {
         GlUtil.glPushMatrix();
         GlUtil.glDisable(2884);
         GlUtil.glDisable(2896);
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         this.drawVBO();
         GlUtil.glDisable(3042);
         GlUtil.glPopMatrix();
         GlUtil.glDisable(2903);
         GlUtil.glEnable(2884);
         GlUtil.glEnable(2896);
      }

   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.initVertexBuffer(512);
   }

   private void drawVBO() {
      GlUtil.glEnableClientState(32884);
      GlUtil.glEnableClientState(32888);
      GlUtil.glBindBuffer(34962, this.vertexVBOName.get(0));
      GL11.glVertexPointer(4, 5126, 0, 0L);
      GlUtil.glBindBuffer(34962, this.parametersVBOName.get(0));
      GL11.glTexCoordPointer(4, 5126, 0, 0L);
      GL11.glDrawArrays(8, 0, this.vertexCount);
      GlUtil.glBindBuffer(34962, 0);
      GlUtil.glDisableClientState(32884);
      GlUtil.glDisableClientState(32888);
   }

   public ParticleController getParticleController() {
      return this.particleController;
   }

   public void setParticleController(ParticleController var1) {
      this.particleController = var1;
   }

   public float getSpriteSize() {
      return this.spriteSize;
   }

   public void setSpriteSize(float var1) {
      this.spriteSize = var1;
   }

   private void handleQuad(int var1, int var2, ParticleContainer var3, float var4, float var5, int var6) {
      try {
         boolean var7 = var5 > 0.0F;
         var3.getPos(var1, this.posHelper);
         if (var5 >= (float)(var6 - 2)) {
            this.axis.set(this.lastAxis);
         } else {
            var3.getPos(var2, this.posHelper2);
            this.axis.sub(this.posHelper2, this.posHelper);
         }

         if (var7) {
            this.correction.cross(this.axis, this.lastAxis);
         }

         GlUtil.billboardAbitraryAxis(this.posHelper, this.axis, Controller.getCamera().getPos(), this.t);
         this.posHelperAndTime.set(this.posHelper.x, this.posHelper.y, this.posHelper.z, var3.getLifetime(var1));
         GlUtil.getRibbon(this.t, 0.5F, this.c);
         this.d1.sub(this.cOld[3], this.c[0]);
         this.d2.sub(this.cOld[2], this.c[1]);
         this.d1.scale(0.5F);
         this.d2.scale(0.5F);
         this.h1.add(this.c[0], this.d1);
         this.h2.add(this.c[1], this.d2);
         if (var5 > 0.0F) {
            this.cor[0].set(this.cOld[0]);
            this.cor[1].set(this.cOld[1]);
            this.cor[2].set(this.h2);
            this.cor[3].set(this.h1);
            GlUtil.putRibbon2(vertexBuffer, this.posHelperAndTime.w, this.cor, vertexBuffer.position() - 8);
         }

         if (var5 > 0.0F) {
            this.cor[0].set(this.h1);
            this.cor[1].set(this.h2);
            this.cor[2].set(this.c[2]);
            this.cor[3].set(this.c[3]);
            GlUtil.putRibbon2(vertexBuffer, this.posHelperAndTime.w, this.cor, -1);
            this.vertexCount += 2;
         } else {
            GlUtil.putRibbon2(vertexBuffer, this.posHelperAndTime.w, this.c, -1);
            this.vertexCount += 2;
         }

         for(var1 = 0; var1 < 2; ++var1) {
            this.attribHelper.set(0.0F, var5 * 0.05F, var4, tCoords[var1]);
            GlUtil.putPoint4(attribBuffer, this.attribHelper);
         }

         for(var1 = 0; var1 < 4; ++var1) {
            if (var5 > 0.0F) {
               this.cOld[var1].set(this.cor[var1]);
            } else {
               this.cOld[var1].set(this.c[var1]);
            }
         }
      } catch (Exception var8) {
         var8.printStackTrace();
         System.err.println("[ERROR][TRAILDRAWER] " + var8.getClass().getSimpleName() + " " + var8.getMessage());
      }

      this.lastAxis.set(this.axis);
   }

   private void initVertexBuffer(int var1) {
      if (vertexBuffer == null) {
         vertexBuffer = BufferUtils.createFloatBuffer((var1 << 2 << 1) + 8);
      } else {
         vertexBuffer.rewind();
      }

      GL15.glGenBuffers(this.vertexVBOName);
      Controller.loadedVBOBuffers.add(this.vertexVBOName.get(0));
      GlUtil.glBindBuffer(34962, this.vertexVBOName.get(0));
      GlUtil.glBindBuffer(34962, 0);
      if (attribBuffer == null) {
         attribBuffer = BufferUtils.createFloatBuffer((var1 << 2 << 1) + 8);
      } else {
         attribBuffer.rewind();
      }

      GL15.glGenBuffers(this.parametersVBOName);
      GlUtil.glBindBuffer(34962, this.parametersVBOName.get(0));
      Controller.loadedVBOBuffers.add(this.parametersVBOName.get(0));
      GlUtil.glBindBuffer(34962, 0);
   }

   private void loadVBO() {
      this.initVertexBuffer(512);
   }

   private boolean updateBufferQuads(ParticleContainer var1) {
      vertexBuffer.rewind();
      attribBuffer.rewind();
      vertexBuffer.limit(vertexBuffer.capacity());
      attribBuffer.limit(attribBuffer.capacity());
      this.up = GlUtil.getUpVector(this.up);
      this.right = GlUtil.getRightVector(this.right);
      this.forward = GlUtil.getForwardVector(this.forward);
      int var2 = Math.min(512, this.getParticleController().getParticleCount());
      int var3 = this.getParticleController().getOffset() % this.getParticleController().getParticles().getCapacity();
      float var4 = 0.0F;
      float var5 = 1.0F / (float)var2;
      this.vertexCount = 0;

      for(int var6 = 0; var6 < var2 && var3 < this.getParticleController().getParticles().getCapacity() - 2; ++var6) {
         int var8 = var3 + 1;
         this.handleQuad(var3, var8, var1, var4, (float)var6, var2);
         var4 += var5;
         ++var3;
      }

      if (vertexBuffer.position() == 0) {
         return false;
      } else {
         vertexBuffer.flip();
         attribBuffer.flip();
         GlUtil.glBindBuffer(34962, this.vertexVBOName.get(0));
         GL15.glBufferData(34962, vertexBuffer, 35040);
         GlUtil.glBindBuffer(34962, this.parametersVBOName.get(0));
         GL15.glBufferData(34962, attribBuffer, 35040);
         return true;
      }
   }

   private boolean updateBuffers() {
      ParticleContainer var1 = this.getParticleController().getParticles();
      return this.updateBufferQuads(var1);
   }
}
