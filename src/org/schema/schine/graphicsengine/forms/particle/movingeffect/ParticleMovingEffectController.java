package org.schema.schine.graphicsengine.forms.particle.movingeffect;

import com.bulletphysics.linearmath.Transform;
import java.util.Random;
import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.camera.Camera;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.particle.ParticleController;
import org.schema.schine.graphicsengine.forms.particle.simple.SimpleParticleContainer;

public class ParticleMovingEffectController extends ParticleController {
   public float spawnCount = 3.3F;
   protected float MAX_LIFETIME = 1000.0F;
   Vector3f front_direction = new Vector3f();
   Vector3f pos = new Vector3f();
   Vector3f camDir = new Vector3f();
   private float spawnPlaneSize = 30.0F;
   private float spawnPlaneDistance = 30.0F;
   private float spawnPlaneDistanceSquared;
   private Random random;
   private float accumTime;
   private float maxSpeed;

   public ParticleMovingEffectController(boolean var1) {
      super(var1);
      this.spawnPlaneDistanceSquared = this.spawnPlaneDistance * this.spawnPlaneDistance;
      this.random = new Random();
      this.accumTime = 0.0F;
   }

   public ParticleMovingEffectController(boolean var1, int var2) {
      super(var1, var2);
      this.spawnPlaneDistanceSquared = this.spawnPlaneDistance * this.spawnPlaneDistance;
      this.random = new Random();
      this.accumTime = 0.0F;
   }

   public float getAccumTime() {
      return this.accumTime;
   }

   public void setAccumTime(float var1) {
      this.accumTime = var1;
   }

   public boolean updateParticle(int var1, Timer var2) {
      Transform var3 = this.getCamera();
      float var4 = ((SimpleParticleContainer)this.getParticles()).getLifetime(var1);
      ((SimpleParticleContainer)this.getParticles()).getPos(var1, this.pos);
      this.camDir.sub(this.pos, var3.origin);
      if (this.camDir.lengthSquared() > this.spawnPlaneDistanceSquared * 2.0F) {
         return false;
      } else if ((double)((this.pos.x - var3.origin.x) * this.front_direction.x + (this.pos.y - var3.origin.y) * this.front_direction.y + (this.pos.z - var3.origin.z) * this.front_direction.z) <= 0.0D) {
         return false;
      } else {
         ((SimpleParticleContainer)this.getParticles()).setLifetime(var1, (float)((double)var4 + (double)(3.0F * var2.getDelta()) * 1000.0D));
         return var4 < this.MAX_LIFETIME;
      }
   }

   public void update(Timer var1) {
      Controller.getCamera().getForward(this.front_direction);
      super.update(var1);
      this.accumTime += var1.getDelta();
   }

   public Transform getCamera() {
      Camera var1;
      return (var1 = Controller.getCamera()) == null ? null : var1.getWorldTransform();
   }

   public void updateEffectFromCam(Transform var1, float var2) {
      this.maxSpeed = var2;
      Vector3f var3 = new Vector3f(0.0F, 0.0F, 0.0F);
      int var9 = (int)(this.spawnCount * (var2 / this.maxSpeed));

      for(int var4 = 0; var4 < var9; ++var4) {
         Vector3f var5 = new Vector3f(var1.origin);
         Vector3f var6 = new Vector3f();
         Vector3f var7 = new Vector3f();
         Vector3f var8 = new Vector3f();
         GlUtil.getForwardVector(var6, var1);
         GlUtil.getUpVector(var7, var1);
         GlUtil.getLeftVector(var8, var1);
         var6.scale(this.spawnPlaneDistance);
         var7.scale(this.random.nextFloat() * this.spawnPlaneSize - this.spawnPlaneSize / 2.0F);
         var8.scale(this.random.nextFloat() * this.spawnPlaneSize - this.spawnPlaneSize / 2.0F);
         var5.add(var6);
         var5.add(var7);
         var5.add(var8);
         this.addParticle(var5, var3);
      }

   }

   public void updateEffectFromCam(float var1, float var2) {
      Transform var3;
      if ((var3 = this.getCamera()) != null) {
         this.maxSpeed = var2;
         Vector3f var4 = new Vector3f(0.0F, 0.0F, 0.0F);
         int var9 = (int)(this.spawnCount * (var1 / var2));

         for(int var10 = 0; var10 < var9; ++var10) {
            Vector3f var5 = new Vector3f(var3.origin);
            Vector3f var6 = new Vector3f();
            Vector3f var7 = new Vector3f();
            Vector3f var8 = new Vector3f();
            GlUtil.getForwardVector(var6, var3);
            GlUtil.getUpVector(var7, var3);
            GlUtil.getLeftVector(var8, var3);
            var6.scale(this.spawnPlaneDistance);
            var7.scale(this.random.nextFloat() * this.spawnPlaneSize - this.spawnPlaneSize / 2.0F);
            var8.scale(this.random.nextFloat() * this.spawnPlaneSize - this.spawnPlaneSize / 2.0F);
            var5.add(var6);
            var5.add(var7);
            var5.add(var8);
            this.addParticle(var5, var4);
         }

      }
   }

   protected SimpleParticleContainer getParticleInstance(int var1) {
      return new SimpleParticleContainer(var1);
   }

   public int addParticle(Vector3f var1, Vector3f var2) {
      if (this.particlePointer >= ((SimpleParticleContainer)this.getParticles()).getCapacity() - 1) {
         ((SimpleParticleContainer)this.getParticles()).growCapacity();
      }

      ++this.idGen;
      int var3;
      if (this.isOrderedDelete()) {
         var3 = this.particlePointer % ((SimpleParticleContainer)this.getParticles()).getCapacity();
         ((SimpleParticleContainer)this.getParticles()).setPos(var3, var1.x, var1.y, var1.z);
         ((SimpleParticleContainer)this.getParticles()).setStart(var3, var1.x, var1.y, var1.z);
         ((SimpleParticleContainer)this.getParticles()).setVelocity(var3, var2.x, var2.y, var2.z);
         ((SimpleParticleContainer)this.getParticles()).setLifetime(var3, 0.0F);
         ++this.particlePointer;
         return var3;
      } else {
         var3 = this.particlePointer % ((SimpleParticleContainer)this.getParticles()).getCapacity();
         ((SimpleParticleContainer)this.getParticles()).setPos(var3, var1.x, var1.y, var1.z);
         ((SimpleParticleContainer)this.getParticles()).setStart(var3, var1.x, var1.y, var1.z);
         ((SimpleParticleContainer)this.getParticles()).setVelocity(var3, var2.x, var2.y, var2.z);
         ((SimpleParticleContainer)this.getParticles()).setLifetime(var3, 0.0F);
         ++this.particlePointer;
         return var3;
      }
   }
}
