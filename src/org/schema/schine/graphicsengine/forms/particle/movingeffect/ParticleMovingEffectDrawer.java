package org.schema.schine.graphicsengine.forms.particle.movingeffect;

import java.nio.FloatBuffer;
import javax.vecmath.Vector4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.common.FastMath;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.particle.ParticleContainer;
import org.schema.schine.graphicsengine.forms.particle.ParticleController;
import org.schema.schine.graphicsengine.forms.particle.ParticleDrawerVBO;
import org.schema.schine.graphicsengine.forms.particle.StartContainerInterface;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.graphicsengine.texture.Texture;

public class ParticleMovingEffectDrawer extends ParticleDrawerVBO implements Shaderable {
   private static final float DUST_SPRITE_SIZE = 0.1F;
   static FloatBuffer store = BufferUtils.createFloatBuffer(16);
   private static Texture tex;
   public boolean frustumCulling;
   public float smear;
   float time;
   Matrix4f[] old;
   private Shader shader;
   private int pointer;
   private boolean first;

   public ParticleMovingEffectDrawer(ParticleController var1) {
      this(var1, 0.1F);
   }

   public ParticleMovingEffectDrawer(ParticleController var1, float var2) {
      super(var1, var2);
      this.frustumCulling = true;
      this.smear = 0.015F;
      this.time = 1.0F;
      this.first = true;
      this.old = new Matrix4f[300];

      for(int var3 = 0; var3 < this.old.length; ++var3) {
         this.old[var3] = new Matrix4f();
         this.old[var3].setIdentity();
      }

   }

   public void draw() {
      if (EngineSettings.D_INFO_DRAW_SPACE_PARTICLE.isOn()) {
         if (this.getParticleController().getParticleCount() > 0) {
            this.shader = ShaderLibrary.spacedustShader;

            assert ShaderLibrary.spacedustShader != null;

            this.shader.setShaderInterface(this);
            this.shader.load();
            if (!this.first) {
               super.draw();
            }

            this.shader.unload();
         }

      }
   }

   public void onInit() {
      this.setDrawInverse(true);
      if (tex == null) {
         tex = Controller.getResLoader().getSprite("star").getMaterial().getTexture();
      }

      super.onInit();
   }

   protected int handleQuad(int var1, ParticleContainer var2) {
      var2.getPos(var1, this.posHelper);
      if (this.frustumCulling && !Controller.getCamera().isPointInFrustrum(this.posHelper)) {
         return 0;
      } else {
         this.posHelperAndTime.set(this.posHelper.x, this.posHelper.y, this.posHelper.z, var2.getLifetime(var1));
         ((StartContainerInterface)var2).getStart(var1, this.startHelper);
         this.attribHelper.set(this.startHelper.x, this.startHelper.y, this.startHelper.z, 0.0F);

         for(var1 = 0; var1 < 4; ++var1) {
            GlUtil.putPoint4(vertexBuffer, this.posHelperAndTime);
            this.attribHelper.w = tCoords[var1];
            GlUtil.putPoint4(attribBuffer, this.attribHelper);
         }

         return 4;
      }
   }

   public void onExit() {
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      store.rewind();
      this.old[FastMath.cyclicModulo(this.pointer - this.old.length - 2, this.old.length - 1)].store(store);
      store.rewind();
      GlUtil.updateShaderMat4(var1, "oldModelViewMatrix", store, false);
      GlUtil.updateShaderVector4f(var1, "Param", new Vector4f(1.0F, 0.1F, 0.15F, 5.0E-4F));
      GlUtil.glBindTexture(3553, tex.getTextureId());
      GlUtil.updateShaderInt(var1, "Texture", 0);
      ParticleMovingEffectController var2;
      if ((var2 = (ParticleMovingEffectController)this.getParticleController()).getAccumTime() > this.smear || this.first) {
         this.old[this.pointer].load(Controller.modelviewMatrix);
         this.pointer = FastMath.cyclicModulo(this.pointer + 1, this.old.length - 1);
         if (this.pointer > 2) {
            this.first = false;
         }

         var2.setAccumTime(0.0F);
      }

   }

   public void update() {
   }

   public void reset() {
      this.pointer = 0;
      this.first = true;
   }
}
