package org.schema.schine.graphicsengine.forms.particle;

import javax.vecmath.Vector3f;

public interface StartContainerInterface {
   Vector3f getStart(int var1, Vector3f var2);
}
