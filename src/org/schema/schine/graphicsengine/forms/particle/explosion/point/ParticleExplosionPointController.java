package org.schema.schine.graphicsengine.forms.particle.explosion.point;

import java.util.Random;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.particle.ExplosionParticleContainer;
import org.schema.schine.graphicsengine.forms.particle.ParticleController;

public class ParticleExplosionPointController extends ParticleController {
   private static final float MAX_LIFETIME = 3.0F;
   Vector3f dir = new Vector3f();
   private Vector3f velocityHelper = new Vector3f();
   private Vector4f colorHelper = new Vector4f();
   private Vector3f posHelper = new Vector3f();
   private Vector3f startHelper = new Vector3f();
   private Vector3f posBeforeUpdate = new Vector3f();
   private Vector3f velocityBefore = new Vector3f();
   private float explosionSpeed = 0.003F;
   private static Random r = new Random();

   public ParticleExplosionPointController(boolean var1) {
      super(var1);
   }

   public void addExplosion(Vector3f var1, Vector3f var2, float var3, int var4, float var5, long var6) {
      this.dir.set(0.0F, 0.0F, 0.0F);
      int var8 = this.addParticle(var1, this.dir);
      ((ExplosionParticleContainer)this.getParticles()).setDamage(var8, Math.min(80.0F, Math.max(5.0F, var5 * 0.05F)));
      ((ExplosionParticleContainer)this.getParticles()).setImpactForce(var8, 0.11F);
      ((ExplosionParticleContainer)this.getParticles()).setColor(var8, 1.0F, 1.0F, 1.0F, 1.0F);
   }

   public int addParticle(Vector3f var1, Vector3f var2) {
      if (this.particlePointer >= ((ExplosionParticleContainer)this.getParticles()).getCapacity() - 1) {
         ((ExplosionParticleContainer)this.getParticles()).growCapacity();
      }

      int var3 = this.idGen++;
      int var4;
      if (this.isOrderedDelete()) {
         var4 = this.particlePointer % ((ExplosionParticleContainer)this.getParticles()).getCapacity();
         ((ExplosionParticleContainer)this.getParticles()).setPos(var4, var1.x, var1.y, var1.z);
         ((ExplosionParticleContainer)this.getParticles()).setStart(var4, var1.x, var1.y, var1.z);
         ((ExplosionParticleContainer)this.getParticles()).setVelocity(var4, var2.x, var2.y, var2.z);
         ((ExplosionParticleContainer)this.getParticles()).setLifetime(var4, 0.0F);
         ((ExplosionParticleContainer)this.getParticles()).setId(var4, var3);
         ((ExplosionParticleContainer)this.getParticles()).setBlockHitIndex(var4, 0);
         ((ExplosionParticleContainer)this.getParticles()).setShotStatus(var4, 0);
         ++this.particlePointer;
         return var4;
      } else {
         var4 = this.particlePointer % ((ExplosionParticleContainer)this.getParticles()).getCapacity();
         ((ExplosionParticleContainer)this.getParticles()).setPos(var4, var1.x, var1.y, var1.z);
         ((ExplosionParticleContainer)this.getParticles()).setStart(var4, var1.x, var1.y, var1.z);
         ((ExplosionParticleContainer)this.getParticles()).setVelocity(var4, var2.x, var2.y, var2.z);
         ((ExplosionParticleContainer)this.getParticles()).setLifetime(var4, 0.0F);
         ((ExplosionParticleContainer)this.getParticles()).setId(var4, var3);
         ((ExplosionParticleContainer)this.getParticles()).setBlockHitIndex(var4, 0);
         ((ExplosionParticleContainer)this.getParticles()).setShotStatus(var4, 0);
         ++this.particlePointer;
         return var4;
      }
   }

   public boolean updateParticle(int var1, Timer var2) {
      float var3 = ((ExplosionParticleContainer)this.getParticles()).getLifetime(var1);
      float var4 = ((ExplosionParticleContainer)this.getParticles()).getImpactForce(var1);
      ((ExplosionParticleContainer)this.getParticles()).setLifetime(var1, var3 + var2.getDelta());
      return var3 < var4;
   }

   protected ExplosionParticleContainer getParticleInstance(int var1) {
      return new ExplosionParticleContainer(var1);
   }
}
