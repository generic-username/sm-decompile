package org.schema.schine.graphicsengine.forms.particle.explosion.particle;

import java.util.Random;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.FastMath;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.particle.ExplosionParticleContainer;
import org.schema.schine.graphicsengine.forms.particle.ParticleController;

public class ParticleExplosionController extends ParticleController {
   private static final float MAX_LIFETIME = 3.0F;
   Vector3f dir = new Vector3f();
   private Vector3f velocityHelper = new Vector3f();
   private Vector4f colorHelper = new Vector4f();
   private Vector3f posHelper = new Vector3f();
   private Vector3f startHelper = new Vector3f();
   private Vector3f posBeforeUpdate = new Vector3f();
   private Vector3f velocityBefore = new Vector3f();
   private float explosionSpeed = 0.003F;
   private static Random r = new Random();

   public ParticleExplosionController(boolean var1) {
      super(var1);
   }

   public void addExplosion(Vector3f var1, Vector3f var2, float var3, int var4, long var5) {
      for(int var9 = 0; var9 < var4; ++var9) {
         this.dir.set(0.5F - FastMath.rand.nextFloat(), 0.5F - FastMath.rand.nextFloat(), 0.5F - FastMath.rand.nextFloat());
         this.dir.scale(this.explosionSpeed * (var3 / 4.0F));
         int var7 = this.addParticle(var1, this.dir);
         ((ExplosionParticleContainer)this.getParticles()).setDamage(var7, var3 / 4.0F);
         ((ExplosionParticleContainer)this.getParticles()).setWeaponId(var7, var5);
         int var8 = r.nextInt(ParticleExplosionDrawer.explTex.length);
         ((ExplosionParticleContainer)this.getParticles()).setSpriteCode(var7, var8, ParticleExplosionDrawer.explTex[var8].getMultiSpriteMaxX(), ParticleExplosionDrawer.explTex[var8].getMultiSpriteMaxY());
      }

   }

   public boolean updateParticle(int var1, Timer var2) {
      ((ExplosionParticleContainer)this.getParticles()).getVelocity(var1, this.velocityHelper);
      ((ExplosionParticleContainer)this.getParticles()).getColor(var1, this.colorHelper);
      ((ExplosionParticleContainer)this.getParticles()).getPos(var1, this.posHelper);
      ((ExplosionParticleContainer)this.getParticles()).getStart(var1, this.startHelper);
      this.posBeforeUpdate.set(this.posHelper);
      this.velocityBefore.set(this.velocityHelper);
      float var3 = ((ExplosionParticleContainer)this.getParticles()).getLifetime(var1);
      this.velocityHelper.scale((float)((double)(var2.getDelta() * 1000.0F) * 0.2D));
      ((ExplosionParticleContainer)this.getParticles()).setLifetime(var1, var3 + var2.getDelta());
      this.posHelper.add(this.velocityHelper);
      this.velocityBefore.scale(0.995F);
      ((ExplosionParticleContainer)this.getParticles()).setVelocity(var1, this.velocityBefore.x, this.velocityBefore.y, this.velocityBefore.z);
      ((ExplosionParticleContainer)this.getParticles()).setPos(var1, this.posHelper.x, this.posHelper.y, this.posHelper.z);
      return var3 < 3.0F;
   }

   protected ExplosionParticleContainer getParticleInstance(int var1) {
      return new ExplosionParticleContainer(var1);
   }

   public int addParticle(Vector3f var1, Vector3f var2) {
      if (this.particlePointer >= ((ExplosionParticleContainer)this.getParticles()).getCapacity() - 1) {
         ((ExplosionParticleContainer)this.getParticles()).growCapacity();
      }

      int var3 = this.idGen++;
      int var4;
      if (this.isOrderedDelete()) {
         var4 = this.particlePointer % ((ExplosionParticleContainer)this.getParticles()).getCapacity();
         ((ExplosionParticleContainer)this.getParticles()).setPos(var4, var1.x, var1.y, var1.z);
         ((ExplosionParticleContainer)this.getParticles()).setStart(var4, var1.x, var1.y, var1.z);
         ((ExplosionParticleContainer)this.getParticles()).setVelocity(var4, var2.x, var2.y, var2.z);
         ((ExplosionParticleContainer)this.getParticles()).setLifetime(var4, 0.0F);
         ((ExplosionParticleContainer)this.getParticles()).setId(var4, var3);
         ((ExplosionParticleContainer)this.getParticles()).setBlockHitIndex(var4, 0);
         ((ExplosionParticleContainer)this.getParticles()).setShotStatus(var4, 0);
         ++this.particlePointer;
         return var4;
      } else {
         var4 = this.particlePointer % ((ExplosionParticleContainer)this.getParticles()).getCapacity();
         ((ExplosionParticleContainer)this.getParticles()).setPos(var4, var1.x, var1.y, var1.z);
         ((ExplosionParticleContainer)this.getParticles()).setStart(var4, var1.x, var1.y, var1.z);
         ((ExplosionParticleContainer)this.getParticles()).setVelocity(var4, var2.x, var2.y, var2.z);
         ((ExplosionParticleContainer)this.getParticles()).setLifetime(var4, 0.0F);
         ((ExplosionParticleContainer)this.getParticles()).setId(var4, var3);
         ((ExplosionParticleContainer)this.getParticles()).setBlockHitIndex(var4, 0);
         ((ExplosionParticleContainer)this.getParticles()).setShotStatus(var4, 0);
         ++this.particlePointer;
         return var4;
      }
   }
}
