package org.schema.schine.graphicsengine.forms.particle;

import com.bulletphysics.linearmath.Transform;
import java.nio.FloatBuffer;
import javax.vecmath.AxisAngle4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;

public class ParticleDrawerVBO implements Drawable {
   public static final int MODE_QUADS = 0;
   private static final int MAX_PARTICLES_DRAWN = 512;
   protected static FloatBuffer vertexBuffer;
   protected static FloatBuffer normalBuffer;
   protected static FloatBuffer attribBuffer;
   protected static float[] tCoords = new float[]{0.0F, 0.5F, 0.75F, 0.25F};
   public Transform currentSystemTransform;
   protected Vector3f up;
   protected Vector3f right;
   protected Vector3f forward;
   protected Vector3f upTmp;
   protected Vector3f rightTmp;
   protected Vector3f posHelper;
   protected Vector4f posHelperAndTime;
   protected Vector4f colorHelper;
   protected Vector3f startHelper;
   protected Vector3f velocityHelper;
   protected Transform t;
   protected AxisAngle4f axis;
   protected Vector4f attribHelper;
   float test;
   private ParticleController particleController;
   private int vertexVBOName;
   private int attribVBOName;
   private float spriteSize;
   private int vertexCount;
   private boolean drawInverse;
   public boolean depthMask;
   private int normalVBOName;

   public ParticleDrawerVBO(ParticleController var1) {
      this.up = new Vector3f();
      this.right = new Vector3f();
      this.forward = new Vector3f();
      this.upTmp = new Vector3f();
      this.rightTmp = new Vector3f();
      this.posHelper = new Vector3f();
      this.posHelperAndTime = new Vector4f();
      this.colorHelper = new Vector4f();
      this.startHelper = new Vector3f();
      this.velocityHelper = new Vector3f();
      this.t = new Transform();
      this.axis = new AxisAngle4f(this.forward, 3.1415927F);
      this.attribHelper = new Vector4f();
      this.test = 0.0F;
      this.spriteSize = 1.0F;
      this.setParticleController(var1);
      this.t.setIdentity();
   }

   public ParticleDrawerVBO(ParticleController var1, float var2) {
      this(var1);
      this.setSpriteSize(var2);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.getParticleController().getParticleCount() > 0 && this.updateBuffers()) {
         GlUtil.glPushMatrix();
         GlUtil.glDisable(2896);
         GlUtil.glEnable(3042);
         GlUtil.glDepthMask(this.depthMask);
         GlUtil.glBlendFunc(770, 771);
         GlUtil.glEnable(2884);
         this.drawVBO();
         GlUtil.glDisable(3042);
         GlUtil.glEnable(2884);
         GlUtil.glDisable(2903);
         GlUtil.glEnable(2896);
         GlUtil.glDepthMask(true);
         GlUtil.glPopMatrix();
      }

   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.loadVBO();
   }

   private void drawVBO() {
      GlUtil.glEnableClientState(32884);
      GlUtil.glEnableClientState(32888);
      GlUtil.glEnableClientState(32885);
      GlUtil.glBindBuffer(34962, this.vertexVBOName);
      GL11.glVertexPointer(4, 5126, 0, 0L);
      GlUtil.glBindBuffer(34962, this.attribVBOName);
      GL11.glTexCoordPointer(4, 5126, 0, 0L);
      GlUtil.glBindBuffer(34962, this.normalVBOName);
      GL11.glNormalPointer(5126, 0, 0L);
      GL11.glDrawArrays(7, 0, this.vertexCount);
      GlUtil.glBindBuffer(34962, 0);
      GlUtil.glDisableClientState(32884);
      GlUtil.glDisableClientState(32888);
      GlUtil.glDisableClientState(32885);
   }

   public ParticleController getParticleController() {
      return this.particleController;
   }

   public void setParticleController(ParticleController var1) {
      this.particleController = var1;
   }

   public float getSpriteSize(int var1, ParticleContainer var2) {
      return this.spriteSize;
   }

   public float getSpriteCode(int var1, ParticleContainer var2) {
      return (float)var2.getSpriteCode(var1);
   }

   protected int handleQuad(int var1, ParticleContainer var2) {
      var2.getPos(var1, this.posHelper);
      if (this.currentSystemTransform != null) {
         this.currentSystemTransform.transform(this.posHelper);
      }

      if (!GlUtil.isPointInView(this.posHelper, Controller.vis.getVisLen())) {
         return 0;
      } else {
         if (this.currentSystemTransform != null) {
            var2.getPos(var1, this.posHelper);
         }

         this.upTmp.set(this.up);
         this.rightTmp.set(this.right);
         this.posHelperAndTime.set(this.posHelper.x, this.posHelper.y, this.posHelper.z, var2.getLifetime(var1));
         GlUtil.putBillboardQuad(vertexBuffer, this.upTmp, this.rightTmp, this.getSpriteSize(var1, var2), this.posHelperAndTime);
         var2.getColor(var1, this.colorHelper);

         for(int var3 = 0; var3 < 4; ++var3) {
            this.attribHelper.set(this.colorHelper.x, this.colorHelper.y, this.colorHelper.z, tCoords[var3]);
            GlUtil.putPoint4(attribBuffer, this.attribHelper);
         }

         GlUtil.putPoint3(normalBuffer, this.getSpriteCode(var1, var2), 0.0F, 0.0F);
         return 4;
      }
   }

   public boolean isDrawInverse() {
      return this.drawInverse;
   }

   public void setDrawInverse(boolean var1) {
      this.drawInverse = var1;
   }

   private void loadVBO() {
      if (vertexBuffer == null) {
         vertexBuffer = BufferUtils.createFloatBuffer(8192);
         attribBuffer = BufferUtils.createFloatBuffer(8192);
         normalBuffer = BufferUtils.createFloatBuffer(6144);
      } else {
         vertexBuffer.rewind();
         attribBuffer.rewind();
         normalBuffer.rewind();
      }

      this.vertexVBOName = GL15.glGenBuffers();
      GlUtil.glBindBuffer(34962, this.vertexVBOName);
      Controller.loadedVBOBuffers.add(this.vertexVBOName);
      GL15.glBufferData(34962, vertexBuffer, 35048);
      GlUtil.glBindBuffer(34962, 0);
      this.normalVBOName = GL15.glGenBuffers();
      GlUtil.glBindBuffer(34962, this.normalVBOName);
      Controller.loadedVBOBuffers.add(this.normalVBOName);
      GL15.glBufferData(34962, normalBuffer, 35048);
      GlUtil.glBindBuffer(34962, 0);
      this.attribVBOName = GL15.glGenBuffers();
      GlUtil.glBindBuffer(34962, this.attribVBOName);
      Controller.loadedVBOBuffers.add(this.attribVBOName);
      GL15.glBufferData(34962, attribBuffer, 35048);
      GlUtil.glBindBuffer(34962, 0);
   }

   public void setSpriteSize(float var1) {
      this.spriteSize = var1;
   }

   private boolean updateBufferQuads(ParticleContainer var1) {
      vertexBuffer.rewind();
      normalBuffer.rewind();
      attribBuffer.rewind();
      vertexBuffer.limit(vertexBuffer.capacity());
      normalBuffer.limit(normalBuffer.capacity());
      attribBuffer.limit(attribBuffer.capacity());
      this.up = GlUtil.getUpVector(this.up);
      this.right = GlUtil.getRightVector(this.right);
      this.forward = GlUtil.getForwardVector(this.forward);
      int var2 = Math.min(512, this.getParticleController().getParticleCount());
      this.vertexCount = 0;
      int var3;
      if (this.isDrawInverse()) {
         for(var3 = var2 - 1; var3 >= 0; --var3) {
            this.vertexCount += this.handleQuad(var3, var1);
         }
      } else {
         for(var3 = 0; var3 < var2; ++var3) {
            this.vertexCount += this.handleQuad(var3, var1);
         }
      }

      if (vertexBuffer.position() == 0) {
         return false;
      } else {
         vertexBuffer.flip();
         attribBuffer.flip();
         normalBuffer.flip();
         GlUtil.glBindBuffer(34962, this.vertexVBOName);
         GL15.glBufferData(34962, vertexBuffer, 35048);
         GlUtil.glBindBuffer(34962, this.normalVBOName);
         GL15.glBufferData(34962, normalBuffer, 35048);
         GlUtil.glBindBuffer(34962, this.attribVBOName);
         GL15.glBufferData(34962, attribBuffer, 35048);
         return true;
      }
   }

   private boolean updateBuffers() {
      ParticleContainer var1 = this.getParticleController().getParticles();
      return this.updateBufferQuads(var1);
   }
}
