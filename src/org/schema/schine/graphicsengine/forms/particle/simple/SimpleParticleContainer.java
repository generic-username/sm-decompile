package org.schema.schine.graphicsengine.forms.particle.simple;

import java.util.Arrays;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.forms.particle.ParticleContainer;
import org.schema.schine.graphicsengine.forms.particle.StartContainerInterface;

public class SimpleParticleContainer extends ParticleContainer implements StartContainerInterface {
   public static final int blocksizeFloat = 14;
   private static final int pos = 0;
   private static final int velocity = 3;
   private static final int start = 6;
   private static final int color = 9;
   private static final int lifetime = 13;
   private float[] particleArrayFloat;

   public SimpleParticleContainer(int var1) {
      super(var1);
      this.particleArrayFloat = new float[var1 * 14];
   }

   public void reset() {
      Arrays.fill(this.particleArrayFloat, 0.0F);
   }

   public void growCapacity() {
      this.capacity <<= 1;

      assert this.capacity < 65536 : this.capacity;

      this.particleArrayFloat = Arrays.copyOf(this.particleArrayFloat, this.capacity * 14);
   }

   public static final int getIndexFloat(int var0) {
      return var0 * 14;
   }

   public Vector4f getColor(int var1, Vector4f var2) {
      var1 = getIndexFloat(var1);
      var2.x = this.particleArrayFloat[var1 + 9];
      var2.y = this.particleArrayFloat[var1 + 9 + 1];
      var2.z = this.particleArrayFloat[var1 + 9 + 2];
      var2.w = this.particleArrayFloat[var1 + 9 + 3];
      return var2;
   }

   public float getLifetime(int var1) {
      var1 = getIndexFloat(var1);
      return this.particleArrayFloat[var1 + 13];
   }

   public Vector3f getPos(int var1, Vector3f var2) {
      var1 = getIndexFloat(var1);
      var2.x = this.particleArrayFloat[var1];
      var2.y = this.particleArrayFloat[var1 + 1];
      var2.z = this.particleArrayFloat[var1 + 2];
      return var2;
   }

   public Vector3f getStart(int var1, Vector3f var2) {
      var1 = getIndexFloat(var1);
      var2.x = this.particleArrayFloat[var1 + 6];
      var2.y = this.particleArrayFloat[var1 + 6 + 1];
      var2.z = this.particleArrayFloat[var1 + 6 + 2];
      return var2;
   }

   public void copy(int var1, int var2) {
      var1 = getIndexFloat(var1);
      var2 = getIndexFloat(var2);

      for(int var3 = 0; var3 < 14; ++var3) {
         this.particleArrayFloat[var2 + var3] = this.particleArrayFloat[var1 + var3];
      }

   }

   public float[] getArrayFloat() {
      return this.particleArrayFloat;
   }

   protected void swapValuesFloat(int var1, int var2) {
      var1 = getIndexFloat(var1);
      var2 = getIndexFloat(var2);

      for(int var3 = 0; var3 < 14; ++var3) {
         float var4 = this.particleArrayFloat[var2 + var3];
         this.particleArrayFloat[var2 + var3] = this.particleArrayFloat[var1 + var3];
         this.particleArrayFloat[var1 + var3] = var4;
      }

   }

   public Vector3f getVelocity(int var1, Vector3f var2) {
      var1 = getIndexFloat(var1);
      var2.x = this.particleArrayFloat[var1 + 3];
      var2.y = this.particleArrayFloat[var1 + 3 + 1];
      var2.z = this.particleArrayFloat[var1 + 3 + 2];
      return var2;
   }

   public void setColor(int var1, float var2, float var3, float var4, float var5) {
      var1 = getIndexFloat(var1);
      this.particleArrayFloat[var1 + 9] = var2;
      this.particleArrayFloat[var1 + 9 + 1] = var3;
      this.particleArrayFloat[var1 + 9 + 2] = var4;
      this.particleArrayFloat[var1 + 9 + 3] = var5;
   }

   public void setColor(int var1, Vector4f var2) {
      this.setColor(var1, var2.x, var2.y, var2.z, var2.w);
   }

   public void setLifetime(int var1, float var2) {
      this.particleArrayFloat[getIndexFloat(var1) + 13] = var2;
   }

   public void setPos(int var1, float var2, float var3, float var4) {
      var1 = getIndexFloat(var1);
      this.particleArrayFloat[var1] = var2;
      this.particleArrayFloat[var1 + 1] = var3;
      this.particleArrayFloat[var1 + 2] = var4;
   }

   public void setStart(int var1, float var2, float var3, float var4) {
      var1 = getIndexFloat(var1);
      this.particleArrayFloat[var1 + 6] = var2;
      this.particleArrayFloat[var1 + 6 + 1] = var3;
      this.particleArrayFloat[var1 + 6 + 2] = var4;
   }

   public void setVelocity(int var1, float var2, float var3, float var4) {
      var1 = getIndexFloat(var1);
      this.particleArrayFloat[var1 + 3] = var2;
      this.particleArrayFloat[var1 + 3 + 1] = var3;
      this.particleArrayFloat[var1 + 3 + 2] = var4;
   }

   public int getSpriteCodeSpriteMaxY(int var1, int var2, int var3, int var4) {
      return this.getSpriteCode(var1) / 10000;
   }

   public int getSpriteCodeSpriteMaxX(int var1, int var2, int var3, int var4) {
      return this.getSpriteCode(var1) % 10000 / 100;
   }

   public int getSpriteCodeSpriteIndex(int var1, int var2, int var3, int var4) {
      return this.getSpriteCode(var1) % 100;
   }

   protected void swapValuesInt(int var1, int var2) {
   }
}
