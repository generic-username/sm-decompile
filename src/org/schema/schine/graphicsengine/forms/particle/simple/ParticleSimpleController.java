package org.schema.schine.graphicsengine.forms.particle.simple;

import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.particle.ParticleController;

public class ParticleSimpleController extends ParticleController {
   private static final float MAX_LIFETIME = 1300.0F;
   Vector3f pos = new Vector3f();
   Vector3f vel = new Vector3f();

   public ParticleSimpleController(boolean var1) {
      super(var1);
   }

   public ParticleSimpleController(boolean var1, int var2) {
      super(var1, var2);
   }

   public void addOne() {
      if (Controller.getCamera() != null) {
         Vector3f var1 = new Vector3f(Controller.getCamera().getForward());
         Vector3f var2 = new Vector3f(Controller.getCamera().getViewable().getPos());
         var1.scale(0.2F);
         this.addParticle(var2, var1);
      }
   }

   public boolean updateParticle(int var1, Timer var2) {
      float var3 = ((SimpleParticleContainer)this.getParticles()).getLifetime(var1);
      ((SimpleParticleContainer)this.getParticles()).getPos(var1, this.pos);
      ((SimpleParticleContainer)this.getParticles()).getVelocity(var1, this.vel);
      ((SimpleParticleContainer)this.getParticles()).setPos(var1, this.pos.x + this.vel.x, this.pos.y + this.vel.y, this.pos.z + this.vel.z);
      return var3 < 1300.0F;
   }

   protected SimpleParticleContainer getParticleInstance(int var1) {
      return new SimpleParticleContainer(var1);
   }

   public int addParticle(Vector3f var1, Vector3f var2) {
      if (this.particlePointer >= ((SimpleParticleContainer)this.getParticles()).getCapacity() - 1) {
         ((SimpleParticleContainer)this.getParticles()).growCapacity();
      }

      ++this.idGen;
      int var3;
      if (this.isOrderedDelete()) {
         var3 = this.particlePointer % ((SimpleParticleContainer)this.getParticles()).getCapacity();
         ((SimpleParticleContainer)this.getParticles()).setPos(var3, var1.x, var1.y, var1.z);
         ((SimpleParticleContainer)this.getParticles()).setStart(var3, var1.x, var1.y, var1.z);
         ((SimpleParticleContainer)this.getParticles()).setVelocity(var3, var2.x, var2.y, var2.z);
         ((SimpleParticleContainer)this.getParticles()).setLifetime(var3, 0.0F);
         ++this.particlePointer;
         return var3;
      } else {
         var3 = this.particlePointer % ((SimpleParticleContainer)this.getParticles()).getCapacity();
         ((SimpleParticleContainer)this.getParticles()).setPos(var3, var1.x, var1.y, var1.z);
         ((SimpleParticleContainer)this.getParticles()).setStart(var3, var1.x, var1.y, var1.z);
         ((SimpleParticleContainer)this.getParticles()).setVelocity(var3, var2.x, var2.y, var2.z);
         ((SimpleParticleContainer)this.getParticles()).setLifetime(var3, 0.0F);
         ++this.particlePointer;
         return var3;
      }
   }
}
