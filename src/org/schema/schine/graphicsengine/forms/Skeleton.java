package org.schema.schine.graphicsengine.forms;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.animation.Animation;
import org.schema.schine.graphicsengine.core.Timer;

public class Skeleton {
   private final HashMap attachments = new HashMap();
   private final Int2ObjectOpenHashMap bones = new Int2ObjectOpenHashMap();
   private final Int2ObjectOpenHashMap skinningMatrices = new Int2ObjectOpenHashMap();
   private final HashMap animationMap = new HashMap();
   private Bone rootBone;
   private boolean initialized;
   private Transform[] boneMatrices;

   public void addAnim(Animation var1) {
      this.animationMap.put(var1.getName(), var1);
   }

   public Transform[] computeSkinningMatrices() {
      if (this.boneMatrices == null) {
         this.boneMatrices = new Transform[this.skinningMatrices.size()];

         for(int var1 = 0; var1 < this.boneMatrices.length; ++var1) {
            this.boneMatrices[var1] = new Transform();
            this.boneMatrices[var1].setIdentity();
         }
      }

      Iterator var4 = this.bones.entrySet().iterator();

      while(var4.hasNext()) {
         Entry var2 = (Entry)var4.next();
         Transform var3 = (Transform)this.skinningMatrices.get((Integer)var2.getKey());
         ((Bone)var2.getValue()).getOffsetTransform(var3);
         this.boneMatrices[((Bone)var2.getValue()).boneID].set(var3);
      }

      return this.boneMatrices;
   }

   private void createSkinningMatrices() {
      Iterator var1 = this.bones.keySet().iterator();

      while(var1.hasNext()) {
         Integer var2 = (Integer)var1.next();
         Transform var3;
         (var3 = new Transform()).setIdentity();
         this.skinningMatrices.put(var2, var3);
      }

   }

   public Animation getAnim(String var1) {
      Animation var2;
      if ((var2 = (Animation)this.animationMap.get(var1)) == null) {
         throw new IllegalArgumentException("animation '" + var1 + "' not found in " + this.animationMap);
      } else {
         return var2;
      }
   }

   public float getAnimationLength(String var1) {
      Animation var2;
      if ((var2 = (Animation)this.animationMap.get(var1)) == null) {
         throw new IllegalArgumentException("The animation " + var1 + " does not exist in this AnimControl");
      } else {
         return var2.getLength();
      }
   }

   public HashMap getAnimationMap() {
      return this.animationMap;
   }

   public Collection getAnimationNames() {
      return this.animationMap.keySet();
   }

   public Bone getBone(String var1) {
      int var2;
      return (var2 = this.getBoneIndex(var1)) >= 0 ? (Bone)this.getBones().get(var2) : null;
   }

   public int getBoneCount() {
      return this.bones.size();
   }

   public int getBoneIndex(String var1) {
      Iterator var2 = this.getBones().values().iterator();

      Bone var3;
      do {
         if (!var2.hasNext()) {
            System.err.println("WARNING: bone not found in skeleton: " + var1);
            return -1;
         }
      } while(!(var3 = (Bone)var2.next()).name.equals(var1));

      return var3.boneID;
   }

   public Int2ObjectOpenHashMap getBones() {
      return this.bones;
   }

   public Bone getRootBone() {
      return this.rootBone;
   }

   public void setRootBone(Bone var1) {
      this.rootBone = var1;
   }

   public void initialize() {
      this.createSkinningMatrices();
      this.rootBone.update((Timer)null);
      this.rootBone.setBindingPose();
      this.setInitialized(true);
   }

   public boolean isInitialized() {
      return this.initialized;
   }

   public void setInitialized(boolean var1) {
      this.initialized = var1;
   }

   public void removeAnim(Animation var1) {
      if (!this.getAnimationMap().containsKey(var1.getName())) {
         throw new IllegalArgumentException("Given animation does not exist in this AnimControl");
      } else {
         this.animationMap.remove(var1.getName());
      }
   }

   public void reset() {
      this.getRootBone().reset();
   }

   public void setLoadedAnimations(List var1) {
      Iterator var3 = var1.iterator();

      while(var3.hasNext()) {
         Animation var2 = (Animation)var3.next();
         this.animationMap.put(var2.getName(), var2);
      }

   }

   public void update(Timer var1) {
      if (!this.isInitialized()) {
         this.initialize();
      }

      this.rootBone.update(var1);
   }

   public void updateAttachment(Timer var1, Bone var2) {
      Vector3f var3 = new Vector3f(this.rootBone.getLocalPos());
      Quat4f var4 = new Quat4f(this.rootBone.getLocalRot());
      Vector3f var5 = new Vector3f(this.rootBone.getLocalScale());
      this.rootBone.getLocalPos().set(var2.worldPos);
      this.rootBone.getLocalRot().set(var2.worldRot);
      this.rootBone.getLocalScale().set(var2.worldScale);
      this.rootBone.getAddLocalRot().set(var2.getAddLocalRot());
      this.rootBone.getAddLocalPos().set(var2.getAddLocalPos());
      this.rootBone.update(var1);
      this.rootBone.getLocalPos().set(var3);
      this.rootBone.getLocalRot().set(var4);
      this.rootBone.getLocalScale().set(var5);
   }
}
