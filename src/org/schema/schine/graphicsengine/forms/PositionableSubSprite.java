package org.schema.schine.graphicsengine.forms;

public interface PositionableSubSprite extends Positionable {
   float getScale(long var1);

   int getSubSprite(Sprite var1);

   boolean canDraw();
}
