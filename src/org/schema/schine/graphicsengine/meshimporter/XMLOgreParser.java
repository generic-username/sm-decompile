package org.schema.schine.graphicsengine.meshimporter;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;
import javax.vecmath.AxisAngle4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.common.util.linAlg.Quat4Util;
import org.schema.schine.graphicsengine.animation.Animation;
import org.schema.schine.graphicsengine.animation.AnimationTrack;
import org.schema.schine.graphicsengine.animation.BoneAnimationTrack;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.AnimationNotFoundException;
import org.schema.schine.graphicsengine.forms.Bone;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.graphicsengine.forms.Geometry;
import org.schema.schine.graphicsengine.forms.Line;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.MeshGroup;
import org.schema.schine.graphicsengine.forms.OldTerrain;
import org.schema.schine.graphicsengine.forms.SceneNode;
import org.schema.schine.graphicsengine.forms.Skeleton;
import org.schema.schine.graphicsengine.forms.Skin;
import org.schema.schine.graphicsengine.forms.VertexBoneWeight;
import org.schema.schine.graphicsengine.forms.WeightsBuffer;
import org.schema.schine.graphicsengine.texture.Material;
import org.schema.schine.resource.ResourceLoader;
import org.schema.schine.xmlparser.XMLAttribute;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLOgreParser {
   public static final int TYPE_MESH = 0;
   public static final int TYPE_TERRAIN = 1;
   static final String sNEWLINE = System.getProperty("line.separator");
   Map matMap = new Object2ObjectOpenHashMap();
   private String scenePath;
   public boolean recordArrays;
   private String sceneFile;
   private Material[] materials;
   private int bufferIndex;

   public static void main(String[] var0) throws ResourceException {
      XMLOgreParser var2 = new XMLOgreParser();
      String var1 = "ogretest/temple-machine.scene";
      var2.parseScene(var1, "");
   }

   private Animation parseAnimation(XMLOgreContainer var1) {
      Animation var2 = new Animation();
      int var3 = var1.childs.size();
      Iterator var4 = var1.attribs.iterator();

      while(var4.hasNext()) {
         XMLAttribute var5;
         if ((var5 = (XMLAttribute)var4.next()).name.equals("name")) {
            var2.setName(var5.value);
            System.err.println("... Animation name " + var2.getName());
         }

         if (var5.name.equals("loop")) {
            var2.setLoop(Boolean.parseBoolean(var5.value));
         }

         var5.name.equals("interpolationMode");
         var5.name.equals("rotationInterpolationMode");
         var5.name.equals("length");
      }

      this.parseKeyFrames(var1, var3);
      return var2;
   }

   private Vector parseAnimations(XMLOgreContainer var1) {
      Vector var2 = new Vector();
      Iterator var4 = var1.childs.iterator();

      while(var4.hasNext()) {
         XMLOgreContainer var3;
         if ((var3 = (XMLOgreContainer)var4.next()).name.equals("animation")) {
            var2.add(this.parseAnimation(var3));
         }
      }

      return var2;
   }

   private Animation parseBoneAnimation(XMLOgreContainer var1) {
      String var3 = null;
      boolean var4 = true;
      float var5 = 0.0F;
      Iterator var6 = var1.attribs.iterator();

      while(var6.hasNext()) {
         XMLAttribute var7;
         if ((var7 = (XMLAttribute)var6.next()).name.equals("name")) {
            var3 = var7.value;
         }

         if (var7.name.equals("loop")) {
            var4 = Boolean.parseBoolean(var7.value);
         }

         var7.name.equals("interpolationMode");
         var7.name.equals("rotationInterpolationMode");
         if (var7.name.equals("length")) {
            var5 = Float.parseFloat(var7.value);
         }
      }

      Animation var2;
      (var2 = new Animation()).setName(var3);
      var2.setLoop(var4);
      var2.animationLength = var5;
      var6 = var1.childs.iterator();

      while(true) {
         XMLOgreContainer var10;
         do {
            if (!var6.hasNext()) {
               return var2;
            }
         } while(!(var10 = (XMLOgreContainer)var6.next()).name.equals("tracks"));

         Iterator var8 = var10.childs.iterator();

         while(var8.hasNext()) {
            XMLOgreContainer var9;
            if ((var9 = (XMLOgreContainer)var8.next()).name.equals("track")) {
               this.parseBoneAnimationTrack(var2, var9);
            }
         }
      }
   }

   private List parseBoneAnimations(XMLOgreContainer var1) {
      ArrayList var2 = new ArrayList();
      Iterator var4 = var1.childs.iterator();

      while(var4.hasNext()) {
         XMLOgreContainer var3;
         if ((var3 = (XMLOgreContainer)var4.next()).name.equals("animation")) {
            var2.add(this.parseBoneAnimation(var3));
         }
      }

      return var2;
   }

   private Animation parseBoneAnimationTrack(Animation var1, XMLOgreContainer var2) {
      String var3 = null;
      Iterator var4 = var2.attribs.iterator();

      while(var4.hasNext()) {
         XMLAttribute var5;
         if ((var5 = (XMLAttribute)var4.next()).name.equals("bone")) {
            var3 = var5.value;
         }
      }

      var4 = var2.childs.iterator();

      while(var4.hasNext()) {
         XMLOgreContainer var7 = (XMLOgreContainer)var4.next();

         assert var3 != null;

         BoneAnimationTrack var6 = new BoneAnimationTrack(var3);
         this.parseTrackKeyFrames(var7, var6);
         var1.getTracks().put(var6.getBoneName(), var6);
      }

      return var1;
   }

   private void parseBoneAssignments(Mesh var1, XMLOgreContainer var2) {
      VertexBoneWeight[] var3 = new VertexBoneWeight[var2.childs.size()];
      int var4 = 0;

      for(Iterator var11 = var2.childs.iterator(); var11.hasNext(); ++var4) {
         XMLOgreContainer var5;
         if ((var5 = (XMLOgreContainer)var11.next()).name.equals("vertexboneassignment")) {
            int var6 = -1;
            int var7 = -1;
            float var8 = -1.0F;
            Iterator var12 = var5.attribs.iterator();

            while(var12.hasNext()) {
               XMLAttribute var9;
               String var10 = (var9 = (XMLAttribute)var12.next()).value;
               if (var9.name.equals("vertexindex")) {
                  var6 = Integer.parseInt(var10);
               }

               if (var9.name.equals("boneindex")) {
                  var7 = Integer.parseInt(var10);
               }

               if (var9.name.equals("weight")) {
                  var8 = Float.parseFloat(var10);
               }
            }

            VertexBoneWeight var13 = new VertexBoneWeight(var6, var7, var8);
            var3[var4] = var13;
            var1.setVertexBoneAssignments(var3);
         }
      }

   }

   private void parseBoneHierarchy(Skeleton var1, XMLOgreContainer var2, String var3) throws ResourceException {
      Iterator var8 = var2.childs.iterator();

      while(true) {
         while(true) {
            label67:
            while(true) {
               XMLOgreContainer var4;
               Iterator var10;
               Bone var11;
               do {
                  if (!var8.hasNext()) {
                     boolean var9 = false;
                     var10 = var1.getBones().values().iterator();

                     while(var10.hasNext()) {
                        if ((var11 = (Bone)var10.next()).getParent() == null) {
                           if (var9) {
                              System.err.println("WARNING: more than one skeleton root bone found " + var11.name);
                              throw new ResourceException("WARNING: more than one skeleton root bone found " + var11.name + "; from scene: " + this.sceneFile + "; skeleton: " + var3);
                           }

                           var9 = true;
                           var1.setRootBone(var11);
                        }
                     }

                     return;
                  }
               } while(!(var4 = (XMLOgreContainer)var8.next()).name.equals("boneparent"));

               String var5 = null;
               String var6 = null;
               var10 = var4.attribs.iterator();

               while(var10.hasNext()) {
                  XMLAttribute var7;
                  if ((var7 = (XMLAttribute)var10.next()).name.equals("bone")) {
                     var5 = var7.value;
                  }

                  if (var7.name.equals("parent")) {
                     var6 = var7.value;
                  }
               }

               assert var5 != null;

               var10 = var1.getBones().values().iterator();

               while(var10.hasNext()) {
                  Bone var12;
                  if ((var12 = (Bone)var10.next()).name.equals(var5)) {
                     var10 = var1.getBones().values().iterator();

                     while(var10.hasNext()) {
                        if ((var11 = (Bone)var10.next()).name.equals(var6)) {
                           var12.setParent(var11);
                           var11.getChilds().add(var12);
                           continue label67;
                        }
                     }
                     break;
                  }
               }
            }
         }
      }
   }

   private void parseBones(Skeleton var1, XMLOgreContainer var2) {
      Iterator var12 = var2.childs.iterator();

      while(true) {
         XMLOgreContainer var3;
         do {
            if (!var12.hasNext()) {
               return;
            }
         } while(!(var3 = (XMLOgreContainer)var12.next()).name.equals("bone"));

         String var4 = null;
         int var5 = 0;
         Iterator var6 = var3.attribs.iterator();

         while(var6.hasNext()) {
            XMLAttribute var7;
            if ((var7 = (XMLAttribute)var6.next()).name.equals("name")) {
               var4 = var7.value;
            }

            if (var7.name.equals("id")) {
               var5 = Integer.parseInt(var7.value);
            }
         }

         Bone var16;
         (var16 = new Bone(var5, var4)).setSkeleton(var1);
         Vector3f var17 = null;
         Vector3f var14 = new Vector3f(1.0F, 1.0F, 1.0F);
         AxisAngle4f var15 = null;
         Iterator var13 = var3.childs.iterator();

         while(var13.hasNext()) {
            XMLOgreContainer var8;
            if ((var8 = (XMLOgreContainer)var13.next()).name.equals("position")) {
               var17 = this.parseVector3(var8);
            }

            if (var8.name.equals("rotation")) {
               var15 = new AxisAngle4f();
               Iterator var9 = var8.attribs.iterator();

               while(var9.hasNext()) {
                  XMLAttribute var10;
                  String var11 = (var10 = (XMLAttribute)var9.next()).value;
                  if (var10.name.equals("angle")) {
                     var15.angle = Float.parseFloat(var11);
                  }
               }

               var9 = var8.childs.iterator();

               while(var9.hasNext()) {
                  XMLOgreContainer var18;
                  if ((var18 = (XMLOgreContainer)var9.next()).name.equals("axis")) {
                     Vector3f var19;
                     (var19 = this.parseVector3(var18)).normalize();
                     var15.x = var19.x;
                     var15.y = var19.y;
                     var15.z = var19.z;
                  }
               }
            }

            if (var8.name.equals("scale")) {
               var14 = this.parseVector3(var8);
            }
         }

         var16.setBindTransforms(var17, Quat4Util.fromAngleNormalAxis(var15.angle, new Vector3f(var15.x, var15.y, var15.z), new Quat4f()), var14);

         assert var16.name != null;

         assert var16.boneID >= 0;

         var1.getBones().put(var16.boneID, var16);
      }
   }

   private Geometry parseEntity(XMLOgreContainer var1) throws ResourceException {
      Geometry var2 = null;
      String var3 = "default";
      Iterator var4 = var1.attribs.iterator();

      while(var4.hasNext()) {
         XMLAttribute var5;
         String var6 = (var5 = (XMLAttribute)var4.next()).value;
         if (var5.name.equals("description")) {
            var3 = var6;
         }

         var5.name.equals("id");
         var5.name.equals("castShadows");
         var5.name.equals("receiveShadows");
         if (var5.name.equals("meshFile")) {
            assert this.sceneFile != null;

            assert this.scenePath != null;

            var2 = this.parseMesh(this.scenePath, var6, 0);
         }
      }

      var2.setName(var3);
      Material var16 = null;
      Iterator var19 = var1.childs.iterator();

      while(true) {
         while(var19.hasNext()) {
            XMLOgreContainer var20;
            if ((var20 = (XMLOgreContainer)var19.next()).name.equals("subentities")) {
               Iterator var13 = var20.childs.iterator();

               label90:
               while(var13.hasNext()) {
                  Iterator var23 = ((XMLOgreContainer)var13.next()).attribs.iterator();

                  while(true) {
                     XMLAttribute var17;
                     do {
                        if (!var23.hasNext()) {
                           continue label90;
                        }

                        (var17 = (XMLAttribute)var23.next()).name.equals("index");
                     } while(!var17.name.equals("materialName"));

                     if (!this.matMap.containsKey(var17.value) && this.materials == null) {
                        this.materials = this.parseMaterial(this.scenePath + this.sceneFile + ".material");
                        Material[] var22;
                        int var24 = (var22 = this.materials).length;

                        for(int var18 = 0; var18 < var24; ++var18) {
                           Material var9 = var22[var18];
                           this.matMap.put(var9.getName(), var9);
                        }
                     }

                     var16 = (Material)this.matMap.get(var17.value);
                  }
               }
            } else if (var20.name.equals("userData") && var20.text != null) {
               try {
                  DocumentBuilder var14 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                  InputSource var7;
                  (var7 = new InputSource()).setCharacterStream(new StringReader(var20.text));
                  Document var15 = var14.parse(var7);
                  var2.setUserData(var15);
                  Node var21;
                  if ((var21 = var15.getElementsByTagName("Mass").item(0)) != null) {
                     float var8 = Float.parseFloat(var21.getTextContent());
                     var2.setMass(var8);
                  }
               } catch (SAXException var10) {
                  System.err.println("Exception while parsing userdata from " + var2.getName());
                  var10.printStackTrace();
               } catch (IOException var11) {
                  var11.printStackTrace();
               } catch (ParserConfigurationException var12) {
                  var12.printStackTrace();
               }
            }
         }

         var2.setMaterial(var16);
         return var2;
      }
   }

   private void parseEnvironment(XMLOgreContainer var1, AbstractSceneNode var2) {
      Iterator var3 = var1.childs.iterator();

      while(var3.hasNext()) {
         var3.next();
      }

   }

   @Deprecated
   private PosRotScale parseKeyFrames(XMLOgreContainer var1, int var2) {
      PosRotScale var7 = new PosRotScale(var2);
      int var3 = 0;
      Iterator var6 = var1.childs.iterator();

      while(true) {
         XMLOgreContainer var4;
         do {
            if (!var6.hasNext()) {
               return var7;
            }
         } while(!(var4 = (XMLOgreContainer)var6.next()).name.equals("keyframe"));

         Iterator var8 = var4.childs.iterator();

         while(var8.hasNext()) {
            XMLOgreContainer var5;
            if ((var5 = (XMLOgreContainer)var8.next()).name.equals("translation")) {
               var7.pos[var3] = this.parseVector3(var5);
            }

            if (var5.name.equals("rotation")) {
               var7.rot[var3] = this.parseRot(var5);
            }

            if (var5.name.equals("scale")) {
               var7.scale[var3] = this.parseVector3(var5);
            }
         }

         ++var3;
      }
   }

   private Material[] parseMaterial(String var1) throws ResourceException {
      BufferedReader var10 = new BufferedReader(new InputStreamReader(ResourceLoader.resourceUtil.getResourceAsInputStream(var1)));
      StringBuffer var2 = new StringBuffer();

      try {
         while(var10.ready()) {
            var2.append(var10.readLine() + "\n");
         }

         var10.close();
      } catch (IOException var9) {
         var9.printStackTrace();
      }

      String[] var3;
      Material[] var11 = new Material[(var3 = var2.toString().trim().split("material")).length];

      for(int var12 = 0; var12 < var3.length; ++var12) {
         String var4;
         String[] var5;
         if ((var5 = (var4 = var3[var12]).split("\n")).length == 0) {
            System.err.println("Material String: line length 0: " + var4);
         }

         var11[var12] = new Material();
         var11[var12].setName(var5[0].trim());

         for(int var13 = 0; var13 < var5.length; ++var13) {
            String var6;
            float[] var7;
            if ((var6 = var5[var13]).contains("ambient")) {
               var7 = this.parseMaterialRBGA(var6);
               var11[var12].setAmbient(var7);
            }

            if (var6.contains("diffuse")) {
               var7 = this.parseMaterialRBGA(var6);
               var11[var12].setDiffuse(var7);
            }

            if (var6.contains("specular")) {
               var7 = this.parseMaterialRBGA(var6);
               var11[var12].setSpecular(var7);
            }

            if (var6.contains("emissive")) {
               this.parseMaterialRBGA(var6);
            }

            if (var6.contains("texture_unit")) {
               String var14 = null;

               boolean var8;
               for(var8 = false; var13 < var5.length; ++var13) {
                  if ((var6 = var5[var13].trim()).startsWith("texture ")) {
                     var14 = var6.split(" ", 2)[1].trim().replaceAll("\"", "");

                     assert !var14.equals("add") : var6;
                  }

                  if (var6.startsWith("colour_op_ex add src_texture src_current")) {
                     var8 = true;
                  }

                  if (var6.startsWith("}")) {
                     break;
                  }
               }

               if (var14 != null) {
                  if (var8) {
                     assert var14.toLowerCase(Locale.ENGLISH).contains("_em") : var14 + "; line" + var6 + " i " + var13;

                     var11[var12].setEmissiveTextureFile(var14);
                  } else if (var14.toLowerCase(Locale.ENGLISH).contains("_nrm")) {
                     var11[var12].setNormalTextureFile(var14);
                  } else {
                     assert !var14.toLowerCase(Locale.ENGLISH).contains("_em") : var14 + "; line" + var6 + " i " + var13;

                     var11[var12].setTextureFile(var14);
                     var11[var12].setMaterialTextured(true);
                  }
               }
            }
         }
      }

      return var11;
   }

   private float[] parseMaterialRBGA(String var1) {
      String[] var3 = var1.split(" ");
      float[] var2;
      (var2 = new float[4])[0] = Float.parseFloat(var3[1]);
      var2[1] = Float.parseFloat(var3[2]);
      var2[2] = Float.parseFloat(var3[3]);
      if (var3.length > 4) {
         var2[3] = Float.parseFloat(var3[4]);
      } else {
         var2[3] = 1.0F;
      }

      return var2;
   }

   public Geometry parseMesh(String var1, String var2, int var3) throws ResourceException {
      ObjectArrayList var4 = new ObjectArrayList();
      StAXDocument var5 = new StAXDocument();
      XMLOgreParser var6;
      (var6 = new XMLOgreParser()).bufferIndex = this.bufferIndex;
      var5.parseXML(var1 + var2 + ".xml");
      var6.sceneFile = this.sceneFile;
      var6.scenePath = this.scenePath;
      var6.recordArrays = this.recordArrays;
      var2 = null;
      Iterator var9 = var5.getRoot().childs.iterator();

      while(var9.hasNext()) {
         XMLOgreContainer var7;
         if ((var7 = (XMLOgreContainer)var9.next()).name.equals("submeshes")) {
            var4.addAll(var6.parseSubmeshes(var7, var3));
         }

         if (var7.name.equals("submeshnames")) {
            var6.parseSubmeshnames(var7, var4);
         }

         var7.name.equals("poses");
         if (var7.name.equals("skeletonlink")) {
            var2 = this.parseSkeletonName(var7);
         }
      }

      Geometry var10 = (Geometry)var4.remove(0);
      Iterator var11 = var4.iterator();

      while(var11.hasNext()) {
         Geometry var8 = (Geometry)var11.next();
         var10.attach(var8);
      }

      if (var2 != null) {
         this.parseSkeleton((Mesh)var10, var1, var2);
      }

      this.bufferIndex = var6.bufferIndex;
      return var10;
   }

   private void parseNode(XMLOgreContainer var1, AbstractSceneNode var2) throws ResourceException {
      String var3 = "default";
      byte var4 = 1;
      Iterator var5 = var1.attribs.iterator();

      while(var5.hasNext()) {
         XMLAttribute var6;
         if ((var6 = (XMLAttribute)var5.next()).name.equals("name")) {
            var3 = var6.value;
         }

         if (var6.name.equals("visibility")) {
            if (var6.value.equals("visible")) {
               var4 = 1;
            }

            if (var6.value.equals("hidden")) {
               var4 = 2;
            }

            if (var6.value.equals("tree visible")) {
               var4 = 4;
            }

            if (var6.value.equals("tree hidden")) {
               var4 = 8;
            }
         }
      }

      Object var14 = null;
      Vector3f var15 = new Vector3f();
      Vector4f var7 = new Vector4f();
      Vector3f var8 = new Vector3f();
      Vector var9 = null;
      boolean var10 = false;
      Iterator var11 = var1.childs.iterator();

      XMLOgreContainer var12;
      while(var11.hasNext()) {
         if ((var12 = (XMLOgreContainer)var11.next()).name.equals("position")) {
            var15 = this.parseVector3(var12);
         }

         if (var12.name.equals("rotation")) {
            var7 = this.parseRot(var12);
         }

         if (var12.name.equals("scale")) {
            var8 = this.parseVector3(var12);
         }

         if (var12.name.equals("animations")) {
            var9 = this.parseAnimations(var12);
         }

         if (var12.name.equals("entity")) {
            var10 = true;

            assert this.sceneFile != null;

            assert this.scenePath != null;

            ((AbstractSceneNode)(var14 = this.parseEntity(var12))).setVisibility(var4);
         }
      }

      if (!var10) {
         ((AbstractSceneNode)(var14 = new SceneNode())).setVisibility(var4);
      }

      var11 = var1.childs.iterator();

      while(var11.hasNext()) {
         if ((var12 = (XMLOgreContainer)var11.next()).name.equals("node")) {
            this.parseNode(var12, (AbstractSceneNode)var14);
         }
      }

      assert !var2.getChilds().contains(var14);

      var2.attach((AbstractSceneNode)var14);
      if (var9 != null) {
         ((AbstractSceneNode)var14).setAnimations(var9);

         try {
            ((AbstractSceneNode)var14).selectAnimation(((AbstractSceneNode)var14).getName());
         } catch (AnimationNotFoundException var13) {
            var13.printStackTrace();
         }

         ((AbstractSceneNode)var14).setAnimated(true);
      }

      ((AbstractSceneNode)var14).setInitionPos(var15);
      ((AbstractSceneNode)var14).setQuatRot(var7);
      ((AbstractSceneNode)var14).setInitialQuadRot(var7);
      ((AbstractSceneNode)var14).getScale().set(var8);
      ((AbstractSceneNode)var14).setInitialScale(new Vector3f(var8));
      ((AbstractSceneNode)var14).setName(var3);
   }

   private void parseNodes(XMLOgreContainer var1, AbstractSceneNode var2) throws ResourceException {
      Iterator var4 = var1.childs.iterator();

      while(var4.hasNext()) {
         XMLOgreContainer var3;
         if ((var3 = (XMLOgreContainer)var4.next()).name.equals("node")) {
            this.parseNode(var3, var2);
         }
      }

   }

   public MeshGroup parseScene(String var1, String var2) throws ResourceException {
      GlUtil.LOCKDYN = !this.recordArrays;
      this.scenePath = var1;
      this.sceneFile = var2;
      this.bufferIndex = 0;

      assert this.sceneFile != null;

      assert this.scenePath != null;

      MeshGroup var3 = new MeshGroup();

      for(var1 = var1 + var2 + ".scene"; var1.contains("//"); var1 = var1.replaceAll("//", "/")) {
      }

      StAXDocument var6;
      (var6 = new StAXDocument()).parseXML(var1);
      Iterator var5 = var6.getRoot().childs.iterator();

      while(var5.hasNext()) {
         XMLOgreContainer var8;
         if ((var8 = (XMLOgreContainer)var5.next()).name.equals("environment")) {
            this.parseEnvironment(var8, var3);
         }

         if (var8.name.equals("nodes")) {
            assert this.sceneFile != null;

            assert this.scenePath != null;

            this.parseNodes(var8, var3);
         }
      }

      int var7 = 0;
      Iterator var9 = var3.getChilds().iterator();

      while(var9.hasNext()) {
         AbstractSceneNode var4;
         if ((var4 = (AbstractSceneNode)var9.next()) instanceof Geometry) {
            var7 = (int)((float)var7 + ((Geometry)var4).getMass());
         }
      }

      var3.setMass((float)var7);
      var3.setBoundingBox(var3.makeBiggestBoundingBox(new Vector3f(), new Vector3f()));
      var3.makeBoundingSphere();
      var3.setLoaded(true);
      var3.scenePath = this.scenePath;
      var3.sceneFile = this.sceneFile;
      return var3;
   }

   private void parseSkeleton(Mesh var1, String var2, String var3) throws ResourceException {
      Skeleton var9 = this.parseSkeletonFile(var2, var3);
      HashMap var10 = new HashMap();
      VertexBoneWeight[] var4;
      int var5 = (var4 = var1.getVertexBoneAssignments()).length;

      for(int var6 = 0; var6 < var5; ++var6) {
         VertexBoneWeight var7 = var4[var6];
         Object var8;
         if ((var8 = (List)var10.get(var7.vertexIndex)) == null) {
            var8 = new com.bulletphysics.util.ObjectArrayList();
            var10.put(var7.vertexIndex, var8);
         }

         ((List)var8).add(var7);

         assert ((List)var8).size() < 5;
      }

      WeightsBuffer var11;
      (var11 = new WeightsBuffer(var1.getVertCount())).initialize(var10, var9);
      var1.setVertexBoneAssignments((VertexBoneWeight[])null);
      var1.setSkin(new Skin(var9, var11));
   }

   private Skeleton parseSkeletonFile(String var1, String var2) throws ResourceException {
      XMLOgreParser var3;
      (var3 = new XMLOgreParser()).bufferIndex = this.bufferIndex;
      StAXDocument var4;
      (var4 = new StAXDocument()).parseXML(var1 + var2 + ".xml");
      Skeleton var9 = new Skeleton();
      XMLOgreContainer var5 = null;
      XMLOgreContainer var6 = null;
      XMLOgreContainer var7 = null;
      Iterator var10 = var4.getRoot().childs.iterator();

      while(var10.hasNext()) {
         XMLOgreContainer var8;
         if ((var8 = (XMLOgreContainer)var10.next()).name.equals("bones")) {
            var5 = var8;
         }

         if (var8.name.equals("bonehierarchy")) {
            var6 = var8;
         }

         if (var8.name.equals("animations")) {
            var7 = var8;
         }
      }

      this.parseBones(var9, var5);
      this.parseBoneHierarchy(var9, var6, var2);
      if (var7 != null) {
         var9.setLoadedAnimations(this.parseBoneAnimations(var7));
      }

      this.bufferIndex = var3.bufferIndex;
      return var9;
   }

   private String parseSkeletonName(XMLOgreContainer var1) {
      Iterator var3 = var1.attribs.iterator();

      XMLAttribute var2;
      do {
         if (!var3.hasNext()) {
            return null;
         }
      } while(!(var2 = (XMLAttribute)var3.next()).name.equals("name"));

      return var2.value;
   }

   public Geometry parseSubmesh(XMLOgreContainer var1, int var2) {
      Object var3 = null;
      String var4 = null;
      Iterator var5 = var1.attribs.iterator();

      while(var5.hasNext()) {
         XMLAttribute var6;
         String var7 = (var6 = (XMLAttribute)var5.next()).value;
         var6.name.equals("material");
         if (var6.name.equals("usesharedvertices")) {
            Boolean.parseBoolean(var7);
         }

         if (var6.name.equals("use32bitindexes")) {
            Boolean.parseBoolean(var7);
         }

         if (var6.name.equals("operationtype")) {
            var4 = var7;
         }
      }

      if (var4.equals("line_list")) {
         var3 = new Line();
      } else {
         if (var2 == 0) {
            ((Geometry)(var3 = new Mesh())).scenePath = this.scenePath;
            ((Geometry)var3).sceneFile = this.sceneFile;
         }

         if (var2 == 1) {
            var3 = new OldTerrain();
         }
      }

      int var15 = 0;

      for(Iterator var18 = var1.childs.iterator(); var18.hasNext(); ++var15) {
         XMLOgreContainer var19;
         if ((var19 = (XMLOgreContainer)var18.next()).name.equals("faces") && var3 instanceof Mesh) {
            Mesh var10 = (Mesh)var3;
            int var14 = Integer.parseInt(((XMLAttribute)var19.attribs.get(0)).value);
            var10.setFaceCount(var14);
            if (!this.recordArrays) {
               var10.setIndexBuffer(GlUtil.getDynamicByteBuffer(var14 * 3 << 2, this.bufferIndex++).asIntBuffer());
               var10.normalIndexBuffer = GlUtil.getDynamicByteBuffer(var14 * 3 << 2, this.bufferIndex++).asIntBuffer();
               var10.texCoordIndexBuffer = GlUtil.getDynamicByteBuffer(var14 * 3 << 2, this.bufferIndex++).asIntBuffer();
               var10.tangentIndexBuffer = GlUtil.getDynamicByteBuffer(var14 * 3 << 2, this.bufferIndex++).asIntBuffer();
               var10.binormalIndexBuffer = GlUtil.getDynamicByteBuffer(var14 * 3 << 2, this.bufferIndex++).asIntBuffer();
            }

            Iterator var8 = var19.childs.iterator();

            while(var8.hasNext()) {
               XMLOgreContainer var12 = (XMLOgreContainer)var8.next();
               var10.setIndicedNormals(true);

               for(var14 = 0; var14 < 3; ++var14) {
                  int var9 = Integer.parseInt(((XMLAttribute)var12.attribs.get(var14)).value);
                  if (this.recordArrays) {
                     var10.recordedIndices.add(var9);
                  } else {
                     var10.getIndexBuffer().put(var9);
                     var10.texCoordIndexBuffer.put(var9);
                     var10.normalIndexBuffer.put(var9);
                     var10.tangentIndexBuffer.put(var9);
                     var10.binormalIndexBuffer.put(var9);
                  }
               }
            }
         }

         if (var19.name.equals("geometry")) {
            int var11 = 0;
            Iterator var13 = var19.attribs.iterator();

            while(var13.hasNext()) {
               XMLAttribute var16;
               String var20 = (var16 = (XMLAttribute)var13.next()).value;
               if (var16.name.equals("vertexcount")) {
                  var11 = Integer.parseInt(var20);
                  ((Geometry)var3).setVertCount(var11);
                  if (var3 instanceof Mesh) {
                     ((Mesh)var3).setTexCoordCount(var11);
                  }
               }
            }

            var13 = var19.childs.iterator();

            while(var13.hasNext()) {
               XMLOgreContainer var17;
               if ((var17 = (XMLOgreContainer)var13.next()).name.equals("vertexbuffer")) {
                  this.parseVertexBuffer((Geometry)var3, var17, var11, var15);
               }
            }
         }

         if (var19.name.equals("boneassignments")) {
            this.parseBoneAssignments((Mesh)var3, var19);
         }
      }

      if (!this.recordArrays) {
         ((Geometry)var3).verticesBuffer.rewind();
         ((Geometry)var3).getIndexBuffer().rewind();
      }

      ((Geometry)var3).scenePath = this.scenePath;
      ((Geometry)var3).sceneFile = this.sceneFile;

      assert this.sceneFile != null;

      assert this.scenePath != null;

      if (var3 instanceof Mesh && !this.recordArrays) {
         ((Mesh)var3).texCoordsBuffer.flip();
         ((Mesh)var3).normalsBuffer.flip();
         if (((Mesh)var3).hasTangents) {
            ((Mesh)var3).tangentsBuffer.flip();
         }

         if (((Mesh)var3).hasBinormals) {
            ((Mesh)var3).binormalsBuffer.flip();
         }

         ((Mesh)var3).normalIndexBuffer.flip();
         ((Mesh)var3).texCoordIndexBuffer.flip();
         ((Mesh)var3).tangentIndexBuffer.flip();
         ((Mesh)var3).binormalIndexBuffer.flip();
      }

      return (Geometry)var3;
   }

   public Vector parseSubmeshes(XMLOgreContainer var1, int var2) {
      Vector var3 = new Vector();
      Iterator var5 = var1.childs.iterator();

      while(var5.hasNext()) {
         XMLOgreContainer var4 = (XMLOgreContainer)var5.next();
         Geometry var6 = this.parseSubmesh(var4, var2);
         var3.add(var6);
      }

      return var3;
   }

   public void parseSubmeshnames(XMLOgreContainer var1, List var2) {
      Iterator var7 = var1.childs.iterator();

      while(var7.hasNext()) {
         XMLOgreContainer var3 = (XMLOgreContainer)var7.next();
         int var4 = 0;
         String var5 = "";
         Iterator var8 = var3.attribs.iterator();

         while(var8.hasNext()) {
            XMLAttribute var6;
            if ((var6 = (XMLAttribute)var8.next()).name.equals("name")) {
               var5 = var6.value;
            }

            if (var6.name.equals("index")) {
               var4 = Integer.parseInt(var6.value);
            }
         }

         ((Geometry)var2.get(var4)).setName(var5);
      }

   }

   @Deprecated
   public Geometry parseSubmeshToMEshArrays(XMLOgreContainer var1, int var2) {
      Object var3 = null;
      String var4 = null;
      Iterator var5 = var1.attribs.iterator();

      while(var5.hasNext()) {
         XMLAttribute var6;
         String var7 = (var6 = (XMLAttribute)var5.next()).value;
         var6.name.equals("material");
         if (var6.name.equals("usesharedvertices")) {
            Boolean.parseBoolean(var7);
         }

         if (var6.name.equals("use32bitindexes")) {
            Boolean.parseBoolean(var7);
         }

         if (var6.name.equals("operationtype")) {
            var4 = var7;
         }
      }

      if (var4.equals("line_list")) {
         var3 = new Line();
      } else {
         if (var2 == 0) {
            var3 = new Mesh();
         }

         if (var2 == 1) {
            var3 = new OldTerrain();
         }
      }

      var5 = var1.childs.iterator();

      while(true) {
         int var8;
         int var21;
         Iterator var22;
         XMLOgreContainer var27;
         XMLOgreContainer var29;
         label249:
         do {
            if (!var5.hasNext()) {
               return (Geometry)var3;
            }

            if ((var29 = (XMLOgreContainer)var5.next()).name.equals("faces")) {
               Mesh var31 = null;
               if (var3 instanceof Mesh) {
                  var31 = (Mesh)var3;
                  var21 = Integer.parseInt(((XMLAttribute)var29.attribs.get(0)).value);
                  var31.faces = new Mesh.Face[var21];
                  var31.setFaceCount(var21);
               }

               var21 = 0;

               for(var22 = var29.childs.iterator(); var22.hasNext(); ++var21) {
                  var27 = (XMLOgreContainer)var22.next();
                  var31.faces[var21] = new Mesh.Face();
                  var31.setIndicedNormals(true);

                  for(var8 = 0; var8 < var31.faces[var21].m_vertsIndex.length; ++var8) {
                     var31.faces[var21].m_vertsIndex[var8] = Integer.parseInt(((XMLAttribute)var27.attribs.get(var8)).value);
                     var31.faces[var21].m_texCoordsIndex[var8] = Integer.parseInt(((XMLAttribute)var27.attribs.get(var8)).value);
                     var31.faces[var21].m_normalIndex[var8] = Integer.parseInt(((XMLAttribute)var27.attribs.get(var8)).value);
                  }
               }
            }

            if (var29.name.equals("geometry")) {
               int var32 = 0;
               Iterator var23 = var29.attribs.iterator();

               while(var23.hasNext()) {
                  XMLAttribute var24;
                  var4 = (var24 = (XMLAttribute)var23.next()).value;
                  if (var24.name.equals("vertexcount")) {
                     var32 = Integer.parseInt(var4);
                     ((Geometry)var3).setVertCount(var32);
                     if (var3 instanceof Mesh) {
                        ((Mesh)var3).setTexCoordCount(var32);
                     }
                  }
               }

               var23 = var29.childs.iterator();

               while(true) {
                  XMLOgreContainer var25;
                  do {
                     if (!var23.hasNext()) {
                        continue label249;
                     }
                  } while(!(var25 = (XMLOgreContainer)var23.next()).name.equals("vertexbuffer"));

                  boolean var28 = false;
                  boolean var33 = false;
                  boolean var9 = false;
                  boolean var10 = false;
                  int var11 = 0;
                  Iterator var12 = var25.attribs.iterator();

                  while(var12.hasNext()) {
                     XMLAttribute var13;
                     String var14 = (var13 = (XMLAttribute)var12.next()).value;
                     System.err.println("PARSING VAL: " + var14);
                     if (var13.name.equals("positions")) {
                        var28 = Boolean.parseBoolean(var14);
                     }

                     if (var13.name.equals("normals")) {
                        var33 = Boolean.parseBoolean(var14);
                     }

                     if (var13.name.equals("tangents")) {
                        var9 = Boolean.parseBoolean(var14);
                     }

                     if (var13.name.equals("binormals")) {
                        var10 = Boolean.parseBoolean(var14);
                     }

                     if (var13.name.equals("texture_coords")) {
                        var11 = Integer.parseInt(var14);
                     }

                     if (var13.name.equals("texture_coord_dimension_0")) {
                        Integer.parseInt(var14);
                     }

                     assert !var13.name.equals("texture_coord_dimensions_0") || var14.equals("float1") || var14.equals("float2") || var14.equals("float3");
                  }

                  ((Geometry)var3).vertices = new Vector3f[var32];
                  if (var3 instanceof Mesh) {
                     if (var33) {
                        ((Mesh)var3).normals = new Vector3f[var32];
                     }

                     ((Mesh)var3).texCoords = new Vector3f[var32];
                     if (var9) {
                        ((Mesh)var3).tangents = new Vector3f[var32];
                     }

                     if (var10) {
                        ((Mesh)var3).binormals = new Vector3f[var32];
                     }
                  }

                  int var39 = 0;
                  float var41 = -2.14748365E9F;
                  float var42 = 2.14748365E9F;
                  float var15 = -2.14748365E9F;
                  float var16 = 2.14748365E9F;
                  float var17 = -2.14748365E9F;
                  float var18 = 2.14748365E9F;

                  for(var22 = var25.childs.iterator(); var22.hasNext(); ++var39) {
                     Iterator var19 = ((XMLOgreContainer)var22.next()).childs.iterator();

                     while(var19.hasNext()) {
                        XMLOgreContainer var20 = (XMLOgreContainer)var19.next();
                        if (var28 && var20.name.equals("position")) {
                           ((Geometry)var3).vertices[var39] = this.parseVector3(var20);
                           var41 = ((Geometry)var3).vertices[var39].x > var41 ? ((Geometry)var3).vertices[var39].x : var41;
                           var42 = ((Geometry)var3).vertices[var39].x < var42 ? ((Geometry)var3).vertices[var39].x : var42;
                           var15 = ((Geometry)var3).vertices[var39].y > var15 ? ((Geometry)var3).vertices[var39].y : var15;
                           var16 = ((Geometry)var3).vertices[var39].y < var16 ? ((Geometry)var3).vertices[var39].y : var16;
                           var17 = ((Geometry)var3).vertices[var39].z > var17 ? ((Geometry)var3).vertices[var39].z : var17;
                           var18 = ((Geometry)var3).vertices[var39].z < var18 ? ((Geometry)var3).vertices[var39].z : var18;
                        }

                        if (var33 && var20.name.equals("normal")) {
                           ((Mesh)var3).normals[var39] = this.parseVector3(var20);
                        }

                        if (var9 && var20.name.equals("tangent")) {
                           ((Mesh)var3).tangents[var39] = this.parseVector3(var20);
                        }

                        if (var10 && var20.name.equals("binormal")) {
                           ((Mesh)var3).binormals[var39] = this.parseVector3(var20);
                        }

                        if (var11 > 0 && var20.name.equals("texcoord")) {
                           ((Mesh)var3).texCoords[var39] = this.parseVector3(var20);
                        }
                     }
                  }

                  Vector3f var26 = new Vector3f(var42, var16, var18);
                  Vector3f var43 = new Vector3f(var41, var15, var17);
                  ((Geometry)var3).setBoundingBox(new BoundingBox(var26, var43));
                  ((Geometry)var3).makeBoundingSphere();
               }
            }
         } while(!var29.name.equals("boneassignments"));

         VertexBoneWeight[] var34 = new VertexBoneWeight[var29.childs.size()];
         var21 = 0;

         for(var22 = var29.childs.iterator(); var22.hasNext(); ++var21) {
            if ((var27 = (XMLOgreContainer)var22.next()).name.equals("vertexboneassignment")) {
               var8 = -1;
               int var35 = -1;
               float var36 = -1.0F;
               Iterator var37 = var27.attribs.iterator();

               while(var37.hasNext()) {
                  XMLAttribute var30;
                  String var40 = (var30 = (XMLAttribute)var37.next()).value;
                  if (var30.name.equals("vertexindex")) {
                     var8 = Integer.parseInt(var40);
                  }

                  if (var30.name.equals("boneindex")) {
                     var35 = Integer.parseInt(var40);
                  }

                  if (var30.name.equals("weight")) {
                     var36 = Float.parseFloat(var40);
                  }
               }

               VertexBoneWeight var38 = new VertexBoneWeight(var8, var35, var36);
               var34[var21] = var38;
               ((Mesh)var3).setVertexBoneAssignments(var34);
            }
         }
      }
   }

   private void parseTrackKeyFrames(XMLOgreContainer var1, AnimationTrack var2) {
      Iterator var12 = var1.childs.iterator();

      while(true) {
         XMLOgreContainer var3;
         do {
            if (!var12.hasNext()) {
               return;
            }
         } while(!(var3 = (XMLOgreContainer)var12.next()).name.equals("keyframe"));

         float var4 = -1.0F;
         Iterator var5 = var3.attribs.iterator();

         while(var5.hasNext()) {
            XMLAttribute var6;
            if ((var6 = (XMLAttribute)var5.next()).name.equals("time")) {
               var4 = Float.parseFloat(var6.value);
            }
         }

         Vector3f var15 = null;
         AxisAngle4f var16 = null;
         Vector3f var7 = null;
         Iterator var13 = var3.childs.iterator();

         while(var13.hasNext()) {
            XMLOgreContainer var8;
            if ((var8 = (XMLOgreContainer)var13.next()).name.equals("translate")) {
               var15 = this.parseVector3(var8, new Vector3f());
            }

            if (var8.name.equals("rotate")) {
               var16 = new AxisAngle4f();
               Iterator var9 = var8.attribs.iterator();

               while(var9.hasNext()) {
                  XMLAttribute var10;
                  String var11 = (var10 = (XMLAttribute)var9.next()).value;
                  if (var10.name.equals("angle")) {
                     var16.angle = Float.parseFloat(var11);
                  }
               }

               var9 = var8.childs.iterator();

               while(var9.hasNext()) {
                  XMLOgreContainer var17;
                  if ((var17 = (XMLOgreContainer)var9.next()).name.equals("axis")) {
                     Vector3f var18;
                     (var18 = this.parseVector3(var17, new Vector3f())).normalize();
                     var16.x = var18.x;
                     var16.y = var18.y;
                     var16.z = var18.z;
                  }
               }
            }

            if (var8.name.equals("scale")) {
               var7 = this.parseVector3(var8);
            }
         }

         assert var4 >= 0.0F;

         assert var15 != null;

         assert var16 != null;

         Quat4f var14 = Quat4Util.fromAngleAxis(var16.angle, new Vector3f(var16.x, var16.y, var16.z), new Quat4f());
         var2.addFrame(var4, var15, var14, var7);
      }
   }

   private Vector3f parseVector3(XMLOgreContainer var1) {
      return this.parseVector3(var1, new Vector3f());
   }

   private Vector3f parseVector3(XMLOgreContainer var1, Vector3f var2) {
      XMLAttribute var3 = (XMLAttribute)var1.attribs.get(0);
      var2.x = Float.parseFloat(var3.value);
      var3 = (XMLAttribute)var1.attribs.get(1);
      var2.y = Float.parseFloat(var3.value);
      if (var1.attribs.size() > 2) {
         var3 = (XMLAttribute)var1.attribs.get(2);
         var2.z = Float.parseFloat(var3.value);
      } else {
         var2.z = 1.0F;
      }

      return var2;
   }

   private Vector4f parseRot(XMLOgreContainer var1) {
      Vector4f var2 = new Vector4f();

      for(int var3 = 0; var3 < var1.attribs.size(); ++var3) {
         XMLAttribute var4;
         if ((var4 = (XMLAttribute)var1.attribs.get(var3)).name.toLowerCase(Locale.ENGLISH).equals("qx")) {
            var2.x = Float.parseFloat(var4.value);
         } else if (var4.name.toLowerCase(Locale.ENGLISH).equals("qy")) {
            var2.y = Float.parseFloat(var4.value);
         } else if (var4.name.toLowerCase(Locale.ENGLISH).equals("qz")) {
            var2.z = Float.parseFloat(var4.value);
         } else {
            if (!var4.name.toLowerCase(Locale.ENGLISH).equals("qw")) {
               throw new RuntimeException("Malformed rotation " + var1);
            }

            var2.w = Float.parseFloat(var4.value);
         }
      }

      return var2;
   }

   private void parseVertexBuffer(Geometry var1, XMLOgreContainer var2, int var3, int var4) {
      boolean var21 = false;
      boolean var5 = false;
      boolean var6 = false;
      boolean var7 = false;
      int var8 = 0;
      int var9 = 0;
      Iterator var10 = var2.attribs.iterator();

      while(true) {
         XMLAttribute var11;
         String var12;
         do {
            if (!var10.hasNext()) {
               if (var21 && !this.recordArrays) {
                  var1.verticesBuffer = GlUtil.getDynamicByteBuffer(var3 * 3 << 2, this.bufferIndex++).asFloatBuffer();
               }

               if (var5 && !this.recordArrays) {
                  ((Mesh)var1).normalsBuffer = GlUtil.getDynamicByteBuffer(var3 * 3 << 2, this.bufferIndex++).asFloatBuffer();
               }

               if (var6) {
                  ((Mesh)var1).hasTangents = true;
                  if (!this.recordArrays) {
                     ((Mesh)var1).tangentsBuffer = GlUtil.getDynamicByteBuffer(var3 * 3 << 2, this.bufferIndex++).asFloatBuffer();
                  }
               }

               if (var7) {
                  ((Mesh)var1).hasBinormals = true;
                  if (!this.recordArrays) {
                     ((Mesh)var1).binormalsBuffer = GlUtil.getDynamicByteBuffer(var3 * 3 << 2, this.bufferIndex++).asFloatBuffer();
                  }
               }

               if (var8 > 0) {
                  if (!this.recordArrays) {
                     ((Mesh)var1).texCoordsBuffer = GlUtil.getDynamicByteBuffer(var3 * var9 * var8 << 2, this.bufferIndex++).asFloatBuffer();
                  }

                  ((Mesh)var1).texCoordSetCount = var8;
               }

               Vector3f var24 = new Vector3f();
               int var27 = 0;
               float var22 = -2.14748365E9F;
               float var23 = 2.14748365E9F;
               float var25 = -2.14748365E9F;
               float var26 = 2.14748365E9F;
               float var13 = -2.14748365E9F;
               float var14 = 2.14748365E9F;
               Iterator var19 = var2.childs.iterator();

               while(var19.hasNext()) {
                  Iterator var15 = ((XMLOgreContainer)var19.next()).childs.iterator();

                  while(var15.hasNext()) {
                     XMLOgreContainer var16 = (XMLOgreContainer)var15.next();
                     Vector3f var17;
                     if (var21 && var16.name.equals("position")) {
                        var17 = this.parseVector3(var16, var24);
                        if (!this.recordArrays) {
                           var1.verticesBuffer.put(var17.x);
                           var1.verticesBuffer.put(var17.y);
                           var1.verticesBuffer.put(var17.z);
                        }

                        var22 = var17.x > var22 ? var17.x : var22;
                        var23 = var17.x < var23 ? var17.x : var23;
                        var25 = var17.y > var25 ? var17.y : var25;
                        var26 = var17.y < var26 ? var17.y : var26;
                        var13 = var17.z > var13 ? var17.z : var13;
                        var14 = var17.z < var14 ? var17.z : var14;
                        ++var27;
                        if (this.recordArrays) {
                           ((Mesh)var1).recordedVectices.add(new Vector3f(var17));
                        }
                     }

                     if (var5 && var16.name.equals("normal")) {
                        var17 = this.parseVector3(var16, var24);
                        if (this.recordArrays) {
                           ((Mesh)var1).recordedNormals.add(new Vector3f(var17));
                        } else {
                           ((Mesh)var1).normalsBuffer.put(var17.x);
                           ((Mesh)var1).normalsBuffer.put(var17.y);
                           ((Mesh)var1).normalsBuffer.put(var17.z);
                        }
                     }

                     if (var5 && var16.name.equals("tangent") && !this.recordArrays) {
                        var17 = this.parseVector3(var16);
                        ((Mesh)var1).tangentsBuffer.put(var17.x);
                        ((Mesh)var1).tangentsBuffer.put(var17.y);
                        ((Mesh)var1).tangentsBuffer.put(var17.z);
                     }

                     if (var5 && var16.name.equals("binormal") && !this.recordArrays) {
                        var17 = this.parseVector3(var16);
                        ((Mesh)var1).binormalsBuffer.put(var17.x);
                        ((Mesh)var1).binormalsBuffer.put(var17.y);
                        ((Mesh)var1).binormalsBuffer.put(var17.z);
                     }

                     if (var8 > 0 && var16.name.equals("texcoord")) {
                        var17 = this.parseVector3(var16);
                        if (this.recordArrays) {
                           ((Mesh)var1).recordedTextcoords.add(new Vector2f(var17.x, var17.y));
                        } else {
                           ((Mesh)var1).texCoordsBuffer.put(var17.x);
                           ((Mesh)var1).texCoordsBuffer.put(var17.y);
                        }
                     }
                  }
               }

               if (var21) {
                  Vector3f var20 = new Vector3f(var23, var26, var14);
                  Vector3f var28 = new Vector3f(var22, var25, var13);

                  assert var27 == var3;

                  var1.setBoundingBox(new BoundingBox(var20, var28));
                  var1.makeBoundingSphere();
               }

               return;
            }

            var12 = (var11 = (XMLAttribute)var10.next()).value;
            if (var11.name.equals("positions")) {
               var21 = Boolean.parseBoolean(var12);
            }

            if (var11.name.equals("normals")) {
               var5 = Boolean.parseBoolean(var12);
            }

            if (var11.name.equals("tangents")) {
               var6 = Boolean.parseBoolean(var12);
            }

            if (var11.name.equals("binormals")) {
               var7 = Boolean.parseBoolean(var12);
            }

            if (var11.name.equals("texture_coords")) {
               var8 = Integer.parseInt(var12);
            }

            if (var11.name.equals("texture_coord_dimension_0")) {
               var9 = Integer.parseInt(var12);
            }
         } while(!var11.name.equals("texture_coord_dimensions_0"));

         try {
            var9 = Integer.parseInt(var12);
         } catch (NumberFormatException var18) {
            if (var12.equals("float1")) {
               var9 = 1;
            } else if (var12.equals("float2")) {
               var9 = 2;
            } else if (var12.equals("float3")) {
               var9 = 3;
            } else {
               assert false : var1.getName() + ": " + var11.name + ": " + var12;
            }
         }
      }
   }
}
