package org.schema.schine.graphicsengine.meshimporter;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.codehaus.stax2.XMLInputFactory2;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.resource.ResourceLoader;
import org.schema.schine.xmlparser.XMLAttribute;

public class StAXDocument {
   XMLOgreContainer current;
   XMLOgreContainer parent;
   private XMLOgreContainer root;

   public void parseXML(String var1) throws ResourceException {
      this.parseXML(var1, false);
   }

   public void parseXML(String var1, boolean var2) throws ResourceException {
      try {
         XMLInputFactory2 var3;
         (var3 = (XMLInputFactory2)XMLInputFactory2.newInstance()).setProperty("javax.xml.stream.isReplacingEntityReferences", Boolean.FALSE);
         var3.setProperty("javax.xml.stream.isSupportingExternalEntities", Boolean.FALSE);
         var3.setProperty("javax.xml.stream.isCoalescing", Boolean.FALSE);
         var3.configureForSpeed();
         XMLStreamReader var8;
         if (var2) {
            var8 = var3.createXMLStreamReader(new GZIPInputStream(ResourceLoader.resourceUtil.getResourceAsInputStream(var1)));
         } else {
            var8 = var3.createXMLStreamReader(ResourceLoader.resourceUtil.getResourceAsInputStream(var1));
         }

         while(true) {
            while(var8.hasNext()) {
               switch(var8.next()) {
               case 1:
                  if (this.getRoot() == null) {
                     this.setRoot(new XMLOgreContainer());
                     this.getRoot().name = var8.getLocalName();
                     this.current = this.getRoot();
                  } else {
                     XMLOgreContainer var9;
                     (var9 = new XMLOgreContainer()).name = var8.getLocalName();
                     var9.attribs = new ObjectArrayList(var8.getAttributeCount());

                     for(int var4 = 0; var4 < var8.getAttributeCount(); ++var4) {
                        String var5 = var8.getAttributeLocalName(var4);
                        if ("".equals(var5)) {
                           var5 = var8.getAttributeName(var4).getLocalPart();
                        }

                        var9.attribs.add(new XMLAttribute(var5, var8.getAttributeValue(var4)));
                     }

                     this.current.childs.add(var9);
                     var9.parent = this.current;
                     if (!this.current.ended) {
                        this.current = var9;
                     }
                  }
                  break;
               case 2:
                  if (this.current.name.equals(var8.getLocalName())) {
                     this.current.ended = true;
                     if (this.current.parent != null) {
                        this.current = this.current.parent;
                     }
                  }
               case 3:
               default:
                  break;
               case 4:
                  this.current.text = var8.getText();
               }
            }

            var8.close();
            return;
         }
      } catch (XMLStreamException var6) {
         var6.printStackTrace();
      } catch (IOException var7) {
         throw new ResourceException(var1, var7);
      }
   }

   public XMLOgreContainer getRoot() {
      return this.root;
   }

   public void setRoot(XMLOgreContainer var1) {
      this.root = var1;
   }
}
