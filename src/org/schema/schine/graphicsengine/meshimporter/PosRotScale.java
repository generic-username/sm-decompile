package org.schema.schine.graphicsengine.meshimporter;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

public class PosRotScale {
   public Vector3f[] scale;
   public Vector4f[] rot;
   public Vector3f[] pos;

   public PosRotScale() {
   }

   public PosRotScale(int var1) {
      this.pos = new Vector3f[var1];
      this.rot = new Vector4f[var1];
      this.scale = new Vector3f[var1];
   }
}
