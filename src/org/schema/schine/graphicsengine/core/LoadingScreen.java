package org.schema.schine.graphicsengine.core;

import java.io.IOException;

public abstract class LoadingScreen {
   public static String serverMessage = "";

   public abstract void drawLoadingScreen();

   public abstract void loadInitialResources() throws IOException, ResourceException;

   public abstract void update(Timer var1);

   public abstract void handleException(Exception var1);
}
