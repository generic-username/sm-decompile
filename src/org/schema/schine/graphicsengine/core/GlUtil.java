package org.schema.schine.graphicsengine.core;

import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import javax.imageio.ImageIO;
import javax.vecmath.Color4f;
import javax.vecmath.Matrix3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.EXTGpuShader4;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL40;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3D;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector4D;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.util.GifEncoder;
import org.schema.schine.input.Mouse;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.FileExt;

public class GlUtil {
   public static final IntOpenHashSet locked = new IntOpenHashSet();
   static final float PI_OVER_360 = 0.008726646F;
   public static FloatBuffer pos = BufferUtils.createFloatBuffer(3);
   public static FloatBuffer projBuffer = BufferUtils.createFloatBuffer(16);
   public static FloatBuffer modelBuffer = BufferUtils.createFloatBuffer(16);
   public static FloatBuffer pModelBuffer = BufferUtils.createFloatBuffer(16);
   public static FloatBuffer mvpBuffer = BufferUtils.createFloatBuffer(16);
   public static FloatBuffer normalBuffer = BufferUtils.createFloatBuffer(16);
   public static IntBuffer nameBuffer = BufferUtils.createIntBuffer(1);
   public static Matrix4f tmpMat = new Matrix4f();
   public static int matrixMode;
   public static Shader loadedShader;
   public static boolean LOCKDYN;
   public static int currentBoundBuffer;
   public static int currentBoundBufferTarget;
   public static boolean colorMask = true;
   static Matrix3f vecMathMat3fTmp = new Matrix3f();
   static float[] tmpMatrix4 = new float[16];
   static FloatBuffer tmpFloatBuffer = BufferUtils.createFloatBuffer(16);
   static Vector3f minBB = new Vector3f();
   static Vector3f maxBB = new Vector3f();
   static org.lwjgl.util.vector.Vector3f transformHelper = new org.lwjgl.util.vector.Vector3f(0.0F, 0.0F, 0.0F);
   static Transform t = new Transform();
   static FloatBuffer buff = BufferUtils.createFloatBuffer(16);
   private static Vector3f xFac = new Vector3f();
   private static Vector3f yFac = new Vector3f();
   private static Vector3f zFac = new Vector3f();
   private static Transform lookT = new Transform();
   private static IntBuffer iBuff = BufferUtils.createIntBuffer(1);
   private static ByteBuffer[] dynamicByteBuffer = new ByteBuffer[256];
   private static Vector3f aTemp = new Vector3f();
   private static Vector3f bTemp = new Vector3f();
   private static Vector3f cTemp = new Vector3f();
   private static Vector3f dTemp = new Vector3f();
   private static Vector4f a4Temp = new Vector4f();
   private static Vector4f b4Temp = new Vector4f();
   private static Vector4f c4Temp = new Vector4f();
   private static Vector4f d4Temp = new Vector4f();
   private static Vector3f eTemp = new Vector3f();
   private static Vector3f fTemp = new Vector3f();
   private static Vector3f gTemp = new Vector3f();
   private static Vector3f hTemp = new Vector3f();
   private static Vector3f minBBOut = new Vector3f();
   private static Vector3f maxBBOut = new Vector3f();
   private static Vector4f posTmp = new Vector4f();
   private static Vector4f posTmp2 = new Vector4f();
   private static Vector3f camPosTest = new Vector3f();
   private static Vector3f look = new Vector3f();
   private static Vector3f up = new Vector3f();
   private static Vector3f right = new Vector3f();
   private static FloatBuffer[] modelstack = new FloatBuffer[64];
   private static int modelstackPointer = 0;
   private static Vector4f[] guiClipStack = new Vector4f[64];
   private static Matrix4f[] guiClipStackTransformsM = new Matrix4f[64];
   private static Matrix4f[] guiClipStackTransformsP = new Matrix4f[64];
   private static int guiClipStackPointer = -1;
   private static FloatBuffer[] projectstack = new FloatBuffer[64];
   private static int projectstackPointer = 0;
   private static float[] floatArrayBuffer = new float[16];
   private static Vector4f currentColor = new Vector4f();
   private static DoubleBuffer leftL = BufferUtils.createDoubleBuffer(4);
   private static DoubleBuffer rightL = BufferUtils.createDoubleBuffer(4);
   private static DoubleBuffer topL = BufferUtils.createDoubleBuffer(4);
   private static DoubleBuffer bottomL = BufferUtils.createDoubleBuffer(4);
   private static boolean blended = false;
   private static int currentMode;
   public static float scrollX;
   public static float scrollY;
   private static boolean startedTextureCaching;
   public static final Vector3f[] DIRECTIONSf;
   public static final Vector3f[] TANGENTSf;
   public static final Vector3f[] BIDIRECTIONSf;
   private static Int2IntOpenHashMap textureCache;
   private static int textureCachePointer;
   private static final IntOpenHashSet enabledGLCaps;
   private static final IntOpenHashSet enabledGLStates;
   static int red;
   private static int activeTexture;

   public static Transform billboardAbitraryAxis(Vector3f var0, Vector3f var1, Vector3f var2, Transform var3) {
      look.sub(var2, var0);
      FastMath.normalizeCarmack(look);
      up.set(var1);
      right.cross(up, look);
      FastMath.normalizeCarmack(right);
      look.cross(right, up);
      setUpVector(up, var3);
      setForwardVector(look, var3);
      setRightVector(right, var3);
      var3.origin.set(var0);
      return var3;
   }

   public static void drawSphere(float var0, float var1) {
      for(float var5 = 0.0F; var5 < 3.1415927F; var5 += 3.1415927F / var1) {
         GL11.glBegin(5);

         for(float var6 = 0.0F; (double)var6 < 6.314601409435271D; var6 += 3.1415927F / var1) {
            float var2 = var0 * FastMath.cos(var6) * FastMath.sin(var5);
            float var3 = var0 * FastMath.sin(var6) * FastMath.sin(var5);
            float var4 = var0 * FastMath.cos(var5);
            GL11.glVertex3f(var2, var3, var4);
            var2 = var0 * FastMath.cos(var6) * FastMath.sin(var5 + 3.1415927F / var1);
            var3 = var0 * FastMath.sin(var6) * FastMath.sin(var5 + 3.1415927F / var1);
            var4 = var0 * FastMath.cos(var5 + 3.1415927F / var1);
            GL11.glVertex3f(var2, var3, var4);
         }

         GL11.glEnd();
      }

   }

   public static void buildAxisAlignedBBMatrix(float var0, float var1, float var2, float[] var3) {
      float var4 = -180.0F * FastMath.atan2(var3[8], var3[10]) / 3.1415927F;
      float var5 = var0 * var0 + var1 * var1 + var2 * var2;
      float var6 = FastMath.cos(0.017453292F * var4);
      var4 = FastMath.sin(0.017453292F * var4);
      if (var5 > 0.0F) {
         var5 = 1.0F / var5;
         var0 *= var5;
         var1 *= var5;
         var2 *= var5;
      }

      var3[0] = 1.0F;
      var3[1] = 0.0F;
      var3[2] = 0.0F;
      var3[3] = 0.0F;
      var3[4] = 0.0F;
      var3[5] = 1.0F;
      var3[6] = 0.0F;
      var3[7] = 0.0F;
      var3[8] = 0.0F;
      var3[9] = 0.0F;
      var3[10] = 1.0F;
      var3[11] = 0.0F;
      var3[12] = 0.0F;
      var3[13] = 0.0F;
      var3[14] = 0.0F;
      var3[15] = 1.0F;
      var3[0] = var0 * var0 + var6 * (1.0F - var0 * var0) + var4 * 0.0F;
      var3[4] = var0 * var1 + var6 * (0.0F - var0 * var1) + var4 * -var2;
      var3[8] = var0 * var2 + var6 * (0.0F - var0 * var2) + var4 * var1;
      var3[1] = var1 * var0 + var6 * (0.0F - var1 * var0) + var4 * var2;
      var3[5] = var1 * var1 + var6 * (1.0F - var1 * var1) + var4 * 0.0F;
      var3[9] = var1 * var2 + var6 * (0.0F - var1 * var2) + var4 * -var0;
      var3[2] = var2 * var0 + var6 * (0.0F - var2 * var0) + var4 * -var1;
      var3[6] = var2 * var1 + var6 * (0.0F - var2 * var1) + var4 * var0;
      var3[10] = var2 * var2 + var6 * (1.0F - var2 * var2) + var4 * 0.0F;
   }

   public static void buildAxisAlignedBBMatrix(float var0, float var1, float var2, Transform var3) {
      buildAxisAlignedBBMatrix(var0, var1, var2, floatArrayBuffer);
      var3.setFromOpenGLMatrix(floatArrayBuffer);
   }

   public static boolean checkAABB(Vector3f var0, Vector3f var1) {
      return var0.x <= var1.x && var0.y <= var1.y && var0.z <= var1.z;
   }

   public static void createStack() {
      int var0;
      for(var0 = 0; var0 < modelstack.length; ++var0) {
         modelstack[var0] = BufferUtils.createFloatBuffer(16);
      }

      for(var0 = 0; var0 < projectstack.length; ++var0) {
         projectstack[var0] = BufferUtils.createFloatBuffer(16);
      }

      for(var0 = 0; var0 < guiClipStack.length; ++var0) {
         guiClipStack[var0] = new Vector4f();
      }

   }

   public static Matrix4f createViewMatrix(float var0, float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8) {
      return createViewMatrix(new Vector3f(var0, var1, var2), new Vector3f(var3, var4, var5), new Vector3f(var6, var7, var8));
   }

   public static Matrix4f createViewMatrix(Vector3f var0, Vector3f var1, Vector3f var2) {
      Matrix4f var3 = new Matrix4f();
      Vector3f var4;
      (var4 = new Vector3f()).sub(var1, var0);
      var4.normalize();
      var2.normalize();
      (var1 = new Vector3f()).cross(var4, var2);
      var1.normalize();
      (var2 = new Vector3f()).cross(var1, var4);
      var2.normalize();
      var3.m00 = var1.x;
      var3.m10 = var1.y;
      var3.m20 = var1.z;
      var3.m30 = 0.0F;
      var3.m01 = var2.x;
      var3.m11 = var2.y;
      var3.m21 = var2.z;
      var3.m31 = 0.0F;
      var3.m02 = -var4.x;
      var3.m12 = -var4.y;
      var3.m22 = -var4.z;
      var3.m32 = 0.0F;
      var3.m03 = 0.0F;
      var3.m13 = 0.0F;
      var3.m23 = 0.0F;
      var3.m33 = 1.0F;
      var3.m30 = -var0.x;
      var3.m31 = -var0.y;
      var3.m32 = -var0.z;
      var3.transpose();
      return var3;
   }

   public static Matrix4f createOrthogonalProjectionMatrix(float var0, float var1, float var2, float var3, float var4, float var5) {
      Matrix4f var6 = new Matrix4f();
      float var7 = var1 - var0;
      float var8 = var2 - var3;
      float var9 = var5 - var4;
      var0 = -(var1 + var0) / var7;
      var1 = -(var2 + var3) / var8;
      var2 = -(var5 + var4) / var9;
      var6.m00 = 2.0F / var7;
      var6.m10 = 0.0F;
      var6.m20 = 0.0F;
      var6.m30 = var0;
      var6.m01 = 0.0F;
      var6.m11 = 2.0F / var8;
      var6.m21 = 0.0F;
      var6.m31 = var1;
      var6.m02 = 0.0F;
      var6.m12 = 0.0F;
      var6.m22 = -2.0F / var9;
      var6.m32 = var2;
      var6.m03 = 0.0F;
      var6.m13 = 0.0F;
      var6.m23 = 0.0F;
      var6.m33 = 1.0F;
      var6.transpose();
      return var6;
   }

   public static Matrix4f createPerspectiveProjectionMatrix(float var0, float var1, float var2, float var3) {
      Matrix4f var4 = new Matrix4f();
      var0 = 1.0F / (float)Math.tan((double)(var0 * 0.5F));
      var4.m00 = var0 / var1;
      var4.m10 = 0.0F;
      var4.m20 = 0.0F;
      var4.m30 = 0.0F;
      var4.m01 = 0.0F;
      var4.m11 = var0;
      var4.m21 = 0.0F;
      var4.m31 = 0.0F;
      var4.m02 = 0.0F;
      var4.m12 = 0.0F;
      var4.m22 = (var3 + var2) / (var2 - var3);
      var4.m32 = var3 * 2.0F * var2 / (var2 - var3);
      var4.m03 = 0.0F;
      var4.m13 = 0.0F;
      var4.m23 = -1.0F;
      var4.m33 = 0.0F;
      var4.transpose();
      return var4;
   }

   public static void displayMemory(String var0) {
      if (GraphicsContext.getCapabilities().GL_NVX_gpu_memory_info) {
         System.err.println("[GL_MEMORY] " + var0);
         int var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX());
         System.err.println("CURRENT_AVAILABLE: " + var1 / 1024 + "MB");
         var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX());
         System.err.println("TOTAL_AVAILABLE: " + var1 / 1024 + "MB");
         var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX());
         System.err.println("INFO_DEDICATED: " + var1 / 1024 + "MB");
         var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_EVICTED_MEMORY_NVX());
         System.err.println("INFO_EVICTED: " + var1 / 1024 + "MB");
      }

   }

   public static void destroyDirectByteBuffer(Buffer var0) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, SecurityException, NoSuchMethodException {
      if (!var0.getClass().getName().equals("java.nio.DirectByteBuffer")) {
         try {
            Field var1;
            (var1 = var0.getClass().getDeclaredField("att")).setAccessible(true);
            var0 = (Buffer)var1.get(var0);
         } catch (NoSuchFieldException var2) {
            var2.printStackTrace();
         }
      }

      Method var4;
      (var4 = var0.getClass().getMethod("cleaner")).setAccessible(true);
      Method var3;
      Object var5;
      (var3 = (var5 = var4.invoke(var0)).getClass().getMethod("clean")).setAccessible(true);
      var3.invoke(var5);
   }

   public static Quat4f eulerToQuaternion(Vector3f var0, Quat4f var1) {
      float var2 = FastMath.sin(var0.x);
      float var3 = FastMath.cos(var0.x);
      float var4 = FastMath.sin(var0.y);
      float var5 = FastMath.cos(var0.y);
      float var6 = FastMath.sin(var0.z);
      float var12;
      float var7 = (var12 = FastMath.cos(var0.z)) * var5;
      float var8 = var12 * var4 * var2 - var6 * var3;
      float var9 = var12 * var4 * var3 + var6 * var2;
      float var10 = var6 * var5;
      float var11 = var6 * var4 * var2 + var12 * var3;
      var12 = var6 * var4 * var3 - var12 * var2;
      var4 = -var4;
      var2 = var5 * var2;
      var3 = var5 * var3;
      vecMathMat3fTmp.m00 = var7;
      vecMathMat3fTmp.m01 = var8;
      vecMathMat3fTmp.m02 = var9;
      vecMathMat3fTmp.m10 = var10;
      vecMathMat3fTmp.m11 = var11;
      vecMathMat3fTmp.m12 = var12;
      vecMathMat3fTmp.m20 = var4;
      vecMathMat3fTmp.m21 = var2;
      vecMathMat3fTmp.m22 = var3;
      var1.set(vecMathMat3fTmp);
      return var1;
   }

   public static void fillBuffer(Transform var0, FloatBuffer var1) {
      var1.put(var0.basis.m00);
      var1.put(var0.basis.m10);
      var1.put(var0.basis.m20);
      var1.put(0.0F);
      var1.put(var0.basis.m01);
      var1.put(var0.basis.m11);
      var1.put(var0.basis.m21);
      var1.put(0.0F);
      var1.put(var0.basis.m02);
      var1.put(var0.basis.m12);
      var1.put(var0.basis.m22);
      var1.put(0.0F);
      var1.put(var0.origin.x);
      var1.put(var0.origin.y);
      var1.put(var0.origin.z);
      var1.put(1.0F);
   }

   public static Vector3f getBackVector(Vector3f var0, Transform var1) {
      var1.basis.getColumn(2, var0);
      var0.negate();
      return var0;
   }

   public static Vector3f getBottomVector(Vector3f var0, Transform var1) {
      getUpVector(var0, var1);
      var0.negate();
      return var0;
   }

   public static ByteBuffer getDynamicByteBuffer(int var0, int var1) {
      long var2 = System.currentTimeMillis();
      boolean var10000 = LOCKDYN;

      assert !locked.contains(var1) : var1 + "; " + locked;

      ByteBuffer var4 = dynamicByteBuffer[var1];
      boolean var5 = false;
      if (var4 == null || var4.capacity() < var0) {
         if (var4 != null) {
            try {
               destroyDirectByteBuffer(var4);
            } catch (IllegalArgumentException var8) {
               var8.printStackTrace();
            } catch (SecurityException var9) {
               var9.printStackTrace();
            } catch (IllegalAccessException var10) {
               var10.printStackTrace();
            } catch (InvocationTargetException var11) {
               var11.printStackTrace();
            } catch (NoSuchMethodException var12) {
               var12.printStackTrace();
            }

            System.gc();
            var5 = true;
         }

         var4 = BufferUtils.createByteBuffer(var0);
         dynamicByteBuffer[var1] = var4;
      }

      var4.limit(var0);

      assert var4.limit() == var0;

      var4.rewind();
      if (LOCKDYN) {
         locked.add(var1);
      }

      long var6;
      if ((var6 = System.currentTimeMillis() - var2) > 25L) {
         System.err.println("[UTIL] native buffer fetch took " + var6 + " ms; invoked GC: " + var5 + "; new size " + var0 + "; index: " + var1);
      }

      return var4;
   }

   public static Vector3f getForwardVector(Vector3f var0) {
      modelBuffer.rewind();
      Controller.modelviewMatrix.store(modelBuffer);
      var0.x = modelBuffer.get(2);
      var0.y = modelBuffer.get(6);
      var0.z = modelBuffer.get(10);
      return var0;
   }

   public static Vector3f getForwardVector(Vector3f var0, Transform var1) {
      var1.basis.getColumn(2, var0);
      return var0;
   }

   public static Vector3f getForwardVector(Vector3f var0, Matrix3f var1) {
      var1.getColumn(2, var0);
      return var0;
   }

   public static String getGlError() {
      int var0 = GL11.glGetError();
      String var1 = "unknown " + var0;
      switch(var0) {
      case 0:
         return null;
      case 1280:
         var1 = "GL_INVALID_ENUM";
         break;
      case 1281:
         var1 = "GL_INVALID_VALUE";
         break;
      case 1282:
         var1 = "GL_INVALID_OPERATION";
         break;
      case 1283:
         var1 = "GL_STACK_OVERFLOW: modelstack-sizes: proj: " + GL11.glGetInteger(2980) + ", model: " + GL11.glGetInteger(2979);
         break;
      case 1284:
         var1 = "GL_STACK_UNDERFLOW";
         break;
      case 1285:
         var1 = "GL_OUT_OF_MEMORY";
         break;
      case 1286:
         var1 = "GL_INVALID_FRAMEBUFFER_OPERATION";
      }

      return var1;
   }

   public static IntBuffer getIntBuffer1() {
      iBuff.rewind();
      return iBuff;
   }

   public static Vector3f getLeftVector(Vector3f var0, Transform var1) {
      getRightVector(var0, var1);
      var0.negate();
      return var0;
   }

   public static void getRibbon(Transform var0, float var1, Vector3f[] var2) {
      var2[0].set(-var1, -var1, 0.0F);
      var2[1].set(var1, -var1, 0.0F);
      var2[2].set(var1, var1, 0.0F);
      var2[3].set(-var1, var1, 0.0F);
      var0.transform(var2[0]);
      var0.transform(var2[1]);
      var0.transform(var2[2]);
      var0.transform(var2[3]);
   }

   public static Vector3f getRightVector(Vector3f var0) {
      modelBuffer.rewind();
      Controller.modelviewMatrix.store(modelBuffer);
      var0.x = modelBuffer.get(0);
      var0.y = modelBuffer.get(4);
      var0.z = modelBuffer.get(8);
      return var0;
   }

   public static Vector3f getRightVector(Vector3f var0, Transform var1) {
      var1.basis.getColumn(0, var0);
      return var0;
   }

   public static Vector3f getRightVector(Vector3f var0, Matrix3f var1) {
      var1.getColumn(0, var0);
      return var0;
   }

   public static Vector3f getUpVector(Vector3f var0) {
      modelBuffer.rewind();
      Controller.modelviewMatrix.store(modelBuffer);
      var0.x = modelBuffer.get(1);
      var0.y = modelBuffer.get(5);
      var0.z = modelBuffer.get(9);
      return var0;
   }

   public static Vector3f getUpVector(Vector3f var0, Transform var1) {
      var1.basis.getColumn(1, var0);
      return var0;
   }

   public static Vector3f getUpVector(Vector3f var0, Matrix3f var1) {
      var1.getColumn(1, var0);
      return var0;
   }

   public static void glColor4f(Vector4f var0) {
      glColor4f(var0.x, var0.y, var0.z, var0.w);
   }

   public static void glColor4f(org.lwjgl.util.vector.Vector4f var0) {
      glColor4f(var0.x, var0.y, var0.z, var0.w);
   }

   public static void glColor4f(Color4f var0) {
      glColor4f(var0.x, var0.y, var0.z, var0.w);
   }

   public static void glColor4f(float var0, float var1, float var2, float var3) {
      if (currentColor.x != var0 || currentColor.y != var1 || currentColor.z != var2 || currentColor.w != var3) {
         currentColor.set(var0, var1, var2, var3);
         GL11.glColor4f(var0, var1, var2, var3);
      }

   }

   public static void glColor4fForced(float var0, float var1, float var2, float var3) {
      currentColor.set(var0, var1, var2, var3);
      GL11.glColor4f(var0, var1, var2, var3);
   }

   public static void glLoadIdentity() {
      if (matrixMode == 5889) {
         Controller.projectionMatrix.setIdentity();
      } else {
         Controller.modelviewMatrix.setIdentity();
      }

      GL11.glLoadIdentity();
   }

   public static void glLoadMatrix(FloatBuffer var0) {
      var0.rewind();
      if (matrixMode == 5889) {
         Controller.projectionMatrix.load(var0);
      } else {
         Controller.modelviewMatrix.load(var0);
      }

      var0.rewind();
      GL11.glLoadMatrix(var0);
   }

   public static void glLoadMatrix(float[] var0) {
      buff.rewind();
      buff.put(var0);
      buff.rewind();
      if (matrixMode == 5889) {
         Controller.projectionMatrix.load(buff);
      } else {
         Controller.modelviewMatrix.load(buff);
      }

      buff.rewind();
      GL11.glLoadMatrix(buff);
   }

   public static void glLoadMatrix(Matrix4f var0) {
      buff.rewind();
      var0.store(buff);
      glLoadMatrix(buff);
   }

   public static void glLoadMatrix(Transform var0) {
      var0.getOpenGLMatrix(floatArrayBuffer);
      buff.rewind();
      buff.put(floatArrayBuffer);
      buff.rewind();
      glLoadMatrix(buff);
   }

   public static void glMatrixMode(int var0) {
      matrixMode = var0;
      GL11.glMatrixMode(var0);
   }

   public static void glMultMatrix(float[] var0) {
      buff.rewind();
      buff.put(var0);
      buff.rewind();
      tmpMat.load(buff);
      if (matrixMode == 5889) {
         Matrix4f.mul(Controller.projectionMatrix, tmpMat, Controller.projectionMatrix);
      } else {
         Matrix4f.mul(Controller.modelviewMatrix, tmpMat, Controller.modelviewMatrix);
      }

      buff.rewind();
      GL11.glMultMatrix(buff);
   }

   public static void glMultMatrix(FloatBuffer var0) {
      var0.rewind();
      tmpMat.load(var0);
      if (matrixMode == 5889) {
         Matrix4f.mul(Controller.projectionMatrix, tmpMat, Controller.projectionMatrix);
      } else {
         Matrix4f.mul(Controller.modelviewMatrix, tmpMat, Controller.modelviewMatrix);
      }

      var0.rewind();
      GL11.glMultMatrix(var0);
   }

   public static void glMultMatrix(Matrix4f var0) {
      buff.rewind();
      var0.store(buff);
      glMultMatrix(buff);
   }

   public static void glMultMatrix(Transform var0) {
      buff.rewind();
      fillBuffer(var0, buff);
      buff.rewind();
      glMultMatrix(buff);
   }

   public static final void glOrtho(float var0, float var1, float var2, float var3, float var4, float var5) {
      glOrtho(var0, var1, var2, var3, var4, var5, floatArrayBuffer);
      buff.rewind();
      buff.put(floatArrayBuffer);
      buff.rewind();
      if (matrixMode == 5889) {
         Controller.projectionMatrix.load(buff);
      } else if (matrixMode == 5888) {
         Controller.modelviewMatrix.load(buff);
      }

      buff.rewind();
      GL11.glLoadMatrix(buff);
   }

   public static final void glOrtho(float var0, float var1, float var2, float var3, float var4, float var5, float[] var6) {
      float var7 = 2.0F / (var1 - var0);
      float var8 = 2.0F / (var3 - var2);
      float var9 = -2.0F / (var5 - var4);
      var0 = -(var1 + var0) / (var1 - var0);
      var1 = -(var3 + var2) / (var3 - var2);
      var2 = -(var5 + var4) / (var5 - var4);
      var6[0] = var7;
      var6[1] = 0.0F;
      var6[2] = 0.0F;
      var6[3] = 0.0F;
      var6[4] = 0.0F;
      var6[5] = var8;
      var6[6] = 0.0F;
      var6[7] = 0.0F;
      var6[8] = 0.0F;
      var6[9] = 0.0F;
      var6[10] = var9;
      var6[11] = 0.0F;
      var6[12] = var0;
      var6[13] = var1;
      var6[14] = var2;
      var6[15] = 1.0F;
   }

   public static void glPopMatrix() {
      if (matrixMode == 5889) {
         glPopProjectionMatrix();
      } else {
         glPopModelviewMatrix();
      }
   }

   private static void glPopModelviewMatrix() {
      assert modelstackPointer > 0;

      --modelstackPointer;
      modelstack[modelstackPointer].rewind();
      Controller.modelviewMatrix.load(modelstack[modelstackPointer]);
      GL11.glPopMatrix();
   }

   private static void glPopProjectionMatrix() {
      assert projectstackPointer > 0;

      --projectstackPointer;
      projectstack[projectstackPointer].rewind();
      Controller.projectionMatrix.load(projectstack[projectstackPointer]);
      GL11.glPopMatrix();
   }

   public static void glPushMatrix() {
      if (matrixMode == 5889) {
         glPushProjectionMatrix();
      } else {
         glPushModelviewMatrix();
      }
   }

   private static void glPushModelviewMatrix() {
      modelstack[modelstackPointer].rewind();
      Controller.modelviewMatrix.store(modelstack[modelstackPointer]);
      ++modelstackPointer;

      assert modelstackPointer <= modelstack.length;

      GL11.glPushMatrix();
   }

   private static void glPushProjectionMatrix() {
      projectstack[projectstackPointer].rewind();
      Controller.projectionMatrix.store(projectstack[projectstackPointer]);
      ++projectstackPointer;

      assert projectstackPointer <= projectstack.length;

      GL11.glPushMatrix();
   }

   public static void gluOrtho2D(float var0, float var1, float var2, float var3) {
      glOrtho(var0, var1, var2, var3, -1.0F, 1.0F);
   }

   public static void gluPerspective(float var0, float var1, float var2, float var3) {
      var0 = 1.0F / FastMath.tan(var0 * 0.008726646F);
      float var4 = var2 - var3;
      floatArrayBuffer[0] = var0 / var1;
      floatArrayBuffer[1] = 0.0F;
      floatArrayBuffer[2] = 0.0F;
      floatArrayBuffer[3] = 0.0F;
      floatArrayBuffer[4] = 0.0F;
      floatArrayBuffer[5] = var0;
      floatArrayBuffer[6] = 0.0F;
      floatArrayBuffer[7] = 0.0F;
      floatArrayBuffer[8] = 0.0F;
      floatArrayBuffer[9] = 0.0F;
      floatArrayBuffer[10] = (var3 + var2) / var4;
      floatArrayBuffer[11] = -1.0F;
      floatArrayBuffer[12] = 0.0F;
      floatArrayBuffer[13] = 0.0F;
      floatArrayBuffer[14] = 2.0F * var2 * var3 / var4;
      floatArrayBuffer[15] = 0.0F;
      buff.rewind();
      buff.put(floatArrayBuffer);
      buff.rewind();
      Controller.projectionMatrix.load(buff);
      buff.rewind();
      GL11.glLoadMatrix(buff);
   }

   public static Matrix4f gluPerspective(Matrix4f var0, float var1, float var2, float var3, float var4, boolean var5) {
      var0.setZero();
      float var6;
      float var7 = (var1 = -(var6 = var3 * FastMath.tan(var1 / 360.0F * 3.1415927F))) / var2;
      var2 = var6 / var2;
      var0.m00 = var3 * 2.0F / (var6 - var1);
      var0.m11 = var3 * 2.0F / (var2 - var7);
      var0.m22 = -(var4 + var3) / (var4 - var3);
      var0.m20 = (var6 + var1) / (var6 - var1);
      var0.m21 = (var2 + var7) / (var2 - var7);
      var0.m23 = -1.0F;
      var0.m32 = -(var4 * 2.0F * var3) / (var4 - var3);
      if (var5) {
         projBuffer.rewind();
         var0.store(projBuffer);
         projBuffer.rewind();
         glLoadMatrix(projBuffer);
      }

      return var0;
   }

   public static int gluProject(float var0, float var1, float var2, float[] var3, float[] var4, int[] var5, float[] var6) {
      float[] var7;
      (var7 = new float[8])[0] = var3[0] * var0 + var3[4] * var1 + var3[8] * var2 + var3[12];
      var7[1] = var3[1] * var0 + var3[5] * var1 + var3[9] * var2 + var3[13];
      var7[2] = var3[2] * var0 + var3[6] * var1 + var3[10] * var2 + var3[14];
      var7[3] = var3[3] * var0 + var3[7] * var1 + var3[11] * var2 + var3[15];
      var7[4] = var4[0] * var7[0] + var4[4] * var7[1] + var4[8] * var7[2] + var4[12] * var7[3];
      var7[5] = var4[1] * var7[0] + var4[5] * var7[1] + var4[9] * var7[2] + var4[13] * var7[3];
      var7[6] = var4[2] * var7[0] + var4[6] * var7[1] + var4[10] * var7[2] + var4[14] * var7[3];
      var7[7] = -var7[2];
      if ((double)var7[7] == 0.0D) {
         return 0;
      } else {
         var7[7] = 1.0F / var7[7];
         var7[4] *= var7[7];
         var7[5] *= var7[7];
         var7[6] *= var7[7];
         var6[0] = (var7[4] * 0.5F + 0.5F) * (float)var5[2] + (float)var5[0];
         var6[1] = (var7[5] * 0.5F + 0.5F) * (float)var5[3] + (float)var5[1];
         var6[2] = (1.0F + var7[6]) * 0.5F;
         return 1;
      }
   }

   public static int gluProject(float var0, float var1, float var2, FloatBuffer var3, FloatBuffer var4, IntBuffer var5, FloatBuffer var6) {
      float[] var7;
      (var7 = new float[8])[0] = var3.get(0) * var0 + var3.get(4) * var1 + var3.get(8) * var2 + var3.get(12);
      var7[1] = var3.get(1) * var0 + var3.get(5) * var1 + var3.get(9) * var2 + var3.get(13);
      var7[2] = var3.get(2) * var0 + var3.get(6) * var1 + var3.get(10) * var2 + var3.get(14);
      var7[3] = var3.get(3) * var0 + var3.get(7) * var1 + var3.get(11) * var2 + var3.get(15);
      var7[4] = var4.get(0) * var7[0] + var4.get(4) * var7[1] + var4.get(8) * var7[2] + var4.get(12) * var7[3];
      var7[5] = var4.get(1) * var7[0] + var4.get(5) * var7[1] + var4.get(9) * var7[2] + var4.get(13) * var7[3];
      var7[6] = var4.get(2) * var7[0] + var4.get(6) * var7[1] + var4.get(10) * var7[2] + var4.get(14) * var7[3];
      var7[7] = -var7[2];
      if ((double)var7[7] == 0.0D) {
         return 0;
      } else {
         var7[7] = 1.0F / var7[7];
         var7[4] *= var7[7];
         var7[5] *= var7[7];
         var7[6] *= var7[7];
         var6.put(0, (var7[4] * 0.5F + 0.5F) * (float)var5.get(2) + (float)var5.get(0));
         var6.put(1, (var7[5] * 0.5F + 0.5F) * (float)var5.get(3) + (float)var5.get(1));
         var6.put(2, (1.0F + var7[6]) * 0.5F);
         return 1;
      }
   }

   private static void handleShaderUniformError(Shader var0, String var1) {
      if (EngineSettings.D_INFO_SHADER_ERRORS.isOn()) {
         String var2 = "[ERROR][SHADER] " + var0.getVertexshaderPath() + " - " + var1 + " HANDLE -1 ";
         if (!AbstractScene.infoList.contains(var2)) {
            AbstractScene.infoList.add(var2);
         }
      }

   }

   public static boolean isInViewFrustum(Transform var0, Mesh var1, float var2) {
      minBB.set(var1.getBoundingBox().min);
      maxBB.set(var1.getBoundingBox().max);
      AabbUtil2.transformAabb(minBB, maxBB, var2, var0, minBBOut, maxBBOut);
      return Controller.getCamera().isAABBInFrustum(minBBOut, maxBBOut);
   }

   public static boolean isLineInView(Vector3f var0, Vector3f var1, float var2) {
      return isPointInCamRange(var0, var2) && isPointInCamRange(var1, var2) && isLineinViewFrustum(var0, var1);
   }

   public static boolean isLineinViewFrustum(Vector3f var0, Vector3f var1) {
      maxBB.x = Math.min(var0.x, var1.x);
      maxBB.y = Math.min(var0.y, var1.y);
      maxBB.z = Math.min(var0.z, var1.z);
      minBB.x = Math.max(var0.x, var1.x);
      minBB.y = Math.max(var0.y, var1.y);
      minBB.z = Math.max(var0.z, var1.z);
      return Controller.getCamera().isAABBInFrustum(minBB, maxBB);
   }

   public static boolean isPointInCamRange(Vector3f var0, float var1) {
      camPosTest.sub(var0, Controller.getCamera().getPos());
      return camPosTest.length() <= var1;
   }

   public static boolean isPointInView(Vector3f var0, float var1) {
      return isPointInCamRange(var0, var1) && Controller.getCamera().isPointInFrustrum(var0);
   }

   public static Transform lookAtWithoutLoad(float var0, float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8) {
      zFac.x = var0 - var3;
      zFac.y = var1 - var4;
      zFac.z = var2 - var5;
      yFac.x = var6;
      yFac.y = var7;
      yFac.z = var8;
      zFac.normalize();
      xFac = Vector3fTools.crossProduct(yFac, zFac, xFac);
      yFac = Vector3fTools.crossProduct(zFac, xFac, yFac);
      xFac.normalize();
      yFac.normalize();
      lookT.basis.setRow(0, xFac);
      lookT.basis.setRow(1, yFac);
      lookT.basis.setRow(2, zFac);
      lookT.origin.set(-var0, -var1, -var2);
      lookT.basis.transform(lookT.origin);
      return lookT;
   }

   public static Transform lookAt(float var0, float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8) {
      zFac.x = var0 - var3;
      zFac.y = var1 - var4;
      zFac.z = var2 - var5;
      yFac.x = var6;
      yFac.y = var7;
      yFac.z = var8;
      zFac.normalize();
      xFac = Vector3fTools.crossProduct(yFac, zFac, xFac);
      yFac = Vector3fTools.crossProduct(zFac, xFac, yFac);
      xFac.normalize();
      yFac.normalize();
      lookT.basis.setRow(0, xFac);
      lookT.basis.setRow(1, yFac);
      lookT.basis.setRow(2, zFac);
      lookT.origin.set(-var0, -var1, -var2);
      lookT.basis.transform(lookT.origin);
      glLoadMatrix(lookT);
      return lookT;
   }

   public static void lwglMatrix4fToVecmathMatrix(Matrix4f var0, javax.vecmath.Matrix4f var1) {
      assert var0 != null : "LWGLMATRIX NULL";

      assert var1 != null : "VECMATHMATRIX NULL";

      tmpFloatBuffer.rewind();
      var0.store(tmpFloatBuffer);
      tmpFloatBuffer.rewind();
      tmpFloatBuffer.get(tmpMatrix4);
      var1.set(tmpMatrix4);
   }

   public static void main(String[] var0) {
   }

   public static Vector3f matrixToEuler(Matrix3f var0, Vector3f var1) {
      float var2;
      float var3;
      float var5;
      if ((var3 = FastMath.cos(var2 = FastMath.atan2(-var0.m20, FastMath.sqrt(var0.m00 * var0.m00 + var0.m10 * var0.m10)))) != 0.0F) {
         float var4 = FastMath.atan2(var0.m21 / var3, var0.m22 / var3);
         var5 = FastMath.atan2(var0.m10 / var3, var0.m00 / var3);
         var1.set(var4, var2, var5);
         return var1;
      } else {
         var5 = FastMath.atan2(var0.m01, var0.m11);
         if (var2 < 0.0F) {
            var5 = -var5;
         }

         var1.set(var5, var2, 0.0F);
         return var1;
      }
   }

   public static void printGlError() {
      String var0;
      if ((var0 = getGlError()) != null) {
         try {
            throw new GLException(var0);
         } catch (GLException var2) {
            System.err.println("JUST STACKTRACE (no exception thrown further): GL_ERROR: " + var0 + " ");
            var2.printStackTrace();
         }
      }

   }

   public static void printGlErrorCritical() {
      String var0;
      if ((var0 = getGlError()) != null) {
         try {
            throw new GLException(var0);
         } catch (GLException var2) {
            System.err.println("GL_ERROR: " + var0 + " ");
            var2.printStackTrace();
            GLFrame.processErrorDialogException((Exception)var2, (StateInterface)null);
         }
      }

   }

   public static void printGlErrorCritical(String var0) {
      String var1;
      if ((var1 = getGlError()) != null) {
         try {
            throw new GLException(var1 + ": " + var0);
         } catch (GLException var2) {
            System.err.println("GL_ERROR: " + var1 + " ");
            var2.printStackTrace();
            GLFrame.processErrorDialogException((Exception)var2, (StateInterface)null);
         }
      }

   }

   public static void project(Vector3f var0, Vector3f var1, Vector3f var2) {
      Vector3f var3 = new Vector3f(var0);
      (new Vector3f(var0)).normalize();
      var3.normalize();
      var3.scale(var1.dot(var3));
      var2.sub(var1, var3);
   }

   public static Vector3f projectOntoPlane(Vector3f var0, Vector3f var1, Vector3f var2) {
      double var3 = (double)var1.dot(var2);
      var0.set(var1);
      var0.x = (float)((double)var0.x - var3 * (double)var2.x);
      var0.y = (float)((double)var0.y - var3 * (double)var2.y);
      var0.z = (float)((double)var0.z - var3 * (double)var2.z);
      return var0;
   }

   public static void putOrthogonalBeam(FloatBuffer var0, Vector3f var1, Vector3f var2, Vector4f var3, Vector3f var4) {
      posTmp.set(var3);
      posTmp2.set(var4);
      aTemp.set(posTmp.x, posTmp.y, posTmp.z);
      aTemp.sub(var1);
      bTemp.set(posTmp.x, posTmp.y, posTmp.z);
      bTemp.add(var2);
      cTemp.set(posTmp.x, posTmp.y, posTmp.z);
      cTemp.add(var1);
      dTemp.set(posTmp.x, posTmp.y, posTmp.z);
      dTemp.sub(var2);
      a4Temp.set(aTemp.x, aTemp.y, aTemp.z, posTmp.w);
      b4Temp.set(bTemp.x, bTemp.y, bTemp.z, posTmp.w);
      c4Temp.set(cTemp.x, cTemp.y, cTemp.z, posTmp.w);
      d4Temp.set(dTemp.x, dTemp.y, dTemp.z, posTmp.w);
      putPoint4(var0, a4Temp);
      putPoint4(var0, b4Temp);
      putPoint4(var0, c4Temp);
      putPoint4(var0, d4Temp);
      eTemp.set(posTmp2.x, posTmp2.y, posTmp2.z);
      eTemp.sub(var1);
      fTemp.set(posTmp2.x, posTmp2.y, posTmp2.z);
      fTemp.add(var2);
      gTemp.set(posTmp2.x, posTmp2.y, posTmp2.z);
      gTemp.add(var1);
      hTemp.set(posTmp2.x, posTmp2.y, posTmp2.z);
      hTemp.sub(var2);
      a4Temp.set(eTemp.x, eTemp.y, eTemp.z, posTmp.w);
      b4Temp.set(fTemp.x, fTemp.y, fTemp.z, posTmp.w);
      c4Temp.set(gTemp.x, gTemp.y, gTemp.z, posTmp.w);
      d4Temp.set(hTemp.x, hTemp.y, hTemp.z, posTmp.w);
      putPoint4(var0, a4Temp);
      putPoint4(var0, b4Temp);
      putPoint4(var0, c4Temp);
      putPoint4(var0, d4Temp);
      a4Temp.set(bTemp.x, bTemp.y, bTemp.z, posTmp.w);
      b4Temp.set(cTemp.x, cTemp.y, cTemp.z, posTmp.w);
      c4Temp.set(hTemp.x, hTemp.y, hTemp.z, posTmp.w);
      d4Temp.set(eTemp.x, eTemp.y, eTemp.z, posTmp.w);
      putPoint4(var0, a4Temp);
      putPoint4(var0, b4Temp);
      putPoint4(var0, c4Temp);
      putPoint4(var0, d4Temp);
   }

   public static void putOrthogonalLineCross(FloatBuffer var0, Vector3f var1, Vector3f var2, Vector4f var3) {
      aTemp.set(var3.x, var3.y, var3.z);
      aTemp.sub(var2);
      dTemp.set(var3.x, var3.y, var3.z);
      dTemp.add(var2);
      bTemp.set(var3.x, var3.y, var3.z);
      bTemp.add(var1);
      cTemp.set(var3.x, var3.y, var3.z);
      cTemp.sub(var1);
      a4Temp.set(aTemp.x, aTemp.y, aTemp.z, var3.w);
      d4Temp.set(dTemp.x, dTemp.y, dTemp.z, var3.w);
      b4Temp.set(bTemp.x, bTemp.y, bTemp.z, var3.w);
      c4Temp.set(cTemp.x, cTemp.y, cTemp.z, var3.w);
      putPoint4(var0, a4Temp);
      putPoint4(var0, b4Temp);
      putPoint4(var0, d4Temp);
      putPoint4(var0, c4Temp);
   }

   public static void putOrthogonalLineHorizontal(FloatBuffer var0, Vector3f var1, Vector3f var2, Vector4f var3) {
      bTemp.set(var3.x, var3.y, var3.z);
      bTemp.add(var2);
      cTemp.set(var3.x, var3.y, var3.z);
      cTemp.add(var1);
      b4Temp.set(bTemp.x, bTemp.y, bTemp.z, var3.w);
      c4Temp.set(cTemp.x, cTemp.y, cTemp.z, var3.w);
      putPoint4(var0, b4Temp);
      putPoint4(var0, c4Temp);
   }

   public static void putOrthogonalLineVertical(FloatBuffer var0, Vector3f var1, Vector3f var2, Vector4f var3) {
      aTemp.set(var3.x, var3.y, var3.z);
      aTemp.sub(var1);
      dTemp.set(var3.x, var3.y, var3.z);
      dTemp.sub(var2);
      a4Temp.set(aTemp.x, aTemp.y, aTemp.z, var3.w);
      d4Temp.set(dTemp.x, dTemp.y, dTemp.z, var3.w);
      putPoint4(var0, a4Temp);
      putPoint4(var0, d4Temp);
   }

   public static void putOrthogonalQuad(FloatBuffer var0, Vector3f var1, Vector3f var2, Vector3f var3) {
      aTemp.set(var3);
      aTemp.sub(var1);
      bTemp.set(var3);
      bTemp.add(var2);
      cTemp.set(var3);
      cTemp.add(var1);
      dTemp.set(var3);
      dTemp.sub(var2);
      putPoint3(var0, aTemp);
      putPoint3(var0, bTemp);
      putPoint3(var0, cTemp);
      putPoint3(var0, dTemp);
   }

   public static void putBillboardQuad(FloatBuffer var0, Vector3f var1, Vector3f var2, float var3, Vector4f var4) {
      aTemp.set(var4.x, var4.y, var4.z);
      Vector3fTools.addScaled(aTemp, -var3 * 0.5F, var1);
      Vector3fTools.addScaled(aTemp, -var3 * 0.5F, var2);
      bTemp.set(var4.x, var4.y, var4.z);
      Vector3fTools.addScaled(bTemp, -var3 * 0.5F, var1);
      Vector3fTools.addScaled(bTemp, var3 * 0.5F, var2);
      cTemp.set(var4.x, var4.y, var4.z);
      Vector3fTools.addScaled(cTemp, var3 * 0.5F, var1);
      Vector3fTools.addScaled(cTemp, var3 * 0.5F, var2);
      dTemp.set(var4.x, var4.y, var4.z);
      Vector3fTools.addScaled(dTemp, var3 * 0.5F, var1);
      Vector3fTools.addScaled(dTemp, -var3 * 0.5F, var2);
      a4Temp.set(aTemp.x, aTemp.y, aTemp.z, var4.w);
      b4Temp.set(bTemp.x, bTemp.y, bTemp.z, var4.w);
      c4Temp.set(cTemp.x, cTemp.y, cTemp.z, var4.w);
      d4Temp.set(dTemp.x, dTemp.y, dTemp.z, var4.w);
      putPoint4(var0, a4Temp);
      putPoint4(var0, b4Temp);
      putPoint4(var0, c4Temp);
      putPoint4(var0, d4Temp);
   }

   public static void putOrthogonalQuad(FloatBuffer var0, Vector3f var1, Vector3f var2, Vector4f var3) {
      aTemp.set(var3.x, var3.y, var3.z);
      aTemp.sub(var1);
      bTemp.set(var3.x, var3.y, var3.z);
      bTemp.add(var2);
      cTemp.set(var3.x, var3.y, var3.z);
      cTemp.add(var1);
      dTemp.set(var3.x, var3.y, var3.z);
      dTemp.sub(var2);
      a4Temp.set(aTemp.x, aTemp.y, aTemp.z, var3.w);
      b4Temp.set(bTemp.x, bTemp.y, bTemp.z, var3.w);
      c4Temp.set(cTemp.x, cTemp.y, cTemp.z, var3.w);
      d4Temp.set(dTemp.x, dTemp.y, dTemp.z, var3.w);
      putPoint4(var0, a4Temp);
      putPoint4(var0, b4Temp);
      putPoint4(var0, c4Temp);
      putPoint4(var0, d4Temp);
   }

   public static void putPoint3(FloatBuffer var0, float var1, float var2, float var3) {
      var0.put(var1);
      var0.put(var2);
      var0.put(var3);
   }

   public static void putPoint3(FloatBuffer var0, Vector3f var1) {
      var0.put(var1.x);
      var0.put(var1.y);
      var0.put(var1.z);
   }

   public static void putPoint4(FloatBuffer var0, Vector4f var1) {
      var0.put(var1.x);
      var0.put(var1.y);
      var0.put(var1.z);
      var0.put(var1.w);
   }

   public static void putPoint4(FloatBuffer var0, Vector3f var1, float var2) {
      var0.put(var1.x);
      var0.put(var1.y);
      var0.put(var1.z);
      var0.put(var2);
   }

   public static void putPoint4(FloatBuffer var0, float var1, float var2, float var3, float var4) {
      var0.put(var1);
      var0.put(var2);
      var0.put(var3);
      var0.put(var4);
   }

   public static void putPoint4(int var0, FloatBuffer var1, float var2, float var3, float var4, float var5) {
      var1.put(var0, var2);
      var1.put(var0 + 1, var3);
      var1.put(var0 + 2, var4);
      var1.put(var0 + 3, var5);
   }

   public static void putRibbon(FloatBuffer var0, Vector4f var1, Transform var2, float var3, Vector3f[] var4) {
      var4[0].set(-var3, -var3, 0.0F);
      var4[1].set(var3, -var3, 0.0F);
      var4[2].set(var3, var3, 0.0F);
      var4[3].set(-var3, var3, 0.0F);
      var2.transform(var4[0]);
      var2.transform(var4[1]);
      var2.transform(var4[2]);
      var2.transform(var4[3]);
      putPoint4(var0, var4[0].x, var4[0].y, var4[0].z, var1.w);
      putPoint4(var0, var4[1].x, var4[1].y, var4[1].z, var1.w);
      putPoint4(var0, var4[2].x, var4[2].y, var4[2].z, var1.w);
      putPoint4(var0, var4[3].x, var4[3].y, var4[3].z, var1.w);
   }

   public static void putRibbon2(FloatBuffer var0, float var1, Vector3f[] var2, int var3) {
      if (var3 < 0) {
         putPoint4(var0, var2[3].x, var2[3].y, var2[3].z, var1);
         putPoint4(var0, var2[3].x, var2[3].y, var2[3].z, var1);
      } else {
         putPoint4(var3, var0, var2[2].x, var2[2].y, var2[2].z, var1);
         putPoint4(var3 + 4, var0, var2[3].x, var2[3].y, var2[3].z, var1);
      }
   }

   public static void putRibbon4(FloatBuffer var0, float var1, Vector3f[] var2) {
      a4Temp.set(var2[0].x, var2[0].y, var2[0].z, var1);
      b4Temp.set(var2[1].x, var2[1].y, var2[1].z, var1);
      c4Temp.set(var2[2].x, var2[2].y, var2[2].z, var1);
      d4Temp.set(var2[3].x, var2[3].y, var2[3].z, var1);
      putPoint4(var0, a4Temp);
      putPoint4(var0, b4Temp);
      putPoint4(var0, c4Temp);
      putPoint4(var0, d4Temp);
   }

   public static Matrix4f retrieveModelviewMatrix(Matrix4f var0) {
      modelBuffer.rewind();
      GL11.glGetFloat(2982, modelBuffer);
      modelBuffer.rewind();
      if (var0 == null) {
         return null;
      } else {
         var0.load(modelBuffer);
         return var0;
      }
   }

   public static Matrix4f retrieveModelviewProjectionMatrix() {
      Matrix4f var0 = new Matrix4f();
      Matrix4f var1 = new Matrix4f();
      var0 = retrieveProjectionMatrix(var0);
      var1 = retrieveModelviewMatrix(var1);
      Matrix4f var2 = new Matrix4f();
      Matrix4f.mul(var0, var1, var2);
      var2.store(mvpBuffer);
      mvpBuffer.rewind();
      return var2;
   }

   public static org.lwjgl.util.vector.Matrix3f retrieveNormalMatrix(Matrix4f var0) {
      org.lwjgl.util.vector.Matrix3f var1;
      (var1 = new org.lwjgl.util.vector.Matrix3f()).m00 = var0.m00;
      var1.m10 = var0.m10;
      var1.m20 = var0.m20;
      var1.m01 = var0.m01;
      var1.m11 = var0.m11;
      var1.m21 = var0.m21;
      var1.m02 = var0.m02;
      var1.m12 = var0.m12;
      var1.m22 = var0.m22;
      var1.invert();
      var1.store(normalBuffer);
      normalBuffer.rewind();
      return var1;
   }

   public static Matrix4f retrieveProjectionMatrix(Matrix4f var0) {
      projBuffer.rewind();
      GL11.glGetFloat(2983, projBuffer);
      projBuffer.rewind();
      if (var0 == null) {
         return null;
      } else {
         var0.load(projBuffer);
         return var0;
      }
   }

   public static void rotateModelview(float var0, float var1, float var2, float var3) {
      transformHelper.set(var1, var2, var3);
      Controller.modelviewMatrix.rotate(var0, transformHelper);
      GL11.glRotatef(var0, var1, var2, var3);
   }

   public static void scaleModelview(float var0, float var1, float var2) {
      transformHelper.set(var0, var1, var2);
      Controller.modelviewMatrix.scale(transformHelper);
      GL11.glScalef(var0, var1, var2);
   }

   public static void setBottomVector(Vector3f var0, Transform var1) {
      var0.negate();
      setUpVector(var0, var1);
   }

   public static void setForwardVector(Vector3f var0, Transform var1) {
      var1.basis.setColumn(2, var0);
   }

   public static void setLeftVector(Vector3f var0, Transform var1) {
      var0.negate();
      setRightVector(var0, var1);
   }

   public static void setRightVector(Vector3f var0, Transform var1) {
      var1.basis.setColumn(0, var0);
   }

   public static void setUpVector(Vector3f var0, Transform var1) {
      var1.basis.setColumn(1, var0);
   }

   public static void setForwardVector(Vector3f var0, Matrix3f var1) {
      var1.setColumn(2, var0);
   }

   public static void setRightVector(Vector3f var0, Matrix3f var1) {
      var1.setColumn(0, var0);
   }

   public static void setUpVector(Vector3f var0, Matrix3f var1) {
      var1.setColumn(1, var0);
   }

   public static void switchSubroutine(Shader var0, String var1, int var2) {
      ByteBuffer var3;
      byte[] var5;
      (var3 = getDynamicByteBuffer((var5 = var1.getBytes()).length, 0)).put(var5);
      var3.rewind();
      int var4 = GL40.glGetSubroutineIndex(var0.getShaderprogram(), var2, var3);
      nameBuffer.put(0, var4);
      nameBuffer.rewind();
      GL40.glUniformSubroutinesu(var2, nameBuffer);
   }

   public static void glTranslatef(Vector3f var0) {
      translateModelview(var0);
   }

   public static void glTranslatef(float var0, float var1, float var2) {
      translateModelview(var0, var1, var2);
   }

   public static void translateModelview(float var0, float var1, float var2) {
      transformHelper.set(var0, var1, var2);
      Controller.modelviewMatrix.translate(transformHelper);
      GL11.glTranslatef(var0, var1, var2);
   }

   public static void translateModelview(Vector3f var0) {
      translateModelview(var0.x, var0.y, var0.z);
   }

   public static void updateShaderBoolean(Shader var0, String var1, boolean var2) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var3;
         if ((var3 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniform1f(var3, var2 ? 1.0F : 0.0F);
         }
      }
   }

   public static void updateShaderColor4f(Shader var0, String var1, Color4f var2) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var3;
         if ((var3 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniform4f(var3, var2.x, var2.y, var2.z, var2.w);
         }
      }
   }

   public static void updateShaderFloat(Shader var0, String var1, float var2) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var3;
         if ((var3 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniform1f(var3, var2);
         }
      }
   }

   public static void updateShaderFloats1(Shader var0, String var1, FloatBuffer var2) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var3;
         if ((var3 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniform1(var3, var2);
         }
      }
   }

   public static void updateShaderFloats2(Shader var0, String var1, FloatBuffer var2) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var3;
         if ((var3 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniform2(var3, var2);
         }
      }
   }

   public static void updateShaderFloats3(Shader var0, String var1, FloatBuffer var2) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var3;
         if ((var3 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniform3(var3, var2);
         }
      }
   }

   public static void updateShaderFloats4(Shader var0, String var1, FloatBuffer var2) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var3;
         if ((var3 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniform4(var3, var2);
         }
      }
   }

   public static void updateShaderInt(Shader var0, String var1, int var2) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var3;
         if ((var3 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniform1i(var3, var2);
         }
      }
   }

   public static void updateShaderMat3(Shader var0, String var1, FloatBuffer var2, boolean var3) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var4 = var0.getHandle(var1);

         assert var2.remaining() >= 9;

         if (var4 == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniformMatrix3(var4, var3, var2);
         }
      }
   }

   public static void updateShaderMat3From4(Shader var0, String var1, FloatBuffer var2, boolean var3) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         assert var2.remaining() == 16;

         normalBuffer.rewind();
         normalBuffer.put(var2.get(0));
         normalBuffer.put(var2.get(1));
         normalBuffer.put(var2.get(2));
         normalBuffer.put(var2.get(4));
         normalBuffer.put(var2.get(5));
         normalBuffer.put(var2.get(6));
         normalBuffer.put(var2.get(8));
         normalBuffer.put(var2.get(9));
         normalBuffer.put(var2.get(10));
         normalBuffer.rewind();
         int var4;
         if ((var4 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniformMatrix3(var4, var3, var2);
         }
      }
   }

   public static void updateShaderMat4(Shader var0, String var1, FloatBuffer var2, boolean var3) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var4 = var0.getHandle(var1);

         assert var2.remaining() == 16;

         if (var4 == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniformMatrix4(var4, var3, var2);
         }
      }
   }

   public static void updateShaderMat4Array(Shader var0, String var1, Collection var2, boolean var3) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         FloatBuffer var4;
         if ((var4 = var0.matrixBuffer) == null || var4.capacity() < var2.size() << 4) {
            var4 = BufferUtils.createFloatBuffer(var2.size() << 4);
            var0.matrixBuffer = var4;
         }

         var4.limit(var2.size() << 4);
         var4.rewind();
         Iterator var5 = var2.iterator();

         while(var5.hasNext()) {
            ((Transform)var5.next()).getOpenGLMatrix(floatArrayBuffer);
            var4.put(floatArrayBuffer);
         }

         var4.flip();
         int var6;
         if ((var6 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            var4.rewind();
            GL20.glUniformMatrix4(var6, var3, var4);
         }
      }
   }

   public static void updateShaderMat4Array(Shader var0, String var1, Matrix4f[] var2, boolean var3) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         FloatBuffer var4;
         if ((var4 = var0.matrixBuffer) == null || var4.capacity() < var2.length << 4) {
            var4 = BufferUtils.createFloatBuffer(var2.length << 4);
            var0.matrixBuffer = var4;
         }

         var4.limit(var2.length << 4);
         var4.rewind();
         int var5 = (var2 = var2).length;

         for(int var6 = 0; var6 < var5; ++var6) {
            var2[var6].store(var4);
         }

         var4.flip();
         int var7;
         if ((var7 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniformMatrix4(var7, var3, var4);
         }
      }
   }

   public static void updateShaderMat4Array(Shader var0, String var1, Transform[] var2, boolean var3) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         FloatBuffer var4;
         if ((var4 = var0.matrixBuffer) == null || var4.capacity() < var2.length << 4) {
            var4 = BufferUtils.createFloatBuffer(var2.length << 4);
            var0.matrixBuffer = var4;
         }

         var4.limit(var2.length << 4);
         var4.rewind();
         int var5 = (var2 = var2).length;

         for(int var6 = 0; var6 < var5; ++var6) {
            var2[var6].getOpenGLMatrix(floatArrayBuffer);
            var4.put(floatArrayBuffer);
         }

         var4.flip();
         int var7;
         if ((var7 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniformMatrix4(var7, var3, var4);
         }
      }
   }

   public static void updateShaderTexture2D(Shader var0, String var1, int var2, int var3) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         glActiveTexture(var3 + '蓀');
         glBindTexture(3553, var2);
         updateShaderInt(var0, var1, var3);
         glActiveTexture(33984);
      }
   }

   public static void updateShaderVector2f(Shader var0, String var1, float var2, float var3) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var4;
         if ((var4 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniform2f(var4, var2, var3);
         }
      }
   }

   public static void updateShaderVector3D(Shader var0, String var1, Vector3D var2, float var3) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var4;
         if ((var4 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniform4f(var4, var2.getX(), var2.getY(), var2.getZ(), var3);
         }
      }
   }

   public static void updateShaderVector3D(Shader var0, String var1, Vector3f var2, float var3) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var4;
         if ((var4 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniform4f(var4, var2.x, var2.y, var2.z, var3);
         }
      }
   }

   public static void updateShaderVector3f(Shader var0, String var1, float var2, float var3, float var4) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var5;
         if ((var5 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniform3f(var5, var2, var3, var4);
         }
      }
   }

   public static void updateShaderVector3f(Shader var0, String var1, Vector3f var2) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var3;
         if ((var3 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniform3f(var3, var2.x, var2.y, var2.z);
         }
      }
   }

   public static void updateShaderFloatArray(Shader var0, String var1, float... var2) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var3;
         if ((var3 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            ByteBuffer var4 = getDynamicByteBuffer(var2.length << 2, 0);

            for(int var5 = 0; var5 < var2.length; ++var5) {
               var4.putFloat(var2[var5]);
            }

            var4.flip();
            GL20.glUniform1(var3, var4.asFloatBuffer());
         }
      }
   }

   public static void updateShaderVector4D(Shader var0, String var1, Vector4D var2) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var3;
         if ((var3 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniform4f(var3, var2.x, var2.y, var2.z, var2.a);
         }
      }
   }

   public static void updateShaderVector4f(Shader var0, String var1, float var2, float var3, float var4, float var5) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var6;
         if ((var6 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniform4f(var6, var2, var3, var4, var5);
         }
      }
   }

   public static void updateShaderVector4f(Shader var0, String var1, Vector4f var2) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var3;
         if ((var3 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniform4f(var3, var2.x, var2.y, var2.z, var2.w);
         }
      }
   }

   public static void updateShaderVector2f(Shader var0, String var1, Vector2f var2) {
      if (var0 != loadedShader) {
         throw new IllegalArgumentException("Wrong shader loaded: " + loadedShader + "; should be " + var0);
      } else {
         int var3;
         if ((var3 = var0.getHandle(var1)) == -1) {
            handleShaderUniformError(var0, var1);
         } else {
            GL20.glUniform2f(var3, var2.x, var2.y);
         }
      }
   }

   public static void writeScreenToDisk(String var0, String var1, int var2, int var3, int var4, FrameBufferObjects var5) {
      printGlErrorCritical();
      ByteBuffer var6 = getDynamicByteBuffer(var2 * var3 * var4, 0);
      int var7 = (Integer)EngineSettings.G_MULTI_SAMPLE.getCurrentState();
      if (var5 == null) {
         assert false : "Must provide 1024x1024 frame buffer, or else stuff is getting cut off";

         System.err.println("FBO " + var5 + "; aa " + var7 + "; " + GL11.glGetInteger(35053) + "; " + GL11.glGetInteger(36010) + "; ");
      }

      printGlErrorCritical();
      GL11.glReadPixels(0, 0, var2, var3, 6408, 5121, var6);
      printGlErrorCritical();
      printGlErrorCritical();
      FileExt var14 = new FileExt(var0 + "." + var1);
      var1 = var1.toUpperCase(Locale.ENGLISH);
      BufferedImage var15 = new BufferedImage(var2, var3, 2);

      for(var7 = 0; var7 < var2; ++var7) {
         for(int var8 = 0; var8 < var3; ++var8) {
            int var9 = (var7 + var2 * var8) * var4;
            int var10 = var6.get(var9) & 255;
            int var11 = var6.get(var9 + 1) & 255;
            int var12 = var6.get(var9 + 2) & 255;
            var9 = var6.get(var9 + 3) & 255;
            var15.setRGB(var7, var3 - (var8 + 1), var9 << 24 | var10 << 16 | var11 << 8 | var12);
         }
      }

      try {
         var14.getParentFile().mkdirs();
         boolean var10000 = $assertionsDisabled;

         assert var1 != null;

         var10000 = $assertionsDisabled;
         ImageIO.write(var15, var1, var14);
      } catch (IOException var13) {
         var13.printStackTrace();
      }

      printGlErrorCritical();
   }

   public static void writeScreenToGif(GifEncoder var0, int var1, int var2, int var3, FrameBufferObjects var4) {
      var0.handleFrame(var1, var2, var3, var4);
   }

   public static boolean glBindBuffer(int var0, int var1) {
      if (var0 == currentBoundBufferTarget && currentBoundBuffer == var1) {
         return false;
      } else {
         GL15.glBindBuffer(var0, var1);
         currentBoundBufferTarget = var0;
         currentBoundBuffer = var1;
         return true;
      }
   }

   public static Transform setTrans(Vector3f var0, Vector3f var1, Vector3f var2, Transform var3) {
      var3.setIdentity();
      setRightVector(var0, var3);
      setUpVector(var1, var3);
      setForwardVector(var2, var3);
      return var3;
   }

   public static Vector4f getClip() {
      return guiClipStackPointer < 0 ? null : guiClipStack[guiClipStackPointer];
   }

   public static Matrix4f getClipProjection() {
      return guiClipStackPointer < 0 ? null : guiClipStackTransformsP[guiClipStackPointer];
   }

   public static Matrix4f getClipModelview() {
      return guiClipStackPointer < 0 ? null : guiClipStackTransformsM[guiClipStackPointer];
   }

   public static void pushClipSubtract(Vector4f var0) {
      if (guiClipStackPointer < 0) {
         pushClip(var0);
      } else {
         ++guiClipStackPointer;
         guiClipStack[guiClipStackPointer].set(var0);
         guiClipStackTransformsP[guiClipStackPointer].load(Controller.projectionMatrix);
         guiClipStackTransformsM[guiClipStackPointer].load(Controller.modelviewMatrix);
         float var1 = guiClipStackTransformsM[guiClipStackPointer - 1].m30;
         float var2 = guiClipStackTransformsM[guiClipStackPointer - 1].m31;
         float var3 = guiClipStackTransformsM[guiClipStackPointer].m30;
         float var4 = guiClipStackTransformsM[guiClipStackPointer].m31;
         var1 -= var3;
         var2 -= var4;
         var3 = Math.max(guiClipStack[guiClipStackPointer - 1].x, var0.x - var1);
         var4 = Math.min(guiClipStack[guiClipStackPointer - 1].y, var0.y - var1);
         float var5 = Math.max(guiClipStack[guiClipStackPointer - 1].z, var0.z - var2);
         float var6 = Math.min(guiClipStack[guiClipStackPointer - 1].w, var0.w - var2);
         var3 += var1;
         var4 += var1;
         var5 += var2;
         var6 += var2;
         put(leftL, 1.0D, 0.0D, 0.0D, (double)(-var3));
         put(rightL, -1.0D, 0.0D, 0.0D, (double)var4);
         put(topL, 0.0D, 1.0D, 0.0D, (double)(-var5));
         put(bottomL, 0.0D, -1.0D, 0.0D, (double)var6);
         GL11.glClipPlane(12288, leftL);
         glEnable(12288);
         GL11.glClipPlane(12289, rightL);
         glEnable(12289);
         GL11.glClipPlane(12290, topL);
         glEnable(12290);
         GL11.glClipPlane(12291, bottomL);
         glEnable(12291);
      }
   }

   public static void pushClip(Vector4f var0) {
      ++guiClipStackPointer;
      guiClipStack[guiClipStackPointer].set(var0);
      guiClipStackTransformsP[guiClipStackPointer].load(Controller.projectionMatrix);
      guiClipStackTransformsM[guiClipStackPointer].load(Controller.modelviewMatrix);
      put(leftL, 1.0D, 0.0D, 0.0D, (double)(-var0.x));
      put(rightL, -1.0D, 0.0D, 0.0D, (double)var0.y);
      put(topL, 0.0D, 1.0D, 0.0D, (double)(-var0.z));
      put(bottomL, 0.0D, -1.0D, 0.0D, (double)var0.w);
      GL11.glClipPlane(12288, leftL);
      glEnable(12288);
      GL11.glClipPlane(12289, rightL);
      glEnable(12289);
      GL11.glClipPlane(12290, topL);
      glEnable(12290);
      GL11.glClipPlane(12291, bottomL);
      glEnable(12291);
   }

   public static void pushClipLR(Vector4f var0) {
      put(leftL, 1.0D, 0.0D, 0.0D, (double)(-var0.x));
      put(rightL, -1.0D, 0.0D, 0.0D, (double)var0.y);
      GL11.glClipPlane(12292, leftL);
      glEnable(12292);
      GL11.glClipPlane(12293, rightL);
      glEnable(12293);
   }

   public static void popClipLR(Vector4f var0) {
      glDisable(12292);
      glDisable(12293);
   }

   public static void popClip() {
      if (--guiClipStackPointer < -1) {
         assert false;
      } else {
         if (guiClipStackPointer == -1) {
            glDisable(12288);
            glDisable(12289);
            glDisable(12290);
            glDisable(12291);
            return;
         }

         glMatrixMode(5889);
         glPushMatrix();
         glLoadMatrix(guiClipStackTransformsP[guiClipStackPointer]);
         glMatrixMode(5888);
         glPushMatrix();
         glLoadMatrix(guiClipStackTransformsM[guiClipStackPointer]);
         Vector4f var0 = guiClipStack[guiClipStackPointer];
         put(leftL, 1.0D, 0.0D, 0.0D, (double)(-var0.x));
         put(rightL, -1.0D, 0.0D, 0.0D, (double)var0.y);
         put(topL, 0.0D, 1.0D, 0.0D, (double)(-var0.z));
         put(bottomL, 0.0D, -1.0D, 0.0D, (double)var0.w);
         GL11.glClipPlane(12288, leftL);
         glEnable(12288);
         GL11.glClipPlane(12289, rightL);
         glEnable(12289);
         GL11.glClipPlane(12290, topL);
         glEnable(12290);
         GL11.glClipPlane(12291, bottomL);
         glEnable(12291);
         glPopMatrix();
         glMatrixMode(5889);
         glPopMatrix();
         glMatrixMode(5888);
      }

   }

   private static void put(DoubleBuffer var0, double var1, double var3, double var5, double var7) {
      var0.put(var1);
      var0.put(var3);
      var0.put(var5);
      var0.put(var7);
      var0.rewind();
   }

   public static void enableBlend(boolean var0) {
      if (var0 != blended) {
         if (var0) {
            glEnable(3042);
         } else {
            glDisable(3042);
         }

         blended = var0;
      }

   }

   public static boolean isColorMask() {
      return colorMask;
   }

   public static void setColorMask(boolean var0) {
      GL11.glColorMask(var0, var0, var0, var0);
      colorMask = var0;
   }

   public static void glEnd() {
      if (currentMode == 0) {
         throw new RuntimeException("Double End");
      } else {
         GL11.glEnd();
         currentMode = 0;
      }
   }

   public static void glBegin(int var0) {
      if (currentMode != 0) {
         throw new RuntimeException("Double Begin");
      } else {
         GL11.glBegin(var0);
         currentMode = var0;
      }
   }

   public static void glVertexAttribIPointer(int var0, int var1, int var2, int var3, long var4) {
      if (GraphicsContext.forceOpenGl30()) {
         GL30.glVertexAttribIPointer(var0, var1, var2, var3, var4);
      } else {
         EXTGpuShader4.glVertexAttribIPointerEXT(var0, var1, var2, var3, var4);
      }
   }

   public static void updateShaderCubeNormalsBiNormalsAndTangentsBoolean(Shader var0) {
      FloatBuffer var1;
      (var1 = getDynamicByteBuffer(84, 0).asFloatBuffer()).clear();
      var1.put(0.0F);
      var1.put(0.0F);
      var1.put(0.0F);

      int var2;
      for(var2 = 0; var2 < 6; ++var2) {
         var1.put(DIRECTIONSf[var2].x);
         var1.put(DIRECTIONSf[var2].y);
         var1.put(DIRECTIONSf[var2].z);
      }

      var1.rewind();
      updateShaderFloats3(var0, "normals", var1);
      var1.clear();
      var1.put(0.0F);
      var1.put(0.0F);
      var1.put(0.0F);

      for(var2 = 0; var2 < 6; ++var2) {
         var1.put(BIDIRECTIONSf[var2].x);
         var1.put(BIDIRECTIONSf[var2].y);
         var1.put(BIDIRECTIONSf[var2].z);
      }

      var1.rewind();
      updateShaderFloats3(var0, "binormals", var1);
      var1.clear();
      var1.put(0.0F);
      var1.put(0.0F);
      var1.put(0.0F);

      for(var2 = 0; var2 < 6; ++var2) {
         var1.put(TANGENTSf[var2].x);
         var1.put(TANGENTSf[var2].y);
         var1.put(TANGENTSf[var2].z);
      }

      var1.rewind();
      updateShaderFloats3(var0, "tangents", var1);
   }

   public static void addScroll(float var0, float var1) {
      float var10000 = scrollY;
      scrollX += var0;
      scrollY += var1;
   }

   public static void subScroll(float var0, float var1) {
      scrollX -= var0;
      scrollY -= var1;
   }

   public static void startTextureChaching() {
      startedTextureCaching = true;
   }

   public static void loadTextureCached(int var0) {
      if (textureCache.containsKey(var0)) {
         glActiveTexture('蓀' + textureCache.get(var0));
      } else {
         glActiveTexture('蓀' + textureCachePointer);
         GL11.glTexParameteri(3553, 10242, 10497);
         GL11.glTexParameteri(3553, 10243, 10497);
         GL11.glTexParameteri(3553, 32882, 10497);
         GL11.glBindTexture(3553, var0);
         textureCache.put(var0, textureCachePointer);
         if ((textureCachePointer = (textureCachePointer + 1) % GraphicsContext.MAX_TEXURE_UNITS) == 0) {
            ++textureCachePointer;
         }

      }
   }

   public static boolean isTextureChaching() {
      return startedTextureCaching;
   }

   public static void stopTextureChaching() {
      if (startedTextureCaching) {
         Iterator var0 = textureCache.values().iterator();

         while(var0.hasNext()) {
            glActiveTexture((Integer)var0.next() + '蓀');
            GL11.glBindTexture(3553, 0);
         }

         glActiveTexture(33984);
         textureCachePointer = 1;
         textureCache.clear();
      }

      startedTextureCaching = false;
   }

   public static Vector3f getMatrix4fPosition(Matrix4f var0, Vector3f var1) {
      var1.set(var0.m30, var0.m31, var0.m32);
      return var1;
   }

   public static Vector3f getModelviewPosition(Vector3f var0) {
      return getMatrix4fPosition(Controller.modelviewMatrix, var0);
   }

   public static Vector3f getModelviewScaleX(Vector3f var0) {
      var0.set(Controller.modelviewMatrix.m00, Controller.modelviewMatrix.m01, Controller.modelviewMatrix.m02);
      return var0;
   }

   public static Vector3f getModelviewScaleY(Vector3f var0) {
      var0.set(Controller.modelviewMatrix.m10, Controller.modelviewMatrix.m11, Controller.modelviewMatrix.m12);
      return var0;
   }

   public static void glEnable(int var0) {
      if (enabledGLCaps.add(var0) || var0 == 3553 || var0 == 32879 || var0 == 35866 || var0 == 37120) {
         GL11.glEnable(var0);
      }

   }

   public static void glDisable(int var0) {
      if (enabledGLCaps.remove(var0) || var0 == 3553 || var0 == 32879 || var0 == 35866 || var0 == 37120) {
         GL11.glDisable(var0);
      }

   }

   public static void glEnableClientState(int var0) {
      if (enabledGLStates.add(var0)) {
         GL11.glEnableClientState(var0);
      }

   }

   public static void glDisableClientState(int var0) {
      if (enabledGLStates.remove(var0)) {
         GL11.glDisableClientState(var0);
      }

   }

   public static void glActiveTexture(int var0) {
      if (activeTexture != var0) {
         GL13.glActiveTexture(var0);
         activeTexture = var0;
      }

   }

   public static void glBindTexture(int var0, int var1) {
      GL11.glBindTexture(var0, var1);
   }

   public static void glDepthMask(boolean var0) {
      GL11.glDepthMask(var0);
   }

   public static void glBlendFunc(int var0, int var1) {
      GL11.glBlendFunc(var0, var1);
   }

   public static void glBlendFuncSeparate(int var0, int var1, int var2, int var3) {
      GL14.glBlendFuncSeparate(var0, var1, var2, var3);
   }

   public static float getRelMouseX() {
      float var0 = Vector3fTools.length(Controller.modelviewMatrix.m00, Controller.modelviewMatrix.m01, Controller.modelviewMatrix.m02);
      return ((float)Mouse.getX() - Controller.modelviewMatrix.m30) * var0;
   }

   public static float getRelMouseY() {
      float var0 = Vector3fTools.length(Controller.modelviewMatrix.m10, Controller.modelviewMatrix.m11, Controller.modelviewMatrix.m12);
      return ((float)(GLFrame.getHeight() - Mouse.getY()) - Controller.modelviewMatrix.m31) * var0;
   }

   public static void printGLState() {
   }

   static {
      dynamicByteBuffer[0] = BufferUtils.createByteBuffer(1310720);
      dynamicByteBuffer[1] = BufferUtils.createByteBuffer(1310720);
      dynamicByteBuffer[2] = BufferUtils.createByteBuffer(1310720);

      for(int var0 = 0; var0 < guiClipStackTransformsM.length; ++var0) {
         guiClipStackTransformsM[var0] = new Matrix4f();
         guiClipStackTransformsP[var0] = new Matrix4f();
      }

      DIRECTIONSf = new Vector3f[]{new Vector3f(0.0F, 0.0F, 1.0F), new Vector3f(0.0F, 0.0F, -1.0F), new Vector3f(0.0F, 1.0F, 0.0F), new Vector3f(0.0F, -1.0F, 0.0F), new Vector3f(1.0F, 0.0F, 0.0F), new Vector3f(-1.0F, 0.0F, 0.0F)};
      TANGENTSf = new Vector3f[]{new Vector3f(-1.0F, 0.0F, 0.0F), new Vector3f(1.0F, 0.0F, 0.0F), new Vector3f(1.0F, 0.0F, 0.0F), new Vector3f(1.0F, 0.0F, 0.0F), new Vector3f(0.0F, 0.0F, 1.0F), new Vector3f(0.0F, 0.0F, -1.0F)};
      BIDIRECTIONSf = new Vector3f[]{new Vector3f(0.0F, 1.0F, 0.0F), new Vector3f(0.0F, 1.0F, 0.0F), new Vector3f(0.0F, 0.0F, 1.0F), new Vector3f(0.0F, 0.0F, -1.0F), new Vector3f(0.0F, 1.0F, 0.0F), new Vector3f(0.0F, 1.0F, 0.0F)};
      textureCache = new Int2IntOpenHashMap();
      textureCachePointer = 1;
      enabledGLCaps = new IntOpenHashSet();
      enabledGLStates = new IntOpenHashSet();
      activeTexture = 0;
   }
}
