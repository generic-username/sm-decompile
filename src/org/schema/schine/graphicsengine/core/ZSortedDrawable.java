package org.schema.schine.graphicsengine.core;

import java.nio.FloatBuffer;
import javax.vecmath.Vector3f;

public interface ZSortedDrawable extends Comparable, Drawable {
   void drawZSorted();

   FloatBuffer getBufferedTransformation();

   Vector3f getBufferedTransformationPosition();
}
