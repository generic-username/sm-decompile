package org.schema.schine.graphicsengine.core;

public interface Drawable {
   void cleanUp();

   void draw();

   boolean isInvisible();

   void onInit();
}
