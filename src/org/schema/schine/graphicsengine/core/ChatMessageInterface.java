package org.schema.schine.graphicsengine.core;

import javax.vecmath.Vector4f;

public interface ChatMessageInterface {
   Vector4f getStartColor();
}
