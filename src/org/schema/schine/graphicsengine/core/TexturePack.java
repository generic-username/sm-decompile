package org.schema.schine.graphicsengine.core;

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import org.schema.common.util.data.DataUtil;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.FileExt;

public class TexturePack {
   public static final String path;
   public final String name;
   public final int[] resolutions;

   public TexturePack(String var1, int[] var2) {
      this.name = var1;
      this.resolutions = var2;
   }

   public static TexturePack[] createTexturePacks() {
      FileExt var0;
      String[] var1 = (var0 = new FileExt(path)).list();
      File[] var11 = var0.listFiles();
      int var2 = 0;

      for(int var3 = 0; var3 < var11.length; ++var3) {
         if (var11[var3].isDirectory()) {
            ++var2;
         } else {
            System.err.println("[TEXTURE-PACK] WARNING: ignoring " + var11[var3].getName() + ": not a directory");
         }
      }

      TexturePack[] var12 = new TexturePack[var2];
      var2 = 0;

      for(int var4 = 0; var4 < var11.length; ++var4) {
         if (var11[var4].isDirectory()) {
            File[] var5 = var11[var4].listFiles();
            ArrayList var6 = new ArrayList();
            int var7 = (var5 = var5).length;

            for(int var8 = 0; var8 < var7; ++var8) {
               File var9 = var5[var8];

               try {
                  if (var9.getName().equals("64") && var9.isDirectory() && check(var9)) {
                     var6.add(64);
                  }

                  if (var9.getName().equals("128") && var9.isDirectory() && check(var9)) {
                     var6.add(128);
                  }

                  if (var9.getName().equals("256") && var9.isDirectory() && check(var9)) {
                     var6.add(256);
                  }
               } catch (Exception var10) {
                  var10.printStackTrace();
                  GLFrame.processErrorDialogExceptionWithoutReport(var10, (StateInterface)null);
               }
            }

            int[] var13 = new int[var6.size()];

            for(var7 = 0; var7 < var13.length; ++var7) {
               var13[var7] = (Integer)var6.get(var7);
            }

            var12[var2] = new TexturePack(var1[var4], var13);
            ++var2;
         }
      }

      return var12;
   }

   public static Dimension getDimension(File var0) throws IOException {
      ImageInputStream var9 = ImageIO.createImageInputStream(var0);

      Dimension var2;
      try {
         Iterator var1;
         if (!(var1 = ImageIO.getImageReaders(var9)).hasNext()) {
            return new Dimension(-1, -1);
         }

         ImageReader var10 = (ImageReader)var1.next();

         try {
            var10.setInput(var9);
            var2 = new Dimension(var10.getWidth(var10.getMinIndex()), var10.getHeight(var10.getMinIndex()));
         } finally {
            var10.dispose();
         }
      } finally {
         if (var9 != null) {
            var9.close();
         }

      }

      return var2;
   }

   private static boolean contains(File[] var0, String[] var1, String var2) {
      for(int var3 = 0; var3 < var0.length; ++var3) {
         for(int var4 = 0; var4 < var1.length; ++var4) {
            String var5;
            if ((var5 = var2 + (var1[var4].startsWith(".") ? "" : ".") + var1[var4]).equals(var0[var3].getName()) || (var5 + ".zip").equals(var0[var3].getName())) {
               return true;
            }
         }
      }

      return false;
   }

   private static boolean check(File var0, String[] var1, File[] var2, String... var3) throws ResourceException, IOException {
      boolean var4 = true;
      int var5 = var0.getName().equals("256") ? 4096 : (var0.getName().equals("128") ? 2048 : 1024);
      new Dimension(var5, var5);

      for(var5 = 0; var5 < var3.length; ++var5) {
         boolean var6 = contains(var2, var1, var3[var5]);
         var4 = var4 && var6;
         if (!var6) {
            throw new ResourceException("The directory " + var0.getAbsolutePath() + " must contain " + var3[var5] + " to be a valid Texture pack");
         }
      }

      return var4;
   }

   private static boolean check(File var0) {
      try {
         return check(var0, new String[]{"png", "tga"}, var0.listFiles(), "overlays", "t000", "t001", "t002");
      } catch (Exception var1) {
         var1.printStackTrace();
         GLFrame.processErrorDialogExceptionWithoutReport(var1, (StateInterface)null);
         return false;
      }
   }

   public String getName() {
      return this.name;
   }

   public int[] getResolutions() {
      return this.resolutions;
   }

   public String toString() {
      return this.name;
   }

   static {
      path = DataUtil.dataPath + "textures" + File.separator + "block" + File.separator;
   }
}
