package org.schema.schine.graphicsengine.core.settings;

import java.util.Comparator;

public class EngineNameLengthComparator implements Comparator {
   public int compare(EngineSettings var1, EngineSettings var2) {
      if (var1.getDescription().length() < var2.getDescription().length()) {
         return -1;
      } else {
         return var1.getDescription().length() > var2.getDescription().length() ? 1 : 0;
      }
   }
}
