package org.schema.schine.graphicsengine.core.settings;

public interface NamedValueInterface {
   String getName();
}
