package org.schema.schine.graphicsengine.core.settings;

import java.util.Arrays;

public class StateParameterNotFoundException extends Exception {
   private static final long serialVersionUID = 1L;
   private final Object[] states;
   private final String arg;

   public StateParameterNotFoundException(String var1, Object[] var2) {
      this.arg = var1;
      this.states = var2;
   }

   public String getArg() {
      return this.arg;
   }

   public String getMessage() {
      return "parameter \"" + this.arg + "\" not recognized; possible parameters: " + this.states != null ? Arrays.toString(this.states) : "clazz state";
   }

   public Object[] getStates() {
      return this.states;
   }
}
