package org.schema.schine.graphicsengine.core.settings;

import org.schema.schine.graphicsengine.forms.gui.SettingsInterface;

public interface EngineSettingsChangeListener {
   void onSettingChanged(SettingsInterface var1);
}
