package org.schema.schine.graphicsengine.core.settings;

public enum ContextFilter {
   CRUCIAL,
   IMPORTANT,
   NORMAL,
   TRIVIAL;
}
