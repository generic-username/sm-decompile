package org.schema.schine.graphicsengine.core.settings.states;

import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.SettingsInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.input.InputState;
import org.schema.schine.resource.tag.Tag;

public abstract class States {
   public abstract boolean contains(Object var1);

   public abstract Object getFromString(String var1) throws StateParameterNotFoundException;

   public abstract String getType();

   public abstract Object next() throws StateParameterNotFoundException;

   public abstract Object previous() throws StateParameterNotFoundException;

   public abstract Tag toTag();

   public abstract Object readTag(Tag var1);

   public abstract Object getCurrentState();

   public abstract void setCurrentState(Object var1);

   public GUIElement getGUIElement(InputState var1, GUIElement var2, String var3, final SettingsInterface var4) {
      GUIActivatableTextBar var5;
      (var5 = new GUIActivatableTextBar(var1, FontLibrary.FontSize.MEDIUM, var3, var2, new TextCallback() {
         public void onTextEnter(String var1, boolean var2, boolean var3) {
         }

         public void onFailedTextCheck(String var1) {
         }

         public void newLine() {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public String[] getCommandPrefixes() {
            return null;
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            try {
               States.this.setCurrentState(States.this.getFromString(var1));
               var4.setCurrentState(States.this.getCurrentState());
            } catch (StateParameterNotFoundException var2) {
            }

            return var1;
         }
      }) {
         protected void onBecomingInactive() {
            this.setText(States.this.getCurrentState().toString());
         }
      }).setDeleteOnEnter(false);
      var5.setText(var4.getCurrentState().toString());
      return var5;
   }
}
