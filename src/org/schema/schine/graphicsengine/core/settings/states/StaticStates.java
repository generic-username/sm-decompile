package org.schema.schine.graphicsengine.core.settings.states;

import java.util.Arrays;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICheckBox;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.SettingsInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.InputState;
import org.schema.schine.resource.tag.Tag;

public class StaticStates extends States {
   public Object[] states;
   private int pointer;

   public StaticStates(Object... var1) {
      assert var1.length > 0;

      this.states = var1;
   }

   public boolean contains(Object var1) {
      for(int var2 = 0; var2 < this.states.length; ++var2) {
         if (var1.equals(this.states[var2])) {
            return true;
         }
      }

      return false;
   }

   public Object getFromString(String var1) throws StateParameterNotFoundException {
      for(int var2 = 0; var2 < this.states.length; ++var2) {
         if (var1.equals(this.states[var2].toString())) {
            this.pointer = var2;
            return this.states[this.pointer];
         }
      }

      throw new StateParameterNotFoundException(var1, this.states);
   }

   public String getType() {
      return this.states[0].getClass().getSimpleName();
   }

   public Object next() throws StateParameterNotFoundException {
      if (this.states.length <= 1) {
         return this.states[this.pointer];
      } else {
         this.pointer = FastMath.cyclicModulo(this.pointer + 1, this.states.length);
         return this.states[this.pointer];
      }
   }

   public Object previous() throws StateParameterNotFoundException {
      if (this.states.length <= 1) {
         return this.states[this.pointer];
      } else {
         this.pointer = FastMath.cyclicModulo(this.pointer - 1, this.states.length);
         return this.states[this.pointer];
      }
   }

   public Tag toTag() {
      return new Tag(Tag.Type.STRING, (String)null, this.states[this.pointer].toString());
   }

   public Object readTag(Tag var1) {
      try {
         return this.getFromString((String)var1.getValue());
      } catch (StateParameterNotFoundException var2) {
         var2.printStackTrace();
         return null;
      }
   }

   public Object getCurrentState() {
      return this.states[this.pointer];
   }

   public void setCurrentState(Object var1) {
      for(int var2 = 0; var2 < this.states.length; ++var2) {
         if (var1.equals(this.states[var2])) {
            this.pointer = var2;
            return;
         }
      }

   }

   public String toString() {
      return Arrays.toString(this.states) + "->[" + this.states[this.pointer] + "]";
   }

   public GUIElement getGUIElement(InputState var1, GUIElement var2, SettingsInterface var3) {
      return this.getGUIElement(var1, var2, "SETTING", var3);
   }

   public GUIElement getGUIElement(InputState var1, final GUIElement var2, String var3, final SettingsInterface var4) {
      if (this.getCurrentState() instanceof Boolean) {
         GUICheckBox var13;
         (var13 = new GUICheckBox(var1) {
            protected boolean isActivated() {
               return (Boolean)var4.getCurrentState();
            }

            protected void deactivate() throws StateParameterNotFoundException {
               StaticStates.this.setCurrentState(Boolean.FALSE);
               var4.setCurrentState(StaticStates.this.getCurrentState());
            }

            protected void activate() throws StateParameterNotFoundException {
               StaticStates.this.setCurrentState(Boolean.TRUE);
               var4.setCurrentState(StaticStates.this.getCurrentState());
            }
         }).activeInterface = new GUIActiveInterface() {
            public boolean isActive() {
               return var2.isActive();
            }
         };
         var13.setPos(4.0F, 4.0F, 0.0F);
         return var13;
      } else {
         int var14 = 0;
         GUIElement[] var5 = new GUIElement[this.states.length];
         int var6 = -1;
         Object[] var7;
         int var8 = (var7 = this.states).length;

         for(int var9 = 0; var9 < var8; ++var9) {
            final Object var10 = var7[var9];
            GUITextOverlay var11 = new GUITextOverlay(100, 24, FontLibrary.getBlenderProMedium16(), var1);
            new Vector3i();
            var11.setTextSimple(new Object() {
               public String toString() {
                  return var10.toString();
               }
            });
            if (var10 == var4.getCurrentState()) {
               var6 = var14;
            }

            GUIAncor var12 = new GUIAncor(var1, 100.0F, 24.0F);
            var11.getPos().x = 3.0F;
            var11.getPos().y = 3.0F;
            var12.attach(var11);
            var12.setUserPointer(var10);
            var5[var14] = var12;
            ++var14;
         }

         GUIDropDownList var15 = new GUIDropDownList(var1, 100, 24, 400, new DropDownCallback() {
            private boolean first = true;

            public void onSelectionChanged(GUIListElement var1) {
               if (this.first) {
                  this.first = false;
               } else {
                  Object var2 = var1.getContent().getUserPointer();
                  StaticStates.this.setCurrentState(var2);
                  var4.setCurrentState(StaticStates.this.getCurrentState());
               }
            }
         }, var5);
         if (var6 >= 0) {
            var15.setSelectedIndex(var6);
         }

         var15.dependend = var2;
         return var15;
      }
   }
}
