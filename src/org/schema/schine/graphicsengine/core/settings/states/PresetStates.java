package org.schema.schine.graphicsengine.core.settings.states;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.core.settings.presets.EngineSettingsPreset;
import org.schema.schine.graphicsengine.core.settings.presets.GraphicsPresetCompatibilityHigh;
import org.schema.schine.graphicsengine.core.settings.presets.GraphicsPresetCompatibilityLow;
import org.schema.schine.graphicsengine.core.settings.presets.GraphicsPresetCompatibilityMedium;
import org.schema.schine.graphicsengine.core.settings.presets.GraphicsPresetCustom;
import org.schema.schine.graphicsengine.core.settings.presets.GraphicsPresetHigh;
import org.schema.schine.graphicsengine.core.settings.presets.GraphicsPresetLow;
import org.schema.schine.graphicsengine.core.settings.presets.GraphicsPresetMedium;
import org.schema.schine.graphicsengine.core.settings.presets.GraphicsPresetUltra;
import org.schema.schine.graphicsengine.core.settings.presets.GraphicsPresetVeryHigh;
import org.schema.schine.graphicsengine.core.settings.presets.GraphicsPresetVeryLow;
import org.schema.schine.resource.tag.Tag;

public class PresetStates extends States {
   private static final List stateList = new ObjectArrayList();
   private static final Object2ObjectOpenHashMap stateMap = new Object2ObjectOpenHashMap();
   private EngineSettingsPreset currentState;

   public static void register() {
      stateList.add(new GraphicsPresetCompatibilityLow());
      stateList.add(new GraphicsPresetCompatibilityMedium());
      stateList.add(new GraphicsPresetCompatibilityHigh());
      stateList.add(new GraphicsPresetVeryLow());
      stateList.add(new GraphicsPresetLow());
      stateList.add(new GraphicsPresetMedium());
      stateList.add(new GraphicsPresetHigh());
      stateList.add(new GraphicsPresetVeryHigh());
      stateList.add(new GraphicsPresetUltra());
      stateList.add(new GraphicsPresetCustom());
      Iterator var0 = stateList.iterator();

      while(var0.hasNext()) {
         EngineSettingsPreset var1 = (EngineSettingsPreset)var0.next();
         stateMap.put(var1.getId(), var1);
      }

      EngineSettingsPreset.checkSanity(stateList);
   }

   public boolean contains(EngineSettingsPreset var1) {
      return var1 != null && stateMap.containsKey(var1.getId());
   }

   public EngineSettingsPreset getFromString(String var1) throws StateParameterNotFoundException {
      return get(var1);
   }

   public static EngineSettingsPreset get(String var0) {
      EngineSettingsPreset var1 = (EngineSettingsPreset)stateMap.get(var0.toUpperCase(Locale.ENGLISH));

      assert var1 != null : "PRESET " + var0 + " -> " + stateMap;

      return var1;
   }

   public String getType() {
      return "Preset";
   }

   public EngineSettingsPreset next() throws StateParameterNotFoundException {
      int var1 = stateList.indexOf(this.getCurrentState());
      EngineSettingsPreset var2;
      if ((var2 = (EngineSettingsPreset)stateList.get((var1 + 1) % stateList.size())) instanceof GraphicsPresetCustom) {
         var2 = (EngineSettingsPreset)stateList.get((var1 + 2) % stateList.size());
      }

      return var2;
   }

   public EngineSettingsPreset previous() throws StateParameterNotFoundException {
      int var1 = stateList.indexOf(this.getCurrentState());
      --var1;
      if (var1 < 0) {
         var1 = stateList.size() - 1;
      }

      EngineSettingsPreset var2;
      if ((var2 = (EngineSettingsPreset)stateList.get(var1)) instanceof GraphicsPresetCustom) {
         --var1;
         if (var1 < 0) {
            var1 = stateList.size() - 1;
         }

         var2 = (EngineSettingsPreset)stateList.get(var1);
      }

      return var2;
   }

   public Tag toTag() {
      return null;
   }

   public EngineSettingsPreset readTag(Tag var1) {
      return null;
   }

   public EngineSettingsPreset getCurrentState() {
      return this.currentState;
   }

   public void setCurrentState(EngineSettingsPreset var1) {
      assert var1 != null;

      this.currentState = var1;
   }

   public static EngineSettingsPreset getDefault() {
      String var0 = "GRAPHICS_MEDIUM";
      EngineSettingsPreset var1 = (EngineSettingsPreset)stateMap.get(var0);

      assert var1 != null : var0 + " -> " + stateMap;

      return var1;
   }

   static {
      register();
   }
}
