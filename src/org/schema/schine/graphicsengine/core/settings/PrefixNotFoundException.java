package org.schema.schine.graphicsengine.core.settings;

public class PrefixNotFoundException extends Exception {
   private static final long serialVersionUID = 1L;

   public PrefixNotFoundException(String var1) {
      super("ERROR: prefix not found: " + var1);
   }
}
