package org.schema.schine.graphicsengine.core.settings.presets;

import org.schema.schine.common.language.Lng;

public class GraphicsPresetCustom extends EngineSettingsPreset {
   public GraphicsPresetCustom() {
      super("GRAPHICS_CUSTOM");
   }

   public String getName() {
      return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_PRESETS_GRAPHICSPRESETCUSTOM_0;
   }

   public void init() {
   }
}
