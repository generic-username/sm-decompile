package org.schema.schine.graphicsengine.core.settings.presets;

import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class GraphicsPresetCompatibilityLow extends EngineSettingsPreset {
   public GraphicsPresetCompatibilityLow() {
      super("GRAPHICS_COMPATIBILITY_LOW");
   }

   public String getName() {
      return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_PRESETS_GRAPHICSPRESETCOMPATIBILITYLOW_0;
   }

   public void init() {
      this.addSetting(EngineSettings.F_FRAME_BUFFER, false);
      this.addSetting(EngineSettings.G_MULTI_SAMPLE, 0);
      this.addSetting(EngineSettings.G_DRAW_SURROUNDING_GALAXIES_IN_MAP, false);
      this.addSetting(EngineSettings.D_LIFETIME_NORM, 0);
      this.addSetting(EngineSettings.G_DEBRIS_THRESHOLD_SLOW_MS, 1);
      this.addSetting(EngineSettings.G_TEXTURE_PACK_RESOLUTION, 64);
      this.addSetting(EngineSettings.G_NORMAL_MAPPING, false);
      this.addSetting(EngineSettings.G_SHADOW_QUALITY, "BAREBONES");
      this.addSetting(EngineSettings.G_SHADOWS, false);
      this.addSetting(EngineSettings.G_PROD_BG, false);
      this.addSetting(EngineSettings.G_PROD_BG_QUALITY, 1024);
      this.addSetting(EngineSettings.F_BLOOM, false);
      this.addSetting(EngineSettings.G_STAR_COUNT, 512);
      this.addSetting(EngineSettings.G_DRAW_EXHAUST_PLUMES, false);
      this.addSetting(EngineSettings.G_USE_VERTEX_LIGHTING_ONLY, true);
      this.addSetting(EngineSettings.LIGHT_RAY_COUNT, 16);
      this.addSetting(EngineSettings.G_MAX_MISSILE_TRAILS, 8);
      this.addSetting(EngineSettings.G_MAX_SEGMENTSDRAWN, 250);
      this.addSetting(EngineSettings.G_MAX_BEAMS, 64);
      this.addSetting(EngineSettings.G_BASIC_SELECTION_BOX, true);
      this.addSetting(EngineSettings.LOD_DISTANCE_IN_THRESHOLD, 10.0F);
      this.addSetting(EngineSettings.CREATE_MANAGER_MESHES, false);
   }
}
