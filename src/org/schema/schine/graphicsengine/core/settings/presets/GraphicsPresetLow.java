package org.schema.schine.graphicsengine.core.settings.presets;

import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class GraphicsPresetLow extends EngineSettingsPreset {
   public GraphicsPresetLow() {
      super("GRAPHICS_LOW");
   }

   public String getName() {
      return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_PRESETS_GRAPHICSPRESETLOW_0;
   }

   public void init() {
      this.addSetting(EngineSettings.F_FRAME_BUFFER, true);
      this.addSetting(EngineSettings.G_MULTI_SAMPLE, 0);
      this.addSetting(EngineSettings.G_DRAW_SURROUNDING_GALAXIES_IN_MAP, false);
      this.addSetting(EngineSettings.D_LIFETIME_NORM, 0);
      this.addSetting(EngineSettings.G_DEBRIS_THRESHOLD_SLOW_MS, 1);
      this.addSetting(EngineSettings.G_TEXTURE_PACK_RESOLUTION, 128);
      this.addSetting(EngineSettings.G_NORMAL_MAPPING, false);
      this.addSetting(EngineSettings.G_SHADOW_QUALITY, "SIMPLE");
      this.addSetting(EngineSettings.G_SHADOWS, false);
      this.addSetting(EngineSettings.G_PROD_BG, true);
      this.addSetting(EngineSettings.G_PROD_BG_QUALITY, 1024);
      this.addSetting(EngineSettings.F_BLOOM, false);
      this.addSetting(EngineSettings.G_STAR_COUNT, 2048);
      this.addSetting(EngineSettings.G_DRAW_EXHAUST_PLUMES, true);
      this.addSetting(EngineSettings.G_USE_VERTEX_LIGHTING_ONLY, true);
      this.addSetting(EngineSettings.LIGHT_RAY_COUNT, 32);
      this.addSetting(EngineSettings.G_MAX_MISSILE_TRAILS, 32);
      this.addSetting(EngineSettings.G_MAX_SEGMENTSDRAWN, 500);
      this.addSetting(EngineSettings.G_MAX_BEAMS, 128);
      this.addSetting(EngineSettings.G_BASIC_SELECTION_BOX, false);
      this.addSetting(EngineSettings.LOD_DISTANCE_IN_THRESHOLD, 25.0F);
      this.addSetting(EngineSettings.CREATE_MANAGER_MESHES, true);
   }
}
