package org.schema.schine.graphicsengine.core.settings.typegetter;

public class TypeNotKnowException extends Exception {
   private static final long serialVersionUID = 1L;

   public TypeNotKnowException(String var1) {
      super(var1);
   }
}
