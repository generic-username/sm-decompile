package org.schema.schine.graphicsengine.core.settings.typegetter;

public class ShortGetter extends TypeGetter {
   public Integer parseType(String var1) throws NumberFormatException {
      return Integer.parseInt(var1);
   }
}
