package org.schema.schine.graphicsengine.core;

import com.bulletphysics.linearmath.Transform;
import com.oculusvr.capi.Hmd;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.nio.FloatBuffer;
import java.util.List;
import javax.vecmath.Matrix3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.common.FastMath;
import org.schema.common.SortedList;
import org.schema.schine.graphicsengine.OculusVrHelper;
import org.schema.schine.graphicsengine.camera.Camera;
import org.schema.schine.graphicsengine.camera.viewer.PositionableViewer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.Light;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.shader.ErrorDialogException;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.Mouse;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.client.ClientController;

public abstract class AbstractScene implements DrawableScene {
   public static final List spotLights = new ObjectArrayList();
   private static final FloatBuffer projMatBuffer = BufferUtils.createFloatBuffer(16);
   public static List infoList = new com.bulletphysics.util.ObjectArrayList();
   public static Light mainLight;
   public static float farPlane = 500000.0F;
   public static float nearPlane = 1.0F;
   public static SortedList zSortedMap = new SortedList();
   static FloatBuffer f = BufferUtils.createFloatBuffer(1);
   static FloatBuffer coord = BufferUtils.createFloatBuffer(3);
   static boolean test = true;
   static float[] fogColor = new float[]{0.5F, 0.5F, 0.5F, 1.0F};
   static FloatBuffer fogColorBuffer = BufferUtils.createFloatBuffer(4);
   private static float zoomFactor = 1.0F;
   protected boolean firstDraw = true;
   protected FrameBufferObjects fbo;
   protected ErrorDialogException requestedThrowError;
   Vector3f rayFrom = new Vector3f();
   Vector3f rayForward = new Vector3f();
   Vector3f rightOffset = new Vector3f();
   Vector3f vertical = new Vector3f();
   GUITextOverlay infoTextOverlay;
   GUITextOverlay pingTextOverlay;
   private Camera sceneCamera;
   private boolean textInit;
   private StateInterface state;

   public AbstractScene(StateInterface var1) {
      this.initialize();
      this.state = var1;
   }

   public static Vector3f getAbsoluteMousePosition(Vector3f var0, FrameBufferObjects var1) {
      float var2 = (float)Mouse.getX();
      float var3 = (float)Mouse.getY();
      if (test) {
         GlUtil.printGlErrorCritical();
      }

      f.rewind();
      if (var1 != null && (Integer)EngineSettings.G_MULTI_SAMPLE.getCurrentState() != 0) {
         GL11.glReadPixels((int)var2, (int)var3, 1, 1, 6402, 5126, f);
      } else {
         GL11.glReadPixels((int)var2, (int)var3, 1, 1, 6402, 5126, f);
      }

      if (test) {
         GlUtil.printGlErrorCritical();
      }

      test = false;
      float var4 = f.get(0);
      GlUtil.modelBuffer.rewind();
      GlUtil.projBuffer.rewind();
      Controller.modelviewMatrix.store(GlUtil.modelBuffer);
      Controller.projectionMatrix.store(GlUtil.projBuffer);
      GlUtil.modelBuffer.rewind();
      GlUtil.projBuffer.rewind();
      coord.rewind();
      GLU.gluUnProject(var2, var3, var4, GlUtil.modelBuffer, GlUtil.projBuffer, Controller.viewport, coord);
      var0.x = coord.get(0);
      var0.y = coord.get(1);
      var0.z = coord.get(2);
      return var0;
   }

   protected static float getDepthOfPixel(int var0, int var1) {
      FloatBuffer var2 = BufferUtils.createFloatBuffer(1);
      GL11.glReadPixels(var0, var1, 1, 1, 6402, 5126, var2);
      return var2.get();
   }

   public static void setZoomFactorForRender(boolean var0, float var1) {
      if (var0) {
         zoomFactor = var1;
      }

   }

   public static float getZoomFactorForRender(boolean var0) {
      return var0 ? zoomFactor : 1.0F;
   }

   public static float getZoomFactorUnchecked() {
      return zoomFactor;
   }

   public static float getFarPlane() {
      return farPlane;
   }

   public void setFarPlane(float var1) {
      farPlane = var1;
   }

   public static float getNearPlane() {
      return nearPlane;
   }

   public void setNearPlane(float var1) {
      nearPlane = var1;
   }

   public static void initProjection() {
   }

   public abstract void addPhysicsDebugDrawer();

   public abstract void applyEngineSettings();

   protected void attachCamera() {
      Controller.viewer = new PositionableViewer();
      this.setSceneCamera(new Camera(this.state, Controller.viewer));
      Controller.setCamera(this.getSceneCamera());
   }

   public abstract void cleanUp();

   public void initProjection(int var1) {
      Controller.ocMode = var1;
      if (var1 == 0) {
         GlUtil.glMatrixMode(5889);
         GlUtil.glLoadIdentity();
         float var13 = (float)GLFrame.getWidth() / (float)GLFrame.getHeight();
         GlUtil.gluPerspective(Controller.projectionMatrix, (Float)EngineSettings.G_FOV.getCurrentState() * zoomFactor, var13, getNearPlane(), getFarPlane(), true);
         GlUtil.glColor4f(0.2F, 1.0F, 0.3F, 1.0F);
         if (EngineSettings.G_DRAW_FOG.isOn()) {
            this.useFog();
         }

         GlUtil.glMatrixMode(5888);
         GlUtil.glLoadIdentity();
         Controller.getCamera().lookAt(true);
      } else {
         GraphicsContext.sensorState = GraphicsContext.hmd.getSensorState(Hmd.getTimeInSeconds());
         Matrix4f var2 = GlUtil.createPerspectiveProjectionMatrix((Float)EngineSettings.G_FOV.getCurrentState() * zoomFactor, OculusVrHelper.getAspectRatio(), getNearPlane(), getFarPlane());
         Transform var3 = new Transform();
         Controller.getMat(var2, var3);
         Transform var11 = new Transform();
         Transform var4 = new Transform();
         var11.setIdentity();
         var11.origin.set(OculusVrHelper.getProjectionCenterOffset(), 0.0F, 0.0F);
         Matrix4f var5 = new Matrix4f();
         Controller.getMat(var11, var5);
         var4.setIdentity();
         var4.origin.set(-OculusVrHelper.getProjectionCenterOffset(), 0.0F, 0.0F);
         Matrix4f var6 = new Matrix4f();
         Controller.getMat(var4, var6);
         Transform var7 = new Transform();
         Transform var8 = new Transform();
         var7.mul(var11, var3);
         var8.mul(var4, var3);
         (var11 = new Transform()).setIdentity();
         (var3 = new Transform()).setIdentity();
         float var15 = OculusVrHelper.getInterpupillaryDistance() * 0.5F;
         var11.origin.set(var15, 0.0F, 0.0F);
         var3.origin.set(-var15, 0.0F, 0.0F);
         Matrix4f var9;
         Transform var10;
         Quat4f var12;
         Matrix3f var14;
         if (var1 == 2) {
            GlUtil.glMatrixMode(5889);
            GlUtil.glLoadIdentity();
            Controller.occulusProjMatrix = new Matrix4f(var5);
            GLFrame.getWidth();
            GLFrame.getHeight();
            var9 = GlUtil.gluPerspective(Controller.projectionMatrix, (Float)EngineSettings.G_FOV.getCurrentState() * zoomFactor, OculusVrHelper.getAspectRatio(), getNearPlane(), getFarPlane(), false);
            Matrix4f.mul(var5, var9, var9);
            GlUtil.glLoadMatrix(var9);
            GlUtil.glMatrixMode(5888);
            GlUtil.glLoadIdentity();
            var10 = new Transform(Controller.getCamera().getWorldTransform());
            Controller.getCamera().getWorldTransform().mul(var11);
            if (Keyboard.isKeyDown(46)) {
               GraphicsContext.hmd.recenterPose();
            }

            var12 = new Quat4f(GraphicsContext.sensorState.HeadPose.Pose.Orientation.x, GraphicsContext.sensorState.HeadPose.Pose.Orientation.y, GraphicsContext.sensorState.HeadPose.Pose.Orientation.z, GraphicsContext.sensorState.HeadPose.Pose.Orientation.w);
            (var14 = new Matrix3f()).set(var12);
            Controller.getCamera().getWorldTransform().basis.set(var14);
            Controller.getCamera().lookAt(true);
            Controller.getCamera().getWorldTransform().set(var10);
         } else if (var1 == 1) {
            GlUtil.glMatrixMode(5889);
            GlUtil.glLoadIdentity();
            Controller.occulusProjMatrix = new Matrix4f(var6);
            GLFrame.getWidth();
            GLFrame.getHeight();
            var9 = GlUtil.gluPerspective(Controller.projectionMatrix, (Float)EngineSettings.G_FOV.getCurrentState() * zoomFactor, OculusVrHelper.getAspectRatio(), getNearPlane(), getFarPlane(), false);
            Matrix4f.mul(var6, var9, var9);
            GlUtil.glLoadMatrix(var9);
            GlUtil.glMatrixMode(5888);
            GlUtil.glLoadIdentity();
            var10 = new Transform(Controller.getCamera().getWorldTransform());
            Controller.getCamera().getWorldTransform().mul(var3);
            var12 = new Quat4f(GraphicsContext.sensorState.HeadPose.Pose.Orientation.x, GraphicsContext.sensorState.HeadPose.Pose.Orientation.y, GraphicsContext.sensorState.HeadPose.Pose.Orientation.z, GraphicsContext.sensorState.HeadPose.Pose.Orientation.w);
            (var14 = new Matrix3f()).set(var12);
            Controller.getCamera().getWorldTransform().basis.set(var14);
            Controller.getCamera().lookAt(true);
            Controller.getCamera().getWorldTransform().set(var10);
         } else {
            assert false;

         }
      }
   }

   protected void drawInfo() {
      if (EngineSettings.S_INFO_DRAW.getCurrentState().equals("NO_INFO") || EngineSettings.G_DRAW_NO_OVERLAYS.isOn()) {
         infoList.clear();
         this.textInit = false;
      }

      if (!this.textInit) {
         infoList.clear();
         this.textInit = true;
      } else {
         String var1 = "";
         if (!EngineSettings.S_INFO_DRAW.getCurrentState().equals("SOME_INFO")) {
            if (!EngineSettings.S_INFO_DRAW.getCurrentState().equals("FPS_AND_PING")) {
               var1 = (String)infoList.remove(1);
            } else {
               while(infoList.size() > 2) {
                  infoList.remove(infoList.size() - 1);
               }

               if (ClientController.isLocalHost()) {
                  infoList.remove(infoList.size() - 1);
               } else {
                  var1 = (String)infoList.remove(infoList.size() - 1);
               }
            }
         } else {
            while(infoList.size() > 5) {
               infoList.remove(infoList.size() - 1);
            }

            var1 = (String)infoList.remove(1);
         }

         this.infoTextOverlay.getPos().x = 2.0F;
         this.infoTextOverlay.getPos().y = (float)(GLFrame.getHeight() - (4 + this.infoTextOverlay.getTextHeight()));
         this.infoTextOverlay.getText().clear();
         this.infoTextOverlay.getText().addAll(infoList);
         this.pingTextOverlay.getPos().x = 138.0F;
         this.pingTextOverlay.getPos().y = (float)(GLFrame.getHeight() - (4 + this.infoTextOverlay.getTextHeight()));
         this.pingTextOverlay.getText().set(0, var1);
         infoList.clear();
         GUIElement.enableOrthogonal();
         this.infoTextOverlay.draw();
         this.pingTextOverlay.draw();
         GUIElement.disableOrthogonal();
      }
   }

   public void drawLine(Vector3f var1, Vector3f var2, Vector3f var3) {
      GL11.glBegin(1);
      GlUtil.glColor4f(var3.x, var3.y, var3.z, 1.0F);
      GL11.glVertex3f(var1.x, var1.y, var1.z);
      GL11.glVertex3f(var2.x, var2.y, var2.z);
      GL11.glEnd();
   }

   public Light getMainLight() {
      return mainLight;
   }

   public void setMainLight(Light var1) {
      mainLight = var1;
   }

   public String getPlayerName() {
      return "unknownScene";
   }

   public Vector3f getRayTo(int var1, int var2) {
      projMatBuffer.rewind();
      Controller.projectionMatrix.load(projMatBuffer);
      float var3 = (float)((double)getNearPlane() * (1.0D + (double)projMatBuffer.get(6)) / (double)projMatBuffer.get(5));
      float var4 = (float)((double)getNearPlane() * ((double)projMatBuffer.get(6) - 1.0D) / (double)projMatBuffer.get(5));
      var3 = (var3 - var4) * 0.5F / getNearPlane();
      var3 = 2.0F * FastMath.atan(var3);
      this.rayFrom.set(Controller.getCamera().getPos());
      this.rayForward.set(Controller.getCamera().getForward());
      this.rayForward.normalize();
      this.rayForward.scale(getFarPlane());
      this.rightOffset = new Vector3f();
      this.vertical.set(Controller.getCamera().getUp());
      Vector3f var9;
      (var9 = new Vector3f()).cross(this.rayForward, this.vertical);
      var9.normalize();
      this.vertical.cross(var9, this.rayForward);
      this.vertical.normalize();
      var3 = FastMath.tan(0.5F * var3);
      float var5 = (float)GLFrame.getHeight() / (float)GLFrame.getWidth();
      var9.scale(2.0F * getFarPlane() * var3);
      this.vertical.scale(2.0F * getFarPlane() * var3);
      if (var5 < 1.0F) {
         var9.scale(1.0F / var5);
      } else {
         this.vertical.scale(var5);
      }

      Vector3f var10;
      (var10 = new Vector3f()).add(this.rayFrom, this.rayForward);
      Vector3f var11;
      (var11 = new Vector3f(var9)).scale(1.0F / (float)GLFrame.getWidth());
      Vector3f var6;
      (var6 = new Vector3f(this.vertical)).scale(1.0F / (float)GLFrame.getHeight());
      Vector3f var7 = new Vector3f();
      Vector3f var8 = new Vector3f();
      var7.scale(0.5F, var9);
      var8.scale(0.5F, this.vertical);
      (var9 = new Vector3f()).sub(var10, var7);
      var9.add(var8);
      var7.scale((float)var1, var11);
      var8.scale((float)var2, var6);
      var9.add(var7);
      var9.sub(var8);
      return var9;
   }

   public Camera getSceneCamera() {
      return this.sceneCamera;
   }

   public void setSceneCamera(Camera var1) {
      this.sceneCamera = var1;
   }

   protected void initialize() {
      Light.resetLightAssignment();
      this.setMainLight(new Light());
      this.getMainLight().setPos(450.0F, 900.0F, 0.0F);
      this.infoTextOverlay = new GUITextOverlay(800, 600, (InputState)null);
      this.infoTextOverlay.setText(new com.bulletphysics.util.ObjectArrayList());
      this.infoTextOverlay.useUncachedDefaultFont(true);
      this.pingTextOverlay = new GUITextOverlay(800, 600, (InputState)null);
      this.pingTextOverlay.useUncachedDefaultFont(true);
      com.bulletphysics.util.ObjectArrayList var1;
      (var1 = new com.bulletphysics.util.ObjectArrayList(1)).add("");
      this.pingTextOverlay.setText(var1);
   }

   public void requestThrowException(ErrorDialogException var1) {
      this.requestedThrowError = var1;
   }

   public void useFog() {
      fogColorBuffer.rewind();
      fogColorBuffer.put(fogColor);
      fogColorBuffer.rewind();
      GlUtil.glEnable(2912);
      GL11.glFogi(2917, 2049);
      GL11.glFog(2918, fogColorBuffer);
      GL11.glFogf(2914, 0.1F);
      GL11.glHint(3156, 4354);
   }
}
