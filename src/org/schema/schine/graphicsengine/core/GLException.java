package org.schema.schine.graphicsengine.core;

public class GLException extends Exception {
   private static final long serialVersionUID = 1L;

   public GLException(String var1) {
      super(var1);
   }
}
