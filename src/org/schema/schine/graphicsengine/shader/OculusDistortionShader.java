package org.schema.schine.graphicsengine.shader;

import org.schema.schine.graphicsengine.OculusVrHelper;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;

public class OculusDistortionShader implements Shaderable {
   private int ocState;
   private int width;
   private int height;

   public void setFBO(int var1) {
      this.ocState = var1;
   }

   public void onExit() {
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderInt(var1, "texSceneFinal", 0);

      assert this.ocState != 0;

      this.width = GLFrame.getWidth();
      this.height = GLFrame.getHeight();
      this.width = (int)((float)this.width * OculusVrHelper.getScaleFactor());
      this.height = (int)((float)this.height * OculusVrHelper.getScaleFactor());
      if (this.ocState == 2) {
         this.updateOcShaderParametersForVP(var1, 0, 0, this.width / 2, this.height, this.ocState);
      } else {
         this.updateOcShaderParametersForVP(var1, this.width / 2, 0, this.width / 2, this.height, this.ocState);
      }
   }

   private void updateOcShaderParametersForVP(Shader var1, int var2, int var3, int var4, int var5, int var6) {
      float var7 = (float)var4 / (float)this.width;
      float var8 = (float)var5 / (float)this.height;
      float var9 = (float)var2 / (float)this.width;
      float var10 = (float)var3 / (float)this.height;
      float var11 = (float)var4 / (float)var5;
      GlUtil.updateShaderVector4f(var1, "ocHmdWarpParam", OculusVrHelper.getDistortionParams()[0], OculusVrHelper.getDistortionParams()[1], OculusVrHelper.getDistortionParams()[2], OculusVrHelper.getDistortionParams()[3]);
      float var12 = var6 == 1 ? -1.0F * OculusVrHelper.getLensViewportShift() : OculusVrHelper.getLensViewportShift();
      GlUtil.updateShaderVector2f(var1, "ocLensCenter", var9 + (var7 + var12 * 0.5F) * 0.5F, var10 + var8 * 0.5F);
      GlUtil.updateShaderVector2f(var1, "ocScreenCenter", var9 + var7 * 0.5F, var10 + var8 * 0.5F);
      var9 = 1.0F / OculusVrHelper.getScaleFactor();
      GlUtil.updateShaderVector2f(var1, "ocScale", var7 / 2.0F * var9, var8 / 2.0F * var9 * var11);
      GlUtil.updateShaderVector2f(var1, "ocScaleIn", 2.0F / var7, 2.0F / var8 / var11);
   }

   public Shader getShader() {
      return ShaderLibrary.ocDistortion;
   }
}
