package org.schema.schine.graphicsengine.shader;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectCollection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.lwjgl.opengl.GL20;
import org.schema.common.util.data.DataUtil;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class ShaderLibrary {
   public static final Set shaders = new HashSet();
   public static int CUBE_VERTEX_COMPONENTS;
   public static Shader projectileShader;
   public static Shader projectileQuadShader;
   public static Shader projectileQuadBloomShader;
   public static Shader projectileBeamQuadShader;
   public static Shader starShader;
   public static Shader starFlareShader;
   public static Shader plasmaShader;
   public static Shader cubeShieldShader;
   public static Shader scanlineShader;
   public static Shader shardShader;
   public static Shader guiTextureWrapperShader;
   public static Shader jumpOverlayShader;
   public static Shader ocDistortion;
   public static Shader powerBarShader;
   public static Shader powerBarShaderHor;
   public static Shader colorBoxShader;
   public static Shader gamma;
   public static Shader terrainShader;
   public static Shader waterShader;
   public static Shader explosionShader;
   public static Shader simpleColorShader;
   public static Shader spacedustShader;
   public static Shader fogShader;
   public static Shader bumpShader;
   public static Shader planetShader;
   public static Shader atmosphereShader;
   public static Shader bloomShader;
   public static Shader sunShader;
   public static Shader lavaShader;
   public static Shader silhouetteShader;
   public static Shader blackHoleShader;
   public static Shader hoffmanSkyShader;
   public static Shader hoffmanTerrainShader;
   public static Shader cubemapShader;
   public static Shader bloomShaderPass1;
   public static Shader bloomShaderPass2;
   public static Shader bloomShaderPass3;
   public static Shader bloomShaderPass4;
   public static Shader bloomShader1Vert;
   public static Shader bloomShader2Hor;
   public static Shader downsamplerFirst;
   public static Shader downsampler;
   public static Shader silhouetteShader2D;
   public static Shader silhouetteAlpha;
   public static Shader shadowShaderCubes;
   public static Shader depthCubeShader;
   public static Shader shadowShaderCubesBlend;
   public static Shader graphConnectionShader;
   public static Shader bloomSimpleShader;
   public static Shader transporterShader;
   public static Shader mineShader;
   public static Shader cubeShader13Simple;
   public static Shader exaustShader;
   public static Shader selectionShader;
   public static Shader solidSelectionShader;
   public static Shader beamBoxShader;
   public static Shader simpleBeamShader;
   public static Shader godRayShader;
   public static Shader projectileTrailShader;
   public static Shader lensFlareShader;
   public static Shader perpixelShader;
   public static Shader skyFromAtmo;
   public static Shader skyFromSpace;
   public static Shader tubesShader;
   public static Shader tubesStreamShader;
   public static Shader pulseShader;
   public static Shader shad_single_prog;
   public static Shader shad_single_hl_prog;
   public static Shader shad_multi_prog;
   public static Shader shad_multi_noleak_prog;
   public static Shader shad_pcf_prog;
   public static Shader shad_pcf_trilin_prog;
   public static Shader shad_pcf_4tap_prog;
   public static Shader shad_pcf_8tap_prog;
   public static Shader shad_pcf_gaussian_prog;
   public static Shader shad_view;
   public static Shader fieldShader;
   public static Shader bgShader;
   public static Shader tunnelShader;
   private static Int2ObjectOpenHashMap skinningShader;
   private static Int2ObjectOpenHashMap skinningShaderSpot;
   private static Int2ObjectOpenHashMap cubeShaders;
   public static boolean USE_CUBE_TEXTURE_EMISSION;
   public static Shader lodCubeShader;
   public static Shader lodCubeShaderTangent;
   public static Shader lodCubeShaderNormalOff;
   public static Shader lodCubeShaderShadow;
   public static Shader outlineShader;
   public static Shader cubeGroupShader;
   public static Shader cubeShader13SimpleWhite;
   public static Shader shieldBubbleShader;
   public static final int CUBE_SHADER_VERT_INDEX = 0;
   public static final int ElementCollectionMesh_VERT_ATTRIB_INDEX = 0;

   public static void cleanUp() {
      Iterator var0 = shaders.iterator();

      while(var0.hasNext()) {
         Shader var1;
         if ((var1 = (Shader)var0.next()) != null) {
            var1.cleanUp();
         }
      }

      skinningShader.clear();
      skinningShaderSpot.clear();
      cubeShaders.clear();
   }

   public static void loadShaders() {
      try {
         if (EngineSettings.G_ATMOSPHERE_SHADER.getCurrentState().equals("normal")) {
            skyFromSpace = new Shader(DataUtil.dataPath + "/shader/atmosphere/SkyFromSpace.vert", DataUtil.dataPath + "/shader/atmosphere/SkyFromSpace.frag", new ShaderModifyInterface[0]);
            skyFromAtmo = new Shader(DataUtil.dataPath + "/shader/atmosphere/SkyFromAtmosphere.vert", DataUtil.dataPath + "/shader/atmosphere/SkyFromAtmosphere.frag", new ShaderModifyInterface[0]);
         }

         perpixelShader = new Shader(DataUtil.dataPath + "/shader/perpixel/perpixel.vert.glsl", DataUtil.dataPath + "/shader/perpixel/perpixel.frag.glsl", new ShaderModifyInterface[0]);
         blackHoleShader = new Shader(DataUtil.dataPath + "/shader/blackhole/blackhole.vert.glsl", DataUtil.dataPath + "/shader/blackhole/blackhole.frag.glsl", new ShaderModifyInterface[0]);
         selectionShader = new Shader(DataUtil.dataPath + "/shader/cube/selectionSingle.vert.glsl", DataUtil.dataPath + "/shader/cube/selectionSingle.frag.glsl", new ShaderModifyInterface[0]);
         solidSelectionShader = new Shader(DataUtil.dataPath + "/shader/cube/selectionSolid.vert.glsl", DataUtil.dataPath + "/shader/cube/selectionSolid.frag.glsl", new ShaderModifyInterface[0]);
         beamBoxShader = new Shader(DataUtil.dataPath + "/shader/cube/beamHitSingle.vert.glsl", DataUtil.dataPath + "/shader/cube/beamHitSingle.frag.glsl", new ShaderModifyInterface[0]);
         plasmaShader = new Shader(DataUtil.dataPath + "/shader/plasma/plasma.vert.glsl", DataUtil.dataPath + "/shader/plasma/plasma.frag.glsl", new ShaderModifyInterface[0]);
         godRayShader = new Shader(DataUtil.dataPath + "/shader/bloom/godrays.vert.glsl", DataUtil.dataPath + "/shader/bloom/godrays.frag.glsl", new ShaderModifyInterface[0]);
         tubesShader = new Shader(DataUtil.dataPath + "/shader/tubes/tube.vert.glsl", DataUtil.dataPath + "/shader/tubes/tube.frag.glsl", new ShaderModifyInterface[0]);
         tubesStreamShader = new Shader(DataUtil.dataPath + "/shader/tubes/stream.vert.glsl", DataUtil.dataPath + "/shader/tubes/stream.frag.glsl", new ShaderModifyInterface[0]);
         ocDistortion = new Shader(DataUtil.dataPath + "/shader/ocDistortion/ocDistortion_vert.glsl", DataUtil.dataPath + "/shader/ocDistortion/ocDistortion_frag.glsl", new ShaderModifyInterface[0]);
         gamma = new Shader(DataUtil.dataPath + "/shader/gamma/gamma.vert.glsl", DataUtil.dataPath + "/shader/gamma/gamma.frag.glsl", new ShaderModifyInterface[0]);
         graphConnectionShader = new Shader(DataUtil.dataPath + "/shader/hud/graph/graphconnection.vert.glsl", DataUtil.dataPath + "/shader/hud/graph/graphconnection.frag.glsl", new ShaderModifyInterface[0]);
         simpleBeamShader = new Shader(DataUtil.dataPath + "/shader/simplebeam/simplebeam.vert.glsl", DataUtil.dataPath + "/shader/simplebeam/simplebeam.frag.glsl", new ShaderModifyInterface[0]);
         transporterShader = new Shader(DataUtil.dataPath + "/shader/transporter/transporter.vert.glsl", DataUtil.dataPath + "/shader/transporter/transporter.frag.glsl", new ShaderModifyInterface[0]);
         mineShader = new Shader(DataUtil.dataPath + "/shader/mine/mine.vert.glsl", DataUtil.dataPath + "/shader/mine/mine.frag.glsl", new ShaderModifyInterface[]{new ShaderPreprocessor(new String[]{EngineSettings.G_SHADOWS.isOn() ? "shadow" : null, EngineSettings.G_SHADOWS.isOn() ? EngineSettings.G_SHADOW_QUALITY.getCurrentState().toString() : null, EngineSettings.G_SHADOWS.isOn() && EngineSettings.G_SHADOWS_VSM.isOn() ? "VSM" : null})});
         shieldBubbleShader = new Shader(DataUtil.dataPath + "/shader/shieldhit/shieldhit.vert.glsl", DataUtil.dataPath + "/shader/shieldhit/shieldhit.frag.glsl", new ShaderModifyInterface[0]);
         pulseShader = new Shader(DataUtil.dataPath + "/shader/pulse/pulse.vert.glsl", DataUtil.dataPath + "/shader/pulse/pulse.frag.glsl", new ShaderModifyInterface[0]);
         bumpShader = new Shader(DataUtil.dataPath + "/shader/bump.vert", DataUtil.dataPath + "/shader/bump.frag", new ShaderModifyInterface[0]);
         projectileShader = new Shader(DataUtil.dataPath + "/shader/projectiles/standard/projectile.vsh", DataUtil.dataPath + "/shader/projectiles/standard/projectile.fsh", new ShaderModifyInterface[0]);
         guiTextureWrapperShader = new Shader(DataUtil.dataPath + "/shader/texture/wrap.vert.glsl", DataUtil.dataPath + "/shader/texture/wrap.frag.glsl", new ShaderModifyInterface[0]);
         scanlineShader = new Shader(DataUtil.dataPath + "/shader/scanline/scanline.vert.glsl", DataUtil.dataPath + "/shader/scanline/scanline.frag.glsl", new ShaderModifyInterface[0]);
         projectileTrailShader = new Shader(DataUtil.dataPath + "/shader/projectiles/trail/projectileTrail.vsh", DataUtil.dataPath + "/shader/projectiles/trail/projectileTrail.fsh", new ShaderModifyInterface[0]);
         projectileQuadShader = new Shader(DataUtil.dataPath + "/shader/projectiles/standard/projectileQuad.vsh", DataUtil.dataPath + "/shader/projectiles/standard/projectileQuad.fsh", new ShaderModifyInterface[0]);
         projectileQuadBloomShader = new Shader(DataUtil.dataPath + "/shader/projectiles/standard/projectileQuad.vsh", DataUtil.dataPath + "/shader/projectiles/standard/projectileQuad.fsh", new ShaderModifyInterface[0]);
         projectileBeamQuadShader = new Shader(DataUtil.dataPath + "/shader/projectiles/beam/laserProjectile.vert.glsl", DataUtil.dataPath + "/shader/projectiles/beam/laserProjectile.frag.glsl", new ShaderModifyInterface[0]);
         explosionShader = new Shader(DataUtil.dataPath + "/shader/explosion/explosion.vert.glsl", DataUtil.dataPath + "/shader/explosion/explosion.frag.glsl", new ShaderModifyInterface[0]);
         simpleColorShader = new Shader(DataUtil.dataPath + "/shader/simple/color.vert.glsl", DataUtil.dataPath + "/shader/simple/color.frag.glsl", new ShaderModifyInterface[0]);
         if (!EngineSettings.G_SPACE_PARTICLE.isOn()) {
            createSpaceDustShader();
         }

         planetShader = new Shader(DataUtil.dataPath + "/shader/planet.vert.glsl", DataUtil.dataPath + "/shader/planet.frag.glsl", new ShaderModifyInterface[0]);
         atmosphereShader = new Shader(DataUtil.dataPath + "/shader/atmosphere.vert.glsl", DataUtil.dataPath + "/shader/atmosphere.frag.glsl", new ShaderModifyInterface[0]);
         bloomShader = new Shader(DataUtil.dataPath + "/shader/bloom.vert.glsl", DataUtil.dataPath + "/shader/bloom2D.frag.glsl", new ShaderModifyInterface[0]);
         sunShader = new Shader(DataUtil.dataPath + "/shader/sun.vert.glsl", DataUtil.dataPath + "/shader/sun.frag.glsl", new ShaderModifyInterface[0]);
         lavaShader = new Shader(DataUtil.dataPath + "/shader/lava/lava.vert.glsl", DataUtil.dataPath + "/shader/lava/lava.frag.glsl", new ShaderModifyInterface[0]);
         (silhouetteShader = new Shader(DataUtil.dataPath + "/shader/silhouette.vert.glsl", DataUtil.dataPath + "/shader/silhouette.frag.glsl", new ShaderModifyInterface[0])).setShaderInterface(new SilhouetteShader());
         silhouetteShader2D = new Shader(DataUtil.dataPath + "/shader/silhouette.vert.glsl", DataUtil.dataPath + "/shader/silhouette2DAlpha.frag.glsl", new ShaderModifyInterface[0]);
         silhouetteAlpha = new Shader(DataUtil.dataPath + "/shader/silhouette.vert.glsl", DataUtil.dataPath + "/shader/silhouetteAlpha.frag.glsl", new ShaderModifyInterface[0]);
         exaustShader = new Shader(DataUtil.dataPath + "/shader/thruster/thruster.vert.glsl", DataUtil.dataPath + "/shader/thruster/thruster.frag.glsl", new ShaderModifyInterface[0]);
         outlineShader = new Shader(DataUtil.dataPath + "/shader/outline/outline.vert.glsl", DataUtil.dataPath + "/shader/outline/outline.frag.glsl", new ShaderModifyInterface[0]);
         if (EngineSettings.G_SHADOWS.isOn()) {
            shad_view = new Shader(DataUtil.dataPath + "/shader/shadow/view_vertex.glsl", DataUtil.dataPath + "/shader/shadow/view_fragment.glsl", new ShaderModifyInterface[0]);
         }

         starShader = new Shader(DataUtil.dataPath + "/shader/starsExt/stars.vsh", DataUtil.dataPath + "/shader/starsExt/stars.fsh", new ShaderModifyInterface[0]);
         fieldShader = new Shader(DataUtil.dataPath + "/shader/starsExt/field.vert.glsl", DataUtil.dataPath + "/shader/starsExt/field.frag.glsl", new ShaderModifyInterface[0]);
         bgShader = new Shader(DataUtil.dataPath + "/shader/bg.vert.glsl", DataUtil.dataPath + "/shader/bg.frag.glsl", new ShaderModifyInterface[0]);
         tunnelShader = new Shader(DataUtil.dataPath + "/shader/hyperspace/tunnel.vert.glsl", DataUtil.dataPath + "/shader/hyperspace/tunnel.frag.glsl", new ShaderModifyInterface[0]);
         starFlareShader = new Shader(DataUtil.dataPath + "/shader/starsExt/starsflare.vsh", DataUtil.dataPath + "/shader/starsExt/starsflare.fsh", new ShaderModifyInterface[0]);
         powerBarShader = new Shader(DataUtil.dataPath + "/shader/hud/powerbar/powerbar.vert.glsl", DataUtil.dataPath + "/shader/hud/powerbar/powerbar.frag.glsl", new ShaderModifyInterface[0]);
         powerBarShaderHor = new Shader(DataUtil.dataPath + "/shader/hud/powerbar/powerbarHorizontal.vert.glsl", DataUtil.dataPath + "/shader/hud/powerbar/powerbarHorizontal.frag.glsl", new ShaderModifyInterface[0]);
         cubeGroupShader = new Shader(DataUtil.dataPath + "/shader/cube/groups/cubegrp.vert.glsl", DataUtil.dataPath + "/shader/cube/groups/cubegrp.frag.glsl", new ShaderModifyInterface[]{new ShaderPreprocessor(new String[]{EngineSettings.G_ELEMENT_COLLECTION_INT_ATT.isOn() && GraphicsContext.INTEGER_VERTICES ? "INTATT" : null, GraphicsContext.EXT_GPU_SHADER4() && EngineSettings.G_USE_SHADER4.isOn() ? "shader4" : null, GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "force130" : null})}) {
            public final void bindAttributes(int var1) {
               if (GraphicsContext.INTEGER_VERTICES) {
                  GL20.glBindAttribLocation(var1, 0, "ivert");
               }

            }
         };
         createCubeShaders();
         colorBoxShader = new Shader(DataUtil.dataPath + "/shader/cube/colorbox.vert.glsl", DataUtil.dataPath + "/shader/cube/colorbox.frag.glsl", new ShaderModifyInterface[0]);
         lodCubeShader = new Shader(DataUtil.dataPath + "/shader/cube/lodCube/lodcube.vert.glsl", DataUtil.dataPath + "/shader/cube/lodCube/lodcube.frag.glsl", new ShaderModifyInterface[]{new ShaderPreprocessor(new String[]{EngineSettings.G_NORMAL_MAPPING.isOn() ? "normalmap" : null, EngineSettings.G_SHADOWS.isOn() && EngineSettings.G_SHADOWS_VSM.isOn() ? "VSM" : null, EngineSettings.G_SHADOWS.isOn() ? "shadow" : null, "owntangent", EngineSettings.G_SHADOWS.isOn() ? EngineSettings.G_SHADOW_QUALITY.getCurrentState().toString() : null, GraphicsContext.EXT_GPU_SHADER4() && EngineSettings.G_USE_SHADER4.isOn() ? "shader4" : null, GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "force130" : null})});
         lodCubeShaderShadow = new Shader(DataUtil.dataPath + "/shader/cube/lodCube/lodcube.vert.glsl", DataUtil.dataPath + "/shader/cube/lodCube/lodcube-shadow.frag.glsl", new ShaderModifyInterface[]{new ShaderPreprocessor(new String[]{EngineSettings.G_SHADOWS.isOn() && EngineSettings.G_SHADOWS_VSM.isOn() ? "VSM" : null, EngineSettings.G_SHADOWS.isOn() ? "shadow" : null, EngineSettings.G_SHADOWS.isOn() ? EngineSettings.G_SHADOW_QUALITY.getCurrentState().toString() : null, EngineSettings.G_NORMAL_MAPPING.isOn() ? "normalmap" : null, "owntangent", GraphicsContext.EXT_GPU_SHADER4() && EngineSettings.G_USE_SHADER4.isOn() ? "shader4" : null, GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "force130" : null})});
         lodCubeShaderTangent = new Shader(DataUtil.dataPath + "/shader/cube/lodCube/lodcube.vert.glsl", DataUtil.dataPath + "/shader/cube/lodCube/lodcube.frag.glsl", new ShaderModifyInterface[]{new ShaderPreprocessor(new String[]{EngineSettings.G_SHADOWS.isOn() && EngineSettings.G_SHADOWS_VSM.isOn() ? "VSM" : null, EngineSettings.G_SHADOWS.isOn() ? "shadow" : null, EngineSettings.G_SHADOWS.isOn() ? EngineSettings.G_SHADOW_QUALITY.getCurrentState().toString() : null, EngineSettings.G_NORMAL_MAPPING.isOn() ? "normalmap" : null, "owntangent", GraphicsContext.EXT_GPU_SHADER4() && EngineSettings.G_USE_SHADER4.isOn() ? "shader4" : null, GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "force130" : null})});
         lodCubeShaderNormalOff = new Shader(DataUtil.dataPath + "/shader/cube/lodCube/lodcube.vert.glsl", DataUtil.dataPath + "/shader/cube/lodCube/lodcube.frag.glsl", new ShaderModifyInterface[]{new ShaderPreprocessor(new String[]{EngineSettings.G_SHADOWS.isOn() && EngineSettings.G_SHADOWS_VSM.isOn() ? "VSM" : null, EngineSettings.G_SHADOWS.isOn() ? "shadow" : null, EngineSettings.G_SHADOWS.isOn() ? EngineSettings.G_SHADOW_QUALITY.getCurrentState().toString() : null, GraphicsContext.EXT_GPU_SHADER4() && EngineSettings.G_USE_SHADER4.isOn() ? "shader4" : null, GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "force130" : null})});
         shardShader = new Shader(DataUtil.dataPath + "/shader/cube/shard/shard.vert.glsl", DataUtil.dataPath + "/shader/cube/shard/shard.frag.glsl", new ShaderModifyInterface[]{new ShaderPreprocessor(new String[]{EngineSettings.G_SHADOWS.isOn() && EngineSettings.G_SHADOWS_VSM.isOn() ? "VSM" : null, CUBE_VERTEX_COMPONENTS > 2 ? "threeComp" : null, GraphicsContext.isTextureArrayEnabled() ? "texarray" : null, EngineSettings.G_SHADOWS.isOn() ? "shadow" : null, EngineSettings.G_SHADOWS.isOn() ? EngineSettings.G_SHADOW_QUALITY.getCurrentState().toString() : null, EngineSettings.G_NORMAL_MAPPING.isOn() && GraphicsContext.isTextureArrayEnabled() ? "normaltexarray" : null, EngineSettings.G_NORMAL_MAPPING.isOn() && !GraphicsContext.isTextureArrayEnabled() ? "normalmap" : null, GraphicsContext.EXT_GPU_SHADER4() && EngineSettings.G_USE_SHADER4.isOn() ? "shader4" : null, GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "force130" : null})});
         if (EngineSettings.G_DRAW_SHIELDS.isOn()) {
            cubeShieldShader = new Shader(DataUtil.dataPath + "/shader/cube/shieldCube/shieldcube-3rd.vsh", DataUtil.dataPath + "/shader/cube/shieldCube/shieldcube.fsh", new ShaderModifyInterface[]{new ShaderPreprocessor(new String[]{GraphicsContext.EXT_GPU_SHADER4() && EngineSettings.G_USE_SHADER4.isOn() ? "shader4" : null, GraphicsContext.INTEGER_VERTICES ? "INTATT" : null, GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "force130" : null})}) {
               public final void bindAttributes(int var1) {
                  if (GraphicsContext.INTEGER_VERTICES) {
                     GL20.glBindAttribLocation(var1, 0, "ivert");
                  }

               }
            };
         }

         if (EngineSettings.G_DRAW_JUMP_OVERLAY.isOn()) {
            jumpOverlayShader = new Shader(DataUtil.dataPath + "/shader/cube/jumpCube/jumpcube.vsh", DataUtil.dataPath + "/shader/cube/jumpCube/jumpcube.fsh", new ShaderModifyInterface[]{new ShaderPreprocessor(new String[]{GraphicsContext.EXT_GPU_SHADER4() && EngineSettings.G_USE_SHADER4.isOn() ? "shader4" : null, GraphicsContext.INTEGER_VERTICES ? "INTATT" : null, GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "force130" : null})}) {
               public final void bindAttributes(int var1) {
                  if (GraphicsContext.INTEGER_VERTICES) {
                     GL20.glBindAttribLocation(var1, 0, "ivert");
                  }

               }
            };
         }

         cubemapShader = new Shader(DataUtil.dataPath + "/shader/cubemap.vsh", DataUtil.dataPath + "/shader/cubemap.fsh", new ShaderModifyInterface[0]);
         bloomShaderPass1 = new Shader(DataUtil.dataPath + "/shader/bloom/bloom.vert.glsl", DataUtil.dataPath + "/shader/bloom/bloom1.frag.glsl", new ShaderModifyInterface[0]);
         bloomShaderPass2 = new Shader(DataUtil.dataPath + "/shader/bloom/bloom.vert.glsl", DataUtil.dataPath + "/shader/bloom/bloom2.frag.glsl", new ShaderModifyInterface[0]);
         bloomShaderPass3 = new Shader(DataUtil.dataPath + "/shader/bloom/bloom.vert.glsl", DataUtil.dataPath + "/shader/bloom/bloom3.frag.glsl", new ShaderModifyInterface[0]);
         bloomShaderPass4 = new Shader(DataUtil.dataPath + "/shader/bloom/bloom.vert.glsl", DataUtil.dataPath + "/shader/bloom/bloom4.frag.glsl", new ShaderModifyInterface[0]);
         bloomShader1Vert = new Shader(DataUtil.dataPath + "/shader/pointBloom/default.vert.glsl", DataUtil.dataPath + "/shader/pointBloom/blur1Vert.frag.glsl", new ShaderModifyInterface[0]);
         bloomShader2Hor = new Shader(DataUtil.dataPath + "/shader/pointBloom/default.vert.glsl", DataUtil.dataPath + "/shader/pointBloom/blur2Hor.frag.glsl", new ShaderModifyInterface[0]);
         downsampler = new Shader(DataUtil.dataPath + "/shader/pointBloom/default.vert.glsl", DataUtil.dataPath + "/shader/pointBloom/downsampler.frag.glsl", new ShaderModifyInterface[]{new ShaderPreprocessor(new String[]{"nothing"})});
         downsamplerFirst = new Shader(DataUtil.dataPath + "/shader/pointBloom/default.vert.glsl", DataUtil.dataPath + "/shader/pointBloom/downsampler.frag.glsl", new ShaderModifyInterface[]{new ShaderPreprocessor(new String[]{"withDepth"})});
         depthCubeShader = new Shader(DataUtil.dataPath + "/shader/cube/quads13/depthcube.vsh", DataUtil.dataPath + "/shader/cube/quads13/depthcube.fsh", new ShaderModifyInterface[]{new ShaderPreprocessor(new String[]{EngineSettings.G_SHADOWS.isOn() && EngineSettings.G_SHADOWS_VSM.isOn() ? "VSM" : null, GraphicsContext.isTextureArrayEnabled() ? "texarray" : null, EngineSettings.G_USE_VERTEX_LIGHTING_ONLY.isOn() ? "vertexLighting" : null, "owntangent", GraphicsContext.INTEGER_VERTICES ? "INTATT" : null, GraphicsContext.EXT_GPU_SHADER4() && EngineSettings.G_USE_SHADER4.isOn() ? "shader4" : null, GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "force130" : null, GraphicsContext.isIntel() && GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "intel130" : null})}) {
            public final void bindAttributes(int var1) {
               if (GraphicsContext.INTEGER_VERTICES) {
                  GL20.glBindAttribLocation(var1, 0, "ivert");
               }

            }
         };
         GlUtil.printGlErrorCritical();
         shaders.add(depthCubeShader);
         shaders.add(blackHoleShader);
         shaders.add(tubesShader);
         shaders.add(tubesStreamShader);
         shaders.add(pulseShader);
         if (!EngineSettings.G_ATMOSPHERE_SHADER.getCurrentState().equals("none")) {
            shaders.add(skyFromSpace);
            shaders.add(skyFromAtmo);
         }

         shaders.add(downsampler);
         shaders.add(downsamplerFirst);
         shaders.add(perpixelShader);
         shaders.add(projectileTrailShader);
         shaders.add(godRayShader);
         shaders.add(plasmaShader);
         shaders.add(silhouetteAlpha);
         shaders.add(bloomShader1Vert);
         shaders.add(bloomShader2Hor);
         shaders.add(shieldBubbleShader);
         shaders.add(silhouetteAlpha);
         shaders.add(fieldShader);
         shaders.add(simpleBeamShader);
         shaders.add(transporterShader);
         shaders.add(selectionShader);
         shaders.add(beamBoxShader);
         shaders.add(exaustShader);
         shaders.add(starFlareShader);
         shaders.add(cubeShieldShader);
         shaders.add(bgShader);
         shaders.add(solidSelectionShader);
         shaders.add(outlineShader);
         shaders.add(graphConnectionShader);
         shaders.add(lodCubeShader);
         shaders.add(lodCubeShaderTangent);
         shaders.add(lodCubeShaderNormalOff);
         shaders.add(lodCubeShaderShadow);
         shaders.add(colorBoxShader);
         shaders.add(cubeGroupShader);
         shaders.add(mineShader);
         shaders.add(silhouetteShader);
         shaders.add(bumpShader);
         shaders.add(scanlineShader);
         shaders.add(planetShader);
         shaders.add(atmosphereShader);
         shaders.add(bloomShader);
         shaders.add(sunShader);
         shaders.add(tunnelShader);
         shaders.add(spacedustShader);
         shaders.add(ocDistortion);
         shaders.add(lavaShader);
         shaders.add(projectileShader);
         shaders.add(guiTextureWrapperShader);
         shaders.add(projectileQuadShader);
         shaders.add(projectileQuadBloomShader);
         shaders.add(projectileBeamQuadShader);
         shaders.add(simpleColorShader);
         shaders.add(explosionShader);
         shaders.add(starShader);
         shaders.add(cubemapShader);
         shaders.add(bloomShaderPass1);
         shaders.add(bloomShaderPass2);
         shaders.add(bloomShaderPass3);
         shaders.add(bloomShaderPass4);
         shaders.add(bloomSimpleShader);
         shaders.add(shardShader);
         shaders.add(jumpOverlayShader);
         shaders.add(cubeShader13Simple);
         shaders.add(cubeShader13SimpleWhite);
         shaders.add(powerBarShader);
         shaders.add(powerBarShaderHor);
         shaders.add(depthCubeShader);
         if (EngineSettings.G_SHADOWS.isOn()) {
            shaders.add(shadowShaderCubes);
            shaders.add(shadowShaderCubesBlend);
            shaders.add(shad_view);
         }

         Iterator var0 = cubeShaders.values().iterator();

         while(var0.hasNext()) {
            Shader var1 = (Shader)var0.next();
            shaders.add(var1);
         }

      } catch (ResourceException var2) {
         var2.printStackTrace();
      }
   }

   private static void createCubeShaders() throws ResourceException {
      if (EngineSettings.G_SHADOWS.isOn()) {
         shadowShaderCubes = new Shader(DataUtil.dataPath + "/shader/cube/quads13/shadowcube.vsh", DataUtil.dataPath + "/shader/cube/quads13/shadowcube.fsh", new ShaderModifyInterface[]{new ShaderPreprocessor(new String[]{EngineSettings.G_SHADOWS.isOn() && EngineSettings.G_SHADOWS_VSM.isOn() ? "VSM" : null, GraphicsContext.isTextureArrayEnabled() ? "texarray" : null, EngineSettings.G_USE_VERTEX_LIGHTING_ONLY.isOn() ? "vertexLighting" : null, "owntangent", GraphicsContext.INTEGER_VERTICES ? "INTATT" : null, GraphicsContext.EXT_GPU_SHADER4() && EngineSettings.G_USE_SHADER4.isOn() ? "shader4" : null, GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "force130" : null, GraphicsContext.isIntel() && GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "intel130" : null})}) {
            public final void bindAttributes(int var1) {
               if (GraphicsContext.INTEGER_VERTICES) {
                  GL20.glBindAttribLocation(var1, 0, "ivert");
               }

            }
         };
         shadowShaderCubesBlend = new Shader(DataUtil.dataPath + "/shader/cube/quads13/shadowcube.vsh", DataUtil.dataPath + "/shader/cube/quads13/shadowcube.fsh", new ShaderModifyInterface[]{new ShaderPreprocessor(new String[]{EngineSettings.G_SHADOWS.isOn() && EngineSettings.G_SHADOWS_VSM.isOn() ? "VSM" : null, GraphicsContext.isTextureArrayEnabled() ? "texarray" : null, "blended", GraphicsContext.INTEGER_VERTICES ? "INTATT" : null, "owntangent", EngineSettings.G_USE_VERTEX_LIGHTING_ONLY.isOn() ? "vertexLighting" : null, GraphicsContext.EXT_GPU_SHADER4() && EngineSettings.G_USE_SHADER4.isOn() ? "shader4" : null, GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "force130" : null, GraphicsContext.isIntel() && GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "intel130" : null})}) {
            public final void bindAttributes(int var1) {
               if (GraphicsContext.INTEGER_VERTICES) {
                  GL20.glBindAttribLocation(var1, 0, "ivert");
               }

            }
         };
      }

      cubeShader13Simple = new Shader(DataUtil.dataPath + "/shader/cube/quads13/simplecube.vsh", DataUtil.dataPath + "/shader/cube/quads13/simplecube.fsh", new ShaderModifyInterface[]{new ShaderPreprocessor(new String[]{EngineSettings.G_SHADOWS.isOn() && EngineSettings.G_SHADOWS_VSM.isOn() ? "VSM" : null, CUBE_VERTEX_COMPONENTS > 2 ? "threeComp" : null, GraphicsContext.isTextureArrayEnabled() ? "texarray" : null, EngineSettings.G_SHADOWS.isOn() ? "shadow" : null, "owntangent", EngineSettings.G_USE_VERTEX_LIGHTING_ONLY.isOn() ? "vertexLighting" : null, EngineSettings.G_SHADOWS.isOn() ? EngineSettings.G_SHADOW_QUALITY.getCurrentState().toString() : null, GraphicsContext.INTEGER_VERTICES ? "INTATT" : null, EngineSettings.G_NORMAL_MAPPING.isOn() && GraphicsContext.isTextureArrayEnabled() ? "normaltexarray" : null, EngineSettings.G_NORMAL_MAPPING.isOn() && !GraphicsContext.isTextureArrayEnabled() ? "normalmap" : null, GraphicsContext.EXT_GPU_SHADER4() && EngineSettings.G_USE_SHADER4.isOn() ? "shader4" : null, GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "force130" : null, GraphicsContext.isIntel() && GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "intel130" : null})}) {
         public final void bindAttributes(int var1) {
            if (GraphicsContext.INTEGER_VERTICES) {
               GL20.glBindAttribLocation(var1, 0, "ivert");
            }

         }
      };
      cubeShader13SimpleWhite = new Shader(DataUtil.dataPath + "/shader/cube/quads13/shadowcube.vsh", DataUtil.dataPath + "/shader/cube/quads13/whitecube.fsh", new ShaderModifyInterface[]{new ShaderPreprocessor(new String[]{EngineSettings.G_SHADOWS.isOn() && EngineSettings.G_SHADOWS_VSM.isOn() ? "VSM" : null, CUBE_VERTEX_COMPONENTS > 2 ? "threeComp" : null, "owntangent", GraphicsContext.isTextureArrayEnabled() ? "texarray" : null, EngineSettings.G_SHADOWS.isOn() ? "shadow" : null, EngineSettings.G_USE_VERTEX_LIGHTING_ONLY.isOn() ? "vertexLighting" : null, EngineSettings.G_SHADOWS.isOn() ? EngineSettings.G_SHADOW_QUALITY.getCurrentState().toString() : null, GraphicsContext.INTEGER_VERTICES ? "INTATT" : null, EngineSettings.G_NORMAL_MAPPING.isOn() && GraphicsContext.isTextureArrayEnabled() ? "normaltexarray" : null, EngineSettings.G_NORMAL_MAPPING.isOn() && !GraphicsContext.isTextureArrayEnabled() ? "normalmap" : null, GraphicsContext.EXT_GPU_SHADER4() && EngineSettings.G_USE_SHADER4.isOn() ? "shader4" : null, GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "force130" : null, GraphicsContext.isIntel() && GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "intel130" : null})}) {
         public final void bindAttributes(int var1) {
            if (GraphicsContext.INTEGER_VERTICES) {
               GL20.glBindAttribLocation(var1, 0, "ivert");
            }

         }
      };
      createCubeShader();
      createCubeShader(ShaderLibrary.CubeShaderType.BLENDED);
      createCubeShader(ShaderLibrary.CubeShaderType.NO_SPOT_LIGHTS);
      createCubeShader(ShaderLibrary.CubeShaderType.NO_SPOT_LIGHTS, ShaderLibrary.CubeShaderType.BLENDED);
      createCubeShader(ShaderLibrary.CubeShaderType.VERTEX_LIGHTING);
      createCubeShader(ShaderLibrary.CubeShaderType.VERTEX_LIGHTING, ShaderLibrary.CubeShaderType.BLENDED);
      createCubeShader(ShaderLibrary.CubeShaderType.LIGHT_ALL);
      createCubeShader(ShaderLibrary.CubeShaderType.LIGHT_ALL, ShaderLibrary.CubeShaderType.BLENDED);
      createCubeShader(ShaderLibrary.CubeShaderType.LIGHT_ALL, ShaderLibrary.CubeShaderType.VERTEX_LIGHTING);
      createCubeShader(ShaderLibrary.CubeShaderType.LIGHT_ALL, ShaderLibrary.CubeShaderType.VERTEX_LIGHTING, ShaderLibrary.CubeShaderType.BLENDED);
      createCubeShader(ShaderLibrary.CubeShaderType.NO_SPOT_LIGHTS, ShaderLibrary.CubeShaderType.LIGHT_ALL);
      createCubeShader(ShaderLibrary.CubeShaderType.NO_SPOT_LIGHTS, ShaderLibrary.CubeShaderType.LIGHT_ALL, ShaderLibrary.CubeShaderType.BLENDED);
      createCubeShader(ShaderLibrary.CubeShaderType.VIRTUAL);
      createCubeShader(ShaderLibrary.CubeShaderType.BLENDED, ShaderLibrary.CubeShaderType.VIRTUAL);
      createCubeShader(ShaderLibrary.CubeShaderType.NO_SPOT_LIGHTS, ShaderLibrary.CubeShaderType.VIRTUAL);
      createCubeShader(ShaderLibrary.CubeShaderType.NO_SPOT_LIGHTS, ShaderLibrary.CubeShaderType.BLENDED, ShaderLibrary.CubeShaderType.VIRTUAL);
      createCubeShader(ShaderLibrary.CubeShaderType.VERTEX_LIGHTING, ShaderLibrary.CubeShaderType.VIRTUAL);
      createCubeShader(ShaderLibrary.CubeShaderType.VERTEX_LIGHTING, ShaderLibrary.CubeShaderType.BLENDED, ShaderLibrary.CubeShaderType.VIRTUAL);
      createCubeShader(ShaderLibrary.CubeShaderType.LIGHT_ALL, ShaderLibrary.CubeShaderType.VIRTUAL);
      createCubeShader(ShaderLibrary.CubeShaderType.LIGHT_ALL, ShaderLibrary.CubeShaderType.BLENDED, ShaderLibrary.CubeShaderType.VIRTUAL);
      createCubeShader(ShaderLibrary.CubeShaderType.LIGHT_ALL, ShaderLibrary.CubeShaderType.VERTEX_LIGHTING, ShaderLibrary.CubeShaderType.VIRTUAL);
      createCubeShader(ShaderLibrary.CubeShaderType.LIGHT_ALL, ShaderLibrary.CubeShaderType.VERTEX_LIGHTING, ShaderLibrary.CubeShaderType.BLENDED, ShaderLibrary.CubeShaderType.VIRTUAL);
      createCubeShader(ShaderLibrary.CubeShaderType.NO_SPOT_LIGHTS, ShaderLibrary.CubeShaderType.LIGHT_ALL, ShaderLibrary.CubeShaderType.VIRTUAL);
      createCubeShader(ShaderLibrary.CubeShaderType.NO_SPOT_LIGHTS, ShaderLibrary.CubeShaderType.LIGHT_ALL, ShaderLibrary.CubeShaderType.BLENDED, ShaderLibrary.CubeShaderType.VIRTUAL);
      createCubeShader(ShaderLibrary.CubeShaderType.SINGLE_DRAW, ShaderLibrary.CubeShaderType.BLENDED, ShaderLibrary.CubeShaderType.LIGHT_ALL);
      createCubeShader(ShaderLibrary.CubeShaderType.SINGLE_DRAW, ShaderLibrary.CubeShaderType.BLENDED);
   }

   public static Shader getCubeShader(int var0) {
      Shader var1;
      if ((var1 = (Shader)cubeShaders.get(var0)) == null) {
         String var6 = "";
         ShaderLibrary.CubeShaderType[] var2;
         int var3 = (var2 = ShaderLibrary.CubeShaderType.values()).length;

         for(int var4 = 0; var4 < var3; ++var4) {
            ShaderLibrary.CubeShaderType var5 = var2[var4];
            if ((var0 & var5.bit) == var5.bit) {
               var6 = var6 + var5.name() + ":" + var5.shaderVar;
            }
         }

         throw new IllegalArgumentException("CubeShader not found: " + var6);
      } else {
         var1.optionBits = var0;
         return var1;
      }
   }

   private static void createCubeShader(ShaderLibrary.CubeShaderType... var0) throws ResourceException {
      String[] var1;
      (var1 = new String[15 + var0.length])[0] = EngineSettings.G_SHADOWS.isOn() && EngineSettings.G_SHADOWS_VSM.isOn() ? "VSM" : null;
      var1[1] = CUBE_VERTEX_COMPONENTS > 2 ? "threeComp" : null;
      var1[2] = GraphicsContext.isTextureArrayEnabled() ? "texarray" : null;
      var1[3] = EngineSettings.G_SHADOWS.isOn() ? "shadow" : null;
      var1[4] = "owntangent";
      var1[5] = EngineSettings.G_USE_VERTEX_LIGHTING_ONLY.isOn() ? "vertexLighting" : null;
      var1[6] = EngineSettings.G_SHADOWS.isOn() ? EngineSettings.G_SHADOW_QUALITY.getCurrentState().toString() : null;
      var1[7] = EngineSettings.G_NORMAL_MAPPING.isOn() && GraphicsContext.isTextureArrayEnabled() ? "normaltexarray" : null;
      var1[8] = EngineSettings.G_NORMAL_MAPPING.isOn() && !GraphicsContext.isTextureArrayEnabled() ? "normalmap" : null;
      var1[9] = GraphicsContext.EXT_GPU_SHADER4() && EngineSettings.G_USE_SHADER4.isOn() ? "shader4" : null;
      var1[10] = GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "force130" : null;
      var1[11] = null;
      var1[12] = GraphicsContext.INTEGER_VERTICES ? "INTATT" : null;
      var1[13] = USE_CUBE_TEXTURE_EMISSION ? null : "noemission";
      var1[14] = GraphicsContext.isIntel() && GraphicsContext.forceOpenGl30() && EngineSettings.G_USE_SHADER4.isOn() ? "intel130" : null;
      int var2 = 0;

      for(int var3 = 0; var3 < var0.length; ++var3) {
         var1[var3 + 15] = var0[var3].shaderVar;
         var2 |= var0[var3].bit;
      }

      Shader var4 = new Shader(DataUtil.dataPath + "/shader/cube/quads13/cube-3rd.vsh", DataUtil.dataPath + "/shader/cube/quads13/cubeTArray.fsh", new ShaderModifyInterface[]{new ShaderPreprocessor(var1)}) {
         public final void bindAttributes(int var1) {
            if (GraphicsContext.INTEGER_VERTICES) {
               GL20.glBindAttribLocation(var1, 0, "ivert");
            }

         }
      };
      cubeShaders.put(var2, var4);
   }

   public static Shader getBoneShader(int var0, boolean var1) throws ResourceException {
      Shader var2;
      if ((var2 = var1 ? (Shader)skinningShaderSpot.get(var0) : (Shader)skinningShader.get(var0)) == null) {
         (var2 = addBoneShader(var0, var1)).recompiled = true;
      }

      return var2;
   }

   private static Shader addBoneShader(int var0, boolean var1) throws ResourceException {
      Shader var2 = new Shader(DataUtil.dataPath + "/shader/skin/skin-tex.vert.glsl", DataUtil.dataPath + "/shader/skin/skin-tex.frag.glsl", new ShaderModifyInterface[]{new ShaderReplaceDynamic("#NUM_BONES", String.valueOf(var0)), new ShaderPreprocessor(new String[]{EngineSettings.G_SHADOWS.isOn() ? "shadow" : null, EngineSettings.G_SHADOWS.isOn() ? EngineSettings.G_SHADOW_QUALITY.getCurrentState().toString() : null, EngineSettings.G_SHADOWS.isOn() && EngineSettings.G_SHADOWS_VSM.isOn() ? "VSM" : null})}) {
         public final void bindAttributes(int var1) {
            GL20.glBindAttribLocation(var1, 3, "indices");
            GL20.glBindAttribLocation(var1, 4, "weights");
         }
      };
      skinningShader.put(var0, var2);
      Shader var3 = new Shader(DataUtil.dataPath + "/shader/skin/skin-tex.vert.glsl", DataUtil.dataPath + "/shader/skin/skin-tex.frag.glsl", new ShaderModifyInterface[]{new ShaderReplaceDynamic("#NUM_BONES", String.valueOf(var0)), new ShaderPreprocessor(new String[]{EngineSettings.G_SHADOWS.isOn() ? "shadow" : null, EngineSettings.G_SHADOWS.isOn() ? EngineSettings.G_SHADOW_QUALITY.getCurrentState().toString() : null, EngineSettings.G_SHADOWS.isOn() && EngineSettings.G_SHADOWS_VSM.isOn() ? "VSM" : null, "nospotlight"})}) {
         public final void bindAttributes(int var1) {
            GL20.glBindAttribLocation(var1, 3, "indices");
            GL20.glBindAttribLocation(var1, 4, "weights");
         }
      };
      skinningShaderSpot.put(var0, var2);
      skinningShader.put(var0, var3);
      shaders.add(var3);
      shaders.add(var2);
      return var1 ? var2 : var3;
   }

   public static void reCompile(boolean var0) {
      GlUtil.printGlErrorCritical();
      Shader var1;
      Iterator var3;
      if (var0) {
         var3 = shaders.iterator();

         while(var3.hasNext()) {
            if ((var1 = (Shader)var3.next()) != null) {
               var1.cleanUp();
            }
         }

         skinningShader.clear();
         skinningShaderSpot.clear();
         shaders.clear();
         loadShaders();
      }

      var3 = shaders.iterator();

      while(var3.hasNext()) {
         var1 = (Shader)var3.next();

         try {
            if (var1 != null) {
               var1.reset();
               var1.compile();
            }
         } catch (ResourceException var2) {
            var2.printStackTrace();
         }
      }

      GlUtil.printGlErrorCritical();
   }

   public static ObjectCollection getCubeShaders() {
      return cubeShaders.values();
   }

   public static void createSpaceDustShader() throws ResourceException {
      if (spacedustShader != null) {
         spacedustShader = new Shader(DataUtil.dataPath + "/shader/spacedustExt/spacedust.vert.glsl", DataUtil.dataPath + "/shader/spacedustExt/spacedust.frag.glsl", new ShaderModifyInterface[0]);
      }

   }

   static {
      CUBE_VERTEX_COMPONENTS = EngineSettings.G_USE_TWO_COMPONENT_SHADER.isOn() ? 2 : 4;
      skinningShader = new Int2ObjectOpenHashMap();
      skinningShaderSpot = new Int2ObjectOpenHashMap();
      cubeShaders = new Int2ObjectOpenHashMap();
   }

   public static enum CubeShaderType {
      BLENDED(1, "blended"),
      VERTEX_LIGHTING(2, "vertexLighting"),
      LIGHT_ALL(4, "lightall"),
      SINGLE_DRAW(8, "singledraw"),
      NO_SPOT_LIGHTS(16, "nospotlights"),
      VIRTUAL(32, "virtual");

      public final int bit;
      private final String shaderVar;

      private CubeShaderType(int var3, String var4) {
         this.bit = var3;
         this.shaderVar = var4;
      }
   }
}
