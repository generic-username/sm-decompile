package org.schema.schine.graphicsengine.shader;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.FastMath;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;

public class HoffmannSky implements Shaderable {
   public Vector3f Position;
   private float _sunIntensity = 1.0F;
   private float _turbitity = 1.0F;
   private Vector3f _hGg = new Vector3f(0.9F, 0.9F, 0.9F);
   private float _inscatteringMultiplier = 1.0F;
   private float _betaRayMultiplier = 1.0F;
   private float _betaMieMultiplier = 5.0E-5F;
   private Vector3f _betaRPlusBetaM;
   private Vector3f _betaDashR;
   private Vector3f _betaDashM;
   private Vector3f _oneOverBetaRPlusBetaM;
   private Vector4f _multipliers;
   private Vector4f _sunColorAndIntensity;
   private Vector3f _betaRay;
   private Vector3f _betaDashRay;
   private Vector3f _betaMie;
   private Vector3f _betaDashMie;

   public HoffmannSky() {
      float[] var1 = new float[3];
      float[] var2 = new float[3];
      float[] var3 = new float[3];
      var1[0] = 1538461.5F;
      var1[1] = 1754386.0F;
      var1[2] = 2105263.2F;

      for(int var4 = 0; var4 < 3; ++var4) {
         var2[var4] = var1[var4] * var1[var4];
         var3[var4] = var2[var4] * var2[var4];
      }

      Vector3f var6 = new Vector3f(var2[0], var2[1], var2[2]);
      Vector3f var5 = new Vector3f(var3[0], var3[1], var3[2]);
      this._betaRay = new Vector3f(var5);
      this._betaRay.scale(1.241624E-30F);
      this._betaDashRay = new Vector3f(var5);
      this._betaDashRay.scale(7.410397E-32F);
      this._betaDashMie = new Vector3f(var6);
      this._betaDashMie.scale(5.6352514E-16F);
      var1 = new float[]{0.685F, 0.679F, 0.67F};
      var5 = new Vector3f(var1[0] * var2[0], var1[1] * var2[1], var1[2] * var2[2]);
      this._betaMie = new Vector3f(var5);
      this._betaMie.scale(3.540733E-15F);
   }

   private void computeAttenuation(float var1) {
      float var2 = 0.04608366F * this._turbitity - 0.04586026F;
      float[] var4 = new float[3];
      float var5 = 1.0F / (FastMath.cos(var1) + 0.15F * FastMath.pow(97.885F - var1 / 3.1415927F * 180.0F, -1.253F));
      float[] var6 = new float[]{0.65F, 0.57F, 0.475F};

      for(int var7 = 0; var7 < 3; ++var7) {
         var1 = FastMath.exp(-var5 * 0.008735F * FastMath.pow(var6[var7], -4.08F));
         float var3 = FastMath.exp(-var5 * var2 * FastMath.pow(var6[var7], -1.3F));
         var4[var7] = var1 * var3;
      }

      this._sunColorAndIntensity = new Vector4f(var4[0], var4[1], var4[2], this._sunIntensity * 100.0F);
   }

   public void onExit() {
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      Vector3f var2 = new Vector3f(0.0F, 0.001F, 0.0F);
      this.Position = AbstractScene.mainLight.getPos();
      float var3;
      if ((var3 = this.Position.dot(var2)) < -var2.y) {
         var3 = -1.0F;
      }

      var3 = FastMath.acos(var3);
      this.computeAttenuation(var3);
      this.setMaterialProperties();
      GlUtil.updateShaderVector3f(var1, "betaDashM", this._betaDashM);
      GlUtil.updateShaderVector3f(var1, "betaDashR", this._betaDashR);
      GlUtil.updateShaderVector3f(var1, "betaRPlusBetaM", this._betaRPlusBetaM);
      GlUtil.updateShaderVector3f(var1, "hGg", this._hGg);
      GlUtil.updateShaderVector4f(var1, "multipliers", this._multipliers);
      GlUtil.updateShaderVector3f(var1, "oneOverBetaRPlusBetaM", this._oneOverBetaRPlusBetaM);
      GlUtil.updateShaderVector4f(var1, "sunColorAndIntensity", this._sunColorAndIntensity);
   }

   private void setMaterialProperties() {
      Vector3f var1;
      (var1 = new Vector3f(this._betaRay)).scale(this._betaRayMultiplier);
      this._betaDashR = new Vector3f(this._betaDashRay);
      this._betaDashR.scale(this._betaRayMultiplier);
      Vector3f var2;
      (var2 = new Vector3f(this._betaMie)).scale(this._betaMieMultiplier);
      this._betaDashM = new Vector3f(this._betaDashMie);
      this._betaDashM.scale(this._betaMieMultiplier);
      this._betaRPlusBetaM = new Vector3f(var1);
      this._betaRPlusBetaM.add(var2);
      this._oneOverBetaRPlusBetaM = new Vector3f(1.0F / this._betaRPlusBetaM.x, 1.0F / this._betaRPlusBetaM.y, 1.0F / this._betaRPlusBetaM.z);
      this._multipliers = new Vector4f(this._inscatteringMultiplier, 0.0138F, 0.0113F, 0.008F);
   }

   public String ToString() {
      return "Atmosphere";
   }
}
