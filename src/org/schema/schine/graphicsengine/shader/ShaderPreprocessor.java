package org.schema.schine.graphicsengine.shader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ShaderPreprocessor implements ShaderModifyInterface {
   private static final int STATE_OUTSIDE_CONDITION = 0;
   private static final int STATE_TRUE_CONDITION = 1;
   private static final int STATE_FALSE_CONDITION = 2;
   private static final int STATE_POST_TRUE_CONDITION = 3;
   private Set fSymbols = new HashSet();
   private Matcher IF_DEF_MATCHER = Pattern.compile("#IFDEF\\s+\\w+").matcher("");
   private Matcher ELSE_IF_MATCHER = Pattern.compile("#ELSEIF\\s+\\w+").matcher("");
   private Matcher ELSE_MATCHER = Pattern.compile("#ELSE$|#ELSE\\W+").matcher("");
   private Matcher END_MATCHER = Pattern.compile("#ENDIF").matcher("");

   public ShaderPreprocessor(String... var1) {
      int var2 = (var1 = var1).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         String var4;
         if ((var4 = var1[var3]) != null && var4.length() > 0) {
            this.fSymbols.add(var4);
         }
      }

   }

   public void setSymbols(String var1) {
      String[] var4 = var1.split(",");

      for(int var2 = 0; var2 < var4.length; ++var2) {
         String var3;
         if ((var3 = var4[var2].trim()).length() > 0) {
            this.fSymbols.add(var3);
         }
      }

   }

   public String preProcessFile(String var1, String var2) {
      try {
         BufferedReader var3 = new BufferedReader(new StringReader(var1));
         StringBuffer var4 = new StringBuffer();
         String var5 = var3.readLine();
         byte var6 = 0;

         boolean var7;
         for(var7 = false; var5 != null; var5 = var3.readLine()) {
            boolean var12;
            boolean var13;
            label152: {
               boolean var8 = this.IF_DEF_MATCHER.reset(var5).find();
               boolean var9 = this.ELSE_IF_MATCHER.reset(var5).find();
               boolean var10 = this.ELSE_MATCHER.reset(var5).find();
               boolean var11 = this.END_MATCHER.reset(var5).find();
               var12 = var8 || var9 || var10 || var11;
               var13 = false;
               String var15;
               switch(var6) {
               case 0:
                  if (var8) {
                     var15 = var5.substring(this.IF_DEF_MATCHER.start(), this.IF_DEF_MATCHER.end()).split("\\s+")[1].trim();
                     if (this.fSymbols.contains(var15)) {
                        var6 = 1;
                     } else {
                        var6 = 2;
                     }
                  } else {
                     if (var9) {
                        throw new RuntimeException("#elseif encountered without corresponding #ifdef");
                     }

                     if (var10) {
                        throw new RuntimeException("#else encountered without corresponding #ifdef ()");
                     }

                     if (var11) {
                        throw new RuntimeException("#endif encountered without corresponding #ifdef");
                     }
                  }
                  break label152;
               case 1:
                  if (var10 || var9) {
                     var6 = 3;
                     break label152;
                  }

                  if (var11) {
                     var6 = 0;
                     break label152;
                  }

                  if (var8) {
                     throw new RuntimeException("illegal nested #ifdef");
                  }
               case 2:
                  if (var9) {
                     var15 = var5.substring(this.ELSE_IF_MATCHER.start(), this.ELSE_IF_MATCHER.end()).split("\\s+")[1].trim();
                     if (this.fSymbols.contains(var15)) {
                        var6 = 1;
                     } else {
                        var6 = 2;
                     }
                  } else {
                     if (var10) {
                        var6 = 1;
                        break label152;
                     }

                     if (var11) {
                        var6 = 0;
                        break label152;
                     }

                     if (var8) {
                        throw new RuntimeException("illegal nested #ifdef");
                     }
                  }
               case 3:
                  break;
               default:
                  break label152;
               }

               if (var11) {
                  var6 = 0;
               } else if (var8) {
                  throw new RuntimeException("illegal nested #ifdef");
               }
            }

            if (!var12 && (var6 == 0 || var6 == 1)) {
               if (var6 == 1 && var2 != null && var5.startsWith(var2)) {
                  var5 = var5.substring(var2.length());
               }

               var4.append(var5);
               var4.append("\n");
               var13 = true;
            }

            var7 = var7 || !var13;
         }

         if (!var7) {
            return var1;
         } else {
            return var4.toString();
         }
      } catch (IOException var14) {
         throw new RuntimeException(var14);
      }
   }

   public String handle(String var1) {
      return this.preProcessFile(var1, (String)null);
   }
}
