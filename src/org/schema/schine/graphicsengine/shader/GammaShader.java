package org.schema.schine.graphicsengine.shader;

import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class GammaShader implements Shaderable {
   private int siluetteFB;

   public void onExit() {
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderInt(var1, "fbTex", 0);
      GlUtil.updateShaderFloat(var1, "gammaInv", 1.0F / (Float)EngineSettings.G_GAMMA.getCurrentState());
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(3553, this.siluetteFB);
      GlUtil.glActiveTexture(33984);
      GlUtil.updateShaderInt(var1, "silTex", 1);
   }

   public void setFGSilouette(int var1) {
      this.siluetteFB = var1;
   }
}
