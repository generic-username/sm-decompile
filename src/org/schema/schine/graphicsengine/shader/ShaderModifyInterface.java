package org.schema.schine.graphicsengine.shader;

public interface ShaderModifyInterface {
   String handle(String var1);
}
