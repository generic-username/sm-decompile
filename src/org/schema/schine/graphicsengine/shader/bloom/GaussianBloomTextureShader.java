package org.schema.schine.graphicsengine.shader.bloom;

import org.lwjgl.util.vector.Matrix3f;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GLException;
import org.schema.schine.graphicsengine.core.GLFrame;

public class GaussianBloomTextureShader extends AbstractGaussianBloomShader {
   private BloomPass2 bloomPass2;
   private BloomPass3 bloomPass3;
   private BloomPass4 bloomPass4;

   public GaussianBloomTextureShader(FrameBufferObjects var1, FrameBufferObjects var2) throws GLException {
      this.fbo = var1;
      this.firstBlur = new FrameBufferObjects("GaussTexFirst", GLFrame.getWidth(), GLFrame.getHeight());
      this.firstBlur.initialize();
      this.secondBlur = new FrameBufferObjects("GaussTexSecond", GLFrame.getWidth(), GLFrame.getHeight());
      this.secondBlur.initialize();
      this.bloomPass3 = new BloomPass3(var1, var2, this.secondBlur, this);
      this.bloomPass4 = new BloomPass4(var1, var2, this.secondBlur, this);
   }

   void calculateMatrices() {
      Matrix4f var1 = new Matrix4f(Controller.projectionMatrix);
      Matrix4f var2 = new Matrix4f(Controller.modelviewMatrix);
      Matrix4f var3 = new Matrix4f();
      Matrix4f.mul(var1, var2, var3);
      mvpBuffer.rewind();
      var3.store(mvpBuffer);
      mvpBuffer.rewind();
      Matrix3f var4;
      (var4 = new Matrix3f()).m00 = var2.m00;
      var4.m10 = var2.m10;
      var4.m20 = var2.m20;
      var4.m01 = var2.m01;
      var4.m11 = var2.m11;
      var4.m21 = var2.m21;
      var4.m02 = var2.m02;
      var4.m12 = var2.m12;
      var4.m22 = var2.m22;
      var4.invert();
      normalBuffer.rewind();
      var4.store(normalBuffer);
      normalBuffer.rewind();
   }

   public void draw(int var1) {
      this.calculateMatrices();
      this.bloomPass2.draw(var1);
      this.bloomPass3.draw(var1);
      this.bloomPass4.draw(var1);
   }
}
