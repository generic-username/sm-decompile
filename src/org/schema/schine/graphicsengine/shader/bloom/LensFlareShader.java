package org.schema.schine.graphicsengine.shader.bloom;

import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class LensFlareShader implements Shaderable {
   static int texSlot = 33991;
   static int texSlotNum = 7;
   static int texSlotDirt = 33993;
   static int texSlotDirtNum = 9;
   public Vector3f lightPosOnScreen = new Vector3f();
   private float exposure = 1.0F;
   private float decay = 0.3F;
   private float desity = 3.0F;
   private float weight = 1.0F;
   private int silouetteTexId = -1;
   private float lx;
   private float ly;

   public int getSilouetteTexId() {
      return this.silouetteTexId;
   }

   public void setSilouetteTexId(int var1) {
      this.silouetteTexId = var1;
   }

   public void onExit() {
      GlUtil.glActiveTexture(texSlot);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33984);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      assert this.getSilouetteTexId() >= 0;

      GlUtil.updateShaderFloat(var1, "exposure", this.exposure);
      GlUtil.updateShaderFloat(var1, "decay", this.decay);
      GlUtil.updateShaderFloat(var1, "desity", this.desity);
      GlUtil.updateShaderFloat(var1, "weight", this.weight);
      GlUtil.updateShaderVector2f(var1, "lightPositionOnScreen", this.lx, this.ly);
      GlUtil.glActiveTexture(texSlot);
      GlUtil.glBindTexture(3553, this.silouetteTexId);
      GlUtil.updateShaderInt(var1, "firstPass", texSlotNum);
      GlUtil.glActiveTexture(texSlotDirt);
      GlUtil.glBindTexture(3553, Controller.getResLoader().getSprite("dirtlense").getMaterial().getTexture().getTextureId());
      GlUtil.updateShaderInt(var1, "Dirt", texSlotDirtNum);
      GlUtil.glActiveTexture(33984);
   }

   public boolean update() {
      this.lx = this.lightPosOnScreen.x / (float)GLFrame.getWidth();
      this.ly = 1.0F - this.lightPosOnScreen.y / (float)GLFrame.getHeight();
      return true;
   }
}
