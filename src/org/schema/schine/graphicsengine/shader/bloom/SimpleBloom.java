package org.schema.schine.graphicsengine.shader.bloom;

import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class SimpleBloom extends AbstractGaussianBloomShader implements Shaderable {
   private int textureId;

   public int getTextureId() {
      return this.textureId;
   }

   public void setTextureId(int var1) {
      this.textureId = var1;
   }

   public void onExit() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      this.calculateMatrices();
      GlUtil.updateShaderMat4(var1, "MVP", mvpBuffer, false);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, this.getTextureId());
      GlUtil.updateShaderInt(var1, "tex", 0);
   }
}
