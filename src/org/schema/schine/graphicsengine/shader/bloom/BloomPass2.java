package org.schema.schine.graphicsengine.shader.bloom;

import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GLException;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class BloomPass2 implements Shaderable {
   private FrameBufferObjects fbo;
   private FrameBufferObjects firstBlur;
   private GaussianBloomShader gShader;

   public BloomPass2(FrameBufferObjects var1, FrameBufferObjects var2, GaussianBloomShader var3) throws GLException {
      this.fbo = var1;
      this.firstBlur = var2;
      this.gShader = var3;
   }

   public void draw(int var1) {
      Shader var2;
      (var2 = ShaderLibrary.bloomShaderPass2).setShaderInterface(this);
      this.firstBlur.enable();
      GL11.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
      GL11.glClear(16640);
      this.fbo.draw(var2, var1);
      this.firstBlur.disable();
   }

   public void onExit() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33984);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      this.gShader.calculateMatrices();
      GlUtil.updateShaderMat4(var1, "MVP", AbstractGaussianBloomShader.mvpBuffer, false);
      GlUtil.updateShaderFloat(var1, "LumThresh", 1.0F - (Float)EngineSettings.F_BLOOM_INTENSITY.getCurrentState());
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, this.fbo.getTexture());
      GlUtil.updateShaderInt(var1, "RenderTex", 0);
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(3553, this.gShader.silhouetteTextureId);
      GlUtil.updateShaderInt(var1, "SilhouetteTex", 1);
      GlUtil.glActiveTexture(33984);
   }

   public void drawFbo(int var1) {
      this.firstBlur.draw(var1);
   }

   public void cleanUp() {
   }
}
