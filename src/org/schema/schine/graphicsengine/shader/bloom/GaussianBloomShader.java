package org.schema.schine.graphicsengine.shader.bloom;

import org.lwjgl.opengl.GL20;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GLException;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.network.StateInterface;

public class GaussianBloomShader extends AbstractGaussianBloomShader {
   private BloomPass2 bloomPass2;
   private BloomPass3 bloomPass3;
   private BloomPass4 bloomPass4;

   public GaussianBloomShader(FrameBufferObjects var1) {
      this.fbo = var1;

      try {
         this.createFrameBuffers();
      } catch (GLException var2) {
         var2.printStackTrace();
      }
   }

   public void applyWeightMult(float var1) {
      this.weightMult = var1;
      this.bloomPass3.applyWeights();
      this.bloomPass4.applyWeights();
   }

   private void createFrameBuffers() throws GLException {
      if (this.firstBlur != null) {
         this.firstBlur.cleanUp();
      }

      if (this.secondBlur != null) {
         this.secondBlur.cleanUp();
      }

      this.firstBlur = new FrameBufferObjects("GaussFirst", GLFrame.getWidth() / 2, GLFrame.getHeight() / 2);
      this.firstBlur.initialize();
      this.secondBlur = new FrameBufferObjects("GaussSecond", GLFrame.getWidth() / 2, GLFrame.getHeight() / 2);
      this.secondBlur.initialize();
      this.bloomPass2 = new BloomPass2(this.fbo, this.firstBlur, this);
      this.bloomPass3 = new BloomPass3(this.fbo, this.firstBlur, this.secondBlur, this);
      this.bloomPass4 = new BloomPass4(this.fbo, this.firstBlur, this.secondBlur, this);
   }

   public void initialize(int var1) {
   }

   public void draw(int var1) {
      if (GraphicsContext.isScreenSettingChanging()) {
         try {
            this.createFrameBuffers();
         } catch (GLException var3) {
            var3.printStackTrace();
            GLFrame.processErrorDialogException((Exception)var3, (StateInterface)null);
         }
      }

      this.calculateMatrices();
      this.bloomPass2.draw(var1);
      this.bloomPass3.draw(var1);
      this.bloomPass4.draw(var1);
      GL20.glUseProgram(0);
   }

   public void draw(FrameBufferObjects var1, int var2) {
      this.calculateMatrices();
      this.bloomPass2.draw(0);
      this.bloomPass3.draw(0);
      var1.enable();
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      this.bloomPass4.draw(var2);
      GlUtil.glDisable(3042);
      var1.disable();
   }

   public void cleanUp() {
      if (this.firstBlur != null) {
         this.firstBlur.cleanUp();
      }

      if (this.secondBlur != null) {
         this.secondBlur.cleanUp();
      }

   }
}
