package org.schema.schine.graphicsengine.shader.bloom;

import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GLException;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class BloomPass4 implements Shaderable {
   private FrameBufferObjects fbo;
   private FrameBufferObjects firstBlur;
   private FrameBufferObjects secondBlur;
   private float[] gaussianWeights;
   private AbstractGaussianBloomShader gShader;
   private FloatBuffer gaussBuffer = BufferUtils.createFloatBuffer(10);

   public BloomPass4(FrameBufferObjects var1, FrameBufferObjects var2, FrameBufferObjects var3, AbstractGaussianBloomShader var4) throws GLException {
      this.fbo = var1;
      this.firstBlur = var2;
      this.secondBlur = var3;
      this.gaussianWeights = AbstractGaussianBloomShader.calculateGaussianWeightsNew(9, 1.0D);
      this.gaussBuffer.rewind();

      for(int var5 = 9; var5 >= 0; --var5) {
         this.gaussBuffer.put(this.gaussianWeights[var5]);
      }

      this.gaussBuffer.rewind();
      this.gShader = var4;
   }

   public void applyWeights() {
      for(int var1 = 0; var1 < this.gaussBuffer.limit(); ++var1) {
         this.gaussBuffer.put(var1, this.gaussBuffer.get(var1) * this.gShader.weightMult);
      }

   }

   public void cleanUp() {
   }

   public void draw(int var1) {
      Shader var2;
      (var2 = ShaderLibrary.bloomShaderPass4).setShaderInterface(this);
      this.fbo.draw(var2, var1);
   }

   public void onExit() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33984);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderFloat(var1, "gammaInv", 1.0F / (Float)EngineSettings.G_GAMMA.getCurrentState());
      GlUtil.updateShaderMat4(var1, "MVP", AbstractGaussianBloomShader.mvpBuffer, false);
      GlUtil.updateShaderFloat(var1, "Width", (float)this.firstBlur.getWidth());
      this.gaussBuffer.rewind();
      GlUtil.updateShaderFloats1(var1, "Weight", this.gaussBuffer);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, this.fbo.getTexture());
      GlUtil.updateShaderInt(var1, "RenderTex", 0);
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(3553, this.secondBlur.getTexture());
      GlUtil.updateShaderInt(var1, "BlurTex", 1);
      GlUtil.glActiveTexture(33984);
   }
}
