package org.schema.schine.graphicsengine.shader.bloom;

import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GLException;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class BloomPass3 implements Shaderable {
   private FrameBufferObjects fbo;
   private FrameBufferObjects firstBlur;
   private FrameBufferObjects secondBlur;
   private float[] gaussianWeights;
   private AbstractGaussianBloomShader gShader;
   private FloatBuffer gaussBuffer = BufferUtils.createFloatBuffer(10);

   public BloomPass3(FrameBufferObjects var1, FrameBufferObjects var2, FrameBufferObjects var3, AbstractGaussianBloomShader var4) throws GLException {
      this.fbo = var1;
      this.firstBlur = var2;
      this.secondBlur = var3;
      this.gaussianWeights = AbstractGaussianBloomShader.calculateGaussianWeightsNew(9, 1.0D);
      this.gaussBuffer.rewind();

      for(int var5 = 9; var5 >= 0; --var5) {
         this.gaussBuffer.put(this.gaussianWeights[var5]);
      }

      this.gaussBuffer.rewind();
      this.gShader = var4;
   }

   public void applyWeights() {
      for(int var1 = 0; var1 < this.gaussBuffer.limit(); ++var1) {
         this.gaussBuffer.put(var1, this.gaussBuffer.get(var1) * this.gShader.weightMult);
      }

   }

   public void draw(int var1) {
      Shader var2;
      (var2 = ShaderLibrary.bloomShaderPass3).setShaderInterface(this);
      this.secondBlur.enable();
      GL11.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
      GL11.glClear(16640);
      this.fbo.draw(var2, var1);
      this.secondBlur.disable();
   }

   public void onExit() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33984);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderMat4(var1, "MVP", AbstractGaussianBloomShader.mvpBuffer, false);
      GlUtil.updateShaderFloat(var1, "Height", (float)this.firstBlur.getHeight());
      this.gaussBuffer.rewind();
      GlUtil.updateShaderFloats1(var1, "Weight", this.gaussBuffer);
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(3553, this.firstBlur.getTexture());
      GlUtil.updateShaderInt(var1, "BlurTex", 1);
      GlUtil.glActiveTexture(33984);
   }
}
