package org.schema.schine.graphicsengine.shader;

public class ShaderException extends Exception {
   private static final long serialVersionUID = 1L;
   public String path;
   public String info;
   public String source;

   public ShaderException(String var1, String var2, String var3) {
      super("\n" + var1 + " \n\n" + var2 + "\n");
      this.source = var3;
      this.path = var1;
      this.info = var2;
   }
}
