package org.schema.schine.graphicsengine.shader.targetBloom;

import java.lang.reflect.InvocationTargetException;
import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GLException;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.bloom.AbstractGaussianBloomShader;
import org.schema.schine.network.StateInterface;

public class Blurrer {
   private FrameBufferObjects inputScene;
   private final FrameBufferObjects toBloom;
   private FrameBufferObjects vertical;
   private FrameBufferObjects output;
   private Blurrer.BloomLevel vertLevel;
   private Blurrer.BloomLevel horLevel;
   private int ocMode;
   private int verticalRadius = 19;
   private int horizontalRadius = 29;
   private int maxRadius;
   private FrameBufferObjects[] downsamplers;
   private int outputReuseDepthBuffer;
   private static FloatBuffer gaussBuffer;

   public Blurrer(FrameBufferObjects var1, int var2) {
      this.toBloom = var1;
      this.ocMode = var2;
      this.vertLevel = new Blurrer.BloomLevel(this.verticalRadius);
      this.horLevel = new Blurrer.BloomLevel(this.horizontalRadius);
      this.maxRadius = Math.max(this.verticalRadius, this.horizontalRadius);
      if (gaussBuffer == null || gaussBuffer.capacity() != this.maxRadius) {
         if (gaussBuffer != null) {
            try {
               GlUtil.destroyDirectByteBuffer(gaussBuffer);
            } catch (IllegalArgumentException var3) {
               var3.printStackTrace();
            } catch (IllegalAccessException var4) {
               var4.printStackTrace();
            } catch (InvocationTargetException var5) {
               var5.printStackTrace();
            } catch (SecurityException var6) {
               var6.printStackTrace();
            } catch (NoSuchMethodException var7) {
               var7.printStackTrace();
            }
         }

         gaussBuffer = BufferUtils.createFloatBuffer((this.maxRadius << 1) + 1);
      }

   }

   public void initialize() {
      try {
         this.createFrameBuffers();
      } catch (GLException var1) {
         var1.printStackTrace();
      }
   }

   public FrameBufferObjects blur(FrameBufferObjects var1) {
      this.inputScene = var1;
      if (GraphicsContext.isScreenSettingChanging()) {
         try {
            this.createFrameBuffers();
         } catch (GLException var2) {
            var2.printStackTrace();
            GLFrame.processErrorDialogException((Exception)var2, (StateInterface)null);
         }
      }

      GlUtil.glDisable(3042);
      var1 = this.downsample(this.toBloom);
      this.verticalPass(var1);
      this.horizontalPass();
      return Keyboard.isKeyDown(68) ? var1 : this.output;
   }

   private FrameBufferObjects downsample(FrameBufferObjects var1) {
      FrameBufferObjects var2 = var1;

      for(int var3 = 0; var3 < this.downsamplers.length; ++var3) {
         this.downsamplers[var3].enable();
         Shader var4;
         if (var2 == var1) {
            var4 = ShaderLibrary.downsampler;
         } else {
            var4 = ShaderLibrary.downsamplerFirst;
         }

         var4.loadWithoutUpdate();
         GlUtil.glActiveTexture(33987);
         GlUtil.glBindTexture(3553, var2.getTexture());
         GlUtil.updateShaderInt(var4, "inputTexture", 3);
         if (var2 == var1) {
            GlUtil.glActiveTexture(33988);
            GlUtil.glBindTexture(3553, var2.getDepthTextureID());
            GlUtil.updateShaderInt(var4, "depthTexture", 4);
         }

         GlUtil.glActiveTexture(33984);
         GlUtil.updateShaderFloat(var4, "inputTextureWidth", (float)var2.getWidth());
         GlUtil.updateShaderFloat(var4, "inputTextureHeight", (float)var2.getHeight());
         GL11.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
         GL11.glClear(16640);
         var2.draw(this.ocMode);
         var4.unloadWithoutExit();
         this.downsamplers[var3].disable();
         var2 = this.downsamplers[var3];
         GlUtil.glActiveTexture(33987);
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glActiveTexture(33988);
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glActiveTexture(33984);
      }

      return var2;
   }

   private void verticalPass(FrameBufferObjects var1) {
      Shader var2 = ShaderLibrary.bloomShader1Vert;
      Blurrer.BloomLevel var3 = this.vertLevel;
      var2.loadWithoutUpdate();
      GlUtil.updateShaderFloat(var2, "Height", (float)var1.getHeight());
      gaussBuffer.clear();

      for(int var4 = var3.weights.length / 2 - 1; var4 >= 0; --var4) {
         gaussBuffer.put(var3.weights[var4]);
      }

      gaussBuffer.rewind();
      GlUtil.updateShaderFloats1(var2, "Weight", gaussBuffer);
      GlUtil.glActiveTexture(33987);
      GlUtil.glBindTexture(3553, var1.getTexture());
      GlUtil.updateShaderInt(var2, "BlurTex", 3);
      GlUtil.glActiveTexture(33984);
      this.vertical.enable();
      GL11.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
      GL11.glClear(16640);
      var1.draw(var2, this.ocMode);
      this.vertical.disable();
      var2.unloadWithoutExit();
   }

   private void horizontalPass() {
      Shader var1 = ShaderLibrary.bloomShader2Hor;
      Blurrer.BloomLevel var2 = this.horLevel;
      FrameBufferObjects var3 = this.vertical;
      var1.loadWithoutUpdate();
      GlUtil.updateShaderFloat(var1, "gammaInv", 1.0F / (Float)EngineSettings.G_GAMMA.getCurrentState());
      GlUtil.updateShaderFloat(var1, "Width", (float)this.vertical.getWidth());
      gaussBuffer.clear();

      for(int var4 = var2.weights.length / 2 - 1; var4 >= 0; --var4) {
         gaussBuffer.put(var2.weights[var4]);
      }

      gaussBuffer.rewind();
      GlUtil.updateShaderFloats1(var1, "Weight", gaussBuffer);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, this.inputScene.getTexture());
      GlUtil.updateShaderInt(var1, "RenderTex", 0);
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(3553, var3.getTexture());
      GlUtil.updateShaderInt(var1, "BlurTex", 1);
      GlUtil.glActiveTexture(33984);
      this.output.enable();
      GL11.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
      GL11.glClear(16640);
      this.inputScene.draw(var1, this.ocMode);
      this.output.disable();
      var1.unloadWithoutExit();
   }

   public void cleanUp() {
      if (this.output != null) {
         this.output.cleanUp();
      }

      if (this.vertical != null) {
         this.vertical.cleanUp();
      }

      if (this.downsamplers != null) {
         for(int var1 = 0; var1 < this.downsamplers.length; ++var1) {
            if (this.downsamplers[var1] != null) {
               this.downsamplers[var1].cleanUp();
               this.downsamplers[var1] = null;
            }
         }
      }

   }

   public void createFrameBuffers() throws GLException {
      this.cleanUp();
      this.output = new FrameBufferObjects("BlurrerOutput", GLFrame.getWidth(), GLFrame.getHeight());
      this.output.setReuseDepthBuffer(this.outputReuseDepthBuffer);
      this.output.initialize();
      this.downsamplers = new FrameBufferObjects[3];
      int var1 = 2;

      for(int var2 = 0; var1 < 16; ++var2) {
         this.downsamplers[var2] = new FrameBufferObjects("BlurrerDownsample" + var2, GLFrame.getWidth() / var1, GLFrame.getHeight() / var1);
         this.downsamplers[var2].setWithDepthAttachment(false);
         this.downsamplers[var2].initialize();
         var1 <<= 1;
      }

      this.vertical = new FrameBufferObjects("BlurrerVertical", GLFrame.getWidth() / 16, GLFrame.getHeight() / 16);
      this.vertical.setWithDepthAttachment(false);
      this.vertical.initialize();
   }

   public FrameBufferObjects getInputScene() {
      return this.inputScene;
   }

   public void setInputScene(FrameBufferObjects var1) {
      this.inputScene = var1;
   }

   public void setReuseRenderDepthBuffer(int var1) {
      this.outputReuseDepthBuffer = var1;
   }

   class BloomLevel {
      private float[] weights;

      public BloomLevel(int var2) {
         this.weights = AbstractGaussianBloomShader.calculateGaussianWeightsNew(var2, 0.1D);
         float var3 = 0.0F;

         for(var2 = 0; var2 < this.weights.length; ++var2) {
            var3 = Math.max(var3, this.weights[var2]);
         }

         for(var2 = 0; var2 < this.weights.length; ++var2) {
         }

      }
   }
}
