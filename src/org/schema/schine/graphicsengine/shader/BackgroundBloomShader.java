package org.schema.schine.graphicsengine.shader;

import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GlUtil;

public class BackgroundBloomShader implements Shaderable {
   private FrameBufferObjects fbo;

   public BackgroundBloomShader(FrameBufferObjects var1) {
      this.fbo = var1;
   }

   public void onExit() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.glEnable(3553);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, this.fbo.getTexture());
      GlUtil.updateShaderInt(var1, "bgl_RenderedTexture", 0);
   }
}
