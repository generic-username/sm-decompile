package org.schema.schine.graphicsengine.texture;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBufferByte;
import java.awt.image.ImageObserver;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;
import javax.imageio.ImageIO;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL30;
import org.newdawn.slick.opengl.LoadableImageData;
import org.newdawn.slick.opengl.PNGImageData;
import org.schema.common.FastMath;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.ResourceLoader;

public class TextureLoader {
   public static final boolean MIPMAP_NORMAL;
   public static final boolean MIPMAP_ARRAY;
   public static final int MIPMAP_LVLS;
   public static final boolean COMPRESSED_ARRAYS;
   public static final boolean COMPRESSED_REGULAR;
   public static final boolean COMPRESSED_SHADOW;
   public static final boolean COMPRESSED_FBO;
   private HashMap table = new HashMap();
   private static ColorModel glAlphaColorModel;
   private static ColorModel glColorModel;

   public static final int createFringeMap() {
      ByteBuffer var0 = GlUtil.getDynamicByteBuffer(768, 0);

      for(int var1 = 0; var1 < 256; ++var1) {
         var0.put(var1 * 3, (byte)((int)(0.5F * (FastMath.sin(6.2831855F * (float)var1 * 0.234375F / 6.0F + 1.5707964F + 0.0F) + 1.0F) * 255.0F)));
         var0.put(var1 * 3 + 1, (byte)((int)(0.5F * (FastMath.sin(6.2831855F * (float)var1 * 0.234375F / 5.0F + 1.5707964F + 0.0F) + 1.0F) * 255.0F)));
         var0.put(var1 * 3 + 2, (byte)((int)(0.5F * (FastMath.sin(6.2831855F * (float)var1 * 0.234375F / 4.0F + 1.5707964F + 0.0F) + 1.0F) * 255.0F)));
      }

      IntBuffer var2;
      (var2 = BufferUtils.createIntBuffer(1)).rewind();
      GL11.glGenTextures(var2);
      GlUtil.glBindTexture(3553, var2.get(0));
      GL11.glPixelStorei(3317, 1);
      GL11.glTexParameteri(3553, 10242, 33071);
      GL11.glTexParameteri(3553, 10243, 33071);
      GL11.glTexParameteri(3553, 10240, 9729);
      GL11.glTexParameteri(3553, 10241, 9987);
      if (MIPMAP_NORMAL) {
         GL11.glTexParameteri(3553, 33169, 1);
      }

      GL11.glTexImage2D(3553, 0, 32849, 256, 1, 0, 6407, 5121, var0);
      return var2.get(0);
   }

   public static Texture get1DTexture(int var0, ByteBuffer var1) {
      var1.rewind();
      GlUtil.glEnable(3552);
      IntBuffer var2;
      GL11.glGenTextures(var2 = BufferUtils.createIntBuffer(1));
      GlUtil.glBindTexture(3552, var2.get(0));
      GL11.glTexParameteri(3552, 10241, 9729);
      GL11.glTexParameteri(3552, 10240, 9729);
      GL11.glTexImage1D(3552, 0, 6408, var0, 0, 6408, 5121, var1);
      GlUtil.glDisable(3552);
      return new Texture(3552, var2.get(0), "1dTexture");
   }

   public static Texture getEmptyTexture(int var0, int var1) {
      GlUtil.glEnable(3553);
      ByteBuffer var2;
      ByteBuffer var10000 = var2 = GlUtil.getDynamicByteBuffer(var0 * var1 << 2, 0);
      var10000.limit(var10000.capacity());
      IntBuffer var3;
      GL11.glGenTextures(var3 = BufferUtils.createIntBuffer(1));
      GlUtil.glBindTexture(3553, var3.get(0));
      Controller.loadedTextures.add(var3.get(0));
      if (!MIPMAP_NORMAL) {
         GL11.glTexParameteri(3553, 10241, 9729);
         GL11.glTexParameteri(3553, 10240, 9729);
      } else {
         GlUtil.printGlErrorCritical();
         GL11.glTexParameteri(3553, 10241, 9987);
         GlUtil.printGlErrorCritical();
         GL11.glTexParameteri(3553, 10240, 9729);
         GlUtil.printGlErrorCritical();
         GL11.glTexParameteri(3553, 33169, 1);
      }

      GL11.glTexImage2D(3553, 0, 6408, var0, var1, 0, 6408, 5121, var2);
      GlUtil.printGlErrorCritical();
      GlUtil.glDisable(3553);
      return new Texture(3553, var3.get(0), "emptyTexture");
   }

   private static ByteBuffer getImageData(BufferedImage var0) {
      byte[] var1;
      ByteBuffer var2;
      (var2 = GlUtil.getDynamicByteBuffer((var1 = ((DataBufferByte)var0.getRaster().getDataBuffer()).getData()).length, 0)).order(ByteOrder.nativeOrder());
      var2.put(var1, 0, var1.length);
      var2.flip();
      return var2;
   }

   public static boolean isPowerOfTwo(int var0) {
      int var1;
      for(var1 = 1; var1 < var0; var1 <<= 1) {
      }

      return var1 == var0;
   }

   private static int createTextureID() {
      GL11.glGenTextures(GlUtil.getIntBuffer1());
      Controller.loadedTextures.add(GlUtil.getIntBuffer1().get(0));
      return GlUtil.getIntBuffer1().get(0);
   }

   public static int getTextureArray(String[] var0, int var1, int var2, int var3, boolean var4) throws IOException, ResourceException {
      var3 = createTextureID();
      GlUtil.glBindTexture(35866, var3);
      PNGImageData[] var5 = new PNGImageData[var0.length];
      var1 = 0;

      for(int var6 = 0; var6 < var5.length; ++var6) {
         var5[var6] = new PNGImageData();
         InputStream var7 = ResourceLoader.resourceUtil.getResourceAsInputStream(var0[var6]);
         var1 = (int)((long)var1 + var5[var6].getSizeBef(var7));
         var7.close();
      }

      ByteBuffer var13;
      (var13 = GlUtil.getDynamicByteBuffer(var1, 0)).rewind();
      int var14 = -1;
      String var11 = "";

      for(int var8 = 0; var8 < var5.length; ++var8) {
         InputStream var9 = ResourceLoader.resourceUtil.getResourceAsInputStream(var0[var8]);
         var5[var8].loadImage(var9);
         if (var14 > 0 && var14 != var5[var8].getDepth()) {
            throw new ResourceException(var0[var8] + " has a different color depth as another file " + var11 + " in the same array: \nother: " + var14 + "; this: " + var5[var8].getDepth() + "\n\nAll textures of this series must either be all RGB or all RGBA, and have the same resolution");
         }

         var11 = var0[var8];
         var14 = var5[var8].getDepth();
         var13.put(var5[var8].getImageBufferData());
         var9.close();
      }

      var13.flip();
      if (MIPMAP_ARRAY && var4) {
         GL11.glTexParameteri(35866, 33085, MIPMAP_LVLS);
         GL11.glTexParameteri(35866, 10241, 9987);
         GL11.glTexParameteri(35866, 10240, hasLinMagForBlock((String)null) ? 9729 : 9728);
         GL11.glTexParameteri(35866, 33169, 1);
      } else {
         GL11.glTexParameteri(35866, 10241, var2);
         GL11.glTexParameteri(35866, 10240, EngineSettings.G_MAG_FILTER_LINEAR_GUI.isOn() ? 9729 : 9728);
         GL11.glTexParameteri(32879, 33085, 0);
         GL11.glTexParameteri(32879, 33169, 0);
      }

      char var12;
      short var15;
      if (var5[0].getDepth() == 32) {
         var15 = 6408;
         var12 = 6408;
         if (COMPRESSED_ARRAYS && GraphicsContext.getCapabilities().GL_ARB_texture_compression) {
            System.err.println("Using texture array compression");
            var12 = '蓮';
         } else if (COMPRESSED_ARRAYS) {
            System.err.println("ERROR: Texture compression not supported by hardware");
         }
      } else {
         var15 = 6407;
         var12 = 6407;
         if (COMPRESSED_ARRAYS && GraphicsContext.getCapabilities().GL_ARB_texture_compression) {
            System.err.println("Using texture array compression");
            var12 = '蓭';
         } else if (COMPRESSED_ARRAYS) {
            System.err.println("ERROR: Texture compression not supported by hardware");
         }
      }

      GlUtil.printGlErrorCritical();

      try {
         GL12.glTexImage3D(35866, 0, var12, var5[0].getWidth(), var5[0].getHeight(), var5.length, 0, var15, 5121, var13);
      } catch (RuntimeException var10) {
         System.err.println("EXCEPTION HAPPENED WITH RESOURCE: " + Arrays.toString(var0));
         throw var10;
      }

      if (COMPRESSED_ARRAYS && GL11.glGetTexLevelParameteri(35866, 0, 34465) != 1) {
         System.err.println("Exception: Texture Compression failed: " + Arrays.toString(var0));

         assert false;
      }

      boolean var10000 = MIPMAP_ARRAY;
      GlUtil.printGlErrorCritical();
      return var3;
   }

   public static int getTextureArrayAdv(String[] var0, int var1, int var2, int var3, boolean var4) throws IOException, ResourceException {
      var3 = createTextureID();
      GlUtil.glBindTexture(35866, var3);
      if (MIPMAP_ARRAY && var4) {
         GL11.glTexParameteri(35866, 33085, MIPMAP_LVLS);
         GL11.glTexParameteri(35866, 10241, 9987);
         GL11.glTexParameteri(35866, 10240, hasLinMagForBlock((String)null) ? 9729 : 9728);
         GL11.glTexParameteri(35866, 33169, 0);
      } else {
         GL11.glTexParameteri(35866, 10241, var2);
         GL11.glTexParameteri(35866, 10240, EngineSettings.G_MAG_FILTER_LINEAR_GUI.isOn() ? 9729 : 9728);
         GL11.glTexParameteri(32879, 33085, 0);
         GL11.glTexParameteri(32879, 33169, 0);
      }

      PNGImageData[] var13 = new PNGImageData[var0.length];
      int var14 = -1;
      String var5 = "";
      boolean var6 = false;

      for(int var7 = 0; var7 < var13.length; ++var7) {
         InputStream var8 = ResourceLoader.resourceUtil.getResourceAsInputStream(var0[var7]);
         var13[var7] = new PNGImageData();
         var13[var7].loadImage(var8);
         if (var14 > 0 && var14 != var13[var7].getDepth()) {
            throw new ResourceException(var0[var7] + " has a different color depth as another file " + var5 + " in the same array: \nother: " + var14 + "; this: " + var13[var7].getDepth() + "\n\nAll textures of this series must either be all RGB or all RGBA, and have the same resolution");
         }

         var5 = var0[var7];
         var14 = var13[var7].getDepth();
         boolean var9 = COMPRESSED_ARRAYS && GraphicsContext.getCapabilities().GL_ARB_texture_compression;
         short var10;
         char var12;
         if (var13[0].getDepth() == 32) {
            var10 = 6408;
            var12 = 6408;
            if (var9) {
               System.err.println("Using texture array compression");
               var12 = '蓮';
            } else if (COMPRESSED_ARRAYS) {
               System.err.println("ERROR: Texture compression not supported by hardware");
            }
         } else {
            var10 = 6407;
            var12 = 6407;
            if (var9) {
               System.err.println("Using texture array compression");
               var12 = '蓭';
            } else if (COMPRESSED_ARRAYS) {
               System.err.println("ERROR: Texture compression not supported by hardware");
            }
         }

         GlUtil.printGlErrorCritical();
         if (!var6) {
            if (var9) {
               GL13.glCompressedTexImage3D(35866, 0, var12, var13[0].getWidth(), var13[0].getHeight(), var13.length, 0, (ByteBuffer)null);
            } else {
               GL12.glTexImage3D(35866, 0, var12, var13[0].getWidth(), var13[0].getHeight(), var13.length, 0, var10, 5121, (ByteBuffer)null);
            }

            var6 = true;
         }

         try {
            if (var9) {
               GL12.glTexSubImage3D(35866, 0, 0, 0, var7, var13[var7].getWidth(), var13[var7].getHeight(), 1, var12, 5121, var13[var7].getImageBufferData());
            } else {
               GL13.glCompressedTexSubImage3D(35866, 0, 0, 0, var7, var13[var7].getWidth(), var13[var7].getHeight(), 1, 5121, var13[var7].getImageBufferData());
            }
         } catch (RuntimeException var11) {
            System.err.println("EXCEPTION HAPPENED WITH RESOURCE: " + Arrays.toString(var0));
            throw var11;
         }

         GlUtil.printGlErrorCritical();
         var8.close();
      }

      GlUtil.printGlErrorCritical();
      if (COMPRESSED_ARRAYS && GL11.glGetTexLevelParameteri(35866, 0, 34465) != 1) {
         System.err.println("Exception: Texture Compression failed: " + Arrays.toString(var0));

         assert false;
      }

      boolean var10000 = MIPMAP_ARRAY;
      GlUtil.printGlErrorCritical();
      return var3;
   }

   private static BufferedImage convertBufferedImage(BufferedImage var0) {
      int var1 = 2;

      int var2;
      for(var2 = 2; var1 < var0.getWidth(); var1 <<= 1) {
      }

      while(var2 < var0.getHeight()) {
         var2 <<= 1;
      }

      WritableRaster var3;
      BufferedImage var5;
      if (var0.getColorModel().hasAlpha()) {
         var3 = Raster.createInterleavedRaster(0, var1, var2, 4, (Point)null);
         var5 = new BufferedImage(glAlphaColorModel, var3, false, new Hashtable());
      } else {
         var3 = Raster.createInterleavedRaster(0, var1, var2, 3, (Point)null);
         var5 = new BufferedImage(glColorModel, var3, false, new Hashtable());
      }

      Graphics var4;
      (var4 = var5.getGraphics()).setColor(new Color(0.0F, 0.0F, 0.0F, 0.0F));
      var4.fillRect(0, 0, var1, var2);
      var4.drawImage(var0, 0, 0, (ImageObserver)null);
      return var5;
   }

   protected IntBuffer createIntBuffer(int var1) {
      ByteBuffer var2;
      (var2 = ByteBuffer.allocateDirect(4 * var1)).order(ByteOrder.nativeOrder());
      return var2.asIntBuffer();
   }

   private static int get2Fold(int var0) {
      int var1;
      for(var1 = 2; var1 < var0; var1 <<= 1) {
      }

      return var1;
   }

   public Texture getCubeMap(String var1, String var2) throws IOException, ResourceException {
      GlUtil.glActiveTexture(33984);
      GL11.glGenTextures(GlUtil.getIntBuffer1());
      int var3 = GlUtil.getIntBuffer1().get(0);
      GlUtil.glEnable(34067);
      GlUtil.glBindTexture(34067, var3);
      String[] var4 = new String[]{"posx", "negx", "posy", "negy", "posz", "negz"};
      int[] var5 = new int[]{34069, 34070, 34071, 34072, 34073, 34074};
      Texture var11 = new Texture(34067, var3, var1);

      for(int var6 = 0; var6 < 6; ++var6) {
         PNGImageData var7 = new PNGImageData();
         InputStream var8 = ResourceLoader.resourceUtil.getResourceAsInputStream(var1 + "_" + var4[var6] + "." + var2);
         var7.loadImage(var8);
         var8.close();
         var11.setOriginalWidth(var7.getWidth());
         var11.setOriginalHeight(var7.getHeight());
         ByteBuffer var12;
         (var12 = var7.getImageBufferData()).rewind();
         var11.setWidth(var7.getWidth());
         var11.setHeight(var7.getHeight());
         short var9;
         if (var7.getDepth() == 32) {
            var9 = 6408;
         } else {
            var9 = 6407;
         }

         char var10 = (char)var9;
         if (COMPRESSED_REGULAR) {
            if (var9 == 6407) {
               var10 = '蓭';
            }

            if (var10 == 6408) {
               var10 = '蓮';
            }
         }

         GL11.glTexImage2D(var5[var6], 0, var10, var7.getWidth(), var7.getHeight(), 0, var9, 5121, var12);
      }

      GL11.glTexParameterf(34067, 10240, 9729.0F);
      GL11.glTexParameterf(34067, 10241, 9729.0F);
      GL11.glTexParameterf(34067, 10242, 33071.0F);
      GL11.glTexParameterf(34067, 10243, 33071.0F);
      GL11.glTexParameterf(34067, 32882, 33071.0F);
      GlUtil.glBindTexture(34067, 0);
      GlUtil.glDisable(34067);
      return var11;
   }

   public static Texture getTexture(BufferedImage var0, String var1, int var2, int var3, int var4, int var5, boolean var6, boolean var7) {
      var3 = createTextureID();
      Texture var9;
      (var9 = new Texture(var2, var3, var1)).setOriginalWidth(var0.getWidth());
      var9.setOriginalHeight(var0.getHeight());
      long var11 = System.currentTimeMillis();
      var0 = convertBufferedImage(var0);
      long var13 = System.currentTimeMillis() - var11;
      var11 = System.currentTimeMillis();
      ByteBuffer var10;
      (var10 = getImageData(var0)).rewind();
      GlUtil.glBindTexture(var2, var3);

      assert var0 != null;

      boolean var10000 = $assertionsDisabled;
      long var15 = System.currentTimeMillis() - var11;
      var9.setWidth(var0.getWidth());
      var9.setHeight(var0.getHeight());
      short var8;
      short var19;
      if (var0.getColorModel().hasAlpha()) {
         var8 = 6408;
         var19 = 6408;
      } else {
         var8 = 6407;
         var19 = 6407;
      }

      var11 = System.currentTimeMillis();
      make2DTexture(var2, var4, var5, var19, var0.getWidth(), var0.getHeight(), var8, var10, var6, var1, var7);
      long var17 = System.currentTimeMillis() - var11;
      GlUtil.printGlErrorCritical();
      System.err.println("[TEXTURE] creation took: convert: " + var13 + "; data: " + var15 + "; make: " + var17);
      return var9;
   }

   public static Texture getTexture(LoadableImageData var0, ByteBuffer var1, String var2, int var3, int var4, int var5, int var6, boolean var7, boolean var8) {
      int var9 = createTextureID();
      Texture var10 = new Texture(var3, var9, var2);

      assert FastMath.isPowerOfTwo(var0.getWidth()) && FastMath.isPowerOfTwo(var0.getHeight()) : var2 + ": " + var0.getWidth() + "; " + var0.getHeight();

      var10.setOriginalWidth(var0.getWidth());
      var10.setOriginalHeight(var0.getHeight());
      ByteBuffer var11 = var1;
      var1.rewind();
      GlUtil.glBindTexture(var3, var9);
      boolean var10000 = $assertionsDisabled;
      var10.setWidth(var0.getWidth());
      var10.setHeight(var0.getHeight());
      short var12;
      if (var0.getDepth() == 32) {
         var12 = 6408;
      } else {
         var12 = 6407;
      }

      make2DTexture(var3, var5, var6, var4, var0.getWidth(), var0.getHeight(), var12, var11, var7, var2, var8);
      GlUtil.printGlErrorCritical();
      return var10;
   }

   public Texture getTexture2D(String var1, boolean var2) throws IOException {
      return this.getTexture2D(var1, var2, true);
   }

   public Texture getTexture2D(String var1, boolean var2, boolean var3) throws IOException {
      Texture var4;
      if ((var4 = (Texture)this.table.get(var1)) != null) {
         return var4;
      } else {
         try {
            var4 = getTexture2D(var1, 3553, 6408, 9729, 9729, var2, var3);
         } catch (ResourceException var5) {
            var5.printStackTrace();
         }

         return var4;
      }
   }

   public Texture getTexture2DAnyFormat(String var1, int var2, boolean var3, boolean var4) throws IOException {
      FileExt var5 = new FileExt(var1 + ".png");
      if ((new FileExt(var1 + ".tga")).exists() && EngineSettings.USE_TGA_NORMAL_MAPS.isOn()) {
         return this.getTexture2D(var1 + ".tga", var2, var3, var4);
      } else if (var5.exists()) {
         return this.getTexture2D(var1 + ".png", var2, var3, var4);
      } else {
         throw new FileNotFoundException("Neither .png or .tga found for resource " + var1);
      }
   }

   public Texture getTexture2D(String var1, int var2, boolean var3, boolean var4) throws IOException {
      Texture var5;
      if ((var5 = (Texture)this.table.get(var1)) != null) {
         return var5;
      } else {
         try {
            var5 = getTexture2D(var1, 3553, 6408, var2, var2, var3, var4);
         } catch (ResourceException var6) {
            var6.printStackTrace();
         }

         return var5;
      }
   }

   public static Texture getTexture2D(String var0, int var1, int var2, int var3, int var4, boolean var5, boolean var6) throws IOException, ResourceException {
      long var7 = 0L;
      long var9 = 0L;
      boolean var17 = false;

      Texture var19;
      long var20;
      label68: {
         label72: {
            try {
               var17 = true;
               if (var0.toLowerCase(Locale.ENGLISH).endsWith(".tga")) {
                  long var23 = System.currentTimeMillis();
                  InputStream var15 = ResourceLoader.resourceUtil.getResourceAsInputStream(var0);
                  var9 = System.currentTimeMillis() - var23;
                  var23 = System.currentTimeMillis();
                  ByteBuffer var21 = TGALoader.loadImage(var15);
                  var15.close();
                  var7 = System.currentTimeMillis() - var23;
                  LoadableImageData var22 = new LoadableImageData() {
                     public final int getWidth() {
                        return TGALoader.getLastWidth();
                     }

                     public final int getTexWidth() {
                        return TGALoader.getLastTexWidth();
                     }

                     public final int getTexHeight() {
                        return TGALoader.getLastTexHeight();
                     }

                     public final ByteBuffer getImageBufferData() {
                        assert false;

                        return null;
                     }

                     public final int getHeight() {
                        return TGALoader.getLastHeight();
                     }

                     public final int getDepth() {
                        return TGALoader.getLastDepth();
                     }

                     public final ByteBuffer loadImage(InputStream var1, boolean var2, boolean var3, int[] var4) throws IOException {
                        assert false;

                        return null;
                     }

                     public final ByteBuffer loadImage(InputStream var1, boolean var2, int[] var3) throws IOException {
                        assert false;

                        return null;
                     }

                     public final ByteBuffer loadImage(InputStream var1) throws IOException {
                        assert false;

                        return null;
                     }

                     public final void configureEdging(boolean var1) {
                        assert false;

                     }
                  };
                  var23 = System.currentTimeMillis();
                  var19 = getTexture(var22, var21, var0, var1, var2, var3, var4, var5, var6);
                  var20 = System.currentTimeMillis() - var23;
                  var17 = false;
                  break label68;
               }

               if (var0.toLowerCase(Locale.ENGLISH).endsWith(".png")) {
                  PNGImageData var13 = new PNGImageData();
                  long var14 = System.currentTimeMillis();
                  InputStream var11 = ResourceLoader.resourceUtil.getResourceAsInputStream(var0);
                  var9 = System.currentTimeMillis() - var14;
                  var14 = System.currentTimeMillis();
                  ByteBuffer var12 = var13.loadImage(var11);
                  var11.close();
                  var7 = System.currentTimeMillis() - var14;
                  var14 = System.currentTimeMillis();
                  var19 = getTexture(var13, var12, var0, var1, var2, var3, var4, var5, var6);
                  var20 = System.currentTimeMillis() - var14;
                  var17 = false;
                  break label72;
               }

               var17 = false;
            } finally {
               if (var17) {
                  System.err.println("[TEXTURE] " + var0 + " LOAD TAKEN: stream: " + var9 + " ms; data: " + var7 + " ms; create: 0 ms");
               }
            }

            System.err.println("[TEXTURE] " + var0 + " LOAD TAKEN: stream: 0 ms; data: 0 ms; create: 0 ms");
            BufferedImage var24 = loadImage(var0);

            assert var24 != null : "cannot load " + var0;

            return getTexture(var24, var0, var1, var2, var3, var4, var5, var6);
         }

         System.err.println("[TEXTURE] " + var0 + " LOAD TAKEN: stream: " + var9 + " ms; data: " + var7 + " ms; create: " + var20 + " ms");
         return var19;
      }

      System.err.println("[TEXTURE] " + var0 + " LOAD TAKEN: stream: " + var9 + " ms; data: " + var7 + " ms; create: " + var20 + " ms");
      return var19;
   }

   private static BufferedImage loadImage(String var0) throws IOException, ResourceException {
      InputStream var3 = ResourceLoader.resourceUtil.getResourceAsInputStream(var0);
      BufferedInputStream var1;
      BufferedImage var2 = ImageIO.read(var1 = new BufferedInputStream(var3));
      var3.close();
      var1.close();
      return var2;
   }

   public static boolean hasLinMagForBlock(String var0) {
      return EngineSettings.G_MAG_FILTER_LINEAR_BLOCKS.isOn() || var0 != null && var0.contains("textures") && var0.contains("block") && var0.contains("256");
   }

   private static void make2DTexture(int var0, int var1, int var2, int var3, int var4, int var5, int var6, ByteBuffer var7, boolean var8, String var9, boolean var10) {
      GlUtil.printGlErrorCritical();
      float var12;
      if (var0 == 3553) {
         if (MIPMAP_NORMAL && var8) {
            GL11.glTexParameteri(var0, 10240, 9729);
            GL11.glTexParameteri(3553, 10241, 9987);
            GlUtil.printGlErrorCritical();
            GL11.glTexParameteri(3553, 33085, 5);
            GlUtil.printGlErrorCritical();
            if (var9.contains("textures") && var9.contains("block")) {
               GL11.glTexParameteri(3553, 10241, 9987);
               GL11.glTexParameteri(3553, 10240, hasLinMagForBlock(var9) ? 9729 : 9728);
               GlUtil.printGlErrorCritical();
               if (EngineSettings.BLOCK_TEXTURE_ANISOTROPY.isOn() && GraphicsContext.getCapabilities().GL_EXT_texture_filter_anisotropic) {
                  var12 = GL11.glGetFloat(34047);
                  GL11.glTexParameterf(3553, 34046, var12);
               }

               GlUtil.printGlErrorCritical();
            } else {
               GL11.glTexParameteri(3553, 10240, 9729);
               GL11.glTexParameteri(3553, 10241, 9987);
               GlUtil.printGlErrorCritical();
            }

            if (!GraphicsContext.openGL30()) {
               GL11.glTexParameteri(var0, 33169, 1);
            }
         } else {
            GL11.glTexParameteri(var0, 10241, var1);
            GlUtil.printGlErrorCritical();
            GL11.glTexParameteri(var0, 10240, EngineSettings.G_MAG_FILTER_LINEAR_GUI.isOn() ? 9729 : 9728);
            GlUtil.printGlErrorCritical();
            GL11.glTexParameteri(3553, 33169, 0);
         }

         GlUtil.printGlErrorCritical();
      }

      if (COMPRESSED_REGULAR && var10) {
         if (var3 == 6407) {
            var3 = 34029;
         }

         if (var3 == 6408) {
            var3 = 34030;
         }
      }

      GlUtil.printGlErrorCritical();
      GL11.glTexImage2D(var0, 0, var3, get2Fold(var4), get2Fold(var5), 0, var6, 5121, var7);
      if (MIPMAP_NORMAL && var8 && GraphicsContext.getCapabilities().OpenGL30) {
         try {
            GL30.glGenerateMipmap(var0);
            GlUtil.printGlErrorCritical();
         } catch (Exception var11) {
            var11.printStackTrace();
         }

         if (var9.contains("textures") && var9.contains("block")) {
            GL11.glTexParameteri(var0, 10240, hasLinMagForBlock(var9) ? 9729 : 9728);
            GL11.glTexParameteri(var0, 10241, 9987);
            if (EngineSettings.BLOCK_TEXTURE_ANISOTROPY.isOn() && GraphicsContext.getCapabilities().GL_EXT_texture_filter_anisotropic) {
               var12 = GL11.glGetFloat(34047);
               GL11.glTexParameterf(3553, 34046, var12);
            }
         } else {
            GL11.glTexParameteri(3553, 10240, 9729);
            GL11.glTexParameteri(3553, 10241, 9987);
         }
      }

      GlUtil.printGlErrorCritical();
   }

   static {
      MIPMAP_NORMAL = EngineSettings.G_TEXTURE_MIPMAP.isOn();
      MIPMAP_ARRAY = EngineSettings.G_TEXTURE_ARRAY_MIPMAP.isOn();
      MIPMAP_LVLS = (Integer)EngineSettings.G_MIPMAP_LEVEL_MAX.getCurrentState();
      COMPRESSED_ARRAYS = EngineSettings.G_TEXTURE_ARRAY_COMPRESSION.isOn();
      COMPRESSED_REGULAR = EngineSettings.G_TEXTURE_COMPRESSION_BLOCKS.isOn();
      COMPRESSED_SHADOW = EngineSettings.G_TEXTURE_COMPRESSION_BLOCKS.isOn();
      COMPRESSED_FBO = EngineSettings.G_TEXTURE_COMPRESSION_BLOCKS.isOn();
      glAlphaColorModel = new ComponentColorModel(ColorSpace.getInstance(1000), new int[]{8, 8, 8, 8}, true, false, 3, 0);
      glColorModel = new ComponentColorModel(ColorSpace.getInstance(1000), new int[]{8, 8, 8, 0}, false, false, 1, 0);
   }
}
