package org.schema.schine.graphicsengine.texture.textureImp;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.schema.schine.graphicsengine.core.GlUtil;

public class Texture3D extends TextureNew {
   protected int _width;
   protected int _height;
   protected int _depth;

   public Texture3D() {
      super(32879);
   }

   public void copySubImage(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8) {
      GL12.glCopyTexSubImage3D(32879, var1, var2, var3, var4, var5, var6, var7, var8);
   }

   public int getDepth() {
      return this._depth;
   }

   public int getHeight() {
      return this._height;
   }

   public int getWidth() {
      return this._width;
   }

   public void setImage(int var1, int var2, boolean var3, BufferedImage var4, int var5, int var6, int var7, boolean var8, boolean var9) {
      int var10 = var4.getWidth() / var5;
      int var11 = var4.getHeight() / var6;
      if (var1 == 0) {
         this._format = var2;
         this._width = var10;
         this._height = var11;
         this._depth = var7;
      }

   }

   public void setImage(int var1, int var2, int var3, int var4, boolean var5, boolean var6) {
      this.setImage(0, var1, var2, var3, var4, var5);
      if (var6) {
         var2 = this._width / 2;
         var3 = this._height / 2;
         var4 = this._depth / 2;

         for(int var7 = 1; var2 > 0 || var3 > 0 || var4 > 0; var4 /= 2) {
            this.setImage(var7, var1, Math.max(var2, 1), Math.max(var3, 1), Math.max(var4, 1), var5);
            ++var7;
            var2 /= 2;
            var3 /= 2;
         }
      }

   }

   public void setImage(int var1, int var2, int var3, int var4, int var5, boolean var6) {
      this.setImage(var1, var2, var3, var4, var5, var6, 6408, 5121, (ByteBuffer)null);
   }

   public void setImage(int var1, int var2, int var3, int var4, int var5, boolean var6, int var7, int var8, ByteBuffer var9) {
      if (var1 == 0) {
         this._format = var2;
         this._width = var3;
         this._height = var4;
         this._depth = var5;
      }

      GlUtil.glEnable(32879);
      GlUtil.glBindTexture(32879, this.getId());
      int var10 = (var2 = var6 ? 1 : 0) << 1;
      GlUtil.printGlErrorCritical();
      GL11.glTexParameteri(32879, 10241, 9729);
      GL11.glTexParameteri(32879, 10240, 9729);
      GL11.glTexParameteri(32879, 10242, 10497);
      GL11.glTexParameteri(32879, 10243, 10497);
      GL11.glTexParameteri(32879, 32882, 10497);
      GlUtil.printGlErrorCritical();
      if (var7 == 8194) {
         GL12.glTexImage3D(32879, var1, 6408, var3 + var10, var4 + var10, var5 + var10, var2, 6403, var8, var9);
         GlUtil.printGlErrorCritical();
         this.setBytes(var1, var9 == null ? var3 * var4 * var5 : var9.remaining());
      } else {
         GL12.glTexImage3D(32879, var1, 6408, var3 + var10, var4 + var10, var5 + var10, var2, var7, var8, var9);
         GlUtil.printGlErrorCritical();
         this.setBytes(var1, var9 == null ? var3 * var4 * var5 << 2 : var9.remaining());
      }

      GlUtil.printGlErrorCritical();
      GlUtil.glBindTexture(32879, 0);
      GlUtil.glDisable(32879);
      GlUtil.printGlErrorCritical();
   }

   public void setImages(int var1, boolean var2, BufferedImage var3, int var4, int var5, int var6, boolean var7, boolean var8, boolean var9) {
      this.setImage(0, var1, var2, var3, var4, var5, var6, var7, var8);
      if (var9) {
         int var10000 = this._width;
         var10000 = this._height;
         var10000 = this._depth;
      }

   }
}
