package org.schema.schine.graphicsengine.texture;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.texture.textureImp.Texture3D;

public class DDSLoader {
   protected static final int DDSD_MIPMAPCOUNT = 131072;
   protected static final int DDPF_FOURCC = 4;
   protected static final int DDPF_ALPHAPIXELS = 1;
   protected static final int DDSCAPS2_CUBEMAP = 512;
   protected static final int DDSCAPS2_VOLUME = 2097152;

   protected static ByteBuffer load(File var0) throws IOException {
      FileInputStream var5 = new FileInputStream(var0);

      ByteBuffer var6;
      try {
         FileChannel var1;
         ByteBuffer var2 = GlUtil.getDynamicByteBuffer((int)(var1 = var5.getChannel()).size(), 0);
         var1.read(var2);
         var2.rewind();
         var6 = var2;
      } finally {
         var5.close();
      }

      return var6;
   }

   public static void load(File var0, Texture3D var1, boolean var2) throws IOException {
      ByteBuffer var3 = load(var0);
      DDSLoader.Header var4;
      if (((var4 = new DDSLoader.Header(var3)).caps2 & 2097152) == 0) {
         throw new IOException("Not a volume texture: " + var0);
      } else if (var4.fourCC != null) {
         throw new IOException("Compression not supported for 3D textures: " + var4.fourCC);
      } else {
         int var11 = var4.width;
         int var5 = var4.height;
         int var6 = var4.depth;
         int var7 = (var4.pixelFormatFlags & 1) == 0 ? 6407 : 6408;
         int var8;
         if ((var8 = var4.getUncompressedFormat()) == 32992) {
            var7 = 6408;
            var8 = 32993;
         }

         int var9 = 0;

         for(int var12 = var4.getLevels(); var9 < var12; ++var9) {
            byte var10;
            switch(var8) {
            case 6407:
               var10 = 3;
               break;
            case 6408:
               var10 = 4;
               break;
            case 8194:
               var10 = 1;
               break;
            case 32992:
               var10 = 3;
               break;
            case 32993:
               var10 = 4;
               break;
            default:
               throw new IllegalArgumentException("unknown format: " + var7);
            }

            int var13 = var3.position() + var11 * var6 * var5 * var10;
            var3.limit(var13);
            var1.setImage(var9, var7, var11, var5, var6, var2, var8, 5121, var3);
            var11 = Math.max(var11 / 2, 1);
            var5 = Math.max(var5 / 2, 1);
            var6 = Math.max(var6 / 2, 1);
            var3.position(var3.limit());
         }

         GlUtil.printGlErrorCritical();
      }
   }

   public static class Header {
      public int flags;
      public int height;
      public int width;
      public int pitchOrLinearSize;
      public int depth;
      public int mipMapCount;
      public int pixelFormatFlags;
      public String fourCC;
      public int rgbBitCount;
      public int rBitMask;
      public int gBitMask;
      public int bBitMask;
      public int alphaBitMask;
      public int caps1;
      public int caps2;

      public Header(ByteBuffer var1) throws IOException {
         byte[] var2 = new byte[4];
         var1.get(var2);
         String var3;
         if (!(var3 = new String(var2, "US-ASCII")).equals("DDS ")) {
            throw new IOException("Bad magic: " + var3);
         } else {
            int var4;
            if ((var4 = var1.getInt()) != 124) {
               throw new IOException("Wrong header size: " + var4);
            } else {
               this.flags = var1.getInt();
               this.height = var1.getInt();
               this.width = var1.getInt();
               this.pitchOrLinearSize = var1.getInt();
               this.depth = var1.getInt();
               this.mipMapCount = var1.getInt();
               var1.position(var1.position() + 44);
               if ((var4 = var1.getInt()) != 32) {
                  throw new IOException("Wrong pixel format size: " + var4);
               } else {
                  this.pixelFormatFlags = var1.getInt();
                  var1.get(var2);
                  this.fourCC = (this.pixelFormatFlags & 4) == 0 ? null : new String(var2, "US-ASCII");
                  this.rgbBitCount = var1.getInt();
                  this.rBitMask = var1.getInt();
                  this.gBitMask = var1.getInt();
                  this.bBitMask = var1.getInt();
                  this.alphaBitMask = var1.getInt();
                  this.caps1 = var1.getInt();
                  this.caps2 = var1.getInt();
                  var1.position(var1.position() + 8);
                  var1.position(var1.position() + 4);
               }
            }
         }
      }

      public int getCompressedFormat() throws IOException {
         char var1;
         if ("DXT1".equals(this.fourCC)) {
            var1 = '蓭';
         } else if ("DXT3".equals(this.fourCC)) {
            var1 = '蓮';
         } else {
            if (!"DXT5".equals(this.fourCC)) {
               throw new IOException("Unknown format: " + this.fourCC);
            }

            var1 = '蓮';
         }

         return var1;
      }

      public int getLevels() {
         return (this.flags & 131072) == 0 ? 1 : this.mipMapCount;
      }

      public int getUncompressedFormat() throws IOException {
         if (this.rBitMask == 255 && this.gBitMask == 65280 && this.bBitMask == 16711680) {
            if ((this.pixelFormatFlags & 1) == 0) {
               return 6407;
            }

            if (this.alphaBitMask == -16777216) {
               return 6408;
            }
         } else if (this.rBitMask == 16711680 && this.gBitMask == 65280 && this.bBitMask == 255) {
            if (!GraphicsContext.getCapabilities().GL_EXT_bgra) {
               throw new IOException("BGRA format not supported.");
            }

            if ((this.pixelFormatFlags & 1) == 0) {
               return 32992;
            }

            if (this.alphaBitMask == -16777216) {
               return 32993;
            }
         } else if (this.rBitMask == 255 && this.gBitMask == 0 && this.bBitMask == 0) {
            return 8194;
         }

         throw new IOException("Unknown format: " + this.rBitMask + "/" + this.gBitMask + "/" + this.bBitMask + "/" + this.alphaBitMask);
      }
   }
}
