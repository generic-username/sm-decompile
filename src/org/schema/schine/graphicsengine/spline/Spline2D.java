package org.schema.schine.graphicsengine.spline;

import java.util.ArrayList;
import java.util.List;

public class Spline2D {
   final int count;
   private final Spline2D.Cubic[] x;
   private final Spline2D.Cubic[] y;
   private List travelCache;
   private float maxTravelStep;
   private float posStep;

   public Spline2D(float[][] var1) {
      this.count = var1.length;
      float[] var2 = new float[this.count];
      float[] var3 = new float[this.count];

      for(int var4 = 0; var4 < this.count; ++var4) {
         var2[var4] = var1[var4][0];
         var3[var4] = var1[var4][1];
      }

      this.x = Spline2D.Curve.calcCurve(this.count - 1, var2);
      this.y = Spline2D.Curve.calcCurve(this.count - 1, var3);
   }

   private static float dist(float[] var0, float[] var1) {
      float var2 = var1[0] - var0[0];
      float var3 = var1[1] - var0[1];
      return (float)Math.sqrt((double)(var2 * var2 + var3 * var3));
   }

   public void enabledTripCaching(float var1, float var2) {
      this.maxTravelStep = var1;
      this.posStep = var2;
      var1 = this.x[0].eval(0.0F);
      var2 = this.y[0].eval(0.0F);
      this.travelCache = new ArrayList();
      this.travelCache.add(new Spline2D.CacheItem(var1, var2, 0.0F));
   }

   public final float[] getPositionAt(float var1) {
      float[] var2 = new float[2];
      this.getPositionAt(var1, var2);
      return var2;
   }

   public final void getPositionAt(float var1, float[] var2) {
      if (var1 < 0.0F) {
         var1 = 0.0F;
      }

      if (var1 >= (float)(this.count - 1)) {
         var1 = (float)(this.count - 1) - Math.ulp((float)(this.count - 1));
      }

      int var3 = (int)var1;
      var1 -= (float)var3;
      var2[0] = this.x[var3].eval(var1);
      var2[1] = this.y[var3].eval(var1);
   }

   private Spline2D.CacheItem getSteppingPosition(float var1, float var2, float var3) {
      float var4 = var1;
      float[] var7 = this.getPositionAt(var1);

      float var5;
      float[] var6;
      for(var5 = 0.0F; var5 < var2 && var4 < (float)this.count; var7 = var6) {
         var6 = this.getPositionAt(var4 += var3);
         var5 += dist(var7, var6);
      }

      Spline2D.CacheItem var8;
      (var8 = new Spline2D.CacheItem(var7[0], var7[1], 0.0F)).position = var4;
      var8.travelled = var5;
      return var8;
   }

   public boolean getTripPosition(float var1, float[] var2) {
      boolean var3 = true;

      Spline2D.CacheItem var4;
      Spline2D.CacheItem var6;
      for(var4 = (Spline2D.CacheItem)this.travelCache.get(this.travelCache.size() - 1); var4.travelled < var1 && var1 != 0.0F; var4 = var6) {
         float var5 = Math.min(var1 - var4.travelled, this.maxTravelStep);
         if ((var6 = this.getSteppingPosition(var4.position, var5, this.posStep)).position >= (float)this.count) {
            var3 = false;
            break;
         }

         if (var6.travelled > this.maxTravelStep * 0.95F) {
            this.travelCache.add(var6);
         }

         var6.travelled = var4.travelled;
      }

      int var8 = 0;
      int var10 = this.travelCache.size() - 1;

      int var7;
      while(true) {
         var7 = (var8 + var10) / 2;
         if ((var4 = (Spline2D.CacheItem)this.travelCache.get(var7)).travelled < var1) {
            if (var8 == var7) {
               break;
            }

            var8 = var7;
         } else {
            if (var10 == var7) {
               break;
            }

            var10 = var7;
         }
      }

      Spline2D.CacheItem var9;
      for(var7 = var8; var7 <= var10 && (var9 = (Spline2D.CacheItem)this.travelCache.get(var7)).travelled <= var1; ++var7) {
         var4 = var9;
      }

      float var11 = var1 - var4.travelled;
      var4 = this.getSteppingPosition(var4.position, var11, this.posStep);
      var2[0] = var4.xpos;
      var2[1] = var4.ypos;
      return var3;
   }

   public final int pointCount() {
      return this.count;
   }

   static class Curve {
      private Curve() {
      }

      static final Spline2D.Cubic[] calcCurve(int var0, float[] var1) {
         float[] var2 = new float[var0 + 1];
         float[] var3 = new float[var0 + 1];
         float[] var4 = new float[var0 + 1];
         Spline2D.Cubic[] var5 = new Spline2D.Cubic[var0];
         var2[0] = 0.5F;

         int var6;
         for(var6 = 1; var6 < var0; ++var6) {
            var2[var6] = 1.0F / (4.0F - var2[var6 - 1]);
         }

         var2[var0] = 1.0F / (2.0F - var2[var0 - 1]);
         var3[0] = 3.0F * (var1[1] - var1[0]) * var2[0];

         for(var6 = 1; var6 < var0; ++var6) {
            var3[var6] = (3.0F * (var1[var6 + 1] - var1[var6 - 1]) - var3[var6 - 1]) * var2[var6];
         }

         var3[var0] = (3.0F * (var1[var0] - var1[var0 - 1]) - var3[var0 - 1]) * var2[var0];
         var4[var0] = var3[var0];

         for(var6 = var0 - 1; var6 >= 0; --var6) {
            var4[var6] = var3[var6] - var2[var6] * var4[var6 + 1];
         }

         for(var6 = 0; var6 < var0; ++var6) {
            float var9 = var1[var6];
            float var10 = var1[var6 + 1];
            float var7 = var4[var6];
            float var8 = var4[var6 + 1];
            var5[var6] = new Spline2D.Cubic(var9, var7, 3.0F * (var10 - var9) - var7 * 2.0F - var8, 2.0F * (var9 - var10) + var7 + var8);
         }

         return var5;
      }
   }

   static class Cubic {
      private final float a;
      private final float b;
      private final float c;
      private final float d;

      Cubic(float var1, float var2, float var3, float var4) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
      }

      final float eval(float var1) {
         return ((this.d * var1 + this.c) * var1 + this.b) * var1 + this.a;
      }
   }

   static class CacheItem {
      public float ypos;
      public float xpos;
      private float position;
      private float travelled;

      public CacheItem(float var1, float var2, float var3) {
         this.position = var1;
         this.travelled = var2;
      }
   }
}
