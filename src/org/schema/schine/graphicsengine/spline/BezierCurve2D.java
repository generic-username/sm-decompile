package org.schema.schine.graphicsengine.spline;

import javax.vecmath.Vector2f;

public class BezierCurve2D {
   private static float coeff(int var0, int var1) {
      return (float)(fact(var0) / (fact(var1) * fact(var0 - var1)));
   }

   private static double fact(int var0) {
      double var1 = 1.0D;

      for(int var3 = 2; var3 <= var0; ++var3) {
         var1 *= (double)var3;
      }

      return var1;
   }

   public static Vector2f interpolate(float var0, Vector2f... var1) {
      float var2 = 0.0F;
      float var3 = 0.0F;
      int var4 = var1.length - 1;

      for(int var5 = 0; var5 <= var4; ++var5) {
         float var6 = pow(1.0F - var0, var4 - var5) * coeff(var4, var5) * pow(var0, var5);
         var2 += var1[var5].x * var6;
         var3 += var1[var5].y * var6;
      }

      return new Vector2f(var2, var3);
   }

   public static Vector2f interpolate4(float var0, Vector2f var1, Vector2f var2, Vector2f var3, Vector2f var4) {
      float var5;
      float var6 = pow3(var5 = 1.0F - var0) * pow0(var0);
      float var7 = pow2(var5) * 3.0F * pow1(var0);
      float var8 = pow1(var5) * 3.0F * pow2(var0);
      var0 = pow0(var5) * pow3(var0);
      var5 = 0.0F + var1.x * var6;
      float var9 = 0.0F + var1.y * var6;
      var5 += var2.x * var7;
      var9 += var2.y * var7;
      var5 += var3.x * var8;
      var9 += var3.y * var8;
      var5 += var4.x * var0;
      var9 += var4.y * var0;
      return new Vector2f(var5, var9);
   }

   private static float pow(float var0, int var1) {
      float var2 = 1.0F;

      for(int var3 = 0; var3 < var1; ++var3) {
         var2 *= var0;
      }

      return var2;
   }

   private static float pow0(float var0) {
      return 1.0F;
   }

   private static float pow1(float var0) {
      return var0;
   }

   private static float pow2(float var0) {
      return var0 * var0;
   }

   private static float pow3(float var0) {
      return var0 * var0 * var0;
   }
}
