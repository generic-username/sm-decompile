package org.schema.schine.graphicsengine.util;

class NeuQuant {
   protected static final int netsize = 256;
   protected static final int prime1 = 499;
   protected static final int prime2 = 491;
   protected static final int prime3 = 487;
   protected static final int prime4 = 503;
   protected static final int minpicturebytes = 1509;
   protected static final int maxnetpos = 255;
   protected static final int netbiasshift = 4;
   protected static final int ncycles = 100;
   protected static final int intbiasshift = 16;
   protected static final int intbias = 65536;
   protected static final int gammashift = 10;
   protected static final int gamma = 1024;
   protected static final int betashift = 10;
   protected static final int beta = 64;
   protected static final int betagamma = 65536;
   protected static final int initrad = 32;
   protected static final int radiusbiasshift = 6;
   protected static final int radiusbias = 64;
   protected static final int initradius = 2048;
   protected static final int radiusdec = 30;
   protected static final int alphabiasshift = 10;
   protected static final int initalpha = 1024;
   protected static final int radbiasshift = 8;
   protected static final int radbias = 256;
   protected static final int alpharadbshift = 18;
   protected static final int alpharadbias = 262144;
   protected int alphadec;
   protected byte[] thepicture;
   protected int lengthcount;
   protected int samplefac;
   protected int[][] network;
   protected int[] netindex = new int[256];
   protected int[] bias = new int[256];
   protected int[] freq = new int[256];
   protected int[] radpower = new int[32];

   public NeuQuant(byte[] var1, int var2, int var3) {
      this.thepicture = var1;
      this.lengthcount = var2;
      this.samplefac = var3;
      this.network = new int[256][];

      for(int var4 = 0; var4 < 256; ++var4) {
         this.network[var4] = new int[4];
         int[] var5;
         (var5 = this.network[var4])[0] = var5[1] = var5[2] = (var4 << 12) / 256;
         this.freq[var4] = 256;
         this.bias[var4] = 0;
      }

   }

   public byte[] colorMap() {
      byte[] var1 = new byte[768];
      int[] var2 = new int[256];

      int var3;
      for(var3 = 0; var3 < 256; var2[this.network[var3][3]] = var3++) {
      }

      var3 = 0;

      for(int var4 = 0; var4 < 256; ++var4) {
         int var5 = var2[var4];
         var1[var3++] = (byte)this.network[var5][0];
         var1[var3++] = (byte)this.network[var5][1];
         var1[var3++] = (byte)this.network[var5][2];
      }

      return var1;
   }

   public void inxbuild() {
      int var7 = 0;
      int var8 = 0;

      int var2;
      for(int var1 = 0; var1 < 256; ++var1) {
         int[] var5 = this.network[var1];
         int var3 = var1;
         int var4 = var5[1];

         int[] var6;
         for(var2 = var1 + 1; var2 < 256; ++var2) {
            if ((var6 = this.network[var2])[1] < var4) {
               var3 = var2;
               var4 = var6[1];
            }
         }

         var6 = this.network[var3];
         if (var1 != var3) {
            var2 = var6[0];
            var6[0] = var5[0];
            var5[0] = var2;
            var2 = var6[1];
            var6[1] = var5[1];
            var5[1] = var2;
            var2 = var6[2];
            var6[2] = var5[2];
            var5[2] = var2;
            var2 = var6[3];
            var6[3] = var5[3];
            var5[3] = var2;
         }

         if (var4 != var7) {
            this.netindex[var7] = var8 + var1 >> 1;

            for(var2 = var7 + 1; var2 < var4; ++var2) {
               this.netindex[var2] = var1;
            }

            var7 = var4;
            var8 = var1;
         }
      }

      this.netindex[var7] = var8 + 255 >> 1;

      for(var2 = var7 + 1; var2 < 256; ++var2) {
         this.netindex[var2] = 255;
      }

   }

   public void learn() {
      if (this.lengthcount < 1509) {
         this.samplefac = 1;
      }

      this.alphadec = 30 + (this.samplefac - 1) / 3;
      byte[] var12 = this.thepicture;
      int var13 = 0;
      int var14 = this.lengthcount;
      int var11;
      int var10 = (var11 = this.lengthcount / (3 * this.samplefac)) / 100;
      int var8 = 1024;
      int var6 = 2048;
      int var7 = 32;

      int var1;
      for(var1 = 0; var1 < 32; ++var1) {
         this.radpower[var1] = 1024 * ((1024 - var1 * var1 << 8) / 1024);
      }

      short var9;
      if (this.lengthcount < 1509) {
         var9 = 3;
      } else if (this.lengthcount % 499 != 0) {
         var9 = 1497;
      } else if (this.lengthcount % 491 != 0) {
         var9 = 1473;
      } else if (this.lengthcount % 487 != 0) {
         var9 = 1461;
      } else {
         var9 = 1509;
      }

      var1 = 0;

      while(true) {
         int var2;
         do {
            if (var1 >= var11) {
               return;
            }

            int var3 = (var12[var13] & 255) << 4;
            int var4 = (var12[var13 + 1] & 255) << 4;
            int var5 = (var12[var13 + 2] & 255) << 4;
            var2 = this.contest(var3, var4, var5);
            this.altersingle(var8, var2, var3, var4, var5);
            if (var7 != 0) {
               this.alterneigh(var7, var2, var3, var4, var5);
            }

            if ((var13 += var9) >= var14) {
               var13 -= this.lengthcount;
            }

            ++var1;
            if (var10 == 0) {
               var10 = 1;
            }
         } while(var1 % var10 != 0);

         var8 -= var8 / this.alphadec;
         if ((var7 = (var6 -= var6 / 30) >> 6) <= 1) {
            var7 = 0;
         }

         for(var2 = 0; var2 < var7; ++var2) {
            this.radpower[var2] = var8 * ((var7 * var7 - var2 * var2 << 8) / (var7 * var7));
         }
      }
   }

   public int map(int var1, int var2, int var3) {
      int var8 = 1000;
      int var10 = -1;
      int var4;
      int var5 = (var4 = this.netindex[var2]) - 1;

      while(var4 < 256 || var5 >= 0) {
         int var6;
         int var7;
         int[] var9;
         if (var4 < 256) {
            if ((var6 = (var9 = this.network[var4])[1] - var2) >= var8) {
               var4 = 256;
            } else {
               ++var4;
               if (var6 < 0) {
                  var6 = -var6;
               }

               if ((var7 = var9[0] - var1) < 0) {
                  var7 = -var7;
               }

               if ((var6 += var7) < var8) {
                  if ((var7 = var9[2] - var3) < 0) {
                     var7 = -var7;
                  }

                  if ((var6 += var7) < var8) {
                     var8 = var6;
                     var10 = var9[3];
                  }
               }
            }
         }

         if (var5 >= 0) {
            var9 = this.network[var5];
            if ((var6 = var2 - var9[1]) >= var8) {
               var5 = -1;
            } else {
               --var5;
               if (var6 < 0) {
                  var6 = -var6;
               }

               if ((var7 = var9[0] - var1) < 0) {
                  var7 = -var7;
               }

               if ((var6 += var7) < var8) {
                  if ((var7 = var9[2] - var3) < 0) {
                     var7 = -var7;
                  }

                  if ((var6 += var7) < var8) {
                     var8 = var6;
                     var10 = var9[3];
                  }
               }
            }
         }
      }

      return var10;
   }

   public byte[] process() {
      this.learn();
      this.unbiasnet();
      this.inxbuild();
      return this.colorMap();
   }

   public void unbiasnet() {
      for(int var1 = 0; var1 < 256; this.network[var1][3] = var1++) {
         int[] var10000 = this.network[var1];
         var10000[0] >>= 4;
         var10000 = this.network[var1];
         var10000[1] >>= 4;
         var10000 = this.network[var1];
         var10000[2] >>= 4;
      }

   }

   protected void alterneigh(int var1, int var2, int var3, int var4, int var5) {
      int var6;
      if ((var6 = var2 - var1) < -1) {
         var6 = -1;
      }

      int var7;
      if ((var7 = var2 + var1) > 256) {
         var7 = 256;
      }

      var1 = var2 + 1;
      --var2;
      int var9 = 1;

      while(var1 < var7 || var2 > var6) {
         int var8 = this.radpower[var9++];
         int[] var10;
         if (var1 < var7) {
            var10 = this.network[var1++];

            try {
               var10[0] -= var8 * (var10[0] - var3) / 262144;
               var10[1] -= var8 * (var10[1] - var4) / 262144;
               var10[2] -= var8 * (var10[2] - var5) / 262144;
            } catch (Exception var11) {
            }
         }

         if (var2 > var6) {
            var10 = this.network[var2--];

            try {
               var10[0] -= var8 * (var10[0] - var3) / 262144;
               var10[1] -= var8 * (var10[1] - var4) / 262144;
               var10[2] -= var8 * (var10[2] - var5) / 262144;
            } catch (Exception var12) {
            }
         }
      }

   }

   protected void altersingle(int var1, int var2, int var3, int var4, int var5) {
      int[] var6;
      int[] var10000 = var6 = this.network[var2];
      var10000[0] -= var1 * (var6[0] - var3) / 1024;
      var6[1] -= var1 * (var6[1] - var4) / 1024;
      var6[2] -= var1 * (var6[2] - var5) / 1024;
   }

   protected int contest(int var1, int var2, int var3) {
      int var9 = Integer.MAX_VALUE;
      int var10 = Integer.MAX_VALUE;
      int var7 = -1;
      int var8 = -1;

      int[] var10000;
      for(int var4 = 0; var4 < 256; ++var4) {
         int var5;
         int[] var11;
         if ((var5 = (var11 = this.network[var4])[0] - var1) < 0) {
            var5 = -var5;
         }

         int var6;
         if ((var6 = var11[1] - var2) < 0) {
            var6 = -var6;
         }

         var5 += var6;
         if ((var6 = var11[2] - var3) < 0) {
            var6 = -var6;
         }

         if ((var5 += var6) < var9) {
            var9 = var5;
            var7 = var4;
         }

         if ((var5 -= this.bias[var4] >> 12) < var10) {
            var10 = var5;
            var8 = var4;
         }

         var5 = this.freq[var4] >> 10;
         var10000 = this.freq;
         var10000[var4] -= var5;
         var10000 = this.bias;
         var10000[var4] += var5 << 10;
      }

      var10000 = this.freq;
      var10000[var7] += 64;
      var10000 = this.bias;
      var10000[var7] -= 65536;
      return var8;
   }
}
