package org.schema.schine.graphicsengine.util;

import com.mortennobel.imagescaling.MultiStepRescaleOp;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.ImageObserver;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class GifEncoder extends Thread {
   protected int width;
   protected int height;
   protected Color transparent = null;
   protected int transIndex;
   protected int repeat = -1;
   protected int delay = 0;
   protected boolean started = false;
   protected OutputStream out;
   protected BufferedImage image;
   protected byte[] pixels;
   protected byte[] indexedPixels;
   protected int colorDepth;
   protected byte[] colorTab;
   protected boolean[] usedEntry = new boolean[256];
   protected int palSize = 7;
   protected int dispose = -1;
   protected boolean closeStream = false;
   protected boolean firstFrame = true;
   protected boolean sizeSet = false;
   protected int sample = 10;
   public static ObjectArrayFIFOQueue gifPool = new ObjectArrayFIFOQueue();
   public static ObjectArrayFIFOQueue ARGBPool = new ObjectArrayFIFOQueue();
   public int running = 0;
   private static BufferedImage bImg;
   private ThreadPoolExecutor threadPool;
   private ObjectArrayFIFOQueue jobs = new ObjectArrayFIFOQueue();

   public void setDelay(int var1) {
      this.delay = Math.round((float)var1 / 10.0F);
   }

   public void setDispose(int var1) {
      if (var1 >= 0) {
         this.dispose = var1;
      }

   }

   public void setRepeat(int var1) {
      if (var1 >= 0) {
         this.repeat = var1;
      }

   }

   public void setTransparent(Color var1) {
      this.transparent = var1;
   }

   public static void freeGifArray(byte[] var0) {
      synchronized(gifPool) {
         gifPool.enqueue(var0);
      }
   }

   public static byte[] getGifArray(int var0) {
      synchronized(gifPool) {
         if (!gifPool.isEmpty()) {
            if (((byte[])gifPool.first()).length != var0) {
               gifPool.clear();
               return getGifArray(var0);
            } else {
               return (byte[])gifPool.dequeue();
            }
         } else {
            return new byte[var0];
         }
      }
   }

   public static void freeARGBArray(int[] var0) {
      synchronized(ARGBPool) {
         ARGBPool.enqueue(var0);
      }
   }

   public static int[] getARGBArray(int var0) {
      synchronized(ARGBPool) {
         if (!ARGBPool.isEmpty()) {
            if (((int[])ARGBPool.first()).length != var0) {
               ARGBPool.clear();
               return getARGBArray(var0);
            } else {
               return (int[])ARGBPool.dequeue();
            }
         } else {
            return new int[var0];
         }
      }
   }

   public boolean addFrame(BufferedImage var1) {
      if (var1 == null) {
         return false;
      } else {
         boolean var2 = true;

         try {
            if (!this.sizeSet) {
               this.setSize(var1.getWidth(), var1.getHeight());
            }

            this.image = var1;
            this.getImagePixels();
            this.analyzePixels();
            if (this.firstFrame) {
               this.writeLSD();
               this.writePalette();
               if (this.repeat >= 0) {
                  this.writeNetscapeExt();
               }
            }

            this.writeGraphicCtrlExt();
            this.writeImageDesc();
            if (!this.firstFrame) {
               this.writePalette();
            }

            this.writePixels();
            this.firstFrame = false;
         } catch (IOException var3) {
            var2 = false;
         }

         return var2;
      }
   }

   public void finish() {
      if (this.started) {
         this.started = false;
      }
   }

   private boolean onFinish() {
      while(!this.jobs.isEmpty()) {
         GifEncoder.GifJob var1 = null;
         synchronized(this.jobs) {
            if (!this.jobs.isEmpty()) {
               var1 = (GifEncoder.GifJob)this.jobs.dequeue();
            }
         }

         if (var1 != null) {
            var1.execute();
         }
      }

      boolean var5 = true;

      try {
         this.out.write(59);
         this.out.flush();
         if (this.closeStream) {
            this.out.close();
         }
      } catch (IOException var3) {
         var5 = false;
      }

      this.transIndex = 0;
      this.out = null;
      this.image = null;
      this.pixels = null;
      this.indexedPixels = null;
      this.colorTab = null;
      this.closeStream = false;
      this.firstFrame = true;
      this.threadPool.shutdown();
      this.running = 0;
      System.err.println("FINISHED GIF WRITING!");
      return var5;
   }

   public void setFrameRate(float var1) {
      if (var1 != 0.0F) {
         this.delay = Math.round(100.0F / var1);
      }

   }

   public void setQuality(int var1) {
      if (var1 <= 0) {
         var1 = 1;
      }

      this.sample = var1;
   }

   public void setSize(int var1, int var2) {
      if (!this.started || this.firstFrame) {
         this.width = var1;
         this.height = var2;
         if (this.width <= 0) {
            this.width = 320;
         }

         if (this.height <= 0) {
            this.height = 240;
         }

         this.sizeSet = true;
      }
   }

   public boolean start(OutputStream var1) {
      if (var1 == null) {
         return false;
      } else {
         boolean var2 = true;
         this.closeStream = false;
         this.out = var1;

         try {
            this.writeString("GIF89a");
         } catch (IOException var3) {
            var2 = false;
         }

         if (var2) {
            this.start();
         }

         return this.started = var2;
      }
   }

   public boolean start(String var1) {
      boolean var2 = false;

      try {
         this.threadPool = (ThreadPoolExecutor)Executors.newFixedThreadPool(10, new ThreadFactory() {
            public Thread newThread(Runnable var1) {
               return new Thread(var1, "GifThread");
            }
         });
         this.out = new BufferedOutputStream(new FileOutputStream(var1));
         var2 = this.start(this.out);
         this.closeStream = true;

         assert this.started;
      } catch (IOException var3) {
         var2 = false;
      }

      return this.started = var2;
   }

   protected void analyzePixels() {
      int var1;
      int var2 = (var1 = this.pixels.length) / 3;
      this.indexedPixels = new byte[var2];
      NeuQuant var6 = new NeuQuant(this.pixels, var1, this.sample);
      this.colorTab = var6.process();

      int var3;
      for(var3 = 0; var3 < this.colorTab.length; var3 += 3) {
         byte var4 = this.colorTab[var3];
         this.colorTab[var3] = this.colorTab[var3 + 2];
         this.colorTab[var3 + 2] = var4;
         this.usedEntry[var3 / 3] = false;
      }

      var3 = 0;

      for(int var7 = 0; var7 < var2; ++var7) {
         int var5 = var6.map(this.pixels[var3++] & 255, this.pixels[var3++] & 255, this.pixels[var3++] & 255);
         this.usedEntry[var5] = true;
         this.indexedPixels[var7] = (byte)var5;
      }

      this.pixels = null;
      this.colorDepth = 8;
      this.palSize = 7;
      if (this.transparent != null) {
         this.transIndex = this.findClosest(this.transparent);
      }

   }

   protected int findClosest(Color var1) {
      if (this.colorTab == null) {
         return -1;
      } else {
         int var2 = var1.getRed();
         int var3 = var1.getGreen();
         int var11 = var1.getBlue();
         int var4 = 0;
         int var5 = 16777216;
         int var6 = this.colorTab.length;

         for(int var7 = 0; var7 < var6; ++var7) {
            int var8 = var2 - (this.colorTab[var7++] & 255);
            int var9 = var3 - (this.colorTab[var7++] & 255);
            int var10 = var11 - (this.colorTab[var7] & 255);
            var8 = var8 * var8 + var9 * var9 + var10 * var10;
            var9 = var7 / 3;
            if (this.usedEntry[var9] && var8 < var5) {
               var5 = var8;
               var4 = var9;
            }
         }

         return var4;
      }
   }

   protected void getImagePixels() {
      int var1 = this.image.getWidth();
      int var2 = this.image.getHeight();
      int var3 = this.image.getType();
      if (var1 != this.width || var2 != this.height || var3 != 5) {
         BufferedImage var4;
         (var4 = new BufferedImage(this.width, this.height, 5)).createGraphics().drawImage(this.image, 0, 0, (ImageObserver)null);
         this.image = var4;
      }

      this.pixels = ((DataBufferByte)this.image.getRaster().getDataBuffer()).getData();
   }

   protected void writeGraphicCtrlExt() throws IOException {
      this.out.write(33);
      this.out.write(249);
      this.out.write(4);
      byte var1;
      int var2;
      if (this.transparent == null) {
         var1 = 0;
         var2 = 0;
      } else {
         var1 = 1;
         var2 = 2;
      }

      if (this.dispose >= 0) {
         var2 = this.dispose & 7;
      }

      var2 <<= 2;
      this.out.write(0 | var2 | var1);
      this.writeShort(this.delay);
      this.out.write(this.transIndex);
      this.out.write(0);
   }

   protected void writeImageDesc() throws IOException {
      this.out.write(44);
      this.writeShort(0);
      this.writeShort(0);
      this.writeShort(this.width);
      this.writeShort(this.height);
      if (this.firstFrame) {
         this.out.write(0);
      } else {
         this.out.write(128 | this.palSize);
      }
   }

   protected void writeLSD() throws IOException {
      this.writeShort(this.width);
      this.writeShort(this.height);
      this.out.write(240 | this.palSize);
      this.out.write(0);
      this.out.write(0);
   }

   protected void writeNetscapeExt() throws IOException {
      this.out.write(33);
      this.out.write(255);
      this.out.write(11);
      this.writeString("NETSCAPE2.0");
      this.out.write(3);
      this.out.write(1);
      this.writeShort(this.repeat);
      this.out.write(0);
   }

   protected void writePalette() throws IOException {
      this.out.write(this.colorTab, 0, this.colorTab.length);
      int var1 = 768 - this.colorTab.length;

      for(int var2 = 0; var2 < var1; ++var2) {
         this.out.write(0);
      }

   }

   protected void writePixels() throws IOException {
      (new LZWEncoder(this.width, this.height, this.indexedPixels, this.colorDepth)).encode(this.out);
   }

   protected void writeShort(int var1) throws IOException {
      this.out.write(var1 & 255);
      this.out.write(var1 >> 8 & 255);
   }

   protected void writeString(String var1) throws IOException {
      for(int var2 = 0; var2 < var1.length(); ++var2) {
         this.out.write((byte)var1.charAt(var2));
      }

   }

   public void run() {
      while(this.started) {
         if (!this.jobs.isEmpty()) {
            GifEncoder.GifJob var1 = null;
            synchronized(this.jobs) {
               if (!this.jobs.isEmpty()) {
                  var1 = (GifEncoder.GifJob)this.jobs.dequeue();
               }
            }

            if (var1 != null) {
               var1.execute();
            }

            try {
               Thread.sleep(10L);
            } catch (InterruptedException var3) {
               var3.printStackTrace();
            }
         } else {
            try {
               Thread.sleep(70L);
            } catch (InterruptedException var4) {
               var4.printStackTrace();
            }
         }
      }

      this.onFinish();
   }

   public void handleFrame(int var1, int var2, int var3, FrameBufferObjects var4) {
      ByteBuffer var5 = GlUtil.getDynamicByteBuffer(var1 * var2 * var3, 0);
      GL11.glReadPixels(0, 0, var1, var2, 6408, 5121, var5);
      byte[] var8 = getGifArray(var1 * var2 * var3);
      var5.get(var8);
      GifEncoder.GifJob var7;
      (var7 = new GifEncoder.GifJob(var8, var1, var2, var3)).prepare();
      synchronized(this.jobs) {
         this.jobs.enqueue(var7);
      }
   }

   class GifJob {
      private Object wt = new Object();
      private int width;
      private int height;
      private int bpp;
      private byte[] gifArray;
      private int imgWidth;
      private int imgHeight;
      private BufferedImage buffered;
      private int resultWidth;
      private int resultHeight;
      private int preCompressionFact;
      private int[] argbArray;
      public final int ySpan = 16;

      public GifJob(byte[] var2, int var3, int var4, int var5) {
         this.gifArray = var2;
         this.width = var3;
         this.height = var4;
         this.bpp = var5;
      }

      public void prepare() {
         this.preCompressionFact = (Integer)EngineSettings.GIF_QUALITY.getCurrentState();
         this.resultWidth = this.width / this.preCompressionFact;
         this.resultHeight = this.height / this.preCompressionFact;
         this.argbArray = GifEncoder.getARGBArray(this.resultWidth * this.resultHeight);
         if (GifEncoder.bImg == null || GifEncoder.bImg.getWidth() != this.resultWidth || GifEncoder.bImg.getHeight() != this.resultHeight) {
            GifEncoder.bImg = new BufferedImage(this.resultWidth, this.resultHeight, 2);
         }

         GifEncoder.this.running = 0;
         synchronized(this.wt) {
            for(int var2 = 0; var2 < this.resultHeight; var2 += 16) {
               GifEncoder.this.threadPool.execute(new GifEncoder.GifJob.XRow(var2));
            }

            while(true) {
               if (GifEncoder.this.running <= 0) {
                  break;
               }

               try {
                  this.wt.wait(1000L);
               } catch (InterruptedException var3) {
                  var3.printStackTrace();
               }
            }
         }

         GifEncoder.bImg.setRGB(0, 0, this.resultWidth, this.resultHeight, this.argbArray, 0, this.resultWidth);
         GifEncoder.freeGifArray(this.gifArray);
         GifEncoder.freeARGBArray(this.argbArray);
         this.imgWidth = (Integer)EngineSettings.GIF_WIDTH.getCurrentState();
         this.imgHeight = (Integer)EngineSettings.GIF_HEIGHT.getCurrentState();
         float var1 = (float)this.height / (float)this.width;
         this.imgHeight = (int)(var1 * (float)this.imgHeight);
         if (this.buffered == null || this.buffered.getWidth() != this.imgWidth || this.buffered.getHeight() != this.imgHeight) {
            this.buffered = new BufferedImage(this.imgWidth, this.imgHeight, 5);
         }

         (new MultiStepRescaleOp(this.imgWidth, this.imgHeight)).filter(GifEncoder.bImg, this.buffered);
      }

      public void execute() {
         GifEncoder.this.addFrame(this.buffered);
      }

      class XRow implements Runnable {
         public int y;

         public XRow(int var2) {
            this.y = var2;
            ++GifEncoder.this.running;
         }

         public void run() {
            int var1 = Math.min(GifJob.this.resultHeight, this.y + 16);

            for(int var2 = this.y; var2 < var1; ++var2) {
               for(int var3 = 0; var3 < GifJob.this.resultWidth; ++var3) {
                  int var4 = (var3 * GifJob.this.preCompressionFact + var2 * GifJob.this.preCompressionFact * GifJob.this.width) * GifJob.this.bpp;
                  int var5 = GifJob.this.gifArray[var4] & 255;
                  int var6 = GifJob.this.gifArray[var4 + 1] & 255;
                  int var7 = GifJob.this.gifArray[var4 + 2] & 255;
                  var4 = GifJob.this.gifArray[var4 + 3] & 255;
                  GifJob.this.argbArray[var3 + (GifJob.this.resultHeight - (var2 + 1)) * GifJob.this.resultWidth] = var4 << 24 | var5 << 16 | var6 << 8 | var7;
               }
            }

            synchronized(GifJob.this.wt) {
               --GifEncoder.this.running;
               GifJob.this.wt.notifyAll();
            }
         }
      }
   }
}
