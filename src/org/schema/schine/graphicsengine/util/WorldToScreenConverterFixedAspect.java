package org.schema.schine.graphicsengine.util;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.common.util.linAlg.Matrix4fTools;
import org.schema.schine.graphicsengine.core.GlUtil;

public class WorldToScreenConverterFixedAspect extends WorldToScreenConverter {
   public boolean overflowX;
   float[] sBufferModel = new float[16];

   public Vector3f convert(Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4, boolean var5) {
      (var4 = new Vector3f(var4)).normalize();
      this.toVec.set(var1);
      this.dir.sub(var1, var3);
      this.dir.normalize();
      this.fBuffer.rewind();
      GlUtil.gluProject(var1.x, var1.y, var1.z, this.modelBuffer, this.projBuffer, this.screenBuffer, this.fBuffer);
      this.toVec.set(this.fBuffer.get(0), this.fBuffer.get(1), this.fBuffer.get(2));
      float var6 = var4.dot(this.dir);
      this.getMiddleOfScreen(this.middle);
      var2.set(this.toVec.x, 600.0F - this.toVec.y, 0.0F);
      this.overflowX = false;
      if (var6 < 0.0F) {
         (var1 = new Vector3f()).sub(this.middle, var2);
         if (var1.length() == 0.0F) {
            var1.set(this.lastDir);
         }

         this.overflowX = true;
         var1.normalize();
         this.lastDir.set(var1);
         if (var5) {
            var1.scale(1.0E7F);
         }

         var2.add(var1);
      }

      return var2;
   }

   public Vector3f getMiddleOfScreen(Vector3f var1) {
      var1.set(400.0F, 300.0F, 0.0F);
      return var1;
   }

   public Vector3f convert(Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4, boolean var5, Matrix4f var6, Transform var7) {
      this.modelBuffer.rewind();
      this.projBuffer.rewind();
      this.screenBuffer.rewind();
      float[] var8 = Matrix4fTools.getCoefficients(var7.getMatrix(new javax.vecmath.Matrix4f()));
      var6.store(this.projBuffer);
      this.modelBuffer.put(var8);
      this.screenBuffer.put(0);
      this.screenBuffer.put(0);
      this.screenBuffer.put(795);
      this.screenBuffer.put(595);
      this.screenBuffer.rewind();
      this.modelBuffer.rewind();
      this.projBuffer.rewind();
      return this.convert(var1, var2, var3, var4, var5);
   }
}
