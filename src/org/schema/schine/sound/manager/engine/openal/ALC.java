package org.schema.schine.sound.manager.engine.openal;

import java.nio.IntBuffer;

public interface ALC {
   int ALC_NO_ERROR = 0;
   int ALC_INVALID_DEVICE = 40961;
   int ALC_INVALID_CONTEXT = 40962;
   int ALC_INVALID_ENUM = 40963;
   int ALC_INVALID_VALUE = 40964;
   int ALC_OUT_OF_MEMORY = 40965;
   int ALC_DEFAULT_DEVICE_SPECIFIER = 4100;
   int ALC_DEVICE_SPECIFIER = 4101;
   int ALC_EXTENSIONS = 4102;
   int ALC_MAJOR_VERSION = 4096;
   int ALC_MINOR_VERSION = 4097;
   int ALC_ATTRIBUTES_SIZE = 4098;
   int ALC_ALL_ATTRIBUTES = 4099;
   int ALC_CAPTURE_DEVICE_SPECIFIER = 784;
   int ALC_CAPTURE_DEFAULT_DEVICE_SPECIFIER = 785;
   int ALC_CAPTURE_SAMPLES = 786;
   int ALC_DEFAULT_ALL_DEVICES_SPECIFIER = 4114;
   int ALC_ALL_DEVICES_SPECIFIER = 4115;

   void createALC();

   void destroyALC();

   boolean isCreated();

   String alcGetString(int var1);

   boolean alcIsExtensionPresent(String var1);

   void alcGetInteger(int var1, IntBuffer var2, int var3);

   void alcDevicePauseSOFT();

   void alcDeviceResumeSOFT();
}
