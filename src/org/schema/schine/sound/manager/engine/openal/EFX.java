package org.schema.schine.sound.manager.engine.openal;

import java.nio.IntBuffer;

public interface EFX {
   String ALC_EXT_EFX_NAME = "ALC_EXT_EFX";
   int ALC_EFX_MAJOR_VERSION = 131073;
   int ALC_EFX_MINOR_VERSION = 131074;
   int ALC_MAX_AUXILIARY_SENDS = 131075;
   int AL_DIRECT_FILTER = 131077;
   int AL_AUXILIARY_SEND_FILTER = 131078;
   int AL_DIRECT_FILTER_GAINHF_AUTO = 131082;
   int AL_REVERB_DENSITY = 1;
   int AL_REVERB_DIFFUSION = 2;
   int AL_REVERB_GAIN = 3;
   int AL_REVERB_GAINHF = 4;
   int AL_REVERB_DECAY_TIME = 5;
   int AL_REVERB_DECAY_HFRATIO = 6;
   int AL_REVERB_REFLECTIONS_GAIN = 7;
   int AL_REVERB_REFLECTIONS_DELAY = 8;
   int AL_REVERB_LATE_REVERB_GAIN = 9;
   int AL_REVERB_LATE_REVERB_DELAY = 10;
   int AL_REVERB_AIR_ABSORPTION_GAINHF = 11;
   int AL_REVERB_ROOM_ROLLOFF_FACTOR = 12;
   int AL_REVERB_DECAY_HFLIMIT = 13;
   int AL_EFFECT_TYPE = 32769;
   int AL_EFFECT_REVERB = 1;
   int AL_EFFECTSLOT_EFFECT = 1;
   int AL_LOWPASS_GAIN = 1;
   int AL_LOWPASS_GAINHF = 2;
   int AL_FILTER_TYPE = 32769;
   int AL_FILTER_NULL = 0;
   int AL_FILTER_LOWPASS = 1;
   int AL_FILTER_HIGHPASS = 2;

   void alGenAuxiliaryEffectSlots(int var1, IntBuffer var2);

   void alGenEffects(int var1, IntBuffer var2);

   void alEffecti(int var1, int var2, int var3);

   void alAuxiliaryEffectSloti(int var1, int var2, int var3);

   void alDeleteEffects(int var1, IntBuffer var2);

   void alDeleteAuxiliaryEffectSlots(int var1, IntBuffer var2);

   void alGenFilters(int var1, IntBuffer var2);

   void alFilteri(int var1, int var2, int var3);

   void alFilterf(int var1, int var2, float var3);

   void alDeleteFilters(int var1, IntBuffer var2);

   void alEffectf(int var1, int var2, float var3);
}
