package org.schema.schine.sound.manager.engine;

public interface AudioRenderer {
   void setListener(Listener var1);

   void setEnvironment(Environment var1);

   void playSourceInstance(AudioSource var1);

   void playSource(AudioSource var1);

   void pauseSource(AudioSource var1);

   void stopSource(AudioSource var1);

   void updateSourceParam(AudioSource var1, AudioParam var2);

   void updateListenerParam(Listener var1, ListenerParam var2);

   float getSourcePlaybackTime(AudioSource var1);

   void deleteFilter(Filter var1);

   void deleteAudioData(AudioData var1);

   void initialize();

   void update(float var1);

   void pauseAll();

   void resumeAll();

   void cleanup();
}
