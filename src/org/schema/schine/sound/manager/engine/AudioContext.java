package org.schema.schine.sound.manager.engine;

public class AudioContext {
   private static ThreadLocal audioRenderer = new ThreadLocal();

   public static void setAudioRenderer(AudioRenderer var0) {
      audioRenderer.set(var0);
   }

   public static AudioRenderer getAudioRenderer() {
      return (AudioRenderer)audioRenderer.get();
   }
}
