package org.schema.schine.sound.manager.engine;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import org.schema.schine.sound.manager.engine.util.NativeObject;

public class AudioStream extends AudioData implements Closeable {
   protected InputStream in;
   protected float duration = -1.0F;
   protected boolean open = false;
   protected boolean eof = false;
   protected int[] ids;
   protected int unqueuedBuffersBytes = 0;

   public AudioStream() {
   }

   protected AudioStream(int[] var1) {
      super(-1);
      this.ids = var1;
   }

   public void updateData(InputStream var1, float var2) {
      if (this.id == -1 && this.in == null) {
         this.in = var1;
         this.duration = var2;
         this.open = true;
      } else {
         throw new IllegalStateException("Data already set!");
      }
   }

   public int readSamples(byte[] var1, int var2, int var3) {
      if (this.open && !this.eof) {
         try {
            int var5;
            if ((var5 = this.in.read(var1, var2, var3)) < 0) {
               this.eof = true;
            }

            return var5;
         } catch (IOException var4) {
            var4.printStackTrace();
            this.eof = true;
            return -1;
         }
      } else {
         return -1;
      }
   }

   public int readSamples(byte[] var1) {
      return this.readSamples(var1, 0, var1.length);
   }

   public float getDuration() {
      return this.duration;
   }

   public int getId() {
      throw new RuntimeException("Don't use getId() on streams");
   }

   public void setId(int var1) {
      throw new RuntimeException("Don't use setId() on streams");
   }

   public void initIds(int var1) {
      this.ids = new int[var1];
   }

   public int getId(int var1) {
      return this.ids[var1];
   }

   public void setId(int var1, int var2) {
      this.ids[var1] = var2;
   }

   public int[] getIds() {
      return this.ids;
   }

   public void setIds(int[] var1) {
      this.ids = var1;
   }

   public AudioData.DataType getDataType() {
      return AudioData.DataType.STREAM;
   }

   public void resetObject() {
      this.id = -1;
      this.ids = null;
      this.setUpdateNeeded();
   }

   public void deleteObject(Object var1) {
      ((AudioRenderer)var1).deleteAudioData(this);
   }

   public NativeObject createDestructableClone() {
      return new AudioStream(this.ids);
   }

   public boolean isEOF() {
      return this.eof;
   }

   public void close() {
      if (this.in != null && this.open) {
         try {
            this.in.close();
         } catch (IOException var1) {
         }

         this.open = false;
      } else {
         throw new RuntimeException("AudioStream is already closed!");
      }
   }

   public boolean isSeekable() {
      return this.in instanceof SeekableStream;
   }

   public int getUnqueuedBufferBytes() {
      return this.unqueuedBuffersBytes;
   }

   public void setUnqueuedBufferBytes(int var1) {
      this.unqueuedBuffersBytes = var1;
   }

   public void setTime(float var1) {
      if (this.in instanceof SeekableStream) {
         ((SeekableStream)this.in).setTime(var1);
         this.eof = false;
         this.unqueuedBuffersBytes = 0;
      } else {
         throw new IllegalStateException("Cannot use setTime on a stream that is not seekable. You must load the file with the streamCache option set to true");
      }
   }

   public long getUniqueId() {
      return 30064771072L | (long)this.ids[0];
   }
}
