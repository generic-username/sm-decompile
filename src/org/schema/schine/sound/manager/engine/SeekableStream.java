package org.schema.schine.sound.manager.engine;

public interface SeekableStream {
   void setTime(float var1);
}
