package org.schema.schine.sound.manager.engine;

import org.schema.common.util.AssetId;

public class AudioId extends AssetId {
   private boolean stream;
   private boolean streamCache;
   private String name;

   public AudioId(String var1, boolean var2, boolean var3) {
      this(var1, var2);
      this.streamCache = var3;
   }

   public AudioId(String var1, boolean var2) {
      this.name = var1;
      this.stream = var2;
   }

   public AudioId(String var1) {
      this(var1, false);
   }

   public AudioId() {
   }

   public String toString() {
      return this.name + (this.stream ? (this.streamCache ? " (Stream/Cache)" : " (Stream)") : " (Buffer)");
   }

   public boolean isStream() {
      return this.stream;
   }

   public boolean isUseStreamCache() {
      return this.streamCache;
   }

   public boolean equals(Object var1) {
      if (var1 != null && this.getClass() == var1.getClass()) {
         AudioId var2 = (AudioId)var1;
         return this.name.equals(var2.name) && this.stream == var2.stream && this.streamCache == var2.streamCache;
      } else {
         return false;
      }
   }

   public int hashCode() {
      return ((469 + super.hashCode()) * 67 + (this.stream ? 1 : 0)) * 67 + (this.streamCache ? 1 : 0);
   }
}
