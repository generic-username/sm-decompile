package org.schema.schine.sound.manager.engine;

import org.schema.common.FastMath;

public class Environment {
   private float airAbsorbGainHf = 0.99426F;
   private float roomRolloffFactor = 0.0F;
   private float decayTime = 1.49F;
   private float decayHFRatio = 0.54F;
   private float density = 1.0F;
   private float diffusion = 0.3F;
   private float gain = 0.316F;
   private float gainHf = 0.022F;
   private float lateReverbDelay = 0.088F;
   private float lateReverbGain = 0.768F;
   private float reflectDelay = 0.162F;
   private float reflectGain = 0.052F;
   private boolean decayHfLimit = true;
   public static final Environment Garage = new Environment(1.0F, 1.0F, 1.0F, 1.0F, 0.9F, 0.5F, 0.751F, 0.0039F, 0.661F, 0.0137F);
   public static final Environment Dungeon = new Environment(0.75F, 1.0F, 1.0F, 0.75F, 1.6F, 1.0F, 0.95F, 0.0026F, 0.93F, 0.0103F);
   public static final Environment Cavern = new Environment(0.5F, 1.0F, 1.0F, 0.5F, 2.25F, 1.0F, 0.908F, 0.0103F, 0.93F, 0.041F);
   public static final Environment AcousticLab = new Environment(0.5F, 1.0F, 1.0F, 1.0F, 0.28F, 1.0F, 0.87F, 0.002F, 0.81F, 0.008F);
   public static final Environment Closet = new Environment(1.0F, 1.0F, 1.0F, 1.0F, 0.15F, 1.0F, 0.6F, 0.0025F, 0.5F, 6.0E-4F);

   private static float eaxDbToAmp(float var0) {
      var0 /= 2000.0F;
      return FastMath.pow(10.0F, var0);
   }

   public Environment() {
   }

   public Environment(Environment var1) {
      this.airAbsorbGainHf = var1.airAbsorbGainHf;
      this.roomRolloffFactor = var1.roomRolloffFactor;
      this.decayTime = var1.decayTime;
      this.decayHFRatio = var1.decayHFRatio;
      this.density = var1.density;
      this.diffusion = var1.diffusion;
      this.gain = var1.gain;
      this.gainHf = var1.gainHf;
      this.lateReverbDelay = var1.lateReverbDelay;
      this.lateReverbGain = var1.lateReverbGain;
      this.reflectDelay = var1.reflectDelay;
      this.reflectGain = var1.reflectGain;
      this.decayHfLimit = var1.decayHfLimit;
   }

   public Environment(float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, float var9, float var10) {
      this.decayTime = var5;
      this.decayHFRatio = var6;
      this.density = var1;
      this.diffusion = var2;
      this.gain = var3;
      this.gainHf = var4;
      this.lateReverbDelay = var10;
      this.lateReverbGain = var9;
      this.reflectDelay = var8;
      this.reflectGain = var7;
   }

   public Environment(float[] var1) {
      if (var1.length != 28) {
         throw new IllegalArgumentException("Not an EAX preset");
      } else {
         this.diffusion = var1[2];
         this.gain = eaxDbToAmp(var1[3]);
         this.gainHf = eaxDbToAmp(var1[4]) / eaxDbToAmp(var1[5]);
         this.decayTime = var1[6];
         this.decayHFRatio = var1[7] / var1[8];
         this.reflectGain = eaxDbToAmp(var1[9]);
         this.reflectDelay = var1[10];
         this.lateReverbGain = eaxDbToAmp(var1[14]);
         this.lateReverbDelay = var1[15];
         this.airAbsorbGainHf = eaxDbToAmp(var1[23]);
         this.roomRolloffFactor = var1[26];
      }
   }

   public float getAirAbsorbGainHf() {
      return this.airAbsorbGainHf;
   }

   public void setAirAbsorbGainHf(float var1) {
      this.airAbsorbGainHf = var1;
   }

   public float getDecayHFRatio() {
      return this.decayHFRatio;
   }

   public void setDecayHFRatio(float var1) {
      this.decayHFRatio = var1;
   }

   public boolean isDecayHfLimit() {
      return this.decayHfLimit;
   }

   public void setDecayHfLimit(boolean var1) {
      this.decayHfLimit = var1;
   }

   public float getDecayTime() {
      return this.decayTime;
   }

   public void setDecayTime(float var1) {
      this.decayTime = var1;
   }

   public float getDensity() {
      return this.density;
   }

   public void setDensity(float var1) {
      this.density = var1;
   }

   public float getDiffusion() {
      return this.diffusion;
   }

   public void setDiffusion(float var1) {
      this.diffusion = var1;
   }

   public float getGain() {
      return this.gain;
   }

   public void setGain(float var1) {
      this.gain = var1;
   }

   public float getGainHf() {
      return this.gainHf;
   }

   public void setGainHf(float var1) {
      this.gainHf = var1;
   }

   public float getLateReverbDelay() {
      return this.lateReverbDelay;
   }

   public void setLateReverbDelay(float var1) {
      this.lateReverbDelay = var1;
   }

   public float getLateReverbGain() {
      return this.lateReverbGain;
   }

   public void setLateReverbGain(float var1) {
      this.lateReverbGain = var1;
   }

   public float getReflectDelay() {
      return this.reflectDelay;
   }

   public void setReflectDelay(float var1) {
      this.reflectDelay = var1;
   }

   public float getReflectGain() {
      return this.reflectGain;
   }

   public void setReflectGain(float var1) {
      this.reflectGain = var1;
   }

   public float getRoomRolloffFactor() {
      return this.roomRolloffFactor;
   }

   public void setRoomRolloffFactor(float var1) {
      this.roomRolloffFactor = var1;
   }
}
