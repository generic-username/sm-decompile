package org.schema.schine.sound.manager.engine.lwjgl;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import org.lwjgl.openal.AL10;
import org.lwjgl.openal.AL11;
import org.schema.schine.sound.manager.engine.openal.AL;

public final class LwjglAL implements AL {
   public final String alGetString(int var1) {
      return AL10.alGetString(var1);
   }

   public final int alGenSources() {
      return AL10.alGenSources();
   }

   public final int alGetError() {
      return AL10.alGetError();
   }

   public final void alDeleteSources(int var1, IntBuffer var2) {
      if (var2.position() != 0) {
         throw new AssertionError();
      } else if (var2.limit() != var1) {
         throw new AssertionError();
      } else {
         AL10.alDeleteSources(var2);
      }
   }

   public final void alGenBuffers(int var1, IntBuffer var2) {
      if (var2.position() != 0) {
         throw new AssertionError();
      } else if (var2.limit() != var1) {
         throw new AssertionError();
      } else {
         AL10.alGenBuffers(var2);
      }
   }

   public final void alDeleteBuffers(int var1, IntBuffer var2) {
      if (var2.position() != 0) {
         throw new AssertionError();
      } else if (var2.limit() != var1) {
         throw new AssertionError();
      } else {
         AL10.alDeleteBuffers(var2);
      }
   }

   public final void alSourceStop(int var1) {
      AL10.alSourceStop(var1);
   }

   public final void alSourcei(int var1, int var2, int var3) {
      AL10.alSourcei(var1, var2, var3);
   }

   public final void alBufferData(int var1, int var2, ByteBuffer var3, int var4, int var5) {
      if (var3.position() != 0) {
         throw new AssertionError();
      } else if (var3.limit() != var4) {
         throw new AssertionError();
      } else {
         AL10.alBufferData(var1, var2, var3, var5);
      }
   }

   public final void alSourcePlay(int var1) {
      AL10.alSourcePlay(var1);
   }

   public final void alSourcePause(int var1) {
      AL10.alSourcePause(var1);
   }

   public final void alSourcef(int var1, int var2, float var3) {
      AL10.alSourcef(var1, var2, var3);
   }

   public final void alSource3f(int var1, int var2, float var3, float var4, float var5) {
      AL10.alSource3f(var1, var2, var3, var4, var5);
   }

   public final int alGetSourcei(int var1, int var2) {
      return AL10.alGetSourcei(var1, var2);
   }

   public final void alSourceUnqueueBuffers(int var1, int var2, IntBuffer var3) {
      if (var3.position() != 0) {
         throw new AssertionError();
      } else if (var3.limit() != var2) {
         throw new AssertionError();
      } else {
         AL10.alSourceUnqueueBuffers(var1, var3);
      }
   }

   public final void alSourceQueueBuffers(int var1, int var2, IntBuffer var3) {
      if (var3.position() != 0) {
         throw new AssertionError();
      } else if (var3.limit() != var2) {
         throw new AssertionError();
      } else {
         AL10.alSourceQueueBuffers(var1, var3);
      }
   }

   public final void alListener(int var1, FloatBuffer var2) {
      AL10.alListener(var1, var2);
   }

   public final void alListenerf(int var1, float var2) {
      AL10.alListenerf(var1, var2);
   }

   public final void alListener3f(int var1, float var2, float var3, float var4) {
      AL10.alListener3f(var1, var2, var3, var4);
   }

   public final void alSource3i(int var1, int var2, int var3, int var4, int var5) {
      AL11.alSource3i(var1, var2, var3, var4, var5);
   }
}
