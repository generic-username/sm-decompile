package org.schema.schine.sound.manager.engine.lwjgl;

import java.nio.IntBuffer;
import org.lwjgl.LWJGLException;
import org.lwjgl.openal.AL;
import org.lwjgl.openal.ALC10;
import org.schema.schine.sound.manager.engine.openal.ALC;

public class LwjglALC implements ALC {
   public void createALC() {
      try {
         AL.create();
      } catch (LWJGLException var2) {
         throw new RuntimeException(var2);
      }
   }

   public void destroyALC() {
      AL.destroy();
   }

   public boolean isCreated() {
      return AL.isCreated();
   }

   public String alcGetString(int var1) {
      return ALC10.alcGetString(ALC10.alcGetContextsDevice(ALC10.alcGetCurrentContext()), var1);
   }

   public boolean alcIsExtensionPresent(String var1) {
      return ALC10.alcIsExtensionPresent(ALC10.alcGetContextsDevice(ALC10.alcGetCurrentContext()), var1);
   }

   public void alcGetInteger(int var1, IntBuffer var2, int var3) {
      if (var2.position() != 0) {
         throw new AssertionError();
      } else if (var2.limit() != var3) {
         throw new AssertionError();
      } else {
         ALC10.alcGetInteger(ALC10.alcGetContextsDevice(ALC10.alcGetCurrentContext()), var1, var2);
      }
   }

   public void alcDevicePauseSOFT() {
   }

   public void alcDeviceResumeSOFT() {
   }
}
