package org.schema.schine.sound.manager.engine;

public enum AudioSourceCategory {
   MUSIC,
   GUI,
   GAME,
   DIALOG,
   VOICE;
}
