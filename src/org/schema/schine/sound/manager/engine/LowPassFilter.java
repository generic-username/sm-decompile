package org.schema.schine.sound.manager.engine;

import org.schema.schine.sound.manager.engine.util.NativeObject;

public class LowPassFilter extends Filter {
   protected float volume;
   protected float highFreqVolume;

   public LowPassFilter(float var1, float var2) {
      this.setVolume(var1);
      this.setHighFreqVolume(var2);
   }

   protected LowPassFilter(int var1) {
      super(var1);
   }

   public float getHighFreqVolume() {
      return this.highFreqVolume;
   }

   public void setHighFreqVolume(float var1) {
      if (var1 >= 0.0F && var1 <= 1.0F) {
         this.highFreqVolume = var1;
         this.updateNeeded = true;
      } else {
         throw new IllegalArgumentException("High freq volume must be between 0 and 1");
      }
   }

   public float getVolume() {
      return this.volume;
   }

   public void setVolume(float var1) {
      if (var1 >= 0.0F && var1 <= 1.0F) {
         this.volume = var1;
         this.updateNeeded = true;
      } else {
         throw new IllegalArgumentException("Volume must be between 0 and 1");
      }
   }

   public NativeObject createDestructableClone() {
      return new LowPassFilter(this.id);
   }

   public long getUniqueId() {
      return 34359738368L | (long)this.id;
   }
}
