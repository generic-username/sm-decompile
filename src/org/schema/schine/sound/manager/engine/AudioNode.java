package org.schema.schine.sound.manager.engine;

import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.forms.Transformable;

public abstract class AudioNode implements Transformable, AudioSource {
   public static final int SAVABLE_VERSION = 1;
   protected boolean loop = false;
   protected float volume = 1.0F;
   protected float pitch = 1.0F;
   protected float timeOffset = 0.0F;
   protected Filter dryFilter;
   protected AudioId audioKey;
   protected transient AudioData data = null;
   protected transient volatile AudioSource.Status status;
   protected transient volatile int channel;
   protected Vector3f previousWorldTranslation;
   protected Vector3f velocity;
   protected boolean reverbEnabled;
   protected float maxDistance;
   protected float refDistance;
   protected Filter reverbFilter;
   private boolean directional;
   protected Vector3f direction;
   protected float innerAngle;
   protected float outerAngle;
   protected boolean positional;
   protected boolean velocityFromTranslation;
   protected float lastTpf;

   public AudioNode() {
      this.status = AudioSource.Status.STOPPED;
      this.channel = -1;
      this.previousWorldTranslation = new Vector3f(Float.NaN, Float.NaN, Float.NaN);
      this.velocity = new Vector3f();
      this.reverbEnabled = false;
      this.maxDistance = 200.0F;
      this.refDistance = 10.0F;
      this.directional = false;
      this.direction = new Vector3f(0.0F, 0.0F, 1.0F);
      this.innerAngle = 360.0F;
      this.outerAngle = 360.0F;
      this.positional = true;
      this.velocityFromTranslation = false;
   }

   public AudioNode(AudioData var1, AudioId var2) {
      this.status = AudioSource.Status.STOPPED;
      this.channel = -1;
      this.previousWorldTranslation = new Vector3f(Float.NaN, Float.NaN, Float.NaN);
      this.velocity = new Vector3f();
      this.reverbEnabled = false;
      this.maxDistance = 200.0F;
      this.refDistance = 10.0F;
      this.directional = false;
      this.direction = new Vector3f(0.0F, 0.0F, 1.0F);
      this.innerAngle = 360.0F;
      this.outerAngle = 360.0F;
      this.positional = true;
      this.velocityFromTranslation = false;
      this.setAudioData(var1, var2);
   }

   protected AudioRenderer getRenderer() {
      AudioRenderer var1;
      if ((var1 = AudioContext.getAudioRenderer()) == null) {
         throw new IllegalStateException("No audio renderer available, make sure call is being performed on render thread.");
      } else {
         return var1;
      }
   }

   public void play() {
      if (this.positional && this.data.getChannels() > 1) {
         throw new IllegalStateException("Only mono audio is supported for positional audio nodes");
      } else {
         this.getRenderer().playSource(this);
      }
   }

   public void playInstance() {
      if (this.positional && this.data.getChannels() > 1) {
         throw new IllegalStateException("Only mono audio is supported for positional audio nodes");
      } else {
         this.getRenderer().playSourceInstance(this);
      }
   }

   public void stop() {
      this.getRenderer().stopSource(this);
   }

   public void pause() {
      this.getRenderer().pauseSource(this);
   }

   public final void setChannel(int var1) {
      if (this.status != AudioSource.Status.STOPPED) {
         throw new IllegalStateException("Can only set source id when stopped");
      } else {
         this.channel = var1;
      }
   }

   public int getChannel() {
      return this.channel;
   }

   public Filter getDryFilter() {
      return this.dryFilter;
   }

   public void setDryFilter(Filter var1) {
      this.dryFilter = var1;
      if (this.channel >= 0) {
         this.getRenderer().updateSourceParam(this, AudioParam.DryFilter);
      }

   }

   public void setAudioData(AudioData var1, AudioId var2) {
      if (this.data != null) {
         throw new IllegalStateException("Cannot change data once its set");
      } else {
         this.data = var1;
         this.audioKey = var2;
      }
   }

   public AudioData getAudioData() {
      return this.data;
   }

   public AudioSource.Status getStatus() {
      return this.status;
   }

   public final void setStatus(AudioSource.Status var1) {
      this.status = var1;
   }

   public AudioData.DataType getType() {
      return this.data == null ? null : this.data.getDataType();
   }

   public boolean isLooping() {
      return this.loop;
   }

   public void setLooping(boolean var1) {
      this.loop = var1;
      if (this.channel >= 0) {
         this.getRenderer().updateSourceParam(this, AudioParam.Looping);
      }

   }

   public float getPitch() {
      return this.pitch;
   }

   public void setPitch(float var1) {
      if (var1 >= 0.5F && var1 <= 2.0F) {
         this.pitch = var1;
         if (this.channel >= 0) {
            this.getRenderer().updateSourceParam(this, AudioParam.Pitch);
         }

      } else {
         throw new IllegalArgumentException("Pitch must be between 0.5 and 2.0");
      }
   }

   public float getVolume() {
      return this.volume;
   }

   public void setVolume(float var1) {
      if (var1 < 0.0F) {
         throw new IllegalArgumentException("Volume cannot be negative");
      } else {
         this.volume = var1;
         if (this.channel >= 0) {
            this.getRenderer().updateSourceParam(this, AudioParam.Volume);
         }

      }
   }

   public float getTimeOffset() {
      return this.timeOffset;
   }

   public void setTimeOffset(float var1) {
      if (var1 < 0.0F) {
         throw new IllegalArgumentException("Time offset cannot be negative");
      } else {
         this.timeOffset = var1;
         if (this.data instanceof AudioStream) {
            ((AudioStream)this.data).setTime(var1);
         } else {
            if (this.status == AudioSource.Status.PLAYING) {
               this.stop();
               this.play();
            }

         }
      }
   }

   public float getPlaybackTime() {
      return this.channel >= 0 ? this.getRenderer().getSourcePlaybackTime(this) : 0.0F;
   }

   public Vector3f getVelocity() {
      return this.velocity;
   }

   public void setVelocity(Vector3f var1) {
      this.velocity.set(var1);
      if (this.channel >= 0) {
         this.getRenderer().updateSourceParam(this, AudioParam.Velocity);
      }

   }

   public boolean isReverbEnabled() {
      return this.reverbEnabled;
   }

   public void setReverbEnabled(boolean var1) {
      this.reverbEnabled = var1;
      if (this.channel >= 0) {
         this.getRenderer().updateSourceParam(this, AudioParam.ReverbEnabled);
      }

   }

   public Filter getReverbFilter() {
      return this.reverbFilter;
   }

   public void setReverbFilter(Filter var1) {
      this.reverbFilter = var1;
      if (this.channel >= 0) {
         this.getRenderer().updateSourceParam(this, AudioParam.ReverbFilter);
      }

   }

   public float getMaxDistance() {
      return this.maxDistance;
   }

   public void setMaxDistance(float var1) {
      if (var1 < 0.0F) {
         throw new IllegalArgumentException("Max distance cannot be negative");
      } else {
         this.maxDistance = var1;
         if (this.channel >= 0) {
            this.getRenderer().updateSourceParam(this, AudioParam.MaxDistance);
         }

      }
   }

   public float getRefDistance() {
      return this.refDistance;
   }

   public void setRefDistance(float var1) {
      if (var1 < 0.0F) {
         throw new IllegalArgumentException("Reference distance cannot be negative");
      } else {
         this.refDistance = var1;
         if (this.channel >= 0) {
            this.getRenderer().updateSourceParam(this, AudioParam.RefDistance);
         }

      }
   }

   public boolean isDirectional() {
      return this.directional;
   }

   public void setDirectional(boolean var1) {
      this.directional = var1;
      if (this.channel >= 0) {
         this.getRenderer().updateSourceParam(this, AudioParam.IsDirectional);
      }

   }

   public Vector3f getDirection() {
      return this.direction;
   }

   public void setDirection(Vector3f var1) {
      this.direction = var1;
      if (this.channel >= 0) {
         this.getRenderer().updateSourceParam(this, AudioParam.Direction);
      }

   }

   public float getInnerAngle() {
      return this.innerAngle;
   }

   public void setInnerAngle(float var1) {
      this.innerAngle = var1;
      if (this.channel >= 0) {
         this.getRenderer().updateSourceParam(this, AudioParam.InnerAngle);
      }

   }

   public float getOuterAngle() {
      return this.outerAngle;
   }

   public void setOuterAngle(float var1) {
      this.outerAngle = var1;
      if (this.channel >= 0) {
         this.getRenderer().updateSourceParam(this, AudioParam.OuterAngle);
      }

   }

   public boolean isPositional() {
      return this.positional;
   }

   public void updateGeometricState(float var1) {
      if (this.channel >= 0) {
         Vector3f var2 = this.getWorldTransform().origin;
         if (Float.isNaN(this.previousWorldTranslation.x) || !this.previousWorldTranslation.equals(var2)) {
            this.getRenderer().updateSourceParam(this, AudioParam.Position);
            if (this.velocityFromTranslation) {
               this.velocity.set(var2);
               this.velocity.sub(this.previousWorldTranslation);
               this.velocity.scale(1.0F / var1);
               this.getRenderer().updateSourceParam(this, AudioParam.Velocity);
            }

            this.previousWorldTranslation.set(var2);
         }

      }
   }

   public void setPositional(boolean var1) {
      this.positional = var1;
      if (this.channel >= 0) {
         this.getRenderer().updateSourceParam(this, AudioParam.IsPositional);
      }

   }

   public boolean isVelocityFromTranslation() {
      return this.velocityFromTranslation;
   }

   public void setVelocityFromTranslation(boolean var1) {
      this.velocityFromTranslation = var1;
   }

   public String toString() {
      String var1 = this.getClass().getSimpleName() + "[status=" + this.status;
      if (this.volume != 1.0F) {
         var1 = var1 + ", vol=" + this.volume;
      }

      if (this.pitch != 1.0F) {
         var1 = var1 + ", pitch=" + this.pitch;
      }

      return var1 + "]";
   }
}
