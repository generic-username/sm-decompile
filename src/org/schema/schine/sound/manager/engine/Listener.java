package org.schema.schine.sound.manager.engine;

import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Quat4fTools;

public class Listener {
   private final Vector3f location;
   private final Vector3f velocity;
   private final Quat4f rotation;
   private float volume = 1.0F;
   private AudioRenderer renderer;

   public Listener() {
      this.location = new Vector3f();
      this.velocity = new Vector3f();
      this.rotation = new Quat4f();
   }

   public Listener(Listener var1) {
      this.location = new Vector3f(var1.location);
      this.velocity = new Vector3f(var1.velocity);
      this.rotation = new Quat4f(var1.rotation);
      this.volume = var1.volume;
   }

   public void setRenderer(AudioRenderer var1) {
      this.renderer = var1;
   }

   public float getVolume() {
      return this.volume;
   }

   public void setVolume(float var1) {
      this.volume = var1;
      if (this.renderer != null) {
         this.renderer.updateListenerParam(this, ListenerParam.VOL);
      }

   }

   public Vector3f getLocation() {
      return this.location;
   }

   public Quat4f getRotation() {
      return this.rotation;
   }

   public Vector3f getVelocity() {
      return this.velocity;
   }

   public Vector3f getLeft() {
      return Quat4fTools.getRotationColumn(this.rotation, 0);
   }

   public Vector3f getUp() {
      return Quat4fTools.getRotationColumn(this.rotation, 1);
   }

   public Vector3f getDirection() {
      return Quat4fTools.getRotationColumn(this.rotation, 2);
   }

   public void setLocation(Vector3f var1) {
      this.location.set(var1);
      if (this.renderer != null) {
         this.renderer.updateListenerParam(this, ListenerParam.POS);
      }

   }

   public void setRotation(Quat4f var1) {
      this.rotation.set(var1);
      if (this.renderer != null) {
         this.renderer.updateListenerParam(this, ListenerParam.ROT);
      }

   }

   public void setVelocity(Vector3f var1) {
      this.velocity.set(var1);
      if (this.renderer != null) {
         this.renderer.updateListenerParam(this, ListenerParam.VEL);
      }

   }
}
