package org.schema.schine.sound;

import com.bulletphysics.linearmath.Transform;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.physics.Physical;
import org.schema.schine.resource.UniqueInterface;

public interface AudioEntity extends Transformable, Physical, UniqueInterface {
   String getInsideSound();

   float getInsideSoundPitch();

   float getInsideSoundVolume();

   String getOutsideSound();

   float getOutsideSoundPitch();

   float getOutsideSoundVolume();

   float getSoundRadius();

   Transform getWorldTransformOnClient();

   boolean isOwnPlayerInside();
}
