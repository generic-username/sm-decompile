package org.schema.schine.network;

import org.schema.schine.network.objects.Sendable;

public interface SendableFactory {
   Sendable getInstance(StateInterface var1);

   Class getClazz();
}
