package org.schema.schine.network;

import it.unimi.dsi.fastutil.ints.Int2LongMap;
import it.unimi.dsi.fastutil.ints.Int2LongOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import java.util.Iterator;
import java.util.Observable;
import java.util.Map.Entry;
import javax.vecmath.Vector4f;

public class DataStatsManager extends Observable {
   private static final int BACK_LOG_ENTRIES = 300;
   private final DataStatsList sentData = new DataStatsList(new Vector4f(0.4F, 1.0F, 0.4F, 0.7F), this);
   private final ObjectArrayFIFOQueue sentDataToAdd = new ObjectArrayFIFOQueue();
   private final DataStatsList receivedData = new DataStatsList(new Vector4f(1.0F, 0.4F, 0.4F, 0.7F), this);
   private final ObjectArrayFIFOQueue receivedDataToAdd = new ObjectArrayFIFOQueue();
   private long lastUpdate;

   public void snapshotUpload(Object2ObjectOpenHashMap var1) {
      this.sentDataToAdd.enqueue(new DataStatsEntry(System.currentTimeMillis(), this.getClone(var1)));
   }

   private Object2ObjectOpenHashMap getClone(Object2ObjectOpenHashMap var1) {
      Object2ObjectOpenHashMap var2 = new Object2ObjectOpenHashMap();
      Iterator var4 = var1.entrySet().iterator();

      while(var4.hasNext()) {
         Entry var3 = (Entry)var4.next();
         var2.put(var3.getKey(), new Int2LongOpenHashMap((Int2LongMap)var3.getValue()));
         ((Int2LongOpenHashMap)var3.getValue()).clear();
      }

      return var2;
   }

   public void snapshotDownload(Object2ObjectOpenHashMap var1) {
      this.receivedDataToAdd.enqueue(new DataStatsEntry(System.currentTimeMillis(), this.getClone(var1)));
   }

   public void update() {
      if (System.currentTimeMillis() - this.lastUpdate > 1000L) {
         while(!this.sentDataToAdd.isEmpty()) {
            this.sentData.add(0, this.sentDataToAdd.dequeue());
         }

         while(true) {
            if (this.receivedDataToAdd.isEmpty()) {
               this.archiveOld();
               this.setChanged();
               this.notifyObservers();
               this.lastUpdate = System.currentTimeMillis();
               break;
            }

            this.receivedData.add(0, this.receivedDataToAdd.dequeue());
         }
      }

   }

   private void archiveOld() {
      while(this.sentData.size() > 300) {
         this.sentData.remove(this.sentData.size() - 1);
      }

      while(this.receivedData.size() > 300) {
         this.receivedData.remove(this.receivedData.size() - 1);
      }

   }

   public DataStatsList getSentData() {
      return this.sentData;
   }

   public DataStatsList getReceivedData() {
      return this.receivedData;
   }

   public void notifyGUI() {
      this.setChanged();
      this.notifyObservers();
   }
}
