package org.schema.schine.network;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public interface SerialializationInterface {
   void serialize(DataOutput var1, boolean var2) throws IOException;

   void deserialize(DataInput var1, int var2, boolean var3) throws IOException;
}
