package org.schema.schine.network;

import java.io.IOException;

public interface Recipient {
   int getId();

   void sendCommand(int var1, short var2, Class var3, Object... var4) throws IOException, InterruptedException;

   Object[] sendReturnedCommand(int var1, short var2, Class var3, Object... var4) throws IOException, InterruptedException;
}
